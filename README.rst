=========
GISCE ERP
=========

`GISCE ERP`_ is an vertical ERP (Enterprise Resource Planning) for electric
power industry. So it manages the generation, transmission, distribution and/or
sale of electric power. The `GISCE ERP`_ system is developed and supported by
`GISCE-TI S.L`_.

This is the code of the server, you must use it with `the client`_.


Docker
======

You can use docker containers to run the GISCE ERP service.

.. code-block:: shell

    # install docker
    sudo aptitude install docker-compose docker.io
    # get a copy of sources
    git clone --recursive git@github.com:gisce/erp.git
    cd erp
    # enable addons
    ./tools/link_addons.sh
    # build the docker containers
    cd docker
    docker-compose build
    # run the docker containers
    POSTGRES_PASSWORD=postgres ERP_POSTGRESQL_PASSWORD=postgres ERP_POSTGRESQL_HOST=db ERP_POSTGRESQL_USER=postgres docker-compose up

Now you can use `the client`_ to connect to the service via
http://localhost:8069 using XML-RPC


.. code-block:: shell

    # remove the docker containers
    docker-compose rm
    # remove the postgresql data
    rm -rf db


Requeriments
============

* Python >= 2.6
* PostgreSQL >= 8.2
* OOOP (webservices transactions patched: ooop-xt)
* libFacturacioATR
* spawn_oop
* progressbar
* supervisor
* Mako template engine
* libComXML>=0.0.5
* OrderedDict

.. _GISCE ERP: https://github.com/gisce/erp
.. _GISCE-TI S.L: http://gisce.net
.. _the client: https://github.com/gisce/erpclient
