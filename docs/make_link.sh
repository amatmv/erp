#!/bin/bash
pushd .
mkdir -p source/addons
cd source/addons
for addon in `find . -type l`; do
    unlink $addon;
done
for addon in `find . ../../../addons/gisce -name 'index.rst'`; do 
    ADDON=`echo $addon | cut -d '/' -f7`; echo "**** $ADDON ****";
    DIR=`dirname $addon`;
    ln -s $DIR $ADDON;
done
popd
