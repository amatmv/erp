.. GISCE-ERP documentation master file, created by
   sphinx-quickstart on Fri Sep 28 12:46:33 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentació GISCE-ERP
======================

Contingut :

.. toctree::
   :maxdepth: 2
   :glob:
   
   changelog/index
   addons/*/index

