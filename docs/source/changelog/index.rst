Changelog
=========

v2.22.0
~~~~~~~

**Novetats i millores**

  * Rectificadores amb el mateix IVA que la factura la qual rectfica.
  * Generació dels fitxers F1 i F1bis.
  * Informe en CSV i PDF de les liquidacions de la CNE.
  * Possibilitat de generar zip de F1 en selecció de factures.
  * Possibilitat de generar resum CSV en selecció de factures.
  * Resum de facturació en format CSV.

**Nous mòduls**

  * Mòdul de control de canvis en una pòlissa/contracte.

v2.21.0
~~~~~~~

**Novets i millores**

  * Millorat el rendiment en la facturació, generació de PDFs i XMLs.
  * Informe de resum CSV enla generació dels XMLs de facturació.
  * Mostar data inici i data final en el llistat de factures.
  * Utilitzar la població del CUPS per generar l'adreça.
  * Mostrar la factura que anul·la/rectifica.
  * Factures rectificadores que afecten el procés de refacturació.
  * Generació d'XMLs amb línies de regularització per la refacturació.
  * Facturació de conceptes (amb anul·ladores i rectificadores)
  * Resum per impostos i distribuïdora.
  * Fitxer OCSUM amb dobre IVA i refacturacions.
  * Dades del port òptic per la TPL.
  * Documentació mòdul Georeferenciació.
  * Monitorització de canvis a un CUPS.

**Nous mòduls**

  * Telegestió PRIME (Implantació en una instal·lació llegint de 6500 comptadors).
  * Full de consums.
  * Gestió de subestacions (F1 i F1bis)
  * Adreça secundària al CUPS.
  * Mòdul de gestió de tala.
