import sys
actual_encoding = sys.getdefaultencoding()
sys.stdout.write("Actual encoding is %s\n" % actual_encoding)
try:
    import locale
    loc = locale.getdefaultlocale()
    if loc[1]:
        encoding = loc[1]
        sys.stdout.write('encoding from "locale": %s\n' % (encoding,))
        sys.setdefaultencoding(encoding)
except ImportError:
    pass
