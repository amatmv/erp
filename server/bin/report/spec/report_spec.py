# coding=utf-8
from expects import *


with description('Report format'):
    with description('date format'):

        with it('must eval to False if is empty'):
            from report.report_sxw import _date_format

            df = _date_format(False)

            expect(bool(df)).to(be_false)

    with description('date time format'):
        with it('must eval to False if is empty'):
            from report.report_sxw import _dttime_format

            df = _dttime_format(False)

            expect(bool(df)).to(be_false)

