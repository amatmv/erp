import base64
import tempfile
from cStringIO import StringIO
from zipfile import ZipFile

from osv import osv, fields


class WizardAttachmentZip(osv.osv_memory):
    _name = 'wizard.attachment.zip'

    _columns = {
        'name': fields.char('File name', size=256),
        'n_items': fields.integer('Number of attachments'),
        'result': fields.binary('Result')
    }

    def make_zip(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        wizard = self.browse(cursor, uid, ids[0], context)
        attach_obj = self.pool.get('ir.attachment')
        attachment_ids = context.get('active_ids', [])
        # Better processing one by one
        fields_r = ['datas', 'datas_fname']
        z = StringIO()
        with ZipFile(z, 'w') as zfile:
            for att_id in attachment_ids:
                attch = attach_obj.read(cursor, uid, att_id, fields_r)
                content = base64.b64decode(attch['datas'])
                zfile.writestr(attch['datas_fname'], content)
        zip_content = base64.b64encode(z.getvalue())
        z.close()
        wizard.write({'result': zip_content})
        return True

    def _default_n_items(self, cursor, uid, context=None):
        if context is None:
            context = {}
        return len(context.get('active_ids', []))

    _defaults = {
        'name': lambda *a: tempfile.mktemp('.zip', 'multi-attach-', dir=''),
        'n_items': _default_n_items,
    }

WizardAttachmentZip()
