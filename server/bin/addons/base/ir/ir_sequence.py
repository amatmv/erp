# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution	
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    $Id$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import time
from osv import fields,osv
import pooler

class ir_sequence_type(osv.osv):
    _name = 'ir.sequence.type'
    _columns = {
        'name': fields.char('Sequence Name',size=64, required=True),
        'code': fields.char('Sequence Code',size=64, required=True),
    }

    _sql_constraints = [
        ('code_unique', 'unique(code)', '`code` must be unique.'),
    ]

ir_sequence_type()



def _code_get(self, cr, uid, context=None):
    cr.execute('select code, name from ir_sequence_type')
    return cr.fetchall()

class ir_sequence(osv.osv):
    _name = 'ir.sequence'
    _columns = {
        'name': fields.char('Sequence Name',size=64, required=True),
        'code': fields.selection(
            _code_get,
            'Sequence Code',
            size=64,
            required=True,
            select=1
        ),
        'implementation': fields.selection(
            [('standard', 'Standard'), ('no_gap', 'No gap')],
            'Implementation', required=True,
            help="Two sequence object implementations are offered: Standard "
            "and 'No gap'. The later is slower than the former but forbids any"
            " gap in the sequence (while they are possible in the former)."),
        'active': fields.boolean('Active'),
        'prefix': fields.char('Prefix',size=64),
        'suffix': fields.char('Suffix',size=64),
        'number_next': fields.integer('Next Number', required=True),
        'number_increment': fields.integer('Increment Number', required=True),
        'padding' : fields.integer('Number padding', required=True),
    }
    _defaults = {
        'implementation': lambda *a: 'no_gap',
        'active': lambda *a: True,
        'number_increment': lambda *a: 1,
        'number_next': lambda *a: 1,
        'padding': lambda *a : 0,
    }

    def _create_sequence(self, cr, id, number_increment, number_next):
        """ Create a PostreSQL sequence.

        There is no access rights check.
        """
        assert isinstance(id, (int, long))
        sql = "CREATE SEQUENCE ir_sequence_%03d INCREMENT BY %%s START WITH %%s" % id
        cr.execute(sql, (number_increment, number_next))

    def _process(self, s):
        return (s or '') % {
            'year':time.strftime('%Y'), 
            'month': time.strftime('%m'), 
            'day':time.strftime('%d'),
            'y': time.strftime('%y'),
            'doy': time.strftime('%j'),
            'woy': time.strftime('%W'),
            'weekday': time.strftime('%w'),
            'h24': time.strftime('%H'),
            'h12': time.strftime('%I'),
            'min': time.strftime('%M'),
            'sec': time.strftime('%S'),
        }

    def _drop_sequence(self, cr, ids):
        """ Drop the PostreSQL sequence if it exists.

        There is no access rights check.
        """

        ids = ids if isinstance(ids, (list, tuple)) else [ids]
        assert all(isinstance(i, (int, long)) for i in ids), \
            "Only ids in (int, long) allowed."
        names = ','.join('ir_sequence_%03d' % i for i in ids)

        # RESTRICT is the default; it prevents dropping the sequence if an
        # object depends on it.
        cr.execute("DROP SEQUENCE IF EXISTS %s RESTRICT " % names)

    def _alter_sequence(self, cr, id, number_increment, number_next):
        """ Alter a PostreSQL sequence.

        There is no access rights check.
        """
        assert isinstance(id, (int, long))
        cr.execute("""
            ALTER SEQUENCE ir_sequence_%03d INCREMENT BY %%s RESTART WITH %%s
            """ % id, (number_increment, number_next))

    def create(self, cr, uid, values, context=None):
        """ Create a sequence, in implementation == standard a fast gaps-allowed PostgreSQL sequence is used.
        """
        values['id'] = super(ir_sequence, self).create(cr, uid, values, context)
        _default_implementation = self._defaults['implementation']()
        if values.get('implementation', _default_implementation) == 'standard':
            self._create_sequence(
                cr, values['id'], values['number_increment'], values['number_next']
            )
        return values['id']

    def unlink(self, cr, uid, ids, context=None):
        super(ir_sequence, self).unlink(cr, uid, ids, context)
        self._drop_sequence(cr, ids)
        return True

    def write(self, cr, uid, ids, values, context=None):
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        new_implementation = values.get('implementation')
        rows = self.read(cr, uid, ids, ['implementation', 'number_increment', 'number_next'], context)
        super(ir_sequence, self).write(cr, uid, ids, values, context)

        for row in rows:
            # 4 cases: we test the previous impl. against the new one.
            i = values.get('number_increment', row['number_increment'])
            n = values.get('number_next', row['number_next'])
            if row['implementation'] == 'standard':
                if new_implementation in ('standard', None):
                    self._alter_sequence(cr, row['id'], i, n)
                else:
                    self._drop_sequence(cr, row['id'])
            else:
                if new_implementation in ('no_gap', None):
                    pass
                else:
                    self._create_sequence(cr, row['id'], i, n)

        return True

    def _interpolate(self, s, d):
        if s:
            return s % d
        return  ''

    def _interpolation_dict(self):
        t = time.localtime() # Actually, the server is always in UTC.
        return {
            'year': time.strftime('%Y', t),
            'month': time.strftime('%m', t),
            'day': time.strftime('%d', t),
            'y': time.strftime('%y', t),
            'doy': time.strftime('%j', t),
            'woy': time.strftime('%W', t),
            'weekday': time.strftime('%w', t),
            'h24': time.strftime('%H', t),
            'h12': time.strftime('%I', t),
            'min': time.strftime('%M', t),
            'sec': time.strftime('%S', t),
        }

    def _next(self, cr, uid, seq_ids, context=None):
        if not seq_ids:
            return False
        if context is None:
            context = {}
        sequences = self.read(cr, uid, seq_ids, ['implementation', 'prefix', 'suffix', 'padding'])
        seq = sequences[0]
        if seq['implementation'] == 'standard':
            cr.execute("SELECT nextval('ir_sequence_%03d')" % seq['id'])
            seq['number_next'] = cr.fetchone()[0]
        else:
            cr.execute("SELECT number_next FROM ir_sequence WHERE id=%s FOR UPDATE", (seq['id'],))
            seq['number_next'] = cr.fetchone()[0]
            cr.execute("UPDATE ir_sequence SET number_next=number_next+number_increment WHERE id=%s ", (seq['id'],))
        d = self._interpolation_dict()
        interpolated_prefix = self._interpolate(seq['prefix'], d)
        interpolated_suffix = self._interpolate(seq['suffix'], d)
        return interpolated_prefix + '%%0%sd' % seq['padding'] % seq['number_next'] + interpolated_suffix

    def next_by_id(self, cr, uid, sequence_id, context=None):
        """ Draw an interpolated string using the specified sequence."""
        return self._next(cr, uid, [sequence_id], context)

    def next_by_code(self, cr, uid, sequence_code, context=None):
        """ Draw an interpolated string using a sequence with the requested code.
        """
        ids = self.search(cr, uid, [('code','=', sequence_code)])
        return self._next(cr, uid, ids, context)

    def get_id(self, cr, uid, sequence_code_or_id, test='id=%s', context=None):
        """ Draw an interpolated string using the specified sequence.

        The sequence to use is specified by the ``sequence_code_or_id``
        argument, which can be a code or an id (as controlled by the
        ``code_or_id`` argument. This method is deprecated.
        """
        if test not in ('id=%s', 'code=%s', 'id', 'code'):
            raise ValueError('invalid test')
        if test.startswith('id'):
            return self.next_by_id(cr, uid, sequence_code_or_id, context)
        else:
            return self.next_by_code(cr, uid, sequence_code_or_id, context)

    def get(self, cr, uid, code, context=None):
        """ Draw an interpolated string using the specified sequence.

        The sequence to use is specified by its code. This method is
        deprecated.
        """
        return self.get_id(cr, uid, code, 'code=%s', context)

    def get_next(self, cursor, uid, code):
        return self.get(cursor, uid, code)

ir_sequence()


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

