# -*- encoding: utf-8 -*-

from osv import osv, fields
from hashlib import sha512
from os.path import join
from addons import get_module_resource


class IrFile(osv.osv):

    _name = 'ir.file'

    def get_checksum(self, cursor, uid, module, filename, context=None):

        with open(get_module_resource(module, filename), 'r') as f:
            checksum = sha512(f.read()).hexdigest()

        return checksum

    def has_changes(self, cursor, uid, module, filename, context=None):

        search_params = [('module_name', '=', module),
                         ('file_name', '=', filename)]
        file_ids = self.search(cursor, uid, search_params)

        checksum = self.get_checksum(cursor, uid, module, filename, context)

        if file_ids:
            stored_checksum = self.read(cursor, uid, file_ids[0],
                                        ['checksum'])['checksum']
            if stored_checksum == checksum:
                return False
            else:
                self.write(cursor, uid, file_ids, {'checksum': checksum})
        else:
            vals = {
                'module_name': module,
                'file_name': filename,
                'checksum': checksum,
            }
            self.create(cursor, uid, vals)

        return True

    _columns = {
        'module_name': fields.char('Module', size=100),
        'file_name': fields.char('File', size=100),
        'checksum': fields.char('Checksum', size=200)
    }

IrFile()
