# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution	
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    $Id$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import wizard
import tools
import base64
import cStringIO
import csv
import pooler
from osv import fields,osv
from tools.translate import _
import tempfile
import shutil


class wizard_export_lang(osv.osv_memory):

    class TempDir(object):
        def __init__(self):
            self.dir = tempfile.mkdtemp()

        def __enter__(self):
            return self

        def __exit__(self, exc_type, exc_val, exc_tb):
            shutil.rmtree(self.dir)

    @staticmethod
    def read_pot_buffer(pot_buffer):
        """
        Read a Po file
        :param po_path: .po path
        :return: po content
        """
        from babel.messages import pofile
        # We need to replace model lines for user comments because read_po
        # requires lineno as INT, and model comment uses a STR
        read_buff = cStringIO.StringIO()
        pot_buffer.seek(0)
        for line in pot_buffer:
            if line.startswith('#: model:'):
                line = line.replace('#: model:', '#. model:')
            read_buff.write(line)
        read_buff.seek(0)
        catalog = pofile.read_po(read_buff)
        return catalog

    @staticmethod
    def read_babel_conf(babel_conf_path):
        with open(babel_conf_path, 'r') as babel_conf:
            confs = babel_conf.read()
        methods = []
        options = {}
        last_prefix = False
        for line in confs.split('\n'):
            if line.startswith('[') and line.endswith(']'):
                method, prefix = line[1:-1].split(':')
                methods.append((method, prefix[1:]))
                last_prefix = prefix[1:]
                options.update({prefix[1:]: {}})
            else:
                if not last_prefix or line == '':
                    continue
                option_name, option_value = line.split('=')
                options.get(last_prefix).update({
                    option_name[:-1]: option_value[1:]
                })
        return methods, options

    @staticmethod
    def append_report_translations(cursor, uid, mods, exp_format, module_buf):
        from tools import config
        from os.path import join, isdir, isfile, exists
        from babel.messages.pofile import write_po
        from babel.messages import extract, Catalog
        import imp
        import pkg_resources
        import logging
        logger = logging.getLogger()

        if len(mods) > 1:
            logger.warning(
                _('Not exporting MAKO report strings for more than 1 module')
            )
            return module_buf
        if exp_format != 'pot':
            logger.warning(
                _('Not exporting MAKO report strings without "po" format')
            )
            return module_buf
        imp.reload(pkg_resources)
        module_name = mods[0]
        filename = '{}.pot'.format(module_name)
        module_path = join(config['addons_path'], module_name)
        i18n_path = join(module_path, 'i18n')
        module_trans_path = join(i18n_path, filename)
        babel_path = join(i18n_path, 'babel.cfg')
        report_path = join(module_path, 'report')
        if not isdir(report_path):
            logger.warning(
                _('No reports on module {}, '
                  'skipping report translations...'.format(module_name))
            )
            return module_buf

        # Exporting report strings
        logger.info('Exporting report strings for module {}'.format(
            module_name
        ))

        if not isfile(babel_path):
            # If <module_path>/i18n doesn't exist, we either cannot create
            # the file <module_path>/i18n/babel.cfg.
            if not exists(i18n_path):
                raise osv.except_osv(
                    _(u'Error'), _(u'El directori i18n no existeix.')
                )

            logger.warning(
                _('No babel.cfg found in module {},'
                  ' initializing for mako reports'.format(module_name))
            )
            with open(babel_path, 'w') as babel_conf:
                babel_conf.write('[mako: **.mako]\n'
                                 'input_encoding = utf-8\n'
                                 'encoding = utf-8\n')
        methods_map, options_map = wizard_export_lang.read_babel_conf(
            babel_path)
        messages = extract.extract_from_dir(
            report_path, method_map=[('**.mako', 'mako')],
            options_map={'**.mako': {
                'input_encoding': 'utf-8',
                'encoding': 'utf-8'
            }}
        )
        report_trans = Catalog()
        for msg in messages:
            file_name = msg[0]
            lineno = msg[1]
            location = 'rml:{module_name}/report/{file_name}'.format(**locals())
            location = (location, lineno)
            msgA = report_trans.get(msg[2])
            if not msgA:
                report_trans.add(id=msg[2], locations=[location])
            else:
                msgA.locations.append(location)

        # Adding missing strings to module catalog
        module_trans = wizard_export_lang.read_pot_buffer(module_buf)
        for msg in report_trans:
            if msg.id == '':
                continue
            msgB = module_trans.get(msg.id)
            if not msgB:
                module_trans.add(id=msg.id, locations=msg.locations)
            else:
                for location in msg.locations:
                    msgB.locations.append(location)

        # Exporting new catalog to buffer
        # TODO: Export with "wizard_export_lang.write_pot_buffer(c: CATALOG)"
        new_buffer = cStringIO.StringIO()
        write_po(new_buffer, module_trans)
        new_buffer.seek(0)
        return_buffer = cStringIO.StringIO()
        # Review all lines of catalog export to:
        # - Split multiple definition in the same lines
        # - Add line number on OpenERP comments
        # - Set Content-Transfer-Encoding without '8bit'
        #
        # These are the keys that OpenERP creates and must be reviewed
        # - "view:"
        # - "selection:"
        # - "field:"
        duplicated_comment_keys = [
            'wizard_field:', 'wizard_button:', 'wizard_view:', 'xsl:', 'rml:',
            'constraint:', 'view:', 'selection:', 'field:', 'help:'
        ]
        for next_string in new_buffer:
            if next_string.startswith('#: '):
                # For each key
                for key in duplicated_comment_keys:
                    # We generate a list of the definitions splitting them,
                    # in order to write them into sepparate lines.
                    split_definitions = []
                    for definition in next_string[:-1].split(' ')[1:]:
                        for definition_key in duplicated_comment_keys:
                            if definition.startswith(definition_key):
                                split_definitions.append(definition)

                    if len(split_definitions) >= 2:
                        next_string = '\n'.join(
                            [
                                # For each definition, include:
                                # - The comment tag '#:'
                                # - The definition itself, with its type
                                # - The line number ':0'
                                '#: {}:0'.format(definition)
                                for definition in split_definitions
                            ]
                        )

                        next_string += '\n'
                        break
                    elif key in next_string:
                        # If it's not a multiple definition line but contains
                        # the key, it'll not have the line number, so we add it
                        try:
                            # Check if already have the number
                            int(next_string.split(':')[-1])
                        except:
                            next_string = '{}:0\n'.format(next_string[:-1])
                        break
                return_buffer.write(next_string)
            elif next_string.startswith('#. model:'):
                next_string = next_string.replace('#. model:', '#: model:')
                return_buffer.write(next_string)
            elif '"Content-Transfer-Encoding: 8bit' in next_string:
                # If the line is like the header,
                #   replace it with the correct header
                return_buffer.write(
                    '"Content-Transfer-Encoding: \\n"\n')
            else:
                # All other lines may remain the same
                return_buffer.write(next_string)
        return return_buffer

    def _get_languages(self, cr, uid, context):
        lang_obj=pooler.get_pool(cr.dbname).get('res.lang')
        ids=lang_obj.search(cr, uid, ['&', ('active', '=', True), ('translatable', '=', True),])
        langs=lang_obj.browse(cr, uid, ids)
        return [(lang.code, lang.name) for lang in langs]
    

    def act_cancel(self, cr, uid, ids, context=None):
        #self.unlink(cr, uid, ids, context)
        return {'type':'ir.actions.act_window_close' }

    def act_destroy(self, *args):
        return {'type':'ir.actions.act_window_close' }

    def act_getfile(self, cr, uid, ids, context=None):
        this = self.browse(cr, uid, ids)[0]
        mods = map(lambda m: m.name, this.modules) or ['all']
        mods.sort()
        buf=cStringIO.StringIO()
        tools.trans_export(this.lang, mods, buf, this.format, dbname=cr.dbname)
        if this.format == 'csv':
            this.advice = _("Save this document to a .CSV file and open it with your favourite spreadsheet software. The file encoding is UTF-8. You have to translate the latest column before reimporting it.")
        elif this.format == 'po':
            if not this.lang:
                this.format = 'pot'
            this.advice = _("Save this document to a %s file and edit it with a specific software or a text editor. The file encoding is UTF-8.") % ('.'+this.format,)
        elif this.format == 'tgz':
            ext = this.lang and '.po' or '.pot'
            this.advice = _('Save this document to a .tgz file. This archive containt UTF-8 %s files and may be uploaded to launchpad.') % (ext,)
        filename = _('new')
        if not this.lang and len(mods) == 1:
            filename = mods[0]
        this.name = "%s.%s" % (this.lang or filename, this.format)
        buf = wizard_export_lang.append_report_translations(
            cr, uid, mods, this.format, buf)
        out=base64.encodestring(buf.getvalue())
        buf.close()
        return self.write(cr, uid, ids, {'state':'get', 'data':out, 'advice':this.advice, 'name':this.name}, context=context)

    _name = "wizard.module.lang.export"
    _columns = {
            'name': fields.char('Filename', 16, readonly=True),
            'lang': fields.selection(_get_languages, 'Language', help='To export a new language, do not select a language.'), # not required: unset = new language
            'format': fields.selection( ( ('csv','CSV File'), ('po','PO File'), ('tgz', 'TGZ Archive')), 'File Format', required=True),
            'modules': fields.many2many('ir.module.module', 'rel_modules_langexport', 'wiz_id', 'module_id', 'Modules', domain=[('state','=','installed')]),
            'data': fields.binary('File', readonly=True),
            'advice': fields.text('Advice', readonly=True),
            'state': fields.selection( ( ('choose','choose'),   # choose language
                                         ('get','get'),         # get the file
                                       ) ),
            }
    _defaults = { 'state': lambda *a: 'choose', 
                }
wizard_export_lang()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

