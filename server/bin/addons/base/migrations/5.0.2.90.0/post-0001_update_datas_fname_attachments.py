# coding=utf-8
import logging

def up(cursor, installed_version):

    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')
    logger.info('Updating fname attachments')

    query = "UPDATE ir_attachment SET datas_fname=name " \
            "WHERE datas_fname is NULL"
    cursor.execute(query)

def down(cursor, installed_version):
    pass

migrate = up
