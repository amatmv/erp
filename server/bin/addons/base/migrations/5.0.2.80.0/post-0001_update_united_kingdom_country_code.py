# coding=utf-8

import logging


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')

    logger.info('Updating United Kingdom country code to "GB"')

    query = '''
        UPDATE res_country
        SET code = 'GB'
        WHERE name = 'United Kingdom' AND code = 'UK'
    '''

    cursor.execute(query)


def down(cursor, installed_version):
    pass


migrate = up
