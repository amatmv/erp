# coding=utf-8

import logging


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')

    logger.info('Change size of field "name" of the model ir.actions.')
    cursor.execute("ALTER TABLE ir_actions ALTER COLUMN name TYPE character varying(128);")

    logger.info('Migration successful!')


def down(cursor, installed_version):
    pass


migrate = up
