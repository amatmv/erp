# -*- coding: utf-8 -*-

import pooler
from gisceupgrade_migratoor.gisceupgrade_migratoor import log

NEW_MODULES_DEPS = {
    'base_extended': ['base_extended_distri'],
    'giscedata_polissa': ['giscedata_polissa_distri'],
    'giscedata_lectures': ['giscedata_facturacio',
                           'giscedata_facturacio_distri',
                           'giscedata_lectures_distri_full_lectures'],
    'giscedata_cups': ['giscedata_cups_distri'],
    'giscedata_contractes_acces': ['giscedata_contractacio_distri']
}

def migrate(cursor, installed_version):
    if not installed_version or not installed_version.startswith("4.2"):
        return
    uid = 1
    log("Searching new modules deps...")
    mod_obj = pooler.get_pool(cursor.dbname).get('ir.module.module')
    mod_obj.update_list(cursor, 1)
    for module, deps in NEW_MODULES_DEPS.items():
        log("Searching uninstalled deps for module %s" % module)
        search_params = [('name', '=', module), ('state', '!=', 'uninstalled')]
        if not mod_obj.search_count(cursor, uid, search_params):
            continue
        search_params = [('name', 'in', deps), ('state', '=', 'uninstalled')]
        mod_ids = mod_obj.search(cursor, uid, search_params)
        if not mod_ids:
            continue
        for mod_to_install in mod_obj.browse(cursor, uid, mod_ids):
            log("Marking to install dep %s" % mod_to_install.name)
            mod_to_install.button_install()

    # Eliminem mòduls antics que ara no necesssitem
    search_params = [('state', '=', 'old_installed')]
    del_ids = mod_obj.search(cursor, uid, search_params,
                             context={'active_test': False})
    mod_obj.unlink(cursor, uid, del_ids)

up = migrate
