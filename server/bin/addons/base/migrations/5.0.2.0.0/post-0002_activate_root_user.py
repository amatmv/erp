# -*- coding: utf-8 -*-

import pooler
from gisceupgrade_migratoor.gisceupgrade_migratoor import log

def migrate(cursor, installed_version):
    if not installed_version or not installed_version.startswith("4.2"):
        return
    uid = 1
    pool = pooler.get_pool(cursor.dbname)
    log("Activant usuari root amb la contrassenya d'admin")
    imd_obj = pool.get('ir.model.data')
    imd_id = imd_obj._get_id(cursor, uid, 'base', 'user_root')
    usr_id = imd_obj.read(cursor, uid, imd_id, ['res_id'])['res_id']
    usr_obj = pool.get('res.users')
    pwd = usr_obj.search_reader(cursor, uid, [('login', '=', 'admin')],
                              ['password'])[0]['password']
    usr_obj.write(cursor, uid, usr_id, {'active': 1, 'password': pwd})

up = migrate
