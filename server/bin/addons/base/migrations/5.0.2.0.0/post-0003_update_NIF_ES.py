# -*- coding: utf-8 -*-

import pooler
from gisceupgrade_migratoor.gisceupgrade_migratoor import log

def mult_add(i, j):
    """Sum each digits of the multiplication of i and j."""
    return reduce(lambda x, y: x + int(y), str(i*j), 0)
def check_vat_es(self, vat):
        '''
        Check Spain VAT number.
        '''
        if len(vat) != 9:
            return False

        conv = {
            1: 'T',
            2: 'R',
            3: 'W',
            4: 'A',
            5: 'G',
            6: 'M',
            7: 'Y',
            8: 'F',
            9: 'P',
            10: 'D',
            11: 'X',
            12: 'B',
            13: 'N',
            14: 'J',
            15: 'Z',
            16: 'S',
            17: 'Q',
            18: 'V',
            19: 'H',
            20: 'L',
            21: 'C',
            22: 'K',
            23: 'E',
        }
        #Legal persons with profit aim
        if vat[0] in ('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'U', 'V'):
            try:
                int(vat[1:8])
            except:
                return False
            sum = mult_add(2, int(vat[1])) + int(vat[2]) + \
                    mult_add(2, int(vat[3])) + int(vat[4]) + \
                    mult_add(2, int(vat[5])) + int(vat[6]) + \
                    mult_add(2, int(vat[7]))
            check = 10 - (sum % 10)
            if check == 10:
                check = 0
#            if check != int(vat[8]):
#                return False
            return True
        #Legal persons with non-profit aim
        elif vat[0] in ('N', 'P', 'Q', 'R', 'S', 'W'):
            try:
                int(vat[1:8])
            except:
                return False
            sum = mult_add(2, int(vat[1])) + int(vat[2]) + \
                    mult_add(2, int(vat[3])) + int(vat[4]) + \
                    mult_add(2, int(vat[5])) + int(vat[6]) + \
                    mult_add(2, int(vat[7]))
            check = 10 - (sum % 10)
            check = chr(check + 64)
            if check != vat[8]:
                return False
            return True
        #Foreign natural persons, under age 14 or non-residents
        elif vat[0] in ('K', 'L', 'M', 'X', 'Y', 'Z'):
            if vat[0] == 'Y':
                check_value = '1' + vat[1:8]
            elif vat[0] == 'Z':
                check_value = '2' + vat[1:8]
            else:
                check_value = vat[1:8]

            try:
                int(check_value)
            except:
                return False
            check = 1 + (int(check_value) % 23)

            check = conv[check]
            if check != vat[8]:
                return False
            return True
        #Spanish natural persons
        else:
            try:
                int(vat[:8])
            except:
                return False
            check = 1 + (int(vat[:8]) % 23)

            check = conv[check]
            if check != vat[8]:
                return False
            return True

def migrate(cursor, installed_version):
    if not installed_version or not installed_version.startswith("4.2"):
        return
    uid = 1
    pool = pooler.get_pool(cursor.dbname)
    partner_obj = pool.get('res.partner')
    partner_ids = partner_obj.search(cursor, uid, [
        ('vat', '!=', False),
        ('vat', 'not like', 'ES%')
    ], context={'active_test': False})
    log("Actualitzant NIFs %s a ES..." % len(partner_ids))
    ok = 0
    ko = 0
    for partner in partner_obj.browse(cursor, uid, partner_ids):
        vat = ''.join([x for x in partner.vat.upper() if x.isalnum()])
        try:
            try:
                if check_vat_es(vat):
                   partner.write({'vat': 'ES%s' % vat})
                   ok += 1
                else:
                   log(u"Partner else: %s vat ES%s no vàlid" % (partner.id,vat))
                   ko += 1
            except Exception, e:
                partner.write({'vat': 'ES%s' % vat})
                ok += 1
        except Exception, e:
            log(u"Partner except: %s vat ES%s no vàlid"
                % (partner.id, vat))
            ko += 1
    log("%s partners arreglats, han fallat %s" % (ok, ko))

up = migrate
