# -*- coding: utf-8 -*-

import netsvc
import pooler
from addons import get_module_path
from addons.gisceupgrade_migratoor.gisceupgrade_migratoor import log

IGNORE_MODULES = ['giscedata_contractes_acces']

def migrate(cursor, installed_version):
    uid = 1
    pool = pooler.get_pool(cursor.dbname)
    imd_obj = pool.get('ir.model.data')
    logger = netsvc.Logger()
    cursor.execute("select id,name from ir_module_module")
    for mod_id, mod_name in cursor.fetchall():
        if not get_module_path(mod_name):
            if mod_name in IGNORE_MODULES:
                cursor.execute("update ir_module_module set "
                               "state = 'old_installed' where name = %s",
                               (mod_name,))
                continue
            else:
                logger.notifyChannel('migracio', netsvc.LOG_INFO,
                                 'Removing unknown module: %s' % mod_name)
                cursor.execute("delete from ir_module_module where id = %s",
                               (mod_id,))
            imd_ids = imd_obj.search(cursor, uid, [('module', '=', mod_name)])
            for imd in imd_obj.read(cursor, uid, imd_ids, ['model', 'res_id']):
                log("Eliminant contingut del %s %s(id:%s)" % (mod_name,
                                                              imd['model'],
                                                              imd['res_id']))
                table = imd['model'].replace('.', '_')
                savepoint = "del_%s_%s" % (table, imd['res_id'])
                cursor.savepoint(savepoint)
                try:
                    cursor.execute("delete from %s where id = %s"
                                   % (table, imd['res_id']))
                except Exception:
                    cursor.rollback(savepoint)
                finally:
                    cursor.release(savepoint)

up = migrate
