# -*- coding: utf-8 -*-

"""Migració del mòdul base
"""

from addons.gisceupgrade_migratoor.gisceupgrade_migratoor import GisceUpgradeMigratoor
import netsvc

def migrate(cursor, installed_version):
    """Canvis a executar relatius al mòdul base
    """
    if not installed_version or not installed_version.startswith("4.2"):
        return

    logger = netsvc.Logger()
    logger.notifyChannel('migration', netsvc.LOG_INFO, 'Migration from %s'
                         % installed_version)
    # Canvis de columnes
    cursor.execute("""ALTER TABLE ir_model
    DROP COLUMN perm_id,
    ADD COLUMN "state" character varying(16),
    ALTER COLUMN name SET DEFAULT '',
    ALTER COLUMN name SET NOT NULL""")

    cursor.execute("""ALTER TABLE ir_model_fields
    DROP COLUMN perm_id,
    DROP COLUMN group_name,
    ADD COLUMN select_level character varying(4) DEFAULT '' NOT NULL,
    ADD COLUMN "state" character varying(64) DEFAULT 'base'::character varying NOT NULL,
    ADD COLUMN relation_field character varying(128),
    ADD COLUMN "domain" character varying(256),
    ADD COLUMN selection character varying(128),
    ADD COLUMN on_delete character varying(16),
    ADD COLUMN "size" integer,
    ADD COLUMN required boolean,
    ADD COLUMN readonly boolean,
    ADD COLUMN "translate" boolean,
    ADD COLUMN complete_name character varying(64),
    ALTER COLUMN name SET DEFAULT '',
    ALTER COLUMN name SET NOT NULL,
    ALTER COLUMN field_description SET DEFAULT '',
    ALTER COLUMN field_description SET NOT NULL,
    ALTER COLUMN ttype SET DEFAULT '',
    ALTER COLUMN ttype SET NOT NULL""")

    cursor.execute("""ALTER TABLE ir_model_fields
    ALTER COLUMN select_level DROP DEFAULT,
    ALTER COLUMN "state" DROP DEFAULT,
    ALTER COLUMN name DROP DEFAULT,
    ALTER COLUMN field_description DROP DEFAULT,
    ALTER COLUMN ttype DROP DEFAULT""")

    # res_users
    cursor.execute("""ALTER TABLE res_users
    DROP COLUMN perm_id,
    DROP COLUMN perm_default,
    ADD COLUMN context_tz character varying(64) DEFAULT NULL::character varying,
    ADD COLUMN context_lang character varying(64) DEFAULT ''::character varying NOT NULL,
    ADD COLUMN create_uid integer,
    ADD COLUMN create_date timestamp without time zone,
    ADD COLUMN write_date timestamp without time zone,
    ADD COLUMN write_uid integer,
    ALTER COLUMN password SET DEFAULT NULL::character varying""")

    cursor.execute("""ALTER TABLE res_users
    ALTER COLUMN context_lang DROP DEFAULT""")

    cursor.execute("""ALTER TABLE ir_act_window
    ADD COLUMN target character varying(16)""")

    # res_company
    cursor.execute("ALTER TABLE res_company RENAME COLUMN rml_header1 TO "
                   "rml_header")

    # USERS
    cursor.execute("insert into ir_model_data "
                   "(name, module, model, noupdate, res_id) "
                   "values ('user_root', 'base', 'res.users', True, 1)")
    cursor.execute("insert into ir_model_data "
                   "(name, module, model, noupdate, res_id) values "
                   "('user_admin', 'base', 'res.users', True, 1)")

    cursor.execute("DELETE FROM ir_module_repository")

    # backup
    mig = GisceUpgradeMigratoor('base', cursor)
    mig.backup(suffix='v4')
    mig.pre_xml()


up = migrate
