# coding=utf-8
from .test_models import *
from .test_extended_view import *
from .test_ooquery import *
from .test_model_access import *
from .test_orm import *
from .test_fields import *
from .test_timescale import *

from unittest import skip

from destral import testing
from destral.transaction import Transaction
from osv import osv, fields
from osv.orm import AccessError
from tools import config

from lxml import etree

from base.res.partner.tests import *
import base64

class TestExtendedView(testing.OOTestCase):

    def tearDown(self):
        from base.res.partner import partner
        partner.res_partner()
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            osv.class_pool['res.partner'].createInstance(
                self.openerp.pool, 'base', cursor
            )

    def test_extended_view(self):
        pool = self.openerp.pool
        ui = pool.get('ir.ui.view')
        imd = pool.get('ir.model.data')
        partner_obj = pool.get('res.partner')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            ResPartnerExtended()
            osv.class_pool['res.partner.extended'].createInstance(
                self.openerp.pool, 'base', cursor
            )
            partner_extended = self.openerp.pool.get('res.partner.extended')
            partner_extended._auto_init(cursor)

            base_id = imd.get_object_reference(cursor, uid,
                'base', 'view_partner_form'
            )[1]

            view_id = ui.create(cursor, uid, {
                'name': 'res.partner.extended',
                'model': 'res.partner.extended',
                'type': 'form',
                'inherit_id': base_id,
                'extend': True,
                'arch': """
                    <xpath expr="//field[@name='name']" position="after">
                        <field name="tested"/>
                    </xpath>
                """
            })

            ui.create(cursor, uid, {
                'name': 'res.partner.extended2',
                'model': 'res.partner.extended',
                'type': 'form',
                'inherit_id': view_id,
                'arch': """
                    <xpath expr="//field[@name='ref']" position="attributes">
                        <attribute name="required">1</attribute>
                    </xpath>
                """
            })

            partner_xml = etree.fromstring(
                partner_obj.fields_view_get(cursor, uid, base_id)['arch']
            )
            partner_ext_xml = etree.fromstring(
                partner_extended.fields_view_get(cursor, uid, view_id)['arch']
            )

            self.assertEqual(
                partner_xml.xpath("//field[@name='tested']"),
                []
            )

            element = partner_ext_xml.xpath("//field[@name='tested']")[0]
            self.assertEqual(element.tag, 'field')
            self.assertEqual(element.attrib, {'name': 'tested'})

            element = partner_ext_xml.xpath("//field[@name='ref']")[0]
            self.assertIn('required', element.attrib)

            ResPartnerModuleExt()
            osv.class_pool['res.partner'].createInstance(
                self.openerp.pool, 'base', cursor
            )
            partner_obj = self.openerp.pool.get('res.partner')
            partner_obj._auto_init(cursor)

            ui.create(cursor, uid, {
                'name': 'res.partner.form2',
                'model': 'res.partner',
                'type': 'form',
                'inherit_id': base_id,
                'arch': """
                    <xpath expr="//field[@name='ref']" position="after">
                        <field name="added_field"/>
                    </xpath>
                """
            })

            partner_xml = etree.fromstring(
                partner_obj.fields_view_get(cursor, uid, base_id)['arch']
            )
            partner_ext_xml = etree.fromstring(
                partner_extended.fields_view_get(cursor, uid, view_id)['arch']
            )
            element = partner_xml.xpath("//field[@name='added_field']")[0]
            self.assertEqual(element.attrib, {'name': 'added_field'})

            element = partner_ext_xml.xpath("//field[@name='added_field']")[0]
            self.assertEqual(element.attrib, {'name': 'added_field'})

            extend_id = ui.create(cursor, uid, {
                'name': 'res.partner2',
                'model': 'res.partner',
                'type': 'form',
                'inherit_id': base_id,
                'extend': True,
                'arch': """
                    <xpath expr="//field[@name='ref']" position="after">
                        <field name="custom_added_field"/>
                    </xpath>
                """
            })

            partner_xml = etree.fromstring(
                partner_obj.fields_view_get(cursor, uid, base_id)['arch']
            )

            self.assertEqual(
                partner_xml.xpath("//field[@name='custom_added_field']"),
                []
            )

            partner2_xml = etree.fromstring(
                partner_obj.fields_view_get(cursor, uid, extend_id)['arch']
            )

            self.assertEqual(
                partner2_xml.xpath("//field[@name='custom_added_field']")[0].attrib,
                {'name': 'custom_added_field'}
            )


class TestIrModelAccess(testing.OOTestCase):

    def test_throws_access_error_exception(self):
        pool = self.openerp.pool
        imdg = pool.get('ir.model.grid')
        user_obj = pool.get('res.users')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            new_user = user_obj.create(cursor, uid,
                {
                    'name': 'test uset',
                    'login': 'test',
                    'password': 'test_password'
                }
            )
            try:
                ids = imdg.search(cursor, new_user, [])
            except Exception as e:
                assert isinstance(e, AccessError)


class TestTranslationsNoUpdate(testing.OOTestCase):

    def test_translated_when_noupdate_only_first_time(self):
        """
        Aquest test comprova que en un NOUPDATE=1 al carregar traduccions per primer cop es creen les
        traduccions pero que al carregar-les per segon cop no modifica res
        """
        pool = self.openerp.pool
        country_obj = pool.get('res.country')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            country_id = country_obj.search(cursor, uid, [('code', '=', 'AD')])
            # instalar idioma
            modobj = pool.get('ir.module.module')
            mids = modobj.search(cursor, uid, [('state', '=', 'installed')])
            modobj.update_translations(cursor, uid, mids, 'es_ES')
            # llegir traduccio original
            original = country_obj.read(
                cursor, uid, country_id, ['name'], {'lang': 'en_US'}
            )[0]['name']
            from tools import convert_xml_import
            from addons import get_module_resource
            convert_xml_import(
                cursor, 'base', get_module_resource('base', 'base_data.xml'),
                None, 'update', noupdate=True
            )
            new = country_obj.read(
                cursor, uid, country_id, ['name'], {'lang': 'es_ES'}
            )[0]['name']
            self.assertNotEqual(new, original)
            # Ara canviem el valor en español i al tornar a carregar traduccions ens ha de mantenir el valor posat
            traduccio_o = pool.get('ir.translation')
            tid = traduccio_o.search(cursor, uid, [('src', '=', original), ('lang', '=', 'es_ES')])
            traduccio_o.write(cursor, uid, tid, {'value': u"Andorra una grande i libre"})
            from tools.misc import cache
            cache.clean_caches_for_db(cursor.dbname)
            new_2 = country_obj.read(
                cursor, uid, country_id, ['name'], {'lang': 'es_ES'}
            )[0]['name']
            self.assertEqual(new_2, u"Andorra una grande i libre")
            convert_xml_import(
                cursor, 'base', get_module_resource('base', 'base_data.xml'),
                None, 'update', noupdate=True
            )
            modobj.update_translations(cursor, uid, mids, 'es_ES')
            cache.clean_caches_for_db(cursor.dbname)
            new_3 = country_obj.read(
                cursor, uid, country_id, ['name'], {'lang': 'es_ES'}
            )[0]['name']
            self.assertEqual(new_2, new_3)



class TestAutoIndex(testing.OOTestCase):

    def tearDown(self):
        from base.res.partner import partner
        partner.res_partner()
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            osv.class_pool['res.partner'].createInstance(
                self.openerp.pool, 'base', cursor
            )

    def test_create_autoindex(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            config['autoindex'] = True

            ResPartnerExtended()
            osv.class_pool['res.partner.extended'].createInstance(
                self.openerp.pool, 'base', cursor
            )
            partner_extended = self.openerp.pool.get('res.partner.extended')
            partner_extended._auto_init(cursor)
            cursor.execute(
                """
                    select t.relname as table_name, i.relname as index_name,
                           am.amname as typeof,
                           array_to_string(array_agg(a.attname), ', ') as column_names
                    from
                        pg_class t, pg_class i, pg_index ix,
                        pg_attribute a, pg_am am
                    where
                        t.oid = ix.indrelid
                        and i.oid = ix.indexrelid
                        and a.attrelid = t.oid
                        and a.attnum = ANY(ix.indkey)
                        and i.relam = am.oid
                        and t.relkind = 'r'
                        and t.relname = %s
                        and i.relname like 'idx_fk%%'
                    group by
                        t.relname, i.relname, am.amname
                    """,
                    ('res_partner_extended',)
            )
            res = cursor.dictfetchall()
            assert (len(res) == 1)

    def test_not_create_autoindex(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            config['autoindex'] = False

            ResPartnerExtended()
            osv.class_pool['res.partner.extended'].createInstance(
                self.openerp.pool, 'base', cursor
            )
            partner_extended = self.openerp.pool.get('res.partner.extended')
            partner_extended._auto_init(cursor)
            cursor.execute(
                """
                    select t.relname as table_name, i.relname as index_name,
                           am.amname as typeof,
                           array_to_string(array_agg(a.attname), ', ') as column_names
                    from
                        pg_class t, pg_class i, pg_index ix,
                        pg_attribute a, pg_am am
                    where
                        t.oid = ix.indrelid
                        and i.oid = ix.indexrelid
                        and a.attrelid = t.oid
                        and a.attnum = ANY(ix.indkey)
                        and i.relam = am.oid
                        and t.relkind = 'r'
                        and t.relname = %s
                        and i.relname like 'idx_fk%%'
                    group by
                        t.relname, i.relname, am.amname
                    """,
                    ('res_partner_extended',)
            )
            res = cursor.dictfetchall()
            assert (len(res) == 0)


class TestReportTranslations(testing.OOTestCase):

    def test_append_translations(self):
        """
        From the base module that hasn't any mako reports but the demo one,
        there should not be any strings but the one exported from the mako file
        > 'Demo String to be Translated'
        This test should use the given IOString (buffer) to append the
        """
        from addons import get_module_resource
        import cStringIO
        demo_pot = get_module_resource(
            'base', 'tests', 'fixtures', 'demo.pot'
        )
        pool = self.openerp.pool
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            export_wizard = pool.get('wizard.module.lang.export')
            with open(demo_pot, 'r') as demo_pot_file:
                pot_buffer = cStringIO.StringIO(demo_pot_file.read())
            original_catalog = export_wizard.read_pot_buffer(pot_buffer)
            with open(demo_pot, 'r') as demo_pot_file:
                pot_buffer = cStringIO.StringIO(demo_pot_file.read())
            new_buffer = export_wizard.append_report_translations(
                cursor=cursor, uid=uid, mods=['base'],
                exp_format='pot', module_buf=pot_buffer
            )
            new_catalog = export_wizard.read_pot_buffer(new_buffer)
            for msgA in original_catalog:
                if msgA.id == '':
                    continue
                msgB = new_catalog.get(msgA.id)
                self.assertTrue(
                    msgB, 'All strings in new catalog should be in old catalog'
                )
            for msgA in new_catalog:
                if msgA.id == '':
                    continue
                msgB = original_catalog.get(msgA.id)
                if not msgB:
                    self.assertEqual(
                        msgA.id, 'Demo String to be Translated',
                        'There should be only one string missing in original'
                        ' Catalog: "Demo String to be Translated"'
                    )
            self.assertTrue(
                new_catalog.get('Demo String to be Translated'),
                'There should be the only translatable string in Mako report:'
                ' "Demo String to be Translated" in the new catalog'
            )

class TestCreateAttachmentWithDatasFname(testing.OOTestCase):

    def setUp(self):
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user

    def tearDown(self):
        self.txn.stop()

    def test_create_attachment_withouth_datas_fname(self):
        attachment_obj = self.openerp.pool.get('ir.attachment')
        values = {
            'name': 'attachment0001',
            'datas': base64.b64encode('attachment test content'),
            'res_model': 'ir.attachment',
            'res_id': 1
        }
        attachment_id = attachment_obj.create(self.cursor, self.uid, values)
        datas_fname = attachment_obj.read(
            self.cursor, self.uid, attachment_id, ['datas_fname']
        )['datas_fname']

        self.assertEqual(datas_fname, values['name'])

    def test_create_attachment_datas_fname_false(self):
        attachment_obj = self.openerp.pool.get('ir.attachment')
        values = {
            'name': 'attachment0002',
            'datas': base64.b64encode('attachment test content'),
            'datas_fname': False,
            'res_model': 'ir.attachment',
            'res_id': 1
        }
        attachment_id = attachment_obj.create(self.cursor, self.uid, values)
        datas_fname = attachment_obj.read(
            self.cursor, self.uid, attachment_id, ['datas_fname']
        )['datas_fname']

        self.assertEqual(datas_fname, values['name'])