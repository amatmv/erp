from destral import testing
from destral.transaction import Transaction
from osv.orm import Constraint, OnlyFieldsConstraint
from mock import MagicMock
from osv import osv, fields, orm


class TestValidateConstraints(testing.OOTestCase):

    def test_validate_constraint_on_create(self):

        constraint = MagicMock(return_value=True)
        partner_obj = self.openerp.pool.get('res.partner')
        partner_obj._constraints = [
            Constraint(constraint, 'Constraint', ['ref'])
        ]
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            id_partner = partner_obj.create(cursor, uid, {
                'name': 'Test'
            })
            constraint.assert_called_with(
                partner_obj, cursor, uid, [id_partner]
            )

        constraint.reset_mock()
        partner_obj._constraints = [
            OnlyFieldsConstraint(constraint, 'Constraint', ['ref'])
        ]
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            id_partner = partner_obj.create(cursor, uid, {
                'name': 'Test',
                'ref': '1234'
            })
            constraint.assert_called_with(
                partner_obj, cursor, uid, [id_partner]
            )

        constraint.reset_mock()
        partner_obj._constraints = [
            OnlyFieldsConstraint(constraint, 'Constraint', ['ref'])
        ]
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            partner_obj.create(cursor, uid, {
                'name': 'Test',
            })
            constraint.assert_not_called()

    def test_validate_constraint_on_write(self):

        constraint = MagicMock(return_value=True)
        partner_obj = self.openerp.pool.get('res.partner')
        partner_obj._constraints = [
            Constraint(constraint, 'Constraint', ['ref'])
        ]
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            ids = partner_obj.search(cursor, uid, [], limit=1)
            partner_obj.write(cursor, uid, ids, {
                'name': 'Test',
                'ref': '1234'
            })
            constraint.assert_called_with(partner_obj, cursor, uid, ids)

        constraint.reset_mock()
        partner_obj._constraints = [
            OnlyFieldsConstraint(constraint, 'Constraint', ['ref'])
        ]
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            ids = partner_obj.search(cursor, uid, [], limit=1)
            partner_obj.write(cursor, uid, ids, {
                'name': 'Test',
                'ref': '1234'
            })
            constraint.assert_called_with(partner_obj, cursor, uid, ids)

        constraint.reset_mock()
        partner_obj._constraints = [
            OnlyFieldsConstraint(constraint, 'Constraint', ['ref'])
        ]
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            ids = partner_obj.search(cursor, uid, [], limit=1)
            partner_obj.write(cursor, uid, ids, {
                'name': 'Test',
            })
            constraint.assert_not_called()


class TestORM(testing.OOTestCase):
    def test_orm_search_operator(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            partner_obj = self.openerp.pool.get('res.partner')

            # Partner with name starting with SOME-WORD
            partner_id_starts = partner_obj.create(
                cursor, uid, {'name': 'SOME-WORD Starting name'})

            # Partner with name containing SOME-WORD
            partner_id_contains = partner_obj.create(
                cursor, uid, {'name': 'Containing SOME-WORD name'})

            # Partner with name ending with SOME-WORD
            partner_id_ends = partner_obj.create(
                cursor, uid, {'name': 'Ending name SOME-WORD'})

            # Partners which name starts with SOME-WORD
            partner_ids_starting = partner_obj.search(
                cursor, uid, [('name', '=ilike', 'some-word%')])
            self.assertIn(partner_id_starts, partner_ids_starting)
            self.assertNotIn(partner_id_contains, partner_ids_starting)
            self.assertNotIn(partner_id_ends, partner_ids_starting)

            # Partners which name contains SOME-WORD
            partner_ids_containing = partner_obj.search(
                cursor, uid, [('name', '=ilike', '%some-word%')])
            self.assertIn(partner_id_starts, partner_ids_containing)
            self.assertIn(partner_id_contains, partner_ids_containing)
            self.assertIn(partner_id_ends, partner_ids_containing)

            # Partners which name ends with SOME-WORD
            partner_ids_ending = partner_obj.search(
                cursor, uid, [('name', '=ilike', '%some-word')])

            self.assertIn(partner_id_ends, partner_ids_ending)
            self.assertNotIn(partner_id_starts, partner_ids_ending)
            self.assertNotIn(partner_id_contains, partner_ids_ending)

    def test_null_field_search(self):
        """
        Test for the '$' operator, that can be used to search for NULL fields.
        :return:
        """
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            partner_obj = self.openerp.pool.get('res.partner')

            empty_ref_partner, non_empty_ref_partner, another_partner = partner_obj.search(
                cursor, uid, [], limit=3
            )

            partner_obj.write(cursor, uid, empty_ref_partner, {'ref': False})
            partner_obj.write(cursor, uid, non_empty_ref_partner, {'ref': 'TEST'})
            partner_obj.write(cursor, uid, another_partner, {'ref': 'TE$T'})

            ids = [empty_ref_partner, non_empty_ref_partner, another_partner]

            result = partner_obj.search(cursor, uid, [
                ('ref', 'ilike', '$'),
                ('id', 'in', ids)
            ])
            self.assertIn(empty_ref_partner, result)
            self.assertNotIn(non_empty_ref_partner, result)
            self.assertNotIn(another_partner, result)

            result = partner_obj.search(cursor, uid, [
                ('ref', 'ilike', '%$%'),
                ('id', 'in', ids)
            ])
            self.assertIn(another_partner, result)
            self.assertNotIn(non_empty_ref_partner, result)
            self.assertNotIn(empty_ref_partner, result)

            result = partner_obj.search(cursor, uid, [
                '|', ('ref', 'ilike', '$'),
                ('ref', 'like', '%$%'),
                ('id', 'in', ids)
            ])
            self.assertIn(another_partner, result)
            self.assertIn(empty_ref_partner, result)
            self.assertNotIn(non_empty_ref_partner, result)

            result = partner_obj.search(cursor, uid, [
                ('ref', '=', '%$%'),
                ('id', 'in', ids)
            ])
            self.assertFalse(result)

            self.assertEquals(
                partner_obj.search(cursor, uid, [('ref', '=', False)]),
                partner_obj.search(cursor, uid, [('ref', 'ilike', '$')])
            )

            result = partner_obj.search(cursor, uid, [
                '|', ('ref', 'ilike', '$'),
                ('ref', 'ilike', 'TEST'),
                ('id', 'in', ids)
            ])
            self.assertIn(non_empty_ref_partner, result)
            self.assertIn(empty_ref_partner, result)
            self.assertNotIn(another_partner, result)

    def test_unlink_object_in_ir_model_data(self):
        """
        Test an object referenced by ir_model_data can't be deleted
        """
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            user_obj = self.openerp.pool.get('res.users')
            imd_obj = self.openerp.pool.get('ir.model.data')
            group_obj = self.openerp.pool.get('res.groups')
            partner_obj = self.openerp.pool.get('res.partner')

            # Choose any user different from the Admin
            users = user_obj.search(cursor, 1, [('id', '<>', 1)])
            admin_uid = 1
            uid = users[0]

            # First we grant access to the alternative user to create and
            # delete partners
            partner_manager_grup = imd_obj.get_object_reference(
                cursor, uid, 'base', 'group_partner_manager')[1]
            group_obj.write(cursor, admin_uid, partner_manager_grup, {
                'users': [(6, 0, [admin_uid, uid])]
            })

            partner_id_not_delete = imd_obj.get_object_reference(
                cursor, uid, 'base', 'main_partner')[1]
            partner_id_delete = partner_obj.create(
                cursor, uid, {'name': 'TEST PARTNER DELETE'})

            def delete_partner(partner_id):
                partner_obj.unlink(cursor, uid, [partner_id])

            # Assert raise an exception when used in ir_model_data
            with self.assertRaises(orm.except_orm) as exception_orm:
                delete_partner(partner_id_not_delete)

            # Assert don't raise an exception when not used in ir_model_data
            delete_partner(partner_id_delete)


class HashField(fields.text):

    @property
    def pg_type(self):
        """
        Returns the postgres type
        :return: The pg_type
        :rtype: tuple
        """
        return "text", "text"

    def __init__(self, string='unknown',  **args):
        """
        Class constructor

        :param string: Field string
        :type string: str
        :param args: Extra arguments
        :type args: dict
        """
        fields._column.__init__(self, string=string, **args)
        self.index_type = "hash"





    @property
    def pg_type(self):
        """
        Returns the postgres type
        :return: The pg_type
        :rtype: tuple
        """
        return "text", "text"

# class WrongIndexModelTest(osv.osv):
#     _name = "wrong.index.model.test"
#     _columns = {
#         "name": fields.char("Name", size=64),
#         "num": WrongField("geom", srid=25830, select=1),
#     }



class NoIndexModelTest(osv.osv):
    _name = "no.index.model.test"
    _columns = {
        "name": fields.char("Name", size=64),
        "num": fields.integer("geom", srid=25830, select=1),
    }


class IndexModelTest(osv.osv):
    _name = "index.model.test"
    _columns = {
        "name": fields.char("Name", size=64),
        "num": HashField("geom", srid=25830, select=1),
    }


def get_indices(cursor, table):
    """
    Gets the indices of a table

    :param cursor: Database cursor
    :param table: Table to get indices
    :return: index name as key and type of index as value
    :rtype: dict
    """

    sql = """
    SELECT i.relname as index_name,
       am.amname as type
    FROM   pg_index as idx
      JOIN   pg_class as i ON i.oid = idx.indexrelid
      JOIN   pg_am as am ON i.relam = am.oid
    where (idx.indrelid::regclass)::text = %(table)s;

    """
    cursor.execute(sql, {"table": table})
    data = cursor.fetchall()
    ret = {}
    for line in data:
        ret[line[0]] = line[1]
    return ret


class IndexTypeTest(testing.OOTestCase):

    def test_creates_index_no_type(self):
        """
        Test creates index whith select=1 and index_type=False

        :return: None
        """

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            NoIndexModelTest()
            osv.class_pool['no.index.model.test'].createInstance(
                self.openerp.pool, 'osv', cursor
            )
            suffix_obj = self.openerp.pool.get('no.index.model.test')
            suffix_obj._auto_init(cursor)
            indices = get_indices(cursor, 'no_index_model_test')
            self.assertDictEqual(
                indices,
                {
                    u"no_index_model_test_name_index": u"btree",
                    u"no_index_model_test_num_index": u"btree",
                    u'no_index_model_test_pkey': u"btree"
                }
            )

    def test_creates_index_type(self):
        """
        Test creates index whith select=1 and index_type=gist

        :return: None
        """

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor

            IndexModelTest()
            osv.class_pool['index.model.test'].createInstance(
                self.openerp.pool, 'osv', cursor
            )
            suffix_obj = self.openerp.pool.get('index.model.test')
            suffix_obj._auto_init(cursor)
            indices = get_indices(cursor, 'index_model_test')
            self.assertDictEqual(
                indices,
                {
                    u"index_model_test_name_index": u"btree",
                    u"index_model_test_num_index": u"hash",
                    u'index_model_test_pkey': u"btree"
                }
            )

    def test_creates_index_wrong_type(self):
        """
        Test creates index whith select=1 and index_type=wrong
        Ensures that exception is raised

        :return: None
        """

        with Transaction().start(self.database) as txn:
            try:
                with self.assertRaises(Exception):
                    class WrongField(fields._column):
                        _pg_type = "text"

                        def __init__(self, string='unknown', **args):
                            """
                            Class constructor

                            :param string: Field string
                            :type string: str
                            :param args: Extra arguments
                            :type args: dict
                            """
                            fields._column.__init__(self, string=string,index_type="wrong", **args)
            except Exception:
                pass