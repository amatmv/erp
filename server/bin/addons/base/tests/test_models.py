# coding=utf-8
from osv import osv, fields


class ResPartnerExtended(osv.osv):
    """Fake class.
    """

    _name = 'res.partner.extended'
    _inherits = {'res.partner': 'partner_id'}

    _columns = {
        'partner_id': fields.many2one('res.partner', 'Partner'),
        'tested': fields.boolean('Tested')
    }


class ResPartnerModuleExt(osv.osv):
    """Fake class to simple inherit res.partner
    """

    _name = 'res.partner'
    _inherit = 'res.partner'

    def _ff_get_function(self, cr, uid, ids, field, arg, context=None):
        """ Just get the id value """
        res = {}
        if not context:
            context = {}

        partner_obj = self.pool.get('res.partner')
        partners = partner_obj.read(cr, uid, ids)
        for p in partners:
            res[p['id']] = p['id']
        return res

    def _function_search(self, cr, uid, obj, field_name, arg, context=None):
        if not arg:
            return []
        else:
            return [('id', arg[0][1], arg[0][2])]

    _columns = {
        'added_field': fields.char('Added Field', size=10),
        'custom_added_field': fields.char('Custom added field', size=10),
        'function': fields.function(
            _ff_get_function, string='Function', fnct_search=_function_search,
            method=True, type='integer'
        ),
    }


