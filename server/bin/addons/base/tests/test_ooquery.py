# coding=utf-8
from base.tests import ResPartnerExtended, ResPartnerModuleExt
from destral import testing
from destral.transaction import Transaction
from osv.expression import OOQuery, get_model_name, convert_order_string
from osv import osv
from ooquery.operators import *


class TestOOQuery(testing.OOTestCase):

    def test_get_table_from_model(self):
        self.assertEqual(
            get_model_name(self.openerp.pool, 'wkf'),
            'workflow'
        )
        self.assertEqual(
            get_model_name(self.openerp.pool, 'wkf_activity'),
            'workflow.activity'
        )

    def test_join_queries_many2one(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            partner_obj = self.openerp.pool.get('res.partner')
            q = OOQuery(partner_obj, cursor, uid)
            sql = q.select(['id', 'name']).where(
                [('parent_id.ean13', '=', '3020178572427')]
            )
            cursor.execute(*sql)
            res = cursor.fetchall()
            self.assertEqual(
                res, [(21, 'Magazin BML 1')]
            )

    def test_different_join_types(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            partner_obj = self.openerp.pool.get('res.partner')
            q = partner_obj.q(cursor, uid)
            sql = q.select([
                'id', 'name', LeftJoin('parent_id.name')
            ], only_active=False).where([
                ('active', '=', True),
                ('name', 'in', ('ASUStek', "Centrale d'achats BML"))
            ])
            cursor.execute(*sql)
            res = cursor.fetchall()
            self.assertEqual(
                res,
                [(2, u'ASUStek', None),
                 (20, u"Centrale d'achats BML", u'Tecsas')]
            )

    def test_join_queries_one2many(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            partner_obj = self.openerp.pool.get('res.partner')
            q = OOQuery(partner_obj, cursor, uid)
            sql = q.select(['id', 'name']).where(
                [('address.city', '=', 'Paris')]
            )
            cursor.execute(*sql)
            res = cursor.fetchall()
            self.assertEqual(
                res, [(9, 'Bank Wealthy and sons')]
            )

    def test_read_join_one2many(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            partner_obj = self.openerp.pool.get('res.partner')
            q = partner_obj.q(cursor, uid)
            sql = q.select(['id', 'name', 'address.street']).where(
                [('id', '=', 3)]
            )
            cursor.execute(*sql)
            sql_res = cursor.dictfetchall()
            addr_obj = self.openerp.pool.get('res.partner')
            q = addr_obj.q(cursor, uid)
            res = q.read(['id', 'name', 'address.street']).where(
                [('id', '=', 3)]
            )
            self.assertEqual(res, sql_res)

    def test_join_queries_many2many(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            partner_obj = self.openerp.pool.get('res.partner')
            q = OOQuery(partner_obj, cursor, uid)
            sql = q.select(['id', 'name']).where(
                [('category_id.name', '=', 'Customer')]
            )
            cursor.execute(*sql)
            res = cursor.fetchall()
            self.assertEqual(
                res, [(19, 'Leclerc')]
            )

    def test_join_queries_many2one_from_different_table(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            address_obj = self.openerp.pool.get('res.partner.address')
            q = OOQuery(address_obj, cursor, uid)
            sql = q.select(['id', 'name']).where(
                [('partner_id.parent_id.ean13', '=', '3020178572427')]
            )
            cursor.execute(*sql)
            res = cursor.fetchall()
            self.assertEqual(
                res, [(19, 'Shop 1')]
            )

    def test_join_queries_one2many_from_different_table(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            address_obj = self.openerp.pool.get('res.partner.address')
            q = OOQuery(address_obj, cursor, uid)
            sql = q.select(['id', 'name']).where(
                [('partner_id.address.city', '=', 'Paris')]
            )
            cursor.execute(*sql)
            res = cursor.fetchall()
            self.assertEqual(
                res, [(13, 'Arthur Grosbonnet')]
            )

    def test_join_queries_many2many_from_different_table(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            address_obj = self.openerp.pool.get('res.partner.address')
            q = OOQuery(address_obj, cursor, uid)
            sql = q.select(['id', 'name']).where(
                [('partner_id.category_id.name', '=', 'Customer')]
            )
            cursor.execute(*sql)
            res = cursor.fetchall()
            self.assertEqual(
                res, [(20, 'Shop 2'), (21, 'Shop 3')]
            )

    def test_model_with_order(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            partner_obj = self.openerp.pool.get('res.partner')
            q = OOQuery(partner_obj, cursor, uid)
            sql = q.select(['id']).where([])
            sql2 = q.select(['id'], order_by=('name', )).where([])

            self.assertEqual(tuple(sql), tuple(sql2))

    def test_convert_order_string(self):
        order_string = 'name asc, foo DESC'
        order = ('name.asc', 'foo.desc')

        self.assertEqual(
            convert_order_string(order_string),
            order
        )

    def test_model_with_active(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            partner_obj = self.openerp.pool.get('res.partner')
            q = OOQuery(partner_obj, cursor, uid)
            sql = q.select(['id', 'name']).where(
                [('ean13', '=', '3020178570171')]
            )
            cursor.execute(*sql)
            res = cursor.fetchall()
            self.assertEqual(
                res, [(21, 'Magazin BML 1')]
            )
            partner_ids = partner_obj.search(cursor, uid, [
                ('ean13', '=', '3020178570171')
            ])
            partner_obj.write(cursor, uid, partner_ids, {'active': False})
            sql = q.select(['id', 'name']).where(
                [('ean13', '=', '3020178570171')]
            )
            cursor.execute(*sql)
            res = cursor.fetchall()
            self.assertEqual(
                res, []
            )
            sql = q.select(['id', 'name'], only_active=False).where(
                [('ean13', '=', '3020178570171')]
            )
            cursor.execute(*sql)
            res = cursor.fetchall()
            self.assertEqual(
                res, [(21, 'Magazin BML 1')]
            )

    def test_model_with_active_with_joins(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            partner_obj = self.openerp.pool.get('res.partner')
            q = OOQuery(partner_obj, cursor, uid)
            sql = q.select(['id', 'name']).where(
                [('parent_id.ean13', '=', '3020178572427')]
            )
            cursor.execute(*sql)
            res = cursor.fetchall()
            self.assertEqual(
                res, [(21, 'Magazin BML 1')]
            )
            partner_ids = partner_obj.search(cursor, uid, [
                ('ean13', '=', '3020178572427')
            ])
            partner_obj.write(cursor, uid, partner_ids, {'active': False})
            sql = q.select(['id', 'name']).where(
                [('parent_id.ean13', '=', '3020178572427')]
            )
            cursor.execute(*sql)
            res = cursor.fetchall()
            self.assertEqual(
                res, []
            )
            sql = q.select(['id', 'name'], only_active=False).where(
                [('parent_id.ean13', '=', '3020178572427')]
            )
            cursor.execute(*sql)
            res = cursor.fetchall()
            self.assertEqual(
                res, [(21, 'Magazin BML 1')]
            )

    def test_where_with_active(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            partner_obj = self.openerp.pool.get('res.partner')

            q = OOQuery(partner_obj, cursor, uid)

            # If we search with active true should get the result
            sql = q.select(['id', 'name']).where(
                [
                    ('ean13', '=', '3020178570171'),
                    ('active', '=', True)
                ]
            )
            cursor.execute(*sql)
            res = cursor.fetchall()
            self.assertEqual(
                res, [(21, 'Magazin BML 1')]
            )

            partner_obj.write(cursor, uid, [21], {'active': False})

            # If we set active to false and search with active true we should
            # not get anything
            sql = q.select(['id', 'name']).where(
                [
                    ('ean13', '=', '3020178570171'),
                    ('active', '=', True)
                ]
            )
            cursor.execute(*sql)
            res = cursor.fetchall()
            self.assertEqual(
                res, []
            )

            # But if we search with active false we should also get it
            sql = q.select(['id', 'name']).where(
                [
                    ('ean13', '=', '3020178570171'),
                    ('active', '=', False)
                ]
            )
            cursor.execute(*sql)
            res = cursor.fetchall()
            self.assertEqual(
                res, [(21, 'Magazin BML 1')]
            )

    def test_where_with_active_with_joins(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            partner_obj = self.openerp.pool.get('res.partner')

            q = OOQuery(partner_obj, cursor, uid)

            # We should get the correct result if we search with active
            sql = q.select(['id', 'name']).where(
                [
                    ('parent_id.ean13', '=', '3020178572427'),
                    ('parent_id.active', '=', True)
                ]
            )
            cursor.execute(*sql)
            res = cursor.fetchall()
            self.assertEqual(
                res, [(21, 'Magazin BML 1')]
            )

            # Then we change the value of active to false
            partner_ids = partner_obj.search(cursor, uid, [
                ('ean13', '=', '3020178572427')
            ])
            partner_obj.write(cursor, uid, partner_ids, {'active': False})

            # After that, we should not get anything if we search with active
            sql = q.select(['id', 'name']).where(
                [
                    ('parent_id.ean13', '=', '3020178572427'),
                    ('parent_id.active', '=', True)
                ]
            )
            cursor.execute(*sql)
            res = cursor.fetchall()
            self.assertEqual(
                res, []
            )

            # But we should get it if we search with active set to false
            sql = q.select(['id', 'name']).where(
                [
                    ('parent_id.ean13', '=', '3020178572427'),
                    ('parent_id.active', '=', False)
                ]
            )
            cursor.execute(*sql)
            res = cursor.fetchall()
            self.assertEqual(
                res, [(21, 'Magazin BML 1')]
            )

    def test_select_joined_tables(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            partner_obj = self.openerp.pool.get('res.partner')
            q = OOQuery(partner_obj, cursor, uid)
            sql = q.select(['id', 'name', 'parent_id.name']).where(
                [('parent_id.ean13', '=', '3020178572427')]
            )
            cursor.execute(*sql)
            res = cursor.fetchall()
            self.assertEqual(
                res, [(21, "Magazin BML 1", "Centrale d'achats BML")]
            )

    def test_select_alias(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            partner_obj = self.openerp.pool.get('res.partner')
            q = OOQuery(partner_obj, cursor, uid)
            sql = q.select(['id', 'name', 'parent_id.name']).where(
                [('parent_id.ean13', '=', '3020178572427')]
            )
            cursor.execute(*sql)
            res = cursor.dictfetchall()
            self.assertEqual(
                res, [{
                    'id': 21,
                    'name': "Magazin BML 1",
                    'parent_id.name': "Centrale d'achats BML"
                }]
            )

    def test_select_inherits(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            ResPartnerExtended()
            osv.class_pool['res.partner.extended'].createInstance(
                self.openerp.pool, 'base', cursor
            )
            partner_extended = self.openerp.pool.get('res.partner.extended')
            partner_extended._auto_init(cursor)

            partner_extended.create(
                cursor, uid, {
                    'name': 'test1',
                    'tested': True,
                }
            )

            partner_extended.create(
                cursor, uid, {
                    'name': 'test2',
                    'tested': False,
                }
            )

            q = OOQuery(partner_extended, cursor, uid)
            sql = q.select(['name', 'tested']).where([])
            cursor.execute(*sql)
            res = cursor.fetchall()

            # We should be able to read both the inherited and the local fields
            self.assertEqual(
                res, [('test1', True), ('test2', False)]
            )

    def test_where_inherits(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            ResPartnerExtended()
            osv.class_pool['res.partner.extended'].createInstance(
                self.openerp.pool, 'base', cursor
            )
            partner_extended = self.openerp.pool.get('res.partner.extended')
            partner_extended._auto_init(cursor)

            partner_extended.create(
                cursor, uid, {
                    'name': 'test1',
                    'tested': True,
                }
            )

            partner_extended.create(
                cursor, uid, {
                    'name': 'test2',
                    'tested': False,
                }
            )

            q = OOQuery(partner_extended, cursor, uid)
            sql = q.select(['name', 'tested']).where([('name', '=', 'test1')])
            cursor.execute(*sql)
            res = cursor.fetchall()

            # We should be able to read both the inherited and the local fields
            self.assertEqual(
                res, [('test1', True)]
            )

            sql = q.select(['name', 'tested']).where([('tested', '=', True)])
            cursor.execute(*sql)
            res = cursor.fetchall()

            # We should be able to read both the inherited and the local fields
            self.assertEqual(
                res, [('test1', True)]
            )

    def test_with_fnct_search(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            ResPartnerModuleExt()
            osv.class_pool['res.partner'].createInstance(
                self.openerp.pool, 'base', cursor
            )
            partner_obj = self.openerp.pool.get('res.partner')
            partner_obj._auto_init(cursor)

            q = OOQuery(partner_obj, cursor, uid)

            partner_obj.search(cursor, uid, [('function', 'in', [1, 2, 3])])

            sql = q.select(['id', 'name']).where(
                [('function', 'in', [1, 2, 3])]
            )
            cursor.execute(*sql)
            expected_result = [
                (3, u'Agrolait'), (2, u'ASUStek'), (1, u'Tiny sprl')
            ]
            for res in cursor.fetchall():
                self.assertIn(res, expected_result)

            sql = q.select(['id', 'name']).where([('function', '=', 1)])
            cursor.execute(*sql)
            res = cursor.fetchall()
            self.assertEqual(
                res, [(1, u'Tiny sprl')]
            )

    def test_aggregation(self):
        from sql.aggregate import Count
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            partner_obj = self.openerp.pool.get('res.partner.address')
            q = OOQuery(partner_obj, cursor, uid)
            sql = q.select([
                Count('id'),
                'partner_id.name'
            ], group_by=['partner_id.name']).where([
                ('partner_id.name', '=', u'Agrolait')
            ])
            cursor.execute(*sql)
            res = cursor.fetchall()
            self.assertEqual(
                res,
                [(3, u'Agrolait')]
            )

    def test_search_false_on_non_boolean_field_search_is_null(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            partner_obj = self.openerp.pool.get('res.partner')
            q = partner_obj.q(cursor, uid)
            sql = q.select(['id']).where([('name', '=', False)])
            sql2 = q.select(['id']).where([('name', '=', None)])
            self.assertEqual(
                tuple(sql),
                tuple(sql2)
            )

    def test_search_1_on_boolen_field_searches_true(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            partner_obj = self.openerp.pool.get('res.partner')
            q = partner_obj.q(cursor, uid)
            sql = q.select(['id']).where([('customer', '=', 1)])
            sql2 = q.select(['id']).where([('customer', '=', True)])
            sql = cursor.mogrify(*sql)
            sql2 = cursor.mogrify(*sql2)
            self.assertEqual(
                sql,
                sql2
            )

    def test_like_should_add_percent_before_and_after(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            partner_obj = self.openerp.pool.get('res.partner')
            q = partner_obj.q(cursor, uid)
            sql = q.select(['id']).where([('name', 'like', 'gro')])
            cursor.execute(*sql)
            res = [x[0] for x in cursor.fetchall()]
            res2 = partner_obj.search(cursor, uid, [('name', 'like', 'gro')])
            self.assertEqual(
                res,
                res2
            )

            sql = q.select(['id']).where([('name', 'ilike', 'gro')])
            cursor.execute(*sql)
            res = [x[0] for x in cursor.fetchall()]
            res2 = partner_obj.search(cursor, uid, [('name', 'ilike', 'gro')])
            self.assertEqual(
                res,
                res2
            )

    def test_search_many2one_if_argument_is_a_string_call_name_search(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            addr_obj = self.openerp.pool.get('res.partner.address')
            q = addr_obj.q(cursor, uid)
            sql = q.select(['id']).where([('partner_id', 'like', 'gro')])
            cursor.execute(*sql)
            res = [x[0] for x in cursor.fetchall()]
            res2 = addr_obj.search(cursor, uid, [('partner_id', 'like', 'gro')])
            self.assertEqual(
                res,
                res2
            )

    def test_object_must_have_ooquery_object_as_q_factory(self):
        from sql.aggregate import Count
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            partner_obj = self.openerp.pool.get('res.partner.address')
            q = OOQuery(partner_obj, cursor, uid)
            sql = q.select([
                Count('id'),
                'partner_id.name'
            ], group_by=['partner_id.name']).where([
                ('partner_id.name', '=', u'Agrolait')
            ])
            sql2 = partner_obj.q(cursor, uid).select([
                Count('id'),
                'partner_id.name'
            ], group_by=['partner_id.name']).where([
                ('partner_id.name', '=', u'Agrolait')
            ])
            self.assertEqual(
                tuple(sql),
                tuple(sql2)
            )

    def test_ooquery_should_have_a_read_method_that_returns_a_dict(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            partner_obj = self.openerp.pool.get('res.partner')
            q = partner_obj.q(cursor, uid)
            res = q.read(['id', 'name', 'parent_id.name']).where(
                [('parent_id.ean13', '=', '3020178572427')]
            )
            self.assertEqual(
                res, [{
                    'id': 21,
                    'name': "Magazin BML 1",
                    'parent_id.name': "Centrale d'achats BML"
                }]
            )

    def test_ooquery_read_returns_fields_function(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            categ_obj = self.openerp.pool.get('res.partner.category')
            q = categ_obj.q(cursor, uid)
            res = q.read(
                ['name', 'parent_id.name', 'complete_name'], limit=1
            ).where([
                ('parent_id', '!=', None)
            ])
            self.assertEqual(
                res, [{
                    'parent_id.name': 'Customer',
                    'complete_name': 'Customer / OpenERP Partners',
                    'id': 3, 'name': 'OpenERP Partners'
                    }]
            )

    def test_filtering_number_and_nulls(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            partner_obj = self.openerp.pool.get('res.partner')
            q = partner_obj.q(cursor, uid)
            sql = q.select(['id', 'credit_limit']).where([
                ('credit_limit', '>', 0)
            ])
            cursor.execute(*sql)
            result = cursor.dictfetchall()
            self.assertEqual(result, [
                {'credit_limit': 15000.0, 'id': 20L},
                {'credit_limit': 1200.0, 'id': 19L},
                {'credit_limit': 1500.0, 'id': 21L},
                {'credit_limit': 32000.0, 'id': 14L},
                {'credit_limit': 12000.0, 'id': 17L},
                {'credit_limit': 5000.0, 'id': 15L}
            ])
