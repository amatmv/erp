# coding=utf-8
from osv.orm import AccessError
from destral import testing
from destral.transaction import Transaction


class TestIrModelAccess(testing.OOTestCase):

    def test_throws_access_error_exception(self):
        pool = self.openerp.pool
        imdg = pool.get('ir.model.grid')
        user_obj = pool.get('res.users')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            new_user = user_obj.create(cursor, uid,
                {
                    'name': 'test uset',
                    'login': 'test',
                    'password': 'test_password'
                }
            )
            try:
                ids = imdg.search(cursor, new_user, [])
            except Exception as e:
                assert isinstance(e, AccessError)
