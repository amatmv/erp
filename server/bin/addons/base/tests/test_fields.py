# -*- coding: utf-8 -*-
from __future__ import absolute_import
from osv import osv, fields
from destral import testing
from destral.transaction import Transaction


class TestModel(osv.osv):
    _name = "test.model"
    _columns = {
        "name": fields.char("Name", size=64),
        "data": fields.json("data"),
    }


class TestJsonField(testing.OOTestCase):

    def setUp(self):
        """
        Setups the test suite
        :return: None
        """
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user

    def tearDown(self):
        """
        Ends the transaction
        :return: None
        """
        self.txn.stop()

    def test_json_read_none(self):
        """
        Tests than when reads a di

        :return: None
        """
        TestModel()
        osv.class_pool['test.model'].createInstance(
            self.openerp.pool, 'base', self.cursor
        )
        test_obj = self.openerp.pool.get('test.model')
        test_obj._auto_init(self.cursor)

        id_null = test_obj.create(
            self.cursor, self.uid,
            {"data": {"normal": "normal", "empty": None}}
        )
        r_data = test_obj.read(
            self.cursor, self.uid,
            id_null, ["data"]
        )
        self.assertEquals(r_data["data"]["empty"], False)

    def test_read_empy(self):
        """
        Test the read when a json field is null

        :return: None
        """

        TestModel()
        osv.class_pool['test.model'].createInstance(
            self.openerp.pool, 'base', self.cursor
        )
        test_obj = self.openerp.pool.get('test.model')
        test_obj._auto_init(self.cursor)

        id_test = test_obj.create(self.cursor, self.uid, {"name": "test"})

        data_test = test_obj.read(self.cursor, self.uid, id_test, ["data"])
        self.assertEquals(data_test["data"], {})


# Test FIX for reference fields
# Test classes to test the new behaviour
class ReferencedModel(osv.osv):
    _name = "referenced.model"
    _columns = {
        "name": fields.char("Name", size=64),
        "description": fields.text("Description")
    }


RELATED_MODELS = [
    ('referenced.model', 'Referenced'),
]


class ReferencingModel(osv.osv):
    _name = "referencing.model"
    _columns = {
        "name": fields.char("Name", size=64),
        "reference": fields.reference(
            'Reference', RELATED_MODELS, size=256, required=True
        )
    }


class BaseFieldsReference(testing.OOTestCase):
    def setUp(self):
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user
        self.init_classes()
        self.created_objects = self.create_objects()

    def tearDown(self):
        self.txn.stop()

    def init_classes(self):
        ReferencedModel()
        osv.class_pool['referenced.model'].createInstance(
            self.openerp.pool, 'base', self.cursor
        )
        referenced_obj = self.openerp.pool.get('referenced.model')
        referenced_obj._auto_init(self.cursor)


        ReferencingModel()
        osv.class_pool['referencing.model'].createInstance(
            self.openerp.pool, 'base', self.cursor
        )
        referencing_obj = self.openerp.pool.get('referencing.model')
        referencing_obj._auto_init(self.cursor)

    def create_objects(self):
        referenced_obj = self.openerp.pool.get('referenced.model')
        referencing_obj = self.openerp.pool.get('referencing.model')

        id_referenced = referenced_obj.create(
            self.cursor, self.uid, {
                "name": "Element Referenced",
                "description": "Element to be referenced"
            }
        )

        id_referencing = referencing_obj.create(
            self.cursor, self.uid, {
                "name": "Element Referencing",
                "reference":
                    "referenced.model,"+str(id_referenced),
            }
        )

        created_elements = {
            'referenced': id_referenced,
            'referencing': id_referencing
        }

        return created_elements


class BaseFieldsReferenceTest(BaseFieldsReference):

    def test_reference_fields_behaviour(self):

        # Inicialitzar objectes i valors
        referenced_obj = self.openerp.pool.get('referenced.model')
        referencing_obj = self.openerp.pool.get('referencing.model')
        created_objects = self.created_objects

        # ABANS DE BORRAR
        # Llegir referencing object per comprovar que es pot llegir
        referencing_elem = referencing_obj.read(
            self.cursor, self.uid, created_objects['referencing']
        )
        model, ref_id = referencing_elem['reference'].split(',')
        self.assertEqual(ref_id, str(created_objects['referenced']))
        self.assertEqual(model, 'referenced.model')

        # Mirar que el browse a un reference funciona bé.
        # Browse_reference TRUE
        context = {'browse_reference': True}
        referencing_elem_bro = referencing_obj.browse(
            self.cursor, self.uid, created_objects['referencing'], context
        )
        reference = referencing_elem_bro.reference
        self.assertEqual(reference.id, created_objects['referenced'])
        self.assertEqual(reference.name, "Element Referenced")
        # Browse_reference FALSE
        context = {'browse_reference': False}
        referencing_elem_bro = referencing_obj.browse(
            self.cursor, self.uid, created_objects['referencing'], context
        )
        reference = referencing_elem_bro.reference
        model, ref_id = reference.split(',')
        self.assertEqual(ref_id, str(created_objects['referenced']))
        self.assertEqual(model, 'referenced.model')

        # BORRAR REFERENCED
        referenced_obj.unlink(
            self.cursor, self.uid, [created_objects['referenced']]
        )

        # DESPRES DE BORRAR
        # Llegir referencing object per comprovar que es pot llegir
        referencing_elem = referencing_obj.read(
            self.cursor, self.uid, created_objects['referencing']
        )
        model, ref_id = referencing_elem['reference'].split(',')
        self.assertEqual(ref_id, '0')
        self.assertEqual(model, 'referenced.model')

        # Mirar que el browse a un reference funciona bé.
        # Browse_reference TRUE
        context = {'browse_reference': True}
        referencing_elem_bro = referencing_obj.browse(
            self.cursor, self.uid, created_objects['referencing'], context
        )
        reference = referencing_elem_bro.reference
        self.assertFalse(reference)
        # Browse_reference FALSE
        context = {'browse_reference': False}
        referencing_elem_bro = referencing_obj.browse(
            self.cursor, self.uid, created_objects['referencing'], context
        )
        reference = referencing_elem_bro.reference
        model, ref_id = reference.split(',')
        self.assertEqual(ref_id, '0')
        self.assertEqual(model, 'referenced.model')
