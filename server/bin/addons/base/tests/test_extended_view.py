# coding=utf-8
from __future__ import unicode_literals

from . import ResPartnerExtended, ResPartnerModuleExt
from destral import testing
from destral.transaction import Transaction
from osv import osv, fields

from lxml import etree


class TestExtendedView(testing.OOTestCase):

    def tearDown(self):
        from base.res.partner import partner
        partner.res_partner()
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            osv.class_pool['res.partner'].createInstance(
                self.openerp.pool, 'base', cursor
            )

    def test_extended_view(self):
        pool = self.openerp.pool
        ui = pool.get('ir.ui.view')
        imd = pool.get('ir.model.data')
        partner_obj = pool.get('res.partner')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            ResPartnerExtended()
            osv.class_pool['res.partner.extended'].createInstance(
                self.openerp.pool, 'base', cursor
            )
            partner_extended = self.openerp.pool.get('res.partner.extended')
            partner_extended._auto_init(cursor)

            base_id = imd.get_object_reference(cursor, uid,
                'base', 'view_partner_form'
            )[1]

            view_id = ui.create(cursor, uid, {
                'name': 'res.partner.extended',
                'model': 'res.partner.extended',
                'type': 'form',
                'inherit_id': base_id,
                'extend': True,
                'arch': """
                    <xpath expr="//field[@name='name']" position="after">
                        <field name="tested"/>
                    </xpath>
                """
            })

            ui.create(cursor, uid, {
                'name': 'res.partner.extended2',
                'model': 'res.partner.extended',
                'type': 'form',
                'inherit_id': view_id,
                'arch': """
                    <xpath expr="//field[@name='ref']" position="attributes">
                        <attribute name="required">1</attribute>
                    </xpath>
                """
            })

            partner_xml = etree.fromstring(
                partner_obj.fields_view_get(cursor, uid, base_id)['arch']
            )
            partner_ext_xml = etree.fromstring(
                partner_extended.fields_view_get(cursor, uid, view_id)['arch']
            )

            self.assertEqual(
                partner_xml.xpath("//field[@name='tested']"),
                []
            )

            element = partner_ext_xml.xpath("//field[@name='tested']")[0]
            self.assertEqual(element.tag, 'field')
            self.assertEqual(element.attrib, {'name': 'tested'})

            element = partner_ext_xml.xpath("//field[@name='ref']")[0]
            self.assertIn('required', element.attrib)

            ResPartnerModuleExt()
            osv.class_pool['res.partner'].createInstance(
                self.openerp.pool, 'base', cursor
            )
            partner_obj = self.openerp.pool.get('res.partner')
            partner_obj._auto_init(cursor)

            ui.create(cursor, uid, {
                'name': 'res.partner.form2',
                'model': 'res.partner',
                'type': 'form',
                'inherit_id': base_id,
                'arch': """
                    <xpath expr="//field[@name='ref']" position="after">
                        <field name="added_field"/>
                    </xpath>
                """
            })

            partner_xml = etree.fromstring(
                partner_obj.fields_view_get(cursor, uid, base_id)['arch']
            )
            partner_ext_xml = etree.fromstring(
                partner_extended.fields_view_get(cursor, uid, view_id)['arch']
            )
            element = partner_xml.xpath("//field[@name='added_field']")[0]
            self.assertEqual(element.attrib, {'name': 'added_field'})

            element = partner_ext_xml.xpath("//field[@name='added_field']")[0]
            self.assertEqual(element.attrib, {'name': 'added_field'})

            extend_id = ui.create(cursor, uid, {
                'name': 'res.partner2',
                'model': 'res.partner',
                'type': 'form',
                'inherit_id': base_id,
                'extend': True,
                'arch': """
                    <xpath expr="//field[@name='ref']" position="after">
                        <field name="custom_added_field"/>
                    </xpath>
                """
            })

            partner_xml = etree.fromstring(
                partner_obj.fields_view_get(cursor, uid, base_id)['arch']
            )

            self.assertEqual(
                partner_xml.xpath("//field[@name='custom_added_field']"),
                []
            )

            partner2_xml = etree.fromstring(
                partner_obj.fields_view_get(cursor, uid, extend_id)['arch']
            )

            self.assertEqual(
                partner2_xml.xpath("//field[@name='custom_added_field']")[0].attrib,
                {'name': 'custom_added_field'}
            )
