# coding=utf-8
from datetime import datetime
from random import randint

from osv import orm, osv, fields
from destral import testing


def format_dt(dt):
    return dt.isoformat().replace('T', ' ')


class TimescaleTest(osv.Timescale):
    _name = 'timescale.test'
    _time_column = 'ts'

    _columns = {
        'ts': fields.datetime('Timestamp', required=True),
        'measure': fields.integer('Measure')
    }


class TestTimescaleORM(testing.OOTestCaseWithCursor):

    def test_raise_if_not_time_column_is_defined(self):

        cursor = self.txn.cursor

        with self.assertRaises(ValueError):
            TimescaleTest()
            osv.class_pool['timescale.test'].createInstance(
                self.openerp.pool, 'base', cursor
            )
            ts_obj = self.openerp.pool.get('timescale.test')
            ts_obj._time_column = None
            ts_obj._auto_init(cursor)

    def test_raise_if_not_time_column_is_required(self):

        cursor = self.txn.cursor

        with self.assertRaises(ValueError):
            TimescaleTest()
            osv.class_pool['timescale.test'].createInstance(
                self.openerp.pool, 'base', cursor
            )
            ts_obj = self.openerp.pool.get('timescale.test')
            value = ts_obj._columns[ts_obj._time_column].required
            ts_obj._columns[ts_obj._time_column].required = False
            ts_obj._auto_init(cursor)
        ts_obj._columns[ts_obj._time_column].required = value

    def test_timescaledb_object_creates_a_hypertable(self):

        cursor = self.txn.cursor
        TimescaleTest()
        osv.class_pool['timescale.test'].createInstance(
            self.openerp.pool, 'base', cursor
        )
        ts_obj = self.openerp.pool.get('timescale.test')
        ts_obj._auto_init(cursor)

        cursor.execute("select table_name from _timescaledb_catalog.hypertable")
        tables = [x[0] for x in cursor.fetchall()]
        self.assertIn(ts_obj._table, tables)


class TestTimescaleModel(testing.OOTestCaseWithCursor):

    def create_model(self):
        cursor = self.txn.cursor
        TimescaleTest()
        osv.class_pool['timescale.test'].createInstance(
            self.openerp.pool, 'base', cursor
        )
        ts_obj = self.openerp.pool.get('timescale.test')
        ts_obj._auto_init(cursor)

    def setUp(self):
        super(TestTimescaleModel, self).setUp()
        self.create_model()

    def test_create_method(self):
        cursor = self.txn.cursor
        uid = self.txn.user
        ts_obj = self.openerp.pool.get('timescale.test')

        created_ids = []
        for _ in range(0, 1000):
            created_id = ts_obj.create(cursor, uid, {
                'ts': format_dt(datetime.now()),
                'measure': randint(0, 100)
            })
            created_ids.append(created_id)

        self.assertEqual(len(created_ids), 1000)

    def test_search_method(self):
        cursor = self.txn.cursor
        uid = self.txn.user
        ts_obj = self.openerp.pool.get('timescale.test')

        for _ in range(0, 1000):
            ts_obj.create(cursor, uid, {
                'ts': format_dt(datetime.now()),
                'measure': randint(0, 100)
            })

        cursor.execute("SELECT id from timescale_test WHERE measure > 50")
        from_sql_ids = [x[0] for x in cursor.fetchall()]

        from_obj_ids = ts_obj.search(cursor, uid, [
            ('measure', '>', 50)
        ])
        self.assertEquals(from_sql_ids, from_obj_ids)

    def test_read_method(self):
        cursor = self.txn.cursor
        uid = self.txn.user
        ts_obj = self.openerp.pool.get('timescale.test')

        for _ in range(0, 1000):
            ts_obj.create(cursor, uid, {
                'ts': format_dt(datetime.now()),
                'measure': randint(0, 100)
            })

        cursor.execute("SELECT id,ts,measure from timescale_test WHERE measure > 50")
        from_sql = [x for x in cursor.dictfetchall()]

        from_obj_ids = ts_obj.search(cursor, uid, [
            ('measure', '>', 50)
        ])
        from_obj = ts_obj.read(cursor, uid, from_obj_ids)
        self.assertEquals(from_sql, from_obj)

    def test_browse_method(self):
        cursor = self.txn.cursor
        uid = self.txn.user
        ts_obj = self.openerp.pool.get('timescale.test')

        for _ in range(0, 1000):
            ts_obj.create(cursor, uid, {
                'ts': format_dt(datetime.now()),
                'measure': randint(0, 100)
            })

        from_obj_ids = ts_obj.search(cursor, uid, [
            ('measure', '>', 50)
        ])
        from_obj = ts_obj.browse(cursor, uid, from_obj_ids)
        self.assertTrue(isinstance(from_obj, orm.browse_record_list))
        for obj in from_obj:
            self.assertTrue(isinstance(obj, orm.browse_record))
            self.assertGreater(obj.measure, 50)
