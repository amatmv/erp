# -*- coding: utf-8 -*-

from expects import expect, equal
from destral import testing
from destral.transaction import Transaction


class TestPartner(testing.OOTestCase):
    def test_clean_vat_country_code(self):
        """
        Test function clean_vat_country_code removes country code form partner
        VAT if it has a country code
        """
        partner_obj = self.openerp.pool.get('res.partner')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            partner_id = partner_obj.search(cursor, uid, [])[0]
            initial_vat = 'ES12345678Z'
            partner_obj.write(cursor, uid, [partner_id], {'vat': initial_vat})

            partner_vat = partner_obj.read(
                cursor, uid, partner_id, ['vat']
            )['vat']

            expect(partner_vat).to(equal(initial_vat))

            clean_vat = partner_obj.get_clean_vat_country_code(
                cursor, uid, partner_vat
            )
            final_vat = initial_vat[2:]

            # Expect the new vat to don't have country code
            expect(clean_vat).to(equal(final_vat))
            # Make sure the partner_vat doesn't get modified inside the function
            expect(partner_vat).to(equal(initial_vat))

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            partner_id = partner_obj.search(cursor, uid, [])[0]
            initial_vat = '12345678Z'
            partner_obj.write(cursor, uid, [partner_id], {'vat': initial_vat})

            partner_vat = partner_obj.read(
                cursor, uid, partner_id, ['vat']
            )['vat']

            expect(partner_vat).to(equal(initial_vat))

            clean_vat = partner_obj.get_clean_vat_country_code(
                cursor, uid, partner_vat
            )

            # Expect the new vat to be equal to the initial vat
            expect(clean_vat).to(equal(initial_vat))
