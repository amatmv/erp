from blinker import signal

DB_CURSOR_CREATE = signal('db-cursor-create')
DB_CURSOR_COMMIT = signal('db-cursor-commit')
DB_CURSOR_ROLLBACK = signal('db-cursor-rollback')
