# coding=utf-8
from lxml import etree
from expects import *


with description('Translating a view'):
    with it('must translate confirm strings'):
        view_xml = '<button confirm="test confirm string"/>'
        view = etree.XML(view_xml)
        from tools.translate import trans_parse_view
        res = trans_parse_view(view)
        expect(res).to(contain("test confirm string"))