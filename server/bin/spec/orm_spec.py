# coding=utf-8
from osv import orm, osv
from expects import *


with description('ORM Exceptions'):
    with context('when getting the message of the exception'):
        with it(r'must following the format pattern of type -- name\n\nvalue'):
            exc = orm.AccessError(
                'AccessError', 'Permission error foo.bar'
            )
            exc_type = unicode(exc).split(' -- ')[0]
            expect(exc_type).to(equal('warning'))

            exc = orm.SqlConstrainError(
                'Constraint Error', 'SQL Constraint Error'
            )
            exc_type = unicode(exc).split(' -- ')[0]
            expect(exc_type).to(equal('warning'))

            exc = orm.SqlIntegrityError(
                'Integrity Error', 'SQL Integrity Error'
            )
            exc_type = unicode(exc).split(' -- ')[0]
            expect(exc_type).to(equal('warning'))

            exc = orm.except_orm(
                'Error', 'ORM Error'
            )
            exc_type = unicode(exc).split(' -- ')[0]
            expect(exc_type).to(equal('error'))
            
            exc = osv.except_osv(
                'Error', 'OSV Error'
            )
            exc_type = unicode(exc).split(' -- ')[0]
            expect(exc_type).to(equal('warning'))

            exc = orm.ConcurrencyException(
                'ConcurrencyException', 'Records were modified in the meanwhile'
            )
            exc_type = unicode(exc).split(' -- ')[0]
            expect(exc_type).to(equal('warning'))

            exc = orm.ValidateException(
                'ValidateError', 'Error validation'
            )
            exc_type = unicode(exc).split(' -- ')[0]
            expect(exc_type).to(equal('warning'))

        with it(r'is warning type'):
            exc = orm.AccessError(
                'AccessError', 'Permission error foo.bar'
            )
            exc_type = unicode(exc).split(' -- ')[0]
            expect(exc_type).to(equal('warning'))

            exc = orm.SqlConstrainError(
                'Constraint Error', 'SQL Constraint Error'
            )
            exc_type = unicode(exc).split(' -- ')[0]
            expect(exc_type).to(equal('warning'))

            exc = orm.SqlIntegrityError(
                'Integrity Error', 'SQL Integrity Error'
            )
            exc_type = unicode(exc).split(' -- ')[0]
            expect(exc_type).to(equal('warning'))

            exc = osv.except_osv(
                'Error', 'OSV Error'
            )
            exc_type = unicode(exc).split(' -- ')[0]
            expect(exc_type).to(equal('warning'))

            exc = orm.ConcurrencyException(
                'ConcurrencyException', 'Records were modified in the meanwhile'
            )
            exc_type = unicode(exc).split(' -- ')[0]
            expect(exc_type).to(equal('warning'))

            exc = orm.ValidateException(
                'ValidateError', 'Error validation'
            )
            exc_type = unicode(exc).split(' -- ')[0]
            expect(exc_type).to(equal('warning'))
