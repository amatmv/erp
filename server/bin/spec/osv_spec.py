from mock import Mock


with description('Using an API Wrapper'):
    with it('must call osv_pool as normal'):
        from osv.osv import TransactionExecute
        model = TransactionExecute('dbname', 4, 'test.object')
        model.api.execute = Mock()
        x = model.foo([1, 2, 3], {'lang': 'es_ES'})
        call_args = [
            'dbname', 4, 'test.object', 'foo', [1, 2, 3], {'lang': 'es_ES'}
        ]
        model.api.execute.assert_called_with(*call_args)
