# coding=utf-8
from flask import Flask, request, Response
from flask_cors import CORS
import msgpack
from netsvc import OpenERPDispatcher, OpenERPDispatcherException
from tools import exception_to_unicode
import bjoern
import logging


app = Flask(__name__)
CORS(app)
logger = logging.getLogger('openerp.msgpack')


@app.route('/<service>', methods=['POST'])
def execute(service):
    ipaddr = (request.remote_addr, 0)
    dispatcher = OpenERPDispatcher(request, ipaddr, None)
    data = request.get_data()
    args = msgpack.unpackb(data, raw=False)
    method = args[0]
    params = args[1:]
    logger.debug('Request from: {}. Message on {}: {}'.format(
        request.remote_addr, service, args
    ))
    try:
        try:
            res = dispatcher.dispatch(service, method, params)
            status_code = 200
        except OpenERPDispatcherException as exc:
            status_code = 210
            res = {
                'exception': exception_to_unicode(exc.exception),
                'traceback': exc.traceback
            }
        logger.debug('result: %s', res)
        response = Response(
            msgpack.packb(res, use_bin_type=False),
            status=status_code,
            mimetype='application/msgpack'
        )
        return response
    except Exception:
        import traceback
        traceback.print_exc()
        raise


class MsgPackDaemon(object):
    def __init__(self, host, port):
        self.host = host
        self.port = port
        bjoern.listen(app, host, port)

    def run(self):
        logger.info('Running msgpack server on {}:{}'.format(
            self.host, self.port
        ))
        bjoern.run()
