# coding=utf-8
from __future__ import unicode_literals
from ast import literal_eval
import logging
from collections import namedtuple
import os


logger = logging.getLogger('openerp.debug')


FrameInfo = namedtuple(
    'FrameInfo', 'frame, path, line_number, method, code, index'
)


class DummyDebugger(object):
    """Dummy debugger
    """
    def set_trace(self, *args, **kwargs):
        import inspect
        frame = FrameInfo(*inspect.stack()[2])
        logger.warn(
            'Debugger called from {f.path}:{f.line_number} method {f.method} '
            'but DEBUG_ENABLED environment is not defined'.format(f=frame)
        )


class DebuggerPatch(object):

    @staticmethod
    def debug_enabled():
        return bool(literal_eval(os.environ.get('DEBUG_ENABLED', 'False')))


class PudbPatch(DebuggerPatch):

    orig_get_debugger = None
    orig_set_interrupt_handler = None

    @staticmethod
    def _get_debugger(**kwargs):
        """Method to get the debugger but only if DEBUG_ENABLED is defined
        """
        if PudbPatch.debug_enabled():
            return PudbPatch.orig_get_debugger(**kwargs)
        else:
            return DummyDebugger()

    @staticmethod
    def set_interrupt_handler(*args, **kwargs):
        if PudbPatch.debug_enabled():
            return PudbPatch.orig_set_interrupt_handler(*args, **kwargs)
        else:
            return

    @staticmethod
    def patch():
        try:
            import pudb
            if PudbPatch.orig_get_debugger is None:
                PudbPatch.orig_get_debugger = staticmethod(pudb._get_debugger)
                pudb._get_debugger = PudbPatch._get_debugger
            if PudbPatch.orig_set_interrupt_handler is None:
                PudbPatch.orig_set_interrupt_handler = staticmethod(
                    pudb.set_interrupt_handler
                )
                pudb.set_interrupt_handler = PudbPatch.set_interrupt_handler
            logger.info('Pudb monkepatched')
        except ImportError:
            logger.warning('Pudb not installed. No monkeypatched')


class PdbPatch(DebuggerPatch):
    orig_set_trace = None

    @staticmethod
    def set_trace():
        if PdbPatch.debug_enabled():
            return PdbPatch.orig_set_trace()
        else:
            dbg = DummyDebugger()
            return dbg.set_trace()

    @staticmethod
    def patch():
        import pdb
        if PdbPatch.orig_set_trace is None:
            PdbPatch.orig_set_trace = staticmethod(pdb.set_trace)
            pdb.set_trace = PdbPatch.set_trace
        logger.info('Pdb monkepatched')


def monkeypatch():
    PudbPatch.patch()
    PdbPatch.patch()
