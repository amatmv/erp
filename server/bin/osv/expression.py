#!/usr/bin/env python
# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution	
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    $Id$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from sql import Table
from sql.operators import Equal

from tools import flatten, reverse_enumerate, has_unaccent
from tools.config import config
from ooquery import OOQuery as OOQueryBase, Parser
import pooler
import fields

from collections import OrderedDict

OPS = [
'=', '!=', '<>', '<=', '<', '>', '>=', '=like', 'like', 'not like', '=ilike',
'ilike', 'not ilike', 'in', 'not in', 'child_of']
INTERNAL_OPS = OPS + ['inselect']


OPS_ALIAS = {}


class expression(object):
    """
    parse a domain expression
    use a real polish notation
    leafs are still in a ('foo', '=', 'bar') format
    For more info: http://christophe-simonis-at-tiny.blogspot.com/2008/08/new-new-domain-notation.html 
    """

    def _is_operator(self, element):
        return isinstance(element, (str, unicode)) and element in ['&', '|', '!']

    def _is_leaf(self, element, internal=False):
        return (isinstance(element, tuple) or isinstance(element, list)) \
           and len(element) == 3 \
           and (((not internal) and OPS_ALIAS.get(element[1], element[1]) in OPS) \
                or (internal and OPS_ALIAS.get(element[1], element[1]) in INTERNAL_OPS))

    def __execute_recursive_in(self, cr, s, f, w, ids, op, type):
        res = []
        if ids:
            if op in ['<','>','>=','<=']:
                cr.execute('SELECT "%s"'    \
                               '  FROM "%s"'    \
                               ' WHERE "%s" %s %%s' % (s, f, w, op), (ids[0],))
                res.extend([r[0] for r in cr.fetchall()])
            else:
                for i in range(0, len(ids), cr.IN_MAX):
                    subids = ids[i:i+cr.IN_MAX]
                    cr.execute('SELECT "%s"'    \
                               '  FROM "%s"'    \
                               ' WHERE "%s" in %%s' % (s, f, w),
                               (tuple(subids),))
                    res.extend([r[0] for r in cr.fetchall()])
        else:
            cr.execute('SELECT distinct("%s")'    \
                           '  FROM "%s" where "%s" is not null'  % (s, f, s)),
            res.extend([r[0] for r in cr.fetchall()])
        return res

    def __init__(self, exp):
        # check if the expression is valid
        if not reduce(lambda acc, val: acc and (self._is_operator(val) or self._is_leaf(val)), exp, True):
            raise ValueError('Bad domain expression: %r' % (exp,))
        self.__exp = exp
        self.__field_tables = {}  # used to store the table to use for the sql generation. key = index of the leaf
        self.__all_tables = set()
        self.__joins = []
        self.__main_table = None # 'root' table. set by parse()
        self.__DUMMY_LEAF = (1, '=', 1) # a dummy leaf that must not be parsed or sql generated

        # Check for unaccent first time expression is called
        if not config.get('unaccent', False):
            db_name = config.get('db_name', False)
            cursor = pooler.get_db_only(db_name).cursor()
            unaccent = has_unaccent(cursor)
            cursor.close()
            config['unaccent'] = unaccent


    def parse(self, cr, uid, table, context):
        """ transform the leafs of the expression """
        if not self.__exp:
            return self

        def _rec_get(ids, table, parent=None, left='id', prefix=''):
            if len(ids) < 9000 and table._parent_store and (not table.pool
            ._init):
# TODO: Improve where joins are implemented for many with '.', replace by:
# doms += ['&',(prefix+'.parent_left','<',o.parent_right),(prefix+'.parent_left','>=',o.parent_left)]
                doms = []
                for o in table.read(cr, uid, ids, ['parent_left',
                                                   'parent_right'],
                                    context=context):
                    if doms:
                        doms.insert(0, '|')
                    doms += ['&', ('parent_left', '<', o['parent_right']),
                             ('parent_left', '>=', o['parent_left'])]
                if prefix:
                    return [(left, 'in', table.search(cr, uid, doms, context=context))]
                return doms
            else:
                def rg(ids, table, parent):
                    if not ids:
                        return []
                    ids2 = table.search(cr, uid, [(parent, 'in', ids)], context=context)
                    return ids + rg(ids2, table, parent)
                return [(left, 'in', rg(ids, table, parent or table._parent_name))]

        self.__main_table = table
        self.__all_tables.add(table)

        i = -1
        while i + 1<len(self.__exp):
            i += 1
            e = self.__exp[i]
            if self._is_operator(e) or e == self.__DUMMY_LEAF:
                continue
            left, operator, right = e
            operator = OPS_ALIAS.get(operator, operator)
            operator = operator.lower()
            working_table = table
            main_table = table
            fargs = left.split('.', 1)
            if left in table._inherit_fields:
                while True:
                    field = main_table._columns.get(fargs[0], False)
                    if field:
                        working_table = main_table
                        self.__field_tables[i] = working_table
                        break
                    working_table = main_table.pool.get(main_table._inherit_fields[left][0])
                    if working_table not in self.__all_tables:
                        self.__joins.append(('%s.%s=%s.%s' % (working_table._table, 'id', main_table._table, main_table._inherits[working_table._name]), working_table._table))
                        self.__all_tables.add(working_table)
                    main_table = working_table
            
            field = working_table._columns.get(fargs[0], False)
            if not field:
                if left == 'id' and operator == 'child_of':
                    dom = _rec_get(right, working_table)
                    self.__exp = self.__exp[:i] + dom + self.__exp[i+1:]
                continue

            field_obj = table.pool.get(field._obj)
            if len(fargs) > 1:
                if field._type == 'many2one':
                    right = field_obj.search(cr, uid, [(fargs[1], operator, right)], context=context)
                    if right == []:
                        self.__exp[i] = ( 'id', '=', 0 )
                    else:
                        self.__exp[i] = (fargs[0], 'in', right)
                # Making search easier when there is a left operand as field.o2m or field.m2m
                if field._type in ['many2many','one2many']:
                    right = field_obj.search(cr, uid, [(fargs[1], operator, right)], context=context)
                    right1 = table.search(cr, uid, [(fargs[0],'in', right)], context=context)
                    if right1 == []:
                        self.__exp[i] = ( 'id', '=', 0 )
                    else:
                        self.__exp[i] = ('id', 'in', right1)
                continue

            if field._properties and ((not field.store) or field._fnct_search):

                # this is a function field
                if not field._fnct_search:
                    # the function field doesn't provide a search function and doesn't store
                    # values in the database, so we must ignore it : we generate a dummy leaf
                    self.__exp[i] = self.__DUMMY_LEAF
                else:
                    subexp = field.search(cr, uid, table, left, [self.__exp[i]], context=context)
                    # we assume that the expression is valid
                    # we create a dummy leaf for forcing the parsing of the resulting expression
                    self.__exp[i] = '&'
                    self.__exp.insert(i + 1, self.__DUMMY_LEAF)
                    for j, se in enumerate(subexp):
                        self.__exp.insert(i + 2 + j, se)

                # else, the value of the field is store in the database, so we search on it


            elif field._type == 'one2many':
                # Applying recursivity on field(one2many)
                if operator == 'child_of':
                    if isinstance(right, basestring):
                        ids2 = [x[0] for x in field_obj.name_search(cr, uid, right, [], 'like', context=context, limit=None)]
                    else:
                        ids2 = list(right)
                    if field._obj != working_table._name:
                        dom = _rec_get(ids2, field_obj, left=left, prefix=field._obj)
                    else:
                        dom = _rec_get(ids2, working_table, parent=left)
                    self.__exp = self.__exp[:i] + dom + self.__exp[i+1:]
                
                else:    
                    call_null = True
                    
                    if right:
                        if isinstance(right, basestring):
                            ids2 = [x[0] for x in field_obj.name_search(cr, uid, right, [], operator, context=context, limit=None)]
                            if ids2:
                                operator = 'in' 
                        else:
                            if not isinstance(right,list):
                                ids2 = [right]
                            else:
                                ids2 = right    
                        if not ids2:
                            if operator in ['like','ilike','in','=']:
                                #no result found with given search criteria
                                call_null = False
                                self.__exp[i] = ('id','=',0)
                            else:
                                call_null = True
                                operator = 'in' # operator changed because ids are directly related to main object
                        else:
                            call_null = False
                            o2m_op = 'in'
                            if operator in  ['not like','not ilike','not in','<>','!=']:
                                o2m_op = 'not in'
                            self.__exp[i] = ('id', o2m_op, self.__execute_recursive_in(cr, field._fields_id, field_obj._table, 'id', ids2, operator, field._type))
                    
                    if call_null:
                        o2m_op = 'not in'
                        if operator in  ['not like','not ilike','not in','<>','!=']:
                            o2m_op = 'in'                         
                        self.__exp[i] = ('id', o2m_op, self.__execute_recursive_in(cr, field._fields_id, field_obj._table, 'id', [], operator, field._type) or [0])      

            elif field._type == 'many2many':
                #FIXME
                if operator == 'child_of':
                    if isinstance(right, basestring):
                        ids2 = [x[0] for x in field_obj.name_search(cr, uid, right, [], 'like', context=context, limit=None)]
                    else:
                        ids2 = list(right)

                    def _rec_convert(ids):
                        if field_obj == table:
                            return ids
                        return self.__execute_recursive_in(cr, field._id1, field._rel, field._id2, ids, operator, field._type)

                    dom = _rec_get(ids2, field_obj)
                    ids2 = field_obj.search(cr, uid, dom, context=context)
                    self.__exp[i] = ('id', 'in', _rec_convert(ids2))
                else:
                    call_null_m2m = True
                    if right:
                        if isinstance(right, basestring):
                            res_ids = [x[0] for x in field_obj.name_search(cr, uid, right, [], operator, context=context)]
                            if res_ids:
                                operator = 'in'
                        else:
                            if not isinstance(right, list):
                                res_ids = [right]
                            else:
                                res_ids = right
                        if not res_ids:
                            if operator in ['like','ilike','in','=']:
                                #no result found with given search criteria
                                call_null_m2m = False
                                self.__exp[i] = ('id','=',0)
                            else: 
                                call_null_m2m = True
                                operator = 'in' # operator changed because ids are directly related to main object
                        else:
                            call_null_m2m = False
                            m2m_op = 'in'        
                            if operator in  ['not like','not ilike','not in','<>','!=']:
                                m2m_op = 'not in'
                            
                            self.__exp[i] = ('id', m2m_op, self.__execute_recursive_in(cr, field._id1, field._rel, field._id2, res_ids, operator, field._type) or [0])
                    if call_null_m2m:
                        m2m_op = 'not in'
                        if operator in  ['not like','not ilike','not in','<>','!=']:
                            m2m_op = 'in'                         
                        self.__exp[i] = ('id', m2m_op, self.__execute_recursive_in(cr, field._id1, field._rel, field._id2, [], operator,  field._type) or [0])

            elif field._type == 'many2one':
                if operator == 'child_of':
                    if isinstance(right, basestring):
                        ids2 = [x[0] for x in field_obj.name_search(cr, uid, right, [], 'like', limit=None)]
                    else:
                        ids2 = list(right)

                    self.__operator = 'in'
                    if field._obj != working_table._name:
                        dom = _rec_get(ids2, field_obj, left=left, prefix=field._obj)
                    else:
                        dom = _rec_get(ids2, working_table, parent=left)
                    self.__exp = self.__exp[:i] + dom + self.__exp[i+1:]
                else:
                    
                    def _get_expression(field_obj,cr, uid, left, right, operator, context=None):
                        if context is None:
                            context = {}
                        c = context.copy()
                        c['active_test'] = False
                        #Special treatment to ill-formed domains
                        operator = ( operator in ['<','>','<=','>='] ) and 'in' or operator
                        
                        dict_op = {'not in':'!=','in':'=','=':'in','!=':'not in','<>':'not in'}
                        if isinstance(right,tuple):
                            right = list(right)
                        if (not isinstance(right,list)) and operator in ['not in','in']:
                            operator = dict_op[operator]
                        elif isinstance(right,list) and operator in ['<>','!=','=']: #for domain (FIELD,'=',['value1','value2'])
                            operator = dict_op[operator]
                        res_ids = field_obj.name_search(cr, uid, right, [], operator, limit=None, context=c)
                        if not res_ids:
                             return ('id','=',0)
                        else:
                            right = map(lambda x: x[0], res_ids)
                            return (left, 'in', right)

                    m2o_str = False
                    if right:
                        if isinstance(right, basestring): # and not isinstance(field, fields.related):
                            m2o_str = True
                        elif isinstance(right,(list,tuple)):
                            m2o_str = True
                            for ele in right:
                                if not isinstance(ele, basestring): 
                                    m2o_str = False
                                    break
                    elif right == []:
                        m2o_str = False
                        if operator in ('not in', '!=', '<>'):
                            # (many2one not in []) should return all records
                            self.__exp[i] = self.__DUMMY_LEAF
                        else:
                            self.__exp[i] = ('id','=',0)
                    else:
                        new_op = '='
                        if operator in  ['not like','not ilike','not in','<>','!=']:
                            new_op = '!='
                        #Is it ok to put 'left' and not 'id' ?
                        self.__exp[i] = (left,new_op,False)

                    if m2o_str:
                        self.__exp[i] = _get_expression(field_obj,cr, uid, left, right, operator, context=context)
            else:
                # other field type
                # add the time part to datetime field when it's not there:
                if field._type == 'datetime' and self.__exp[i][2] and len(self.__exp[i][2]) == 10:
                    
                    self.__exp[i] = list(self.__exp[i])
                    
                    if operator in ('>', '>='):
                        self.__exp[i][2] += ' 00:00:00'
                    elif operator in ('<', '<='):
                        self.__exp[i][2] += ' 23:59:59'
                    
                    self.__exp[i] = tuple(self.__exp[i])
                        
                if field.translate:
                    if operator in ('like', 'ilike', 'not like', 'not ilike'):
                        right = '%%%s%%' % right

                    operator = (
                        operator == '=like' and 'like'
                        or operator == '=ilike' and 'ilike'
                        or operator
                    )

                    query1 = '( SELECT res_id'          \
                             '    FROM ir_translation'  \
                             '   WHERE name = %s'       \
                             '     AND lang = %s'       \
                             '     AND type = %s'

                    if operator.endswith('like') and config.get('unaccent'):
                        unaccent = lambda x: 'unaccent({})'.format(x)
                    else:
                        unaccent = lambda x: x
                    instr = unaccent('%s')
                    #Covering in,not in operators with operands (%s,%s) ,etc.
                    if operator in ['in','not in']:
                        instr = ','.join(['%s'] * len(right))
                        query1 += '     AND value ' + operator +  ' ' +" (" + instr + ")"   \
                             ') UNION ('                \
                             '  SELECT id'              \
                             '    FROM "' + working_table._table + '"'       \
                             '   WHERE "' + left + '" ' + operator + ' ' +" (" + instr + "))"
                    else:
                        query1_add = '''
                            AND {value} {operator} {instr})
                            UNION (SELECT id
                                   FROM {working}
                                   WHERE {left} {operator} {instr})
                        '''.format(value=unaccent('value'),
                                   operator=operator,
                                   instr=instr,
                                   working=working_table._table,
                                   left=unaccent(left))
                        query1 += query1_add

                    query2 = [working_table._name + ',' + left,
                              context.get('lang', False) or 'en_US',
                              'model',
                              right,
                              right,
                             ]

                    self.__exp[i] = ('id', 'inselect', (query1, query2))

        return self

    def __leaf_to_sql(self, leaf, table):
        if leaf == self.__DUMMY_LEAF:
            return ('(1=1)', [])
        left, operator, right = leaf
        operator = OPS_ALIAS.get(operator, operator)

        if operator == 'inselect':
            query = '(%s.%s in (%s))' % (table._table, left, right[0])
            params = right[1]
        elif operator in ['in', 'not in']:
            params = right and right[:] or []
            len_before = len(params)
            for i in range(len_before)[::-1]:
                if params[i] == False:
                    del params[i]

            len_after = len(params)
            check_nulls = len_after != len_before
            query = '(1=0)'
            
            if len_after:
                if left == 'id':
                    instr = ','.join(['%s'] * len_after)
                else:
                    instr = ','.join([table._columns[left]._symbol_set[0]] * len_after)
                query = '(%s.%s %s (%s))' % (table._table, left, operator, instr)
            else:
                # the case for [field, 'in', []] or [left, 'not in', []]
                if operator == 'in':
                    query = '(%s.%s IS NULL)' % (table._table, left)
                else:
                    query = '(%s.%s IS NOT NULL)' % (table._table, left)
            if check_nulls:
                query = '(%s OR %s.%s IS NULL)' % (query, table._table, left)
        else:
            params = []
            
            if right == False and (leaf[0] in table._columns)  and table._columns[leaf[0]]._type=="boolean"  and (operator == '='):
                query = '(%s.%s IS NULL or %s.%s = false )' % (table._table, left,table._table, left)
            elif (((right == False) and (type(right)==bool)) or (right is None)) and (operator == '='):
                query = '%s.%s IS NULL ' % (table._table, left)
            elif right == False and (leaf[0] in table._columns)  and table._columns[leaf[0]]._type=="boolean"  and (operator in ['<>', '!=']):
                query = '(%s.%s IS NOT NULL and %s.%s != false)' % (table._table, left,table._table, left)
            elif (((right == False) and (type(right)==bool)) or right is None) and (operator in ['<>', '!=']):
                query = '%s.%s IS NOT NULL' % (table._table, left)
            else:
                if left == 'id':
                    query = '%s.id %s %%s' % (table._table, operator)
                    params = right
                else:
                    like = operator in ('like', 'ilike', 'not like', 'not ilike')

                    op = (
                        operator == '=like' and 'like'
                        or operator == '=ilike' and 'ilike'
                        or operator
                    )
                    if left in table._columns:
                        format = like and '%s' or table._columns[left]._symbol_set[0]
                        if operator.endswith('like') and config.get('unaccent'):
                            unaccent = lambda x: 'unaccent({})'.format(x)
                        else:
                            unaccent = lambda x: x
                        column = '{}.{}'.format(table._table, left)
                        query = '(%s %s %s)' % (unaccent(column), op, unaccent(format))
                    else:
                        query = "(%s.%s %s '%s')" % (table._table, left, op, right)

                    add_null = False
                    if like:
                        if isinstance(right, str):
                            str_utf8 = right
                        elif isinstance(right, unicode):
                            str_utf8 = right.encode('utf-8')
                        else:
                            str_utf8 = str(right)
                        params = '%%%s%%' % str_utf8
                        add_null = not str_utf8
                    elif left in table._columns:
                        params = table._columns[left]._symbol_set[1](right)

                    if add_null:
                        query = '(%s OR %s IS NULL)' % (query, left)

        if isinstance(params, basestring):
            params = [params]
        return (query, params)


    def to_sql(self):
        stack = []
        params = []
        for i, e in reverse_enumerate(self.__exp):
            if self._is_leaf(e, internal=True):
                table = self.__field_tables.get(i, self.__main_table)
                q, p = self.__leaf_to_sql(e, table)
                params.insert(0, p)
                stack.append(q)
            else:
                if e == '!':
                    stack.append('(NOT (%s))' % (stack.pop(),))
                else:
                    ops = {'&': ' AND ', '|': ' OR '}
                    q1 = stack.pop()
                    q2 = stack.pop()
                    stack.append('(%s %s %s)' % (q1, ops[e], q2,))

        query = ' AND '.join(reversed(stack))
        joins = ' AND '.join(map(lambda j: j[0], self.__joins))
        if joins:
            query = '(%s) AND (%s)' % (joins, query)
        return (query, flatten(params))

    def get_tables(self):
        return ['"%s"' % t._table for t in self.__all_tables]



def get_model_name(pool, table):
    mapping = dict((k._table, k._name) for k in pool.obj_pool.values())
    return mapping.get(table, None)


def convert_order_string(order_string):
    order_by = []
    for item in order_string.split(','):
        # TODO: Better with RE
        order_by.append(item.strip().replace(' ', '.').lower())
    return tuple(order_by)


class ErpParser(Parser):
    def __init__(self, table, pool, cursor, uid, foreign_key=None):
        self.pool = pool
        self.cursor = cursor
        self.uid = uid

        super(ErpParser, self).__init__(table, foreign_key)

    def create_expressions(self, expression, column):
        model_name = get_model_name(self.pool, column.table._name)
        model = self.pool.get(model_name)

        field = model._columns.get(column.name)

        if expression[1] in ('like', 'ilike'):
            expression = (
                expression[0], expression[1], '%{}%'.format(expression[2])
            )

        if isinstance(field, fields.function) and field._fnct_search:
            fnct_search_expressions = field._fnct_search(
                model, self.cursor, self.uid, model, field, [expression], {}
            )

            res = []
            for exp in fnct_search_expressions:
                res += self.get_expressions(exp)

            return res
        elif isinstance(field, fields.many2one) and isinstance(expression[2], basestring):
            res = self.pool.get(field._obj).name_search(
                self.cursor, self.uid, expression[2], [], expression[1]
            )
            if res:
                return self.get_expressions(
                    ('{}.id'.format(expression[0]), 'in', [x[0] for x in res])
                )
            else:
                return self.get_expressions(
                    ('{}.id'.format(expression[0]), '=', 0)
                )

        else:
            if isinstance(field, fields.boolean):
                    if expression[2] == 1:
                        expression = (expression[0], expression[1], True)
                    elif expression[2] == 0:
                        expression = (expression[0], expression[1], False)
            if not isinstance(field, fields.boolean) and expression[2] is False:
                expression = (expression[0], expression[1], None)
            return super(ErpParser, self).create_expressions(expression, column)

    def get_field_from_table(self, table, field):
        model_name = get_model_name(self.pool, table._name)
        model = self.pool.get(model_name)

        perm_fields = ['id']
        if model._log_access:
            perm_fields += ['write_uid', 'write_date', 'create_uid',
                            'create_date']

        if field in model._columns or field in perm_fields:
            return super(ErpParser, self).get_field_from_table(table, field)
        elif field in model._inherit_fields:
            inherit_relation_field = model._inherit_fields[field][1]
            return self.get_field_from_related_table(
                [inherit_relation_field], field
            )


class OOQueryReader(object):
    def __init__(self, q, fields_function=None):
        self.fields_function = fields_function
        if self.fields_function is None:
            self.fields_function = []
        self.q = q

    def where(self, *args, **kwargs):
        multis = []
        sql = self.q.where(*args, **kwargs)
        self.q.cursor.execute(*sql)
        res = OrderedDict()
        for x in self.q.cursor.dictfetchall():
            res.setdefault(x['id'], [])
            res[x['id']].append(x)
        for ffield, function in self.fields_function.items():
            if function._multi and function._multi in multis:
                continue
            calc = function.get(
                self.q.cursor, self.q.object, res.keys(), ffield, self.q.uid
            )
            for fid, value in calc.items():
                if function._multi:
                    multis.append(function._multi)
                    for v in res[fid]:
                        v.update(value)
                else:
                    for v in res[fid]:
                        v[ffield] = value

        j = []
        for x in res.values():
            j.extend(x)
        return j

class OOQuery(OOQueryBase):
    def __init__(self, obj, cursor, uid):
        self.object = obj
        self.cursor = cursor
        self.uid = uid
        self.pool = pooler.get_pool(cursor.dbname)
        self.only_active = True
        super(OOQuery, self).__init__(obj._table, self.foreing_key)

    def create_parser(self):
        return ErpParser(
            self.table, self.pool, self.cursor, self.uid, self.foreign_key
        )

    def foreing_key(self, table, field):

        model = get_model_name(self.pool, table)
        definition = self.pool.get(model)._columns[field]

        relation = self.pool.get(definition._obj)
        fk = {
            'constraint_name': '{}_fk'.format(field),
            'table_name': table,
            'foreign_table_name': relation._table,
        }
        if definition._type == 'many2one':
            fk.update({
                'column_name': field,
                'foreign_column_name': 'id'
            })
        elif definition._type == 'one2many':
            fk.update({
                'column_name': 'id',
                'foreign_column_name': definition._fields_id
            })
        elif definition._type == 'many2many':
            fk.update({
                'column_name': 'id',
                'foreign_column_name': 'id'
            })

            field_path = self.parser.join_path[:-1] + [field]
            join_name = '.'.join(field_path)

            if not self.parser.joins_map.get(join_name, None):
                if isinstance(self.parser.join_on, Table):
                    from_table = self.parser.join_on
                else:
                    from_table = self.parser.join_on.right
                rel_table = Table(definition._rel)
                to_table = Table(self.pool.get(definition._obj)._table)

                join = self.parser.join_on.join(rel_table)
                join.condition = Equal(
                    getattr(from_table, 'id'),
                    getattr(rel_table, definition._id1)
                )

                join = join.join(to_table)
                join.condition = Equal(
                    getattr(rel_table, definition._id2),
                    getattr(to_table, 'id')
                )
                self.parser.joins_map[join_name] = join
        return fk

    def select(self, fields=None, **kwargs):
        if ('order_by' not in kwargs
            and 'group_by' not in kwargs
            and self.object._order):
            kwargs['order_by'] = convert_order_string(self.object._order)
        self.only_active = kwargs.pop('only_active', True)
        return super(OOQuery, self).select(fields, **kwargs)

    def read(self, cols=None, **kwargs):
        ff_fields = {}
        if cols is None:
            cols = []
        if 'id' not in cols:
            cols.insert(0, 'id')
        for field in cols[:]:
            field_def = self.object._columns.get(field)
            if isinstance(field_def, fields.function) and not field_def.store:
                ff_fields[field] = field_def
                cols.remove(field)

        q = self.select(cols, **kwargs)
        return OOQueryReader(q, ff_fields)

    def where(self, domain):
        sql = super(OOQuery, self).where(domain)

        # First we find all the tables where we select by active
        with_active_tables = [
            where.left.table
            for where in flatten(sql.where)
            if where.left.name == 'active'
        ]

        if self.only_active:
            # Add the active condition to the main table
            main_table = sql.from_[0]

            while not isinstance(main_table, Table):
                main_table = main_table.left

            if main_table not in with_active_tables:
                model = get_model_name(self.pool, main_table._name)
                if model:
                    if 'active' in self.pool.get(model)._columns:
                        sql.where.append(main_table.active == True)

            # Add the active conditions to the joined tables
            for j in sql.from_:
                # If we are not using the active value in the where
                if j.right not in with_active_tables:
                    model = get_model_name(self.pool, j.right._name)
                    if model:
                        if 'active' in self.pool.get(model)._columns:
                            j.condition = j.condition & (j.right.active == True)
        return sql


