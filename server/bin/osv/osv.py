# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    $Id$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

#
# OSV: Objects Services
#

import orm
import netsvc
import pooler
import copy
import sys
import traceback

from psycopg2 import IntegrityError, InternalError
from tools.func import wraps


module_list = []
module_class_list = {}
class_pool = {}

class except_osv(Exception):
    def __init__(self, name, value, exc_type='warning'):
        self.name = name
        self.exc_type = exc_type
        self.value = value
        self.args = (exc_type, name)
        super(except_osv, self).__init__(
            "%s -- %s\n\n%s" % (self.exc_type, self.name, self.value)
        )


class osv_pool(netsvc.Service):
   
    def check(f):
        @wraps(f)
        def wrapper(self, dbname, *args, **kwargs):
            try:
                if not pooler.get_pool(dbname)._ready:
                    raise except_osv('Database not ready', 'Currently, this database is not fully loaded and can not be used.')
                return f(self, dbname, *args, **kwargs)
            except orm.AccessError:
                tb_s = "AccessError\n" + "".join(
                    traceback.format_exception(*sys.exc_info())
                )
                self.logger.notifyChannel(
                    'web-services', netsvc.LOG_DEBUG, tb_s
                )
                raise
            except IntegrityError as inst:
                for key in self._sql_error.keys():
                    if key in inst[0]:
                        raise orm.SqlConstrainError(
                            'Constraint Error', self._sql_error[key]
                        )
                raise orm.SqlConstrainError('Integrity Error', inst[0])
            except InternalError as exc:
                if "read-only transaction" in exc.message:
                    raise except_osv(
                        'Error',
                        'Database in read only mode'
                    )
            except Exception:
                tb_s = "".join(traceback.format_exception(*sys.exc_info()))
                self.logger.notifyChannel('web-services', netsvc.LOG_ERROR, tb_s)
                raise

        return wrapper


    def __init__(self):
        self._ready = False
        self.obj_pool = {}
        self.module_object_list = {}
        self.created = []
        self._sql_error = {}
        self._store_function = {}
        self._init = True
        self._init_parent = {}
        self.logger = netsvc.Logger()
        netsvc.Service.__init__(self, 'object_proxy', audience='')
        self.exportMethod(self.obj_list)
        self.exportMethod(self.exec_workflow)
        self.exportMethod(self.execute)

    def init_set(self, cr, mode):
        different = mode != self._init
        if different:
            if mode:
                self._init_parent = {}
            if not mode:
                for o in self._init_parent:
                    self.get(o)._parent_store_compute(cr)
            self._init = mode
        
        self._ready = True
        return different
   
    def execute_cr(self, cr, uid, obj, method, *args, **kw):
        object = pooler.get_pool(cr.dbname).get(obj)
        if not object:
            raise except_osv('Object Error', 'Object %s doesn\'t exist' % str(obj))
        return getattr(object, method)(cr, uid, *args, **kw)
    
    @check
    def execute(self, db, uid, obj, method, *args, **kw):
        db, pool = pooler.get_db_and_pool(db)
        cr = db.cursor()
        service_name = 'sync.%s' % id(cr)
        try:
            try:
                res = pool.execute_cr(cr, uid, obj, method, *args, **kw)
                cr.commit()
                if netsvc.service_exist(service_name):
                    netsvc.SERVICES[service_name].commit()
            except Exception:
                cr.rollback()
                if netsvc.service_exist(service_name):
                    netsvc.SERVICES[service_name].rollback()
                raise
        finally:
            cr.close()
            if netsvc.service_exist(service_name):
                netsvc.SERVICES[service_name].close()
                del netsvc.SERVICES[service_name]
        return res

    def exec_workflow_cr(self, cr, uid, obj, method, *args):
        wf_service = netsvc.LocalService("workflow")
        return wf_service.trg_validate(uid, obj, args[0], method, cr)

    @check
    def exec_workflow(self, db, uid, obj, method, *args):
        cr = pooler.get_db(db).cursor()
        service_name = 'sync.%s' % id(cr)
        try:
            try:
                res = self.exec_workflow_cr(cr, uid, obj, method, *args)
                cr.commit()
                if netsvc.service_exist(service_name):
                    netsvc.SERVICES[service_name].commit()
            except Exception:
                cr.rollback()
                if netsvc.service_exist(service_name):
                    netsvc.SERVICES[service_name].rollback()
                raise
        finally:
            cr.close()
            if netsvc.service_exist(service_name):
                netsvc.SERVICES[service_name].close()
                del netsvc.SERVICES[service_name]
        return res

    def obj_list(self):
        return self.obj_pool.keys()

    # adds a new object instance to the object pool.
    # if it already existed, the instance is replaced
    def add(self, name, obj_inst):
        if name in self.obj_pool:
            del self.obj_pool[name]
        self.obj_pool[name] = obj_inst

        module = str(obj_inst.__class__)[6:]
        module = module[:len(module)-1]
        module = module.split('.')[0][2:]
        self.module_object_list.setdefault(module, []).append(obj_inst)

    # Return None if object does not exist
    def get(self, name):
        obj = self.obj_pool.get(name, None)
        return obj

    #TODO: pass a list of modules to load
    def instanciate(self, module, cr):
        res = []
        class_list = module_class_list.get(module, [])
        for klass in class_list:
            res.append(klass.createInstance(self, module, cr))
        return res


class osv_memory(orm.orm_memory):
    #__metaclass__ = inheritor
    def __new__(cls):
        module = str(cls)[6:]
        module = module[:len(module)-1]
        module = module.split('.')[0][2:]
        if not hasattr(cls, '_module'):
            cls._module = module
        module_class_list.setdefault(cls._module, []).append(cls)
        class_pool[cls._name] = cls
        if module not in module_list:
            module_list.append(cls._module)
        return None

    #
    # Goal: try to apply inheritancy at the instanciation level and
    #       put objects in the pool var
    #
    def createInstance(cls, pool, module, cr):
        parent_names = getattr(cls, '_inherit', None)
        if parent_names:
            if isinstance(parent_names, (str, unicode)):
                name = cls._name or parent_names
                parent_names = [parent_names]
            else:
                name = cls._name
            if not name:
                raise TypeError('_name is mandatory in case of multiple inheritance')

            for parent_name in ((type(parent_names)==list) and parent_names or [parent_names]):
                parent_class = pool.get(parent_name).__class__
                assert pool.get(parent_name), "parent class %s does not exist in module %s !" % (parent_name, module)
                nattr = {}
                for s in ('_columns', '_defaults'):
                    new = copy.deepcopy(getattr(pool.get(parent_name), s))
                    if hasattr(new, 'update'):
                        new.update(cls.__dict__.get(s, {}))
                    else:
                        new.extend(cls.__dict__.get(s, []))
                    nattr[s] = new
                from six import text_type, binary_type, PY2
                if PY2 and isinstance(name, text_type):
                    name = binary_type(name)
                cls = type(name, (cls, parent_class), nattr)

        obj = object.__new__(cls)
        obj.__init__(pool, cr)
        return obj
    createInstance = classmethod(createInstance)

    def __init__(self, pool, cr):
        pool.add(self._name, self)
        self.pool = pool
        orm.orm_memory.__init__(self, cr)



class osv(orm.orm):
    #__metaclass__ = inheritor
    def __new__(cls):
        module = str(cls)[6:]
        module = module[:len(module)-1]
        module = module.split('.')[0][2:]
        if not hasattr(cls, '_module'):
            cls._module = module
        module_class_list.setdefault(cls._module, []).append(cls)
        class_pool[cls._name] = cls
        if module not in module_list:
            module_list.append(cls._module)
        return None

    #
    # Goal: try to apply inheritancy at the instanciation level and
    #       put objects in the pool var
    #
    def createInstance(cls, pool, module, cr):
        parent_name = hasattr(cls, '_inherit') and cls._inherit
        if parent_name:
            parent_class = pool.get(parent_name).__class__
            assert pool.get(parent_name), "parent class %s does not exist in module %s !" % (parent_name, module)
            nattr = {}
            for s in ('_columns', '_defaults', '_inherits', '_constraints', '_sql_constraints'):
                new = copy.deepcopy(getattr(pool.get(parent_name), s))
                if hasattr(new, 'update'):
                    new.update(cls.__dict__.get(s, {}))
                else:
                    if s=='_constraints':
                        for c in cls.__dict__.get(s, []):
                            exist = False
                            for c2 in range(len(new)):
                                #For _constraints, we should check field and methods as well
                                if new[c2][2]==c[2] and (new[c2][0] == c[0] \
                                        or getattr(new[c2][0],'__name__', True) == \
                                            getattr(c[0],'__name__', False)):
                                    new[c2] = c
                                    exist = True
                                    break
                            if not exist:
                                new.append(c)
                    else:
                        new.extend(cls.__dict__.get(s, []))
                nattr[s] = new
            name = hasattr(cls, '_name') and cls._name or cls._inherit
            if cls in parent_class.mro():
                cls = parent_class
            else:
                from six import text_type, binary_type, PY2
                if PY2 and isinstance(name, text_type):
                    name = binary_type(name)

                cls = type(name, (cls, parent_class), nattr)
        obj = object.__new__(cls)
        obj.__init__(pool, cr)
        return obj
    createInstance = classmethod(createInstance)

    def __init__(self, pool, cr):
        pool.add(self._name, self)
        self.pool = pool
        super(osv, self).__init__(cr)


class TransactionExecute(object):
    """Simple object to wrap the object service

    Every call is an independent transaction using osv serveices
    """
    def __init__(self, dbname, uid, model):
        self.dbname = dbname
        self.model = model
        self.api = osv_pool()
        self.uid = uid

    def __getattr__(self, method):
        def wrapper(*args, **kwargs):
            return self.api.execute(
                self.dbname,
                self.uid,
                self.model,
                method,
                *args,
                **kwargs
            )

        return wrapper


class OsvInherits(osv):
    def call_parent(self, method, field, *args, **kwargs):
        args = list(args)
        if len(args) >= 3:
            ids = args[2]
            if all([str(x).isdigit() for x in ids]):
                base_ids = [
                    x[field][0] for x in self.read(
                        args[0], args[1], ids, [field])
                ]
                args[2] = base_ids
            elif all([isinstance(x, orm.browse_record) for x in ids]):
                args[2] = [getattr(x, field) for x in ids]
        args = tuple(args)
        return method(*args, **kwargs)

    def __getattr__(self, item):
        for base, base_field in self._inherits.items():
            obj = self.pool.get(base)
            base = getattr(obj, item)
            if callable(base):
                return lambda *args, **kwargs: self.call_parent(
                    base, base_field, *args, **kwargs)
            else:
                return base


class Timescale(osv, orm.Timescale):
    pass
