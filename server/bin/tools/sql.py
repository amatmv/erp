# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    $Id$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


def drop_view_if_exists(cr, viewname):
    sql = "select count(1) from pg_class where relkind=%s and relname=%s"
    cr.execute(sql, ('v', viewname,))
    if cr.fetchone()[0]:
        cr.execute("DROP view %s" % (viewname,))


def install_array_agg(cursor):
    cursor.execute("select * from pg_proc where proname = 'array_agg'")
    rows = cursor.fetchall()
    if not rows:
        cursor.execute('''
            CREATE AGGREGATE array_agg(anyelement) (
                SFUNC=array_append,
                STYPE=anyarray,
                INITCOND='{}'
            )
        ''')
        return 1
    return 0


def is_postgis_db(cursor):
    try:
        cursor.savepoint('TESTPOSTGIS')
        cursor.execute('SELECT POSTGIS_FULL_VERSION()')
        ret = True
    except Exception:
        cursor.rollback('TESTPOSTGIS')
        ret = False
    finally:
        cursor.release('TESTPOSTGIS')
        return ret

def has_unaccent(cursor):
    query = "SELECT proname FROM pg_proc WHERE proname='unaccent'"
    cursor.execute(query)
    if len(cursor.fetchall()) > 0:
        return True
    else:
        return False


def get_primary_keys(cursor, table):
    cursor.execute("""SELECT c.column_name, tc.constraint_name
FROM information_schema.table_constraints tc
JOIN information_schema.constraint_column_usage AS ccu USING (constraint_schema, constraint_name)
JOIN information_schema.columns AS c ON c.table_schema = tc.constraint_schema
  AND tc.table_name = c.table_name AND ccu.column_name = c.column_name
WHERE constraint_type = 'PRIMARY KEY' and tc.table_name = %s
    """, (table, ))
    pkeys = {}
    for row in cursor.fetchall():
        pkeys.setdefault(row[1], [])
        pkeys[row[1]].append(row[0])
    return pkeys

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
