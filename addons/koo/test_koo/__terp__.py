# -*- coding: utf-8 -*-
{
    "name": "GISCE Modul to test koo client",
    "description": """Just for test""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Baixa Tensió",
    "depends":[
        "base"
    ],
    "init_xml": [

    ],
    "demo_xml":[],
    "update_xml": [
        "koo_test_data.xml",
        "koo_view.xml",
        "security/koo_groups_security.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
