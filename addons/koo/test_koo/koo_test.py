# -*- coding: utf-8 -*-
from osv import osv, fields


class KooTest(osv.osv):

    _name = "koo.test"
    _description = "Koo test element"

    def fn_char(self, cr, uid, ids, field_name, ar, context={}):
        ret = {}
        for identificador in ids:
            ret[int(identificador)] = ''
        return ret

    def fn_char_ple(self, cr, uid, ids, field_name, ar, context={}):
        ret = {}
        for identificador in ids:
            ret[int(identificador)] = 'Linux rules'
        return ret

    _defaults = {}

    _columns = {
        "name": fields.char("Nom", size=256),
        "name_ple": fields.char("Nom", size=256),
        "name_required": fields.char("Nom",size=256, required=True),
        "name_ro": fields.char("Nom read only",size=256,readonly=True),

        'fitxer': fields.binary("Fitxer"),
        'fitxer_ple': fields.binary("Fitxer ple"),
        'fitxer_required': fields.binary("Fitxer required", required=True),
        'fitxer_ro': fields.binary("Fitxer read only", readonly=True),

        'boolea': fields.boolean("Boolea"),
        'boolea_ple': fields.boolean("Boolea ple"),
        'boolea_required': fields.boolean("Boolea required", required=True),
        'boolea_ro': fields.boolean("Boolea read only", readonly=True),

        'data': fields.date("Data"),
        'data_ple': fields.date("Data ple"),
        'data_required': fields.date("Data required"),
        'data_ro': fields.date("Data read only"),

        'data_hora': fields.datetime("Data+hora"),
        'data_hora_ple': fields.datetime("Data+hora ple"),
        'data_hora_required': fields.datetime("Data+hora required", required=True),
        'data_hora_ro': fields.datetime("Data+hora read only", readonly=True),

        'decimal': fields.float("Decimal"),
        'decimal_ple': fields.float("Decimal ple"),
        'decimal_required': fields.float("Decimal required", required=True),
        'decimal_ro': fields.float("Decimal read only", readonly=True),

        'fn_char': fields.function(
            fn_char,
            string="Function(char)",
            type='char',
            method=True,
            size=256
        ),
        'fn_char_ple': fields.function(
            fn_char_ple,
            string="Function(char)",
            type='char',
            method=True,
            size=256
        ),
        'fn_char_ro': fields.function(
            fn_char_ple,
            string="Function(char)",
            type='char',
            readonly=True,
            method=True,
            size=256

        ),
        'fn_char_required': fields.function(
            fn_char_ple,
            string="Function(char)",
            type='char',
            required=True,
            method=True,
            size=256
        ),

        'enter': fields.integer("Enter"),
        'enter_ple': fields.integer("Enter ple"),
        'enter_required': fields.integer("Enter required"),
        'enter_ro': fields.integer("Enter read only"),
        'partner': fields.many2many(
            'res.partner', 'res_partner_koo_rel',
            'koo_id', 'partner_id',
            string="Many2Many(res.partner)"
        ),
        'partner_ple': fields.many2many(
            'res.partner', 'res_partner_koo_ple_rel',
            'koo_id', 'partner_id',
            string="Many2Many(res.partner) ple"
        ),
        'partner_required': fields.many2many(
            'res.partner', 'res_partner_koo_required_rel',
            'koo_id', 'partner_id',
            string="Many2Many(res.partner) required",
            required=True
        ),
        'partner_ro': fields.many2many(
            'res.partner', 'res_partner_koo_ro_rel',
            'koo_id', 'partner_id',
            string="Many2Many(res.partner) read only",
            readonly=True
        ),

        'texte': fields.text("Texte"),
        'texte_ple': fields.text("Texte"),
        'texte_required': fields.text("Texte required"),
        'texte_ro': fields.text("Texte read only"),

        "referencia": fields.reference(
            "Referencia",
            selection=[
                ('res.partner', 'Partner')
            ],
            size=256
        ),
        "referencia_ple": fields.reference(
            "Referencia ple",
            selection=[
                ('res.partner', 'Partner')
            ],
            size=256
        ),
        "referencia_ro": fields.reference(
            "Referencia read only",
            selection=[
                ('res.partner', 'Partner')
            ],
            size=256,
            readonly=True
        ),
        "referencia_required": fields.reference(
            "Referencia required",
            selection=[
                ('res.partner', 'Partner')
            ],
            size=256,
            required=True
        ),
        "relacionat": fields.related(
            'partner_ple', 'name',
            relation='res.partner',
            type='char', size=256, string='Relacionat'),

        "relacionat_ple": fields.related(
            'partner_ple', 'name',
            relation='res.partner',
            type='char', size=256, string='Relacionat'),
        "relacionat_ro": fields.related(
            'partner_ple', 'name',
            relation='res.partner',
            type='char', size=256, string='Relacionat'),
        "relacionat_required": fields.related(
            'partner_ple', 'name',
            relation='res.partner',
            type='char', size=256, string='Relacionat',
        ),

        "select": fields.selection(
            [
                ("1", "Si"),
                ("2", "No"),
                ("3", "No ho se"),


            ],
            string="Seleccionable"
        ),
        "select_ple": fields.selection(
            [
                ("1", "Si"),
                ("2", "No"),
                ("3", "No ho se"),

            ],
            string="Seleccionable ple"
        ),
        "select_ro": fields.selection(
            [
                ("1", "Si"),
                ("2", "No"),
                ("3", "No ho se"),

            ],
            string="Seleccionable read only"
        ),
        "select_required": fields.selection(
            [
                ("1", "Si"),
                ("2", "No"),
                ("3", "No ho se"),

            ],
            string="Seleccionable required"
        ),

        "one": fields.one2many(
            'koo.test.child', 'test_id',
            "Adreça"),
        "one_ple": fields.one2many(
            'koo.test.child', 'test_id',
            "Adreça ple"),
        "one_required": fields.one2many(
            'koo.test.child', 'test_id',
            "Adreça required", required=True),
        "one_ro": fields.one2many(
            'koo.test.child', 'test_id',
            "Adreça read only", readonly=True),

        "many": fields.many2one("res.partner", "Many"),
        "many_ple": fields.many2one("res.partner", "Many ple"),
        "many_ro": fields.many2one("res.partner", "Many read only", readonly=True),
        "many_required": fields.many2one("res.partner", "Many required", required=True),
    }


KooTest()


class KooTestChild(osv.osv):
    _name = "koo.test.child"
    _columns = {
        "name": fields.char("Nom", size=256),
        "test_id": fields.many2one("koo.test", "Koo Test")
    }


KooTestChild()