#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import re
import codecs
sys.stdout = codecs.getwriter('utf8')(sys.stdout)

HEADER ="""# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\\n"
"Report-Msgid-Bugs-To: \\n"
"POT-Creation-Date: 2010-12-21 03:35+0100\\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\\n"
"Language-Team: LANGUAGE <LL@li.org>\\n"
"Language: \\n"
"MIME-Version: 1.0\\n"
"Content-Type: text/plain; charset=utf-8\\n"
"Content-Transfer-Encoding: 8bit\\n"

"""

if __name__ == "__main__":
    jrxml = open(sys.argv[1], 'r').read()
    trans = re.findall("tr\(.*,\"(.*)\"(,.*)*\)", jrxml)
    po = ""
    po += HEADER
    for term in set(trans):
        po += "msgid \"%s\"\n" % unicode(term[0], 'utf-8').encode('utf-8')
        po += "msgstr \"\"\n\n"
    sys.stdout.write(unicode(po, 'utf-8'))
    sys.stdout.flush()
    
