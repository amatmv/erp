# -*- encoding: utf-8 -*-

from osv import osv, fields
import StringIO
import base64
from addons import get_module_resource
from datetime import datetime
from babel.dates import format_date


# wizard_multi_charts_accounts
class wizard_multi_charts_accounts(osv.osv_memory):
    _name = 'wizard.multi.charts.accounts'
    _inherit = 'wizard.multi.charts.accounts'

    def get_tax_values_from_template(self, cursor, uid, tax_template_id, context=None):
        tax_template_obj = self.pool.get('account.tax.template')
        res = {}

        if tax_template_id and isinstance(tax_template_id, (int, long)):
            res = super(
                wizard_multi_charts_accounts, self
            ).get_tax_values_from_template(
                cursor, uid, tax_template_id, context=context
            )

            reducido = tax_template_obj.read(
                cursor, uid, tax_template_id,
                ['is_impuesto_reducido'],
                context=context
            )['is_impuesto_reducido']

            res.update({
                'is_impuesto_reducido': reducido
            })

        return res

wizard_multi_charts_accounts()