# -*- encoding: utf-8 -*-

from osv import osv, fields
from tools.translate import _


class wizard_update_charts_accounts(osv.osv_memory):
    _name = 'wizard.update.charts.accounts'
    _inherit = 'wizard.update.charts.accounts'

    def _find_taxes_conditions(self, cursor, uid, tax, tax_template, notes, modified, context=None):
        notes, modified = super(
            wizard_update_charts_accounts, self
        )._find_taxes_conditions(
            cursor, uid, tax, tax_template, notes, modified, context=context
        )

        if tax.is_impuesto_reducido != tax_template.is_impuesto_reducido:
            notes += _("The is_impuesto_reducido field is different.\n")
            modified = True

        return notes, modified

    def _update_taxes_new_fields(self, cursor, uid, tax_template, context=None):
        res = super(
            wizard_update_charts_accounts, self
        )._update_taxes_new_fields(
            cursor, uid, tax_template, context=context
        )

        upd_dict = {
            'is_impuesto_reducido': tax_template.is_impuesto_reducido
        }
        res.update(upd_dict)

        return res

wizard_update_charts_accounts()