# -*- coding: utf-8 -*-
{
    "name": "Spain - Chart of Accounts 2008",
    "description": """Spanish General Chart of Accounts 2008

* Defines the following chart of account templates:
        * Spanish General Chart of Accounts 2008.
        * Spanish General Chart of Accounts 2008 for small and medium companies.
* Defines templates for sale and purchase VAT.
* Defines tax code templates.

Note: You should install the l10n_ES_account_balance_report module
for yearly account reporting (balance, profit & losses).
""",
    "version": "0-dev",
    "author": "Spanish Localization Team",
    "category": "Localisation/Account Charts",
    "depends":[
        "account",
        "base_vat",
        "base_iban",
        "account_chart_update"
    ],
    "init_xml":[
        "account_view.xml",
        "account_chart.xml",
        "taxes_data.xml",
        "fiscal_templates.xml",
        "account_chart_pymes.xml",
        "taxes_data_pymes.xml",
        "fiscal_templates_pymes.xml",
        "account_data.xml",
    ],
    "demo_xml":[
        "l10n_chart_ES_demo.xml"
    ],
    "update_xml": [],
    "active": False,
    "installable": True
}
