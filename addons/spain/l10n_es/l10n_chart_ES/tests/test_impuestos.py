# -*- coding: utf-8 -*-

from destral import testing
from destral.transaction import Transaction
from addons import get_module_resource
import xml.etree.cElementTree as et


class TestsIvaReducido(testing.OOTestCase):
    def test_iva_reducido_tax_templates_sets_correctly_from_data(self):
        with Transaction().start(self.database) as txn:
            pool = self.openerp.pool
            cursor = txn.cursor
            uid = txn.user
            ac_tax_tmp_obj = pool.get('account.tax.template')

            params_current = [('is_impuesto_reducido', '=', True)]

            params_expected = [
                ('amount', '!=', -0.21),
                ('amount', '!=', -0.18),
                '|',
                ('amount', '=', 1.00), # hadcoded line amounts wrong configurated
                ('amount', '<', 0.21),
                ('type', '=', 'percent'),
                ('template_name', 'not ilike', '%PYMES%'),
                ('name', 'not ilike', '%18%%'),
                ('name', 'not ilike', '%21%%'),
                ('amount', '!=', 0.18),
                '|', '|',
                ('name', 'ilike', '%iva %'),
                ('name', 'ilike', '% iva %'),
                ('name', 'ilike', '% iva%'),
            ]

            tax_reducido = sorted(
                ac_tax_tmp_obj.search(cursor, uid, params_current)
            )

            tax_expected_reducidos = sorted(
                ac_tax_tmp_obj.search(cursor, uid, params_expected)
            )

            self.assertEqual(tax_reducido, tax_expected_reducidos)

    def test_iva_reducido_tax_sets_correctly_from_data(self):
        with Transaction().start(self.database) as txn:
            pool = self.openerp.pool
            cursor = txn.cursor
            uid = txn.user
            imd_obj = pool.get('ir.model.data')
            ac_tax_obj = pool.get('account.tax')

            params_current = [('is_impuesto_reducido', '=', True)]

            tax_reducido_initial = sorted(
                ac_tax_obj.search(cursor, uid, params_current)
            )
            self.assertFalse(tax_reducido_initial)

            plan_contable_id = imd_obj.get_object_reference(
                cursor, uid, 'l10n_chart_ES', 'l10nES_chart_template'
            )[1]

            sig_wiz_obj = pool.get('wizard.update.charts.accounts')

            vals = {
                'lang': False,
                'code_digits': 6,
                'fiscal_position_ids': [],
                'chart_template_id': plan_contable_id,
                'company_id': 1,
                'update_children_accounts_parent': 1,
                'update_fiscal_position': 1,
                'update_tax': 1,
                'account_ids': [],
                'tax_code_ids': [],
                'continue_on_errors': 0,
                'update_account': 1,
                'update_tax_code': 1,
                'tax_ids': []
            }

            wiz_id = sig_wiz_obj.create(cursor, uid, vals)

            wizard = sig_wiz_obj.browse(cursor, uid, wiz_id)

            wizard.action_find_records()

            wizard.action_update_records()

            params_current = [('is_impuesto_reducido', '=', True)]

            params_expected = [
                ('amount', '!=', -0.21),
                ('amount', '!=', -0.18),
                '|',
                ('amount', '=', 1.00), # hadcoded line amounts wrong configurated
                ('amount', '<', 0.21),
                ('type', '=', 'percent'),
                ('name', 'not ilike', '%18%%'),
                ('name', 'not ilike', '%21%%'),
                ('amount', '!=', 0.18),
                ('amount', '!=', -0.18),
                '|', '|',
                ('name', 'ilike', '%iva %'),
                ('name', 'ilike', '% iva %'),
                ('name', 'ilike', '% iva%'),
            ]

            tax_reducido = sorted(
                ac_tax_obj.search(cursor, uid, params_current)
            )

            tax_expected_reducidos = sorted(
                ac_tax_obj.search(cursor, uid, params_expected)
            )

            self.assertEqual(tax_reducido, tax_expected_reducidos)

            tax_data_path = get_module_resource(
                'l10n_chart_ES', 'taxes_data.xml'
            )
            tree_xml = et.parse(tax_data_path)

            xml_records_list = tree_xml.findall(
                "data/record/field[@name='is_impuesto_reducido'][@eval='True']"
            )

            self.assertEqual(len(tax_reducido), len(xml_records_list))
