# -*- coding: utf-8 -*-

import logging


def up(cursor, installed_version):
    """Posem el camp is_impusto_reducido a l'estat que toca"""
    logger = logging.getLogger('openerp.migration')

    query_update_account_tax_templates = """
    UPDATE account_tax_template
    SET is_impuesto_reducido = True
    WHERE
        (ABS(amount) < 0.21 OR ABS(amount) = 1.00)
        AND
        ABS(amount) != 0.18
        AND
        type = 'percent'
        AND
        template_name NOT ILIKE '%PYMES%'
        AND
        (name NOT ILIKE '%18%%' AND name NOT ILIKE '%21%%')
        AND
        (name ilike '%iva %' OR name ilike '% iva %' OR name ilike '% iva%')
    """

    query_update_account_tax = """
        UPDATE account_tax
        SET is_impuesto_reducido = True
        WHERE
            (ABS(amount) < 0.21 OR ABS(amount) = 1.00)
            AND
            ABS(amount) != 0.18
            AND
            type = 'percent'
            AND
            (name NOT ILIKE '%18%%' AND name NOT ILIKE '%21%%')
            AND
            (name ilike '%iva %' OR name ilike '% iva %' OR name ilike '% iva%')
        """

    cursor.execute(query_update_account_tax_templates)
    cursor.execute(query_update_account_tax)

    logger.info('S\'han actualitzat els impostos')


def down(cursor, installed_version):
    pass

migrate = up
