# coding=utf-8
import logging
import pooler


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')

    pool = pooler.get_pool(cursor.dbname)
    uid = 1

    logger.info('Deleting unused account.tax.template for Inv. Sujeto Pasivo')
    acc_tax_template_obj = pool.get('account.tax.template')

    imd_obj = pool.get('ir.model.data')
    acc_tax_templ_ids = [
        imd_obj.get_object_reference(
            cursor, uid, 'l10n_chart_ES', acc_tax_template
        )[1]
        for acc_tax_template in [
            'iva_ISP_compras_21_1',
            'iva_ISP_compras_21_2',
            'iva_ISP_compras_21_3'
        ]
    ]
    acc_tax_template_obj.unlink(cursor, uid, acc_tax_templ_ids)

    logger.info('Deleting unused taxes Inv. Sujeto Pasivo')
    acc_tax_obj = pool.get('account.tax')
    acc_tax_ids = acc_tax_obj.search(
        cursor, uid, [('name', 'like', 'IVA 21% sujeto pasivo %')]
    )
    acc_tax_obj.unlink(cursor, uid, acc_tax_ids)

    acc_tax_ids = acc_tax_obj.search(
        cursor, uid, [('name', 'like', 'IVA 21% sujeto pasivo')]
    )
    if acc_tax_ids:
        logger.info('Updating inv. sujeto pasivo tax')
        acc_tax_obj.write(cursor, uid, acc_tax_ids, {
            'child_depend': False,
            'type_tax_use': 'sale',
            'amount': 0.0
        })

    logger.info('Migration successful!')


def down(cursor, installed_version):
    pass


migrate = up
