# coding=utf-8
from osv import osv
from datetime import datetime


class PaymentMandate(osv.osv):
    _name = 'payment.mandate'
    _inherit = 'payment.mandate'

    def alta_contracte(self, cursor, uid, res_id, context=None):
        if context is None:
            context = {}
        date_signature = datetime.now().strftime('%Y-%m-%d')

        self.write(cursor, uid, [res_id], {
            'signed': 1,
            'date': date_signature
        }, context=context)

        return True

    def bank_account_change(self, cursor, uid, res_id, context=None):
        if context is None:
            context = {}
        date_signature = datetime.now().strftime('%Y-%m-%d')

        self.write(cursor, uid, [res_id], {
            'signed': 1,
            'date': date_signature
        }, context=context)

        return True

    def process_signature_callback(self, cursor, uid, res_id, context=None):
        if not context:
            context = {}
        process_data = context.get('process_data', False)
        if process_data:
            method = process_data.get('callback_method', False)
            if method == 'alta_contracte':
                self.alta_contracte(cursor, uid, res_id, context=context)
            elif method == 'bank_account_change':
                self.bank_account_change(cursor, uid, res_id, context=context)

        return True

    def process_attachment_callback(self, cursor, uid, man_id, doc_filename,
                                    signature_id, signature_doc_id, model, context=None):
        if not context:
            context = {}
        process_data = context.get('process_data', False)
        if process_data:
            method = process_data.get('callback_method', False)
            if method == '':
                pass
            else:
                pro_obj = self.pool.get('giscedata.signatura.process')
                pro_obj.default_attach_document(
                    cursor, uid, man_id, signature_id,
                    signature_doc_id, doc_filename, model, context=context
                )


PaymentMandate()
