# -*- coding: utf-8 -*-
{
  "name": "Remesas (signaturit)",
  "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Integració amb signaturit mandatos
""",
  "version": "0-dev",
  "author": "GISCE",
  "category": "Master",
  "depends": [
      'l10n_ES_remesas',
      'l10n_ES_remesas_signatura',
      'giscedata_signatura_documents_signaturit'
  ],
  "init_xml": [],
  "demo_xml": [],
  "update_xml": [
      'wizard_signature_model_remesas.xml'
  ],
  "active": False,
  "installable": True
}
