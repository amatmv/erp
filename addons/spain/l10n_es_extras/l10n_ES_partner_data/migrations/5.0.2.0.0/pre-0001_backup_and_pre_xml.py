# -*- coding: utf-8 -*-

from addons.gisceupgrade_migratoor.gisceupgrade_migratoor import GisceUpgradeMigratoor
import netsvc

def migrate(cursor, installed_version):
    """Canvis a executar relatius al mòdul base
    """
    logger = netsvc.Logger()
    logger.notifyChannel('migration', netsvc.LOG_INFO, 'Migration from %s'
                         % installed_version)

    # backup
    mig = GisceUpgradeMigratoor('l10n_ES_partner_data', cursor)
    mig.backup(suffix='v4')
    search_keys = {
        'res.partner.function': ['code']
    }
    mig.pre_xml(search_keys)
