# encoding=utf-8
import logging


def up(cursor, installed_version):
    logger = logging.getLogger('openerp.migration')

    cursor.execute('''
        SELECT id from res_country
        where code = 'ES'
    ''')

    if cursor.rowcount:
        country_id = cursor.fetchone()[0]
        sql_base = '''
            UPDATE res_country_state SET name=%s 
            WHERE code=%s and country_id ={country_id}            
        '''.format(country_id=country_id)
        logger.info('Updating Araba -> Araba/Álava')
        cursor.execute(sql_base, ('Araba/Álava', '01'))
        logger.info('Updated rows: {}'.format(cursor.rowcount))

        logger.info('Updating Alacant -> Alicante/Alacant')
        cursor.execute(sql_base, ('Alicante/Alacant', '03'))
        logger.info('Updated rows: {}'.format(cursor.rowcount))

        logger.info('Updating Avila -> Ávila')
        cursor.execute(sql_base, ('Ávila', '05'))
        logger.info('Updated rows: {}'.format(cursor.rowcount))

        logger.info('Updating Castelló -> Castellón/Castelló')
        cursor.execute(sql_base, ('Castellón/Castelló', '12'))
        logger.info('Updated rows: {}'.format(cursor.rowcount))

        logger.info('Updating A Coruña -> Coruña, A')
        cursor.execute(sql_base, ('Coruña, A', '15'))
        logger.info('Updated rows: {}'.format(cursor.rowcount))

        logger.info('Updating Jaen -> Jaén')
        cursor.execute(sql_base, ('Jaén', '23'))
        logger.info('Updated rows: {}'.format(cursor.rowcount))

        logger.info('Updating Las Palmas -> Palmas, Las')
        cursor.execute(sql_base, ('Palmas, Las', '35'))
        logger.info('Updated rows: {}'.format(cursor.rowcount))

        logger.info('Updating La Rioja -> Rioja, La')
        cursor.execute(sql_base, ('Rioja, La', '26'))
        logger.info('Updated rows: {}'.format(cursor.rowcount))

        logger.info('Updating València -> Valencia/València')
        cursor.execute(sql_base, ('Valencia/València', '46'))
        logger.info('Updated rows: {}'.format(cursor.rowcount))

        logger.info('All names are update')
    else:
        logger.info('Country Spain is not found, skip updates')

migrate = up