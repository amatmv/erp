from addons.gisceupgrade_migratoor.gisceupgrade_migratoor import GisceUpgradeMigratoor
import netsvc

def migrate(cursor, installed_version):
    logger = netsvc.Logger()
    logger.notifyChannel('migration', netsvc.LOG_INFO, 'Migration from %s'
                         % installed_version)

    # backup
    mig = GisceUpgradeMigratoor('l10n_ES_toponyms', cursor)
    mig.pre_xml(
        xmlfiles=['l10n_ES_toponyms_states_official.xml'],
        force_xmlfiles=['l10n_ES_toponyms_states_official.xml'],
        search_keys={'res.country.state': ['code', 'country_id']}
    )
