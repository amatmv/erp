from __future__ import absolute_import

from destral import testing
from destral.transaction import Transaction
import base64
from lxml import objectify, etree
import netsvc
import unittest
import sepa
import os
from l10n_ES_remesas.wizard.export_remesas import NoInvoiceException
from l10n_ES_remesas.wizard.sepa19 import Sepa19Core as Sepa19


class ExportRemesesWizard(testing.OOTestCase):

    def test_create_wizard_export_file_ok(self):
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            pool = self.openerp.pool
            imd_obj = pool.get('ir.model.data')
            wiz_obj = pool.get('wizard.payment.file.spain')

            remesa1_id = imd_obj.get_object_reference(
                cursor, uid, 'account_payment', 'payment_order_demo'
            )[1]

            context = {'active_id': remesa1_id}
            wiz_id = wiz_obj.create(cursor, uid, {}, context=context)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)

            self.assertEqual(wiz.state, 'init')
            self.assertEqual(wiz.join, False)

            wiz_obj.create_payment_file(cursor, uid, [wiz_id], context=context)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            self.assertEqual(wiz.state, 'end')

    def test_get_invoice(self):
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            pool = self.openerp.pool
            imd_obj = pool.get('ir.model.data')

            payorder_obj = pool.get('payment.order')
            payline_obj = pool.get('payment.line')

            wiz_obj = pool.get('wizard.payment.file.spain')

            invoice_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'invoice_0001'
            )[1]
            account_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'a_sale'
            )[1]
            currency_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'EUR'
            )[1]
            remesa1_id = imd_obj.get_object_reference(
                cursor, uid, 'account_payment', 'payment_order_demo'
            )[1]

            remesa = payorder_obj.browse(cursor, uid, remesa1_id)

            remesa.mode.bank_id.write({'acc_number': '14910001202012341234'})
            remesa.mode.bank_id.partner_id.write({'vat': 'ES11111111H'})

            wf_service = netsvc.LocalService("workflow")
            wf_service.trg_validate(
                uid, 'account.invoice', invoice_id, 'invoice_open', cursor
            )
            # Create payment line
            # payment line with invoice
            vals = {
                'name': '000001/F',
                'order_id': remesa1_id,
                'currency': currency_id,
                'partner_id': remesa.mode.bank_id.partner_id.id,
                'company_currency': currency_id,
                'bank_id': remesa.mode.bank_id.id,
                'state': 'normal',
                'amount_currency': -1 * 100,
                'account_id': account_id,
                'communication': 'PAYMENT TEST LINE',
                'comm_text': 'PAYMENT TEST LINE',
                'invoice': invoice_id,
                'move_line_ids': [1]
            }

            payline_id = payline_obj.create(cursor, uid, vals)

            # Gets invoice from id
            with self.assertRaises(NoInvoiceException):
                wiz_obj.get_invoice(cursor, uid, payline_id)

            # Gets invoice from object
            payline = payline_obj.browse(cursor, uid, payline_id)
            with self.assertRaises(NoInvoiceException):
                wiz_obj.get_invoice(cursor, uid, payline)

    @unittest.skip("Needs better demo data")
    def test_export_file_ok(self):
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            pool = self.openerp.pool
            imd_obj = pool.get('ir.model.data')

            payorder_obj = pool.get('payment.order')
            payline_obj = pool.get('payment.line')

            wiz_obj = pool.get('wizard.payment.file.spain')

            invoice_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'invoice_0001'
            )[1]
            account_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'a_sale'
            )[1]
            currency_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'EUR'
            )[1]
            remesa1_id = imd_obj.get_object_reference(
                cursor, uid, 'account_payment', 'payment_order_demo'
            )[1]

            remesa = payorder_obj.browse(cursor, uid, remesa1_id)

            remesa.mode.bank_id.write({'acc_number': '14910001202012341234'})
            remesa.mode.bank_id.partner_id.write({'vat': 'ES11111111H'})

            wf_service = netsvc.LocalService("workflow")
            wf_service.trg_validate(
                uid, 'account.invoice', invoice_id, 'invoice_open', cursor
            )
            # Create payment line
            # payment line with invoice
            vals = {
                'name': '000001/F',
                'order_id': remesa1_id,
                'currency': currency_id,
                'partner_id': remesa.mode.bank_id.partner_id.id,
                'company_currency': currency_id,
                'bank_id': remesa.mode.bank_id.id,
                'state': 'normal',
                'amount_currency': -1 * 100,
                'account_id': account_id,
                'communication': 'PAYMENT TEST LINE',
                'comm_text': 'PAYMENT TEST LINE',
                'invoice': invoice_id,
                'move_line_ids': [1]
            }

            payline_id = payline_obj.create(cursor, uid, vals)

            context = {'active_id': remesa1_id}
            wiz_id = wiz_obj.create(cursor, uid, {}, context=context)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)

            self.assertEqual(wiz.state, 'init')
            self.assertEqual(wiz.join, False)

            # # Create payment file
            # wiz_obj.create_payment_file(cursor, uid, [wiz_id], context=context)
            # wiz = wiz_obj.browse(cursor, uid, wiz_id)
            #
            # xsd_file_path = os.path.join(
            #     os.path.dirname(os.path.abspath(sepa.__file__)),
            #     'xsd',
            #     'pain.008.001.02.xsd'
            # )
            # self.assertEqual(wiz.state, 'end')
            # self.assertEqual(
            #     wiz.pay_fname,
            #     'remesa_0001_payment_mode_Remesa 0001.xml'
            # )
            # # valid XML file
            # wiz_pay_xml = base64.decode(wiz.pay)
            #
            # schema = etree.XMLSchema(file=xsd_file_path)
            # parser = objectify.makeparser(schema=schema)
            # objectify.fromstring(wiz_pay_xml, parser)

    def test_invoice_pattern_remesa(self):
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            pool = self.openerp.pool
            imd_obj = pool.get('ir.model.data')
            config_obj = pool.get('res.config')

            payorder_obj = pool.get('payment.order')
            payline_obj = pool.get('payment.line')

            wiz_obj = pool.get('wizard.payment.file.spain')

            invoice_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'invoice_0001'
            )[1]
            account_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'a_sale'
            )[1]
            currency_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'EUR'
            )[1]
            remesa1_id = imd_obj.get_object_reference(
                cursor, uid, 'account_payment', 'payment_order_demo'
            )[1]

            remesa = payorder_obj.browse(cursor, uid, remesa1_id)

            remesa.mode.bank_id.write(
                {'acc_number': '14910001202012341234'})
            remesa.mode.bank_id.partner_id.write({'vat': 'ES11111111H'})

            wf_service = netsvc.LocalService("workflow")
            wf_service.trg_validate(
                uid, 'account.invoice', invoice_id, 'invoice_open', cursor
            )
            # Create payment line
            # payment line with invoice
            vals = {
                'name': '000001/F',
                'order_id': remesa1_id,
                'currency': currency_id,
                'partner_id': remesa.mode.bank_id.partner_id.id,
                'company_currency': currency_id,
                'bank_id': remesa.mode.bank_id.id,
                'state': 'normal',
                'amount_currency': -1 * 100,
                'account_id': account_id,
                'communication': 'PAYMENT TEST LINE',
                'comm_text': 'PAYMENT TEST LINE',
                'invoice': invoice_id,
                'move_line_id': 1
            }

            payline_id = payline_obj.create(cursor, uid, vals)

            invoice = wiz_obj.get_invoice(cursor, uid, payline_id)

            line = {
                'invoice': invoice,
                'communication': 'PAYMENT TEST LINE'
            }

            concept = Sepa19()._concept(pool, cursor, uid, line)

            expected_unstructured = '{} - Factura {} - {}'.format(invoice.name, invoice.number, invoice.company_id.name)
            self.assertEqual(concept.unstructured.value, expected_unstructured)

            new_pattern = '{invoice.name} {invoice.number} {invoice.state} {invoice.partner_id.name}'
            config_obj.set(cursor, uid, 'remesas_description_pattern', new_pattern)

            concept = Sepa19()._concept(pool, cursor, uid, line)
            expected_unstructured = '{} {} {} {}'.format(
                invoice.name,
                invoice.number,
                invoice.state,
                invoice.partner_id.name
            )
            self.assertEqual(concept.unstructured.value, expected_unstructured)
