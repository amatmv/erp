# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2006 ACYSOS S.L. (http://acysos.com) All Rights Reserved.
#                       Pedro Tarrafeta <pedro@acysos.com>
#    Copyright (c) 2008 Pablo Rocandio. All Rights Reserved.
#    Copyright (c) 2009 Zikzakmedia S.L. (http://zikzakmedia.com)
#                       All Rights Reserved.
#                       Jordi Esteve <jesteve@zikzakmedia.com>
#    $Id$
#
# Corregido para instalación TinyERP estándar 4.2.0: Zikzakmedia S.L. 2008
#   Jordi Esteve <jesteve@zikzakmedia.com>
#
# Añadidas cuentas de remesas y tipos de pago. 2008
#    Pablo Rocandio <salbet@gmail.com>
#
# Rehecho de nuevo para instalación OpenERP 5.0.0
# sobre account_payment_extension: Zikzakmedia S.L. 2009
#   Jordi Esteve <jesteve@zikzakmedia.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv, fields
import pooler
import wizard
import base64
from datetime import datetime
from tools.translate import _
from converter import *
import sepa19
import csb_19
import csb_32
import csb_34
import sepa34
import csb_34_ceca
import csb_58
from base_iban.base_iban import IBAN_LENGTH


# Generic class when payment line is not an invoice, i.e. aggregated invoice
class FakeInvoice(object):
    def __init__(self, mandate, date, name, number, company_id, description=None):
        '''
        :param mandate:
        :param date:
        :param name:
        :param number:
        :param company_id:
        :param description:
        '''
        self.mandate_id = mandate
        self.date_invoice = date
        self.name = name
        self.number = number
        self.company_id = company_id
        self.description = description


class NoInvoiceException(Exception):
    def __init__(self, message, line_id=None):
        self.line_id = line_id
        super(Exception, NoInvoiceException).__init__(self, message)


class WizardExportPaymentFile(osv.osv_memory):

    _name = 'wizard.payment.file.spain'

    def get_invoice(self, cursor, uid, line, context=None):
        """Gets invoice from payment line
        :param line: payment.line instance or id
        :return: Invoice object. May be an "account.invoice" instance or a
        FakeInvoice
        :raises: NoInvoiceException when invoice can not be found
        """
        line_obj = self.pool.get('payment.line')
        # Gets payment_line object if integer as line
        if isinstance(line, (int, long)):
            l = line_obj.browse(cursor, uid, line, context=context)
        else:
            l = line

        invoice = None
        try:
            if not l.ml_inv_ref:
                # This is a multipoint/aggrupped invoice
                last_invoice = None
                for move_line in l.move_line_id.move_id.line_id:
                    if move_line.invoice:
                        if l.order_id.type == 'payable':
                            # We don't need mandates
                            last_invoice = move_line.invoice
                            break
                        else:
                            if move_line.invoice.mandate_id:
                                last_invoice = move_line.invoice
                                break
                date_invoice = datetime.strptime(
                    l.create_date, '%Y-%m-%d %H:%M:%S'
                ).strftime('%Y-%m-%d')
                invoice = FakeInvoice(last_invoice.mandate_id,
                                      date_invoice,
                                      l.move_line_id.ref,
                                      l.move_line_id.ref,
                                      last_invoice.company_id,
                                      self.get_group_description(cursor, uid, line, context=context)
                                      )
            else:
                invoice = l.ml_inv_ref
        except Exception as e:
            if l.order_id.mode.tipo == 'sepa34' and not invoice:
                invoice = FakeInvoice('',
                                      l.date,
                                      l.name,
                                      str(l.id),
                                      '',
                                      l.name
                                      )
            else:
                # Can't find invoice
                raise NoInvoiceException(e, line_id=l.id or 0)
        return invoice

    def get_engine(self, cursor, uid, ids, tipo):
        if tipo == 'sepa19':
            csb = sepa19.Sepa19Core()
        elif tipo == 'csb_19':
            csb = csb_19.csb_19()
        elif tipo == 'csb_32':
            csb = csb_32.csb_32()
        elif tipo == 'csb_34':
            csb = csb_34.csb_34()
        elif tipo == 'sepa34':
            csb = sepa34.Sepa34Transfer()
        elif tipo == '34ceca':
            csb = csb_34_ceca.csb_34_ceca()
        elif tipo == 'csb_58':
            csb = csb_58.csb_58()
        else:
            raise Log(_(u"User error:\n\nThe payment mode is not "
                        u"CSB 19, CSB 32, CSB 34 or CSB 58"), True)
        return csb

    def get_group_description(self, cursor, uid, line, context=None):
        """ Ovwerwrite this method to give custom descriptions to grouped invoices"""
        return ''

    def create_payment_file(self, cr, uid, ids, context=None):

        if not context:
            context = {}
        txt_remesa = ''
        num_lineas_opc = 0
        wizard = self.browse(cr, uid, ids[0])
        payment_id = context.get('active_id', 0)

        try:
            pool = pooler.get_pool(cr.dbname)
            orden = pool.get('payment.order').browse(cr, uid,
                                                     payment_id, context)
            if not orden.line_ids:
                raise Log(_(u"User error:\n\nWizard can not generate export "
                             u"file, there are not payment lines."), True)

            # Comprobamos que exista número de C.C. y que tenga 20 dígitos
            if not orden.mode.bank_id:
                raise Log(_("User error:\n\nThe bank account of the company "
                             u"%s is not defined.")
                             % (orden.mode.partner_id.name), True)
            if orden.mode.bank_id.state == 'iban':
                cc = orden.mode.bank_id.iban
                cc_len = (cc[:2] in IBAN_LENGTH
                          and IBAN_LENGTH[cc[:2]] or len(cc))
            else:
                cc = digits_only(orden.mode.bank_id.acc_number)
                cc_len = 20
            if len(cc) != cc_len:
                raise Log(_(u"User error:\n\nThe bank account number "
                             u"of the company %s has not %s digits.")
                             % (orden.mode.partner_id.name, cc_len), True)

            # Comprobamos que exista el CIF de la compañía
            # asociada al C.C. del modo de pago
            if not orden.mode.bank_id.partner_id.vat:
                raise Log(_(u"User error:\n\nThe company VAT number related "
                            u"to the bank account of the payment mode "
                            u"is not defined."), True)

            recibos = []
            if wizard.join:
                # Lista con todos los partners+bancos diferentes de la remesa
                partner_bank_l = reduce((lambda l, x: x not in l
                                                      and l.append(x) or l),
                                         [(recibo.partner_id, recibo.bank_id)
                                          for recibo in orden.line_ids], [])
                # Cómputo de la lista de recibos
                # agrupados por mismo partner+banco.
                # Los importes se suman, los textos se concatenan
                # con un espacio en blanco y las fechas se escoge el máximo
                for partner, bank in partner_bank_l:
                    lineas = [recibo for recibo in orden.line_ids
                              if recibo.partner_id == partner
                                 and recibo.bank_id == bank]
                    amount = sum([l.amount for l in lineas])
                    if amount >= 0:
                        raise Log(
                            _(u"User error:\n\nThe grouped amount of "
                              u"lines %s for partner: %s is greater than 0")
                                % (', '.join([l.name for l in lineas]),
                                   partner.name), True)
                    recibos.append({
                        'partner_id': partner,
                        'bank_id': bank,
                        'name': partner.ref or l.name,
                        'amount': amount,
                        'communication': ' '.join(set([l.communication
                                                       for l in lineas
                                                       if l.communication])),
                        'communication2':  ' '.join(set([l.communication2
                                                     for l in lineas
                                                     if l.communication2])),
                        'date': max([l.date for l in lineas]),
                        'ml_maturity_date': max([l.ml_maturity_date
                                                 for l in lineas]),
                        'create_date': max([l.create_date for l in lineas]),
                        'ml_date_created': max([l.ml_date_created
                                                for l in lineas]),
                        'invoice_number': '.'.join(set([l.ml_inv_ref.number]))
                    })
            else:
                # Cada línea de pago es un recibo
                for l in orden.line_ids:
                    invoice = self.get_invoice(cr, uid, l, context=context)

                    recibos.append({
                        'partner_id': l.partner_id,
                        'bank_id': l.bank_id,
                        'name': l.ml_inv_ref.name or l.name,
                        'amount': l.amount,
                        'communication': l.name + ' ' + l.communication,
                        'communication2': l.communication2,
                        'date': l.date,
                        'ml_maturity_date': l.ml_maturity_date,
                        'create_date': l.create_date,
                        'ml_date_created': l.ml_date_created,
                        'comm_text': l.comm_text,
                        # invoice_number is the EndToEndId in the bank order
                        'invoice_number': l.ml_inv_ref.number or invoice.number,
                        'invoice': invoice,
                        'ml': l
                    })

            if orden.mode.require_bank_account:
                for line in recibos:
                    if line['bank_id']:
                        if (line['bank_id'].state == 'iban'
                            and line['bank_id'].iban):
                            ccc = line['bank_id'].iban
                            cc_len = (ccc[:2] in IBAN_LENGTH
                                      and IBAN_LENGTH[ccc[:2]] or len(ccc))
                        elif (line['bank_id'].state == 'bank'
                              and line['bank_id'].acc_number):
                            ccc = digits_only(line['bank_id'].acc_number)
                            cc_len = 20
                        else:
                            ccc = False
                    else:
                        ccc = False
                    if not ccc:
                        raise Log(_(u"User error:\n\nThe bank account number "
                                    u"of the customer %s is not defined and "
                                    u"current payment mode enforces all lines "
                                    u"to have a bank account.")
                                  % (line['partner_id'].name), True)
                    if len(ccc) != cc_len:
                        raise Log(_(u"User error:\n\nThe bank account number "
                                    u"of the customer %s has not %s digits.")
                                  % (line['partner_id'].name, cc_len), True)

            csb = self.get_engine(cr, uid, ids, orden.mode.tipo)
            result = csb.create_file(pool, cr, uid, orden,
                                         recibos, context)

        except Log, log:
            wizard.write({'note': log(),
                          'error': True,
                          'state': 'end'})
            return True
        else:
            # Ensure line breaks use MS-DOS (CRLF) format as standards require.
            file_type = getattr(csb, '_file_type', 'txt')
            if not isinstance(result, dict):
                result = {'': result}

            if len(result) > 1:
                import zipfile
                from StringIO import StringIO
                f = StringIO()
                zf = zipfile.ZipFile(f, 'w')

            for rtype, txt_remesa in result.items():
                txt_remesa = txt_remesa.replace('\r\n', '\n').replace('\n', '\r\n')
                fname = (
                    _('remesa') + '_' + rtype + '_' + orden.mode.tipo + '_'
                    + orden.reference + '.' + file_type
                ).replace('/', '-')
                if file_type == 'txt':
                    txt_remesa = unicode(txt_remesa).encode('iso-8859-1',
                                                            'replace')
                if len(result) > 1:
                    zf.writestr(fname, txt_remesa)
                file = base64.encodestring(txt_remesa)
                pool.get('ir.attachment').create(cr, uid, {
                    'name': _('Remesa ') + orden.mode.tipo + ' ' + orden.reference + ' (' + rtype + ')',
                    'datas': file,
                    'datas_fname': fname,
                    'res_model': 'payment.order',
                    'res_id': orden.id,
                    }, context=context)

            log = (_(u"Successfully Exported\n\nSummary:\n"
                     u"Total amount paid: %.2f\n"
                     u"Total Number of Payments: %d\n")
                     % (-orden.total, len(recibos)))
            pool.get('payment.order').set_done(cr, uid, orden.id, context)

            if len(result) > 1:
                zf.close()
                file = base64.encodestring(f.getvalue())
                f.close()
                fname = fname = (
                    _('remesa') + '_' + orden.mode.tipo + '_'
                    + orden.reference + '.zip'
                ).replace('/', '-')

            wizard.write({
                'note': log,
                'pay': file,
                'pay_fname': fname,
                'state': 'end'
            })
        return True

    _state_selection = [('init', 'Init'),
                        ('end', 'End')]

    _columns = {
        'join': fields.boolean(u"Join payment lines of the "
                               u"same partner and bank account"),
        'pay': fields.binary('Payment order file', readonly=True),
        'pay_fname': fields.char('File name', size=64),
        'note': fields.text('Log', readonly=True),
        'error': fields.boolean('Error'),
        'state': fields.selection(_state_selection, 'State')
    }

    _defaults = {
        'state': lambda *a: 'init',
        'error': lambda *a: False,
    }

WizardExportPaymentFile()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
