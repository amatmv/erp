# -*- encoding: utf-8 -*-

import re

from datetime import datetime
from tools.translate import _
from converter import *
from tools import config
from copy import deepcopy
from lxml import etree

from sepa import sepa19


def vat_enterprise(vat):
    '''checks if we have an enterprise or
    a person depending on vat value'''
    vat_value = len(vat) == 9 and vat or vat[2:]
    enterprise = ('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                  'J', 'U', 'V', 'N', 'P', 'Q', 'R', 'S', 'W')
    if vat_value[0] in enterprise:
        return True
    return False


def format_address_line(address):
    street = address.street or ''
    zip_code = address.zip or ''
    city = address.city or ''
    address_line = u'%s %s (%s)' % (street, zip_code, city)
    return to_ascii(address_line)[:70]


class Sepa19Core:

    _file_type = 'xml'

    def _generate_message_id(self, pool, cr, uid, type_='CORE'):
        #CIF Codi remesa data creacio remesa
        msg_id = u'%s%s%s%s' % (
            self.order.mode.cif, self.order.date_planned, type_,
            self.order.reference
        )
        msg_id = re.sub(r'\W+', '', msg_id)
        msg_id = to_ascii(msg_id)[-35:]
        if self.order.mode.financiado:
            msg_id = u'FSDD{}'.format(msg_id)
        return msg_id

    def _generate_pay_info_id(self, pool, cr, uid, line, line_num):
        pay_info_raw_id = "%s/%s" % (line['name'], line['invoice_number'])
        pay_info_raw_id = to_ascii(pay_info_raw_id)
        pay_info_id = re.sub(r'[^a-zA-Z0-9_/]+', '', pay_info_raw_id).lower()
        return pay_info_id[-35:]

    def _generate_end_to_end_pay_id(self, pool, cr, uid, line,
                                    line_num, subline_num):
        end_to_end_id = to_ascii(line['invoice_number'])[-35:]
        return end_to_end_id

    def _initiating_party(self, pool, cr, uid):
        address_dict = self.order.mode.partner_id.address_get(['contact'])
        address_id = address_dict['contact']

        address_obj = pool.get('res.partner.address')
        address = address_obj.browse(cr, uid, address_id)

        postal_address = sepa19.PostalAddress()
        country = unicode(address.country_id and address.country_id.code or '')
        postal_address_fields = {
            'country': to_ascii(country),
            'address_line': format_address_line(address)
        }
        postal_address.feed(postal_address_fields)

        other_identification = sepa19.OtherLegalEntity()
        other_identification_fields = {
            'identification': self.order.mode.sepa_creditor_code
        }
        other_identification.feed(other_identification_fields)

        org_identification = sepa19.PhysicalLegalEntity('OrgId')
        org_identification_fields = {
            'other': other_identification
        }
        org_identification.feed(org_identification_fields)

        identification = sepa19.GenericPhysicalLegalEntityId()
        identification_fields = {
            'legal_entity': org_identification
        }
        identification.feed(identification_fields)

        init_party = sepa19.GenericPhysicalLegalEntity('InitgPty')
        init_party_fields = {
            'entity_name': to_ascii(self.order.mode.nombre),
            'postal_address': postal_address,
            'identification': identification
        }
        init_party.feed(init_party_fields)

        return init_party

    def _sepa_header(self, pool, cr, uid):
        iso_today = str(datetime.now())[:-7].replace(' ', 'T')

        header = sepa19.SepaHeader()
        header_fields = {
            'message_id': self._generate_message_id(pool, cr, uid),
            'creation_date_time': iso_today,
            'number_of_operations': str(self.n_lines),
            'checksum': "%.2f" % abs(self.order.total),
            'initiating_party': self._initiating_party(pool, cr, uid)
        }
        header.feed(header_fields)
        return header

    def _sepa_header_lines(self, pool, cr, uid, lines, type_='CORE'):
        iso_today = str(datetime.now())[:-7].replace(' ', 'T')

        header = sepa19.SepaHeader()
        total_amount = sum(l['amount'] for l in lines)
        header_fields = {
            'message_id': self._generate_message_id(pool, cr, uid, type_),
            'creation_date_time': iso_today,
            'number_of_operations': str(len(lines)),
            'checksum': "%.2f" % abs(total_amount),
            'initiating_party': self._initiating_party(pool, cr, uid)
        }
        header.feed(header_fields)
        return header

    def _creditor_info(self, pool, cr, uid, line):
        creditor_info = {}

        creditor = sepa19.NameAndAddress('Cdtr')
        creditor_fields = {
            'name_name': to_ascii(self.order.mode.partner_id.name)
        }

        creditor.feed(creditor_fields)

        if self.order.mode.bank_id.iban:
            iban = self.order.mode.bank_id.iban
        else:
            creditor_name = self.order.mode.partner_id.name
            raise Log((_(u"Missing IBAN on creditor %s")
                       % creditor_name), True)
        account_id = sepa19.AccountIdentification()
        account_id_fields = {
            'iban': to_ascii(iban)
        }
        account_id.feed(account_id_fields)

        account = sepa19.BankAccount('CdtrAcct')
        account_fields = {
            'account_identification': account_id
        }
        account.feed(account_fields)

        if self.order.mode.bank_id.bank.bic:
            bic = self.order.mode.bank_id.bank.bic
        else:
            bank_name = self.order.mode.bank_id.bank.name
            raise Log((_(u"Missing creditor bank BIC: %s")
                       % bank_name), True)

        agent_id = sepa19.AgentIdentifier()
        agent_id_fields = {
            'bic': to_ascii(bic)
        }
        agent_id.drop_empty = False
        agent = sepa19.BankAgent('CdtrAgt')
        agent_id.feed(agent_id_fields)

        agent_fields = {
            'agent_identifier': agent_id
        }
        agent.drop_empty = False
        agent.feed(agent_fields)

        # Creditor identifier
        schema_name = sepa19.SchemeName()
        schema_name_fields = {
            'propietary': 'SEPA',  # Hardcoded as rule book says for 2.27
        }
        schema_name.feed(schema_name_fields)

        other = sepa19.OtherLegalEntity()
        other_fields = {
            'identification': self.order.mode.sepa_creditor_code,
            'scheme_name': schema_name,
        }
        other.feed(other_fields)

        physical_person = sepa19.PhysicalLegalEntity('PrvtId')
        physical_person_fields = {
            'other': other,
        }
        physical_person.feed(physical_person_fields)

        identification = sepa19.GenericPhysicalLegalEntityId()
        identification_fields = {
            'physical_person': physical_person,
        }
        identification.feed(identification_fields)

        identifier = sepa19.GenericPhysicalLegalEntity('CdtrSchmeId')
        identifier_fields = {
            'identification': identification,
        }
        identifier.feed(identifier_fields)

        creditor_info['creditor'] = creditor
        creditor_info['account'] = account
        creditor_info['agent'] = agent
        creditor_info['identifier'] = identifier

        return creditor_info

    def _get_debtor_entity(self, line):
        return to_ascii(line['partner_id'].name)

    def _debtor_info(self, pool, cr, uid, line):
        debtor_info = {}

        debtor = sepa19.GenericPhysicalLegalEntity('Dbtr')

        entity_name = self._get_debtor_entity(line)

        debtor_fields = {
            'entity_name': entity_name,
        }

        if line['bank_id'].iban:
            iban = line['bank_id'].iban
        else:
            debtor_name = line['partner_id'].name
            raise Log((_(u"Missing IBAN on debtor %s")
                       % debtor_name), True)
        account_id = sepa19.AccountIdentification()
        account_id_fields = {
            'iban': to_ascii(iban)
        }
        account = sepa19.BankAccount('DbtrAcct')
        account_id.feed(account_id_fields)
        account_fields = {
            'account_identification': account_id
        }

        if self.insert_bic == 1:
            if line['bank_id'].bank.bic:
                bic = line['bank_id'].bank.bic
            else:
                debtor_name = line['partner_id'].name
                raise Log((_(u"Missing debtor bank BIC: %s")
                           % debtor_name), True)
        elif self.insert_bic == 0:
                bic = self.empty_bic
        elif self.insert_bic == 2:
            if line['bank_id'].bank and line['bank_id'].bank.bic:
                bic = line['bank_id'].bank.bic
            else:
                bic = self.empty_bic
        else:
            raise Log((_(u"Value %s configuration for "
                         u"insert_bic_code is not supported ")
                       % self.insert_bic), True)
        agent_id = sepa19.AgentIdentifier()
        agent_id_fields = {
            'bic': to_ascii(bic or '')
        }
        agent = sepa19.BankAgent('DbtrAgt')
        agent.drop_empty = False
        agent_id.feed(agent_id_fields)
        agent_fields = {
            'agent_identifier': agent_id
        }
        debtor.feed(debtor_fields)
        agent.feed(agent_fields)
        account.feed(account_fields)

        debtor_info['debtor'] = debtor
        debtor_info['agent'] = agent
        debtor_info['account'] = account
        return debtor_info

    def pattern_remesa_description_parse_config_var(self, cursor, uid, invoice, context=None):
        import pooler
        res_config = pooler.get_pool(cursor.dbname).get('res.config')
        add_sector = int(res_config.get(
            cursor, uid, 'remesas_add_sector', 0
        ))
        invoice.factura_sector = ""
        invoice.name_add_info = ""
        if add_sector:
            if hasattr(invoice, "journal_id") and invoice.journal_id.code.startswith("ENERGIA"):
                invoice.factura_sector = _("Electricidad ")
                invoice.name_add_info = _("Contrato ")
            elif hasattr(invoice, "journal_id") and invoice.journal_id.code.startswith("GAS"):
                invoice.factura_sector = _("Gas ")
                invoice.name_add_info = _("Contrato ")

        default_pattern = (
            '{invoice.name_add_info}{invoice.name} - Factura {invoice.factura_sector}{invoice.number} '
            '- {invoice.company_id.name}'
        )
        invoice_pattern = str(
            res_config.get(
                cursor, uid,
                'remesas_description_pattern', default_pattern
                )
        )

        return invoice_pattern.format(invoice=invoice)

    def _concept(self, pool, cr, uid, line):
        u_concept = line['communication']
        invoice = line.get('invoice')
        if invoice:
            from l10n_ES_remesas.wizard.export_remesas import FakeInvoice
            if isinstance(invoice, FakeInvoice) and invoice.description:
                u_concept = invoice.description
            else:
                u_concept = self.pattern_remesa_description_parse_config_var(
                    cr, uid, invoice
                )

        concept = sepa19.Concept()
        concept_fields = {
            'unstructured': u_concept
        }
        concept.feed(concept_fields)
        return concept

    def _mandate(self, pool, cr, uid, line, creditor_id):
        invoice = line['invoice']

        if not invoice.mandate_id:
            if invoice.number:
                err = invoice.number
            else:
                err = line['communication']
            raise Log(_(u"Missing mandate for invoices in line {}".format(err)),
                      True)

        mandate_info = sepa19.MandateInformation()
        mandate_fields = {
            'mandate_identifier': invoice.mandate_id.name,
            'date_of_sign': invoice.mandate_id.date,
            'modification_indicator': 'false',
        }

        mandate_info.feed(mandate_fields)
        operation = sepa19.DirectDebitOperation()
        operation_fields = {
            'mandate_information': mandate_info,
            'creditor_identifier': creditor_id,
        }
        operation.feed(operation_fields)
        return operation

    def _operation_info(self, pool, cr, uid, lines, line_num, creditor_id):
        payment_type_info = {}
        operation_info = {}
        operations = {}
        checksum = {}
        num_op = {}

        if not isinstance(lines, list):
            lines = [lines]

        for op in lines:
            creditor = deepcopy(creditor_id)
            mandate = self._mandate(pool, cr, uid, op, creditor)
            pay_type_info = self._payment_type_info(pool, cr, uid, op)
            pay_type = pay_type_info.sequence_type.value
            payment_type_info.setdefault(pay_type, pay_type_info)
            num_op.setdefault(pay_type, 0)
            checksum.setdefault(pay_type, 0)
            operations.setdefault(pay_type, [])

            i = num_op[pay_type]
            end_to_end_id = self._generate_end_to_end_pay_id(pool, cr,
                                                             uid, op,
                                                             line_num, i)
            payment_id = sepa19.PaymentIdentifier()
            payment_id_fields = {
                'end_to_end_identifier': end_to_end_id
            }
            payment_id.feed(payment_id_fields)
            debtor_info = self._debtor_info(pool, cr, uid, op)
            concept = self._concept(pool, cr, uid, op)
            operation = sepa19.DirectDebitOperationInfo()
            operation_fields = {
                'payment_identifier': payment_id,
                'instructed_amount': "%.2f" % -op['amount'],
                'direct_debit_operation': mandate,
                'debtor_agent': debtor_info['agent'],
                'debtor': debtor_info['debtor'],
                'debtor_account': debtor_info['account'],
                'concept': concept,
            }

            if 'currency' in op and op['currency'].name:
                currency = op['currency'].name
            else:
                currency = 'EUR'
            operation.set_currency(currency)

            operation.feed(operation_fields)
            operation.build_tree()
            operation_str = etree.tostring(operation.doc_root,
                                          xml_declaration=False,
                                          encoding=operation.xml_enc)
            operations[pay_type].append(operation_str)
            checksum[pay_type] += -op['amount']
            num_op[pay_type] += 1

        for pti in operations.keys():
            operation_info[pti] = {}
            operation_info[pti]['operations'] = operations[pti]
            operation_info[pti]['number_of_operations'] = num_op[pti]
            operation_info[pti]['checksum'] = checksum[pti]
            operation_info[pti]['payment_type_info'] = payment_type_info[pti]

        return operation_info

    def _missing_mandates(self, pool, cr, uid, order_id):
        #Busca factures sense mandato
        _queryfile = ("%s/l10n_ES_remesas/sql/"
                      "mandate_missing.sql") % config['addons_path']
        _query = open(_queryfile).read()
        cr.execute(_query, (order_id,))
        factures = cr.fetchall()
        return ["%s (%s)" % (f[3], f[2]) for f in factures if f[3]]

    def _missing_banks(self, pool, cr, uid, order_id):
        #Busca factures sense mandato
        _queryfile = ("%s/l10n_ES_remesas/sql/"
                      "bank_missing.sql") % config['addons_path']
        _query = open(_queryfile).read()
        cr.execute(_query, (order_id,))
        factures = cr.fetchall()
        return ["%s (%s)" % (f[2], f[3]) for f in factures if f[3]]

    def _payment_type_info(self, pool, cr, uid, line):
        invoice = line['invoice']
        mandate = invoice.mandate_id

        _queryfile = ("%s/l10n_ES_remesas/sql/"
                      "mandate_payment_count.sql") % config['addons_path']
        _query = open(_queryfile).read()

        cr.execute(_query, (invoice.date_invoice, mandate.id, ))
        use_count = cr.fetchall()

        if use_count:
            use_count = use_count[0][1]
        else:
            use_count = 0

        #If it is a mandate before migration RCUR
        if mandate.date == '2009-10-31':
            type = 'RCUR'
        #If the mandate have not been used before
        elif use_count == 0:
            type = 'FRST'
        #If the mandate has been cancelled
        elif (use_count > 0
              and mandate.date_end
              and mandate.date_end <= invoice.date_invoice):
            type = 'FNAL'
        else:
            type = 'RCUR'

        #Include service level. HARDCODED as Rule book says in 2.9
        service_level = sepa19.ServiceLevel()
        service_level_fields = {
            'code': 'SEPA',
        }
        service_level.feed(service_level_fields)

        #Include service level. HARDCODED as Rule book says in 2.12
        local_instrument = sepa19.LocalInstrument()
        if mandate.mandate_scheme == 'b2b':
            schema_code = 'B2B'
        else:
            # CORE or COR1 scheme
            schema_code = self.order.mode.cor1_scheme and 'COR1' or 'CORE'
        local_instrument_fields = {
            'code': schema_code
        }
        local_instrument.feed(local_instrument_fields)

        payment_type_info = sepa19.PaymentTypeInfo()
        payment_type_info_fields = {
            'sequence_type': type,
            'service_level': service_level,
            'local_insturment': local_instrument
        }
        payment_type_info.feed(payment_type_info_fields)
        return payment_type_info

    def _payments_info(self, pool, cr, uid, lines):
        payments_info = []
        i = 0
        for line in lines:
            pay_info = sepa19.PaymentInformation()

            pay_info_id = self._generate_pay_info_id(pool, cr, uid, line, i)
            payment_method = 'DD'  # Yes hardcoded, only accepted option
            collection_date = self.order.date_planned
            creditor = self._creditor_info(pool, cr, uid, line)
            # Create a copy of the identifier. Reused in other tags
            creditor_identifier = deepcopy(creditor['identifier'])
            operation_infos = self._operation_info(pool, cr, uid, line, i,
                                                   creditor_identifier)
            operation_info = operation_infos[operation_infos.keys()[0]]
            payment_type_info = self._payment_type_info(pool, cr, uid, line)
            pay_info_fields = {
                'payment_info_identifier': pay_info_id,
                'number_of_operations': operation_info['number_of_operations'],
                'checksum': operation_info['checksum'],
                'payment_type_info': payment_type_info,
                'payment_method': payment_method,
                'collection_date': collection_date,
                'creditor': creditor['creditor'],
                'creditor_account': creditor['account'],
                'creditor_agent': creditor['agent'],
                'creditor_identifier': creditor['identifier'],
                'charge_clausule': 'SLEV',  # Hardcode. Rule book in 2.45
                'direct_debit_operation_info': operation_info['operations'],
            }
            i += 1
            pay_info.feed(pay_info_fields)
            payments_info.append(pay_info)

        return payments_info

    def _payment_info(self, pool, cr, uid, lines):
        payment_info = []
        i = 0

        payment_method = 'DD'  # Yes harcoded, only accepted option
        collection_date = self.order.date_planned
        creditor = self._creditor_info(pool, cr, uid, lines[0])
        # Create a copy of the identifier. Reused in other tags
        creditor_identifier = deepcopy(creditor['identifier'])
        operation_infos = self._operation_info(pool, cr, uid, lines, i,
                                               creditor_identifier)
        for ops in operation_infos.keys():
            pay_info = sepa19.PaymentInformation()
            operation_info = operation_infos[ops]
            # Payment information ID <PmtInfId>
            # The system generates an internal code.
            # Maximum of 35 characters.
            pay_info_id = '{}/{}'.format(self.order.reference[:30], ops)
            creditor = self._creditor_info(pool, cr, uid, lines[0])
            pay_info_fields = {
                'payment_type_info': operation_info['payment_type_info'],
                'payment_info_identifier': pay_info_id,
                'number_of_operations': operation_info['number_of_operations'],
                'checksum': "%.2f" % operation_info['checksum'],
                'payment_method': payment_method,
                'collection_date': collection_date,
                'creditor': creditor['creditor'],
                'creditor_account': creditor['account'],
                'creditor_agent': creditor['agent'],
                'creditor_identifier': creditor['identifier'],
                'charge_clausule': 'SLEV',  # Hardcode. Rule book in 2.45
                'direct_debit_operation_info': operation_info['operations'],
            }

            pay_info.feed(pay_info_fields)
            payment_info.append(pay_info)

        return payment_info

    def create_file(self, pool, cr, uid, order, lines, context):

        self.order = order
        self.n_lines = order.n_lines
        config_obj = pool.get('res.config')
        try:

            self.insert_bic = int(config_obj.get(cr, uid,
                                                 'insert_bic_code', '1'))
        except:
            self.insert_bic = 1

        self.empty_bic = config_obj.get(
            cr, uid, 'empty_bic_code', 'NOTPROVIDED')
        # validem els mandatos
        fact_no_man = self._missing_mandates(pool, cr, uid, order.id)
        if fact_no_man:
            raise Log(_(u"Missing mandate in this invoices:\n"
                        u"%s") % "\n".join(fact_no_man), True)

        # validem els bancs
        if self.insert_bic == 1:
            fact_no_bank = self._missing_banks(pool, cr, uid, order.id)
            if fact_no_bank:
                raise Log(_(u"Missing banks in this partners:\n"
                            u"%s") % "\n".join(fact_no_bank), True)

        lines_core = []
        lines_b2b = []
        result = {}

        for line in lines:
            mandate_scheme = line['invoice'].mandate_id.mandate_scheme
            if mandate_scheme == 'b2b':
                lines_b2b.append(line)
            else:
                lines_core.append(line)

        if lines_core:
            xml = sepa19.DirectDebitInitDocument()
            direct_debit = sepa19.DirectDebitInitMessage()

            header = self._sepa_header_lines(pool, cr, uid, lines_core)
            payment_info = self._payment_info(pool, cr, uid, lines_core)

            direct_debit.feed({
                'sepa_header': header,
                'payment_information': payment_info
            })
            xml.feed({
                'customer_direct_debit': direct_debit
            })

            xml.pretty_print = True
            xml.build_tree()
            result['core'] = str(xml)
        if lines_b2b:
            xml = sepa19.DirectDebitInitDocument()
            direct_debit = sepa19.DirectDebitInitMessage()

            header = self._sepa_header_lines(pool, cr, uid, lines_b2b, 'B2B')
            payment_info = self._payment_info(pool, cr, uid, lines_b2b)

            direct_debit.feed({
                'sepa_header': header,
                'payment_information': payment_info
            })
            xml.feed({
                'customer_direct_debit': direct_debit
            })

            xml.pretty_print = True
            xml.build_tree()
            result['b2b'] = str(xml)
        return result
