# -*- encoding: utf-8 -*-

import re

from datetime import datetime
from tools.translate import _
from converter import *

from sepa19 import vat_enterprise, format_address_line

from sepa import sepa34
from sepa import sepa19


class Sepa34Transfer():

    _file_type = 'xml'

    def _generate_message_id(self, pool, cr, uid):
        #CIF Codi remesa data creacio remesa
        msg_id = u'%s%s%s' % (self.order.mode.cif, self.order.date_planned,
                              self.order.reference)
        msg_id = re.sub(r'\W+', '', msg_id)
        return to_ascii(msg_id)[-35:]

    def _generate_pay_info_id(self, pool, cr, uid, line, line_num):
        pay_info_raw_id = "%s/%s" % (line['name'], line['invoice_number'])
        pay_info_raw_id = to_ascii(pay_info_raw_id)
        pay_info_id = re.sub(r'[^a-zA-Z0-9_/]+', '', pay_info_raw_id).lower()
        return pay_info_id[-35:]

    def _generate_end_to_end_pay_id(self, pool, cr, uid, line,
                                    line_num, subline_num):
        end_to_end_id = to_ascii(line['invoice_number'])[-35:]
        return end_to_end_id

    def _creditor_info(self, pool, cr, uid, line):
        address = line['partner_id'].address[0]
        creditor_info = {}

        postal_address = sepa19.PostalAddress()
        country = unicode(address.country_id and address.country_id.code or '')
        postal_address_fields = {
            'country': to_ascii(country),
        }
        postal_address.feed(postal_address_fields)

        creditor = sepa19.NameAndAddress('Cdtr')
        creditor_fields = {
            'name_name': to_ascii(line['partner_id'].name),
            'postal_address': postal_address
        }

        creditor.feed(creditor_fields)

        if line['bank_id'].iban:
            iban = line['bank_id'].iban
        else:
            creditor_name = line['bank_id'].iban
            raise Exception('Missing IBAN on creditor %s' % creditor_name)
        account_id = sepa19.AccountIdentification()
        account_id_fields = {
            'iban': to_ascii(iban)
        }
        account_id.feed(account_id_fields)

        account = sepa19.BankAccount('CdtrAcct')
        account_fields = {
            'account_identification': account_id
        }
        account.feed(account_fields)

        if self.insert_bic:
            if line['bank_id'].bank.bic:
                bic = line['bank_id'].bank.bic
            else:
                bank_name = line['partner_id'].name
                raise Exception('Missing BIC on bank of creditor %s'
                                % bank_name)
        else:
            bic = 'NOTPROVIDED'

        agent_id = sepa19.AgentIdentifier()
        agent_id_fields = {
            'bic': to_ascii(bic)
        }
        agent = sepa19.BankAgent('CdtrAgt')
        agent_id.feed(agent_id_fields)

        agent_fields = {
            'agent_identifier': agent_id
        }
        agent.feed(agent_fields)

        creditor_info['creditor'] = creditor
        creditor_info['account'] = account
        creditor_info['agent'] = agent
        return creditor_info

    def _concept(self, pool, cr, uid, line):
        u_concept = line['communication']

        concept = sepa19.Concept()
        concept_fields = {
            'unstructured': u_concept
        }
        concept.feed(concept_fields)
        return concept

    def _operation_info(self, pool, cr, uid, lines, line_num):
        operation_info = {}
        operations = []
        checksum = 0
        i = 0

        if not isinstance(lines, list):
            lines = [lines]

        for op in lines:
            end_to_end_id = self._generate_end_to_end_pay_id(pool, cr,
                                                             uid, op,
                                                             line_num, i)
            payment_id = sepa19.PaymentIdentifier()
            payment_id_fields = {
                'end_to_end_identifier': end_to_end_id
            }
            payment_id.feed(payment_id_fields)

            creditor_info = self._creditor_info(pool, cr, uid, op)

            concept = self._concept(pool, cr, uid, op)

            amount = sepa34.Amount()
            amount_fields = {
                'instructed_amount': str(op['amount'])
            }
            if 'currency' in op and op['currency'].name:
                currency = op['currency'].name
            else:
                currency = 'EUR'
            amount.set_currency(currency)
            amount.feed(amount_fields)

            operation = sepa34.CreditTransferInfo()
            operation_fields = {
                'payment_identifier': payment_id,
                'amount': amount,
                'creditor_agent': creditor_info['agent'],
                'creditor': creditor_info['creditor'],
                'creditor_account': creditor_info['account'],
                'concept': concept,
            }

            operation.feed(operation_fields)
            operations.append(operation)
            checksum += op['amount']
            i += 1

        operation_info['operations'] = operations
        operation_info['number_of_operations'] = i
        operation_info['checksum'] = checksum

        return operation_info

    def _payment_type_info(self, pool, cr, uid, line):
        service_level = sepa19.ServiceLevel()
        service_level_fields = {
            'code': 'SEPA'  # Yes Hardcoded only possible value in this case
        }
        service_level.feed(service_level_fields)

        payment_type_info = sepa19.PaymentTypeInfo()
        payment_type_info_fields = {
            'service_level': service_level,
        }
        payment_type_info.feed(payment_type_info_fields)
        return payment_type_info

    def _debtor_info(self, pool, cr, uid, line):
        debtor_info = {}

        other_identification = sepa19.OtherLegalEntity()
        # The debtor VAT + sufix
        iden = "%s%03s" % (self.order.mode.cif[2:], self.order.mode.sufijo)
        other_identification_fields = {
            'identification': iden
        }
        other_identification.feed(other_identification_fields)

        org_identification = sepa19.PhysicalLegalEntity('OrgId')
        org_identification_fields = {
            'other': other_identification
        }
        org_identification.feed(org_identification_fields)

        identification = sepa19.GenericPhysicalLegalEntityId()
        identification_fields = {
            'legal_entity': org_identification
        }
        identification.feed(identification_fields)

        address_dict = self.order.mode.partner_id.address_get(['contact'])
        address_id = address_dict['contact']

        address_obj = pool.get('res.partner.address')
        address = address_obj.browse(cr, uid, address_id)

        postal_address = sepa19.PostalAddress()
        country = unicode(address.country_id and address.country_id.code or '')
        postal_address_fields = {
            'country': to_ascii(country),
            'address_line': format_address_line(address)
        }
        postal_address.feed(postal_address_fields)

        debtor = sepa19.GenericPhysicalLegalEntity('Dbtr')
        debtor_fields = {
            'entity_name': to_ascii(self.order.mode.partner_id.name),
            'identification': identification,
            'postal_address': postal_address,
        }

        if self.order.mode.bank_id.iban:
            iban = self.order.mode.bank_id.iban
        else:
            debtor_name = self.order.mode.partner_id.name
            raise Exception('Missing IBAN on debtor %s' % debtor_name)

        account_id = sepa19.AccountIdentification()
        account_id_fields = {
            'iban': to_ascii(iban)
        }
        account = sepa19.BankAccount('DbtrAcct')
        account_id.feed(account_id_fields)
        account_fields = {
            'account_identification': account_id
        }

        if self.order.mode.bank_id.bank.bic:
            bic = self.order.mode.bank_id.bank.bic
        else:
            bank_name = self.order.mode.bank_id.bank.name
            raise Exception('Missing BIC on bank of debtor %s' % bank_name)

        agent_id = sepa19.AgentIdentifier()
        agent_id_fields = {
            'bic': to_ascii(bic)
        }
        agent = sepa19.BankAgent('DbtrAgt')
        agent_id.feed(agent_id_fields)
        agent_fields = {
            'agent_identifier': agent_id
        }
        debtor.feed(debtor_fields)
        agent.feed(agent_fields)
        account.feed(account_fields)

        debtor_info['debtor'] = debtor
        debtor_info['agent'] = agent
        debtor_info['account'] = account
        return debtor_info

    def _payments_info(self, pool, cr, uid, lines):
        ''' Cada transacció en un Payment info (no s'usa)'''
        payments_info = []
        i = 0
        for line in lines:
            pay_info = sepa34.PaymentInformation()

            pay_info_id = self._generate_pay_info_id(pool, cr, uid, line, i)
            payment_method = 'TRF'  # Transfer
            collection_date = self.order.date_planned
            payment_type_info = self._payment_type_info(pool, cr, uid, line)
            debtor = self._debtor_info(pool, cr, uid, line)
            operation_info = self._operation_info(pool, cr, uid, line, i)
            pay_info_fields = {
                'payment_info_identifier': pay_info_id,
                'payment_method': payment_method,
                'number_of_operations': operation_info['number_of_operations'],
                'checksum': operation_info['checksum'],
                'payment_type_info': payment_type_info,
                'collection_date': collection_date,
                'debtor': debtor['debtor'],
                'debtor_account': debtor['account'],
                'debtor_agent': debtor['agent'],
                'credit_transfer_info': operation_info['operations'],
            }
            i += 1
            pay_info.feed(pay_info_fields)
            payments_info.append(pay_info)

        return payments_info

    def _payment_info(self, pool, cr, uid, lines):
        payments_info = []
        i = 0

        payment_method = 'TRF'  # Transfer
        pay_info_id = '%s/TRF' % self.order.reference[:31]
        pay_info = sepa34.PaymentInformation()
        collection_date = self.order.date_planned
        payment_type_info = self._payment_type_info(pool, cr, uid, lines[0])
        debtor = self._debtor_info(pool, cr, uid, lines[0])
        operation_infos = self._operation_info(pool, cr, uid, lines, i)

        pay_info_fields = {
            'payment_info_identifier': pay_info_id,
            'payment_method': payment_method,
            'number_of_operations': operation_infos['number_of_operations'],
            'checksum': operation_infos['checksum'],
            'payment_type_info': payment_type_info,
            'collection_date': collection_date,
            'debtor': debtor['debtor'],
            'debtor_account': debtor['account'],
            'debtor_agent': debtor['agent'],
            'credit_transfer_info': operation_infos['operations'],
        }

        pay_info.feed(pay_info_fields)
        payments_info.append(pay_info)

        return payments_info

    def _initiating_party(self, pool, cr, uid):
        address_dict = self.order.mode.partner_id.address_get(['contact'])
        address_id = address_dict['contact']

        address_obj = pool.get('res.partner.address')
        address = address_obj.browse(cr, uid, address_id)

        postal_address = sepa19.PostalAddress()
        country = unicode(address.country_id and address.country_id.code or '')
        postal_address_fields = {
            'country': to_ascii(country),
            'address_line': format_address_line(address)
        }
        postal_address.feed(postal_address_fields)

        other_identification = sepa19.OtherLegalEntity()
        # The debtor VAT + sufix
        iden = "%s%03s" % (self.order.mode.cif[2:], self.order.mode.sufijo)
        other_identification_fields = {
            'identification': iden
        }
        other_identification.feed(other_identification_fields)

        org_identification = sepa19.PhysicalLegalEntity('OrgId')
        org_identification_fields = {
            'other': other_identification
        }
        org_identification.feed(org_identification_fields)

        identification = sepa19.GenericPhysicalLegalEntityId()
        identification_fields = {
            'legal_entity': org_identification
        }
        identification.feed(identification_fields)

        init_party = sepa19.GenericPhysicalLegalEntity('InitgPty')
        init_party_fields = {
            'entity_name': to_ascii(self.order.mode.nombre),
            'postal_address': postal_address,
            'identification': identification
        }
        init_party.feed(init_party_fields)

        return init_party

    def _sepa_header(self, pool, cr, uid):
        iso_today = str(datetime.now())[:-7].replace(' ', 'T')

        header = sepa19.SepaHeader()
        header_fields = {
            'message_id': self._generate_message_id(pool, cr, uid),
            'creation_date_time': iso_today,
            'number_of_operations': str(self.n_lines),
            'checksum': "%.2f" % abs(self.order.total),
            'initiating_party': self._initiating_party(pool, cr, uid)
        }
        header.feed(header_fields)
        return header

    def create_file(self, pool, cr, uid, order, lines, context):
        self.order = order
        self.n_lines = len(lines)

        try:
            config_obj = pool.get('res.config')
            self.insert_bic = int(config_obj.get(cr, uid,
                                                 'insert_bic_code', '1'))
        except:
            self.insert_bic = 1

        xml = sepa34.CustomerCreditTransferDocument()
        credit_transfer = sepa34.CustomerCreditTransfer()

        header = self._sepa_header(pool, cr, uid)
        payments_info = self._payment_info(pool, cr, uid, lines)

        credit_transfer.feed({
            'sepa_header': header,
            'payment_information': payments_info
        })
        xml.feed({
            'customer_credit_transfer': credit_transfer
        })

        xml.pretty_print = True
        xml.build_tree()
        return str(xml)
