# -*- coding: utf-8 -*-
"""
Eliminar valors per defecte de les payment.order del camp create_account_moves
perquè tingui sempre per defecte el valor del _defaults
"""
import logging


def up(cursor, installed_version):
    logger = logging.getLogger('openerp.migration')

    logger.info('Deleting default values for create_account_moves from payment '
                'orders...')

    delete_query = """
        DELETE FROM ir_values 
        WHERE name = 'create_account_moves' 
        AND model = 'payment.order'
    """

    cursor.execute(delete_query)

    logger.info('Deleted default values for create_account_moves from payment '
                'orders successfully!')


def down(cursor, installed_version):
    pass


migrate = up
