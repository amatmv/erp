# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#
# Copyright (c) 2006 ACYSOS S.L.. (http://acysos.com) All Rights Reserved.
#    Pedro Tarrafeta <pedro@acysos.com>
#
# Corregido para instalación TinyERP estándar 4.2.0: Zikzakmedia S.L. 2008
#   Jordi Esteve <jesteve@zikzakmedia.com>
#
# Añadidas cuentas de remesas y tipos de pago. 2008
#    Pablo Rocandio <salbet@gmail.com>
#
# Corregido para instalación OpenERP 5.0.0 sobre account_payment_extension: 
#   Zikzakmedia S.L. 2009
#   Jordi Esteve <jesteve@zikzakmedia.com>
#
# Mejoras para la generación de remesas de la norma 19 con más de un registro
# opcional.
#   Marcos de Vera Piquero <mdevera@gisce.net>
#
#    $Id$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import time
import re

from osv import osv, fields, orm
import pooler

TIPOS_REMESAS_EXTENDED = {
    'none': {'name': 'None', 'type': 'none'},
    'sepa19': {'name': 'SEPA 19', 'type': 'receivable'},
    'csb_19': {'name': 'CSB 19', 'type': 'receivable'},
    'csb_32': {'name': 'CSB 32', 'type': 'none'},
    'csb_34': {'name': 'CSB 34', 'type': 'payable'},
    'sepa34': {'name': 'SEPA 34', 'type': 'payable'},
    '34ceca': {'name': 'CSB 34(CECA)', 'type': 'none'},
    'csb_58': {'name': 'CSB 58', 'type': 'none'}
}

TIPOS_REMESAS = [
    (r, TIPOS_REMESAS_EXTENDED[r]['name'])
    for r in TIPOS_REMESAS_EXTENDED
]



class payment_mode(osv.osv):
    """Extensión de payment_mode para generación de remesas.
    """
    _name = 'payment.mode'
    _inherit = 'payment.mode'

    def gen_sepa_checksum(self, cursor, uid, sepa_id):
        """ Given a full sepa_creditor_code return
        " the expected digits
        """
        base = re.sub(r'\W+', '', sepa_id[7:]) # Sanitize from 8 to 35
        base += '%s%s' % (sepa_id[:2] , '00')
        base = base.lower()
        base_conv = ''
        for char in base:
            if char.isalpha():
                base_conv += str(ord(char)-87)
            else:
                base_conv += char
        base_int = 98 - (int(base_conv) % 97)
        return str(base_int).zfill(2)

    def onchange_partner(self, cr, uid, ids, partner_id):
        if partner_id:
            pool = pooler.get_pool(cr.dbname)
            obj = pool.get('res.partner')
            field = ['name']
            ids = [partner_id]
            filas = obj.read(cr, uid, ids, field) 
            return {'value':{'nombre': filas[0]["name"][:40]}}
        return {'value':{'nombre': ""}}

    _columns = {
        'tipo': fields.selection(TIPOS_REMESAS, 'Type of payment file',
                                  size=6, select=True, required=True),
        'sufijo': fields.char('suffix',size=3, select=True),
        'partner_id': fields.many2one('res.partner', 'Partner', select=True),
        'nombre': fields.char('Company name in file', size=40),
        'cif': fields.related('partner_id','vat', type='char', 
                              string='VAT code', select=True, readonly=True),
        # Código INE (9 dígitos)
        'ine': fields.char('INE code', size=9),
        'cedente': fields.char('Cedente', size=15),
        # Incluir registro obligatorio de domicilio (para no domiciliados)
        'inc_domicile': fields.boolean('Include domicile', 
                                       help='Add partner domicile records to '
                                       'the exported file (CSB 58)'),
        # Usar formato alternativo para el registro de domicilio
        'alt_domicile_format': fields.boolean('Alt. domicile format', 
                                              help='Alternative domicile '
                                              'record format'),
        # Require bank account?
        'require_bank_account': fields.boolean('Require bank account', 
                                                help='If your bank allows you '
                                                'to send orders without the '
                                                'bank account info, you may '
                                                'disable this option'),
        'sepa_creditor_code': fields.char('SEPA ID', size=16,
                                          help=u'SEPA unique creditor identifier'),
        'cor1_scheme': fields.boolean('SEPA COR1',
                                       help=u'Use COR1 optional schema in SEPA direct debit'),
        'financiado': fields.boolean(
            'SEPA Financiado', help=u"Genera remesa finançada posant el prefix "
                                    u"FSDD al nom de la remesa."
        )
    }

    _defaults = {
        'tipo': lambda *a: 'none',
        'sufijo': lambda *a: '000',
        'inc_domicile': lambda *a: False,
        'alt_domicile_format': lambda *a: False,
        'cor1_scheme': lambda *a: False,

        # Override default: We want to be safe so we require bank account by 
        # default
        'require_bank_account': lambda *a: True,
    }

    def _check_creditor_code(self, cursor, uid, ids):

        for mode in self.browse(cursor, uid, ids):
            if mode.sepa_creditor_code:
                code = mode.sepa_creditor_code
                checksum = self.gen_sepa_checksum(cursor, uid, code)
                if code[2:4] != checksum:
                    return False
        return True

    _constraints = [
        (_check_creditor_code,
         'The SEPA creditor code is not correct',
         ['sepa_creditor_code']),
    ]

payment_mode()

class payment_line_template(osv.osv):
    """Plantilla para los registros opcionales de las remesas del tipo
    payment_mode_id.

    Actualmente probado únicamente con CSB19.
    """
    _name = 'payment.line.template'
    _columns = {
        'name': fields.char('Nombre', size=64, required=True,
                            help='Nombre identificativo de la plantilla'),
        'tipo_remesa': fields.selection(TIPOS_REMESAS, 'Tipo remesa',
                                        size=6, required=True,
                                        help='Tipo de remesa para el que '
                                             'aplica esta plantilla.'),
        'code': fields.text('Código python', required=True,
                            help='El código que generará los registros '
                                 'opcionales de las remesas.'),
    }
   
    _defaults = {
        'code': lambda *a: """#
partner = line.partner_id

periode_1 = filter(lambda x: x and int(x.name[-1:])  == 1, factura.linies_energia)
if periode_1:
    periode_1 = periode_1[0]
periode_2 = filter(lambda x: x and int(x.name[-1:])  == 2, factura.linies_energia)
if periode_2:
    periode_2 = periode_2[0]
periode_3 = filter(lambda x: x and int(x.name[-1:])  == 3, factura.linies_energia)
if periode_3:
    periode_3 = periode_3[0]

iva = filter(lambda x: x and x.name.startswith('IVA'), factura.tax_line)
if iva:
    iva = iva[0]

iese = filter(lambda x: x and 'elect' in x.name, factura.tax_line)
if iese:
    iese = iese[0]

_linia_1 = ("Tarifa: %s" % factura.tarifa_acces_id.name).ljust(40)
_linia_1 += ("Potencia %s" % factura.potencia).ljust(40)
_linia_1 += ("N Factura: %s" % factura.number).ljust(40)
_linia_2 = ("Consumo P1: %s" % (periode_1 and periode_1.quantity or '')).ljust(40)
_linia_2 += ("Consumo P2: %s" % (periode_2 and periode_2.quantity or '')).ljust(40)
_linia_2 += ("Consumo P3: %s" % (periode_3 and periode_3.quantity or '')).ljust(40)
_linia_3 = ("Importe potencia: %s" % factura.total_potencia).ljust(40)
_linia_3 += ("Importe energia: %s" % factura.total_energia).ljust(40)
_linia_3 += ("%s S/%.2f %.2f" % (iva.name, iva.base, factura.amount_total)).ljust(40)
_linia_4 = ("Impuesto Electricidad: %.2f" % (iese and iese.amount or 0)).ljust(40)
_linia_4 += ("Fecha factura: %s" % factura.date_invoice).ljust(40)
_linia_4 += " ".ljust(40)
_linia_5 = ("NIF: %s" % partner.vat).ljust(40)
_linia_5 += ("%s" % (factura.address_invoice_id.street+(factura.address_invoice_id.street2 or ''))).ljust(40)[:40]
_linia_5 += ("%s" % (factura.address_invoice_id.city or '')).ljust(40)

result = u"%s\r\n%s\r\n%s\r\n%s\r\n%s" % (_linia_1, _linia_2, _linia_3, _linia_4, _linia_5)
"""
    }
payment_line_template()

class payment_line(osv.osv):
    """Extensión para albergar los registros opcionales de remesas.

    Además se sobreescriben los campos communication y communication2 para
    poder albergar registros opcionales completos (se necesitan hasta 134
    carácteres y no sólo 64 tal y como está definido el campo en payment.line)

    Son 134 carácteres en caso de CSB58 y 115 en caso de CSB19; como el
    asistente correspondiente ya toma sólo el número de carácteres adecuado,
    se usa aquí el mayor.
    """
    _name = 'payment.line'
    _inherit = 'payment.line'

    _columns = {
        'communication': fields.char('Communication', size=134, required=True,
                                     help='Registro cabecera presentador'),
        'communication2': fields.char('Communication 2', size=134,
                                      required=False,
                                      help='Registro cabecera presentador'),

        'comm_text': fields.text('Communication text', required=False,
                                 help='The communication text processed for '
                                      'this line.'),
    }
payment_line()

class payment_order(osv.osv):
    """Extensión para albergar una plantilla con la que obtener los diferentes
    registros opcionales para las líneas de una orden de pago.
    """
    _name = 'payment.order'
    _inherit = 'payment.order'

    def get_wizard(self, type):
        if type in ('RECIBO_CSB', 'TRANSFERENCIA_CSB'):
            return ('l10n_ES_remesas', 'wizard_create_payment_file_spain')
        else:
            return super(payment_order, self).get_wizard(type)

    def action_open(self, cursor, uid, ids, *args):
        """Sobrecarga para parsear la posible plantilla y guardar el resultado
        en cada una de las líneas de la orden de pago.
        """
        factures_obj = self.pool.get('giscedata.facturacio.factura')
        invoices_obj = self.pool.get('account.invoice')
        for order in self.browse(cursor, uid, ids):
            # Sólo aplicar la plantilla si coinciden los tipos de remesa y si
            # hay alguna seleccionada, obviamente
            if (order.template and
                order.mode.tipo == order.template.tipo_remesa):
                for line in order.line_ids:
                    search_params = [('id', '=', line.ml_inv_ref.id)]
                    invoice_id = invoices_obj.search(cursor, uid, search_params)
                    search_params = [('invoice_id', '=', invoice_id[0])]
                    factura_id = factures_obj.search(cursor, uid, search_params)
                    factura = factures_obj.browse(cursor, uid, factura_id[0])
                    tpl_dict = {
                        'factura': factura,
                        'partner': line.partner_id,
                        'data_factura': time.strftime('%d/%m/%Y',
                                        time.strptime(factura.date_invoice, 
                                                      '%Y-%m-%d')),
                        'data_inici': time.strftime('%d/%m/%Y',
                                        time.strptime(factura.data_inici
                                                      or '3000-01-01',
                                        '%Y-%m-%d')),
                        'data_final': time.strftime('%d/%m/%Y',
                                        time.strptime(factura.data_final
                                                      or '3000-01-01', 
                                        '%Y-%m-%d')),
                    }
                    # el resultado debe estar en la clave 'result'
                    exec order.template.code in tpl_dict
                    line.write({'comm_text': tpl_dict['result']})

        return super(payment_order, self).action_open(cursor, uid, ids, *args)

    _columns = {
        'template': fields.many2one('payment.line.template', 'Plantilla',
                                    required=False,
                                    help='La plantilla para los registros '
                                         'opcionales de esta orden de pago.')
    }
payment_order()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
