# -*- encoding: utf-8 -*-

from osv import osv, fields
from uuid import uuid4
from datetime import datetime
from datetime import timedelta
from tools import config
from base_iban.base_iban import _printed_iban_format
from tools.translate import _

MANDATE_SCHEME_SELECTION = [('core', 'CORE'), ('b2b', 'B2B')]

class PaymentMandate(osv.osv):

    _name = 'payment.mandate'
    _order = 'date desc'

    def default_get(self, cursor, uid, fields, context=None):

        user_obj = self.pool.get('res.users')
        mode_obj = self.pool.get('payment.mode')
        address_obj = self.pool.get('res.partner.address')

        res = super(PaymentMandate,
                    self).default_get(cursor, uid, fields, context=context)
        user = user_obj.browse(cursor, uid, uid)

        res['creditor_id'] = user.company_id.id
        partner = user.company_id.partner_id
        # Search for creditor code
        search_params = [('partner_id', '=', partner.id),
                         ('require_bank_account', '=', True),
                         ('tipo', '=', 'sepa19')]
        mode_ids = mode_obj.search(cursor, uid, search_params)
        if mode_ids:
            mode = mode_obj.browse(cursor, uid, mode_ids[0])
            res['creditor_code'] = mode.sepa_creditor_code or ''
        else:
            res['creditor_code'] = ''
        # Search for address
        address_id = partner.address_get(['contact'])['contact']
        address = address_obj.browse(cursor, uid, address_id)
        creditor_address = ('%s (%s)'
                        % (address.name_get(context=context)[0][1],
                           address.country_id.name.upper()))
        address_contact = address.name and '%s, ' % address.name or ''
        if address_contact:
            creditor_address = creditor_address.replace(address_contact, '')
        res['creditor_address'] = creditor_address.upper()

        # Només els camps passats a fields
        retorn = dict([(field, res[field]) for field in fields
                       if res.get(field, False)])

        return retorn

    def previous_mandate(self, cursor, uid, mandate_id, context=None):
        """
        For example, in the following case the function will return the
        mandate 2 if we call the function with mandate 3.
                   date start - date end
        mandate 3  01/03/2018 -
        mandate 2  01/02/2018 - 28/02/2018
        mandate 1  01/01/2018 - 31/01/2018

        Search the previous mandate of the mandate_id of the reference.
        :param mandate_id: The id of the mandate which you want to search the
        previous mandate
        :type mandate_id: int
        :return: previous mandate id
        """

        mandate_values = self.read(
            cursor, uid, mandate_id, ['reference', 'date'],
            context=context
        )

        search_prev = [
            ('reference', '=', mandate_values['reference']),
            ('date_end', '<', mandate_values['date'])
        ]
        prev = self.search(cursor, uid, search_prev, order='date_end DESC',
                           limit=1, context=context)
        return prev

    def create(self, cursor, uid, vals, context=None):

        if 'reference' in vals and vals['reference']:
            reference_vals = self.get_reference_vals(
                cursor, uid, vals['reference'], context=context
            )
            # Only update null values or non existen
            for k, v in reference_vals.items():
                if k not in vals or not vals[k]:
                    vals[k] = v
        if 'creditor_code' not in vals or not vals['creditor_code']:
            vals.update(self.default_get(cursor, uid,
                                         ['creditor_code'],
                                         context=context))
        return super(PaymentMandate,
                       self).create(cursor, uid, vals, context=context)

    def get_reference_vals(self, cursor, uid, reference, context=None):

        model_name, model_id = reference.split(',')
        model_obj = self.pool.get(model_name)

        if not hasattr(model_obj, 'update_mandate'):
            return {}

        return model_obj.update_mandate(
            cursor, uid, int(model_id), context=context
        )

    def update_from_reference(self, cursor, uid, ids, context=None):
        '''Update mandate from reference model'''

        for mandate in self.browse(cursor, uid, ids, context=context):
            if mandate.reference:
                vals = self.get_reference_vals(cursor, uid,
                                               mandate.reference,
                                               context=context)
                mandate.write(vals)
        return True

    def _get_printed_iban(self, cursor, uid, ids, name, args, context=None):
        '''returns the IBAN in printed format
        In four char groups when possible. The last group
        can be of a different length due to different account
        lenghts in other countries'''

        res = {}
        for read in self.read(cursor, uid, ids, ['debtor_iban']):
            iban = read['debtor_iban']
            res[read['id']] = _printed_iban_format(iban)
        return res

    def _get_reference(self, cursor, uid, context=None):

        link_obj = self.pool.get('res.request.link')

        if not context:
            context = {}
        res = []
        link_ids = link_obj.search(cursor, uid, [], context=context)
        for link in link_obj.browse(cursor, uid, link_ids, context=context):
            res.append((link.object, link.name))

        return res

    def print_mandate(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        ctx = {
            'print': context.get('print', ['default']),
            'address': context.get('address', False)
        }
        return {
            'type': 'ir.actions.report.xml',
            'model': 'l10n.ES.remesas',
            'report_name': 'report_mandato',
            'report_webkit': "'l10n_ES_remesas/report/mandato_generico.mako'",
            'webkit_header': 'report_header_mandato_generico',
            'groups_id': ids,
            'multi': '0',
            'auto': '0',
            'header': '0',
            'report_rml': 'False',
            'datas': {
                'ids': ids,
                'context': ctx
            },
        }

    def _close_mandate(self, cursor, uid, ids, end_date, context=None):
        """
        Tanca mandats.
        :param end_date: Data de final del mandat.
        :return:
        """
        mandate_obj = self.pool.get('payment.mandate')
        mandate_obj.write(cursor, uid, ids, {'date_end': end_date})

    def _create_mandate(self, cursor, uid, init_date, mandate_reference,
                        mandate_scheme, context=None):
        """
        Crea un mandat.
        :param init_date: data de inici del mandat.
        :param mandate_reference: referència del mandat.
        :param mandate_scheme: esquema del mandat.
        :return: id del nou mandat.
        """
        mandate_obj = self.pool.get('payment.mandate')
        params = {
            'reference': mandate_reference,
            'mandate_scheme': mandate_scheme
        }
        mandate_id = mandate_obj.create(cursor, uid, params, context=context)

        mandate_obj.update_from_reference(
            cursor, uid, [mandate_id], context=context
        )

        mandate_obj.write(
            cursor, uid, [mandate_id], {'date': init_date}, context=context
        )
        return mandate_id

    def split_by_zip_code(self, cursor, uid, ids, context=None):
        """
        Divideix una adreça
        :param address:
        :type: str
        :return: tupla amb l'adreça. El segon valor comença pel zip code.
        """
        if context is None:
            context = {}
        res = {}
        if context:
            import re
            if context['address'] == 'creditor':
                field = 'creditor_address'
            elif context['address'] == 'debtor':
                field = 'debtor_address'
            else:
                raise Exception
            mandates_addresses = self.read(
                cursor, uid, ids, ['id', field], context=context
            )
            rr = r"""
                ^           # begin
                (.*)        # match everything
                (\d{5}.*)   # split when 5 consecutive digits
                $           # end
            """
            rr = re.compile(rr, re.VERBOSE)
            for mandate_address in mandates_addresses:
                res[mandate_address['id']] = rr.findall(mandate_address[field])[0]
        return res

    _columns = {
        'name': fields.char('Code', size=35, required=True, readonly=True),
        'signed': fields.boolean('Signed'),
        'date': fields.date('Date signed'),
        'date_end': fields.date('Date end'),
        'creditor_id': fields.many2one('res.company', 'Company',
                                      required=True),
        'creditor_code': fields.char('Company code', size=16, required=True),
        'creditor_address': fields.char('Creditor address', size=250),
        'debtor_name': fields.char('Debtor name', size=140),
        'debtor_vat': fields.char('Debtor vat', size=32),
        'debtor_address': fields.char('Debtor address', size=250),
        'debtor_state': fields.char('Debtor state', size=40),
        'debtor_country': fields.char('Debtor country', size=40),
        'debtor_iban': fields.char('IBAN', size=36),
        'debtor_iban_print': fields.function(_get_printed_iban,
            string='Printed IBAN', readonly=True,
            method=True, type='char', size=40,
            store={'payment.mandate':
                   (lambda self, cr, uid, ids, c=None: ids,
                    ['debtor_iban'], 10)
                   }),
        'reference': fields.reference('Reference', selection=_get_reference,
                                      size=128),
        'notes': fields.text('Notes'),
        'payment_type': fields.selection([
            ('recurring', 'Recurring'), ('one_payment', 'One payment')
        ], 'Payment type'),
        'mandate_scheme': fields.selection(
            MANDATE_SCHEME_SELECTION, 'Mandate Schema', required=True,
            select=True
        )
    }

    _defaults = {
        'name': lambda *a: uuid4().hex,
        'date': lambda *a: datetime.now().strftime('%Y-%m-%d'),
        'payment_type': lambda *a: 'recurring',
        'mandate_scheme': lambda *a: 'core',
        'signed': lambda *a: True
    }

    def _check_iban(self, cursor, uid, ids):

        bank_obj = self.pool.get('res.partner.bank')

        for mandate in self.browse(cursor, uid, ids):
            if mandate.debtor_iban:
                check_iban = bank_obj.calculate_iban(cursor, uid,
                                                     mandate.debtor_iban[4:],
                                                     mandate.debtor_iban[:2])
                if check_iban != mandate.debtor_iban:
                    return False

        return True

    def _check_vat(self, cursor, uid, ids):
        partner_obj = self.pool.get('res.partner')
        for mandate in self.browse(cursor, uid, ids):
            if not mandate.debtor_vat:
                continue
            debtor_vat = mandate.debtor_vat
            vat_country, vat_number = partner_obj._split_vat(debtor_vat)
            if not partner_obj.simple_vat_check(cursor, uid, vat_country,
                                                vat_number):
                return False
        return True

    _constraints = [
        (_check_iban,
         _(u"The iban is not correct"),
         ['debtor_iban']),
        (_check_vat, _(u"The VAT doesn't seem to be correct."),
         ["debtor_vat"])
    ]

PaymentMandate()


class InvoicePaymentMandate(osv.osv):

    _name = 'account.invoice'
    _inherit = 'account.invoice'

    _columns = {
        'mandate_id': fields.many2one('payment.mandate', 'Mandate'),
    }

InvoicePaymentMandate()
