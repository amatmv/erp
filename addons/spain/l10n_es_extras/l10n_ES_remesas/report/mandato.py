# -*- coding: utf-8 -*-
from c2c_webkit_report import webkit_report
from report import report_sxw


class MandatoWebkitParser(webkit_report.WebKitParser):
    def __init__(
        self, name='report.report_mandato',
        table='payment.mandate',
        rml=None,
        parser=report_sxw.rml_parse, header=True, store=False,
    ):
        super(MandatoWebkitParser, self).__init__(
            name, table, rml=rml, parser=parser, header=header, store=store
        )


MandatoWebkitParser()
