## -*- coding: utf-8 -*-
<%
    from datetime import datetime
    from ast import literal_eval
    import babel

    if data and len(data.get('ids', [])):
        mandates_ids = data.get('ids')
        mandate_obj = pool.get('payment.mandate')
        mandates = mandate_obj.browse(cursor, user.id, mandates_ids)
        if not context:
            raise Exception
    else:
        mandates = objects

    if data and data.get('context'):
        contexte = data.get("context")
        if not contexte.get('print'):
            contexte.update({'print': ['default']})
    else:
        confvar_obj = pool.get('res.config')
        confvar = literal_eval(confvar_obj.get(
            cursor, uid, 'default_mandate_print_copies', "['default']"
        ))
        contexte = {'print': confvar}
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
        <title>${_(u"AUTORIZACIÓN DOMICILIACIÓN BANCARIA")}</title>
        <link rel="stylesheet" type="text/css" href="${addons_path}/l10n_ES_remesas/report/stylesheet_mandato_generico.css">
        <%block name="custom_css" />
    </head>
    <body>
    %for mandato in mandates:
        <%
            inner_loop = 0
        %>
        %for tipus_impresio in contexte['print']:
            <br/>
            <div id="mandate-type">
                %if tipus_impresio == 'client':
                    EJEMPLAR PARA EL CLIENTE
                %elif tipus_impresio == 'company':
                    EJEMPLAR PARA LA EMPRESA EMISORA
                %elif tipus_impresio == 'bank':
                    EJEMPLAR PARA SU ENTIDAD BANCARIA
                %endif
            </div>
            <div id="logo">
                <%block name="mandato_company_logo">
                    <!--<img id="logo" src="data:image/jpeg;base64,${company.logo}"/>-->
                </%block>
            </div>
            <%
                css_header = "header"
                if len(mandato.creditor_id.name) > 40:
                    css_header = "header-reduced"
            %>
            <h1 id=${"{}".format(css_header)}>${mandato.creditor_id.name}</h1>

            <div id="enterprise">
                <p class="automated-input">
                    ${mandato.creditor_id.name}
                </p>
                <p class="automated-input">
                    <%block name="mandato_company_vat">
                        ${company.partner_id and company.partner_id.vat or ''}
                        ${company.rml_footer1 or ''}
                    </%block>
                </p>
            </div>
            <div id="description">
                <h1 id="main-title" class="title strong">${_(u"AUTORIZACIÓN DOMICILIACIÓN BANCARIA")}</h1>
                <p class="automated-input-translation">DEBIT AUTHORIZATION</p>
                <p class="normal-text">${_(u"Documento de autorización bancaria para la domiciliación de efectos según los datos a continuación reseñados")}</p>
                <p class="translation">Paper bank debit authorization for the purposes as outlined in the information below</p>
            </div>
            <div id="fields">
                <table style="width:100%">
                    <tr>
                        <td class="data w30p">
                            <p class="automated-input strong">${_(u"Código de autorización")}</p>
                            <p class="translation">Authorization code</p>
                        </td>
                        <td>
                            <p class="normal-text">${mandato.name}</p>
                        </td>
                    </tr>
                </table>
                <h2 class="automated-input strong">${_(u"EMPRESA EMISORA")}</h2>
                <p class="automated-input-translation">ISSUING COMPANY</p>
                <table style="width:100%">
                    <tr>
                        <td class="data w10p">
                            <p class="automated-input strong">${_(u"Nombre")}</p>
                            <p class="translation">Name</p>
                        </td>
                        <td class="w40p">
                            <p class="normal-text">${mandato.creditor_id.name}</p>
                        </td>
                        <td class="data w20p">
                            <p class="automated-input strong">${_(u"Código Acreedor")}</p>
                            <p class="translation">Creditor code</p>
                        </td>
                        <td>
                            <p class="normal-text">${mandato.creditor_code or ''}</p>
                        </td>
                    </tr>
                    <tr>
                        <td class="data">
                            <p class="automated-input strong">${_(u"Dirección")}</p>
                            <p class="translation">Address</p>
                        </td>
                        <td colspan="3">
                            <p class="normal-text">
                            ${mandato.creditor_address}
                            </p>
                        </td>
                    </tr>
                </table>
                <h2 class="automated-input strong">${_(u"DATOS PERSONALES")}</h2>
                <p class="automated-input-translation">PROFILE</p>
                <table style="width:100%">
                    <tr>
                        <td class="data w10p">
                            <p class="automated-input strong">${_(u"Nombre")}</p>
                            <p class="translation">Name</p>
                        </td>
                        <td>
                            <p class="normal-text">${mandato.debtor_name}</p>
                        </td>
                        <td class="data w10p">
                            <p class="automated-input strong">${_(u"NIF/CIF")}</p>
                        </td>
                        <td>
                            <p class="normal-text">${mandato.debtor_vat}</p>
                        </td>
                    </tr>
                    <tr>
                        <td class="data">
                            <p class="automated-input strong">${_(u"Dirección")}</p>
                            <p class="translation">Address</p>
                        </td>
                        <td colspan="3">
                            <p class="normal-text">
                            ${mandato.debtor_address}
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td class="data">
                            <p class="automated-input strong">${_(u"Provincia")}</p>
                            <p class="translation">Province</p>
                        </td>
                        <td>
                            <p class="normal-text">${mandato.debtor_state}</p>
                        </td>
                        <td class="data">
                            <p class="automated-input strong">${_(u"País")}</p>
                            <p class="translation">Country</p>
                        </td>
                        <td>
                            <p class="normal-text">${mandato.debtor_country}</p>
                        </td>
                    </tr>
                </table>
                <h2 class="automated-input strong">${_(u"DATOS BANCARIOS")}</h2>
                <p class="automated-input-translation">BANK DETAILS</p>
                <table style="width:100%">
                    <tr>
                        <td class="data w10p">
                            <p class="automated-input strong">${_(u"IBAN")}</p>
                        </td>
                        <td>
                            <p class="normal-text">${mandato.debtor_iban_print or ''}</p>
                        </td>
                    </tr>
                </table>
                <table style="width:100%" class="payment">
                    <tr>
                        <td class="payment">
                            <h2 class="automated-input strong">${_(u"TIPO DE PAGO")}</h2>
                            <p class="automated-input-translation">TYPE OF PAYMENT</p>
                        </td>
                        <td class="payment">
                            <p class="automated-input strong">${_(u"Pago Recurrente")}</p>
                            <p class="translation">Recurring Payment</p>
                        </td>
                        <td class="payment">
                            <div class="checkbox"><p class="X">
                                %if mandato.payment_type == 'recurring':
                                    X
                                %endif
                            </p></div>
                        </td>
                        <td class="payment">
                            <p class="automated-input strong">${_(u"Pago único")}</p>
                            <p class="translation">Single payment</p>
                        </td>
                        <td class="payment">
                            <div class="checkbox"><p class="X">
                                %if mandato.payment_type != 'recurring':
                                    X
                                %endif
                            </p></div>
                        </td>
                    </tr>
                </table>

                <%block name="mandato_custom_part" />

                <h2 class="automated-input strong">${_(u"INFORMACIÓN ADICIONAL")}</h2>
                <p class="automated-input-translation">ADDITIONAL INFORMATION</p>
                <div id="additional-information">
                    ${mandato.notes.replace("\n","<br/>")}
                </div>
            </div>
            %if mandato.date_end:
                <p class="normal-text justify">${_(u"Por la presente le informo que a partir de")} ${mandato.date_end} ${_(u"no me corresponde hacerme cargo de los recibos cuyos datos se reseñan, dejando por tanto sin efecto la orden que firmé en su día")}</p>
                <p class="translation justify">I hereby inform you that as of ${mandato.date_end} I am no longer responsible for the receipts whose details are reviewed, thereby cancelling the order I signed at the time. </p>
            %else:
                <p class="normal-text justify">${_(u"Mediante la firma de esta orden de domiciliación, el deudor autoriza al acreedor a enviar instrucciones a la entidad del deudor para adeudar su cuenta y a la entidad para efectuar los adeudos en su cuenta siguiendo las instrucciones del acreedor. Como parte de sus derechos, el deudor está legitimado al reembolso por su entidad en los términos y condiciones del contrato suscrito con la misma. La solicitud de reembolso deberá efectuarse dentro de las ocho semanas que siguen a la fecha de adeudo en cuenta. Puede obtener información adicional sobre sus derechos en su entidad financiera.")}</p>
                <p class="translation justify">By signing this Mandate, the debtor authorizes the creditor to send instructions to the bank to debit the debtor's account and the entity to make debits to your account as instructed by the creditor. As part of their rights, the debtor is entitled to reimbursement by the entity in the terms and conditions of the contract with it. The refund request must be made within eight weeks following the date of debit entry. You can obtain additional information about your rights in your financial institution.</p>
            %endif
            <div id="date">
                <p>
                <%
                    from datetime import datetime
                    import time
                    data_firma =  datetime.now()
                %>
                    ${(company.partner_id.city + ", " or "")} ${babel.dates.format_datetime(data_firma, "d LLLL 'de' YYYY", locale='es_ES')}

                </p>
                <p>
                    ${_(u"Firma del titular")}
                    /
                    Account holder signature
                    :
                </p>
            </div>
            %if tipus_impresio == 'bank':
                <div id="footer">
                    <p>
                        ${_(u"UNA VEZ FIRMADA, ESTA ORDEN DE DOMICILIACIÓN DEBE SER ENVIADA A SU ENTIDAD BANCARIA")}
                        ${_(u"PARA SU CUSTODIA.")}
                    </p>
                    <p class="translation">
                        ALL GAPS ARE MANDATORY.
                        ONCE THIS MANDATE HAS BEEN SIGNED MUST BE SENT TO YOUR BANK ENTITY
                    </p>
                </div>
            %endif
            %if tipus_impresio == 'company':
                <div id="footer">
                    <%
                        nom_empresa = mandato.creditor_id.name
                        nom_empresa = str(nom_empresa).upper()
                    %>
                    <p>
                        ${_(u"UNA VEZ FIRMADA, ESTA ORDEN DE DOMICILIACIÓN DEBE SER ENVIADA A")}
                        ${nom_empresa}
                        ${_(u"PARA SU CUSTODIA.")}
                    </p>
                    <p class="translation">
                        ALL GAPS ARE MANDATORY.
                        ONCE THIS MANDATE HAS BEEN SIGNED MUST BE SENT TO
                        ${nom_empresa}
                    </p>
                </div>
            %endif
            <%
                inner_loop += 1
            %>
            %if inner_loop < len(contexte['print']):
                <p style="page-break-after:always;"></p>
            %endif
        %endfor
        %if loop.index + 1 < len(mandates):
            <p style="page-break-after:always;"></p>
        %endif
    %endfor
    </body>
</html>

