# -*- coding: utf-8 -*-
{
    "name": "Remesas de recibos CSB 19 y CSB 58",
    "description": """Módulo para la exportación de remesas de recibos según las normas CSB 19 (recibos domiciliados), CSB 58 (anticipos de créditos) y CSB 34 (emisión de transferencias) para poder ser enviados a la entidad bancaria.

Crea un tipo de pago "Recibo domiciliado" con el código RECIBO_CSB. Este código es importante pues permite ejecutar el asistente de creación del fichero de remesas cuando se presiona el botón "Realizar pagos" en la orden de pagos o remesa.

También crea el tipo de pago "Transferencia" con el código TRANSFERENCIA_CSB.

Antes de generar un fichero de remesas habrá que definir un modo de pago que use el tipo de pago anterior y donde se defina la forma de pago (CSB 19, CSB 58 o CSB 34), la compañía que emite el fichero y el sufijo y nombre de compañia a incluir en el fichero (lo que en la versión 4.2 se definía como la cuenta de remesas).

Al crear el fichero de remesa:

  * Se pueden agrupar o no los pagos de una misma empresa y cuenta bancaria
  * El fichero creado se guarda como adjunto de la orden de pagos. Se puede volver a crear el fichero de remesas siempre que sea necesario (puede tener que renombrar el anterior fichero adjunto si tienen el mismo nombre).

También se proporciona un informe para imprimir un listado de los recibos de la remesa.
""",
    "version": "1.5",
    "author": "Acysos SL, Zikzakmedia SL, Pablo Rocandio, NaN",
    "category": "Localisation/Accounting",
    "depends":[
        "base_extended",
        "account",
        "account_payment_extension",
        "base_iban",
        "c2c_webkit_report"
    ],
    "init_xml":[
        "remesas_data.xml"
    ],
    "demo_xml":[
        "res_partner_bank_demo.xml"
    ],
    "update_xml":[
        "wizard/export_remesas_view.xml",
        "remesas_report.xml",
        "remesas_view.xml",
        "mandato_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
