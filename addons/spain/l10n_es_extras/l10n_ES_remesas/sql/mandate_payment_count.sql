SELECT
  mandate.id,
  count(pline.id)
FROM payment_mandate mandate
LEFT JOIN account_invoice AS i
  ON i.mandate_id = mandate.id
LEFT JOIN account_move_line AS mline
  ON mline.move_id = i.move_id
LEFT JOIN payment_line AS pline
  ON mline.id = pline.move_line_id
INNER JOIN payment_order AS porder
  ON porder.id = pline.order_id
  AND porder.date_done <= %s
WHERE mandate.id = %s
GROUP BY mandate.id
