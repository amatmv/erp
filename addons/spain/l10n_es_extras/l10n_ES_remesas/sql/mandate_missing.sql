SELECT pl.id AS linia_id, pl.communication AS linia, f.id AS factura_id, i.number AS num_factura
FROM payment_line pl
LEFT JOIN account_move_line AS ml on ml.id=pl.move_line_id
LEFT JOIN account_invoice AS i ON i.move_id=ml.move_id
LEFT JOIN giscedata_facturacio_factura AS f ON (f.invoice_id=i.id)
WHERE i.mandate_id IS NULL AND pl.order_id=%s;
