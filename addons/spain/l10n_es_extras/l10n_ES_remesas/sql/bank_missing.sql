SELECT pl.id AS linia_id, pl.communication AS linia, p.name, p.id
FROM payment_line AS pl
LEFT JOIN res_partner_bank AS pb ON pb.id=pl.bank_id
LEFT JOIN res_partner AS p ON p.id=pl.partner_id
WHERE pb.bank IS NULL AND pl.order_id=%s;
