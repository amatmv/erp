# -*- coding: utf-8 -*-
{
    "name": "FACe",
    "description": """All FACe related improvements""",
    "version": "0-dev",
    "author": "GISCE-TI",
    "category": "Account",
    "depends":[
        "base",
        "base_vat",
        "account",
        "facturae_module",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "res_partner_view.xml",
        "account_invoice_view.xml",
    ],
    "active": False,
    "installable": True
}
