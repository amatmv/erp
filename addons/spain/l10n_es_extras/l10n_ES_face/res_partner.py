# -*- encoding: utf-8 -*-

from osv import osv, fields

class ResPartner(osv.osv):

    _name = 'res.partner'
    _inherit = 'res.partner'

    _columns = {
        'face_subjected': fields.boolean(
            'Sujeto a FACe',
            help=u"Estará activo cuando deba mandar las facturas a FACe"
        ),
    }

    _defaults = {
        'face_subjected': lambda *a: False,
    }

ResPartner()
