# -*- encoding: utf-8 -*-

from osv import osv, fields
from osv.expression import OOQuery

class AccountInvoice(osv.osv):

    _name = 'account.invoice'
    _inherit = 'account.invoice'

    def _is_face_subjected(self, cursor, uid, ids, field_name, arg, context=None):
        if context is None:
            context = {}
        res = dict.fromkeys(ids, False)
        search_params = [
            ('id', 'in', ids),
            ('type', 'in', ['out_invoice', 'out_refund']),
            ('partner_id.face_subjected', '=', True),
        ]
        q = OOQuery(self, cursor, uid)
        sql = q.select(['id']).where(search_params)
        cursor.execute(*sql)

        for result in cursor.fetchall():
            res[result[0]] = True
        return res

    _columns = {
        'face_subjected': fields.function(
            _is_face_subjected, type='boolean', string='Enviar a FACe',
            method=True, store=True, select=1, readonly=True,
            help=u"Estará activo cuando deba mandarse a FACe"
        ),
    }

AccountInvoice()
