# -*- coding: utf-8 -*-

import netsvc
import pooler
from datetime import datetime, timedelta

DEFAULT_COUNTRY = 'ES'

IBAN_MSG = u'El compte %s ja te IBAN (%s) i no coincideix amb el calculat (%s)'
NO_BANK_MSG = u'For account %s there are no bank entity with code %s'
MANY_BANK_MSG = u'For account %s there are many bank entities with code %s'
NO_BIC_MSG = u'The bank %s code %s (id:%s) has no BIC, you should fix it'


def migrate(cursor, installed_version):
    """
    " This migration script generates the IBAN codes from acc_numbers,
    " Also reload the bank list from l10n_ES_partner and try to associate all
    " accounts to a bank because we need all accounts associated with
    " a bank entity with a valid BIC code
    """
    
    uid = 1
    logger = netsvc.Logger()
    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         u'Calculant IBANs')

    cursor.execute("SELECT id from res_users where id = %s" % (uid,))
    if not cursor.fetchall():
        logger.notifyChannel('migration', netsvc.LOG_ERROR,
                             u'No existeix uid:%s (superusuari).' % uid)
        raise Exception(u"No existeix uid:%s" % uid)

    pool = pooler.get_pool(cursor.dbname)

    # Reload bank list xml
    wizard = pool.get('l10n.es.partner.import.wizard')
    wizard.action_import(cursor, uid, [])
    
    # Accounts check and update
    bank_obj = pool.get('res.bank')
    country_obj = pool.get('res.country')
    bank_account_obj = pool.get('res.partner.bank')
    model_obj = pool.get('ir.model.data')

    search_params = [('code', '=', DEFAULT_COUNTRY)]
    country_ids = country_obj.search(cursor, uid, search_params)
    if not country_ids:
        raise Exception(u"No s'ha trobat cap pais amb el codi %s" %
                        DEFAULT_COUNTRY)
    elif len(country_ids) > 1:
        raise Exception(u"Més d'un pais amb el codi %s" % DEFAULT_COUNTRY)
    default_country_id = country_ids[0]

    context = {'active_test': False}
    bank_account_ids = bank_account_obj.search(cursor, uid, [], 
                                               context=context) # All
    for bank_account_id in bank_account_ids:
        vals = {}

        bank_account = bank_account_obj.browse(cursor, uid, bank_account_id)
        if not bank_account.acc_number:
            continue
        
        bank_code = bank_account.acc_number.replace(' ', '')[0:4]
        search_params = [('code', '=', bank_code)]
        bank_entity_ids = bank_obj.search(cursor, uid, search_params)
        if len(bank_entity_ids) == 1:
            bank_entity_id = bank_entity_ids[0]
            bank = bank_obj.browse(cursor, uid, bank_entity_id)
            if not bank.bic:
                logger.notifyChannel('migration', netsvc.LOG_WARNING,
                                 NO_BIC_MSG % (bank.name, bank.code, bank.id))
            vals.update({
                'bank': bank_entity_id
            })
        elif len(bank_entity_ids) > 1:
            # Check if only one loaded from xml
            search_params = [('model', '=', 'res.bank'),
                             ('res_id', 'in', bank_entity_ids)]
            model_ids = model_obj.search(cursor, uid, search_params)
            # If only one found, move all the bank accounts
            # to the one created with the xml and deactivate
            # the manual created ones
            if len(model_ids) == 1:
                xml_bank_id = model_obj.read(cursor, uid, model_ids,
                                            ['res_id'])[0]['res_id']
                not_from_xml = list(set(bank_entity_ids) - set([xml_bank_id]))
                search_params = [('bank', 'in', not_from_xml)]
                account_ids = bank_account_obj.search(cursor, uid,
                                                      search_params)
                bank_account_obj.write(cursor, uid, account_ids,
                                       {'bank': xml_bank_id})
                bank_obj.write(cursor, uid, not_from_xml,
                               {'active': False})
                # Write the bank in the origin account
                vals.update({
                    'bank' : xml_bank_id
                })
            else:
                logger.notifyChannel('migration', netsvc.LOG_WARNING,
                                     MANY_BANK_MSG % (bank_account.id,
                                                      bank_code))
        else:
            logger.notifyChannel('migration', netsvc.LOG_WARNING,
                                 NO_BANK_MSG % (bank_account.id, bank_code))
        
        acc_country_id = (bank_account.acc_country_id
                          and bank_account.acc_country_id.id
                          or default_country_id)
        acc_country_code = (bank_account.acc_country_id
                            and bank_account.acc_country_id.code
                            or DEFAULT_COUNTRY)

        iban = bank_account_obj.calculate_iban(cursor, uid,
                                               bank_account.acc_number,
                                               acc_country_code)
        if bank_account.iban and bank_account.iban.upper() != iban:
            logger.notifyChannel('migration', netsvc.LOG_WARNING,
                                 IBAN_MSG % (bank_account.id, bank_account.iban,
                                             iban))
        else:
            vals.update({
                    'acc_country_id': acc_country_id,
                    'iban': iban,
                    'state': 'iban'
            })
            bank_account.write(vals)
    logger.notifyChannel('migration', netsvc.LOG_INFO,
                 'Calculats %s IBANs' % len(bank_account_ids))
