# -*- coding: utf-8 -*-

from addons.gisceupgrade_migratoor.gisceupgrade_migratoor import GisceUpgradeMigratoor
from addons.gisceupgrade_migratoor.gisceupgrade_migratoor import log


def migrate(cursor, installed_version):
    """Canvis a executar relatius al mòdul base
    """
    log('Migration from %s' % installed_version)

    mig = GisceUpgradeMigratoor('l10n_ES_partner', cursor)
    mig.backup(only_load=True)
    mig.pre_xml({'res.bank': ['code']}, force_xmlfiles=['data_banks.xml'])

