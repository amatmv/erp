# -*- coding: utf-8 -*-

import netsvc
import pooler
from datetime import datetime, timedelta

DEFAULT_COUNTRY = 'ES'

IBAN_MSG = u'El compte %s ja te IBAN (%s) i no coincideix amb el calculat (%s)'
NO_BANK_MSG = u'For account %s there are no bank entity with code %s'
MANY_BANK_MSG = u'For account %s there are many bank entities with code %s'
NO_BIC_MSG = u'The bank %s code %s (id:%s) has no BIC, you should fix it'


def migrate(cursor, installed_version):
    """
    " This migration script generates the IBAN codes from acc_numbers,
    " Also reload the bank list from l10n_ES_partner and try to associate all
    " accounts to a bank because we need all accounts associated with
    " a bank entity with a valid BIC code
    """
    
    uid = 1
    logger = netsvc.Logger()
    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         u'Reloading bank list')

    pool = pooler.get_pool(cursor.dbname)

    # Reload bank list xml
    wizard = pool.get('l10n.es.partner.import.wizard')
    wizard.action_import(cursor, uid, [])

    logger.notifyChannel('migration', netsvc.LOG_INFO,
                 u'Reloaded bank list')
