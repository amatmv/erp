# -*- coding: utf-8 -*-
{
  "name": "AEAT Model 193",
  "description": """
Módulo para la presentación del modelo 193

Retenciones e ingresos a cuenta sobre determinados rendimientos de capital mobiliario. Retenciones e ingresos a cuenta sobre determinadas rentas. Resumen anual.

Basado en la orden EHA/3377/2011, de 1 de diciembre, por el que se aprueban los diseños físicos y lógicos.
    """,
  "version": "0-dev",
  "author": "Joan M. Grande",
  "category": "Localisation/Accounting",
  "depends": ['account'],
  "init_xml": [],
  "demo_xml": [],
  "update_xml": ['mod193_view.xml', 'wizard/export_mod193_view.xml', 'security/ir.model.access.csv'],
  "active": False,
  "installable": True
}
