# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _
import time
import base64
import unicodedata


def unaccent(text):
    if isinstance( text, str ):
        text = unicode( text, 'utf-8' )
    return unicodedata.normalize('NFKD', text ).encode('ASCII', 'ignore')

class wizard_export_mod193(osv.osv_memory):

    _name = 'wizard.export.mod193'
    _description = "Export AEAT Model 193 to BOE format"

    ########################
    ### HELPER FUNCTIONS ###
    ########################
    def _formatString(self, text, length, fill=' ', align='<'):
        """
        Formats the string into a fixed length ASCII (iso-8859-1) record.

        Note:
            'Todos los campos alfanuméricos y alfabéticos se presentará
            alineados a la izquierda y rellenos de blancos por la derecha,
            en mayúsculas sin caracteres especiales, y sin vocales acentuadas.
            Para los caracteres específicos del idioma se utilizará la
            codificación ISO-8859-1. De esta forma la letra “Ñ” tendrá el
            valor ASCII 209 (Hex. D1) y la “Ç”(cedilla mayúscula) el
            valor ASCII 199 (Hex. C7).'
        """

        if not text:
            return fill*length

        #
        # Replace accents
        #
        replacements = [
            (u'Á', 'A'),(u'á', 'A'),
            (u'É', 'E'),(u'é', 'E'),
            (u'Í', 'I'),(u'í', 'I'),
            (u'Ó', 'O'),(u'ó', 'O'),
            (u'Ú', 'U'),(u'ú', 'U'),
            (u'Ä', 'A'),(u'ä', 'A'),
            (u'Ë', 'E'),(u'ë', 'E'),
            (u'Ï', 'I'),(u'ï', 'I'),
            (u'Ö', 'O'),(u'ö', 'O'),
            (u'Ü', 'U'),(u'ü', 'U'),
            (u'À', 'A'),(u'à', 'A'),
            (u'È', 'E'),(u'è', 'E'),
            (u'Ì', 'I'),(u'ì', 'I'),
            (u'Ò', 'O'),(u'ò', 'O'),
            (u'Ù', 'U'),(u'ù', 'U'),
            (u'Â', 'A'),(u'â', 'A'),
            (u'Ê', 'E'),(u'ê', 'E'),
            (u'Î', 'I'),(u'î', 'I'),
            (u'Ô', 'O'),(u'ô', 'O'),
            (u'Û', 'U'),(u'û', 'U')]

        #
        # String uppercase
        #
        text = unaccent( text.upper())

        #
        # Turn text (probably unicode) into an ASCII (iso-8859-1) string
        #
        if isinstance(text, (unicode)):
            ascii_string = text.encode('iso-8859-1', 'ignore')
        else:
            ascii_string = str(text or '')
        # Fix one space
        ' '.join([f.upper() for f in text.split(' ') if f])
        # Cut the string if it is too long
        if len(ascii_string) > length:
            ascii_string = ascii_string[:length]
        # Format the string
        if align == '<':
            ascii_string = (str(ascii_string) +
                            (length-len(str(ascii_string)))*fill)
        elif align == '>':
            ascii_string = ((length-len(str(ascii_string)))*fill +
                            str(ascii_string))
        else:
            assert (False,
                    _('Wrong aling option. It should be < or >'))

        # Sanity-check
        assert len(ascii_string) == length, \
                            _(u"The formated string must match "
                              u"the given length")
        # Return string
        
        return ascii_string 

    def _formatDate(self, date, length, fill=' ', align='<'):
        """
        Formats the date into a fixed length ASCII (iso-8859-1) record.
        """

        if not date:
            return fill*length
        str_date = date[:4] + date[5:7]+ date[8:10]

        if align == '<':
            str_date = (str_date +
                            (length-len(str_date))*fill)
        elif align == '>':
            str_date = ((length-len(str_date))*fill +
                            str_date)
        else:
            assert (False,
                    _('Wrong aling option. It should be < or >'))

        # Sanity-check
        assert len(str_date) == length, \
                            _(u"The formated string must match "
                              u"the given length")
        # Return string
        
        return str_date 

    def _formatNumber(self, number, int_length, dec_length=0,
                      include_sign=False):
        """
        Formats the number into a fixed length ASCII (iso-8859-1) record.
        Note:
            'Todos los campos numéricos se presentarán alineados a la derecha
             y rellenos a ceros por la izquierda sin signos y sin empaquetar.'
        """
        #
        # Separate the number parts (-55.23 => int_part=55, dec_part=0.23, sign='N')
        #
        if number == '':
            number = 0.0

        number = float(number)
        if "%s.2f"%number == '0.00':
            sign =' '
        else:
            sign = number >= 0.0 and ' ' or 'N'
            
        number = abs(number)
        int_part = int(number)

        ##
        ## Format the string
        ascii_string = ''
        if include_sign:
            ascii_string += sign
            
        if dec_length > 0:
            ascii_string += '%0*.*f' % (int_length+dec_length+1, 
                                        dec_length, number)
            ascii_string = ascii_string.replace('.','')
        elif int_length > 0:
            ascii_string += '%.*d' % (int_length, int_part)
            
        # Sanity-check
        assert len(ascii_string) == ((include_sign and 1 or 0) +
                                     int_length + dec_length), \
                                    _(u"The formated string must "
                                      u"match the given length")
        # Return the string
        return ascii_string

    def _formatBoolean(self, value, yes='X', no=' '):
        """
        Formats a boolean value into a fixed length ASCII (iso-8859-1) record.
        """
        return value and yes or no

    def action_export(self, cr, uid, ids, context=None):
        """
        Action that exports the data into a BOE formated text file
        """
        attachment_obj = self.pool.get('ir.attachment')
        if context is None:
            context = {}
        report_obj = self.pool.get('l10n.es.aeat.mod193.report')
        report_id = context.get('report_id', False)
        report = report_obj.browse(cr, uid, report_id)
        file_contents = ''

        ##
        ## Add header record
        file_contents += self._get_formated_header_record(report)

        ##
        ## Add the partner records
        for record in report.record_ids:
            file_contents += self._get_formated_record(report, record)

        ##
        ## Generate the file and save as attachment
        file = base64.encodestring(file_contents)
        file_name = _("%s_report_%s.txt") % ('193',
                                             time.strftime(_("%Y-%m-%d")))
        vals = {"name" : file_name,
                "datas" : file,
                "datas_fname" : file_name,
                "res_model" : "l10n.es.aeat.mod193.report",
                "res_id" : context['report_id']
                }
        attachment_obj.create(cr, uid, vals, context=context)
        self.write(cr, uid, ids, {'state': 'end'})

    def _get_formated_header_record(self, report):
        """
        Returns a type 1, declaration/company, formated record.

        Format of the record:
            Tipo registro 1 – Registro de declarante:
            Posiciones  Descripción
            1           Tipo de Registro
            2-4         Modelo Declaración
            5-8         Ejercicio
            9-17        NIF del declarante
            18-57       Apellidos y nombre o razón social del declarante
            58          Tipo de soporte
            59-107      Persona con quien relacionarse
                59-67   Telefono
                68-107  Apellidos y nombre
            108-120     Número identificativo de la declaración
            121-122     Declaración complementaria o substitutiva
            123-135     Número identificativo de la declaración anterior
            136-144     Número total de perceptores
            145-159     Base de rentenciones e ingresos a cuenta
            160-174     Retenciones e ingresos a cuenta
            175-189     Retenciones e ingresos a cuenta ingresados
            190-219     Blancos
            220-234     Gastos
            235         Naturaleza del declarante
            236-487     Blancos
            488-500     Sello electrónico
        """
        text = ''

        text += '1'                                           # Tipo de Registro
        text += '193'                                         # Modelo Declaración
        text += self._formatString(report.fiscalyear_id.date_stop[:4], 4)   # Ejercicio
        text += self._formatString(report.company_vat, 9)          # NIF del declarante
        text += self._formatString(report.company_name, 40)     # Apellidos y nombre o razón social del declarante
        text += self._formatString(report.support_type, 1)         # Tipo de soporte
        text += self._formatString(report.contact_phone, 9)       # Persona de contacto (Teléfono)
        text += self._formatString(report.contact_name, 40)        # Persona de contacto (Apellidos y nombre)
        text += self._formatString(report.name, 13, '0', '<')      # Número identificativo de la declaración
        if report.type == 'C':
            text += self._formatString(report.type, 2, align='<')                # Declaración complementaria o substitutiva
        elif report.type == 'S':
            text += self._formatString(report.type, 2, align='>')                # Declaración complementaria o substitutiva
        else:
            text += 2*' '
        text += self._formatString(report.previous_name, 13, '0', '<')     # Número identificativo de la declaración anterior
        text += self._formatNumber(report.partner_number, 9)          # Número total de perceptores
        text += self._formatNumber(report.amount_base, 13, 2, False)               # Base de retenciones e ingresos a cuenta
        text += self._formatNumber(report.amount_tax, 13, 2, False)               # Retenciones e ingresos a cuenta
        text += self._formatNumber(report.amount_tax2, 13, 2, False)               # Retenciones e ingresos a cuenta ingresados
        text += 30*' '                                       # Blancos
        text += 15*'0'    # Gastos
        text += self._formatString(report.company_nature, 1) #Naturaleza del declarante
        text += 252*' '                                        # Blancos
        text += 13*' '                                        # Sello electrónico
        text += '\r\n'


        assert len(text) == 502, _("The type 1 record must be 502 characters long")
        return text

    def _get_formated_record(self, report, record):
        """
        Returns a type 2, partner, formated record.

        Format of the record:
            Tipo registro 1 – Registro de declarante:
            Posiciones  Descripción
            1           Tipo de Registro
            2-4         Modelo Declaración
            5-8         Ejercicio
            9-17        NIF del declarante
            18-26       NIF del perceptor
            27-35       NIF del representante legal
            36-75       Apellidos y nombre o razón social del perceptor
            76          Pago a un mediador
            77-78       Código de provincia
            79          Clave de código
            80-91       Código emisor
            92          Clave de percepción
            93-94       Naturaleza
            95          Pago
            96          Tipo código
            97-116      Código cuenta valores/número operación prestamo
            117         Pendiente
            118-121     Ejercicio devengo
            122         Tipo de percepción
            123-135     Importe de percepción
            136-138     Blanco
            139-151     Importe reducciones
            152-164     Base retenciones e ingresos a cuenta
            165-168     % Retención
            169-181     Retenciones e ingresos a cuenta
            182-192     Penalizaciones
            193-207     Blancos
            208         Naturaleza del declarante
            209-216     Fecha de inicio del préstamo
            217-224     Fecha de vencimiento del préstamo
            225-236     Compensaciones
            237-248     Garantías
            249-500     Blancos
        """
        text = ''

        text += '2'                                           # Tipo de Registro
        text += '193'                                         # Modelo Declaración
        text += self._formatString(report.fiscalyear_id.date_stop[:4], 4)   # Ejercicio
        text += self._formatString(report.company_vat, 9)          # NIF del declarante
        text += self._formatString(record.partner_vat, 9)          # NIF del perceptor
        text += self._formatString(record.lr_vat, 9)          # NIF del representante legal
        text += self._formatString(record.partner_id.name, 40)     # Apellidos y nombre o razón social del perceptor
        text += self._formatBoolean(record.mediator) #pago a un mediador
        text += self._formatString(record.state_id.code, 2) #Código de provincia
        text += self._formatString(record.code_key, 1) #Clave de código
        text += self._formatString(record.code_value, 12) #Código emisor
        text += self._formatString(record.incoming_key, 1) #Clave de percepción
        text += self._formatString(record.nature, 2) #Naturaleza
        text += self._formatString(record.payment, 1) #Pago
        text += self._formatString(record.code_type, 1) #Tipo código
        text += self._formatString(record.code_bank, 20) #Código cuenta de valores
        text += self._formatString(record.pending, 1) #Pendiente
        text += 4*'0' #Ejercicio de devengo
        text += self._formatString(record.incoming_type, 1) #Tipo de percepción
        text += self._formatNumber(record.amount, 11, 2, False) #Importe de la percepción
        text += 3*' ' #Blancos
        text += self._formatNumber(record.reduction_amount, 11, 2, False) #Importe de las reducciones
        text += self._formatNumber(record.amount_base, 11, 2, False) #Base retenciones e ingresos a cuenta
        text += self._formatNumber(record.tax_percent, 2, 2, False) #% Retencion
        text += self._formatNumber(record.amount_tax, 11, 2, False) #Retenciones e ingresos a cuenta
        text += self._formatNumber(record.penalty, 9, 2, False) #Penalizaciones
        text += 15*' ' #Blancos
        text += self._formatString(record.company_nature, 1) #Naturaleza del declarante
        text += self._formatDate(record.lending_start, 8, '0') #Inicio del prestamo
        text += self._formatDate(record.lending_end, 8, '0') #Fecha de vencimiento del prestamo
        text += self._formatNumber(record.compensation, 10, 2, False) #Compensaciones
        text += self._formatNumber(record.warranty, 10, 2, False) #Garantias
        text += 252*' '                                        # Blancos
        text += '\r\n'

        assert len(text) == 502, _("The type 2 record must be 502 characters long")
        return text

    _columns = {
        'state': fields.selection([('init', 'Init'),
                                   ('end', 'End')],
                                  'State'),
    }

    _defaults = {
        'state': lambda *a: 'init',
    }
wizard_export_mod193()