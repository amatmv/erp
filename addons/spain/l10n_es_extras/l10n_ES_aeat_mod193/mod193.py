# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv, fields


class l10n_es_aeat_mod193_report(osv.osv):

    _name = 'l10n.es.aeat.mod193.report'
    _description = 'AEAT 193 report'

    def _get_default_company(self, cursor, uid, context=None):

        user_obj = self.pool.get('res.users')
        return user_obj.browse(cursor, uid, uid).company_id.id

    def _get_default_company_name(self, cursor, uid, context=None):

        user_obj = self.pool.get('res.users')
        return user_obj.browse(cursor, uid, uid).company_id.name

    def _get_default_company_vat(self, cursor, uid, context=None):

        user_obj = self.pool.get('res.users')
        vat = user_obj.browse(cursor, uid, uid).company_id.partner_id.vat
        if vat.startswith('ES'):
            return vat[2:]
        return vat

    def _get_default_name(self, cursor, uid, context=None):
        return '193'.ljust(13, '0')

    def _get_report_from_record(self, cursor, uid, ids, context=None):
        query = '''select distinct report_id
                from l10n_es_aeat_mod193_record
                where id in %s'''
        cursor.execute(query, (tuple(ids), ))
        res = []
        for result in cursor.fetchall():
            res.append(result[0])
        return res

    def _get_totals(self, cursor, uid, ids, field_name, arg, context=None):
        '''return total amounts from records in report'''
        report_obj = self.pool.get('l10n.es.aeat.mod193.report')
        res = {}
        for report in report_obj.browse(cursor, uid, ids, context=context):
            totals = {'amount_base': 0,
                      'amount_tax': 0,
                      'amount_tax2':0}
            for record in report.record_ids:
                totals['amount_base'] += record.amount_base
                totals['amount_tax'] += record.amount_tax
                if record.incoming_key == 'C':
                    totals['amount_tax2'] += record.amount_tax
                if (record.incoming_key in ('A', 'B', 'D')
                    and record.payment in ('1', '3')):
                    totals['amount_tax2'] += record.amount_tax
            res[report.id] = totals
        return res

    def _get_partner_number(self, cursor, uid, ids, field_name,
                            arg, context=None):
        record_obj = self.pool.get('l10n.es.aeat.mod193.record')
        res = {}
        for report_id in ids:
            search_params = [('report_id', '=', report_id)]
            res[report_id] = record_obj.search(cursor, uid, search_params,
                                               count=True,
                                               context=context)
        return res

    _columns = {
        'company_id': fields.many2one('res.company', 'Company',
                                      required=True),
        'fiscalyear_id': fields.many2one('account.fiscalyear', 'Fiscal year',
                                         required=True),
        'name': fields.char('Declaration number', required=True, size=13),
        'previous_name': fields.char('Prev. declaration number', size=13,
                                     help=u"Previous declaration number. "
                                          u"Leave blank if declaration "
                                          u"type is Normal"),
        'company_name': fields.char('Company Name', size=40, required=True),
        'company_vat': fields.char('Company VAT', size=9,
                                   required=True),
        'contact_name': fields.char('Contact Name', size=40,
                                    required=True),
        'contact_phone': fields.char('Contact Phone', size=9,
                                     required=True),
        'type': fields.selection([('N','Normal'),
                                  ('C','Complementary'),
                                  ('S','Substitutive')],
                                 'Statement Type'),            
        'support_type': fields.selection([('C','DVD'),
                                          ('T','Telematics')],
                                         'Support Type'),
        'company_nature': fields.char('Company nature', size=1),
        'state': fields.selection([('draft', 'Draft'),
                                   ('computed', 'Computed'),
                                   ('done', 'Done')],
                                  'State'),
        'amount_base': fields.function(_get_totals, string='Amount base',
                        multi='totals', method=True,
                        type='float', digits=(15,2),
                        store={'l10n.es.aeat.mod193.report': 
                               (lambda self, cr, uid, ids, c={}: ids,
                                ['accion_desde','accion_hasta'], 20),
                               'l10n.es.aeat.mod193.record': 
                                (_get_report_from_record,
                                ['amount_base', 'amount_tax'], 20)}),
        'amount_tax': fields.function(_get_totals, string='Amount tax',
                        multi='totals', method=True,
                        type='float', digits=(15,2),
                        store={'l10n.es.aeat.mod193.report': 
                               (lambda self, cr, uid, ids, c={}: ids,
                                ['accion_desde','accion_hasta'], 20),
                               'l10n.es.aeat.mod193.record': 
                                (_get_report_from_record,
                                ['amount_base', 'amount_tax'], 20)}),
        'amount_tax2': fields.function(_get_totals, string='Amount tax2',
                        multi='totals', method=True,
                        type='float', digits=(15,2),
                        store={'l10n.es.aeat.mod193.report': 
                               (lambda self, cr, uid, ids, c={}: ids,
                                ['accion_desde','accion_hasta'], 20),
                               'l10n.es.aeat.mod193.record': 
                                (_get_report_from_record,
                                ['amount_base', 'amount_tax'], 20)},
                        help=u"Only taxes from records with "
                             u"incoming key equals to C or incoming key "
                             u"in A, B, D and payment in 1, 3"),
        'partner_number': fields.function(_get_partner_number,
                        string='Partner number',
                        method=True, type='integer',
                        store={'l10n.es.aeat.mod193.report': 
                               (lambda self, cr, uid, ids, c={}: ids,
                                ['record_ids'], 20),
                               'l10n.es.aeat.mod193.record': 
                                (_get_report_from_record,
                                ['report_id'], 20)}),
    }

    _defaults = {
        'company_id': _get_default_company,
        'company_name': _get_default_company_name,
        'company_vat': _get_default_company_vat,
        'name': _get_default_name,
        'type': lambda *a: 'N',
        'support_type': lambda *a: 'T',
        'state': lambda *a: 'draft',
    }

l10n_es_aeat_mod193_report()


class l10n_es_aeat_mod193_record(osv.osv):

    _name = 'l10n.es.aeat.mod193.record'
    _description = 'Partner record'

    _columns = {
        'report_id': fields.many2one('l10n.es.aeat.mod193.report', 'AEAT 193 Report',
                                     required=True),
        'partner_id': fields.many2one('res.partner', 'Partner', required=True),
        'partner_vat': fields.char('VAT', size=9, required=True),
        'lr_vat': fields.char('L.R. Vat number', size=9,
                              help="Legal representative vat number"),
        'mediator': fields.boolean('Mediator',
                                   help="Payment to a mediator"),
        'state_id': fields.many2one('res.country.state',
                                            'Country State',
                                            required=True),
        'code_key': fields.selection([('1', 'VAT'),
                                      ('2', 'ISIN'),
                                      ('3', 'International without ISIN')],
                                     'Code key'),
        'code_value': fields.char('Code Value', size=12),
        'incoming_key': fields.selection([('A', 'A'),
                                          ('B', 'B'),
                                          ('C', 'C'),
                                          ('D', 'D')],
                                         'Incoming key'),
        'nature': fields.char('Nature', size=2, required=True),
        'payment': fields.selection([('1', 'As emitter'),
                                     ('2', 'As national value mediator'),
                                     ('3', 'As international value mediator')],
                                    'Payment', required=True),
        'code_type': fields.selection([('C', 'Bank account'),
                                       ('O', 'Other'),
                                       ('P', 'Securities lending')],
                                      'Code type'),
        'code_bank': fields.char('Bank code', size=20),
        'pending': fields.selection([('X', 'Pending'),
                                     (' ', 'No pending')],
                                    'Pending'),
        'fiscal_year_id': fields.many2one('account.fiscalyear', 'Fiscal year',
                                          required=True,),
        'incoming_type': fields.selection([('1', 'Money'),
                                           ('2', 'Kind')],
                                          'Incoming type'),
        'amount': fields.float('Amount', digits=(13,2)),
        'reduction_amount': fields.float('Reduction Amount', digits=(13,2)),
        'amount_base': fields.float('Amount base', digits=(13,2)),
        'tax_percent': fields.float('Tax percent', digits=(4,2)),
        'amount_tax': fields.float('Amount Tax', digits=(13,2)),
        'penalty': fields.float('Penalty', digits=(9,2)),
        'company_nature': fields.char('Company nature', size=1),
        'lending_start': fields.date('Lending start'),
        'lending_end': fields.date('Lending end'),
        'compensation': fields.float('Compensation', digits=(12,2)),
        'warranty': fields.float('Warranty', digits=(12,2)),
        'postal_address_id': fields.many2one('res.partner.address',
                                             'Postal Address'),
        'fiscal_address_id': fields.many2one('res.partner.address',
                                             'Fiscal Address'),
    }

    _defaults = {
        'reduction_amount': lambda *a: 0,
        'compensation': lambda *a: 0,
        'warranty': lambda *a: 0,
    }

l10n_es_aeat_mod193_record()


class l10n_es_aeat_mod193_report(osv.osv):

    _name = 'l10n.es.aeat.mod193.report'
    _inherit = 'l10n.es.aeat.mod193.report'

    _columns = {
        'record_ids': fields.one2many('l10n.es.aeat.mod193.record', 'report_id',
                                      'Records'),
    }

l10n_es_aeat_mod193_report()
