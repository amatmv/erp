# -*- coding: utf-8 -*-
{
  "name": "AEAT base",
  "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Modula base AEAT
""",
  "version": "0-dev",
  "author": "GISCE",
  "category": "Master",
  "depends": ['base', 'account', 'l10n_chart_ES'],
  "init_xml": [],
  "demo_xml": [],
  "update_xml": ['l10n_ES_aeat_view.xml'],
  "active": False,
  "installable": True
}
