# -*- encoding: utf-8 -*-
from osv import osv, fields
from tools.translate import _


class WizardIsRegisteredAEAT(osv.osv_memory):

    _name = 'wizard.is.registered.aeat'

    def _get_default_message(self, cursor, uid, context=None):
        if context is None:
            context = {}

        selected_ids = context.get('active_ids', [])
        from_model = context.get('from_model')
        msg = ''
        if from_model == 'res.partner':
            msg = _('Se han seleccionado {0} partners.\n\n').format(
                len(selected_ids))
        elif from_model == 'account.sii.register':
            msg = _('Se han seleccionado {0} registros.\n\n').format(
                len(selected_ids))

        return msg

    def check_is_registered(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        res_partner = self.pool.get('res.partner')

        wiz = self.browse(cursor, uid, ids[0])

        result = wiz.result

        partner_ids = []

        if context.get('from_model') == 'res.partner':
            partner_ids = context.get('active_ids', [])
        elif context.get('from_model') == 'account.sii.register':
            register_obj = self.pool.get('account.sii.register')
            register_ids = context.get('active_ids', [])
            partner_ids = list(set([
                register_data['invoice_partner_id'][0]
                for register_data in register_obj.read(
                    cursor, uid, register_ids, ['invoice_partner_id']
                )
            ]))

        registered = res_partner.check_is_aeat_registered(
            cursor, uid, partner_ids, context
        )

        invalid_ids = registered['invalid']
        valid_ids = registered['valid']

        if valid_ids or invalid_ids:
            result += _(
                'Se han comprobado {0} NIFs de los cuales:\n'
                '    - {1} se han marcado como censados\n'
                '    - {2} se han marcado como no censados\n'
            ).format(len(partner_ids), len(valid_ids), len(invalid_ids))
        else:
            result = _('No se ha podido realizar la comprobación')

        wiz.write({'state': 'end', 'result': result})

    _columns = {
        'state': fields.selection(
            [('init', 'Initial'), ('end', 'End')], 'State'
        ),
        'result': fields.text('Resultado', readonly=True)
    }

    _defaults = {
        'state': lambda *a: 'init',
        'result': _get_default_message
    }


WizardIsRegisteredAEAT()
