# -*- encoding: utf-8 -*-
from osv import osv, fields
from tools.translate import _
from tools import config
import base64
import StringIO

from sii.models.basic_models import *
from sii.resource import SII
from sii.server import SiiService
from addons.l10n_ES_aeat_sii.account_sii import (
    SII_REGISTERED_STATE, UNREGISTERED_NIF_CODES
)
import csv
from datetime import datetime, date

STATES_TO_EXCLUDE = ['Correcto']

DEFAULT_RESULT_HEADERS = [
    'EstadoRegistro', 'Resultado', 'CSV', 'FechaRegistro', 'ObjetoEnviado'
]


def get_line_data(line, line_info_cols=9):
    line_invoice_data = line[:line_info_cols]
    rest_of_line = line[line_info_cols:]
    return line_invoice_data, rest_of_line


def get_invoice_from_line(line_invoice_data, invoice_direction, sii_description,
                          previous_result, previous_error_code,
                          company_name, company_vat,
                          clave_regimen_especial='01'):
    country_code = 'ES'
    if previous_error_code in UNREGISTERED_NIF_CODES:
        partner_aeat_registered = False
    else:
        partner_aeat_registered = True

    # Parse file invoice data
    numero_factura = line_invoice_data[0]
    fecha_factura = line_invoice_data[1]
    partner_name = line_invoice_data[2]
    partner_vat = ''.join([x for x in line_invoice_data[3] if x.isalnum()])
    base_iva = float(line_invoice_data[4])
    cuota_iva = float(line_invoice_data[5])
    tipo_impositivo = float(line_invoice_data[6][:2])
    tipo_factura = line_invoice_data[7]
    total_factura = float(line_invoice_data[8])

    if tipo_factura == 'normal':
        rectificative_type = 'N'
    elif tipo_factura == 'abonadora':
        rectificative_type = 'A'
    elif tipo_factura == 'rectificativa':
        rectificative_type = 'R'

    invoice_type = '{}_invoice'.format(invoice_direction)

    # Generate basic models
    fecha_emision_factura = datetime.strptime(
        fecha_factura, '%d/%m/%Y'
    ).strftime('%Y-%m-%d')
    # total_factura = base_iva + cuota_iva
    today = date.today().strftime('%Y-%m-%d')

    sii_registered = bool(
        previous_result in SII_REGISTERED_STATE
        or previous_error_code == '3000'
    )

    spain = Country(code=country_code)
    partner_invoice = Partner(
        name=partner_name, nif=partner_vat, country=spain,
        aeat_registered=partner_aeat_registered
    )

    partner_company = Company(
        partner_id=Partner(
            name=company_name, nif=company_vat,
            country=spain
        )
    )

    period = Period(name=fecha_factura.split('/', 1)[-1])
    name_iva = 'IVA 21%'
    tax_iva = Tax(
        name=name_iva, amount=float(tipo_impositivo / 100), type='percent')
    invoice_line = [
        InvoiceLine(price_subtotal=base_iva, invoice_line_tax_id=[tax_iva])
    ]

    tax_line = [
        InvoiceTax(
            base=base_iva, name=name_iva, tax_amount=cuota_iva, tax_id=tax_iva
        )
    ]
    invoice = ''
    if invoice_direction == 'out':
        invoice = Invoice(
            invoice_type=invoice_type,
            period_id=period,
            journal_id=False,
            fiscal_position=False,
            address_contact_id=False,
            number=numero_factura,
            amount_total=total_factura,
            amount_untaxed=base_iva,
            amount_tax=cuota_iva,
            tax_line=tax_line,
            invoice_line=invoice_line,
            rectificative_type=rectificative_type,
            partner_id=partner_invoice,
            company_id=partner_company,
            date_invoice=fecha_emision_factura,
            sii_registered=sii_registered,
            sii_out_clave_regimen_especial=clave_regimen_especial,
            sii_description=sii_description,
        )
    elif invoice_direction == 'in':
        invoice = Invoice(
            invoice_type=invoice_type,
            period_id=period,
            journal_id=False,
            fiscal_position=False,
            address_contact_id=False,
            number=False,
            origin=numero_factura,
            amount_total=total_factura,
            amount_untaxed=base_iva,
            amount_tax=cuota_iva,
            tax_line=tax_line,
            invoice_line=invoice_line,
            rectificative_type=rectificative_type,
            partner_id=partner_invoice,
            company_id=partner_company,
            date_invoice=today,
            origin_date_invoice=fecha_emision_factura,
            sii_registered=sii_registered,
            sii_in_clave_regimen_especial=clave_regimen_especial,
            sii_description=sii_description,
        )

    return invoice


def send_invoice(invoice, test_mode, certificate, key, proxy):
    validation = SII(invoice).validate_invoice()
    objeto_validado = validation['object_validated']
    try:
        if validation['successful']:
            # If validation is succesful, send to SII
            service_sii = SiiService(certificate, key, proxy, test_mode)
            result = service_sii.send(invoice)

            respuesta_linea = result['RespuestaLinea'][0]
            codigo_error = respuesta_linea.get(
                'CodigoErrorRegistro', False)
            descripcion_error = respuesta_linea.get(
                'DescripcionErrorRegistro', False)
            result_text = ''
            if codigo_error:
                result_text += '[{}] '.format(codigo_error)
            if descripcion_error:
                result_text += descripcion_error

            estado_registro = respuesta_linea.get('EstadoRegistro', False)
            resultado = result_text
            csv_res = respuesta_linea.get('CSV', False)
            fecha_registro = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            if result and result.get('TimestampPresentacion', None):
                fecha_registro = datetime.strptime(
                    result['TimestampPresentacion'], '%d-%m-%Y %H:%M:%S'
                ).strftime('%Y-%m-%d %H:%M:%S')
        else:
            # If validation is not successful, show a list of errors
            errors_list = '\n'.join(
                [
                    u'* {}'.format(err)
                    for err in validation['errors']
                ]
            )
            estado_registro = 'ValidacionIncorrecta'
            resultado = str(errors_list)
            csv_res = False
            fecha_registro = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    except Exception as e:
        # If something is wrong (connection, certificate) catch the exeption
        estado_registro = 'Error'
        resultado = str(e)
        csv_res = False
        fecha_registro = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

    res = [estado_registro, resultado, csv_res, fecha_registro, objeto_validado]
    return res


class WizardSendInvoiceSIIFromCSV(osv.osv_memory):
    _name = 'wizard.send.invoice.sii.from.csv'

    def _get_default_info(self, cursor, uid, context=None):
        if context is None:
            context = {}

        return _(
            'Este asistente permite el envío de facturas al SII a través de un '
            'archivo CSV. Las columnas que debe contener el CSV deben estar '
            'separadas por punto y coma ";" y deben ser las siguientes '
            '(respectando el orden):\n'
            '    - Número de factura\n'
            '    - Fecha de factura\n'
            '    - Nombre de la contraparte\n'
            '    - DNI de la contraparte\n'
            '    - Base del IVA\n'
            '    - Cuota del IVA\n'
            '    - Tipo impositivo del IVA\n'
            '    - Tipo de factura (normal, abonadora, rectificativa)\n'
            '    - Total de factura\n'
        )

    def send_invoices_sii_from_csv(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        wiz = self.browse(cursor, uid, ids[0])

        if wiz.ignore_accepted_with_errors:
            STATES_TO_EXCLUDE.append('AceptadoConErrores')

        # Input file variables
        input_file = wiz.input_file
        txt = base64.decodestring(str(input_file))
        csv_file = StringIO.StringIO(txt)
        csv_reader = csv.reader(csv_file, delimiter=';')

        # Output file variables
        output_file = StringIO.StringIO()
        writer = csv.writer(output_file, delimiter=';', quoting=csv.QUOTE_ALL)

        # SII configuration
        proxy = config.get('sii_proxy', None)
        test_mode = bool(int(config.get('sii_test_mode', '1')))
        sii_certificate = config.get('sii_certificate', None)
        sii_key = config.get('sii_key', None)
        if proxy and test_mode:
            proxy = '{}/test'.format(proxy)

        # ERP Company information
        user = self.pool.get('res.users').browse(
            cursor, uid, uid, context=context)
        company_name = user.company_id.partner_id.name
        company_vat = user.company_id.partner_id.vat

        # Headers
        headers_line = next(csv_reader)
        headers, result_headers = get_line_data(headers_line)
        if not result_headers:
            result_headers = DEFAULT_RESULT_HEADERS

        # Write output file with input file data
        writer.writerow(headers + result_headers)
        for line in csv_reader:
            line_invoice_data, previous_result_line = get_line_data(line)
            previous_result = (previous_result_line[0]
                               if previous_result_line else '')
            previous_error_code = (previous_result_line[1][1:5]
                                   if previous_result_line else '')
            if previous_result not in STATES_TO_EXCLUDE:
                invoice = get_invoice_from_line(
                    line_invoice_data=line_invoice_data,
                    previous_result=previous_result,
                    previous_error_code=previous_error_code,
                    invoice_direction=wiz.invoice_type,
                    company_name=company_name,
                    company_vat=company_vat,
                    sii_description=wiz.operation_description
                )
                results = send_invoice(
                    invoice, test_mode, sii_certificate, sii_key, proxy)
                row = line_invoice_data + results
            else:
                row = line_invoice_data + previous_result_line
            writer.writerow(row)

        # Write output file to wizard
        value = output_file.getvalue()
        output_file.close()

        wiz.write({
            'output_file': base64.b64encode(value),
            'state': 'end'
        })

    _columns = {
        'state': fields.selection(
            [('init', 'Inicio'), ('end', 'Fin')], 'Estado'
        ),
        'input_file': fields.binary(
            string='Archivo de entrada', required=True,
        ),
        'output_file': fields.binary(
            string='Archivo de salida',
        ),
        'ignore_accepted_with_errors': fields.boolean(
            string='Ignorar Aceptados con Errores',
            help='Por defecto se ignoran facturas con estado Correcto. Este '
                 'check permite ignorar facturas Aceptadas con Errores'
        ),
        'invoice_type': fields.selection(
            string='Tipo de facturas', required=True,
            selection=[('out', 'Facturas de Cliente'),
                       ('in', 'Facturas de Proveedor')]
        ),
        'operation_description': fields.char(
            string='Descripción de la operación', size=500, required=True,
        ),
        'info': fields.text('Información', readonly=True)
    }

    _defaults = {
        'state': lambda *a: 'init',
        'ignore_accepted_with_errors': lambda *a: False,
        'info': _get_default_info
    }


WizardSendInvoiceSIIFromCSV()
