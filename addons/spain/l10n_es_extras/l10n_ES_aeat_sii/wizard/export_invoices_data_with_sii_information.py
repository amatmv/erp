# -*- encoding: utf-8 -*-
from osv import osv, fields
from addons import get_module_resource
import StringIO
import csv
import base64
import xlwt
from datetime import datetime

TIPO_FACTURAS = [
    ('out', 'Facturas de cliente'),
    ('in', 'Facturas de proveedor'),
    ('all', 'Todas las facturas'),
]


class WizardExportInvoicesDataSII(osv.osv_memory):
    _name = 'wizard.export.invoices.data.sii'

    def build_report_xls(self, headers, results):
        output_report = StringIO.StringIO()
        writer_report = xlwt.Workbook(encoding='utf-8')
        writer_sheet = writer_report.add_sheet("Hoja1")

        title_style = xlwt.XFStyle()
        title_font = xlwt.Font()
        title_font.bold = True
        title_style.font = title_font

        for col, value in enumerate(headers):
            writer_sheet.write(0, col, value, style=title_style)

        fil = 1
        for result in results:
            val_list = [result[x] for x in headers]
            for col, value in enumerate(val_list):
                writer_sheet.write(fil, col, value)
            fil += 1

        writer_report.save(output_report)
        value = output_report.getvalue()
        output_report.close()
        return value

    def build_report_csv(self, headers, results):
        output_report = StringIO.StringIO()
        quoting = csv.QUOTE_ALL
        writer_report = csv.writer(
            output_report, delimiter=';', quoting=quoting
        )
        writer_report.writerow(headers)
        for result in results:
            val_list = [result[x.encode('utf-8')] for x in headers]
            writer_report.writerow(val_list)
        value = output_report.getvalue()
        output_report.close()
        return value

    def build_report(self, headers, results, file_type):
        if file_type == 'xls':
            return self.build_report_xls(headers, results)
        elif file_type == 'csv':
            return self.build_report_csv(headers, results)

    def export_invoices_data(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        wizard = self.browse(cursor, uid, ids[0], context=context)

        header_fields = [
            'numero',
            'contraparte',
            'origen',
            'estado',
            'total_factura',
            'fecha_factura',
            'fecha_factura_original',
            'remesa',
            'posicion_fiscal',
            'enviado_sii',
            'registrado_sii',
            'estado_sii',
            'fecha_registro_sii',
            'estado_registro_sii'
        ]

        sql_file = get_module_resource(
            'l10n_ES_aeat_sii', 'sql', 'export_invoices_data_sii.sql'
        )

        with open(sql_file, 'r') as f:
            sql = f.read()

            if wizard.invoices_type != 'all':
                sql += "\n  AND inv.type LIKE '{}%'".format(
                    wizard.invoices_type
                )
            if wizard.remesa:
                sql += '\n  AND pay_ord.id = {}'.format(wizard.remesa.id)
            if wizard.partner:
                sql += '\n  AND inv.partner_id = {}'.format(wizard.partner.id)
            if wizard.initial_date_invoice:
                sql += "\n  AND inv.date_invoice >= '{}'".format(
                    wizard.initial_date_invoice
                )
            if wizard.end_date_invoice:
                sql += "\n  AND inv.date_invoice <= '{}'".format(
                    wizard.end_date_invoice
                )
            if wizard.initial_origin_date_invoice:
                sql += "\n  AND inv.origin_date_invoice >= '{}'".format(
                    wizard.initial_origin_date_invoice
                )
            if wizard.end_origin_date_invoice:
                sql += "\n  AND inv.origin_date_invoice <= '{}'".format(
                    wizard.end_origin_date_invoice
                )

            cursor.execute(sql)
            results = cursor.dictfetchall()

            file_generated = self.build_report(
                header_fields, results, wizard.file_type)
            base_file_name = 'informacion_facturas_SII'
            filename = '{}_{:%Y-%d-%m_%H-%M-%S}.{}'.format(
                base_file_name, datetime.now(), wizard.file_type
            )
            wizard.write({
                'file': base64.b64encode(file_generated),
                'filename': filename,
                'state': 'end'
            })

        return results

    _columns = {
        'state': fields.selection(
            [('init', 'Initial'), ('end', 'End')], string='Estado'
        ),
        'invoices_type': fields.selection(
            TIPO_FACTURAS, string='Tipo de facturas', required=True
        ),
        'partner': fields.many2one(
            'res.partner', string='Cliente/Empresa',
            help='Si está vacío se buscarán todas las facturas '
                 'independientemente del cliente/empresa'),
        'initial_date_invoice': fields.date(string='Fecha de factura inicial'),
        'end_date_invoice': fields.date(string='Fecha de factura final'),
        'initial_origin_date_invoice': fields.date(
            string='Fecha de factura original inicial'),
        'end_origin_date_invoice': fields.date(
            string='Fecha de factura original final'),
        'remesa': fields.many2one(
            'payment.order', string='Remesa',
            help='Si está vacío se buscarán todas las facturas '
                 'independientemente de la remesa'),
        'filename': fields.char('Nombre del archivo', size=64),
        'file_type': fields.selection(
            [('csv', 'CSV'), ('xls', 'Excel')],
            string='Tipo archivo', required=True
        ),
        'file': fields.binary(
            string='Archivo resultado', filters='*.csv,*.xls'),
    }

    _defaults = {
        'state': lambda *a: 'init',
    }


WizardExportInvoicesDataSII()
