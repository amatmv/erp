# -*- encoding: utf-8 -*-
from osv import osv, fields
from tools.translate import _
from tools import config
from l10n_ES_aeat_sii.account_sii import SII_REGISTER_STATE


class WizardSendInvoiceSII(osv.osv_memory):

    _name = 'wizard.send.invoice.sii'

    def _get_default_message(self, cursor, uid, context=None):
        if context is None:
            context = {}

        aeat_server = {'0': _('Real'), '1': _('Pruebas')}

        msg = _('Servidor AEAT de envío: "{}"').format(
            aeat_server[config['sii_test_mode']]
        )

        selected_ids = context.get('active_ids', [])
        from_model = context.get('from_model')
        if from_model == 'account.invoice':
            msg += _('\n\nSe han seleccionado {0} facturas.').format(
                len(selected_ids))
        elif from_model == 'account.sii.register':
            msg += _('\n\nSe han seleccionado {0} registros.').format(
                len(selected_ids))

        return msg

    def action_send_invoices_sii(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        invoice_obj = self.pool.get('account.invoice')
        register_obj = self.pool.get('account.sii.register')

        wiz = self.browse(cursor, uid, ids[0])

        result = wiz.result

        # Get invoice_ids depending on context
        invoice_ids = []
        selected_ids = context.get('active_ids', [])
        from_model = context.get('from_model')
        if from_model == 'account.invoice':
            invoice_ids = selected_ids
        elif from_model == 'account.sii.register':
            selected_registers_ids = selected_ids

            invoice_ids = [
                register_data['invoice_id'][0]
                for register_data in register_obj.read(
                    cursor, uid, selected_registers_ids, ['invoice_id']
                )
            ]

        # If check_registered is checked get partner_ids to check
        partner_ids = []
        if wiz.check_registered:
            if from_model == 'account.invoice':
                object_to_read = invoice_obj
                field_to_read = 'partner_id'
            elif from_model == 'account.sii.register':
                object_to_read = register_obj
                field_to_read = 'invoice_partner_id'

            partner_ids = list(set(
                [
                    reg[field_to_read][0]
                    for reg in object_to_read.read(
                        cursor, uid, selected_ids, [field_to_read]
                    )
                ]
            ))
            # Check AEAT registered for all partners
            res_partner_obj = self.pool.get('res.partner')
            registered = res_partner_obj.check_is_aeat_registered(
                cursor, uid, partner_ids, context
            )
            result += _(
                '\n\nSe han comprobado {0} NIFs de los cuales:\n'
                '    * {1} se han marcado como censados\n'
                '    * {2} se han marcado como no censados'
            ).format(
                len(partner_ids),
                len(registered['valid']),
                len(registered['invalid'])
            )

        # Send invoices and create new registers
        new_reg_ids = []
        new_context = context.copy()
        new_context.update({'active_test': False})

        for inv_id in invoice_ids:
            lot_id = invoice_obj.send_sii_sync(cursor, uid, inv_id, context)

            new_reg_ids += register_obj.search(
                cursor, uid, [('lot_id', '=', lot_id)], context=new_context
            )

        register_vals = register_obj.read(
            cursor, uid, new_reg_ids, ['state']
        )

        facturas_a_enviar = len(invoice_ids)
        register_states = list(set([reg['state'] for reg in register_vals]))
        states_dict = dict(SII_REGISTER_STATE)
        result += _('\n\nSe han enviado {0} facturas de las cuales:').format(
            facturas_a_enviar)

        for state in register_states:
            state_occurrences = len(register_obj.search(
                cursor, uid, [('state', '=', state), ('id', 'in', new_reg_ids)],
                context=new_context
            ))
            result += '\n    * {0} facturas tienen estado "{1}"'.format(
                state_occurrences, states_dict[state]
            )

        registros_creados = len(new_reg_ids)
        facturas_sin_enviar = facturas_a_enviar - registros_creados

        if facturas_sin_enviar:
            result += _(
                '\n\n{0} facturas no se han podido enviar.'
            ).format(facturas_sin_enviar)

        wiz.write({'state': 'end', 'result': result})

    _columns = {
        'state': fields.selection(
            [('init', 'Initial'), ('end', 'End')], 'State'
        ),
        'check_registered': fields.boolean(
            string='Comprobar censado AEAT',
            help='Comprobar previamente al envío si el receptor de la '
                 'factura está censado'
        ),
        'result': fields.text('Resultado', readonly=True)
    }

    _defaults = {
        'state': lambda *a: 'init',
        'check_registered': lambda *a: False,
        'result': _get_default_message
    }
WizardSendInvoiceSII()
