# coding: utf-8

import re
from sii.server import IDService
from erppeek import Client
from tqdm import tqdm
import click
from copy import deepcopy
import csv
from difflib import SequenceMatcher
from unidecode import unidecode

CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])


def unidecode_str(s):
    if isinstance(s, bytes):
        res = unidecode(s.decode('utf-8'))
    else:
        res = unidecode(s)

    return res


@click.command(context_settings=CONTEXT_SETTINGS)
@click.option('-s', '--server',
              default='http://localhost', help=u'Dirección servidor ERP')
@click.option('-x', '--proxy',
              default=None, help=u'Dirección servidor proxy')
@click.option('-p', '--port',
              default=8069, help='Puerto servidor ERP', type=click.INT)
@click.option('-u', '--user', default='admin', help='Usuario servidor ERP')
@click.option('-w', '--password', prompt=True, confirmation_prompt=False,
              hide_input=True, help='Contraseña usuario ERP')
@click.option('-d', '--database', help='Nombre de la base de datos')
@click.option('-c', '--certificate-dir',
              default='/home/erp/conf/sii/client.crt',
              help='Directorio del certificado')
@click.option('-k', '--key-dir',
              default='/home/erp/conf/sii/client.key',
              help='Directorio de la clave del certificado')
@click.option('-C', '--clients', is_flag=True,
              help='Comprobar NIFs de clientes')
@click.option('-P', '--suppliers', is_flag=True,
              help='Comprobar NIFs de proveedores')
@click.option('-A', '--check-all', is_flag=True,
              help='Comprobar NIFs de todos los partners')
@click.option('-D', '--disable-warnings', is_flag=True,
              help='Desactivar warnings')
@click.option('-Z', '--deleg-comercial', is_flag=True,
              help=u'El archivo resultante muestra la delegación y el '
                   u'comercial del partner')
# @click.argument()
def check_partners_nifs(**kwargs):
    server = kwargs['server']  # 'http://localhost'
    port = kwargs['port']  # '8069'
    database = kwargs['database']
    user = kwargs['user']
    password = kwargs['password']
    certificate_path = kwargs['certificate_dir']
    key_path = kwargs['key_dir']
    proxy = kwargs['proxy']
    disable_warnings = kwargs['disable_warnings']
    info_deleg_comercial = kwargs['deleg_comercial']

    if disable_warnings:
        import requests
        requests.packages.urllib3.disable_warnings(
            requests.packages.urllib3.exceptions.InsecureRequestWarning
        )

    server_name = '{}:{}'.format(server, port)
    c = Client(server=server_name, db=database, user=user, password=password)
    print "Connected succsessfully"

    invoice_obj = c.model('account.invoice')
    first_date = '2017-01-01'
    last_date = '2017-07-01'
    search_params = [
        ('date_invoice', '>=', first_date),
        ('date_invoice', '<', last_date),
        ('state', 'in', ['open', 'paid'])
    ]

    if kwargs['clients'] and not kwargs['check_all']:
        search_params.append(('type', '=like', 'out_%'))
    if kwargs['suppliers'] and not kwargs['check_all']:
        search_params.append(('type', '=like', 'in_%'))

    print 'Searching invoices between {} and {} ...'.format(
        first_date, last_date)

    invoice_ids = invoice_obj.search(search_params)

    print '{} Invoices found'.format(len(invoice_ids))
    print 'Searching partners affected by the invoices...'

    partner_ids_check = [
        invoice_partner['partner_id'][0]
        for invoice_partner in invoice_obj.read(invoice_ids, ['partner_id'])
        if invoice_partner['partner_id'] and invoice_partner['partner_id'][0]
    ]
    partner_ids_check = list(set(partner_ids_check))

    total_partners = len(partner_ids_check)
    msg = '{} Partners found, do you want to check them all?'.format(
        total_partners)

    if click.confirm(msg):
        print 'Comprobando partners...'

        partner_obj = c.model('res.partner')
        partners_without_vat = {}
        partner_vats_check = {}
        polissa_obj = c.model('giscedata.polissa')
        for partner in tqdm(partner_obj.read(
                partner_ids_check, ['name', 'vat'])):
            partner_id = partner['id']
            partner_vat = partner['vat']
            partner_name = partner['name']

            if partner_vat and len(partner_vat) > 2:
                d = {
                    'vat': re.sub('^ES', '', partner_vat),
                    'name': partner_name
                }

                partner_key = (partner_id, partner_vat, partner_name)

                if info_deleg_comercial:
                    partner_key = (partner_id, partner_vat, partner_name)
                    polissa = polissa_obj.search([('titular', '=', partner_id)])
                    delegacio = ''
                    comercial = ''
                    if polissa:
                        polissa_id = polissa[0]
                        polissa_data = polissa_obj.read(
                            polissa_id, ['delegacion', 'user_id']
                        )
                        delegacio = polissa_data['delegacion'][1]
                        comercial = polissa_data['user_id'][1]

                    d.update({'delegacio': delegacio, 'comercial': comercial})
                partner_vats_check[partner_key] = d
            else:
                partners_without_vat[partner_id] = {
                    'vat': partner_vat,
                    'name': partner_name
                }

        print '{} partners no tienen NIF'.format(len(partners_without_vat))
        print 'Comprobando {} partners...'.format(len(partner_vats_check))
        service = IDService(
            certificate=certificate_path, key=key_path, url=proxy)
        # service.ids_validate(partner_vats_check, max_id_checks=1000)
        partners_result = {}
        partners_to_check = deepcopy(partner_vats_check)

        for partner_key, partner_info in tqdm(partners_to_check.items()):
            result = service.ids_validate([partner_info])[0]

            resultado = result['Resultado']
            partners_result[partner_key] = result

        invalid_partners_nif_filename = (
            '/tmp/sii_partner_nifs_result_{0}.csv'.format(database)
        )
        with open(invalid_partners_nif_filename, 'w') as f:
            w = csv.writer(f, quoting=csv.QUOTE_ALL)

            row_headers = [
                'Resultado AEAT',
                'ID interna ERP',
                'DNI/NIF ERP',
                'Nombre ERP',
                'DNI/NIF AEAT',
                'Nombre AEAT',
                'Porcentaje similitud'
            ]

            if info_deleg_comercial:
                row_headers.extend(['Delegación', 'Comercial'])

            w.writerow(row_headers)

            for (partner_id, vat, name), result in partners_result.items():
                row = [
                    result['Resultado'],
                    partner_id,
                    vat,
                    name,
                    result['Nif'],
                    result['Nombre'],
                    SequenceMatcher(
                        lambda x: x == ' ',
                        unidecode_str(name).upper(),
                        unidecode_str(result['Nombre']).upper()
                    ).ratio() * 100
                ]

                if info_deleg_comercial:
                    partner_key = (partner_id, vat, name)
                    row.extend([
                        partner_vats_check[partner_key]['delegacio'],
                        partner_vats_check[partner_key]['comercial']
                    ])

                w.writerow(row)

        partners_without_vat_filename = (
            '/tmp/sii_partners_without_nif_{0}.csv'.format(database)
        )
        with open(partners_without_vat_filename, 'w') as f:
            w = csv.writer(f, quoting=csv.QUOTE_ALL)

            w.writerow(['ID', 'DNI/NIF ERP', 'Nombre ERP'])

            for partner_id, result in partners_without_vat.items():
                w.writerow([partner_id, result['vat'], result['name']])

        num_partners_without_vat = len(partners_without_vat.keys())
        num_partners_result = len(partners_result.keys())

        msg = (
            'Total partners: {}\n'
            '---------------------\n'
            'Partners sin NIF: {}\n'
            'Partners comprobados: {}\n'
            'Archivo de partners con NIF incorrecto: "{}"\n'
            'Archivo de partners sin NIF: "{}"\n'.format(
                total_partners,
                num_partners_without_vat,
                num_partners_result,
                invalid_partners_nif_filename,
                partners_without_vat_filename
            )
        )

        print msg


if __name__ == '__main__':
    check_partners_nifs()
