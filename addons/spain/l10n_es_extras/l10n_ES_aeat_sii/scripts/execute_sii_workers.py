# -*- encoding: utf-8 -*-

import logging
import sys
import os
from logging.handlers import RotatingFileHandler
from subprocess import Popen, PIPE
import click
from rq.job import Job
from rq import push_connection, Queue
from redis import StrictRedis

LOGGER_NAME = 'execute_sii_workers'


def setup_log(filename, rotate_log):
    default_format = '%(asctime)s: %(levelname)s: %(message)s'
    default_date_format = '[%Y/%m/%d-%H:%M:%S]'
    formatter = logging.Formatter(fmt=default_format,
                                  datefmt=default_date_format)
    if filename:
        if rotate_log:
            handler = RotatingFileHandler(
                filename=filename, maxBytes=20 * 1024 * 1024, backupCount=9
            )
        else:
            handler = logging.FileHandler(filename=filename)
    else:
        handler = logging.StreamHandler(sys.stdout)
    handler.setFormatter(formatter)
    logger = logging.getLogger(LOGGER_NAME)
    logger.addHandler(handler)
    logger.setLevel(logging.INFO)


def execute_sii_workers(num_workers, redis_url, queue, rq_path, server_conf):
    logger = logging.getLogger(LOGGER_NAME)
    current_directory_path = os.path.dirname(os.path.abspath(__file__))
    script_path = current_directory_path + '/execute_sii_workers.sh'

    p = Popen(
        [script_path, rq_path, server_conf, num_workers, redis_url, queue],
        stdout=PIPE, stderr=PIPE
    )
    out, err = p.communicate()

    logger.info(out)
    logger.error(err)


def cancel_duplicated_sii_jobs(redis_url):
    logger = logging.getLogger(LOGGER_NAME)
    logger.info('Canceling duplicated jobs...')
    conn = StrictRedis.from_url(redis_url)
    push_connection(conn)
    q = Queue('sii')
    seen_invoice_ids = set()
    jobs_canceled = 0
    for job_id in q.get_job_ids():
        job = Job.fetch(job_id)
        job_invoice_id = job.args[5]
        if job_invoice_id in seen_invoice_ids:
            job.cancel()
            jobs_canceled += 1
            # print job.id, job_invoice_id
        else:
            seen_invoice_ids.add(job_invoice_id)
    logger.info('Jobs canceled: {}'.format(jobs_canceled))


@click.option('-c', '--config', help='Server configuration file',
              default='/home/erp/conf/server.conf', show_default=True)
@click.option('-q', '--queue', help='Name of the queue to execute the workers',
              default='sii', show_default=True)
@click.option('-n', '--num-workers', help='Number of workers',
              default='1', show_default=True)
@click.option('--rotate-log', is_flag=True, default=False, show_default=True,
              help='Indicates if logrotate configuration file is used')
@click.option('-l', '--log-file',
              help='Path of the file for the output log\n'
                   '[suggested: /home/erp/var/log/sii_workers.log]')
@click.option('-u', '--redis-url', help='URL of redis server',
              default='redis://localhost', show_default=True)
@click.option('-r', '--rq-path', help='Path of the rq command',
              default='/home/erp/bin', show_default=True)
@click.command(context_settings=dict(help_option_names=['-h', '--help']))
def sii_process(**kwargs):
    output_file = kwargs['log_file']
    redis_url = kwargs['redis_url']
    rq_path = kwargs['rq_path']
    num_workers = kwargs['num_workers']
    config = kwargs['config']
    queue = kwargs['queue']
    rotate_log = kwargs['rotate_log']

    setup_log(output_file, rotate_log)

    cancel_duplicated_sii_jobs(redis_url)
    execute_sii_workers(num_workers, redis_url, queue, rq_path, config)


if __name__ == '__main__':
    sii_process()
