#!/bin/bash
path=$1
openerp_server_conf=$2
num_workers=$3
redis_url=$4
queue=$5

# ERP SERVER
#PATH=${path}:/usr/local/bin:/usr/bin:/bin PYTHONPATH=/home/erp/src/erp/server/bin:/home/erp/src/erp/server/bin/addons OPENERP_SERVER=${openerp_server_conf} /home/erp/src/oorq/oorq/bin/create_burst_workers.sh ${num_workers} -u ${redis_url} -woorq.worker.Worker ${queue}

# EJECUTAR WORKERS
export PATH=${path}:/usr/local/bin:/usr/bin:/bin
export PYTHONPATH=/home/erp/src/erp/server/bin:/home/erp/src/erp/server/bin/addons
export OPENERP_SERVER=${openerp_server_conf}
echo "Creating ${num_workers} workers"
for i in `seq 1 ${num_workers}`;
do
    rq worker --burst -u ${redis_url} -woorq.worker.Worker ${queue} &
done

