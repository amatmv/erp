
# coding: utf-8

# In[1]:

from sii.server import IDService
from erppeek import Client
from progressbar import ProgressBar, ETA, Percentage, Bar
from copy import deepcopy
from zeep.exceptions import Fault
from unidecode import unidecode
import csv


# In[3]:

HOST = 'http://localhost'
PORT = '8069'
DATABASE = ''
USER = 'admin'


# In[4]:

host_name = '{}:{}'.format(HOST, PORT)
c = Client(host_name, DATABASE, USER)
print "Connected succsessfully"


# In[4]:

service = IDService(
    '/home/erp/sii_certificate.crt',  # Certificat
    '/home/erp/sii_key.key',  # Key
)


# In[5]:

polissa_obj = c.GiscedataPolissa
res_partner = c.ResPartner


# In[6]:

polissa_ids = polissa_obj.search([])

widgets = [Percentage(), ' ', Bar(), ' ', ETA()]
pbar = ProgressBar(widgets=widgets, maxval=len(polissa_ids)).start()

done = 0
polissa_by_vat = {}
check_cases = []
for polissa_vals in polissa_obj.read(polissa_ids, ['pagador', 'name', 'state']):
    if polissa_vals['pagador']:
        pagador_vals = res_partner.read(polissa_vals['pagador'][0], ['name', 'vat'])

        vat = pagador_vals['vat']

        pagador_vals.pop('id')

        if vat and vat.upper().startswith('ES'):
            vat = vat[2:].upper()
            pagador_vals['vat'] = vat

        polissa_by_vat.setdefault(vat, [])
        polissa_by_vat[vat].append(polissa_vals['name'])

        # TODO: Actually, if it's a NIE or a Passaport it doesn't seem to validate it neither
        check_cases.append(pagador_vals)
    else:
        pass
#         print polissa_vals
    done += 1
    pbar.update(done)
    
pbar.finish()


# In[51]:

to_validate = deepcopy(check_cases)

for case in to_validate:
    case['name'] = unidecode(case['name'])
invalid_cases = service.invalid_ids(to_validate, 2000)
# errors = []
# invalid_cases = []
# for case in to_validate:
#     try:
#         case['name'] = unidecode(case['name'])
#         invalid_cases.append(
#             service.ids_validate(case, 9000)
#         )
#     except Exception as e:
#         errors.append((case, e))


# In[42]:

# print len(errors)

# for err in errors:
#     print err
#     print type(unidecode(err[0]['Nombre']))


# In[45]:

total = len(check_cases)
checked = len(to_validate)
invalid = len(invalid_cases)
valid = checked - invalid


print 'Validated {0}/{1}'.format(checked, total)
print 'Of a total of {0} there are {1} correct and {2} incorrect ({3}%)'.format(checked, valid, invalid, round(invalid * 100.0 / checked, 2))
# print '=' * 113
# print invalid_cases


# In[47]:

with open('/tmp/{0}_invalid_sii.csv'.format(DATABASE), 'w') as f:
    w = csv.writer(f, quoting=csv.QUOTE_ALL)
    
    for inv_case in invalid_cases:
        w.writerow(inv_case.values() + polissa_by_vat.get(inv_case['Nif'], []))

