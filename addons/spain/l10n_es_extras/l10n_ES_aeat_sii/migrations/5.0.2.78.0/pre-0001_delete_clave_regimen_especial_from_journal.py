# coding=utf-8

import logging


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')

    logger.info('Deleting Clave Regimen Especial from account.journal')

    query = '''
        ALTER TABLE account_journal
        DROP COLUMN sii_in_clave_regimen_especial,
        DROP COLUMN sii_out_clave_regimen_especial
    '''

    cursor.execute(query)

    logger.info('Deleted Clave Regimen Especial successfully!')


def down(cursor, installed_version):
    pass


migrate = up
