# coding=utf-8

import logging


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')

    logger.info('Renaming account.sii.register "current_state" to "active"')

    query = '''
        ALTER TABLE account_sii_register
        RENAME COLUMN current_state TO active
    '''

    cursor.execute(query)

    logger.info('Updated account.sii.register successfully!')


def down(cursor, installed_version):
    pass


migrate = up
