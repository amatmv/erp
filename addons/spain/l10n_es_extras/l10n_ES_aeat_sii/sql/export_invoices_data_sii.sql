SELECT
  inv.number AS numero,
  partner.name AS contraparte,
  inv.origin AS origen,
  inv.state AS estado,
  CASE
    WHEN inv.type LIKE '%_refund' THEN -1 * ABS(inv.amount_total)
    ELSE inv.amount_total
  END AS total_factura,
  inv.date_invoice AS fecha_factura,
  inv.origin_date_invoice AS fecha_factura_original,
  pay_ord.reference AS remesa,
  fp.name AS posicion_fiscal,
  inv.sii_sent AS enviado_sii,
  inv.sii_registered AS registrado_sii,
  reg.date_sent AS fecha_registro_sii,
  inv.sii_state AS estado_sii,
  CASE
   WHEN reg.reg_correctos = 1 THEN 'Alta'
   WHEN reg.reg_correctos > 1 THEN 'Modificacion'
   WHEN inv.sii_state = 'Anulada' THEN 'Anulacion'
   ELSE 'No registrada'
  END AS estado_registro_sii
FROM account_invoice AS inv
LEFT JOIN (
  SELECT invoice_id, count(id) AS reg_correctos, max(date_sent::text) AS date_sent
  FROM account_sii_register AS r
  WHERE r.state IN ('Correcto', 'AceptadoConErrores')
  GROUP BY invoice_id
) AS reg ON (reg.invoice_id = inv.id)
LEFT JOIN res_partner AS partner ON partner.id = inv.partner_id
LEFT JOIN account_fiscal_position AS fp ON fp.id = inv.fiscal_position
LEFT JOIN payment_order AS pay_ord ON pay_ord.id = inv.payment_order_id
WHERE inv.state IN ('open', 'paid')
