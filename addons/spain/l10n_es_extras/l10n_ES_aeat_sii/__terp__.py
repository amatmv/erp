# -*- coding: utf-8 -*-
{
    "name": "Módulo para la Librería de Suministro Inmediato de Información",
    "description": """Este módulo añade las siguientes funcionalidades:
  * Añade los campos necesarios para la utilización de la librería SII
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base_extended",
        "partner_extended",
        "account",
        "base_vat",
        "oorq"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "partner_view.xml",
        "security/ir.model.access.csv",
        "account_sii_view.xml",
        "wizard/send_invoice_sii_view.xml",
        "wizard/send_invoice_sii_from_csv_view.xml",
        "wizard/is_registered_aeat_view.xml",
        "wizard/export_invoices_data_with_sii_information_view.xml",
        "account_sii_data.xml"
    ],
    "active": False,
    "installable": True
}
