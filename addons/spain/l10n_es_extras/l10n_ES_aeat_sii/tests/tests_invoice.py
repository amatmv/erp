# -*- coding: utf-8 -*-

from destral import testing
from destral.transaction import Transaction


class TestAccountInvoiceSII(testing.OOTestCase):
    def test_invoice_is_possible_to_send_sii(self):
        """Comprobar que las facturas con fecha superior a 01/01/2017 se pueden
        enviar al SII"""
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            inv_obj = self.openerp.pool.get('account.invoice')
            invoice_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'invoice_0001'
            )[1]
            inv_obj.write(
                cursor, uid, [invoice_id],
                {
                    'type': 'in_invoice',
                    'date_invoice': '2016-12-31'
                }
            )
            invoice = inv_obj.browse(cursor, uid, invoice_id)
            self.assertEqual(inv_obj.is_possible_to_send_sii(invoice), False)

            inv_obj.write(
                cursor, uid, [invoice_id], {'date_invoice': '2017-01-01'}
            )
            invoice = inv_obj.browse(cursor, uid, invoice_id)
            self.assertEqual(inv_obj.is_possible_to_send_sii(invoice), True)

    def test_journal_sii_to_send_false(self):

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            inv_obj = self.openerp.pool.get('account.invoice')
            journal_obj = self.openerp.pool.get('account.journal')
            imd_obj = self.openerp.pool.get('ir.model.data')

            view_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'account_journal_view'
            )[1]

            sequence_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'sequence_journal'
            )[1]

            partner_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'res_partner_asus'
            )[1]

            partner_address_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'res_partner_address_tang'
            )[1]

            partner_account_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'a_recv'
            )[1]

            journal_id = journal_obj.create(cursor, uid, {
                'view_id': view_id,
                'sequence_id': sequence_id,
                'type': 'general',
                'sii_to_send': False,
                'name': 'Test journal no sii'
            })

            inv_id = inv_obj.create(cursor, uid, {
                'journal_id': journal_id,
                'partner_id': partner_id,
                'partner_address_id': partner_address_id,
                'address_invoice_id': partner_address_id,
                'account_id': partner_account_id
            })

            invoice = inv_obj.browse(cursor, uid, inv_id)
            self.assertEqual(invoice.sii_to_send, False)
