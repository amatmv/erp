# -*- coding: utf-8 -*-

import unittest

from expects import expect, equal

import netsvc

from destral import testing
from destral.transaction import Transaction


class TestAccountSII(testing.OOTestCase):
    def test_duplicate_invoice_fields_sii(self):
        """ Aquest test comprova que quan es duplica una factura, alguns camps
         del SII no es dupliquin."""
        pool = self.openerp.pool
        factura_obj = pool.get('account.invoice')
        imd_obj = pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            # Importar factura a duplicar
            factura_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'invoice_0001'
            )[1]

            factura_obj.write(cursor, uid, factura_id, {
                'sii_state': 'Correcto',
                'sii_sent': True,
                'sii_registered': True
            })

            # Es duplica la factura i el retorna
            copy_factura, x = factura_obj.copy_data(cursor, uid, factura_id)

            # Dades que s'esperen una vegada duplicada la factura
            expected_values = {
                'sii_state': 'NoEnviado',
                'sii_sent': False,
                'sii_registered': False
            }
            # Selecciona els camps d'interes
            copy_factura_data = {
                i: copy_factura[i] for i in expected_values.keys()
            }
            # Comprova que els camps de la nova factura siguin els esperats
            self.assertDictEqual(copy_factura_data, expected_values)

    def test_create_BRA_invoice_without_being_sent_sii(self):
        lot_obj = self.openerp.pool.get('account.sii.lot')
        register_obj = self.openerp.pool.get('account.sii.register')
        invoice_obj = self.openerp.pool.get('account.invoice')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            invoice_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'invoice_0001'
            )[1]

            invoice_data = invoice_obj.read(cursor, uid, invoice_id, [])
            invoice_data.pop('id')

            new_invoice_vals = {}

            for k, v in invoice_data.items():
                if isinstance(v, tuple):
                    new_v = v[0]
                else:
                    new_v = v

                new_invoice_vals[k] = new_v

            new_invoice_vals['rectificative_type'] = 'BRA'
            new_invoice_id = invoice_obj.create(cursor, uid, new_invoice_vals)
            new_invoice_data = invoice_obj.read(
                cursor, uid, new_invoice_id, ['sii_to_send'])
            new_invoice_sii_to_send = new_invoice_data['sii_to_send']

            self.assertEqual(new_invoice_sii_to_send, False)

    def test_invoice_and_register_automaticly_link(self):
        lot_obj = self.openerp.pool.get('account.sii.lot')
        register_obj = self.openerp.pool.get('account.sii.register')
        invoice_obj = self.openerp.pool.get('account.invoice')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            invoice_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'invoice_0001'
            )[1]

            invoice_obj.write(cursor, uid, invoice_id, {'number': 'FP-9999'})

            invoice = invoice_obj.browse(cursor, uid, invoice_id)
            # Initially, sii_sent should be False because no register exists
            expect(invoice.sii_sent).to(equal(False))
            expect(invoice.sii_state).to(equal('NoEnviado'))

            invoice = invoice_obj.browse(cursor, uid, invoice_id)

            journal_id = invoice.journal_id.id

            lot_id = lot_obj.create(
                cursor, uid, {
                    'csv': 'XXXXXXXXX',
                    'date_sent': '2017-05-26',
                    'sii_version': '0.7',
                    'sent_state': 'Correcto',
                }
            )

            reg_id = register_obj.create(
                cursor, uid, {
                    'lot_id': lot_id,
                    'invoice_number': invoice.number,
                    'invoice_journal_id': journal_id,
                    'invoice_direction': invoice.type.split('_')[0],
                    'invoice_partner_id': invoice.partner_id.id,
                    'state': 'Incorrecto',
                    'result': '[3000] Factura duplicada',
                    'csv': 'YYYYYYYYYY',
                    'active': True
                }
            )
            invoice = invoice_obj.browse(cursor, uid, invoice_id)
            # If we create a register but it's state is Incorrecto we have sent
            # it but it's incorrect
            expect(invoice.sii_sent).to(equal(True))
            expect(invoice.sii_state).to(equal('Incorrecto'))

            register_obj.write(
                cursor, uid, reg_id, {
                    'state': 'Correcto',
                    'result': '',
                }
            )
            invoice = invoice_obj.browse(cursor, uid, invoice_id)
            # Finally, when the register state is Correcto we have sent it and
            # it's correct
            expect(invoice.sii_sent).to(equal(True))
            expect(invoice.sii_state).to(equal('Correcto'))

    @unittest.skip(reason="We don't have prepared to sent the message to SII")
    def test_opening_invoice_automaticly_sends_sii(self):
        register_obj = self.openerp.pool.get('account.sii.register')
        invoice_obj = self.openerp.pool.get('account.invoice')
        imd_obj = self.openerp.pool.get('ir.model.data')

        wf_service = netsvc.LocalService("workflow")

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            invoice_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'invoice_0001'
            )[1]

            wf_service.trg_validate(
                uid, 'account.invoice', invoice_id, 'invoice_open', cursor
            )

            invoice = invoice_obj.browse(cursor, uid, invoice_id)
            # Sending the invoice should have worked
            expect(invoice.sii_sent).to(equal(True))
            expect(invoice.sii_state).to(equal('Correcto'))

    def test_adding_new_current_state_sets_others_to_false(self):
        lot_obj = self.openerp.pool.get('account.sii.lot')
        register_obj = self.openerp.pool.get('account.sii.register')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            lot_id = lot_obj.create(
                cursor, uid, {
                    'csv': 'XXXXXXXXX',
                    'date_sent': '2017-05-26',
                    'sii_version': '0.7',
                    'sent_state': 'Correcto',
                }
            )

            reg_id_1 = register_obj.create(
                cursor, uid, {
                    'lot_id': lot_id,
                    'invoice_number': 'F00001',
                    'invoice_journal_id': 1,
                    'invoice_direction': 'in',
                    'invoice_partner_id': 1,
                    'state': 'Incorrecto',
                    'active': True
                }
            )

            reg1 = register_obj.browse(cursor, uid, reg_id_1)

            # When we create it, it should keep it's current state to True
            expect(reg1.active).to(equal(True))

            reg_id_2 = register_obj.create(
                cursor, uid, {
                    'lot_id': lot_id,
                    'invoice_number': 'F00001',
                    'invoice_journal_id': 1,
                    'invoice_direction': 'in',
                    'invoice_partner_id': 1,
                    'state': 'Incorrecto',
                    'active': False,
                }
            )

            reg1 = register_obj.browse(cursor, uid, reg_id_1)
            reg2 = register_obj.browse(cursor, uid, reg_id_2)

            # If we create one with active to False, it shouldn't change
            # the other's current state
            expect(reg1.active).to(equal(True))
            expect(reg2.active).to(equal(False))

            reg_id_3 = register_obj.create(
                cursor, uid, {
                    'lot_id': lot_id,
                    'invoice_number': 'F00001',
                    'invoice_journal_id': 1,
                    'invoice_direction': 'in',
                    'invoice_partner_id': 1,
                    'state': 'Incorrecto',
                    'active': True,
                }
            )

            reg1 = register_obj.browse(cursor, uid, reg_id_1)
            reg2 = register_obj.browse(cursor, uid, reg_id_2)
            reg3 = register_obj.browse(cursor, uid, reg_id_3)

            # When we create a new one with active to True, all previous
            # should go to False
            expect(reg1.active).to(equal(False))
            expect(reg2.active).to(equal(False))
            expect(reg3.active).to(equal(True))
