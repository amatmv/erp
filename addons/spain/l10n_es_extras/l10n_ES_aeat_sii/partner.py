# -*- coding: utf-8 -*-
from unidecode import unidecode

from osv import fields, osv
from sii.models.invoices_record \
    import CRE_FACTURAS_EMITIDAS, CRE_FACTURAS_RECIBIDAS
from sii.server import IDService
from tools import config
import re


class AccountFiscalPosition(osv.osv):
    _name = 'account.fiscal.position'
    _inherit = 'account.fiscal.position'
    _columns = {
        'sii_in_to_send': fields.boolean(
            string='Enviar Facturas Recibidas SII',
            help='Activar para que las Facturas Recibidas creadas con esta '
                 'posición fiscal se envíen al SII'
        ),
        'sii_out_to_send': fields.boolean(
            string='Enviar Facturas Emitidas SII',
            help='Activar para que las Facturas Emitidas creadas con esta '
                 'posición fiscal se envíen al SII'
        ),
        'sii_out_clave_regimen_especial': fields.selection(
            CRE_FACTURAS_EMITIDAS,
            string='Clave de Régimen Especial para Facturas Emitidas',
            help='Régimen Especial / Identificación de operaciones con '
                 'trascendencia tributaria para Facturas Emitidas'
        ),
        'sii_in_clave_regimen_especial': fields.selection(
            CRE_FACTURAS_RECIBIDAS,
            string='Clave de Régimen Especial para Facturas Recibidas',
            help='Régimen Especial / Identificación de operaciones con '
                 'trascendencia tributaria para Facturas Recibidas'
        )
    }

    _defaults = {
        'sii_in_to_send': lambda *a: True,
        'sii_out_to_send': lambda *a: True
    }


AccountFiscalPosition()


class ResPartner(osv.osv):
    _name = 'res.partner'
    _inherit = 'res.partner'

    def is_official_identification_document(self, cursor, uid, vat):
        """ Returns True if vat is a foreign official identification document"""
        if not vat:
            return False

        country_code = len(vat) >= 2 and vat[:2]
        if country_code != 'ES':
            country_obj = self.pool.get('res.country')
            country_id = country_obj.search(
                cursor, uid, [('code', '=', country_code)]
            )
            if country_id:
                return True
        return False

    def sii_get_vat_type(self, cursor, uid, partner_id, context=None):
        if context is None:
            context = {}

        if isinstance(partner_id, list):
            partner_id = partner_id[0]

        partner_vat = self.read(cursor, uid, partner_id, ['vat'])['vat']
        is_nif = self.is_dni_vat(partner_vat)
        is_nie = self.is_nie_vat(partner_vat)
        is_enterprise = self.is_enterprise_vat(partner_vat)
        if is_nif or is_nie or is_enterprise:
            return '02'
        elif self.is_passport_vat(partner_vat):
            return '03'
        elif self.is_official_identification_document(cursor, uid, partner_vat):
            return '04'
        else:
            return '02'

    def check_is_aeat_registered(self, cursor, uid, partner_ids, context=None):
        if context is None:
            context = {}

        if isinstance(partner_ids, (int, long)):
            partner_ids = [partner_ids]

        proxy = config.get('sii_proxy', None)
        if proxy:
            proxy = '{0}/nifs'.format(proxy)

        service = IDService(
            config.get('sii_certificate', None),
            config.get('sii_key', None),
            proxy,  # PROXY
        )

        pagador_vals = self.read(
            cursor, uid, partner_ids, ['name', 'vat']
        )

        invalid_ids = []
        valid_ids = []
        for pag_vals in pagador_vals:
            partner_id = pag_vals.pop('id')

            if pag_vals['vat']:
                pag_vals['vat'] = re.sub('^ES', '', pag_vals['vat'].upper())
                pag_vals['name'] = unidecode(pag_vals['name'])

                aeat_validated = service.ids_validate([pag_vals])[0]
                result = aeat_validated['Resultado']
                if result == 'IDENTIFICADO':
                    valid_ids.append(partner_id)
                elif 'NO IDENTIFICADO' in result:
                    invalid_ids.append(partner_id)
                else:
                    raise ValueError('Unexpected result: {}'.format(result))
            else:
                invalid_ids.append(partner_id)

        self.write(cursor, uid, invalid_ids, {'aeat_registered': False})
        self.write(cursor, uid, valid_ids, {'aeat_registered': True})

        return {
            'invalid': invalid_ids,
            'valid': valid_ids,
        }

    _columns = {
        'aeat_registered': fields.boolean('Censado AEAT')
    }

    _defaults = {
        'aeat_registered': lambda *a: True,
    }
ResPartner()
