# -*- coding: utf-8 -*-
import logging
from osv import fields, osv
from sii.server import SiiService
from sii.resource import SII
from sii.models.invoices_record \
    import CRE_FACTURAS_EMITIDAS, CRE_FACTURAS_RECIBIDAS
from oorq.decorators import job
from osv.osv import TransactionExecute
from tools import config
from datetime import datetime, date
from pprintpp import pformat

UNREGISTERED_NIF_CODES = [
    1116,  # El NIF no está identificado. NIF:XXXXX
    1117,  # El NIF no está identificado. NIF:XXXXX. NOMBRE_RAZON:YYYYY
    4109  # El NIF no está identificado. NIF: XXXX
]

SII_SENT_STATE = [
    ('Correcto', 'Correcto'),
    ('ParcialmenteCorrecto', 'Parcialmente Correcto'),
    ('Incorrecto', 'Incorrecto'),
    ('NoEnviado', 'No Enviado'),
]

SII_REGISTER_STATE = [
    ('Correcto', 'Correcto'),
    ('AceptadoConErrores', 'Aceptado con Errores'),
    ('Incorrecto', 'Incorrecto'),
    ('ValidacionIncorrecta', 'Validación Incorrecta'),
    ('Error', 'Error'),
    ('Anulada', 'Anulada'),
]

SII_INVOICE_STATE = SII_REGISTER_STATE + [('NoEnviado', 'No Enviado')]

SII_REGISTERED_STATE = [
    'Correcto',
    'AceptadoConErrores',
    'Anulada',
]

INVOICE_DIRECTION = [
    ('in', 'Entrada (Proveedor)'),
    ('out', 'Salida (Cliente)')
]


class AccountSIILot(osv.osv):

    _name = 'account.sii.lot'

    _rec_name = 'csv'

    def is_current_state(self, cursor, uid, new_state, inv, context=None):
        if context is None:
            context = {}

        if inv.sii_state in SII_REGISTERED_STATE:
            if new_state not in SII_REGISTERED_STATE:
                # If we have previously registered the invoice and didn't
                # register again, this is not current state
                return False

        return True

    def persist_result(self, cursor, uid, result,
                       inv, invoice_dict, context=None):
        if context is None:
            context = {}

        register_obj = self.pool.get('account.sii.register')
        res_partner = self.pool.get('res.partner')

        present_data = result['DatosPresentacion']
        date_sent = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        if present_data and present_data.get('TimestampPresentacion', None):
            date_sent = datetime.strptime(
                present_data['TimestampPresentacion'], '%d-%m-%Y %H:%M:%S'
            ).strftime('%Y-%m-%d %H:%M:%S')
        header = result['Cabecera']

        lot_id = self.create(
            cursor, uid, {
                'csv': result['CSV'],
                'date_sent': date_sent,
                'sii_version': header['IDVersionSii'],
                'sent_state': result['EstadoEnvio'],
            }
        )

        for res in result['RespuestaLinea']:
            codigo_error = res.get('CodigoErrorRegistro', False)
            descripcion_error = res.get('DescripcionErrorRegistro', False)
            result_text = ''
            if codigo_error:
                result_text += '[{}] '.format(codigo_error)
            if descripcion_error:
                result_text += descripcion_error

            inv_direction = inv.type.split('_')[0]

            if (inv_direction == 'out'
                    and codigo_error in UNREGISTERED_NIF_CODES):
                res_partner.write(
                    cursor, uid, inv.partner_id.id, {'aeat_registered': False}
                )

            estado_registro = res.get('EstadoRegistro', False)

            if inv_direction == 'in':
                inv_number = inv.origin
            else:
                inv_number = inv.number

            register_obj.create(
                cursor, uid, {
                    'lot_id': lot_id,
                    'invoice_number': inv_number,
                    'invoice_journal_id': inv.journal_id.id,
                    'invoice_direction': inv_direction,
                    'invoice_partner_id': inv.partner_id.id,
                    'state': estado_registro,
                    'result': result_text,
                    'csv': res.get('CSV', False),
                    'active': self.is_current_state(
                        cursor, uid, estado_registro, inv
                    ),
                    'object_sent': invoice_dict
                }
            )

        return lot_id

    def persist_exception(self, cursor, uid, exception,
                          inv, invoice_dict, context=None):
        if context is None:
            context = {}

        register_obj = self.pool.get('account.sii.register')

        lot_id = self.create(
            cursor, uid, {
                'csv': False,
                'date_sent': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                'sii_version': False,
                'sent_state': False,
            }
        )

        inv_direction = inv.type.split('_')[0]

        if inv_direction == 'in':
            inv_number = inv.origin
        else:
            inv_number = inv.number

        register_obj.create(
            cursor, uid, {
                'lot_id': lot_id,
                'invoice_number': inv_number,
                'invoice_journal_id': inv.journal_id.id,
                'invoice_direction': inv_direction,
                'invoice_partner_id': inv.partner_id.id,
                'state': 'Error',
                'result': str(exception),
                'csv': False,
                'active': self.is_current_state(
                    cursor, uid, 'Error', inv
                ),
                'object_sent': invoice_dict
            }
        )

        return lot_id

    def persist_not_validated(self, cursor, uid, error,
                              inv, invoice_dict, context=None):
        if context is None:
            context = {}

        register_obj = self.pool.get('account.sii.register')

        lot_id = self.create(
            cursor, uid, {
                'csv': False,
                'date_sent': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                'sii_version': False,
                'sent_state': False,
            }
        )

        inv_direction = inv.type.split('_')[0]

        if inv_direction == 'in':
            inv_number = inv.origin
        else:
            inv_number = inv.number

        register_obj.create(
            cursor, uid, {
                'lot_id': lot_id,
                'invoice_number': inv_number,
                'invoice_journal_id': inv.journal_id.id,
                'invoice_direction': inv_direction,
                'invoice_partner_id': inv.partner_id.id,
                'state': 'ValidacionIncorrecta',
                'result': str(error),
                'csv': False,
                'active': self.is_current_state(
                    cursor, uid, 'ValidacionIncorrecta', inv
                ),
                'object_sent': invoice_dict
            }
        )

        return lot_id

    _columns = {
        'csv': fields.char('CSV', size=20, readonly=True),
        'date_sent': fields.datetime('Fecha de envio', readonly=True),
        'sii_version': fields.char('Version SII', size=5, readonly=True),
        'sent_state': fields.selection(
            SII_SENT_STATE, 'Estado envio', readonly=True
        ),
    }
AccountSIILot()


class AccountSIIRegister(osv.osv):

    _name = 'account.sii.register'

    _rec_name = 'invoice_number'

    STORE_INVOICE = {
        'account.sii.register': (
            lambda self, cr, uid, ids, c=None: ids,
            [
                'invoice_number', 'invoice_journal_id', 
                'invoice_direction', 'invoice_partner'
            ],
            20
        )
    }

    def create(self, cursor, uid, vals, context=None):
        if context is None:
            context = {}

        if vals.get('active', False):
            # If we are creating a register with a current state on True, that
            # means that all previous states are no longer the current state
            old_register_ids = self.search(
                cursor, uid, [
                    ('invoice_number', '=', vals['invoice_number']),
                    ('invoice_journal_id', '=', vals['invoice_journal_id']),
                    ('invoice_direction', '=', vals['invoice_direction']),
                    ('invoice_partner_id', '=', vals['invoice_partner_id']),
                    ('active', '=', True)
                ]
            )
            self.write(
                cursor, uid, old_register_ids, {'active': False}
            )

        res = super(AccountSIIRegister, self).create(cursor, uid, vals, context)

        # We write state to proc the trigger from account_invoice, which is not
        # activated otherwise
        if 'state' in vals:
            self.write(cursor, uid, res, {'state': vals['state']}, context)

        return res

    def _get_related_invoices(self, cursor, uid, reg_id, context=None):
        if context is None:
            context = {}

        invoice_obj = self.pool.get('account.invoice')

        read_params = [
            'invoice_number', 'invoice_journal_id',
            'invoice_direction', 'invoice_partner_id'
        ]
        register_vals = self.read(cursor, uid, reg_id, read_params, context)

        search_params = [
            ('journal_id', '=', register_vals['invoice_journal_id'][0]),
            ('type', 'like', register_vals['invoice_direction']),
            ('partner_id', '=', register_vals['invoice_partner_id'][0])
        ]

        if register_vals['invoice_direction'] == 'in':
            search_params.append(
                ('origin', '=', register_vals['invoice_number'])
            )
        else:
            search_params.append(
                ('number', '=', register_vals['invoice_number'])
            )

        invoice_ids = invoice_obj.search(
            cursor, uid, search_params
        )

        return invoice_ids

    def _get_invoice(self, cursor, uid, ids, field_name, arg, context=None):
        if context is None:
            context = {}

        res = {}
        for register_id in ids:
            invoice_ids = self._get_related_invoices(
                cursor, uid, register_id, context
            )

            if len(invoice_ids) == 1:
                res[register_id] = invoice_ids[0]
            else:
                res[register_id] = False
        return res

    _columns = {
        'lot_id': fields.many2one(
            'account.sii.lot', 'Lote subida', required=True, readonly=True
        ),
        'invoice_id': fields.function(
            _get_invoice, obj='account.invoice', method=True, type='many2one',
            string='Factura', store=STORE_INVOICE, readonly=True
        ),
        'invoice_number': fields.char(
            'Numero factura', size=32, required=True, readonly=True
        ),
        'invoice_journal_id': fields.many2one(
            'account.journal', 'Diario de la factura', required=True,
            readonly=True
        ),
        'invoice_direction': fields.selection(
            INVOICE_DIRECTION, u'Dirección Factura', required=True,
            readonly=True
        ),
        'invoice_partner_id': fields.many2one(
            'res.partner', 'Emisor Factura', required=True, readonly=True
        ),
        'state': fields.selection(
            SII_REGISTER_STATE, 'Estado del Registro', select=1, readonly=True
        ),
        'result': fields.text('Resultado', readonly=True),
        'object_sent': fields.text('Factura enviada', readonly=True),
        'csv': fields.char('CSV', size=20, readonly=True),
        'active': fields.boolean(
            string='Activo', readonly=True, select=1,
            help='Un registro es activo si es el último registro '),
        'date_sent': fields.related('lot_id', 'date_sent', type='datetime',
                                    string='Fecha de envio', readonly=True,
                                    store=True),
        'date_invoice': fields.related('invoice_id', 'date_invoice',
                                       type='date',
                                       string='Fecha de factura',
                                       readonly=True),
        'invoice_amount': fields.related('invoice_id', 'amount_total',
                                         type='float',
                                         string='Importe factura',
                                         readonly=True),
        'nif': fields.related('invoice_partner_id', 'vat',
                              type='char',
                              string='NIF',
                              readonly=True),
    }

    _order = 'date_sent desc'

    _defaults = {
        'active': lambda *a: True
    }

AccountSIIRegister()


class AccountInvoice(osv.osv):

    _name = 'account.invoice'
    _inherit = 'account.invoice'

    def _trg_from_register(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        reg_vals = self.read(cursor, uid, ids, ['invoice_id'])

        return [val['invoice_id'][0] for val in reg_vals if val['invoice_id']]

    STORE_SII = {
        'account.sii.register': (
            _trg_from_register, ['state', 'invoice_id'], 30
        )
    }

    STARTING_SII_DATE = '2017-01-01'

    def is_possible_to_send_sii(self, invoice):
        # Invoices with date before STARTING_SII_DATE should not be possible to
        # be sent to SII
        return invoice.date_invoice >= self.STARTING_SII_DATE

    def send_sii_sync(self, cursor, uid, inv_id, context=None):
        if context is None:
            context = {}

        lot_obj = TransactionExecute(cursor.dbname, uid, 'account.sii.lot')

        logger = logging.getLogger(
            'openerp.{}.process_file'.format(__name__)
        )

        inv = self.browse(cursor, uid, inv_id)

        res = None
        conf_obj = self.pool.get('res.config')
        sii_send_invoices = bool(
            int(conf_obj.get(cursor, uid, 'sii_send_invoices', True))
        )

        if not self.is_possible_to_send_sii(inv):
            return res
        if inv.sii_to_send and sii_send_invoices:
            test_mode = bool(int(config.get('sii_test_mode', '1')))

            if test_mode:
                logger.warning('You are working on test mode')
            else:
                logger.warning('You are working on production mode')

            correct_inv_state = inv.state in ('open', 'paid')

            if correct_inv_state:
                invoice = self.browse(cursor, uid, inv_id, context)
                validation = SII(invoice).validate_invoice()
                invoice_dict = pformat(validation['object_validated'])
                try:
                    if validation['successful']:
                        proxy = config.get('sii_proxy', None)
                        if proxy and test_mode:
                            proxy = '{}/test'.format(proxy)
                        service_sii = SiiService(
                            config.get('sii_certificate', None),
                            config.get('sii_key', None),
                            proxy,  # PROXY
                            test_mode
                        )
                        result = service_sii.send(inv)

                        res = lot_obj.persist_result(
                            result, inv, invoice_dict, context
                        )
                    else:
                        errors_list = '\n'.join(
                            [
                                u'* {}'.format(err)
                                for err in validation['errors']
                            ]
                        )
                        res = lot_obj.persist_not_validated(
                            errors_list, inv, invoice_dict, context
                        )
                except Exception as e:
                    sentry = self.pool.get('sentry.setup')
                    if sentry:
                        sentry.client.captureException()

                    res = lot_obj.persist_exception(
                        e, inv, invoice_dict, context
                    )

                    # We raise e to move to failed
                    raise e

        return res

    @job(queue='sii', timeout=300)
    def send_sii(self, cursor, uid, inv_id, context=None):
        if context is None:
            context = {}

        logger = logging.getLogger(
            'openerp.{}.process_file'.format(__name__)
        )
        self.send_sii_sync(cursor, uid, inv_id, context)
        logger.info('Processed invoice id {0}'.format(inv_id))

    def queue_invoices_pending_to_send(self, cursor, uid, context=None):
        if context is None:
            context = {}

        today = date.today().strftime('%Y-%m-%d')
        search_params = [
            ('state', 'in', ['open', 'paid']),
            ('date_invoice', '>=', self.STARTING_SII_DATE),
            ('rectificative_type', '!=', 'BRA'),
            ('sii_to_send', '=', True),
            ('date_invoice', '<=', today),
            ('sii_registered', '=', False)
        ]
        sql = self.q(cursor, uid).select(['id']).where(search_params)
        # cursor.execute(*sql)
        # res = cursor.fetchall()

        inv_ids = self.search(cursor, uid, search_params)

        for inv_id in inv_ids:
            self.send_sii(cursor, uid, inv_id, context=context)
        return inv_ids

    def action_move_create(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        res = super(AccountInvoice, self).action_move_create(
            cursor, uid, ids, context
        )

        for inv_id in ids:
            self.send_sii(cursor, uid, inv_id, context)

        return res

    def copy_data(self, cr, uid, id, default=None, context=None):
        if default is None:
            default = {}
        res, x = super(AccountInvoice, self).copy_data(
            cr, uid, id, default, context
        )
        res.update({
            'sii_sent': False,
            'sii_state': 'NoEnviado',
            'sii_registered': False
        })
        return res, x

    def _get_sii_sent(self, cursor, uid, ids, field_name, arg, context=None):
        if context is None:
            context = {}

        register_obj = self.pool.get('account.sii.register')

        res = {}
        fields_to_read = [
            'number', 'origin', 'type', 'journal_id', 'partner_id'
        ]
        invoices_data = self.read(cursor, uid, ids, fields_to_read)

        for invoice in invoices_data:
            invoice_id = invoice['id']

            if invoice['type'] in ('in_invoice', 'in_refund'):
                search_number = invoice['origin']
            else:
                search_number = invoice['number']

            search_params = [
                ('invoice_number', '=', search_number),
                ('invoice_journal_id', '=', invoice['journal_id'][0]),
                ('invoice_partner_id', '=', invoice['partner_id'][0]),
                ('active', '=', True)
            ]
            register_ids = register_obj.search(cursor, uid, search_params)

            res.setdefault(invoice_id, {})
            res[invoice_id]['sii_sent'] = bool(register_ids)
            if len(register_ids) == 1:
                sii_state = register_obj.read(
                    cursor, uid, register_ids[0], ['state']
                )['state']
                sii_registered = sii_state in ('Correcto', 'AceptadoConErrores')

                res[invoice_id]['sii_registered'] = sii_registered
                res[invoice_id]['sii_state'] = sii_state
            else:
                res[invoice_id]['sii_registered'] = False
                res[invoice_id]['sii_state'] = False

        return res

    def create(self, cursor, uid, vals, context=None):
        if context is None:
            context = {}

        res = super(AccountInvoice, self).create(
            cursor, uid, vals, context=context
        )

        invoice = self.browse(cursor, uid, res, context=context)
        write_values = {}

        fp = invoice.fiscal_position
        if invoice.type.startswith('in_'):
            if fp:
                cre_value = fp.sii_in_clave_regimen_especial or '01'
                sii_to_send = fp.sii_in_to_send
            else:
                cre_value = '01'
                sii_to_send = True
            write_values.update({
                'sii_in_clave_regimen_especial': cre_value,
                'sii_to_send': sii_to_send and invoice.journal_id.sii_to_send
            })
        elif invoice.type.startswith('out_'):
            if fp:
                cre_value = fp.sii_out_clave_regimen_especial or '01'
                sii_to_send = fp.sii_out_to_send
            else:
                cre_value = '01'
                sii_to_send = True
            write_values.update({
                'sii_out_clave_regimen_especial': cre_value,
                'sii_to_send': sii_to_send and invoice.journal_id.sii_to_send
            })

        if invoice.rectificative_type == 'BRA':
            write_values.update({'sii_to_send': False})

        sii_description = (
            invoice.journal_id.sii_description or invoice.journal_id.name
        )
        write_values.update({'sii_description': sii_description})

        invoice.write(write_values, context)

        return res

    _columns = {
        'sii_to_send': fields.boolean(
            string='Para enviar al SII',
            help='Si está activado se enviará al SII'
        ),
        'sii_sent': fields.function(
            _get_sii_sent, type='boolean', string='Enviado SII', method=True,
            multi='sii', store=STORE_SII, select=1, readonly=True,
            help='Estará activado cuando se haya enviado la factura al SII, '
                 'pero no necesariamente esté registrada'
        ),
        'sii_registered': fields.function(
            _get_sii_sent, type='boolean', string='Registrado SII', method=True,
            multi='sii', store=STORE_SII, select=1, readonly=True,
            help='Estará activado cuando se haya registrado la factura al '
                 'SII (Aceptada o AceptadaConErrores)'
        ),
        'sii_state': fields.function(
            _get_sii_sent, type='selection', selection=SII_INVOICE_STATE,
            string='Estado SII', method=True, multi='sii', store=STORE_SII,
            select=1, readonly=True,
            help='Último estado registrado en la AEAT de la factura en caso de '
                 'estar registrada, o último estado erróneo en caso de no '
                 'estarlo'
        ),
        'sii_description': fields.char(
            string='Descripción de la operación', size=500
        ),
        'sii_out_clave_regimen_especial': fields.selection(
            CRE_FACTURAS_EMITIDAS,
            string='Clave de Régimen Especial para Facturas Emitidas',
            help='Régimen Especial / Identificación de operaciones con '
                 'trascendencia tributaria para Facturas Emitidas'
        ),
        'sii_in_clave_regimen_especial': fields.selection(
            CRE_FACTURAS_RECIBIDAS,
            string='Clave de Régimen Especial para Facturas Recibidas',
            help='Régimen Especial / Identificación de operaciones con '
                 'trascendencia tributaria para Facturas Recibidas'
        )
    }

    _defaults = {
        'sii_state': lambda *a: 'NoEnviado',
        'sii_to_send': lambda *a: True
    }
AccountInvoice()


class AccountJournal(osv.osv):
    _name = 'account.journal'
    _inherit = 'account.journal'

    _columns = {
        'sii_description': fields.char(
            string='Descripción de la operación', size=500
        ),
        'sii_to_send': fields.boolean(
            'Envío al SII'
        )
    }

    _defaults = {
        'sii_to_send': lambda *a: True
    }
AccountJournal()
