import base64
import zipfile
from StringIO import StringIO
from datetime import datetime

from destral import testing
from destral.transaction import Transaction
from destral.patch import PatchNewCursors
from retrofix.tests.test import read_flat

from osv import osv, fields



class SuffixTest(osv.osv):
    _name = 'suffix.test'
    _columns = {
        'name': fields.char('Name', size=64),
        'amount': fields.float('Amount'),
        'payed': fields.boolean('Payed'),
        'payed_date': fields.date('Payed date')
    }

    def _pay_from_n57(self, cursor, uid, ids, amount, journal=None,
                      context=None):
        if context is None:
            context = {}
        for res in self.browse(cursor, uid, ids, context=context):
            if res.amount == amount:
                res.write({
                    'payed': True,
                    'payed_date': context['date_p']
                })
        return {'state': 'confirmed'}


class TestProcessNorma57(testing.OOTestCase):
    def test_process_norma57_pay_from_file(self):
        c57 = read_flat('c57.txt')
        n57_obj = self.openerp.pool.get('norma57.file')
        n57_line = self.openerp.pool.get('norma57.file.line')
        n57_suffix_obj = self.openerp.pool.get('norma57.suffix')
        model_obj = self.openerp.pool.get('ir.model')
        cfg_obj = self.openerp.pool.get('res.config')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            with PatchNewCursors() as patch:
                cfg_obj.set(cursor, uid, 'n57_payment_date', 'from_file')

                SuffixTest()
                osv.class_pool['suffix.test'].createInstance(
                    self.openerp.pool, 'l10n_ES_cobros_ventanilla', cursor
                )
                suffix_obj = self.openerp.pool.get('suffix.test')
                suffix_obj._auto_init(cursor)

                model_id = model_obj.search(cursor, uid, [
                    ('model', '=', 'suffix.test')
                ])[0]
                n57_suffix_obj.create(cursor, uid, {
                    'name': '501',
                    'model': model_id
                })

                cursor.execute("""
                  INSERT INTO suffix_test (id, name, amount, payed) VALUES
                    (1021096, 'test_102109676', 23.39, False)
                """)

                filename = 'test_C57.txt'
                n57_id = n57_obj.process(cursor, uid, filename, c57)
                n57 = n57_obj.browse(cursor, uid, n57_id)
                self.assertEqual(n57.name, filename)
                self.assertEqual(len(n57.lines), 9)

                line = n57.lines[0]

                self.assertEqual(line.resource, 'suffix.test,1021096')
                self.assertEqual(line.state, 'draft')

                # Object doesn't have pay_from_n57 method, the state of the line
                # remains in draft
                line.confirm()
                line = n57_line.browse(cursor, uid, line.id)
                self.assertEqual(line.state, 'draft')

                # Now we have correctly set `pay_from_n57` method, and the state
                # of the line will be set to 'confirmed'
                suffix_obj.pay_from_n57 = suffix_obj._pay_from_n57
                line.confirm()
                line = n57_line.browse(cursor, uid, line.id)
                self.assertEqual(line.state, 'confirmed')

                # The suffix.test object must be payed too
                suffix = suffix_obj.browse(cursor, uid, 1021096)
                self.assertEqual(suffix.payed, True)
                self.assertEqual(suffix.payed_date, '2015-06-26')

    def test_process_norma57_pay_today(self):
        c57 = read_flat('c57.txt')
        n57_obj = self.openerp.pool.get('norma57.file')
        n57_line = self.openerp.pool.get('norma57.file.line')
        n57_suffix_obj = self.openerp.pool.get('norma57.suffix')
        model_obj = self.openerp.pool.get('ir.model')
        cfg_obj = self.openerp.pool.get('res.config')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            with PatchNewCursors() as patch:

                cfg_obj.set(cursor, uid, 'n57_payment_date', 'today')

                SuffixTest()
                osv.class_pool['suffix.test'].createInstance(
                    self.openerp.pool, 'l10n_ES_cobros_ventanilla', cursor
                )
                suffix_obj = self.openerp.pool.get('suffix.test')
                suffix_obj._auto_init(cursor)

                model_id = model_obj.search(cursor, uid, [
                    ('model', '=', 'suffix.test')
                ])[0]
                n57_suffix_obj.create(cursor, uid, {
                    'name': '501',
                    'model': model_id
                })

                cursor.execute("""
                  INSERT INTO suffix_test (id, name, amount, payed) VALUES
                    (1021096, 'test_102109676', 23.39, False)
                """)

                filename = 'test_C57.txt'
                n57_id = n57_obj.process(cursor, uid, filename, c57)
                n57 = n57_obj.browse(cursor, uid, n57_id)
                self.assertEqual(n57.name, filename)
                self.assertEqual(len(n57.lines), 9)

                line = n57.lines[0]

                self.assertEqual(line.resource, 'suffix.test,1021096')
                self.assertEqual(line.state, 'draft')

                # Object doesn't have pay_from_n57 method, the state of the line
                # remains in draft
                line.confirm()
                line = n57_line.browse(cursor, uid, line.id)
                self.assertEqual(line.state, 'draft')

                # Now we have correctly set `pay_from_n57` method, and the state
                # of the line will be set to 'confirmed'
                suffix_obj.pay_from_n57 = suffix_obj._pay_from_n57
                line.confirm()

                line = n57_line.browse(cursor, uid, line.id)
                self.assertEqual(line.state, 'confirmed')

                # The suffix.test object must be payed too
                suffix = suffix_obj.browse(cursor, uid, 1021096)
                self.assertEqual(suffix.payed, True)
                today = datetime.now().strftime('%Y-%m-%d')
                self.assertEqual(suffix.payed_date, today)


class TestImportWizard(testing.OOTestCase):
    def test_import_single_file(self):
        c57 = read_flat('c57.txt')
        wizard_obj = self.openerp.pool.get('wizard.import.norma57')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            wiz_id = wizard_obj.create(cursor, uid, {
                'name': 'c57.txt',
                'norma57_file': base64.b64encode(c57)
            })
            wiz = wizard_obj.browse(cursor, uid, wiz_id)
            result = wiz.import_file()
            self.assertIsInstance(result, dict)
            self.assertListEqual(
                sorted(result.keys()),
                ['domain', 'name', 'res_model', 'type', 'view_mode',
                 'view_type']
            )

    def test_import_zip_file(self):
        c57 = read_flat('c57.txt')
        f = StringIO()
        zf = zipfile.ZipFile(f, 'a', zipfile.ZIP_DEFLATED, False)
        zf.writestr('c57.txt', c57)
        zf.close()

        self.assertEqual(zipfile.is_zipfile(f), True)

        wizard_obj = self.openerp.pool.get('wizard.import.norma57')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            wiz_id = wizard_obj.create(cursor, uid, {
                'name': 'c57.txt',
                'norma57_file': base64.b64encode(f.getvalue())
            })
            wiz = wizard_obj.browse(cursor, uid, wiz_id)
            result = wiz.import_file()
            self.assertIsInstance(result, dict)
            self.assertListEqual(
                sorted(result.keys()),
                ['domain', 'name', 'res_model', 'type', 'view_mode',
                 'view_type']
            )
