# -*- coding: utf-8 -*-
{
  "name": "Cobros ventanilla",
  "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Importación de ficheros con la Norma57 (Cobros en ventanilla)
""",
  "version": "0-dev",
  "author": "GISCE",
  "category": "Master",
  "depends": ["base", "base_extended", "account_payment", "c2c_webkit_report"],
  "init_xml": [],
  "demo_xml": [],
  "update_xml": [
      "l10n_ES_cobros_ventanilla_view.xml",
      "l10n_ES_cobros_ventanilla_report.xml",
      "l10n_ES_cobros_ventanilla_data.xml",
      "wizard/wizard_import_norma57_view.xml",
      "wizard/wizard_export_n57_excel_view.xml",
      "wizard/wizard_open_n57_invoices_view.xml",
      "security/ir.model.access.csv"
  ],
  "active": False,
  "installable": True
}
