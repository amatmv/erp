# Translation of OpenERP Server.
# This file contains the translation of the following modules:
# * l10n_ES_cobros_ventanilla
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.14\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2019-07-29 14:50+0000\n"
"PO-Revision-Date: 2019-07-29 14:50+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: \n"
"Generated-By: Babel 2.7.0\n"

#. module: l10n_ES_cobros_ventanilla
#: field:wizard.import.norma57,name:0
msgid "Filename"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: code:addons/l10n_ES_cobros_ventanilla/norma57.py:326
msgid "Pago N57 desde fichero: {}"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: constraint:ir.actions.act_window:0
msgid "Invalid model name in the action definition."
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: view:wizard.export.n57.excel:0
msgid "Exportar"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.file,header_free_F2:0
msgid "Free F2"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: model:ir.actions.report.xml,name:l10n_ES_cobros_ventanilla.report_ventanilla
msgid "Cobraments en finestreta bancaria"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.file,total_lines:0
msgid "Total lines"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: model:ir.actions.act_window,name:l10n_ES_cobros_ventanilla.action_sc_n57_file_lines
msgid "File lines"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.file.line,notes:0
msgid "Notes"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.file.line,amount:0
msgid "Amount"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.file,footer_free_H:0
#: field:norma57.file,header_free_H:0
msgid "Free H"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.file,footer_free_I:0
#: field:norma57.file,header_free_I:0
msgid "Free I"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.file,header_free_J:0
msgid "Free J"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.file,footer_free_K:0
#: field:norma57.file,header_free_K:0
#: field:norma57.file.line,free_K:0
msgid "Free K"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.file,footer_free_G:0
#: field:norma57.file,header_free_G:0
msgid "Free G"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.file,footer_free_C:0
#: field:norma57.file.line,free_C:0
msgid "Free C"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.file.line,office_number:0
msgid "Office Number"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.file.line,reference:0
msgid "Reference"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: model:ir.module.module,shortdesc:l10n_ES_cobros_ventanilla.module_meta_information
msgid "Cobros ventanilla"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.file,header_presentation_date:0
msgid "Presentation Date"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: selection:norma57.file.line,state:0
msgid "Confirmed"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.file.line,bank_office:0
msgid "Bank Office"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: view:norma57.file:0
#: view:norma57.file.line:0
msgid "Confirm"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.file.line,operation_code:0
msgid "Operation Code"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:wizard.export.n57.excel,file:0
msgid "Excel"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.file,footer_negative:0
msgid "Negative"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.file.line,state view:norma57.file.line:0
msgid "State"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.file.line,debit:0
msgid "Debit"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: model:ir.ui.menu,name:l10n_ES_cobros_ventanilla.menu_norma57_file
msgid "N57 Cobros ventanilla"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.file,footer_free_D3:0
#: field:norma57.file,header_free_D3:0
msgid "Free D3"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.file,footer_free_D2:0
#: field:norma57.file,header_free_D2:0
msgid "Free D2"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.file.line,payment_channel:0
msgid "Payment Channel"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: model:ir.ui.menu,name:l10n_ES_cobros_ventanilla.menu_configuration
msgid "Configuración"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: view:wizard.import.norma57:0
msgid "N57 File import"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: constraint:ir.ui.view:0
msgid "Invalid XML for View Architecture!"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.file,footer_free_J2:0
msgid "Free J2"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: model:ir.model,name:l10n_ES_cobros_ventanilla.model_norma57_suffix
msgid "norma57.suffix"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: model:ir.actions.act_window,name:l10n_ES_cobros_ventanilla.action_wizard_export_n57_excel_form
msgid "Exportar N57 a Excel"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: model:ir.model,name:l10n_ES_cobros_ventanilla.model_wizard_export_n57_excel
msgid "wizard.export.n57.excel"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.file,footer_free_E2:0
#: field:norma57.file,header_free_E2:0
msgid "Free E2"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: view:wizard.export.n57.excel:0
msgid "N57 Excel file export"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.suffix,name:0
msgid "Suffix"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: model:ir.model,name:l10n_ES_cobros_ventanilla.model_norma57_file
msgid "norma57.file"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.file.line,bank_account_dc:0
msgid "Bank Account Dc"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.file,header_free_C:0
msgid "Libre C"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: model:ir.model,name:l10n_ES_cobros_ventanilla.model_norma57_file_line
msgid "norma57.file.line"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: selection:norma57.file.line,state:0
msgid "Draft"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: view:wizard.import.norma57:0
msgid "Import"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: model:ir.actions.act_window,name:l10n_ES_cobros_ventanilla.action_wizard_import_norma57_form
#: model:ir.ui.menu,name:l10n_ES_cobros_ventanilla.menu_wizard_import_norma57_form
msgid "Import N57 File"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: model:ir.actions.act_window,name:l10n_ES_cobros_ventanilla.action_norma57_suffix_tree
#: view:norma57.suffix:0
msgid "Norma57 Suffix"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.file.line,file_id:0
msgid "unknown"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.file,footer_total_amount:0
msgid "Total Amount"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: view:wizard.export.n57.excel:0
msgid "Cancel·lar"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.file,footer_number_of_records:0
msgid "Number Of Records"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.file.line,date:0
msgid "Date"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: view:norma57.file:0
msgid "Footer"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.file,create_date:0
msgid "Creation date"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: code:addons/l10n_ES_cobros_ventanilla/wizard/wizard_import_norma57.py:40
msgid "N57 Imported files"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: selection:norma57.file.line,state:0
msgid "Error"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.file.line,cancel_code:0
msgid "Cancel Code"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:wizard.export.n57.excel,filename:0
msgid "Nom del fitxer"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: model:ir.model,name:l10n_ES_cobros_ventanilla.model_wizard_import_norma57
msgid "wizard.import.norma57"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.file.line,bank_code:0
msgid "Bank Code"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: view:norma57.file:0
#: view:norma57.file.line:0
#: view:norma57.suffix:0
#: view:wizard.import.norma57:0
msgid "General"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.file.line,sufix:0
msgid "Sufix"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: view:norma57.file:0
msgid "Header"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:wizard.import.norma57,norma57_file:0
msgid "N57 File"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.file.line,record_code:0
msgid "Record Code"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.file,progress:0
msgid "File Progress"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: constraint:ir.model:0
msgid "The Object name must start with x_ and not contain any special character !"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: selection:norma57.file.line,state:0
msgid "To review"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: model:ir.actions.act_window,name:l10n_ES_cobros_ventanilla.action_norma57_file_tree
#: view:norma57.file:0
msgid "Norma 57"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.file,journal_id:0
msgid "Journal"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: model:ir.module.module,description:l10n_ES_cobros_ventanilla.module_meta_information
msgid ""
"Aquest mòdul afegeix les següents funcionalitats:\n"
"  * Importación de ficheros con la Norma57 (Cobros en ventanilla)\n"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.file,name:0
msgid "File name"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.file.line,resource:0
msgid "Resource"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.file.line,bank_account:0
msgid "Bank Account"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.file,lines:0
msgid "Lines"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.suffix,model:0
msgid "Model"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: view:norma57.file.line:0
msgid "Norma 57 line"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.file,footer_sender_number:0
#: field:norma57.file,header_sender_number:0
#: field:norma57.file.line,sender_number:0
msgid "Sender Number"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: model:ir.ui.menu,name:l10n_ES_cobros_ventanilla.menu_norma57_suffix_tree
msgid "Norma57 Suffixes"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.file.line,sequence:0
msgid "Sequence"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.file,header_entity_number:0
#: field:norma57.file.line,entity_number:0
msgid "Entity Number"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: view:norma57.file:0
msgid "Actions"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: field:norma57.file.line,payment_identification:0
msgid "Payment Identification"
msgstr ""

#. module: l10n_ES_cobros_ventanilla
#: model:ir.ui.menu,name:l10n_ES_cobros_ventanilla.menu_main
msgid "Cobros por ventanilla"
msgstr ""

#: rml:l10n_ES_cobros_ventanilla/report/cobros_ventanilla.mako:35
msgid "Cobros Ventanilla Bancaria N57"
msgstr ""

#: rml:l10n_ES_cobros_ventanilla/report/cobros_ventanilla.mako:42
msgid "Entidad"
msgstr ""

#: rml:l10n_ES_cobros_ventanilla/report/cobros_ventanilla.mako:54
msgid "Fichero Procesado"
msgstr ""

#: rml:l10n_ES_cobros_ventanilla/report/cobros_ventanilla.mako:64
msgid "Cobros"
msgstr ""

#: rml:l10n_ES_cobros_ventanilla/report/cobros_ventanilla.mako:70
#: rml:l10n_ES_cobros_ventanilla/report/cobros_ventanilla.mako:119
msgid "Importe"
msgstr ""

#: rml:l10n_ES_cobros_ventanilla/report/cobros_ventanilla.mako:76
#: rml:l10n_ES_cobros_ventanilla/report/cobros_ventanilla.mako:116
msgid "Fecha"
msgstr ""

#: rml:l10n_ES_cobros_ventanilla/report/cobros_ventanilla.mako:113
msgid "Nº Factura"
msgstr ""

#: rml:l10n_ES_cobros_ventanilla/report/cobros_ventanilla.mako:122
msgid "F. Cobro"
msgstr ""

#: rml:l10n_ES_cobros_ventanilla/report/cobros_ventanilla.mako:125
msgid "Cliente"
msgstr ""

#: rml:l10n_ES_cobros_ventanilla/report/cobros_ventanilla.mako:128
msgid "Nombre"
msgstr ""

#: rml:l10n_ES_cobros_ventanilla/report/cobros_ventanilla.mako:131
msgid "Sufijo"
msgstr ""

