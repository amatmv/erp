import logging
from datetime import datetime

from osv import osv, fields
from osv.osv import TransactionExecute
from tools import config
from retrofix import c57
from tools.translate import _


class Norma57Suffix(osv.osv):
    """Link a N57 suffix with a OpenERP model.
    """
    _name = 'norma57.suffix'
    _columns = {
        'name': fields.char('Suffix', size=3, required=True),
        'model': fields.many2one('ir.model', 'Model', required=True),
    }

    _order = 'name asc'

Norma57Suffix()


class Norma57File(osv.osv):
    _name = 'norma57.file'

    _order = 'id desc, create_date'

    def _total_lines(self, cursor, uid, ids, field_name, arg, context=None):
        res = dict.fromkeys(ids, False)
        if not context:
            context = {}
        for n57_file in self.read(cursor, uid, ids, ['lines']):
            res[n57_file['id']] = len(n57_file['lines'])

        return res

    def _progress(self, cursor, uid, ids, field_name, arg, context=None):
        res = dict.fromkeys(ids, False)
        if not context:
            context = {}
        n57_line_obj = self.pool.get('norma57.file.line')
        suffix_obj = self.pool.get('norma57.suffix')
        suffix_ids = suffix_obj.search(cursor, uid, [])
        suffixs = suffix_obj.read(cursor, uid, suffix_ids, ['name'])
        suffixs = [x['name'] for x in suffixs]
        for n57_file in ids:
            search_params = [
                ('file_id', '=', n57_file),
                ('sufix', 'in', suffixs)
            ]
            lines_detected = n57_line_obj.search_count(
                cursor, uid, search_params)
            if lines_detected:
                search_params.append(('state', '=', 'confirmed'))
                lines_detected_proc = n57_line_obj.search_count(
                    cursor, uid, search_params)
                percent = (
                    float(lines_detected_proc) / float(lines_detected)) * 100.0

            else:
                percent = 100.0
            res[n57_file] = percent

        return res

    _columns = {
        'name': fields.char('File name', size=256),
        'journal_id': fields.many2one('account.journal', 'Journal'),
        'create_date': fields.datetime('Creation date'),
        'header_free_C': fields.char('Libre C', size=6),
        'header_sender_number': fields.char('Sender Number', size=8),
        'header_free_D2': fields.char('Free D2', size=3),
        'header_free_D3': fields.char('Free D3', size=1),
        'header_entity_number': fields.char('Entity Number', size=4),
        'header_free_E2': fields.char('Free E2', size=10),
        'header_presentation_date': fields.date('Presentation Date'),
        'header_free_F2': fields.char('Free F2', size=6),
        'header_free_G': fields.char('Free G', size=6),
        'header_free_H': fields.char('Free H', size=20),
        'header_free_I': fields.char('Free I', size=1),
        'header_free_J': fields.char('Free J', size=14),
        'header_free_K': fields.char('Free K', size=11),
        'footer_free_C': fields.char('Free C', size=6),
        'footer_sender_number': fields.char('Sender Number', size=8),
        'footer_free_D2': fields.char('Free D2', size=3),
        'footer_free_D3': fields.char('Free D3', size=1),
        'footer_number_of_records': fields.char('Number Of Records', size=6),
        'footer_free_E2': fields.char('Free E2', size=8),
        'footer_total_amount': fields.char('Total Amount', size=12),
        'footer_free_G': fields.char('Free G', size=6),
        'footer_free_H': fields.char('Free H', size=20),
        'footer_free_I': fields.char('Free I', size=1),
        'footer_negative': fields.boolean('Negative'),
        'footer_free_J2': fields.char('Free J2', size=13),
        'footer_free_K': fields.char('Free K', size=11),
        'lines': fields.one2many('norma57.file.line', 'file_id', 'Lines'),
        'total_lines': fields.function(
            _total_lines, string='Total lines', type='int', method=True
        ),
        'progress': fields.function(
            _progress, string='File Progress', type='float', method=True
        )
    }

    def confirm(self, cursor, uid, ids, context=None):
        """Confirm all the lines from this N57 file
        """
        if context is None:
            context = {}
        for n57 in self.browse(cursor, uid, ids, context=context):
            for line in n57.lines:
                line.confirm()
        return True

    def process(self, cursor, uid, filename, data, context=None):
        logger = logging.getLogger('openerp')
        if context is None:
            context = {}
        line_obj = self.pool.get('norma57.file.line')
        records = c57.read(data)
        header = records[0]
        del records[0]
        footer = records[-1]
        del records[-1]
        values = {
            'name': filename,
            'header_free_C': header.free_C,
            'header_sender_number': header.sender_number,
            'header_free_D2': header.free_D2,
            'header_free_D3': header.free_D3,
            'header_entity_number': header.entity_number,
            'header_free_E2': header.free_E2,
            'header_presentation_date': header.presentation_date.strftime('%Y-%m-%d'),
            'header_free_F2': header.free_F2,
            'header_free_G': header.free_G,
            'header_free_H': header.free_H,
            'header_free_I': header.free_I,
            'header_free_J': header.free_J,
            'header_free_K': header.free_K,
            'footer_free_C': footer.free_C,
            'footer_sender_number': footer.sender_number,
            'footer_free_D2': footer.free_D2,
            'footer_free_D3': footer.free_D3,
            'footer_number_of_records': footer.number_of_records,
            'footer_free_E2': footer.free_E2,
            'footer_total_amount': footer.total_amount,
            'footer_free_G': footer.free_G,
            'footer_free_H': footer.free_H,
            'footer_free_I': footer.free_I,
            'footer_negative': footer.negative,
            'footer_free_J2': footer.free_J2,
            'footer_free_K': footer.free_K,
        }
        norma57_id = self.create(cursor, uid, values, context=context)
        for idx, record in enumerate(records):
            logger.info('Processing {0}/{1}: {2}'.format(
                idx, len(records), record
            ))
            if record.record_code in ('02', '80'):
                # Ignore sender header and sender footer
                continue
            line_values = {
                'file_id': norma57_id,
                'sequence': idx,
                'record_code': record.record_code,
                'operation_code': record.operation_code,
                'free_C': record.free_C,
                'sender_number': record.sender_number,
                'sufix': record.sufix,
                'payment_channel': record.payment_channel,
                'entity_number': record.entity_number,
                'office_number': record.office_number,
                'date': record.date.strftime('%Y-%m-%d'),
                'amount': float(record.amount),
                'payment_identification': record.payment_identification,
                'bank_code': record.bank_code,
                'bank_office': record.bank_office,
                'bank_account_dc': record.bank_account_dc,
                'bank_account': record.bank_account,
                'debit': record.debit,
                'cancel_code': record.cancel_code,
                'reference': record.reference,
                'free_K': record.free_K,
                'resource': self.get_resource(cursor, uid, record, context)
            }

            line_obj.create(cursor, uid, line_values, context=context)
        return norma57_id

    def get_resource(self, cursor, uid, record, context=None):
        if context is None:
            context = {}
        suffix_obj = self.pool.get('norma57.suffix')
        resource = False
        suffix_id = suffix_obj.search(cursor, uid, [
            ('name', '=', record.sufix)
        ])
        if suffix_id:
            suffix = suffix_obj.browse(cursor, uid, suffix_id[0])
            model = suffix.model.model
            resource_obj = self.pool.get(model)
            resource_id = resource_obj.search(cursor, uid, [
                ('id', '=', int(record.reference[0:-2]))
            ], context={'active_test': False})
            if resource_id:
                resource_id = resource_id[0]
                resource = '{0},{1}'.format(
                    model, resource_id
                )
        return resource


Norma57File()


class Norma57FileLine(osv.osv):
    _name = 'norma57.file.line'

    def _resource_selection(self, cursor, uid, context=None):
        if context is None:
            context = {}
        suffix_obj = self.pool.get('norma57.suffix')
        suffix_ids = suffix_obj.search(cursor, uid, [])
        suffixes = suffix_obj.browse(cursor, uid, suffix_ids, context=context)
        return [(s.model.model, s.model.model) for s in suffixes]

    def get_factura(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        fact_obj = self.pool.get('giscedata.facturacio.factura')
        res = {}
        for linia in self.browse(cursor, uid, ids, context):
            data = {
                'num_factura': '',
                'nom_client': '',
                'client': ''
            }
            if linia.resource:
                model_fact, id_fact = linia.resource.split(',')
                id_fact = int(id_fact)
                if model_fact == 'account.invoice':
                    id_fact = fact_obj.search(cursor, uid, [('invoice_id', '=', id_fact)])
                    if not id_fact:
                        continue
                    id_fact = id_fact[0]
                fact = fact_obj.read(cursor, uid, id_fact, [])
                data['num_factura'] = fact['number']
                data['nom_client'] = fact['partner_id'][1]
                data['client'] = fact['polissa_id'][1]
            res[linia.id] = data
        return res

    def confirm_line(self, cursor, uid, line_id, model, model_id, line_amount,
                     journal_id, context=None):
        if context is None:
            context = {}
        obj = self.pool.get(model)
        result = obj.pay_from_n57(
            cursor, uid, [model_id], line_amount, journal_id, context=context
        )
        self.write(cursor, uid, [line_id], result)

    _columns = {
        'sequence': fields.integer('Sequence'),
        'record_code': fields.char('Record Code', size=2),
        'operation_code': fields.char('Operation Code', size=2),
        'free_C': fields.char('Free C', size=6),
        'sender_number': fields.char('Sender Number', size=8),
        'sufix': fields.char('Sufix', size=3),
        'payment_channel': fields.char('Payment Channel', size=1),
        'entity_number': fields.char('Entity Number', size=4),
        'office_number': fields.char('Office Number', size=4),
        'date': fields.date('Date'),
        'amount': fields.float(
            'Amount', digits=(16, int(config['price_accuracy']))
        ),
        'payment_identification': fields.date('Payment Identification'),
        'bank_code': fields.char('Bank Code', size=4),
        'bank_office': fields.char('Bank Office', size=4),
        'bank_account_dc': fields.char('Bank Account Dc', size=2),
        'bank_account': fields.char('Bank Account', size=10),
        'debit': fields.char('Debit', size=1),
        'cancel_code': fields.char('Cancel Code', size=1),
        'reference': fields.char('Reference', size=13),
        'free_K': fields.char('Free K', size=11),
        'file_id': fields.many2one('norma57.file', ondelete='cascade'),
        'resource': fields.reference('Resource', _resource_selection, size=250),
        'state': fields.selection([
                ('draft', 'Draft'),
                ('error', 'Error'),
                ('review', 'To review'),
                ('confirmed', 'Confirmed')
            ],'State'
        ),
        'notes': fields.text('Notes')
    }

    _defaults = {
        'state': lambda *a: 'draft'
    }

    def confirm(self, cursor, uid, ids, context=None):
        """Confirm this line

        To perform this action the line must be linked with a resource and
        the resource must have a method with the following signature::

            def pay_from_n57(self, cursor, uid, ids, amount, context=None):
                # Do some stuff to pay from N57 barcode
                return True
        """
        if context is None:
            context = {}
        cfg_obj = self.pool.get('res.config')
        payment_date = cfg_obj.get(
            cursor, uid, 'n57_payment_date', 'today'
        )
        logger = logging.getLogger('openerp')
        line_obj_trans = TransactionExecute(
            cursor.dbname, uid, 'norma57.file.line')
        for line in self.browse(cursor, uid, ids, context=context):
            if line.resource and line.state != 'confirmed':
                model, model_id = line.resource.split(',')
                model_id = int(model_id)
                obj = self.pool.get(model)
                if not hasattr(obj, 'pay_from_n57'):
                    logger.warning(
                        'Object: {0} has not method `pay_from_n57`'.format(
                            model
                        )
                    )
                    continue
                journal_id = None
                if line.file_id.journal_id:
                    journal_id = line.file_id.journal_id.id
                ctx = context.copy()
                if payment_date == 'from_file':
                    ctx['date_p'] = line.date
                else:
                    ctx['date_p'] = datetime.now().strftime('%Y-%m-%d')
                ctx['norma_57_name'] = _(u'Pago N57 desde fichero: {}').format(
                        line.file_id.name
                    )
                try:
                    line_obj_trans.confirm_line(
                        line.id, model, model_id, line.amount,
                        journal_id, context=ctx
                    )
                except Exception as e:
                    logger.warning(
                        'Object: {} id:{} error: {} failed when method `pay_from_n57`'.format(
                            model, model_id, e
                        )
                    )
                    
Norma57FileLine()
