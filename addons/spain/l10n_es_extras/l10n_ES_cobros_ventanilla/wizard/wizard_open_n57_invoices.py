from osv import osv, fields
from tools.translate import _


class WizardOpenN57Invoices(osv.osv_memory):

    _name = 'wizard.open.n57.invoices'

    def action_open_invoices(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        n57_ids = context.get("active_ids")
        n57_o = self.pool.get("norma57.file")
        n57_line_o = self.pool.get("norma57.file.line")

        invoice_ids = []

        for info in n57_o.read(cursor, uid, n57_ids, ['lines']):

            for line_info in n57_line_o.read(cursor, uid, info['lines'], ['resource']):

                related_info = line_info['resource']
                if not related_info:
                    continue

                related_model, related_id = related_info.split(",")
                related_model = related_model.strip()
                related_id = int(related_id)

                if related_model != "account.invoice":
                    related_o = self.pool.get(related_model)
                    related_id = related_o.read(cursor, uid, related_id, ['invoice_id']).get('invoice_id')
                    if related_id and isinstance(related_id, (list, tuple)):
                        related_id = related_id[0]
                    if related_id:
                        invoice_ids.append(related_id)

                else:
                    invoice_ids.append(related_id)

        return {
            'name': _(u'Factures de la N57'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': "account.invoice",
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': "[('id', 'in', %s)]" % str(tuple(invoice_ids)),
        }

    _columns = {
    }


WizardOpenN57Invoices()
