import base64
import zipfile
from StringIO import StringIO

from osv import osv, fields
from tools.translate import _


class WizardImportNorma57(osv.osv_memory):
    _name = 'wizard.import.norma57'
    _columns = {
        'name': fields.char('Filename', size=256),
        'norma57_file': fields.binary('N57 File', required=True)
    }

    def import_file(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        n57_file_obj = self.pool.get('norma57.file')

        wiz = self.browse(cursor, uid, ids[0], context=context)
        norma57_file = StringIO(base64.b64decode(wiz.norma57_file))
        n57_files = {}
        if zipfile.is_zipfile(norma57_file):
            n57_zip = zipfile.ZipFile(norma57_file)
            for n57_file in n57_zip.namelist():
                n57_files[n57_file] = n57_zip.open(n57_file).read()
        else:
            n57_files[wiz.name] = norma57_file.getvalue()

        results = []
        for filename, content in n57_files.iteritems():
            n57_id = n57_file_obj.process(
                cursor, uid, filename, content, context=context
            )
            results.append(n57_id)

        return {
            'name': _('N57 Imported files'),
            'view_type': 'form',
            "view_mode": 'tree,form',
            'res_model': 'norma57.file',
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', results)]
        }

WizardImportNorma57()
