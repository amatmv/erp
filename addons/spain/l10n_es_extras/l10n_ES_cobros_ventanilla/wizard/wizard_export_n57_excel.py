from osv import osv, fields
import StringIO
import base64
from pandas import ExcelWriter
from pandas import DataFrame


class WizardExportN57Excel(osv.osv_memory):

    def action_export(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        n57_obj = self.pool.get('norma57.file')
        df = None
        filename = 'n57'
        for n57 in n57_obj.browse(cursor, uid, context.get('active_ids'), context=context):
            filename = filename + '_' + n57.name
            table = {
                'N Factura': [],
                'Fecha': [],
                'Importe': [],
                'F. Cobro': [],
                'Cliente': [],
                'Nombre': [],
                'Sufijo': [],
                'Clasificacion': []
            }
            for n57linia in n57.lines:
                factura_vals = n57linia.get_factura()[n57linia.id]
                table['N Factura'].append(factura_vals['num_factura'])
                table['Fecha'].append(n57linia.date)
                table['Importe'].append(float('{0:.2f}'.format(n57linia.amount)))
                table['F. Cobro'].append(n57linia.payment_identification)
                table['Cliente'].append(factura_vals['client'])
                table['Nombre'].append(factura_vals['nom_client'])
                table['Sufijo'].append(n57linia.sufix)
                if n57linia.state == 'draft':
                    if n57linia.resource:
                        clasificacion = 'No confirmado'
                    else:
                        clasificacion = 'No encontrado'
                else:
                    clasificacion = 'Confirmado'
                table['Clasificacion'].append(clasificacion)
            df = DataFrame(table)
        file_buffer = StringIO.StringIO()
        writer = ExcelWriter(file_buffer)
        df.to_excel(writer, 'Hoja 1', index=False)
        writer.save()
        excel = file_buffer.getvalue()
        file_buffer.close()
        params = {
            'filename': filename + '.xlsx',
            'file': base64.b64encode(excel)
        }
        self.write(cursor, uid, ids, params, context=context)

    _name = 'wizard.export.n57.excel'
    _columns = {
        'file':  fields.binary('Excel'),
        'filename': fields.char('Nom del fitxer', size=256)
    }


WizardExportN57Excel()
