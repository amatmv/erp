<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<head>
		<style type="text/css">
	        ${css}
	    </style>
        <link rel="stylesheet" href="${addons_path}/l10n_ES_cobros_ventanilla/report/cobros_ventanilla.css"/>
	</head>
	<body>
    <%
        pool = objects[0].pool
        bank_obj = pool.get('res.bank')
        linies_obj = pool.get('norma57.file.line')

        def bank_name(cursor, uid, code):   
            bank_id = bank_obj.search(cursor, uid, [('code', '=', code)])         
            name = bank_obj.read(cursor, uid, bank_id, ['lname'])[0]['lname']
            return name

        def bank_city(cursor, uid, code):
            bank_id = bank_obj.search(cursor, uid, [('code', '=', code)])
            city = bank_obj.read(cursor, uid, bank_id, ['city'])[0]['city']
            return city

        def num_linies(cursor, uid, id_norma):
            ids = linies_obj.search(cursor, uid, [('file_id', '=', id_norma)])
            return len(ids)
    %>
    %for Norma57File in objects:
        <div id="capcalera">
            ${company.name}
        </div>
        <div id="titol">
            ${_(u"Cobros Ventanilla Bancaria N57")}
        </div>
        <br>
        <br>
        <table>
            <tr>
                <td class="label w50" >
                    ${_(u"Entidad")}
                </td>
                <td class="w200" >
                    ${bank_name(cursor, uid, Norma57File.header_entity_number)}
                </td>
                <td class="label w50" >
                </td>
            </tr>
        </table>
        <table id="tableb">
            <tr>
                <td class="label w100">
                    ${_(u"Fichero Procesado")}
                </td>
                <td>
                    ${Norma57File.name}
                </td>
            </tr>
        </table>
        <table id="tablec">
            <tr>
                <td class="label w40">
                    ${_(u"Cobros")}
                </td>
                <td class="centered">
                    ${num_linies(cursor, uid, Norma57File.id)}
                </td>
                <td class="label w40" >
                    ${_(u"Importe")}
                </td>
                <td class="centered">
                    ${Norma57File.footer_total_amount}
                </td>
                <td class="label w40" >
                    ${_(u"Fecha")}
                </td>
                <td class="centered">
                    ${Norma57File.header_presentation_date}
                </td>
            </tr>
        </table>
        <br>
        <%
            no_detectats = []
            detectats_i_confirmats = []
            detectats_i_no_confirmats = []
            tables = [
                ('DETECTADOS', detectats_i_confirmats),
                ('DETECTADOS Y NO CONFIRMADOS', detectats_i_no_confirmats),
                ('NO DETECTADOS', no_detectats)
            ]
        %>
        %for Norma57FileLine in Norma57File.lines:
            <%
                if Norma57FileLine.state == 'draft':
                    if Norma57FileLine.resource:
                        detectats_i_no_confirmats.append(Norma57FileLine)
                    else:
                        no_detectats.append(Norma57FileLine)
                else:
                    if Norma57FileLine.resource:
                        detectats_i_confirmats.append(Norma57FileLine)
            %>
        %endfor
        %for header, table in tables:
            <table class="taula-contingut">
                <tr class="label2">
                    <td colspan="7" class="centered">${header}</td>
                </tr>
                <tr class="label2">
                    <td >
                        ${_(u"Nº Factura")}
                    </td>
                    <td>
                        ${_(u"Fecha")}
                    </td>
                    <td>
                        ${_(u"Importe")}
                    </td>
                    <td>
                        ${_(u"F. Cobro")}
                    </td>
                    <td>
                        ${_(u"Cliente")}
                    </td>
                    <td class="col-head-nombre" >
                        ${_(u"Nombre")}
                    </td>
                    <td class="col-head-incidencia" >
                        ${_(u"Sufijo")}
                    </td>
                </tr>
                <%
                    subtotal = 0
                %>
                %for Norma57FileLine in table:
                    <%
                        factura_vals = Norma57FileLine.get_factura()[Norma57FileLine.id]
                    %>
                    <tr class="row-contingut-value">
                        <td id="col-val-nfactura" >
                            ${factura_vals['num_factura']}
                        </td>
                        <td id="col-val-fecha" >
                            ${Norma57FileLine.date}
                        </td>
                        <td id="col-val-importe">
                            <%
                                subtotal += Norma57FileLine.amount
                                importe = float('{0:.2f}'.format(Norma57FileLine.amount))
                            %>
                            ${importe}
                        </td>
                        <td id="col-val-fcobro">
                            ${Norma57FileLine.payment_identification}
                        </td>
                        <td id="col-val-cliente">
                            ${factura_vals['client']}
                        </td>
                        <td id="col-val-nombre">
                            ${factura_vals['nom_client']}
                        </td>
                        <td id="col-val-sufijo">
                            ${Norma57FileLine.sufix}
                        </td>
                    </tr>
                %endfor
                <tr class="label2">
                    <td id="subtotal-header">SUBTOTAL</td>
                    <td colspan="6" id="subtotal-value">${subtotal}</td>
                </tr>
            </table>
        %endfor
    %endfor
	</body>
</html>
