# -*- coding: utf-8 -*-
{
  "name": "Remesas (Signatura)",
  "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Afegeix el mandato com a model signable
""",
  "version": "0-dev",
  "author": "GISCE",
  "category": "Master",
  "depends": [
      'l10n_ES_remesas',
      'giscedata_signatura_documents'
  ],
  "init_xml": [],
  "demo_xml": [],
  "update_xml": [
      'l10n_ES_remesas_signatura_data.xml'
  ],
  "active": False,
  "installable": True
}
