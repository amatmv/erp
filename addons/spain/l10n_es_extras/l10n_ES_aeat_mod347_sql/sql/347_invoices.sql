SELECT
   coalesce(sum(amount_total), 0) as total
FROM account_invoice invoice
WHERE invoice.partner_id in %s
AND invoice.type = %s
AND invoice.period_id in %s
AND coalesce(invoice.move_id, 0) <> 0
