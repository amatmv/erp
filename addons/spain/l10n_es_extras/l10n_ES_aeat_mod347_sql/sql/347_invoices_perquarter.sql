SELECT
   coalesce(sum(CASE
                  WHEN invoice.type = %s
                  THEN amount_total*-1
                  ELSE amount_total
                END), 0) as total,
   period.quarter
FROM account_invoice invoice
INNER JOIN account_period period
ON period.id = invoice.period_id
WHERE invoice.partner_id in %s
AND invoice.type in %s
AND invoice.period_id in %s
AND coalesce(invoice.move_id, 0) <> 0
group by period.quarter