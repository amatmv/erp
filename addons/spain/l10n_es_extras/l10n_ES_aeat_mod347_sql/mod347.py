# -*- coding: utf-8 -*-

from osv import osv, fields
from tools import config


class Mod347DistriInvoiceRecord(osv.osv):

    _name = 'l10n.es.aeat.mod347.invoice_record'
    _inherit = 'l10n.es.aeat.mod347.invoice_record'

    _columns = {
        'invoice_id': fields.many2one('account.invoice', 'Invoice',
                                      required=False, ondelete="cascade"),
        'quarter':fields.selection( [('first','First'),
                                     ('second','Second'),
                                     ('third','Third'),
                                     ('fourth','Fourth')], 'Quarter'),
    }

Mod347DistriInvoiceRecord()


class Mod347DistriPartnerRecord(osv.osv):
    """
    Represents a partner record for the 347 model.
    """
    _name = 'l10n.es.aeat.mod347.partner_record'
    _inherit = 'l10n.es.aeat.mod347.partner_record'

    def _get_quarter_totals(self, cr, uid, ids, field_name, arg, context = None):
        
        if context is None:
            context={}

        result = {}
        for record  in self.browse(cr, uid, ids, context):
            result[record.id] ={
            'first_quarter':0,
            'first_quarter_real_state_transmission_amount':0,
            'second_quarter': 0,
            'second_quarter_real_state_transmission_amount':0,
            'third_quarter': 0,
            'third_quarter_real_state_transmission_amount':0,
            'fourth_quarter': 0,
            'fourth_quarter_real_state_transmission_amount':0,
            }
            for invoice in record.invoice_record_ids:
                if invoice.quarter == 'first':
                    result[record.id]['first_quarter'] += invoice.amount
                elif invoice.quarter == 'second':
                    result[record.id]['second_quarter'] += invoice.amount
                elif invoice.quarter == 'third':
                    result[record.id]['third_quarter'] += invoice.amount
                elif invoice.quarter == 'fourth':
                    result[record.id]['fourth_quarter'] += invoice.amount

        return result

    def _get_lines( self, cr, uid, ids, context ):
        invoice_record_obj = self.pool.get('l10n.es.aeat.mod347.invoice_record')

        res = []        
        for invoice_record in invoice_record_obj.browse(cr, uid, ids, context):
            res.append( invoice_record.partner_record_id.id )
        return list(set(res))

    _columns = {
        'first_quarter': fields.function(_get_quarter_totals, string="First Quarter",
                method=True, type='float', multi="quarter_multi",digits=(13,2),
                store= { 
                    'l10n.es.aeat.mod347.invoice_record': (_get_lines, ['amount'] , 10 )
                }),
        'first_quarter_real_state_transmission_amount':fields.function(_get_quarter_totals, string="First Quarter Real State Transmission Amount",
                method=True, type='float', multi="quarter_multi" ,digits=(13,2),
                store= { 
                    'l10n.es.aeat.mod347.invoice_record': (_get_lines, ['amount'] , 10 )
                }
                ),
        'second_quarter': fields.function(_get_quarter_totals, string="Second Quarter", method=True,
                type='float', multi="quarter_multi", digits=(13,2), 
                store= { 
                    'l10n.es.aeat.mod347.invoice_record': (_get_lines, ['amount'] , 10 )
                }),
        'second_quarter_real_state_transmission_amount':fields.function(_get_quarter_totals, string="Second Quarter Real State Transmission Amount",
                method=True, type='float', multi="quarter_multi",digits=(13,2), store= { 
                    'l10n.es.aeat.mod347.invoice_record': (_get_lines, ['amount'] , 10 )
                }),
        'third_quarter': fields.function(_get_quarter_totals, string="Third Quarter", method=True, type='float',
                multi="quarter_multi",digits=(13,2), store= { 
                    'l10n.es.aeat.mod347.invoice_record': (_get_lines, ['amount'] , 10 )
                }),
        'third_quarter_real_state_transmission_amount':fields.function(_get_quarter_totals, string="Third Quarter Real State Transmission Amount",
                method=True, type='float', multi="quarter_multi",digits=(13,2), store= { 
                    'l10n.es.aeat.mod347.invoice_record': (_get_lines, ['amount'] , 10 )
                } ),
        'fourth_quarter': fields.function(_get_quarter_totals, string="Fourth Quarter",
                method=True, type='float', multi="quarter_multi",digits=(13,2), store= { 
                    'l10n.es.aeat.mod347.invoice_record': (_get_lines, ['amount'] , 10 )
                }),
        'fourth_quarter_real_state_transmission_amount':fields.function(_get_quarter_totals, string="Fourth Quarter Real State Transmossion Amount",
                method=True, type='float', multi="quarter_multi",digits=(13,2), store= { 
                    'l10n.es.aeat.mod347.invoice_record': (_get_lines, ['amount'] , 10 )
                }), 
    }

Mod347DistriPartnerRecord()
