# -*- coding: utf-8 -*-
{
  "name": "AEAT Modelo 560",
  "description": """
  * Modelo 560 para la AEAT
""",
  "version": "0-dev",
  "author": "GISCE",
  "category": "Master",
  "depends": ['base', 'account_chart_iese', 'l10n_chart_ES', 'l10n_ES_aeat', 'c2c_webkit_report', 'giscedata_facturacio_iese', 'report_joiner'],
  "init_xml": [],
  "demo_xml": [],
  "update_xml": ['wizard/wizard_print_mod560_view.xml', 'security/ir.model.access.csv'],
  "active": False,
  "installable": True
}
