select
  factura,
  contrato,
  nombre,
  nif,
  cups,
  cie_opt,
  provincia,
  round(cantidad / 1000.0, 2) as cantidad,
  case when tax like '%%-85%%' then 'R'
	     when tax like '%%(Exento)%%' then 'E'
	     when nif in (select prop.vat from res_partner prop
  			inner join res_company com on com.partner_id = prop.id and com.id = 1)  then 'A'
	     else 'P'
  end as concepto,
  base,
  base_imponible,
  tarifa_aeat,
  cuota
from (
  select
    factura,
    p.name as contrato,
    partner.name as nombre,
    partner.vat as nif,
    coalesce(p.cie, '') as cie_opt,
    cups.name as cups,
    prov.name as provincia,
    sum(ener.cantidad) as cantidad,
    case
       when case when taxes.name like '%%-85%%' then 'R'
				 when taxes.name like '%%(Exento)%%' then 'E'
				 when tit.vat in (select prop.vat from res_partner prop
                        inner join res_company com on com.partner_id = prop.id and com.id = 1)  then 'A'
				 else 'P'
			end in ('A','E') then ''
       when t.name like '2.0%%' then '1'
       when t.name ilike '2.1%%' then '2'
       when t.name ilike '3.0%%' then '3'
       when t.name ilike '3.1%%' then '4'
       when t.name ilike '6.1%%' then '5'
       else '6'
    end as tarifa_aeat,
    taxes.name as tax,
    sum(taxes.factor_base * case when i.type = 'out_refund' then -1 else 1 end) as base_imponible,
    sum(abs(taxes.base_amount) * case when i.type = 'out_refund' then -1 else 1 end) as base,
    sum(abs(taxes.tax_amount) * case when i.type = 'out_refund' then -1 else 1 end) as cuota
  from (
    select
      i.number as factura,
      sum(il.quantity * case when i.type = 'out_refund' then -1 else 1 end * case when l.tipus = 'energia' then 1 else 0 end) as cantidad,
      il.invoice_id,
      l.factura_id
    from giscedata_facturacio_factura_linia l
    inner join account_invoice_line il on l.invoice_line_id = il.id
    inner join account_invoice i on il.invoice_id = i.id
    where l.tipus in ('energia', 'potencia') and l.isdiscount = False
    group by il.invoice_id, l.factura_id, i.number
  ) as ener
  inner join giscedata_facturacio_factura f on ener.factura_id = f.id
  inner join account_invoice i on f.invoice_id = i.id
  inner join giscedata_polissa_tarifa t on f.tarifa_acces_id = t.id
  inner join giscedata_polissa p on f.polissa_id = p.id
  inner join res_partner tit on tit.id = p.titular
  inner join giscedata_cups_ps cups on f.cups_id = cups.id
  inner join res_municipi mun on cups.id_municipi = mun.id
  inner join res_country_state prov on mun.state = prov.id
  inner join account_invoice_tax taxes on taxes.invoice_id = i.id
  inner join res_partner partner on i.partner_id = partner.id
  inner join account_fiscal_position afp on i.fiscal_position = afp.id
  where
    i.date_invoice between %s and %s
    and taxes.name ilike '%%electricidad%%'
    and i.type in ('out_invoice', 'out_refund')
    and i.state in ('open', 'paid')
    and afp.name ilike '%%IESE%%'
  group by tarifa_aeat,taxes.name, cie_opt, factura, contrato, nombre, nif, cups.name, provincia
) as q
order by concepto, tarifa_aeat
