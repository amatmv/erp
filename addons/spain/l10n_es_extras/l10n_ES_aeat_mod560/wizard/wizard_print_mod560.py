# coding=utf-8
from osv import osv, fields
from addons import get_module_resource


class WizardPrintMod650(osv.osv_memory):
    _name = 'wizard.print.mod560'

    _columns = {
        'date_start': fields.date('Fecha inicio'),
        'date_end': fields.date('Fecha final'),
        'cie_disaggregated': fields.boolean('Desagregado por CIE'),
        'reduction_annex': fields.boolean('Anexo de reducciones')
    }

    def get_results(self, cursor, uid, ids, context=None):
        wiz = self.browse(cursor, uid, ids[0], context=context)
        sql_file = get_module_resource(
            'l10n_ES_aeat_mod560', 'sql', 'mod560.sql'
        )
        with open(sql_file, 'r') as f:
            sql = f.read()

        cursor.execute(sql, (
            bool(wiz.cie_disaggregated), wiz.date_start, wiz.date_end
        ))
        results = cursor.dictfetchall()
        return results

    def print_report(self, cursor, uid, ids, context=None):
        wiz = self.browse(cursor, uid, ids[0], context=context)
        results = wiz.get_results()
        datas = {'form': wiz.read(), 'results': results}
        report = 'mod560'
        if wiz.reduction_annex:
            report = 'mod560.with.annex'
        return {
            'type': 'ir.actions.report.xml',
            'report_name': report,
            'datas': datas
        }

    _defaults = {
        'cie_disaggregated': 0,
        'reduction_annex': 0
    }

WizardPrintMod650()
