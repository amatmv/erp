<%
    from datetime import datetime
    from babel.numbers import format_currency, format_number
    from babel.dates import format_date
    from addons import get_module_resource


    def get_results(cursor, start, end):
        sql_file = get_module_resource(
            'l10n_ES_aeat_mod560', 'sql', 'mod560_annex.sql'
        )
        with open(sql_file, 'r') as f:
            sql = f.read()
        cursor.execute(sql, (start, end))
        results = cursor.dictfetchall()
        return results
%>
<!doctype html>
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<head>
    	<style type="text/css">
body
{
    padding: 5px;
	line-height: 1.6em;
    font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
    font-size: 12px;
}

#hor-minimalist-a
{
	background: #fff;
	margin: 45px;
	border-collapse: collapse;
	text-align: left;
}
#hor-minimalist-a th
{
	font-size: 14px;
	font-weight: normal;
	color: #039;
	padding: 10px 8px;
	border-bottom: 2px solid #6678b1;
}
#hor-minimalist-a td
{
	color: #669;
	padding: 9px 8px 0px 8px;
    min-width: 100px;
}
            #hor-minimalist-a tfoot td
            {
                padding-top: 15px;
                font-weight: bold;
            }

        </style>
</head>
${company.name}
<h1>Anexo - Modelo 560</h1>
<dl>
    <dt>Fecha desde:</dt>
    <dd>${format_date(datetime.strptime(data['form'][0]['date_start'], '%Y-%m-%d'), "dd/MM/yyyy", locale='es_ES')}</dd>
    <dt>Fecha hasta:</dt>
    <dd>${format_date(datetime.strptime(data['form'][0]['date_end'], '%Y-%m-%d'), "dd/MM/yyyy", locale='es_ES')}</dd>
</dl>
<table id="hor-minimalist-a">
    <thead>
        <th>Factura</th>
        <th>Contrato</th>
        <th>CIE</th>
        <th>Nombre</th>
        <th>NIF</th>
        <th>CUPS</th>
        <th>Provincia</th>
        <th>Concepto</th>
        <th>Tarifa</th>
        <th>Cantidad (MWh)</th>
        <th>Base imponible</th>
        <th>Base</th>
        <th>Cuota</th>
    </thead>
    <tbody>
    <%
        total_cantidad = 0
        total_base_imponible = 0
        total_base = 0
        total_cuota = 0
    %>
    %for result in get_results(user._cr, data['form'][0]['date_start'], data['form'][0]['date_end']):
        <%
            total_cantidad += result['cantidad']
            total_base_imponible += result['base_imponible']
            total_base += result['base']
            total_cuota += result['cuota']
        %>
        <tr>
            <td>${result['factura']}</td>
            <td>${result['contrato']}</td>
            <td>${result['cie_opt']}</td>
            <td>${result['nombre']}</td>
            <td>${result['nif']}</td>
            <td>${result['cups']}</td>
            <td>${result['provincia']}</td>
            <td>${result['concepto']}</td>
            <td>${result['tarifa_aeat']}</td>
            <td>${format_number(result['cantidad'], locale='es_ES')}</td>
            <td>${format_currency(result['base_imponible'], 'EUR', locale='es_ES')}</td>
            <td>${format_currency(result['base'], 'EUR', locale='es_ES')}</td>
            <td>${format_currency(result['cuota'], 'EUR', locale='es_ES')}</td>
        </tr>
    %endfor
    </tbody>
    <tfoot>
        <tr>
            <td colspan="9">Totales</td>
            <td>${format_number(total_cantidad, locale='es_ES')}</td>
            <td>${format_currency(total_base_imponible, 'EUR', locale='es_ES')}</td>
            <td>${format_currency(total_base, 'EUR', locale='es_ES')}</td>
            <td>${format_currency(total_cuota, 'EUR', locale='es_ES')}</td>
        </tr>
    </tfoot>
</table>
</html>
