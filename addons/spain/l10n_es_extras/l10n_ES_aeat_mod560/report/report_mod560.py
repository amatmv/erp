# coding=utf-8
# coding=utf-8
from c2c_webkit_report import webkit_report
from report import report_sxw
from report_joiner.report_joiner import ReportJoiner


class DummyHeader(object):
    margin_top = 0
    margin_bottom = 0
    margin_left = 0
    margin_right = 0
    orientation = 'Portrait'
    format = 'A4'
    html = '<html></html>'
    footer_html = '<html></html>'
    css = ''


class NoXMLWebkitReport(webkit_report.WebKitParser):
    def create_single_pdf(self, cursor, uid, ids, data, report_xml=False,
                          context=None):
        webkit_header = getattr(report_xml, 'webkit_header', None)
        if not webkit_header:
            report_xml.webkit_header = DummyHeader()
        return super(NoXMLWebkitReport, self).create_single_pdf(
            cursor, uid, ids, data, report_xml, context
        )


NoXMLWebkitReport(
    'report.mod560', 'wizard.print.mod560',
    'l10n_ES_aeat_mod560/report/report_mod560.mako',
    parser=report_sxw.rml_parse
)


class DummyHeader2(object):
    margin_top = 0
    margin_bottom = 0
    margin_left = 0
    margin_right = 0
    orientation = 'Landscape'
    format = 'A4'
    html = '<html></html>'
    footer_html = '<html></html>'
    css = ''


class NoXMLWebkitReport2(webkit_report.WebKitParser):
    def create_single_pdf(self, cursor, uid, ids, data, report_xml=False,
                          context=None):
        webkit_header = getattr(report_xml, 'webkit_header', None)
        if not webkit_header:
            report_xml.webkit_header = DummyHeader2()
        return super(NoXMLWebkitReport2, self).create_single_pdf(
            cursor, uid, ids, data, report_xml, context
        )


NoXMLWebkitReport2(
    'report.mod560.annex', 'wizard.print.mod560',
    'l10n_ES_aeat_mod560/report/report_mod560_annex.mako',
    parser=report_sxw.rml_parse
)


ReportJoiner(
    'report.mod560.with.annex',
    reports=['report.mod560', 'report.mod560.annex']
)
