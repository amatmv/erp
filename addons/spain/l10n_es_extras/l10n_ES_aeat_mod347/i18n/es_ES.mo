��    �      �  �   |      h  u  i     �     �                2     N     b     k  (   |     �  &   �     �     �       A        Y     h     w     �     �     �  )   �     �     �            =     =   Y  7   �     �     �     �  <   �     ,     0     6  ;   P  	   �     �     �     �     �     �     �     �     �     �     	       ,   *     W     \  
   d  -   o     �     �     �  T   �                    *     =     E     J     O     U  [   Y     �  #   �     �     �            ,        H     T     a     g     n  -   }  	   �     �     �     �     �     �      	  	   *     4  "   H  ,   k     �     �     �     �     �     �  	   �  	   �                  5      V      Z      a      h      |      �   W   �   8   �      !!     /!  !   A!     c!     v!     �!     �!     �!     �!     �!     �!     �!     �!  L   �!     L"     R"     Y"     e"  	   �"  
   �"     �"     �"  
   �"     �"     �"     �"     �"  %   #     -#     M#     `#     u#     �#     �#     �#     �#     �#  	   �#     �#     �#  -   $  �   2$     �$     �$  
   �$     �$     �$     %     %     %  &   )%  
   P%  J   [%  r   �%  R   &  F   l&  /   �&  -   �&  9   '  <   K'     �'     �'  ,   �'     �'     �'     �'  
   �'     �'  '   �'  `  &(  u  �)     �1     2     +2     >2     P2     l2     �2     �2  )   �2     �2  %   �2     3     3     '3  A   63     x3     �3     �3     �3     �3     �3  1   �3     $4  
   74  
   B4     M4  X   `4  =   �4  Q   �4     I5     Q5     g5  <   k5     �5     �5     �5  ;   �5     6     6     6     /6  	   86     B6     V6     k6     6     �6     �6     �6  7   �6  
   7  
   7     )7  2   57     h7  	   w7     �7  T   �7     �7     �7     �7     �7     8  	   8     )8     08     98  [   =8     �8  !   �8     �8     �8     �8     �8  "   9     09     A9     S9     Z9     a9  "   r9     �9     �9     �9     �9     �9     �9  '   :  
   .:     9:  0   K:  9   |:     �:     �:     �:     �:     �:     ;     $;  
   0;     ;;      H;      i;     �;     �;     �;     �;     �;     �;  �   �;  E   �<     �<     �<  7   �<     0=     H=     g=     o=     �=     �=     �=     �=     �=     �=  j    >  	   k>     u>     |>     �>  	   �>  
   �>  	   �>     �>     �>     �>     �>     ?     -?  +   K?     w?     �?     �?     �?     �?     �?  
   @     @     -@  	   K@     U@     ]@  "   o@  �  �@     �B     �B     �B     �B     �B     �B     �B     �B  )   �B     C  T   'C  u   |C  T   �C  e   GD  1   �D  9   �D  E   E  I   _E     �E     �E  "   �E  	   �E     �E     F     F     F  4    F     D   j              m           �   �   8       '               �      �         �          r       =   6          &   �   �   P   |   [       L   f      z   *   �       �          �       4       (          V           T   O   �       ?   �   �   E   ;   y   �   �              X   p      W               �   5   q          �   ,   �          �           �                              >   �      C             �       �   0   t   �   �      �      Z   i   U   B   M   �   7   �   K   �   d   ^   �          g   
   w   l       �   �          )   b       ]             �   	   $   �           �               �   �   s       3   x               `      I   �       N   A       �       @      �   �   �   H   ~   F   o               �   a   2   "       R   S   Y   J   �   +   G   �          u       e       v   �                 �   {   9   <   h   �   }   �   1   .       :   �   \       c   k   #   �       !   _   %       /   -      n   �           Q       �   �   �    
    Módulo para la presentación del Modelo AEAT 347 (Declaración Anual de Operaciones con Terceros)

Basado en la Orden EHA/3012/2008, de 20 de Octubre, por el que se aprueban los diseños físicos y lógicos del 347.

De acuerdo con la normativa de la Hacienda Española, están obligados a presentar el modelo 347:
    * Todas aquellas personas físicas o jurídicas, de naturaleza pública o
        privada que desarrollen actividades empresariales o profesionales,
        siempre y cuando hayan realizado operaciones que, en su conjunto,
        respecto de otra persona o Entidad, cualquiera que sea su naturaleza
        o carácter, hayan superado la cifra de 3.005,06€ durante el año natural
        al que se refiere la declaración. Para el cálculo de la cifra de
        3.005,06 € se computan de forma separada las entregas de bienes
        y servicios y las adquisiciones de los mismos.

De acuerdo con la normativa no están obligados a presentar el modelo 347:
    * Quienes realicen en España actividades empresariales o profesionales sin
        tener en territorio español la sede de su actividad, un establecimiento
        permanente o su domicilio fiscal.
    * Las personas físicas y entidades en régimen de atribución de rentas en
        el IRPF, por las actividades que tributen en dicho impuesto por el
        régimen de estimación objetiva y, simultáneamente, en el IVA por los
        régimenes especiales simplificados o de la agricultura, ganadería
        y pesca o recargo de equivalencia, salvo las operaciones que estén
        excluidas de la aplicación de los expresados regímenes.
    * Los obligados tributarios que no hayan realizado operaciones que en su
        conjunto superen la cifra de 3.005,06€
    * Los obligados tributarios que hayan realizado exclusivamente operaciones
        no declarables.
    * Los obligados tributarios que deban informar sobre las operaciones
        incluidas en los libros registro de IVA (modelo 340) salvo que realicen
        operaciones que expresamente deban incluirse en el modelo 347.

(http://www.boe.es/boe/dias/2008/10/23/pdfs/A42154-42190.pdf)
     ## DATA LINES ## ## DESRIPTION / SEPARATOR ## ## DETAIL LINES ## ## REPORT INFO ## ### PARTER RECORD LINES ### ### REPORT INFO ### %Y-%m-%d %s_report_%s.txt 1 - Spain but Basque Country and Navarra 2 - Basque Country and Navarra 3 - Spain, without catastral reference 347 report %s 347_report_%s.txt 4 - Foreign A - Adquisiciones de bienes y servicios superiores al límite (1) AEAT 347 MODEL AEAT 347 Model AEAT 347 Model report AEAT 347 Report AEAT 347 Reports AEAT Model 347 AEAT Model 347 Wizard - Calculate Records AEAT Reports Account move line Address Address type All partner state code field must be filled.
Partner: %s (%s) All partner vat number field must be filled.
Partner: %s (%s) All real state records state code field must be filled. Amount Amount (Q1|Q2|Q3|Q4) Ant B - Entregas de bienes y servicios superiores al límite (1) Bis Block Bussiness Real State Rent C - Cobros por cuenta de terceros superiores al límite (3) Calculate Calculation Calculation date Cancel Canceled Cash Amount Cash Record Cash amount Cash payments journal Cash record Cash records Catastral Reference Charges on behalf of third parties Limit (3) City Company Complement Complement (urbanization, industrial park...) Complementary Confirm Country Code D - Adquisiciones efectuadas por Entidades Públicas (...) superiores al límite (1) DVD Date Declaration Declaration Number Details Done Door Draft Dup E - Subvenciones, auxilios y ayudas satisfechas por Ad. Públicas superiores al límite (1) Error! Export AEAT Model 347 to BOE format Export to BOE F - Ventas agencia viaje First First Quarter First Quarter Real State Transmission Amount Fiscal Year Fiscal year: Floor Fourth Fourth Quarter Fourth Quarter Real State Transmossion Amount Full Name G - Compras agencia viaje General Group by cif Identification Include in 347 Report Include in AEAT 347 Model report Ins. Oper Insurance Operation Invalid XML for View Architecture! Invalid model name in the action definition. Invoice Invoice Record Invoice record Invoice records Invoiced Limit (1) Invoiced amount Key Oper. Kilometer L.R. VAT number Legal Representative VAT number Legal Representative VAT number. Mod Normal Number Number calification Number type Number: Only for insurance companies. Set to identify insurance operations aside from the rest. Only reports in 'draft' or 'cancel' state can be removed Operation Key Operations amount Origin cash operation fiscal year Origin fiscal year PARTNER RECORD LINES Partner Partner Record Partner Records Partner info Partner record Partner records Partners records Payment records Payments of this journal will be considered as cash (used on the 347 report) Phone Portal Postal code Previous Declaration Number Processed Processing Quarter RS Rent RST amount Real State Amount Real State Record Real State Records Real State Transmisions amount Real State Transmission (Q1|Q2|Q3|Q4) Real State Transmissions Amount Real State records Real state Situation Real state address Real state info Real state records Recalculate Received cash Limit (2) Received cash amount SEPARATOR Second Second Quarter Second Quarter Real State Transmission Amount Set to identify real state rent operations aside from the rest. You'll need to fill in the real state info only when you are the one that receives the money. Stairway State State Code Statement Type Substitutive Summary Support Type Support type: Susbtitutive/complementary declaration Telematics The Object name must start with x_ and not contain any special character ! The declaration will include partners from which we received payments, on behalf of third parties, over this limit The declaration will include partners with the total of operations over this limit The declaration will show the total of cash operations over this limit The formated string must match the given length The type 1 record must be 502 characters long The type 2-D record (partner) must be 502 characters long The type 2-I record (real state) must be 502 characters long Third Third Quarter Third Quarter Real State Transmission Amount Township Township Code VAT VAT number Without number Wrong aling option. It should be < or > Project-Id-Version: OpenERP Server 5.0.14
Report-Msgid-Bugs-To: support@openerp.com
POT-Creation-Date: 2013-01-28 12:39:14+0000
PO-Revision-Date: 2013-01-28 13:41+0100
Last-Translator: Joan M. Grande <totaler@gmail.com>
Language-Team: 
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: 
 
    Módulo para la presentación del Modelo AEAT 347 (Declaración Anual de Operaciones con Terceros)

Basado en la Orden EHA/3012/2008, de 20 de Octubre, por el que se aprueban los diseños físicos y lógicos del 347.

De acuerdo con la normativa de la Hacienda Española, están obligados a presentar el modelo 347:
    * Todas aquellas personas físicas o jurídicas, de naturaleza pública o
        privada que desarrollen actividades empresariales o profesionales,
        siempre y cuando hayan realizado operaciones que, en su conjunto,
        respecto de otra persona o Entidad, cualquiera que sea su naturaleza
        o carácter, hayan superado la cifra de 3.005,06€ durante el año natural
        al que se refiere la declaración. Para el cálculo de la cifra de
        3.005,06 € se computan de forma separada las entregas de bienes
        y servicios y las adquisiciones de los mismos.

De acuerdo con la normativa no están obligados a presentar el modelo 347:
    * Quienes realicen en España actividades empresariales o profesionales sin
        tener en territorio español la sede de su actividad, un establecimiento
        permanente o su domicilio fiscal.
    * Las personas físicas y entidades en régimen de atribución de rentas en
        el IRPF, por las actividades que tributen en dicho impuesto por el
        régimen de estimación objetiva y, simultáneamente, en el IVA por los
        régimenes especiales simplificados o de la agricultura, ganadería
        y pesca o recargo de equivalencia, salvo las operaciones que estén
        excluidas de la aplicación de los expresados regímenes.
    * Los obligados tributarios que no hayan realizado operaciones que en su
        conjunto superen la cifra de 3.005,06€
    * Los obligados tributarios que hayan realizado exclusivamente operaciones
        no declarables.
    * Los obligados tributarios que deban informar sobre las operaciones
        incluidas en los libros registro de IVA (modelo 340) salvo que realicen
        operaciones que expresamente deban incluirse en el modelo 347.

(http://www.boe.es/boe/dias/2008/10/23/pdfs/A42154-42190.pdf)
     ## DATA LINES ## ## DESRIPTION / SEPARATOR ## ## DETAIL LINES ## ## REPORT INFO ## ### PARTER RECORD LINES ### ### REPORT INFO ### %Y-%m-%d %s_informe_%s.txt 1 - España excepto País Vasco y Navarra 2 - País Vasco y Navarra 3 - España, sin referencia catastral Informe 347 %s informe_347_%s.txt 4 - Extranjero A - Adquisiciones de bienes y servicios superiores al límite (1) MODELO AEAT 347 Modelo AEAT 347 Declaración modelo AEAT 347 Declaración AEAT 347 Declaraciones AEAT 347 Modelo AEAT 347 Asistente modelo AEAT 347 - Cálculo de registros Declaraciones AEAT Movimiento Dirección Tipo de dirección Todos lso códigos de provincia de las empresas deben estar rellenados.
Empresa: %s (%s) All partner vat number field must be filled.
Partner: %s (%s) El código de provincia de todos los registros de inmuebles debe estar rellenado. Importe Importe (T1|T2|T3|T4) Ant B - Entregas de bienes y servicios superiores al límite (1) Bis Bloque Arrendamiento local negocio C - Cobros por cuenta de terceros superiores al límite (3) Calcular Cálculo Fecha de cálculo Cancelar Cancelada Importe en efectivo Registro de efectivo Importe en efectivo Diario de pagos en efectivo Registro de efectivo Registros de efectivo Referencia catastral Cargos por cuenta de terceros superiores al límite (3) Población Compañía Complemento Complemento (urbanización, centro comercial, ...) Complementaria Confirmar Código de país D - Adquisiciones efectuadas por Entidades Públicas (...) superiores al límite (1) DVD Fecha Declaración Número de declaración Detalles Realizada Puerta Borrador Dup E - Subvenciones, auxilios y ayudas satisfechas por Ad. Públicas superiores al límite (1) ¡Error! Exportar modelo 347 a formato BOE Exportar a formato BOE F - Ventas agencia viaje Primero Primer Trimestre Importe transmisión inmueble (T1) Ejercicio fiscal Ejercicio fiscal: Planta Cuarto Cuarto Trimestre Importe transmisión inmueble (T4) Apellidos y nombre G - Compras agencia viaje General Agrupar por cif Identificación Incluir en declaración 347 Incluir en declaración modelo AEAT 347 Op. seguro Operación seguro ¡XML inválido para la definición de la vista! Nombre de modelo no válido en la definición de acción. Factura Registro de factura Registro de factura Registros de facturas Límite facturado (1) Importe facturado Clave oper. Kilómetro CIF/NIF R.L. CIF/NIF del representante legal. CIF/NIF del representante legal. Mod Normal Número Calificador del número Tipo de numeración Número: Sólo entidades aseguradoras. Las entidades aseguradoras marcarán esta casilla para identificar las operaciones de seguros, debiendo consignarlas separadamente del resto de operaciones. Sólo informes en estado 'borrador' o 'cancelado' pueden ser borrados Clave operación Importe operaciones Ejercicio fiscal origen de las operaciones en efectivo. Ejercicio fiscal origen LÍNEAS DE REGISTRO DE EMPRESA Empresa Registro de empresa Registros de empresas Información empresa Registro de empresa Registros de empresas Registros de empresas Registros de pago Los pagos realizados con este diario serán considerados como efectivo (utilizado en la declaración 347). Teléfono Portal Código postal Número declaración anterior Procesada Procesando Trimestre Arrend. inmueb. negocio Importe T.I. Importe inmuebles Registros de inmuebles Registros de inmuebles Importe transmisión inmueble Importe transmisión inmueble (T1|T2|T3|T4) Importe transmisión inmueble Registros de inmuebles Situación de inmuebles Dirección del inmueble Información de inmuebles Registros de inmuebles Recalcular Límite efectivo (2) Importe recibido en metálico SEPARATOR Segundo Segundo Trimestre Importe transmisión inmueble (T2) (Sólo arrendadores y arrendatarios de Locales de Negocio). Marcarán esta casilla para identificar las operaciones de arrendamiento de locales de negocio, debiendo consignarlas separadamente del resto. Además los arrendadores deberán cumplimentar los campos que componen el REGISTRO DE INMUEBLE, consignando el Importe Total de cada arrendamiento correspondiente al año natural al que se refiere la declaración, con independencia de que éste ya haya sido incluido en la clave 'B' (ventas). Escalera Estado Código de provincia Tipo de declaración Sustitutiva Resumen Tipo de soporte Tipo de soporte: Declaración Complementaria / Sustitutiva Telemática ¡El nombre del objeto debe empezar con x_ y no contener ningún carácter especial! La declaración incluirá las empresas a las que hemos realizado cobros por cuenta de terceros superiores al límite. La declaración incluirá las empresas cuya suma de operaciones supere este límite. La declaración muestra el total de las operaciones realizadas en efectivo superiores a este límite. La cadena formateada debe cumplir el tamaño dado El tipo de registro 1 debe ser de 502 caracteres de largo El tipo de registro 2-D (empresa) debe ser de 502 caracteres de largo El tipo de registro 2-I (estado real) debe ser de 502 caracteres de largo Tercero Tercer Trimestre Importe transmisión inmueble (T3) Municipio Código de municipio CIF/NIF CIF/NIF Sin número Opción de alineamiento errónea. Debería ser < o > 