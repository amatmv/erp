{
    "name" : "Google translation",
    "version": "1.0",
    "author" : "Tiny",
    "website" : "http://tinyerp.com",
    "category" : "Generic Modules/Others",
    "depends" : ["base"],
    "description": "Module translate the tiny terms available in translation using google",
    "init_xml" : [],
    "update_xml": ["google_translate_view.xml", "google_translate_wizard.xml"],
    "active": False,
    "installable": True
}