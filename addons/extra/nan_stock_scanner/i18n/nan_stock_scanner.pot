# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* nan_stock_scanner
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.12\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2010-08-29 16:55:07+0000\n"
"PO-Revision-Date: 2010-08-29 16:55:07+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: nan_stock_scanner
#: view:stock.picking:0
msgid "Pending Stock Moves"
msgstr ""

#. module: nan_stock_scanner
#: constraint:ir.model:0
msgid "The Object name must start with x_ and not contain any special character !"
msgstr ""

#. module: nan_stock_scanner
#: code:addons/nan_stock_scanner/stock.py:0
#, python-format
msgid "Invalid printer"
msgstr ""

#. module: nan_stock_scanner
#: model:ir.model,name:nan_stock_scanner.model_stock_scanner_config
msgid "stock.scanner.config"
msgstr ""

#. module: nan_stock_scanner
#: view:nan.stock.picking.scanner.confirm.wizard:0
msgid "Confirm"
msgstr ""

#. module: nan_stock_scanner
#: view:stock.picking:0
msgid "Check Availability"
msgstr ""

#. module: nan_stock_scanner
#: field:stock.scanner.config,input_reports_ids:0
msgid "Input Reports"
msgstr ""

#. module: nan_stock_scanner
#: view:stock.picking:0
msgid "Process Later"
msgstr ""

#. module: nan_stock_scanner
#: help:stock.move,pending_quantity:0
msgid "Quantity pending to be received"
msgstr ""

#. module: nan_stock_scanner
#: constraint:ir.actions.act_window:0
msgid "Invalid model name in the action definition."
msgstr ""

#. module: nan_stock_scanner
#: view:stock.picking:0
msgid "Pending Moves"
msgstr ""

#. module: nan_stock_scanner
#: field:stock.picking,pending_move_line_ids:0
msgid "Pending Lines"
msgstr ""

#. module: nan_stock_scanner
#: view:stock.scanner.config:0
msgid "Input Packing Reports"
msgstr ""

#. module: nan_stock_scanner
#: view:stock.scanner.config:0
msgid "Output Packing Reports"
msgstr ""

#. module: nan_stock_scanner
#: view:stock.picking:0
msgid "Force Availability"
msgstr ""

#. module: nan_stock_scanner
#: help:stock.scanner.config,input_reports_ids:0
msgid "Reports to print after input packing has done"
msgstr ""

#. module: nan_stock_scanner
#: field:stock.move,pending_quantity:0
msgid "Pending Quantity"
msgstr ""

#. module: nan_stock_scanner
#: field:stock.picking,scanned_packaging_id:0
msgid "Packaging"
msgstr ""

#. module: nan_stock_scanner
#: model:ir.module.module,shortdesc:nan_stock_scanner.module_meta_information
msgid "Stock Barcode Scanner"
msgstr ""

#. module: nan_stock_scanner
#: help:product.supplierinfo,product_barcode:0
msgid "The barcode for this product of this supplier."
msgstr ""

#. module: nan_stock_scanner
#: code:addons/nan_stock_scanner/stock.py:0
#, python-format
msgid "No printer specified for report \"%s\"."
msgstr ""

#. module: nan_stock_scanner
#: field:stock.scanner.config,active:0
msgid "Active"
msgstr ""

#. module: nan_stock_scanner
#: view:nan.stock.picking.scanner.confirm.wizard:0
msgid "Partial Picking"
msgstr ""

#. module: nan_stock_scanner
#: help:stock.picking,scanned_lot_ref:0
msgid "Supplier's lot reference."
msgstr ""

#. module: nan_stock_scanner
#: code:addons/nan_stock_scanner/stock.py:0
#, python-format
msgid "Product Error"
msgstr ""

#. module: nan_stock_scanner
#: help:stock.scanner.config,auto_create_lot:0
msgid "Create atutomatic lot on input material, taking care of supplier ref, dluo..."
msgstr ""

#. module: nan_stock_scanner
#: model:ir.actions.act_window,name:nan_stock_scanner.action_stock_picking_scanner_confirm
msgid "Confirm Scanner Picking"
msgstr ""

#. module: nan_stock_scanner
#: view:stock.picking:0
msgid "Process Now"
msgstr ""

#. module: nan_stock_scanner
#: constraint:ir.ui.view:0
msgid "Invalid XML for View Architecture!"
msgstr ""

#. module: nan_stock_scanner
#: help:stock.picking,scanned_quantity:0
msgid "Quantity of the scanned product."
msgstr ""

#. module: nan_stock_scanner
#: field:stock.scanner.config,name:0
msgid "Name"
msgstr ""

#. module: nan_stock_scanner
#: field:product.supplierinfo,product_barcode:0
msgid "Product Barcode"
msgstr ""

#. module: nan_stock_scanner
#: view:nan.stock.picking.scanner.confirm.wizard:0
msgid "Are you sure you want to confirm this picking?"
msgstr ""

#. module: nan_stock_scanner
#: model:ir.actions.act_window,name:nan_stock_scanner.action_stock_scanner_config_form
#: model:ir.ui.menu,name:nan_stock_scanner.menu_action_stock_scanner_form
msgid "Stock Scanner Configuration"
msgstr ""

#. module: nan_stock_scanner
#: view:stock.picking:0
msgid "Packing list"
msgstr ""

#. module: nan_stock_scanner
#: help:stock.picking,scanned_packaging_id:0
msgid "Product's packaging."
msgstr ""

#. module: nan_stock_scanner
#: field:stock.scanner.config,output_reports_ids:0
msgid "Output Reports"
msgstr ""

#. module: nan_stock_scanner
#: field:stock.picking,scanned_product_id:0
msgid "Scanned product"
msgstr ""

#. module: nan_stock_scanner
#: help:stock.scanner.config,output_reports_ids:0
msgid "Reports to print after output packing has done"
msgstr ""

#. module: nan_stock_scanner
#: model:ir.model,name:nan_stock_scanner.model_nan_stock_picking_scanner_confirm_wizard
msgid "nan.stock.picking.scanner.confirm.wizard"
msgstr ""

#. module: nan_stock_scanner
#: help:stock.picking,scanned_dluo:0
msgid "Lot's expire date."
msgstr ""

#. module: nan_stock_scanner
#: field:stock.picking,scanned_quantity:0
msgid "Quantity"
msgstr ""

#. module: nan_stock_scanner
#: view:stock.picking:0
msgid "Confirm scanned products"
msgstr ""

#. module: nan_stock_scanner
#: field:stock.picking,scanned_ean:0
msgid "Ean13"
msgstr ""

#. module: nan_stock_scanner
#: code:addons/nan_stock_scanner/stock.py:0
#, python-format
msgid "Wrong product quantity. Expected %(expected).2f at most but you introduced %(introduced).2f."
msgstr ""

#. module: nan_stock_scanner
#: help:stock.move,received_quantity:0
msgid "Quantity of product received"
msgstr ""

#. module: nan_stock_scanner
#: view:stock.picking:0
msgid "Input Packing List"
msgstr ""

#. module: nan_stock_scanner
#: code:addons/nan_stock_scanner/stock.py:0
#, python-format
msgid "There are no pending moves with this product."
msgstr ""

#. module: nan_stock_scanner
#: code:addons/nan_stock_scanner/stock.py:0
#, python-format
msgid "This product is not pending to be scanned in this order."
msgstr ""

#. module: nan_stock_scanner
#: help:stock.picking,scanned_ean:0
msgid "Type ean for update on  current product"
msgstr ""

#. module: nan_stock_scanner
#: view:stock.picking:0
msgid "Accept"
msgstr ""

#. module: nan_stock_scanner
#: help:stock.picking,scanned_product_id:0
msgid "Scan the code of the next product."
msgstr ""

#. module: nan_stock_scanner
#: field:stock.scanner.config,auto_create_lot:0
msgid "Create automatic lot on input material"
msgstr ""

#. module: nan_stock_scanner
#: view:stock.picking:0
msgid "General Information"
msgstr ""

#. module: nan_stock_scanner
#: field:stock.picking,scanned_dluo:0
msgid "DLUO"
msgstr ""

#. module: nan_stock_scanner
#: field:stock.picking,scanned_lot_ref:0
msgid "Supplier Lot Ref."
msgstr ""

#. module: nan_stock_scanner
#: view:nan.stock.picking.scanner.confirm.wizard:0
#: view:stock.picking:0
msgid "Cancel"
msgstr ""

#. module: nan_stock_scanner
#: help:stock.picking,pending_move_line_ids:0
msgid "List of pending products to be received."
msgstr ""

#. module: nan_stock_scanner
#: view:stock.scanner.config:0
msgid "Scanner Config"
msgstr ""

#. module: nan_stock_scanner
#: model:ir.module.module,description:nan_stock_scanner.module_meta_information
msgid "This module allows the usage of a barcode scanner to process incoming products."
msgstr ""

#. module: nan_stock_scanner
#: field:stock.move,received_quantity:0
msgid "Received Quantity"
msgstr ""

