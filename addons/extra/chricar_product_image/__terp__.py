{
	"name" : "Product Image",
	"version" : "1.1",
	"author" : "ChriCar Beteiligungs- und Beratungs- GmbH",
	"website" : "http://www.chricar.at/ChriCar",
	"category" : "Generic Modules/Others",
	"depends" : ["product"],
	"description" : """Adds an image tab to products
sorts the products by code
        """,
	"init_xml" : [],
	"demo_xml" : [],
	"update_xml" : ["chricar_product_image_view.xml"],
	"active": False,
	"installable": True
}
