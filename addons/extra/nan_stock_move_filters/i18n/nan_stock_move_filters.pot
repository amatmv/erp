# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* nan_stock_move_filters
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.12\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2010-08-31 08:33:07+0000\n"
"PO-Revision-Date: 2010-08-31 08:33:07+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: nan_stock_move_filters
#: constraint:ir.ui.view:0
msgid "Invalid XML for View Architecture!"
msgstr ""

#. module: nan_stock_move_filters
#: model:ir.module.module,description:nan_stock_move_filters.module_meta_information
msgid "This module adds filters to stock moves so only available locations, products and lots are shown in searches, easing the selection of the appropiate ones to the user.\n"
"	\n"
"This module provides a useful infrastructure for specific filters to be implemented in new modules."
msgstr ""

#. module: nan_stock_move_filters
#: model:ir.module.module,shortdesc:nan_stock_move_filters.module_meta_information
msgid "Stock Move Filters"
msgstr ""

