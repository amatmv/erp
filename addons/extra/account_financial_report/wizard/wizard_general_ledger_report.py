# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution	
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    Copyright (c) 2009 Zikzakmedia S.L. (http://zikzakmedia.com) All Rights Reserved.
#                       Jordi Esteve <jesteve@zikzakmedia.com>
#    $Id$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import wizard
import pooler
import time
import base64

from account_financial_report.utils import general_ledger_utils as utils
from tools.translate import _

account_form = '''<?xml version="1.0"?>
<form string="Select parent account">
    <field name="account_list" colspan="4"/>
</form>'''

account_fields = {
    'account_list': {'string':'Account', 'type':'many2many', 'relation':'account.account', 'required':True ,'domain':[]},
}

period_form = '''<?xml version="1.0"?>
<form string="Select Date-Period">
    <field name="company_id" colspan="4"/>
    <newline/>
    <field name="fiscalyear"/>
    <label colspan="2" string="(Keep empty for all open fiscal years)" align="0.0"/>
    <newline/>

    <field name="display_account" required="True"/>
    <field name="sortbydate" required="True"/>
    <field name="landscape"/>
    <field name="amount_currency"/>
    <field name="initial_balance"/>  
    <newline/>
    <separator string="Filters" colspan="4"/>
    <field name="state" required="True"/>
    <newline/>
    <group attrs="{'invisible':[('state','=','none')]}" colspan="4">
        <group attrs="{'invisible':[('state','=','byperiod')]}" colspan="4">
            <separator string="Date Filter" colspan="4"/>
            <field name="date_from"/>
            <field name="date_to"/>
        </group>
        <group attrs="{'invisible':[('state','=','bydate')]}" colspan="4">
            <separator string="Filter on Periods" colspan="4"/>
            <field name="periods" colspan="4" nolabel="1"/>
        </group>
    </group> 
</form>'''

period_fields = {
    'company_id': {'string': 'Company', 'type': 'many2one', 'relation': 'res.company', 'required': True},
    'state':{
        'string':"Date/Period Filter",
        'type':'selection',
        'selection':[('bydate','By Date'),('byperiod','By Period'),('all','By Date and Period'),('none','No Filter')],
        'default': lambda *a:'none'
    },
    'fiscalyear': {'string': 'Fiscal year', 'type': 'many2one', 'relation': 'account.fiscalyear',
                   'help': 'Keep empty for all open fiscal year'},
    'periods': {'string': 'Periods', 'type': 'many2many', 'relation': 'account.period', 'help': 'All periods if empty'},
    'sortbydate':{'string':"Sort by", 'type':'selection', 'selection':[('sort_date','Date'),('sort_mvt','Movement')]},
    'display_account':{'string':"Display accounts ", 'type':'selection',
                       'selection':[('bal_mouvement','With movements'),('bal_all','All'),
                                    ('bal_solde','With balance is not equal to 0')]},
    'landscape':{'string':"Landscape Mode", 'type':'boolean'},
    'initial_balance':{'string':"Show initial balances", 'type':'boolean'},
    'amount_currency':{'string':"With Currency", 'type':'boolean'},
    'date_from': {'string':"Start date", 'type':'date', 'required':True, 'default': lambda *a: time.strftime('%Y-01-01')},
    'date_to': {'string':"End date", 'type':'date', 'required':True, 'default': lambda *a: time.strftime('%Y-%m-%d')},
}

excel_form = '''<?xml version="1.0"?>
<form string="Excel">
    <field name="file" colspan="4"/>
</form>'''

excel_fields = {
    'name': {'string': 'Fitxer Excel', 'type': 'char'},
    'file': {'string': 'Fitxer Excel', 'type': 'binary'}
}


class wizard_report(wizard.interface):

    query = ""  # SQL query to get account moves for given date or period
    min_date = ""  # Min date of the given date or period
    ctx = {}  # Context for given date or period
    ctxfy = {}  # Context from the date start or first period of the fiscal year
    child_ids = ""
    tot_currency = 0.0
    balance_accounts = {}

    def _get_defaults(self, cr, uid, data, context={}):
        user = pooler.get_pool(cr.dbname).get('res.users').browse(cr, uid, uid, context=context)
        if user.company_id:
            company_id = user.company_id.id
        else:
            company_id = pooler.get_pool(cr.dbname).get('res.company').search(cr, uid, [('parent_id', '=', False)])[0]
        data['form']['company_id'] = company_id
        fiscalyear_obj = pooler.get_pool(cr.dbname).get('account.fiscalyear')
        data['form']['fiscalyear'] = fiscalyear_obj.find(cr, uid)
        # Better allow users to set theirs defaults
        #periods_obj=pooler.get_pool(cr.dbname).get('account.period')
        #data['form']['periods'] =periods_obj.search(cr, uid, [('fiscalyear_id','=',data['form']['fiscalyear'])])
        #data['form']['display_account']='bal_all'
        #data['form']['sortbydate'] = 'sort_date'
        #data['form']['landscape']=True
        #data['form']['amount_currency'] = True
        data['form']['context'] = context
        return data['form']


    def _check_path(self, cr, uid, data, context):
        if data['model'] == 'account.account':
            return 'checktype'
        else:
            return 'account_selection'


    def _check(self, cr, uid, data, context):
        if data['form']['landscape']==True:
            return 'report_landscape'
        else:
            return 'report'


    def _check_date(self, cr, uid, data, context):
        sql = """SELECT f.id, f.date_start, f.date_stop
            FROM account_fiscalyear f
            WHERE '%s' between f.date_start and f.date_stop """ % (data['form']['date_from'])
        cr.execute(sql)
        res = cr.dictfetchall()
        if res:
            if (data['form']['date_to'] > res[0]['date_stop'] or data['form']['date_to'] < res[0]['date_start']):
                raise wizard.except_wizard(_('UserError'),_('Date to must be set between %s and %s') % (str(res[0]['date_start']), str(res[0]['date_stop'])))
            else:
                return 'checkreport'
        else:
            raise wizard.except_wizard(_('UserError'),_('Date not in a defined fiscal year'))


    def _check_state(self, cr, uid, data, context):
        if data['form']['state'] == 'bydate':
            self._check_date(cr, uid, data, context)
        #     data['form']['fiscalyear'] = 0
        # else :
        #     data['form']['fiscalyear'] = 1
        return data['form']


    def _excel_create(self, cr, uid, data, context={}):
        form = data['form']
        account_id = form['account_list'][0][2]
        context = form['context']

        pool = pooler.get_pool(cr.dbname)
        account_obj = pool.get('account.account')
        account_browse = account_obj.browse(cr, uid, account_id)[0]

        utils.get_context_date_period(self, cr, uid, form, context=context)

        child_accounts = utils.get_children_accounts(self, cr, uid, account_id, account_browse, form)

        currency_label = ''
        if form['amount_currency']:
            currency_label = 'Currency'

        csv_file = '\n'
        csv_file += 'Fiscal year: {}\n'.format(utils.get_fiscalyear(cr, form))
        csv_file += 'Periods/Date range: {}\n'.format(utils.get_periods(cr, uid, form))
        csv_file += 'Date;Mvt;Partner;Ref;Entry Label;Debit;Credit;Balance;{}\n'.format(currency_label)
        for child_account in child_accounts:
            csv_file += '\n'
            csv_file += '{}\n'.format(child_account.code + ' ' + child_account.name)

            lines = utils.lines(self, cr, uid, child_account, form, context)
            for line in lines:
                line_date = utils.parse_date(line['date'])
                line_mvt = line['move']
                line_partner = line['partner']
                line_ref = line['ref']
                line_entry = line['name']
                line_debit = utils.float_string(line['debit'])
                line_credit = utils.float_string(line['credit'])
                line_balance = utils.float_string(line['progress'])
                line_currency = ''
                if form['amount_currency'] and line['amount_currency'] != 0.0 and line['currency_code'] is not None:
                    line_currency = '{} {}'.format(line['amount_currency'], line['currency_code'])
                csv_file += '{};{};{};{};{};{};{};{};{}\n'.format(line_date, line_mvt, line_partner, line_ref, line_entry, line_debit, line_credit, line_balance, line_currency)
            sum_debit_account = utils.float_string(utils._sum_debit_account(self, cr, child_account, form))
            sum_credit_account = utils.float_string(utils._sum_credit_account(self, cr, child_account, form))
            sum_balance_account = utils.float_string(utils._sum_balance_account(self, cr, uid, child_account, form))
            csv_file += '{};;;;;{};{};{}\n'.format(child_account.code + ' ' + child_account.name, sum_debit_account,
                                                  sum_credit_account, sum_balance_account)

        data['form']['file'] = base64.b64encode(csv_file)
        data['form']['name'] = 'Llibre Major.csv'

        return data['form']

    states = {
        'init': {
            'actions': [],
            'result': {'type':'choice','next_state':_check_path}
        },
        'account_selection': {
            'actions': [],
            'result': {'type':'form', 'arch':account_form,'fields':account_fields, 'state':[('end','Cancel','gtk-cancel'),('checktype','Next','gtk-go-forward')]}
        },
        'checktype': {
            'actions': [_get_defaults],
            'result': {'type':'form', 'arch':period_form, 'fields':period_fields, 'state':[('end','Cancel','gtk-cancel'),('checkreport','Print','gtk-print'), ('excelfile','Excel','gtk-save')]}
        },
        'checkreport': {
            'actions': [],
            'result': {'type':'choice','next_state':_check}
        },
        'excelfile': {
            'actions': [_excel_create],
            'result': {'type': 'form', 'arch': excel_form, 'fields': excel_fields, 'state': [('end', 'Sortir', 'gtk-close')]}
        },
        'report_landscape': {
            'actions': [_check_state],
            'result': {'type':'print', 'report':'account.general.ledger.cumulative.landscape', 'state':'end'}
        },
        'report': {
            'actions': [_check_state],
            'result': {'type':'print', 'report':'account.general.ledger.cumulative', 'state':'end'}
        }
    }
wizard_report('account.general.ledger.cumulative.report')
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
