from tools import config, pooler
from datetime import datetime


def get_fiscalyear_text(cr, uid, form):
    """
    Returns the fiscal year text used on the report.
    """
    fiscalyear_obj = pooler.get_pool(cr.dbname).get('account.fiscalyear')
    if form.get('fiscalyear'):
        fiscalyear = fiscalyear_obj.browse(cr, uid, form['fiscalyear'])
        return fiscalyear.name or fiscalyear.code
    else:
        fiscalyear = fiscalyear_obj.browse(cr, uid, fiscalyear_obj.find(cr, uid))
        return "%s*" % (fiscalyear.name or fiscalyear.code)


def get_periods_and_date_text(cr, uid, form):
    """
    Returns the text with the periods/dates used on the report.
    """
    pool = pooler.get_pool(cr.dbname)
    period_obj = pool.get('account.period')
    fiscalyear_obj = pool.get('account.fiscalyear')
    periods_str = None
    fiscalyear_id = form['fiscalyear'] or fiscalyear_obj.find(cr, uid)
    period_ids = period_obj.search(cr, uid, [('fiscalyear_id' ,'=' ,fiscalyear_id) ,('special' ,'=' ,False)])
    if form['state'] in ['byperiod', 'all']:
        period_ids = form['periods'][0][2]
    periods_str = ', '.join([period.name or period.code for period in period_obj.browse(cr, uid, period_ids)])

    dates_str = None
    if form['state'] in ['bydate', 'all']:
        #dates_str = self.formatLang(form['date_from'], date=True) + ' - ' + self.formatLang(form['date_to'], date=True) + ' '
        date_from = parse_date(form['date_from'])
        date_to = parse_date(form['date_to'])
        dates_str = date_from + ' - ' + date_to + ' '
    if periods_str and dates_str:
        return "%s / %s" % (periods_str, dates_str)
    elif periods_str:
        return "%s" % periods_str
    elif dates_str:
        return "%s" % dates_str
    else:
        return ''


def lines(self, cr, uid, form, ids={}, done=None, level=0, context={}):
    """
    Returns all the data needed for the report lines
    (account info plus debit/credit/balance in the selected period
    and the full year)
    """
    pool = pooler.get_pool(cr.dbname)
    if not ids:
        ids = self.ids
    if not ids:
        return []
    if not done:
        done = {}
    if form.has_key('account_list') and form['account_list']:
        account_ids = form['account_list'][0][2]
        del form['account_list']

    res = {}
    result_acc = []
    accounts_levels = {}

    account_obj = pool.get('account.account')
    period_obj = pool.get('account.period')
    fiscalyear_obj = pool.get('account.fiscalyear')

    # Get the fiscal year
    if form.get('fiscalyear'):
        fiscalyear = fiscalyear_obj.browse(cr, uid, form['fiscalyear'])
    else:
        fiscalyear = fiscalyear_obj.browse(cr, uid, fiscalyear_obj.find(cr, uid))

    #
    # Get the accounts
    #
    child_ids = account_obj._get_children_and_consol(cr, uid, account_ids, context)
    if child_ids:
        account_ids = child_ids

    #
    # Calculate the FY Balance.
    # (from full fiscal year without closing periods)
    #
    ctx = context.copy()
    if form.get('fiscalyear'):
        # Use only the current fiscal year
        ctx['fiscalyear'] = fiscalyear.id
        ctx['periods'] = period_obj.search(cr, uid, [('fiscalyear_id', '=', fiscalyear.id), '|',
                                                     ('special', '=', False),
                                                     ('date_stop', '<', fiscalyear.date_stop)])
    else:
        # Use all the open fiscal years
        open_fiscalyear_ids = fiscalyear_obj.search(cr, uid, [('state', '=', 'draft')])
        ctx['periods'] = period_obj.search(cr, uid, [('fiscalyear_id', 'in', open_fiscalyear_ids), '|',
                                                     ('special', '=', False),
                                                     ('date_stop', '<', fiscalyear.date_stop)])

    fy_balance = {}
    for acc in account_obj.read(cr, uid, account_ids, ['balance'], ctx):
        fy_balance[acc['id']] = acc['balance']

    #
    # Calculate the FY Debit/Credit
    # (from full fiscal year without opening or closing periods)
    #
    ctx = context.copy()
    ctx['fiscalyear'] = fiscalyear.id
    ctx['periods'] = period_obj.search(cr, uid, [('fiscalyear_id', '=', fiscalyear.id),
                                                 ('special', '=', False)])

    fy_debit = {}
    fy_credit = {}

    for acc in account_obj.read(cr, uid, account_ids, ['debit', 'credit', 'balance'], ctx):
        fy_debit[acc['id']] = acc['debit']
        fy_credit[acc['id']] = acc['credit']

    #
    # Calculate the period Debit/Credit
    # (from the selected period or all the non special periods in the fy)
    #
    ctx = context.copy()
    ctx['state'] = form['context'].get('state', 'all')
    ctx['fiscalyear'] = fiscalyear.id
    ctx['periods'] = period_obj.search(cr, uid, [('fiscalyear_id', '=', fiscalyear.id),
                                                 ('special', '=', False)])
    if form['state'] in ['byperiod', 'all']:
        ctx['periods'] = form['periods'][0][2]
    if form['state'] in ['bydate', 'all']:
        ctx['date_from'] = form['date_from']
        ctx['date_to'] = form['date_to']

    accounts = account_obj.read(cr, uid, account_ids, ['type', 'code', 'name', 'debit', 'credit', 'balance', 'parent_id'], ctx)

    #
    # Calculate the period initial Balance
    # (fy balance minus the balance from the start of the selected period
    #  to the end of the year)
    #
    ctx = context.copy()
    ctx['state'] = form['context'].get('state', 'all')
    ctx['fiscalyear'] = fiscalyear.id
    ctx['periods'] = period_obj.search(cr, uid, [('fiscalyear_id', '=', fiscalyear.id), ('special', '=', False)])
    if form['state'] in ['byperiod', 'all']:
        ctx['periods'] = form['periods'][0][2]
        date_start = min([period.date_start for period in period_obj.browse(cr, uid, ctx['periods'])])
        ctx['periods'] = period_obj.search(cr, uid, [('fiscalyear_id', '=', fiscalyear.id),
                                                     ('date_start', '>=', date_start),
                                                     ('special', '=', False)])
    if form['state'] in ['bydate', 'all']:
        ctx['date_from'] = form['date_from']
        ctx['date_to'] = fiscalyear.date_stop

    period_balanceinit = {}
    for acc in account_obj.read(cr, uid, account_ids, ['balance'], ctx):
        period_balanceinit[acc['id']] = fy_balance[acc['id']] - acc['balance']

    #
    # Generate the report lines (checking each account)
    #
    for account in accounts:
        account_id = account['id']

        if account_id in done:
            continue

        done[account_id] = 1

        #
        # Calculate the account level
        #
        parent_id = account['parent_id']
        if parent_id:
            if isinstance(parent_id, tuple):
                parent_id = parent_id[0]
            account_level = accounts_levels.get(parent_id, 0) + 1
        else:
            account_level = level
        accounts_levels[account_id] = account_level

        #
        # Check if we need to include this level
        #
        if not form['display_account_level'] or account_level <= form['display_account_level']:
            #
            # Copy the account values
            #
            res = {
                'id' : account_id,
                'type' : account['type'],
                'code': account['code'],
                'name': account['name'],
                'level': account_level,
                'balanceinit': period_balanceinit[account_id],
                'debit': account['debit'],
                'credit': account['credit'],
                'balance': period_balanceinit[account_id ] +account['balance'],
                'balanceinit_fy': fy_balance[account_id ] -fy_debit[account_id ] +fy_credit[account_id],
                'debit_fy': fy_debit[account_id],
                'credit_fy': fy_credit[account_id],
                'balance_fy': fy_balance[account_id],
                'parent_id': account['parent_id'],
                'bal_type': '',
            }

            #
            # Round the values to zero if needed (-0.000001 ~= 0)
            #
            if abs(res['balance']) < 0.5 * 10**-int(config['price_accuracy']):
                res['balance'] = 0.0
            if abs(res['balance_fy']) < 0.5 * 10**-int(config['price_accuracy']):
                res['balance_fy'] = 0.0
            if abs(res['balanceinit']) < 0.5 * 10**-int(config['price_accuracy']):
                res['balanceinit'] = 0.0
            if abs(res['balanceinit_fy']) < 0.5 * 10**-int(config['price_accuracy']):
                res['balanceinit_fy'] = 0.0

            #
            # Check whether we must include this line in the report or not
            #
            if form['display_account'] == 'bal_mouvement' and account['parent_id']:
                # Include accounts with movements
                if abs(res['balance']) >= 0.5 * 10**-int(config['price_accuracy']) \
                        or abs(res['credit']) >= 0.5 * 10**-int(config['price_accuracy']) \
                        or abs(res['debit']) >= 0.5 * 10**-int(config['price_accuracy']):
                    result_acc.append(res)
            elif form['display_account'] == 'bal_solde' and account['parent_id']:
                # Include accounts with balance
                if abs(res['balance']) >= 0.5 * 10**-int(config['price_accuracy']):
                    result_acc.append(res)
            else:
                # Include all accounts
                result_acc.append(res)

    return result_acc

def parse_date(date):
    return datetime.strptime(date, '%Y-%m-%d').strftime('%d/%m/%Y')


def float_string(my_loat):
    return ('%.2f' % my_loat).replace('.', ',')