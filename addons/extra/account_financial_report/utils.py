import pooler

from datetime import datetime
from tools import config
from tools.translate import _


def get_fiscalyear(cr, form):
    res = []
    if form.has_key('fiscalyear'):
        fisc_id = form['fiscalyear']
        if not fisc_id:
            return ''
        cr.execute("SELECT name FROM account_fiscalyear WHERE id = %s", (int(fisc_id),))
        res = cr.fetchone()
    return res and res[0] or ''

def get_periods(cr, uid, form):
    result = ''
    if form.has_key('periods') and form['periods'][0][2]:
        period_ids = ",".join([str(x) for x in form['periods'][0][2] if x])
        cr.execute("SELECT name FROM account_period WHERE id in (%s)" % (period_ids))
        res = cr.fetchall()
        len_res = len(res)
        for r in res:
            if r == res[len_res - 1]:
                result += r[0] + ". "
            else:
                result += r[0] + ", "
    elif form.has_key('date_from') and form.has_key('date_to'):
        # result = self.formatLang(form['date_from'], date=True) + ' - ' + self.formatLang(form['date_to'], date=True) + ' '
        date_from = parse_date(form['date_from'])
        date_to = parse_date(form['date_to'])
        result = date_from + ' - ' + date_to + ' '
    else:
        fy_obj = pooler.get_pool(cr.db_name).get('account.fiscalyear').browse(cr, uid, form['fiscalyear'])
        res = fy_obj.period_ids
        len_res = len(res)
        for r in res:
            if r == res[len_res - 1]:
                result += r.name + ". "
            else:
                result += r.name + ", "

    return str(result and result[:-1]) or ''

def _calc_contrepartie(cr, uid, ids, context={}):
    result = {}
    # for id in ids:
    #    result.setdefault(id, False)

    for account_line in pooler.get_pool(cr.dbname).get('account.move.line').browse(cr, uid, ids, context):
        # For avoid long text in the field we will limit it to 5 lines
        #
        result[account_line.id] = ' '
        num_id_move = str(account_line.move_id.id)
        num_id_line = str(account_line.id)
        account_id = str(account_line.account_id.id)
        # search the basic account
        # We have the account ID we will search all account move line from now until this time
        # We are in the case of we are on the top of the account move Line
        cr.execute("SELECT distinct(ac.code) as code_rest,ac.name as name_rest " \
                   "FROM account_account AS ac, account_move_line mv " \
                   "WHERE ac.id = mv.account_id and mv.move_id = " + num_id_move + " and mv.account_id <> " + account_id)
        res_mv = cr.dictfetchall()
        # we need a result more than 2 line to make the test so we will made the the on 1 because we have exclude the current line
        if len(res_mv) >= 1:
            concat = ''
            rup_id = 0
            for move_rest in res_mv:
                concat = concat + move_rest['code_rest'] + '|'
                result[account_line.id] = concat
                if rup_id > 5:
                    # we need to stop the computing and to escape but before we will add "..."
                    result[account_line.id] = concat + '...'
                    break
                rup_id += 1
    return result

def lines(self, cr, uid, account, form, context={}):
    pool = pooler.get_pool(cr.dbname)

    inv_types = {
        'out_invoice': _('CI: '),
        'in_invoice': _('SI: '),
        'out_refund': _('OR: '),
        'in_refund': _('SR: '),
    }

    if form['sortbydate'] == 'sort_date':
        sorttag = 'l.date'
    else:
        sorttag = 'j.code'
    sql = """
                SELECT l.id, l.date, j.code, c.code AS currency_code, l.amount_currency, l.ref, l.name , l.debit, l.credit, l.period_id
                        FROM account_move_line as l
                           LEFT JOIN res_currency c on (l.currency_id=c.id)
                              JOIN account_journal j on (l.journal_id=j.id)
                                 AND account_id = %%s
                                 AND %s
                                   ORDER by %s""" % (self.query, sorttag)
    cr.execute(sql % account.id)

    res = cr.dictfetchall()
    move_line_obj = pool.get('account.move.line')
    account_obj = pool.get('account.account')
    invoice_obj = pool.get('account.invoice')

    # Balance from init fiscal year to last date given by the user
    accounts = account_obj.read(cr, uid, [account.id], ['balance'], self.ctxfy)
    sum = accounts[0]['balance']

    for l in reversed(res):
        line = move_line_obj.browse(cr, uid, l['id'])
        l['move'] = line.move_id.name
        cr.execute('Select id from account_invoice where move_id =%s' % (line.move_id.id))
        tmpres = cr.dictfetchall()
        if len(tmpres) > 0:
            inv = invoice_obj.browse(cr, uid, tmpres[0]['id'])
            l['ref'] = inv_types[inv.type] + ': ' + str(inv.number)
        if line.partner_id:
            l['partner'] = line.partner_id.name
        else:
            l['partner'] = ''
        l['line_corresp'] = _calc_contrepartie(cr, uid, [l['id']])[l['id']]

        # Cumulative balance update
        l['progress'] = sum
        sum = sum - l['debit'] + l['credit']

        # Modification of currency amount
        if (l['credit'] > 0):
            if l['amount_currency'] != None:
                l['amount_currency'] = abs(l['amount_currency']) * -1
        if l['amount_currency'] != None:
            self.tot_currency = self.tot_currency + l['amount_currency']

    if abs(sum) > 10 ** -int(config['price_accuracy']) and form['initial_balance']:
        res.insert(0, {
            'date': self.min_date,
            'name': _('Initial balance'),
            'progress': sum,
            'partner': '',
            'move': '',
            'ref': '',
            'debit': '',
            'credit': '',
            'amount_currency': '',
            'currency_code': '',
            'code': '',
            'line_corresp': '',
        })

    return res

def get_children_accounts(self, cr, uid, ids,  account, form):
    pool = pooler.get_pool(cr.dbname)
    move_line_obj = pool.get('account.move.line')
    account_obj = pool.get('account.account')
    invoice_obj = pool.get('account.invoice')
    child_ids = account_obj.search(cr, uid, [('parent_id', 'child_of', ids)])

    res = []
    ctx = self.ctx.copy()
    if account and account.child_consol_ids: # add ids of consolidated childs also of selected account
        ctx['consolidate_childs'] = True
        ctx['account_id'] = account.id
    ids_acc = account_obj.search(cr, uid,[('parent_id', 'child_of', [account.id])], context=ctx)
    for child_id in ids_acc:
        child_account = account_obj.browse(cr, uid, child_id)
        balance_account = _sum_balance_account(self, cr, uid, child_account,form)
        self.balance_accounts[child_account.id] = balance_account
        if form['display_account'] == 'bal_mouvement':
            if child_account.type != 'view' \
            and len(move_line_obj.search(cr, uid,
                [('account_id','=',child_account.id)],
                context=ctx)) <> 0 :
                res.append(child_account)
        elif form['display_account'] == 'bal_solde':
            if child_account.type != 'view' \
            and len(move_line_obj.search(cr, uid,
                [('account_id','=',child_account.id)],
                context=ctx)) <> 0 :
                if balance_account <> 0.0:
                    res.append(child_account)
        else:
            if child_account.type != 'view' \
            and len(move_line_obj.search(cr, uid,
                [('account_id','>=',child_account.id)],
                context=ctx)) <> 0 :
                res.append(child_account)
    ##
    if not len(res):
        return [account]
    else:
        ## We will now compute initial balance
        for move in res:
            sql_balance_init = "SELECT sum(l.debit) AS sum_debit, sum(l.credit) AS sum_credit "\
                "FROM account_move_line l "\
                "WHERE l.account_id = " + str(move.id) +  " AND %s" % (self.query)
            cr.execute(sql_balance_init)
            resultat = cr.dictfetchall()
            if resultat[0] :
                if resultat[0]['sum_debit'] == None:
                    sum_debit = 0
                else:
                    sum_debit = resultat[0]['sum_debit']
                if resultat[0]['sum_credit'] == None:
                    sum_credit = 0
                else:
                    sum_credit = resultat[0]['sum_credit']

                move.init_credit = sum_credit
                move.init_debit = sum_debit
            else:
                move.init_credit = 0
                move.init_debit = 0
    return res

def _sum_debit_account(self, cr, account, form):
    cr.execute("SELECT sum(debit) " \
                    "FROM account_move_line l " \
                    "WHERE l.account_id = %s AND %s " % (account.id, self.query))
    sum_debit = cr.fetchone()[0] or 0.0
    return sum_debit

def _sum_credit_account(self, cr, account, form):
    cr.execute("SELECT sum(credit) " \
                    "FROM account_move_line l " \
                    "WHERE l.account_id = %s AND %s " % (account.id, self.query))
    sum_credit = cr.fetchone()[0] or 0.0
    return sum_credit

def _sum_balance_account(self, cr, uid, account, form):
    # Balance from init fiscal year to last date given by the user
    accounts = pooler.get_pool(cr.dbname).get('account.account').read(cr, uid, [account.id], ['balance'], self.ctxfy)
    sum_balance = accounts[0]['balance']
    return sum_balance

def _set_get_account_currency_code(self, cr, account_id):
    cr.execute("SELECT c.code as code " \
                    "FROM res_currency c, account_account as ac " \
                    "WHERE ac.id = %s AND ac.currency_id = c.id" % (account_id))
    result = cr.fetchone()
    if result:
        self.account_currency = result[0]
    else:
        self.account_currency = False

def _sum_currency_amount_account(self, cr, account, form):
    _set_get_account_currency_code(self, cr, account.id)
    cr.execute("SELECT sum(l.amount_currency) " \
                    "FROM account_move_line as l, res_currency as rc " \
                    "WHERE l.currency_id = rc.id AND l.account_id= %s AND %s" % (account.id, self.query))
    total = cr.fetchone()
    if self.account_currency:
        return_field = str(total[0]) + self.account_currency
        return return_field
    else:
        currency_total = self.tot_currency = 0.0
        return currency_total

def get_context_date_period(self, cr, uid, form, context={}):
    date_min = period_min = False

    # ctx: Context for the given date or period
    ctx = context.copy()
    ctx['state'] = form['context'].get('state','all')
    if 'fiscalyear' in form and form['fiscalyear']:
        ctx['fiscalyear'] = form['fiscalyear']
    if form['state'] in ['byperiod', 'all']:
        ctx['periods'] = form['periods'][0][2]
    if form['state'] in ['bydate', 'all']:
        ctx['date_from'] = form['date_from']
        ctx['date_to'] = form['date_to']
    if 'periods' not in ctx:
        ctx['periods'] = []
    self.ctx = ctx
    self.query = pooler.get_pool(cr.dbname).get('account.move.line')._query_get(cr, uid, context=ctx)

    # ctxfy: Context from the date start / first period of the fiscal year
    ctxfy = ctx.copy()
    ctxfy['periods'] = ctx['periods'][:]

    if form['state'] in ['byperiod', 'all'] and len(ctx['periods']):
        cr.execute("""SELECT id, date_start, fiscalyear_id
            FROM account_period
            WHERE date_start = (SELECT min(date_start) FROM account_period WHERE id in (%s))"""
            % (','.join([str(x) for x in ctx['periods']])))
        res = cr.dictfetchone()
        period_min = res['date_start']
        cr.execute("""SELECT id
            FROM account_period
            WHERE fiscalyear_id in (%s) AND date_start < '%s'"""
            % (res['fiscalyear_id'], res['date_start']))
        ids = filter(None, map(lambda x:x[0], cr.fetchall()))
        ctxfy['periods'].extend(ids)

    if form['state'] in ['bydate', 'all']:
        cr.execute("""SELECT date_start
            FROM account_fiscalyear
            WHERE '%s' BETWEEN date_start AND date_stop""" % (ctx['date_from']))
        res = cr.dictfetchone()
        ctxfy['date_from'] = res['date_start']
        date_min = form['date_from']

    if form['state'] == 'none' or (form['state'] == 'byperiod' and not len(ctx['periods'])):
        if 'fiscalyear' in form and form['fiscalyear']:
            sql = """SELECT id, date_start
                FROM account_period
                WHERE fiscalyear_id in (%s)
                ORDER BY date_start""" % (ctx['fiscalyear'])
        else:
            sql = """SELECT id, date_start
                FROM account_period
                WHERE fiscalyear_id in (SELECT id FROM account_fiscalyear WHERE state='draft')
                ORDER BY date_start"""
        cr.execute(sql)
        res = cr.dictfetchall()
        period_min = res[0]['date_start']
        ids = filter(None, map(lambda x:x['id'], res))
        ctxfy['periods'] = ids
    self.ctxfy = ctxfy

    if not period_min:
        self.min_date = date_min
    elif not date_min:
        self.min_date = period_min
    else:
        # If period and date are given, the maximum of the min dates is choosed
        if period_min < date_min:
            self.min_date = date_min
        else:
            self.min_date = period_min


def parse_date(date):
    return datetime.strptime(date, '%Y-%m-%d').strftime('%d/%m/%Y')


def float_string(my_loat):
    return ('%.2f' % my_loat).replace('.', ',')
