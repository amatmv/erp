# Translation of OpenERP Server.
# This file contains the translation of the following modules:
# * account_financial_report
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.14\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2019-02-26 05:27+0000\n"
"PO-Revision-Date: 2019-02-26 05:27+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: \n"
"Generated-By: Babel 2.5.3\n"

#. module: account_financial_report
#: rml:account.account.chart.report:0
msgid "Account number"
msgstr ""

#. module: account_financial_report
#: rml:account.invoice.list.report:0
msgid "Customer Refund Invoices"
msgstr ""

#. module: account_financial_report
#: rml:account.move.line.report:0
msgid "First line"
msgstr ""

#. module: account_financial_report
#: rml:account.invoice.list.report:0
msgid "Fiscal position"
msgstr ""

#. module: account_financial_report
#: code:addons/account_financial_report/wizard/wizard_account_balance_report.py:112
#: code:addons/account_financial_report/wizard/wizard_general_ledger_report.py:134
#, python-format
msgid "Date to must be set between %s and %s"
msgstr ""

#. module: account_financial_report
#: rml:account.invoice.list.report:0
msgid "Vat"
msgstr ""

#. module: account_financial_report
#: model:ir.actions.report.xml,name:account_financial_report.report_account_move_line
msgid "Entry lines"
msgstr ""

#. module: account_financial_report
#: rml:account.move.report:0
#: rml:account.move.report.h:0
#: rml:account.print.journal.entries:0
#: rml:account.print.journal.entriesh:0
msgid "Own style"
msgstr ""

#. module: account_financial_report
#: rml:account.invoice.list.report:0
msgid "<drawRightString x=\"19.8cm\" y=\"25.50cm\">"
msgstr ""

#. module: account_financial_report
#: rml:account.account.chart.report:0
#: rml:account.move.report:0
#: rml:account.move.report.h:0
#: rml:account.print.journal.entries:0
#: rml:account.print.journal.entriesh:0
msgid "Account name"
msgstr ""

#. module: account_financial_report
#: rml:account.move.line.report:0
#: rml:account.move.report:0
#: rml:account.move.report.h:0
#: rml:account.print.journal.entries:0
#: rml:account.print.journal.entriesh:0
msgid "Table header"
msgstr ""

#. module: account_financial_report
#: rml:account.move.line.report:0
msgid "Analitic account"
msgstr ""

#. module: account_financial_report
#: rml:account.balance.full:0
#: rml:account.move.line.report:0
#: rml:account.move.report:0
#: rml:account.move.report.h:0
#: rml:account.print.journal.entries:0
#: rml:account.print.journal.entriesh:0
msgid "Account"
msgstr ""

#. module: account_financial_report
#: rml:account.move.line.report:0
msgid "Second line"
msgstr ""

#. module: account_financial_report
#: code:addons/account_financial_report/report/general_ledger.py:290
#: code:addons/account_financial_report/report/general_ledger_landscape.py:291
msgid "CI: "
msgstr ""

#. module: account_financial_report
#: model:ir.actions.report.xml,name:account_financial_report.account_invoice_list_report
msgid "Invoice list"
msgstr ""

#. module: account_financial_report
#: rml:account.move.line.report:0
msgid "Own styles"
msgstr ""

#. module: account_financial_report
#: rml:account.invoice.list.report:0
msgid "Taxes"
msgstr ""

#. module: account_financial_report
#: rml:account.general.ledger.cumulative:0
msgid "Ref - Entry Label"
msgstr ""

#. module: account_financial_report
#: rml:account.invoice.list.report:0
msgid "Cancelled"
msgstr ""

#. module: account_financial_report
#: model:ir.actions.wizard,name:account_financial_report.wizard_account_general_ledger_cumulative_report
msgid "Cum. general ledger"
msgstr ""

#. module: account_financial_report
#: rml:account.move.line.report:0
msgid "Entry Lines  -"
msgstr ""

#. module: account_financial_report
#: rml:account.move.line.report:0
msgid "Table styles"
msgstr ""

#. module: account_financial_report
#: code:addons/account_financial_report/wizard/wizard_account_balance_report.py:112
#: code:addons/account_financial_report/wizard/wizard_account_balance_report.py:116
#: code:addons/account_financial_report/wizard/wizard_general_ledger_report.py:134
#: code:addons/account_financial_report/wizard/wizard_general_ledger_report.py:138
msgid "UserError"
msgstr ""

#. module: account_financial_report
#: model:ir.module.module,description:account_financial_report.module_meta_information
msgid ""
"\n"
"Add some common financial/accounting reports and some wizards to quickly "
"compute them:\n"
"  * Account chart list\n"
"  * Invoice list\n"
"  * Account move (journal ledger)\n"
"  * Account move line\n"
"  * Account balance compared period-fiscal year\n"
"  * Cumulative general ledger\n"
"\n"
"They can be found in the \"Financial Management/Legal Statements/Generic "
"Reports\" menu or in the tree/form views of accounts, journals, invoices,"
" account entries and account move lines.\n"
"\n"
"Some reports are based on previous work by Pexego and others on the "
"c2c_finance_report module for TinyERP 4.2 by Camptocamp SA.\n"
msgstr ""

#. module: account_financial_report
#: rml:account.move.line.report:0
#: rml:account.move.report:0
#: rml:account.move.report.h:0
#: rml:account.print.journal.entries:0
#: rml:account.print.journal.entriesh:0
msgid "<drawString x=\"1cm\" y=\"0.9cm\">"
msgstr ""

#. module: account_financial_report
#: rml:account.account.chart.report:0
#: rml:account.general.ledger.cumulative:0
#: rml:account.general.ledger.cumulative.landscape:0
#: rml:account.invoice.list.report:0
msgid "<fill color=\"darkblue\"/>"
msgstr ""

#. module: account_financial_report
#: rml:account.move.report:0
#: rml:account.move.report.h:0
#: rml:account.print.journal.entries:0
#: rml:account.print.journal.entriesh:0
msgid "Reference"
msgstr ""

#. module: account_financial_report
#: code:addons/account_financial_report/wizard/wizard_account_balance_report.py:116
#: code:addons/account_financial_report/wizard/wizard_general_ledger_report.py:138
msgid "Date not in a defined fiscal year"
msgstr ""

#. module: account_financial_report
#: model:ir.actions.report.xml,name:account_financial_report.report_account_move_line_record_h
msgid "Entry landscape"
msgstr ""

#. module: account_financial_report
#: rml:account.account.chart.report:0
#: rml:account.general.ledger.cumulative:0
#: rml:account.general.ledger.cumulative.landscape:0
#: rml:account.invoice.list.report:0
msgid "<stroke color=\"darkblue\"/>"
msgstr ""

#. module: account_financial_report
#: rml:account.account.chart.report:0
msgid "Account Chart"
msgstr ""

#. module: account_financial_report
#: rml:account.invoice.list.report:0
msgid "Untaxed"
msgstr ""

#. module: account_financial_report
#: rml:account.move.report.h:0
#: rml:account.print.journal.entriesh:0
msgid "To check"
msgstr ""

#. module: account_financial_report
#: rml:account.general.ledger.cumulative:0
#: rml:account.general.ledger.cumulative.landscape:0
#: rml:account.invoice.list.report:0
#: rml:account.move.line.report:0
#: rml:account.move.report:0
#: rml:account.move.report.h:0
#: rml:account.print.journal.entries:0
#: rml:account.print.journal.entriesh:0
msgid "Partner"
msgstr ""

#. module: account_financial_report
#: rml:account.move.report.h:0
#: rml:account.print.journal.entriesh:0
msgid "YES"
msgstr ""

#. module: account_financial_report
#: rml:account.move.report:0
#: rml:account.move.report.h:0
#: rml:account.print.journal.entries:0
#: rml:account.print.journal.entriesh:0
msgid "Table style"
msgstr ""

#. module: account_financial_report
#: rml:account.balance.full:0
#: rml:account.general.ledger.cumulative:0
#: rml:account.general.ledger.cumulative.landscape:0
msgid "Periods/Date range:"
msgstr ""

#. module: account_financial_report
#: rml:account.invoice.list.report:0
msgid "<td>                             <para style=\"Text body\">"
msgstr ""

#. module: account_financial_report
#: code:addons/account_financial_report/report/general_ledger.py:347
#: code:addons/account_financial_report/report/general_ledger_landscape.py:348
msgid "Initial balance"
msgstr ""

#. module: account_financial_report
#: rml:account.invoice.list.report:0
#: rml:account.move.line.report:0
#: rml:account.move.report.h:0
#: rml:account.print.journal.entriesh:0
msgid "State"
msgstr ""

#. module: account_financial_report
#: rml:account.move.line.report:0
msgid "Valid"
msgstr ""

#. module: account_financial_report
#: rml:account.balance.full:0
#: rml:account.general.ledger.cumulative:0
#: rml:account.general.ledger.cumulative.landscape:0
#: rml:account.move.line.report:0
#: rml:account.move.report:0
#: rml:account.move.report.h:0
#: rml:account.print.journal.entries:0
#: rml:account.print.journal.entriesh:0
msgid "Debit"
msgstr ""

#. module: account_financial_report
#: rml:account.account.chart.report:0
#: rml:account.general.ledger.cumulative:0
#: rml:account.general.ledger.cumulative.landscape:0
#: rml:account.invoice.list.report:0
msgid "logo"
msgstr ""

#. module: account_financial_report
#: rml:account.general.ledger.cumulative.landscape:0
msgid "Ref"
msgstr ""

#. module: account_financial_report
#: rml:account.balance.full:0
#: rml:account.general.ledger.cumulative:0
#: rml:account.general.ledger.cumulative.landscape:0
msgid "Fiscal year:"
msgstr ""

#. module: account_financial_report
#: rml:account.balance.full:0
msgid "Fiscal year"
msgstr ""

#. module: account_financial_report
#: rml:account.invoice.list.report:0
msgid ""
"<td>                             <para style=\"Standard\" "
"size=\"5\">Invoice description</para>                         </td>"
msgstr ""

#. module: account_financial_report
#: rml:account.balance.full:0
msgid "Account Balance -"
msgstr ""

#. module: account_financial_report
#: model:ir.actions.report.xml,name:account_financial_report.account_account_chart_report
#: model:ir.actions.wizard,name:account_financial_report.wizard_account_chart_report
#: model:ir.ui.menu,name:account_financial_report.menu_account_chart_report
msgid "Chart of accounts"
msgstr ""

#. module: account_financial_report
#: rml:account.invoice.list.report:0
msgid ""
"<blockTable "
"colWidths=\"1.8cm,1.4cm,1.4cm,1.8cm,1.8cm,2cm,3.5cm,2cm,4cm,1.2cm,1.8cm,1.4cm,1.8cm,1.6cm\""
" repeatRows=\"1\" style=\"Table3\">"
msgstr ""

#. module: account_financial_report
#: rml:account.general.ledger.cumulative:0
#: rml:account.general.ledger.cumulative.landscape:0
msgid "Mvt"
msgstr ""

#. module: account_financial_report
#: rml:account.account.chart.report:0
msgid "1cm 27.9cm 20cm 27.9cm"
msgstr ""

#. module: account_financial_report
#: rml:account.general.ledger.cumulative.landscape:0
msgid "Entry Label"
msgstr ""

#. module: account_financial_report
#: model:ir.actions.report.xml,name:account_financial_report.account_general_ledger_cumulative_landscape
msgid "Cumulative general ledger landscape"
msgstr ""

#. module: account_financial_report
#: rml:account.general.ledger.cumulative.landscape:0
msgid "Counterpart"
msgstr ""

#. module: account_financial_report
#: code:addons/account_financial_report/wizard/wizard_print_journal_entries.py:76
msgid "No Data Available"
msgstr ""

#. module: account_financial_report
#: rml:account.invoice.list.report:0
msgid "Due date"
msgstr ""

#. module: account_financial_report
#: rml:account.invoice.list.report:0
msgid "Pro-forma"
msgstr ""

#. module: account_financial_report
#: code:addons/account_financial_report/report/general_ledger.py:292
#: code:addons/account_financial_report/report/general_ledger_landscape.py:293
msgid "OR: "
msgstr ""

#. module: account_financial_report
#: rml:account.invoice.list.report:0
msgid "Partner code"
msgstr ""

#. module: account_financial_report
#: rml:account.move.line.report:0
#: rml:account.move.report:0
#: rml:account.move.report.h:0
#: rml:account.print.journal.entries:0
#: rml:account.print.journal.entriesh:0
msgid "Report title"
msgstr ""

#. module: account_financial_report
#: rml:account.invoice.list.report:0
msgid "External ref."
msgstr ""

#. module: account_financial_report
#: rml:account.invoice.list.report:0
#: rml:account.move.line.report:0
#: rml:account.move.report.h:0
#: rml:account.print.journal.entriesh:0
msgid "Draft"
msgstr ""

#. module: account_financial_report
#: rml:account.account.chart.report:0
#: rml:account.general.ledger.cumulative:0
#: rml:account.general.ledger.cumulative.landscape:0
msgid "TITLE COMPANY"
msgstr ""

#. module: account_financial_report
#: rml:account.invoice.list.report:0
msgid "</para>                         </td>"
msgstr ""

#. module: account_financial_report
#: model:ir.actions.report.xml,name:account_financial_report.account_balance_full
#: model:ir.actions.wizard,name:account_financial_report.wizard_account_balance_full_report
#: model:ir.actions.wizard,name:account_financial_report.wizard_account_balance_full_report_menu
#: model:ir.ui.menu,name:account_financial_report.menu_account_balance_full_report
msgid "Full account balance"
msgstr ""

#. module: account_financial_report
#: rml:account.account.chart.report:0
#: rml:account.general.ledger.cumulative:0
#: rml:account.general.ledger.cumulative.landscape:0
msgid "<drawRightString x=\"19.8cm\" y=\"28cm\">"
msgstr ""

#. module: account_financial_report
#: rml:account.move.report.h:0
#: rml:account.print.journal.entriesh:0
msgid "NO"
msgstr ""

#. module: account_financial_report
#: rml:account.invoice.list.report:0
msgid "Paid"
msgstr ""

#. module: account_financial_report
#: rml:account.balance.full:0
msgid "Init. balance"
msgstr ""

#. module: account_financial_report
#: rml:account.general.ledger.cumulative:0
#: rml:account.general.ledger.cumulative.landscape:0
#: rml:account.move.line.report:0
#: rml:account.move.report:0
#: rml:account.move.report.h:0
#: rml:account.print.journal.entries:0
#: rml:account.print.journal.entriesh:0
msgid "Date"
msgstr ""

#. module: account_financial_report
#: rml:account.general.ledger.cumulative.landscape:0
msgid "1.3cm 24.9cm 38.3cm 24.9cm"
msgstr ""

#. module: account_financial_report
#: rml:account.move.line.report:0
#: rml:account.move.report:0
#: rml:account.move.report.h:0
#: rml:account.print.journal.entries:0
#: rml:account.print.journal.entriesh:0
msgid "Footer"
msgstr ""

#. module: account_financial_report
#: rml:account.move.line.report:0
#: rml:account.move.report:0
#: rml:account.move.report.h:0
#: rml:account.print.journal.entries:0
#: rml:account.print.journal.entriesh:0
msgid "Table content"
msgstr ""

#. module: account_financial_report
#: rml:account.balance.full:0
#: rml:account.general.ledger.cumulative:0
#: rml:account.general.ledger.cumulative.landscape:0
#: rml:account.move.line.report:0
#: rml:account.move.report:0
#: rml:account.move.report.h:0
#: rml:account.print.journal.entries:0
#: rml:account.print.journal.entriesh:0
msgid "Credit"
msgstr ""

#. module: account_financial_report
#: rml:account.account.chart.report:0
#: rml:account.balance.full:0
#: rml:account.general.ledger.cumulative:0
#: rml:account.general.ledger.cumulative.landscape:0
#: rml:account.invoice.list.report:0
msgid "COL 2"
msgstr ""

#. module: account_financial_report
#: rml:account.account.chart.report:0
#: rml:account.balance.full:0
#: rml:account.general.ledger.cumulative:0
#: rml:account.general.ledger.cumulative.landscape:0
#: rml:account.invoice.list.report:0
msgid "COL 1"
msgstr ""

#. module: account_financial_report
#: rml:account.balance.full:0
#: rml:account.general.ledger.cumulative:0
#: rml:account.general.ledger.cumulative.landscape:0
msgid "Balance"
msgstr ""

#. module: account_financial_report
#: rml:account.account.chart.report:0
#: rml:account.general.ledger.cumulative:0
#: rml:account.general.ledger.cumulative.landscape:0
msgid "<drawString x=\"4.6cm\" y=\"28.7cm\">"
msgstr ""

#. module: account_financial_report
#: rml:account.balance.full:0
msgid "Code"
msgstr ""

#. module: account_financial_report
#: rml:account.invoice.list.report:0
msgid "Entry num."
msgstr ""

#. module: account_financial_report
#: rml:account.invoice.list.report:0
msgid "Customer Invoices"
msgstr ""

#. module: account_financial_report
#: rml:account.invoice.list.report:0
msgid "Number"
msgstr ""

#. module: account_financial_report
#: rml:account.account.chart.report:0
#: rml:account.general.ledger.cumulative:0
#: rml:account.general.ledger.cumulative.landscape:0
#: rml:account.invoice.list.report:0
msgid "</drawRightString>"
msgstr ""

#. module: account_financial_report
#: rml:account.move.report:0
#: rml:account.move.report.h:0
#: rml:account.print.journal.entries:0
#: rml:account.print.journal.entriesh:0
msgid "Subtable style"
msgstr ""

#. module: account_financial_report
#: rml:account.invoice.list.report:0
msgid "Open"
msgstr ""

#. module: account_financial_report
#: model:ir.module.module,shortdesc:account_financial_report.module_meta_information
msgid "Common financial reports"
msgstr ""

#. module: account_financial_report
#: model:ir.actions.report.xml,name:account_financial_report.account_print_journal_entries
#: model:ir.actions.wizard,name:account_financial_report.wizard_journal_entries_report
#: model:ir.ui.menu,name:account_financial_report.menu_journal_entries_report
msgid "Print journal by entries"
msgstr ""

#. module: account_financial_report
#: rml:account.balance.full:0
#: rml:account.move.report:0
#: rml:account.move.report.h:0
#: rml:account.print.journal.entries:0
#: rml:account.print.journal.entriesh:0
msgid "Period"
msgstr ""

#. module: account_financial_report
#: code:addons/account_financial_report/report/general_ledger.py:291
#: code:addons/account_financial_report/report/general_ledger_landscape.py:292
msgid "SI: "
msgstr ""

#. module: account_financial_report
#: rml:account.invoice.list.report:0
msgid "<td>                                 <para style=\"Text body\">"
msgstr ""

#. module: account_financial_report
#: rml:account.invoice.list.report:0
msgid "Supplier Invoices"
msgstr ""

#. module: account_financial_report
#: rml:account.move.line.report:0
#: rml:account.move.report:0
#: rml:account.move.report.h:0
#: rml:account.print.journal.entries:0
#: rml:account.print.journal.entriesh:0
msgid "Description"
msgstr ""

#. module: account_financial_report
#: rml:account.general.ledger.cumulative:0
#: rml:account.general.ledger.cumulative.landscape:0
msgid "Currency"
msgstr ""

#. module: account_financial_report
#: rml:account.move.report:0
#: rml:account.move.report.h:0
#: rml:account.print.journal.entries:0
#: rml:account.print.journal.entriesh:0
msgid "Journal"
msgstr ""

#. module: account_financial_report
#: rml:account.invoice.list.report:0
msgid "Invoice List"
msgstr ""

#. module: account_financial_report
#: rml:account.invoice.list.report:0
msgid "Supplier Refund Invoices"
msgstr ""

#. module: account_financial_report
#: rml:account.move.line.report:0
msgid "Ref."
msgstr ""

#. module: account_financial_report
#: rml:account.move.report.h:0
#: rml:account.print.journal.entriesh:0
msgid "Posted"
msgstr ""

#. module: account_financial_report
#: rml:account.move.report:0
#: rml:account.move.report.h:0
#: rml:account.print.journal.entries:0
#: rml:account.print.journal.entriesh:0
msgid "Journal Ledger  -"
msgstr ""

#. module: account_financial_report
#: rml:account.invoice.list.report:0
msgid "</para>                             </td>"
msgstr ""

#. module: account_financial_report
#: rml:account.account.chart.report:0
#: rml:account.general.ledger.cumulative:0
#: rml:account.general.ledger.cumulative.landscape:0
#: rml:account.move.line.report:0
#: rml:account.move.report:0
#: rml:account.move.report.h:0
#: rml:account.print.journal.entries:0
#: rml:account.print.journal.entriesh:0
msgid "</drawString>"
msgstr ""

#. module: account_financial_report
#: rml:account.invoice.list.report:0
msgid "Invoice date"
msgstr ""

#. module: account_financial_report
#: rml:account.move.line.report:0
msgid "Data table"
msgstr ""

#. module: account_financial_report
#: rml:account.invoice.list.report:0
msgid "0.8cm 19.8cm 28.8cm 19.8cm"
msgstr ""

#. module: account_financial_report
#: rml:account.invoice.list.report:0
msgid "Currency :"
msgstr ""

#. module: account_financial_report
#: model:ir.actions.report.xml,name:account_financial_report.report_account_move_line_record
#: rml:account.move.line.report:0
#: rml:account.move.report:0
#: rml:account.move.report.h:0
#: rml:account.print.journal.entries:0
#: rml:account.print.journal.entriesh:0
msgid "Entry"
msgstr ""

#. module: account_financial_report
#: rml:account.general.ledger.cumulative:0
msgid "0.9cm 27.7cm 20.1cm 27.7cm"
msgstr ""

#. module: account_financial_report
#: rml:account.account.chart.report:0
#: rml:account.balance.full:0
#: rml:account.general.ledger.cumulative:0
#: rml:account.general.ledger.cumulative.landscape:0
#: rml:account.invoice.list.report:0
#: rml:account.move.line.report:0
#: rml:account.move.report:0
#: rml:account.move.report.h:0
#: rml:account.print.journal.entries:0
#: rml:account.print.journal.entriesh:0
msgid "Page"
msgstr ""

#. module: account_financial_report
#: model:ir.actions.report.xml,name:account_financial_report.account_print_journal_entriesh
msgid "Print journal by entries landscape"
msgstr ""

#. module: account_financial_report
#: code:addons/account_financial_report/wizard/wizard_print_journal_entries.py:76
msgid "No records found for your selection!"
msgstr ""

#. module: account_financial_report
#: model:ir.actions.wizard,name:account_financial_report.wizard_invoice_list_report
#: model:ir.ui.menu,name:account_financial_report.menu_invoice_list_report
msgid "Print invoice list"
msgstr ""

#. module: account_financial_report
#: rml:account.general.ledger.cumulative.landscape:0
msgid "JNRL"
msgstr ""

#. module: account_financial_report
#: rml:account.balance.full:0
msgid "1cm 19.2cm 28.7cm 19.2cm"
msgstr ""

#. module: account_financial_report
#: model:ir.actions.report.xml,name:account_financial_report.account_general_ledger_cumulative
#: model:ir.actions.wizard,name:account_financial_report.wizard_account_general_ledger_cumulative_report_menu
#: model:ir.ui.menu,name:account_financial_report.menu_account_general_ledger_cumulative_report
msgid "Cumulative general ledger"
msgstr ""

#. module: account_financial_report
#: rml:account.general.ledger.cumulative:0
#: rml:account.general.ledger.cumulative.landscape:0
msgid "General Ledger -"
msgstr ""

#. module: account_financial_report
#: rml:account.general.ledger.cumulative:0
#: rml:account.general.ledger.cumulative.landscape:0
#: rml:account.invoice.list.report:0
msgid "Total"
msgstr ""

#. module: account_financial_report
#: code:addons/account_financial_report/report/general_ledger.py:293
#: code:addons/account_financial_report/report/general_ledger_landscape.py:294
msgid "SR: "
msgstr ""

