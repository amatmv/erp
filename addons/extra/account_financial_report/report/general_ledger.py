# -*- encoding: utf-8 -*-
##############################################################################
#
#    Copyright (c) 2005-2006 CamptoCamp
#    Copyright (c) 2009 Zikzakmedia S.L. (http://zikzakmedia.com) All Rights Reserved.
#                       Jordi Esteve <jesteve@zikzakmedia.com>
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsability of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# garantees and support are strongly adviced to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

import time
from account_financial_report.utils import general_ledger_utils as utils
from report import report_sxw
import rml_parse


class general_ledger(rml_parse.rml_parse):
    _name = 'report.account.general.ledger.cumulative'

    def set_context(self, objects, data, ids, report_type = None):
        self.get_context_date_period(data['form'])
        new_ids = []
        if (data['model'] == 'account.account'):
            new_ids = ids
        else:
            new_ids = data['form']['account_list'][0][2]
            objects = self.pool.get('account.account').browse(self.cr, self.uid, new_ids)
        super(general_ledger, self).set_context(objects, data, new_ids, report_type)


    def __init__(self, cr, uid, name, context):
        super(general_ledger, self).__init__(cr, uid, name, context=context)
        self.query = ""    # SQL query to get account moves for given date or period
        self.min_date = "" # Min date of the given date or period
        self.ctx = {}      # Context for given date or period
        self.ctxfy = {}    # Context from the date start or first period of the fiscal year
        self.child_ids = ""
        self.tot_currency = 0.0
        self.balance_accounts = {}
        self.localcontext.update( {
            'time': time,
            'lines': self.lines,
            'sum_debit_account': self._sum_debit_account,
            'sum_credit_account': self._sum_credit_account,
            'sum_balance_account': self._sum_balance_account,
            'get_children_accounts': self.get_children_accounts,
            'sum_currency_amount_account': self._sum_currency_amount_account,
            'get_fiscalyear':self.get_fiscalyear,
            'get_periods':self.get_periods,
        })
        self.context = context


    def get_fiscalyear(self, form):
        return utils.get_fiscalyear(self.cr, form)


    def get_periods(self, form):
        return utils.get_periods(self.cr, self.uid, form)


    def _calc_contrepartie(self, cr, uid, ids, context={}):
        return utils._calc_contrepartie(cr, uid, ids, context=context)


    def get_context_date_period(self, form):
        utils.get_context_date_period(self, self.cr, self.uid, form, context={})

    def get_children_accounts(self, account, form):
        return utils.get_children_accounts(self, self.cr, self.uid, self.ids,  account, form)


    def lines(self, account, form):
        return utils.lines(self, self.cr, self.uid, account, form, context=self.context)

    def _sum_debit_account(self, account, form):
        return utils._sum_debit_account(self, self.cr, account, form)


    def _sum_credit_account(self, account, form):
        return utils._sum_credit_account(self, self.cr, account, form)


    def _sum_balance_account(self, account, form):
        return utils._sum_balance_account(self, self.cr, self.uid, account, form)


    def _set_get_account_currency_code(self, account_id):
        utils._set_get_account_currency_code(self, self.cr, account_id)


    def _sum_currency_amount_account(self, account, form):
        return utils._sum_currency_amount_account(self, self.cr, account, form)

report_sxw.report_sxw('report.account.general.ledger.cumulative', 'account.account', 'addons/account_financial_report/report/general_ledger.rml', parser=general_ledger, header=False)
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
