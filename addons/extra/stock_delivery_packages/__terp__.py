{
	"name" : "Stock Delivery Packages",
	"version" : "0.1",
	"description" : """This module adds a new field to outgoing pickings to store the number of packges to be delivered.""",
	"author" : "NaN",
	"website" : "http://www.NaN-tic.com",
	"depends" : [ 
		'delivery',
	],
	"category" : "Custom Modules",
	"init_xml" : [],
	"demo_xml" : [],
	"update_xml" : [ 
		'stock_view.xml',
	],
	"active": False,
	"installable": True
}
