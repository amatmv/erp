{
	"name":"Board for Hotel Restaurant users",
	"version":"1.0",
	"author":"Tiny",
	"category":"Board/Hotel Restaurant",
	"depends":[
		"board",
		"report_hotel_restaurant"
		
	],
	"demo_xml":[],
	"update_xml":["board_restaurant_view.xml"],
	"description": """
This module implements a dashboard for project member that includes:
	* list of current orders
	* List of Kitchen order tickets 
	* List of current table reservation
	
	""",
	"active":False,
	"installable":True,
}
