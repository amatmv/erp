# -*- coding: utf-8 -*-

from destral import testing
from destral.transaction import Transaction
from expects import expect, be_empty


class TestsAccountChartUpdate(testing.OOTestCase):

    def test_wizard_copies_field_based_on(self):
        tax_obj = self.openerp.pool.get('account.tax')
        tax_template_obj = self.openerp.pool.get('account.tax.template')
        update_tax_wiz = self.openerp.pool.get('wizard.update.charts.accounts')
        imd_obj = self.openerp.pool.get('ir.model.data')

        self.openerp.install_module('l10n_chart_ES')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            
            tax_template = imd_obj.get_object_reference(
                cursor, uid, 'account', 'demo_tax_template_01'
            )[1]

            # We create another template with the field_based_on quantity to
            # test if the wizard correctly copies the field
            from datetime import datetime
            semi_random_name = '{}'.format(datetime.now())

            tax_template_obj.copy(
                cursor, uid, tax_template, {
                    'name': semi_random_name,
                    'field_based_on': 'quantity'
                }
            )

            tax_chart_template_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'demo_chart_template_01'
            )[1]

            # Wizard to update the taxes with the templates
            wiz_id = update_tax_wiz.create(cursor, uid, {})
            update_tax_wiz.action_init(cursor, uid, [wiz_id])
            update_tax_wiz.write(cursor, uid, [wiz_id], {
                'update_tax_code': False,
                'update_account': False,
                'update_fiscal_position': False,
                'update_children_accounts_parent': False,
                'chart_template_id': tax_chart_template_id,

            })
            update_tax_wiz.action_find_records(cursor, uid, [wiz_id])
            update_tax_wiz.action_update_records(cursor, uid, [wiz_id])

            tax_id = tax_obj.search(
                cursor, uid, [
                    ('name', '=', semi_random_name),
                    ('field_based_on', '=', 'quantity')
                ]
            )

            expect(tax_id).not_to(be_empty)
