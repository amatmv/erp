{
	"name" : "Production Merge",
	"version" : "0.1",
	"description" : """This module adds a new wizard that allows merging two or more production orders.""",
	"author" : "NaN for Trod y Avia, S.L.",
	"website" : "http://www.NaN-tic.com",
	"depends" : [ 
		'mrp',
	],
	"category" : "Custom Modules",
	"init_xml" : [],
	"demo_xml" : [],
	"update_xml" : [ 
		'mrp_view.xml',
	],
	"active": False,
	"installable": True
}
