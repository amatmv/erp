# -*- coding: utf-8 -*-

from osv import osv, fields


class PaymentType(osv.osv):
    _name = 'payment.type'
    _inherit = 'payment.type'

    _columns = {
        'automatic_unpaid': fields.boolean(u'Move to unpaid automatically'),
    }

    _defaults = {
        'automatic_unpaid': lambda *a: False
    }


PaymentType()
