# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _


class WizardChangePending(osv.osv_memory):
    """Wizard for changing pending state in invoices"""

    _name = 'wizard.change.pending'

    def _num_invoices(self, cursor, uid, context=None):

        if not context:
            context = {}
        return len(context.get('active_ids', []))

    def action_set_new_pending(self, cursor, uid, ids, context=None):

        if context is None:
            context = {}
        if not context.get('model', False):
            raise osv.except_osv(_(u'Error'),
                                 _(u'Cannot determine model'))

        wizard = self.browse(cursor, uid, ids[0])
        if not wizard.new_pending:
            raise osv.except_osv(
                _('Error!'),
                _('If you want to change the pending state'
                  ' the "new pending" field must be filled up')
            )
        model_ids = context.get('active_ids', [])
        model = context['model']
        model_obj = self.pool.get(model)
        new_pending_id = wizard.new_pending.id
        changed = 0
        for model_id in model_ids:
            model_obj.set_pending(
                cursor, uid, [model_id], new_pending_id, context=context
            )
            changed += 1

        wizard.write({'changed_invoices': changed,
                      'state': 'end'})

    def action_set_next_pending(self, cursor, uid, ids, context=None):

        if not context:
            context = {}
        if not context.get('model', False):
            raise osv.except_osv(_(u'Error'),
                                 _(u'Cannot determine model'))

        wizard = self.browse(cursor, uid, ids[0])
        model_ids = context.get('active_ids', [])
        model = context['model']
        model_obj = self.pool.get(model)
        changed = 0
        for model_id in model_ids:
            model_obj.go_on_pending(
                cursor, uid, [model_id], context=context
            )
            changed += 1

        wizard.write({'changed_invoices': changed,
                      'state': 'end'})

    def _get_old_state(self, cursor, uid, context=None):
        if not context:
            context = {}
        invoice_ids = context.get('active_ids', [])
        inv_obj = self.pool.get(context['model'])
        pending_obj = self.pool.get('account.invoice.pending.state')
        pending_ids = inv_obj.read(cursor, uid, invoice_ids, ['pending_state'])
        processes_ids = pending_obj.read(
            cursor, uid, [pstate['pending_state'][0] for pstate in pending_ids],
            ['process_id']
        )
        processes_set = set([proc['process_id'][0] for proc in processes_ids])
        if len(processes_set) > 1:
            return []
        else:
            return processes_set.pop()

    _columns = {
        'new_pending': fields.many2one('account.invoice.pending.state',
                                       'New pending',
                                       required=False),
        'num_invoices': fields.integer('Invoices to change',
                                       readonly=True),
        'changed_invoices': fields.integer('Changed invoices',
                                           readonly=True),
        'state': fields.char('State', size=10),
        'process_id': fields.many2one('account.invoice.pending.state.process',
                                      'Process of the invoice')
    }

    _defaults = {
        'process_id': _get_old_state,
        'num_invoices': _num_invoices,
        'changed_invoices': lambda *a: 0,
        'state': lambda *a: 'init',
    }


WizardChangePending()
