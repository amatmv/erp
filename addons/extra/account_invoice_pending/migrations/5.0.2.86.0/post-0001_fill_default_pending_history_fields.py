# coding=utf-8
import netsvc


def up(cursor, installed_version):
    """ Migration to 2.86.0 """
    logger = netsvc.Logger()
    if not installed_version:
        return

    logger.notifyChannel(
        'migration', netsvc.LOG_INFO,
        u'Creating the history lines of the pending state for each invoice.'
    )
    cursor.execute("""
      INSERT INTO
        account_invoice_pending_history(invoice_id, pending_state_id, change_date)
      SELECT
        id,
        pending_state,
        pending_state_date
      FROM account_invoice
      WHERE pending_state IS NOT NULL
        AND pending_state_date IS NOT NULL;

    """)

    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         'Succesfully set default fields!')


def down(cursor, installed_version):
    pass


migrate = up
