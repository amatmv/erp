# coding=utf-8
import netsvc


def up(cursor, installed_version):
    """ Migration to 2.86.0 """
    logger = netsvc.Logger()
    if not installed_version:
        return

    logger.notifyChannel(
        'migration', netsvc.LOG_INFO,
        u'Change date format of the "pending_state_date" field.'
    )

    cursor.execute("""
        UPDATE account_invoice
        SET pending_state_date =
        to_char(pending_state_date, 'YYYY-MM-DD HH24:MI:SS')::TIMESTAMP
        WHERE pending_state_date IS NOT NULL;
    """)

    logger.notifyChannel(
        'migration', netsvc.LOG_INFO, u'Date format successfully changed!'
    )


def down(cursor, installed_version):
    pass

migrate = up
