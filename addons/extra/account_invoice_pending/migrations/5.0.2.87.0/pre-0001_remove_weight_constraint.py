# coding=utf-8
import logging


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')

    logger.info(
        'Delete constraint to let the ERP create the new one, which is '
        'different.'
    )

    cursor.execute("""
        ALTER TABLE account_invoice_pending_state
        DROP CONSTRAINT account_invoice_pending_state_weight_unique
    """)

    logger.info('Migration successful!')


def down(cursor, installed_version):
    pass


migrate = up
