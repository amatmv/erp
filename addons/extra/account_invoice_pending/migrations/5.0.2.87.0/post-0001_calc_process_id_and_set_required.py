# coding=utf-8
import logging
import pooler


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')

    pool = pooler.get_pool(cursor.dbname)
    uid = 1

    logger.info('Setting process_id for existing account_invoice_pending_state')
    pstate_obj = pool.get('account.invoice.pending.state')
    imd_obj = pool.get('ir.model.data')

    default_process_id = imd_obj.get_object_reference(
        cursor, uid, 'account_invoice_pending',
        'default_pending_state_process'
    )[1]
    pstate_ids = pstate_obj.search(cursor, uid, [('process_id', '=', False)])
    pstate_obj.write(
        cursor, uid, pstate_ids, {'process_id': default_process_id})

    logger.info('Set process_id from account_invoice_pending_state not null')
    cursor.execute("""
        ALTER TABLE account_invoice_pending_state
        ALTER COLUMN process_id SET NOT NULL
    """)

    logger.info('Migration successful!')


def down(cursor, installed_version):
    pass


migrate = up
