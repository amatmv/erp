# -*- encoding: utf-8 -*-

from osv import osv, fields
from tools.translate import _
from tools import cache
from datetime import datetime
from osv.expression import OOQuery

PENDING_DAYS_TYPE = [
    ('natural', 'Natural Days'),
    ('business', 'Business Days')
]


class AccountInvoicePendingStateProcess(osv.osv):

    _name = 'account.invoice.pending.state.process'
    _description = 'Model that describes a cut off process'

    def get_default_state(self, cursor, uid, process_id, context=None):
        if context is None:
            context = {}
        pstate_obj = self.pool.get('account.invoice.pending.state')
        # The default pending_state is the one that has weight=0 and belongs to
        # the process_id
        search_params = [('weight', '=', '0'),
                         ('process_id', '=', process_id)]

        return pstate_obj.search(cursor, uid, search_params)[0]

    def get_default_process(self, cursor, uid, context=None):
        if context is None:
            context = {}

        imd_obj = self.pool.get('ir.model.data')
        default_process_id = imd_obj.get_object_reference(
            cursor, uid, 'account_invoice_pending',
            'default_pending_state_process'
        )[1]

        return default_process_id

    _columns = {
        'name': fields.char(u'Cut off process name', size=32, required=True),
        'pending_states': fields.one2many(
            'account.invoice.pending.state', 'process_id',
            u'Pending states', readonly=True
        )
    }

AccountInvoicePendingStateProcess()


class AccountInvoicePendingState(osv.osv):

    _name = 'account.invoice.pending.state'

    _order = 'weight'

    @cache(30)
    def get_next(self, cursor, uid, weight, process_id, context=None):
        if context is None:
            context = {}
        pstate_id = self.search(
            cursor, uid, [
                ('weight', '>=', weight),
                ('process_id', '=', process_id)
            ], limit=2, order='weight asc', context=context)
        return pstate_id[-1]

    def _fnt_is_last(self, cursor, uid, ids, field_name, arg, context=None):
        """Check if is the last state.

        to be the last state it doesn't have to have any states following and
        have to have previous states.
        """
        if context is None:
            context = {}
        res = dict.fromkeys(ids, False)
        for state in self.read(
                cursor, uid, ids, ['weight', 'process_id', 'active'],
                context=context):
            following = self.search_count(cursor, uid, [
                ('weight', '>', state['weight']),
                ('process_id', '=', state['process_id'][0])
            ])
            previous = self.search_count(cursor, uid, [
                ('weight', '<', state['weight']),
                ('process_id', '=', state['process_id'][0])
            ])
            res[state['id']] = previous and not following and state['active']
        return res

    def _get_default_state(self, cursor, uid, process_id=None, context=None):
        if context is None:
            context = {}
        process_obj = self.pool.get('account.invoice.pending.state.process')
        if process_id is None:
            process_id = process_obj.get_default_process(
                cursor, uid, context=context
            )

        return process_obj.get_default_state(
            cursor, uid, process_id=process_id, context=context)

    # START_CORREOS

    # Si acuse de recivo next state
    def notification_delivered(
            self, cursor, uid, invoice_id, origen_pending_state_id, context=None):
        '''
        :param invoice_id: Ids de les account.invoice que tienen que ser
        actualizadas
        :param origen_pending_state_id: pending state original cuando se añadio
        la factura a la remesa de correos (la linea de la remesa contiene ese
        estado)
        :return:
        '''
        inv_obj = self.pool.get('account.invoice')

        if context is None:
            context = {}

        if isinstance(origen_pending_state_id, (list, tuple)):
            origen_pending_state_id = origen_pending_state_id[0]

        if not isinstance(invoice_id, (list, tuple)):
            invoice_id = [invoice_id]

        pending_params = ['process_id', 'weight']
        p_state_reads = self.read(
            cursor, uid, origen_pending_state_id, pending_params,
            context=context
        )

        weight = p_state_reads['weight']
        process = p_state_reads['process_id']

        pending_to = self.get_next(
            cursor, uid, weight=weight, process_id=process, context=context
        )

        inv_obj.set_pending(
            cursor, uid, invoice_id, pending_to, context=context
        )

        return pending_to

    # Si se ha perdido la carta volvemos al estado original
    def notification_lost(
            self, cursor, uid, invoice_id, origen_pending_state_id, context=None):
        '''
        :param invoice_id: Ids de les account.invoice que tienen que ser
        actualizadas
        :param origen_pending_state_id: pending state original cuando se añadio
        la factura a la remesa de correos (la linea de la remesa contiene ese
        estado)
        :return:
        '''
        inv_obj = self.pool.get('account.invoice')

        if context is None:
            context = {}

        if isinstance(origen_pending_state_id, (list, tuple)):
            origen_pending_state_id = origen_pending_state_id[0]

        if not isinstance(invoice_id, (list, tuple)):
            invoice_id = [invoice_id]

        inv_obj.set_pending(
            cursor, uid, invoice_id, origen_pending_state_id, context=context
        )

    # Carta enviada
    def notification_sent(
            self, cursor, uid, invoice_id, ori_pending_state_id, context=None):
        pass

    def notification_not_delivered(
            self, cursor, uid, invoice_id, ori_pending_state_id, context=None):
        pass

    def search_destination_pending_by_name(self, cursor, uid, pend_dest_name):
        select_id_of_pending_destiny_state = (
            u"SELECT res_id FROM ir_model_data "
            u"WHERE "
            u"name = '%s' "
            u"AND "
            u"model = 'account.invoice.pending.state'"
        ) % pend_dest_name

        cursor.execute(select_id_of_pending_destiny_state)

        pend_dest_id = cursor.dictfetchone()[u'res_id']

        return pend_dest_id

    # END_CORREOS

    _columns = {
        'name': fields.char('Name', size=64, required=True, translate=True),
        'weight': fields.integer('Order', required=True),
        'is_last': fields.function(
            _fnt_is_last, type='boolean', method=True, string='Is last'
        ),
        'pending_days': fields.integer(
            u'Days', help=u'Days in which the state has to be before changing automatically.'
        ),
        'pending_days_type': fields.selection(PENDING_DAYS_TYPE, u'Days type'),
        'process_id': fields.many2one(
            'account.invoice.pending.state.process', u'Cut off process',
            required=True
        ),
        'active': fields.boolean(
            string=u'Active',
            help=u'Indicates if the pending state is active and can be searched'
        ),
    }

    _defaults = {
        'pending_days': lambda *a: 0,
        'active': lambda *a: True,
        'pending_days_type': lambda *a: 'natural'
    }

    _sql_constraints = [
        ('weight_unique',
         'unique (process_id, weight)',
         _('The weight must be unique for this process!')),
    ]

AccountInvoicePendingState()


class AccountInvoice(osv.osv):

    _name = 'account.invoice'
    _inherit = 'account.invoice'

    def _get_default_pending(self, cursor, uid, process_id=None, context=None):
        '''Default pending is always the one with weight = 0'''

        pending_obj = self.pool.get('account.invoice.pending.state')
        return pending_obj._get_default_state(
            cursor, uid, process_id=process_id, context=context)

    def unlink(self, cursor, uid, ids, context=None):
        pstate_history_obj = self.pool.get('account.invoice.pending.history')
        pstates_history_ids = pstate_history_obj.search(
            cursor, uid, [('invoice_id', 'in', ids)])
        pstate_history_obj.unlink(cursor, uid, pstates_history_ids)
        super(AccountInvoice, self).unlink(cursor, uid, ids, context)
        return True

    def set_pending(self, cursor, uid, ids, pending_id, context=None):
        """ A history line will be created and the pending_state field will
        be changed with the last history line pending_state value."""
        if context is None:
            context = {}
        context.update({'history_pending_state': pending_id})
        res = self.create_history_line(cursor, uid, ids, context)
        return res

    def get_default_pending_state(self, cursor, uid, invoice_id=None,
                                  context=None):
        if context is None:
            context = {}
        process_obj = self.pool.get('account.invoice.pending.state.process')
        default_process_id = process_obj.get_default_process(cursor, uid)
        return process_obj.get_default_state(cursor, uid, default_process_id)

    def get_last_change_date(self, cursor, uid, invoice_id, pending_id,
                             context=None):
        pending_history_obj = self.pool.get('account.invoice.pending.history')
        changes_ids = pending_history_obj.search(cursor, uid, [
            ('invoice_id', '=', invoice_id),
            ('pending_state_id', '=', pending_id)
        ], order='create_date DESC', limit=1)
        change_date = False
        if changes_ids:
            change_date = pending_history_obj.read(
                cursor, uid, changes_ids[0], ['change_date']
            )['change_date']

        return change_date

    def write(self, cursor, uid, ids, vals, context=None):
        def get_line_info(cursor, uid, invoice_id):
            q = OOQuery(hist_obj, cursor, uid)
            sql = q.select(
                ['id'], order_by=['change_date.asc'], limit=1,
            ).where([
                ('invoice_id', '=', invoice_id),
                ('pending_state_id.weight', '=', 0)
            ])
            cursor.execute(*sql)
            return cursor.dictfetchall()

        if context is None:
            context = {}
        if not isinstance(ids, list):
            ids = [ids]
        # If opening an invoice is wanted, then the correct state that should be
        # set is the default one of the contract's process
        if vals.get('state') == 'open':
            for inv_id in ids:
                hist_obj = self.pool.get('account.invoice.pending.history')
                line_vals = get_line_info(cursor, uid, inv_id)
                if not line_vals:
                    default_pstate = self.get_default_pending_state(
                        cursor, uid, inv_id)
                    self.set_pending(cursor, uid, [inv_id], default_pstate)
                else:
                    line_vals = line_vals[0]
                    inv_date = vals.get(
                        'date_invoice', self.read(
                            cursor, uid, inv_id, ['date_invoice']
                        )['date_invoice']
                    )
                    hist_obj.write(
                        cursor, uid, line_vals['id'], {'change_date': inv_date},
                        context=context
                    )
        return super(AccountInvoice, self).write(
            cursor, uid, ids, vals, context=context
        )

    def get_current_pending_state_info(self, cursor, uid, ids, context=None):
        """
            Get the info of the last history line by invoice id.
        :return: a dict containing the info of the last history line of the
                 invoice indexed by its id.
                 ==== Fields of the dict for each invoice ===
                 'id': if of the last account.invoice.pending.history
                 'pending_state_id': id of its pending_state
                 'change_date': date of change (also, date of the creation of
                                the line)
        """
        if context is None:
            context = {}
        if not isinstance(ids, list):
            ids = [ids]
        pending_history_obj = self.pool.get('account.invoice.pending.history')
        result = dict.fromkeys(ids, False)
        fields_to_read = ['pending_state_id', 'change_date', 'invoice_id']
        for id in ids:
            res = pending_history_obj.search(
                cursor, uid, [('invoice_id', '=', id)]
            )
            if res:
                # We consider the last record the first one due to order
                # statement in the model definition.
                values = pending_history_obj.read(
                    cursor, uid, res[0], fields_to_read)
                result[id] = {
                    'id': values['id'],
                    'pending_state_id': values['pending_state_id'][0],
                    'change_date': values['change_date'],
                }
            else:
                result[id] = False
        return result

    def create_history_line(self, cursor, uid, ids, context=None):
        """
        :param cursor: database cursor
        :param uid: user identifier
        :param ids: ids of the invoices changed
        :param context: dictionary with the context which must
                        include the pending_stat id
        :return: Returns True if some record has been created
        """
        if context is None:
            context = {}
        if not isinstance(ids, list):
            ids = [ids]
        pending_history_obj = self.pool.get('account.invoice.pending.history')
        next_state = context.get(
            'history_pending_state', self._get_default_pending(cursor, uid)
        )
        registers_created = 0
        last_lines = self.get_current_pending_state_info(cursor, uid, ids)
        for current_inv_id in ids:
            change_date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            if last_lines[current_inv_id]:
                pending_history_obj.write(
                    cursor, uid, last_lines[current_inv_id]['id'], {
                        'end_date': change_date
                    }
                )
            res = pending_history_obj.create(cursor, uid, {
                'pending_state_id': next_state,
                'change_date': change_date,
                'invoice_id': current_inv_id
            })
            if res:
                registers_created += 1
        return registers_created > 0

    def go_on_pending(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        if not isinstance(ids, list):
            ids = [ids]
        if not ids:
            return False
        pstate_obj = self.pool.get('account.invoice.pending.state')
        q = OOQuery(self, cursor, uid)
        sql = q.select([
            'id', 'pending_state.weight', 'pending_state.process_id',
        ]).where([('id', 'in', ids)])
        cursor.execute(*sql)
        res = cursor.dictfetchall()
        for invoice in res:
            weight = invoice['pending_state.weight']
            process_id = invoice['pending_state.process_id']
            pstate_id = pstate_obj.get_next(cursor, uid, weight, process_id)
            self.set_pending(cursor, uid, [invoice['id']], pstate_id)
        return True

    def pay_and_reconcile(self, cursor, uid, ids, pay_amount, pay_account_id,
                          period_id, pay_journal_id, writeoff_acc_id,
                          writeoff_period_id, writeoff_journal_id, context=None,
                          name=''):
        res = super(AccountInvoice, self).pay_and_reconcile(
            cursor, uid, ids, pay_amount, pay_account_id, period_id,
            pay_journal_id, writeoff_acc_id, writeoff_period_id,
            writeoff_journal_id, context, name=name
        )
        invoice = self.browse(cursor, uid, ids[0])
        if not invoice.residual and invoice.pending_state.weight != 0:
            correct_state = self._get_default_pending(
                cursor, uid,
                process_id=invoice.pending_state.process_id.id,
                context=context
            )
            invoice.set_pending(correct_state)
        return res

    def _get_last_pending_state_from_history(self, cursor, uid, ids,
                                             field_name, arg, context=None):
        result = {k: {} for k in ids}
        last_lines = self.get_current_pending_state_info(cursor, uid, ids)
        for id in ids:
            if last_lines[id]:
                result[id]['pending_state'] = last_lines[id]['pending_state_id']
                result[id]['pending_state_date'] = last_lines[id]['change_date']
            else:
                result[id]['pending_state'] = False
                result[id]['pending_state_date'] = False
        return result

    def change_state(self, cursor, uid, ids, context):
        values = self.read(cursor, uid, ids, ['invoice_id'])
        return [value['invoice_id'][0] for value in values]

    _STORE_PENDING_STATE = {
        'account.invoice.pending.history': (
            change_state, ['change_date'], 10
        )
    }

    _columns = {
        'pending_state': fields.function(
            _get_last_pending_state_from_history, method=True, type='many2one',
            obj='account.invoice.pending.state', string=u'Pending State',
            required=False, readonly=True, store=_STORE_PENDING_STATE,
            multi='pending'
        ),
        'pending_state_date': fields.function(
            _get_last_pending_state_from_history, method=True, type='datetime',
            store=_STORE_PENDING_STATE, string=u'Pending State Date',
            multi='pending'
        ),
        'pending_history_ids': fields.one2many(
            'account.invoice.pending.history',
            'invoice_id',
            u'Pending States History',
            readonly=True
        ),
    }

AccountInvoice()


class AccountInvoicePendingHistory(osv.osv):
    _name = 'account.invoice.pending.history'

    _columns = {
        'pending_state_id': fields.many2one(
            'account.invoice.pending.state', u'Pending State', required=False
        ),
        'change_date': fields.date(u'Change Date', select=True, readonly=True),
        'end_date': fields.date(u'End Date', select=True, readonly=True),
        'invoice_id': fields.many2one(
            'account.invoice', u'Invoice', readonly=True, ondelete="set null"
        )
    }
    _order = 'end_date desc, id desc'

AccountInvoicePendingHistory()
