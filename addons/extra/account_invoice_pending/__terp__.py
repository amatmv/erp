# -*- coding: utf-8 -*-
{
    "name": "Pending invoices management",
    "description": """
Pending invoices management:
    * Pending state per invoice
    * Configurable pending states
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Facturació",
    "depends": [
        "base",
        "account",
        "account_payment_extension",
    ],
    "init_xml": [],
    "demo_xml": [
        "pending_state_process_demo.xml",
    ],
    "update_xml": [
        "account_invoice_process_data.xml",
        "account_invoice_data.xml",
        "account_invoice_view.xml",
        "account_payment_view.xml",
        "pending_state_changer_cron.xml",
        "wizard/wizard_change_pending_view.xml",
        "security/ir.model.access.csv",
    ],
    "active": False,
    "installable": True
}
