# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from datetime import date

from osv import osv
from osv.expression import OOQuery


class MoveToUnpaid(osv.osv_memory):

    _name = 'move.to.unpaid'

    def get_invoices_to_update(self, cursor, uid):
        pstate_obj = self.pool.get('account.invoice.pending.state')
        inv_obj = self.pool.get('account.invoice')
        tipo_pago_obj = self.pool.get('payment.type')

        def_pstates_ids = pstate_obj.search(cursor, uid, [('weight', '=', 0)])
        cobrador_ids = tipo_pago_obj.search(
            cursor, uid, [('automatic_unpaid', '=', True)])
        if cobrador_ids:
            today = date.today().strftime('%Y-%m-%d')
            search_params = [('payment_type', 'in', cobrador_ids),
                             ('state', '=', 'open'),
                             ('type', '=', 'out_invoice'),
                             ('pending_state', 'in', def_pstates_ids),
                             ('date_due', '<', today)]
            q = OOQuery(inv_obj, cursor, uid)
            sql = q.select(['id']).where(search_params)
            cursor.execute(*sql)
            return [inv['id'] for inv in cursor.dictfetchall()]
        return []

    def move_to_unpaid(self, cursor, uid, context=None):

        if context is None:
            context = {}

        invoice_ids = self.get_invoices_to_update(cursor, uid)
        inv_obj = self.pool.get('account.invoice')
        if invoice_ids:
            inv_obj.go_on_pending(cursor, uid, invoice_ids)

        return True


MoveToUnpaid()
