# -*- coding: utf-8 -*-

from datetime import datetime, timedelta, date

from osv import osv
from osv.expression import OOQuery


class UpdatePendingStates(osv.osv_memory):

    _name = 'update.pending.states'

    def get_invoices_to_update(self, cursor, uid):
        inv_obj = self.pool.get('account.invoice')
        search_params = [('pending_state.active', '=', True),
                         ('pending_state.pending_days', '>', 0),
                         ('type', '=', 'out_invoice'),
                         ('state', '=', 'open')]
        return inv_obj.search(cursor, uid, search_params)

    def update_state(self, cursor, uid, invoice_id, history_values):
        """
        If the pending days have passed, the pending_state of the invoice
        will change
        :param invoice_id: id of the invoice
        :param history_values: values of the pending history line
        """
        pstate_obj = self.pool.get('account.invoice.pending.state')
        inv_obj = self.pool.get('account.invoice')

        fields_to_read = ['pending_days', 'is_last', 'pending_days_type']
        pstate = pstate_obj.read(
            cursor, uid, history_values['pending_state_id'], fields_to_read
        )
        change_date = datetime.strptime(
            history_values['change_date'], '%Y-%m-%d'
        ).date()
        current_date = date.today()

        if pstate['pending_days_type'] == 'business':
            from workalendar.europe import Spain
            due_date = Spain().add_working_days(
                change_date, pstate['pending_days']
            )
        else:
            due_date = (change_date + timedelta(days=pstate['pending_days']))

        if current_date >= due_date and not pstate['is_last']:
            inv_obj.go_on_pending(cursor, uid, [invoice_id])

    def update_invoices(self, cursor, uid, context=None):

        if context is None:
            context = {}

        inv_obj = self.pool.get('account.invoice')
        invoice_ids = self.get_invoices_to_update(cursor, uid)

        last_lines_by_invoice = inv_obj.get_current_pending_state_info(
            cursor, uid, invoice_ids
        )

        for invoice_id, history_values in last_lines_by_invoice.items():
            self.update_state(cursor, uid, invoice_id, history_values)

        return True

UpdatePendingStates()
