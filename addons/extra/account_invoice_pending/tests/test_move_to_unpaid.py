# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction


class TestMoveToUnPaid(testing.OOTestCase):

    def clear_cache_pending(self, txn):
        pool = self.openerp.pool
        account_pending_obj = pool.get('account.invoice.pending.state')
        cursor = txn.cursor
        account_pending_obj.get_next.clear_cache(cursor.dbname)

    def test_move_to_unpaid(self):
        """ Test if model sets to 'unpaid' the invoices correctly."""
        pool = self.openerp.pool
        imd_obj = pool.get('ir.model.data')
        payment_type_obj = pool.get('payment.type')
        inv_obj = pool.get('account.invoice')
        pstate_obj = pool.get('account.invoice.pending.state')
        process_obj = pool.get('account.invoice.pending.state.process')
        move_wiz = pool.get('move.to.unpaid')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            invoice_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'invoice_0001'
            )[1]
            default_pstate = inv_obj._get_default_pending(cursor, uid)
            demo_pay_type_id = imd_obj.get_object_reference(
                cursor, uid, 'account_payment', 'payment_mode_demo'
            )[1]
            # We update the invoice that will be moved to unpaid with the info
            # that we need
            inv_obj.write(cursor, uid, invoice_id, {
                'state': 'open',
                'date_due': '2018-01-01',
                'payment_type': demo_pay_type_id
            })

            move_wiz.move_to_unpaid(cursor, uid)

            inv_pstate = inv_obj.read(
                cursor, uid, invoice_id, ['pending_state'])['pending_state'][0]

            default_process = process_obj.get_default_process(cursor, uid)
            id_10 = pstate_obj.create(cursor, uid, {
                'name': 'Unpaid',
                'weight': 10,
                'process_id': default_process
            })
            self.assertEquals(inv_pstate, default_pstate)

            payment_type_obj.write(cursor, uid, demo_pay_type_id, {
                'automatic_unpaid': True
            })

            self.clear_cache_pending(txn)

            move_wiz.move_to_unpaid(cursor, uid)
            inv_pstate = inv_obj.read(
                cursor, uid, invoice_id, ['pending_state'])['pending_state'][0]
            self.assertEqual(inv_pstate, id_10)
