# -*- coding: utf-8 -*-
import time
from datetime import date

from destral import testing
from destral.transaction import Transaction
import netsvc

from test_move_to_unpaid import *
from test_update_pending_states import *


class PendingStateTests(testing.OOTestCase):

    def clear_cache_pending(self, cursor):
        pool = self.openerp.pool
        account_pending_obj = pool.get('account.invoice.pending.state')
        account_pending_obj.get_next.clear_cache(cursor.dbname)

    require_demo_data = True

    def test_get_next_state(self):
        pstate_obj = self.openerp.pool.get('account.invoice.pending.state')
        imd_obj = self.openerp.pool.get('ir.model.data')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            default_process_id = imd_obj.get_object_reference(
                cursor, uid, 'account_invoice_pending',
                'default_pending_state_process'
            )[1]
            pstate_obj.create(cursor, uid, {
                'name': 'Weight 10',
                'weight': 1,
                'process_id': default_process_id
            })

            id_20 = pstate_obj.create(cursor, uid, {
                'name': 'Weight 20',
                'weight': 2,
                'process_id': default_process_id
            })

            self.assertEqual(id_20, pstate_obj.get_next(
                cursor, uid, 1, default_process_id
            ))
            self.assertEqual(id_20, pstate_obj.get_next(
                cursor, uid, 2, default_process_id
            ))

    def test_dont_get_non_active_pending_state(self):
        pstate_obj = self.openerp.pool.get('account.invoice.pending.state')
        imd_obj = self.openerp.pool.get('ir.model.data')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            default_process_id = imd_obj.get_object_reference(
                cursor, uid, 'account_invoice_pending',
                'default_pending_state_process'
            )[1]
            weight_10 = 10
            weight_20 = 20
            weight_30 = 30
            pstate_weight_10 = pstate_obj.create(cursor, uid, {
                'name': 'Weight 10',
                'weight': weight_10,
                'process_id': default_process_id
            })

            pstate_weight_20 = pstate_obj.create(cursor, uid, {
                'name': 'Weight 20',
                'weight': weight_20,
                'process_id': default_process_id,
                'active': False
            })

            pstate_weight_30 = pstate_obj.create(cursor, uid, {
                'name': 'Weight 30',
                'weight': weight_30,
                'process_id': default_process_id,
            })

            self.assertEqual(pstate_weight_30, pstate_obj.get_next(
                cursor, uid, weight_10, default_process_id
            ))
            self.assertEqual(pstate_weight_30, pstate_obj.get_next(
                cursor, uid, weight_20, default_process_id
            ))
            self.assertEqual(pstate_weight_30, pstate_obj.get_next(
                cursor, uid, weight_30, default_process_id
            ))

    def test_is_last_with_non_active_pending_state(self):
        pstate_obj = self.openerp.pool.get('account.invoice.pending.state')
        imd_obj = self.openerp.pool.get('ir.model.data')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            model, res_id = imd_obj.get_object_reference(
                cursor, uid, 'account_invoice_pending',
                'default_invoice_pending_state'
            )
            default_process_id = imd_obj.get_object_reference(
                cursor, uid, 'account_invoice_pending',
                'default_pending_state_process'
            )[1]

            default_state = pstate_obj.browse(cursor, uid, res_id)
            # When there is only one state this is no the last state because
            # no more states exists
            self.assertEqual(bool(default_state.is_last), False)

            id_10 = pstate_obj.create(cursor, uid, {
                'name': 'Weight 10',
                'weight': 10,
                'process_id': default_process_id
            })
            state_10 = pstate_obj.browse(cursor, uid, id_10)

            id_20 = pstate_obj.create(cursor, uid, {
                'name': 'Weight 20',
                'weight': 20,
                'process_id': default_process_id
            })
            state_20 = pstate_obj.browse(cursor, uid, id_20)

            id_30 = pstate_obj.create(cursor, uid, {
                'name': 'Weight 30',
                'weight': 30,
                'process_id': default_process_id,
                'active': False
            })
            state_30 = pstate_obj.browse(
                cursor, uid, id_30, context={'active_test': False})

            self.assertEqual(bool(state_10.is_last), False)
            self.assertEqual(bool(state_20.is_last), True)
            self.assertEqual(bool(state_30.is_last), False)

    def test_go_on_pending_with_non_active_pending_state(self):
        """Test pending state from invoice is going on.
        """
        pool = self.openerp.pool
        imd_obj = pool.get('ir.model.data')
        invoice_obj = pool.get('account.invoice')
        pstate_obj = pool.get('account.invoice.pending.state')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            model, ref = imd_obj.get_object_reference(
                cursor, uid, 'account', 'test_invoice_1'
            )
            correct_state_id = imd_obj.get_object_reference(
                cursor, uid,
                'account_invoice_pending', 'default_invoice_pending_state'
            )[1]
            default_process_id = imd_obj.get_object_reference(
                cursor, uid, 'account_invoice_pending',
                'default_pending_state_process'
            )[1]
            invoice_id = invoice_obj.copy(cursor, uid, ref)
            wf_service = netsvc.LocalService("workflow")
            wf_service.trg_validate(
                uid, 'account.invoice', invoice_id, 'invoice_open', cursor
            )
            invoice = invoice_obj.browse(cursor, uid, invoice_id)
            self.assertEqual(invoice.state, 'open')
            invoice.set_pending(correct_state_id)
            # Refresh invoice object
            invoice = invoice_obj.browse(cursor, uid, invoice_id)
            self.assertEqual(invoice.pending_state.id, correct_state_id)

            id_5 = pstate_obj.create(cursor, uid, {
                'name': 'Weight 5',
                'weight': 5,
                'process_id': default_process_id,
                'active': False
            })

            id_10 = pstate_obj.create(cursor, uid, {
                'name': 'Weight 10',
                'weight': 10,
                'process_id': default_process_id
            })

            self.clear_cache_pending(cursor)
            invoice.go_on_pending()
            # Refresh invoice object
            invoice = invoice_obj.browse(cursor, uid, invoice_id)
            self.assertEqual(invoice.pending_state.id, id_10)

    def test_set_correct_on_pay(self):
        """Test set correct state from invoice on payed.
        """
        pool = self.openerp.pool
        imd_obj = pool.get('ir.model.data')
        invoice_obj = pool.get('account.invoice')
        pstate_obj = pool.get('account.invoice.pending.state')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            model, ref = imd_obj.get_object_reference(
                cursor, uid, 'account', 'test_invoice_1'
            )
            correct_state_id = imd_obj.get_object_reference(
                cursor, uid,
                'account_invoice_pending', 'default_invoice_pending_state'
            )[1]
            default_process_id = imd_obj.get_object_reference(
                cursor, uid, 'account_invoice_pending',
                'default_pending_state_process'
            )[1]
            invoice_id = invoice_obj.copy(cursor, uid, ref)
            wf_service = netsvc.LocalService("workflow")
            wf_service.trg_validate(
                uid, 'account.invoice', invoice_id, 'invoice_open', cursor
            )
            invoice = invoice_obj.browse(cursor, uid, invoice_id)
            self.assertEqual(invoice.state, 'open')

            id_10 = pstate_obj.create(cursor, uid, {
                'name': 'Weight 10',
                'weight': 1,
                'process_id': default_process_id
            })

            id_20 = pstate_obj.create(cursor, uid, {
                'name': 'Weight 20',
                'weight': 2,
                'process_id': default_process_id
            })

            invoice.set_pending(id_20)
            # Refresh invoice object
            invoice = invoice_obj.browse(cursor, uid, invoice_id)
            self.assertEqual(invoice.pending_state.id, id_20)

            cash = imd_obj.get_object_reference(
                cursor, uid, 'account', 'cash'
            )[1]
            month = 'period_{0}'.format(int(time.strftime('%m')))
            period_id = imd_obj.get_object_reference(
                cursor, uid, 'account', month
            )[1]
            journal_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'bank_journal'
            )[1]

            invoice.pay_and_reconcile(
                1850, cash, period_id, journal_id, cash, period_id, journal_id
            )
            # Refresh invoice object
            invoice = invoice_obj.browse(cursor, uid, invoice_id)
            self.assertEqual(invoice.pending_state.id, correct_state_id)
            self.assertEqual(invoice.state, 'paid')

    def test_correctly_generates_record_on_invoice_creation(self):
        pool = self.openerp.pool
        imd_obj = pool.get('ir.model.data')
        invoice_obj = pool.get('account.invoice')
        pstate_obj = pool.get('account.invoice.pending.state')
        pstate_hist_obj = pool.get('account.invoice.pending.history')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            model, ref = imd_obj.get_object_reference(
                cursor, uid, 'account', 'test_invoice_1'
            )
            default_pending_state_id = pstate_obj.search(
                cursor, uid, [('weight', '=', 0)]
            )[0]
            invoice_id = invoice_obj.copy(cursor, uid, ref)
            invoice_obj.write(cursor, uid, invoice_id, {
                'state': 'open', 'date_invoice': date.today()
            })
            pstate_hist_record = pstate_hist_obj.search(
                cursor, uid, [
                    ('invoice_id', '=', invoice_id),
                    ('pending_state_id', '=', default_pending_state_id)
                ]
            )
            self.assertTrue(pstate_hist_record)

    def test_correctly_generates_history_line_on_state_change(self):
        pool = self.openerp.pool
        imd_obj = pool.get('ir.model.data')
        invoice_obj = pool.get('account.invoice')
        pstate_obj = pool.get('account.invoice.pending.state')
        pstate_hist_obj = pool.get('account.invoice.pending.history')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            model, ref = imd_obj.get_object_reference(
                cursor, uid, 'account', 'test_invoice_1'
            )
            model, default_process_id = imd_obj.get_object_reference(
                cursor, uid, 'account_invoice_pending',
                'default_pending_state_process'
            )
            invoice_id = invoice_obj.copy(cursor, uid, ref)
            invoice = invoice_obj.browse(cursor, uid, invoice_id)
            default_pending_state = invoice_obj._get_default_pending(
                cursor, uid
            )
            invoice.write({'state': 'open', 'date_invoice': date.today()})
            # Pending state successfully created
            self.assertEqual(invoice.pending_state.id, default_pending_state)

            search_vals = [
                ('invoice_id', '=', invoice_id)
            ]
            pstate_hist_search_ids = pstate_hist_obj.search(
                cursor, uid, search_vals
            )
            # There has been created a record with default pending state
            self.assertTrue(pstate_hist_search_ids)

            # Create another pending state
            id_10 = pstate_obj.create(cursor, uid, {
                'name': 'Weight 10',
                'weight': 10,
                'process_id': default_process_id
            })

            # Change the pending state and this will create another history line
            invoice.set_pending(id_10)

            # Refresh the invoice
            invoice = invoice_obj.browse(cursor, uid, invoice_id)
            # The pending state has been changed
            self.assertEqual(invoice.pending_state.id, id_10)

            # We search for a line with that pending id
            pstate_hist_search_ids = pstate_hist_obj.search(
                cursor, uid, search_vals
            )
            # There are more than one pending state logs
            self.assertTrue(len(pstate_hist_search_ids) > 1)
            fields = ['change_date', 'pending_state_id']
            vals_pstate_hist = pstate_hist_obj.read(
                cursor, uid, pstate_hist_search_ids, fields
            )[0]

            # 'YYYY-MM-DD HH:MM:SS' ---> 'YYYY-MM-DD'
            invoice_pending_date = invoice.pending_state_date[:10]
            # The date has been correctly changed
            self.assertEquals(
                vals_pstate_hist['change_date'], invoice_pending_date
            )
            

class PendingStateProcessTests(testing.OOTestCase):

    require_demo_data = True

    def test_go_next_pending_state(self):
        self.pool = self.openerp.pool

        with Transaction().start(self.database) as txn:

            cursor = txn.cursor
            uid = txn.user

            pstate_obj = self.pool.get('account.invoice.pending.state')
            imd_obj = self.pool.get('ir.model.data')

            pstate_id_1 = imd_obj.get_object_reference(
                cursor, uid, 'account_invoice_pending', 'pending_state_1'
            )[1]
            pstate_id_2 = imd_obj.get_object_reference(
                cursor, uid, 'account_invoice_pending', 'pending_state_2'
            )[1]

            process_1 = imd_obj.get_object_reference(
                cursor, uid, 'account_invoice_pending', 'process_1'
            )[1]

            weight_state_1 = pstate_obj.read(
                cursor, uid, pstate_id_1, ['weight']
            )['weight']

            # Testing if the method returns the next state OF THE PROCESS
            self.assertEqual(
                pstate_obj.get_next(cursor, uid, weight_state_1, process_1),
                pstate_id_2,
            )

            is_last_pstate_2 = pstate_obj.read(
                cursor, uid, pstate_id_2, ['is_last']
            )['is_last']

            # Testing if 'is_last' attribute has been calculated properly in
            # the function method.
            self.assertTrue(is_last_pstate_2)
