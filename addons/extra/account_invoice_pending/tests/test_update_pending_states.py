# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from datetime import date, timedelta


class TestUpdatePendingStates(testing.OOTestCase):

    def setUp(self):
        self.pool = self.openerp.pool

    def _load_demo_data(self, cursor, uid):
        imd_obj = self.pool.get('ir.model.data')
        self.invoice_1_id = imd_obj.get_object_reference(
            cursor, uid, 'account', 'invoice_0001'
        )[1]
        self.invoice_2_id = imd_obj.get_object_reference(
            cursor, uid, 'account', 'invoice_0002'
        )[1]

        # First pstate of the the Demo process 1
        self.demo_pstate_1 = imd_obj.get_object_reference(
            cursor, uid, 'account_invoice_pending', 'pending_state_1'
        )[1]
        # Second and last pstate of the the Demo process 1
        self.demo_pstate_2 = imd_obj.get_object_reference(
            cursor, uid, 'account_invoice_pending', 'pending_state_2'
        )[1]

    def test_update_pending_states(self):
        """
        Test if the script changes pending_states if due date has passed.
        """

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self._load_demo_data(cursor, uid)

            inv_obj = self.pool.get('account.invoice')
            pstate_obj = self.pool.get('account.invoice.pending.state')
            pstate_hist_obj = self.pool.get('account.invoice.pending.history')
            update_wiz = self.pool.get('update.pending.states')

            pstate_obj.write(cursor, uid, self.demo_pstate_1, {
                'pending_days': 1
            })
            invoices_ids = [self.invoice_1_id, self.invoice_2_id]
            inv_obj.write(cursor, uid, invoices_ids, {
                'state': 'open',
                'type': 'out_invoice'
            })

            inv_obj.set_pending(cursor, uid, [self.invoice_1_id], self.demo_pstate_1)
            inv_obj.set_pending(cursor, uid, [self.invoice_2_id], self.demo_pstate_2)

            # We modify the date which the state has been changed
            last_lines = inv_obj.get_current_pending_state_info(
                cursor, uid, invoices_ids
            )
            pstate_hist_ids = [line['id'] for line in last_lines.values()]
            yesterday = (date.today() - timedelta(days=1)).strftime('%Y-%m-%d')
            pstate_hist_obj.write(cursor, uid, pstate_hist_ids, {
                'change_date': yesterday
            })

            # call to update pending state of the invoices
            update_wiz.update_invoices(cursor, uid)

            invoice_1_data = inv_obj.read(
                cursor, uid, self.invoice_1_id, ['pending_state']
            )
            invoice_2_data = inv_obj.read(
                cursor, uid, self.invoice_2_id, ['pending_state']
            )

            self.assertEqual(invoice_1_data['pending_state'][0], self.demo_pstate_2)
            self.assertEqual(invoice_2_data['pending_state'][0], self.demo_pstate_2)
