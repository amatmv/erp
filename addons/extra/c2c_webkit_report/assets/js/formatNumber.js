/**
 * Obté el nombre formatat en es_ES. p.e: 1234.5 → 1.234,5
 * @param v: Number
 * @returns {string}
 */
formatNumber = function (v) {
    var v_str = v.toString();
    v_str = v_str.replace(".", ",");
    var splitted_number = v_str.split(",");
    var whole_number = splitted_number[0];
    var formatted_whole_number = whole_number.split('').reverse();
    for (i = whole_number.length - 1; i > 0; i--) {
        if (i % 3 == 0) {
            formatted_whole_number.splice(i, 0, '.')
        }
    }
    formatted_whole_number = formatted_whole_number.reverse().join('');
    if (formatted_whole_number.slice(-1) == '.') {
        formatted_whole_number.slice(0, -1)
    }
    var res = formatted_whole_number;
    if (splitted_number.length > 1) {
        res += "," + splitted_number[1].slice(0, 2)
    }

    return res
};