# -*- coding: utf-8 -*-
##############################################################################
#
# Copyright (c) 2010 Camptocamp SA (http://www.camptocamp.com) 
# All Right Reserved
#
# Author : Nicolas Bessi (Camptocamp)
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsability of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# garantees and support are strongly adviced to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################
import re
import sys
import platform
import os
import tempfile
import report
from report.report_sxw import *
from osv import osv
from tools.translate import _
import time
from mako.exceptions import html_error_template
from mako.template import Template
from mako.lookup import TemplateLookup
import inspect
import pypdftk
from report_helper import WebKitHelper
import netsvc
import pooler
from tools.config import config
from debug import DebuggerPatch
import commands
from lxml import etree
import ir
import inspect
import uuid
from string import join

from oorq.decorators import job
import re

logger = netsvc.Logger()


string_re = re.compile(r'_(.\".[a-z A-Z 0-9]*)')


class ReportWebKitBackground(osv.osv):
    _name = 'report.webkit.background'
    _auto = False

    @job(queue='reports')
    def background_report(self, cr, uid, ids, filename, report_name,
                          source_model, attach_model, attach_id,
                          context=None):
        user_obj = self.pool.get('res.users')
        attachment_obj = self.pool.get('ir.attachment')

        if not context:
            context = {}

        search_params = [('res_model', '=', attach_model),
                         ('res_id', '=', attach_id),
                         ('name', '=', filename)]
        # Skipe attachments that already exist
        if attachment_obj.search(cr, uid, search_params, context=context):
            return True

        data = {'model': source_model}
        ctx = context.copy()
        if 'lang' not in ctx or ctx['lang'] == 'False':
            ctx['lang'] = user_obj.read(cr, uid, uid, ['context_lang'],
                                        context)['context_lang']
        service = netsvc.LocalService("report.%s" % report_name)
        (result, format) = service.create(cr, uid, ids, data, ctx)

        vals = {
            'name': filename,
            'datas': base64.b64encode(result),
            'datas_fname': filename,
            'res_model': attach_model,
            'res_id': attach_id
        }
        attachment_obj.create(cr, uid, vals)
        return True

ReportWebKitBackground()


class WebKitParser(report_sxw):
    """Custom class that use webkit to render HTML reports
       Code partially taken from report openoffice. Thanks guys :)
    """

    def __init__(self, name, table, rml=False, parser=False, 
        header=True, store=False):
        self.parser_instance = False
        # From Jasper Module to avoid assert on report inheritance
        if name in netsvc.SERVICES:
            del netsvc.SERVICES[name]
        report_sxw.__init__(self, name, table, rml, parser, 
            header, store)
        self.exportMethod(self.create_background)

    def create_background(self, cr, uid, ids, filename, report_name,
                          source_model, attach_model, attach_id,
                          context=None):
        webkit_obj = self.pool.get('report.webkit.background')
        webkit_obj.background_report(cr, uid, ids, filename, report_name,
                                     source_model, attach_model,
                                     attach_id, context)
        return True

    def get_lib(self, cursor, uid, company) :
        """Return the lib wkhtml path"""
        #TODO Detect lib in system first
        path = self.pool.get('res.company').read(cursor, uid, company, ['lib_path',])
        
        if path['lib_path']:
            path = path['lib_path'].replace(u' ','')
        else :
            raise osv.except_osv(
                                _('Webkit executable not set in company'+
                                'Please complete company setting'),
                                _('Path is none')
                                )
        if os.path.isabs(path) :
            if (os.path.exists(path) and os.access(path, os.X_OK) and os.path.basename(path).startswith('wkhtmltopdf')):
                return path
            else:
                raise osv.except_osv(
                                    _('Wrong path set in company'+
                                    'Given path is not executable or path is wrong'),
                                    'for path %s'%(path)
                                    )
        else:
            for syspath in os.environ["PATH"].split(os.pathsep):
                cmd = os.path.join(syspath, path)
                if (os.path.exists(cmd) and os.access(cmd, os.X_OK)):
                    return path

        raise osv.except_osv(
                            _('Please install wkhtmltopdf 0.9.9'+
                            '(sudo apt-get install wkhtmltopdf) '+
                            'or download it from here: '+
                            'http://code.google.com/p/wkhtmltopdf/downloads/list.'+
                            ' You can set executable path into the company'),
                            _('The embeeded lib are not anymore present for security reasons')
                            )
        return False
        
    def genreate_pdf(self, comm_path, report_xml, header, footer, html_list,
                     webkit_header=False, context=None):
        """Call webkit in order to generate pdf"""

        def get_wkhtmltopdf_version(comm_path=False, context=None):
            if context is None:
                context = {}
            comm = comm_path or 'wkhtmltopdf'
            comm += " --version | cut -d ' ' -f2"
            status, version = commands.getstatusoutput(comm)
            if status != 0:
                raise osv.except_osv('Webkit HTML to PDF failed with version')
            return version

        if not webkit_header:
            webkit_header = report_xml.webkit_header

        if not context:
            context = {}

        tmp_dir = tempfile.gettempdir()
        out = '%s.%s.pdf' % (
            ''.join([x for x in report_xml.name.encode('utf-8')
                     if x.isalnum()]),
            uuid.uuid4()
        )
        out = os.path.join(tmp_dir, out.replace(' ',''))
        files = []
        file_to_del = []
        try:
            if comm_path:
                command = [comm_path]
            else:
                command = ['wkhtmltopdf']

            command.append('-q')

            if report_xml.webkit_flags:
                command += report_xml.webkit_flags.split(' ')

            if header :
                head_file = file( os.path.join(
                    tmp_dir,
                    str(uuid.uuid4()) + '.head.html'
                ),
                    'w'
                )
                head_file.write(header)
                head_file.close()
                file_to_del.append(head_file.name)
                command.append("--header-html '%s'"%(head_file.name))
            if footer :
                foot_file = file(  os.path.join(
                    tmp_dir,
                    str(uuid.uuid4()) + '.foot.html'
                ),
                    'w'
                )
                foot_file.write(footer)
                foot_file.close()
                file_to_del.append(foot_file.name)
                command.append("--footer-html '%s'"%(foot_file.name))

            if webkit_header.margin_top :
                if webkit_header.margin_top == 0.01:
                    command.append('--margin-top 0')
                else :
                    command.append('--margin-top %s'%(webkit_header.margin_top))
            if webkit_header.margin_bottom :
                if webkit_header.margin_bottom == 0.01:
                    command.append('--margin-bottom 0')
                else :
                    command.append('--margin-bottom %s'%(webkit_header.margin_bottom))
            if webkit_header.margin_left :
                if webkit_header.margin_left == 0.01:
                    command.append('--margin-left 0')
                else :
                    command.append('--margin-left %s'%(webkit_header.margin_left))
            if webkit_header.margin_right :
                if webkit_header.margin_right == 0.01:
                    command.append('--margin-right 0')
                else :
                    command.append('--margin-right %s'%(webkit_header.margin_right))
            if webkit_header.orientation :
                command.append("--orientation '%s'"%(webkit_header.orientation))
            if webkit_header.format :
                command.append(" --page-size '%s'"%(webkit_header.format))
            if getattr(webkit_header, 'grayscale', False):
                command.append('--grayscale')
            version = getattr(webkit_header, 'version', '0.12.2.1')
            zoom = False
            if getattr(webkit_header, 'zoom', 1.0) != 1.0:
                zoom = webkit_header.zoom
            if not zoom and version == '0.12.2.1' and get_wkhtmltopdf_version(comm_path) > '0.12.2.1':
                zoom = 1.28
            if zoom:
                command.append('--zoom {0:.2f}'.format(zoom))

            f = 0
            for html in html_list :
                html_file = file(os.path.join(tmp_dir, str(uuid.uuid4()) + str(f) + '.body.html'), 'w')
                f += 1
                html_file.write(html)
                html_file.close()
                file_to_del.append(html_file.name)
                command.append(html_file.name)
            command.append(out)
            generate_command = ' '.join(command)
            try:
                status = commands.getstatusoutput(generate_command)
                if status[0] :
                    raise osv.except_osv(
                        _('Webkit raise an error' ),
                        status[1]
                    )
            except Exception, exc:
                try:
                    for f_to_del in file_to_del :
                        os.unlink(f_to_del)
                except Exception, exc:
                    pass
        except Exception, exc:
            raise exc

        if (context.get('webkit_unsigned_pdf', False)
                or not getattr(report_xml, 'webkit_sign', False)):
            pdf = file(out).read()
        else:
            out_signed = self.sign_pdf(out)
            pdf = file(out_signed).read()
            os.unlink(out_signed)

        try:
            for f_to_del in file_to_del :
                os.unlink(f_to_del)
        except Exception, exc:
            pass
        os.unlink(out)

        return pdf

    def sign_pdf(self, out_file):
        '''
            Add digital signature to pdf
        '''
        jar = ("{}/c2c_webkit_report/java/JSignPdf.jar").format(
                                                        config['addons_path'])
        command = ['java']
        command.append('-jar {}'.format(jar))
        command.append('-kst PKCS12')
        command.append('-ksf {}'.format(config['pdf_cert']))
        command.append('-ksp {}'.format(config['pdf_cert_pass']))
        command.append('-ka {}'.format(config['pdf_cert_alias']))
        command.append('-d {}'.format(tempfile.gettempdir()))
        command.append('-q')
        command.append(out_file)
        generate_command = ' '.join(command)

        status = commands.getstatusoutput(generate_command)
        if status[0]:
            raise osv.except_osv(_('Error signing pdf '), status[1])

        file_name = join(out_file.split('.')[:-1], '.')
        return '{}_signed.pdf'.format(file_name)

    def translate_call(self, src):
        """Translate String."""
        ir_translation = self.pool.get('ir.translation')
        if not self.tmpl:
            ir_obj = self.pool.get('ir.actions.report.xml')
            cursor = self.parser_instance.cr
            uid = self.parser_instance.uid
            # Remove report. => [7:]
            report_xml_ids = ir_obj.search(cursor, uid, [
                ('report_name', '=', self.name[7:])
            ])
            if report_xml_ids:
                report_xml = ir_obj.read(
                    cursor, uid, report_xml_ids[0], ['report_webkit']
                )
                self.tmpl = report_xml['report_webkit']
        res = ir_translation._get_source(self.parser_instance.cr,
                                         self.parser_instance.uid, self.tmpl,
                                         'rml',
                                         self.parser_instance.localcontext.get(
                                             'lang', 'en_US'), src)
        if not res:
            res = src
        return res

    def get_css(self, report_xml, template):
        """
        Obtains the css customizable by the user. It could be modified in two
        ways: using the css of the webkit header or using the report colors.
        :param report_xml:
        :param template:
        :return: css code
        :rtype: str
        """
        # check the css of the webkit header
        css = report_xml.webkit_header.css
        if not css:
            css = ''

        # Due to some unknown reason, the webkit_header can be an other class
        # than a browse_record (see DummyHeader). For this reason, we have
        # to ensure that the webkit header is a browse record.
        if isinstance(report_xml.webkit_header, osv.orm.browse_record) and report_xml.webkit_header.color_ids:
            # check if colors by variables
            # find all the references to css files in the template
            # WARNING: it won't work if there are multiple css links in one
            # line, to be improved.
            css_links = re.findall('.*\${addons_path}.*.css.*', template)
            for css_link in css_links:
                # obtain the relative path to the css file
                css_raw_path = re.search(
                    '.*(\${addons_path}.*.css).*', css_link
                )
                css_raw_path = css_raw_path.group(1)

                # obtain the full path to the css file
                css_path = css_raw_path.replace(
                    '${addons_path}', config['addons_path']
                )

                with open(css_path, 'r') as css_file:
                    css_content = css_file.read()
                    # if the css file contains variables, then 'mark' the css
                    # file as if it was defined at the webkit header css
                    if 'var(--' in css_content:
                        css += css_content

            # replace the variables with the colors defined by the user
            for report_color in report_xml.webkit_header.color_ids:
                css = css.replace("var(--{})".format(
                    report_color.name), '#' + report_color.color
                )

        return css

    # override needed to keep the attachments' storing procedure
    def create_single_pdf(self, cursor, uid, ids, data, report_xml, context=None):
        """generate the PDF"""
        self.parser_instance = self.parser(
            cursor, uid, self.name2, context=context
        )
        self.pool = pooler.get_pool(cursor.dbname)
        objs = self.getObjects(cursor, uid, ids, context)
        self.parser_instance.set_context(objs, data, ids, report_xml.report_type)
        template = False
        report_webkit_path = getattr(report_xml, 'report_webkit', self.tmpl)

        if report_webkit_path:
            path = os.path.join(config['addons_path'], report_webkit_path)
            if os.path.exists(path):
                template = file(path).read()
            self.parser_instance.localcontext.update({
                'cursor': cursor,
                'uid': uid,
                'addons_path': config['addons_path']
            })

        if not template and report_xml.report_webkit_data:
            template = report_xml.report_webkit_data

        if not template:
            raise osv.except_osv(_('Report template not found !'), _(''))

        header = report_xml.webkit_header.html
        footer = report_xml.webkit_header.footer_html
        if not header and report_xml.header:
            raise osv.except_osv(
                _('No header defined for this report header html is empty !'), 
                _('look in company settings')
            )

        if not report_xml.header:
            header = u"""
                <html>
                    <head>
                        <style type="text/css"> 
                            ${css}
                        </style>
                        <script>
                        function subst() {
                           var vars={};
                           var x=document.location.search.substring(1).split('&');
                           for(var i in x) {var z=x[i].split('=',2);vars[z[0]] = unescape(z[1]);}
                           var x=['frompage','topage','page','webpage','section','subsection','subsubsection'];
                           for(var i in x) {
                             var y = document.getElementsByClassName(x[i]);
                             for(var j=0; j<y.length; ++j) y[j].textContent = vars[x[i]];
                           }
                         }
                        </script>
                    </head>
                <body style="border:0; margin: 0;" onload="subst()">
                </body>
                </html>
            """
        css = self.get_css(report_xml, template)
        user = self.pool.get('res.users').browse(cursor, uid, uid)
        company = user.company_id
        parse_template = template
        
        addons_lookup = TemplateLookup(
            directories=[config['addons_path']], input_encoding='utf-8'
        )
        body_mako_tpl = Template(
            parse_template, input_encoding='utf-8', lookup=addons_lookup
        )
        
        report_id = getattr(report_xml, 'id', None)
        helper = WebKitHelper(cursor, uid, report_id, context)

        debugging = (
            DebuggerPatch.debug_enabled() or
            config.get('debug_mode', False) or
            getattr(report_xml, 'webkit_debug', False)
        )

        try:
            html = body_mako_tpl.render(
                helper=helper, css=css, _=self.translate_call,
                **self.parser_instance.localcontext
            )
        except Exception as err:
            if debugging:
                html = html_error_template().render()
            else:
                raise err

        head_mako_tpl = Template(header, input_encoding='utf-8')

        try:
            head = head_mako_tpl.render(
                helper=helper, css=css, _=self.translate_call,
                **self.parser_instance.localcontext
            )
        except Exception as err:
            if debugging:
                head = html_error_template().render()
            else:
                raise err

        foot = False
        if footer:
            foot_mako_tpl = Template(footer, input_encoding='utf-8')
            try:
                foot = foot_mako_tpl.render(
                    helper=helper, css=css, _=self.translate_call,
                    **self.parser_instance.localcontext
                )
            except Exception as err:
                if debugging:
                    foot = html_error_template().render()
                else:
                    raise err

        if getattr(report_xml, 'webkit_debug', False):
            return (html, 'html')

        bin = self.get_lib(cursor, uid, company.id)
        pdf = self.genreate_pdf(
            bin, report_xml, head, foot, [html], context=context
        )
        return (pdf, 'pdf')

    def get_attachment_name(self, cursor, uid, ids, attach, context=None):
        """
        This function returns the name and the object id of the attachment
        :param cursor: DB Cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id
        :type uid: long
        :param ids:
        :type ids: list[long]
        :param attach: report_xml.attachment which defines the attachement name
        :type attach: osv.orm.browse_record
        :param context: OpenERP context.
        :type context: dict
        :return: list of tuples, each one contains the object id and the
        attachment name.
        :rtype: list[(long, str)]
        """
        table_obj = pooler.get_pool(cursor.dbname).get(self.table)
        obj = table_obj.browse(cursor, uid, ids, context=context)
        res = []
        for o in obj:
            res.append((
                o.id, eval(attach, {'object': o, 'time': time})
            ))
        return res

    def get_report_attachment(self, cursor, uid, obj_id, attachment_name):
        """
        Obtains the report attachment.
        :param cursor: DB Cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id
        :type uid: long
        :param obj_id: id of the source object related to the attachment.
        :type obj_id: long
        :param attachment_name: name of the file related to the attachment.
        :type attachment_name: str
        :return: report related to the attachment as a plain file. If not found,
        then False.
        :rtype: str
        """
        res = False
        pool = pooler.get_pool(cursor.dbname)
        attach_o = pool.get('ir.attachment')
        dmn = [('datas_fname', '=', attachment_name),
               ('res_model', '=', self.table),
               ('res_id', '=', obj_id)]
        attach_ids = attach_o.search(cursor, uid, dmn)
        if attach_ids:
            attach_o = pool.get('ir.attachment')
            attach_v = attach_o.read(cursor, uid, attach_ids[0], ['datas'])
            report_coded = attach_v['datas']
            if report:
                plain_report = base64.decodestring(report_coded)
                res = plain_report, 'pdf'

        return res

    def create_report_attachment(self, cursor, uid, obj_id, attach_name, plain_file, context=None):
        """
        Creates an attachment.
        :param cursor: DB Cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id
        :type uid: long
        :param obj_id: id of the object to be printed
        :type obj_id: long
        :param attach_name: attachment's name.
        :type attach_name: str
        :param plain_file: file as plain text.
        :type plain_file: str
        :param context: OpenERP context.
        :type context: dict
        :return:
        """
        pool = pooler.get_pool(cursor.dbname)
        try:
            attachment = {
                'name': attach_name,
                'datas': base64.encodestring(plain_file),
                'datas_fname': attach_name,
                'res_model': self.table,
                'res_id': obj_id,
            }
            pool.get('ir.attachment').create(
                cursor, uid, attachment, context=context)
            cursor.commit()
            res = True

        except Exception as exp:
            import traceback
            import sys
            tb_s = reduce(
                lambda x, y: x+y, traceback.format_exception(sys.exc_type, sys.exc_value, sys.exc_traceback)
            )
            netsvc.Logger().notifyChannel(
                'report',
                netsvc.LOG_ERROR,
                str(exp)
            )
            res = False

        return res

    def join_pdfs(self, plain_pdf_files):
        """
        Joins pdf files.
        :param plain_pdf_files: list of pdf files to be joined.
        :type plain_pdf_files: list[str]
        :return: Joined pdf files as plain text.
        :rtype: str
        """
        if len(plain_pdf_files) == 1:
            res = plain_pdf_files[0]
        else:
            pdf_paths = []
            for plain_pdf in plain_pdf_files:
                attachment_path = tempfile.mkstemp(
                    '-join.pdf', 'report-'
                )[1]
                with open(attachment_path, 'w') as f:
                    f.write(plain_pdf)

                pdf_paths.append(attachment_path)

            pdf_path = pypdftk.concat(files=pdf_paths)

            for tmp_file in pdf_paths:
                os.remove(tmp_file)

            with open(pdf_path, 'r') as full_pdf:
                res = full_pdf.read()

            os.remove(pdf_path)

        return res

    def create_source_webkit(self, cursor, uid, ids, data, report_xml, context=None):
        """
        Obtains the pdf file, either by creating it or loading it from the
        attachments.
        :param cursor: DB Cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id
        :type uid: long
        :param ids:
        :param data:
        :param report_xml:
        :type report_xml: osv.orm.browse_record
        :param context: OpenERP context.
        :type context: dict
        :return: tuple with the report as a plain file and its extension
        :rtype: (str, str)
        """
        if not context:
            context = {}

        attach = report_xml.attachment
        # check if we should generate the attachment
        if attach:
            plain_pdf_files = []
            objs = self.get_attachment_name(cursor, uid, ids, attach, context=context)

            pdfs_from_attach = report_xml.attachment_use and context.get('attachment_use', True)

            for obj_id, attach_name in objs:
                # check if we should return the attachment
                from_attach = pdfs_from_attach and attach_name
                attachment = False
                if from_attach:
                    attachment = self.get_report_attachment(
                        cursor, uid, obj_id, attach_name
                    )
                    if attachment:
                        plain_file, file_extension = attachment
                        if file_extension == 'pdf':
                            plain_pdf_files.append(plain_file)

                if not from_attach or not attachment:
                    plain_file, file_extension = self.create_single_pdf(
                        cursor, uid, [obj_id], data, report_xml, context
                    )
                    if plain_file and file_extension == 'pdf':
                        plain_pdf_files.append(plain_file)
                        self.create_report_attachment(
                            cursor, uid, obj_id, attach_name, plain_file,
                            context=context
                        )

            content = self.join_pdfs(plain_pdf_files)
            result = content, 'pdf'

        else:
            result = self.create_single_pdf(
                cursor, uid, ids, data, report_xml, context
            )

        return result

    def create(self, cursor, uid, ids, data, context=None):
        """We override the create function in order to handle generator
           Code taken from report openoffice. Thanks guys :) """
        pool = pooler.get_pool(cursor.dbname)
        ir_obj = pool.get('ir.actions.report.xml')
        report_xml_ids = ir_obj.search(cursor, uid,
                [('report_name', '=', self.name[7:])], context=context)
        if report_xml_ids:
            
            report_xml = ir_obj.browse(
                                        cursor, 
                                        uid, 
                                        report_xml_ids[0], 
                                        context=context
                                    )
            report_xml.report_rml = None
            report_xml.report_rml_content = None
            report_xml.report_sxw_content_data = None
            report_rml.report_sxw_content = None
            report_rml.report_sxw = None
        else:
            return super(WebKitParser, self).create(cursor, uid, ids, data, context)
        if report_xml.report_type != 'webkit' :
            return super(WebKitParser, self).create(cursor, uid, ids, data, context)
        fnct_ret = self.create_source_webkit(cursor, uid, ids, data, report_xml, context)
        if not fnct_ret:
            return (False,False)
        return fnct_ret


class WebKitParserAppendPDF(WebKitParser):

    def get_attachment_datas(self, pool, cursor, uid, model_id, datas, context=None):
        """
        Obté els PDFs que s'adjuntaran al informe original.
        :param pool:
        :param cursor:
        :param uid:
        :param model_id: identificador del model a imprimir.
        :param datas: Data passed to report
        :param context:
        :return: retorna una llista, on cada valor es un valor preparat per ser
        escrit a un fitxer PDF.
        :rtype: []
        """
        return []

    def create(self, cursor, uid, ids, datas, context=None):
        """
        Imprimeix els reports amb la possibilitat d'afegir un segon pdf segons
        la funció get_attachment_datas.
        :param cursor:
        :param uid:
        :param ids: identificadors dels registres del model a imprimir.
        :param datas: Diccionari amb la següent estructura:
            - id: identificador d'un registre del model. No sé si es fa servir
            ja que quan s'imprimeixen varios registres, només apareix el primer.
            - model: Model del report. p.e: giscedata.polissa
            - report_type: 'pdf' generalment.
        :param context:
        :return:
        """
        if context is None:
            context = {}
        if not isinstance(ids, (tuple, list)):
            ids = [ids]
        pdf_paths = []
        pool = pooler.get_pool(cursor.dbname)
        for model_id in ids:
            res = super(WebKitParserAppendPDF, self).create(
                cursor, uid, [model_id], datas, context=context
            )
            if res[1] != 'pdf':
                continue
            orig_pdf_path = tempfile.mkstemp('-join.pdf', 'report-')[1]
            with open(orig_pdf_path, 'w') as f:
                f.write(res[0])

            # generate de 2nd page
            attachments = self.get_attachment_datas(
                pool, cursor, uid, model_id, datas, context
            )
            if attachments:
                attachments_paths = []
                for attachment in attachments:
                    attachment_path = tempfile.mkstemp(
                        '-join.pdf', 'report-'
                    )[1]
                    with open(attachment_path, 'w') as f:
                        f.write(attachment)

                    attachments_paths.append(attachment_path)

                pdf_path = pypdftk.concat(
                    files=[orig_pdf_path] + attachments_paths
                )
                os.remove(orig_pdf_path)
                for file_path in attachments_paths:
                    os.remove(file_path)

                pdf_paths.append(pdf_path)
            else:
                pdf_paths.append(orig_pdf_path)

        if len(pdf_paths) > 1:
            final_pdf_path = pypdftk.concat(files=pdf_paths)
            for file_path in pdf_paths:
                os.remove(file_path)
        else:
            final_pdf_path = pdf_paths[0]

        with open(final_pdf_path, 'r') as f:
            final_pdf_bin = f.read()

        os.remove(final_pdf_path)

        return final_pdf_bin, 'pdf'


def write_file_and_get_path(res):
    """
    Writes the report in a temporary file and returns its path.
    :param res: the result call of the the WebKit Parser create function.
    :type res: (str, str)
    :return: tuple where the first value is the file path and the second value
    is the file type (pdf, html, etc).
    :rtype: (str, str)
    """
    file_type = res[1]
    tmp_file_path = tempfile.mkstemp(
        '-join.'.format(file_type), 'report-'
    )[1]
    with open(tmp_file_path, 'w') as f:
        f.write(res[0])

    return tmp_file_path, file_type


def concat_files(file_paths, file_type):
    """
    Obtains a file result of the concatenation of the passed files.
    :param file_paths: list of file paths to concatenate
    :type file_paths: list[str]
    :param file_type: type of the files (pdf, html, etc).
    :type file_type: str
    :return: the file concatenated
    :rtype: str
    """
    if file_type == 'pdf' and len(file_paths) > 1:
        final_file_path = pypdftk.concat(files=file_paths)
        for file_path in file_paths:
            os.remove(file_path)
    else:
        # TODO (improvement). Create an 'index' that references the multiple
        # html files when debug is marked. Currently, it only returns the
        # first html report.
        final_file_path = file_paths[0]

    with open(final_file_path, 'r') as f:
        final_file = f.read()

    os.remove(final_file_path)

    return final_file


class WebKitParserNumberPages(WebKitParser):

    def create(self, cursor, uid, ids, datas, context=None):
        """
        Creates the PDF one by one instead of all at the same time. It's less
        efficient because it writtes n PDF files and concatenates them as the
        final PDF.
        It's only useful when the page numbering is needed.
        NOTE: when debug is marked, it only returns the 'first' result.
        :param cursor: DB Cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param ids:
        :param datas:
        :param context: OpenERP context.
        :type context: dict
        :return: tuple where the first value is the file content and the second
        is the the file type.
        :rtype: (str, str)
        """
        if context is None:
            context = {}

        if not isinstance(ids, (tuple, list)):
            ids = [ids]

        file_type = 'pdf'
        pdf_paths = []

        for model_id in ids:
            res = super(WebKitParserNumberPages, self).create(
                cursor, uid, [model_id], datas, context=context
            )
            orig_pdf_path, file_type = write_file_and_get_path(res)
            pdf_paths.append(orig_pdf_path)

        final_pdf_bin = concat_files(pdf_paths, file_type)

        return final_pdf_bin, file_type


def register_report(name, model, tmpl_path, parser):
    name = 'report.%s' % name
    if netsvc.service_exist( name ):
        #service = netsvc.SERVICES[name].parser
        if isinstance( netsvc.SERVICES[name], WebKitParser ):
            return
        del netsvc.SERVICES[name]
    WebKitParser(name, model, tmpl_path, parser=parser)
    
old_register_all = report.interface.register_all

def new_register_all(db):
    value = old_register_all(db)
    cursor = db.cursor()
    cursor.execute(
        "SELECT * FROM ir_act_report_xml WHERE report_type ='webkit'"
    )
    records = cursor.dictfetchall()
    cursor.close()
    for record in records:
        parser = rml_parse
        register_report(
            record['report_name'],
            record['model'],
            record['report_rml'], parser
        )
    return value

report.interface.register_all = new_register_all
