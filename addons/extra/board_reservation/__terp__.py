{
	"name":"Board for Hotel Reservation",
	"version":"1.0",
	"author":"Tiny",
	"category":"Board/Hotel Reservation",
	"depends":[
		"board",
		"report_hotel_reservation",
	],
	"demo_xml":[],
	"update_xml":["board_reservation_view.xml"],
	"description": """
This module implements a dashboard for hotel Reservation that includes:
	* List of Today's checkin
	* List of Today's chekout
	* Graph of Reservation by states(pie)
	* Graph of reservation by states(bar)
	""",
	"active":False,
	"installable":True,
}
