# Catalan translation for openobject-addons
# Copyright (c) 2009 Rosetta Contributors and Canonical Ltd 2009
# This file is distributed under the same license as the openobject-addons package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: openobject-addons\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2009-12-06 13:36+0000\n"
"PO-Revision-Date: 2009-12-07 06:02+0000\n"
"Last-Translator: Jordi Esteve (www.zikzakmedia.com) "
"<jesteve@zikzakmedia.com>\n"
"Language-Team: Catalan <ca@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2010-08-10 03:47+0000\n"
"X-Generator: Launchpad (build Unknown)\n"

#. module: wiki_extension
#: constraint:ir.ui.view:0
msgid "Invalid XML for View Architecture!"
msgstr "﻿XML invàlid per a la definició de la vista!"

#. module: wiki_extension
#: code:addons/wiki_extension/object/wiki.py:0
#, python-format
msgid "Read Only"
msgstr "Només lectura"

#. module: wiki_extension
#: view:wiki.groups:0
msgid "Members"
msgstr "Membres"

#. module: wiki_extension
#: model:ir.actions.act_window,name:wiki_extension.action_wiki_extension
msgid "Wiki Pages"
msgstr "Pàgines Wiki"

#. module: wiki_extension
#: model:ir.module.module,shortdesc:wiki_extension.module_meta_information
msgid "Document Management - Wiki Extension"
msgstr "Gestió documental - Extensió de la Wiki"

#. module: wiki_extension
#: model:ir.module.module,description:wiki_extension.module_meta_information
msgid ""
"\n"
"    The base module to manage documents(wiki) \n"
"\n"
"    Add new features :\n"
"    - members in wiki groups\n"
"    - state in page wiki (Running, Raed Only, Obsolete)\n"
"    - improvement view\n"
"    "
msgstr ""
"\n"
"    Estén el modul bàsic per a gestionar documents (Wiki) \n"
"\n"
"    Afegeix noves funcionalitats :\n"
"    - Grups de membres en la Wiki\n"
"    - Estat de les pàgines Wiki (En progrés, Només lectura, Obsolet)\n"
"    - Vista millorada\n"
"    "

#. module: wiki_extension
#: code:addons/wiki_extension/object/wiki.py:0
#, python-format
msgid "Obsolete"
msgstr "Obsolet"

#. module: wiki_extension
#: code:addons/wiki_extension/object/wiki.py:0
#, python-format
msgid "Running"
msgstr "En progrés"

#. module: wiki_extension
#: constraint:ir.actions.act_window:0
msgid "Invalid model name in the action definition."
msgstr "Nom de model no vàlid en la definició de l'acció."

#. module: wiki_extension
#: view:wiki.groups:0
msgid "Configuration"
msgstr "Configuració"
