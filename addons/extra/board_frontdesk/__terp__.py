{
    "name":"Board for Hotel FrontDesk",
    "version":"1.0",
    "author":"Tiny",
    "category":"Board/Hotel FrontDesk",
    "depends":[
        "board",
        "report_hotel_reservation",
        
        
    ],
    "demo_xml":[],
    "update_xml":["board_frontdesk_view.xml"],
    "description": """
This module implements a dashboard for hotel FrontDesk that includes:
    * Calendar view of Today's Check-In and Check-Out
    * Calendar view of Weekly Check-In and Check-Out
    * Calendar view of Monthly Check-In and Check-Out
    """,
    "active":False,
    "installable":True,
}