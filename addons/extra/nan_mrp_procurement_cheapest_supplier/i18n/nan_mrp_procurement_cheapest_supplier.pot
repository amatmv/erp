# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* nan_mrp_procurement_cheapest_supplier
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.12\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2010-08-31 07:03:22+0000\n"
"PO-Revision-Date: 2010-08-31 07:03:22+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: nan_mrp_procurement_cheapest_supplier
#: model:ir.module.module,description:nan_mrp_procurement_cheapest_supplier.module_meta_information
msgid "This module selects the cheapest supplier for a product when purchase orders are created from procurements. By default OpenERP will select the partner with lowest sequence number."
msgstr ""

#. module: nan_mrp_procurement_cheapest_supplier
#: model:ir.module.module,shortdesc:nan_mrp_procurement_cheapest_supplier.module_meta_information
msgid "Procurement Cheapest Supplier"
msgstr ""

