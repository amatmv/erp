# Catalan translation for openobject-addons
# Copyright (c) 2010 Rosetta Contributors and Canonical Ltd 2010
# This file is distributed under the same license as the openobject-addons package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2010.
#
msgid ""
msgstr ""
"Project-Id-Version: openobject-addons\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2009-11-12 17:15+0000\n"
"PO-Revision-Date: 2010-09-25 14:07+0000\n"
"Last-Translator: Jordi Esteve (www.zikzakmedia.com) "
"<jesteve@zikzakmedia.com>\n"
"Language-Team: Catalan <ca@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2010-09-29 05:32+0000\n"
"X-Generator: Launchpad (build Unknown)\n"

#. module: product_category_extended
#: view:product.category:0
msgid "Information"
msgstr "Informació"

#. module: product_category_extended
#: constraint:ir.ui.view:0
msgid "Invalid XML for View Architecture!"
msgstr "XML no vàlid per a l'estructura de la vista!"

#. module: product_category_extended
#: field:product.category,uos_id:0
msgid "Unit of Sale"
msgstr "Unitat de venda"

#. module: product_category_extended
#: field:product.category,purchase_taxes_ids:0
msgid "Purchase Taxes"
msgstr "Impostos de compres"

#. module: product_category_extended
#: field:product.category,uom_po_id:0
msgid "Purchase UoM"
msgstr "UdM de compra"

#. module: product_category_extended
#: view:product.category:0
msgid "Secondary Unit (UoS)"
msgstr "Unitat secundaria (UdV)"

#. module: product_category_extended
#: model:ir.module.module,description:product_category_extended.module_meta_information
msgid ""
"\n"
"    Add On Change on product category to:\n"
"    * Fill sales and purchase taxes on product.\n"
"    * Add default UOM on product\n"
"    "
msgstr ""
"\n"
"    Afegeix mètode \"On Change\" (al canviar) a la categoria de producte per "
"a:\n"
"    * Omplir impostos de venda i compra en el producte\n"
"    * Afegir UdM per defecte en el producte\n"
"    "

#. module: product_category_extended
#: view:product.category:0
msgid "Sales taxes"
msgstr "Impostos de venda"

#. module: product_category_extended
#: view:product.category:0
msgid "Purchases taxes"
msgstr "Impostos de compra"

#. module: product_category_extended
#: field:product.category,uom_id:0
msgid "Default UoM"
msgstr "UdM per defecte"

#. module: product_category_extended
#: view:product.category:0
msgid "General"
msgstr "General"

#. module: product_category_extended
#: view:product.category:0
msgid "Sale and Purchase Unit (UoM)"
msgstr "Unitat de compra i venda (UdM)"

#. module: product_category_extended
#: code:addons/product_category_extended/object/product.py:0
#, python-format
msgid "Caution"
msgstr "Atenció"

#. module: product_category_extended
#: help:product.category,uos_coeff:0
#: help:product.category,uos_id:0
msgid "See product definition"
msgstr "Veure definició del producte"

#. module: product_category_extended
#: field:product.category,uos_coeff:0
msgid "UOM -> UOS Coeff"
msgstr "Coef. UdM -> UdV"

#. module: product_category_extended
#: code:addons/product_category_extended/object/product.py:0
#, python-format
msgid ""
"\"\"The product category have been changed, thanks to control\n"
"* Sales and Purchases Taxes\n"
"* Unit sales and stock\n"
"* The price with return unit\"\""
msgstr ""
"La categoria de producte s'ha canviat, comproveu:\n"
"* Els impostos de compra i venda\n"
"* La unitat de venda i estoc\n"
"* El preu amb retorn unitat"

#. module: product_category_extended
#: field:product.category,sale_taxes_ids:0
msgid "Sales Taxes"
msgstr "Impostos de vendes"

#. module: product_category_extended
#: model:ir.module.module,shortdesc:product_category_extended.module_meta_information
msgid "Product Category Extended"
msgstr "Categoría de producto estesa"
