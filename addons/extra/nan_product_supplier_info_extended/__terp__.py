{
	"name" : "Product Supplier Info Extended",
	"version" : "0.1",
	"description" : """Product Supplier Info Extended""",
	"author" : "NaN for Trod y Avia, S.L.",
	"website" : "http://www.NaN-tic.com",
	"depends" : [ 'product' ], 
	"category" : "Custom Modules",
	"init_xml" : [],
	"demo_xml" : [],
	"update_xml" : [ 'product_view.xml' ],
	"active": False,
	"installable": True
}
