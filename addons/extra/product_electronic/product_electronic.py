# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution	
#    Copyright (C) 2004-2008 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    $Id$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import fields,osv

class product_product(osv.osv):
    _inherit = 'product.product'
    _name = 'product.product'
    _columns = {
        'manufacturer' : fields.many2one('res.partner', 'Manufacturer'),
        'manufacturer_pname' : fields.char('Manufacturer product name', size=64),
        'manufacturer_pref' : fields.char('Manufacturer product code', size=64),
        'attribute_ids': fields.one2many('product.electronic.attribute', 'product_id', 'Attributes'),
    }
product_product()

class product_attribute(osv.osv):
    _name = "product.electronic.attribute"
    _description = "Product attributes"
    _columns = {
        'name' : fields.char('Attribute', size=64, required=True),
        'value' : fields.char('Value', size=64),
        'product_id': fields.many2one('product.product', 'Product', ondelete='cascade'),
    }
product_attribute()


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

