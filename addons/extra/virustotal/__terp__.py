{
    'name': "Allow attachments to be checked via virustotal.com",
    'version': '1.0',
    'author': 'Christophe Simonis',
    'description': 'This module allow users to send any attachment to virustotal.com in order to be checked with latest antivirus engines',
    'depends': ['base'],
    'init_xml': [],
    'update_xml': ['attachment_view.xml'],
    'active': False,
    'installable': True,
}
