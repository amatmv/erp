* Implemented sftp protocall using paramiko-1.7.4
* Inherits 'res.users' model to add new field 'ssh_key' for user public key 
* FIX in code :
        - host : 'localhost',
        - port : 8022, 
        - allowed auths : 'publickey'

* Require python module : 
        - paramiko lib (http://www.lag.net/paramiko/?ref=darwinports.com) 


