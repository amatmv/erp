# -*- coding: utf-8 -*-

from expects import expect, equal
from destral import testing
from destral.transaction import Transaction


class TestPartner(testing.OOTestCase):

    def test_get_default_bank_returns_default_bank(self):
        """
        Test function get_default_bank returns the default bank for the partner
        """
        partner_obj = self.openerp.pool.get('res.partner')
        partner_bank_obj = self.openerp.pool.get('res.partner.bank')
        imd_obj = self.openerp.pool.get('ir.model.data')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            partner_bank_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'res_partner_bank_0002'
            )[1]
            partner_bank = partner_bank_obj.browse(cursor, uid, partner_bank_id)
            default_bank_id = partner_obj.get_default_bank(
                cursor, uid, partner_bank.partner_id.id)
            # Expect the function to return the default partner bank
            expect(default_bank_id).to(equal(partner_bank_id))
