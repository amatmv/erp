# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution	
#    Copyright (C) 2004-2008 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    $Id$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    "name" : "Auto Email Stock Picking - Send email to customer and supplier from picking",
    "version" : "1.0",
    "depends" : ["smtpclient","stock"],
    "author" : "Tiny",
    "description": """Use Email Client module 
    to Send the Automatic Sales Order to the 
    Customer or supplier by Email 
    when the Picking order confirm
    """,
    "website" : "http://www.openerp.com",
    "category" : "Generic Modules",
    "init_xml" : [
        
    ],
    "demo_xml" : [
    ],
    "update_xml" : [
        "smtpclient_wizard.xml"
    ],
    "active": False,
    "installable": True,

}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

