{
    "name" : "Module to  extracts the schema structure",
    "version" : "0.1",
    "author" : "Tiny",
    "website" : "http://www.openerp.com",
    "depends" : ["olap"],
    "category" : "Generic Modules/Olap",
    "description": """
    Extracts the schema structure.
    """,
    "init_xml" :  [],
    "update_xml" : ["olap_extract_wizard.xml"],
    "demo_xml" : [],
    "active": False,
    "installable": True
}
