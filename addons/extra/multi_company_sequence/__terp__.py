# -*- encoding: utf-8 -*-

{
    "name" : "MultiCompany Sequence",
    "version" : "1.1",
    "depends" : [
                    "base", 
                ],
    "author" : "Axelor",
    "description": """Multi company Sequence Module
    """,
    'website': 'http://www.axelor.com',
    'init_xml': [],
    'update_xml': [
                
        'multi_company_sequence_view.xml',

    ],
    'demo_xml': [],
    'installable': True,
    'active': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
