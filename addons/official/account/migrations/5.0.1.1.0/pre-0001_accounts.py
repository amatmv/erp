# -*- coding: utf-8 -*-
def migrate(cursor, installed_version):
    # Les comptes ja no es creen a través dels fitxers _data si no que van
    # amb les templates
    cursor.execute("DELETE FROM ir_model_data where model = 'account.account'")
    
    # Comprovem que existeix account_account
    cursor.execute("""SELECT relname FROM pg_class WHERE relkind in ('r','v')
    AND relname=%s""", ('account_account',))
    if not cursor.rowcount:
        return
    # Renombrem comptes antigues que hi tinguem assentaments, ja que no ho
    # fèiem bé
    cursor.execute("""update account_account SET code='OLD_' || code 
    where id in (select id from account_account where id in
    (SELECT distinct account_id from account_move_line))""")

    cursor.execute("""update account_account SET code='OLD_' || code 
    where id in (select id from account_account where id in
    (SELECT distinct account_id from account_invoice_line))""")

    cursor.execute("""update account_account SET code='OLD_' || code 
    where id in (select id from account_account where id in
    (SELECT distinct account_id from account_invoice))""")

    # Desactivem les taxes antigues
    cursor.execute("UPDATE account_tax SET active = False")
