# coding=utf-8

import logging


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')

    logger.info(
        'Migration for saldo field in invoices')

    query_rename = '''
        UPDATE account_invoice as ori SET saldo =  
        CASE
            WHEN rectificative_type = 'RA' THEN (amount_total - (SELECT amount_total FROM account_invoice WHERE id = ori.rectifying_id))
            ELSE amount_total 
        END
        WHERE (date_invoice >= '2017-08-01' and (state IN ('open', 'paid')));
    '''

    cursor.execute(query_rename)


def down(cursor, installed_version):
    pass

migrate = up
