# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    $Id$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import time
from operator import itemgetter

import netsvc
from osv import fields, osv
from osv.orm import except_orm
import pooler
from tools import config, float_round
from tools.translate import _

INVOICE_STATE_SELECTION = [
    ('draft', 'Draft'),
    ('proforma', 'Accounting Pro-forma'),
    ('proforma2', 'Pro-forma'),
    ('open', 'Open'),
    ('paid', 'Done'),
    ('cancel', 'Cancelled')
]

RECTIFICATIVE_TYPE_SELECTION = [
    ('N', 'Normal'),
    ('R', 'Rectificative'),
    ('A', 'Nullifier'),
    ('B', 'Nullifier with substitution'),
    ('C', 'Complementary'),
    ('G', 'Regularizer'),
    ('RA', 'Rectificative without Nullifier'),
    ('BRA', 'Nullifier fake')
]

NULLIFIER_TYPES = ['A', 'B', 'RA']


class fiscalyear_seq(osv.osv):
    _name = "fiscalyear.seq"
    _description = "Maintains Invoice sequences with Fiscal Year"
    _rec_name = 'fiscalyear_id'
    _columns = {
        'journal_id': fields.many2one('account.journal', 'Journal'),
        'fiscalyear_id': fields.many2one('account.fiscalyear', 'Fiscal Year',required=True),
        'sequence_id':fields.many2one('ir.sequence', 'Sequence',required=True),
    }

fiscalyear_seq()

class account_invoice(osv.osv):
    def _amount_all(self, cr, uid, ids, name, args, context=None):
        res = {}
        for invoice in self.browse(cr,uid,ids, context=context):
            res[invoice.id] = {
                'amount_untaxed': 0.0,
                'amount_tax': 0.0,
                'amount_total': 0.0
            }
            for line in invoice.invoice_line:
                res[invoice.id]['amount_untaxed'] += line.price_subtotal
            for line in invoice.tax_line:
                res[invoice.id]['amount_tax'] += line.amount
            res[invoice.id]['amount_total'] = res[invoice.id]['amount_tax'] + res[invoice.id]['amount_untaxed']
        return res

    def _get_journal(self, cr, uid, context):
        if context is None:
            context = {}
        type_inv = context.get('type', 'out_invoice')
        type2journal = {'out_invoice': 'sale', 'in_invoice': 'purchase', 'out_refund': 'sale', 'in_refund': 'purchase'}
        refund_journal = {'out_invoice': False, 'in_invoice': False, 'out_refund': True, 'in_refund': True}
        journal_obj = self.pool.get('account.journal')
        res = journal_obj.search(cr, uid, [('type', '=', type2journal.get(type_inv, 'sale')), ('refund_journal', '=', refund_journal.get(type_inv, False))], limit=1)
        if res:
            return res[0]
        else:
            return False

    def _get_currency(self, cr, uid, context):
        user = pooler.get_pool(cr.dbname).get('res.users').browse(cr, uid, [uid])[0]
        if user.company_id:
            return user.company_id.currency_id.id
        else:
            return pooler.get_pool(cr.dbname).get('res.currency').search(cr, uid, [('rate','=',1.0)])[0]

    def _get_journal_analytic(self, cr, uid, type_inv, context=None):
        type2journal = {'out_invoice': 'sale', 'in_invoice': 'purchase', 'out_refund': 'sale', 'in_refund': 'purchase'}
        tt = type2journal.get(type_inv, 'sale')
        result = self.pool.get('account.analytic.journal').search(cr, uid, [('type','=',tt)], context=context)
        if not result:
            raise osv.except_osv(_('No Analytic Journal !'),_("You must define an analytic journal of type '%s' !") % (tt,))
        return result[0]

    def _get_type(self, cr, uid, context=None):
        if context is None:
            context = {}
        type = context.get('type', 'out_invoice')
        return type

    def _reconciled(self, cr, uid, ids, name, args, context):
        res = {}
        for id in ids:
            res[id] = self.test_paid(cr, uid, [id])
        return res

    def _get_reference_type(self, cr, uid, context=None):
        return [('none', _('Free Reference'))]

    def copy_data(self, cr, uid, ids, default=None, context=None):
        if default is None:
            default = {}
        data, x = super(account_invoice, self).copy_data(cr, uid, ids, default, context)
        data.update({
            'origin': False,
            'origin_date_invoice': False,
            'reference': False,
            'period_id': False
        })
        return data, x

    def is_not_payable_manually(self, cursor, uid, invoice_id, context=None):
        """
        Method that returns if an invoice is NOT payable. If the invoice is not
        payable it returns a dictionary representing an error in the following
        format:

        {
            'title': error_title,
            'message': error_message
        }
        """
        if context is None:
            context = {}
        if isinstance(invoice_id, (list, tuple)):
            invoice_id = invoice_id[0]

        is_not_payable = False
        invoice = self.browse(cursor, uid, invoice_id)
        if invoice.state != 'open' or invoice.reconciled:
            is_not_payable = {
                'title': _('You cannot pay invoices already paid!'),
                'message': _(
                    'The invoice {} is already paid and cannot be paid again'
                ).format(invoice.number)
            }
        return is_not_payable

    @staticmethod
    def get_invoice_sign(invoice):
        if invoice.type.endswith('refund'):
            return -1
        return 1

    def get_sign(self, cursor, uid, invoice_id, context=None):
        if context is None:
            context = {}
        if isinstance(invoice_id, (list, tuple)):
            invoice_id = invoice_id[0]

        invoice = self.read(cursor, uid, invoice_id, ['type'])
        if invoice['type'].endswith('refund'):
            return -1
        return 1

    def defer_payment(self, cursor, uid, invoice_id, journal_id, deferred_date,
                      context=None):
        """Changes due date of an invoice and makes account moves to make
        traceability
        """
        if context is None:
            context = {}

        ml_obj = self.pool.get('account.move.line')
        move_obj = self.pool.get('account.move')
        wf_service = netsvc.LocalService('workflow')
        rec_obj = self.pool.get('account.move.reconcile')

        if isinstance(invoice_id, list):
            assert len(invoice_id) == 1
            invoice_id = invoice_id[0]

        invoice = self.browse(cursor, uid, invoice_id)
        if invoice.state != 'open':
            raise osv.except_osv(
                _('Error'),
                _('Invoice must be open to postpone the payment')
            )

        payment_move_ids = invoice.move_line_id_payment_get()
        if len(payment_move_ids) > 1:
            raise osv.except_osv(
                _('Error'),
                _('Postpone fractioned payments are not supported')
            )
        invoice.write({'date_due': deferred_date})
        lines_to_reconcile = [x.id for x in invoice.payment_ids]
        ml = ml_obj.browse(cursor, uid, payment_move_ids[0], context=context)
        move_id = move_obj.create(cursor, uid, {
            'journal_id': journal_id,
            'line_id': [
                (0, 0, {
                    'name': _('Payment deferred'),
                    'ref': ml.ref,
                    'partner_id': ml.partner_id.id,
                    'credit': ml.debit,
                    'debit': ml.credit,
                    'quantity': ml.quantity,
                    'account_id': ml.account_id.id,
                    'journal_id': ml.journal_id.id,
                    'date_maturity': deferred_date
                }),
                (0, 0, {
                    'name': _('Payment deferred'),
                    'ref': ml.ref,
                    'partner_id': ml.partner_id.id,
                    'credit': ml.credit,
                    'debit': ml.debit,
                    'quantity': ml.quantity,
                    'account_id': ml.account_id.id,
                    'journal_id': ml.journal_id.id,
                    'date_maturity': deferred_date
                })
            ]
        })
        move = move_obj.browse(cursor, uid, move_id)
        move.button_validate()
        for line in move.line_id:
            if line.account_id.id == ml.account_id.id:
                lines_to_reconcile.append(line.id)
        lines_to_reconcile += self.move_line_id_payment_get(
            cursor, uid, [invoice.id]
        )
        # We have to unreconcile current payments
        rec_ids = []
        for payment_line in invoice.payment_ids:
            if payment_line.reconcile_id:
                rec_ids.append(payment_line.reconcile_id.id)
            if payment_line.reconcile_partial_id:
                rec_ids.append(payment_line.reconcile_partial_id.id)
        rec_obj.unlink(cursor, uid, rec_ids)
        ctx = context.copy()

        ctx['force_different_account'] = True
        ml_obj.reconcile_partial(
            cursor, uid, lines_to_reconcile, 'manual', ctx
        )
        # Update the stored value (fields.function), so we write to trigger recompute
        wf_service.trg_validate(uid, 'account.invoice', invoice.id,
                                'open_test', cursor)
        ml_obj.write(cursor, uid, invoice.move_line_id_payment_get(), {})
        invoice.write({})
        return True

    def _amount_residual(self, cr, uid, ids, name, args, context=None):
        res = {}
        data_inv = self.browse(cr, uid, ids)
        cur_obj = self.pool.get('res.currency')
        for inv in data_inv:
            debit = credit = 0.0
            context_unreconciled=context.copy()
            # If one of the invoice line is not in the currency of the invoice,
            # we use the currency of the company to compute the residual amount.
            # All the lines must use the same currency source (company or invoice)
            fromcompany = False
            for lines in inv.move_lines:
                if lines.currency_id.id <> inv.currency_id.id:
                    fromcompany = True
                    break

            for lines in inv.move_lines:
                # If currency conversion needed
                if fromcompany:
                    context_unreconciled.update({'date':lines.date})
                    # If amount currency setted, compute for debit and credit in company currency
                    if lines.credit:
                        credit += abs(cur_obj.compute(cr, uid, inv.company_id.currency_id.id, inv.currency_id.id, lines.credit, round=False,context=context_unreconciled))
                    else:
                        debit += abs(cur_obj.compute(cr, uid, inv.company_id.currency_id.id, inv.currency_id.id, lines.debit, round=False,context=context_unreconciled))
                else:
                    debit += lines.debit > 0 and abs(lines.amount_currency) or  0.00
                    credit += lines.credit > 0 and abs(lines.amount_currency) or  0.00
            if inv.type in ('out_invoice','in_refund'):
                amount = credit-debit
            else:
                amount = debit-credit

            in_draft = inv.state == 'draft'
            rectif_anul = inv.rectificative_type == 'RA'
            original_amount = 0
            if in_draft and rectif_anul and inv.rectifying_id:
                # This is in order to calculate the residual correctly while we
                # are still in draft
                from_currency = inv.rectifying_id.currency_id.id
                to_currency = inv.currency_id.id
                original_amount += cur_obj.compute(
                    cr, uid, from_currency, to_currency,
                    inv.rectifying_id.amount_total, round=False,
                    context={'date': inv.date_invoice}
                )

            result = inv.amount_total - amount
            residual = self.pool.get('res.currency').round(cr, uid, inv.currency_id, result)
            res[inv.id] = residual

            saldo = inv.amount_total - original_amount
            saldo = self.pool.get('res.currency').round(cr, uid, inv.currency_id, saldo)

            # Only doing this for 'out' invoices because if we do this for
            # 'in' invoices we will be overwitting the value of the imported F1
            if inv.state == 'draft' and inv.type.startswith('out'):
                inv.write({'saldo': saldo})

        return res

    def _get_lines(self, cr, uid, ids, name, arg, context=None):
        res = {}
        for id in ids:
            move_lines = self.move_line_id_payment_get(cr,uid,[id])
            if not move_lines:
                res[id] = []
                continue
            res[id] = []
            data_lines = self.pool.get('account.move.line').browse(cr,uid,move_lines)
            partial_ids = []# Keeps the track of ids where partial payments are done with payment terms
            for line in data_lines:
                ids_line = []
                if line.reconcile_id:
                    ids_line = line.reconcile_id.line_id
                elif line.reconcile_partial_id:
                    ids_line = line.reconcile_partial_id.line_partial_ids
                l = map(lambda x: x.id, ids_line)
                partial_ids.append(line.id)
                res[id] =[x for x in l if x <> line.id and x not in partial_ids]
        return res

    def _get_invoice_line(self, cr, uid, ids, context=None):
        result = {}
        for line in self.pool.get('account.invoice.line').browse(cr, uid, ids, context=context):
            result[line.invoice_id.id] = True
        return result.keys()

    def _get_invoice_tax(self, cr, uid, ids, context=None):
        result = {}
        for tax in self.pool.get('account.invoice.tax').browse(cr, uid, ids, context=context):
            result[tax.invoice_id.id] = True
        return result.keys()

    def _compute_lines(self, cr, uid, ids, name, args, context=None):
        if context is None:
            context = {}
        result = {}
        for invoice in self.browse(cr, uid, ids, context):
            moves = self.move_line_id_payment_get(cr, uid, [invoice.id])
            src = []
            lines = []
            for m in self.pool.get('account.move.line').browse(cr, uid, moves, context):
                temp_lines = []#Added temp list to avoid duplicate records
                if m.reconcile_id:
                    temp_lines = map(lambda x: x, m.reconcile_id.line_id)
                elif m.reconcile_partial_id:
                    temp_lines = map(lambda x: x, m.reconcile_partial_id.line_partial_ids)
                if context.get("avoid_0_lines"):
                    lines += [x.id for x in temp_lines if x.id not in lines and abs(x.debit) + abs(x.credit) != 0]
                else:
                    lines += [x.id for x in temp_lines if x.id not in lines]
                src.append(m.id)
            lines = filter(lambda x: x not in src, lines)
            result[invoice.id] = lines
        return result

    def _get_invoice_from_line(self, cr, uid, ids, context={}):
        move = {}
        for line in self.pool.get('account.move.line').browse(cr, uid, ids):
            if line.reconcile_partial_id:
                for line2 in line.reconcile_partial_id.line_partial_ids:
                    move[line2.move_id.id] = True
            if line.reconcile_id:
                for line2 in line.reconcile_id.line_id:
                    move[line2.move_id.id] = True
        invoice_ids = []
        if move:
            invoice_ids = self.pool.get('account.invoice').search(cr, uid, [('move_id','in',move.keys())], context=context)
        return invoice_ids

    def _get_invoice_from_reconcile(self, cr, uid, ids, context={}):
        move = {}
        for r in self.pool.get('account.move.reconcile').browse(cr, uid, ids):
            for line in r.line_partial_ids:
                move[line.move_id.id] = True
            for line in r.line_id:
                move[line.move_id.id] = True
        
        invoice_ids = []
        if move:
            invoice_ids = self.pool.get('account.invoice').search(cr, uid, [('move_id','in',move.keys())], context=context)
        return invoice_ids

    def _trg_refund_by(self, cursor, uid, ids, context=None):
        rectifying_ids = {
            inv_vals['rectifying_id'][0]
            for inv_vals in self.read(
                cursor, uid, ids, ['rectifying_id', 'rectificative_type']
            )
            if (
                inv_vals['rectifying_id']
                and inv_vals['rectificative_type'] in NULLIFIER_TYPES
            )
        }
        refunded_ids = set(
            self.search(cursor, uid, [('refund_by_id', 'in', ids)])
        )

        return list(rectifying_ids.union(refunded_ids))

    def _ff_get_refund_by(self, cursor, uid, ids, field, arg, context=None):
        if context is None:
            context = {}

        res = {}
        for inv_id in ids:
            refund_by = self.search(
                cursor, uid, [
                    ('rectifying_id', '=', inv_id),
                    ('rectificative_type', 'in', NULLIFIER_TYPES),
                ]
            )

            if len(refund_by) == 1:
                res[inv_id] = refund_by[0]
            else:
                res[inv_id] = False

        return res

    _STORE_REFUND_BY = {
        'account.invoice': (
            _trg_refund_by, ['rectificative_type', 'rectifying_id'], 10
        ),
    }

    _name = "account.invoice"
    _description = 'Invoice'
    _order = "number"
    _columns = {
        'name': fields.char('Description', size=64, select=True,readonly=True, states={'draft':[('readonly',False)]}),
        'origin': fields.char(
            'Origin', size=64, select=True, readonly=True,
            states={'draft': [('readonly', False)]},
            help="Reference of the document that produced this invoice.",
        ),
        'origin_date_invoice': fields.date(
            'Original Invoice Date', readonly=True,
            states={'draft': [('readonly', False)]},
            help="Date when the original invoice was opened"
        ),
        'type': fields.selection([
            ('out_invoice', 'Customer Invoice'),
            ('in_invoice', 'Supplier Invoice'),
            ('out_refund', 'Customer Refund'),
            ('in_refund', 'Supplier Refund'),
            ],
            'Type',
            readonly=True,
            select=True),

        'number': fields.char('Invoice Number', size=32, readonly=True, help="Unique number of the invoice, computed automatically when the invoice is created."),
        'reference': fields.char('Invoice Reference', size=64, help="The partner reference of this invoice."),
        'reference_type': fields.selection(_get_reference_type, 'Reference Type',
            required=True),
        'comment': fields.text('Additional Information'),

        'state': fields.selection(
            INVOICE_STATE_SELECTION,'State', select=True, readonly=True
        ),

        'date_invoice': fields.date(
            'Date Invoiced',
            states={
                'open': [('readonly', True)],
                'paid': [('readonly', True)]
            }, help="Keep empty to use the current date", select=True,
        ),
        'date_due': fields.date('Due Date', states={'open':[('readonly',True)],'close':[('readonly',True)]},
            help="If you use payment terms, the due date will be computed automatically at the generation "\
                "of accounting entries. If you keep the payment term and the due date empty, it means direct payment. The payment term may compute several due dates, for example 50% now, 50% in one month."),
        'partner_id': fields.many2one(
            'res.partner',
            'Partner',
            ondelete='restrict',
            change_default=True,
            readonly=True,
            required=True,
            states={'draft':
                        [('readonly', False)]
                    },
            select=1
        ),
        'address_contact_id': fields.many2one('res.partner.address', 'Contact Address', ondelete='restrict', readonly=True, states={'draft':[('readonly',False)]}),
        'address_invoice_id': fields.many2one('res.partner.address', 'Invoice Address', ondelete='restrict', readonly=True, required=True, states={'draft':[('readonly',False)]}),
        'payment_term': fields.many2one('account.payment.term', 'Payment Term',readonly=True, states={'draft':[('readonly',False)]},
            help="If you use payment terms, the due date will be computed automatically at the generation "\
                "of accounting entries. If you keep the payment term and the due date empty, it means direct payment. "\
                "The payment term may compute several due dates, for example 50% now, 50% in one month."),
        'period_id': fields.many2one('account.period', 'Force Period', domain=[('state','<>','done')], help="Keep empty to use the period of the validation(invoice) date.", readonly=True, states={'draft':[('readonly',False)]}),

        'account_id': fields.many2one('account.account', 'Account', required=True, readonly=True, states={'draft':[('readonly',False)]}, help="The partner account used for this invoice."),
        'invoice_line': fields.one2many('account.invoice.line', 'invoice_id', 'Invoice Lines', readonly=True, states={'draft':[('readonly',False)]}),
        'tax_line': fields.one2many('account.invoice.tax', 'invoice_id', 'Tax Lines', readonly=True, states={'draft':[('readonly',False)]}),

        'move_id': fields.many2one('account.move', 'Invoice Movement', readonly=True, select=True, ondelete='set null',
                                   help="Link to the automatically generated account moves."),
        'amount_untaxed': fields.function(_amount_all, method=True, digits=(16, int(config['price_accuracy'])),string='Untaxed',
            store={
                'account.invoice': (lambda self, cr, uid, ids, c={}: ids, ['invoice_line'], 20),
                'account.invoice.tax': (_get_invoice_tax, None, 20),
                'account.invoice.line': (_get_invoice_line, ['price_unit','invoice_line_tax_id','quantity','discount'], 20),
            },
            multi='all'),
        'amount_tax': fields.function(
            _amount_all,
            method=True,
            digits=(16, int(config['price_accuracy'])),
            string='Tax',
            store={
                'account.invoice': (lambda self, cr, uid, ids, c={}: ids, ['invoice_line'], 20),
                'account.invoice.tax': (_get_invoice_tax, None, 20),
                'account.invoice.line': (_get_invoice_line, [
                    'price_unit', 'invoice_line_tax_id', 'quantity',
                    'discount'
                ], 20),
            },
            multi='all',
            select=1
        ),
        'amount_total': fields.function(
            _amount_all,
            method=True,
            digits=(16, int(config['price_accuracy'])),
            string='Total',
            select=1,
            store={
                'account.invoice': (lambda self, cr, uid, ids, c={}: ids, ['invoice_line'], 20),
                'account.invoice.tax': (_get_invoice_tax, None, 20),
                'account.invoice.line': (_get_invoice_line, ['price_unit','invoice_line_tax_id','quantity','discount'], 20),
            },
            multi='all'),
        'currency_id': fields.many2one('res.currency', 'Currency', required=True, readonly=True, states={'draft':[('readonly',False)]}),
        'journal_id': fields.many2one(
            'account.journal',
            'Journal',
            required=True,
            readonly=True,
            states={'draft': [('readonly', False)]},
            select=1
        ),
        'company_id': fields.many2one('res.company', 'Company', required=True),
        'check_total': fields.float('Total', digits=(16, int(config['price_accuracy'])), states={'open':[('readonly',True)],'close':[('readonly',True)]}),
        'reconciled': fields.function(_reconciled, method=True, string='Paid/Reconciled', type='boolean',
            store={
                'account.invoice': (lambda self, cr, uid, ids, c={}: ids, None, 50), # Check if we can remove ?
                'account.move.line': (_get_invoice_from_line, None, 50),
                'account.move.reconcile': (_get_invoice_from_reconcile, None, 50),
            }, help="The account moves of the invoice have been reconciled with account moves of the payment(s)."),
        'partner_bank': fields.many2one('res.partner.bank', 'Bank Account',
            help='The bank account to pay to or to be paid from'),
        'move_lines':fields.function(_get_lines , method=True,type='many2many' , relation='account.move.line',string='Move Lines'),
        'residual': fields.function(_amount_residual, method=True, digits=(16, int(config['price_accuracy'])),string='Residual',
            store={
                'account.invoice': (lambda self, cr, uid, ids, c={}: ids, ['invoice_line'], 50),
                'account.invoice.tax': (_get_invoice_tax, None, 50),
                'account.invoice.line': (_get_invoice_line, ['price_unit','invoice_line_tax_id','quantity','discount'], 50),
                'account.move.line': (_get_invoice_from_line, None, 50),
                'account.move.reconcile': (_get_invoice_from_reconcile, None, 50),
            },
            help="Remaining amount due."),
        'payment_ids': fields.function(_compute_lines, method=True, relation='account.move.line', type="many2many", string='Payments'),
        'move_name': fields.char('Account Move', size=64),
        'fiscal_position': fields.many2one('account.fiscal.position', 'Fiscal Position'),
        'rectificative_type': fields.selection(
            RECTIFICATIVE_TYPE_SELECTION, 'Rectificative type', readonly=True,
            states={'draft': [('readonly', False)]},
            select=True
        ),
        'rectifying_id': fields.many2one(
            'account.invoice', 'Ref. invoice', readonly=True
        ),
        'refund_by_id': fields.function(
            _ff_get_refund_by, type='many2one', obj='account.invoice',
            method=True, string='Refund by', readonly=True,
            store=_STORE_REFUND_BY
        ),
        'saldo': fields.float('Saldo', readonly=True, digits=(16, int(config['price_accuracy']))),
    }
    _defaults = {
        'type': _get_type,
        #'date_invoice': lambda *a: time.strftime('%Y-%m-%d'),
        'state': lambda *a: 'draft',
        'journal_id': _get_journal,
        'currency_id': _get_currency,
        'company_id': lambda self, cr, uid, context: \
                self.pool.get('res.users').browse(cr, uid, uid,
                    context=context).company_id.id,
        'reference_type': lambda *a: 'none',
        'check_total': lambda *a: 0.0,
        'rectificative_type': lambda *a: 'N',
    }
    
    def create(self, cr, uid, vals, context={}):
        try:
            res = super(account_invoice, self).create(cr, uid, vals, context)
            return res
        except Exception,e:
            if '"journal_id" viol' in e.args[0]:
                raise except_orm(_('Configuration Error!'),
                     _('There is no Accounting Journal of type Sale/Purchase defined!'))
            else:
                raise except_orm(_('UnknownError'), str(e))
            
    def unlink(self, cr, uid, ids, context=None):
        invoices = self.read(cr, uid, ids, ['state'])
        unlink_ids = []
        for t in invoices:
            if t['state'] in ('draft', 'cancel'):
                unlink_ids.append(t['id'])
            else:
                raise osv.except_osv(_('Invalid action !'), _('Cannot delete invoice(s) that are already opened or paid !'))
        osv.osv.unlink(self, cr, uid, unlink_ids, context=context)
        return True

#   def get_invoice_address(self, cr, uid, ids):
#       res = self.pool.get('res.partner').address_get(cr, uid, [part], ['invoice'])
#       return [{}]

    def onchange_partner_id(self, cr, uid, ids, type, partner_id,
            date_invoice=False, payment_term=False, partner_bank=False):
        invoice_addr_id = False
        contact_addr_id = False
        partner_payment_term = False
        acc_id = False
        bank_id = False
        fiscal_position = False

        opt = [('uid', str(uid))]
        if partner_id:

            opt.insert(0, ('id', partner_id))
            res = self.pool.get('res.partner').address_get(cr, uid, [partner_id], ['contact', 'invoice'])
            contact_addr_id = res['contact']
            invoice_addr_id = res['invoice']
            p = self.pool.get('res.partner').browse(cr, uid, partner_id)
            if type in ('out_invoice', 'out_refund'):
                acc_id = p.property_account_receivable.id
            else:
                acc_id = p.property_account_payable.id
            fiscal_position = p.property_account_position and p.property_account_position.id or False
            partner_payment_term = p.property_payment_term and p.property_payment_term.id or False
            if p.bank_ids:
                bank_id = p.bank_ids[0].id

        result = {'value': {
            'address_contact_id': contact_addr_id,
            'address_invoice_id': invoice_addr_id,
            'account_id': acc_id,
            'payment_term': partner_payment_term,
            'fiscal_position': fiscal_position
            }
        }

        if type in ('in_invoice', 'in_refund'):
            result['value']['partner_bank'] = bank_id

        if partner_bank != bank_id:
            to_update = self.onchange_partner_bank(cr, uid, ids, bank_id)
            result['value'].update(to_update['value'])
        return result

    def onchange_currency_id(self, cr, uid, ids, curr_id):
        return {}

    def onchange_payment_term_date_invoice(self, cr, uid, ids, payment_term_id, date_invoice):
        if not payment_term_id:
            return {}
        res={}
        pt_obj= self.pool.get('account.payment.term')
        if not date_invoice :
            date_invoice = time.strftime('%Y-%m-%d')

        pterm_list = pt_obj.compute(cr, uid, payment_term_id, value=1, date_ref=date_invoice)

        if pterm_list:
            pterm_list = [line[0] for line in pterm_list]
            pterm_list.sort()
            res= {'value':{'date_due': pterm_list[-1]}}
        else:
             raise osv.except_osv(_('Data Insufficient !'), _('The Payment Term of Supplier does not have Payment Term Lines(Computation) defined !'))

        return res

    def onchange_invoice_line(self, cr, uid, ids, lines):
        return {}

    def onchange_partner_bank(self, cursor, user, ids, partner_bank):
        return {'value': {}}

    # go from canceled state to draft state
    def action_cancel_draft(self, cr, uid, ids, *args):
        self.write(cr, uid, ids, {'state':'draft'})
        wf_service = netsvc.LocalService("workflow")
        for inv_id in ids:
            wf_service.trg_create(uid, 'account.invoice', inv_id, cr)
        return True

    # Workflow stuff
    #################

    # return the ids of the move lines which has the same account than the invoice
    # whose id is in ids
    def move_line_id_payment_get(self, cr, uid, ids, *args):
        res = []
        if not ids: return res
        cr.execute('SELECT l.id '\
                   'FROM account_move_line l '\
                   'LEFT JOIN account_invoice i ON (i.move_id=l.move_id) '\
                   'WHERE i.id IN %s '\
                   'AND l.account_id=i.account_id',
                   (tuple(ids),))
        res = map(itemgetter(0), cr.fetchall())
        return res

    def copy(self, cr, uid, id, default=None, context=None):
        if default is None:
            default = {}
        default = default.copy()
        default.update({'state':'draft', 'number':False, 'move_id':False, 'move_name':False,})
        if 'date_invoice' not in default:
            default['date_invoice'] = False
        if 'date_due' not in default:
            default['date_due'] = False
        return super(account_invoice, self).copy(cr, uid, id, default, context)

    def test_paid(self, cr, uid, ids, *args):
        res = self.move_line_id_payment_get(cr, uid, ids)
        if not res:
            return False
        ok = True
        for id in res:
            cr.execute('select reconcile_id from account_move_line where id=%s', (id,))
            ok = ok and  bool(cr.fetchone()[0])
        return ok

    def button_reset_taxes(self, cr, uid, ids, context=None):
        if not context:
            context = {}
        ctx = context.copy()
        ait_obj = self.pool.get('account.invoice.tax')
        for id in ids:
            cr.execute("DELETE FROM account_invoice_tax WHERE invoice_id=%s", (id,))
            partner = self.browse(cr, uid, id,context=ctx).partner_id
            if partner.lang:
                ctx.update({'lang': partner.lang})
            for taxe in ait_obj.compute(cr, uid, id, context=ctx).values():
                ait_obj.create(cr, uid, taxe)
         # Update the stored value (fields.function), so we write to trigger recompute
        self.pool.get('account.invoice').write(cr, uid, ids, {'invoice_line':[]}, context=ctx)    
#        self.pool.get('account.invoice').write(cr, uid, ids, {}, context=context)
        return True

    def button_compute(self, cr, uid, ids, context=None, set_total=False):
        self.button_reset_taxes(cr, uid, ids, context)
        for inv in self.browse(cr, uid, ids):
            if set_total:
                self.pool.get('account.invoice').write(cr, uid, [inv.id], {'check_total': inv.amount_total})
        return True

    def _convert_ref(self, cr, uid, ref):
        return (ref or '').replace('/','')

    def _get_analytic_lines(self, cr, uid, id):
        inv = self.browse(cr, uid, [id])[0]
        cur_obj = self.pool.get('res.currency')

        company_currency = inv.company_id.currency_id.id
        if inv.type in ('out_invoice', 'in_refund'):
            sign = 1
        else:
            sign = -1

        iml = self.pool.get('account.invoice.line').move_line_get(cr, uid, inv.id)
        for il in iml:
            if il['account_analytic_id']:
                if inv.type in ('in_invoice', 'in_refund'):
                    ref = inv.reference
                else:
                    ref = self._convert_ref(cr, uid, inv.number)
                il['analytic_lines'] = [(0,0, {
                    'name': il['name'],
                    'date': inv['date_invoice'],
                    'account_id': il['account_analytic_id'],
                    'unit_amount': il['quantity'],
                    'amount': cur_obj.compute(cr, uid, inv.currency_id.id, company_currency, il['price'], context={'date': inv.date_invoice}) * sign,
                    'product_id': il['product_id'],
                    'product_uom_id': il['uos_id'],
                    'general_account_id': il['account_id'],
                    'journal_id': self._get_journal_analytic(cr, uid, inv.type),
                    'ref': ref,
                })]
        return iml

    def action_date_assign(self, cr, uid, ids, *args):
        for inv in self.browse(cr, uid, ids):
            res = self.onchange_payment_term_date_invoice(cr, uid, inv.id, inv.payment_term.id, inv.date_invoice)
            if res and res['value']:
                self.write(cr, uid, [inv.id], res['value'])
        return True
    
    def finalize_invoice_move_lines(self, cr, uid, invoice_browse, move_lines):
        """finalize_invoice_move_lines(cr, uid, invoice, move_lines) -> move_lines
        Hook method to be overridden in additional modules to verify and possibly alter the 
        move lines to be created by an invoice, for special cases.
        :param invoice_browse: browsable record of the invoice that is generating the move lines
        :param move_lines: list of dictionaries with the account.move.lines (as for create())
        :return: the (possibly updated) final move_lines to create for this invoice 
        """
        return move_lines

    def check_taxes_correctly_computed(self, cursor, uid, inv_id, context=None):
        if context is None:
            context = {}

        ait_obj = self.pool.get('account.invoice.tax')

        inv = self.browse(cursor, uid, inv_id)

        context.update({'lang': inv.partner_id.lang})
        compute_taxes = ait_obj.compute(cursor, uid, inv.id, context=context)
        if not inv.tax_line:
            for tax in compute_taxes.values():
                ait_obj.create(cursor, uid, tax)
        else:
            tax_key = []
            for tax in inv.tax_line:
                if tax.manual:
                    continue
                key = (
                    tax.tax_code_id.id, tax.base_code_id.id, tax.account_id.id
                )
                tax_key.append(key)
                if key not in compute_taxes:
                    raise osv.except_osv(_('Warning !'), _(
                        'Global taxes defined, but are not in invoice lines !'))
                base = compute_taxes[key]['base']
                if abs(base - tax.base) > inv.company_id.currency_id.rounding:
                    raise osv.except_osv(
                        _('Warning !'),
                        _(
                            'Tax base different !\n'
                            'Click on compute to update tax base'
                        )
                    )
            for key in compute_taxes:
                if key not in tax_key:
                    raise osv.except_osv(
                        _('Warning !'), _('Taxes missing !')
                    )

    def calculate_total_move_line(self, cursor, uid, inv_id, iml, context):
        if context is None:
            context = {}

        cur_obj = self.pool.get('res.currency')

        inv = self.browse(cursor, uid, inv_id)

        if inv.type in ('in_invoice', 'in_refund'):
            ref = inv.reference
        else:
            ref = self._convert_ref(cursor, uid, inv.number)

        company_currency = inv.company_id.currency_id.id

        diff_currency_p = inv.currency_id.id <> company_currency
        # create one move line for the total and possibly adjust the other
        # lines amount
        total = 0
        total_currency = 0
        lines = []
        for i in iml:
            if inv.currency_id.id != company_currency:
                i['currency_id'] = inv.currency_id.id
                i['amount_currency'] = i['price']
                i['price'] = cur_obj.compute(
                    cursor, uid, inv.currency_id.id, company_currency,
                    i['price'], context={
                        'date': inv.date_invoice or time.strftime('%Y-%m-%d')
                    }
                )
            else:
                i['amount_currency'] = False
                i['currency_id'] = False
            i['ref'] = ref
            if inv.type in ('out_invoice', 'in_refund'):
                total += i['price']
                total_currency += i['amount_currency'] or i['price']
                i['price'] = - i['price']
            else:
                total -= i['price']
                total_currency -= i['amount_currency'] or i['price']
        acc_id = inv.account_id.id

        name = inv['name'] or '/'
        totlines = False
        if inv.payment_term:
            totlines = self.pool.get('account.payment.term').compute(
                cursor, uid, inv.payment_term.id, total,
                inv.date_invoice or False
            )
        if totlines:
            res_amount_currency = total_currency
            i = 0
            for t in totlines:
                if inv.currency_id.id != company_currency:
                    amount_currency = cur_obj.compute(
                        cursor, uid, company_currency, inv.currency_id.id, t[1]
                    )
                else:
                    amount_currency = False

                # last line add the diff
                res_amount_currency -= amount_currency or 0
                i += 1
                if i == len(totlines):
                    amount_currency += res_amount_currency

                lines.append({
                    'type': 'dest',
                    'name': name,
                    'price': t[1],
                    'account_id': acc_id,
                    'date_maturity': t[0],
                    'amount_currency':
                        diff_currency_p and amount_currency or False,
                    'currency_id':
                        diff_currency_p and inv.currency_id.id or False,
                    'ref': ref,
                })
        else:
            lines.append({
                'type': 'dest',
                'name': name,
                'price': total,
                'account_id': acc_id,
                'date_maturity': inv.date_due or False,
                'amount_currency': diff_currency_p and total_currency or False,
                'currency_id': diff_currency_p and inv.currency_id.id or False,
                'ref': ref
            })
        return lines

    def calculate_move_lines_for_invoice(self, cursor, uid, inv_id,
                                         context=None):
        if context is None:
            context = {}

        ait_obj = self.pool.get('account.invoice.tax')

        inv = self.browse(cursor, uid, inv_id)

        # one move line per invoice line
        iml = self._get_analytic_lines(cursor, uid, inv.id)
        # check if taxes are all computed

        self.check_taxes_correctly_computed(cursor, uid, inv_id, context)

        difference = abs(inv.check_total - inv.amount_total)
        precision = (inv.currency_id.rounding / 2.0)
        if inv.type in ('in_invoice', 'in_refund') and difference >= precision:
            raise osv.except_osv(
                _('Bad total !'),
                _(
                    'Please verify the price of the invoice !\n'
                    'The real total does not match the computed total.'
                )
            )

        # one move line per tax line
        iml += ait_obj.move_line_get(cursor, uid, inv.id)

        iml += self.calculate_total_move_line(cursor, uid, inv_id, iml, context)

        return iml

    def format_move_lines(self, cursor, uid, inv_id, iml, context=None):
        if context is None:
            context = {}

        journal_obj = self.pool.get('account.journal')

        inv = self.browse(cursor, uid, inv_id)

        date = inv.date_invoice or time.strftime('%Y-%m-%d')
        part = inv.partner_id.id

        ctx = context.copy()
        ctx.update({'invoice_id': inv.id})
        lines = map(
            lambda x: (
                0, 0, self.line_get_convert(
                    cursor, uid, x, part, date,
                    context=ctx
                )
            ), iml
        )

        if inv.journal_id.group_invoice_lines:
            line2 = {}
            for x, y, l in lines:
                tmp = str(l['account_id'])
                tmp += '-' + str(l.get('tax_code_id', "False"))
                tmp += '-' + str(l.get('product_id', "False"))
                tmp += '-' + str(l.get('analytic_account_id', "False"))
                tmp += '-' + str(l.get('date_maturity', "False"))

                if tmp in line2:
                    am = line2[tmp]['debit'] - line2[tmp]['credit']
                    am += (l['debit'] - l['credit'])
                    line2[tmp]['debit'] = (am > 0) and am or 0.0
                    line2[tmp]['credit'] = (am < 0) and -am or 0.0
                    line2[tmp]['tax_amount'] += l['tax_amount']
                    line2[tmp]['analytic_lines'] += l['analytic_lines']
                else:
                    line2[tmp] = l
            lines = []
            for key, val in line2.items():
                lines.append((0, 0, val))

        journal_id = inv.journal_id.id
        journal_vals = journal_obj.read(
            cursor, uid, journal_id, ['centralisation']
        )
        if journal_vals['centralisation']:
            raise osv.except_osv(
                _('UserError'), _(
                    'Cannot create invoice move on centralised journal'
                )
            )

        lines = self.finalize_invoice_move_lines(cursor, uid, inv, lines)

        return lines

    def create_move_from_lines(self, cursor, uid, inv_id, period_id, line,
                               context=None):
        if context is None:
            context = {}

        move_obj = self.pool.get('account.move')

        inv = self.browse(cursor, uid, inv_id)

        date = inv.date_invoice or time.strftime('%Y-%m-%d')
        part = inv.partner_id.id

        journal_id = inv.journal_id.id

        move = {
            'ref': inv.number, 'line_id': line, 'journal_id': journal_id,
            'date': date
        }
        if period_id:
            move['period_id'] = period_id
            for i in line:
                i[2]['period_id'] = period_id

        return move_obj.create(
            cursor, uid, move, context=context
        )

    def always_compensate_RA(self, cursor, uid, invoice_type):
        return False

    def action_move_create(self, cr, uid, ids, *args):
        context = {}

        move_obj = self.pool.get('account.move')
        move_line_obj = self.pool.get('account.move.line')

        for inv in self.browse(cr, uid, ids):
            if inv.rectifying_id and inv.rectifying_id.state == 'draft':
                raise osv.except_osv(
                    _('Estat de la factura recitificada incorrecte!'),
                    _('La factura rectificada (id: {0}) està en borrador.\nObre'
                      ' la factura original per poder obrir la rectificadora.'
                      ).format(inv.rectifying_id.id)
                )

            if inv.move_id:
                continue

            if not inv.date_invoice:
                self.write(
                    cr, uid, [inv.id], {
                        'date_invoice': time.strftime('%Y-%m-%d')
                    }
                )
            # create the analytical lines
            line_ids = self.read(
                cr, uid, [inv.id], ['invoice_line']
            )[0]['invoice_line']

            iml = self.calculate_move_lines_for_invoice(
                cr, uid, inv.id, context
            )

            date = inv.date_invoice or time.strftime('%Y-%m-%d')
            period_id = inv.period_id and inv.period_id.id or False
            if not period_id:
                period_ids = self.pool.get('account.period').search(
                    cr, uid, [
                        ('date_start', '<=', date),
                        ('date_stop', '>=', date)
                    ]
                )
                if len(period_ids):
                    period_id = period_ids[0]

            lines = self.format_move_lines(cr, uid, inv.id, iml, context)

            move_id = self.create_move_from_lines(
                cr, uid, inv.id, period_id, lines, context
            )

            new_move_name = move_obj.read(
                cr, uid, move_id, ['name']
            )['name']
            # make the invoice point to that move
            self.write(
                cr, uid, [inv.id], {
                    'move_id': move_id,
                    'period_id': period_id,
                    'move_name': new_move_name
                }
            )
            # Pass invoice in context in method post: used if you want to get the
            # same account move reference when creating the same invoice after a
            # cancelled one:
            move_obj.post(cr, uid, [move_id], context={'invoice': inv})

            if inv.rectificative_type == 'RA' and inv.rectifying_id:
                # Search for a BRA invoice to open and use it to pay the
                # difference
                bra_ids = self.search(cr, uid, [
                    ('rectificative_type', '=', 'BRA'),
                    ('rectifying_id', '=', inv.rectifying_id.id)
                ])
                # Raise and Exception if is not equal to 1 ?
                # We can work with BRA or not..
                if len(bra_ids) == 1:
                    bra_inv = self.browse(cr, uid, bra_ids[0])
                    wf_service = netsvc.LocalService("workflow")
                    wf_service.trg_validate(
                        uid, 'account.invoice', bra_inv.id, 'invoice_open', cr
                    )
                    compensate_RA = (
                        inv.rectifying_id.state == 'paid'
                        or self.always_compensate_RA(cr, uid, inv.type)
                    )
                    if compensate_RA:
                        # Create one move with two move lines
                        # DXX C00 -> Reconcile with BRA
                        # C00 DXX -> Reconcile with RA
                        # Move line pay with BRA
                        # The other line pay the RA

                        move_id = move_obj.create(cr, uid, {
                            'date': date,
                            'period_id': period_id,
                            'journal_id': inv.journal_id.id
                        })

                        common_values = {
                            'name': 'Comp. {}'.format(bra_inv.number),
                            'account_id': bra_inv.account_id.id,
                            'ref': self._convert_ref(cr, uid, inv.origin),
                            'date': date,
                            'journal_id': inv.journal_id.id,
                            'period_id': period_id,
                            'move_id': move_id,
                            'partner_id': bra_inv.partner_id.id
                        }
                        # Create a account move line to reconcile with the BRA
                        # invoice
                        l1_values = common_values.copy()
                        l1_values.update({
                            'debit': bra_inv.amount_total,
                            'credit': 0,
                        })
                        l1 = move_line_obj.create(cr, uid, l1_values)

                        # Create a account move line to reconcile with the RA
                        # invoice
                        l2_values = common_values.copy()
                        l2_values.update({
                            'debit': 0,
                            'credit': bra_inv.amount_total,
                        })
                        l2 = move_line_obj.create(cr, uid, l2_values)

                        # If is a provider invoice we have to change direction
                        # debit/credit
                        if inv.type == 'in_invoice':
                            l1, l2 = l2, l1

                        # Pay the BRA invoice reconcilying it with l1
                        reconcile_bra_lines = self.move_line_id_payment_get(
                            cr, uid, [bra_inv.id]
                        )
                        reconcile_bra_lines.append(l1)
                        move_line_obj.reconcile_partial(
                            cr, uid, reconcile_bra_lines, type='auto'
                        )

                        # Partially pay the RA invoice reconcilying with l2
                        reconcile_ra_lines = self.move_line_id_payment_get(
                            cr, uid, [inv.id]
                        )
                        reconcile_ra_lines.append(l2)
                        move_line_obj.reconcile_partial(
                            cr, uid, reconcile_ra_lines, type='auto'
                        )

                    elif inv.rectifying_id.state == 'open':
                        if inv.rectifying_id.residual != inv.rectifying_id.amount_total:
                            move_id = move_obj.create(cr, uid, {
                                'date': date,
                                'period_id': period_id,
                                'journal_id': inv.journal_id.id
                            })

                            common_values = {
                                'name': 'Comp. {}'.format(bra_inv.number),
                                'account_id': bra_inv.account_id.id,
                                'ref': self._convert_ref(cr, uid, inv.origin),
                                'date': date,
                                'journal_id': inv.journal_id.id,
                                'period_id': period_id,
                                'move_id': move_id,
                                'partner_id': bra_inv.partner_id.id
                            }
                            # Create a account move line to reconcile with
                            # the BRA invoice
                            l1_values = common_values.copy()

                            if inv.type == 'in_invoice':
                                if inv.rectifying_id.residual > 0:
                                    l1_values.update({
                                        'debit': abs(inv.rectifying_id.residual),
                                        'credit': 0
                                    })
                                else:
                                    l1_values.update({
                                        'debit': 0,
                                        'credit': abs(inv.rectifying_id.residual)
                                    })
                                l1 = move_line_obj.create(cr, uid, l1_values)

                                # Create a account move line to reconcile with
                                # the RA invoice
                                l2_values = common_values.copy()
                                l2_values.update({
                                    'debit': inv.rectifying_id.amount_total - inv.rectifying_id.residual,
                                    'credit': 0
                                })
                                l2 = move_line_obj.create(cr, uid, l2_values)

                                l3_values = common_values.copy()
                                l3_values.update({
                                    'debit': 0,
                                    'credit': bra_inv.amount_total
                                })
                                l3 = move_line_obj.create(cr, uid, l3_values)
                            else:
                                if inv.rectifying_id.residual > 0:

                                    l1_values.update({
                                        'debit': 0,
                                        'credit': abs(inv.rectifying_id.residual)
                                    })
                                else:
                                    l1_values.update({
                                        'debit': abs(inv.rectifying_id.residual),
                                        'credit': 0
                                    })

                                l1 = move_line_obj.create(cr, uid, l1_values)

                                # Create a account move line to reconcile with the RA
                                # invoice
                                l2_values = common_values.copy()
                                l2_values.update({
                                    'debit': 0,
                                    'credit': inv.rectifying_id.amount_total - inv.rectifying_id.residual
                                })
                                l2 = move_line_obj.create(cr, uid, l2_values)

                                l3_values = common_values.copy()
                                l3_values.update({
                                    'debit': bra_inv.amount_total,
                                    'credit': 0
                                })
                                l3 = move_line_obj.create(cr, uid, l3_values)

                            # Pay the remaining amount of original invoice
                            reconcile_orig_lines = self.move_line_id_payment_get(
                                cr, uid, [inv.rectifying_id.id]
                            )
                            reconcile_orig_lines.append(l1)
                            move_line_obj.reconcile_partial(
                                cr, uid, reconcile_orig_lines, type='auto'
                            )

                            # Pay the BRA invoice reconcilying it with l3
                            reconcile_bra_lines = self.move_line_id_payment_get(
                                cr, uid, [bra_inv.id]
                            )
                            reconcile_bra_lines.append(l3)
                            move_line_obj.reconcile_partial(
                                cr, uid, reconcile_bra_lines, type='auto'
                            )

                            # Partially pay the RA invoice reconcilying with l2
                            reconcile_ra_lines = self.move_line_id_payment_get(
                                cr, uid, [inv.id]
                            )
                            reconcile_ra_lines.append(l2)
                            move_line_obj.reconcile_partial(
                                cr, uid, reconcile_ra_lines, type='auto'
                            )
                        else:
                            reconcile_lines = self.move_line_id_payment_get(
                                cr, uid, [inv.rectifying_id.id, bra_inv.id]
                            )
                            move_line_obj.reconcile(
                                cr, uid, reconcile_lines, type='auto'
                            )

                elif not bra_ids:
                    refund_move_lines = self.calculate_move_lines_for_invoice(
                        cr, uid, inv.rectifying_id.id, context
                    )

                    for move_line in refund_move_lines:
                        move_line['price'] = -1 * move_line['price']
                        if 'amount_currency' in move_line:
                            amount_currency = move_line['amount_currency']
                            move_line['amount_currency'] = -1 * amount_currency

                    refund_lines = self.format_move_lines(
                        cr, uid, inv.rectifying_id.id, refund_move_lines,
                        context
                    )

                    refunding_move_id = self.create_move_from_lines(
                        cr, uid, inv.id, period_id, refund_lines, context
                    )

                    total_lines = []
                    move_vals = move_obj.read(
                        cr, uid, move_id, ['line_id']
                    )
                    lines_values = move_line_obj.read(
                        cr, uid, move_vals['line_id'], ['account_id']
                    )
                    for line_vals in lines_values:
                        if line_vals['account_id'][0] == inv.account_id.id:
                            total_lines.append(line_vals['id'])

                    refund_lines = []
                    ref_move_vals = move_obj.read(
                        cr, uid, refunding_move_id, ['line_id']
                    )
                    ref_lines_values = move_line_obj.read(
                        cr, uid, ref_move_vals['line_id'], ['account_id']
                    )
                    for line_vals in ref_lines_values:
                        line_account_id = line_vals['account_id'][0]
                        if line_account_id == inv.rectifying_id.account_id.id:
                            refund_lines.append(line_vals['id'])

                    move_line_obj.reconcile_partial(
                        cr, uid, total_lines + refund_lines, type='auto'
                    )
                else:
                    raise Exception('More than one BRA, not supported')

        self._log_event(cr, uid, ids)
        return True

    def line_get_convert(self, cr, uid, x, part, date, context=None):
        return {
            'date_maturity': x.get('date_maturity', False),
            'partner_id':part,
            'name':x['name'][:64],
            'date': date,
            'debit':x['price']>0 and x['price'],
            'credit':x['price']<0 and -x['price'],
            'account_id':x['account_id'],
            'analytic_lines':x.get('analytic_lines', []),
            'amount_currency':x['price']>0 and abs(x.get('amount_currency', False)) or -abs(x.get('amount_currency', False)),
            'currency_id':x.get('currency_id', False),
            'tax_code_id': x.get('tax_code_id', False),
            'tax_amount': x.get('tax_amount', False),
            'ref':x.get('ref',False),
            'quantity':x.get('quantity',1.00),
            'product_id':x.get('product_id', False),
            'product_uom_id':x.get('uos_id',False),
            'analytic_account_id':x.get('account_analytic_id',False),
        }

    def action_number(self, cr, uid, ids, *args):
        cr.execute('SELECT id, type, number, move_id, reference ' \
                   'FROM account_invoice ' \
                   'WHERE id IN %s',
                   (tuple(ids),))
        obj_inv = self.browse(cr, uid, ids)[0]
        for (id, invtype, number, move_id, reference) in cr.fetchall():
            if not number:
                tmp_context = {
                    'fiscalyear_id' : obj_inv.period_id.fiscalyear_id.id,
                }
                if obj_inv.journal_id.invoice_sequence_id:
                    sid = obj_inv.journal_id.invoice_sequence_id.id
                    number = self.pool.get('ir.sequence').get_id(cr, uid, sid, 'id=%s', context=tmp_context)
                else:
                    number = self.pool.get('ir.sequence').get_id(cr, uid,
                                                                 'account.invoice.' + invtype,
                                                                 'code=%s',
                                                                 context=tmp_context)
                if not number:
                    raise osv.except_osv(_('Warning !'), _('There is no active invoice sequence defined for the journal !'))

                if invtype in ('in_invoice', 'in_refund'):
                    ref = reference
                else:
                    ref = self._convert_ref(cr, uid, number)
                cr.execute('UPDATE account_invoice SET number=%s ' \
                        'WHERE id=%s', (number, id))
                cr.execute('UPDATE account_move SET ref=%s ' \
                        'WHERE id=%s AND (ref is null OR ref = \'\')',
                        (ref, move_id))
                cr.execute('UPDATE account_move_line SET ref=%s ' \
                        'WHERE move_id=%s AND (ref is null OR ref = \'\')',
                        (ref, move_id))
                cr.execute('UPDATE account_analytic_line SET ref=%s ' \
                        'FROM account_move_line ' \
                        'WHERE account_move_line.move_id = %s ' \
                            'AND account_analytic_line.move_id = account_move_line.id',
                            (ref, move_id))
        return True

    def action_cancel(self, cr, uid, ids, *args):
        account_move_obj = self.pool.get('account.move')
        invoices = self.read(cr, uid, ids, ['move_id', 'payment_ids'])
        for i in invoices:
            if i['move_id']:
                account_move_obj.button_cancel(cr, uid, [i['move_id'][0]])
                # delete the move this invoice was pointing to
                # Note that the corresponding move_lines and move_reconciles
                # will be automatically deleted too
                account_move_obj.unlink(cr, uid, [i['move_id'][0]])
            if i['payment_ids']:
                account_move_line_obj = self.pool.get('account.move.line')
                pay_ids = account_move_line_obj.browse(cr, uid , i['payment_ids'])
                for move_line in pay_ids:
                    if move_line.reconcile_partial_id and move_line.reconcile_partial_id.line_partial_ids:
                        raise osv.except_osv(_('Error !'), _('You cannot cancel the Invoice which is Partially Paid! You need to unreconcile concerned payment entries!'))

        self.write(cr, uid, ids, {'state':'cancel', 'move_id':False})
        self._log_event(cr, uid, ids,-1.0, 'Cancel Invoice')
        return True

    ###################

    def list_distinct_taxes(self, cr, uid, ids):
        invoices = self.browse(cr, uid, ids)
        taxes = {}
        for inv in invoices:
            for tax in inv.tax_line:
                if not tax['name'] in taxes:
                    taxes[tax['name']] = {'name': tax['name']}
        return taxes.values()

    def _log_event(self, cr, uid, ids, factor=1.0, name='Open Invoice'):
        invs = self.read(cr, uid, ids, ['type','partner_id','amount_untaxed'])
        for inv in invs:
            part=inv['partner_id'] and inv['partner_id'][0]
            pc = pr = 0.0
            cr.execute('select sum(quantity*price_unit) from account_invoice_line where invoice_id=%s', (inv['id'],))
            total = inv['amount_untaxed']
            if inv['type'] in ('in_invoice','in_refund'):
                partnertype='supplier'
                eventtype = 'purchase'
                pc = total*factor
            else:
                partnertype = 'customer'
                eventtype = 'sale'
                pr = total*factor
            if self.pool.get('res.partner.event.type').check(cr, uid, 'invoice_open'):
                self.pool.get('res.partner.event').create(cr, uid, {'name':'Invoice: '+name, 'som':False, 'description':name+' '+str(inv['id']), 'document':name, 'partner_id':part, 'date':time.strftime('%Y-%m-%d %H:%M:%S'), 'canal_id':False, 'user_id':uid, 'partner_type':partnertype, 'probability':1.0, 'planned_revenue':pr, 'planned_cost':pc, 'type':eventtype})
        return len(invs)

    def name_get(self, cursor, uid, ids, context=None):
        res = []
        if len(ids):
            types = {
                'out_invoice': 'CI:',
                'in_invoice': 'SI:',
                'out_refund': 'CR:',
                'in_refund': 'SR:',
            }
            for invoice in self.read(cursor, uid, ids,
                                     ['type', 'number', 'name', 'origin'],
                                     context=context):
                number = ''
                inv_type = invoice['type']
                if inv_type.startswith('in'):
                    number = invoice['origin'] or invoice['name']
                elif inv_type.startswith('out'):
                    number = invoice['number']

                invoice_desc = (
                    invoice['id'], '{prefix} {number}'.format(
                        prefix=types[inv_type], number=number
                    )
                )
                res.append(invoice_desc)

        return res

    def name_search(self, cr, user, name, args=None, operator='ilike', context=None, limit=80):
        if not args:
            args=[]
        if context is None:
            context={}
        ids = []
        if name:
            ids = self.search(cr, user, [('number','=',name)]+ args, limit=limit, context=context)
        if not ids:
            ids = self.search(cr, user, [('name',operator,name)]+ args, limit=limit, context=context)
        return self.name_get(cr, user, ids, context)

    def _refund_cleanup_lines(self, cr, uid, lines):
        for line in lines:
            del line['id']
            del line['invoice_id']
            if 'account_id' in line:
                line['account_id'] = line.get('account_id', False) and line['account_id'][0]
            if 'product_id' in line:
                line['product_id'] = line.get('product_id', False) and line['product_id'][0]
            if 'uos_id' in line:
                line['uos_id'] = line.get('uos_id', False) and line['uos_id'][0]
            if 'invoice_line_tax_id' in line:
                line['invoice_line_tax_id'] = [(6,0, line.get('invoice_line_tax_id', [])) ]
            if 'account_analytic_id' in line:
                line['account_analytic_id'] = line.get('account_analytic_id', False) and line['account_analytic_id'][0]
            if 'tax_code_id' in line :
                if isinstance(line['tax_code_id'],tuple)  and len(line['tax_code_id']) >0 :
                    line['tax_code_id'] = line['tax_code_id'][0]
            if 'base_code_id' in line :
                if isinstance(line['base_code_id'],tuple)  and len(line['base_code_id']) >0 :
                    line['base_code_id'] = line['base_code_id'][0]
        return map(lambda x: (0,0,x), lines)

    def check_if_some_invoice_is_bra(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        # Comprobamos que no sean BRA's
        tipos_rectificadora = self.read(
            cursor, uid, ids, ['rectificative_type', 'number'], context=context
        )

        some_bra = []

        # Acabamos el recorrido para informar de todas las que son BRA's y
        # evitar llamar a la funcion cada vez para comprobar si hay alguna BRA's
        # más
        for fact in tipos_rectificadora:
            if fact['rectificative_type'] == 'BRA':
                some_bra.append(fact['number'])

        if some_bra:
            bras = ','.join(some_bra)
            raise osv.except_osv(_('Error!'),
                                 _(u"No es pot anul·lar i rectificar "
                                   u"factures BRA:\n{0}"
                                   ).format(bras)
                                 )

    def refund(self, cr, uid, ids, date=None, period_id=None, description=None,
               rectificative_type='A'):
        self.check_if_some_invoice_is_bra(cr, uid, ids)
        invoices = self.read(cr, uid, ids, [
            'name', 'type', 'number', 'reference', 'comment', 'date_due',
            'partner_id', 'address_contact_id', 'address_invoice_id',
            'partner_contact', 'partner_insite', 'partner_ref', 'payment_term',
            'account_id', 'currency_id', 'invoice_line', 'tax_line',
            'journal_id', 'fiscal_position'
        ])

        new_ids = []
        for invoice in invoices:
            rectified_id = invoice['id']
            del invoice['id']

            type_dict = {
                'out_invoice': 'out_refund', # Customer Invoice
                'in_invoice': 'in_refund',   # Supplier Invoice
                'out_refund': 'out_invoice', # Customer Refund
                'in_refund': 'in_invoice',   # Supplier Refund
            }


            invoice_lines = self.pool.get('account.invoice.line').read(cr, uid, invoice['invoice_line'])
            invoice_lines = self._refund_cleanup_lines(cr, uid, invoice_lines)

            tax_lines = self.pool.get('account.invoice.tax').read(cr, uid, invoice['tax_line'])
            tax_lines = filter(lambda l: l['manual'], tax_lines)
            tax_lines = self._refund_cleanup_lines(cr, uid, tax_lines)
            if not date :
                date = time.strftime('%Y-%m-%d')
            invoice.update({
                'type': type_dict[invoice['type']],
                'rectificative_type': rectificative_type,
                'rectifying_id': rectified_id,
                'date_invoice': date,
                'state': 'draft',
                'number': False,
                'invoice_line': invoice_lines,
                'tax_line': tax_lines
            })
            if period_id :
                invoice.update({
                    'period_id': period_id,
                })
            if description :
                invoice.update({
                    'name': description,
                })
            # take the id part of the tuple returned for many2one fields
            for field in ('address_contact_id', 'address_invoice_id', 'partner_id',
                    'account_id', 'currency_id', 'payment_term', 'journal_id', 'fiscal_position'):
                invoice[field] = invoice[field] and invoice[field][0]
            # create the new invoice
            new_ids.append(self.create(cr, uid, invoice))
        return new_ids

    def rectify(self, cursor, uid, ids, date=None, period=None,
                description=None, context=None):
        if context is None:
            context = {}

        invoice_line_obj = self.pool.get('account.invoice.line')
        invoice_tax_obj = self.pool.get('account.invoice.tax')

        created_inv = self.refund(
            cursor, uid, ids, date, period, description, rectificative_type='B'
        )

        created_inv += self.rectify_only(
            cursor, uid, ids, date, period, description, rectificative_type='R',
            context=context
        )

        return created_inv

    def rectify_only(self, cursor, uid, ids, date=None, period=None,
                     description=None, rectificative_type='R', context=None):
        if context is None:
            context = {}

        created_inv = []

        for inv_id in ids:
            invoice, aux = self.copy_data(
                cursor, uid, inv_id, {
                    'rectificative_type': rectificative_type,
                    'rectifying_id': inv_id,
                    'date_invoice': date,
                    'state': 'draft',
                    'number': False,
                    'period_id': period,
                    'name': description,
                    'move_id': False,
                }, context
            )

            for invoice_line in invoice['invoice_line']:
                del invoice_line[2]['invoice_id']

            for tax_line in invoice['tax_line']:
                del tax_line[2]['invoice_id']

            # create the new invoice
            new_inv_id = self.create(cursor, uid, invoice, {})
            # we compute due date
            if invoice.get('payment_term', False):
                data = self.onchange_payment_term_date_invoice(
                    cursor, uid, [new_inv_id], invoice['payment_term'], date
                )
                if 'value' in data and data['value']:
                    self.write(cursor, uid, [new_inv_id], data['value'])
            created_inv.append(new_inv_id)

        return created_inv

    def get_src_account_id_from_invoice(self, cursor, uid, invoice_id=None, invoice=None, context=None):
        if context is None:
            context = {}
        if not invoice_id and not invoice:
            raise osv.except_osv(
                'Error',
                "S'ha de especificar una factura o in ID de factura."
            )
        if isinstance(invoice_id, (list, tuple)):
            invoice_id = invoice_id[0]
        if invoice_id and not invoice:
            invoice = self.browse(cursor, uid, invoice_id, context=context)

        from collections import Counter
        move_obj = self.pool.get('account.move.line')
        payments = Counter()
        moves_id = invoice.move_line_id_payment_get()
        zero_lines = set()
        for payment in invoice.payment_ids + move_obj.browse(cursor, uid, moves_id):
            payments[payment.account_id.id] += payment.credit - payment.debit
            if payment.credit == 0.0 and payment.debit == 0.0:
                zero_lines.add(payment.account_id.id)

        zero_lines = list(zero_lines)
        accounts_ids = []
        for account_id, amount in payments.items():
            if float_round(amount, 2):
                accounts_ids.append(account_id)
        if len(accounts_ids) != 1:
            if len(zero_lines) != 1:
                raise osv.except_osv(
                    'Error',
                    'Impossible to get the account to reconcile!'
                )
            else:
                return zero_lines[0]
        else:
            return accounts_ids[0]

    def pay_and_reconcile(self, cr, uid, ids, pay_amount, pay_account_id,
                          period_id, pay_journal_id, writeoff_acc_id,
                          writeoff_period_id, writeoff_journal_id,
                          context=None, name=''):
        if context is None:
            context = {}
        #TODO check if we can use different period for payment and the writeoff line
        assert len(ids)==1, "Can only pay one invoice at a time"
        invoice = self.browse(cr, uid, ids[0])
        # Si la factura ja està pagada no hem de fer res
        if invoice.residual == 0.0 and invoice.state == 'paid':
            return True
        # Si la quantitat a pagar es 0 i no ens donden dades per contabilitzar el writeoff no hem de fer res
        elif pay_amount == 0.0 and not writeoff_acc_id and invoice.residual != 0:
            return True
        move_obj = self.pool.get('account.move.line')
        wf_service = netsvc.LocalService('workflow')
        moves_id = invoice.move_line_id_payment_get()
        src_account_id = self.get_src_account_id_from_invoice(cr, uid, invoice=invoice, context=context)
        # Take the seq as name for move
        types = {'out_invoice': -1, 'in_invoice': 1, 'out_refund': 1, 'in_refund': -1}
        direction = types[invoice.type]
        # let pay negative invoices
        if invoice.residual < 0 < pay_amount:
            direction *= -1
        # take the choosen date
        if 'date_p' in context and context['date_p']:
            date=context['date_p']
        else:
            date=time.strftime('%Y-%m-%d')
            
        # Take the amount in currency and the currency of the payment
        if 'amount_currency' in context and context['amount_currency'] and 'currency_id' in context and context['currency_id']:
            amount_currency = context['amount_currency']
            currency_id = context['currency_id']
        else:
            amount_currency = False
            currency_id = False
        
        if invoice.type in ('in_invoice', 'in_refund'):
            ref = invoice.reference
        else:
            ref = self._convert_ref(cr, uid, invoice.number)
        pay_partner_id = context.get('pay_partner_id', invoice.partner_id.id)
        src_partner_id = context.get('src_partner_id', invoice.partner_id.id)
        # Pay attention to the sign for both debit/credit AND amount_currency
        l1 = {
            'debit': direction * pay_amount>0 and direction * pay_amount,
            'credit': direction * pay_amount<0 and - direction * pay_amount,
            'account_id': src_account_id,
            'partner_id': src_partner_id,
            'ref':ref,
            'date': date,
            'currency_id':currency_id,
            'amount_currency':amount_currency and direction * amount_currency or 0.0,
        }
        l2 = {
            'debit': direction * pay_amount<0 and - direction * pay_amount,
            'credit': direction * pay_amount>0 and direction * pay_amount,
            'account_id': pay_account_id,
            'partner_id': pay_partner_id,
            'ref':ref,
            'date': date,
            'currency_id':currency_id,
            'amount_currency':amount_currency and - direction * amount_currency or 0.0,
        }

        if not name:
            name = invoice.invoice_line and invoice.invoice_line[0].name or invoice.number
        l1['name'] = name
        l2['name'] = name

        lines = [(0, 0, l1), (0, 0, l2)]
        move = {'ref': ref, 'line_id': lines, 'journal_id': pay_journal_id, 'period_id': period_id, 'date': date}
        move_id = self.pool.get('account.move').create(cr, uid, move, context=context)

        cr.execute(
            'SELECT id FROM account_move_line WHERE move_id = %s', (move_id,))
        lines_ids = map(lambda x: x[0], cr.fetchall())

        ctx = context.copy()
        ctx['force_different_account'] = True

        move_obj.reconcile_lines(
            cr, uid, lines_ids + moves_id, 'manual', writeoff_acc_id,
            writeoff_period_id, writeoff_journal_id, src_account_id, context=ctx
        )

        wf_service.trg_validate(uid, 'account.invoice', invoice.id,
                                'open_test', cr)
        move_obj.write(cr, uid, invoice.move_line_id_payment_get(), {})


        # Update the stored value (fields.function), so we write to trigger recompute
        invoice.write({}, context=context)
        return move_id

    def set_state(self, cursor, uid, ids, state, context=None):
        if context is None:
            context = {}

        return self.write(cursor, uid, ids, {'state': state})
account_invoice()

class account_invoice_line(osv.osv):
    def _amount_line(self, cr, uid, ids, prop, unknow_none,unknow_dict):
        res = {}
        cur_obj=self.pool.get('res.currency')
        for line in self.browse(cr, uid, ids):
            if line.invoice_id:
                res[line.id] = line.price_unit * line.quantity * (1-(line.discount or 0.0)/100.0)
                cur = line.invoice_id.currency_id
                res[line.id] = cur_obj.float_round(cr, uid, cur, res[line.id])
            else:
                res[line.id] = float_round((line.price_unit *
                                            line.quantity *
                                            (1-(line.discount or 0.0)/100.0)),
                                           int(config['price_accuracy']))
        return res


    def _price_unit_default(self, cr, uid, context=None):
        if context is None:
            context = {}
        if 'check_total' in context:
            t = context['check_total']
            for l in context.get('invoice_line', {}):
                if isinstance(l, (list, tuple)) and len(l) >= 3 and l[2]:
                    tax_obj = self.pool.get('account.tax')
                    p = l[2].get('price_unit', 0) * (1-l[2].get('discount', 0)/100.0)
                    t = t - (p * l[2].get('quantity'))
                    taxes = l[2].get('invoice_line_tax_id')
                    if len(taxes[0]) >= 3 and taxes[0][2]:
                        taxes=tax_obj.browse(cr, uid, taxes[0][2])
                        for tax in tax_obj.compute(cr, uid, taxes, p,l[2].get('quantity'), context.get('address_invoice_id', False), l[2].get('product_id', False), context.get('partner_id', False)):
                            t = t - tax['amount']
            return t
        return 0

    _name = "account.invoice.line"
    _description = "Invoice line"
    _columns = {
        'name': fields.char('Description', size=256, required=True),
        'origin': fields.char('Origin', size=256, help="Reference of the document that produced this invoice."),
        'invoice_id': fields.many2one('account.invoice', 'Invoice Ref', ondelete='cascade', select=True),
        'uos_id': fields.many2one('product.uom', 'Unit of Measure', ondelete='set null'),
        'product_id': fields.many2one('product.product', 'Product', ondelete='set null'),
        'account_id': fields.many2one('account.account', 'Account', required=True, domain=[('type','<>','view'), ('type', '<>', 'closed')], help="The income or expense account related to the selected product."),
        'price_unit': fields.float('Unit Price', required=True, digits=(16, int(config['price_accuracy']))),
        'price_subtotal': fields.function(_amount_line, method=True, string='Subtotal',store=True, type="float", digits=(16, int(config['price_accuracy']))),
        'quantity': fields.float('Quantity', digits=(16, 3), required=True),
        'discount': fields.float('Discount (%)', digits=(16, int(config['price_accuracy']))),
        'invoice_line_tax_id': fields.many2many('account.tax', 'account_invoice_line_tax', 'invoice_line_id', 'tax_id', 'Taxes', domain=[('parent_id','=',False)]),
        'note': fields.text('Notes'),
        'account_analytic_id':  fields.many2one('account.analytic.account', 'Analytic Account'),
    }
    _defaults = {
        'quantity': lambda *a: 1,
        'discount': lambda *a: 0.0,
        'price_unit': _price_unit_default,
    }

    def product_id_change_unit_price_inv(self, cr, uid, tax_id, price_unit, qty, address_invoice_id, product, partner_id, context=None):
        tax_obj = self.pool.get('account.tax')
        if price_unit:
            taxes = tax_obj.browse(cr, uid, tax_id)
            for tax in tax_obj.compute_inv(cr, uid, taxes, price_unit, qty, address_invoice_id, product, partner_id):
                price_unit = price_unit - tax['amount']
        return {'price_unit': price_unit,'invoice_line_tax_id': tax_id}

    def product_id_change(self, cr, uid, ids, product, uom, qty=0, name='', type='out_invoice', partner_id=False, fposition_id=False, price_unit=False, address_invoice_id=False, context=None):
        if context is None:
            context = {}
        if not partner_id:
            raise osv.except_osv(_('No Partner Defined !'),_("You must first select a partner !") )
        if not product:
            if type in ('in_invoice', 'in_refund'):
                return {'value':{}, 'domain':{'product_uom':[]}}
            else:
                return {'value': {'price_unit': 0.0}, 'domain':{'product_uom':[]}}
        part = self.pool.get('res.partner').browse(cr, uid, partner_id)
        fpos = fposition_id and self.pool.get('account.fiscal.position').browse(cr, uid, fposition_id) or False

        if part.lang:
            context.update({'lang': part.lang})
        result = {}
        res = self.pool.get('product.product').browse(cr, uid, product, context=context)

        if type in ('out_invoice','out_refund'):
            a = res.product_tmpl_id.property_account_income.id
            if not a:
                categ_id = res.categ_id
                a = categ_id.property_account_income_categ.id
                while not a and categ_id.parent_id:
                    categ_id = categ_id.parent_id
                    a = categ_id.property_account_income_categ.id
        else:
            a = res.product_tmpl_id.property_account_expense.id
            if not a:
                categ_id = res.categ_id
                a = categ_id.property_account_expense_categ.id
                while not a and categ_id.parent_id:
                    categ_id = categ_id.parent_id
                    a = categ_id.property_account_expense_categ.id

        a = self.pool.get('account.fiscal.position').map_account(cr, uid, fpos, a)
        if a:
            result['account_id'] = a

        taxep=None
        tax_obj = self.pool.get('account.tax')
        if type in ('out_invoice', 'out_refund'):
            taxes = res.taxes_id and res.taxes_id or (a and self.pool.get('account.account').browse(cr, uid,a).tax_ids or False)
            tax_id = self.pool.get('account.fiscal.position').map_tax(cr, uid, fpos, taxes)
        else:
            taxes = res.supplier_taxes_id and res.supplier_taxes_id or (a and self.pool.get('account.account').browse(cr, uid,a).tax_ids or False)
            tax_id = self.pool.get('account.fiscal.position').map_tax(cr, uid, fpos, taxes)
        if type in ('in_invoice', 'in_refund'):
            to_update = self.product_id_change_unit_price_inv(cr, uid, tax_id, price_unit or res.standard_price, qty, address_invoice_id, product, partner_id, context=context)
            result.update(to_update)
        else:
            result.update({'price_unit': res.list_price, 'invoice_line_tax_id': tax_id})

        if not name:
            result['name'] = res.partner_ref

        domain = {}
        result['uos_id'] = uom or res.uom_id.id or False
        if result['uos_id']:
            res2 = res.uom_id.category_id.id
            if res2 :
                domain = {'uos_id':[('category_id','=',res2 )]}
        return {'value':result, 'domain':domain}

    def move_line_get(self, cr, uid, invoice_id, context=None):
        res = []
        tax_grouped = {}
        tax_obj = self.pool.get('account.tax')
        cur_obj = self.pool.get('res.currency')
        ait_obj = self.pool.get('account.invoice.tax')
        inv = self.pool.get('account.invoice').browse(cr, uid, invoice_id)
        company_currency = inv.company_id.currency_id.id
        cur = inv.currency_id

        for line in inv.invoice_line:
            mres = self.move_line_get_item(cr, uid, line, context)
            if not mres:
                continue
            res.append(mres)
            tax_code_found= False
            for tax in tax_obj.compute(cr, uid, line.invoice_line_tax_id,
                    (line.price_unit * (1.0 - (line['discount'] or 0.0) / 100.0)),
                    line.quantity, inv.address_invoice_id.id, line.product_id,
                    inv.partner_id):

                if inv.type in ('out_invoice', 'in_invoice'):
                    tax_code_id = tax['base_code_id']
                    tax_amount = line.price_subtotal * tax['base_sign']
                else:
                    tax_code_id = tax['ref_base_code_id']
                    tax_amount = line.price_subtotal * tax['ref_base_sign']

                if tax_code_found:
                    if not tax_code_id:
                        continue
                    res.append(self.move_line_get_item(cr, uid, line, context))
                    res[-1]['price'] = 0.0
                    res[-1]['account_analytic_id'] = False
                elif not tax_code_id:
                    continue
                tax_code_found = True

                res[-1]['tax_code_id'] = tax_code_id
                res[-1]['tax_amount'] = cur_obj.compute(cr, uid, inv.currency_id.id, company_currency, tax_amount, context={'date': inv.date_invoice})
        return res

    def move_line_get_item(self, cr, uid, line, context=None):
        return {
            'type':'src',
            'name': line.name[:64],
            'price_unit':line.price_unit,
            'quantity':line.quantity,
            'price':line.price_subtotal,
            'account_id':line.account_id.id,
            'product_id':line.product_id.id,
            'uos_id':line.uos_id.id,
            'account_analytic_id':line.account_analytic_id.id,
            'taxes':line.invoice_line_tax_id,
        }
    #
    # Set the tax field according to the account and the fiscal position
    #
    def onchange_account_id(self, cr, uid, ids, fposition_id, account_id):
        if not account_id:
            return {}
        taxes = self.pool.get('account.account').browse(cr, uid, account_id).tax_ids
        fpos = fposition_id and self.pool.get('account.fiscal.position').browse(cr, uid, fposition_id) or False
        res = self.pool.get('account.fiscal.position').map_tax(cr, uid, fpos, taxes)
        r = {'value':{'invoice_line_tax_id': res}}
        return r
account_invoice_line()

class account_invoice_tax(osv.osv):
    _name = "account.invoice.tax"
    _description = "Invoice Tax"
    _columns = {
        'invoice_id': fields.many2one('account.invoice', 'Invoice Line', ondelete='cascade', select=True),
        'name': fields.char('Tax Description', size=64, required=True),
        'account_id': fields.many2one('account.account', 'Tax Account', required=True, domain=[('type','<>','view'),('type','<>','income'), ('type', '<>', 'closed')]),
        'base': fields.float('Base', digits=(16,int(config['price_accuracy']))),
        'amount': fields.float('Amount', digits=(16,int(config['price_accuracy']))),
        'manual': fields.boolean('Manual'),
        'sequence': fields.integer('Sequence'),

        'base_code_id': fields.many2one('account.tax.code', 'Base Code', help="The account basis of the tax declaration."),
        'base_amount': fields.float('Base Code Amount', digits=(16,int(config['price_accuracy']))),
        'tax_code_id': fields.many2one('account.tax.code', 'Tax Code', help="The tax basis of the tax declaration."),
        'tax_amount': fields.float('Tax Code Amount', digits=(16,int(config['price_accuracy']))),
        'tax_id': fields.many2one('account.tax', 'Tax'),
    }
    
    def base_change(self, cr, uid, ids, base,currency_id=False,company_id=False,date_invoice=False):
        cur_obj = self.pool.get('res.currency')
        company_obj = self.pool.get('res.company')
        company_currency=False
        if company_id:            
            company_currency = company_obj.read(cr,uid,[company_id],['currency_id'])[0]['currency_id'][0]
        if currency_id and company_currency:
            base = cur_obj.compute(cr, uid, currency_id, company_currency, base, context={'date': date_invoice or time.strftime('%Y-%m-%d')}, round=False)
        return {'value': {'base_amount':base}}

    def amount_change(self, cr, uid, ids, amount,currency_id=False,company_id=False,date_invoice=False):
        cur_obj = self.pool.get('res.currency')
        company_obj = self.pool.get('res.company')
        company_currency=False
        if company_id:
            company_currency = company_obj.read(cr,uid,[company_id],['currency_id'])[0]['currency_id'][0]
        if currency_id and company_currency:
            amount = cur_obj.compute(cr, uid, currency_id, company_currency, amount, context={'date': date_invoice or time.strftime('%Y-%m-%d')}, round=False)
        return {'value': {'tax_amount':amount}}
    
    _order = 'sequence'
    _defaults = {
        'manual': lambda *a: 1,
        'base_amount': lambda *a: 0.0,
        'tax_amount': lambda *a: 0.0,
    }

    def compute(self, cr, uid, invoice_id, line_id=None, context=None):

        if not context:
            context = {}

        tax_obj = self.pool.get('account.tax')
        cur_obj = self.pool.get('res.currency')
        inv = self.pool.get('account.invoice').browse(cr, uid, invoice_id, context)
        cur = inv.currency_id
        company_currency = inv.company_id.currency_id.id
        tax_tree = {}
        tax_grouped = {}
        # For each line in the invoice compute taxes
        for line in inv.invoice_line:
            if line_id is not None and line.id != line_id:
                continue
            to_include = None
            # Ensure we evaluate taxes using sequence order
            line_taxes_full = []
            for tax in line.invoice_line_tax_id:
                if tax.child_depend:
                    line_taxes_full.extend([x for x in tax.child_ids])
                else:
                    line_taxes_full.append(tax)
            line_taxes = sorted([(x, x.sequence)
                                 for x in line_taxes_full],
                                key=lambda a: a[1])
            for tax, sequence in line_taxes:
                val = {}
                tax_id = val['tax_id'] = tax.id
                val['invoice_id'] = inv.id
                val['name'] = tax.name
                val['manual'] = False
                val['sequence'] = tax.sequence
                val['base'] = cur_obj.float_round(
                    cr, uid, cur, getattr(line, tax.field_based_on)
                )

                ctx = {'date': inv.date_invoice or time.strftime('%Y-%m-%d')}
                if inv.type in ('out_invoice','in_invoice'):
                    val['base_code_id'] = tax.base_code_id.id
                    val['tax_code_id'] = tax.tax_code_id.id
                    val['account_id'] = (tax.account_collected_id and
                                         tax.account_collected_id.id or
                                         line.account_id.id)
                    base_sign = tax.base_sign
                else:
                    val['base_code_id'] = tax.ref_base_code_id.id
                    val['tax_code_id'] = tax.ref_tax_code_id.id
                    val['account_id'] = (tax.account_paid_id and
                                         tax.account_paid_id.id or
                                         line.account_id.id)
                    base_sign = tax.ref_base_sign

                val['base_amount'] = cur_obj.compute(cr, uid,
                                              inv.currency_id.id,
                                              company_currency,
                                              val['base'] * base_sign,
                                              context=ctx, round=False)
                val['base_amount'] = cur_obj.float_round(cr, uid, cur,
                                                        val['base_amount'])
                # Group taxes by key
                tax_key = (val['tax_code_id'], val['base_code_id'], val['account_id'])
                if tax_key not in tax_grouped:
                    tax_grouped[tax_key] = val
                else:
                    tax_grouped[tax_key]['base'] += val['base']
                    tax_grouped[tax_key]['base_amount'] += val['base_amount']
                # If previous tax_amount must be included in this tax base
                # store it for later computing
                tax_tree_key = (tax_id, tax_key)
                tax_tree.setdefault(tax_tree_key, {})
                if to_include is not None:
                    inc_tax_id = to_include['tax_id']
                    tax_tree[tax_tree_key].setdefault(inc_tax_id, {'base': 0,
                                                             'base_amount': 0})
                    tax_tree[tax_tree_key][inc_tax_id]['base'] += to_include['base']
                    tax_tree[tax_tree_key][inc_tax_id]['base_amount'] += to_include['base_amount']
                # If tax_amount must be included in next tax base, store it
                if tax.include_base_amount:
                    to_include = val
                else:
                    to_include = None

        # With all base amounts already summed
        # and rounded compute tax amounts
        for tax_multikey, include_taxes in tax_tree.iteritems():
            tax_id = tax_multikey[0]
            tax_key = tax_multikey[1]
            tax = tax_obj.browse(cr, uid, tax_id)
            base_add = 0
            base_amount_add = 0
            # Include tax_amount from taxes inside tax
            for inc_tax_id, values in include_taxes.iteritems():
                inc_tax = tax_obj.browse(cr, uid, inc_tax_id)
                amount = tax_obj._unit_compute(cr, uid, [inc_tax],
                                               values['base'])[0]['amount']
                base_amount = tax_obj._unit_compute(cr, uid, [inc_tax],
                                        values['base_amount'])[0]['amount']
                base_add += cur_obj.float_round(cr, uid, cur, amount)
                base_amount_add += cur_obj.float_round(cr, uid, cur, base_amount)
            tax_grouped[tax_key]['base'] += base_add
            tax_grouped[tax_key]['base_amount'] += base_amount_add
            amount = tax_obj._unit_compute(cr, uid, [tax],
                                    tax_grouped[tax_key]['base'])[0]['amount']
            tax_amount = tax_obj._unit_compute(cr, uid, [tax],
                            tax_grouped[tax_key]['base_amount'])[0]['amount']
            tax_grouped[tax_key]['amount'] = cur_obj.float_round(cr, uid, cur, amount)
            tax_grouped[tax_key]['tax_amount'] = cur_obj.float_round(cr, uid, cur,
                                                              tax_amount)
        if context.get('string_key', False):
            res = {}
            for tax_key, tax_value in tax_grouped.iteritems():
                tax_id = str(tax_value['tax_id'])
                res[tax_id] = tax_value
            return res
        return tax_grouped

    def move_line_get(self, cr, uid, invoice_id):
        res = []
        cr.execute('SELECT * FROM account_invoice_tax WHERE invoice_id=%s', (invoice_id,))
        for t in cr.dictfetchall():
            if not t['amount'] \
                    and not t['tax_code_id'] \
                    and not t['tax_amount'] \
                    and not t['base']:
                continue
            res.append({
                'type':'tax',
                'name':t['name'],
                'price_unit': t['amount'],
                'quantity': 1,
                'price': t['amount'] or 0.0,
                'account_id': t['account_id'],
                'tax_code_id': t['tax_code_id'],
                'tax_amount': t['tax_amount']
            })
        return res
account_invoice_tax()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

