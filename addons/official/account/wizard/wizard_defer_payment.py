# coding=utf-8
from osv import osv, fields
from tools.translate import _


class WizardDeferPayment(osv.osv_memory):
    _name = 'wizard.defer.payment'

    _columns = {
        'deferred_date': fields.date('Deferred date', required=True),
        'journal_id': fields.many2one(
            'account.journal', 'Journal', required=True
        )
    }

    def defer(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        invoice_ids = context.get('active_ids', [])
        if not invoice_ids:
            raise osv.except_osv(
                _('Error'),
                _('No invoices selected')
            )

        inv_obj = self.pool.get('account.invoice')

        wiz = self.browse(cursor, uid, ids[0], context=context)

        for invoice_id in invoice_ids:
            inv_obj.defer_payment(
                cursor, uid, invoice_id, wiz.journal_id.id, wiz.deferred_date
            )
        return {}


WizardDeferPayment()
