# -*- encoding: utf-8 -*-
from osv import osv, fields
import base64
import StringIO
from datetime import datetime
import pandas as pd

TIPO_FACTURAS = [
    ('out', 'Facturas de cliente'),
    ('in', 'Facturas de proveedor'),
    ('all', 'Todas las facturas'),
]


class WizardExportAccountMoveLine(osv.osv_memory):
    _name = 'wizard.export.account.move.line'

    def build_report(self, rows, file_type, columns=None, headers=None):
        df = pd.DataFrame(rows, columns=columns)
        df['accumulated_balance'] = (df['debit'] - df['credit']).cumsum()
        if file_type == 'xls':
            output_report = StringIO.StringIO()
            writer = pd.ExcelWriter(output_report, engine='xlsxwriter')
            df.to_excel(writer, index=False, header=headers)
            writer.save()
            value = output_report.getvalue()
            output_report.close()
            return value
        elif file_type == 'csv':
            return df.to_csv(index=False, sep=';', header=headers)

    def export_entry_lines_data(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        wizard = self.browse(cursor, uid, ids[0], context=context)

        selected_ids = context.get('active_ids', [])

        header_fields = [
            'Date',
            'Period',
            'Account code',
            'Account name',
            'Partner',
            'Name',
            'Ref',
            'Move',
            'Debit',
            'Credit',
            'Accumulated balance',
        ]

        fields_to_read = [
            'date',  # Date
            'period_id.name',  # Period
            'account_id.code',  # Account code
            'account_id.name',  # Account name
            'partner_id.name',  # Partner
            'name',  # Name
            'ref',  # Ref
            'move_id.ref',  # Move
            'debit',  # Debit
            'credit',  # Credit
        ]

        acc_move_line_obj = self.pool.get('account.move.line')
        q = acc_move_line_obj.q(cursor, uid).select(fields_to_read).where(
            [('id', 'in', selected_ids)]
        )
        cursor.execute(*q)
        results = cursor.dictfetchall()

        file_generated = self.build_report(
            rows=results,
            columns=fields_to_read,
            file_type=wizard.file_type,
            headers=header_fields,
        )
        base_file_name = 'entry_lines'
        filename = '{}_{:%Y-%d-%m_%H-%M-%S}.{}'.format(
            base_file_name, datetime.now(), wizard.file_type
        )
        wizard.write({
            'file': base64.b64encode(file_generated),
            'filename': filename,
            'state': 'end'
        })

    _columns = {
        'state': fields.selection(
            [('init', 'Initial'), ('end', 'End')], string='Estado'
        ),
        'filename': fields.char('File name', size=64),
        'file_type': fields.selection(
            [('csv', 'CSV'), ('xls', 'Excel')],
            string='File type', required=True
        ),
        'file': fields.binary(
            string='File result', filters='*.csv,*.xls'),
    }

    _defaults = {
        'state': lambda *a: 'init',
    }


WizardExportAccountMoveLine()
