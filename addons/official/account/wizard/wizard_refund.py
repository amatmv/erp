# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    $Id$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import wizard
import pooler
from osv import osv, fields
import netsvc
import time
from tools.translate import _


class WizardRefund(osv.osv_memory):

    _name = 'wizard.refund'

    def action_confirm(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        self.write(cursor, uid, ids, {'state': 'conf'})
        return True

    def action_confirm_rectif(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        self.write(cursor, uid, ids, {'state': 'confRef'})
        return True

    def action_confirm_rectif_subs(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        self.write(cursor, uid, ids, {'state': 'confRefSubs'})
        return True

    def action_refund(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        invoice_obj = self.pool.get('account.invoice')

        invoice_ids = context.get('active_ids', [])

        created_invoices = invoice_obj.refund(cursor, uid, invoice_ids)

        self.write(
            cursor, uid, ids, {
                'state': 'end',
                'generated_invoices_ids': created_invoices
            }
        )

        return True

    def action_refund_and_rectify(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        invoice_obj = self.pool.get('account.invoice')

        invoice_ids = context.get('active_ids', [])

        created_invoices = invoice_obj.rectify(cursor, uid, invoice_ids)

        self.write(
            cursor, uid, ids, {
                'state': 'end',
                'generated_invoices_ids': created_invoices
            }
        )

        return True

    def action_rectify(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        invoice_obj = self.pool.get('account.invoice')

        invoice_ids = context.get('active_ids', [])

        created_invoices = invoice_obj.rectify_only(
            cursor, uid, invoice_ids, rectificative_type='RA'
        )

        self.write(
            cursor, uid, ids, {
                'state': 'end',
                'generated_invoices_ids': created_invoices
            }
        )

        return True

    def action_end(self, cursor, uid, ids, context=None):
        """Acció final del wizard.

        Retornem les factures creades per repassar-les
        """
        wiz = self.browse(cursor, uid, ids[0])
        return {
            'domain': "[('id','in', {0})]".format(wiz.generated_invoices_ids),
            'name': _('Factures generades'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.invoice',
            'type': 'ir.actions.act_window'
        }

    _columns = {
        'state': fields.selection(
            [
                ('init', 'Initial'),
                ('conf', 'Confirm'),
                ('confRef', 'Confirm Refund'),
                ('confRefSubs', 'Confirm Refund by Substitution'),
                ('end', 'End')
            ], 'State'
        ),
        'date': fields.date('Operation date'),
        'generated_invoices_ids': fields.json('Generated invoices'),
    }

    _defaults = {
        'date': lambda *a: time.strftime('%Y-%m-%d'),
        'state': lambda *a: 'init',
    }

WizardRefund()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

