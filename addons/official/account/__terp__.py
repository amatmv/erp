# -*- coding: utf-8 -*-
{
    "name": "Accounting and financial management",
    "description": """Financial and accounting module that covers:
    General accounting
    Cost / Analytic accounting
    Third party accounting
    Taxes management
    Budgets
    Customer and Supplier Invoices
    Bank statements
    """,
    "version": "0-dev",
    "author": "Tiny",
    "category": "",
    "depends":[
        "product",
        "base",
        "process"
    ],
    "init_xml": [],
    "demo_xml":[
        "account_demo.xml",
        "project/project_demo.xml",
        "project/analytic_account_demo.xml",
        "demo/account_minimal.xml",
        "demo/invoice_demo.xml",
        "test/invoice_basic_test.xml",
        "test/invoice_bug452854_test.xml"
    ],
    "update_xml":[
        "security/account_security.xml",
        "security/ir.model.access.csv",
        "account_menuitem.xml",
        "account_wizard.xml",
        "account_view.xml",
        "account_end_fy.xml",
        "account_invoice_view.xml",
        "account_report.xml",
        "partner_view.xml",
        "data/account_invoice.xml",
        "data/account_data2.xml",
        "account_invoice_workflow.xml",
        "project/project_view.xml",
        "project/project_report.xml",
        "product_view.xml",
        "process/statement_process.xml",
        "process/customer_invoice_process.xml",
        "process/supplier_invoice_process.xml",
        "sequence_view.xml",
        "company_view.xml",
        "wizard/wizard_refund_view.xml",
        "wizard/wizard_defer_payment_view.xml",
        "wizard/wizard_export_account_move_line_view.xml",
    ],
    "active": False,
    "installable": True
}
