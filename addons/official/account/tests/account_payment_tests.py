# coding=utf-8
from datetime import datetime, timedelta
from destral import testing


class AccountPaymentTerm(testing.OOTestCaseWithCursor):

    def test_get_date_due_from_payemnt_term(self):
        imd_obj = self.openerp.pool.get('ir.model.data')
        pterm_obj = self.openerp.pool.get('account.payment.term')

        cursor = self.cursor
        uid = self.uid

        pterm_id = imd_obj.get_object_reference(
            cursor, uid, 'account', 'account_payment_term'
        )[1]

        due_date = pterm_obj.get_due_date(cursor, uid, pterm_id, '2018-01-05')
        self.assertEqual(due_date, '2018-02-28')

    def test_create_moves_from_payment_terms_lines(self):
        imd_obj = self.openerp.pool.get('ir.model.data')
        inv_obj = self.openerp.pool.get('account.invoice')
        line_obj = self.openerp.pool.get('account.invoice.line')
        prod_obj = self.openerp.pool.get('product.product')
        pterm_obj = self.openerp.pool.get('account.payment.term')

        cursor = self.cursor
        uid = self.uid

        import netsvc
        wkf = netsvc.LocalService('workflow')

        inv_id = imd_obj.get_object_reference(
            cursor, uid, 'account', 'invoice_0001'
        )[1]

        payment_term_id = imd_obj.get_object_reference(
            cursor, uid, 'account', 'payment_term_30_60_90'
        )[1]

        date_invoice = datetime.now().strftime('%Y-%m-%d')
        due_date = pterm_obj.get_due_date(
            cursor, uid, payment_term_id, date_invoice
        )

        inv_obj.write(cursor, uid, [inv_id], {
            'date_invoice': date_invoice,
            'payment_term': payment_term_id,
            'date_due': due_date
        })

        prod_id = imd_obj.get_object_reference(
            cursor, uid, 'product', 'product_product_pc1'
        )[1]

        product = prod_obj.browse(cursor, uid, prod_id)

        # Create invoice lines
        line_obj.create(cursor, uid, {
            'invoice_id': inv_id,
            'name': product.name,
            'product_id': product.id,
            'account_id': product.categ_id.property_account_income_categ.id,
            'quantity': 1,
            'price_unit': 450

        })

        wkf.trg_validate(
            uid, 'account.invoice', inv_id, 'invoice_open', cursor
        )

        invoice = inv_obj.browse(cursor, uid, inv_id)
        self.assertEqual(invoice.state, 'open')
        self.assertEqual(invoice.date_invoice, date_invoice)
        self.assertEqual(invoice.date_due, due_date)
        self.assertEqual(invoice.amount_total, 450)
        self.assertEqual(len(invoice.move_line_id_payment_get()), 3)
