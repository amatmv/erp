# -*- coding: utf-8 -*-
import unittest
from datetime import datetime, timedelta

import netsvc
from destral import testing
from destral.transaction import Transaction
from expects import expect, equal, be_empty


class AccountInvoiceTests(testing.OOTestCase):

    def test_duplicate_invoice_fields(self):
        """This test checks that certain fields are not copied when an
         invoice is duplicated"""
        pool = self.openerp.pool
        invoice_obj = pool.get('account.invoice')
        imd_obj = pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            # Import the invoice to duplicate
            invoice_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'invoice_0001'
            )[1]

            # Duplicate the invoice
            invoice_copy, x = invoice_obj.copy_data(cursor, uid, invoice_id)

            # Expected values after duplicating the invoice
            expected_values = {
                'number': False,
                'state': 'draft',
                'origin': False,
                'origin_date_invoice': False,
                'reference': False,
                'period_id': False
            }
            # Select the fields we want to check
            invoice_copy_data = {
                i: invoice_copy[i] for i in expected_values.keys()
            }
            # Check that expected fields are equal to new invoice fields
            self.assertDictEqual(invoice_copy_data, expected_values)

    def test_refund_by_is_coherent(self):
        invoice_obj = self.openerp.pool.get('account.invoice')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            invoice_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'invoice_0001'
            )[1]

            # Initially, it should be False because it hasn't been refund by
            # any other invoice
            invoice = invoice_obj.browse(cursor, uid, invoice_id)
            expect(invoice.refund_by_id.id).to(equal(False))

            refund_by_ids = invoice_obj.refund(cursor, uid, [invoice_id])

            # When we refund it it should point to the invoice we just created
            invoice = invoice_obj.browse(cursor, uid, invoice_id)
            expect(invoice.refund_by_id.id).to(equal(refund_by_ids[0]))

            invoice_obj.unlink(cursor, uid, refund_by_ids)

            # If we delete the invoice refunding it, the value should clear
            # itself and go back to False
            invoice = invoice_obj.browse(cursor, uid, invoice_id)
            expect(invoice.refund_by_id.id).to(equal(False))

            rectifyed_ids = invoice_obj.rectify(cursor, uid, [invoice_id])

            # When we rectify it it should point to the invoice we just created
            # that is actually refunding it (type B)
            refund_by_ids = [
                inv_vals['id']
                for inv_vals in invoice_obj.read(
                    cursor, uid, rectifyed_ids, ['rectificative_type']
                )
                if inv_vals['rectificative_type'] == 'B'
            ]
            invoice = invoice_obj.browse(cursor, uid, invoice_id)
            expect(invoice.refund_by_id.id).to(equal(refund_by_ids[0]))

            invoice_obj.unlink(cursor, uid, refund_by_ids)

            # If we delete the invoice refunding it, the value should clear
            # itself and go back to False the same way as before
            invoice = invoice_obj.browse(cursor, uid, invoice_id)
            expect(invoice.refund_by_id.id).to(equal(False))

    def test_residual_is_correct_on_ra_draft(self):
        account_obj = self.openerp.pool.get('account.account')
        invoice_obj = self.openerp.pool.get('account.invoice')
        inv_line_obj = self.openerp.pool.get('account.invoice.line')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            invoice_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'test_invoice_1'
            )[1]

            rectify_ids = invoice_obj.refund(
                cursor, uid, [invoice_id], rectificative_type='BRA'
            )

            rectify_ids += invoice_obj.rectify_only(
                cursor, uid, [invoice_id], rectificative_type='RA'
            )

            expect(len(rectify_ids)).to(equal(2))

            account_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'a_sale'
            )[1]

            ra_invoice_id = rectify_ids[1]

            inv_line_obj.create(
                cursor, uid, {
                    'name': 'New line',
                    'price_unit': 10.0,
                    'quantity': 1.0,
                    'invoice_id': ra_invoice_id,
                    'account_id': account_id,
                }
            )

            rectify = invoice_obj.browse(cursor, uid, ra_invoice_id)

            expect(rectify.rectificative_type).to(equal('RA'))
            expect(rectify.residual).to(equal(rectify.rectifying_id.amount_total + 10))
            expect(rectify.saldo).to(equal(10))

            inv_line_obj.create(
                cursor, uid, {
                    'name': 'New line',
                    'price_unit': 15.0,
                    'quantity': 1.0,
                    'invoice_id': ra_invoice_id,
                    'account_id': account_id,
                }
            )

            rectify = invoice_obj.browse(cursor, uid, ra_invoice_id)

            expect(rectify.residual).to(equal(rectify.rectifying_id.amount_total + 25))
            expect(rectify.saldo).to(equal(25))

    def test_residual_is_correct_on_ra_after_opening_for_postitive(self):
        account_obj = self.openerp.pool.get('account.account')
        invoice_obj = self.openerp.pool.get('account.invoice')
        inv_line_obj = self.openerp.pool.get('account.invoice.line')
        imd_obj = self.openerp.pool.get('ir.model.data')

        wf_service = netsvc.LocalService('workflow')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            invoice_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'test_invoice_1'
            )[1]

            rectify_ids = invoice_obj.refund(
                cursor, uid, [invoice_id], rectificative_type='BRA'
            )

            rectify_ids += invoice_obj.rectify_only(
                cursor, uid, [invoice_id], rectificative_type='RA'
            )

            expect(len(rectify_ids)).to(equal(2))

            ra_invoice_id = rectify_ids[1]

            account_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'a_sale'
            )[1]

            inv_line_id = inv_line_obj.create(
                cursor, uid, {
                    'name': 'New line',
                    'price_unit': 10.0,
                    'quantity': 1.0,
                    'invoice_id': ra_invoice_id,
                    'account_id': account_id,
                }
            )

            wf_service.trg_validate(
                uid, 'account.invoice', ra_invoice_id, 'invoice_open', cursor
            )

            rectify = invoice_obj.browse(cursor, uid, ra_invoice_id)

            expect(rectify.rectificative_type).to(equal('RA'))
            expect(rectify.state).to(equal('open'))
            expect(rectify.residual).to(equal(10))
            expect(rectify.saldo).to(equal(10))

    def test_residual_is_correct_on_ra_after_opening_for_negative(self):
        account_obj = self.openerp.pool.get('account.account')
        invoice_obj = self.openerp.pool.get('account.invoice')
        inv_line_obj = self.openerp.pool.get('account.invoice.line')
        imd_obj = self.openerp.pool.get('ir.model.data')

        wf_service = netsvc.LocalService('workflow')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            invoice_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'test_invoice_1'
            )[1]

            rectify_ids = invoice_obj.refund(
                cursor, uid, [invoice_id], rectificative_type='BRA'
            )

            rectify_ids += invoice_obj.rectify_only(
                cursor, uid, [invoice_id], rectificative_type='RA'
            )

            expect(len(rectify_ids)).to(equal(2))

            ra_invoice_id = rectify_ids[1]

            rectify_inv = invoice_obj.browse(
                cursor, uid, ra_invoice_id
            )

            line = [
                line
                for line in rectify_inv.invoice_line
                if line.quantity == 2
            ][0]

            line.write({'quantity': 1})

            wf_service.trg_validate(
                uid, 'account.invoice', ra_invoice_id, 'invoice_open', cursor
            )

            rectify = invoice_obj.browse(cursor, uid, ra_invoice_id)

            # TODO: Revise that this is actually correct and legal, ...
            # Since we are invoice one unit less we expect the total to be the
            # negative value of one unit
            expected_residual = -line.price_unit

            expect(rectify.rectificative_type).to(equal('RA'))
            expect(rectify.state).to(equal('open'))
            expect(rectify.residual).to(equal(expected_residual))
            expect(rectify.saldo).to(equal(expected_residual))

    def test_residual_and_saldo_are_correct_after_pay_without(self):
        account_obj = self.openerp.pool.get('account.account')
        invoice_obj = self.openerp.pool.get('account.invoice')
        inv_line_obj = self.openerp.pool.get('account.invoice.line')
        imd_obj = self.openerp.pool.get('ir.model.data')

        wf_service = netsvc.LocalService('workflow')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            invoice_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'test_invoice_1'
            )[1]

            rectify_ids = invoice_obj.rectify_only(
                cursor, uid, [invoice_id], rectificative_type='RA'
            )

            ra_invoice_id = rectify_ids[0]

            expect(len(rectify_ids)).to(equal(1))

            account_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'a_sale'
            )[1]

            inv_line_obj.create(
                cursor, uid, {
                    'name': 'New line',
                    'price_unit': 10.0,
                    'quantity': 1.0,
                    'invoice_id': ra_invoice_id,
                    'account_id': account_id,
                }
            )

            wf_service.trg_validate(
                uid, 'account.invoice', ra_invoice_id, 'invoice_open', cursor
            )

            rectify = invoice_obj.browse(cursor, uid, ra_invoice_id)

            expect(rectify.rectificative_type).to(equal('RA'))
            expect(rectify.state).to(equal('open'))
            expect(rectify.saldo).to(equal(10))
            expect(rectify.residual).to(equal(10))

            journal_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'sales_journal'
            )[1]

            rectify.pay_and_reconcile(
                10, account_id, rectify.period_id.id, journal_id,
                None, None, None
            )

            rectify = invoice_obj.browse(cursor, uid, ra_invoice_id)

            expect(rectify.state).to(equal('paid'))
            expect(rectify.residual).to(equal(0))
            expect(rectify.saldo).to(equal(10))

    def test_open_ra_invoice_bra_compenses_rectifying_invoice_if_original_is_paid(self):
        """Opening a RA invoice, if original invoice is  paid we have to
        compense refund with the rectifying.
        """
        invoice_obj = self.openerp.pool.get('account.invoice')
        inv_line_obj = self.openerp.pool.get('account.invoice.line')
        imd_obj = self.openerp.pool.get('ir.model.data')

        wf_service = netsvc.LocalService('workflow')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            invoice_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'test_invoice_1'
            )[1]

            rectify_ids = invoice_obj.refund(
                cursor, uid, [invoice_id], rectificative_type='BRA'
            )

            rectify_ids += invoice_obj.rectify_only(
                cursor, uid, [invoice_id], rectificative_type='RA'
            )

            bra_invoice_id = rectify_ids[0]
            ra_invoice_id = rectify_ids[1]

            expect(len(rectify_ids)).to(equal(2))

            account_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'a_sale'
            )[1]

            inv_line_obj.create(
                cursor, uid, {
                    'name': 'New line',
                    'price_unit': 10.0,
                    'quantity': 1.0,
                    'invoice_id': ra_invoice_id,
                    'account_id': account_id,
                }
            )

            wf_service.trg_validate(
                uid, 'account.invoice', ra_invoice_id, 'invoice_open', cursor
            )

            rectify = invoice_obj.browse(cursor, uid, ra_invoice_id)

            expect(rectify.rectificative_type).to(equal('RA'))
            expect(rectify.state).to(equal('open'))
            expect(rectify.residual).to(equal(10))
            expect(rectify.saldo).to(equal(10))

            bra_invoice = invoice_obj.browse(cursor, uid, bra_invoice_id)

            expect(bra_invoice.rectificative_type).to(equal('BRA'))
            expect(bra_invoice.residual).to(equal(0))
            expect(bra_invoice.state).to(equal('paid'))

            journal_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'sales_journal'
            )[1]

            rectify.pay_and_reconcile(
                10, account_id, rectify.period_id.id, journal_id,
                None, None, None
            )

            rectify = invoice_obj.browse(cursor, uid, ra_invoice_id)

            expect(rectify.state).to(equal('paid'))
            expect(rectify.residual).to(equal(0))
            expect(rectify.saldo).to(equal(10))

    def test_open_ra_invoice_bra_compenses_original_invoice_if_original_is_not_paid(self):
        """Opening a RA invoice, if original invoice is not paid we have to
        compense refund with the rectifying.
        """
        invoice_obj = self.openerp.pool.get('account.invoice')
        inv_line_obj = self.openerp.pool.get('account.invoice.line')
        imd_obj = self.openerp.pool.get('ir.model.data')

        wf_service = netsvc.LocalService('workflow')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            invoice_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'test_invoice_original_unpaid'
            )[1]

            wf_service.trg_validate(
                uid, 'account.invoice', invoice_id, 'invoice_open', cursor
            )

            orig_invoice = invoice_obj.browse(cursor, uid, invoice_id)
            expect(orig_invoice.state).to(equal('open'))

            rectify_ids = invoice_obj.refund(
                cursor, uid, [invoice_id], rectificative_type='BRA'
            )

            rectify_ids += invoice_obj.rectify_only(
                cursor, uid, [invoice_id], rectificative_type='RA'
            )

            bra_invoice_id = rectify_ids[0]
            ra_invoice_id = rectify_ids[1]

            expect(len(rectify_ids)).to(equal(2))

            account_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'a_sale'
            )[1]

            inv_line_obj.create(
                cursor, uid, {
                    'name': 'New line',
                    'price_unit': 10.0,
                    'quantity': 1.0,
                    'invoice_id': ra_invoice_id,
                    'account_id': account_id,
                }
            )

            wf_service.trg_validate(
                uid, 'account.invoice', ra_invoice_id, 'invoice_open', cursor
            )

            rectify = invoice_obj.browse(cursor, uid, ra_invoice_id)

            expect(rectify.rectificative_type).to(equal('RA'))
            expect(rectify.state).to(equal('open'))
            expect(rectify.residual).to(equal(rectify.rectifying_id.amount_total + 10))
            expect(rectify.saldo).to(equal(10))

            bra_invoice = invoice_obj.browse(cursor, uid, bra_invoice_id)

            orig_invoice = invoice_obj.browse(cursor, uid, invoice_id)
            expect(orig_invoice.state).to(equal('paid'))

            expect(bra_invoice.rectificative_type).to(equal('BRA'))
            expect(bra_invoice.state).to(equal('paid'))

            journal_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'sales_journal'
            )[1]

            rectify.pay_and_reconcile(
                1210, account_id, rectify.period_id.id, journal_id,
                None, None, None
            )

            rectify = invoice_obj.browse(cursor, uid, ra_invoice_id)

            expect(rectify.state).to(equal('paid'))
            expect(rectify.residual).to(equal(0))
            expect(rectify.saldo).to(equal(10))
    
    def test_invoice_is_not_payable_if_it_is_paid(self):
        """Test a paid invoice is not payable manually"""
        imd_obj = self.openerp.pool.get('ir.model.data')
        invoice_obj = self.openerp.pool.get('account.invoice')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            invoice_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'invoice_0001'
            )[1]

            invoice_obj.write(
                cursor, uid, [invoice_id],
                {'state': 'open', 'reconciled': False}
            )
            is_not_payable = invoice_obj.is_not_payable_manually(
                cursor, uid, invoice_id
            )
            self.assertFalse(is_not_payable)

            states = ['paid', 'draft']
            for state in states:
                invoice_obj.write(
                    cursor, uid, [invoice_id],
                    {'state': state, 'reconciled': True}
                )
                is_not_payable = invoice_obj.is_not_payable_manually(
                    cursor, uid, invoice_id
                )
                self.assertTrue(is_not_payable)

    def test_field_based_on_is_set_default(self):
        """
        Test that templates are set with default
        field_based_on = 'price_subtotal'.
        """
        tax_template_obj = self.openerp.pool.get('account.tax.template')

        with Transaction().start(self.database) as txn:

            cursor = txn.cursor
            uid = txn.user

            tax_templates_ids = tax_template_obj.search(cursor, uid, [])
            field_based_on_tax_templ_ids = tax_template_obj.search(
                cursor, uid, [('field_based_on', '=', 'price_subtotal')]
            )

            diff = list(
                set(tax_templates_ids) - set(field_based_on_tax_templ_ids)
            )
            expect(diff).to(be_empty)

    def test_default_tax_applies_on_price_subtotal(self):
        """
        Test that the DEFAULT functionality isn't broken by the introduction of
        the field_based_on field.
        """
        tax_obj = self.openerp.pool.get('account.tax')
        inv_line_obj = self.openerp.pool.get('account.invoice.line')
        invoice_obj = self.openerp.pool.get('account.invoice')
        imd_obj = self.openerp.pool.get('ir.model.data')

        wf_service = netsvc.LocalService('workflow')

        with Transaction().start(self.database) as txn:

            cursor = txn.cursor
            uid = txn.user

            invoice_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'invoice_0001'
            )[1]

            test_invoice_line = imd_obj.get_object_reference(
                cursor, uid, 'account', 'test_invoice_original_line_1'
            )[1]

            # We delete all the lines existing in the invoice
            inv_line_obj.unlink(cursor, uid, inv_line_obj.search(
                cursor, uid, [('invoice_id', '=', invoice_id)]
            ))

            tax_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'demo_tax_01'
            )[1]

            # And we modify the test_invoice_line to associate it with the demo
            # invoice
            inv_line_obj.write(cursor, uid, [test_invoice_line], {
                'invoice_id': invoice_id,
                'invoice_line_tax_id': [(6, 0, [tax_id])],
            })

            wf_service.trg_validate(
                uid, 'account.invoice', invoice_id, 'invoice_open', cursor
            )

            inv_vals = invoice_obj.read(
                cursor, uid, invoice_id, ['amount_tax']
            )

            inv_line_vals = inv_line_obj.read(
                cursor, uid, test_invoice_line, ['price_subtotal']
            )

            # If the tax is based on quantity, and amount_taxed in the invoice
            # must be the the price subtotal of the invoice
            expect(inv_vals['amount_tax']).to(
                equal(inv_line_vals['price_subtotal'] * 0.5)
            )

    def test_open_invoice_calculates_taxes_with_correct_base_field(self):
        """
        Test if the field_based_on works on the tax model and the template model.
        When opening an invoice_the taxes applied to it should be based on the
        field_based_on, that is a field of the invoice lines.
        This test checks the behaviour for the two fields: 'price_subtotal'
        and 'quantity'.
        """

        inv_tax_obj = self.openerp.pool.get('account.invoice.tax')
        tax_obj = self.openerp.pool.get('account.tax')
        inv_line_obj = self.openerp.pool.get('account.invoice.line')
        invoice_obj = self.openerp.pool.get('account.invoice')
        imd_obj = self.openerp.pool.get('ir.model.data')

        wf_service = netsvc.LocalService('workflow')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            invoice_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'invoice_0001'
            )[1]

            test_invoice_line = imd_obj.get_object_reference(
                cursor, uid, 'account', 'test_invoice_original_line_1'
            )[1]

            tax_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'demo_tax_01'
            )[1]

            tax_obj.write(cursor, uid, tax_id, {
                'amount': 1,
                'field_based_on': 'quantity'
            })

            # We delete all the lines existing in the invoice
            inv_line_obj.unlink(cursor, uid, inv_line_obj.search(
                cursor, uid, [('invoice_id', '=', invoice_id)]
            ))

            # And we modify the test_invoice_line to associate it with the demo
            # invoice
            inv_line_obj.write(cursor, uid, [test_invoice_line], {
                'invoice_id': invoice_id,
                'invoice_line_tax_id': [(6, 0, [tax_id])],
            })

            wf_service.trg_validate(
                uid, 'account.invoice', invoice_id, 'invoice_open', cursor
            )

            inv_vals = invoice_obj.read(
                cursor, uid, invoice_id, ['amount_tax']
            )

            inv_line_vals = inv_line_obj.read(
                cursor, uid, test_invoice_line, ['price_subtotal', 'quantity']
            )

            # If the tax is based on quantity, and amount_taxed in the invoice
            # must be the the price subtotal of the invoice
            expect(inv_vals['amount_tax']).to(equal(inv_line_vals['quantity']))

            # We repeat the process for the field_based_on = 'quantity'
            tax_obj.write(cursor, uid, tax_id, {
                'field_based_on': 'price_subtotal'
            })

            self.openerp.pool.get('account.journal').write(
                cursor, uid, invoice_obj.read(
                    cursor, uid, invoice_id, ['journal_id']
                )['journal_id'][0], {'update_posted': True}
            )

            wf_service.trg_validate(
                uid, 'account.invoice', invoice_id, 'invoice_cancel', cursor
            )

            invoice_obj.action_cancel_draft(cursor, uid, [invoice_id])

            inv_tax_obj.unlink(
                cursor, uid, invoice_obj.read(
                    cursor, uid, invoice_id, ['tax_line']
                )['tax_line']
            )

            wf_service.trg_validate(
                uid, 'account.invoice', invoice_id, 'invoice_open', cursor
            )

            inv_vals = invoice_obj.read(
                cursor, uid, invoice_id, ['amount_tax']
            )

            expect(inv_vals['amount_tax']).to(
                equal(inv_line_vals['price_subtotal'])
            )


class AccountInvoicePaymentTests(testing.OOTestCaseWithCursor):

    def test_on_postponement_due_date_is_updated(self):
        imd_obj = self.openerp.pool.get('ir.model.data')
        inv_obj = self.openerp.pool.get('account.invoice')
        line_obj = self.openerp.pool.get('account.invoice.line')
        prod_obj = self.openerp.pool.get('product.product')
        pterm_obj = self.openerp.pool.get('account.payment.term')
        ml_obj = self.openerp.pool.get('account.move.line')

        cursor = self.cursor
        uid = self.uid

        import netsvc
        wkf = netsvc.LocalService('workflow')

        inv_id = imd_obj.get_object_reference(
            cursor, uid, 'account', 'invoice_0001'
        )[1]

        payment_term_id = imd_obj.get_object_reference(
            cursor, uid, 'account', 'account_payment_term'
        )[1]

        journal_id = imd_obj.get_object_reference(
            cursor, uid, 'account', 'bank_journal'
        )[1]

        date_invoice = datetime.now().strftime('%Y-%m-%d')
        due_date = pterm_obj.get_due_date(
            cursor, uid, payment_term_id, date_invoice
        )

        inv_obj.write(cursor, uid, [inv_id], {
            'date_invoice': date_invoice,
            'payment_term': payment_term_id,
            'date_due': due_date
        })

        prod_id = imd_obj.get_object_reference(
            cursor, uid, 'product', 'product_product_pc1'
        )[1]

        product = prod_obj.browse(cursor, uid, prod_id)

        # Create invoice lines
        line_obj.create(cursor, uid, {
            'invoice_id': inv_id,
            'name': product.name,
            'product_id': product.id,
            'account_id': product.categ_id.property_account_income_categ.id,
            'quantity': 1,
            'price_unit': 450

        })

        wkf.trg_validate(
            uid, 'account.invoice', inv_id, 'invoice_open', cursor
        )

        invoice = inv_obj.browse(cursor, uid, inv_id)
        self.assertEqual(invoice.state, 'open')
        self.assertEqual(invoice.date_invoice, date_invoice)
        self.assertEqual(invoice.date_due, due_date)
        self.assertEqual(invoice.amount_total, 450)

        deferred_date = (
                datetime.now() + timedelta(days=60)
        ).strftime('%Y-%m-%d')

        invoice.defer_payment(journal_id, deferred_date)

        invoice = inv_obj.browse(cursor, uid, inv_id)
        self.assertEqual(invoice.state, 'open')
        self.assertEqual(
            invoice.date_due, deferred_date
        )
        self.assertEqual(invoice.payment_ids[-1].date_maturity, deferred_date)
        self.assertEqual(invoice.residual, 450)
