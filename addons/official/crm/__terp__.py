# -*- coding: utf-8 -*-
{
    "name": "Customer & Supplier Relationship Management",
    "description": """The generic Open ERP Customer Relationship Management
system enables a group of people to intelligently and efficiently manage
leads, opportunities, tasks, issues, requests, bugs, campaign, claims, etc.
It manages key tasks such as communication, identification, prioritization,
assignment, resolution and notification.

Open ERP ensures that all cases are successfully tracked by users, customers and
suppliers. It can automatically send reminders, escalate the request, trigger
specific methods and lots of others actions based on your enterprise own rules.

The greatest thing about this system is that users don't need to do anything
special. They can just send email to the request tracker. Open ERP will take
care of thanking them for their message, automatically routing it to the
appropriate staff, and making sure all future correspondence gets to the right
place.

The CRM module has a email gateway for the synchronisation interface
between mails and Open ERP.""",
    "version": "0-dev",
    "author": "Tiny",
    "category": "Generic Modules/CRM & SRM",
    "depends":[
        "base",
        "base_extended"
    ],
    "init_xml":[
        "crm_data.xml"
    ],
    "demo_xml":[
        "crm_demo.xml"
    ],
    "update_xml":[
        "crm_view.xml",
        "crm_report.xml",
        "crm_wizard.xml",
        "security/crm_security.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
