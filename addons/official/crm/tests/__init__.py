# coding=utf-8
from __future__ import unicode_literals

from destral import testing
from destral.transaction import Transaction

from expects import *


class TestCRMCase(testing.OOTestCase):
    def setUp(self):
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user
        self.pool = self.openerp.pool

    def tearDown(self):
        self.txn.stop()
        self.cursor = False
        self.uid = False

    def test_section_subscribe(self):
        section_obj = self.pool.get('crm.case.section')
        address_obj = self.pool.get('res.partner.address')
        user_obj = self.pool.get('res.users')
        test_mail = 'user@example.com'
        # First create the section for this test
        vals = {
            'name': 'test_section',
        }
        section_id = section_obj.create(self.cursor, self.uid, vals)

        # Then Check for the user's email address
        # -> If the default demo does no carry it, add one
        address_id = user_obj.read(
            self.cursor, self.uid, self.uid, ['address_id'])
        if not address_id.get('address_id', False):
            address_id = address_obj.create(self.cursor, self.uid, {
                'name': 'User Testing Address',
                'email': test_mail,
            })
            user_obj.write(self.cursor, self.uid, self.uid, {
                'address_id': address_id
            })
        else:
            address_id = address_id['address_id'][0]
            address_obj.write(self.cursor, self.uid, address_id, {
                'email': test_mail
            })

        subs_ids = section_obj.read(
            self.cursor, self.uid, section_id, ['subscriber_ids']
        )['subscriber_ids']
        self.assertEqual(
            subs_ids, [],
            msg='A new section without subscribers should not have subsribers'
        )

        section_obj.subscribe_me(self.cursor, self.uid, section_id)

        subs_ids = section_obj.read(
            self.cursor, self.uid, section_id, ['subscriber_ids']
        )['subscriber_ids']
        self.assertTrue(
            address_id in subs_ids,
            msg="After subscribing user, user's email address should be in the "
                "subscriber's list"
        )

        subs = section_obj.get_subscriber_emails(
            self.cursor, self.uid, section_id, ['subscriber_ids'])

        self.assertEqual(
            subs, [test_mail],
            msg="Get subscriber emails,"
                " should return a list with the user's email."
        )


class TestCRMMailing(testing.OOTestCaseWithCursor):
    def test_get_case_emails(self):
        address_obj = self.openerp.pool.get('res.partner.address')
        case_obj = self.openerp.pool.get('crm.case')
        imd_obj = self.openerp.pool.get('ir.model.data')
        case_id = imd_obj.get_object_reference(
            self.cursor, self.uid, 'crm', 'crm_case_0001'
        )[1]
        case = case_obj.browse(self.cursor, self.uid, case_id)
        self.assertFalse(case.get_cc_emails(), 'Case should not have CC emails')
        self.assertFalse(case.get_bcc_emails(),
                         'Case should not have BCC emails')
        case.write({
            'email_cc': 'demo@example.com'
        })
        addr_id = address_obj.search(self.cursor, self.uid, [], limit=1)
        address_obj.write(self.cursor, self.uid, addr_id, {
            'email': 'user@example.com'
        })
        case.section_id.write({
            'subscriber_ids': [(6, 0, addr_id)]
        })

        self.assertEqual(case.get_cc_emails(), ['demo@example.com'],
                         'Case should return CC emails')
        self.assertEqual(case.get_bcc_emails(), ['user@example.com'],
                         'Case should return BCC emails')
