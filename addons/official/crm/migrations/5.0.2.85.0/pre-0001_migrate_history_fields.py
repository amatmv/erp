# coding=utf-8
from oopgrade import oopgrade
import logging


def up(cursor, installed_version):
    logger = logging.getLogger('migration')
    if not installed_version:
        return

    cursor.execute("SELECT COUNT(id) FROM crm_case_rule")
    rules_ids = cursor.fetchone()[0]
    logger.info(
        'Renaming "trg_max_history" to "trg_min_history" '
        'of {0} rules'.format(rules_ids)
    )

    oopgrade.rename_columns(cursor, {
        'crm_case_rule': [('trg_max_history', 'trg_min_history')]
    })

    logger.info(
        'Renaming "trg_limit_history" to "trg_max_history_log" '
        'of {0} rules'.format(rules_ids)
    )

    oopgrade.rename_columns(cursor, {
        'crm_case_rule': [('trg_limit_history', 'trg_max_history_log')]
    })

    logger.info('Migration successful!')


def down(cursor, installed_version):
    oopgrade.rename_columns(cursor, {
        'crm_case_rule': [('trg_min_history', 'trg_max_history')]
    })
    oopgrade.rename_columns(cursor, {
        'crm_case_rule': [('trg_max_history_log', 'trg_limit_history')]
    })

migrate = up
