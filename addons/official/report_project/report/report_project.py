# -*- coding: utf-8 -*-

from c2c_webkit_report import webkit_report
from report import report_sxw
from tools import config


class report_webkit_html_proj(report_sxw.rml_parse):
    def __init__(self, cursor, uid, name, context):
        super(report_webkit_html_proj, self).__init__(
            cursor, uid, name, context=context
        )
        self.localcontext.update({
            'cursor': cursor,
            'uid': uid,
            'company_id': self.localcontext['company'].id,
            'addons_path': config['addons_path'],
            'button_pressed': name,
        })

webkit_report.WebKitParser(
    'report.project.report.descripcio',
    'project.project',
    'report_project/report/report_project.mako',
    parser=report_webkit_html_proj
)

webkit_report.WebKitParser(
    'report.project.report.hores',
    'project.project',
    'report_project/report/report_project.mako',
    parser=report_webkit_html_proj
)

webkit_report.WebKitParser(
    'report.project.report.client',
    'project.project',
    'report_project/report/report_project.mako',
    parser=report_webkit_html_proj
)

webkit_report.WebKitParser(
    'report.project.report.dedicades',
    'project.project',
    'report_project/report/report_project.mako',
    parser=report_webkit_html_proj
)

webkit_report.WebKitParser(
    'report.project.report.facturar',
    'project.project',
    'report_project/report/report_project.mako',
    parser=report_webkit_html_proj
)
