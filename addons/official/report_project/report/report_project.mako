<%
    from datetime import datetime
    import markdown as parser
    descripcio = "project.report.descripcio"
    client_version = "project.report.client"
    hores_planejades = "project.report.hores"
    dedicades_planejades = "project.report.dedicades"
    dedicades_facturar = "project.report.facturar"

    def get_date(date=None):
        if date:
            dt = datetime.strptime(date, '%Y-%m-%d')
            res = dt.strftime('%d/%m/%Y')
            return res
        return ''

    def get_date_timedate(timedate=None):
        if timedate:
            dt = datetime.strptime(timedate, '%Y-%m-%d %H:%M:%S')
            res = dt.strftime('%d/%m/%Y')
            return res
        return ''
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
      <style>
        ${css}
      </style>
      <link rel="stylesheet" href="${addons_path}/report_project/report/estils.css"/>
    </head>
    <body>
    %for project in objects:
    <% setLang(project.partner_id.lang) %>
        <div id="capcalera">
            <div id="logo">
              <img id="logo_img" src="data:image/jpeg;base64,${project.manager.company_id.logo}">
            </div>
            <div><h1 id="report_title">${project.name}</h1></div>
        </div>

        <table id="taula_capçalera">
            <colgroup>
                <col width="25%">
                <col>
                <col width="25%">
                <col width="30%">
            </colgroup>
            <tbody>
                <tr>
                    <th>${_("Partner:")}</th>
                    <td colspan="3">${project.partner_id.name}</td>
                </tr>
                %if not button_pressed == descripcio:
                    <tr>
                        <th>${_("Project Name:")}</th>
                        <td colspan="3">${project.name}</td>
                    </tr>
                %endif
                %if button_pressed == descripcio:
                    <tr>
                        <th>${_("Manager:")}</th>
                        <td>${project.manager.name}</td>
                    </tr>
                %endif
                <tr>
                    <th>${_("Date Start:")}</th>
                    <td>${get_date(project.date_start)}</td>
                    <th>${_("Date Stop:")}</th>
                    <td>${get_date(project.date_end)}</td>
                </tr>
                <tr>
                    %if button_pressed in [client_version, dedicades_planejades, dedicades_facturar]:
                        <th>${_("Hours:")}</th>
                        <td>${project.effective_hours}</td>
                    %else:
                        <th>${_("Hours:")}</th>
                        <td>${project.planned_hours}</td>
                    %endif
                    %if button_pressed == descripcio:
                        <th>${_("Hours:")}</th>
                        <td>${project.effective_hours}</td>
                    %elif button_pressed in [dedicades_planejades, dedicades_facturar]:
                        <th>${_("Hours:")}</th>
                        <td>${project.planned_hours}</td>
                    %endif
                </tr>
                %if button_pressed == descripcio:
                    <tr>
                        <th>${_("Members:")}</th>
                        <td>${project.members or ''}</td>
                    </tr>
                %endif
            </tbody>
        </table>
        <table id="taula_dades">
            <thead>
                <tr>
                  % if button_pressed == descripcio:
                      <th style="width:50%;">${_("Tasks")}</th>
                      <th>${_("Hours")}</th>
                      <th>${_("Done")}</th>
                      <th>${_("Deadline")}</th>
                      <th>${_("Responsible")}</th>
                  % else:
                      <th>${_("Order")}</th>
                      <th style="width:80%;">${_("Description")}</th>
                      <th>${_("Hours")}</th>
                  % endif
                </tr>
            </thead>
            <tfoot><tr>
              <td><div style="height:30px"></div></td><td></td><td></td>
              % if button_pressed == descripcio:
                  <td></td><td></td>
              % endif
            </tr></tfoot>
            <tbody>
                %for task in project.tasks:
                <tr>
                    % if button_pressed == descripcio:
                        <td id="data_task">
                            <div id="task_name">${task.name or ''}</div>
                            <div id="task_descr">${parser.markdown(task.description or "")}</div>
                        </td>
                        <td class="times">${task.planned_hours}</td>
                        <td class="times">${task.effective_hours}</td>
                        <td id="deadline_task" class="times">${get_date_timedate(task.date_deadline)}</td>
                        <td style="text-align:center">${task.user_id.name or ''}</td>
                    % else:
                        <td style="text-align: center">${task.sequence or ''}</td>
                        <td id="data_task">
                            <div id="task_name">${task.name or ''}</div>
                            <div id="task_descr">${parser.markdown(task.description or "")}</div>
                        </td>
                        <td  class="times">
                          % if button_pressed in [dedicades_planejades, dedicades_facturar]:
                              <table id="taula_hores">
                                <tr>
                                  <td>${_("Effective")}</td>
                                  % if button_pressed == dedicades_planejades:
                                    <td>${_("Planned")}</td>
                                  % else:
                                    <td>${_("Factured")}</td>
                                  %endif
                                </tr>
                                <tr>
                                  <td class="times">${task.effective_hours}</td>
                                  <td class="times">${task.planned_hours}</td>
                                </tr>
                              </table>
                          % elif button_pressed == client_version:
                              ${task.effective_hours}
                          % else :
                              ${task.planned_hours}
                          %endif
                        </td>
                    % endif
                </tr>
                %endfor
            </tbody>
        </table>
        %if project != objects[-1]:
            <p style="page-break-after:always"></p>
        %endif
    %endfor
    </body>
</html>
