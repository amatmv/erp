# -*- coding: utf-8 -*-

from c2c_webkit_report import webkit_report
from report import report_sxw


webkit_report.WebKitParser(
    'report.sale.order.tofix',
    'sale.order',
    'sale/report/sale_order.mako',
    parser=report_sxw.rml_parse
)
