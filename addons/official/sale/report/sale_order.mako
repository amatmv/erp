## coding=utf-8
<%
    address_o = pool.get('res.partner.address')
    sale_order_o = pool.get('sale.order')

    enterprise = company

    def format_address(res_partner_address):
        dest_addr_street = res_partner_address.street if res_partner_address.street else ""
        destination_zip = res_partner_address.zip

        dest_addr = True
        try:
            destination_poblacion = res_partner_address.id_poblacio.name
            destination_provincia = res_partner_address.state_id.name
            destination_country = res_partner_address.country_id.name
        except:
            dest_addr = ""

        if dest_addr:
            dest_addr = "{}, ".format(destination_zip) if destination_zip else ""
            dest_addr += destination_poblacion if destination_poblacion else ""
            dest_addr += " ({})".format(destination_provincia) if destination_provincia else ""
            dest_addr += "</br>{}".format(destination_country) if destination_country else ""

        dest_addr = '{}</br>{}'.format(dest_addr_street, dest_addr)
        return dest_addr
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
        <style type="text/css">
            ${css}
        </style>
        <link rel="stylesheet" href="${addons_path}/sale/report/sale_order.css"/>
    </head>
    <body>
        %for order in objects:
            <%
                title = _("Sale order - {}").format(order.name)

                if 'company_id' in sale_order_o.fields_get(cursor, uid).keys():
                    enterprise = order.company_id

                company_address_id = enterprise.partner_id.address_get(['default'])['default']
                company_address = address_o.browse(cursor, uid, company_address_id)
            %>
            <!-- HEADER -->
            <table id="header">
                <tr>
                    <td id="company-logo" rowspan="4"><img class="logo" src="data:image/jpeg;base64,${enterprise.logo}"/></td>
                    <td>${enterprise.name}</td>
                </tr>
                <tr>
                    <td>${enterprise.partner_id.vat}</td>
                </tr>
                <tr>
                    <td>${company_address.street}</td>
                </tr>
                <tr>
                    <td>${company_address.zip}, ${company_address.id_poblacio.name} (${company_address.state_id.name})</td>
                </tr>
            </table>
            <!-- END HEADER -->
            <!-- ORDER INFO -->
            <table id="order">
                 <colgroup>
                   <col span="1" width="50%">
                   <col span="1" width="50%">
                </colgroup>
                <thead>
                    <tr>
                        <th id="order-header" colspan="2">${title}</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td id="separator">
                            <table id="sale-info">
                                <colgroup>
                                   <col span="1" width="40%">
                                   <col span="1" width="60%">
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th colspan="2">${_("Sale's information")}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>${_("Buyer")}:</td>
                                        <td>${order.partner_id.name}</td>
                                    </tr>
                                    <tr>
                                        <td>${_("Order number")}:</td>
                                        <td>${order.name}</td>
                                    </tr>
                                    <tr>
                                        <td>${_("Order date")}:</td>
                                        <td>${order.date_order}</td>
                                    </tr>
                                    <tr>
                                        <td rowspan="2">${_("Invoice's address")}:</td>
                                        <td>${format_address(order.partner_invoice_id)}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        <td>
                            <!-- SHIPPING ADDRESS -->
                            <table id="shipping-address">
                                <tr>
                                    <th>${_("Shipping address")}</th>
                                </tr>
                                <tr>
                                    <td>${format_address(order.partner_shipping_id)}</td>
                                </tr>
                            </table>
                            <!-- END SHIPPING ADDRESS -->
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <!-- ORDER LINES -->
                            <table id="order-lines">
                                <colgroup>
                                   <col span="1" style="width: 64%;">
                                   <col span="1" style="width: 10%;">
                                   <col span="1" style="width: 13%;">
                                   <col span="1" style="width: 13%;">
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th class="subtitles">${_("Description")}</th>
                                        <th class="right subtitles">${_("Quantity")}</th>
                                        <th class="right subtitles">${_("Price")} (€)</th>
                                        <th class="right subtitles">${_("Total")} (€)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    %for line in order.order_line:
                                        <%
                                            price_unit = formatLang(line.price_unit, digits=2)
                                            subtotal = formatLang(line.price_subtotal, digits=2)
                                        %>
                                        <tr class="row-values">
                                            <td id="descr">${line.name}</td>
                                            <td class="qty">${line.product_uos_qty}</td>
                                            <td class="qty">${price_unit}</td>
                                            <td class="qty bold">${subtotal}</td>
                                        </tr>
                                    %endfor
                                </tbody>
                            </table>
                            <!-- END ORDER LINES -->
                        </td>
                    </tr>
                    %if order.note:
                        <tr><td id="notes-header" class="bold" colspan="2">${_("Notes")}</td></tr>
                        <tr><td id="notes-body" colspan="2">${order.note.replace('\n','<br/>')}</td></tr>
                    %endif
                    <tr>
                        <td colspan="2">
                            <%
                                untaxed = formatLang(order.amount_untaxed, digits=2)
                                taxed = formatLang(order.amount_tax, digits=2)
                                total = formatLang(order.amount_total, digits=2)
                            %>
                            <table id="summary">
                            %if untaxed!=total:
                                <tr>
                                    <th>${_("Subtotal")}</th>
                                    <td>${untaxed} €</td>
                                </tr>
                                <tr>
                                    <th id="impuestos">${_("IVA (21%)")}</th>
                                    <td>${taxed} €</td>
                                </tr>
                            %endif
                                <tr>
                                    <th>${_("Total")}</th>
                                    <td class="bold">${total} €</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
            %if order != objects[-1]:
                <p style="page-break-after:always"></p>
            %endif
        %endfor
    </body>
</html>
