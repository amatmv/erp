## -*- coding: utf-8 -*-
<%
    import babel
    from babel.numbers import format_currency
    from datetime import datetime, timedelta
    from bankbarcode.cuaderno57 import Recibo507
    from account_invoice_base.report.utils import localize_period
    from giscedata_facturacio_impagat.giscedata_facturacio import find_B1
    from workalendar.europe.spain import Spain
%>

<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
        <title>${_(u"HR expenses")}</title>
        <link rel="stylesheet" href="${addons_path}/hr_expense/report/expense.css"/>
    </head>
    <body>
        %for expense in objects:
            <%
               phone = company.partner_id.address[0].phone or company.partner_id.address[0].mobile or ''
               email = company.partner_id.address[0].email or ''
               web = company.partner_id.website or ''
               company_address = company.partner_id
            %>
            <table class="header">
                <colgroup>
                    <col style="width: 25%"/>
                    <col style="width: 40%"/>
                    <col style="width: 30%"/>
                </colgroup>
                <tr>
                    <td>
                        <img alt="logo" class="logo" src="data:image/jpeg;base64,${company.logo}" height="75px"/>
                    </td>
                    <td>
                        <h1>
                            ${company.name} <br/>
                        </h1>
                        <div id="subtitle">${company.rml_footer2}</div>
                    </td>
                    <td id="partner_address">
                        ${company_address.address[0].street or ''} ${company_address.address[0].street2 or ''} <br/>
                        ${company_address.address[0].zip or ''} ${company_address.address[0].id_poblacio.name or ''} (${company_address.address[0].state_id.name or ''})<br/>
                        Telf: ${phone} <br/>
                        %if web:
                            Web: <a href="${web}" style="color: inherit; text-decoration: none;">${web}</a> <br/>
                        %endif
                        %if email:
                            E-mail: <a href="mailto:${email}" style="color: inherit; text-decoration: none;">${email}</a>
                        %endif
                    </td>
                </tr>
            </table>
            <h2>${_(u"EXPENSE SHEET")}</h2>
            <div class="expense_data">
                <table>
                    <colgroup>
                        <col style="width: 55%"/>
                        <col style="width: 45%"/>
                    </colgroup>
                    <tr>
                        <td>
                            <table class="address">
                                <tr>
                                    <td>
                                        <u><b>${_(u"Expense Data")}</b></u>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>${_(u"Name:")}</b> ${expense.name}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>${_(u"Employee:")}</b> ${expense.employee_id.name}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>${_(u"Date:")}</b> ${expense.date}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>${_(u"Amount:")}</b> ${expense.amount} €
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="minheight100">
                <table class="line_detail">
                    <tr>
                        <th>
                            ${_(u'Date')}
                        </th>
                        <th>
                            ${_(u'Short Description')}
                        </th>
                        <th>
                            ${_(u'Reference')}
                        </th>
                        <th>
                            ${_(u'Analytic account')}
                        </th>
                        <th>
                            ${_(u'Unit Price')}
                        </th>
                        <th>
                            ${_(u'Quantities')}
                        </th>
                        <th>
                            ${_(u'Total')}
                        </th>
                    </tr>
                    %for line in expense.line_ids:
                        <tr>
                            <td>
                                ${line.date_value}
                            </td>
                            <td style="text-align: left">
                                ${line.name}
                            </td>
                            <td>
                                ${line.ref or ""}
                            </td>
                            <td>
                                ${line.analytic_account}
                            </td>
                            <td>
                                ${line.unit_amount}
                            </td>
                            <td style="text-align: right">
                                ${line.unit_quantity}
                            </td>
                            <td>
                                ${line.total_amount}
                            </td>
                        </tr>
                    %endfor
                </table>
            </div>
            % if expense != objects[-1]:
                <p style="page-break-after:always"></p>
            % endif
        %endfor
    </body>
</html>
