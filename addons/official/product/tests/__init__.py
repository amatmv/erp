# coding=utf-8
from unittest import skip
from datetime import datetime

from destral import testing
from destral.transaction import Transaction
from osv.osv import except_osv


skip_not_implemented = skip('not implemented')


class TestDynamicBase(testing.OOTestCase):

    def test_exists_a_dynamic_base_in_selection(self):
        pl_item_obj = self.openerp.pool.get('product.pricelist.item')

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            context = txn.context

            selection = pl_item_obj._columns['base'].selection(
                pl_item_obj, cursor, uid, context
            )

        self.assertIn((-4, 'Dynamic from context'), selection)

    def test_base_is_getted_from_context(self):
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            context = txn.context

            imd = self.openerp.pool.get('ir.model.data')
            pl_id = imd.get_object_reference(cursor, uid, 'product', 'pl0')[1]

            product_id = imd.get_object_reference(
                cursor, uid, 'product', 'product_product_0'
            )[1]

            ctx = context.copy()
            ctx['pricelist_base_price'] = 5

            pl_obj = self.openerp.pool.get('product.pricelist')
            price = pl_obj.price_get(
                cursor, uid, [pl_id], product_id, 0, context=ctx
            )[pl_id]

            self.assertEqual(price, 5)

    def test_raise_when_is_dynamic_and_is_not_in_context(self):
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            context = txn.context

            imd = self.openerp.pool.get('ir.model.data')
            pl_id = imd.get_object_reference(cursor, uid, 'product', 'pl0')[1]

            product_id = imd.get_object_reference(
                cursor, uid, 'product', 'product_product_0'
            )[1]

            ctx = context.copy()

            pl_obj = self.openerp.pool.get('product.pricelist')

            def price_get():
                price = pl_obj.price_get(
                    cursor, uid, [pl_id], product_id, 0, context=ctx
                )[pl_id]
                return price

            self.assertRaises(except_osv, price_get)

    def test_operations_in_dynamic_price(self):
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            context = txn.context

            imd = self.openerp.pool.get('ir.model.data')
            pl_id = imd.get_object_reference(cursor, uid, 'product', 'pl0')[1]
            pl_item_id = imd.get_object_reference(
                cursor, uid, 'product', 'pl_item_dynamic'
            )[1]

            product_id = imd.get_object_reference(
                cursor, uid, 'product', 'product_product_0'
            )[1]

            ctx = context.copy()
            ctx['pricelist_base_price'] = 5

            pl_obj = self.openerp.pool.get('product.pricelist')
            pl_item_obj = self.openerp.pool.get('product.pricelist.item')

            pl_item_obj.write(cursor, uid, [pl_item_id], {
                'price_discount': 0.25,
                'price_surcharge': 3
            })

            price = pl_obj.price_get(
                cursor, uid, [pl_id], product_id, 0, context=ctx
            )[pl_id]

            new_price = (5 * (1 + 0.25) + 3)

            self.assertEqual(price, new_price)

    def test_inheritance_on_dynamic_price(self):
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            context = txn.context

            imd = self.openerp.pool.get('ir.model.data')
            pl_id = imd.get_object_reference(
                cursor, uid, 'product', 'pl1_inherit_pl0'
            )[1]
            pl_item_id = imd.get_object_reference(
                cursor, uid, 'product', 'pl_item_dynamic'
            )[1]

            pl_item_obj = self.openerp.pool.get('product.pricelist.item')

            pl_item_obj.write(cursor, uid, [pl_item_id], {
                'price_discount': 0.25,
                'price_surcharge': 3
            })

            product_id = imd.get_object_reference(
                cursor, uid, 'product', 'product_product_0'
            )[1]

            ctx = context.copy()
            ctx['date'] = datetime.now().date().strftime('%Y-%m-%d')
            ctx['pricelist_base_price'] = 5

            pl_obj = self.openerp.pool.get('product.pricelist')

            price = pl_obj.price_get(
                cursor, uid, [pl_id], product_id, 0, context=ctx
            )[pl_id]

            new_price = (5 * (1 + 0.25) + 3)
            new_price = new_price * (1 - 0.25) - 3

            self.assertEqual(price, new_price)
