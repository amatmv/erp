<%
    from datetime import datetime, date, timedelta
    from babel.dates import format_date
    from electrical_calendar import REECalendar, OMIECalendar
    import calendar
    usr_obj = pool.get("res.users")
    work_obj = pool.get("project.task.work")
    if context['lang'] != company.partner_id.lang:
        setLang(company.partner_id.lang)

    def getDatetime(datestring):
        """
        Parses the date using default format
        """
        formatt = '%Y-%m-%d %H:%M:%S'
        d = datetime.strptime(datestring, formatt)
        return d

    def getWorkOn(date, userworks):
        workdone = 0.0
        for work in userworks:
            work_date = getDatetime(work['date'])
            if work_date.year == date.year and \
                    work_date.month == date.month and \
                    work_date.day == date.day:
                workdone += work['hours']
        return round(workdone, 2)

    def getDoM(date):
        """
        Get all days of the month specified above
        """
        dom = [datetime(date.year, date.month, d)
          for d in
          range(1,
            calendar.monthrange(date.year, date.month)[1] + 1
          )
        ]
        return dom

    def getMonth(date):
        from babel.dates import format_date
        data = format_date(date, "MMMM - yyyy", locale=context['lang'])
        return data

    def getMonths(d0, d1):
        months = []
        diff = (d1.year-d0.year)*12 + d1.month - d0.month
        for i in range(0, diff+1):
            month = d0.month + i
            year = d0.year
            while month > 12:
              year += 1
              month -= 12
            m = datetime(year=year, month=month, day=1)
            months.append(m)
        return months

    def getUsers(pool, use_company=False):
        usr_obj = pool.get("res.users")
        if use_company:
            u_ids = usr_obj.search(cursor, uid, [
                ('company_id', '=', context['company'].id)
            ])
        else:
            u_ids = usr_obj.search(cursor, uid, [])
        # Remove "Admin" from user list
        if 1 in u_ids:
            u_ids.remove(1)
        users = usr_obj.read(cursor, uid, u_ids, ['name'])
        users.sort(key=(lambda u: u['name']))
        return [u['id'] for u in users]


    if not objects or ids:
        if date_start:
            mindate = getDatetime(date_start)
        else:
            mindate = min(i['date'] for i in work_obj.read(
                cursor, uid, ids, ['date']
            ))
            mindate = getDatetime(mindate)
        if date_end:
            maxdate = getDatetime(date_end)
        else:
            maxdate = max(i['date'] for i in work_obj.read(
                cursor, uid, ids, ['date']
            ))
            maxdate = getDatetime(maxdate)
    else:
        mindate = getDatetime(min(i.date for i in objects))
        maxdate = getDatetime(max(i.date for i in objects))

    ree_cal = REECalendar()
    omie_cal = OMIECalendar()
    months = getMonths(mindate, maxdate)
    users = getUsers(pool, company_user)
    userworks = {}
    logo = context['logo']
    for u_id in users:
        userworks[u_id] = {}
        if ids:
            works = work_obj.search(
                cursor, uid,[
                    ('user_id', '=', u_id),
                    ('id', 'in', ids),
                ]
            )
        else:
            works = work_obj.search(
                cursor, uid,[
                    ('user_id', '=', u_id),
                    ('date', '>=', mindate.strftime('%Y-%m-%d %H:%M:%S')),
                    ('date', '<=', maxdate.strftime('%Y-%m-%d 23:59:59'))
                ]
            )
        if works:
            workdones = work_obj.read(
                cursor, uid, works, ['date', 'hours']
            )
            userworks[u_id] = workdones
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
        <style type="text/css">
        thead {
            background-color: #bdccff;
            border-bottom: 2px solid #444;
        }
        thead tr:nth-child(odd){
            background-color: #bdccff;
        }
        table {
            width: 100%;
            border-collapse: collapse;
            page-break-inside:auto;
        }
        tr:nth-child(odd){
            background-color: #ddd;
        }
        .inside tr:nth-child(odd){
            background-color: transparent;
        }
        td, th{
            align-content: center;
            align-self: center;
            align-items: center;
            padding: 5px;
        }
        tr {
            page-break-inside:avoid;
            page-break-after:auto;
        }
        .totals{
            background-color: #bdccff;
            border-top: 2px solid black;
        }
        .lb{
            border-left: 2px solid black;
            background-color: #bdccff;
        }
        .rb{
            border-right: 2px solid black;
        }
        .work_of_day {
            border-left: 1px solid grey;
        }
        .month{
            page-break-inside:avoid;
            page-break-after:auto;
        }
        .wkend{
            background-color: #b3b3b3;
        }
        #logo {         /*Div Logo*/
            position: absolute;
            margin: 5px;
            top: 0;
            right: 10px;
            vertical-align: middle;
        }

        #logo_img{      /*Imatge Logo*/
            max-height: 75px;
            max-width: 100px;
        }
        #capcalera {    /*Div Capçalera*/
            width: calc(100%- 100px);
            margin-left: 100px;
            position: relative;
            text-align: center;
            align-content: center;
            vertical-align: middle;
            font-weight: bold;
            height: 80px;
        }
        </style>
    </head>

    <body>
        <div id="capcalera">
            <div id="logo">
                  <img id="logo_img" src="data:image/jpeg;base64,${logo}">
            </div>
            <div><h1>${_('Monthly Works Done Briefing')}</h1></div>
        </div>
        %for month in months:
          <%
            days = getDoM(month)
            work_days = len(days)
            works = {}
          %>
          <div class="month">
            <br/>
            <span>
                <strong>${_('Month')}</strong>
            </span>
            <span>
                <strong>${getMonth(month)}</strong>
            </span>
            <br/>
            <br/>
            <table>
              <thead>
                <tr class="line">
                  <th class="rb" rowspan="2">${_('Done By')}</th>
                  <th colspan=${len(days)}>${_('Day')}</th>
                  <th class="lb" rowspan="2">${_('Totals')}</th>
                </tr>
                <tr>
                  %for day in days:
                    <%
                        if not (ree_cal.is_working_day(day) and omie_cal.is_working_day(day)):
                            work_days = work_days - 1
                        elif day.isoweekday() == 7 or day.isoweekday() == 6:
                            work_days = work_days - 1
                    %>
                    <th>${day.day}</th>
                  %endfor
                </tr>
              </thead>
              <tbody>
                %for user_id in users:
                  <%
                    user = usr_obj.browse(cursor, uid, user_id)
                    workdone = 0
                  %>
                  <tr class="inside">
                    <td class="rb">${user.name}</td>
                    %for day in days:
                      <%
                        work_of_day = getWorkOn(day, userworks[user_id])
                        workdone += work_of_day
                        works[user_id]= workdone
                        if work_of_day == 0:
                          work_of_day = '-'
                      %>
                      %if day.isoweekday() == 7:
                        <td class="rb wkend work_of_day">${work_of_day}</td>
                      %elif (ree_cal.is_working_day(day) and omie_cal.is_working_day(day)):
                        <td class="work_of_day">${work_of_day}</td>
                      %else:
                        <td class="wkend work_of_day">${work_of_day}</td>
                      %endif
                    %endfor
                    <td class="totals lb">${workdone}</td>
                  </tr>
                %endfor
                <tr class="totals, inside">
                  <td class="totals rb">${_('Totals')}</td>
                  <td class="totals" colspan=${len(days)-6}></td>
                  <td class="totals" colspan=2>4h:${4*work_days}</td>
                  <td class="totals" colspan=2>6h:${6*work_days}</td>
                  <td class="totals" colspan=2>8h:${8*work_days}</td>
                  <td class="totals lb">${sum([works[u] for u in users])}</td>
                </tr>
              </tbody>
            </table>
          </div>
        %endfor
    </body>
</html>
