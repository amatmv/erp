<%
    def getFirstDate(items):
        return max(i.date for i in items)


    def getLastDate(items):
        return min(i.date for i in items)

    logo = objects[0].project_id.manager.company_id.logo or False
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
        <style type="text/css">
            ${css}
            #logo {         /*Div Logo*/
                position: absolute;
                margin: 5px;
                top: 0;
                right: 10px;
                vertical-align: middle;
            }

            #logo_img{      /*Imatge Logo*/
                max-height: 75px;
                max-width: 100px;
            }

            #capcalera {    /*Div Capçalera*/
                width: calc(100%- 100px);
                margin-left: 100px;
                position: relative;
                text-align: center;
                align-content: center;
                vertical-align: middle;
                font-weight: bold;
                height: 80px;
            }

        </style>
        <link rel="stylesheet" href="${addons_path}/project/report/estils_workdones.css"/>
    </head>

    <body>
        <div id="capcalera">
            <div id="logo">
                  <img id="logo_img" src="data:image/jpeg;base64,${logo}">
            </div>
            <div><h1>${_('Works Done Briefing')}</h1></div>
        </div>
        <div>
            <span>
                <strong>${_('Start Date:')}</strong>
            </span>
            <span>
                ${getFirstDate(objects)}
            </span>
            <span>
                <strong>${_('End Date:')}</strong>
            </span>
            <span>
                ${getLastDate(objects)}
            </span>
        </div>
        <table>
            <thead>
                <tr>
                    <th>${_('Project')}</th>
                    <th>${_('Task')}</th>
                    <th>${_('Work brief')}</th>
                    <th>${_('Date Done')}</th>
                    <th>${_('Time Spent')}</th>
                    <th>${_('Done By')}</th>
                </tr>
            </thead>
            <tbody>
                <% total_hores = 0 %>
                %for workdone in objects:
                    <tr>
                        <td>${workdone.project_id.name}</td>
                        <td>${workdone.task_id.name}</td>
                        <td>${workdone.name}</td>
                        <td>${workdone.date}</td>
                        <td>${round(workdone.hours, 2)}</td>
                        <td>${workdone.user_id.name}</td>
                    </tr>
                    <% total_hores += round(workdone.hours, 2) %>
                %endfor
                <tr>
                    <td colspan="4"><b>${_('Total hours')}</b></td>
                    <td colspan="2"><b>${total_hores}</b></td>
                </tr>
            </tbody>
        </table>
    </body>
</html>
