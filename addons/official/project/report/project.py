# -*- coding: utf-8 -*-
from c2c_webkit_report import webkit_report
from report import report_sxw
from tools import config


class report_webkit_html(report_sxw.rml_parse):
    def __init__(self, cursor, uid, name, context):
        super(report_webkit_html, self).__init__(
            cursor, uid, name, context=context
        )
        ids = context.get('wd_ids', [])
        dinici = context.get('date_start', False)
        dfinal = context.get('date_end', False)
        cuser = context.get('company_user', False)
        self.localcontext.update({
            'cursor': cursor,
            'uid': uid,
            'ids': ids,
            'date_start': dinici,
            'date_end': dfinal,
            'company_users': cuser,
            'addons_path': config['addons_path'],
            'pool': self.pool
        })

webkit_report.WebKitParser(
    'report.workdones',
    'project.task.work',
    'project/report/workdones_report.mako',
    parser=report_webkit_html
)
webkit_report.WebKitParser(
    'report.monthly.workdones',
    'project.task.work',
    'project/report/workdones_monthly_report.mako',
    parser=report_webkit_html
)
