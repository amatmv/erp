# -*- coding: utf-8 -*-

from datetime import datetime
from tools.translate import _
from osv import osv, fields
import wizard

class WizardWorkdones(osv.osv_memory):
    """Wizard to filter data for the report 'monthly workdones'
    fields:
    *   date_start: Starting date for the filter.
                Today at 00:00:00 by default
                If not set, min date from workdones will be used
    *   date_end:   Ending date for the filter.
                Today at 23:59:59 by default
                If not set, max date from workdones will be used
    *   company_user:   Boolean to filter using company users or not.
                Company is picked from self user
                False by default
    """
    _name = "project.task.work.wizard.workdones"

    _columns = {
        'date_start': fields.datetime(string=_("Start Date")),
        'date_end': fields.datetime(string=_("End Date")),
        'company_user': fields.boolean(string=_(
            "Use company users"
        ))
    }

    _defaults = {
        'date_start': datetime.now().strftime('%Y-%m-%d 00:00:00'),
        'date_end': datetime.now().strftime('%Y-%m-%d 23:59:59'),
        'company_user': False
    }

    def getWorkDones(self, cursor, uid, context):
        dinici = self.datas[self.datas.keys()[-1]]['date_start']
        dfinal = self.datas[self.datas.keys()[-1]]['date_end']
        workdones_obj = self.pool.get('project.task.work')
        if dinici and dfinal:
            workdones_ids = workdones_obj.search(cursor, uid, [
                ('date', '>=', dinici),
                ('date', '<=', dfinal)
            ])
        elif dinici:
            workdones_ids = workdones_obj.search(cursor, uid, [
                ('date', '>=', dinici)
            ])
        elif dfinal:
            workdones_ids = workdones_obj.search(cursor, uid, [
                ('date', '<=', dfinal)
            ])
        else:
            workdones_ids = workdones_obj.search(cursor, uid, [])
        return workdones_ids

    def print_report(self, cursor, uid, ids, context):
        if not context:
            context = {}
        data_ids = self.getWorkDones(cursor, uid, context)
        cuser = self.datas[self.datas.keys()[-1]]['company_user']
        return {
            'type': 'ir.actions.report.xml',
            'model': 'project.task.work',
            'report_name': 'monthly.workdones',
            'report_webkit': "'project/report/workdones_report.mako'",
            'webkit_header': 'project_report_sense_fons',
            'groups_id': data_ids,
            'multi': '0',
            'auto': '0',
            'header': '0',
            'report_rml': 'False',
            'context': {
                'wd_ids': data_ids,
                'date_start': self.datas[self.datas.keys()[-1]]['date_start'],
                'date_end': self.datas[self.datas.keys()[-1]]['date_end'],
                'company_user': cuser
            }
        }

WizardWorkdones()
