# -*- coding: utf-8 -*-
from tools.translate import _
from osv import osv, fields


class WizardRegularizeTime(osv.osv_memory):
    _name = "project.task.wizard.regularize.time"

    def _get_default_output(self, cursor, uid, context=None):
        selected_tasks = len(context.get('active_ids', []))
        return _('Input hours to update {} tasks').format(selected_tasks)

    def update_task(self, cursor, uid, task_ids, hours):
        """
        Class method to update 'planned_hours' with the specified hours
        :param cursor:      OpenERP Cursor
        :param uid:         User ID
        :param task_ids:    Tasks IDs to update
        :type task_ids:     [int]
        :param hours:       Hours to apply on tasks.planned_hours
        :type hours:        float
        :return:            Returns updated tasks within task_ids
        :rtype:             int
        """
        task_obj = self.pool.get('project.task')
        updated = 0
        for task_id in task_ids:
            try:
                task_obj.write(cursor, uid, task_id, {
                    'planned_hours': hours
                })
                updated += 1
            except:
                pass
        return updated

    def update_with_value(self, cursor, uid, ids, context):
        """
        Wizard 'update' button with value specified controller
        :param cursor:  OpenERP Cursor
        :param uid:     User ID
        :param ids:     Wizard ids
        :param context: Wizard context
        :return:
        """
        wiz_data = self.read(cursor, uid, ids, ['input_hours'])[0]
        forced_hours = wiz_data.get('input_hours', False)
        if not forced_hours:
            raise osv.except_osv(
                _('ERROR WITH MANUAL UPDATE'),
                _("You're trying to write hours but the field is empty!")
            )
        updated = self.update_task(
            cursor, uid,
            task_ids=context.get('active_ids', []),
            hours=forced_hours
        )
        self.write(cursor, uid, ids, {
            'output': _('Updated {} tasks with {} as planned hours').format(
                updated, forced_hours)
        })

    def update_with_data(self, cursor, uid, ids, context):
        """
        Wizard 'update' button with effective hours
        :param cursor:  OpenERP Cursor
        :param uid:     User ID
        :param ids:     Wizard ids
        :param context: Wizard context
        :return:
        """
        task_obj = self.pool.get('project.task')
        updated = 0
        for task_id in context.get('active_ids', []):
            task_data = task_obj.read(
                cursor, uid, task_id, ['effective_hours'])
            forced_hours = task_data.get('effective_hours', 0)
            updated += self.update_task(
                cursor, uid,
                task_ids=[task_id],
                hours=forced_hours
            )
        self.write(cursor, uid, ids, {
            'output': _(
                'Updated {} tasks with effective hours as planned hours'
            ).format(updated)
        })

    _columns = {
        'output': fields.text('Output info text', readonly=True),
        'input_hours': fields.integer('New planned hours')
    }

    _defaults = {
        'output': _get_default_output
    }

WizardRegularizeTime()
