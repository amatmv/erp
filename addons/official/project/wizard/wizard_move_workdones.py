# -*- coding: utf-8 -*-

from datetime import datetime
from tools.translate import _
from osv import osv, fields
import wizard


class WizardMoveWorkdones(osv.osv_memory):

    _name = 'wizard.move.workdones'

    def move_workdones_from_task_to_task(self, cursor, uid, ids, context=None):
        workdones_obj = self.pool.get('project.task.work')
        selected_wokdones = context.get('active_ids', False)
        if selected_wokdones:
            to_task_id = self.read(cursor, uid, ids, ['tasks'],
                                   context=context)[0]['tasks']
            if to_task_id:
                for work_id in selected_wokdones:
                    workdones_obj.write(
                        cursor, uid, work_id,
                        {'task_id': to_task_id},
                        context=context
                    )
                return {}
            else:
                raise osv.except_osv(_('Error'), _('No task selected!'))
        else:
            raise osv.except_osv(_('Error'), _('No workdones selected!'))

    def generate_summary(self, cursor, uid, context=None):
        selected_wokdones = context.get('active_ids', False)
        info = _('{0} workdones will be moved to the task:').format(
            len(selected_wokdones)
        )
        return info

    def back_to_init(self, cursor, uid, ids, context=None):
        self.write(cursor, uid, ids,
                   {'state': 'init', 'tasks': False, 'info': ''},
                   context=context)

    _columns = {
        'tasks': fields.many2one('project.task', 'Task', required=True),
        'info': fields.text('summary', readonly=True),
    }

    _defaults = {
        'info': generate_summary
    }

WizardMoveWorkdones()
