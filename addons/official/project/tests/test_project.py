# coding=utf-8
from destral import testing


class TestProject(testing.OOTestCaseWithCursor):

    def test_star_a_project_updates_project_user_relation(self):

        project_obj = self.openerp.pool.get('project.project')
        project_user_obj = self.openerp.pool.get('project.user')

        cursor = self.cursor
        uid = self.uid

        project_id = project_obj.create(cursor, uid, {
            'name': 'Test project'
        })

        project_obj.write(cursor, uid, [project_id], {
            'starred': True
        })

        result = project_user_obj.search(cursor, uid, [
            ('project_id', '=', project_id),
            ('user_id', '=', uid),
            ('starred', '=', True)
        ])
        self.assertEqual(len(result), 1)

        project = project_obj.read(cursor, uid, project_id, ['starred'])
        self.assertTrue(project['starred'])

        project_obj.write(cursor, uid, [project_id], {
            'starred': False
        })
        project = project_obj.read(cursor, uid, project_id, ['starred'])
        self.assertFalse(project['starred'])

    def test_search_for_a_starred_project(self):
        project_obj = self.openerp.pool.get('project.project')

        cursor = self.cursor
        uid = self.uid

        project_id = project_obj.create(cursor, uid, {
            'name': 'Test project'
        })

        values = project_obj.search(cursor, uid, [('starred', '=', True)])
        self.assertEqual(len(values), 0)

        project_obj.write(cursor, uid, [project_id], {
            'starred': True
        })

        values = project_obj.search(cursor, uid, [('starred', '=', True)])
        self.assertListEqual(values, [project_id])


    def test_star_project_button(self):
        project_obj = self.openerp.pool.get('project.project')
        project_user_obj = self.openerp.pool.get('project.user')

        cursor = self.cursor
        uid = self.uid

        project_id = project_obj.create(cursor, uid, {
            'name': 'Test project'
        })

        project = project_obj.browse(cursor, uid, project_id)
        project.star()

        result = project_user_obj.search(cursor, uid, [
            ('project_id', '=', project_id),
            ('user_id', '=', uid),
            ('starred', '=', True)
        ])
        self.assertEqual(len(result), 1)

        p = project_obj.read(cursor, uid, project_id, ['starred'])
        self.assertTrue(p['starred'])

        project.unstar()
        p = project_obj.read(cursor, uid, project_id, ['starred'])
        self.assertFalse(p['starred'])
