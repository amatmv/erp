# coding=utf-8

import logging


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')

    logger.info('Actualizando workflows de las ordenes de pago/cobro:')

    # Modifica la actvivity done de las remesas para que no sea final de
    # workflow

    inf_workflow = """
       UPDATE wkf_activity 
       SET flow_stop = false
       FROM wkf
       WHERE wkf.id = wkf_activity.wkf_id 
       and wkf_activity.name = 'done' 
       and wkf.osv = 'payment.order' 
       and wkf_activity.flow_stop;
       """

    cursor.execute(inf_workflow)

    # Reactiva los workflows de les remeses a done

    reactivate_wkf = """
       UPDATE wkf_instance 
       SET state = 'active' 
       WHERE res_type = 'payment.order' and state = 'complete'
       """
    cursor.execute(reactivate_wkf)

    logger.info('Workflows actualizados!!')


def down(cursor, installed_version):
    pass

migrate = up
