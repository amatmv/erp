# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction

from osv.orm import ValidateException
from osv.osv import except_osv

import logging


class TestPickings(testing.OOTestCase):
    def setUp(self):
        self.logger = logging.getLogger(__name__)
        self.pool = self.openerp.pool
        self.myobj = self.pool.get('stock.picking')

    def test_delete_moves_on_delete(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            move_obj = self.pool.get('stock.move')
            irmd_obj = self.pool.get('ir.model.data')
            product_obj = self.pool.get('product.product')

            location_01_id = irmd_obj.get_object_reference(
                cursor, uid, 'stock', 'stock_location_13'
            )[1]
            location_02_id = irmd_obj.get_object_reference(
                cursor, uid, 'stock', 'stock_location_14'
            )[1]
            product_id = irmd_obj.get_object_reference(
                cursor, uid, 'product', 'product_product_pc1'
            )[1]
            uom_id = product_obj.read(
                cursor, uid, product_id, ['uom_id']
            )['uom_id'][0]

            # Create temp picking
            picking_id = self.myobj.create(cursor, uid, {
                'name': 'TestPicking'
            })

            # Create move for picking

            move_id = move_obj.create(cursor, uid, {
                'name': 'TestMove',
                'picking_id': picking_id,
                'location_id': location_01_id,
                'location_dest_id': location_02_id,
                'product_id': product_id,
                'product_uom': uom_id,
            })

            # Case 1:
            #   Deleting a move that is done should raise an exception

            move_obj.write(cursor, uid, move_id, {'state': 'done'})

            with self.assertRaises(except_osv):
                self.myobj.unlink(cursor, uid, [picking_id])

            # Case 2:
            #   Deleting a picking should delete the move if it's on draft state
            move_obj.write(cursor, uid, move_id, {'state': 'draft'})

            self.myobj.unlink(cursor, uid, [picking_id])

            self.assertTrue(
                move_obj.search_count(cursor, uid, [('id', '=', move_id)]) == 0,
                msg='Deleting a picking should delete its moves'
            )
