# -*- encoding: utf-8 -*-
from osv import osv, fields
from lxml import etree
from tools.translate import _


class WizardReturnPicking(osv.osv_memory):

    _name = "wizard.return.picking"

    _columns = {
        'invoice_state': fields.selection([
            ("invoiced", "Invoiced"),
            ("2binvoiced", "To Be Invoiced"),
            ("none", "Not from Packing")
        ], "Invoice Status", required=True
        )
    }

    def fields_view_get(self, cursor, uid, view_id=None, view_type='form', context=None, toolbar=False):
        """
        Creates the dinamic view of the wizard: adds the move quantity fields
        to the wizard in function of the active picking.
        :param cursor: DB cursor.
        :type cursor: sq_db.Cursor
        :param uid: <res.users> ids.
        :type uid: long
        :param view_id: <ir.ui.view> id.
        :type view_id: long
        :param view_type: view type (form, tree, graph, etc).
        :type view_type: str
        :param context: OpenERP context.
        :type context: dict
        :param toolbar: ??
        :return:
        :rtype: dict
        """
        res = super(WizardReturnPicking, self).fields_view_get(
            cursor, uid, view_id, view_type, context=context, toolbar=toolbar
        )

        picking_id = False
        if context is not None:
            picking_id = context.get('active_id', False)

        if picking_id:
            move_o = self.pool.get('stock.move')
            dmn = [('picking_id', '=', picking_id)]
            move_ids = move_o.search(cursor, uid, dmn, context=context)
            move_f = ['name', 'product_qty']
            move_vs = move_o.read(cursor, uid, move_ids, move_f, context=context)

            xml = etree.fromstring(res['arch'])

            for move_v in move_vs:
                move_id = move_v['id']
                field_name = 'move_{}'.format(move_id)

                # adds a new field to the columns of the wizard
                self._columns[field_name] = fields.float(field_name)

                # adds a new field to the view of the wizard
                res['fields'][field_name] = {
                    'string': move_v['name'],
                    'type': 'float',
                    'required': True,
                }
                field_xml = '<field name="{}" colspan="8"/>'.format(field_name)

                # adds the new field at the beginning
                xml.insert(0, etree.fromstring(field_xml))

            res['arch'] = etree.tostring(xml)

        return res

    def action_return_picking(self, cursor, uid, ids, context=None):
        """
        Opens a new tab with the new return picking.
        :param cursor: DB cursor.
        :type cursor: sq_db.Cursor
        :param uid: <res.users> ids.
        :type uid: long
        :param ids: <wizard.return.picking> ids
        :type ids: list[long]
        :param context: OpenERP context.
        :type context: dict
        :return: Dictionary with the values of an action in order to open a new
        tab.
        :rtype: dict[str, str]
        """
        picking_o = self.pool.get('stock.picking')
        picking_id = context['active_id']

        wiz = self.browse(cursor, uid, ids, context=context)[0]

        to_return = {}

        for column in self._columns:
            if 'move' in column:
                move_id = column[5:]
                move_id = int(move_id)
                to_return[move_id] = getattr(wiz, column)

        return_picking_id = picking_o.return_picking(
            cursor, uid, picking_id, wiz.invoice_state, to_return,
            context=context
        )

        return {
            'domain': "[('id','=', {})]".format(return_picking_id),
            'name': _('Return picking'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'stock.picking',
            'type': 'ir.actions.act_window'
        }


WizardReturnPicking()
