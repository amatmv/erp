# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    $Id$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv, fields
from tools.translate import _


class WizardInvoiceFromPicking(osv.osv_memory):
    _name = 'wizard.invoice.from.picking'
    _columns = {
        'journal_id': fields.many2one(
            'account.journal', 'Destination Journal', required=True
        ),
        'type': fields.selection(
            [
                ('out_invoice', 'Customer Invoice'),
                ('in_invoice', 'Supplier Invoice'),
                ('out_refund', 'Customer Refund'),
                ('in_refund', 'Supplier Refund'),
            ],
            string='Type',
            required=True
        ),
        'group': fields.boolean('Group by partner'),
    }

    def _get_type(self, cr, uid, context=None):
        """
        Obtains the default invoice type to be created.
        :param cr: DB cursor.
        :type cr: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param context: OpenERP context.
        :type context: dict
        :return: Invoice type (out_refund, etc)
        :rtype: str
        """
        picking_o = self.pool.get('stock.picking')
        src_usage = dest_usage = None

        picking_id = context.get('active_id', False)

        res = False

        if picking_id:

            pick = picking_o.browse(cr, uid, picking_id, context=context)
            if pick.invoice_state == 'invoiced':
                raise osv.except_osv(
                    _(u'UserError'),
                    _(u'Invoice is already created.')
                )
            if pick.invoice_state == 'none':
                raise osv.except_osv(
                    _(u'UserError'),
                    _(u'Invoice cannot be created from Packing.')
                )

            if pick.move_lines:
                src_usage = pick.move_lines[0].location_id.usage
                dest_usage = pick.move_lines[0].location_dest_id.usage

            if pick.type == 'out' and dest_usage == 'supplier':
                res = 'in_refund'
            elif pick.type == 'out' and dest_usage == 'customer':
                res = 'out_invoice'
            elif pick.type == 'in' and src_usage == 'supplier':
                res = 'in_invoice'
            elif pick.type == 'in' and src_usage == 'customer':
                res = 'out_refund'
            else:
                res = 'out_invoice'

        return res

    _defaults = {
        'type': _get_type,
    }

    def action_create_invoice(self, cursor, uid, ids, context=None):
        """
        Creates an invoice using a picking and the wizard data. Opens a tab with
        the created invoice.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor.
        :param uid: <res.users> id.
        :type uid: long
        :param ids: <wizard.invoice.from.picking> ids.
        :type ids: list
        :param context: OpenERP context.
        :type context: dict
        :return: Dictionary that opens a new tab with the invoice.
        :rtype: dict
        """
        if context is None:
            context = {}

        picking_o = self.pool.get('stock.picking')
        mod_o = self.pool.get('ir.model.data')
        act_o = self.pool.get('ir.actions.act_window')

        picking_ids = context.get('active_ids', False)

        if not isinstance(ids, list) or len(ids) != 1:
            raise NotImplementedError

        wiz = self.browse(cursor, uid, ids, context=context)[0]

        res = picking_o.action_invoice_create(
            cursor, uid, picking_ids, journal_id=wiz.journal_id.id,
            group=wiz.group, type=wiz.type, context=context
        )

        invoice_ids = res.values()
        if not invoice_ids:
            raise osv.except_osv(
                _(u'Error'),
                _(u'Invoice is not created')
            )

        if type == 'out_invoice':
            xml_id = 'action_invoice_tree5'
        elif type == 'in_invoice':
            xml_id = 'action_invoice_tree8'
        elif type == 'out_refund':
            xml_id = 'action_invoice_tree10'
        else:
            xml_id = 'action_invoice_tree12'

        result = mod_o._get_id(cursor, uid, 'account', xml_id)
        id = mod_o.read(cursor, uid, result, ['res_id'], context=context)
        result = act_o.read(cursor, uid, id['res_id'], context=context)
        result['res_id'] = invoice_ids
        result['context'] = context

        return result


WizardInvoiceFromPicking()
