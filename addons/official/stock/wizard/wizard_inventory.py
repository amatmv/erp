# -*- encoding: utf-8 -*-
from osv import fields, osv
from tools.translate import _


class fill_inventory(osv.osv_memory):
    """Product Stock Location List """
    _name = "stock.wizard.fill.inventory"

    _columns = {
        'location_id': fields.many2one(
            'stock.location', 'Location', required=True),
        'line': fields.char('Line', size=32),
        'column': fields.char('Column', size=32),
        'level': fields.char('Level', size=32),
        'recursive': fields.boolean('Include all childs for the location'),
    }

    def _find_products(self, cursor, uid, ids, data, context):
        res_location = {}
        location_obj = self.pool.get('stock.location')
        if data['recursive']:
            location_ids = location_obj.search(cursor, uid, [
                ('location_id', 'child_of', [data['location_id']])
            ])
            for location in location_ids:
                res = location_obj._product_get(cursor, uid, location)
                res_location[location] = res
        else:
            context.update({'compute_child': False})
            res = location_obj._product_get(
                cursor, uid, data['location_id'], context=context)
            res_location[data['location_id']] = res
        return res_location

    def filter_product(
        self, cursor, uid, wizard_id, product_id, wizard_data, context=None
    ):
        product_obj = self.pool.get('product.product')
        prod = product_obj.read(cursor, uid, product_id, [
            'uom_id', 'loc_rack', 'loc_row', 'loc_case'
        ])

        if wizard_data['line'] and wizard_data['line'] != prod['loc_rack']:
            return False
        if wizard_data['column'] and wizard_data['column'] != prod['loc_row']:
            return False
        if wizard_data['level'] and wizard_data['level'] != prod['loc_case']:
            return False
        return True

    def _fill_inventory(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        inventory_obj = self.pool.get('stock.inventory')
        location_obj = self.pool.get('stock.location')
        inventory_line_obj = self.pool.get('stock.inventory.line')
        product_obj = self.pool.get('product.product')
        data = self.read(cursor, uid, ids, context=context)[0]
        inv_data = inventory_obj.read(cursor, uid, context['active_id'])
        res_location = self._find_products(
            cursor=cursor, uid=uid, ids=ids, data=data, context=context)
        product_ids = []
        for location in res_location.keys():
            res = res_location[location]
            for product_id in res.keys():
                uom = False
                prod = product_obj.read(cursor, uid, product_id, ['uom_id'])
                if prod['uom_id'] and prod['uom_id'][0]:
                    uom = prod['uom_id'][0]
                    context.update({'uom': uom})
                amount = location_obj._product_get(
                    cursor, uid, location, [product_id], context=context
                )[product_id]
                if amount and self.filter_product(
                    cursor, uid, ids, product_id, data, context=context
                ):
                    line_ids = inventory_line_obj.search(
                        cursor, uid, [
                            ('inventory_id', '=', inv_data['id']),
                            ('location_id', '=', location),
                            ('product_id', '=', product_id),
                            ('product_uom', '=', uom),
                            ('product_qty', '=', amount)
                        ])
                    if not len(line_ids):
                        inventory_line = {
                            'inventory_id': inv_data['id'],
                            'location_id': location,
                            'product_id': product_id,
                            'product_uom': uom,
                            'product_qty': amount
                        }
                        inventory_line_obj.create(cursor, uid, inventory_line)
                    product_ids.append(product_id)

        if len(product_ids) == 0:
            raise osv.except_osv(
                _('Error !'), _('No product in this location.'))
        return {}


fill_inventory()
