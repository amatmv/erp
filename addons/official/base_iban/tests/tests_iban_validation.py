# -*- coding: utf-8 -*-
from destral import testing
from osv.orm import ValidateException
from expects import expect
from expects import raise_error
from destral.transaction import Transaction


class TestIbanValidation(testing.OOTestCase):
    def test_iban_validation_bad_checksum(self):
        partner_bank_obj = self.openerp.pool.get(
            'res.partner.bank'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            res_country_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'es'
            )[1]
            res_partner_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'main_partner'
            )[1]

            expect(
                lambda: partner_bank_obj.create(
                    cursor, uid, {
                        'acc_country_id': res_country_id,
                        'iban': 'ES9121000418450200051331',
                        'partner_id': res_partner_id,
                        'state': 'iban'
                    }
                )
            ).to(
                raise_error(ValidateException)
            )

    def test_iban_validation_bad_format(self):
        partner_bank_obj = self.openerp.pool.get(
            'res.partner.bank'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            res_country_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'es'
            )[1]
            res_partner_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'main_partner'
            )[1]

            expect(
                lambda: partner_bank_obj.create(
                    cursor, uid, {
                        'acc_country_id': res_country_id,
                        'iban': 'ES912100041845020005133!',
                        'partner_id': res_partner_id,
                        'state': 'iban'
                    }
                )
            ).to(
                raise_error(ValidateException)
            )

    def test_iban_validation_bad_length(self):
        partner_bank_obj = self.openerp.pool.get(
            'res.partner.bank'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            res_country_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'es'
            )[1]
            res_partner_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'main_partner'
            )[1]

            expect(
                lambda: partner_bank_obj.create(
                    cursor, uid, {
                        'acc_country_id': res_country_id,
                        'iban': 'ES912100041',
                        'partner_id': res_partner_id,
                        'state': 'iban'
                    }
                )
            ).to(
                raise_error(ValidateException)
            )

    def test_iban_validation_bad_country_code(self):
        partner_bank_obj = self.openerp.pool.get(
            'res.partner.bank'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            res_country_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'es'
            )[1]
            res_partner_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'main_partner'
            )[1]

            expect(
                lambda: partner_bank_obj.create(
                    cursor, uid, {
                        'acc_country_id': res_country_id,
                        'iban': '123421000418450200051332',
                        'partner_id': res_partner_id,
                        'state': 'iban'
                    }
                )
            ).to(
                raise_error(ValidateException)
            )

    def test_iban_validation_ok(self):
        partner_bank_obj = self.openerp.pool.get(
            'res.partner.bank'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            res_country_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'es'
            )[1]
            res_partner_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'main_partner'
            )[1]
            id_bank = partner_bank_obj.create(
                cursor, uid, {
                    'acc_country_id': res_country_id,
                    'iban': 'ES9121000418450200051332',
                    'partner_id': res_partner_id,
                    'state': 'iban'
                }
            )
            new_iban = partner_bank_obj.read(
                cursor, uid, id_bank, ['iban']
            )['iban']
            assert new_iban == 'ES9121000418450200051332'
