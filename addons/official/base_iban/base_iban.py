# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution	
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    $Id$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import netsvc
import re
from stdnum import iban
from osv import fields, osv

CHAR_OFFSET = 55 # 87 if country code is in lowercase

# Extracted from IBAN Registry - ISO 13616 - Release 46 - November 2013
IBAN_LENGTH = {
    'AL': 28,
    'AD': 24,
    'AT': 20,
    'AZ': 28,
    'BH': 22,
    'BE': 16,
    'BA': 20,
    'BR': 29,
    'BG': 22,
    'CR': 21,
    'HR': 21,
    'CY': 28,
    'DK': 18,
    'FO': 18,
    'GL': 18,
    'DO': 28,
    'EE': 20,
    'FI': 18,
    'FR': 27,
    'GE': 22,
    'DE': 22,
    'GI': 23,
    'GR': 27,
    'GT': 28,
    'HU': 28,
    'IS': 26,
    'IE': 22,
    'IL': 23,
    'IT': 27,
    'KZ': 20,
    'KW': 30,
    'LV': 21,
    'LB': 28,
    'LI': 21,
    'LT': 20,
    'LU': 20,
    'MK': 19,
    'MT': 31,
    'MR': 27,
    'MU': 30,
    'MD': 24,
    'MC': 27,
    'ME': 22,
    'NL': 18,
    'NO': 15,
    'PK': 24,
    'PS': 29,
    'PL': 28,
    'PT': 25,
    'RO': 24,
    'QA': 29,
    'SM': 27,
    'SA': 24,
    'RS': 22,
    'SK': 24,
    'SI': 19,
    'ES': 24,
    'SE': 24,
    'CH': 21,
    'TN': 24,
    'TR': 26,
    'AE': 23,
    'GB': 22,
    'VG': 24,
}


def _format_iban(string):
    '''
    This function removes all characters from given 'string' that isn't a
    alpha numeric and converts it to upper case.
    '''
    res = ""
    for char in string:
        if char.isalnum():
            res += char.upper()
    return res


def shadow_iban(IBAN, indexes):
    """
    Returns a formatted 'shadowed' IBAN. (some characters hidden and separated
    in 6 blocks of 4 items).
    :param IBAN:
    :type: str
    :param indexes: indexes of the numbers to be hidden.
    :type: [int]
    :return:
    """
    iban_cleaned = list(_format_iban(IBAN))
    for index in indexes:
        iban_cleaned[index] = '*'
    iban_shadowed = "".join(iban_cleaned)
    return _printed_iban_format(iban_shadowed)


def _printed_iban_format(iban):
    '''returns a iban in the print format. Groups of four characters'''
    if not iban:
        # For False/None...
        return iban
    groups = (len(iban) % 4) and (len(iban) / 4 + 1) or (len(iban) / 4)
    split_iban = [iban[x:x+4] for x in range(0, groups*4, 4)]
    return ' '.join(split_iban)


class res_partner_bank(osv.osv):
    _inherit = "res.partner.bank"

    def create(self, cr, uid, vals, context={}):
        #overwrite to format the iban number correctly
        if 'iban' in vals and vals['iban']:
            vals['iban'] = _format_iban(vals['iban'])
            
        return super(res_partner_bank, self).create(cr, uid, vals, context)

    def write(self, cr, uid, ids, vals, context={}):
        #overwrite to format the iban number correctly
        if 'iban' in vals and vals['iban']:
            vals['iban'] = _format_iban(vals['iban'])
            
        return super(res_partner_bank, self).write(cr, uid, ids, vals, context)

    def is_iban_valid(self, cursor, uid, number):
        try:
            iban.validate(number)
            return True
        except iban.ValidationError:
            return False

    def check_iban(self, cr, uid, ids):
        '''
        Check the IBAN number
        '''
        for bank_acc in self.browse(cr, uid, ids):
            if not bank_acc.iban or not bank_acc.state == 'iban':
                continue
            iban =_format_iban(bank_acc.iban)
            if not self.is_iban_valid(cr, uid, iban):
                return False
        return True

    def name_get(self, cr, uid, ids, context=None):
        res = []
        to_check_ids = []
        for id in self.browse(cr, uid, ids):
            if id.iban:
                res.append((id.id, id.printable_iban))
            else:
                to_check_ids.append(id.id)
        res += super(res_partner_bank, self).name_get(cr, uid, to_check_ids,
                                                      context)
        return res

    def search(self, cr, uid, args, offset=0, limit=None, order=None,
               context=None, count=False):
    #overwrite the search method in order to search not only on bank
    #type == basic account number but also on type == iban
        res = super(res_partner_bank,self).search(cr, uid, args, offset, limit,
                                                  order, context=context, count=count)
        if filter(lambda x:x[0]=='acc_number' ,args):
            #get the value of the search
            iban_value = filter(lambda x:x[0]=='acc_number' ,args)[0][2]
            #get the other arguments of the search
            args1 =  filter(lambda x:x[0]!='acc_number' ,args)
            #add the new criterion
            args1 += [('iban','ilike',iban_value)]
            #append the results to the older search
            res += super(res_partner_bank,self).search(cr, uid, args1, offset, limit,
                order, context=context, count=count)
        return res

    def get_bban_from_iban(self, cr, uid, ids, context=None):
        '''
        This function returns the bank account number computed from the iban
        account number, thanks to the mapping_list dictionary that contains the
        rules associated to its country.
        '''
        res = {}
        mapping_list = {
         #TODO add rules for others countries
            'be': lambda x: x[4:],
            'fr': lambda x: x[14:],
            'ch': lambda x: x[9:],
            'gb': lambda x: x[14:],
        }
        for record in self.browse(cr, uid, ids, context):
            if not record.iban:
                res[record.id] = False
                continue
            res[record.id] = False
            for code, function in mapping_list.items():
                if record.iban.lower().startswith(code):
                    res[record.id] = function(record.iban)
                    break
        return res
    
    def calculate_iban(self, cr, uid, acc_number, country_code, context=None):
        if not re.match('[a-zA-Z]{2}', country_code):
            raise Exception('Wrong IBAN country code %s' % str(country_code))
        
        raw_iban = _format_iban('%s%s00' % (acc_number, country_code))
        raw_num_iban = ''
        for char in raw_iban:
            if char.isalpha():
                raw_num_iban += str(ord(char)-CHAR_OFFSET)
            else:
                raw_num_iban += char         
        iban_check_code = 98 - (int(raw_num_iban) % 97)
        iban_check_code_str = str(iban_check_code).zfill(2)
        iban = '%s%s%s' % (country_code, iban_check_code_str, acc_number)
        return _format_iban(iban)
    
    def get_iban_from_bban(self, cr, uid, ids, context=None):
        res = {}            
        for id in ids:
            bank = self.browse(cr, uid, id)
            res[bank.id] = False
            if bank.acc_number:
                res[id] = self.calculate_iban(cr, uid, bank.acc_number,
                                              bank.acc_country_id.code)
        return res
 
    def _get_printable_iban(self, cursor, uid, ids, name, arg, context=None):
        res = {}
        for id in ids:
            bank = self.browse(cursor, uid, id)
            res[id] = _printed_iban_format(bank.iban)
        return res
            
    _columns = {
        'iban': fields.char('IBAN', size=34, readonly=True,
                            help="International Bank Account Number"),
        'printable_iban': fields.function(
            _get_printable_iban,
            method=True,
            string='Printable IBAN',
            type='char', size=42, readonly=True,
            help="Space formated International Bank Account Number",
            store={'res.partner.bank': (
                lambda self, cr, uid, ids, c=None: ids,['iban'], 10
            )}
        )
    }

    _constraints = [(check_iban, "The IBAN number doesn't seem to be correct.",
                     ["iban"])]

res_partner_bank()


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

