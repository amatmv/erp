<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="cts"/>
  </xsl:template>

  <xsl:template match="cts">
    <document>
      <template>
        <pageTemplate id="main">
          <frame id="first" x1="1cm" y1="1cm" width="19cm" height="277mm"/>
        </pageTemplate>
      </template>
      
      <stylesheet>

        <paraStyle name="titol"
					fontName="Helvetica"
					fontSize="12"
          leading="30"
				/>

        <blockTableStyle id="taula_contingut">
          <lineStyle kind="GRID" colorName="silver"/>
          <blockFont name="Helvetica" size="8" />
					<blockBackground colorName="grey" start="0,0" stop="7,0" />
					<blockFont name="Helvetica-Bold" size="8" start="0,0" stop="7,0" />
        </blockTableStyle>

      </stylesheet>
    
      <story>
      <para style="titol"  t="1">Llistat de Centres Transformadors</para>

      <blockTable style="taula_contingut" colWidths="1.6cm,5cm,4cm,1.6cm,1.6cm,1.6cm,2cm,2cm">
       <tr>
          <td t="1">Codi</td>
          <td t="1">Nom</td>
          <td t="1">Municipi</td>
          <td t="1">Trafo</td>
          <td t="1">Marca</td>
          <td t="1">Pot.</td>
          <td t="1">Tipus</td>
          <td t="1">Instal.</td>
        </tr>
        <xsl:apply-templates select="ct" mode="story">
        	<xsl:sort select="descripcio" />
        </xsl:apply-templates>
      </blockTable>
      </story>
    </document>
  </xsl:template>

  <xsl:template match="ct" mode="story">
    <xsl:choose>
      <xsl:when test="count(trafo-list/trafo)&gt;0" >
        <xsl:apply-templates select="trafo-list" mode="story" />
      </xsl:when>
      <xsl:otherwise>
        <tr>
          <td><xsl:value-of select="codi"/></td>
          <td><xsl:value-of select="descripcio"/></td>
          <td><xsl:value-of select="municipi"/></td>
          <td></td>
          <td></td>
          <td></td>
          <td><xsl:value-of select="subtipus"/></td>
          <td><xsl:value-of select="installacio"/></td>
        </tr>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="trafo-list" mode="story">
    <xsl:apply-templates select="trafo" mode="story" />
  </xsl:template>

  <xsl:template match="trafo" mode="story">
    <tr>
      <td><xsl:value-of select="../../codi"/></td>
      <td><xsl:value-of select="../../descripcio"/></td>
      <td><xsl:value-of select="../../municipi"/></td>
      <td><xsl:value-of select="trafo-name" /></td>
      <td><xsl:value-of select="trafo-marca" /></td>
      <td><xsl:value-of select="trafo-pot" /></td>
      <td><xsl:value-of select="../../subtipus"/></td>
      <td><xsl:value-of select="../../installacio"/></td>
    </tr>

  </xsl:template>

</xsl:stylesheet>
