# -*- coding: utf-8 -*-
{
    "name": "Report CTS Descripció ASC",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Centres Transformadors",
    "depends":[
        "giscedata_cts"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "report_cts_description_asc_report.xml"
    ],
    "active": False,
    "installable": True
}
