# -*- coding: utf-8 -*-
{
    "name": "Report de Facturació per Bassols",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Account",
    "depends":[
        "giscemisc_pressuposts",
        "account"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "report_facturacio.xml"
    ],
    "active": False,
    "installable": True
}
