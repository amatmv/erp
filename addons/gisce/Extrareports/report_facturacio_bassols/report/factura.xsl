<?xml version="1.0" encoding="iso-8859-15"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

  
  <xsl:decimal-format name="ca_ES" decimal-separator="," grouping-separator="."/>
  
  <xsl:template match="/">
    <xsl:apply-templates select="factures"/>
  </xsl:template>

  <xsl:template match="factures">
    <xsl:apply-templates select="factura" />
  </xsl:template>

  <xsl:template match="factura">
    <document debug="1" compression="1">
      <template pageSize="(21cm, 29.7cm)">
        <pageTemplate id="main">
          <pageGraphics>
          	<image file="addons/giscemisc_pressuposts_multicompany/report/logo_2.png" width="20mm" height="20mm" x="1cm" y="26.6cm" />
          	<setFont name="Helvetica-Bold" size="30" />
          	<drawString x="3.2cm" y="27.8cm"><xsl:value-of select="//corporate-header/corporation/rml_header1" /></drawString>
          	<setFont name="Helvetica" size="10" />
          	<drawString x="3.2cm" y="27.1cm"><xsl:value-of select="//corporate-header/corporation/rml_footer1" /></drawString>
          	<lineMode width="2" />
          	<stroke color="lightgreen"/>
          	<lines>
          		3.2cm 27cm 20cm 27cm
          	</lines>
          	<setFont name="Helvetica" size="8" />
          	<!--<drawString x="3.2cm" y="26.6cm"><xsl:value-of select="//corporate-header/corporation/name" /></drawString>-->
          	<drawRightString x="20cm" y="26.7cm"><xsl:value-of select="//corporate-header/corporation/address[type='default']/street" /></drawRightString>
          	<drawRightString x="20cm" y="26.4cm">Tel. <xsl:value-of select="//corporate-header/corporation/address[type='default']/phone" /> - Fax <xsl:value-of select="//corporate-header/corporation/address[type='default']/fax" /></drawRightString>
          	<drawRightString x="20cm" y="26.1cm"><xsl:value-of select="concat(//corporate-header/corporation/address[type='default']/zip, ' ', //corporate-header/corporation/address[type='default']/city)" /> (<xsl:value-of select="//corporate-header/corporation/address[type='default']/state" />)</drawRightString>
          	<drawRightString x="20cm" y="25.8cm">E-mail: <xsl:value-of select="//corporate-header/corporation/address[type='default']/email" /></drawRightString>
          	<drawRightString x="20cm" y="0.5cm">P�gina <pageNumber /></drawRightString>
          	<rotate degrees="90" />
            <setFont name="Helvetica" size="7" />
            <drawString x="60mm" y="-4mm"><xsl:value-of select="//corporate-header/corporation/rml_footer2" /></drawString>
          </pageGraphics>
          <frame id="first" x1="1cm" y1="1cm" width="19cm" height="247mm"/>
        </pageTemplate>
      </template>
      
      <stylesheet> 
      	<paraStyle name="text"
      		fontName="Helvetica"
      		fontSize="10" />
      		
      	<paraStyle name="vermell"
      		textColor="darkred"
      		fontName="Helvetica-Bold"
      		fontSize="10" />
      		
      	<paraStyle name="blau"
      		textColor="darkblue"
      		fontName="Helvetica-Bold"
      		fontSize="10"
      		leftIndent="0.5cm" />
      		
      	<blockTableStyle id="taula">
          <blockFont name="Helvetica" size="10" />
          <lineStyle kind="LINEBELOW" start="0,0" stop="-1,-1" colorName="silver" />
          <blockFont name="Helvetica-Bold" start="0,0" stop="-1,0" />
        </blockTableStyle>
        
        <blockTableStyle id="taula2">
          <blockFont name="Helvetica" size="10" />
          <lineStyle kind="LINEBELOW" start="1,0" stop="-1,-1" colorName="silver" />
        </blockTableStyle>
        
        <blockTableStyle id="taula_preus">
          <blockFont name="Helvetica" size="10" />
          <lineStyle kind="LINEBELOW" start="0,0" stop="-1,0" colorName="black" />
        </blockTableStyle>
        
        <blockTableStyle id="taula_signatures">
          <blockFont name="Helvetica" size="10" />
          <blockAlignment value="CENTER" />
          <blockValign value="TOP" start="1,1" stop="1,1" />
        </blockTableStyle>

      		
      </stylesheet>
    
      <story>
      	<!-- contingut de la factura -->
      	<spacer length="2cm" />
      	<para style="text">DATA FRA.: <xsl:value-of select="concat(substring(date_invoice, 9, 2),'/',substring(date_invoice, 6, 2),'/',substring(date_invoice, 1, 4))" /></para>
      	<para style="text">NOM: <xsl:value-of select="partner/name" /></para>
      	<para style="text">ADRE�A: <xsl:value-of select="invoice_address/street" /></para>
      	<para style="text">POBLACI�: <xsl:value-of select="invoice_address/city" /> C.P.: <xsl:value-of select="invoice_address/zip" /></para>
      	<spacer length="0.5cm" />
      	<para style="text">NIF: <xsl:value-of select="partner/vat" /></para>
      	
      	<spacer length="1cm" />
      	<para style="text"><b>CONCEPTE:</b></para>
      	<para style="text"><xsl:value-of select="name" /></para>
      	<spacer length="1cm" />
      	<blockTable style="taula" colWidths="2cm,11cm,3cm,3cm" repeatRows="1">
      		<tr>
      			<td>UNITATS</td>
      			<td>DESCRIPCI�</td>
      			<td>PREU UNITARI</td>
      			<td>IMPORT TOTAL</td>
      		</tr>
      		<xsl:apply-templates match="invoice_lines/invoice_line" mode="story" />
      	</blockTable>
      	<spacer length="2cm" />
      	<blockTable style="taula2" colWidths="8cm,8cm,3cm">
      		<tr>
      			<td></td>
      			<td><para style="text">TOTAL BRUT</para></td>
      			<td><para style="text" alignment="right"><xsl:value-of select="format-number(amount_untaxed, '###.##0,00', 'ca_ES')" /> �</para></td>
      		</tr>
      		<tr>
      			<td></td>
      			<td><para style="text">IVA ORDINARI (16%)</para></td>
      			<td><para style="text" alignment="right"><xsl:value-of select="format-number(amount_tax, '###.##0,00', 'ca_ES')" /> �</para></td>
      		</tr>
      		<tr>
      			<td></td>
      			<td><para style="text"><b>TOTAL</b></para></td>
      			<td><para style="text" alignment="right"><b><xsl:value-of select="format-number(amount_total, '###.##0,00', 'ca_ES')" /> �</b></para></td>
      		</tr>
      	</blockTable>
      </story>
    </document>
  </xsl:template>
  
  <xsl:template match="invoice_line" mode="story">
  	<tr>
  		<td><para style="text" alignment="right"><xsl:value-of select="format-number(quantity, '###.##0,00', 'ca_ES')" /></para></td>
  		<td><para style="text"><xsl:value-of select="product" /></para></td>
  		<td><para style="text" alignment="right"><xsl:value-of select="format-number(price_unit, '###.##0,00', 'ca_ES')" /> �</para></td>
  		<td><para style="text" alignment="right"><xsl:value-of select="format-number(price_subtotal, '###.##0,00', 'ca_ES')" /> �</para></td>
  	</tr>
  </xsl:template>
  
</xsl:stylesheet>
