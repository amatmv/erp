<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="descarrec_list" />
  </xsl:template>
  
  <xsl:template match="descarrec_list">
    <xsl:apply-templates select="descarrec" />
  </xsl:template>

  <xsl:template match="descarrec">
    <document>
      <template pageSize="(21cm, 29.7cm)">
        <pageTemplate id="main">
          <pageGraphics>
          	<image file="addons/custom/logo.png" width="40mm" height="40mm" x="85mm" y="247mm" />
          	<setFont name="Helvetica-Bold" size="30" />
          	<drawString x="3.2cm" y="27.8cm"><xsl:value-of select="//corporate-header/corporation/rml_header1" /></drawString>
          	<setFont name="Helvetica" size="10" />
          	<drawString x="3.2cm" y="27.1cm"><xsl:value-of select="//corporate-header/corporation/rml_footer1" /></drawString>
          	<lineMode width="2" />
          	<stroke color="blue"/>
          	<lines>
          		1cm 24cm 20cm 24cm
          	</lines>
          	<setFont name="Helvetica" size="14" />
          	<drawCentredString x="10.5cm" y="23cm">
              <xsl:value-of select="//corporate-header/corporation/name" />
            </drawCentredString>
          	<!--<drawRightString x="20cm" y="26.7cm"><xsl:value-of select="//corporate-header/corporation/address[type='default']/street" /></drawRightString>
          	<drawRightString x="20cm" y="26.4cm"><xsl:value-of select="concat(//corporate-header/corporation/address[type='default']/zip, ' ', //corporate-header/corporation/address[type='default']/city)" /> (<xsl:value-of select="//corporate-header/corporation/address[type='default']/state" />)</drawRightString>
          	<drawRightString x="20cm" y="26.1cm">Telelelelelele. <xsl:value-of select="//corporate-header/corporation/address[type='default']/phone" /> - Fax <xsl:value-of select="//corporate-header/corporation/address[type='default']/fax" /></drawRightString>
          	<drawRightString x="20cm" y="25.8cm">E-mail: <xsl:value-of select="//corporate-header/corporation/address[type='default']/email" /></drawRightString> -->
          	<rotate degrees="90" />
            <setFont name="Helvetica" size="7" />
            <drawString x="60mm" y="-4mm"><xsl:value-of select="//corporate-header/corporation/rml_footer2" /></drawString>
          </pageGraphics>
          <frame id="first" x1="1cm" y1="1cm" width="19cm" height="200mm"/>
        </pageTemplate>
      </template>
      
      <stylesheet>
      
      <paraStyle name="text"
      		fontName="Helvetica"
      		fontSize="12" />
      <paraStyle name="textbold"
          fontName="Helvetica-Bold"
          fontSize="12" />
      </stylesheet>
      
      <story>
      	<para style="text"><t t="1">Cooperativa El�ctrica comunica a sus socios y abonados que debido a trabajos de mantenimiento en la red de distribuci�n se suspender� temporalmente el servicio desde las <xsl:value-of select="substring(data_inici, 12, 5)"/> horas del d�a <xsl:value-of select="concat(substring(data_inici, 9,2),'/',substring(data_inici, 6, 2),'/',substring(data_inici, 1, 4))"/> hasta las <xsl:value-of select="substring(data_final, 12, 5)"/>  horas del d�a <xsl:value-of select="concat(substring(data_inici, 9,2),'/',substring(data_inici, 6, 2),'/',substring(data_inici, 1, 4))"/>.</t></para>
        <spacer length="0.5cm"/>
        <para style="text">La zona afectada es la siguiente:
        </para>
        	<spacer length="0.5cm" />
        	<xpre style="text"><xsl:value-of select="carrers_afectats" /></xpre>
        	<spacer length="0.5cm" />
      	<para style="textbold"><t t="1">Las l�neas se consideraran en tensi�n durante todo el corte. El servicio el�ctrico se repondr� sin previo aviso.</t></para>
        <spacer length="0.5cm" />
        <para style="textbold"><t t="1">Rogamos disculpen las molestias.</t></para>
      	<spacer length="3cm" />
      	<para style="text"><xsl:value-of select="//corporate-header/corporation/address[type='default']/city" />, a <xsl:value-of select="data_string_ca" /></para>
      </story>
    </document>

    </xsl:template>

</xsl:stylesheet>
