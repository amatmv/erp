<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

  <xsl:key name="cts-per-municipi" match="ct" use="municipi" />

  <xsl:template match="/">
    <xsl:apply-templates select="cts"/>
  </xsl:template>

  <xsl:template match="cts">
    <document>
      <template>
        <pageTemplate id="main">
          <frame id="first" x1="1cm" y1="1cm" width="19cm" height="277mm"/>
        </pageTemplate>
      </template>
      
      <stylesheet>

        <paraStyle name="titol"
	  fontName="Helvetica-Bold"
	  fontSize="10"
          leading="20" />
          
        <paraStyle name="text"
	  fontName="Helvetica"
	  fontSize="8" />

        <blockTableStyle id="taula_contingut">
          <lineStyle kind="GRID" colorName="silver" start="0,1" stop="-1,-1"/>
          <blockFont name="Helvetica" size="8" />
          <blockBackground colorName="grey" start="0,1" stop="-1,1" />
          <blockFont name="Helvetica-Bold" size="8" start="0,0" stop="-1,1" />
        </blockTableStyle>

      </stylesheet>
    
      <story>
      <!-- 
        Agrupaci� per grups utilitzant el m�tode Muenchian
        tret de: http://www.jenitennison.com/xslt/grouping/muenchian.html
      -->

      <para style="titol"  t="1">LLISTAT DE CT'S AMB TRANSFORMADOR EN QUALSEVOL ESTAT O SENSE TRANSFORMADOR</para>
      <xsl:for-each select="ct[count(. | key('cts-per-municipi', municipi)[1]) = 1]">
      	<xsl:sort select="municipi" />
	<blockTable style="taula_contingut" colWidths="2.6cm,6cm,2cm,2.8cm,6cm" repeatRows="2">
	  <tr>
	    <td><xsl:value-of select="municipi" /></td>
	    <td></td>
	    <td></td>
	    <td></td>
	    <td></td>
	  </tr>
          <tr>
            <td t="1">N� CT</td>
            <td t="1">NOM C.T.</td>
            <td t="1">TRAFO</td>
            <td t="1">POTENCIA</td>
            <td t="1">ADRE�A</td>
          </tr>
          <xsl:apply-templates select="key('cts-per-municipi', municipi)" mode="story">
            <xsl:sort select="codi" />
          </xsl:apply-templates>
        </blockTable>
        <blockTable colWidths="10.6cm,2.8cm,6cm">
          <tr>
            <td t="1"><para style="text" alignment="right"><b>Pot�ncia Total:</b></para></td>
            <td><para style="text" alignment="right"><xsl:value-of select="sum(key('cts-per-municipi', municipi)/trafos/trafo/pot)" /> kVA</para></td>
            <td></td>
          </tr>
        </blockTable>
        <nextFrame />
      </xsl:for-each>
      </story>
    </document>
  </xsl:template>

  <xsl:template match="ct" mode="story">
    <xsl:choose>
      <xsl:when test="count(trafos/trafo)&gt;0" >
        <xsl:apply-templates select="trafos" mode="story" />
      </xsl:when>
      <xsl:otherwise>
        <tr>
          <td><para style="text"><xsl:value-of select="codi"/></para></td>
          <td><para style="text"><xsl:value-of select="descripcio"/></para></td>
          <td></td>
          <td></td>
          <td><para style="text"><xsl:value-of select="adreca"/></para></td>
        </tr>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="trafos" mode="story">
    <xsl:apply-templates select="trafo" mode="story" />
  </xsl:template>

  <xsl:template match="trafo" mode="story">
    <tr>
      <td><para style="text"><xsl:value-of select="../../codi"/></para></td>
      <td><para style="text"><xsl:value-of select="../../descripcio"/></para></td>
      <td><para style="text"><xsl:value-of select="num" /></para></td>
      <td><para style="text" alignment="right"><xsl:value-of select="pot" /></para></td>
      <td><para style="text"><xsl:value-of select="../../adreca"/></para></td>
    </tr>

  </xsl:template>

</xsl:stylesheet>
