# -*- coding: utf-8 -*-
{
    "name": "Report CTS Agrupat per municipis",
    "description": """Afegeix un report per els CTS agrupats per municpis""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Extrareports",
    "depends":[
        "giscedata_cts"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "report_cts_per_municpi_report.xml"
    ],
    "active": False,
    "installable": True
}
