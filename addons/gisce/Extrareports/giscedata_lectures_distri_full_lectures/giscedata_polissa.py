# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscedataPolissa(osv.osv):
    """Afegim camps per la fulla de lectures manual (v4)
    """
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def _ultima_lectura(self, cursor, uid, ids, field_name, arg, context):
        periode = arg['periode']
        tipus = arg['tipus']
        res = {}
        ids_sql = '(%s)' % ', '.join([str(int(x)) for x in ids])
        cursor.execute("""select
      c.polissa,
      (select
        l.lectura
       from
         giscedata_lectures_lectura l
         left join giscedata_lectures_comptador com on (l.comptador = com.id)
         left join giscedata_polissa pol on (com.polissa = pol.id)
       where
         l.comptador = c.id
         and l.periode in (
            SELECT
              p.id
            from
              giscedata_polissa_tarifa_periodes p
              left join product_product pr
                on (p.product_id = pr.id)
              left join product_template pt
                on (pr.product_tmpl_id = pt.id)
              left join giscedata_polissa_tarifa t
                on (p.tarifa = t.id)
            where
              pt.name = %%s
              and p.tipus = 'te'
         )
         and l.tipus = %%s
         and l.name = pol.data_ultima_lectura
       order by
         l.name desc, l.id desc
       limit 1
       ) as lectura
    from
      giscedata_lectures_comptador c
    where
      c.polissa in %s
      and c.active = True
    """ % ids_sql, (periode, tipus))

        for a in cursor.fetchall():
            res[a[0]] = a[1] or 0
        return res

    _columns = {
        'lect_ant_a_p1': fields.function(
            _ultima_lectura,
            method=True,
            type='integer',
            string='Lectura Ant P1',
            arg={'periode': 'P1', 'tipus': 'A'}
        ),
        'lect_ant_r_p1': fields.function(
            _ultima_lectura,
            method=True,
            type='integer',
            string='Lectura Ant P1',
            arg={'periode': 'P1', 'tipus': 'R'}
        ),
        'lect_ant_a_p2': fields.function(
            _ultima_lectura,
            method=True,
            type='integer',
            string='Lectura Ant P1',
            arg={'periode': 'P2', 'tipus': 'A'}
        ),
        'lect_ant_r_p2': fields.function(
            _ultima_lectura,
            method=True,
            type='integer',
            string='Lectura Ant P1',
            arg={'periode': 'P2', 'tipus': 'R'}
        ),
    }

GiscedataPolissa()