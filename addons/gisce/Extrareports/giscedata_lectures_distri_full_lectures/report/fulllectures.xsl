<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:output indent="yes"/>
  <xsl:key name="kzona" match="polissa" use="zona"/>

  <xsl:template match="/">
    <xsl:apply-templates select="polisses"/>
  </xsl:template>
  <xsl:template match="polisses">
    <document>
      <template pageSize="(21cm, 29.7cm)" showBoundary="0">
        <pageTemplate id="main">
          <pageGraphics> 
            <setFont name="Helvetica-Bold" size="9" />
            <drawCentredString x="10.5cm" y="29cm">RELACION DE ABONADOS PARA TOMA DE LECTURAS - <xsl:value-of select="//corporate-header/corporation/name"/></drawCentredString>
            <setFont name="Helvetica" size="9" />
            <drawCentredString x="10.5cm" y="28.7cm">Hoja <pageNumber/> -- <xsl:value-of select="//polisses/data" />  -- <xsl:value-of select="//polisses/polissa/poblacio" />  <xsl:value-of select="mes" /> <xsl:value-of select="anyy" /></drawCentredString> 
            <lineMode width="1"/>
            <stroke color="black"/>
            <lines>1cm 28.4cm 20cm 28.4cm</lines>
          </pageGraphics>
          <frame id="first" x1="1mm" y1="1mm" width="210mm" height="265mm" />
        </pageTemplate>
      </template>
      <stylesheet>
        <paraStyle name="titol"
          fontName="Helvetica-Bold"
          fontSize="10"
          alignment="CENTER"/>
        <paraStyle name="textheader"
          fontName="Helvetica-Bold"
          fontSize="6" />
        <paraStyle name="text"
          fontName="Helvetica"
          fontSize="6" />

        <paraStyle name="td"
          fontName="Helvetica-Bold"
          fontSize="6"
          leading="4"/>
        <paraStyle name="td2"
          fontName="Helvetica"
          fontSize="6"/>
        <paraStyle name="td2b"
          fontName="Helvetica-Bold"
          fontSize="6"/>
        <blockTableStyle id="a">
          <blockFont name="Helvetica-Bold" size="6" start="0,0" stop="-1,-1"/>
          <lineStyle kind="LINEABOVE" colorName="black" start="0,0" stop="-1,-1" thickness="1"/>
        </blockTableStyle>
        <blockTableStyle id="b">
          <blockFont name="Helvetica-Bold" size="6" start="0,0" stop="-1,-1"/>
        </blockTableStyle>

        <blockTableStyle id="caixa">
          <lineStyle kind="GRID" colorName="black" start="0,0" stop="7,1" thickness="1"/> 
          <lineStyle kind="LINEABOVE" colorName="black" start="-1,0" stop="-1,0" thickness="1"/>
          <lineStyle kind="LINEBELOW" colorName="black" start="-1,1" stop="-1,1" thickness="1"/>
          <lineStyle kind="LINEAFTER" colorName="black" start="8,0" stop="8,1" thickness="1" />
          <lineStyle kind="LINEBEFORE" colorName="black" start="0,0" stop="0,-1" thickness="1"/>
          <lineStyle kind="LINEBELOW" colorName="black" start="0,-1" stop="-1,-1" thickness="1"/>
          <lineStyle kind="LINEAFTER" colorName="black" start="-1,-1" stop="-1,-1" thickness="1"/>

          <blockFont name="Helvetica" size="6" start="0,0" stop="-1,-1"/>
          <blockFont name="Helvetica-Bold" size="6" start="1,2" stop="-1,-1"/>
          <blockValign value="TOP" start="0,0" stop="-1,-1"/>
          <blockLeftPadding length="2" start="0,0" stop="-1, -1"/>
          <blockRightPadding length="2" start="0,0" stop="-1, -1"/>
          <blockBottomPadding length="2" start="0,0" stop="-1, -1"/>
          <blockTopPadding length="2" start="0,0" stop="-1, -1"/>

        </blockTableStyle>

      </stylesheet>
      <story>
        <xsl:for-each select="//polissa[generate-id(.)=generate-id(key('kzona', zona)[1])]">
          <xsl:sort select="ordre"/> 
          <xsl:for-each select="key('kzona', zona)">
            <xsl:sort select="ordre"/>
            <blockTable
              style="a"
              colWidths="3cm,3cm,4.5cm,5cm,3.5cm">
              <tr>
                <td><para style="td">POLIZA=<xsl:value-of select="name"/></para></td>
                <td><para style="td">TARIFA=<xsl:value-of select="tarifa"/></para></td>
                <td><para style="td">Nº CONTADOR=<xsl:value-of select="comptador"/></para></td>
                <td><para style="td">CUPS=<xsl:value-of select="cups"/></para></td>
                <td><para style="td">POT CONTRATADA <xsl:value-of select="potencia"/> kW</para></td>
              </tr>
            </blockTable>
            <blockTable 
              style="b"
              rowHeights="0.3cm"
              colWidths="5cm,9cm,2cm,2cm">
              <tr>
                <td><para style="td"><xsl:value-of select="titular"/></para></td>
                <td><para style="td"><xsl:value-of select="carrer"/> - <xsl:value-of select="poblacio"/></para></td>
                <td><para style="td">ZONA <xsl:value-of select="zona"/></para></td>
                <td><para style="td">ORDEN <xsl:value-of select="ordre"/></para></td>
              </tr>
            </blockTable>
            <!-- fila -->
            <!-- <blockTable style="caixa"> -->
            <blockTable 
              style="caixa" 
              colWidths="2.5cm,1.5cm,2.5cm,1.5cm,2.5cm,1.5cm,2.7cm,1.5cm,1.5cm"
              rowHeights="0.4cm,0.4cm, 0.4cm"> 
              <tr>
                <td><para style="td2">LECT ANT ACT P1</para></td>
                <td><para style="td2"><xsl:value-of select="lect_ant_a_p1"/></para></td>
                <td><para style="td2">LECT ANT REAC P1</para></td>
                <td><para style="td2"><xsl:value-of select="lect_ant_r_p1"/></para></td>
                <td><para style="td2b">Lect Actual Activa P1</para></td>
                <td><para style="td2"></para></td>
                <td><para style="td2b">Lect Actual Reactiva P1</para></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <td><para style="td2">LECT ANT ACT P2</para></td>
                <td><para style="td2"><xsl:value-of select="lect_ant_a_p2"/></para></td>
                <td><para style="td2">LECT ANT REAC P2</para></td>
                <td><para style="td2"><xsl:value-of select="lect_ant_r_p2"/></para></td>
                <td><para style="td2b">Lect Actual Activa P2</para></td>
                <td><para style="td2"></para></td>
                <td><para style="td2b">Lect Actual Reactiva P2</para></td>
                <td></td>
                <td>Max=</td>
              </tr>
              <tr>
                <td><para style="td2">Observaciones:</para></td>
                <td />
                <td />
                <td />
                <td />
                <td />
                <td />
                <td />
                <td />
              </tr>
            </blockTable>
            <spacer length="0.3cm"/>
          </xsl:for-each>
          <nextFrame/>
        </xsl:for-each>
      </story>
    </document>
  </xsl:template>
</xsl:stylesheet>

