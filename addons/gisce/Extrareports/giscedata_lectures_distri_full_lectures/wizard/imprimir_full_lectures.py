# -*- coding: utf-8 -*-

import wizard
import pooler
import time
import calendar
import base64
from osv.osv import except_osv


def _zones(self, cr, uid, context={}):
    cr.execute("select distinct zona as zona from giscedata_cups_ps "
               "where zona is not null order by zona")
    zones = [(a['zona'], a['zona']) for a in cr.dictfetchall()]
    zones.append(('%', 'Todas'))
    return zones

_mesos = [
  ('01', 'Enero'),
  ('02', 'Febrero'),
  ('03', 'Marzo'),
  ('04', 'Abril'),
  ('05', 'Mayo'),
  ('06', 'Junio'),
  ('07', 'Julio'),
  ('08', 'Agosto'),
  ('09', 'Septiembre'),
  ('10', 'Octubre'),
  ('11', 'Noviembre'),
  ('12', 'Diciembre'),
]

_years = [
  (str(int(time.strftime('%Y')) - 1), str(int(time.strftime('%Y')) - 1)),
  (str(int(time.strftime('%Y'))), str(int(time.strftime('%Y'))))
]


def _escollir_zona(self, cr, uid, data, context=None):

    return {
      'type': 'csv',
      'zona': '%',
      'mes': time.strftime('%m'),
      'year': time.strftime('%Y'),
    }

_escollir_zona_form = """<?xml version="1.0"?>
<form string="Seleccionar zona y mes" col="2">
  <field name="zona"/>
  <field name="mes"/>
  <field name="year" />
  <field name="type" />
</form>"""

_escollir_zona_fields = {
  'zona': {'string': 'Zona', 'type': 'selection', 'selection': _zones, 'required': True},
  'mes': {'string': 'Mes', 'type': 'selection', 'selection': _mesos, 'required': True},
  'year': {'string': 'Año', 'type': 'selection', 'selection': _years, 'required': True},
  'type': {'string': 'Salida', 'type': 'selection', 'selection': [('pdf', 'PDF'), ('csv', 'XLS')], 'required': True},
}


def _choose_type_next_state(self, cr, uid, data, context={}):
    if data['form']['type'] == 'csv':
        return 'gen_csv'
    else:
        return 'calc'


def _select_polisses(self, cr, uid, data, context={}):
    polissa_obj = pooler.get_pool(cr.dbname).get('giscedata.polissa')
    cups_obj = pooler.get_pool(cr.dbname).get('giscedata.cups.ps')
    month = data['form']['mes']
    year = data['form']['year']
    data_inici = '%s-%s-01' % (year, month)
    data_final = '%s-%s-%s' % (year, month, calendar.monthrange(int(year), int(month))[1])
    if data['form']['zona'] == '%':
        data['cups_ids'] = cups_obj.search(cr, uid, [('zona', '!=', False)])
    else:
        data['cups_ids'] = cups_obj.search(cr, uid, [('zona', '=', data['form']['zona'])])
    data['polisses_ids'] = polissa_obj.search(cr, uid, [
        ('cups', 'in', data['cups_ids']),
        ('proxima_facturacio', '>=', data_inici),
        ('proxima_facturacio', '<=', data_final),
    ])
    cups_obj = pooler.get_pool(cr.dbname).get('giscedata.cups.ps')

    return {}


def _calc(self, cr, uid, data, context={}):

    # busquem les pòlisses associades als cups que toqui facturar l'any i el mes donats
    polissa_obj = pooler.get_pool(cr.dbname).get('giscedata.polissa')
    return {'ids': data['polisses_ids']}


def _gen_csv(self, cr, uid, data, context={}):
    # Hem de saber si volem generar un CSV per cada zona (tenim el zona = '%') o volem una
    # zona en concret (zona != '%')
    polissa_obj = pooler.get_pool(cr.dbname).get('giscedata.polissa')
    cups_obj = pooler.get_pool(cr.dbname).get('giscedata.cups.ps')
    zones = {}
    for polissa in polissa_obj.browse(cr, uid, data['polisses_ids'], context):
        if not zones.has_key(polissa.cups.zona):
            zones[polissa.cups.zona] = []
        zones[polissa.cups.zona].append(polissa.cups.id)
    # Ja tenim un diccionari amb cada zona i els cups d'aquesta a punt
    # per generar el CSV
    try:
        from xlwt import Workbook, easyxf, XFStyle
    except ImportError:
        raise except_osv("Error", "You must intsall xlutils:\n"
                                  "pip install xlutils")
    w = Workbook(encoding='utf-8')
    for zona in zones.keys():
        ws = w.add_sheet(zona)
        style = easyxf(
          'align: vertical center, horizontal center;'
        )
        ws.write_merge(0,0,4,9, 'ACTIVA', style)
        ws.write_merge(0,0,10,15, 'REACTIVA', style)
        ws.write_merge(0,0,16,21, 'MAXIMETRO', style)
        ws.write_merge(0,0,22,27, 'EXCESOS 6.1', style)
        ws.write(1,0, 'CLIENTE')
        ws.write(1,1, 'DIRECCION')
        ws.write(1,2, 'FECHA')
        ws.write(1,3, 'CONTADOR')
        # Activa
        for i in range(1,7):
            ws.write(1,3+i, 'P%i' % i)
        # Reactiva
        for i in range(1,7):
            ws.write(1,9+i, 'P%i' % i)
        # Maxímetres
        for i in range(1,7):
            ws.write(1,15+i, 'P%i' % i)
        # Excesos
        for i in range(1,7):
            ws.write(1,21+i, 'P%i' % i)
        i = 2
        style = XFStyle()
        style.num_format_str = 'D/M/YYYY'
        cups_ids = cups_obj.search(cr, uid, [('id', 'in', zones[zona])], order="ordre asc")
        for cups in cups_obj.browse(cr, uid, cups_ids):
            if not cups.polissa_polissa:
                continue
            ws.write(i, 0, cups.polissa_polissa.titular.name)
            ws.write(i, 1, cups.polissa_polissa.cups_direccio)
            ws.write(i, 2, None, style)
            ws.write(i, 3, cups.polissa_polissa.comptador)
            i += 1
    if not len(zones):
        w.add_sheet('0')
    w.save('/tmp/LECTURAS_%s.xls' % (data['form']['mes']))
    f = open('/tmp/LECTURAS_%s.xls' % (data['form']['mes']), 'r')
    xls_file = base64.b64encode(f.read())
    f.close()

    return {
      'name': 'LECTURAS_%s.xls' % (data['form']['mes']),
      'xls_file': xls_file,
    }

_gen_csv_form = """<?xml version="1.0"?>
<form string="Fichero" col="2">
  <field name="name" invisible="1" />
  <field name="xls_file" readonly="1" />
</form>"""

_gen_csv_fields = {
  'name': {'string': 'name', 'type': 'char', 'size': 50},
  'xls_file': {'string': 'XLS', 'type': 'binary'}
}


class wizard_imprimir_full_lectures(wizard.interface):
    states = {
      'init': {
        'actions': [],
        'result': {'type': 'state', 'state': 'escollir_zona'}
      },
      'escollir_zona': {
        'actions': [_escollir_zona],
        'result': {'type': 'form', 'arch': _escollir_zona_form, 'fields': _escollir_zona_fields, 'state': [('choose_type','Generar')]},
      },
      'choose_type': {
        'actions': [_select_polisses],
        'result': {'type': 'choice', 'next_state': _choose_type_next_state},
      },
      'calc': {
        'actions': [_calc],
        'result': {'type': 'print', 'report': 'giscedata.polissa.full.lectures', 'get_id_from_action': True, 'state': 'end'},
      },
      'gen_csv': {
        'actions': [_gen_csv],
        'result': {'type': 'form', 'arch': _gen_csv_form, 'fields': _gen_csv_fields, 'state': [('end', 'Cerrar', 'gtk-close')]},
      },
      'end': {
        'actions': [],
        'result': {'type': 'state', 'state': 'end'},
      },
    }

wizard_imprimir_full_lectures('giscedata.cups.ps.full.lectures')
