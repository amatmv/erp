# -*- coding: utf-8 -*-
{
    "name": "Full de lectures",
    "description": """
    This module provide :
      * Full de lectures per passar a llegir de forma manual (impresa)
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Extrareports",
    "depends":[
        "base",
        "giscedata_lectures_distri"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_lectures_report.xml",
        "giscedata_lectures_wizard.xml"
    ],
    "active": False,
    "installable": True
}
