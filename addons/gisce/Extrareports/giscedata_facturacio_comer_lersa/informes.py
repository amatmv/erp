# -*- coding: utf-8 -*-

import jasper_reports


def informe_factura(cr, uid, ids, data, context):
    return {
        'parameters': {'poweremail': context.get('poweremail', False)}
    }

jasper_reports.report_jasper(
   'report.giscedata.facturacio.factura',
   'wizard.informes.facturacio.comer',
   parser=informe_factura)
