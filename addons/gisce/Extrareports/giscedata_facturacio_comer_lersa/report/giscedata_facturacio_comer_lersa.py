# -*- coding: utf-8 -*-

from giscedata_facturacio_comer.report.giscedata_facturacio_comer import FacturaReportWebkitParserHTML, FacturaReportWebkitHTML

FacturaReportWebkitParserHTML(
    'report.giscedata.facturacio.factura',
    'giscedata.facturacio.factura',
    'giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako',
    factura_conceptes_path='giscedata_facturacio_comer/report/factura_conceptes.mako',
    parser=FacturaReportWebkitHTML
)
