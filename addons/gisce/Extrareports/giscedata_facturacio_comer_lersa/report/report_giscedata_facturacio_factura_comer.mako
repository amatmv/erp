<%
from operator import attrgetter, itemgetter
from datetime import datetime, timedelta
import json
from giscedata_facturacio_comer.report.utils import has_setu35_line

pool = objects[0].pool
polissa_obj = objects[0].pool.get('giscedata.polissa')
model_obj = objects[0].pool.get('ir.model.data')
sup_territorials_2013_comer_obj = pool.get('giscedata.suplements.territorials.2013.comer')
banner_obj =  objects[0].pool.get('report.banner')


model_ids = model_obj.search(cursor, uid,
                                 [('module','=','giscedata_facturacio'),
                                  ('name','=','pricelist_tarifas_electricidad')])
tarifa_elect_atr = model_obj.read(cursor, uid, model_ids[0],['res_id'])['res_id']

iet_peatges = "ORDEN IET/107/2014"
itc_lloguer = "ORDEN ITC/3860/2007"

comptador_factures = 0

def clean_nif(nif):
    if nif:
        return nif.replace('ES', '')
    else:
        return ''

def get_exces_potencia(cursor, uid, factura):
    linia_obj = factura.pool.get('giscedata.facturacio.factura.linia')
    linies = linia_obj.search(cursor, uid,[
                ('tipus', '=', 'exces_potencia'),('factura_id', '=', factura.id)
             ])
    if linies:
        return linia_obj.browse(cursor, uid, linies)
    else:
        return []


def get_atr_price(cursor, uid, tarifa, linia):
    pricelist_obj = linia.pool.get('product.pricelist')
    uom_obj = linia.pool.get('product.uom')
    model_obj = objects[0].pool.get('ir.model.data')

    tipus = linia.tipus
    uom_data = {'uom_name': linia.uos_id.name, 'factor': 1}
    quantity = linia.quantity
    product_id = linia.product_id.id

    if tipus == 'potencia':
        uom_data = {'uom_name': 'kW/mes', 'factor': 1}
    elif tipus == 'reactiva':
        model_ids = model_obj.search(cursor, uid,
                                     [('module','=','giscedata_facturacio'),
                                      ('name','=','product_cosfi')])
        product_id = model_obj.read(cursor, uid, model_ids[0],['res_id'])['res_id']
        quantity = l.cosfi * 100

    uom_id = uom_obj.search(cursor, uid, [('name', '=', uom_data['uom_name'])])[0]

    atr_price = pricelist_obj.price_get(cursor, uid, [tarifa],
                                        product_id, quantity,
                                        factura.company_id.partner_id.id,
                                        {'date': linia.data_desde,
                                         'uom_name': uom_id
                                         })[tarifa]
    return atr_price

def get_origen_lectura(cursor, uid, lectura):
    """Busquem l'origen de la lectura cercant-la a les lectures de facturació"""
    res = {lectura.data_actual: _(u"real"),
           lectura.data_anterior: _(u"real")}

    lectura_obj = lectura.pool.get('giscedata.lectures.lectura')
    tarifa_obj = lectura.pool.get('giscedata.polissa.tarifa')
    origen_obj = lectura.pool.get('giscedata.lectures.origen')
    origen_comer_obj = lectura.pool.get('giscedata.lectures.origen_comer')

    # Recuperar l'estimada
    estimada_id = origen_obj.search(cursor, uid, [('codi', '=', '40')])[0]

    #Busquem la tarifa
    if lectura and lectura.name:
        tarifa_id = tarifa_obj.search(cursor, uid, [('name', '=',
                                                lectura.name[:-5])])
    else:
        tarifa_id = False
    if tarifa_id:
        tipus = lectura.tipus == 'activa' and 'A' or 'R'

        search_vals = [('comptador', '=', lectura.comptador),
                       ('periode.name', '=', lectura.name[-3:-1]),
                       ('periode.tarifa', '=', tarifa_id[0]),
                       ('tipus', '=', tipus),
                       ('name', 'in', [lectura.data_actual,
                                       lectura.data_anterior])]
        lect_ids = lectura_obj.search(cursor, uid, search_vals)
        lect_vals = lectura_obj.read(cursor, uid, lect_ids,
                                     ['name', 'origen_comer_id', 'origen_id'])

        for lect in lect_vals:
            # En funció dels origens, escrivim el text
            # Si Estimada (40)
            # La resta: Real
            origen_txt = _(u"real")
            if lect['origen_id'][0] == estimada_id:
                origen_txt = _(u"estimada")

            res[lect['name']] = "%s" % (origen_txt)

    return res

def get_distri_phone(cursor, uid, polissa):
    """ Per telèfons de ENDESA segons CUPS, aprofitant funció de switching"""
    sw_obj = polissa.pool.get('giscedata.switching')
    pa_obj = polissa.pool.get('res.partner.address')

    return polissa.distribuidora.address[0].phone

    partner_id = sw_obj.partner_map(cursor, uid,
                                    polissa.cups, polissa.distribuidora.id)
    if partner_id:
        pa_ids = pa_obj.search(cursor, uid, [('partner_id', '=', partner_id)])
        return (pa_obj.read(cursor, uid, [pa_ids[0]],['phone'])[0]['phone'] or
                polissa.distribuidora.address[0].phone)
    else:
        return polissa.distribuidora.address[0].phone

historic_sql = """SELECT * FROM (
SELECT mes AS mes,
periode AS periode,
sum(suma_fact) AS facturat,
sum(suma_consum) AS consum,
min(data_ini) AS data_ini,
max(data_fin) AS data_fin
FROM (
SELECT f.polissa_id AS polissa_id,
       to_char(f.data_inici, 'YYYY/MM') AS mes,
       pt.name AS periode,
       COALESCE(SUM(il.quantity*(fl.tipus='energia')::int*(CASE WHEN i.type='out_refund' THEN -1 ELSE 1 END)),0.0) as suma_consum,
       COALESCE(SUM(il.price_subtotal*(fl.tipus='energia')::int*(CASE WHEN i.type='out_refund' THEN -1 ELSE 1 END)),0.0) as suma_fact,
       MIN(f.data_inici) data_ini,
       MAX(f.data_final) data_fin
       FROM
            giscedata_facturacio_factura f
            LEFT JOIN account_invoice i on f.invoice_id = i.id
            LEFT JOIN giscedata_facturacio_factura_linia fl on f.id=fl.factura_id
            LEFT JOIN account_invoice_line il on il.id=fl.invoice_line_id
            LEFT JOIN product_product pp on il.product_id=pp.id
            LEFT JOIN product_template pt on pp.product_tmpl_id=pt.id
       WHERE
            fl.tipus = 'energia' AND
            f.polissa_id = %(p_id)s AND
            f.data_inici <= %(data_inicial)s AND
            f.data_inici >= date_trunc('month', date %(data_final)s) - interval '14 month'
            AND (fl.isdiscount IS NULL OR NOT fl.isdiscount)
            AND i.type in('out_invoice', 'out_refund')
       GROUP BY
            f.polissa_id, pt.name, f.data_inici
       ORDER BY f.data_inici DESC ) AS consums
GROUP BY polissa_id, periode, mes
ORDER BY mes DESC, periode ASC
) consums_ordenats
ORDER BY mes ASC"""

# Repartiment segons BOE
rep_BOE = {'i': 36.28, 'c': 32.12 ,'o': 31.60}

%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<head>
<link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_lersa/report/pie.css"/>
<link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_lersa/report/consum.css"/>
<link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_lersa/report/giscedata_facturacio_comer_lersa.css"/>
<script src="${addons_path}/giscedata_facturacio_comer/report/assets/d3.min.js"></script>
</head>
<body>
%for factura in objects:
<%
    if factura.lang_partner not in ['es_ES', 'ca_ES']:
        factura_lang = 'ca_ES'
    else:
        factura_lang = factura.lang_partner
    setLang(factura_lang)

    polissa = polissa_obj.browse(cursor, uid, factura.polissa_id.id,
                                 context={'date': factura.data_final.val})

    tarifa_elect_comer = polissa.llista_preu.id

    dphone = get_distri_phone(cursor, uid, polissa)
    cphone = factura.company_id.partner_id.address[0].phone
    distri_phone = ''.join([dphone[i:i+3] for i in range(0,len(dphone),3)])
    comer_phone = '.'.join([cphone[i:i+3] for i in range(0,len(cphone),3)])

    factura._cr.execute(historic_sql,{'p_id':factura.polissa_id.id,
                                      'data_inicial': factura.data_inici,
                                      'data_final': factura.data_final,
                                      })
    historic = factura._cr.dictfetchall()

    historic_graf = {}
    periodes_graf = []

    for row in historic:
        historic_graf.setdefault(row['mes'],{})
        historic_graf[row['mes']].update({row['periode']: row['consum']})
        periodes_graf.append(row['periode'])

    periodes_graf = list(set(periodes_graf))
    consums_mig = dict.fromkeys(list(set(periodes_graf)), 0)
    consums_mig.update({'total':0})
    historic_js = []
    for mes, consums in historic_graf.items():
        p_js = {'mes': mes}
        for p in periodes_graf:
            p_js.update({p: consums.get(p,0.0)})
            consums_mig[p] += consums.get(p,0.0)
        historic_js.append(p_js)
        consums_mig['total'] += 1

    consums_mig = dict((k, v/consums_mig['total']) for k, v in consums.items() if k.startswith('P'))
    consums_mig = sorted(consums_mig.items(),key=itemgetter(1),reverse=True)

    total_historic_kw = sum([h['consum'] for h in historic])
    total_historic_eur = sum([h['facturat'] for h in historic])
    data_ini = min([h['data_ini'] for h in historic])
    data_fin = max([h['data_fin'] for h in historic])
    historic_dies = 1 + (datetime.strptime(data_fin, '%Y-%m-%d') - datetime.strptime(data_ini, '%Y-%m-%d')).days

    mes_any_inicial = (datetime.strptime(factura.data_inici,'%Y-%m-%d') - timedelta(days=365)).strftime("%Y/%m")
    total_any = sum([h['consum'] for h in historic if h['mes'] > mes_any_inicial])

    iese_lines = [l for l in factura.tax_line if 'IVA' not in l.name]
    iva_lines = [l for l in factura.tax_line if 'IVA' in l.name]

    impostos = {}
    for l in factura.tax_line:
        impostos.update({l.name: l.amount})

    iese = sum([v for k, v in impostos.items() if 'IVA' not in k])

    #TODO:  Quan es processin les línies, repassar una forma + eficient
    lloguer_lines = [l for l in factura.linia_ids if l.tipus in 'lloguer']
    altres_lines = [l for l in factura.linia_ids if l.tipus in 'altres'
                    and l.invoice_line_id.product_id.code != 'DN01']
    donatiu_lines = [l for l in factura.linia_ids if l.tipus in 'altres'
                    and l.invoice_line_id.product_id.code == 'DN01']

    donatiu = sum([l.price_subtotal for l in donatiu_lines])
    total_altres = sum([l.price_subtotal for l in altres_lines])
    periodes = dict([('P%s' % p,'') for p in range(1, 7)])
    periodes_a = sorted(list(set([lectura.name[-3:-1]
                                for lectura in factura.lectures_energia_ids
                                if lectura.tipus == 'activa'])))

    periodes_r = sorted(list(set([lectura.name[-3:-1]
                                for lectura in factura.lectures_energia_ids
                                if lectura.tipus == 'reactiva'])))

    periodes_m = sorted(list(set([lectura.name
                                  for lectura in factura.lectures_potencia_ids])))

    potencies_periode = dict([('P%s' % p,'') for p in range(1, 7)])
    for pot in polissa.potencies_periode:
        potencies_periode[pot.periode_id.name] = pot.potencia

    fact_pot_txt = ((polissa.facturacio_potencia=='max' or len(periodes_a)>3)
                        and _(u"facturació per maxímetre")
                     or _(u"facturació per ICP"))

    # lectures (activa) ordenades per comptador i període
    lectures_a = {}
    lectures_r = {}
    lectures_m = []

    # Detall factures per excés potència

    exces_potencia = get_exces_potencia(cursor, uid, factura)

    for lectura in factura.lectures_energia_ids:
        origens = get_origen_lectura(cursor, uid, lectura)
        if lectura.tipus == 'activa':
            lectures_a.setdefault(lectura.comptador,[])
            lectures_a[lectura.comptador].append((lectura.name[-3:-1],
                                                lectura.lect_anterior,
                                                lectura.lect_actual,
                                                lectura.consum,
                                                lectura.data_anterior,
                                                lectura.data_actual,
                                                origens[lectura.data_anterior],
                                                origens[lectura.data_actual],
                                                ))
        elif lectura.tipus == 'reactiva':
            lectures_r.setdefault(lectura.comptador,[])
            lectures_r[lectura.comptador].append((lectura.name[-3:-1],
                                                lectura.lect_anterior,
                                                lectura.lect_actual,
                                                lectura.consum,
                                                lectura.data_anterior,
                                                lectura.data_actual,
                                                origens[lectura.data_anterior],
                                                origens[lectura.data_actual],
                                                ))

    for lectura in factura.lectures_potencia_ids:
        lectures_m.append((lectura.name, lectura.pot_contract,
                           lectura.pot_maximetre ))

    for lects in lectures_a.values():
        sorted(lectures_a, key=lambda x: x[0])

    for lects in lectures_r.values():
        sorted(lectures_r, key=lambda x: x[0])

    for lects in lectures_m:
        sorted(lectures_m, key=lambda x: x[0])

    total_lectures_a = dict([(p, 0) for p in periodes_a])
    total_lectures_r = dict([(p, 0) for p in periodes_r])
    fact_potencia = dict([(p,0) for p in periodes_m])

    dies_factura = 1 + (datetime.strptime(factura.data_final, '%Y-%m-%d') - datetime.strptime(factura.data_inici, '%Y-%m-%d')).days
    diari_factura_actual_eur = factura.total_energia / (dies_factura or 1.0)
    diari_factura_actual_kwh = (factura.energia_kwh * 1.0) / (dies_factura or 1.0)

    for c, ls in lectures_a.items():
        for l in ls:
            total_lectures_a[l[0]] += l[3]

    for c, ls in lectures_r.items():
        for l in ls:
            total_lectures_r[l[0]] += l[3]

    for p in [(p.product_id.name, p.quantity) for p in factura.linies_potencia]:
        fact_potencia.update({p[0]: max(fact_potencia.get(p[0], 0), p[1])})

    #comprovem si té alguna lectura de maxímetre
    te_maximetre = (max([l[2] for l in lectures_m] or (0,)) > 0)


    # DADES PIE CHART
    pie_total = factura.amount_total - factura.total_lloguers
    pie_regulats = (factura.total_atr + total_altres)
    pie_impostos = float(factura.amount_tax)
    pie_costos = (pie_total - pie_regulats - pie_impostos )

    reparto = { 'i': ((pie_regulats * rep_BOE['i'])/100),
                'c': ((pie_regulats * rep_BOE['c'])/100),
                'o': ((pie_regulats * rep_BOE['o'])/100)
               }

    dades_reparto = [
        [[0, rep_BOE['i']], 'i', _(u"Incentius a les energies renovables, cogeneració i residus"), formatLang(reparto['i'])],
         [[rep_BOE['i'] , rep_BOE['i'] + rep_BOE['c']], 'c', _(u"Cost de xarxes de distribució i transport"), formatLang(reparto['c'])] ,
         [[rep_BOE['i'] + rep_BOE['c'], 100.00], 'o', _(u"Altres Costos regulats (inclosa anualitat del dèficit)"), formatLang(reparto['o'])]
        ]
%>
<%def name="emergency_complaints(factura)">
    <div class="emergency_complaints">
        <div class="left-50 emergency">
            <h1>${_(u"AVARIES I URGÈNCIES")}</h1>
            <dl>
                <dt>${_(u"Empresa distribuïdora")}</dt>
                <dl>${polissa.distribuidora.name}</dl>
                <dt>${_(u"Telèfon gratuït")}</dt>
                <dl>${distri_phone}</dl>
                <dt>${_(u"Contracte d'accés")}</dt>
                <dl>${polissa.ref_dist or factura.polissa_id.ref_dist}</dl>
            </dl>
        </div>
        <div class="right-50 complaints">
            <h1>${_(u"RECLAMACIONS")}</h1>
            <dl>
                <dt>${_(u"Empresa comercialitzadora")}</dt>
                <dl>${factura.company_id.partner_id.name}</dl>
                <dt>${_(u"Telèfon gratuït")}</dt>
                <dl>${factura.company_id.partner_id.address[0].mobile}</dl>
                <dt>${_(u"Email")}</dt>
                <dl>${factura.company_id.partner_id.address[0].email}</dl>
            </dl>
        </div>
        <div class="separator" ></div>
        <p>${_(u"Si no està d'acord amb la nostra resposta a la seva reclamació, pot reclamar a l'organisme administratiu competent: INFORMEU-VOS AL 012 (teléfon d'Atenció Ciutadana)")}</p>
    </div>
    <div class="separator" ></div>
</%def>
<%def name="header_factura()">
    <div class="logo">
        <div class="left">
            <img class="lersa" src="${addons_path}/giscedata_facturacio_comer_lersa/report/logo_lersa.png"/>
        </div>
    </div>
    <div class="separator" ></div>
</%def>
<div id="container">
    <div class="page">
        <div class="capcalera">
            <div class="left-50">
                <div class="company_address">
                    <span class="company_name">${factura.company_id.partner_id.name}</span><br />
                    ${_(u"CIF:")} ${clean_nif(factura.company_id.partner_id.vat)} <br />
                    ${factura.company_id.partner_id.address[0].street.replace('..', '.')} - ${factura.company_id.partner_id.address[0].city} - ${factura.company_id.partner_id.address[0].zip} - ${factura.company_id.partner_id.address[0].state_id.name}<br />
                    ${factura.company_id.partner_id.address[0].phone} <br />
                    <span class="dades_web">${factura.company_id.partner_id.address[0].email}</span><span class="dades_web">www.lersaenergia.com</span>
                </div>
            </div>
            <div class="right-50">
                <div class="logo">
                    <div class="left">
                        <img class="lersa" src="${addons_path}/giscedata_facturacio_comer_lersa/report/logo_lersa.png"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="separator" ></div>
        <!-- PART SUPERIOR -->
        <div class="part_superior">
            <div class="left-30">
                <div class="logos">
                    <div class="left">
                        <img style="width: 62px;" src="${addons_path}/giscedata_facturacio_comer_lersa/report/renovable.jpg"/>
                    </div>
                    <div class="left">
                        <img style="width: 45px; margin-left: 20px;" src="${addons_path}/giscedata_facturacio_comer_lersa/report/geode.jpg"/>
                    </div>
                    <div class="left">
                        <img style="width: 100px; margin-left: 20px; margin-top: 10px;" src="${addons_path}/giscedata_facturacio_comer_lersa/report/logo_aseme.jpg"/>
                    </div>
                </div>
            </div>
            <div class="right-70">
                <div class="separator"></div>
                <div class="direccio_enviament">
                    <table>
                        <tr><td><em>${factura.address_invoice_id.name}</em></td></tr>
                        <tr><td><em>${factura.address_invoice_id.street.replace('..', '.')}</em></td></tr>
                        <tr><td><em>${factura.address_invoice_id.zip} ${factura.address_invoice_id.id_municipi.name}</em></td></tr>
                        <tr><td><em>${factura.address_invoice_id.state_id.name}</em></td></tr>
                    </table>
                </div>
            </div>
        </div>
        <!-- END PART SUPERIOR -->
        <div></div>
        <!-- CONTRATO  -->
        <div class="contracte">
            <div class="left-50">
                <div class="separator"></div>
                <div class="box">
                    <div class="box-header" >
                        <h1>${_(u"FACTURA")}</h1>
                    </div>
                    <table>
                        <tr><td><em>${_(u"Número de factura")}</em></td><td>${factura.number or ''}</td></tr>
                        %if factura.tipo_rectificadora in ['B', 'A', 'BRA']:
                            <tr><td></td><td>${_('Aquesta factura anul·la la')} ${factura.ref.number or ''}</td></tr>
                        %endif

                        %if factura.tipo_rectificadora in ['R', 'RA']:
                            <tr><td></td><td>${_('Aquesta factura rectifica la')} ${factura.ref.number or ''}</td></tr>
                        %endif
                        <tr><td><em>${_(u"Data Emissió")}</em></td><td>${factura.date_invoice}</td></tr>
                        <tr><td><em>${_(u"Període de facturació")}</em></td><td>${_("de %s a %s") % (factura.data_inici, factura.data_final)}</td></tr>
                        <tr><td><em>${_(u"Dies facturats")}</em></td><td>${dies_factura}</td></tr>
                        <tr><td><em>${_(u"Pagador")}</em></td><td>${factura.partner_id.name}</td></tr>
                        <tr><td><em>${_(u"NIF")}</em></td><td>${clean_nif(factura.partner_id.vat)}</td></tr>
                        %if factura.partner_bank:
                            <tr><td><em>${_(u"Entitat")}</em></td><td>${factura.partner_bank.bank.name}</td></tr>
                            <tr><td><em>${_(u"IBAN")}</em></td><td>${factura.partner_bank.printable_iban[:-5]}*****</td></tr>
                        %endif
                    </table>
                </div>
                <div class="box">
                    <div class="box-header" >
                        <h1>${_(u"RESUM FACTURA")}</h1>
                    </div>
                    <table>
                        <tr><td><em>${_(u"Per energia")}</em></td><td class="e">${"%s&euro;" % formatLang(factura.total_energia)}</td></tr>
                        <tr><td><em>${_(u"Per potència")}</em></td><td class="e">${"%s&euro;" % formatLang(factura.total_potencia)}</td></tr>
                    % if factura.total_reactiva > 0:
                        <tr><td><em>${_("Penalització per energia reactiva")}</em></td><td class="e">${"%s&euro;" % formatLang(factura.total_reactiva)}</td></tr>
                    % endif
                        <tr><td><em>${_(u"Impost electricitat")}</em></td><td class="e">${"%s&euro;" % formatLang(iese)}</td></tr>
                        <tr><td><em>${_(u"Lloguer comptador")}</em></td><td class="e">${"%s&euro;" % formatLang(factura.total_lloguers)}</td></tr>
                    % if total_altres != 0:
                        <tr><td><em>${_("Altres conceptes")}</em></td><td class="e">${"%s&euro;" % formatLang(total_altres)}</td></tr>
                    % endif
                    % for n, v in impostos.items():
                        % if "IVA" in n:
                            <tr><td><em>${n}</em></td></em><td class="e">${"%s&euro;" % formatLang(v)}</td></tr>
                        % endif
                    % endfor
                        <tr class="total"><td>${_(u"TOTAL")}</td><td class="e">${"%s&euro;" % formatLang(factura.amount_total)}</td></tr>
                    </table>
                </div>
            </div>
            <div class="right-50">
                <div class="box">
                    <div class="box-header" >
                        <h1>${_(u"CONTRACTE")}</h1>
                    </div>
                    <table>
                        <tr><td><em>${_(u"Titular")}</em></td><td></td></tr>
                        <tr><td><em>${_(u"Nom/Raó social")}</em></td><td>${polissa.titular.name}</td></tr>
                        <tr><td><em>${_(u"NIF/CIF")}</em></td><td>${clean_nif(polissa.titular.vat)}</td></tr>
                    </table>
                    <div class="separator contract"></div>
                    <table>
                        <tr><td><em>${_(u"Contracte del <br> suministrament")}</em></td><td>${factura.polissa_id.name}</td></tr>
                        <tr><td><em>${_(u"Contracte d'accés")}</em></td><td>${polissa.ref_dist or factura.polissa_id.ref_dist}</td></tr>
                        <tr><td><em>${_(u"Producte Comer")}</em></td><td>${polissa.llista_preu.name}</td></tr>
                        <tr><td><em>${_(u"Distribuïdora")}</em></td><td>${polissa.distribuidora.name}</td></tr>
                        <tr><td><em>${_(u"Data alta")}</em></td><td>${formatLang(polissa.data_alta, date=True)}</td></tr>
                        <tr><td><em>${_(u"Data final")}</em><br><span class="little">${_("(renovació anual automàtica)")}</span></td><td>${formatLang(polissa.modcontractual_activa.data_final, date=True)}</td></tr>
                        <tr><td><em>${_(u"CNAE")}</em></td><td>${polissa.cnae.name}</td></tr>
                        <tr><td><em>${_(u"CUPS")}</em></td><td>${factura.cups_id.name}</td></tr>
                        <tr><td><em>${_(u"Adreça de subministrament")}</em></td><td>${factura.cups_id.direccio}</td></tr>
                    </table>
                    <div class="separator contract"></div>
                    <div class="potencies_contractades">
                        <table>
                            <tr class="header">
                                <td colspan="6">
                                    ${_(u"Potència contractada")}
                                    % if te_maximetre:
                                        ${_(u" (Maxímetre)")}
                                    %else:
                                        ${_(u" (ICP)")}
                                    % endif:
                                </td>
                            </tr>
                            <tr class="header">
                                % for periode, potencia in sorted(potencies_periode.items(),key=itemgetter(0)):
                                    % if potencia:
                                        <td>${periode}</td>
                                    %endif
                                % endfor
                            </tr>
                            <tr>
                                % for periode, potencia in sorted(potencies_periode.items(),key=itemgetter(0)):
                                     % if potencia:
                                        <td>${formatLang(potencia, digits=3)}
                                        kW
                                        </td>
                                      % endif

                                % endfor
                            </tr>
                        </table>
                    </div>
                    <div class="separator"></div>
                </div>
            </div>
        </div>
        <div class="separator"></div>
        <!-- END CONTRATO -->
        <!-- FACTURA  -->
<!--
        <div class="box">
            <div class="box-header" >
                <h1>${_(u"FACTURA")}</h1>
            </div>
            <div class="left-50">
                <div class="invoice_summary">
                    <table>
                        <tr><td><em>${_(u"Número de factura")}</em></td><td>${factura.number or ''}</td></tr>
                        <tr><td><em>${_(u"Data")}</em></td><td>${factura.date_invoice}</td></tr>
                        <tr><td><em>${_(u"Període de facturació")}</em></td><td>${_("de %s a %s") % (factura.data_inici, factura.data_final)}</td></tr>
                        <tr><td><em>${_(u"Dies facturats")}</em></td><td>${dies_factura}</td></tr>
                        <tr><td><em>${_(u"Pagador")}</em></td><td>${factura.partner_id.name}</td></tr>
                        <tr><td><em>${_(u"NIF")}</em></td><td>${clean_nif(factura.partner_id.vat)}</td></tr>
                        %if factura.partner_bank:
                            <tr><td><em>${_(u"Entitat")}</em></td><td>${factura.partner_bank.bank.name}</td></tr>
                            <tr><td><em>${_(u"IBAN")}</em></td><td>${factura.partner_bank.printable_iban[:-5]}*****</td></tr>
                        %endif
                    </table>
                </div>
            </div>
            <div class="right-50">
                <div class="invoice_data">
                    <table>
                    <tr><td><em>${_(u"Per energia")}</em></td><td class="e">${"%s&euro;" % formatLang(factura.total_energia)}</td></tr>
                    <tr><td><em>${_(u"Per potència")}</em></td><td class="e">${"%s&euro;" % formatLang(factura.total_potencia)}</td></tr>
                % if factura.total_reactiva > 0:
                    <tr><td><em>${_("Penalització per energia reactiva")}</em></td><td class="e">${"%s&euro;" % formatLang(factura.total_reactiva)}</td></tr>
                % endif
                    <tr><td><em>${_(u"Impost electricitat")}</em></td><td class="e">${"%s&euro;" % formatLang(iese)}</td></tr>
                    <tr><td><em>${_(u"Lloguer comptador")}</em></td><td class="e">${"%s&euro;" % formatLang(factura.total_lloguers)}</td></tr>
                % if total_altres > 0:
                    <tr><td><em>${_("Altres conceptes")}</em></td><td class="e">${"%s&euro;" % formatLang(total_altres)}</td></tr>
                % endif
                % for n, v in impostos.items():
                    % if "IVA" in n:
                        <tr><td><em>${n}</em></td></em><td class="e">${"%s&euro;" % formatLang(v)}</td></tr>
                    % endif
                % endfor
                    <tr class="total"><td>${_(u"TOTAL")}</td><td class="e">${"%s&euro;" % formatLang(factura.amount_total)}</td></tr>
                    </table>
                </div>
            </div>

            <div class="separator"></div>
        </div>
-->
        <!-- END FACTURA -->
        <!-- LECTURES -->
        <div class="box detall_factura">
            <div class="box-header" >
                <h1>${_(u"CONSUM ELÈCTRIC")}</h1>
            </div>
            <div>
                <table class="lectures">
                    <!-- ACTIVA -->
                    <tr class="header">
                        <th><em>${_("ENERGIA")}</em></th>
                        % for i, elem in enumerate(periodes):
                            %try:
                                <% p = periodes_a[i] %>
                                <td>${periodes_a[i]}</td>
                            %except IndexError:
                                <td></td>
                            %endtry
                        % endfor
                    </tr>
                    % for comptador in sorted(lectures_a):
                        <tr>
                            <th>${_(u"Número de comptador")}</th>
                                % for i, elem in enumerate(periodes):
                                    %try:
                                        <% p = periodes_a[i] %>
                                        <td>${comptador}</td>
                                    %except IndexError:
                                        <td></td>
                                    %endtry
                                % endfor
                        </tr>
                        <tr>
                            <th>${_(u"Lectura anterior")} ${lectures_a[comptador][0][4]} (${lectures_a[comptador][0][6]})</th>
                            % for i, elem in enumerate(periodes):
                                %try:
                                    <% p = periodes_a[i] %>
                                    <td>${formatLang(int(lectures_a[comptador][i][1]),digits=0)} kWh</td>
                                %except IndexError:
                                    <td></td>
                                %endtry
                            % endfor
                        </tr>
                        <tr>
                            <th>${_(u"Lectura actual")} ${lectures_a[comptador][0][5]} (${lectures_a[comptador][0][7]})</th>
                            % for i, elem in enumerate(periodes):
                                %try:
                                    <% p = periodes_a[i] %>
                                    <td>${formatLang(int(lectures_a[comptador][i][2]),digits=0)} kWh</td>
                                %except IndexError:
                                    <td></td>
                                %endtry
                            % endfor
                        </tr>
                    % endfor
                    <tr class="resum_periode">
                        <th>${_(u"Total període")}</th>
                        % for i, elem in enumerate(periodes):
                            %try:
                                <% p = periodes_a[i] %>
                                <td>${formatLang(total_lectures_a[p], digits=0)} kWh</td>
                            %except IndexError:
                                <td></td>
                            %endtry
                        %endfor
                    </tr>
                    <!-- ACTIVA -->
                    <!-- REACTIVA -->
                    % if len(periodes_r) or te_maximetre:
                        <tr><td colspan="7" class="separator_energia big"></td></tr>
                        <tr class="header">
                            <th><em>${_("ENERGIA REACTIVA")}</em></th>
                            % for i, elem in enumerate(periodes):
                                %try:
                                    <% p = periodes_r[i] %>
                                    <td>${periodes_r[i]}</td>
                                %except IndexError:
                                    <td></td>
                                %endtry
                            % endfor
                        </tr>
                        % for comptador in sorted(lectures_r):
                            <tr>
                                <th>${_(u"Número de comptador")}</th>
                                    % for i, elem in enumerate(periodes):
                                        %try:
                                            <% p = periodes_r[i] %>
                                            <td>${comptador}</td>
                                        %except IndexError:
                                            <td></td>
                                        %endtry
                                    % endfor
                            </tr>
                            <tr>
                                <th>${_(u"Lectura anterior")} ${lectures_r[comptador][0][4]} (${lectures_r[comptador][0][6]})</th>
                                % for i, elem in enumerate(periodes):
                                    %try:
                                        <% p = periodes_r[i] %>
                                        <td>${formatLang(int(lectures_r[comptador][i][1]),digits=0)} kWh</td>
                                    %except IndexError:
                                        <td></td>
                                    %endtry
                                % endfor
                            </tr>
                            <tr>
                                <th>${_(u"Lectura actual")} ${lectures_r[comptador][0][5]} (${lectures_r[comptador][0][7]})</th>
                                % for i, elem in enumerate(periodes):
                                    %try:
                                        <% p = periodes_r[i] %>
                                        <td>${formatLang(int(lectures_r[comptador][i][2]),digits=0)} kWh</td>
                                    %except IndexError:
                                        <td></td>
                                    %endtry
                                % endfor
                            </tr>
                        % endfor
                        <tr class="resum_periode">
                            <th>${_(u"Total període")}</th>
                            % for i, elem in enumerate(periodes):
                                %try:
                                    <% p = periodes_r[i] %>
                                    <td>${formatLang(total_lectures_r[p], digits=0)} kWh</td>
                                %except IndexError:
                                    <td></td>
                                %endtry
                            % endfor
                        </tr>
                    %endif
                    <!-- REACTIVA -->
                    <!-- MAXIMETRE -->
                    % if te_maximetre:
                        <tr><td colspan="7" class="separator_energia big"></td></tr>
                        <tr class="header">
                            <th><em>${_("MAXÍMETRE")}</em></th>
                            % for i, elem in enumerate(periodes):
                                %try:
                                    <% p = periodes_m[i] %>
                                    <td>${p}</td>
                                %except IndexError:
                                    <td></td>
                                %endtry
                            % endfor
                        </tr>
                        <tr>
                            <th>${_(u"Potència contractada")}</th>
                            % for i, elem in enumerate(periodes):
                                %try:
                                    <% lec = lectures_m[i] %>
                                    <td>${formatLang(lec[1], digits=3)} kW</td>
                                %except IndexError:
                                    <td></td>
                                %endtry
                            % endfor
                        </tr>
                        <tr>
                            <th>${_(u"Lectura maxímetre")}</th>
                            % for i, elem in enumerate(periodes):
                                %try:
                                    <% lec = lectures_m[i] %>
                                    <td>${formatLang(lec[2], digits=3)} kW</td>
                                %except IndexError:
                                    <td></td>
                                %endtry
                            % endfor
                        </tr>
                        <tr class="resum_periode">
                            <th>${_(u"Potència facturada període")}</th>
                            % for i, elem in enumerate(periodes):
                                %try:
                                    <% p = periodes_m[i] %>
                                    <td>${formatLang(fact_potencia[p],digits=3)} kW</td>
                                %except IndexError:
                                    <td></td>
                                %endtry
                            % endfor
                        </tr>
                    %endif
                    <!-- MAXIMETRE -->
                </table>
            </div>
        </div> <!-- END LECTURES -->
        <p class="little">${_(u"El seu consum mitjà diari en el període facturat ha sigut de %s€ que corresponent a %s kWh/dia (%s dies)") % (formatLang(diari_factura_actual_eur), formatLang(diari_factura_actual_kwh), dies_factura or 1)}</p>
        <!-- HISTORIC -->
        <div class="box historic">
            <div class="box-header" >
                <h1>${_(u"HISTORIAL ÚLTIMS 14 MESOS")}</h1>
            </div>
            <div class="chart_consum_container">
                <div class="chart_consum" id="chart_consum_${factura.id}"></div>
            </div>
            <p>${(_(u"El seu consum mitjà diari els últims %.0f mesos (%s dies) ha estat de %s€ que corresponen a %s kWh/dia ")
                  % ((historic_dies*1.0 / 30) , historic_dies or 1.0, formatLang((total_historic_eur * 1.0) / (historic_dies or 1.0)), formatLang((total_historic_kw * 1.0) / (historic_dies or 1.0))))}
                <br />
                ${_(u"El seu consum acumulat el darrer any ha sigut de %s kWh") % formatLang(total_any, digits=0)}
            </p>
        </div>
        <!-- END HISTORIC -->
        <div class="new_page" ></div>
    <!-- END PAGE -->
    </div>
    <!-- LECTURES ACTIVA i GRÀFIC BARRES -->
    <div class="page">
        <!-- DETALLE FACTURA -->
        <div class="box detall_factura">
            <div class="box-header" >
                <h1>${_(u"DETALL FACTURA")}</h1>
            </div>
            <!-- ENERGIA -->
            <p><em>${_(u"Terme d'energia")}</em></p>
            <div class="periodes">
                <table>
                    <tr class="head">
                        <td class="col_periode"><!--Periode--></td>
                        <td class="col_2"><strong>${_(u"Consum")}</strong></td>
                        <td class="col_X"><!--X--></td>
                        <td class="col_4"><strong>${_(u"Preu")}</strong></td>
                        <td class="col_X"><!--X--></td>
                        <td class="col_X"><!--=--></td>
                        <td class="col_total"><strong>${_(u"Total")}</strong></td>
                        <td class="col_peatges"><!--PEATGES--></td>
                        <td class="total col_total_verd">${_(u"Total")}</td>
                    </tr>
                    % for l in sorted(sorted(factura.linies_energia, key=attrgetter('data_desde')), key=attrgetter('name')):
                        <tr>
                            <td class="col_periode"><strong>${l.name}</strong></td>
                            <td class="col_2">${int(l.quantity)} kWh</td>
                            <td class="col_X">X</td>
                            <td class="col_4">${formatLang(l.price_unit_multi, digits=6)} €/kWh</td>
                            <td class="col_X"><!--X--></td>
                            <td class="col_X">=</td>
                            <td class="col_total"><strong>${_(u"%s €") % formatLang(l.price_subtotal)}</strong></td>
                            <td class="col_peatges">(${_(u"Dels quals peatges: %s €") % formatLang(l.atrprice_subtotal)})</td>
                            <td class="total col_total_verd"></td>
                        </tr>
                    % endfor
                    <tr>
                        <td class="col_periode"></td>
                        <td class="col_2"></td>
                        <td class="col_X"></td>
                        <td class="col_4"></td>
                        <td class="col_X"></td>
                        <td class="col_X"></td>
                        <td class="col_total"></td>
                        <td class="col_peatges"></td>
                        <td class="total col_total_verd">${"%s&euro;" % formatLang(factura.total_energia)}</td>
                    </tr>
                </table>
            </div>

            <div class="separator_energia" ></div>
            <p><em>${_(u"Terme de potència")}</em></p>
            <div class="periodes">
                <table>
                    <%
                        fact_messos = 'mes' in polissa.property_unitat_potencia.name
                    %>
                    <tr class="head">
                        <td class="col_periode"><!--Periode--></td>
                        <td class="col_2"><strong>${_(u"Consum")}</strong></td>
                        <td class="col_X"><!--X--></td>
                        <td class="col_4"><strong>${_(u"Preu")}</strong></td>
                        <td class="col_X"><!--X--></td>
                        % if fact_messos:
                            <td class="col_dies"><strong>${_(u"Mesos")}</strong></td>
                        %else:
                            <td class="col_dies"><strong>${_(u"Dies")}</strong></td>
                        %endif

                        <td class="col_X"><!--=--></td>
                        <td class="col_total">${_(u"Total")}</td>
                        <td class="col_peatges"><!--PEATGES--></td>
                        <td class="total col_total_verd"></td>
                    </tr>
                    % for l in sorted(sorted(factura.linies_potencia, key=attrgetter('data_desde')), key=attrgetter('name')):
                        <tr>
                            <td class="col_periode"><strong>${l.name}</strong></td>
                            <td class="col_2">${formatLang(l.quantity, digits=3)} kW</td>
                            <td class="col_X">X</td>
                            %if fact_messos:
                                <td class="col_4">${formatLang(l.price_unit_multi, digits=6)} ${_("€/kW mes")}</td>
                            %else:
                                <td class="col_4">${formatLang(l.price_unit_multi, digits=6)} ${_("€/kW dia")}</td>
                            %endif
                            <td class="col_X">X</td>
                            <td class="col_dies">${l.multi}</td>
                            <td class="col_X">=</td>
                            <td class="col_total"><strong>${_(u"%s €") % formatLang(l.price_subtotal)}</strong></td>
                            <td class="col_peatges">(${_(u"Dels quals peatges: %s €") % formatLang(l.atrprice_subtotal)})</td>
                            <td class="total col_total_verd"></td>
                        </tr>
                    % endfor
                    <tr>
                        <td class="col_periode"></td>
                        <td class="col_2"></td>
                        <td class="col_X"></td>
                        <td class="col_4"></td>
                        <td class="col_X"></td>
                        <td class="col_dies"></td>
                        <td class="col_X"></td>
                        <td class="col_total"></td>
                        <td class="col_peatges"></td>
                        <td class="total col_total_verd">${"%s&euro;" % formatLang(factura.total_potencia)}</td>
                    </tr>
                </table>
            </div>
            % if factura.total_reactiva:
                <!-- REACTIVA -->
                <div class="separator_energia" ></div>
                <p><em>${_(u"Energia reactiva")}</em></p>
                <div class="periodes">
                    <table>
                        <tr class="head">
                            <td class="col_periode"><!--Periode--></td>
                            <td class="col_2"><strong>${_(u"Excés")}</strong></td>
                            <td class="col_X"><!--X--></td>
                            <td class="col_4"><strong>${_(u"Preu")}</strong></td>
                            <td class="col_X"><!--X--></td>
                            <td class="col_dies"><!--DIAS--></td>
                            <td class="col_X"><!--=--></td>
                            <td class="col_total">${_(u"Total")}</td>
                            <td class="col_peatges"><!--PEATGES--></td>
                            <td class="total col_total_verd"></td>
                        </tr>
                        % for l in sorted(sorted(factura.linies_reactiva, key=attrgetter('data_desde')), key=attrgetter('name')):
                            <tr>
                                <td class="col_periode"><strong>${l.name}</strong></td>
                                <td class="col_2">${formatLang(l.quantity)} kVArh</td>
                                <td class="col_X">X</td>
                                <td class="col_4">${formatLang(l.price_unit_multi, digits=6)} €/kVArh</td>
                                <td class="col_X"><!--X--></td>
                                <td class="col_dies"><!--DIAS --></td>
                                <td class="col_X">=</td>
                                <td class="col_total"><strong>${_(u"%s €") % formatLang(l.price_subtotal)}</strong></td>
                                <td class="col_peatges"></td>
                                <td class="total col_total_verd"></td>
                            </tr>
                        % endfor
                        <tr>
                            <td class="col_periode"></td>
                            <td class="col_2"></td>
                            <td class="col_X"></td>
                            <td class="col_4"></td>
                            <td class="col_X"></td>
                            <td class="col_dies"></td>
                            <td class="col_X"></td>
                            <td class="col_total"></td>
                            <td class="col_peatges"></td>
                            <td class="total col_total_verd">${"%s&euro;" % formatLang(factura.total_reactiva)}</td>
                        </tr>
                    </table>
                </div>
            % endif
            % if len(exces_potencia) :
                <!-- EXCES POTENCIA -->
                <div class="separator_energia" ></div>
                <p><em>${_(u"Excés potència")}</em></p>
                <div class="periodes">
                    <table>
                        <tr class="head">
                            <td class="col_periode"><!--Periode--></td>
                            <td class="col_2"><strong>${_(u"Excés")}</strong></td>
                            <td class="col_X"><!--X--></td>
                            <td class="col_4"><strong>${_(u"Preu")}</strong></td>
                            <td class="col_X"><!--X--></td>
                            <td class="col_dies"><!--DIAS--></td>
                            <td class="col_X"><!--=--></td>
                            <td class="col_total">${_(u"Total")}</td>
                            <td class="col_peatges"><!--PEATGES--></td>
                            <td class="total col_total_verd"></td>
                        </tr>
                        <% total_exces = 0.0 %>
                        % for l in sorted(sorted(exces_potencia, key=attrgetter('data_desde')), key=attrgetter('name')):
                            <tr>
                                <td class="col_periode"><strong>${l.name}</strong></td>
                                <td class="col_2">${formatLang(l.quantity)} kVArh</td>
                                <td class="col_X">X</td>
                                <td class="col_4">${formatLang(l.price_unit_multi, digits=6)} €/kVArh</td>
                                <td class="col_X"><!--X--></td>
                                <td class="col_dies"><!--DIAS --></td>
                                <td class="col_X">=</td>
                                <td class="col_total"><strong>${_(u"%s €") % formatLang(l.price_subtotal)}</strong></td>
                                <td class="col_peatges"></td>
                                <td class="total col_total_verd"></td>
                            </tr>
                            <% total_exces += l.price_subtotal %>
                        % endfor
                        <tr>
                            <td class="col_periode"></td>
                            <td class="col_2"></td>
                            <td class="col_X"></td>
                            <td class="col_4"></td>
                            <td class="col_X"></td>
                            <td class="col_dies"></td>
                            <td class="col_X"></td>
                            <td class="col_total"></td>
                            <td class="col_peatges"></td>
                            <td class="total col_total_verd">${"%s&euro;" % formatLang(total_exces)}</td>
                        </tr>
                    </table>
                </div>
            % endif
            <div class="separator_energia big" ></div>
            <table>
            % for l in iese_lines:
                <tr>
                    <td class="col_impostos"><em>${_(u"Impost de l'electricitat")}</em></td>
                    <td class="col_2">${_(u"%s €") % (formatLang(l.base_amount))}</td>
                    <td class="col_X">X</td>
                    <td class="col_valor_impost">${u"5,11269632%"}</td>
                    <td class="col_X">=</td>
                    <td class="col_total"></td>
                    <td class="col_peatges"></td>
                    <td class="total col_total_verd">${_(u"%s €") % formatLang(l.tax_amount)}</td>
                </tr>
            % endfor
            % for l in lloguer_lines:
                <tr>
                    <td class="col_impostos"><em>${_(u"Lloguer de comptador")}</em></td>
                    <td class="col_2"></td>
                    <td class="col_X"></td>
                    <td class="col_valor_impost"></td>
                    <td class="col_X">=</td>
                    <td class="col_total"></td>
                    <td class="col_peatges"></td>
                    <td class="total col_total_verd">${_(u"%s €") % formatLang(l.price_subtotal)}</td>
                </tr>
            % endfor
            % for l in altres_lines:
                <tr>
                    <td class="col_impostos" colspan="4"><em>${l.name}</em></td>
                    <td class="col_X">=</td>
                    <td class="col_total"></td>
                    <td class="col_peatges"></td>
                    <td class="total col_total_verd">${_(u"%s €") % formatLang(l.price_subtotal)}</td>
                </tr>
            % endfor
            </table>
            <div class="separator_energia big" ></div>
            <table>
            % for l in iva_lines:
                <tr>
                    <td class="col_impostos"><em>${l.name}</em></td>
                    <td class="col_2">${_(u"%s €") % (formatLang(l.base))}</td>
                    <td class="col_X">X</td>
                    <td class="col_valor_impost">${l.name.replace('IVA','').strip()}</td>
                    <td class="col_X">=</td>
                    <td class="col_total"></td>
                    <td class="col_peatges"></td>
                    <td class="total col_total_verd">${_(u"%s €") % formatLang(l.amount)}</td>
                </tr>
            % endfor
            </table>
            <div class="separator_energia final" ></div>
            <table class="final">
                <tr>
                    <td class="total_factura"><em>${_(u"TOTAL FACTURA")}</em></td>
                    <td class="total col_total_verd">${_(u"%s&euro;") % formatLang(factura.amount_total)}</td>
                </tr>
            </table>
        </div>
        <!-- END DETALL -->
        <p class="preus_publicats">
           ${_(u"Els preus dels termes de peatge d’accés són els publicats a ORDEN TEC/1366/2018")}<br />
           ${_(u"Els preus del lloguer dels comptadors són els establerts a ORDEN IET/1491/2013")}<br />
        </p>
        <!-- DESTI -->
        <div class="destination">
            <h1 class="seccio">${_(u"DESTÍ DE LA FACTURA")}</h1>
            <p>${_(u"El destí de l'import de la seva factura, %s euros, és el següent:") % formatLang(factura.amount_total)}</p>
            <div class="chart_desti" id="chart_desti_${factura.id}"></div>
            <p class="lloguers">${_(u"Als imports indicats en el diagrama se'ls ha d'afegir, en el seu cas, el lloguer dels equips de mesura i control")}</p>
        </div>
        <!-- END DESTI -->
        <!-- ORIGEN I MESCLA -->
        <div>
            <div class="left-50 m_b">
                <%
                    diagrama_circular = banner_obj.get_banner(
                        cursor, uid, 'giscedata.facturacio.factura',
                        factura.date_invoice, code='{}_circular'.format(factura_lang)
                    )
                    diagrama_radioactivo = banner_obj.get_banner(
                        cursor, uid, 'giscedata.facturacio.factura',
                        factura.date_invoice, code='{}_Radiactivo'.format(factura_lang)
                    )
                    diagrama_co2 = banner_obj.get_banner(
                        cursor, uid, 'giscedata.facturacio.factura',
                        factura.date_invoice, code='{}_CO2'.format(factura_lang)
                    )
                %>
                <div class="environment_impact">
                    <h1 class="seccio">${_(u"IMPACTE MEDIAMBIENTAL")}</h1>
                    <div class="env_imp">
                        <div class="left-50 env_imp_carbo" >
                            <div class="env_titol"><p>${_(u"Emissions de CO2")}</div>
                            <div class="env_img">
                                <img class="a_g" src="data:image/jpeg;base64,${diagrama_co2}"/>
                            </div>
                        </div>
                        <div class="right-50 env_imp_nuclear" >
                            <div class="env_titol"><p>${_(u"Residus radioactius")}</div>
                            <div class="env_img">
                                <img class="a_g" src="data:image/jpeg;base64,${diagrama_radioactivo}"/>
                            </div>
                        </div>
                        <div>
                            <div class="env_img">
                                <img style="height: 48%; position: relative; left: 45px;" src="data:image/jpeg;base64,${diagrama_circular}"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="right-50 m_b">
                <div class="environment_impact">
                    % if has_setu35_line(cursor, uid ,pool, factura):
                        <style>
                            #report_sups{
                                position: relative;
                                float: left;
                                width: 355px;
                                height: 225px;
                                font-size: 6px;
                                overflow:hidden;
                            }
                            table.supl, table.supl th, table.supl td{
                                padding:1px !important;
                                font-size: 6px !important;
                            }
                        </style>
                        <div class="no_float">
                            <%
                                cups = polissa_obj.read(cursor, uid, factura.polissa_id.id, ['cups'])['cups'][1]
                            %>
                            <div id="report_sups">
                                <p>${_("Tabla detallada Suplementos Autonómicos 2013 (*)")}</p>
                                ${sup_territorials_2013_comer_obj.get_info_html(cursor, uid, cups)}
                                <p>${_("En caso de que el importe total de la regularización supere los dos euros, sin incluir impuestos, el mismo será fraccionado en partes iguales por las empresas comercializadoras en las facturas que se emitan en el plazo de 12 cuotas a partir de la primera regularización. En caso de facturación bimestral, se recibirá en 6 cuotas.")}</p>
                                <p>${_("* Tabla informativa conforme a lo establecido en la ETU/35/2017 de 23 de enero, por la cual le informamos de los parámetros para el cálculo de los suplementos territoriales facilitados por su empresa distribuidora")} ${polissa.distribuidora.name}.</p>
                            </div>
                        </div>
                    % else:
                        <h1 class="seccio">${_(u"MESCLA DE PRODUCCIÓ 2018")}</h1>
                        <div class="mix_taula">
                            <table>
                                <thead>
                                    <tr>
                                        <th>${_("TECNOLOGIES")}</th>
                                        <th style="width: 30%">${_("Comercializadora Lersa S.L")}</th>
                                        <th style="width: 40%">${_("Mescla sistema elèctric espanyol")}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>${_("Renovable")}</td><td>100%</td><td>38,2%</td>
                                </tr>
                                <tr>
                                    <td>${_("Cogeneració alta eficiència")}</td><td>0%</td><td>4,4%</td>
                                </tr>
                                <tr>
                                    <td>${_("Cogeneració")}</td><td>0%</td><td>6,9%</td>
                                </tr>
                                <tr>
                                    <td>${_("CC Gas Natural")}</td><td>0%</td><td>11,7%</td>
                                </tr>
                                <tr>
                                    <td>${_("Carbó")}</td><td>0%</td><td>14,5%</td>
                                </tr>
                                <tr>
                                    <td>${_("Fuel/Gas")}</td><td>0%</td><td>2,6%</td>
                                </tr>
                                <tr>
                                    <td>${_("Nuclear")}</td><td>0%</td><td>20,7%</td>
                                </tr>
                                <tr>
                                    <td>${_("Altres")}</td><td>0%</td><td>1,0%</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    % endif
                </div>
            </div>
        </div>
        <div class="separator"></div>
        <!-- AVERIES -->
        <div class="peu">
            ${emergency_complaints(factura)}
        </div>
        <!-- END AVERIES -->
    <!-- END PAGE -->
    </div>
</div>
<script>
var pie_total = ${pie_total};
var pie_data = [{val: ${pie_regulats}, perc: 30, code: "REG"},
                {val: ${pie_costos}, perc: 55, code: "PROD"},
                {val: ${pie_impostos},  perc: 15 ,code: "IMP"}
               ];

var pie_etiquetes = {'REG': {t: ['${formatLang(float(pie_regulats))}€','${_(u"Costos regulats")}'], w:100},
                     'IMP': {t: ['${formatLang(float(pie_impostos))}€','${_(u"Impostos aplicats")}'], w:100},
                     'PROD': {t: ['${formatLang(float(pie_costos))}€','${_(u"Costos de producció electricitat")}'], w:150}
                    };

var reparto = ${json.dumps(reparto)}
var dades_reparto = ${json.dumps(dades_reparto)}

var factura_id = ${factura.id}
var data_consum = ${json.dumps(sorted(historic_js, key=lambda h: h['mes']))}
var es30 = ${len(periodes_a)>3 and 'true' or 'false'}

</script>
<script src="${addons_path}/giscedata_facturacio_comer_lersa/report/consum.js"></script>
<script src="${addons_path}/giscedata_facturacio_comer_lersa/report/pie.js"></script>
</div>
<%
comptador_factures += 1;
%>
% if comptador_factures<len(objects):
    <p style="page-break-after:always"></p>
% endif
%endfor
</body>
</html>
