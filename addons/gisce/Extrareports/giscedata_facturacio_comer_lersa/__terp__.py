# -*- coding: utf-8 -*-
{
    "name": "Reports Facturació LERSA (Comercialitzadora)",
    "description": """Reports Facturació LERSA (Comercialitzadora)""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Extrareports",
    "depends":[
        "base",
        "giscedata_facturacio_comer",
        "giscedata_polissa_comer",
        "c2c_webkit_report",
        "jasper_reports"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_facturacio_comer_data.xml",
        "giscedata_facturacio_comer_report.xml"
    ],
    "active": False,
    "installable": True
}
