# Translation of OpenERP Server.
# This file contains the translation of the following modules:
# * giscedata_facturacio_comer_lersa
# 
# Translators:
#   <>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: GISCE-ERP\n"
"Report-Msgid-Bugs-To: https://github.com/gisce/erp/issues\n"
"POT-Creation-Date: 2019-11-14 12:34+0000\n"
"PO-Revision-Date: 2019-12-04 12:31+0000\n"
"Last-Translator: destanyol <>\n"
"Language-Team: Spanish (Spain) <erp@dev.gisce.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Generated-By: Babel 2.5.3\n"
"Language: es_ES\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. module: giscedata_facturacio_comer_lersa
#: model:ir.module.module,description:giscedata_facturacio_comer_lersa.module_meta_information
#: model:ir.module.module,shortdesc:giscedata_facturacio_comer_lersa.module_meta_information
msgid "Reports Facturació LERSA (Comercialitzadora)"
msgstr "Reports Facturación LERSA (Comercializadora)"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:72
#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:73
#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:106
msgid "real"
msgstr "real"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:108
msgid "estimada"
msgstr "estimada"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:272
msgid "facturació per maxímetre"
msgstr "facturación por maxímetro"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:273
msgid "facturació per ICP"
msgstr "facturación por ICP"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:357
msgid "Incentius a les energies renovables, cogeneració i residus"
msgstr "Incentivos a las energías renovables, cogeneración y residuos"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:358
msgid "Cost de xarxes de distribució i transport"
msgstr "Costes de redes de distribución y transporte"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:359
msgid "Altres Costos regulats (inclosa anualitat del dèficit)"
msgstr "Otros Costes regulados (incluida anualidad del déficit)"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:365
msgid "AVARIES I URGÈNCIES"
msgstr "AVERÍAS Y URGENCIAS "

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:367
msgid "Empresa distribuïdora"
msgstr "Empresa distribuidora"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:369
#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:380
msgid "Telèfon gratuït"
msgstr "Teléfono gratuito"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:371
#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:514
msgid "Contracte d'accés"
msgstr "Contrato de acceso"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:376
msgid "RECLAMACIONS"
msgstr "RECLAMACIONES"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:378
msgid "Empresa comercialitzadora"
msgstr "Empresa comercializadora"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:382
msgid "Email"
msgstr "Email"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:387
msgid ""
"Si no està d'acord amb la nostra resposta a la seva reclamació, pot reclamar"
" a l'organisme administratiu competent: INFORMEU-VOS AL 012 (teléfon "
"d'Atenció Ciutadana)"
msgstr "Si no está de acuerdo con nuestra respuesta a su reclamación, puede reclamar al organismo administrativo competente: Para más información llame al 012 (teléfono de Atención Ciudadana)"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:405
msgid "CIF:"
msgstr "CIF:"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:455
#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:565
msgid "FACTURA"
msgstr "FACTURA"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:458
#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:570
msgid "Número de factura"
msgstr "Número de factura"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:460
msgid "Aquesta factura anul·la la"
msgstr "Esta factura anula la"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:464
msgid "Aquesta factura rectifica la"
msgstr "Esta factura rectifica la"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:466
msgid "Data Emissió"
msgstr "Fecha emisión"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:467
#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:572
msgid "Període de facturació"
msgstr "Periodo de facturación"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:467
#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:572
#, python-format
msgid "de %s a %s"
msgstr "de %s a %s"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:468
#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:573
msgid "Dies facturats"
msgstr "Días facturados"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:469
#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:574
msgid "Pagador"
msgstr "Pagador"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:470
#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:575
msgid "NIF"
msgstr "NIF"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:472
#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:577
msgid "Entitat"
msgstr "Entidad"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:473
#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:578
msgid "IBAN"
msgstr "IBAN"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:479
msgid "RESUM FACTURA"
msgstr "RESUMEN FACTURA"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:482
#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:586
msgid "Per energia"
msgstr "Por energía"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:483
#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:587
msgid "Per potència"
msgstr "Por potencia"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:485
#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:589
msgid "Penalització per energia reactiva"
msgstr "Penalización por energía reactiva"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:487
#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:591
msgid "Impost electricitat"
msgstr "Impuesto electricidad"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:488
#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:592
msgid "Lloguer comptador"
msgstr "Alquiler contador"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:490
#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:594
msgid "Altres conceptes"
msgstr "Otros conceptos"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:497
#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:601
msgid "TOTAL"
msgstr "TOTAL"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:504
msgid "CONTRACTE"
msgstr "CONTRATO"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:507
msgid "Titular"
msgstr "Titular"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:508
msgid "Nom/Raó social"
msgstr "Nombre/Razón social"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:509
msgid "NIF/CIF"
msgstr "NIF/CIF"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:513
msgid "Contracte del <br> suministrament"
msgstr "Contrato del <br> suministro"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:515
msgid "Producte Comer"
msgstr "Producto Comer"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:516
msgid "Distribuïdora"
msgstr "Distribuidora"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:517
msgid "Data alta"
msgstr "Fecha alta"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:518
msgid "Data final"
msgstr "Fecha final"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:518
msgid "(renovació anual automàtica)"
msgstr "(renovación anual automática)"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:519
msgid "CNAE"
msgstr "CNAE"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:520
msgid "CUPS"
msgstr "CUPS"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:521
msgid "Adreça de subministrament"
msgstr "Dirección de suministro"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:528
#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:753
msgid "Potència contractada"
msgstr "Potencia contratada"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:530
msgid " (Maxímetre)"
msgstr " (Maxímetro)"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:532
msgid " (ICP)"
msgstr " (ICP)"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:571
msgid "Data"
msgstr "Fecha"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:613
msgid "CONSUM ELÈCTRIC"
msgstr "CONSUMO ELÉCTRICO"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:619
msgid "ENERGIA"
msgstr "ENERGÍA"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:631
#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:692
msgid "Número de comptador"
msgstr "Número de contador"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:642
#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:703
msgid "Lectura anterior"
msgstr "Lectura anterior"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:653
#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:714
msgid "Lectura actual"
msgstr "Lectura actual"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:665
#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:726
msgid "Total període"
msgstr "Total período"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:680
msgid "ENERGIA REACTIVA"
msgstr "ENERGÍA REACTIVA"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:742
msgid "MAXÍMETRE"
msgstr "MAXÍMETRO"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:764
msgid "Lectura maxímetre"
msgstr "Lectura maxímetro"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:775
msgid "Potència facturada període"
msgstr "Potencia facturada período"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:790
#, python-format
msgid ""
"El seu consum mitjà diari en el període facturat ha sigut de %s€ que "
"corresponent a %s kWh/dia (%s dies)"
msgstr "Su consumo medio diario en el periodo facturado ha sido de %s€ que corresponde a %s kWh/día (%s días)"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:794
msgid "HISTORIAL ÚLTIMS 14 MESOS"
msgstr "HISTORIAL ÚLTIMOS 14 MESES"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:799
#, python-format
msgid ""
"El seu consum mitjà diari els últims %.0f mesos (%s dies) ha estat de %s€ "
"que corresponen a %s kWh/dia "
msgstr "Su consumo medio diario los últimos %.0f meses (%s días) ha sido de %s€ que corresponden a %s kWh/día "

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:802
#, python-format
msgid "El seu consum acumulat el darrer any ha sigut de %s kWh"
msgstr "Su consumo acumulado el último año ha sido de %s kWh"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:814
msgid "DETALL FACTURA"
msgstr "DETALLE FACTURA"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:817
msgid "Terme d'energia"
msgstr "Término de energía"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:822
#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:867
msgid "Consum"
msgstr "Consumo"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:824
#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:869
#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:924
#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:971
msgid "Preu"
msgstr "Precio"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:827
#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:829
#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:878
#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:928
#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:975
msgid "Total"
msgstr "Total"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:839
#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:895
#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:941
#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:989
#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:1015
#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:1021
#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:1033
#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:1042
#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:1051
#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:1057
#, python-format
msgid "%s €"
msgstr "%s €"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:840
#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:896
#, python-format
msgid "Dels quals peatges: %s €"
msgstr "De los cuales peajes: %s €"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:859
msgid "Terme de potència"
msgstr "Término de potencia"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:872
msgid "Mesos"
msgstr "Meses"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:874
msgid "Dies"
msgstr "Días"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:888
msgid "€/kW mes"
msgstr "€/kW mes"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:890
msgid "€/kW dia"
msgstr "€/kW día"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:917
msgid "Energia reactiva"
msgstr "Energía reactiva"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:922
#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:969
msgid "Excés"
msgstr "Exceso"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:964
msgid "Excés potència"
msgstr "Exceso potencia"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:1014
msgid "Impost de l'electricitat"
msgstr "Impuesto de electricidad"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:1026
msgid "Lloguer de comptador"
msgstr "Alquiler de contador"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:1064
msgid "TOTAL FACTURA"
msgstr "TOTAL FACTURA"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:1065
#, python-format
msgid "%s&euro;"
msgstr "%s&euro;"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:1071
msgid ""
"Els preus dels termes de peatge d’accés són els publicats a ORDEN "
"TEC/1366/2018"
msgstr "Los precios de los términos de peaje de acceso son los publicados en la ORDEN TEC/1366/2018"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:1072
msgid ""
"Els preus del lloguer dels comptadors són els establerts a ORDEN "
"IET/1491/2013"
msgstr "Los precios del alquiler de los contadores son los establecidos en la ORDEN IET/1491/2013"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:1076
msgid "DESTÍ DE LA FACTURA"
msgstr "DESTINO DE LA FACTURA"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:1077
#, python-format
msgid "El destí de l'import de la seva factura, %s euros, és el següent:"
msgstr "El destino del importe de su factura, %s euros, es la siguiente:"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:1079
msgid ""
"Als imports indicats en el diagrama se'ls ha d'afegir, en el seu cas, el "
"lloguer dels equips de mesura i control"
msgstr "A los importes indicados en el diagrama se debe añadir, en su caso, el alquiler de los equipos de medida y control"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:1100
msgid "IMPACTE MEDIAMBIENTAL"
msgstr "IMPACTO MEDIOAMBIENTAL"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:1103
msgid "Emissions de CO2"
msgstr "Emisiones de CO2"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:1109
msgid "Residus radioactius"
msgstr "Residuos radioactivos"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:1144
msgid "Tabla detallada Suplementos Autonómicos 2013 (*)"
msgstr "Tabla detallada Suplementos Autonómicos 2013 (*)"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:1146
msgid ""
"En caso de que el importe total de la regularización supere los dos euros, "
"sin incluir impuestos, el mismo será fraccionado en partes iguales por las "
"empresas comercializadoras en las facturas que se emitan en el plazo de 12 "
"cuotas a partir de la primera regularización. En caso de facturación "
"bimestral, se recibirá en 6 cuotas."
msgstr "En caso de que el importe total de la regularización supere los dos euros, sin incluir impuestos, el mismo será fraccionado en partes iguales por las empresas comercializadoras en las facturas que se emitan en el plazo de 12 cuotas a partir de la primera regularización. En caso de facturación bimestral, se recibirá en 6 cuotas."

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:1147
msgid ""
"* Tabla informativa conforme a lo establecido en la ETU/35/2017 de 23 de "
"enero, por la cual le informamos de los parámetros para el cálculo de los "
"suplementos territoriales facilitados por su empresa distribuidora"
msgstr "* Tabla informativa conforme a lo establecido en la ETU/35/2017 de 23 de enero, por la cual le informamos de los parámetros para el cálculo de los suplementos territoriales facilitados por su empresa distribuidora"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:1151
msgid "MESCLA DE PRODUCCIÓ 2018"
msgstr "MEZCLA DE PRODUCCIÓN 2018"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:1156
msgid "TECNOLOGIES"
msgstr "TECNOLOGÍAS"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:1157
msgid "Comercializadora Lersa S.L"
msgstr "Comercializadora Lersa S.L"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:1158
msgid "Mescla sistema elèctric espanyol"
msgstr "Mezcla sistema eléctrico español"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:1163
msgid "Renovable"
msgstr "Renovable"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:1166
msgid "Cogeneració alta eficiència"
msgstr "Cogeneración alta eficiencia"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:1169
msgid "Cogeneració"
msgstr "Cogeneración"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:1172
msgid "CC Gas Natural"
msgstr "CC Gas Natural"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:1175
msgid "Carbó"
msgstr "Carbón"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:1178
msgid "Fuel/Gas"
msgstr "Fuel/Gas"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:1181
msgid "Nuclear"
msgstr "Nuclear"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:1184
msgid "Altres"
msgstr "Otros"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:1209
msgid "Costos regulats"
msgstr "Costes regulados"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:1210
msgid "Impostos aplicats"
msgstr "Impuestos aplicados"

#: rml:giscedata_facturacio_comer_lersa/report/report_giscedata_facturacio_factura_comer.mako:1211
msgid "Costos de producció electricitat"
msgstr "Costes de producción electricidad"
