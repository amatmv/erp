import logging


def up(cursor, installed_version):
    logging.info("Deleting duplicated factura header...")

    query = """
        SELECT id, res_id
        FROM ir_model_data
        WHERE
            name = 'factura_lersa_webkit_header'
            AND model = 'ir.header_webkit'
    """
    cursor.execute(query)
    res = cursor.dictfetchall()

    ir_header_webkit_id = res[0]['res_id']
    ir_model_data_id = res[0]['id']

    query = "DELETE FROM ir_header_webkit WHERE id = %s"
    cursor.execute(query, (ir_header_webkit_id,))

    query = "DELETE FROM ir_model_data WHERE id = %s"
    cursor.execute(query, (ir_model_data_id,))

    logging.info("Duplicated factura header deleted.")


def down(cursor, installed_version):
    pass


migrate = up
