# -*- coding: utf-8 -*-

from osv import osv, fields


class poweremail_send_wizard(osv.osv_memory):
    _name = 'poweremail.send.wizard'
    _inherit = 'poweremail.send.wizard'

    def save_to_mailbox(self, cr, uid, ids, context=None):
        """Pel cas particular de Lersa, afegim la clau poweremail al context
           per informar al jasper que ens cal afegir la plantilla a la factura.
        """
        if context is None:
            context = {}
        ctx = context.copy()
        ctx['poweremail'] = True
        return super(poweremail_send_wizard, self).\
                        save_to_mailbox(cr, uid, ids, ctx)

poweremail_send_wizard()
