<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="descarrec_list" />
  </xsl:template>
  
  <xsl:template match="descarrec_list">
    <xsl:apply-templates select="descarrec" />
  </xsl:template>

  <xsl:template name="monthname">
    <xsl:param name="number"/>
    <xsl:choose>
        <xsl:when test="$number = '01'">enero</xsl:when>
        <xsl:when test="$number = '02'">febrero</xsl:when>
        <xsl:when test="$number = '03'">marzo</xsl:when>
        <xsl:when test="$number = '04'">abril</xsl:when>
        <xsl:when test="$number = '05'">mayo</xsl:when>
        <xsl:when test="$number = '06'">junio</xsl:when>
        <xsl:when test="$number = '07'">julio</xsl:when>
        <xsl:when test="$number = '08'">agosto</xsl:when>
        <xsl:when test="$number = '09'">septiembre</xsl:when>
        <xsl:when test="$number = '10'">octubre</xsl:when>
        <xsl:when test="$number = '11'">noviembre</xsl:when>
        <xsl:when test="$number = '12'">diciembre</xsl:when>
    </xsl:choose>
  </xsl:template>
  <xsl:template match="descarrec">
    <document>
      <template pageSize="(29.7cm, 21cm)">
      	<pageTemplate id="main">
      		<frame id="first" x1="1.8cm" y1="1cm" width="12.7cm" height="190mm"/>
      		<frame id="second" x1="14.5cm" y1="1cm" width="12.7cm" height="190mm"/>
      	</pageTemplate>
      </template>
      
      <stylesheet>
      <paraStyle name="hdr1"
        fontName="Times-Bold"
        fontSize="30"
        leading="30"/>

      <paraStyle name="hdr2"
        fontName="Times-Bold"
        fontSize="22"
        leading="22"/>

      <paraStyle name="hdr3"
        fontName="Times-Roman"
        fontSize="18"
        leading="18"/>

      <paraStyle name="text"
      		fontName="Times-Italic"
      		fontSize="16"
            leading="16"/>

      <paraStyle name="date_hdr"
        fontName="Times-Bold"
        fontSize="16"
        leading="16"/>

      <paraStyle name="fecha"
        fontName="Times-Italic"
        fontSize="20"
        leading="20"/>

      <paraStyle name="hora"
        fontName="Times-Italic"
        fontSize="20"
        leading="20"/>

      <blockTableStyle id="taula1">
        <lineStyle kind="BOX" start="0,0" stop="0,0" thickness="1" colorName="black"/>
      </blockTableStyle>
      </stylesheet>
    
      <story>
        <para style="hdr1" alignment="center">ELECTRA ADURIZ, SA</para>
        <spacer length="1cm"/>
        <para style="hdr2" alignment="center">AVISO DE CORTE</para>
        <para style="hdr2" alignment="center">DE SUMINISTRO EL�CTRICO</para>
        <spacer length="0.5cm"/>
        <para style="text">Con el fin de realizar trabajos en la red de distribuci�n se cortar� el suministro el�ctrico a este edificio</para>
        <spacer length="0.5cm"/>
        <para style="date_hdr">el d�a:</para>
        <spacer length="0.2cm"/>
        <blockTable colWidths="12cm" style="taula1">
            <tr>
                <td><para style="fecha" alignment="center">
                    <xsl:value-of select="concat(substring(data_inici, 9, 2),' de ')"/>
                    <xsl:call-template name="monthname">
                        <xsl:with-param name="number"><xsl:value-of select="substring(data_inici, 6, 2)"/></xsl:with-param>
                    </xsl:call-template>
                    <xsl:value-of select="concat(' de ',substring(data_inici, 1, 4))" /> 
                </para></td>
            </tr>
        </blockTable>
        <spacer length="0.2cm"/>
        <para style="date_hdr">horario:</para>
        <spacer length="0.2cm"/>
        <blockTable colWidths="12cm" style="taula1">
            <tr>
                <td><para style="hora" alignment="center">de las <xsl:value-of select="substring(data_inici, 12, 5)"/> a las <xsl:value-of select="substring(data_final, 12, 5)"/> horas</para></td>
            </tr>
        </blockTable>
        <spacer length="0.5cm"/>
        <para style="text">En caso de terminar los trabajos antes de la hora prevista se restablecer� el servicio sin previo aviso.</para>
        <spacer length="0.5cm"/>
        <para style="text">Les rogamos disculpen las molestias</para>
        <spacer length="0.5cm"/>
        <blockTable colWidths="6.7cm,6cm">
            <tr>
                <td>Algorta, 10</td>
                <td/>
            </tr>
            <tr>
                <td>MEDINA DE POMAR</td>
                <td><para alignment="right">TFNO. 947.190.480</para></td>
            </tr>
        </blockTable>
        <nextFrame/>
        <para style="hdr1" alignment="center">ELECTRA ADURIZ, SA</para>
        <spacer length="1cm"/>
        <para style="hdr2" alignment="center">AVISO DE CORTE</para>
        <para style="hdr2" alignment="center">DE SUMINISTRO EL�CTRICO</para>
        <spacer length="0.5cm"/>
        <para style="text">Con el fin de realizar trabajos en la red de distribuci�n se cortar� el suministro el�ctrico a este edificio</para>
        <spacer length="0.5cm"/>
        <para style="date_hdr">el d�a:</para>
        <spacer length="0.2cm"/>
        <blockTable colWidths="12cm" style="taula1">
            <tr>
                <td><para style="fecha" alignment="center">
                    <xsl:value-of select="concat(substring(data_inici, 9, 2),' de ')"/>
                    <xsl:call-template name="monthname">
                        <xsl:with-param name="number"><xsl:value-of select="substring(data_inici, 6, 2)"/></xsl:with-param>
                    </xsl:call-template>
                    <xsl:value-of select="concat(' de ',substring(data_inici, 1, 4))" /> 
                </para></td>
            </tr>
        </blockTable>
        <spacer length="0.2cm"/>
        <para style="date_hdr">horario:</para>
        <spacer length="0.2cm"/>
        <blockTable colWidths="12cm" style="taula1">
            <tr>
                <td><para style="hora" alignment="center">de las <xsl:value-of select="substring(data_inici, 12, 5)"/> a las <xsl:value-of select="substring(data_final, 12, 5)"/> horas</para></td>
            </tr>
        </blockTable>
        <spacer length="0.5cm"/>
        <para style="text">En caso de terminar los trabajos antes de la hora prevista se restablecer� el servicio sin previo aviso.</para>
        <spacer length="0.5cm"/>
        <para style="text">Les rogamos disculpen las molestias</para>
        <spacer length="0.5cm"/>
        <blockTable colWidths="6.7cm,6cm">
            <tr>
                <td>Algorta, 10</td>
                <td/>
            </tr>
            <tr>
                <td>MEDINA DE POMAR</td>
                <td><para alignment="right">TFNO. 947.190.480</para></td>
            </tr>
        </blockTable>
        <nextPage/>
        <para style="hdr1" alignment="center">ELECTRA ADURIZ, SA</para>
        <spacer length="1cm"/>
        <para style="hdr2" alignment="center">AVISO DE CORTE</para>
        <para style="hdr2" alignment="center">DE SUMINISTRO EL�CTRICO</para>
        <spacer length="0.5cm"/>
        <para style="text">Con el fin de realizar trabajos en la red de distribuci�n se cortar� el suministro el�ctrico a esta localidad</para>
        <spacer length="0.5cm"/>
        <para style="date_hdr">el d�a:</para>
        <spacer length="0.2cm"/>
        <blockTable colWidths="12cm" style="taula1">
            <tr>
                <td><para style="fecha" alignment="center">
                    <xsl:value-of select="concat(substring(data_inici, 9, 2),' de ')"/>
                    <xsl:call-template name="monthname">
                        <xsl:with-param name="number"><xsl:value-of select="substring(data_inici, 6, 2)"/></xsl:with-param>
                    </xsl:call-template>
                    <xsl:value-of select="concat(' de ',substring(data_inici, 1, 4))" /> 
                </para></td>
            </tr>
        </blockTable>
        <spacer length="0.2cm"/>
        <para style="date_hdr">horario:</para>
        <spacer length="0.2cm"/>
        <blockTable colWidths="12cm" style="taula1">
            <tr>
                <td><para style="hora" alignment="center">de las <xsl:value-of select="substring(data_inici, 12, 5)"/> a las <xsl:value-of select="substring(data_final, 12, 5)"/> horas</para></td>
            </tr>
        </blockTable>
        <spacer length="0.5cm"/>
        <para style="text">En caso de terminar los trabajos antes de la hora prevista se restablecer� el servicio sin previo aviso.</para>
        <spacer length="0.5cm"/>
        <para style="text">Les rogamos disculpen las molestias</para>
        <spacer length="0.5cm"/>
        <blockTable colWidths="6.7cm,6cm">
            <tr>
                <td>Algorta, 10</td>
                <td/>
            </tr>
            <tr>
                <td>MEDINA DE POMAR</td>
                <td><para alignment="right">TFNO. 947.190.480</para></td>
            </tr>
        </blockTable>
        <nextFrame/>
        <para style="hdr1" alignment="center">ELECTRA ADURIZ, SA</para>
        <spacer length="1cm"/>
        <para style="hdr2" alignment="center">AVISO DE CORTE</para>
        <para style="hdr2" alignment="center">DE SUMINISTRO EL�CTRICO</para>
        <spacer length="0.5cm"/>
        <para style="text">Con el fin de realizar trabajos en la red de distribuci�n se cortar� el suministro el�ctrico a esta localidad</para>
        <spacer length="0.5cm"/>
        <para style="date_hdr">el d�a:</para>
        <spacer length="0.2cm"/>
        <blockTable colWidths="12cm" style="taula1">
            <tr>
                <td><para style="fecha" alignment="center">
                    <xsl:value-of select="concat(substring(data_inici, 9, 2),' de ')"/>
                    <xsl:call-template name="monthname">
                        <xsl:with-param name="number"><xsl:value-of select="substring(data_inici, 6, 2)"/></xsl:with-param>
                    </xsl:call-template>
                    <xsl:value-of select="concat(' de ',substring(data_inici, 1, 4))" /> 
                </para></td>
            </tr>
        </blockTable>
        <spacer length="0.2cm"/>
        <para style="date_hdr">horario:</para>
        <spacer length="0.2cm"/>
        <blockTable colWidths="12cm" style="taula1">
            <tr>
                <td><para style="hora" alignment="center">de las <xsl:value-of select="substring(data_inici, 12, 5)"/> a las <xsl:value-of select="substring(data_final, 12, 5)"/> horas</para></td>
            </tr>
        </blockTable>
        <spacer length="0.5cm"/>
        <para style="text">En caso de terminar los trabajos antes de la hora prevista se restablecer� el servicio sin previo aviso.</para>
        <spacer length="0.5cm"/>
        <para style="text">Les rogamos disculpen las molestias</para>
        <spacer length="0.5cm"/>
        <blockTable colWidths="6.7cm,6cm">
            <tr>
                <td>Algorta, 10</td>
                <td/>
            </tr>
            <tr>
                <td>MEDINA DE POMAR</td>
                <td><para alignment="right">TFNO. 947.190.480</para></td>
            </tr>
        </blockTable>


      </story>
    </document>

    </xsl:template>

</xsl:stylesheet>
