<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

  <xsl:template match="/">
    <xsl:apply-templates select="solicituds_list"/>
  </xsl:template>

  <xsl:template match="solicituds_list">
    <xsl:apply-templates select="solicitud"/>
  </xsl:template>

  <xsl:template name="repeat">
    <xsl:param name="n"/>
    <xsl:if test="$n &gt; 0">
      <tr>
              <td/>
              <td/>
              <td/>
              <td/>
              <td/>
              <td/>
              <td/>
              <td/>
              <td/>
              <td/>
              <td/>
      </tr>
      <xsl:call-template name="repeat">
        <xsl:with-param name="n" select="$n - 1"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>
  <xsl:template match="bt_cortes">
    <tr>
      <td><xsl:value-of select="num_ct"/></td>
      <td><xsl:value-of select="num_salida"/></td>
      <td><xsl:value-of select="num_acometida"/></td>
      <td><xsl:value-of select="substring-after(corte_previsto, ' ')"/></td>
      <td><xsl:value-of select="substring-before(corte_previsto, ' ')"/></td>
      <td><xsl:value-of select="substring-after(reposicion_previsto, ' ')"/></td>
      <td><xsl:value-of select="substring-before(reposicion_previsto, ' ')"/></td>
    </tr>
  </xsl:template>

  <xsl:template match="solicitud">
    <document>
      <template pageSize="(21cm, 29.7cm)">
        <pageTemplate id="main">
          <frame id="first" x1="2cm" y1="1cm" width="18cm" height="25cm"/>
          <pageGraphics>
            <setFont name="Helvetica" size="8"/>
            <drawString x="1cm" y="1cm">I-27-01 Rev1</drawString>
          </pageGraphics>
        </pageTemplate>
      </template>
      <stylesheet>
        <paraStyle name="p"
          fontName="Helvetica"
          fontSize="8"/>

        <paraStyle name="p6"
          fontName="Helvetica"
          fontSize="6"/>

        <blockTableStyle id="titol">
          <blockFont name="Helvetica-Bold" size="10" start="0,0" stop="-1,-1"/>
          <lineStyle kind="BOX" colorName="black" />
          <blockValign value="MIDDLE"/>
          <blockAlignment value="CENTER"/>
        </blockTableStyle>
        <blockTableStyle id="n_peticion">
          <blockFont name="Helvetica" size="8" start="0,0" stop="-1,-1"/>
          <lineStyle kind="BOX" colorName="black" start="1,0" stop="-1,-1"/>
          <blockBackground colorName="silver" start="2,-1" stop="2,-1"/>
          <blockValign value="MIDDLE"/>
        </blockTableStyle>
        <blockTableStyle id="solicitante">
          <blockFont name="Helvetica" size="8" start="0,0" stop="-1,-1"/>
          <blockValign value="MIDDLE" start="0,0" stop="-1,-1"/>
          <blockFont name="Helvetica-Bold" size="8" start="0,0" stop="0,0"/>
          <lineStyle kind="BOX" colorName="black"/>
          <lineStyle kind="LINEBELOW" colorName="black" start="0,0" stop="-1,-1" />
          <blockBackground colorName="silver" start="1,1" stop="1,1"/>
          <blockBackground colorName="silver" start="1,2" stop="1,2"/>
          <blockBackground colorName="silver" start="3,1" stop="3,1"/>
          <blockBackground colorName="silver" start="3,2" stop="3,2"/>
        </blockTableStyle>
        <blockTableStyle id="d_instalacion">
          <blockFont name="Helvetica" size="8" start="0,0" stop="-1,-1"/>
          <blockValign value="MIDDLE" start="0,0" stop="-1,-1"/>
          <blockValign value="TOP" start="1,0" stop="1,-1"/>
          <blockFont name="Helvetica-Bold" size="8" start="0,0" stop="0,0"/>
          <lineStyle kind="LINEAFTER" colorName="black" start="-1,0" stop="-1,-1"/>
          <lineStyle kind="LINEBEFORE" colorName="black" start="0,0" stop="0,-1"/>
          <lineStyle kind="LINEABOVE" colorName="black" start="0,0" stop="-1,0"/>
          <lineStyle kind="LINEBELOW" colorName="black" start="0,0" stop="-1,0"/>
          <lineStyle kind="LINEBELOW" colorName="black" start="3,1" stop="-1,1"/>
          <lineStyle kind="GRID" colorName="black" start="3,3" stop="-1,-2"/>
        </blockTableStyle>
        <blockTableStyle id="horarios">
          <blockFont name="Helvetica" size="8" start="0,0" stop="-1,-1"/>
          <blockValign value="MIDDLE" start="0,0" stop="-1,-1"/>
          <lineStyle kind="BOX" colorName="black"/>
          <lineStyle kind="GRID" colorName="black" start="0,2" stop="-1,-1"/>
        </blockTableStyle>
        <blockTableStyle id="t_previstos">
          <blockAlignment value="RIGHT" start="0,1" stop="-1,-1"/>
          <blockFont name="Helvetica" size="8" start="0,0" stop="-1,-1"/>
          <blockFont name="Helvetica-Bold" size="8" start="0,0" stop="0,0"/>
          <blockValign value="MIDDLE" start="0,0" stop="-1,-1"/>
          <blockValign value="TOP" start="1,0" stop="1,-1"/>
          <lineStyle kind="BOX" colorName="black"/>
          <lineStyle kind="LINEBELOW" colorName="black" start="0,0" stop="-1,0"/>
        </blockTableStyle>
        <blockTableStyle id="tramitacio">
          <blockFont name="Helvetica" size="8" start="0,0" stop="-1,-1"/>
          <blockValign value="MIDDLE" start="0,0" stop="-1,-1"/>
        </blockTableStyle>
        <blockTableStyle id="s_re">
          <blockFont name="Helvetica" size="8" start="0,0" stop="-1,-1"/>
          <blockValign value="MIDDLE" start="0,0" stop="-1,-1"/>
          <blockAlignment value="RIGHT" start="0,0" stop="0,0"/>
          <lineStyle kind="BOX" colorName="black" start="1,0" stop="-1,-1"/>
          <blockBackground colorName="silver" start="-1,-1" stop="-1,-1"/>
        </blockTableStyle>
        <blockTableStyle id="h_horaris"> 
          <blockFont name="Helvetica" size="8" start="0,0" stop="-1,-1"/>
          <blockValign value="MIDDLE" start="0,0" stop="-1,-1"/>
          <blockFont name="Helvetica-Bold" size="8" start="0,0" stop="0,0"/>
          <blockAlignment value="CENTRE" start="0,1" stop="-1,-1"/>
          <lineStyle kind="GRID" colorName="black" start="0,2" stop="-1,-1"/>
          <lineStyle kind="GRID" colorName="black" start="1,1" stop="-1,1"/>
          <lineStyle kind="BOX" colorName="black" start="0,0" stop="-1,-1"/>
        </blockTableStyle>
        <blockTableStyle id="horaris">
          <blockFont name="Helvetica" size="8" start="0,0" stop="-1,-1"/>
          <blockValign value="MIDDLE" start="0,0" stop="-1,-1"/>
          <blockBackground colorName="silver" start="1,1" stop="-1,5"/>
          <lineStyle kind="GRID" colorName="black" start="0,1" stop="-1,5"/>
          <lineStyle kind="GRID" colorName="black" start="1,6" stop="-1,-1"/>
          <lineStyle kind="LINEBEFORE" colorName="black" start="0,0" stop="-1,0"/>
          <lineStyle kind="LINEAFTER" colorName="black" start="-1,0" stop="-1,0"/>
          <lineStyle kind="LINEBEFORE" colorName="black" start="0,-2" stop="0,-1"/>
          <lineStyle kind="LINEBELOW" colorName="black" start="0,-1" stop="0,-1"/>
        </blockTableStyle>
        <blockTableStyle id="esquema">
          <blockFont name="Helvetica-Bold" size="8" start="0,0" stop="-1,-1"/>
          <blockValign value="MIDDLE" start="0,0" stop="-1,-1"/>
          <lineStyle kind="GRID" colorName="black" start="0,0" stop="-1,-1"/>
        </blockTableStyle>
        <!-- repercusions -->
        <blockTableStyle id="repercusions">
          <blockFont name="Helvetica" size="8" start="0,0" stop="-1,-1"/>
          <blockBackground colorName="silver" start="-1,0" stop="-1,0"/>
          <lineStyle kind="BOX" colorName="black" start="-2,0" stop="-1,0"/>
        </blockTableStyle>
        <blockTableStyle id="mod_esquema">
          <blockFont name="Helvetica" size="8" start="0,0" stop="-1,-1"/>
          <blockFont name="Helvetica" size="6" start="0,1" stop="0,1"/>
          <blockFont name="Helvetica-Bold" size="8" start="0,0" stop="0,0"/>
          <lineStyle kind="BOX" colorName="black" start="0,0" stop="-1,-1"/>
          <lineStyle kind="LINEBELOW" colorName="black" start="0,0" stop="-1,0"/>
          <blockAlignment value="CENTRE" start="-2,-1" stop="-1,-1"/>
        </blockTableStyle>
        <blockTableStyle id="devolucion">
          <blockFont name="Helvetica" size="8" start="0,0" stop="-1,-1"/>
          <blockFont name="Helvetica-Bold" size="8" start="0,0" stop="-1,0"/>
          <blockFont name="Helvetica-Bold" size="8" start="7,1" stop="7,1"/>
          <blockFont name="Helvetica-Bold" size="8" start="11,1" stop="11,1"/>

          <blockAlignment value="CENTRE" start="0,2" stop="14,-1"/>
          <blockFont name="Helvetica" size="7" start="0,2" stop="0,-1"/>
          <blockValign value="TOP" start="0,-1" stop="0,-1"/>
          <blockValign value="BOTTOM" start="0,-2" stop="0,-2"/>

          <lineStyle kind="LINEABOVE" colorName="black" start="0,4" stop="0,4"/>

          <lineStyle kind="LINEABOVE" colorName="black" start="6,0" stop="14,0"/>
          <lineStyle kind="LINEAFTER" colorName="black" start="14,0" stop="14,-1"/>
          <lineStyle kind="LINEAFTER" colorName="black" start="0,2" stop="14,-1"/>
          <lineStyle kind="LINEBEFORE" colorName="black" start="0,2" stop="0,-1"/>

          <lineStyle kind="LINEBELOW" colorName="black" start="2,2" stop="2,-1"/>
          <lineStyle kind="LINEBELOW" colorName="black" start="4,2" stop="4,-1"/>
          <lineStyle kind="LINEBELOW" colorName="black" start="7,2" stop="7,-1"/>
          <lineStyle kind="LINEBELOW" colorName="black" start="9,2" stop="9,-1"/>
          <lineStyle kind="LINEBELOW" colorName="black" start="11,2" stop="11,-1"/>
          <lineStyle kind="LINEBELOW" colorName="black" start="13,2" stop="13,-1"/>

          <lineStyle kind="LINEBELOW" colorName="black" start="0,-1" stop="14,-1"/>
          <lineStyle kind="BOX" colorName="black" start="0,0" stop="5,1"/>
          <lineStyle kind="BOX" colorName="black" start="-2,0" stop="-1,-1"/>
          <blockFont name="Helvetica" size="6" start="16,1" stop="17,-1"/>
          <blockAlignment value="LEFT" start="16,1" stop="17,1"/>
          <lineStyle kind="GRID" colorName="black" start="16,1" stop="17,-1"/>
         
          <blockValign value="MIDDLE" start="16,1" stop="17,-1"/>
          <blockLeftPadding length="1mm" start="16,1" stop="16,-1"/>
          <lineStyle kind="LINEBELOW" colorName="black" start="6,0" stop="14,1"/>
          <blockAlignment value="CENTRE" start="2,0" stop="2,0"/>

          <blockLeftPadding length="1mm" start="19,-2" stop="19,-1"/>

          <blockBackground colorName="silver" start="2,3" stop="2,3"/>
          <blockBackground colorName="silver" start="4,3" stop="4,3"/>
          <blockBackground colorName="silver" start="7,3" stop="7,3"/>
          <blockBackground colorName="silver" start="9,3" stop="9,3"/>
          <blockBackground colorName="silver" start="11,3" stop="11,3"/>
          <blockBackground colorName="silver" start="13,3" stop="13,3"/>

          <blockBackground colorName="silver" start="2,5" stop="2,5"/>
          <blockBackground colorName="silver" start="4,5" stop="4,5"/>
          <blockBackground colorName="silver" start="7,5" stop="7,5"/>
          <blockBackground colorName="silver" start="9,5" stop="9,5"/>
          <blockBackground colorName="silver" start="11,5" stop="11,5"/>
          <blockBackground colorName="silver" start="13,5" stop="13,5"/>

          <blockBackground colorName="silver" start="17,1" stop="17,-1"/>
          <blockBackground colorName="silver" start="19,2" stop="20,3"/>

          <blockBackground colorName="silver" start="20,4" stop="20,5"/>
        </blockTableStyle>
        <blockTableStyle id="observaciones_8">
          <blockFont name="Helvetica" size="8" start="0,0" stop="-1,-1"/>
          <blockFont name="Helvetica-Bold" size="8" start="0,0" stop="0,0"/>
          <lineStyle kind="BOX" colorName="black" start="0,0" stop="-1,-1"/>
          <blockBackground colorName="silver" start="0,1" stop="1,-1"/>
          <blockLeftPadding length="1mm" start="-2,-2" stop="-2,-1"/>
        </blockTableStyle>
        <blockTableStyle id="validacion">
          <blockFont name="Helvetica" size="8" start="0,0" stop="-1,-1"/>
          <blockFont name="Helvetica-Bold" size="8" start="0,0" stop="0,0"/>
          <blockFont name="Helvetica-Bold" size="8" start="6,4" stop="6,4"/>
          
          <lineStyle kind="BOX" colorName="black" start="0,0" stop="3,-1"/>
          <lineStyle kind="BOX" colorName="black" start="5,4" stop="-1,-1"/>
          <lineStyle kind="LINEBELOW" colorName="black" start="0,0" stop="3,3"/>
          <lineStyle kind="GRID" colorName="black" start="8,0" stop="8,2"/>

          <blockBackground colorName="silver" start="8,0" stop="8,2"/>
          <blockBackground colorName="silver" start="2,1" stop="3,1"/>
          <blockBackground colorName="silver" start="1,3" stop="3,3"/>
          <blockBackground colorName="silver" start="0,5" stop="3,-1"/>
          <blockBackground colorName="silver" start="5,6" stop="-1,7"/>
          <blockBackground colorName="silver" start="6,7" stop="-1,-1"/>

          <blockAlignment value="CENTRE" start="6,4" stop="6,4"/>

          <blockLeftPadding length="1mm" start="-2,-2" stop="-2,-1"/>
        </blockTableStyle>

        <blockTableStyle id="observacions_10">
          <blockBackground colorName="silver" start="0,0" stop="-1,-1"/>
          <blockBackground colorName="white" start="0,0" stop="0,0"/>
          <blockFont name="Helvetica" size="8" start="0,0" stop="-1,-1"/>
          <blockFont name="Helvetica-Bold" size="8" start="0,0" stop="0,0"/>
          <lineStyle kind="BOX" colorName="black" start="0,0" stop="-1,-1"/>
        </blockTableStyle>
        <blockTableStyle id="personal">
          <lineStyle kind="GRID" colorName="black" start="0,0" stop="-1,-1"/>
          <blockFont name="Helvetica-Bold" size="8" start="0,0" stop="-1,0"/>
          <blockFont name="Helvetica-Bold" size="6" start="0,1" stop="0,-1"/>
          <blockFont name="Helvetica" size="8" start="1,1" stop="-1,-1"/>
          <blockBackground colorName="silver" start="1,1" stop="-1,-1"/>
          <blockValign value="MIDDLE" />
        </blockTableStyle>
        <blockTableStyle id="reglas">
          <blockBackground colorName="black" start="1,0" stop="1,-1"/>
          <blockTextColor colorName="white" start="1,1" stop="1,1"/>
          <blockAlignment value="CENTRE" start="1,1" stop="1,1"/>
          <blockFont name="Helvetica-Bold" size="9" start="1,1" stop="1,1"/>
          <blockBackground colorName="silver" start="3,1" stop="3,1"/>
          <blockFont name="Helvetica" size="6" start="-1,1" stop="-1,1"/>
          <blockTextColor colorName="black" start="-1,-1" stop="-1,-1"/>
          <lineStyle kind="BOX" colorName="black" start="3,1" stop="3,1"/>
        </blockTableStyle>
        <blockTableStyle id="i_descargar">
          <blockFont name="Helvetica" size="8" start="0,0" stop="-1,-1"/>
          <blockAlignment value="CENTRE" start="1,0" stop="2,4"/>
          <blockValign value="MIDDLE" start="0,0" stop="-1,-1"/>
          <blockValign value="TOP" start="1,0" stop="1,-1"/>
          <blockFont name="Helvetica-Bold" size="8" start="0,0" stop="0,0"/>
          <lineStyle kind="BOX" colorName="black" />
          <lineStyle kind="LINEBELOW" colorName="black" start="0,0" stop="-1,0"/>
          <lineStyle kind="LINEBELOW" colorName="black" start="3,1" stop="-1,1"/>
          <lineStyle kind="LINEAFTER" colorName="black" start="0,1" stop="2,4"/>
          <lineStyle kind="LINEAFTER" colorName="black" start="6,1" stop="6,3"/>
          <lineStyle kind="LINEAFTER" colorName="black" start="4,2" stop="4,2"/>
          <lineStyle kind="LINEAFTER" colorName="black" start="8,2" stop="8,2"/>
          <lineStyle kind="GRID" colorName="black" start="3,3" stop="-1,-1"/>
          <lineStyle kind="GRID" colorName="black" start="0,4" stop="2,-1"/>
        </blockTableStyle>
        <blockTableStyle id="firmas">
          <blockFont name="Helvetica" size="8" start="0,0" stop="-1,-1"/>
          <blockValign value="MIDDLE" start="0,0" stop="-1,-1"/>
          
          <lineStyle kind="BOX" colorName="black" start="0,0" stop="-1,-1"/>
          <lineStyle kind="GRID" colorName="black" start="-2,0" stop="-1,-3"/>
          <lineStyle kind="GRID" colorName="black" start="0,-2" stop="3,-1"/>
          <lineStyle kind="LINEAFTER" colorName="black" start="1,0" stop="1,-1"/>
        </blockTableStyle>

        <blockTableStyle id="recordatori">
          <blockFont name="Helvetica" size="8" start="0,0" stop="-1,-1"/>
          <lineStyle kind="BOX" colorName="black" start="0,0" stop="-1,-1"/>
          <lineStyle kind="BOX" colorName="black" start="0,-4" stop="-3,-1"/>
          <lineStyle kind="BOX" colorName="black" start="-1,0" stop="-1,-1"/>
          <lineStyle kind="LINEBELOW" colorName="black" start="-1,4" stop="-1,4"/>
          <lineStyle kind="LINEBELOW" colorName="black" start="-1,-3" stop="-1,-1"/>
        </blockTableStyle>

        <blockTableStyle id="zona_trabajo">
          <blockFont name="Helvetica" size="8" start="0,0" stop="-1,-1"/>
          <lineStyle kind="BOX" colorName="black" start="0,0" stop="0,-1"/>
          <lineStyle kind="BOX" colorName="black" start="-1,0" stop="-1,-1"/>
          <lineStyle kind="LINEBELOW" colorName="black" start="0,0" stop="0,0"/>
          <lineStyle kind="LINEBELOW" colorName="black" start="-1,0" stop="-1,0"/>
        </blockTableStyle>
        <blockTableStyle id="zona_trabajo_2">
          <blockFont name="Helvetica" size="6" start="0,0" stop="-1,-1"/>
          <blockValign value="MIDDLE" start="0,0" stop="-1,0"/>
          <lineStyle kind="BOX" colorName="black" start="0,0" stop="1,-1"/>
          <lineStyle kind="BOX" colorName="black" start="-2,0" stop="-1,-1"/>
        </blockTableStyle>
        <blockTableStyle id="disclaimer">
          <blockFont name="Helvetica" size="6" start="0,0" stop="-1,-1"/>
          <lineStyle kind="LINEAFTER" colorName="black" start="0,0" stop="-1,-1"/>
          <lineStyle kind="LINEBEFORE" colorName="black" start="0,0" stop="-1,-1"/>
          <lineStyle kind="LINEBELOW" colorName="black" start="0,0" stop="-1,-1"/>
        </blockTableStyle>
      </stylesheet>
      <story>
        <!-- titol -->
        <blockTable style="titol" colWidths="18cm" rowHeights="0.5cm">
          <tr><td>HOJA PETICIÓN DE DESCARGO EN BT</td></tr>
        </blockTable>
        <spacer length="0.2cm"/>
        <!-- numero de peticion -->
        <blockTable style="n_peticion" colWidths="12cm,4cm,2cm" rowHeights="0.5cm">
          <tr>
            <td />
            <td>Nº PETICION</td>
            <td><xsl:value-of select="name"/></td>
          </tr>
        </blockTable>
        <spacer length="0.2cm"/>
        <!-- tramitacio -->
        <blockTable style="tramitacio">
          <tr>
            <td>Tramitación</td>
            <xsl:choose>
              <xsl:when test="tramitacio = 'N'">
                <td>NORMAL [X]</td>
                <td>URGENTE [_]</td>
                <td>INMEDIATA [_]</td>
              </xsl:when>
              <xsl:when test="tramitacio = 'U'">
                <td>NORMAL [_]</td>
                <td>URGENTE [X]</td>
                <td>INMEDIATA [_]</td>
              </xsl:when>
              <xsl:when test="tramitacio = 'I'">
                <td>NORMAL [_]</td>
                <td>URGENTE [_]</td>
                <td>INMEDIATA [X]</td>
              </xsl:when>
            </xsl:choose>
          </tr>
        </blockTable>
        <spacer length="0.2cm"/>
        <blockTable style="solicitante" colWidths="4cm,7cm,2cm,5cm" rowHeights="0.5cm,0.5cm,0.5cm">
          <tr>
            <td>1. DATOS DEL SOLICITANTE</td>
            <td />
            <td />
            <td />
          </tr>
          <tr>
            <td>Nombre del solicitante:</td>
            <td><xsl:value-of select="solicitant/name"/></td>
            <td>Teléfono:</td>
            <td><xsl:value-of select="solicitant/telefon"/></td>
          </tr>
          <tr>
            <td>Empresa:</td>
            <td><xsl:value-of select="solicitant/empresa"/></td>
            <td>Cargo:</td>
            <td><xsl:value-of select="solicitant/carrec"/></td>
          </tr>
        </blockTable>
        <spacer length="0.2cm"/>
        <blockTable style="d_instalacion" colWidths="5cm,13cm"
          rowHeights="0.5cm,0.5cm,0.5cm,0.5cm">
          <tr>
            <td>2. SITUACIÓN EN LA QUE SE ENTREGA LA INSTALACIÓN</td>
            <td/>
          </tr>
          <tr>
            <xsl:choose>
              <xsl:when test="bt_cortocircuito = 1">
                <td>Cortocircuito [X]</td><td>Descargo [_]</td>
              </xsl:when>
              <xsl:when test="bt_descargo = 1">
                <td>Cortocircuito [_]</td><td>Descargo [X]</td>
              </xsl:when>
            </xsl:choose>
          </tr>
          <tr>
            <td>Cortocircuito situado en: </td>
            <td><para style="p"><xsl:value-of select="bt_situacion_cortocircuito"/></para></td>
          </tr>
          <tr>
            <td />
            <td />
          </tr>
        </blockTable>
        <blockTable style="disclaimer" colWidths="18cm" rowHeights="1cm">
          <tr>
            <td><para style="p">Si la instalación se entrega solamente en descargo, para la realización de los trabajos se utilizará el procedimiento de trabajos en tensión en BT</para></td>
          </tr>
        </blockTable>
        <spacer length="0.2cm"/>
        <blockTable style="i_descargar" 
          colWidths="1.33cm,1.33cm,1.33cm,1.5cm,2cm,1.5cm,2cm,1.5cm,2cm,1.5cm,2cm"
          rowHeights="0.5cm,0.5cm,0.5cm,0.5cm,0.5cm,0.5cm,0.5cm,0.5cm,0.5cm,0.5cm">
          <tr>
            <td>3. INSTALACIÓN A DESCARGAR</td>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
          </tr>
          <tr>
            <td/>
            <td/>
            <td>Nº</td>
            <td/>
            <td>HORARIO PREVISTO</td>
            <td/>
            <td/>
            <td/>
            <td>HORARIO REAL</td>
            <td/>
            <td/>
          </tr>
          <tr>
            <td>Nº CT</td>
            <td>SALIDA</td>
            <td>ACOMET</td>
            <td>CORTE</td>
            <td/>
            <td>REPOSICION</td>
            <td/>
            <td>CORTE</td>
            <td/>
            <td>REPOSICION</td>
            <td/>
          </tr>
          <tr>
            <td/>
            <td/>
            <td>IDA</td>
            <td>HORA</td>
            <td>DD/MM</td>
            <td>HORA</td>
            <td>DD/MM</td>
            <td>HORA</td>
            <td>DD/MM</td>
            <td>HORA</td>
            <td>DD/MM</td>
          </tr>
          <xsl:apply-templates select="bt_cortes"/>
          <xsl:if test="count(bt_cortes) &lt; 6">
            <xsl:call-template name="repeat">
              <xsl:with-param name="n" select="6 - count(bt_cortes)"/>
            </xsl:call-template>
          </xsl:if>
        </blockTable>
        <spacer length="0.2cm"/>
        <blockTable style="t_previstos" colWidths="4cm,4cm,4cm,4cm,2cm" rowHeights="0.5cm,0.5cm,0.5cm">
          <tr>
            <td>3. TRABAJOS PREVISTOS</td>
            <td/>
            <td/>
            <td/>
            <td/>
          </tr>
          <tr>
            <td>conectar cliente [<xsl:choose><xsl:when test="bt_conectar_cliente = 1">X</xsl:when><xsl:otherwise>_</xsl:otherwise></xsl:choose>]</td>
            <td>variante [<xsl:choose><xsl:when test="bt_variante = 1">X</xsl:when><xsl:otherwise>_</xsl:otherwise></xsl:choose>]</td>
            <td>reparación [<xsl:choose><xsl:when test="bt_reparacion = 1">X</xsl:when><xsl:otherwise>_</xsl:otherwise></xsl:choose>]</td>
            <td>poda [<xsl:choose><xsl:when test="bt_poda = 1">X</xsl:when><xsl:otherwise>_</xsl:otherwise></xsl:choose>]</td>
          </tr>
          <tr>
            <td>desconectar cliente [<xsl:choose><xsl:when test="bt_desconectar_cliente = 1">X</xsl:when><xsl:otherwise>_</xsl:otherwise></xsl:choose>]</td>
            <td>mejoras BT [<xsl:choose><xsl:when test="bt_mejoras = 1">X</xsl:when><xsl:otherwise>_</xsl:otherwise></xsl:choose>]</td>
            <td>otros [<xsl:choose><xsl:when test="bt_otros = 1">X</xsl:when><xsl:otherwise>_</xsl:otherwise></xsl:choose>]</td>
            <td><xsl:choose><xsl:when test="bt_otros_txt = ''">[____________]</xsl:when><xsl:otherwise><xsl:value-of select="bt_otros_txt"/></xsl:otherwise></xsl:choose></td>
          </tr>
        </blockTable>
        <spacer length="0.2cm"/>
        <blockTable style="h_horaris" colWidths="4.5cm,4.5cm,4.5cm,4.5cm" rowHeights="0.5cm,0.5cm,0.5cm,0.5cm">
          <tr>
            <td>4. FECHA Y HORARIO</td>
            <td/>
            <td/>
            <td/>
          </tr>
          <tr>
            <td></td>
            <td>Fecha</td>
            <td>Hora Prevista</td>
            <td>Hora Real</td>
          </tr>
          <tr>
            <td>Entrega</td>
            <td><xsl:value-of select="substring-before(data_inici, ' ')"/></td>
            <td><xsl:value-of select="substring-after(data_inici, ' ')"/></td>
            <td/>
          </tr>
          <tr>
            <td>Devolución</td>
            <td><xsl:value-of select="substring-before(data_final, ' ')"/></td>
            <td><xsl:value-of select="substring-after(data_final, ' ')"/></td>
            <td/>
          </tr>
        </blockTable>
        <spacer length="0.2cm"/>
        <blockTable style="personal" colWidths="4.8cm,5.2cm,8cm" rowHeights="0.5cm,0.5cm,0.5cm,0.5cm,0.5cm,0.5cm">
          <tr>
            <td>PERSONAL QUE INTERVIENE</td>
            <td>NOMBRE Y APELLIDOS</td>
            <td>EMPRESA, TELÉFONO, OTROS DATOS</td>
          </tr>
          <tr>
            <td>AGENTE DE DESCARGO 1</td>
            <td><xsl:value-of select="agent_descarrec"/></td>
            <td/>
          </tr>
           <tr>
            <td>SUPLENTE AGENTE DE DESCARGO</td>
            <td><xsl:value-of select="agent_descarrec_2"/></td>
            <td/>
          </tr>
          <tr>
            <td>JEFE DE TRABAJO 1</td>
            <td><xsl:value-of select="cap_de_treball"/></td>
            <td/>
          </tr>
          <tr>
            <td>JEFE DE TRABAJO 2</td>
            <td><xsl:value-of select="cap_de_treball_2"/></td>
            <td/>
          </tr>
          <tr>
            <td>JEFE DE TRABAJO 3</td>
            <td><xsl:value-of select="cap_de_treball_3"/></td>
            <td/>
          </tr>
        </blockTable>
        <spacer length="0.2cm"/>
        <blockTable style="observacions_10" colWidths="4cm,14cm" rowHeights="0.5cm,2.3cm">
          <tr>
            <td>5. OBSERVACIONES</td>
            <td/>
          </tr>
          <tr>
            <td/>
            <td><xsl:value-of select="observacions"/></td>
          </tr>
        </blockTable>

        <nextPage/>
        <blockTable style="repercusions" 
          rowHeights="0.5cm"
          colWidths="5cm,0.7cm,0.7cm,0.7cm,0.7cm,0.7cm,0.7cm,0.7cm,0.7cm,0.7cm,0.7cm,0.2cm,3.5cm,2.3cm">
          <tr>
            <td></td>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td>Nº PETICIÓN</td>
            <td><xsl:value-of select="name"/></td>
          </tr>
        </blockTable>
        <spacer length="0.2cm"/>
        <blockTable style="firmas" 
          rowHeights="0.5cm,0.5cm,0.5cm,0.5cm,0.5cm,0.5cm,0.5cm"
          colWidths="1.5cm,4cm,1.5cm,4cm,5cm,2cm">
          <tr>
            <td>El solicitatnte</td>
            <td/>
            <td>Validado, el Responsable de Red</td>
            <td/>
            <td>Afecta a clientes</td>
            <td><xsl:choose><xsl:when test="afecta_clients = 1">SI</xsl:when><xsl:otherwise>NO</xsl:otherwise></xsl:choose></td>
          </tr>
          <tr>
            <td>firma</td>
            <td/>
            <td>firma</td>
            <td/>
            <td>Modifica el esquema de la red</td>
            <td><xsl:choose><xsl:when test="modifica_esquema = 1">SI</xsl:when><xsl:otherwise>NO</xsl:otherwise></xsl:choose></td>
          </tr>
          <tr>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
          </tr>
          <tr>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
          </tr>
          <tr>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
          </tr>
          <tr>
            <td>Nombre:</td>
            <td><xsl:value-of select="solicitant/name"/></td>
            <td>Nombre:</td>
            <td><xsl:value-of select="cap_instalacio"/></td>
            <td>En caso de modificación del esquema actual,</td>
            <td/>
          </tr>
          <tr>
            <td>Fecha:</td>
            <td></td>
            <td>Fecha:</td>
            <td></td>
            <td>adjuntar el esquema antes y después del descargo</td>
            <td/>
          </tr>
        </blockTable>
        <spacer length="0.2cm"/>
        <blockTable style="observaciones_8"
          colWidths="18cm">
          <tr>
            <td>6. OBSERVACIONES</td>
          </tr>
          <tr>
            <td/>
          </tr>
          <tr/>
          <tr/>
        </blockTable>
        <spacer length="0.2cm"/>
        <blockTable style="recordatori"
          rowHeights="0.5cm,0.5cm,0.5cm,0.5cm,0.5cm,0.5cm,0.5cm,0.5cm,0.5cm,0.5cm,0.5cm,0.5cm"
          colWidths="13.5cm,0.5cm,4cm">
          <tr>
            <td>RECORDAR:</td>
            <td/>
            <td>EN REDES MALLADAS</td>
          </tr>
          <tr>
            <td>· USAR PROTECCION PERSONAL Y COMPLEMENTARIA NORMALIZADA</td>
            <td/>
            <td>APROBADO [_]</td>
          </tr>
          <tr>
            <td>· ABRIR TODOS LOS CIRCUITOS PARA AISLAR TODAS LAS FUENTES DE TENSIÓN</td>
            <td/>
            <td>APLAZADO [_]</td>
          </tr>
          <tr>
            <td>· BLOQUEAR Y SI ES POSIBLE, EN POSICIÓN DE APERTURA, LOS APARATOS DE CORTE</td>
            <td/>
            <td>DENEGADO [_]</td>
          </tr>
          <tr>
            <td>Y SEÑALIZACIÓN DE PROHIBICIÓN DE MANIOBRAR</td>
            <td/>
            <td/>
          </tr>
          <tr>
            <td>· VERIFICAR LA AUSENCIA DE TENSIÓN</td>
            <td/>
            <td>Centro de Control</td>
          </tr>
          <tr>
            <td>· PUESA EN CORTOCIRCUITO DE LA INSTALACIÓN SI PROCEDE</td>
            <td/>
            <td/>
          </tr>
          <tr>
            <td>· DELIMITAR Y SEÑALIZAR LA ZONA DE TRABAJO</td>
            <td/>
            <td/>
          </tr>
          <tr>
            <td/>
            <td/>
            <td/>
          </tr>
          <tr>
            <td>LA AUTORIZACIÓN PARA LA CREACIÓN DE LA OZNA DE TRABAJO DEBERÁ IR ACOMPAÑADA</td>
            <td/>
            <td/>
          </tr>
          <tr>
            <td>DEL VOLANTE O VOLANTES DE AUTORIZACIÓN DE CREACIÓN Y ANULACIÓN DE LA ZONA DE</td>
            <td/>
            <td>Nombre:</td>
          </tr>
          <tr>
            <td>TRABAJO EN BT</td>
            <td/>
            <td>Fecha:</td>
          </tr>
        </blockTable>
        <spacer length="0.2cm"/>
        <blockTable style="zona_trabajo" colWidths="8.75cm,0.5cm,8.75cm" rowHeights="0.5cm,2cm,0.5cm,0.5cm">
          <tr>
            <td>AUTORIZACIÓN PARA LA CREACIÓN DE LA ZONA DE TRABAJO</td>
            <td/>
            <td>ANULACIÓN DE LA ZONA DE TRABAJO</td>
          </tr>
          <tr>
            <td><para style="p">Autorización por el agente de Descargo al Jefe de Trabajo para la creación de la Zona de Trabajo, mediante_________ volantes de autorización de creación de Zona de Trabajo.</para></td>
            <td/>
            <td><para style="p">Confirmación por el Jefe de Trabajo al Agente de Descargo de la anulación de la Zona de Trabajo, mediante _________ volantes de confirmación de anulación de Zona de Trabajo.</para></td>
          </tr>
          <tr>
            <td/>
            <td/>
            <td/>
          </tr>
          <tr>
            <td>Firma y nombre</td>
            <td/>
            <td>Firma y nombre</td>
          </tr>
        </blockTable>
        <blockTable style="zona_trabajo_2" 
        rowHeights="1.5cm,0.5cm,0.5cm,0.5cm,0.5cm,0.5cm,0.5cm"
        colWidths="6cm,2.75cm,0.5cm,6cm,2.75cm">
          <tr>
            <td><para style="p6">RECEPCIÓN POR EL JEFE DE TRABAJO DEL VOLANTE DE AUTORIZACIÓN PARA LA CREACIÓN DE LA ZONA DE TRABAJO</para></td>
            <td>FECHA/HORA</td>
            <td/>
            <td><para style="p6">DEVOLUCIÓN POR EL JEFE DE TRABAJO DEL VOLANTE DE CONFIRMACIÓN DE ANULACIÓN DE LA ZONA DE TRABAJO</para></td>
            <td>FECHA/HORA</td>
          </tr>
          <tr>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
          </tr>
          <tr>
            <td>Firma y nombre</td>
            <td/>
            <td/>
            <td>Firma y nombre</td>
            <td/>
          </tr>
          <tr>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
          </tr>
          <tr>
            <td>Firma y nombre</td>
            <td/>
            <td/>
            <td>Firma y nombre</td>
            <td/>
          </tr>
          <tr>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
          </tr>
          <tr>
            <td>Firma y nombre</td>
            <td/>
            <td/>
            <td>Firma y nombre</td>
            <td/>
          </tr>
        </blockTable>
        <spacer length="0.2cm"/>
        <blockTable style="observacions_10" colWidths="4cm,14cm" rowHeights="0.5cm,2cm">
          <tr>
            <td>OBSERVACIONES</td>
            <td/>
          </tr>
          <tr>
            <td/>
            <td/>
          </tr>
        </blockTable>
      </story>
    </document>
  </xsl:template>
</xsl:stylesheet>
