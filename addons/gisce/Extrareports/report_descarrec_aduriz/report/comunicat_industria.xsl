<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="descarrec_list" />
  </xsl:template>
  
  <xsl:template match="descarrec_list">
    <xsl:apply-templates select="descarrec" />
  </xsl:template>

  <xsl:template match="descarrec">
    <document>
      <template pageSize="(21cm, 29.7cm)">
      	<pageTemplate id="main">
      		<frame id="first" x1="1cm" y1="1cm" width="19cm" height="247mm"/>
      	</pageTemplate>
      </template>
      
      <stylesheet>
      
      <paraStyle name="text"
      		fontName="Helvetica"
      		fontSize="10" />

      </stylesheet>
    
      <story>
      	<blockTable colWidths="11cm,8cm">
      		<tr>
      			<td></td>
      			<td>
      				<para style="text" alignment="center"><b>DELEGACI�N TERRITORIAL DE LA JUNTA DE CASTILLA Y LE�N</b></para>
      				<para style="text" alignment="center">Servicio de Industria</para>
      				<para style="text" alignment="center">Plaza de Bilabo, 3</para>
      				<para style="text" alignment="center">09071 <u>BURGOS</u></para>
      			</td>
      		</tr>
      	</blockTable>

      	<spacer length="2cm" />
        <para style="data" alignment="right"><xsl:value-of select="data"/></para>
      	<para style="text">Muy se�ores nuestros:</para>
      	<spacer length="0.5cm" />
      	<para style="text">Con el fin de realizar trabajos en la red de distribuci�n, ponemos en su conocimiento de acuerdo con el art. 101 del RD 1955/2000 que precisamos efectuar los siguientes cortes programados del suministro el�ctrico que afectan a las siguientes instalaciones:
</para>
      	
      	<spacer length="2cm" />
      	<para style="text" leftIndent="1cm"><b>FECHA.- </b><xsl:value-of select="data_comunicat_industria" /></para>
      	<para style="text" leftIndent="1cm"><b>HORARIO.- </b> De las <xsl:value-of select="substring(data_inici, 12, 5)" /> a las <xsl:value-of select="substring(data_final, 12, 5)" /></para>
      	<xpre style="text" leftIndent="1cm"><b>AFECTA.- </b> <xsl:value-of select="cts_afectats" /></xpre>
      	<xpre style="text" leftIndent="1cm"><b>CALLES.- </b> <xsl:value-of select="carrers_afectats" /></xpre>
      	<para style="text" leftIndent="1cm"><b>N� CLIENTES AFECTADOS.- </b> <xsl:value-of select="count(clients/client)" /></para>
      	<spacer length="2cm" />
      	<para style="text">En caso de terminar los trabajos antes de la hora prevista se restablecer� el servicio sin previo aviso.</para>
      	<spacer length="0.5cm" />
      	<para style="text">Estos cortes se ponen en conocimiento de los clientes de acuerdo con la legislaci�n vigente.</para>
      	<spacer length="0.5cm" />
      	<para style="text" leftIndent="10cm">Atentamente les saludan</para>
      </story>
    </document>

    </xsl:template>

</xsl:stylesheet>
