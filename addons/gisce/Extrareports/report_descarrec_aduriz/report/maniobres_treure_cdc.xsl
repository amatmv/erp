<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="solicituds_list" />
  </xsl:template>
  
  <xsl:template match="solicituds_list">
    <xsl:apply-templates select="solicitud" />
  </xsl:template>

  <xsl:template match="maniobra">
    <blockTable style="quadricula" colWidths="1cm,5cm,7cm,2cm,2cm">
    <tr>
      <td><para style="textmaniobra"><xsl:value-of select="seq" /></para></td>
      <td><para style="textmaniobra"><xsl:value-of select="ct" /></para></td>
      <td><para style="textmaniobra"><xsl:value-of select="element" /></para></td>
      <td><para style="textmaniobra"><xsl:value-of select="accio" /></para></td>
      <td></td>
    </tr>
    </blockTable>
    <blockTable style="descripcio" colWidths="17cm">
    <tr>
      <td><para style="textdesc"><xsl:value-of select="descripcio" /></para></td>
    </tr>
    </blockTable>
    <spacer length="0.3cm" />
  </xsl:template>

  <xsl:template match="solicitud">
    <document>
      <template pageSize="(21cm, 29.7cm)">
        <pageTemplate id="main">
          <pageGraphics>
          	<image file="addons/custom/logo.png" width="40mm" height="40mm" x="85mm" y="247mm" />
          	<setFont name="Helvetica-Bold" size="30" />
          	<drawString x="3.2cm" y="27.8cm"><xsl:value-of select="//corporate-header/corporation/rml_header1" /></drawString>
          	<setFont name="Helvetica" size="10" />
          	<drawString x="3.2cm" y="27.1cm"><xsl:value-of select="//corporate-header/corporation/rml_footer1" /></drawString>
          	<lineMode width="2" />
          	<stroke color="blue"/>
          	<lines>
          		1cm 24cm 20cm 24cm
          	</lines>
          	
            <setFont name="Helvetica" size="14" />
          	
            <drawCentredString x="10.5cm" y="23cm">
              <xsl:value-of select="//corporate-header/corporation/name" />
            </drawCentredString>
            <setFont name="Helvetica-Bold" size="12" />
            <drawCentredString x="10.5cm" y="2cm">LAS CINCO REGLAS DE ORO SON DE OBLIGADO CUMPLIMIENTO</drawCentredString>
            <setFont name="Helvetica" size="8" />
            <drawString x="1cm" y="1cm">P-19-06 Rev1</drawString>
            <drawString x="14cm" y="1cm">EJEMPLAR PARA EL CENTRO DE CONTROL</drawString>
	
          </pageGraphics>
          <frame id="first" x1="1cm" y1="3cm" width="19cm" height="180mm"/>
        </pageTemplate>
      </template>
      
      <stylesheet>
      
      <paraStyle name="text"
      		fontName="Helvetica"
      		fontSize="12" />
      <paraStyle name="textbold"
          fontName="Helvetica-Bold"
          fontSize="12" 
          alignment="center"/>
      <paraStyle name="textdesc"
          fontName="Helvetica"
          fontSize="10" />
      <paraStyle name="textmaniobra"
          fontName="Helvetica"
          fontSize="12" />
      <paraStyle name="header"
          fontName="Helvetica-Bold"
          fontSize="8" />

      <blockTableStyle id="table">
        <lineStyle kind="GRID" colorName="black" start="0,0" stop="-1,-1" />
      </blockTableStyle>

      <blockTableStyle id="quadricula">
        <lineStyle kind="GRID" colorName="black" start="0,0" stop="-1,-1" />
      </blockTableStyle>

      <blockTableStyle id="descripcio">
        <lineStyle kind="LINEBEFORE" colorName="black" start="0,0" stop="0,0" />
        <lineStyle kind="LINEAFTER" colorName="black" start="-1,-1" stop="-1,-1" />
        <lineStyle kind="LINEBELOW" colorName="black" start="0,0" stop="-1,-1" />
      </blockTableStyle>

      </stylesheet>
      
      <story>
        <para style="textbold"><t t="1">OPERACIONES DE DESCARGO</t></para>
        <spacer length="0.8cm" />
      	<para style="text"><t t="1">Maniobras para el corte que se efectuar� desde las <xsl:value-of select="substring(data_inici, 12, 5)"/> horas del d�a <xsl:value-of select="concat(substring(data_inici, 9,2),'/',substring(data_inici, 6, 2),'/',substring(data_inici, 1, 4))"/> hasta las <xsl:value-of select="substring(data_final, 12, 5)"/>  horas del d�a <xsl:value-of select="concat(substring(data_inici, 9,2),'/',substring(data_inici, 6, 2),'/',substring(data_inici, 1, 4))"/>.</t></para>
        <spacer length="0.5cm"/>
        <blockTable style="table" colWidths="1cm,5cm,7cm,2cm,2cm">
          <tr>
            <td><para style="header" t="1">SEC</para></td>
            <td><para style="header" t="1">CT</para></td>
            <td><para style="header" t="1">ELEMENTO</para></td>
            <td><para style="header" t="1">ACCI�N</para></td>
            <td><para style="header" t="1">REALIZADA</para></td>
          </tr>
        </blockTable>
        <xsl:apply-templates select="maniobra" /> 
      </story>
    </document>

    </xsl:template>

</xsl:stylesheet>


