<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

  <xsl:template match="/">
    <xsl:apply-templates select="solicituds_list"/>
  </xsl:template>

  <xsl:template match="solicituds_list">
    <xsl:apply-templates select="solicitud"/>
  </xsl:template>

  <xsl:template match="solicitud">
    <document>
      <template pageSize="(21cm, 29.7cm)">
        <pageTemplate id="main">
          <frame id="first" x1="2cm" y1="1cm" width="18cm" height="25cm"/>
          <pageGraphics>
            <setFont name="Helvetica" size="8"/>
            <drawString x="1cm" y="1cm">I-26-01 Rev1</drawString>
          </pageGraphics>
        </pageTemplate>
      </template>
      <stylesheet>
        <paraStyle name="p"
          fontName="Helvetica"
          fontSize="8"/>

        <blockTableStyle id="titol">
          <blockFont name="Helvetica-Bold" size="10" start="0,0" stop="-1,-1"/>
          <lineStyle kind="BOX" colorName="black" />
          <blockValign value="MIDDLE"/>
          <blockAlignment value="CENTER"/>
        </blockTableStyle>
        <blockTableStyle id="n_peticion">
          <blockFont name="Helvetica" size="8" start="0,0" stop="-1,-1"/>
          <lineStyle kind="BOX" colorName="black" start="1,0" stop="-1,-1"/>
          <blockBackground colorName="silver" start="2,-1" stop="2,-1"/>
          <blockValign value="MIDDLE"/>
        </blockTableStyle>
        <blockTableStyle id="solicitante">
          <blockFont name="Helvetica" size="8" start="0,0" stop="-1,-1"/>
          <blockValign value="MIDDLE" start="0,0" stop="-1,-1"/>
          <blockFont name="Helvetica-Bold" size="8" start="0,0" stop="0,0"/>
          <lineStyle kind="BOX" colorName="black"/>
          <lineStyle kind="LINEBELOW" colorName="black" start="0,0" stop="-1,-1" />
          <blockBackground colorName="silver" start="1,1" stop="1,1"/>
          <blockBackground colorName="silver" start="1,2" stop="1,2"/>
          <blockBackground colorName="silver" start="3,1" stop="3,1"/>
          <blockBackground colorName="silver" start="3,2" stop="3,2"/>
        </blockTableStyle>
        <blockTableStyle id="d_instalacion">
          <blockFont name="Helvetica" size="8" start="0,0" stop="-1,-1"/>
          <blockValign value="MIDDLE" start="0,0" stop="-1,-1"/>
          <blockValign value="TOP" start="1,0" stop="1,-1"/>
          <blockFont name="Helvetica-Bold" size="8" start="0,0" stop="0,0"/>
          <lineStyle kind="BOX" colorName="black" />
          <lineStyle kind="LINEBELOW" colorName="black" start="0,0" stop="-1,0"/>
          <lineStyle kind="LINEBELOW" colorName="black" start="0,2" stop="-1,2"/>
          <lineStyle kind="LINEBELOW" colorName="black" start="0,4" stop="-1,4"/>
          <blockBackground colorName="silver" start="1,1" stop="1,-1"/>
          <blockBackground colorName="silver" start="0,2" stop="0,2"/>
          <blockBackground colorName="silver" start="0,4" stop="0,4"/>
          <blockBackground colorName="silver" start="0,6" stop="0,6"/>
        </blockTableStyle>
        <blockTableStyle id="horarios">
          <blockFont name="Helvetica" size="8" start="0,0" stop="-1,-1"/>
          <blockValign value="MIDDLE" start="0,0" stop="-1,-1"/>
          <lineStyle kind="BOX" colorName="black"/>
          <lineStyle kind="GRID" colorName="black" start="0,2" stop="-1,-1"/>
        </blockTableStyle>
        <blockTableStyle id="t_previstos">
          <blockFont name="Helvetica" size="8" start="0,0" stop="-1,-1"/>
          <blockFont name="Helvetica-Bold" size="8" start="0,0" stop="0,0"/>
          <blockValign value="MIDDLE" start="0,0" stop="-1,-1"/>
          <blockValign value="TOP" start="1,0" stop="1,-1"/>
          <lineStyle kind="BOX" colorName="black"/>
          <lineStyle kind="LINEBELOW" colorName="black" start="0,0" stop="-1,0"/>
          <blockBackground colorName="silver" start="1,1" stop="1,-1"/>
          <blockBackground colorName="silver" start="0,2" stop="0,-1"/>
        </blockTableStyle>
        <blockTableStyle id="tramitacio">
          <blockFont name="Helvetica" size="8" start="0,0" stop="-1,-1"/>
          <blockValign value="MIDDLE" start="0,0" stop="-1,-1"/>
        </blockTableStyle>
        <blockTableStyle id="s_re">
          <blockFont name="Helvetica" size="8" start="0,0" stop="-1,-1"/>
          <blockValign value="MIDDLE" start="0,0" stop="-1,-1"/>
          <blockAlignment value="RIGHT" start="0,0" stop="0,0"/>
          <lineStyle kind="BOX" colorName="black" start="1,0" stop="-1,-1"/>
          <blockBackground colorName="silver" start="-1,-1" stop="-1,-1"/>
        </blockTableStyle>
        <blockTableStyle id="h_horaris"> 
          <blockFont name="Helvetica" size="8" start="0,0" stop="-1,-1"/>
          <blockValign value="MIDDLE" start="0,0" stop="-1,-1"/>
          <blockFont name="Helvetica-Bold" size="8" start="0,0" stop="0,0"/>
          <blockAlignment value="CENTRE" start="0,0" stop="-1,-1"/>
          <lineStyle kind="LINEABOVE" colorName="black" start="0,0" stop="0,0"/>
          <lineStyle kind="LINEBEFORE" colorName="black" start="0,0" stop="0,0"/>
          <lineStyle kind="GRID" colorName="black" start="1,0" stop="-1,-1"/>
        </blockTableStyle>
        <blockTableStyle id="horaris">
          <blockFont name="Helvetica" size="8" start="0,0" stop="-1,-1"/>
          <blockValign value="MIDDLE" start="0,0" stop="-1,-1"/>
          <blockBackground colorName="silver" start="1,1" stop="-1,5"/>
          <lineStyle kind="GRID" colorName="black" start="0,1" stop="-1,5"/>
          <lineStyle kind="GRID" colorName="black" start="1,6" stop="-1,-1"/>
          <lineStyle kind="LINEBEFORE" colorName="black" start="0,0" stop="-1,0"/>
          <lineStyle kind="LINEAFTER" colorName="black" start="-1,0" stop="-1,0"/>
          <lineStyle kind="LINEBEFORE" colorName="black" start="0,-2" stop="0,-1"/>
          <lineStyle kind="LINEBELOW" colorName="black" start="0,-1" stop="0,-1"/>
        </blockTableStyle>
        <blockTableStyle id="esquema">
          <blockFont name="Helvetica-Bold" size="8" start="0,0" stop="-1,-1"/>
          <blockValign value="MIDDLE" start="0,0" stop="-1,-1"/>
          <lineStyle kind="GRID" colorName="black" start="0,0" stop="-1,-1"/>
        </blockTableStyle>
        <!-- repercusions -->
        <blockTableStyle id="repercusions">
          <blockFont name="Helvetica" size="8" start="0,0" stop="-1,-1"/>
          <blockFont name="Helvetica-Bold" size="8" start="0,0" stop="0,0"/>
          <blockBackground colorName="silver" start="-1,0" stop="-1,0"/>
          <lineStyle kind="GRID" colorName="black" start="1,-1" stop="10,-1"/>
          <lineStyle kind="BOX" colorName="black" start="0,0" stop="-4,-1"/>
          <lineStyle kind="GRID" colorName="black" start="-2,0" stop="-1,0"/>
        </blockTableStyle>
        <blockTableStyle id="mod_esquema">
          <blockFont name="Helvetica" size="8" start="0,0" stop="-1,-1"/>
          <blockFont name="Helvetica" size="6" start="0,1" stop="0,1"/>
          <blockFont name="Helvetica-Bold" size="8" start="0,0" stop="0,0"/>
          <lineStyle kind="BOX" colorName="black" start="0,0" stop="-1,-1"/>
          <lineStyle kind="LINEBELOW" colorName="black" start="0,0" stop="-1,0"/>
          <blockAlignment value="CENTRE" start="-2,-1" stop="-1,-1"/>
        </blockTableStyle>
        <blockTableStyle id="devolucion">
          <blockFont name="Helvetica" size="8" start="0,0" stop="-1,-1"/>
          <blockFont name="Helvetica-Bold" size="8" start="0,0" stop="-1,0"/>
          <blockFont name="Helvetica-Bold" size="8" start="7,1" stop="7,1"/>
          <blockFont name="Helvetica-Bold" size="8" start="11,1" stop="11,1"/>

          <blockAlignment value="CENTRE" start="0,2" stop="14,-1"/>
          <blockFont name="Helvetica" size="7" start="0,2" stop="0,-1"/>
          <blockValign value="TOP" start="0,-1" stop="0,-1"/>
          <blockValign value="BOTTOM" start="0,-2" stop="0,-2"/>

          <lineStyle kind="LINEABOVE" colorName="black" start="0,4" stop="0,4"/>

          <lineStyle kind="LINEABOVE" colorName="black" start="6,0" stop="14,0"/>
          <lineStyle kind="LINEAFTER" colorName="black" start="14,0" stop="14,-1"/>
          <lineStyle kind="LINEAFTER" colorName="black" start="0,2" stop="14,-1"/>
          <lineStyle kind="LINEBEFORE" colorName="black" start="0,2" stop="0,-1"/>

          <lineStyle kind="LINEBELOW" colorName="black" start="2,2" stop="2,-1"/>
          <lineStyle kind="LINEBELOW" colorName="black" start="4,2" stop="4,-1"/>
          <lineStyle kind="LINEBELOW" colorName="black" start="7,2" stop="7,-1"/>
          <lineStyle kind="LINEBELOW" colorName="black" start="9,2" stop="9,-1"/>
          <lineStyle kind="LINEBELOW" colorName="black" start="11,2" stop="11,-1"/>
          <lineStyle kind="LINEBELOW" colorName="black" start="13,2" stop="13,-1"/>

          <lineStyle kind="LINEBELOW" colorName="black" start="0,-1" stop="14,-1"/>
          <lineStyle kind="BOX" colorName="black" start="0,0" stop="5,1"/>
          <lineStyle kind="BOX" colorName="black" start="-2,0" stop="-1,-1"/>
          <blockFont name="Helvetica" size="6" start="16,1" stop="17,-1"/>
          <blockAlignment value="LEFT" start="16,1" stop="17,1"/>
          <lineStyle kind="GRID" colorName="black" start="16,1" stop="17,-1"/>
         
          <blockValign value="MIDDLE" start="16,1" stop="17,-1"/>
          <blockLeftPadding length="1mm" start="16,1" stop="16,-1"/>
          <lineStyle kind="LINEBELOW" colorName="black" start="6,0" stop="14,1"/>
          <blockAlignment value="CENTRE" start="2,0" stop="2,0"/>

          <blockLeftPadding length="1mm" start="19,-2" stop="19,-1"/>

          <blockBackground colorName="silver" start="2,3" stop="2,3"/>
          <blockBackground colorName="silver" start="4,3" stop="4,3"/>
          <blockBackground colorName="silver" start="7,3" stop="7,3"/>
          <blockBackground colorName="silver" start="9,3" stop="9,3"/>
          <blockBackground colorName="silver" start="11,3" stop="11,3"/>
          <blockBackground colorName="silver" start="13,3" stop="13,3"/>

          <blockBackground colorName="silver" start="2,5" stop="2,5"/>
          <blockBackground colorName="silver" start="4,5" stop="4,5"/>
          <blockBackground colorName="silver" start="7,5" stop="7,5"/>
          <blockBackground colorName="silver" start="9,5" stop="9,5"/>
          <blockBackground colorName="silver" start="11,5" stop="11,5"/>
          <blockBackground colorName="silver" start="13,5" stop="13,5"/>

          <blockBackground colorName="silver" start="17,1" stop="17,-1"/>
          <blockBackground colorName="silver" start="19,2" stop="20,3"/>

          <blockBackground colorName="silver" start="20,4" stop="20,5"/>
        </blockTableStyle>
        <blockTableStyle id="observaciones_8">
          <blockFont name="Helvetica" size="8" start="0,0" stop="-1,-1"/>
          <blockFont name="Helvetica-Bold" size="8" start="0,0" stop="0,0"/>
          <blockFont name="Helvetica-Bold" size="8" start="-2,0" stop="-2,0"/>
          <lineStyle kind="BOX" colorName="black" start="0,0" stop="1,-1"/>
          <lineStyle kind="BOX" colorName="black" start="-2,0" stop="-1,-1"/>
          <blockBackground colorName="silver" start="0,1" stop="1,-1"/>
          <blockBackground colorName="silver" start="-2,2" stop="-1,2"/>
          <blockBackground colorName="silver" start="-1,3" stop="-1,4"/>
          <blockLeftPadding length="1mm" start="-2,-2" stop="-2,-1"/>
        </blockTableStyle>
        <blockTableStyle id="validacion">
          <blockFont name="Helvetica" size="8" start="0,0" stop="-1,-1"/>
          <blockFont name="Helvetica-Bold" size="8" start="0,0" stop="0,0"/>
          <blockFont name="Helvetica-Bold" size="8" start="6,4" stop="6,4"/>
          
          <lineStyle kind="BOX" colorName="black" start="0,0" stop="3,-1"/>
          <lineStyle kind="BOX" colorName="black" start="5,4" stop="-1,-1"/>
          <lineStyle kind="LINEBELOW" colorName="black" start="0,0" stop="3,3"/>
          <lineStyle kind="GRID" colorName="black" start="8,0" stop="8,2"/>

          <blockBackground colorName="silver" start="8,0" stop="8,2"/>
          <blockBackground colorName="silver" start="2,1" stop="3,1"/>
          <blockBackground colorName="silver" start="1,3" stop="3,3"/>
          <blockBackground colorName="silver" start="0,5" stop="3,-1"/>
          <blockBackground colorName="silver" start="5,6" stop="-1,7"/>
          <blockBackground colorName="silver" start="6,7" stop="-1,-1"/>

          <blockAlignment value="CENTRE" start="6,4" stop="6,4"/>

          <blockLeftPadding length="1mm" start="-2,-2" stop="-2,-1"/>
        </blockTableStyle>

        <blockTableStyle id="observacions_10">
          <blockBackground colorName="silver" start="0,0" stop="-1,-1"/>
          <blockBackground colorName="white" start="0,0" stop="0,0"/>
          <blockFont name="Helvetica" size="8" start="0,0" stop="-1,-1"/>
          <blockFont name="Helvetica-Bold" size="8" start="0,0" stop="0,0"/>
          <lineStyle kind="BOX" colorName="black" start="0,0" stop="-1,-1"/>
        </blockTableStyle>
        <blockTableStyle id="personal">
          <lineStyle kind="GRID" colorName="black" start="0,0" stop="-1,-1"/>
          <blockFont name="Helvetica-Bold" size="8" start="0,0" stop="-1,0"/>
          <blockFont name="Helvetica-Bold" size="8" start="0,0" stop="0,-1"/>
          <blockFont name="Helvetica" size="8" start="1,1" stop="-1,-1"/>
          <blockBackground colorName="silver" start="1,1" stop="-1,-1"/>
        </blockTableStyle>
        <blockTableStyle id="reglas">
          <blockBackground colorName="black" start="1,0" stop="1,-1"/>
          <blockTextColor colorName="white" start="1,1" stop="1,1"/>
          <blockAlignment value="CENTRE" start="1,1" stop="1,1"/>
          <blockFont name="Helvetica-Bold" size="9" start="1,1" stop="1,1"/>
          <blockBackground colorName="silver" start="3,1" stop="3,1"/>
          <blockFont name="Helvetica" size="6" start="-1,1" stop="-1,1"/>
          <blockTextColor colorName="black" start="-1,-1" stop="-1,-1"/>
          <lineStyle kind="BOX" colorName="black" start="3,1" stop="3,1"/>
        </blockTableStyle>
      </stylesheet>
      <story>
        <!-- titol -->
        <blockTable style="titol" colWidths="18cm" rowHeights="0.5cm">
          <tr><td>HOJA PETICIÓN DE DESCARGO EN MT</td></tr>
        </blockTable>
        <spacer length="0.2cm"/>
        <!-- numero de peticion -->
        <blockTable style="n_peticion" colWidths="12cm,4cm,2cm" rowHeights="0.5cm">
          <tr>
            <td />
            <td>Nº PETICION</td>
            <td><xsl:value-of select="name"/></td>
          </tr>
        </blockTable>
        <spacer length="0.2cm"/>
        <!-- tramitacio -->
        <blockTable style="tramitacio">
          <tr>
            <td>Tramitación</td>
            <xsl:choose>
              <xsl:when test="tramitacio = 'N'">
                <td>NORMAL [X]</td>
                <td>URGENTE [_]</td>
                <td>INMEDIATA [_]</td>
              </xsl:when>
              <xsl:when test="tramitacio = 'U'">
                <td>NORMAL [_]</td>
                <td>URGENTE [X]</td>
                <td>INMEDIATA [_]</td>
              </xsl:when>
              <xsl:when test="tramitacio = 'I'">
                <td>NORMAL [_]</td>
                <td>URGENTE [_]</td>
                <td>INMEDIATA [X]</td>
              </xsl:when>
            </xsl:choose>
          </tr>
        </blockTable>
        <spacer length="0.2cm"/>
        <!-- datos solicitante -->
        <blockTable style="solicitante" colWidths="4cm,7cm,2cm,5cm" rowHeights="0.5cm,0.5cm,0.5cm">
          <tr>
            <td>1. DATOS DEL SOLICITANTE</td>
            <td />
            <td />
            <td />
          </tr>
          <tr>
            <td>Nombre del solicitante:</td>
            <td><xsl:value-of select="solicitant/name"/></td>
            <td>Teléfono:</td>
            <td><xsl:value-of select="solicitant/telefon"/></td>
          </tr>
          <tr>
            <td>Empresa:</td>
            <td><xsl:value-of select="solicitant/empresa"/></td>
            <td>Cargo:</td>
            <td><xsl:value-of select="solicitant/carrec"/></td>
          </tr>
        </blockTable>
        <spacer length="0.2cm"/>
        <!-- datos de la instalación -->
        <blockTable style="d_instalacion" colWidths="5cm,13cm" rowHeights="0.5cm,0.5cm,0.5cm,0.5cm,0.5cm,0.5cm,0.5cm">
          <tr>
            <td>2. DATOS DE LA INSTALACIÓN</td>
            <td/>
          </tr>
          <tr>
            <td>Instalación a descargar</td>
            <td><xsl:value-of select="inst_a_descarregar"/></td>
          </tr>
          <tr>
            <td/>
            <td/>
          </tr>
          <tr>
            <td>ABRIR elementos de corte efectivo nº</td>
            <td><xsl:value-of select="elements"/></td>
          </tr>
          <tr>
            <td/>
            <td/>
          </tr>
          <tr>
            <td>Puesta Tierra en:</td>
            <td><xsl:value-of select="tierra"/><xsl:value-of select="posada_a_terra"/></td>
          </tr>
          <tr>
            <td/>
            <td/>
          </tr>
        </blockTable>
        <spacer length="0.2cm"/>
        <!-- trabajos previstos -->
        <blockTable style="t_previstos" colWidths="4cm,14cm" rowHeights="0.5cm,0.5cm,0.5cm">
          <tr>
            <td>3. TRABAJOS PREVISTOS</td>
            <td/>
          </tr>
          <tr>
            <td>Descripción</td>
            <td><xsl:value-of select="treball"/></td>
          </tr>
          <tr>
            <td/>
            <td/>
          </tr>
        </blockTable>
        <spacer length="0.2cm"/>
        <!-- solicitud RE -->
        <blockTable style="s_re" colWidths="4cm,11cm,3cm" rowHeights="0.5cm">
          <tr>
            <td>[_]</td>
            <td>Se acompaña SOLICITUD DE RÉGIMEN ESPECIAL DE EXPLOTACIÓN Nº</td>
            <td/>
          </tr>
        </blockTable>
        <spacer length="0.2cm"/>
        <!-- horaris -->
        <blockTable style="h_horaris" colWidths="4.5cm,4.5cm,4.5cm,4.5cm" rowHeights="0.5cm">
          <tr>
            <td>4. FECHA Y HORARIO</td>
            <td>SOLICITADO</td>
            <td>ACORDADO</td>
            <td>REAL</td>
          </tr>
        </blockTable>
        <blockTable style="horaris" colWidths="4.5cm,2.25cm,2.25cm,2.25cm,2.25cm,2.25cm,2.25cm"
          rowHeights="0.5cm,0.5cm,0.5cm,0.5cm,0.5cm,0.5cm,0.5cm,0.5cm">
          <tr>
            <td></td>
            <td>Fecha</td>
            <td>Hora</td>
            <td>Fecha</td>
            <td>Hora</td>
            <td>Fecha</td>
            <td>Hora</td>
          </tr>
          <tr>
            <td>Inicio maniobras</td>
            <td><xsl:value-of select="substring-before(inici_maniobres_sol, ' ')"/></td>
            <td><xsl:value-of select="substring-after(inici_maniobres_sol, ' ')"/></td>
            <td><xsl:value-of select="substring-before(inici_maniobres_aco, ' ')"/></td>
            <td><xsl:value-of select="substring-after(inici_maniobres_aco, ' ')"/></td>
            <td/>
            <td/>
          </tr>
          <tr>
            <td>Corte suministro</td>
            <td><xsl:value-of select="substring-before(data_inici, ' ')"/></td>
            <td><xsl:value-of select="substring-after(data_inici, ' ')"/></td>
            <td><xsl:value-of select="substring-before(data_inici_aco, ' ')"/></td>
            <td><xsl:value-of select="substring-after(data_inici_aco, ' ')"/></td>
            <td/>
            <td/>
          </tr>
          <tr>
            <td>Entrega Z. Protegida</td>
            <td><xsl:value-of select="substring-before(entrega_zp_sol, ' ')"/></td>
            <td><xsl:value-of select="substring-after(entrega_zp_sol, ' ')"/></td>
            <td><xsl:value-of select="substring-before(entrega_zp_aco, ' ')"/></td>
            <td><xsl:value-of select="substring-after(entrega_zp_aco, ' ')"/></td>
            <td/>
            <td/>
          </tr>
          <tr>
            <td>Devolución Z. Protegida</td>
            <td><xsl:value-of select="substring-before(devolucion_zp_sol, ' ')"/></td>
            <td><xsl:value-of select="substring-after(devolucion_zp_sol, ' ')"/></td>
            <td><xsl:value-of select="substring-before(devolucion_zp_aco, ' ')"/></td>
            <td><xsl:value-of select="substring-after(devolucion_zp_aco, ' ')"/></td>
            <td/>
            <td/>
          </tr>
          <tr>
            <td>Reanudación suministro</td>
            <td><xsl:value-of select="substring-before(data_final, ' ')"/></td>
            <td><xsl:value-of select="substring-after(data_final, ' ')"/></td>
            <td><xsl:value-of select="substring-before(data_final_aco, ' ')"/></td>
            <td><xsl:value-of select="substring-after(data_final_aco, ' ')"/></td>
            <td/>
            <td/>
          </tr>
          <tr>
            <td>Tiempo de reposición en</td>
            <td>En laborable</td>
            <td><xsl:value-of select="reposicion_laborable_sol"/></td>
            <td>En laborable</td>
            <td><xsl:value-of select="reposicion_laborable_aco"/></td>
            <td>En laborable</td>
            <td/>
          </tr>
          <tr>
            <td>caso de emergencia (minutos)</td>
            <td>En festivo</td>
            <td><xsl:value-of select="reposicion_festivo_sol"/></td>
            <td>En festivo</td>
            <td><xsl:value-of select="reposicion_festivo_aco"/></td>
            <td>En festivo</td>
            <td/>
          </tr>
        </blockTable>
        <spacer length="0.2cm"/>
        <blockTable style="esquema" colWidths="18cm" rowHeights="0.5cm,9cm">
          <tr>
            <td>5. TRABAJOS PREVISTOS. ESQUEMA con delimitación de la zona protegida</td>
          </tr>
          <tr/>
        </blockTable>
        <nextPage/>
        <blockTable style="repercusions" colWidths="5cm,0.7cm,0.7cm,0.7cm,0.7cm,0.7cm,0.7cm,0.7cm,0.7cm,0.7cm,0.7cm,0.2cm,3.5cm,2.3cm">
          <tr>
            <td>6. REPERCUSIÓN POR HORA DE DEMORA EN LA ENTREGA</td>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td>Nº PETICIÓN</td>
            <td><xsl:value-of select="name"/></td>
          </tr>
          <tr>
            <td>Marcar el valor correspondiente</td>
            <td>1</td>
            <td>2</td>
            <td>3</td>
            <td>4</td>
            <td>5</td>
            <td>6</td>
            <td>7</td>
            <td>8</td>
            <td>9</td>
            <td>10</td>
            <td/>
            <td/>
            <td/>
          </tr>
        </blockTable>
        <spacer length="0.2cm"/>
        <!-- modificacio esquema -->
        <blockTable style="mod_esquema" colWidths="12cm,1cm,2.5cm,2.5cm">
          <tr>
            <td>7. MODIFICA EL ESQUEMA Y/O CARACTERÍSTICAS DE EQUIPOS Y/O INSTALACIONES</td>
            <td/>
            <td/>
            <td/>
          </tr>
          <tr>
            <td>En caso afirmativo es obligatorio aportar previamente la información técnica necesaria para actualizar los esquemas de maniobras.</td>
            <td/>
            <xsl:choose>
              <xsl:when test="modifica_esquema = 1">
                <td>SI [X]</td>
                <td>NO [_]</td>
              </xsl:when>
              <xsl:otherwise>
                <td>SI [_]</td>
                <td>NO [X]</td>
              </xsl:otherwise>
            </xsl:choose>
          </tr>
        </blockTable>
        <spacer length="0.2cm"/>
        <blockTable 
          style="devolucion" 
          colWidths="
          1.2cm,0.4cm,0.8cm,0.4cm,0.8cm,0.4cm,
          0.5cm,0.8cm,0.4cm,0.8cm,0.4cm,0.8cm,0.4cm,0.8cm,0.4cm,
          0.2cm,3.8cm,0.6cm,
          0.2cm,1.2cm,2.7cm"
          rowHeights="0.5cm,0.5cm,0.5cm,0.5cm,0.5cm,0.5cm">
          <tr>
            <td/>
            <td/>
            <td>DEVOLUCIÓN</td>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td>NORMALIZACIÓN</td>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td>Solicitante</td>
          </tr>
          <tr>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td>SOLICITADA</td>
            <td/>
            <td/>
            <td/>
            <td>ACORDADA</td>
            <td/>
            <td/>
            <td/>
            <td/>
            <td>Afecta a clientes</td>
            <xsl:choose>
              <xsl:when test="afecta_clients = 1"><td>Sí</td></xsl:when>
              <xsl:otherwise><td>No</td></xsl:otherwise>
            </xsl:choose>
            <td/>
            <td>firma</td>
            <td/>
          </tr>
          <tr>
            <td>DIARIA</td>
            <td/>
            <td>SI</td>
            <td/>
            <td>NO</td>
            <td/>
            <td/>
            <td>SI</td>
            <td/>
            <td>NO</td>
            <td/>
            <td>SI</td>
            <td/>
            <td>NO</td>
            <td/>
            <td/>
            <td>Son necesarias maniobras en MT</td>
            <xsl:choose>
              <xsl:when test="necessaries_maniobres_mt = 1"><td>Sí</td></xsl:when>
              <xsl:otherwise><td>No</td></xsl:otherwise>
            </xsl:choose>
            <td/>
            <td/>
            <td/>
          </tr>
          <tr>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td>Son necesarias operaciones en SE</td>
            <xsl:choose>
              <xsl:when test="necessaries_operacions_se = 1"><td>Sí</td></xsl:when>
              <xsl:otherwise><td>No</td></xsl:otherwise>
            </xsl:choose>
            <td/>
            <td/>
            <td/>
          </tr>
          <tr>
            <td>FIN DE</td>
            <td/>
            <td>SI</td>
            <td/>
            <td>NO</td>
            <td/>
            <td/>
            <td>SI</td>
            <td/>
            <td>NO</td>
            <td/>
            <td>SI</td>
            <td/>
            <td>NO</td>
            <td/>
            <td/>
            <td>Afecta a instalaciones de clientes</td>
            <xsl:choose>
              <xsl:when test="afecta_inst_clients = 1"><td>Sí</td></xsl:when>
              <xsl:otherwise><td>No</td></xsl:otherwise>
            </xsl:choose>
            <td/>
            <td>Nombre:</td>
            <td><xsl:value-of select="substring(solicitant/name,1,18)"/></td>
          </tr>
          <tr>
            <td>SEMANA</td>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td>Afecta instalaciones de otra empresa</td>
            <xsl:choose>
              <xsl:when test="afecta_inst_altres = 1"><td>Sí</td></xsl:when>
              <xsl:otherwise><td>No</td></xsl:otherwise>
            </xsl:choose>
            <td/>
            <td>Fecha:</td>
            <td/>
          </tr>
        </blockTable>
        <spacer length="0.2cm"/>
        <blockTable style="observaciones_8"
          colWidths="4cm,9.9cm,0.2cm,1.2cm,2.7cm">
          <tr>
            <td>8. OBSERVACIONES</td>
            <td/>
            <td/>
            <td>Validado Responsable Red</td>
            <td/>
          </tr>
          <tr>
            <td/>
            <td/>
            <td/>
            <td>Firma</td>
            <td/>
          </tr>
          <tr>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
          </tr>
          <tr>
            <td/>
            <td/>
            <td/>
            <td>Nombre:</td>
            <td><xsl:value-of select="substring(cap_instalacio,1,18)"/></td>
          </tr>
          <tr>
            <td/>
            <td/>
            <td/>
            <td>Fecha:</td>
            <td/>
          </tr>
        </blockTable>
        <spacer length="0.2cm"/>
        <!-- validacion -->
        <blockTable style="validacion"
          colWidths="4.6cm,2cm,4.4cm,2.9cm,0.2cm,1.2cm,1.2cm,0.7cm,0.5cm,0.3cm"
          rowHeights="0.5cm,0.5cm,0.5cm,0.5cm,0.5cm,0.5cm,0.5cm,0.5cm,0.5cm,0.5cm">
          <tr>
            <td>9. VALIDACIÓN</td>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td>APROBADO</td>
            <td/>
            <td/>
            <td/>
          </tr>
          <tr>
            <td>Fecha de entrada en el Centro de Control:</td>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td>APLAZADO</td>
            <td/>
            <td/>
            <td/>
          </tr>
          <tr>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td>DENEGADO</td>
            <td/>
            <td/>
            <td/>
          </tr>
          <tr>
            <td>Operador de subestación:</td>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
          </tr>
          <tr>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td>Centro de Control</td>
            <td/>
            <td/>
            <td/>
          </tr>
          <tr>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td>Firma</td>
            <td/>
            <td/>
            <td/>
            <td/>
          </tr>
          <tr>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
          </tr>
          <tr>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
          </tr>
          <tr>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td>Nombre:</td>
            <td><xsl:value-of select="substring(cdc,1,18)"/></td>
            <td/>
            <td/>
            <td/>
          </tr>
          <tr>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td>Fecha:</td>
            <td/>
            <td/>
            <td/>
            <td/>
          </tr>
        </blockTable>
        <spacer length="0.2cm"/>
        <!-- observacions_10 -->
        <blockTable style="observacions_10" colWidths="4cm,14cm" rowHeights="0.5cm,4.3cm">
          <tr>
            <td>10. OBSERVACIONES</td>
            <td/>
          </tr>
          <tr>
            <td/>
            <td/>
          </tr>
        </blockTable>
        <spacer length="0.2cm"/>
        <!-- personal -->
        <blockTable style="personal" colWidths="4.8cm,5.2cm,8cm" rowHeights="0.5cm,0.5cm,0.5cm,0.5cm,0.5cm,0.5cm">
          <tr>
            <td>PERSONAL QUE INTERVIENE</td>
            <td>NOMBRE Y APELLIDOS</td>
            <td>EMPRESA, TELÉFONO, OTROS DATOS</td>
          </tr>
          <tr>
            <td>AGENTE DE DESCARGO 1</td>
            <td/>
            <td/>
          </tr>
           <tr>
            <td>AGENTE DE DESCARGO 2</td>
            <td/>
            <td/>
          </tr>
          <tr>
            <td>JEFE DE TRABAJO 1</td>
            <td/>
            <td/>
          </tr>
          <tr>
            <td>JEFE DE TRABAJO 2</td>
            <td/>
            <td/>
          </tr>
          <tr>
            <td>JEFE DE TRABAJO 3</td>
            <td/>
            <td/>
          </tr>
        </blockTable>
        <spacer length="0.2cm"/>
        <blockTable style="reglas" colWidths="0.5cm,8cm,1.5cm,1cm,7cm" rowHeights="0.2cm,0.7cm,0.2cm">
          <tr>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
          </tr>
          <tr>
            <td/>
            <td>CUMPLIR LAS 5 REGLAS DE ORO</td>
            <td/>
            <td/>
            <td>Rellenado por el centro de control</td>
          </tr>
          <tr>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
          </tr>
        </blockTable>
      </story>
    </document>
  </xsl:template>
</xsl:stylesheet>
