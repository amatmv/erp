# -*- coding: utf-8 -*-

from osv import osv,fields
import locale
import time

class giscedata_descarrecs_descarrec(osv.osv):

  _name = 'giscedata.descarrecs.descarrec'
  _inherit = 'giscedata.descarrecs.descarrec'

  def _data_comunicat_industria(self, cr, uid, ids, field_name, *args):
    res = {}
    for d in self.browse(cr, uid, ids):
      try:
        locale.setlocale(locale.LC_TIME, 'es_ES.UTF-8')
        s = time.strftime('%d de %B de %Y, %A', time.strptime(d.data_inici, '%Y-%m-%d %H:%M:%S'))
      except locale.Error:
        s = '%s/%s/%s %s' % (d.data_inici[8:10],d.data_inici[5:7],d.data_inici[0:4],d.data_inici[11:16])
        pass
      u = unicode( s, "utf-8" )
      res[d.id] = u
    return res

  # Sobreescribim la funció cts_afectats pq ens mostri l'string tal i com la volem al 
  # report d'Aduriz

  def _cts_afectats2(self, cr, uid, ids, name, arg, context={}):
    res = {}
    for id in ids:
      id=int(id)
      cr.execute("SELECT i.code_ct, ct.descripcio, m.name as municipi from giscedata_descarrecs_descarrec_installacions i, giscedata_cts ct, giscedata_descarrecs_descarrec d, res_municipi m where i.code_ct = ct.name and ct.id_municipi = m.id and i.descarrec_id = %s group by i.code_ct, ct.descripcio, m.name order by i.code_ct asc", (id,))
      cts = cr.dictfetchall()
      string = ''
      for ct in cts:
        string += '%s "%s" en %s (Burgos)\n' % (ct['code_ct'], ct['descripcio'], ct['municipi'])
      res[id] = string
    return res

  _columns = {
    'data_comunicat_industria': fields.function(_data_comunicat_industria, method=True, type='char', size=255, string="Fecha comunicado industria"),
    'cts_afectats': fields.function(_cts_afectats2, type='text', string='CTS afectats', method=True),
    
  }

giscedata_descarrecs_descarrec()
