# -*- coding: utf-8 -*-

import wizard
import pooler

_init_form = """<?xml version="1.0"?>
<form string="Impresión de aviso de descargo">
  <label string="Se va a imprimir el aviso de descargo"/>
</form>
"""

_init_fields = {

}

def _init(self, cr, uid, data, context={}):
  return {}

def _print(self, cr, uid, data, context={}):
  return {'ids': [data['id']]}

class giscedata_descarrecs_aduriz_comunicat_carrer(wizard.interface):
  states = {
    'init' : {
      'actions': [_init],
      'result': {'type': 'state', 'state': 'print'},
    },
    'print': {
      'actions': [_print],
      'result': {'type': 'print', 'report': 'giscedata.descarrecs.comunicat_carrer', 'get_id_from_action': True, 'state': 'end'}
    },
  }

giscedata_descarrecs_aduriz_comunicat_carrer('giscedata.descarrecs.aduriz.comunicat.carrer')
