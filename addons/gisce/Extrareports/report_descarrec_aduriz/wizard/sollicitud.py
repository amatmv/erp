# -*- coding: utf-8 -*-

import wizard
import pooler

_init_form = """<?xml version="1.0"?>
<form string="Impresión de solicitud">
  <label string="Se va a imprimir la solicitud de descargo"/>
</form>
"""

_init_fields = {

}

def _init(self, cr, uid, data, context={}):
  return {}

def _tipus(self, cr, uid, data, context={}):
  sol_obj = pooler.get_pool(cr.dbname).get('giscedata.descarrecs.sollicitud')
  sol = sol_obj.browse(cr, uid, data['id'], context)
  
  if sol.xarxa == '1': # AT
    return 'print_at'
  return 'print_bt' # BT

def _print(self, cr, uid, data, context={}):
  return {'ids': [data['id']]}

class giscedata_descarrecs_aduriz_sollicitud(wizard.interface):
  states = {
    'init' : {
      'actions': [_init],
      'result': {'type': 'state', 'state': 'triar'},
    },
    'triar': {
      'actions': [],
      'result': {'type': 'choice', 'next_state': _tipus},
    },
    'print_at': {
      'actions': [_print],
      'result': {'type': 'print', 'report': 'giscedata.descarrecs.sollicitud_cdc', 'get_id_from_action': True, 'state': 'end'},
    },
    'print_bt': {
      'actions': [_print],
      'result': {'type': 'print', 'report': 'giscedata.descarrecs.sollicitud_agent', 'get_id_from_action': True, 'state': 'end'}
    },
  }

giscedata_descarrecs_aduriz_sollicitud('giscedata.descarrecs.aduriz.sollicitud')
