# -*- coding: utf-8 -*-
import wizard
import pooler


# Carreguem el text que ja està guardat a la base de dades
def _init(self, cr, uid, data, context={}):
  descarrec_obj = pooler.get_pool(cr.dbname).get('giscedata.descarrecs.descarrec')
  descarrec = descarrec_obj.browse(cr, uid, data['id'], context)
  # Generem el text de carrers afectats si no hi ha res guardat
  carrers = {}
  for client in descarrec.clients:
    if not carrers.has_key(client.street):
      carrers[client.street] = []
    try:
      if int(client.number) and client.number not in carrers[client.street]:
        carrers[client.street].append(client.number)
    except:
      pass
  carrers_str = []
  for carrer in carrers.keys():
    if len(carrers[carrer]):
      carrers_str.append("%s nº%s" % (carrer, ', '.join(map(str, sorted(map(int, carrers[carrer]))))))
    else:
      carrers_str.append("%s" % (carrer))

  data['carrers_afectats'] = '\n'.join(carrers_str)

  return {
    'carrers_afectats': data['carrers_afectats'],
    'cts_afectats': descarrec.cts_afectats 
  }

def _generar_carrers(self, cr, uid, data, context={}):
  descarrec_obj = pooler.get_pool(cr.dbname).get('giscedata.descarrecs.descarrec')
  descarrec = descarrec_obj.browse(cr, uid, data['id'], context)
  carrers = {}
  for client in descarrec.clients:
    if not carrers.has_key(client.street):
      carrers[client.street] = []
    try:
      if int(client.number) and client.number not in carrers[client.street]:
        carrers[client.street].append(client.number)
    except:
      pass
  carrers_str = []
  for carrer in carrers.keys():
    if len(carrers[carrer]):
      carrers_str.append("%s nº %s" % (carrer, ', '.join(map(str, sorted(map(int, carrers[carrer]))))))
    else:
      carrers_str.append("%s" % (carrer))
  data['carrers_afectats'] = '\n'.join(carrers_str)
  return {}

_init_form = """<?xml version="1.0"?>
<form string="Comunicat Indústria">
  <separator string="CTS afectados" colspan="4" />
  <field name="cts_afectats" nolabel="1" colspan="4" />
  <separator string="Calles" colspan="4" />
  <field name="carrers_afectats" nolabel="1" colspan="4" width="500" height="100"/>
</form>"""

_init_fields = {
  'carrers_afectats': {'string': 'Calles afectadas', 'type': 'text'},
  'cts_afectats': {'string': 'CTS afectats', 'type': 'text', 'readonly': True},
}

# Guardem el text que ens han introduït amb el formulari
def _save_text(self, cr, uid, data, context={}):
  descarrec_obj = pooler.get_pool(cr.dbname).get('giscedata.descarrecs.descarrec')
  descarrec_obj.write(cr, uid, [data['id']], {'carrers_afectats': data['form']['carrers_afectats']})
  return {}


def _print(self, cr, uid, data, context={}):
  return {'ids': [data['id']]}
  

class giscedata_descarrecs_aduriz_comunicat_industria(wizard.interface):

  states = {
    'init': {
    	'actions': [_init],
      'result': {'type': 'form', 'arch': _init_form, 'fields': _init_fields, 'state': [('end', 'Cancelar', 'gtk-cancel'), ('generar_carrers', 'Generar texto calles', 'gtk-refresh'), ('save_text', 'Imprimir', 'gtk-print')]}
    },
    'generar_carrers': {
      'actions': [_generar_carrers],
      'result': {'type': 'state', 'state': 'init'},
    },
    'save_text': {
    	'actions': [_save_text],
    	'result': {'type': 'state', 'state': 'print'}
    },
    'print': {
      'actions': [_print],
      'result': {'type': 'print', 'report':
      'giscedata.descarrecs.comunicat_industria', 'get_id_from_action':True, \
      'state':'end'}
    },
  }
giscedata_descarrecs_aduriz_comunicat_industria('giscedata.descarrecs.aduriz.comunicat.industria')
