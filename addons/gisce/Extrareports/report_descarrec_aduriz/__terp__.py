# -*- coding: utf-8 -*-
{
    "name": "Reports Descàrrecs Aduriz",
    "description": """
Modificació dels reports de descarrecs:
  * Comunicat a Indústria
Creació del report
  * Carta aviso general
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Extrareports",
    "depends":[
        "base",
        "giscedata_descarrecs"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "report_descarrec_aduriz_report.xml",
        "report_descarrec_aduriz_wizard.xml",
        "report_descarrec_aduriz_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
