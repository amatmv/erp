# -*- coding: utf-8 -*-

from osv import fields, osv

class giscedata_descarrecs_talls_bt(osv.osv):
  _name = "giscedata.descarrecs.talls_bt"
  
  _columns = {
    'name': fields.char('Name', size=6),
    'num_ct': fields.char('Nº CT', size=6),
    'num_salida': fields.integer('Nº Salida'),
    'num_acometida': fields.char('Nº Acometida', size=6),
    'corte_previsto': fields.datetime('Horario previsto de corte'),
    'reposicion_previsto': fields.datetime('Horario previsto de reposición'),
    'sollicitud_id': fields.many2one('giscedata.descarrecs.sollicitud', 'Solicitud de descargo'),
  }

giscedata_descarrecs_talls_bt()

class giscedata_descarrecs_sollicitud(osv.osv):
  _name = "giscedata.descarrecs.sollicitud"
  _inherit = "giscedata.descarrecs.sollicitud"

  _columns = {
    'inst_a_descarregar': fields.text('Instalación a descargar'),
    'elements_tall_efectiu': fields.text('Elementos de corte efectivos'),
    'posada_a_terra': fields.text('Puesta a tierra'),

    'data_inici_aco': fields.datetime('Corte de suministro acordado'),

    'inici_maniobres_sol': fields.datetime('Inicio de maniobras Solicitado'),
    'inici_maniobres_aco': fields.datetime('Inicio de maniobras Acordado'),
    
    'entrega_zp_sol': fields.datetime('Entrega Zona Protegida Solicitado'),
    'entrega_zp_aco': fields.datetime('Entrega Zona Protegida Acordado'),

    'devolucion_zp_sol': fields.datetime('Devolución Z Protegida Solicitado'),
    'devolucion_zp_aco': fields.datetime('Devolución Z Protegida Acordado'),

    'data_final_aco': fields.datetime('Reanudación de suministro acordado'),
    
    'reposicion_laborable_sol': fields.integer('T. reposición en emergencia (Laborable)', help="Tiempo de reposición en caso de emergencia en día laborable, en minutos."),
    'reposicion_laborable_aco': fields.integer('T. reposición en emergencia (Laborable)', help="Tiempo de reposición en caso de emergencia en día laborable, en minutos."),
    'reposicion_festivo_sol': fields.integer('T. reposición en emergencia (Festivo)', help="Tiempo de reposición en caso de emergencia en día festivo, en minutos."),
    'reposicion_festivo_aco': fields.integer('T. reposición en emergencia (Festivo)', help="Tiempo de reposición en caso de emergencia en día festivo, en minutos."),
    
    'afecta_clients': fields.boolean('Afecta a clientes'),
    'afecta_inst_clients': fields.boolean('Afecta a instalaciones de clientes'),
    'afecta_inst_altres': fields.boolean('Afecta a instalaciones de otra empresa'),
    'necessaries_maniobres_mt': fields.boolean('Son necesarias maniobras en MT'),
    'necessaries_operacions_se': fields.boolean('Son necesarias operaciones en subestación'),

    'agent_descarrec_2': fields.many2one('res.partner.address', 'Agente de descargo 2', readonly=True, states={'draft': [('readonly', False)]}),
    'cap_de_treball_2': fields.many2one('res.partner.address', 'Jefe de trabajo 2', readonly=True, states={'draft': [('readonly', False)]}),
    'cap_de_treball_3': fields.many2one('res.partner.address', 'Jefe de trabajo 3', readonly=True, states={'draft': [('readonly', False)]}),

    'bt_situacion_cortocircuito': fields.text('Situación del cortocircuito'),
    'bt_cortocircuito': fields.boolean('Cortocircuito'),
    'bt_descargo': fields.boolean('Descargo'),

    'bt_conectar_cliente': fields.boolean('Conectar cliente'),
    'bt_variante': fields.boolean('Variante'),
    'bt_reparacion': fields.boolean('Reparación'),
    'bt_poda': fields.boolean('Poda'),
    'bt_desconectar_cliente': fields.boolean('Desconectar cliente'),
    'bt_mejoras': fields.boolean('Mejoras BT'),
    'bt_otros': fields.boolean('Otros'),
    'bt_otros_txt': fields.char('Otros', size=32),
    'bt_cortes': fields.one2many('giscedata.descarrecs.talls_bt', 'sollicitud_id', 'Cortes'), 
  }

giscedata_descarrecs_sollicitud()

