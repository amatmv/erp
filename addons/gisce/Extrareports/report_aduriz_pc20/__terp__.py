# -*- coding: utf-8 -*-
{
    "name": "Informe PC20",
    "description": """
    Informe PC-20 Índice de Calidad de Atención al Consumidor.
    """,
    "version": "0-dev",
    "author": "GISCE Enginyeria, SL",
    "category": "CRM Reports",
    "depends":[
        "base",
        "base_extended",
        "crm",
        "giscedata_polissa",
        "jasper_reports"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "report_aduriz_pc20_report.xml"
    ],
    "active": False,
    "installable": True
}
