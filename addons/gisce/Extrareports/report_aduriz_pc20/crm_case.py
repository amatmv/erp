# -*- coding: utf-8 -*-

from datetime import datetime

from osv import fields, osv

class CRMCase(osv.osv):
    """Extensió del crm.case per afegir un camp funció 'pòlissa'
    
    S'agafa el camp ref que apunti a una pòlissa i es retorna aquesta.
    """
    
    _name = "crm.case"
    _inherit = "crm.case"

    def _unpack_refs(self, cursor, uid, ref, ref2):
        """Mira si el ref és una pòlissa i en retorna l'id o False
        """
        MODEL='giscedata.polissa'
        id_ok = False
        polissa_obj = self.pool.get('giscedata.polissa')
        if ref:
            model, id = ref.split(',')
            if model == MODEL and id:    
                id_ok = int(id)
        if ref2:
            model2, id2 = ref2.split(',')
            if model2 == MODEL and id2:
                id_ok = int(id2)

        if id_ok:
            polissa = polissa_obj.read(cursor, uid, [id_ok], ['id', 'name'])
            if polissa:
                return (id_ok, polissa[0]['name'])
        
        return False

    def _get_polissa(self, cursor, uid, ids, field_name, arg, context):
        """Retorna la pòlissa.
        """
        res = {}
        for case in self.browse(cursor, uid, ids):
            res[case.id] = self._unpack_refs(cursor, uid, case.ref, case.ref2)
        return res

    def _get_duration(self, cursor, uid, ids, field_name, arg, context):
        """Retorna la duració del cas
        """
        res = {}
        for case in self.browse(cursor, uid, ids):
            timediff = ( 
                    datetime.strptime(case.date_closed, "%Y-%m-%d %H:%M:%S") - 
                    datetime.strptime(case.date, "%Y-%m-%d %H:%M:%S")
                    )
            res[case.id] = '%sd' % (timediff.days,)
        return res
        
    _columns = {
        'polissa': fields.function(_get_polissa, method=True, type='many2one',
                                   relation='giscedata.polissa',
                                   string='Pòlissa'),
        'duracio': fields.function(_get_duration, method=True, type='char',
                                   size='3', string='Duració')
    }

CRMCase()
