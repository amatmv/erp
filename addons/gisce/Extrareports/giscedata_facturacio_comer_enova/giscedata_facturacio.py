# -*- coding: utf-8 -*-

from osv import osv
from tools.translate import _
from datetime import datetime, date

class GiscedataFacturacioFacturador(osv.osv):
    """Overwrite invoice method to add management fee if necessary"""

    _name = 'giscedata.facturacio.facturador'
    _inherit = 'giscedata.facturacio.facturador'

    def versions_de_preus(self, cursor, uid, polissa_id, data_inici,
                          data_final, context=None):
        if context is None:
            context = {}
        polissa_obj = self.pool.get('giscedata.polissa')
        product_obj = self.pool.get('product.product')
        pricelist_obj = self.pool.get('product.pricelist')

        # Fem un browse amb la data final per obtenir quina tarifa té
        polissa = polissa_obj.browse(cursor, uid, polissa_id, context={
            'date': data_final
        })

        res = super(GiscedataFacturacioFacturador, self).versions_de_preus(
            cursor, uid, polissa_id, data_inici, data_final, context
        )

        if polissa.mode_facturacio == 'index' and polissa.add_management_fee:
            pgestion_id = product_obj.search(
                cursor, uid, [('default_code', '=', 'CG01')]
            )[0]

            if context.get('llista_preu', False):
                llista_preu_id = context['llista_preu']
            else:
                llista_preu_id = polissa.llista_preu.id

            ctx = context.copy()
            # Get coeficients k/d from contract intervals
            ctx_i = {'ffields': ['coeficient_k', 'coeficient_d']}
            polissa_intervals = polissa_obj.get_modcontractual_intervals(
                cursor, uid, polissa_id, data_inici, data_final, context=ctx_i
            )

            # We have to get prices for every contract interval
            for interval, dades in polissa_intervals.items():
                inici = max(dades['dates'][0], data_inici)
                fi = min(dades['dates'][1], data_final)
                prices = super(
                    GiscedataFacturacioFacturador,
                    self
                ).versions_de_preus(
                    cursor, uid, polissa_id, inici, fi, context
                )
                res.update(prices)

            for date_version in res:
                ctx['date'] = date_version

                res[date_version]['gestion'] = pricelist_obj.price_get(
                    cursor, uid, [llista_preu_id], pgestion_id, 1, context=ctx
                )[llista_preu_id]

                # management fee
                pol_vals = polissa_obj.read(
                    cursor, uid, polissa_id, ['management_fee'],
                    context={'date': date_version}
                )
                gestion = pol_vals['management_fee']
                if round(gestion, 6) > 0.0:
                    res[date_version]['gestion'] = gestion

        return res



    def fact_via_lectures(self, cursor, uid, polissa_id, lot_id, context=None):
        """Method overwrite to add an invoice line related to management fee
        if necessary"""
        if not context:
            context = {}
        fact_obj = self.pool.get('giscedata.facturacio.factura')
        product_obj = self.pool.get('product.product')

        invoice_ids = super(
            GiscedataFacturacioFacturador, self).fact_via_lectures(
            cursor, uid, polissa_id, lot_id, context
        )
        ctx = context.copy()
        # identificar el producte donacions
        pgestion_id = product_obj.search(
            cursor, uid, [('default_code', '=', 'CG01')])[0]

        for fact in fact_obj.browse(cursor, uid, invoice_ids, context):
            if fact.journal_id.code in ('ENERGIA', 'ENERGIA.R'):
                if fact.polissa_id.add_management_fee:
                    ctx.update(
                        {'lang': fact.partner_id.lang, 'date': fact.data_final}
                    )
                    p_br = product_obj.browse(cursor, uid, pgestion_id, ctx)
                    versions = self.versions_de_preus(
                        cursor, uid, fact.polissa_id.id, fact.data_inici,
                        fact.data_final
                    )
                    num_versions = len(versions.keys())
                    # adds final_date to list
                    dates = versions.keys() + [fact.data_final]
                    for version_idx in range(0, num_versions):
                        mgmnt_days = (
                            datetime.strptime(
                                dates[version_idx + 1], '%Y-%m-%d'
                            ) -
                            datetime.strptime(
                                max(dates[version_idx], fact.data_inici),
                                '%Y-%m-%d'
                            )
                        ).days + 1
                        version_name = dates[version_idx]
                        force_price = versions[version_name]['gestion']
                        vals = {
                            'data_desde': fact.data_inici,
                            'data_fins': fact.data_final,
                            'uos_id': p_br.uom_id.id,
                            'quantity': mgmnt_days,
                            'multi': 1,
                            'product_id': pgestion_id,
                            'tipus': 'altres',
                            'name': p_br.description,
                            'force_price': force_price
                        }

                        self.crear_linia(cursor, uid, fact.id, vals, ctx)

                fact_obj.button_reset_taxes(cursor, uid, [fact.id])
        return invoice_ids

GiscedataFacturacioFacturador()