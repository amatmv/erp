<%inherit file="/account_invoice_base/report/account_invoice.mako"/>
<%block name="custom_css">
    <style>
        .styled-table th{
            border-collapse: collapse;
            background: #71A923;
            color: white;
        }
    </style>
</%block>
<%def name="logo_factura(invoice)">
    <%
        lang = invoice.partner_id.lang or 'es_ES'
    %>
    %if lang == 'gl_ES':
        <img alt="logo" class="logo" src="${addons_path}/giscedata_facturacio_comer_enova/report/gl_ES_logo_enova.png"/>
    %elif lang == 'ca_ES':
        <img alt="logo" class="logo" src="${addons_path}/giscedata_facturacio_comer_enova/report/ca_ES_logo_enova.png"/>
    %else:
        <img alt="logo" class="logo" src="${addons_path}/giscedata_facturacio_comer_enova/report/es_ES_logo_enova.png"/>
    %endif
</%def>
<%def name="details(invoice)">
    <div id="details">${invoice.name or ''}</div>
</%def>
<%def name="additional_details(invoice)">
    <tr>
        <td>
            ${_("Data de venciment:")}
        </td>
        <td>
            ${invoice.date_due}
        </td>
    </tr>
</%def>
