<%
from operator import attrgetter, itemgetter
from datetime import datetime, timedelta
import json, re

pool = objects[0].pool
polissa_obj = objects[0].pool.get('giscedata.polissa')
model_obj = objects[0].pool.get('ir.model.data')
tarifa_obj = objects[0].pool.get('giscedata.polissa.tarifa')
banner_obj =  objects[0].pool.get('report.banner')
sup_territorials_2013_comer_obj = pool.get('giscedata.suplements.territorials.2013.tec271.comer')

model_ids = model_obj.search(cursor, uid,
                                 [('module','=','giscedata_facturacio'),
                                  ('name','=','pricelist_tarifas_electricidad')])
tarifa_elect_atr = model_obj.read(cursor, uid, model_ids[0],['res_id'])['res_id']

iet_peatges = "ORDEN IET/107/2014"
itc_lloguer = "ORDEN ITC/3860/2007"

comptador_factures = 0

def check_ajuste(linia_energia, lectures_energia, tipus, id_tarifa):
    '''
    Aquest mètode comprova si, per una línia d'energia d'una factura existeix' \
    'alguna lectura d'energia amb ajuste integrador
    :param linia_energia: línia d'energia d'una factura a comprovar
    :param lectures_energia: lectures d'energia de la factura la qual pertany' \
    'la línia
    :param tipus: pot prendre dos valors: activa o reactiva
    :param id_tarifa: id de la tarifa d'accés
    :return: retorna CERT si existeix alguna lectura d'energia amb ajuste integrador' \
    'que es correspongui amb la línia d'energia. Per exemple, si tenim una lectura 3.0A(P3)' \
    'amb ajuste integrador i del tipus "Activa", buscara una línia d'energia de la factura' \
    'que sigui del tipus 'Energia (Activa)' i del període 1. Si existeix, retornarà cert.
    '''
    agrupades = tarifa_obj.get_grouped_periods(cursor, uid, id_tarifa)
    for lect in lectures_energia:
        if lect.tipus == tipus and lect.ajust > 0:
            name = re.findall('(P[0-9])', lect.name)[0]
            if name in agrupades:
                name = agrupades[name]
            if linia_energia.name in name:
                return True
    return False

def get_coenova(cursor, uid, tarifa_comer, linia):
    pricelist_obj = linia.pool.get('product.pricelist')
    imd_obj = pool.get('ir.model.data')
    imd_id = imd_obj._get_id(cursor, uid, 'giscedata_facturacio_indexada',
                             'product_factor_k')
    factor_k = imd_obj.read(cursor, uid, imd_id, ['res_id'])['res_id']
    coenova = pricelist_obj.price_get(
        cursor, uid, [tarifa_comer], factor_k, 1,
        context= {'date': linia.data_desde})[tarifa_comer]
    return coenova

def get_atr_price(cursor, uid, tarifa, linia):
    pricelist_obj = linia.pool.get('product.pricelist')
    uom_obj = linia.pool.get('product.uom')
    model_obj = objects[0].pool.get('ir.model.data')

    tipus = linia.tipus
    uom_data = {'uom_name': linia.uos_id.name, 'factor': 1}
    quantity = linia.quantity
    product_id = linia.product_id.id

    if tipus == 'potencia':
        uom_data = {'uom_name': 'kW/mes', 'factor': 1}
    elif tipus == 'reactiva':
        model_ids = model_obj.search(cursor, uid,
                                     [('module','=','giscedata_facturacio'),
                                      ('name','=','product_cosfi')])
        product_id = model_obj.read(cursor, uid, model_ids[0],['res_id'])['res_id']
        quantity = l.cosfi * 100

    uom_id = uom_obj.search(cursor, uid, [('name', '=', uom_data['uom_name'])])[0]

    atr_price = pricelist_obj.price_get(cursor, uid, [tarifa],
                                        product_id, quantity,
                                        factura.company_id.partner_id.id,
                                        {'date': linia.data_desde,
                                         'uom_name': uom_id
                                         })[tarifa]
    return atr_price

def get_origen_lectura(cursor, uid, lectura):
    """Busquem l'origen de la lectura cercant-la a les lectures de facturació"""
    res = {lectura.data_actual: '',
           lectura.data_anterior: ''}

    lectura_obj = lectura.pool.get('giscedata.lectures.lectura')
    tarifa_obj = lectura.pool.get('giscedata.polissa.tarifa')
    origen_obj = lectura.pool.get('giscedata.lectures.origen')
    origen_comer_obj = lectura.pool.get('giscedata.lectures.origen_comer')

    # Recuperar l'estimada
    estimada_id = origen_obj.search(cursor, uid, [('codi', '=', '40')])[0]

    #Busquem la tarifa
    if lectura and lectura.name:
        tarifa_id = tarifa_obj.search(cursor, uid, [('name', '=',
                                                lectura.name[:-5])])
    else:
        tarifa_id = False
    if tarifa_id:
        tipus = lectura.tipus == 'activa' and 'A' or 'R'

        search_vals = [('comptador', '=', lectura.comptador),
                       ('periode.name', '=', lectura.name[-3:-1]),
                       ('periode.tarifa', '=', tarifa_id[0]),
                       ('tipus', '=', tipus),
                       ('name', 'in', [lectura.data_actual,
                                       lectura.data_anterior])]
        lect_ids = lectura_obj.search(cursor, uid, search_vals)
        lect_vals = lectura_obj.read(cursor, uid, lect_ids,
                                     ['name', 'origen_comer_id', 'origen_id'])
        for lect in lect_vals:
            # En funció dels origens, escrivim el text
            # Si Estimada (40)
            # La resta: Real
            origen_txt = _(u"real")
            if lect['origen_id'][0] == estimada_id:
                origen_txt = _(u"estimada")

            res[lect['name']] = "%s" % (origen_txt)

    return res

def get_exces_potencia(cursor, uid, factura):
    linia_obj = factura.pool.get('giscedata.facturacio.factura.linia')
    linies = linia_obj.search(cursor, uid,[
                ('tipus', '=', 'exces_potencia'),('factura_id', '=', factura.id)
             ])
    if linies:
        return linia_obj.browse(cursor, uid, linies)
    else:
        return []

def get_modalitat(tipus, management_fee):
    res = ''
    if tipus == 'index':
        res = 'Enova INDEX'
        if management_fee:
            res = 'Enova ZERO'
    elif tipus == 'atr':
        res = 'Enova CLASSIC'
    return res

def get_distri_phone(cursor, uid, polissa):
    """ Per telèfons de ENDESA segons CUPS, aprofitant funció de switching"""
    sw_obj = polissa.pool.get('giscedata.switching')
    pa_obj = polissa.pool.get('res.partner.address')

    partner_id = sw_obj.partner_map(cursor, uid,
                                    polissa.cups, polissa.distribuidora.id)
    if partner_id:
        pa_ids = pa_obj.search(cursor, uid, [('partner_id', '=', partner_id)])
        return (pa_obj.read(cursor, uid, [pa_ids[0]],['phone'])[0]['phone'] or
                polissa.distribuidora.address[0].phone)
    else:
        return polissa.distribuidora.address[0].phone

historic_sql = """SELECT * FROM (
SELECT mes AS mes,
periode AS periode,
sum(suma_fact) AS facturat,
sum(suma_consum) AS consum,
min(data_ini) AS data_ini,
max(data_fin) AS data_fin
FROM (
SELECT f.polissa_id AS polissa_id,
       to_char(f.data_inici, 'YYYY/MM') AS mes,
       pt.name AS periode,
       COALESCE(SUM(il.quantity*(fl.tipus='energia')::int*(CASE WHEN i.type='out_refund' THEN -1 ELSE 1 END)),0.0) as suma_consum,
       COALESCE(SUM(il.price_subtotal*(fl.tipus='energia')::int*(CASE WHEN i.type='out_refund' THEN -1 ELSE 1 END)),0.0) as suma_fact,
       MIN(f.data_inici) data_ini,
       MAX(f.data_final) data_fin
       FROM
            giscedata_facturacio_factura f
            LEFT JOIN account_invoice i on f.invoice_id = i.id
            LEFT JOIN giscedata_facturacio_factura_linia fl on f.id=fl.factura_id
            LEFT JOIN account_invoice_line il on il.id=fl.invoice_line_id
            LEFT JOIN product_product pp on il.product_id=pp.id
            LEFT JOIN product_template pt on pp.product_tmpl_id=pt.id
       WHERE
            fl.tipus = 'energia' AND
            f.polissa_id = %(p_id)s AND
            f.data_inici <= %(data_inicial)s AND
            f.data_inici >= date_trunc('month', date %(data_final)s) - interval '14 month'
            AND (fl.isdiscount IS NULL OR NOT fl.isdiscount)
            AND i.type in('out_invoice', 'out_refund')
       GROUP BY
            f.polissa_id, pt.name, f.data_inici
       ORDER BY f.data_inici DESC ) AS consums
GROUP BY polissa_id, periode, mes
ORDER BY mes DESC, periode ASC
) consums_ordenats
ORDER BY mes ASC"""

# Repartiment segons BOE
rep_BOE = {'i': 36.28, 'c': 32.12 ,'o': 31.60}


show_ajust = False
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<head>
<link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_enova/report/pie.css"/>
<link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_enova/report/consum.css"/>
<link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_enova/report/giscedata_facturacio_comer_enova.css"/>
<link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_enova/report/enova.css"/>
<script src="${addons_path}/giscedata_facturacio_comer/report/assets/d3.min.js"></script>
</head>
<body>
%for factura in objects:
<%
    setLang(factura.lang_partner)
    if factura.lang_partner not in ['gl_ES', 'es_ES', 'ca_ES']:
        factura_lang = 'es_ES'
    else:
        factura_lang = factura.lang_partner
    #setLang('es_ES')

    polissa = polissa_obj.browse(cursor, uid, factura.polissa_id.id,
                                 context={'date': factura.data_final.val})

    tarifa_elect_comer = polissa.llista_preu.id

    dphone = get_distri_phone(cursor, uid, polissa)
    cphone = factura.company_id.partner_id.address[0].phone
    distri_phone = ''.join([dphone[i:i+3] for i in range(0,len(dphone),3)])
    comer_phone = '.'.join([cphone[i:i+3] for i in range(0,len(cphone),3)])

    factura._cr.execute(historic_sql,{'p_id':factura.polissa_id.id,
                                      'data_inicial': factura.data_inici,
                                      'data_final': factura.data_final,
                                      })
    historic = factura._cr.dictfetchall()

    historic_graf = {}
    periodes_graf = []

    for row in historic:
        historic_graf.setdefault(row['mes'],{})
        historic_graf[row['mes']].update({row['periode']: row['consum']})
        periodes_graf.append(row['periode'])

    periodes_graf = sorted(list(set(periodes_graf)))
    consums_mig = dict.fromkeys(periodes_graf, 0)
    consums_mig.update({'total':0})
    historic_js = []
    for mes, consums in historic_graf.items():
        p_js = {'mes': mes}
        for p in periodes_graf:
            p_js.update({p: consums.get(p,0.0)})
            consums_mig[p] += consums.get(p,0.0)
        historic_js.append(p_js)
        consums_mig['total'] += 1

    consums_mig = dict((k, v/(consums_mig['total'] or 1.0)) for k, v in consums_mig.items() if k.startswith('P'))

    total_historic_kw = sum([h['consum'] for h in historic])
    total_historic_eur = sum([h['facturat'] for h in historic])
    data_ini = min([h['data_ini'] for h in historic])
    data_fin = max([h['data_fin'] for h in historic])
    historic_dies = 1 + (datetime.strptime(data_fin, '%Y-%m-%d') - datetime.strptime(data_ini, '%Y-%m-%d')).days

    mes_any_inicial = (datetime.strptime(factura.data_inici,'%Y-%m-%d') - timedelta(days=365)).strftime("%Y/%m")
    total_any = sum([h['consum'] for h in historic if h['mes'] > mes_any_inicial])

    iese_lines = [l for l in factura.tax_line if 'IVA' not in l.name]
    exempt = [True for l in factura.tax_line if '-85%' in l.name]
    iva_lines = [l for l in factura.tax_line if 'IVA' in l.name]

    if not len(exempt):
        exempt = False
    else:
        exempt = exempt[0]

    impostos = {}
    for l in factura.tax_line:
        impostos.update({l.name: l.amount})

    iese = sum([v for k, v in impostos.items() if 'IVA' not in k])

    #TODO:  Quan es processin les línies, repassar una forma + eficient
    lloguer_lines = [l for l in factura.linia_ids if l.tipus in 'lloguer']
    altres_lines = [l for l in factura.linia_ids if l.tipus in 'altres'
                    and l.invoice_line_id.product_id.code != 'CG01']
    gestion_lines = [l for l in factura.linia_ids if l.tipus in 'altres'
                    and l.invoice_line_id.product_id.code == 'CG01']

    total_altres = sum([l.price_subtotal for l in altres_lines])
    total_gestion = sum([l.price_subtotal for l in gestion_lines])
    periodes = dict([('P%s' % p,'') for p in range(1, 7)])
    periodes_a = sorted(list(set([lectura.name[-3:-1]
                                for lectura in factura.lectures_energia_ids
                                if lectura.tipus == 'activa'])))

    periodes_r = sorted(list(set([lectura.name[-3:-1]
                                for lectura in factura.lectures_energia_ids
                                if lectura.tipus == 'reactiva'])))

    periodes_m = sorted(list(set([lectura.name
                                  for lectura in factura.lectures_potencia_ids])))

    potencies_periode = dict([('P%s' % p,'') for p in range(1, 7)])
    for pot in polissa.potencies_periode:
        potencies_periode[pot.periode_id.name] = pot.potencia

    fact_pot_txt = ((polissa.facturacio_potencia=='max' or len(periodes_a)>3)
                        and _(u"facturació per maxímetre")
                     or _(u"facturació per ICP"))

    # Detall factures per excés potència
    exces_potencia = get_exces_potencia(cursor, uid, factura)


    # lectures (activa) ordenades per comptador i període
    lectures_a = {}
    lectures_r = {}
    lectures_m = []

    for lectura in factura.lectures_energia_ids:
        origens = get_origen_lectura(cursor, uid, lectura)
        if lectura.tipus == 'activa':
            lectures_a.setdefault(lectura.comptador,[])
            lectures_a[lectura.comptador].append((lectura.name[-3:-1],
                                                lectura.lect_anterior,
                                                lectura.lect_actual,
                                                lectura.consum,
                                                lectura.data_anterior,
                                                lectura.data_actual,
                                                origens[lectura.data_anterior],
                                                origens[lectura.data_actual],
                                                ))
        elif lectura.tipus == 'reactiva':
            lectures_r.setdefault(lectura.comptador,[])
            lectures_r[lectura.comptador].append((lectura.name[-3:-1],
                                                lectura.lect_anterior,
                                                lectura.lect_actual,
                                                lectura.consum,
                                                lectura.data_anterior,
                                                lectura.data_actual,
                                                origens[lectura.data_anterior],
                                                origens[lectura.data_actual],
                                                ))

    for lectura in factura.lectures_potencia_ids:
        lectures_m.append((lectura.name, lectura.pot_contract,
                           lectura.pot_maximetre ))

    for lects in lectures_a.values():
        sorted(lectures_a, key=lambda x: x[0])

    for lects in lectures_r.values():
        sorted(lectures_r, key=lambda x: x[0])

    for lects in lectures_m:
        sorted(lectures_m, key=lambda x: x[0])

    total_lectures_a = dict([(p, 0) for p in periodes_a])
    total_lectures_r = dict([(p, 0) for p in periodes_r])
    fact_potencia = dict([(p,0) for p in periodes_m])

    dies_factura = 1 + (datetime.strptime(factura.data_final, '%Y-%m-%d') - datetime.strptime(factura.data_inici, '%Y-%m-%d')).days
    diari_factura_actual_eur = factura.total_energia / (dies_factura or 1.0)
    diari_factura_actual_kwh = (factura.energia_kwh * 1.0) / (dies_factura or 1.0)

    for c, ls in lectures_a.items():
        for l in ls:
            total_lectures_a[l[0]] += l[3]

    for c, ls in lectures_r.items():
        for l in ls:
            total_lectures_r[l[0]] += l[3]

    for p in [(p.product_id.name, p.quantity) for p in factura.linies_potencia]:
        fact_potencia.update({p[0]: max(fact_potencia.get(p[0], 0), p[1])})

    #comprovem si té alguna lectura de maxímetre
    te_maximetre = (max([l[2] for l in lectures_m] or (0,)) > 0)


    # DADES PIE CHART
    pie_total = factura.amount_total - factura.total_lloguers
    pie_regulats = (factura.total_atr + total_altres)
    pie_impostos = float(factura.amount_tax)
    pie_costos = (pie_total - pie_regulats - pie_impostos )

    reparto = { 'i': ((pie_regulats * rep_BOE['i'])/100),
                'c': ((pie_regulats * rep_BOE['c'])/100),
                'o': ((pie_regulats * rep_BOE['o'])/100)
               }

    dades_reparto = [
        [[0, rep_BOE['i']], 'i', _(u"Incentius a les energies renovables, cogeneració i residus"), formatLang(reparto['i'])],
         [[rep_BOE['i'] , rep_BOE['i'] + rep_BOE['c']], 'c', _(u"Cost de xarxes de distribució i transport"), formatLang(reparto['c'])] ,
         [[rep_BOE['i'] + rep_BOE['c'], 100.00], 'o', _(u"Altres Costos regulats (inclosa anualitat del dèficit)"), formatLang(reparto['o'])]
        ]
    tipus_rect = factura.tipo_rectificadora
    abonadora = False
    rectificadora = False
    if tipus_rect in ['A', 'B', 'BRA']:
        abonadora = True
    elif tipus_rect in ['R', 'RA']:
        rectificadora = True

    es_proforma = factura.state in ['proforma', 'proforma2', 'grouped_proforma']

    garantia = False
    for l in altres_lines:
        if l.name.lower() == 'depósito de garantía':
            garantia = True
%>
<%def name="emergency_complaints(factura)">
    <div class="emergency_complaints">
        <div class="left-50 emergency">
            <h1>${_(u"AVARIES I URGÈNCIES")}</h1>
            <dl>
                <dt>${_(u"Empresa distribuïdora")}</dt>
                <dl>${polissa.distribuidora.name}</dl>
                <dt>${_(u"Telèfon")}</dt>
                <dl>${distri_phone}</dl>
                <dt>${_(u"Contracte d'accés")}</dt>
                <dl>${polissa.ref_dist or factura.polissa_id.ref_dist or ''}</dl>
            </dl>
        </div>
        <div class="right-50 complaints">
            <h1>${_(u"RECLAMACIONS")}</h1>
            <dl>
                <dt>${_(u"Empresa comercialitzadora")}</dt>
                <dl>${factura.company_id.partner_id.name}</dl>
                <dt>${_(u"Telèfon gratuït (9 a 18h)")}</dt>
                <dl>${factura.company_id.partner_id.address[0].phone}</dl>
                <dt>${_(u"Email")}</dt>
                <dl>${_(u"contacte@enova-energia.cat")}</dl>
            </dl>
        </div>
    </div>
    <div class="separator" ></div>
</%def>
<%def name="header_factura()">
    <div class="logo">
        <div class="left">
            <img class="enova" src="${addons_path}/giscedata_facturacio_comer_enova/report/${factura_lang}_logo_enova.png"/>
        </div>
        <div class="right">
            <div class="text">
                <p>${_(u"Energia Intel·ligent<br />per Empreses Intel·ligents")}</p>
            </div>
        </div>
    </div>
    <div class="separator" ></div>
</%def>
<%def name="origen_electricitat(factura)">\
    <div class="destination">
        <div class="titol_small seccio">${_(u"ORIGEN DE L'ELECTRICITAT")}</div>
        <p style="line-height: 1.0;">
        ${_(u"Si bé l'energia elèctrica que arriba a les nostres llars és indistingible de la que consumeixen els nostres veïns o altres consumidors "
        u"connectats al mateix sistema elèctric, ara sí que és possible garantir l'origen de de la producció d'energia elèctrica que vostè "
        u"consumeix. A aquestes efectes, es proporciona el desglossament de la mescla de tecnologies de producció nacional per axí comparar els "
        u"porcentatges del promig nacional amb els corresponents a l'energia venuda per E.Nova Energía.")}<br />
        </p>
    </div>
    <div class="mix">
        <%
            diagrama_origen = banner_obj.get_banner(
                cursor, uid, 'giscedata.facturacio.factura',
                factura.date_invoice, code='{}_origen_electricidad'.format(factura_lang)
            )

            grafico_radioactivo = banner_obj.get_banner(
                cursor, uid, 'giscedata.facturacio.factura',
                factura.date_invoice, code='{}_radioactivo'.format(factura_lang)
            )

            grafico_co2 = banner_obj.get_banner(
                cursor, uid, 'giscedata.facturacio.factura',
                factura.date_invoice, code='{}_CO2'.format(factura_lang)
            )
        %>
        <div class="titol" style="margin-top: 35px;"><span>${_(u"Mescla sistema espanyol 2018")}</span></div>
        <div class="graf">
            <img width="135" src="data:image/jpeg;base64,${diagrama_origen}">
        </div>
        <div class="titol" style="margin-top: 40px;"><span>${_(u"Mescla E.NOVA")}</span></div>
        <div class="graf mt_5">
            <img width="95" src="${addons_path}/giscedata_facturacio_comer_enova/report/${factura_lang}_enova_Mix.png"/>
        </div>
    </div>
    <div class="mix_taula">
        <table class="borderless">
            <thead>
                <tr>
                    <th>${_(u"TECNOLOGIES")}</th>
                    <th class="text_center" style="width: 35%">${_(u"Mescla sistema espanyol")}</th>
                    <th class="text_center" style="width: 30%">${_(u"Mescla E.NOVA")}</th>
                </tr>
            </thead>
            <tbody>
            <tr>
                <td>${_(u"Renovable")}</td><td class="text_center">38,20%</td><td class="text_center">100%</td>
            </tr>
            <tr>
                <td>${_(u"Cogeneració alta eficiència")}</td><td class="text_center">4,40%</td><td class="text_center">0%</td>
            </tr>
            <tr>
                <td>${_(u"Cogeneració")}</td><td class="text_center">6,90%</td><td class="text_center">0%</td>
            </tr>
            <tr>
                <td>${_(u"Nuclear")}</td><td class="text_center">20,70%</td><td class="text_center">0%</td>
            </tr>
            <tr>
                <td>${_(u"Carbó")}</td><td class="text_center">14,50%</td><td class="text_center">0%</td>
            </tr>
            <tr>
                <td>${_(u"CC Gas Natural")}</td><td class="text_center">11,70%</td><td class="text_center">0%</td>
            </tr>
            <tr>
                <td>${_(u"Fuel/Gas")}</td><td class="text_center">2,60%</td><td class="text_center">0%</td>
            </tr>
            <tr>
                <td>${_(u"Altres")}</td><td class="text_center">1,00%</td><td class="text_center">0%</td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="environment_impact">
        <h1 class="titol_small seccio">${_(u"IMPACTE MEDIAMBIENTAL")}</h1>
        <p class="env_imp_desc">
            ${_(u"L'impacte ambiental de la seva electricitat depèn de les fonts energètiques utilitzades per la seva "
                u"generació. En una escala de A a G on A indica el mínim impacte ambiental i G el màxim i que el nivell "
                u"mig nacional correspon al nivell D, l'energia comercialitzada per E.Nova Energía te els següents valors:")}
        </p>
        <div class="env_imp">
            <div class="left-50 env_imp_carbo" >
                <div class="env_titol"><p>${_(u"Emissions de CO2")}</div>
                <div class="env_img">
                    <img width="140" class="a_g" src="data:image/jpeg;base64,${grafico_co2}">
                </div>
            </div>
            <div class="right-50 env_imp_nuclear" >
                <div class="env_titol"><p>${_(u"Residus radioactius")}</div>
                <div class="env_img">
                    <img width="140" class="a_g" src="data:image/jpeg;base64,${grafico_radioactivo}">
                </div>
            </div>
        </div>
    </div>
    % if factura.has_tec271_line():
        <div class="destination_50">
            <%
                cups = polissa_obj.read(cursor, uid, factura.polissa_id.id, ['cups'])['cups'][1]
            %>
            <div class="supl">
                <h1 style="color:#3e7c00; font-weight: bold; font-size:1.2em; padding: 0em 0.5em 0.5em 0.5em;">${_(u"TAULA DETALLADA DELS SUPLEMENTS AUTONÒMICS 2013 (*)")}</h1>
                <%
                    html_table = sup_territorials_2013_comer_obj.get_info_html(cursor, uid, cups)
                %>
                ${html_table}
                ${_(u"En cas que l'import total de la regularització superi els dos euros, sense incloure impostos, aquest serà fraccionat en parts iguals superiors a 1€ per les empreses comercialitzadores en les factures que s'emeten en el plaç de 12 mesos a partir de la primera regularització.")}
                <br>
                ${_(u"* Taula informativa conforme a l'establert en la TEC/271/2019 del 6 de març, per la qual li informem dels paràmetres per al càlcul dels suplements territorials facilitats per la seva empresa distribuidora")} ${polissa.distribuidora.name}
                <br>
                <%
                    text = sup_territorials_2013_comer_obj.get_info_text(cursor, uid, cups)
                    if text:
                        text = text.format(distribuidora=polissa.distribuidora.name)
                %>
                ${text}
            </div>
        </div>
    % else:
         <div class="destination_50">
            <h1 class="titol_small seccio">${_(u"DESTÍ DE LA FACTURA")}</h1>
            <p>${_(u"El destí de l'import de la seva factura, %s euros, és el següent:") % formatLang(factura.amount_total)}</p>
            <div class="chart_desti" id="chart_desti_${factura.id}"></div>
            <p class="lloguers">${_(u"Als imports indicats en el diagrama se'ls ha d'afegir, en el seu cas, el lloguer dels equips de mesura i control")}</p>
        </div>
    % endif
</%def>
<div id="container">
    <div class="page">
        <%
            watermarks = []
            if es_proforma:
                watermarks.append(_(u"PROFORMA"))
            if abonadora:
                watermarks.append(_(u"ABONADORA"))
            if rectificadora:
                watermarks.append(_(u"RECTIFICADORA"))
        %>
        <div class="watermark">${'\n'.join(watermarks)}</div>
        ${header_factura()}
        <div class="capcalera">
            <div class="left-30">
                <div class="company_address">
                <span class="company_name">INICIATIVA E.NOVA S.L.</span><br/>
                %if factura.lang_partner != 'ca_ES':
                    R/ Galeras, 13, 1º, Oficina 5, 15705<br />
                    Santiago de Compostela<br />
                % endif
                CIF: B70412473<br />
                900834473<br />
                <span class="dades_web">${_(u"contacte@enova-energia.cat")}</span><br>
                <span class="dades_web">${_(u"www.enova-energia.cat")}</span>
                </div>
            </div>
            <div class="right-70">
                <div class="box titular">
                    <div class="box-header" >
                        <h1>${_(u"TITULAR")}</h1>
                    </div>
                    <div class="owner_data">
                        <table>
                            <tr class="less-padding">
                                <td class="first">${_(u"Nom/Raó social")}</td>
                                <td>${factura.address_contact_id.name}</td>
                            </tr>
                            <tr class="less-padding">
                                <td class="first">${_(u"Direcció")}</td>
                                <td>${factura.address_contact_id.street}</td>
                            </tr>
                            <tr class="less-padding">
                                <td class="first">${_(u"CP/Municipi")}</td>
                                <td>${factura.address_contact_id.zip} ${factura.address_contact_id.id_municipi.name}</td>
                            </tr>
                            <tr class="less-padding">
                                <td class="first">${_(u"Provincia")}</td>
                                <td>${factura.address_contact_id.state_id.name or ''}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="owner_data cif">
                        <table>
                            <tr>
                                <td class="first">${_(u"NIF/CIF")}</td>
                                <td>${polissa.titular.vat.replace('ES','')}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="separator" ></div>
        <!-- FACTURA  -->
        <div class="box">
            <div class="box-header" >
                <h1>${_(u"FACTURA")}</h1>
            </div>
            <div class="left-50">
                <div class="invoice_summary">
                    <table>
                    <%
                        per_energia = factura.total_energia
                        per_potencia = factura.total_potencia
                        penalitzacio = factura.total_reactiva
                        impost_elec = iese
                        lloguer_comptador = factura.total_lloguers
                        altres = total_altres
                        gestion = total_gestion
                        total = factura.amount_total
                        if abonadora:
                            per_energia *= -1
                            per_potencia *= -1
                            penalitzacio *= -1
                            impost_elec *= -1
                            lloguer_comptador *= -1
                            altres *= -1
                            gestion *= -1
                            total *= -1
                    %>
                    <tr><td><em>${_(u"Per energia")}</em></td><td class="e">${"%s&euro;" % formatLang(per_energia)}</td></tr>
                    <tr><td><em>${_(u"Per potència")}</em></td><td class="e">${"%s&euro;" % formatLang(per_potencia)}</td></tr>
                % if factura.total_reactiva > 0:
                    <tr><td><em>${_(u"Penalització per energia reactiva")}</em></td><td class="e">${"%s&euro;" % formatLang(penalitzacio)}</td></tr>
                % endif
                    <tr><td>
                        %if exempt:
                            <em>${_(u"Impost electricitat (85% exempt)")}</em>
                        %else:
                            <em>${_(u"Impost electricitat")}</em>
                        %endif
                    </td><td class="e">${"%s&euro;" % formatLang(impost_elec)}
                    </td></tr>
                    <tr><td><em>${_(u"Lloguer comptador")}</em></td><td class="e">${"%s&euro;" % formatLang(lloguer_comptador)}</td></tr>
                % if total_gestion > 0:
                    <tr><td><em>${_(u"Cost Gestió")}</em></td><td class="e">${"%s&euro;" % formatLang(gestion)}</td></tr>
                % endif
                % if total_altres != 0:
                    <tr><td><em>${_(u"Altres conceptes")}</em></td><td class="e">${"%s&euro;" % formatLang(altres)}</td></tr>
                % endif
                % for n, v in impostos.items():
                    % if "IVA" in n:
                        % if not("Exento" in n and garantia):
                            <%
                              if factura.lang_partner == 'gl_ES':
                                n = n.replace('IVA', 'IVE')
                            %>
                            % if abonadora:
                                <tr><td><em>${n}</em></td></em><td class="e">${"%s&euro;" % formatLang((v*-1))}</td></tr>
                            %else:
                                <tr><td><em>${n}</em></td></em><td class="e">${"%s&euro;" % formatLang(v)}</td></tr>
                            %endif
                        % endif
                    % endif
                % endfor
                    <tr class="total"><td>${_(u"TOTAL")}</td><td class="e">${"%s&euro;" % formatLang(total)}</td></tr>
                    </table>
                </div>
            </div>
            <div class="right-50">
                <div class="invoice_data">
                    <table>
                        <tr><td><em>${_(u"Número de factura")}</em></td><td>${factura.number or ''}</td></tr>
                        <tr><td><em>${_(u"Data")}</em></td><td>${factura.date_invoice}</td></tr>
                        <tr><td><em>${_(u"Data de venciment")}</em></td><td>${factura.date_due or ''}</td></tr>
                        <tr><td><em>${_(u"Període de facturació")}</em></td><td>${_(u"de %s a %s") % (factura.data_inici, factura.data_final)}</td></tr>
                        <tr><td><em>${_(u"Dies facturats")}</em></td><td>${dies_factura}</td></tr>
                        %if abonadora:
                            <tr><td><em>${_(u"Anul·la factura")}</em></td><td>${factura.ref.number}</td></tr>
                        %elif rectificadora:
                            <tr><td><em>${_(u"Rectifica factura")}</em></td><td>${factura.ref.number}</td></tr>
                        %endif
                    </table>
                </div>
            </div>
            <div class="separator"></div>
        </div>
        <!-- END FACTURA -->
        <!-- CONTRATO  -->
        <div class="box">
            <div class="box-header" >
                <h1>${_(u"CONTRACTE")}</h1>
            </div>
            <div class="left-50">
                <div class="contract_data">
                    <table id="info_contracte_taula">
                        <tr><td style="width: 160px;"><em>${_(u"Contracte del suministrament")}</em></td><td>${factura.polissa_id.name}</td></tr>
                        <tr><td style="width: 160px;"><em>${_(u"Contracte d'accés")}</em></td><td>${polissa.ref_dist or factura.polissa_id.ref_dist or ''}</td></tr>
                        <tr><td style="width: 160px;"><em>${_(u"Modalitat de contracte")}</em></td><td>${get_modalitat(factura.polissa_id.mode_facturacio, factura.polissa_id.add_management_fee)}</td></tr>
                        <tr><td style="width: 160px;"><em>${_(u"Tarifa")}</em></td><td>${polissa.tarifa.name}</td></tr>
                        <tr><td style="width: 160px;"><em>${_(u"Distribuïdora")}</em></td><td>${polissa.distribuidora.name}</td></tr>
                        <tr><td style="width: 160px;"><em>${_(u"Data alta")}</em></td><td>${formatLang(polissa.data_alta, date=True)}</td></tr>
                        <tr><td style="width: 160px;"><em>${_(u"Data final (sense permanència)")}</em></td><td>${formatLang(polissa.modcontractual_activa.data_final, date=True)}</td></tr>
                        <tr><td style="width: 160px;"><em>${_(u"CNAE")}</em></td><td>${polissa.cnae.name or ''}</td></tr>
                    </table>
                </div>
            </div>
            <div class="right-50">
                <div class="contract_data2">
                    <table>
                        <tr><td><em>${_(u"CUPS")}</em></td><td>${factura.cups_id.name}</td></tr>
                        <tr><td><em>${_(u"Adreça de subministrament")}</em></td><td>${factura.cups_id.direccio}</td></tr>
                    </table>
                    <table>
                        <thead>
                            <tr><th class="table_header" colspan="2">${_(u"FORMA DE PAGAMENT")}</th></tr>
                        </thead>
                        <tr><td><em>${_(u"Pagador")}</em></td><td>${factura.partner_id.name}</td></tr>
                        <tr><td><em>${_(u"NIF")}</em></td><td>${factura.partner_id.vat.replace('ES','')}</td></tr>
                        % if factura.payment_type:
                            <tr><td><em>${_(u"Entitat")}</em></td><td>${factura.partner_bank.bank.name if factura.payment_type.code != 'TRANSFERENCIA_CSB' else _(u"TRANSFERÈNCIA")}</td></tr>
                            <tr><td><em>${_(u"IBAN")}</em></td><td>${factura.partner_bank.iban[:-5]+"*****" if factura.payment_type.code != 'TRANSFERENCIA_CSB' else polissa.payment_mode_id.bank_id.iban+" (" + polissa.payment_mode_id.bank_id.owner_name +")"}</td></tr>
                        % else:
                            <tr><td><em>${_(u"Forma de pago")}</em></td><td>${_(u"Pendiente de definir")}</td></tr>
                        % endif
                   </table>
                </div>
            </div>
            <div class="separator" ></div>
            <div class="potencies_contractades">
                <div class="left-30 text">
                    <br />
                    <em>
                        ${_(u"Potencia contractada")}
                        % if te_maximetre:
                            ${_(u" (Maxímetre)")}
                        %else:
                            ${_(u" (ICP)")}
                        % endif:
                    </em>
                </div>
                <div class="right-70">
                    <table>
                        <tr class="header">
                            % for periode, potencia in sorted(potencies_periode.items(),key=itemgetter(0)):
                                <td>${periode}</td>
                            % endfor
                        </tr>
                        <tr>
                            % for periode, potencia in sorted(potencies_periode.items(),key=itemgetter(0)):
                                <td>${formatLang(potencia, digits=3)}
                                  % if potencia:
                                    kW
                                  % endif
                                </td>
                            % endfor
                        </tr>
                    </table>
                </div>
            </div>
            <div class="separator"></div>
        </div>
        <!-- END CONTRATO -->

        <!-- HISTORIC -->
        <div class="box historic">
            <div class="box-header" >
                <h1>${_(u"HISTORIAL ÚLTIMS 14 MESOS")}</h1>
            </div>
            <table class="consums_mig">
                <tr class="header">
                    <th></th>
                    % for k in sorted(consums_mig.keys()):
                        %try:
                            <td>${k}</td>
                        %except IndexError:
                            <td></td>
                        %endtry
                    % endfor
                    <td>${_(u"TOTAL")}</td>
                </tr>
                <tr>
                    <th>${_(u"CONSUMO MEDIO (kWh)")}</th>
                    % for k in sorted(consums_mig.keys()):
                        %try:
                            <% p = consums_mig.get(k, 0) %>
                            <td>${int(p)}</td>
                        %except IndexError:
                            <td></td>
                        %endtry
                    % endfor
                    <td>${int(sum(consums_mig.values()))}</td>
                </tr>
            </table>
            <div class="chart_consum_container">
                <div class="chart_consum" id="chart_consum_${factura.id}"></div>
            </div>
            <p>${(_(u"El seu consum mitjà diari els últims %.0f mesos (%s dies) ha estat de %s€ que corresponen a %s kWh/dia ")
                  % ((historic_dies*1.0 / 30) , historic_dies or 1.0, formatLang((total_historic_eur * 1.0) / (historic_dies or 1.0)), formatLang((total_historic_kw * 1.0) / (historic_dies or 1.0))))}
                <br />
                ${_(u"El seu consum acumulat el darrer any ha sigut de %s kWh") % formatLang(total_any, digits=0)}
            </p>
        </div>
        <p>${_(u"El seu consum mitjà diari en el període facturat ha sigut de %s€ que corresponen a %s kWh/dia (%s dies)") % (formatLang(diari_factura_actual_eur), formatLang(diari_factura_actual_kwh), dies_factura or 1)}</p>
        <!-- HISTORIC -->

        <div class="peu">
            ${emergency_complaints(factura)}
        </div>
        <div class="new_page" ></div>
        <div class="watermark">${'\n'.join(watermarks)}</div>
    <!-- END PAGE -->
    </div>
    <!-- LECTURES ACTIVA i GRÀFIC BARRES -->
    <div class="page">
        ${header_factura()}
        <!-- DETALLE FACTURA -->
        <div class="box detall_factura">
            <div class="box-header" >
                <h1>${_(u"DETALL FACTURA")}</h1>
            </div>
            <!-- ENERGIA -->
            <p><em>${_(u"Terme d'energia")}</em></p>
            <div class="periodes">
                <table>
                    <tr class="head">
                        <td class="col_periode"><!--Periode--></td>
                        <td class="col_2"><strong>${_(u"Consum")}</strong></td>
                        <td class="col_X"><!--X--></td>
                        <td class="col_4"><strong>${_(u"Preu")}</strong></td>
                        <td class="col_X"><!--X--></td>
                        <td class="col_dies"><!--DIAS--></td>
                        <td class="col_X"><!--=--></td>
                        <td class="col_total"><strong>${_(u"Total")}</strong></td>
                        <td class="col_peatges"><!--PEATGES--></td>
                        <td class="total col_total_verd">${_(u"Total")}</td>
                    </tr>
                    % if polissa.modcontractual_activa.mode_facturacio != 'index' or not polissa.modcontractual_activa.mostrar_coenova:
                        % for l in sorted(sorted(factura.linies_energia, key=attrgetter('data_desde')), key=attrgetter('name')):
                            <tr>

                                <td class="col_periode">
                                    <strong>${l.name}</strong>
                                    %if check_ajuste(l, factura.lectures_energia_ids, 'activa', factura.tarifa_acces_id.id):
                                        *
                                        <%
                                            show_ajust = True
                                        %>
                                    %endif
                                </td>
                                <td class="col_2">${int(l.quantity)} kWh</td>
                                <td class="col_X">X</td>
                                <td class="col_4">${formatLang(l.price_unit_multi, digits=6)} €/kWh</td>
                                <td class="col_X"><!--X--></td>
                                <td class="col_dies"><!--DIAS--></td>
                                <td class="col_X">=</td>
                                <td class="col_total"><strong>${_(u"%s €") % formatLang(l.price_subtotal)}</strong></td>
                                <td class="col_peatges">(${_(u"Dels quals peatges: %s €") % formatLang(l.atrprice_subtotal)})</td>
                                <td class="total col_total_verd"></td>
                            </tr>
                        % endfor
                        <tr>
                            <td class="col_periode"></td>
                            <td class="col_2"></td>
                            <td class="col_X"></td>
                            <td class="col_4"></td>
                            <td class="col_X"></td>
                            <td class="col_dies"></td>
                            <td class="col_X"></td>
                            <td class="col_total"></td>
                            <td class="col_peatges"></td>
                            <td class="total col_total_verd">${"%s&euro;" % formatLang(per_energia)}</td>
                        </tr>
                    </table>
                % else:
                        <%

                            consumo_total = 0
                            precio_coenova_total = 0
                            precio_total = 0
                            coenova = 0
                        %>
                        % for l in sorted(sorted(factura.linies_energia, key=attrgetter('data_desde')), key=attrgetter('name')):
                            <%
                                coenova = get_coenova(cursor, uid, tarifa_elect_comer, l)
                                consumo_total += int(l.quantity)
                                consumo_enova = int(l.quantity) * coenova
                                precio_coenova_total += consumo_enova
                                consumo_parcial = l.price_subtotal - consumo_enova
                                coste_energia = consumo_parcial / (int(l.quantity) or 1.0)
                                precio_total += consumo_parcial
                            %>
                            <tr>
                                <td class="col_periode">
                                    <strong>${l.name}</strong>
                                    %if check_ajuste(l, factura.lectures_energia_ids, 'activa', factura.tarifa_acces_id.id):
                                        *
                                        <%
                                            show_ajust = True
                                        %>
                                    %endif
                                </td>
                                <td class="col_2">${int(l.quantity)} kWh</td>
                                <td class="col_X">X</td>
                                <td class="col_4">${formatLang(coste_energia, digits=6)} €/kWh</td>
                                <td class="col_X"><!--X--></td>
                                <td class="col_dies"><!--DIAS--></td>
                                <td class="col_X">=</td>
                                <td class="col_total"><strong>${_(u"%s €") % formatLang(consumo_parcial)}</strong></td>
                                <td class="col_peatges">(${_(u"Dels quals peatges: %s €") % formatLang(l.atrprice_subtotal)})</td>
                                <td class="total col_total_verd"></td>
                            </tr>
                        % endfor
                        <tr>
                            <td class="col_periode"></td>
                            <td class="col_2"></td>
                            <td class="col_X"></td>
                            <td class="col_4"></td>
                            <td class="col_X"></td>
                            <td class="col_dies"></td>
                            <td class="col_X"></td>
                            <td class="col_total"></td>
                            <td class="col_peatges"></td>
                            <td class="total col_total_verd">${"%s&euro;" % formatLang(precio_total)}</td>
                        </tr>
                    </table>
                    <div class="separator_energia resum_coenova" ></div>
                    <table class="resum_coenova">
                        <tr>
                            <td class="col_enova">${_(u"COEnova (Honoraris de gestió)")}</td>
                            <td class="col_2">${int(consumo_total)} kWh</td>
                            <td class="col_X">X</td>
                            <td class="col_valor_impost">${formatLang(coenova, digits=4)} €/kWh</td>
                            <td class="col_X">=</td>
                            <td class="col_total"></td>
                            <td class="total col_total_verd">${_(u"%s €") % formatLang(precio_coenova_total)}</td>
                        </tr>
                    </table>
                % endif
            </div>

            <div class="separator_energia" ></div>
            <p><em>${_(u"Terme de potència")}</em></p>
            <div class="periodes">
                <table>
                    <tr class="head">
                        <td class="col_periode"><!--Periode--></td>
                        <td class="col_2"><strong>${_(u"Consum")}</strong></td>
                        <td class="col_X"><!--X--></td>
                        <td class="col_4"><strong>${_(u"Preu")}</strong></td>
                        <td class="col_X"><!--X--></td>
                        <td class="col_dies"><strong>${_(u"Dies")}</strong></td>
                        <td class="col_X"><!--=--></td>
                        <td class="col_total">${_(u"Total")}</td>
                        <td class="col_peatges"><!--PEATGES--></td>
                        <td class="total col_total_verd"></td>
                    </tr>
                    % for l in sorted(sorted(factura.linies_potencia, key=attrgetter('data_desde')), key=attrgetter('name')):
                        <tr>
                            <td class="col_periode"><strong>${l.name}</strong></td>
                            <td class="col_2">${formatLang(l.quantity, digits=3)} kW</td>
                            <td class="col_X">X</td>
                            <td class="col_4">${formatLang(l.price_unit_multi, digits=6)} ${_(u"€/kW dia")}</td>
                            <td class="col_X">X</td>
                            <td class="col_dies">${dies_factura}</td>
                            <td class="col_X">=</td>
                            <td class="col_total"><strong>${_(u"%s €") % formatLang(l.price_subtotal)}</strong></td>
                            <td class="col_peatges">(${_(u"Dels quals peatges: %s €") % formatLang(l.atrprice_subtotal)})</td>
                            <td class="total col_total_verd"></td>
                        </tr>
                    % endfor
                    <tr>
                        <td class="col_periode"></td>
                        <td class="col_2"></td>
                        <td class="col_X"></td>
                        <td class="col_4"></td>
                        <td class="col_X"></td>
                        <td class="col_dies"></td>
                        <td class="col_X"></td>
                        <td class="col_total"></td>
                        <td class="col_peatges"></td>
                        <td class="total col_total_verd">${"%s&euro;" % formatLang(per_potencia)}</td>
                    </tr>
                </table>
            </div>
            % if factura.total_reactiva:
                <!-- REACTIVA -->
                <div class="separator_energia" ></div>
                <p><em>${_(u"Energia reactiva")}</em></p>
                <div class="periodes">
                    <table>
                        <tr class="head">
                            <td class="col_periode"><!--Periode--></td>
                            <td class="col_2"><strong>${_(u"Excés")}</strong></td>
                            <td class="col_X"><!--X--></td>
                            <td class="col_4"><strong>${_(u"Preu")}</strong></td>
                            <td class="col_X"><!--X--></td>
                            <td class="col_dies"><!--DIAS--></td>
                            <td class="col_X"><!--=--></td>
                            <td class="col_total">${_(u"Total")}</td>
                            <td class="col_peatges"><!--PEATGES--></td>
                            <td class="total col_total_verd"></td>
                        </tr>
                        % for l in sorted(sorted(factura.linies_reactiva, key=attrgetter('data_desde')), key=attrgetter('name')):
                            <tr>
                                <td class="col_periode">
                                    <strong>${l.name}</strong>
                                    %if check_ajuste(l, factura.lectures_energia_ids, 'reactiva', factura.tarifa_acces_id.id):
                                        *
                                        <%
                                            show_ajust = True
                                        %>
                                    %endif
                                </td>
                                <td class="col_2">${formatLang(l.quantity)} kVArh</td>
                                <td class="col_X">X</td>
                                <td class="col_4">${formatLang(l.price_unit_multi, digits=6)} €/kVArh</td>
                                <td class="col_X"><!--X--></td>
                                <td class="col_dies"><!--DIAS --></td>
                                <td class="col_X">=</td>
                                <td class="col_total"><strong>${_(u"%s €") % formatLang(l.price_subtotal)}</strong></td>
                                <td class="col_peatges"></td>
                                <td class="total col_total_verd"></td>
                            </tr>
                        % endfor
                        <tr>
                            <td class="col_periode"></td>
                            <td class="col_2"></td>
                            <td class="col_X"></td>
                            <td class="col_4"></td>
                            <td class="col_X"></td>
                            <td class="col_dies"></td>
                            <td class="col_X"></td>
                            <td class="col_total"></td>
                            <td class="col_peatges"></td>
                            <td class="total col_total_verd">${"%s&euro;" % formatLang(penalitzacio)}</td>
                        </tr>
                    </table>
                </div>
            % endif
            % if len(exces_potencia) :
                <!-- EXCES POTENCIA -->
                <div class="separator_energia" ></div>
                <p><em>${_(u"Excés potència")}</em></p>
                <div>
                    <table>
                        <tr class="head">
                            <td class="col_periode"><!--Periode--></td>
                            <td class="col_2"><strong>${_(u"Excés")}</strong></td>
                            <td class="col_X"><!--X--></td>
                            <td class="col_4"><strong>${_(u"Preu")}</strong></td>
                            <td class="col_X"><!--X--></td>
                            <td class="col_dies"><!--DIAS--></td>
                            <td class="col_X"><!--=--></td>
                            <td class="col_total">${_(u"Total")}</td>
                            <td class="col_peatges"><!--PEATGES--></td>
                            <td class="total col_total_verd"></td>
                        </tr>
                        <% total_exces = 0.0 %>
                        % for l in sorted(sorted(exces_potencia, key=attrgetter('data_desde')), key=attrgetter('name')):
                            <tr>
                                <td class="col_periode"><strong>${l.name}</strong></td>
                                <td class="col_2">${formatLang(l.quantity)} kW</td>
                                <td class="col_X">X</td>
                                <td class="col_4">${formatLang(l.price_unit_multi, digits=6)} €/kW</td>
                                <td class="col_X"><!--X--></td>
                                <td class="col_dies"><!--DIAS --></td>
                                <td class="col_X">=</td>
                                <td class="col_total"><strong>${_(u"%s €") % formatLang(l.price_subtotal)}</strong></td>
                                <td class="col_peatges"></td>
                                <td class="total col_total_verd"></td>
                            </tr>
                            <% total_exces += l.price_subtotal %>
                        % endfor
                        <tr>
                            <td class="col_periode"></td>
                            <td class="col_2"></td>
                            <td class="col_X"></td>
                            <td class="col_4"></td>
                            <td class="col_X"></td>
                            <td class="col_dies"></td>
                            <td class="col_X"></td>
                            <td class="col_total"></td>
                            <td class="col_peatges"></td>
                            <td class="total col_total_verd">${"%s&euro;" % formatLang(total_exces)}</td>
                        </tr>
                    </table>
                </div>
            % endif
            <div class="separator_energia big" ></div>
            <table>
            % for l in iese_lines:
                <tr>
                    <td class="col_impostos">
                        %if exempt:
                            <em>${_(u"Impost de l'electricitat (85% exempt)")}</em>
                        %else:
                            <em>${_(u"Impost de l'electricitat")}</em>
                        %endif
                    </td>
                    <td class="col_2">${_(u"%s €") % (formatLang(l.base_amount))}</td>
                    <td class="col_X">X</td>
                    <td class="col_valor_impost">${u"5,11269632%"}</td>
                    <td class="col_X">=</td>
                    <td class="col_total"></td>
                    <td class="col_peatges"></td>
                    <td class="total col_total_verd">${_(u"%s €") % formatLang(l.tax_amount)}</td>
                </tr>
            % endfor
            % for l in lloguer_lines:
                <tr>
                    <td class="col_impostos"><em>${_(u"Lloguer de comptador")}</em></td>
                    <td class="col_2"></td>
                    <td class="col_X"></td>
                    <td class="col_valor_impost"></td>
                    <td class="col_X">=</td>
                    <td class="col_total"></td>
                    <td class="col_peatges"></td>
                    <%
                        detall_lloguer = l.price_subtotal
                        if abonadora:
                            detall_lloguer *= -1
                    %>
                    <td class="total col_total_verd">${_(u"%s €") % formatLang(detall_lloguer)}</td>
                </tr>
            % endfor
            % for l in gestion_lines:
                <tr>
                    <td class="col_impostos"><em>${l.name}</em></td>
                    <td class="col_2">${_(u"%s €/mes") % formatLang(l.price_unit_multi * 365 / 12)}</td>
                    <td class="col_X">x</td>
                    <td class="col_valor_impost">${_(u"%s mesos") % formatLang(dies_factura * 12.0 / 365, 6)}</td>
                    <td class="col_X">=</td>
                    <td class="col_total"></td>
                    <td class="col_peatges"></td>
                    <%
                        detall_gestion = l.price_subtotal
                        if abonadora:
                            detall_gestion *= -1
                    %>
                    <td class="total col_total_verd">${_(u"%s €") % formatLang(detall_gestion)}</td>
                </tr>
            % endfor
            % for l in altres_lines:
                <tr>
                    <td class="col_impostos"><em>${l.name}</em></td>
                    <td class="col_2"></td>
                    <td class="col_X"></td>
                    <td class="col_valor_impost"></td>
                    <td class="col_X">=</td>
                    <td class="col_total"></td>
                    <td class="col_peatges"></td>
                    <%
                        detall_altres = l.price_subtotal
                        if abonadora:
                            detall_altres *= -1
                    %>
                    <td class="total col_total_verd">${_(u"%s €") % formatLang(detall_altres)}</td>
                </tr>
            % endfor
            </table>
            <div class="separator_energia big" ></div>
            <table>
            % for l in iva_lines:
                %if not("Exento" in l.name and garantia):
                    <tr>
                        <%
                            if 'IVA' in l.name and factura.lang_partner == 'gl_ES':
                                name = l.name.replace('IVA', 'IVE')
                            else:
                                name = l.name
                        %>
                        <td class="col_impostos"><em>${name}</em></td>
                        <td class="col_2">${_(u"%s €") % (formatLang(l.base))}</td>
                        <td class="col_X">X</td>
                        <td class="col_valor_impost">21%</td>
                        <td class="col_X">=</td>
                        <td class="col_total"></td>
                        <td class="col_peatges"></td>
                        <%
                            detall_iva = l.amount
                            if abonadora:
                                detall_iva *= -1
                        %>
                        <td class="total col_total_verd">${_(u"%s €") % formatLang(detall_iva)}</td>
                    </tr>
                %endif
            % endfor
            </table>
            <div class="separator_energia final" ></div>
            <table class="final">
                <tr>
                    <td class="total_factura"><em>${_(u"TOTAL FACTURA")}</em></td>
                    <td class="total col_total_verd">${_(u"%s&euro;") % formatLang(total)}</td>
                </tr>
            </table>
        </div>
        <p class="preus_publicats">
            %if show_ajust:
                <span style="color: black;">
                    ${_(u"* Aquest consum està fixat amb un ajustament per període per part de la Distribuidora")}
                </span>
                <br />
            %endif
            ${_(u"Els preus dels termes de peatge d'accés són els publicats a (%s)") % iet_peatges}<br />
            ${_(u"Els preus del lloguer dels comptadors són els establerts a (%s)") % itc_lloguer}<br />
        </p>
        <!-- END DETALLE FACTURA -->
        % if polissa.modcontractual_activa.mode_facturacio == 'index' and len(factura.linies_reactiva) == 0:
            <div class="preus_indexats">
                <p>${_(u"(*) L'import mensual del terme d'energia es calcularà a nivell horari, "
                u"multiplicant el preu horari final de l'energia pel consum horari, per totes les hores del mes. "
                u"El preu horari final de l'energia s'obté mitjançant la següent suma: ")}</p>
                <div class="formula">
                    <p>PHF = ([PMD + POS] + [PC3]) ∗ (1 + ${_(u"Perdues")}) + PA + COEnova + IMU + D</p>
                </div>
                <div class="llistat">
                    <div class="left">
                        <dl>
                            <dt>${_(u"PHF")}</dt><dd>${_(u"Preu horari final")}</dd>
                            <dt>${_(u"PMD")}</dt><dd>${_(u"Preu mercat diari")}</dd>
                            <dt>${_(u"POS")}</dt><dd>${_(u"Preu operador del sistema")}</dd>
                        </dl>
                    </div>
                    <div class="left">
                        <dl>
                            <dt>${_(u"PC3")}</dt><dd>${_(u"Pagaments per capacitat (BOE)")}</dd>
                            <dt>${_(u"Perdues")}</dt><dd>${_(u"Regulades (BOE)")}</dd>
                            <dt>${_(u"PA")}</dt><dd>${_(u"Peatge accés energia (BOE)")}</dd>
                        </dl>
                    </div>
                    <div class="left">
                        <dl>
                            <dt>${_(u"COEnova")}</dt><dd>${_(u"Cost operatiu d'E.NOVA")}</dd>
                            <dt>${_(u"IMU")}</dt><dd>${_(u"Impost municipal")}</dd>
                            <dt>${_(u"D")}</dt><dd>${_(u"Cost desviaments")}</dd>
                        </dl>
                    </div>
                    <div class="separator" ></div>
                </div>
            </div>
        % endif
        <!-- LECTURES -->
        <div class="box detall_factura">
            <div class="box-header" >
                <h1>${_(u"MESURES")}</h1>
            </div>
            <div>
                <table class="lectures">
                    <!-- ACTIVA -->
                    <tr class="header">
                        <th><em>${_(u"ENERGIA")}</em></th>
                        % for i, elem in enumerate(periodes):
                            %try:
                                <% p = periodes_a[i] %>
                                <td>${periodes_a[i]}</td>
                            %except IndexError:
                                <td></td>
                            %endtry
                        % endfor
                    </tr>
                    % for comptador in sorted(lectures_a):
                        <tr>
                            <th>${_(u"Número de comptador")}</th>
                                % for i, elem in enumerate(periodes):
                                    %try:
                                        <% p = periodes_a[i] %>
                                        <td>${comptador}</td>
                                    %except IndexError:
                                        <td></td>
                                    %endtry
                                % endfor
                        </tr>
                        <tr>
                            <th>${_(u"Lectura anterior")} ${lectures_a[comptador][0][4]} (${lectures_a[comptador][0][6]})</th>
                            % for i, elem in enumerate(periodes):
                                %try:
                                    <% p = periodes_a[i] %>
                                    <td>${int(lectures_a[comptador][i][1])} kWh</td>
                                %except IndexError:
                                    <td></td>
                                %endtry
                            % endfor
                        </tr>
                        <tr>
                            <th>${_(u"Lectura actual")} ${lectures_a[comptador][0][5]} (${lectures_a[comptador][0][7]})</th>
                            % for i, elem in enumerate(periodes):
                                %try:
                                    <% p = periodes_a[i] %>
                                    <td>${int(lectures_a[comptador][i][2])} kWh</td>
                                %except IndexError:
                                    <td></td>
                                %endtry
                            % endfor
                        </tr>
                    % endfor
                    <tr class="resum_periode">
                        <th>${_(u"Total període")}</th>
                        % for i, elem in enumerate(periodes):
                            %try:
                                <% p = periodes_a[i] %>
                                <td>${formatLang(total_lectures_a[p], digits=0)} kWh</td>
                            %except IndexError:
                                <td></td>
                            %endtry
                        %endfor
                    </tr>
                    <!-- ACTIVA -->
                    <!-- REACTIVA -->
                    % if len(periodes_r) or te_maximetre:
                        <tr><td colspan="7" class="separator_energia big"></td></tr>
                        <tr class="header">
                            <th><em>${_(u"ENERGIA REACTIVA")}</em></th>
                            % for i, elem in enumerate(periodes):
                                %try:
                                    <% p = periodes_r[i] %>
                                    <td>${periodes_r[i]}</td>
                                %except IndexError:
                                    <td></td>
                                %endtry
                            % endfor
                        </tr>
                        % for comptador in sorted(lectures_r):
                            <tr>
                                <th>${_(u"Número de comptador")}</th>
                                    % for i, elem in enumerate(periodes):
                                        %try:
                                            <% p = periodes_r[i] %>
                                            <td>${comptador}</td>
                                        %except IndexError:
                                            <td></td>
                                        %endtry
                                    % endfor
                            </tr>
                            <tr>
                                <th>${_(u"Lectura anterior")} ${lectures_r[comptador][0][4]} (${lectures_r[comptador][0][6]})</th>
                                % for i, elem in enumerate(periodes):
                                    %try:
                                        <% p = periodes_r[i] %>
                                        <td>${int(lectures_r[comptador][i][1])} kWh</td>
                                    %except IndexError:
                                        <td></td>
                                    %endtry
                                % endfor
                            </tr>
                            <tr>
                                <th>${_(u"Lectura actual")} ${lectures_r[comptador][0][5]})(${lectures_r[comptador][0][7]})</th>
                                % for i, elem in enumerate(periodes):
                                    %try:
                                        <% p = periodes_r[i] %>
                                        <td>${int(lectures_r[comptador][i][2])} kWh</td>
                                    %except IndexError:
                                        <td></td>
                                    %endtry
                                % endfor
                            </tr>
                        % endfor
                        <tr class="resum_periode">
                            <th>${_(u"Total període")}</th>
                            % for i, elem in enumerate(periodes):
                                %try:
                                    <% p = periodes_r[i] %>
                                    <td>${formatLang(total_lectures_r[p], digits=0)} kWh</td>
                                %except IndexError:
                                    <td></td>
                                %endtry
                            % endfor
                        </tr>
                    %endif
                    <!-- REACTIVA -->
                    <!-- MAXIMETRE -->
                    % if te_maximetre:
                        <tr><td colspan="7" class="separator_energia big"></td></tr>
                        <tr class="header">
                            <th><em>${_(u"MAXÍMETRE")}</em></th>
                            % for i, elem in enumerate(periodes):
                                %try:
                                    <% p = periodes_m[i] %>
                                    <td>${p}</td>
                                %except IndexError:
                                    <td></td>
                                %endtry
                            % endfor
                        </tr>
                        <tr>
                            <th>${_(u"Potència contractada")}</th>
                            % for i, elem in enumerate(periodes):
                                %try:
                                    <% lec = lectures_m[i] %>
                                    <td>${formatLang(float(lec[1]),digits=3)}</td>
                                %except IndexError:
                                    <td></td>
                                %endtry
                            % endfor
                        </tr>
                        <tr>
                            <th>${_(u"Lectura maxímetre")}</th>
                            % for i, elem in enumerate(periodes):
                                %try:
                                    <% lec = lectures_m[i] %>
                                    <td>${formatLang(float(lec[2]),digits=3)}</td>
                                %except IndexError:
                                    <td></td>
                                %endtry
                            % endfor
                        </tr>
                        <tr class="resum_periode">
                            <th>${_(u"Potència facturada període")}</th>
                            % for i, elem in enumerate(periodes):
                                %try:
                                    <% p = periodes_m[i] %>
                                    <td>${formatLang(float(fact_potencia[p]),digits=3)}</td>
                                %except IndexError:
                                    <td></td>
                                %endtry
                            % endfor
                        </tr>
                    %endif
                    <!-- MAXIMETRE -->
                </table>
            </div>
        </div> <!-- END BOX -->
        <!-- LECTURES ACTIVA i GRÀFIC BARRES -->
        ${origen_electricitat(factura)}
    </div>
    <!-- END PAGE -->
</div>
<script>
var pie_total = ${pie_total};
var pie_data = [{val: ${pie_regulats}, perc: 30, code: "REG"},
                {val: ${pie_costos}, perc: 55, code: "PROD"},
                {val: ${pie_impostos},  perc: 15 ,code: "IMP"}
               ];

var pie_etiquetes = {'REG': {t: ['${formatLang(float(pie_regulats))}€','${_(u"Costos regulats")}'], w:100},
                     'IMP': {t: ['${formatLang(float(pie_impostos))}€','${_(u"Impostos aplicats")}'], w:100},
                     'PROD': {t: ['${formatLang(float(pie_costos))}€','${_(u"Costos de producció electricitat i marge de comercialització")}'], w:150}
                    };

var reparto = ${json.dumps(reparto)}
var dades_reparto = ${json.dumps(dades_reparto)}

var factura_id = ${factura.id}
var data_consum = ${json.dumps(sorted(historic_js, key=lambda h: h['mes']))}
var two_periods_or_greater = ${len(periodes_a)>1 and 'true' or 'false'}

</script>
<script src="${addons_path}/giscedata_facturacio_comer_enova/report/consum.js"></script>
<script src="${addons_path}/giscedata_facturacio_comer_enova/report/pie.js"></script>
</div>
<%
comptador_factures += 1;
%>
% if comptador_factures<len(objects):
    <p style="page-break-after:always"></p>
% endif
%endfor
</body>
</html>
