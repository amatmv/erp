# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscedataPolissa(osv.osv):

    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    _columns = {
        'mostrar_coenova': fields.boolean(
            string='Mostrar COEnova',
            readonly=True,
            states={
                'esborrany': [('readonly', False)],
                'validar': [('readonly', False), ('required', True)],
                'modcontractual': [('readonly', False), ('required', True)]
            }
        ),
        'add_management_fee': fields.boolean(
            string='Añadir coste de gestión',
            readonly=True,
            states={
                'esborrany': [('readonly', False)],
                'validar': [('readonly', False), ('required', True)],
                'modcontractual': [('readonly', False), ('required', True)]
            }
        ),
        'management_fee': fields.float(
            string='Coste de gestión [€/día]',
            help='Coste de gestión (CG) en €/día',
            digits=(8, 6),
            readonly=True,
            states={
                'esborrany': [('readonly', False)],
                'validar': [('readonly', False)],
                'modcontractual': [('readonly', False)]
            }
        )
    }

    _defaults = {
        'mostrar_coenova': lambda *a: False,
        'add_management_fee': lambda *a: False,
        'management_fee': lambda *a: 0
    }

GiscedataPolissa()

class GiscedataPolissaModcontractual(osv.osv):
    _name = 'giscedata.polissa.modcontractual'
    _inherit = 'giscedata.polissa.modcontractual'

    _columns = {
        'mostrar_coenova': fields.boolean(string='Mostrar COEnova'),
        'add_management_fee': fields.boolean(string='Añadir coste de gestión'),
        'management_fee': fields.float(
            string='Coste de gestión [€/día]', digits=(8, 6)
        )
    }

GiscedataPolissaModcontractual()