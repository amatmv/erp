# -*- coding: utf-8 -*-
{
    "name": "Reports Facturació Enova (Comercialitzadora)",
    "description": """Reports Facturació Enova (Comercialitzadora)""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Extrareports",
    "depends":[
        "base",
        "account_invoice_base",
        "giscedata_facturacio_comer",
        "giscedata_facturacio_indexada",
        "giscedata_polissa_comer",
        "c2c_webkit_report",
        "jasper_reports"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_facturacio_comer_data.xml",
        "giscedata_facturacio_comer_report.xml",
        "giscedata_polissa_view.xml"
    ],
    "active": False,
    "installable": True
}
