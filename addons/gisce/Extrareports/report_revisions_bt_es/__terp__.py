# -*- coding: utf-8 -*-
{
    "name": "Report Revisions BT (ES)",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Línies AT",
    "depends":[
        "giscedata_revisions_bt"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "report_revisions_bt_es_report.xml"
    ],
    "active": False,
    "installable": True
}
