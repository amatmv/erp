<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="descarrec_list" />
  </xsl:template>
  
  <xsl:template match="descarrec_list">
    <xsl:apply-templates select="descarrec" />
  </xsl:template>

  <xsl:template match="descarrec">
    <document>
      <template pageSize="(21cm, 29.7cm)">
      	<pageTemplate id="main">
      		<frame id="first" x1="1cm" y1="1cm" width="19cm" height="247mm"/>
      	</pageTemplate>
        <pageTemplate id="cap">
          <pageGraphics>
          	<image file="addons/custom/logo.png" width="20mm" height="20mm" x="1cm" y="26.6cm" />
          	<setFont name="Helvetica-Bold" size="30" />
          	<drawString x="3.2cm" y="27.8cm"><xsl:value-of select="//corporate-header/corporation/rml_header1" /></drawString>
          	<setFont name="Helvetica" size="10" />
          	<drawString x="3.2cm" y="27.1cm"><xsl:value-of select="//corporate-header/corporation/rml_footer1" /></drawString>
          	<lineMode width="2" />
          	<stroke color="blue"/>
          	<lines>
          		3.2cm 27cm 20cm 27cm
          	</lines>
          	<setFont name="Helvetica" size="8" />
          	<drawString x="3.2cm" y="26.6cm"><xsl:value-of select="//corporate-header/corporation/name" /></drawString>
          	<drawRightString x="20cm" y="26.7cm"><xsl:value-of select="//corporate-header/corporation/address[type='default']/street" /></drawRightString>
          	<drawRightString x="20cm" y="26.4cm"><xsl:value-of select="concat(//corporate-header/corporation/address[type='default']/zip, ' ', //corporate-header/corporation/address[type='default']/city)" /> (<xsl:value-of select="//corporate-header/corporation/address[type='default']/state" />)</drawRightString>
          	<drawRightString x="20cm" y="26.1cm">Tel. <xsl:value-of select="//corporate-header/corporation/address[type='default']/phone" /> - Fax <xsl:value-of select="//corporate-header/corporation/address[type='default']/fax" /></drawRightString>
          	<drawRightString x="20cm" y="25.8cm">E-mail: <xsl:value-of select="//corporate-header/corporation/address[type='default']/email" /></drawRightString>
          	<rotate degrees="90" />
            <setFont name="Helvetica" size="7" />
            <drawString x="60mm" y="-4mm"><xsl:value-of select="//corporate-header/corporation/rml_footer2" /></drawString>
          </pageGraphics>
          <frame id="first" x1="1cm" y1="1cm" width="19cm" height="210mm"/>
        </pageTemplate>
      </template>
      
      <stylesheet>
      
      <paraStyle name="text"
      		fontName="Helvetica"
      		fontSize="10" />

      </stylesheet>
    
      <story>
      	<blockTable colWidths="11cm,8cm">
      		<tr>
      			<td></td>
      			<td>
      				<para style="text" alignment="center"><b>DELEGACI� TERRITORIAL D'IND�STRIA COMER� I TURISME</b></para>
      				<para style="text" alignment="center">Avd. Segre, 7</para>
      				<para style="text" alignment="center"><u>25007 LLEIDA</u></para>
      			</td>
      		</tr>
      	</blockTable>
      	<para style="text"><xsl:value-of select="data" /></para>
      	<para style="text">Interrupcions programades de subministrament el�ctric</para>
      	<spacer length="5cm" />
      	<para style="text" fontSize="12">Senyors :</para>
      	<spacer length="0.5cm" />
      	<para style="text" fontSize="12">D'acord amb el que disposa l'article 101.3 del R.D. 1955/2000, us comuniquem les interrupcions programades que es detallen en el full adjunt.</para>
      	<spacer length="3cm" />
      	<para style="text" fontSize="12">Rebeu una salutaci� molt atenta.</para>
      	<setNextTemplate name="cap" />      	
      	<nextPage />
      	<para style="text" alignment="center" fontSize="20">INTERRUPCI� TEMPORAL PROGRAMADA</para>
      	<spacer length="12" />
      	<para style="text" alignment="center" fontSize="20">DE SUBMINISTRAMENT EL�CTRIC</para>
      	
      	<spacer length="2cm" />
      	<para style="text"><b t="1">Dia i hora d'inici</b>: <xsl:value-of select="concat(substring(data_inici, 9, 2),'/',substring(data_inici, 6, 2),'/',substring(data_inici, 1, 4), ' ', substring(data_inici, 12, 5))" /></para>
      	<para style="text"><b t="1">Dia i hora de finalitzaci�</b>: <xsl:value-of select="concat(substring(data_final, 9, 2),'/',substring(data_final, 6, 2),'/',substring(data_final, 1, 4), ' ', substring(data_final, 12, 5))" /></para>
      	<spacer length="1cm" />
      	<para style="text" t="1" fontName="Helvetica-Bold">CENTRES DE TRANSFORMACI� AFECTATS:</para>
      	<xpre style="text"><xsl:value-of select="cts_afectats" /></xpre>
      	<spacer length="0.5cm" />
      	<para style="text" t="1" fontName="Helvetica-Bold">NUCLIS AFECTATS:</para>
      	<xpre style="text"><xsl:value-of select="nuclis_afectats" /></xpre>
      	<spacer length="0.5cm" />
      	<para style="text" t="1" fontName="Helvetica-Bold">Causes:</para>
      	<xpre style="text"><xsl:value-of select="causes" /></xpre>
      </story>
    </document>

    </xsl:template>

</xsl:stylesheet>
