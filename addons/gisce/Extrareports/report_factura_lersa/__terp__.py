# -*- coding: utf-8 -*-
{
    "name": "Report facturació LERSA comercialitzadora",
    "description": """Definició del report de factures de LERSA Energia""",
    "version": "0-dev",
    "author": "GISCE Enginyeria, SL",
    "category": "Comercialitzadora",
    "depends": [],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
