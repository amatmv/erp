# -*- coding: utf-8 -*-
{
    "name": "Report Trams AT amb Origen i Final",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Reports Extra",
    "depends":[
        "giscedata_at"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "report_at_trams_origen_final_report.xml"
    ],
    "active": False,
    "installable": True
}
