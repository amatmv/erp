<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="trams"/>
  </xsl:template>

  <xsl:template match="trams">
    <document>
      <template>
        <pageTemplate id="main">
          <frame id="first" x1="1cm" y1="1cm" width="19cm" height="277mm"/>
        </pageTemplate>
      </template>

      <stylesheet>
        <paraStyle name="parastyle" fontName="Helvetica" fontsize="8" spaceBefore="0" spaceAfter="0"/>
        <paraStyle name="text" fontName="Helvetica" fontsize="7" spaceBefore="0" spaceAfter="0"/>
        
        <paraStyle name="titol"
        	fontName="Helvetica-Bold"
        	fontSize="14"
        	leading="28"
        />
        
        
        <blockTableStyle id="trams">
        	<blockBackground colorName="grey" start="0,0" stop="-1,0" />
        	<blockFont name="Helvetica" size="8" />
        	<blockFont name="Helvetica-Bold" size="10" start="0,0" stop="-1,0"/>
        	<blockAlignment value="RIGHT" start="2,1" stop="2,-1" />
        	<blockAlignment value="RIGHT" start="3,1" stop="3,-1" />
        	<lineStyle kind="LINEBELOW" start="0,0" stop="-1,-1" colorName="black"/>
        </blockTableStyle>
      </stylesheet>

      <story>
      <blockTable style="trams" colWidths="1cm,6cm,6cm,1.5cm,1.5cm,2cm,1cm" repeatRows="1">
    <tr>
      <td t="1">Codi</td>
      <td t="1">Origen</td>
      <td t="1">Final</td>
      <td t="1">L. CAD</td>
      <td t="1">L. Op.</td>
      <td t="1">Conductor</td>
      <td t="1">Sec.</td>
    </tr>
    <xsl:apply-templates select="tram" mode="story" />
    </blockTable>
      </story>
    </document>
  </xsl:template>

  <xsl:template match="tram" mode="story">
    <tr>
      <td><para style="text"><xsl:value-of select="name" /></para></td>
      <td><para style="text"><xsl:value-of select="origen" /></para></td>
      <td><para style="text"><xsl:value-of select="final" /></para></td>
      <td><para style="text"><xsl:if test="longitud_cad!=''"><xsl:value-of select="format-number(longitud_cad, '0.00')" /></xsl:if></para></td>
      <td><para style="text"><xsl:if test="longitud_op!=''"><xsl:value-of select="format-number(longitud_op, '0.00')" /></xsl:if></para></td>
      <td><para style="text"><xsl:value-of select="conductor" /></para></td>
      <td><para style="text"><xsl:value-of select="seccio" /></para></td>
    </tr>
  </xsl:template>
</xsl:stylesheet>
