# -*- coding: utf-8 -*-
{
    "name": "Cartes canvis de comptadors a telegestió",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Centres Transformadors",
    "depends":[
        "c2c_webkit_report",
        "giscedata_cups"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "report_canvi_comptadors_tele_report.xml"
    ],
    "active": False,
    "installable": True
}
