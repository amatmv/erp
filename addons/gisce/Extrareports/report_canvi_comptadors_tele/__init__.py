# -*- coding: utf-8 -*-
{
  "name": "Report canvi de comptadors telegestió",
  "description": """""",
  "version": "0-dev",
  "author": "GISCE",
  "category": "Extrareports",
  "depends": ['giscedata_polissa', 'report_aeroo'],
  "init_xml": [],
  "demo_xml": [],
  "update_xml": ['report_canvi_comptadors_tele_report.xml'],
  "active": False,
  "installable": True
}
