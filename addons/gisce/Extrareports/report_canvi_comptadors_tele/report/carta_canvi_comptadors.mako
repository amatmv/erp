<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<head>
 <style type="text/css">
    ${css}
    body {
        font-family: helvetica;
        font-size: 14px;
    }
    .company_name {
        float: left;
        text-align: left;
        color: #FF6600;
        text-transform: uppercase;
    }
    .company_address {
        float: right;
        text-align: right;
        color: #FF6600;
    }
    .titular_address {
        margin-top: 30px;
        float: right;
        text-align: right;
    }
    p {
        padding: 4px;
        text-align: justify;
    }
    </style>
</head>
<body>
%for cups in objects:
<%
    if not cups.polissa_polissa or cups.polissa_polissa.potencia > 15:
        continue
%>
<div class="company_name">
<span style="font-size: 20px; font-weight: bold;">${cups.distribuidora_id\
.name}</span><br>
distribución de energía eléctrica
</div>
<div class="company_address">
    ${cups.distribuidora_id.address[0].street} <br />
    ${cups.distribuidora_id.address[0].zip} ${cups.distribuidora_id.address[0].city}
        (${cups.distribuidora_id.address[0].state_id.name}) <br />
    Tfno.: ${cups.distribuidora_id.address[0].phone} - Fax: ${cups.distribuidora_id.address[0].fax} <br />
    E-mail: ${cups.distribuidora_id.address[0].email}
</div>
<div class="titular_address" style="clear: both;">
    <strong style="text-decoration: underline;">${cups.polissa_polissa.titular.name}</strong> <br />
    ${cups.nv}, ${cups.pnp} <br/>
    ${cups.dp} - ${cups.id_poblacio.name} <br/>
    (${cups.id_provincia.name}) <br/>
</div>

<p style="clear: both;">
    <strong>CUPS: ${cups.name}</strong> <br/>
    Fecha: ${time.strftime('%d/%m/%Y')}
</p>
<p>
    <strong style="text-decoration: underline;">ASUNTO:</strong> Instalación del nuevo contador con discriminación horaria y capacidad de telegestión.
</p>
<p>
    Estimado Sr./a.
</p>
<p>
    Nos ponemos en contacto con Ud. para informarle que ${cups.distribuidora_id.name}, como encargado de la lectura,
está realizando la sustitución de los equipos de medida (contadores), de acuerdo con lo regulado por la Orden IET/290/2012.
</p>
<p>
    La sustitución del equipo de medida correspondiente a su contrato de
    acceso a la red ref.${cups.polissa_polissa.name}, sito en ${cups.nv},
%if cups.pnp:
${cups.pnp},
%endif
        población de ${cups.id_poblacio.name} (${cups.id_provincia\
.name}), está previsto realizarlo durante las semanas próximas.
</p>
<p>
    En la fecha indicada, personal acreditado de ${cups.distribuidora_id.name} contactará con usted para proceder a la
retirada y colocación del equipo de medida,
</p>
<p>
    ${cups.distribuidora_id.name} seguirá poniendo el contador a su disposición como hasta ahora. El desmontaje del

equipo de medida y la instalación del nuevo no supondrá coste alguno para Ud. La sustitución del contador se

realizará de forma rápida y sencilla, tan solo le ocasionará una pequeña interrupción del suministro durante un

periodo breve de unos pocos minutos.
</p>
<p>
    Indicarles que el contador lleva incorporada la función ICP (Interruptor de Control de Potencia). Esto significa

que el equipo cortará el suministro si Vd. demanda mas potencia de la contratada. Si tal evento ocurre, el equipo

reconecta cuando Vd. desconecte su instalación. Siga los siguientes pasos, baje el interruptor general de su

vivienda espere cinco segundos y vuelva a subir el interruptor general, en ese momento debería de volver a tener

luz, si esto se repite es porque esta consumiendo mas potencia de la que tiene contratada.
</p>
<p>
    Al dorso encontrará una breve explicación de las cuestiones que, sobre este asunto, creemos que puedan ser de su

interés.
</p>
<p>
    Le recordamos que nos tiene a su disposición en nuestra Oficina de
    Servicio al Consumidor sita en ${cups.distribuidora_id.address[0].street}
    de ${cups.distribuidora_id.address[0].city} (${cups.distribuidora_id\
.address[0].state_id.name}), o bien a través de la información de contacto (teléfono, fax, e-mail) que se indica en el

encabezado.
</p>
<p>
    Aprovechamos la ocasión para saludarle atentamente,
</p>
<p>
    LA DIRECCIÓN <br/>
    ${cups.distribuidora_id.name}
</p>
<p style="page-break-after:always"></p>
%endfor
</body>
</html>