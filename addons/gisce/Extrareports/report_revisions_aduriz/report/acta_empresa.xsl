<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="revisio-list"/>
  </xsl:template>

  <xsl:template match="revisio-list" >
    <document>
      <template pageSize="(19cm,297mm)" leftMargin="3cm" rightMargin="3cm" showBoundary="0">
        <pageTemplate id="main">

          <frame id="body" x1="1cm" y1="5mm" width="175mm" height="287mm" />

          <pageGraphics>
            <translate dx="-168mm" dy="170mm" />
            <rotate degrees="-90" />
            <setFont name="Helvetica" size="7" />
            <drawString x="0mm"
            y="170mm">doc. 13.3-40_05</drawString>
          </pageGraphics>

        </pageTemplate>
      </template>

      <stylesheet>

        <paraStyle name="text"
					fontName="Helvetica"
					fontSize="9"
          alignment="justify"
				/>

        <blockTableStyle id="taula_logos">
          <blockAlignment value="LEFT" start="0,0" stop="0,0" />
          <blockAlignment value="RIGHT" start="1,0" stop="1,0" />
        </blockTableStyle>

        <blockTableStyle id="taula_titol">
          <blockAlignment value="CENTER" />
          <blockTopPadding length="0" />
          <blockLeftPadding length="0" />
          <blockRightPadding length="0" />
          <blockBottomPadding length="0" />
          <blockBackground colorName="silver" />
          <lineStyle kind="BOX" colorName="silver" />
          <blockFont name="Helvetica-Bold" size="10" />
          <blockLeading length="4" start="0,0" stop="0,0" />
          <blockLeading length="4" start="0,2" stop="0,2" />
        </blockTableStyle>

        <blockTableStyle id="taula_contingut">
          <lineStyle kind="LINEBELOW" colorName="silver" start="0,0" stop="1,0" />
          <blockFont name="Helvetica" size="9" />
          <blockLeading length="7" />

          <blockLeading length="0.000001" start="0,0" stop="1,0" />

          <blockLeading length="20" start="0,15" stop="1,15" />
          <lineStyle kind="LINEBELOW" colorName="silver" start="0,16" stop="1,16" />
          <blockFont name="Helvetica-Bold" start="0,16" stop="1,16" />
          <blockLeading length="10" start="0,16" stop="1,16" />

          <blockLeading length="20" start="0,29" stop="1,29" />
          <lineStyle kind="LINEBELOW" colorName="silver" start="0,30" stop="1,30" />
          <blockFont name="Helvetica-Bold" start="0,30" stop="1,30" />
          <blockLeading length="10" start="0,30" stop="1,30" />
        </blockTableStyle>

        <blockTableStyle id="taula_trafos">
          <blockFont name="Helvetica" size="9" />
        </blockTableStyle>

        <blockTableStyle id="taula_trafos_pot">
          <blockFont name="Helvetica" size="9" />
          <blockFont name="Helvetica-Bold" start="1,0" stop="1,0" />
        </blockTableStyle>

        <blockTableStyle id="taula_firma">
          <blockFont name="Helvetica" size="9" />
          <blockAlignment value="RIGHT" start="1,0" stop="1,0" />
        </blockTableStyle>

        <blockTableStyle id="cap_taula_defectes">
          <blockFont name="Helvetica-Bold" size="10" />
          <lineStyle kind="BOX" colorName="silver" />
          <blockAlignment value="CENTER" />
        </blockTableStyle>

        <blockTableStyle id="taula_defectes">
          <blockFont name="Helvetica" size="8" />
          <blockFont name="Helvetica-Bold" start="0,0" stop="4,0" />
          <lineStyle kind="BOX" colorName="silver" />
          <blockAlignment value="CENTER" />
        </blockTableStyle>

        <blockTableStyle id="taula_tecnic">
          <blockFont name="Helvetica" size="10" />
          <lineStyle kind="BOX" colorName="silver" />
          <blockFont name="Helvetica-Bold" start="0,1" stop="3,1" />
        </blockTableStyle>

      </stylesheet>

      <story>
        <xsl:apply-templates select="revisio" mode="story" />
      </story>
    </document>
  </xsl:template>

  <xsl:template match="revisio" mode="story">
    <blockTable colWidths="100mm, 75mm" style="taula_logos">
      <tr>
        <td><image file="addons/giscedata_revisions/report/logoelectra.jpg"
        width="13.5mm" height="17mm" /></td>
        <td> </td>
      </tr>
    </blockTable>
    <spacer length="10" />
    <blockTable colWidths="175mm" style="taula_titol">
      <tr><td></td></tr>
      <tr>
        <td>ACTA DE RECONOCIMIENTO PERI�DICO</td>
      </tr>
      <tr>
        <td>INSTALACI�N EL�CTRICA DE SERVICIO P�BLICO</td>
      </tr>
      <tr><td></td></tr>
    </blockTable>
    <spacer length="6" />
    <para alignment="center" fontName="Helvetica-Bold">Reconocimiento de L�neas A�reas de Baja Tensi�n</para>
    <blockTable colWidths="70mm,105mm" style="taula_contingut">
      <tr><td></td><td></td></tr>
      <tr>
        <td>C�digo de documento</td>
        <td>A-BT-<xsl:value-of select="concat(ct/codi, ' ')" /><xsl:value-of select="trimestre" /></td>
      </tr>
      <tr>
        <td>C�digo de revisi�n</td>
        <td>BT-<xsl:value-of select="concat(ct/codi, ' ')" /><xsl:value-of select="trimestre" /></td>
      </tr>
      <tr>
        <td>Fecha de revisi�n</td>
        <td><xsl:value-of select="concat(substring(data, 9, 2), '/', substring(data, 6, 2), '/', substring(data, 1, 4))"/></td>
      </tr>
      <tr>
        <td>Unidad de reconocimiento</td>
        <td>Lineas a�reas BT correspondientes al CT <xsl:value-of select="ct/codi" /></td>
      </tr>
      <tr>
        <td>Empresa titular</td>
        <td><xsl:value-of select="ct/propietari/nom" /></td>
      </tr>
      <tr>
        <td>Direcci�n para notificaciones</td>
        <td><xsl:value-of select="concat(ct/propietari/partner/adreca/carrer, ' - ', ct/propietari/partner/adreca/cp, ' - ', ct/propietari/partner/adreca/ciutat)" /></td>
      </tr>
      <tr>
        <td>Organismo de inspecci�n</td>
        <td><xsl:value-of select="tecnic/organisme" /></td>
      </tr>
      <tr>
        <td>Direcci�n Organismo de inspecci�n</td>
        <td><xsl:value-of select="tecnic/adreca" /></td>
      </tr>
      <tr>
        <td>Nombre y apellidos del titular</td>
        <td><xsl:value-of select="tecnic/nom" /></td>
      </tr>
      <tr>
        <td>DNI</td>
        <td><xsl:value-of select="tecnic/dni" /></td>
      </tr>
      <tr>
        <td>Titulaci�n</td>
        <td><xsl:value-of select="tecnic/titolacio" /></td>
      </tr>
      <tr>
        <td>N�mero de colegiado</td>
        <td><xsl:value-of select="tecnic/ncol" /></td>
      </tr>
      <tr>
        <td>Direcci�n para notificaciones</td>
        <td><xsl:value-of select="tecnic/adreca" /></td>
      </tr>
      <tr>
        <td>Tel�fono</td>
        <td><xsl:value-of select="tecnic/telefon" /></td>
      </tr>
      <tr>
        <td>Correo electr�nico</td>
        <td><xsl:value-of select="tecnic/email" /></td>
      </tr>
      <tr>
        <td>Datos de la instalaci�n</td>
        <td></td>
      </tr>
      <tr>
        <td>Instalaci�n</td>
        <td>Lineas a�reas BT del Centro Transformador <xsl:value-of select="ct/codi" /></td>
      </tr>
      <tr>
        <td>Emplazamiento</td>
        <td><xsl:value-of select="ct/descripcio" /></td>
      </tr>
      <tr>
      <tr>
        <td>Poblaci�n</td>
        <td><xsl:value-of select="ct/poblacio" /></td>
      </tr>
        <td>Municipio</td>
        <td><xsl:value-of select="ct/municipi" /></td>
      </tr>
      <tr>
        <td>Tensi�n de primario</td>
        <!-- buscar de totes les connexions quina �s la que est� connectada
             i mostrar la tensi� -->
        <td><xsl:value-of select="ct/tensio_p" /></td>
      </tr>
      <tr>
        <td>Tensi�n de secundario</td>
        <td><xsl:value-of select="ct/tensio_s" /></td>
      </tr>
      <tr>
        <td>Salidas de baja tensi�n</td>
        <td><xsl:value-of select="ct/sortides_baixa" /></td>
      </tr>
      <tr>
      	<td>Longitud de l�neas a�reas</td>
      	<td><xsl:value-of select="format-number(ct/longitud_bt_aeri, '###.##')" /> m</td>
      </tr>
      <tr>
      	<td>Longitud de l�neas subterr�neas</td>
      	<td><xsl:value-of select="format-number(ct/longitud_bt_subt, '###.##')" /> m</td>
      </tr>
      <tr>
      	<td>N�mero de apoyos</td>
      	<td><xsl:value-of select="ct/n_suports_metallics" /> apoyos met�licos</td>
      </tr>
      <tr>
      	<td>
      	</td>
      	<td><xsl:value-of select="ct/n_suports_formigo" /> apoyos de hormig�n</td>
      </tr>
      <tr>
      	<td>
      	</td>
      	<td><xsl:value-of select="ct/n_suports_fusta" /> apoyos de madera</td>
      </tr>
      <tr>
      	<td>
      	</td>
      	<td><xsl:value-of select="ct/n_cadiretes_travessers" /> palomillas en fachada</td>
      </tr>
      <tr>
      	<td>Secciones Conductores</td>
      	<td><para style="text" alignment="left"><xsl:value-of select="ct/seccions_bt_string" /></para></td>
      </tr>
      <tr>
        <td>Acta</td>
        <td></td>
      </tr>
    </blockTable>
    <para style="text"><xsl:value-of select="tecnic/nom" />, ingeniero competente que ha realizado la inspecci�n de la instalaci�n el�ctrica de referencia, de acuerdo con el art�culo 7 del Decreto 328/2001, de 4 de diciembre, por el que se establece el procedimiento aplicable para efectuar los reconocimientos peri�dicos de las instalaciones de producci�n, transformaci�n, transporte y distribuici�n de energia el�ctrica, publicado en el DOGC n�3.536, y seg�n la ITC-BT-06 del Reglamento Electrot�cnico de baja tensi�n RD 842/2002 del 2 de agosto, o las MIEBT 002, 003 y 004 del Reglamento Electrot�cnico de baja tensi�n RD 2413/1973 del 20 de septiembre por las lineas legalizadas antes del 18 de septiembre del 2003.
    </para>
    <spacer length="15" />
    <para style="text"><b>HACE CONSTAR:</b></para>
    <para style="text">
      Que la instalaci�n tiene defectos  y se adjunta a esta acta una relaci�n de defectos deacuerdo con la legislaci�n vigente seg�n el Decreto 328/2001 y la ITC-BT-06 del RD 842/2002.
    </para>
    <spacer length="10" />
    <para style="text">
      Observaciones y explicaci�n de acciones no realizadas: <xsl:if test="observacions != '' and observacions != '0'"><xsl:value-of select="observacions" /></xsl:if>
    </para>
    <spacer length="25" />
    <blockTable style="taula_firma" colWidths="70mm,105mm">
      <tr>
        <td>Firma</td><td>Girona, a <xsl:value-of select="concat(substring(data, 9, 2), '/', substring(data, 6, 2), '/', substring(data, 1, 4))"/></td>
      </tr>
    </blockTable>
    <para style="text">
      El presente infrome tiene una validez de 3 a�os a partir de la fecha de revisi�n.
    </para>
    <nextPage />
    <xsl:apply-templates select="defectes" mode="story" />
  </xsl:template>


  <xsl:template match="defectes" mode="story">
    <blockTable colWidths="100mm, 75mm" style="taula_logos">
     <tr>
        <td><image file="addons/giscedata_revisions/report/logoelectra.jpg"
        width="13.5mm" height="17mm" /></td>
        <td> </td>
      </tr>
    </blockTable>
    <spacer length="10" />
    <blockTable colWidths="175mm" style="taula_titol">
      <tr><td></td></tr>
      <tr>
        <td>ACTA DE RECONOCIMIENTO PERI�DICO</td>
      </tr>
      <tr>
        <td>INSTALACI�N EL�CTRICA DE SERVICIO P�BLICO</td>
      </tr>
      <tr><td></td></tr>
    </blockTable>
    <spacer length="6" />
    <para alignment="center" fontName="Helvetica-Bold">Reconocimiento de L�neas A�reas de baja tensi�n</para>
    <spacer length="10" />
    <blockTable style="cap_taula_defectes" colWidths="175mm">
      <tr><td>DEFECTOS EN LAS L�NEAS BT DEL CENTRO TRANSFORMADOR <xsl:value-of select="../ct/codi" /></td></tr>
    </blockTable>
    <blockTable style="taula_defectes" colWidths="1.5cm,2cm,1cm,9cm,1.5cm,2.5cm" repeatRows="0">
      <tr>
        <td>C�digo</td>
        <td>Linea</td>
        <td>Tipo</td>
        <td>Descripci�n</td>
        <td>Valoraci�n</td>
        <td>L�mite reparaci�n</td>
      </tr>
      <xsl:apply-templates select="defecte" mode="story" />
    </blockTable>
    <blockTable style="taula_tecnic" colWidths="5cm, 5cm, 3cm, 4.5cm">
      <tr>
        <td>T�cnico:</td>
        <td>Titulaci�n:</td>
        <td>Colegiado:</td>
        <td>Fecha de reconocimieto:</td>
      </tr>
      <tr>
        <td><xsl:value-of select="../tecnic/nom" /></td>
        <td><xsl:value-of select="../tecnic/titolacio" /></td>
        <td><xsl:value-of select="../tecnic/ncol" /></td>
        <td><xsl:value-of select="concat(substring(../data, 9, 2), '/', substring(../data, 6, 2), '/', substring(../data, 1, 4))" /></td>
      </tr>
    </blockTable>
    <setNextTemplate name="main" />
    <nextFrame/>
  </xsl:template>

  <xsl:template match="defecte" mode="story">
    <xsl:if test="intern!=1">
    <tr>
      <td><xsl:value-of select="codi" /></td>
      <td><xsl:value-of select="linia_bt" /></td>
      <td><xsl:value-of select="tipus" /></td>
      <xsl:choose>
        <xsl:when test="string-length(descripcio) * 0.21 &gt; 7.5">
          <td><xsl:value-of select="concat(substring(descripcio, 1, 65), '...')" /></td>
        </xsl:when>
        <xsl:otherwise>
          <td><xsl:value-of select="descripcio" /></td>
        </xsl:otherwise>
      </xsl:choose>
      <td><xsl:value-of select="valoracio" /></td>
      <td><xsl:value-of select="concat(substring(data_limit, 9, 2), '/', substring(data_limit, 6, 2), '/', substring(data_limit, 1, 4))" /></td>
    </tr>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td><para fontName="Helvetica" fontSize="8"><xsl:value-of select="observacions" /></para></td>
      <td></td>
      <td></td>
    </tr>
    </xsl:if>
  </xsl:template>

</xsl:stylesheet>