<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="revisio-list"/>
  </xsl:template>

  <xsl:template match="revisio-list">
    <document>
      <template pageSize="(19cm,297mm)" leftMargin="3cm" rightMargin="3cm" showBoundary="0">
        <pageTemplate id="main">
          <frame id="body" x1="1cm" y1="5mm" width="175mm" height="287mm" />

          <pageGraphics>
            <translate dx="-168mm" dy="170mm" />
            <rotate degrees="-90" />
            <setFont name="Helvetica" size="7" />
            <drawString x="0mm"
            y="170mm">doc. 13.3-08_07</drawString>
          </pageGraphics>

        </pageTemplate>
      </template>
      
      <stylesheet>

        <paraStyle name="text"
					fontName="Helvetica"
					fontSize="9"
          alignment="justify"
				/>

        <blockTableStyle id="taula_logos">
          <blockAlignment value="LEFT" start="0,0" stop="0,0" />
          <blockAlignment value="RIGHT" start="1,0" stop="1,0" />
        </blockTableStyle>

        <blockTableStyle id="taula_titol">
          <blockAlignment value="CENTER" />
          <blockTopPadding length="0" />
          <blockLeftPadding length="0" />
          <blockRightPadding length="0" />
          <blockBottomPadding length="0" />
          <blockBackground colorName="silver" />
          <lineStyle kind="BOX" colorName="silver" />
          <blockFont name="Helvetica-Bold" size="10" />
          <blockLeading length="4" start="0,0" stop="0,0" />
          <blockLeading length="4" start="0,2" stop="0,2" />
        </blockTableStyle>

        <blockTableStyle id="taula_contingut">
          <lineStyle kind="LINEBELOW" colorName="silver" start="0,0" stop="1,0" />
          <blockFont name="Helvetica" size="9" />
          <blockLeading length="7" />
          
          <blockLeading length="0.000001" start="0,0" stop="1,0" />
          <!--<blockLeading length="20" start="0,9" stop="1,9" />-->

          <blockLeading length="20" start="0,19" stop="1,19" />
          <lineStyle kind="LINEBELOW" colorName="silver" start="0,20" stop="1,20" />
          <blockFont name="Helvetica-Bold" start="0,20" stop="1,20" />
          <blockLeading length="10" start="0,20" stop="1,20" />

          <blockLeading length="20" start="0,23" stop="1,23" />
          <lineStyle kind="LINEBELOW" colorName="silver" start="0,24" stop="1,24" />
          <blockFont name="Helvetica-Bold" start="0,24" stop="1,24" />
          <blockLeading length="10" start="0,24" stop="1,24" />
          
          <blockLeading length="20" start="0,31" stop="1,31" />
          <lineStyle kind="LINEBELOW" colorName="silver" start="0,32" stop="1,32" />
          <blockFont name="Helvetica-Bold" start="0,32" stop="1,32" />
          <blockLeading length="10" start="0,32" stop="1,32" />
        </blockTableStyle>

        <blockTableStyle id="taula_firma">
          <blockFont name="Helvetica" size="9" />
          <blockAlignment value="RIGHT" start="1,0" stop="1,0" />
        </blockTableStyle>

      </stylesheet>
    
      <story>
        <xsl:apply-templates select="revisio" mode="story" />
      </story>
    </document>
  </xsl:template>

  <xsl:template match="revisio" mode="story">
    <blockTable colWidths="100mm, 75mm" style="taula_logos">
      <tr>
        <td><image file="addons/giscedata_revisions/report/logoelectra.jpg"
        width="13.5mm" height="16.23mm" /></td>
        <td><image file="addons/giscedata_revisions/report/enac.tif"
        width="22mm" height="17mm" /></td>
      </tr>
    </blockTable>
    <spacer length="10" />
    <blockTable colWidths="175mm" style="taula_titol">
      <tr><td></td></tr>
      <tr>
        <td>CERTIFICADO DE RECONOCIMIENTO PERI�DICO</td>
      </tr>
      <tr>
        <td>INSTALACI�N EL�CTRICA DE SERVICIO P�BLICO</td>
      </tr>
      <tr><td></td></tr>
    </blockTable>
    <spacer length="6" />
    <para alignment="center" fontName="Helvetica-Bold">L�neas de Alta Tensi�n</para>
    <blockTable colWidths="70mm,105mm" style="taula_contingut">
      <tr><td></td><td></td></tr>
      <tr>
        <td>C�digo de documento</td>
        <td>C-AT-<xsl:value-of select="concat(linia/codi, ' ')" /><xsl:value-of select="trimestre" /></td>
      </tr>
      <tr>
        <td>C�digo de revisi�n</td>
        <td><xsl:value-of select="concat(linia/codi, ' ')" /><xsl:value-of select="trimestre" /></td>
      </tr>
      <tr>
        <td>Expediente autoritizaci�n administrativa</td>
        <td><xsl:value-of select="linia/expedients" /></td>
      </tr>
      <tr>
        <td>Fecha de revisi�n</td>
        <td><xsl:value-of select="concat(substring(data, 9, 2), '/', substring(data, 6, 2), '/', substring(data, 1, 4))" /></td>
      </tr>
      <tr>
        <td>Fecha comprovaci�n reparaci�n de defectos</td>
        <td><xsl:if test="data_comprovacio!=''"><xsl:value-of select="concat(substring(data_comprovacio, 9, 2), '/', substring(data_comprovacio, 6, 2), '/', substring(data_comprovacio, 1, 4))" /></xsl:if></td>
      </tr>
      <tr>
      	<td>Tipo de comprovaci�n</td>
      	<td><xsl:if test="data_ca2!='' and data_ca2 != '0'">2a visita</xsl:if></td>
      </tr>
      <tr>
        <td>Unidad de reconocimiento</td>
        <td>LAT-<xsl:value-of select="linia/codi" /></td>
      </tr>
      <tr>
        <td>Tipo de instalaci�n</td>
        <td>L�nea a�rea de alta tensi�n</td>
      </tr>
      <tr>
        <td>Empresa titular</td>
        <td><xsl:value-of select="linia/propietari/nom" /></td>
      </tr>
      <tr>
        <td>Direcci�n para notificaciones</td>
        <td><xsl:value-of select="concat(linia/propietari/adreca/carrer, ' ', linia/propietari/adreca/cp, ' - ', linia/propietari/adreca/ciutat)" /></td>
      </tr>
      <tr>
        <td>Organismo de inspecci�n</td>
        <td><xsl:value-of select="tecnic/organisme" /></td>
      </tr>
      <tr>
        <td>Direcci�n Organismo de inspecci�n</td>
        <td><xsl:value-of select="tecnic/adreca" /></td>
      </tr>
      <tr>
        <td>Nombre y apellidos del titular</td>
        <td><xsl:value-of select="tecnic/nom" /></td>
      </tr>
      <tr>
        <td>DNI</td>
        <td><xsl:value-of select="tecnic/dni" /></td>
      </tr>
      <tr>
        <td>Titulaci�n</td>
        <td><xsl:value-of select="tecnic/titolacio" /></td>
      </tr>
      <tr>
        <td>N�mero de colegiado</td>
        <td><xsl:value-of select="tecnic/ncol" /></td>
      </tr>
      <tr>
        <td>Direcci�n para notificaciones</td>
        <td><xsl:value-of select="tecnic/adreca" /></td>
      </tr>
      <tr>
        <td>Tel�fono</td>
        <td><xsl:value-of select="tecnic/telefon" /></td>
      </tr>
      <tr>
        <td>Correo electr�nico</td>
        <td><xsl:value-of select="tecnic/email" /></td>
      </tr>
      <tr>
        <td>Datos de la instalaci�n</td>
        <td></td>
      </tr>
      <tr>
        <td>Instalaci�n</td>
        <td>L�nea AT <xsl:value-of select="linia/codi" /></td>
      </tr>
      <tr>
        <td>Emplazamiento</td>
        <td><xsl:value-of select="linia/descripcio" /></td>
      </tr>
      <tr>
        <td>Municipio</td>
        <td><xsl:value-of select="linia/municipi" /></td>
      </tr>
      <tr>
        <td>Caracter�sticas t�cnicas principales</td>
        <td></td>
      </tr>
      <tr>
        <td>Origen de la l�nea</td>
        <td><xsl:value-of select="linia/origen" /></td>
      </tr>
      <tr>
        <td>Final de la l�nea</td>
        <td><xsl:value-of select="linia/final" /></td>
      </tr>
      <tr>
        <td>Tensi�n de servicio</td>
        <td><xsl:value-of select="concat(linia/tensio, ' V')" /></td>
      </tr>
      <tr>
        <td>Secci�n conductores</td>
        <td><xsl:value-of select="linia/seccio_conductors" /></td>
      </tr>
      <tr>
        <td>Longitud a�rea</td>
        <td>
          <xsl:choose>
            <xsl:when test="linia/aeri_op&gt;0">
              <xsl:value-of select="concat(format-number(linia/aeri_op, '###,###.##'), ' m')" />
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="concat(format-number(linia/aeri_cad, '###,###.##'), ' m')" />
            </xsl:otherwise>
          </xsl:choose>
        </td>
      </tr>
      <tr>
        <td>Longitud Subterr�nea</td>
        <td>
          <xsl:choose>
            <xsl:when test="linia/subt_op&gt;0.0">
              <xsl:value-of select="concat(format-number(linia/subt_op, '###,###.##'), ' m')" />
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="concat(format-number(linia/subt_cad, '###,###.##'), ' m')" />
            </xsl:otherwise>
          </xsl:choose>
        </td>
      </tr>
      <tr>
        <td>Apoyos</td>
        <td>Met�licos: <xsl:value-of select="linia/n_sup_metal"/>; Hormig�n: <xsl:value-of select="linia/n_sup_for" />; Madera: <xsl:value-of select="linia/n_sup_fus" /></td>
      </tr>
      <tr>
        <td>Certificaci�n</td>
        <td></td>
      </tr>
    </blockTable>
    <para style="text"><xsl:value-of select="tecnic/nom" />, ingeniero competente que ha realizado la inspecci�n de la instalaci�n el�ctrica de referencia, de acuerdo con el art�culo 7 del Decreto 328/2001, de 4 de diciembre, por el que se establece el procedimiento aplicable para efectuar los reconocimientos peri�dicos de las instalaciones de producci�n, tranformaci�n, transporte y distribuci�n de energia el�ctrica, publicado en el DOGC n�3.536,
    </para>
    <spacer length="15" />
    <para style="text"><b>CERTIFICA:</b></para>
    <para style="text">
      Que la instalaci�n es correcta y no tiene defectos que precisen correcci�n inmediata ni defectos a corregir dentro de un plazo.
    </para>
    <spacer length="10" />
    <para style="text">
      Observaciones y explicaci�n de acciones no realizadas: <xsl:if test="observacions != '' and observacions != '0'"><xsl:value-of select="observacions" /></xsl:if>
    </para>
    <spacer length="40" />
    <blockTable style="taula_firma" colWidths="70mm,105mm">
      <tr>
        <td>Firma</td><td>Girona, a <xsl:choose><xsl:when test="data_ca2 != '' and data_ca2 != '0'"><xsl:value-of select="concat(substring(data_comprovacio, 9, 2), '/', substring(data_comprovacio, 6, 2), '/', substring(data_comprovacio, 1, 4))" /></xsl:when><xsl:otherwise><xsl:value-of select="concat(substring(data, 9, 2), '/', substring(data, 6, 2), '/', substring(data, 1, 4))" /></xsl:otherwise></xsl:choose></td>
      </tr>
    </blockTable>
    <para style="text">
      El presente infrome tiene una validez de 3 a�os a partir de la fecha de revisi�n.
    </para>
  </xsl:template>

</xsl:stylesheet>
