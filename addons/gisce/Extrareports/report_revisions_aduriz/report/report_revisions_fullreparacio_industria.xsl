<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="revisio-list" />
  </xsl:template>

  <xsl:template match="revisio-list">
    <document>
      <template pageSize="(297mm,19cm)" topMargin="0.5cm" bottomMargin="0.5cm" rightMargin="1cm">
        <pageTemplate id="main">
          <pageGraphics>
            <place x="1cm" y="15.5cm" width="277cm" height="2.5cm">
            <para fontName="Helvetica" fontSize="10" t="1"><tr t="1">RECONEIXEMENTS PERI�DICS DE LES INSTAL�LACIONS DE PRODUCCI�, TRANSFORMACI�, TRANSPORT I DISTRIBUCI� D'ENERGIA EL�CTRICA</tr></para>
      <blockTable style="taula1" colWidths="8.92cm,5.92cm,8.92cm,3.92cm">
        <tr t="1">
          <td t="1">LLISTAT DE DEFECTES EN L�NIES D'ALTA TENSI�</td>
          <td t="1">FULL DE REPARACI�</td>
          <td><xsl:value-of select="//corporate-header/corporation/name" /></td>
          <td t="1"><image file="addons/giscedata_revisions/report/logoelectra.jpg"
          width="6.2mm" height="8.4mm" /></td>
        </tr>
      </blockTable>

      <blockTable style="taula2" colWidths="27.7cm">
        <tr t="1">
          <td t="1">S'han d'omplir els apartats 'Data de reparaci�', 'Observacions' i 'Operari/s'</td>
        </tr>
      </blockTable>
      <blockTable style="taula3" colWidths="1.1cm,1.8cm,1.8cm,3cm,2cm,11.7cm,1.8cm,1mm,1.8cm,5.5mm,1.5cm">
        <tr t="1">
          <td t="1">Codi</td>
          <td t="1">Revisi�</td>
          <td t="1">Data</td>
          <td t="1">L�nia</td>
          <td t="1">Suport</td>
          <td t="1">Descripci�</td>
          <td t="1">Data l�mit</td>
          <td></td>
          <td t="1">Data reparaci�</td>
          <td></td>
          <td t="1">Operari/s</td>
        </tr>
      </blockTable>
            </place>
          </pageGraphics>
          <frame id="first" x1="1cm" y1="0.5cm" width="277mm" height="15.3cm"/>
        </pageTemplate>
      </template>
      
      <stylesheet>

        <paraStyle name="titol"
					fontName="Helvetica"
					fontSize="10"
          leading="20"
          alignment="center"
				/>

        <paraStyle name="empresa"
					fontName="Helvetica"
					fontSize="8"
          leading="16"
          alignment="right"
				/>

        <blockTableStyle id="taula1">
          <blockFont name="Helvetica" size="8" />
          <blockValign value="MIDDLE" />
          <blockFont name="Helvetica-Bold" size="12" start="1,0" stop="1,0" />
          <lineStyle kind="BOX" colorName="black" start="1,0" stop="1,0" />
          <blockAlignment value="CENTER" start="1,0" stop="2,0" />
          <blockTopPadding leading="0" />
        </blockTableStyle>

        <blockTableStyle id="taula2">
          <blockFont name="Helvetica" size="8" />
          <blockAlignment value="CENTER" />
          <blockLeading length="10" />
          <lineStyle kind="LINEBELOW" colorName="silver" />
        </blockTableStyle>

        <blockTableStyle id="taula3">
          <blockFont name="Helvetica" size="8" />
          <blockLeading length="10" />
          <lineStyle kind="LINEBELOW" colorName="silver" start="0,0" stop="12,0" />
        </blockTableStyle>

        <blockTableStyle id="taula4">
          <blockFont name="Helvetica" size="8" />
          <blockFont name="Helvetica-Bold" start="0,0" stop="0,0" />
          <blockFont name="Helvetica-Bold" start="4,0" stop="4,0" />
          <blockFont name="Helvetica-Bold" start="3,0" stop="3,0" />
          <lineStyle kind="BOX" colorName="silver" start="8,0" stop="8,0" />          
          <lineStyle kind="BOX" colorName="silver" start="10,0" stop="10,0" />
          <lineStyle kind="LINEBEFORE" colorName="silver" start="12,0" stop="12,0" />
          <lineStyle kind="LINEABOVE" colorName="silver" start="12,0" stop="12,0" />
          <lineStyle kind="LINEAFTER" colorName="silver" start="12,0" stop="12,0" />
          <blockTopPadding leading="0" />
          <blockLeftPadding leading="0" />
          <blockRightPadding leading="0" />
          <blockBottomPadding leading="0" />
          <blockValign value="MIDDLE"/>
          <blockAlignment value="CENTER" />
        </blockTableStyle>
        
        <blockTableStyle id="taula5">
          <blockFont name="Helvetica" size="8" />

          <blockLeading length="0.1" start="0,0" stop="3,0" />
          <blockTopPadding length="0" start="0,0" stop="3,0" />

          <lineStyle kind="LINEBEFORE" colorName="silver" start="3,0" stop="3,0" />
          <lineStyle kind="LINEAFTER" colorName="silver" start="3,0" stop="3,0" />

          <lineStyle kind="LINEBEFORE" colorName="silver" start="1,1" stop="1,2" />
          <lineStyle kind="LINEBELOW" colorName="silver" start="1,2" stop="1,2" />
          <lineStyle kind="LINEAFTER" colorName="silver" start="1,1" stop="1,2" />
          <lineStyle kind="LINEABOVE" colorName="silver" start="1,1" stop="1,1" />
          
          <lineStyle kind="LINEBEFORE" colorName="silver" start="3,1" stop="3,1" />
          <lineStyle kind="LINEAFTER" colorName="silver" start="3,1" stop="3,1" />
          <lineStyle kind="LINEBEFORE" colorName="silver" start="3,2" stop="3,2" />
          <lineStyle kind="LINEAFTER" colorName="silver" start="3,2" stop="3,2" />
          <lineStyle kind="LINEBEFORE" colorName="silver" start="3,3" stop="3,3" />
          <lineStyle kind="LINEAFTER" colorName="silver" start="3,3" stop="3,3" />
          
          <lineStyle kind="LINEBEFORE" colorName="silver" start="1,4" stop="1,5" />
          <lineStyle kind="LINEBELOW" colorName="silver" start="1,5" stop="1,5" />
          <lineStyle kind="LINEAFTER" colorName="silver" start="1,4" stop="1,5" />
          <lineStyle kind="LINEABOVE" colorName="silver" start="1,4" stop="1,4" />
          <blockFont name="Helvetica-Bold" start="1,5" stop="1,5" />
          <blockAlignment value="RIGHT" start="1,5" stop="1,5" />
          
          <lineStyle kind="LINEBEFORE" colorName="silver" start="3,4" stop="3,4" />
          <lineStyle kind="LINEAFTER" colorName="silver" start="3,4" stop="3,4" />
          <lineStyle kind="LINEBEFORE" colorName="silver" start="3,5" stop="3,5" />
          <lineStyle kind="LINEAFTER" colorName="silver" start="3,5" stop="3,5" />
          <lineStyle kind="LINEBELOW" colorName="silver" start="3,5" stop="3,5" />

          <blockLeading length="0.1" start="0,3" stop="3,3" />
          <blockTopPadding length="0" start="0,3" stop="3,3" />
        </blockTableStyle>

      </stylesheet>
    
      <story>
      <xsl:apply-templates select="revisio" mode="story">
        <xsl:sort select="defectes/defecte/name" data-type="number" order="ascending"/>
      </xsl:apply-templates>
      </story>
    </document>

    </xsl:template>

    <xsl:template match="revisio" mode="story">
      <xsl:if test="count(defectes/defecte[intern!=1 and reparat=0]) &gt; 0 and estat != 'sense assignar'">
        <xsl:apply-templates select="defectes/defecte[intern!=1 and reparat=0]" mode="story" />
        <pageBreak />
      </xsl:if>

  </xsl:template>

  <xsl:template match="defectes/defecte" mode="story">
  	<xsl:if test="intern!=1 and reparat=0">
      <spacer length="1mm" />
      <blockTable style="taula4" colWidths="0.6cm,1cm,1.8cm,3.3cm,2cm,12.7cm,1.8cm,1mm,1.8cm,1mm,2.5cm">
        <tr>
          <td><xsl:value-of select="name"/></td>
          <td><xsl:value-of select="../../trimestre"/></td>
          <td><xsl:value-of select="concat(substring(../../data, 9, 2), '/', substring(../../data, 6, 2), '/', substring(../../data, 1, 4))"/></td>
          <td><para fontName="Helvetica-Bold" fontSize="8" leading="8"><xsl:value-of select="../../linia/codi"/> - <xsl:value-of select="../../linia/descripcio"/></para></td>
          <td><xsl:value-of select="suport" /></td>
          <td><para fontSize="8" fontName="Helvetica" leading="8"><xsl:value-of select="codi"/> - <xsl:value-of select="descripcio" /></para></td>
          <td><xsl:value-of select="concat(substring(data_limit, 9, 2), '/', substring(data_limit, 6, 2), '/', substring(data_limit, 1, 4))" /></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>          
        </tr>
      </blockTable>
      <blockTable style="taula5" colWidths="1.6cm,23.5cm,1mm,2.5cm">
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td>OBSERVACIONS RECONEIXEMENT:</td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td><para fontSize="8" fontName="Helvetica"><xsl:value-of select="observacions" /></para></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>    
        <tr>
          <td></td>
          <td>OBSERVACIONS REPARACI�:</td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td><xsl:value-of select="../../linia/municipi" /></td>
          <td></td>
          <td></td>
        </tr>
      </blockTable>
      <xsl:if test="position() mod 4 = 0">
      	<setNextTemplate name="main" />
      </xsl:if>
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>
