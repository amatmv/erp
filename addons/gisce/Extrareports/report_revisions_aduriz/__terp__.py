# -*- coding: utf-8 -*-
{
    "name": "Report GISCE DATA Revisions Aduriz",
    "description": """Revisions""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Revisions",
    "depends":[
        "giscedata_revisions",
        "giscedata_revisions_bt"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "report_revisions_aduriz.xml"
    ],
    "active": False,
    "installable": True
}
