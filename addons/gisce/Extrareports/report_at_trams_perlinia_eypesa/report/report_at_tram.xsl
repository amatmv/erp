<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="linies"/>
  </xsl:template>

  <xsl:template match="linies">
    <document>
      <template>
        <pageTemplate id="main">
          <frame id="first" x1="1cm" y1="1cm" width="19cm" height="277mm"/>
        </pageTemplate>
      </template>

      <stylesheet>
        <paraStyle name="parastyle" fontName="Helvetica" fontsize="10" spaceBefore="0" spaceAfter="0"/>
        
        <paraStyle name="titol"
        	fontName="Helvetica-Bold"
        	fontSize="14"
        	leading="28"
        />
        
        
        <blockTableStyle id="trams">
        	<blockBackground colorName="grey" start="0,0" stop="-1,0" />
        	<blockFont name="Helvetica" size="10" />
        	<blockFont name="Helvetica-Bold" size="10" start="0,0" stop="-1,0"/>
        	<blockAlignment value="RIGHT" start="3,1" stop="3,-1" />
        </blockTableStyle>
      </stylesheet>

      <story>
        <xsl:apply-templates select="linia" mode="story"/>
      </story>
    </document>
  </xsl:template>

  <xsl:template match="linia" mode="story">
    <para style="titol">Trams L�nia <xsl:value-of select="name" /></para>
    <xsl:apply-templates select="trams" mode="story" />
  </xsl:template>

  <xsl:template match="trams" mode="story">
    <blockTable style="trams" colWidths="1cm,2.5cm,1.2cm,2cm,4.5cm,1.7cm,1.7cm,1cm,1.3cm,1.5cm" repeatRows="1">
    <tr>
      <td t="1">Codi</td>
      <td t="1">Ordre</td>
      <td t="1">Baixa</td>
      <td t="1">Long. CAD</td>
      <td t="1">Conductor</td>
      <td t="1">A�llam.</td>
      <td t="1">Secci�</td>
      <td t="1">Mat.</td>
      <td t="1">N� Cir.</td>
      <td t="1">Tipus</td>
    </tr>
    <xsl:apply-templates select="tram" mode="story" />
    <tr>
    	<td t="1">Totals:</td>
    	<td></td>
    	<td></td>
    	<td><xsl:value-of select="format-number(sum(tram/longitud_cad), '0')" /></td>
    	<td></td>
    	<td></td>
    </tr>
    </blockTable>
  </xsl:template>

  <xsl:template match="tram" mode="story">
    <tr>
      <td><xsl:value-of select="name" /></td>
      <td><xsl:value-of select="ordre" /></td>
      <td><xsl:value-of select="baixa" /></td>
      <td><xsl:value-of select="format-number(longitud_cad, '0')" /></td>
      <td><xsl:value-of select="conductor" /></td>
      <td><xsl:value-of select="aillament" /></td>
      <td><xsl:value-of select="seccio" /></td>
      <td><xsl:value-of select="material" /></td>
      <td><xsl:value-of select="circuits" /></td>
      <td><xsl:if test="tipus=1">A</xsl:if><xsl:if test="tipus=2">S</xsl:if></td>
    </tr>
  </xsl:template>
</xsl:stylesheet>
