# -*- coding: utf-8 -*-
{
    "name": "Report Trams AT versió EYPESA",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Extrareports",
    "depends":[
        "giscedata_at"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "report_at_trams_perlinia_eypesa_report.xml"
    ],
    "active": False,
    "installable": True
}
