# -*- coding: utf-8 -*-
{
    "name": "Report Descàrrecs Catral",
    "description": """Personalització del report de descàrrecs""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "",
    "depends":[
        "giscedata_descarrecs"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "report_descarrec_catral_report.xml"
    ],
    "active": False,
    "installable": True
}
