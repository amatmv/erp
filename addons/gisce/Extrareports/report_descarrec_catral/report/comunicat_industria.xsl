<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="descarrec_list" />
  </xsl:template>
  <xsl:template match="descarrec_list">
    <xsl:apply-templates select="descarrec" />
  </xsl:template>
  <xsl:template match="descarrec">
    <document>
      <template pageSize="(21cm, 29.7cm)">
        <pageTemplate id="main">
          <pageGraphics>
                <image width="3cm" height="3cm" x="15cm" y="25.8cm">
                    <xsl:value-of select="//corporate-header/corporation/logo" />
                </image>
				<image file="addons/gisce/Extrareports/report_descarrec_catral/report/logo1.png" width="80mm" height="80mm" x="1cm" y="26.6cm" />
                <setFont name="Helvetica-Bold" size="18" />
                <drawString x="1.2cm" y="24.8cm"><xsl:value-of select="//corporate-header/corporation/name" /></drawString>
                <setFont name="Helvetica" size="10" />
                <drawString x="1.2cm" y="24.1cm"><xsl:value-of select="//corporate-header/corporation/rml_footer1" /></drawString>
                <lineMode width="2" />
                <stroke color="blue"/>
                <lines>
                        1cm 24cm 20cm 24cm
                </lines>
                <setFont name="Helvetica" size="8" />
                <drawString x="1.2cm" y="23.6cm"><xsl:value-of select="//corporate-header/corporation/rml_header" /></drawString>
                <drawRightString x="20cm" y="23.7cm"><xsl:value-of select="//corporate-header/corporation/address[type='default']/street" /></drawRightString>
                <drawRightString x="20cm" y="23.4cm"><xsl:value-of select="concat(//corporate-header/corporation/address[type='default']/zip, ' ', //corporate-header/corporation/address[type='default']/city)" /> (<xsl:value-of select="//corporate-header/corporation/address[type='default']/state" />)</drawRightString>
                <drawRightString x="20cm" y="23.1cm">Tel. <xsl:value-of select="//corporate-header/corporation/address[type='default']/phone" /> - Fax <xsl:value-of select="//corporate-header/corporation/address[type='default']/fax" /></drawRightString>
                <drawRightString x="20cm" y="22.8cm">E-mail: <xsl:value-of select="//corporate-header/corporation/address[type='default']/email" /></drawRightString>
                <rotate degrees="90" />
            <setFont name="Helvetica" size="7" />
            <drawString x="60mm" y="-4mm"><xsl:value-of select="//corporate-header/corporation/rml_footer2" /></drawString>
          </pageGraphics>
          <frame id="first" x1="1cm" y1="1cm" width="19cm" height="210mm"/>
        </pageTemplate>
      </template>
      <stylesheet>
      <paraStyle name="text"
                    fontName="Helvetica"
                    fontSize="10" />
      </stylesheet>
      <story t="1">
            <para t="1" style="text" alignment="center" fontSize="20">INTERRUPCI� TEMPORAL PROGRAMADA</para>
            <spacer length="12" />
            <para t="1" style="text" alignment="center" fontSize="20">DE SUBMINISTRAMENT EL�CTRIC</para>
            <spacer length="2cm" />
            <para t="1" style="text"><b t="1">Dia i hora d'inici</b>: <xsl:value-of select="concat(substring(data_inici, 9, 2),'/',substring(data_inici, 6, 2),'/',substring(data_inici, 1, 4), ' ', substring(data_inici, 12, 5))" /></para>
            <para t="1" style="text"><b t="1">Dia i hora de finalitzaci�</b>: <xsl:value-of select="concat(substring(data_final, 9, 2),'/',substring(data_final, 6, 2),'/',substring(data_final, 1, 4), ' ', substring(data_final, 12, 5))" /></para>
            <spacer length="1cm" />
            <para style="text" t="1" fontName="Helvetica-Bold">CENTRES DE TRANSFORMACI� AFECTATS:</para>
            <xpre style="text"><xsl:value-of select="cts_afectats" /></xpre>
            <spacer length="0.5cm" />
            <para style="text" t="1" fontName="Helvetica-Bold">NUCLIS AFECTATS:</para>
            <xpre style="text"><xsl:value-of select="nuclis_afectats" /></xpre>
            <spacer length="0.5cm" />
            <para style="text" t="1" fontName="Helvetica-Bold">Causes:</para>
            <xpre style="text"><xsl:value-of select="causes" /></xpre>
      </story>
    </document>
    </xsl:template>
</xsl:stylesheet>
