# -*- coding: utf-8 -*-
{
    "name": "Extensió CRM",
    "description": """
  * Report de ordre de feina i canvi de comptador
  * Referencia al segon abonat directament per contractació
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Extrareports",
    "depends":[
        "crm_generic",
        "soller_crm",
        "giscedata_polissa_crm",
        "jasper_reports"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_polissa_crm_chilla_report.xml"
    ],
    "active": False,
    "installable": True
}
