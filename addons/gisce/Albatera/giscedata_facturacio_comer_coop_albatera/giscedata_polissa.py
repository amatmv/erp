# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscedataPolissa(osv.osv):

    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def copy_data(self, cursor, uid, id, default=None, context=None):
        """
        No copiar las bonificaciones al duplicar contratos
        """
        if default is None:
            default = {}
        if context is None:
            context = {}
        default_values = {
            'bonificaciones': False,
        }
        default.update(default_values)
        res_id = super(GiscedataPolissa, self).copy_data(
            cursor, uid, id, default, context)
        return res_id

    def _search_bonos(self, cursor, uid, obj, name, args, context=None):
        """
        Método de búsqueda de bonificaciones. Se pueden buscar varios términos
        separando por ','.
        """
        if context is None:
            context = {}
        if not args:
            return [('id', '=', 0)]

        bono_obj = self.pool.get('giscedata.bonificacion.albatera')
        field, operator, value = args[0]

        polissa_ids = bono_obj.q(cursor, uid).read(['polissa_id']).where(
            [('bonificacion_kwh', operator, value)]
        )
        if polissa_ids:
            polissa_ids = map(lambda res: res['polissa_id'], polissa_ids)
            return [('id', 'in', polissa_ids)]
        else:
            return [('id', '=', 0)]

    def _bonos(self, cursor, uid, ids, field_name, arg, context=None):
        return dict.fromkeys(ids, False)

    _columns = {
        'bonificaciones': fields.one2many(
            'giscedata.bonificacion.albatera', 'polissa_id',
            'Bonificaciones'
        ),
        'buscar_bono': fields.function(
            _bonos, type='float', string=u'Bonificación',
            fnct_search=_search_bonos, method=True
        )
    }


GiscedataPolissa()
