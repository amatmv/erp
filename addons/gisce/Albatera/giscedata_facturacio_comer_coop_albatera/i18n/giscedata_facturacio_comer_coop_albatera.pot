# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* giscedata_facturacio_comer_coop_albatera
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.14\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2018-11-20 12:22\n"
"PO-Revision-Date: 2018-11-20 12:22\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: giscedata_facturacio_comer_coop_albatera
#: constraint:ir.ui.view:0
msgid "Invalid XML for View Architecture!"
msgstr ""

#. module: giscedata_facturacio_comer_coop_albatera
#: field:giscedata.bonificacion.albatera,fecha_inicio:0
msgid "Fecha Inicio"
msgstr ""

#. module: giscedata_facturacio_comer_coop_albatera
#: view:giscedata.bonificacion.albatera:0
#: field:giscedata.polissa,bonificaciones:0
#: field:giscedata.polissa,buscar_bono:0
msgid "Bonificaciones"
msgstr ""

#. module: giscedata_facturacio_comer_coop_albatera
#: constraint:ir.model:0
msgid "The Object name must start with x_ and not contain any special character !"
msgstr ""

#. module: giscedata_facturacio_comer_coop_albatera
#: field:giscedata.bonificacion.albatera,bonificacion_kwh:0
msgid "Bonificación (kWh)"
msgstr ""

#. module: giscedata_facturacio_comer_coop_albatera
#: model:ir.module.module,shortdesc:giscedata_facturacio_comer_coop_albatera.module_meta_information
msgid "Módulo de Facturación (Comercializadora)"
msgstr ""

#. module: giscedata_facturacio_comer_coop_albatera
#: field:giscedata.bonificacion.albatera,polissa_id:0
msgid "Contrato"
msgstr ""

#. module: giscedata_facturacio_comer_coop_albatera
#: model:ir.module.module,description:giscedata_facturacio_comer_coop_albatera.module_meta_information
msgid "Módulo de facturación personalitzado para Albatera"
msgstr ""

#. module: giscedata_facturacio_comer_coop_albatera
#: help:giscedata.bonificacion.albatera,fecha_fin:0
msgid "Dejar vacío si se quiere aplicar indefinidamente"
msgstr ""

#. module: giscedata_facturacio_comer_coop_albatera
#: field:giscedata.bonificacion.albatera,fecha_fin:0
msgid "Fecha Fin"
msgstr ""

#. module: giscedata_facturacio_comer_coop_albatera
#: model:ir.model,name:giscedata_facturacio_comer_coop_albatera.model_giscedata_bonificacion_albatera
msgid "giscedata.bonificacion.albatera"
msgstr ""

#. module: giscedata_facturacio_comer_coop_albatera
#: view:giscedata.bonificacion.albatera:0
msgid "Bonificación"
msgstr ""

