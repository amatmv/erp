# -*- coding: utf-8 -*-
from osv import osv


class GiscedataFacturacioValidationValidator(osv.osv):

    _name = 'giscedata.facturacio.validation.validator'
    _inherit = 'giscedata.facturacio.validation.validator'

    def check_consume_less_than_bonification(self, cursor, uid, fact,
                                             parameters):
        fact_obj = self.pool.get('giscedata.facturacio.factura')

        bonifications = fact_obj.bonificacion_aplicable(cursor, uid, fact.id)

        if fact.id in bonifications:
            inv_consume = fact.energia_kwh
            if inv_consume < bonifications[fact.id]:
                return {
                    'consume': inv_consume,
                    'bonification_kwh': bonifications[fact.id],
                }

        return None


GiscedataFacturacioValidationValidator()
