# -*- encoding: utf-8 -*-

from osv import osv, fields
from tools import cache

class GiscedataBonificacionAlbatera(osv.osv):

    _name = 'giscedata.bonificacion.albatera'

    _columns = {
        'polissa_id': fields.many2one(
            'giscedata.polissa', 'Contrato', required=True
        ),
        'fecha_inicio': fields.date('Fecha Inicio', required=True),
        'fecha_fin': fields.date(
            'Fecha Fin', help=u'Dejar vacío si se quiere aplicar '
                              u'indefinidamente'
        ),
        'bonificacion_kwh': fields.float(
            'Bonificación (kWh)', digits=(16, 2), required=True
        )
    }

    _order = 'fecha_inicio desc'

    def _dates_order(self, cursor, uid, ids):
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        dates = self.read(cursor, uid, ids, ['fecha_inicio', 'fecha_fin'])
        for vals in dates:
            if vals['fecha_fin'] and vals['fecha_inicio'] > vals['fecha_fin']:
                return False
        return True

    def _overlapped_bonus(self, cursor, uid, ids):
        """
        Para cada configuración de bono, comprueba que no se solapa con ninguna
        otra configuración de su mismo contrato.
        :param ids: giscedata.bonificacion.albatera ids.
        :return: False si hay algúna bonificación solapada.
        """
        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        for current_bono in self.read(
                cursor, uid, ids, ['polissa_id', 'fecha_inicio', 'fecha_fin']
        ):
            if self._return_overlapped_bonus(
                    cursor, uid, current_bono['polissa_id'][0],
                    current_bono['fecha_inicio'], current_bono['fecha_fin']
            ):
                return False
        return True

    def _search_existing_equal_bonus(self, cursor, uid, polissa_id,
                                     fecha_inicio, fecha_fin):
        """
        Buscar un bono con la misma fecha de inicio y fin en el mismo contrato.
        """
        search_params = [
            ('polissa_id', '=', polissa_id),
            ('fecha_inicio', '=', fecha_inicio),
            '|',
            ('fecha_fin', '=', False),
            ('fecha_fin', '=', fecha_fin),
        ]
        return self.search(cursor, uid, search_params)

    def _return_overlapped_bonus(self, cursor, uid, polissa_id, fecha_inicio,
                                 fecha_fin):
        """
        :return: dado un contrato, si hay algúna bonificación solapada entre
        las fechas dadas por parámetro, devuelve su id.
        """

        def between(date_1, date_2, date_3):
            """
            :return: date_1 is between date_2 and date_3
            """
            return date_2 <= date_1 <= date_3

        # We get all the bonus that apply to this contract
        params = [('polissa_id', '=', polissa_id)]
        bonuses_on_contract = self.search(
            cursor, uid, params, order='fecha_inicio asc')

        equal_bonuses = self._search_existing_equal_bonus(
            cursor, uid, polissa_id, fecha_inicio, fecha_fin)
        if len(equal_bonuses) > 1:
            # If there are more than one bonus equals, there are overlapped
            return True
        non_equal_bonuses = list(set(bonuses_on_contract)-set(equal_bonuses))

        # We need the dates of those bonus
        bonos_existentes = self.read(
            cursor, uid, non_equal_bonuses, ['fecha_inicio', 'fecha_fin']
        )

        # Then, for each of them, we want to check that both start date and
        # end date, aren't between any of the bonus of the contract.
        for bono_existente in bonos_existentes:
            try:
                # We consider a False end date as endless.
                fecha_fin_bono_existente = bono_existente['fecha_fin'] or '3000-01-01'
                fecha_fin = fecha_fin or '3000-01-01'

                # The start date is between an existing bonus
                assert not between(fecha_inicio, bono_existente['fecha_inicio'], fecha_fin_bono_existente)

                # The end date is between an existing bonus
                assert not between(fecha_fin, bono_existente['fecha_inicio'], fecha_fin_bono_existente)

                # Both the start date and the end date is between an existing
                # bonus
                assert not (
                    between(bono_existente['fecha_inicio'], fecha_inicio, fecha_fin) and
                    between(fecha_fin_bono_existente, fecha_inicio, fecha_fin)
                )
            except:
                return bono_existente['id']
        return False

    _constraints = [
        (
            _dates_order,
            'La fecha de inicio no puede ser posterior a la fecha de fin.',
            ['fecha_inicio', 'fecha_fin']
        ),
        (
            _overlapped_bonus,
            'Hay bonificaciones con fechas solapadas.',
            ['fecha_inicio', 'fecha_fin']
        )
    ]


GiscedataBonificacionAlbatera()


class GiscedataFacturacioFacturador(osv.osv):
    """ Sobreescritura del método fact_via_lectures para aplicar la bonificaicón
    """
    _name = 'giscedata.facturacio.facturador'
    _inherit = 'giscedata.facturacio.facturador'

    def fact_via_lectures(self, cursor, uid, polissa_id, lot_id, context=None):
        facturas = super(GiscedataFacturacioFacturador, self).fact_via_lectures(
            cursor, uid, polissa_id, lot_id, context
        )
        factura_obj = self.pool.get('giscedata.facturacio.factura')
        factura_obj.aplicar_bonificacion(cursor, uid, facturas, context)
        factura_obj.button_reset_taxes(cursor, uid, facturas)

        # Si las facturas no tienen consumo, tenemos que eliminar el IESE
        facts_to_remove_iese = super(
            GiscedataFacturacioFacturador, self).get_invoices_to_remove_iese(
            cursor, uid, facturas, context=context
        )

        if facts_to_remove_iese:
            super(GiscedataFacturacioFacturador, self).remove_iese_tax(
                cursor, uid, facts_to_remove_iese, context=context
            )
        return facturas


GiscedataFacturacioFacturador()


class GiscedataFacturacioFactura(osv.osv):

    _name = 'giscedata.facturacio.factura'
    _inherit = 'giscedata.facturacio.factura'

    def bonificacion_aplicable(self, cursor, uid, ids, context=None):
        """
        Devuelve la bonificación aplicable (en kWh) para el cliente dada una 
        fecha, indexado por id de factura.
        :return: {
            fact_id: cantidad,
            ...
        }
        """

        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        res = {}
        for fact_id in ids:
            cursor.execute("""
                SELECT gdba.bonificacion_kwh, gff.dies
                FROM giscedata_bonificacion_albatera gdba
                JOIN giscedata_polissa AS gdp ON gdp.id = gdba.polissa_id
                JOIN giscedata_facturacio_factura gff ON gff.polissa_id = gdp.id
                JOIN account_invoice ai ON gff.invoice_id = ai.id
                WHERE gff.id = %s
                AND gff.data_final BETWEEN gdba.fecha_inicio
                AND COALESCE(gdba.fecha_fin, '3000-01-01')
                AND ai.type IN ('out_invoice', 'out_refund')
                ORDER BY gdba.fecha_fin DESC
                """, (fact_id,)
            )
            query_result = cursor.dictfetchone()
            if query_result is not None:
                # Valor de la bonificació = bonificacion_kwh * 12 / 365 * dies
                res[fact_id] = (
                    query_result['bonificacion_kwh'] * 12 /
                    365 * query_result['dies']
                )
        return res
    
    def bonus_account_get(self, cursor, uid, context=None):
        account_o = self.pool.get('account.account')
        conf_o = self.pool.get('res.config')
        conf_v = conf_o.get(cursor, uid, 'bonus_account', '')
        if not conf_v:
            raise osv.except_osv(
                "Error", "No hay una cuenta contable para bonificaciones. Véase la variable de configuración <bonus_account>"
            )

        account_id = account_o.search(cursor, uid, [('code', '=', conf_v)])
        if not account_id:
            raise osv.except_osv(
                "Error", "La cuenta contable de bonificación {} no existe.".format(conf_v)
            )

        return account_id[0]

    def aplicar_bonificacion(self, cursor, uid, ids, context=None):
        """
        Por cada factura, lee la cantidad a bonificar según información del
        contrato y le aplica la bonificación, creando líneas en
        la factura con importe negativo y la cantidad correspondiente según
        período.
        """

        if context is None:
            context = {}

        if not isinstance(ids, (tuple, list)):
            ids = [ids]

        linia_obj = self.pool.get('giscedata.facturacio.factura.linia')
        bonus_account_id = self.bonus_account_get(cursor, uid, context=context)

        for fact_id, kwh_bonificados in self.bonificacion_aplicable(
                cursor, uid, ids).items():
            if not kwh_bonificados:
                continue

            lines = {}
            total_kwh_consumits = 0
            for line_id in self.read(cursor, uid, fact_id,
                                     ['linies_energia'])['linies_energia']:
                line_vals, __ = linia_obj.copy_data(cursor, uid, line_id)
                lines[line_vals['product_id']] = line_vals
                total_kwh_consumits += line_vals['quantity']

            if total_kwh_consumits < kwh_bonificados:
                kwh_bonificados = float(total_kwh_consumits)

            if not total_kwh_consumits:
                continue

            for __, line_vals in lines.items():
                line_vals.update({
                    'price_unit_multi': -line_vals['price_unit_multi'],
                    'quantity': (
                            line_vals['quantity'] / total_kwh_consumits *
                            kwh_bonificados
                    ),
                    'name': u'Bonificación {}'.format(line_vals['name']),
                    'account_id': bonus_account_id,
                    'isdiscount': True
                })
                linia_obj.create(cursor, uid, line_vals)


GiscedataFacturacioFactura()
