# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction

from giscedata_polissa.tests import utils as utils_polissa
from osv.orm import ValidateException

from datetime import datetime, timedelta


class TestBonificacion(testing.OOTestCase):

    def setUp(self):
        """
        Definición de variables de entorno.
        """
        self.pool = self.openerp.pool
        self.imd_obj = self.pool.get('ir.model.data')
        self.bono_obj = self.pool.get('giscedata.bonificacion.albatera')
        self.fact_obj = self.pool.get('giscedata.facturacio.factura')
        self.polissa_obj = self.pool.get('giscedata.polissa')
        self.wiz_obj = self.pool.get('wizard.manual.invoice')
        self.journal_obj = self.pool.get('account.journal')
        self.lectura_obj = self.pool.get('giscedata.lectures.lectura')
        self.lectura_pot_obj = self.pool.get('giscedata.lectures.potencia')
        self.tax_obj = self.pool.get('account.tax')

        self.openerp.install_module(
            'giscedata_tarifas_peajes_20180101'
        )
        self.openerp.install_module(
            'giscedata_tarifas_pagos_capacidad_20180101'
        )

        self.FECHA_INICIO_BONO_1 = '2018-01-01'
        self.FECHA_FIN_BONO_1 = '2018-12-31'
        self.INVOICE_START_DATE = '2018-03-01'
        self.INVOICE_END_DATE = '2018-03-31'
        self.CONSUM_P1 = 55
        self.CONSUM_P2 = 38
        self.CANTIDAD_BONIFICADA = 80

        self.txn = Transaction().start(self.database)

    def tearDown(self):
        self.txn.stop()

    def setup_contract(self, cursor, uid, context=None):
        """
        Preparamos el contrato, las lecturas i los impuestos para poder
        bonificar un cliente.
        """
        if context is None:
            context = {}
        self.import_data(cursor, uid)
        polissa = self.polissa_obj.browse(
            cursor, uid, self.polissa_id
        )

        polissa.write({
            'potencia': context.get('potencia', 2),
            'llista_preu': self.pricelist_id,
            'tarifa': self.tarifa_id
        })
        polissa.send_signal([
            'validar', 'contracte'
        ])

        # Borramos todos los contadores activos para evitar interferencias
        for comptador in polissa.comptadors:
            for l in comptador.lectures:
                l.unlink(context={})
            for lp in comptador.lectures_pot:
                lp.unlink(context={})
            comptador.write({'lloguer': False})

        comptador = polissa.comptadors[0]

        # Lectura anterior P1
        vals = {
            'name': '2018-02-10',
            'periode': self.periode_id_p1,
            'lectura': 10,
            'tipus': 'A',
            'comptador': comptador.id,
            'observacions': '',
            'origen_id': self.origen_id,
        }
        self.lectura_obj.create(cursor, uid, vals)

        # Lectura anterior P2
        vals = {
            'name': '2018-02-10',
            'periode': self.periode_id_p2,
            'lectura': 25,
            'tipus': 'A',
            'comptador': comptador.id,
            'observacions': '',
            'origen_id': self.origen_id,
        }
        self.lectura_obj.create(cursor, uid, vals)

        # Lectura actual P1
        vals = {
            'name': '2018-03-11',
            'periode': self.periode_id_p1,
            'lectura': 10 + self.CONSUM_P1,
            'tipus': 'A',
            'comptador': comptador.id,
            'observacions': '',
            'origen_id': self.origen_id,
        }
        self.lectura_obj.create(cursor, uid, vals)

        # Lectura actual P2
        vals = {
            'name': '2018-03-11',
            'periode': self.periode_id_p2,
            'lectura': 25 + self.CONSUM_P2,
            'tipus': 'A',
            'comptador': comptador.id,
            'observacions': '',
            'origen_id': self.origen_id,
        }
        self.lectura_obj.create(cursor, uid, vals)

        utils_polissa.crear_modcon(
            self.pool, cursor, uid, self.polissa_id, {
                'potencies_periode': [
                    (1, polissa.potencies_periode[0].id, {'potencia': 5.750})
                ]
             }, '2018-01-01', '2018-12-31'
        )

        config_taxes_wiz_obj = self.pool.get('config.taxes.facturacio.comer')

        iva_venda = self.tax_obj.search(cursor, uid, [
            ('name', '=', 'IVA 21%'),
            ('type_tax_use', 'ilike', 'sale')
        ])[0]
        iva_compra = self.tax_obj.search(cursor, uid, [
            ('name', 'ilike', '%21% IVA Soportado (operaciones corrientes)%'),
            ('type_tax_use', 'ilike', 'purchase')
        ])[0]
        iese_venda = self.tax_obj.search(cursor, uid, [
            ('name', 'ilike', '%Impuesto especial sobre la electricidad%'),
        ])[0]
        wiz_id = config_taxes_wiz_obj.create(cursor, uid, {
            'iva_venda': iva_venda,
            'iese_venda': iese_venda,
            'iva_compra': iva_compra,
        })
        config_taxes_wiz_obj.action_set(cursor, uid, [wiz_id])

        self.bono_obj.create(cursor, uid, {
            'bonificacion_kwh': self.CANTIDAD_BONIFICADA,
            'polissa_id': context.get('polissa_id', self.polissa_id),
            'fecha_inicio': self.FECHA_INICIO_BONO_1,
            'fecha_fin': self.FECHA_FIN_BONO_1,
        })
        return polissa

    def setup_invoice(self, cursor, uid, context=None):
        """
        Creamos la factura con el asistente.
        :return: browse_record de la factura.
        """
        if context is None:
            context = {}
        wiz_id = self.wiz_obj.create(
            cursor, uid, {}
        )
        wiz_fact = self.wiz_obj.browse(
            cursor, uid, wiz_id
        )
        wiz_fact.write({
            'polissa_id': context.get('polissa_id', self.polissa_id),
            'date_start': context.get('date_start', self.INVOICE_START_DATE),
            'date_end': context.get('date_end', self.INVOICE_END_DATE),
            'journal_id': context.get('journal_id', self.journal_id)
        })
        wiz_fact.action_manual_invoice()
        invoice_id = eval(wiz_fact.invoice_ids)[0]

        invoice = self.fact_obj.browse(
            cursor, uid, invoice_id
        )
        wiz_fact.unlink()
        return invoice

    def import_data(self, cursor, uid):
        """
        Importamos datos de demo para los tests.
        """
        self.polissa_id = self.imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]
        self.pricelist_id = self.imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio',
            'pricelist_tarifas_electricidad'
        )[1]

        self.periode_id_p1 = self.imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'p1_e_tarifa_21DHS'
        )[1]

        self.periode_id_p2 = self.imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'p2_e_tarifa_21DHS'
        )[1]

        self.origen_id = self.imd_obj.get_object_reference(
            cursor, uid, 'giscedata_lectures', 'origen10'
        )[1]

        self.journal_id = self.journal_obj.search(
            cursor, uid, [('code', '=', 'ENERGIA')]
        )[0]

        self.tarifa_id = self.imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'tarifa_21DHS'
        )[1]

    def get_cantidad_bonificada(self, cursor, uid, invoice_id):
        """
        Obtener cantidad bonificada en kWh por cada periodo
        """
        linia_obj = self.pool.get('giscedata.facturacio.factura.linia')
        line_ids = self.fact_obj.read(cursor, uid, invoice_id,
                                      ['linies_energia'])['linies_energia']
        line_values = linia_obj.read(
            cursor, uid, line_ids, ['price_unit_multi', 'product_id', 'quantity']
        )
        cantidades = {}
        if line_values is not None:
            # Hay que seleccionar cantidades negativas porque son las lineas de
            # bonificación
            for values in line_values:
                if values['price_unit_multi'] > 0:
                    continue
                # Por cada periodo
                cantidades[values['product_id'][1]] = values['quantity']
        return cantidades

    @staticmethod
    def cantidad_total_bonificada(bonificaciones):
        """
        Dado un diccionario con las cantidades bonificadas en un contrato,
        indexadas por periodo, devuelve la cantidad total bonificada.
        """
        cantidad = 0
        for _, bono in bonificaciones.items():
            cantidad += bono
        return cantidad

    @staticmethod
    def truncate(number, decimals):
        return int(number * 10 ** decimals) / 10.0 ** decimals

    def test_apply_bonificacion_invoice(self):
        """
        Test que comprueba que la bonificación es aplicada proporcionalmente
        por cada período de energía cuando se crea una factura que le
        corresponde una cantidad a descontar.
        """

        cursor = self.txn.cursor
        uid = self.txn.user

        self.setup_contract(cursor, uid)
        invoice = self.setup_invoice(cursor, uid)

        _, bono_a_aplicar = self.fact_obj.bonificacion_aplicable(
            cursor, uid, invoice.id
        ).items()[0]

        cantidad_bonificada = self.get_cantidad_bonificada(cursor, uid,
                                                           invoice.id)

        bono_aplicado = self.cantidad_total_bonificada(cantidad_bonificada)

        self.assertTrue(round(bono_a_aplicar, 3) == round(bono_aplicado, 3))

        # Por cada perído, comprobamos que las cantidades bonificadas son el
        # resultado de multiplicar la cantidad bonficada total por la relación:
        # consumo0 período / consumo total.
        self.assertEquals(
            cantidad_bonificada['P1'],
            round(bono_aplicado * (
                float(self.CONSUM_P1) / (self.CONSUM_P1 + self.CONSUM_P2)
            ), 3)
        )

        self.assertEquals(
            cantidad_bonificada['P2'],
            round(bono_aplicado * (
                float(self.CONSUM_P2) / (self.CONSUM_P1 + self.CONSUM_P2)
            ), 3)
        )

        # Comprobamos que la base de la factura ha estado bien calculada
        base_teorica_factura = sum(line.price_subtotal
                                   for line in invoice.linia_ids)
        self.assertEquals(invoice.amount_untaxed, base_teorica_factura)

        # Tambien tenemos que comprobar que los impuestos se han aplicado
        # correctamente. Para esto, comprobaremos que la base de cada impuesto
        # corresponda a la base + impuesto del anterior.
        self.assertTrue(len(invoice.tax_line) > 0)
        base_calculated = invoice.amount_untaxed
        for tax in invoice.tax_line:
            self.assertEquals(tax.base, base_calculated)
            base_calculated = tax.base + tax.amount

        # Finalmente, la cantidad calculada que tenemos hasta ahora debe
        # corresponder a la cantidad con impuestos de la factura
        self.assertEquals(round(base_calculated, 3),
                          round(invoice.amount_total, 3))

    def test_bonificacion_isnt_applied_when_has_finished(self):
        """
        Test para comprobar que no se bonifica ninguna cantidad si se factura
        y la factura acaba después de la bonifiación.
        """

        cursor = self.txn.cursor
        uid = self.txn.user

        self.setup_contract(cursor, uid)

        bono_id = self.bono_obj.search(cursor, uid, [
            ('polissa_id', '=', self.polissa_id)
        ])

        self.bono_obj.write(cursor, uid, bono_id, {
            'fecha_fin': datetime.strftime(
                (
                    datetime.strptime(self.INVOICE_END_DATE, '%Y-%m-%d') -
                    timedelta(days=10)
                ), '%Y-%m-%d'
            )
        })

        invoice = self.setup_invoice(cursor, uid)

        cantidad_bonificada = self.get_cantidad_bonificada(cursor, uid,
                                                           invoice.id)

        self.assertEquals(self.cantidad_total_bonificada(cantidad_bonificada), 0)

    def test_iese_applies_when_bono_is_over_consume(self):
        """
        Test para comprobar que el IESE se aplica cuando se bonifica
        toda la factura porque el consumo es menor.
        """

        cursor = self.txn.cursor
        uid = self.txn.user

        self.setup_contract(cursor, uid)

        bono_id = self.bono_obj.search(cursor, uid, [
            ('polissa_id', '=', self.polissa_id)
        ])

        self.bono_obj.write(cursor, uid, bono_id, {
            'bonificacion_kwh': (self.CONSUM_P1 + self.CONSUM_P2) / 2
        })

        # Creamos la factura manualmente
        invoice = self.setup_invoice(cursor, uid)

        # Assert that there's a IESE tax in invoice's taxes.
        self.assertTrue(
            filter(lambda tax: 'electricidad' in tax.name, invoice.tax_line)
        )

    def test_iese_applies_when_bono_is_like_consume(self):
        """
        Test para comprobar que el IESE se aplica cuando se bonifica
        la misma cantidad que se consume.
        """

        cursor = self.txn.cursor
        uid = self.txn.user

        self.setup_contract(cursor, uid)

        bono_id = self.bono_obj.search(cursor, uid, [
            ('polissa_id', '=', self.polissa_id)
        ])

        self.bono_obj.write(cursor, uid, bono_id, {
            'bonificacion_kwh': (self.CONSUM_P1 + self.CONSUM_P2)
        })

        # Creamos la factura manualmente
        invoice = self.setup_invoice(cursor, uid)

        # Assert that there's a IESE tax in invoice's taxes.
        self.assertTrue(
            filter(lambda tax: 'electricidad' in tax.name, invoice.tax_line)
        )

    def test_iese_doesnt_apply_when_consume_is_zero(self):
        """
        Test para comprobar que el IESE NO se aplica cuando el consumo del
        cliente es 0.
        Esta funcionalidad está probada en facturacio_comer, pero se comprueba
        que la implementación de la bonificación no la rompa.
        """

        cursor = self.txn.cursor
        uid = self.txn.user

        self.CONSUM_P1 = self.CONSUM_P2 = 0
        self.setup_contract(cursor, uid)
        # Creamos la factura manualmente
        invoice = self.setup_invoice(cursor, uid)

        # Assert that there isn't a IESE tax in invoice's taxes.
        self.assertFalse(
            filter(lambda tax: 'electricidad' in tax.name, invoice.tax_line)
        )

    def test_overlapped_bonuses(self):
        """
        Test overlapped bonuses doesn't create.

        Case 1
                            Bono 1
                         |---------|
                |------------|
                    Bono 2

        Case 2
                            Bono 1
                         |---------|
                              |--------------|
                                   Bono 2

        Case 3
                           Bono 1
                        |---------|
                   |-------------------|
                           Bono 2
        Case 4
                       Bono 1
                   |-------------|
                   |-------------|
                       Bono 2
        """

        cursor = self.txn.cursor
        uid = self.txn.user

        self.setup_contract(cursor, uid)

        bono_id = self.bono_obj.search(cursor, uid, [
            ('polissa_id', '=', self.polissa_id)
        ])

        self.bono_obj.write(cursor, uid, bono_id, {
            'fecha_inicio': datetime.strftime(
                (
                    datetime.strptime(self.FECHA_INICIO_BONO_1, '%Y-%m-%d') -
                    timedelta(days=15)
                ), '%Y-%m-%d'
            ),
            'fecha_fin': datetime.strftime(
                (
                    datetime.strptime(self.FECHA_FIN_BONO_1, '%Y-%m-%d') +
                    timedelta(days=10)
                ), '%Y-%m-%d'
            ),
        })

        # New behavior: now the ERP restricts the creation of overlapped bonus
        # and can not be created.
        # Case 1
        with self.assertRaises(ValidateException):
            self.bono_obj.create(cursor, uid, {
                'bonificacion_kwh': 80,
                'polissa_id': self.polissa_id,
                'fecha_inicio': datetime.strftime(
                    (
                        datetime.strptime(self.FECHA_INICIO_BONO_1, '%Y-%m-%d') -
                        timedelta(days=5)
                    ), '%Y-%m-%d'
                ),
                'fecha_fin': datetime.strftime(
                    (
                        datetime.strptime(self.FECHA_INICIO_BONO_1, '%Y-%m-%d') +
                        timedelta(days=5)
                    ), '%Y-%m-%d'
                ),
            })

        # Case 2
        with self.assertRaises(ValidateException):
            self.bono_obj.create(cursor, uid, {
                'bonificacion_kwh': 80,
                'polissa_id': self.polissa_id,
                'fecha_inicio': datetime.strftime(
                    (
                        datetime.strptime(self.FECHA_FIN_BONO_1, '%Y-%m-%d') -
                        timedelta(days=5)
                    ), '%Y-%m-%d'
                ),
                'fecha_fin': datetime.strftime(
                    (
                        datetime.strptime(self.FECHA_FIN_BONO_1, '%Y-%m-%d') +
                        timedelta(days=5)
                    ), '%Y-%m-%d'
                ),
            })

        # Case 3
        with self.assertRaises(ValidateException):
            self.bono_obj.create(cursor, uid, {
                'bonificacion_kwh': 80,
                'polissa_id': self.polissa_id,
                'fecha_inicio': datetime.strftime(
                    (
                        datetime.strptime(self.FECHA_INICIO_BONO_1, '%Y-%m-%d') -
                        timedelta(days=5)
                    ), '%Y-%m-%d'
                ),
                'fecha_fin': datetime.strftime(
                    (
                        datetime.strptime(self.FECHA_FIN_BONO_1, '%Y-%m-%d') +
                        timedelta(days=5)
                    ), '%Y-%m-%d'
                ),
            })

        # Case 4
        with self.assertRaises(ValidateException):
            self.bono_obj.create(cursor, uid, {
                'bonificacion_kwh': 80,
                'polissa_id': self.polissa_id,
                'fecha_inicio': datetime.strptime(
                    self.FECHA_INICIO_BONO_1,'%Y-%m-%d'
                ),
                'fecha_fin': datetime.strptime(
                    self.FECHA_INICIO_BONO_1, '%Y-%m-%d'
                )
            })

    def test_validation_A001_lot(self):

        cursor = self.txn.cursor
        uid = self.txn.user

        cnt_lot_obj = self.pool.get('giscedata.facturacio.contracte_lot')
        lot_obj = self.pool.get('giscedata.facturacio.lot')
        vali_obj = self.pool.get('giscedata.facturacio.validation.validator')
        warn_obj = self.pool.get('giscedata.facturacio.validation.warning')

        self.setup_contract(cursor, uid)

        # We remove other lot_contracts associated with out invoicing lot
        lot_id = self.polissa_obj.read(cursor, uid, self.polissa_id,
                                       ['lot_facturacio'])['lot_facturacio'][0]

        cnt_lot_to_remove = lot_obj.read(cursor, uid, lot_id,
                                         ['contracte_lot_ids'])['contracte_lot_ids']
        cnt_lot_id = cnt_lot_to_remove[0]
        cnt_lot_to_remove.remove(cnt_lot_id)
        cnt_lot_obj.unlink(cursor, uid, cnt_lot_to_remove)
        lot_obj.write(cursor, uid, lot_id, {
            'data_final': self.INVOICE_END_DATE,
            'data_inici': self.INVOICE_START_DATE
        })

        invoice = self.setup_invoice(cursor, uid, context={
            'date_start': self.INVOICE_START_DATE,
            'date_end': self.INVOICE_END_DATE
        })

        # Next step, we want to validate the lot and the A001 SHOULDN'T pop
        warning_ids = vali_obj.validate_invoice(cursor, uid, invoice.id)

        warning_vals = warn_obj.read(cursor, uid, warning_ids, ['name'])

        warning_names = [warn['name'] for warn in warning_vals]
        self.assertNotIn('A001', warning_names)

        # Now we increase the quantity to discount to more than the consume
        bono_id = self.bono_obj.search(cursor, uid, [
            ('polissa_id', '=', self.polissa_id)
        ])

        self.bono_obj.write(cursor, uid, bono_id, {
            'bonificacion_kwh': (self.CONSUM_P1 + self.CONSUM_P2) * 2
        })

        # And if we validate the invoice again, the warning will appear
        warning_ids = vali_obj.validate_invoice(cursor, uid, invoice.id)

        warning_vals = warn_obj.read(cursor, uid, warning_ids, ['name'])

        warning_names = [warn['name'] for warn in warning_vals]
        self.assertIn('A001', warning_names)

    def test_bonificacions_arent_copied_between_contracts(self):
        """
        Check that bonifications doesn't duplicate when copying the contract
        """

        cursor = self.txn.cursor
        uid = self.txn.user

        self.setup_contract(cursor, uid)

        bonificaciones = self.polissa_obj.read(
            cursor, uid, self.polissa_id, ['bonificaciones'])['bonificaciones']

        self.assertGreater(len(bonificaciones), 0)

        new_contract = self.polissa_obj.copy(cursor, uid, self.polissa_id)

        bonificaciones = self.polissa_obj.read(
            cursor, uid, new_contract, ['bonificaciones'])['bonificaciones']

        self.assertFalse(bonificaciones)
