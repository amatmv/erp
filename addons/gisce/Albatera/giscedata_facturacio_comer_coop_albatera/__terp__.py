# -*- coding: utf-8 -*-
{
    "name": "Módulo de Facturación Coop Albatera (Comercializadora)",
    "description": """Facturación Coop Albatera""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Albatera",
    "depends": [
        "base",
        "giscedata_facturacio_comer",
        "giscedata_facturacio_services"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "giscedata_facturacio_comer_report.xml",
        "giscedata_polissa_view.xml",
        "giscedata_facturacio_validation_data.xml",
        "giscedata_facturacio_comer_data.xml",
        "security/ir.model.access.csv",
    ],
    "active": False,
    "installable": True
}
