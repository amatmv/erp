<%
    from operator import attrgetter, itemgetter
    from datetime import datetime, timedelta
    import json

    def clean_nif(nif):
        if nif:
            return nif.replace('ES', '')
        else:
            return ''
%>
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<head>
    <style type="text/css">
        ${css}
    </style>
    <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_albatera/report/factura_conceptes.css"/>
</head>
<body>
    <%
        pool = objects[0].pool
        polissa_obj = objects[0].pool.get('giscedata.polissa')
        comptador_factures = 0
        from datetime import datetime

        def clean(text):
            return text or ''
    %>
     %for inv in objects :
##     <div class="back" style="position:relative; margin:0; padding:0;">
##     <div class="background">
##         <img src="${addons_path}/giscedata_facturacio_comer_ab/report/FACTURA_ABE.png" >
##     </div>
         <% company_address = inv.company_id.partner_id.address[0] %>

        <div id="lateral-info" class="center">
            ${company.rml_footer2 or ''}
        </div>
        <div id="page">
            <table id="header">
                <colgroup>
                    <col width="60%"/>
                    <col>
                </colgroup>
                <tbody>
                    <tr>
                        <td rowspan="4"><img src="data:image/jpeg;base64,${company.logo}"/></td>
                        <td>${inv.company_id.partner_id.name}</td>
                    </tr>
                    <tr>
                        <td>${clean_nif(inv.company_id.partner_id.vat)}</td>
                    </tr>
                    <tr>
                        <td>${company_address.street.replace('..', '.')}</td>
                    </tr>
                    <tr>
                        <td>${company_address.zip}, ${company_address.id_poblacio.name} (${company_address.state_id.name})</td>
                    </tr>
                </tbody>
            </table>
##     <div class="wrapper">

            <%
                altres_lines = [l for l in inv.linia_ids]
                total_altres = sum([l.price_subtotal for l in altres_lines])

                # bar code
                entity = ''.join([c for c in inv.polissa_id.comercialitzadora.vat if c.isdigit()])
                due_date = str(inv.date_due) # plazo pago = fecha factura + payment_term
                if due_date:
                    suffix = '501'
                    due_date = inv.date_due
                else:
                    suffix = '101'
                    due_date = None
                ref = '{}'.format(inv.id).zfill(11) # id factura
                notice = '000000'
                amount = '{0:.2f}'.format(inv.amount_total) # amount factura
                periodes_a = sorted(list(set([lectura.name[-3:-1]
                                for lectura in inv.lectures_energia_ids
                                if lectura.tipus == 'activa'])))
                polissa = polissa_obj.browse(cursor, uid, inv.polissa_id.id)
                setLang(inv.partner_id.lang)
            %>
            <!-- capcalera -->
            <div class="bloc capcalera">
                <div class="esquerra">
                    <span>&nbsp;</span>
                </div>
                <div class="dreta">
                   <div class="box address">
                       <p>${inv.partner_id.name}</p>
                       <p>${inv.address_contact_id.street}</p>
                       <div>
                           <span style="float:left; max-width: 100%; ">${inv.address_contact_id.zip}</span>
                           <span style="float:left; max-width: 100%; display:block; padding-left: 15px;">
                            ${inv.address_contact_id.id_poblacio.name}
                               <br>
                            ${inv.address_contact_id.state_id.name}
                           </span>
                       </div>
                    </div>
                </div>
                <div class="clear fi_bloc"></div>
            </div><!-- Fi capcalera -->
            <!-- part central -->
            <div class="bloc part_central">
                <table class="styled-table round-top-borders">
                    <thead>
                        <tr>
                            <th>Factura Nº</th>
                            <th>Fecha</th>
                            <th>Nº Cliente</th>
                            <th>DNI/CIF</th>
                            <th>Nº Póliza</th>
                            <th>Fecha Alta</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>${clean(inv.number)}</td>
                            <td>${inv.date_invoice}</td>
                            <td>${clean(polissa.name)}</td>
                            <td>${inv.partner_id.vat and clean(inv.partner_id.name.replace('ES',''))}</td>
                            <td>${clean(polissa.name)}</td>
                            <td>${clean(polissa.data_alta)}</td>
                        </tr>
                    </tbody>
                </table>
                <table class="styled-table">
                    <caption>Datos del Punto de Suministro</caption>
                    <thead>
##                         <tr>
##                             <th colspan="2">Datos del Punto de Suministro</th>
##                         </tr>
                        <tr>
                            <th>Nombre del Titular</th>
                            <th>Ciudad</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>${clean(polissa.titular.name)}</td>
                            <td>${clean(polissa.cups.id_poblacio.name)}</td>
                        </tr>
                        <tr>
                            <th>Domicilio</th>
                            <th>CUPS</th>
                        </tr>
                        <tr>
                            <td>${clean(polissa.cups.direccio)}</td>
                            <td>${clean(polissa.cups.name)}</td>
                        </tr>
                    </tbody>
                </table>
                <div class="content-concepts">
                    <table class="styled-table">
                        <thead>
                            <tr>
                                <th class="concept">Descripción del Concepto</th>
                                <th class="iva">% I.V.A.</th>
                                <th class="import">Importe</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                iva_set = set()
                            %>
                            %for l in altres_lines:
                                <tr>
                                    <td>${l.name}</td>
                                    <td class="number">
                                        %for tax in l.invoice_line_tax_id:
                                            %if 'IVA' in tax.name:
                                                ${formatLang(tax.amount * 100.0)}
                                                <%
                                                    iva_set.add(tax.amount * 100.0)
                                                %>
                                            %endif
                                        %endfor
                                    </td>
                                    <td class="number">${"%s" % formatLang(l.price_subtotal)}</td>
                                </tr>
                            %endfor
                        </tbody>
                    </table>
                </div>
                <table class="total">
                    <thead>
                        <tr>
                            <th>Suma Importes</th>
                            <th>Base Imponible</th>
                            <th>% IVA</th>
                            <th>Importe IVA</th>
                            <th class="bold">TOTAL FACTURA</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="number">${formatLang(total_altres)} €</td>
                            <td class="number">${formatLang(inv.amount_total)} €</td>
                            <td class="number">${len(list(iva_set)) and formatLang(list(iva_set)[0]) or ''} €</td>
                            <td class="number">${formatLang(inv.amount_tax)} €</td>
                            <td class="number bold">${formatLang(inv.amount_total)} €</td>
                        </tr>
                    </tbody>
                </table>
            </div> <!-- part central -->
            <div class="peu"> <!-- part peu -->
                <div class="esquerra">
                    <table class="styled-table">
                        <tbody>
                            <tr>
                                <th>Forma de pago</th>
                                <td>${clean(inv.payment_type.name)}</td>
                            </tr>
                            % if inv.payment_type.name != "Transferencia":
                                <tr>
                                    <th>Domiciliación</th>
                                    <td>${inv.partner_bank.name if inv.partner_bank.name else ""}</td>
                                </tr>
                                <tr>
                                    <th>Nº de Cuenta</th>
                                    <td>${inv.partner_bank.iban[4:]}</td>
                                </tr>
                            % endif
                        </tbody>
                    </table>
                </div>
                <div class="clear fi_bloc"></div>
            </div> <!-- fi peu -->
            <%
            comptador_factures += 1
            %>
            % if comptador_factures<len(objects):
                <p style="page-break-after:always;"></p>
            % endif

        </div> <!-- page -->
##       </div> <!-- wrapper -->
##     </div> <!-- back -->
    %endfor
</body>
</html>