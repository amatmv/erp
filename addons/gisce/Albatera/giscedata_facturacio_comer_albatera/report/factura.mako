## -*- coding: utf-8 -*-
<%
    from operator import attrgetter, itemgetter
    from datetime import datetime, timedelta
    import json
    from giscedata_facturacio_comer.report.utils import has_setu35_line

    pool = objects[0].pool
    polissa_obj = objects[0].pool.get('giscedata.polissa')
    model_obj = objects[0].pool.get('ir.model.data')
    sup_territorials_2013_comer_obj = pool.get('giscedata.suplements.territorials.2013.comer')

    model_ids = model_obj.search(
        cursor, uid,
         [('module','=','giscedata_facturacio'),
         ('name','=','pricelist_tarifas_electricidad')]
    )
    tarifa_elect_atr = model_obj.read(cursor, uid, model_ids[0],['res_id'])['res_id']

    iet_peatges = "ORDEN IET/107/2014"
    itc_lloguer = "ORDEN ITC/3860/2007"

    comptador_factures = 0

    def clean_nif(nif):
        if nif:
            return nif.replace('ES', '')
        else:
            return ''

    def get_exces_potencia(cursor, uid, factura):
        linia_obj = factura.pool.get('giscedata.facturacio.factura.linia')
        linies = linia_obj.search(
            cursor, uid,[('tipus', '=', 'exces_potencia'),('factura_id', '=', factura.id)]
        )
        if linies:
            return linia_obj.browse(cursor, uid, linies)
        else:
            return []


    def get_atr_price(cursor, uid, tarifa, linia):
        pricelist_obj = linia.pool.get('product.pricelist')
        uom_obj = linia.pool.get('product.uom')
        model_obj = objects[0].pool.get('ir.model.data')

        tipus = linia.tipus
        uom_data = {'uom_name': linia.uos_id.name, 'factor': 1}
        quantity = linia.quantity
        product_id = linia.product_id.id

        if tipus == 'potencia':
            uom_data = {'uom_name': 'kW/mes', 'factor': 1}
        elif tipus == 'reactiva':
            model_ids = model_obj.search(
                cursor, uid, [('module','=','giscedata_facturacio'), ('name','=','product_cosfi')]
            )
            product_id = model_obj.read(cursor, uid, model_ids[0],['res_id'])['res_id']
            quantity = l.cosfi * 100

        uom_id = uom_obj.search(cursor, uid, [('name', '=', uom_data['uom_name'])])[0]

        atr_price = pricelist_obj.price_get(
            cursor, uid, [tarifa], product_id, quantity,
            factura.company_id.partner_id.id,
            {'date': linia.data_desde, 'uom_name': uom_id}
        )[tarifa]
        return atr_price

    def get_origen_lectura(cursor, uid, lectura):
        """Busquem l'origen de la lectura cercant-la a les lectures de facturació"""
        res = {lectura.data_actual: _(u"real"),
               lectura.data_anterior: _(u"real")}

        lectura_obj = lectura.pool.get('giscedata.lectures.lectura')
        tarifa_obj = lectura.pool.get('giscedata.polissa.tarifa')
        origen_obj = lectura.pool.get('giscedata.lectures.origen')

        # Recuperar l'estimada
        estimada_id = origen_obj.search(cursor, uid, [('codi', '=', '40')])[0]

        #Busquem la tarifa
        if lectura and lectura.name:
            tarifa_id = tarifa_obj.search(cursor, uid, [('name', '=',
                                                    lectura.name[:-5])])
        else:
            tarifa_id = False
        if tarifa_id:
            tipus = lectura.tipus == 'activa' and 'A' or 'R'

            search_vals = [('comptador', '=', lectura.comptador),
                           ('periode.name', '=', lectura.name[-3:-1]),
                           ('periode.tarifa', '=', tarifa_id[0]),
                           ('tipus', '=', tipus),
                           ('name', 'in', [lectura.data_actual,
                                           lectura.data_anterior])]
            lect_ids = lectura_obj.search(cursor, uid, search_vals)
            lect_vals = lectura_obj.read(cursor, uid, lect_ids,
                                         ['name', 'origen_comer_id', 'origen_id'])

            for lect in lect_vals:
                # En funció dels origens, escrivim el text
                # Si Estimada (40)
                # La resta: Real
                origen_txt = _(u"real")
                if lect['origen_id'][0] == estimada_id:
                    origen_txt = _(u"estimada")

                res[lect['name']] = origen_txt

        return res

    historic_sql = """
    SELECT * FROM (
        SELECT mes AS mes,
        periode AS periode,
        sum(suma_fact) AS facturat,
        sum(suma_consum) AS consum,
        min(data_ini) AS data_ini,
        max(data_fin) AS data_fin
        FROM (
            SELECT f.polissa_id AS polissa_id,
                   to_char(f.data_inici, 'YYYY/MM') AS mes,
                   pt.name AS periode,
                   COALESCE(SUM(il.quantity*(fl.tipus='energia')::int*(CASE WHEN i.type='out_refund' THEN -1 ELSE 1 END)),0.0) as suma_consum,
                   COALESCE(SUM(il.price_subtotal*(fl.tipus='energia')::int*(CASE WHEN i.type='out_refund' THEN -1 ELSE 1 END)),0.0) as suma_fact,
                   MIN(f.data_inici) data_ini,
                   MAX(f.data_final) data_fin
                   FROM
                        giscedata_facturacio_factura f
                        LEFT JOIN account_invoice i on f.invoice_id = i.id
                        LEFT JOIN giscedata_facturacio_factura_linia fl on f.id=fl.factura_id
                        LEFT JOIN account_invoice_line il on il.id=fl.invoice_line_id
                        LEFT JOIN product_product pp on il.product_id=pp.id
                        LEFT JOIN product_template pt on pp.product_tmpl_id=pt.id
                   WHERE
                        fl.tipus = 'energia' AND
                        f.polissa_id = %(p_id)s AND
                        f.data_inici <= %(data_inicial)s AND
                        f.data_inici >= date_trunc('month', date %(data_final)s) - interval '14 month'
                        AND (fl.isdiscount IS NULL OR NOT fl.isdiscount)
                        AND i.type in('out_invoice', 'out_refund')
                   GROUP BY
                        f.polissa_id, pt.name, f.data_inici
                   ORDER BY f.data_inici DESC ) AS consums
        GROUP BY polissa_id, periode, mes
        ORDER BY mes DESC, periode ASC
    ) consums_ordenats
    ORDER BY mes ASC"""

    # Repartiment segons BOE
    rep_BOE = {'i': 36.28, 'c': 32.12 ,'o': 31.60}

    banner_obj = pool.get('report.banner')

%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_albatera/report/extras/pie.css"/>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_albatera/report/extras/consum.css"/>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_albatera/report/factura.css"/>
        <script src="${addons_path}/giscedata_facturacio_comer/report/assets/d3.min.js"></script>
    </head>
    <body>
        %for factura in objects:
            <%
                reverse_banners = []
                if context.get('send_by_email', False):
                    for i in range(1, 6):
                        code = "albatera_revers_{}".format(i)
                        img = banner_obj.get_banner(
                            cursor, uid, 'giscedata.facturacio.factura',
                            factura.data_final.val, code=code, model_id=factura.id
                        )
                        reverse_banners.append(img)

                polissa = polissa_obj.browse(cursor, uid, factura.polissa_id.id,
                                             context={'date': factura.data_final.val})

                tarifa_elect_comer = polissa.llista_preu.id
                contracte_dacces = polissa.ref_dist or factura.polissa_id.ref_dist or ''

                dphone = polissa.distribuidora.address[0].phone
                cphone = factura.company_id.partner_id.address[0].phone
                distri_phone = ' '.join([dphone[i:i+3] for i in range(0,len(dphone),3)])
                comer_phone = ' '.join([cphone[i:i+3] for i in range(0,len(cphone),3)])

                cursor.execute(
                    historic_sql,
                    {
                        'p_id':factura.polissa_id.id,
                        'data_inicial': factura.data_inici,
                        'data_final': factura.data_final
                    }
                )
                historic = cursor.dictfetchall()

                historic_graf = {}
                periodes_graf = []
                total_historic_kw = 0
                total_historic_eur = 0
                historic_dies = 0
                total_any = 0
                historic_js = []
                if historic:
                    for row in historic:
                        historic_graf.setdefault(row['mes'],{})
                        historic_graf[row['mes']].update({row['periode']: row['consum']})
                        periodes_graf.append(row['periode'])

                    periodes_graf = list(set(periodes_graf))
                    for mes, consums in historic_graf.items():
                        p_js = {'mes': mes}
                        for p in periodes_graf:
                            p_js.update({p: consums.get(p,0.0)})
                        historic_js.append(p_js)

                    total_historic_kw = sum([h['consum'] for h in historic])
                    total_historic_eur = sum([h['facturat'] for h in historic])
                    data_ini = min([h['data_ini'] for h in historic])
                    data_fin = max([h['data_fin'] for h in historic])
                    historic_dies = 1 + (datetime.strptime(data_fin, '%Y-%m-%d') - datetime.strptime(data_ini, '%Y-%m-%d')).days

                    mes_any_inicial = (datetime.strptime(factura.data_inici,'%Y-%m-%d') - timedelta(days=365)).strftime("%Y/%m")
                    total_any = sum([h['consum'] for h in historic if h['mes'] > mes_any_inicial])

                iese_lines = [l for l in factura.tax_line if 'IVA' not in l.name]
                iva_lines = [l for l in factura.tax_line if 'IVA' in l.name]

                impostos = {}
                for l in factura.tax_line:
                    impostos.update({l.name: l.amount})

                iese = sum([v for k, v in impostos.items() if 'IVA' not in k])

                lloguer_lines = [l for l in factura.linia_ids if l.tipus in 'lloguer']
                altres_lines = [l for l in factura.linia_ids if l.tipus in 'altres'
                                and l.invoice_line_id.product_id.code != 'DN01']
                donatiu_lines = [l for l in factura.linia_ids if l.tipus in 'altres'
                                and l.invoice_line_id.product_id.code == 'DN01']

                donatiu = sum([l.price_subtotal for l in donatiu_lines])
                total_altres = sum([l.price_subtotal for l in altres_lines])
                periodes = dict([('P%s' % p,'') for p in range(1, 7)])
                periodes_a = sorted(list(set([lectura.name[-3:-1]
                                            for lectura in factura.lectures_energia_ids
                                            if lectura.tipus == 'activa'])))

                periodes_r = sorted(list(set([lectura.name[-3:-1]
                                            for lectura in factura.lectures_energia_ids
                                            if lectura.tipus == 'reactiva'])))

                periodes_m = sorted(list(set([lectura.name
                                              for lectura in factura.lectures_potencia_ids])))

                potencies_periode = dict([('P%s' % p,'') for p in range(1, 7)])
                for pot in polissa.potencies_periode:
                    potencies_periode[pot.periode_id.name] = pot.potencia

                fact_pot_txt = ((polissa.facturacio_potencia=='max' or len(periodes_a)>3)
                                    and _(u"facturació per maxímetre")
                                 or _(u"facturació per ICP"))

                # lectures (activa) ordenades per comptador i període
                lectures_a = {}
                lectures_r = {}
                lectures_m = []

                # Detall factures per excés potència

                exces_potencia = get_exces_potencia(cursor, uid, factura)

                for lectura in factura.lectures_energia_ids:
                    origens = get_origen_lectura(cursor, uid, lectura)
                    if lectura.tipus == 'activa':
                        lectures_a.setdefault(lectura.comptador,[])
                        lectures_a[lectura.comptador].append((lectura.name[-3:-1],
                                                            lectura.lect_anterior,
                                                            lectura.lect_actual,
                                                            lectura.consum,
                                                            lectura.data_anterior,
                                                            lectura.data_actual,
                                                            origens[lectura.data_anterior],
                                                            origens[lectura.data_actual],
                                                            ))
                    elif lectura.tipus == 'reactiva':
                        lectures_r.setdefault(lectura.comptador,[])
                        lectures_r[lectura.comptador].append((lectura.name[-3:-1],
                                                            lectura.lect_anterior,
                                                            lectura.lect_actual,
                                                            lectura.consum,
                                                            lectura.data_anterior,
                                                            lectura.data_actual,
                                                            origens[lectura.data_anterior],
                                                            origens[lectura.data_actual],
                                                            ))

                for lectura in factura.lectures_potencia_ids:
                    lectures_m.append((lectura.name, lectura.pot_contract,
                                       lectura.pot_maximetre ))

                for lects in lectures_a.values():
                    sorted(lectures_a, key=lambda x: x[0])

                for lects in lectures_r.values():
                    sorted(lectures_r, key=lambda x: x[0])

                for lects in lectures_m:
                    sorted(lectures_m, key=lambda x: x[0])

                total_lectures_a = dict([(p, 0) for p in periodes_a])
                total_lectures_r = dict([(p, 0) for p in periodes_r])
                fact_potencia = dict([(p,0) for p in periodes_m])

                dies_factura = 1 + (datetime.strptime(factura.data_final, '%Y-%m-%d') - datetime.strptime(factura.data_inici, '%Y-%m-%d')).days
                diari_factura_actual_eur = factura.total_energia / (dies_factura or 1.0)
                diari_factura_actual_kwh = (factura.energia_kwh * 1.0) / (dies_factura or 1.0)

                for c, ls in lectures_a.items():
                    for l in ls:
                        total_lectures_a[l[0]] += l[3]

                for c, ls in lectures_r.items():
                    for l in ls:
                        total_lectures_r[l[0]] += l[3]

                for p in [(p.product_id.name, p.quantity) for p in factura.linies_potencia]:
                    fact_potencia.update({p[0]: max(fact_potencia.get(p[0], 0), p[1])})

                #comprovem si té alguna lectura de maxímetre
                te_maximetre = (max([l[2] for l in lectures_m] or (0,)) > 0)


                # DADES PIE CHART
                pie_total = factura.amount_total - factura.total_lloguers
                pie_regulats = (factura.total_atr + total_altres)
                pie_impostos = float(factura.amount_tax)
                pie_costos = (pie_total - pie_regulats - pie_impostos )

                reparto = { 'i': ((pie_regulats * rep_BOE['i'])/100),
                            'c': ((pie_regulats * rep_BOE['c'])/100),
                            'o': ((pie_regulats * rep_BOE['o'])/100)
                           }

                dades_reparto = [
                    [[0, rep_BOE['i']], 'i', _(u"Incentius a les energies renovables, cogeneració i residus"), formatLang(reparto['i'])],
                     [[rep_BOE['i'] , rep_BOE['i'] + rep_BOE['c']], 'c', _(u"Cost de xarxes de distribució i transport"), formatLang(reparto['c'])] ,
                     [[rep_BOE['i'] + rep_BOE['c'], 100.00], 'o', _(u"Altres Costos regulats (inclosa anualitat del dèficit)"), formatLang(reparto['o'])]
                    ]
                company_address = factura.company_id.partner_id.address[0]

                setLang(factura.lang_partner or 'es_ES')
            %>
            <div id="lateral-info" class="center">
                ${company.rml_footer2 or ''}
            </div>
            <div id="page">
            <div id="background">
            %if duplicate:
                    <div id="background-text">${_(u"COPIA")}</div>
            %endif
            </div>

            <table id="header">
                <colgroup>
                    <col width="50%"/>
                    <col>
                </colgroup>
                <tbody>
                    <tr>
                        <td rowspan="5"><img src="data:image/jpeg;base64,${company.logo}"/></td>
                        <td>${factura.company_id.partner_id.name}</td>
                    </tr>
                    <tr>
                        <td>${clean_nif(factura.company_id.partner_id.vat)}</td>
                    </tr>
                    <tr>
                        <td>${company_address.street.replace('..', '.')}</td>
                    </tr>
                    <tr>
                        <td>${company_address.zip}, ${company_address.id_poblacio.name} (${company_address.state_id.name})</td>
                    </tr>
                    <tr>
                        <td>${company.partner_id.website}</td>
                    </tr>
                </tbody>
            </table>
            <!-- TITULAR -->
            <div class="left-50">
                <table id="titular" class="styled-table">
                    <colgroup>
                        <col width="35%"/>
                        <col/>
                    </colgroup>
                    <thead>
                        <tr>
                            <th colspan="2">${_(u"TITULAR")}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="bold">${_(u"Nom/Raó social")}</td>
                            <td>${polissa.titular.name}</td>
                        </tr>
                        <tr>
                            <td class="bold">${_(u"NIF/CIF")}</td>
                            <td>${clean_nif(polissa.titular.vat)}</td>
                        </tr>
                        %if factura.partner_id.ref:
                            <td class="bold">${_("Nº Soci")}</td>
                            <td>${factura.partner_id.ref}</td>
                        %endif
                        <tr>
                            <td class="bold">${_(u"Direcció")}</td>
                            <td colspan="3">${polissa.titular.address[0].street.replace('..', '.')} ${polissa.titular.address[0].zip} ${polissa.titular.address[0].id_municipi.name}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- END TITULAR -->
            <!-- FINESTRETA -->
            <div class="right-50 top-30">
                <table class="w100p postal">
                    <%
                        finestreta_dades = factura.address_contact_id
                        finestreta_name = '&nbsp;'
                        finestreta_street = '&nbsp;'
                        finestreta_zip = '&nbsp;'
                        finestreta_municipi = '&nbsp;'
                        finestreta_provincia = '&nbsp;'
                        if finestreta_dades:
                            finestreta_name = finestreta_dades.name
                            finestreta_street = finestreta_dades.street.replace('..', '.')
                            finestreta_zip = finestreta_dades.zip
                            finestreta_municipi = finestreta_dades.id_municipi.name
                            finestreta_provincia = finestreta_dades.state_id.name
                    %>
                    <tr>
                        <td>${finestreta_name}</td>
                    </tr>
                    <tr>
                        <td>${finestreta_street}</td>
                    </tr>
                    <tr>
                        <td>${finestreta_zip} ${finestreta_municipi}</td>
                    </tr>
                    <tr>
                        <td>${finestreta_provincia}</td>
                    </tr>
                </table>
            </div>
            <!-- END FINESTRETA -->
            <!-- FACTURA & RESUM FACTURA -->
            <div class="left-50">
                <!-- FACTURA -->
                <table id="factura" class="styled-table">
                    <colgroup>
                        <col width="30%"/>
                        <col width="32%"/>
                        <col width="20%"/>
                        <col/>
                    </colgroup>
                    <thead>
                        <tr>
                            <th colspan="4">${_(u"FACTURA")}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <% invoice_number = factura.number if factura.number else '' %>
                            <td class="bold">${_(u"Número de factura")}</td>
                            <td>${invoice_number}</td>
                            <td class="bold">${_(u"Data Emissió")}</td>
                            <td>${factura.date_invoice}</td>
                        </tr>
                        %if factura.tipo_rectificadora in ['B', 'A', 'BRA']:
                            <% invoice_ref = factura.ref.number if factura.ref.number else ''%>
                            <tr>
                                <td></td>
                                <td colspan="2">${_('Aquesta factura anul·la la {}').format(invoice_ref)}</td>
                            </tr>
                        %endif
                        %if factura.tipo_rectificadora in ['R', 'RA']:
                            <% invoice_ref = factura.ref.number if factura.ref.number else ''%>
                            <tr>
                                <td></td>
                                <td>${_('Aquesta factura rectifica la {}').format(invoice_ref)}</td>
                            </tr>
                        %endif
                        <tr>
                            <td colspan="2" class="bold">${_(u"Període de facturació")}</td>
                            <td colspan="2">${_("{} - {}").format(factura.data_inici, factura.data_final)}</td>
                        </tr>
                        <tr>
                            <td class="bold">${_(u"Vencimient")}</td>
                            <td>${factura.date_due}</td>
                            <td class="bold">${_(u"Dies")}</td>
                            <td>${dies_factura}</td>
                        </tr>
                        <tr>
                            <td class="bold">${_(u"Pagador")}</td>
                            <td colspan="3">${factura.partner_id.name}</td>
                        </tr>
                        <tr>
                            <td class="bold">${_(u"NIF")}</td>
                            <td>${clean_nif(factura.partner_id.vat)}</td>
                        </tr>
                        %if factura.partner_bank and factura.payment_mode_id.type.code == 'RECIBO_CSB':
                            <tr>
                                <td class="bold">${_(u"Entitat")}</td>
                                <td colspan="3">${factura.partner_bank.bank.name}</td>
                            </tr>
                            <tr>
                                <td class="bold">${_(u"IBAN")}</td>
                                <td colspan="3">**** **** **** **** **** ${factura.partner_bank.printable_iban[-4:]}</td>
                            </tr>
                        %else:
                            <tr>
                                <td class="bold">${_(u"Tipus de pagament")}</td>
                                <td colspan="3">${_(u"No domiciliat")}</td>
                            </tr>
                        %endif
                    </tbody>
                </table>
                <!-- END FACTURA -->
                <!-- RESUM FACTURA -->
                <table id="resum-factura" class="styled-table">
                    <thead>
                        <tr>
                            <th colspan="2">${_(u"RESUM FACTURA")}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="bold">${_(u"Per potència")}</td>
                            <td class="bold right">${"{}&euro;".format(formatLang(factura.total_potencia))}</td>
                        </tr>
                        <tr>
                            <td class="bold">${_(u"Per energia")}</td>
                            <td class="bold right">${"{}&euro;".format(formatLang(factura.total_energia))}</td>
                        </tr>
                        % if factura.total_reactiva > 0:
                            <tr>
                                <td class="bold">${_("Penalització per energia reactiva")}</td>
                                <td class="bold right">${"{}&euro;".format(formatLang(factura.total_reactiva))}</td>
                            </tr>
                        % endif
                            <tr class="row-separator">
                                <td class="bold">${_(u"Impost electricitat")}</td>
                                <td class="bold right">${"{}&euro;".format(formatLang(iese))}</td>
                            </tr>
                            <tr>
                                <td class="bold">${_(u"Lloguer comptador")}</td>
                                <td class="bold right">${"{}&euro;".format(formatLang(factura.total_lloguers))}</td>

                            </tr>
                        % if total_altres != 0:
                            <tr>
                                <td class="bold">${_("Altres conceptes")}</td>
                                <td class="bold right">${"{}&euro;".format(formatLang(total_altres))}</td>
                            </tr>
                        % endif
                        % for n, v in impostos.items():
                            % if "IVA" in n:
                                <tr>
                                    <td class="bold">${n}</td>
                                    <td class="bold right">${"{}&euro;".format(formatLang(v))}</td>
                                </tr>
                            % endif
                        % endfor
                        <tr class="row-separator">
                            <td class="bold">${_(u"TOTAL")}</td>
                            <td class="bold right">${"{}&euro;".format(formatLang(factura.amount_total))}</td>
                        </tr>
                    </tbody>
                </table>
                <!-- END RESUM FACTURA -->
            </div>
            <!-- END FACTURA & RESUM FACTURA -->
            <!-- CONTRACTE -->
            <div class="right-50">
                <table id="contracte" class="styled-table">
                    <thead>
                        <tr>
                            <th colspan="4">${_(u"CONTRACTE")}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="bold">${_(u"Contracte del suministrament")}</td>
                            <td>${factura.polissa_id.name}</td>
                            <td class="bold">${_(u"Contracte d'accés")}</td>
                            <td>${contracte_dacces}</td>
                        </tr>
                        <tr>
                            <td class="bold">${_(u"Data final")}</td>
                            <td>${formatLang(polissa.modcontractual_activa.data_final, date=True)}</td>
                            <td colspan="2" class="little">${_("(renovació anual automàtica)")}</td>
                        </tr>
                        <tr>
                            <td class="bold">${_(u"Tarifa")}</td>
                            <td>${factura.tarifa_acces_id.name}</td>
                            <td class="bold">${_(u"CNAE")}</td>
                            <td>${polissa.cnae.name}</td>
                        </tr>
                        <tr>
                            <td class="bold">${_(u"CUPS")}</td>
                            <td colspan="3">${factura.cups_id.name}</td>
                        </tr>
                        <tr>
                            <td class="bold">${_(u"Distribuïdora")}</td>
                            <td colspan="3">${polissa.distribuidora.name}</td>
                        </tr>
                        <tr>
                            <td class="bold">${_(u"Adreça de subministrament")}</td>
                            <td colspan="3">${factura.cups_id.direccio}</td>
                        </tr>

                    </tbody>
                </table>
                <table class="styled-inner-table">
                    <%
                        potencies_contractades = []
                        potencia_contractada_titol = _(u"Potència contractada: {} (kW)").format(_(u"Maxímetre") if te_maximetre else _(u"ICP"))

                        for periode, potencia in sorted(potencies_periode.items(),key=itemgetter(0)):
                            if potencia:
                                potencies_contractades.append((periode, potencia))
                    %>
                    <thead>
                        <tr>
                           <th class="center" colspan="${len(potencies_contractades)}">${potencia_contractada_titol}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            % for periode, potencia in potencies_contractades:
                                <td class="bold center">${periode}</td>
                            % endfor
                        </tr>
                        <tr>
                            % for periode, potencia in potencies_contractades:
                                 <td class="center">${formatLang(potencia, digits=3)}</td>
                            % endfor
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- END CONTRACTE -->
            <!-- LECTURES -->
            <table id="consum-electric" class="styled-table">
                <thead>
                    <tr>
                        <th></th>
                        <th>${_(u"CONSUM ELÈCTRIC")}</th>
                        % for period_name in periodes_a:
                            <th>${period_name}</th>
                        % endfor
                    </tr>
                </thead>
                <tbody>
                    <!-- ACTIVA -->
                    %if lectures_a:
                        <tr>
                            <th rowspan=${3*len(lectures_a)+2} class="rotate h65px"><div>${_("ENERGIA")}</div></th>
                        </tr>
                        % for comptador in sorted(lectures_a):
                            <tr class="row-separator">
                                <td class="bold">${_(u"Número de comptador")}</td>
                                % for period_name in periodes_a:
                                    <td class="center">${comptador}</td>
                                % endfor
                            </tr>
                            <tr>
                                <td class="bold">${_(u"Lectura anterior")} ${lectures_a[comptador][0][4]} (${lectures_a[comptador][0][6]})</td>
                                % for i in range(len(periodes_a)):
                                    <td class="right">${formatLang(int(lectures_a[comptador][i][1]),digits=0)} kWh</td>
                                % endfor
                            </tr>
                            <tr>
                                <td class="bold">${_(u"Lectura actual")} ${lectures_a[comptador][0][5]} (${lectures_a[comptador][0][7]})</td>
                                % for i in range(len(periodes_a)):
                                    <td class="right">${formatLang(int(lectures_a[comptador][i][2]),digits=0)} kWh</td>
                                % endfor
                            </tr>
                        % endfor
                        <tr>
                            <td class="bold">${_(u"Total període")}</td>
                            % for period_name in periodes_a:
                                <td class="right">${formatLang(total_lectures_a[period_name], digits=0)} kWh</td>
                            %endfor
                        </tr>
                    %endif
                    <!-- ACTIVA -->
                    <!-- REACTIVA -->
                    <%
                        render_reactive = len(periodes_r)
                        if render_reactive:
                            for period_name in periodes_r:
                                if total_lectures_r[period_name]:
                                    break
                                if period_name == periodes_r[-1]:
                                    render_reactive = False
                    %>
                    % if render_reactive:
                        <tr>
                            <th rowspan=${3*len(lectures_r)+2} class="rotate h65px"><div>${_("REACTIVA")}</div></th>
                        </tr>
                        % for comptador in sorted(lectures_r):
                            <tr class="row-separator">
                                <td class="bold">${_(u"Número de comptador")}</td>
                                % for period_name in periodes_r:
                                        <td class="center">${comptador}</td>
                                % endfor
                            </tr>
                            <tr>
                                <td class="bold">${_(u"Lectura anterior")} ${lectures_r[comptador][0][4]} (${lectures_r[comptador][0][6]})</td>
                                % for i in range(len(periodes_r)):
                                    <td class="right">${formatLang(int(lectures_r[comptador][i][1]),digits=0)} kWh</td>
                                % endfor
                            </tr>
                            <tr>
                                <td class="bold">${_(u"Lectura actual")} ${lectures_r[comptador][0][5]} (${lectures_r[comptador][0][7]})</td>
                                % for i in range(len(periodes_r)):
                                    <td class="right">${formatLang(int(lectures_r[comptador][i][2]),digits=0)} kWh</td>
                                % endfor
                            </tr>
                        % endfor
                        <tr>
                            <td class="bold">${_(u"Total període")}</td>
                            % for period_name in periodes_r:
                                <td class="right">${formatLang(total_lectures_r[period_name], digits=0)} kWh</td>
                            % endfor
                        </tr>
                    %endif
                    <!-- REACTIVA -->
                    <!-- MAXIMETRE -->
                    % if te_maximetre:
                        <tr>
                            <th rowspan="4" class="rotate h65px"> <div>${_("MAXÍMETRE")}</div></th>
                        </tr>
                        <tr class="row-separator">
                            <td class="bold">${_(u"Potència contractada")}</td>
                            <%
                                ##                              TODO chapuza, a la espera de las funciones de #7902
                                lectures_max = {}
                                periodes_max = []
                                for lec in lectures_m:
                                    lectures_max[lec[0]] = 0
                                    periodes_max.append(lec[0])

                                periodes_max = sorted(set(periodes_max))

                                for lec in lectures_m:
                                    lectures_max[lec[0]] = lec[2] if lec[2] > lectures_max[lec[0]] else lectures_max[lec[0]]

                                contractada_max = {}
                                for lec in lectures_m:
                                    contractada_max[lec[0]] = lec[1]
                            %>
                            % for p in periodes_max:
                                <td class="right">${formatLang(contractada_max[p], digits=3)} kW</td>
                            % endfor
                        </tr>
                        <tr>
                            <td class="bold">${_(u"Lectura maxímetre")}</td>
                                % for p in periodes_max:
                                <td class="right">${formatLang(lectures_max[p], digits=3)} kW</td>
                            % endfor
                        </tr>
                        <tr>
                            <td class="bold">${_(u"Potència facturada període")}</td>
                            % for period_name in periodes_m:
                                <td class="right">${formatLang(fact_potencia[period_name],digits=3)} kW</td>
                            % endfor
                        </tr>
                    %endif
                    <!-- MAXIMETRE -->
                </tbody>
                <tfoot>
                    <tr>
                        <% colspan =  len(periodes_a) + 2 %>
                        <td colspan="${colspan}" class="center">${_(u"El seu consum mitjà diari en el període facturat ha sigut de {} € que corresponent a {} kWh/dia ({} dies)").format(formatLang(diari_factura_actual_eur), formatLang(diari_factura_actual_kwh), dies_factura or 1)}</td>
                    </tr>
                </tfoot>
            </table>
            <!-- END LECTURES -->
            <!-- HISTORIC -->
            <div class="left-50-clear">
                <%
                    mesos = int(historic_dies*1.0 / 30)
                    dies = historic_dies or 1.0
                    consum_preu = formatLang((total_historic_eur * 1.0) / (historic_dies or 1.0))
                    kwh_dia = formatLang((total_historic_kw * 1.0) / (historic_dies or 1.0))
                %>
                <table class="styled-table">
                    <thead>
                        <tr>
                            <th>${_(u"HISTORIAL ÚLTIMS 14 MESOS")}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="background-color">
                                <div class="chart_consum" id="chart_consum_${factura.id}"></div>
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td>
                                ${(_(u"El seu consum mitjà diari els últims {} mesos ({} dies) ha estat de {} € que corresponen a {} kWh/dia ").format(mesos, dies, consum_preu, kwh_dia))}<br/>
                                ${_(u"El seu consum acumulat el darrer any ha sigut de {} kWh").format(formatLang(total_any, digits=0))}
                            </td>
                        </tr>
                    </tfoot>
                </table>
                <% last_div_classes = 'emergency-complaints' %>
                %if '2.' in factura.tarifa_acces_id.name:
                    <!-- DESTI -->
                    <%
                        if len(lectures_a) > 1 and render_reactive:
                            last_div_classes += ' last-tables-next-to-detall-factura'
                        else:
                            last_div_classes += ' last-tables-on-bottom'
                    %>
                    <table id="desti-factura" class="styled-table">
                        <thead>
                            <tr>
                                <th class="seccio">${_(u"DESTÍ DE LA FACTURA")}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>${_(u"El destí de l'import de la seva factura ({} euros) és el següent:").format(formatLang(factura.amount_total))}</td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="chart_desti" id="chart_desti_${factura.id}"></div>
                                </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td class="italic">${_(u"Als imports indicats en el diagrama se'ls ha d'afegir, en el seu cas, el lloguer dels equips de mesura i control")}</td>
                            </tr>
                        </tfoot>
                    </table>
                    <!-- END DESTI -->
                %else:
                    <% last_div_classes += ' last-tables-next-to-detall-factura' %>
                %endif
            </div>
            <!-- END HISTORIC -->
            <!-- DETALL FACTURA -->
            <div class="right-50">
                <table id="detall-factura" class="styled-table center-th">
                    <caption>${_(u"DETALL FACTURA")}</caption>
                    <colgroup>
                        <col width="32%" class="background-color"/>
                        <col width="20%"/>
                        <col/>
                        <col width="24%"/>
                    </colgroup>
                    <tbody>
                       <tr>
                            <td colspan="4">
                                <!-- POTENCIA -->
                                <table class="styled-inner-table round-top-borders">
                                    <colgroup>
                                        <col width="22%"/>
                                        <col width="22%"/>
                                        <col width="19%"/>
                                        <col/>
                                        <col/>
                                    </colgroup>
                                    <thead>
                                        <%
                                            fact_messos = 'mes' in polissa.property_unitat_potencia.name
                                            unitat_temps = _(u"mes") if fact_messos else _(u"dia")
                                            preu_unitat = "(€/kW {})".format(unitat_temps)
                                        %>
                                        <tr>
                                            <th>${_(u"Terme de potència")}</th>
                                            <th>${_(u"Potencia")} (kW)</th>
                                            <th>${_(u"Preu")} ${preu_unitat} </th>
                                            % if fact_messos:
                                                <th>${_(u"Mesos")}</th>
                                            %else:
                                                <th>${_(u"Dies")}</th>
                                            %endif
                                            <th>${_(u"Total")} (€)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        % for l in sorted(sorted(factura.linies_potencia, key=attrgetter('data_desde')), key=attrgetter('name')):
                                            <tr>
                                                <td class="bold center">${l.name}</td>
                                                <td class="right">${formatLang(l.quantity, digits=3)}</td>
                                                <td class="right">
                                                    %if fact_messos:
                                                        ${formatLang(l.price_unit_multi, digits=6)}
                                                    %else:
                                                        ${formatLang(l.price_unit_multi, digits=6)}
                                                    %endif
                                                </td>
                                                <td class="center">${int(l.multi)}</td>
                                                <td class="right">${formatLang(l.price_subtotal)}</td>
                                            </tr>
                                        % endfor
                                        <tr>
                                            <td colspan="5" class="bold right">${formatLang(factura.total_potencia)}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <!-- ENERGIA -->
                                <table class="styled-inner-table">
                                    <colgroup>
                                        <col class="detall-factura-inner-table-col-1"/>
                                        <col class="detall-factura-inner-table-col-2"/>
                                        <col class="detall-factura-inner-table-col-3"/>
                                        <col class="detall-factura-inner-table-col-4"/>
                                    </colgroup>
                                    <thead>
                                        <tr>
                                            <th>${_(u"Terme d'energia")}</th>
                                            <th>${_(u"Consum")} (kWh)</th>
                                            <th>${_(u"Preu")} (€/kWh)</th>
                                            <th>${_(u"Total")} (€)</th>
                                         </tr>
                                    </thead>
                                    <tbody>
                                            % for l in sorted(sorted(factura.linies_energia, key=attrgetter('data_desde')), key=attrgetter('name')):
                                                <tr>
                                                    <td class="bold center">${l.name}</td>
                                                    %if 'bonificación' in l.name.lower():
                                                        <td></td>
                                                        <td></td>
                                                    %else:
                                                        <td class="right">${int(l.quantity)}</td>
                                                        <td class="right">${formatLang(l.price_unit_multi, digits=6)}</td>
                                                    %endif
                                                    <td class="right">${formatLang(l.price_subtotal)}</td>
                                                </tr>
                                            % endfor
                                        <tr>
                                            <td colspan="4" class="bold right">${formatLang(factura.total_energia)}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        %if factura.total_reactiva:
                            <tr>
                                <td colspan="4">
                                    <!-- ENERGIA REACTIVA  -->
                                    <table class="styled-inner-table">
                                        <colgroup>
                                            <col class="detall-factura-inner-table-col-1"/>
                                            <col class="detall-factura-inner-table-col-2"/>
                                            <col class="detall-factura-inner-table-col-3"/>
                                            <col class="detall-factura-inner-table-col-4"/>
                                        </colgroup>
                                        <thead>
                                            <tr>
                                                <th>${_(u"Energia reactiva")}</th>
                                                <th>${_(u"Excés")} (kVArh)</th>
                                                <th>${_(u"Preu")} (€/kVArh)</th>
                                                <th>${_(u"Total")} (€)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            % for l in sorted(sorted(factura.linies_reactiva, key=attrgetter('data_desde')), key=attrgetter('name')):
                                                <tr>
                                                    <td class="bold center">${l.name}</td>
                                                    <td class="right">${formatLang(l.quantity)}</td>
                                                    <td class="right">${formatLang(l.price_unit_multi, digits=6)}</td>
                                                    <td class="right">${formatLang(l.price_subtotal)}</td>
                                                </tr>
                                            % endfor
                                            <tr>
                                                <td colspan="4" class="bold right">${formatLang(factura.total_reactiva)}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        %endif
                        %if len(exces_potencia):
                            <tr>
                                <td colspan="4">
                                    <!-- EXCES POTENCIA -->
                                    <table class="styled-inner-table">
                                        <colgroup>
                                            <col class="detall-factura-inner-table-col-1"/>
                                            <col class="detall-factura-inner-table-col-2"/>
                                            <col class="detall-factura-inner-table-col-3"/>
                                            <col class="detall-factura-inner-table-col-4"/>
                                        </colgroup>
                                        <thead>
                                            <tr>
                                                <th>${_(u"Excés potència")}</th>
                                                <th>${_(u"Excés")} (kVArh)</th>
                                                <th>${_(u"Preu")} (€/kVArh)</th>
                                                <th>${_(u"Total")} (€)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <% total_exces = 0.0 %>
                                            % for l in sorted(sorted(exces_potencia, key=attrgetter('data_desde')), key=attrgetter('name')):
                                                <tr>
                                                    <td class="bold center">${l.name}</td>
                                                    <td class="right">${formatLang(l.quantity)}</td>
                                                    <td class="right">${formatLang(l.price_unit_multi, digits=6)}</td>
                                                    <td class="right">${_(u"{} €").format(formatLang(l.price_subtotal))}</td>
                                                </tr>
                                                <% total_exces += l.price_subtotal %>
                                            % endfor
                                            <tr>
                                                <td colspan="4" class="bold right">${formatLang(total_exces)}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        %endif
                        % for l in iese_lines:
                            <tr class="row-separator">
                                <th>${_(u"Impost de l'electricitat")}</th>
                                <td class="right">${_(u"{} €").format(formatLang(l.base_amount))}</td>
                                <td class="right">${u"5,11269632%"}</td>
                                <td class="bold right">${_(u"{} €").format(formatLang(l.tax_amount))}</td>
                            </tr>
                        % endfor
                        % for l in lloguer_lines:
                            <tr class="row-separator">
                                <th>${_(u"Lloguer de comptador")}</th>
                                <td></td>
                                <td></td>
                                <td class="bold right">${_(u"{} €").format(formatLang(l.price_subtotal))}</td>
                            </tr>
                        % endfor
                        %if altres_lines:
                            <tr class="row-separator">
                                <% rowspan = len(altres_lines) + 1 %>
                                <th rowspan="${rowspan}">${_(u"Altres")}</th>
                                <td colspan="3"></td>
                            </tr>
                            % for l in altres_lines:
                                <tr>
                                    <td colspan="2" class="bold">${l.name}</td>
                                    <td class="bold right">${_(u"{} €").format(formatLang(l.price_subtotal))}</td>
                                </tr>
                            % endfor
                        %endif
                        % for l in iva_lines:

                            <tr class="row-separator">
                                <th>${l.name}</th>
                                <td class="right">${_(u"{} €").format(formatLang(l.base))}</td>
                                <td class="right">${l.name.replace('IVA','').strip()}</td>
                                <td class="bold right">${_(u"{} €").format(formatLang(l.amount))}</td>
                            </tr>
                        % endfor
                        <tr class="row-separator">
                            <th>${_(u"TOTAL FACTURA")}</th>
                            <td></td>
                            <td></td>
                            <td class="bold right">${_(u"{} &euro;").format(formatLang(factura.amount_total))}</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="4" class="bold cursiva">${_(u"Els preus dels termes de peatge d’accés són els publicats a ORDEN ETU/1976/2016")}</td>
                        </tr>
                        <tr>
                            <td colspan="4" class="bold cursiva">${_(u"Els preus del lloguer dels comptadors són els establerts a ORDEN IET/1491/2013")}</td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <!-- END DETALL FACTURA -->
            <!-- AVARIES & URGENCIES, & ATENCIO AL CLIENT -->
            <div class="${last_div_classes}">
                <!-- AVARIES & URGENCIES -->
                <table class="styled-table-with-caption">
                    <caption>${_(u"AVARIES I URGÈNCIES")}</caption>
                    <colgroup>
                        <col width="40%"/>
                    </colgroup>
                    <tbody>
                        <tr>
                            <th>${_(u"Empresa distribuïdora")}</th>
                            <%
                                fit_distri_name = ""
                                if len(polissa.distribuidora.name) > 40:
                                    fit_distri_name = 'class="fit-distri-name"'
                            %>
                            <td ${fit_distri_name}>${polissa.distribuidora.name}</td>
                        </tr>
                        <tr>
                            <th>${_(u"Telèfon avaries")}</th>
                            <td>${distri_phone}</td>
                        </tr>
                        <tr>
                            <th>${_(u"Contracte d'accés")}</th>
                            <td>${contracte_dacces}</td>
                        </tr>
                    </tbody>
                </table>
                <!-- END AVARIES & URGENCIES -->
                <!-- ATENCIO AL CLIENT -->
                <table class="styled-table-with-caption ">
                    <caption>${_(u"ATENCIÓ AL CLIENT")}</caption>
                    <colgroup>
                        <col width="40%"/>
                    </colgroup>
                    <tbody>
                        <tr>
                            <th>${_(u"Empresa comercialitzadora")}</th>
                            <%
                                fit_comer_name = ""
                                if len(factura.company_id.partner_id.name) > 40:
                                    fit_comer_name = 'class="fit-comer-name"'
                            %>
                            <td ${fit_comer_name}>${factura.company_id.partner_id.name}</td>
                        </tr>
                        <tr>
                            <th>${_(u"Telèfon atenció")}</th>
                            <td>${comer_phone}</td>
                        </tr>
                        <tr>
                            <th>${_(u"Email")}</th>
                            <td>${factura.company_id.partner_id.address[0].email}</td>
                        </tr>
                    </tbody>
                </table>
                <!-- END ATENCIO AL CLIENT -->
            </div>
            <!-- END AVARIES & URGENCIES, & ATENCIO AL CLIENT -->
            <div class="small-styled">
                ${_("El pagament de la factura es justificarà amb el rebut o càrrec en el nostre compte bancari.")}
                ${_(" El seu pagament no presuposa la liquidacio de les anteriors.")}
            </div>
            <!-- END PAGE -->
            <script>
                var pie_total = ${pie_total};
                var pie_data = [{val: ${pie_regulats}, perc: 30, code: "REG"},
                                {val: ${pie_costos}, perc: 55, code: "PROD"},
                                {val: ${pie_impostos},  perc: 15 ,code: "IMP"}
                               ];

                var pie_etiquetes = {'REG': {t: ['${formatLang(float(pie_regulats))}€','${_(u"Costos regulats")}'], w:100},
                                     'IMP': {t: ['${formatLang(float(pie_impostos))}€','${_(u"Impostos aplicats")}'], w:100},
                                     'PROD': {t: ['${formatLang(float(pie_costos))}€','${_(u"Costos de producció electricitat")}'], w:150}
                                    };

                var reparto = ${json.dumps(reparto)};
                var dades_reparto = ${json.dumps(dades_reparto)};

                var factura_id = ${factura.id};
                var data_consum = ${json.dumps(sorted(historic_js, key=lambda h: h['mes']))};
                var es30 = ${len(periodes_a)>9 and 'true' or 'false'};
                 ##  var es30 = ${len(periodes_a)>3 and 'true' or 'false'}
            </script>
            <script src="${addons_path}/giscedata_facturacio_comer_albatera/report/extras/consum.js"></script>
            <script src="${addons_path}/giscedata_facturacio_comer_albatera/report/extras/pie.js"></script>
            </div>

            %if any(reverse_banners):
                <p style="page-break-after:always; clear:both"></p>
                %for reverse_banner in reverse_banners:
                    <div class="banner reverse-banners">
                        <img src="data:image/jpeg;base64,${reverse_banner}">
                    </div>
                %endfor
            %endif

            % if factura != objects[-1]:
                <p style="page-break-after:always; clear:both"></p>
            % endif
        %endfor
    </body>
</html>
