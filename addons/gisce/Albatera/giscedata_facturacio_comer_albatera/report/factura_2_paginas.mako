## -*- coding: utf-8 -*-
<%
    from operator import attrgetter, itemgetter
    from datetime import datetime, timedelta
    from giscedata_facturacio_comer.report.utils import clean_nif
    import json, re

    pool = objects[0].pool
    report_o = pool.get('giscedata.facturacio.factura.report')
    banner_obj = pool.get('report.banner')

%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer/report/factura.css"/>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer/report/assets/pie-chart.css"/>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer/report/assets/consumption-chart.css"/>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_albatera/report/factura_2_paginas.css"/>
        <script src="${assets_path}/js/d3.min.js"></script>
        <style>
            @font-face {
                font-family: "Roboto-Regular";
                src: url("${assets_path}/fonts/Roboto/Roboto-Regular.ttf") format('truetype');
                font-weight: normal;
            }
        </style>
    </head>
    <%
        # Company information
        comer = report_o.get_company_info(cursor, uid, company.id)[company.id]
        lateral_info =  comer['lateral_info'] or ''
    %>
    <body>
        %for invoice in objects:
            <%
                holder = report_o.get_holder_info(cursor, uid, invoice.id)[invoice.id]
                contact = report_o.get_contact_info(cursor, uid, invoice.id)[invoice.id]
## _____________________________________________________________________________
## -----------------------------------------------------------------------------
                # Taxes lines
                invoice_taxes = report_o.get_taxes(cursor, uid, invoice.id)[invoice.id]
                taxes = invoice_taxes['taxes']
                iese =  invoice_taxes['iese']
                iva =  invoice_taxes['iva']
## _____________________________________________________________________________
                # TODO Those 2 lines could be encapsulated to avoid browsing the
                # models.
                tariff = invoice.tarifa_acces_id.name
                cups = invoice.cups_id

                # Invoice related information
                total = {
                    'energy': invoice.total_energia,
                    'power': invoice.total_potencia,
                    'reactive': invoice.total_reactiva,
                    'electricity_tax': iese['total'],
                    'rent': invoice.total_lloguers,
                    'invoice': invoice.amount_total,
                }
                invoice_init_date = datetime.strptime(invoice.data_inici, '%Y-%m-%d')
                invoice_end_date = datetime.strptime(invoice.data_final, '%Y-%m-%d')
                dies_factura = 1 + (invoice_end_date - invoice_init_date).days
                diari_factura_actual_eur = total['energy'] / (dies_factura or 1.0)
                diari_factura_actual_kwh = (invoice.energia_kwh * 1.0) / (dies_factura or 1.0)
## -----------------------------------------------------------------------------
                # Other lines (meters rent, "suplemento", ...)  and power excess lines
                power_excess_and_other_detailed_info = report_o.get_power_exces_and_other_detailed_info(cursor, uid, invoice.id)[invoice.id]
                meters_rent = power_excess_and_other_detailed_info['meters_rent']
                other_lines = power_excess_and_other_detailed_info['other_lines']
                power_lines_with_excess = power_excess_and_other_detailed_info['lines_with_power_excess']
## _____________________________________________________________________________
## -----------------------------------------------------------------------------
                # Policy information
                policy = report_o.get_policy_info(cursor, uid, invoice.id)[invoice.id]
## _____________________________________________________________________________
## -----------------------------------------------------------------------------
                # Supplier ("distribuidora") information
                supplier = report_o.get_supplier_info(cursor, uid, invoice.id)[invoice.id]
                access_contract = supplier['ref'] or ''
## _____________________________________________________________________________
## -----------------------------------------------------------------------------
                # Invoice detail related information
                detailed_lines = report_o.get_lines_detailed_info(cursor, uid, invoice.id)[invoice.id]
                energy_lines = detailed_lines['energy']
                power_lines = detailed_lines['power']
                reactive_lines = detailed_lines['reactive']
## _____________________________________________________________________________
## -----------------------------------------------------------------------------
                # Pie chart data
                pie = report_o.get_pie_data(cursor, uid, invoice.id, {invoice.id: other_lines['total']})[invoice.id]
                pie_total = pie['total']
                pie_regulats = pie['regulats']
                pie_impostos = pie['impostos']
                pie_costos = pie['costos']
                reparto = pie['reparto']
                dades_reparto = pie['dades_reparto']
                for repartiment in dades_reparto:
                    repartiment[3] = formatLang(repartiment[3])
## _____________________________________________________________________________
## -----------------------------------------------------------------------------
                # 14-month historic data
                historic = report_o.get_historics(cursor, uid, invoice.id)[invoice.id]
                historical = historic['historic']
                historic_js = historic['historic_js']
## _____________________________________________________________________________
## -----------------------------------------------------------------------------
                # Readings (electric consumption)
                periods_and_readings = report_o.get_periods_and_readings(cursor, uid, invoice.id)[invoice.id]

                periods_a = periods_and_readings['periods']['active']
                periods_r = periods_and_readings['periods']['reactive']
                periods_m = periods_and_readings['periods']['maximeter']

                readings_a = periods_and_readings['readings']['active']
                readings_r = periods_and_readings['readings']['reactive']
                readings_m = periods_and_readings['readings']['maximeter']

                # Invoiced power
                fact_potencia = report_o.get_invoiced_power(cursor, uid, invoice.id, {invoice.id: periods_m})[invoice.id]
## _____________________________________________________________________________
            %>
            <div id="lateral-info" class="center">${lateral_info}</div>
            <div id="first-page">
                <!-- HEADER -->
                <table id="header">
                    <colgroup>
                        <col width="50%"/>
                        <col>
                    </colgroup>
                    <tbody>
                        <tr>
                            <td rowspan="5"><img src="data:image/jpeg;base64,${comer['logo']}"/></td>
                            <td>${comer['name']}</td>
                        </tr>
                        <tr>
                            <td>${clean_nif(comer['vat'])}</td>
                        </tr>
                        <tr>
                            <td>${comer['street'].replace('..', '.')}</td>
                        </tr>
                        <tr>
                            <td>${comer['zip']}, ${comer['poblacio']} (${comer['state']})</td>
                        </tr>
                        <tr>
                            <td>${company.partner_id.website}</td>
                        </tr>
                    </tbody>
                </table>
                <!-- END HEADER -->
                <!-- BANNER + (TITULAR + RESUM FACTURA)-->
                <!-- BANNER + (TITULAR + RESUM FACTURA)-->
                <table id="banner-holder-summary">
                    <colgroup>
                        <col width="50%"/>
                        <col width="50%"/>
                    </colgroup>
                    <tbody>
                        <tr>
                            <td>
                                <!-- RESUM FACTURA -->
                                <table id="resum-factura" class="styled-table w70p">
                                    <thead>
                                        <tr>
                                            <th colspan="2">${_(u"RESUM FACTURA")}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="bold">${_(u"Per energia")}</td>
                                            <td class="right">${total['energy']}</td>
                                        </tr>
                                        <tr>
                                            <td class="bold">${_(u"Per potència")}</td>
                                            <td class="right">${total['power']}</td>
                                        </tr>
                                        % if total['reactive'] > 0:
                                            <tr>
                                                <td class="bold">${_("Penalització per energia reactiva")}</td>
                                                <td class="right">${total['reactive']}</td>
                                            </tr>
                                        % endif
                                        <tr class="row-separator">
                                            <td class="bold">${_(u"Impost electricitat")}</td>
                                            <td class="right">${formatLang(total['electricity_tax'])}</td>
                                        </tr>
                                        <tr>
                                            <td class="bold">${_(u"Lloguer comptador")}</td>
                                            <td class="right">${total['rent']}</td>

                                        </tr>
                                        % if total_altres != 0:
                                            <tr>
                                                <td class="bold">${_("Altres conceptes")}</td>
                                                <td class="right">${formatLang(other_lines['total'])}</td>
                                            </tr>
                                        % endif
                                        % for tax, tax_amount in taxes.items():
                                            % if "IVA" in tax:
                                                <tr>
                                                    <td class="bold">${tax}</td>
                                                    <td class="right">${formatLang(tax_amount)}</td>
                                                </tr>
                                            % endif
                                        % endfor
                                        <tr class="row-separator">
                                            <td class="bold">${_(u"TOTAL")}</td>
                                            <td class="bold right">${"{} €".format(formatLang(total['invoice']))}</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <!-- END RESUM FACTURA -->
                            </td>
                            <td valign="top">
                                <table class="w100p">
                                    <tr>
                                        <td>${contact['name']}</td>
                                    </tr>
                                    <tr>
                                        <td>${contact['street'].replace('..', '.')}</td>
                                    </tr>
                                    <tr>
                                        <td>${contact['zip']} ${contact['city']}</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <!-- CONTRACTE -->
                <table class="styled-table">
                    <thead>
                        <tr>
                            <th colspan="4">${_(u"CONTRACTE")}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="bold">${_(u"Titular")}</td>
                            <td>${holder['name']}</td>
                            <td class="bold">${_(u"NIF")}</td>
                            <td>${clean_nif(holder['vat'])}</td>
                        </tr>
                        <tr>
                            <td class="bold">${_(u"Contracte del suministrament")}</td>
                            <td>${policy['name']}</td>
                            <td class="bold">${_(u"Contracte d'accés")}</td>
                            <td>${access_contract}</td>
                        </tr>
                        <tr>
                            <td rowspan="2" class="bold">${_(u"Data alta")}</td>
                            <td rowspan="2">${formatLang(policy['start_date'], date=True)}</td>
                            <td class="bold">${_(u"Data final")}</td>
                            <td>${formatLang(policy['end_date'], date=True)}</td>
                        </tr>
                        <tr>
                            <td colspan="2" class="little">${_("(renovació anual automàtica)")}</td>
                        </tr>
                        <tr>
                            <td class="bold">${_(u"Tarifa")}</td>
                            <td>${tariff}</td>
                            <td class="bold">${_(u"CNAE")}</td>
                            <td>${policy['cnae']}</td>
                        </tr>
                        <tr>
                            <td class="bold">${_(u"Producte Comer")}</td>
                            <td colspan="3">${policy['pricelist']}</td>
                        </tr>
                        <tr>
                            <td class="bold">${_(u"CUPS")}</td>
                            <td colspan="3">${cups.name}</td>
                        </tr>
                        <tr>
                            <td class="bold">${_(u"Distribuïdora")}</td>
                            <td colspan="3">${supplier['name']}</td>
                        </tr>
                        <tr>
                            <td class="bold">${_(u"Adreça de subministrament")}</td>
                            <td colspan="3">${cups.direccio}</td>
                        </tr>
                    </tbody>
                </table>
                <!-- END CONTRACTE -->
                <!-- HISTORIC -->
                <table class="styled-table">
                    <thead>
                        <tr>
                            <th>${_(u"HISTORIAL ÚLTIMS 14 MESOS")}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="background-color">
                                <div class="chart_consum" id="chart_consum_${invoice.id}"></div>
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td class="italic">
                                ${_(u"El seu consum mitjà diari els últims {} mesos ({} dies) ha estat de {} € que corresponen a {} kWh/dia ").format(historical['months'], historical['days'], formatLang(historical['average_cost']), formatLang(historical['average_consumption']))}<br/>
                                ${_(u"El seu consum acumulat el darrer any ha sigut de {} kWh").format(formatLang(historical['year_consumption'], digits=0))}
                            </td>
                        </tr>
                    </tfoot>
                </table>
                <!-- END HISTORIC -->
                <!-- AVARIES-URGENCIES-ATENCIO CLIENT -->
                <table id="avaries-urgencies-atencio-client">
                    <colgroup>
                        <col width="50%"/>
                        <col width="50%"/>
                    </colgroup>
                    <tbody>
                        <tr>
                            <td valign="top">
                                <table class="styled-table-with-caption">
                                    <caption>${_(u"AVARIES I URGÈNCIES")}</caption>
                                    <colgroup>
                                        <col width="40%"/>
                                    </colgroup>
                                    <tbody>
                                        <tr>
                                            <th>${_(u"Empresa distribuïdora")}</th>
                                            <td>${supplier['name']}</td>
                                        </tr>
                                        <tr>
                                            <th>${_(u"Telèfon gratuït")}</th>
                                            <td>${supplier['phone']}</td>
                                        </tr>
                                        <tr>
                                            <th>${_(u"Contracte d'accés")}</th>
                                            <td>${access_contract}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td valign="top">
                                <table class="styled-table-with-caption ">
                                    <caption>${_(u"ATENCIÓ AL CLIENT")}</caption>
                                    <colgroup>
                                        <col width="40%"/>
                                    </colgroup>
                                    <tbody>
                                        <tr>
                                            <th>${_(u"Empresa comercialitzadora")}</th>
                                            <td>${comer['name']}</td>
                                        </tr>
                                        <tr>
                                            <th>${_(u"Telèfon gratuït")}</th>
                                            <td>${comer['phone']}</td>
                                        </tr>
                                        <tr>
                                            <th>${_(u"Email")}</th>
                                            <td>${comer['email']}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <!-- AVARIES-URGENCIES-ATENCIO CLIENT -->
            </div>
            <!-- END FIRST PAGE -->
            <p style="page-break-after:always; clear:both"></p>
            <!-- SECOND PAGE -->
            <div id="second-page">
                <!-- DETALLE FACTURA -->
                <table id="detall-factura" class="styled-table center-th">
                    <colgroup>
                        <col width="32%" class="background-color"/>
                        <col width="20%"/>
                        <col/>
                        <col width="10%"/>
                    </colgroup>
                    <thead>
                        <tr>
                            <th colspan="3">${_(u"DETALL FACTURA")}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="3">
                                <!-- ENERGIA -->
                                <table class="styled-inner-table">
                                    <colgroup>
                                        <col class="detall-factura-inner-table-col-1"/>
                                        <col class="detall-factura-inner-table-col-2"/>
                                        <col class="detall-factura-inner-table-col-3"/>
                                        <col class="detall-factura-inner-table-col-4"/>
                                        <col class="detall-factura-inner-table-col-5"/>
                                    </colgroup>
                                    <thead>
                                        <tr>
                                            <th>${_(u"Terme d'energia")}</th>
                                            <th>${_(u"Consum")} (kWh)</th>
                                            <th>${_(u"Preu")} (€/kWh)</th>
                                            <th colspan="2">${_(u"Total")} (€)</th>
                                         </tr>
                                    </thead>
                                    <tbody>
                                        % for energy_line in energy_lines['lines']:
                                            <tr>
                                                <td class="bold center">${energy_line['name']}</td>
                                                %if 'bonificación' in energy_line['name']:
                                                    <td></td>
                                                    <td></td>
                                                %else:
                                                    <td class="right">${int(energy_line['consumption'])}</td>
                                                    <td class="right">${formatLang(energy_line['price'], digits=6)}</td>
                                                %endif
                                                <td class="right">${formatLang(energy_line['total'])}</td>
                                                <td class="left white toll italic">(${_(u"dels quals peatges {}").format(formatLang(energy_line['toll']))})</td>
                                            </tr>
                                        % endfor
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr class="row-separator">
                            <td colspan="4" class="bold right white">${total['energy']} €</td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <!-- POTENCIA -->
                                <table class="styled-inner-table">
                                    <colgroup>
                                        <col width="20%"/>
                                        <col width="20%"/>
                                        <col width="20%"/>
                                        <col/>
                                        <col/>
                                        <col width="20%"/>
                                    </colgroup>
                                    <thead>
                                        <tr>
                                            <th>${_(u"Terme de potència")}</th>
                                            <th>${_(u"Consum")} (kW)</th>
                                            <th>${_(u"Preu")} (${policy['power_unit_price']})</th>
                                            % if monthly_invoice:
                                                <th>${_(u"Mesos")}</th>
                                            %else:
                                                <th>${_(u"Dies")}</th>
                                            %endif
                                            <th colspan="2">${_(u"Total")} (€)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        % for power_line in power_lines['lines']:
                                            <tr>
                                                <td class="bold center white">${power_line['name']}</td>
                                                <td class="right white">${formatLang(power_line['consumption'], digits=3)}</td>
                                                <td class="right white">${formatLang(power_line['price'], digits=6)}</td>
                                                <td class="center white">${int(power_line['time'])}</td>
                                                <td class="right white">${formatLang(power_line['total'])}</td>
                                                <td class="left white italic toll">(${_(u"dels quals peatges {}").format(formatLang(power_line['toll']))})</td>
                                            </tr>
                                        % endfor
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr class="row-separator">
                            <td colspan="5" class="bold right white">${formatLang(total['power'])} €</td>
                        </tr>
                        %if total['reactive']:
                            <tr>
                                <td colspan="3">
                                    <!-- ENERGIA REACTIVA  -->
                                    <table class="styled-inner-table">
                                        <colgroup>
                                            <col class="detall-factura-inner-table-col-1"/>
                                            <col class="detall-factura-inner-table-col-2"/>
                                            <col class="detall-factura-inner-table-col-3"/>
                                            <col class="detall-factura-inner-table-col-4"/>
                                            <col class="detall-factura-inner-table-col-5"/>
                                        </colgroup>
                                        <thead>
                                            <tr>
                                                <th>${_(u"Energia reactiva")}</th>
                                                <th>${_(u"Excés")} (kVArh)</th>
                                                <th>${_(u"Preu")} (€/kVArh)</th>
                                                <th colspan="2">${_(u"Total")} (€)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            % for reactive_line in reactive_lines:
                                                <tr>
                                                    <td class="bold center">${reactive_line['name']}</td>
                                                    <td class="right">${formatLang(reactive_line['consumption'])}</td>
                                                    <td class="right">${formatLang(reactive_line['price'], digits=6)}</td>
                                                    <td class="right">${formatLang(reactive_line['total'])}</td>
                                                    <td class="left white toll italic">(${_(u"dels quals peatges {}").format(formatLang(reactive_line['toll']))})</td>
                                                </tr>
                                            % endfor
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr class="row-separator">
                                <td colspan="4" class="bold right white">${formatLang(total['reactive'])} €</td>
                            </tr>
                        %endif
                        %if power_lines_with_excess:
                            <tr>
                                <td colspan="3">
                                    <!-- EXCES POTENCIA -->
                                    <table class="styled-inner-table">
                                        <colgroup>
                                            <col class="detall-factura-inner-table-col-1"/>
                                            <col class="detall-factura-inner-table-col-2"/>
                                            <col class="detall-factura-inner-table-col-3"/>
                                            <col class="detall-factura-inner-table-col-4"/>
                                            <col class="detall-factura-inner-table-col-5"/>
                                        </colgroup>
                                        <thead>
                                            <tr>
                                                <th>${_(u"Excés potència")}</th>
                                                <th>${_(u"Excés")} (kVArh)</th>
                                                <th>${_(u"Preu")} (€/kVArh)</th>
                                                <th colspan="2">${_(u"Total")} (€)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <% total_exces = 0.0 %>
                                            % for power_line_with_excess in power_lines_with_excess:
                                                <tr>
                                                    <td class="bold center">${power_line_with_excess['name']}</td>
                                                    <td class="right">${formatLang(power_line_with_excess['excess'])}</td>
                                                    <td class="right">${formatLang(power_line_with_excess['price'], digits=6)}</td>
                                                    <td class="center" colspan="2">${power_line_with_excess['total']}</td>
                                                </tr>
                                                <% total_exces += power_line_with_excess['total'] %>
                                            % endfor
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr class="row-separator">
                                <td colspan="4" class="bold right white">${formatLang(total_exces)} €</td>
                            </tr>
                        %endif
                        % for iese_line in iese['lines']:
                            <tr class="row-separator">
                                <th>${_(u"Impost de l'electricitat")}</th>
                                <td class="right">${formatLang(iese_line['base_amount'])} €</td>
                                <td class="right">${u"5,11269632%"}</td>
                                <td class="bold right">${formatLang(iese_line['tax_amount'])} €</td>
                            </tr>
                        % endfor
                        % for meter_rent in meters_rent:
                            <tr class="row-separator">
                                <th>${_(u"Lloguer de comptador")}</th>
                                <td></td>
                                <td></td>
                                <td class="bold right">${formatLang(meter_rent['amount'])} €</td>
                            </tr>
                        % endfor
                        %if other_lines['lines']:
                            <tr class="row-separator">
                                <% rowspan = len(other_lines['lines']) + 1 %>
                                <th rowspan="${rowspan}">${_(u"Altres")}</th>
                                <td colspan="3"></td>
                            </tr>
                            % for other_line in other_lines['lines']:
                                <tr>
                                    <td colspan="2" class="bold">${other_line['name']}</td>
                                    <td class="bold right">${formatLang(other_line['price'])} €</td>
                                </tr>
                            % endfor
                        %endif
                        % for iva_line in iva['lines']:
                            <tr class="row-separator">
                                <th>${iva_line['name']}</th>
                                <td class="right">${formatLang(iva_line['base'])} €</td>
                                <td class="right">${iva_line['percentage']}</td>
                                <td class="bold right">${formatLang(iva_line['amount'])} €</td>
                            </tr>
                        % endfor
                        <tr class="row-separator">
                            <th>${_(u"TOTAL FACTURA")}</th>
                            <td></td>
                            <td></td>
                            <td class="bold right">${formatLang(total['invoice'])} €</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="4" class="white">${_(u"Els preus dels termes de peatge d’accés són els publicats a ORDEN ETU/1976/2016")}</td>
                        </tr>
                        <tr>
                            <td colspan="4" class="white">${_(u"Els preus del lloguer dels comptadors són els establerts a ORDEN IET/1491/2013")}</td>
                        </tr>
                    </tfoot>
                </table>
                <!-- END DETALLE FACTURA -->
                <!-- LECTURES -->
                <table id="consum-electric" class="styled-table">
                    <thead>
                        <tr>
                            <th></th>
                            <th>${_(u"CONSUM ELÈCTRIC")}</th>
                            % for period_name in periods_a:
                                <th>${period_name}</th>
                            % endfor
                        </tr>
                    </thead>
                    <tbody>
                        <!-- ACTIVA -->
                        %if readings_a['readings']:
                            <tr>
                                <th rowspan="5" class="rotate h65px"><div>${_("ENERGIA")}</div></th>
                            </tr>
                            % for meter in sorted(readings_a['readings']):
                                <%
                                    meter_readings =  readings_a['readings'][meter]
                                    previous_reading_title = _(u"Lectura anterior {} ({})").format(
                                        formatLang(meter_readings['P1']['previous_date'], date=True),
                                        meter_readings['P1']['previous_origin']
                                    )
                                    current_reading_title = _(u"Lectura actual {} ({})").format(
                                        formatLang(meter_readings['P1']['current_date'], date=True),
                                        meter_readings['P1']['current_origin']
                                    )
                                %>
                                <tr>
                                    <td class="bold light-blue">${_(u"Número de comptador")}</td>
                                    % for period_name in periods_a:
                                        <td class="center light-blue">${meter}</td>
                                    % endfor
                                </tr>
                                <tr>
                                    <td class="bold">${previous_reading_title}</td>
                                    % for period in periods_a:
                                        <td class="right">${formatLang(meter_readings[period]['previous_reading'], digits=0)} kWh</td>
                                    % endfor
                                </tr>
                                <tr>
                                    <td class="bold">${current_reading_title}</td>
                                    % for period in periods_a:
                                        <td class="right">${formatLang(meter_readings[period]['current_reading'], digits=0)} kWh</td>
                                    % endfor
                                </tr>
                            % endfor
                            <tr>
                                <td class="bold">${_(u"Total període")}</td>
                                % for period in periods_a:
                                    <td class="right">${formatLang(readings_a['total_consumption'][period], digits=0)} kWh</td>
                                %endfor
                            </tr>
                        %endif
                        <!-- ACTIVA -->
                        <!-- REACTIVA -->
                        % if readings_r['readings'] or te_maximetre:
                            <tr>
                                <th rowspan="5" class="rotate h65px"><div>${_("REACTIVA")}</div></th>
                            </tr>
                            % for meter in sorted(readings_r['readings']):
                                <%
                                    meter_readings =  readings_r['readings'][meter]
                                    previous_reading_title = _(u"Lectura anterior {} ({})").format(
                                        formatLang(meter_readings['P1']['previous_date'], date=True),
                                        meter_readings['P1']['previous_origin']
                                    )
                                    current_reading_title = _(u"Lectura actual {} ({})").format(
                                        formatLang(meter_readings['P1']['current_date'], date=True),
                                        meter_readings['P1']['current_origin']
                                    )
                                %>
                                <tr class="row-separator">
                                    <td class="bold light-blue">${_(u"Número de comptador")}</td>
                                    % for period_name in periods_r:
                                            <td class="center light-blue">${meter}</td>
                                    % endfor
                                </tr>
                                <tr>
                                    <td class="bold">${previous_reading_title}</td>
                                    % for period in periods_r:
                                        <td class="right">${formatLang(meter_readings[period]['previous_reading'], digits=0)} kWh</td>
                                    % endfor
                                </tr>
                                <tr>
                                    <td class="bold">${current_reading_title}</td>
                                    % for period in periods_r:
                                        <td class="right">${formatLang(meter_readings[period]['current_reading'], digits=0)} kWh</td>
                                    % endfor
                                </tr>
                            % endfor
                            <tr>
                                <td class="bold">${_(u"Total període")}</td>
                                % for period_name in periods_r:
                                    <td class="right">${formatLang(readings_r['total_consumption'][period], digits=0)} kWh</td>
                                % endfor
                            </tr>
                        %endif
                        <!-- REACTIVA -->
                        <!-- MAXIMETRE -->
                        % if policy['has_maximeter']:
                            <%
                                if len(periods_a) != len(periods_m):
                                    raise NotImplementedError
                            %>
                            <tr class="header">
                                <th rowspan="4" class="rotate h65px"> <div>${_("MAXÍMETRE")}</div></th>
                            </tr>
                            <tr class="row-separator">
                                <td class="bold">${_(u"Potència contractada")}</td>
                                % for period in periods_m:
                                    <td class="right">${formatLang(readings_m[period]['power'], digits=3)} kW</td>
                                % endfor
                            </tr>
                            <tr>
                                <td class="bold">${_(u"Lectura maxímetre")}</td>
                                % for period in periods_m:
                                    <td class="right">${formatLang(readings_m[period]['maximeter'], digits=3)} kW</td>
                                % endfor
                            </tr>
                            <tr>
                                <td class="bold">${_(u"Potència facturada període")}</td>
                                % for period in periods_m:
                                    <td class="right">${formatLang(fact_potencia[period], digits=3)} kW</td>
                                % endfor
                            </tr>
                        %endif
                        <!-- MAXIMETRE -->
                    </tbody>
                    <tfoot>
                        <tr>
                            <% colspan =  len(periods_a) + 2 %>
                            <td colspan="${colspan}" class="center">
                                ${_(u"El seu consum mitjà diari en el període facturat ha sigut de {} € que corresponent a {} kWh/dia ({} dies)").format(formatLang(diari_factura_actual_eur), formatLang(diari_factura_actual_kwh), dies_factura or 1)}
                            </td>
                        </tr>
                    </tfoot>
                </table>
                <!-- END LECTURES -->
                %if len(periods_a) <= 3:
                    <!-- DESTI -->
                    <table id="desti-factura" class="styled-table">
                        <thead>
                            <tr>
                                <th class="seccio">${_(u"DESTÍ DE LA FACTURA")}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>${_(u"El destí de l'import de la seva factura ({} €) és el següent:").format(formatLang(total['invoice']))}</td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="chart_desti" id="chart_desti_${invoice.id}"></div>
                                </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td class="italic">${_(u"Als imports indicats en el diagrama se'ls ha d'afegir, en el seu cas, el lloguer dels equips de mesura i control")}</td>
                            </tr>
                        </tfoot>
                    </table>
                    <!-- END DESTI -->
                %endif
                <!-- IMPACTE AMBIENTAL -->
                <!-- END IMPACTE AMBIENTAL -->
                <!-- ORIGEN DE L'ENERGIA -->
                <!-- END ORIGEN DE L'ENERGIA -->
            </div>

            <script>
                var pie_total = ${pie_total};
                var pie_data = [
                    {val: ${pie_regulats}, perc: 30, code: "REG"},
                    {val: ${pie_costos}, perc: 55, code: "PROD"},
                    {val: ${pie_impostos},  perc: 15 ,code: "IMP"}
                ];

                var pie_etiquetes = {
                    'REG': {t: ['${formatLang(float(pie_regulats))} €','${_(u"Costos regulats")}'], w:100},
                    'IMP': {t: ['${formatLang(float(pie_impostos))} €','${_(u"Impostos aplicats")}'], w:100},
                    'PROD': {t: ['${formatLang(float(pie_costos))} €','${_(u"Costos de producció electricitat")}'], w:150}
                };

                var reparto = ${json.dumps(reparto)};
                var dades_reparto = ${json.dumps(dades_reparto)};

                var factura_id = ${invoice.id};
                var data_consum = ${json.dumps(sorted(historic_js, key=lambda h: h['mes']))};
                var es30 = ${len(periods_a)>9 and 'true' or 'false'};
                 ##  var es30 = ${len(periodes_a)>3 and 'true' or 'false'}
            </script>
            <script src="${addons_path}/giscedata_facturacio_comer/report/assets/consumption-chart.js"></script>
            <script src="${addons_path}/giscedata_facturacio_comer/report/assets/pie-chart.js"></script>
            % if invoice != objects[-1]:
                <p style="page-break-after:always; clear:both"></p>
            % endif
        %endfor
    </body>
</html>
