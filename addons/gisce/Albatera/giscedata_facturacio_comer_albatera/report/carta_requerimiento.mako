## -*- coding: utf-8 -*-
<%
    import babel
    from babel.numbers import format_currency
    from datetime import datetime, timedelta
    from bankbarcode.cuaderno57 import Recibo507
    from account_invoice_base.report.utils import  localize_period
    report_obj = pool.get('giscedata.facturacio.carta.requeriment.report')
    imd_obj = pool.get('ir.model.data')
    account_invoice_obj = pool.get('account.invoice')
    ncarta = context['carta']
    if ncarta == 1:
        tipo_carta = 'req1'
    elif ncarta == 2:
        tipo_carta = 'req2'
    else:
        tipo_carta = 'tall'

    phone = company.partner_id.address[0].phone or ''
    email = company.partner_id.address[0].email or ''
    company_address = company.partner_id
    web = company.partner_id.website or ''
    is_coop = company.codi_r2 == '438'
%>

<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
        <title>Carta de Requerimiento</title>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_albatera/report/estilo_carta_requerimiento.css"/>
    </head>
    <body>
        %for invoice in objects:
            <%
                data_inici = report_obj.get_start_day(
                    cursor, uid, invoice.id, tipo_carta, context=context
                )[invoice.id]
            %>
            <table class="header">
                <colgroup>
                    <col style="width: 29%"/>
                    <col style="width: 40%"/>
                    <col style="width: 30%"/>
                </colgroup>
                <tr>
                    <td>
                        <img alt="logo" class="logo" src="data:image/jpeg;base64,${company.logo}" height="75px"/>
                    </td>
                    <td>
                        <h1>
                            ${company.name} <br/>
                        </h1>
                        <div id="subtitle">${company.rml_footer2}</div>
                    </td>
                    <td id="partner_address">
                        ${company_address.address[0].street or ''} ${company_address.address[0].street2 or ''} <br/>
                        ${company_address.address[0].zip or ''} ${company_address.address[0].id_poblacio.name or ''} (${company_address.address[0].state_id.name or ''})<br/>
                        Telf: ${phone} <br/>
                        %if web:
                            Web: <a href="${web}" style="color: inherit; text-decoration: none;">${web}</a> <br/>
                        %endif
                        %if email:
                            E-mail: <a href="mailto:${email}" style="color: inherit; text-decoration: none;">${email}</a>
                        %endif
                    </td>
                </tr>
            </table>
            <div class="address">
                <table>
                    <colgroup>
                        <col style="width: 55%"/>
                        <col style="width: 45%"/>
                    </colgroup>
                    <tr>
                        <td>
                            <table class="address">
                                <tr>
                                    <td>
                                        <u><b>Datos del suministro</b></u>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Núm. Contrato</b> ${invoice.polissa_id.name}
                                    </td>
                                </tr>
                                <%
                                    state_cups = ''
                                    if invoice.cups_id.id_provincia.name:
                                        state_cups = '(' + invoice.cups_id.id_provincia.name + ')'
                                %>
                                <tr>
                                    <td>
                                        ${invoice.address_invoice_id.name}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${invoice.cups_id.nv or ''} ${invoice.cups_id.pnp or ''} ${invoice.cups_id.es or ''} ${invoice.cups_id.pt or ''} ${invoice.cups_id.pu or ''}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${invoice.cups_id.cpo or ''} ${invoice.cups_id.cpa or ''} ${invoice.cups_id.dp or ''} ${invoice.cups_id.id_poblacio.name or ''} ${state_cups}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${invoice.cups_id.aclarador or ''}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p id="referencia">CUPS: ${invoice.cups_id.name}</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <%
                                state_contact = ''
                                if invoice.address_contact_id.state_id.name:
                                    state_contact = '(' + invoice.address_contact_id.state_id.name + ')'
                            %>
                            <table class="address">
                                <tr>
                                    <td>
                                        ${invoice.address_contact_id.name}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${invoice.address_contact_id.street or ''}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${invoice.address_contact_id.street2 or ''}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${invoice.address_contact_id.zip or ''} ${invoice.address_contact_id.city or ''}  ${state_contact}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div>
                Albatera, ${babel.dates.format_datetime(data_inici, "d LLLL 'de' YYYY,", locale='es_ES')}<br/>
##                 Ref abonat<br/>
                <b>${_('ASUNTO: Comunicación vencimiento período de pago y requerimiento fehaciente de pago.')}</b><br/>
                <br/>
                ${_('Muy Sres. Nuestros:')}<br/>
                ${_('Mediante la presente, se le requiere el pago de las cantidades adeudadas en concepto de consumo de energía eléctrica.')}<br/>
                ${_('A día de hoy, no ha satisfecho el pago de la factura emitida a su cargo que detallamos a continuación:')}<br/>

                <br/>
                ${_('DETALLE FACTURA:')}<br/>
                <table class="invoice_details">
                    <tr>
                        <td>
                            ${_('Ref. recibo')}
                        </td>
                        <td>
                            ${_('Periodo facturado')}
                        </td>
                        <td>
                            ${_('Total fact.')}
                        </td>
                        <td>
                            ${_('Pagado')}
                        </td>
                        <td>
                            ${_('Pendiente')}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            ${invoice.number}
                        </td>
                        <td>
                            ${formatLang(invoice.data_inici, date=True)} - ${formatLang(invoice.data_final, date=True)}
                        </td>
                        <td>
                            ${format_currency(float(invoice.amount_total), 'EUR', locale='es_ES')}
                        </td>
                        <%
                            pagat = invoice.amount_total - invoice.residual
                        %>
                        <td>
                            ${format_currency(float(pagat), 'EUR', locale='es_ES')}
                        </td>
                        <td>
                            ${format_currency(float(invoice.residual), 'EUR', locale='es_ES')}
                        </td>
                    </tr>
                </table>
            </div>
            ${_('Le informamos que podrá hacer efectiva la deuda en las oficinas comerciales indicadas en el encabezamiento de este escrito, o bien efectuando una transferencia bancaria a cualquiera de las siguientes cuentas:')}
            %if is_coop:
                <ul>
                    <li>
                        BANKIA c/ah. ES90 2038-3251-01-6000119746
                    </li>
                    <li>
                        CAJA RURAL c/ah. ES11 3005-0050-02-2215908514
                    </li>
                    <li>
                        CAJA MAR c/ah. ES29 3058-2590-13-2710000028
                    </li>
                    <li>
                        BANCO SABADELL c/ah. ES16 0081-1429-70-0001090912
                    </li>
                </ul>
            %else:
                <ul>
                    <li>
                        BANKIA c/ah. ES36 2038-3251-07-6000112858
                    </li>
                    <li>
                        CAJA RURAL c/ah. ES78 3005-0050-09-1140084524
                    </li>
                    <li>
                        CAJA MAR c/ah. ES44 3058-2590-11-2720002399
                    </li>
                </ul>
            %endif
            <div class="condicions">
                ${_('De no abonarse la cantidad adeuda, en un plazo de ')}
                <b>
                    ${_('2 meses a partir de la notificación del presente requerimiento,')}
                </b>
                ${(' la empresa distribuidora podrá suspender su suministro de electricidad.')}<br/>
                <br/>
                ${_('Sin perjuicio de lo anterior, si usted cumple los requisitos para ser consumidor vulnerable, puede solicitar a una de las empresas comercializadores de referencia acogerse al bono social, que supone un descuento sobre el precio voluntario para el pequeño consumidor. (PVPC).')} <br/>
                <br/>
                ${_('El cambio de modalidad en el contrato para pasar a PVPC, siempre que no se modifique los parámetros recogidos en el contrato de acceso de terceros a la red, se llevará a cabo sin ningún tipo de penalización ni coste adicional.')} <br/>
                <br/>
                ${_('Una vez acogido al PVPC, y siempre que se hayan acreditado los requisitos para ser consumidor vulnerable, el plazo para que su suministro de electricidad pueda ser suspendido, de no haber sido abonado la cantidad adeudada, pasará a ser de 4 meses (contados siempre desde la recepción del requerimiento fehaciente de pago).')} <br/>
                <br/>
                ${_('El enlace de la página web de la CNMC donde encontrará los datos necesarios para contactar con la comercializadora de referencia es el siguiente:')}<br/>
                <br/>
                <a href="https://www.cnmc.es/ambitos-de-actuacion/energia/mercado-electrico">https://www.cnmc.es/ambitos-de-actuacion/energia/mercado-electrico</a><br/>
                <br/>
                ${_('Adicionalmente si usted cumple los requisitos para ser vulnerable severo, puede ponerse en contacto con los servicios sociales del municipio y comunidad autónoma donde reside, para que le informen sobre la posibilidad de atender al pago de su suministro de electricidad.')}<br/>
                <br/>
                ${_('Los requisitos para ser consumidor vulnerable vienen recogidos en el artículo 3 de Real Decreto 897/2017, de 6 de octubre, por el que se regula la figura del consumidor vulnerable, el bono social y otras medidas de protección para los consumidores domésticos de energía eléctrica, el cual detallamos en la hoja anexa a este escrito.')}
                <br/>
                ${_('Quedamos a su disposición para cualquier consulta o aclaración.')}<br/><br/>
            </div>
            ${_('Atentamente,')}<br/>
            ${_('Departamento de Cobros')}

            <p style="page-break-after:always"></p>

            <div id="boe">
                <i>
                    ${_('Real Decreto 897/2017, de 6 de octubre, por el que se regula la figura del consumidor vulnerable, el bono social y otras medidas de protección para los consumidores domésticos de energía eléctrica.')}
                </i>
                <b>Artículo 3. Definición de consumidor vulnerable.</b>
                <ol>
                    <li>A los efectos de este real decreto y demás normativa de aplicación, tendrá la consideración de consumidor vulnerable el titular de un punto de suministro de electricidad en su vivienda habitual que, siendo persona física, esté acogido al precio voluntario para el pequeño consumidor (PVPC) y cumpla los restantes requisitos del presente artículo.</li>
                    <li>Para que un consumidor de energía eléctrica pueda ser considerado consumidor vulnerable, deberá cumplir alguno de los requisitos siguientes:
                        <ol type="a">
                            <li>Que su renta o, caso de formar parte de una unidad familiar, la renta conjunta anual de la unidad familiar a que pertenezca sea igual o inferior:
                                <ul>
                                    <li>a 1,5 veces el Indicador Público de Renta de Efectos Múltiples (IPREM) de 14 pagas, en el caso de que no forme parte de una unidad familiar o no haya ningún menor en la unidad familiar;</li>
                                    <li>a 2 veces el índice IPREM de 14 pagas, en el caso de que haya un menor en la unidad familiar;</li>
                                    <li>a 2,5 veces el índice IPREM de 14 pagas, en el caso de que haya dos menores en la unidad familiar.</li>
                                </ul>
                                A estos efectos, se considera unidad familiar a la constituida conforme a lo dispuesto en la Ley 35/2006, de 28 de noviembre, del Impuesto sobre la Renta de las Personas Físicas y de modificación parcial de las leyes de los Impuestos sobre Sociedades, sobre la Renta de no Residentes y sobre el Patrimonio.</li>
                            <li>Estar en posesión del título de familia numerosa.</li>
                            <li>Que el propio consumidor y, en el caso de formar parte de una unidad familiar, todos los miembros de la misma que tengan ingresos, sean pensionistas del Sistema de la Seguridad Social por jubilación o incapacidad permanente, percibiendo la cuantía mínima vigente en cada momento para dichas clases de pensión, y no perciban otros ingresos.</li>
                        </ol>
                    </li>
                    <li>Los multiplicadores de renta respecto del índice IPREM de 14 pagas establecidos en el apartado 2.a) se incrementarán, en cada caso, en 0,5, siempre que concurra alguna de las siguientes circunstancias especiales:
                        <ol type="a">
                            <li>Que el consumidor o alguno de los miembros de la unidad familiar tenga discapacidad reconocida igual o superior al 33%.</li>
                            <li>Que el consumidor o alguno de los miembros de la unidad familiar acredite la situación de violencia de género, conforme a lo establecido en la legislación vigente.</li>
                            <li>Que el consumidor o alguno de los miembros de la unidad familiar tenga la condición de víctima de terrorismo, conforme a lo establecido en la legislación vigente.</li>
                        </ol>
                    </li>
                    <li>Cuando, cumpliendo los requisitos anteriores, el consumidor y, en su caso, la unidad familiar a la que pertenezca, tengan una renta anual inferior o igual al 50% de los umbrales establecidos en el apartado 2.a), incrementados en su caso conforme a lo dispuesto en el apartado 3, el consumidor será considerado vulnerable severo. Asimismo también será considerado vulnerable severo cuando el consumidor, y, en su caso, la unidad familiar a que pertenezca, tengan una renta anual inferior o igual a una vez el IPREM a 14 pagas o dos veces el mismo, en el caso de que se encuentre en la situación del apartado 2.c) o 2.b), respectivamente.</li>

                    <li>En todo caso, para que un consumidor sea considerado vulnerable deberá acreditar el cumplimiento de los requisitos recogidos en el presente artículo en los términos que se establezcan por orden del Ministro de Energía, Turismo y Agenda Digital.</li>

                </ol>
            </div>
            % if invoice != objects[-1]:
                <p style="page-break-after:always"></p>
            % endif
        %endfor
    </body>
</html>
