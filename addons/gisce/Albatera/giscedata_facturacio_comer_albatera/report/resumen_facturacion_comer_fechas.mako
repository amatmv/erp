<%inherit file="/giscedata_facturacio_comer/report/resumen_facturacion_comer_fechas.mako"/>
<%def name="get_fields_to_sum_fact()">
    <%
        fields_to_sum_fact = [
             'lp_euros',
             'le_euros',
             'lr_euros',
             'excesos_euros',
             'otros',
             'lloguers_euros',
             'ie_amount',
             'amount_untaxed',
             'no_imponible',
             'amount_tax',
             'amount_total'
        ]
        return fields_to_sum_fact
    %>
</%def>
<%def name="get_facturacion_cols()">
    <th>${_(u"Tarifa")}</th>
    <th>${_(u"Clients")}</th>
    <th>${_(u"Pot. Fact")}</th>
    <th class="split">${_(u"E")}</th>
    <th>${_(u"P1")}</th>
    <th>${_(u"P2")}</th>
    <th>${_(u"P3")}</th>
    <th>${_(u"P4")}</th>
    <th>${_(u"P5")}</th>
    <th>${_(u"P6")}</th>
    <th>${_(u"Total")}</th>
    <th class="split">${_(u"Potencia")}</th>
    <th>${_(u"Activa")}</th>
    <th>${_(u"Reactiva")}</th>
    <th>${_(u"Exces.")}</th>
    <th>${_(u"Altres")}</th>
    <th>${_(u"Lloguers")}</th>
    <th>${_(u"IE")}</th>
    <th>${_(u"Base imponible (I)")}</th>
    <th>${_(u"Base imponible (II)")}</th>
    <th>${_(u"IVA")}</th>
    <th>${_(u"Total")}</th>
</%def>