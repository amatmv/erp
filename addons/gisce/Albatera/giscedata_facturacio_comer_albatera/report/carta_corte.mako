## -*- coding: utf-8 -*-
<%
    import babel
    from babel.numbers import format_currency
    from datetime import datetime, timedelta
    from bankbarcode.cuaderno57 import Recibo507
    from account_invoice_base.report.utils import  localize_period
    from giscedata_facturacio_impagat.giscedata_facturacio import find_B1
    from workalendar.europe.spain import Spain

    report_obj = pool.get('giscedata.facturacio.carta.requeriment.report')
    imd_obj = pool.get('ir.model.data')
    account_invoice_obj = pool.get('account.invoice')
    ncarta = context['carta']

    def obtenir_dia_pagament(date_due):
        origin_date = datetime.strptime(date_due, '%Y-%m-%d')
        calendar = Spain()
        new_date = calendar.add_working_days(origin_date, delta=-1)
        return new_date

    phone = company.partner_id.address[0].phone or ''
    email = company.partner_id.address[0].email or ''
    web = company.partner_id.website or ''
    company_address = company.partner_id
    is_coop = company.codi_r2 == '438'
%>

<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
        <title>Carta de Aviso de Corte</title>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_albatera/report/estilo_carta_requerimiento.css"/>
    </head>
    <body>
        %for invoice in objects:
            <%
                b1_ids = find_B1(cursor, uid, invoice.id, context={'only_cuts': 1})
            %>
            % if not b1_ids:
                % if len(objects) == 1:
                    <div id="contingut">
                        <h1>No se ha detectado ningún corte para la factura ${invoice.number or ''}</h1>
                    </div>
                % endif
                <%
                    continue
                %>
            %else:
                <%
                    data_inici = report_obj.get_start_day(cursor, uid, invoice.id, ncarta, context=context)[invoice.id]
                    b1_01_obj = pool.get('giscedata.switching.b1.01')
                    swh_ids = b1_01_obj.search(cursor, uid, [('sw_id', '=', b1_ids[0])], limit=1)
                    swh_init = b1_01_obj.browse(cursor, uid, swh_ids[0])
                    date_due = datetime.strptime(swh_init.data_accio, '%Y-%m-%d')
                    fecha_pago = obtenir_dia_pagament(swh_init.data_accio)
                %>
            %endif
            <table class="header">
                <colgroup>
                    <col style="width: 25%"/>
                    <col style="width: 40%"/>
                    <col style="width: 30%"/>
                </colgroup>
                <tr>
                    <td>
                        <img alt="logo" class="logo" src="data:image/jpeg;base64,${company.logo}" height="75px"/>
                    </td>
                    <td>
                        <h1>
                            ${company.name} <br/>
                        </h1>
                        <div id="subtitle">${company.rml_footer2}</div>
                    </td>
                    <td id="partner_address">
                        ${company_address.address[0].street or ''} ${company_address.address[0].street2 or ''} <br/>
                        ${company_address.address[0].zip or ''} ${company_address.address[0].id_poblacio.name or ''} (${company_address.address[0].state_id.name or ''})<br/>
                        Telf: ${phone} <br/>
                        %if web:
                            Web: <a href="${web}" style="color: inherit; text-decoration: none;">${web}</a> <br/>
                        %endif
                        %if email:
                            E-mail: <a href="mailto:${email}" style="color: inherit; text-decoration: none;">${email}</a>
                        %endif
                    </td>
                </tr>
            </table>
            <div class="address">
                <table>
                    <colgroup>
                        <col style="width: 55%"/>
                        <col style="width: 45%"/>
                    </colgroup>
                    <tr>
                        <td>
                            <table class="address">
                                <tr>
                                    <td>
                                        <u><b>Datos del suministro</b></u>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Núm. Contrato</b> ${invoice.polissa_id.name}
                                    </td>
                                </tr>
                                <%
                                    state_cups = ''
                                    if invoice.cups_id.id_provincia.name:
                                        state_cups = '(' + invoice.cups_id.id_provincia.name + ')'
                                %>
                                <tr>
                                    <td>
                                        ${invoice.address_invoice_id.name}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${invoice.cups_id.nv or ''} ${invoice.cups_id.pnp or ''} ${invoice.cups_id.es or ''} ${invoice.cups_id.pt or ''} ${invoice.cups_id.pu or ''}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${invoice.cups_id.cpo or ''} ${invoice.cups_id.cpa or ''} ${invoice.cups_id.dp or ''} ${invoice.cups_id.id_poblacio.name or ''} ${state_cups}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${invoice.cups_id.aclarador or ''}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p id="referencia">CUPS: ${invoice.cups_id.name}</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <%
                                state_contact = ''
                                if invoice.address_contact_id.state_id.name:
                                    state_contact = '(' + invoice.address_contact_id.state_id.name + ')'
                            %>
                            <table class="address">
                                <tr>
                                    <td>
                                        ${invoice.address_contact_id.name}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${invoice.address_contact_id.street or ''}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${invoice.address_contact_id.street2 or ''}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${invoice.address_contact_id.zip or ''} ${invoice.address_contact_id.city or ''}  ${state_contact}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <h2>AVISO DE CORTE DEL SERVICIO ELÉCTRICO</h2>
            <div class="">
                <table>
                    <colgroup>
                        <col style="width: 60%"/>
                        <col style="width: 40%"/>
                    </colgroup>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            ÚLTIMO DÍA DE PAGO:
                            % if date_due != 'False':
                                ${formatLang(fecha_pago, date=True)}
                            % endif
                        </td>
                    </tr>
                </table>
            </div>
            Albatera, ${babel.dates.format_datetime(data_inici, "d LLLL 'de' YYYY,", locale='es_ES')}<br/>
            ${_("Muy Sres. Nuestros:")}<br/>
            <div class="condicions">
                ${_("Por la presente le comunicamos que tenemos constancia de que la siguiente factura de suministro eléctrico ha sido DEVUELTA por su Banco o Caja de Ahorros y que a continuación detallamos.")} <br/>
                ${_("(Rogamos que, si la devolución es debida a error o cancelación de su domiciliación bancaria, se acerquen a nuestra oficina para actualizar sus datos bancarios)")} <br/>
            </div>
            <br/>
            ${_("DETALLE FACTURA:")}
            <div class="minheight100 border margin_bottom_10">
                <table class="cut_invoice_details">
                    <tr>
                        <td>
                            ${_('Ref. recibo')}
                        </td>
                        <td>
                            ${_('Periodo facturado')}
                        </td>
                        <td>
                            ${_('Total fact.')}
                        </td>
                        <td>
                            ${_('Pagado')}
                        </td>
                        <td>
                            ${_('Pendiente')}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            ${invoice.number}
                        </td>
                        <td>
                            ${formatLang(invoice.data_inici, date=True)} - ${formatLang(invoice.data_final, date=True)}
                        </td>
                        <td>
                            ${format_currency(float(invoice.amount_total), 'EUR', locale='es_ES')}
                        </td>
                        <%
                            pagat = invoice.amount_total - invoice.residual
                        %>
                        <td>
                            ${format_currency(float(pagat), 'EUR', locale='es_ES')}
                        </td>
                        <td>
                            ${format_currency(float(invoice.residual), 'EUR', locale='es_ES')}
                        </td>
                    </tr>
                </table>
                <br/>
                <div id="total">
                    TOTAL: ${format_currency(float(invoice.residual), 'EUR', locale='es_ES')}
                </div>
            </div>
            <div class="condicions">
                ${_("Por lo expuesto, y de acuerdo con el R.D. 1955/2000, art 84 punto 1 y legislación posterior del pago y suspension del suministro de energía Eléctrica, se le concede un plazo determinado, para que proceda a la cancelación íntegra de la citada deuda ingresando su importe en algunas de las cuentas indicadas:")}
            </div>
            %if is_coop:
                <ul>
                    <li>
                        BANKIA c/ah. ES90 2038-3251-01-6000119746
                    </li>
                    <li>
                        CAJA RURAL c/ah. ES11 3005-0050-02-2215908514
                    </li>
                    <li>
                        CAJA MAR c/ah. ES29 3058-2590-13-2710000028
                    </li>
                    <li>
                        BANCO SABADELL c/ah. ES16 0081-1429-70-0001090912
                    </li>
                </ul>
            %else:
                <ul>
                    <li>
                        BANKIA c/ah. ES36 2038-3251-07-6000112858
                    </li>
                    <li>
                        CAJA RURAL c/ah. ES78 3005-0050-09-1140084524
                    </li>
                    <li>
                        CAJA MAR c/ah. ES44 3058-2590-11-2720002399
                    </li>
                </ul>
            %endif
            <div class="condicions">
                ${_("Una vez efectuado el pago en el plazo señalado, deberá Vd. personarse en las oficinas de esta entidad C/Av. de la Libertad nº45 a fin de acreditar dicho pago antes de la fecha de corte más abajo indicada, o bien por correo electrónico")}
                <a href="mailto:${email}" style="color: inherit; text-decoration: none;">${email}</a>.
                ${_("Pasado dicho plazo sin haberlo efectuado procederemos al corte del suministro eléctrico que se llevará a cabo, en el horario habitual de trabajo.")}
                ${_("De no atender al pago del recibo actual, se le repercutirá en concepto de ENGANCHE el importe de 21,88 euros (IVA incluido), incluido en la siguiente factura.")} <br/>
            </div>
            <br/>
            <table class="invoice_details">
                <tr>
                    <td>
                        ${_("FECHA DE CORTE DEL SUMINISTRO ELÉCTRICO:")}
                    </td>
                    <td>
                        ${date_due.strftime('%d/%m/%Y')}
                    </td>
                </tr>
            </table>
            ${_("Atentamente,")} <br/>
            ${company.name}
            % if invoice != objects[-1]:
                <p style="page-break-after:always"></p>
            % endif
        %endfor
    </body>
</html>
