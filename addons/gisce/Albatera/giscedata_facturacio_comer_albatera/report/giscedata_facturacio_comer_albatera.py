# -*- coding: utf-8 -*-
from c2c_webkit_report import webkit_report
from report import report_sxw
from tools import config


class report_webkit_html(report_sxw.rml_parse):
    def __init__(self, cursor, uid, name, context):
        super(report_webkit_html, self).__init__(cursor, uid, name,
                                                 context=context)
        self.localcontext.update({
            'cursor': cursor,
            'uid': uid,
            'addons_path': config['addons_path'],
            'origin_access': name,
            'pool': self.pool,
            'duplicate': name == 'giscedata.facturacio.factura.copia'
        })


webkit_report.WebKitParser(
    'report.giscedata.facturacio.factura',
    'giscedata.facturacio.factura',
    'giscedata_facturacio_comer_albatera/report/factura.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.facturacio.factura.copia',
    'giscedata.facturacio.factura',
    'giscedata_facturacio_comer_albatera/report/factura.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.facturacio.factura_concepto',
    'giscedata.facturacio.factura',
    'giscedata_facturacio_comer_albatera/report/factura_conceptes.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.facturacio.factura.carta.requerimiento.1',
    'giscedata.facturacio.factura',
    'giscedata_facturacio_comer_albatera/report/carta_requerimiento.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.facturacio.factura.carta.requerimiento.2',
    'giscedata.facturacio.factura',
    'giscedata_facturacio_comer_albatera/report/carta_requerimiento.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.facturacio.factura.carta.corte',
    'giscedata.facturacio.factura',
    'giscedata_facturacio_comer_albatera/report/carta_corte.mako',
    parser=report_webkit_html
)
