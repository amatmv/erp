# -*- coding: utf-8 -*-
{
    "name": "Facturación Cooperativa/Eléctrica (Comercializadora)",
    "description": """Facturación Albatera Cooperativa/Eléctrica (Comercializadora)""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Albatera",
    "depends": [
        "giscedata_facturacio_comer",
        "report_banner",
        "giscedata_facturacio_impagat_comer"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "giscedata_facturacio_comer_albatera_report.xml"
    ],
    "active": False,
    "installable": True
}