# -*- coding: utf-8 -*-
{
    "name": "Control Gestió Distri",
    "description": """Mòdul per a la generació de fixers XLS per comptar altes, baixes i energia facturada a Distribuidora""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Lersa",
    "depends": [
        "base",
        "giscedata_polissa_distri",
        "giscedata_switching",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "wizard/giscedata_control_gestio_distri_view.xml",
        "security/giscedata_control_gestio_distri_security.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
