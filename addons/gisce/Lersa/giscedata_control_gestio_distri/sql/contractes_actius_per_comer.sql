SELECT
    par.name AS Comercialitzadora,
    COUNT (*) AS Contractes
FROM
    giscedata_polissa_modcontractual modcon
LEFT JOIN res_partner par ON (modcon.comercialitzadora = par.id)
LEFT JOIN giscedata_polissa pol ON modcon.polissa_id = pol.id
WHERE
    %(data)s BETWEEN modcon.data_inici AND modcon.data_final
    AND pol.state NOT IN ('esborrany', 'cancelada')
GROUP BY
    par.name
ORDER BY
    par.name
