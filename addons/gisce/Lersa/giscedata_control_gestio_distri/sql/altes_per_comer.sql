SELECT
    par.name AS Comercialitzadora,
    COUNT(*) AS Altes
FROM
(
SELECT
    'A3' AS process,
    '05' AS step,
    sw_a3_05.data_activacio AS fecha_cambio,
    sw.cups_polissa_id AS polissa_id,
    sw.cups_id AS cups_id,
    sw.case_id AS case_id
FROM
    giscedata_switching sw,
    giscedata_switching_step_header sw_sh,
    giscedata_switching_a3_05 sw_a3_05
WHERE
    sw_a3_05.header_id = sw_sh.id
    AND sw_sh.sw_id = sw.id
UNION
    SELECT
        'C1' AS process,
        '05' AS step,
        sw_c1_05.data_activacio AS fecha_cambio,
        sw.cups_polissa_id AS polissa_id,
        sw.cups_id AS cups_id,
        sw.case_id AS case_id
    FROM
        giscedata_switching sw,
        giscedata_switching_step_header sw_sh,
        giscedata_switching_c1_05 sw_c1_05
    WHERE
        sw_c1_05.header_id = sw_sh.id
        AND sw_sh.sw_id = sw.id
UNION
    SELECT
        'C2' AS process,
        '05' AS step,
        sw_c2_05.data_activacio AS fecha_cambio,
        sw.cups_polissa_id AS polissa_id,
        sw.cups_id AS cups_id,
        sw.case_id AS case_id
    FROM
        giscedata_switching sw,
        giscedata_switching_step_header sw_sh,
        giscedata_switching_c2_05 sw_c2_05
    WHERE
        sw_c2_05.header_id = sw_sh.id
        AND sw_sh.sw_id = sw.id
) AS casos
LEFT JOIN crm_case ON (casos.case_id  = crm_case.id)
LEFT JOIN res_partner par ON (crm_case.partner_id = par.id)
WHERE
    casos.fecha_cambio BETWEEN %(data_inici)s AND %(data_final)s
GROUP BY
    par.name
ORDER BY
    par.name
