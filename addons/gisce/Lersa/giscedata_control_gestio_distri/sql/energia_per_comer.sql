SELECT
    energia.comercialitzadora AS Comercialitzazora,
    SUM (energia.energia * CASE WHEN energia.tensio <> 'E0' THEN 1 ELSE 0 END) AS AT,
    SUM (energia.energia * CASE WHEN energia.tensio = 'E0' THEN 1 ELSE 0 END) AS BT,
    SUM (energia.energia) AS TOTAL
FROM (
    SELECT
        comer.name AS comercialitzadora,
        pol.agree_tensio AS tensio,
        t.name AS tarifa,
        f.energia_kwh * CASE WHEN i.type = 'out_refund' THEN -1 ELSE 1 END AS energia
    FROM
        giscedata_facturacio_factura f
        INNER JOIN account_invoice i ON f.invoice_id = i.id
        INNER JOIN giscedata_polissa_tarifa t ON f.tarifa_acces_id = t.id
        INNER JOIN res_partner comer ON i.partner_id = comer.id
        INNER JOIN giscedata_polissa pol ON f.polissa_id = pol.id
        INNER JOIN account_journal j ON i.journal_id = j.id
    WHERE
        i.date_invoice BETWEEN %(data_inici)s AND %(data_final)s
        AND i.type IN ('out_invoice', 'out_refund')
        AND i.state IN ('open', 'paid')
        AND j.code LIKE 'ENERGIA%%'
    GROUP BY
        i.type,
        i.number,
        pol.agree_tensio,
        f.energia_kwh,
        f.potencia,
        comer.ref,
        comer.name,
        t.name
    ) AS energia
GROUP BY
    energia.comercialitzadora
ORDER BY
    energia.comercialitzadora