SELECT
    casos.tarifa AS Tarifa,
    COUNT (*) AS Baixes
FROM
(
SELECT
    'B1' as process,
    '05' as step,
    sw_b1_05.data_activacio as fecha_cambio,
    sw.cups_polissa_id as polissa_id,
    sw.cups_id as cups_id,
    sw.case_id as case_id,
    pt.descripcio as tarifa
FROM
    giscedata_switching sw,
    giscedata_switching_step_header sw_sh,
    giscedata_switching_b1_05 sw_b1_05,
    giscedata_polissa_modcontractual modcon,
    giscedata_polissa_tarifa pt
WHERE
    sw_b1_05.header_id = sw_sh.id
    AND sw_sh.sw_id = sw.id
    AND modcon.polissa_id = sw.cups_polissa_id
    AND modcon.tarifa = pt.id
UNION
    SELECT
        'C1' as process,
        '06' as step,
        sw_c1_06.data_activacio as fecha_cambio,
        sw.cups_polissa_id as polissa_id,
        sw.cups_id as cups_id,
        sw.case_id as case_id,
        pt.descripcio as tarifa
    FROM
        giscedata_switching sw,
        giscedata_switching_step_header sw_sh,
        giscedata_switching_c1_06 sw_c1_06,
        giscedata_polissa_modcontractual modcon,
        giscedata_polissa_tarifa pt
    WHERE
        sw_c1_06.header_id = sw_sh.id
        AND sw_sh.sw_id = sw.id
        AND modcon.polissa_id = sw.cups_polissa_id
        AND modcon.tarifa = pt.id
UNION
    SELECT
        'C2' as process,
        '06' as step,
        sw_c2_06.data_activacio as fecha_cambio,
        sw.cups_polissa_id as polissa_id,
        sw.cups_id as cups_id,
        sw.case_id as case_id,
        pt.descripcio as tarifa
    FROM
        giscedata_switching sw,
        giscedata_switching_step_header sw_sh,
        giscedata_switching_c2_06 sw_c2_06,
        giscedata_polissa_modcontractual modcon,
        giscedata_polissa_tarifa pt
    WHERE
        sw_c2_06.header_id = sw_sh.id
        AND sw_sh.sw_id = sw.id
        AND modcon.polissa_id = sw.cups_polissa_id
        AND modcon.tarifa = pt.id
) as casos
LEFT JOIN crm_case on (casos.case_id  = crm_case.id)
LEFT JOIN res_partner par on (crm_case.partner_id = par.id)
WHERE
    casos.fecha_cambio::date + INTERVAL '1 day' BETWEEN %(data_inici)s AND %(data_final)s
GROUP BY
    casos.tarifa
ORDER BY
    casos.tarifa