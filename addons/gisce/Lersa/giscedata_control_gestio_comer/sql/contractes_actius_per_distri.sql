SELECT
    contractes.*,
    round((Contractes / SUM(Contractes) OVER ())*100, 2) AS "%%"
FROM (
    SELECT
        par.name AS Distribuidora,
        COUNT (*) AS Contractes
    FROM
        giscedata_polissa_modcontractual modcon
    LEFT JOIN res_partner par ON (modcon.distribuidora = par.id)
    LEFT JOIN giscedata_polissa pol ON modcon.polissa_id = pol.id
    WHERE
        %(data)s BETWEEN modcon.data_inici AND modcon.data_final
        AND pol.state NOT IN ('esborrany', 'cancelada')
    GROUP BY
        par.name
    ORDER BY
        par.name
    ) contractes
