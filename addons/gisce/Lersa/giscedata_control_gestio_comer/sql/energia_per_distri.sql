SELECT
    resultat.distribuidora AS Distribuidora,
    resultat.energia AS Energia_kWh,
    round((resultat.energia / SUM(resultat.energia) OVER ())*100, 2) AS "%%"
FROM (
    SELECT
        energia.distribuidora AS distribuidora,
        SUM(energia.energia) AS energia
    FROM (
        SELECT
            distri.name AS distribuidora,
            f.energia_kwh * CASE WHEN i.type = 'out_refund' THEN -1 ELSE 1 END AS energia
        FROM
            giscedata_facturacio_factura f
        INNER JOIN account_invoice i ON f.invoice_id = i.id
        INNER JOIN giscedata_polissa_tarifa t ON f.tarifa_acces_id = t.id
        INNER JOIN giscedata_polissa pol ON f.polissa_id = pol.id
        INNER JOIN giscedata_polissa_modcontractual modcon ON modcon.polissa_id = pol.id
        INNER JOIN res_partner distri ON modcon.distribuidora = distri.id
        INNER JOIN account_journal j ON i.journal_id = j.id
        WHERE
            i.date_invoice BETWEEN %(data_inici)s AND %(data_final)s
            AND i.type IN ('out_invoice', 'out_refund')
            AND i.state IN ('open', 'paid')
            AND j.code LIKE 'ENERGIA%%'
        GROUP BY
            i.type,
            i.number,
            pol.agree_tensio,
            f.energia_kwh,
            f.potencia,
            distri.name,
            t.name
        ) AS energia
    GROUP BY
        energia.distribuidora
    ) AS resultat
GROUP BY
    resultat.distribuidora,
    resultat.energia
ORDER BY
    resultat.distribuidora
