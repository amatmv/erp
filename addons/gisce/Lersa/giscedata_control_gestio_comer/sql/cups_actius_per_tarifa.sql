SELECT
    pt.descripcio AS Tarifa,
    modcon.cups AS CUPS
FROM
    giscedata_polissa_modcontractual modcon
LEFT JOIN giscedata_polissa_tarifa pt ON modcon.tarifa = pt.id
LEFT JOIN giscedata_polissa pol ON modcon.polissa_id = pol.id
WHERE
    %(data)s BETWEEN modcon.data_inici AND modcon.data_final
    AND pol.state NOT IN ('esborrany', 'cancelada')
ORDER BY
    pt.descripcio
