SELECT
    energia.tarifa AS Tarifa,
    SUM (energia.energia) AS Energia_kWh,
    SUM (energia.euros) AS Euros
FROM (
    SELECT
        t.name AS tarifa,
        f.energia_kwh * CASE WHEN i.type = 'out_refund' THEN -1 ELSE 1 END AS energia,
        (f.total_energia + f.total_reactiva + f.total_potencia + total_exces_potencia + total_altres) * CASE WHEN i.type = 'out_refund' THEN -1 ELSE 1 END AS euros
    FROM
        giscedata_facturacio_factura f
        INNER JOIN account_invoice i ON f.invoice_id = i.id
        INNER JOIN giscedata_polissa_tarifa t ON f.tarifa_acces_id = t.id
        INNER JOIN giscedata_polissa pol ON f.polissa_id = pol.id
        INNER JOIN account_journal j ON i.journal_id = j.id
    WHERE
        i.date_invoice BETWEEN %(data_inici)s AND %(data_final)s
        AND i.type IN ('out_invoice', 'out_refund')
        AND i.state IN ('open', 'paid')
        AND j.code LIKE 'ENERGIA%%'
    GROUP BY
        i.type,
        i.number,
        energia,
        euros,
        t.name
    ) AS energia
GROUP BY
    energia.tarifa
ORDER BY
    energia.tarifa