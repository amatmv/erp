# -*- encoding: utf-8 -*-
from collections import Counter
from osv import osv, fields
from datetime import datetime, timedelta
from tools import config
from tools.translate import _
import base64
import pandas as pd
import netsvc
import StringIO
import xlsxwriter


class WizardControlGestioComerFiles(osv.osv_memory):
    """Assistent per exportar fitxers de control de gestió."""
    _name = 'wizard.control.gestio.comer.files'

    def export_file(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        wiz_fields = self.read(
            cursor, uid, ids, ['data_inici', 'data_final'], context=context
        )[0]

        # Date range
        start_date = str(wiz_fields['data_inici'])
        day_before_start_date = (datetime.strptime(start_date, "%Y-%m-%d") - timedelta(days=1)).strftime("%Y-%m-%d")
        end_date = str(wiz_fields['data_final'])

        logger = netsvc.Logger()
        logger.notifyChannel("addons", netsvc.LOG_INFO,
                             _("Realitzant les consultes SQL..."))

        try:
            sql_path = '{}/{}/sql/'.format(config['addons_path'], 'giscedata_control_gestio_comer')
            sql1 = open(sql_path + 'contractes_actius_per_tarifa.sql').read()
            sql2 = open(sql_path + 'contractes_actius_per_distri.sql').read()
            sql3 = open(sql_path + 'cups_actius_per_tarifa.sql').read()
            sql5 = open(sql_path + 'energia_per_tarifa.sql').read()
            sql6 = open(sql_path + 'energia_per_distri.sql').read()

            # Contracts at start date
            logger.notifyChannel("addons", netsvc.LOG_INFO,
                                 _("Nombre de contractes a data d'inici..."))

            params = {'data': day_before_start_date}

            cursor.execute(sql1, params)
            lines = cursor.fetchall()
            contracts_start_date_by_tariff = pd.DataFrame(lines, columns=['Tarifa', 'Contractes'])

            # Contracts at end date
            logger.notifyChannel("addons", netsvc.LOG_INFO,
                                 _("Nombre de contractes a data final..."))

            params = {'data': end_date}

            cursor.execute(sql1, params)
            lines = cursor.fetchall()
            contracts_end_date_by_tariff = pd.DataFrame(lines, columns=['Tarifa', 'Contractes'])
            cursor.execute(sql2, params)
            lines = cursor.fetchall()
            contracts_end_date_by_distri = pd.DataFrame(lines, columns=['Distribuidora', 'Contractes', '%'])

            # Signups & drop outs
            logger.notifyChannel("addons", netsvc.LOG_INFO,
                                 _("Altes i baixes durant el període..."))

            params = {'data': day_before_start_date}

            cursor.execute(sql3, params)
            lines = cursor.fetchall()
            cups_before = pd.DataFrame(lines, columns=['Tarifa', 'CUPS'])

            params = {'data': end_date}
            cursor.execute(sql3, params)
            lines = cursor.fetchall()
            cups_after = pd.DataFrame(lines, columns=['Tarifa', 'CUPS'])

            # Signups
            logger.notifyChannel("addons", netsvc.LOG_INFO,
                                 _("Altes durant el període..."))

            signups_list = []

            for row in cups_after.iterrows():
                if not ((cups_before['Tarifa'] == row[1].Tarifa) & (cups_before['CUPS'] == row[1].CUPS)).any():
                    signups_list.append((row[1].Tarifa, row[1].CUPS))

            # Drop outs
            logger.notifyChannel("addons", netsvc.LOG_INFO,
                                 _("Baixes durant el període..."))

            dropouts_list = []

            for row in cups_before.iterrows():
                if not ((cups_after['Tarifa'] == row[1].Tarifa) & (cups_after['CUPS'] == row[1].CUPS)).any():
                    dropouts_list.append((row[1].Tarifa, row[1].CUPS))

            signups_counter = Counter(elem[0] for elem in signups_list)
            if len(signups_counter):
                signups = pd.DataFrame.from_dict(signups_counter, orient='index').reset_index()
                signups = signups.rename(columns={'index': 'Tarifa', 0: 'Altes'})
            else:
                signups = pd.DataFrame(columns=['Tarifa', 'Altes'])

            dropouts_counter = Counter(elem[0] for elem in dropouts_list)
            if len(dropouts_counter):
                dropouts = pd.DataFrame.from_dict(dropouts_counter, orient='index').reset_index()
                dropouts = dropouts.rename(columns={'index': 'Tarifa', 0: 'Baixes'})
            else:
                dropouts = pd.DataFrame(columns=['Tarifa', 'Baixes'])

            # Energy
            logger.notifyChannel("addons", netsvc.LOG_INFO,
                                 _("Energia distribuïda durant el període..."))

            params = {'data_inici': start_date,
                      'data_final': end_date}

            # Invoiced energy
            cursor.execute(sql5, params)
            lines = cursor.fetchall()
            energy_by_tariff = pd.DataFrame(lines, columns=['Tarifa', 'Energia', 'Euros'])

            # Supplied energy
            cursor.execute(sql6, params)
            lines = cursor.fetchall()
            energy_by_distri = pd.DataFrame(lines, columns=['Distribuidora', 'Energia', '%'])

            logger.notifyChannel("addons", netsvc.LOG_INFO,
                                 _("Consultes SQL realitzades."))

        except Exception as e:
            raise osv.except_osv(
                'Error',
                _("S'ha produit un error al realitzar les consultes:\n{0}").format(e)
            )

        try:
            # Excel file generation
            logger.notifyChannel("addons", netsvc.LOG_INFO,
                                 _("Generant el fitxer de resultats..."))

            clients_old_title = 'Nº clientes inicial'
            clients_new_title = 'Total clientes Comercialización'

            # First notebook
            df_energy = energy_by_tariff.rename(columns={'Tarifa': 'Evolución Consumos',
                                                         'Energia': 'KWh',
                                                         'Euros': u'Facturación €'})

            # Drop NaN
            df_energy.fillna(0, inplace=True)

            # Calculate and append totals row
            total_kwh = df_energy.get('KWh', pd.Series([0])).sum()
            total_euros = df_energy.get(u'Facturación €', pd.Series([0])).sum()
            df_energy = df_energy.append({'Evolución Consumos': 'TOTAL',
                                          'KWh': total_kwh,
                                          u'Facturación €': total_euros},
                                         ignore_index=True)

            # Second notebook
            # Contracts
            df_contractes_tariff = pd.merge(contracts_start_date_by_tariff, contracts_end_date_by_tariff,
                                            how='outer', on=['Tarifa'])
            df_contractes_tariff = df_contractes_tariff.rename(
                columns={'Contractes_x': clients_old_title, "Contractes_y": clients_new_title})

            # Signups and drop outs
            df_altes_baixes = pd.merge(signups, dropouts, how='outer', on=['Tarifa'])
            df_altes_baixes = df_altes_baixes.rename(columns={'Altes': 'Altas', 'Baixes': 'Bajas'})

            # First merge
            df_total = pd.merge(df_contractes_tariff, df_altes_baixes, how='outer', on=['Tarifa'])

            # Rename and order columns
            df_total = df_total.rename(columns={'Tarifa': 'Evolución clientes'})
            order = ['Evolución clientes', clients_old_title, 'Altas', 'Bajas', clients_new_title]
            df_total = df_total[order]

            # Drop NaN
            df_total.fillna(0, inplace=True)

            # Calculate and append totals row
            total_clients_old = df_total.get(clients_old_title, pd.Series([0])).sum()
            total_altes = df_total.get('Altas', pd.Series([0])).sum()
            total_baixes = df_total.get('Bajas', pd.Series([0])).sum()
            total_clients_new = df_total.get(clients_new_title, pd.Series([0])).sum()
            df_total = df_total.append(
                {'Evolución clientes': 'TOTAL', clients_old_title: total_clients_old, 'Altas': total_altes,
                 'Bajas': total_baixes, clients_new_title: total_clients_new}, ignore_index=True)

            # Third notebook
            df_contracts_energy = pd.merge(contracts_end_date_by_distri, energy_by_distri,
                                           how='outer', on='Distribuidora')

            # Drop NaN
            df_contracts_energy.fillna(0, inplace=True)

            # Calculate and append totals row
            total_clients = df_contracts_energy.get('Contractes', pd.Series([0])).sum()
            total_x = df_contracts_energy.get('%_x', pd.Series([0])).sum()
            total_energy = df_contracts_energy.get('Energia', pd.Series([0])).sum()
            total_y = df_contracts_energy.get('%_y', pd.Series([0])).sum()
            df_contracts_energy = df_contracts_energy.append(
                {'Distribuidora': 'TOTAL', 'Contractes': total_clients, '%_x': total_x, 'Energia': total_energy,
                 '%_y': total_y}, ignore_index=True)

            # Rename and order columns
            order = ['Distribuidora', 'Contractes', '%_x', 'Energia', '%_y']
            df_contracts_energy = df_contracts_energy[order]
            df_contracts_energy = df_contracts_energy.rename(
                columns={'Nº clientes inicial': 'Clientes', '%_x': '%', 'Energia': 'Energía KWh', '%_y': '%'})

            logger.notifyChannel("addons", netsvc.LOG_INFO,
                                 _("Fitxer de resultats generat."))

            # Excel file exportation
            logger.notifyChannel("addons", netsvc.LOG_INFO,
                                 _("Exportant el fitxer de resultats..."))

            filename = 'control_gestio_comer_{}_{}.xlsx'.format(start_date, end_date)

            output = StringIO.StringIO()

            # Create a Pandas Excel writer using XlsxWriter as the engine.
            writer = pd.ExcelWriter(output, engine='xlsxwriter')

            # Write each dataframe to a different worksheet.
            df_energy.to_excel(writer, sheet_name='Evolución Consumos', index=False, startrow=1, header=False)
            df_total.to_excel(writer, sheet_name='Evolución clientes', index=False, startrow=1, header=False)
            df_contracts_energy.to_excel(writer, sheet_name='Distribuidoras', index=False, startrow=1, header=False)

            # Get the xlsxwriter workbook and worksheet objects.
            workbook = writer.book

            # Add style to headers
            header_format = workbook.add_format({
                'bold': True,
                'text_wrap': True,
                'valign': 'top',
                'fg_color': '#2E75B6',
                'font_color': '#F5F5F5',
                'border': 1})

            # Write the column headers with the defined format.
            worksheet = writer.sheets['Evolución Consumos']
            for col_num, value in enumerate(df_energy.columns.values):
                worksheet.write(0, col_num, value, header_format)

            worksheet = writer.sheets['Evolución clientes']
            for col_num, value in enumerate(df_total.columns.values):
                worksheet.write(0, col_num, value, header_format)

            worksheet = writer.sheets['Distribuidoras']
            for col_num, value in enumerate(df_contracts_energy.columns.values):
                worksheet.write(0, col_num, value, header_format)

            # Close the Pandas Excel writer and output the Excel file.
            writer.save()

            xlsx_data = output.getvalue()
            output.close()

            mfile = base64.b64encode(xlsx_data)

            self.write(cursor, uid, ids, {
                'state': 'end',
                'file_name': filename,
                'file': mfile
            })

            logger.notifyChannel("addons", netsvc.LOG_INFO,
                                 _("Exportació finalitzada."))
        except Exception as e:
            raise osv.except_osv(
                'Error',
                _("S'ha produit un error a l'exportar el fitxer de resultats:\n{0}").format(e)
            )

        results = [int(total_kwh),
                   int(total_clients_new),
                   int(total_energy)]

        return results

    _columns = {
        'data_inici': fields.date('Data inici', required=True,
                                  help='Data des de la qual es comptabilitzaran els moviments'),
        'data_final': fields.date('Data final', required=True,
                                  help='Data fins la qual es comptabilitzaran els moviments'),
        'file': fields.binary('Fitxer', readonly=1),
        'file_name': fields.text('Nom del fitxer'),
        'state': fields.selection(
            [('init', 'Init'), ('end', 'End')],
            'State'
        ),
    }

    _defaults = {
        'data_inici': datetime.now().strftime('%Y-%m-%d'),
        'data_final': datetime.now().strftime('%Y-%m-%d'),
        'state': lambda *a: 'init'
    }


WizardControlGestioComerFiles()
