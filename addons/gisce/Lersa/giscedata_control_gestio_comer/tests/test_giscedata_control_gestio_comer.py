# -*- coding: utf-8 -*-
from destral import testing


class TestGiscedataControlGestioComer(testing.OOTestCaseWithCursor):

    def create_policies(self, cursor, uid):
        pool = self.openerp.pool
        policy_obj = pool.get('giscedata.polissa')
        imd_obj = pool.get('ir.model.data')

        policy_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]

        policy_obj.send_signal(cursor, uid, [policy_id], [
            'validar', 'contracte'
        ])

        self.create_modcon(cursor, uid, policy_id, 7.000, '2019-09-16', '2019-10-15')

        policy_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0002'
        )[1]

        policy_obj.send_signal(cursor, uid, [policy_id], [
            'validar', 'contracte'
        ])

        self.create_modcon(cursor, uid, policy_id, 7.000, '2019-10-16', '2019-11-15')

        policy_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0004'
        )[1]

        policy_obj.send_signal(cursor, uid, [policy_id], [
            'validar', 'contracte'
        ])

        self.create_modcon(cursor, uid, policy_id, 7.000, '2019-09-30', '2019-10-31')

        # TODO: Extend test to all cases

        return policy_obj.browse(cursor, uid, policy_id)

    def create_modcon(self, cursor, uid, policy_id, potencia, start, end):
        pool = self.openerp.pool
        policy_obj = pool.get('giscedata.polissa')
        pol = policy_obj.browse(cursor, uid, policy_id)
        pol.send_signal(['modcontractual'])
        policy_obj.write(cursor, uid, policy_id, {'potencia': potencia})
        wz_crear_mc_obj = pool.get('giscedata.polissa.crear.contracte')

        ctx = {'active_id': policy_id}
        params = {'duracio': 'nou'}

        wz_id_mod = wz_crear_mc_obj.create(cursor, uid, params, ctx)
        wiz_mod = wz_crear_mc_obj.browse(cursor, uid, wz_id_mod, ctx)
        wz_crear_mc_obj.onchange_duracio(
            cursor, uid, [wz_id_mod], wiz_mod.data_inici, wiz_mod.duracio, ctx
        )
        wiz_mod.write({
            'data_inici': start,
            'data_final': end
        })
        wiz_mod.action_crear_contracte(ctx)

    def test_check_sql_results(self):
        pool = self.openerp.pool
        self.create_policies(self.cursor, self.uid)

        expected = [0, 2, 0]

        wiz_obj = pool.get('wizard.control.gestio.comer.files')
        vals = {'data_inici': '2019-10-01',
                'data_final': '2019-10-31',
                'file': '',
                'file_name': '',
                'state': 'init'}
        wiz_id = wiz_obj.create(
            self.cursor, self.uid, vals, context=None
        )
        wiz_obj.browse(self.cursor, self.uid, wiz_id)
        results = wiz_obj.export_file(self.cursor, self.uid, [wiz_id], context=None)

        self.assertEqual(results, expected)
