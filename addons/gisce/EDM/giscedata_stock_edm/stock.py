# -*- encoding: utf-8 -*-

from osv import fields, osv


class stock_move(osv.osv):

    _name = "stock.move"
    _inherit = "stock.move"

    def onchange_product_id(self, cr, uid, ids, prod_id=False, address_id=False,
                            picking_type='default'):
        """
        When product changed and parameter <prod_id> is filled → Name,
        Product UOM, Product UOS and Product UOS quantity filled automatically.
        Product quantity set to 1.
        Adds domain to the source location: only can be selected those locations
        which have the product.
        :param cr: 
        :param uid:
        :param ids:
        :param prod_id:
        :param address_id:
        :param picking_type: Possible values: default, in, out, internal.
        :return:
        """
        location_o = self.pool.get('stock.location')

        template = super(stock_move, self).onchange_product_id(
            cr, uid, ids, prod_id, address_id
        )
        if prod_id and picking_type != 'in':
            product_o = self.pool.get('product.product')
            location_ids = product_o.get_locations(cr, uid, prod_id)
            location_ids = location_ids[prod_id]

            template['domain'].update({
                'location_id': [('id', 'in', location_ids)]
            })
        elif not prod_id:
            location_ids = location_o.search(cr, uid, [])

            location_dmn = ('id', 'in', location_ids)
            prodlot_dmn = ('id', 'in', [])

            if template.get('domain', False):
                if template['domain'].get('location_id', False):
                    template['domain']['location_id'].append(location_dmn)
                else:
                    template['domain'].update({'location_id': [location_dmn]})

                if template['domain'].get('prodlot_id', False):
                    template['domain']['prodlot_id'].append(prodlot_dmn)
                else:
                    template['domain'].update({'location_id': [prodlot_dmn]})
            else:
                template['domain'] = {
                    'location_id': [location_dmn],
                    'prodlot_id': [prodlot_dmn]
                }

        return template

    def onchange_location_id(self, cursor, uid, ids, location_id, product_id,
                             picking_type='default', context=None):
        """
        Adds domain to prodlot_id field: only shown production lots which are
        in the selected location.
        :param cursor:
        :param uid:
        :param ids:
        :param location_id:
        :param product_id:
        :param prodlot_id:
        :param picking_type:
        :param context:
        :return:
        """
        prodlot_o = self.pool.get('stock.production.lot')

        template = super(stock_move, self).onchange_location_id(
            cursor, uid, ids, location_id, product_id,
            picking_type=picking_type, context=context
        )

        dmn = [('product_id', '=', product_id)]
        prodlot_ids = prodlot_o.search(cursor, uid, dmn, context=context)

        if product_id:
            if location_id:
                if prodlot_ids:
                    prodlot_stock_on_location = prodlot_o._get_stock(
                        cursor, uid, prodlot_ids, False, False,
                        context={'location_id': location_id}
                    )
                    prodlots_elegibles = []
                    for prodlot_id, qty_available in prodlot_stock_on_location.items():
                        if qty_available > 0:
                            prodlots_elegibles.append(prodlot_id)

                    template['domain'] = {
                        'prodlot_id': [('id', 'in', prodlots_elegibles)]
                    }
            else:
                template = {
                    'domain': {
                        'prodlot_id': [('id', 'in', prodlot_ids)]
                    }
                }

        return template

    def onchange_lot_id(self, cr, uid, ids, prodlot_id=False, product_qty=False,
                        loc_id=False, context=None):
        """
        Autocompletes the stock move location with the location of the selected
        production lot.
        :param cr:
        :param uid:
        :param ids:
        :param prodlot_id:
        :param product_qty:
        :param loc_id:
        :param context:
        :return:
        """
        prodlot_o = self.pool.get('stock.production.lot')
        location_o = self.pool.get('stock.location')

        template = super(stock_move, self).onchange_lot_id(
            cr, uid, ids, prodlot_id, product_qty, loc_id, context=context
        )
        if prodlot_id and not loc_id:
            prodlot_v = prodlot_o.read(
                cr, uid, prodlot_id, ['location_id'], context=context
            )
            location_id = prodlot_v['location_id']
            if location_id:
                value = {'location_id': location_id}
                if template.get('value', False):
                    template['value'].update(value)
                else:
                    template['value'] = value
            else:
                default_locations = ['Red de Distribución', 'Inventory loss', 'Suppliers']
                default_locations_dmn = [('name', 'in', default_locations)]
                default_locations = location_o.search(
                    cr, uid, default_locations_dmn, context=context
                )

                dmn_value = ('id', 'in', default_locations)
                if template.get('domain', False):
                    if template['domain'].get('location_id', False):
                        template['domain']['location_id'].append(dmn_value)
                    else:
                        template['domain'].update({'location_id': [dmn_value]})
                else:
                    template['domain'] = {'location_id': [dmn_value]}

        return template


stock_move()


class stock_location(osv.osv):
    _name = "stock.location"
    _inherit = "stock.location"

    def _product_reserve_in_locations(self, cursor, uid, ids, context=None):
        return ids


stock_location()


class stock_picking(osv.osv):
    _name = "stock.picking"
    _inherit = "stock.picking"

    def __init__(self, pool, cr):
        self.supplier_packing_name_str = None
        self.views_load_sequence = []
        super(stock_picking, self).__init__(pool, cr)

    def fields_view_get(self, cursor, uid, view_id=None, view_type='form',
                        context=None, toolbar=False):
        res = super(stock_picking, self).fields_view_get(
            cursor, uid, view_id, view_type, context=context, toolbar=toolbar
        )
        # only the first view tells what picking type it is
        if 'supplier_packing_name' in res['fields']:
            self.views_load_sequence.append(view_type)
            if self.views_load_sequence[0] == 'form':
                # if the first view is a form, then update label
                if 'stock.picking.internal.' in res['name']:
                    self.supplier_packing_name_str = 'Motivo'
                elif 'stock.picking.out.' in res['name']:
                    self.supplier_packing_name_str = 'Trabajo'
                elif res['name'] not in ('stock.picking.tree', 'stock.picking.form'):
                    self.supplier_packing_name_str = res['fields']['supplier_packing_name']['string']
                self.views_load_sequence = []
            else:
                if len(self.views_load_sequence) == 1:
                    # only the first view tells what picking type it is
                    view_name = res['name']
                    if 'stock.picking.internal.' in view_name:
                        self.supplier_packing_name_str = 'Motivo'
                    elif 'stock.picking.out.' in view_name:
                        self.supplier_packing_name_str = 'Trabajo'
                    elif res['name'] not in ('stock.picking.tree', 'stock.picking.form'):
                        self.supplier_packing_name_str = res['fields']['supplier_packing_name']['string']
                    elif view_name == 'stock.picking.tree' and self.views_load_sequence[0] == 'tree':
                        self.supplier_packing_name_str = 'Albarán proveedor/Motivo/Trabajo'
                        self.views_load_sequence = []

                elif len(self.views_load_sequence) == 3:
                    self.views_load_sequence = []

            res['fields']['supplier_packing_name']['string'] = self.supplier_packing_name_str

        return res


stock_picking()
