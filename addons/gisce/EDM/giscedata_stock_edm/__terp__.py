# -*- coding: utf-8 -*-
{
    "name": "GISCE-EDM Stock",
    "description": """Extensió del mòdul giscedata_stock per a EDM.""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE",
    "depends":[
        "giscedata_stock",
        "giscedata_stock_location",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_stock_edm_view.xml",
        "giscedata_stock_edm_report.xml",
    ],
    "active": False,
    "installable": True
}
