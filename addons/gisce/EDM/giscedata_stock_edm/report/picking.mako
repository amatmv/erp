## coding=utf-8
<%inherit file="/giscedata_stock/report/picking.mako"/>
<%def name="datos_del_albaran(picking, picking_date, picking_shipping_date)">
    <%
        supplier_packing_name_str = _("Albarán proveedor")
        if picking.type == 'internal':
            supplier_packing_name_str = 'Motivo'
        elif picking.type == 'out':
            supplier_packing_name_str = 'Trabajo'
    %>
    <table id="datos-del-albaran">
        <colgroup>
           <col span="1" style="width: 50%;">
           <col span="1" style="width: 50%;">
        </colgroup>
        <thead>
            <tr>
                <th colspan="2">${_(u"Datos del albarán")}</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>${_("Albarán")}:</td>
                <td>${picking.name}</td>
            </tr>
            <tr>
                <td>${supplier_packing_name_str}:</td>
                <td>${picking.supplier_packing_name or ''}</td>
            </tr>
            <tr>
                <td>${_("Fecha del albarán")}: </td>
                <td>${picking_date}</td>
            </tr>
            <tr>
                <td>${_("Fecha de envio prevista")}: </td>
                <td>${picking_shipping_date}</td>
            </tr>
        </tbody>
    </table>
</%def >