# -*- coding: utf-8 -*-
{
    "name": "Account chart distri EDM",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends": [
        "base",
        "l10n_chart_ES",
        "account",
        "account_chart_update",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "account_chart_data.xml",
    ],
    "active": False,
    "installable": True
}
