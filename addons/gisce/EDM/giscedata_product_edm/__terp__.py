# -*- coding: utf-8 -*-
{
    "name": "Extension for products (EDM)",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Adds a sequence for EDM materials
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "product",
        "base_extended"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "product_data.xml",
    ],
    "active": False,
    "installable": True
}
