# -*- coding: utf-8 -*-

from osv import osv, fields
from osv.orm import OnlyFieldsConstraint, ValidateException
from ast import literal_eval


class product_product(osv.osv):

    _name = 'product.product'
    _inherit = 'product.product'

    def update_material_seq(self, cursor, uid, context=None):
        imd_obj = self.pool.get('ir.model.data')
        seq_obj = self.pool.get('ir.sequence')
        seq_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_ot_stock', 'seq_ot_materials'
        )[1]
        seq_prefix = seq_obj.read(cursor, uid, seq_id, ['prefix'])['prefix']
        cursor.execute('''
        SELECT MAX(default_code)
        FROM product_product
        WHERE default_code ilike %s
        ''', (seq_prefix+'%',))
        max_code = cursor.fetchone()[0]
        seq_obj.write(cursor, uid, [seq_id], {
            'number_next': int(max_code[len(seq_prefix):]) + 1
        })

    def get_material_categories(self, cursor, uid, context=None):
        imd_obj = self.pool.get('ir.model.data')
        categ_obj = self.pool.get('product.category')
        cat_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_product_edm', 'product_cat_materiales_ot'
        )[1]
        cat_ids = categ_obj.search(cursor, uid, [('id', 'child_of', [cat_id])])
        return cat_ids

    def check_material_name(self, cursor, uid, ids):
        cat_ids = self.get_material_categories(cursor, uid)
        for prod_id in self.read(cursor, uid, ids, ['code']):
            if self.search_count(
                    cursor, uid, [
                        ('default_code', '=', prod_id['code']),
                        ('product_tmpl_id.categ_id', 'in', cat_ids),
                    ]) > 1:
                return False
        return True

    def _next_material_code(
            self, cursor, uid, prod_id=None, category_id=False, context=None):
        if context is None:
            context = {}
        if isinstance(prod_id, (tuple, list)):
            prod_id = prod_id[0]
        if not category_id and prod_id:
            templ_obj = self.pool.get('product.template')
            templ_id = self.read(
                cursor, uid, prod_id, ['product_tmpl_id'])['product_tmpl_id'][0]
            category_id = templ_obj.read(
                cursor, uid, templ_id, ['categ_id'])['categ_id']
            if not category_id:
                return False
            category_id = category_id[0]

        cat_ids = self.get_material_categories(cursor, uid)
        if category_id in cat_ids:
            return self.pool.get('ir.sequence').get(
                cursor, uid, 'product.ot.materials')
        return False

    def create(self, cr, user, vals, context=None):
        if context is None:
            context = {}
        config_obj = self.pool.get('res.config')
        use_sequence = literal_eval(config_obj.get(
            cr, user, 'use_material_sequence_code', '0'))
        if use_sequence:
            # If duplicating, categ is in vals and must be checked on creation
            if 'categ_id' in vals:
                vals['default_code'] = self._next_material_code(
                    cursor=cr, uid=user,
                    category_id=vals['categ_id'], context=context
                ) or vals['default_code']
                try:
                    res_id = super(product_product, self).create(
                        cr, user, vals, context)
                except ValidateException as ex:
                    self.update_material_seq(cr, user)
                    vals['default_code'] = self._next_material_code(
                        cursor=cr, uid=user,
                        category_id=vals['categ_id'], context=context
                    ) or vals['default_code']
                    res_id = super(product_product, self).create(
                        cr, user, vals, context)
                finally:
                    return res_id
        # If creating, mat_code will be checked on write
        res_id = super(product_product, self).create(cr, user, vals, context)
        if not use_sequence:
            return res_id
        code = self._next_material_code(
            cursor=cr, uid=user, prod_id=res_id, context=context
        ) or vals['default_code']
        if code != vals['default_code']:
            try:
                self.write(cr, user, [res_id], {'default_code': code}, context)
            except ValidateException as ex:
                self.update_material_seq(cr, user)
                code = self._next_material_code(
                    cursor=cr, uid=user, prod_id=res_id, context=context
                ) or vals['default_code']
                self.write(cr, user, [res_id], {'default_code': code}, context)
        return res_id

    _constraints = [
        OnlyFieldsConstraint(check_material_name,
                             'El código de Material OT debe ser único',
                             ['default_code'])
    ]


product_product()
