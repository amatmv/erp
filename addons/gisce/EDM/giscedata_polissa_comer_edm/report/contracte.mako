<%inherit file="/giscedata_polissa_comer/report/contracte.mako"/>
<%block name="custom_css" >
    <link rel="stylesheet"
      href="${addons_path}/giscedata_polissa_comer_edm/report/stylesheet_EDM_contract.css"/>
</%block >
<%block name="text_capcalera">
    <p>
        ${_("www.electraenergia.es")}
    </p>
</%block>
<%block name="text_legal">
    <p>
        ${_("El abajo firmante solicita contratar con Electra Energía S.A.U. el "
            "suministro de electricidad, de acuerdo con las condiciones generales"
            " y de la información específica del producto que se acompañan como "
            "anexo al presente documento. A tal efecto, autoriza expresamente a "
            "esta empresa a contratar con su actual distribuidora, por si "
            "misma y asumiendo a todos los efectos la posición jurídica contractual"
            " del cliente, el Acceso a Terceros a la Red (ATR), así como a "
            "gestionar la resolución de su actual contrato, para que Electra Energía "
            "S.A.U. pase a ser su nuevo suministrador. Asimismo, autoriza a Electra "
            "Energía S.A.U., con el fin de poder llevar a cabo las "
            "mencionadas gestiones, a acceder al Sistema de Información de Puntos "
            "de Suministro de acuerdo con lo dispuesto en el artículo 7 del "
            "RD 1435/2002 de 27 de diciembre. El solicitante declara igualmente "
            "que los datos y la información reflejada en la presente solicitud "
            "son completos y veraces, autorizando a la mencionada empresa a "
            "efectuar las comprobaciones de los mismo, admitiendo que esta "
            "solicitud no implica la aceptación de Electra Energía, S.A.U. "
            "Inscrita en el Reg. Mercantil de Castellón, Tomo 1045, Libro 609, "
            "Folio 8, Sección 8, Hoja CS 16995, el 29-11-2000 - C.I.F. A12542296")}
    </p>
</%block>
<%block name="signatura_img">
    <img src="${addons_path}/giscedata_polissa_comer_edm/report/IMG/FirmaContrato_Leopoldo.png" >
</%block>
<%block name="signatura_firmant">
    ${_("CONSEJERO DELEGADO: Leopoldo J. Trillo-Figueroa Ygual")}
</%block>