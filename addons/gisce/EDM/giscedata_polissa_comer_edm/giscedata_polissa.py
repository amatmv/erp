# -*- coding: utf-8 -*-



from osv import osv, fields




class GiscedataPolissa(osv.osv):
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def copy_data(self, cursor, uid, id, default=None, context=None):
        if default is None:
            default = {}
        default_values = {
            'lot_facturacio': False,
            'data_ultima_lectura': False,
            'data_ultima_lectura_estimada': False,
            'versio_primera_factura': False,
            'data_alta': False,
            'data_firma_contracte': False,
        }
        default.update(default_values)
        res_id = super(
            GiscedataPolissa, self).copy_data(cursor, uid, id, default, context)
        return res_id

    _columns = {
        'descripcion_uso': fields.char(
            string='Descripción uso', size=64, readonly=True,
            write=['giscedata_polissa.group_giscedata_polissa_cutoff'],
            states={
                'esborrany': [('readonly', False)],
                'validar': [('readonly', False)],
                'modcontractual': [('readonly', False)]
            }
        )
    }


GiscedataPolissa()
