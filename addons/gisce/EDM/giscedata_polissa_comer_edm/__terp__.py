# -*- coding: utf-8 -*-
{
    "name": "Extension for polissa (EDM)",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Adds a report for EDM contract
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "c2c_webkit_report",
        "giscedata_polissa",
        "giscedata_polissa_comer",
        "giscedata_polissa_condicions_generals",
        "report_joiner"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_polissa_comer_edm_report.xml",
        "giscedata_polissa_view.xml"
    ],
    "active": False,
    "installable": True
}
