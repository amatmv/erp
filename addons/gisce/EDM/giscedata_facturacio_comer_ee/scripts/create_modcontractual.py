# -*- encoding: utf-8 -*-
import click
from csv import reader
from erppeek import Client
from collections import namedtuple
from tqdm import tqdm
from datetime import datetime


@click.command()
@click.option('-f', '--fromcsv', type=str, required=True, show_default=True,
              help='Succesful policies identifiers which m1-01 were generated')
@click.option('-s', '--server',
              default='http://localhost', help=u'ERP address', required=True)
@click.option('-p', '--port',
              default=8069, help='ERP port', type=click.INT, required=True)
@click.option('-u', '--user', default='admin', help='ERP user', required=True)
@click.option('-w', '--password', prompt=True, confirmation_prompt=False,
              hide_input=True, help='ERP user password')
@click.option('-d', '--database', help='Database name', required=True)
@click.option('-k', '--from', required=True,
              help='Search creation date M1-02 from given date. '
                   'Format: yyyy-mm-dd. The interval is 1 day')
@click.option('-l', '--logfilepath', help='Log file path',
              default='./comer_create_modcon.txt')
def change_cnaes_creating_modcontractuals(**kwargs):
    def count_lines(filepath):
        with open(filepath, 'r') as file_to_read:
            return len(file_to_read.readlines())

    def import_from_csv(filepath, separator=','):
        print ("Creating modcons started...")
        total = count_lines(filepath)
        with open(filepath, 'r') as csvfile:
            csvreader = reader(csvfile, delimiter=separator)
            for vals in tqdm(csvreader, desc='Modcons created', total=total):
                row = Row(*vals)
                parse_line(row)
        print ("Creating modcons ended")

    def parse_line(row):
        pol_id = polissa_obj.search([('name', '=', row.num_contrato)])
        if pol_id:
            if pol_id[0] in pols_m102:
                cnae_id = cnae_obj.search([('name', '=', row.cnae)])
                if cnae_id:
                    # -- Crear modificació contractual amb data: avui.
                    pol_state = polissa_obj.read(pol_id[0], ['state'])['state']
                    if pol_state == 'activa':
                        try:
                            polissa_obj.send_signal(pol_id, 'modcontractual')
                            polissa_obj.write(pol_id, {'cnae': cnae_id[0]})
                            polissa_obj.create_contracte(pol_id, data)
                            polisses_success.append(row.num_contrato)
                        except Exception as e:
                            polissa_obj.send_signal(pol_id, 'undo_modcontractual')
                            message = e.faultCode
                            if not message:
                                message = e.message
                            polisses_error.append(
                                (row.num_contrato, message.replace("\n", "")))
                    else:
                        polisses_error.append(
                            (row.num_contrato, "polissa no activa")
                        )
                else:
                    incorrect_cnae.append(row.cnae)
            else:
                polisses_warning.append(
                    "{} is not in the success file from the first script".format(
                        row.num_contrato
                    )
                )
        else:
            polisses_dont_exist.append(row.num_contrato)

    path = kwargs['fromcsv']
    server = kwargs['server']  # 'http://localhost'
    port = kwargs['port']  # '8069'
    database = kwargs['database']
    user = kwargs['user']
    password = kwargs['password']
    logfilepath = kwargs['logfilepath']
    from_date = kwargs['from']

    server_name = '{}:{}'.format(server, port)
    c = Client(server=server_name, db=database, user=user, password=password)
    print ("Connected succsessfully")

    HEADERS = [
        'num_contrato',
        'cnae'
    ]
    Row = namedtuple('CSVRow', field_names=HEADERS)

    polissa_obj = c.model('giscedata.polissa')
    cnae_obj = c.model('giscemisc.cnae')
    sw_obj = c.model('giscedata.switching')

    data = datetime.strftime(datetime.now().date(), '%Y-%m-%d')
    polisses_dont_exist = []
    polisses_success = []
    polisses_error = []
    polisses_warning = []
    incorrect_cnae = []

    sw_ids = sw_obj.search([
        ('proces_id.name', '=', 'M1'), ('date', '>=', from_date),
        ('step_id.name', '=', '02')
    ])
    sw_vals = sw_obj.read(sw_ids, ['cups_polissa_id'])
    pols_m102 = {v['cups_polissa_id'][0] for v in sw_vals}

    import_from_csv(path)

    log = "-------------------STATS-------------------\n"
    log += "ERROR: The following contracts does not exist (x{}):\n".format(
        len(polisses_dont_exist))
    for contract in polisses_dont_exist:
        log += "\t{}\n".format(contract)

    log += "ERROR: The following CNAE are incorrect (x{}):\n".format(
        len(incorrect_cnae))
    for cnae in incorrect_cnae:
        log += "\t{}\n".format(cnae)

    log += "ERROR: The new modcon couldn't be created for the following contracts (x{}):\n".format(
        len(polisses_error))
    for num_contr, descr in polisses_error:
        log += "\t{} : {}\n".format(num_contr, descr)

    log += "WARNING: (x{}):\n".format(
        len(polisses_warning))
    for descr in polisses_warning:
        log += "\t{}\n".format(descr)

    log += "SUCCESS: The following contracts have a new modcon (x{})\n".format(
        len(polisses_success))
    for contract in polisses_success:
        log += "\t{}\n".format(contract)

    with open(logfilepath, 'w') as log_file:
        log_file.write(log)
    print ("Script finished")


if __name__ == '__main__':
    change_cnaes_creating_modcontractuals()
