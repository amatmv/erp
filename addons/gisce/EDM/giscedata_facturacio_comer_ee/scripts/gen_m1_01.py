# -*- encoding: utf-8 -*-
import click
import csv
from csv import reader
from erppeek import Client
from collections import namedtuple
from tqdm import tqdm


def clean_tel_number(tel_number):
    '''
    Strips non-number chars from telephone number except "+"
    Copied function from giscedata_switching
    '''
    res = {'tel': tel_number, 'pre': '', 'fuzzy': False}
    if not tel_number:
        return res
    tel_net = ''.join([c for c in tel_number if c.isdigit()])
    tel = tel_net
    pre = '34'
    fuzzy = False
    if len(tel_net) > 9:
        pre = tel_net[:-9][-2:]
        tel = tel_net[-9:]
        fuzzy = len(tel) + len(pre) < len(tel_net)

    res = {'tel': tel, 'pre': pre, 'fuzzy': fuzzy}
    return res


def create_m1_01_changing_cnae(c, polissa_id, cnae_id):
    """

    :param c:
    :type c: erppeek client
    :param polissa_id:
    :param cnae_id:
    :return:
    """

    def get_phone_number(partner_id):
        addr_ids = addr_obj.search(
            [('partner_id', '=', partner_id),
             '|', ('phone', '!=', ''), ('mobile', '!=', '')]
        )
        phone = '000000000'
        if addr_ids:
            numbers = addr_obj.read(
                addr_ids, ['phone', 'mobile']
            )
            for number in numbers:
                if number['phone'] and len(number['phone']) > 5:
                    phone = number['phone']
                    break
                elif len(number['mobile']) > 5:
                    phone = number['mobile']
                    break

        tel_str = clean_tel_number(phone)
        res = {}
        res.update({'phone_num': tel_str['tel']})
        if tel_str['pre']:
            res.update({'phone_pre': tel_str['pre']})
        return res

    wizard_m1_01 = c.model('giscedata.switching.mod.con.wizard')
    polissa_obj = c.model('giscedata.polissa')
    addr_obj = c.model('res.partner.address')

    wiz = wizard_m1_01.create(
        {}, {'cas': 'M1', 'pol_id': polissa_id}
    )
    pol_titular_id = polissa_obj.read(polissa_id, ['titular'])['titular'][0]
    phone_d = get_phone_number(pol_titular_id)
    wiz_vals = {
        'owner': pol_titular_id,            # to avoid change owner accidentally
        'skip_validations': True,           # to avoid raise of unchanged owner
        'change_atr': False,
        'change_adm': True,
        'owner_change_type': 'A',
        'cnae': cnae_id,
        'phone_num': phone_d['phone_num'],
        'phone_pre': phone_d['phone_pre'],
        'activacio_cicle': 'A'
    }
    wiz.write(wiz_vals)
    wiz.genera_casos_atr()
    return wiz.info


@click.command()
@click.option('-f', '--fromcsv', help='CSV file path', type=str, required=True,
              show_default=True)
@click.option('-s', '--server',
              default='http://localhost', help=u'ERP address', required=True)
@click.option('-p', '--port',
              default=8069, help='ERP port', type=click.INT, required=True)
@click.option('-u', '--user', default='admin', help='ERP user', required=True)
@click.option('-w', '--password', prompt=True, confirmation_prompt=False,
              hide_input=True, help='ERP user password')
@click.option('-d', '--database', help='Database name', required=True)
@click.option('-l', '--logfilepath', help='Log file path',
              default='./update_cnaes_log.txt')
@click.option('-m', '--succesful_policies',
              help='Succesful policies identifiers which m1-01 were generated',
              default='./policies_m1_01.csv')
def change_cnaes_creating_modcontractuals(**kwargs):
    def count_lines(filepath):
        with open(filepath, 'r') as file_to_read:
            return len(file_to_read.readlines())

    def import_from_csv(filepath, separator=';'):
        print ("Counting started...")
        total = count_lines(filepath)
        with open(filepath, 'r') as csvfile:
            csvreader = reader(csvfile, delimiter=separator)
            for vals in tqdm(csvreader, desc='Counted:', total=total):
                row = Row(*vals)
                parse_line(row)
        print ("Counting ended.")

    def parse_line(row):
        dmn = [('name', '=', row.num_contrato)]
        polissa_id = polissa_obj.search(dmn, context={'active_test': False})
        if polissa_id:
            cups_id = cups_obj.search([('polissa_polissa', '=', polissa_id[0])])
            if cups_id:
                if len(row.cnae) == 4:
                    cnae_id = cnae_obj.search([('name', '=', row.cnae)])
                    if cnae_id:
                        pol_cols = ['cnae', 'state', 'distribuidora']
                        pol_vals = polissa_obj.read(polissa_id, pol_cols)[0]
                        pol_state = pol_vals['state']
                        if pol_state == 'activa':
                            pol_cnae = pol_vals['cnae']
                            pol_distri = pol_vals['distribuidora'][0]
                            if not pol_cnae or pol_cnae[0] != cnae_id[0]:
                                if pol_cnae and '9821' in pol_cnae[1]:
                                    non_existent_cnae = "the current CNAE isn't valid (9821)"
                                    if row.cnae == '9820':
                                        non_existent_cnae += " and possibly the change shouldn't be done"
                                    polisses_warning.append({
                                        'policy': row.num_contrato,
                                        'message': non_existent_cnae
                                    })

                                # contracts to be migrated
                                if pol_distri == self_distri_id:
                                    result = create_m1_01_changing_cnae(
                                        c, polissa_id[0], cnae_id[0]
                                    )
                                    if "ERROR" in result.upper():
                                        error = (row.num_contrato, result)
                                        error_migration.append(error)
                                    else:
                                        success = (row.num_contrato, result)
                                        success_migration.append(success)
                                        to_create_modcon = (row.num_contrato, row.cnae)
                                        policies_m1_01.append(to_create_modcon)
                                else:
                                    polisses_warning.append({
                                        'policy': row.num_contrato,
                                        'message': "the distri is external"
                                    })
                            else:
                                not_affected = (row.num_contrato, row.cnae)
                                polisses_not_affected.append(not_affected)
                        else:
                            polisses_non_active.append({
                                'policy': row.num_contrato,
                                'message': "the policy's state is {}".format(pol_state)
                            })
                    else:
                        polisses_error.append((
                            row.num_contrato,
                            'CNAE {} does not exist.'.format(row.cnae)
                        ))
                else:
                    polisses_error.append((
                        row.num_contrato,
                        'CNAE {} is not valid (lenght must be 4).'.format(
                            row.cnae)
                    ))
            else:
                polisses_error.append((row.num_contrato, "CUPS does not match"))
        else:
            error = (row.num_contrato, 'contract does not exist.')
            polisses_error.append(error)

    path = kwargs['fromcsv']
    server = kwargs['server']  # 'http://localhost'
    port = kwargs['port']  # '8069'
    database = kwargs['database']
    user = kwargs['user']
    password = kwargs['password']
    logfilepath = kwargs['logfilepath']
    policiespath = kwargs['succesful_policies']

    server_name = '{}:{}'.format(server, port)
    c = Client(server=server_name, db=database, user=user, password=password)
    print ("Connected succsessfully")

    HEADERS = [
        'num_contrato',
        'cups',
        'cnae',
    ]
    Row = namedtuple('CSVRow', field_names=HEADERS)

    cnae_obj = c.model('giscemisc.cnae')
    cups_obj = c.model('giscedata.cups.ps')
    polissa_obj = c.model('giscedata.polissa')
    partner_obj = c.model('res.partner')
    self_distri_id = partner_obj.search([('name', '=', 'MAESTRAZGO DISTRIBUCIÓN ELÉCTRICA, S.L.U.')])[0]

    polisses_error = []
    polisses_warning = []
    polisses_not_affected = []
    polisses_non_active = []
    success_migration = []
    error_migration = []
    policies_m1_01 = []
    import_from_csv(path)

    log = "-------------------STATS-------------------\n"
    log += "ERROR: The following contracts are not valid (x{}):\n" \
           "[num policy, descr]\n".format(len(polisses_error))
    for polissa_error in polisses_error:
        log += "\t{} : {}\n".format(str(polissa_error[0]), polissa_error[1])

    log += "ERROR: The M1-01 for the following contracts were not created (x{}):\n" \
           "[policy, descr]\n".format(len(error_migration))
    for m1_not_generated in error_migration:
        log += "\t{} : {}".format(str(m1_not_generated[0]), m1_not_generated[1])

    log += "WARNING: {} contracts won't be modified.\n".format(len(polisses_not_affected))
    log += "WARNING: (x{}):\n[policy, descr]\n".format(len(polisses_warning) + len(polisses_non_active))
    for polissa_non_active in polisses_non_active:
        log += "\t{policy}: {message}\n".format(**polissa_non_active)
    for polissa_warning in polisses_warning:
        log += "\t{policy}: {message}\n".format(**polissa_warning)

    log += "SUCCESS: generated M1-01 (x{}):\n" \
           "[policy, descr]\n".format(len(success_migration))
    for m1_generated in success_migration:
        log += "\t{}: {}".format(m1_generated[0], m1_generated[1])

    with open(logfilepath, 'w') as log_file:
        log_file.write(log)
    with open(policiespath, 'w') as policies_file:
        csv_out = csv.writer(policies_file)
        for row in policies_m1_01:
            csv_out.writerow(row)


if __name__ == '__main__':
    change_cnaes_creating_modcontractuals()
