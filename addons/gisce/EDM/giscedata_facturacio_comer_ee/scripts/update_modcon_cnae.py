# -*- encoding: utf-8 -*-
import click
from erppeek import Client
from tqdm import tqdm


@click.command()
@click.option('-s', '--server',
              default='http://localhost', help=u'ERP address', required=True)
@click.option('-p', '--port',
              default=8069, help='ERP port', type=click.INT, required=True)
@click.option('-u', '--user', default='admin', help='ERP user', required=True)
@click.option('-w', '--password', prompt=True, confirmation_prompt=False,
              hide_input=True, help='ERP user password')
@click.option('-d', '--database', help='Database name', required=True)
@click.option('-f', '--from', required=True,
              help='Search creation date M1-01 from given date. '
                   'Format: yyyy-mm-dd. The interval is 1 day')
@click.option('-l', '--logfilepath', help='Log file path',
              default='./distri_update_modcon.txt')
def change_cnaes_updating_modcontractuals(**kwargs):

    server = kwargs['server']  # 'http://localhost'
    port = kwargs['port']  # '8069'
    database = kwargs['database']
    user = kwargs['user']
    password = kwargs['password']
    from_date = kwargs['from']
    logfilepath = kwargs['logfilepath']

    server_name = '{}:{}'.format(server, port)
    c = Client(server=server_name, db=database, user=user, password=password)
    print ("Connected succsessfully")

    pol_obj = c.model('giscedata.polissa')
    m1_01_obj = c.model('giscedata.switching.m1.01')
    sw_obj = c.model('giscedata.switching')

    m1_01_ids = m1_01_obj.search(
        [('date_created', '>', from_date),
         ('header_id.sw_id.step_id.name', '=', '01')]
    )
    m1_01_vals = m1_01_obj.read(m1_01_ids, ['sw_id', 'cnae'])
    sw_ids = [x['sw_id'][0] for x in m1_01_vals]

    sw_vals = {sw['id']: sw['cups_polissa_id'][0] for sw in sw_obj.read(sw_ids, ['cups_polissa_id'])}
    pol_name = {pol['id']: pol['name'] for pol in pol_obj.read(sw_vals.values(), ['name'])}

    pol_success = []
    pol_error = []

    for m1_01 in tqdm(m1_01_vals):
        m1_01_sw_id = m1_01['sw_id'][0]
        pol_id = sw_vals[m1_01_sw_id]
        try:
            pol_obj.send_signal([pol_id], 'modcontractual')
            pol_obj.write(pol_id, {'cnae': m1_01['cnae'][0]})
            pol_obj.modify_contracte(pol_id)
            pol_success.append(pol_name[pol_id])
        except Exception as e:
            pol_obj.send_signal([pol_id], 'undo_modcontractual')
            message = e.faultCode
            if not message:
                message = e.message
            pol_error.append((pol_name[pol_id], message.replace("\n", "")))

    log = "-------------------STATS-------------------\n"
    log += "ERROR: couldn't update the modcon for the next contracts(x{}):\n".format(len(pol_error))
    for num_contr, descr in pol_error:
        log += "\t{} : {}\n".format(num_contr, descr)

    log += "SUCCESS: updated succesfully the modcon for the next contracts(x{}):\n".format(len(pol_success))
    for num_contr in pol_success:
        log += "\t{}\n".format(num_contr)

    with open(logfilepath, 'w') as log_file:
        log_file.write(log)
    print ("Script finished")


if __name__ == '__main__':
    change_cnaes_updating_modcontractuals()
