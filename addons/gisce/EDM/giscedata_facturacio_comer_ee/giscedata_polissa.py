# -*- coding: utf-8 -*-
from osv import fields, osv
from datetime import datetime, timedelta


ZONA_CARTA = 'ENVIO E-MAIL'


class GiscedataPolissa(osv.osv):
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def get_polisses_force_process_id(self, cursor, uid, ids, field_name, args,
                                      context=None):
        """
        Si el contrato está como no cortable tipificado por el BOE, el proceso
        de corte debe ser el No Cortable
        """
        res = super(GiscedataPolissa, self).get_polisses_force_process_id(
            cursor, uid, ids, field_name, args, context=context
        )

        polisses_ids_no_cortables = self.search(
            cursor, uid, [('nocutoff.boe_check', '=', True), ('id', 'in', ids)]
        )

        if polisses_ids_no_cortables:
            imd_obj = self.pool.get('ir.model.data')
            nocutoff_process_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_ee',
                'no_cortable_pending_state_process'
            )[1]

            polisses_no_cortables_dict = dict.fromkeys(
                polisses_ids_no_cortables, nocutoff_process_id
            )

            res.update(polisses_no_cortables_dict)

        return res

    def wrapper_constraint_process_id(self, cursor, uid, ids):
        return True

    def on_change_nocutoff(self, cursor, uid, ids, nocutoff, context=None):
        res = {'value': {}, 'domain': {}, 'warning': {}}

        if nocutoff:
            imd_obj = self.pool.get('ir.model.data')
            nocutoff_process_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_ee',
                'no_cortable_pending_state_process'
            )[1]
            res['value'].update({'process_id': nocutoff_process_id})

        return res

    def extraer_tarifa_sinpapel(self, cursor, uid, poliza_id, context=None):
        poliza_obj = self.pool.get('giscedata.polissa')
        poliza_datos = poliza_obj.browse(cursor, uid, poliza_id)
        fecha_ultima_lectura = poliza_datos.data_ultima_lectura
        fecha_act_modcon = poliza_datos.modcontractual_activa.data_inici
        if fecha_act_modcon >= fecha_ultima_lectura:
            fecha_activacion = datetime.today()
        else:
            fecha_activacion = datetime.strptime(
                fecha_ultima_lectura, '%Y-%m-%d') + timedelta(days=1)
        search_params = [
            ('tarifes_atr_compatibles', '=', poliza_datos.tarifa.id),
            ('type', '=', 'sale'),
            ('name', 'ilike', 'sin papel')]
        price_list_obj = self.pool.get('product.pricelist')
        price_list_id = price_list_obj.search(cursor, uid, search_params)
        potencia = poliza_datos.potencia
        modo_facturacion_pvpc = poliza_datos.mode_facturacio == 'pvpc'
        if price_list_id and modo_facturacion_pvpc and potencia <= 15:
            if len(price_list_id) > 1:
                raise osv.except_osv(
                    'Error multiples tarifas sin papel',
                    'Existe mas de una tarifa '
                    'sin papel para el contrato {} ({})'.format(poliza_datos.name, poliza_id))
            else:
                return fecha_activacion, price_list_id[0],
        else:
            return datetime.today(), None

    def activar_envio_mail(self, cursor, uid, poliza_id, email, context=None):
        if context is None:
            context = {}
        poliza_obj = self.pool.get('giscedata.polissa')
        poliza_datos = poliza_obj.browse(cursor, uid, poliza_id)
        fecha_activacion, lista_precios_actual = self.extraer_tarifa_sinpapel(
            cursor, uid, poliza_id
        )
        poliza_obj.send_signal(
            cursor, uid, [poliza_id], 'modcontractual'
        )
        vals = {}
        if lista_precios_actual:
            vals['llista_preu'] = lista_precios_actual
        zona_carta = self.pool.get('giscedata.polissa.carta.zona')
        zona_id = zona_carta.search(cursor, uid, [('name', '=', ZONA_CARTA)])
        if not zona_id:
            raise osv.except_osv(
                'Imposible efectuar operacion',
                'No se ha encontrado una zona de carta válida para envio por '
                'email'
            )

        vals.update({
            'enviament': 'email',
            'zona_carta_id': zona_id[0],
            'ordre_carta': '99'
        })
        poliza_datos.write(vals)

        poliza_datos.direccio_notificacio.write({'email': email})

        self.activar_polissa(cursor, uid, poliza_id, fecha_activacion)

    def activar_polissa(self, cursor, uid, poliza_id, fecha_activacion,
                        context=None):
        try:
            wz_crear_mc_obj = self.pool.get('giscedata.polissa.crear.contracte')
            ctx = {'active_id': poliza_id, 'lang': 'es_ES'}
            params = {'duracio': 'nou', 'data_inici': fecha_activacion.strftime('%Y-%m-%d')}

            wz_id = wz_crear_mc_obj.create(cursor, uid, params, context=ctx)
            wiz = wz_crear_mc_obj.browse(cursor, uid, wz_id)
            res = wz_crear_mc_obj.onchange_duracio(
                cursor, uid, [wiz.id], wiz.data_inici, wiz.duracio, context=ctx)
            obs = wiz.observacions
            obs = u'Activación envío por email y, si es posible, ' \
                  u'activación factura sin papel\n\n' + obs
            wz_crear_mc_obj.write(cursor, uid, [wiz.id], {'observacions': obs})
            if res.get('warning', False):
                params = {'accio': 'modificar'}
                wz_id = wiz.write(params)
                res = wz_crear_mc_obj.onchange_duracio(
                    cursor, uid, [wiz.id], wiz.data_inici, wiz.duracio, context=ctx)
                wz_crear_mc_obj.action_crear_contracte(
                    cursor, uid, [wiz.id], context=ctx)
            else:
                wz_crear_mc_obj.write(
                    cursor, uid, [wiz.id], {'data_final': res['value']['data_final']})
                wz_crear_mc_obj.action_crear_contracte(
                    cursor, uid, [wiz.id], context=ctx)

        except Exception as e:
            raise osv.except_osv('Imposible efectuar operacion',
                                 'No se ha podido generar la modificación contractual ({}){}'.format(poliza_id, e))


GiscedataPolissa()
