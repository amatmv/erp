# -*- coding: utf-8 -*-
from osv import osv, fields
from ast import literal_eval


class GiscedataFacturacioFactura(osv.osv):
    _name = 'giscedata.facturacio.factura'
    _inherit = 'giscedata.facturacio.factura'

    _columns = {
        'motivo_devolucion': fields.char(
            string=u'Motivo devolución', size=128, required=False
        )
    }

    def copy_data(self, cursor, uid, id, default=None, context=None):
        if default is None:
            default = {}
        if context is None:
            context = {}
        default.update({'motivo_devolucion': False})
        return super(GiscedataFacturacioFactura, self).copy_data(
            cursor, uid, id, default, context)

    def check_if_gen_non_payment_case(self, cursor, uid, factura, context=None):
        """
        No generar caso de impago si el contrato tiene el proceso de corte
        "No cortable"

        :param factura: Browse record de una factura.
        :type: Browse Record.
        """
        imd_obj = self.pool.get('ir.model.data')
        no_cutoff_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio_comer_ee',
            'no_cortable_pending_state_process'
        )[1]
        no_cortable = factura.polissa_id.process_id.id == no_cutoff_id
        if no_cortable:
            return False
        else:
            return super(
                GiscedataFacturacioFactura, self
            ).check_if_gen_non_payment_case(
                cursor, uid, factura, context=context
            )


GiscedataFacturacioFactura()


class GiscedataFacturacioDevolucio(osv.osv):
    _name = 'giscedata.facturacio.devolucio'
    _inherit = 'giscedata.facturacio.devolucio'

    def get_factura_devolucio_vals(self, cursor, uid, linia, context=None):
        """
        :param cursor:
        :param uid:
        :param devolucio: browse record of giscedata.facturacio.devolucio.linia
        :param context:
        :return: Devuelve los valores a escribir en una factura al cargar una
                 devolución
        """
        vals = super(
            GiscedataFacturacioDevolucio, self
        ).get_factura_devolucio_vals(cursor, uid, linia, context)

        vals.update({'motivo_devolucion': linia.reject_msg})
        return vals


GiscedataFacturacioDevolucio()


class GiscedataFacturacioSwitchingHelper(osv.osv):
    _name = 'giscedata.facturacio.switching.helper'
    _inherit = 'giscedata.facturacio.switching.helper'

    def get_values_in_ext(self, cursor, uid, fact_id, fact_name, ori_lines,
                              context=None):
        """Comprovem si alguna de les línies és de reposició
           no la carreguem perquè ja hem fet les factures
           previes de reconexió
        """
        values = super(
            GiscedataFacturacioSwitchingHelper, self
        ).get_values_in_ext(cursor, uid, fact_id, fact_name, ori_lines,
                            context)
        imd_obj = self.pool.get('ir.model.data')
        con13_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio_comer', 'concepte_13'
        )[1]
        new_values = []
        for vals in values:
            if vals['product_id'] != con13_id:
                new_values.append(vals)
        return new_values
        
GiscedataFacturacioSwitchingHelper()
