<%
    from datetime import datetime, timedelta
    from operator import attrgetter, itemgetter
    import json, re
    from giscedata_facturacio_comer.report.utils import get_giscedata_fact,get_other_info,get_discriminador,get_distri_phone,get_historics,get_linies_tipus,get_discounts
    from account_invoice_base.report.utils import localize_period

    pool = objects[0].pool
    polissa_obj = objects[0].pool.get('giscedata.polissa')
    tarifa_obj = objects[0].pool.get('giscedata.polissa.tarifa')

    def check_tax(tax, tipus):
        res = '-'
        if tipus is not None:
            if "IVA" in tax.name:
                res = tax.name.replace("IVA ", "")
                res = res.replace("%", "")
                res = "{} %".format(res)
        elif tax:
            if "IVA" in tax.name:
                res = "{} %".format(formatLang(tax.amount * 100, digits=0))
        return res

    def get_iva(linia, tipus=None):
        res  = '-'
        if tipus is not None:
            res = check_tax(linia, tipus)
        elif linia.invoice_line_tax_id:
            for tax in linia.invoice_line_tax_id:
                res = check_tax(tax, tipus)
        return res

    def get_texto_abr(inv):
        if inv.tipo_rectificadora == 'N':
            return ""
        elif inv.tipo_rectificadora in ['A','B', 'BRA']:
            accion = "anula"
        elif inv.tipo_rectificadora in ['R', 'RA']:
            accion = "rectifica"
        return "Esta factura {0} la factura {1} de fecha {2}".format(accion,inv.ref.number,inv.ref.date_invoice)
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
    	<style type="text/css">
            ${css}
            @font-face {
                font-family: 'Ubuntu bold';
                src: local('Ubuntu'), url(${addons_path}/giscedata_facturacio_comer_ee/report/fonts/Ubuntu-Bold.ttf) format('ttf');
            }
            @font-face {
                font-family: 'Ubuntu';
                src: local('Ubuntu'), url(${addons_path}/giscedata_facturacio_comer_ee/report/fonts/Ubuntu-Regular.ttf) format('ttf');
            }
            body{
                background: #ffffff;
                background-image: url(${addons_path}/giscedata_facturacio_comer_ee/report/img/imagen_fondo.png);
                background-repeat: repeat;
                background-position: left top;
                background-size: 100%;
            }
            #logo_superior_derecha_degradado{
                background-image: url(${addons_path}/giscedata_facturacio_comer_ee/report/img/degradado_superior.jpg);
                background-size: 115%;
                background-repeat: repeat-y;
            }
        </style>
        <link href="${addons_path}/giscedata_facturacio_comer_ee/report/estils_ee.css" rel="stylesheet">
        <link href="${addons_path}/giscedata_facturacio_comer_ee/report/consum.css" rel="stylesheet">
    </head>
<body>
    %for inv in objects :
        <%
            sentido = 1.0
            entidad_bancaria = ''
            bank_acc = ''
            polissa = polissa_obj.browse(cursor, uid, inv.polissa_id.id, context={'date': inv.data_final.val})
            direccio = polissa.cups.direccio
            dies_factura = 1 + (datetime.strptime(inv.data_final, '%Y-%m-%d') - datetime.strptime(inv.data_inici, '%Y-%m-%d')).days
            diari_factura_actual_eur = inv.total_energia / (dies_factura or 1.0)
            diari_factura_actual_kwh = (inv.energia_kwh * 1.0) / (dies_factura or 1.0)
            idx_pob = direccio.rfind('(')
            if idx_pob != -1:
                direccio = direccio[:idx_pob]
            if polissa.cups.id_poblacio:
                poblacio_cups = polissa.cups.id_poblacio.name
            else:
                poblacio_cups = polissa.cups.id_municipi.name
            try:
                cp = polissa.cups.dp
                direccio = direccio.replace(cp, '')
            except:
                raise Exception(
                    u'El CUPS no tiene definido el código postal'
                )
            cups = polissa.cups.name
            pot_periode = dict([('P%s' % p,'') for p in range(1, 7)])
            for pot in polissa.potencies_periode:
                pot_periode[pot.periode_id.name] = pot.potencia

            if inv.payment_type.code == 'RECIBO_CSB':
                if not inv.partner_bank:
                    raise Exception(
                        u'La factura {0} no tiene número de cuenta definido.'.format(
                        inv.number)
                    )
                else:
                    entidad_bancaria = inv.partner_bank.name or inv.partner_bank.bank.lname
                    bank_acc = inv.partner_bank.iban[:-4].replace(' ','')  + '****'
            else:
                if inv.partner_bank:
                    entidad_bancaria = inv.partner_bank.name or inv.partner_bank.bank.lname
                    bank_acc = inv.partner_bank.iban[:-4].replace(' ','') + '****'
            altres_lines = [l for l in inv.linia_ids]
            total_altres = sum([l.price_subtotal for l in altres_lines])
            factura = get_giscedata_fact(cursor, inv)[0]
            info = get_other_info(cursor, factura['id'])[0]
        %>
        <div id="contenido">
            <!-- Div superior donde se encuentran los logos-->
            <div id="cabecera">
                <div id="logo_superior_izq">
                    <img class="logo" src="${addons_path}/giscedata_facturacio_comer_ee/report/img/electraenergia_color_f_blanco_33.png" align="right">
                </div>
                <div id="logo_superior_derecha_degradado">
                    <div id="texto_degradado">
                        FACTURA DE CONCEPTOS
                    </div>
                </div>
            </div>
            <div id="texto_vertical_lateral">
                ${company.rml_footer2}
            </div>
            <div id="cabecera_datos_dir_envio">
                <div id="cabecera_datos_factura">
                    <div class="texto-vertical_azul_col1 ajustes_vertical_col1_cabecera">
                       DATOS DE LA FACTURA
                    </div>
                    <table class="tabla_datos_factura mt_5">
                        <tr>
                            <td>Nº FACTURA</td>
                            <td class="field">${inv.number or ''}</td>
                        </tr>
                        <tr>
                            <td>FECHA FACTURA</td>
                            <td class="field">${localize_period(inv.date_invoice, 'es_ES')}</td>
                        </tr>
                        <tr>
                            <td style="font-size: 12pt">TOTAL</td>
                            <td class="field f_21">${formatLang(sentido * inv.amount_total, digits=2)} €</td>
                        </tr>
                        <tr>
                            <td>DOMICILIACIÓN</td>
                            %if inv.payment_type.code == 'RECIBO_CSB':
                                <td class="field" style="font-size: 7pt;">${entidad_bancaria}</td>
                            %else:
                                <td class="field" style="font-size: 7pt;">PAGO POR TRANSFERENCIA</td>
                            %endif
                        </tr>
                        <tr>
                            <td>Nº DE CUENTA</td>
                            %if inv.payment_type.code == 'RECIBO_CSB':
                                <td class="field">${bank_acc}</td>
                            %else:
                                <td class="field">${inv.payment_mode_id and inv.payment_mode_id.bank_id.iban or inv.payment_type.note}</td>
                            %endif
                        </tr>
                        %if inv.payment_type.id == 2:
                            <tr>
                                <td>FORMA DE PAGO</td>
                                <td class="field">${inv.payment_term.name.upper()} FECHA FACTURA</td>
                            </tr>
                        %endif
                    </table>
                </div>
                <div id="cabecera_envio">
                    <div class="texto-vertical_azul_col2 ajustes_direccion_cliente">
                       DIRECCIÓN DEL CLIENTE
                    </div>
                    <table style="left: -18px;" class="tabla_datos_factura_derecha">
                        <tr>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="field f_10_5">
                                ${inv.address_contact_id.name or ''}
                            </td>
                        </tr>
                        <tr>
                            <td class="field f_10_5">
                                ${inv.address_contact_id.street or ''}
                            </td>
                        </tr>
                        <tr>
                            <td class="field f_10_5">
                                ${inv.address_contact_id.zip or ''} ${inv.address_contact_id.city or ''}
                            </td>
                        </tr>
                        <tr>
                            <td class="field f_10_5">
                                ${inv.address_contact_id.state_id.name.upper() or ''}
                            </td>
                        </tr>
                         %if inv.address_contact_id.country_id.code != 'ES':
                            <tr>
                                <td class="field f_10_5">
                                    ${inv.address_contact_id.country_id.name.upper() or ''}
                                </td>
                            </tr>
                        %endif
                    </table>
                </div>
            </div>
            <!-- Datos del titular -->
            <!-- Div datos de la factura y ademas los datos del cliente. -->
            <div style="clear: both;"></div>
            <div id="Cuerpo_DatosTitular">
                <div id="cabecera_datos_titular">
                    <div class="texto-vertical_azul_col1 ajustes_vertical_col1_titular">
                       DATOS DE SUMINISTRO
                    </div>
                    <table class="tabla_datos_factura mt_5">
                        <tr>
                            <td>TITULAR</td>
                            %if len(inv.partner_id.name.rstrip()) > 30:
                                <td class="field small_text">
                            %else:
                                <td class="field">
                            %endif
                            ${inv.partner_id.name or ''}</td>
                        </tr>
                        <tr>
                            <td>DIR. SUMINISTRO</td>
                            %if len(direccio) > 30:
                                <td class="field small_text">${direccio}</td>
                            %else:
                                <td class="field">${direccio}</td>
                            %endif
                        </tr>
                        <tr>
                            <td>POBLACIÓN</td>
                            <td class="field">${poblacio_cups}</td>
                        </tr>
                        <tr>
                            <td>DNI/CIF</td>
                            <td class="field">${(inv.partner_id.vat or '').replace('ES','')}</td>
                        </tr>
                        <tr>
                            <td><span style="white-space: nowrap;">REF. CATASTRAL</span></td>
                            <td class="field">${info['giscedata_cups_ps_ref_cat']}</td>
                        </tr>
                        <tr>
                            <td><span style="white-space: nowrap;">TARIFA ACCESO</span></td>
                            <td class="field">${polissa.tarifa.name or ''}</td>
                        </tr>
                    </table>
                </div>
                <div id="cabecera_datos_titular_derecha">
                    <table class="tabla_datos_factura" style="position: relative; float: left; margin-top: 16px;">
                        <tr>
                            <td>TENSIÓN</td>
                            <td class="field">${info['giscedata_polissa_tensio']}</td>
                        </tr>
                        <tr>
                            <td><span style="white-space: nowrap;">Nº CONTRATO</span></td>
                            <td class="field">${polissa.name}</td>
                        </tr>
                        <tr>
                            <td>CNAE</td>
                            <td class="field">${info['giscedata_polissa_cnae'] or ''}</td>
                        </tr>
                        <tr>
                            <td>CUPS</td>
                            <td class="field">${cups}</td>
                        </tr>
                        <tr>
                            <td>POTENCIA</td>
                            <td class="field">
                                <%
                                    primer = True
                                %>
                                % for periode, potencia in sorted(pot_periode.items(),key=itemgetter(0)):
                                    % if potencia:
                                        %if not primer:
                                            -
                                        %else:
                                            <%
                                                primer = False
                                            %>
                                        %endif
                                        ${periode}:
                                        ${formatLang(potencia, digits=3)}
                                    % endif
                                % endfor
                            </td>
                        </tr>
                        <tr>
                            <td>DISCRIM.</td>
                            <td class="field">${polissa.tarifa.get_num_periodes()}</td>
                        </tr>
                        %if polissa.descripcion_uso:
                            <tr>
                                <td>USO</td>
                                <td class="field">${polissa.descripcion_uso}</td>
                            </tr>
                        %endif
                    </table>
                </div>
            </div>
            <div id="cuerpo">
                <div id="calculo_factura">
                    <div class="texto-vertical_azul_col1 ajustes_vertical_col1_concepto">
                       CONCEPTOS
                    </div>
                    <table class="tabla_calculo_factura_conceptos">
                        <%
                            lines_sin_imp = {}
                            base_imponible = 0
                        %>
                        <tr>
                            <td class="titulo_calculo_factura_conceptos">Descripción del concepto</td>
                            <td class="titulo_calculo_factura_conceptos align_right">Cantidad</td>
                            <td class="titulo_calculo_factura_conceptos align_right">Precio</td>
                            <td class="titulo_calculo_factura_conceptos align_right">Subtotal</td>
                        </tr>
                        %for l in altres_lines:
                            %if len(l.invoice_line_tax_id) > 0:
                                <tr>
                                    <td class="f_9">${l.name}</td>
                                    <td class="f_9 align_right">${formatLang(l.quantity, 3)}</td>
                                    <td class="f_9 align_right">${formatLang(l.price_unit, 6)} €</td>
                                    <td class="f_9 align_right">${formatLang(l.price_subtotal)} €</td>
                                </tr>
                                <%
                                    base_imponible += l.price_subtotal
                                %>
                            %else:
                                <%
                                    lines_sin_imp[l.id] = l
                                %>
                            %endif
                        %endfor
                    </table>
                    <div id="contenido_totales">
                        <table id="totales">
                            <tr>
                                <td>BASE IMPONIBLE</td>
                                <td class="align_right">${formatLang(base_imponible, 2)}</td>
                            </tr>
                            <tr>
                                <%
                                    iva_set = set()
                                %>
                                %for tax in l.invoice_line_tax_id:
                                    %if 'IVA' in tax.name:
                                        <%
                                            iva_set.add(tax.amount * 100.0)
                                        %>
                                    %endif
                                %endfor
                                <td>IVA 21%</td>
                                <td class="align_right">${formatLang(inv.amount_tax)}</td>
                            </tr>
                            %for elem in lines_sin_imp:
                                <tr>
                                    <td class="texto_abr">${lines_sin_imp[elem].name}</td>
                                    <td class="align_right">
                                        ${formatLang(lines_sin_imp[elem].price_subtotal * sentido, 2)}
                                    </td>
                                </tr>
                            %endfor
                            <tr>
                                <td class="field f_10">TOTAL FACTURA</td>
                                <td class="field align_right f_10">${formatLang(inv.amount_total)}</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div id="historial_consumo">
                    <div id="grafico"></div>
                    <div id="publi">
                        <img style="width: 355px;" src="${addons_path}/giscedata_facturacio_comer_ee/report/img/imagen_anuncio.jpeg">
                    </div>
                </div>
            </div>
            <div id="pie">
                <div class="texto-vertical_azul_col1 ajustes_vertical_col1_info">
                   INFORMACIÓN ÚTIL
                </div>
                <table class="tabla_datos_pie_averias mt_5">
                    <tr>
                        <td colspan="2" class="f_10 field">AVERÍAS</td>
                    </tr>
                    <tr>
                        <td class="pt_5">EMPRESA DISTRIBUIDORA</td>
                        <td class="pt_5 field f_5">${polissa.distribuidora.name}</td>
                    </tr>
                    <tr>
                        <td>DIRECCIÓN</td>
                        <td class="field f_5">${polissa.distribuidora.address[0].street}, ${polissa.distribuidora.address[0].city}</td>
                    </tr>
                    <tr>
                        <td>TELÉFONO</td>
                        <td class="field f_5">${polissa.distribuidora.address[0].phone}</td>
                    <tr>
                        <td>CONTRATO DE ACCESO</td>
                        <td class="field f_5">${polissa.ref_dist}</td>
                    </tr>
                </table>
                <table class="tabla_datos_pie_reclamaciones mt_5">
                    <tr>
                        <td colspan="2" class="f_10 field">RECLAMACIONES</td>
                    </tr>
                    <tr>
                        <td class="pt_5">EMPRESA COMERCIALIZADORA</td>
                        <td class="pt_5 field f_5">${polissa.comercialitzadora.name}</td>
                    </tr>
                    <tr>
                        <td>TELÉFONO</td>
                        <td class="field f_5">900 373 275</td>
                    </tr>
                    <tr>
                        <td>CORREO ELECTRÓNICO</td>
                        <td class="field f_5">castellon@electraenergia.es</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td class="field f_5">morella@electraenergia.es</td>
                    </tr>
                </table>
            </div>
            <p style="page-break-after:always"></p>
            <div style="clear: both;"></div>
    %endfor
    </div>
    </body>
</html>
