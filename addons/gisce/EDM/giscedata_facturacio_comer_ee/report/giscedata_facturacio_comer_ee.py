# -*- coding: utf-8 -*-

from c2c_webkit_report import webkit_report
from report import report_sxw
from tools import config
from report_joiner.report_joiner import ReportJoiner


class report_webkit_html(report_sxw.rml_parse):
    def __init__(self, cursor, uid, name, context):
        super(report_webkit_html, self).__init__(cursor, uid, name,
                                                 context=context)
        self.localcontext.update({
            'cursor': cursor,
            'uid': uid,
            'addons_path': config['addons_path']
        })

webkit_report.WebKitParser(
    'report.giscedata.facturacio.factura',
    'giscedata.facturacio.factura',
    'addons/giscedata_facturacio_comer_ee/report/factura_ee.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.facturacio.factura.duplicat',
    'giscedata.facturacio.factura',
    'addons/giscedata_facturacio_comer_ee/report/factura_ee.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.facturacio.factura_concepto_ee',
    'giscedata.facturacio.factura',
    'addons/giscedata_facturacio_comer_ee/report/factura_conceptos_ee.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.facturacio.carta_orden_mandato_sepa',
    'payment.mandate',
    'addons/giscedata_facturacio_comer_ee/report/carta_orden_mandato_sepa.mako',
    parser=report_webkit_html
)

ReportJoiner(
    'report.mandate.with.carta',
    reports=['report.report_mandato',
             'report.giscedata.facturacio.comer.ee.carta.orden.mandato.sepa']
)
