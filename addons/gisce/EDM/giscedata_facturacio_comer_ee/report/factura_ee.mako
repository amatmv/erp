<%
    from datetime import datetime, timedelta
    from operator import attrgetter, itemgetter
    import json, re
    from giscedata_facturacio_comer.report.utils import get_giscedata_fact,get_other_info,get_discriminador,get_distri_phone,get_historics,get_linies_tipus,get_discounts,has_setu35_line
    from account_invoice_base.report.utils import localize_period

    pool = objects[0].pool
    polissa_obj = objects[0].pool.get('giscedata.polissa')
    tarifa_obj = objects[0].pool.get('giscedata.polissa.tarifa')
    sup_territorials_2013_comer_obj = pool.get('giscedata.suplements.territorials.2013.comer')
    sup_territorials_2013_tec271_comer_obj = pool.get('giscedata.suplements.territorials.2013.tec271.comer')
    report_banner_obj = pool.get('report.banner')

    def check_tax(tax, tipus):
        res = '-'
        if tipus is not None:
            if "IVA" in tax.name:
                res = tax.name.replace("IVA ", "")
                res = res.replace("%", "")
                res = "{} %".format(res)
        elif tax:
            if "IVA" in tax.name:
                res = "{} %".format(formatLang(tax.amount * 100, digits=0))
        return res

    def get_iva(linia, tipus=None):
        res  = '-'
        if tipus is not None:
            res = check_tax(linia, tipus)
        elif linia.invoice_line_tax_id:
            for tax in linia.invoice_line_tax_id:
                res = check_tax(tax, tipus)
        return res

    def get_texto_abr(inv):
        if inv.tipo_rectificadora == 'N':
            return ""
        elif inv.tipo_rectificadora in ['A','B', 'BRA']:
            accion = "anula"
        elif inv.tipo_rectificadora in ['R', 'RA']:
            accion = "rectifica"
        return "Esta factura {0} la factura {1} de fecha {2}".format(accion,inv.ref.number,inv.ref.date_invoice)
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_ee/report/pie.css"/>
    	<style type="text/css">
            ${css}
            @font-face {
                font-family: 'Ubuntu bold';
                src: local('Ubuntu'), url(${addons_path}/giscedata_facturacio_comer_ee/report/fonts/Ubuntu-Bold.ttf) format('ttf');
            }
            @font-face {
                font-family: 'Ubuntu';
                src: local('Ubuntu'), url(${addons_path}/giscedata_facturacio_comer_ee/report/fonts/Ubuntu-Regular.ttf) format('ttf');
            }
            body{
                background: #ffffff;
                background-image: url(${addons_path}/giscedata_facturacio_comer_ee/report/img/imagen_fondo.jpeg);
                background-repeat: repeat;
                background-position: left top;
                background-size: 100%;
            }
            #logo_superior_derecha_degradado{
                background-image: url(${addons_path}/giscedata_facturacio_comer_ee/report/img/degradado_superior.jpg);
                background-size: 115%;
                background-repeat: repeat-y;
            }
            .seccion_degradado, .seccion_degradado_2{
                background-image: url(${addons_path}/giscedata_facturacio_comer_ee/report/img/degradado_superior.jpg);
                background-repeat: repeat;
                background-position: left top;
                background-size: 100% 100%;
            }
        </style>
        <link href="${addons_path}/giscedata_facturacio_comer_ee/report/estils_ee.css" rel="stylesheet">
        <link href="${addons_path}/giscedata_facturacio_comer_ee/report/consum.css" rel="stylesheet">
    </head>
<body>
<%def name="taula_lectures(comptadors, tipus_5, integrat)">
        %for compt_act in comptadors:
             <!--
                "comptadors" es un dicconario con la siguiente estructura:
                    - Clave: tupla con 3 valores: (Número de serie, fecha actual, fecha anterior)
                    - Valor: lista con 3 elementos:
                        * Primer elemento: lecturas activa
                        * Segundo elemento: lecturas reactiva
                        * Tercer elemento: potencia maxímetro
            -->
            <div class="contenedor_tabla_lecturas">
                <div class="texto_contador">
                    Contador: ${compt_act[0]}
                    %if tipus_5:
                        <br>
                        Telegestión:
                        %if integrat:
                            sí
                        %else:
                            no
                        %endif
                    %endif

                </div>
                <!-- Recorremos cada contador de la lista de contadores -->
                <table class="tabla_datos_lectura espacio_columnas" style="float: left !important; margin-top: -1px; margin-left: 8px;">
                    <tr>
                        <td colspan="3" class="texto-azul text_center pb_5 f_7">ACTIVA</td>
                        <td colspan="3" class="texto-azul text_center pb_5 f_7">REACTIVA</td>
                        <td class="texto-azul text_center pb_5 f_7">MAXÍMETRO</td>
                    </tr>
                    <tr>
                        %for elem in range(1,3):
                            <td class="field">ANTERIOR</td>
                            <td class="field">ACTUAL</td>
                            <td class="field">CONSUMO</td>
                        %endfor
                        <td class="field"></td>
                    </tr>
                    <tr>
                        %for elem in range(1,3):
                            <td>${compt_act[2] or ''}</td>
                            <td>${compt_act[1] or ''}</td>
                            <td>
                                %if comptadors[compt_act][0][0].origen_id.codi == '40':
                                    ESTIMADA
                                %elif comptadors[compt_act][0][0].origen_id.codi == '99':
                                    SIN LECTURA
                                %else:
                                    REAL
                                %endif
                            </td>
                        %endfor
                        <td>&nbsp;</td>
                    </tr>
                    %for lectures in comptadors[compt_act]:
                        <!-- Recorremos las lecturas del contador actual -->
                        <%
                            comptador = 0
                        %>
                        <tr>
                        %for lectura in lectures:
                            <!--
                                Recorremos cada lectura individual. El contador (comptador) es utilizado
                                para conocer cual de los 3 elementos estamos recorriendo (activa, reactiva o maxímetro).
                                Para cada tipo de lectura se crea una nueva tabla que flotará a la derecha de la anterior.
                            -->

                                %if comptador == 0 or comptador == 1 and lectura:
                                    <td>${int(lectura.lect_anterior)}</td>
                                    <td>${int(lectura.lect_actual)}</td>
                                    <td>${int(lectura.consum)}</td>
                                %elif lectura:
                                    <td>${formatLang(lectura.pot_maximetre, digits=3)}</td>
                                %else:
                                    <td>&nbsp;</td>
                                %endif

                            <%
                                comptador += 1
                            %>
                        %endfor
                        </tr>
                    %endfor
                </table>
            </div>
        %endfor
</%def>
    %for inv in objects :
        <%
            sentido = 1.0
            banner_image = report_banner_obj.get_banner(cursor, uid, 'giscedata.facturacio.factura', inv.date_invoice, code='factura_cliente', model_id=inv.id)
            if inv.type in ('out_refund', 'in_refund'):
                sentido = -1.0
            entidad_bancaria = ''
            bank_acc = ''
            polissa = polissa_obj.browse(cursor, uid, inv.polissa_id.id, context={'date': inv.data_final.val})
            direccio = polissa.cups.direccio
            dies_factura = 1 + (datetime.strptime(inv.data_final, '%Y-%m-%d') - datetime.strptime(inv.data_inici, '%Y-%m-%d')).days
            diari_factura_actual_eur = inv.total_energia / (dies_factura or 1.0)
            diari_factura_actual_kwh = (inv.energia_kwh * 1.0) / (dies_factura or 1.0)
            idx_pob = direccio.rfind('(')
            if idx_pob != -1:
                direccio = direccio[:idx_pob]
            if polissa.cups.id_poblacio:
                poblacio_cups = polissa.cups.id_poblacio.name
            else:
                poblacio_cups = polissa.cups.id_municipi.name
            try:
                cp = polissa.cups.dp
                direccio = direccio.replace(cp, '')
            except:
                raise Exception(
                    u'El CUPS no tiene definido el código postal'
                )
            cups = polissa.cups.name
            pot_periode = dict([('P%s' % p,'') for p in range(1, 7)])
            for pot in polissa.potencies_periode:
                pot_periode[pot.periode_id.name] = pot.potencia

            if inv.payment_type.code == 'RECIBO_CSB':
                if not inv.partner_bank:
                    raise Exception(
                        u'La factura {0} no tiene número de cuenta definido.'.format(
                        inv.number)
                    )
                else:
                    entidad_bancaria = inv.partner_bank.name or inv.partner_bank.bank.lname
                    bank_acc = inv.partner_bank.iban[:-4].replace(' ','')  + '****'
            else:
                if inv.partner_bank:
                    entidad_bancaria = inv.partner_bank.name or inv.partner_bank.bank.lname
                    bank_acc = inv.partner_bank.iban[:-4].replace(' ','') + '****'
            altres_lines = [l for l in inv.linia_ids if l.tipus in 'altres']
            total_altres = sum([l.price_subtotal for l in altres_lines])

            # DATOS PIE CHART
            # Repartimento según BOE
            rep_BOE = {'i': 36.28, 'c': 32.12 ,'o': 31.60}
            pie_total = inv.amount_total - inv.total_lloguers
            pie_regulats = (inv.total_atr + total_altres)
            pie_impostos = float(inv.amount_tax)
            pie_costos = (pie_total - pie_regulats - pie_impostos )

            reparto = { 'i': ((pie_regulats * rep_BOE['i'])/100),
                        'c': ((pie_regulats * rep_BOE['c'])/100),
                        'o': ((pie_regulats * rep_BOE['o'])/100)
                       }

            dades_reparto = [
                [[0, rep_BOE['i']], 'i', _(u"Incentivos a las energías renovables, cogeneración y residuos"), formatLang(reparto['i'])],
                 [[rep_BOE['i'] , rep_BOE['i'] + rep_BOE['c']], 'c', _(u"Coste de redes de distribución y transporte"), formatLang(reparto['c'])] ,
                 [[rep_BOE['i'] + rep_BOE['c'], 100.00], 'o', _(u"Otros costes regulados (incluída anualidad del déficit)"), formatLang(reparto['o'])]
                ]
        %>
        <div id="contenido">
            <!-- Div superior donde se encuentran los logos-->
            <div id="cabecera">
                <div id="logo_superior_izq">
                    <img class="logo" src="${addons_path}/giscedata_facturacio_comer_ee/report/img/electraenergia_color_f_blanco_33.png" align="right">
                </div>
                <div id="logo_superior_derecha_degradado">
                    <div id="texto_degradado">
                        FACTURA DE ELECTRICIDAD
                    </div>
                </div>
            </div>
            <div id="texto_vertical_lateral">
                ${company.rml_footer2}
            </div>
            <div id="cabecera_datos_dir_envio">
                <div id="cabecera_datos_factura">
                    <div class="texto-vertical_azul_col1 ajustes_vertical_col1_cabecera">
                       DATOS DE LA FACTURA
                    </div>
                    <table class="tabla_datos_factura">
                        <tr>
                            <td>Nº FACTURA</td>
                            <td class="field">${inv.number or ''}</td>
                            <td class="field duplicado">
                                %if context.get('duplicado', False):
                                    DUPLICADO
                                %endif
                            </td>
                        </tr>
                        <tr>
                            <td>FECHA FACTURA</td>
                            <td class="field" colspan="2">${localize_period(inv.date_invoice, 'es_ES')}</td>
                        </tr>
                        <tr>
                            <td>PERIODO</td>
                            <%
                                factura = get_giscedata_fact(cursor, inv)[0]
                                info = get_other_info(cursor, factura['id'])[0]
                                fecha_inicio = datetime.strptime(factura['data_inici'], '%Y-%m-%d')
                                fecha_inicio_str = fecha_inicio.strftime('%d/%m/%Y')
                                fecha_final = datetime.strptime(factura['data_final'], '%Y-%m-%d')
                                fecha_final_str = fecha_final.strftime('%d/%m/%Y')
                            %>
                            <td class="field" colspan="2">Del ${fecha_inicio_str} al ${fecha_final_str}</td>
                        </tr>
                        <tr>
                            <td style="font-size: 12pt">TOTAL</td>
                            <td class="field f_21" colspan="2">${formatLang(sentido * inv.amount_total, digits=2)} €</td>
                        </tr>
                        <tr>
                        <td>DOMICILIACIÓN</td>
                            %if inv.payment_type.code == 'RECIBO_CSB':
                                <td class="field" style="font-size: 7pt;" colspan="2">${entidad_bancaria}</td>
                            %else:
                                <td class="field" style="font-size: 7pt;" colspan="2">PAGO POR TRANSFERENCIA</td>
                            %endif
                        </tr>
                        <tr>
                            <td>Nº DE CUENTA</td>
                            %if inv.payment_type.code == 'RECIBO_CSB':
                                <td class="field" colspan="2">${bank_acc}</td>
                            %elif inv.type != 'out_refund':
                                <td class="field" colspan="2">${inv.payment_mode_id and inv.payment_mode_id.bank_id.iban or inv.payment_type.note}</td>
                            %else:
                                <td class="field" colspan="2">${bank_acc or inv.partner_bank.iban or ''}</td>
                            %endif
                        </tr>
                        %if inv.payment_type.id == 2 and inv.type != 'out_refund':
                            <tr>
                                <td>FORMA DE PAGO</td>
                                <td class="field" colspan="2">${inv.payment_term.name.upper()} FECHA FACTURA</td>
                            </tr>
                        %endif
                    </table>
                </div>
                <div id="cabecera_envio">
                    <div class="texto-vertical_azul_col2 ajustes_direccion_cliente">
                       DIRECCIÓN DEL CLIENTE
                    </div>
                    <table style="left: -18px;" class="tabla_datos_factura_derecha">
                        <tr>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="field f_10_5">
                                ${inv.address_contact_id.name or ''}
                            </td>
                        </tr>
                        <tr>
                            <td class="field f_10_5">
                                ${inv.address_contact_id.street or ''}
                            </td>
                        </tr>
                        <tr>
                            <td class="field f_10_5">
                                ${inv.address_contact_id.zip or ''} ${inv.address_contact_id.city or ''}
                            </td>
                        </tr>
                        <tr>
                            <td class="field f_10_5">
                                ${inv.address_contact_id.state_id.name.upper() or ''}
                            </td>
                        </tr>
                         %if inv.address_contact_id.country_id.code != 'ES':
                            <tr>
                                <td class="field f_10_5">
                                    ${inv.address_contact_id.country_id.name.upper() or ''}
                                </td>
                            </tr>
                        %endif
                    </table>
                </div>
            </div>
            <!-- Datos del titular -->
            <!-- Div datos de la factura y ademas los datos del cliente. -->
            <div style="clear: both;"></div>
            <div id="Cuerpo_DatosTitular">
                <div id="cabecera_datos_titular">
                    <div class="texto-vertical_azul_col1 ajustes_vertical_col1_titular">
                       DATOS DE SUMINISTRO
                    </div>
                    <table class="tabla_datos_factura mt_5">
                        <tr>
                            <td><span style="white-space: nowrap;">Nº CONTRATO</span></td>
                            <td class="field">${polissa.name}</td>
                        </tr>
                        <tr>
                            <td>TITULAR</td>
                            %if len(inv.partner_id.name.rstrip()) > 30:
                                <td class="field small_text">
                            %else:
                                <td class="field">
                            %endif
                            ${inv.partner_id.name or ''}</td>
                        </tr>
                        <tr>
                            <td>DIR. SUMINISTRO</td>
                            %if len(direccio) > 30:
                                <td class="field small_text">${direccio}</td>
                            %else:
                                <td class="field">${direccio}</td>
                            %endif
                        </tr>
                        <tr>
                            <td>POBLACIÓN</td>
                            <td class="field">${poblacio_cups}</td>
                        </tr>
                        <tr>
                            <td>DNI/CIF</td>
                            <td class="field">${(inv.partner_id.vat or '').replace('ES','')}</td>
                        </tr>
                        <tr>
                            <td><span style="white-space: nowrap;">REF. CATASTRAL</span></td>
                            <td class="field">${info['giscedata_cups_ps_ref_cat']}</td>
                        </tr>
                    </table>
                </div>
                <div id="cabecera_datos_titular_derecha">
                    <table class="tabla_datos_factura" style="position: relative; float: left; margin-top: 16px;">
                        <tr>
                            <td><span style="white-space: nowrap;">TARIFA ACCESO</span></td>
                            <td class="field">${polissa.tarifa.name or ''}</td>
                        </tr>
                        <tr>
                            <td>TENSIÓN</td>
                            <td class="field">${info['giscedata_polissa_tensio']}</td>
                        </tr>
                        <tr>
                            <td>CNAE</td>
                            <td class="field">${info['giscedata_polissa_cnae'] or ''}</td>
                        </tr>
                        <tr>
                            <td>CUPS</td>
                            <td class="field">${cups}</td>
                        </tr>
                        <tr>
                            <td>POTENCIA</td>
                            <td class="field">
                                <%
                                    primer = True
                                %>
                                % for periode, potencia in sorted(pot_periode.items(),key=itemgetter(0)):
                                    % if potencia:
                                        %if not primer:
                                            -
                                        %else:
                                            <%
                                                primer = False
                                            %>
                                        %endif
                                        ${periode}:
                                        ${formatLang(potencia, digits=3)}
                                    % endif
                                % endfor
                            </td>
                        </tr>
                        <tr>
                            <td>DISCRIM.</td>
                            <td class="field">${polissa.tarifa.get_num_periodes()}</td>
                        </tr>
                        %if polissa.descripcion_uso:
                            <tr>
                                <td>USO</td>
                                <td class="field">${polissa.descripcion_uso}</td>
                            </tr>
                        %endif
                    </table>
                </div>
            </div>
            <div id="cuerpo">
                <div id="calculo_factura">
                    <div class="texto-vertical_azul_col1 ajustes_vertical_col1_calculo">
                       CÁLCULO DE LA FACTURA
                    </div>
                    <table class="tabla_calculo_factura">
                        <%
                            potencies = sorted(sorted(inv.linies_potencia, key=attrgetter('data_desde')), key=attrgetter('name'))
                            total_factura = 0
                            base_imponible = 0
                            lines_sin_imp = {}
                        %>
                        %if potencies:
                            <tr>
                                <td colspan=2 class="titulo_calculo_factura">Término de potencia</td>
                            </tr>
                            %for pot in potencies:
                                <%
                                    act = (sentido * pot.price_subtotal)
                                    total_factura += act
                                    iva_actual = get_iva(pot)
                                %>
                                <tr>
                                    <td>${pot.name} ${pot.quantity} kW x ${formatLang(pot.multi, digits=0)} días x ${formatLang(pot.price_unit_multi, digits=6)} €</td>
                                    <td class="align_right">${formatLang(sentido * pot.price_subtotal, digits=2)} €</td>
                                </tr>
                            %endfor
                        %endif
                        <%
                            energia = sorted(sorted(inv.linies_energia, key=attrgetter('price_unit_multi'), reverse=True), key=attrgetter('name'))
                        %>
                        %if energia:
                            <tr>
                                <td colspan=2 class="titulo_calculo_factura pt_5">Término de energía</td>
                            </tr>
                            %for ene in energia:
                                <tr>
                                    %if ene.isdiscount:
                                        <td>Descuento facturas sin papel sobre T.energía</td>
                                    %else:
                                        %if ene.quantity - int(ene.quantity):
                                         <td>${ene.name} ${formatLang(ene.quantity, digits=3)} kWh x ${formatLang(ene.price_unit_multi, digits=6)} €</td>
                                        %else:
                                            %if polissa.mode_facturacio == 'pvpc' and not ene.quantity:
                                              <td>${ene.name} ${formatLang(ene.quantity, digits=3)} kWh</td>
                                            %else:
                                              <td>${ene.name} ${formatLang(ene.quantity, digits=3)} kWh x ${formatLang(ene.price_unit_multi, digits=6)} €</td>
                                            %endif
                                        %endif                                       
                                    %endif
                                    <td class="align_right">${formatLang(sentido * ene.price_subtotal, digits=2)} €</td>
                                </tr>
                                <%
                                    act = (sentido * ene.price_subtotal)
                                    total_factura += act
                                    iva_actual = get_iva(ene)
                                %>
                            %endfor
                        %endif
                        <%
                            react = sorted(sorted(inv.linies_reactiva, key=attrgetter('data_desde')), key=attrgetter('name'))
                        %>
                        %if react:
                            <tr>
                                <td colspan=2 class="titulo_calculo_factura pt_5">Término de reactiva</td>
                            </tr>
                            %for ene in react:
                                <tr>
                                    <td>${ene.name} ${ene.quantity} kVArh x ${formatLang(ene.price_unit_multi, digits=6)} €</td>
                                    <td class="align_right">${formatLang(sentido * ene.price_subtotal, digits=2)} €</td>
                                </tr>
                                <%
                                    act = (sentido * ene.price_subtotal)
                                    total_factura += act
                                    iva_actual = get_iva(ene)
                                %>
                            %endfor
                        %endif
                        <%
                            exces_potencies = sorted(sorted([l for l in inv.linia_ids if l.tipus == 'exces_potencia'],key=attrgetter('data_desde')), key=attrgetter('name'))
                            total_exces_potencia = sum([l.price_subtotal for l in exces_potencies])
                        %>
                        %if exces_potencies:
                            <tr>
                                <td colspan=2 class="titulo_calculo_factura pt_5">Término de excesos</td>
                            </tr>
                            %for pot in exces_potencies:
                                <%
                                    act = (sentido * pot.price_subtotal)
                                    total_factura += act
                                    iva_actual = get_iva(pot)
                                %>
                                <tr>
                                    <td>${pot.name} ${pot.quantity} kW x ${pot.multi} x ${formatLang(pot.price_unit, digits=6)} €</td>
                                    <td class="align_right">${formatLang(sentido * pot.price_subtotal, digits=2)} €</td>
                                </tr>
                            %endfor
                        %endif
                        %if inv.tax_line:
                            <tr>
                                <td colspan=2 class="titulo_calculo_factura pt_5">Impuesto Electricidad</td>
                            </tr>
                            <%
                                base = 0.0
                                total_iese = 0.0
                                total_iva = 0.0
                            %>
                            %for t in inv.tax_line :
                                %if 'especial sobre la electricidad'.upper() in t.name.upper():
                                    <%
                                        base +=  t.base
                                        total_iese += t.amount
                                    %>
                                %elif 'IVA' in t.name.upper():
                                    <% total_iva += t.amount %>
                                    <% base_imponible += sentido * t.base %>
                                %endif
                                <%
                                    iva_actual = get_iva(t, 'iese')
                                %>
                            %endfor
                            <tr>
                                <%
                                   fiscal_percent = ''
                                   cl_percent='pl_15'
                                   full_excempt = False
                                %>
                                %if inv.fiscal_position and 'IESE' in inv.fiscal_position.name :
                                    <%
                                        fiscal_percent = 'Exento 100% de '
                                        cl_percent='pl_fiscal_position'
                                        full_excempt = True
                                    %>
                                    %if '90' in inv.fiscal_position.name :
                                      <%
                                          fiscal_percent = 'Exento 90% del 85% de '
                                          full_excempt = False
                                      %>
                                    %elif '95' in inv.fiscal_position.name :
                                      <%
                                          fiscal_percent = 'Exento 95% del 85% de '
                                          full_excempt = False
                                      %>
                                    %elif '98' in inv.fiscal_position.name :
                                      <%
                                          fiscal_percent = 'Exento 98% del 85% de '
                                          full_excempt = False
                                      %>
                                    %elif '100' in inv.fiscal_position.name :
                                      <%
                                          fiscal_percent = 'Exento 100% del 85% de '
                                          full_excempt = False
                                      %>
                                    %elif '85' in inv.fiscal_position.name :
                                      <%
                                          fiscal_percent = 'Exento 85% de '
                                          full_excempt = True
                                      %>
                                    %endif
                                %endif
                                %if not full_excempt:
                                    <td>${fiscal_percent}5,11269632 % ${_('sobre')} ${formatLang(sentido * base, digits=2)}€</td>
                                %else:
                                    <td>${fiscal_percent}5,11269632 %</td>
                                %endif
                                <%
                                    act = (sentido * total_iese)
                                    total_factura += act
                                %>
                                <td class="align_right">${formatLang(sentido * total_iese, digits=2)} €</td>
                            </tr>
                        %endif
                        %if altres_lines:
                            <tr>
                                <td colspan=2 class="titulo_calculo_factura pt_5">Otros Conceptos</td>
                            </tr>
                            %for ene in altres_lines:
                                %if len(ene.invoice_line_tax_id) > 0:
                                    <tr>
                                        <%
                                            act = (sentido * ene.price_subtotal)
                                            total_factura += act
                                            iva_actual = get_iva(ene)
                                        %>
                                        <td style="max-width: 200px;">${ene.name}</td>
                                        <td class="align_right">${formatLang(sentido * ene.price_subtotal, digits=2)} €</td>
                                    </tr>
                                %else:
                                    <%
                                        lines_sin_imp[ene.id] = ene
                                    %>
                                %endif
                            %endfor
                        %endif
                        %if inv.linies_lloguer:
                            <tr>
                                <td colspan=2 class="titulo_calculo_factura pt_5">Alquiler</td>
                            </tr>
                            %for alq in inv.linies_lloguer:
                                <tr>
                                    <%
                                        act = (sentido * alq.invoice_line_id.price_subtotal)
                                        total_factura += act
                                        iva_actual = get_iva(alq)
                                    %>
                                    <td>Importe alquiler</td>
                                    <td class="align_right">${formatLang(sentido * alq.invoice_line_id.price_subtotal, digits=2)} €</td>
                                </tr>
                            %endfor
                        %endif
                    </table>
                    <div id="contenido_totales">
                        <table id="totales">
                            %if get_texto_abr(inv):
                                <tr>
                                    <td colspan="2" class="texto_abr">
                                        ${get_texto_abr(inv)}
                                    </td>
                                </tr>
                            %endif
                            <tr>
                                <td>BASE IMPONIBLE</td>
                                <td class="align_right">
                                    ${formatLang(base_imponible, 2)}
                                </td>
                            </tr>
                            <tr>
                                <td>IVA 21%</td>
                                <td class="align_right">${formatLang(sentido * total_iva, digits=2)}</td>
                            </tr>
                            %for elem in lines_sin_imp:
                                <tr>
                                    <td class="texto_abr">${lines_sin_imp[elem].name}</td>
                                    <td class="align_right">
                                        ${formatLang(lines_sin_imp[elem].price_subtotal * sentido, 2)}
                                    </td>
                                </tr>
                            %endfor
                            <tr>
                                <td class="field f_10">TOTAL FACTURA</td>
                                <td class="field align_right f_10">${formatLang(sentido * inv.amount_total, digits=2)}</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div id="historial_consumo">
                    <div id="grafico">
                        <div class="texto-vertical_azul_col2 ajustes_historial_consumo">
                            HISTORIAL DE CONSUMO
                        </div>
                        <div id="contenedor_grafico">
                            <div class="chart_consum_container">
                                <div class="chart_consum" id="chart_consum_${inv.id}"></div>
                            </div>
                            <div id="text_consums">
                                <%
                                    historic = get_historics(cursor, info['giscedata_polissa_id'],inv.data_inici, inv.data_final)
                                    historic_graf = {}
                                    periodes_graf = []
                                    consums = {}
                                    historic_dies = 0
                                    total_historic_eur = 0

                                    for row in historic:
                                            historic_graf.setdefault(row['mes'],{})
                                            historic_graf[row['mes']].update({row['periode']: row['consum']})
                                            periodes_graf.append(row['periode'])

                                    periodes_graf = list(set(periodes_graf))
                                    consums_mig = dict.fromkeys(list(set(periodes_graf)), 0)
                                    consums_mig.update({'total':0})
                                    historic_js = []
                                    for mes, consums in historic_graf.items():
                                            p_js = {'mes': mes}
                                            for p in periodes_graf:
                                                p_js.update({p: consums.get(p,0.0)})
                                                consums_mig[p] += consums.get(p,0.0)
                                            historic_js.append(p_js)
                                            consums_mig['total'] += 1
                                    if consums:
                                            consums_mig = dict((k, v/consums_mig['total']) for k, v in consums.items() if k.startswith('P'))
                                            consums_mig = sorted(consums_mig.items(),key=itemgetter(1),reverse=True)
                                    if historic:
                                            total_historic_kw = sum([h['consum'] for h in historic])
                                            total_historic_eur = sum([h['facturat'] for h in historic])
                                            data_ini = min([h['data_ini'] for h in historic])
                                            data_fin = max([h['data_fin'] for h in historic])
                                            historic_dies = 1 + (datetime.strptime(data_fin, '%Y-%m-%d') - datetime.strptime(data_ini, '%Y-%m-%d')).days

                                            mes_any_inicial = (datetime.strptime(inv.data_inici,'%Y-%m-%d') - timedelta(days=365)).strftime("%Y/%m")
                                            h['mes'] = h['mes'][2:]
                                            total_any = sum([h['consum'] for h in historic if h['mes'] > mes_any_inicial])
                                %>
                            </div>
                        </div>
                    </div>
                    <div id="publi">
                        % if has_setu35_line(cursor, uid, pool, inv):
                            <div>
                                <%
                                    cups = polissa_obj.read(cursor, uid, inv.polissa_id.id, ['cups'])['cups'][1]
                                %>
                                <div id="report_sups">
                                    <p>Tabla detallada Suplementos Autonómicos 2013 (*)</p>
                                    ${sup_territorials_2013_comer_obj.get_info_html(cursor, uid, cups)}
                                    <p>En caso de que el importe total de la regularización supere los dos euros, sin incluir impuestos, el mismo será fraccionado en partes iguales por las empresas comercializadoras en las facturas que se emitan en el plazo de 12 cuotas a partir de la primera regularización. En caso de facturación bimestral, se recibirá en 6 cuotas.</p>
                                    <p>* Tabla informativa conforme a lo establecido en la ETU/35/2017 de 23 de enero, por la cual le informamos de los parámetros para el cálculo de los suplementos territoriales facilitados por su empresa distribuidora ${polissa.distribuidora.name}.</p>
                                </div>
                            </div>
                        % elif inv.has_tec271_line():
                            <div>
                                <%
                                    cups = polissa_obj.read(cursor, uid, inv.polissa_id.id, ['cups'])['cups'][1]
                                %>
                                <div id="report_sups">
                                    <p>Tabla detallada Suplementos Autonómicos 2013 (*)</p>
                                    ${sup_territorials_2013_tec271_comer_obj.get_info_html(cursor, uid, cups)}
                                    <p>En caso de que el importe total de la regularización supere los dos euros, sin incluir impuestos, el mismo será fraccionado en partes iguales superiores a 1€ por las empresas comercializadoras en las facturas que se emitan en el plazo de 12 meses a partir de la primera regularización</p>
                                    <p>* Tabla informativa conforme a lo establecido en la TEC/271/2019 de 6 de marzo, por la cual le informamos de los parámetros para el cálculo de los suplementos territoriales facilitados por su empresa distribuidora ${polissa.distribuidora.name}.</p>
                                </div>
                            </div>
                        % else:
                            <!--<img style="width: 355px;" src="${addons_path}/giscedata_facturacio_comer_ee/report/img/imagen_anuncio.jpeg">-->
                            <img style="width: 355px;" src="data:image/jpeg;base64,${banner_image}"/>
                        % endif
                    </div>
                </div>
            </div>
            <div id="pie">
                <div class="texto-vertical_azul_col1 ajustes_vertical_col1_info">
                   INFORMACIÓN ÚTIL
                </div>
                <table class="tabla_datos_pie_averias mt_5">
                    <tr>
                        <td colspan="2" class="f_10 field">AVERÍAS</td>
                    </tr>
                    <tr>
                        <td class="pt_5">EMPRESA DISTRIBUIDORA</td>
                        <td class="pt_5 field f_5">${polissa.distribuidora.name}</td>
                    </tr>
                    <tr>
                        <td>DIRECCIÓN</td>
                        <td class="field f_5">${polissa.distribuidora.address[0].street}, ${polissa.distribuidora.address[0].city}</td>
                    </tr>
                    <tr>
                        <td>TELÉFONO</td>
                        <td class="field f_5">${polissa.distribuidora.address[0].phone}</td>
                    <tr>
                        <td>CONTRATO DE ACCESO</td>
                        <td class="field f_5">${polissa.ref_dist}</td>
                    </tr>
                </table>
                <table class="tabla_datos_pie_reclamaciones mt_5">
                    <tr>
                        <td colspan="2" class="f_10 field">RECLAMACIONES Y GESTIONES</td>
                    </tr>
                    <tr>
                        <td class="pt_5">EMPRESA COMERCIALIZADORA</td>
                        <td class="pt_5 field f_5">${polissa.comercialitzadora.name}</td>
                    </tr>
                    <tr>
                        <td>TELÉFONO</td>
                        <td class="field f_5">900 373 275</td>
                    </tr>
                    <tr>
                        <td>CORREO ELECTRÓNICO</td>
                        <td class="field f_5">facturacion@electraenergia.es</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td class="field f_5">morella@electraenergia.es</td>
                    </tr>
                </table>

                <table class="tabla_datos_pie_informacion_adicional mt_10">
                <%
                   web = polissa.distribuidora.website
                   if not web:
                     web = 'Sin web configurada. Disculpen las molestias'
                %>

                       <tr><td> Puede acceder gratuitamente a los datos de la medida horaria que han servido para la
                       facturación a través de la web de su distribuidor:</td></tr>
                        <tr><td><a class="field f_5" style='text-decoration:none;color:black;' href="${web}">${web}</a></td></tr>

                </table>

            </div>
    <script>
        var factura_id = ${inv.id}
        var data_consum = ${json.dumps(sorted(historic_js, key=lambda h: h['mes']))}
    </script>
    <script src="${addons_path}/giscedata_facturacio_comer/report/assets/d3.min.js"></script>
    <script src="${addons_path}/giscedata_facturacio_comer_ee/report/consum.js"></script>
    <p style="page-break-after:always"></p>
    <!-- ########################    SEGUNDA PÁGINA    ######################## -->
    <div class="seccion_degradado"></div>
    <%
        periodes_a = sorted(sorted(
                        list([lectura for lectura in inv.lectures_energia_ids
                                        if lectura.tipus == 'activa']),
                        key=attrgetter('name')
                        ), key=attrgetter('comptador'), reverse=True
                    )

        periodes_r = sorted(sorted(
                        list([lectura for lectura in inv.lectures_energia_ids
                                    if lectura.tipus == 'reactiva']),
                                    key=attrgetter('name')
                                    ), key=attrgetter('comptador'), reverse=True
                                 )

        periodes_m = sorted(sorted(list([lectura
                                      for lectura in inv.lectures_potencia_ids]
                                    ),
                                    key=attrgetter('name')
                                    ), key=attrgetter('comptador'), reverse=True
                                )

        lectures=map(None, periodes_a, periodes_r, periodes_m)

        comptador_actual = None
        comptador_anterior = None

        comptadors = {}
        llista_lect = []
        data_actual = None
        for lect in lectures:
            if lect[0]:
                comptador_actual = lect[0].comptador
                data_ant = lect[0].data_anterior
                data_act = lect[0].data_actual
            elif lect[1]:
                comptador_actual = lect[1].comptador
                data_ant = lect[1].data_anterior
                data_act = lect[1].data_actual
            elif lect[2]:
                comptador_actual = lect[2].comptador
                data_ant = lect[2].data_anterior
                data_act = lect[2].data_actual

            if comptador_anterior and comptador_actual != comptador_anterior:
                comptadors[(comptador_anterior, data_actual_pre, data_ant_pre)] = llista_lect
                llista_lect = []

            if data_act:
                comptadors[(comptador_actual, data_act, data_ant)] = llista_lect
                llista_lect.append((lect[0], lect[1], lect[2]))

                comptador_anterior = comptador_actual
                data_actual_pre = data_act
                data_ant_pre = data_ant
    %>
    <div class="seccion mt_10 w_17">
        <div class="texto-vertical_azul_col1 ajustes_vertical_col1_seccion">
           LECTURAS
        </div>
        <table class="tabla_datos_lectura mr_5 mt_25">
            %if comptadors:
                <tr>
                    <td class="field">FECHA</td>
                </tr>
                %for lect in comptadors[comptadors.keys()[0]]:
                    <tr>
                        <td class="field">
                            % if lect[0]:
                                ${lect[0].name[-3:-1]}
                            %endif
                        </td>
                    </tr>
                %endfor
            %endif
        </table>
    </div>
    <div class="seccion mt_10 ml_5 pl_5 w_81_8">
        ${taula_lectures(comptadors, inv.potencia <= 15, inv.polissa_tg == '1')}
    </div>
    <div class="seccion_2 mt_5 w_100">
        <div class="texto-vertical_azul_col1 ajustes_vertical_col1_seccion_2">
           DESTINO DEL IMPORTE DE LA FACTURA
        </div>
        <div class="nota_destino_importe_superior">
            El destino del importe de su factura, ${formatLang(sentido * inv.amount_total, digits=2)} €, es el siguiente
        </div>
        <div class="grafico_destino_importe">
            <div class="chart_desti" id="chart_desti_${inv.id}"></div>
        </div>
        <div class="nota_destino_importe_inferior">
            %if inv.linies_lloguer:
                A los importes indicados en el diagrama debe añadirse, en su caso, el importe de alquiler
                de los equipos de medida y control: ${formatLang(sentido * alq.invoice_line_id.price_subtotal, digits=2)} €
            %endif
        </div>
    </div>
   <div class="seccion_3 mt_5 w_100">
        <div class="datos_origen w_60">
            <div class="texto-vertical_azul_col1 ajustes_vertical_col1_seccion_2">
               ORIGEN DE LA ELECTRICIDAD
            </div>
            <div class="tabla_origen">
                <table class="tabla_datos_origen_electricidad">
                    <tr>
                        <td></td>
                        <td class="p5 text_center azul_origen white_font" style="padding: 20px !important;">ORIGEN</td>
                        <td class="p5 text_center negro_comer white_font" style="width: 120px;">ELECTRA ENERGÍA, S.A.U.</td>
                        <td class="p5 azul_mezcla white_font" style="padding-left: 10px !important;">MEZCLA DE PRODUCCIÓN SISTEMA ELÉCTRICO ESPAÑOL</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="azul_origen_2" style="height: 5px;"></td>
                        <td class="negro_comer_2"></td>
                        <td class="azul_mezcla_2"></td>
                    </tr>
                    <tr>
                        <td>
                            <div class="leyenda_grafico leyenda_renovable"></div>
                        </td>
                        <td class="azul_origen_3 pl_5">Renovable</td>
                        <td class="negro_comer_3 pl_42">15,7%</td>
                        <td class="azul_mezcla_3 pl_60">38,2%</td>
                    </tr>
                    <tr>
                        <td>
                            <div class="leyenda_grafico leyenda_cae"></div>
                        </td>
                        <td class="azul_origen_2 pl_5">Cogeneración de Alta Eficiencia</td>
                        <td class="negro_comer_2 pl_42">0,6%</td>
                        <td class="azul_mezcla_2 pl_60">4,4%</td>
                    </tr>
                    <tr>
                        <td>
                            <div class="leyenda_grafico leyenda_cog"></div>
                        </td>
                        <td class="azul_origen_3 pl_5">Cogeneración</td>
                        <td class="negro_comer_3 pl_42">10,0%</td>
                        <td class="azul_mezcla_3 pl_60">6,9%</td>
                    </tr>
                    <tr>
                        <td>
                            <div class="leyenda_grafico leyenda_gas"></div>
                        </td>
                        <td class="azul_origen_2 pl_5">CC Gas Natural</td>
                        <td class="negro_comer_2 pl_42">17,0%</td>
                        <td class="azul_mezcla_2 pl_60">11,7%</td>
                    </tr>
                    <tr>
                        <td>
                            <div class="leyenda_grafico leyenda_carbon"></div>
                        </td>
                        <td class="azul_origen_3 pl_5">Carbón</td>
                        <td class="negro_comer_3 pl_42">21,1%</td>
                        <td class="azul_mezcla_3 pl_60">14,5%</td>
                    </tr>
                    <tr>
                        <td>
                            <div class="leyenda_grafico leyenda_fuel"></div>
                        </td>
                        <td class="azul_origen_2 pl_5">Fuel/Gas</td>
                        <td class="negro_comer_2 pl_42">3,8%</td>
                        <td class="azul_mezcla_2 pl_60">2,6%</td>
                    </tr>
                    <tr>
                        <td>
                            <div class="leyenda_grafico leyenda_nuclear"></div>
                        </td>
                        <td class="azul_origen_3 pl_5">Nuclear</td>
                        <td class="negro_comer_3 pl_42">30,2%</td>
                        <td class="azul_mezcla_3 pl_60">20,7%</td>
                    </tr>
                    <tr>
                        <td>
                            <div class="leyenda_grafico leyenda_otras"></div>
                        </td>
                        <td class="azul_origen_2 pl_5">Otras</td>
                        <td class="negro_comer_2 pl_42">1,6%</td>
                        <td class="azul_mezcla_2 pl_60">1,0%</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="datos_origen w_39_4 ml_5">
            <div class="texto-vertical_azul_col1 ajustes_vertical_col1_seccion_2_izq">
                INFORMACIÓN SOBRE SU ELECTRICIDAD
            </div>
            <div class="texto_origen">
                Si bien la energía eléctrica que llega a
                nuestros hogares es indistinguible de la
                que consumen nuestros vecinos u otros
                consumidores conectados al mismo
                sistema eléctrico, ahora sí es posible
                garantizar el origen de la producción de
                energía eléctrica que usted consume.
            </div>
            <div class="texto_origen mt_5 texto-azul">
                A estos efectos se proporciona el
                desglose de la mezcla de tecnologías de
                producción nacional para así comparar los
                porcentajes del promedio nacional con los
                correspondientes a la energía vendida por
                su Compañía Comercializadora.
            </div>
        </div>
        <div class="seccion_2 transparente">
            <div class="texto-vertical_azul_col1 ajustes_vertical_col1_seccion_2">
               ORIGEN DE LA ELECTRICIDAD
            </div>
            <table class="graficos_origen">
                <tr>
                    <td style="padding: 0; margin: 0; width: 160px;">
                        <img style="width: 180px;" src="${addons_path}/giscedata_facturacio_comer_ee/report/img/grafico_01.png">
                    </td>
                    <td style="vertical-align: bottom; font-size: 7pt; width: 160px;">
                        MEZCLA DE PRODUCCIÓN EN EL SISTEMA ELÉCTRICO ESPAÑOL 2018
                    </td>
                    <td style="padding: 0; margin: 0; width: 160px;">
                        <img style="width: 160px;" src="${addons_path}/giscedata_facturacio_comer_ee/report/img/grafico_02.png">
                    </td>
                    <td style="vertical-align: bottom; font-size: 7pt;">
                        MEZCLA ELECTRA ENERGÍA, S.A.U
                    </td>
                </tr>
                <tr>
                    <td colspan="4" class="nota_origen">El sistema eléctrico nacional ha importado un 4,3% de producción neta total nacional</td>
                </tr>
            </table>
        </div>
    </div>
    <div class="seccion mt_5 gris_oscuro h_160 w_40">
        <div class="texto-vertical_azul_col1 ajustes_vertical_col1_seccion">
           IMPACTO MEDIOAMBIENTAL
        </div>
        <div class="texto_impacto_ambiental">
            El impacto ambiental de su electricidad
            depende de las fuentes energéticas utilizadas
            para su generación.
            <br><br>
            En una escala de A a G donde A indica el mínimo
            impacto ambiental y G el máximo, y que el
            valor medio nacional corresponde al nivel D, la
            energía comercializada por su “Comercializador
            A” tiene los siguientes valores:
        </div>
    </div>
    <div class="grafico_barras">
        <img style="width: 170px;" src="${addons_path}/giscedata_facturacio_comer_ee/report/img/grafico_co2.png">
    </div>
    <div class="grafico_barras" style="margin-left: 40px;">
        <img style="width: 170px;" src="${addons_path}/giscedata_facturacio_comer_ee/report/img/grafico_residuos.png">
    </div>
    <div style="clear: both;"></div>
    <div class="seccion_4 mt_5"style="width: 100% !important;">
        <div class="seccion_degradado_2 w_40 rotate_180"></div>
        <div class="texto-vertical_azul_col1 ajustes_vertical_col1_seccion_4">
            &nbsp;&nbsp;&nbsp;&nbsp;VENCIMIENTO CONTRATO:
            <br>
            <div class="fecha_vencimiento">
                ${formatLang(polissa.modcontractual_activa.data_final, date=True)}
            </div>
        </div>
        <div class="container_texto">
            <div class="texto_lopd">
                En virtud de las normativas vigentes en protección de datos personales,
                el Reglamento (UE) 2016/679 de 27 de abril de 2016
                (GDPR) y la Ley Orgánica (ES) 15/1999 de 13 de diciembre (LOPD), le informamos
                que todos los datos que usted nos ha facilitado para la
                facturación de nuestros servicios están incluidos en un
                fichero de datos personales, siendo responsable del mismo
                ELECTRA ENERGÍA, S.A.U. para su tratamiento con la finalidad
                de emisión de facturas, así como para mantener con Vd. una
                relación comercial e informarle de las novedades relacionadas
                con la compañía. Usted podrá ejercer sus derechos de acceso,
                rectificación, cancelación y oposición dirigiéndose por escrito
                al Servicio de Atención al Cliente/Facturación,
                <span class="nowrap">C/. Huerto de
                Mas, 3 - 2º, 12002 - CASTELLÓN</span>
            </div>
        </div>
    </div>
<script>
    var pie_total = ${pie_total};
    var pie_data = [{val: ${pie_regulats}, perc: 30, code: "REG"},
                    {val: ${pie_costos}, perc: 55, code: "PROD"},
                    {val: ${pie_impostos},  perc: 15 ,code: "IMP"}
                   ];

    var pie_etiquetes = {'REG': {t: ['${formatLang(float(pie_regulats))}€','${_(u"Costes regulados")}'], w:100},
                         'IMP': {t: ['${formatLang(float(pie_impostos))}€','${_(u"Impuestos aplicados")}'], w:100},
                         'PROD': {t: ['${formatLang(float(pie_costos))}€','${_(u"Coste de producción")}'], w:100}
                        };

    var reparto = ${json.dumps(reparto)}
    var dades_reparto = ${json.dumps(dades_reparto)}

    var factura_id = ${inv.id}
</script>
<script src="${addons_path}/giscedata_facturacio_comer_ee/report/pie.js"></script>
<div style="clear: both;"></div>
%endfor
</div>
</body>
</html>
