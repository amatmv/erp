<%
    from datetime import datetime
    from babel.dates import format_date

    polissa_obj = objects[0].pool.get('giscedata.polissa')
    address_obj = objects[0].pool.get('res.partner.address')

    if data and len(data.get('ids', [])):
        mandates_ids = data.get('ids')
        pool = objects[0].pool
        mandate_obj = pool.get('payment.mandate')
        mandates = mandate_obj.browse(objects[0]._cr, user.id, mandates_ids)
        if not context:
            raise Exception
    else:
        mandates = objects

    if data and data.get('context'):
        contexte = data.get("context")
    else:
        contexte = {}
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
        <title>${_("CARTA DE ORDEN DE MANDATO SEPA")}}</title>
        <link rel="stylesheet" type="text/css" href="${addons_path}/giscedata_facturacio_comer_ee/report/carta_orden_mandato_sepa.css"/>
    </head>
    <body>
        %for mandate in mandates:
             <%
                address_id = company.partner_id.address_get()['default']
                address = address_obj.browse(cursor, uid, address_id)
                referencia = str(mandate.reference).split(',')
                polissa = polissa_obj.browse(cursor, uid, [eval(referencia[1])])[0]
            %>
            <table id="header">
                <tr>
                    <td rowspan="2">
                        <img id="logo" src="data:image/jpeg;base64,${company.logo}"/>
                    </td>
                    <td class="header-info">
                        <p><b>OFICINA CENTRAL</b></p>
                        <p>${address.street}</p>
                        <%
                            poblacio = address.id_poblacio.name if address.id_poblacio else ''
                            fax = 'Fax {}'.format(address.fax) if address.fax else ''
                        %>
                        <p>${address.zip} - ${poblacio}</p>
                        <p>Tlfno. ${address.phone}   ${fax}</p>
                    </td>
                </tr>
                <tr>
                    <td class="header-info">
                        <p><b>DELEGACIÓN COMERCIAL</b></p>
                        <p>Plaza de Colón, 12 Bajos</p>
                        <p>12300 - MORELLA</p>
                        <p>Tlfno. 964 160 250   Fax 964 160 826</p>
                    </td>
                </tr>
            </table>

            <div id="destinatari">
                <%
                    if contexte and contexte.get('address'):
                        rec_addr = address_obj.browse(cursor, uid, contexte['address'])
                        calle_direccion_xx = rec_addr.street
                        codpostal_ciudad = rec_addr.zip + " " + rec_addr.city
                        provincia = rec_addr.state_id.name
                    else:
                        binary_address = mandate.split_by_zip_code(context={'address': 'debtor'})[mandate.id]
                        calle_direccion_xx = binary_address[0]
                        codpostal_ciudad = binary_address[1]
                        provincia = mandate.debtor_state
                %>
                <p>${(mandate.debtor_name).upper()}</p>
                <p>
                    ${calle_direccion_xx.upper()}<br/>
                    ${codpostal_ciudad.upper()}<br/>
                    ${provincia.upper()}
                </p>
            </div>

            <div id="lloc-i-data">Castellón de la Plana, ${format_date(datetime.strptime(mandate.date, '%Y-%m-%d'), "d 'de' MMMM 'de' yyyy", locale='es_ES')}</div>

            <p id="apertura" class="cos">Estimado Cliente:</p>

            <p class="missatge cos">Atendiendo a su solicitud de cambio de
                domiciliación bancaria para su contrato número <span class="blue-bold">${polissa.name}</span> con ${company.name}, cuyo
            titular es <span class="blue-bold">${(polissa.titular.name).upper()}</span>, le comunicamos que hemos procedido a modificar los
            datos según su deseo.</p>

            <p class="missatge cos">Le adjuntamos tres ejemplares de la <u>Orden de Domiciliación Adeudo
            Directo SEPA</u>, para que nos devuelva firmado el ejemplar para el
            ACREEDOR en el sobre con correo pagado que al efecto le adjuntamos</p>

            <p class="missatge cos">Deberá hacer llegar a su entidad bancaria el ejemplar para el banco y la
            copia para el cliente se la quedará usted para tener constancia del
            cambio.</p>

            <p class="missatge cos">Agradecemos de antemano su atención y quedamos a su disposición,</p>

            <div id="remitent">
                ${company.name}<br/>
                Departamento de Facturación
            </div>
        %endfor
    </body>
</html>