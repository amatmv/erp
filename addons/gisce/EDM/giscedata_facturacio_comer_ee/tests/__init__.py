# -*- coding: utf-8 -*-

import unittest

from destral import testing
from destral.transaction import Transaction

from giscedata_facturacio_comer_ee.giscedata_polissa import ZONA_CARTA

class TestFacturacionComerEdm(testing.OOTestCase):

    def setUp(self):
        pool = self.openerp.pool
        polissa_obj = pool.get('giscedata.polissa')
        imd_obj = pool.get('ir.model.data')
        self.txn = Transaction().start(self.database)

        cursor = self.txn.cursor
        uid = self.txn.user
        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]

        polissa_obj.send_signal(cursor, uid, [polissa_id], [
            'validar', 'contracte'
        ])
        self.polissa_0001_id = polissa_id

    def tearDown(self):
        self.txn.stop()

    def testActivarEnvioMail(self):
        pool = self.openerp.pool
        poliza_obj = pool.get('giscedata.polissa')
        poliza_obj.activar_envio_mail(self.txn.cursor, self.txn.user, self.polissa_0001_id, "algo@valido.com")
        poliza = poliza_obj.browse(self.txn.cursor, self.txn.user, self.polissa_0001_id)
        self.assertEqual(poliza.enviament,"email")
        self.assertEqual(poliza.ordre_carta, "99")
        self.assertEqual(poliza.direccio_notificacio.email, "algo@valido.com")
        self.assertEqual(poliza.zona_carta_id.name, ZONA_CARTA)
        ##poliza_obj.read(self.polissa_0001_id, ["enviament","zona_carta_id","ordre_carta","direccio_notificacio"]),