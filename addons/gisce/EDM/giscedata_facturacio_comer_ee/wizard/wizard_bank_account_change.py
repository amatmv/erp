from osv import osv


class WizardBankAccountChange(osv.osv_memory):

    _name = "wizard.bank.account.change"
    _inherit = "wizard.bank.account.change"

    def print_mandate(self, cursor, uid, ids, context=None):
        mandate_obj = self.pool.get('payment.mandate')
        mandate_print_return = mandate_obj.print_mandate(
            cursor, uid, ids=ids, context=context
        )
        mandate_with_carta_print = {
            'type': 'ir.actions.report.xml',
            'report_name': 'mandate.with.carta',
            'datas': mandate_print_return['datas']
        }
        return mandate_with_carta_print

    _defaults = {
        'payment_mode': False
    }


WizardBankAccountChange()
