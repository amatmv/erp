# -*- encoding: utf-8 -*-

from osv import osv, fields
import StringIO
import base64
from addons import get_module_resource
from datetime import datetime
from babel.dates import format_date


class WizardGenerarListadosDeCorreos(osv.osv_memory):
    _name = 'wizard.listados.correos'

    def generate_xls(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        from base_extended.excel import Sheet

        active_ids = context.get('active_ids', False)
        factura_ids = tuple(active_ids)

        wizard = self.browse(cursor, uid, ids[0], context=context)
        if wizard.listado == 'corte':
            sql_file_path = get_module_resource(
                'giscedata_facturacio_comer_ee', 'sql', 'query_carta_corte.sql'
            )
            name = 'Cartas de corte'
        elif wizard.listado == 'aviso':
            sql_file_path = get_module_resource(
                'giscedata_facturacio_comer_ee', 'sql', 'query_carta_aviso.sql'
            )
            name = 'Cartas de aviso'
        elif wizard.listado == 'correos':
            sql_file_path = get_module_resource(
                'giscedata_facturacio_comer_ee', 'sql', 'query_carta_corte_correos.sql'
            )
            name = 'Cartas de corte para correos'
        else:
            raise osv.except_osv(
                ('Falta seleccionar opción!'),
                ('Por favor, selecciona una opción en \"Listado\"')
            )

        query = open(sql_file_path).read()
        cursor.execute(query, (factura_ids, ))
        results = cursor.fetchall()

        sheet = Sheet('correos')

        if wizard.listado == 'aviso':

            headers = [
                ('CODIGO_CLIENTE', 'CiudadFecha', 'Nombre1', 'Direccion1',
                  'Nombre2', 'Direccion2', 'CPNotificacion',
                  'Poblacion2', 'Municipio2', 'Provincia2', 'Pais2', 'NumFact', 'ImpFact', 'FecFact',
                 'Expr1', 'Observaciones')
            ]
            results = headers + results
        elif wizard.listado == 'corte':
            POSITION_FECHA = 1
            POSITION_IMP_FACT = 14
            from devolucions_base.defs import sepa_devolution_reason
            results = [list(tupla) for tupla in results]
            for row in results:
                reject_code = row[-1]
                row[POSITION_FECHA] = u'Castellón {fecha}'.format(
                    fecha=format_date(
                        datetime.now(), "d 'de' LLLL 'de' YYYY", locale='es_ES')
                )
                row[POSITION_IMP_FACT] = '{imp_fact}'.format(
                    imp_fact=row[POSITION_IMP_FACT]
                )
                if reject_code in sepa_devolution_reason:
                    reject_msg = "({0}) {1}".format(
                        reject_code, sepa_devolution_reason[reject_code])
                    row[-1] = reject_msg
            headers = [
                ('CODIGO_CLIENTE', 'CiudadFecha', 'Nombre1', 'Direccion1',
                 'Nombre2', 'Direccion2', 'CPNotificacion',
                  'Poblacion2', 'Municipio2', 'Provincia2', 'Pais2', 'NumFact', 'ImpFact', 'FecFact',
                 'FechaCorte')
            ]
            results = headers + results

        for row in results:
            sheet.add_row(row)

        output_report = StringIO.StringIO()
        sheet.save(output_report)

        report = output_report.getvalue()
        output_report.close()

        extension = '.xls'

        wizard = self.browse(cursor, uid, ids[0], context=context)
        wizard.write({
            'report': base64.b64encode(report),
            'filename_report': '{0}{1}'.format(name, extension),
            'state': 'end'
        })
        return True

    _columns = {
        'listado': fields.selection(
            [('corte', 'Cartas de corte'), ('aviso', 'Cartas de aviso'), ('correos', 'Cartas de corte para correos')],
            'Listado'),
        'report': fields.binary('Excel generado'),
        'state': fields.selection([('init', 'Init'), ('end', 'End')], 'Estat'),
        'filename_report': fields.char('Nombre de archivo exportado', size=256)
    }

    _defaults = {
        'state': lambda *a: 'init',
    }


WizardGenerarListadosDeCorreos()
