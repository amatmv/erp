# -*- encoding: utf-8 -*-

from osv import osv, fields
import re

class WizardActivarEnvioEmail(osv.osv_memory):

    _name = "wizard.activar.envio.email"

    def activar_envio_email(self, cursor, uid, ids, context=None):
        #Comprobamos que se sólo se haya seleccionado una única poliza para
        #realizar el cambio a envío por email
        active_ids = context.get('active_ids', False)
        if len(active_ids) > 1:
            raise osv.except_osv(
                'Error múltiples pólizas seleccionadas',
                'Este asistente solo se puede usar con una única póliza '
                'a la vez'
            )
        #Extraemos el id de la poliza:
        # Si hacemos context.get('active_id', False) es que se ha seleccionado
        # el asistente desde el menú de la derecha
        # Si hacemos active_ids[0] es que se ha seleccionado del menú acciones,
        #  pero como ya se ha realizado la comprobación anterior, sabemos que
        # sólo hay uno seleccionado, por tanto del array extraemos el primero
        # y, en este caso, único elemento
        poliza_id = context.get('active_id', False) or active_ids[0]

        # Extraemos el email escrito en el asistente
        wizard = self.browse(cursor, uid, ids[0], context=context)
        email = wizard.email

        # Comprobacion mediante expresion regular que el email es válido
        if not re.match('^[(a-z0-9\_\-\.)]+@[(a-z0-9\_\-\.)]+\.[(a-z)]{2,15}$',
                    email.lower()):
            raise osv.except_osv(
                'El formato del email es incorrecto',
                'Se debe indicar un email válido del tipo '
                'usuario@dominio.extension'
            )
        # Se crea un objeto del tipo giscedata.polissa
        poliza_obj = self.pool.get('giscedata.polissa')

        # Ejecutamos el proceso de activación por email y cambio de tarifa
        # en caso que sea necesario
        poliza_obj.activar_envio_mail(
            cursor, uid, poliza_id, email, context=context)
        #Ejecutamos extraer factura sin papel para comprobar si se ha cambiado
        # la tarifa
        fecha_activacion, tarifa_sin_papel = poliza_obj.extraer_tarifa_sinpapel(
                cursor, uid, poliza_id, context=context
            )

        texto_informacion = 'Se ha activado el envío de facturas por email'
        #Si hay una tarifa de cambio se mostrará la tarifa al usuario
        if tarifa_sin_papel:
            price_list_obj = self.pool.get('product.pricelist')
            tarifa_nueva = \
                price_list_obj.browse(cursor, uid, tarifa_sin_papel).name

            texto_informacion += '\nSe ha activado también la tarifa a: '\
                                 + tarifa_nueva
        #En caso que no haya una tarifa, se indica al usuario que no se ha
        # cambiado la tarifa
        else:
            texto_informacion += '\nNo se ha cambiado a la tarifa sin ' \
                                 'papel debido a que la tarifa actual' \
                                 ' no tiene una tarifa sin papel asociada ' \
                                 'y/o la potencia contratada es mayor de 15kW'

        # Sirve para actualizar el campos "Columns" del wizard
        # En este caso actualizamos el estado del formulario y el
        # campo información
        wizard.write({'state': 'end', 'informacion': texto_informacion})

    #necesario para los datos por defecto a mostrar el formulario
    # (definidos mas abajo en _defaults)
    def _get_default_information(self, cursor, uid, context=None):
        return (
            u"Asistente para envío de factura por email. \n\n"
            u"En caso de ser una póliza compatible con la tarifa sin papel "
            u"se activará automáticamente."
        )
    #variables a utilizar en el formulario
    _columns = {
        'informacion': fields.text('Información', readonly=True),
        'email': fields.char(
            'Email para activación', size=128, required=True, readonly=True,
            states={'init': [('readonly', False)]}
        ),
        'state': fields.selection([('init', 'Init'), ('end', 'End')], 'Estado')
    }
    #Valores por defecto de las variables del _columns
    _defaults = {
        #Llama al método que devuelve el valor a mostrar
        'informacion': _get_default_information,
        #Se utiliza una función lambda que devuelve el valor 'init'
        'state': lambda *a: 'init',
    }



WizardActivarEnvioEmail()
