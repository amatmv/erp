# -*- coding: utf-8 -*-
{
    "name": "Reports Facturació Eléctrica Del Maestrazgo (Electra EnergíaComercialitzadora)",
    "description": """Reports Facturació Eléctrica Del Maestrazgo (Electra Energía, Comercialitzadora)""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "EDM",
    "depends":[
        "base",
        "crm_impagos",
        "report_joiner",
        "giscedata_facturacio",
        "giscedata_facturacio_comer",
        "facturae_module",
        "giscedata_facturacio_comer_ifo",
        "giscedata_polissa_comer",
        "giscedata_facturacio_impagat_comer",
        "giscedata_facturacio_devolucions",
        "giscedata_facturacio_switching",
        "c2c_webkit_report",
        "jasper_reports"
    ],
    "init_xml": [],
    "demo_xml": ["giscedata_facturacio_comer_demo.xml"],
    "update_xml":[
        "giscedata_facturacio_comer_data.xml",
        "giscedata_facturacio_comer_report.xml",
        "giscedata_polissa_view.xml",
        "giscedata_facturacio_view.xml",
        "wizard/wizard_generar_listados_de_correos_view.xml",
        "wizard/wizard_activar_envio_email_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
