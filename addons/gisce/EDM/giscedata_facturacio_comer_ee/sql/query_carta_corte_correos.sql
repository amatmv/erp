SELECT pa.name,
       '',
       '',
       pa.street,
       pa.zip,
       pob.name,
       prov.name,
       country.code,
       polissa.name,
       --country.name,
       1, /*TRAMO DE PESO:  1= 20 gr.
                            2= 20 gr. no norm.
                            3= 50 gr.
                            4= 100 gr.
                            5= 200 gr.
                            6= 350 gr.
                            7= 500 gr.
                            8= 1000 gr.
                            9= 1500 gr.*/
       1 /*VALOR AÑADIDO:   0=sin valor añadio
                            1=con Aviso de Recibo*/
FROM giscedata_facturacio_factura AS f
LEFT JOIN account_invoice AS acc_inv ON (f.invoice_id = acc_inv.id)
LEFT JOIN res_partner_address AS pa ON (acc_inv.address_contact_id = pa.id)
LEFT JOIN res_poblacio AS pob ON (pa.id_poblacio = pob.id)
LEFT JOIN res_country_state AS prov ON (pa.state_id = prov.id)
LEFT JOIN res_country AS country ON (pa.country_id = country.id)
LEFT JOIN giscedata_polissa AS polissa ON (f.polissa_id = polissa.id)
WHERE f.id IN %s