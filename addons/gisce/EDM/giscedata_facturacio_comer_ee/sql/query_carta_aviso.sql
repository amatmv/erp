SELECT polissa.name,
       acc_inv.date_due,
       partner.name,
       cups.direccio,
       pa.name,
       pa.street,
       pa.zip,
       upper(pob_pa.name) as Poblacion_notif,
	   upper(mun_pa.name) as Municipio_notif,
	   upper(sta_pa.name) as Provincia_notif,
	   upper(cou_pa.name) as Pais_notif,
       acc_inv.number,
--       acc_inv.amount_total, Pendiente de añadir en futuro
       acc_inv.residual,
       acc_inv.date_invoice,
       regexp_replace(pb.printable_iban, '(....)$', '****'),
       devolucio_linia.reject_code
FROM giscedata_facturacio_factura AS f
LEFT JOIN giscedata_polissa AS polissa ON (f.polissa_id = polissa.id)
LEFT JOIN res_partner AS partner ON (polissa.titular = partner.id)
LEFT JOIN giscedata_cups_ps AS cups ON (polissa.cups = cups.id)
LEFT JOIN res_poblacio AS pob_cups ON (cups.id_poblacio = pob_cups.id)
LEFT JOIN account_invoice AS acc_inv ON (f.invoice_id = acc_inv.id)
LEFT JOIN res_partner_address AS pa ON (acc_inv.address_contact_id = pa.id)
LEFT JOIN res_poblacio AS pob_pa ON (pa.id_poblacio = pob_pa.id)
LEFT JOIN res_municipi as mun_pa ON (mun_pa.id = pob_pa.municipi_id)
LEFT JOIN res_country_state as sta_pa ON (mun_pa.state = sta_pa.id)
LEFT JOIN res_country as cou_pa ON (cou_pa.id = sta_pa.country_id)
LEFT JOIN res_partner_bank AS pb ON (acc_inv.partner_bank = pb.id)
LEFT JOIN giscedata_facturacio_devolucio AS devolucio ON (f.devolucio_id = devolucio.id)
LEFT JOIN giscedata_facturacio_devolucio_linia AS devolucio_linia
    ON (devolucio.id = devolucio_linia.devolucio_id
    AND devolucio_linia.numfactura = acc_inv.number)
WHERE f.id IN %s
