# -*- encoding: utf-8 -*-

from osv import osv
from tools import config
from facturae import facturae
import netsvc
import base64
import tempfile
import subprocess
from tools.translate import _


class AccountInvoiceFacturae(osv.osv):

    _name = 'account.invoice.facturae'
    _inherit = 'account.invoice.facturae'

    def facturae_administrative_centre(self, administrative, center_address,
                                       info=None, context=None):
        centre = super(
            AccountInvoiceFacturae, self
        ).facturae_administrative_centre(
            administrative, center_address, info=info, context=context)
        if administrative.role =='01':
            centre.feed({
                'centredescription': info
            })

        return centre


AccountInvoiceFacturae()
