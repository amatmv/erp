# -*- coding: utf-8 -*-
from osv import osv, fields
from datetime import datetime
import json
from tools.translate import _


class WizardCreateOt(osv.osv_memory):

    _name = "wizard.create.ot.from.case"

    def create_ot_case(self, cursor, uid, ids, context=None):
        ''' Generates the cases from invoices'''
        if not context:
            context = {}
        wizard = self.browse(cursor, uid, ids[0], context)
        case_id = context.get('active_ids', [])[0]

        case_obj = self.pool.get("crm.case")
        case = case_obj.browse(cursor, uid, case_id)
        polissa = None
        partner_id = None
        email = None
        addr = None
        if case.polissa_id:
            polissa = case.polissa_id
            partner_id = polissa.titular.id
            partner_obj = self.pool.get('res.partner')
            addr = partner_obj.address_get(cursor, uid, [partner_id], ['contact'])
            email = ''
            if addr['contact']:
                adres_obj = self.pool.get('res.partner.address')
                email = adres_obj.read(cursor, uid, addr['contact'], ['email'])
                email = email['email']
        vals = {
            'name': wizard.name,
            'date': wizard.date,
            'user_id': wizard.responsable_id.id,
            'section_id': wizard.section_id.id,
            'polissa_id': polissa and polissa.id or None,
            'partner_id': partner_id or None,
            'partner_address_id': addr and addr['contact'] or False,
            'email_from': email or None,
            'ref': '%s,%i' % ('crm.case', case_id)
        }

        casos_ids = []
        casos_ids.append(case_obj.create(cursor, uid, vals, context))
        cases_json = json.dumps(casos_ids)
        wizard.write({'state': 'done', 'generated_cases': cases_json})

    def action_casos(self, cursor, uid, ids, context=None):
        wiz = self.browse(cursor, uid, ids[0])
        casos_str = wiz.generated_cases
        casos_ids = json.loads(casos_str)
        return {
            'domain': "[('id','in', {0})]".format(str(casos_ids)),
            'name': _('Ordres de Treball Creades'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'crm.case',
            'type': 'ir.actions.act_window'
        }

    def _get_default_responsable(self, cr, uid, context):
        return uid

    def _get_default_section(self, cr, uid, context):
        imd_obj = self.pool.get('ir.model.data')
        sec_id = imd_obj.get_object_reference(
            cr, uid, 'giscedata_ot_edm', 'ot_ordenes_de_trabajo')
        return sec_id[1]

    _columns = {
        'name': fields.char(_(u'Descripcio'), size=128, required=True),
        'date': fields.datetime(_('Data'), required=True),
        'responsable_id': fields.many2one('res.users', 'Responsable', required=True),
        'section_id': fields.many2one('crm.case.section', 'Seccio'),
        'state': fields.char('State', size=16),
        'generated_cases': fields.text(_('Ordres de Treball Generades')),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'date': lambda *a: datetime.today().strftime('%Y-%m-%d %H:%M:%S'),
        'responsable_id': _get_default_responsable,
        'section_id': _get_default_section,
    }

WizardCreateOt()
