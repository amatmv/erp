# -*- coding: utf-8 -*-
from osv import osv, fields

class crm_case_section(osv.osv):
    _name = 'crm.case.section'
    _inherit = 'crm.case.section'

    _columns = {
        'report_id': fields.many2one(
            'ir.actions.report.xml', 'Report ID', required=False)
    }

    _defaults = {
        'report_id': lambda * a: False
    }

crm_case_section()

class GiscedataSwitchingCrm(osv.osv):
    _name = 'crm.case'
    _inherit = 'crm.case'

    def create_case_generic(self, cursor, uid, model_ids, context=None,
                                  description='Generic Case',
                                  section='CASE',
                                  category=None,
                                  assign=None,
                                  extra_vals=None):

        case_ids = super(GiscedataSwitchingCrm, self).create_case_generic(
                                  cursor, uid, model_ids, context,
                                  description, section,
                                  category, assign, extra_vals)

        if not context:
            context = {}
        model = context.get('model', False)

        if model == 'giscedata.switching':
            sw_obj = self.pool.get('giscedata.switching')
            cups_obj = self.pool.get('giscedata.cups.ps')
            for case in self.browse(cursor, uid, case_ids, context):
                ref_spl = case.ref.split(',')
                if len(ref_spl) == 2:
                    sw_id = int(ref_spl[1])
                    sw = sw_obj.read(cursor, uid, sw_id,
                                     ['cups_id', 'titular_polissa'])

                    # afegim el CUPS i adreça de subministrament a la descripcio
                    cups_id = sw['cups_id'][0]
                    cupsr = cups_obj.read(cursor, uid, [cups_id],
                                          ['name', 'direccio'])
                    cups = cupsr[0]['name']
                    adr = cupsr[0]['direccio']
                    desc = case.description
                    vals = {'name': '{0}: {1}, {2}'.format(desc, cups, adr)}

                    # afegim el titular de la polissa relacionada a la OT
                    empresa_id = sw['titular_polissa'][0]
                    vals.update({'partner_id': empresa_id})
                    case.write(vals)

        return case_ids

GiscedataSwitchingCrm()
