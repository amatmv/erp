from __future__ import absolute_import
import os
import tempfile

import pooler
from tools import config
from c2c_webkit_report import webkit_report
import pypdftk


class WebKitParser(webkit_report.WebKitParser):
    def create_single_pdf(self, cursor, uid, ids, data, report_xml, context=None):
        parent = super(WebKitParser, self).create_single_pdf
        pool = pooler.get_pool(cursor.dbname)
        crm_obj = pool.get('crm.case')
        per_template = {}
        for case in crm_obj.browse(cursor, uid, ids, context=context):
            report_path = case.section_id.report_id.report_webkit
            per_template.setdefault(report_path, [])
            per_template[report_path].append(case.id)
        to_join = []
        for report_webkit, model_ids in per_template.items():
            self.tmpl = report_webkit
            report_xml.report_webkit = os.path.join(
                    config['addons_path'], self.tmpl
            )
            content, _ = parent(cursor, uid, model_ids, data, report_xml,
                                context)
            res_path = tempfile.mkstemp('-join.pdf', 'report-')[1]
            with open(res_path, 'w') as f:
                f.write(content)
            to_join.append(res_path)

        out = pypdftk.concat(files=to_join)
        with open(out, 'r') as f:
            content = f.read()
        return content, 'pdf'
