# -*- coding: utf-8 -*-
from osv.orm import browse_null


class Fake(browse_null):

    def __getitem__(self, name):
        return Fake()

    def __getattr__(self, name):
        return Fake()


def get_comptador(case):
    for attr in ['ref', 'ref2']:
        ref = str(getattr(case, attr)).split(',')
        if len(ref) > 1:
            if ref[0] == 'giscedata.lectures.comptador':
                comptador_id = int(ref[1])
                compt_obj = case.pool.get('giscedata.lectures.comptador')
                return compt_obj.browse(case._cr, case._uid, comptador_id)
            elif ref[0] == 'giscedata.polissa':
                polissa_id = int(ref[1])
                pol_obj = case.pool.get('giscedata.polissa')
                pol = pol_obj.browse(case._cr, case._uid, polissa_id)
                return pol.comptadors[0]
    return Fake()


def get_icp(case, num, field):
    icps = case.polissa_id.icps
    icp = None
    if num == 0:
        if len(icps) > 0:
            icp = case.polissa_id.icps[0]
    elif num == 1:
        if len(icps) > 1:
            icp = case.polissa_id.icps[1]
    if field == 'name' and icp:
        return icp.name
    elif field == 'tipo' and icp:
        return icp.product_id.name
    elif field == 'marca' and icp:
        return icp.product_id.default_code
    elif field == 'fecha_inst' and icp:
        return icp.data_alta
    return ''


def get_observacions(case, type):
    if type == 'generales':
        if len(case.history_line) > 1:
            return case.history_line[len(case.history_line)-1].note
        elif len(case.history_line) == 1:
            return case.history_line[0].note
    elif type == 'operario':
        if case.description:
            return case.description
        elif len(case.history_line) > 0:
            return case.history_line[0].note
    return ''


def get_butlleti(case, field):
    if case.polissa_id.butlletins:
        if field == 'instalador':
            partner = case.polissa_id.butlletins[0].partner_id
            if partner:
                return partner.name
        elif field == 'referencia':
            return case.polissa_id.butlletins[0].name
        elif field == 'telefon':
            addr = case.polissa_id.butlletins[0].partner_address_id
            if addr:
                phone = addr.phone or '-'
                mobile = addr.mobile or '-'
                return '{0} / {1}'.format(phone, mobile)

        elif field == 'data_inst':
            return case.polissa_id.butlletins[0].data
        elif field == 'data_vig':
            return case.polissa_id.butlletins[0].data_vigencia
    return ''
