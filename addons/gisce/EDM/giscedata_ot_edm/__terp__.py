# -*- coding: utf-8 -*-
{
    "name": "Ordres de treball",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Reports",
    "depends":[
        "c2c_webkit_report",
        "giscedata_polissa_crm"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_ot_edm_report.xml",
        "giscedata_ot_edm_data.xml",
        "wizard/wizard_create_ot_from_case.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
