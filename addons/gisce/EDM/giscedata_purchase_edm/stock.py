# -*- encoding: utf-8 -*-
from osv import osv


class stock_picking(osv.osv):
    _name = "stock.picking"
    _inherit = "stock.picking"

    def send_signal_to_open_purchases(self, cursor, uid, ids, signal, context=None):
        """
        Envia el 'signal' indicado a ciertos pedidos de compra. Estos pedidos
        de compra son aquellos que esten relacionados con los albaranes
        indicados ('ids') y que esten en un estado determinado según el
        'signal':
            - 'purchase_pending' si el estado del pedido es 'recibido'
            - 'purchase_received' si es 'pendiente'

        Con este comportamiento se consigue que, si el pedido es del tipo
        'abierto', se puedan ligar albaranes a este y dependiendo del estado
        del albaran, el estado del pedido baile entre 'pendiente' (si hay
        albaranes pendientes) y 'recibido' (si todos los albaranes estan
        realizados).

        :param cursor:
        :param uid: <res.users> ids
        :type uid: long
        :param ids: <stock.picking> ids. Albaranes cuyos pedidos de compra se
        enviará el signal.
        :type ids: [long]
        :param signal: nombre del signal que se enviará al pedido de compra.
        :type signal: str
        :param context: OpenERP context.
        :type context: {}
        :return:
        """
        purchase_o = self.pool.get('purchase.order')

        purchase_f = ['state', 'picking_ids']
        picking_f = ['purchase_id']
        picking_vs = self.read(cursor, uid, ids, picking_f, context=context)
        purchase_to_pending_ids = set()

        signal_by_state = {
            'purchase_pending': 'received',
            'purchase_received': 'pending',
        }

        for picking_v in picking_vs:
            purchase_id = picking_v['purchase_id']
            if purchase_id:
                purchase_id = purchase_id[0]
                purchase_v = purchase_o.read(
                    cursor, uid, purchase_id, purchase_f, context=context
                )
                if purchase_v['state'] == signal_by_state[signal]:
                    send_signal = True

                    # Si el signal es cuando se realiza el albaran, entonces
                    # para poder cambiar el estado del pedido de compra
                    # de pendiente a recibido se necesita que todos los
                    # albaranes de este esten o cancelados o realizados.
                    if signal == 'purchase_received':
                        dmn = [
                            ('purchase_id', '=', purchase_id),
                            ('state', 'not in', ['done', 'cancel'])
                        ]
                        purchase_picking_ids = self.search(
                            cursor, uid, dmn, context=context
                        )
                        send_signal = not purchase_picking_ids

                    if send_signal:
                        purchase_to_pending_ids.add(purchase_id)

        purchase_to_pending_ids = list(purchase_to_pending_ids)
        purchase_o.send_signal(cursor, uid, purchase_to_pending_ids, [signal])

        return True

    def action_confirm(self, cursor, uid, ids, context=None):
        """
        Si el albarán pasa a estado 'confirmado' correctamente y este tiene
        relacionado algun pedido de compra, entonces el estado del pedido
        pasará a 'pendiente' solamente si el estado de es 'recepcionado'.
        :param cursor:
        :param uid: <res.users> ids
        :type uid: long
        :param ids: <stock.picking> ids.
        :type ids: [long]
        :return: True si se pasa a 'confirmado' correctamente.
        :rtype: bool
        """
        res = super(stock_picking, self).action_confirm(cursor, uid, ids, context=context)
        if res:
            self.send_signal_to_open_purchases(cursor, uid, ids, 'purchase_pending')
        return res

    def action_draft_wkf(self, cursor, uid, ids, context=None):
        """
        Si el albarán se crea correctamente en estado 'borrador' y este tiene
        relacionado algun pedido de compra, entonces el estado del pedido
        pasará a 'pendiente' solamente si el estado de es 'recepcionado'.
        :param cursor:
        :param uid: <res.users> ids
        :type uid: long
        :param ids: <stock.picking> ids.
        :type ids: [long]
        :return: True si se el albarán se crea en 'borrador' correctamente.
        :rtype: bool
        """
        res = super(stock_picking, self).action_draft_wkf(cursor, uid, ids, context=context)
        if res:
            self.send_signal_to_open_purchases(cursor, uid, ids, 'purchase_pending')
        return res

    def action_assign_wkf(self, cursor, uid, ids):
        """
        Si el albarán pasa a estado 'pendiente' correctamente y este tiene
        relacionado algun pedido de compra, entonces el estado del pedido
        pasará a 'pendiente' solamente si el estado de es 'recepcionado'.
        :param cursor:
        :param uid: <res.users> ids
        :type uid: long
        :param ids: <stock.picking> ids.
        :type ids: [long]
        :return: True si se pasa a 'pendiente' correctamente.
        :rtype: bool
        """
        res = super(stock_picking, self).action_assign_wkf(cursor, uid, ids)
        if res:
            self.send_signal_to_open_purchases(cursor, uid, ids, 'purchase_pending')

        return res

    def action_done(self, cursor, uid, ids, context=None):
        """
        Si el albarán pasa a estado 'realizado' correctamente y este tiene
        relacionado algun pedido de compra, entonces el estado del pedido
        pasará a 'recepcionado' solamente si todos los albaranes de este estan
        en estado 'realizado' o 'cancelado'.
        :param cursor:
        :param uid: <res.users> ids
        :type uid: long
        :param ids: <stock.picking> ids.
        :type ids: [long]
        :param context: OpenERP context.
        :type context: {}
        :return: True si se pasa a 'realizado' correctamente.
        :rtype: bool
        """
        res = super(stock_picking, self).action_done(cursor, uid, ids, context=context)
        if res:
            self.send_signal_to_open_purchases(
                cursor, uid, ids, 'purchase_received', context=context
            )

        return res

    def action_cancel(self, cursor, uid, ids, context=None):
        """
        Si el albarán pasa a estado 'cancelado' correctamente y este tiene
        relacionado algun pedido de compra, entonces el estado del pedido
        pasará a 'recepcionado' solamente si todos los albaranes de este estan
        en estado 'realizado' o 'cancelado'.
        :param cursor:
        :param uid: <res.users> ids
        :type uid: long
        :param ids: <stock.picking> ids.
        :type ids: [long]
        :param context: OpenERP context.
        :type context: {}
        :return: True si se pasa a 'cancelado' correctamente.
        :rtype: bool
        """
        if context is None:
            context = {}

        res = super(stock_picking, self).action_cancel(cursor, uid, ids, context=context)
        if res:
            self.send_signal_to_open_purchases(
                cursor, uid, ids, 'purchase_received', context=context
            )

        return res


stock_picking()
