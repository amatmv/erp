# -*- encoding: utf-8 -*-

from osv import fields
from osv import osv
import netsvc


class purchase_order(osv.osv):

    _name = 'purchase.order'
    _inherit = 'purchase.order'

    def __init__(self, pool, cursor):
        """
        Indica que ciertos campos deben ser readonly en el nuevo estado (open)
        :param pool:
        :param cursor:
        """
        super(purchase_order, self).__init__(pool, cursor)
        uid = 1
        purchase_fields = self.fields_get(cursor, uid)
        ro_true = [('readonly', True)]
        purchase_fields['partner_id']['states']['pending'] = ro_true
        purchase_fields['partner_id']['states']['received'] = ro_true
        purchase_fields['date_order']['states']['pending'] = ro_true
        purchase_fields['date_order']['states']['received'] = ro_true
        purchase_fields['pricelist_id']['states']['pending'] = ro_true
        purchase_fields['pricelist_id']['states']['received'] = ro_true

    def _get_states(self, cursor, uid, context=None):
        """
        Obtiene los estados del pedido de compra. Para este módulo se declara
        uno nuevo: open (pedido abierto).
        :param cursor:
        :param uid: <res.users> id
        :type uid: long
        :param context: OpenERP context.
        :type context: {}
        :return:
        """
        res = super(purchase_order, self)._get_states(cursor, uid, context=context)
        res += [('pending', 'Pendiente'), ('received', 'Recepcionado')]
        return res

    def all_pickings_cancelled_or_done(self, cursor, uid, ids, context=None):
        """
        Indica si todos los albaranes asociados al pedido estan en un estado
        final (cancelados o realizados).
        :param cursor:
        :param uid: <res.users> id
        :type uid: long
        :param ids: <purchase.order> ids
        :type ids: [long]
        :param context: OpenERP context
        :type context: {}
        :return: True si todos los albaranes estan cancelados o realizados.
        :rtype: bool
        """
        end_picking_states = ['done', 'cancel']
        res = True
        for purchase in self.browse(cursor, uid, ids, context=context):
            for picking in purchase.picking_ids:
                if picking.state not in end_picking_states:
                    res = False
                    break

        return res

    def wkf_pending_order(self, cursor, uid, ids, context=None):
        """
        Pasa los pedidos de compra al estado 'pending' (pendiente).
        :param cursor:
        :param uid: <res.users> id
        :type uid: long
        :param ids: <purchase.order> ids
        :type ids: [long]
        :param context: OpenERP context
        :type context: {}
        :return: True
        :rtype: bool
        """
        return self.write(cursor, uid, ids, {'state': 'pending'}, context=context)

    def send_signal(self, cursor, uid, ids, signals):
        """
        Envia los 'signals' al workflow del pedido de compra.
        :param cursor:
        :param uid: <res.users> ids
        :type uid: long
        :param ids: <stock.picking> ids.
        :type ids: [long]
        :return: True
        :rtype: bool
        """
        wf_service = netsvc.LocalService('workflow')
        if not isinstance(signals, list) and not isinstance(signals, tuple):
            signals = [signals]
        for p_id in ids:
            for signal in signals:
                wf_service.trg_validate(uid, 'purchase.order', p_id, signal, cursor)
        return True

    def wkf_received_order(self, cursor, uid, ids, context=None):
        """
        Pasa los pedidos de compra al estado 'received' (recepcionados).
        :param cursor:
        :param uid: <res.users> id
        :type uid: long
        :param ids: <purchase.order> ids
        :type ids: [long]
        :param context: OpenERP context
        :type context: {}
        :return: True
        :rtype: bool
        """
        return self.write(cursor, uid, ids, {'state': 'received'}, context=context)

    def wkf_picking_done(self, cursor, uid, ids, context=None):
        """
        Ejecuta el comportamiento de la función original si el estado del pedido
        es diferente de 'open'. De esta manera se evita que el pedido pase a
        estado aprobado y se marque como que se ha recibido.
        :param cursor:
        :param uid: <res.users> id
        :type uid: long
        :param ids: <purchase.order> ids
        :type ids: [long]
        :param context: OpenERP context
        :type context: {}
        :return: True
        :rtype: bool
        """
        purchase_vs = self.read(cursor, uid, ids, ['state'], context=context)
        for purchase_v in purchase_vs:
            if purchase_v['state'] not in ['pending', 'received']:
                super(purchase_order, self).wkf_picking_done(
                    cursor, uid, purchase_v['id'], context=context
                )

        return True

    _columns = {
        'notes_no_print': fields.text('Notas (no imprime)'),
        'open': fields.boolean(
            'Abierto',
            states={
                'approved': [('readonly', True)],
                'pending': [('readonly', True)],
                'received': [('readonly', True)],
                'done': [('readonly', True)]
            },
            help="Indica si el pedido se debe considerar como abierto. "
                 "Si es así, cuando se apruebe, se podran seguir añadiendo"
                 "lineas al pedido. La gestión de los albaranes se deberá hacer"
                 "manualmente para cada nueva linea que se agregue al pedido."
        ),
        # Copy-paste del campo 'state' del modulo base (purchase) para poder
        # indicar el nuevo estado con la función sobrecargada _get_states
        'state': fields.selection(
            _get_states, 'Order Status', readonly=True, select=True,
            help="El estado del pedido de compra o de la solicitud de "
                 "presupuesto. Un presupuesto es un pedido de compra en estado "
                 "'Borrador'. Entonces, si el pedido es confirmado por el "
                 "usuario, el estado cambiará a 'Confirmado'. Entonces el "
                 "proveedor debe confirmar el pedido para cambiar el estado a "
                 "'Aprobado'. Cuando el pedido de compra está pagado y "
                 "recibido, el estado se convierte en 'Relizado'. Si una "
                 "acción de cancelación ocurre en la factura o en la recepción "
                 "de mercancías, el estado se convierte en 'Excepción'.",
        ),
    }


purchase_order()


class purchase_order_line(osv.osv):
    _name = "purchase.order.line"
    _inherit = "purchase.order.line"

    def product_id_change(self, cr, uid, ids, pricelist, product, qty, uom,
                          partner_id, date_order=False, fiscal_position=False):
        """
        Permite introducir cero en la cantidad del producto. Útil en los pedidos
        en estado abierto cuando una linea se debe "cancelar" pero el resto no.
        :param cr:
        :param uid:
        :param ids:
        :param pricelist:
        :param product:
        :param qty:
        :param uom:
        :param partner_id:
        :param date_order:
        :param fiscal_position:
        :return:
        """

        qty_backup = qty

        res = super(purchase_order_line, self).product_id_change(
            cr, uid, ids, pricelist, product, qty, uom, partner_id,
            date_order=date_order, fiscal_position=fiscal_position
        )

        if qty_backup == 0:
            res['value']['product_qty'] = qty_backup

        return res


purchase_order_line()
