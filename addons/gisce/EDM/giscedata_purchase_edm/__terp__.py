# -*- coding: utf-8 -*-
{
    "name": "GISCE-EDM Purchase",
    "description": """Extensió del mòdul giscedata_purchase per a EDM.""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE",
    "depends":[
        "giscedata_purchase",
        "giscedata_stock_edm",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_purchase_edm_view.xml",
        "giscedata_purchase_edm_report.xml",
        "purchase_workflow.xml",
        "stock_view.xml",
    ],
    "active": False,
    "installable": True
}
