<%
    from datetime import datetime
    from giscedata_facturacio_comer.report.utils import get_other_info,get_discriminador,get_distri_phone,get_historics,get_linies_tipus,get_discounts
    from account_invoice_base.report.utils import localize_period
    from babel.numbers import format_currency

    # add line note in invoice
    pool = objects[0].pool
    conf_obj = pool.get('res.config')
    add_line_note = bool(int(conf_obj.get(cursor, uid, 'add_generic_invoice_line_note', False)))
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
    	<style type="text/css">
        ${css}
        @font-face {
            font-family: 'Ubuntu bold';
            src: local('Ubuntu'), url(${addons_path}/giscedata_facturacio_comer_ee/report/fonts/Ubuntu-Bold.ttf) format('ttf');
        }
        @font-face {
            font-family: 'Ubuntu';
            src: local('Ubuntu'), url(${addons_path}/giscedata_facturacio_comer_ee/report/fonts/Ubuntu-Regular.ttf) format('ttf');
        }
        body{
            background: #ffffff;
            background-image: url(${addons_path}/giscedata_facturacio_comer_ee/report/img/imagen_fondo.jpeg);
            background-repeat: repeat;
            background-position: left top;
            background-size: 100%;
        }
        #logo_superior_derecha_degradado{
            background-image: url(${addons_path}/giscedata_facturacio_comer_ee/report/img/degradado_superior.jpg);
            background-size: 115%;
            background-repeat: repeat-y;
        }
        .seccion_degradado, .seccion_degradado_2{
            background-image: url(${addons_path}/giscedata_facturacio_comer_ee/report/img/degradado_superior.jpg);
            background-repeat: repeat;
            background-position: left top;
            background-size: 100% 100%;
        }
        </style>
        <link href="${addons_path}/giscedata_facturacio_comer_ee/report/estils_ee.css" rel="stylesheet">
    </head>
    <body>
        %for inv in objects :
            <%
                sentido = 1.0
                if inv.type in['out_refund', 'in_refund']:
                    sentido = -1.0
                entidad_bancaria = ''
                bank_acc = ''
                if inv.payment_type:
                    if inv.payment_type.code == 'RECIBO_CSB':
                        if not inv.partner_bank:
                            raise Exception(
                                u'La factura {0} no tiene número de cuenta definido.'.format(
                                inv.number)
                            )
                        else:
                            entidad_bancaria = inv.partner_bank.name or inv.partner_bank.bank.lname
                            bank_acc = inv.partner_bank.iban[:-4].replace(' ','')  + '****'
                else:
                    if inv.partner_bank:
                        entidad_bancaria = inv.partner_bank.name or inv.partner_bank.bank.lname
                        bank_acc = inv.partner_bank.iban[:-4].replace(' ','') + '****'
                altres_lines = [l for l in inv.invoice_line]
            %>
            <div id="contenido">
                <div id="cabecera">
                    <div id="logo_superior_izq">
                        <img class="logo" src="data:image/jpeg;base64,${company.logo}" align="right">
                    </div>
                    <div id="logo_superior_derecha_degradado">
                        <div id="texto_degradado">
                            FACTURA
                        </div>
                    </div>
                </div>
                <div id="texto_vertical_lateral">
                    ${company.rml_footer2}
                </div>
                <div id="cabecera_datos_dir_envio">
                    <div id="cabecera_datos_factura">
                        <div class="texto-vertical_azul_col1 ajustes_vertical_col1_cabecera">
                           DATOS DE LA FACTURA
                        </div>
                        <table class="tabla_datos_factura mt_15">
                            <tr>
                                <td>Nº FACTURA</td>
                                <td class="field">${inv.number or ''}</td>
                            </tr>
                            <tr>
                                <td>FECHA FACTURA</td>
                                <td class="field">${localize_period(inv.date_invoice, 'es_ES')}</td>
                            </tr>
                            <tr>
                                <td style="font-size: 12pt">TOTAL</td>
                                <td class="field f_21">${formatLang(sentido * inv.amount_total, digits=2)} €</td>
                            </tr>
                            <tr>
                                <td>DOMICILIACIÓN</td>
                                %if inv.payment_type.code == 'RECIBO_CSB':
                                    <td class="field" style="font-size: 7pt;">${entidad_bancaria}</td>
                                %else:
                                    <td class="field" style="font-size: 7pt;">PAGO POR TRANSFERENCIA</td>
                                %endif
                            </tr>
                            <tr>
                                <td>Nº DE CUENTA</td>
                                %if inv.payment_type.code == 'RECIBO_CSB':
                                    <td class="field">${bank_acc}</td>
                                %else:
                                    <td class="field">${inv.payment_mode_id and inv.payment_mode_id.bank_id.iban or inv.payment_type.note}</td>
                                %endif
                            </tr>
                        </table>
                    </div>
                    <div id="cabecera_envio">
                        <div class="texto-vertical_azul_col2 ajustes_direccion_cliente">
                           DIRECCIÓN DEL CLIENTE
                        </div>
                        <table style="left: -18px;" class="tabla_datos_factura_derecha">
                            <tr>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="field f_10_5">
                                    ${inv.address_contact_id.name or ''}
                                </td>
                            </tr>
                            <tr>
                                <td class="field f_10_5">
                                    ${inv.address_contact_id.street or ''}
                                </td>
                            </tr>
                            <tr>
                                <td class="field f_10_5">
                                    ${inv.address_contact_id.zip or ''} ${inv.address_contact_id.city or ''}
                                </td>
                            </tr>
                             %if inv.address_contact_id.country_id.code != 'ES':
                                <tr>
                                    <td class="field f_10_5">
                                        ${inv.address_contact_id.country_id.name or ''}
                                    </td>
                                </tr>
                            %endif
                        </table>
                    </div>
                </div>
                <!-- Datos del titular -->
                <!-- Div datos de la factura y ademas los datos del cliente. -->
                <div style="clear: both;"></div>
                <div id="Cuerpo_DatosTitular_2">
                    <div id="cabecera_datos_titular">
                        <div class="texto-vertical_azul_col1 ajustes_vertical_col1_titular_2">
                           DATOS DE CLIENTE
                        </div>
                        <table class="tabla_datos_factura mt_10">
                            <tr>
                                <td>NOMBRE Y APELLIDOS</td>
                                <%
                                    dades_client = inv.address_invoice_id
                                    nom_client = dades_client.name or ''
                                    poblacio_client = ''
                                    provincia_client = ''
                                    if dades_client.id_poblacio:
                                        poblacio_client = dades_client.id_poblacio.name or ''
                                    if dades_client.state_id:
                                        provincia_client = dades_client.state_id.name or ''
                                %>
                                %if len(nom_client.rstrip()) > 30:
                                    <td class="field small_text_2">${nom_client}</td>
                                %else:
                                    <td class="field">${nom_client}</td>
                                %endif
                            </tr>
                            <tr>
                                <td>DNI/CIF</td>
                                <td class="field">${inv.partner_id.vat.replace('ES','')}</td>
                            </tr>
                            <tr>
                                <td>CÓDIGO POSTAL</td>
                                <td class="field">${dades_client.zip or ''}</td>
                            </tr>
                        </table>
                    </div>
                    <div id="cabecera_datos_titular_derecha">
                        <table class="tabla_datos_factura" style="position: relative; float: left; margin-top: 20px;">
                            <tr>
                                <td>DIRECCIÓN</td>
                                <td class="field">${dades_client.street or ''}</td>
                            </tr>
                            <tr>
                                <td>POBLACIÓN</td>
                                <td class="field">${poblacio_client}</td>
                            </tr>
                            <tr>
                                <td>PROVINCIA</td>
                                <td class="field">${provincia_client}</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div id="cuerpo">
                    <div id="calculo_factura">
                        <div class="texto-vertical_azul_col1 ajustes_vertical_col1_concepto_2">
                           CÁLCULO DE LA FACTURA
                        </div>
                        <table class="tabla_calculo_factura_conceptos">
                            <tr>
                                <td class="titulo_calculo_factura_conceptos">Descripción</td>
                                <td class="titulo_calculo_factura_conceptos align_right">Cantidad</td>
                                <td class="titulo_calculo_factura_conceptos align_right">Precio</td>
                                <td class="titulo_calculo_factura_conceptos align_right">Subtotal</td>
                            </tr>
                            %for l in altres_lines:
                                <tr>
                                    <td class="f_9">${l.name}</td>
                                    <td class="f_9 align_right">${formatLang(l.quantity, 3)}</td>
                                    <td class="f_9 align_right">${formatLang(l.price_unit, 2)} €</td>
                                    <td class="f_9 align_right">${formatLang(l.price_subtotal, 2)} €</td>
                                 </tr>
                                 %if l.note and add_line_note:
                                     <tr>
                                         <td class="f_9" width="60%"><p>${l.note.replace('\n','<br/>')}</p></td>
                                         <td class="f_9" colspan="3">&nbsp;</td>
                                     </tr>
                                 %endif
                             %endfor
                        </table>
                        <table class="tabla_calculo_factura_conceptos mt_10">
                            <tr>
                                <td style="font-size: 15px">${inv.name or ''}</td>
                            </tr>
                        </table>
                    </div>
                    <div id="contenido_totales_2">
                            <table id="totales">
                                %for tax in inv.tax_line:
                                    <tr>
                                        <td>${tax.name}</td>
                                        <td class="align_right">${format_currency(float(tax.base_amount), 'EUR', locale='es_ES')}</td>
                                        <td class="align_right">${format_currency(float(tax.tax_amount), 'EUR', locale='es_ES')}</td>
                                    </tr>
                                %endfor
                                <tr>
                                    <td colspan="2" class="field f_10">TOTAL FACTURA</td>
                                    <td class="field align_right f_10">${format_currency(float(inv.amount_total), 'EUR', locale='es_ES')}</td>
                                </tr>
                            </table>
                        </div>
                </div>
                <div id="pie_2">
                    <div class="texto-vertical_azul_col1 ajustes_vertical_col1_seccion_4_2">
                        INFORMACIÓN ÚTIL
                    </div>
                    <div class="container_texto">
                        <div class="texto_lopd pt_10">
                            En virtud de la Ley Orgánica 15/1999, de 13 de diciembre, de
                            Protección de Datos de Carácter Personal, le informamos
                            que todos los datos que usted nos ha facilitado para la
                            facturación de nuestros servicios están incluidos en un
                            fichero de datos personales, siendo responsable del mismo
                            ELECTRA ENERGÍA, S.A.U. para su tratamiento con la finalidad
                            de emisión de facturas, así como para mantener con Vd. una
                            relación comercial e informarle de las novedades relacionadas
                            con la compañía. Usted podrá ejercer sus derechos de acceso,
                            rectificación, cancelación y oposición dirigiéndose por escrito
                            al Servicio de Atención al Cliente/Facturación, <span class="nowrap">C/. Huerto de
                            Mas, 3 - 2º, 12002 - CASTELLÓN</span>
                        </div>
                    </div>
                </div>
                <p style="page-break-after:always"></p>
                <div style="clear: both;"></div>
        %endfor
        </div>
    </body>
</html>