# -*- coding: utf-8 -*-
{
    "name": "Account invoice (EDM)",
    "description": """Este módulo añade las siguientes funcionalidades:
  * Extiende account_invoice
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "account",
        "c2c_webkit_report"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "account_invoice_edm_report.xml"
    ],
    "active": False,
    "installable": True
}
