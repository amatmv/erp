# -*- coding: utf-8 -*-
{
    "name": "Facturació distribuidora EDM",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends": [
        "base",
        "account_chart_distri_edm",
        "giscedata_facturacio_distri",
        "giscedata_contractacio_distri",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "giscedata_facturacio_distri_edm_data.xml",
        "giscedata_facturacio_distri_edm_view.xml"
    ],
    "active": False,
    "installable": True
}
