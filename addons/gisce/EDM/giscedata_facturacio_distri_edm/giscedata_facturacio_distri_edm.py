# -*- coding: utf-8 -*-
from osv import osv, fields
from osv.expression import OOQuery
import pooler

class GiscedataFacturacioContracteLot(osv.osv):
    _name = 'giscedata.facturacio.contracte_lot'
    _inherit = 'giscedata.facturacio.contracte_lot'

    def _find_zona_polissa(cursor, model, ids, variable=None, algo2=None, context=None):
        """Find Zona TPL in cups
        :param cursor: Database cursor
        :param ids: contracte_lot id list
        :param context: Application context
        :returns zona TPL
        """
        result = dict.fromkeys(ids, '')
        pool = pooler.get_pool(cursor.dbname)
        contracte_lot_obj = pool.get('giscedata.facturacio.contracte_lot')

        q = OOQuery(contracte_lot_obj, cursor, ids)
        sql = q.select(['id', 'polissa_id.cups.zona']) \
            .where([
            ('id', 'in', ids)
        ])
        cursor.execute(*sql)
        tpv_bls = cursor.dictfetchall()
        for tpv in tpv_bls:
            contracte_lot_id = tpv["id"]
            zona_query = tpv["polissa_id.cups.zona"]
            result[contracte_lot_id] = zona_query

        return result

    def _find_origen_polissa(cursor, model, ids, variable=None, algo2=None, context=None):
        """Find Origin  lecturas in Contador
        :param cursor: Database cursor
        :param ids: contracte_lot id list
        :param context: Application context
        :returns origen lecturas
        """
        result = dict.fromkeys(ids, '')
        pool = pooler.get_pool(cursor.dbname)
        contracte_lot_obj = pool.get('giscedata.facturacio.contracte_lot')

        q = OOQuery(contracte_lot_obj, cursor, ids)
        sql = q.select(['lot_id.data_inici', 'lot_id.data_final']) \
            .where([('id', '=', ids[0])])
        cursor.execute(*sql)
        fechas = cursor.dictfetchall()
        data_inicio = fechas[0]["lot_id.data_inici"]
        data_final = fechas[0]["lot_id.data_final"]

        search_params = [
            ('id', 'in', ids),
            # ('polissa_id.comptadors.active', '=', True),
            ('polissa_id.comptadors.lectures.name', '>=', data_inicio),
            ('polissa_id.comptadors.lectures.name', '<=', data_final)
        ]
        q = OOQuery(contracte_lot_obj, cursor, ids)
        sql = q.select(['id', 'polissa_id.comptadors.lectures.origen_id.name']) \
            .where(search_params)
        cursor.execute(*sql)
        origen_bls = cursor.dictfetchall()
        for origen in origen_bls:
            contracte_lot_id = origen["id"]
            origen_query = origen["polissa_id.comptadors.lectures.origen_id.name"]
            origen_result = result[contracte_lot_id]
            if not origen_query in origen_result:
                if len(origen_result) == 0:
                    result[contracte_lot_id] = origen_query
                else:
                    result[contracte_lot_id] = origen_result + '-' + origen_query

        return result

    _columns = {
        'zona_tpl': fields.function(_find_zona_polissa, type='string', string=u'Zona'),
        'origen_lectura': fields.function(_find_origen_polissa, type='string', string=u'Origen Lectura')

    }


GiscedataFacturacioContracteLot()


