# -*- coding: utf-8 -*-
import base64

from osv import osv, fields
from datetime import datetime
import pandas as pd


class WizardExportAccountMoves(osv.osv_memory):
    _name = 'wizard.export.account.moves'
    _inherit = 'wizard.export.account.moves'

    def build_report_csv(self, rows, headers=None):
        df = pd.DataFrame(rows, columns=headers)
        return df.to_csv(
            index=False, header=False, sep=';', line_terminator=';\r\n')

    def export_account_moves_data(self, cursor, uid, wiz_id, acc_move_line_data,
                                  context=None):
        if context is None:
            context = {}

        wiz = self.browse(cursor, uid, wiz_id)
        initial_date = wiz.initial_date
        end_date = wiz.end_date

        # Set filenames
        filename_template = (
            'archivo_{tipo_archivo}_{f_inicio}_{f_fin}.{file_type}'
        )
        date_format = '%Y%m%d'
        vals = dict(
            f_inicio=datetime.strptime(
                initial_date, '%Y-%m-%d').strftime(date_format),
            f_fin=datetime.strptime(
                end_date, '%Y-%m-%d').strftime(date_format),
            file_type='csv',
        )
        vals.update({'tipo_archivo': 'A'})
        filename_A = filename_template.format(**vals)
        vals.update({'tipo_archivo': 'D'})
        filename_D = filename_template.format(**vals)

        # Get account move lines information
        acc_move_line_obj = self.pool.get('account.move.line')

        archivo_A_results = []
        archivo_D_results = []
        seen_accounts = set()
        formato_fecha = '%d/%m/%y'
        acc_move_ids = []
        # Agrupación de la cuenta 705000004 en un único movimiento por factura (lines de energia, potencia, etc.)
        acc_move_line_data = self.process_join_peatges(cursor, uid, acc_move_line_data, context=context)
        for acc_move_line in acc_move_line_data:
            codigo_cliente = acc_move_line['partner_id.codigo_contable'] or ''
            contador = acc_move_line['move_id']
            acc_move_ids.append(contador)
            fecha = datetime.strptime(
                acc_move_line['date'], '%Y-%m-%d'
            ).strftime(formato_fecha)
            codigo_cuenta_base = acc_move_line['account_id.code']
            # if len(codigo_cuenta_base) > 4:
            #     codigo_cuenta_base = acc_move_line['account_id.parent_id.code']
            codigo_cuenta = False
            # Si la cuenta es una cuenta de cliente
            if (codigo_cuenta_base.startswith('43') or
                    codigo_cuenta_base.startswith('41')):
                if codigo_cuenta_base.startswith('4315'):
                    # Sustituir cuenta 4315 por 4300
                    codigo_cuenta_base = '4300' + codigo_cuenta_base[4:]
                # Facturas de contratacion de EESAU
                if acc_move_line['partner_id.vat'] == 'ESA12542296':
                    new_acc_move_line = False
                    factura_contratacion = acc_move_line['ref'].startswith('MD')
                    cash_journal = acc_move_line['journal_id.type'] == 'cash'
                    pago_factura_contratacion = (
                        factura_contratacion and cash_journal
                        and acc_move_line['reconcile_id'] and acc_move_line['credit']
                    )
                    if pago_factura_contratacion:
                        search_params = [
                            ('id', '!=', acc_move_line['id']),
                            ('reconcile_id', '=', acc_move_line['reconcile_id'])
                        ]
                        new_acc_move_line = acc_move_line_obj.q(
                            cursor, uid
                        ).read(['account_id.code']).where(search_params)
                    journal_contratacion = (
                        acc_move_line['journal_id.code'].startswith('CONTRATACION')
                    )
                    if journal_contratacion or new_acc_move_line:
                        codigo_cuenta = '433000002'
                if not codigo_cuenta:
                    codigo_cuenta = (
                        codigo_cuenta_base.ljust(4, '0')[:4] + codigo_cliente.ljust(5, '0')
                    )
            else:
                codigo_cuenta = codigo_cuenta_base.ljust(9, '0')

            # Si es una cuenta de cliente se pone como descripción el nombre
            # del cliente
            # Si es otra cuenta se pone como descripción el nombre de la cuenta
            if codigo_cuenta.startswith('43'):
                descripcion_cuenta = acc_move_line['partner_id.name']
            else:
                descripcion_cuenta = acc_move_line['account_id.name']
            concepto = acc_move_line['name']
            if acc_move_line['credit'] >= acc_move_line['debit']:
                importe = str(acc_move_line['credit'] - acc_move_line['debit']).replace('.', ',')
                debe_haber = 'H'
            else:
                importe = str(acc_move_line['debit'] - acc_move_line['credit']).replace('.', ',')
                debe_haber = 'D'
            referencia = acc_move_line['ref']
            if concepto == '/':
                concepto = 'Número de factura {}'.format(referencia)

            row = [contador, fecha, codigo_cuenta, concepto,
                   importe, debe_haber, referencia]
            archivo_D_results.append(row)
            if codigo_cuenta not in seen_accounts:
                archivo_A_results.append([codigo_cuenta, descripcion_cuenta])
                seen_accounts.add(codigo_cuenta)

        archivo_A = self.build_report_csv(rows=archivo_A_results)
        archivo_D = self.build_report_csv(rows=archivo_D_results)

        # self.mark_as_exported(cursor, uid, acc_move_ids)

        wiz.write({'archivo_A': base64.b64encode(archivo_A),
                   'archivo_A_filename': filename_A,
                   'archivo_D': base64.b64encode(archivo_D),
                   'archivo_D_filename': filename_D,
                   'state': 'end'})

    def process_join_peatges(self, cursor, uid, rows, context=None):
        ctx = context.copy()
        return self.process_join_rows(
            cursor, uid, rows, join={'credit': 'sum', 'debit': 'sum', 'name': lambda x: ','.join(x), 'id': lambda x: ','.join(str(x))},
            where=('account_id.code', '==', '705000004'), context=ctx
        )

    _columns = {
        'archivo_A': fields.binary(string='Archivo A (Plan contable)',
                                   readonly=True),
        'archivo_A_filename': fields.char('Nombre archivo A', size=128),
        'archivo_D': fields.binary(string='Archivo D (Asientos ordinarios)',
                                   readonly=True),
        'archivo_D_filename': fields.char('Nombre archivo D', size=128),
    }


WizardExportAccountMoves()
