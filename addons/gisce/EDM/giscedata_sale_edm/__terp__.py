# -*- coding: utf-8 -*-
{
    "name": "GISCE-EDM Sale",
    "description": """Extensió del mòdul giscedata_sale per a EDM.""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE",
    "depends":[
        "partner_address_tipovia",
        "giscedata_sale",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_sale_edm_view.xml",
        "giscedata_sale_edm_report.xml",
    ],
    "active": False,
    "installable": True
}
