# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* giscedata_sale_edm
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.14\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2018-12-27 17:26\n"
"PO-Revision-Date: 2018-12-27 17:26\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: giscedata_sale_edm
#: view:sale.order:0
msgid "Trabajos"
msgstr ""

#. module: giscedata_sale_edm
#: constraint:ir.ui.view:0
msgid "Invalid XML for View Architecture!"
msgstr ""

#. module: giscedata_sale_edm
#: model:ir.module.module,shortdesc:giscedata_sale_edm.module_meta_information
msgid "GISCE-EDM Sale"
msgstr ""

#. module: giscedata_sale_edm
#: model:ir.module.module,description:giscedata_sale_edm.module_meta_information
msgid "Extensió del mòdul giscedata_sale per a EDM."
msgstr ""

