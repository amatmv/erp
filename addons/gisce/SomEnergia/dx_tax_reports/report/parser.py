# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2010 Domatix Technologies  S.L. (http://www.domatix.com) 
#                       info <info@domatix.com>
#                        Angel Moya <angel.moya@domatix.com>
#
#        $Id$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from datetime import date
from dateutil.parser import parse
from dateutil.relativedelta import relativedelta
from operator import itemgetter

from report import report_sxw
import pooler

class Parser(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(Parser, self).__init__(cr, uid, name, context=context)
        self.init_bal_sum = 0.0
        self.total = []
        self.localcontext.update( {
            'date_from': self._date_from,
            'date_to': self._date_to,
            'fiscalyear': self._fiscalyear,
            'period': self._period,
            'tax_amount_get': self._tax_amount_get,
            'resume_tax_amount_get': self._resume_tax_amount_get,
            'invoice_sales_get': self._invoice_sales_get,
            'invoice_purchases_get': self._invoice_purchases_get,
            'invoice_resume_get': self._invoice_resume_get,
        })
    
    
    def _date_from(self, time_filter_by, fiscalyear, period, date_from):
        
        result = False
        if time_filter_by == 'dates':
            result = date_from
        elif time_filter_by == 'period':
            result = period.date_start
        elif time_filter_by == 'fiscalyear':
            result = fiscalyear.date_start
            
        return result 
    
    def _date_to(self, time_filter_by, fiscalyear, period, date_to):
        
        result = False
        if time_filter_by == 'dates':
            result = date_to
        elif time_filter_by == 'period':
            result = period.date_stop
        elif time_filter_by == 'fiscalyear':
            result = fiscalyear.date_stop
            
        return result 
    
    def _period(self, time_filter_by, period):
        result = ''
        if time_filter_by == 'period':
            result = period.name
        return result 
    
    def _fiscalyear(self, time_filter_by, fiscalyear, period):
        result = ''
        if time_filter_by == 'fiscalyear':
            result = fiscalyear.name
        elif time_filter_by == 'period':
            result = period.fiscalyear_id.name
        return result 
    
    def _tax_amount_get(self, invoice, group):
        tax_amount = 0.0
        if invoice.tax_summary:
            for tax_summary in invoice.tax_summary:
                if tax_summary.tax_code_group_id.id == group.id:
                    tax_amount = tax_summary.tax_amount
        return tax_amount 
    
    def _resume_tax_amount_get(self, code_id, group, time_filter_by, fiscalyear, period, date_from, date_to):
        invoice_obj = pooler.get_pool(self.cr.dbname).get('account.invoice')
        code_obj = pooler.get_pool(self.cr.dbname).get('account.tax.code')

        tax_amount = 0.0
        
        
        date_start = False
        if time_filter_by == 'dates':
            date_start = date_from
        elif time_filter_by == 'period':
            date_start = period.date_start
        elif time_filter_by == 'fiscalyear':
            date_start = fiscalyear.date_start
        
        
        date_stop = False
        if time_filter_by == 'dates':
            date_stop = date_to
        elif time_filter_by == 'period':
            date_stop = period.date_stop
        elif time_filter_by == 'fiscalyear':
            date_stop = fiscalyear.date_stop
        
        filter = [('state','in',('open','paid')),]
        
        if date_start:
            filter.append(('date_invoice','>=',date_start))
            
        if date_stop:
            filter.append(('date_invoice','<=',date_stop))
        
        
        invoice_ids = invoice_obj.search(self.cr, self.uid, filter)
        code_ids = code_obj.search(self.cr, self.uid, [('tax_code_group_id','=',group.id),])
        
        sql="""select   sum(ait2.tax_amount) as tax_amount
                from    account_invoice_tax ait, account_invoice_tax ait2
                where   ait.invoice_id = ait2.invoice_id 
                        and ait.base_amount = ait2.base_amount
                        and ait.tax_code_id != ait2.tax_code_id
                        and ait.tax_code_id = %s
                        and ( ait2.tax_code_id in %s )
                        and ( ait2.invoice_id in %s )
                group by ait.tax_code_id, ait2.tax_code_id"""
        
        self.cr.execute(sql,[code_id,tuple(code_ids),tuple(invoice_ids),])
        
        #return self.cr.fetchone()[0] or 0.0
        result = self.cr.dictfetchall()
        if isinstance(result, (list)) and len(result) > 0 :
            result = result[0]
        if isinstance(result, (dict)) and 'tax_amount' in result:
            amount = result['tax_amount']
        else:
            amount = 0.0
        return amount
    
    
    def _invoice_sales_get(self, time_filter_by, fiscalyear, period, date_from, date_to):
        invoice_obj = pooler.get_pool(self.cr.dbname).get('account.invoice')
        
        date_start = False
        if time_filter_by == 'dates':
            date_start = date_from
        elif time_filter_by == 'period':
            date_start = period.date_start
        elif time_filter_by == 'fiscalyear':
            date_start = fiscalyear.date_start
        
        date_stop = False
        if time_filter_by == 'dates':
            date_stop = date_to
        elif time_filter_by == 'period':
            date_stop = period.date_stop
        elif time_filter_by == 'fiscalyear':
            date_stop = fiscalyear.date_stop
        
        filter = [('state','in',('open','paid')),('type','in',('out_invoice','out_refund'))]
        
        if date_start:
            filter.append(('date_invoice','>=',date_start))
            
        if date_stop:
            filter.append(('date_invoice','<=',date_stop))
        
        invoice_ids = invoice_obj.search(self.cr, self.uid, filter, order = 'date_invoice')
        
        return  invoice_obj.browse(self.cr, self.uid, invoice_ids)
    
    def _invoice_purchases_get(self, time_filter_by, fiscalyear, period, date_from, date_to):
        invoice_obj = pooler.get_pool(self.cr.dbname).get('account.invoice')
        
        date_start = False
        if time_filter_by == 'dates':
            date_start = date_from
        elif time_filter_by == 'period':
            date_start = period.date_start
        elif time_filter_by == 'fiscalyear':
            date_start = fiscalyear.date_start
        
        date_stop = False
        if time_filter_by == 'dates':
            date_stop = date_to
        elif time_filter_by == 'period':
            date_stop = period.date_stop
        elif time_filter_by == 'fiscalyear':
            date_stop = fiscalyear.date_stop
        
        filter = [('state','in',('open','paid')),('type','in',('in_invoice','in_refund'))]
        
        if date_start:
            filter.append(('date_invoice','>=',date_start))
            
        if date_stop:
            filter.append(('date_invoice','<=',date_stop))
        
        invoice_ids = invoice_obj.search(self.cr, self.uid, filter, order = 'sequence_number,date_invoice')
        
        return  invoice_obj.browse(self.cr, self.uid, invoice_ids)
  
    
    def _invoice_resume_get(self, time_filter_by, fiscalyear, period, date_from, date_to, code_filter):
        invoice_obj = pooler.get_pool(self.cr.dbname).get('account.invoice')
        
        date_start = False
        if time_filter_by == 'dates':
            date_start = date_from
        elif time_filter_by == 'period':
            date_start = period.date_start
        elif time_filter_by == 'fiscalyear':
            date_start = fiscalyear.date_start
        
        
        date_stop = False
        if time_filter_by == 'dates':
            date_stop = date_to
        elif time_filter_by == 'period':
            date_stop = period.date_stop
        elif time_filter_by == 'fiscalyear':
            date_stop = fiscalyear.date_stop
        
        filter = [('state','in',('open','paid')),]
        
        
        if date_start:
            filter.append(('date_invoice','>=',date_start))
            
        if date_stop:
            filter.append(('date_invoice','<=',date_stop))
        
        invoice_ids = invoice_obj.search(self.cr, self.uid, filter)
        
        code_filter_ids = []
        for f in code_filter.tax_code_ids:
            code_filter_ids.append(f.id)
        
        sql = """(
                select atc.id as code_id,
                        atc.name as code,
                        sum(ait.base_amount) as base_amount,
                        sum(ait.tax_amount) as tax_amount
                from    account_invoice_tax ait, account_tax_code atc
                where   ait.tax_code_id = atc.id
                        and ( ait.invoice_id in %s )
                        and ( ait.tax_code_id in %s )
                group by atc.name, atc.id
                order by atc.name
                ) union (
                select atc.id as code_id,
                        atc.name as code,
                        sum(ait.base_amount) as base_amount,
                        0 as tax_amount
                from    account_invoice_tax ait, account_tax_code atc
                where   ait.base_code_id = atc.id
                        and ( ait.invoice_id in %s )
                        and ( ait.tax_code_id in %s )
                group by atc.name, atc.id
                order by atc.name
                )"""

        #   
        self.cr.execute(sql,[tuple(invoice_ids),tuple(code_filter_ids),tuple(invoice_ids),tuple(code_filter_ids),])
        
        #self.cr.execute(sql)

        return self.cr.dictfetchall()


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

