# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2010 Domatix Technologies  S.L. (http://www.domatix.com) 
#                       info <info@domatix.com>
#                        Angel Moya <angel.moya@domatix.com>
#
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from osv import fields,osv

from tools import config

import tools

class account_invoice_tax(osv.osv):
    _inherit = "account.invoice.tax"
    
    _columns = {
                'tax_code_group_id': fields.related('tax_code_id', 'tax_code_group_id', type='many2one', relation='tax.code.group', string='Tax Code Group', readonly=True, store=True),
                }
account_invoice_tax()
    
class account_invoice(osv.osv):
    _inherit = "account.invoice"
    
    def _taxes_label(self, cr, uid, ids, name, args, context=None):
        res = {}
        for invoice in self.browse(cr, uid, ids, context=context):
            res[invoice.id] = ''
            if invoice.tax_line:
                for tax_line in invoice.tax_line:
                    if tax_line.amount >= 0 :
                        res[invoice.id] += '+ '
                    else:
                        res[invoice.id] += '- ' 
                    res[invoice.id] += tax_line.name + ' '

        return res

    def create(self, cursor, uid, vals=None, context=None):
        """Es treu el valor de tax_summary per tractar-se d'una vista
        """
        if vals is None:
            vals = {}
        vals = vals.copy()
        vals.update({'tax_summary': []})
        res_id = super(account_invoice,
                       self).create(cursor, uid, vals, context)
        return res_id

    _columns = {
                'taxes_label': fields.function(_taxes_label, method=True,  store=True, type="char", size=256,string='Taxes'),
                'tax_summary': fields.one2many('invoice.tax.summary', 'invoice_id', 'Taxes Summary', readonly=True),
                }


account_invoice()



class invoice_tax_summary(osv.osv):
    _name = "invoice.tax.summary"
    _description = "Tax Summary"
    _auto = False
    
    _columns = {
                'tax_code_group_id': fields.many2one('tax.code.group', 'Tax Code Group'),
                'invoice_id': fields.many2one('account.invoice', 'Tax Code Group'),
                'tax_amount': fields.float('Tax Code Amount', digits=(16, int(config['price_accuracy']))),
        }
    
    _rec_name = 'tax_code_group_id'
    
    
    def init(self, cr):
        
        tools.drop_view_if_exists(cr, 'invoice_tax_summary')
        cr.execute("""
        create or replace view invoice_tax_summary as (
            select (max(ait.id)*100)+(tcg.id) as id,
                ait.invoice_id ,
                tcg.id as tax_code_group_id,
                sum(case when ait.tax_code_group_id = tcg.id then ait.tax_amount else 0 end) as tax_amount
            from     tax_code_group tcg,
                account_invoice_tax ait 
            group by ait.invoice_id, tcg.id
        )""")
        
invoice_tax_summary()
