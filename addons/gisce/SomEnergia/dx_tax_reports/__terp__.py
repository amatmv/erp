# -*- coding: utf-8 -*-
{
    "name": "Dx Tax Reports",
    "description": """
        *** Dx Tax Reports
    """,
    "version": "0-dev",
    "author": "Domatix",
    "category": "Reporting",
    "depends":[
        "dx_invoice_supplier_renumber",
        "report_aeroo"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "report/invoice_customer_tax_list.xml",
        "report/invoice_supplier_tax_list.xml",
        "report/invoice_tax_resume.xml",
        "tax_code_group_view.xml",
        "tax_code_filter_view.xml",
        "invoice_view.xml",
        "wizards_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
