# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2010 Domatix Technologies  S.L. (http://www.domatix.com) 
#                       info <info@domatix.com>
#                        Angel Moya <angel.moya@domatix.com>
#
#        $Id$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

import time
from datetime import datetime
from dateutil.relativedelta import relativedelta
from operator import itemgetter
from osv import osv, fields

class invoice_tax_resume(osv.osv_memory): 
    _name = 'invoice.tax.resume'
    _description = 'Invoice Tax resume'
    
    _columns = {
        'company_id': fields.many2one('res.company', 'Company', required=True),
        'time_filter_by': fields.selection([
                            ('fiscalyear','Fiscal Year'),
                            ('period','Period'),
                            ('dates','Dates'),
                            ('none','None'),
                            ],'Time Filter by', required=True),
        'period_id': fields.many2one('account.period', 'Period'),
        'fiscalyear_id': fields.many2one('account.fiscalyear', 'Fiscal Year'),
        'date_from': fields.date('Date From'),
        'date_to': fields.date('Date To'),
        'resume_tax_code_group_ids': fields.many2many('tax.code.group', 'resume_tax_group_rel', 'tax_code_group_id', 'wizard_id',  'Tax Groups'),
        'tax_filter_id': fields.many2one('tax.code.filter', 'Tax Filter', required=True),
    }
        
    
### Function to prepare data for sending to report
        
    def resume_report(self, cr, uid, ids, context=None):
        """
        Prepares data for sending to parser.
        @return: report object res.partner.statement
        """
        if context is None:
            context = {}
        data = {}
        
        data['ids'] = ids
        data['id'] = ids[0]
        data['model'] = 'invoice.tax.resume'
        
        data['form'] = self.read(cr, uid, ids, ['date_from', 'date_to'], context=context)[0]
        context.update(data)
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'invoice.tax.resume',
            'datas': data,
            'context': context,
            }
  
invoice_tax_resume()

