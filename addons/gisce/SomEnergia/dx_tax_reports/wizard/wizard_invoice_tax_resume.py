# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2010 Domatix Technologies  S.L. (http://www.domatix.com) 
#                       info <info@domatix.com>
#                        Angel Moya <angel.moya@domatix.com>
#
#        $Id$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

import wizard
import pooler

_invoice_tax_resume_form  = '''<?xml version="1.0"?>
                <form string="Report Options">
                <field name="company_id"/>
                <separator string="Time Filter" colspan="4"/>
                
                <group colspan="4" col="6">
                    <field name="time_filter_by"/>
                    <group colspan="4" col="4">
                        <group colspan="2" col="2"  attrs="{'invisible':[('time_filter_by','!=','period')]}">
                            <field name="period_id" />
                        </group>
                        <group colspan="2" col="2"  attrs="{'invisible':[('time_filter_by','!=','fiscalyear')]}">
                            <field name="fiscalyear_id" />
                        </group>
                        <group colspan="4" col="4"  attrs="{'invisible':[('time_filter_by','!=','dates')]}">
                            <field name="date_from"/>
                            <field name="date_to"/>
                        </group>
                    </group>
                </group>
                   
                <separator string="Tax Filter" colspan="4"/>
                <field name="tax_filter_id"/>
                <newline/>
                   
                <separator string="Tax Groups in resume resume columns" colspan="4"/>
                <group colspan="4" col="4">
                    <field name="resume_tax_code_group_ids" colspan="4" nolabel="1"/>
                </group>
                           
                </form>
'''
_invoice_tax_resume_fields  = {
            'company_id': {
                'string': 'Company',
                'type': 'many2one',
                'relation': 'res.company'
        },
            'period_id': {
                'string': 'Period',
                'type': 'many2one',
                'relation': 'account.period'
        },
            'fiscalyear_id': {
                'string': 'Fiscal year',
                'type': 'many2one',
                'relation': 'account.fiscalyear',
                'help': 'Keep empty for all open fiscal year',
        },
            'date_from': {
                'string': 'Date from',
                'type': 'date',
        },
            'date_to': {
                'string': 'Date To',
                'type': 'date',
        },
            'time_filter_by': {
                'string': 'Time Filter by',
                'type': 'selection',
                'selection': [('fiscalyear','Fiscal Year'),('period','Period'),('dates','Dates'),('none','None'),],
                'required': True,
                'default': lambda *a:"none",
        },
            'resume_tax_code_group_ids': {
                'string': 'Tax Groups',
                'type': 'many2many',
                'relation': 'tax.code.group'
        },
            'tax_filter_id': {
                'string': 'Tax Filter',
                'type': 'many2one',
                'relation': 'tax.code.filter'
        },
    }
    
def _get_defaults(self, cr, uid, data, context):
        fiscalyear_obj = pooler.get_pool(cr.dbname).get('account.fiscalyear')
        resume_tax_code_group_ids = []
        for group_id in pooler.get_pool(cr.dbname).get('tax.code.group').search(cr, uid, [('show_on_resume','=',True),] ,context=context):
            resume_tax_code_group_ids.append(group_id)
        data['form']['fiscalyear_id'] = fiscalyear_obj.find(cr, uid)
        data['form']['company_id'] = pooler.get_pool(cr.dbname).get('res.users').browse(cr,uid, uid).company_id.id
        data['form']['resume_tax_code_group_ids'] = resume_tax_code_group_ids
        return data['form']
    
### Function to prepare data for sending to report
        
def _resume_report(self, cr, uid, data, context=None):
        """
        Prepares data for sending to parser.
        @return: report object res.partner.statement
        """
        if context is None:
            context = {}
        
        invoice_tax_resume_obj = pooler.get_pool(cr.dbname).get('invoice.tax.resume')
        
        resume_data = {
                        'company_id': data['form']['company_id'],
                        'time_filter_by': data['form']['time_filter_by'],
                        'fiscalyear_id': data['form']['fiscalyear_id'],
                        'date_from': data['form']['date_from'],
                        'date_to': data['form']['date_to'],
                        'resume_tax_code_group_ids': data['form']['resume_tax_code_group_ids'],
                        'tax_filter_id': data['form']['tax_filter_id'],
                        }
        
        invoice_tax_resume_id = invoice_tax_resume_obj.create(cr,uid,resume_data,context=context)
        
        return invoice_tax_resume_obj.resume_report(cr,uid,[invoice_tax_resume_id],context=context)
        
        
        
  
class wizard_invoice_tax_resume(wizard.interface):
    states = {
        'init': {
            'actions': [_get_defaults],
            'result': {'type': 'form', 'arch':_invoice_tax_resume_form, 'fields':_invoice_tax_resume_fields, 'state': [('end', 'Cancel'), ('print', 'Print')]}
        },
        'print': {
            'actions': [],
            'result': {'type': 'action', 'action':_resume_report, 'state':'end'}
        }
    }
wizard_invoice_tax_resume('invoice.tax.resume')

