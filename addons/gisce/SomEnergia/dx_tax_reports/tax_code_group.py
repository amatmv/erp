# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2010 Domatix Technologies  S.L. (http://www.domatix.com) 
#                       info <info@domatix.com>
#                        Angel Moya <angel.moya@domatix.com>
#
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from osv import fields,osv

class tax_code_group(osv.osv):
    _name = "tax.code.group"
    _description = "Tax Code Group"
    
    _columns = {
                'name': fields.char('Name', size = 128 ),
                'label': fields.char('Label', size = 64 ),
                'tax_code_ids': fields.one2many('account.tax.code', 'tax_code_group_id', 'Taxe Codes'),
                'show_on_list': fields.boolean('Show on list'),
                'show_on_resume': fields.boolean('Show on resume')
                
        }


tax_code_group()


class account_tax_code(osv.osv):
    _inherit = "account.tax.code"
    
    _columns = {
                'tax_code_group_id': fields.many2one('tax.code.group', 'Tax Code Group'),
                
        }
account_tax_code()

    