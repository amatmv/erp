# -*- coding: utf-8 -*-
from osv import osv, fields
from datetime import datetime
from tools.translate import _


class GiscedataSwitchingHelpers(osv.osv):

    _name = 'giscedata.switching.helpers'
    _inherit = 'giscedata.switching.helpers'

    def tancar_reclamacio(self, cursor, uid, sw_id, context=None):
        if context is None:
            context = {}
        res = super(GiscedataSwitchingHelpers, self).tancar_reclamacio(
            cursor, uid, sw_id, context=context
        )
        
        if len(res) and res[0] == "ERROR":
            return res

        sw_obj = self.pool.get('giscedata.switching')
        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        polissa = sw.cups_polissa_id
        pas = sw.get_pas()

        autom_r1_fact = pas.subtipus_id.type == '02' and (
            sw.state == "pending" or
            (sw.state == 'open' and pas.resultat == "01" and not polissa.refacturacio_pendent)
        )

        if pas.subtipus_id.type != '02' or autom_r1_fact:
            polisses_obj = self.pool.get('giscedata.polissa')
            imd_obj = self.pool.get('ir.model.data')
            categoria_xml_id = 'som_sw_reclamacions_lectura_en_curs'
            categoria_id = imd_obj.get_object_reference(
                cursor, uid, 'som_polissa_soci', categoria_xml_id)[1]
            categories_ids = [
                c.id for c in polissa.category_id if c.id != categoria_id
            ]
            polisses_obj.write(
                cursor, uid, [polissa.id], {
                    'category_id': [(6, 0, categories_ids)],
                }
            )
            # No hi hauria d'haver contractes d'alta sense lot.
            # Abans n'hi havia, peró amb la facturacio suspesa es va deixar
            # de treure contractes del lot
            if not polissa.lot_facturacio:
                polissa.assignar_seguent_lot()

        if autom_r1_fact:
            subtipus_info_str = "{0}-{1}".format(pas.subtipus_id.type, pas.subtipus_id.name)
            user_obj = self.pool.get("res.users")
            user_name = user_obj.read(cursor, uid, uid, ['name'])['name']
            sw_code = sw.codi_sollicitud
            new_text = _(
                u"\n{0} {1} Reclamació {2} SUBTIPUS {3}. R105 Procedent/Improcedent\n").format(
                datetime.today().strftime("%d-%m-%Y"), user_name, sw_code,
                subtipus_info_str
            )
            polissa.write({
                'observacions': (polissa.observacions or "") + new_text,
                'facturacio_suspesa': False,
                'refacturacio_pendent': False
            })
            sw.case_close()
        return res

GiscedataSwitchingHelpers()
