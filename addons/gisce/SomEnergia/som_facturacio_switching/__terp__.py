# -*- coding: utf-8 -*-
{
    "name": "Funcions de suport a switching per SOM",
    "description": """
    This module provide :
        * Funció propia a pólissa (escull_llista_preus) per escollir tarifa a partir llistes de preus.
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "SomEnergia",
    "depends":[
        "base",
        "giscedata_switching"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
