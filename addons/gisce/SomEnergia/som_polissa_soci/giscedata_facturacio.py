# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _
from datetime import datetime, date


class GiscedataFacturacioFacturador(osv.osv):
    """Sobreescrivim el mètode per facturar per incloure el donatiu"""

    _name = 'giscedata.facturacio.facturador'
    _inherit = 'giscedata.facturacio.facturador'

    def get_donatiu_product(self, cursor, uid, polissa_id, context=None):
        product_obj = self.pool.get('product.product')
        return product_obj.search(
            cursor, uid, [('default_code', '=', 'DN01')]
        )[0]

    def fact_via_lectures(self, cursor, uid, polissa_id, lot_id, context=None):
        """Sobreescrivim el mètode per incloure-hi la línia de factura
           rel·lacionada amb el donatiu si s'escau"""
        if not context:
            context = {}
        fact_obj = self.pool.get('giscedata.facturacio.factura')
        journal = self.pool.get('account.journal')
        product_obj = self.pool.get('product.product')

        factures_creades = super(GiscedataFacturacioFacturador, self).\
                            fact_via_lectures(cursor, uid, polissa_id, lot_id,
                                              context)
        ctx = context.copy()
        # identificar el producte donacions
        pdona_id = self.get_donatiu_product(cursor, uid, polissa_id, context)

        # identificar el producte Bo Social
        pbosocial_ids = product_obj.search(
            cursor, uid, [('default_code', '=', 'BS01')]
        )
        active_bo_social = int(self.pool.get('res.config').get(
            cursor, uid, 'som_invoice_active_bo_social', '0'
        ))
        start_date_bo_social = self.pool.get('res.config').get(
            cursor, uid, 'som_invoice_start_date_bo_social', '2017-07-01'
        )
        pbosocial_id = len(pbosocial_ids) and pbosocial_ids[0]
        fes_bo_social = pbosocial_id and active_bo_social or False
        for fact in fact_obj.browse(cursor, uid, factures_creades, context):
            if fact.journal_id.code in ('ENERGIA', 'ENERGIA.R'):
                if fact.polissa_id.donatiu:
                    # si la factura és del diari d'energia s'afegeix la línia de
                    # factura rel·lacionada amb el producte donatiu
                    ctx.update({'lang': fact.partner_id.lang})
                    p_br = product_obj.browse(cursor, uid, pdona_id, ctx)
                    # identificar la línia d'energia
                    kwh = sum([x.quantity for x in fact.linia_ids
                                                   if x.tipus == 'energia'])
                    vals = {'data_desde': fact.data_inici,
                            'data_fins': fact.data_final,
                            'force_price': 0.01,
                            'uos_id': p_br.uom_id.id,
                            'quantity': kwh,
                            'multi': 1,
                            'product_id': pdona_id,
                            'tipus': 'altres',
                            'name': p_br.description}
                    self.crear_linia(cursor, uid, fact.id, vals, ctx)
                # Only invoices with start date after 'start_date_bo_social'
                if fes_bo_social and fact.data_inici >= start_date_bo_social:
                    # si la factura és del diari d'energia s'afegeix la línia de
                    # factura rel·lacionada amb el producte Bo Social
                    ctx.update({'lang': fact.partner_id.lang})
                    p_br = product_obj.browse(cursor, uid, pbosocial_id, ctx)
                    dies_bo = (
                        datetime.strptime(fact.data_final, '%Y-%m-%d') -
                        datetime.strptime(fact.data_inici, '%Y-%m-%d')
                    ).days + 1
                    vals = {'data_desde': fact.data_inici,
                            'data_fins': fact.data_final,
                            'uos_id': p_br.uom_id.id,
                            'quantity': dies_bo,
                            'multi': 1,
                            'product_id': pbosocial_id,
                            'tipus': 'altres',
                            'name': p_br.description}
                    self.crear_linia(cursor, uid, fact.id, vals, ctx)

                fact_obj.button_reset_taxes(cursor, uid, [fact.id])

        facts_to_remove_iese = self.get_invoices_to_remove_iese(
            cursor, uid, factures_creades, context=context
        )

        if facts_to_remove_iese:
            self.remove_iese_tax(
                cursor, uid, facts_to_remove_iese, context=context
            )

        return factures_creades

GiscedataFacturacioFacturador()


class GiscedataFacturacioFactura(osv.osv):
    """Modificació de la factura per poder-la afegir a una remesa
    """
    _name = 'giscedata.facturacio.factura'
    _inherit = 'giscedata.facturacio.factura'

    _columns = {
        'visible_ov': fields.boolean(
            'Visible OV',
            help=_(u'Camp per indicar si la factura '
                   u'sera visible a la Oficina Virtual')
        )
    }

    _defaults = {
        'visible_ov': lambda *a: True
    }

GiscedataFacturacioFactura()
