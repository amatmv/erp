# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from datetime import date, timedelta


class TestUpdatePendingStates(testing.OOTestCase):

    def setUp(self):
        self.pool = self.openerp.pool

    def _load_demo_data(self, cursor, uid):
        imd_obj = self.pool.get('ir.model.data')
        self.invoice_1_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'factura_0001'
        )[1]
        self.invoice_2_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'factura_0002'
        )[1]

        #0
        self.correct_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio_comer_bono_social',
            'correct_bono_social_pending_state'
        )[1]
        #82
        self.waiting_unpaid_id = imd_obj.get_object_reference(
            cursor, uid, 'som_account_invoice_pending',
            'esperant_segona_factura_impagada_pending_state'
        )[1]
        #83
        self.waiting_notif = imd_obj.get_object_reference(
            cursor, uid, 'som_account_invoice_pending',
            'pendent_notificacio_tall_imminent_pending_state'
        )[1]
        #0
        self.def_correct_id = imd_obj.get_object_reference(
            cursor, uid, 'account_invoice_pending',
            'default_invoice_pending_state'
        )[1]
        #185
        self.def_waiting_unpaid_id = imd_obj.get_object_reference(
            cursor, uid, 'som_account_invoice_pending',
            'default_esperant_segona_factura_impagada_pending_state'
        )[1]
        #190
        self.def_waiting_notif = imd_obj.get_object_reference(
            cursor, uid, 'som_account_invoice_pending',
            'default_pendent_notificacio_tall_imminent_pending_state'
        )[1]
 

    def test__update_second_unpaid_invoice__two_invoices_moving(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            self._load_demo_data(cursor, uid)
            self._load_data_unpaid_invoices(cursor, uid, [self.waiting_unpaid_id, self.waiting_unpaid_id])
            pending_obj = self.pool.get('update.pending.states')
            fact_obj = self.pool.get('giscedata.facturacio.factura')
            inv_data = fact_obj.browse(cursor, uid, self.invoice_1_id)
            self.assertEqual(inv_data.pending_state.id, self.waiting_unpaid_id)
            inv_data = fact_obj.browse(cursor, uid, self.invoice_2_id)
            self.assertEqual(inv_data.pending_state.id, self.waiting_unpaid_id)

            pending_obj.update_second_unpaid_invoice(cursor, uid)

            inv_data = fact_obj.browse(cursor, uid, self.invoice_1_id)
            self.assertEqual(inv_data.pending_state.id, self.waiting_notif)
            inv_data = fact_obj.browse(cursor, uid, self.invoice_2_id)
            self.assertEqual(inv_data.pending_state.id, self.waiting_notif)

    def test__update_second_unpaid_invoice__two_invoices_not_moving(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            self._load_demo_data(cursor, uid)
            self._load_data_unpaid_invoices(cursor, uid, [self.def_correct_id, self.def_waiting_unpaid_id])
            pending_obj = self.pool.get('update.pending.states')
            fact_obj = self.pool.get('giscedata.facturacio.factura')
            inv_data = fact_obj.browse(cursor, uid, self.invoice_1_id)
            self.assertEqual(inv_data.pending_state.id, self.def_correct_id)
            inv_data = fact_obj.browse(cursor, uid, self.invoice_2_id)
            self.assertEqual(inv_data.pending_state.id, self.def_waiting_unpaid_id)

            pending_obj.update_second_unpaid_invoice(cursor, uid)
            inv_data = fact_obj.browse(cursor, uid, self.invoice_1_id)
            self.assertEqual(inv_data.pending_state.id, self.def_correct_id)
            inv_data = fact_obj.browse(cursor, uid, self.invoice_2_id)
            self.assertEqual(inv_data.pending_state.id, self.def_waiting_unpaid_id)

    def test__update_second_unpaid_invoice__one_unpaid_invoice_not_moving(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            self._load_demo_data(cursor, uid)
            self._load_data_unpaid_invoices(cursor, uid, [self.def_waiting_unpaid_id])
            pending_obj = self.pool.get('update.pending.states')
            fact_obj = self.pool.get('giscedata.facturacio.factura')
            inv_data = fact_obj.browse(cursor, uid, self.invoice_1_id)
            self.assertEqual(inv_data.pending_state.id, self.def_waiting_unpaid_id)

            pending_obj.update_second_unpaid_invoice(cursor, uid)
            inv_data = fact_obj.browse(cursor, uid, self.invoice_1_id)
            self.assertEqual(inv_data.pending_state.id, self.def_waiting_unpaid_id)

    def _load_data_unpaid_invoices(self, cursor, uid, invoice_semid_list=[]):
        imd_obj = self.pool.get('ir.model.data')
        inv_obj = self.pool.get('account.invoice')
        fact_obj = self.pool.get('giscedata.facturacio.factura')

        contract_name = ''
        for index, res_id in enumerate(invoice_semid_list, start=1):
            fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_000'+str(index)
            )[1]
            invoice_id = fact_obj.read(cursor, uid, fact_id, ['invoice_id'])['invoice_id'][0]

            if index == 1: 
                contract_name = inv_obj.read(cursor, uid, invoice_id, ['name'])['name']  
            
            inv_obj.write(cursor, uid, invoice_id, {
                'name': contract_name,
            })
            inv_obj.set_pending(cursor, uid, [invoice_id], res_id)

