# -*- coding: utf-8 -*-

from datetime import datetime, timedelta, date

from osv import osv
from osv.expression import OOQuery

class UpdatePendingStates(osv.osv_memory):
    _name = 'update.pending.states'
    _inherit = 'update.pending.states'

    def update_invoices(self, cursor, uid, context=None):
        super(UpdatePendingStates,
              self).update_invoices(cursor, uid, context=context)
        self.update_second_unpaid_invoices(cursor, uid)

    def update_second_unpaid_invoice(self, cursor, uid, context=None):

        if context is None:
            context = {}

        ir_model_data = self.pool.get('ir.model.data')

        # BO SOCIAL
        bs_correct_id = ir_model_data.get_object_reference(
            cursor, uid, 'giscedata_facturacio_comer_bono_social', 'correct_bono_social_pending_state'
        )[1]

        bs_waiting_unpaid_id = ir_model_data.get_object_reference(
            cursor, uid, 'som_account_invoice_pending', 'esperant_segona_factura_impagada_pending_state'
        )[1]

        bs_waiting_notif_id = ir_model_data.get_object_reference(
            cursor, uid, 'som_account_invoice_pending', 'pendent_notificacio_tall_imminent_pending_state'
        )[1]

        # DEFAULT PROCESS
        dp_correct_id = ir_model_data.get_object_reference(
            cursor, uid, 'account_invoice_pending', 'default_invoice_pending_state'
        )[1]

        dp_waiting_unpaid_id = ir_model_data.get_object_reference(
            cursor, uid, 'som_account_invoice_pending', 'default_esperant_segona_factura_impagada_pending_state'
        )[1]

        dp_waiting_notif_id = ir_model_data.get_object_reference(
            cursor, uid, 'som_account_invoice_pending', 'default_pendent_notificacio_tall_imminent_pending_state'
        )[1]

        self.update_state_with_2_invoices_unpaid(cursor, uid, 'Bo Social', bs_correct_id, bs_waiting_unpaid_id, bs_waiting_notif_id, context)
        self.update_state_with_2_invoices_unpaid(cursor, uid, 'Default Process', dp_correct_id, dp_waiting_unpaid_id, dp_waiting_notif_id, context)


    def update_state_with_2_invoices_unpaid(self, cursor, uid, process_name, correct_id, waiting_unpaid_id, waiting_notif_id, context=None):
        """
        If exists one invoice with state corresponding to waiting_unpaid_id
        and at least two invoices between state corresponding to correct_id
        and waiting_notif_id, all are updated to the state corresponding
        to waiting_notif_id

        :param process_name: pending state process name
        :param correct_id: lowest weight in process
        :param waiting_unpaid_id: at least one invoice with this pstate
        :param waiting_notif_id: final state after update
        """
        if context is None:
            context = {}
        inv_obj = self.pool.get('account.invoice')
        pend_obj = self.pool.get('account.invoice.pending.state')
        waiting_unpaid_weight = pend_obj.browse(cursor, uid, waiting_unpaid_id).weight
        correct_weight = pend_obj.browse(cursor, uid, correct_id).weight

        invoice_ids = inv_obj.search(cursor, uid, [('pending_state.id', '=', waiting_unpaid_id), ('pending_state.process_id.name', 'like', process_name)])
        for inv_id in invoice_ids:
            contract_name = inv_obj.read(cursor, uid, inv_id, ['name'])['name']
            inv_list = inv_obj.search(cursor, uid, [('name', 'like', contract_name),
                                                    ('pending_state.weight', '<=', waiting_unpaid_weight),
                                                    ('pending_state.weight', '>', correct_weight)])
            if len(inv_list) >= 2:
                for invoice_id in inv_list:
                    inv_obj.set_pending(cursor, uid, [invoice_id], waiting_notif_id)


UpdatePendingStates()
