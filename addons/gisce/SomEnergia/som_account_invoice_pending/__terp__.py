# -*- coding: utf-8 -*-
{
    "name": "Procediments d'impagats específics de Som Energia",
    "description": """Mòdul per a la gestió dels impagaments de contractes 
        especifics de SomEngergia""",
    "version": "0-dev",
    "author": "Som Energia SCCL",
    "category": "Master",
    "depends":[
        "giscedata_facturacio_comer_bono_social",
        "account_invoice_pending",
        "giscedata_facturacio",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "som_account_invoice_pending_data.xml",
    ],
    "active": False,
    "installable": True
}
