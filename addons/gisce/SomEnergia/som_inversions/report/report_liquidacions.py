from c2c_webkit_report import webkit_report
from report import report_sxw
from tools import config


class report_webkit_html(report_sxw.rml_parse):
    def __init__(self, cursor, uid, name, context):
        super(report_webkit_html, self).__init__(cursor, uid, name,
                                                 context=context)
        self.localcontext.update({
            'cursor': cursor,
            'uid': uid,
            'addons_path': config['addons_path'],
        })

webkit_report.WebKitParser(
    'report.liquidacions.interessos',
    'account.invoice',
    'som_inversions/report/report_liquidacions_interessos.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.liquidacions.titols',
    'account.invoice',
    'som_inversions/report/report_liquidacions_titols.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.retencions.interessos',
    'account.invoice',
    'som_inversions/report/report_retencions_interessos.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.generationkwh.doc',
    'account.invoice',
    'som_inversions/report/report_generationkwh_doc.mako',
    parser=report_webkit_html
)
