<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<head>
    <style type="text/css">
    ${css}
    body {
        font-family: helvetica;
        font-size: 14px;
    }
    p {
        padding: 12px;
    }
    td {
        padding: 5px;
    }
    </style>
</head>
<body>
    %for inv in objects :
    <% setLang(inv.partner_id.lang) %>
    <img src="${addons_path}/som_inversions/report/logo.jpg" width="150" height="150"/>
    <p>${_("Benvolgut/da")} ${',' in inv.partner_id.name and inv.partner_id.name.split(', ')[1] or inv.partner_id.name},</p>
    <p>${_("Els interessos generats fins a %s per les teves inversions en títols participatius de SOM ENERGIA SCCL són:") % inv.invoice_line[-1].name.split(' ')[-1]}</p>
    <p style="margin-left: 90px;">
    ${_("Titular")}: ${inv.partner_id.name}
    <br/>
    ${_("NIF")}: ${inv.partner_id.vat[2:]}</p>
    <div style="border: 1px solid #000; border-bottom-width: 0px; border-top-left-radius: 15px; border-top-right-radius: 15px; width: 300px; margin-left: 102px; font-size: 16px;" align="center"><strong>${_("Liquidació interessos").upper()}</strong></div>
    <div align="center">
    <table style="border: 1px solid #000;">
    <th>${_("Inici")}</th>
    <th>${_("Final")}</th>
    <th>${_("Saldo invertit")}</th>
    <th>${_("Tipus interès")}</th>
    <th>${_("Interessos generats")}</th>
    %for line in inv.invoice_line:
        <tr>
          <td align="center">${line.name.split(' ')[1]}</td>
          <td align="center">${line.name.split(' ')[3]}</td>
          <td align="right">${formatLang(line.quantity, monetary=True)}</td>
          <td align="right">${formatLang(float(line.note.split(' ')[6].split('/')[0]) * 100)}%</td>
          <td align="right">${formatLang(line.price_subtotal)}</td>
        </tr>
    %endfor
    <tr>
          <td></td>
          <td></td>
          <td></td>
          <td>${_("Total interessos")}</td>
          <td align="right">${formatLang(inv.amount_untaxed)}</td>
    </tr>
    %for tax in inv.tax_line:
        <tr>
          <td></td>
          <td></td>
          <td colspan="2">${tax.name}</td>
          <td align="right">${formatLang(tax.amount)}</td>
        </tr>
    %endfor
        <tr>
          <td></td>
          <td></td>
          <td colspan="2"><strong>${_("Import a abonar")}</strong></td>
          <td align="right"><strong>${formatLang(inv.amount_total)}</strong></td>
        </tr>
    </table>
    </div>
    <p>${_("Aquest import serà transferit al teu compte corrent número")} <strong><nobr>${(inv.partner_bank.iban[:-9] or '')}****</nobr></strong>.</p>
    <p>${_("Actualment les teves inversions en títols participatius de SOM ENERGIA SCCL sumen")} <strong><nobr>${formatLang(abs(inv.partner_id.property_account_titols.balance), monetary=True)} ${_("euros.")}</nobr></strong></p>
    <p>${_("Gràcies a inversions com la teva la cooperativa finança projectes i pot generar cada vegada més energia provinent de fonts renovables")}</p>
    <img src="${addons_path}/som_inversions/report/peu.jpg" width="650px" style="margin-top: 50px;"/>
    %endfor
</body>
</html>
