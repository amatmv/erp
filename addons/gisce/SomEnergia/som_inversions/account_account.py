# -*- coding: utf-8 -*-

from osv import fields, osv


class AccountAccount(osv.osv):
    """Modifiquem res_partner per poder assignar un bank on cobrar els
    interessos de les inversions"""
    _name = 'account.account'
    _inherit = 'account.account'

    def get_stats_account_investment(self, cursor, uid, accounts, context=None):
        """
        Returns a list of dict with investment statistics:
            res: {'account': account code
                  'socis': Number of 'socis'
                  'amount': Total investment }
            params:
                list/tuple or simple type intepreted as follows:
                - int: account id
                - string: account code
        """
        acc_obj = self.pool.get('account.account')

        result = []

        if not context:
            context = {}

        if not isinstance(accounts, (list, tuple)):
            accounts = [accounts]

        for acc in accounts:
            # get account_id
            if isinstance(acc, basestring):
                account_ids = acc_obj.search(cursor, uid, [('code', '=', acc)])
                if not account_ids:
                    continue
                else:
                    account_id = account_ids[0]
            elif isinstance(acc, int):
                account_id = acc
            else:
                continue

            sql = """SELECT MAX(aup.code),
                     ABS(SUM(amount)),
                     COUNT(DISTINCT pl.partner_id)
                     FROM payment_line pl
                     LEFT JOIN account_account au ON au.id=pl.account_id
                     LEFT JOIN account_account aup ON aup.id=au.parent_id
                     WHERE au.parent_id=%s
                  """
            cursor.execute(sql, (account_id,))
            res = cursor.fetchone()
            result.append({'account': res[0], 'amount': res[1],
                           'socis': res[2]})
        return result


    def get_stats_investment_generation(self, cursor, uid, context=None):
        """
        Returns a list of dict with investment GenerationKwh statistics:
            res: {'account': account code
                  'socis': Number of 'socis' with Generation
                  'amount': Total investment in Generation }
            params: No params
        """

        result = []

        if not context:
            context = {}

        sql = """SELECT sum(nsocis) as "nsocis", sum(total) as "total"
                FROM
                (SELECT count(distinct(member_id)) as nsocis, 0 as total
                FROM public.generationkwh_investment
                UNION all
                SELECT 0 as nsocis, sum(nshares) as total
                FROM public.generationkwh_investment
                ) as stats_investment_generation
                """

        cursor.execute(sql)
        res = cursor.fetchone()
        result.append({'amount': res[1]*100,
                       'socis': res[0]})
        return result

AccountAccount()

# vim: et ts=4 sw=4
