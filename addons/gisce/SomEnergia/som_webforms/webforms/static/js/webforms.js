function check_acc_number(){
    $("#acc_number_message").empty();
    var hentitat = $("#hentitat").val();
    var hsucursal = $("#hsucursal").val();
    var hcontrol = $("#hcontrol").val();
    var hncompte = $("#hncompte").val();
    if (hentitat && hsucursal && hcontrol && hncompte) {
        jQuery.ajax({
            url: "/_check_bank/" + hentitat + hsucursal + hcontrol + hncompte,
            dataType: "json",
            success: function(result){
                if (!result.status) {
                    $("#ccc").addClass("error");
                }
                else {
                    $("#ccc").removeClass("error");
                }
				$("#acc_number_message").empty()
                $("#acc_number_message").append(result.message)
            }
        });
    }
}

function check_empty(element) {
    if (element.val() == "" || element.val() == null) {
        element.addClass('error');
    }
}

function check_vat(element) {
    var nif = element.val();
    if (nif != null && nif != "") {
        $.getJSON('/_check_vat/' + nif, function(data) {
            if (!data.result) {
              element.addClass('error');
            } else {
              element.removeClass('error');
            }
        });
    }
  }
