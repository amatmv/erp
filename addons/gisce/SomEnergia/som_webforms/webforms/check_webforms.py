#!/usr/bin/env python

import xmlrpclib
import sys
import requests
from raven import Client

WEB = 'https://webforms.somenergia.coop/infosocis'
SENTRY_DSN = 'http://20228e277e0348ae9af6763587a54756'
SENTRY_DSN += ':cfa557ec960244dd8a22359098c8a7cd@10.242.2.126:9000/4'
SUPERVISOR_URL = 'http://10.242.2.126:9001/RPC2/'

if __name__ == '__main__':
    status_code = requests.get(WEB).status_code
    if  status_code != 200:
        client = Client(dsn=SENTRY_DSN)
        try:
            server = xmlrpclib.Server(SUPERVISOR_URL)
        except:
            client.captureException()
            sys.exit(1)
        last_log = server.supervisor.tailProcessStderrLog(
            'webforms', 0, 500
        )[0].split('\n')[-2]
        client.captureMessage("Reiniciem Webforms degut a una penjada "
                              "Code: %s" % status_code,
                              extra={'last_log': last_log})
        try:
            server.supervisor.stopProcess('webforms')
            server.supervisor.startProcess('webforms')
        except:
            client.captureException()
        sys.exit(1)
    sys.exit(0)
