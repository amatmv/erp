# -*- coding: utf-8 -*-

"""
    Webforms
    ~~~~~~~~

    Aplicació Flask per presentar formularis via web per operar amb l'ERP.

"""
import base64
import hashlib
import json
import os
import pickle
import shutil
import time
import vatnumber
import xmlrpclib
import traceback
import operator

import sermepa
import arquiapgw

from datetime import datetime
from functools import wraps
from flask import (abort, Flask, flash, jsonify, redirect, render_template,
                   request, Response, session, url_for)
from flask.ext.babel import Babel, Locale, gettext as _
from ooop_wst import OOOP_WST
from werkzeug import secure_filename, ImmutableMultiDict
from werkzeug.contrib.cache import SimpleCache

from distris import DISTRIS

app = Flask(__name__)
app.config.from_pyfile('webforms.config')
babel = Babel(app)
app.secret_key = app.config['SECRET_KEY']

try:
    app.config['OOOP'] = OOOP_WST(app.config['OOOP_USER'],
                                  app.config['OOOP_PWD'],
                                  app.config['OOOP_DBNAME'],
                                  app.config['OOOP_URI'],
                                  app.config['OOOP_PORT'])
    COUNTRY_ES = app.config['OOOP'].ResCountry.search([('code',
                                                        '=',
                                                        'ES')])[0]
    MUNI_DESCON = app.config['OOOP'].ResMunicipi.search([('ine',
                                                          '=',
                                                          '00000')])[0]
    PROV_DESCON = app.config['OOOP'].ResCountryState.search(
        [('code', '=', '00')])[0]
except:
    app.config['OOOP'] = None
    COUNTRY_ES = None
    MUNI_DESCON = None
    PROV_DESCON = None

#Nombre màxim de contractes permesos per titular
MAX_CONTRACTES = 50

REQUIRED_FIELDS = ["id_soci", "dni", "tarifa",
                   "cups", "cnae", "potencia", "cups_municipi",
                   "cups_adreca", "cups_provincia", "entitat",
                   "sucursal", "control", "ncompte", "escull_pagador",
                   "condicions", "condicions_privacitat", "condicions_titular"]

REQUIRED_FIELDS_TITULAR = ["titular_nom", "titular_cognom", "titular_dni",
                           "titular_email", "titular_adreca",
                           "titular_municipi", "titular_cp",
                           "titular_provincia"]

REQUIRED_FIELDS_REPRESENTANT = ["representant_nom", "representant_dni"]
UNREQUIRED_FIELDS_REPRESENTANT = ["titular_cognom"]

REQUIRED_FIELDS_PAGADOR = ["compte_nom", "compte_dni", "compte_adreca",
                           "compte_provincia", "compte_municipi",
                           "compte_email", "compte_tel", "compte_cp"]

AVAILABLE_LANGS = [l.language for l in babel.list_translations()]

TIPUS_PERSONA_FISICA = 'fisica'
TIPUS_PERSONA_JURIDICA = 'juridica'

sentry = None

if app.config.get('SENTRY_DSN', False):
    from raven.contrib.flask import Sentry
    sentry = Sentry(app)
    sentry.client.captureMessage("Starting webforms...")

cache = SimpleCache(default_timeout=259200)


class WebformException(Exception):
    def __init__(self, value, error_fields=[]):
        self.error_fields = error_fields
        super(Exception, WebformException).__init__(self, value)


def check_online(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        O = app.config['OOOP']
        ok_ips = app.config['IPS_OK'] or []
        filter_ips = app.config['FILTER_IPS'] or False
        try:
            if filter_ips:
                client_ip = request.headers.get('X-Real-Ip',
                                                request.remote_addr)
                print "IP Filter: C: '%s' ok: %s" % (client_ip, ok_ips)
                if client_ip not in ok_ips:
                    raise Exception

            O.ResPartner.search([], 0, 1)
        except:
            return render_template('offline.html')
        return f(*args, **kwargs)
    return decorated_function


@babel.localeselector
def get_locale():
    """Retorna el locale més adient segons la request"""
    if 'lang' in request.args:
        locale = request.args.get('lang', 'ca')
    elif session.get('lang', False):
        locale = session.get('lang')
    else:
        locale = request.accept_languages.best_match(['ca', 'es'])
    if not locale:
        # provem si és una request de sermepa
        clang = request.args.get('Ds_ConsumerLanguage', False)
        if clang:
            clang = str(clang).zfill(3)
            if clang in sermepa.LANG_MAP:
                locale = sermepa.LANG_MAP.get(clang).split('_')[0]
    try:
        if locale:
            locale = Locale.parse(locale, '_').language
            if locale not in AVAILABLE_LANGS:
                locale = 'ca'
        else:
            locale = 'ca'
    except:
        locale = 'ca'
    if '_' in locale:
        locale = locale.split('_')[0]
    return locale


def fix_vat(vat):
    vat = vat.upper()
    if vat and vat[0].isdigit():
        vat = vat.zfill(9)
    return vat


@app.route('/soci')
@check_online
def soci():
    """Acció per presentar el formulari de soci inicialment"""
    return render_template('soci.html')


@app.route('/_paisos')
def _get_paisos():
    """Funció AJAX per obtenir el llistat de païssos"""
    paisos = cache.get('paisos')
    if paisos is None:
        O = app.config['OOOP']
        paisos_ids = O.ResCountry.search([])
        paisos = O.ResCountry.read(paisos_ids, ['name'])
        cache.set('paisos', paisos)
    return json.dumps(paisos)


@app.route('/_provincies/<int:countryid>')
def _get_provincies(countryid=0):
    """Funció AJAX per obtenir el llistat de províncies segons país"""
    O = app.config['OOOP']
    if not countryid:
        countryid = cache.get('default_countryid')
        if not countryid:
            countryid = O.ResCountry.search([('code', '=', 'ES')])[0]
            cache.set('default_countryid', countryid)
    ckey = 'prov_%s' % countryid
    provincies = cache.get(ckey)
    if not provincies:
        provincia_id = O.ResCountryState.search([('country_id', '=',
                                                  countryid)])
        if not provincia_id:
            provincia_id = O.ResCountryState.search([('code', '=', '00')])
        provincies = O.ResCountryState.read(provincia_id, ['name'])
        #La primera província és la desconnectada
        prov = provincies[1:]
        prov.sort(key=operator.itemgetter('name'))
        provincies = [provincies[0]] + prov
        cache.set(ckey, provincies)
    return json.dumps(provincies)


@app.route('/_get_municipis/<int:provincia>')
def _get_municipis(provincia=0):
    """Funció AJAX per obtenir el llistat de municipis segons província."""
    O = app.config['OOOP']
    ckey = 'munis_%s' % provincia
    res = cache.get(ckey)
    if not res:
        search_params = []
        if provincia:
            search_params += [('state.id', '=', provincia)]
        municipis_ids = O.ResMunicipi.search(search_params, 0, 0, 'name asc')
        if not municipis_ids:
            municipis_ids = O.ResMunicipi.search([('ine', '=', '00000')])
        res = O.ResMunicipi.read(municipis_ids, ['name'])
        res.sort(key=operator.itemgetter('name'))
        cache.set(ckey, res)
    return json.dumps(res)


@app.route('/_get_municipis_ac/<int:provincia>')
def _get_municipis_ac(provincia=0):
    """Funció AJAX per obtenir el llistat de municipis segons província i amb
    autocomplete."""
    O = app.config['OOOP']
    search_params = []
    if provincia:
        search_params += [('state', '=', provincia)]
    term = request.args.get('term', '')
    search_params += [('name', 'ilike', '%%%s%%' % term)]
    municipis_ids = O.ResMunicipi.search(search_params, 0, 0, 'name asc')
    res = [a['name'] for a in O.ResMunicipi.read(municipis_ids, ['name'])]
    return json.dumps(res.sort())


@app.route('/_dades_soci/<soci>/<nif>')
def _dades_soci(soci, nif):
    """Funció AJAX per carregar les dades de soci.

    {
       "id":"1",
       "uid":"1",
       "nom":"GIJSBERT",
       "cognom":"HUIJINK",
       "dni":"X6963738M",
       "tel":"647465902",
       "tel2":"34647465902",
       "email":"gijsberthuijink@gmail.com",
       "email2":"gijsberthuijink@gmail.com",
       "country":"",
       "adreca":"",
       "ciutat":"ORFES",
       "municipi":"",
       "cp":"",
       "com":"",
       "conegut":"",
       "animat":"",
       "info":"1",
       "date":"2010-10-21 00:00:00",
       "date_registre":"2010-09-26 00:00:00",
       "provincia":"GIRONA",
       "lang":"ca",
       "exist":false
    }
    """
    O = app.config['OOOP']
    nif2 = 'ES%s' % nif.upper()
    soci = 'S%s' % soci.zfill(6)
    soci = O.ResPartner.search([('vat', '=', nif2), ('ref', '=', soci)])
    if not soci:
        return "null"
    soci = soci[0]
    addr = O.ResPartnerAddress.search([('partner_id.id', '=', soci)])
    if not addr:
        vals = O.ResPartner.read(soci, ['name'])
        vals['nom'] = vals['name'].split(',')[-1].strip()
        vals['cognom'] = vals['name'].split(',')[0].strip()
    else:
        vals = O.ResPartnerAddress.read(addr[0], ['name', 'phone', 'mobile',
                                                  'email', 'street', 'city',
                                                  'id_municipi', 'zip',
                                                  'state_id', 'country_id',
                                                  'partner_id'])
        vals['nom'] = vals['partner_id'][1].split(',')[-1].strip()
        vals['cognom'] = vals['partner_id'][1].split(',')[0].strip()
    ncon = O.GiscedataPolissa.search_count(['|', ('titular.id', '=', soci),
                                                 ('soci.id', '=', soci)])
    if ncon >= MAX_CONTRACTES:
        vals['exist'] = True
    tel = vals.get('phone', '') or vals.get('mobile', '') or ''
    tel2 = (tel != vals.get('mobile', '') and vals.get('mobile', '') or  '')
    res = {
        'nom': vals['nom'],
        'cognom': vals['cognom'],
        'dni': fix_vat(nif),
        'tel': tel,
        'tel2': tel2,
        'email': vals.get('email', ''),
        'country': vals.get('country_id', '') and vals['country_id'][0] or '',
        'adreca': vals.get('street', ''),
        'ciutat': vals.get('city', ''),
        'municipi':
        vals.get('id_municipi', False) and vals['id_municipi'][0] or '',
        'provincia': vals.get('state_id', False) and vals['state_id'][0] or '',
        'cp': vals.get('zip', ''),
        'exist': vals.get('exist', False)
    }
    return json.dumps(res)


@app.route('/_check_cups/<cups>')
def _check_cups(cups):
    """Comprovem si el CUPS és correcte.
    """
    info = {}
    O = app.config['OOOP']
    cups = cups.replace(' ', '').upper()
    info['cups'] = '%s%s%s%s' % (cups[0:2], cups[2:18].replace('O', '0'),
                               cups[18:20], cups[20:22].replace('O', '0'))
    try:
        res = O.GiscedataCupsPs.check_cups_code(info['cups'])
    except Exception, e:
        res = False
    info['result'] = res
    return json.dumps(info)


@app.route('/_check_vat/<vat>')
def _check_vat(vat):
    """Comprovem si el NIF és correcte
    """
    vat = fix_vat(vat)
    return json.dumps({'result': vatnumber.check_vat_es(vat)})


@app.route('/_poblacions/<int:municipi_id>')
def _get_poblacions(municipi_id=0):
    """Funció AJAX per obtenir les poblacions."""
    O = app.config['OOOP']
    term = request.args.get('term', '')
    poblacions = []
    search_params = [('name', 'ilike', '%%%s%%' % term)]
    if not municipi_id:
        poblacions_ids = O.ResMunicipi.search([('name', 'ilike', '%%%s%%' %
                                                term)])
        poblacions += [a['name']
                   for a in O.ResMunicipi.read(poblacions_ids, ['name'])]
    else:
        search_params += [('municipi_id', '=', municipi_id)]
        municipi = O.ResMunicipi.read(municipi_id, ['name'])
        poblacions += [municipi['name']]
    poblacions_ids = O.ResPoblacio.search(search_params)
    poblacions += [a['name']
                   for a in O.ResPoblacio.read(poblacions_ids, ['name'])]
    poblacions = list(set(poblacions))
    return json.dumps(poblacions)


@app.route('/_check_bank/<acc_number>')
def _check_bank(acc_number):
    """Funció AJAX per comprovar el número de compte bancari."""
    O = app.config['OOOP']
    spain = O.ResCountry.search([('code', '=', 'ES')])[0]
    lang = '%s_ES' % get_locale()
    vals = O.ResPartnerBank.onchange_banco([], acc_number, spain,
                                           {'lang': lang})
    res = {'message': '', 'status': False}
    if 'warning' in vals:
        res['message'] = vals['warning'].get('message', '')
        res['status'] = False
    elif 'value' in vals:
        if 'bank' in vals['value']:
            bank = O.ResBank.get(vals['value']['bank'])
            res['message'] = "%s: %s" % (bank.name,
                                         vals['value']['acc_number'])
        else:
            res['message'] = ""
        res['status'] = True
    return jsonify(res)


@app.route('/_check_cnae/<cnae>')
def _check_cnae(cnae):
    """Funció AJAX per comprovar el número de CNAE."""
    O = app.config['OOOP']
    cnae_id = O.GiscemiscCnae.search([('name', '=', cnae)])
    if not cnae_id:
        res = {'status': False, 'msg': ''}
    else:
        res = {'status': True,
               'message': O.GiscemiscCnae.get(cnae_id[0]).descripcio}
    return jsonify(res)


def _dump_form(form):
    """Funció de debug per imprimir tot el que arriba a un formulari."""
    res = "**** DADES DEL FORMULARI ****\n"
    for field, value in form.items():
        res += ' * %s: %s\n' % (field, value)
    res += "**** FINAL FORMULARI ****\n"
    return res


@app.route('/infosocis')
def infosocis():
    """Funció per mostrar informació del nombre de socis i últim soci."""
    res = ''
    try:
        nsocis = cache.get('infosocis_nsocis_c')
        if nsocis is None:
            O = app.config['OOOP']
            nsocis = O.ResPartner.search_count([('category_id', 'ilike', 'Soci')])
            cache.set('infosocis_nsocis_c', nsocis, timeout=5 * 60)
            cache.set('infosocis_nsocis', nsocis)
    except:
        nsocis = cache.get('infosocis_nsocis')
    return render_template("infosocis.html", nom='', poble='', dia='',
                           nsocis=nsocis)


@check_online
def desar_factura(factura):
    """Desa el fitxer factura"""
    # Creem directori per emmagatzamar el fitxer
    stime = datetime.now()
    tid = (stime.strftime('%Y') +
           hashlib.sha1(
                     stime.strftime('%Y%m%d%H%M%S%f')).hexdigest()[:8])
    dirname = os.path.sep.join([app.config['TMP_DIR'], tid])
    try:
        os.mkdir(dirname, 0755)
        fitxer = os.path.sep.join([dirname, secure_filename(factura.filename)])
        f_obj = open(fitxer, 'wb')
        f_obj.write(factura.read())
        f_obj.close()
    except IOError:
        raise Exception(_(u"Error del sistema"))
    return tid

@check_online
def llegir_factura(tid, f_name):
    """Retorna les dades del fitxer f_name"""
    dirname = os.path.sep.join([app.config['TMP_DIR'], tid])
    try:
        fitxer = os.path.sep.join([dirname, f_name])
        f_obj = open(fitxer, 'rb')
        datas = base64.b64encode(f_obj.read())
    except IOError:
        raise Exception(_(u"Error del sistema"))
    return datas


@app.route('/contractacio', methods=['GET', 'POST'])
@check_online
def contractacio():
    """Funció pel formulari de contractació."""
    context = {'lang': '%s_ES' % get_locale()}
    session['lang'] = context['lang']
    if request.method == 'GET':
        REQUIRED_FIELDS_CTR = list(REQUIRED_FIELDS)
        REQUIRED_FIELDS_CTR.extend(REQUIRED_FIELDS_TITULAR)
        return render_template('contractacio.html',
                                            rfields=REQUIRED_FIELDS_CTR)
    elif request.method == 'POST':
        form = request.form
        # carregar dades de la sessió
        dades = {}
        dades['factura'] = {}
        factura = request.files['fitxer']
        if factura:
            dades['factura'].update({
                                    'name': secure_filename(factura.filename),
                                    'tid': desar_factura(factura)
                                     })
            factura.close()
        # Comprovació dels paràmetres de contractació
        try:
            error_fields = contractacio_check(dades, False)
            if error_fields:
                return render_template('contractacio.html', efields=error_fields)
        except Exception, e:
            flash(e, 'error')
            return render_template('contractacio.html')
        info_soci = json.loads(_dades_soci(form.get('id_soci', False),
                                           form.get('dni', False)))
        provincies = dict([(i['id'],
                            i['name']) for i in json.loads(_get_provincies())])

        municipis_cups = dict([(i['id'], i['name'])
                for i in json.loads(_get_municipis(form['cups_provincia']))])

        info_soci['provincia_nom'] = provincies[info_soci['provincia']]
        dades.update({'info_soci': info_soci})
        # handle ImmutableMultiDict objects
        dades['form'] = {}
        dades['form'].update(dict(form.items()))
        s_form = dades['form']
        if int(form.get('soci_titular', 0)):
            set_info_titular(s_form)
        s_form['cups_provincia_nom'] = \
                             provincies[eval(s_form['cups_provincia'])]
        s_form['cups_municipi_nom'] = \
                               municipis_cups[eval(s_form['cups_municipi'])]
        # els municipis de la provincia del titular
        municipis_titular = dict([(i['id'], i['name'])
            for i in json.loads(_get_municipis(s_form['titular_provincia']))])
        # les dades del titular
        s_form['titular_provincia_nom'] = \
                          provincies[eval(s_form['titular_provincia'])]
        titular_municipi = "%s" % s_form['titular_municipi']
        s_form['titular_municipi_nom'] = \
                          municipis_titular[eval(titular_municipi)]
        # afegir la informació del pagador si cal
        set_info_pagador(s_form, provincies=provincies)
     
        # json amb totes les dades que es rebran del post de confirmació
        dades['loggedin'] = True
        dades_json = json.dumps(dades)
        res = render_template('contractacio_confirmacio.html',
                              fields=dades, dades_json=dades_json)
        return res


@app.route('/contractacio/desfer', methods=['POST'])
def contractacio_desfer():
    """Funció de desfer el contracte
    """
    # Commitar les dades del nou contracte
    form = request.form
    dades = form.get('form_json', {})
    error_fields = []
    autoritzat = True
    context = {'lang': '%s_ES' % get_locale()}
    if dades:
        try:
            dades = json.loads(dades)
        except:
            autoritzat = False
        if not autoritzat or not dades.get('loggedin', False):
            flash(_(u"Accés no autoritzat"))
            return redirect(url_for('contractacio'))
        if dades['factura']:
            flash(_(u"Si us plau, recarregui la factura"))
            error_fields.append('fitxer')
    try:
        error_fields.extend(contractacio_check(dades, False))

        if not error_fields:
            res = render_template('contractacio.html', form_json=dades['form'])
        else:
            res = render_template('contractacio.html',
                                 efields=error_fields, form_json=dades['form'])
    except Exception, e:
        flash(e, 'error')
        res = render_template('contractacio.html')
        session.clear()
    return res

@app.route('/contractacio/confirmar', methods=['POST'])
def contractacio_confirmar():
    """Funció de confirmació del nou contracte
    """
    # Commitar les dades del nou contracte
    form = request.form
    dades = form.get('form_json', False)
    autoritzat = True
    if dades:
        try:
            dades = json.loads(dades)
        except:
            autoritzat = False
    else:
        autoritzat = False
    if not autoritzat or not dades.get('loggedin', False):
        flash(_(u"Accés no autoritzat"))
        return redirect(url_for('contractacio'))
    try:
        error_fields = contractacio_check(dades, True)
        if not error_fields:
            res = render_template('contractacio_ok.%s.html' % get_locale())
        else:
            res = render_template('contractacio.html', efields=error_fields)
    except Exception, e:
        flash(e, 'error')
        res = render_template('contractacio.html')
    finally:
        session.clear()
    return res

@check_online
def set_info_titular(form):
    """Configura la informació del titular com la del soci ja que
       no ens ha arrivat pel post (camps "disabled")
    """
    info_soci = json.loads(_dades_soci(form.get('id_soci', False),
                                            form.get('dni', False)))
    if info_soci:
        form.update({
            'titular_nom': info_soci['nom'],
            'titular_cognom': info_soci['cognom'],
            'titular_tel': info_soci['tel'],
            'titular_tel2': info_soci['tel2'],
            'titular_dni': info_soci['dni'],
            'titular_email': info_soci['email'],
            'titular_adreca': info_soci['adreca'],
            'titular_municipi': info_soci['municipi'],
            'titular_cp': info_soci['cp'],
            'titular_provincia': str(info_soci['provincia']),
        })

@check_online
def set_info_pagador(form, ref=None, provincies=None):
    """Configura la informació del pagador igual que la del soci o el 
       titular segons especificat
    """
    if not ref:
       ref = form.get('escull_pagador', 'altre')
    
    if ref == 'soci':
        info_soci = json.loads(_dades_soci(form.get('id_soci', False),
                                                form.get('dni', False)))
        if info_soci:
            form.update({
                'compte_nom': info_soci['nom'],
                'compte_cognom': info_soci['cognom'],
                'compte_tel': info_soci['tel'],
                'compte_tel2': info_soci['tel2'],
                'compte_dni': info_soci['dni'],
                'compte_email': info_soci['email'],
                'compte_adreca': info_soci['adreca'],
                'compte_municipi': info_soci['municipi'],
                'compte_cp': info_soci['cp'],
                'compte_provincia': str(info_soci['provincia']),
            })
    elif ref == 'titular':
        form.update({
            'compte_nom': form.get('titular_nom', False),
            'compte_cognom': form.get('titular_cognom', False),
            'compte_tel': form.get('titular_tel', False),
            'compte_tel2': form.get('titular_tel2', False),
            'compte_dni': form.get('titular_dni', False),
            'compte_email': form.get('titular_email', False),
            'compte_adreca': form.get('titular_adreca', False),
            'compte_municipi': form.get('titular_municipi', False),
            'compte_cp': form.get('titular_cp', False),
            'compte_provincia': form.get('titular_provincia', False),
        })
    if provincies:
        form['compte_provincia_nom'] =  \
                        provincies[eval(form['compte_provincia'])]
        municipis_pagador = dict([(i['id'], i['name'])
                for i in json.loads(_get_municipis(form['compte_provincia']))])
        form['compte_municipi_nom'] = \
                        municipis_pagador[eval('%s' % form['compte_municipi'])]

@check_online
def contractacio_check(dades, commit=False):
    """Comprovació i actualització de dades del nou contracte
    """
    try:
        raise_exc = False
        t = False
        error_fields = []
        context = {'lang': '%s_ES' % get_locale()}
        form = {}
        if 'form' in dades:
            form.update(dades['form'])
        else:
            form.update(dict(request.form.items()))

        REQUIRED_FIELDS_CTR = list(REQUIRED_FIELDS)
        if not int(form.get('soci_titular', 0)):
            REQUIRED_FIELDS_CTR.extend(REQUIRED_FIELDS_TITULAR)
        else:
            set_info_titular(form)
        if int(form.get('tipus_persona', 0)):
            REQUIRED_FIELDS_CTR.extend(REQUIRED_FIELDS_REPRESENTANT)
            for urf in UNREQUIRED_FIELDS_REPRESENTANT:
                urf in REQUIRED_FIELDS_CTR and REQUIRED_FIELDS_CTR.remove(urf)
        pagador = form.get('escull_pagador', 'altre')
        if pagador == 'altre':
            REQUIRED_FIELDS_CTR.extend(REQUIRED_FIELDS_PAGADOR)
        elif pagador == 'soci':
            set_info_pagador(form, 'soci')
        else:
            set_info_pagador(form, 'titular')

        O = app.config['OOOP']
        # Titular
        t = O.begin()
        # Comprovem tots els camps obligatoris
        for rfield in REQUIRED_FIELDS_CTR:
            if rfield not in form or not form[rfield]:
                error_fields.append(rfield)
        if error_fields:
            raise Exception(_(u"Emplenar camps obligatoris, "
                                    u"marcats amb vermell."))
        try:
            for intfield in ['potencia', 'cups_municipi', 'cups_provincia',
                             'titular_provincia', 'titular_municipi']:
                int(form[intfield])
        except Exception:
            error_fields = [intfield]
            raise Exception(_(u"El valor %(valor)s no és vàlid pel "
                                    u"camp %(camp)s", valor=intfield,
                                    camp=form[intfield]))
        # Comprovem que la tarifa és correcte
        tarifes = {}
        tids = t.GiscedataPolissaTarifa.search([])
        for tarifa in t.GiscedataPolissaTarifa.read(tids, ['name']):
            tarifes[tarifa['name']] = tarifa['id']
        if form['tarifa'] not in tarifes:
            error_fields = ['tarifa']
            raise Exception(_(u"La tarifa no és correcte"))
        cnae_id = O.GiscemiscCnae.search([('name', '=', form['cnae'])])
        if not cnae_id:
            error_fields = ['cnae']
            raise Exception(_(u"El CNAE no és vàlid."))
        # mirem la ip des d'on es fa la petició
        # L'NGINX com a proxy ens envia la IP amb el header
        #    X-Real-IP: 1.2.3.4
        client_ip = request.headers.get('X-Real-Ip', request.remote_addr)
        ip_obs = "From IP %s :\n" % client_ip
        p_vals = {
            'tarifa': tarifes[form['tarifa']],
            'observacions': ip_obs + _dump_form(form),
            'potencia': float(form['potencia']) / 1000.0,  # kW
            'cnae': cnae_id[0],
            'data_firma_contracte': datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        }
        # Busquem el titular
        form_id_soci = 'S%s' % form['id_soci'].zfill(6)
        form_dni = 'ES%s' % form['dni'].upper()
        soci = t.ResPartner.search([('ref', '=', form_id_soci),
                                    ('vat', '=', form_dni)])
        if not soci:
            raise Exception(_(u"Soci no trobat!"))
        ncon = t.GiscedataPolissa.search_count(['|',
                                                ('titular.id', '=', soci),
                                                ('soci.id', '=', soci)])
        if ncon >= MAX_CONTRACTES:
            raise Exception(_(u"Aquest soci ha arribat al màxim nombre de "
                              u"contractes de llum permesos. Posis en "
                              u"contacte amb "
                              u"comercialitzacio@somenergia.coop per "
                              u"fer-ne un de nou."))
        # Representant
        if int(form.get('tipus_persona', 0)):
            if not vatnumber.check_vat('ES%s' % form['representant_dni']):
                error_fields += ['representant_dni']
                raise Exception(_(u'El NIF entrat no és vàlid.'))
            
        # Titular
        if form['soci_titular'] == '0':
            # Està marcat que el soci no és el titular, busquem si és
            # veritat o si les dades ja hi són
            form_titular_dni = 'ES%s' % fix_vat(form['titular_dni'])
            if not vatnumber.check_vat(form_titular_dni):
                error_fields += ['titular_dni']
                raise Exception(_(u'El NIF entrat no és vàlid.'))
            sp = [('vat', '=', form_titular_dni)]
            titular_ids = t.ResPartner.search(sp)
            if titular_ids:
                titular_ids = titular_ids[0]
            else:
                if not int(form.get('tipus_persona', 0)):
                    name_titular =  ('%s, %s' % (form['titular_cognom'],
                                                 form['titular_nom'])).title()
                else:
                    name_titular = form['titular_nom'].title()
                vals = {
                    'name': name_titular,
                    'vat': form_titular_dni,
                    'ref': t.IrSequence.get_next('res.partner.titular'),
                    'lang': t.ResPartner.get(soci[0]).lang
                }
                titular_ids = t.ResPartner.create(vals, context)
        else:
            titular_ids = soci[0]
        p_vals.update({
            'titular': titular_ids,
            'soci': soci[0],
        })
        # Busquem si existeix el pagador
        if pagador == 'altre':
            form_pagador_dni = 'ES%s' % fix_vat(form['compte_dni'])
            if not vatnumber.check_vat(form_pagador_dni):
                error_fields += ['compte_dni']
                raise Exception(_(u'El NIF entrat no és vàlid.'))
            sp = [('vat', '=', form_pagador_dni)]
            pagador_ids = t.ResPartner.search(sp)
            if pagador_ids:
                pagador_ids = pagador_ids[0]
            else:
                if form['compte_cognom']:
                    name_pagador =  ('%s, %s' % (form['compte_cognom'],
                                                 form['compte_nom'])).title()
                else:
                    name_pagador = form['compte_nom'].title()
                vals = {
                    'name': name_pagador,
                    'vat': form_pagador_dni,
                    'ref': t.IrSequence.get_next('res.partner.titular'),
                    'lang': t.ResPartner.get(soci[0]).lang
                }
                pagador_ids = t.ResPartner.create(vals, context)
        elif pagador == 'soci':
            pagador_ids = soci[0]
        else:
            pagador_ids = titular_ids

        p_vals.update({
            'pagador': pagador_ids,
        })
        # Municipi & Poblacio titular
        mun = t.ResMunicipi.read([form['titular_municipi']], ['name'])
        if not mun:
            error_fields += ['titular_municipi']
            raise Exception(_(u"Municipi del titular introduït no és vàlid"))

        pob = t.ResPoblacio.search([('municipi_id', '=', mun[0]['id']),
                                    ('name', '=', mun[0]['name'])])
        if not pob:
            vals = {'name': mun[0]['name'],
                    'municipi_id': mun[0]['id']}
            pob = t.ResPoblacio.create(vals, context)
        else:
            pob = pob[0]

        # Comprovem si ja existeix l'adreça del titular
        sp = [('street', '=', form['titular_adreca']),
              ('partner_id.id', '=', p_vals['titular'])]
        addr_ids = t.ResPartnerAddress.search(sp)
        if addr_ids:
            addr_ids_titular = addr_ids[0]
        else:
            if not int(form.get('tipus_persona', 0)):
                name_titular = ('%s, %s' % (form['titular_cognom'],
                                            form['titular_nom'])).title()
            else:
                name_titular = form['titular_nom'].title()

            vals = {
                'partner_id': titular_ids,
                'name': name_titular,
                'phone': form['titular_tel'],
                'mobile': form['titular_tel2'],
                'email': form['titular_email'],
                'nv': form['titular_adreca'],
                'id_poblacio': pob,
                'id_municipi': mun[0]['id'],
                'zip': form['titular_cp'],
                'country_id': COUNTRY_ES,
                'state_id': int(form['titular_provincia'])
            }
            addr_ids_titular = t.ResPartnerAddress.create(vals, context)
        if pagador == 'altre':
            # Municipi & Poblacio pagador
            mun = t.ResMunicipi.read([form['compte_municipi']], ['name'])
            if not mun:
                error_fields += ['compte_municipi']
                raise Exception(_(u"El municipi de l'adreça de pagament " 
                                  u"indroduït no és vàlid"))

            pob = t.ResPoblacio.search([('municipi_id', '=', mun[0]['id']),
                                        ('name', '=', mun[0]['name'])])
            if not pob:
                vals = {'name': mun[0]['name'],
                        'municipi_id': mun[0]['id']}
                pob = t.ResPoblacio.create(vals, context)
            else:
                pob = pob[0]
            # Comprovem si ja existeix l'adreça del pagador
            sp = [('street', '=', form['compte_adreca']),
                  ('partner_id.id', '=', p_vals['pagador'])]
            addr_ids = t.ResPartnerAddress.search(sp)
            if addr_ids:
                addr_ids_pagador = addr_ids[0]
            else:
                if form['compte_cognom']:
                    name_pagador =  ('%s, %s' % (form['compte_cognom'],
                                                 form['compte_nom'])).title()
                else:
                    name_pagador = form['compte_nom'].title()
                    
                vals = {
                    'partner_id': pagador_ids,
                    'name': name_pagador,
                    'phone': form['compte_tel'],
                    'mobile': form['compte_tel2'],
                    'email': form['compte_email'],
                    'nv': form['compte_adreca'],
                    'id_poblacio': pob,
                    'id_municipi': mun[0]['id'],
                    'zip': form['compte_cp'],
                    'country_id': COUNTRY_ES,
                    'state_id': int(form['compte_provincia'])
                }
                addr_ids_pagador = t.ResPartnerAddress.create(vals, context)
        elif pagador == 'titular':
            addr_ids_pagador = addr_ids_titular
        else:
            # el pagador és el soci
            addr_ids_pagador = t.ResPartnerAddress.\
                                 search([('partner_id', '=', soci)])[0]
        if (form.get('soci_titular', 0) and pagador in 'soci'):
            pagador_sel = 'titular'
        else:
            pagador_sel = 'altre_p' if pagador in ('altre', 'soci') \
                                    else 'titular'
        
        p_vals.update({
            'pagador_sel': pagador_sel,
            'direccio_pagament': addr_ids_pagador,
            'notificacio': 'titular',
            'direccio_notificacio': addr_ids_titular,
        })
        # CUPS
        res = json.loads(_check_cups(form['cups']))
        if not res['result']:
            error_fields += ['cups']
            raise Exception(_(u"El CUPS introduït no és vàlid"))
        if commit:
            form['cups'] = res['cups']
        codi_distri = form['cups'][2:6]
        distri = t.ResPartner.search([('ref', '=', codi_distri)])
        if not distri:
            distri = t.ResPartner.create(
                {'name': DISTRIS.get(codi_distri, 'DESCONEGUDA'),
                 'ref': codi_distri,
                 'supplier': 1},
                context
            )
        else:
            distri = distri[0]
        cups_id = t.GiscedataCupsPs.search([('name', '=', form['cups'])],
                                           0, 0, False,
                                           {'active_test': False})
        vals = {
            'name': form.get('cups', False),
            'nv': form['cups_adreca'],
            'id_municipi': int(form['cups_municipi']),
            'distribuidora_id': distri,
            'ref_catastral': form['referencia'],
        }
        if cups_id:
            if t.GiscedataPolissa.search_count([('cups', 'in', cups_id)]):
                error_fields += ['cups']
                raise Exception(_(u"Aquest CUPS ja està contractat"
                                  u" amb Som Energia"))
            t.GiscedataCupsPs.write(cups_id, vals)
            cups_id = cups_id[0]
        else:
            cups_id = t.GiscedataCupsPs.create(vals, context)
        p_vals.update({
            'cups': cups_id,
            'distribuidora': distri,
        })
        # Dades de pagament
        # Bank i tipus de pagament
        acc_number = '%s %s %s %s' % (form['entitat'], form['sucursal'],
                                   form['control'], form['ncompte'])
        bank_ids = t.ResPartnerBank.search([
                        ('acc_number', '=', acc_number),
                        ('partner_id', '=', pagador_ids)])
        if bank_ids:
            bank_ids = bank_ids[0]
        else:
            vals = t.ResPartnerBank.onchange_banco([], acc_number,
                    COUNTRY_ES, context)
            if vals.get('warning', False):
                error_fields += ['entitat', 'sucursal', 'control', 'ncompte']
                raise Exception(vals['warning']['message'])
            else:
                vals = vals['value']
                vals.update({
                    'name': '',
                    'state': 'bank',
                    'country_id': COUNTRY_ES,
                    'state_id': int(form['compte_provincia']),
                    'partner_id': pagador_ids,
                    'acc_country_id': COUNTRY_ES
                })
                bank_ids = t.ResPartnerBank.create(vals)
        p_vals.update({
            'payment_mode_id': t.PaymentMode.search([
                                    ('name', '=', 'ENGINYERS')])[0],
            'tipo_pago': t.PaymentType.search([
                                ('code', '=', 'RECIBO_CSB')])[0],
            'bank': bank_ids
        })
        # donatiu
        p_vals.update({'donatiu': form.get('donatiu',  False) and True})
        polissa_id = t.GiscedataPolissa.create(p_vals, context)
        t.GiscedataPolissa.generar_periodes_potencia([polissa_id])
        if dades['factura']:
            fname = dades['factura']['name']
            tid = dades['factura']['tid']
            vals = {
                'name': fname,
                'datas': llegir_factura(tid, fname),
                'datas_fname': fname,
                'description': 'Factura des del formulari de contractació',
                'res_model': 'giscedata.polissa',
                'res_id': polissa_id,
            }
            t.IrAttachment.create(vals, context)
        if commit:
            t.commit()
        else:
            t.rollback()
    except (xmlrpclib.Fault, Exception), exc:
        t and t.rollback()
        if error_fields:
            flash(exc, 'error')
        else:
            raise_exc = True
    finally:
        if t:
            t.close()
            del t
        if raise_exc:
            traceback.print_exc()
            msg = _(u"En aquests moments no s'ha pogut completar el procés.\n"
                    u"Si us plau, torni-ho a provar més endavant")
            raise Exception(msg)
    return error_fields

@app.route('/soci/alta', methods=['POST'])
@check_online
def alta_soci():
    """Funció per guardar un nou soci a l'ERP"""
    O = app.config['OOOP']
    t = O.begin()
    stime = datetime.now()
    tid = (stime.strftime('%Y') +
           hashlib.sha1(stime.strftime('%Y%m%d%H%M%S%f')).hexdigest()[:8])
    session['tid'] = tid
    action = 0
    # guardem l'idioma per següents pantalles
    if request.form.get('idioma', False):
        session['lang'] = request.form.get('idioma')

    try:
        required_fields = ['nom', 'cognom', 'dni', 'tel', 'email',
                           'cp', 'provincia', 'adreca', 'municipi']
        empty_fields = []
        if (request.form.get('municipi', False)
                and request.form['municipi'] == str(MUNI_DESCON)):
            empty_fields.append('municipi')
        if (request.form.get('provincia', False)
                and request.form['provincia'] == str(PROV_DESCON)):
            empty_fields.append('provincia')
        if(request.form.get('tipuspersona', TIPUS_PERSONA_FISICA) == TIPUS_PERSONA_JURIDICA):
            required_fields.remove('cognom')
        for field in required_fields:
            if not request.form.get(field, False):
                if field not in empty_fields:
                    empty_fields.append(field)
        if empty_fields:
            raise Exception(_(u'Cal emplenar els camps marcats en vermell.'))
        comentari = u"""Ens ha conegut a través de: %s
        El soci/a que l'ha animat és: %s
        Vol rebre informació: %s
        Comentari:
        %s""" % (request.form['conegut'], request.form['animat'],
                 request.form['info'], request.form['com'])

        # crear directori per guardar accions
        dirname = os.path.sep.join([app.config['TMP_DIR'], tid])
        try:
            os.mkdir(dirname, 0755)
        except IOError:
            raise Exception(_(u"Error del sistema"))

        # recibo_csb = t.PaymentType.search([('code', '=', 'RECIBO_CSB')])[0]
        c_soci = t.ResPartnerCategory.search([('name', '=', 'Soci')])
        if 'cognom' in request.form:
            s_name = ('%s, %s' % (request.form['cognom'],
                                  request.form['nom'])).title()
        else:
            s_name = request.form['nom'].title()

        vat = fix_vat(request.form['dni'])
        vat = 'ES%s' % vat
        found = t.ResPartner.search([('vat', '=', vat)])
        if found:
            raise Exception(_(u'No es pot crear aquest soci.'))

        if not vatnumber.check_vat_es(fix_vat(request.form['dni'])):
            raise Exception(_(u'El NIF entrat no és vàlid.'))
        vals_partner = {
            'name': s_name,
            'vat': vat,
            'active': True,
            'comment': comentari,
            'category_id': [(6, 0, c_soci)],
            'lang': request.form['idioma'],
            'comercial': tid,
            'date': datetime.now().strftime('%Y-%m-%d'),
        }

        try:
            filep = open(os.path.sep.join([dirname, str(action)]), 'w')
            partner_file = [
                tid,
                'ResPartner',
                'create',
                datetime.now().strftime('%Y%m%d%H%M%S%f'),
                vals_partner,
                {'ResPartnerAddress': 'partner_id'},
            ]
            pickle.dump(partner_file, filep, 1)
            action += 1
        except Exception:
            raise Exception(_("Error del sistema"))
        finally:
            filep.close()

        id_municipi = int(request.form['municipi'])

        vals_address = {
            'name': s_name,
            'country_id': COUNTRY_ES,
            'state_id': request.form['provincia'],
            'email': request.form['email'],
            'phone': request.form['tel'],
            'mobile': request.form.get('tel2',''),
            'zip': request.form['cp'],
            'nv': request.form['adreca'],
            'type': 'default',
            'id_municipi': id_municipi,
        }
        form_ciutat = t.ResMunicipi.read([id_municipi],
                                         ['name'])[0]['name']
        id_poblacio = t.ResPoblacio.search([
            ('name', '=', form_ciutat),
            ('municipi_id.id', '=', id_municipi)
        ])
        if not id_poblacio:
            poblacio_file = [
                tid,
                'ResPoblacio',
                'create',
                datetime.now().strftime('%Y%m%d%H%M%S%f'),
                {'name': form_ciutat,
                 'municipi_id': id_municipi},
                {'ResPartnerAddress': 'id_poblacio'},
            ]
            filename = os.path.sep.join([dirname, str(action)])
            try:
                filep = open(filename, 'w')
                pickle.dump(poblacio_file, filep, 1)
                action += 1
            except:
                # amaguem a l'usuari com treballem internament
                raise Exception(_(u"Error de sistema"))
            finally:
                filep.close()
        else:
            id_poblacio = id_poblacio[0]
        vals_address.update({'id_poblacio': id_poblacio})

        filename = os.path.sep.join([dirname, str(action)])
        try:
            filep = open(os.path.sep.join([dirname, str(action)]), 'w')
            address_file = [
                tid,
                'ResPartnerAddress',
                'create',
                datetime.now().strftime('%Y%m%d%H%M%S%f'),
                vals_address,
                {},
            ]
            pickle.dump(address_file, filep, 1)
        except:
            raise Exception(_(u"Error de sistema"))
        finally:
            filep.close()
        t.commit()
    except Exception, e:
        traceback.print_exc()
        t.rollback()
        flash(e.message)
        return render_template('soci.html', error_fields=empty_fields)
    finally:
        t.close()

    # redireccionem al pagament ...
    # posem abans quin mètode de pagament volen fer servir
    session['payment_method'] = request.form['payment_method']
    session['dni'] = request.form['dni'].upper()
    session['s_name'] = s_name
    return redirect(url_for('prepagament'))


@app.route('/_get_idiomes')
def _get_idiomes():
    """Funció per retornar els idiomes"""
    idiomes = cache.get('idiomes')
    if not idiomes:
        O = app.config['OOOP']
        lang_ids = O.ResLang.search([])
        langs = O.ResLang.read(lang_ids, ['code', 'name'])
        idiomes = []
        for lang in langs:
            if lang['code'] not in ('es_ES', 'ca_ES'):
                continue
            if '/' in lang['name']:
                lang['name'] = lang['name'].split('/')[1].strip()
            idiomes += [{'code': lang['code'],
                         'name': lang['name']}]
        cache.set('idiomes', idiomes, timeout=5 * 60)
    return json.dumps(idiomes)


# URLs pel pagament amb tarja
@app.route('/prepagament')
@check_online
def prepagament():
    """Destí del formulari de soci, que obrirà la plana de pagament

    Renderitza una pàgina sense gairebé contingut que obre el popup del tpv.
    """
    payment = session.get('payment_method', False)
    if not payment:
        abort(500)

    quantitat_a_cobrar = 100
    if payment == 'tpv':
        lang_map_i = {}
        for key, value in sermepa.LANG_MAP.items():
            newkey = value.split('_')[0]
            if not newkey in lang_map_i:
                lang_map_i.update({newkey: key})
        if app.config['SERMEPA_TEST']:
            tpv_client = sermepa.TestClient
        else:
            tpv_client = sermepa.Client
        client = tpv_client(app.config['SERMEPA_USER'],
                              app.config['SERMEPA_SECRET'])
        data = client.get_pay_form_data({
            'Ds_Merchant_Amount': quantitat_a_cobrar,
            'Ds_Merchant_Order': session.get('tid'),
            'Ds_Merchant_ProductDescription': app.config['SERMEPA_PRODUCT'],
            'Ds_Merchant_Titular': app.config['SERMEPA_OWNER'],
            'Ds_Merchant_MerchantURL': app.config['SERMEPA_URL'],
            'Ds_Merchant_UrlOK': app.config['SERMEPA_URLOK'],
            'Ds_Merchant_UrlKO': app.config['SERMEPA_URLKO'],
            'Ds_Merchant_MerchantName': app.config['SERMEPA_NAME'],
            'Ds_Merchant_ConsumerLanguage': lang_map_i.get(get_locale(), '001'),
            'Ds_Merchant_Terminal': app.config['SERMEPA_TERMINAL'],
            'Ds_Merchant_SumTotal': quantitat_a_cobrar,
            'Ds_Merchant_TransactionType': 0,
            'Ds_Merchant_MerchantData': _('COBRAMENT QUOTA SOCI'),
        })
    elif payment == 'rebut':
        """
        ID_OPERACION: unique operation id
          CONF:         payment modes configuration (1100, 0100 or 1000)
          REF:          payment reference
          DNI_CLI:      customer's VAT number
          NOMBRE_CLI:   customer's name
          IMPORTE:      payment amount
          CONC1:        payment's description line 1
          CONC2:        payment's description line 2
          CONC3:        payment's description line 3
          CONC4:        payment's description line 4
          CCVERIF:      (not required)
        """
        O = app.config['OOOP']
        client = arquiapgw.ArquiaPGwClient(
            app.config['ARQUIA_USER'],
            app.config['ARQUIA_SECRET'],
            {'ID_OPERACION': O.IrSequence.get_next('arquia.id_operacion'),
             'REF': session.get('tid'),
             'DNI_CLI': session['dni'],
             'NOMBRE_CLI': session['s_name'],
             'IMPORTE': quantitat_a_cobrar,
             'CONC1': _('QUOTA SOCI'),
             'CONF': '0100'},
        )
        data = client.get_payment_form_data()
        data['ID_OPERACION'] = data['ID_OPERACION']
        del session['dni']
        del session['s_name']
    return render_template('prepagament.html', payment_data=data,
                           payment_type=payment, endpoint=client.endpoint)


@app.route('/pagament/notificacio', methods=['POST'])
def pagament_notificacio():
    """Funció que es crida quan el pagament ha estat satisfactori.
    Es generen els registres corresponents a l'ERP.

    Aquesta funció la cridarà el servidor de Sermepa, no ha de retornar res cap
    a l'usuari.

    Aquests són els paràmetres que es reben de Sermepa:

    Ds_Date: dd/mm/yyyy
    Ds_Hour: hh:mm
    Ds_Amount: int(12)
    Ds_Currency: int(4)
    Ds_Order: int(4) + varchar(8) (tid)
    Ds_MerchantCode: int(9)
    Ds_Terminal: int(3)
    Ds_Signature: char(40)
    Ds_Response: int(4)
    Ds_MerchantData: varchar(1024)
    Ds_SecurePayment: 1/0
    Ds_TransactionType: char(1)
    Ds_Card_Country: int(3)
    Ds_AuthorisationCode: varchar(6)
    Ds_ConsumerLanguage: int(3)
    Ds_Card_Type: C/D

    Hem d'obrir el fitxer corresponent a la petició que ens arriba i executar
    les accions.

    Les accions del fitxer tenen el format següent:

    [
        Ds_Order,
        model,
        method,
        timestamp,
        vals,
        fk,
    ]

    passades pel pickle. 'fk' és un diccionari amb un model com a clau i una
    columna com a valor i indica a on ha d'anar escrit l'id retornat per
    l'acció.

    I estan guardades en fitxers anomenats 0, 1, ... segons l'ordre en què
    s'han d'executar, al directori "app.config['TMP_DIR']/Ds_Order/".
    """
    # Sermepa
    tx_ok = True
    Ds_Order = request.form.get('Ds_Order', False)
    if Ds_Order and (request.form.get('Ds_ErrorCode', False)
                     or request.form.get('Ds_Response', '9999') != '0000'):
        tx_ok = False
    if not Ds_Order:
        # Arquia
        Ds_Order = request.form.get('ref', False)
        if Ds_Order and request.form.get('estado', 'OK') != 'OK':
            tx_ok = False
    if not Ds_Order:
        return abort(400)
    # llegir si existeix directori "Ds_Order"
    dirname = os.path.sep.join([app.config['TMP_DIR'], Ds_Order])
    fks = {}  # {'model': 'columna'}
    ids = {}  # {'model': [{'columna': 'id'}, ]}
    try:
        if not os.path.exists(dirname):
            raise Exception("no dirname")
        try:
            # Log the post variables
            vals = []
            for k, v in request.form.items():
                vals.append('%s => %s' % (k, v))
            post_file = os.path.sep.join([dirname, 'post'])
            with open(post_file, 'w') as pf:
                pf.write('\n'.join(vals))
        except Exception:
            pass
        if not tx_ok:
            return Response('OK', status=200)
        t = app.config['OOOP'].begin()
        actions = [0]
        for action in actions:
            current = os.path.sep.join([dirname, str(action)])
            if os.path.exists(current):
                fcurrent = open(current, 'r')
                data = pickle.load(fcurrent)
                fcurrent.close()
                metadata = os.stat(current)
                if (time.time() - metadata.st_mtime) < app.config['TRANS_TTL']:
                    if data[5]:
                        fks.update(data[5])
                    if data[1] in ids:
                        # alguna accio anterior vol posar un id aquí
                        for keyval in ids[data[1]]:
                            data[4].update(keyval)
                    proxy = getattr(t, data[1])
                    method = getattr(proxy, data[2])
                    try:
                        if data[1] == 'ResPartner':
                            data[4].update({
                              'ref': t.IrSequence.get_next('res.partner.soci')
                            })
                        id_ = method(data[4], {'nousoci': True})
                        for key in data[5]:
                            if key not in ids:
                                ids.update({key: []})
                            ids[key].append({fks[key]: id_})
                        # si va bé, renombrar el fitxer
                        os.rename(current, '%s.done' % current)
                    except Exception, e:
                        traceback.print_exc()
                        raise e
                else:
                    raise Exception("trans_ttl")
            following = os.path.sep.join([dirname, str(action + 1)])
            if os.path.exists(following):
                actions.append(action + 1)
        t.commit()
    except Exception:
        t.rollback()
        traceback.print_exc()
        return abort(500)
    finally:
        t.close()
        del t
    return Response('OK', status=200)


@app.route('/pagament/clean', methods=['GET'])
def pagament_clean():
    """Funció que neteja les transaccions que ja han superat el TTL.

    Quan es demana aquesta URL des de localhost, es netegen les transaccions
    guardades que ja han superat el TTL configurat.

    Ho fem dins l'aplicació per tenir-ho tot sota un mateix programa. Després
    per executar-la és tan simple com fer un wget de l'URL des d'un crontab.
    """
    # només deixem accedir des de localhost
    if request.remote_addr != '127.0.0.1':
        return abort(403)
    for item in os.listdir(app.config['TMP_DIR']):
        path = os.path.join([app.config['TMP_DIR'], item])
        if os.path.isdir(path):
            shutil.rmtree(path)
    return Response('OK', status=200)


@app.route('/pagament/ok')
def pagament_ok():
    """Funció que retorna la pàgina de pagament OK al client.

    Aquesta és una pàgina que s'obre al popup i el que fa és tancar el popup i
    obrir la pàgina amb el missatge corresponent dins l'iframe."""
    return render_template('pagament_ok.html')


@app.route('/pagament/ko')
def pagament_ko():
    """Funció que retorna la pàgina de pagament KO al client.

    Igual que amb l'OK, aquesta pàgina tancarà el popup i obrirà la pàgina amb
    l'error a l'iframe."""
    return render_template('pagament_ko.html')


@app.route('/pagament/realitzat')
def pagament_realitzat():
    """Funció que retorna la pàgina de dins l'iframe per pagament OK."""
    return render_template('pagament_realitzat.%s.html' % get_locale())


@app.route('/pagament/cancelat')
def pagament_cancelat():
    """Funció que retorna la pàgina de dins l'iframe per pagament KO."""
    return render_template('pagament_cancelat.%s.html' % get_locale())


@app.route('/rebuts/resposta')
def rebuts_resposta():
    """URL que reenvia a l'usuari ARQUIA.

    ?ID_OPERACION=bzyjVprhtOK31aT1qU+AhVlsA1Z3VLt4&REF=2012b9f3d837
    &IMPORTE=10.00&ESTADO=OK&NUMERROR=0&TXTERROR=No%20hay%20errores.
    """
    estat = request.args.get('ESTADO', False)
    if estat != 'OK':
        return redirect(url_for('pagament_cancelat'))
    else:
        return redirect(url_for('pagament_realitzat'))


@app.route('/inversions', methods=['GET', 'POST'])
@check_online
def inversions():
    """Funció per mostrar el formulari d'inversions
    """
    if request.method == 'GET':
        return render_template('inversions.html')
    elif request.method == 'POST':
        error = False
        nsoci = request.form.get('nsoci', False)
        vat = request.form.get('vat', False)
        if not nsoci:
            flash(_(u"No s'ha introduït el número de soci"))
            error = True
        if not vat:
            flash(_(u"No s'ha introduït el DNI/CIF/NIE"))
            error = True
        if error:
            return render_template('inversions.html')
        vat = vat.upper()
        if not vat.startswith('ES'):
            vat = 'ES%s' % (vat,)
        if not nsoci.startswith('S'):
            nsoci = 'S%s' % (nsoci.zfill(6),)
        O = app.config['OOOP']
        # provem la combinació nsoci/vat
        found = O.ResPartner.search([('ref', '=', nsoci), ('vat', '=', vat)])
        if not found:
            flash(_(u"Accés incorrecte"))
            return render_template('inversions.html')
        session['loggedin'] = True
        session['uid'] = found[0]
        dades = O.ResPartner.read(found, ['name', 'ref', 'lang'])[0]
        session['lang'] = dades['lang']
        return render_template('inversions_tria.html', dades_soci=dades)


@app.route('/inversions/titols', methods=['GET'])
def inversions_titols():
    return render_template('inversions_tria.html')
    if not session.get('loggedin', False) or not session.get('uid', False):
        flash(_(u"Accés no autoritzat"))
        return redirect(url_for('inversions'))
    O = app.config['OOOP']
    soci = O.ResPartner.get(session['uid'])
    dades = {
        'name': soci.name,
        'ref': soci.ref,
        'adreca': ('%s\n'
                   '%s\n'
                   '%s (%s)' % (soci.address[0].street, soci.address[0].city,
                                soci.address[0].zip,
                                soci.address[0].id_municipi.name)),
        'email': soci.address[0].email
    }
    return render_template('inversions_titols_form.html', dades_soci=dades)


def check_inversions_form(required_fields, checkboxes, amount_min,
                                 amount_max, amount_module, check_acc_number):
    """Funció per validar el formulari d'inversions.
    """
    required_fields += checkboxes
    error_fields = []
    form = request.form
    for rfield in required_fields:
        if rfield not in form or not form[rfield]:
            error_fields.append(rfield)
    if error_fields:
        raise WebformException(_(u"Emplenar camps obligatoris, marcats amb "
                                 u"vermell."),
                                 error_fields=error_fields)
    amount = form['eur_inversio']
    amount = amount.replace(',', '.')
    try:
        amount = float(amount)
    except Exception:
        raise WebformException(_(u"Quantitat no vàlida"),
                               error_fields=['eur_inversio'])
    if amount < 0:
        raise WebformException(_(u"La quanitat ha de ser positiva."),
                               error_fields=['eur_inversio'])
    if amount == 0:
        raise WebformException(_(u"No vols aportar res? :)"),
                               error_fields=['eur_inversio'])
    elif amount < amount_min:
        raise WebformException(_(u"L'aportació mínima és de %%s€") % amount_min,
                               error_fields=['eur_inversio'])
    elif amount % amount_module != 0:
        raise WebformException(_(u"Les aportacions han de ser múltiples "
                                 u"de %%s") % amount_module,
                               error_fields=['eur_inversio'])
    elif amount_max and amount > amount_max:
        raise WebformException(_(u"Les aportacions no poden superar "
                                 u"els %%s€") % amount_max,
                               error_fields=['eur_inversio'])
    if check_acc_number:
        O = app.config['OOOP']
        acc_number = '%s %s %s %s' % (form['hentitat'], form['hsucursal'],
                                      form['hcontrol'], form['hncompte'])
        vals = O.ResPartnerBank.onchange_banco([], acc_number,
                        COUNTRY_ES, {'lang': '%s_ES' % get_locale()})
        if vals.get('warning', False):
            raise WebformException(vals['warning']['message'],
                                   error_fields=['hentitat', 'hsucursal',
                                                 'hcontrol', 'hncompte'])
    return error_fields


@app.route('/inversions/titols/confirmar', methods=['POST'])
def inversions_titols_confirmar():
    """Funció de confirmació per les inversions.
    """
    try:
        form = request.form
        O = app.config['OOOP']
        soci = O.ResPartner.get(session.get('uid'))
        dades = {
            'name': soci.name,
            'ref': soci.ref,
            'adreca': (
                '%s\n%s\n%s (%s)' % (
                    soci.address[0].street, soci.address[0].city,
                    soci.address[0].zip,
                    soci.address[0].id_municipi.name)
                ),
            'email': soci.address[0].email
        }
        required_fields = ['eur_inversio', 'hentitat', 'hsucursal', 'hcontrol',
                       'hncompte']
        checkboxes = ['condicions_ok', 'titular_ok', 'privacitat_ok',
                      'contracte_ok']
        amount_min = 1000
        amount_max = 100000
        amount_module = 1000
        check_acc_number = True
        check_inversions_form(required_fields, checkboxes, amount_min,
                              amount_max, amount_module, check_acc_number)
        amount = form['eur_inversio']
        amount = amount.replace(',', '.')
        session['amount'] = amount
        acc_number = '%s %s %s %s' % (form['hentitat'], form['hsucursal'],
                                      form['hcontrol'], form['hncompte'])
        session['acc_number'] = acc_number
        return render_template('inversions_titols_confirmacio.html',
                               acc_number=acc_number, amount=amount,
                               dades_soci=dades)
    except WebformException, exc:
        flash(exc, 'error')
        flash(_(u"Recorda tornar a marcar les caselles d'acceptació"), 'error')
        traceback.print_exc()
        return render_template('inversions_titols_form.html', dades_soci=dades,
                               efields=exc.error_fields)


@app.route('/inversions/titols/nova', methods=['POST'])
def inversions_titols_nova():
    """Funció per rebre el formulari d'una nova inversió.
    """
    if not session.get('loggedin', False):
        flash(_(u"Accés no autoritzat"))
        return redirect(url_for('inversions'))
    O = app.config['OOOP']
    error_fields = []
    try:
        t = O.begin()
        partner = t.ResPartner.get(session.get('uid'))
        dades = {
            'name': partner.name,
            'ref': partner.ref,
            'adreca': ('%s\n'
                       '%s\n'
                       '%s (%s)' % (partner.address[0].street,
                                    partner.address[0].city,
                                    partner.address[0].zip,
                                    partner.address[0].id_municipi.name)),
            'email': partner.address[0].email
        }
        if not 'amount' in session or not 'acc_number' in session:
            flash(_(u"Torna-ho a intentar entrant les dades al "
                    u"següent formulari"))
            return render_template('inversions_titols_form.html',
                                   efields=['eur_inversio', 'hentitat',
                                            'hsucursal', 'hcontrol',
                                            'hncompte'],
                                   dades_soci=dades)
        # Comprovem tots els camps obligatoris
        amount = float(session['amount'])
        # mirem el CCC si existeix o no
        acc_number = session['acc_number']
        search_params = [('acc_number', '=', acc_number),
                         ('partner_id', '=', session.get('uid'))]
        partner_bank = t.ResPartnerBank.search(search_params)
        if not partner_bank:
            vals = t.ResPartnerBank.onchange_banco([], acc_number,
                        COUNTRY_ES, {'lang': '%s_ES' % get_locale()})['value']
            vals.update({
                'name': '',
                'state': 'bank',
                'country_id': COUNTRY_ES,
                'partner_id': session.get('uid'),
                'acc_number': acc_number,
            })
            partner_bank = t.ResPartnerBank.create(vals)
        else:
            partner_bank = partner_bank[0]

        # cridem als "botons" d'assignar comptes per si els partners no els hi
        # tenen encara
        #t.ResPartner.button_assign_acc_43([partner.id])
        #t.ResPartner.button_assign_acc_1714([partner.id])
        #t.ResPartner.button_assign_acc_410([partner.id])

        t.ResPartner.write(partner.id, {'bank_inversions': partner_bank})
        partner = t.ResPartner.get(partner.id)
        # busquem una remesa amb mode de pagament = 'TITOLS (Enginyers)'
        # en estat 'Esborrany'. Si no n'hi ha cap, la creem nova.
        search_params = [('name', '=', 'TITOLS (Enginyers)')]
        payment_mode = t.PaymentMode.search(search_params)[0]
        search_params = [('mode', '=', payment_mode), ('state', '=', 'draft')]
        payment_order = t.PaymentOrder.search(search_params)
        if not payment_order:
            vals = {
                'date_prefered': 'fixed',
                'user_id': O.uid,
                'state': 'draft',
                'mode': payment_mode,
                'type': 'receivable',
                'create_account_moves': 'direct-payment',
            }
            payment_order = t.PaymentOrder.create(vals)
        else:
            payment_order = payment_order[0]
        order = t.PaymentOrder.get(payment_order)
        currency = t.ResCurrency.search([('code', '=', 'EUR')])
        payment_name = t.IrSequence.get_next('som.inversions.titols')
        vals = {
            'name': payment_name,
            'order_id': order.id,
            'currency_id': currency[0],
            'partner_id': partner.id,
            'company_currency': currency[0],
            'bank_id': partner_bank,
            'state': 'normal',
            'amount_currency': -1 * amount,
            #'account_id': partner.property_account_titols.id,
            'communication': _('COMPRA TITOLS SOM ENERGIA'),
            'comm_text': _('COMPRA TITOLS SOM ENERGIA'),
        }
        t.PaymentLine.create(vals)
        del session['amount']
        del session['acc_number']
        res = render_template('inversions_titols_ok.html', dades_soci=dades)
        t.commit()
        return res
    except xmlrpclib.Fault:
        if sentry:
            sentry.client.captureException()
        flash(_(u"Hi ha hagut un error, torneu-hi més tard siusplau."),
              'error')
        traceback.print_exc()
        t.rollback()
        return render_template('inversions_titols_form.html', dades_soci=dades,
                               efields=error_fields)
    except WebformException, exc:
        flash(exc, 'error')
        flash(_(u"Recorda tornar a marcar les caselles d'acceptació"), 'error')
        traceback.print_exc()
        t.rollback()
        return render_template('inversions_titols_form.html', dades_soci=dades,
                               efields=exc.error_fields)
    except Exception, exc:
        if sentry:
            sentry.client.captureException()
        flash(_(u"Hi ha hagut un error, torneu-hi més tard siusplau."),
              'error')
        traceback.print_exc()
        t.rollback()
        return render_template('inversions_titols_form.html', dades_soci=dades,
                               efields=error_fields)
    finally:
        t.close()
        del(t)


@app.route('/inversions/aportacio', methods=['GET'])
def inversions_aportacio():
    return render_template('inversions_tria.html')
    if not session.get('loggedin', False) or not session.get('uid', False):
        flash(_(u"Accés no autoritzat"))
        return redirect(url_for('inversions'))
    O = app.config['OOOP']
    dades = O.ResPartner.read(session['uid'], ['name', 'ref', 'lang'])
    return render_template('inversions_form.html', dades_soci=dades)


@app.route('/inversions/confirmar', methods=['POST'])
def inversions_confirmar():
    """Funció de confirmació per les inversions.
    """
    try:
        form = request.form
        O = app.config['OOOP']
        partner = O.ResPartner.get(session.get('uid'))
        dades_soci = {'name': partner.name}
        required_fields = ['eur_inversio', 'hentitat', 'hsucursal', 'hcontrol',
                       'hncompte']
        checkboxes = ['condicions_ok', 'titular_ok', 'privacitat_ok']
        amount_min = 100
        amount_max = 25000
        amount_module = 100
        check_acc_number = True
        check_inversions_form(required_fields, checkboxes, amount_min,
                              amount_max, amount_module, check_acc_number)
        amount = form['eur_inversio']
        amount = amount.replace(',', '.')
        session['amount'] = amount
        acc_number = '%s %s %s %s' % (form['hentitat'], form['hsucursal'],
                                      form['hcontrol'], form['hncompte'])
        session['acc_number'] = acc_number
        return render_template('inversions_confirmacio.html',
                               acc_number=acc_number, amount=amount)
    except WebformException, exc:
        flash(exc, 'error')
        flash(_(u"Recorda tornar a marcar les caselles d'acceptació"), 'error')
        traceback.print_exc()
        return render_template('inversions_form.html', dades_soci=dades_soci,
                               efields=exc.error_fields)


@app.route('/inversions/nova', methods=['POST'])
def inversions_nova():
    """Funció per rebre el formulari d'una nova inversió.
    """
    if not session.get('loggedin', False):
        flash(_(u"Accés no autoritzat"))
        return redirect(url_for('inversions'))
    O = app.config['OOOP']
    error_fields = []
    try:
        t = O.begin()
        partner = t.ResPartner.get(session.get('uid'))
        dades_soci = {'name': partner.name}
        if not 'amount' in session or not 'acc_number' in session:
            flash(_(u"Torna-ho a intentar entrant les dades al "
                    u"següent formulari"))
            return render_template('inversions_form.html',
                                   efields=['eur_inversio', 'hentitat',
                                            'hsucursal', 'hcontrol',
                                            'hncompte'],
                                   dades_soci=dades_soci)
        # Comprovem tots els camps obligatoris
        amount = float(session['amount'])
        # mirem el CCC si existeix o no
        acc_number = session['acc_number']
        search_params = [('acc_number', '=', acc_number),
                         ('partner_id', '=', session.get('uid'))]
        partner_bank = t.ResPartnerBank.search(search_params)
        if not partner_bank:
            vals = t.ResPartnerBank.onchange_banco([], acc_number,
                        COUNTRY_ES, {'lang': '%s_ES' % get_locale()})['value']
            vals.update({
                'name': '',
                'state': 'bank',
                'country_id': COUNTRY_ES,
                'partner_id': session.get('uid'),
                'acc_number': acc_number,
            })
            partner_bank = t.ResPartnerBank.create(vals)
        else:
            partner_bank = partner_bank[0]

        # cridem als "botons" d'assignar comptes per si els partners no els hi
        # tenen encara
        #t.ResPartner.button_assign_acc_43([partner.id])
        #t.ResPartner.button_assign_acc_163([partner.id])
        #t.ResPartner.button_assign_acc_410([partner.id])

        t.ResPartner.write(partner.id, {'bank_inversions': partner_bank})
        partner = t.ResPartner.get(partner.id)
        # busquem una remesa amb mode de pagament = 'APORTACIONS (Enginyers)'
        # en estat 'Esborrany'. Si no n'hi ha cap, la creem nova.
        search_params = [('name', '=', 'APORTACIONS (Enginyers)')]
        payment_mode = t.PaymentMode.search(search_params)[0]
        search_params = [('mode', '=', payment_mode), ('state', '=', 'draft')]
        payment_order = t.PaymentOrder.search(search_params)
        if not payment_order:
            vals = {
                'date_prefered': 'fixed',
                'user_id': O.uid,
                'state': 'draft',
                'mode': payment_mode,
                'type': 'receivable',
                'create_account_moves': 'direct-payment',
            }
            payment_order = t.PaymentOrder.create(vals)
        else:
            payment_order = payment_order[0]
        order = t.PaymentOrder.get(payment_order)
        currency = t.ResCurrency.search([('code', '=', 'EUR')])
        payment_name = t.IrSequence.get_next('som.inversions')
        vals = {
            'name': payment_name,
            'order_id': order.id,
            'currency_id': currency[0],
            'partner_id': partner.id,
            'company_currency': currency[0],
            'bank_id': partner_bank,
            'state': 'normal',
            'amount_currency': -1 * amount,
            #'account_id': partner.property_account_aportacions.id,
            'communication': _('APORTACIO SOM ENERGIA'),
            'comm_text': _('APORTACIO SOM ENERGIA'),
        }
        t.PaymentLine.create(vals)
        del session['amount']
        del session['acc_number']
        res = render_template('inversions_ok.html')
        t.commit()
        return res
    except xmlrpclib.Fault, err:
        if sentry:
            sentry.client.captureException()
        flash(_(u"Hi ha hagut un error, torneu-hi més tard siusplau."),
              'error')
        traceback.print_exc()
        t.rollback()
        return render_template('inversions_form.html', dades_soci=dades_soci,
                               efields=error_fields)
    except WebformException, exc:
        flash(exc, 'error')
        flash(_(u"Recorda tornar a marcar les caselles d'acceptació"), 'error')
        traceback.print_exc()
        t.rollback()
        return render_template('inversions_form.html', dades_soci=dades_soci,
                               efields=exc.error_fields)
    except Exception, exc:
        if sentry:
            sentry.client.captureException()
        flash(_(u"Hi ha hagut un error, torneu-hi més tard siusplau."), 'error')
        traceback.print_exc()
        t.rollback()
        return render_template('inversions_form.html', dades_soci=dades_soci,
                               efields=error_fields)
    finally:
        t.close()
        del(t)


if __name__ == '__main__':
    app.run()
