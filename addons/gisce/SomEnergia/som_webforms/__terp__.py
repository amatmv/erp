# -*- coding: utf-8 -*-
{
    "name": "SomEnergia WebForms",
    "description": """
    This module provide :
        * Webforms for SomEnergia website
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "SomEnergia",
    "depends":[
        "base",
        "som_partner_seq",
        "som_polissa_soci",
        "ws_transactions",
        "poweremail",
        "account",
        "account_payment",
        "base_extended",
        "giscedata_cups",
        "giscedata_facturacio",
        "giscedata_polissa",
        "giscemisc_cnae",
        "l10n_ES_remesas",
        "product",
        "stock"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "som_webforms_seq_data.xml",
        "som_webforms_data.xml",
        "ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
