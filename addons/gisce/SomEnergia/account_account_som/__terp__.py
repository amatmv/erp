# -*- coding: utf-8 -*-
{
    "name": "Mòdul de comptes",
    "description": """
    This module provide :
        * Obligació que un compte comptabñe tingui pare
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "SomEnergia",
    "depends":[
        "account",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "account_view.xml"
    ],
    "active": False,
    "installable": True
}
