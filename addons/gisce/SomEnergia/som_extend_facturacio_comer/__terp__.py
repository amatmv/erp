# -*- coding: utf-8 -*-
{
    "name": "Som Energia billing extension",
    "description": """SomEnergia billing extension""",
    "version": "0-dev",
    "author": "Som Energia",
    "category": "Master",
    "depends":[
        "base",
        "giscedata_facturacio_comer"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_facturacio_view.xml",
        "wizard/wizard_change_payment_type_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
