# -*- coding: utf-8 -*-

from destral import testing
from destral.transaction import Transaction

from datetime import date,datetime,timedelta

class TestPolissaWwwAutolectura(testing.OOTestCase):

    def model(self, model_name):
        return self.openerp.pool.get(model_name)

    def get_fixture(self, model, reference):
        return self.imd_obj.get_object_reference(
            self.cursor, self.uid,
            model,
            reference
        )[1]

    def setUp(self):
        self.pol_obj = self.model('giscedata.polissa')
        self.pool_lect_obj = self.model('giscedata.lectures.lectura.pool')
        self.fact_lect_obj = self.model('giscedata.lectures.lectura')
        self.meter_obj = self.model('giscedata.lectures.comptador')
        self.period_obj = self.model('giscedata.polissa.tarifa.periodes')
        self.origin_comer_obj = self.model('giscedata.lectures.origen_comer')
        self.origin_obj = self.model('giscedata.lectures.origen')
        self.tarif_obj = self.model('giscedata.polissa.tarifa')
        self.imd_obj = self.model('ir.model.data')

        self.txn = Transaction().start(self.database)

        self.cursor = self.txn.cursor
        self.uid = self.txn.user

    def tearDown(self):
        self.txn.stop()

    # Helper functions
    def create_lect_pool(self,pol_id,
        date,period,lect_type,value,observations,origen_comer_code,origin_code):
        return self.create_lect(
            pol_id,
            date,
            period,
            lect_type,
            value,
            observations,
            origen_comer_code,
            origin_code,
            True)

    def create_lect_fact(self,pol_id,
        date,period,lect_type,value,observations,origen_comer_code,origin_code):
        return self.create_lect(
            pol_id,
            date,
            period,
            lect_type,
            value,
            observations,
            origen_comer_code,
            origin_code,
            False)

    def create_lect(self,pol_id,
        date,period,lect_type,value,observations,origen_comer_code,origin_code,pool):
        # avaliable codes for origin_comer_code
        # ['Q1','F1','MA','OV','AL','ES','AT','CC']

        # available codes for origin_code
        # ['10','20','30','40','50','99','40','40','60']

        pol_val = self.pol_obj.read(
            self.cursor, self.uid, pol_id, ['comptador', 'tarifa'])
        if not pol_val:
            return None

        meter_search = [
            ('polissa.id', '=', pol_id),
            ('name', '=', pol_val['comptador'])]
        meter_ids = self.meter_obj.search(
            self.cursor, self.uid,meter_search)
        if not meter_ids:
            return None

        period_search = [
            ('tarifa.id', '=', pol_val['tarifa'][0]),
            ('tipus', '=', 'te'),
            ('agrupat_amb', '=', False),
            ('name', '=', period)]
        period_ids = self.period_obj.search(
            self.cursor, self.uid, period_search)
        if not period_ids:
            return None

        origin_comer_ids = self.origin_comer_obj.search(
            self.cursor, self.uid, 
            [('codi', '=', origen_comer_code)])
        if not origin_comer_ids:
            return None

        origin_id = self.origin_obj.search(
            self.cursor, self.uid, 
            [('codi', '=', origin_code)])[0]

        vals = {'comptador': meter_ids[0],
            'name': date,
            'periode': period_ids[0],
            'tipus': lect_type,
            'lectura': value,
            'observacions': observations,
            'origen_comer_id': origin_comer_ids[0],
            'origen_id': origin_id,
            }
        if pool:
            return self.pool_lect_obj.create(self.cursor,self.uid,vals)
        else:
            return self.fact_lect_obj.create(self.cursor,self.uid,vals)

    def test_create_lect_pool__add_lect_20A(self):
        pol_id = self.get_fixture('giscedata_polissa', 'polissa_0001')
        p_lect_id = self.create_lect_pool(
            pol_id,
            '2018-01-01',
            'P1',
            'A',
            100,
            'test',
            'OV',
            '50')
        p_lect = self.pool_lect_obj.browse(
            self.cursor,self.uid,p_lect_id)
        self.assertEqual(p_lect.name, '2018-01-01')
        self.assertEqual(p_lect.periode.name, 'P1')
        self.assertEqual(p_lect.tipus, 'A')
        self.assertEqual(p_lect.lectura, 100)
        self.assertEqual(p_lect.observacions, 'test')
        self.assertEqual(p_lect.origen_comer_id.name, 'Oficina Virtual')
        self.assertEqual(p_lect.origen_id.name, 'Autolectura')

    def test_create_lect_pool__add_2lect_20A(self):
        pol_id = self.get_fixture('giscedata_polissa', 'polissa_0001')
        p_lect1_id = self.create_lect_pool(
            pol_id,
            '2028-01-01',
            'P1',
            'A',
            100,
            'test',
            'OV',
            '50')
        p_lect2_id = self.create_lect_pool(
            pol_id,
            '2028-02-01',
            'P1',
            'A',
            120,
            'more test',
            'F1',
            '10')
        p_lect1 = self.pool_lect_obj.browse(
            self.cursor,self.uid,p_lect1_id)
        self.assertEqual(p_lect1.name, '2028-01-01')
        self.assertEqual(p_lect1.periode.name, 'P1')
        self.assertEqual(p_lect1.tipus, 'A')
        self.assertEqual(p_lect1.lectura, 100)
        self.assertEqual(p_lect1.observacions, 'test')
        self.assertEqual(p_lect1.origen_comer_id.name, 'Oficina Virtual')
        self.assertEqual(p_lect1.origen_id.name, 'Autolectura')
        p_lect2 = self.pool_lect_obj.browse(
            self.cursor,self.uid,p_lect2_id)
        self.assertEqual(p_lect2.name, '2028-02-01')
        self.assertEqual(p_lect2.periode.name, 'P1')
        self.assertEqual(p_lect2.tipus, 'A')
        self.assertEqual(p_lect2.lectura, 120)
        self.assertEqual(p_lect2.observacions, 'more test')
        self.assertEqual(p_lect2.origen_comer_id.name, 'Fitxer de factura F1')
        self.assertEqual(p_lect2.origen_id.name, 'Telemesura')

    def test_create_lect_pool__add_lect_20A_bad_polissa(self):
        p_lect_id = self.create_lect_pool(
            None,
            '2018-01-01',
            'P1',
            'A',
            100,
            'test',
            'OV',
            '50')
        self.assertEqual(p_lect_id, None)

    def test_create_lect_pool__add_lect_20A_bad_period(self):
        pol_id = self.get_fixture('giscedata_polissa', 'polissa_0001')
        p_lect_id = self.create_lect_pool(
            pol_id,
            '2018-01-01',
            'P2',
            'A',
            100,
            'test',
            'OV',
            '50')
        self.assertEqual(p_lect_id, None)

    def test_create_lect_pool__add_lect_20A_bad_origin_comer(self):
        pol_id = self.get_fixture('giscedata_polissa', 'polissa_0001')
        p_lect_id = self.create_lect_pool(
            pol_id,
            '2018-01-01',
            'P2',
            'A',
            100,
            'test',
            'ZZ',
            '50')
        self.assertEqual(p_lect_id, None)

    def test_create_lect_pool__add_lect_20A_bad_origin(self):
        pol_id = self.get_fixture('giscedata_polissa', 'polissa_0001')
        p_lect_id = self.create_lect_pool(
            pol_id,
            '2018-01-01',
            'P2',
            'A',
            100,
            'test',
            'OV',
            '42')
        self.assertEqual(p_lect_id, None)


    # Scenario buildup helper functions
    def create_initial_lecture(self, pol_id, period, value):
        initial_date = self.pol_obj.read(
            self.cursor,self.uid,pol_id,['data_alta']
            )['data_alta']
        return self.create_lect_pool(
            pol_id,
            initial_date,
            period,
            'A',
            value,
            'initial lecture',
            'F1',
            '10')

    def create_initial_CNMC_lecture(self, pol_id, period, value):
        initial_date = self.pol_obj.read(
            self.cursor,self.uid,pol_id,['data_alta']
            )['data_alta']
        initial_date = datetime.strptime(initial_date,"%Y-%m-%d")
        initial_date -= timedelta(days=1)
        initial_date = initial_date.strftime("%Y-%m-%d")
        return self.create_lect_pool(
            pol_id,
            initial_date,
            period,
            'A',
            value,
            'initial lecture',
            'F1',
            '10')

    def create_fact_lecture(self, pol_id, period, days_back, value):
        return self.create_lect_fact(
            pol_id,
            str(date.today() - timedelta(days=days_back)),
            period,
            'A',
            value,
            'invoicing lecture',
            'F1',
            '10')

    def create_pool_lecture(self, pol_id, period, days_back, value):
        return self.create_lect_pool(
            pol_id,
            str(date.today() - timedelta(days=days_back)),
            period,
            'A',
            value,
            'distri lecture',
            'F1',
            '10')

    def change_polissa_tariff(self, pol_id, tarif):
        tarif_ids = self.tarif_obj.search(self.cursor, self.uid,
            [('name','=',tarif)])
        if not tarif_ids:
            return None
        self.pol_obj.write(self.cursor, self.uid,
            pol_id, {'tarifa': tarif_ids[0]})
        return True

    def change_polissa_power_method(self, pol_id, method):
        self.pol_obj.write(self.cursor, self.uid,
            pol_id, {'facturacio_potencia': method})

    def disable_actual_meter(self, pol_id):
        meter_ids = self.meter_obj.search(
            self.cursor,self.uid,[
            ('polissa','=',pol_id),
            ])
        if meter_ids:
            self.meter_obj.write(
                self.cursor,self.uid,
                meter_ids[0],
                {'active': False})
            return meter_ids[0]
        return None

    def create_new_meter(self, pol_id, active, name, data_alta = None):
        meter_vals = {
            'polissa': pol_id,
            'name': name,
            'active': active,
        }
        if data_alta:
            meter_vals['data_alta'] = data_alta
        meter_id = self.meter_obj.create(
            self.cursor,self.uid,meter_vals)
        return meter_id

    # Main cases testing functions
    def test_www_autolectura__wrong_date(self):
        result = self.pol_obj.www_autolectura(
            self.cursor, self.uid,
            None,
            "wrong date",
            {})
        self.assertFalse(result['status'])
        self.assertEqual(result['message'],u'Formato de fecha incorrecto.')

    def test_www_autolectura__none_date(self):
        result = self.pol_obj.www_autolectura(
            self.cursor, self.uid,
            None,
            None,
            {})
        self.assertFalse(result['status'])
        self.assertEqual(result['message'],u'Formato de fecha incorrecto.')

    def test_www_autolectura__future_date(self):
        result = self.pol_obj.www_autolectura(
            self.cursor, self.uid,
            None,
            (date.today() + timedelta(days=1)).strftime("%d-%m-%Y"),
            {})
        self.assertFalse(result['status'])
        self.assertEqual(result['message'],u'Fecha mayor a día de hoy.')

    def test_www_autolectura__maximeter(self):
        pol_id = self.get_fixture('giscedata_polissa', 'polissa_0001')
        self.change_polissa_power_method(pol_id,'max')
        result = self.pol_obj.www_autolectura(
            self.cursor, self.uid,
            pol_id,
            (date.today() - timedelta(days=1)).strftime("%d-%m-%Y"),
            {})
        self.assertFalse(result['status'])
        self.assertEqual(result['message'],(
            u"Tienes contratada facturación por maxímetro. Te pedimos "
            u"que nos envíes tus lecturas a través de un correo "
            u"electrónico a factura@somenergia.coop"))

    def test_www_autolectura__20A_two_meters_active(self):
        pol_id = self.get_fixture('giscedata_polissa', 'polissa_0001')
        self.change_polissa_power_method(pol_id,'icp')
        self.create_new_meter(pol_id,True,'31415269')

        result = self.pol_obj.www_autolectura(
            self.cursor, self.uid,
            pol_id,
            (date.today() - timedelta(days=1)).strftime("%d-%m-%Y"),
            {
                'P1':300,
            })
        self.assertFalse(result['status'])
        self.assertEqual(result['message'],(
            u"En tu contrato hay dos contadores activos. Te pedimos "
            u"que nos envíes tus lecturas a través de un correo "
            u"electrónico a factura@somenergia.coop."))

    def test_www_autolectura__20A_without_inital_lect(self):
        pol_id = self.get_fixture('giscedata_polissa', 'polissa_0001')
        self.change_polissa_power_method(pol_id,'icp')

        result = self.pol_obj.www_autolectura(
            self.cursor, self.uid,
            pol_id,
            (date.today() - timedelta(days=1)).strftime("%d-%m-%Y"),
            {
                'P1':300,
            })
        self.assertFalse(result['status'])
        self.assertEqual(result['message'],(
            u'La distribuidora todavía no nos ha facilitado la '
            u'lectura inicial de tu contrato. Para evitar problemas '
            u'en la facturación, te pedimos que nos envíes tu '
            u'autolectura a través de un correo electrónico a '
            u'factura@somenergia.coop y revisaremos el caso.'))

    def test_www_autolectura__20DHA_with_partial_inital_lect(self):
        pol_id = self.get_fixture('giscedata_polissa', 'polissa_0001')
        self.change_polissa_power_method(pol_id,'icp')
        self.change_polissa_tariff(pol_id,'2.0DHS')
        self.create_initial_lecture(pol_id,'P2',200)

        result = self.pol_obj.www_autolectura(
            self.cursor, self.uid,
            pol_id,
            (date.today() - timedelta(days=1)).strftime("%d-%m-%Y"),
            {
                'P1':300,
                'P2':500,
            })
        self.assertFalse(result['status'])
        self.assertEqual(result['message'],(
            u'La distribuidora todavía no nos ha facilitado la '
            u'lectura inicial de tu contrato. Para evitar problemas '
            u'en la facturación, te pedimos que nos envíes tu '
            u'autolectura a través de un correo electrónico a '
            u'factura@somenergia.coop y revisaremos el caso.'))

    def test_www_autolectura__20DHA_without_inital_lect(self):
        pol_id = self.get_fixture('giscedata_polissa', 'polissa_0001')
        self.change_polissa_power_method(pol_id,'icp')
        self.change_polissa_tariff(pol_id,'2.0DHS')

        result = self.pol_obj.www_autolectura(
            self.cursor, self.uid,
            pol_id,
            (date.today() - timedelta(days=1)).strftime("%d-%m-%Y"),
            {
                'P1':300,
                'P2':500,
            })
        self.assertFalse(result['status'])
        self.assertEqual(result['message'],(
            u'La distribuidora todavía no nos ha facilitado la '
            u'lectura inicial de tu contrato. Para evitar problemas '
            u'en la facturación, te pedimos que nos envíes tu '
            u'autolectura a través de un correo electrónico a '
            u'factura@somenergia.coop y revisaremos el caso.'))

    def test_www_autolectura__20A_bad_period(self):
        pol_id = self.get_fixture('giscedata_polissa', 'polissa_0001')
        self.change_polissa_power_method(pol_id,'icp')
        self.create_initial_lecture(pol_id,'P1',200)

        result = self.pol_obj.www_autolectura(
            self.cursor, self.uid,
            pol_id,
            (date.today() - timedelta(days=1)).strftime("%d-%m-%Y"),
            {'P2':1000})
        self.assertFalse(result['status'])
        self.assertEqual(result['message'],u'Periodo incorrecto.')

    def test_www_autolectura__20DHA_bad_period(self):
        pol_id = self.get_fixture('giscedata_polissa', 'polissa_0001')
        self.change_polissa_power_method(pol_id,'icp')
        self.change_polissa_tariff(pol_id,'2.0DHA')
        self.create_initial_lecture(pol_id,'P1',200)
        self.create_initial_lecture(pol_id,'P2',200)

        result = self.pol_obj.www_autolectura(
            self.cursor, self.uid,
            pol_id,
            (date.today() - timedelta(days=1)).strftime("%d-%m-%Y"),
            {'P5':1000})
        self.assertFalse(result['status'])
        self.assertEqual(result['message'],u'Periodo incorrecto.')

    def test_www_autolectura__two_lects_same_day(self):
        test_date = date.today() - timedelta(days=1)
        pol_id = self.get_fixture('giscedata_polissa', 'polissa_0001')
        self.change_polissa_power_method(pol_id,'icp')
        self.create_initial_lecture(pol_id,'P1',200)
        self.create_lect_pool(
            pol_id,str(test_date),'P1','A',200,'test','OV','50')

        result = self.pol_obj.www_autolectura(
            self.cursor, self.uid,
            pol_id,
            test_date.strftime("%d-%m-%Y"),
            {'P1':300})
        self.assertFalse(result['status'])
        self.assertEqual(result['message'],
            u'Ya existe una lectura para la fecha indicada.')

    def test_www_autolectura__20A_inferior_measure(self):
        pol_id = self.get_fixture('giscedata_polissa', 'polissa_0001')
        self.change_polissa_power_method(pol_id,'icp')
        self.create_initial_lecture(pol_id,'P1',200)
        self.create_fact_lecture(pol_id,'P1',2,600)

        result = self.pol_obj.www_autolectura(
            self.cursor, self.uid,
            pol_id,
            (date.today() - timedelta(days=1)).strftime("%d-%m-%Y"),
            {
                'P1':599,
            })
        self.assertFalse(result['status'])
        self.assertEqual(result['message'],(
            u"La lectura que nos pasas es inferior a la última "
            u"facturada. Te pedimos que nos la envíes a través "
            u"de un correo electrónico a factura@somenergia.coop "
            u"y revisaremos el caso."))

    def test_www_autolectura__20DHA_one_inferior_measure(self):
        pol_id = self.get_fixture('giscedata_polissa', 'polissa_0001')
        self.change_polissa_power_method(pol_id,'icp')
        self.change_polissa_tariff(pol_id,'2.0DHA')
        self.create_initial_lecture(pol_id,'P1',200)
        self.create_initial_lecture(pol_id,'P2',200)
        self.create_fact_lecture(pol_id,'P1',2,600)
        self.create_fact_lecture(pol_id,'P2',2,600)

        result = self.pol_obj.www_autolectura(
            self.cursor, self.uid,
            pol_id,
            (date.today() - timedelta(days=1)).strftime("%d-%m-%Y"),
            {
                'P1':500,
                'P2':750,
            })
        self.assertFalse(result['status'])
        self.assertEqual(result['message'],(
            u"La lectura que nos pasas es inferior a la última "
            u"facturada. Te pedimos que nos la envíes a través "
            u"de un correo electrónico a factura@somenergia.coop "
            u"y revisaremos el caso."))

    def test_www_autolectura__20A_inferior_pool_measure(self):
        pol_id = self.get_fixture('giscedata_polissa', 'polissa_0001')
        self.change_polissa_power_method(pol_id,'icp')
        self.create_initial_lecture(pol_id,'P1',200)
        self.create_fact_lecture(pol_id,'P1',10,500)
        self.create_pool_lecture(pol_id,'P1',2,600)

        result = self.pol_obj.www_autolectura(
            self.cursor, self.uid,
            pol_id,
            (date.today() - timedelta(days=1)).strftime("%d-%m-%Y"),
            {
                'P1':599,
            })
        self.assertFalse(result['status'])
        self.assertEqual(result['message'],(
            u"La lectura que nos pasas es inferior a la última "
            u"de distribuidora. Te pedimos que nos la envíes a través "
            u"de un correo electrónico a factura@somenergia.coop "
            u"y revisaremos el caso."))

    def test_www_autolectura__20DHA_one_inferior_pool_measure(self):
        pol_id = self.get_fixture('giscedata_polissa', 'polissa_0001')
        self.change_polissa_power_method(pol_id,'icp')
        self.change_polissa_tariff(pol_id,'2.0DHA')
        self.create_initial_lecture(pol_id,'P1',200)
        self.create_initial_lecture(pol_id,'P2',200)
        self.create_fact_lecture(pol_id,'P1',10,400)
        self.create_fact_lecture(pol_id,'P2',10,700)
        self.create_pool_lecture(pol_id,'P1',2,600)
        self.create_pool_lecture(pol_id,'P2',2,600)

        result = self.pol_obj.www_autolectura(
            self.cursor, self.uid,
            pol_id,
            (date.today() - timedelta(days=1)).strftime("%d-%m-%Y"),
            {
                'P1':500,
                'P2':750,
            })
        self.assertFalse(result['status'])
        self.assertEqual(result['message'],(
            u"La lectura que nos pasas es inferior a la última "
            u"de distribuidora. Te pedimos que nos la envíes a través "
            u"de un correo electrónico a factura@somenergia.coop "
            u"y revisaremos el caso."))

    def test_www_autolectura__20A_exteme_high_new_measure(self):
        pol_id = self.get_fixture('giscedata_polissa', 'polissa_0001')
        self.change_polissa_power_method(pol_id,'icp')
        self.create_initial_lecture(pol_id,'P1',200)
        self.create_fact_lecture(pol_id,'P1',10,500)
        self.create_pool_lecture(pol_id,'P1',2,600)

        result = self.pol_obj.www_autolectura(
            self.cursor, self.uid,
            pol_id,
            (date.today() - timedelta(days=1)).strftime("%d-%m-%Y"),
            {
                'P1':2000.1,
            })
        self.assertFalse(result['status'])
        self.assertEqual(result['message'],(
            u"La lectura que nos pasas implica un consumo muy "
            u"superior al que facturamos habitualmente. Te "
            u"pedimos que nos la envíes a través de un correo "
            u"electrónico a factura@somenergia.coop y "
            u"revisaremos el caso."))

    def test_www_autolectura__20DHA_one_exteme_high_new_measure(self):
        pol_id = self.get_fixture('giscedata_polissa', 'polissa_0001')
        self.change_polissa_power_method(pol_id,'icp')
        self.change_polissa_tariff(pol_id,'2.0DHA')
        self.create_initial_lecture(pol_id,'P1',200)
        self.create_initial_lecture(pol_id,'P2',200)
        self.create_fact_lecture(pol_id,'P1',10,400)
        self.create_fact_lecture(pol_id,'P2',10,700)
        self.create_pool_lecture(pol_id,'P1',2,600)
        self.create_pool_lecture(pol_id,'P2',2,600)

        result = self.pol_obj.www_autolectura(
            self.cursor, self.uid,
            pol_id,
            (date.today() - timedelta(days=1)).strftime("%d-%m-%Y"),
            {
                'P1':1900.1,
                'P2':750,
            })
        self.assertFalse(result['status'])
        self.assertEqual(result['message'],(
            u"La lectura que nos pasas implica un consumo muy "
            u"superior al que facturamos habitualmente. Te "
            u"pedimos que nos la envíes a través de un correo "
            u"electrónico a factura@somenergia.coop y "
            u"revisaremos el caso."))

    def test_www_autolectura__21A_exteme_high_new_measure(self):
        pol_id = self.get_fixture('giscedata_polissa', 'polissa_0001')
        self.change_polissa_power_method(pol_id,'icp')
        self.change_polissa_tariff(pol_id,'2.1A')
        self.create_initial_lecture(pol_id,'P1',200)
        self.create_fact_lecture(pol_id,'P1',10,500)
        self.create_pool_lecture(pol_id,'P1',2,600)

        result = self.pol_obj.www_autolectura(
            self.cursor, self.uid,
            pol_id,
            (date.today() - timedelta(days=1)).strftime("%d-%m-%Y"),
            {
                'P1':3500.1,
            })
        self.assertFalse(result['status'])
        self.assertEqual(result['message'],(
            u"La lectura que nos pasas implica un consumo muy "
            u"superior al que facturamos habitualmente. Te "
            u"pedimos que nos la envíes a través de un correo "
            u"electrónico a factura@somenergia.coop y "
            u"revisaremos el caso."))

    def test_www_autolectura__21DHA_one_exteme_high_new_measure(self):
        pol_id = self.get_fixture('giscedata_polissa', 'polissa_0001')
        self.change_polissa_power_method(pol_id,'icp')
        self.change_polissa_tariff(pol_id,'2.1DHA')
        self.create_initial_lecture(pol_id,'P1',200)
        self.create_initial_lecture(pol_id,'P2',200)
        self.create_fact_lecture(pol_id,'P1',10,400)
        self.create_fact_lecture(pol_id,'P2',10,700)
        self.create_pool_lecture(pol_id,'P1',2,600)
        self.create_pool_lecture(pol_id,'P2',2,600)

        result = self.pol_obj.www_autolectura(
            self.cursor, self.uid,
            pol_id,
            (date.today() - timedelta(days=1)).strftime("%d-%m-%Y"),
            {
                'P1':700,
                'P2':3700.1,
            })
        self.assertFalse(result['status'])
        self.assertEqual(result['message'],(
            u"La lectura que nos pasas implica un consumo muy "
            u"superior al que facturamos habitualmente. Te "
            u"pedimos que nos la envíes a través de un correo "
            u"electrónico a factura@somenergia.coop y "
            u"revisaremos el caso."))

    def test_www_autolectura__20A_cnmc_date_ok(self):
        pol_id = self.get_fixture('giscedata_polissa', 'polissa_0001')
        self.change_polissa_power_method(pol_id,'icp')
        self.create_initial_CNMC_lecture(pol_id,'P1',200)
        self.create_fact_lecture(pol_id,'P1',2,200)
        self.create_pool_lecture(pol_id,'P1',2,200)

        result = self.pol_obj.www_autolectura(
            self.cursor, self.uid,
            pol_id,
            (date.today() - timedelta(days=1)).strftime("%d-%m-%Y"),
            {
                'P1':300,
            })
        self.assertTrue(result['status'])
        self.assertEqual(result['message'],
            u'La Autolectura ha sido guardada correctamente.')

    def test_www_autolectura__20A_ok(self):
        pol_id = self.get_fixture('giscedata_polissa', 'polissa_0001')
        self.change_polissa_power_method(pol_id,'icp')
        self.create_initial_lecture(pol_id,'P1',200)
        self.create_fact_lecture(pol_id,'P1',2,200)
        self.create_pool_lecture(pol_id,'P1',2,200)

        result = self.pol_obj.www_autolectura(
            self.cursor, self.uid,
            pol_id,
            (date.today() - timedelta(days=1)).strftime("%d-%m-%Y"),
            {
                'P1':300,
            })
        self.assertTrue(result['status'])
        self.assertEqual(result['message'],
            u'La Autolectura ha sido guardada correctamente.')

    def test_www_autolectura__20A_two_meters_one_active_ok(self):
        pol_id = self.get_fixture('giscedata_polissa', 'polissa_0001')
        self.change_polissa_power_method(pol_id,'icp')
        self.create_initial_lecture(pol_id,'P1',200)
        self.create_fact_lecture(pol_id,'P1',2,200)
        self.create_pool_lecture(pol_id,'P1',2,200)
        self.create_new_meter(pol_id,False,'31415269')

        result = self.pol_obj.www_autolectura(
            self.cursor, self.uid,
            pol_id,
            (date.today() - timedelta(days=1)).strftime("%d-%m-%Y"),
            {
                'P1':300,
            })
        self.assertTrue(result['status'])
        self.assertEqual(result['message'],
            u'La Autolectura ha sido guardada correctamente.')

    def test_www_autolectura__20A_two_meters_last_active_ok(self):
        pol_id = self.get_fixture('giscedata_polissa', 'polissa_0001')
        self.change_polissa_power_method(pol_id,'icp')
        self.create_initial_lecture(pol_id,'P1',150)
        self.create_fact_lecture(pol_id,'P1',4,200)
        self.create_pool_lecture(pol_id,'P1',4,200)
        self.disable_actual_meter(pol_id)
        self.create_new_meter(pol_id,True,'31415269','2017-01-01')
        self.create_fact_lecture(pol_id,'P1',2,250)
        self.create_pool_lecture(pol_id,'P1',2,250)

        result = self.pol_obj.www_autolectura(
            self.cursor, self.uid,
            pol_id,
            (date.today() - timedelta(days=1)).strftime("%d-%m-%Y"),
            {
                'P1':300,
            })
        self.assertTrue(result['status'])
        self.assertEqual(result['message'],
            u'La Autolectura ha sido guardada correctamente.')

    def test_www_autolectura__20DHA_ok(self):
        pol_id = self.get_fixture('giscedata_polissa', 'polissa_0001')
        self.change_polissa_power_method(pol_id,'icp')
        self.change_polissa_tariff(pol_id,'2.0DHA')
        self.create_initial_lecture(pol_id,'P1',200)
        self.create_initial_lecture(pol_id,'P2',200)
        self.create_fact_lecture(pol_id,'P1',2,300)
        self.create_fact_lecture(pol_id,'P2',2,400)
        self.create_pool_lecture(pol_id,'P1',2,300)
        self.create_pool_lecture(pol_id,'P2',2,400)

        result = self.pol_obj.www_autolectura(
            self.cursor, self.uid,
            pol_id,
            (date.today() - timedelta(days=1)).strftime("%d-%m-%Y"),
            {
                'P1':500,
                'P2':750,
            })
        self.assertTrue(result['status'])
        self.assertEqual(result['message'],
            u'La Autolectura ha sido guardada correctamente.')

    def test_www_autolectura__20DHA_incomplete_ok(self):
        pol_id = self.get_fixture('giscedata_polissa', 'polissa_0001')
        self.change_polissa_power_method(pol_id,'icp')
        self.change_polissa_tariff(pol_id,'2.0DHS')
        self.create_initial_lecture(pol_id,'P1',200)
        self.create_initial_lecture(pol_id,'P2',200)
        self.create_initial_lecture(pol_id,'P3',200)
        self.create_fact_lecture(pol_id,'P1',2,300)
        self.create_fact_lecture(pol_id,'P2',2,400)
        self.create_pool_lecture(pol_id,'P1',2,300)
        self.create_pool_lecture(pol_id,'P2',2,400)

        result = self.pol_obj.www_autolectura(
            self.cursor, self.uid,
            pol_id,
            (date.today() - timedelta(days=1)).strftime("%d-%m-%Y"),
            {
                'P2':750,
            })
        self.assertTrue(result['status'])
        self.assertEqual(result['message'],
            u'La Autolectura ha sido guardada correctamente.')

    def test_www_autolectura__20DHS_ok(self):
        pol_id = self.get_fixture('giscedata_polissa', 'polissa_0001')
        self.change_polissa_power_method(pol_id,'icp')
        self.change_polissa_tariff(pol_id,'2.0DHS')
        self.create_initial_lecture(pol_id,'P1',200)
        self.create_initial_lecture(pol_id,'P2',200)
        self.create_initial_lecture(pol_id,'P3',200)
        self.create_fact_lecture(pol_id,'P1',2,100)
        self.create_fact_lecture(pol_id,'P2',2,450)
        self.create_fact_lecture(pol_id,'P3',2,800)
        self.create_pool_lecture(pol_id,'P1',2,100)
        self.create_pool_lecture(pol_id,'P2',2,450)
        self.create_pool_lecture(pol_id,'P3',2,800)

        result = self.pol_obj.www_autolectura(
            self.cursor, self.uid,
            pol_id,
            (date.today() - timedelta(days=1)).strftime("%d-%m-%Y"),
            {
                'P1':200,
                'P2':500,
                'P3':1000,
            })
        self.assertTrue(result['status'])
        self.assertEqual(result['message'],
            u'La Autolectura ha sido guardada correctamente.')

    def esborrar_lectures(self, cursor, uid, lect, context=None):
        """Esborrar les lectures"""
        lect_ene = self.pool.get('giscedata.lectures.lectura.pool')
        lect_pot = self.pool.get('giscedata.lectures.potencia.pool')
        for i in lect:
            if i[0] in ['A', 'R']:
                lect_ene.unlink(cursor, uid, [i[1]], context)
            else:
                lect_pot.unlink(cursor, uid, [i[1]], context)

    def test_www_ultimes_lectures_reals_retorna_reals_i_estimades(self):
        pol_obj = self.openerp.pool.get('giscedata.polissa')
        pol_id = self.get_fixture('giscedata_polissa', 'polissa_0002')
        test_date = date.today() - timedelta(days=1)
        self.change_polissa_power_method(pol_id, 'icp')

        a = self.create_lect_pool(
            pol_id, str(test_date), 'P1', 'A', 22287, 'test', 'OV', '50')

        b = self.create_lect_pool(
            pol_id, str(test_date-timedelta(days=31)), 'P1', 'A', 33387, 'test', 'OV', '40')

        c = self.create_lect_pool(
            pol_id, str(test_date-timedelta(days=61)), 'P1', 'A', 99987, 'test', 'OV', '99')

        res = pol_obj.www_ultimes_lectures_reals(self.cursor, self.uid, pol_id)
        kw_lects = [i['lectura'] for i in res]

        self.assertIn(22287, kw_lects)
        self.assertIn(33387, kw_lects)
        self.assertNotIn(99987, kw_lects)

        # Test origen correcto
        for lectura in res:
            if lectura['lectura'] == 22287:
                self.assertEqual(lectura['origen'], 'Autolectura')
            elif lectura['lectura'] == 33387:
                self.assertEqual(lectura['origen'], 'Distribuidora (Estimada)')
