# -*- coding: utf-8 -*-
from datetime import datetime, timedelta
from dateutil import parser

from osv import osv
from osv import fields


class GiscedataPolissa(osv.osv):

    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def _www_current_pagament(self, cursor, uid, ids, field_name, arg,
                              context=None):
        if not isinstance(ids, (list, set, tuple)):
            ids = [ids]
        res = dict.fromkeys(ids, True)
        factura_obj = self.pool.get('giscedata.facturacio.factura')
        conf_obj = self.pool.get('res.config')
        interval_venciment = int(conf_obj.get(
            cursor, uid, 'www_interval_venciment', '10'))
        now = datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
        for polissa_id in ids:
            search = [('state', '=', 'open'),
                      ('polissa_id', '=', polissa_id),
                      ('type', 'like', 'out\_')]
            invoices_open = factura_obj.search(cursor, uid, search)
            if invoices_open:
                invoices = factura_obj.read(
                    cursor, uid, invoices_open, ['date_invoice'])
                for invoice in invoices:
                    date_invoice = datetime.strptime(
                        invoice['date_invoice'], '%Y-%m-%d') + timedelta(
                        days=interval_venciment)
                    if date_invoice < now:
                        res[polissa_id] = False
        return res

    def _www_get_comptador_id(self, cursor, uid, polissa_id, comptador_name):
        comptador_obj = self.pool.get('giscedata.lectures.comptador')
        comptador_id = False
        if comptador_name:
            search_params = [('polissa.id', '=', polissa_id),
                             ('name', '=', comptador_name)]
            comptador_id = comptador_obj.search(
                cursor, uid, search_params, 0, 0, False, {'active_test':False})
            if comptador_id:
                comptador_id = comptador_id[0]
        return comptador_id

    def www_ultimes_lectures_reals(self, cursor, uid, ids):
        lectures = []
        if isinstance(ids, (list, set, tuple)):
            polissa_id = ids[0]
        else:
            polissa_id = ids
        conf_obj = self.pool.get('res.config')
        pool_lect_obj = self.pool.get('giscedata.lectures.lectura.pool')
        polissa_obj = self.pool.get('giscedata.polissa')
        lect_origen_obj = self.pool.get('giscedata.lectures.origen')
        comptador_obj = self.pool.get('giscedata.lectures.comptador')

        # Eliminat codi 40 del filtre, ara retorna també estimades
        search_params = [('codi', 'not in', ['99'])]
        origen_ids = lect_origen_obj.search(cursor, uid, search_params)
        comptador_name = polissa_obj.read(
            cursor, uid, polissa_id, ['comptador'])
        comptador_id = self._www_get_comptador_id(
            cursor, uid, polissa_id, comptador_name['comptador'])

        if not comptador_name['comptador']:
            search_params = [('polissa.id', '=', polissa_id)]
            llista_comptadors = comptador_obj.search(
                cursor, uid, search_params, 0, 0, False, {'active_test':False})
            llista_comptadors.sort(reverse = True)
            comptador_name = comptador_obj.read(
                cursor, uid, llista_comptadors[0], ['name'])
            comptador_id = self._www_get_comptador_id(
                cursor, uid, polissa_id, comptador_name['name'])

        if not comptador_id:
            return lectures

        search_params = [('comptador.id', '=', comptador_id),
                         ('origen_id', 'in', origen_ids),
                         ('tipus', '=', 'A')]
        limit_lectures = int(conf_obj.get(
            cursor, uid, 'www_limit_lectures', '5'))
        lect_ids = pool_lect_obj.search(
            cursor, uid, search_params, 0, limit_lectures, 'name desc', {'active_test':False})
        read_fields = ['name', 'periode', 'lectura', 'origen_id']
        for lectura in pool_lect_obj.read(cursor, uid, lect_ids, read_fields):
            data = datetime.strptime(lectura['name'], '%Y-%m-%d')
            origen_code = lect_origen_obj.read(
                cursor, uid, lectura['origen_id'][0], ['codi']
            )['codi']

            if origen_code in ('50',):
                origen = 'Autolectura'
            elif origen_code in ('40',):
                origen = 'Distribuidora (Estimada)'
            else:
                origen = 'Distribuidora (Real)'
            lectures.append({'data': data.strftime('%d-%m-%Y'),
                             'periode': lectura['periode'][1],
                             'lectura': lectura['lectura'],
                             'origen': origen
                             })
        return lectures


    def www_autolectura(self, cursor, uid, ids, data, periodes):
        def get_periode(name, dicts):
            return (element for element in dicts
                    if element['name'] == name).next()
        resultat = {'status': True,
                    'message': u'La Autolectura ha sido guardada correctamente.'}

        if isinstance(ids, (list, set, tuple)):
            polissa_id = ids[0]
        else:
            polissa_id = ids

        try:
            data_autolectura = parser.parse(data, dayfirst=True)
        except:
            resultat['status'] = False
            resultat['message'] = u'Formato de fecha incorrecto.'
            return resultat

        if data_autolectura > datetime.now():
            resultat['status'] = False
            resultat['message'] = u'Fecha mayor a día de hoy.'
            return resultat
        origen_comer_obj = self.pool.get('giscedata.lectures.origen_comer')
        periodes_obj = self.pool.get('giscedata.polissa.tarifa.periodes')
        origen_obj = self.pool.get('giscedata.lectures.origen')
        polissa_obj = self.pool.get('giscedata.polissa')
        pool_lect_obj = self.pool.get('giscedata.lectures.lectura.pool')
        fact_lect_obj = self.pool.get('giscedata.lectures.lectura')

        polissa_dades = polissa_obj.read(
            cursor, uid, polissa_id,[
            'comptador',
            'tarifa',
            'data_alta',
            'facturacio_potencia',
            'data_ultima_lectura'])

        if polissa_dades['facturacio_potencia'] == 'max':
                resultat['status'] = False
                resultat['message'] = (
                    u"Tienes contratada facturación por maxímetro. Te pedimos "
                    u"que nos envíes tus lecturas a través de un correo "
                    u"electrónico a factura@somenergia.coop")
                return resultat

        search_params = [('codi', '=', 'OV')]
        origen_comer_id = origen_comer_obj.search(cursor, uid, search_params)[0]
        search_params = [('codi', '=', '50')]
        origen_id = origen_obj.search(cursor, uid, search_params)[0]

        search_params = [('tarifa.id', '=', polissa_dades['tarifa'][0]),
                         ('tipus', '=', 'te'),
                         ('agrupat_amb', '=', False)]
        periodes_ids = periodes_obj.search(cursor, uid, search_params)
        periodes_data = periodes_obj.read(cursor, uid, periodes_ids,['name'])
        noms_periodes = [x['name'] for x in periodes_data]

        meter_obj = self.pool.get('giscedata.lectures.comptador')
        meter_ids = meter_obj.search(cursor, uid, [
            ('polissa.id', '=', polissa_id),
            ])
        if len(meter_ids) > 1:
            resultat['status'] = False
            resultat['message'] = (
                u"En tu contrato hay dos contadores activos. Te pedimos "
                u"que nos envíes tus lecturas a través de un correo "
                u"electrónico a factura@somenergia.coop.")
            return resultat

        comptador_id = self._www_get_comptador_id(
            cursor, uid, polissa_id, polissa_dades['comptador'])

        if not polissa_dades['data_ultima_lectura']:
            all_meter_ids = meter_obj.search(cursor, uid, [
                ('polissa.id', '=', polissa_id),
                ],context={'active_test': False})
            for period in noms_periodes:
                initial_lect_d_ids =  pool_lect_obj.search(
                    cursor, uid,[
                    ('comptador', 'in', all_meter_ids),
                    ('periode', '=', period),
                    ('name', '=', polissa_dades['data_alta']),
                    ])

                cnmcDminus1 = polissa_dades['data_alta']
                cnmcDminus1 = datetime.strptime(cnmcDminus1,"%Y-%m-%d")
                cnmcDminus1 -= timedelta(days=1)
                cnmcDminus1 = cnmcDminus1.strftime("%Y-%m-%d")
                initial_lect_d_minus_1_ids = pool_lect_obj.search(
                    cursor, uid,[
                    ('comptador', 'in', all_meter_ids),
                    ('periode', '=', period),
                    ('name', '=', cnmcDminus1),
                    ])
                if not (initial_lect_d_ids or initial_lect_d_minus_1_ids):
                    resultat['status'] = False
                    resultat['message'] = (
                        u'La distribuidora todavía no nos ha facilitado la '
                        u'lectura inicial de tu contrato. Para evitar problemas '
                        u'en la facturación, te pedimos que nos envíes tu '
                        u'autolectura a través de un correo electrónico a '
                        u'factura@somenergia.coop y revisaremos el caso.')
                    return resultat

        values = []
        tipus_lectura = 'A'
        observacions = 'Autolectura oficina virtual'
        for p in periodes.iteritems():
            if p[0] not in noms_periodes:
                resultat['status'] = False
                resultat['message'] = u'Periodo incorrecto.'
                return resultat
            periode = get_periode(p[0], periodes_data)
            vals = {'comptador': comptador_id,
                    'name': data_autolectura.strftime('%Y-%m-%d'),
                    'periode': periode['id'],
                    'tipus': tipus_lectura,
                    'lectura': p[1],
                    'observacions': observacions,
                    'origen_comer_id': origen_comer_id,
                    'origen_id': origen_id,
                    }
            #comprovem que no hi és
            search_vals = [('comptador', '=', vals['comptador']),
                           ('name', '=', vals['name']),
                           ('periode', '=', vals['periode']),
                           ('tipus', '=', vals['tipus'])]
            if pool_lect_obj.search(cursor, uid, search_vals):
                resultat['status'] = False
                resultat['message'] = u'Ya existe una ' \
                                      u'lectura para la fecha indicada.'
                return resultat

            search_vals = [('comptador', '=', vals['comptador']),
                           ('name', '<', vals['name']),
                           ('periode', '=', vals['periode']),
                           ('lectura', '>', vals['lectura']),
                           ]
            if fact_lect_obj.search(cursor, uid,
                search_vals,limit=1,order="name DESC"):
                resultat['status'] = False
                resultat['message'] = (
                    u"La lectura que nos pasas es inferior a la última "
                    u"facturada. Te pedimos que nos la envíes a través "
                    u"de un correo electrónico a factura@somenergia.coop "
                    u"y revisaremos el caso.")
                return resultat
            if pool_lect_obj.search(cursor, uid,
                search_vals,limit=1,order="name DESC"):
                resultat['status'] = False
                resultat['message'] = (
                    u"La lectura que nos pasas es inferior a la última "
                    u"de distribuidora. Te pedimos que nos la envíes a través "
                    u"de un correo electrónico a factura@somenergia.coop "
                    u"y revisaremos el caso.")
                return resultat

            l_fact_ids = fact_lect_obj.search(
                cursor, uid, [
                ('comptador', '=', vals['comptador']),
                ('name', '<', vals['name']),
                ('periode', '=', vals['periode']),
                ],
                limit=1,order="name DESC")
            if l_fact_ids:
                lect_vals = fact_lect_obj.browse(cursor, uid,l_fact_ids[0],['lectura'])
                if ".1" in polissa_dades['tarifa'][1]:
                    margin = 3000
                else:
                    margin = 1500
                if lect_vals['lectura'] + margin < vals['lectura']:
                    resultat['status'] = False
                    resultat['message'] = (
                        u"La lectura que nos pasas implica un consumo muy "
                        u"superior al que facturamos habitualmente. Te "
                        u"pedimos que nos la envíes a través de un correo "
                        u"electrónico a factura@somenergia.coop y "
                        u"revisaremos el caso.")
                    return resultat

            values.append(vals)

        for vals in values:
            pool_lect_obj.create(cursor, uid, vals)

        return resultat

    def www_limit_autolectura(self, cursor, uid, ids):
        if isinstance(ids, (list, set, tuple)):
            polissa_id = ids[0]
        else:
            polissa_id = ids
        polissa_obj = self.pool.get('giscedata.polissa')
        comptador_obj = self.pool.get('giscedata.lectures.comptador')

        comptador_name = polissa_obj.read(
            cursor, uid, polissa_id, ['comptador'])
        comptador_id = self._www_get_comptador_id(
            cursor, uid, polissa_id, comptador_name['comptador'])
        if not comptador_id:
            return 6
        comptador_data = comptador_obj.read(
            cursor, uid, comptador_id, ['giro'])
        if comptador_data['giro'] == 0:
            return 6
        longitud = len(str(comptador_data['giro']))-1
        return longitud

    def www_permet_autolectura(self, cursor, uid, ids):
        def singlify(l):
            return l[0] if isinstance(l, (list, set, tuple)) else l

        autoreadableFares = [
            '2.0A',
            '2.1A',
            '2.0DHA',
            '2.1DHA',
            '2.0DHS',
            '2.1DHS',
            ]

        polissa_id = singlify(ids)
        polissa_obj = self.pool.get('giscedata.polissa')
        polissa = polissa_obj.read(cursor, uid, polissa_id, [
            'no_estimable',
            'tarifa',
            'facturacio_potencia',
            ])

        if polissa['facturacio_potencia'] == 'max':
            return False

        if polissa['tarifa'][1] not in autoreadableFares:
            return False

        return not polissa['no_estimable']
    

    _columns = {
        'www_current_pagament': fields.function(_www_current_pagament,
                                        string='Pagament corrent portal',
                                        type='boolean', method=True),
    }
GiscedataPolissa()
