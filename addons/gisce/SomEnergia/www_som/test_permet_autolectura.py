#!/usr/bin/venv python2
#-*- encoding: utf-8 -*-

import unittest
import erppeek
import psycopg2

caseSelect="""\
SELECT
	p.id
FROM
	giscedata_polissa AS p
	LEFT JOIN giscedata_polissa_tarifa AS t ON p.tarifa = t.id
WHERE
	p.data_baixa IS NULL AND 
	p.no_estimable = %(no_estimable)s AND
	t.name = %(fare)s AND
    p.facturacio_potencia = %(maxicp)s AND
	true
ORDER BY p.id
LIMIT 1
"""

dbconfig = None
try:
    import dbconfig
except ImportError:
    pass

tgWithCurves='1'
tgNotAvailable='2'
tgWithoutCurves='3'

@unittest.skipIf(not dbconfig, "depends on ERP")
class MayAutoread_Test(unittest.TestCase):

    def setUp(self):
        self.maxDiff=None
        self.o = erppeek.Client(**dbconfig.erppeek)

    def getPolissa(self, polissa_id):
        return self.o.GiscedataPolissa.get(polissa_id)

    def getCase(self, fare, modeFacturacioPotencia, no_estimable):
        db = psycopg2.connect(**dbconfig.psycopg)
        with db.cursor() as cursor :
            cursor.execute(caseSelect, dict(
                fare=fare,
                maxicp=modeFacturacioPotencia,
                no_estimable=no_estimable,
                ))
            case = cursor.fetchone()
            if case is None:
                raise Exception("Test case not found")
            return self.getPolissa(case[0])

    def test_mayAutoread_whenFareNot2XEstimable_false(self):
        p = self.getCase('3.0A', 'max', False)
        self.assertEqual(False, p.www_permet_autolectura())

    def test_mayAutoread_withFare21AEstimable_true(self):
        p = self.getCase('2.1A', 'icp', False)
        self.assertEqual(True, p.www_permet_autolectura())

    def test_mayAutoread_withFare20DHAEstimable_true(self):
        p = self.getCase('2.0DHA', 'icp', False)
        self.assertEqual(True, p.www_permet_autolectura())

    def test_mayAutoread_withFare21DHAEstimable_true(self):
        p = self.getCase('2.1DHA', 'icp', False)
        self.assertEqual(True, p.www_permet_autolectura())

    def test_mayAutoread_withFare20DHSEstimable_true(self):
        p = self.getCase('2.0DHS', 'icp', False)
        self.assertEqual(True, p.www_permet_autolectura())

    def test_mayAutoread_withMaximeter_false(self):
        p = self.getCase('2.0A', 'max', False)
        self.assertEqual(False, p.www_permet_autolectura())

    def test_myautoread_withNotEstimable_false(self):
        p = self.getCase('2.0A', 'icp' , True)
        self.assertEqual(False, p.www_permet_autolectura())


# vim: et ts=4 sw=4
