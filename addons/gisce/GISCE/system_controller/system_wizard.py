# -*- coding: utf-8 -*-
try:
    # Due: https://www.python.org/dev/peps/pep-0476
    import ssl
    try:
        _create_unverified_https_context = ssl._create_unverified_context
    except AttributeError:
        # Legacy Python that doesn't verify HTTPS certificates by default
        pass
    else:
        # Handle target environment that doesn't support HTTPS verification
        ssl._create_default_https_context = _create_unverified_https_context
except ImportError:
    pass

from erppeek import Client

from fabric.tasks import execute, WrappedCallableTask
from fabric.api import local, run, cd, put, settings, abort, sudo, hide, task
from fabtools import user

from urlparse import urlparse

import ConfigParser
import StringIO

class SystemWizard:

    def __init__(self, peek_connection=None, host=None, with_user=True):
        if peek_connection:
            self.client = Client(**peek_connection)
        if host:
            url = urlparse(host, scheme='ssh')
            if not with_user:
                self.host = '{}:{}'.format(url.hostname, (url.port or 22))
            else:
                self.host = '{}@{}:{}'.format(
                    url.username, url.hostname, (url.port or 22))


    def get_installed_modules(self):
        module_obj = self.client.model('ir.module.module')
        modules_ids = module_obj.search(
            [('state', '=', 'installed')]
        )
        modules_data = module_obj.read(modules_ids, ['name', 'state', 'latest_version'])
        return modules_data

    def get_host_version(self, src_path):
        fab_get_version_task = WrappedCallableTask(fab_get_version)
        result = execute(
            fab_get_version_task,
            src_path=src_path,
            host=self.host,
        )
        return '{}'.format(result[self.host])

    def get_status_version(self, src_path):
        fab_get_status_task = WrappedCallableTask(fab_get_status)
        result = execute(
            fab_get_status_task,
            src_path=src_path,
            host=self.host,
        )
        return '{}'.format(result[self.host])

    def get_erp_config(self, config_path, filename):
        fab_get_erp_config_task = WrappedCallableTask(fab_get_erp_config)
        result = execute(
            fab_get_erp_config_task,
            config_path=config_path,
            filename=filename,
            host=self.host
        )
        config_values = result[self.host]
        buf = StringIO.StringIO(config_values)
        config = ConfigParser.ConfigParser()
        config.readfp(buf)
        return config

    def deploy_fabric_key(self, password, username='erp', key_path='~/.ssh/id_rsa.pub'):
        fab_deploy_fabric_key_task = WrappedCallableTask(
            fab_deploy_fabric_key
        )
        result = execute(
            fab_deploy_fabric_key_task,
            username=username,
            password=password,
            key_path=key_path,
            host=self.host
        )
        return True



@task
def fab_get_version(src_path):
    with cd(src_path):
        result = sudo(
            "git describe --tags",
            combine_stderr=True
        )
        return result
@task
def fab_get_status(src_path):
    with cd(src_path):
        with hide('output'):
            result = sudo(
                "git status | cat ",
                combine_stderr=True
            )
            return result

@task
def fab_get_erp_config(config_path, filename):
    with cd(config_path):
        with hide('output'):
            result = sudo(
                "cat {}".format(filename),
                combine_stderr=False,
                stdout=False
            )
            return result

@task
def fab_deploy_fabric_key(username, password, key_path='~/.ssh/id_rsa.pub'):
    with settings(user=username, password=password):
        user.add_ssh_public_key('fabric', key_path)

