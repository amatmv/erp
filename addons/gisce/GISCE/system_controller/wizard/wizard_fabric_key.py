# -*- coding: utf-8 -*-
from osv import osv, fields


class WizardFabricKey(osv.osv_memory):
    _name = 'wizard.fabric.key'


    _columns = {
        'username': fields.char(
            'Username', size=50, required=True
        ),
        'password': fields.char(
            'Password', size=50, required=True
        ),
        'message': fields.text('Message', readonly=True),
        'state': fields.selection(
            [('init', 'Init'), ('validated', 'Validated'), ('end', 'End')],
            string='State')
    }

    def deploy_fabric_key(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        wiz = self.browse(cursor, uid, ids[0])
        server_obj = self.pool.get('sys.server')
        for server_id in context.get('active_ids', []):
            server = server_obj.browse(cursor, uid, server_id)
            server.deploy_fabric_key(wiz.username, wiz.password)

WizardFabricKey()