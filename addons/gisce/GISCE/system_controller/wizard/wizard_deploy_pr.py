# -*- coding: utf-8 -*-
from osv import osv, fields

from fabric.api import env
from apply_pr.cli import apply_pr

class FabricException(Exception):
    pass

env.abort_exception = FabricException

class WizardDeployPr(osv.osv_memory):
    _name = 'wizard.deploy.pr'


    _columns = {
        'pull_request': fields.char(
            'Pull Request', size=50, required=True
        ),
        'message': fields.text('Message', readonly=True),
        'state': fields.selection(
            [('init', 'Init'), ('deploying', 'Deploying'), ('end', 'End')],
            string='State')
    }

    def deploy_pr(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        wiz = self.browse(cursor, uid, ids[0])
        server_obj = self.pool.get('sys.server')
        server_ids = context.get('active_ids', [])
        servers_status = {'done': [], 'failed': [], 'exception': []}

        for server in server_obj.read(cursor, uid, server_ids, ['url', 'name']):
            try:
                result = apply_pr(wiz.pull_request, server['url'], auto_exit=True)
                if result:
                    server_key = result.keys()[0]
                    if result[server_key]:
                        servers_status['done'].append(server['name'])
                    else:
                        servers_status['failed'].append(server['name'])
                else:
                    servers_status['failed'].append(server['name'])
            except FabricException as e:
                servers_status['exception'].append('{} - failed with exception {}'.format(
                    server['name'], e)
                )
        failed_names = ''
        success_names = ''
        except_names = ''
        if servers_status['failed']:
            failed_names = '* Failed deploys:\n {}'.format(
                '\n'.join(servers_status['failed']))
        if servers_status['done']:
            success_names = '\n * Success deploys\n{}'.format(
                '\n'.join(servers_status['done'])
            )
        if servers_status['exception']:
            except_names = '\n ******* Exceptions deploys\n{}'.format(
                '\n'.join(servers_status['exception'])
            )
        wiz.write({'message': '{}{}{}'.format(
            failed_names, success_names,except_names
        )})

WizardDeployPr()
