# -*- coding: utf-8 -*-
{
    "name": "system_controller",
    "description": """System ERP controller""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE",
    "depends": [
        "base",
        "base_extended",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "security/system_controller_security.xml",
        "security/ir.model.access.csv",
        "system_repository_view.xml",
        "system_server_view.xml",
        "wizard/wizard_fabric_key_view.xml",
        "wizard/wizard_deploy_pr_view.xml",
    ],
    "active": False,
    "installable": True
}
