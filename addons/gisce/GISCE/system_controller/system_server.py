# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _
from system_wizard import SystemWizard


class SysConfig(osv.osv):
    """Sistema per guardar configuracions tipus (clau,valor)."""

    _name = 'sys.config'

    def get(self, cursor, uid, key, default=False):
        """Retorna el valor d'una certa clau."""
        ids = self.search(cursor, uid, [('name', '=', key)])
        if not ids:
            res = default
        else:
            res = self.read(cursor, uid, ids, ['value'])[0].get('value',
                                                                 default)
        return res

    def set(self, cursor, uid, key, value):
        """Asigna un valor per una clau."""
        vals = {
          'name': key,
          'value': value
        }
        ids = self.search(cursor, uid, [('name', '=', key)])
        if not ids:
            res = self.create(cursor, uid, vals)
        else:
            res = self.write(cursor, uid, ids, vals)
        return res

    _columns = {
        'name': fields.char('Key', size=256, required=True),
        'value': fields.text('Value'),
        'description': fields.text('Description')
    }

SysConfig()

class SysServerCategory(osv.osv):
    _description = 'Server category'
    _name = 'sys.server.category'

    def name_get(self, cursor, uid, ids, context=None):
        if not len(ids):
            return []
        reads = self.read(
            cursor, uid, ids, ['name', 'parent_id'], context=context
        )
        res = []
        for record in reads:
            name = record['name']
            if record['parent_id']:
                name = record['parent_id'][1] + ' / ' + name
            res.append((record['id'], name))
        return res

    def _name_get_fnc(self, cursor, uid, ids, prop, unknow_none, context=None):
        res = self.name_get(cursor, uid, ids, context=context)
        return dict(res)

    def _check_recursion(self, cursor, uid, ids):
        level = 100
        while len(ids):
            cursor.execute('SELECT DISTINCT parent_id '
                           'FROM sys_server_category WHERE id IN %s',
                           (tuple(ids),))
            ids = filter(None, map(lambda x: x[0], cursor.fetchall()))
            if not level:
                return False
            level -= 1
        return True

    _columns = {
        'name': fields.char(
            'Category name', required=True, size=64, translate=True),
        'parent_id': fields.many2one(
            'sys.server.category', 'Parent category', select=True),
        'complete_name': fields.function(
            _name_get_fnc, method=True, type="char", string='Complete Name'),
        'child_ids': fields.one2many(
            'sys.server.category', 'parent_id', 'Child categories'),
        'active': fields.boolean('Active'),
        'code': fields.char('Code', size=64),
    }
    _constraints = [
        (_check_recursion,
         _(u'Error ! No pots crear categories recursives.'),
         ['parent_id'])
    ]
    _defaults = {
        'active': lambda *a: 1,
    }
    _order = 'parent_id,name'

SysServerCategory()


class SysServer(osv.osv):

    _name = 'sys.server'
    _description = 'ERP Server'

    def get_system_connection_method(self, cursor, uid, context=None):
        if context is None:
            context = {}
        return [
            ('ssh', 'SSH'),
        ]

    def get_peek_connection_method(self, cursor, uid, context=None):
        if context is None:
            context = {}
        return [
            ('http', 'HTTP'),
            ('https', 'HTTPS'),
        ]

    def get_peek_connection(self, cursor, uid, server_id):
        if isinstance(server_id, (tuple, list)):
            server_id = server_id[0]
        fields_to_read = [
            'peek_user',
            'peek_password',
            'peek_port',
            'peek_connection',
            'peek_hostname'
        ]
        peek_values = self.read(cursor, uid, server_id, fields_to_read)
        url = '{con}://{host}:{port}'.format(
            con=peek_values['peek_connection'],
            host=peek_values['peek_hostname'],
            port=peek_values['peek_port']
        )
        set_obj = self.pool.get('sys.erpserver.setting')
        querier = set_obj.q(cursor, uid)
        db_name_query = querier.select(
            ['value']
        ).where(
            [('server_id', '=', server_id), ('name', '=', 'db_name')]
        )
        cursor.execute(*db_name_query)
        result = cursor.dictfetchall()
        if result:
            db_name = result[0]['value']
        else:
            db_name = False
        conn = {'user': peek_values['peek_user'], 'password': peek_values['peek_password'], 'server': url, 'db': db_name}
        return conn

    def get_fabric_connection(self, cursor, uid, server_id):
        if isinstance(server_id, (tuple, list)):
            server_id = server_id[0]
        server_data = self.read(cursor, uid, server_id, [])
        return server_data['url']

    def get_system_connection_url(
            self, cursor, uid, ids, field_name, args, context=None):
        if context is None:
            context = {}
        res = dict.fromkeys(ids, '')
        for server in self.read(
                cursor, uid, ids, ['hostname', 'user', 'method']):
            res[server['id']] = '{method}://{user}@{hostname}'.format(**server)
        return res

    def get_installed_modules(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        modules_installe_obj = self.pool.get('sys.installed.module')
        for server_id in ids:
            peek_connection = self.get_peek_connection(
                cursor, uid, server_id)
            server = SystemWizard(peek_connection)
            modules_list = server.get_installed_modules()
            for module in modules_list:
                modules_installe_obj.update_module(
                    cursor, uid, server_id,
                    module['name'], module['state'], module['latest_version']
                )

    def button_get_server_info(self, cursor, uid, ids, context=None):
        self.get_server_info(cursor, uid, ids, context=context)

    def get_server_info(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        setting_obj = self.pool.get('sys.erpserver.setting')
        for server_data in self.read(cursor, uid, ids, []):
            server_id = server_data['id']
            server = SystemWizard(host=server_data['url'])
            erp_src = '{}/{}'.format(
                    server_data['source_path'],
                server_data['erp_source_folder'])
            version = server.get_host_version(
                src_path=erp_src
            )
            vals = {'version': version}
            status = server.get_status_version(
                src_path=erp_src)
            vals['git_status'] = status
            self.write(cursor, uid, server_id, vals)
            erp_config = server.get_erp_config(
                server_data['settings_path'],
                server_data['settings_filename']
            )
            setting_obj.clean(
                cursor, uid, server_id
            )
            for conf in erp_config.items('options'):
                setting_obj.upsert(
                    cursor, uid, server_id, conf[0], conf[1]
                )

            return vals

    def deploy_fabric_key(
            self, cursor, uid, ids, username, password,
            key_path='/home/erp/.ssh/id_rsa.pub', context=None):
        if context is None:
            context = {}

        for server_id in ids:
            host = self.get_fabric_connection(cursor, uid, server_id)
            server = SystemWizard(host=host, with_user=False)
            server.deploy_fabric_key(
                username=username,
                password=password,
                key_path=key_path
            )



    def _trg_recalcular_url(self, cursor, uid, ids, context=None):
        return ids

    _trg_connection_url_ = {
        'sys.server': (
            _trg_recalcular_url, ['hostname', 'user', 'method'], 10)
    }

    _columns = {
        'name': fields.char('Server name', size=250, required=True, select=1),
        'owner_id': fields.many2one('res.partner', string='Owner'),
        # Connection
        'hostname': fields.char('Hostname', size=250, required=True, select=1),
        'user': fields.char('User', size=50, required=True, select=1),
        'method': fields.selection(
            get_system_connection_method, 'Connection Method', required=True),
        'url': fields.function(
            get_system_connection_url, string='Connection URL', type='char',
            size=250, method=True, readonly=True, store=_trg_connection_url_),
        # Information
        'crontab': fields.text('Crontab'),
        'version': fields.char('Version', size=100, select=1),
        'git_status': fields.text('git status'),
        'repository_ids': fields.one2many('sys.repository', 'server_id', string='Repositories'),
        'pip_ids': fields.one2many('sys.pip.requirement', 'server_id', string='Pip requirements'),
        'supervisor_conf': fields.one2many('sys.supervisor', 'server_id', string='Supervisor'),
        'settings_ids': fields.one2many('sys.erpserver.setting', 'server_id', string='ERP Settings'),
        'modules_ids': fields.one2many(
            'sys.installed.module',
            'server_id',
            string='Installed modules',
            ),
        # Configuration
        'settings_filename': fields.char('Settings filename', size=100, required=True, select=1),
        'settings_path': fields.char('Settings path', size=512, required=True, select=1),
        'source_path': fields.char('Source path', size=512, required=True, select=1),
        'erp_source_folder': fields.char('ERP Source folder', size=512, required=True,
                                   select=1),
        'venv_path': fields.char('Virtualenv path', size=512, required=True, select=1),
        # Categories
        'category_id': fields.many2many('sys.server.category',
                                        'sys_server_category_rel',
                                        'server_id',
                                        'category_id',
                                        'Categories', select=True),
        #PEEK connection
        'peek_user': fields.char('Peek user', size=64, select=1),
        'peek_hostname': fields.char('Peek hostname', size=256, select=1),
        'peek_password': fields.char('Peek password', size=64, select=1),
        'peek_connection': fields.selection(
            get_peek_connection_method, 'Connection Method'),
        'peek_port': fields.integer('Port connection'),

    }
    _defaults = {
        'user': lambda *a: 'fabric',
        'venv_path': lambda *a: '/home/erp',
        'source_path': lambda *a: '/home/erp/src',
        'erp_source_folder': lambda *a: 'erp',
        'settings_path': lambda *a: '/home/erp/conf',
        'settings_filename': lambda *a: 'erp.conf',
    }

SysServer()


class SysErpServerSetting(osv.osv):

    _name = 'sys.erpserver.setting'
    _description = 'ERP setting'
    _inherit = 'sys.config'

    def upsert(
            self, cursor, uid,
            server_id, config_name, config_value, description='', context=None
    ):
        if context is None:
            context = {}
        vals = {
            'name': config_name,
            'value': config_value,
            'server_id': server_id
        }
        if description:
            vals['description'] = description
        setting_id = self.search(
            cursor, uid, [('server_id', '=', server_id),
                          ('name', '=', config_name)]
        )
        if setting_id:
            vals
            self.write(
                cursor, uid, setting_id[0], vals
            )
        else:
            setting_id = self.create(cursor, uid, vals)
        return setting_id

    def clean(self, cursor, uid, server_id, context=None):
        if context is None:
            context = {}
        setting_ids =  self.search(
            cursor, uid, [('server_id', '=', server_id)])
        if setting_ids:
            self.unlink(cursor, uid, setting_ids)

    _columns = {
        'server_id': fields.many2one('sys.server', string='ERP Server'),
    }

    _sql_constraints = [
        ('name_server_uniq', 'unique (name, server_id)',
         'The key must be unique !')
    ]

SysErpServerSetting()


class SysPipRequirement(osv.osv):

    _name = 'sys.pip.requirement'
    _description = 'PIP requirement'

    _columns = {
        'name': fields.char('Pip name', size=100, required=True, select=1),
        'version': fields.text('Version', select=1),
        'server_id': fields.many2one('sys.server', string='ERP Server'),
    }

SysPipRequirement()



class SysModule(osv.osv):
    '''ERP Module'''
    _name = 'sys.module'
    _description = 'ERP Module'

    _columns = {
        'name': fields.char('Module name', size=512, required=True, select=1)
    }

    _sql_constraints = [
        (
            'name_uniq',
            'unique (name)',
            _(u'Module name is unique')
        )
    ]

    def get_module(self, cursor, uid, module_name, context=None):
        if context is None:
            context = {}
        module_id = self.search(cursor, uid, [('name', '=', module_name)])
        if module_id:
            return module_id[0]
        else:
            return self.create(cursor, uid, {'name': module_name})

SysModule()


class SysModuleInstalled(osv.osv):

    _name = 'sys.installed.module'
    _description = 'ERP Installed module'

    def update_module(self, cursor, uid, server_id, module_name, state, version):
        module_obj = self.pool.get('sys.module')
        module_id = module_obj.get_module(cursor, uid, module_name)
        installe_module_id = self.search(
            cursor, uid, [
                ('module_id', '=', module_id),
                ('server_id', '=', server_id)
            ]
        )
        vals = {
            'state': state,
            'name': version
        }
        if installe_module_id:
            self.write(cursor, uid, installe_module_id[0], vals)
        else:
            vals.update({
                'module_id': module_id,
                'server_id': server_id
            })
            self.create(cursor, uid, vals)

    _columns = {
        'module_id': fields.many2one('sys.module', string='Installed module', ondelete='restrict'),
        'server_id': fields.many2one('sys.server', string='ERP Server'),
        'state': fields.selection([
            ('uninstallable', 'Not Installable'),
            ('uninstalled', 'Not Installed'),
            ('installed', 'Installed'),
            ('to upgrade', 'To be upgraded'),
            ('to remove', 'To be removed'),
            ('to install', 'To be installed')
        ], string='State', readonly=True),
        'name': fields.char('Installed version', size=64, readonly=True),
    }

SysModuleInstalled()


