# -*- coding: utf-8 -*-
from osv import osv, fields


class SysSupervisor(osv.osv):

    _name = 'sys.supervisor'
    _description = 'Supervisor'

    _columns = {
        'name': fields.char('Process name', size=250, required=True, select=1),
        'description': fields.text('Description process'),
        'server_id': fields.many2one('sys.server', string='ERP server'),
        'conf_path': fields.char('Config file path', size=512, required=True, select=1),
        'config_ids': fields.one2many(
            'sys.supervisor.config', 'process_id', string='Configurations'),
    }

SysSupervisor()


class SysSupervisorConfig(osv.osv):

    _name = 'sys.supervisor.config'
    _inherit = 'sys.config'
    _description = 'Supervisor config'

    _columns = {
        'process_id': fields.many2one('sys.supervisor', string='Process'),
    }

    _sql_constraints = [
        ('name_process_uniq', 'unique (name, process_id)',
         'The key must be unique !')
    ]

SysSupervisorConfig()

