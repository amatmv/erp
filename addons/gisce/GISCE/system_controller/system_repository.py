# -*- coding: utf-8 -*-
from osv import osv, fields


class SysRepository(osv.osv):

    _name = 'sys.repository'
    _description = 'Repository'


    _columns = {
        'name': fields.char('Repository', size=512, required=True, select=1),
        'server_id': fields.many2one('sys.server', string='ERP server'),
        'branch': fields.char('Branch', size=512, required=True, select=1),
        'version': fields.char('Version', size=50, select=1),
        'log': fields.text('Log'),
        'deployment_ids': fields.one2many(
            'sys.deployment', 'repository_id', 'Deployments'
        )
    }

SysRepository()


DEPLOY_STATE = [
    ('scheduled', 'to be applied'),
    ('pending', 'applying'),
    ('success', 'applied'),
    ('error', 'not applied')
]

class SysDeployment(osv.osv):

    _name = 'sys.deployment'
    _description = 'Deployment'

    _columns = {
        'name': fields.char('PR', size=50, required=True, select=1),
        'repository_id': fields.many2one(
            'sys.repository', string='Repository',required=True, select=1),
        'state': fields.selection(DEPLOY_STATE, 'State', required=True, select=1),
        'log': fields.text('Log')
    }

SysDeployment()




