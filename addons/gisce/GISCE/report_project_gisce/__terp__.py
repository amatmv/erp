# -*- coding: utf-8 -*-
{
    "name": "Project Management - Reporting - GISCE",
    "description": """A module that adds some reports to project module ( GISCE customized )""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Generic Modules/Project",
    "depends":[
        "project"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "report_project_gisce_view.xml",
        "report_project_gisce_report.xml"
    ],
    "active": False,
    "installable": True
}
