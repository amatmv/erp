from osv import fields, osv

class report_hours_user(osv.osv):
  _name = "report.hours.user"
  _description = "Hours by user"
  _auto = False
  _columns = {
    'name': fields.date('Month', readonly=True),
    'user_id':fields.many2one('res.users', 'User', readonly=True, relate=True),
    'hours':fields.float('Hours', readonly=True),
  }
  _defaults = {

  }
  _order = 'name desc, user_id'
  
  def init(self,cr):
    cr.execute("""
      create or replace view report_hours_user as (
                select
                    min(project_task_work.id) as id,
                    to_char(project_task_work.create_date, 'YYYY-MM') || '-01' as name,
                    project_task_work.user_id,
                    sum(project_task_work.hours) as hours
                from project_task
                  left join project_task_work on (project_task.id=project_task_work.task_id) 
                group by
                    to_char(project_task_work.create_date, 'YYYY-MM'),
                    project_task_work.user_id
            )  
    """)
report_hours_user()

class report_hours_alltask(osv.osv):
  _name = "report.hours.alltask"
  _description = "Hours by All Tasks"
  _auto = False
  _columns = {
    'name': fields.date('Month', readonly=True),
    'project_id': fields.many2one('project.project', 'Project', readonly=True, relate=True),
    'task_id': fields.many2one('project.task', 'Task', readonly=True, relate=True),
    'hours': fields.float('Hours', readonly=True),
  }

  _defaults = {

  }

  _order = 'name desc, project_id, task_id'

  def init(self,cr):
    cr.execute("""
      create or replace view report_hours_alltask as (
        select
          min(project_task_work.id) as id,
          to_char(project_task_work.create_date, 'YYYY-MM') || '-01' as name,
          project_task.project_id,
          project_task_work.task_id,
          sum(project_task_work.hours) as hours
        from project_task
          left join project_task_work on (project_task.id=project_task_work.task_id)
          left join project_project on (project_task.project_id=project_project.id)
        group by
          to_char(project_task_work.create_date, 'YYYY-MM'),
          project_task_work.task_id,
          project_task.project_id
      )
    """)
report_hours_alltask()

class report_hours_allproject(osv.osv):
  _name = "report.hours.allproject"
  _description = "Hours by All Projects"
  _auto = False
  _columns = {
    'name': fields.date('Month', readonly=True),
    'project_id': fields.many2one('project.project', 'Project', readonly=True, relate=True),
    'hours': fields.float('Hours', readonly=True),
  }

  _defaults = {

  }

  _order = 'name desc, project_id'

  def init(self,cr):
    cr.execute("""
      create or replace view report_hours_allproject as (
        select
          min(project_task_work.id) as id,
          to_char(project_task_work.create_date, 'YYYY-MM') || '-01' as name,
          project_task.project_id,
          sum(project_task_work.hours) as hours
        from project_task
          left join project_task_work on (project_task.id=project_task_work.task_id)
          left join project_project on (project_task.project_id=project_project.id)
        group by
          to_char(project_task_work.create_date, 'YYYY-MM'),
          project_task.project_id
      )
    """)
report_hours_allproject()

class report_workdones(osv.osv):
  _name = "report.workdones"
  _description = "All Workdones"
  _auto = False
  _columns = {
    'name': fields.char('Task summary', size=128, readonly=True),
    'project_id': fields.many2one('project.project', 'Project', readonly=True, relate=True),
		'task_id': fields.many2one('project.task', 'Task', readonly=True),
		'user_id': fields.many2one('res.users', 'Done by', readonly=True),
		'date': fields.datetime('Date', readonly=True),
		'task_description': fields.text('Description', readonly=True),
    'hours': fields.float('Hours spent', readonly=True),
  }

  _order = 'date asc'

  def init(self, cr):
    cr.execute("""
      create or replace view report_workdones as (
        select 
          w.id,t.project_id, w.task_id,
          w.user_id,w.name,w.date,w.hours,
          t.description as task_description
        from project_task_work w, project_task t 
        where w.task_id = t.id
      )""")
report_workdones()
