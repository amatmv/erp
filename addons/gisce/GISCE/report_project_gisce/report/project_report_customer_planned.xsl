<?xml version="1.0" encoding="iso-8859-15"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:template match="/">
		<xsl:apply-templates select="projects"/>
	</xsl:template>

	<xsl:template match="projects">
		<document xmlns:fo="http://www.w3.org/1999/XSL/Format">
			<template leftMargin="1.0cm" rightMargin="1.0cm" topMargin="1.0cm" bottomMargin="1.0cm" title="Project Description" author="Generated by GISCE-ERP" allowSplitting="20">
				<pageTemplate id="all">
					<pageGraphics/>
					<frame id="list" x1="1.0cm" y1="1.0cm" width="19.0cm" height="27.7cm"/>
				</pageTemplate>
			</template>

			<stylesheet>
				<paraStyle name="title" fontName="Helvetica-Bold" fontSize="18" alignment="center"/>
				<paraStyle name="notes" fontName="Helvetica" fontSize="8" alignment="justify"/>
				<blockTableStyle id="project">
					<blockFont name="Helvetica" size="9" />
					<blockFont name="Helvetica-Bold" size="9" start="0,0" stop="0,-1"/>
					<blockFont name="Helvetica-Bold" size="9" start="2,0" stop="2,-1"/>
					 <blockValign value="TOP"/>
				</blockTableStyle>
				<blockTableStyle id="tasks">
					<blockFont name="Helvetica" size="9" />
					<blockValign value="TOP"/>
					<blockAlignment value="LEFT"/>
					<blockFont name="Helvetica-Bold" size="9" start="0,0" stop="-1,0"/>
					<lineStyle kind="LINEABOVE" thickness="0.5" colorName="black" start="0,0" stop="-1,0"/>
					<lineStyle kind="LINEBELOW" thickness="0.5" colorName="black" start="0,0" stop="-1,0"/>
					<blockBackground colorName="(0.72,0.72,0.72)" start="0,0" stop="-1,0"/>
					 <blockValign value="TOP"/>
					 <blockAlignment value="CENTER" start="1,0" stop="-1,-1"/>
				</blockTableStyle>
			</stylesheet>

			<story>
				<xsl:apply-templates select="project"/>
			</story>
		</document>
	</xsl:template>

	<xsl:template match="task">
    <tr>
      <td>
        <para><xsl:value-of select="task_sequence"/></para>
      </td>
			<td>
				<para><xsl:value-of select="task_name"/></para>
				<para style="notes"><xsl:value-of select="task_description"/></para>
			</td>
			<td><xsl:value-of select="task_planned_hours"/></td>
		</tr>
	</xsl:template>

	<xsl:template match="tasks">
		<blockTable style="tasks" colWidths="2cm,15cm,2cm">
		<tr>
      <td>Ordre</td>
			<td>Descripció</td>
			<td>Hores</td>
		</tr>
		<xsl:apply-templates select="task">
		  <xsl:sort select="task_sequence" data-type="number"/>
    </xsl:apply-templates>
		</blockTable>
	</xsl:template>

	<xsl:template match="project">
		<para style="title">
			<xsl:value-of select="name"/>
		</para>
		<spacer length="1cm"/>
		<blockTable colWidths="4cm,6cm,3cm,6cm" style="project">
		<tr>
			<td>Empresa / Projecte:</td>
			<td><xsl:value-of select="parent"/></td>
			<td></td>
			<td></td>
		</tr><tr>
			<td>Data Inici:</td>
			<td><xsl:value-of select="date_start"/></td>
			<td>Data Final:</td>
			<td><xsl:value-of select="date_stop"/></td>
		</tr><tr>
			<td>Total Hores:</td>
			<td><xsl:value-of select="planned_hours"/></td>
		  <td></td>
      <td></td>
    </tr><tr>
			<td>Nom del projecte:</td>
			<td><xsl:value-of select="name"/></td>
		  <td></td>
      <td></td>
    </tr>
		</blockTable>
		<spacer length="0.3cm"/>
		<pre>
			<xsl:value-of select="notes"/>
		</pre>
		<spacer length="0.7cm"/>
		<xsl:apply-templates select="tasks"/>
		<setNextTemplate name="all" />
	</xsl:template>
</xsl:stylesheet>
