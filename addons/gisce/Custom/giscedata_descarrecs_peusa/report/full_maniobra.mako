<%inherit file="/giscedata_descarrecs/report/full_maniobra.mako"/>
<%block name="custom_data1">
##     <span class="inbox_left">
##         ${_("ANNEX-3")}
##     </span>
##     <span class="inbox_right">
##         ${_("Document PGO-1")}
##     </span>
</%block>
<%block name="custom_data_box2">
##     <span class="inbox_right">
##         ${_("Revisio: 0")}
##     </span>
</%block>
<%block name="custom_data_box3">
    %for descarrec in objects:
        %if (descarrec.observacions_obertura and action == "treure") or (descarrec.observacions_tancament and action == "donar"):
            <div class="maniobres_observacions_container_peusa">
                <table>
                    <tr>
                        <th>${_("Observacions")}</th>
                    </tr>
                    %if action == "treure":
                        <tr>
                            <td class="nocount left_text"><div class="maniobres_observacions">${descarrec.observacions_obertura or ''}</div></td>
                        </tr>
                    %elif action == "donar":
                        <tr>
                            <td class="nocount left_text"><div class="maniobres_observacions">${descarrec.observacions_tancament or ''}</div></td>
                        </tr>
                    %endif
                </table>
            </div>
        %endif
    %endfor
    <div class="container_avisos">
        <div id="warning">
            <strong>${_("ATENCIÓ!")}</strong>
        </div>
        %if action == "treure":
            <div>
                ${_("-Per entrar en un Centre subterrani, prèviament s'ha de comprovar la qualitat de l'aire mitjançant el detector de gasos. Complementar el document normalitzat amb referència ALD-08.")}
            </div>
        %elif action == "donar":
            <div>
                ${_("-Per entrar en un Centre subterrani, prèviament s'ha de comprovar la qualitat de l'aire mitjançant el detector de gasos. Complementar el document normalitzat amb referència ALD-08.")}
            </div>
        %endif
        <br>
    </div>
</%block>
