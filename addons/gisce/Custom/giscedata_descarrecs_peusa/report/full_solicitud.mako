<%inherit file="/giscedata_descarrecs/report/full_solicitud.mako"/>
<%block name="custom_data1">
##     <span class="full_desc_inbox_left_top">
##         ${_("ANNEX-1")}
##     </span>
##     <span class="full_desc_inbox_right_top">
##         ${_("Document PGO-01")}
##     </span>
</%block>
<%block name="custom_data_box2">
##     <span class="full_desc_inbox_right_bot">
##         ${_("Revisió: 0")}
##     </span>
</%block>
<%block name="custom_data3">
    <p style="page-break-after:always"></p>
    <br>
    <div class="full_sol_desc_peusa_text_container">
        <div class="full_sol_desc_peusa_text_title">
            ${_("Entrega de la zona protegida del descàrrec: ")}
        </div>
        <div class="full_sol_desc_peusa_text_content">
            <ul>
                <li>
                    ${_("L\'AGENT DE DESCÀRREC ELÈCTRIC posa en coneixement del CAP DE TREBALL  que la instal·lació corresponent a la "+
                    "petició de descàrrec elèctric a dalt indicada es troba en situació de ZONA PROTEGIDA, "+
                    "amb les posades a terra i en curt circuit que defineixen dita zona.")}
                </li>
                <li>
                    ${_("EL CAP DE TREBALL accepta la  ZONA PROTEGIDA un cop comprovada l'absència de tensió i confirma ser "+
                    "coneixedor dels límits que la defineixen. Es compromet a crear la ZONA DE TREBALL.")}
                </li>
                <li>
                    ${_("Coneix els treballs a realitzar i les \"PRESCRIPCIONS DE SEGURETAT\" per a treballs i maniobres.")}
                </li>
                <li>
                    ${_("No iniciarà ni realitzarà els treballs si en alguna part del procés no pot complir les \"PRESCRIPCIONS DE SEGURETAT\"")}
                </li>
            </ul>
        </div>
    </div>
    <p class="float_clear"></p>
    <div class="full_sol_desc_peusa_check_text_container">
        <div class="full_sol_desc_peusa_check_container">
            <input type="checkbox" class="full_sol_desc_peusa_check_text_check"/>
        </div>
        <div class="full_sol_desc_peusa_check_text_content">
            <span class="full_sol_desc_peusa_check_text_content_1">${_("Els terres de \"Zona Protegida\" també faran la funció de \"Terres de Treball\"")}</span>
            <span class="full_sol_desc_peusa_check_text_content_2">${_("(marcar en cas afirmatiu)")}</span>
        </div>
    </div>
    <p class="float_clear"><br></p>
    <div class="full_sol_desc_peusa_text_container">
        <div class="full_sol_desc_peusa_text_title">
            ${_("Devolució de la zona protegida del descàrrec: ")}
        </div>
        <div class="full_sol_desc_peusa_text_content">
            <ul>
                <li>
                   ${_("El CAP DE TREBALL  posa en coneixement de l\'AGENT DE DESCÀRREC ELÈCTRIC haver finalitzat els treballs relatius al "+
                   "present descàrrec elèctric i retirat el seu personal, així com els dispositius de seguretat que ha col·locat exclusivament "+
                   "per a la creació de la seva ZONA DE TREBALL.")}
                </li>
            </ul>
        </div>
    </div>
    <p class="float_clear"></p>
    %for descarrec in objects:
        <div class="sol_desc_custom_table_container">
            <table class="sol_desc_custom_table">
                <tr>
                  <th colspan="4">${_("ENTREGA ZONA SENSE TENSIÓ")}</th>
                </tr>
                <tr>
                  <th class="sol_desc_custom_table_header" colspan="2">${_("L'AGENT DE DESCÀRREC")}</th>
                  <th class="sol_desc_custom_table_header" colspan="2">${_("EL CAP DE TREBALL")}</th>
                </tr>
                <tr>
                  <td class="sol_desc_custom_table_data">${_("Nom: {}").format(descarrec.agent_descarrec.name or "")}</td>
                  <td class="sol_desc_custom_table_data_firma" rowspan="3">${_("Firma:")}</td>
                  <td class="sol_desc_custom_table_data">${_("Nom: {}").format(descarrec.cap_de_treball.name or "")}</td>
                  <td class="sol_desc_custom_table_data_firma" rowspan="3">${_("Firma:")}</td>
                </tr>
                <tr>
                  <td class="sol_desc_custom_table_data">${_("Data: ")}</td>
                  <td class="sol_desc_custom_table_data">${_("Data: ")}</td>
                </tr>
                <tr>
                  <td class="sol_desc_custom_table_data">${_("Hora: ")}</td>
                  <td class="sol_desc_custom_table_data">${_("Hora: ")}</td>
                </tr>
            </table>
        </div>
        <p class="float_clear"></p>
        <div class="sol_desc_custom_table_container">
            <table class="sol_desc_custom_table">
                <tr>
                  <th colspan="4">${_("DEVOLUCIÓ ZONA SENSE TENSIÓ")}</th>
                </tr>
                <tr>
                  <th class="sol_desc_custom_table_header" colspan="2">${_("L'AGENT DE DESCÀRREC")}</th>
                  <th class="sol_desc_custom_table_header" colspan="2">${_("EL CAP DE TREBALL")}</th>
                </tr>
                <tr>
                  <td class="sol_desc_custom_table_data">${_("Nom: {}").format(descarrec.agent_descarrec.name or "")}</td>
                  <td class="sol_desc_custom_table_data_firma" rowspan="3">${_("Firma:")}</td>
                  <td class="sol_desc_custom_table_data">${_("Nom: {}").format(descarrec.cap_de_treball.name or "")}</td>
                  <td class="sol_desc_custom_table_data_firma" rowspan="3">${_("Firma:")}</td>
                </tr>
                <tr>
                  <td class="sol_desc_custom_table_data">${_("Data: ")}</td>
                  <td class="sol_desc_custom_table_data">${_("Data: ")}</td>
                </tr>
                <tr>
                  <td class="sol_desc_custom_table_data">${_("Hora: ")}</td>
                  <td class="sol_desc_custom_table_data">${_("Hora: ")}</td>
                </tr>
            </table>
        </div>
        <p class="float_clear"></p>
    %endfor
    <div class="regles_or_containter">
        <div id="warning">
            <strong>${_("CAL COMPLIR SEMPRE LES 5 REGLES D'OR")}</strong>
        </div>
        <img class="regles_or" src="${addons_path}/giscedata_descarrecs_peusa/report/img/5_regles_d_or_v3.jpg"/>
    </div>
</%block>
