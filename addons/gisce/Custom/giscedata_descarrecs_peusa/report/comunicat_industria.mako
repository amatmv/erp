<%inherit file="/giscedata_descarrecs/report/comunicat_industria.mako"/>
<%block name="custom_missatge">
    ${_("Senyors")},<br/>
    ${data["form"]["p_paragraf"]}
</%block>

<%block name="custom_area">
    %if data["form"]["num_municipis"] > 1:
        Un sector dels municipis de ${", ".join(data["form"]["municipis_afectats"])}
    %else:
        Un sector del municipi de ${", ".join(data["form"]["municipis_afectats"])}
    %endif
</%block>

<%block name="custom_descripcio">
    %if len(data["form"]["municipis_afectats"])>1:
        ${_("Treballs a la xarxa de distribució elèctrica dels municipis de")} ${", ".join(data["form"]["municipis_afectats"]) or ''}
    %else:
        ${_("Treballs a la xarxa de distribució elèctrica del municipi de")} ${", ".join(data["form"]["municipis_afectats"]) or ''}
    %endif
</%block>
