# -*- coding: utf-8 -*-
{
    "name": "giscedata_descarrecs",
    "description": """GISCE Descarrecs per PEUSA""",
    "version": "0-dev",
    "author": "GISCE Enginyeria, SL",
    "category": "Descarrecs",
    "depends":[
        "base",
        "giscedata_descarrecs"
    ],
    "init_xml":[
        "giscedata_descarrecs_peusa_report.xml",
        "giscedata_descarrecs_peusa_wizard.xml"
    ],
    "demo_xml": [],
    "update_xml":[
        "giscedata_descarrecs_peusa_report.xml"
    ],
    "active": False,
    "installable": True
}
