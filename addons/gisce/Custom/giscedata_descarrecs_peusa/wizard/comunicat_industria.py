# -*- coding: utf-8 -*-
import wizard
import pooler
from tools.translate import _
from datetime import datetime


def get_municipis_afectats(cursor, uid, ids, context=None):
    """
    Gets the list of municipi

    :param cursor: Database cursror
    :param uid: User id
    :param ids: Ids of the descarrecs
    :param context: OpenERP context
    :return: List of affected municipis
    :rtype: list of str
    """

    pool = pooler.get_pool(cursor.dbname)
    descarrec_obj = pool.get("giscedata.descarrecs.descarrec")
    inst_obj = pool.get("giscedata.descarrecs.descarrec.installacions")
    cli_obj = pool.get("giscedata.descarrecs.descarrec.clients")
    cts_obj = pool.get("giscedata.cts")

    lst_inst = descarrec_obj.read(
        cursor, uid, ids[0], ["installacions"])["installacions"]
    lst_clients = descarrec_obj.read(
        cursor, uid, ids[0], ["clients"])["clients"]
    ret = []
    if lst_inst:
        for inst_id in lst_inst:
            nom_ct = inst_obj.read(cursor, uid, inst_id, ["code_ct"])
            search_param = [("name", "=", nom_ct["code_ct"])]
            id_ct = cts_obj.search(cursor, uid, search_param)
            ct = cts_obj.read(cursor, uid, id_ct, ["id_municipi"])
            municipi = " ".join(ct[0]["id_municipi"][1].split(",")[::-1])
            ret.append(municipi)
    else:
        for client in lst_clients:
            nom_ct = cli_obj.read(cursor,uid, client, ["code_ct"])
            search_param = [("name", "=", nom_ct["code_ct"])]
            id_ct = cts_obj.search(cursor, uid, search_param)
            ct = cts_obj.read(cursor, uid, id_ct, ["id_municipi"])
            municipi = " ".join(ct[0]["id_municipi"][1].split(",")[::-1])
            ret.append(municipi)
    return list(set(ret))


def get_adreces(cursor, uid, ids, context=None):
    """
    Gets the list of adreces

    :param cursor: Database cursror
    :param uid: User id
    :type uid: int
    :param ids: Ids of the descarrecs
    :type ids: list of int
    :param context: OpenERP context
    :type context: dict
    :return: List of affected municipis
    :rtype: list of str
    """

    pool = pooler.get_pool(cursor.dbname)
    descarrec_obj = pool.get("giscedata.descarrecs.descarrec")
    inst_obj = pool.get("giscedata.descarrecs.descarrec.installacions")
    cli_obj = pool.get("giscedata.descarrecs.descarrec.clients")
    cts_obj = pool.get("giscedata.cts")

    lst_inst = descarrec_obj.read(
        cursor, uid, ids[0], ["installacions"])["installacions"]
    lst_clients = descarrec_obj.read(
        cursor, uid, ids[0], ["clients"])["clients"]
    ret = []
    if lst_inst:
        for inst_id in lst_inst:
            nom_ct = inst_obj.read(cursor, uid, inst_id, ["code_ct"])
            search_param = [("name", "=", nom_ct["code_ct"])]
            id_ct = cts_obj.search(cursor, uid, search_param)
            ct = cts_obj.read(cursor, uid, id_ct, ["adreca"])
            if ct[0]["adreca"]:
                municipi = ct[0]["adreca"]
                ret.append(municipi)
    else:
        for client in lst_clients:
            nom_ct = cli_obj.read(cursor,uid, client, ["code_ct"])
            search_param = [("name", "=", nom_ct["code_ct"])]
            id_ct = cts_obj.search(cursor, uid, search_param)
            ct = cts_obj.read(cursor, uid, id_ct, ["adreca"])
            if ct[0]["adreca"]:
                municipi = ct[0]["adreca"]
                ret.append(municipi)
    return list(set(ret))


def _init(self, cr, uid, data, context=None):
    """
    Carreguem el text que ja està guardat a la base de dades

    :param self:
    :param cr: Database cursor
    :param uid: User id
    :type uid: int
    :param data: Data of the form
    :type data: dict
    :param context: OpenERP context
    :type context: dict
    :return: Data to fill the report
    :rtype: dict
    """
    if context is None:
        context = {}
    descarrec_obj = pooler.get_pool(cr.dbname).get('giscedata.descarrecs.descarrec')
    descarrec = descarrec_obj.browse(cr, uid, data['id'], context)
    causes = descarrec.causes
    if not causes:
        causes = descarrec.descripcio

    municipis_afectats = get_municipis_afectats(cr, uid, [data["id"]], context)
    adreces = get_adreces(cr, uid, [data["id"]], context)
    xarxa = descarrec.xarxa
    linies_bt = {}
    for client in descarrec.clients:
        linies_actuals = linies_bt.get(client["code_ct"], [])
        linies_actuals.append(client["line"])
        linies_bt[client["code_ct"]] = sorted(list(set(linies_actuals)))


    if descarrec.sollicitud.tramitacio == "U":
        p_paragraf = _("D’acord amb el que disposa l’Article 101.3 del RD 1955/2000, us comuniquem les interrupcions programades molt urgents, següents:")
    else:
        p_paragraf = _("D’acord amb el que disposa l’Article 101.3 del RD 1955/2000, us comuniquem les interrupcions programades següents:")
    ret = {
        'nuclis_afectats': descarrec.nuclis_afectats,
        'causes': causes,
        'cts_afectats': descarrec.cts_afectats.replace(",", ", "),
        'poblacions': ", ".join(municipis_afectats),
        'adreces': ", ".join(sorted(adreces)),
        "clients": len(descarrec.clients),
        "num_descarrec": descarrec.name,
        "tramitacio": descarrec.sollicitud.tramitacio,
        "num_municipis": len(municipis_afectats),
        "municipis_afectats": municipis_afectats,
        'peu_pagina': " ",
        "p_paragraf": p_paragraf,
        "xarxa": xarxa,
        "linies_bt": linies_bt,
    }

    if descarrec.data_inici:
        ret["inici"] = descarrec.data_inici.split(" ")
        ret["inici"][1] = datetime.strptime(ret["inici"][1], "%H:%M:%S").strftime('%H:%M')

    else:
        ret["inici"] = descarrec.data_inici

    if descarrec.data_final:
        ret["fi"] = descarrec.data_final.split(" ")
        ret["fi"][1] = datetime.strptime(ret["fi"][1], "%H:%M:%S").strftime('%H:%M')
    else:
        ret["fi"] = descarrec.data_final
    return ret

_init_form = """<?xml version="1.0"?>
<form string="Comunicat Indústria">
  <separator string="Paràgraf" colspan="4"/>
  <field name="p_paragraf" nolabel="1" colspan="4" width="500" height="50"/>
  <separator string="Població" colspan="4"/>
  <field name="poblacions" nolabel="1" colspan="4" width="500" height="50"/>
  <separator string="Adreçes" colspan="4"/>
  <field name="adreces" nolabel="1" colspan="4" width="500" height="50"/>
  <separator string="CTS afectats" colspan="4" />
  <field name="cts_afectats" nolabel="1" colspan="4" />
</form>"""

_init_fields = {
    'nuclis_afectats': {'string': 'Nuclis afectats', 'type': 'text'},
    'poblacions': {'string': 'Població', 'type': 'text'},
    'adreces': {'string': 'Adreça', 'type': 'text'},
    'p_paragraf': {'string': 'Primer paragraf', 'type': 'text'},
    'paragraf': {'string': 'Paràgraf', 'type': 'text'},
    'poblacio': {'string': 'Poblacio', 'type': 'text'},
    'causes': {'string': 'Causes', 'type': 'text'},
    'cts_afectats': {'string': 'CTS afectats', 'type': 'text', 'readonly': True},
}


def _save_text(self, cr, uid, data, context=None):
    """
    Guardem el text que ens han introduït amb el formulari

    :param self:
    :param cr: Database cursor
    :param uid: User id
    :type uid: int
    :param data: Descarrec data
    :type data: dict
    :param context: OpenERP context
    :type context: dict
    :return: {}
    :rtype: dict
    """

    descarrec_obj = pooler.get_pool(cr.dbname).get('giscedata.descarrecs.descarrec')
    descarrec_obj.write(cr, uid, [data['id']], {'nuclis_afectats': data['form']['nuclis_afectats'], 'causes': data['form']['causes'], 'poblacions': data['form']['poblacions'], 'adreces': data['form']['adreces']})
    return {}


def _print(self, cr, uid, data, context={}):
    return {'ids': [data['id']]}


class giscedata_descarrecs_comunicat_industria(wizard.interface):

    states = {
        'init': {
            'actions': [_init],
            'result': {'type': 'form', 'arch': _init_form, 'fields': _init_fields, 'state': [('end', 'Cancelar', 'gtk-cancel'), ('save_text', 'Imprimir', 'gtk-print')]}
        },
        'save_text': {
            'actions': [_save_text],
            'result': {'type': 'state', 'state': 'print'}
        },
        'print': {
            'actions': [_print],
            'result': {
                'type': 'print',
                'report': 'giscedata.descarrecs.comunicat_industria',
                'get_id_from_action': True,
                'state': 'end'
            }
        },
    }
giscedata_descarrecs_comunicat_industria('giscedata.descarrecs.peusa.comunicat.industria')
