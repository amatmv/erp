# -*- coding: utf-8 -*-

from osv import fields, osv
import sqlite3
import tempfile
import os
import base64


class giscedata_descarrecs_descarrec(osv.osv):
    _name = 'giscedata.descarrecs.descarrec'
    _inherit = 'giscedata.descarrecs.descarrec'

    def _cts_afectats(self, cr, uid, ids, name, arg, context={}):
        res = {}
        cts = self._cts_afectats_obj(cr, uid, ids, name, arg, context)
        for i in cts.keys():
            string = ''
            for ct in cts[i]:
                string += '%s "%s"\n' % (ct['code_ct'], ct['descripcio'])
            res[i] = string
        return res

    def _adreces_afectades(self, cr, uid, ids, name, arg, context={}):
        res = {}
        # agafem els cts que hi ha afectats
        cts = self._cts_afectats_obj(cr, uid, ids, name, arg, context)
        for i in cts.keys():
            string = ''
            for ct in cts[i]:
                cr.execute("SELECT adreca FROM giscedata_cts WHERE id = %s",
                           (ct['id'],))
                p = cr.dictfetchone()
                string += "%s\n" % (p['adreca'],)
            res[i] = string
        return res

    def get_carrers_afectats(self, cursor, uid, desc_id, context=None):
        """
        Get data of the afected streets by the descarrec
        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param desc_id: Descarrec id
        :type desc_id: list of int
        :param context: OpenERP context
        :type context: dict[str, Any]
        :return: Afected streets
        :rtype: dict[str, str]
        """
        if context is None:
            context = {}

        ret = {}
        descarrec_obj = self.pool.get("giscedata.descarrecs.descarrec")
        desc_client_obj = self.pool.get(
            "giscedata.descarrecs.descarrec.clients"
        )
        clients = descarrec_obj.read(
            cursor, uid, desc_id, ["clients"], context
        )["clients"]
        clients_data = desc_client_obj.read(
            cursor, uid, clients, ["street", "codeine", "poblacio", "number"]
        )

        for client in clients_data:
            carrer = client["street"].split(",")[0]
            num = client["number"]
            if "," in num:
                num = num.split(",")[0]
            if client["codeine"] and client["poblacio"]:
                mun = " ".join(
                    map(str.strip, str(client["codeine"][1]).split(","))[::-1]
                )
                pob = " ".join(
                    map(str.strip, str(client["poblacio"][1]).split(","))[::-1]
                )
                if "{};{}".format(mun, pob) in ret:
                    if carrer in ret["{};{}".format(mun, pob)]:
                        num = str.strip(str(num))
                        if num and (num not in ret["{};{}".format(
                                mun, pob)][carrer]):
                            if num not in ret["{};{}".format(mun, pob)][carrer]:
                                ret["{};{}".format(mun, pob)][carrer].append(
                                    num)
                    else:
                        ret["{};{}".format(mun, pob)][carrer] = [num]
                else:
                    ret["{};{}".format(mun, pob)] = {carrer: [num]}
        return ret

    def export_descarrecs(self, cursor, uid, date, format="sqlite", context=None):
        """
        Exports descarrecs with state confirmat and with descarrec
        date >= date on a file

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param date:
        :param format: Export format, by default sqlite
        :param context: OpenERP context
        :type context: dict
        :return: Exported file in base64
        :rtype: str
        """
        search_params = [
            ('data_inici', '>=', date),
            ('state', '=', 'confirm')
        ]
        descarrecs_ids = self.search(cursor, uid, search_params)
        carrers_afectats_obj = self.pool.get(
            "giscedata.descarrecs.descarrec.carrers.afectats"
        )
        file_url = tempfile.mktemp()
        if format == "sqlite":
            sqlite_con = sqlite3.connect(file_url)
            sqlite_cur = sqlite_con.cursor()
            sql_create = """
                CREATE TABLE descarrecs(
                    inici datetime,
                    fi datetime,
                    municipi text,
                    poblacio text,
                    codi_postal text,
                    carrers_afectats text
                );
            """
            sql_insert = """
                INSERT INTO descarrecs(
                  inici,fi, municipi, poblacio, codi_postal,carrers_afectats
                )
                VALUES( 
                  ?,?,?,?,?,?
                );
            """
            sqlite_cur.execute(sql_create)
            for descarrec_id in (descarrecs_ids):
                carrers_ids = carrers_afectats_obj.search(
                    cursor, uid, [('descarrec_id', '=', descarrec_id)]
                )
                descarrec_data = self.read(
                    cursor, uid, descarrec_id, ['data_inici', 'data_final']
                )
                carrers_data = carrers_afectats_obj.read(
                    cursor, uid, carrers_ids,
                    ['mun_name', 'pob_name', 'carrers_afectats']
                )
                data = []
                for poblacio in carrers_data:
                    data = [
                        descarrec_data['data_inici'],
                        descarrec_data['data_final'],
                        poblacio['mun_name'],
                        poblacio['pob_name'],
                        "",
                        poblacio['carrers_afectats']
                    ]
                    sqlite_cur.execute(sql_insert, data)
                    data = []
            sqlite_con.commit()
            sqlite_con.close()
            with open(file_url, 'r') as f:
                db_data = f.read()
            os.remove(file_url)
            return base64.encodestring(db_data)
        return ""

    _columns = {
        'area': fields.text('Àrea de distribució'),
        'causes': fields.text('Descripció dels treballs'),
        'poblacions': fields.text('Població'),
        'adreces': fields.text('Adreça'),
        'adreces_afectades': fields.function(
            _adreces_afectades, type='text',
            string='Adreces afectades',
            method=True),
    }


giscedata_descarrecs_descarrec()
