# -*- coding: utf-8 -*-
{
    "name": "Programació dels detectors de PEUSA",
    "description": """Detectors""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Custom",
    "depends":[
        "giscedata_at_detectors"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_at_detectors_peusa_data.xml"
    ],
    "active": False,
    "installable": True
}
