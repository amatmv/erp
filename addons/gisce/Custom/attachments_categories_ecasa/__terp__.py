# -*- coding: utf-8 -*-
{
    "name": "attachment_categories_ecasa",
    "description": """Categories d'adjunts per eCasa
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Custom",
    "depends":[
        "base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "attachment_categories_ecasa_data.xml"
    ],
    "active": False,
    "installable": True
}
