# -*- coding: utf-8 -*-
{
    "name": "GISCE Tala per Estabanell",
    "description": """Modificacions a Tala per Estabanell:
 * Link a carpeta de documentació de línies des de trams de tala AT
$Id$""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Tala",
    "depends":[
        "giscedata_tala"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_tala_at_view.xml"
    ],
    "active": False,
    "installable": True
}
