# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscedataTalaAtTram(osv.osv):

    _name = 'giscedata.tala.at.tram'
    _inherit = 'giscedata.tala.at.tram'

    def _ff_link(self, cursor, uid, ids, field_name, arg, context=None):
        ''' Link a la carpeta dels documents de les línies AT del tram'''
        res = {}.fromkeys(ids, '')

        path = 'W:\\Departaments\\Tecnica\\INTEDIS\\Doc_ERP\\LAT'

        tram_obj = self.pool.get('giscedata.at.tram')

        for t_vals in self.read(cursor, uid, ids, ['name']):
            nom_linia = tram_obj.read(cursor, uid, t_vals['name'][0],
                                      ['linia'])['linia'][1]
            res[t_vals['id']] = '%s\%s\Tala' % (path, nom_linia)

        return res

    _columns = {
        'link': fields.function(_ff_link, string=u'Enllaç', type='char',
                                method=True, size=512,
                                help=u'Enllaça a documentació de la línia'),
    }

GiscedataTalaAtTram()