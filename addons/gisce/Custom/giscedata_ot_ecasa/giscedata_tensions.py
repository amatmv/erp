from osv import osv, fields


class GiscedataTensionsTensio(osv.osv):

    _name = 'giscedata.tensions.tensio'
    _inherit = "giscedata.tensions.tensio"

    _columns = {
        'show_in_ot': fields.boolean('Mostrar en OS')
    }

    _defaults = {
        'show_in_ot': lambda *a: True,
    }

GiscedataTensionsTensio()
