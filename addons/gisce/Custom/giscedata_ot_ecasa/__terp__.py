# -*- coding: utf-8 -*-
{
  "name": "Ordres de treball ECASA",
  "description": """""",
  "version": "0-dev",
  "author": "GISCE",
  "category": "Custom",
  "depends": ['base', 'c2c_webkit_report', 'giscedata_ot_comptadors', 'giscedata_ot_facturacio'],
  "init_xml": [],
  "demo_xml": [],
  "update_xml": ['giscedata_ot_ecasa_report.xml', 'giscedata_ot_ecasa_data.xml', 'giscedata_tensions_view.xml', 'giscedata_ot_view.xml'],
  "active": False,
  "installable": True
}