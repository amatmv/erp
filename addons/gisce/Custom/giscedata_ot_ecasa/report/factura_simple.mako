<%
  from datetime import datetime

  def get_sorted_orders(ordres):
      res = {}
      min_date = ordres[0].date_closed
      max_date = min_date
      for ot in ordres:
          contrata_ot = ot.operator_partner_id.name
          code = ot.close_id.name
          if contrata_ot not in res.keys():
            res[contrata_ot] = {}
          if code not in res[contrata_ot].keys():
            res[contrata_ot][code] = []
          res[contrata_ot][code].append(ot)
          if ot.create_date < min_date:
            min_date = ot.create_date
          if ot.date_closed > max_date:
            max_date = ot.date_closed
      return res, min_date.split(' ')[0], max_date.split(' ')[0]

  def get_product_price(cursor, uid, objects, close_obj):
      pricelist_obj = objects[0].pool.get('product.pricelist')
      imd_obj = objects[0].pool.get('ir.model.data')
      tarif_id = imd_obj.get_object_reference(cursor, uid, 'giscedata_ot_facturacio', 'tarif_ot')[1]
      ctx = {'date': datetime.today().strftime('%Y-%m-%d')}
      return pricelist_obj.price_get(cursor, uid, [tarif_id], close_obj.product_id.id, 1.0, context=ctx)[tarif_id]

  ordres, min_date, max_date = get_sorted_orders(objects)
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<head>
  <link rel="stylesheet" href="${addons_path}/giscedata_ot_ecasa/report/factura_simple.css"/>
</head>
<body>
% for contrata, ordres_contrata in ordres.items():
<table class="capcalera">
    <tr>
      <td><img class="logo" src="data:image/jpeg;base64,${company.logo}"></td>
      <td><div class="text_capcalera"><h4>${_(u"- Factura Órdenes Servicio - ")}${contrata} -</h4></div></td>
    </tr>
</table>
<table class="taula_resum">
    <tr>
        <td>${_(u"Fecha de facturación")}</td>
        <td>${_(u"Período de facturación")}</td>
        <td>${_(u"Número de Órdenes")}</td>
        <td>${_(u"Importe total")}</td>
    </tr>
    <tr>
        <td>${datetime.today().strftime("%d/%m/%y")}</td>
        <td>${"Del "}${min_date}${" al "}${max_date}</td>
        <td>${sum(len(x) for x in ordres_contrata.values())}</td>
        <td><b>${sum([sum(y.units*(get_product_price(cursor, uid, objects, y.close_id)) for y in x) for x in ordres_contrata.values()])}${u" €"}</b></td>
    </tr>
</table>
    % for ordre, ots in ordres_contrata.items():
        <table  class="resum_ordres_actuacio" style="page-break-inside: avoid">
            <tr><td colspan="4"><div class="capcalera_ordres_actuacio">${ordre}&nbsp;&nbsp;&nbsp;&nbsp;<span style="color: black;font-weight: normal;">${ots[0].close_id.product_description}</span></div></td></tr>
            <%
              product_price = get_product_price(cursor, uid, objects, ots[0].close_id)
              total_units = sum(y.units for y in ots)
              total_price = total_units * product_price
            %>
            <tr class="info_resum_ordres_actuacio">
               <td>${_(u"Nº de órdenes: ")}${len(ots)}</td>
               <td>${_(u"Nº de unidades: ")}${total_units}</td>
               <td>${_(u"Coste Unitario: ")}${product_price}${u" €"}</td>
               <td>${_(u"Importe: ")}${total_price}${u" €"}</td>
             </tr>
        </table>
    % endfor
    %if contrata != ordres.items()[-1][0]:
      <p style="page-break-after:always;clear: both"></p>
    %endif
% endfor
</body>
</html>
