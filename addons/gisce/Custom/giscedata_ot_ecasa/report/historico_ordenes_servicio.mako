<%
    from datetime import datetime
    data_act = str(datetime.today().strftime('%d/%m/%Y'))
    logo = objects[0].user_id.company_id.logo
    _colours = ['white', 'gray']
    index = 0
    units = 0
    _closed_aux = []
    _closed = {}

    def modifData(data):
        data_temp = datetime.strptime(data, '%Y-%m-%d %H:%M:%S')
        _data = data_temp.strftime('%d/%m/%Y')
        return _data

    for ot in sorted(objects, key=lambda k: k.date_closed):
        if ot.units != False:
            units+= ot.units
        _closed_aux.append(ot)

    for os in _closed_aux:
        if os.date_closed:
                data_key = modifData(os.date_closed)
        elif os.work_date:
                data_key = modifData(os.work_date)
        else:
                data_key = modifData(os.date)

        if _closed.has_key(data_key):
            _closed[data_key].append(os)
        else:
            _closed[data_key] = [os]

    _closed = sorted(_closed.items())

    if _closed:
        data_inici = _closed[0][0]
        data_fi = _closed[len(_closed)-1][0]
    else:
        data_inici = ''
        data_fi = ''
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<head>
  <style type="text/css">
    ${css}
  </style>
  <link rel="stylesheet" href="${addons_path}/giscedata_ot_ecasa/report/historico_ordenes_servicio.css"/>
</head>
<body>
    <div>
        <div class="header_left">
            %if logo:
                <img id="img_logo" src="data:image/jpeg;base64,${logo}">
            %endif
        </div>
        <div class="header_right">
            <div id="box-sup">${_(u"Histórico de Órdenes de Servicio cerradas desde")} ${data_inici} ${_(u"hasta")} ${data_fi}</div>
            <div id="box-inf">
                <div id="box_information"><font color="red">${_(u"Uds: ")}${units}</font></div>
                <div id="box_information"><font color="red">${_(u"Regs: ")}${len(_closed_aux)}</font></div>
                <div id="box_information"><font color="red">${data_act}</font></div>
            </div>
        </div>
    </div>
    <table style="font-weight: bold; text-align: center;">
        <tr>
            <td>${_(u"CÓD. CLIENTE")}</td>
            <td>${_(u"NÚMERO")}</td>
            <td>${_(u"F.CIERRE")}</td>
            <td>${_(u"UNIDADES")}</td>
            <td>${_(u"CÓDIGO")}</td>
            <td colspan="3">${_(u"ACTUACIÓN")}</td>
            <td colspan="4">${_(u"DIRECCIÓN")}</td>
        </tr>
    </table>
    %for _os in _closed:
        %for os in _os[1]:
            <%
                index = (index + 1) % 2
                classe = _colours[index]
                if os.date_closed:
                    data_temp = datetime.strptime(os.date_closed, '%Y-%m-%d %H:%M:%S')
                    data = data_temp.strftime('%d/%m/%Y')
                else: data = ''
            %>
            <table class=${classe}>
                <tr>
                    <td>
                        %if os.polissa_id.name:
                            ${os.polissa_id.name}
                        %endif
                    </td>
                    <td>${os.id}</td>
                    <td>${data}</td>
                    <td>
                        %if os.units:
                            ${os.units}
                        %endif
                    </td>
                    <td>
                        %if os.close_id.name:
                            ${os.close_id.name}
                        %endif
                    </td>
                    <td colspan="3">${os.section_id.name}</td>
                    <td colspan="4">
                        %if os.cups_address:
                            ${os.cups_address}
                        %endif
                    </td>
                </tr>
            </table>
        %endfor
        %if _closed_aux[-1] != os:
            <p class="separation">
            <hr>

        %endif
    %endfor
</body>
</html>
