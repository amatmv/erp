# -*- coding: utf-8 -*-
from c2c_webkit_report import webkit_report
from report import report_sxw
from tools import config
import babel
from datetime import datetime


class report_webkit_html(report_sxw.rml_parse):
    def __init__(self, cursor, uid, name, context):
        super(report_webkit_html, self).__init__(
            cursor, uid, name, context=context
        )
        dt = babel.dates.format_datetime(
            datetime.today(), "EEEE',' d 'de' LLLL 'de' YYYY",  locale='es_ES'
        )
        self.localcontext.update({
            'cursor': cursor,
            'uid': uid,
            'addons_path': config['addons_path'],
            'today': dt.encode('ascii', 'xmlcharrefreplace'),
        })

webkit_report.WebKitParser(
    'report.giscedata.ot.factura.detalle',
    'giscedata.ot',
    'giscedata_ot/report/factura_detalle.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.ot.historico.ordenes.servicio',
    'giscedata.ot',
    'giscedata_ot/report/historico_ordenes_servicio.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.ot.factura.simple',
    'giscedata.ot',
    'giscedata_ot/report/factura_simple.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.ot.cambio.contador',
    'giscedata.ot',
    'giscedata_ot/report/cambio_contador.mako',
    parser=report_webkit_html
)
