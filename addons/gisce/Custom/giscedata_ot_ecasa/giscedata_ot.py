# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _
from xml import dom


class GiscedataOt(osv.osv):
    _name = 'giscedata.ot'
    _inherit = 'giscedata.ot'

    FIELDS_BLOC_OT = {
        '1': [],
        '2': ['tensio_id'],
        '3': ['old_meter_id', 'old_meter_intensitat', 'old_meter_num_pols',
              'old_meter_group', 'meters_group'],
        '4': ['old_meter_id', 'old_meter_lect', 'old_meter_lect_pot',
              'old_meter_group', 'meters_group'],
        '5': ['old_meter_id', 'old_meter_group', 'meters_group'],
        '6': ['old_meter_id', 'new_meter_id', 'new_meter_intensitat',
              'new_meter_num_pols', 'old_meter_group', 'new_meter_group',
              'meters_group'],
        '7': ['old_meter_id', 'old_meter_lect', 'old_meter_lect_pot',
              'new_meter_id', 'new_meter_lect', 'new_meter_lect_pot',
              'old_meter_group', 'new_meter_group', 'meters_group'],
        '8': ['old_meter_id', 'old_meter_intensitat', 'old_meter_num_pols',
              'old_meter_group', 'meters_group'],
        '9': ['old_meter_id', 'old_meter_group', 'meters_group'],
        '10': ['old_meter_id', 'old_meter_intensitat', 'old_meter_num_pols',
               'new_meter_id', 'old_meter_group', 'new_meter_group',
               'meters_group'],
        '11': ['old_meter_id', 'new_meter_intensitat', 'new_meter_num_pols',
               'new_meter_id', 'old_meter_group', 'new_meter_group',
               'meters_group'],
        '13': ['new_meter_id', 'new_meter_intensitat', 'new_meter_num_pols', 'new_meter_group', 'meters_group'],
        '14': ['new_meter_id', 'new_meter_lect', 'new_meter_lect_pot', 'new_meter_group', 'meters_group'],
        '15': ['new_meter_id', 'new_meter_group', 'meters_group'],
        'n11': ['old_concentrador'],
        'n12': ['old_concentrador', 'new_concentrador'],
        'n13': ['old_sim', 'new_sim'],

    }

    FIELDS_OT = ['tensio_id', 'old_meter_id', 'old_meter_num_pols',
                 'old_meter_intensitat', 'old_meter_lect', 'old_meter_lect_pot',
                 'new_meter_id', 'new_meter_num_pols', 'new_meter_intensitat',
                 'new_meter_lect', 'new_meter_lect_pot', 'old_concentrador',
                 'new_concentrador', 'old_sim', 'new_sim']

    DEFAULT_CLOSE_CODE = {
        'OC': '8903103 (3.7.1)',
        'OD': '8903107 (3.7.6)',
        'RCAM': '8903106 (3.7.7)',
        'SDR': '8903105 (3.7.4)',
        'RIAM': '8903110 (3.7.11)',
        'SAR': '8903107 (3.7.6)',
        'CT': '8903110 (3.7.11)',
        'CC': '8903110 (3.7.11)',
        'OGT': '8903109 (3.7.9)',
        'OCT': '8903118 (4.1)',
        'OR': '8903119 (4.2)',
        'IPF': '8903109 (3.7.9)',
        'TRR': '8903134',
        'ARP': '8903108 (3.7.8)',
        'AC': '8903105 (3.7.4)',
        'CCTG': 'MONO',
        'ICNT': '8903210',
        'CCNT': '6754371',
        'SIM': '6754372'
    }

    def fields_view_get(self, cursor, uid, view_id=None, view_type='form',
                        context=None, toolbar=False):
        res = super(GiscedataOt, self).fields_view_get(
            cursor, uid, view_id, view_type, context=context, toolbar=toolbar
        )
        if res['name'] != 'crm.case.form':
            return res
        close_obj = self.pool.get('giscedata.ot.tancament')
        close_ids = close_obj.search(cursor, uid, [])
        close_ids_info = close_obj.read(cursor, uid, close_ids, ['blocs'])
        mydom = dom.minidom.parseString(
            unicode(res['arch'], 'utf-8').encode('utf-8')
        )
        page = [x for x in mydom.getElementsByTagName('page')
                if x.getAttribute('string')
                in ["Ordre de Treball", "Orden de Trabajo"]]
        if len(page) == 0:
            return res
        for grp in page[0].getElementsByTagName('group'):
            value = []
            grp_attr = {}
            if grp._attrs.get('attrs', False):
                grp_attr = eval(grp._attrs.get('attrs').value)
            for field in [f for f in grp.getElementsByTagName('field')
                          if f.getAttribute('name') in self.FIELDS_OT]:
                close_ids = self.get_visible_close_ids_of_field(
                    cursor, uid, close_ids_info, field.getAttribute('name')
                )
                value.append(('close_id', 'not in', close_ids))
                grp_attr.update({'invisible': value})
            grp.setAttribute('attrs', str(grp_attr))

        res['arch'] = mydom.toxml()
        return res

    def get_visible_close_ids_of_field(self, cr, uid, close_ids_info, field):
        res = []
        for close_id_info in close_ids_info:
            close_id = close_id_info['id']
            blocs = close_id_info['blocs']
            if not blocs:
                continue
            for bloc in blocs.split(','):
                if field in self.FIELDS_BLOC_OT[bloc]:
                    res.append(close_id)
                    break
        return res

    def onchange_close_id(self, cr, uid, ids, close_id, context=None):
        close_obj = self.pool.get("giscedata.ot.tancament")
        info = close_obj.read(cr, uid, close_id, ['name'])
        unitats = 1
        if info['name'] in ['8903117 (3.7.10)', '8903110 (3.7.11)']:
            unitats = 1
        return {'value': {'close_name': info['name'], 'units': unitats}}

    def _ff_close_name(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        close_obj = self.pool.get("giscedata.ot.tancament")
        res = {}
        for id in ids:
            info = self.read(cursor, uid, id, ['close_id'])
            if not info['close_id']:
                res[id] = False
            else:
                close_id = info['close_id'][0]
                names = close_obj.read(cursor, uid, close_id, ['name'])
                res[id] = names['name']
        return res

    _columns = {
        'close_name': fields.function(_ff_close_name, 'name', method=True,
                                      type='char', readonly=True, ),
        'old_concentrador': fields.char('Concentrador (Antiguo/Actual)', size=16),
        'new_concentrador': fields.char('Concentrador (Nuevo)', size=16),
        'old_sim': fields.char('Datos Tarjeta SIM (Antiguo/Actual)', size=13),
        'new_sim': fields.char('Datos Tarjeta SIM (Nuevo)', size=13),

    }

GiscedataOt()


class NewComptadorOt(osv.osv_memory):
    _name = 'wizard.new.comptador.ot'
    _inherit = 'wizard.new.comptador.ot'

    FIELDS_BLOC_OLD_METER = {
        '1': ['name'],
        '2': ['name'],
        '3': ['name', 'num_precinte', 'data_precinte', 'icp_num_pols', 'icp_intensitat'],
        '4': ['name', 'lectures', 'lectures_pot'],
        '5': ['name', 'num_precinte', 'data_precinte'],
        '6': ['name'],
        '7': ['name', 'lectures', 'lectures_pot'],
        '8': ['name', 'icp_num_pols', 'icp_intensitat'],
        '9': ['name'],
        '10': ['name', 'icp_num_pols', 'icp_intensitat'],
        '11': ['name', 'num_precinte', 'data_precinte', 'icp_num_pols', 'icp_intensitat'],
        '13': ['name', 'num_precinte', 'data_precinte', 'icp_num_pols',
              'icp_intensitat'],
        '14': ['name', 'lectures', 'lectures_pot'],
        '15': ['name', 'num_precinte', 'data_precinte'],
        'n11': ['name'],
        'n12': ['name'],
        'n13': ['name'],
    }

    FIELDS_BLOC_NEW_METER = {
        '1': ['name'],
        '2': ['name'],
        '3': ['name'],
        '4': ['name'],
        '5': ['name'],
        '6': ['name', 'num_precinte', 'data_precinte', 'icp_num_pols',
              'icp_intensitat'],
        '7': ['name', 'lectures', 'lectures_pot'],
        '8': ['name'],
        '9': ['name'],
        '10': ['name', 'num_precinte', 'data_precinte'],
        '11': ['name', 'num_precinte', 'data_precinte', 'icp_num_pols',
               'icp_intensitat'],
        '13': ['name', 'num_precinte', 'data_precinte', 'icp_num_pols', 'icp_intensitat'],
        '14': ['name', 'lectures', 'lectures_pot'],
        '15': ['name', 'num_precinte', 'data_precinte'],
        'n11': ['name'],
        'n12': ['name'],
        'n13': ['name'],
    }

    FIELDS_COMPTADOR = ['name', 'num_precinte', 'data_precinte', 'icp_num_pols',
                       'icp_intensitat', 'old_concentrador', 'new_concentrador',
                        'old_sim', 'new_sim']

    def fields_view_get(self, cursor, uid, view_id=None, view_type='form',
                        context=None, toolbar=False):
        res = super(NewComptadorOt, self).fields_view_get(
            cursor, uid, view_id, view_type, context=context, toolbar=toolbar
        )
        if not context:
            context = {}
        if res['name'] != 'wizard.new.comptador.ot.form':
            return res

        close_obj = self.pool.get('giscedata.ot.tancament')
        close_ids = close_obj.search(cursor, uid, [])
        close_ids_info = close_obj.read(cursor, uid, close_ids, ['blocs'])
        if context.get('new_meter'):
            blocs = self.FIELDS_BLOC_NEW_METER
        else:
            blocs = self.FIELDS_BLOC_OLD_METER
        from xml import dom
        mydom = dom.minidom.parseString(
            unicode(res['arch'], 'utf-8').encode('utf-8')
        )
        for grp in mydom.getElementsByTagName('group'):
            value = []
            for field in [f for f in grp.childNodes if f.localName == 'field']:
                field_name = field.getAttribute('name')
                if field_name in self.FIELDS_COMPTADOR:
                    close_ids = self.get_visible_close_ids_of_field(
                        cursor, uid, close_ids_info, field_name, blocs
                    )
                    value.append(('close_id', 'not in', close_ids))
                    grp.setAttribute('attrs', str({'invisible': value}))
                    break
        res['arch'] = mydom.toxml()
        return res

    def get_visible_close_ids_of_field(self, cr, uid, close_ids_info, field,
                                       bloc_fields):
        res = []
        for close_id_info in close_ids_info:
            close_id = close_id_info['id']
            blocs = close_id_info['blocs']
            if not blocs:
                continue
            for bloc in blocs.split(','):
                if field in bloc_fields[bloc]:
                    res.append(close_id)
                    break
        return res

    def onchange_ot_id(self, cursor, uid, ids, ot_id):
        if not ot_id:
            return {'value': {'close_id': False}}
        ot_obj = self.pool.get('giscedata.ot')
        ot_info = ot_obj.read(cursor, uid, ot_id, ['close_id'])
        if not ot_info:
            return {'value': {'close_id': False}}
        else:
            return {'value': {'close_id': ot_info['close_id'][0]}}

    _columns = {
        'close_id': fields.many2one('giscedata.ot.tancament')
    }

NewComptadorOt()
