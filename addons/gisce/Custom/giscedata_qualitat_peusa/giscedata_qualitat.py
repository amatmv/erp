# -*- coding: utf-8 -*-

from osv import fields, osv

class giscedata_qualitat_span_report(osv.osv):
    _name = 'giscedata.qualitat.span.report'

    _columns = {
      'id': fields.integer('Id'),
      'interruptor': fields.char('Interruptor', size=50),
      'linia': fields.text('Línia'),
      'data': fields.datetime('Data'),
      'any': fields.integer('Any'),
      'tipus': fields.char('Tipus',size=64),
      'causa': fields.char('Causa', size=64),
      'mes': fields.integer('Mes'),
    }

    _auto = False

    def init(self, cr):
        cr.execute("""
        create or replace view giscedata_qualitat_span_report as (
          SELECT sw.codi AS interruptor,
            CASE
                WHEN li.name::text = '301'::text THEN 'ADRALL-1'::text
                WHEN li.name::text = '302'::text THEN 'ADRALL-2'::text
                WHEN li.name::text = '239'::text THEN 'QUERFORADAT'::text
                WHEN li.name::text = '106'::text THEN 'LES TORRES'::text
                WHEN li.name::text = '201'::text THEN 'MARTINET'::text
                WHEN li.name::text = '403'::text OR li.name::text = '405'::text THEN 'LA FARGA'::text
                WHEN li.name::text = '420'::text THEN 'OS DE CIVIS'::text
                WHEN li.name::text = '224'::text THEN 'ARSEGUEL'::text
                WHEN li.name::text = '235'::text THEN 'ARDATX'::text
                WHEN li.name::text = '316'::text THEN 'ARAVELL-BELLESTAR'::text
                WHEN li.name::text = '247'::text THEN 'ESTANA'::text
                WHEN li.name::text = '305'::text THEN '104-ADRALL-1'::text
                WHEN li.name::text = '306'::text THEN '154-ADRALL-2'::text
                WHEN li.name::text = '205'::text THEN 'PONT-DE-BAR-MARTINET-1'::text
                WHEN li.name::text = '258'::text THEN 'PONT-DE-BAR-MARTINET-2'::text
                ELSE 'ALTRES'::text

            END AS linia, sp.begin_date AS data, ca.name AS causa, ty.name AS tipus, date_part('month'::text, sp.begin_date)::integer AS mes, date_part('year'::text, sp.begin_date)::integer AS "any", sp.id
         FROM giscedata_qualitat_span_switch sw
         LEFT JOIN giscedata_qualitat_span sp ON sw.span_id = sp.id
         LEFT JOIN giscedata_qualitat_span_cause ca ON sp.cause = ca.id
         LEFT JOIN giscedata_qualitat_span_type ty ON sp."type" = ty.id
         LEFT JOIN giscedata_at_linia li on sp.linia = li.id
         WHERE sp.tipus_tall::text = 'm'::text
        )
        """)

giscedata_qualitat_span_report()
