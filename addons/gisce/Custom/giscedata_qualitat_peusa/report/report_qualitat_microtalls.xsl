<?xml version="1.0" encoding="iso-8859-15"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:gi="http://gisce.net/XSL/xpath-functions" version="1.0">
    <xsl:strip-space elements="microtalls span"/>
    <xsl:template match="/">
        <xsl:apply-templates select="microtalls"/>
    </xsl:template>
    <xsl:template name="mes.numero.nom">
        <xsl:param name="numero"/>
        <xsl:choose>
            <xsl:when test="$numero = 1">Gener</xsl:when>
            <xsl:when test="$numero = 2">Febrer</xsl:when>
            <xsl:when test="$numero = 3">Mar�</xsl:when>
            <xsl:when test="$numero = 4">Abril</xsl:when>
            <xsl:when test="$numero = 5">Maig</xsl:when>
            <xsl:when test="$numero = 6">Juny</xsl:when>
            <xsl:when test="$numero = 7">Juliol</xsl:when>
            <xsl:when test="$numero = 8">Agost</xsl:when>
            <xsl:when test="$numero = 9">Setembre</xsl:when>
            <xsl:when test="$numero = 10">Octubre</xsl:when>
            <xsl:when test="$numero = 11">Novembre</xsl:when>
            <xsl:when test="$numero = 12">Desembre</xsl:when>
        </xsl:choose>
    </xsl:template>
    <xsl:template name="for.loop">
        <xsl:param name="i"/>
        <xsl:param name="mark"/>
        <xsl:param name="count"/>
        <xsl:choose>
            <xsl:when test="$i = $mark">
                <td><para style="creu">X</para></td>
            </xsl:when>
            <xsl:otherwise>
                <td/>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:if test="$i &lt;= $count">
            <xsl:call-template name="for.loop">
                <xsl:with-param name="i">
                    <xsl:value-of select="$i + 1"/>
                </xsl:with-param>
                <xsl:with-param name="count">
                    <xsl:value-of select="$count"/>
                </xsl:with-param>
                <xsl:with-param name="mark">
                    <xsl:value-of select="$mark"/>
                </xsl:with-param>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
    <xsl:template name="talls.mes">
        <xsl:param name="mesActual"/>
        <xsl:param name="mesFinal"/>
        <xsl:if test="$mesActual &lt;= $mesFinal">
            <!-- fer la feina -->
            <para style="heading2">Talls del mes <xsl:call-template name="mes.numero.nom"><xsl:with-param name="numero"><xsl:value-of select="$mesActual"/></xsl:with-param></xsl:call-template></para>
            <xsl:if test="count(//microtalls/span[mes=$mesActual]) = 0">
                <para style="notalls">No hi ha talls per aquest mes.</para>
            </xsl:if>
            <xsl:if test="count(//microtalls/span[mes=$mesActual]) &gt; 0">
                <blockTable style="talls" id="taula" colWidths="1.4cm,1.4cm,1.4cm,1.4cm,1.4cm,1.4cm,1.4cm,1.4cm,1.4cm,1.4cm,1.4cm,1.4cm,1.4cm,1.4cm,1.4cm,1.4cm,2cm,2.2cm,1.5cm" repeatRows="1">
                    <tr>
                        <!-- fila titols -->
                        <td>ADRALL-1</td>
                        <td>ADRALL-2</td>
                        <td>QUERFORADAT</td>
                        <td>LES TORRES</td>
                        <td>MARTINET</td>
                        <td>LA FARGA</td>
                        <td>OS CIVIS</td>
                        <td>ARS�GUEL</td>
                        <td>ARDATX</td>
                        <td><para style="titolpetit">ARAVELL-
                            BELLESTAR</para></td>
                        <td>ESTANA</td>
                        <td>104-ADRALL-1</td>
                        <td>154-ADRALL-2</td>
                        <td><para style="titolpetit">PONT-
                            DE-BAR-
                            MARTINET-1</para></td>
                        <td><para style="titolpetit">PONT-
                            DE-BAR-
                            MARTINET-2</para></td>
                        <td>ALTRES</td>
                        <td>Falta</td>
                        <td>Data</td>
                        <td>Causa</td>
                    </tr>
                    <xsl:for-each select="//microtalls/span[mes=$mesActual]">
                        <tr>
                            <xsl:choose>
                                <xsl:when test="//microtalls/span[mes=$mesActual and linia='ADRALL-1'] = current()">
                                    <xsl:call-template name="marcar-x">
                                        <xsl:with-param name="index">1</xsl:with-param>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:when test="//microtalls/span[mes=$mesActual and linia='ADRALL-2'] = current()">
                                    <xsl:call-template name="marcar-x">
                                        <xsl:with-param name="index">2</xsl:with-param>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:when test="//microtalls/span[mes=$mesActual and linia='QUERFORADAT'] = current()">
                                    <xsl:call-template name="marcar-x">
                                        <xsl:with-param name="index">3</xsl:with-param>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:when test="//microtalls/span[mes=$mesActual and linia='LES TORRES'] = current()">
                                    <xsl:call-template name="marcar-x">
                                        <xsl:with-param name="index">4</xsl:with-param>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:when test="//microtalls/span[mes=$mesActual and linia='MARTINET'] = current()">
                                    <xsl:call-template name="marcar-x">
                                        <xsl:with-param name="index">5</xsl:with-param>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:when test="//microtalls/span[mes=$mesActual and linia='LA FARGA'] = current()">
                                    <xsl:call-template name="marcar-x">
                                        <xsl:with-param name="index">6</xsl:with-param>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:when test="//microtalls/span[mes=$mesActual and linia='OS DE CIVIS'] = current()">
                                    <xsl:call-template name="marcar-x">
                                        <xsl:with-param name="index">7</xsl:with-param>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:when test="//microtalls/span[mes=$mesActual and linia='ARSEGUEL'] = current()">
                                    <xsl:call-template name="marcar-x">
                                        <xsl:with-param name="index">8</xsl:with-param>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:when test="//microtalls/span[mes=$mesActual and linia='ARDATX'] = current()">
                                    <xsl:call-template name="marcar-x">
                                        <xsl:with-param name="index">9</xsl:with-param>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:when test="//microtalls/span[mes=$mesActual and linia='ARAVELL-BELLESTAR'] = current()">
                                    <xsl:call-template name="marcar-x">
                                        <xsl:with-param name="index">10</xsl:with-param>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:when test="//microtalls/span[mes=$mesActual and linia='ESTANA'] = current()">
                                    <xsl:call-template name="marcar-x">
                                        <xsl:with-param name="index">11</xsl:with-param>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:when test="//microtalls/span[mes=$mesActual and linia='104-ADRALL-1'] = current()">
                                    <xsl:call-template name="marcar-x">
                                        <xsl:with-param name="index">12</xsl:with-param>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:when test="//microtalls/span[mes=$mesActual and linia='154-ADRALL-2'] = current()">
                                    <xsl:call-template name="marcar-x">
                                        <xsl:with-param name="index">13</xsl:with-param>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:when test="//microtalls/span[mes=$mesActual and linia='PONT-DE-BAR-MARTINET-1'] = current()">
                                    <xsl:call-template name="marcar-x">
                                        <xsl:with-param name="index">14</xsl:with-param>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:when test="//microtalls/span[mes=$mesActual and linia='PONT-DE-BAR-MARTINET-2'] = current()">
                                    <xsl:call-template name="marcar-x">
                                        <xsl:with-param name="index">15</xsl:with-param>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:when test="//microtalls/span[mes=$mesActual and linia='ALTRES'] = current()">
                                    <xsl:call-template name="marcar-x">
                                        <xsl:with-param name="index">16</xsl:with-param>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:otherwise>
                                    <td/>
                                    <td/>
                                    <td/>
                                    <td/>
                                    <td/>
                                    <td/>
                                    <td/>
                                    <td/>
                                    <td/>
                                    <td/>
                                    <td/>
                                    <td/>
                                    <td/>
                                    <td/>
                                    <td/>
                                    <td/>
                                </xsl:otherwise>
                            </xsl:choose>
                            <td>
                                <xsl:value-of select="tipus"/>
                            </td>
                            <td>
                                <xsl:value-of select="concat(substring(data, 9, 2), '/', substring(data, 6, 2), '/', substring(data, 1, 4), ' ', substring(data, 12, 8))"/>
                            </td>
                            <td>
                                <xsl:value-of select="causa"/>
                            </td>
                        </tr>
                    </xsl:for-each>
                </blockTable>
            </xsl:if>
            <nextPage/>
            <!-- i cridar la funci� un altre cop -->
            <xsl:call-template name="talls.mes">
                <xsl:with-param name="mesActual">
                    <xsl:value-of select="$mesActual + 1"/>
                </xsl:with-param>
                <xsl:with-param name="mesFinal">12</xsl:with-param>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
    <xsl:template name="marcar-x">
        <xsl:param name="index"/>
        <xsl:call-template name="for.loop">
            <xsl:with-param name="i">1</xsl:with-param>
            <xsl:with-param name="mark">
                <xsl:value-of select="($index)"/>
            </xsl:with-param>
            <xsl:with-param name="count">15</xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    <xsl:template match="microtalls">
        <document>
            <template pageSize="(297mm,19cm)" showBoundary="0">
                <pageTemplate id="main">
                    <!-- <frame id="frameTitol" x1="0cm" y1="16.5cm" width="29cm" height="2cm"/> -->
                    <!-- <frame id="frameMesos" x1="0cm" y1="0cm" width="29cm" height="17cm"/> -->
                    <frame id="frameTalls" x1="0cm" y1="0.5cm" width="29cm" height="18cm"/>
                </pageTemplate>
            </template>
            <stylesheet>
                <paraStyle name="heading" fontSize="24" fontName="Helvetica-Bold" alignment="center" leading="40"/>
                <paraStyle name="heading2" fontSize="16" fontName="Helvetica-Bold" alignment="center" leading="40"/>
                <paraStyle name="notalls" fontSize="14" fontName="Helvetica" alignment="center"/>
                <paraStyle name="titolpetit" fontSize="4.5" fontName="Helvetica-Bold" alignment="center"/>
                <paraStyle name="creu" fontSize="12" fontName="Helvetica-Bold" alignment="center"/>
                <blockTableStyle id="mesos">
                    <blockFont name="Helvetica-Bold" size="6"/>
                    <blockAlignment value="CENTER" start="0,0" stop="-1,-1"/>
                    <lineStyle kind="LINEBEFORE" colorName="black" start="0,0" stop="-1,-1"/>
                    <lineStyle kind="LINEABOVE" colorName="black" start="0,0" stop="-1,-1"/>
                    <lineStyle kind="LINEBELOW" colorName="black" start="0,0" stop="-1,-1"/>
                    <blockBackground colorName="silver" start="0,0" stop="0,-1"/>
                </blockTableStyle>
                <blockTableStyle id="talls">
                    <blockAlignment value="CENTER" start="0,0" stop="-1,-1"/>
                    <blockFont name="Helvetica-Bold" size="4.5" start="0,0" stop="-1,0"/>
                    <blockFont name="Helvetica" size="4" start="0,1" stop="-1,-1"/>
                    <lineStyle kind="GRID" colorName="black" start="0,0" stop="-1,-1"/>
                    <blockBackground colorName="silver" start="0,0" stop="-1,0"/>
                </blockTableStyle>
                <blockTableStyle id="totals">
                    <blockAlignment value="CENTER" start="0,0" stop="-1,-1"/>
                    <blockFont name="Helvetica-Bold" size="6" start="0,0" stop="-1,-1"/>
                    <lineStyle kind="GRID" colorName="black" start="0,0" stop="-1,-1"/>
                </blockTableStyle>
            </stylesheet>
            <story>
                <para style="heading">Resum dels microtalls de l'any <xsl:value-of select="//microtalls/span[1]/any"/></para>
                <xsl:call-template name="talls.mes">
                    <xsl:with-param name="mesActual">1</xsl:with-param>
                    <xsl:with-param name="mesFinal">12</xsl:with-param>
                </xsl:call-template>
                <para style="heading2">Totals</para>
                <blockTable style="talls" id="taula-totals" colWidths="1.4cm,1.4cm,1.4cm,1.4cm,1.4cm,1.4cm,1.4cm,1.4cm,1.4cm,1.4cm,1.4cm,1.4cm,1.4cm,1.4cm,1.4cm,1.4cm">
                    <tr>
                        <!-- fila titols -->
                        <td>ADRALL-1</td>
                        <td>ADRALL-2</td>
                        <td>QUERFORADAT</td>
                        <td>LES TORRES</td>
                        <td>MARTINET</td>
                        <td>LA FARGA</td>
                        <td>OS CIVIS</td>
                        <td>ARS�GUEL</td>
                        <td>ARDATX</td>
                        <td><para style="titolpetit">ARAVELL-
                            BELLESTAR</para></td>
                        <td>ESTANA</td>
                        <td>104-ADRALL-1</td>
                        <td>154-ADRALL-2</td>
                        <td><para style="titolpetit">PONT-
                            DE-BAR-
                            MARTINET-1</para></td>
                        <td><para style="titolpetit">PONT-
                            DE-BAR-
                            MARTINET-2</para></td>
                        <td>ALTRES</td>
                    </tr>
                    <tr>
                        <td>
                            <xsl:value-of select="count(//microtalls/span[linia='ADRALL-1'])"/>
                        </td>
                        <td>
                            <xsl:value-of select="count(//microtalls/span[linia='ADRALL-2'])"/>
                        </td>
                        <td>
                            <xsl:value-of select="count(//microtalls/span[linia='QUERFORADAT'])"/>
                        </td>
                        <td>
                            <xsl:value-of select="count(//microtalls/span[linia='LES TORRES'])"/>
                        </td>
                        <td>
                            <xsl:value-of select="count(//microtalls/span[linia='MARTINET'])"/>
                        </td>
                        <td>
                            <xsl:value-of select="count(//microtalls/span[linia='LA FARGA'])"/>
                        </td>
                        <td>
                            <xsl:value-of select="count(//microtalls/span[linia='OS DE CIVIS'])"/>
                        </td>
                        <td>
                            <xsl:value-of select="count(//microtalls/span[linia='ARSEGUEL'])"/>
                        </td>
                        <td>
                            <xsl:value-of select="count(//microtalls/span[linia='ARDATX'])"/>
                        </td>
                        <td>
                            <xsl:value-of select="count(//microtalls/span[linia='ARAVELL-BELLESTAR'])"/>
                        </td>
                        <td>
                            <xsl:value-of select="count(//microtalls/span[linia='ESTANA'])"/>
                        </td>
                        <td>
                            <xsl:value-of select="count(//microtalls/span[linia='104-ADRALL-1'])"/>
                        </td>
                        <td>
                            <xsl:value-of select="count(//microtalls/span[linia='154-ADRALL-2'])"/>
                        </td>
                        <td>
                            <xsl:value-of select="count(//microtalls/span[linia='PONT-DE-BAR-MARTINET-1'])"/>
                        </td>
                        <td>
                            <xsl:value-of select="count(//microtalls/span[linia='PONT-DE-BAR-MARTINET-2'])"/>
                        </td>
                        <td>
                            <xsl:value-of select="count(//microtalls/span[linia='ALTRES'])"/>
                        </td>
                    </tr>
                </blockTable>
            </story>
        </document>
    </xsl:template>
</xsl:stylesheet>
