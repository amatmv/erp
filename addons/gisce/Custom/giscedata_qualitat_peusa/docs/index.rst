.. Linies Microtalls documentation master file, created by
   sphinx-quickstart on Thu Mar 20 09:31:30 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Linies Microtalls
=============================================

Correspondencia dels noms de les columnes amb els numeros de línia
==================================================================
+---------------+----------------+ 
| Nom columna   |  Número línia  | 
+===============+================+ 
| ADRALL-1  	| 301  		 | 
+---------------+----------------+ 
| ADRALL-2  	| 302  		 | 
+---------------+----------------+ 
| QUERFORADAT  	| 239 		 | 
+---------------+----------------+ 
| LES TORRES  	| 106  		 | 
+---------------+----------------+ 
| MARTINET  	| 201  		 | 
+---------------+----------------+ 
| LA FARGA  	| 403 i 405  	 | 
+---------------+----------------+ 
| OS DE CIVIS  	| 420  		 | 
+---------------+----------------+ 
| ARSEGUEL  	| 224  		 | 
+---------------+----------------+ 
| ARDATX  	| 235  		 | 
+---------------+----------------+ 
| ARAVELL-BELLESTAR  | 316  	 |
+---------------+----------------+ 
| ESTANA  	| 247  		 | 
+---------------+----------------+ 
| 104-ADRALL-1  | 305  		 | 
+---------------+----------------+ 
| 154-ADRALL-2  | 306  		 | 
+---------------+----------------+ 
| PONT-DE-BAR-MARTINET-1  | 205  | 
+---------------+----------------+ 
| PONT-DE-BAR-MARTINET-2  | 258  | 
+---------------+----------------+ 












