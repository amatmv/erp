# -*- coding: utf-8 -*-
{
    "name": "giscedata_qualitat",
    "description": """GISCE Qualitat per PEUSA""",
    "version": "0-dev",
    "author": "GISCE Enginyeria, SL",
    "category": "Qualitat",
    "depends":[
        "giscedata_qualitat"
    ],
    "init_xml":[
        "giscedata_qualitat_report.xml",
        "giscedata_qualitat_wizard.xml"
    ],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
