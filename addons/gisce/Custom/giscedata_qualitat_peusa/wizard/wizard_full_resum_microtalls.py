# *-* coding: utf-8 *-*

import wizard

def _selection_any(self, cr, uid, context={}):
    cr.execute('select distinct "any" from giscedata_qualitat_span_report')
    return [(a[0], a[0]) for a in cr.fetchall()]

_init_form = """<?xml version="1.0"?>
<form string="Seleccionar any" col="1">
  <field name="any" />
</form>
"""

_init_fields = {
  'any' : {'string':'Any', 'type':'selection', 'selection': _selection_any,
           'required': True },
}

def _print(self, cr, uid, data, context={}):
    cr.execute('select id from giscedata_qualitat_span_report where "any"=%d' % data['form']['any'])
    return { 'ids': [a[0] for a in cr.fetchall()] }

class wizard_full_resum_microtalls(wizard.interface):

    states = {
      'init': {
        'actions': [],
        'result': {'type': 'form', 'arch': _init_form, 'fields': _init_fields, 'state': [('end', 'Cancelar', 'gtk-cancel'), ('print', 'Imprimir', 'gtk-print')]}
      },
      'print': {
        'actions': [_print],
        'result': {'type': 'print', 'report':
        'giscedata.qualitat.microtalls', 'get_id_from_action':True,
        'state':'end'}
      },
      'end': {
        'actions': [],
        'result': {'type': 'state', 'state': 'end'}
      }
    }


wizard_full_resum_microtalls('giscedata.qualitat.microtalls')
