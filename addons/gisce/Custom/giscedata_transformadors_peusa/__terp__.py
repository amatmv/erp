# -*- coding: utf-8 -*-
{
    "name": "GISCE Transformadors (PEUSA)",
    "description": """Multi-company support for Transformadors""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Custom",
    "depends":[
        "base",
        "giscedata_transformadors"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_transformadors_peusa_view.xml"
    ],
    "active": False,
    "installable": True
}
