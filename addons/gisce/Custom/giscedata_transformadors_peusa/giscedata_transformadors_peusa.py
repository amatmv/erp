# -*- coding: utf-8 -*-

from osv import osv, fields

class giscedata_transformador_trafo(osv.osv):
    _name = "giscedata.transformador.trafo"
    _inherit = "giscedata.transformador.trafo"
    _columns = {
        'emergencia': fields.boolean('Emergencia'),
    }
    _defaults = {
		'emergencia': lambda *a: 0,
    }
giscedata_transformador_trafo()
