# -*- coding: utf-8 -*-
{
    "name": "res_partner_custom_ab",
    "description": """
        Personalització partner per AB Enegia
        """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Custom",
    "depends":[
        "base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "res_partner_view.xml"
    ],
    "active": False,
    "installable": True
}
