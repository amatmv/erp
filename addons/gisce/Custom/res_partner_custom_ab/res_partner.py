# -*- coding: utf-8 -*-
from osv import fields, osv

class res_partner(osv.osv):
    _name = 'res.partner'
    _inherit = 'res.partner'

    _columns = {
        'info_comercial': fields.boolean(
            u'Recibir información comercial'
        ),
        'info_boletin': fields.boolean(
            u'Recibir boletín'
        ),
    }

    _defaults = {
        'info_comercial': lambda * a: False,
        'info_boletin': lambda * a: False
    }

res_partner()
