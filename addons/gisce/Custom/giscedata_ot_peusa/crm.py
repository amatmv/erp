# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _

class crm_case_section(osv.osv):
    _name = 'crm.case.section'
    _inherit = 'crm.case.section'

    _columns = {
        'report_id': fields.many2one(
            'ir.actions.report.xml', 'Report ID', required=False)
    }

    _defaults = {
        'report_id': lambda * a: False
    }

crm_case_section()

class GiscedataSwitchingCrmAb(osv.osv):
    _name = 'crm.case'
    _inherit = 'crm.case'

    SAME_CUPS_TEXT = _(u"Existencia de OT en curso para el mismo CUPS en las secciones:")

    def create_case_generic(self, cursor, uid, model_ids, context=None,
                                  description='Generic Case',
                                  section='CASE',
                                  category=None,
                                  assign=None,
                                  extra_vals=None):

        case_ids = super(GiscedataSwitchingCrmAb, self).create_case_generic(
                                  cursor, uid, model_ids, context,
                                  description, section,
                                  category, assign, extra_vals)

        if not context:
            context = {}
        model = context.get('model', False)

        if model == 'giscedata.switching':
            sw_obj = self.pool.get('giscedata.switching')
            cups_obj = self.pool.get('giscedata.cups.ps')
            sect_obj = self.pool.get('crm.case.section')
            for case in self.browse(cursor, uid, case_ids, context):
                ref_spl = case.ref.split(',')
                if len(ref_spl) == 2:
                    sw_id = int(ref_spl[1])
                    sw = sw_obj.read(cursor, uid, sw_id,
                                     ['cups_id', 'titular_polissa'])

                    # afegim el CUPS i adreça de subministrament a la descripcio
                    cups_id = sw['cups_id'][0]
                    cupsr = cups_obj.read(cursor, uid, [cups_id],
                                          ['name', 'direccio'])
                    cups = cupsr[0]['name']
                    adr = cupsr[0]['direccio']
                    desc = case.description
                    vals = {'name': '{0}: {1}, {2}'.format(desc, cups, adr)}

                    # afegim el titular de la polissa relacionada a la OT
                    empresa_id = sw['titular_polissa'][0]
                    vals.update({'partner_id': empresa_id})
                    case.write(vals)

                    # comprovar altres OTs amb mateix CUPS
                    ot_ids = self.get_ots_cups_coincident(cursor, uid, cups_id, case.id)
                    if ot_ids:
                        ot_ids = [case.id] + ot_ids
                        # Get the sections
                        sec_inf = self.read(cursor, uid, ot_ids, ['section_id'])
                        sec_ids = [s['section_id'][0] for s in sec_inf]
                        sections = sect_obj.read(cursor, uid, sec_ids, ['code'])
                        sec_msg = ""
                        for sec in sections:
                            sec_msg = "{0}, {1}".format(sec_msg, sec['code'])
                        sec_msg = sec_msg[1:]
                        # Historize current description of cases
                        self.case_log(cursor, uid, ot_ids, context)
                        # Write and historize the comment about CUPS
                        self.write(cursor, uid, ot_ids, {
                            'description': self.SAME_CUPS_TEXT+sec_msg
                        })
                        self.case_log(cursor, uid, ot_ids, context)
        return case_ids

    def get_ots_cups_coincident(self, cursor, uid, cups_id, case_id=None,
                                context=None):
        """ Returns OTs (not closed) with the specified cups"""
        case_obj = self.pool.get("crm.case")
        imd_obj = self.pool.get('ir.model.data')
        ot_section = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_ot_peusa', 'ot_ordenes_de_trabajo'
        )[1]
        ot_ids = case_obj.search(cursor, uid, [
            ('polissa_id.cups', '=', cups_id),
            ('section_id', 'child_of', [ot_section]),
            ('id', '!=', case_id),
            ('state', '!=', 'done')
        ])
        return ot_ids

    def get_non_commented_ots(self, cursor, uid, ot_ids, context=None):
        """ Returns ot ids which don't have the comment SAME_CUPS_TEXT """
        case_history_obj = self.pool.get("crm.case.history")
        res_ids = []
        for ot_id in ot_ids:
            history_lines = case_history_obj.search(cursor, uid, [
                ('case_id', '=', ot_id),
                ('description', '=', self.SAME_CUPS_TEXT)
            ])
            if not len(history_lines):
                res_ids.append(ot_id)
        return res_ids

GiscedataSwitchingCrmAb()
