# -*- coding: utf-8 -*-
{
    "name": "Ordres de treball Peusa",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Reports",
    "depends":[
        "c2c_webkit_report",
        "giscedata_polissa_crm"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_ot_peusa_report.xml",
        "giscedata_ot_peusa_data.xml"
    ],
    "active": False,
    "installable": True
}
