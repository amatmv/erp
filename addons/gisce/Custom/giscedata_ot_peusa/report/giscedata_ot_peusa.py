# -*- coding: utf-8 -*-
from __future__ import absolute_import

from giscedata_ot_peusa.report import ot_report
from c2c_webkit_report import webkit_report
from report import report_sxw
from tools import config
from .report_helper import get_comptador
from .report_helper import get_icp
from .report_helper import get_observacions
from .report_helper import get_butlleti


class report_webkit_html(report_sxw.rml_parse):
    def __init__(self, cursor, uid, name, context):
        super(report_webkit_html, self).__init__(cursor, uid, name,
                                                 context=context)
        self.localcontext.update({
            'cursor': cursor,
            'uid': uid,
            'addons_path': config['addons_path'],
            'get_comptador': get_comptador,
            'get_icp': get_icp,
            'get_observacions': get_observacions,
            'get_butlleti': get_butlleti
        })


webkit_report.WebKitParser(
    'report.giscedata.ot.peusa.base',
    'crm.case',
    'giscedata_ot_peusa/report/giscedata_ot_peusa_base.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.ot.peusa.alta',
    'crm.case',
    'giscedata_ot_peusa/report/giscedata_ot_peusa_alta.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.ot.peusa.baixa',
    'crm.case',
    'giscedata_ot_peusa/report/giscedata_ot_peusa_baixa.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.ot.peusa.tall',
    'crm.case',
    'giscedata_ot_peusa/report/giscedata_ot_peusa_tall.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.ot.peusa.general',
    'crm.case',
    'giscedata_ot_peusa/report/giscedata_ot_peusa_general.mako',
    parser=report_webkit_html
)

ot_report.WebKitParser(
    'report.giscedata.ot.peusa.section',
    'crm.case',
    '',
    parser=report_webkit_html
)
