<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<head>
		<style type="text/css">
	    ${css}
		    body{
		        font-family: "Liberation Sans";
		        font-size: 10px;
		    }
		    #header{
		    	margin-top: 20px;

		    }
		    #title{
		    	float: right;
		    	left: 50px;
		    	border-style: solid;
		    	border-width: 1px;
		    	width: 400px;
		    	padding: 10px;
		    	text-align: center;
		    	font-weight: bold;
		    	font-size: 16px;
		    }
		    table{
		    	border: 1px solid black;
		    	border-collapse: collapse;
		    	width: 100%;
		    }
		    table td{
		    	border: 1px solid black;
		    	text-align: center;
		    	font-size: 10px;
		    }
		    #taula_firma td{
		    	border: none;
		    	text-align: left;
		    }
		    #taula_firma{
		    	position: relative;
		    	left: 1px;
		    }
		    #separador{
		    	margin-top: 5px;
		    	margin-bottom: 5px;
		    }
		    .table_title{
		    	background-color: #DBE0D6;
		    	font-weight: bold;
		    }
		    .aparells td{
		    	width: 11%
		    }
		    .linia_aparell{
		    	position: relative;
		    	top: -1px;
		    }
		    .linia_aparell td{
		    	width: 8%;
		    }
		    .linia_aparell td.first{
		    	width:11%;
		    }
		    #observacions{
		    	height: 50px;
		    	text-align: left;
		    	vertical-align: top;
		    }
		    #informe td{
		    	width: 14%;
		    }
		    #observacions_informe{
		    	height: 100px;
		    	text-align: left;
		    	vertical-align: top;
		    }
		    #peu_informe{
		    	position: relative;
		    	float: left;
		    	width: 80%;
		    }
		    #firma{
		    	position: relative;
		    	float: left;
		    	width: 20%;
		    	height: 100px;
		    }
		    #logo{
		    	position: relative;
		    	float: left;
		    	width: 250px;
		    	height: 75px;
		    }
	    </style>
	</head>
	<%def name="header_factura(case)">
		<i>
		<div id="header">
		    <div id="logo">
                <img src="${addons_path}/giscedata_ot_peusa/report/peusa-logo.png"/>
            </div>
			<div id="title">${case.section_id.name}</div>
			<div style="clear:both"></div>
			<br>
		</div>
		</i>
	</%def>
    <% comptador_casos = 0 %>
	<body>
	%for case in objects:
		${header_factura(case)}
		<table>
			<tr class="table_title">
				<td>${_("Nº O.T")}</td>
				<td>${_("Fecha")}</td>
				<td>${_("Ruta")}</td>
				<td>${_("Orden")}</td>
				<td>${_("Nº Aviso")}</td>
				<td>${_("Gestor")}</td>
			</tr>
			<tr>
				<td>${case.id or ''}</td>
				<td>${formatLang(case.create_date.split(' ')[0], date=True) or ''}</td>
				<td>${case.polissa_id.cups.zona or ''}</td>
				<td>${case.polissa_id.cups.ordre or ''}</td>
				<td></td>
				<td>${case.user_id.name or ''}</td>
			</tr>
		</table>
		<br>
		<table>
			<tr class="table_title">
				<td colspan="5">
					${_("Datos del Titular y Dirección de Suministro")}
				</td>
			</tr>
			<tr class="table_title">
				<td>${_("Nº P.S.")}</td>
				<td>${_("Nº Cliente")}</td>
				<td>${_("Nº Poliza")}</td>
				<td>${_("Nombre del titular")}</td>
				<td>${_("DNI/CIF")}</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>${case.polissa_id.name or ''}</td>
				<td>${case.polissa_id.name or ''}</td>
				<td>${case.polissa_id.titular.name or ''}</td>
				<td>${case.polissa_id.titular.vat.lstrip('ES') or ''}</td>
			</tr>
		</table>
		<table style="position: relative; top: -1px;">
			<tr class="table_title">
				<td>
					${_("Dirección")}
				</td>
			</tr>
			<tr>
				<%
					direccio = case.polissa_id.cups.direccio
					idx_pob = direccio.rfind('(')
					if idx_pob != -1:
                    	direccio = direccio[:idx_pob]
				%>
				<td>${direccio}</td>
			</tr>
		</table>
		<table style="position: relative; top: -2px;">
			<tr class="table_title">
				<td>${_("C.P.")}</td>
				<td>${_("Población")}</td>
				<td>${_("Teléfono 1")}</td>
				<td>${_("Teléfono 2")}</td>
				<td>${_("CUPS")}</td>
			</tr>
			<tr>
				<td>${case.polissa_id.cups.dp or ''}</td>
				<td>${case.polissa_id.cups.id_poblacio.name or ''}</td>
				<td>${case.polissa_id.titular.address.phone[0] or ''}</td>
				<td>${case.polissa_id.titular.address.mobile[0] or ''}</td>
				<td>${case.polissa_id.cups.name or ''}</td>
			</tr>
		</table>
		<br>
		<table>
			<tr class="table_title">
				<td colspan="3">${_("Boletín de Instalación")}</td>
			</tr>
			<tr class="table_title">
				<td>${_("Instalador")}</td>
				<td>${_("Referencia")}</td>
				<td>${_("Teléfono")}</td>
			</tr>
			<tr>
				<td>${get_butlleti(case, 'instalador') or ''}&nbsp;</td>
				<td>${get_butlleti(case, 'referencia') or ''}</td>
				<td>${get_butlleti(case, 'telefon') or ''}</td>
			</tr>
		</table>
		<table style="position: relative; top: -1px;">
			<tr class="table_title">
				<td>${_("Nº Boletín")}</td>
				<td>${_("Fecha Instalación")}</td>
				<td>${_("Fecha Vigencia")}</td>
				<td>${_("Observaciones")}</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>${get_butlleti(case, 'data_inst') or ''}</td>
				<td>${get_butlleti(case, 'data_vig') or ''}</td>
				<td></td>
			</tr>
		</table>
		<br>
		<table>
			<tr class="table_title">
				<td colspan="5">
					${_("Datos técnicos")}
				</td>
			</tr>
			<tr class="table_title">
				<td>${_("Línea BT")}</td>
				<td>${_("Centro")}</td>
				<td>${_("Circuito")}</td>
				<td>${_("Caja")}</td>
				<td>${_("Acometida")}</td>
			</tr>
			<tr>
				<td>${case.polissa_id.cups.linia or ''}</td>
				<td>${case.polissa_id.cups.et or ''}</td>
				<td>&nbsp;</td>
				<td></td>
				<td>${case.polissa_id.cups.id_escomesa.name or ''}</td>
			</tr>
		</table>
		<table style="position: relative; top: -1px;">
			<tr class="table_title">
				<td>${_("Tensión")}</td>
				<td>${_("Fases")}</td>
				<td>${_("Amperios")}</td>
				<td>${_("Tarifa")}</td>
				<td>${_("Potencias")}</td>
				<td>${_("Discrim.")}</td>
				<td colspan="2">${_("Maxim.")}</td>
			</tr>
			<tr>
				<td>${case.polissa_id.tensio or ''}</td>
				<td></td>
				<td></td>
				<td>${case.polissa_id.tarifa.name or ''}</td>
				<td>${case.polissa_id.modcontractual_activa.potencies_periode or ''}</td>
				<td></td>
				<td style="width: 20px;">&nbsp;</td>
				<td style="width: 40px;"></td>
			</tr>
		</table>
		<br>
		<table class="aparells">
			<tr class="table_title">
				<td colspan="9">
					${_("Aparatos de Medida")}
				</td>
			</tr>
			<tr class="table_title">
				<td>${_("Datos")}</td>
				<td>${_("Activa")}</td>
				<td>${_("Reactiva")}</td>
				<td>${_("Reloj")}</td>
				<td>${_("ICP1")}</td>
				<td>${_("ICP2")}</td>
				<td>${_("Maxímetro")}</td>
				<td>${_("R. Horario")}</td>
				<td>${_("Modem")}</td>
			</tr>
			<tr>
				<td><b>${_("Número")}</b></td>
				<td>${get_comptador(case).name or ''}</td>
				<td></td>
				<td></td>
				<td>${get_icp(case, 0, 'name') or ''}</td>
				<td>${get_icp(case, 1, 'name') or ''}</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td><b>${_("Tipo")}</b></td>
				<td>${get_comptador(case).product_id.name or ''}</td>
				<td></td>
				<td></td>
				<td>${get_icp(case, 0, 'tipo') or ''}</td>
				<td>${get_icp(case, 1, 'tipo') or ''}</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td><b>${_("Relación")}</b></td>
				<td>&nbsp;</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td><b>${_("Marca")}</b></td>
				<td>${get_comptador(case).product_id.default_code or ''}</td>
				<td></td>
				<td></td>
				<td>${get_icp(case, 0, 'marca') or ''}</td>
				<td>${get_icp(case, 1, 'marca') or ''}</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td><b>${_("Amperios")}</b></td>
				<td>&nbsp;</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td><b>${_("Voltios")}</b></td>
				<td>&nbsp;</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td><b>${_("Ana/Digit.")}</b></td>
				<td>&nbsp;</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td><b>${_("Precinto")}</b></td>
				<td>&nbsp;</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td><b>${_("Fecha Inst")}</b></td>
				<td>${formatLang(get_comptador(case).data_alta, date=True) or ''}</td>
				<td></td>
				<td></td>
				<td>${get_icp(case, 0, 'fecha_inst') or ''}</td>
				<td>${get_icp(case, 1, 'fecha_inst') or ''}</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td><b>${_("Propietario")}</b></td>
				<td>${get_comptador(case).propietat or ''}</td>
				<td>&nbsp;</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
		</table>
		<table class="linia_aparell">
			<tr>
				<td class="first"><b>${_("P1")}</b></td>
				<td></td>
				<td ><b>${_("P2")}</b></td>
				<td></td>
				<td><b>${_("P3")}</b></td>
				<td></td>
				<td><b>${_("Reactiva")}</b></td>
				<td></td>
				<td><b>${_("Maxímetro")}</b></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
		</table>
		<table id="separador">
			<tr>
				<td>&nbsp;</td>
			</tr>
		</table>
		<table>
			<tr class="table_title">
				<td>${_("Persona Contacto")}</td>
				<td>${_("Teléfono")}</td>
				<td>${_("Lugar y Hora de Contacto")}</td>
			</tr>
			<tr>
				<td>${case.polissa_id.titular.name or ''}</td>
				<td>${case.polissa_id.titular.address[0].phone or ''}</td>
				<td>&nbsp;</td>
			</tr>
			<tr class="table_title">
				<td colspan="3">${_("Descripción del trabajo a realizar")}</td>
			</tr>
			<tr>
				<td id="observacions" colspan="3">
					${get_observacions(case, 'generales') or ''}
				</td>
			</tr>
		</table>
		<br>
		<div id="peu_informe">
			<table>
				<tr class="table_title">
					<td colspan="2">${_("Informe Trabajos Realizados")}</td>
				</tr>
				<tr>
					<td class="table_title" style="width: 100px;">${_("Asignado a")}</td>
					<td>&nbsp;</td>
				</tr>
			</table>
			<table id="informe" style="position: relative; top: -1px;">
				<tr class="table_title">
					<td>${_("Fecha")}</td>
					<td>${_("Horas")}</td>
					<td>${_("Fecha")}</td>
					<td>${_("Horas")}</td>
					<td>${_("Fecha")}</td>
					<td>${_("Horas")}</td>
					<td>${_("Total")}</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<td colspan="7" id="observacions_informe">
						${get_observacions(case, 'operario') or ''}
				</td>
			</table>
		</div>
		<div id="firma">
			<table id="taula_firma">
				<tr>
					<td><b>${_("Firma operario")}</b></td>
				</tr>
				<tr>
					<td style="height: 119px;">&nbsp;</td>
				</tr>
				<tr>
					<td><b>${_("Fecha")}</b></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
			</table>
		</div>
            <%
                comptador_casos += 1;
            %>
            % if comptador_casos <len(objects):
                <p style="page-break-after:always"></p>
            % endif
		%endfor
		</body>
</html>
