# -*- coding: utf-8 -*-
{
    "name": "polissa_ccc_chilla",
    "description": """Afegeix el camp sequence a la vista del CCC del partner""",
    "version": "0-dev",
    "author": "GISCE Enginyeria, SL",
    "category": "Partner",
    "depends":[
        "base",
        "giscedata_polissa"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "polissa_ccc_chilla_view.xml",
        "res_partner_view.xml"
    ],
    "active": False,
    "installable": True
}
