# -*- coding: utf-8 -*-
from osv import osv, fields

class ResPartnerBank(osv.osv):

 _name = 'res.partner.bank'
 _inherit = 'res.partner.bank'

 _columns = {
  'phone': fields.char('Phone', size=64),
  'mobile': fields.char('Mobile', size=64)
 }

ResPartnerBank()
