# -*- coding: utf-8 -*-
from osv import osv, fields


class ResPartnerAddress(osv.osv):
    """Direccions dels partners semblants als CUPS.
    """
    _name = 'res.partner.address'
    _inherit = 'res.partner.address'

    # El que farem és sobreescriure el mètode write per tal de que quan es guardi
    # escriurem al camp 'street' la composició dels diferents camps.

    # TODO: Fer on_change d'alguns camps per autocompletar.

    def _trg_street(self, cursor, uid, ids, context=None):
        """Trigger que es cridarà per tal de tornar a calcular l'street.
        """
        return ids

    def _street(self, cursor, uid, ids, field_name, arg, context=None):
        """Mètode write, per sobreescriure els camps originals.
        """
        res = {}
        for address in self.browse(cursor, uid, ids, context):
            comp = []
            if address.tv and address.tv.abr:
                comp.append(address.tv.abr)
                comp.append('. ')
            if address.nv:
                comp.append(address.nv)
            if address.pnp:
                comp.append(', ')
                comp.append(address.pnp)
            if address.portal:
                comp.append(' ')
                comp.append(address.portal)
            if address.es:
                comp.append(' ')
                comp.append(address.es)
            if address.pt:
                comp.append(' ')
                comp.append(address.pt)
            if address.pu:
                comp.append(' ')
                comp.append(address.pu)
            if address.aclarador:
                comp.append(' - ')
                comp.append(address.aclarador)
            if address.duplicador:
                comp.append(' - ')
                comp.append(address.duplicador)
            street = ''.join(map(unicode, comp))
            res[address.id] = street
        return res

    _columns = {
        'duplicador': fields.char('Duplicador', size=256),
        'portal': fields.char('Portal', size=64),
        'street': fields.function(_street, method=True, string="Street",
                                type="char", size=128, readonly=True,
                                store={'res.partner.address': (
                                       _trg_street, ['tv', 'nv', 'pnp',
                                                     'es', 'pt', 'pu', 'portal',
                                                     'cpo', 'cpa', 'aclarador',
                                                     'duplicador'],
                                                     10)}),
        'id_municipi': fields.many2one('res.municipi', 'Municipi'),
        'id_poblacio': fields.many2one('res.poblacio', 'Població')
    }

ResPartnerAddress()
