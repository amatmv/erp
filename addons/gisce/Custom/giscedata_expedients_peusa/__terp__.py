# -*- coding: utf-8 -*-
{
    "name": "Expedients Peusa (Custom)",
    "description": """
    This module provide :
      * Tipus de plànols pels expedients predefinits
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Custom",
    "depends":[
        "base",
        "giscedata_expedients"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_expedients_peusa_data.xml"
    ],
    "active": False,
    "installable": True
}
