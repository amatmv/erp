# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* giscedata_expedients_peusa
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.14\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2017-10-16 18:05\n"
"PO-Revision-Date: 2017-10-16 18:05\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: giscedata_expedients_peusa
#: model:ir.module.module,shortdesc:giscedata_expedients_peusa.module_meta_information
msgid "Expedients Peusa (Custom)"
msgstr ""

#. module: giscedata_expedients_peusa
#: model:ir.module.module,description:giscedata_expedients_peusa.module_meta_information
msgid "\n"
"    This module provide :\n"
"      * Tipus de plànols pels expedients predefinits\n"
"    "
msgstr ""

