# -*- coding: utf-8 -*-

from osv import fields, osv

class giscedata_descarrecs_descarrec(osv.osv):
  _name = 'giscedata.descarrecs.descarrec'
  _inherit = 'giscedata.descarrecs.descarrec'


  def _cts_afectats_obj(self, cr, uid, ids, name, arg, context={}):
    res = {}
    for id in ids:
      cr.execute("SELECT ct.id, i.code_ct, ct.descripcio from giscedata_descarrecs_descarrec_installacions i, giscedata_cts ct, giscedata_descarrecs_descarrec d where i.code_ct = ct.name and i.descarrec_id = %i group by ct.id, i.code_ct, ct.descripcio order by i.code_ct asc", (id,))
      res[id] = cr.dictfetchall()
    return res

  def _cts_afectats(self, cr, uid, ids, name, arg, context={}):
    res = {}
    cts = self._cts_afectats_obj(cr, uid, ids, name, arg, context)
    for i in cts.keys():
      string = ''
      for ct in cts[i]:
        string += '%s "%s"\n' % (ct['code_ct'], ct['descripcio'])
      res[i] = string
    return res

  def _adreces_afectades(self, cr, uid, ids, name, arg, context={}):
    res = {}
    # agafem els cts que hi ha afectats
    cts = self._cts_afectats_obj(cr, uid, ids, name, arg, context)
    for i in cts.keys():
      string = ''
      for ct in cts[i]:
        cr.execute("select adreca from giscedata_cts where id = %s" % (ct['id']))
        p = cr.dictfetchone()
        string += "%s\n" % (p['adreca'],)
      res[i] = string
    return res

  def _poblacions_afectades(self, cr, uid, ids, name, arg, context={}):
    res = {}
    # agafem els cts que hi ha afectats
    cts = self._cts_afectats_obj(cr, uid, ids, name, arg, context)
    for i in cts.keys():
      string = ''
      for ct in cts[i]:
        cr.execute("select poblacio from giscedata_cts where id = %s" % (ct['id']))
        p = cr.dictfetchone()
        string += "%s\n" % (p['poblacio'],)
      res[i] = string
    return res

  _columns = {
    'area': fields.text('Àrea de distribució'),
    'causes': fields.text('Descripció dels treballs'),
    'poblacions': fields.text('Població'),
    'adreces': fields.text('Adreça'),
    'poblacions_afectades': fields.function(_poblacions_afectades, type='text', string='Poblacions afectades', method=True),
    'adreces_afectades': fields.function(_adreces_afectades, type='text', string='Adreces afectades', method=True),
    'p_paragraf': fields.text('Primer paràgraf'),
  }

giscedata_descarrecs_descarrec()
