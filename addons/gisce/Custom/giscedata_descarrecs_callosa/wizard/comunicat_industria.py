# -*- coding: utf-8 -*-
import wizard
import pooler


# Carreguem el text que ja està guardat a la base de dades
def _init(self, cr, uid, data, context={}):
  descarrec_obj = pooler.get_pool(cr.dbname).get('giscedata.descarrecs.descarrec')
  descarrec = descarrec_obj.browse(cr, uid, data['id'], context)
  causes = descarrec.causes
  if not causes:
    causes = descarrec.descripcio
  return {
    'causes': causes,
  }

_init_form = """<?xml version="1.0"?>
<form string="Comunicado Indústria">
  <separator string="... para realizar ..." colspan="4" />
  <field name="causes" nolabel="1" colspan="4" width="500" height="100"/>
</form>"""

_init_fields = {
  'causes': {'string': 'Causas', 'type': 'text'},
}

# Guardem el text que ens han introduït amb el formulari
def _save_text(self, cr, uid, data, context={}):
  descarrec_obj = pooler.get_pool(cr.dbname).get('giscedata.descarrecs.descarrec')
  descarrec_obj.write(cr, uid, [data['id']], {'causes': data['form']['causes']})
  return {}


def _print(self, cr, uid, data, context={}):
  return {'ids': [data['id']]}
  

class giscedata_descarrecs_comunicat_industria(wizard.interface):

  states = {
    'init': {
    	'actions': [_init],
      'result': {'type': 'form', 'arch': _init_form, 'fields': _init_fields, 'state': [('end', 'Cancelar', 'gtk-cancel'), ('save_text', 'Imprimir', 'gtk-print')]}
    },
    'save_text': {
    	'actions': [_save_text],
    	'result': {'type': 'state', 'state': 'print'}
    },
    'print': {
      'actions': [_print],
      'result': {'type': 'print', 'report':
      'giscedata.descarrecs.comunicat_industria', 'get_id_from_action':True, \
      'state':'end'}
    },
  }
giscedata_descarrecs_comunicat_industria('giscedata.descarrecs.callosa.comunicat.industria')
