# -*- coding: utf-8 -*-
import wizard
import pooler

# Carreguem el text que ja està guardat a la base de dades
def _init(self, cr, uid, data, context={}):
  descarrec_obj = pooler.get_pool(cr.dbname).get('giscedata.descarrecs.descarrec')
  descarrec = descarrec_obj.browse(cr, uid, data['id'], context)
  res = {
    'p_paragraf': descarrec.p_paragraf,
    'carrers_afectats': descarrec.carrers_afectats,
  }

  if data.has_key('carrers_afectats') and data['carrers_afectats']:
    res.update({'carrers_afectats': data['carrers_afectats'],})

  return res

_init_form = """<?xml version="1.0"?>
<form string="Comunicado Calle">
  <separator string="... les comunica que debido a ..." colspan="4"/>
  <field name="p_paragraf" nolabel="1" colspan="4" width="600" height="100"/>
  <separator string=", el próximo ...." colspan="4"/>
  <separator string="Calles afectadas" colspan="4" />
  <field name="carrers_afectats" nolabel="1" colspan="4" width="600" height="400"/>
</form>"""

_init_fields = {
  'carrers_afectats': {'string': 'Carrers afectats', 'type': 'text'},
  'p_paragraf': {'string': 'Primer paràgraf', 'type': 'text', 'required': True},
}

def _generar_carrers(self, cr, uid, data, context={}):
  descarrec_obj = pooler.get_pool(cr.dbname).get('giscedata.descarrecs.descarrec')
  descarrec = descarrec_obj.browse(cr, uid, data['id'], context)
  # Generem el text de carrers afectats si no hi ha res guardat
  carrers = {}
  municipis = {}
  m = {}
  for client in descarrec.clients:
    if not m.has_key(client.codeine):
      m[client.codeine] = {}
    try:
      if not m[client.codeine].has_key(client.street):
        m[client.codeine][client.street] = []
      try:
        if int(client.number) and client.number not in m[client.codeine][client.street]:
          m[client.codeine][client.street].append(client.number)
      except:
        pass
    except:
      pass

  carrers_str = []
  for municipi in m.keys():
    if len(m[municipi]):
      for c in m[municipi].keys():
        if len(m[municipi][c]):
          carrers_str.append("%s, nº%s" % (c, ', '.join(map(str, sorted(map(int,m[municipi][c])))))) 
        else:
          carrers_str.append("%s" % (c,))

  data['carrers_afectats'] = '\n'.join(carrers_str)
  return {}

# Guardem el text que ens han introduït amb el formulari
def _save_text(self, cr, uid, data, context={}):
  descarrec_obj = pooler.get_pool(cr.dbname).get('giscedata.descarrecs.descarrec')
  descarrec_obj.write(cr, uid, [data['id']], {'carrers_afectats': data['form']['carrers_afectats'], 'p_paragraf': data['form']['p_paragraf'].upper()})
  return {}


def _print(self, cr, uid, data, context={}):
  return {'ids': [data['id']]}
  

class giscedata_descarrecs_comunicat_carrer(wizard.interface):

  states = {
    'init': {
    	'actions': [_init],
      'result': {'type': 'form', 'arch': _init_form, 'fields': _init_fields, 'state': [('end', 'Cancelar', 'gtk-cancel'), ('generar_carrers', 'Generar texto calles', 'gtk-refresh'), ('save_text', 'Imprimir', 'gtk-print')]}
    },
    'generar_carrers': {
      'actions': [_generar_carrers],
      'result': {'type': 'state', 'state': 'init'},
    },
    'save_text': {
    	'actions': [_save_text],
    	'result': {'type': 'state', 'state': 'print'}
    },
    'print': {
      'actions': [_print],
      'result': {'type': 'print', 'report':
      'giscedata.descarrecs.comunicat_carrer', 'get_id_from_action':True, \
      'state':'end'}
    },
  }
giscedata_descarrecs_comunicat_carrer('giscedata.descarrecs.callosa.comunicat.carrer')
