# -*- coding: utf-8 -*-
{
    "name": "giscedata_descarrecs",
    "description": """GISCE Descarrecs per PEUSA""",
    "version": "0-dev",
    "author": "GISCE Enginyeria, SL",
    "category": "Descarrecs",
    "depends":[
        "giscedata_descarrecs"
    ],
    "init_xml":[
        "giscedata_descarrecs_callosa_report.xml",
        "giscedata_descarrecs_callosa_wizard.xml",
        "giscedata_descarrecs_view.xml"
    ],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
