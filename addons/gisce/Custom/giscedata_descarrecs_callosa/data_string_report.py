# -*- coding: utf-8 -*-
from report import print_fnc

def _data_string_es(arg):
  import locale
  import time
  locale.setlocale(locale.LC_TIME, 'es_ES.UTF-8')
  s = time.strftime("%e de %B de %Y")
  u = unicode( s, "utf-8" )
  return u

print_fnc.functions['data_string_es'] = _data_string_es

