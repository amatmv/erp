<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

  <xsl:template match="/">
    <xsl:apply-templates select="descarrec_list" />
  </xsl:template>
  
  <xsl:template match="descarrec_list">
      <document>
      <template pageSize="(21cm, 29.7cm)">
        <pageTemplate id="main">
          <frame id="first" x1="1cm" y1="1cm" width="19cm" height="220mm"/>
        </pageTemplate>
      </template>
      
      <stylesheet>
      
      <paraStyle name="text"
      		fontName="Helvetica"
      		fontSize="10" />
      <paraStyle name="btext"
      		fontName="Helvetica-Bold"
      		fontSize="10" />
      <paraStyle name="rtext"
          fontName="Helvetica"
          fontSize="10"
          alignment="RIGHT"/>

      <paraStyle name="ctext"
          fontName="Helvetica"
          fontSize="10"
          alignment="CENTER"/>

      </stylesheet>
    
      <story>
      <xsl:apply-templates select="descarrec/clients/polissa[avis='1']"/>
      </story>
      </document>
  </xsl:template>

  <xsl:template match="polissa">
        <para style="rtext">
          <xsl:value-of select="descarrec_address/name"/><br/>
          <xsl:value-of select="descarrec_address/street"/><br/>
          <xsl:value-of select="descarrec_address/street2"/><br/>
          <xsl:value-of select="concat(descarrec_address/zip, ' ', descarrec_address/city)"/><br/>
          <br/>
          </para>
        <para style="text">
        Muy Sr. Nuestro.<br/>
        <br/>
        Por la presente le comunicamos que por motivos de <xsl:value-of select="../../p_paragraf"/>, el pr�ximo <xsl:value-of select="concat(substring(../../data_inici, 9, 2),'/',substring(../../data_inici, 6, 2),'/',substring(../../data_inici, 1, 4))"/> se va a producir un corte en el suministro el�ctrico entre las <xsl:value-of select="substring(../../data_inici, 12, 5)"/> y las <xsl:value-of select="substring(../../data_final, 12, 5)"/> horas, el cual afectar� a su empresa.<br/>
        <br/>
        Sin otro particular aprovecho la ocasi�n para saludarle atentamente, en <xsl:value-of select="//corporate-header/corporation/address[type='default']/city" />, a <xsl:value-of select="../../data_string_es" />.
        </para>
        <spacer length="4cm"/>
        <para style="ctext">Fdo. Antionio Fuster Garri</para>
        <para style="ctext">Director T�cnico</para>
        <spacer length="3cm"/>
        <para style="text">Recib�:</para>
        <spacer length="3cm"/>
        <para style="text">Fdo.:</para>
        <xsl:if test="not(position() = last())"><nextFrame/></xsl:if>
    </xsl:template>

</xsl:stylesheet>
