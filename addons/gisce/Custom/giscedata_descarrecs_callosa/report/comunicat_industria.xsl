<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="descarrec_list" />
  </xsl:template>
  
  <xsl:template match="descarrec_list">
    <xsl:apply-templates select="descarrec" />
  </xsl:template>

  <xsl:key name="carrers" match="//*/client/carrer" use="."/>

  <xsl:template match="descarrec">
    <document>
      <template pageSize="(21cm, 29.7cm)">
      	<pageTemplate id="main">
      		<frame id="first" x1="1cm" y1="1cm" width="19cm" height="247mm"/>
      	</pageTemplate>
        <pageTemplate id="cap">
          <pageGraphics>
          	<image file="addons/custom/logo.png" width="20mm" height="20mm" x="1cm" y="26.6cm" />
          	<setFont name="Helvetica-Bold" size="30" />
          	<drawString x="3.2cm" y="27.8cm"><xsl:value-of select="//corporate-header/corporation/rml_header1" /></drawString>
          	<setFont name="Helvetica" size="10" />
          	<drawString x="3.2cm" y="27.1cm"><xsl:value-of select="//corporate-header/corporation/rml_footer1" /></drawString>
          	<lineMode width="2" />
          	<stroke color="blue"/>
          	<lines>
          		3.2cm 27cm 20cm 27cm
          	</lines>
          	<setFont name="Helvetica" size="8" />
          	<drawString x="3.2cm" y="26.6cm"><xsl:value-of select="//corporate-header/corporation/name" /></drawString>
          	<drawRightString x="20cm" y="26.7cm"><xsl:value-of select="//corporate-header/corporation/address[type='default']/street" /></drawRightString>
          	<drawRightString x="20cm" y="26.4cm"><xsl:value-of select="concat(//corporate-header/corporation/address[type='default']/zip, ' ', //corporate-header/corporation/address[type='default']/city)" /> (<xsl:value-of select="//corporate-header/corporation/address[type='default']/state" />)</drawRightString>
          	<drawRightString x="20cm" y="26.1cm">Tel. <xsl:value-of select="//corporate-header/corporation/address[type='default']/phone" /> - Fax <xsl:value-of select="//corporate-header/corporation/address[type='default']/fax" /></drawRightString>
          	<drawRightString x="20cm" y="25.8cm">E-mail: <xsl:value-of select="//corporate-header/corporation/address[type='default']/email" /></drawRightString>
          	<rotate degrees="90" />
            <setFont name="Helvetica" size="7" />
            <drawString x="60mm" y="-4mm"><xsl:value-of select="//corporate-header/corporation/rml_footer2" /></drawString>
          </pageGraphics>
          <frame id="first" x1="1cm" y1="1cm" width="19cm" height="210mm"/>
        </pageTemplate>
      </template>
      
      <stylesheet>
      
      <paraStyle name="text"
      		fontName="Helvetica"
      		fontSize="10" />

      <paraStyle name="taula"
        fontName="Helvetica"
        fontSize="8" />

      <paraStyle name="rtext"
        fontName="Helvetica"
        fontSize="10"
        alignment="RIGHT"/>
      <paraStyle name="ctext"
        fontName="Helvetica"
        fontSize="10"
        alignment="CENTER"/>

      <blockTableStyle id="taula_descarrec">
         <blockFont name="Helvetica-Bold" start="0,0" stop="-1,0" />
	 <blockFont name="Helvetica" start="0,1" stop="-1,-1" />
	 <blockValign value="TOP" />
	 <lineStyle kind="LINEBELOW" colorName="black" thickness="1" start="0,0" stop="-1,0"/>
      </blockTableStyle>

      </stylesheet>
    
      <story>
        <para style="rtext">
        DIRECCI�N GENERAL DE LA ENERG�A<br/>
        C. Rambla M�ndez N��ez, 41-5�<br/>
        03002 - Alicante<br/>
        </para>
        <spacer length="1cm"/>
        <para style="text">
        Muy Sr. Nuestro:
        </para>
        <spacer length="0.5cm"/>
        <para style="text">
        Por la presente le solicitamos un corte programado seg�n el art�culo 101.3 del R.D. 1955/2000 de 1 de diciembre del 2000, para realizar <xsl:value-of select="causes"/>.
        </para>
        <spacer length="0.5cm"/>
        <para style="text">
        El corte se efectuar� el dia siguiente y afectar� a los siguientes abonados:
        </para>
      	<spacer length="2cm" />

        
	<blockTable 
	  style="taula_descarrec"
	  colWidths="4cm,4cm,6cm,3cm,3cm">
    <tr>
	    <td><para style="taula">Fecha Corte</para></td>
      <td><para style="taula">Fecha Reanudaci�n</para></td>
      <td><para style="taula">Calles afectadas</para></td>
      <td><para style="taula">Cli. Afectados</para></td>
      <td><para style="taula">Potencia (kW)</para></td>
	  </tr>
    <tr>
      <td><para style="taula"><xsl:value-of select="data_inici"/></para></td>
      <td><para style="taula"><xsl:value-of select="data_final"/></para></td>
      <td><para style="taula">
      <xsl:for-each select="client/carrer[generate-id() = generate-id(key('carrers', . ))]">
        <xsl:value-of select="."/>
        <xsl:if test="not(position() = last())">,</xsl:if>
      </xsl:for-each>
      </para></td>
      <td><para style="taula"><xsl:value-of select="count(client/cups)"/></para></td>
      <td><para style="taula"><xsl:value-of select="sum(client/potencia)"/></para></td>
    </tr>
  </blockTable>

  <spacer length="3cm"/>
 	<para style="ctext"><xsl:value-of select="//corporate-header/corporation/address[type='default']/city"/>, <xsl:value-of select="data"/>. </para>
  <spacer length="2cm"/>
  <para style="ctext">Fdo.- Antonio Fuster Garri.</para>
  <para style="ctext">Director T�cnico</para>
</story>
</document>

</xsl:template>

</xsl:stylesheet>
