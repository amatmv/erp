<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="descarrec_list" />
  </xsl:template>
  
  <xsl:template match="descarrec_list">
    <xsl:apply-templates select="descarrec" />
  </xsl:template>

  <xsl:template match="descarrec">
    <document>
      <template pageSize="(21cm, 29.7cm)">
        <pageTemplate id="main">
          <pageGraphics>
          	<image file="addons/custom/logo.png" width="20mm" height="20mm" x="1cm" y="26.6cm" />
          	<setFont name="Helvetica-Bold" size="30" />
          	<drawString x="3.2cm" y="27.8cm"><xsl:value-of select="//corporate-header/corporation/rml_header1" /></drawString>
          	<setFont name="Helvetica" size="10" />
          	<drawString x="3.2cm" y="27.1cm"><xsl:value-of select="//corporate-header/corporation/rml_footer1" /></drawString>
          	<lineMode width="2" />
          	<stroke color="blue"/>
          	<lines>
          		3.2cm 27cm 20cm 27cm
          	</lines>
          	<setFont name="Helvetica" size="8" />
          	<drawString x="3.2cm" y="26.6cm"><xsl:value-of select="//corporate-header/corporation/name" /></drawString>
          	<drawRightString x="20cm" y="26.7cm"><xsl:value-of select="//corporate-header/corporation/address[type='default']/street" /></drawRightString>
          	<drawRightString x="20cm" y="26.4cm"><xsl:value-of select="concat(//corporate-header/corporation/address[type='default']/zip, ' ', //corporate-header/corporation/address[type='default']/city)" /> (<xsl:value-of select="//corporate-header/corporation/address[type='default']/state" />)</drawRightString>
          	<drawRightString x="20cm" y="26.1cm">Tel. <xsl:value-of select="//corporate-header/corporation/address[type='default']/phone" /> - Fax <xsl:value-of select="//corporate-header/corporation/address[type='default']/fax" /></drawRightString>
          	<drawRightString x="20cm" y="25.8cm">E-mail: <xsl:value-of select="//corporate-header/corporation/address[type='default']/email" /></drawRightString>
          	<rotate degrees="90" />
            <setFont name="Helvetica" size="7" />
            <drawString x="60mm" y="-4mm"><xsl:value-of select="//corporate-header/corporation/rml_footer2" /></drawString>
          </pageGraphics>
          <frame id="first" x1="1cm" y1="1cm" width="19cm" height="220mm"/>
        </pageTemplate>
      </template>
      
      <stylesheet>
      
      <paraStyle name="text"
      		fontName="Helvetica"
      		fontSize="10" />
      <paraStyle name="btext"
      		fontName="Helvetica-Bold"
      		fontSize="10" />

      <paraStyle name="gtext"
          fontName="Helvetica"
          fontSize="18"
          leading="22"/>
      </stylesheet>
    
      <story>
        <para style="gtext">EL�CTRICA DE CALLOSA DE SEGURA, CV LES COMUNICA QUE DEBIDO A <xsl:value-of select="p_paragraf"/>, EL PR�XIMO <xsl:value-of select="concat(substring(data_inici, 9, 2),'/',substring(data_inici, 6, 2),'/',substring(data_inici, 1, 4))"/> SE PROCEDER� A REALIZAR UN CORTE EN EL SUMINISTRO EL�CTRICO DESDE LAS <xsl:value-of select="substring(data_inici, 12, 5)"/> A LAS <xsl:value-of select="substring(data_final, 12, 5)"/>.</para>
        <spacer length="0.5cm"/>
        <para style="text">EL CORTE AFECTAR� A LAS SIGUIENTES CALLES:</para>
      	<xpre style="btext"><xsl:value-of select="carrers_afectats" /></xpre>
      	<spacer length="0.5cm" />
      	<para style="text"><xsl:value-of select="//corporate-header/corporation/address[type='default']/city" />, a <xsl:value-of select="data_string_es" /></para>
      </story>
    </document>

    </xsl:template>

</xsl:stylesheet>
