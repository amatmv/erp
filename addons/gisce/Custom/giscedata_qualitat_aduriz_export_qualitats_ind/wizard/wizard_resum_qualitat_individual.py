# -*- coding: utf-8 -*-
try:
    from cStringIO import StringIO
except:
    from StringIO import StringIO
import csv
import base64


from osv import osv, fields
from tools import config
from tools.translate import _


class WizardResumQualitatIndividual(osv.osv_memory):

    _name = 'wizard.resum.qualitat.individual'

    _columns = {
        'year': fields.char('Any', size=5),
        'tipus': fields.selection([('tiepi', _('individual hores')),
                                   ('niepi', _(u'individual incidències'))],
                                  _('Tipus resum')),
        'status': fields.text('Resultat del resum'),
        'resum_fitxer': fields.binary('Fitxer'),
        'nom_fitxer': fields.char('Nom fitxer', size=50)
    }

    def action_resum_qualitat(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        try:
            #Query amb els camps que necessitem
            wiz = self.browse(cursor, uid, ids[0])
            path_base = (config['addons_path'] + '/'
                         'giscedata_qualitat_aduriz_export_qualitats_ind/sql')
            if wiz.tipus == 'tiepi':
                query_file = path_base + '/resum_qualitat_individual_tiepi.sql'
            else:
                query_file = path_base + '/resum_qualitat_individual_niepi.sql'

            query = open(query_file).read()
            anyinici = '%s-01-01' % wiz.year
            anyfinal = '%s-01-01' % (int(wiz.year) + 1)
            cursor.execute(query, (wiz.year, anyinici, anyfinal))

            res = cursor.fetchall()
            if len(res) == 0:
                wiz.write({'status': _('No s\'ha creat el fitxer perquè '
                                       'no hi ha registres amb dret a descompte.')})
                return True
            output = StringIO()
            fout = csv.writer(output, delimiter=';', quoting=csv.QUOTE_ALL)
            for linia in res:
                fout.writerow(linia)

            mfile = base64.b64encode(output.getvalue().encode('utf-8'))
            filename = _('qualitat_invidual_%s_%s.csv') % (wiz.tipus, wiz.year)
            wiz.write({'resum_fitxer': mfile,
                       'nom_fitxer': filename,
                       'status': _('Fitxer generat correctament')})
            output.close()
        except Exception, e:
            sentry = self.pool.get('sentry.setup')
            if sentry is not None:
                sentry.client.captureException()
            wiz.write({'status': _(u'Error en la generació')})
        return True

WizardResumQualitatIndividual()