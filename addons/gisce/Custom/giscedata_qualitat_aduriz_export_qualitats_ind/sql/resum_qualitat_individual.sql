select
  cups,
  polissa,
  czone.name,
  case when (tarifa = '3.1' or tarifa like '6.') then
    czoneq.at_hores * 3600
  else
    czoneq.bt_hores * 3600
  end as limit_temps_zona,
  case when (tarifa = '3.1' or tarifa like '6.') then
    czoneq.at_interrupcions
  else
    czoneq.bt_interrupcions end
  as limit_talls_zona,
  nom_incidencia,
  data_inici,
  data_final,
  duracio,
  tipus_tall,
  case when dret_descompte then
    'SI'
  else
    'No'
  end as descompte
from
(
  select cups, polissa, tarifa, zona, tipus_tall, nom_incidencia, data_inici, data_final, duracio, dret_descompte from
  (
    select
      tl.cups,
      tl.polissa,
      tl.tarifa,
      tl.zona as zona,
      tl.tipus_tall,
      ts.nom_incidencia,
      ts.data_inici,
      ts.data_final,
      ts.duracio,
      ts.dret_descompte
    from
      giscedata_qualitat_individual_tiepi_span ts,
      giscedata_qualitat_individual_tiepi_linia tl,
      giscedata_qualitat_individual_tiepi it
    where
      tl.year = it.id
      and ts.linia = tl.id
      and it.name=%s
      and tl.dret_descompte is true
    union all
    select
      tl.cups,
      tl.polissa,
      tl.tarifa,
      tl.zona as zona,
      tl.tipus_tall,
      ts.nom_incidencia,
      ts.data_inici,
      ts.data_final,
      ts.duracio,
      ts.dret_descompte
    from
      giscedata_qualitat_individual_niepi_span ts,
      giscedata_qualitat_individual_niepi_linia tl,
      giscedata_qualitat_individual_niepi it
    where
      tl.year = it.id
      and ts.linia = tl.id
      and it.name=%s
      and tl.dret_descompte is true
  ) total
  group by cups, polissa, tarifa, zona, tipus_tall, nom_incidencia, data_inici, data_final, duracio, dret_descompte
) interrupcions,
giscedata_cts_zona czone,
giscedata_cts_zona_qualitat czoneq
where zona = czone.id and czone.id = czoneq.zona_id
order by cups;
