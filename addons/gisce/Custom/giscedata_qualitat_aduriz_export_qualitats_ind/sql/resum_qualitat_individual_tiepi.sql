select
  c.cups as CUPS,
  c.policy as Poliza,
  czone.name as Zona,
case when (c.pricelist = '3.1' or c.pricelist like '6.') then
  czoneq.at_hores * 3600
else
  czoneq.bt_hores * 3600
end as limit_temps_zona,
case when (c.pricelist = '3.1' or c.pricelist like '6.') then
    czoneq.at_interrupcions
  else
    czoneq.bt_interrupcions end
  as limit_talls_zona,
  i.name as incidencia_nom,
  s.begin_date as data_inici_inter,
  s.end_date as data_final_inter,
  extract('epoch' from s.end_date - s.begin_date)::int as temps_total_inter,
  tip.name as tipus_tall,
  cau.name as causa_nom,
  case when (tip.code != 1 and cau.code = 7) THEN
    'SI'
  ELSE
    'NO'
  end as dret_devolucio
from
  giscedata_qualitat_span s,
  giscedata_qualitat_affected_customer c,
  giscedata_qualitat_incidence i,
  giscedata_qualitat_cause cau,
  giscedata_qualitat_type tip,
  giscedata_cts_zona czone,
  giscedata_cts_zona_qualitat czoneq,
  (select tl.cups, tl.polissa
    from
     giscedata_qualitat_individual_tiepi_linia tl,
      giscedata_qualitat_individual_tiepi it
    where
      tl.year = it.id
      and it.name=%s
      and tl.dret_descompte is true
  ) tiepi_ind
where tiepi_ind.cups = c.cups and c.policy = tiepi_ind.polissa
and s.id = c.span_id
and s.incidence_id = i.id
and s.begin_date >= %s
and s.end_date < %s
and i.type_id = tip.id
and i.cause_id = cau.id
and extract('epoch' from s.end_date - s.begin_date) > 180
and c.zone_ct_id = czone.id
and czone.id = czoneq.zona_id
order by c.cups, c.policy, i.name