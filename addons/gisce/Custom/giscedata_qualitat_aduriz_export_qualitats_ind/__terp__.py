# -*- coding: utf-8 -*-
{
    "name": "GISCE Qualitat Aduriz Export TIEPI i NIEPI",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Custom",
    "depends":[
        "giscedata_qualitat"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/wizard_resum_qualitat_individual_view.xml",
        "giscedata_qualitat_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
