# -*- coding: utf-8 -*-
{
    "name": "lectures_obs_chilla",
    "description": """Afegeix el camp observacions a la vista de lectures""",
    "version": "0-dev",
    "author": "GISCE Enginyeria, SL",
    "category": "Partner",
    "depends":[
        "base",
        "giscedata_lectures"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "lectures_obs_chilla_view.xml"
    ],
    "active": False,
    "installable": True
}
