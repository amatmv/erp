# -*- coding: utf-8 -*-
{
    "name": "preus de transferencia",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Preus de transferencia
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "giscedata_polissa",
        "product",
        "giscedata_tarifas_pagos_capacidad_2010",
        "giscedata_facturacio"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/ir.model.access.csv",
        "giscedata_tp_data.xml"
    ],
    "active": False,
    "installable": True
}
