# -*- coding: utf-8 -*-
from osv import osv, fields
from datetime import datetime


class GiscedataTpPeriodsK(osv.osv):
    _name = 'giscedata.tp.periods.k'

    def get_k(self, cursor, uid, period, context=None):
        if context is None:
            context = {}
        product_id = self.search(cursor, uid, [
            ('period_id.id', '=', period)
        ], context=context)
        if product_id:
            return product_id
        else:
            return False

    _columns = {
        'period_id': fields.many2one('giscedata.polissa.tarifa.periodes'),
        'product_id': fields.many2one('product.product')
    }

GiscedataTpPeriodsK()


class GiscedataTp(osv.osv):
    _name = 'giscedata.tp'

    def get_peatge_price_from_period_id(self, cursor, uid, period_id):

        pricelist_obj = self.pool.get('product.pricelist')
        imd_obj = self.pool.get('ir.model.data')
        period_obj = self.pool.get('giscedata.polissa.tarifa.periodes')

        period = period_obj.browse(cursor, uid, period_id)

        peatge_pl = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio',
            'pricelist_tarifas_electricidad'
        )[1]
        peatge_price = pricelist_obj.price_get(
            cursor, uid, [peatge_pl], period.product_id.id, 1,context=None
        )[peatge_pl]

        return peatge_price

    def calc_price(self, cursor, uid, period_id, pl=None, context=None):
        if not period_id:
            return 0
        k_obj = self.pool.get('giscedata.tp.periods.k')
        pricelist_obj = self.pool.get('product.pricelist')
        imd_obj = self.pool.get('ir.model.data')
        period_obj = self.pool.get('giscedata.polissa.tarifa.periodes')

        period = period_obj.browse(cursor, uid, period_id)

        if context is None or 'date' not in context:
            context = {'date': datetime.now().strftime('%Y-%m-%d')}

        k_ids = k_obj.get_k(cursor, uid, period_id, context=context)

        # Formula
        # Preu de Transferència = ((K1 + PxC) x K2) + K3) x I.M.) + Peatges

        # K
        k_prices = {}
        if pl is None:
            k_pricelist = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_tp', 'pricelist_tp'
            )[1]
        else:
            k_pricelist = pl
        for k in k_obj.browse(cursor, uid, k_ids):
            k_code = k.product_id.name.split(" ")[-1]
            k_prices[k_code] = pricelist_obj.price_get(
                cursor, uid, [k_pricelist], k.product_id.id, 1, context=context
            )[k_pricelist]

        # Pagos por capacidad
        pc_pl = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_pagos_capacidad',
            'pricelist_pagos_capacidad'
        )[1]
        pc_price = pricelist_obj.price_get(
            cursor, uid, [pc_pl], period.product_id.id, 1, context=context
        )[pc_pl]

        # Peatges
        peatge_pl = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio',
            'pricelist_tarifas_electricidad'
        )[1]
        peatge = pricelist_obj.price_get(
            cursor, uid, [peatge_pl], period.product_id.id, 1, context=context
        )[peatge_pl]

        formula = (
            (((k_prices['K1'] + pc_price) * k_prices['K2'])
                + k_prices['K3']) * (1 + 0.015)) + peatge
        return formula

GiscedataTp()