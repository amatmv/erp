# -*- coding: utf-8 -*-
{
    "name": "Gestió documental",
    "description": """Gestió documental del cens""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Estabanell",
    "depends": [
        "base_extended",
        "giscedata_projecte_obres",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "security/giscedata_gestio_documental_security.xml",
        "security/ir.model.access.csv",
        "giscedata_gestio_documental_view.xml",
        "giscedata_at_view.xml",
        "giscedata_bt_view.xml",
        "giscedata_centrestransformadors_view.xml",
        "giscedata_cens_view.xml",
        "giscedata_conveni_view.xml",
        "giscedata_encarrec_view.xml",
        "giscedata_escriptura_view.xml",
        "wizard/assignar_cens_view.xml",
    ],
    "active": False,
    "installable": True
}
