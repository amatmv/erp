# -*- coding: utf-8 -*-

from osv import osv, fields


class GiscedataBtElement(osv.osv):
    _name = "giscedata.bt.element"
    _inherit = "giscedata.bt.element"

    _columns = {
        'cens_id': fields.many2one('giscedata.cens', 'Cens')
    }


GiscedataBtElement()
