# -*- coding: utf-8 -*-

from osv import osv, fields


class GiscedataCts(osv.osv):
    _name = "giscedata.cts"
    _inherit = "giscedata.cts"

    _columns = {
        'cens_id': fields.many2one('giscedata.cens', 'Cens')
    }


GiscedataCts()
