# -*- coding: utf-8 -*-

from osv import osv, fields


class GiscedataConveni(osv.osv):
    _name = "giscedata.conveni"
    _inherit = "giscedata.conveni"
    _columns = {
        'patrimony_ids': fields.many2many(
            "giscedata.cens", "patrimony_agreement", "agreement_id",
            "patrimony_id", "Cens"
        ),
    }


GiscedataConveni()


class GiscedataCens(osv.osv):
    _name = "giscedata.cens"
    _inherit = "giscedata.cens"
    _columns = {
        'deed_ids': fields.many2many(
            "giscedata.escriptura", "patrimony_deed", "patrimony_id", "deed_id",
            "Escriptures"
        ),
        'agreement_ids': fields.many2many(
            "giscedata.conveni", "patrimony_agreement", "patrimony_id",
            "agreement_id", "Convenis"
        ),
        'assignment_ids': fields.many2many(
            "giscedata.encarrec", "patrimony_assignment", "patrimony_id",
            "assignment_id", "Encàrrecs"
        )
    }


GiscedataCens()


class GiscedataEscriptura(osv.osv):
    _name = "giscedata.escriptura"
    _inherit = "giscedata.escriptura"
    _columns = {
        'patrimony_ids': fields.many2many(
            "giscedata.cens", "patrimony_deed", "deed_id", "patrimony_id", "Cens"
        ),
    }


GiscedataEscriptura()


class GiscedataEncarrec(osv.osv):
    _name = "giscedata.encarrec"
    _inherit = "giscedata.encarrec"
    _columns = {
        'patrimony_ids': fields.many2many(
            "giscedata.cens", "patrimony_assignment", "assignment_id",
            "patrimony_id", "Cens"
        )
    }


GiscedataEncarrec()
