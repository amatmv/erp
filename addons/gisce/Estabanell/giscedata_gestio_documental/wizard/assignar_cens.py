# -*- coding: utf-8 -*-
from osv import osv, fields


INSTALACIONS = [
    ('giscedata.cts', 'Centres Transformadors')
]


class WizardAssignarCensEnInstalacions(osv.osv_memory):
    _name = 'wizard.assignar.cens.en.instalacions'
    _columns = {
        'cens_id': fields.many2one('giscedata.cens', 'Cens')
    }

    def action_assign_cens(self, cursor, uid, ids, context=None):
        model_name = context['src_model']
        model_o = self.pool.get(model_name)
        model_ids = context['active_ids']

        wiz = self.browse(cursor, uid, ids, context=context)[0]

        cens_id = wiz.cens_id.id if wiz.cens_id else False

        model_o.write(cursor, uid, model_ids, {'cens_id': cens_id})
        return {}


WizardAssignarCensEnInstalacions()
