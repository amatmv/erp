# -*- coding: utf-8 -*-

from osv import osv, fields


class GiscedataAtTram(osv.osv):
    _name = "giscedata.at.tram"
    _inherit = "giscedata.at.tram"

    _columns = {
        'cens_id': fields.many2one('giscedata.cens', 'Cens')
    }


GiscedataAtTram()
