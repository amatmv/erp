# -*- coding: utf-8 -*-

from osv import osv, fields


class GiscedataEscriptura(osv.osv):
    _name = "giscedata.escriptura"

    def _ff_attachment_field(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        res = dict.fromkeys(ids, False)
        att_obj = self.pool.get('ir.attachment')
        for res_id in ids:
            res[res_id] = att_obj.search(cursor, uid, [
                ('res_model', '=', self._name),
                ('res_id', '=', res_id)
            ])

        return res

    _columns = {
        'name': fields.integer("Número d'escriptura"),
        'date': fields.date("Data de l'escriptura"),
        'obra_id': fields.many2one("giscedata.projecte.obra", "Obra"),
        'notary': fields.char("Notari", size=64),  # <-- podria ser res.partner ?
        'notary_municipality_id': fields.many2one("res.municipi", "Municipi de la notaria"),
        'land_municipality_id': fields.many2one("res.municipi", "Municipi de la finca"),
        'land_address': fields.char("Adreça", size=64),
        'land_area': fields.float("Superficie del terreny"),
        'builded_land': fields.float("Superficie del terreny construida"),
        'acquisition_date': fields.date("Data de l'adquisicio"),    # <-- a la documentació no esta del tot clar el tipus
        'seller': fields.char("Transmissor", size=64),    # no podria ser un res.partner?
        'buyer': fields.char("Receptor", size=64),    # no podria ser un res.partner?
        'pr_code': fields.char("Codi del Registre de la Propietat", size=64),
        'pr_date': fields.date("Data del registre de la Propietat"),
        'pr_municipality_id': fields.many2one("res.municipi", "Municipi del registre de la propietat"),
        'pr_land': fields.integer("Registre Propietat de la finca"),    # depenent del que sigui, podria apuntar a giscedata.finca
        'cadastral_reference': fields.char("Referencia cadastral", size=64),
        'brief_description': fields.char("Descripció breu", size=256),
        'observations': fields.text("Observacions"),
        'attachment_ids': fields.function(
            _ff_attachment_field, method=True, string='Adjunts',
            type='one2many', relation='ir.attachment'
        ),
        # altres camps no indicats a la documentació
        'start_date': fields.date("Data alta/data inici"),
        'end_date': fields.date("Data baixa/data final")
    }

    _sql_constraints = [
        (
            'name_unique',
            'unique (name)',
            'El número d\'escriptura ha de ser únic.'
        ),
    ]


GiscedataEscriptura()
