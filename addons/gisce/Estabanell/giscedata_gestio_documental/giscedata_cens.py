# -*- coding: utf-8 -*-

from osv import osv, fields


class GiscedataCens(osv.osv):
    _name = "giscedata.cens"

    def _ff_attachment_field(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        res = dict.fromkeys(ids, False)
        att_obj = self.pool.get('ir.attachment')
        for res_id in ids:
            res[res_id] = att_obj.search(cursor, uid, [
                ('res_model', '=', self._name),
                ('res_id', '=', res_id)
            ])

        return res

    def _ff_many2many_field(self, cursor, uid, ids, field_name, arg, context=None):
        """
        Obté el valor pel camp "Transmitent".
        :param field_name: Name of the field to be calculated.
        :type field_name: str
        :param arg: idk
        :return: Empty transmitter for all the cens registers.
        :rtype: dict
        """
        return dict.fromkeys(ids, False)

    def _ff_srch_transmitter_field(self, cursor, uid, obj, name, args, context=None):
        """
        Obté els censos que tenen algun conveni el qual té com a transmitent
        el valor buscat.
        :param obj: self.
        :type obj: <giscedata.cens> object.
        :param name: Name of the field that is being filtered by.
        :type name: str
        :param args: search domain.
        :type args: [(str, str, Obj)]
        :return: domain to filter the cens registers.
        :rtype: [(str, str, [long])]
        """
        agreement_o = self.pool.get('giscedata.conveni')
        agreement_ids = agreement_o.search(cursor, uid, args, context=context)
        return [('agreement_ids', 'in', agreement_ids)]

    def _ff_srch_seller_field(self, cursor, uid, obj, name, args, context=None):
        """
        Obté els censos que tenen alguna escriptura la qual té com a transmissor
        el valor buscat.
        :param obj: self.
        :type obj: <giscedata.cens> object.
        :param name: Name of the field that is being filtered by.
        :type name: str
        :param args: search domain.
        :type args: [(str, str, Obj)]
        :return: domain to filter the cens registers.
        :rtype: [(str, str, [long])]
        """
        agreement_o = self.pool.get('giscedata.escriptura')
        agreement_ids = agreement_o.search(cursor, uid, args, context=context)
        return [('deed_ids', 'in', agreement_ids)]

    def _ff_srch_installation_descr_field(self, cursor, uid, obj, name, args, context=None):
        """
        Obté els censos que tenen algún CT o SE els quals tinguin la descripció
        buscada.
        :param obj: <giscedata.cens> object.
        :param name: Name of the field that is being filtered by.
        :type name: str
        :param args: search domain.
        :type args: [(str, str, Obj)]
        :return: domain to filter the cens registers.
        :rtype: [(str, str, [long])]
        """
        ct_o = self.pool.get('giscedata.cts')
        se_o = self.pool.get('giscedata.cts.subestacions')

        search_value = args[0][2]
        dmn = [('descripcio', 'ilike', search_value)]

        ct_ids = ct_o.search(cursor, uid, dmn, context=context)
        se_ids = se_o.search(cursor, uid, dmn, context=context)
        ct_vs = se_o.read(cursor, uid, se_ids, ['ct_id'], context=context)
        ct_ids += [x['ct_id'][0] for x in ct_vs]

        return [('ct_ids', 'in', ct_ids)]

    def name_get(self, cursor, uid, ids, context=None):
        res = []

        vs = self.read(cursor, uid, ids, ['name', 'address', 'holder'])

        for v in vs:
            composed_name = "{} - {} - {}".format(
                v['name'] if v['name'] else "",
                v['holder'] if v['holder'] else "",
                v['address'] if v['address'] else ""
            )
            res.append((v['id'], composed_name))

        return res

    def read(self, cr, user, ids, fields=None, context=None, load='_classic_read'):
        """
        Filters the SE from the CT model.
        :param fields: fields to be read.
        :type fields: [str]
        :param load: npi
        :type load: str
        :return: The values of the fields. The ct ids are filtered by
        installacio name (other than SE)
        :rtype: [{}]
        """
        res = super(GiscedataCens, self).read(
            cr, user, ids, fields, context, load
        )
        if 'ct_ids' in fields:
            ct_o = self.pool.get('giscedata.cts')
            for patrimony_v in res:
                ct_and_se_ids = patrimony_v['ct_ids']
                dmn = [
                    ('id', 'in', ct_and_se_ids),
                    '|',
                    ('id_installacio_name', '!=', 'SE'),
                    ('id_installacio_name', '=', False)
                ]
                ct_ids = ct_o.search(cr, user, dmn, context=context)
                patrimony_v['ct_ids'] = ct_ids

        return res

    _columns = {
        'name': fields.integer("Identificador"),
        'holder': fields.char("Titular", size=64),
        'address': fields.char("Adreça", size=64),    # <-- es una adreça? poblacio? coordenades?
        'municipality_id': fields.many2one("res.municipi", "Municipi"),
        'observations': fields.text("Observacions"),
        'attachment_ids': fields.function(
            _ff_attachment_field, method=True, string='Adjunts',
            type='one2many', relation='ir.attachment'
        ),
        'obra_id': fields.many2one("giscedata.projecte.obra", "Obra"),
        # altres camps no indicats a la documentació
        'start_date': fields.date("Data alta/data inici"),
        'end_date': fields.date("Data baixa/data final"),
        'transmitter': fields.function(
            _ff_many2many_field, method=True, string='Transmitent',
            type='char', size=32, fnct_search=_ff_srch_transmitter_field
        ),
        'seller': fields.function(
            _ff_many2many_field, method=True, string='Transmissor',
            type='char', size=32, fnct_search=_ff_srch_seller_field
        ),
        'installation': fields.function(
            _ff_many2many_field, method=True, string='Instal·lació',
            type='char', size=32, fnct_search=_ff_srch_installation_descr_field
        ),
        'ct_ids': fields.one2many(
            'giscedata.cts', 'cens_id', 'Centres Transformadors',
            context={'active_test': False}
        ),
        'subestacio_ids': fields.one2many(
            'giscedata.cts.subestacions', 'cens_id', 'Subestacions',
            context={'active_test': False}
        ),
        'element_bt_ids': fields.one2many(
            'giscedata.bt.element', 'cens_id', 'Elements BT',
            context={'active_test': False}
        ),
        'tram_at_ids': fields.one2many(
            'giscedata.at.tram', 'cens_id', 'Trams AT',
            context={'active_test': False}
        ),
    }

    _sql_constraints = [
        (
            'name_unique',
            'unique (name)',
            'Els identificadors han de ser únics.'
        ),
    ]


GiscedataCens()
