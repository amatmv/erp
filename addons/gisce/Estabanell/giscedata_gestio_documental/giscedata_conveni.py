# -*- coding: utf-8 -*-

from osv import osv, fields


class GiscedataConveni(osv.osv):
    _name = "giscedata.conveni"

    def _ff_attachment_field(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        res = dict.fromkeys(ids, False)
        att_obj = self.pool.get('ir.attachment')
        for res_id in ids:
            res[res_id] = att_obj.search(cursor, uid, [
                ('res_model', '=', self._name),
                ('res_id', '=', res_id)
            ])

        return res

    _columns = {
        'name': fields.integer("Identificador"),
        'date': fields.date("Data signatura"),
        'obra_id': fields.many2one("giscedata.projecte.obra", "Obra"),
        'object': fields.text("Objecte"), # <-- exemple?
        'land_address': fields.char("Adreça", size=64),
        'municipality_id': fields.many2one("res.municipi", "Municipi"), # <-- poblacio de que? d'on s'ha signat o de la finca?
        'transmitter': fields.char("Transmitent", size=64), # <-- millor res.partner?
        'observations': fields.text("Observacions"),
        'precarious': fields.boolean("Precaris"),
        'precarious_descr': fields.char("Descripció precaris", size=32),
        'attachment_ids': fields.function(
            _ff_attachment_field, method=True, string='Adjunts',
            type='one2many', relation='ir.attachment'
        ),
        # altres camps no indicats a la documentació
        'start_date': fields.date("Data alta/data inici"), # <-- es la mateixa que la data de signatura?
        'end_date': fields.date("Data baixa/data final")
    }

    _sql_constraints = [
        (
            'name_unique',
            'unique (name)',
            'Els identificadors han de ser únics.'
        ),
    ]


GiscedataConveni()
