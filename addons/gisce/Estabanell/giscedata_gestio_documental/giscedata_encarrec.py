# -*- coding: utf-8 -*-

from osv import osv, fields


ASSIGNMENT_STATES = [
    ('pending', 'Pendent'),
    ('in_progress', 'En curs'),
    ('done', 'Finalitzat')
]


class GiscedataEncarrec(osv.osv):
    _name = "giscedata.encarrec"

    def _ff_attachment_field(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        res = dict.fromkeys(ids, False)
        att_obj = self.pool.get('ir.attachment')
        for res_id in ids:
            res[res_id] = att_obj.search(cursor, uid, [
                ('res_model', '=', self._name),
                ('res_id', '=', res_id)
            ])

        return res

    _columns = {
        'name': fields.integer("Identificador"),
        'observations': fields.text("Observacions"),         # <-- diferencia entre observacions i notes adicionals?
        'notes_adicionals': fields.text("Notes adicionals"), # <-- diferencia entre observacions i notes adicionals?
        'comments_and_current_state': fields.text("Comentaris i situacio actual"), # <-- no podria ser tambe el camp observations?
        'description': fields.text("Descripció"),
        'action': fields.text("Acció"), # <-- algun exemple?
        'budget': fields.char("Presupost", size=32), # float?
        'partner_id': fields.many2one("res.partner", "Empresa"),
        'classification': fields.char("Classificació", size=32), # exemple?
        'municipality_id': fields.many2one("res.municipi", "Municipi"),
        'attachment_ids': fields.function(
            _ff_attachment_field, method=True, string='Adjunts',
            type='one2many', relation='ir.attachment'
        ),
        'date': fields.date("Data d'encàrrec"),
        'end_date': fields.date("Data finalització"),
        'state': fields.selection(ASSIGNMENT_STATES, 'Estat')
    }

    _sql_constraints = [
        (
            'name_unique',
            'unique (name)',
            'Els identificadors han de ser únics.'
        ),
    ]


GiscedataEncarrec()
