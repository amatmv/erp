# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function, unicode_literals

from osv import osv, fields


class GiscedataProjecteObra(osv.osv):

    _name = "giscedata.projecte.obra"
    _inherit = "giscedata.projecte.obra"
    _description = 'Obra'

    def write(self, cursor, uid, ids, vals, context=None):
        """
        We override the write method to set the field Work Type to 'inversions'
        whenever the field Type of the obra is less than 1000.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param ids: <giscedata.projecte.obra> ids
        :type ids: list
        :param vals: vals to write to the <giscedata.projecte.obra>
        :type vals: dict
        :param context:
        :return: result of the write
        """
        if context is None:
            context = {}

        tipus = vals.get('tipus', False)
        if tipus and tipus < 1000:
            vals['work_type'] = 'manteniment'
        else:
            vals['work_type'] = 'inversions'

        return super(GiscedataProjecteObra, self).write(
            cursor, uid, ids, vals, context
        )

    def create(self, cursor, uid, vals, context=None):
        """
        We override the create method to set the field Work Type to 'inversions'
        whenever the field Type of the obra is less than 1000.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param vals: vals to create the <giscedata.projecte.obra>
        :type vals: dict
        :param context:
        :return: result of the write
        """
        if context is None:
            context = {}

        tipus = vals.pop('tipus', False)

        obra_id = super(GiscedataProjecteObra, self).create(cursor, uid, vals)

        if tipus:
            # We write the field manually to trigger the overrided "write"
            # method
            self.write(cursor, uid, obra_id, {
                'tipus': tipus
            })
        return obra_id

    _columns = {
        'tipus': fields.integer('Tipus'),
        'estat': fields.char('Estat', size=64),
        'ref_int': fields.char('Referència Interna', size=16),
    }


GiscedataProjecteObra()
