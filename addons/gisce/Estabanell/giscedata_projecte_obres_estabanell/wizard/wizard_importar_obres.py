# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function, unicode_literals

import pooler
from osv import osv, fields

import base64
import csv
from datetime import datetime
from six import StringIO


class WizardImportProjectesObres(osv.osv_memory):
    """ Wizard per importar CSV dels projectes d'obres"""
    _name = 'wizard.import.projectes.obres'

    _columns = {
        'state': fields.selection([
            ('init', 'Init'),
            ('end', 'Final')
        ], 'Estat'),
        'info': fields.text('Informació', readonly=True),
        'csv_file': fields.binary(
            'Fitxer', states={'load': [('required', True)]}
        ),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'csv_file': False
    }

    def existeix_obra(self, cursor, uid, obra_number, context=None):
        """
        Retorna cert si existeix alguna obra amb

        """
        obra_obj = self.pool.get('giscedata.projecte.obra')
        return obra_obj.search_count(cursor, uid,
                                     [('name', '=', obra_number)]) > 1

    def importar_csv(self, cursor, uid, ids, context=None):
        def conv_date(date):
            correct_format = '%Y-%m-%d'
            try:
                return datetime.strptime(date, '%d/%m/%Y').strftime(
                    correct_format)
            except Exception as __:
                for format_date in ('%d/%m/%Y', '%Y-%m-%d', '%d-%m-%Y'):
                    try:
                        return datetime.strptime(
                            date, format_date
                        ).strftime(correct_format)
                    except:
                        pass
                info = u"La data: {} té un format desconegut.".format(date)
                raise osv.except_osv(u'Error!', info)

        wiz_file = self.read(cursor, uid, ids[0], ['csv_file'],
                             context)[0]['csv_file']
        if not csv:
            raise osv.except_osv(
                'Error', u"S'ha d'introduir algun fitxer CSV!")
        obra_obj = self.pool.get('giscedata.projecte.obra')
        txt = base64.decodestring(wiz_file)
        csv_file = StringIO(txt)
        reader = csv.reader(csv_file, delimiter=str(';'))

        obres_existents = []
        obres_creades = []
        excepcions = []
        # Ometem la capçalera
        next(reader)
        row_number = 0
        for row_number, row in enumerate(reader):
            if len(row) == 8:
                obra, data_creac, desc, data_tanc, data_fin, estat, ref, tipus = list(
                    map(lambda elem: elem or False, row)
                )
                if not self.existeix_obra(cursor, uid, obra):
                    tmp_cursor = pooler.get_db_only(cursor.dbname).cursor()
                    values = {
                        'name': obra,
                        'descripcio': desc,
                        'ref_int': ref,
                        'tipus': tipus,
                        'data_finalitzacio': data_fin and conv_date(data_fin),
                        'date_start': data_creac and conv_date(data_creac),
                        'date_end': data_tanc and conv_date(data_tanc),
                        'state': 'template',
                        'estat': estat
                    }
                    try:
                        obra_obj.create(tmp_cursor, uid, values)
                        obres_creades.append(obra)
                        tmp_cursor.commit()
                    except Exception as e:
                        tmp_cursor.rollback()
                        excepcions.append((obra, e.message))
                    finally:
                        tmp_cursor.close()
                else:
                    obres_existents.append(obra)
            else:
                line_msg = 'Línia {}'.format(row_number + 1)
                excepcions.append(
                    (line_msg, 'La línia no compleix el format de camps establert.')
                )

        info = u'''
            S\'han processat {totals} línies.\n
        '''.format(totals=row_number + 1)

        if obres_creades:
            info += u'Obres creades correctament:\n\t\t{creades}'.format(
                creades='\n\t\t'.join(obres_creades)
            )

        if obres_existents:
            info += u'Obres existents:\n\t\t{existents}'.format(
                existents='\n\t\t'.join(obres_existents)
            )

        if excepcions:
            llista_excepcions = [
                u'{}: {}'.format(ex[0], ex[1]) for ex in excepcions
            ]
            info += u'Excepcions:\n\t\t{excepcions}'.format(
                excepcions='\n\t\t'.join(llista_excepcions)
            )

        self.write(cursor, uid, ids, {'state': 'end', 'info': info})


WizardImportProjectesObres()
