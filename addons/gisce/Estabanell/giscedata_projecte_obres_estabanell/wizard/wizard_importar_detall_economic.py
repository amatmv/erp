# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function, unicode_literals

import pooler
from osv import osv, fields
import base64
import csv
from tools.translate import _

from six import StringIO


class WizardImportDetallEconomic(osv.osv_memory):

    """ Wizard per importar CSV dels projectes d'obres"""
    _name = 'wizard.import.detall.economic'

    _columns = {
        'state': fields.selection([
            ('init', 'Init'),
            ('end', 'Final')
        ], 'Estat'),
        'info': fields.text('Informació', readonly=True),
        'csv_file': fields.binary(
            'Fitxer', states={'load': [('requiered', True)]}
        ),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'csv_file': False
    }

    def obtenir_obra(self, cursor, uid, obra_name, context=None):
        obra_obj = self.pool.get('giscedata.projecte.obra')
        obra_id = obra_obj.search(cursor, uid, [('name', '=', obra_name)])
        if obra_id:
            return obra_id[0]
        return False

    def importar_csv(self, cursor, uid, ids, context=None):
        def format_decimal(dec):
            try:
                return dec and float(dec.replace(',', '.'))
            except:
                if len(dec.split(',')) == 2:
                    return dec
                else:
                    raise osv.except_osv(
                        'Error', 'Mal format en el nombre decimal: {}'.format(
                            dec)
                    )

        wiz_file = self.read(cursor, uid, ids[0], ['csv_file'],
                             context)[0]['csv_file']
        if not wiz_file:
            raise osv.except_osv(
                'Error', _(u"S'ha d'introduir algun fitxer CSV!"))
        detall_obj = self.pool.get('giscedata.projecte.obra.detall.economic')
        inst_obj = self.pool.get('giscedata.tipus.installacio')
        codi_ins_obj = self.pool.get('giscedata.codi.installacio')

        txt = base64.decodestring(wiz_file)
        csv_file = StringIO(txt)
        reader = csv.reader(csv_file, delimiter=str(';'))

        linies_incorrectes = []
        tis_creats = tis_actualitzats = 0
        excepcions = []
        linies_totals = linies_correctes = 0
        # Ometem la capçalera
        next(reader)
        for i, row in enumerate(reader):
            tmp_cursor = pooler.get_db_only(cursor.dbname).cursor()
            try:
                try:
                    obra, ti, tipo_c, act, subact, mat, ing, im_civil, im_trabaj, financ = list(
                        map(lambda elem: elem or False, row))
                    codi_ins = False
                except Exception:
                    try:
                        obra, ti, tipo_c, act, subact, mat, ing, im_civil, im_trabaj, financ, codi_ins = list(
                            map(lambda elem: elem or False, row))
                    except:
                        raise osv.except_osv(
                            'Error', 'La línia {linia} no té el format '
                                     'establert'.format(linia=i+1)
                        )
                linies_totals += 1

                if not obra:
                    linies_incorrectes.append(
                        _('Línia {}: hi ha d\'haver la referència d\'alguna '
                          'obra.').format(i+1))
                    continue

                obra_id = self.obtenir_obra(tmp_cursor, uid, obra)

                if not obra_id:
                    linies_incorrectes.append(
                        _('Línia {num_linia}: Obra "{obra_name}" no '
                          'existent').format(num_linia=i+1, obra_name=obra))
                    continue

                matching_tis = inst_obj.search(cursor, uid, [('name', '=', ti)])
                if not matching_tis:
                    linies_incorrectes.append(
                        _('Línia {num_linia}: El TI {ti} no existeixa la base '
                          'de dades.').format(num_linia=i+1, ti=ti))
                    continue

                # Si hi ha algun detall coincident en obra, ti, activitat i
                # subactivitat, actualitzarem els seus imports
                check_existing_detall_fields = {
                    'obra_id': obra_id,
                    'ti_ccuu': ti,
                    'actividad': act,
                    'subactividad': subact
                }
                detalls_existents = detall_obj.return_existing_detalls(
                    tmp_cursor, uid, check_existing_detall_fields
                )
                if detalls_existents and len(detalls_existents) == 1:
                    values_to_update = {}
                    if ing:
                        values_to_update['im_ingenieria'] = format_decimal(ing)
                    if im_trabaj:
                        values_to_update['im_trabajos'] = format_decimal(im_trabaj)
                    if im_civil:
                        values_to_update['im_obracivil'] = format_decimal(im_civil)
                    if mat:
                        values_to_update['im_materiales'] = format_decimal(mat)

                    detall_obj.write(cursor, uid, detalls_existents,
                                     values_to_update)
                    tis_actualitzats += 1
                    continue

                codi_ins_id = False
                if codi_ins:
                    codi_ins_id = codi_ins_obj.search(cursor, uid, [
                        ('name', '=', codi_ins)
                    ])
                    if not codi_ins_id:
                        format_vals = {
                            'num_linia': i + 1,
                            'cod_ins': codi_ins
                        }
                        linies_incorrectes.append(_(
                            'Advertència a la línia {num_linia}: El Codi '
                            'INS {cod_ins} no existeix.'
                        ).format(**format_vals))
                    else:
                        codi_ins_id = codi_ins_id[0]

                values = {
                    'codi_ins': codi_ins_id,
                    'obra_id': obra_id,
                    'ti_ccuu': matching_tis[0],
                    'financiado': format_decimal(financ),
                    'im_ingenieria': format_decimal(ing),
                    'im_trabajos': format_decimal(im_trabaj),
                    'im_obracivil': format_decimal(im_civil),
                    'im_materiales': format_decimal(mat),
                    'tipo_inversion': tipo_c,
                    'actividad': act,
                    'subactividad': subact,
                }

                detall_obj.create(tmp_cursor, uid, values)
                tis_creats += 1
                tmp_cursor.commit()
            except Exception as e:
                tmp_cursor.rollback()
                excepcions.append((i, e.message))
            finally:
                tmp_cursor.close()

        info = u'''
            S\'han processat {totals} línies.\n\t\t
        '''.format(totals=linies_totals)

        if tis_creats:
            info += u'Línies creades: {creades}\n\t\t'.format(creades=tis_creats)

        if tis_creats:
            info += u'Línies actualitzades: {actualitzades}\n\t\t'.format(
                actualitzades=tis_actualitzats)

        if linies_incorrectes:
            info += u'Línies incorrectes:\n\t\t{incorrectes}\n\t\t'.format(
                incorrectes='\n\t\t'.join(linies_incorrectes)
            )

        if excepcions:
            llista_excepcions = [
                u'{}: {}'.format(ex[0], ex[1]) for ex in excepcions
            ]
            info += u'Excepcions:\n\t\t{excepcions}\n\t\t'.format(
                excepcions='\n\t\t'.join(llista_excepcions)
            )

        self.write(cursor, uid, ids, {'state': 'end', 'info': info})


WizardImportDetallEconomic()
