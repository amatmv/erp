# -*- coding: utf-8 -*-
{
    "name": "GISCE Obres Estabanell",
    "description": """Obres Estabanell""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends": [
        "giscedata_projecte_obres"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "giscedata_projecte_obres_view.xml",
        "giscedata_projecte_obres_ti_view.xml",
        "wizard/wizard_importar_obres_view.xml",
        "wizard/wizard_importar_detall_economic_view.xml",
        "security/ir.model.access.csv",
    ],
    "active": False,
    "installable": True
}
