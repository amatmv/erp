# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function, unicode_literals

from osv import osv, fields


class GiscedataProjecteObraDetallEconomic(osv.osv):

    _inherit = 'giscedata.projecte.obra.detall.economic'

    _columns = {
        'actividad': fields.char('Activitat', size=128),
        'subactividad': fields.char('Subactivitat', size=128),
    }


GiscedataProjecteObraDetallEconomic()


class GiscedataProjecteObraTiAt(osv.osv):

    _inherit = 'giscedata.projecte.obra.ti.at'

    _defaults = {
        'codigo_ccaa_1': lambda *args: 9,
        'codigo_ccaa_2': lambda *args: 9,
    }


GiscedataProjecteObraTiAt()


class GiscedataProjecteObraTiBt(osv.osv):

    _inherit = 'giscedata.projecte.obra.ti.bt'

    _defaults = {
        'codigo_ccaa_1': lambda *args: 9,
        'codigo_ccaa_2': lambda *args: 9,
    }


GiscedataProjecteObraTiBt()


class GiscedataProjecteObraTiPosicio(osv.osv):

    _inherit = 'giscedata.projecte.obra.ti.posicio'

    _defaults = {
        'codigo_ccaa': lambda *args: 9,
    }


GiscedataProjecteObraTiPosicio()


class GiscedataProjecteObraTiTransformador(osv.osv):

    _inherit = 'giscedata.projecte.obra.ti.transformador'

    _defaults = {
        'codigo_ccaa': lambda *args: 9,
    }


GiscedataProjecteObraTiTransformador()


class GiscedataProjecteObraTiDespatx(osv.osv):

    _inherit = 'giscedata.projecte.obra.ti.despatx'

    _defaults = {
        'codigo_ccaa': lambda *args: 9,
    }


GiscedataProjecteObraTiDespatx()


class GiscedataProjecteObraTiCelles(osv.osv):

    _inherit = 'giscedata.projecte.obra.ti.celles'

    _defaults = {
        'codigo_ccaa': lambda *args: 9,
    }


GiscedataProjecteObraTiCelles()


class GiscedataProjecteObraTiCts(osv.osv):

    _inherit = 'giscedata.projecte.obra.ti.cts'

    _defaults = {
        'codigo_ccaa': lambda *args: 9,
    }


GiscedataProjecteObraTiCts()
