# -*- coding: utf-8 -*-
from osv import osv, fields
import calendar
from datetime import datetime, timedelta, date
from tools import config, cache, float_round


class GiscegasPolissa(osv.osv):
    """Polissa customizations for Lucera."""

    _name = 'giscegas.polissa'
    _inherit = 'giscegas.polissa'

    _columns = {
        'quota_discounts': fields.one2many('giscegas.polissa.quota.discount', 'polissa_id', 'Descuentos de cuota')
    }

    def get_active_quota_discounts(self, cursor, uid, polissa_id, context=None):

        if not context:
            context = {}

        discounts = []

        discounts_obj = self.pool.get('giscegas.polissa.quota.discount')

        search_discounts = [('polissa_id', '=', polissa_id)]

        ctx = context.copy()
        ctx.update({'active_test': False})

        discounts_ids = discounts_obj.search(cursor, uid, search_discounts, context=ctx)

        for quota_discount in discounts_obj.browse(cursor, uid, discounts_ids):

            pending_amount = quota_discount.initial_amount - quota_discount.discounted_amount

            if pending_amount > 0:
                if quota_discount.dependant_polissa_id:
                    if quota_discount.dependant_polissa_id.state == 'activa':
                        discounts.append(quota_discount)
                else:
                    discounts.append(quota_discount)

        # Sort discounts by pending amount and then by date
        discounts = sorted(discounts, key=lambda d1: (d1.pending_amount, d1.date))

        return discounts

    def copy_data(self, cursor, uid, id, default=None, context=None):
        if default is None:
            default = {}

        default_values = {
            'affiliate_code': False,
            'affiliate_external_id': False,
            'quota_discounts': []
        }

        default.update(default_values)

        res_id = super(
            GiscegasPolissa, self).copy_data(cursor, uid, id, default, context)

        return res_id

GiscegasPolissa()
