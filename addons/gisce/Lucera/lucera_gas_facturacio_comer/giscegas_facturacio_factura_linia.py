# -*- coding: utf-8 -*-
from osv import osv, fields
import calendar
from datetime import datetime, timedelta, date
from tools import config, cache, float_round
from giscegas_polissa_quota_discount import *


class GiscegasFacturacioFacturaLinia(osv.osv):
    """Saves Lucera quota discount amount information"""

    # _name = 'giscegas.facturacio.factura.linia.quota.discount'
    _name = 'giscegas.facturacio.factura.linia'
    _inherit = 'giscegas.facturacio.factura.linia'

    _columns = {
        'polissa_quota_discount_id': fields.many2one('giscegas.polissa.quota.discount', 'Descuento',
                                                     required=False)
    }

    def unlink(self, cursor, uid, ids, context=None):

        discount_obj = self.pool.get('giscegas.polissa.quota.discount')

        for linia in self.browse(cursor, uid, ids, context=context):
            if linia.polissa_quota_discount_id:
                discount_ids = [linia.polissa_quota_discount_id.id]
                discount_obj._fnct_compute_amounts(cursor, uid, discount_ids, context=context)

        super(GiscegasFacturacioFacturaLinia, self).unlink(cursor, uid, ids, context=context)


GiscegasFacturacioFacturaLinia()
