# -*- coding: utf-8 -*-
from osv import osv, fields
import calendar
from datetime import datetime, timedelta, date
from tools import config, cache, float_round


class GiscegasPolissaQuotaDiscount(osv.osv):
    """Saves Lucera quota discount remaining information"""

    _name = 'giscegas.polissa.quota.discount'

    def _fnct_compute_amounts(self, cursor, uid, ids, field_name=None, arg=None, context=None):
        """Compute the applied discount amount for earch discount id"""
        if not ids:
            return {}

        res = {}

        for discount in self.browse(cursor, uid, ids):
            discounted_amount = 0.0
            for linia in discount.factura_linia_ids:
                if linia.invoice_id.type in ['out_refund']:
                    discounted_amount -= abs(linia.price_subtotal)
                elif linia.invoice_id.type in ['out_invoice']:
                    discounted_amount += abs(linia.price_subtotal)

            pending_amount = discount.initial_amount - discounted_amount
            res[discount.id] = {
                'discounted_amount': discounted_amount,
                'pending_amount': max(0, pending_amount)
            }

        return res

    def _get_applied_discount_ids_by_factura_linia(self, cursor, uid, ids, context=None):
        """Get discount_ids of modified factura_linias"""

        if not context:
            context = {}

        query = '''
            SELECT DISTINCT polissa_quota_discount_id
            FROM giscegas_facturacio_factura_linia
            WHERE polissa_quota_discount_id > 0 AND id IN %s
        '''

        query_params = (tuple(ids), )

        cursor.execute(query, query_params)
        discounts_ids = [x[0] for x in cursor.fetchall()]

        # linia_obj = self.pool.get('giscegas.facturacio.factura.linia')
        # linias = linia_obj.read(cursor, uid, ids, fields=['polissa_quota_discount_id'])
        # discounts_ids_2 = list(set([x['polissa_quota_discount_id'][0] for x in linias if x['polissa_quota_discount_id']]))

        return discounts_ids

    def _get_applied_discount_ids_by_factura(self, cursor, uid, ids, context=None):
        """Get discount_ids of modified facturas"""
        # return ids
        if not context:
            context = {}

        query = '''
            SELECT DISTINCT polissa_quota_discount_id
            FROM giscegas_facturacio_factura_linia
            WHERE polissa_quota_discount_id > 0 AND factura_id IN %s
        '''

        query_params = (tuple(ids),)

        cursor.execute(query, query_params)
        discounts_ids = [x[0] for x in cursor.fetchall()]

        # Other way of computing discount ids

        # linia_obj = self.pool.get('giscegas.facturacio.factura.linia')
        # linia_ids = linia_obj.search(cursor, uid, [('factura_id', 'in', ids)])
        # linias = linia_obj.read(cursor, uid, linia_ids, fields=['polissa_quota_discount_id'])

        # discounts_ids_2 = list(set([x['polissa_quota_discount_id'][0] for x in linias if x['polissa_quota_discount_id']]))

        return discounts_ids

    # When to recompute amounts
    _store_amounts = {
        # When initial amount changes it should recompute amounts. It returns the same ids as passed
        'giscegas.polissa.quota.discount': (lambda self, cr, uid, ids, c=None: ids, [], 30),
        # Every change on factura should recompute discounts
        'giscegas.facturacio.factura': (_get_applied_discount_ids_by_factura, [], 30),
        # Every change on factura linea should recompute discounts
        'giscegas.facturacio.factura.linia': (_get_applied_discount_ids_by_factura_linia, [], 30)
    }

    _columns = {
        'polissa_id': fields.many2one(
            'giscegas.polissa',
            'Contrato',
            ondelete='cascade',
            required=True
        ),
        'name': fields.char("Descripción", size=64),
        'affiliate_code': fields.char('Afiliado', size=32, required=False),
        'dependant_polissa_id': fields.many2one(
            'giscegas.polissa',
            'Contrato amigo',
            ondelete='cascade',
            required=False,
        ),
        'initial_amount': fields.float("Descuento inicial",
                                       digits=(16, 2), required=True),
        'discounted_amount': fields.function(_fnct_compute_amounts,
                                             method=True,
                                             type="float", digits=(16, 2),
                                             string="Descuento aplicado",
                                             readonly=True,
                                             store=_store_amounts,
                                             multi="compute_amounts"),
        'pending_amount': fields.function(_fnct_compute_amounts,
                                          method=True,
                                          type="float", digits=(16, 2),
                                          string="Descuento restante",
                                          readonly=True,
                                          store=_store_amounts,
                                          multi="compute_amounts"),
        'date': fields.date('Fecha de creación'),
        'factura_linia_ids': fields.one2many('giscegas.facturacio.factura.linia',
                                             'polissa_quota_discount_id', 'Líneas de descuento',
                                             readonly=True)
    }

    _defaults = {
        'date': date.today().strftime('%Y-%m-%d')
    }


GiscegasPolissaQuotaDiscount()