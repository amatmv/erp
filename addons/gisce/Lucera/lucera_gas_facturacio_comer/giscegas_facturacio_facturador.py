# -*- coding: utf-8 -*-
from osv import osv, fields
import calendar
from datetime import datetime, timedelta, date
from tools import config, cache, float_round

GAS_JOURNAL_CODES_WITH_LUCERA_QUOTAS = ['GAS', 'GAS.R']


class GiscegasFacturacioFacturador(osv.osv):
    """Adds Lucera service line to invoice"""

    _name = 'giscegas.facturacio.facturador'
    _inherit = 'giscegas.facturacio.facturador'

    @cache(timeout=300)
    def _get_lucera_quota_product(self, cursor, uid, tariff_code, context=None):
        product_obj = self.pool.get('product.product')
        product_code = 'LUCERAGAS{}'.format(tariff_code)
        product_id = product_obj.search(cursor, uid, [('default_code', '=', product_code)])[0]
        product = product_obj.browse(cursor, uid, product_id, context)
        return product

    def _add_quota_line(self, cursor, uid, factura, context=None):

        def _get_quota_quantity(start_date, end_date):
            if isinstance(start_date, basestring):
                start_date = datetime.strptime(start_date, '%Y-%m-%d')
            if isinstance(end_date, basestring):
                end_date = datetime.strptime(end_date, '%Y-%m-%d')
            value = 0.0
            while end_date >= start_date:
                units = 1.0 / calendar.monthrange(end_date.year, end_date.month)[1]
                value += units
                end_date -= timedelta(days=1)
            return round(value, 2)

        # Find quota discounts for this contract
        quantity = _get_quota_quantity(factura.data_inici, factura.data_final)
        product = self._get_lucera_quota_product(cursor, uid, factura.tarifa_acces_id.codi_ocsum, context)

        vals = {'data_desde': factura.data_inici,
                'data_fins': factura.data_final,
                'uos_id': product.uom_id.id,
                'quantity': quantity,
                'multi': 1,
                'product_id': product.id,
                'tipus': 'altres',
                'isdiscount': False,
                'name': product.description}

        return self.crear_linia(cursor, uid, factura.id, vals, context)

    def _add_quota_discount_lines(self, cursor, uid, factura, quota_linia, context=None):

        quota_amount = quota_linia.price_subtotal

        if quota_amount == 0:
            return []

        polissa = factura.polissa_id
        discounts = polissa.get_active_quota_discounts()

        if len(discounts) == 0:
            return []

        product = self._get_lucera_quota_product(cursor, uid, context)

        pending_quota_amount = quota_amount

        lines = []

        for discount in discounts:
            discount_amount = min(pending_quota_amount, discount.pending_amount)
            pending_quota_amount -= discount_amount

            discount_amount = -float_round(discount_amount, int(config['price_accuracy']))
            # TODO: Should we use quantity and price_unit instead of price_subtotal?
            # Or quantity of the initial discount
            quantity = 1

            line_vals = {
                'name': discount.name,
                'factura_id': factura.id,
                'product_id': product.id,
                'quantity': quantity,
                'multi': 1,
                'data_desde': factura.data_inici,
                'data_fins': factura.data_final,
                'uos_id': product.uom_id.id,
                'tipus': 'altres',
                'force_price': discount_amount,
                'isdiscount': True,
                'polissa_quota_discount_id': discount.id,
            }

            new_line = self.crear_linia(cursor, uid, factura.id, line_vals, context)

            lines.append(new_line)

            # If there is no more pending_quota_amount we don't have to continue processing discounts
            if pending_quota_amount <= 0:
                break

    def _process_lucera_quota(self, cursor, uid, factura, context=None):
        linia_obj = self.pool.get('giscegas.facturacio.factura.linia')

        quota_linea_id = self._add_quota_line(cursor, uid, factura, context)
        quota_linia = linia_obj.browse(cursor, uid, quota_linea_id)

        self._add_quota_discount_lines(cursor, uid, factura, quota_linia, context)

        # Update invoice taxes
        invoice_obj = self.pool.get('giscegas.facturacio.factura')
        invoice_obj.button_reset_taxes(cursor, uid, [factura.id])


    def fact_via_lectures(self, cursor, uid, polissa_id, lot_id, context=None):
        """We override the method to add the Lucera invoice line and set temporal lectures"""
        if not context:
            context = {}

        factura_obj = self.pool.get('giscegas.facturacio.factura')

        invoices = super(GiscegasFacturacioFacturador, self). \
            fact_via_lectures(cursor, uid, polissa_id, lot_id, context)
        
        ctx = context.copy()

        for factura in factura_obj.browse(cursor, uid, invoices, context):
            if factura.journal_id.code in GAS_JOURNAL_CODES_WITH_LUCERA_QUOTAS:
                # if the invoice is from the energy journal
                self._process_lucera_quota(cursor, uid, factura, ctx)

        return invoices


GiscegasFacturacioFacturador()