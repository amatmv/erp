# -*- coding: utf-8 -*-
{
    "name": "Cuota Lucera for Gas",
    "description": """Creates the invoice line for the Lucera fee for Gas. NOT READY - DO NOT INSTALL""",
    "version": "0-dev",
    "author": "Lucera",
    "category": "Lucera",
    "depends":[
        "base",
        "giscegas_polissa",
        "giscegas_facturacio",
        "giscegas_facturacio_comer",
        "giscegas_polissa_comer"
    ],
    "init_xml": [],
    "demo_xml": [
        "giscegas_facturacio_comer_demo.xml",
    ],
    "update_xml":[
        "giscegas_facturacio_comer_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": False
}
