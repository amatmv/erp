# -*- coding: utf-8 -*-
from osv import osv, fields
import calendar
from datetime import datetime, timedelta, date
from tools import config, cache, float_round


class GiscegasFacturacioFactura(osv.osv):
    """Clase per la factura de Lucera."""
    _name = 'giscegas.facturacio.factura'
    _inherit = 'giscegas.facturacio.factura'

    def unlink(self, cursor, uid, ids, context=None):

        discounts_ids = set()

        for factura in self.browse(cursor, uid, ids, context=context):
            for linia in factura.linia_ids:
                if linia.polissa_quota_discount_id:
                    discounts_ids.add(linia.polissa_quota_discount_id.id)

        super(GiscegasFacturacioFactura, self).unlink(cursor, uid, ids, context=context)

        if discounts_ids:
            discount_obj = self.pool.get('giscegas.polissa.quota.discount')
            # Recompute discount amounts.
            discount_obj.write(cursor, uid, list(discounts_ids), {}, context)


GiscegasFacturacioFactura()


