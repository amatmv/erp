# -*- coding: utf-8 -*-
import unittest
from datetime import datetime
from dateutil.relativedelta import relativedelta
from destral import testing
from destral.transaction import Transaction
from expects import *
from osv.orm import except_orm
from lucera_common.testing import create_test_gas_polissa, create_test_gas_invoice


class TestGiscegasFacturacioFacturador(testing.OOTestCase):

    require_demo_data = True

    # Quota
    def test_adds_full_quota_when_creating_a_full_month_invoice(self):
        pass
        # May be we don't create quotas in gas or it is a unique quota for any service
        # self.openerp.install_module(
        #     'giscegas_tarifas_peajes_20180101'
        # )
        # pool = self.openerp.pool
        # with Transaction().start(self.database) as txn:
        #     cursor = txn.cursor
        #     uid = txn.user
        #
        #     # Arrange
        #     result = create_test_gas_polissa(pool, cursor, uid, state='activa',
        #                                      override_vals={'data_alta': '2018-01-01'})
        #     gas_polissa_id = result['gas_polissa_id']
        #
        #     # Act
        #     invoice_id = create_test_gas_invoice(pool, cursor, uid, gas_polissa_id, '2018-01-01', '2018-01-31')
        #
        #     # Assert
        #     fact_obj = self.openerp.pool.get('giscegas.facturacio.factura')
        #     invoice = fact_obj.browse(cursor, uid, invoice_id)
        #
        #     expect(invoice).not_to(be_false)

    def test_adds_partial_quota_when_creating_a_partial_month_invoice(self):
        pass

    def test_does_not_add_quota_if_it_is_not_an_energy_invoice(self):
        pass

    def test_adds_proportional_quota_when_creating_more_than_one_month_invoice(self):
        pass

    # Discounts

    def test_adds_quota_and_equivalent_discount_if_polissa_has_available_discounts(self):
        pass

    def test_adds_quota_and_equivalent_partial_discount_if_polissa_has_partial_available_discount(self):

        # Arrange

        # - Create discount for polissa
        # - Create measures

        # Act

        # - Create invoice

        # Check

        # - Invoice is created
        # - Invoice has quota
        # - Invoice has partial discount
        # - Discount is consumed and remaining amount is 0

        pass

    def test_adds_quota_and_equivalent_discounts_from_two_different_discounts_when_first_is_exhausted(self):
        pass

    def test_returns_discounted_amounts_if_invoice_is_deleted(self):
        pass

    def test_returns_discounted_amounts_to_multiple_discounts_if_invoice_is_deleted(self):
        pass

    def test_returns_discounted_amounts_if_invoice_is_refunded(self):
        pass

    def test_returns_discounted_amounts_to_multiple_discounts_if_invoice_is_refunded(self):
        pass

if __name__ == '__main__':
    unittest.main()
