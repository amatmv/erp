# -*- coding: utf-8 -*-
{
    "name": "Invoice debt reporting information for Gas",
    "description": """Allows to mark an invoice to be taken in account when reporting partners to debt listings""",
    "version": "0-dev",
    "author": "Lucera",
    "category": "Lucera",
    "depends":[
        "base",
        "giscegas_polissa",
        "giscegas_facturacio_comer",
        "giscegas_polissa_comer"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "giscegas_facturacio_comer_view.xml"
    ],
    "active": False,
    "installable": True
}
