# -*- coding: utf-8 -*-
{
  "name": "Lucera polissa simple search",
  "description": """Simplifies searching for polissas""",
  "version": "0-dev",
  "author": "Lucera",
  "category": "Lucera",
  "depends": ['base',
              'giscedata_polissa_comer',
              'giscedata_facturacio',
              'giscedata_facturacio_iese' ],
  "init_xml": [],
  "demo_xml": [],
  "update_xml": ['lucera_extend_polissa_view.xml',
                 'lucera_polissa_comer_view_simple_search.xml'],
  "active": False,
  "installable": True
}
