# -*- coding: utf-8 -*-


def create_partner(openerp, txn, name, vat, email, phone, is_end_user, is_customer=True, is_active=True):
    """ Creates a new partner and returns the new partner_id, partner_address_id"""
    partner_obj = openerp.pool.get('res.partner')
    partner_addr_obj = openerp.pool.get('res.partner.address')
    new_partner = {
        'name': name,
        'customer': is_customer,
        'vat': vat,
        'is_end_user': is_end_user,
        'active': is_active
    }
    partner_id = partner_obj.create(txn.cursor, txn.user, new_partner)
    new_partner_address = {
        'partner_id': partner_id,
        'name': name,
        'email': email,
        'phone': phone
    }
    address_id = partner_addr_obj.create(txn.cursor, txn.user, new_partner_address)

    return partner_id, address_id


def get_polissa_id_by_reference(openerp, txn, polissa_reference):
    pool = openerp.pool
    polissa_obj = pool.get('giscedata.polissa')
    imd_obj = pool.get('ir.model.data')
    polissa_id = imd_obj.get_object_reference(
        txn.cursor, txn.user, 'giscedata_polissa', polissa_reference
    )[1]
    return polissa_id
