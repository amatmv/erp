# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
import unittest
import utils
from lucera_common.testing import create_partner_and_address
from expects import *
from osv import osv, fields, orm

# from normalization_tests import *

existing_end_user_vat = 'ESZ4200525V'
existing_end_user_email = 'test@test.com'
existing_end_user_phone = '(+34)961580184'
existing_non_end_user_vat = 'ES48473113S'
existing_non_end_user_email = 'test2@test2.com'
existing_non_end_user_phone = '(+34)961202332'


class PartnerEndUserMatchTests(testing.OOTestCase):
    """ Test find user matching """

    def _create_partner(self, txn, name, vat, email, phone, is_end_user, is_customer=True, is_active=True):
        return utils.create_partner(self.openerp, txn, name, vat, email, phone, is_end_user, is_customer, is_active)

    def test_can_find_matching_end_users(self):
        name = u'Hernández, Miguel'
        vat = 'ES72983942C'
        email = 'test99@test.com'
        phone = '931111111'

        with Transaction().start(self.database) as txn:
            p1, a1 = self._create_partner(txn, name, vat, email, phone, True)
            partner_obj = self.openerp.pool.get('res.partner')

            # Exact match by all fields
            match_params = {'name': name, 'vat': vat, 'email': email, 'phone': phone}
            result = partner_obj.match_end_users(txn.cursor, txn.user, match_params)
            expect(result[0]['id']).to(equal(p1))
            expect(result[0]['match_type']).to(equal('full'))

            # Match, but fuzzy name
            match_params = {'name': 'ernandez, miguel', 'vat': vat, 'email': email, 'phone': phone}
            result = partner_obj.match_end_users(txn.cursor, txn.user, match_params)
            expect(result[0]['id']).to(equal(p1))
            expect(result[0]['match_type']).to(equal('full'))

            # Match name, vat and email but not phone
            match_params = {'name': u'Hernández, Miguel', 'vat': vat, 'email': email, 'phone': '91222222'}
            result = partner_obj.match_end_users(txn.cursor, txn.user, match_params)
            expect(result[0]['id']).to(equal(p1))
            expect(result[0]['match_type']).to(equal('full'))  # It's a full match because we don't consider phone
            expect(result[0]['match_result']['vat']).to(be_true)
            expect(result[0]['match_result']['name']).to(be_true)
            expect(result[0]['match_result']['email']).to(be_true)
            expect(result[0]['match_result']['phone']).to(be_false)

            # Match vat, but not email and phone
            match_params = {'name': u'Hernández, Miguel', 'vat': vat, 'email': 'nop@email.com', 'phone': '91222222'}
            result = partner_obj.match_end_users(txn.cursor, txn.user, match_params)
            expect(len(result)).to(equal(1))
            expect(result[0]['id']).to(equal(p1))
            expect(result[0]['match_type']).to(equal('partial'))
            expect(result[0]['match_result']['vat']).to(be_true)
            expect(result[0]['match_result']['name']).to(be_true)
            expect(result[0]['match_result']['email']).to(be_false)
            expect(result[0]['match_result']['phone']).to(be_false)

            # Match email but not vat and not phone
            match_params = {'name': u'Hernández, Miguel', 'vat': '202022823', 'email': email, 'phone': '91222222'}
            result = partner_obj.match_end_users(txn.cursor, txn.user, match_params)
            expect(len(result)).to(equal(1))
            expect(result[0]['id']).to(equal(p1))
            expect(result[0]['match_type']).to(equal('partial'))
            expect(result[0]['match_result']['vat']).to(be_false)
            expect(result[0]['match_result']['name']).to(be_true)
            expect(result[0]['match_result']['email']).to(be_true)
            expect(result[0]['match_result']['phone']).to(be_false)

            # Do not match anything
            match_params = {'name': 'Otro', 'vat': '202022823', 'email': 'nop@email.com', 'phone': '91222222'}
            result = partner_obj.match_end_users(txn.cursor, txn.user, match_params)
            expect(len(result)).to(equal(0))

            # Match phone but not name, vat and not email. It's like it does not match anything
            match_params = {'name': 'Otro', 'vat': '202022823', 'email': 'nop@email.com', 'phone': phone}
            result = partner_obj.match_end_users(txn.cursor, txn.user, match_params)
            expect(len(result)).to(equal(0))

            # Match name and phone but not vat and not email. It's like it does not match anything
            match_params = {'name': name, 'vat': '202022823', 'email': 'nop@email.com', 'phone': phone}
            result = partner_obj.match_end_users(txn.cursor, txn.user, match_params)
            expect(len(result)).to(equal(0))

    def test_match_does_not_find_non_end_users(self):
        name = u'Hernández, Miguel'
        vat = 'ES72983942C'
        email = 'test99@test.com'
        phone = '931111111'

        with Transaction().start(self.database) as txn:
            p1, a1 = self._create_partner(txn, name, vat, email, phone, False)
            partner_obj = self.openerp.pool.get('res.partner')

            # Exact match by all fields
            match_params = {'name': name, 'vat': vat, 'email': email, 'phone': phone}
            result = partner_obj.match_end_users(txn.cursor, txn.user, match_params)
            expect(len(result)).to(equal(0))

    def test_match_return_partial_if_name_is_very_different(self):
        name = u'Hernández, Miguel'
        vat = 'ES72983942C'
        email = 'test99@test.com'
        phone = '931111111'

        with Transaction().start(self.database) as txn:
            p1, a1 = self._create_partner(txn, name, vat, email, phone, True)
            partner_obj = self.openerp.pool.get('res.partner')

            # Exact match by all fields
            match_params = {'name': 'Otro', 'vat': vat, 'email': email, 'phone': phone}
            result = partner_obj.match_end_users(txn.cursor, txn.user, match_params)
            expect(len(result)).to(equal(1))
            expect(result[0]['id']).to(equal(p1))
            expect(result[0]['match_type']).to(equal('partial'))

    def test_can_find_matching_end_users_using_non_normalized_fields(self):
        name = u'Hernández, Miguel'
        vat = '72983942-C'
        email = ' test99@test.com '
        phone = '+34 93 11 11 111'

        with Transaction().start(self.database) as txn:
            p1, a1 = self._create_partner(txn, name, vat, email, phone, True)
            partner_obj = self.openerp.pool.get('res.partner')

            # Exact match by all fields
            match_params = {'name': name, 'vat': vat, 'email': email, 'phone': phone}
            result = partner_obj.match_end_users(txn.cursor, txn.user, match_params)
            expect(len(result)).to(equal(1))
            expect(result[0]['id']).to(equal(p1))
            expect(result[0]['match_type']).to(equal('full'))

    def test_returns_full_match_if_existing_user_phone_is_null(self):
        name = u'Hernández, Miguel'
        vat = '72983942-C'
        email = ' test99@test.com '

        with Transaction().start(self.database) as txn:
            p1, a1 = self._create_partner(txn, name, vat, email, '', True)
            partner_obj = self.openerp.pool.get('res.partner')

            # Exact match by all fields but new phone
            match_params = {'name': name, 'vat': vat, 'email': email, 'phone': '+34 93 11 11 111'}
            result = partner_obj.match_end_users(txn.cursor, txn.user, match_params)
            expect(len(result)).to(equal(1))
            expect(result[0]['id']).to(equal(p1))
            expect(result[0]['match_type']).to(equal('full'))

    def test_can_match_multiple_end_users(self):
        name = u'Hernández, Miguel'
        vat = '72983942-C'
        email = ' test99@test.com '
        phone = '+34 93 11 11 111'

        with Transaction().start(self.database) as txn:
            p1, a1 = self._create_partner(txn, name, vat, email, phone, True)
            p2, a2 = self._create_partner(txn, 'Otro', '05454596P', 'new@email.com', '961212332', True)
            partner_obj = self.openerp.pool.get('res.partner')

            # Match two end user. One by vat and the other by email
            match_params = {'name': name, 'vat': vat, 'email': 'new@email.com', 'phone': '914457896'}
            result = partner_obj.match_end_users(txn.cursor, txn.user, match_params)
            expect(len(result)).to(equal(2))
            expect(result[0]['id']).to(equal(p1))
            expect(result[0]['match_type']).to(equal('partial'))
            expect(result[1]['id']).to(equal(p2))
            expect(result[1]['match_type']).to(equal('partial'))
