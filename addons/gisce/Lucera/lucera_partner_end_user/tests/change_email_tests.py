# -*- coding: utf-8 -*-
from datetime import timedelta

from destral import testing
from destral.transaction import Transaction
from expects import *

from lucera_common.fake import get_random_email
from lucera_common.testing.common import TESTING_DEFAULT_DATE
from lucera_common.testing import *

import utils


class ChangeEmailTests(testing.OOTestCase):

    def test_can_change_email_from_end_user_without_contracts(self):
        with Transaction().start(self.database) as txn:
            pool = self.openerp.pool
            cursor = txn.cursor
            uid = txn.user

            # Arrange
            partner_id, address_id = utils.create_partner(
                self.openerp, txn, 'Prueba1', 'ES05153714N', 'test1@t.com', '(+34)961111111', True)

            # Act
            new_email = 'test2@test2.com'
            partner_obj = pool.get('res.partner')
            result = partner_obj.change_end_user_email(cursor, uid, partner_id, new_email)

            # Assert
            expect(result).to(be_true)

            partner = partner_obj.browse(cursor, uid, partner_id)
            expect(partner.address[0].email).to(equal(new_email))

    def test_can_change_email_from_end_user_with_two_contracts(self):
        def check_polissa_change_is_correct(polissa, email):
            expect(polissa.direccio_notificacio.email).to(equal(email))

            if polissa.state == 'activa':
                expect(len(polissa.modcontractuals_ids)).to(equal(2))
                expect(polissa.modcontractuals_ids[0].observacions).to(contain(email))
            elif polissa.state == 'esborrany':
                expect(len(polissa.modcontractuals_ids)).to(equal(0))
            elif polissa.state == 'baixa':
                expect(len(polissa.modcontractuals_ids)).to(equal(1))

        with Transaction().start(self.database) as txn:
            pool = self.openerp.pool
            cursor = txn.cursor
            uid = txn.user

            # Arrange
            user_id, address_id = utils.create_partner(
                self.openerp, txn, 'Prueba1', 'ES05153714N', 'test1@t.com', '(+34)961111111', True)

            data_alta = (TESTING_DEFAULT_DATE - timedelta(days=10)).strftime('%Y-%m-%d')

            result1 = create_test_electricity_and_gas_polissa(pool, cursor, uid, user_id=user_id,
                                                              override_vals={'state': 'activa', 'data_alta': data_alta})

            result_electricity_1 = create_test_electricity_polissa(pool, cursor, uid, user_id=user_id,
                                                                   override_vals={'state': 'esborrany',
                                                                                  'data_alta': data_alta})

            result_electricity_2 = create_test_electricity_polissa(pool, cursor, uid, user_id=user_id,
                                                                   override_vals={'state': 'baixa',
                                                                                  'data_alta': data_alta})

            result_gas_1 = create_test_gas_polissa(pool, cursor, uid, user_id=user_id,
                                                   override_vals={'state': 'esborrany', 'data_alta': data_alta})

            result_gas_2 = create_test_gas_polissa(pool, cursor, uid, user_id=user_id,
                                                   override_vals={'state': 'activa', 'data_alta': data_alta})

            other_result = create_test_electricity_and_gas_polissa(pool, cursor, uid,
                                                                   override_vals={'state': 'activa',
                                                                                  'data_alta': data_alta})

            # Act
            new_email = 'test2@test2.com'
            partner_obj = pool.get('res.partner')
            result = partner_obj.change_end_user_email(cursor, uid, user_id, new_email)

            # Assert
            expect(result).to(be_true)

            partner = partner_obj.browse(cursor, uid, user_id)
            expect(partner.address[0].email).to(equal(new_email))

            polissa_obj = pool.get('giscedata.polissa')

            polissa_activa = polissa_obj.browse(cursor, uid, result1['polissa_id'])
            polissa_esborrany = polissa_obj.browse(cursor, uid, result_electricity_1['polissa_id'])
            polissa_baixa = polissa_obj.browse(cursor, uid, result_electricity_2['polissa_id'])

            gas_polissa_obj = pool.get('giscegas.polissa')
            gas_polissa_activa = gas_polissa_obj.browse(cursor, uid, result1['gas_polissa_id'])
            gas_polissa_esborrany = gas_polissa_obj.browse(cursor, uid, result_gas_1['gas_polissa_id'])
            gas_polissa_baixa = gas_polissa_obj.browse(cursor, uid, result_gas_2['gas_polissa_id'])

            check_polissa_change_is_correct(polissa_activa, new_email)
            check_polissa_change_is_correct(polissa_esborrany, new_email)
            check_polissa_change_is_correct(polissa_baixa, new_email)

            check_polissa_change_is_correct(gas_polissa_activa, new_email)
            check_polissa_change_is_correct(gas_polissa_esborrany, new_email)
            check_polissa_change_is_correct(gas_polissa_baixa, new_email)

            polissa_other = polissa_obj.browse(cursor, uid, other_result['polissa_id'])
            gas_polissa_other = gas_polissa_obj.browse(cursor, uid, other_result['gas_polissa_id'])

            expect(len(polissa_other.modcontractuals_ids)).to(equal(1))
            expect(polissa_other.direccio_notificacio.email).not_to(equal(new_email))

            expect(len(gas_polissa_other.modcontractuals_ids)).to(equal(1))
            expect(gas_polissa_other.direccio_notificacio.email).not_to(equal(new_email))

    def test_cannot_change_email_from_user_if_it_is_not_end_user(self):
        with Transaction().start(self.database) as txn:
            pool = self.openerp.pool
            cursor = txn.cursor
            uid = txn.user

            # Arrange
            partner_id, address_id = utils.create_partner(
                self.openerp, txn, 'Prueba1', 'ES05153714N', 'test1@t.com', '(+34)961111111', False)

            # Act
            new_email = 'test2@test2.com'
            partner_obj = pool.get('res.partner')

            with self.assertRaises(Exception) as cm:
                result = partner_obj.change_end_user_email(cursor, uid, partner_id, new_email)

            # Assert
            expect(cm.exception.name).to(be('ValidateError'))

    def test_cannot_change_email_from_end_user_if_it_is_invalid(self):
        with Transaction().start(self.database) as txn:
            pool = self.openerp.pool
            cursor = txn.cursor
            uid = txn.user

            # Arrange
            partner_id, address_id = utils.create_partner(
                self.openerp, txn, 'Prueba1', 'ES05153714N', 'test1@t.com', '(+34)961111111', True)

            # Act
            new_email = 'invalid'
            partner_obj = pool.get('res.partner')
            with self.assertRaises(Exception) as cm:
                result = partner_obj.change_end_user_email(cursor, uid, partner_id, new_email)

            # Assert
            expect(cm.exception.name).to(be('ValidateError'))

    def test_cannot_change_email_from_end_user_if_it_is_already_from_other_end_user(self):
        with Transaction().start(self.database) as txn:
            pool = self.openerp.pool
            cursor = txn.cursor
            uid = txn.user

            # Arrange
            existing_email = 'test_existing2@t2.com'
            partner_id, address_id = utils.create_partner(
                self.openerp, txn, 'Prueba1', 'ES05153714N', 'test1@t.com', '(+34)961111111', True)
            partner_id2, address_id2 = utils.create_partner(
                self.openerp, txn, 'Prueba2', '99319798W', existing_email, '(+34)961111111', True)

            # Act
            new_email = existing_email
            partner_obj = pool.get('res.partner')
            with self.assertRaises(Exception) as cm:
                result = partner_obj.change_end_user_email(cursor, uid, partner_id, new_email)

            # Assert
            expect(cm.exception.name).to(be('ValidateError'))

    def test_is_valid_new_end_user_email(self):
        with Transaction().start(self.database) as txn:
            pool = self.openerp.pool
            cursor = txn.cursor
            uid = txn.user

            # Arrange
            existing_email = 'test_existing2@t2.com'
            user_id, address_id = utils.create_partner(
                self.openerp, txn, 'Prueba1', 'ES05153714N', 'test1@t.com', '(+34)961111111', True)
            partner_id2, address_id2 = utils.create_partner(
                self.openerp, txn, 'Prueba2', '99319798W', existing_email, '(+34)961111111', True)

            # Act
            partner_obj = pool.get('res.partner')

            # Assert
            expect(partner_obj.is_valid_new_end_user_email(cursor, uid, user_id, get_random_email())[0]).to(be_true)
            expect(partner_obj.is_valid_new_end_user_email(cursor, uid, user_id, 'invalid')[0]).to(be_false)
            expect(partner_obj.is_valid_new_end_user_email(cursor, uid, user_id, existing_email)[0]).to(be_false)
            expect(partner_obj.is_valid_new_end_user_email(cursor, uid, user_id, '')[0]).to(be_false)
            expect(partner_obj.is_valid_new_end_user_email(cursor, uid, user_id, False)[0]).to(be_false)
            expect(partner_obj.is_valid_new_end_user_email(cursor, uid, user_id, None)[0]).to(be_false)
