# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from expects import *
from osv import osv
from lucera_common.testing import *

import utils


class PolissaEndUserTests(testing.OOTestCase):
    def _create_partner(self, txn, name, vat, email, phone, is_end_user, is_customer=True, is_active=True):
        return utils.create_partner(self.openerp, txn, name, vat, email, phone, is_end_user, is_customer, is_active)

    def test_on_change_notification_address_to_altre_p_it_does_not_fail_if_it_is_end_user(self):

        with Transaction().start(self.database) as txn:
            partner_id, address_id = \
                self._create_partner(txn, 'Prueba1', 'ES05153714N', 'test1@t.com', '(+34)961111111', True)
            polissa_id = utils.get_polissa_id_by_reference(self.openerp, txn, 'polissa_0001')
            polissa_obj = self.openerp.pool.get('giscedata.polissa')
            res = polissa_obj.onchange_notificacio(txn.cursor, txn.user, [polissa_id], 'altre_p',
                                                   titular=None, pagador=None, altre_p=partner_id)
            new_address_id = res['value']['direccio_notificacio']
            expect(new_address_id).to(equal(address_id))

    def test_on_change_notification_address_to_altre_p_i_it_fails_if_is_not_address_from_end_user(self):

        with self.assertRaises(osv.except_osv) as cm:
            with Transaction().start(self.database) as txn:
                partner_id, address_id = \
                    self._create_partner(txn, 'Prueba1', 'ES05153714N', 'test1@t.com', '(+34)961111111', False)
                polissa_id = utils.get_polissa_id_by_reference(self.openerp, txn, 'polissa_0001')
                polissa_obj = self.openerp.pool.get('giscedata.polissa')
                res = polissa_obj.onchange_notificacio(txn.cursor, txn.user, [polissa_id], 'altre_p',
                                                       titular=None, pagador=None, altre_p=partner_id)

        expect(cm.exception.name).to(be('ValidateError'))

    def test_on_change_notification_address_to_pagador_it_does_not_fail_if_it_is_end_user(self):

        with Transaction().start(self.database) as txn:
            partner_id, address_id = \
                self._create_partner(txn, 'Prueba1', 'ES05153714N', 'test1@t.com', '(+34)961111111', True)

            polissa_result = create_test_electricity_polissa(self.openerp.pool, txn.cursor, txn.user)
            polissa_id = polissa_result['polissa_id']

            polissa_obj = self.openerp.pool.get('giscedata.polissa')
            # Change payer
            vals = {}
            values = polissa_obj.onchange_pagador_sel(
                txn.cursor, txn.user, polissa_id, pagador_sel='altre_p', titular=None,
                pagador=partner_id, direccio_pagament=None, tipo_pago=None, notificacio=None)['value']
            vals.update(values)
            polissa_obj.write(txn.cursor, txn.user, polissa_id, vals)

            # Try to change notification address
            res = polissa_obj.onchange_notificacio(txn.cursor, txn.user, polissa_id, 'pagador',
                                                   titular=None, pagador=partner_id, altre_p=None)
            new_address_id = res['value']['direccio_notificacio']
            expect(new_address_id).to(equal(address_id))

    def test_on_change_notification_address_to_pagador_it_fails_if_it_is_end_user(self):
        with self.assertRaises(osv.except_osv) as cm:
            with Transaction().start(self.database) as txn:
                partner_id, address_id = \
                    self._create_partner(txn, 'Prueba1', 'ES05153714N', 'test1@t.com', '(+34)961111111', False)
                polissa_result = create_test_electricity_polissa(self.openerp.pool, txn.cursor, txn.user)
                polissa_id = polissa_result['polissa_id']
                polissa_obj = self.openerp.pool.get('giscedata.polissa')
                # Change payer
                vals = {}
                values = polissa_obj.onchange_pagador_sel(
                    txn.cursor, txn.user, polissa_id, pagador_sel='altre_p', titular=None,
                    pagador=partner_id, direccio_pagament=None, tipo_pago=None, notificacio=None)['value']
                vals.update(values)
                polissa_obj.write(txn.cursor, txn.user, polissa_id, vals)

                # Try to change notification address
                res = polissa_obj.onchange_notificacio(txn.cursor, txn.user, polissa_id, 'pagador',
                                                       titular=None, pagador=partner_id, altre_p=None)
                new_address_id = res['value']['direccio_notificacio']

        expect(cm.exception.name).to(be('ValidateError'))

    def test_cannot_delete_end_user_if_it_is_polissa_notification_address(self):
        with self.assertRaises(osv.except_osv) as cm:
            with Transaction().start(self.database) as txn:
                # Arrange
                pool = self.openerp.pool
                cursor = txn.cursor
                uid = txn.user

                user_id, user_address_id = create_partner_and_address(pool, cursor, uid, is_end_user=True)
                payer_id, payer_address_id = create_partner_and_address(pool, cursor, uid, is_end_user=False)
                holder_id, holder_address_id = create_partner_and_address(pool, cursor, uid, is_end_user=False)

                result = create_test_electricity_polissa(pool, cursor, uid,
                                                         user_id=user_id, fiscal_customer_id=payer_id, atr_holder_id=holder_id)
                # Act
                partner_obj = pool.get('res.partner')
                partner_obj.unlink(cursor, uid, user_id)  # We should not be able to delete

        # Arrange
        expect(cm.exception.name).to(be('ValidateError'))

    def test_can_write_polissa_if_notification_address_is_from_end_user(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            pool = self.openerp.pool
            cursor = txn.cursor
            uid = txn.user

            partner_id, address_id = \
                self._create_partner(txn, 'Prueba1', 'ES05153714N', 'test1@t.com', '(+34)961111111', True)
            result = create_test_electricity_polissa(pool, cursor, uid)
            polissa_id = result['polissa_id']
            polissa_obj = self.openerp.pool.get('giscedata.polissa')
            vals = {'direccio_notificacio': address_id}
            polissa_obj.write(cursor, uid, polissa_id, vals)

    def test_cannot_write_polissa_if_notification_address_is_not_from_end_user(self):
        with self.assertRaises(osv.except_osv) as cm:
            with Transaction().start(self.database) as txn:
                # Arrange
                pool = self.openerp.pool
                cursor = txn.cursor
                uid = txn.user

                partner_id, address_id = \
                    self._create_partner(txn, 'Prueba1', 'ES05153714N', 'test1@t.com', '(+34)961111111', False)
                result = create_test_electricity_polissa(pool, cursor, uid)
                polissa_id = result['polissa_id']
                polissa_obj = self.openerp.pool.get('giscedata.polissa')
                vals = {'direccio_notificacio': address_id}
                polissa_obj.write(cursor, uid, polissa_id, vals)

        # Arrange
        expect(cm.exception.name).to(be('ValidateError'))