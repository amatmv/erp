# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
import unittest
import utils
from lucera_common.testing import create_partner_and_address
from expects import *
from osv import osv, fields, orm

# from normalization_tests import *

existing_end_user_vat = 'ESZ4200525V'
existing_end_user_email = 'test@test.com'
existing_end_user_phone = '(+34)961580184'
existing_non_end_user_vat = 'ES48473113S'
existing_non_end_user_email = 'test2@test2.com'
existing_non_end_user_phone = '(+34)961202332'


class PartnerEndUserTests(testing.OOTestCase):
    def _create_partner(self, txn, name, vat, email, phone, is_end_user, is_customer=True, is_active=True):
        return utils.create_partner(self.openerp, txn, name, vat, email, phone, is_end_user, is_customer, is_active)

    def _create_partner_in_transaction(self, name, vat, email, phone, is_end_user, is_customer=True):
        with Transaction().start(self.database) as txn:
            return self._create_partner(txn, name, vat, email, phone, is_end_user, is_customer)

    def test_cannot_create_end_user_with_same_vat(self):
        with Transaction().start(self.database) as txn:
            self._create_partner(txn, 'Prueba1', existing_end_user_vat, 'test1@test.com', '(+34)961111111', True)
            with self.assertRaises(osv.orm.ValidateException) as cm:
                self._create_partner(txn, 'Prueba2', existing_end_user_vat, 'test2@test.com', '(+34)962222222', True)

        expect(cm.exception.name).to(be('ValidateError'))

    def test_cannot_create_end_user_with_same_email(self):
        email = existing_end_user_email
        with Transaction().start(self.database) as txn:
            self._create_partner(txn, 'Prueba 1', 'ES48473113S', email, '(+34)961111111', True)
            with self.assertRaises(osv.orm.ValidateException) as cm:
                self._create_partner(txn, 'Prueba 2', 'ESZ6185284Z', email, '(+34)962222222', True)

        expect(cm.exception.name).to(be('ValidateError'))

    def test_cannot_create_end_user_with_same_email_with_different_casing(self):

        with Transaction().start(self.database) as txn:
            self._create_partner(txn, 'Prueba 1', 'ES48473113S', 'test@test.com', '(+34)961111111', True)
            with self.assertRaises(osv.orm.ValidateException) as cm:
                self._create_partner(txn, 'Prueba 2', 'ESZ6185284Z', 'TEST@test.com', '(+34)962222222', True)

        expect(cm.exception.name).to(be('ValidateError'))

    # We have decidaded to not consider phone as unique

    # def test_cannot_create_end_user_with_same_phone(self):
    #     phone = existing_end_user_phone
    #     with Transaction().start(self.database) as txn:
    #         self._create_partner(txn, 'Prueba1', 'ES48473113S', 'test1@test.com', phone, True)
    #         with self.assertRaises(osv.orm.ValidateException) as cm:
    #             self._create_partner(txn, 'Prueba2', 'ESZ6185284Z', 'test2@test.com', phone, True)
    #     expect(cm.exception.name).to(be('ValidateError'))
    #
    # def test_cannot_create_end_user_with_same_phone_not_normalized(self):
    #     with Transaction().start(self.database) as txn:
    #         self._create_partner(txn, 'Prueba1', 'ES48473113S', 'test1@test.com', '961111111', True)
    #         with self.assertRaises(osv.orm.ValidateException) as cm:
    #             self._create_partner(txn, 'Prueba2', 'ESZ6185284Z', 'test2@test.com', '96 111 11-11', True)
    #     expect(cm.exception.name).to(be('ValidateError'))

    def test_can_create_end_user_with_same_phone(self):
        phone = existing_end_user_phone
        with Transaction().start(self.database) as txn:
            p1, a1 = self._create_partner(txn, 'Prueba1', 'ES48473113S', 'test1@test.com', phone, True)
            p2, a2 = self._create_partner(txn, 'Prueba2', 'ESZ6185284Z', 'test2@test.com', phone, True)
            expect(p2).to(be_above(0))
            expect(a2).to(be_above(0))
            expect(p1).not_to(equal(p2))
            expect(a1).not_to(equal(a2))

    def test_cannot_create_end_user_with_same_phone_not_normalized(self):
        with Transaction().start(self.database) as txn:
            p1, a1 = self._create_partner(txn, 'Prueba1', 'ES48473113S', 'test1@test.com', '961111111', True)
            p2, a2 = self._create_partner(txn, 'Prueba2', 'ESZ6185284Z', 'test2@test.com', '96 111 11-11', True)
            expect(p2).to(be_above(0))
            expect(a2).to(be_above(0))
            expect(p1).not_to(equal(p2))
            expect(a1).not_to(equal(a2))

    def test_can_create_end_user_if_it_matches_with_partner_but_not_with_end_user(self):
        vat = 'ES48473113S'
        email = 'test1@test.com'
        phone = '961111111'
        with Transaction().start(self.database) as txn:
            # Create a non end user partner
            self._create_partner(txn, 'Prueba1', vat, email, phone, False)
            # Create a end user partner but inactive
            self._create_partner(txn, 'Prueba1', vat, email, phone, True, is_active=False)
            # End user that matches by vat
            self._create_partner(txn, 'Prueba2', vat, 'test2@test.com', '962222222', True)
            # End user that matches by email
            self._create_partner(txn, 'Prueba3', 'ESZ6185284Z', email, '963333333', True)
            # End user that matches by phone
            self._create_partner(txn, 'Prueba4', 'ESZ2477190A', 'test4@test.com', phone, True)

    def test_can_update_end_user_vat_if_it_does_not_match_with_end_user(self):
        with Transaction().start(self.database) as txn:
            partner_id, address_id = \
                self._create_partner(txn, 'Prueba1', 'ES48473113S', 'test1@test.com', '961111111', True)
            partner_obj = self.openerp.pool.get('res.partner')
            partner_obj.write(txn.cursor, txn.user, [partner_id], {
                'vat': 'ES05153714N'
            })

    def test_can_update_end_user_with_same_vat(self):
        vat = 'ES48473113S'
        with Transaction().start(self.database) as txn:
            partner_id, address_id = self._create_partner(txn, 'Prueba1', vat, 'test1@test.com', '961111111', True)
            partner_obj = self.openerp.pool.get('res.partner')
            partner_obj.write(txn.cursor, txn.user, [partner_id], {
                'vat': vat
            })

    def test_cannot_update_end_user_vat_if_it_matches_with_end_user(self):
        vat = 'ES48473113S'

        with Transaction().start(self.database) as txn:
            self._create_partner(txn, 'Prueba1', vat, 'test1@test.com', '961111111', True)
            partner_id, address_id = \
                self._create_partner(txn, 'Prueba2', 'ESZ6185284Z', 'test2@test.com', '962222222', True)
            partner_obj = self.openerp.pool.get('res.partner')
            with self.assertRaises(osv.orm.ValidateException) as cm:
                partner_obj.write(txn.cursor, txn.user, [partner_id], {
                    'vat': vat
                })

        expect(cm.exception.name).to(be('ValidateError'))

    def test_can_update_end_user_email_if_it_does_not_match_with_end_user(self):
        with Transaction().start(self.database) as txn:
            partner_id, address_id = \
                self._create_partner(txn, 'Prueba1', 'ES48473113S', 'test1@test.com', '961111111', True)
            partner_address_obj = self.openerp.pool.get('res.partner.address')
            partner_address_obj.write(txn.cursor, txn.user, [address_id], {
                'email': 'testX@test.com'
            })

    def test_cannot_update_end_user_email_if_it_matches_with_existing_end_user(self):
        email = 'test1@test.com'
        with self.assertRaises(osv.orm.ValidateException) as cm:
            with Transaction().start(self.database) as txn:
                self._create_partner(txn, 'Prueba1', 'ES48473113S', email, '961111111', True)
                partner_id, address_id = \
                    self._create_partner(txn, 'Prueba2', 'ESZ6185284Z', 'test2@test.com', '962222222', True)
                partner_address_obj = self.openerp.pool.get('res.partner.address')
                partner_address_obj.write(txn.cursor, txn.user, [address_id], {
                    'email': email
                })

        expect(cm.exception.name).to(be('ValidateError'))

    def test_can_update_end_user_phone_even_if_it_matches_with_end_user(self):
        phone = '961111111'
        with Transaction().start(self.database) as txn:
            self._create_partner(txn, 'Prueba1', 'ES48473113S', 'test1@test.com', phone, True)
            partner_id, address_id = \
                self._create_partner(txn, 'Prueba2', 'ESZ6185284Z', 'test2@test.com', '962222222', True)
            partner_address_obj = self.openerp.pool.get('res.partner.address')
            result = partner_address_obj.write(txn.cursor, txn.user, [address_id], {
                        'phone': phone
                    })
            expect(result).to(be_true)

    def test_can_convert_to_end_user_if_vat_does_not_match_with_existing_user(self):
        with Transaction().start(self.database) as txn:
            self._create_partner(txn, 'Prueba1', 'ES48473113S', 'test1@test.com', '961111111', True)
            # Non existing
            partner_id, address_id = \
                self._create_partner(txn, 'Prueba1', 'ES05153714N', 'test2@test.com', '962222222', False)
            partner_obj = self.openerp.pool.get('res.partner')
            partner_obj.write(txn.cursor, txn.user, [partner_id], {
                'is_end_user': '1'
            })

    def test_cannot_convert_to_end_user_if_vat_matches_with_existing_user(self):
        vat = 'ES48473113S'
        with self.assertRaises(osv.orm.ValidateException) as cm:
            with Transaction().start(self.database) as txn:
                self._create_partner(txn, 'Prueba1', vat, 'test1@test.com', '961111111', True)
                partner_id, address_id = \
                    self._create_partner(txn, 'Prueba2', vat, 'test2@test.com', '962222222', False)
                partner_obj = self.openerp.pool.get('res.partner')
                partner_obj.write(txn.cursor, txn.user, [partner_id], {
                    'is_end_user': 1
                })

        expect(cm.exception.name).to(be('ValidateError'))

    def test_cannot_convert_to_end_user_if_new_vat_matches_with_existing_user(self):
        vat = 'ES48473113S'
        with Transaction().start(self.database) as txn:
            self._create_partner(txn, 'Prueba1', vat, 'test1@test.com', '961111111', True)
            partner_id, address_id = \
                self._create_partner(txn, 'Prueba2', 'ES05153714N', 'test2@test.com', '962222222', False)
            partner_obj = self.openerp.pool.get('res.partner')
            with self.assertRaises(osv.orm.ValidateException) as cm:
                partner_obj.write(txn.cursor, txn.user, [partner_id], {
                    'vat': vat,
                    'is_end_user': 1
                })

        expect(cm.exception.name).to(be('ValidateError'))

    def test_cannot_convert_to_end_user_if_email_matches_with_existing_user(self):
        email = 'test1@test.com'
        with Transaction().start(self.database) as txn:
            self._create_partner(txn, 'Prueba1', 'ES48473113S', email, '961111111', True)
            partner_id, address_id = \
                self._create_partner(txn, 'Prueba2', 'ES05153714N', email, '962222222', False)
            partner_obj = self.openerp.pool.get('res.partner')
            with self.assertRaises(osv.orm.ValidateException) as cm:
                partner_obj.write(txn.cursor, txn.user, [partner_id], {
                    'is_end_user': 1
                })

        expect(cm.exception.name).to(be('ValidateError'))

    def test_can_convert_to_end_user_if_phone_matches_with_existing_user(self):
        phone = '961111111'
        with Transaction().start(self.database) as txn:
            self._create_partner(txn, 'Prueba1', 'ES48473113S', 'test1@test.com', phone, True)
            partner_id, address_id = \
                self._create_partner(txn, 'Prueba2', 'ES05153714N', 'test2@test.com', phone, False)
            partner_obj = self.openerp.pool.get('res.partner')
            result = partner_obj.write(txn.cursor, txn.user, [partner_id], {
                        'is_end_user': 1
                    })
            expect(result).to(be_true)

    def test_cannot_create_end_user_without_vat(self):
        with Transaction().start(self.database) as txn:
            with self.assertRaises(osv.orm.ValidateException) as cm:
                self._create_partner(txn, 'Prueba1', '', 'test1@test.com', '961111111', True)

        expect(cm.exception.name).to(be('ValidateError'))

    def test_cannot_create_end_user_without_email(self):
        with self.assertRaises(osv.orm.ValidateException) as cm:
            with Transaction().start(self.database) as txn:
                self._create_partner(txn, 'Prueba1', 'ES48473113S', '', '961111111', True)

        expect(cm.exception.name).to(be('ValidateError'))

    def test_can_create_end_user_without_phone(self):
        with Transaction().start(self.database) as txn:
            p1, a1 = self._create_partner(txn, 'Prueba1', 'ES48473113S', 'test1@test.com', '', True)
            expect(p1).to(be_above(0))
            expect(a1).to(be_above(0))

    def test_cannot_update_end_user_without_vat(self):
        with Transaction().start(self.database) as txn:
            partner_id, address_id = \
                self._create_partner(txn, 'Prueba1', existing_end_user_vat, 'test1@test.com', '961111111', True)
            partner_obj = self.openerp.pool.get('res.partner')
            with self.assertRaises(osv.orm.ValidateException) as cm:
                partner_obj.write(txn.cursor, txn.user, [partner_id], {
                    'vat': ''
                })
        expect(cm.exception.name).to(be('ValidateError'))

    def test_cannot_update_end_user_with_invalid_vat(self):
        with Transaction().start(self.database) as txn:
            partner_id, address_id = \
                self._create_partner(txn, 'Prueba1', existing_end_user_vat, 'test1@test.com', '961111111', True)
            partner_obj = self.openerp.pool.get('res.partner')
            with self.assertRaises(osv.orm.ValidateException) as cm:
                partner_obj.write(txn.cursor, txn.user, [partner_id], {
                    'vat': 'ES23333333A'
                })

        expect(cm.exception.name).to(be('ValidateError'))

    def test_cannot_convert_to_end_user_address_without_email(self):
        with Transaction().start(self.database) as txn:
            partner_id, address_id = self._create_partner(txn, 'Prueba1', 'ESZ4200525V', '', '961111111', False)
            partner_obj = self.openerp.pool.get('res.partner')
            with self.assertRaises(osv.orm.ValidateException) as cm:
                partner_obj.write(txn.cursor, txn.user, [partner_id], {
                    'is_end_user': '1'
                })

        expect(cm.exception.name).to(be('ValidateError'))

    def test_can_convert_to_end_user_address_without_phone(self):
        with Transaction().start(self.database) as txn:
            partner_id, address_id = \
                self._create_partner(txn, 'Prueba1', 'ESZ4200525V', 'test1@test.com', '', False)
            partner_obj = self.openerp.pool.get('res.partner')
            result = partner_obj.write(txn.cursor, txn.user, [partner_id], {
                'is_end_user': '1'
            })
            expect(result).to(be_true)

    def test_can_convert_to_end_user_address_without_phone(self):
        with Transaction().start(self.database) as txn:
            partner_id, address_id = \
                self._create_partner(txn, 'Prueba1', 'ESZ4200525V', 'test1@test.com', '', False)
            partner_obj = self.openerp.pool.get('res.partner')
            result = partner_obj.write(txn.cursor, txn.user, [partner_id], {
                'is_end_user': '1'
            })
            expect(result).to(be_true)

    def test_can_update_multiple_end_users(self):
        with Transaction().start(self.database) as txn:
            p1, a1 = self._create_partner(txn, 'Prueba1', 'ES48473113S', 'test1@test.com', '961111111', True)
            p2, a2 = self._create_partner(txn, 'Prueba2', 'ES05153714N', 'test2@test.com', '962222222', True)
            partner_obj = self.openerp.pool.get('res.partner')
            result = partner_obj.write(txn.cursor, txn.user, [p1, p2], {
                'comment': 'I can update multiple users'
            })

            expect(result).to(be_true)

    def test_can_convert_multiple_end_users(self):
        with Transaction().start(self.database) as txn:
            p1, a1 = self._create_partner(txn, 'Prueba1', 'ES48473113S', 'test1@test.com', '961111111', False)
            p2, a2 = self._create_partner(txn, 'Prueba2', 'ES05153714N', 'test2@test.com', '962222222', False)
            partner_obj = self.openerp.pool.get('res.partner')
            result = partner_obj.write(txn.cursor, txn.user, [p1, p2], {
                'is_end_user': '1'
            })
            expect(result).to(be_true)

    def test_cannot_convert_multiple_end_users_if_one_already_matches(self):
        vat = 'ES48473113S'

        with Transaction().start(self.database) as txn:
            self._create_partner(txn, 'Prueba0', vat, 'test0@test.com', '961111111', True)
            p1, a1 = self._create_partner(txn, 'Prueba1', vat, 'test1@test.com', '961111111', False)
            p2, a2 = self._create_partner(txn, 'Prueba2', 'ES05153714N', 'test2@test.com', '962222222', False)
            partner_obj = self.openerp.pool.get('res.partner')
            with self.assertRaises(osv.orm.ValidateException) as cm:
                result = partner_obj.write(txn.cursor, txn.user, [p1, p2], {
                    'is_end_user': '1'
                })

        expect(cm.exception.name).to(be('ValidateError'))

    def test_can_convert_to_end_user_when_it_has_multiple_addresses_and_email_matches(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            email = 'test1@test.com'
            partner_id, address_id = \
                self._create_partner(txn, 'Prueba1', 'ESZ4200525V', email, '961111111', False)
            partner_address_obj = self.openerp.pool.get('res.partner.address')
            partner_address_obj.create(txn.cursor, txn.user, {
                'partner_id': partner_id,
                'email': email
            })

            # Act
            partner_obj = self.openerp.pool.get('res.partner')
            result = partner_obj.write(txn.cursor, txn.user, [partner_id], {
                'is_end_user': '1'
            })

            # Assert
            expect(result).to(be_true)

    def test_can_convert_to_end_user_when_it_has_multiple_addresses_and_email_does_not_match(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            partner_id, address_id = \
                self._create_partner(txn, 'Prueba1', 'ESZ4200525V', 'test1@test.com', '961111111', False)
            partner_address_obj = self.openerp.pool.get('res.partner.address')
            partner_address_obj.create(txn.cursor, txn.user, {
                'partner_id': partner_id,
                'email': ''
            })

            # Act
            partner_obj = self.openerp.pool.get('res.partner')
            result = partner_obj.write(txn.cursor, txn.user, [partner_id], {
                'is_end_user': '1'
            })

            # Assert
            expect(result).to(be_true)


class PartnerComputedPropertiesTests(testing.OOTestCase):
    """ Test find user matching """

    def test_can_get_customer_type(self):
        with Transaction().start(self.database) as txn:

            particular_id, a1 = create_partner_and_address(
                self.openerp.pool, txn.cursor, txn.user,
                partner_data={'name': 'De la Calzada, Chiquito', 'vat': 'ES00294276Z'})

            business_id, a2 = create_partner_and_address(
                self.openerp.pool, txn.cursor, txn.user,
                partner_data={'name': 'ACME', 'vat': 'ESN9485722D'})

            partner_obj = self.openerp.pool.get('res.partner')

            particular = partner_obj.browse(txn.cursor, txn.user, particular_id)
            expect(particular.customer_type).to(equal('particular'))

            business = partner_obj.browse(txn.cursor, txn.user, business_id)
            expect(business.customer_type).to(equal('business'))

    def test_can_get_customer_first_name_and_surnames(self):
        with Transaction().start(self.database) as txn:

            particular_id, a1 = create_partner_and_address(
                self.openerp.pool, txn.cursor, txn.user,
                partner_data={'name': 'De la Calzada, Chiquito', 'vat': 'ES00294276Z'})

            business_id, a2 = create_partner_and_address(
                self.openerp.pool, txn.cursor, txn.user,
                partner_data={'name': 'ACME', 'vat': 'ESN9485722D'})

            partner_obj = self.openerp.pool.get('res.partner')

            particular = partner_obj.browse(txn.cursor, txn.user, particular_id)
            expect(particular.first_name).to(equal('Chiquito'))
            expect(particular.surnames).to(equal('De la Calzada'))

            business = partner_obj.browse(txn.cursor, txn.user, business_id)
            expect(business.first_name).to(equal('ACME'))
            expect(business.surnames).to(be_false)

