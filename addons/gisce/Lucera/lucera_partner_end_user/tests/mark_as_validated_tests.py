# -*- coding: utf-8 -*-
import unittest
from destral import testing
from destral.transaction import Transaction
from expects import *
from osv import osv, fields
from lucera_common.testing import *

import utils


class MarkEmailAndUserAsValidatedTests(testing.OOTestCase):
    def _create_partner(self, txn, name, vat, email, phone, is_end_user, is_customer=True, is_active=True):
        return utils.create_partner(self.openerp, txn, name, vat, email, phone, is_end_user, is_customer, is_active)

    def test_can_mark_email_as_validated(self):
        with Transaction().start(self.database) as txn:
            partner_id, address_id = \
                self._create_partner(txn, 'Prueba1', 'ES05153714N', 'test1@t.com', '(+34)961111111', True)
            address_obj = self.openerp.pool.get('res.partner.address')
            address = address_obj.browse(txn.cursor, txn.user, address_id)
            expect(address.is_email_validated).to(be_false)
            address_obj.mark_email_as_validated(txn.cursor, txn.user, [address_id], 'test1@t.com')
            address = address_obj.browse(txn.cursor, txn.user, address_id)
            expect(address.is_email_validated).to(be_true)

    def test_can_mark_phone_as_validated(self):
        with Transaction().start(self.database) as txn:
            partner_id, address_id = \
                self._create_partner(txn, 'Prueba1', 'ES05153714N', 'test1@t.com', '(+34)961111111', True)
            address_obj = self.openerp.pool.get('res.partner.address')
            address = address_obj.browse(txn.cursor, txn.user, address_id)
            expect(address.is_email_validated).to(be_false)
            address_obj.mark_phone_as_validated(txn.cursor, txn.user, [address_id], '961111111')
            address = address_obj.browse(txn.cursor, txn.user, address_id)
            expect(address.is_phone_validated).to(be_true)
