# -*- coding: utf-8 -*-
{
  "name": "Lucera end users",
  "description": """Manage and validate Lucera end users""",
  "version": "0-dev",
  "author": "Lucera",
  "category": "Lucera",
  "depends": ['base',
              'lucera_extend_polissa_comer',
              'lucera_gas_extend_polissa_comer',
              'giscedata_facturacio',
              'giscedata_facturacio_comer',
              'giscedata_polissa_comer',
              'giscegas_polissa_comer',
              'lucera_common'],
  "init_xml": [],
  "demo_xml": ['partner_demo.xml'],
  "update_xml": ['res_partner_view.xml'],
  "active": False,
  "installable": True
}
