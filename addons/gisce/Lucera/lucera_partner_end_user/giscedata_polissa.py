# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _
from lucera_common.normalize import *


class GiscedataPolissa(osv.osv):
    """Extend polissa to validate changes"""
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def _check_is_valid_end_user_addres(self, cursor, uid, address_id, context=None):

        partner_obj = self.pool.get('res.partner')
        partner_address_obj = self.pool.get('res.partner.address')

        if address_id:
            partner_address = partner_address_obj.read(cursor, uid, address_id, ['partner_id'], context)
            partner_id = partner_address['partner_id'][0]
            partner = partner_obj.browse(cursor, uid, partner_id, context)
            if not partner.is_end_user:
                raise osv.except_osv(_('ValidateError'), _('La dirección de notificación '
                                                           'no pertenece a un usuario gestor!'))

    def onchange_notificacio(self, cursor, uid, ids, notificacio, titular,
                             pagador, altre_p, context=None):
        """Checks that notification address belongs to an end user
                """
        res = super(GiscedataPolissa, self).onchange_notificacio(
            cursor, uid, ids,  notificacio, titular, pagador, altre_p, None)

        address_id = res['value']['direccio_notificacio']

        self._check_is_valid_end_user_addres(cursor, uid, address_id, context)

        return res

    def create(self, cursor, uid, vals, context=None):
        # We make sure direccio_notificacio belongs to an end user
        polissa_id = super(GiscedataPolissa, self).create(cursor, uid, vals, context)
        if 'direccio_notificacio' in vals:
            address_id = vals['direccio_notificacio']
            self._check_is_valid_end_user_addres(cursor, uid, address_id, context)

        return polissa_id

    def write(self, cursor, uid, ids, vals, context=None):
        # We make sure direccio_notificacio belongs to an end user
        if not context:
            context = {}
        if isinstance(ids, (int, long)):
            ids = [ids]
        res = super(GiscedataPolissa, self).write(cursor, uid, ids, vals, context)
        if 'direccio_notificacio' in vals:
            address_id = vals['direccio_notificacio']
            self._check_is_valid_end_user_addres(cursor, uid, address_id, context)
        return res

    def user_has_edit_permission(self, cursor, uid, ids, user_id, context=None):
        """
            Checks if the user has permissions to edit this contract
        """
        if not context:
            context = {}
        if isinstance(ids, (int, long)):
            ids = [ids]
        polissas = self.browse(cursor, uid, ids, context=context)
        for p in polissas:
            if p.direccio_notificacio.partner_id.id != user_id:
                return False
        return True


GiscedataPolissa()
