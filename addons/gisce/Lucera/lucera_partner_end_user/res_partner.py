# -*- coding: utf-8 -*-
from datetime import datetime

from giscedata_ot_comptadors.wizard.introduir_lectures_comptador import _save_pot
from osv import osv, fields
from tools.translate import _
from lucera_common.normalize import *
from lucera_common.validate import *
from fuzzywuzzy import fuzz


def _split_full_name(full_name):
    if not full_name:
        return None, None
    if ',' not in full_name:
        return full_name, None
    i = full_name.rfind(',')
    surnames = full_name[:i].strip()
    first_name = full_name[i + 1:].strip()
    return first_name, surnames


class ResPartner(osv.osv):
    """Store is_end_user information and validate is unique"""
    _name = 'res.partner'
    _inherit = 'res.partner'

    _CUSTOMER_TYPES = [
        ('particular', 'Particular'),
        ('business', 'Empresa')
    ]

    @staticmethod
    def _normalize_partner_vals(vals):
        if vals:
            vals = vals.copy()
            if 'vat' in vals:
                vals['vat'] = normalize_vat(vals['vat'])
            if 'name' in vals:
                vals['name'] = vals['name'].strip()
        return vals

    def _check_is_valid_end_user(self, cursor, uid, partner_id, vals, context=None):
        # If it passes is_end_user and it is false, it has nothing to check
        if 'is_end_user' in vals and not vals['is_end_user']:
            return True

        # Initialize the context in case it was not defined
        if not context:
            context = {}

        # Normalize partner vals
        vals = self._normalize_partner_vals(vals)

        # Get is_end_user and vat if are missing
        existing_vals = None
        if 'is_end_user' not in vals or 'vat' not in vals:
            existing_vals = self.read(cursor, uid, partner_id, fields=['is_end_user', 'vat'], context=context)
            vals['is_end_user'] = vals.get('is_end_user', existing_vals['is_end_user'])
            vals['vat'] = vals.get('vat', existing_vals['vat'])

        if vals['is_end_user']:
            vat = vals.get('vat', False)

            # Check vat is valid
            if not is_valid_vat(vat):
                raise osv.except_osv(_('ValidateError'), _('End user has invalid vat {0}!'.format(vat)))

            # Check vat is unique
            search_params = [('id', '<>', partner_id),
                             ('is_end_user', '=', '1'),
                             ('vat', '=', vat)]
            partner_ids = self.search(cursor, uid, search_params)
            if len(partner_ids) > 0:
                raise osv.except_osv(_('ValidateError'), _('End user vat is not unique!'))

            # Check address is valid if it was not an end user before and it is not editing at the same time
            if 'address' not in vals and existing_vals and not existing_vals['is_end_user']:
                address_obj = self.pool.get('res.partner.address')
                search_address_params = [('partner_id', '=', partner_id)]
                address_ids = address_obj.search(cursor, uid, search_address_params, context=context)
                if not len(address_ids):
                    raise osv.except_osv(_('ValidateError'), _('End user must have an active address!'))
                address_id = address_ids[0]
                context['is_end_user'] = vals['is_end_user']
                address_obj.check_is_valid_for_end_user(cursor, uid, address_id, {}, context=context)

        return

    def create(self, cursor, uid, vals, context=None):
        # We always normalize partner vals
        vals = self._normalize_partner_vals(vals)

        return super(ResPartner, self).create(cursor, uid, vals, context)

    def write(self, cursor, uid, ids, vals, context=None):
        # Normalize partner vals
        vals = self._normalize_partner_vals(vals)

        return super(ResPartner, self).write(cursor, uid, ids, vals, context)

    def unlink(self, cursor, uid, ids, context=None):
        """ Do not allow deleting a parter if it is end user of an active contract"""
        if not ids:
            return True
        if isinstance(ids, (int, long)):
            ids = [ids]
        # Check it has no active contracts
        polissa_obj = self.pool.get('giscedata.polissa')

        search_params = [('direccio_notificacio.partner_id', 'in', ids),
                         ('direccio_notificacio.partner_id.is_end_user', '=', True),
                         ('active', '=', True)]
        contracts_found = polissa_obj.search_count(cursor, uid, search_params, context)
        if contracts_found > 0:
            raise osv.except_osv(_('ValidateError'), _('Cannot delete partner with existing active contract!'))

        return super(ResPartner, self).unlink(cursor, uid, ids, context)

    def _check_end_user_valid_vat(self, cursor, uid, ids):
        """Checks vat is valid when it is an end user"""
        for partner in self.browse(cursor, uid, ids):
            if not partner.is_end_user:
                continue
            if not is_valid_vat(partner.vat):
                return False
        return True

    def _check_is_unique_end_user(self, cursor, uid, ids):
        """Checks that only one end user with same vat exists"""
        for partner in self.read(cursor, uid, ids, ['is_end_user', 'vat']):
            if not partner:
                continue
            if not partner['is_end_user']:
                continue
            search_params = [('id', '<>', partner['id']),
                             ('is_end_user', '=', True),
                             ('vat', '=', partner['vat'])]
            if self.search_count(cursor, uid, search_params) > 0:
                return False
        return True

    def _check_valid_end_user_address(self, cursor, uid, ids, context=None):
        """Checks that only one end user with same vat exists"""
        # address_ids = [partner.address[0].id for partner in self.browse(cursor, uid, ids) if partner.is_end_user]
        address_ids = []
        for partner in self.read(cursor, uid, ids, ['is_end_user', 'address']):
            if not partner['is_end_user']:
                continue
            # When user is being created it does not have an address yet, so we ignore it
            if not partner['address']:
                continue
            address_ids.append(partner['address'][0])

        # When user is being created it does not have an address yet, so we ignore it
        if not address_ids:
            return True
        address_obj = self.pool.get('res.partner.address')
        return address_obj.check_is_valid_end_user_address(cursor, uid, address_ids)

    def is_valid_end_user(self, cursor, uid, ids):
        res = {}
        for partner_id in ids:
            res[str(partner_id)] = self._check_is_unique_end_user(cursor, uid, [partner_id]) and \
                                   self._check_valid_end_user_address(cursor, uid, [partner_id])
        return res

    def _get_match_result(self, partner, args):
        match_result = {}
        if partner:
            partner_vat = partner.vat
            partner_name = partner.name.lower()
            args_vat = args['vat']
            match_result = {
                'vat': normalize_vat(partner_vat) == normalize_vat(args['vat']),
                'name': True if (fuzz.ratio(partner_name, args['name'].lower()) > 80) else False
            }
            # Add address if exists
            if partner.address and partner.address[0]:
                match_result['email'] = normalize_email(partner.address[0].email) == normalize_email(args['email'])
                match_result['phone'] = normalize_phone(partner.address[0].phone) == normalize_phone(args['phone'])
            else:
                match_result['email'] = False
                match_result['phone'] = False

            # It is a full match if all fields match excluding the phone
            exact_match = all(value for key, value in match_result.iteritems() if key != 'phone')
            match_type = 'full' if exact_match else 'partial'
        else:
            match_type = 'none'

        return {
            'id': partner.id,
            'match_type': match_type,
            'match_result': match_result
        }

    def match_end_users(self, cursor, uid, args):
        args['vat'] = normalize_vat(args['vat'])
        args['name'] = normalize_name(args['name'])
        args['email'] = normalize_email(args['email'])
        args['phone'] = normalize_phone(args['phone'])

        if args['email'] and args['vat']:
            cursor.execute("select p.id from res_partner p join res_partner_address a on p.id = a.partner_id "
                           "where p.is_end_user = true and (p.vat= %s or a.email = %s)", (args['vat'], args['email']))
        elif args['vat']:
            cursor.execute("select p.id from res_partner p join res_partner_address a on p.id = a.partner_id "
                           "where p.is_end_user = true and p.vat= %s", (args['vat'],))
        elif args['email']:
            cursor.execute("select p.id from res_partner p join res_partner_address a on p.id = a.partner_id "
                           "where p.is_end_user = true and a.email= %s", (args['email'],))
        else:
            return []

        partner_ids = map(lambda x: x[0], cursor.fetchall())

        res = []
        if partner_ids:
            for partner in self.browse(cursor, uid, partner_ids):
                res.append(self._get_match_result(partner, args))

        return res

    def onchange_is_end_user(self, cursor, uid, ids, is_end_user, vat, address_email, address_phone, context=None):
        # Try to make it end user
        if is_end_user:
            is_valid = self.is_valid_end_user(cursor, uid, ids)
            if not is_valid:
                return {'warning': {'is_end_user': 'El DNI/CIF y el email deben ser únicos para un gestor'}}
        return {'values': {'is_end_user': is_end_user}}

    def _get_parsed_name(self, cursor, uid, ids, field_names, arg, context=None):
        res = {}
        for partner in self.read(cursor, uid, ids, ['name'], context):
            first_name, surnames = _split_full_name(partner['name'])
            res[partner['id']] = {
                'first_name': first_name,
                'surnames': surnames
            }
        return res

    def _get_customer_type(self, cursor, uid, ids, field_name, arg, context=None):
        res = {}
        for partner in self.read(cursor, uid, ids, ['vat'], context):
            if partner['vat'] and len(partner['vat']) > 2:
                if partner['vat'][2].isdigit():
                    res[partner['id']] = 'particular'
                else:
                    res[partner['id']] = 'business'
        return res

    def _get_manage_url(self, cursor, uid, ids, field_name, arg, context=None):
        res = {}
        for partner_id in ids:
            res[partner_id] = 'https://manage.lucera.es/customers/{}'.format(partner_id)
        return res

    def _get_dashboard_url(self, cursor, uid, ids, field_name, arg, context=None):
        res = {}
        for partner_id in ids:
            res[partner_id] = 'https://manage.lucera.es/customers/{}/dashboard'.format(partner_id)
        return res

    def _get_reset_password_url(self, cursor, uid, ids, field_name, arg, context=None):
        res = {}
        for partner_id in ids:
            res[partner_id] = 'https://manage.lucera.es/customers/{}/password'.format(partner_id)
        return res

    def get_related_bank_accounts_ids(self, cursor, uid, user_id):
        """
        Get the bank account and holders of the active contracts handled by this user
        """
        contract_obj = self.pool.get('giscedata.polissa')
        search_contracts = [
            ('active', '=', True),
            '|',
            ('direccio_notificacio.partner_id', '=', user_id),
            ('pagador.id', '=', user_id)
                            # ('direccio_notificacio.partner_id.is_end_user', '!=', True),
                            ]

        contract_ids = contract_obj.search(cursor, uid, search_contracts)

        # Search for current user banks
        partner_bank_obj = self.pool.get('res.partner.bank')
        user_bank_accounts_id = partner_bank_obj.search(cursor, uid, ['|', ('partner_id', '=', user_id), ('owner_id', '=', user_id)])
        bank_accounts_id = set(user_bank_accounts_id)

        # Search for other contracts banks
        for contract in contract_obj.browse(cursor, uid, contract_ids):
            if contract.bank and not any(b == contract.bank.id for b in bank_accounts_id):
                bank_accounts_id.add(contract.bank.id)

        return list(bank_accounts_id)

    def set_analytics_user_id_if_empty(self, cursor, uid, user_id, analytics_user_id):
        partner_ids = self.search(cursor, uid, [('id', '=', user_id)])
        if not partner_ids:
            raise osv.except_osv(_('ValidateError'), _('Partner with id {} does not exists'.format(user_id)))
        partner = self.browse(cursor, uid, user_id)
        if not partner.analytics_user_id and analytics_user_id:
            self.write(cursor, uid, user_id, {'analytics_user_id': analytics_user_id})
            return analytics_user_id
        return partner.analytics_user_id

    def _validate_end_user_email(self, cursor, uid, user_id, new_email, partner):
        if not partner:
            raise osv.except_osv(_('ValidateError'), _('Partner with id {} does not exists'.format(user_id)))

        if not partner.is_end_user:
            raise osv.except_osv(_('ValidateError'), _('Partner with id {} is not end user'.format(user_id)))

        if not is_valid_email(new_email):
            raise osv.except_osv(_('ValidateError'), _('new_email is not end user'.format(user_id)))

        partner_address_obj = self.pool.get('res.partner.address')
        address_ids = partner_address_obj.search(cursor, uid, [('email', '=', new_email),
                                                               ('partner_id', '!=', user_id),
                                                               ('partner_id.is_end_user', '=', True)])

        if address_ids:
            raise osv.except_osv(_('ValidateError'), _('Exists other end user with same email'))

        return True

    def is_valid_new_end_user_email(self, cursor, uid, user_id, new_email):
        try:
            partner = self.browse(cursor, uid, user_id)
            self._validate_end_user_email(cursor, uid, user_id, new_email, partner)
            return True,
        except osv.except_osv as e:
            return False, e.value

    def change_end_user_email(self, cursor, uid, user_id, new_email, is_email_validated=True, context=None):

        new_email = normalize_email(new_email)

        partner = self.browse(cursor, uid, user_id)

        self._validate_end_user_email(cursor, uid, user_id, new_email, partner)

        partner.address[0].write({'email': new_email, 'is_email_validated': is_email_validated})

        modcon_date = datetime.now().strftime('%Y-%m-%d')

        for polissa_obj_name in ['giscedata.polissa', 'giscegas.polissa']:
            polissa_obj = self.pool.get(polissa_obj_name)
            polissa_ids = polissa_obj.search(cursor, uid, [
                ('state', '=', 'activa'),
                ('direccio_notificacio.partner_id', '=', user_id)])
            if polissa_ids:
                for polissa_id in polissa_ids:
                    polissa = polissa_obj.browse(cursor, uid, polissa_id)
                    polissa.send_signal('modcontractual')
                    if polissa.modcontractual_activa:
                        if polissa.modcontractual_activa.data_inici != modcon_date:
                            polissa.create_contracte(modcon_date, context=context)
                        else:
                            polissa.modify_contracte(context=context)
        return True

    _columns = {
        'is_end_user': fields.boolean('Usuario', help='Es el cliente que se dio de alta', required=True),
        'analytics_user_id': fields.char('Analytics Id', size=64, help='User Id usado en analítica', required=False),
        # Computed fields
        'email': fields.related('address', 'email',
                                type='char', size=240,
                                string='E-mail', readonly=True),
        'phone': fields.related('address', 'phone',
                                type='char', size=32,
                                string='Teléfono', readonly=True),
        'first_name': fields.function(_get_parsed_name, string='Alias', method=True, type='char', multi='parsed_name'),
        'surnames': fields.function(_get_parsed_name, string='Apellidos', method=True, type='char',
                                    multi='parsed_name'),
        'customer_type': fields.function(_get_customer_type,
                                         string='Empresa',
                                         type='selection',
                                         selection=_CUSTOMER_TYPES,
                                         method=True),
        'manage_url': fields.function(_get_manage_url,
                                      method=True, type='char',
                                      size=250,
                                      string='Manage',
                                      readonly=True),
        'dashboard_url': fields.function(_get_dashboard_url,
                                         method=True, type='char',
                                         size=250,
                                         string='Panel de control',
                                         readonly=True),
        'reset_password_url': fields.function(_get_reset_password_url,
                                              method=True, type='char',
                                              size=250,
                                              string='Restablecer contraseña',
                                              readonly=True)
    }

    _defaults = {
        'is_end_user': lambda *a: False
    }

    _constraints = [
        (
            _check_end_user_valid_vat,
            'Error: Vat is not valid for end user!',
            ['vat']
        ),
        (
            _check_is_unique_end_user,
            'Error: End user must have unique vat, phone and email',
            ['is_end_user']
        ),
        (
            _check_valid_end_user_address,
            'Error: End user must have valid and unique phone and email',
            ['address']
        )
    ]


ResPartner()
