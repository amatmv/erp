# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _
from lucera_common.normalize import *


class GiscegasPolissa(osv.osv):
    """Extend polissa to validate changes"""
    _name = 'giscegas.polissa'
    _inherit = 'giscegas.polissa'

    def _check_is_valid_end_user_addres(self, cursor, uid, address_id, context=None):
        partner_obj = self.pool.get('res.partner')
        partner_address_obj = self.pool.get('res.partner.address')

        if address_id:
            partner_address = partner_address_obj.read(cursor, uid, address_id, ['partner_id'], context)
            partner_id = partner_address['partner_id'][0]
            partner = partner_obj.browse(cursor, uid, partner_id, context)
            if not partner.is_end_user:
                raise osv.except_osv(_('ValidateError'), _('La dirección de notificación '
                                                           'no pertenece a un usuario gestor!'))

    def create(self, cursor, uid, vals, context=None):
        # We make sure direccio_notificacio belongs to an end user
        polissa_id = super(GiscegasPolissa, self).create(cursor, uid, vals, context)
        if 'direccio_notificacio' in vals:
            address_id = vals['direccio_notificacio']
            self._check_is_valid_end_user_addres(cursor, uid, address_id, context)

        return polissa_id

    def write(self, cursor, uid, ids, vals, context=None):
        # We make sure direccio_notificacio belongs to an end user
        res = super(GiscegasPolissa, self).write(cursor, uid, ids, vals, context)
        if 'direccio_notificacio' in vals:
            address_id = vals['direccio_notificacio']
            self._check_is_valid_end_user_addres(cursor, uid, address_id, context)
        return res

GiscegasPolissa()
