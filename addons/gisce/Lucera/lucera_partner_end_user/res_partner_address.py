# -*- coding: utf-8 -*-
from osv import osv, fields
from lucera_common.validate import *


class ResPartnerAddress(osv.osv):
    _name = 'res.partner.address'
    _inherit = 'res.partner.address'

    @staticmethod
    def _normalize_address_vals(vals):
        if vals:
            vals = vals.copy()
            if 'email' in vals:
                vals['email'] = normalize_email(vals['email'])
            if 'phone' in vals:
                vals['phone'] = normalize_phone(vals['phone'])
        return vals

    def create(self, cursor, uid, vals, context=None):
        vals = self._normalize_address_vals(vals)

        return super(ResPartnerAddress, self).create(cursor, uid, vals, context=context)

    def write(self, cursor, uid, ids, vals, context=None):
        vals = self._normalize_address_vals(vals)

        return super(ResPartnerAddress, self).write(cursor, uid, ids, vals, context=context)

    def _check_is_valid_end_user_email(self, cursor, uid, ids):
        """Checks that is a valid email if is end user"""
        for address in self.browse(cursor, uid, ids):
            if not address.partner_id.is_end_user:
                continue
            if not is_valid_email(address.email):
                return False
        return True

    def _check_is_valid_phone_if_set(self, cursor, uid, ids):
        """If the phone is set, it checks that is a valid phone"""
        for address in self.browse(cursor, uid, ids):
            if address.phone and not is_valid_phone(address.phone):
                return False
        return True

    def _check_is_unique_end_user_email(self, cursor, uid, ids):
        """Checks that is a email and phone are unique if is end user"""
        for address in self.browse(cursor, uid, ids):
            if not address.partner_id.is_end_user:
                continue
            # Check email is unique
            if not address.email:
                return False
            search_params = [('id', '<>', address.id),
                             ('partner_id', '<>', address.partner_id.id),
                             ('partner_id.is_end_user', '=', True),
                             ('email', '=', address.email)]
            if self.search_count(cursor, uid, search_params):
                return False
        return True

    def check_is_valid_end_user_address(self, cursor, uid, ids):

        is_valid = self._check_is_valid_end_user_email(cursor, uid, ids) and \
                   self._check_is_valid_phone_if_set(cursor, uid, ids) and \
                   self._check_is_unique_end_user_email(cursor, uid, ids)
        return is_valid

    def mark_email_as_validated(self, cursor, uid, ids, email, context=None):
        """Checks that the email matches and marks it as validated"""
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        for address in self.read(cursor, uid, ids, ['email'], context):
            if normalize_email(email) != normalize_email(address['email']):
                raise osv.except_osv('ValidateError', 'Cannot validate email if it does not match')
        self.write(cursor, uid, ids, {'is_email_validated': True})

    def mark_phone_as_validated(self, cursor, uid, ids, phone, context=None):
        """Checks that the phone matches and marks it as validated"""
        for address in self.read(cursor, uid, ids, ['phone'], context):
            if normalize_phone(phone) != normalize_phone(address['phone']):
                raise osv.except_osv('ValidateError', 'Cannot validate phone if it does not match')
        self.write(cursor, uid, ids, {'is_phone_validated': True})

    _columns = {
        'is_email_validated': fields.boolean('Email validado'),
        'is_phone_validated': fields.boolean('Teléfono validado')
    }

    _defaults = {
        'is_email_validated': lambda *a: False,
        'is_phone_validated': lambda *a: False
    }

    _constraints = [
        (
            _check_is_valid_end_user_email,
            'Error: Invalid email',
            ['email']
        ),
        (
            _check_is_valid_phone_if_set,
            'Error: Invalid phone',
            ['phone']
        ),
        (
            _check_is_unique_end_user_email,
            'Error: Email must be unique for end user',
            ['email']
        )
    ]


ResPartnerAddress()
