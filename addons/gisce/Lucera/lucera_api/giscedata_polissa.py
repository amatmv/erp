# -*- coding: utf-8 -*-
from osv import osv


def _format_iban(string):
    """
    This function removes all characters from given 'string' that isn't a
    alpha numeric and converts it to lower case.
    """
    res = ""
    for char in string:
        if char.isalnum():
            res += char.upper()
    return res


class GiscedataPolissa(osv.osv):
    """Extensió de la pòlissa amb les dades bàsiques de facturació.
    """
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def api_set_iban(self, cursor, uid, ids, iban, bank_account_owner_id, context=None):
        if not context:
            context = {}
        if isinstance(ids, (list, set, tuple)):
            polissa_id = ids[0]
        else:
            polissa_id = ids

        polissa_obj = self.pool.get('giscedata.polissa')
        bank_obj = self.pool.get('res.partner.bank')
        country_obj = self.pool.get('res.country')
        iban = self.www_check_iban(cursor, uid, iban)
        if not iban:
            return False
        country_id = country_obj.search(cursor, uid, [('code', '=', iban[:2])])[0]
        vals_bank = bank_obj.onchange_banco(cursor, uid, [], iban[4:], country_id, context)
        polissa_data = polissa_obj.read(cursor, uid, polissa_id, ['pagador'])
        pagador_id = polissa_data['pagador'][0]
        bank_ids = bank_obj.search(
            cursor, uid, [('iban', '=', iban), ('partner_id', '=', pagador_id)])
        if bank_ids:
            bank_id = bank_ids[0]
        else:
            vals = vals_bank['value']
            vals.update({
                'state': 'iban',
                'country_id': country_id,
                'partner_id': pagador_id,
                'acc_country_id': country_id,
                'iban': iban
            })
            bank_id = bank_obj.create(cursor, uid, vals)
        return self.www_actualitzar_polissa(
            cursor, uid, polissa_id, {'bank': bank_id})

GiscedataPolissa()