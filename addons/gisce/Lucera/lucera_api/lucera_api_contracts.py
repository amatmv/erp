# -*- coding: utf-8 -*-
from datetime import datetime

from lucera_api.utils import map_api_location_from_cups, map_api_location_from_address, \
    get_partner_and_address, create_partner_and_address
from lucera_common.api import *
from lucera_common.validate import *
from tools.translate import _


class LuceraApiBaseContracts(object):
    _polissa_obj_name = ''
    _factura_obj_name = ''

    def _get_payment_mandate_date(self, cursor, uid, polissa_id, context):
        factura_obj = self.pool.get(self._factura_obj_name)
        facturas_ids = factura_obj.search(cursor, uid,
                                          [('polissa_id', '=', polissa_id), ('type', '=', 'out_invoice')],
                                          order='data_final desc',
                                          limit=1)

        if facturas_ids:
            factura = factura_obj.browse(cursor, uid, facturas_ids[0])
            return factura.data_final
        else:
            polissa_obj = self.pool.get(self._polissa_obj_name)
            polissa = polissa_obj.browse(cursor, uid, polissa_id, context)
            
            if polissa.data_alta:
                return polissa.data_alta

            return datetime.now().strftime('%Y-%m-%d')

    def _update_current_mod_con(self, cursor, uid, polissa_id):
        wz_crear_mc_obj = self.pool.get(self._namespace + '.polissa.crear.contracte')
        ctx = {'active_id': polissa_id}
        params = {'accio': 'modificar'}
        wz_id = wz_crear_mc_obj.create(cursor, uid, params, ctx)
        wiz = wz_crear_mc_obj.browse(cursor, uid, wz_id, ctx)
        wiz.action_crear_contracte(ctx)

    def _create_payment_mandate(self, cursor, uid, polissa_id, date=None):
        polissa_obj = self.pool.get(self._polissa_obj_name)
        mandate_vals = polissa_obj.update_mandate(cursor, uid, polissa_id)
        if date:
            mandate_vals['date'] = date
        else:
            mandate_vals['date'] = self._get_payment_mandate_date(cursor, uid, polissa_id)
        mandate_obj = self.pool.get('payment.mandate')
        mandate_id = mandate_obj.create(cursor, uid, mandate_vals)
        return mandate_id

    def _create_modcon(self, cursor, uid, polissa_id, vals, ini, fi):
        polissa_obj = self.pool.get(self._polissa_obj_name)
        polissa = polissa_obj.browse(cursor, uid, polissa_id)
        polissa.send_signal(['modcontractual'])
        polissa_obj.write(cursor, uid, polissa_id, vals)
        wz_crear_mc_obj = self.pool.get(self._polissa_obj_name + '.crear.contracte')

        ctx = {'active_id': polissa_id}
        params = {'duracio': 'nou'}

        wz_id_mod = wz_crear_mc_obj.create(cursor, uid, params, ctx)
        wiz_mod = wz_crear_mc_obj.browse(cursor, uid, wz_id_mod, ctx)
        res = wz_crear_mc_obj.onchange_duracio(
            cursor, uid, [wz_id_mod], wiz_mod.data_inici, wiz_mod.duracio, ctx
        )
        wiz_mod.write({
            'data_inici': ini,
            'data_final': fi
        })
        wiz_mod.action_crear_contracte(ctx)

    def _change_bank_account_to_existing(self, cursor, uid,
                                         user_id, contract_id, partner_bank_id, change_date, context=None):
        if context is None:
            context = {}

        polissa_obj = self.pool.get(self._namespace + '.polissa')
        mandate_obj = self.pool.get('payment.mandate')
        partner_bank_obj = self.pool.get('res.partner.bank')
        partner_obj = self.pool.get('res.partner')
        partner_address_obj = self.pool.get('res.partner.address')

        polissa = polissa_obj.browse(cursor, uid, contract_id, context=context)
        bank = partner_bank_obj.browse(cursor, uid, partner_bank_id)

        if not bank:
            raise osv.except_osv(_('ValidateError'), _('Bank account does not exist!'))

        # Check that is owner or partner of this bank account to avoid hacks
        bank_account_ids = partner_obj.get_related_bank_accounts_ids(cursor, uid, user_id)

        if partner_bank_id not in bank_account_ids:
            raise osv.except_osv(_('ValidateError'),
                                 _('Existing parter bank account must exist for this user!'))

        bank_account_owner_id = bank.owner_id.id
        pagador_id = polissa.pagador.id

        if bank.partner_id.id == pagador_id and (not bank_account_owner_id
                                                 or bank.owner_id.id == bank_account_owner_id):
            bank_new_id = partner_bank_id
        else:
            # We must create a copy of the bank account, but changing the partner and owner
            defaults = {'partner_id': pagador_id, 'owner_id': (bank_account_owner_id or bank.owner_id.id)}
            bank_new_id = partner_bank_obj.copy(cursor, uid, partner_bank_id, defaults)

        mandate_id = self._update_polissa_bank_id(cursor, uid, bank_new_id, change_date, polissa, context)

        return {
            'contract_id': contract_id,
            'bank_account_id': bank_new_id,
            'payment_mandate_id': mandate_id
        }

    def _change_bank_account_to_new(self, cursor, uid,
                                    polissa_id,
                                    iban,
                                    change_date,
                                    pagador_id,
                                    mandate_scheme,
                                    bank_account_owner_id,
                                    bank_account_owner_address_id, payment_mode_inst,
                                    modcon_type=None, context=None):

        def _check_data(polissa_inst, account_iban):

            if polissa.state != 'esborrany':
                if polissa_inst.payment_mode_id and polissa_inst.payment_mode_id.require_bank_account:
                    bank = polissa_inst.bank
                    if not bank:
                        raise osv.except_osv(_(u"ERROR"), _(
                            u"La pòlissa ha de tenir un compte associat."))

                    bank_old_iban = bank.iban.replace(" ", "")

                    if account_iban == bank_old_iban:
                        raise osv.except_osv(_(u"ERROR"), _(
                            u"El compte bancari a canviar ha de ser diferent a "
                            u"l'actual"
                        ))

            # if polissa_inst.state != 'activa':
            #     raise osv.except_osv(_(u"ERROR"), _(
            #         u"La pòlissa ha d'estar activa."))

        if context is None:
            context = {}

        polissa_obj = self.pool.get(self._polissa_obj_name)
        mandate_obj = self.pool.get('payment.mandate')
        bank_obj = self.pool.get('res.partner.bank')
        partner_obj = self.pool.get('res.partner')
        partner_address_obj = self.pool.get('res.partner.address')
        polissa = polissa_obj.browse(cursor, uid, [polissa_id], context=context)[0]

        # -- If it is same IBAN, don't do anything
        if polissa.payment_mode_id and polissa.payment_mode_id.require_bank_account and polissa.bank:
            bank_old_iban = normalize_bank_account_number(polissa.bank.iban)

            if iban == bank_old_iban:
                mandate_reference = self._get_mandate_reference(polissa_id)
                mandate_obj = self.pool.get('payment.mandate')
                mandate_ids = mandate_obj.search(
                    cursor, uid,
                    [('reference', '=', mandate_reference), ('date_end', '=', None)],
                    context=context
                )

                if mandate_ids:
                    mandate_id = mandate_ids[0]
                else:
                    False

                return {
                    'contract_id': polissa_id,
                    'bank_account_id': polissa.bank.id,
                    'payment_mandate_id': mandate_id
                }

        # -- Verificar que les dades son coherents.
        _check_data(polissa, iban)

        # Guardar les noves dades bancaries
        account_owner_inst = partner_obj.browse(cursor, uid, bank_account_owner_id)
        owner_address_inst = partner_address_obj.browse(cursor, uid, bank_account_owner_address_id)

        bank_new_id = bank_obj._find_or_create_bank( cursor, uid, pagador_id, iban,
                                                     account_owner_inst, owner_address_inst, context=context)

        mandate_id = self._update_polissa_bank_id(cursor, uid, bank_new_id, change_date, polissa, context)

        return {
            'contract_id': polissa_id,
            'bank_account_id': bank_new_id,
            'payment_mandate_id': mandate_id
        }

    def _update_polissa_bank_id(self, cursor, uid, bank_new_id, change_date, polissa_inst, context=None):
        # Update polissa with a modcon
        params = {
            'bank': bank_new_id,
        }

        if polissa_inst.payment_mode_id:
            params.update({
                'payment_mode_id': polissa_inst.payment_mode_id.id,
                'tipo_pago': polissa_inst.payment_mode_id.type.id
            })

        modcon_date = change_date

        if polissa_inst.state == 'esborrany' or polissa_inst.modcontractual_activa.data_inici == modcon_date:
            modcon_type = 'current'
        else:
            modcon_type = 'new'

        self._update_polissa_with_modcon(cursor, uid, polissa_inst.id, params, modcon_type, modcon_date, context)

        # Mandate
        mandate_id = self._update_polissa_mandate(cursor, uid, polissa_inst.id, context=context)

        return mandate_id

    def _update_polissa_with_modcon(self, cursor, uid, polissa_id, params, modcon_type, modcon_date, context=None):

        polissa_obj = self.pool.get(self._polissa_obj_name)

        polissa = polissa_obj.browse(cursor, uid, polissa_id, context)

        if polissa.state == 'esborrany':
            polissa.write(params, context=context)
        else:
            # -- Crear modificació contractual
            polissa.send_signal('modcontractual')
            polissa.write(params, context=context)
            # -- Crear o modificar el contracte
            if modcon_type == 'new':
                polissa.create_contracte(modcon_date, context=context)
            elif modcon_type == 'current':
                polissa.modify_contracte(context=context)

    @lucera_api_method(body='bank_account')
    def update_bank_account(self, cursor, uid, user_id, contract_ids, bank_account, context=None):
        """
        Update bank accounts for multiple contracts
        """

        if not contract_ids or not bank_account:
            raise Exception("Contract ids or bank account not set")

        if isinstance(contract_ids, (int, long)):
            contract_ids = [contract_ids]

        bank_account_owner = bank_account.get('owner', {})
        if bank_account_owner and (bank_account_owner['rel'] not in ('user', 'fiscal_customer', 'existing')
                                   and not bank_account_owner['user_declares_owner_permission']):
            raise osv.except_osv(_('ValidateError'), _('User must have bank account owner permission!'))

        polissa_obj = self.pool.get(self._polissa_obj_name)
        partner_bank_obj = self.pool.get('res.partner.bank')
        country_obj = self.pool.get('res.country')
        res_bank_obj = self.pool.get('res.bank')
        partner_obj = self.pool.get('res.partner')
        partner_address_obj = self.pool.get('res.partner.address')

        change_contract_banks = {}
        bank_account_owner_id = False
        bank_account_owner_address_id = False

        for contract_id in contract_ids:
            polissa = polissa_obj.browse(cursor, uid, contract_id)
            if not polissa:
                raise osv.except_osv(_('ValidateError'), _('Contract not found!'))

            if polissa.direccio_notificacio.partner_id.id != user_id:
                raise osv.except_osv(_('ValidateError'), _('User has no permissions to change contract!'))

            user_address_id = polissa.direccio_notificacio.id
            partner_id = polissa.pagador.id
            fiscal_customer_id = polissa.pagador.id
            fiscal_customer_address_id = polissa.direccio_pagament.id

            # Location
            bank_account_owner_location = False
            if bank_account_owner:
                if bank_account_owner['location']['rel'] == 'supply_point':
                    bank_account_owner_location = map_api_location_from_cups(cursor, uid, polissa.cups)
                elif bank_account_owner['location']['rel'] == 'fiscal_customer':
                    bank_account_owner_location = map_api_location_from_address(cursor, uid,
                                                                                polissa.pagador.address[0])
                else:
                    bank_account_owner_location = bank_account_owner.get('location', {})

            # Owner
            if bank_account_owner:
                if bank_account_owner['rel'] == 'user':
                    bank_account_owner_id = user_id
                    bank_account_owner_address_id = user_address_id
                elif bank_account_owner['rel'] == 'fiscal_customer':
                    bank_account_owner_id = fiscal_customer_id
                    bank_account_owner_address_id = fiscal_customer_address_id
                elif bank_account_owner['rel'] == 'existing':
                    bank_account_owner_id, bank_account_owner_address_id = \
                        get_partner_and_address(self.pool, cursor, uid, bank_account_owner['id'])
                elif bank_account_owner['rel'] == 'none':
                    # If it is the second contract modified, then we can reuse the bank account
                    if not bank_account_owner_id and not bank_account_owner_address_id:
                        bank_account_owner_id, bank_account_owner_address_id = \
                            create_partner_and_address(self.pool, cursor, uid, bank_account_owner,
                                                       bank_account_owner_location)
                else:
                    raise NotImplementedError("Rel for bank account owner not implemented: {}"
                                              .format(bank_account_owner['rel']))
            else:
                bank_account_owner_id = False

            """Creates or returns a partner bank account"""
            bank_account_owner_location = False
            data_activacio = str(datetime.today().date())

            if bank_account['rel'] == 'existing':
                partner_bank_id = int(bank_account['id'])

                change_contract_banks[contract_id] = self._change_bank_account_to_existing(cursor, uid,
                                                                                           user_id,
                                                                                           contract_id,
                                                                                           partner_bank_id,
                                                                                           data_activacio,
                                                                                           context)
            else:
                if 'iban_code' in bank_account or 'account_number' in bank_account:
                    iban_code = normalize_bank_account_number(bank_account.get('iban_code',
                                                                               bank_account.get('account_number')))
                    change_contract_banks[contract_id] = self._change_bank_account_to_new(cursor, uid, contract_id,
                                                                                          iban_code,
                                                                                          data_activacio,
                                                                                          partner_id,
                                                                                          'core',
                                                                                          bank_account_owner_id,
                                                                                          bank_account_owner_address_id,
                                                                                          polissa.payment_mode_id)
                else:
                    change_contract_banks[contract_id] = False

        result = {
            # 'changed_contract_banks': {str(k): v for (k, v) in change_contract_banks.items()}  # Keys as strings
            'changed_contract_banks': change_contract_banks.values()  # Keys as strings
        }

        return result

    def _get_mandate_reference(self, polissa_id):
        return self._polissa_obj_name + "," + str(polissa_id)

    def _update_polissa_mandate(self, cursor, uid, polissa_id, mandate_scheme='core', context=None):
        mandate_date = self._get_payment_mandate_date(cursor, uid, polissa_id, context=context)
        mandate_reference = self._get_mandate_reference(polissa_id)
        mandate_obj = self.pool.get('payment.mandate')
        mandate_ids = mandate_obj.search(
            cursor, uid,
            [('reference', '=', mandate_reference), ('date_end', '=', None)],
            context=context
        )

        if mandate_ids:
            # Check if start date is the same. In that case we must delete previous
            current_mandate = mandate_obj.browse(cursor, uid, mandate_ids[0])

            # We don't close mandates until we are sure we know when to close them
            # if current_mandate.date == mandate_date:
            #     # mandate_obj.unlink(cursor, uid, [current_mandate.id], context)
            #     mandate_obj._close_mandate(cursor, uid, mandate_ids, current_mandate.date, context=context)
            # else:
            #     # -- Finalitzar el mandato
            #     mandate_close_date = datetime.strptime(mandate_date, '%Y-%m-%d') - timedelta(days=1)
            #     mandate_obj._close_mandate(cursor, uid, mandate_ids, mandate_close_date, context=context)

        # Add new mandate
        mandate_new_id = mandate_obj._create_mandate(
            cursor, uid, mandate_date, mandate_reference, mandate_scheme,
            context=context
        )
        return mandate_new_id
