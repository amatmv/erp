# -*- coding: utf-8 -*-
from lucera_api.lucera_api_contracts import LuceraApiBaseContracts
from lucera_common.api import lucera_api_method
from lucera_common.config import LUCERA_ELECTRICITY_ALLOWED_TARIFF_OCSUM_CODES
from lucera_common import check
from osv import osv
from tools.translate import _


class LuceraApiElectricityContracts(osv.osv_memory, LuceraApiBaseContracts):
    _name = 'lucera.api.electricity.contracts'

    _namespace = 'giscedata'
    _polissa_obj_name = '{}.polissa'.format(_namespace)
    _factura_obj_name = '{}.facturacio.factura'.format(_namespace)

    @lucera_api_method()
    def request_change_tariff_and_power(self, cursor, uid, user_id, contract_id, tariff, power, context=None):

        if not tariff and not power:
            raise osv.except_osv('ValidateError', 'Tariff or power not set')
        
        polissa_obj = self.pool.get('giscedata.polissa')
        sw_obj = self.pool.get('giscedata.switching')

        polissa = polissa_obj.browse(cursor, uid, contract_id, context)

        if not polissa_obj.user_has_edit_permission(cursor, uid, contract_id, user_id):
            raise osv.except_osv(_('ValidateError'), _('User has not enough permissions!'))

        tariff = tariff or polissa.tarifa.codi_ocsum
        if tariff not in LUCERA_ELECTRICITY_ALLOWED_TARIFF_OCSUM_CODES:
            raise osv.except_osv('ValidateError', 'Not allowed tariff')

        power = power or polissa.potencia

        check.is_valid_power_and_tariff(self.pool, cursor, uid, tariff, power)

        search_sw_params = [('cups_polissa_id', '=', contract_id),
                            ('proces_id.name', '=', 'M1'),
                            ('state', 'not in', ('done', 'cancel'))]

        if sw_obj.search_count(cursor, uid, search_sw_params) > 0:
            raise osv.except_osv('ValidateError', 'Power and tariff change already in progress')

        wiz_obj = self.pool.get('giscedata.switching.mod.con.wizard')
        wiz_id = wiz_obj.create(cursor, uid, {
            'change_atr': True,
            'tariff': tariff,
            'power_p1': power,
            'activacio_cicle': 'A'
        }, context={'cas': 'M1', 'pol_id': contract_id})
        wiz = wiz_obj.browse(cursor, uid, wiz_id, context=context)
        wiz.genera_casos_atr()

        switching_cases = sw_obj.search(cursor, uid, search_sw_params)

        result = {
            'success': (len(switching_cases) > 0),
            'current': {
                'power': polissa.potencia,
                'tariff': polissa.tarifa.codi_ocsum
            },
            'new': {
                'power': power,
                'tariff': tariff
            },
            'switching_cases': switching_cases
        }

        return result


LuceraApiElectricityContracts()
