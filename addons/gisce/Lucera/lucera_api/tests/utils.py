# -*- coding: utf-8 -*-
import os
from lucera_common.config import *


def get_request(name):
    dir_path = os.path.dirname(os.path.realpath(__file__))
    file_path = os.path.join(dir_path, 'data', name)
    return open(file_path, 'r').read()


def setup_tarifa(pool, cursor, uid):
    user = pool.get('res.users').browse(cursor, uid, uid)
    if not user.company_id.partner_id.ref:
        partner_id = pool.get('ir.model.data').get_object_reference(
            cursor, uid, 'base', 'main_partner'
        )[1]
        pool.get('res.partner').write(cursor, uid, [partner_id], {
            'ref': '0971'
        })

    giscedata_polissa_tarifa_obj = pool.get('giscedata.polissa.tarifa')

    # Add relation between giscedata_polissa_tarifa and pricelist
    sql = """insert into giscedata_polissa_tarifa_pricelist (tarifa_id, pricelist_id)
             select a.id, (select id from product_pricelist where name = '{}' and active=True limit 1)
             from (select DISTINCT id from giscedata_polissa_tarifa) a """.format(LUCERA_PRODUCT_PRICE_LIST_NAME)
    cursor.execute(sql)
