# -*- coding: utf-8 -*-
from destral import testing
from lucera_api.tests.lucera_api_base_contracts_tests import LuceraApiBaseContractsTests


class LuceraApiElectricityContractsTest(testing.OOTestCase, LuceraApiBaseContractsTests):
    _type = 'electricity'  # electricity | gas
