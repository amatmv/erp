# -*- coding: utf-8 -*-
import unittest
from destral import testing
from destral.transaction import Transaction
from expects import *
from datetime import datetime, timedelta
from lucera_common.config import *
from lucera_common.testing import *
from osv import osv, fields, orm
import os
import json


class LuceraApiPricesTests(testing.OOTestCase):

    @unittest.skip("TODO")
    def test_can_get_all_atr_prices_for_a_kind(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            uid = txn.user
            pool = self.openerp.pool
            currency_id = pool.get('res.currency').search(cursor, uid, [])[0]
            product_obj = pool.get('product.product')
            pricelist_obj = pool.get('product.pricelist')
            pricelist_version_obj = pool.get('product.pricelist.version')
            pricelist_item_obj = pool.get('product.pricelist.item')
            pricelist_id = pricelist_obj.create(cursor, uid, {
                                           'name': 'Tarifa Precio Variable',
                                           'type': 'sale',
                                           'kind': 'variable_price',
                                           'currency_id': currency_id})

            priceversion_id = pricelist_version_obj.create(cursor, uid, {
                                                           'name': 'Variable Enero 2018',
                                                           'date_start': '2018-01-01',
                                                           'date_end': '2018-01-31',
                                                           'pricelist_id': pricelist_id
                                                           })

            categ_id = pool.get('ir.model.data').get_object_reference(
                cursor, uid, 'giscedata_polissa', 'categ_e_t20A_new'
            )[1]
            product_id = product_obj.search(cursor, uid, [('name', '=', 'P1'), ('categ_id', '=', categ_id)])

            pricelist_item_obj.create(cursor, uid, {
                                      'name': 'P1 - 2.0A - Energía',
                                      'price_version_id': priceversion_id,
                                      'product_id': product_id,
                                      'price_discount': -1,
                                      'price_surcharge': 0.10,
                                      'base': 1,
                                      })

            tarifa_obj = pool.get('giscedata.polissa.tarifa')
            tarifas = tarifa_obj.browse(cursor, uid, tarifa_obj.search(cursor, uid, []))
            for tarifa in tarifas:
                if pricelist_id not in [x.id for x in tarifa.llistes_preus_comptatibles]:
                    # 4 és l'id de la tarifa de preus d'electricitat
                    tarifa.write({'llistes_preus_comptatibles': [(4, pricelist_id), ]})

            api_prices_obj = pool.get('lucera.api.prices')

            # Act
            response = api_prices_obj.all_atr_prices(cursor, uid, 'variable_price', datetime.now())

            # Assert
            expect(response).not_to(be_false)

    @unittest.skip("TODO test set up")
    def test_can_get_all_current_prices(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            uid = txn.user
            pool = self.openerp.pool

            api_prices_obj = pool.get('lucera.api.prices')

            # Act
            response = api_prices_obj.atr_prices_by_date(cursor, uid, 'variable_price', datetime.now() - timedelta(days=365), datetime.now())

            # Assert
            expect(response).not_to(be_false)