# -*- coding: utf-8 -*-
import unittest
from destral import testing
from destral.transaction import Transaction
from expects import *
from datetime import datetime, timedelta
from lucera_common.config import *
from lucera_common.testing import *
from osv import osv, fields, orm
import os
import json
from lucera_api.tests.lucera_api_base_contracts_tests import LuceraApiBaseContractsTests
from osv.osv import except_osv

LUCERA_REE_CODE = '0971'


class LuceraApiElectricityChangeAtrTest(testing.OOTestCaseWithCursor):

    def _setup_company_ree(self):
        user_obj = self.pool.get('res.users')
        user = user_obj.browse(self.cursor, self.uid, self.uid)
        if not user.company_id.partner_id.ref:
            user.company_id.partner_id.write({'ref': LUCERA_REE_CODE})

    # noinspection PyPep8Naming
    def setUp(self):
        super(LuceraApiElectricityChangeAtrTest, self).setUp()
        self.pool = self.openerp.pool
        self._setup_company_ree()

    def test_can_request_change_tariff_and_power(self):
        # Arrange
        cursor = self.cursor
        uid = self.uid

        create_result = create_test_electricity_polissa(self.pool, cursor, uid, override_vals={'state': 'activa'})
        user_id = create_result['user_id']
        polissa_id = create_result['polissa_id']
        polissa = self.pool.get('giscedata.polissa').browse(cursor, uid, polissa_id)

        # Act
        tariff = '004'  # 2.0DHA
        power = 3100
        api_obj = self.pool.get('lucera.api.electricity.contracts')
        result = api_obj.request_change_tariff_and_power(cursor, uid, user_id,
                                                         polissa_id, tariff, power)

        # Assert
        expect(result['current']['power']).to(equal(polissa.potencia))
        expect(result['current']['tariff']).to(equal(polissa.tarifa.codi_ocsum))
        expect(result['new']['power']).to(equal(power))
        expect(result['new']['tariff']).to(equal(tariff))
        expect(result['switching_cases']).not_to(be_empty)

    def test_cannot_request_change_tariff_and_power_if_it_is_current_tariff_and_power(self):

        # Arrange
        cursor = self.cursor
        uid = self.uid
        pool = self.pool
        create_result = create_test_electricity_polissa(pool, cursor, uid, override_vals={'state': 'activa'})
        user_id = create_result['user_id']
        polissa_id = create_result['polissa_id']
        polissa = pool.get('giscedata.polissa').browse(cursor, uid, polissa_id)

        # Act
        tariff = polissa.tarifa.codi_ocsum
        power = polissa.potencia * 1000

        api_obj = pool.get('lucera.api.electricity.contracts')
        with self.assertRaises(except_osv) as cm:
            result = api_obj.request_change_tariff_and_power(cursor, uid, user_id, polissa_id, tariff, power)

        # Assert
        expect(cm.exception.name).to(equal('Canvi de potència o tarifa'))

    def test_cannot_request_change_tariff_and_power_if_it_is_power_is_not_in_tariff_range(self):
        # Arrange
        cursor = self.cursor
        uid = self.uid
        pool = self.pool
        create_result = create_test_electricity_polissa(pool, cursor, uid, override_vals={'state': 'activa'})
        user_id = create_result['user_id']
        polissa_id = create_result['polissa_id']
        polissa = pool.get('giscedata.polissa').browse(cursor, uid, polissa_id)

        # Act
        tariff = polissa.tarifa.codi_ocsum
        power = 17000

        api_obj = pool.get('lucera.api.electricity.contracts')
        with self.assertRaises(Exception) as cm:
            result = api_obj.request_change_tariff_and_power(cursor, uid, user_id, polissa_id, tariff, power)

        # Assert
        expect(cm.exception.name).to(be('ValidateError'))

    def test_cannot_request_change_tariff_and_power_if_tariff_is_not_accepted(self):
        # Arrange
        cursor = self.cursor
        uid = self.uid
        pool = self.pool
        create_result = create_test_electricity_polissa(pool, cursor, uid, override_vals={'state': 'activa'})
        user_id = create_result['user_id']
        polissa_id = create_result['polissa_id']
        polissa = pool.get('giscedata.polissa').browse(cursor, uid, polissa_id)

        # Act
        tariff = '003'  # 3.0A
        power = polissa.potencia * 1000

        api_obj = pool.get('lucera.api.electricity.contracts')
        with self.assertRaises(Exception) as cm:
            result = api_obj.request_change_tariff_and_power(cursor, uid, user_id, polissa_id, tariff, power)

        # Assert
        expect(cm.exception.value).to(equal('Not allowed tariff'))

    def test_cannot_request_change_tariff_and_power_if_there_is_tariff_and_power_change_in_progress(self):
        # Arrange
        cursor = self.cursor
        uid = self.uid

        create_result = create_test_electricity_polissa(self.pool, cursor, uid, override_vals={'state': 'activa'})
        user_id = create_result['user_id']
        polissa_id = create_result['polissa_id']
        polissa = self.pool.get('giscedata.polissa').browse(cursor, uid, polissa_id)
        tariff = '004'  # 2.0DHA
        power = 3100
        api_obj = self.pool.get('lucera.api.electricity.contracts')
        result = api_obj.request_change_tariff_and_power(cursor, uid, user_id,
                                                         polissa_id, tariff, power)

        # Act
        with self.assertRaises(Exception) as cm:
            result = api_obj.request_change_tariff_and_power(cursor, uid, user_id,
                                                             polissa_id, tariff, power)

        # Assert
        expect(cm.exception.value).to(equal('Power and tariff change already in progress'))
