# -*- coding: utf-8 -*-
import unittest

from destral.transaction import Transaction
from expects import *
from lucera_common.fake import get_random_iban
from lucera_common.testing.common import create_bank, create_bank_account, TESTING_DEFAULT_DATE
from lucera_common.testing.electricity import create_test_electricity_polissa
from lucera_common.testing.gas import create_test_gas_polissa
from datetime import datetime, timedelta

class LuceraApiBaseContractsTests(object):

    _type = None  # electricity | gas

    def get_api_obj(self):
        return self.openerp.pool.get('lucera.api.{}.contracts'.format(self._type))

    def _get_namespace(self):
        if self._type == 'electricity':
            return 'giscedata'
        else:
            return 'giscegas'

    def _get_polissa_obj_name(self):
        return '{}.polissa'.format(self._get_namespace())

    def _get_polissa_obj(self):
        return self.openerp.pool.get(self._get_polissa_obj_name())

    def create_test_polissa(self, *args, **kwargs):
        if self._type == 'electricity':
            result = create_test_electricity_polissa(*args, **kwargs)
            return result["polissa_id"], result
        else:
            result = create_test_gas_polissa(*args, **kwargs)
            return result["gas_polissa_id"], result

    # def create_test_invoice(self, *args, **kwargs):create_test_invoice
    #     if self._type == 'electricity':
    #         result = create_test_electricity_polissa(*args, **kwargs)
    #         return result["polissa_id"], result
    #     else:
    #         result = create_test_gas_polissa(*args, **kwargs)
    #         return result["gas_polissa_id"], result

    def test_can_change_single_active_polissa_bank_account(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            uid = txn.user
            pool = self.openerp.pool
            data_alta = (TESTING_DEFAULT_DATE - timedelta(days=10)).strftime('%Y-%m-%d')
            change_date = datetime.now().strftime('%Y-%m-%d')
            polissa_id, result = self.create_test_polissa(pool, cursor, uid, override_vals={'state': 'activa',
                                                                                            'data_alta': data_alta})
            user_id = result['user_id']
            bank_id = create_bank(pool, cursor, uid)

            # Act
            iban_code = get_random_iban()
            api_obj = self.get_api_obj()
            bank_account = {
                    'rel': 'none',
                    'iban_code': iban_code,
                    'owner': {
                        'rel': 'user',
                        'location': {
                            'rel': 'user'
                        }
                    }
                }

            api_result = api_obj.update_bank_account(cursor, uid, result['user_id'], [polissa_id], bank_account)

            # Assert Polissa
            polissa_obj = self._get_polissa_obj()
            polissa = polissa_obj.browse(cursor, uid, polissa_id)
            expect(polissa.bank.iban).to(equal(bank_account['iban_code']))
            expect(polissa.bank.owner_id.id).to(equal(user_id))

            # Assert Polissa change was created

            # Assert Mandate
            mandate_obj = pool.get('payment.mandate')
            mandate_reference = '{},{}'.format(self._get_polissa_obj_name(), polissa_id)
            mandate_ids = mandate_obj.search(cursor, uid, [('debtor_iban', '=', iban_code),
                                                           ('reference', '=', mandate_reference)])
            expect(len(mandate_ids)).to(equal(1))
            mandate = mandate_obj.browse(cursor, uid, mandate_ids[0])
            expect(mandate.debtor_iban).to(equal(iban_code))
            expect(mandate.date).to(equal(data_alta))

            # Assert Modcon
            expect(len(polissa.modcontractuals_ids)).to(equal(2))
            expect(polissa.modcontractuals_ids[0].bank.iban).to(equal(iban_code))
            expect(polissa.modcontractuals_ids[0].data_inici).to(equal(change_date))

    def test_can_change_single_esborrany_polissa_bank_account(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            uid = txn.user
            pool = self.openerp.pool
            data_alta = (TESTING_DEFAULT_DATE - timedelta(days=10)).strftime('%Y-%m-%d')
            change_date = datetime.now().strftime('%Y-%m-%d')
            polissa_id, result = self.create_test_polissa(pool, cursor, uid, override_vals={'data_alta': data_alta})
            user_id = result['user_id']
            bank_id = create_bank(pool, cursor, uid)

            # Act
            iban_code = get_random_iban()
            api_obj = self.get_api_obj()
            bank_account = {
                    'rel': 'none',
                    'iban_code': iban_code,
                    'owner': {
                        'rel': 'user',
                        'location': {
                            'rel': 'user'
                        }
                    }
                }

            api_result = api_obj.update_bank_account(cursor, uid, result['user_id'], [polissa_id], bank_account)

            # Assert Polissa
            polissa_obj = self._get_polissa_obj()
            polissa = polissa_obj.browse(cursor, uid, polissa_id)
            expect(polissa.bank.iban).to(equal(iban_code))
            expect(polissa.bank.owner_id.id).to(equal(user_id))

            # Assert Mandate
            mandate_obj = pool.get('payment.mandate')
            mandate_reference = '{},{}'.format(self._get_polissa_obj_name(), polissa_id)
            mandate_ids = mandate_obj.search(cursor, uid, [('debtor_iban', '=', iban_code),
                                                           ('reference', '=', mandate_reference)])
            expect(len(mandate_ids)).to(equal(1))
            mandate = mandate_obj.browse(cursor, uid, mandate_ids[0])
            expect(mandate.debtor_iban).to(equal(iban_code))
            expect(mandate.date).to(equal(data_alta))

            # Assert Modcon
            expect(len(polissa.modcontractuals_ids)).to(equal(0))

    def test_can_change_multiple_polissa_active_bank_accounts(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            uid = txn.user
            pool = self.openerp.pool
            data_alta = (TESTING_DEFAULT_DATE - timedelta(days=10)).strftime('%Y-%m-%d')
            change_date = datetime.now().strftime('%Y-%m-%d')
            polissa_id1, result1 = self.create_test_polissa(pool, cursor, uid, override_vals={'state': 'activa',
                                                                                              'data_alta': data_alta})
            polissa_id2, result2 = self.create_test_polissa(pool, cursor, uid, user_id=result1['user_id'],
                                                            override_vals={'state': 'activa', 'data_alta': data_alta})
            polissa_ids = [polissa_id1, polissa_id2]
            user_id = result1['user_id']
            bank_id = create_bank(pool, cursor, uid)

            # Act
            iban_code = get_random_iban()
            api_obj = self.get_api_obj()
            bank_account = {
                    'rel': 'none',
                    'iban_code': iban_code,
                    'owner': {
                        'rel': 'user',
                        'location': {
                            'rel': 'user'
                        }
                    }
                }

            api_result = api_obj.update_bank_account(cursor, uid, result1['user_id'], polissa_ids, bank_account)

            # Assert Polissa
            polissa_obj = self._get_polissa_obj()

            bank_id = False
            for polissa_id in polissa_ids:
                # Assert Polissa change was created
                polissa = polissa_obj.browse(cursor, uid, polissa_id)
                expect(polissa.bank.iban).to(equal(bank_account['iban_code']))
                expect(polissa.bank.owner_id.id).to(equal(user_id))
                if bank_id:
                    expect(polissa.bank.id).to(equal(bank_id))
                else:
                    bank_id = polissa.bank.id

                # Assert Mandate
                mandate_obj = pool.get('payment.mandate')
                mandate_reference = '{},{}'.format(self._get_polissa_obj_name(), polissa_id)
                mandate_ids = mandate_obj.search(cursor, uid, [('debtor_iban', '=', iban_code),
                                                               ('reference', '=', mandate_reference)])
                expect(len(mandate_ids)).to(equal(1))
                mandate = mandate_obj.browse(cursor, uid, mandate_ids[0])
                expect(mandate.debtor_iban).to(equal(iban_code))
                expect(mandate.date).to(equal(data_alta))

                # Assert Modcon
                expect(len(polissa.modcontractuals_ids)).to(equal(2))
                expect(polissa.modcontractuals_ids[0].bank.iban).to(equal(iban_code))
                expect(polissa.modcontractuals_ids[0].data_inici).to(equal(change_date))

    def test_can_change_polissa_bank_account_to_existing_user_bank_account(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            uid = txn.user
            pool = self.openerp.pool
            data_alta = (TESTING_DEFAULT_DATE - timedelta(days=10)).strftime('%Y-%m-%d')
            change_date = datetime.now().strftime('%Y-%m-%d')
            polissa_id, result = self.create_test_polissa(pool, cursor, uid, override_vals={'state': 'activa',
                                                                                            'data_alta': data_alta})
            user_id = result['user_id']
            bank_id = create_bank(pool, cursor, uid)
            iban_code = get_random_iban()
            bank_account_id = create_bank_account(pool, txn.cursor, txn.user, user_id, iban_code=iban_code)

            # Act
            api_obj = self.get_api_obj()
            bank_account = {
                    'rel': 'existing',
                    'id': bank_account_id
                }
            api_result = api_obj.update_bank_account(cursor, uid, result['user_id'], [polissa_id], bank_account)

            # Assert
            polissa_obj = self._get_polissa_obj()
            polissa = polissa_obj.browse(cursor, uid, polissa_id)
            expect(polissa.bank.id).to(equal(bank_account_id))

            # Assert Mandate
            mandate_obj = pool.get('payment.mandate')
            mandate_reference = '{},{}'.format(self._get_polissa_obj_name(), polissa_id)
            mandate_ids = mandate_obj.search(cursor, uid, [('debtor_iban', '=', iban_code),
                                                           ('reference', '=', mandate_reference)])
            expect(len(mandate_ids)).to(equal(1))
            mandate = mandate_obj.browse(cursor, uid, mandate_ids[0])
            expect(mandate.debtor_iban).to(equal(iban_code))
            expect(mandate.date).to(equal(data_alta))

            # Assert Modcon
            expect(len(polissa.modcontractuals_ids)).to(equal(2))
            expect(polissa.modcontractuals_ids[0].bank.iban).to(equal(iban_code))
            expect(polissa.modcontractuals_ids[0].data_inici).to(equal(change_date))

    def test_can_change_polissa_bank_account_to_new_partner(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            uid = txn.user
            pool = self.openerp.pool
            data_alta = (TESTING_DEFAULT_DATE - timedelta(days=10)).strftime('%Y-%m-%d')
            change_date = datetime.now().strftime('%Y-%m-%d')
            polissa_id, result = self.create_test_polissa(pool, cursor, uid, override_vals={'state': 'activa',
                                                                                            'data_alta': data_alta})
            polissa_obj = self._get_polissa_obj()
            previous_polissa = polissa_obj.browse(cursor, uid, polissa_id)
            previous_polissa_bank_id = previous_polissa.bank.id
            bank_id = create_bank(pool, cursor, uid)

            # Act
            api_obj = self.get_api_obj()
            iban_code = get_random_iban()
            bank_account = {
              "rel": "none",
              "account_number": iban_code,
              "iban_code": iban_code,
              "owner": {
                "rel": "none",
                "type": "company",
                "name": "Test S.L.",
                "identity_document": {
                  "code": "B25163783",
                  "type": "implicit"
                },
                "email": "test@testsl.com",
                "phone": "963232929",
                "user_declares_owner_permission": True,
                "location": {
                  "rel": "none",
                  "address": "PZA MARÍA BENEYTO 13 11",
                  "postal_code": "46008",
                  "town_code": "46250"
                }
              }
            }

            api_result = api_obj.update_bank_account(cursor, uid, result['user_id'], [polissa_id], bank_account)

            # Assert
            polissa = polissa_obj.browse(cursor, uid, polissa_id)
            expect(polissa.bank.id).not_to(equal(previous_polissa_bank_id))
            expect(polissa.bank.owner_id.id).not_to(equal(result['bank_account_owner_id']))
            expect(polissa.bank.owner_id.name).to(equal(bank_account['owner']['name']))
            expect(polissa.bank.owner_id.vat).to(equal('ES' + bank_account['owner']['identity_document']['code']))
            expect(polissa.bank.owner_id.address[0].email).to(equal(bank_account['owner']['email']))
            expect(polissa.bank.owner_id.address[0].phone).to(equal(bank_account['owner']['phone']))
            expect(polissa.bank.owner_id.address[0].zip).to(equal('46008'))

            # Assert Mandate
            mandate_obj = pool.get('payment.mandate')
            mandate_reference = '{},{}'.format(self._get_polissa_obj_name(), polissa_id)
            mandate_ids = mandate_obj.search(cursor, uid, [('debtor_iban', '=', iban_code),
                                                           ('reference', '=', mandate_reference)])
            expect(len(mandate_ids)).to(equal(1))
            mandate = mandate_obj.browse(cursor, uid, mandate_ids[0])
            expect(mandate.debtor_iban).to(equal(iban_code))
            expect(mandate.date).to(equal(data_alta))

            # Assert Modcon
            expect(len(polissa.modcontractuals_ids)).to(equal(2))
            expect(polissa.modcontractuals_ids[0].bank.iban).to(equal(iban_code))
            expect(polissa.modcontractuals_ids[0].data_inici).to(equal(change_date))

    @unittest.skip('Not creating invoices yet')
    def test_can_change_single_active_polissa_bank_account_with_invoice(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            uid = txn.user
            pool = self.openerp.pool
            data_alta = (TESTING_DEFAULT_DATE - timedelta(days=50)).strftime('%Y-%m-%d')
            change_date = datetime.now().strftime('%Y-%m-%d')
            polissa_id, result = self.create_test_polissa(pool, cursor, uid, override_vals={'state': 'activa',
                                                                                            'data_alta': data_alta})
            user_id = result['user_id']
            bank_id = create_bank(pool, cursor, uid)

            self.create_test_invoice(pool, cursor, uid, polissa_id)

            # Act
            iban_code = get_random_iban()
            api_obj = self.get_api_obj()
            bank_account = {
                    'rel': 'none',
                    'iban_code': iban_code,
                    'owner': {
                        'rel': 'user',
                        'location': {
                            'rel': 'user'
                        }
                    }
                }

            api_result = api_obj.update_bank_account(cursor, uid, result['user_id'], [polissa_id], bank_account)

            # Assert Polissa
            polissa_obj = self._get_polissa_obj()
            polissa = polissa_obj.browse(cursor, uid, polissa_id)
            expect(polissa.bank.iban).to(equal(bank_account['iban_code']))
            expect(polissa.bank.owner_id.id).to(equal(user_id))

            # Assert Polissa change was created

            # Assert Mandate
            mandate_obj = pool.get('payment.mandate')
            mandate_reference = '{},{}'.format(self._get_polissa_obj_name(), polissa_id)
            mandate_ids = mandate_obj.search(cursor, uid, [('debtor_iban', '=', iban_code),
                                                           ('reference', '=', mandate_reference)])
            expect(len(mandate_ids)).to(equal(1))
            mandate = mandate_obj.browse(cursor, uid, mandate_ids[0])
            expect(mandate.debtor_iban).to(equal(iban_code))
            expect(mandate.date).to(equal(data_alta))

            # Assert Modcon
            expect(len(polissa.modcontractuals_ids)).to(equal(2))
            expect(polissa.modcontractuals_ids[0].bank.iban).to(equal(iban_code))
            expect(polissa.modcontractuals_ids[0].data_inici).to(equal(change_date))