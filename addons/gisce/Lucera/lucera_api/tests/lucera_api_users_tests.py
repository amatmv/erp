# -*- coding: utf-8 -*-
from datetime import datetime, timedelta

from destral import testing
from destral.transaction import Transaction
from expects import *

from lucera_common.fake import get_random_email
from lucera_common.testing import create_test_electricity_and_gas_polissa, create_partner_and_address
from lucera_common.testing.common import TESTING_DEFAULT_DATE


class LuceraApiUsersTests(testing.OOTestCase):

    def test_can_change_email_from_end_user(self):

        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            uid = txn.user
            pool = self.openerp.pool
            data_alta = (TESTING_DEFAULT_DATE - timedelta(days=10)).strftime('%Y-%m-%d')
            result = create_test_electricity_and_gas_polissa(pool, cursor, uid, override_vals={
                'data_alta': data_alta,
                'state': 'activa'})

            # Act
            new_email = get_random_email()
            user_id = result['user_id']
            api_obj = pool.get('lucera.api.users')
            api_obj.change_email(cursor, uid, user_id, new_email)

            # Assert
            partner_obj = pool.get('res.partner')
            partner = partner_obj.browse(cursor, uid, user_id)
            expect(partner.address[0].email).to(equal(new_email))

            polissa = pool.get('giscedata.polissa').browse(cursor, uid, result['polissa_id'])
            expect(polissa.direccio_notificacio.email).to(equal(new_email))
            expect(polissa.modcontractuals_ids[0].observacions).to(contain(new_email))
            expect(len(polissa.modcontractuals_ids)).to(equal(2))

            gas_polissa = pool.get('giscegas.polissa').browse(cursor, uid, result['gas_polissa_id'])
            expect(gas_polissa.direccio_notificacio.email).to(equal(new_email))
            expect(gas_polissa.modcontractuals_ids[0].observacions).to(contain(new_email))
            expect(len(gas_polissa.modcontractuals_ids)).to(equal(2))

    def test_can_check_if_new_email_is_valid(self):

        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            uid = txn.user
            pool = self.openerp.pool
            existing_email = 'test_existing2@t2.com'
            user_id, address_id = create_partner_and_address(pool, cursor, uid,
                                                             partner_data={'name': 'Prueba1',
                                                                           'vat': 'ES05153714N'},
                                                             address_data={'email': 'test1@t.com',
                                                                           'phone': '(+34)961111111'})
            partner_id2, address_id2 = create_partner_and_address(pool, cursor, uid,
                                                                  partner_data={'name': 'Prueba2',
                                                                                'vat': '99319798W'},
                                                                  address_data={'email': existing_email,
                                                                                'phone': '(+34)961111111'})

            # Act
            api_obj = pool.get('lucera.api.users')

            # Assert
            new_email = get_random_email()
            result = api_obj.is_valid_change_email(cursor, uid, user_id, new_email)
            expect(result['is_valid']).to(be_true)

            result = api_obj.is_valid_change_email(cursor, uid, user_id, existing_email)
            expect(result['is_valid']).to(be_false)
            expect(result['not_valid_reason']).not_to(be_empty)
