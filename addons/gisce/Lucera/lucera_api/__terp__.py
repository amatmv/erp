# -*- coding: utf-8 -*-
{
  "name": "Lucera API",
  "description": """Acts as an interface with the outside work to the ERP for signups""",
  "version": "0-dev",
  "author": "Lucera",
  "category": "Lucera",
  "depends":[
      "base_bank_extended",
      "lucera_common",
      "lucera_partner_end_user",
      "giscedata_facturacio",
      "giscedata_facturacio_comer_lucera",
      "giscegas_facturacio",
      "giscegas_facturacio_comer_lucera",
      "www_base",
  ],
  "init_xml": [],
  "demo_xml": ["lucera_api_demo.xml"],
  "update_xml": ["security/ir.model.access.csv"],
  "active": False,
  "installable": True
}
