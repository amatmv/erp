# -*- coding: utf-8 -*-
from lucera_common.normalize import normalize_postal_code, normalize_street_name
from lucera_common.normalize import normalize_name, normalize_phone, normalize_email, normalize_vat
from lucera_common.utils import get_municipi_by_ine_code, get_country_id_by_code, get_street_type_id
from lucera_common.validate import is_valid_email
from osv import osv
from osv.orm import ValidateException
from tools.translate import _


def map_api_location_to_erp_address(pool, cursor, uid, location):
    ine_code = location.get('town_code', False)
    id_municipi = False
    state_id = False
    if ine_code:
        municipi = get_municipi_by_ine_code(pool, cursor, uid, ine_code)
        if not municipi:
            raise ValidateException('ValidateError',
                                    'No existe el municipo con código ine {} en el ERP!'.format(ine_code))
        id_municipi = municipi.id
        state_id = municipi.state.id

    postal_code = normalize_postal_code(location.get('postal_code', False))
    country_id = get_country_id_by_code(pool, cursor, uid, 'ES')

    address_location = {
        'zip': postal_code,
        'id_municipi': id_municipi,
        'state_id': state_id,
        'country_id': country_id
    }

    address = location.get('address', False)
    if address:
        if isinstance(address, basestring):
            address_location['street'] = normalize_street_name(address)
        else:
            address_location['nv'] = normalize_street_name(address.get('street_name', False)) or False
            address_location['pnp'] = address.get('number', False) or False
            address_location['es'] = address.get('stair', False) or False
            address_location['pt'] = address.get('floor', False) or False
            address_location['pu'] = address.get('door', False) or False
            address_location['aclarador'] = address.get('clarifying', False) or False
            address_location['tv'] = get_street_type_id(pool, cursor, uid, address.get('street_type_code', False))

    return address_location


def map_api_location_from_cups(cursor, uid, cups):
    location = {
        'postal_code': cups.dp,
        'street_name': cups.nv,
        'number': cups.pnp,
        'stair': cups.es,
        'floor': cups.pt,
        'door': cups.pu,
        'clarifying': cups.aclarador,
        'town_code': cups.id_municipi.ine
    }
    return location


def map_api_location_from_address(cursor, uid, address):
    location = {
        'postal_code': address.zip,
        'street_name': address.nv,
        'number': address.pnp,
        'stair': address.es,
        'floor': address.pt,
        'door': address.pu,
        'clarifying': address.aclarador,
        'town_code': address.id_municipi.ine
    }
    return location


def get_partner_and_address(pool, cursor, uid, partner_id, is_end_user=None):
    # Check that user exists and is end user
    search_params = [('id', '=', partner_id)]
    if is_end_user is not None:
        search_params.append(('is_end_user', '=', is_end_user))
    partner_obj = pool.get('res.partner')
    partner_ids = partner_obj.search(cursor, uid, search_params)
    if not partner_ids:
        raise osv.except_osv(_('ValidateError'), _('Partner with id {} does not exists'.format(partner_id)))
    partner_id = partner_ids[0]
    partner = partner_obj.browse(cursor, uid, partner_id)
    return partner.id, partner.address[0].id


def create_partner_address(pool, cursor, uid, partner_id, contact_data, location, is_end_user):
    """Creates partner address"""
    partner = pool.get('res.partner').browse(cursor, uid, partner_id)
    if partner.address:
        raise osv.except_osv(_('ValidateError'), _('Partner already has an existing address!'))

    vals = {
        'active': True,
        'partner_id': partner_id,
        'name': normalize_name(contact_data['name'], contact_data.get('surnames', False)),
        'phone': normalize_phone(contact_data.get('phone', False)),
        'email': normalize_email(contact_data.get('email', False))
    }

    # If is end user or we are passing an email
    if (is_end_user or vals['email']) and not is_valid_email(vals['email']):
        raise osv.except_osv(_('ValidateError'), _('End users must have a valid email!'))

    if location:
        # Check that existing address is the one from the user
        if location.get('rel', False) == 'existing':
            existing_partner_address_id = location.get('id', False)
            if not existing_partner_address_id:
                raise osv.except_osv(_('ValidateError'), _('Partner rel existing without id!'))
            copied = pool.get('res.partner.address').copy(cursor, uid, existing_partner_address_id, vals)
            return copied

        try:
            address_location = map_api_location_to_erp_address(pool, cursor, uid, location)
        except ValidateException as e:
            raise osv.except_osv(e.name, e.value)

        vals.update(address_location)

    partner_address_obj = pool.get('res.partner.address')

    return partner_address_obj.create(cursor, uid, vals)


def create_partner(pool, cursor, uid, partner_data, is_end_user=False, analytics_user_id=False):
    """Creates a new partner"""
    partner_obj = pool.get('res.partner')
    # Normalize values
    vat = normalize_vat(partner_data['identity_document']['code'])
    name = normalize_name(partner_data['name'], partner_data.get('surnames', None))
    # TODO: Get default payment_type_customer, and property_payment_term
    payment_type_customer = 1
    property_payment_term = 3

    vals = {
        'active': True,
        'name': name,
        # 'lang': 'es_ES',
        'customer': True,
        'supplier': False,
        'is_end_user': is_end_user,
        'vat': vat,
        'vat_subjected': True,
        'payment_type_customer': payment_type_customer,
        'include_in_mod347': True,
        'property_payment_term': property_payment_term,
        'analytics_user_id': analytics_user_id
    }

    return partner_obj.create(cursor, uid, vals)


def create_partner_and_address(pool, cursor, uid, partner_data, location, is_end_user=False,
                               analytics_user_id=False):
    partner_id = create_partner(pool, cursor, uid, partner_data, is_end_user, analytics_user_id)
    address_id = create_partner_address(pool, cursor, uid, partner_id,
                                        partner_data, location, is_end_user)
    return partner_id, address_id
