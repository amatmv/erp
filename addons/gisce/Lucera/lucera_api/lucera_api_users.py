# -*- coding: utf-8 -*-
from lucera_common.api import *
from lucera_common.validate import *


class LuceraApiUsers(osv.osv_memory):
    """
    End users endpoing
    """
    _name = 'lucera.api.users'

    @lucera_api_method
    def change_email(self, cursor, uid, user_id, new_email):

        partner_obj = self.pool.get('res.partner')
        result = partner_obj.change_end_user_email(cursor, uid, user_id, new_email)

        return {
            'success': result
        }

    @lucera_api_method
    def is_valid_change_email(self, cursor, uid, user_id, new_email):

        partner_obj = self.pool.get('res.partner')
        is_valid_email_check = partner_obj.is_valid_new_end_user_email(cursor, uid, user_id, new_email)

        result = {
            'is_valid': is_valid_email_check[0]
        }

        if not is_valid_email_check[0]:
            result['not_valid_reason'] = is_valid_email_check[1]

        return result


LuceraApiUsers()
