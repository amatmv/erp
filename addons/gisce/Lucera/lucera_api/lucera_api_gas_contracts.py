# -*- coding: utf-8 -*-
from lucera_api.lucera_api_contracts import LuceraApiBaseContracts
from osv import osv


class LuceraApiGasContracts(osv.osv_memory, LuceraApiBaseContracts):
    _name = 'lucera.api.gas.contracts'

    _namespace = 'giscegas'
    _polissa_obj_name = '{}.polissa'.format(_namespace)
    _factura_obj_name = '{}.facturacio.factura'.format(_namespace)


LuceraApiGasContracts()
