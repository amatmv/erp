# -*- coding: utf-8 -*-
import netsvc
from lucera_common.api import *
from osv import osv
from lucera_common import pricelist_selector
from tools.misc import cache

import datetime

PRICE_CACHE_TIMEOUT_IN_SECONDS = 1800


class LuceraApiPrices(osv.osv_memory):
    _name = 'lucera.api.prices'

    def _to_api_product(self, cursor, uid, price_list, tarifa, tipus, product, date):
        price_list_obj = self.pool.get('product.pricelist')

        result = {}
        name = product.product_tmpl_id.name
        price_result = price_list_obj.price_get(cursor, uid, [price_list.id], product.id, 1.0, context={'date': date})
        price = round(price_result[price_list.id], 6)
        result[name] = price
        return result

    @cache(timeout=PRICE_CACHE_TIMEOUT_IN_SECONDS)
    def _to_api_tariff_prices(self, cursor, uid, price_list, tarifa, tipus, date):
        result = {}

        for periode in tarifa.periodes:
            if periode.tipus == tipus:
                result.update(self._to_api_product(cursor, uid, price_list, tarifa, tipus,
                                                   periode.product_id, date))
            elif tipus == 'tr' and periode.product_reactiva_id:
                result.update(self._to_api_product(cursor, uid, price_list, tarifa, tipus,
                                                   periode.product_reactiva_id, date))
            elif tipus == 'ep' and periode.product_exces_pot_id:
                result.update(self._to_api_product(cursor, uid, price_list, tarifa, tipus,
                                                   periode.product_exces_pot_id, date))

        return result

    @cache(timeout=PRICE_CACHE_TIMEOUT_IN_SECONDS)
    def _to_api_tariff_atr(self, cursor, uid, price_list, tarifa, date, product_type):
        """
        Converts a tariff to an SignUp API Tariff
        :return:
        {
        }
        """
        result = {
            'id': tarifa.id,
            'cnmc_code': tarifa.codi_ocsum,
            'name': tarifa.name,
            'description': tarifa.descripcio,
            'prices': {
                'energy': self._to_api_tariff_prices(cursor, uid, price_list, tarifa, 'te', date),
            }
        }

        if product_type == 'gas':
            result['prices']['fixed'] = self._to_api_tariff_prices(cursor, uid, price_list, tarifa, 'tp', date)
            result['consumption_range'] = {
                'min': tarifa.consumo_min,
                'max': tarifa.consumo_max
            }
            result['pressure_range'] = {
                'min': tarifa.presion_min,
                'max': tarifa.presion_max
            }
        else:
            result['power_range'] = {
                'min': tarifa.pot_min,
                'max': tarifa.pot_max
            }
            result['prices']['power'] = self._to_api_tariff_prices(cursor, uid, price_list, tarifa, 'tp', date)
            reactive_energy_prices = self._to_api_tariff_prices(cursor, uid, price_list, tarifa, 'tr', date)
            if reactive_energy_prices:
                result['prices']['reactive_energy'] = reactive_energy_prices
            excess_power_prices = self._to_api_tariff_prices(cursor, uid, price_list, tarifa, 'ep', date)
            if excess_power_prices:
                result['prices']['excess_power'] = excess_power_prices

        return result

    @lucera_api_method()
    def all_atr_prices(self, cursor, uid, kind, date, product_type):
        """
        Get all ATR Product prices
        :param cursor:
        :param uid:
        :param kind: Price list kind
        :param date: Date. If not set is current date
        :param product_type: electricity | gas
        :return:
        """
        # Get current price list version
        price_list_id, price_list_version_id = \
            pricelist_selector.get_current_price_list_by_kind(self.pool, cursor, uid, product_type, kind, date)

        if price_list_id and price_list_version_id:
            return self._to_api_price_list_prices(cursor, uid, price_list_version_id, date, product_type)

        return False

    @lucera_api_method()
    def by_pricelist_and_date(self, cursor, uid, pricelist_id, date, product_type):
        """
        Get all ATR Product prices
        :param product_type:
        :param cursor:
        :param uid:
        :param pricelist_id: Price list id
        :type pricelist_id:int
        :param date: Date. If not set is current date
        :type date:datetime
        :return:
        """
        # Get current price list version
        price_list_id, price_list_version_id = pricelist_selector\
            .get_current_price_list_by_id(self.pool, cursor, uid, pricelist_id, date)

        if not pricelist_id or not price_list_version_id:
            return False

        return self._to_api_price_list_prices(cursor, uid, price_list_version_id, date, product_type)

    @cache(timeout=PRICE_CACHE_TIMEOUT_IN_SECONDS)
    def _to_api_price_list_prices(self, cursor, uid, price_list_version_id, date, product_type):
        price_list_version_obj = self.pool.get('product.pricelist.version')
        price_list_version = price_list_version_obj.browse(cursor, uid, price_list_version_id)
        if not price_list_version:
            raise osv.except_osv('Error!', 'Could not find active price list')

        price_list = price_list_version.pricelist_id

        result = {
            'id': price_list.id,
            'name': price_list.name,
            'kind': price_list.kind,
            'version': {
                'id': price_list_version.id,
                'name': price_list_version.name,
                'date_start': price_list_version.date_start,
                'date_end': price_list_version.date_end
            },
            'access_tariffs': []
        }

        if product_type == 'electricity':
            for tarifa_atr in price_list.tarifes_atr_compatibles:
                result['access_tariffs'].append(self._to_api_tariff_atr(cursor, uid, price_list,
                                                                        tarifa_atr, date, product_type))
        else:
            for tarifa_atr in price_list.tarifes_atr_gas_compatibles:
                result['access_tariffs'].append(self._to_api_tariff_atr(cursor, uid, price_list,
                                                                        tarifa_atr, date, product_type))

        return result

    @lucera_api_method()
    def atr_prices_by_date(self, cursor, uid, price_list_kind, start_date, end_date, product_type):
        # Get current price list version
        price_list_id = pricelist_selector.get_price_list_by_kind(self.pool, cursor, uid,
                                                                  product_type, price_list_kind)

        if not price_list_id:
            return False

        price_list_version_obj = self.pool.get('product.pricelist.version')
        price_list_version_ids = price_list_version_obj.search(
            cursor, uid, [('pricelist_id', '=', price_list_id),
                          # ('date_start', '>=', start_date), ('date_start', '<', end_date),
                          ('date_end', '>=', start_date), ('date_start', '<', end_date),
                          ('active', '=', True)],
            order='date_start')

        price_list_versions = price_list_version_obj.browse(cursor, uid, price_list_version_ids)

        results = []

        for price_list_version in price_list_versions:
            results.append(self.by_pricelist_and_date(cursor, uid, price_list_version.pricelist_id.id,
                                                      price_list_version.date_start, product_type))

        return results


LuceraApiPrices()


def log(msg, level=netsvc.LOG_INFO):
    logger = netsvc.Logger()
    logger.notifyChannel('oorq', level, msg)
