# -*- coding: utf-8 -*-
def get_country_id_by_code(pool, cursor, uid, code):
    country_obj = pool.get('res.country')
    return country_obj.search(cursor, uid, [('code', '=', code)])[0]


def get_municipi_by_ine_code(pool, cursor, uid, ine_code):
    if not ine_code:
        return False
    municipi_obj = pool.get('res.municipi')
    municipi_ids = municipi_obj.search(cursor, uid, [('ine', '=', ine_code)])
    if municipi_ids:
        return municipi_obj.browse(cursor, uid, municipi_ids[0])
    return None


def get_street_type_id(pool, cursor, uid, street_type_code):
    if street_type_code:
        tpv_obj = pool.get('res.tipovia')
        search = [('codi', '=', street_type_code)]
        tpv_id = tpv_obj.search(cursor, uid, search)
        if tpv_id:
            return tpv_id[0]
        else:
            return False
            # raise osv.except_osv(_('ValidateError'), 'Could not find tipovia.code {}'.format(street_type_code))
    return False


def get_cnae_id_by_code(pool, cursor, uid, cnae_code):
    """Gets the cnae code from the fiscal_customer type and the primary residence"""
    cnae_obj = pool.get('giscemisc.cnae')
    cnae_id = cnae_obj.search(cursor, uid, [('name', '=', cnae_code)])
    if not cnae_id:
        raise Exception('Invalid cnae code')
    return cnae_id[0]