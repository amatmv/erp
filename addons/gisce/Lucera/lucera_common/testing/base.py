# -*- coding: utf-8 -*-
import random
import uuid
from datetime import datetime
#  from lucera_common.testing import LUCERA_DEFAULT_PAYMENT_MODE_NAME, LUCERA_PAYMENT_TYPE_CODE
from lucera_common.config import LUCERA_DEFAULT_PAYMENT_MODE_NAME, LUCERA_PAYMENT_TYPE_CODE
from lucera_common.testing.common import create_partner_and_address, TESTING_DEFAULT_DATE
from lucera_common.fake import get_random_cups_code, get_random_bank_account


class BaseTestPolissaFactory(object):
    _namespace = None

    def __init__(self):
        # self.user_id = user_id
        # self.atr_holder_id = atr_holder_id
        # self.fiscal_customer_id = fiscal_customer_id
        # self.bank_account_owner_id = bank_account_owner_id
        # self.cups_code = cups_code
        # self.is_signed = is_signed
        # self.account_number = account_number
        # self.power = power
        # self.has_mandate = has_mandate
        # self.has_cups = has_cups
        # self.uid = uid
        # self.pool = pool
        # self.cursor = cursor
        # _polissa_obj_name =
        # _tarifa_obj_name =
        # _cups_obj_name = '{}.cups.ps'.format(self._namespace)
        pass

    @property
    def _polissa_obj_name(self):
        return '{}.polissa'.format(self._namespace)

    @property
    def _tarifa_obj_name(self):
        return '{}.polissa.tarifa'.format(self._namespace)

    @property
    def _cups_obj_name(self):
        return '{}.cups.ps'.format(self._namespace)

    def create_test_polissa(self, pool, cursor, uid, **kwargs):
        """
           Creates a polissa with the argument supplied.

           :param pool:
           :param cursor:
           :param uid:
           :param has_cups:
           :param has_mandate:
           :param tariff_code:
           :param power:
           :param account_number:
           :param user_id:
           :param atr_holder_id:
           :param fiscal_customer_id:
           :param bank_account_owner_id:
           :param cups_code:
           :param is_signed:
           :param override_vals:
           :param partner_data:
           :param address_data:
           :param is_email_validated:
           :param counter_name:
           :return:
               'user_id': user_id,
               'user_address_id': user_address_id,
               'fiscal_customer_id': fiscal_customer_id,
               'fiscal_customer_address_id': fiscal_customer_address_id,
               'atr_holder_id': atr_holder_id,
               'bank_account_id': bank_account_id,
               'bank_owner_id': bank_owner_id,
               'polissa_id': polissa_id,
               'cups_id': cups_id
           """
        user_id = kwargs.get('user_id', False)
        partner_data = kwargs.get('user_id', None)
        address_data = kwargs.get('address_data', None)
        is_email_validated = kwargs.get('is_email_validated', False)
        tariff_code = self._get_tariff_code(**kwargs)
        fiscal_customer_id = kwargs.get('fiscal_customer_id', None)
        atr_holder_id = kwargs.get('atr_holder_id', None)
        counter_name = kwargs.get('counter_name', None)
        override_vals = kwargs.get('override_vals', {})
        has_mandate = kwargs.get('has_mandate', True)
        is_signed = kwargs.get('is_signed', True)
        is_email_validated = kwargs.get('is_email_validated', True)
        has_cups = kwargs.get('has_cups', True)
        cups_code = kwargs.get('cups_code', False)
        bank_account_owner_id = kwargs.get('bank_account_owner_id', None)
        account_number = kwargs.get('account_number', get_random_bank_account())

        if user_id:
            user = pool.get('res.partner').browse(cursor, uid, user_id)
            user_address_id = user.address[0].id
        else:
            user_id, user_address_id = \
                create_partner_and_address(pool, cursor, uid,
                                           partner_data, address_data, is_email_validated=is_email_validated)

        cnae_id = 986  # Particular
        country_obj = pool.get('res.country')
        country_id = country_obj.search(cursor, uid, [('code', '=', 'ES')])[0]
        payment_type_customer_id = 1,
        payment_term_id = 3
        conf_obj = pool.get('res.config')
        if conf_obj:
            payment_mode_name = conf_obj.get(cursor, uid, 'lucera_signups_payment_mode_name')
        else:
            payment_mode_name = LUCERA_DEFAULT_PAYMENT_MODE_NAME

        payment_mode_obj = pool.get('payment.mode')
        payment_mode_ids = payment_mode_obj.search(
            cursor, uid, [('name', '=', payment_mode_name)])

        if payment_mode_ids:
            payment_mode_id = payment_mode_ids[0]
        else:  # If lucera payment mode is not yet configured, select first payment mode
            payment_mode_id = pool.get('payment.mode').search(cursor, uid, [], limit=1)[0]

        payment_type_id = pool.get('payment.type').search(
            cursor, uid, [('code', '=', LUCERA_PAYMENT_TYPE_CODE)])[0]

        tarifa_obj = pool.get(self._tarifa_obj_name)
        if tariff_code:
            tariff_id = tarifa_obj.search(cursor, uid, [('codi_ocsum', '=', tariff_code)])[0]
            tariff = tarifa_obj.browse(cursor, uid, tariff_id)

            # Price list
            price_list_id = None
            for price_list in tariff.llistes_preus_comptatibles:
                if price_list.type == 'sale':
                    price_list_id = price_list.id
                    break
        else:
            tariff_id = False
            price_list_id = False

        # Payer
        pagador_sel = 'titular'
        if fiscal_customer_id and fiscal_customer_id != user_id:
            pagador_sel = 'altre_p'
            fiscal_customer_address_id = pool.get('res.partner').browse(cursor, uid, fiscal_customer_id).address[0].id
        else:
            fiscal_customer_id = user_id
            fiscal_customer_address_id = user_address_id

        # Holder
        if not atr_holder_id:
            atr_holder_id = user_id

        if atr_holder_id == user_id:
            notificacio = 'titular'
        elif atr_holder_id == fiscal_customer_id:
            notificacio = 'pagador'
        else:
            notificacio = 'altre_p'

        if not counter_name:
            counter_name = '{}'.format(random.randrange(1, 9999999999))

        # Default values
        vals = {
            'cnae': cnae_id,
            'comercialitzadora': 1,
            'notificacio': notificacio,
            'direccio_notificacio': user_address_id,
            'tarifa': tariff_id,
            'llista_preu': price_list_id,
            'titular': atr_holder_id,
            'state': 'esborrany',
            'tipus_vivenda': 'habitual',
            'pagador_sel': pagador_sel,
            'facturacio': 1,
            'payment_mode_id': payment_mode_id,
            'enviament': 'email',
            'pagador': fiscal_customer_id,
            'direccio_pagament': fiscal_customer_address_id,
            'tipo_pago': payment_type_id,
            'altre_p': False
        }

        vals.update(self._get_custom_default_values())

        if override_vals:
            vals.update(override_vals)

        cups_id = False
        if has_cups:
            cups_id = self._create_cups(pool, cursor, uid, **kwargs)
            cups_entity = pool.get(self._cups_obj_name).browse(cursor, uid, cups_id)
            vals['cups'] = cups_id
            vals['distribuidora'] = cups_entity.distribuidora_id.id

        if is_signed:
            vals['data_firma_contracte'] = TESTING_DEFAULT_DATE

        bank_account_id = False
        if has_mandate:
            imd_obj = pool.get('ir.model.data')
            bank = pool.get('res.bank').search(cursor, uid, [], limit=1)[0]
            bank_vals = {
                'name': '',
                'state': 'bank',
                'country_id': country_id,
                'partner_id': fiscal_customer_id,
                'owner_id': bank_account_owner_id if bank_account_owner_id else fiscal_customer_id,
                'bank': bank,
                'acc_number': account_number,
            }
            partner_bank_obj = pool.get('res.partner.bank')
            bank_account_id = partner_bank_obj.create(cursor, uid, bank_vals)
            vals['bank'] = bank_account_id
        else:
            bank_vals = {}

        # If it is activated we have to add required fields
        if vals['state'] in ['activa', 'baixa']:
            if 'data_alta' in vals and vals['data_alta']:
                data_alta = vals['data_alta']
            else:
                data_alta = TESTING_DEFAULT_DATE.strftime('%Y-%m-%d')
            if 'data_alta' not in vals:
                vals['data_alta'] = data_alta

            if 'comptadors' not in vals:
                vals['comptadors'] = [(0, 0, {'name': counter_name,
                                              'lloguer': True,
                                              'active': True,
                                              'data_alta': data_alta})]

        # Before create hook

        self._before_create(pool, cursor, uid, vals, **kwargs)

        polissa_obj = pool.get(self._polissa_obj_name)
        polissa_id = polissa_obj.create(cursor, uid, vals)

        self._after_create(pool, cursor, uid, vals, **kwargs)

        if has_mandate:
            name = str(uuid.uuid1()).replace('-', '')
            reference = '{},{}'.format(self._polissa_obj_name, polissa_id)
            mandate_vals = {
                'name': name,
                'reference': reference,
                'creditor_id': 1,
                'date': TESTING_DEFAULT_DATE,
                'creditor_code': '2020202'
            }
            if override_vals and 'data_alta' in override_vals:
                bank_vals.update({'date': override_vals['data_alta']})
            mandate_obj = pool.get('payment.mandate')
            #  TODO: It does not work and I don't know why
            mandate_id = mandate_obj.create(cursor, uid, mandate_vals)

        # Change polissa state if it was passed in vals
        if vals['state'] == 'validar':
            polissa_obj.wkf_validar(cursor, uid, [polissa_id])
        if vals['state'] in ['activa', 'baixa']:
            polissa_obj.send_signal(
                cursor, uid, [polissa_id],
                ['validar', 'contracte']
            )
            #  polissa_obj.wkf_activa(cursor, uid, [polissa_id])
        if vals['state'] == 'baixa':
            polissa_obj.wkf_baixa(cursor, uid, [polissa_id])

        if vals['state'] == 'cancelada':
            polissa_obj.write(cursor, uid, [polissa_id],
                              {'state': 'cancelada', 'active': False,
                               'data_baixa': datetime.today().strftime("%Y-%m-%d")},
                              {'sync': False})
        result = {
            'user_id': user_id,
            'user_address_id': user_address_id,
            'fiscal_customer_id': fiscal_customer_id,
            'fiscal_customer_address_id': fiscal_customer_address_id,
            'atr_holder_id': atr_holder_id,
            'bank_account_id': bank_account_id,
            'bank_account_owner_id': bank_account_owner_id,
            self._output_polissa_key: polissa_id,
            self._output_cups_key: cups_id
        }

        return result

    def _get_custom_default_values(self, *args, **kwargs):
        return {}

    def _get_tariff_code(self, **kwargs):
        return False

    def _before_create(self, pool, cursor, uid, vals, **kwargs):
        pass

    def _after_create(self, pool, cursor, uid, vals, **kwargs):
        pass

    def _create_cups(self, pool, cursor, uid, **kwargs):
        cups_code = kwargs.get('cups_code', False)

        """Creates a new cups"""
        cups_obj = pool.get(self._cups_obj_name)
        if not cups_code:
            cups_code = get_random_cups_code('0021')
        else:
            cups_ids = cups_obj.search(cursor, uid, [('name', '=', cups_code)])
            if cups_ids:
                return cups_ids[0]

        imd_obj = pool.get('ir.model.data')
        distri_id = False
        if cups_code:
            partner_ref = cups_code[2:6]
            partner_obj = pool.get('res.partner')
            distri_ids = partner_obj.search(
                cursor, uid, [('ref', '=', partner_ref)]
            )
            if distri_ids:
                distri_id = distri_ids[0]
        if not distri_id:
            distri_id = imd_obj.get_object_reference(cursor, uid, 'base', 'main_partner')[1]
        id_municipi = imd_obj.get_object_reference(cursor, uid, 'base_extended', 'ine_01001')[1]
        id_poblacio = imd_obj.get_object_reference(cursor, uid, 'base_extended', 'poble_01')[1]
        vals = {
            'name': cups_code,
            'distribuidora_id': distri_id,
            'id_poblacio': id_poblacio,
            'id_municipi': id_municipi
        }
        return cups_obj.create(cursor, uid, vals)
