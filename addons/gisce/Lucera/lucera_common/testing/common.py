# -*- coding: utf-8 -*-
import random
from datetime import datetime

from lucera_common.fake import get_random_nif, get_random_phone, get_random_iban


def create_partner_and_address(pool, cursor, uid, partner_data=None, address_data=None,
                               is_end_user=True, is_email_validated=True):
    """Creates a new partner with her address"""
    if not partner_data:
        name = 'User {:d}'.format(random.randrange(1, 99999999))
        vat_code = get_random_nif()
        partner_data = {'name': name, 'is_end_user': is_end_user, 'vat': vat_code}
    if 'vat' in partner_data and partner_data['vat'][:2] != 'ES':
        partner_data['vat'] = 'ES' + partner_data['vat']
    if 'is_end_user' not in partner_data:
        partner_data['is_end_user'] = is_end_user
    partner_id = pool.get('res.partner').create(cursor, uid, partner_data)
    if not address_data:
        email = '{}@email.com'.format(partner_data['name'].replace(',', ' ').replace(' ', '_').lower())
        phone = get_random_phone()
        address_data = {'email': email, 'phone': phone}
    if is_email_validated:
        address_data['is_email_validated'] = True
    address_data['partner_id'] = partner_id
    partner_address_id = pool.get('res.partner.address').create(cursor, uid, address_data)
    return partner_id, partner_address_id


def create_bank(pool, cursor, uid, name='Test Bank', code=3008, other_vals=None):
    res_bank_obj = pool.get('res.bank')
    bank_ids = res_bank_obj.search(cursor, uid, [('code', '=', code)])
    if not bank_ids:
        vals = {
            'name': name,
            'code': code
        }
        if other_vals:
            vals.update(other_vals)
        return res_bank_obj.create(cursor, uid, vals)
    return bank_ids[0]


def create_bank_account(pool, cursor, uid, partner_id, iban_code=None, owner_id=None):
    """Creates a bank account for a user"""
    country_obj = pool.get('res.country')
    country_id = country_obj.search(cursor, uid, [('code', '=', 'ES')])[0]
    bank = pool.get('res.bank').search(cursor, uid, [], limit=1)[0]
    bank_vals = {
        'name': '',
        'state': 'iban',
        'country_id': country_id,
        'partner_id': partner_id,
        'owner_id': owner_id if owner_id else partner_id,
        'bank': bank,
        'iban': iban_code or get_random_iban(),
    }
    partner_bank_obj = pool.get('res.partner.bank')
    bank_account_id = partner_bank_obj.create(cursor, uid, bank_vals)
    return bank_account_id


TESTING_DEFAULT_DATE = datetime(2018, 06, 01)  # type: datetime