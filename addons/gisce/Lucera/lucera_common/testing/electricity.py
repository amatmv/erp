# -*- coding: utf-8 -*-
import random
import uuid
from datetime import datetime

from lucera_common.config import LUCERA_DEFAULT_PAYMENT_MODE_NAME, LUCERA_PAYMENT_TYPE_CODE
from lucera_common.fake import get_random_cups_code, get_random_bank_account
from lucera_common.testing.base import BaseTestPolissaFactory
from lucera_common.testing.common import create_partner_and_address


class ElectricityTestPolissaFactory(BaseTestPolissaFactory):

    _namespace = 'giscedata'
    _output_polissa_key = 'polissa_id'
    _output_cups_key = 'cups_id'

    def _get_custom_default_values(self, **kwargs):
        return {
            'potencia': self._get_power(**kwargs),
            'facturacio_distri': 1,  # Mensual
        }

    def _get_tariff_code(self, **kwargs):
        return kwargs.get('tariff_code', '001')

    def _get_power(self, **kwargs):
        power = kwargs.get('power', 3.450)
        if power > 99.0:
            power /= 1000.0
        return power

    def _before_create(self, pool, cursor, uid, vals, **kwargs):
        if vals['state'] in ['activa', 'baixa']:
            if 'tensio_normalitzada' not in vals:
                tensio_id = pool.get('giscedata.tensions.tensio').search(cursor, uid, [], limit=1)[0]
                vals['tensio_normalitzada'] = tensio_id
        elif vals['state'] == 'cancelada':
            motiu_canvi_obj = pool.get('giscedata.motiu.canvi')
            if motiu_canvi_obj:  # May be it is not installed
                motiu_canvi_id = motiu_canvi_obj.search(cursor, uid, [('type', '=', 'cancelacio')], limit=1)[0]
                vals['motius_canvis_ids'] = [(0, 0, {'motiu_id': motiu_canvi_id, 'tipus_motiu': 'cancelacio'})]


def create_electricity_cups(pool, cursor, uid, cups_code=None):
    """Creates a new cups"""
    cups_obj = pool.get('giscedata.cups.ps')
    if not cups_code:
        cups_code = get_random_cups_code('0021')
    else:
        cups_ids = cups_obj.search(cursor, uid, [('name', '=', cups_code)])
        if cups_ids:
            return cups_ids[0]

    imd_obj = pool.get('ir.model.data')
    if cups_code:
        partner_ref = cups_code[2:6]
        partner_obj = pool.get('res.partner')
        distri_ids = partner_obj.search(
            cursor, uid, [('ref', '=', partner_ref)]
        )
        if distri_ids:
            distri_id = distri_ids[0]
    if not distri_id:
        distri_id = imd_obj.get_object_reference(cursor, uid, 'base', 'main_partner')[1]
    id_municipi = imd_obj.get_object_reference(cursor, uid, 'base_extended', 'ine_01001')[1]
    id_poblacio = imd_obj.get_object_reference(cursor, uid, 'base_extended', 'poble_01')[1]
    vals = {
        'name': cups_code,
        'distribuidora_id': distri_id,
        'id_poblacio': id_poblacio,
        'id_municipi': id_municipi
    }
    return cups_obj.create(cursor, uid, vals)


def create_test_electricity_polissa(pool, cursor, uid, **kwargs):
    """
    Creates a polissa with the argument supplied.

    :param pool:
    :param cursor:
    :param uid:
    :param has_cups:
    :param has_mandate:
    :param tariff_code:
    :param power:
    :param account_number:
    :param user_id:
    :param atr_holder_id:
    :param fiscal_customer_id:
    :param bank_account_owner_id:
    :param cups_code:
    :param is_signed:
    :param override_vals:
    :param partner_data:
    :param address_data:
    :param is_email_validated:
    :param counter_name:
    :return:
        'user_id': user_id,
        'user_address_id': user_address_id,
        'fiscal_customer_id': fiscal_customer_id,
        'fiscal_customer_address_id': fiscal_customer_address_id,
        'atr_holder_id': atr_holder_id,
        'bank_account_id': bank_account_id,
        'bank_owner_id': bank_owner_id,
        'polissa_id': polissa_id,
        'cups_id': cups_id
    """

    factory = ElectricityTestPolissaFactory()
    return factory.create_test_polissa(pool, cursor, uid, **kwargs)


