# -*- coding: utf-8 -*-
from lucera_common.testing.electricity import *
from lucera_common.testing.gas import *


def create_test_electricity_and_gas_polissa(pool, cursor, uid, is_signed=True, user_id=None, override_vals=None):
    electricity_result = create_test_electricity_polissa(pool, cursor, uid, is_signed=is_signed,
                                                         user_id=user_id, override_vals=override_vals)
    gas_result = create_test_gas_polissa(pool, cursor, uid,
                                         user_id=electricity_result['user_id'],
                                         fiscal_customer_id=electricity_result['fiscal_customer_id'],
                                         atr_holder_id=electricity_result['atr_holder_id'],
                                         is_signed=is_signed,
                                         override_vals=override_vals)

    result = {}
    result.update(electricity_result)
    result.update(gas_result)
    return result
