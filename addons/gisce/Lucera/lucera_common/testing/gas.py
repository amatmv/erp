# -*- coding: utf-8 -*-
import json
import random
import uuid
from datetime import datetime

from lucera_common.config import LUCERA_DEFAULT_PAYMENT_MODE_NAME, LUCERA_PAYMENT_TYPE_CODE
from lucera_common.fake import get_random_cups_code, get_random_bank_account
from lucera_common.testing import create_partner_and_address
from lucera_common.testing.base import BaseTestPolissaFactory


class GasTestPolissaFactory(BaseTestPolissaFactory):

    _namespace = 'giscegas'
    _output_polissa_key = 'gas_polissa_id'
    _output_cups_key = 'gas_cups_id'

    def _get_tariff_code(self, **kwargs):
        return kwargs.get('tariff_code', '31')

    def _get_preasure(self, **kwargs):
        return kwargs.get('preasure', 2.0)

    def _get_daily_flow(self, **kwargs):
        daily_flow = kwargs.get('daily_flow', False)
        if not daily_flow:
            tariff_code = kwargs.get('tariff_code', self._get_tariff_code())
            if tariff_code == '31':
                daily_flow = 5000 / 366.0
            elif tariff_code == '32':
                daily_flow = 50000 / 366.0

    def _get_custom_default_values(self, **kwargs):
        return {
            'pressio': self._get_preasure(**kwargs),
            'caudal_diario': self._get_daily_flow(**kwargs),
            'facturacio_distri': 'M',  # Mensual
        }

    # def _output_polissa_key(self):
    #     return 'gas_polissa_id'
    #
    # def _output_cups_key(self):
    #     return 'gas_cups_id'

    def _before_create(self, pool, cursor, uid, vals, **kwargs):
        lot_facturacio_ids = pool.get('giscegas.facturacio.lot').search(cursor, uid, [], limit=1)

        if lot_facturacio_ids:
            lot_facturacio_id = lot_facturacio_ids[0]
        else:
            lot_facturacio_id = False

        vals['lot_facturacio'] = lot_facturacio_id


def create_gas_cups(pool, cursor, uid, cups_code=None):
    """Creates a new cups"""
    cups_obj = pool.get('giscegas.cups.ps')

    if not cups_code:
        cups_code = get_random_cups_code('0021')
    else:
        cups_ids = cups_obj.search(cursor, uid, [('name', '=', cups_code)])
        if cups_ids:
            return cups_ids[0]

    imd_obj = pool.get('ir.model.data')

    if cups_code:
        partner_ref = cups_code[2:6]
        partner_obj = pool.get('res.partner')
        distri_ids = partner_obj.search(
            cursor, uid, [('ref', '=', partner_ref)]
        )
        if distri_ids:
            distri_id = distri_ids[0]
        else:
            distri_id = None
    else:
        distri_id = None

    if not distri_id:
        distri_id = imd_obj.get_object_reference(cursor, uid, 'base', 'main_partner')[1]
    id_municipi = imd_obj.get_object_reference(cursor, uid, 'base_extended', 'ine_01001')[1]
    id_poblacio = imd_obj.get_object_reference(cursor, uid, 'base_extended', 'poble_01')[1]
    vals = {
        'name': cups_code,
        'distribuidora_id': distri_id,
        'id_poblacio': id_poblacio,
        'id_municipi': id_municipi
    }
    return cups_obj.create(cursor, uid, vals)


def create_test_gas_polissa(pool, cursor, uid, **kwargs):

    """
    Creates a polissa with the argument supplied.

    :param polissa_state:
    :param counter_name:
    :param preasure:
    :param daily_flow:
    :param pool:
    :param cursor:
    :param uid:
    :param has_cups:
    :param has_mandate:
    :param tariff_code:
    :param power:
    :param account_number:
    :param user_id:
    :param atr_holder_id:
    :param fiscal_customer_id:
    :param bank_account_owner_id:
    :param cups_code:
    :param is_signed:
    :param override_vals:
    :param partner_data:
    :param address_data:
    :param is_email_validated:
    :return:
        'user_id': user_id,
        'user_address_id': user_address_id,
        'fiscal_customer_id': fiscal_customer_id,
        'fiscal_customer_address_id': fiscal_customer_address_id,
        'atr_holder_id': atr_holder_id,
        'bank_account_id': bank_account_id,
        'bank_owner_id': bank_owner_id,
        'polissa_id': polissa_id,
        'cups_id': cups_id
    """

    factory = GasTestPolissaFactory()
    return factory.create_test_polissa(pool, cursor, uid, **kwargs)


def create_test_gas_invoice(pool, cursor, uid, polissa_id, date_start, date_end, lectures=None):
    imd_obj = pool.get('ir.model.data')
    polissa_obj = pool.get('giscegas.polissa')
    lectura_obj = pool.get('giscegas.lectures.lectura')
    fact_obj = pool.get('giscegas.facturacio.factura')
    wz_mi_obj = pool.get("giscegas.wizard.manual.invoice")

    polissa = polissa_obj.browse(cursor, uid, polissa_id)
    periode_id = imd_obj.get_object_reference(
        cursor, uid, 'giscegas_polissa', 'p1_v_tarifa_31_gas'
    )[1]

    comptador = polissa.comptadors[0]
    comptador_id = comptador.id

    origen_id = imd_obj.get_object_reference(
        cursor, uid, 'giscegas_lectures', 'origen_gas_0'
    )[1]

    if not lectures:
        lectures = [
            {
                'name': '2018-10-28',
                'periode': periode_id,
                'lectura': 8000,
                'comptador': comptador_id,
                'observacions': '',
                'origen_id': origen_id,
            },
            {
                'name': '2018-11-28',
                'periode': periode_id,
                'lectura': 9000,
                'comptador': comptador_id,
                'observacions': '',
                'origen_id': origen_id,
            }
        ]
    else:
        # Delete all lectures from meter
        for comptador in polissa.comptadors:
            for l in comptador.lectures:
                l.unlink(context={})
            # Delete lloguer because it will fail in manual invoice because
            # it's implemented in comer/dist facturacio modules
            comptador.write({'lloguer': False})

        comptador = polissa.comptadors[0]

        for lectura in lectures:
            lectura_obj.create(cursor, uid, lectura)

    journal_obj = pool.get('account.journal')
    journal_id = journal_obj.search(
        cursor, uid, [('code', '=', 'GAS')]
    )[0]

    wz_fact_id = wz_mi_obj.create(cursor, uid, {})
    wz_fact = wz_mi_obj.browse(cursor, uid, wz_fact_id)
    wz_fact.write({
        'polissa_id': polissa_id,
        'date_start': date_start,
        'date_end': date_end,
        'journal_id': journal_id
    })
    wz_fact.action_manual_invoice()
    wz_fact = wz_mi_obj.browse(cursor, uid, wz_fact_id)
    invoice_id = json.loads(wz_fact.invoice_ids)[0]
    return invoice_id


