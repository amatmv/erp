# -*- coding: utf-8 -*-
import phonenumbers
import vatnumber
import string


def normalize_phone(phone):
    """Normalizes the phone number"""
    if not phone:
        return phone

    if isinstance(phone, list):
        return [normalize_phone(p) for p in phone]

    split_chars = [';', ',', '/']

    phone = phone.strip().replace("O", "0").replace("o", "o")
    phone = ''.join([x for x in phone if x.isdigit() or x == '+'])
    try:
        p = phonenumbers.parse(phone, region='ES')
        if p.country_code == 34:
            return str(p.national_number)
        else:
            return phonenumbers.format_number(p, phonenumbers.PhoneNumberFormat.E164)
    except phonenumbers.NumberParseException:
        return phone


def normalize_email(email):
    """Normalizes the email"""
    if not email:
        return email

    if isinstance(email, list):
        return [normalize_email(e) for e in email]

    email = email.strip().replace(" ", "")
    return email.lower()


def normalize_vat(vat):
    """Normalizes the vat"""
    if vat:
        vat = vat.upper()
        vat = ''.join([x for x in vat if x.isdigit() or x.isalpha()])

        if not vat.startswith('ES') and not vat.startswith('PS') and len(vat) >=8:
            vat = 'ES' + vat

    return vat


def normalize_cups(cups):
    """Normalizes the cups code"""
    if cups:
        new_cups = cups.upper().replace('O', '0')
        new_cups = ''.join([x for x in new_cups if x.isdigit() or x.isalpha()])
        if len(new_cups) >= 20:
            return new_cups
    return cups


def _normalize_proper_noun(value):
    try:
        if not value:
            return value
        value = ' '.join(value.strip().split())
        if value == value.lower() or value == value.upper():
            words = map(lambda w: w.capitalize() if w.isalnum() else w, value.split())
            return ' '.join(words)
        return value
    except Exception, e:
        return value


def normalize_name(name, surnames=None):
    """Normalizes the name and surnames"""
    name = _normalize_proper_noun(name)
    surnames = _normalize_proper_noun(surnames)

    if name and not surnames:
        return name
    elif not name and surnames:
        return surnames
    else:
        return u'{}, {}'.format(surnames, name)


def normalize_street_name(address):
    """Normalizes the address"""
    return _normalize_proper_noun(address)


def normalize_bank_account_number(bank_account):
    """Normalizes a bank account"""
    if bank_account:
        bank_account = bank_account.strip().upper()
        bank_account = ''.join([x for x in bank_account if x.isdigit() or x.isalpha()])
    return bank_account


def get_power_as_kilowatts(power):
    """Converts the power to kW."""
    if power > 200:
        power /= 1000.0
    return power


def get_power_as_watts(power):
    """Checks if it is a power that is already in wats (<200) If it is not it multiplies it by 1000"""
    if power < 200:
        power *= 1000
    return int(power)

def normalize_postal_code(postal_code, length=5):
    """Normalizes postal codes"""
    if not postal_code:
        return postal_code
    return postal_code.strip().zfill(length)
