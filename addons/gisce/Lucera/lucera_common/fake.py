# -*- coding: utf-8 -*-
import random
from stdnum.es.iban import ccc, iban
from stdnum.es import cups, vat

# Do we really need this?
from lucera_common.normalize import normalize_email

random.seed()


def get_random_bank_account(bank_code=3008, office_code=None, account_code=None):
    if not bank_code:
        bank_code = random.randint(1, 9999)
    if not office_code:
        office_code = random.randint(1, 9999)
    if not account_code:
        account_code = random.randint(1, 9999999999)
    number = '{:04d}{:04d}00{:010d}'.format(bank_code, office_code, account_code)
    control = ccc.calc_check_digits(number)
    return '{:04d}{:04d}{}{:010d}'.format(bank_code, office_code, control, account_code)


def get_random_iban(country_code='ES', bank_code=3008, office_code=0, account_code=None, account_number=None):
    if not account_number:
        account_number = get_random_bank_account(bank_code, office_code, account_code)
    else:
        account_number = ccc.compact(account_number)

    # Copy pasted from res_partner_bank to use the same method because iban.check_digit sometimes differ
    raw_iban = '%s%s00' % (account_number, country_code)
    raw_num_iban = ''
    for char in raw_iban:
        if char.isalpha():
            raw_num_iban += str(ord(char) - 55)
        else:
            raw_num_iban += char
    iban_check_code = 98 - (int(raw_num_iban) % 97)
    iban_control = str(iban_check_code).zfill(2)
    iban_number = '{}{}{}'.format(country_code, iban_control, account_number)

    return iban_number


def get_random_cups_code(distribution_company_code=21, country_code='ES'):
    if not distribution_company_code:
        distribution_company_code = random.randint(1, 99)
    internal_code = random.randint(1, 999999999999)
    distribution_company_code = int(distribution_company_code)
    number = '{}{:04d}{:012d}00'.format(country_code, distribution_company_code, internal_code)
    cups_control = cups.calc_check_digits(number)
    cups_code = '{}{:04d}{:012d}{}'.format(country_code, distribution_company_code, internal_code, cups_control)
    return cups_code


def get_random_nif(code=None):
    if not code:
        code = random.randint(1, 99999999)
    number = '{:08d}'.format(code)
    control_digit = vat.dni.calc_check_digit(number)
    return '{}{}'.format(number, control_digit)


def get_random_phone(mobile=True):
    prefix = random.randint(60, 74) if mobile else random.randint(91, 98)
    return '{}{:07d}'.format(prefix, random.randint(1, 9999999))


def get_random_email(user=None, domain=None):
    user = '{}{}'.format(user or '', str(random.randint(1, 99999999)))
    if not domain:
        domain = '{}.{}'.format(str(random.randint(1, 99999999)), random.choice(['com', 'net', 'es']))
    email = '{}@{}'.format(user, domain)
    return normalize_email(email)
