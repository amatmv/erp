# -*- coding: utf-8 -*-
import unittest
from lucera_common.normalize import *
from expects import *


class NormalizationTest(unittest.TestCase):

    def test_normalize_phones(self):
        values = {
            '961580184': '961580184',
            '96 158 01 84': '961580184',
            '96-158-01-84': '961580184',
            '+34 96 158 01 84': '961580184',
            '0034961580184': '961580184',
            '666581633': '666581633',
            '666 58 16 33': '666581633',
            '666-58-16-33': '666581633',
            '+34 666 58 16 33': '666581633',
            '0034666581633': '666581633',
            '+33142764040': '+33142764040',
            '+33 1 42 76 40 40': '+33142764040',
            '0033142764040': '+33142764040',
            '00 33 1 42 76 40 40': '+33142764040',
            ' ': '',
            None: None,
            '1234': '1234'
        }

        for value, expected_value in values.iteritems():
            normalized_value = normalize_phone(value)
            self.assertEqual(normalized_value, expected_value)

    def test_normalize_emails(self):
        values = {
            'test@test.com': 'test@test.com',
            ' test@test.com ': 'test@test.com',
            'TEST@TEST.COM': 'test@test.com',
            ' test @ test.com ': 'test@test.com',
            'test_test+test@test.test.com': 'test_test+test@test.test.com',
            ' ': '',
            None: None,
            '1234': '1234'
        }

        for value, expected_value in values.iteritems():
            normalized_value = normalize_email(value)
            self.assertEqual(normalized_value, expected_value)

    def test_normalize_vats(self):
        values = {
            '75383118C': 'ES75383118C',
            'ES75383118C': 'ES75383118C',
            ' 7_5-38/3.118 c ': 'ES75383118C',
            'y-4937904 W': 'ESY4937904W',
            'ps2020202N': 'PS2020202N',
            '  d-53 79-04.73   ': 'ESD53790473',
            ' ': '',
            None: None,
            '1234': '1234'
        }

        for value, expected_value in values.iteritems():
            normalized_value = normalize_vat(value)
            self.assertEqual(normalized_value, expected_value)

    def test_normalize_cups(self):
        values = {
            'ES0021000012650424KF': 'ES0021000012650424KF',
            'ES-0021-0000-1265-0424-KF': 'ES0021000012650424KF',
            'ES.0021.0000.1265.0424.KF': 'ES0021000012650424KF',
            '  ES 0021  0000 1265 0424 KF': 'ES0021000012650424KF',
            'es0021000012650424kf': 'ES0021000012650424KF',
            'ESOO21OOOO1265O424KF': 'ES0021000012650424KF',
            'es 00314O6 094042 003EF0F': 'ES0031406094042003EF0F',
            None: None,
            '': '',
            'NOTVALID': 'NOTVALID'
        }

        for value, expected_value in values.iteritems():
            normalized_value = normalize_cups(value)
            self.assertEqual(normalized_value, expected_value)

    def test_normalize_names(self):
        values = {
            'Apellido1 Apellido2, Nombre1': ('Nombre1', 'Apellido1 Apellido2'),
            'Apellido1 Apellido2, Nombre1': ('nombre1', 'apellido1 apellido2'),
            'Apellido1 Apellido2, Nombre1': ('NOMBRE1', 'APELLIDO1 APELLIDO2'),
            'Apellido1 Apellido2, Nombre1': (' Nombre1  ', ' Apellido1   Apellido2   '),
            'Apellido1, Nombre1': ('Nombre1', 'Apellido1'),
            'Empresa S.L.': ('Empresa S.L.', None),
            'Empresa S.L.': (' Empresa   S.L.  ', None),
            'Empresa S.L.': ('EMPRESA S.L.', None),
            'Empresa s.l.': ('empresa s.l.', None)
        }

        for expected_value, (name, surnames) in values.iteritems():
            normalized_value = normalize_name(name, surnames)
            self.assertEqual(normalized_value, expected_value,
                             '("{}", "{}") = {} != {}'.format(name, surnames, normalized_value, expected_value))


if __name__ == '__main__':
    unittest.main()
