# -*- coding: utf-8 -*-
import unittest

from lucera_common.fake import get_random_nif, get_random_iban
from lucera_common.testing import *
from expects import *
from stdnum import iban
from stdnum.es import ccc
from stdnum.es import cups
from stdnum.es import vat


# noinspection PyMethodMayBeStatic
class FakeTests(unittest.TestCase):

    def test_can_get_random_bank_account(self):
        number = get_random_bank_account(2038, 9937, 6000240525)
        expect(number).to(equal('20389937156000240525'))
        expect(ccc.is_valid(number)).to(be_true)

    def test_can_get_random_iban(self):
        number = get_random_iban('ES', 49, 4410, 2490013140)
        expect(number).to(equal('ES3800494410182490013140'))
        expect(iban.is_valid(number)).to(be_true)

    def test_can_get_random_cups(self):
        cups_code = get_random_cups_code(23)
        expect(cups.is_valid(cups_code)).to(be_true)
        expect(cups_code).to(start_with('ES0023'))
        cups_code = get_random_cups_code(21)
        expect(cups_code).to(start_with('ES0021'))
        expect(cups.is_valid(cups_code)).to(be_true)

    def test_can_get_random_nif(self):
        nif = get_random_nif()
        expect(vat.is_valid(nif)).to(be_true)

    def test_can_get_multiple_random_cups(self):
        for i in range(0, 100):
            cups_code = get_random_cups_code()
            expect(cups.is_valid(cups_code)).to(be_true)


if __name__ == '__main__':
    unittest.main()
