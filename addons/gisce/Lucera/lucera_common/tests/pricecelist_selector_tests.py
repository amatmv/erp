# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from expects import *
from lucera_common import pricelist_selector


class LuceraApiSignupsContractsTests(testing.OOTestCase):

    def _create_or_update_pricelist(self, pool, cursor, uid, vals, versions):
        pricelist_obj = pool.get('product.pricelist')
        pricelist_version_obj = pool.get('product.pricelist.version')
        if 'kind' not in vals:
            raise Exception, 'Cannot upsert pricelist without kind'

        if 'tarifes_atr_compatibles' in vals:
            product_type = 'electricity'
        elif 'tarifes_atr_gas_compatibles' in vals:
            product_type = 'gas'
        else:
            raise Exception, 'Cannot upsert pricelist without tarifes comptabiles'

        # Search for existing pricelist
        if product_type == 'electricity':
            pricelist_id = pricelist_obj.search(cursor, uid, [('kind', '=', vals['kind']),
                                                              ('tarifes_atr_compatibles', '<>', False)])
        else:
            pricelist_id = pricelist_obj.search(cursor, uid, [('kind', '=', vals['kind']),
                                                              ('tarifes_atr_gas_compatibles', '<>', False)])
        if pricelist_id:
            pricelist_id = pricelist_id[0]

        if pricelist_id:
            pricelist_obj.write(cursor, uid, pricelist_id, vals)
        else:
            pricelist_id = pricelist_obj.create(cursor, uid, vals)

        if not isinstance(versions, list):
            versions = [versions]

        pricelist_version_ids = []
        for version in versions:
            version.update({'pricelist_id': pricelist_id})

            pricelist_version_id = pricelist_version_obj.search(cursor, uid, [('pricelist_id', '=', pricelist_id),
                                                                              ('date_start', '=',
                                                                               version['date_start'])])
            if pricelist_version_id:
                pricelist_version_id = pricelist_version_id[0]

            if pricelist_version_id:
                pricelist_version_obj.write(cursor, uid, pricelist_version_id, version)
            else:
                pricelist_version_id = pricelist_version_obj.create(cursor, uid, version)

            pricelist_version_ids.append(pricelist_version_id)

        return pricelist_id, pricelist_version_id if len(pricelist_version_ids) == 1 else pricelist_version_ids

    def _create_test_pricelists(self, pool, cursor, uid):
        currency_id = pool.get('res.currency').search(cursor, uid, [])[0]
        imd_obj = pool.get('ir.model.data')
        tarifa_20a = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'tarifa_20A_new'
        )[1]

        tarifa_31_gas = imd_obj.get_object_reference(
            cursor, uid, 'giscegas_polissa', 'tarifa_31_gas'
        )[1]

        result = {}

        # Electricity Variable
        result['electricity_variable_pricelist_id'], result['electricity_variable_pricelist_version_id'] \
            = self._create_or_update_pricelist(pool, cursor, uid, {
                'name': 'Tarifa Precio Variable Electricidad',
                'type': 'sale',
                'kind': 'variable_price',
                'tarifes_atr_compatibles': [(4, tarifa_20a)],
                'currency_id': currency_id
            },
            [{
                'name': 'Variable Enero 2018 Electricidad',
                'date_start': '2018-01-01',
                'date_end': '2018-01-31'
            }])

        # Electricity Fixed
        result['electricity_fixed_pricelist_id'], result['electricity_fixed_pricelist_version_id'] \
            = self._create_or_update_pricelist(pool, cursor, uid, {
                'name': 'Tarifa Precio Fijo Electricidad',
                'type': 'sale',
                'kind': 'fixed_price',
                'tarifes_atr_compatibles': [(4, tarifa_20a)],
                'currency_id': currency_id},
            [{
                'name': 'Fijo Enero 2018 Electricidad',
                'date_start': '2018-01-01',
                'date_end': '2018-01-31'
            }])

        # Gas Variable
        result['gas_variable_pricelist_id'], result['gas_variable_pricelist_version_id'] \
            = self._create_or_update_pricelist(pool, cursor, uid, {
                'name': 'Tarifa Precio Variable Gas',
                'type': 'sale',
                'kind': 'variable_price',
                'tarifes_atr_gas_compatibles': [(4, tarifa_31_gas)],
                'currency_id': currency_id}, [
                {
                    'name': 'Variable Enero 2018 Gas',
                    'date_start': '2018-01-01',
                    'date_end': '2018-01-31'
                }])

        # Gas Fixed
        result['gas_fixed_pricelist_id'], result['gas_fixed_pricelist_version_id'] \
            = self._create_or_update_pricelist(pool, cursor, uid, {
                'name': 'Tarifa Precio Fijo Gas',
                'type': 'sale',
                'kind': 'fixed_price',
                'tarifes_atr_gas_compatibles': [(4, tarifa_31_gas)],
                'currency_id': currency_id},
            [{
                'name': 'Fijo Enero 2018',
                'date_start': '2018-01-01',
                'date_end': '2018-01-31',

            }])

        return result

    def test_can_select_pricelist_by_kind_and_product_type(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            uid = txn.user
            pool = self.openerp.pool

            result = self._create_test_pricelists(pool, cursor, uid)

            # Act and assert

            date = '2018-01-15'

            # Electricity Variable
            pricelist_id, pricelist_version_id = \
                pricelist_selector.get_current_price_list_by_kind(pool, cursor, uid,
                                                                  'electricity', 'variable_price', date)

            expect(pricelist_id).to(
                equal(result['electricity_variable_pricelist_id']))
            expect(pricelist_version_id).to(
                equal(result['electricity_variable_pricelist_version_id']))

            # Electricity Fixed
            pricelist_id, pricelist_version_id = \
                pricelist_selector.get_current_price_list_by_kind(pool, cursor, uid,
                                                                  'electricity', 'fixed_price', date)

            expect(pricelist_id).to(
                equal(result['electricity_fixed_pricelist_id']))
            expect(pricelist_version_id).to(
                equal(result['electricity_fixed_pricelist_version_id']))

            # Gas Variable

            pricelist_id, pricelist_version_id = \
                pricelist_selector.get_current_price_list_by_kind(pool, cursor, uid,
                                                                  'gas', 'variable_price', date)

            expect(pricelist_id).to(equal(result['gas_variable_pricelist_id']))
            expect(pricelist_version_id).to(
                equal(result['gas_variable_pricelist_version_id']))

            # Gas fixed
            pricelist_id, pricelist_version_id = \
                pricelist_selector.get_current_price_list_by_kind(pool, cursor, uid,
                                                                  'gas', 'fixed_price', date)

            expect(pricelist_id).to(equal(result['gas_fixed_pricelist_id']))
            expect(pricelist_version_id).to(
                equal(result['gas_fixed_pricelist_version_id']))
