# -*- coding: utf-8 -*-
import unittest
from lucera_common.validate import *


class ValidationTests(unittest.TestCase):
    def test_is_valid_phone(self):
        valid_values = ['961580184', '96 158 01 84', '96-158-01-84', '+34 96 158 01 84', '0034961580184',
                        '666581633', '666 58 16 33', '666-58-16-33', '+34 666 58 16 33', '0034666581633',
                        '+33142764040', '+33 1 42 76 40 40', '0033142764040', '00 33 1 42 76 40 40']
        for value in valid_values:
            is_valid = is_valid_phone(value)
            self.assertTrue(is_valid, '{0} was expected to be valid'.format(value))

    def test_is_invalid_phone(self):
        invalid_values = [
            ('123', 'Too short'), 
            ('', 'Empty value'), 
            (None, 'Undefined'), 
            ('229292', 'Too short'), 
            ('96282828282828', 'Too long'), 
            ('995104171', 'Invalid prefix'), 
            ('995-104-171', 'Invalid prefix')]

        for value, reason in invalid_values:
            is_valid = is_valid_phone(value)
            self.assertFalse(is_valid, '{0} was not expected to be valid, reason: {1}'.format(value, reason))

    def test_is_valid_email(self):
        valid_values = ['test@test.com', 'test.mo-re@test.com', 'test+more@test.com',
                        'test_more@test.more.com', 'test_ñu@test.com', 'tést@tést.com', 'ñu@ñu.bla']
        for value in valid_values:
            is_valid = is_valid_email(value)
            self.assertTrue(is_valid, '{0} was expected to be valid'.format(value))
            is_valid = is_valid_email(value.upper())
            self.assertTrue(is_valid, '{0} was expected to be valid'.format(value.upper()))

    def test_is_invalid_email(self):
        invalid_values = ['test', '', None, 'test@', 'test@t']

        for value in invalid_values:
            is_valid = is_valid_email(value)
            self.assertFalse(is_valid, '{0} was not expected to be valid'.format(value))
            
    def test_is_valid_vat(self):
        valid_values = ['75383118C', 'ES75383118C', 'es75383118c',  # nifs
                        'Y4937904W', 'ESY4937904W', 'esY4937904w',  # nies
                        'D53790473', 'ESD53790473', 'esd53790473',  # cifs
                        'PSABC000000A']                             # pasaporte

        for value in valid_values:
            is_valid = is_valid_vat(value)
            self.assertTrue(is_valid, '{0} was expected to be valid'.format(value))

    def test_is_invalid_vat(self):
        invalid_values = ['test', '', None, '', 'test@t',           # bad format values
                          '11111111C', 'ES11111111C'                # bad crc nif
                          'B11111111', 'B11111111'                  # bad crc cif
                          ]

        for value in invalid_values:
            is_valid = is_valid_vat(value)
            self.assertFalse(is_valid, '{0} was not expected to be valid'.format(value))


if __name__ == '__main__':
    unittest.main()
