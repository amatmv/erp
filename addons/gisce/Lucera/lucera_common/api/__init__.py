# -*- coding: utf-8 -*-
import inspect
import json


def get_request_body(request, body_parameter='body'):
    """Returns the request body as json"""
    if isinstance(request, basestring):
        return request
    if body_parameter in request:
        return request[body_parameter]
    return request


def get_request_body_as_json(request):
    """Returns the request body as json"""
    body = get_request_body(request)
    return json.loads(body)


def get_request_param(request, param_name):
    """Returns a request param"""
    return request[param_name]


class ApiRequest(object):
    """An API request"""

    def __init__(self, request):
        self.request = request

    @property
    def body(self):
        return get_request_body(self.request)

    @property
    def body_as_json(self):
        return get_request_body_as_json(self.request)

    def parameter(self, name):
        return get_request_param(self.request, name)

    def __getitem__(self, key):
        return self.parameter(key)


class ApiResponse(object):
    """Generic API Response"""

    # Content cannot be null because it causes a problem with xmlrpc
    def __init__(self, content='', status_code=200):
        self.api_response_content = content
        self.api_response_status_code = status_code


class ApiResponseNotFound(ApiResponse):
    """Not found response"""

    def __init__(self):
        super(ApiResponseNotFound, self).__init__(status_code=404)


class ApiResponseBadRequest(ApiResponse):
    """Bad request response"""

    def __init__(self, message=''):
        self.message = message
        super(ApiResponseBadRequest, self).__init__(status_code=400)


def lucera_api_method(method=None, body='body', mappings=None, all_params_required=False):
    """
    Wrapper method that binds parameters to api method arguments
    """
    if mappings is None:
        mappings = {}

    def decorator(func):
        def wrapper(*original_args, **original_kwargs):
            if len(original_args) == 4 and isinstance(original_args[3], dict):
                # Inspect called function
                args, varargs, keywords, defaults = inspect.getargspec(func)
                # Get called funcion default values or empty dictionary
                default_values = dict(zip(args[-len(defaults):], defaults)) if defaults else {}
                # The third argument has always the posted data
                request = original_args[3]
                # Check all passed arguments exist (may be this is optional)
                # for key in request:
                #     if key not in args[3:]:
                #         raise NameError('%s parameter not found in called api method'.format(key))
                # Add self, cursor, uid to new arguments
                new_arg_values = list(original_args[0:3])
                # Mappings
                argument_maps = {}
                if mappings:  # Invert key<->value because it is more comprensible
                    argument_maps = dict((v, k) for k, v in mappings.iteritems())
                if body:
                    argument_maps[body] = 'body'
                # Add posted data key, values to new arguments
                for arg in args[3:]:
                    key = arg if arg not in argument_maps else argument_maps[arg]
                    if key in request:
                        value = request[key]
                        if arg == body:
                            value = json.loads(value)
                        new_arg_values.append(value)
                    elif key in default_values:
                        new_arg_values.append(default_values[arg])
                    else:
                        # If a parameter is not set it is set to None
                        if all_params_required:
                            raise NameError('%s parameter not found in posted data'.format(key))
                        else:
                            new_arg_values.append(None)
                # function args have to be a tuple
                new_args = tuple(new_arg_values)
                return func(*new_args, **original_kwargs)
            else:
                return func(*original_args, **original_kwargs)
        return wrapper

    if method:
        # This was an actual decorator call, ex: @lucera_api_method
        return decorator(method)
    else:
        # This is a factory call, ex: @lucera_api_method()
        return decorator
