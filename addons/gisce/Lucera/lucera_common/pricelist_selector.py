import logging

from osv import osv


logger = logging.getLogger('pricelist_selector')


def get_price_list_by_kind(pool, cursor, uid, product_type, price_list_kind):
    pricelist_obj = pool.get('product.pricelist')

    electricity_polissa_tarifa_obj = pool.get('giscedata.polissa.tarifa')
    gas_polissa_tarifa_obj = pool.get('giscegas.polissa.tarifa')

    if product_type != 'gas':
        product_type = 'electricity'  # By default is electricity to be backwards compatible

    # To get the version,
    if product_type == 'electricity':
        tarifa_id = electricity_polissa_tarifa_obj.search(cursor, uid, [('codi_ocsum', '=', '001')], order='id asc')[0]  # 2.0A
        price_list_ids = pricelist_obj.search(cursor, uid, [('kind', '=', price_list_kind),
                                                            ('tarifes_atr_compatibles', '=', tarifa_id)])
    else:
        tarifa_id = gas_polissa_tarifa_obj.search(cursor, uid, [('codi_ocsum', '=', '31')], order='id asc')[0]  # 3.1
        price_list_ids = pricelist_obj.search(cursor, uid, [('kind', '=', price_list_kind),
                                                            ('tarifes_atr_gas_compatibles', '=', tarifa_id)])

    if not price_list_ids:
        raise osv.except_osv('Error!', 'No price list found for price list kind and product type')

    if len(price_list_ids) > 1:
        raise osv.except_osv('Error!', 'More than one price list found for price list kind and product type')

    return price_list_ids[0]


def get_current_price_list_by_kind(pool, cursor, uid, product_type, price_list_kind, date):
    """
    Chooses the default active price list for the specified date and kind

    It will choose the price list whose version is active and with a start date newer than the rest.
    If two versions start at the same date, the result is not determined.
    :param pool
    :param product_type: electricity | gas
    :param cursor: Current cursor
    :param uid: Current user id
    :param price_list_kind: Kind of price list: variable, fixed, etc
    :type price_list_kind:string
    :param date: Date to look for price list
    :type date:datetime
    :return: The id of the price list considered the default for the date
    """
    price_list_version_obj = pool.get('product.pricelist.version')

    price_list_id = get_price_list_by_kind(pool, cursor, uid, product_type, price_list_kind)

    price_list_version_ids = price_list_version_obj.search(
        cursor, uid, [('pricelist_id', '=', price_list_id),
                      ('date_start', '<=', date),
                      '|', ('date_end', '>=', date), ('date_end', '=', False)],
        limit=1, order='date_start DESC')

    if len(price_list_version_ids) == 0:
        logger.warning('Error! No price list version found for price list kind and product type')
        return False, False

    price_list_version_id = price_list_version_ids[0]

    return price_list_id, price_list_version_id


def get_current_price_list_by_id(pool, cursor, uid, pricelist_id, date):
    """
    Chooses the default active price list for the specified date and kind

    It will choose the price list whose version is active and with a start date newer than the rest.
    If two versions start at the same date, the result is not determined.

    :param cursor: Current cursor
    :param uid: Current user id
    :param pricelist_id: Id of price list
    :type pricelist_id:int
    :param date: Date to look for price list
    :type date:datetime
    :return: The id of the price list considered the default for the date
    :rtype:int
    """

    price_list_version_obj = pool.get('product.pricelist.version')
    price_list_version_ids = price_list_version_obj.search(
        cursor, uid, [('pricelist_id', '=', pricelist_id),
                      ('date_start', '<=', date),
                      '|', ('date_end', '>=', date), ('date_end', '=', False)],
        limit=1, order='date_start DESC')

    if len(price_list_version_ids) == 0:
        return False, False

    price_list_version_id = price_list_version_ids[0]
    price_list_id = price_list_version_obj.read(cursor, uid,
                                                price_list_version_id, ['pricelist_id'])['pricelist_id'][0]

    return price_list_id, price_list_version_id
