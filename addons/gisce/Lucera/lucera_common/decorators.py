# -*- coding: utf-8 -*-
import netsvc
from lucera_common.api import *
from lucera_common.validate import *
from osv import osv
from osv.osv import except_osv
from osv.orm import ValidateException
import json
import pooler
import sql_db


def lucera_local_transaction(method=None, body='body', mappings=None, all_params_required=False):
    """
    Wrapper method that
    """
    if mappings is None:
        mappings = {}

    def decorator(func):
        def wrapper(*original_args, **original_kwargs):
            # Replace original cursor with a new one
            args = []
            for a in original_args:
                if isinstance(a, sql_db.Cursor):
                    db = pooler.get_db_only(a.dbname)
                    cr = db.cursor()
                    args.append(cr)
                else:
                    args.append(a)
            try:
                res = func(*args, **original_kwargs)
                cr.commit()  # all good, we commit
            except Exception:
                cr.rollback()  # error, rollback everything atomically
                raise
            finally:
                cr.close()  # always close cursor opened manually
            return res
        return wrapper

    if method:
        # This was an actual decorator call, ex: @lucera_api_method
        return decorator(method)
    else:
        # This is a factory call, ex: @lucera_api_method()
        return decorator