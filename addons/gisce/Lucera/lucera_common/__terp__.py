# -*- coding: utf-8 -*-
{
  "name": "Lucera common",
  "description": """Common functions and tools shared between lucera modules """,
  "version": "0-dev",
  "author": "Lucera",
  "category": "Lucera",
  "depends": [
    "base",
    "product",
    "giscedata_polissa",
    "giscegas_polissa",
    "giscegas_facturacio_comer",
    "giscedata_facturacio_comer_lucera"
  ],
  "init_xml": [],
  "demo_xml": [],
  "update_xml": [],
  "active": False,
  "installable": True
}
