# -*- coding: utf-8 -*-
import re
from normalize import *
from stdnum import es
from tools.translate import _
from osv import osv

def is_valid_email(email):
    """Validates an email
    :type email: basestring
    """
    if not email:
        return False

    email = normalize_email(email)

    if len(email) < 4:
        return False

    email_regex = re.compile('^[A-Za-z0-9ñÑáéíóúàèìòù.+_-]+@[A-Za-z0-9ñÑáéíóúàèìòù._-]+\.[a-zA-Z]*$',
                             re.IGNORECASE | re.UNICODE)

    if not email_regex.match(email.lower()):
        return False
    return True


def is_valid_phone(phone):
    """Validates a phone"""
    if not phone:
        return False

    phone = normalize_phone(phone)

    if len(phone) < 3:
        return False

    try:
        p = phonenumbers.parse(phone, region='ES')
        return phonenumbers.is_valid_number(p)
    except Exception:
        return False


def is_valid_vat(vat):
    """Validates a vat"""
    if not vat:
        return False

    vat = normalize_vat(vat)

    if vat.startswith('PS') and len(vat) > 7:
        return True # We cannot validate passports

    return vatnumber.check_vat(vat)


def is_valid_cif(vat):
    """Validates a CIF"""

    if not vat:
        return False

    vat = normalize_vat(vat)

    # Remove prepending country code if exists
    vat = vat[2:] if vat.startswith('ES') else vat

    return es.cif.is_valid(vat)


def is_valid_postal_code(postal_code):
    """
    Check if postal code is valid

    :param postal_code:
    :type postal_code: str
    :return:
    :rtype: bool
    """

    if not postal_code:
        return False

    postal_code = normalize_postal_code(postal_code)

    if len(postal_code) != 5:
        return False

    if not re.match(r"^\d+$", postal_code):
        return False

    return True
