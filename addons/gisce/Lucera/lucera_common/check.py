# -*- coding: utf-8 -*-
from osv import osv
from tools.translate import _


def is_valid_power_and_tariff(pool, cursor, uid, tariff_code, power):
    tarifa_obj = pool.get('giscedata.polissa.tarifa')
    tariff_id = tarifa_obj.search(cursor, uid, [('codi_ocsum', '=', tariff_code)])[0]
    tariff = tarifa_obj.browse(cursor, uid, tariff_id)
    if power > 100:
        power /= 1000.0  # Change to kW

    if not tariff:
        raise osv.except_osv(_('ValidateError'), _('Tariff with code {} not found!'.format(tariff_code)))
    if tariff.pot_min > power:
        raise osv.except_osv(_('ValidateError'), _('Power < pot_min!'))
    if tariff.pot_max < power:
        raise osv.except_osv(_('ValidateError'), _('Power > pot_max!'))
    return
