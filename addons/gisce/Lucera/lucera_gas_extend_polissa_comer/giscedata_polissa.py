# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscedataPolissa(osv.osv):
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    _columns = {
        'gas_polissa_id': fields.many2one('giscegas.polissa', 'Contrato gas')
    }

    def _gas_polissa_id_unique(self, cursor, uid, ids, context=None):
        polissa_obj = self.pool.get('giscedata.polissa')
        for polissa in self.browse(cursor, uid, ids, context=context):
            if polissa.active and polissa.state != 'esborrany' and polissa.gas_polissa_id.id:
                found = polissa_obj.search_count(cursor, uid, [
                    ('id', '<>', polissa.id),
                    ('gas_polissa_id', '=', polissa.gas_polissa_id.id)])
                return found == 0
        return True

    _constraints = [(_gas_polissa_id_unique,
                     u'Ya existe un contrato de gas asociado al contrato de electricidad',
                     ['gas_polissa_id'])]

    # _sql_constraints = [('gas_polissa_id', 'unique (gas_polissa_id, cups)',
    #                      'Ya existe un contrato de gas asociado al contrato de electricidad')]

    def create(self, cursor, uid, vals, context=None):
        electricity_polissa_id = super(GiscedataPolissa, self).create(cursor, uid, vals, context=context)
        # Update related electricity polissa
        gas_polissa_id = vals.get('gas_polissa_id', False)
        if gas_polissa_id:
            self._update_related_gas_polissa(cursor, uid, electricity_polissa_id, gas_polissa_id, context=context)

        return electricity_polissa_id

    def write(self, cursor, uid, ids, vals, context=None):
        if not context:
            context = {}

        if isinstance(ids, (int, long)):
            ids = [ids]
        # Update related electricity polissa
        if 'gas_polissa_id' in vals and not context.get('polissa_link_in_progress', False):
            for electricity_polissa_id in ids:
                self._update_related_gas_polissa(cursor, uid, electricity_polissa_id, vals['gas_polissa_id'],
                                                 context=context)

        # We don't send bus message here because it can be sent when activated
        res = super(GiscedataPolissa, self).write(cursor, uid, ids, vals, context=context)
        return res

    def unlink(self, cursor, uid, ids, context=None):
        linked_gas_polissas = self.read(cursor, uid, ids, ['gas_polissa_id'])

        res = super(GiscedataPolissa, self).unlink(cursor, uid, ids, context=context)
        
        for polissa in linked_gas_polissas:
            if 'gas_polissa_id' in polissa and polissa['gas_polissa_id']:
                self._update_related_gas_polissa(cursor, uid, polissa['id'], polissa['gas_polissa_id'], context=context)
        return res

    def _update_related_gas_polissa(self, cursor, uid, electricity_polissa_id, gas_polissa_id, context=None):
        if not context:
            context = {}

        context['polissa_link_in_progress'] = True

        giscegas_polissa_obj = self.pool.get('giscegas.polissa')

        if gas_polissa_id:
            # Only update if values do not match to avoid recursion
            gas_polissa = giscegas_polissa_obj.browse(cursor, uid, gas_polissa_id, context=context)
            if gas_polissa.electricity_polissa_id.id != electricity_polissa_id:
                giscegas_polissa_obj.write(cursor, uid, gas_polissa_id,
                                           {'electricity_polissa_id': electricity_polissa_id},
                                           context=context)
        else:  # It has been deleted
            gas_polissa_ids = giscegas_polissa_obj.search(cursor, uid,
                                                          [('electricity_polissa_id', '=', electricity_polissa_id)],
                                                          context=context)
            if gas_polissa_ids:
                giscegas_polissa_obj.write(cursor, uid, gas_polissa_ids, {'electricity_polissa_id': False},
                                           context=context)


GiscedataPolissa()
