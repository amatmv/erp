# -*- coding: utf-8 -*-
from lucera_gas_extend_polissa_comer.giscedata_polissa import GiscedataPolissa
from lucera_common import config
from osv import osv, fields


class GiscegasPolissa(osv.osv):

    _name = 'giscegas.polissa'
    _inherit = 'giscegas.polissa'

    def _get_manage_url(self, cursor, uid, ids, field_name, arg, context=None):
        res = {}
        for contract_id in ids:
            res[contract_id] = 'https://manage.lucera.es/gas/contracts/{}'.format(contract_id)
        return res

    def _electricity_polissa_id(self, cursor, uid, ids, field_name, arg, context=None):
        res = dict.fromkeys(ids, False)
        giscedata_polissa_obj = self.pool.get('giscedata.polissa')
        electricity_polissa_ids = giscedata_polissa_obj.search(cursor, uid, [('gas_polissa_id', 'in', ids)])
        polissas = giscedata_polissa_obj.read(cursor, uid, electricity_polissa_ids, ['polissa_id'])
        for p in polissas:
            res[p['gas_polissa_id']] = p['id']
        return res

    def _on_electricity_polissa_id_changed(self, cursor, uid, ids, context=None):
        if not context:
            context = {}

        giscedata_polissa_obj = self.pool.get('giscedata.polissa')
        electricity_polissa_vals = giscedata_polissa_obj.read(cursor, uid, ids, ['gas_polissa_id'])
        return [x['gas_polissa_id'] for x in electricity_polissa_vals if x.get('gas_polissa_id', False)]

    _columns = {
        'manage_url': fields.function(_get_manage_url,
                                      method=True,
                                      type='char',
                                      size=250,
                                      string='Manage',
                                      readonly=True),
        'electricity_polissa_id': fields.many2one('giscedata.polissa', 'Contrato electricidad'),
        'affiliate_code': fields.char('Afiliado', size=32, required=False),
        'affiliate_external_id': fields.char('Código externo afilidado', size=32, required=False),
        'collective': fields.selection(config.LUCERA_COLLECTIVES_SELECTION, 'Colectivo', required=False, size=32)
    }

    def _electricity_polissa_id_unique(self, cursor, uid, ids, context=None):
        polissa_obj = self.pool.get('giscegas.polissa')
        for polissa in self.browse(cursor, uid, ids, context=context):
            if polissa.active and polissa.state != 'esborrany' and polissa.electricity_polissa_id.id:
                found = polissa_obj.search_count(cursor, uid, [
                    ('id', '<>', polissa.id),
                    ('electricity_polissa_id', '=', polissa.electricity_polissa_id.id)])
                return found == 0
        return True

    _constraints = [(_electricity_polissa_id_unique,
                     u'Ya existe un contrato de gas asociado al contrato de electricidad',
                     ['electricity_polissa_id'])]

    def create(self, cursor, uid, vals, context=None):
        bus = self.pool.get('lucera.bus')

        # Create gas polissa
        gas_polissa_id = super(GiscegasPolissa, self).create(cursor, uid, vals, context=context)

        # Search for active polissa with same cups and send bus if not exists
        existing_polissa_ids = False
        if 'cups' in vals:
            existing_polissa_ids = self.search(cursor, uid, [('cups', '=', vals['cups']), ('id', '!=', gas_polissa_id)])
        if not existing_polissa_ids:
            bus.send(cursor, uid, 'giscegas.polissa.created', {'polissa_id': gas_polissa_id}, 'erp')

        # Update related electricity polissa
        electricity_polissa_id = vals.get('electricity_polissa_id', False)
        if electricity_polissa_id:
            self._update_related_electricity_polissa(cursor, uid,
                                                     gas_polissa_id, electricity_polissa_id, context=context)

        return gas_polissa_id

    def write(self, cursor, uid, ids, vals, context=None):
        if not context:
            context = {}
        # Update related electricity polissa
        if isinstance(ids, (int, long)):
            ids = [ids]

        if 'electricity_polissa_id' in vals and not context.get('polissa_link_in_progress', False):
            for gas_polissa_id in ids:
                self._update_related_electricity_polissa(cursor, uid, gas_polissa_id,
                                                         vals['electricity_polissa_id'],
                                                         context=context)

        # TODO: We should move it to the switching process when it is ready
        # Update it is activating the polissa.
        if 'active' in vals and vals['active']:
            polissas = self.browse(cursor, uid, ids)
            for p in polissas:
                if not p.active:
                    bus = self.pool.get('lucera.bus')
                    bus.send(cursor, uid, 'giscegas.polissa.activated', {'polissa_id': p.id}, 'erp')

        res = super(GiscegasPolissa, self).write(cursor, uid, ids, vals, context=context)
        return res

    def unlink(self, cursor, uid, ids, context=None):
        res = super(GiscegasPolissa, self).unlink(cursor, uid, ids, context=context)

        # Update related electricity polissa
        for polissa in self.browse(cursor, uid, ids, context=context):
            if polissa.electricity_polissa_id:
                self._update_related_electricity_polissa(cursor, uid, polissa.id,
                                                         polissa.electricity_polissa_id.id, context=context)
        return res

    def _update_related_electricity_polissa(self, cursor, uid, gas_polissa_id, electricity_polissa_id, context=None):
        if not context:
            context = {}

        context['polissa_link_in_progress'] = True

        electricity_polissa_obj = self.pool.get('giscedata.polissa')

        if electricity_polissa_id:
            # Only update if values do not match to avoid recursion
            electricity_polissa = electricity_polissa_obj.browse(cursor, uid, electricity_polissa_id, context=context)
            if electricity_polissa.gas_polissa_id.id != gas_polissa_id:
                electricity_polissa_obj.write(cursor, uid, electricity_polissa_id,
                                              {'gas_polissa_id': gas_polissa_id}, context=context)
        else:  # It has been deleted
            electricity_polissa_ids = electricity_polissa_obj.search(cursor, uid,
                                                                     [('gas_polissa_id', '=', gas_polissa_id)],
                                                                     context=context)
            if electricity_polissa_ids:
                electricity_polissa_obj.write(cursor, uid, electricity_polissa_ids,
                                              {'gas_polissa_id': False}, context=context)


GiscegasPolissa()
