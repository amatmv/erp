# lucera_gas_extend_polissa_comer

* Añade el campo 'manage_url' al contrato de gas para poder visitar fácilmente manage.
* Envía un mensaje al bus cuando se activa el contrato de gas.
* Relaciona la póliza de electricidad con la póliza de gas.

## Campos y entidades

### giscegas.polissa

- **manage_url** Muestra la url de gestión en manage
- **electricity_polissa_id** Póliza de electricidad relacionada.


### giscedata.polissa

- **gas_polissa_id** 
    








