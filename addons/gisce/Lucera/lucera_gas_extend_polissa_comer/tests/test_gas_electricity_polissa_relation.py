# -*- coding: utf-8 -*-
import unittest
from datetime import datetime
from dateutil.relativedelta import relativedelta
from destral import testing
from destral.transaction import Transaction
from expects import *
from osv.orm import except_orm
from lucera_common.testing.electricity import create_test_electricity_polissa
from lucera_common.testing.gas import create_test_gas_polissa


class TestGasElectricityPolissaRelation(testing.OOTestCase):

    def test_can_create_relation_between_gas_and_electricity_polissas(self):
        pool = self.openerp.pool
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            # Arrange
            electricity_polissa_id = create_test_electricity_polissa(pool, cursor, uid)['polissa_id']
            gas_polissa_id = create_test_gas_polissa(pool, cursor, uid)['gas_polissa_id']

            # Act
            self.openerp.pool.get('giscegas.polissa').write(cursor, uid, gas_polissa_id,
                                                            {'electricity_polissa_id': electricity_polissa_id})

            # Assert
            elec_polissa = self.openerp.pool.get('giscedata.polissa').browse(cursor, uid, electricity_polissa_id)

            expect(elec_polissa.gas_polissa_id.id).to(equal(gas_polissa_id))

    def test_can_create_relation_between_electricity_and_gas_polissas(self):
        pool = self.openerp.pool
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            # Arrange
            electricity_polissa_id = create_test_electricity_polissa(pool, cursor, uid)['polissa_id']
            gas_polissa_id = create_test_gas_polissa(pool, cursor, uid)['gas_polissa_id']

            # Act
            self.openerp.pool.get('giscedata.polissa').write(cursor, uid, electricity_polissa_id,
                                                             {'gas_polissa_id': gas_polissa_id})

            # Assert
            gas_polissa = self.openerp.pool.get('giscegas.polissa').browse(cursor, uid, gas_polissa_id)

            expect(gas_polissa.electricity_polissa_id.id).to(equal(electricity_polissa_id))

    def test_can_clone_linked_electricity_and_gas_polissas(self):
        pool = self.openerp.pool
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            # Arrange
            electricity_polissa_id = create_test_electricity_polissa(pool, cursor, uid)['polissa_id']
            gas_polissa_id = create_test_gas_polissa(pool, cursor, uid)['gas_polissa_id']
            self.openerp.pool.get('giscedata.polissa').write(cursor, uid, electricity_polissa_id,
                                                             {'gas_polissa_id': gas_polissa_id})

            polissa_obj = pool.get('giscedata.polissa')
            gas_polissa_obj = pool.get('giscegas.polissa')

            # Act
            new_electricity_polissa_id = polissa_obj.copy(cursor, uid, electricity_polissa_id)
            new_gas_polissa_id = gas_polissa_obj.copy(cursor, uid, gas_polissa_id)

            # Assert
            gas_polissa = self.openerp.pool.get('giscegas.polissa').browse(cursor, uid, new_gas_polissa_id)
            expect(gas_polissa.electricity_polissa_id.id).to(equal(new_electricity_polissa_id))
