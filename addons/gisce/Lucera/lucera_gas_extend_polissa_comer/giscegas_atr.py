# -*- coding: utf-8 -*-

from osv import osv, fields
from datetime import datetime


class GiscegasAtrHelpers(osv.osv):

    _name = 'giscegas.atr.helpers'
    _inherit = 'giscegas.atr.helpers'

    def activa_polissa_from_a3n(self, cursor, uid, sw_id, context=None):
        res = super(GiscegasAtrHelpers, self).activa_polissa_from_a3n(
                      cursor, uid, sw_id, context=context)

        # We send the activation event to the bus
        sw_obj = self.pool.get('giscegas.atr')
        sw = sw_obj.browse(cursor, uid, sw_id)
        bus = self.pool.get('lucera.bus')
        bus.send(cursor, uid, 'giscegas.polissa.activated', {'polissa_id': sw.cups_polissa_id.id}, 'erp')
        return res


GiscegasAtrHelpers()