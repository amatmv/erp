# -*- coding: utf-8 -*-
{
  "name": "Lucera gas polissa customizations",
  "description": """Customize gas polissa""",
  "version": "0-dev",
  "author": "Lucera",
  "category": "Lucera",
  "depends": ['base',
              'giscegas_polissa',
              'giscegas_facturacio',
              'giscegas_facturacio_comer',
              'giscegas_atr',
              'lucera_common',
              'lucera_extend_polissa_comer',
              'lucera_gas_polissa_simple_search',
              'lucera_bus'],
  "init_xml": [],
  "demo_xml": [],
  "update_xml": ['giscegas_polissa_view.xml',
                 'giscedata_polissa_view.xml'],
  "active": False,
  "installable": True
}
