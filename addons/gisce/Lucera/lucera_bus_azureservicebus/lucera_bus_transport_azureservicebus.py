# -*- coding: utf-8 -*-

from lucera_bus.transports import LuceraBusBaseTransport, register_transport_provider
from azure.servicebus import ServiceBusService, Message, Queue
import json


class LuceraBusAzureServiceBusTransport(LuceraBusBaseTransport):
    """ Azure Service Bus Transport """

    def __init__(self, config=None):
        if not config:
            raise Exception('No configuration has been passed to azure service bus transport')

        super(LuceraBusAzureServiceBusTransport, self).__init__(config)

        self.config = config
        c = json.loads(config)
        self.bus_service = ServiceBusService(
            service_namespace=c['service_namespace'],
            shared_access_key_name=c['shared_access_key_name'],
            shared_access_key_value=c['shared_access_key_value'])

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def send(self, queue, message):
        msg = Message(message)
        self.bus_service.send_queue_message(queue, msg)

    def publish(self, topic, message):
        msg = Message(message.content)
        self.bus_service.send_topic_message(topic, msg)


# We register the provider
register_transport_provider('azureservicebus', LuceraBusAzureServiceBusTransport)
