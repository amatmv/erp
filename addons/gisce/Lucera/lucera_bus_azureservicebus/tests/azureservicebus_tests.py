# -*- coding: utf-8 -*-

import unittest
from destral import testing
from destral.transaction import Transaction
from expects import *
import os
import json



@unittest.skip('This are integration tests and should be executed manually')
class LuceraBusAzureServiceBusTests(testing.OOTestCase):
    """ Test that a polissa cannot be activated in multiple cases"""

    def new_bus_transaction(self):
        txn = Transaction().start(self.database)

        # Add fake endpoint if it does not exist
        endpoint_obj = self.openerp.pool.get('lucera.bus.endpoint')
        endpoint_ids = endpoint_obj.search(txn.cursor, txn.user, [('name', '=', 'azureservicebus-test')])
        if not endpoint_ids:
            transport_config = json.dumps({
                'service_namespace': os.environ['AZURESERVICEBUS_service_namespace'],
                'shared_access_key_name': os.environ['AZURESERVICEBUS_shared_access_key_name'],
                'shared_access_key_value': os.environ['AZURESERVICEBUS_shared_access_key_value']
            })
            self.endpoint_id = endpoint_obj.create(txn.cursor, txn.user,
                                                   {'name': 'azureservicebus-test',
                                                    'transport': 'azureservicebus',
                                                    'default': True,
                                                    'transport_config': transport_config})
        else:
            self.endpoint_id = endpoint_ids[0]

        return txn


    def test_send_message(self):
        with self.new_bus_transaction() as txn:
            # Arrange
            pool = self.openerp.pool
            cursor = txn.cursor
            user = txn.user

            # Act
            lucera_bus = pool.get('lucera.bus')
            message_id = lucera_bus.send(cursor, user, 'test.message1', 'Hello World!', queue='hello')
            cursor.commit()

            # Assert
            expect(message_id).to(be_above(0))

            message_obj = pool.get('lucera.bus.message')
            message = message_obj.browse(cursor, user, message_id)
            expect(message.name).to(equal('test.message1'))
            expect(message.content).to(equal('Hello World!'))
            expect(message.type).to(equal('command'))
            expect(message.state).to(equal('sent'))

    def test_publish_message(self):
        with self.new_bus_transaction() as txn:
            # Arrange
            pool = self.openerp.pool
            cursor = txn.cursor
            user = txn.user

            # Act
            lucera_bus = pool.get('lucera.bus')
            message_id = lucera_bus.publish(cursor, user, 'test.message1', 'Hello World!', topic='hello')
            cursor.commit()

            # Assert
            expect(message_id).to(be_above(0))

            message_obj = pool.get('lucera.bus.message')
            message = message_obj.browse(cursor, user, message_id)
            expect(message.name).to(equal('test.message1'))
            expect(message.content).to(equal('Hello World!'))
            expect(message.type).to(equal('command'))
            expect(message.state).to(equal('sent'))
