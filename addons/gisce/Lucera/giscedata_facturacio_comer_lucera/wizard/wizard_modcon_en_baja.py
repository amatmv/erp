# -*- coding: utf-8 -*-
from datetime import date, timedelta, datetime

import netsvc
from tools.translate import _
from osv import osv, fields


class WizardModconEnBaja(osv.osv_memory):

    _name = 'wizard.modcon.en.baja'

    def do_modcon(self, cursor, uid, ids, context=None):
        if isinstance(ids, (tuple, list)):
            ids = ids[0]

        vals = self.read(
            cursor, uid, ids, [
                'fecha_inicio', 'fecha_final', 'ultima_modcon',
                'cuenta_bancaria', 'direccion_notificacion'
            ]
        )[0]

        modcon_obj = self.pool.get('giscedata.polissa.modcontractual')

        if vals.get('direccion_notificacion') or vals.get('cuenta_bancaria'):

            data_final = datetime.strftime((
                    datetime.strptime(vals['fecha_inicio'], '%Y-%m-%d')
                    - timedelta(days=1)), '%Y-%m-%d'
            )

            modcon_obj.write(cursor, uid, vals['ultima_modcon'], {
                'data_final': data_final
            })

            new_modcon_vals = modcon_obj.copy_data(
                cursor, uid, vals['ultima_modcon']
            )[0]
            contract_values = {}
            if vals['direccion_notificacion']:
                cursor.execute(*(
                    self.pool.get('res.partner.address').q(cursor, uid).select(
                        ['partner_id']
                    ).where([('id', '=', vals['direccion_notificacion'])])
                ))
                direccion_partner_id = cursor.fetchone()
                direccion_partner_id = False if direccion_partner_id is None else direccion_partner_id[0]
                if direccion_partner_id == new_modcon_vals['titular']:
                    notificacio = 'titular'
                elif direccion_partner_id == new_modcon_vals['pagador']:
                    notificacio = 'pagador'
                else:
                    contract_values.update({'altre_p': direccion_partner_id})
                    new_modcon_vals.update({'altre_p': direccion_partner_id})
                    notificacio = 'altre_p'
                new_modcon_vals.update({
                    'direccio_notificacio': vals['direccion_notificacion'],
                    'notificacio': notificacio
                })
                contract_values['direccio_notificacio'] = vals['direccion_notificacion']
                contract_values['notificacio'] = notificacio

            if vals['cuenta_bancaria']:
                new_modcon_vals.update({
                    'bank': vals['cuenta_bancaria']
                })
                contract_values['bank'] = vals['cuenta_bancaria']

            new_modcon_vals.update({
                'data_inici': vals['fecha_inicio'],
                'data_final': vals['fecha_final'],
                'active': False,
                'state': 'baixa',
                'modcontractual_ant': vals['ultima_modcon'],
            })
            new_modcon = modcon_obj.create(cursor, uid, new_modcon_vals)

            wf_service = netsvc.LocalService('workflow')
            wf_service.trg_validate(
                uid, 'giscedata.polissa.modcontractual', new_modcon, 'baixa',
                cursor
            )
            # We write the new modcon as the following of the previous
            modcon_obj.write(cursor, uid, vals['ultima_modcon'], {
                'modcontractual_seg': new_modcon,
                'state': 'baixa2'
            })

            # We want to remove the fields modcontractual_seg and
            # modcontractual_ant to write them to the contract
            self.pool.get('giscedata.polissa').write(
                cursor, uid, [new_modcon_vals['polissa_id']], contract_values
            )
            self.write(cursor, uid, [ids], {'state': 'end'})

        else:
            self.write(cursor, uid, [ids], {'state': 'error'})

    def onchange_fecha_inicio(
            self, cursor, uid, ids, fecha_inicio, fecha_final,
            ultima_modcon, context=None
    ):
        res = {'value': {}, 'warning': {}, 'domain': {}}
        if not fecha_inicio or not fecha_final or not ultima_modcon:
            return res

        modcon_obj = self.pool.get('giscedata.polissa.modcontractual')

        data_inici_modcon = modcon_obj.read(
            cursor, uid, ultima_modcon, ['data_inici']
        )['data_inici']
        data_inici_modcon = datetime.strptime(data_inici_modcon, '%Y-%m-%d')
        fecha_final = datetime.strptime(fecha_final, '%Y-%m-%d')

        if data_inici_modcon < datetime.strptime(fecha_inicio, '%Y-%m-%d') < fecha_final:
            res['value'].update({'data_inici': fecha_inicio})
        else:
            res['value'].update({'data_inici': False})
            res['warning'].update({
                'title': _(u'¡Error! Fecha no válida.'),
                'message': _(
                    u"La fecha de inicio debe estar comprendida dentro de la "
                    u"última modificación actual del contrato. ({} - {})"
                ).format(
                    data_inici_modcon.strftime('%d-%m-%Y'),
                    fecha_final.strftime('%d-%m-%Y'),
                )
            })

        return res

    def onchange_bank(
            self, cursor, uid, ids, cuenta_bancaria, ultima_modcon, context=None
    ):
        warning_template = {'value': {}, 'warning': {}, 'domain': {}}
        if not cuenta_bancaria or not ultima_modcon:
            return warning_template

        modcon_obj = self.pool.get('giscedata.polissa.modcontractual')

        partner_id = self.pool.get('res.partner.bank').read(
            cursor, uid, cuenta_bancaria, ['partner_id']
        )['partner_id']
        if partner_id:
            partner_id = partner_id[0]
            polissa_id = modcon_obj.read(
                cursor, uid, ultima_modcon, ['polissa_id']
            )['polissa_id'][0]
            pagador = self.pool.get(
                'giscedata.polissa'
            ).read(cursor, uid, polissa_id, ['pagador'])['pagador'][0]
            if pagador == partner_id:
                warning_template['value'].update({'cuenta_bancaria': cuenta_bancaria})
            else:
                warning_template['value'].update({'cuenta_bancaria': False})
                warning_template['warning'].update({
                    'title': _(u'¡Atención! Cuenta bancaria no válida.'),
                    'message': _(
                        u"El propietario de la cuenta bancaria no coincide con "
                        u"el pagador del contrato."
                    )
                })
        return warning_template

    def _get_ultima_modcon(self, cursor, uid, context=None):
        if context is None:
            context = {}

        polissa_id = context.get('active_id', False)
        if not polissa_id:
            raise osv.except_osv(
                "Error",
                _(u"Este asistente necesita ser ser creado con un contrato!")
            )

        return self.pool.get('giscedata.polissa').read(
            cursor, uid, polissa_id, ['modcontractuals_ids']
        )['modcontractuals_ids'][0]

    def _get_fecha_final(self, cursor, uid, context=None):
        if context is None:
            context = {}
        modcon_id = self._get_ultima_modcon(cursor, uid, context=context)
        data_final = self.pool.get('giscedata.polissa.modcontractual').read(
            cursor, uid, modcon_id, ['data_final']
        )['data_final']
        if not data_final:
            return date.today().strftime('%Y-%m-%d')
        return data_final

    _columns = {
        'fecha_inicio': fields.date(_("Fecha inicio"), required=True),
        'fecha_final': fields.date(_('Fecha final')),
        'cuenta_bancaria': fields.many2one(
            'res.partner.bank', 'Cuenta Bancaria',
        ),
        'ultima_modcon': fields.many2one(
            'giscedata.polissa.modcontractual', 'Ultima modificación del contrato'
        ),
        'direccion_notificacion': fields.many2one(
            'res.partner.address', 'Dirección notificación'
        ),
        'state': fields.selection([
            ('init', 'Init'), ('error', 'Error'), ('end', 'End')
        ], 'Estat'),
    }

    _defaults = {
        'ultima_modcon': _get_ultima_modcon,
        'fecha_final': _get_fecha_final,
        'state': lambda *a: 'init'
    }


WizardModconEnBaja()
