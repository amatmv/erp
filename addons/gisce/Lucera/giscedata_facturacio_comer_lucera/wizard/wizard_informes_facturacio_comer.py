# -*- coding: utf-8 -*-
from tools import config
from giscedata_facturacio_comer.wizard.wizard_informes_facturacio_comer import (
    INFORMES_CSV
)
from giscedata_facturacio_comer.sql.sql_column_titles import QUERY_RESUM_COMER_TITLES

INFORMES_CSV.update({
    'export_factures_desglossat': {
        'header': QUERY_RESUM_COMER_TITLES,
        'query': "{}/giscedata_facturacio_comer_lucera/sql/query_resum_comer.sql".format(config['addons_path'])
    }
})
