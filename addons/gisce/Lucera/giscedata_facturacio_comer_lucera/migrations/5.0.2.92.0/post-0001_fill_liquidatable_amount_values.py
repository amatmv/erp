# -*- coding: utf-8 -*-
"""
Omple el camp liquidatable_amount.
"""
import logging


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')
    logger.info('Set liquidatable_amount for existent invoices.')

    # el valor del importe para facturas abiertas será el residual
    query = """
        UPDATE giscedata_facturacio_factura AS fact
        SET liquidatable_amount = (select inv.residual FROM  account_invoice AS inv WHERE fact.invoice_id=inv.id)
        WHERE (select inv.state FROM  account_invoice AS inv WHERE fact.invoice_id=inv.id) = 'open'
    """
    cursor.execute(query)

    # el valor del importe para facturas pagadas será el importe original
    query = """
        UPDATE giscedata_facturacio_factura AS fact
        SET liquidatable_amount = (select inv.amount_total FROM  account_invoice AS inv WHERE fact.invoice_id=inv.id)
        WHERE (select inv.state FROM  account_invoice AS inv WHERE fact.invoice_id=inv.id) = 'paid'
    """
    cursor.execute(query)

    logger.info('liquidatable_amount from invoices updated.')


migrate = up
