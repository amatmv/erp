# -*- coding: utf-8 -*-
"""
Corregeix el signe al camp liquidatable_amount de les factures anuladores.
"""
import logging


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')
    logger.info('Fix liquidatable_amount sign for facturas anuladoras.')

    query = """
        UPDATE giscedata_facturacio_factura AS fact
        SET liquidatable_amount = liquidatable_amount * -1
        WHERE liquidatable_amount > 0 AND 
          (SELECT inv.type FROM  account_invoice AS inv WHERE fact.invoice_id=inv.id) = 'out_refund'
    """
    cursor.execute(query)

    logger.info('liquidatable_amount from facturas anuladoras fixed.')


migrate = up
