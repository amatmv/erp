# -*- coding: utf-8 -*-

from osv import fields, osv
from tools import cache, config


class ProductPricelist(osv.osv):
    """ Price list
    """
    _name = 'product.pricelist'
    _inherit = 'product.pricelist'

    _columns = {
        'kind': fields.selection([('', ''),
                                  ('variable_price', 'Precio variable'),
                                  ('fixed_price', 'Precio fijo')],
                                 'Clasificación'),
    }


ProductPricelist()
