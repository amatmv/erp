select d.name AS "Distribuidora"
    , d.ref AS "Ref. Distribuidora"
    , p.name AS "Comercializadora"
    , p.ref AS "Cod. Comer."
    , pol.name as "Póliza"
    , pt.name
    , replace(tit.name,'"', '') as "Nombre cliente"
    , SUBSTRING(c.name,0,21) AS CUPS
    , c.dp AS "Provincia CUPS"
    , mun.ine AS "INE Municipio"
    , replace(c.direccio,'"', '') as "dirección"
    , t.name AS "Tarifa Acceso"
    , tc.name AS "Tarifa Comercial"
    , CASE WHEN i.type LIKE 'out_%%' THEN i.number
   WHEN i.type LIKE 'in_%%' THEN i.reference
   END AS factura
    , i.number AS "Ref. factura"
    , i.date_invoice AS "Fecha Factura"
    , CASE WHEN f.tipo_rectificadora='A' THEN 'Anuladora'
   WHEN f.tipo_rectificadora='B' THEN 'Anuladora+Rectificadora'
   WHEN f.tipo_rectificadora='R' THEN 'Rectificadora'
   WHEN f.tipo_rectificadora='RA' THEN 'Rectificadora sin anuladora'
   WHEN f.tipo_rectificadora='BRA' THEN 'Anuladora (ficticia)'
   ELSE 'Normal'
   END AS "tipo factura"
    , f.data_inici AS "fecha inicial"
    , f.data_final AS "fecha final"
    , (f.data_final -f.data_inici) + 1 as "num. días"
    , d.vat
    , TO_CHAR(COALESCE(despeses_devolucions.import, 0.0) * (CASE WHEN i.type LIKE '%%_refund' THEN -1 ELSE 1 END), 'FM9999990D999990') AS "Costes de devolución"
    , TO_CHAR(COALESCE(repercussio_bo_social.import, 0.0) * (CASE WHEN i.type LIKE '%%_refund' THEN -1 ELSE 1 END), 'FM9999990D999990') AS "Repercussió bo social"
    , TO_CHAR(COALESCE(atr_energia.atr_energia, 0.0) * (CASE WHEN i.type LIKE '%%_refund' THEN -1 ELSE 1 END), 'FM9999990D999990') AS "Peajes Energía"
    , TO_CHAR(COALESCE(atr_potencia.atr_potencia, 0.0) * (CASE WHEN i.type LIKE '%%_refund' THEN -1 ELSE 1 END), 'FM9999990D999990') AS "Peajes Potencia"
    , TO_CHAR(COALESCE(atr_lloguers.atr_lloguers, 0.0) * (CASE WHEN i.type LIKE '%%_refund' THEN -1 ELSE 1 END), 'FM9999990D999990') AS "Peajes Alquileres"
    , TO_CHAR(COALESCE(atr_altres.atr_altres, 0.0) * (CASE WHEN i.type LIKE '%%_refund' THEN -1 ELSE 1 END), 'FM9999990D999990') AS "Peajes Otros"
    , TO_CHAR(COALESCE(cuota.import, 0.0) * (CASE WHEN i.type LIKE '%%_refund' THEN -1 ELSE 1 END), 'FM9999990D999990') AS "Cuota Lucera"
    , ene.energia   * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS energia_kW
    , f.total_potencia  * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS potencia
    , COALESCE(descuentos_potencia.descuento, 0) * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS descuentos_potencia
    , f.total_energia  * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS energia
    , COALESCE(descuentos_energia.desc_ener, 0) * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS descuentos_energia
    , COALESCE(linies_p1.quantity, 0.0) AS P1
    , TO_CHAR(COALESCE(linies_p1.price_subtotal * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END), 0.0), 'FM9999990D999990') AS P1
    , COALESCE(linies_p2.quantity, 0.0) AS P2
    , TO_CHAR(COALESCE(linies_p2.price_subtotal * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END), 0.0), 'FM9999990D999990') AS P2
    , COALESCE(linies_p3.quantity, 0.0) AS P3
    , TO_CHAR(COALESCE(linies_p3.price_subtotal * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END), 0.0), 'FM9999990D999990') AS P3
    , COALESCE(linies_p4.quantity, 0.0) AS P4
    , TO_CHAR(COALESCE(linies_p4.price_subtotal * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END), 0.0), 'FM9999990D999990') AS P4
    , COALESCE(linies_p5.quantity, 0.0) AS P5
    , TO_CHAR(COALESCE(linies_p5.price_subtotal * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END), 0.0), 'FM9999990D999990') AS P5
    , COALESCE(linies_p6.quantity, 0.0) AS P6
    , TO_CHAR(COALESCE(linies_p6.price_subtotal * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END), 0.0), 'FM9999990D999990') AS P6
    , f.total_reactiva  * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS reactiva
    , COALESCE(exces.exces_potencia, 0) * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS exces
    , altres.altres  * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS "otros conceptos"
    , (altres.altres -  refact.refacturacio)  * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS "otros sin refacturacion"
    , refact.refacturacio  * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS refacturacion
    , refact_T4.refacturacio  * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS refacturacion_T4
    , refact_T1.refacturacio  * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS refacturacion_T1
    , f.total_lloguers  * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS alquiler
    , COALESCE(garantia.garantia, 0) * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS garantia
    , COALESCE(no_iva_concepts.base_costs,0)  * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END)  as conceptos_sin_iva
    , i.amount_untaxed  * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS "total conceptos"
    , it_18.name AS IVA1
    , COALESCE(it_18.base,0)  * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS "Base IVA1"
    , COALESCE(it_18.amount,0)  * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS "total iva1"
    , it_21.name AS IVA2
    , COALESCE(it_21.base,0)  * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS "Base IVA2"
    , COALESCE(it_21.amount,0)  * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS "total iva2"
    , COALESCE(iese_18.base,0)   * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS "Base IESE IVA1"
    , COALESCE(iese_18.amount,0)  * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS "IESE IVA1"
    , (COALESCE(it_iese.base,0) - COALESCE(iese_18.base,0) )  * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS "Base IESE IVA2"
    , (COALESCE(it_iese.amount,0) - COALESCE(iese_18.amount,0)) * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END)  AS "IESE IVA2"
    , COALESCE(it_iese.base,0)  * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END)  AS "Base IESE"
    , COALESCE(it_iese.amount,0)  * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS "total IESE"
    , i.amount_untaxed  * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END)  AS Base
    , i.amount_tax * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END)  AS iva
    , i.amount_total  * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS TOTAL
FROM giscedata_facturacio_factura f
LEFT JOIN giscedata_cups_ps c ON (f.cups_id=c.id)
LEFT JOIN (
    SELECT ine, id AS id_mun
    FROM res_municipi
) AS mun ON mun.id_mun=c.id_municipi
LEFT JOIN giscedata_polissa pol ON (f.polissa_id=pol.id)
LEFT JOIN account_invoice i ON (i.id=f.invoice_id)
LEFT JOIN product_pricelist tc ON (tc.id=f.llista_preu)
LEFT JOIN giscedata_polissa_tarifa t ON (t.id=f.tarifa_acces_id)
LEFT JOIN (
SELECT lf.factura_id AS factura_id,SUM(li.quantity) AS energia
FROM giscedata_facturacio_factura_linia lf
LEFT JOIN account_invoice_line li ON (li.id=lf.invoice_line_id)
WHERE lf.tipus='energia' AND lf.isdiscount = False
GROUP BY lf.factura_id
) AS ene ON (ene.factura_id=f.id)
LEFT JOIN (
  SELECT SUM(ail.quantity) AS quantity, SUM(ail.price_subtotal) AS price_subtotal, lf.factura_id AS factura_id
  FROM giscedata_facturacio_factura_linia lf JOIN account_invoice_line ail ON ail.id=lf.invoice_line_id
  LEFT JOIN product_product p ON p.id=ail.product_id
  LEFT JOIN product_template pt ON pt.id=p.product_tmpl_id
  WHERE lf.tipus = 'energia'
  AND pt.name = 'P1'
  GROUP BY factura_id
) AS linies_p1 ON (linies_p1.factura_id=f.id)
LEFT JOIN (
  SELECT SUM(ail.quantity) AS quantity, SUM(ail.price_subtotal) AS price_subtotal, lf.factura_id AS factura_id
  FROM giscedata_facturacio_factura_linia lf JOIN account_invoice_line ail ON ail.id=lf.invoice_line_id
  LEFT JOIN product_product p ON p.id=ail.product_id
  LEFT JOIN product_template pt ON pt.id=p.product_tmpl_id
  WHERE lf.tipus = 'energia'
  AND pt.name = 'P2'
  GROUP BY factura_id
) AS linies_p2 ON (linies_p2.factura_id=f.id)
LEFT JOIN (
  SELECT SUM(ail.quantity) AS quantity, SUM(ail.price_subtotal) AS price_subtotal, lf.factura_id AS factura_id
  FROM giscedata_facturacio_factura_linia lf JOIN account_invoice_line ail ON ail.id=lf.invoice_line_id
  LEFT JOIN product_product p ON p.id=ail.product_id
  LEFT JOIN product_template pt ON pt.id=p.product_tmpl_id
  WHERE lf.tipus = 'energia'
  AND pt.name = 'P3'
  GROUP BY factura_id
) AS linies_p3 ON (linies_p3.factura_id=f.id)
LEFT JOIN (
  SELECT SUM(ail.quantity) AS quantity, SUM(ail.price_subtotal) AS price_subtotal, lf.factura_id AS factura_id
  FROM giscedata_facturacio_factura_linia lf JOIN account_invoice_line ail ON ail.id=lf.invoice_line_id
  LEFT JOIN product_product p ON p.id=ail.product_id
  LEFT JOIN product_template pt ON pt.id=p.product_tmpl_id
  WHERE lf.tipus = 'energia'
  AND pt.name = 'P4'
  GROUP BY factura_id
) AS linies_p4 ON (linies_p4.factura_id=f.id)
LEFT JOIN (
  SELECT SUM(ail.quantity) AS quantity, SUM(ail.price_subtotal) AS price_subtotal, lf.factura_id AS factura_id
  FROM giscedata_facturacio_factura_linia lf JOIN account_invoice_line ail ON ail.id=lf.invoice_line_id
  LEFT JOIN product_product p ON p.id=ail.product_id
  LEFT JOIN product_template pt ON pt.id=p.product_tmpl_id
  WHERE lf.tipus = 'energia'
  AND pt.name = 'P5'
  GROUP BY factura_id
) AS linies_p5 ON (linies_p5.factura_id=f.id)
LEFT JOIN (
  SELECT SUM(ail.quantity) AS quantity, SUM(ail.price_subtotal) AS price_subtotal, lf.factura_id AS factura_id
  FROM giscedata_facturacio_factura_linia lf JOIN account_invoice_line ail ON ail.id=lf.invoice_line_id
  LEFT JOIN product_product p ON p.id=ail.product_id
  LEFT JOIN product_template pt ON pt.id=p.product_tmpl_id
  WHERE lf.tipus = 'energia'
  AND pt.name = 'P6'
  GROUP BY factura_id
) AS linies_p6 ON (linies_p6.factura_id=f.id)
LEFT JOIN (
SELECT lf.factura_id AS factura_id,SUM(li.price_subtotal) AS exces_potencia
FROM giscedata_facturacio_factura_linia lf
LEFT JOIN account_invoice_line li ON (li.id=lf.invoice_line_id)
WHERE lf.tipus ='exces_potencia'
GROUP BY lf.factura_id
) AS exces ON (exces.factura_id=f.id)
LEFT JOIN (
  SELECT lf.factura_id AS factura_id,SUM(lf.atrprice_subtotal) AS atr_energia
  FROM giscedata_facturacio_factura_linia lf
  WHERE
      lf.tipus ='energia'
  GROUP BY lf.factura_id
) AS atr_energia ON (atr_energia.factura_id=f.id)
LEFT JOIN (
  SELECT lf.factura_id AS factura_id,SUM(lf.atrprice_subtotal) AS atr_potencia
  FROM giscedata_facturacio_factura_linia lf
  WHERE
      lf.tipus ='potencia'
  GROUP BY lf.factura_id
) AS atr_potencia ON (atr_potencia.factura_id=f.id)
LEFT JOIN (
  SELECT lf.factura_id AS factura_id,SUM(lf.atrprice_subtotal) AS atr_lloguers
  FROM giscedata_facturacio_factura_linia lf
  WHERE
      lf.tipus ='lloguer'
  GROUP BY lf.factura_id
) AS atr_lloguers ON (atr_lloguers.factura_id=f.id)
LEFT JOIN (
  SELECT lf.factura_id AS factura_id,SUM(lf.atrprice_subtotal) AS atr_altres
  FROM giscedata_facturacio_factura_linia lf
  WHERE
      lf.tipus ='altres'
  GROUP BY lf.factura_id
) AS atr_altres ON (atr_altres.factura_id=f.id)
LEFT JOIN (
SELECT lf.factura_id AS factura_id,SUM(li.price_subtotal) AS desc_ener
FROM giscedata_facturacio_factura_linia lf
LEFT JOIN account_invoice_line li ON (li.id=lf.invoice_line_id)
WHERE
    lf.tipus ='energia'
    AND lf.isdiscount = True
GROUP BY lf.factura_id
) AS descuentos_energia ON (descuentos_energia.factura_id=f.id)
LEFT JOIN (
SELECT lf.factura_id AS factura_id,SUM(li.price_subtotal) AS descuento
FROM giscedata_facturacio_factura_linia lf
LEFT JOIN account_invoice_line li ON (li.id=lf.invoice_line_id)
WHERE
    lf.tipus ='potencia'
    AND lf.isdiscount = True
GROUP BY lf.factura_id
) AS descuentos_potencia ON (descuentos_potencia.factura_id=f.id)
LEFT JOIN (
SELECT lf.factura_id AS factura_id,SUM(li.price_subtotal) AS refacturacio
FROM giscedata_facturacio_factura_linia lf
LEFT JOIN account_invoice_line li ON (li.id=lf.invoice_line_id)
WHERE lf.tipus='altres' AND li.name LIKE 'Refact%%'
GROUP BY lf.factura_id
) AS refact ON (refact.factura_id=f.id)
LEFT JOIN (
SELECT lf.factura_id AS factura_id,SUM(li.price_subtotal) AS refacturacio
FROM giscedata_facturacio_factura_linia lf
LEFT JOIN account_invoice_line li ON (li.id=lf.invoice_line_id)
WHERE lf.tipus='altres' AND li.name LIKE 'Refact%%T4%%'
GROUP BY lf.factura_id
) AS refact_T4 ON (refact_T4.factura_id=f.id)
LEFT JOIN (
SELECT lf.factura_id AS factura_id,SUM(li.price_subtotal) AS refacturacio
FROM giscedata_facturacio_factura_linia lf
LEFT JOIN account_invoice_line li ON (li.id=lf.invoice_line_id)
WHERE lf.tipus='altres' AND li.name LIKE 'Refact%%T1%%'
GROUP BY lf.factura_id
) AS refact_T1 ON (refact_T1.factura_id=f.id)
LEFT JOIN (
SELECT lf.factura_id AS factura_id,SUM(li.price_subtotal) AS altres
FROM giscedata_facturacio_factura_linia lf
LEFT JOIN account_invoice_line li ON (li.id=lf.invoice_line_id)
WHERE lf.tipus='altres'
GROUP BY lf.factura_id
) AS altres ON (altres.factura_id=f.id)
LEFT JOIN (
SELECT lf.factura_id as factura_id, SUM(li.price_subtotal) AS import
FROM giscedata_facturacio_factura_linia lf
LEFT JOIN account_invoice_line li ON (li.id=lf.invoice_line_id)
WHERE lf.tipus='altres'
AND li.product_id = (
    SELECT res_id
    FROM ir_model_data
    WHERE name = 'product_devolucions'
    AND module='giscedata_facturacio_devolucions'
)
GROUP BY lf.factura_id
) AS despeses_devolucions ON (despeses_devolucions.factura_id=f.id)
LEFT JOIN (
  SELECT
    lf.factura_id as factura_id, SUM(li.price_subtotal) AS import
  FROM
    giscedata_facturacio_factura_linia lf
  LEFT JOIN
    account_invoice_line li ON (li.id=lf.invoice_line_id)
  WHERE
    lf.tipus='altres'
  AND li.product_id = (
      SELECT res_id
      FROM ir_model_data
      WHERE name = 'bosocial_BS01'
      AND module='giscedata_repercusio_bo_social'
  )
  GROUP BY lf.factura_id
) AS repercussio_bo_social ON (repercussio_bo_social.factura_id=f.id)
LEFT JOIN (
SELECT lf.factura_id as factura_id, SUM(li.price_subtotal) AS import
FROM giscedata_facturacio_factura_linia lf
LEFT JOIN account_invoice_line li ON (li.id=lf.invoice_line_id)
WHERE lf.tipus='altres'
AND li.product_id IN (
    SELECT id
    FROM product_product
    WHERE default_code
    LIKE '%%LUCERA%%'
)
GROUP BY lf.factura_id
) AS cuota ON (cuota.factura_id=f.id)
LEFT JOIN (
SELECT lf.factura_id AS factura_id
-- , STRING_AGG(li.name,',') AS linies
, SUM(li.price_subtotal) AS base
, ROUND(SUM(li.price_subtotal) * 1.051130 * 0.04864,2) AS amount
FROM giscedata_facturacio_factura_linia lf
LEFT JOIN account_invoice_line li ON (li.id=lf.invoice_line_id)
LEFT JOIN account_invoice_line_tax it_18 ON (li.id=it_18.invoice_line_id)
LEFT JOIN account_tax t_18 ON (it_18.tax_id=t_18.id AND t_18.name LIKE 'IVA 18%%')
LEFT JOIN account_invoice_line_tax iese ON (li.id=iese.invoice_line_id )
LEFT JOIN account_tax t_iese ON (iese.tax_id=t_iese.id AND t_iese.name LIKE '%%electricidad%%')
WHERE t_iese.id IS NOT NULL and t_18.id IS NOT NULL
GROUP BY lf.factura_id
) AS iese_18 ON (iese_18.factura_id=f.id)
LEFT JOIN (
  SELECT fl.factura_id AS factura_id, SUM(il.price_subtotal) AS base_costs, 0 AS iva_costs
  FROM account_invoice_line il
  LEFT JOIN account_invoice_line_tax lt ON (lt.invoice_line_id = il.id)
  LEFT JOIN giscedata_facturacio_factura_linia fl ON (fl.invoice_line_id = il.id)
  WHERE lt.invoice_line_id IS NULL
    AND fl.tipus = 'altres'
  GROUP BY fl.factura_id
) no_iva_concepts ON (no_iva_concepts.factura_id = f.id)
LEFT JOIN (
  SELECT li.invoice_id AS invoice_id, SUM(li.price_subtotal) AS garantia
  FROM account_invoice_line li
  JOIN product_product p ON (li.product_id = p.id)
  WHERE p.default_code = 'CON06'
  GROUP BY li.invoice_id
) AS garantia ON (garantia.invoice_id = i.id)
LEFT JOIN account_invoice_tax it_18 ON (it_18.invoice_id=i.id AND (it_18.name LIKE 'IVA 18%%' OR it_18.name LIKE '18%%%% IVA%%'))
LEFT JOIN account_invoice_tax it_21 ON (it_21.invoice_id=i.id AND (it_21.name LIKE 'IVA 21%%' OR it_21.name LIKE '21%%%% IVA%%'))
LEFT JOIN account_invoice_tax it_iese ON (it_iese.invoice_id=i.id AND it_iese.name LIKE '%%electricidad%%')
LEFT JOIN res_partner d ON (d.id=c.distribuidora_id)
LEFT JOIN res_partner p ON (p.id=i.company_id)
LEFT JOIN res_partner tit on (tit.id = i.partner_id)
LEFT JOIN payment_type pt on (i.payment_type = pt.id)
WHERE
f.id in (%s)
ORDER BY p.ref,CUPS,factura
