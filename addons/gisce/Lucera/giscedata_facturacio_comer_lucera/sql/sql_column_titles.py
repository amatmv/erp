# -*- coding: utf-8 -*-
from tools.translate import _
from giscedata_facturacio_comer.sql.sql_column_titles import add_fields_query_resum

inheritations = {
    _(u"vat"): [
        u"Costes de devolución", u"Repercusión bono social", u"Peajes Energía",
        u"Peajes Potencia", u"Peajes Alquileres", u"Peajes Otros",
        u"Cuota Lucera"
    ],
    _(u"CUPS"): [u"Provincia CUPS", u"INE Municipio"],
    _(u"descuentos_energia"): [
        u"P1 (kWh)", u"P1 (€)",
        u"P2 (kWh)", u"P2 (€)",
        u"P3 (kWh)", u"P3 (€)",
        u"P4 (kWh)", u"P4 (€)",
        u"P5 (kWh)", u"P5 (€)",
        u"P6 (kWh)", u"P6 (€)"
    ]
}

for inherit_field, fields_to_add in inheritations.items():
    add_fields_query_resum(inherit_field, fields_to_add)
