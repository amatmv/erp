# -*- coding: utf-8 -*-
import unittest
from datetime import datetime
from dateutil.relativedelta import relativedelta
from destral import testing
from destral.transaction import Transaction
from expects import *
from osv.orm import except_orm


class TestGiscedataFacturacioFacturador(unittest.TestCase):

    # Quota

    def test_adds_full_quota_when_creating_a_full_month_invoice(self):
        pass

    def test_adds_partial_quota_when_creating_a_partial_month_invoice(self):
        pass

    def test_does_not_add_quota_if_it_is_not_an_energy_invoice(self):
        pass

    def test_adds_proportional_quota_when_creating_more_than_one_month_invoice(self):
        pass

    # Discounts

    def test_adds_quota_and_equivalent_discount_if_polissa_has_available_discounts(self):
        pass

    def test_adds_quota_and_equivalent_partial_discount_if_polissa_has_partial_available_discount(self):

        # Arrange

        # - Create discount for polissa
        # - Create measures

        # Act

        # - Create invoice

        # Check

        # - Invoice is created
        # - Invoice has quota
        # - Invoice has partial discount
        # - Discount is consumed and remaining amount is 0

        pass

    def test_adds_quota_and_equivalent_discounts_from_two_different_discounts_when_first_is_exhausted(self):
        pass

    def test_returns_discounted_amounts_if_invoice_is_deleted(self):
        pass

    def test_returns_discounted_amounts_to_multiple_discounts_if_invoice_is_deleted(self):
        pass

    def test_returns_discounted_amounts_if_invoice_is_refunded(self):
        pass

    def test_returns_discounted_amounts_to_multiple_discounts_if_invoice_is_refunded(self):
        pass

if __name__ == '__main__':
    unittest.main()
