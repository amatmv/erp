# -*- coding: utf-8 -*-
import unittest
import base64

from destral import testing
from destral.transaction import Transaction
from expects import *
from osv.osv import except_osv
from addons import get_module_resource
from gestionatr.input.messages import F1
from datetime import datetime, timedelta
from giscedata_polissa.tests.utils import crear_modcon


class TestImportF1Lucera(testing.OOTestCase):
    def setUp(self):
        module_obj = self.openerp.pool.get('ir.module.module')

        newest_name = None

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            newest_id = module_obj.search(
                cursor, uid, [
                    ('name', 'like', 'giscedata_tarifas_peajes_20160101'),
                    ('state', '!=', 'installed')
                ], limit=1, order='name DESC'
            )
            if newest_id:
                newest_name = module_obj.read(
                    cursor, uid, newest_id[0], ['name']
                )['name']

        if newest_name:
            self.openerp.install_module(newest_name)

        return super(TestImportF1Lucera, self).setUp()

    def activar_polissa(self, txn, polissa_ref):
        """Activa una polissa i obté el seu id."""
        cursor = txn.cursor
        uid = txn.user

        imd_obj = self.openerp.pool.get('ir.model.data')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')

        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', polissa_ref
        )[1]

        polissa_obj.send_signal(cursor, uid, [polissa_id], [
            'validar', 'contracte'
        ])

        return polissa_id

    def activar_polissa_CUPS(self, txn):
        cursor = txn.cursor
        uid = txn.user
        imd_obj = self.openerp.pool.get('ir.model.data')

        polissa_obj = self.openerp.pool.get('giscedata.polissa')

        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]

        polissa_obj.send_signal(cursor, uid, [polissa_id], [
            'validar', 'contracte'
        ])
        polissa = polissa_obj.browse(cursor, uid, polissa_id)
        polissa.modcontractual_activa.write({'data_final': '3000-01-01'})
        user_obj = self.openerp.pool.get('res.users')
        user_browse = user_obj.browse(cursor, uid, uid)
        user_browse.company_id.partner_id.write({'ref': '4321'})

    def clean_hash_lines(self, txn):
        cursor = txn.cursor
        uid = txn.user
        line_obj = self.openerp.pool.get('giscedata.facturacio.importacio.linia')
        lids = line_obj.search(cursor, uid, [])
        line_obj.write(cursor, uid, lids, {"md5_hash": "123456789"})

    def crear_modcon(self, txn, potencia, ini, fi, tariff_id=None, extra_fields=None):
        cursor = txn.cursor
        uid = txn.user
        pool = self.openerp.pool
        polissa_obj = pool.get('giscedata.polissa')
        imd_obj = pool.get('ir.model.data')
        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]
        pol = polissa_obj.browse(cursor, uid, polissa_id)
        pol.send_signal(['modcontractual'])
        polissa_obj.write(cursor, uid, polissa_id, {'potencia': potencia})
        if extra_fields and isinstance(extra_fields, dict):
            polissa_obj.write(cursor, uid, polissa_id, extra_fields)

        if tariff_id:
            polissa_obj.write(cursor, uid, polissa_id, {'tarifa': tariff_id})

        wz_crear_mc_obj = pool.get('giscedata.polissa.crear.contracte')

        ctx = {'active_id': polissa_id}
        params = {'duracio': 'nou'}

        wz_id_mod = wz_crear_mc_obj.create(cursor, uid, params, ctx)
        wiz_mod = wz_crear_mc_obj.browse(cursor, uid, wz_id_mod, ctx)
        res = wz_crear_mc_obj.onchange_duracio(
            cursor, uid, [wz_id_mod], wiz_mod.data_inici, wiz_mod.duracio,
            ctx
        )
        wiz_mod.write({
            'data_inici': ini,
            'data_final': fi
        })
        wiz_mod.action_crear_contracte(ctx)

    def add_supported_iva_to_periods(self, txn):
        tarifa_obj = self.openerp.pool.get('giscedata.polissa.tarifa')
        periode_obj = self.openerp.pool.get('giscedata.polissa.tarifa.periodes')
        tax_obj = self.openerp.pool.get('account.tax')
        product_obj = self.openerp.pool.get('product.product')

        cursor = txn.cursor
        uid = txn.user

        iva_id = tax_obj.search(
            cursor, uid, [
                ('name', '=', '21% IVA Soportado (operaciones corrientes)')
            ]
        )

        product_ids = []
        for tarifa_id in tarifa_obj.search(cursor, uid, []):
            periode_ids = tarifa_obj.read(
                cursor, uid, tarifa_id, ['periodes']
            )['periodes']
            for periode_vals in periode_obj.read(cursor, uid, periode_ids):
                product_ids.append(periode_vals['product_id'][0])
                if periode_vals['product_reactiva_id']:
                    product_ids.append(periode_vals['product_reactiva_id'][0])
                if periode_vals['product_exces_pot_id']:
                    product_ids.append(periode_vals['product_exces_pot_id'][0])

        product_obj.write(
            cursor, uid, product_ids, {
                'supplier_taxes_id': [(6, 0, iva_id)]
            }
        )

    def add_supported_iva_to_concepts(self, txn, iva='21'):
        tax_obj = self.openerp.pool.get('account.tax')
        product_obj = self.openerp.pool.get('product.product')

        cursor = txn.cursor
        uid = txn.user

        name = '{}% IVA Soportado (operaciones corrientes)'.format(iva)
        iva_id = tax_obj.search(
            cursor, uid, [
                ('name', '=', name)
            ]
        )

        no_iva_concepts = ['CON06', 'CON40', 'CON41', 'CON42']
        product_ids = product_obj.search(
            cursor, uid, [
                '|',
                ('default_code', 'like', 'CON'),
                ('default_code', 'like', 'ALQ'),
                ('default_code', 'not in', no_iva_concepts)
            ]
        )
        product_obj.write(
            cursor, uid, product_ids, {
                'supplier_taxes_id': [(6, 0, iva_id)]
            }
        )

    def add_sell_taxes_to_suplemento_territorial(self, txn):
        tax_obj = self.openerp.pool.get('account.tax')
        product_obj = self.openerp.pool.get('product.product')

        cursor = txn.cursor
        uid = txn.user

        tax_ids = tax_obj.search(
            cursor, uid, [
                '|',
                ('name', '=', 'IVA 21%'),
                ('name', '=', 'Impuesto especial sobre la electricidad')
            ]
        )

        product_ids = product_obj.search(
            cursor, uid, [
                ('default_code', 'in', ('SETU35', 'TEC271'))
            ]
        )
        product_obj.write(
            cursor, uid, product_ids, {
                'taxes_id': [(6, 0, tax_ids)]
            }
        )

    def prepare_for_phase_3(self, txn, context=None):
        if not context:
            context = {}
        polissa_obj = self.openerp.pool.get(
            'giscedata.polissa'
        )
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        pricelist_obj = self.openerp.pool.get('product.pricelist')
        version_obj = self.openerp.pool.get('product.pricelist.version')
        cups_obj = self.openerp.pool.get('giscedata.cups.ps')
        partner_obj = self.openerp.pool.get('res.partner')
        address_obj = self.openerp.pool.get('res.partner.address')
        imd_obj = self.openerp.pool.get('ir.model.data')
        pot_obj = self.openerp.pool.get(
            "giscedata.polissa.potencia.contractada.periode")

        xml_path = get_module_resource(
            'giscedata_facturacio_switching', 'tests', 'fixtures',
            context.get("test_xml", 'test_correct.xml')
        )

        with open(xml_path, 'r') as f:
            self.xml_file = f.read()
        self.xml_file = self.xml_file.replace(
            '<CodigoREEEmpresaEmisora>1234</CodigoREEEmpresaEmisora>',
            '<CodigoREEEmpresaEmisora>0031</CodigoREEEmpresaEmisora>'
        )

        conceptes_xml_path = get_module_resource(
            'giscedata_facturacio_switching', 'tests', 'fixtures',
            'F1_conceptes.xml'
        )

        with open(conceptes_xml_path, 'r') as f:
            self.conceptes_xml_file = f.read()

        cursor = txn.cursor
        uid = txn.user

        cups_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_cups', 'cups_01'
        )[1]
        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]
        import_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
        )[1]
        distri_id = partner_obj.search(
            cursor, uid, [('ref', '=', '0031')]
        )[0]
        pricelist_id = pricelist_obj.search(
            cursor, uid, [('name', '=', 'TARIFAS ELECTRICIDAD')]
        )[0]
        versions = version_obj.search(cursor, uid, [('pricelist_id', '=', pricelist_id)], order="date_end desc",
                                      limit=1)
        version_obj.write(cursor, uid, versions[-1], {'date_end': None})
        self.line_id = line_obj.create_from_xml(
            cursor, uid, import_id, 'Import Name', self.xml_file
        )
        line_obj.write(cursor, uid, self.line_id, {'cups_id': cups_id})
        pvals = {
            'cups': cups_id,
            'state': 'activa',
            'distribuidora': distri_id,
            'facturacio_potencia': 'icp',
            'potencia': 5.75,
        }
        if context.get("tarifa"):
            pvals['tarifa'] = context.get("tarifa")

        polissa_obj.write(
            cursor, uid, polissa_id, pvals
        )
        pol_pots = polissa_obj.read(cursor, uid, polissa_id, ['potencies_periode'])['potencies_periode']
        pot_obj.write(cursor, uid, pol_pots, {'potencia': 5.750})
        cups_obj.write(cursor, uid, cups_id, {'distribuidora_id': distri_id})
        address_obj.create(cursor, uid, {'partner_id': distri_id})
        partner_obj.write(
            cursor, uid, distri_id, {
                'property_product_pricelist_purchase': pricelist_id
            }
        )

        self.activar_polissa_CUPS(txn)

        self.add_supported_iva_to_periods(txn)
        self.add_supported_iva_to_concepts(txn)
        self.add_sell_taxes_to_suplemento_territorial(txn)
        self.clean_hash_lines(txn)

    def check_linies_factura(self, factura, linies_vals):
        for linia_vals in linies_vals:
            found = True
            for l in factura.linia_ids:
                found = True
                for camp, valor in linia_vals.items():
                    if eval("l.{0}".format(camp)) != valor:
                        found = False
                        break
                if found:
                    break
            self.assertTrue(found, msg="No s'ha trobat la linia de la factura amb dades {0}".format(linia_vals))
        return True

    def check_lectures_factura(self, factura, lectures_vals):
        for linia_vals in lectures_vals:
            found = True
            for l in factura.lectures_energia_ids:
                found = True
                for camp, valor in linia_vals.items():
                    if eval("l.{0}".format(camp)) != valor:
                        found = False
                        break
                if found:
                    break
            self.assertTrue(found, msg="No s'ha trobat la lectura de la factura amb dades {0}".format(linia_vals))
        return True

    def check_lectures_pool_polissa(self, polissa, lectures_vals):
        for linia_vals in lectures_vals:
            lectures_pool = []
            for c in polissa.comptadors:
                lectures_pool.extend(c.pool_lectures)
            found = True
            for l in lectures_pool:
                found = True
                for camp, valor in linia_vals.items():
                    if eval("l.{0}".format(camp)) != valor:
                        found = False
                        break
                if found:
                    break

            self.assertTrue(found, msg="No s'ha trobat la lectura pool amb dades {0}".format(linia_vals))
        return True

    def test_nou_autoconsum_31_quadra_energia_consums(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            context = {"test_xml": 'test_nou_autoconsum_31_quadra_energia_consums.xml'}
            self.prepare_for_phase_3(txn, context=context)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )
            line_obj.process_line_sync(cursor, uid, line_id)

            polissa_id = imd_obj.get_object_reference(cursor, uid, 'giscedata_polissa', 'polissa_0001')[1]
            f1 = line_obj.browse(cursor, uid, line_id)
            polissa = polissa_obj.browse(cursor, uid, polissa_id)
            factura = f1.liniafactura_id[0].factura_id

            # Comprovem que el F1 es correcte
            expect(f1.state).to(equal('valid'))
            expect(f1.lectures_processades).to(equal(True))
            expect(f1.import_phase).to(equal(40))
            # Comprovem que la factura es correcte
            self.assertEqual(factura.amount_untaxed, 27.94)
            self.assertEqual(factura.check_total, 33.81)
            self.check_linies_factura(factura, [
                {'product_id.code': 'ACTIPO', 'price_subtotal': 0.0, 'quantity': 31.0},
            ])
            self.check_lectures_factura(factura, [
                {'magnitud': 'AE', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-31",
                 'lect_anterior': 4000.0, 'lect_actual': 4190.0, 'ajust': 10.0, 'motiu_ajust': '99',
                 'comptador': "141244861", "name": "2.0A (P1)"},
            ])
            self.check_lectures_pool_polissa(polissa, [
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0A',
                 'lectura': 4000, 'ajust': 0, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': False,
                 'comptador.name': "141244861", 'lectura_exporta_informada': False},
                {'tipus': 'A', 'name': "2019-08-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0A',
                 'lectura': 4190, 'ajust': 10, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': '99',
                 'comptador.name': "141244861", 'lectura_exporta_informada': False},
            ])
            # Ara fem un write a la lectura amb un valor != 0. Al fer-ho hauria de posar a true el lectura_exporta_informada
            lec_o = self.openerp.pool.get("giscedata.lectures.lectura.pool")
            lids = lec_o.search(cursor, uid, [])
            lec_o.write(cursor, uid, lids, {'lectura_exporta': 0})
            polissa = polissa.browse()[0]
            self.check_lectures_pool_polissa(polissa, [
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0A',
                 'lectura': 4000, 'ajust': 0, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': False,
                 'comptador.name': "141244861", 'lectura_exporta_informada': False},
            ])
            lec_o.write(cursor, uid, lids, {'lectura_exporta': 1})
            polissa = polissa.browse()[0]
            self.check_lectures_pool_polissa(polissa, [
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0A',
                 'lectura': 4000, 'ajust': 0, 'lectura_exporta': 1, 'ajust_exporta': 0, 'motiu_ajust': False,
                 'comptador.name': "141244861", 'lectura_exporta_informada': True},
            ])

    def test_nou_autoconsum_31_no_quadra_energia_consums(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            context = {"test_xml": 'test_nou_autoconsum_31_no_quadra_energia_consums.xml'}
            self.prepare_for_phase_3(txn, context=context)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )
            line_obj.process_line_sync(cursor, uid, line_id)

            polissa_id = imd_obj.get_object_reference(cursor, uid, 'giscedata_polissa', 'polissa_0001')[1]
            f1 = line_obj.browse(cursor, uid, line_id)
            polissa = polissa_obj.browse(cursor, uid, polissa_id)
            factura = f1.liniafactura_id[0].factura_id

            # Comprovem que el F1 es correcte
            expect(f1.state).to(equal('valid'))
            expect(f1.lectures_processades).to(equal(True))
            expect(f1.import_phase).to(equal(40))
            # Comprovem que la factura es correcte
            self.assertEqual(factura.amount_untaxed, 27.94)
            self.assertEqual(factura.check_total, 33.81)
            self.check_linies_factura(factura, [
                {'product_id.code': 'ACTIPO', 'price_subtotal': 0.0, 'quantity': 31.0},
            ])
            self.check_lectures_factura(factura, [
                {'magnitud': 'AE', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-31",
                 'lect_anterior': 4000.0, 'lect_actual': 5000.0, 'ajust': 0.0, 'motiu_ajust': False,
                 'comptador': "141244861", "name": "2.0A (P1)"},
            ])
            self.check_lectures_pool_polissa(polissa, [
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0A',
                 'lectura': 4000, 'ajust': 0, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': False,
                 'comptador.name': "141244861", 'lectura_exporta_informada': False},
                {'tipus': 'A', 'name': "2019-08-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0A',
                 'lectura': 5000, 'ajust': -800, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': '98',
                 'comptador.name': "141244861", 'lectura_exporta_informada': False},
            ])

    def test_nou_autoconsum_41_20A(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            context = {"test_xml": 'test_nou_autoconsum_41_20A.xml'}
            self.prepare_for_phase_3(txn, context=context)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )
            line_obj.process_line_sync(cursor, uid, line_id)

            polissa_id = imd_obj.get_object_reference(cursor, uid, 'giscedata_polissa', 'polissa_0001')[1]
            f1 = line_obj.browse(cursor, uid, line_id)
            polissa = polissa_obj.browse(cursor, uid, polissa_id)
            factura = f1.liniafactura_id[0].factura_id

            # Comprovem que el F1 es correcte
            expect(f1.state).to(equal('valid'))
            expect(f1.lectures_processades).to(equal(True))
            expect(f1.import_phase).to(equal(40))
            # Comprovem que la factura es correcte
            self.assertEqual(factura.amount_untaxed, 27.94)
            self.assertEqual(factura.check_total, 33.81)
            self.check_linies_factura(factura, [
                {'product_id.code': 'ACACP1', 'price_subtotal': 0.0, 'quantity': 10.0},
                {'product_id.code': 'ACGNP1', 'price_subtotal': 0.0, 'quantity': 15.0},
                {'product_id.code': 'ACEXP1', 'price_subtotal': 0.0, 'quantity': 5.0},
                {'product_id.code': 'ACTIPO', 'price_subtotal': 0.0, 'quantity': 41.0},
                {'product_id.code': 'ACCR', 'price_subtotal': 0.0, 'quantity': 1.0}
            ])
            self.check_lectures_factura(factura, [
                {'magnitud': 'AE', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-31",
                 'lect_anterior': 4000.0, 'lect_actual': 4240.0, 'ajust': 10.0, 'motiu_ajust': '99',
                 'comptador': "141244861", "name": "2.0A (P1)"},
                {'magnitud': 'AS', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-31",
                 'lect_anterior': 0.0, 'lect_actual': 20.0, 'ajust': 0.0, 'motiu_ajust': False,
                 'comptador': "155385244", "name": "2.0A (P1)"},
            ])
            self.check_lectures_pool_polissa(polissa, [
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0A',
                 'lectura': 4000, 'ajust': 0, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': False,
                 'comptador.name': "141244861", 'lectura_exporta_informada': False},
                {'tipus': 'A', 'name': "2019-08-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0A',
                 'lectura': 4240, 'ajust': -40, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': '98',
                 'comptador.name': "141244861", 'lectura_exporta_informada': False},
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0A', 'lectura': 0,
                 'ajust': 0, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': False,
                 'comptador.name': "155385244", 'lectura_exporta_informada': True},
                {'tipus': 'A', 'name': "2019-08-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0A', 'lectura': 0,
                 'ajust': 0, 'lectura_exporta': 20, 'ajust_exporta': -15, 'motiu_ajust': '98',
                 'comptador.name': "155385244", 'lectura_exporta_informada': True},
            ])

    def test_nou_autoconsum_41_20A_un_sol_comptador(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            context = {"test_xml": 'test_nou_autoconsum_41_20A_un_sol_comptador.xml'}
            self.prepare_for_phase_3(txn, context=context)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )
            line_obj.process_line_sync(cursor, uid, line_id)

            polissa_id = imd_obj.get_object_reference(cursor, uid, 'giscedata_polissa', 'polissa_0001')[1]
            f1 = line_obj.browse(cursor, uid, line_id)
            polissa = polissa_obj.browse(cursor, uid, polissa_id)
            factura = f1.liniafactura_id[0].factura_id

            # Comprovem que el F1 es correcte
            expect(f1.state).to(equal('valid'))
            expect(f1.lectures_processades).to(equal(True))
            expect(f1.import_phase).to(equal(40))
            # Comprovem que la factura es correcte
            self.assertEqual(factura.amount_untaxed, 27.94)
            self.assertEqual(factura.check_total, 33.81)
            self.check_linies_factura(factura, [
                {'product_id.code': 'ACACP1', 'price_subtotal': 0.0, 'quantity': 10.0},
                {'product_id.code': 'ACGNP1', 'price_subtotal': 0.0, 'quantity': 15.0},
                {'product_id.code': 'ACEXP1', 'price_subtotal': 0.0, 'quantity': 5.0},
                {'product_id.code': 'ACTIPO', 'price_subtotal': 0.0, 'quantity': 41.0},
                {'product_id.code': 'ACCR', 'price_subtotal': 0.0, 'quantity': 1.0}
            ])
            self.check_lectures_factura(factura, [
                {'magnitud': 'AE', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-31",
                 'lect_anterior': 4000.0, 'lect_actual': 4240.0, 'ajust': 10.0, 'motiu_ajust': '99',
                 'comptador': "141244861", "name": "2.0A (P1)"},
                {'magnitud': 'AS', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-31",
                 'lect_anterior': 0.0, 'lect_actual': 20.0, 'ajust': 0.0, 'motiu_ajust': False,
                 'comptador': "141244861", "name": "2.0A (P1)"},
            ])
            self.check_lectures_pool_polissa(polissa, [
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0A',
                 'lectura': 4000, 'ajust': 0, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': False,
                 'comptador.name': "141244861", 'lectura_exporta_informada': True},
                {'tipus': 'A', 'name': "2019-08-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0A',
                 'lectura': 4240, 'ajust': -40, 'lectura_exporta': 20, 'ajust_exporta': -15, 'motiu_ajust': '98',
                 'comptador.name': "141244861", 'lectura_exporta_informada': True},
            ])

    def test_nou_autoconsum_41_20DHA(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            tarif_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'tarifa_20DHA_new'
            )[1]
            context = {"test_xml": 'test_nou_autoconsum_41_20DHA.xml', 'tarifa': tarif_id}
            self.prepare_for_phase_3(txn, context=context)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )
            line_obj.process_line_sync(cursor, uid, line_id)

            polissa_id = imd_obj.get_object_reference(cursor, uid, 'giscedata_polissa', 'polissa_0001')[1]
            f1 = line_obj.browse(cursor, uid, line_id)
            polissa = polissa_obj.browse(cursor, uid, polissa_id)
            factura = f1.liniafactura_id[0].factura_id

            # Comprovem que el F1 es correcte
            expect(f1.state).to(equal('valid'))
            expect(f1.lectures_processades).to(equal(True))
            expect(f1.import_phase).to(equal(40))
            # Comprovem que la factura es correcte
            self.assertEqual(factura.amount_untaxed, 27.94)
            self.assertEqual(factura.check_total, 33.81)
            self.check_linies_factura(factura, [
                {'product_id.code': 'ACACP1', 'price_subtotal': 0.0, 'quantity': 5.0},
                {'product_id.code': 'ACACP2', 'price_subtotal': 0.0, 'quantity': 2.0},
                {'product_id.code': 'ACGNP1', 'price_subtotal': 0.0, 'quantity': 20.0},
                {'product_id.code': 'ACGNP2', 'price_subtotal': 0.0, 'quantity': 10.0},
                {'product_id.code': 'ACEXP1', 'price_subtotal': 0.0, 'quantity': 15.0},
                {'product_id.code': 'ACEXP2', 'price_subtotal': 0.0, 'quantity': 8.0},
                {'product_id.code': 'ACTIPO', 'price_subtotal': 0.0, 'quantity': 41.0},
                {'product_id.code': 'ACCR', 'price_subtotal': 0.0, 'quantity': 1.0}
            ])
            self.check_lectures_factura(factura, [
                {'magnitud': 'AE', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-31",
                 'lect_anterior': 4000.0, 'lect_actual': 4240.0, 'ajust': 0.0, 'motiu_ajust': False,
                 'comptador': "141244861", "name": "2.0DHA (P1)"},
                {'magnitud': 'AE', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-31",
                 'lect_anterior': 100.0, 'lect_actual': 500.0, 'ajust': 0.0, 'motiu_ajust': False,
                 'comptador': "141244861", "name": "2.0DHA (P2)"},
                {'magnitud': 'AS', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-31",
                 'lect_anterior': 100.0, 'lect_actual': 120.0, 'ajust': 0.0, 'motiu_ajust': False,
                 'comptador': "155385244", "name": "2.0DHA (P1)"},
                {'magnitud': 'AS', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-31",
                 'lect_anterior': 10.0, 'lect_actual': 20.0, 'ajust': 0.0, 'motiu_ajust': False,
                 'comptador': "155385244", "name": "2.0DHA (P2)"},
            ])
            self.check_lectures_pool_polissa(polissa, [
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0DHA',
                 'lectura': 4000, 'ajust': 0, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': False,
                 'comptador.name': "141244861", 'lectura_exporta_informada': False},
                {'tipus': 'A', 'name': "2019-08-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0DHA',
                 'lectura': 4240, 'ajust': -190, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': '98',
                 'comptador.name': "141244861", 'lectura_exporta_informada': False},
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P2', 'periode.tarifa.name': '2.0DHA',
                 'lectura': 100, 'ajust': 0, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': False,
                 'comptador.name': "141244861", 'lectura_exporta_informada': False},
                {'tipus': 'A', 'name': "2019-08-31", 'periode.name': 'P2', 'periode.tarifa.name': '2.0DHA',
                 'lectura': 500, 'ajust': -250, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': '98',
                 'comptador.name': "141244861", 'lectura_exporta_informada': False},
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0DHA',
                 'lectura': 0, 'ajust': 0, 'lectura_exporta': 100, 'ajust_exporta': 0, 'motiu_ajust': False,
                 'comptador.name': "155385244", 'lectura_exporta_informada': True},
                {'tipus': 'A', 'name': "2019-08-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0DHA',
                 'lectura': 0, 'ajust': 0, 'lectura_exporta': 120, 'ajust_exporta': -5, 'motiu_ajust': '98',
                 'comptador.name': "155385244", 'lectura_exporta_informada': True},
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P2', 'periode.tarifa.name': '2.0DHA',
                 'lectura': 0, 'ajust': 0, 'lectura_exporta': 10, 'ajust_exporta': 0, 'motiu_ajust': False,
                 'comptador.name': "155385244", 'lectura_exporta_informada': True},
                {'tipus': 'A', 'name': "2019-08-31", 'periode.name': 'P2', 'periode.tarifa.name': '2.0DHA',
                 'lectura': 0, 'ajust': 0, 'lectura_exporta': 20, 'ajust_exporta': -2, 'motiu_ajust': '98',
                 'comptador.name': "155385244", 'lectura_exporta_informada': True},
            ])

    def test_nou_autoconsum_41_20DHA_un_sol_comptador(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            tarif_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'tarifa_20DHA_new'
            )[1]
            context = {"test_xml": 'test_nou_autoconsum_41_20DHA_un_sol_comptador.xml', 'tarifa': tarif_id}
            self.prepare_for_phase_3(txn, context=context)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )
            line_obj.process_line_sync(cursor, uid, line_id)

            polissa_id = imd_obj.get_object_reference(cursor, uid, 'giscedata_polissa', 'polissa_0001')[1]
            f1 = line_obj.browse(cursor, uid, line_id)
            polissa = polissa_obj.browse(cursor, uid, polissa_id)
            factura = f1.liniafactura_id[0].factura_id

            # Comprovem que el F1 es correcte
            expect(f1.state).to(equal('valid'))
            expect(f1.lectures_processades).to(equal(True))
            expect(f1.import_phase).to(equal(40))
            # Comprovem que la factura es correcte
            self.assertEqual(factura.amount_untaxed, 27.94)
            self.assertEqual(factura.check_total, 33.81)
            self.check_linies_factura(factura, [
                {'product_id.code': 'ACACP1', 'price_subtotal': 0.0, 'quantity': 5.0},
                {'product_id.code': 'ACACP2', 'price_subtotal': 0.0, 'quantity': 2.0},
                {'product_id.code': 'ACGNP1', 'price_subtotal': 0.0, 'quantity': 20.0},
                {'product_id.code': 'ACGNP2', 'price_subtotal': 0.0, 'quantity': 10.0},
                {'product_id.code': 'ACEXP1', 'price_subtotal': 0.0, 'quantity': 15.0},
                {'product_id.code': 'ACEXP2', 'price_subtotal': 0.0, 'quantity': 8.0},
                {'product_id.code': 'ACTIPO', 'price_subtotal': 0.0, 'quantity': 41.0},
                {'product_id.code': 'ACCR', 'price_subtotal': 0.0, 'quantity': 1.0}
            ])
            self.check_lectures_factura(factura, [
                {'magnitud': 'AE', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-31",
                 'lect_anterior': 4000.0, 'lect_actual': 4240.0, 'ajust': 0.0, 'motiu_ajust': False,
                 'comptador': "141244861", "name": "2.0DHA (P1)"},
                {'magnitud': 'AE', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-31",
                 'lect_anterior': 100.0, 'lect_actual': 500.0, 'ajust': 0.0, 'motiu_ajust': False,
                 'comptador': "141244861", "name": "2.0DHA (P2)"},
                {'magnitud': 'AS', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-31",
                 'lect_anterior': 100.0, 'lect_actual': 120.0, 'ajust': 0.0, 'motiu_ajust': False,
                 'comptador': "141244861", "name": "2.0DHA (P1)"},
                {'magnitud': 'AS', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-31",
                 'lect_anterior': 10.0, 'lect_actual': 20.0, 'ajust': 0.0, 'motiu_ajust': False,
                 'comptador': "141244861", "name": "2.0DHA (P2)"},
            ])
            self.check_lectures_pool_polissa(polissa, [
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0DHA',
                 'lectura': 4000, 'ajust': 0, 'lectura_exporta': 100, 'ajust_exporta': 0, 'motiu_ajust': False,
                 'comptador.name': "141244861", 'lectura_exporta_informada': True},
                {'tipus': 'A', 'name': "2019-08-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0DHA',
                 'lectura': 4240, 'ajust': -190, 'lectura_exporta': 120, 'ajust_exporta': -5, 'motiu_ajust': '98',
                 'comptador.name': "141244861", 'lectura_exporta_informada': True},
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P2', 'periode.tarifa.name': '2.0DHA',
                 'lectura': 100, 'ajust': 0, 'lectura_exporta': 10, 'ajust_exporta': 0, 'motiu_ajust': False,
                 'comptador.name': "141244861", 'lectura_exporta_informada': True},
                {'tipus': 'A', 'name': "2019-08-31", 'periode.name': 'P2', 'periode.tarifa.name': '2.0DHA',
                 'lectura': 500, 'ajust': -250, 'lectura_exporta': 20, 'ajust_exporta': -2, 'motiu_ajust': '98',
                 'comptador.name': "141244861", 'lectura_exporta_informada': True},
            ])

    def test_nou_autoconsum_41_20DHA_canvi_comptador(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            tarif_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'tarifa_20DHA_new'
            )[1]
            context = {"test_xml": 'test_nou_autoconsum_41_20DHA_canvi_comptador.xml', 'tarifa': tarif_id}
            self.prepare_for_phase_3(txn, context=context)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )
            line_obj.process_line_sync(cursor, uid, line_id)

            polissa_id = imd_obj.get_object_reference(cursor, uid, 'giscedata_polissa', 'polissa_0001')[1]
            f1 = line_obj.browse(cursor, uid, line_id)
            polissa = polissa_obj.browse(cursor, uid, polissa_id)
            factura = f1.liniafactura_id[0].factura_id

            # Comprovem que el F1 es correcte
            expect(f1.state).to(equal('valid'))
            expect(f1.lectures_processades).to(equal(True))
            expect(f1.import_phase).to(equal(40))
            # Comprovem que la factura es correcte
            self.assertEqual(factura.amount_untaxed, 27.94)
            self.assertEqual(factura.check_total, 33.81)
            self.check_linies_factura(factura, [
                {'product_id.code': 'ACACP1', 'price_subtotal': 0.0, 'quantity': 5.0},
                {'product_id.code': 'ACACP2', 'price_subtotal': 0.0, 'quantity': 2.0},
                {'product_id.code': 'ACGNP1', 'price_subtotal': 0.0, 'quantity': 20.0},
                {'product_id.code': 'ACGNP2', 'price_subtotal': 0.0, 'quantity': 10.0},
                {'product_id.code': 'ACEXP1', 'price_subtotal': 0.0, 'quantity': 15.0},
                {'product_id.code': 'ACEXP2', 'price_subtotal': 0.0, 'quantity': 8.0},
                {'product_id.code': 'ACTIPO', 'price_subtotal': 0.0, 'quantity': 41.0},
                {'product_id.code': 'ACCR', 'price_subtotal': 0.0, 'quantity': 1.0}
            ])
            self.check_lectures_factura(factura, [
                {'magnitud': 'AE', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-20",
                 'lect_anterior': 4000.0, 'lect_actual': 4200.0, 'ajust': 0.0, 'motiu_ajust': False,
                 'comptador': "141244861", "name": "2.0DHA (P1)"},
                {'magnitud': 'AE', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-20",
                 'lect_anterior': 100.0, 'lect_actual': 400.0, 'ajust': 0.0, 'motiu_ajust': False,
                 'comptador': "141244861", "name": "2.0DHA (P2)"},
                {'magnitud': 'AE', 'tipus': 'activa', 'data_anterior': "2019-08-20", 'data_actual': "2019-08-31",
                 'lect_anterior': 4200.0, 'lect_actual': 4240.0, 'ajust': 0.0, 'motiu_ajust': False,
                 'comptador': "241244861", "name": "2.0DHA (P1)"},
                {'magnitud': 'AE', 'tipus': 'activa', 'data_anterior': "2019-08-20", 'data_actual': "2019-08-31",
                 'lect_anterior': 400.0, 'lect_actual': 500.0, 'ajust': 0.0, 'motiu_ajust': False,
                 'comptador': "241244861", "name": "2.0DHA (P2)"},
                {'magnitud': 'AS', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-31",
                 'lect_anterior': 100.0, 'lect_actual': 120.0, 'ajust': 0.0, 'motiu_ajust': False,
                 'comptador': "155385244", "name": "2.0DHA (P1)"},
                {'magnitud': 'AS', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-31",
                 'lect_anterior': 10.0, 'lect_actual': 20.0, 'ajust': 0.0, 'motiu_ajust': False,
                 'comptador': "155385244", "name": "2.0DHA (P2)"},
            ])
            self.check_lectures_pool_polissa(polissa, [
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0DHA',
                 'lectura': 4000, 'ajust': 0, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': False,
                 'comptador.name': "141244861", 'lectura_exporta_informada': False},
                {'tipus': 'A', 'name': "2019-08-20", 'periode.name': 'P1', 'periode.tarifa.name': '2.0DHA',
                 'lectura': 4200, 'ajust': -175, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': '98',
                 'comptador.name': "141244861", 'lectura_exporta_informada': False},
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P2', 'periode.tarifa.name': '2.0DHA',
                 'lectura': 100, 'ajust': 0, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': False,
                 'comptador.name': "141244861", 'lectura_exporta_informada': False},
                {'tipus': 'A', 'name': "2019-08-20", 'periode.name': 'P2', 'periode.tarifa.name': '2.0DHA',
                 'lectura': 400, 'ajust': -225, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': '98',
                 'comptador.name': "141244861", 'lectura_exporta_informada': False},
                {'tipus': 'A', 'name': "2019-08-20", 'periode.name': 'P1', 'periode.tarifa.name': '2.0DHA',
                 'lectura': 4200, 'ajust': 0, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': False,
                 'comptador.name': "241244861", 'lectura_exporta_informada': False},
                {'tipus': 'A', 'name': "2019-08-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0DHA',
                 'lectura': 4240, 'ajust': -15, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': '98',
                 'comptador.name': "241244861", 'lectura_exporta_informada': False},
                {'tipus': 'A', 'name': "2019-08-20", 'periode.name': 'P2', 'periode.tarifa.name': '2.0DHA',
                 'lectura': 400, 'ajust': 0, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': False,
                 'comptador.name': "241244861", 'lectura_exporta_informada': False},
                {'tipus': 'A', 'name': "2019-08-31", 'periode.name': 'P2', 'periode.tarifa.name': '2.0DHA',
                 'lectura': 500, 'ajust': -25, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': '98',
                 'comptador.name': "241244861", 'lectura_exporta_informada': False},
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0DHA',
                 'lectura': 0, 'ajust': 0, 'lectura_exporta': 100, 'ajust_exporta': 0, 'motiu_ajust': False,
                 'comptador.name': "155385244", 'lectura_exporta_informada': True},
                {'tipus': 'A', 'name': "2019-08-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0DHA',
                 'lectura': 0, 'ajust': 0, 'lectura_exporta': 120, 'ajust_exporta': -5, 'motiu_ajust': '98',
                 'comptador.name': "155385244", 'lectura_exporta_informada': True},
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P2', 'periode.tarifa.name': '2.0DHA',
                 'lectura': 0, 'ajust': 0, 'lectura_exporta': 10, 'ajust_exporta': 0, 'motiu_ajust': False,
                 'comptador.name': "155385244", 'lectura_exporta_informada': True},
                {'tipus': 'A', 'name': "2019-08-31", 'periode.name': 'P2', 'periode.tarifa.name': '2.0DHA',
                 'lectura': 0, 'ajust': 0, 'lectura_exporta': 20, 'ajust_exporta': -2, 'motiu_ajust': '98',
                 'comptador.name': "155385244", 'lectura_exporta_informada': True},
            ])

    def test_nou_autoconsum_41_20DHA_un_sol_comptador_canvi_comptador(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            tarif_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'tarifa_20DHA_new'
            )[1]
            context = {"test_xml": 'test_nou_autoconsum_41_20DHA_un_sol_comptador_canvi_comptador.xml',
                       'tarifa': tarif_id}
            self.prepare_for_phase_3(txn, context=context)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )
            line_obj.process_line_sync(cursor, uid, line_id)

            polissa_id = imd_obj.get_object_reference(cursor, uid, 'giscedata_polissa', 'polissa_0001')[1]
            f1 = line_obj.browse(cursor, uid, line_id)
            polissa = polissa_obj.browse(cursor, uid, polissa_id)
            factura = f1.liniafactura_id[0].factura_id

            # Comprovem que el F1 es correcte
            expect(f1.state).to(equal('valid'))
            expect(f1.lectures_processades).to(equal(True))
            expect(f1.import_phase).to(equal(40))
            # Comprovem que la factura es correcte
            self.assertEqual(factura.amount_untaxed, 27.94)
            self.assertEqual(factura.check_total, 33.81)
            self.check_linies_factura(factura, [
                {'product_id.code': 'ACACP1', 'price_subtotal': 0.0, 'quantity': 5.0},
                {'product_id.code': 'ACACP2', 'price_subtotal': 0.0, 'quantity': 2.0},
                {'product_id.code': 'ACGNP1', 'price_subtotal': 0.0, 'quantity': 20.0},
                {'product_id.code': 'ACGNP2', 'price_subtotal': 0.0, 'quantity': 10.0},
                {'product_id.code': 'ACEXP1', 'price_subtotal': 0.0, 'quantity': 15.0},
                {'product_id.code': 'ACEXP2', 'price_subtotal': 0.0, 'quantity': 8.0},
                {'product_id.code': 'ACTIPO', 'price_subtotal': 0.0, 'quantity': 41.0},
                {'product_id.code': 'ACCR', 'price_subtotal': 0.0, 'quantity': 1.0}
            ])
            self.check_lectures_factura(factura, [
                {'magnitud': 'AE', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-20",
                 'lect_anterior': 4000.0, 'lect_actual': 4200.0, 'ajust': 0.0, 'motiu_ajust': False,
                 'comptador': "141244861", "name": "2.0DHA (P1)"},
                {'magnitud': 'AE', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-20",
                 'lect_anterior': 100.0, 'lect_actual': 400.0, 'ajust': 0.0, 'motiu_ajust': False,
                 'comptador': "141244861", "name": "2.0DHA (P2)"},
                {'magnitud': 'AE', 'tipus': 'activa', 'data_anterior': "2019-08-20", 'data_actual': "2019-08-31",
                 'lect_anterior': 4200.0, 'lect_actual': 4240.0, 'ajust': 0.0, 'motiu_ajust': False,
                 'comptador': "241244861", "name": "2.0DHA (P1)"},
                {'magnitud': 'AE', 'tipus': 'activa', 'data_anterior': "2019-08-20", 'data_actual': "2019-08-31",
                 'lect_anterior': 400.0, 'lect_actual': 500.0, 'ajust': 0.0, 'motiu_ajust': False,
                 'comptador': "241244861", "name": "2.0DHA (P2)"},
                {'magnitud': 'AS', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-20",
                 'lect_anterior': 100.0, 'lect_actual': 110.0, 'ajust': 0.0, 'motiu_ajust': False,
                 'comptador': "141244861", "name": "2.0DHA (P1)"},
                {'magnitud': 'AS', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-20",
                 'lect_anterior': 10.0, 'lect_actual': 15.0, 'ajust': 0.0, 'motiu_ajust': False,
                 'comptador': "141244861", "name": "2.0DHA (P2)"},
                {'magnitud': 'AS', 'tipus': 'activa', 'data_anterior': "2019-08-20", 'data_actual': "2019-08-31",
                 'lect_anterior': 110.0, 'lect_actual': 120.0, 'ajust': 0.0, 'motiu_ajust': False,
                 'comptador': "241244861", "name": "2.0DHA (P1)"},
                {'magnitud': 'AS', 'tipus': 'activa', 'data_anterior': "2019-08-20", 'data_actual': "2019-08-31",
                 'lect_anterior': 15.0, 'lect_actual': 20.0, 'ajust': 0.0, 'motiu_ajust': False,
                 'comptador': "241244861", "name": "2.0DHA (P2)"},
            ])
            self.check_lectures_pool_polissa(polissa, [
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0DHA',
                 'lectura': 4000, 'ajust': 0, 'lectura_exporta': 100, 'ajust_exporta': 0, 'motiu_ajust': False,
                 'comptador.name': "141244861", 'lectura_exporta_informada': True},
                {'tipus': 'A', 'name': "2019-08-20", 'periode.name': 'P1', 'periode.tarifa.name': '2.0DHA',
                 'lectura': 4200, 'ajust': -175, 'lectura_exporta': 110, 'ajust_exporta': -3, 'motiu_ajust': '98',
                 'comptador.name': "141244861", 'lectura_exporta_informada': True},
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P2', 'periode.tarifa.name': '2.0DHA',
                 'lectura': 100, 'ajust': 0, 'lectura_exporta': 10, 'ajust_exporta': 0, 'motiu_ajust': False,
                 'comptador.name': "141244861", 'lectura_exporta_informada': True},
                {'tipus': 'A', 'name': "2019-08-20", 'periode.name': 'P2', 'periode.tarifa.name': '2.0DHA',
                 'lectura': 400, 'ajust': -225, 'lectura_exporta': 15, 'ajust_exporta': -1, 'motiu_ajust': '98',
                 'comptador.name': "141244861", 'lectura_exporta_informada': True},
                {'tipus': 'A', 'name': "2019-08-20", 'periode.name': 'P1', 'periode.tarifa.name': '2.0DHA',
                 'lectura': 4200, 'ajust': 0, 'lectura_exporta': 110, 'ajust_exporta': 0, 'motiu_ajust': False,
                 'comptador.name': "241244861", 'lectura_exporta_informada': True},
                {'tipus': 'A', 'name': "2019-08-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0DHA',
                 'lectura': 4240, 'ajust': -15, 'lectura_exporta': 120, 'ajust_exporta': -2, 'motiu_ajust': '98',
                 'comptador.name': "241244861", 'lectura_exporta_informada': True},
                {'tipus': 'A', 'name': "2019-08-20", 'periode.name': 'P2', 'periode.tarifa.name': '2.0DHA',
                 'lectura': 400, 'ajust': 0, 'lectura_exporta': 15, 'ajust_exporta': 0, 'motiu_ajust': False,
                 'comptador.name': "241244861", 'lectura_exporta_informada': True},
                {'tipus': 'A', 'name': "2019-08-31", 'periode.name': 'P2', 'periode.tarifa.name': '2.0DHA',
                 'lectura': 500, 'ajust': -25, 'lectura_exporta': 20, 'ajust_exporta': -1, 'motiu_ajust': '98',
                 'comptador.name': "241244861", 'lectura_exporta_informada': True},
            ])

    def test_lectura_exporta_informada_is_not_set(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]
            tarif_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'tarifa_20DHA_new'
            )[1]
            polissa_obj.write(
                cursor, uid, polissa_id, {'tarifa': tarif_id}
            )

            polissa = polissa_obj.browse(cursor, uid, polissa_id)
            comptador = polissa.comptadors[0]
            for l in comptador.lectures:
                l.unlink(context={})
            for lp in comptador.lectures_pot:
                lp.unlink(context={})
            comptador.write({'lectures': [(6, 0, [])],
                             'lectures_pot': [(6, 0, [])]})

            self.prepare_for_phase_3(txn, context={"test_xml": "test_ignore_totalitzador.xml"})

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )

            line_obj.process_line_sync(cursor, uid, line_id)
            # Check is imported correctlly
            line = line_obj.browse(cursor, uid, line_id)
            expect(line.state).to(equal('valid'))
            expect(line.import_phase).to(equal(40))
            # Check lectures
            self.assertEqual(
                len(line.liniafactura_id[0].factura_id.lectures_energia_ids), 2
            )

