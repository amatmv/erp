# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from datetime import datetime, timedelta


class TestsWizardModconEnBaja(testing.OOTestCase):

    def setUp(self):
        self.txn = Transaction().start(self.database)

        self.uid = self.txn.user
        self.cursor = self.txn.cursor

        imd_obj = self.openerp.pool.get('ir.model.data')
        self.modcon_wiz_obj = self.openerp.pool.get('wizard.modcon.en.baja')
        self.polissa_obj = self.openerp.pool.get('giscedata.polissa')
        self.bank_obj = self.openerp.pool.get('res.bank')
        self.address_obj = self.openerp.pool.get('res.partner.address')
        self.modcon_obj = self.openerp.pool.get('giscedata.polissa.modcontractual')

        self.polissa_id = imd_obj.get_object_reference(
            self.cursor, self.uid, 'giscedata_polissa', 'polissa_0001'
        )[1]

        self.polissa_obj.send_signal(self.cursor, self.uid, [self.polissa_id], [
            'validar', 'contracte'
        ])

        self.polissa_obj.wkf_baixa(self.cursor, self.uid, [self.polissa_id])

    def tearDown(self):
        self.txn.stop()

    def test_coherencia_de_fechas(self):

        last_modcon_id = self.polissa_obj.read(
            self.cursor, self.uid, self.polissa_id, ['modcontractuals_ids']
        )['modcontractuals_ids'][0]

        modcon_data = self.modcon_obj.read(
            self.cursor, self.uid, last_modcon_id, ['data_inici']
        )

        # We create a modcon that overlaps the start date of the
        # contract modcon

        wiz_id = self.modcon_wiz_obj.create(self.cursor, self.uid, {
            'fecha_inicio': modcon_data['data_inici']
        }, context={'active_id': self.polissa_id})

        vals = self.modcon_wiz_obj.read(self.cursor, self.uid, wiz_id, [])[0]
        # The date coherence validation must fail because we are
        # overlapping dates
        warning_template = self.modcon_wiz_obj.onchange_fecha_inicio(
            self.cursor, self.uid, wiz_id, vals['fecha_inicio'], vals['fecha_final'],
            vals['ultima_modcon']
        )
        self.assertTrue(warning_template['warning'])

        # But if we set a start date one day after the one from the modcon
        # won't fail
        wiz_id = self.modcon_wiz_obj.create(self.cursor, self.uid, {
            'fecha_inicio': datetime.strftime((
                    datetime.strptime(modcon_data['data_inici'], '%Y-%m-%d')
                    + timedelta(days=1)), '%Y-%m-%d'
            )
        }, context={'active_id': self.polissa_id})

        vals = self.modcon_wiz_obj.read(self.cursor, self.uid, wiz_id, [])[0]
        warning_template = self.modcon_wiz_obj.onchange_fecha_inicio(
            self.cursor, self.uid, wiz_id, vals['fecha_inicio'], vals['fecha_final'],
            vals['ultima_modcon']
        )
        self.assertFalse(warning_template['warning'])

    def test_hacer_modcon(self):

        last_modcon_id = self.polissa_obj.read(
            self.cursor, self.uid, self.polissa_id, ['modcontractuals_ids']
        )['modcontractuals_ids'][0]
        modcon_fields = [
            'data_inici', 'data_final', 'bank', 'direccio_notificacio', 'state'
        ]
        modcon_data = self.modcon_obj.read(
            self.cursor, self.uid, last_modcon_id, modcon_fields
        )

        def coalesce(item):
            return False if not item else item[0]

        test_bank = self.bank_obj.search(
            self.cursor, self.uid, [('id', '<>', coalesce(modcon_data['bank']))], limit=1
        )[0]
        test_address = self.address_obj.search(
            self.cursor, self.uid, [('id', '<>', coalesce(modcon_data['direccio_notificacio']))], limit=1
        )[0]

        # We create a modcon that overlaps the start date of the
        # contract modcon
        plus_one_day = datetime.strftime((
                datetime.strptime(modcon_data['data_inici'], '%Y-%m-%d')
                + timedelta(days=1)), '%Y-%m-%d'
        )
        wiz_id = self.modcon_wiz_obj.create(self.cursor, self.uid, {
            'direccion_notificacion': test_address,
            'cuenta_bancaria': test_bank,
            'fecha_inicio': plus_one_day
        }, context={'active_id': self.polissa_id})

        self.modcon_wiz_obj.do_modcon(self.cursor, self.uid, wiz_id)

        modcons_ids = self.polissa_obj.read(
            self.cursor, self.uid, self.polissa_id, ['modcontractuals_ids']
        )['modcontractuals_ids']

        fst_modcon_vals, snd_modcon_vals = self.modcon_obj.read(
            self.cursor, self.uid, modcons_ids, modcon_fields
        )

        # Values for the old modcon
        self.assertEqual(snd_modcon_vals['data_inici'], modcon_data['data_inici'])
        self.assertEqual(snd_modcon_vals['data_final'], modcon_data['data_inici'])
        self.assertEqual(snd_modcon_vals['state'], 'baixa2')
        self.assertNotEqual(coalesce(snd_modcon_vals['bank']), test_bank)
        self.assertNotEqual(coalesce(snd_modcon_vals['direccio_notificacio']), test_address)

        # Values for the created modcon
        self.assertEqual(fst_modcon_vals['data_inici'], plus_one_day)
        self.assertEqual(fst_modcon_vals['data_final'], modcon_data['data_final'])
        self.assertEqual(fst_modcon_vals['state'], 'baixa')
        self.assertEqual(coalesce(fst_modcon_vals['bank']), test_bank)
        self.assertEqual(coalesce(fst_modcon_vals['direccio_notificacio']), test_address)
