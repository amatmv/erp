# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscedataLecturesLectura(osv.osv):

    _inherit = 'giscedata.lectures.lectura'

    def write(self, cursor, uid, ids, vals, context=None):
        if vals and 'lectura_exporta' in vals and vals['lectura_exporta'] != 0:
            vals['lectura_exporta_informada'] = True
        res = super(GiscedataLecturesLectura, self).write(cursor, uid, ids, vals, context)
        return res

    _columns = {
        'lectura_exporta_informada': fields.boolean('Lectura Exporta Informada')
    }

    _defaults = {
        'lectura_exporta_informada': lambda *a: False,
    }


GiscedataLecturesLectura()


class GiscedataLecturesLecturaPool(osv.osv):

    _inherit = 'giscedata.lectures.lectura.pool'

    def write(self, cursor, uid, ids, vals, context=None):
        if vals and 'lectura_exporta' in vals and vals['lectura_exporta'] != 0:
            vals['lectura_exporta_informada'] = True
        res = super(GiscedataLecturesLecturaPool, self).write(cursor, uid, ids, vals, context)
        return res

    _columns = {
        'lectura_exporta_informada': fields.boolean('Lectura Exporta Informada')
    }

    _defaults = {
        'lectura_exporta_informada': lambda *a: False,
    }


GiscedataLecturesLecturaPool()


class GiscedataLecturesSwitchingHelper(osv.osv):

    _inherit = 'giscedata.lectures.switching.helper'

    def vals_lectura(self, cursor, uid, lect, ordre, compt, tarifa=None, en_baixa=False, context=None):
        res = super(GiscedataLecturesSwitchingHelper, self).vals_lectura(
            cursor, uid, lect, ordre, compt, tarifa=tarifa, en_baixa=en_baixa, context=context
        )
        if res.get('magnitud') == 'AS':
            res['lectura_exporta_informada'] = True
        return res

GiscedataLecturesSwitchingHelper()