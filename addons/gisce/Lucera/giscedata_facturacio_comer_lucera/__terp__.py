# -*- coding: utf-8 -*-
{
    "name": "Cuota Lucera",
    "description": """Creates the invoice line for the Lucera fee""",
    "version": "0-dev",
    "author": "Lucera",
    "category": "Lucera",
    "depends":[
        "base",
        "giscedata_polissa",
        "giscedata_facturacio_comer",
        "giscedata_facturacio_switching",
        "giscedata_facturacio_refund_signed",
        "giscedata_polissa_comer"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_facturacio_comer_view.xml",
        "wizard/wizard_ranas_view.xml",
        "pricelist_view.xml",
        "wizard/wizard_modcon_en_baja_view.xml",
        "giscedata_polissa_view.xml",
        "giscedata_lectures_view.xml",
        "security/ir.model.access.csv",
    ],
    "active": False,
    "installable": True
}
