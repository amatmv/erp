# -*- coding: utf-8 -*-
from osv import osv, fields
import calendar
from datetime import datetime, timedelta, date
from tools import config, cache, float_round


class GiscedataFacturacioFacturador(osv.osv):
    """Adds Lucera service line to invoice"""

    _name = 'giscedata.facturacio.facturador'
    _inherit = 'giscedata.facturacio.facturador'

    @cache(timeout=300)
    def _get_lucera_quota_product(self, cursor, uid, tariff_name, context=None):
        product_obj = self.pool.get('product.product')
        product_code = 'LUCERA{}'.format(tariff_name[:3])
        product_id = product_obj.search(cursor, uid, [('default_code', '=', product_code)])
        if not product_id:
            product_id = product_obj.search(cursor, uid, [('default_code', '=', 'LUCERA2.0')])
        product = product_obj.browse(cursor, uid, product_id[0], context)
        return product

    def _add_quota_line(self, cursor, uid, factura, context=None):

        def _get_quota_quantity(start_date, end_date):
            if isinstance(start_date, basestring):
                start_date = datetime.strptime(start_date, '%Y-%m-%d')
            if isinstance(end_date, basestring):
                end_date = datetime.strptime(end_date, '%Y-%m-%d')
            value = 0.0
            while end_date >= start_date:
                units = 1.0 / calendar.monthrange(end_date.year, end_date.month)[1]
                value += units
                end_date -= timedelta(days=1)
            return round(value, 2)

        # Find quota discounts for this contract
        quantity = _get_quota_quantity(factura.data_inici, factura.data_final)
        product = self._get_lucera_quota_product(cursor, uid, factura.tarifa_acces_id.name, context)

        vals = {'data_desde': factura.data_inici,
                'data_fins': factura.data_final,
                'uos_id': product.uom_id.id,
                'quantity': quantity,
                'multi': 1,
                'product_id': product.id,
                'tipus': 'altres',
                'isdiscount': False,
                'name': product.description}

        return self.crear_linia(cursor, uid, factura.id, vals, context)

    def _add_quota_discount_lines(self, cursor, uid, factura, quota_linia, context=None):

        quota_amount = quota_linia.price_subtotal

        if quota_amount == 0:
            return []

        polissa = factura.polissa_id
        discounts = polissa.get_active_quota_discounts()

        if len(discounts) == 0:
            return []

        product = self._get_lucera_quota_product(cursor, uid, factura.tarifa_acces_id.name, context=context)

        pending_quota_amount = quota_amount

        lines = []

        for discount in discounts:
            discount_amount = min(pending_quota_amount, discount.pending_amount)
            pending_quota_amount -= discount_amount

            discount_amount = -float_round(discount_amount, int(config['price_accuracy']))
            # TODO: Should we use quantity and price_unit instead of price_subtotal?
            # Or quantity of the initial discount
            quantity = 1

            line_vals = {
                'name': discount.name,
                'factura_id': factura.id,
                'product_id': product.id,
                'quantity': quantity,
                'multi': 1,
                'data_desde': factura.data_inici,
                'data_fins': factura.data_final,
                'uos_id': product.uom_id.id,
                'tipus': 'altres',
                'force_price': discount_amount,
                'isdiscount': True,
                'polissa_quota_discount_id': discount.id,
            }

            new_line = self.crear_linia(cursor, uid, factura.id, line_vals, context)

            lines.append(new_line)

            # If there is no more pending_quota_amount we don't have to continue processing discounts
            if pending_quota_amount <= 0:
                break

    def _process_lucera_quota(self, cursor, uid, invoice, context=None):

        linia_obj = self.pool.get('giscedata.facturacio.factura.linia')

        quota_linea_id = self._add_quota_line(cursor, uid, invoice, context)
        quota_linia = linia_obj.browse(cursor, uid, quota_linea_id)

        self._add_quota_discount_lines(cursor, uid, invoice, quota_linia, context)

        # Update invoice taxes
        invoice_obj = self.pool.get('giscedata.facturacio.factura')
        invoice_obj.button_reset_taxes(cursor, uid, [invoice.id])

    def _set_temporal_lectures(self, cursor, uid, invoice, context=None):
        # Establece has_temporal_lectures a True
        # si la primera lectura del primer contador de la polissa es de origin ATR
        lectures_obj = self.pool.get('giscedata.lectures.lectura')
        comptador_obj = self.pool.get('giscedata.lectures.comptador')

        has_temporal_lectures = False
        for lectura_energia in invoice.lectures_energia_ids:
            comptador_id = lectura_energia.comptador_id.id
            search_params = [
                ('comptador.id', '=', comptador_id)
            ]
            lectures_ids = lectures_obj.search(cursor, uid, search_params, limit=1,
                                               order="name asc", context=context)
            if lectures_ids:
                origen_comer_id = lectures_obj.browse(cursor, uid, lectures_ids[0]).origen_comer_id
                if origen_comer_id.codi == 'AT':
                    # solo si es el primer contador de la polissa
                    comptador = comptador_obj.browse(cursor, uid, comptador_id)
                    search_params = [
                        ('polissa.id', '=', comptador.polissa.id),
                        ('data_alta', '<', comptador.data_alta)
                    ]
                    prev_polissa_comptadors = comptador_obj.search(cursor, uid, search_params,
                                                                   limit=1, context=context)
                    if not prev_polissa_comptadors:
                        has_temporal_lectures = True

        if has_temporal_lectures:
            vals = {'has_temporal_lectures': has_temporal_lectures}
            invoice.write(vals)

    def fact_via_lectures(self, cursor, uid, polissa_id, lot_id, context=None):
        """We override the method to add the Lucera invoice line and set temporal lectures"""
        if not context:
            context = {}

        invoice_obj = self.pool.get('giscedata.facturacio.factura')

        invoices = super(GiscedataFacturacioFacturador, self). \
            fact_via_lectures(cursor, uid, polissa_id, lot_id, context)

        ctx = context.copy()

        for invoice in invoice_obj.browse(cursor, uid, invoices, context):
            if invoice.journal_id.code in ('ENERGIA', 'ENERGIA.R'):
                # if the invoice is from the energy journal
                self._process_lucera_quota(cursor, uid, invoice, ctx)
                self._set_temporal_lectures(cursor, uid, invoice, ctx)

        return invoices


GiscedataFacturacioFacturador()


class GiscedataFacturacioFactura(osv.osv):
    """Clase per la factura de Lucera."""
    _name = 'giscedata.facturacio.factura'
    _inherit = 'giscedata.facturacio.factura'

    def copy_data(self, cr, uid, id, default=None, context=None):
        if not default:
            default = {}
        default.update({'liquidatable_amount': 0})
        return super(GiscedataFacturacioFactura, self).copy_data(
            cr, uid, id, default, context=context
        )

    def invoice_open(self, cursor, uid, ids, context=None):
        res = super(GiscedataFacturacioFactura, self).invoice_open(
            cursor, uid, ids, context=context
        )
        self_obj = self.pool.get('giscedata.facturacio.factura')
        residuals = self_obj.read(
            cursor, uid, ids, ['signed_residual'], context=context
        )
        for residual in residuals:
            self_obj.write(
                cursor, uid, ids,
                {'liquidatable_amount': residual['signed_residual']}
            )
        return res

    _columns = {
        'liquidatable_amount': fields.float(
            u"Importe liquidable", readonly=True,
            digits=(16, int(config['price_accuracy']))
        ),
        'has_temporal_lectures': fields.boolean("Lecturas temporales", readonly=True)
    }

    _defaults = {
        'has_temporal_lectures': lambda *a: False,
    }

    def unlink(self, cursor, uid, ids, context=None):

        discounts_ids = set()

        for factura in self.browse(cursor, uid, ids, context=context):
            for linia in factura.linia_ids:
                if linia.polissa_quota_discount_id:
                    discounts_ids.add(linia.polissa_quota_discount_id.id)

        super(GiscedataFacturacioFactura, self).unlink(cursor, uid, ids, context=context)

        if discounts_ids:
            discount_obj = self.pool.get('giscedata.polissa.quota.discount')
            # Recompute discount amounts.
            discount_obj.write(cursor, uid, list(discounts_ids), {}, context)


GiscedataFacturacioFactura()


class GiscedataPolissa(osv.osv):
    """Polissa customizations for Lucera."""

    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    _columns = {
        'affiliate_code': fields.char('Afiliado', size=32, required=False),
        'affiliate_external_id': fields.char('Código externo afilidado', size=32, required=False),
        'quota_discounts': fields.one2many('giscedata.polissa.quota.discount', 'polissa_id', 'Descuentos de cuota')
    }

    def get_active_quota_discounts(self, cursor, uid, polissa_id, context=None):

        if not context:
            context = {}

        discounts = []

        discounts_obj = self.pool.get('giscedata.polissa.quota.discount')

        search_discounts = [('polissa_id', '=', polissa_id)]

        ctx = context.copy()
        ctx.update({'active_test': False})

        discounts_ids = discounts_obj.search(cursor, uid, search_discounts, context=ctx)

        for quota_discount in discounts_obj.browse(cursor, uid, discounts_ids):

            pending_amount = quota_discount.initial_amount - quota_discount.discounted_amount

            if pending_amount > 0:
                if quota_discount.dependant_polissa_id:
                    if quota_discount.dependant_polissa_id.state == 'activa':
                        discounts.append(quota_discount)
                else:
                    discounts.append(quota_discount)

        # Sort discounts by pending amount and then by date
        discounts = sorted(discounts, key=lambda d1: (d1.pending_amount, d1.date))

        return discounts

    def copy_data(self, cursor, uid, id, default=None, context=None):
        if default is None:
            default = {}

        default_values = {
            'affiliate_code': False,
            'affiliate_external_id': False,
            'quota_discounts': []
        }

        default.update(default_values)

        res_id = super(
            GiscedataPolissa, self).copy_data(cursor, uid, id, default, context)

        return res_id

GiscedataPolissa()


class GiscedataPolissaQuotaDiscount(osv.osv):
    """Saves Lucera quota discount remaining information"""

    _name = 'giscedata.polissa.quota.discount'

    def _fnct_compute_amounts(self, cursor, uid, ids, field_name=None, arg=None, context=None):
        """Compute the applied discount amount for earch discount id"""
        if not ids:
            return {}

        res = {}

        for discount in self.browse(cursor, uid, ids):
            discounted_amount = 0.0
            for linia in discount.factura_linia_ids:
                if linia.invoice_id.type in ['out_refund']:
                    discounted_amount -= abs(linia.price_subtotal)
                elif linia.invoice_id.type in ['out_invoice']:
                    discounted_amount += abs(linia.price_subtotal)

            pending_amount = discount.initial_amount - discounted_amount
            res[discount.id] = {
                'discounted_amount': discounted_amount,
                'pending_amount': max(0, pending_amount)
            }

        return res

    def _get_applied_discount_ids_by_factura_linia(self, cursor, uid, ids, context=None):
        """Get discount_ids of modified factura_linias"""

        if not context:
            context = {}

        query = '''
            SELECT DISTINCT polissa_quota_discount_id
            FROM giscedata_facturacio_factura_linia
            WHERE polissa_quota_discount_id > 0 AND id IN %s
        '''

        query_params = (tuple(ids), )

        cursor.execute(query, query_params)
        discounts_ids = [x[0] for x in cursor.fetchall()]

        # linia_obj = self.pool.get('giscedata.facturacio.factura.linia')
        # linias = linia_obj.read(cursor, uid, ids, fields=['polissa_quota_discount_id'])
        # discounts_ids_2 = list(set([x['polissa_quota_discount_id'][0] for x in linias if x['polissa_quota_discount_id']]))

        return discounts_ids

    def _get_applied_discount_ids_by_factura(self, cursor, uid, ids, context=None):
        """Get discount_ids of modified facturas"""
        # return ids
        if not context:
            context = {}

        query = '''
            SELECT DISTINCT polissa_quota_discount_id
            FROM giscedata_facturacio_factura_linia
            WHERE polissa_quota_discount_id > 0 AND factura_id IN %s
        '''

        query_params = (tuple(ids),)

        cursor.execute(query, query_params)
        discounts_ids = [x[0] for x in cursor.fetchall()]

        # Other way of computing discount ids

        # linia_obj = self.pool.get('giscedata.facturacio.factura.linia')
        # linia_ids = linia_obj.search(cursor, uid, [('factura_id', 'in', ids)])
        # linias = linia_obj.read(cursor, uid, linia_ids, fields=['polissa_quota_discount_id'])

        # discounts_ids_2 = list(set([x['polissa_quota_discount_id'][0] for x in linias if x['polissa_quota_discount_id']]))

        return discounts_ids

    # When to recompute amounts
    _store_amounts = {
        # When initial amount changes it should recompute amounts. It returns the same ids as passed
        'giscedata.polissa.quota.discount': (lambda self, cr, uid, ids, c=None: ids, [], 30),
        # Every change on factura should recompute discounts
        'giscedata.facturacio.factura': (_get_applied_discount_ids_by_factura, [], 30),
        # Every change on factura linea should recompute discounts
        'giscedata.facturacio.factura.linia': (_get_applied_discount_ids_by_factura_linia, [], 30)
    }

    _columns = {
        'polissa_id': fields.many2one(
            'giscedata.polissa',
            'Contrato',
            ondelete='cascade',
            required=True
        ),
        'name': fields.char("Descripción", size=64),
        'affiliate_code': fields.char('Afiliado', size=32, required=False),
        'dependant_polissa_id': fields.many2one(
            'giscedata.polissa',
            'Contrato amigo',
            ondelete='cascade',
            required=False,
        ),
        'initial_amount': fields.float("Descuento inicial",
                                       digits=(16, 2), required=True),
        'discounted_amount': fields.function(_fnct_compute_amounts,
                                             method=True,
                                             type="float", digits=(16, 2),
                                             string="Descuento aplicado",
                                             readonly=True,
                                             store=_store_amounts,
                                             multi="compute_amounts"),
        'pending_amount': fields.function(_fnct_compute_amounts,
                                          method=True,
                                          type="float", digits=(16, 2),
                                          string="Descuento restante",
                                          readonly=True,
                                          store=_store_amounts,
                                          multi="compute_amounts"),
        'date': fields.date('Fecha de creación'),
        'factura_linia_ids': fields.one2many('giscedata.facturacio.factura.linia',
                                             'polissa_quota_discount_id', 'Líneas de descuento',
                                             readonly=True)
    }

    _defaults = {
        'date': date.today().strftime('%Y-%m-%d')
    }


GiscedataPolissaQuotaDiscount()


class GiscedataFacturacioFacturaLinia(osv.osv):
    """Saves Lucera quota discount amount information"""

    # _name = 'giscedata.facturacio.factura.linia.quota.discount'
    _name = 'giscedata.facturacio.factura.linia'
    _inherit = 'giscedata.facturacio.factura.linia'

    _columns = {
        'polissa_quota_discount_id': fields.many2one('giscedata.polissa.quota.discount', 'Descuento',
                                                     required=False)
    }

    def unlink(self, cursor, uid, ids, context=None):

        discount_obj = self.pool.get('giscedata.polissa.quota.discount')

        for linia in self.browse(cursor, uid, ids, context=context):
            if linia.polissa_quota_discount_id:
                discount_ids = [linia.polissa_quota_discount_id.id]
                discount_obj._fnct_compute_amounts(cursor, uid, discount_ids, context=context)

        super(GiscedataFacturacioFacturaLinia, self).unlink(cursor, uid, ids, context=context)


GiscedataFacturacioFacturaLinia()


class AccountInvoice(osv.osv):

    _inherit = 'account.invoice'

    def _compute_lines(self, cr, uid, ids, name, args, context=None):
        if context is None:
            context = {}
        ctx = context.copy()
        ctx['avoid_0_lines'] = True
        return super(AccountInvoice, self)._compute_lines(cr, uid, ids, name, args, context=ctx)

    _columns = {
        'payment_ids': fields.function(_compute_lines, method=True, relation='account.move.line', type="many2many", string='Pagos'),
    }

AccountInvoice()
