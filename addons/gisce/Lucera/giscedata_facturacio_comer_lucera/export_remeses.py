# -*- encoding: utf-8 -*-
from osv import osv, fields
from tools.translate import _


class WizardExportPaymentFile(osv.osv_memory):

    _name = 'wizard.payment.file.spain'
    _inherit = 'wizard.payment.file.spain'

    def get_group_description(self, cursor, uid, line, context=None):
        message_inv = ""
        company_name = ""
        group_name = ""
        # Search in account.move.line of group
        for move_line in line.move_line_id.move_id.line_id:
            if move_line.invoice:
                iv = move_line.invoice
                company_name = iv.company_id.name
                message_inv = _(u"{0}{1}: {2} ").format(
                    message_inv, iv.number, iv.amount_total
                )
                if len(message_inv) > 140:
                    break
            else:
                group_name = move_line.name

        msg = u"{0} - {1}: {2}".format(
            group_name, company_name, message_inv
        )
        # If len is too big, return default description
        if len(msg) > 140:
            return super(WizardExportPaymentFile, self).get_group_description(cursor, uid, line, context=None)
        else:
            return msg

WizardExportPaymentFile()
