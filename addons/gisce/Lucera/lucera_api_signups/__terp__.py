# -*- coding: utf-8 -*-
{
  "name": "Lucera Signups API",
  "description": """Acts as an interface with the outside work to the ERP for signups""",
  "version": "0-dev",
  "author": "Lucera",
  "category": "Lucera",
  "depends": ['lucera_signups',
              'lucera_common',
              'base_extended'],
  "init_xml": [],
  "demo_xml": ['lucera_api_signups_demo.xml'],
  "update_xml": ['security/ir.model.access.csv'],
  "active": False,
  "installable": True
}
