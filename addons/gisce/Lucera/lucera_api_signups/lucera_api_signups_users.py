# -*- coding: utf-8 -*-
import netsvc
from lucera_common.api import *
from osv import osv


class LuceraApiSignUpsUsers(osv.osv_memory):
    _name = 'lucera.api.signups.users'

    @staticmethod
    def _to_api_partner(partner):
        """
        Converts a partner to an SignUp API Partner
        :param partner:
        :return:
        {
            'id': int,
            'full_name': string,
            'first_name': string,
            'surnames': string,
            'identity_document': {
                'code': string
            },
            'email': string,
            'phone': string,
            'customer_type': 'business|particular|none',
            'is_end_user': True|False,
            'has_address': True|False,
            'address': {
                'id': int,
                'postal_code': string,
                'street': string
        }
        """
        result = {
            'id': partner.id,
            'full_name': partner.name,
            'first_name': partner.first_name,
            'surnames': partner.surnames,
            'identity_document': {
                'code': partner.vat
            },
            'customer_type': partner.customer_type,
            'is_end_user': partner.is_end_user,
            'has_street': False,
            'email': False,
            'phone': False
        }
        # Add address if exists
        if partner.address:
            result.update({
                'has_street': True if partner.address[0].street else False,
                'location': {
                    'id': partner.address[0].id,
                    'postal_code': partner.address[0].zip,
                    'address': partner.address[0].street
                },
                'email': partner.address[0].email,
                'phone': partner.address[0].phone,
            })

        return result

    @lucera_api_method
    def match(self, cursor, uid, vat, name, surnames, email, phone):
        partner_obj = self.pool.get('res.partner')

        if surnames:
            full_name = u'{}, {}'.format(surnames, name)
        else:
            full_name = name

        args = {
            'vat': vat,
            'name': full_name,
            'email': email,
            'phone': phone
        }

        results = partner_obj.match_end_users(cursor, uid, args)
        matches = []
        if results:
            for result in results:
                partner = partner_obj.browse(cursor, uid, result['id'])
                match = self._to_api_partner(partner)
                match.update({
                    'match_type': result['match_type'],
                    'match_result': result['match_result']
                })
                matches.append(match)

        return matches

    @lucera_api_method
    def bank_accounts(self, cursor, uid, user_id):
        """
        Get the bank account and holders of the active contracts handled by this user
        """

        partner_obj = self.pool.get('res.partner')
        bank_account_ids = partner_obj.get_related_bank_accounts_ids(cursor, uid, user_id)

        partner_bank_obj = self.pool.get('res.partner.bank')
        partner_banks = partner_bank_obj.browse(cursor, uid, bank_account_ids)

        bank_accounts = []

        for partner_bank in partner_banks:
            if partner_bank and not any(b['id'] == partner_bank.id for b in bank_accounts):
                bank_account = {
                    'id': partner_bank.id,
                    'code': partner_bank.iban or partner_bank.acc_number,
                    'owner': self._to_api_partner(partner_bank.partner_id)
                }
                if partner_bank.bank:
                    bank_account['bank'] = {
                        'id': partner_bank.bank.id,
                        'name': partner_bank.bank.name,
                    }
                bank_accounts.append(bank_account)

        return bank_accounts

    @lucera_api_method
    def user_contracts_holders(self, cursor, uid, user_id):
        """
         Get the holders of the active contracts handled by this user
        :param cursor:
        :param uid:
        :param user_id:
        :return: Array with contract holders that are not active users.
            [
                {
                    'is_signable': True|False,
                    'cups_status: 'available|in_progress|active|not_found,
                    'cups_id': cups_id?
                    'polissa_id': polissa_id?,
                },
                ...
            ]

        """
        # Search active contracs where this user is the payer
        contract_obj = self.pool.get('giscedata.polissa')
        search_contracts = [('direccio_notificacio.partner_id', '=', user_id),
                            ('titular', '!=', user_id),  # Not from this user
                            ('titular.is_end_user', '=', False),  # Not from end users
                            ('active', '=', True)]

        contract_ids = contract_obj.search(cursor, uid, search_contracts)

        holders = []

        for contract in contract_obj.browse(cursor, uid, contract_ids):
            titular = contract.titular
            if not any(h['id'] == titular.id for h in holders):
                holder = self._to_api_partner(contract.titular)
                holders.append(holder)

        return holders

    @lucera_api_method
    def validate_email(self, cursor, uid, user_id, email):
        """Validates a single user email"""
        partner_obj = self.pool.get('res.partner')
        partner = partner_obj.browse(cursor, uid, user_id)
        if not partner:
            raise osv.except_osv('ValidateError', 'Partner not found')
        if not partner.address:
            raise osv.except_osv('ValidateError', 'Cannot validate user without email')
        address_id = partner.address[0].id
        address_obj = self.pool.get('res.partner.address')
        address_obj.mark_email_as_validated(cursor, uid, address_id, email)
        return True


LuceraApiSignUpsUsers()


def log(msg, level=netsvc.LOG_INFO):
    logger = netsvc.Logger()
    logger.notifyChannel('oorq', level, msg)
