# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from expects import *
from lucera_common.testing import create_tested_polissa
import json
import os
import utils
import mock
from lucera_signups import giscedata_polissa


class LuceraApiSignupsContractsTests(testing.OOTestCase):
    """Test API contracts endpoint"""

    @mock.patch.object(giscedata_polissa.GiscedataPolissa, '_create_asnef_request')
    def test_can_sign_contract(self, mock_method):
        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            user = txn.user
            pool = self.openerp.pool

            result = create_tested_polissa(pool, cursor, user, is_signed=False)
            polissa_id = result['polissa_id']
            polissa_obj = pool.get('giscedata.polissa')

            polissa = polissa_obj.browse(cursor, user, polissa_id)
            email = polissa.direccio_notificacio.email

            # Act
            api_contracts_obj = pool.get('lucera.api.signups.contracts')
            result = api_contracts_obj.sign_by_email(cursor, user, polissa_id, email, '192.168.0.1')

            # Assert
            expect(result['success']).to(be_true)

            # Assert polissa
            polissa = polissa_obj.browse(cursor, user, polissa_id)
            expect(polissa.data_firma_contracte).not_to(be_false)
            expect(polissa.signature_method).to(equal('email'))
            expect(polissa.signature_data).to(equal('192.168.0.1'))
            expect(polissa.direccio_notificacio.is_email_validated).to(be_true)

    @mock.patch.object(giscedata_polissa.GiscedataPolissa, '_create_asnef_request')
    def test_cannot_sign_contract_if_not_notification_email(self, mock_method):
        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            user = txn.user
            pool = self.openerp.pool

            result = create_tested_polissa(pool, cursor, user, is_signed=False)
            polissa_id = result['polissa_id']
            polissa_obj = pool.get('giscedata.polissa')

            polissa = polissa_obj.browse(cursor, user, polissa_id)
            email = 'hacker@gmail.com'

            # Act
            api_contracts_obj = pool.get('lucera.api.signups.contracts')
            result = api_contracts_obj.sign_by_email(cursor, user, polissa_id, email, '192.168.0.1')

            # Assert
            expect(result['success']).to(be_false)
            expect(result['error_code']).to(equal(403))

            polissa = polissa_obj.browse(cursor, user, polissa_id)
            expect(polissa.data_firma_contracte).to(be_false)
            expect(polissa.signature_method).to(be_false)
            expect(polissa.signature_data).to(be_false)

    def test_cannot_sign_contract_if_already_signed(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            user = txn.user
            pool = self.openerp.pool

            result = create_tested_polissa(pool, cursor, user, is_signed=True)  # Signed contract
            polissa_id = result['polissa_id']
            polissa_obj = pool.get('giscedata.polissa')

            polissa = polissa_obj.browse(cursor, user, polissa_id)
            email = polissa.direccio_notificacio.email

            # Act
            api_contracts_obj = pool.get('lucera.api.signups.contracts')
            result = api_contracts_obj.sign_by_email(cursor, user, polissa_id, email, '192.168.0.1')

            # Assert
            expect(result['success']).to(be_false)
            expect(result['error_code']).to(equal(422))

            polissa = polissa_obj.browse(cursor, user, polissa_id)
            expect(polissa.data_firma_contracte).not_to(be_false)

    @staticmethod
    def _get_request(name):
        dir_path = os.path.dirname(os.path.realpath(__file__))
        file_path = os.path.join(dir_path, 'data', name)
        return open(file_path, 'r').read()

    @mock.patch.object(giscedata_polissa.GiscedataPolissa, '_create_asnef_request')
    def test_can_sign_contract_from_signup_with_existing_attribution(self, mock_method):
        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            user = txn.user
            pool = self.openerp.pool
            utils.setup_tarifa(pool, cursor, user)

            data = utils.get_request('default.json')
            request = {'body': data}
            signup_json = json.loads(data)
            api_signup_obj = self.openerp.pool.get('lucera.api.signups.signups')
            response = api_signup_obj.post(cursor, user, request)

            polissa_id = response['commit_result']['polissa_id']
            polissa_obj = self.openerp.pool.get('giscedata.polissa')
            polissa = polissa_obj.browse(cursor, user, polissa_id)
            email = polissa.direccio_notificacio.email

            # Act
            attribution = """
                       {
                             "route": [
                             {
                               "source": "source9",
                               "medium": "medium9",
                               "term": "term9",
                               "content": "content9",
                               "name": "name9",
                               "identifier": "id9",
                               "date": "2017-01-02T02:02:02+01:00"
                             }
                             ]
                       }"""
            api_contracts_obj = pool.get('lucera.api.signups.contracts')
            result = api_contracts_obj.sign_by_email(cursor, user, polissa_id, email, '192.168.0.1', attribution)

            # Assert signup
            signup_electricity_obj = pool.get('lucera.signup.electricity')
            signup_electricity_id = signup_electricity_obj.search(cursor, user, [('polissa_id', '=', polissa_id)])[0]
            signup_electricity = signup_electricity_obj.browse(cursor, user, signup_electricity_id)
            signup = signup_electricity.signups[0]

            via = signup_json['via']
            expect(len(signup.attributions)).to(equal(len(via['attribution']['route'])))

            has_new_attribution = any(a['source'] == 'source9' for a in signup.attributions)
            expect(signup.attributions).not_to(equal('source9'))  # Should not add attribution

    @mock.patch.object(giscedata_polissa.GiscedataPolissa, '_create_asnef_request')
    def test_can_sign_contract_from_signup_with_not_existing_attribution(self, mock_method):
        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            user = txn.user
            pool = self.openerp.pool
            utils.setup_tarifa(pool, cursor, user)

            data = utils.get_request('default.json')
            signup_json = json.loads(data)
            del (signup_json['via']['attribution'])  # Delete the attribution info
            data = json.dumps(signup_json)
            request = {'body': data}

            api_signup_obj = self.openerp.pool.get('lucera.api.signups.signups')
            response = api_signup_obj.post(cursor, user, request)

            polissa_id = response['commit_result']['polissa_id']
            polissa_obj = self.openerp.pool.get('giscedata.polissa')
            polissa = polissa_obj.browse(cursor, user, polissa_id)
            email = polissa.direccio_notificacio.email

            # Act
            attribution = """
                       {
                           
                             "route": [
                              {
                               "source": "source4",
                               "medium": "medium4",
                               "term": "term4",
                               "content": "content4",
                               "name": "name4",
                               "identifier": "id4",
                               "date": "2017-01-02T02:02:02+01:00"
                               },
                               {
                                 "source": "source5",
                                 "medium": "medium5",
                                 "term": "term5",
                                 "content": "content5",
                                 "name": "name5",
                                 "identifier": "id5",
                                 "date": "2017-02-02T02:02:02+01:00"
                               },
                               {
                                 "source": "source6",
                                 "medium": "medium6",
                                 "term": "term6",
                                 "content": "content6",
                                 "name": "name6",
                                 "identifier": "id6",
                                 "date": "2017-03-02T02:02:02+01:00"
                               }
                             ]
                       }"""
            api_contracts_obj = pool.get('lucera.api.signups.contracts')
            result = api_contracts_obj.sign_by_email(cursor, user, polissa_id, email, '192.168.0.1', attribution)

            # Assert signup
            signup_electricity_obj = pool.get('lucera.signup.electricity')
            signup_electricity_id = signup_electricity_obj.search(cursor, user, [('polissa_id', '=', polissa_id)])[0]
            signup_electricity = signup_electricity_obj.browse(cursor, user, signup_electricity_id)
            signup = signup_electricity.signups[0]

            # Attribution is added
            expect(signup.attributions[0].source).to(equal('source4'))
            expect(signup.attributions[0].medium).to(equal('medium4'))
            expect(signup.attributions[0].term).to(equal('term4'))
            expect(signup.attributions[0].content).to(equal('content4'))
            expect(signup.attributions[0].name).to(equal('name4'))
            expect(signup.attributions[0].identifier).to(equal('id4'))
            expect(signup.attributions[0].date).to(equal('2017-01-02 02:02:02'))
