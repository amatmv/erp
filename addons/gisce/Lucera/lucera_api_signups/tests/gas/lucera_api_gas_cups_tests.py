# -*- coding: utf-8 -*-
import unittest
from destral import testing
from destral.transaction import Transaction
from expects import *
from lucera_common.config import *
from lucera_common.testing import create_gas_cups, get_random_cups_code, create_test_gas_polissa
from osv import osv, fields, orm

import os
import json


class LuceraApiGasSignUpCupsTests(testing.OOTestCase):
    def _test_get_signable_status(self, cups, polissa_vals, expected):
        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            uid = txn.user
            pool = self.openerp.pool
            api_signup_obj = pool.get('lucera.api.signups.gas.cups')
            cups_id = False
            if cups and not polissa_vals:  # Create the cups and/or the polissa
                cups_id = create_gas_cups(pool, cursor, uid, cups)
            elif polissa_vals:
                polissa_result = create_test_gas_polissa(pool, cursor, uid,
                                                         cups_code=cups,
                                                         override_vals=polissa_vals)
                polissa_id = polissa_result['gas_polissa_id']
                cups_id = polissa_result['gas_cups_id']
            else:
                cups = get_random_cups_code()

            # Act
            response = api_signup_obj.get_signable_status(cursor, uid, cups)

            # Assert
            try:
                if cups_id:
                    expect(response['cups_id']).to(equal(cups_id))
                expect(response['is_signable']).to(equal(expected['is_signable']))
                expect(response['cups_status']).to(equal(expected['cups_status']))
            except AssertionError as e:
                print 'Cups {} did not pass the test'.format(cups)
                raise

    def test_can_get_signable_status_when_it_is_in_an_active_contract(self):
        self._test_get_signable_status(
            cups='ES0021000000194799FD0F',
            polissa_vals={'state': 'activa'},
            expected={'is_signable': False, 'cups_status': 'active'})

    def test_can_get_signable_status_when_it_is_in_a_draft_contract(self):
        self._test_get_signable_status(
            cups='ES0021000000358446SB0F',
            polissa_vals={'state': 'esborrany'},
            expected={'is_signable': False, 'cups_status': 'in_progress'})

    def test_can_get_signable_status_when_it_is_in_a_validate_contract(self):
        self._test_get_signable_status(
            cups='ES0021000000551747RC0F',
            polissa_vals={'state': 'validar'},
            expected={'is_signable': False, 'cups_status': 'in_progress'})

    def test_can_get_signable_status_when_cups_is_not_in_contract(self):
        self._test_get_signable_status(
            cups='ES0021000000455695BQ0F',
            polissa_vals=False,
            expected={'is_signable': True, 'cups_status': 'available'})

    def test_can_get_signable_status_when_it_is_in_a_validate_contract(self):
        self._test_get_signable_status(
            cups='ES0021000000461016JR0F',
            polissa_vals={'state': 'baixa', 'activa': True},
            expected={'is_signable': True, 'cups_status': 'available'})

    def test_can_get_signable_status_when_cups_is_not_found(self):
        self._test_get_signable_status(
            cups=False,  # Does not create cups before contract
            polissa_vals=False,
            expected={'is_signable': True, 'cups_status': 'not_found'})
