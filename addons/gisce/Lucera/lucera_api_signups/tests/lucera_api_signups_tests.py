# -*- coding: utf-8 -*-
import unittest
from destral import testing
from destral.transaction import Transaction
from expects import *
import sql_db
import lucera_api_signups
from lucera_common.config import *
from osv import osv, fields, orm
import utils
import os
import json
import mock
from lucera_signups import giscedata_polissa, giscegas_polissa
from lucera_common.testing import create_test_electricity_and_gas_polissa
from lucera_common.testing.electricity import create_test_electricity_polissa
from lucera_common.testing.gas import create_test_gas_polissa


class LuceraApiSignUpTests(testing.OOTestCase):

    def test_can_create_and_commit_signup(self):
        """Test that we can create a simple signup.
           Advanced use cases can be found in giscedata_polissa_signup_tests.py"""
        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            user = txn.user
            utils.setup_tarifa(self.openerp.pool, cursor, user)
            data = utils.get_request('user_holder_and_payer_are_the_same_and_supply_point_found.json')
            request = {'body': data}
            data_json = json.loads(data)

            # Act
            api_signup_obj = self.openerp.pool.get('lucera.api.signups.signups')
            response = api_signup_obj.post(cursor, user, request)

            # Assert
            # expect(response.get('status', 200)).to(equal(200))
            # expect(response['content']).not_to(be_empty)

            commit_result = response['commit_result']
            expect(commit_result['user_id']).to(be_above(0))
            expect(commit_result['atr_holder_id']).to(be_above(0))
            expect(commit_result['fiscal_customer_id']).to(be_above(0))
            expect(commit_result['bank_account_owner_id']).to(be_above(0))
            expect(commit_result['bank_account_id']).to(be_above(0))
            expect(commit_result['polissa_id']).to(be_above(0))
            expect(commit_result['cups_id']).to(be_above(0))
            expect(commit_result['electricity_signup_id']).to(be_above(0))

            # Signup
            signup = self.openerp.pool.get('lucera.signup').browse(cursor, user, commit_result['signup_id'])
            expect(signup.electricity_signup_id.polissa_id.id).to(equal(commit_result['polissa_id']))
            expect(signup.external_id).to(equal(data_json['id']))
            expect(signup.original_signup_serialized).to(have_len(be_above_or_equal(100)))

            # Contract

            polissa = self.openerp.pool.get('giscedata.polissa').browse(cursor, user, commit_result['polissa_id'])
            expect(polissa.cups.id).to(equal(commit_result['cups_id']))
            expect(polissa.active).to(equal(True))
            expect(polissa.tarifa.id).to(be_false)  # equal(1))
            expect(polissa.titular.id).to(equal(commit_result['user_id']))
            expect(polissa.state).to(equal('esborrany'))
            expect(polissa.data_firma_contracte).to(equal(False))
            expect(polissa.tipus_vivenda).to(equal('habitual'))
            expect(polissa.cnae.id).to(equal(986))
            expect(polissa.data_baixa).to(equal(False))
            expect(polissa.tipo_pago.code).to(equal('RECIBO_CSB'))
            expect(polissa.direccio_pagament.id).to(equal(commit_result['user_address_id']))
            expect(polissa.pagador.id).to(equal(commit_result['user_id']))
            expect(polissa.pagador_sel).to(equal('titular'))
            expect(polissa.direccio_notificacio.id).to(equal(commit_result['user_address_id']))
            expect(polissa.payment_mode_id.name).to(equal('Recibo mensual BBVA'))
            # expect(polissa.llista_preu.name).to(equal('Tarifas Lucera'))
            expect(polissa.llista_preu.type).to(equal('sale'))
            expect(polissa.bank.id).to(equal(commit_result['bank_account_id']))
            expect(polissa.bank.owner_id.id).to(equal(commit_result['bank_account_owner_id']))
            expect(polissa.facturacio).to(equal(1))
            expect(polissa.facturacio_potencia).to(equal('icp'))
            expect(polissa.altre_p.id).to(equal(False))
            expect(polissa.affiliate_code).to(equal(data_json['promotion']['affiliate']['code']))
            expect(polissa.affiliate_external_id).to(equal(data_json['promotion']['affiliate']['external_id']))
            expect(polissa.potencia).to(be_false)  # .to(equal(4.5)) # We now have this info in the signup
            expect(polissa.observacions).to(be_false)

    @unittest.skip('To make it transactional, we must do two calls: create request and commit request')
    def test_can_create_signup_request_even_if_commit_fails(self):
        """Test that we can create a simple signup.
           Advanced use cases can be found in giscedata_polissa_signup_tests.py"""
        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            user = txn.user
            supply_point_code = 'ES0088000000988828NY'  # Not existing distri, it should not commit
            utils.setup_tarifa(self.openerp.pool, cursor, user)
            data_json = json.loads(utils.get_request('default.json'))
            data_json['electricity']['supply_point']['code'] = supply_point_code
            data = json.dumps(data_json, indent=2)
            request = {'body': data}

            # Act
            api_signup_obj = self.openerp.pool.get('lucera.api.signups.signups')
            response = api_signup_obj.post(cursor, user, request)

            # Assert
            expect(response['signup_request_id']).to(be_above(0))
            expect(response['committed']).to(be_false)

    @unittest.skip('To make it transactional, we must do two calls: create request and commit request')
    def test_does_not_create_two_signups_requests_when_signup_already_exists_and_they_are_equal_and_not_fails(self):
        """Test that we can create a simple signup.
           Advanced use cases can be found in giscedata_polissa_signup_tests.py"""
        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            user = txn.user
            utils.setup_tarifa(self.openerp.pool, cursor, user)
            data_json = json.loads(utils.get_request('default.json'))
            data_json['electricity']['supply_point']['code'] = 'ES0021000000761932DP'  # Another supply point
            data = json.dumps(data_json, indent=2)
            request = {'body': data}

            api_signup_obj = self.openerp.pool.get('lucera.api.signups.signups')
            original = api_signup_obj.post(cursor, user, request)
            expect(original['committed']).to(be_false)

            # Act
            # Try to create the same signup again
            data_json = json.loads(utils.get_request('default.json'))
            data_json['electricity']['supply_point']['code'] = 'ES0021000000540158GT'  # Another supply point
            data = json.dumps(data_json, indent=2)
            request = {'body': data}
            response = api_signup_obj.post(cursor, user, request)

            # Assert
            expect(response['signup_request_id']).to(equal(original['signup_request_id']))
            expect(response['committed']).to(be_true)

    def test_does_not_create_two_signups_requests_with_electricity_when_signup_exists_and_are_equal_and_not_fails(self):
        """Test that we can create a simple signup.
           Advanced use cases can be found in giscedata_polissa_signup_tests.py"""
        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            user = txn.user
            utils.setup_tarifa(self.openerp.pool, cursor, user)
            data_json = json.loads(utils.get_request('default.json'))
            data = json.dumps(data_json, indent=2)
            request = {'body': data}

            api_signup_obj = self.openerp.pool.get('lucera.api.signups.signups')
            original = api_signup_obj.post(cursor, user, request)
            expect(original['committed']).to(be_true)
            expect(original['commit_result']['polissa_id']).to(be_above(0))
            expect(original['commit_result']['gas_polissa_id']).to(be_false)

            # Act
            # Try to create the same signup again
            response = api_signup_obj.post(cursor, user, request)

            # Assert
            expect(response['signup_request_id']).to(equal(original['signup_request_id']))
            expect(response['committed']).to(be_true)
            expect(response['commit_result']['polissa_id']).to(equal(original['commit_result']['polissa_id']))
            expect(response['commit_result']['gas_polissa_id']).to(equal(original['commit_result']['gas_polissa_id']))

    def test_does_not_create_two_signups_requests_with_gas_when_signup_already_exists_and_are_equal_and_not_fails(self):
        """Test that we can create a simple signup.
           Advanced use cases can be found in giscedata_polissa_signup_tests.py"""
        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            user = txn.user
            utils.setup_tarifa(self.openerp.pool, cursor, user)
            data_json = json.loads(utils.get_request('gas.json'))
            data = json.dumps(data_json, indent=2)
            request = {'body': data}

            api_signup_obj = self.openerp.pool.get('lucera.api.signups.signups')
            original = api_signup_obj.post(cursor, user, request)
            expect(original['committed']).to(be_true)
            expect(original['commit_result']['polissa_id']).to(be_false)
            expect(original['commit_result']['gas_polissa_id']).to(be_above(0))

            # Act
            # Try to create the same signup again
            response = api_signup_obj.post(cursor, user, request)

            # Assert
            expect(response['signup_request_id']).to(equal(original['signup_request_id']))
            expect(response['committed']).to(be_true)
            expect(response['commit_result']['polissa_id']).to(equal(original['commit_result']['polissa_id']))
            expect(response['commit_result']['gas_polissa_id']).to(equal(original['commit_result']['gas_polissa_id']))

    def test_does_not_create_two_signups_requests_when_signup_already_exists_and_they_are_not_equal_and_fails(self):
        """Test that we can create a simple signup.
           Advanced use cases can be found in giscedata_polissa_signup_tests.py"""
        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            user = txn.user
            utils.setup_tarifa(self.openerp.pool, cursor, user)
            data_json = json.loads(utils.get_request('default.json'))
            data = json.dumps(data_json, indent=4)
            request = {'body': data}

            api_signup_obj = self.openerp.pool.get('lucera.api.signups.signups')
            original = api_signup_obj.post(cursor, user, request)

            # Act
            # Try to create the same signup again
            # Assert
            result = api_signup_obj.post(cursor, user, request)

            expect(result['commit_error']['type']).to(equal('duplicated'))
            expect(result['signup_request_id']).to(equal(original['signup_request_id']))
            expect(result['commit_result']['polissa_id']).to(equal(original['commit_result']['polissa_id']))
            expect(result['commit_result']['user_id']).to(equal(original['commit_result']['user_id']))
            expect(result['commit_result']['cups_id']).to(equal(original['commit_result']['cups_id']))

    def test_invalid_emails_does_not_create_user(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            user = txn.user
            pool = self.openerp.pool
            utils.setup_tarifa(pool, cursor, user)
            data_json = json.loads(utils.get_request('default.json'))
            data_json['user']['email'] = 'invalid email+españa.com'
            data = json.dumps(data_json, indent=2)
            request = {'body': data}

            # Act
            api_signup_obj = pool.get('lucera.api.signups.signups')

            # Assert
            with self.assertRaises(osv.except_osv) as cm:
                response = api_signup_obj.post(cursor, user, request)

            expect(cm.exception.name).to(be('ValidateError'))

    @mock.patch.object(giscedata_polissa.GiscedataPolissa, '_create_asnef_request')
    def test_can_sign_electricity_contract(self, mock_method):
        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            user = txn.user
            pool = self.openerp.pool

            result = create_test_electricity_polissa(pool, cursor, user, is_signed=False)
            electricity_polissa_id = result['polissa_id']
            polissa_obj = pool.get('giscedata.polissa')

            electricity_polissa = polissa_obj.browse(cursor, user, electricity_polissa_id)
            email = electricity_polissa.direccio_notificacio.email

            # Act
            api_signups_obj = pool.get('lucera.api.signups.signups')
            result = api_signups_obj.sign_by_email(cursor, user, electricity_polissa_id, False, email, '192.168.0.1')

            # Assert
            expect(result['success']).to(be_true)

            # Assert polissa
            electricity_polissa = polissa_obj.browse(cursor, user, electricity_polissa_id)
            expect(electricity_polissa.data_firma_contracte).not_to(be_false)
            expect(electricity_polissa.signature_method).to(equal('email'))
            expect(electricity_polissa.signature_data).to(equal('192.168.0.1'))
            expect(electricity_polissa.direccio_notificacio.is_email_validated).to(be_true)

            # Assert bus message
            bus_message_obj = self.openerp.pool.get('lucera.bus.message')
            bus_message_ids = bus_message_obj.search(cursor, user,
                                                     [('name', '=', 'lucera.contracts.signed')],
                                                     order='create_date desc')
            expect(bus_message_ids).not_to(be_empty)
            bus_message = bus_message_obj.browse(cursor, user, bus_message_ids[0])
            expect(bus_message.name).to(equal('lucera.contracts.signed'))
            bus_message_content = json.loads(bus_message.content)
            expect(bus_message_content['electricity_contract_id']).to(equal(electricity_polissa_id))

    @mock.patch.object(giscegas_polissa.GiscegasPolissa, '_create_asnef_request')
    def test_can_sign_gas_contract(self, mock_method):
        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            user = txn.user
            pool = self.openerp.pool

            result = create_test_gas_polissa(pool, cursor, user, is_signed=False)
            gas_polissa_id = result['gas_polissa_id']
            polissa_obj = pool.get('giscegas.polissa')

            gas_polissa = polissa_obj.browse(cursor, user, gas_polissa_id)
            email = gas_polissa.direccio_notificacio.email

            # Act
            api_signups_obj = pool.get('lucera.api.signups.signups')
            result = api_signups_obj.sign_by_email(cursor, user, False, gas_polissa_id, email, '192.168.0.1')

            # Assert
            expect(result['success']).to(be_true)

            # Assert polissa
            gas_polissa = polissa_obj.browse(cursor, user, gas_polissa_id)
            expect(gas_polissa.data_firma_contracte).not_to(be_false)
            expect(gas_polissa.signature_method).to(equal('email'))
            expect(gas_polissa.signature_data).to(equal('192.168.0.1'))
            expect(gas_polissa.direccio_notificacio.is_email_validated).to(be_true)

            # Assert bus message
            bus_message_obj = self.openerp.pool.get('lucera.bus.message')
            bus_message_ids = bus_message_obj.search(cursor, user,
                                                     [('name', '=', 'lucera.contracts.signed')],
                                                     order='create_date desc')
            expect(bus_message_ids).not_to(be_empty)
            bus_message = bus_message_obj.browse(cursor, user, bus_message_ids[0])
            expect(bus_message.name).to(equal('lucera.contracts.signed'))
            bus_message_content = json.loads(bus_message.content)
            expect(bus_message_content['gas_contract_id']).to(equal(gas_polissa_id))

    @mock.patch.object(giscedata_polissa.GiscedataPolissa, '_create_asnef_request')
    @mock.patch.object(giscegas_polissa.GiscegasPolissa, '_create_asnef_request')
    def test_can_sign_electricity_and_gas_contracts(self, mock_method, mock_method_2):
        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            user = txn.user
            pool = self.openerp.pool

            result = create_test_electricity_and_gas_polissa(pool, cursor, user, is_signed=False)
            electricity_polissa_id = result['polissa_id']
            gas_polissa_id = result['gas_polissa_id']
            electricity_polissa_obj = pool.get('giscedata.polissa')
            gas_polissa_obj = pool.get('giscegas.polissa')

            polissa = electricity_polissa_obj.browse(cursor, user, electricity_polissa_id)
            email = polissa.direccio_notificacio.email

            # Act
            api_signups_obj = pool.get('lucera.api.signups.signups')
            result = api_signups_obj.sign_by_email(cursor, user,
                                                   electricity_polissa_id, gas_polissa_id, email, '192.168.0.1')

            # Assert
            expect(result['success']).to(be_true)

            # Assert electricity polissa
            polissa = electricity_polissa_obj.browse(cursor, user, electricity_polissa_id)
            expect(polissa.data_firma_contracte).not_to(be_false)
            expect(polissa.signature_method).to(equal('email'))
            expect(polissa.signature_data).to(equal('192.168.0.1'))
            expect(polissa.direccio_notificacio.is_email_validated).to(be_true)

            # Assert gas polissa
            gas_polissa = gas_polissa_obj.browse(cursor, user, gas_polissa_id)
            expect(gas_polissa.data_firma_contracte).not_to(be_false)
            expect(gas_polissa.signature_method).to(equal('email'))
            expect(gas_polissa.signature_data).to(equal('192.168.0.1'))
            expect(gas_polissa.direccio_notificacio.is_email_validated).to(be_true)

            # Assert bus message
            bus_message_obj = self.openerp.pool.get('lucera.bus.message')
            bus_message_ids = bus_message_obj.search(cursor, user,
                                                     [('name', '=', 'lucera.contracts.signed')],
                                                     order='create_date desc')
            expect(bus_message_ids).not_to(be_empty)
            bus_message = bus_message_obj.browse(cursor, user, bus_message_ids[0])
            expect(bus_message.name).to(equal('lucera.contracts.signed'))
            bus_message_content = json.loads(bus_message.content)
            expect(bus_message_content['electricity_contract_id']).to(equal(electricity_polissa_id))
            expect(bus_message_content['gas_contract_id']).to(equal(gas_polissa_id))

    @mock.patch.object(giscedata_polissa.GiscedataPolissa, '_create_asnef_request')
    @mock.patch.object(giscegas_polissa.GiscegasPolissa, '_create_asnef_request')
    @mock.patch.object(sql_db.Cursor, 'rollback')
    def test_cannot_sign_electricity_and_gas_contracts_if_already_signed(self, mock_method, mock_method_2, rollback):
        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            user = txn.user
            pool = self.openerp.pool

            result = create_test_electricity_and_gas_polissa(pool, cursor, user, is_signed=True)
            electricity_polissa_id = result['polissa_id']
            gas_polissa_id = result['gas_polissa_id']
            electricity_polissa_obj = pool.get('giscedata.polissa')
            gas_polissa_obj = pool.get('giscegas.polissa')

            electricity_polissa = electricity_polissa_obj.browse(cursor, user, electricity_polissa_id)
            email = electricity_polissa.direccio_notificacio.email

            # Act
            api_signups_obj = pool.get('lucera.api.signups.signups')
            result = api_signups_obj.sign_by_email(cursor, user,
                                                   electricity_polissa_id, gas_polissa_id, email, '192.168.0.1')

            # Assert
            expect(result['success']).to(be_false)
            expect(result['error_code']).to(equal(422))

            # Assert electricity polissa
            electricity_polissa = electricity_polissa_obj.browse(cursor, user, electricity_polissa_id)
            expect(electricity_polissa.data_firma_contracte).not_to(be_false)

            # Assert gas polissa
            gas_polissa = gas_polissa_obj.browse(cursor, user, gas_polissa_id)
            expect(gas_polissa.data_firma_contracte).not_to(be_false)

            # Assert bus message
            bus_message_obj = self.openerp.pool.get('lucera.bus.message')
            bus_message_ids = bus_message_obj.search(cursor, user,
                                                     [('name', '=', 'lucera.contracts.signed')],
                                                     order='create_date desc')
            expect(bus_message_ids).to(be_empty)

    @mock.patch.object(giscedata_polissa.GiscedataPolissa, '_create_asnef_request')
    @mock.patch.object(giscegas_polissa.GiscegasPolissa, '_create_asnef_request')
    def test_can_sign_contract_from_signup_with_existing_attribution(self, mock_method, mock_method_2):
        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            user = txn.user
            pool = self.openerp.pool
            lucera_api_signups.tests.utils.setup_tarifa(pool, cursor, user)

            data = lucera_api_signups.tests.utils.get_request('electricity_and_gas.json')
            request = {'body': data}
            signup_json = json.loads(data)
            api_signup_obj = self.openerp.pool.get('lucera.api.signups.signups')
            response = api_signup_obj.post(cursor, user, request)

            electricity_polissa_id = response['commit_result']['polissa_id']
            gas_polissa_id = response['commit_result']['gas_polissa_id']
            electricity_polissa_obj = self.openerp.pool.get('giscedata.polissa')
            electricity_polissa = electricity_polissa_obj.browse(cursor, user, electricity_polissa_id)
            email = electricity_polissa.direccio_notificacio.email

            # Act
            attribution = """
                                   {
                                         "route": [
                                         {
                                           "source": "source9",
                                           "medium": "medium9",
                                           "term": "term9",
                                           "content": "content9",
                                           "name": "name9",
                                           "identifier": "id9",
                                           "date": "2017-01-02T02:02:02+01:00"
                                         }
                                         ]
                                   }"""
            api_signups_obj = pool.get('lucera.api.signups.signups')
            result = api_signups_obj.sign_by_email(cursor, user,
                                                   electricity_polissa_id, gas_polissa_id,
                                                   email, '192.168.0.1', attribution=attribution)

            # Assert
            expect(result['success']).to(be_true)

            # Assert signup
            signup_electricity_obj = pool.get('lucera.signup.electricity')
            signup_electricity_id = signup_electricity_obj.search(cursor, user,
                                                                  [('polissa_id', '=', electricity_polissa_id)])[0]
            signup_electricity = signup_electricity_obj.browse(cursor, user, signup_electricity_id)
            signup = signup_electricity.signups[0]

            via = signup_json['via']
            expect(len(signup.attributions)).to(equal(len(via['attribution']['route'])))

            has_new_attribution = any(a['source'] == 'source9' for a in signup.attributions)
            expect(signup.attributions).not_to(equal('source9'))  # Should not add attribution

    @mock.patch.object(giscedata_polissa.GiscedataPolissa, '_create_asnef_request')
    @mock.patch.object(giscegas_polissa.GiscegasPolissa, '_create_asnef_request')
    def test_can_sign_contract_from_signup_with_existing_attribution(self, mock_method, mock_method_2):
        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            user = txn.user
            pool = self.openerp.pool
            lucera_api_signups.tests.utils.setup_tarifa(pool, cursor, user)

            data = lucera_api_signups.tests.utils.get_request('electricity_and_gas.json')
            signup_json = json.loads(data)
            del (signup_json['via']['attribution'])  # Delete the attribution info
            data = json.dumps(signup_json)
            request = {'body': data}

            api_signup_obj = self.openerp.pool.get('lucera.api.signups.signups')
            response = api_signup_obj.post(cursor, user, request)

            electricity_polissa_id = response['commit_result']['polissa_id']
            gas_polissa_id = response['commit_result']['gas_polissa_id']
            electricity_polissa_obj = self.openerp.pool.get('giscedata.polissa')
            electricity_polissa = electricity_polissa_obj.browse(cursor, user, electricity_polissa_id)
            email = electricity_polissa.direccio_notificacio.email

            # Act
            attribution = """
                                   {

                                         "route": [
                                          {
                                           "source": "source4",
                                           "medium": "medium4",
                                           "term": "term4",
                                           "content": "content4",
                                           "name": "name4",
                                           "identifier": "id4",
                                           "date": "2017-01-02T02:02:02+01:00"
                                           },
                                           {
                                             "source": "source5",
                                             "medium": "medium5",
                                             "term": "term5",
                                             "content": "content5",
                                             "name": "name5",
                                             "identifier": "id5",
                                             "date": "2017-02-02T02:02:02+01:00"
                                           },
                                           {
                                             "source": "source6",
                                             "medium": "medium6",
                                             "term": "term6",
                                             "content": "content6",
                                             "name": "name6",
                                             "identifier": "id6",
                                             "date": "2017-03-02T02:02:02+01:00"
                                           }
                                         ]
                                   }"""
            api_signups_obj = pool.get('lucera.api.signups.signups')
            result = api_signups_obj.sign_by_email(cursor, user,
                                                   electricity_polissa_id, gas_polissa_id,
                                                   email, '192.168.0.1', attribution=attribution)

            # Assert
            expect(result['success']).to(be_true)

            # Assert signup
            signup_electricity_obj = pool.get('lucera.signup.electricity')
            signup_electricity_id = signup_electricity_obj.search(cursor, user,
                                                                  [('polissa_id', '=', electricity_polissa_id)])[0]
            signup_electricity = signup_electricity_obj.browse(cursor, user, signup_electricity_id)
            signup = signup_electricity.signups[0]

            # Attribution is added
            expect(signup.attributions[0].source).to(equal('source4'))
            expect(signup.attributions[0].medium).to(equal('medium4'))
            expect(signup.attributions[0].term).to(equal('term4'))
            expect(signup.attributions[0].content).to(equal('content4'))
            expect(signup.attributions[0].name).to(equal('name4'))
            expect(signup.attributions[0].identifier).to(equal('id4'))
            expect(signup.attributions[0].date).to(equal('2017-01-02 02:02:02'))
