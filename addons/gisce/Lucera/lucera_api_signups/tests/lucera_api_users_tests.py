# -*- coding: utf-8 -*-
import unittest
from destral import testing
from destral.transaction import Transaction
from expects import *
from lucera_common.config import *
from osv import osv, fields, orm
from lucera_common.testing import create_partner_and_address
from lucera_common.testing.electricity import create_test_electricity_polissa
import os
import json


class LuceraApiSignUpUsersTests(testing.OOTestCase):

    def test_can_find_matching_customers(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            partner_data = {'name': 'Test', 'vat': '05454596P', 'is_end_user': True}
            address_data = {'email': 't@test.com', 'phone': '961202332', 'street': 's'}
            p1, a1 = create_partner_and_address(self.openerp.pool, txn.cursor, txn.user, partner_data, address_data)

            # Act
            users_api_obj = self.openerp.pool.get('lucera.api.signups.users')
            result = users_api_obj.match(txn.cursor, txn.user, {
                'name': 'Test', 'vat': '05454596P',
                'email': 't@test.com', 'phone': '961202332'
            })

            # Assert
            expect(len(result)).to(equal(1))
            expect(result[0]['match_type']).to(equal('full'))
            expect(result[0]['id']).to(equal(p1))
            expect(result[0]['location']['address']).to(equal('s'))

    def test_can_find_matching_customers_with_surnames(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            partner_data = {'name': 'Surnames, Test', 'vat': '05454596P', 'is_end_user': True}
            address_data = {'email': 't@test.com', 'phone': '961202332', 'street': 's'}
            p1, a1 = create_partner_and_address(self.openerp.pool, txn.cursor, txn.user, partner_data, address_data)

            # Act
            users_api_obj = self.openerp.pool.get('lucera.api.signups.users')
            result = users_api_obj.match(txn.cursor, txn.user, {
                'name': 'Test', 'surnames': 'Surnames',
                'vat': '05454596P',
                'email': 't@test.com', 'phone': '961202332'
            })

            # Assert
            expect(len(result)).to(equal(1))
            expect(result[0]['match_type']).to(equal('full'))
            expect(result[0]['id']).to(equal(p1))
            expect(result[0]['location']['address']).to(equal('s'))

    def test_can_find_multiple_matching_customers(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            partner_data = {'name': 'Test', 'vat': '05454596P', 'is_end_user': True}
            address_data = {'email': 't@test.com', 'phone': '961202332', 'street': 's'}
            p1, a1 = create_partner_and_address(self.openerp.pool, txn.cursor, txn.user, partner_data, address_data)

            partner_data2 = {'name': 'Test 2', 'vat': '17325373W', 'is_end_user': True}
            address_data2 = {'email': 't2@test2.com', 'phone': '931214848', 'street': 'e'}
            p2, a2 = create_partner_and_address(self.openerp.pool, txn.cursor, txn.user, partner_data2, address_data2)

            # Act
            users_api_obj = self.openerp.pool.get('lucera.api.signups.users')
            result = users_api_obj.match(txn.cursor, txn.user, {
                'name': 'Test',
                'vat': '05454596P',  # name from p1
                'email': 't2@test2.com',  # email from p2
                'phone': '931214848'
            })

            # Assert
            expect(len(result)).to(equal(2))
            expect(result[0]['match_type']).to(equal('partial'))
            expect(result[0]['id']).to(equal(p1))
            expect(result[0]['location']['address']).to(equal('s'))

            expect(result[1]['match_type']).to(equal('partial'))
            expect(result[1]['id']).to(equal(p2))
            expect(result[1]['location']['address']).to(equal('e'))

    def test_can_find_user_bank_accounts(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            pool = self.openerp.pool
            cursor = txn.cursor
            uid = txn.user

            result = create_test_electricity_polissa(pool, cursor, uid)
            polissa1 = result['polissa_id']
            user_id = result['user_id']

            # Additional contract with same holder
            result2 = create_test_electricity_polissa(pool, cursor, uid, user_id=user_id)
            polissa2 = result2['polissa_id']

            # Additional contract with different bank holder
            payer_id, payer_address_id = create_partner_and_address(pool, cursor, uid, is_end_user=False)
            result3 = create_test_electricity_polissa(pool, cursor, uid, user_id=user_id, fiscal_customer_id=payer_id)
            polissa3 = result3['polissa_id']

            # Additional contract (baixa). It should not appear
            result4 = create_test_electricity_polissa(self.openerp.pool, txn.cursor, txn.user, user_id=user_id)
            polissa4 = result4['polissa_id']
            self.openerp.pool.get('giscedata.polissa').write(txn.cursor, txn.user, polissa4, {
                'active': False,
                'state': 'baixa'
            })

            # Another contract with a different user. It should not appear
            result5 = create_test_electricity_polissa(self.openerp.pool, txn.cursor, txn.user)

            users_api_obj = self.openerp.pool.get('lucera.api.signups.users')

            response = users_api_obj.bank_accounts(txn.cursor, txn.user, user_id)

            expect(len(response)).to(equal(4))
            expect(response[0]['id']).to(be_above(0))
            expect(response[0]['owner']['id']).to(be_above(0))
            expect(len(response[0]['owner']['full_name'])).to(be_above(0))
            expect(len(response[0]['owner']['identity_document']['code'])).to(be_above(0))
            expect(response[0]['bank']['id']).to(be_above(0))
            expect(len(response[0]['bank']['name'])).to(be_above(0))

            holders_ids = set([r['owner']['id'] for r in response])
            expect(holders_ids).to(contain_only(user_id, payer_id))

            partner_bank_ids = list([r['id'] for r in response])
            expect(partner_bank_ids).not_to(contain(result5['bank_account_id']))  # Other user
            expect(partner_bank_ids).to(contain(result['bank_account_id'],
                                                result2['bank_account_id'],
                                                result3['bank_account_id'],
                                                result4['bank_account_id']))  # We show even baixa because it may happen between actions

    def test_can_find_related_contract_holders(self):
        with Transaction().start(self.database) as txn:
            pool = self.openerp.pool
            cursor = txn.cursor
            uid = txn.user

            # Creates a contract. # This must not appear because the holder is the same as the user
            result = create_test_electricity_polissa(pool, cursor, uid)
            polissa1 = result['polissa_id']
            user_id = result['user_id']

            # Additional contract with same holder. This must not appear because the holder is the same as the user
            result2 = create_test_electricity_polissa(pool, cursor, uid, user_id=user_id)
            polissa2 = result2['polissa_id']

            # Additional contract with different holder. . This MUST appear
            holder2_id, holder2_address_id = create_partner_and_address(pool, cursor, uid, is_end_user=False)
            result3 = create_test_electricity_polissa(pool, cursor, uid, user_id=user_id, atr_holder_id=holder2_id)
            polissa3 = result3['polissa_id']

            # Additional contract (baixa). This must not appear
            result4 = create_test_electricity_polissa(self.openerp.pool, txn.cursor, txn.user, user_id=user_id)
            polissa4 = result4['polissa_id']
            self.openerp.pool.get('giscedata.polissa').write(txn.cursor, txn.user, polissa4, {
                'active': False,
                'state': 'baixa'
            })

            # Another contract with a different user. This must not appear
            result5 = create_test_electricity_polissa(self.openerp.pool, txn.cursor, txn.user)

            users_api_obj = self.openerp.pool.get('lucera.api.signups.users')

            response = users_api_obj.user_contracts_holders(txn.cursor, txn.user, user_id)

            expect(len(response)).to(equal(1))

            holders_ids = set([r['id'] for r in response])
            expect(holders_ids).to(contain_only(holder2_id))

    def test_can_mark_email_as_validated(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            partner_data = {'name': 'Test', 'vat': '05454596P', 'is_end_user': True}
            address_data = {'email': 't@test.com', 'phone': '961202332', 'street': 's'}
            p1, a1 = create_partner_and_address(self.openerp.pool, txn.cursor, txn.user,
                                                partner_data, address_data, is_email_validated=False)

            address_obj = self.openerp.pool.get('res.partner.address')
            address = address_obj.browse(txn.cursor, txn.user, a1)
            expect(address.is_email_validated).to(be_false)

            # Act
            users_api_obj = self.openerp.pool.get('lucera.api.signups.users')
            result = users_api_obj.validate_email(txn.cursor, txn.user, p1, 't@test.com')

            # Assert
            expect(result).to(be_true)
            address = address_obj.browse(txn.cursor, txn.user, a1)
            expect(address.is_email_validated).to(be_true)
