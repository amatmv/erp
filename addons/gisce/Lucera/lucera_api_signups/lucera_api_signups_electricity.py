# -*- coding: utf-8 -*-
import netsvc
from lucera_common.api import *
from lucera_common.validate import *
from osv import osv


class LuceraApiSignUpsElectricityCups(osv.osv_memory):
    _name = 'lucera.api.signups.electricity.cups'

    @lucera_api_method()
    def get_signable_status(self, cursor, uid, code):
        """
        Gets if a supply point can be used as signup
        :param cursor
        :param uid
        :param code: The supply point code
        :return:
            {
                'is_signable': True|False,
                'cups_status: 'available|in_progress|active|not_found,
                'cups_id': cups_id?
                'polissa_id': polissa_id?,
            }
        """

        # Check if cups already exists and is in contract
        cups_code = normalize_cups(code)
        if not cups_code:
            raise {'is_signable': True, 'cups_status': 'not_found'}

        cups_obj = self.pool.get('giscedata.cups.ps')
        polissa_obj = self.pool.get('giscedata.polissa')
        cups_ids = cups_obj.search(cursor, uid, [('name', 'like', cups_code[:20])], context={'active_test': False})
        if cups_ids and len(cups_ids) > 0:
            cups_id = cups_ids[0]
            polissa_ids = polissa_obj.search(cursor, uid,
                                             [('cups', '=', cups_id), ('active', '=', True)], order='id desc')
            if polissa_ids:
                polissa_id = polissa_ids[0]
                polissa = polissa_obj.read(cursor, uid, polissa_id, ['state'])
                if polissa['state'] in ['esborrany', 'validar']:
                    cups_status, is_signable = ('in_progress', False)
                elif polissa['state'] in 'baixa':
                    cups_status, is_signable = ('available', True)
                else:
                    cups_status, is_signable = ('active', False)
                return {'is_signable': is_signable, 'cups_status': cups_status,
                        'cups_id': cups_id, 'polissa_id': polissa_id}
            else:
                return {'is_signable': True, 'cups_status': 'available', 'cups_id': cups_ids[0]}

        # Check if cups already exists in a signup that is not
        # signup_obj = self.pool.get('lucera.signup')
        return {'is_signable': True, 'cups_status': 'not_found'}


LuceraApiSignUpsElectricityCups()


class LuceraApiSignUpsElectricityContracts(osv.osv_memory):
    _name = 'lucera.api.signups.electricity.contracts'

    @lucera_api_method()
    def sign_by_email(self, cursor, uid, contract_id, email, ip, attribution=None, analytics_user_id=None):
        """
        Signs a contract
        :param cursor
        :param uid
        :param contract_id: The contract_id
        :param email: Email that is signing the contract
        :param ip: IP address signing the contract
        :param attribution: JSON string with attribution info
        :param analytics_user_id: user id for analytics reports
        :return:
            {
                'success': True|False,
                'cups_status: 'available|in_progress|active|not_found,
                'cups_id': cups_id?
                'polissa_id': polissa_id?,
            }
        """

        polissa_obj = self.pool.get('giscedata.polissa')

        try:
            # Sign contract
            results = polissa_obj.sign_by_email(cursor, uid, contract_id, email, ip)

            notification_partner = polissa_obj.browse(cursor, uid, contract_id).direccio_notificacio.partner_id
            if not notification_partner.analytics_user_id:
                if not analytics_user_id:
                    analytics_user_id = 'default_id_' + str(notification_partner.id)
                analytics_user_id = self.pool.get('res.partner')\
                    .set_analytics_user_id_if_empty(cursor, uid, notification_partner.id, analytics_user_id)
            else:
                analytics_user_id = notification_partner.analytics_user_id

            # Mark email as validated
            address_id = results[contract_id]['address_id']
            address_obj = self.pool.get('res.partner.address')
            address_obj.mark_email_as_validated(cursor, uid, address_id, email)

            # Add signature attribution
            if attribution:
                polissa = polissa_obj.browse(cursor, uid, contract_id)
                if polissa.electricity_signups and polissa.electricity_signups[0].signups:
                    signup_id = polissa.electricity_signups[0].signups[0].id
                    signup_obj = self.pool.get('lucera.signup')
                    signup = signup_obj.add_signature_attribution(cursor, uid, signup_id, attribution)

            return {'success': True, 'analytics_user_id': analytics_user_id}
        except osv.except_osv, o:
            if o.name == 'ValidateError' and \
                    (o.value == 'Already signed' or o.value == 'Cancelled and exists new active contract'):
                error_code = 422  # Already signed
            elif o.name == 'SecurityError':
                error_code = 403  # Forbidden
            else:
                error_code = 500  # Other error is not recognized
            return {'success': False, 'error': '{} - {}'.format(o.name, o.value), 'error_code': error_code}


LuceraApiSignUpsElectricityContracts()


def log(msg, level=netsvc.LOG_INFO):
    logger = netsvc.Logger()
    logger.notifyChannel('oorq', level, msg)
