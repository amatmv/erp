# -*- coding: utf-8 -*-
import netsvc
from lucera_common.api import *
from lucera_common.validate import *
from osv import osv
from osv.osv import except_osv
from osv.orm import ValidateException
import json


class LuceraApiSignUpsSignUp(osv.osv_memory):
    _name = 'lucera.api.signups.signups'

    def _get_result_from_existing_signup_request(self, signup_request):
        result = {
            'signup_request_id': signup_request.id,
            'committed': signup_request.state == 'committed',
            'existing': True
        }

        if result['committed']:
            polissa = signup_request.signup_id.electricity_signup_id.polissa_id
            result['commit_result'] = {
                'polissa_id': polissa.id,
                'user_id': polissa.direccio_notificacio.partner_id.id or 0,  # It can be null sometimes
                'user_address_id': polissa.direccio_notificacio.id,
                'atr_holder_id': polissa.titular.id,
                'atr_holder_address_id': polissa.titular.address[0].id,
                'fiscal_customer_id': polissa.pagador.id,
                'fiscal_customer_address_id': polissa.pagador.address[0].id,
                'bank_account_id': polissa.bank.id,
                'bank_account_owner_id': polissa.bank.owner_id.id,
                'cups_id': polissa.cups.id,
                'signup_id': signup_request.signup_id.id,
            }

        return result

    @lucera_api_method(body='signup')
    def post(self, cursor, uid, signup):
        """Creates a signup"""
        signup_request_obj = self.pool.get('lucera.signup.request')
        vals = {
            'external_id': signup['id'],
            'signup_serialized': json.dumps(signup, indent=4)
            # 'cups_code': signup['electricity']['supply_point'].get('code', False)
        }

        # Search for existing signup by external id
        signup_request_ids = signup_request_obj.search(cursor, uid,
                                                       [('external_id', '=', vals['external_id'])],
                                                       order='id desc')
        if signup_request_ids:
            signup_request_id = signup_request_ids[0]
            signup_request = signup_request_obj.browse(cursor, uid, signup_request_id)
            if signup_request.signup_id and signup_request.signup_id.id > 0:
                commit_result = {
                        'signup_request_id': signup_request_id,
                        'signup_id': signup_request.signup_id.id
                    }
                if signup_request.signup_id.electricity_signup_id.polissa_id:
                    polissa = signup_request.signup_id.electricity_signup_id.polissa_id
                    commit_result['polissa_id'] = polissa.id
                    commit_result['user_id'] = polissa.direccio_notificacio.partner_id.id
                    commit_result['cups_id'] = polissa.cups.id
                else:
                    commit_result['polissa_id'] = False
                    commit_result['cups_id'] = False
                if signup_request.signup_id.gas_signup_id \
                        and signup_request.signup_id.gas_signup_id.polissa_id:
                    polissa = signup_request.signup_id.gas_signup_id.polissa_id
                    commit_result['gas_polissa_id'] = polissa.id
                    commit_result['gas_cups_id'] = polissa.cups.id
                    commit_result['user_id'] = polissa.direccio_notificacio.partner_id.id
                else:
                    commit_result['gas_polissa_id'] = False
                    commit_result['gas_cups_id'] = False
                result = {
                    'signup_request_id': signup_request_id,
                    'committed': True,
                    'commit_error': {
                        'type': 'duplicated',
                        'message': 'Signup already exists'
                    },
                    'commit_result': commit_result
                }
                return result
                # raise osv.except_osv('ValidateError', 'You cannot update a request when it already has a signup')
            # It is a retry and we don't have a signup
            signup_request_obj.write(cursor, uid, signup_request_id, vals)
        else:
            # Create signup
            signup_request_id = signup_request_obj.create(cursor, uid, vals)
        result = {
            'signup_request_id': signup_request_id,
            'committed': False
        }
        # Try to commit signup
        try:
            commit_result = signup_request_obj.commit(cursor, uid, [signup_request_id])
            result['committed'] = True
            result['commit_result'] = commit_result[signup_request_id]
        except ValidateException, e:
            logger = netsvc.Logger()
            result['commit_error'] = {'type': 'validation', 'message': e.value}
            logger.notifyChannel('lucera_api_signups', netsvc.LOG_WARNING,
                                 'Could not commit signup {}. \nWarning:\n {}'.format(signup_request_id, str(e)))
        except except_osv, e:
            logger = netsvc.Logger()
            result['commit_error'] = {'type': 'validation', 'message': e.value}
            logger.notifyChannel('lucera_api_signups', netsvc.LOG_WARNING,
                                 'Could not commit signup {}. \nWarning:\n {}'.format(signup_request_id, str(e)))
        except Exception, e:
            logger = netsvc.Logger()
            result['commit_error'] = {'type': 'generic', 'message': e.message}
            logger.notifyChannel('lucera_api_signups', netsvc.LOG_ERROR,
                                 'Could not commit signup {}. \nError:\n {}'.format(signup_request_id, str(e)))

        return result

    @lucera_api_method(body='signup')
    def update_signup_json(self, cursor, uid, signup):
        """Migrate campaign routes only"""
        context = {}

        polissa_obj = self.pool.get('giscedata.polissa')
        signup_request_obj = self.pool.get('lucera.signup.request')
        signup_obj = self.pool.get('lucera.signup')
        signup_commiter = self.pool.get('lucera.signup.committer')

        external_id = signup['id']
        signup_serialized = json.dumps(signup, indent=4, ensure_ascii=False).encode('utf8')
        signup_request_ids = signup_request_obj.search(cursor, uid,
                                                       [('external_id', '=', external_id)], order='id desc')
        if signup_request_ids:
            signup_request_obj.write(cursor, uid, signup_request_ids[0], {'signup_serialized': signup_serialized})

        signup_ids = signup_obj.search(cursor, uid, [('external_id', '=', external_id)], order='id desc')

        if signup_ids:
            signup_obj.write(cursor, uid, signup_ids[0], {'original_signup_serialized': signup_serialized})
            signup_commiter.update_signup_attributions(cursor, uid, signup_ids[0])

        return True


LuceraApiSignUpsSignUp()


class LuceraApiSignUpsUser(osv.osv_memory):
    _name = 'lucera.api.signups.users'

    @staticmethod
    def _to_api_partner(partner):
        """
        Converts a partner to an SignUp API Partner
        :param partner:
        :return:
        {
            'id': int,
            'full_name': string,
            'first_name': string,
            'surnames': string,
            'identity_document': {
                'code': string
            },
            'email': string,
            'phone': string,
            'customer_type': 'business|particular|none',
            'is_end_user': True|False,
            'has_address': True|False,
            'address': {
                'id': int,
                'postal_code': string,
                'street': string
        }
        """
        result = {
            'id': partner.id,
            'full_name': partner.name,
            'first_name': partner.first_name,
            'surnames': partner.surnames,
            'identity_document': {
                'code': partner.vat
            },
            'customer_type': partner.customer_type,
            'is_end_user': partner.is_end_user,
            'has_street': False,
            'email': False,
            'phone': False
        }
        # Add address if exists
        if partner.address:
            result.update({
                'has_street': True if partner.address[0].street else False,
                'location': {
                    'id': partner.address[0].id,
                    'postal_code': partner.address[0].zip,
                    'address': partner.address[0].street
                },
                'email': partner.address[0].email,
                'phone': partner.address[0].phone,
            })

        return result

    @lucera_api_method
    def match(self, cursor, uid, vat, name, surnames, email, phone):
        partner_obj = self.pool.get('res.partner')

        if surnames:
            full_name = u'{}, {}'.format(surnames, name)
        else:
            full_name = name

        args = {
            'vat': vat,
            'name': full_name,
            'email': email,
            'phone': phone
        }

        results = partner_obj.match_end_users(cursor, uid, args)
        matches = []
        if results:
            for result in results:
                partner = partner_obj.browse(cursor, uid, result['id'])
                match = self._to_api_partner(partner)
                match.update({
                    'match_type': result['match_type'],
                    'match_result': result['match_result']
                })
                matches.append(match)

        return matches

    @lucera_api_method
    def bank_accounts(self, cursor, uid, user_id):
        """
        Get the bank account and holders of the active contracts handled by this user
        """

        partner_obj = self.pool.get('res.partner')
        bank_account_ids = partner_obj.get_related_bank_accounts_ids(cursor, uid, user_id)

        partner_bank_obj = self.pool.get('res.partner.bank')
        partner_banks = partner_bank_obj.browse(cursor, uid, bank_account_ids)

        bank_accounts = []

        for partner_bank in partner_banks:
            if partner_bank and not any(b['id'] == partner_bank.id for b in bank_accounts):
                bank_account = {
                    'id': partner_bank.id,
                    'code': partner_bank.iban or partner_bank.acc_number,
                    'owner': self._to_api_partner(partner_bank.partner_id)
                }
                if partner_bank.bank:
                    bank_account['bank'] = {
                        'id': partner_bank.bank.id,
                        'name': partner_bank.bank.name,
                    }
                bank_accounts.append(bank_account)

        return bank_accounts

    @lucera_api_method
    def user_contracts_holders(self, cursor, uid, user_id):
        """
         Get the holders of the active contracts handled by this user
        :param cursor:
        :param uid:
        :param user_id:
        :return: Array with contract holders that are not active users.
            [
                {
                    'is_signable': True|False,
                    'cups_status: 'available|in_progress|active|not_found,
                    'cups_id': cups_id?
                    'polissa_id': polissa_id?,
                },
                ...
            ]

        """
        # Search active contracs where this user is the payer
        contract_obj = self.pool.get('giscedata.polissa')
        search_contracts = [('direccio_notificacio.partner_id', '=', user_id),
                            ('titular', '!=', user_id),  # Not from this user
                            ('titular.is_end_user', '=', False),  # Not from end users
                            ('active', '=', True)]

        contract_ids = contract_obj.search(cursor, uid, search_contracts)

        holders = []

        for contract in contract_obj.browse(cursor, uid, contract_ids):
            titular = contract.titular
            if not any(h['id'] == titular.id for h in holders):
                holder = self._to_api_partner(contract.titular)
                holders.append(holder)

        return holders

    @lucera_api_method
    def validate_email(self, cursor, uid, user_id, email):
        """Validates a single user email"""
        partner_obj = self.pool.get('res.partner')
        partner = partner_obj.browse(cursor, uid, user_id)
        if not partner:
            raise osv.except_osv('ValidateError', 'Partner not found')
        if not partner.address:
            raise osv.except_osv('ValidateError', 'Cannot validate user without email')
        address_id = partner.address[0].id
        address_obj = self.pool.get('res.partner.address')
        address_obj.mark_email_as_validated(cursor, uid, address_id, email)
        return True


LuceraApiSignUpsUser()


class LuceraApiSignUpsCups(osv.osv_memory):
    _name = 'lucera.api.signups.cups'

    @lucera_api_method()
    def get_signable_status(self, cursor, uid, code):
        """
        Gets if a supply point can be used as signup
        :param cursor
        :param uid
        :param code: The supply point code
        :return:
            {
                'is_signable': True|False,
                'cups_status: 'available|in_progress|active|not_found,
                'cups_id': cups_id?
                'polissa_id': polissa_id?,
            }
        """

        # Check if cups already exists and is in contract
        cups_code = normalize_cups(code)
        if not cups_code:
            raise {'is_signable': True, 'cups_status': 'not_found'}

        cups_obj = self.pool.get('giscedata.cups.ps')
        polissa_obj = self.pool.get('giscedata.polissa')
        cups_ids = cups_obj.search(cursor, uid, [('name', 'like', cups_code[:20])], context={'active_test': False})
        if cups_ids and len(cups_ids) > 0:
            cups_id = cups_ids[0]
            polissa_ids = polissa_obj.search(cursor, uid,
                                             [('cups', '=', cups_id), ('active', '=', True)], order='id desc')
            if polissa_ids:
                polissa_id = polissa_ids[0]
                polissa = polissa_obj.read(cursor, uid, polissa_id, ['state'])
                if polissa['state'] in ['esborrany', 'validar']:
                    cups_status, is_signable = ('in_progress', False)
                else:
                    cups_status, is_signable = ('active', False)
                return {'is_signable': is_signable, 'cups_status': cups_status,
                        'cups_id': cups_id, 'polissa_id': polissa_id}
            else:
                return {'is_signable': True, 'cups_status': 'available', 'cups_id': cups_ids[0]}

        # Check if cups already exists in a signup that is not
        # signup_obj = self.pool.get('lucera.signup')
        return {'is_signable': True, 'cups_status': 'not_found'}


LuceraApiSignUpsCups()


class LuceraApiSignUpsContracts(osv.osv_memory):
    _name = 'lucera.api.signups.contracts'

    @lucera_api_method()
    def sign_by_email(self, cursor, uid, contract_id, email, ip, attribution=None, analytics_user_id=None):
        """
        Signs a contract
        :param cursor
        :param uid
        :param contract_id: The contract_id
        :param email: Email that is signing the contract
        :param ip: IP address signing the contract
        :param attribution: JSON string with attribution info
        :param analytics_user_id: user id for analytics reports
        :return:
            {
                'success': True|False,
                'cups_status: 'available|in_progress|active|not_found,
                'cups_id': cups_id?
                'polissa_id': polissa_id?,
            }
        """

        polissa_obj = self.pool.get('giscedata.polissa')

        try:
            # Sign contract
            results = polissa_obj.sign_by_email(cursor, uid, contract_id, email, ip)

            notification_partner = polissa_obj.browse(cursor, uid, contract_id).direccio_notificacio.partner_id
            if not notification_partner.analytics_user_id:
                if not analytics_user_id:
                    analytics_user_id = 'default_id_' + str(notification_partner.id)
                analytics_user_id = self.pool.get('res.partner')\
                    .set_analytics_user_id_if_empty(cursor, uid, notification_partner.id, analytics_user_id)
            else:
                analytics_user_id = notification_partner.analytics_user_id

            # Mark email as validated
            address_id = results[contract_id]['address_id']
            address_obj = self.pool.get('res.partner.address')
            address_obj.mark_email_as_validated(cursor, uid, address_id, email)

            # Add signature attribution
            if attribution:
                signup_obj = self.pool.get('lucera.signup')
                signup = signup_obj.add_signature_attribution(cursor, uid, contract_id, attribution)

            return {'success': True, 'analytics_user_id': analytics_user_id}
        except osv.except_osv, o:
            if o.name == 'ValidateError' and o.value == 'Already signed':
                error_code = 422  # Already signed
            elif o.name == 'SecurityError':
                error_code = 403  # Forbidden
            else:
                error_code = 500  # Other error is not recognized
            return {'success': False, 'error': '{} - {}'.format(o.name, o.value), 'error_code': error_code}


LuceraApiSignUpsContracts()


def log(msg, level=netsvc.LOG_INFO):
    logger = netsvc.Logger()
    logger.notifyChannel('oorq', level, msg)
