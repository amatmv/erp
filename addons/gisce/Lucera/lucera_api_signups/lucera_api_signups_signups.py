# -*- coding: utf-8 -*-
import netsvc
from lucera_common.api import *
from osv import osv
from osv.orm import ValidateException
from osv.osv import except_osv


class LuceraApiSignUpsSignUps(osv.osv_memory):
    _name = 'lucera.api.signups.signups'

    def _get_result_from_existing_signup_request(self, signup_request):
        result = {
            'signup_request_id': signup_request.id,
            'committed': signup_request.state == 'committed',
            'existing': True
        }

        if result['committed']:
            polissa = signup_request.signup_id.electricity_signup_id.polissa_id
            result['commit_result'] = {
                'polissa_id': polissa.id,
                'user_id': polissa.direccio_notificacio.partner_id.id or 0,  # It can be null sometimes
                'user_address_id': polissa.direccio_notificacio.id,
                'atr_holder_id': polissa.titular.id,
                'atr_holder_address_id': polissa.titular.address[0].id,
                'fiscal_customer_id': polissa.pagador.id,
                'fiscal_customer_address_id': polissa.pagador.address[0].id,
                'bank_account_id': polissa.bank.id,
                'bank_account_owner_id': polissa.bank.owner_id.id,
                'cups_id': polissa.cups.id,
                'signup_id': signup_request.signup_id.id,
            }

        return result

    @lucera_api_method(body='signup')
    def post(self, cursor, uid, signup):
        """Creates a signup"""
        signup_request_obj = self.pool.get('lucera.signup.request')
        vals = {
            'external_id': signup['id'],
            'signup_serialized': json.dumps(signup, indent=4)
            # 'cups_code': signup['electricity']['supply_point'].get('code', False)
        }

        # Search for existing signup by external id
        signup_request_ids = signup_request_obj.search(cursor, uid,
                                                       [('external_id', '=', vals['external_id'])],
                                                       order='id desc')
        if signup_request_ids:
            signup_request_id = signup_request_ids[0]
            signup_request = signup_request_obj.browse(cursor, uid, signup_request_id)
            if signup_request.signup_id and signup_request.signup_id.id > 0:
                commit_result = {
                        'signup_request_id': signup_request_id,
                        'signup_id': signup_request.signup_id.id
                    }
                if signup_request.signup_id.electricity_signup_id \
                        and signup_request.signup_id.electricity_signup_id.polissa_id:
                    polissa = signup_request.signup_id.electricity_signup_id.polissa_id
                    commit_result['polissa_id'] = polissa.id
                    commit_result['user_id'] = polissa.direccio_notificacio.partner_id.id
                    commit_result['cups_id'] = polissa.cups.id
                else:
                    commit_result['polissa_id'] = False
                    commit_result['cups_id'] = False
                if signup_request.signup_id.gas_signup_id \
                        and signup_request.signup_id.gas_signup_id.polissa_id:
                    polissa = signup_request.signup_id.gas_signup_id.polissa_id
                    commit_result['gas_polissa_id'] = polissa.id
                    commit_result['user_id'] = polissa.direccio_notificacio.partner_id.id
                    commit_result['gas_cups_id'] = polissa.cups.id
                else:
                    commit_result['gas_polissa_id'] = False
                    commit_result['gas_cups_id'] = False
                result = {
                    'signup_request_id': signup_request_id,
                    'committed': True,
                    'commit_error': {
                        'type': 'duplicated',
                        'message': 'Signup already exists'
                    },
                    'commit_result': commit_result
                }
                return result
                # raise osv.except_osv('ValidateError', 'You cannot update a request when it already has a signup')
            # It is a retry and we don't have a signup
            signup_request_obj.write(cursor, uid, signup_request_id, vals)
        else:
            # Create signup
            signup_request_id = signup_request_obj.create(cursor, uid, vals)
        result = {
            'signup_request_id': signup_request_id,
            'committed': False
        }
        # Try to commit signup
        try:
            commit_result = signup_request_obj.commit(cursor, uid, [signup_request_id])
            result['committed'] = True
            result['commit_result'] = commit_result[signup_request_id]
        except ValidateException, e:
            logger = netsvc.Logger()
            result['commit_error'] = {'type': 'validation', 'message': e.value}
            logger.notifyChannel('lucera_api_signups', netsvc.LOG_WARNING,
                                 'Could not commit signup {}. \nWarning:\n {}'.format(signup_request_id, str(e)))
            raise
        except except_osv, e:
            logger = netsvc.Logger()
            result['commit_error'] = {'type': 'validation', 'message': e.value}
            logger.notifyChannel('lucera_api_signups', netsvc.LOG_WARNING,
                                 'Could not commit signup {}. \nWarning:\n {}'.format(signup_request_id, str(e)))
            raise
        except Exception, e:
            logger = netsvc.Logger()
            result['commit_error'] = {'type': 'generic', 'message': e.message}
            logger.notifyChannel('lucera_api_signups', netsvc.LOG_ERROR,
                                 'Could not commit signup {}. \nError:\n {}'.format(signup_request_id, str(e)))
            raise

        return result

    @lucera_api_method(body='signup')
    def update_signup_json(self, cursor, uid, signup):
        """Migrate campaign routes only"""
        context = {}

        polissa_obj = self.pool.get('giscedata.polissa')
        signup_request_obj = self.pool.get('lucera.signup.request')
        signup_obj = self.pool.get('lucera.signup')
        signup_commiter = self.pool.get('lucera.signup.committer')

        external_id = signup['id']
        signup_serialized = json.dumps(signup, indent=4, ensure_ascii=False).encode('utf8')
        signup_request_ids = signup_request_obj.search(cursor, uid,
                                                       [('external_id', '=', external_id)], order='id desc')
        if signup_request_ids:
            signup_request_obj.write(cursor, uid, signup_request_ids[0], {'signup_serialized': signup_serialized})

        signup_ids = signup_obj.search(cursor, uid, [('external_id', '=', external_id)], order='id desc')

        if signup_ids:
            signup_obj.write(cursor, uid, signup_ids[0], {'original_signup_serialized': signup_serialized})
            signup_commiter.update_signup_attributions(cursor, uid, signup_ids[0])

        return True

    @lucera_api_method()
    def sign_by_email(self, cursor, uid, electricity_contract_id, gas_contract_id,
                      email, ip, attribution=None, analytics_user_id=None):
        """
        Signs a contract
        :param cursor
        :param uid
        :param electricity_contract_id: The contract_id
        :param gas_contract_id: The contract_id
        :param email: Email that is signing the contract
        :param ip: IP address signing the contract
        :param attribution: JSON string with attribution info
        :param analytics_user_id: user id for analytics reports
        :return:
            {
                'success': True|False,
                'cups_status: 'available|in_progress|active|not_found,
                'cups_id': cups_id?
                'polissa_id': polissa_id?,
            }
        """
        electricity_polissa_obj = self.pool.get('giscedata.polissa')
        gas_polissa_obj = self.pool.get('giscegas.polissa')
        results = {}
        address_id = False
        signup_id = False
        success = False
        notification_partner = False

        try:
            # Sign electricity contract
            if electricity_contract_id:
                results['electricity'] = electricity_polissa_obj.sign_by_email(
                    cursor, uid, electricity_contract_id, email, ip)
                electricity_polissa = electricity_polissa_obj.browse(cursor, uid, electricity_contract_id)
                notification_partner = electricity_polissa.direccio_notificacio.partner_id
                if electricity_polissa.electricity_signups and electricity_polissa.electricity_signups[0].signups:
                    signup_id = electricity_polissa.electricity_signups[0].signups[0].id
                address_id = results['electricity'][electricity_contract_id]['address_id']

            # Sign gas contract
            if gas_contract_id:
                results['gas'] = gas_polissa_obj.sign_by_email(
                    cursor, uid, gas_contract_id, email, ip)
                gas_polissa = gas_polissa_obj.browse(cursor, uid, gas_contract_id)
                if not notification_partner:
                    notification_partner = gas_polissa.direccio_notificacio.partner_id
                if not signup_id:
                    if gas_polissa.electricity_signups and gas_polissa.electricity_signups[0].signups:
                        signup_id = gas_polissa.gas_signups[0].signups[0].id
                if not address_id:
                    address_id = results['gas'][gas_contract_id]['address_id']

            if not notification_partner.analytics_user_id:
                if not analytics_user_id:
                    analytics_user_id = 'default_id_' + str(notification_partner.id)
                analytics_user_id = self.pool.get('res.partner')\
                    .set_analytics_user_id_if_empty(cursor, uid, notification_partner.id, analytics_user_id)
            else:
                analytics_user_id = notification_partner.analytics_user_id

            # Mark email as validated
            address_obj = self.pool.get('res.partner.address')
            address_obj.mark_email_as_validated(cursor, uid, address_id, email)

            # Add signature attribution
            if attribution and signup_id:
                signup_obj = self.pool.get('lucera.signup')
                signup = signup_obj.add_signature_attribution(cursor, uid, signup_id, attribution)

            # Send bus message
            bus = self.pool.get('lucera.bus')
            message = {}
            if electricity_contract_id:
                message['electricity_contract_id'] = electricity_contract_id
            if gas_contract_id:
                message['gas_contract_id'] = gas_contract_id
            if signup_id:
                message['signup_id'] = gas_contract_id
            bus.send(cursor, uid, 'lucera.contracts.signed', message, 'erp')

            # Results
            return {'success': True, 'analytics_user_id': analytics_user_id}

        except osv.except_osv, o:
            # Roll back changes in case one of them where signed
            cursor.rollback()

            if o.name == 'ValidateError' and o.value == 'Already signed':
                error_code = 422  # Already signed
            elif o.name == 'SecurityError':
                error_code = 403  # Forbidden
            else:
                error_code = 500  # Other error is not recognized
            return {'success': False, 'error': '{} - {}'.format(o.name, o.value), 'error_code': error_code}


LuceraApiSignUpsSignUps()


def log(msg, level=netsvc.LOG_INFO):
    logger = netsvc.Logger()
    logger.notifyChannel('oorq', level, msg)
