# -*- coding: utf-8 -*-
{
    "name": "Giscegas Atr Lucera",
    "description": """Mòdul custom de gas ATR de Lucera""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE",
    "depends":[
        "giscegas_atr",
        "giscegas_polissa_motius_canvis"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
    ],
    "active": False,
    "installable": True
}