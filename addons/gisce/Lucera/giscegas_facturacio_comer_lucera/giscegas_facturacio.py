# -*- coding: utf-8 -*-
from osv import osv, fields
from tools import config


class GiscegasFacturacioFactura(osv.osv):
    """Clase per la factura de Lucera."""
    _name = 'giscegas.facturacio.factura'
    _inherit = 'giscegas.facturacio.factura'

    def copy_data(self, cr, uid, id, default=None, context=None):
        if not default:
            default = {}
        default.update({'liquidatable_amount': 0})
        return super(GiscegasFacturacioFactura, self).copy_data(
            cr, uid, id, default, context=context
        )

    def invoice_open(self, cursor, uid, ids, context=None):
        res = super(GiscegasFacturacioFactura, self).invoice_open(
            cursor, uid, ids, context=context
        )
        self_obj = self.pool.get('giscegas.facturacio.factura')
        residuals = self_obj.read(
            cursor, uid, ids, ['signed_residual'], context=context
        )
        for residual in residuals:
            self_obj.write(
                cursor, uid, ids,
                {'liquidatable_amount': residual['signed_residual']}
            )
        return res

    _columns = {
        'liquidatable_amount': fields.float(
            u"Importe liquidable", readonly=True,
            digits=(16, int(config['price_accuracy']))
        ),
    }


GiscegasFacturacioFactura()
