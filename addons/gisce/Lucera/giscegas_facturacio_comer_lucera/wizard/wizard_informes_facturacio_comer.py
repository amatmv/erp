# -*- coding: utf-8 -*-
from tools import config
from giscegas_facturacio.wizard.wizard_informes_facturacio import (
    INFORMES_EXPORTABLES
)

INFORMES_EXPORTABLES.update({
    'export_factures_desglossat': "{}/giscegas_facturacio_comer_lucera/sql/query_resum_comer.sql".format(config['addons_path'])
})
