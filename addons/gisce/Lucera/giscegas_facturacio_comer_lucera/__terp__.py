# -*- coding: utf-8 -*-
{
    "name": "Giscegas Facturació Comer Lucera",
    "description": """Mòdul custom de gas per a la facturació de Lucera""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE",
    "depends":[
        "giscegas_facturacio_comer",
        "giscegas_facturacio_refund_signed"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscegas_facturacio_comer_view.xml",
    ],
    "active": False,
    "installable": True
}
