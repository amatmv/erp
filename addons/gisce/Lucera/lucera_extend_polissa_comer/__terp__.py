# -*- coding: utf-8 -*-
{
  "name": "Lucera polissa customizations",
  "description": """Customize polissa""",
  "version": "0-dev",
  "author": "Lucera",
  "category": "Lucera",
  "depends": ['base',
              'giscedata_facturacio',
              'giscedata_facturacio_comer',
              'giscedata_switching',
              'lucera_common',
              'lucera_polissa_simple_search',
              'lucera_bus'],
  "init_xml": [],
  "demo_xml": [],
  "update_xml": ['giscedata_polissa_view.xml'],
  "active": False,
  "installable": True
}
