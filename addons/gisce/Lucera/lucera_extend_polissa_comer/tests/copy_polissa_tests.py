# -*- coding: utf-8 -*-

import unittest
from destral import testing
from destral.transaction import Transaction
from expects import *
import tools
import netsvc
from lucera_common.fake import get_random_cups_code
from lucera_common.testing import create_test_electricity_polissa


class LuceraBusTests(testing.OOTestCase):
    """ Test lucera bus"""

    def test_copy_polissa_sends_message_if_previous_active_polissa_with_same_cups_does_not_exist(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            pool = self.openerp.pool
            cursor = txn.cursor
            uid = txn.user

            cups_code = get_random_cups_code('0021')
            result = create_test_electricity_polissa(pool, cursor, uid, cups_code=cups_code,
                                                     override_vals={'state': 'cancelada'})
            polissa_id = result['polissa_id']

            # Act
            polissa_obj = pool.get('giscedata.polissa')
            copied_polissa_id = polissa_obj.copy(cursor, uid, polissa_id, default={})

            # Assert
            bus_message_obj = self.openerp.pool.get('lucera.bus.message')
            bus_message_ids = bus_message_obj.search(cursor, uid,
                                                     [('name', '=', 'giscedata.polissa.created')],
                                                     order='create_date desc')

            # There must be TWO messages, one for original and one for the copy
            expect(len(bus_message_ids)).to(equal(2))

    def test_copy_polissa_does_not_sends_message_if_previous_active_polissa_with_same_cups_exists(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            pool = self.openerp.pool
            cursor = txn.cursor
            uid = txn.user

            cups_code = get_random_cups_code('0021')
            result = create_test_electricity_polissa(pool, cursor, uid, cups_code=cups_code)
            polissa_id = result['polissa_id']

            # Act
            polissa_obj = pool.get('giscedata.polissa')
            copied_polissa_id = polissa_obj.copy(cursor, uid, polissa_id, default={})

            # Assert
            bus_message_obj = self.openerp.pool.get('lucera.bus.message')
            bus_message_ids = bus_message_obj.search(cursor, uid,
                                                     [('name', '=', 'giscedata.polissa.created')],
                                                     order='create_date desc')

            # There must be ONE message, one for original and one for the copy
            expect(len(bus_message_ids)).to(equal(1))
