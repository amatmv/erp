# -*- coding: utf-8 -*-
from osv import osv, fields
from lucera_common import config
import re
from tools.translate import _


class GiscedataPolissa(osv.osv):

    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def create(self, cursor, uid, vals, context=None):
        # if 'electricity_signups' in vals:
        #     del(vals['electricity_signups'])
        polissa_id = super(GiscedataPolissa, self).create(cursor, uid, vals, context)
        bus = self.pool.get('lucera.bus')
        polissa = self.browse(cursor, uid, polissa_id)
        # Search for active polissa with same cups
        existing_polissa_ids = None
        if 'cups' in vals:
            existing_polissa_ids = self.search(cursor, uid, [('cups', '=', vals['cups']), ('id', '!=', polissa_id)])
        if not existing_polissa_ids:
            bus.send(cursor, uid, 'giscedata.polissa.created', {'polissa_id': polissa_id}, 'erp')
        return polissa_id

    def _get_manage_url(self, cursor, uid, ids, field_name, arg, context=None):
        res = {}
        for contract_id in ids:
            res[contract_id] = 'https://manage.lucera.es/contracts/{}'.format(contract_id)
        return res

    _columns = {
        'manage_url': fields.function(_get_manage_url,
                                      method=True, type='char',
                                      size=250,
                                      string='Manage',
                                      readonly=True),
        'collective': fields.selection(config.LUCERA_COLLECTIVES_SELECTION, 'Colectivo', required=False, size=32)
    }


GiscedataPolissa()
