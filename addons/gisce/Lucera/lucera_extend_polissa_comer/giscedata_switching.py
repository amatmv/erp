# -*- coding: utf-8 -*-

from osv import osv, fields
from datetime import datetime


class GiscedataSwitchingHelpers(osv.osv):

    _name = 'giscedata.switching.helpers'
    _inherit = 'giscedata.switching.helpers'

    def activa_polissa_from_cn(self, cursor, uid, sw_id, context=None):
        res = super(GiscedataSwitchingHelpers, self).activa_polissa_from_cn(
                      cursor, uid, sw_id, context=context)

        # We send the activation event to the bus
        sw_obj = self.pool.get('giscedata.switching')
        sw = sw_obj.browse(cursor, uid, sw_id)
        bus = self.pool.get('lucera.bus')
        bus.send(cursor, uid, 'giscedata.polissa.activated', {'polissa_id': sw.cups_polissa_id.id}, 'erp')

        return res

    def activar_polissa_from_m1(self, cursor, uid, sw_id, context=None):
        if context is None:
            context = {}

        # Li passem al context que la activació ve des d'ATR
        context.update({'activacio_from_atr': True})

        res = super(GiscedataSwitchingHelpers, self).activar_polissa_from_m1(
                      cursor, uid, sw_id, context=context)

        return res

GiscedataSwitchingHelpers()
