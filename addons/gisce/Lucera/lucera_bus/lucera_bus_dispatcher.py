# -*- coding: utf-8 -*-
import json
import logging
from datetime import datetime

import tools
from transports import get_transport_provider

logger = logging.getLogger('lucera.bus')


class LuceraBusDispatcher(object):
    """Dispatches messages to the correct transport"""

    def __init__(self, pool):
        self.pool = pool

    def _get_namespace(self, message_name):
        """Returns the namespace of a message"""
        index = message_name.rfind('.')
        if index > 0:
            return message_name[:index]
        else:
            return False

    def _get_endpoint_id(self, cr, uid, endpoint_name):
        """Gets the endpoint by its name or """
        endpoint_obj = self.pool.get('lucera.bus.endpoint')

        # If it has a defined endpoint, it must exist
        if endpoint_name:
            endpoint_ids = endpoint_obj.search(cr, uid, [('name', '=', endpoint_name)],
                                               order='create_date desc', limit=1)
            return endpoint_ids[0]

        # Use default endpoint
        endpoint_ids = endpoint_obj.search(cr, uid, [('default', '=', True)],
                                           order='create_date desc', limit=1)
        return endpoint_ids[0]

    def _get_endpoint(self, cr, uid, endpoint_name, context):
        endpoint_obj = self.pool.get('lucera.bus.endpoint')
        endpoint_id = self._get_endpoint_id(cr, uid, endpoint_name)
        return endpoint_obj.read(cr, uid, endpoint_id, [], context)

    def _get_target(self, cr, uid, endpoint_id, message_name, context):
        route_obj = self.pool.get('lucera.bus.route')
        # Search route for this endpoint, message name or namespace
        message_namespace = self._get_namespace(message_name)
        search_params = [('endpoint_id', '=', endpoint_id),
                         '|', ('message_name', '=', message_name), ('message_namespace', '=', message_namespace)]
        order = 'message_name asc'  # Ordered so if a route with message_name exists it will use it
        route_ids = route_obj.search(cr, uid, search_params, order=order, limit=1)
        if route_ids:
            route = route_obj.read(cr, uid, route_ids[0], ['target'])
            return route['target']
        # If no route is found, return default target
        return ''

    def _get_transport(self, provider_name, provider_config):
        provider_type = get_transport_provider(provider_name)
        return provider_type(provider_config)

    def _dispatch_message(self, cr, uid, message, raw=False, context=None):
        """Resolve endpoint, target and transport and put message in transport"""
        endpoint = self._get_endpoint(cr, uid, message.endpoint, context)
        target = message.target or self._get_target(cr, uid, endpoint['id'], message.name, context)
        transport = endpoint['transport']
        config_key = endpoint.get('config_key', '')
        if not config_key:
            config_key = 'bus_{}'.format(transport)
        transport_config = tools.config.get(config_key, False)
        if not transport_config:
            raise Exception('Configuration key {} not found in configuration file'.format(config_key))

        with self._get_transport(transport, transport_config) as t:
            if raw:
                msg = message.content
            else:
                try:
                    content = json.loads(message.content)
                except ValueError as e:
                    content = message.content

                msg = json.dumps({
                    'id': message.id,
                    'name': message.name,
                    'type': message.type,
                    'date': datetime.now().isoformat(),
                    'content': content
                })

            if message.type == 'command':
                t.send(target, msg)
            else:
                t.publish(target, msg)

    def dispatch(self, cr, uid, message_ids, raw=False, context=None):
        """Send the messages """
        if not context:
            context = {}

        now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

        message_obj = self.pool.get('lucera.bus.message')

        for message in message_obj.browse(cr, uid, message_ids, context):
            try:
                if context.get('force', False) or message.send_at_date <= now and message.state == 'pending':
                    self._dispatch_message(cr, uid, message, raw, context)
                    # TODO: Should we delete if they are already sent? Config?
                    message_obj.write(cr, uid, [message.id], {'state': 'sent', 'sent_date': now})
            except Exception as e:
                logger.error(e)
                message_obj.process_error(cr, uid, [message.id], e, context)
                if context.get('raise_exceptions', False):
                    raise e
