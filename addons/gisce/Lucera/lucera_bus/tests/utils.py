# -*- coding: utf-8 -*-

from lucera_bus.transports import LuceraBusBaseTransport
from lucera_bus.lucera_bus import LuceraBusService


class LuceraBusFakeTransport(LuceraBusBaseTransport):
    """Bus fake provider for test"""

    Instance = None

    def __init__(self, config=None):
        self.commands = []
        self.events = []
        LuceraBusFakeTransport.Instance = self

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        return

    def send(self, queue, message):
        self.commands.append((queue, message))

    def publish(self, topic, message):
        self.events.append((topic, message))

    def _contains_message_by_id(self, collection, target, message_id):
        for (target, message) in collection:
            ids = '"id": {}'.format(message_id)
            if ids in message:
                return True
        return False

    def is_sent(self, queue, message_id):
        return self._contains_message_by_id(self.commands, queue, message_id)

    def is_published(self, topic, message_id):
        return self._contains_message_by_id(self.events, topic, message_id)


