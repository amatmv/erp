# -*- coding: utf-8 -*-

import unittest
from destral import testing
from destral.transaction import Transaction
from expects import *
import tools
import netsvc
from utils import LuceraBusFakeTransport
from lucera_bus.transports import register_transport_provider


class LuceraBusTests(testing.OOTestCase):
    """ Test lucera bus"""

    def new_bus_transaction(self):
        txn = Transaction().start(self.database)

        # Register fake transport
        register_transport_provider('fake', LuceraBusFakeTransport)

        # Add fake endpoint if it does not exist
        endpoint_obj = self.openerp.pool.get('lucera.bus.endpoint')
        endpoint_ids = endpoint_obj.search(txn.cursor, txn.user, [('name', '=', 'fake')])
        if not endpoint_ids:
            endpoint_vals = {'name': 'fake', 'transport': 'fake', 'default': True}
            self.endpoint_id = endpoint_obj.create(txn.cursor, txn.user, endpoint_vals)
        else:
            self.endpoint_id = endpoint_ids[0]

        # Fake config
        tools.config['bus_fake'] = 'fake'

        return txn

    def _commit_bus(self, cr):
        cr.commit()

    def test_send_message_stores_the_message(self):
        with self.new_bus_transaction() as txn:
            # Arrange
            pool = self.openerp.pool
            cursor = txn.cursor
            user = txn.user

            # Act
            lucera_bus = pool.get('lucera.bus')
            message_id = lucera_bus.send(cursor, user, 'test.message1', 'content1')

            # Assert
            expect(message_id).to(be_above(0))

            message_obj = pool.get('lucera.bus.message')
            message = message_obj.browse(cursor, user, message_id)
            expect(message.name).to(equal('test.message1'))
            expect(message.content).to(equal('content1'))
            expect(message.type).to(equal('command'))
            expect(message.state).to(equal('pending'))
            expect(message.priority).to(equal(0))
            expect(message.retry_count).to(equal(0))

    def test_publish_message_stores_the_message(self):
        with self.new_bus_transaction() as txn:
            # Arrange
            pool = self.openerp.pool
            cursor = txn.cursor
            user = txn.user

            # Act
            lucera_bus = pool.get('lucera.bus')
            message_id = lucera_bus.publish(cursor, user, 'test.message1', 'content1')

            # Assert
            message_obj = pool.get('lucera.bus.message')
            expect(message_id).to(be_above(0))

            message = message_obj.browse(cursor, user, message_id)
            expect(message.name).to(equal('test.message1'))
            expect(message.content).to(equal('content1'))
            expect(message.type).to(equal('event'))
            expect(message.state).to(equal('pending'))
            expect(message.priority).to(equal(0))
            expect(message.retry_count).to(equal(0))

    def test_send_message_sends_the_message_on_commit(self):
        with self.new_bus_transaction() as txn:
            # Act
            lucera_bus = self.openerp.pool.get('lucera.bus')
            message_id = lucera_bus.send(txn.cursor, txn.user, 'test.message1', 'content1')

            self._commit_bus(txn.cursor)

            # Assert
            message_obj = self.openerp.pool.get('lucera.bus.message')
            expect(message_id).to(be_above(0))
            message = message_obj.browse(txn.cursor, txn.user, message_id)
            expect(message.state).to(equal('sent'))

            transport = LuceraBusFakeTransport.Instance
            expect(transport.is_sent('', message_id)).to(be_true)
            expect(transport.is_published('', message_id)).to(be_false)

    def test_send_message_sends_the_message_to_queue_on_commit(self):
        with self.new_bus_transaction() as txn:
            # Act
            lucera_bus = self.openerp.pool.get('lucera.bus')
            message_id = lucera_bus.send(txn.cursor, txn.user, 'test.message1', 'content1', 'queue1')

            self._commit_bus(txn.cursor)

            # Assert
            message_obj = self.openerp.pool.get('lucera.bus.message')
            expect(message_id).to(be_above(0))
            message = message_obj.browse(txn.cursor, txn.user, message_id, 'queue1')
            expect(message.state).to(equal('sent'))

            transport = LuceraBusFakeTransport.Instance
            expect(transport.is_sent('queue1', message_id)).to(be_true)

    def test_send_message_sends_the_message_to_routed_queue_by_name_on_commit(self):
        with self.new_bus_transaction() as txn:
            # Arrange
            route_obj = self.openerp.pool.get('lucera.bus.route')
            route_vals = {
                'message_name': 'test.message1',
                'target': 'routed-queue',
                'endpoint_id': self.endpoint_id
            }
            route_id = route_obj.create(txn.cursor, txn.user, route_vals)

            # Act
            lucera_bus = self.openerp.pool.get('lucera.bus')
            message_id = lucera_bus.send(txn.cursor, txn.user, 'test.message1', 'content1')

            self._commit_bus(txn.cursor)

            # Assert
            message_obj = self.openerp.pool.get('lucera.bus.message')
            expect(message_id).to(be_above(0))
            message = message_obj.browse(txn.cursor, txn.user, message_id)
            expect(message.state).to(equal('sent'))

            transport = LuceraBusFakeTransport.Instance
            expect(transport.is_sent('routed-queue', message_id)).to(be_true)

    def test_send_message_sends_the_message_to_specified_target_even_if_route_exists(self):
        with self.new_bus_transaction() as txn:
            # Arrange
            route_obj = self.openerp.pool.get('lucera.bus.route')
            route_vals = {
                'message_name': 'test.message1',
                'target': 'routed-queue',
                'endpoint_id': self.endpoint_id
            }
            route_id = route_obj.create(txn.cursor, txn.user, route_vals)

            # Act
            lucera_bus = self.openerp.pool.get('lucera.bus')
            message_id = lucera_bus.send(txn.cursor, txn.user, 'test.message1', 'content1', queue='queue1')

            self._commit_bus(txn.cursor)

            # Assert
            message_obj = self.openerp.pool.get('lucera.bus.message')
            expect(message_id).to(be_above(0))
            message = message_obj.browse(txn.cursor, txn.user, message_id)
            expect(message.state).to(equal('sent'))

            transport = LuceraBusFakeTransport.Instance
            expect(transport.is_sent('queue1', message_id)).to(be_true)

    def test_send_message_sends_the_message_to_routed_queue_by_namespace_on_commit(self):
        with self.new_bus_transaction() as txn:
            # Arrange
            route_obj = self.openerp.pool.get('lucera.bus.route')
            route_vals = {
                'message_namespace': 'test',
                'target': 'routed-queue',
                'endpoint_id': self.endpoint_id
            }
            route_id = route_obj.create(txn.cursor, txn.user, route_vals)

            # Act
            lucera_bus = self.openerp.pool.get('lucera.bus')
            message_id = lucera_bus.send(txn.cursor, txn.user, 'test.message1', 'content1')

            self._commit_bus(txn.cursor)

            # Assert
            message_obj = self.openerp.pool.get('lucera.bus.message')
            expect(message_id).to(be_above(0))
            message = message_obj.browse(txn.cursor, txn.user, message_id)
            expect(message.state).to(equal('sent'))

            transport = LuceraBusFakeTransport.Instance
            expect(transport.is_sent('routed-queue', message_id)).to(be_true)

    def test_send_message_sends_the_routed_queue_by_name_when_name_and_namespace_route_exists(self):
        with self.new_bus_transaction() as txn:
            # Arrange
            route_obj = self.openerp.pool.get('lucera.bus.route')
            route_vals = {
                'message_namespace': 'test',
                'target': 'routed-queue-by-namespace',
                'endpoint_id': self.endpoint_id
            }
            route_obj.create(txn.cursor, txn.user, route_vals)
            route_vals = {
                'message_name': 'test.message1',
                'target': 'routed-queue-by-name',
                'endpoint_id': self.endpoint_id
            }
            route_obj.create(txn.cursor, txn.user, route_vals)

            # Act
            lucera_bus = self.openerp.pool.get('lucera.bus')
            message_id = lucera_bus.send(txn.cursor, txn.user, 'test.message1', 'content1')
            self._commit_bus(txn.cursor)

            # Assert
            message_obj = self.openerp.pool.get('lucera.bus.message')
            expect(message_id).to(be_above(0))
            message = message_obj.browse(txn.cursor, txn.user, message_id)
            expect(message.state).to(equal('sent'))

            transport = LuceraBusFakeTransport.Instance
            expect(transport.is_sent('routed-queue-by-name', message_id)).to(be_true)

    def test_publish_message_publishes_the_message_on_commit(self):
        with self.new_bus_transaction() as txn:
            # Act
            lucera_bus = self.openerp.pool.get('lucera.bus')
            message_id = lucera_bus.publish(txn.cursor, txn.user, 'test.message1', 'content1')

            self._commit_bus(txn.cursor)

            # Assert
            message_obj = self.openerp.pool.get('lucera.bus.message')
            expect(message_id).to(be_above(0))
            message = message_obj.browse(txn.cursor, txn.user, message_id)
            expect(message.state).to(equal('sent'))

            transport = LuceraBusFakeTransport.Instance
            expect(transport.is_sent('', message_id)).to(be_false)
            expect(transport.is_published('', message_id)).to(be_true)

    def test_publish_message_publishes_the_message_to_default_topic_on_commit(self):
        with self.new_bus_transaction() as txn:
            # Act
            lucera_bus = self.openerp.pool.get('lucera.bus')
            message_id = lucera_bus.publish(txn.cursor, txn.user, 'test.message1', 'content1')
            self._commit_bus(txn.cursor)

            # Assert
            message_obj = self.openerp.pool.get('lucera.bus.message')
            expect(message_id).to(be_above(0))
            message = message_obj.browse(txn.cursor, txn.user, message_id, 'topic1')
            expect(message.state).to(equal('sent'))

            transport = LuceraBusFakeTransport.Instance
            expect(transport.is_published('', message_id)).to(be_true)

    def test_publish_message_publishes_the_message_to_topic_on_commit(self):
        with self.new_bus_transaction() as txn:
            # Act
            lucera_bus = self.openerp.pool.get('lucera.bus')
            message_id = lucera_bus.publish(txn.cursor, txn.user, 'test.message1', 'content1', 'topic1')
            self._commit_bus(txn.cursor)

            # Assert
            message_obj = self.openerp.pool.get('lucera.bus.message')
            expect(message_id).to(be_above(0))
            message = message_obj.browse(txn.cursor, txn.user, message_id, 'topic1')
            expect(message.state).to(equal('sent'))

            transport = LuceraBusFakeTransport.Instance
            expect(transport.is_published('topic1', message_id)).to(be_true)
