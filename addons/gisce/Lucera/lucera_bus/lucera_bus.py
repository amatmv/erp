# -*- coding: utf-8 -*-
import logging
import tools
from lucera_bus_service import LuceraBusService
from lucera_bus_dispatcher import LuceraBusDispatcher
from osv import fields, osv
from transports import *

_max_retry_count = 5

logger = logging.getLogger('lucera.bus')


class LuceraBusMessage(osv.osv):
    """Bus messages"""

    def process_error(self, cr, uid, message_ids, exception=None, context=None):
        for message in self.browse(cr, uid, message_ids, context):
            retry_count = message.retry_count + 1
            now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            vals = {
                'retry_count': retry_count,
                'last_retry_date': now,
                'state': 'failed' if retry_count >= _max_retry_count else 'pending',
                'last_exception': tools.exception_to_unicode(exception) if exception else False
            }
            self.write(cr, uid, [message.id], vals, context)

    _name = 'lucera.bus.message'

    _columns = {
        'name': fields.char('Nombre', size=64, required=True),
        'content': fields.text('Contenido', required=True),
        'state': fields.selection([('pending', 'Pending'),
                                   ('sending', 'Sending'),
                                   ('sent', 'Sent'),
                                   ('failed', 'Failed')], 'Estado', required=True),
        'retry_count': fields.integer('Reintentos realizados', readonly=True),
        'last_retry_date': fields.datetime('Fecha último intento'),
        'sending_date': fields.datetime('Fecha comienzo envío'),
        'sent_date': fields.datetime('Fecha enviado'),
        'send_at_date': fields.datetime('Fecha programada envío', required=True),
        'priority': fields.integer('Prioridad'),
        'last_exception': fields.text('Última excepción'),
        'target': fields.char('Topic, Queue where the message is sent', size=32, required=False),
        'endpoint': fields.char('Endpoint where we want to send this', size=32, required=False),
        'type': fields.selection([('command', 'Command'),
                                  ('event', 'Event')], 'Tipo', required=True)
    }

    _defaults = {
        'state': lambda *a: 'pending',
        'retry_count': lambda *a: 0,
        'priority': lambda *a: 0,
        'send_at_date': lambda *a: datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
    }

    _order = 'sent_date DESC'

    def dispatch(self, cr, uid, ids, context=None):
        if not context:
            context = {}
        context['raise_exceptions'] = True
        dispatcher = LuceraBusDispatcher(self.pool)
        dispatcher.dispatch(cr, uid, ids, context=context)


LuceraBusMessage()


class LuceraBus(osv.osv_memory):
    """Sends messages to a bus"""
    _name = 'lucera.bus'

    def _dispatch(self, cr, uid, message_type,
                  message_name, message_content, target=False, endpoint=False, context=None):

        if isinstance(message_content, basestring):
            content = message_content
        else:
            content = json.dumps(message_content)
        vals = {
            'name': message_name,
            'content': content,
            'target': target,
            'endpoint': endpoint,
            'type': message_type
        }
        message_obj = self.pool.get('lucera.bus.message')
        message_id = message_obj.create(cr, uid, vals, context=context)

        LuceraBusService.enqueue(cr, uid, message_id, message_type)

        return message_id

    def send(self, cr, uid, message_name, message_content, queue=False, endpoint=False, context=None):
        """
        :param cr:
        :param uid:
        :param message_name: Full message type and namespace. Example: res.partner.created
        :param message_content: Object to pass as message. If it is not a string it is serialized to json
        :param queue:
        :param endpoint:
        :param context:
        :return:
        """
        return self._dispatch(cr, uid, 'command', message_name, message_content, queue, endpoint, context)

    def publish(self, cr, uid, message_name, message_content, topic=False, endpoint=False, context=None):
        """
        :param cr: Cursor
        :param uid: UserId
        :param message_name: Full message type and namespace. Example: res.partner.created
        :param message_content: Object to pass as message. If it is not a string it is serialized to json
        :param topic: Topic where this message will be sent. If it is not set it will use routing.
        :param endpoint: Endpoint where this message will be sent.
        :param context: Context
        :return:
        """
        return self._dispatch(cr, uid, 'event', message_name, message_content, topic, endpoint, context)


LuceraBus()


class LuceraBusEndPoint(osv.osv):
    """Configured endpoints where we can send messages"""

    _name = 'lucera.bus.endpoint'

    def _get_transport_providers(self, cr, uid, context=None):
        return [(name, name) for name in get_available_transport_providers()]

    _columns = {
        'name': fields.char('Name', size=32),
        'default': fields.boolean('Default', required=True),
        'transport': fields.selection(_get_transport_providers, 'Transport', required=True, size=32),
        'config_key': fields.char('Key to read from config file', size=64)
    }

    _defaults = {
        'default': lambda *a: False
    }

    _sql_constraints = [
        ('name_uniq', 'unique(name)', 'This endpoint name already exists')
    ]


LuceraBusEndPoint()


class LuceraBusRoute(osv.osv):
    """Routes messages to endpoints"""

    _name = 'lucera.bus.route'

    _columns = {
        'message_name': fields.char('Nombre completo del mensaje', size=64, required=False),
        'message_namespace': fields.char('Prefijo del mensaje', size=64, required=False),
        'target': fields.char('Topic, Queue where the message is sent', size=32, required=True),
        'endpoint_id': fields.many2one('lucera.bus.endpoint', string='Endpoint', required=True),
    }

    def _check_method(self, cr, uid, ids, context=None):

        for route in self.browse(cr, uid, ids):
            if not route.message_name and not route.message_namespace:
                return False
                # if route.message_name and route.message_namespace:
                #     return False
        return True

    _constraints = [
        (_check_method,
         'Error! You must specify message name or message namespace but not both',
         ['message_name', 'message_namespace'])
    ]


LuceraBusRoute()
