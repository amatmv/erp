# coding=utf-8
import logging
import pooler
from datetime import datetime, timedelta
from osv import osv

logger = logging.getLogger('lucera.bus')


class LuceraBusCron(osv.osv):
    """Cron jobs to monitors messages to ensure they are sent and other manteninance """
    _name = 'lucera.bus.cron'

    def process_pending_messages(self, cr, uid, context=None):
        """Searchs for pending messages that must be sent and sends them"""
        if context is None:
            context = {}

        res_config = self.pool.get('res.config')
        pending_timeout = int(res_config.get(cr, uid, 'lucera.bus.pending_timeout_in_seconds', 15))
        max_pending_messages = int(res_config.get(cr, uid, 'lucera.bus.max_pending_messages_per_batch', 1000))

        now = datetime.now() + timedelta(seconds=-pending_timeout)
        filters = [('send_at_date', '<=', now),
                   ('state', '=', 'pending')]
        order = "priority desc, send_at_date asc"

        message_obj = self.pool.get('lucera.bus.message')
        message_ids = message_obj.search(cr, uid, filters,
                                         limit=max_pending_messages, order=order,
                                         context=context)

        db = pooler.get_db_only(cr.dbname)
        cr_tmp = db.cr()
        try:
            self.write(cr_tmp, uid, message_ids, {'state': 'sending', 'sending_date': now}, context)
            cr_tmp.commit()
        except Exception as e:
            logger.error(e)
            cr_tmp.rollback()
        finally:
            cr_tmp.close()

        message_obj.dispatch(cr, uid, message_ids, context)

        return True

    def process_sending_timeouts(self, cr, uid, context=None):
        """Search for messages that are in sending state that have timeout out and mark them as errors"""
        if context is None:
            conext = {}
        now = datetime.now()
        res_config = self.pool.get('res.config')
        sending_timeout = int(res_config.get(cr, uid, 'lucera.bus.sending_timeout_in_seconds', 120))
        sending_timeout = now + timedelta(seconds=-sending_timeout)
        filters = [('state', '=', 'sending'), ('sending_date', '<=', sending_timeout)]
        message_obj = self.pool.get('lucera.bus.message')
        message_ids = message_obj.search(cr, uid, filters, context)
        message_obj.process_error(cr, uid, message_ids, context)

    def clean_old_messages(self, cr, uid, context=None):
        """Search for messages that have been sent and delete them to free resources from DB"""
        if context is None:
            conext = {}
        # TODO
        return


LuceraBusCron()
