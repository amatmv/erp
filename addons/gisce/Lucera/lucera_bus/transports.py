# -*- coding: utf-8 -*-
from datetime import datetime
import json

_AVAILABLE_TRANSPORT_PROVIDERS = {}


def register_transport_provider(type, cls):
    """
    Register a transport
    :param type:
    :param cls:
    :return:
    """

    _AVAILABLE_TRANSPORT_PROVIDERS[type] = cls


def get_transport_provider(type):
    return _AVAILABLE_TRANSPORT_PROVIDERS[type]


def get_available_transport_providers():
    return _AVAILABLE_TRANSPORT_PROVIDERS


class LuceraBusBaseTransport(object):
    """ Base dispatcher interface """

    def __init__(self, config=None):
        pass

    def __enter__(self):
        raise NotImplementedError()

    def __exit__(self, exc_type, exc_val, exc_tb):
        raise NotImplementedError()

    def send(self, queue, message):
        """
        :param queue:
        :param message: Serialized queue
        :return:
        """
        raise NotImplementedError()

    def publish(self, topic, message):
        raise NotImplementedError()
