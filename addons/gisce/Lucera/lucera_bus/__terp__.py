# -*- coding: utf-8 -*-
{
  "name": "Lucera Bus",
  "description": """Publish messages to a bus""",
  "version": "0-dev",
  "author": "Lucera",
  "category": "Lucera",
  "depends": [],
  "init_xml": [],
  "demo_xml":   ['lucera_bus_demo.xml'],
  "update_xml": ['security/lucera_bus.xml',
                 'security/ir.model.access.csv',
                 'lucera_bus_data.xml',
                 'lucera_bus_view.xml'],
  "active": False,
  "installable": True
}
