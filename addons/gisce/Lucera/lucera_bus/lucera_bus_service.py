# -*- coding: utf-8 -*-
from osv import fields, osv
from tools.misc import cache
from datetime import datetime, timedelta
import pooler
import tools
import logging
import json
import transports
from lucera_bus_dispatcher import LuceraBusDispatcher
from signals import DB_CURSOR_COMMIT, DB_CURSOR_ROLLBACK
import netsvc

logger = logging.getLogger('lucera.bus')


class LuceraBusService(object):
    """ Service that waits for cr.commit() and after that tries to send messages to the configured endpoints"""

    BUS_QUEUE = {}

    @staticmethod
    def rollback(cursor):
        transaction_id = id(cursor)
        messages = LuceraBusService.BUS_QUEUE.pop(transaction_id, [])
        if messages:
            log('Cancelling {} messages from rollback of transaction {}'.format(
                len(messages), transaction_id
            ))

    @staticmethod
    def commit(cursor):
        transaction_id = id(cursor)
        messages = LuceraBusService.BUS_QUEUE.pop(transaction_id, [])
        if messages:
            try:
                dbname = cursor.dbname
                dispatcher = LuceraBusDispatcher(pooler.get_pool(dbname))
                for message in sorted(messages, key=lambda t: t[0]):
                    cr = pooler.get_db(dbname).cursor()
                    message_id, message_type, uid = message
                    try:
                        dispatcher.dispatch(cr, uid, [message_id], context=None)
                        cr.commit()
                        messages.discard(message)
                    except Exception as e:
                        log('Could not send bus message {}'.format(message_id), level=netsvc.LOG_ERROR)
                    finally:
                        cr.close()
            except Exception as e:
                log('Could not commit bus messages'.format(message_id), level=netsvc.LOG_ERROR)


    @classmethod
    def enqueue(cls, cursor, uid, message_id, message_type):
        transaction_id = id(cursor)
        cls.BUS_QUEUE.setdefault(transaction_id, set())
        cls.BUS_QUEUE[transaction_id].add((message_id, message_type, uid))


DB_CURSOR_COMMIT.connect(LuceraBusService.commit)

DB_CURSOR_ROLLBACK.connect(LuceraBusService.rollback)


def log(msg, level=netsvc.LOG_INFO):
    logger = netsvc.Logger()
    logger.notifyChannel('oorq', level, msg)