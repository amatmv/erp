Lucera Bus
==========

This module allows sending messages to a bus with "at least one" strategy. It creates a message in the database
in a pending state and after the transaction is finished, tries to send it to the configured bus. If it fails, there is
a cron job that makes manteinance doing retries with this messages.

There are two kinds of messages:

**Commands**. Are sent to a single endpoint to be processed
**Events**. Are sent to a topic and multiple endpoints can be listening there.

All events must have a full type (including namespace) and a content. If the content is not a string, it is serialized
with json

Sending and publishing messages
-------------------------------

To send a command to the 'erp' queue:

.. code:: python
    bus = pool.get('lucera.bus')
    bus.send(cursor, uid, 'res.partner.created', '23', 'erp')

To publish an event to the 'erp' topic:

.. code:: python
    bus = pool.get('lucera.bus')
    bus.publish(cursor, uid, 'res.partner.created', '23', 'erp')


Routing
-------

Instead of setting manually the queues and topics they can set by configuration allowing more flexibility.

The routes can match messages by their full name or namespace (the part of the name of the message before the last dot).
Only one route is matched.

For example, to route all events from the res.partner we can create this route.

.. code:: python
    route_obj = pool.get('lucera.bus.route')
    route_obj.create(cursor, uid, { 'message_namespace': 'res.partner', 'target': 'partners_topic', 'endpoint_id': 1})

Then, all the events that are in the message namespace 'res.partner' are sent to the endpoint 1 and the topic
'partners_topic' and you don't have to specify it manually.

.. code:: python
    bus = pool.get('lucera.bus')
    bus.publish(cursor, uid, 'res.partner.created', '23')


Setup
-----
* Install lucera_bus module
* Install required transports (i.e. lucera_bus_azurequeuestorage)
* Add to server.conf file the required settings of each transport
* Add current user to the groups BUS Manager, BUS ReadOnnly and BUS User
* Open erp client, (Ctrl T+R to reset menus and views),  Administration > Bus
* Endpoints:
   * Create default endpoint: name: doesn't matter, check default, choose transport, key (leave empty)
   * Routes: not needed