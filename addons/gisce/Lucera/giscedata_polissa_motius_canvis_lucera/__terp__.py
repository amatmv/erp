# -*- coding: utf-8 -*-
{
    "name": "Motius de canvi de Lucera",
    "description": """Mòdul relatiu als motius de canvi de Lucera""",
    "version": "0-dev",
    "author": "Gisce",
    "category": "Lucera",
    "depends":[
        "giscedata_polissa_motius_canvis"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_polissa_motius_canvis_data.xml"
    ],
    "active": False,
    "installable": True
}
