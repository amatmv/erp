# -*- coding: utf-8 -*-
{
    "name": "Lucera polissa module.",
    "description": """Lucera polissa module.""",
    "version": "0-dev",
    "author": "GISCE-TI",
    "category": "GISCE-TI",
    "depends":[
        "giscedata_facturacio"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[],
    "active": False,
    "installable": True
}