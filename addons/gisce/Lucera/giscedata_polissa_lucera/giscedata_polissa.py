# -*- coding: utf-8 -*-

from osv import osv
from tools.translate import _


class GiscedataPolissa(osv.osv):

    _name = "giscedata.polissa"
    _inherit = "giscedata.polissa"

    def onchange_altre_pagador(self, cursor, uid, ids, altre_pagador,
                               notificacio, context=None):
        """
        Sobrecarrega el mètode que es declara al mòdul de facturació.
        Si el pagador (raó fiscal) no és el mateix que el titular llavors
        llençarà un warning.
        """
        res = super(GiscedataPolissa, self).onchange_altre_pagador(
            cursor, uid, ids, altre_pagador, notificacio, context=context
        )
        titular_id = self.read(cursor, uid, ids, ['titular'])[0]['titular']
        if altre_pagador and altre_pagador != titular_id[0]:
            res['warning'] = {
                'title': "Warning",
                'message': _(u"La raó fiscal ha de ser el mateix que "
                             u"el titular")
            }
        return res


GiscedataPolissa()
