# -*- coding: utf-8 -*-
{
    "name": "Invoice debt reporting information",
    "description": """Allows to mark an invoice to be taken in account when reporting partners to debt listings""",
    "version": "0-dev",
    "author": "Lucera",
    "category": "Lucera",
    "depends":[
        "base",
        "giscedata_polissa",
        "giscedata_facturacio_comer",
        "giscedata_polissa_comer"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "giscedata_facturacio_comer_view.xml"
    ],
    "active": False,
    "installable": True
}
