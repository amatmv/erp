# -*- coding: utf-8 -*-

from osv import osv, fields


class GiscedataFacturacioFactura(osv.osv):
    """Class to assign a debt recovery partner."""
    _name = 'giscedata.facturacio.factura'
    _inherit = 'giscedata.facturacio.factura'

    _columns = {
        'debt_report_include':  fields.boolean('Incluir en informe de morosidad'),
        'debt_report_include_date': fields.date('Fecha de inclusión en informe', select=True),
    }


GiscedataFacturacioFactura()