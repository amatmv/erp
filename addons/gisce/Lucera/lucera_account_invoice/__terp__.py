# -*- coding: utf-8 -*-
{
    "name": "Account invoice Lucera",
    "description": """Creates the customizations for account invoice""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Lucera",
    "depends":[
        "base",
        "l10n_ES_aeat_sii",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "account_invoice_view.xml",
    ],
    "active": False,
    "installable": True
}
