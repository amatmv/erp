# -*- coding: utf-8 -*-

from osv import osv, fields

class GiscegasFacturacioFactura(osv.osv):
    """Class to assign a debt recovery partner."""
    _name = 'giscegas.facturacio.factura'
    _inherit = 'giscegas.facturacio.factura'

    _columns = {
        'debt_recovery_partner_id':  fields.many2one('res.partner', 'Gestión recobro',
                                      domain=[('category_id.name', '=', 'Recobro')],
                                      select=True,
                                      change_default=True,
                                      readonly=False,
                                      required=False,
                                      index=True),
        'debt_recovery_partner_date': fields.date('Fecha asignación recobro', select=True),
    }


GiscegasFacturacioFactura()