# lucera_gas_facturacio_comer_debtpartner

This module adds two fields to allow managing the invoice 
debt recovery by a external partner that must have the category 'Recobro' assigned.


## Fields and entities

### giscegas.facturacio.factura*

- **debt_recovery_partner_id**  Partner assigned for recovering the debt 
- **debt_recovery_partner_date** Date when the parterner was assigned





