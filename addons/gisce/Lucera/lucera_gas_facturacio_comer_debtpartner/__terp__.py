# -*- coding: utf-8 -*-
{
    "name": "Gas invoice debt partner id",
    "description": """Allows to assign a debt recovery partner to every gas invoice""",
    "version": "0-dev",
    "author": "Lucera",
    "category": "Lucera",
    "depends":[
        "base",
        "giscegas_polissa",
        "giscegas_facturacio_comer",
        "giscegas_polissa_comer",
        "giscedata_facturacio_comer_lucera_debtpartner"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscegas_facturacio_comer_view.xml"
    ],
    "active": False,
    "installable": True
}
