# -*- coding: utf-8 -*-
from osv import osv
from osv.expression import OOQuery


class GiscedataPolissa(osv.osv):
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def pre_canceling_behaviour(self, cursor, uid, ids, context=None):
        """
        Comportamiento deseado antes de cancelar el contrato.
        En este caso, interesa asignar el motivo de cancelación: "desestimiento
        del cliente" al contrato.
        :returns: None
        """
        if context is None:
            context = {}
        if not isinstance(ids, (tuple, list)):
            ids = [ids]

        # Crida a super perquè les classes pare facin el que vulguin
        res = super(GiscedataPolissa, self).pre_canceling_behaviour(
            cursor, uid, ids, context=context
        )

        # Cancelarem la pòlissa encara que no estigui en estat esborrany
        motiu_canvi_obj = self.pool.get('giscedata.motiu.canvi')
        pol_motiu_canvi_obj = self.pool.get('giscedata.polissa.motiu.canvi')
        motiu_canvi_ids = motiu_canvi_obj.search(cursor, uid, [
            ('codi', '=', 'CDO'), ('type', '=', 'cancelacio')
        ])
        motiu_canvi_id = False
        if motiu_canvi_ids:
            motiu_canvi_id = motiu_canvi_ids[0]
        else:
            return

        q = OOQuery(pol_motiu_canvi_obj, cursor, uid)
        q_sql = q.select(['polissa_id']).where([
            ('motiu_id', '=', motiu_canvi_id),
            ('polissa_id', 'in', ids)
        ])
        cursor.execute(*q_sql)
        existing_relations_pol_ids = [res[0] for res in cursor.fetchall()]
        ids = list(set(ids) - set(existing_relations_pol_ids))

        for polissa_id in ids:
            vals = {
                'tipus_motiu': 'cancelacio',
                'motiu_id': motiu_canvi_id,
                'polissa_id': polissa_id,
            }

            pol_motiu_canvi_obj.create(cursor, uid, vals)

GiscedataPolissa()
