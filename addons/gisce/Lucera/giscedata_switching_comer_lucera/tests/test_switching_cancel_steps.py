# -*- coding: utf-8 -*-
from __future__ import absolute_import

from addons import get_module_resource
from destral import testing
from destral.transaction import Transaction
from giscedata_switching.tests.common_tests import TestSwitchingImport
from osv.osv import except_osv
import json

power_invoice_modes = {'1': 'icp', '2': 'max'}


class TestSwitchingCancelSteps(TestSwitchingImport):
    def test_cn_09_create_motivo_cancelacion_and_cancel_contract(self):
        sw_ayudame_obj = self.openerp.pool.get('giscedata.switching.helpers')
        motiu_canvi_obj = self.openerp.pool.get('giscedata.motiu.canvi')
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        pol_motiu_canvi_obj = self.openerp.pool.get(
            'giscedata.polissa.motiu.canvi'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            # Remove invoices related with contract
            fact_ids = fact_obj.search(cursor, uid, [])
            fact_obj.unlink(cursor, uid, fact_ids)

            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.activar_polissa_CUPS(txn)

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'C1', '09'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.c1.09')

            c109 = step_obj.browse(cursor, uid, step_id)

            lucera_motiu_id = motiu_canvi_obj.create(
                cursor, uid,
                {
                    'codi': 'CDO',
                    'name': 'malacatones',
                    'type': 'cancelacio'
                }
            )

            self.assertTrue(lucera_motiu_id)

            response = sw_ayudame_obj.cancelar_polissa_from_cn(
                cursor, uid, c109.sw_id.id, {}
            )

            expected_response = ('OK', u'La p\xf3lissa de cas  Proc\xe9s '
                                       u'C1 pel cups: ES1234000000000001JN0F '
                                       u's\'ha cancel.lat correctament.'
                                 )

            self.assertEqual(response, expected_response)

            relation_created = pol_motiu_canvi_obj.search(
                cursor, uid, [('polissa_id', '=', contract_id),
                              ('motiu_id', '=', lucera_motiu_id),
                              ('tipus_motiu', '=', 'cancelacio')])
            self.assertTrue(relation_created)
