# -*- coding: utf-8 -*-
{
    "name": "Módulo de Switching de Lucera",
    "description": """Módulo personalizado de Switching de Lucera""",
    "version": "0-dev",
    "author": "GISCE-TI",
    "category": "Lucera",
    "depends": [
        "giscedata_switching",
        "giscedata_polissa_motius_canvis"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}