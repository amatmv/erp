# -*- coding: utf-8 -*-
{
    "name": "lucera_switching",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Calcul de lloguers per ATR
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Lucera",
    "depends":[
        "giscedata_switching_comer",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
    ],
    "active": False,
    "installable": True
}
