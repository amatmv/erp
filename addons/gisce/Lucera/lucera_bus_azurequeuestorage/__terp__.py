# -*- coding: utf-8 -*-
{
  "name": "Lucera Bus Azure Queue Transport",
  "description": """Publish messages to Azure Queue Storage""",
  "version": "0-dev",
  "author": "Lucera",
  "category": "Lucera",
  "depends": ['lucera_bus'],
  "init_xml": [],
  "demo_xml":   [],
  "update_xml": [],
  "active": False,
  "installable": True
}
