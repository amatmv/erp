# -*- coding: utf-8 -*-

from lucera_bus.transports import LuceraBusBaseTransport, register_transport_provider
from azure.storage.queue import QueueService, QueueMessageFormat
import json


class LuceraBusAzureQueueStorageTransport(LuceraBusBaseTransport):
    """ Azure Queue Storage Transport """

    def __init__(self, config=None):
        if not config:
            raise Exception('No configuration has been passed to azure queue storage transport')

        super(LuceraBusAzureQueueStorageTransport, self).__init__(config)

        self.config = config
        c = self._parse_config(config)
        account_name = c['account_name']
        account_key = c['account_key']
        is_emulated = c.get('is_emulated', 'False').lower() == 'true'
        self.queue_service = QueueService(account_name=account_name, account_key=account_key, is_emulated=is_emulated)
        self.queue_service.encode_function = QueueMessageFormat.text_base64encode
        self.queue_service.decode_function  = QueueMessageFormat.text_base64decode

    def _parse_config(self, value):
        c = {}
        for key_value in value.split(';'):
            key = key_value[:key_value.find('=')]
            value = key_value[key_value.find('=')+1:]
            c[key] = value
        return c
            

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def send(self, queue, message):
        if isinstance(message, str):
            message = unicode(message)
        self.queue_service.put_message(queue, message)

    def publish(self, topic, message):
        if isinstance(message, str):
            message = unicode(message)
        self.queue_service.put_message(topic, message)


# We register the provider
register_transport_provider('azurequeuestorage', LuceraBusAzureQueueStorageTransport)
