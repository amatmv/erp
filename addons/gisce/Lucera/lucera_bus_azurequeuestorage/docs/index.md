Lucera Bus - Azure Queue Storage Transport
========================================

This module dependes in lucera_bus module and allows sending messages to Azure Queue Storage.

It does not allow to publish events.

Queues must be created manually.

Azure Queue Storage does not have support for "topic" only for queues.

Transport configuration
-----------------------

You have to create the endpoint in lucera_bus erp module and tell what config_key of server.conf erp settings file must use.
If no config key is set, then it uses bus_azurequeuestorage key.

bus_azurequeuestorage=account_name=test;account_key=my_secrete_key

If you want to test locally with azure storage emulator, you can set this config value in the config file

bus_azurequeuestorage = account_name=devstoreaccount1;account_key=Eby8vdM02xNOcqFlqUwJPLlmEtlCDXJ1OUzFT50uSRZ6IFsuFq2UVErCz4I6tq/K1SZFPTOtr/KBHBeksoGMGw==;is_emulated=True
