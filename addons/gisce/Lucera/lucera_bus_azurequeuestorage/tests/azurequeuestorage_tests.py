# -*- coding: utf-8 -*-
import unittest
from destral import testing
from destral.transaction import Transaction
from expects import *
import tools


@unittest.skip('This are integration tests and should be executed manually')
class LuceraBusAzureQueueStorageTests(testing.OOTestCase):
    """ Test that a polissa cannot be activated in multiple cases"""

    def new_bus_transaction(self):
        txn = Transaction().start(self.database)

        # Add fake endpoint if it does not exist
        endpoint_obj = self.openerp.pool.get('lucera.bus.endpoint')
        endpoint_obj.unlink(txn.cursor, txn.user, endpoint_obj.search(txn.cursor, txn.user, [('name', '=', 'azurequeuestorage-test')]))
        endpoint_ids = endpoint_obj.search(txn.cursor, txn.user, [('name', '=', 'azurequeuestorage-test')])
        if not endpoint_ids:
            self.endpoint_id = endpoint_obj.create(txn.cursor, txn.user,
                                                   {'name': 'azurequeuestorage-test',
                                                    'transport': 'azurequeuestorage',
                                                    'default': True,
                                                    'config_key': 'bus_azurequeuestorage'})
            config = 'account_name=devstoreaccount1;' \
                     'account_key=Eby8vdM02xNOcqFlqUwJPLlmEtlCDXJ1OUzFT50uSRZ6IFsuFq2UVErCz4I6tq/K1SZFPTOtr/KBHBeksoGMGw==;' \
                     'is_emulated=True'
            tools.config['bus_azurequeuestorage'] = config
        else:
            self.endpoint_id = endpoint_ids[0]

        return txn

    def test_send_message(self):
        with self.new_bus_transaction() as txn:
            # Arrange
            pool = self.openerp.pool
            cursor = txn.cursor
            user = txn.user

            # Act
            lucera_bus = pool.get('lucera.bus')
            message_id = lucera_bus.send(cursor, user, 'test.message1', 'Hello World!', queue='tests')
            cursor.commit()

            # Assert
            expect(message_id).to(be_above(0))

            message_obj = pool.get('lucera.bus.message')
            message = message_obj.browse(cursor, user, message_id)
            expect(message.name).to(equal('test.message1'))
            expect(message.content).to(equal('Hello World!'))
            expect(message.type).to(equal('command'))
            expect(message.state).to(equal('sent'))

    def test_publish_message(self):
        with self.new_bus_transaction() as txn:
            # Arrange
            pool = self.openerp.pool
            cursor = txn.cursor
            user = txn.user

            # Act
            lucera_bus = pool.get('lucera.bus')
            message_id = lucera_bus.publish(cursor, user, 'test.message1', 'Hello World!', topic='tests')
            cursor.commit()

            # Assert
            expect(message_id).to(be_above(0))

            message_obj = pool.get('lucera.bus.message')
            message = message_obj.browse(cursor, user, message_id)
            expect(message.name).to(equal('test.message1'))
            expect(message.content).to(equal('Hello World!'))
            expect(message.type).to(equal('event'))

            # We cannot publish events to azure queue storage
            expect(message.state).to(equal('pending'))







