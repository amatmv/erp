# -*- coding: utf-8 -*-

from lucera_bus.transports import LuceraBusBaseTransport, register_transport_provider
import json
import pika


class LuceraBusRabbitMQTransport(LuceraBusBaseTransport):
    """  RabbitMQ Bus Transport """

    def __init__(self, config=None):
        if not config:
            raise Exception('No configuration has been passed to rabbitmq transport')

        super(LuceraBusRabbitMQTransport, self).__init__(config)
        self.config = config
        self.connection_params = pika.URLParameters(self.config)

    def __enter__(self):
        self.connection = pika.BlockingConnection(self.connection_params)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.connection.close()

    def send(self, queue, message):
        channel = self.connection.channel()  # start a channel
        channel.queue_declare(queue=queue)  # Declare a queue
        channel.basic_publish(exchange='', routing_key=queue, body=message)

    def publish(self, topic, message):
        raise NotImplementedError('Publish not implemented for RabbitMQ')


# We register the provider
register_transport_provider('rabbitmq', LuceraBusRabbitMQTransport)
