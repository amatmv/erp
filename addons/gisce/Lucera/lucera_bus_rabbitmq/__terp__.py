# -*- coding: utf-8 -*-
{
  "name": "Lucera Bus RabbitMQ",
  "description": """Publish bus messages to RabbitMQ""",
  "version": "0-dev",
  "author": "Lucera",
  "category": "Infrastructure",
  "depends": ['lucera_bus'],
  "init_xml": [],
  "demo_xml":   [],
  "update_xml": [],
  "active": False,
  "installable": True
}
