Lucera Bus - RabbitMQ Transport
========================================

This module dependes in lucera_bus module and allows sending messages to RabbitMQ

Transport configuration
-----------------------

The configuration is made with a connection string.

**Configuration example**:

amqp://guest:guest@localhost/%2f
