# -*- coding: utf-8 -*-
from lucera_common.validate import *
import dateutil
from osv.orm import ValidateException


def get_country_id_by_code(pool, cursor, uid, code):
    country_obj = pool.get('res.country')
    return country_obj.search(cursor, uid, [('code', '=', code)])[0]


def get_municipi_by_ine_code(pool, cursor, uid, ine_code):
    if not ine_code:
        return False
    municipi_obj = pool.get('res.municipi')
    municipi_ids = municipi_obj.search(cursor, uid, [('ine', '=', ine_code)])
    if municipi_ids:
        return municipi_obj.browse(cursor, uid, municipi_ids[0])
    return None


def get_street_type_id(pool, cursor, uid, street_type_code):
    if street_type_code:
        tpv_obj = pool.get('res.tipovia')
        search = [('codi', '=', street_type_code)]
        tpv_id = tpv_obj.search(cursor, uid, search)
        if tpv_id:
            return tpv_id[0]
        else:
            return False
            # raise osv.except_osv(_('ValidateError'), 'Could not find tipovia.code {}'.format(street_type_code))
    return False


def get_cnae_id_by_code(pool, cursor, uid, cnae_code):
    """Gets the cnae code from the fiscal_customer type and the primary residence"""
    cnae_obj = pool.get('giscemisc.cnae')
    cnae_id = cnae_obj.search(cursor, uid, [('name', '=', cnae_code)])
    if not cnae_id:
        raise Exception('Invalid cnae code')
    return cnae_id[0]


def map_to_erp_location(pool, cursor, uid, location):
    ine_code = location.get('town_code', False)
    id_municipi = False
    state_id = False
    if ine_code:
        municipi = get_municipi_by_ine_code(pool, cursor, uid, ine_code)
        if not municipi:
            raise ValidateException('ValidateError',
                                    'No existe el municipo con código ine {} en el ERP!'.format(ine_code))
        id_municipi = municipi.id
        state_id = municipi.state.id

    postal_code = normalize_postal_code(location.get('postal_code', False))
    country_id = get_country_id_by_code(pool, cursor, uid, 'ES')

    address_location = {
        'zip': postal_code,
        'id_municipi': id_municipi,
        'state_id': state_id,
        'country_id': country_id
    }

    address = location.get('address', False)
    if address:
        if isinstance(address, basestring):
            address_location['street'] = normalize_street_name(address)
        else:
            address_location['nv'] = normalize_street_name(address.get('street_name', False)) or False
            address_location['pnp'] = address.get('number', False) or False
            address_location['es'] = address.get('stair', False) or False
            address_location['pt'] = address.get('floor', False) or False
            address_location['pu'] = address.get('door', False) or False
            address_location['aclarador'] = address.get('clarifying', False) or False
            address_location['tv'] = get_street_type_id(pool, cursor, uid, address.get('street_type_code', False))

    return address_location


def convert_date(s):
    if not s:
        return False
    d = dateutil.parser.parse(s)
    return d.strftime('%Y-%m-%d %H:%M:%S')
