# -*- coding: utf-8 -*-
from osv import osv, fields
from datetime import datetime
from utils import *
import json


class LuceraSignUp(osv.osv):
    """Class to store signup data."""
    _name = 'lucera.signup'

    _AUTHOR_ROLES = [('user', 'Cliente'),
                     ('operator', 'Operador'),
                     ('anonymous', 'Anónimo')]

    def add_signature_attribution(self, cursor, uid, signup_id, attribution):
        """Adds the signature attribution of a contract"""
        if not attribution:
            return False

        signup = self.browse(cursor, uid, signup_id)

        if not signup:
            return False
        vals = {
            'signature_attribution_serialized': attribution
        }
        self.write(cursor, uid, signup_id, vals)

        if not signup.attributions:
            self.pool.get('lucera.signup.committer').update_signup_attributions(cursor, uid, signup_id)

        return True

    _rec_name = 'external_id'

    _columns = {
        'date': fields.datetime('Fecha alta', required=True, select=True),
        'external_id': fields.char('Identificador externo del alta', size=36, required=False),
        
        # Relations
        'electricity_signup_id': fields.many2one('lucera.signup.electricity', 'Alta electricidad', readonly=True),
        'gas_signup_id': fields.many2one('lucera.signup.gas', 'Alta gas', readonly=True),

        # External data
        'original_signup_serialized': fields.text('Datos del alta original serializados'),

        # Via
        'via_channel': fields.char('Canal', size=32),
        'via_lead_id': fields.char('Lead id', size=32),
        'via_lead_type': fields.char('Lead type', size=32),
        'via_invoice_study_id': fields.char('Invoice study id', size=32),
        'via_invoice_study_date': fields.datetime('Invoice study date'),
        'via_invoice_study_electricity_recommendation_power': fields.integer('Invoice study recommendation power'),
        'via_invoice_study_electricity_recommendation_tariff_code': fields.char('Invoice study recommendation tariff',
                                                                                size=4),
        'via_personal_study_id': fields.char('Study Id', size=32),
        'via_personal_study_date': fields.datetime('Personal study date'),
        'via_personal_study_electricity_recommendation_power': fields.integer('Personal study recommendation power'),
        'via_personal_study_electricity_recommendation_tariff_code': fields.char('Personal study recommendation tariff',
                                                                                 size=4),

        # Signup attribution
        'signature_attribution_serialized': fields.text('Atribución de la firma serializados'),

        # Attributions
        'attributions': fields.one2many('lucera.signup.attribution', 'signup_id', 'Atribuciones'),

        # UI
        'electricity_polissa_id': fields.related('electricity_signup_id', 'polissa_id', type='many2one',
                                                 relation='giscedata.polissa', string='Póliza electricidad',
                                                 readonly=True),
        'electricity_gas_id': fields.related('gas_signup_id', 'polissa_id', type='many2one',
                                                 relation='giscegas.polissa', string='Póliza gas',
                                                 readonly=True),
        'electricity_cups': fields.related('electricity_signup_id', 'polissa_id', 'cups', 'name', type='char',
                                           relation='giscedata.cups.ps', string='CUPS Electricidad', readonly=True),
        'gas_cups': fields.related('gas_signup_id', 'polissa_id', 'cups', 'name', type='char',
                                           relation='giscegas.cups.ps', string='CUPS Gas', readonly=True),

    }

    _defaults = {
        'date': lambda *a: datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    }

    _order = 'date desc'

    _sql_constraints = [('unique_external_id', 'unique (external_id)',
                         u'Ya existe un identificador externo para esta alta')
                        ]


LuceraSignUp()


class LuceraSignUpAttribution(osv.osv):

    _name = 'lucera.signup.attribution'

    _columns = {
        'date': fields.datetime('Fecha'),
        'source': fields.char('Fuente', size=32),
        'medium': fields.char('Medio', size=64),
        'term': fields.char('Attribution term', size=64),
        'content': fields.char('Content', size=64),
        'name': fields.char('Name', size=64),
        'identifier': fields.char('identifier', size=128),
        'page_path': fields.char('Page path', size=128),
        'index': fields.integer('Posición', required=True),
        'is_first': fields.boolean('Primera atribución'),
        'is_last': fields.boolean('Última atribución'),
        'signup_id': fields.many2one('lucera.signup', 'Alta')
    }

    _rec_name = 'index'

    _order = 'signup_id asc, index asc'

    def parse_from_json(self, attribution_json, signup_id=None,
                        is_first=None, is_last=None, is_signature=None, index=None):

        attribution = attribution_json.copy()
        attribution['date'] = convert_date(attribution_json.get('date'))

        if signup_id is not None:
            attribution['signup_id'] = signup_id

        if is_first is not None:
            attribution['is_first'] = is_first

        if is_last is not None:
            attribution['is_last'] = is_last

        if is_signature is not None:
            attribution['is_signature'] = is_last

        if index is not None:
            attribution['index'] = index

        return attribution


LuceraSignUpAttribution()
