# -*- coding: utf-8 -*-
from osv import osv, fields
from datetime import datetime
from lucera_signups.utils import *
import json


class LuceraPolissaSignUpGas(osv.osv):
    """Class to store gas signup data."""
    _name = 'lucera.signup.gas'

    def create(self, cursor, uid, vals, context=None):
        active = vals.get('active', False)
        if active:
            signup_gas_id = super(LuceraPolissaSignUpGas, self).create(cursor, uid, vals, context)
            return signup_gas_id
        return False

    def get_switching_process_code(self, cursor, uid, gas_signup_id, context=None):
        c = self.browse(cursor, uid, gas_signup_id, context)
        return self._get_computed_switching_process_code(c.has_current_atr_contract,
                                                         c.is_atr_contract_change,
                                                         c.holder_change)

    def _get_computed_switching_process_code(self, has_current_atr_contract, is_atr_contract_change, holder_change):
        if not has_current_atr_contract:
            return '38'
        elif is_atr_contract_change or holder_change:
            return '41'
        else:
            return '02'

    def _get_switching_config_vals(self, cursor, uid, gas_signup_id, context=None):
        """Gets the switching config values """
        if not context:
            context = {}

        process_code = self.get_switching_process_code(cursor, uid, gas_signup_id, context)
        gas_signup = self.browse(cursor, uid, gas_signup_id, context)
        contract_id = gas_signup.polissa_id.id

        if process_code == '41':
            wiz_obj = self.pool.get('giscegas.atr.a141.wizard')
            wiz_id = wiz_obj.create(
                cursor, uid, {}, context={'contract_id': contract_id}
            )
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            wiz.write({'updatereason': '01', 'surrogacy': 'S'})
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            return {
                'updatereason': wiz.updatereason,
                'modeffectdate': wiz.modeffectdate,
                'disconnectedserviceaccepted': wiz.disconnectedserviceaccepted,
                'titular_id': wiz.titular_id.id,
                'newreqqd': wiz.newreqqd,
                'surrogacy': wiz.surrogacy,
                'new_titular_id': wiz.new_titular_id.id,
                'new_titular_address_id': wiz.new_titular_address_id.id,
            }

        if process_code == '02':
            wiz_obj = self.pool.get('giscegas.atr.a102.wizard')
            wiz_id = wiz_obj.create(
                cursor, uid, {}, context={'contract_id': contract_id}
            )
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            reqqd = gas_signup.polissa_id.caudal_diario

            return {
                'modeffectdate': wiz.modeffectdate,
                'reqtransferdate': wiz.reqtransferdate,
                'extrainfo': wiz.extrainfo,
                'disconnectedserviceaccepted': wiz.disconnectedserviceaccepted,
                'reqqd': reqqd,
                'reqestimatedqa': reqqd*365.0
            }

        return False

    def _get_switching_polissa_vals(self, cursor, uid, gas_signup_id, context=None):
        if not context:
            context = {}

        process_code = self.get_switching_process_code(cursor, uid, gas_signup_id, context)

        signup = self.browse(cursor, uid, gas_signup_id, context)

        if process_code == '02':
            tariff_id = signup.current_tariff.id
        elif process_code == '41':
            if signup.is_atr_contract_change:
                tariff_id = signup.target_tariff.id
            elif signup.holder_change:
                tariff_id = signup.current_tariff.id
        elif process_code == '38':
            tariff_id = signup.target_tariff.id
        else:
            raise NotImplementedError

        if not tariff_id:
            raise osv.except_osv('ValidateError', 'Tariff is not defined in signup')

        return {'tarifa': tariff_id}

    def get_switching_process_data(self, cursor, uid, gas_signup_id, context=None):

        c = self.browse(cursor, uid, gas_signup_id, context)

        res = {
            'process_code': self.get_switching_process_code(cursor, uid, gas_signup_id, context),
            'config_vals': self._get_switching_config_vals(cursor, uid, gas_signup_id, context)
        }

        return res

    def _get_switching_process_code(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        res = dict.fromkeys(ids, False)
        for gas_signup_id in ids:
            res[gas_signup_id] = self.get_switching_process_code(cursor, uid, gas_signup_id, context)
        return res

    def on_change_atr(self, cursor, uid, ids,
                      has_current_atr_contract,
                      is_atr_contract_change,
                      holder_change,
                      context=None):
        data = {
            'switching_process': self._get_computed_switching_process_code(
                has_current_atr_contract,
                is_atr_contract_change,
                holder_change)
        }

        if not has_current_atr_contract:
            data['is_atr_contract_change'] = 0
            data['holder_change'] = 0

        return {
            'value': data
        }

    _rec_name = 'polissa_id'

    _columns = {

        # Manual disable checks
        'skip_is_signed': fields.boolean('Omitir contracto firmado'),
        'skip_has_bank_mandate': fields.boolean('Omitir mandato firmado'),
        'skip_needs_social_bonus_waiver': fields.boolean('Omitir documento bono social firmado'),
        'skip_has_previous_debt': fields.boolean('Omitir comprobación de deuda'),
        'skip_is_email_validated': fields.boolean('Omitir comprobación de email validado'),
        'skip_has_valid_tariff': fields.boolean('Omitir comprobación de tarifa permitida'),
        'skip_has_bad_solvency_rating': fields.boolean('Omitir comprobación de rating de solvencia malo'),

        # Gas Contract changes
        'holder_change': fields.boolean('Cambia titular'),
        'has_current_atr_contract': fields.boolean('Contrato ATR en vigor'),
        'is_atr_contract_change': fields.boolean('Cambia contrato acceso'),

        # Current
        'current_tariff': fields.many2one('giscegas.polissa.tarifa', 'Tarifa actual'),

        # Target
        'target_tariff': fields.many2one('giscegas.polissa.tarifa', 'Tarifa deseada'),

        # Gas Computed fields
        'switching_process': fields.function(_get_switching_process_code,
                                             type='selection',
                                             selection=[('02', '02 - Sin cambios'),
                                                        ('41', '41 - Con cambios'),
                                                        ('38', '38 - Nuevo suministro')],
                                             method=True,
                                             readonly=True,
                                             string='Proceso de switching',
                                             store=True),

        # Computed checks on commit
        'has_previous_debt': fields.boolean('Tiene deuda anterior'),

        # Relations
        # 'signup_id': fields.many2one('lucera.signup', 'Alta', readonly=True),
        'signups': fields.one2many('lucera.signup', 'gas_signup_id', 'Alta'),

        # This two fields are to make a one2one relation
        'polissa_id': fields.many2one('giscegas.polissa', 'Póliza gas', readonly=True),

        'active': fields.boolean('Activa', required=True),

        # Other fields
        'no_supply_reason': fields.selection([('rent', 'Estoy de alquiler y necesito un contrato nuevo'),
                                              ('purchase', 'Acabo de comprar mi casa de segunda mano'),
                                              ('new', 'Acabo de comprar una casa de obra nueva'),
                                              ('suspended', 'Me han cortado el gas'),
                                              ('terminated', 'Mi contrato anterior está dado de baja')],
                                             'Motivo de no tener gas'),
        'supply_point_code_source': fields.selection([('code', u'Introducido por el usuario'),
                                                      ('address_match', u'Dirección seleccionada'),
                                                      ('invoice_study', u'Estudio factura'),
                                                      ('personal_study', u'Calculadora'),
                                                      ('auto_match', u'Coincide con Electricidad')], 'Origen del código CUPS'),
    }

    _defaults = {
        'active': lambda *a: False,
        'supply_point_code_source': lambda *a: 'code',

        # Skip checks
        'skip_is_signed': lambda *a: False,
        'skip_has_bank_mandate': lambda *a: False,
        'skip_needs_social_bonus_waiver': lambda *a: False,
        'skip_has_previous_debt': lambda *a: False,
        'skip_is_email_validated': lambda *a: False,
        'skip_has_valid_tariff': lambda *a: False,
        'skip_has_bad_solvency_rating': lambda *a: False,

        # Gas
        'holder_change': lambda *a: False,
        'has_current_atr_contract': lambda *a: True,

        # Computed
        'has_previous_debt': lambda *a: False
    }

    _sql_constraints = [
        ('unique_polissa_id', 'unique (polissa_id)', u'Ya existe un alta de gas para esta póliza')]


LuceraPolissaSignUpGas()
