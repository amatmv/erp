# -*- coding: utf-8 -*-
from osv import osv, fields
from datetime import datetime
from lucera_signups.utils import *
import json


class LuceraPolissaSignUpElectricity(osv.osv):
    """Class to store electricity signup data."""
    _name = 'lucera.signup.electricity'

    def create(self, cursor, uid, vals, context=None):
        active = vals.get('active', False)
        if active:
            signup_electricity_id = super(LuceraPolissaSignUpElectricity, self).create(cursor, uid, vals, context)
            return signup_electricity_id
        return False

    def get_switching_process_code(self, cursor, uid, electricity_signup_id, context=None):
        c = self.browse(cursor, uid, electricity_signup_id, context)
        return self._get_computed_switching_process_code(c.has_current_atr_contract,
                                                         c.is_atr_contract_change,
                                                         c.holder_change)

    def _get_computed_switching_process_code(self, has_current_atr_contract, is_atr_contract_change, holder_change):
        if not has_current_atr_contract:
            return 'A3'
        elif is_atr_contract_change or holder_change:
            return 'C2'
        else:
            return 'C1'

    def _get_switching_config_vals(self, cursor, uid, electricity_signup_id, context=None):
        """Gets the switching config values """
        if not context:
            context = {}

        process_code = self.get_switching_process_code(cursor, uid, electricity_signup_id, context)

        c = self.browse(cursor, uid, electricity_signup_id, context)
        # Fill context
        context['pol_id'] = c.polissa_id.id
        context['active_model'] = 'giscedata.polissa'
        context['cas'] = process_code

        wiz_obj = self.pool.get('giscedata.switching.mod.con.wizard')

        config_vals = {}

        if process_code == 'C1':
            config_vals['activacio_cicle'] = 'N'  # Cuanto antes
            # config_vals['power_invoicing'] = False  # No nos fiamos del SIPS
            # config_vals['tariff'] = c.current_tariff.codi_ocsum
            # config_vals['power_p1'] = c.current_power
            return config_vals

        if process_code == 'C2':
            field_names = wiz_obj.fields_get(cursor, uid, context=context).keys()
            default_vals = wiz_obj.default_get(cursor, uid, field_names, context)

            config_vals = {}
            for field_name in field_names:
                if field_name in default_vals:
                    config_vals[field_name] = default_vals[field_name]
                else:
                    if 'potencia' in field_name:
                        config_vals[field_name] = 0
                    elif 'con_sur' in field_name:
                        config_vals[field_name] = ''

            # We change the phone_num to get the notification address first
            config_vals.update(wiz_obj.get_phone(cursor, uid, c.polissa_id.direccio_notificacio.id))
            if not config_vals['phone_num']:  # Fall back to fiscal address
                config_vals.update(wiz_obj.get_phone(cursor, uid, c.polissa_id.direccio_pagament.id))

            if c.is_atr_contract_change and c.holder_change:
                config_vals['change_type'] = 'both'
                config_vals['change_atr'] = 1
                config_vals['change_adm'] = 1
                config_vals['tariff'] = c.target_tariff.codi_ocsum
                config_vals['power_p1'] = get_power_as_watts(c.target_power)
            elif c.holder_change:
                config_vals['change_type'] = 'owner'
                config_vals['change_atr'] = 0
                config_vals['change_adm'] = 1
                config_vals['tariff'] = c.current_tariff.codi_ocsum
                config_vals['power_p1'] = get_power_as_watts(c.current_power)
            elif c.is_atr_contract_change:
                config_vals['change_type'] = 'tarpot'
                config_vals['change_atr'] = 1
                config_vals['change_adm'] = 0
                config_vals['tariff'] = c.target_tariff.codi_ocsum
                config_vals['power_p1'] = get_power_as_watts(c.target_power)

            config_vals['activacio_cicle'] = 'A'  # Cuanto antes
            config_vals['data_accio'] = datetime.strftime(datetime.now(), '%Y-%m-%d')
            config_vals['power_invoicing'] = False  # No nos fiamos del SIPS
            # config_vals['change_retail_tariff'] = 0,

            return config_vals

        if process_code == 'A3':
            # field_names = wiz_obj.fields_get(cursor, uid, context=context).keys()
            # default_vals = wiz_obj.default_get(cursor, uid, field_names, context)
            # config_vals = {}
            # for field_name in field_names:
            #     if field_name in default_vals:
            #         config_vals[field_name] = default_vals[field_name]
            #     else:
            #         if 'potencia' in field_name:
            #             config_vals[field_name] = 0
            #         elif 'con_sur' in field_name:
            #             config_vals[field_name] = ''
            config_vals['activacio_cicle'] = 'A'
            config_vals['data_accio'] = datetime.strftime(datetime.now(), '%Y-%m-%d')
            config_vals['cnae'] = c.polissa_id.cnae and c.polissa_id.cnae.id or False

            return config_vals

        return False

    def get_switching_tariff_and_power_vals(self, cursor, uid, electricity_signup_id, context=None):
        if not context:
            context = {}

        process_code = self.get_switching_process_code(cursor, uid, electricity_signup_id, context)

        electricity_signup = self.browse(cursor, uid, electricity_signup_id, context)

        if process_code == 'C1':
            power = electricity_signup.current_power
            tariff_id = electricity_signup.current_tariff.id
        elif process_code == 'C2':
            if electricity_signup.is_atr_contract_change:
                power = electricity_signup.target_power
                tariff_id = electricity_signup.target_tariff.id
            elif electricity_signup.holder_change:
                power = electricity_signup.current_power
                tariff_id = electricity_signup.current_tariff.id
        elif process_code == 'A3':
            power = electricity_signup.target_power
            tariff_id = electricity_signup.target_tariff.id
        else:
            raise NotImplementedError

        cups = electricity_signup.polissa_id.cups.name

        if not tariff_id:
            raise osv.except_osv('ValidateError', 'Tariff is not defined in contract with CUPS {}'.format(cups))

        power = get_power_as_kilowatts(power)

        if power <= 0:
            raise osv.except_osv('ValidateError', 'Power is not defined in contract with CUPS {}'.format(cups))

        return {'potencia': power,
                'tarifa': tariff_id}

    def get_switching_process_data(self, cursor, uid, electricity_signup_id, context=None):

        c = self.browse(cursor, uid, electricity_signup_id, context)

        res = {
            'process_code': self.get_switching_process_code(cursor, uid, electricity_signup_id, context),
            'config_vals': self._get_switching_config_vals(cursor, uid, electricity_signup_id, context),
            'polissa_vals': self.get_switching_tariff_and_power_vals(cursor, uid, electricity_signup_id, context)
        }

        return res

    def _get_switching_process_code(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        res = dict.fromkeys(ids, False)
        for electricity_signup_id in ids:
            res[electricity_signup_id] = self.get_switching_process_code(cursor, uid, electricity_signup_id, context)
        return res

    def on_change_atr(self, cursor, uid, ids,
                      has_current_atr_contract,
                      is_atr_contract_change,
                      holder_change,
                      context=None):
        data = {
            'switching_process': self._get_computed_switching_process_code(
                has_current_atr_contract,
                is_atr_contract_change,
                holder_change)
        }

        if not has_current_atr_contract:
            data['is_atr_contract_change'] = 0
            data['holder_change'] = 0

        return {
            'value': data
        }

    _rec_name = 'polissa_id'

    _columns = {

        # Manual disable checks
        'skip_is_signed': fields.boolean('Omitir contracto firmado'),
        'skip_has_bank_mandate': fields.boolean('Omitir mandato firmado'),
        'skip_needs_social_bonus_waiver': fields.boolean('Omitir documento bono social firmado'),
        'skip_has_previous_debt': fields.boolean('Omitir comprobación de deuda'),
        'skip_is_email_validated': fields.boolean('Omitir comprobación de email validado'),
        'skip_has_valid_tariff': fields.boolean('Omitir comprobación de tarifa permitida'),
        'skip_has_bad_solvency_rating': fields.boolean('Omitir comprobación de rating de solvencia malo'),
        'skip_requires_current_atr_data': fields.boolean('Omitir comprobación de potencia y tarifa actual'),

        # Electricity Contract changes
        'holder_change': fields.boolean('Cambia titular'),
        'has_current_atr_contract': fields.boolean('Contrato ATR en vigor'),
        'is_atr_contract_change': fields.boolean('Cambia contrato acceso'),

        # Electricity Social bonus
        'has_social_bonus': fields.boolean('Necesita documento de bono social'),
        'has_social_bonus_waiver': fields.boolean('Documento bono social aportado'),

        # Current
        'current_power': fields.float('Potencia actual', digits=(15, 3)),
        'current_tariff': fields.many2one('giscedata.polissa.tarifa', 'Tarifa actual'),
        # Target
        'target_power': fields.float('Potencia deseada', digits=(15, 3), required=False),
        'target_tariff': fields.many2one('giscedata.polissa.tarifa', 'Tarifa deseada'),


        # Electricity Computed fields
        'switching_process': fields.function(_get_switching_process_code,
                                             type='selection',
                                             selection=[('C1', 'C1'), ('C2', 'C2'), ('A3', 'A3')],
                                             method=True,
                                             readonly=True,
                                             string='Proceso de switching',
                                             store=True),

        # Computed checks on commit
        'has_previous_debt': fields.boolean('Tiene deuda anterior'),

        # Relations
        'signups': fields.one2many('lucera.signup', 'electricity_signup_id', 'Alta'),

        # This two fields are to make a one2one relation
        'polissa_id': fields.many2one('giscedata.polissa', 'Póliza electricidad', readonly=True),

        'active': fields.boolean('Activa', required=True),

        # Other fields
        'no_supply_reason': fields.selection([('rent', 'Estoy de alquiler y necesito un contrato nuevo'),
                                              ('purchase', 'Acabo de comprar mi casa de segunda mano'),
                                              ('new', 'Acabo de comprar una casa de obra nueva'),
                                              ('suspended', 'Me han cortado la luz'),
                                              ('terminated', 'Mi contrato anterior está dado de baja')],
                                             'Motivo de no tener luz'),
        'supply_point_code_source': fields.selection([('code', u'Introducido por el usuario'),
                                                      ('address_match', u'Dirección seleccionada'),
                                                      ('invoice_study', u'Estudio factura'),
                                                      ('personal_study', u'Calculadora')], 'Origen del código CUPS'),
    }

    _defaults = {
        'active': lambda *a: False,
        'supply_point_code_source': lambda *a: 'code',

        # Skip checks
        'skip_is_signed': lambda *a: False,
        'skip_has_bank_mandate': lambda *a: False,
        'skip_needs_social_bonus_waiver': lambda *a: False,
        'skip_has_previous_debt': lambda *a: False,
        'skip_is_email_validated': lambda *a: False,
        'skip_has_valid_tariff': lambda *a: False,
        'skip_has_bad_solvency_rating': lambda *a: False,

        # Electricity
        'has_social_bonus': lambda *a: False,
        'has_social_bonus_waiver': lambda *a: False,
        'holder_change': lambda *a: False,
        'has_current_atr_contract': lambda *a: True,

        # Computed
        'has_previous_debt': lambda *a: False
    }

    _sql_constraints = [
        ('unique_polissa_id', 'unique (polissa_id)', u'Ya existe un alta de electricidad para')]


LuceraPolissaSignUpElectricity()
