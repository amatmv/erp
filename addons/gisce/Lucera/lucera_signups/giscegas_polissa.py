# -*- coding: utf-8 -*-
import lucera_common
from lucera_common.config import LUCERA_SUPPORTED_GAS_SIGNUP_PROCESS_CODES, \
    LUCERA_SUPPORTED_GAS_SIGNUP_PROCESS_CODES_SELECTION
from osv import osv, fields
from datetime import datetime
from dateutil import parser
from oorq.decorators import job
import logging

from common_polissa import BasePolissa


class GiscegasPolissa(BasePolissa):
    _name = 'giscegas.polissa'
    _inherit = 'giscegas.polissa'

    def copy_data(self, cursor, uid, id, default=None, context=None):
        if default is None:
            default = {}

        default_values = {
            'gas_signups': False,
            'gas_signup_id': False,
            'has_cups': False,
            'has_bank_mandate': False,
            'signature_data': False,
            'signature_method': False,
            'has_valid_tariff': False,
            'is_signable': False,
            'is_switching_in_progress': False,
            'switching_state': False,
            'is_activable': False,
            'has_signup': False,
            'is_not_activable_reasons': False,
            'related_debt_polissa_id': False,
            'signup_status': False
        }

        default.update(default_values)

        res_id = super(
            GiscegasPolissa, self).copy_data(cursor, uid, id, default, context)

        return res_id

    # def _has_cups(self, cursor, uid, ids, field_name, arg, context):
    #     res = {}
    #     for polissa in self.browse(cursor, uid, ids, context=context):
    #         res[polissa.id] = polissa.cups.id is not False \
    #                           and polissa.cups.active \
    #                           and polissa.cups.name is not False
    #     return res

    def _has_cups_search(self, cursor, uid, obj, name, args, context):
        """Search signable contracts"""
        polissa_ids = self.search(cursor, uid, [('state', '=', 'esborrany')])
        res = self._has_cups(cursor, uid, polissa_ids, None, None, context)
        polissas_with_cups_ids = set(k for k, v in res.items() if v)
        if args[0][2]:  # has_cups = True
            ids = list(polissas_with_cups_ids)
        else:  # has_cups = False
            ids = list(set(polissa_ids) - polissas_with_cups_ids)
        return [('id', 'in', ids)]

    def _has_signup(self, cursor, uid, ids, field_name, arg, context):
        res = {}
        for polissa in self.browse(cursor, uid, ids, context=context):
            res[polissa.id] = True if polissa.gas_signups else False
        return res

    def _has_signup_search(self, cursor, uid, obj, name, args, context):
        """Search signable contracts"""
        polissa_ids = self.search(cursor, uid, [('state', '=', 'esborrany')])
        res = self._has_signup(cursor, uid, polissa_ids, None, None, context)
        polissas_with_signups = set(k for k, v in res.items() if v)
        if args[0][2]:  # has_cups = True
            ids = list(polissas_with_signups)
        else:  # has_cups = False
            ids = list(set(polissa_ids) - polissas_with_signups)
        return [('id', 'in', ids)]

    def _has_valid_tariff(self, cursor, uid, ids, field_name, arg, context):
        res = {}
        for polissa in self.browse(cursor, uid, ids, context=context):
            if polissa.gas_signups:
                gas_signup = polissa.gas_signups[0]
                if gas_signup.target_tariff:
                    res[polissa.id] = gas_signup.target_tariff.codi_ocsum \
                                      in lucera_common.config.LUCERA_GAS_ALLOWED_TARIFF_OCSUM_CODES
                elif gas_signup.current_tariff:
                    res[polissa.id] = gas_signup.current_tariff.codi_ocsum \
                                      in lucera_common.config.LUCERA_GAS_ALLOWED_TARIFF_OCSUM_CODES
                else:
                    res[polissa.id] = True
            else:
                res[polissa.id] = not polissa.tarifa \
                                  or polissa.tarifa.codi_ocsum \
                                  in lucera_common.config.LUCERA_GAS_ALLOWED_TARIFF_OCSUM_CODES
        return res

    def find_related_contracts_with_debt(self, cursor, uid, polissa_id, context):

        return []
        # TODO: giscegas_facturacio_impagats
        # - https://github.com/gisce/erp/issues/7323
        # - https://github.com/gisce/erp/pull/7321

        partner_obj = self.pool.get('res.partner')
        polissa = self.browse(cursor, uid, polissa_id, context=context)

        # Search by partners vats, phones or emails
        vats = [i for i in list(
            {polissa.titular.vat if polissa.titular else False,
             polissa.direccio_notificacio.partner_id.vat if polissa.direccio_notificacio else False,
             polissa.pagador.vat if polissa.pagador else False}) if i]
        phones = [i for i in list(
            {polissa.titular.address[0].phone if polissa.titular else False,
             polissa.direccio_notificacio.phone if polissa.direccio_notificacio else False,
             polissa.pagador.address[0].phone if polissa.pagador else False}) if i]
        emails = [i for i in list(
            {polissa.titular.address[0].email if polissa.titular else False,
             polissa.direccio_notificacio.email if polissa.direccio_notificacio else False,
             polissa.pagador.address[0].email if polissa.pagador else False}) if i]

        search_partners = [('vat', 'in', vats)]

        # We only add fields that are not empty
        if phones:
            search_partners.insert(0, '|')
            search_partners.insert(len(search_partners), ('address.phone', 'in', phones))

        if emails:
            search_partners.insert(0, '|')
            search_partners.insert(len(search_partners), ('address.email', 'in', emails))

        partner_ids = partner_obj.search(cursor, uid, search_partners)

        search_params = [
            ('id', '!=', polissa.id),
            ('debt_amount', '>', 0),
            '|', '|',
            ('titular', 'in', partner_ids),
            ('pagador', 'in', partner_ids),
            ('direccio_notificacio.partner_id', 'in', partner_ids)
        ]

        if polissa.bank.iban:
            search_params.insert(3, '|')
            search_params.insert(len(search_params), ('bank.iban', '=', polissa.bank.iban))

        if polissa.cups.name:
            search_params.insert(3, '|')
            search_params.insert(len(search_params), ('cups.name', '=', polissa.cups.name))

        # We search polissas that may have debt, event if the are not active any more
        related_polissa_ids = self.search(cursor, uid, search_params, context={'active_test': False})

        return related_polissa_ids

    def _related_debt_polissa_id(self, cursor, uid, ids, field_name, arg, context):
        res = {}
        for polissa_id in ids:
            related_polissa_ids = self.find_related_contracts_with_debt(cursor, uid, polissa_id, context)
            for related_polissa_id in related_polissa_ids:
                res[related_polissa_id] = True
        return res

    def _related_debt_polissa_id_search(self, cursor, uid, obj, name, args, context):
        """Search contracts with debts contracts in borrador"""
        # polissa_ids = self.search(cursor, uid, [('state', '=', 'esborrany')])
        polissa_id = args[0][2]
        res = self._related_debt_polissa_id(cursor, uid, [polissa_id], None, None, context)
        ids = list(set(res.keys()))
        return [('id', 'in', ids)]

    def _has_bank_mandate(self, cursor, uid, ids, field_name, arg, context):
        res = {}
        mandate_obj = self.pool.get('payment.mandate')
        for polissa in self.browse(cursor, uid, ids, context=context):
            if polissa.bank:
                search = [('reference', '=', 'giscegas.polissa,%s' % polissa['id'])]
                mandate_id = mandate_obj.search(cursor, uid, search)
                res[polissa.id] = True if mandate_id else False
            else:
                res[polissa.id] = False
        return res

    def _has_bank_mandate_search(self, cursor, uid, obj, name, args, context):
        """Search signable contracts"""
        res = {}
        has_bank_mandate = args[0][2]

        polissa_ids = self.search(cursor, uid, [('state', '=', 'esborrany')])
        res = self._has_bank_mandate(cursor, uid, polissa_ids, None, None, context)
        polissas_with_cups_mandate = set(k for k, v in res.items() if v)
        if args[0][2]:  # has_cups = True
            ids = list(polissas_with_cups_mandate)
        else:  # has_cups = False
            ids = list(set(polissa_ids) - polissas_with_cups_mandate)
        return [('id', 'in', ids)]

    def _is_signable(self, cursor, uid, ids, field_name, arg, context):
        res = {}
        for polissa in self.browse(cursor, uid, ids, context=context):
            res[polissa.id] = polissa.data_firma_contracte is False \
                              and polissa.cups.name
        return res

    def _is_signable_search(self, cursor, uid, obj, name, args, context):
        """Search signable contracts (pending to be signed). 'cancelada' are also signable, but not pending"""
        return [('state', 'in', ['esborrany']),
                ('data_firma_contracte', '=', False),
                ('cups', '!=', False)]

    def _is_activable_errors(self, cursor, uid, polissa_id, context):
        result = []
        skip_conditions = {}
        gas_signup = False
        polissa = self.browse(cursor, uid, polissa_id, context=context)
        if polissa.gas_signups:
            gas_signup = polissa.gas_signups[0]
            skip_conditions = {
                'skip_has_bank_mandate': gas_signup.skip_has_bank_mandate,
                'skip_is_signed': gas_signup.skip_is_signed,
                'skip_is_email_validated': gas_signup.skip_is_email_validated,
                'skip_has_previous_debt': gas_signup.skip_has_previous_debt,
                'skip_has_valid_tariff': gas_signup.skip_has_valid_tariff,
                'skip_has_bad_solvency_rating': gas_signup.skip_has_bad_solvency_rating
            }

        polissa = self.browse(cursor, uid, polissa_id, context=context)
        if not polissa.has_cups:
            result.append('not_has_cups')
        if not skip_conditions.get('skip_has_bank_mandate', False) and not polissa.has_bank_mandate:
            result.append('not_has_bank_mandate')
        if not skip_conditions.get('skip_is_signed', False) and not polissa.firmat:
            result.append('not_signed')
        if not skip_conditions.get('skip_is_email_validated', False) \
                and not polissa.direccio_notificacio.is_email_validated:
            result.append('not_is_email_validated')
        if not skip_conditions.get('skip_has_previous_debt', False) \
                and gas_signup and gas_signup.has_previous_debt:
            result.append('has_previous_debt')
        if not skip_conditions.get('skip_has_valid_tariff', False) and not polissa.has_valid_tariff:
            result.append('invalid_tariff')
        if not skip_conditions.get('skip_has_bad_solvency_rating', False):
            if polissa.pagador.has_bad_solvency_rating:
                # Sólo paramos aquellos que tienen mala solvencia y que son un A3
                if gas_signup and gas_signup.switching_process == '38':
                    result.append('bad_solvency_rating')
        return result

    def _is_activable(self, cursor, uid, ids, field_name, arg, context):
        res = {}
        for polissa_id in ids:
            result = self._is_activable_errors(cursor, uid, polissa_id, context)
            res[polissa_id] = not any(result)
        return res

    def _is_activable_search(self, cursor, uid, obj, name, args, context=None):
        """Search activable contracts in borrador"""
        polissa_ids = self.search(cursor, uid, [('state', '=', 'esborrany')])
        res = self._is_activable(cursor, uid, polissa_ids, None, None, context)
        activable_ids = set(k for k, v in res.items() if v)
        if args[0][2]:  # is_activable = True
            ids = list(activable_ids)
        else:  # is_activable = False
            ids = list(set(polissa_ids) - activable_ids)
        return [('id', 'in', ids)]

    def _translate_error(self, error):
        if error == 'not_has_cups':
            return 'sin cups'
        if error == 'not_has_bank_mandate':
            return 'mandato no firmado'
        if error == 'not_signed':
            return 'contrato no firmado'
        if error == 'not_is_email_validated':
            return 'email no validado'
        if error == 'has_previous_debt':
            return 'tiene deuda anterior'
        if error == 'invalid_tariff':
            return 'tarifa no permitida'
        if error == 'bad_solvency_rating':
            return 'rating de solvencia malo'
        raise NotImplementedError('{} not translated'.format(error))

    def _is_not_activable_reasons(self, cursor, uid, ids, field_name, arg, context):
        res = {}
        for polissa_id in ids:
            result = [self._translate_error(e) for e in self._is_activable_errors(cursor, uid, polissa_id, context)]
            res[polissa_id] = ', '.join(result)
        return res

    def _signup_fields(self, cursor, uid, ids, field_name, arg, context):
        res = dict.fromkeys(ids, {'signup_date': False,
                                  'signup_switching_process': False,
                                  'has_signup': False})
        signup_obj = self.pool.get('lucera.signup')
        sql = 'select p.id as polissa_id, e.switching_process as switching_process, s.date as signup_date ' \
              'from giscegas_polissa p join lucera_signup_gas e on p.id = e.polissa_id ' \
              'join lucera_signup s on s.gas_signup_id = e.id ' \
              'where p.id in %s'
        cursor.execute(sql, (tuple(ids),))
        for s in cursor.dictfetchall():
            res[s['polissa_id']] = {'has_signup': True,
                                    'signup_date': s['signup_date'],
                                    'signup_switching_process': s['switching_process']}
        return res

    def _switching_process(self, cursor, uid, ids, field_name, arg, context):
        res = dict.fromkeys(ids, {'is_switching_in_progress': False,
                                  'switching_process': False,
                                  'switching_state': False})
        sw_obj = self.pool.get('giscegas.atr')
        search_params = [('proces_id.name', 'in', LUCERA_SUPPORTED_GAS_SIGNUP_PROCESS_CODES),
                         ('case_id.state', 'in', ('open', 'pending', 'draft')),
                         ('cups_polissa_id', 'in', ids),
                         ('cups_polissa_id.active', '=', True)]
        switching_ids = sw_obj.search(cursor, uid, search_params)
        switchings = sw_obj.browse(cursor, uid, switching_ids, context=context)
        for s in switchings:
            res[s.cups_polissa_id.id] = {'is_switching_in_progress': True,
                                         'switching_state': s.state,
                                         'switching_process': s.proces_id.name}
        return res

    def _signup_status(self, cursor, uid, ids, field_name, arg, context):
        if not context:
            context = {}

        res = dict.fromkeys(ids, 'unknown')
        sw_obj = self.pool.get('giscegas.atr')
        activables = self._is_activable(cursor, uid, ids, None, None, context)
        for polissa_id in ids:
            if not activables[polissa_id]:
                res[polissa_id] = 'not_activable'
            else:
                search_params = [('proces_id.name', 'in', LUCERA_SUPPORTED_GAS_SIGNUP_PROCESS_CODES),
                                 # ('case_id.state', 'in', ('open', 'pending', 'draft')),
                                 ('cups_polissa_id', '=', polissa_id)]
                sw_ids = sw_obj.search(cursor, uid, search_params, order='id desc', limit=1)
                if sw_ids:
                    sw = sw_obj.read(cursor, uid, sw_ids[0], ['state', 'rebuig'])
                    if sw['rebuig']:
                        res[polissa_id] = 'activation_failed'
                    elif sw['state'] in ('open', 'pending', 'draft'):
                        res[polissa_id] = 'activation_requested'
                    elif sw['state'] in ('done',):
                        res[polissa_id] = 'activation_succeeded'
                    else:
                        res[polissa_id] = 'activation_failed'
                else:
                    res[polissa_id] = 'activable'

        return res

    def _signup_status_search(self, cursor, uid, obj, name, args, context):
        res = {}
        status_operator = args[0][1]
        status = args[0][2]

        ids = self.search(cursor, uid, [('state', '=', 'esborrany')])
        polissa_status = self._signup_status(cursor, uid, ids, 'signup_status', args, context)

        if status_operator in ('=', '=='):
            polissa_ids = [k for k in polissa_status if polissa_status[k] == status]
        elif status_operator in ('!=', '<>'):
            polissa_ids = [k for k in polissa_status if polissa_status[k] != status]
        elif status_operator in ('in',):
            polissa_ids = [k for k in polissa_status if polissa_status[k] in tuple(status)]
        elif status_operator in ('not in',):
            polissa_ids = [k for k in polissa_status if polissa_status[k] not in tuple(status)]
        else:
            raise NotImplementedError

        return [('id', 'in', polissa_ids)]

    def _gas_signup_id(self, cursor, uid, ids, field_name, arg, context):

        res = dict.fromkeys(ids, False)
        signup_gas_obj = self.pool.get('lucera.signup.gas')
        signup_gas_ids = signup_gas_obj.search(cursor, uid, [('polissa_id', 'in', ids)])
        signups = signup_gas_obj.read(cursor, uid, signup_gas_ids, ['polissa_id'])
        for s in signups:
            res[s['polissa_id']] = s['id']
        return res

    def sign_by_email(self, cursor, uid, ids, email, ip_address, context=None):
        """Signs a contract using the email method """
        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        # Check is allowed to sign contracts
        res = {}
        for polissa in self.browse(cursor, uid, ids, context=context):
            if polissa.data_firma_contracte:
                raise osv.except_osv('ValidateError', 'Already signed')
            if polissa.direccio_notificacio.email != email:
                raise osv.except_osv('SecurityError', 'Email not allowed'.format(polissa.id))
            if polissa.state not in ['esborrany', 'cancelada']:
                raise osv.except_osv('ValidateError', 'Invalid state to be signed')
            res[polissa.id] = {
                'user_id': polissa.direccio_notificacio.partner_id,
                'address_id': polissa.direccio_notificacio.id
            }

        self._sign(cursor, uid, ids, datetime.now(), 'email', ip_address)

        return res

    def _sign(self, cursor, uid, ids, signature_date, signature_method, signature_data, context=None):

        for polissa in self.browse(cursor, uid, ids, context=context):
            if polissa.state == 'cancelada':
                polissa.send_signal('to_draft')
                self.write(cursor, uid, [polissa.id], {'data_baixa': None}, context=context)

        vals = {
            'data_firma_contracte': signature_date,
            'signature_method': signature_method,
            'signature_data': signature_data
        }
        if self.write(cursor, uid, ids, vals, context=context):
            bus = self.pool.get('lucera.bus')
            for polissa_id in ids:
                bus.send(cursor, uid, 'giscegas.polissa.signed', {'polissa_id': polissa_id}, 'erp')

                self._create_asnef_request(cursor, uid, polissa_id, context)
            return True

        return False

    @job(queue='asnef')
    def _create_asnef_request(self, cursor, uid, ids, context=None):
        """
        Creates a request if conditions are not met

        * If has a recent rating
        * If has contracts (active or terminated)

        :return:
        """

        if isinstance(ids, (int, long)):
            ids = [ids]

        polissas = self.browse(cursor, uid, ids, context)
        for polissa in polissas:
            try:
                if polissa.pagador.asnef_requests:
                    latest_request = polissa.pagador.asnef_requests[0]
                    latest_request_date = latest_request.date
                    if isinstance(latest_request_date, basestring):
                        latest_request_date = parser.parse(latest_request_date)
                    delta = datetime.now() - latest_request_date

                    if delta.days < 365:
                        return None

                pagador_id = polissa.pagador.id

                if not context:
                    context = {}

                new_context = context.copy()
                new_context['active_test'] = False

                other_polissas = self.search(cursor, uid, [
                    ('pagador', '=', pagador_id),
                    ('id', '<>', polissa.id),
                    ('state', 'not in', ['esborrany', 'cancelada'])], context=new_context)

                if other_polissas:
                    return False

                partner_obj = self.pool.get('res.partner')

                partner_obj.create_asnef_request(cursor, uid, [pagador_id], context=context).get(pagador_id)
            except Exception as e:
                logger = logging.getLogger('openerp.{}'.format(__name__))
                logger.error('Could not perform Equifax ASNEF request: {0}'.format(e))

    def send_signature_email(self, cursor, uid, ids, context=None):
        return self._send_signup_email(cursor, uid, ids, 'send_gas_signup_signature_email', context)

    def send_welcome_email(self, cursor, uid, ids, context=None):
        return self._send_signup_email(cursor, uid, ids, 'send_gas_signup_welcome_email', context)

    def send_activation_email(self, cursor, uid, ids, context=None):
        return self._send_signup_email(cursor, uid, ids, 'send_gas_polissa_activation_email', context)

    def _send_signup_email(self, cursor, uid, ids, email_type, context=None):
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        if ids:
            bus = self.pool.get('lucera.bus')
            for polissa_id in ids:
                bus.send(cursor, uid, 'giscegas.polissa.{}'.format(email_type), {'polissa_id': polissa_id}, 'erp')
        return True

    def crear_cas_atr(self, cursor, uid, polissa_id, proces=None,
                      config_vals=None, context=None):

        if not context:
            context = {}

        # Search for existing signup if it is process An
        if proces == 'An':
            if not config_vals:
                config_vals = {}
            p = self.browse(cursor, uid, polissa_id)
            if not p.is_activable:
                raise osv.except_osv('ValidateError', 'No es posible activar el contrato {} porque {}'
                                     .format(p.name + ' ' if p.name else '', p.is_not_activable_reasons))
            if p.gas_signups:
                signup_obj = self.pool.get('lucera.signup.gas')
                sw = signup_obj.get_switching_process_data(cursor, uid, p.gas_signups[0].id, context)
                context['proces'] = sw['process_code']
                # Configuration vals
                if 'config_vals' in sw:
                    config_vals.update(sw['config_vals'])
                # For some process, we have to update the polissa
                if 'polissa_vals' in sw:
                    self.write(cursor, uid, [polissa_id], sw['polissa_vals'], context)

                return super(GiscegasPolissa, self).crear_cas_atr(cursor, uid, polissa_id, sw['process_code'],
                                                                  config_vals,
                                                                  context)

        return super(GiscegasPolissa, self).crear_cas_atr(cursor, uid, polissa_id, proces, config_vals, context)

    def _cnt_positive_pressure(self, cursor, uid, ids):
        """Comprova que la presion sigui estrictament positiva
        """
        for polissa_vals in self.read(cursor, uid, ids, ['pressio', 'state']):
            if polissa_vals['state'] not in ('esborrany', 'cancelada') and polissa_vals['pressio'] < 0:
                return False
        return True

    _constraints = [
        (
            _cnt_positive_pressure,
            'Error: La presión ha de ser estrictament positiva',
            ['pressio']
        )
    ]

    # def __init__(self, pool, cursor):
    #     """ Override _cnt_positive_power """
    #     for c in self._constraints:
    #         if c[0].func_name == '_cnt_positive_power':
    #             c[0] = self._cnt_positive_power

    _columns = {
        # Relations
        'gas_signups': fields.one2many('lucera.signup.gas', 'polissa_id', 'Alta', required=False),
        'gas_signup_id': fields.function(_gas_signup_id,
                                         method=True,
                                         type='many2one',
                                         string='Alta gas',
                                         relation='lucera.signup.gas',
                                         readonly=True),
        # Checks
        'has_cups': fields.function(BasePolissa._has_cups,
                                    method=True,
                                    type='boolean',
                                    string='CUPS proporcionado',
                                    fnct_search=_has_cups_search,
                                    readonly=True),
        'has_bank_mandate': fields.function(_has_bank_mandate,
                                            method=True,
                                            type='boolean',
                                            string='Mandato firmado',
                                            fnct_search=_has_bank_mandate_search,
                                            readonly=True),
        # 'has_previous_debt': fields.boolean('Tiene deuda anterior'),
        'is_email_validated': fields.related('direccio_notificacio', 'is_email_validated',
                                             type='boolean',
                                             relation='res_partner_address',
                                             string='Email validado',
                                             readonly=True),
        'has_valid_tariff': fields.function(_has_valid_tariff,
                                            method=True,
                                            type='boolean',
                                            string='Tarifa válida',
                                            readonly=True),
        'is_signable': fields.function(_is_signable,
                                       method=True,
                                       type='boolean',
                                       string='Firmable',
                                       fnct_search=_is_signable_search,
                                       readonly=True),
        'is_activable': fields.function(_is_activable,
                                        method=True,
                                        type='boolean',
                                        string='Activable',
                                        fnct_search=_is_activable_search,
                                        readonly=True),

        # Switching
        'is_switching_in_progress': fields.function(_switching_process,
                                                    method=True,
                                                    type='boolean',
                                                    string='Activación en curso',
                                                    # fnct_search=_is_switching_in_progress_search,
                                                    multi='Switching',
                                                    readonly=True),
        'switching_state': fields.function(_switching_process,
                                           method=True,
                                           type='selection',
                                           selection=[('none', 'None'),
                                                      ('draft', 'Draft'),
                                                      ('open', 'Open'),
                                                      ('cancel', 'Canceled'),
                                                      ('done', 'Closed'),
                                                      ('pending', 'Pending')],
                                           string='Estado switching',
                                           # fnct_search=_switching_process_state_search,
                                           multi='Switching',
                                           readonly=True),

        'switching_process': fields.function(_switching_process,
                                             method=True,
                                             type='selection',
                                             selection=LUCERA_SUPPORTED_GAS_SIGNUP_PROCESS_CODES_SELECTION,
                                             size='16',
                                             string='Proceso (Actual)',
                                             multi='Switching',
                                             readonly=True),

        # Signup
        'signup_switching_process': fields.function(_signup_fields,
                                                    method=True,
                                                    type='char',
                                                    # selection=[('C1', 'C1'), ('C2', 'C2'), ('A3', 'A3')],
                                                    size='16',
                                                    string='Proceso (Alta)',
                                                    multi='SignUp',
                                                    readonly=True),
        'has_signup': fields.function(_signup_fields,
                                      method=True,
                                      type='boolean',
                                      string='Tiene alta',
                                      fnct_search=_has_signup_search,
                                      multi='SignUp',
                                      readonly=True),

        'signup_date': fields.function(_signup_fields,
                                       method=True,
                                       type='date',
                                       string='Fecha de alta',
                                       multi='SignUp',
                                       readonly=True),

        # Signature
        'signature_method': fields.selection([('email', u'Email'), ('phone', u'Teléfono')], 'Método de firma'),
        'signature_data': fields.char('Datos de la firma', size=32),

        # UI
        'is_not_activable_reasons': fields.function(_is_not_activable_reasons,
                                                    method=True,
                                                    type='char',
                                                    size=256,
                                                    string='Por qué no es activable',
                                                    readonly=True),
        # Queries
        'related_debt_polissa_id': fields.function(_related_debt_polissa_id,
                                                   method=True,
                                                   type='one2many',
                                                   string='Contratos relacionados con deuda',
                                                   fnct_search=_related_debt_polissa_id_search,
                                                   relation='giscegas.polissa',
                                                   readonly=True),

        'signup_status': fields.function(_signup_status,
                                         method=True,
                                         type='selection',
                                         selection=[('not_activable', 'No activable'),
                                                    ('activable', 'Activable'),
                                                    ('activation_requested', 'Activación solicitada'),
                                                    ('activation_failed', 'Activación fallida')],
                                         string='Estado alta',
                                         fnct_search=_signup_status_search,
                                         readonly=True)

    }


GiscegasPolissa()
