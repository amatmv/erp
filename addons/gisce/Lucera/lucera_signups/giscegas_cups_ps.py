# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscegasCupsPs(osv.osv):
    """ We want to check only if cups is active. This way we can create empty cups"""

    _name = 'giscegas.cups.ps'
    _inherit = 'giscegas.cups.ps'

    def check_cups(self, cursor, uid, ids):
        # Only check if active
        if any(x['active'] for x in self.read(cursor, uid, ids, ['active'])):
            return super(GiscegasCupsPs, self).check_cups(cursor, uid, ids)
        return True

    def check_unique_cups(self, cursor, uid, ids):
        # Only check if active
        if any(x['active'] for x in self.read(cursor, uid, ids, ['active'])):
            return super(GiscegasCupsPs, self).check_unique_cups(cursor, uid, ids)
        return True

    _columns = {
        'name': fields.char('CUPS', size=25, required=False),
        'distribuidora_id': fields.many2one('res.partner', 'Distribuidora', required=False)
    }

    _constraints = [
        (check_cups, "El codi del CUPS no és correcte.", ["name"]),
        (check_unique_cups, "Aquest Codi de CUPS ja existeix", ["name"]),
    ]


GiscegasCupsPs()