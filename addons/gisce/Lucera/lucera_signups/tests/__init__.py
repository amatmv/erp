# -*- coding: utf-8 -*-
from signup_tests import *
from signup_committer_tests import *
from signup_signature_tests import *

# Electricity
from lucera_signups.tests.electricity.signup_polissa_activation_tests import *
from lucera_signups.tests.electricity.signup_switching_tests import *
from lucera_signups.tests.electricity.signup_polissa_tests import *

# Gas
from lucera_signups.tests.gas.signup_polissa_tests import *
# from lucera_signups.tests.gas.signup_polissa_activation_tests import *
# from lucera_signups.tests.gas.signup_switching_tests import *

