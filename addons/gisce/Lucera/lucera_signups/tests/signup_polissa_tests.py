# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from expects import *
import utils


class SignUpPolissaTests(testing.OOTestCase):

    def test_signup_is_not_copied_when_cloning_a_polissa(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            pool = self.openerp.pool
            cursor = txn.cursor
            user = txn.user

            result = utils.create_tested_polissa_with_signup(pool, cursor, user)
            polissa_id = result['polissa_id']
            signup_id = result['signup_id']

            # Act
            polissa_obj = pool.get('giscedata.polissa')
            copied_polissa_id = polissa_obj.copy(cursor, user, polissa_id, default={})

            # Assert
            signup_obj = pool.get('lucera.signup.electricity')
            signup_ids = signup_obj.search(cursor, user, [('polissa_id', '=', copied_polissa_id)])
            expect(signup_ids).to(be_empty)

