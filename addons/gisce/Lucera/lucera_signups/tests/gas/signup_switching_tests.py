# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from expects import *
import lucera_signups.tests.utils
import unittest
import json


class SignUpGasSwitchingTests(testing.OOTestCase):
    """ Test that a polissa cannot be activated in multiple cases"""

    def setup_comer(self, pool, cursor, user):
        imd_obj = pool.get('ir.model.data')
        partner_id = imd_obj.get_object_reference(
            cursor, user, 'base', 'main_partner'
        )[1]
        partner_obj = pool.get('res.partner')
        partner_obj.write(cursor, user, partner_id, {'ref': '9999'})

    @unittest.skip('giscegas.switching.log instantiates a new cursor and that is not compatible with this test')
    def test_can_create_c1_case(self):
        with Transaction().start(self.database) as txn:
            pool = self.openerp.pool
            cursor = txn.cursor
            user = txn.user

            # Arrange
            self.setup_comer(pool, cursor, user)
            tarifa_obj = pool.get('giscegas.polissa.tarifa')
            tariff_id = tarifa_obj.search(cursor, user, [('codi_ocsum', '=', '001')])[0]

            signup_vals = {
                'is_atr_contract_change': False,
                'holder_change': False,
                'has_current_atr_contract': True,
                'current_tariff': tariff_id,
                'current_power': 3450
            }
            result = lucera_signups.tests.utils.create_test_gas_polissa_with_signup(pool, cursor, user, gas_signup_vals=signup_vals)
            polissa_id = result['polissa_id']

            # Act
            polissa_obj = pool.get('giscegas.polissa')
            result = polissa_obj.crear_cas_atr(cursor, user, polissa_id, proces='Cn')

            # Assert
            expect(result).to_not(be_false)

    def test_can_get_c1_switching_data(self):
        with Transaction().start(self.database) as txn:
            pool = self.openerp.pool
            cursor = txn.cursor
            user = txn.user

            # Arrange
            self.setup_comer(pool, cursor, user)
            tarifa_obj = pool.get('giscegas.polissa.tarifa')
            tariff_id = tarifa_obj.search(cursor, user, [('codi_ocsum', '=', '31')])[0]

            signup_vals = {
                'is_atr_contract_change': False,
                'holder_change': False,
                'has_current_atr_contract': True,
                'current_tariff': tariff_id,
                'current_power': 3450
            }
            result = lucera_signups.tests.utils.create_test_gas_polissa_with_signup(pool, cursor, user, gas_signup_vals=signup_vals)
            polissa_id = result['polissa_id']
            gas_signup_id = result['gas_signup_id']

            # Act
            polissa_obj = pool.get('giscegas.polissa')
            gas_signup_obj = pool.get('lucera.signup.gas')
            result = gas_signup_obj.get_switching_process_data(cursor, user, gas_signup_id)

            # Assert
            expect(result).to_not(be_false)
            expect(result['process_code']).to(equal('C1'))
            expect(result['config_vals']['activacio_cicle']).to(equal('N'))
            expect(result['polissa_vals']['tarifa']).to(equal(tariff_id))
            expect(result['polissa_vals']['pressio']).to(equal(3.45))

    def test_can_get_c2_only_owner_switching_data(self):
        with Transaction().start(self.database) as txn:
            pool = self.openerp.pool
            cursor = txn.cursor
            user = txn.user

            # Arrange
            self.setup_comer(pool, cursor, user)
            tarifa_obj = pool.get('giscegas.polissa.tarifa')
            tariff_id = tarifa_obj.search(cursor, user, [('codi_ocsum', '=', '31')])[0]
            tariff2_id = tarifa_obj.search(cursor, user, [('codi_ocsum', '=', '32')])[0]

            signup_vals = {
                'is_atr_contract_change': False,
                'holder_change': True,
                'has_current_atr_contract': True,
                'current_tariff': tariff_id,
                'current_power': 3450,
                'target_tariff': tariff2_id,
                'target_power': 4600
            }
            result = lucera_signups.tests.utils.create_test_gas_polissa_with_signup(pool, cursor, user, gas_signup_vals=signup_vals)
            polissa_id = result['polissa_id']
            gas_signup_id = result['gas_signup_id']

            # Act
            polissa_obj = pool.get('giscegas.polissa')
            gas_signup_obj = pool.get('lucera.signup.gas')
            result = gas_signup_obj.get_switching_process_data(cursor, user, gas_signup_id)

            # Assert
            expect(result).to_not(be_false)
            expect(result['process_code']).to(equal('C2'))
            expect(result['config_vals']['activacio_cicle']).to(equal('A'))
            expect(result['config_vals']['change_type']).to(equal('owner'))
            expect(result['config_vals']['change_adm']).to(equal(1))
            expect(result['config_vals']['change_atr']).to(equal(0))
            expect(result['polissa_vals']['tarifa']).to(equal(tariff_id))
            expect(result['polissa_vals']['potencia']).to(equal(3.45))

    def test_can_get_c2_only_atr_switching_data(self):
        with Transaction().start(self.database) as txn:
            pool = self.openerp.pool
            cursor = txn.cursor
            user = txn.user

            # Arrange
            self.setup_comer(pool, cursor, user)
            tarifa_obj = pool.get('giscegas.polissa.tarifa')
            tariff_id = tarifa_obj.search(cursor, user, [('codi_ocsum', '=', '31')])[0]
            tariff2_id = tarifa_obj.search(cursor, user, [('codi_ocsum', '=', '32')])[0]

            signup_vals = {
                'is_atr_contract_change': True,
                'holder_change': False,
                'has_current_atr_contract': True,
                'current_tariff': tariff_id,
                'current_power': 3450,
                'target_tariff': tariff2_id,
                'target_power': 4600
            }
            result = lucera_signups.tests.utils.create_test_gas_polissa_with_signup(pool, cursor, user, gas_signup_vals=signup_vals)
            polissa_id = result['polissa_id']
            gas_signup_id = result['gas_signup_id']

            # Act
            polissa_obj = pool.get('giscegas.polissa')
            gas_signup_obj = pool.get('lucera.signup.gas')
            result = gas_signup_obj.get_switching_process_data(cursor, user, gas_signup_id)

            # Assert
            expect(result).to_not(be_false)
            expect(result['process_code']).to(equal('C2'))
            expect(result['config_vals']['activacio_cicle']).to(equal('A'))
            expect(result['config_vals']['change_type']).to(equal('tarpot'))
            expect(result['config_vals']['change_adm']).to(equal(0))
            expect(result['config_vals']['change_atr']).to(equal(1))
            expect(result['polissa_vals']['tarifa']).to(equal(tariff2_id))
            expect(result['polissa_vals']['potencia']).to(equal(4.6))

    def test_can_get_c2_with_owner_and_atr_switching_data(self):
        with Transaction().start(self.database) as txn:
            pool = self.openerp.pool
            cursor = txn.cursor
            user = txn.user

            # Arrange
            self.setup_comer(pool, cursor, user)
            tarifa_obj = pool.get('giscegas.polissa.tarifa')
            tariff_id = tarifa_obj.search(cursor, user, [('codi_ocsum', '=', '31')])[0]
            tariff2_id = tarifa_obj.search(cursor, user, [('codi_ocsum', '=', '32')])[0]

            signup_vals = {
                'is_atr_contract_change': True,
                'holder_change': True,
                'has_current_atr_contract': True,
                'current_tariff': tariff_id,
                'current_power': 3450,
                'target_tariff': tariff2_id,
                'target_power': 4600
            }
            result = lucera_signups.tests.utils.create_test_gas_polissa_with_signup(pool, cursor, user, gas_signup_vals=signup_vals)
            polissa_id = result['polissa_id']
            gas_signup_id = result['gas_signup_id']

            # Act
            polissa_obj = pool.get('giscegas.polissa')
            gas_signup_obj = pool.get('lucera.signup.gas')
            result = gas_signup_obj.get_switching_process_data(cursor, user, gas_signup_id)

            # Assert
            expect(result).to_not(be_false)
            expect(result['process_code']).to(equal('C2'))
            expect(result['config_vals']['activacio_cicle']).to(equal('A'))
            expect(result['config_vals']['change_type']).to(equal('both'))
            expect(result['config_vals']['change_adm']).to(equal(1))
            expect(result['config_vals']['change_atr']).to(equal(1))
            expect(result['polissa_vals']['tarifa']).to(equal(tariff2_id))
            expect(result['polissa_vals']['potencia']).to(equal(4.6))

    def test_can_get_a3_switching_data(self):
        with Transaction().start(self.database) as txn:
            pool = self.openerp.pool
            cursor = txn.cursor
            user = txn.user

            # Arrange
            self.setup_comer(pool, cursor, user)
            tarifa_obj = pool.get('giscegas.polissa.tarifa')
            tariff_id = tarifa_obj.search(cursor, user, [('codi_ocsum', '=', '31')])[0]
            tariff2_id = tarifa_obj.search(cursor, user, [('codi_ocsum', '=', '32')])[0]

            signup_vals = {
                'is_atr_contract_change': False,
                'holder_change': False,
                'has_current_atr_contract': False,
                'current_tariff': tariff_id,
                'current_power': 3450,
                'target_tariff': tariff2_id,
                'target_power': 4600
            }
            result = lucera_signups.tests.utils.create_test_gas_polissa_with_signup(pool, cursor, user, gas_signup_vals=signup_vals)
            polissa_id = result['polissa_id']
            gas_signup_id = result['gas_signup_id']

            # Act
            polissa_obj = pool.get('giscegas.polissa')
            gas_signup_obj = pool.get('lucera.signup.gas')
            result = gas_signup_obj.get_switching_process_data(cursor, user, gas_signup_id)

            # Assert
            expect(result).to_not(be_false)
            expect(result['process_code']).to(equal('A3'))
            expect(result['config_vals']['activacio_cicle']).to(equal('A'))
            expect(result['polissa_vals']['tarifa']).to(equal(tariff2_id))
            expect(result['polissa_vals']['potencia']).to(equal(4.6))
