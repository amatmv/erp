# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from expects import *
from lucera_common.testing import create_test_gas_polissa
import lucera_signups.tests.utils


class SignUpGasPolissaActivationTests(testing.OOTestCase):
    """ Test that a polissa cannot be activated in multiple cases"""

    def test_polissa_is_activable(self):
        with Transaction().start(self.database) as txn:
            pool = self.openerp.pool
            cursor = txn.cursor
            user = txn.user

            # Arrange
            polissa_id = create_test_gas_polissa(pool, cursor, user)['polissa_id']

            # Act
            polissa_obj = self.openerp.pool.get('giscegas.polissa')
            polissa = polissa_obj.browse(cursor, user, polissa_id)

            # Assert
            expect(polissa.has_cups).to(be_true)
            expect(polissa.has_bank_mandate).to(be_true)
            expect(polissa.firmat).to(be_true)
            expect(polissa.is_email_validated).to(be_true)
            expect(polissa.is_activable).to(be_true)

    def test_polissa_is_not_activable_because_it_does_not_have_cups(self):
        with Transaction().start(self.database) as txn:
            pool = self.openerp.pool
            cursor = txn.cursor
            user = txn.user

            # Arrange
            polissa_id = create_test_gas_polissa(pool, cursor, user, has_cups=False)['polissa_id']

            # Act
            polissa_obj = pool.get('giscegas.polissa')
            polissa = polissa_obj.browse(cursor, user, polissa_id)

            # Assert
            expect(polissa.has_cups).to(be_false)
            expect(polissa.is_activable).to(be_false)

    def test_polissa_is_not_activable_because_it_does_not_have_payment_mandate(self):
        with Transaction().start(self.database) as txn:
            pool = self.openerp.pool
            cursor = txn.cursor
            user = txn.user

            # Arrange
            polissa_id = create_test_gas_polissa(pool, cursor, user, has_mandate=False)['polissa_id']

            # Act
            polissa_obj = pool.get('giscegas.polissa')
            polissa = polissa_obj.browse(cursor, user, polissa_id)

            # Assert
            expect(polissa.has_bank_mandate).to(be_false)
            expect(polissa.is_activable).to(be_false)

    def test_polissa_is_activable_although_it_does_not_have_payment_mandate_because_it_is_skipped(self):
        with Transaction().start(self.database) as txn:
            pool = self.openerp.pool
            cursor = txn.cursor
            user = txn.user

            # Arrange
            signup_vals = {'skip_has_bank_mandate': True}
            result = lucera_signups.tests.utils.create_test_gas_polissa_with_signup(pool, cursor, user,
                                                                                    gas_signup_vals=signup_vals,
                                                                                    has_mandate=False)
            polissa_id = result['polissa_id']

            # Act
            polissa_obj = pool.get('giscegas.polissa')
            polissa = polissa_obj.browse(cursor, user, polissa_id)

            # Assert
            expect(polissa.has_bank_mandate).to(be_false)
            expect(polissa.is_activable).to(be_true)

    def test_polissa_is_not_activable_because_it_is_not_signed(self):
        with Transaction().start(self.database) as txn:
            pool = self.openerp.pool
            cursor = txn.cursor
            user = txn.user

            # Arrange
            polissa_id = create_test_gas_polissa(pool, cursor, user, is_signed=False)['polissa_id']

            # Act
            polissa_obj = pool.get('giscegas.polissa')
            polissa = polissa_obj.browse(cursor, user, polissa_id)

            # Assert
            expect(polissa.firmat).to(be_false)
            expect(polissa.is_activable).to(be_false)

    def test_polissa_is_activable_although_it_is_not_signed_because_it_is_skipped(self):
        with Transaction().start(self.database) as txn:
            pool = self.openerp.pool
            cursor = txn.cursor
            user = txn.user

            # Arrange
            signup_vals = {'skip_is_signed': True}
            result = lucera_signups.tests.utils.create_test_gas_polissa_with_signup(pool, cursor, user,
                                                                                    gas_signup_vals=signup_vals,
                                                                                    is_signed=False)
            polissa_id = result['polissa_id']

            # Act
            polissa_obj = pool.get('giscegas.polissa')
            polissa = polissa_obj.browse(cursor, user, polissa_id)

            # Assert
            expect(polissa.firmat).to(be_false)
            expect(polissa.is_activable).to(be_true)

    def test_polissa_is_not_activable_because_email_is_not_validated(self):
        with Transaction().start(self.database) as txn:
            pool = self.openerp.pool
            cursor = txn.cursor
            user = txn.user

            # Arrange
            result = create_test_gas_polissa(pool, cursor, user, is_email_validated=False)
            polissa_id = result['polissa_id']

            # Act
            polissa_obj = pool.get('giscegas.polissa')
            polissa = polissa_obj.browse(cursor, user, polissa_id)

            # Assert
            expect(polissa.is_email_validated).to(be_false)
            expect(polissa.is_activable).to(be_false)

    def test_polissa_is_not_activable_if_has_social_bonus_document_and_does_not_have_waiver(self):
        with Transaction().start(self.database) as txn:
            pool = self.openerp.pool
            cursor = txn.cursor
            user = txn.user

            # Arrange
            signup_vals = {
                'has_social_bonus': True,
                'has_social_bonus_waiver': False
            }

            result = lucera_signups.tests.utils.create_test_gas_polissa_with_signup(pool, cursor, user,
                                                                                    gas_signup_vals=signup_vals)
            polissa_id = result['polissa_id']

            # Act
            polissa_obj = pool.get('giscegas.polissa')
            polissa = polissa_obj.browse(cursor, user, polissa_id)

            # Assert
            expect(polissa.needs_social_bonus_waiver).to(be_true)
            expect(polissa.is_activable).to(be_false)

    def test_polissa_is_activable_although_has_does_not_have_waiver_because_is_skipped(self):
        with Transaction().start(self.database) as txn:
            pool = self.openerp.pool
            cursor = txn.cursor
            user = txn.user

            # Arrange
            signup_vals = {
                'skip_needs_social_bonus_waiver': True,
                'has_social_bonus': True,
                'has_social_bonus_waiver': False
            }

            result = lucera_signups.tests.utils.create_test_gas_polissa_with_signup(pool, cursor, user,
                                                                                    gas_signup_vals=signup_vals)
            polissa_id = result['polissa_id']

            # Act
            polissa_obj = pool.get('giscegas.polissa')
            polissa = polissa_obj.browse(cursor, user, polissa_id)

            # Assert
            expect(polissa.needs_social_bonus_waiver).to(be_true)
            expect(polissa.is_activable).to(be_true)

    def test_polissa_is_not_activable_if_user_has_other_polissa_with_debt(self):
        with Transaction().start(self.database) as txn:
            pool = self.openerp.pool
            cursor = txn.cursor
            user = txn.user

            # Arrange
            signup_vals = {'has_previous_debt': True}
            override_vals = {'debt_amount': 10, 'active': True}
            result_with_debt = create_test_gas_polissa(pool, cursor, user, override_vals=override_vals)
            result = lucera_signups.tests.utils.create_test_gas_polissa_with_signup(pool, cursor, user,
                                                                                    gas_signup_vals=signup_vals,
                                                                                    user_id=result_with_debt['user_id'])
            polissa_id = result['polissa_id']

            # Act
            polissa_obj = pool.get('giscegas.polissa')
            polissa = polissa_obj.browse(cursor, user, polissa_id)

            # Assert
            expect(polissa.gas_signups[0].has_previous_debt).to(be_true)
            expect(polissa.is_activable).to(be_false)

    def test_polissa_is_activable_although_user_has_other_polissa_with_debt_because_is_skipped(self):
        with Transaction().start(self.database) as txn:
            pool = self.openerp.pool
            cursor = txn.cursor
            user = txn.user

            # Arrange
            signup_vals = {'has_previous_debt': True, 'skip_has_previous_debt': True}
            override_vals = {'debt_amount': 10, 'active': True}
            result_with_debt = create_test_gas_polissa(pool, cursor, user, override_vals=override_vals)
            result = lucera_signups.tests.utils.create_test_gas_polissa_with_signup(pool, cursor, user,
                                                                                    gas_signup_vals=signup_vals,
                                                                                    user_id=result_with_debt['user_id'])
            polissa_id = result['polissa_id']

            # Act
            polissa_obj = pool.get('giscegas.polissa')
            polissa = polissa_obj.browse(cursor, user, polissa_id)

            # Assert
            expect(polissa.gas_signups[0].has_previous_debt).to(be_true)
            expect(polissa.is_activable).to(be_true)

    def test_polissa_is_not_activable_if_exists_user_with_same_phone_and_has_debt(self):
        with Transaction().start(self.database) as txn:
            pool = self.openerp.pool
            cursor = txn.cursor
            user = txn.user

            # Arrange
            override_vals = {'debt_amount': 10, 'active': True}
            result_with_debt = create_test_gas_polissa(pool, cursor, user,
                                                       override_vals=override_vals,
                                                       address_data={'email': 'test@test.com', 'phone': '961580184'})
            result = lucera_signups.tests.utils.create_test_gas_polissa_with_signup(pool, cursor, user,
                                                                                    gas_signup_vals={
                                                                                        'has_previous_debt': True},
                                                                                    address_data={
                                                                                        'email': 'prueba@prueba.com',
                                                                                        'phone': '961580184'})
            polissa_id = result['polissa_id']

            # Act
            polissa_obj = pool.get('giscegas.polissa')
            polissa = polissa_obj.browse(cursor, user, polissa_id)

            # Assert
            expect(polissa.gas_signups[0].has_previous_debt).to(be_true)
            expect(polissa.is_activable).to(be_false)

    def test_polissa_is_not_activable_because_it_does_not_have_a_valid_tariff(self):
        with Transaction().start(self.database) as txn:
            pool = self.openerp.pool
            cursor = txn.cursor
            user = txn.user

            # Arrange
            tariff = '005'  # 3.0A
            polissa_id = create_test_gas_polissa(pool, cursor, user, has_cups=False, tariff_code=tariff)['polissa_id']

            # Act
            polissa_obj = pool.get('giscegas.polissa')
            polissa = polissa_obj.browse(cursor, user, polissa_id)

            # Assert
            expect(polissa.has_valid_tariff).to(be_false)
            expect(polissa.is_activable).to(be_false)

    def test_polissa_is_not_activable_because_related_signup_does_not_have_a_valid_tariff(self):
        with Transaction().start(self.database) as txn:
            pool = self.openerp.pool
            cursor = txn.cursor
            user = txn.user

            # Arrange
            result = lucera_signups.tests.utils.create_test_gas_polissa_with_signup(pool, cursor, user)
            polissa_id = result['polissa_id']
            tarifa_obj = pool.get('giscegas.polissa.tarifa')
            tariff_id = tarifa_obj.search(cursor, user, [('codi_ocsum', '=', '005')])[0]
            gas_signup_id = result['gas_signup_id']
            gas_signup_obj = pool.get('lucera.signup.gas')
            gas_signup_obj.write(cursor, user, gas_signup_id, {'target_tariff': tariff_id})

            # Act
            polissa_obj = pool.get('giscegas.polissa')
            polissa = polissa_obj.browse(cursor, user, polissa_id)

            # Assert
            expect(polissa.has_valid_tariff).to(be_false)
            expect(polissa.is_activable).to(be_false)

    def test_polissa_is_activable_because_related_signup_does_not_have_a_valid_tariff_but_skips_checks(self):
        with Transaction().start(self.database) as txn:
            pool = self.openerp.pool
            cursor = txn.cursor
            user = txn.user

            # Arrange
            signup_vals = {
                'skip_has_valid_tariff': True
            }
            tariff = '005'  # 3.0A
            result = lucera_signups.tests.utils.create_test_gas_polissa_with_signup(pool, cursor, user,
                                                                                    gas_signup_vals=signup_vals)
            polissa_id = result['polissa_id']
            tarifa_obj = pool.get('giscegas.polissa.tarifa')
            tariff_id = tarifa_obj.search(cursor, user, [('codi_ocsum', '=', tariff)])[0]
            gas_signup_id = result['gas_signup_id']
            gas_signup_obj = pool.get('lucera.signup.gas')
            gas_signup_obj.write(cursor, user, gas_signup_id, {'target_tariff': tariff_id})

            # Act
            polissa_obj = pool.get('giscegas.polissa')
            polissa = polissa_obj.browse(cursor, user, polissa_id)

            # Assert
            expect(polissa.has_valid_tariff).to(be_false)
            expect(polissa.is_activable).to(be_true)

    def test_polissa_is_not_activable_because_it_does_not_have_a_good_rating(self):
        with Transaction().start(self.database) as txn:
            pool = self.openerp.pool
            cursor = txn.cursor
            user = txn.user

            # Arrange
            result = create_test_gas_polissa(pool, cursor, user)
            polissa_id = result['polissa_id']

            # Act
            polissa_obj = pool.get('giscegas.polissa')
            polissa = polissa_obj.browse(cursor, user, polissa_id)

            request_obj = pool.get('lucera.asnef.request')
            request_obj.create(cursor, user, {
                'vat': polissa.pagador.vat,
                'success': True,
                'present': '00',
                'rating': '1'
            })

            # Assert
            expect(polissa.pagador.has_bad_solvency_rating).to(be_true)
            expect(polissa.is_activable).to(be_false)

    def test_polissa_is_activable_although_has_does_not_have_good_rating_because_is_skipped(self):
        with Transaction().start(self.database) as txn:
            pool = self.openerp.pool
            cursor = txn.cursor
            user = txn.user

            # Arrange
            signup_vals = {'skip_has_bad_solvency_rating': True}
            result = lucera_signups.tests.utils.create_test_gas_polissa_with_signup(pool, cursor, user,
                                                                                    gas_signup_vals=signup_vals)
            polissa_id = result['polissa_id']

            # Act
            polissa_obj = pool.get('giscegas.polissa')
            polissa = polissa_obj.browse(cursor, user, polissa_id)

            request_obj = pool.get('lucera.asnef.request')
            request_obj.create(cursor, user, {
                'vat': polissa.pagador.vat,
                'success': True,
                'present': '00',
                'rating': '1'
            })

            # Assert
            expect(polissa.pagador.has_bad_solvency_rating).to(be_true)
            expect(polissa.is_activable).to(be_true)
