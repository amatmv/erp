# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from expects import *
from osv import osv
from lucera_common.testing import create_test_electricity_polissa
from utils import get_serialized_signup, setup_tarifa
import json
import mock
from lucera_signups import giscedata_polissa


class SignUpSignatureTests(testing.OOTestCase):

    @mock.patch.object(giscedata_polissa.GiscedataPolissa, '_create_asnef_request')
    def test_can_sign_by_email(self, mock_method):
        with Transaction().start(self.database) as txn:
            # Arrange
            pool = self.openerp.pool
            cursor = txn.cursor
            user = txn.user
            result = create_test_electricity_polissa(pool, cursor, user, is_signed=False)
            polissa_id = result['polissa_id']
            polissa_obj = pool.get('giscedata.polissa')
            bus_message_obj = pool.get('lucera.bus.message')

            polissa = polissa_obj.browse(cursor, user, polissa_id)
            email = polissa.direccio_notificacio.email

            # Act
            polissa_obj.sign_by_email(cursor, user, polissa_id, email, '192.168.0.1')

            # Assert
            polissa = polissa_obj.browse(cursor, user, polissa_id)
            expect(polissa.data_firma_contracte).not_to(be_false)
            expect(polissa.signature_method).to(equal('email'))
            expect(polissa.signature_data).to(equal('192.168.0.1'))

            bus_message_ids = bus_message_obj.search(cursor, user,
                                                     [('name', '=', 'giscedata.polissa.signed')],
                                                     order='create_date desc')
            expect(bus_message_ids).not_to(be_empty)
            bus_message = bus_message_obj.browse(cursor, user, bus_message_ids[0])
            expect(bus_message.name).to(equal('giscedata.polissa.signed'))
            expect(str(polissa_id) in bus_message.content).to(be_true)

            mock_method.assert_called()

    def test_cannot_sign_by_email_when_already_signed(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            pool = self.openerp.pool
            cursor = txn.cursor
            user = txn.user
            result = create_test_electricity_polissa(pool, cursor, user, is_signed=True)
            polissa_id = result['polissa_id']
            polissa_obj = pool.get('giscedata.polissa')
            polissa = polissa_obj.browse(cursor, user, polissa_id)
            email = polissa.direccio_notificacio.email

            # Act
            with self.assertRaises(osv.except_osv) as cm:
                polissa_obj.sign_by_email(cursor, user, polissa_id, email, '192.168.0.1')

            # Assert
            expect(cm.exception.name).to(be('ValidateError'))

    def test_cannot_sign_by_email_not_equal_to_notification_email(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            pool = self.openerp.pool
            cursor = txn.cursor
            user = txn.user
            result = create_test_electricity_polissa(pool, cursor, user, is_signed=False)
            polissa_id = result['polissa_id']
            polissa_obj = pool.get('giscedata.polissa')

            # Act
            email = 'not_equal_email@gmail.com'
            with self.assertRaises(osv.except_osv) as cm:
                polissa_obj.sign_by_email(cursor, user, polissa_id, email, '192.168.0.1')

            # Assert
            expect(cm.exception.name).to(be('SecurityError'))

    @mock.patch.object(giscedata_polissa.GiscedataPolissa, '_create_asnef_request')
    def test_can_add_signature_attribution_when_attribution_already_exists(self, mock_method):
        with Transaction().start(self.database) as txn:
            # Arrange
            pool = self.openerp.pool
            cursor = txn.cursor
            user = txn.user
            setup_tarifa(pool, cursor, user)
            content = get_serialized_signup('default.json')
            signup_json = json.loads(content)
            vals = {
                'signup_serialized': content
            }
            signup_obj = pool.get('lucera.signup')
            signup_request_obj = pool.get('lucera.signup.request')
            signup_request_id = signup_request_obj.create(cursor, user, vals)
            results = signup_request_obj.commit(cursor, user, [signup_request_id])
            result = results[signup_request_id]
            signup_id = result['signup_id']
            polissa_id = result['polissa_id']
            polissa_obj = pool.get('giscedata.polissa')
            polissa = polissa_obj.browse(cursor, user, polissa_id)

            # patcher = mock.patch.object(polissa, '_create_asnef_request')
            # patcher.start()

            email = polissa.direccio_notificacio.email
            polissa_obj.sign_by_email(cursor, user, polissa_id, email, '192.168.0.1')

            attribution = """
            {                
                  "route": [
                    {
                      "source": "source4",
                      "medium": "medium4",
                      "term": "term4",
                      "content": "content4",
                      "name": "name4",
                      "identifier": "id4",
                      "date": "2018-02-01T02:02:02+01:00"
                    }
                  ]
            }"""

            # Act
            signup_obj = pool.get('lucera.signup')
            result = signup_obj.add_signature_attribution(cursor, user, signup_id, attribution)

            # Assert
            expect(result).to(be_true)
            signup = pool.get('giscedata.polissa').browse(cursor, user, polissa_id).electricity_signups[0].signups[0]
            expect(signup.signature_attribution_serialized).to(equal(attribution))

            via = signup_json['via']

            # No new attributions are added
            expect(len(signup.attributions)).to(equal(2))

            # Not replaced attribution
            expect(signup.attributions[0].source).to(equal('source1'))
            expect(signup.attributions[0].medium).to(equal('medium1'))
            expect(signup.attributions[0].term).to(equal('term1'))
            expect(signup.attributions[0].content).to(equal('content1'))
            expect(signup.attributions[0].name).to(equal('name1'))
            expect(signup.attributions[0].identifier).to(equal('id1'))
            expect(signup.attributions[0].date).to(equal('2018-02-15 03:03:03'))

            mock_method.assert_called()

    @mock.patch.object(giscedata_polissa.GiscedataPolissa, '_create_asnef_request')
    def test_can_add_signature_attribution_when_attribution_does_not_exists(self, mock_method):
        with Transaction().start(self.database) as txn:
            # Arrange
            pool = self.openerp.pool
            cursor = txn.cursor
            user = txn.user
            setup_tarifa(pool, cursor, user)
            content = get_serialized_signup('default.json')
            signup_json = json.loads(content)
            del(signup_json['via']['attribution'])
            content = json.dumps(signup_json)
            vals = {
                'signup_serialized': content
            }
            signup_request_obj = pool.get('lucera.signup.request')
            signup_obj = pool.get('lucera.signup')
            signup_request_id = signup_request_obj.create(cursor, user, vals)
            results = signup_request_obj.commit(cursor, user, [signup_request_id])
            result = results[signup_request_id]
            expect(result).not_to(be_false)
            signup_id = result['signup_id']
            polissa_id = result['polissa_id']
            polissa_obj = pool.get('giscedata.polissa')
            polissa = polissa_obj.browse(cursor, user, polissa_id)
            email = polissa.direccio_notificacio.email
            polissa_obj.sign_by_email(cursor, user, polissa_id, email, '192.168.0.1')

            attribution = """
            {                
                  "route": [
                    {
                      "source": "source3",
                      "medium": "medium3",
                      "term": "term3",
                      "content": "content3",
                      "name": "name3",
                      "identifier": "id3",
                      "date": "2018-02-02T03:03:03+01:00"
                    },
                    {
                      "source": "source2",
                      "medium": "medium2",
                      "term": "term2",
                      "content": "content2",
                      "name": "name2",
                      "identifier": "id2",
                      "date": "2018-02-02T02:02:02+01:00"
                    }
                  ]
            }"""

            # Act
            signup_obj = pool.get('lucera.signup')
            result = signup_obj.add_signature_attribution(cursor, user,signup_id, attribution)

            # Assert
            expect(result).to(be_true)
            signup = signup_obj.browse(cursor, user, signup_id)
            expect(signup.signature_attribution_serialized).to(equal(attribution))

            # Attribution is added
            expect(signup.attributions[0].source).to(equal('source2'))
            expect(signup.attributions[0].medium).to(equal('medium2'))
            expect(signup.attributions[0].term).to(equal('term2'))
            expect(signup.attributions[0].content).to(equal('content2'))
            expect(signup.attributions[0].name).to(equal('name2'))
            expect(signup.attributions[0].identifier).to(equal('id2'))
            expect(signup.attributions[0].date).to(equal('2018-02-02 02:02:02'))

            mock_method.assert_called()
