# -*- coding: utf-8 -*-
import unittest
from destral import testing
from destral.transaction import Transaction
from expects import *
from lucera_common.config import *
from lucera_common.fake import *
from lucera_common.normalize import normalize_postal_code, normalize_street_name
from osv import osv, fields, orm
from lucera_common.testing import create_partner_and_address
from lucera_common.testing.common import create_bank_account
from lucera_common.testing.gas import create_test_gas_polissa
from lucera_common.testing.electricity import create_test_electricity_polissa
from utils import get_serialized_signup

import os
import json


# LUCERA_PRODUCT_PRICE_LIST_NAME = 'Tarifas Lucera'

class SignUpCommitterTests(testing.OOTestCase):
    def _setup(self, cursor, uid):
        user = self.openerp.pool.get('res.users').browse(cursor, uid, uid)
        if not user.company_id.partner_id.ref:
            partner_id = self.openerp.pool.get('ir.model.data').get_object_reference(
                cursor, uid, 'base', 'main_partner'
            )[1]
            self.openerp.pool.get('res.partner').write(cursor, uid, [partner_id], {
                'ref': '0971'
            })

    def _create_user_and_address_from_signup(self, content, is_end_user=True, txn=None):
        if not txn:
            txn = self.txn

        if isinstance(content, basestring):
            signup = json.loads(content)
        else:
            signup = content

        user_data = {'name': '{}, {}'.format(signup['user']['surnames'], signup['user']['name']),
                     'vat': signup['user']['identity_document']['code'],
                     'is_end_user': is_end_user}
        address_data = {'email': signup['user']['email'],
                        'phone': signup['user']['phone']}
        return create_partner_and_address(self.openerp.pool, txn.cursor, txn.user, user_data, address_data)

    def new_transaction(self):
        self.txn = Transaction().start(self.database)
        return self.txn

    @staticmethod
    def _check_result(cursor, uid, result, electricity=True, gas=False):
        expect(result['signup_id']).to(be_above(0))
        expect(result['user_id']).to(be_above(0))
        expect(result['user_address_id']).to(be_above(0))
        expect(result['fiscal_customer_id']).to(be_above(0))
        expect(result['fiscal_customer_address_id']).to(be_above(0))
        expect(result['bank_account_owner_id']).to(be_above(0))
        expect(result['bank_account_owner_address_id']).to(be_above(0))
        expect(result['bank_account_id']).to(be_above(0))
        if electricity:
            expect(result['polissa_id']).to(be_above(0))
            expect(result['cups_id']).to(be_above(0))
        if gas:
            expect(result['gas_polissa_id']).to(be_above(0))
            expect(result['gas_cups_id']).to(be_above(0))
        expect(result['external_id']).to(be_above(0))

    def test_can_create_gas_only_signup(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            user = txn.user
            self._setup(cursor, user)
            content = get_serialized_signup('gas.json')
            signup_json = json.loads(content)

            # Act
            signup_committer_obj = self.openerp.pool.get('lucera.signup.committer')
            result = signup_committer_obj.commit(cursor, user, content)

            # Assert
            self._check_result(cursor, user, result, electricity=False, gas=True)

            # Signup
            signup_obj = self.openerp.pool.get('lucera.signup')
            signup = signup_obj.browse(cursor, user, result['signup_id'])
            expect(signup.gas_signup_id.polissa_id.id).to(equal(result['gas_polissa_id']))
            expect(signup.external_id).to(equal(str(signup_json['id'])))

            # Gas tariff
            gas_tariff_code = signup_json['gas']['product']['access_contract']['current']['tariff_code']
            gas_tariff_id = self.openerp.pool.get('giscegas.polissa.tarifa').search(cursor, user,
                                                                            [('codi_ocsum', '=', gas_tariff_code)])[0]
            # Contract
            gas_polissa = self.openerp.pool.get('giscegas.polissa').browse(cursor, user, result['gas_polissa_id'])
            expect(gas_polissa.cups.id).to(equal(result['gas_cups_id']))
            expect(gas_polissa.cups.name).to(equal(signup_json['gas']['supply_point']['code']))
            expect(gas_polissa.active).to(equal(True))
            expect(gas_polissa.tarifa.id).to(equal(gas_tariff_id))
            expect(gas_polissa.caudal_diario).to(be_above(0))
            expect(gas_polissa.titular.id).to(equal(result['user_id']))
            expect(gas_polissa.titular.vat).to(equal('ES' + signup_json['user']['identity_document']['code']))
            expect(gas_polissa.titular.name).to(equal('De la Calzada, Chiquito'))
            expect(gas_polissa.titular.address[0].email).to(equal(signup_json['user']['email'].lower()))
            expect(gas_polissa.titular.address[0].phone).to(equal(signup_json['user']['phone']))
            expect(gas_polissa.state).to(equal('esborrany'))
            expect(gas_polissa.data_firma_contracte).to(equal(False))
            expect(gas_polissa.tipus_vivenda).to(equal('habitual'))
            expect(gas_polissa.cnae.id).to(equal(986))
            expect(gas_polissa.data_baixa).to(equal(False))
            expect(gas_polissa.tipo_pago.code).to(equal('RECIBO_CSB'))
            expect(gas_polissa.direccio_pagament.id).to(equal(result['user_address_id']))
            expect(gas_polissa.pagador.id).to(equal(result['user_id']))
            expect(gas_polissa.pagador_sel).to(equal('titular'))
            expect(gas_polissa.direccio_notificacio.id).to(equal(result['user_address_id']))
            expect(gas_polissa.direccio_notificacio.partner_id.analytics_user_id).to(equal('123.456'))
            expect(gas_polissa.payment_mode_id.name).to(equal('Recibo mensual BBVA'))
            expect(gas_polissa.llista_preu.name).to(equal('Lucera Precios Variables Gas'))
            expect(gas_polissa.llista_preu.type).to(equal('sale'))
            expect(gas_polissa.llista_preu.kind).to(equal('variable_price'))
            expect(gas_polissa.bank.id).to(equal(result['bank_account_id']))
            expect(gas_polissa.bank.iban).to(equal('ES8800303123410000260271'))
            expect(gas_polissa.facturacio).to(equal(1))
            expect(gas_polissa.notificacio).to(equal('pagador'))
            expect(gas_polissa.altre_p.id).to(be_false)
            expect(gas_polissa.affiliate_code).to(equal(signup_json['promotion']['affiliate']['code']))
            expect(gas_polissa.affiliate_external_id).to(equal(signup_json['promotion']['affiliate']['external_id']))
            expect(gas_polissa.pressio).to(equal(0))
            # expect(polissa.tipo_uso_gas).to(equal('01'))  # TODO: Doméstico TAULA_TIPOS_DE_USO_DEL_GAS_EXTENDED
            expect(gas_polissa.property_unitat_terme_fix.name).to(equal(LUCERA_GAS_FIXED_TERM_UOM_NAME))
            expect(gas_polissa.observacions).to(be_false)
            expect(gas_polissa.versio_primera_factura.id).to(be_false)
            expect(gas_polissa.electricity_polissa_id.id).to(be_false)

            # Mandate
            mandates_id = self.openerp.pool.get('payment.mandate').search(
                cursor, user, [('reference', '=', 'giscegas.polissa,%s' % gas_polissa.id)])
            expect(mandates_id).not_to(be_empty)
            expect(mandates_id[0]).to(be_above(0))

            # Bus message
            bus_message_obj = self.openerp.pool.get('lucera.bus.message')
            bus_message_ids = bus_message_obj.search(cursor, user,
                                                     [('name', '=', 'giscegas.polissa.created')],
                                                     order='create_date desc')
            expect(bus_message_ids).not_to(be_empty)
            bus_message = bus_message_obj.browse(cursor, user, bus_message_ids[0])
            expect(bus_message.name).to(equal('giscegas.polissa.created'))
            expect(str(result['gas_polissa_id']) in bus_message.content).to(be_true)

    def test_can_create_gas_only_signup_when_fiscal_customer_is_different_from_user(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            user = txn.user
            self._setup(cursor, user)
            content = get_serialized_signup('gas.json')
            signup_json = json.loads(content)
            signup_json['fiscal_customer']['rel'] = 'none'
            signup_json['fiscal_customer']['type'] = 'individual'
            signup_json['fiscal_customer']['name'] = 'Fiscal'
            signup_json['fiscal_customer']['surnames'] = 'Customer'
            signup_json['fiscal_customer']['identity_document'] = {
                'code': '31118100C',
                'type': 'dni'
            }
            signup_json['fiscal_customer']['user_declares_representative_permission'] = True
            signup_json['fiscal_customer']['location'] = {
                'rel': 'supply_point'
            }
            content = json.dumps(signup_json, indent=4)

            # Act
            signup_committer_obj = self.openerp.pool.get('lucera.signup.committer')
            result = signup_committer_obj.commit(cursor, user, content)

            # Assert
            self._check_result(cursor, user, result, electricity=False, gas=True)

            # Signup
            signup_obj = self.openerp.pool.get('lucera.signup')
            signup = signup_obj.browse(cursor, user, result['signup_id'])
            expect(signup.gas_signup_id.polissa_id.id).to(equal(result['gas_polissa_id']))
            expect(signup.external_id).to(equal(str(signup_json['id'])))

            # Gas tariff
            gas_tariff_code = signup_json['gas']['product']['access_contract']['current']['tariff_code']
            gas_tariff_id = self.openerp.pool.get('giscegas.polissa.tarifa').search(cursor, user,
                                                                            [('codi_ocsum', '=', gas_tariff_code)])[0]
            # Contract
            gas_polissa = self.openerp.pool.get('giscegas.polissa').browse(cursor, user, result['gas_polissa_id'])
            expect(gas_polissa.state).to(equal('esborrany'))
            expect(gas_polissa.data_firma_contracte).to(equal(False))
            expect(gas_polissa.tipus_vivenda).to(equal('habitual'))
            expect(gas_polissa.cnae.id).to(equal(986))
            expect(gas_polissa.data_baixa).to(equal(False))
            expect(gas_polissa.tipo_pago.code).to(equal('RECIBO_CSB'))
            expect(gas_polissa.direccio_pagament.id).to(equal(result['fiscal_customer_address_id']))
            expect(gas_polissa.direccio_pagament.id).to(be_above(0))
            expect(gas_polissa.pagador.id).to(equal(result['fiscal_customer_id']))
            expect(gas_polissa.pagador.id).to(be_above(0))
            expect(gas_polissa.pagador.id).not_to(equal(result['user_id']))
            expect(gas_polissa.cups.id).to(equal(result['gas_cups_id']))
            expect(gas_polissa.cups.name).to(equal(signup_json['gas']['supply_point']['code']))
            expect(gas_polissa.active).to(equal(True))
            expect(gas_polissa.tarifa.id).to(equal(gas_tariff_id))
            expect(gas_polissa.caudal_diario).to(be_above(0))
            expect(gas_polissa.titular.id).to(equal(result['fiscal_customer_id']))
            expect(gas_polissa.titular.id).not_to(equal(result['user_id']))
            expect(gas_polissa.titular.id).to(be_above(0))
            expect(gas_polissa.titular.vat).to(equal('ES' + signup_json['fiscal_customer']['identity_document']['code']))
            expect(gas_polissa.titular.name).to(equal('Customer, Fiscal'))
            # expect(gas_polissa.titular.address[0].email).to(equal(signup_json['user']['email'].lower()))
            # expect(gas_polissa.titular.address[0].phone).to(equal(signup_json['user']['phone']))
            expect(gas_polissa.state).to(equal('esborrany'))
            expect(gas_polissa.data_firma_contracte).to(equal(False))
            expect(gas_polissa.tipus_vivenda).to(equal('habitual'))
            expect(gas_polissa.cnae.id).to(equal(986))
            expect(gas_polissa.data_baixa).to(equal(False))
            expect(gas_polissa.tipo_pago.code).to(equal('RECIBO_CSB'))
            expect(gas_polissa.direccio_pagament.id).to(equal(result['fiscal_customer_address_id']))
            expect(gas_polissa.direccio_pagament.id).to(be_above(0))
            expect(gas_polissa.direccio_pagament.id).not_to(equal(result['user_address_id']))
            expect(gas_polissa.pagador.id).to(equal(result['fiscal_customer_id']))
            expect(gas_polissa.pagador.id).to(be_above(0))
            expect(gas_polissa.pagador_sel).to(equal('titular'))
            expect(gas_polissa.direccio_notificacio.id).to(equal(result['user_address_id']))
            expect(gas_polissa.direccio_notificacio.partner_id.analytics_user_id).to(equal('123.456'))
            expect(gas_polissa.payment_mode_id.name).to(equal('Recibo mensual BBVA'))
            expect(gas_polissa.llista_preu.name).to(equal('Lucera Precios Variables Gas'))
            expect(gas_polissa.llista_preu.type).to(equal('sale'))
            expect(gas_polissa.llista_preu.kind).to(equal('variable_price'))
            expect(gas_polissa.bank.id).to(equal(result['bank_account_id']))
            expect(gas_polissa.bank.iban).to(equal('ES8800303123410000260271'))
            expect(gas_polissa.facturacio).to(equal(1))
            expect(gas_polissa.notificacio).to(equal('altre_p'))
            expect(gas_polissa.altre_p.id).to(equal(result['user_id']))
            expect(gas_polissa.affiliate_code).to(equal(signup_json['promotion']['affiliate']['code']))
            expect(gas_polissa.affiliate_external_id).to(equal(signup_json['promotion']['affiliate']['external_id']))
            expect(gas_polissa.pressio).to(equal(0))
            # expect(polissa.tipo_uso_gas).to(equal('01'))  # TODO: Doméstico TAULA_TIPOS_DE_USO_DEL_GAS_EXTENDED
            expect(gas_polissa.property_unitat_terme_fix.name).to(equal(LUCERA_GAS_FIXED_TERM_UOM_NAME))
            expect(gas_polissa.observacions).to(be_false)
            expect(gas_polissa.versio_primera_factura.id).to(be_false)
            expect(gas_polissa.electricity_polissa_id.id).to(be_false)

            # Mandate
            mandates_id = self.openerp.pool.get('payment.mandate').search(
                cursor, user, [('reference', '=', 'giscegas.polissa,%s' % gas_polissa.id)])
            expect(mandates_id).not_to(be_empty)
            expect(mandates_id[0]).to(be_above(0))

            # Bus message
            bus_message_obj = self.openerp.pool.get('lucera.bus.message')
            bus_message_ids = bus_message_obj.search(cursor, user,
                                                     [('name', '=', 'giscegas.polissa.created')],
                                                     order='create_date desc')
            expect(bus_message_ids).not_to(be_empty)
            bus_message = bus_message_obj.browse(cursor, user, bus_message_ids[0])
            expect(bus_message.name).to(equal('giscegas.polissa.created'))
            expect(str(result['gas_polissa_id']) in bus_message.content).to(be_true)

    def test_can_create_gas_only_signup_linked_to_electricity_contract(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            user = txn.user
            pool = self.openerp.pool
            self._setup(cursor, user)
            electricity_result = create_test_electricity_polissa(pool, txn.cursor, txn.user)
            electricity_polissa_id = electricity_result['polissa_id']
            user_id = electricity_result['user_id']

            content = get_serialized_signup('gas.json')
            signup_json = json.loads(content)
            signup_json['user'] = {
                'rel': 'existing',
                'id': user_id
            }
            signup_json['gas']['related_electricity_contract_id'] = electricity_polissa_id

            # Act
            signup_committer_obj = self.openerp.pool.get('lucera.signup.committer')
            result = signup_committer_obj.commit(cursor, user, signup_json)

            # Assert
            self._check_result(cursor, user, result, electricity=False, gas=True)

            # Signup
            signup_obj = self.openerp.pool.get('lucera.signup')
            signup = signup_obj.browse(cursor, user, result['signup_id'])
            expect(signup.gas_signup_id.polissa_id.id).to(equal(result['gas_polissa_id']))
            expect(signup.electricity_signup_id.id).to(be_false)
            expect(signup.external_id).to(equal(str(signup_json['id'])))

            # Gas Contract
            gas_polissa = self.openerp.pool.get('giscegas.polissa').browse(cursor, user, result['gas_polissa_id'])
            expect(gas_polissa.electricity_polissa_id.id).to(equal(electricity_polissa_id))

            # Electricity contract
            electricity_polissa = self.openerp.pool.get('giscedata.polissa').browse(cursor, user, electricity_polissa_id)
            expect(electricity_polissa.gas_polissa_id.id).to(equal(result['gas_polissa_id']))

    def test_can_create_electricity_only_signup_linked_to_gas_contract(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            user = txn.user
            pool = self.openerp.pool
            self._setup(cursor, user)
            content = get_serialized_signup('default.json')
            gas_result = create_test_gas_polissa(pool, txn.cursor, txn.user)
            gas_polissa_id = gas_result['gas_polissa_id']
            user_id = gas_result['user_id']
            signup_json = json.loads(content)
            signup_json['user'] = {'rel': 'existing', 'id': user_id }
            signup_json['electricity']['related_gas_contract_id'] = gas_polissa_id

            # Act
            signup_committer_obj = self.openerp.pool.get('lucera.signup.committer')
            result = signup_committer_obj.commit(cursor, user, signup_json)

            # Assert
            self._check_result(cursor, user, result, electricity=True, gas=False)

            # Signup
            signup_obj = self.openerp.pool.get('lucera.signup')
            signup = signup_obj.browse(cursor, user, result['signup_id'])
            expect(signup.gas_signup_id.id).to(be_false)
            expect(signup.electricity_signup_id.polissa_id.id).to(equal(result['polissa_id']))
            expect(signup.external_id).to(equal(str(signup_json['id'])))

            # Gas Contract
            gas_polissa = self.openerp.pool.get('giscegas.polissa').browse(cursor, user, gas_polissa_id)
            expect(gas_polissa.electricity_polissa_id.id).to(equal(result['polissa_id']))

            # Electricity contract
            electricity_polissa = self.openerp.pool.get('giscedata.polissa').browse(cursor, user, result['polissa_id'])
            expect(electricity_polissa.gas_polissa_id.id).to(equal(gas_polissa_id))

    def test_can_create_electricity_and_gas_signup(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            user = txn.user
            self._setup(cursor, user)
            content = get_serialized_signup('electricity_and_gas.json')
            signup_json = json.loads(content)

            # Act
            signup_committer_obj = self.openerp.pool.get('lucera.signup.committer')
            result = signup_committer_obj.commit(cursor, user, content)

            # Assert
            self._check_result(cursor, user, result, electricity=True, gas=True)

            # Signup
            signup_obj = self.openerp.pool.get('lucera.signup')
            signup = signup_obj.browse(cursor, user, result['signup_id'])
            expect(signup.gas_signup_id.polissa_id.id).to(equal(result['gas_polissa_id']))
            expect(signup.external_id).to(equal(str(signup_json['id'])))

            # Electricity Contract
            electricity_polissa = self.openerp.pool.get('giscedata.polissa').browse(cursor, user, result['polissa_id'])
            expect(electricity_polissa.cups.id).to(equal(result['cups_id']))
            expect(electricity_polissa.cups.name).to(equal(signup_json['electricity']['supply_point']['code']))
            expect(electricity_polissa.active).to(equal(True))
            expect(electricity_polissa.tarifa.id).to(be_false)
            expect(electricity_polissa.titular.id).to(equal(result['user_id']))
            expect(electricity_polissa.titular.vat).to(equal('ES' + signup_json['user']['identity_document']['code']))
            expect(electricity_polissa.titular.name).to(equal('De la Calzada, Chiquito'))
            expect(electricity_polissa.titular.address[0].email).to(equal(signup_json['user']['email'].lower()))
            expect(electricity_polissa.titular.address[0].phone).to(equal(signup_json['user']['phone']))
            expect(electricity_polissa.state).to(equal('esborrany'))
            expect(electricity_polissa.data_firma_contracte).to(equal(False))
            expect(electricity_polissa.tipus_vivenda).to(equal('habitual'))
            expect(electricity_polissa.cnae.id).to(equal(986))
            expect(electricity_polissa.data_baixa).to(equal(False))
            expect(electricity_polissa.tipo_pago.code).to(equal('RECIBO_CSB'))
            expect(electricity_polissa.direccio_pagament.id).to(equal(result['user_address_id']))
            expect(electricity_polissa.pagador.id).to(equal(result['user_id']))
            expect(electricity_polissa.pagador_sel).to(equal('titular'))
            expect(electricity_polissa.direccio_notificacio.id).to(equal(result['user_address_id']))
            expect(electricity_polissa.direccio_notificacio.partner_id.analytics_user_id).to(equal('123.456'))
            expect(electricity_polissa.payment_mode_id.name).to(equal('Recibo mensual BBVA'))
            expect(electricity_polissa.llista_preu.name).to(equal('Lucera Precios Variables'))
            expect(electricity_polissa.llista_preu.type).to(equal('sale'))
            expect(electricity_polissa.llista_preu.kind).to(equal('variable_price'))
            expect(electricity_polissa.bank.id).to(equal(result['bank_account_id']))
            expect(electricity_polissa.bank.iban).to(equal('ES8800303123410000260271'))
            expect(electricity_polissa.facturacio).to(equal(1))
            expect(electricity_polissa.facturacio_potencia).to(equal('icp'))
            expect(electricity_polissa.notificacio).to(equal('pagador'))
            expect(electricity_polissa.altre_p.id).to(be_false)
            expect(electricity_polissa.affiliate_code).to(equal(signup_json['promotion']['affiliate']['code']))
            expect(electricity_polissa.affiliate_external_id).to(equal(signup_json['promotion']['affiliate']['external_id']))
            expect(electricity_polissa.potencia).to(equal(0))
            expect(electricity_polissa.observacions).to(be_false)
            expect(electricity_polissa.versio_primera_factura.id).to(be_false)
            expect(electricity_polissa.gas_polissa_id.id).to(equal(result['gas_polissa_id']))

            # Mandate
            electricity_mandate_id = self.openerp.pool.get('payment.mandate').search(
                cursor, user, [('reference', '=', 'giscedata.polissa,%s' % electricity_polissa.id)])
            expect(electricity_mandate_id).not_to(be_empty)
            expect(electricity_mandate_id[0]).to(be_above(0))

            # Bus message
            bus_message_obj = self.openerp.pool.get('lucera.bus.message')
            bus_message_ids = bus_message_obj.search(cursor, user,
                                                     [('name', '=', 'giscedata.polissa.created')],
                                                     order='create_date desc')
            expect(bus_message_ids).not_to(be_empty)
            bus_message = bus_message_obj.browse(cursor, user, bus_message_ids[0])
            expect(bus_message.name).to(equal('giscedata.polissa.created'))
            expect(str(result['polissa_id']) in bus_message.content).to(be_true)

            # Gas Tariff
            gas_tariff_code = signup_json['gas']['product']['access_contract']['current']['tariff_code']
            gas_tariff_id = self.openerp.pool.get('giscegas.polissa.tarifa').search(cursor, user,
                                                                            [('codi_ocsum', '=', gas_tariff_code)])[0]
            # Gas Contract
            gas_polissa = self.openerp.pool.get('giscegas.polissa').browse(cursor, user, result['gas_polissa_id'])
            expect(gas_polissa.cups.id).to(equal(result['gas_cups_id']))
            expect(gas_polissa.cups.name).to(equal(signup_json['gas']['supply_point']['code']))
            expect(gas_polissa.active).to(equal(True))
            expect(gas_polissa.tarifa.id).to(equal(gas_tariff_id))
            expect(gas_polissa.caudal_diario).to(be_above(0))
            expect(gas_polissa.titular.id).to(equal(result['user_id']))
            expect(gas_polissa.titular.vat).to(equal('ES' + signup_json['user']['identity_document']['code']))
            expect(gas_polissa.titular.name).to(equal('De la Calzada, Chiquito'))
            expect(gas_polissa.titular.address[0].email).to(equal(signup_json['user']['email'].lower()))
            expect(gas_polissa.titular.address[0].phone).to(equal(signup_json['user']['phone']))
            expect(gas_polissa.state).to(equal('esborrany'))
            expect(gas_polissa.data_firma_contracte).to(equal(False))
            expect(gas_polissa.tipus_vivenda).to(equal('habitual'))
            expect(gas_polissa.cnae.id).to(equal(986))
            expect(gas_polissa.data_baixa).to(equal(False))
            expect(gas_polissa.tipo_pago.code).to(equal('RECIBO_CSB'))
            expect(gas_polissa.direccio_pagament.id).to(equal(result['user_address_id']))
            expect(gas_polissa.pagador.id).to(equal(result['user_id']))
            expect(gas_polissa.pagador_sel).to(equal('titular'))
            expect(gas_polissa.direccio_notificacio.id).to(equal(result['user_address_id']))
            expect(gas_polissa.direccio_notificacio.partner_id.analytics_user_id).to(equal('123.456'))
            expect(gas_polissa.payment_mode_id.name).to(equal('Recibo mensual BBVA'))
            expect(gas_polissa.llista_preu.name).to(equal('Lucera Precios Variables Gas'))
            expect(gas_polissa.llista_preu.type).to(equal('sale'))
            expect(gas_polissa.llista_preu.kind).to(equal('variable_price'))
            expect(gas_polissa.bank.id).to(equal(result['bank_account_id']))
            expect(gas_polissa.bank.iban).to(equal('ES8800303123410000260271'))
            expect(gas_polissa.facturacio).to(equal(1))
            expect(gas_polissa.notificacio).to(equal('pagador'))
            expect(gas_polissa.altre_p.id).to(be_false)
            expect(gas_polissa.affiliate_code).to(equal(signup_json['promotion']['affiliate']['code']))
            expect(gas_polissa.affiliate_external_id).to(equal(signup_json['promotion']['affiliate']['external_id']))
            expect(gas_polissa.pressio).to(equal(0))
            # expect(polissa.tipo_uso_gas).to(equal('01'))  # TODO: Doméstico TAULA_TIPOS_DE_USO_DEL_GAS_EXTENDED
            expect(gas_polissa.observacions).to(be_false)
            expect(gas_polissa.versio_primera_factura.id).to(be_false)
            expect(gas_polissa.electricity_polissa_id.id).to(equal(result['polissa_id']))

            # Mandate
            gas_mandate_id = self.openerp.pool.get('payment.mandate').search(
                cursor, user, [('reference', '=', 'giscegas.polissa,%s' % gas_polissa.id)])
            expect(gas_mandate_id).not_to(be_empty)
            expect(gas_mandate_id[0]).to(be_above(0))

            # Bus message
            bus_message_obj = self.openerp.pool.get('lucera.bus.message')
            bus_message_ids = bus_message_obj.search(cursor, user,
                                                     [('name', '=', 'giscegas.polissa.created')],
                                                     order='create_date desc')
            expect(bus_message_ids).not_to(be_empty)
            bus_message = bus_message_obj.browse(cursor, user, bus_message_ids[0])
            expect(bus_message.name).to(equal('giscegas.polissa.created'))
            expect(str(result['gas_polissa_id']) in bus_message.content).to(be_true)

    def test_can_create_signup_when_fiscal_customer_is_different_from_user(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            user = txn.user
            self._setup(cursor, user)
            content = get_serialized_signup('default.json')
            signup_json = json.loads(content)
            signup_json['fiscal_customer']['rel'] = 'none'
            signup_json['fiscal_customer']['type'] = 'individual'
            signup_json['fiscal_customer']['name'] = 'Fiscal'
            signup_json['fiscal_customer']['surnames'] = 'Customer'
            signup_json['fiscal_customer']['identity_document'] = {
                'code': '31118100C',
                'type': 'dni'
            }
            signup_json['fiscal_customer']['user_declares_representative_permission'] = True
            signup_json['fiscal_customer']['location'] = {
                'rel': 'supply_point'
            }
            signup_serialized = json.dumps(signup_json, indent=4)

            # Act
            signup_committer_obj = self.openerp.pool.get('lucera.signup.committer')
            result = signup_committer_obj.commit(cursor, user, signup_serialized)

            # Assert
            self._check_result(cursor, user, result)

            # Signup
            signup_obj = self.openerp.pool.get('lucera.signup')
            signup = signup_obj.browse(cursor, user, result['signup_id'])
            expect(signup.electricity_signup_id.polissa_id.id).to(equal(result['polissa_id']))
            expect(signup.electricity_signup_id.supply_point_code_source).to(equal('invoice_study'))
            expect(signup.external_id).to(equal(str(signup_json['id'])))

            # Contract
            polissa = self.openerp.pool.get('giscedata.polissa').browse(cursor, user, result['polissa_id'])
            expect(polissa.cups.id).to(equal(result['cups_id']))
            expect(polissa.cups.name).to(equal(signup_json['electricity']['supply_point']['code']))
            expect(polissa.active).to(equal(True))
            expect(polissa.tarifa.id).to(be_false)
            expect(polissa.titular.id).to(equal(result['fiscal_customer_id']))
            expect(polissa.titular.id).to(be_above(0))
            expect(polissa.titular.vat).to(equal('ES' + signup_json['fiscal_customer']['identity_document']['code']))
            expect(polissa.titular.name).to(equal('Customer, Fiscal'))
            # expect(polissa.titular.address[0].email).to(equal(signup_json['user']['email']))
            # expect(polissa.titular.address[0].phone).to(equal(signup_json['user']['phone']))
            expect(polissa.state).to(equal('esborrany'))
            expect(polissa.data_firma_contracte).to(equal(False))
            expect(polissa.tipus_vivenda).to(equal('habitual'))
            expect(polissa.cnae.id).to(equal(986))
            expect(polissa.data_baixa).to(equal(False))
            expect(polissa.tipo_pago.code).to(equal('RECIBO_CSB'))
            expect(polissa.direccio_pagament.id).to(equal(result['fiscal_customer_address_id']))
            expect(polissa.direccio_pagament.id).to(be_above(0))
            expect(polissa.direccio_pagament.id).not_to(equal(result['user_address_id']))
            expect(polissa.pagador.id).to(equal(result['fiscal_customer_id']))
            expect(polissa.pagador.id).to(be_above(0))
            expect(polissa.pagador_sel).to(equal('titular'))
            expect(polissa.direccio_notificacio.id).to(equal(result['user_address_id']))
            expect(polissa.direccio_notificacio.id).to(be_above(0))
            expect(polissa.payment_mode_id.name).to(equal('Recibo mensual BBVA'))
            expect(polissa.llista_preu.name).to(equal('Lucera Precios Variables'))
            expect(polissa.llista_preu.type).to(equal('sale'))
            expect(polissa.bank.id).to(equal(result['bank_account_id']))
            expect(polissa.bank.id).to(be_above(0))
            expect(polissa.bank.iban).to(equal('ES8800303123410000260271'))
            expect(polissa.facturacio).to(equal(1))
            expect(polissa.facturacio_potencia).to(equal('icp'))
            expect(polissa.notificacio).to(equal('altre_p'))
            expect(polissa.altre_p.id).to(equal(result['user_id']))
            expect(polissa.altre_p.id).to(be_above(0))
            expect(polissa.affiliate_code).to(equal(signup_json['promotion']['affiliate']['code']))
            expect(polissa.affiliate_external_id).to(equal(signup_json['promotion']['affiliate']['external_id']))
            expect(polissa.potencia).to(equal(0))
            expect(polissa.observacions).to(be_false)


            # Mandate
            mandates_id = self.openerp.pool.get('payment.mandate').search(
                cursor, user, [('reference', '=', 'giscedata.polissa,%s' % polissa.id)])
            expect(mandates_id).not_to(be_empty)
            expect(mandates_id[0]).to(be_above(0))

            # Bus message
            bus_message_obj = self.openerp.pool.get('lucera.bus.message')
            bus_message_ids = bus_message_obj.search(cursor, user,
                                                     [('name', '=', 'giscedata.polissa.created')],
                                                     order='create_date desc')
            expect(bus_message_ids).not_to(be_empty)
            bus_message = bus_message_obj.browse(cursor, user, bus_message_ids[0])
            expect(bus_message.name).to(equal('giscedata.polissa.created'))
            expect(str(result['polissa_id']) in bus_message.content).to(be_true)

    def test_can_create_signup_when_supply_point_has_cups_code_but_not_supply(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            uid = txn.user
            self._setup(cursor, uid)
            content = get_serialized_signup('default.json')
            signup = json.loads(content)
            signup['electricity']['supply_point']['has_supply'] = False
            signup['electricity']['supply_point']['no_supply_reason'] = 'purchase'
            del (signup['electricity']['product']['access_contract']['current'])
            signup['electricity']['product']['access_contract']['target'] = {
                'power': 4500,
                'tariff_code': '001'
            }

            # Act
            signup_committer_obj = self.openerp.pool.get('lucera.signup.committer')
            result = signup_committer_obj.commit(cursor, uid, signup)

            # Assert
            self._check_result(cursor, uid, result)

            # Signup
            signup = self.openerp.pool.get('lucera.signup').browse(cursor, uid, result['signup_id'])
            expect(signup.electricity_signup_id.has_current_atr_contract).to(be_false)
            expect(signup.electricity_signup_id.no_supply_reason).to(equal('purchase'))

            # Contract
            polissa = self.openerp.pool.get('giscedata.polissa').browse(cursor, uid, result['polissa_id'])
            expect(polissa.observacions).to(be_false)

    def test_can_create_signup_when_supply_point_does_not_have_cups_assigned_code(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            uid = txn.user
            self._setup(cursor, uid)
            content = get_serialized_signup('default.json')
            signup_json = json.loads(content)
            signup_json['electricity']['supply_point']['has_assigned_code'] = False
            signup_json['electricity']['supply_point']['has_supply'] = False
            del (signup_json['electricity']['supply_point']['code'])
            del (signup_json['electricity']['product']['access_contract']['current'])

            signup_json['electricity']['product']['access_contract']['target'] = {
                'power': 4500,
                'tariff_code': '001'
            }
            # Act
            signup_committer_obj = self.openerp.pool.get('lucera.signup.committer')
            result = signup_committer_obj.commit(cursor, uid, signup_json)

            # Assert
            expect(result['cups_id']).not_to(be_false)

            # Contract
            polissa = self.openerp.pool.get('giscedata.polissa').browse(cursor, uid, result['polissa_id'])
            expect(polissa.observacions).to(be_false)

    def test_can_create_signup_when_supply_point_does_not_have_code_but_has_assigned_code_and_supply(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            uid = txn.user
            self._setup(cursor, uid)
            content = get_serialized_signup('default.json')
            signup = json.loads(content)
            del (signup['electricity']['supply_point']['code'])
            signup['electricity']['supply_point']['has_assigned_code'] = True
            del (signup['electricity']['product']['access_contract']['current'])
            signup['electricity']['product']['access_contract']['changes'] = True
            signup['electricity']['product']['access_contract']['target'] = {
                'power': 4500,
                'tariff_code': '001'
            }

            # Act
            signup_committer_obj = self.openerp.pool.get('lucera.signup.committer')
            result = signup_committer_obj.commit(cursor, uid, signup)

            # Assert
            expect(result['cups_id']).not_to(be_false)
            polissa = self.openerp.pool.get('giscedata.polissa').browse(cursor, uid, result['polissa_id'])
            expect(polissa.cups.id).not_to(be_false)
            expect(polissa.observacions).to(be_false)

    def test_can_create_signup_when_user_holder_and_payer_are_distinct_and_supply_point_not_found(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            self._setup(txn.cursor, txn.user)
            content = get_serialized_signup('user_holder_and_payer_are_distinct_and_supply_point_not_found.json')
            signup = json.loads(content)

            # Act
            signup_committer_obj = self.openerp.pool.get('lucera.signup.committer')
            result = signup_committer_obj.commit(txn.cursor, txn.user, content)

            # Assert
            self._check_result(txn.cursor, txn.user, result)

            # User, fiscal_customer and payer should be distinct
            expect(result['fiscal_customer_id']).not_to(equal(result['user_id']))
            expect(result['fiscal_customer_address_id']).not_to(equal(result['user_address_id']))
            expect(result['bank_account_owner_id']).not_to(equal(result['user_id']))
            expect(result['bank_account_owner_address_id']).not_to(equal(result['user_address_id']))
            expect(result['bank_account_owner_id']).not_to(equal(result['fiscal_customer_id']))
            expect(result['bank_account_owner_address_id']).not_to(equal(result['fiscal_customer_address_id']))

            # User
            user = self.openerp.pool.get('res.partner').browse(txn.cursor, txn.user, result['user_id'])
            expect(user.name).to(equal(u'De la Calzada, Chiquito'))
            expect(user.is_end_user).to(be_true)
            expect(user.vat).to(equal('ES{0}'.format(signup['user']['identity_document']['code'])))
            expect(user.address[0].email).to(equal(signup['user']['email'].lower()))
            expect(user.address[0].phone).to(equal(signup['user']['phone']))

            # fiscal_customer
            fiscal_customer = self.openerp.pool.get('res.partner').browse(txn.cursor, txn.user, result['fiscal_customer_id'])
            expect(fiscal_customer.name).to(equal(u'Test 2 S.L.'))
            expect(fiscal_customer.is_end_user).to(be_false)
            expect(fiscal_customer.vat).to(equal('ES{0}'.format(signup['fiscal_customer']['identity_document']['code'])))
            expect(fiscal_customer.address[0].email).to(equal(signup['fiscal_customer']['email'].lower()))
            expect(fiscal_customer.address[0].phone).to(equal(signup['fiscal_customer']['phone']))

            # Payer
            payer = self.openerp.pool.get('res.partner').browse(txn.cursor, txn.user, result['bank_account_owner_id'])
            expect(payer.name).to(equal(u'Test S.L.'))
            expect(payer.is_end_user).to(be_false)
            expect(payer.vat).to(equal('ES{0}'.format(signup['payment_method']['bank_account']['owner']['identity_document']['code'])))
            expect(payer.address[0].email).to(equal(signup['payment_method']['bank_account']['owner']['email'].lower()))
            expect(payer.address[0].phone).to(equal(signup['payment_method']['bank_account']['owner']['phone']))

            # Contract
            polissa = self.openerp.pool.get('giscedata.polissa')\
                .browse(txn.cursor, txn.user, result['polissa_id'])
            expect(polissa.titular.id).to(equal(result['fiscal_customer_id']))
            expect(polissa.pagador.id).to(equal(result['fiscal_customer_id']))
            expect(polissa.pagador.bank_ids[0].owner_id.id).to(equal(result['bank_account_owner_id']))
            expect(polissa.pagador.bank_ids[0].owner_id.address[0].id).to(equal(result['bank_account_owner_address_id']))
            expect(polissa.pagador_sel).to(equal('titular'))
            expect(polissa.direccio_pagament.id).to(equal(result['fiscal_customer_address_id']))
            expect(polissa.notificacio).to(equal('altre_p'))
            expect(polissa.direccio_notificacio.id).to(equal(result['user_address_id']))
            expect(polissa.bank.id).to(equal(result['bank_account_id']))
            expect(polissa.altre_p.id).to(equal(result['user_id']))
            expect(polissa.observacions).to(be_false)

    def test_can_create_signup_when_user_holder_and_payer_already_exist(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            self._setup(txn.cursor, txn.user)
            user_id, user_address_id = \
                create_partner_and_address(self.openerp.pool, txn.cursor, txn.user,
                                           {'name': 'User 1', 'is_end_user': True, 'vat': '07654174G'},
                                           {'email': 'user1@test.com', 'phone': '961544878'})
            fiscal_customer_id, fiscal_customer_address_id = \
                create_partner_and_address(self.openerp.pool, txn.cursor, txn.user,
                                           {'name': 'User 2', 'is_end_user': False, 'vat': 'D58531898'},
                                           {'email': 'user2@test.com', 'phone': '911544811'})

            bank_account_owner_id, bank_account_owner_address_id = \
                create_partner_and_address(self.openerp.pool, txn.cursor, txn.user,
                                           {'name': 'User 3', 'is_end_user': False, 'vat': 'H74622325'},
                                           {'email': 'user3@test.com', 'phone': '931544833'})

            content = get_serialized_signup('user_holder_and_payer_already_exist.json')

            # Replace content with generated ids
            content = content.replace("user_id", str(user_id)).replace("user_address_id", str(user_address_id)) \
                .replace("fiscal_customer_id", str(fiscal_customer_id)).replace("fiscal_customer_address_id", str(fiscal_customer_address_id)) \
                .replace("bank_account_owner_id", str(bank_account_owner_id)).replace("bank_account_owner_address_id", str(bank_account_owner_address_id))

            signup = json.loads(content)

            # Act
            signup_committer_obj = self.openerp.pool.get('lucera.signup.committer')
            result = signup_committer_obj.commit(txn.cursor, txn.user, content)

            # Assert

            self._check_result(txn.cursor, txn.user, result)

            # User, fiscal_customer and payer should be the existing ones
            expect(result['user_id']).to(equal(user_id))
            expect(result['user_address_id']).to(equal(user_address_id))
            expect(result['fiscal_customer_id']).to(equal(fiscal_customer_id))
            expect(result['fiscal_customer_address_id']).to(equal(fiscal_customer_address_id))
            expect(result['bank_account_owner_id']).to(equal(bank_account_owner_id))
            expect(result['bank_account_owner_address_id']).to(equal(bank_account_owner_address_id))

            # User
            user = self.openerp.pool.get('res.partner').browse(txn.cursor, txn.user, result['user_id'])
            expect(user.id).to(equal(user_id))
            expect(user.is_end_user).to(be_true)

            # fiscal_customer
            fiscal_customer = self.openerp.pool.get('res.partner')\
                .browse(txn.cursor, txn.user, result['fiscal_customer_id'])
            expect(fiscal_customer.id).to(equal(fiscal_customer_id))

            # Payer
            bank_account_owner = self.openerp.pool.get('res.partner')\
                .browse(txn.cursor, txn.user, result['bank_account_owner_id'])
            expect(bank_account_owner.id).to(equal(bank_account_owner_id))

            # Contract
            polissa = self.openerp.pool.get('giscedata.polissa')\
                .browse(txn.cursor, txn.user, result['polissa_id'])
            expect(polissa.titular.id).to(equal(fiscal_customer_id))
            expect(polissa.pagador.id).to(equal(fiscal_customer_id))
            expect(polissa.pagador.bank_ids[0].owner_id.id).to(equal(bank_account_owner_id))
            expect(polissa.pagador_sel).to(equal('titular'))
            expect(polissa.direccio_pagament.id).to(equal(fiscal_customer_address_id))
            expect(polissa.notificacio).to(equal('altre_p'))
            expect(polissa.direccio_notificacio.id).to(equal(user_address_id))
            expect(polissa.bank.id).to(equal(result['bank_account_id']))
            expect(polissa.altre_p.id).to(equal(user_id))
            expect(polissa.observacions).to(be_false)

    def test_can_create_signup_without_cups(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            self._setup(txn.cursor, txn.user)
            content = get_serialized_signup('default.json')
            signup = json.loads(content)
            # Remove supply point info
            del (signup['electricity']['supply_point']['code'])

            # Act
            signup_committer_obj = self.openerp.pool.get('lucera.signup.committer')
            result = signup_committer_obj.commit(txn.cursor, txn.user, signup)

            # Assert
            expect(result['cups_id']).not_to(be_false)  # We don't create a cups
            expect(result['polissa_id']).to(be_above(0))

    def test_cannot_create_signup_without_cups_if_it_has_no_address(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            self._setup(txn.cursor, txn.user)
            content = get_serialized_signup('default.json')
            signup = json.loads(content)
            # Remove supply point info
            del (signup['electricity']['supply_point']['code'])
            del (signup['electricity']['supply_point']['location'])

            # Act
            signup_committer_obj = self.openerp.pool.get('lucera.signup.committer')

            # Assert
            with self.assertRaises(osv.except_osv) as cm:
                result = signup_committer_obj.commit(txn.cursor, txn.user, signup)

            expect(cm.exception.name).to(be('ValidateError'))

    def test_can_create_signup_without_bank_account(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            self._setup(txn.cursor, txn.user)
            content = get_serialized_signup('default.json')
            signup = json.loads(content)
            # Remove bank info
            del (signup['payment_method']['bank_account']['account_number'])
            del (signup['payment_method']['bank_account']['iban_code'])

            # Act
            signup_committer_obj = self.openerp.pool.get('lucera.signup.committer')
            result = signup_committer_obj.commit(txn.cursor, txn.user, signup)

            # Assert
            expect(result['bank_account_id']).to(be_false)
            expect(result['polissa_id']).to(be_above(0))

    def test_can_create_signup_with_empty_bank_account(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            self._setup(txn.cursor, txn.user)
            content = get_serialized_signup('default.json')
            signup = json.loads(content)
            # Remove bank info
            signup['payment_method']['bank_account']['iban_code'] = ''

            # Act
            signup_committer_obj = self.openerp.pool.get('lucera.signup.committer')
            result = signup_committer_obj.commit(txn.cursor, txn.user, signup)

            # Assert
            expect(result['bank_account_id']).to(be_false)
            expect(result['polissa_id']).to(be_above(0))

    def test_can_create_signup_when_user_and_bank_account_are_existing(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            self._setup(txn.cursor, txn.user)
            user_id, user_address_id = \
                create_partner_and_address(self.openerp.pool, txn.cursor, txn.user,
                                           {'name': 'User 1', 'is_end_user': True, 'vat': '07654174G'},
                                           {'email': 'user1@test.com', 'phone': '961544878'})

            bank_account_id = create_bank_account(self.openerp.pool, txn.cursor, txn.user, user_id)

            content = get_serialized_signup('default.json')
            signup = json.loads(content)
            signup['user'] = {
                'rel': 'existing',
                'id': user_id
            }
            signup['fiscal_customer'] = {
                'rel': 'user',
                'location': {
                    'rel': 'supply_point'
                }
            }
            signup['payment_method'] = {
                'bank_account': {
                    'rel': 'existing',
                    'id': bank_account_id
                }
            }

            # Act
            signup_committer_obj = self.openerp.pool.get('lucera.signup.committer')
            result = signup_committer_obj.commit(txn.cursor, txn.user, signup)

            # Assert
            expect(result['bank_account_id']).to(equal(bank_account_id))
            expect(result['polissa_id']).to(be_above(0))

            # Contract
            polissa = self.openerp.pool.get('giscedata.polissa') \
                .browse(txn.cursor, txn.user, result['polissa_id'])
            expect(polissa.titular.id).to(equal(user_id))
            expect(polissa.pagador.id).to(equal(user_id))
            expect(polissa.bank.id).to(equal(bank_account_id))
            expect(polissa.bank.owner_id.id).to(equal(user_id))

    def test_can_create_signup_when_bank_account_matches_existing(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            self._setup(txn.cursor, txn.user)
            user_id, user_address_id = \
                create_partner_and_address(self.openerp.pool, txn.cursor, txn.user,
                                           {'name': 'User 1', 'is_end_user': True, 'vat': '07654174G'},
                                           {'email': 'user1@test.com', 'phone': '961544878'})

            bank_account_id = create_bank_account(self.openerp.pool, txn.cursor, txn.user, user_id,
                                                  iban_code='ES4131572084666378784327')

            content = get_serialized_signup('default.json')
            signup = json.loads(content)
            signup['user'] = {
                'rel': 'existing',
                'id': user_id
            }
            signup['fiscal_customer'] = {
                'rel': 'user',
                'location': {
                    'rel': 'supply_point'
                }
            }
            signup['payment_method'] = {
                'bank_account': {
                    'rel': 'none',
                    'iban_code': 'ES4131572084666378784327',
                    'owner': {
                        'rel': 'user',
                        'location': {
                            'rel': 'user'
                        }
                    }
                }
            }

            # Act
            signup_committer_obj = self.openerp.pool.get('lucera.signup.committer')
            result = signup_committer_obj.commit(txn.cursor, txn.user, signup)

            # Assert
            expect(result['bank_account_id']).to(equal(bank_account_id))
            expect(result['polissa_id']).to(be_above(0))

            # Contract
            polissa = self.openerp.pool.get('giscedata.polissa') \
                .browse(txn.cursor, txn.user, result['polissa_id'])
            expect(polissa.titular.id).to(equal(user_id))
            expect(polissa.pagador.id).to(equal(user_id))
            expect(polissa.bank.id).to(equal(bank_account_id))
            expect(polissa.bank.owner_id.id).to(equal(user_id))
            expect(polissa.bank.partner_id.id).to(equal(user_id))

    def test_can_create_signup_when_fiscal_customer_is_other_and_bank_account_is_from_user(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            pool = self.openerp.pool
            cursor = txn.cursor
            user = txn.user
            self._setup(txn.cursor, txn.user)
            user_id, user_address_id = \
                create_partner_and_address(self.openerp.pool, txn.cursor, txn.user,
                                           {'name': 'User 1', 'is_end_user': True, 'vat': '07654174G'},
                                           {'email': 'user1@test.com', 'phone': '961544878'})

            bank_account_id = create_bank_account(self.openerp.pool, txn.cursor, txn.user, user_id,
                                                  iban_code='ES4131572084666378784327')

            content = get_serialized_signup('default.json')
            signup = json.loads(content)
            signup['user'] = {
                'rel': 'existing',
                'id': user_id
            }
            signup['fiscal_customer'] = {
                'rel': 'none',
                'type': 'individual',
                'name': 'Fiscal',
                'surnames': 'Customer',
                'identity_document': {
                    'code': '31118100C',
                    'type': 'dni'
                },
                'user_declares_representative_permission': True,
                'location': {
                    'rel': 'supply_point'
                }
            }
            signup['payment_method'] = {
                'bank_account': {
                    'rel': 'existing',
                    'id': bank_account_id
                }
            }

            # Act
            signup_committer_obj = self.openerp.pool.get('lucera.signup.committer')
            result = signup_committer_obj.commit(txn.cursor, txn.user, signup)

            # Assert
            expect(result['fiscal_customer_id']).not_to(equal(user_id))
            expect(result['polissa_id']).to(be_above(0))

            # Bank Account
            expect(result['bank_account_id']).to(be_above(0))
            expect(result['bank_account_id']).not_to(equal(bank_account_id))  # We have created a copy

            # Contract
            polissa = self.openerp.pool.get('giscedata.polissa').browse(txn.cursor, txn.user, result['polissa_id'])
            expect(polissa.titular.id).to(equal(result['fiscal_customer_id']))
            expect(polissa.pagador.id).to(equal(result['fiscal_customer_id']))
            expect(polissa.bank.id).not_to(equal(bank_account_id))  # We have created a copy
            expect(polissa.bank.iban).to(equal('ES4131572084666378784327'))
            expect(polissa.bank.owner_id.id).to(equal(user_id))
            expect(polissa.bank.partner_id.id).to(equal(result['fiscal_customer_id']))
            expect(polissa.direccio_notificacio.partner_id.id).to(equal(user_id))

    def test_cannot_create_signup_when_power_greater_than_allowed(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            self._setup(txn.cursor, txn.user)
            content = get_serialized_signup('default.json')
            signup = json.loads(content)
            # Change power
            signup['electricity']['product']['access_contract']['current']['power'] = 17000

            # Act
            signup_committer_obj = self.openerp.pool.get('lucera.signup.committer')
            with self.assertRaises(osv.except_osv) as cm:
                result = signup_committer_obj.commit(txn.cursor, txn.user, signup)

    def test_cannot_create_signup_when_tariff_not_allowed(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            self._setup(txn.cursor, txn.user)
            content = get_serialized_signup('default.json')
            signup = json.loads(content)
            # Change tariff to 3.0
            signup['electricity']['product']['access_contract']['current']['tariff_code'] = "003"

            # Act
            signup_committer_obj = self.openerp.pool.get('lucera.signup.committer')
            with self.assertRaises(osv.except_osv) as cm:
                result = signup_committer_obj.commit(txn.cursor, txn.user, signup)

    def test_cannot_create_signup_when_user_is_not_end_user(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            self._setup(txn.cursor, txn.user)
            user_id, user_address_id = \
                create_partner_and_address(self.openerp.pool, txn.cursor, txn.user,
                                           {'name': 'User 1', 'is_end_user': False,
                                            'vat': '07654174G'},
                                           {'email': 'user1@test.com', 'phone': '961544878'})

            content = get_serialized_signup('default.json')
            signup = json.loads(content)
            signup['user']['rel'] = 'existing'
            signup['user']['id'] = user_id

            # Act
            signup_committer_obj = self.openerp.pool.get('lucera.signup.committer')

            with self.assertRaises(osv.except_osv) as cm:
                result = signup_committer_obj.commit(txn.cursor, txn.user, signup)

    def test_can_create_signup_when_fiscal_customer_location_is_existing(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            self._setup(txn.cursor, txn.user)
            fiscal_customer_id, fiscal_customer_address_id = \
                create_partner_and_address(self.openerp.pool, txn.cursor, txn.user, is_end_user=False)

            content = get_serialized_signup('default.json')
            signup = json.loads(content)
            signup['fiscal_customer']['rel'] = 'existing'
            signup['fiscal_customer']['id'] = fiscal_customer_id
            signup['fiscal_customer']['user_declares_representative_permission'] = True
            signup['fiscal_customer']['location']['rel'] = 'existing'
            signup['fiscal_customer']['location']['id'] = fiscal_customer_address_id

            # Act
            signup_committer_obj = self.openerp.pool.get('lucera.signup.committer')
            result = signup_committer_obj.commit(txn.cursor, txn.user, signup)

            # Assert
            expect(result['fiscal_customer_id']).to(equal(fiscal_customer_id))
            expect(result['fiscal_customer_address_id']).to(equal(fiscal_customer_address_id))

    def test_cannot_create_signup_when_fiscal_customer_location_is_existing_but_is_not_from_user(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            self._setup(txn.cursor, txn.user)

            user_id, user_address_id = \
                create_partner_and_address(self.openerp.pool, txn.cursor, txn.user,
                                           {'name': 'User 1', 'is_end_user': False,
                                            'vat': '07654174G'},
                                           {'email': 'user1@test.com', 'phone': '961544878'})
            user2_id, user2_address_id = \
                create_partner_and_address(self.openerp.pool, txn.cursor, txn.user,
                                           {'name': 'User 2', 'is_end_user': True,
                                            'vat': 'H73745069'},
                                           {'email': 'user2@test.com', 'phone': '961334444'})

            content = get_serialized_signup('default.json')
            signup = json.loads(content)
            signup['fiscal_customer']['rel'] = 'existing'
            signup['fiscal_customer']['id'] = user_id
            signup['fiscal_customer']['user_declares_representative_permission'] = True
            signup['fiscal_customer']['location']['rel'] = 'existing'
            # Instead of user_id_address we user user2_address
            signup['fiscal_customer']['location']['id'] = user2_address_id

            # Act
            signup_committer_obj = self.openerp.pool.get('lucera.signup.committer')
            result = signup_committer_obj.commit(txn.cursor, txn.user, signup)

            # Assert
            # It ignores passed user2_address_id
            expect(result['fiscal_customer_address_id']).not_to(equal(user2_address_id))
            expect(result['fiscal_customer_address_id']).to(equal(user_address_id))

    def test_can_create_signup_when_bank_account_owner_location_is_existing(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            self._setup(txn.cursor, txn.user)
            bank_account_owner_id, bank_account_owner_address_id = \
                create_partner_and_address(self.openerp.pool, txn.cursor, txn.user, is_end_user=False)

            content = get_serialized_signup('default.json')
            signup = json.loads(content)
            signup['payment_method']['bank_account']['owner']['rel'] = 'existing'
            signup['payment_method']['bank_account']['owner']['id'] = bank_account_owner_id
            signup['payment_method']['bank_account']['owner']['user_declares_owner_permission'] = True

            # Act
            signup_committer_obj = self.openerp.pool.get('lucera.signup.committer')
            result = signup_committer_obj.commit(txn.cursor, txn.user, signup)

            # Assert
            expect(result['bank_account_owner_id']).to(equal(bank_account_owner_id))
            expect(result['bank_account_owner_address_id']).to(equal(bank_account_owner_address_id))
            
    def test_can_create_signup_when_bank_account_owner_is_user_and_location_is_supply_point(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            self._setup(txn.cursor, txn.user)
            content = get_serialized_signup('default.json')
            signup = json.loads(content)

            # We set a different fiscal customer
            signup['fiscal_customer']['rel'] = 'none'
            signup['fiscal_customer']['name'] = 'Test 2 S.L.'
            signup['fiscal_customer']['identity_document'] = {'code': 'R2196754B', 'type': 'implicit'}
            signup['fiscal_customer']['email'] = 'contabilidad@testsl.com'
            signup['fiscal_customer']['phone'] = '961727272'
            signup['fiscal_customer']['user_declares_representative_permission'] = True
            signup['fiscal_customer']['location']['rel'] = 'none'
            signup['fiscal_customer']['location']['postal_code'] = '46001'
            signup['fiscal_customer']['location']['town_code'] = '46250'
            signup['fiscal_customer']['location']['address'] = {'street_type_code': 'CL',
                                                                'street_name': 'COLON 23',
                                                                'number': '12' }

            # We set the bank account owner address to a user but choose supply point as rel
            signup['payment_method']['bank_account']['owner']['rel'] = 'user'
            signup['payment_method']['bank_account']['owner']['location']['rel'] = 'supply_point'

            # Act
            signup_committer_obj = self.openerp.pool.get('lucera.signup.committer')
            result = signup_committer_obj.commit(txn.cursor, txn.user, signup)

            # Assert
            expect(result['bank_account_owner_id']).to(equal(result['user_id']))
            expect(result['bank_account_owner_id']).not_to(equal(result['fiscal_customer_id']))
            expect(result['bank_account_owner_address_id']).to(equal(result['user_address_id']))
            expect(result['bank_account_owner_address_id']).not_to(equal(result['fiscal_customer_address_id']))

            address = self.openerp.pool.get('res.partner.address').browse(txn.cursor, txn.user, result['bank_account_owner_address_id'])
            supply_point_location = signup['electricity']['supply_point']['location']
            expect(address.nv).to(equal(normalize_street_name(supply_point_location['address']['street_name'])))
            expect(address.zip).to(equal(normalize_postal_code(supply_point_location['postal_code'])))

    def test_can_create_signup_when_bank_account_owner_is_user_and_location_is_fiscal_customer(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            self._setup(txn.cursor, txn.user)
            content = get_serialized_signup('default.json')
            signup = json.loads(content)

            # We set a different fiscal customer with different address from supply point
            signup['fiscal_customer']['rel'] = 'none'
            signup['fiscal_customer']['name'] = 'Test 2 S.L.'
            signup['fiscal_customer']['identity_document'] = {'code': 'R2196754B', 'type': 'implicit'}
            signup['fiscal_customer']['email'] = 'contabilidad@testsl.com'
            signup['fiscal_customer']['phone'] = '961727272'
            signup['fiscal_customer']['user_declares_representative_permission'] = True
            signup['fiscal_customer']['location']['rel'] = 'none'
            signup['fiscal_customer']['location']['postal_code'] = '46001'
            signup['fiscal_customer']['location']['town_code'] = '46250'
            signup['fiscal_customer']['location']['address'] = {'street_type_code': 'CL',
                                                                'street_name': 'COLON 23',
                                                                'number': '12'}

            # We set the bank account owner address to a user but choose supply point as rel
            signup['payment_method']['bank_account']['owner']['rel'] = 'user'
            signup['payment_method']['bank_account']['owner']['location']['rel'] = 'fiscal_customer'

            # Act
            signup_committer_obj = self.openerp.pool.get('lucera.signup.committer')
            result = signup_committer_obj.commit(txn.cursor, txn.user, signup)

            # Assert
            expect(result['bank_account_owner_id']).to(equal(result['user_id']))
            expect(result['bank_account_owner_id']).not_to(equal(result['fiscal_customer_id']))
            expect(result['bank_account_owner_address_id']).to(equal(result['user_address_id']))
            expect(result['bank_account_owner_address_id']).not_to(equal(result['fiscal_customer_address_id']))

            address = self.openerp.pool.get('res.partner.address').browse(txn.cursor, txn.user, result['bank_account_owner_address_id'])
            expect(address.nv).to(equal(normalize_street_name('COLON 23')))
            expect(address.zip).to(equal('46001'))

    def test_can_create_signup_when_bank_account_owner_is_user_and_location_is_new(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            self._setup(txn.cursor, txn.user)
            content = get_serialized_signup('default.json')
            signup = json.loads(content)

            # We set a different fiscal customer with different address from supply point
            signup['fiscal_customer']['rel'] = 'none'
            signup['fiscal_customer']['name'] = 'Test 2 S.L.'
            signup['fiscal_customer']['identity_document'] = {'code': 'R2196754B', 'type': 'implicit'}
            signup['fiscal_customer']['email'] = 'contabilidad@testsl.com'
            signup['fiscal_customer']['phone'] = '961727272'
            signup['fiscal_customer']['user_declares_representative_permission'] = True
            signup['fiscal_customer']['location']['rel'] = 'none'
            signup['fiscal_customer']['location']['postal_code'] = '46001'
            signup['fiscal_customer']['location']['town_code'] = '46250'
            signup['fiscal_customer']['location']['address'] = {'street_type_code': 'CL',
                                                                'street_name': 'COLON',
                                                                'number': '23'}

            # We set the bank account owner address to a user but choose supply point as rel
            signup['payment_method']['bank_account']['owner']['rel'] = 'user'
            signup['payment_method']['bank_account']['owner']['location']['rel'] = 'none'
            signup['payment_method']['bank_account']['owner']['location']['address'] = {'street_type_code': 'CL',
                                                                                        'street_name': 'Albufera',
                                                                                        'number': '2'}
            signup['payment_method']['bank_account']['owner']['location']['postal_code'] = '46192'
            signup['payment_method']['bank_account']['owner']['location']['town_code'] = '46172'

            # Act
            signup_committer_obj = self.openerp.pool.get('lucera.signup.committer')
            result = signup_committer_obj.commit(txn.cursor, txn.user, signup)

            # Assert
            expect(result['bank_account_owner_id']).to(equal(result['user_id']))
            expect(result['bank_account_owner_id']).not_to(equal(result['fiscal_customer_id']))
            expect(result['bank_account_owner_address_id']).to(equal(result['user_address_id']))
            expect(result['bank_account_owner_address_id']).not_to(equal(result['fiscal_customer_address_id']))

            address = self.openerp.pool.get('res.partner.address').browse(txn.cursor, txn.user, result['bank_account_owner_address_id'])
            expect(address.nv).to(equal(normalize_street_name('Albufera')))
            expect(address.zip).to(equal('46192'))

    def test_can_create_signup_when_bank_account_owner_is_user_with_already_existing_location(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            self._setup(txn.cursor, txn.user)
            # Create a user with postal address
            user_id, user_address_id = \
                create_partner_and_address(self.openerp.pool, txn.cursor, txn.user,
                                           {'name': 'User 1', 'is_end_user': True, 'vat': '07654174G'},
                                           {'email': 'user1@test.com', 'phone': '961544878',
                                            'zip': '46470', 'nv': 'Mestalla'})

            content = get_serialized_signup('default.json')
            signup = json.loads(content)

            # We set user rel as existing
            signup['user']['rel'] = 'existing'
            signup['user']['id'] = user_id

            # We set a different fiscal customer with different address from supply point
            signup['fiscal_customer']['rel'] = 'none'
            signup['fiscal_customer']['name'] = 'Test 2 S.L.'
            signup['fiscal_customer']['identity_document'] = {'code': 'R2196754B', 'type': 'implicit'}
            signup['fiscal_customer']['email'] = 'contabilidad@testsl.com'
            signup['fiscal_customer']['phone'] = '961727272'
            signup['fiscal_customer']['user_declares_representative_permission'] = True
            signup['fiscal_customer']['location']['rel'] = 'none'
            signup['fiscal_customer']['location']['postal_code'] = '46001'
            signup['fiscal_customer']['location']['town_code'] = '46250'
            signup['fiscal_customer']['location']['address'] = {'street_type_code': 'CL',
                                                                'street_name': 'COLON 23',
                                                                'number': '12'}

            # We set the bank account owner address to a user but choose supply point as rel
            signup['payment_method']['bank_account']['owner']['rel'] = 'user'
            signup['payment_method']['bank_account']['owner']['location']['rel'] = 'none'
            signup['payment_method']['bank_account']['owner']['location']['address'] = {'street_type_code': 'CL',
                                                                                        'street_name': 'Albufera',
                                                                                        'number': '2'}
            signup['payment_method']['bank_account']['owner']['location']['postal_code'] = '46192'
            signup['payment_method']['bank_account']['owner']['location']['town_code'] = '46172'

            # Act
            signup_committer_obj = self.openerp.pool.get('lucera.signup.committer')
            result = signup_committer_obj.commit(txn.cursor, txn.user, signup)

            # Assert
            expect(result['bank_account_owner_id']).to(equal(result['user_id']))
            expect(result['bank_account_owner_address_id']).to(equal(result['user_address_id']))
            address = self.openerp.pool.get('res.partner.address').browse(txn.cursor, txn.user, result['bank_account_owner_address_id'])
            # Mantain existing user address
            expect(address.nv).to(equal(normalize_street_name('Mestalla')))
            expect(address.zip).to(equal('46470'))

    def test_can_create_signup_when_user_is_matching_but_not_signed_up_and_not_detected(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            self._setup(txn.cursor, txn.user)

            content = get_serialized_signup('default.json')
            signup = json.loads(content)

            # Arrange - create existing user
            user_id, user_address_id = self._create_user_and_address_from_signup(signup, txn=txn)

            # Act
            signup_committer_obj = self.openerp.pool.get('lucera.signup.committer')
            result = signup_committer_obj.commit(txn.cursor, txn.user, signup)

            # Assert
            # It is assigned to existing end user
            expect(result['user_id']).to(equal(user_id))
            expect(result['user_address_id']).to(equal(user_address_id))

    def test_can_create_signup_when_user_is_matching_but_not_signed_and_matching_detected(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            self._setup(txn.cursor, txn.user)

            content = get_serialized_signup('default.json')
            signup = json.loads(content)

            # Arrange - create existing user
            user_id, user_address_id = self._create_user_and_address_from_signup(signup, txn=txn)
            signup['user']['rel'] = 'matching'
            signup['user']['id'] = user_id

            # Act
            signup_committer_obj = self.openerp.pool.get('lucera.signup.committer')
            result = signup_committer_obj.commit(txn.cursor, txn.user, signup)

            # Assert
            # It is assigned to existing end user
            expect(result['user_id']).to(equal(user_id))
            expect(result['user_address_id']).to(equal(user_address_id))

    def test_cannot_create_signup_when_user_is_matching_but_user_id_does_not_match_we_ignore_the_user_id(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            self._setup(txn.cursor, txn.user)

            content = get_serialized_signup('default.json')
            signup = json.loads(content)

            # Arrange - create existing user
            user_id, user_address_id = self._create_user_and_address_from_signup(signup, txn=txn)
            signup['user']['rel'] = 'matching'
            signup['user']['id'] = 222222222  # Not existing or wrong user_id

            # Act
            signup_committer_obj = self.openerp.pool.get('lucera.signup.committer')
            result = signup_committer_obj.commit(txn.cursor, txn.user, signup)

            # Assert
            # It is assigned to existing end user
            expect(result['user_id']).to(equal(user_id))
            expect(result['user_address_id']).to(equal(user_address_id))

    def test_can_create_signup_when_user_is_matching_but_not_signed_and_not_detected_and_address_do_match(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            self._setup(txn.cursor, txn.user)

            content = get_serialized_signup('default.json')
            signup = json.loads(content)

            # Arrange - create existing user with location
            location = signup['electricity']['supply_point']['location']
            ine_code = location['town_code']
            municipi_obj = self.openerp.pool.get('res.municipi')
            municipi_ids = municipi_obj.search(txn.cursor, txn.user, [('ine', '=', ine_code)])
            municipi = municipi_obj.browse(txn.cursor, txn.user, municipi_ids[0])
            country_id = self.openerp.pool.get('res.country').search(txn.cursor, txn.user, [('code', '=', 'ES')])[0]
            address_data = {
                'nv': normalize_street_name(location['address']['street_name']),
                'zip': normalize_postal_code(location['postal_code']),
                'id_municipi': municipi.id,
                'state_id': municipi.state.id,
                'country_id': country_id
            }
            user_id, user_address_id = self._create_user_and_address_from_signup(signup, txn=txn)
            self.openerp.pool.get('res.partner.address').write(txn.cursor, txn.user, user_address_id, address_data)

            # Act
            signup_committer_obj = self.openerp.pool.get('lucera.signup.committer')
            result = signup_committer_obj.commit(txn.cursor, txn.user, signup)

            # Assert
            # It is assigned to existing end user
            expect(result['user_id']).to(equal(user_id))
            expect(result['user_address_id']).to(equal(user_address_id))

            address = self.openerp.pool.get('res.partner.address').browse(txn.cursor, txn.user, user_address_id)
            expect(address.nv).to(equal(normalize_street_name(location['address']['street_name'])))
            expect(address.zip).to(equal(normalize_postal_code(location['postal_code'])))

    def test_can_create_signup_when_user_is_matching_but_not_signed_and_not_detected_and_address_almost_match(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            self._setup(txn.cursor, txn.user)

            content = get_serialized_signup('default.json')
            signup = json.loads(content)

            # Arrange - create existing user with location
            location = signup['electricity']['supply_point']['location']
            ine_code = location['town_code']
            municipi_obj = self.openerp.pool.get('res.municipi')
            municipi_ids = municipi_obj.search(txn.cursor, txn.user, [('ine', '=', ine_code)])
            municipi = municipi_obj.browse(txn.cursor, txn.user, municipi_ids[0])
            country_id = self.openerp.pool.get('res.country').search(txn.cursor, txn.user, [('code', '=', 'ES')])[0]
            address_data = {
                'nv': normalize_street_name(location['address']['street_name']).lower() + ".",  # In lowercase and append a dot
                'zip': normalize_postal_code(location['postal_code']),
                'id_municipi': municipi.id,
                'state_id': municipi.state.id,
                'country_id': country_id
            }
            user_id, user_address_id = self._create_user_and_address_from_signup(signup, txn=txn)
            self.openerp.pool.get('res.partner.address').write(txn.cursor, txn.user, user_address_id, address_data)

            # Act
            signup_committer_obj = self.openerp.pool.get('lucera.signup.committer')
            result = signup_committer_obj.commit(txn.cursor, txn.user, signup)

            # Assert
            # It is assigned to existing end user
            expect(result['user_id']).to(equal(user_id))
            expect(result['user_address_id']).to(equal(user_address_id))

            address = self.openerp.pool.get('res.partner.address').browse(txn.cursor, txn.user, user_address_id)

            # It must not be changed
            expect(address.nv).to(equal(address_data['nv']))
            expect(address.zip).to(equal(address_data['zip']))

    def test_cannot_create_signup_when_user_is_matching_but_not_signed_and_detected_and_address_do_not_match(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            self._setup(txn.cursor, txn.user)

            content = get_serialized_signup('default.json')
            signup = json.loads(content)

            # Arrange - create existing user with location
            location = signup['electricity']['supply_point']['location']
            ine_code = location['town_code']
            municipi_obj = self.openerp.pool.get('res.municipi')
            municipi_ids = municipi_obj.search(txn.cursor, txn.user, [('ine', '=', ine_code)])
            municipi = municipi_obj.browse(txn.cursor, txn.user, municipi_ids[0])
            country_id = self.openerp.pool.get('res.country').search(txn.cursor, txn.user, [('code', '=', 'ES')])[0]
            address_data = {
                'nv': 'Barrio sésamo',  # We change the street
                'zip': normalize_postal_code(location['postal_code']),
                'id_municipi': municipi.id,
                'state_id': municipi.state.id,
                'country_id': country_id
            }
            user_id, user_address_id = self._create_user_and_address_from_signup(signup, txn=txn)
            self.openerp.pool.get('res.partner.address').write(txn.cursor, txn.user, user_address_id, address_data)

            # Act
            signup_committer_obj = self.openerp.pool.get('lucera.signup.committer')

            with self.assertRaises(osv.except_osv) as cm:
                result = signup_committer_obj.commit(txn.cursor, txn.user, signup)

            expect(cm.exception.name).to(be('ValidateError'))

    def test_cannot_create_signup_if_there_is_an_existing_contract_with_same_supply_point_code_and_it_is_a_active(self):
        # No sólo que esté activo, también se aplica en borrador
        with Transaction().start(self.database) as txn:
            pool = self.openerp.pool

            # Arrange
            self._setup(txn.cursor, txn.user)
            content = get_serialized_signup('default.json')
            signup = json.loads(content)

            cups_code = signup['electricity']['supply_point']['code']
            vals = {'state': 'activa'}
            existing_polissa_id = create_test_electricity_polissa(pool, txn.cursor, txn.user,
                                                                  cups_code=cups_code, override_vals=vals)['polissa_id']

            # Act
            signup_committer_obj = self.openerp.pool.get('lucera.signup.committer')

            # Assert
            with self.assertRaises(osv.except_osv) as cm:
                result = signup_committer_obj.commit(txn.cursor, txn.user, signup)

            expect(cm.exception.name).to(be('ValidateError'))

    def test_cannot_create_signup_if_there_is_an_existing_contract_with_same_supply_point_code_and_it_is_a_draft(self):
        # No sólo que esté activo, también se aplica en borrador
        with Transaction().start(self.database) as txn:
            pool = self.openerp.pool

            # Arrange
            self._setup(txn.cursor, txn.user)
            content = get_serialized_signup('default.json')
            signup = json.loads(content)

            cups_code = signup['electricity']['supply_point']['code']
            vals = {'state': 'esborrany'}
            existing_polissa_id = create_test_electricity_polissa(pool, txn.cursor, txn.user,
                                                                  cups_code=cups_code, override_vals=vals)['polissa_id']

            # Act
            signup_committer_obj = self.openerp.pool.get('lucera.signup.committer')

            # Assert
            with self.assertRaises(osv.except_osv) as cm:
                result = signup_committer_obj.commit(txn.cursor, txn.user, signup)

            expect(cm.exception.name).to(be('ValidateError'))

    def test_can_create_signup_if_there_is_an_existing_contract_with_same_supply_point_code_and_it_is_baixa(self):
        # No sólo que esté activo, también se aplica en borrador
        with Transaction().start(self.database) as txn:
            pool = self.openerp.pool

            # Arrange
            self._setup(txn.cursor, txn.user)
            content = get_serialized_signup('default.json')
            signup = json.loads(content)

            cups_code = signup['electricity']['supply_point']['code']
            vals = {'state': 'baixa'}
            existing_polissa_id = create_test_electricity_polissa(pool, txn.cursor, txn.user,
                                                                  cups_code=cups_code, override_vals=vals)['polissa_id']

            # Act
            signup_committer_obj = self.openerp.pool.get('lucera.signup.committer')

            # Assert
            result = signup_committer_obj.commit(txn.cursor, txn.user, signup)

            expect(result['polissa_id']).to(be_above(0))
            expect(result['polissa_id']).not_to(equal(existing_polissa_id))

    def test_can_create_signup_if_there_is_an_existing_contract_with_same_supply_point_code_and_it_is_cancelled(self):
        # No sólo que esté activo, también se aplica en borrador
        with Transaction().start(self.database) as txn:
            pool = self.openerp.pool

            # Arrange
            self._setup(txn.cursor, txn.user)
            content = get_serialized_signup('default.json')
            signup = json.loads(content)

            cups_code = signup['electricity']['supply_point']['code']
            vals = {'state': 'cancelada'}
            existing_polissa_id = create_test_electricity_polissa(pool, txn.cursor, txn.user,
                                                                  cups_code=cups_code, override_vals=vals)['polissa_id']

            # Act
            signup_committer_obj = self.openerp.pool.get('lucera.signup.committer')

            # Assert
            result = signup_committer_obj.commit(txn.cursor, txn.user, signup)

            expect(result['polissa_id']).to(be_above(0))
            expect(result['polissa_id']).not_to(equal(existing_polissa_id))

    def test_cannot_create_signup_when_is_deleted(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            user = txn.user
            self._setup(cursor, user)
            content = get_serialized_signup('default.json')
            signup = json.loads(content)
            signup['deleted'] = True

            # Act
            signup_committer_obj = self.openerp.pool.get('lucera.signup.committer')

            # Assert
            with self.assertRaises(osv.except_osv) as cm:
                result = signup_committer_obj.commit(txn.cursor, txn.user, signup)

            expect(cm.exception.name).to(be('ValidateError'))

    def test_can_create_signup_when_user_has_previous_debt(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            user = txn.user
            pool = self.openerp.pool
            self._setup(cursor, user)

            signup_vals = {'has_previous_debt': True}
            override_vals = {'debt_amount': 10, 'active': True}
            result_with_debt = create_test_electricity_polissa(pool, cursor, user, override_vals=override_vals)

            content = get_serialized_signup('default.json')
            signup = json.loads(content)
            signup['user']['rel'] = 'existing'
            signup['user']['id'] = result_with_debt['user_id']

            # Act
            signup_committer_obj = self.openerp.pool.get('lucera.signup.committer')
            result = signup_committer_obj.commit(txn.cursor, txn.user, signup)

            # Assert
            # Signup
            signup = self.openerp.pool.get('lucera.signup').browse(cursor, user, result['signup_id'])
            expect(signup.electricity_signup_id.has_previous_debt).to(be_true)


    def test_can_create_signup_when_electricity_product_kind_is_fixed_price(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            user = txn.user
            self._setup(cursor, user)
            signup_json = json.loads(get_serialized_signup('default.json'))
            signup_json['electricity']['product']['kind'] = 'fixed_price'

            # Act
            signup_committer_obj = self.openerp.pool.get('lucera.signup.committer')
            result = signup_committer_obj.commit(cursor, user, signup_json)

            # Assert
            self._check_result(cursor, user, result)

            # Signup
            signup_obj = self.openerp.pool.get('lucera.signup')
            signup = signup_obj.browse(cursor, user, result['signup_id'])
            expect(signup.electricity_signup_id.polissa_id.id).to(equal(result['polissa_id']))
            expect(signup.electricity_signup_id.supply_point_code_source).to(equal('invoice_study'))
            expect(signup.external_id).to(equal(str(signup_json['id'])))

            # Contract
            polissa = self.openerp.pool.get('giscedata.polissa').browse(cursor, user, result['polissa_id'])
            expect(polissa.llista_preu.kind).to(equal('fixed_price'))
            expect(polissa.llista_preu.type).to(equal('sale'))

    def test_can_create_signup_when_electricity_product_price_list_is_set_and_ignores_kind(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            user = txn.user
            self._setup(cursor, user)
            pool = self.openerp.pool
            price_list_id = pool.get('product.pricelist').search(cursor, user, [('kind', '=', 'fixed_price')])[0]
            signup_json = json.loads(get_serialized_signup('default.json'))
            signup_json['electricity']['product']['price_list_id'] = price_list_id
            signup_json['electricity']['product']['kind'] = 'variable_price'

            # Act
            signup_committer_obj = pool.get('lucera.signup.committer')
            result = signup_committer_obj.commit(cursor, user, signup_json)

            # Assert
            self._check_result(cursor, user, result)

            # Signup
            signup_obj = pool.get('lucera.signup')
            signup = signup_obj.browse(cursor, user, result['signup_id'])
            expect(signup.electricity_signup_id.polissa_id.id).to(equal(result['polissa_id']))
            expect(signup.electricity_signup_id.supply_point_code_source).to(equal('invoice_study'))
            expect(signup.external_id).to(equal(str(signup_json['id'])))

            # Contract
            polissa = self.openerp.pool.get('giscedata.polissa').browse(cursor, user, result['polissa_id'])
            expect(polissa.llista_preu.kind).to(equal('fixed_price'))
            expect(polissa.llista_preu.type).to(equal('sale'))

    def test_can_create_signup_with_motiu_canvi(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            user = txn.user
            self._setup(cursor, user)
            pool = self.openerp.pool
            signup_json = json.loads(get_serialized_signup('default.json'))
            signup_json['reason'] = {
                'code': 'MAG',
                'comments': 'Lorem Ipsum'
            }

            # Act
            signup_committer_obj = pool.get('lucera.signup.committer')
            result = signup_committer_obj.commit(cursor, user, signup_json)

            # Motiu Canvi
            polissa_motiu_canvi = self.openerp.pool.get('giscedata.polissa.motiu.canvi')\
                .browse(cursor, user, result['polissa_motiu_canvi_id'])
            expect(polissa_motiu_canvi.motiu_id.codi).to(equal('MAG'))
            expect(polissa_motiu_canvi.observacions).to(equal('Lorem Ipsum'))

    def test_can_create_signup_with_collective(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            user = txn.user
            self._setup(cursor, user)
            pool = self.openerp.pool
            signup_json = json.loads(get_serialized_signup('electricity_and_gas.json'))
            signup_json['category']['collective'] = 'clear'

            # Act
            signup_committer_obj = pool.get('lucera.signup.committer')
            result = signup_committer_obj.commit(cursor, user, signup_json)

            # Electricity contract
            polissa = self.openerp.pool.get('giscedata.polissa').browse(cursor, user, result['polissa_id'])
            expect(polissa.collective).to(equal('clear'))

            # Gas contract
            gas_polissa = self.openerp.pool.get('giscegas.polissa').browse(cursor, user, result['gas_polissa_id'])
            expect(gas_polissa.collective).to(equal('clear'))

    def test_uses_sips_distribution_company_code_if_available_in_sips(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            uid = txn.user
            self._setup(cursor, uid)
            pool = self.openerp.pool
            participant_obj = pool.get("giscemisc.participant")
            ree_code = '0031'
            sifco_code = '0029'
            signup_json = json.loads(get_serialized_signup('electricity_and_gas.json'))
            signup_json['sips']['electricity']['company_code'] = ree_code
            signup_json['sips']['gas']['company_code'] = sifco_code

            # Act
            signup_committer_obj = pool.get('lucera.signup.committer')
            result = signup_committer_obj.commit(cursor, uid, signup_json)

            # Electricity contract
            polissa = pool.get('giscedata.polissa').browse(cursor, uid, result['polissa_id'])
            expect(polissa.cups.distribuidora_id.ref).to(equal(ree_code))
            expect(polissa.distribuidora.id).to(equal(polissa.cups.distribuidora_id.id))

            # Gas contract
            gas_polissa = pool.get('giscegas.polissa').browse(cursor, uid, result['gas_polissa_id'])
            participant = participant_obj.get_from_partner(cursor, uid, gas_polissa.cups.distribuidora_id.id)
            expect(participant.codi_sifco).to(equal(sifco_code))
            expect(gas_polissa.distribuidora.id).to(equal(gas_polissa.cups.distribuidora_id.id))
