# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from expects import *
from utils import get_serialized_signup, setup_tarifa
import json
from datetime import datetime


class SignUpTests(testing.OOTestCase):
    """ Test that a polissa cannot be activated in multiple cases"""

    def test_can_commit_signup(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            pool = self.openerp.pool
            cursor = txn.cursor
            user = txn.user
            setup_tarifa(pool, cursor, user)
            content = get_serialized_signup('default.json')
            signup_json = json.loads(content)
            vals = {
                'signup_serialized': content
            }
            signup_request_obj = pool.get('lucera.signup.request')
            signup_request_id = signup_request_obj.create(cursor, user, vals)

            # Act
            results = signup_request_obj.commit(cursor, user, [signup_request_id])
            result = results[signup_request_id]

            # Assert
            signup_id = result['signup_id']
            expect(signup_request_id).to(be_above(0))

            # Signup
            signup_obj = pool.get('lucera.signup')
            signup = signup_obj.browse(cursor, user, signup_id)

            # Via
            via = signup_json['via']
            expect(signup.via_channel).to(equal(via['channel']))
            expect(signup.via_lead_id).to(equal(via['lead']['id'])) 
            expect(signup.via_lead_type).to(equal(via['lead']['type'])) 
            expect(signup.via_invoice_study_id).to(equal(via['invoice_study']['id'])) 
            expect(signup.via_invoice_study_date).to(equal('2018-02-02 02:02:02'))
            expect(signup.via_invoice_study_electricity_recommendation_power)\
                .to(equal(via['invoice_study']['electricity']['recommendation']['power']))
            expect(signup.via_invoice_study_electricity_recommendation_tariff_code)\
                .to(equal(via['invoice_study']['electricity']['recommendation']['tariff_code']))
            expect(signup.via_personal_study_id).to(equal(via['personal_study']['id']))
            expect(signup.via_personal_study_date).to(equal('2018-02-02 02:02:02'))
            expect(signup.via_personal_study_electricity_recommendation_power)\
                .to(equal(via['personal_study']['electricity']['recommendation']['power']))
            expect(signup.via_personal_study_electricity_recommendation_tariff_code)\
                .to(equal(via['personal_study']['electricity']['recommendation']['tariff_code']))

            # Electricity
            product_json = signup_json['electricity']['product']
            location = signup_json['electricity']['supply_point']['location']
            location_address = signup_json['electricity']['supply_point']['location']['address']

            # Electricity - ATR
            expect(signup.electricity_signup_id.holder_change).to(be_false)
            expect(signup.electricity_signup_id.has_current_atr_contract).to(be_true)
            expect(signup.electricity_signup_id.is_atr_contract_change).to(be_false)
            expect(signup.electricity_signup_id.current_power*1000).to(equal(product_json['access_contract']['current']['power']))
            expect(signup.electricity_signup_id.current_tariff.id).to(be_above(0))

            # Contract
            polissa_id = long(result['polissa_id'])
            polissa = pool.get('giscedata.polissa').browse(cursor, user, polissa_id)
            expect(polissa.cups.id).to(equal(result['cups_id']))
            expect(polissa.cups.name).to(equal(signup_json['electricity']['supply_point']['code']))
            expect(polissa.active).to(equal(True))
            expect(polissa.tarifa.id).to(be_false)
            expect(polissa.titular.id).to(equal(result['user_id']))
            expect(polissa.titular.vat).to(equal('ES' + signup_json['user']['identity_document']['code']))
            expect(polissa.titular.name).to(equal('De la Calzada, Chiquito'))
            expect(polissa.titular.address[0].email).to(equal(signup_json['user']['email'].lower()))
            expect(polissa.titular.address[0].phone).to(equal(signup_json['user']['phone']))
            expect(polissa.state).to(equal('esborrany'))
            expect(polissa.data_firma_contracte).to(equal(False))
            expect(polissa.tipus_vivenda).to(equal('habitual'))
            expect(polissa.cnae.id).to(equal(986))
            expect(polissa.data_baixa).to(equal(False))
            expect(polissa.tipo_pago.code).to(equal('RECIBO_CSB'))
            expect(polissa.direccio_pagament.id).to(equal(result['user_address_id']))
            expect(polissa.pagador.id).to(equal(result['user_id']))
            expect(polissa.pagador_sel).to(equal('titular'))
            expect(polissa.direccio_notificacio.id).to(equal(result['user_address_id']))
            expect(polissa.payment_mode_id.name).to(equal('Recibo mensual BBVA'))
            expect(polissa.llista_preu.name).to(equal('Lucera Precios Variables'))
            expect(polissa.llista_preu.type).to(equal('sale'))
            expect(polissa.bank.id).to(equal(result['bank_account_id']))
            expect(polissa.bank.iban).to(equal('ES8800303123410000260271'))
            expect(polissa.facturacio).to(equal(1))
            expect(polissa.facturacio_potencia).to(equal('icp'))
            expect(polissa.altre_p.id).to(equal(False))
            expect(polissa.affiliate_code).to(equal(signup_json['promotion']['affiliate']['code']))
            expect(polissa.affiliate_external_id).to(equal(signup_json['promotion']['affiliate']['external_id']))
            expect(polissa.potencia).to(be_false)
            expect(polissa.observacions).to(be_false)

            # Electricity - Supply point
            expect(polissa.cups.name).to(equal(signup_json['electricity']['supply_point']['code']))
            expect(polissa.cups.id_municipi.ine).to(equal(location['town_code']))
            expect(polissa.cups.dp).to(equal(location['postal_code']))
            expect(polissa.cups.aclarador).to(equal(location_address['clarifying']))
            expect(polissa.cups.nv.lower()).to(equal(location_address['street_name'].lower()))
            expect(polissa.cups.pnp).to(equal(location_address['number']))
            expect(polissa.cups.es).to(equal(location_address['stair']))
            expect(polissa.cups.pt).to(equal(location_address['floor']))
            expect(polissa.cups.pu).to(equal(location_address['door']))

            # Mandate
            mandates_id = self.openerp.pool.get('payment.mandate').search(
                cursor, user, [('reference', '=', 'giscedata.polissa,%s' % polissa.id)])
            expect(mandates_id).not_to(be_empty)
            expect(mandates_id[0]).to(be_above(0))

            # Attributions
            attribution_obj = self.openerp.pool.get('lucera.signup.attribution')
            attribution_ids = attribution_obj.search(cursor, user, [('signup_id', '=', signup_id)], order='index')
            attributions = attribution_obj.browse(cursor, user, attribution_ids)
            expect(len(attributions)).to(equal(2))

            expect(attributions[0].source).to(equal('source1'))
            expect(attributions[0].medium).to(equal('medium1'))
            expect(attributions[0].term).to(equal('term1'))
            expect(attributions[0].content).to(equal('content1'))
            expect(attributions[0].name).to(equal('name1'))
            expect(attributions[0].identifier).to(equal('id1'))
            expect(attributions[0].date).to(equal('2018-02-15 03:03:03'))
            expect(attributions[0].is_first).to(be_true)
            expect(attributions[0].is_last).to(be_false)
            expect(attributions[0].index).to(equal(0))

            expect(attributions[1].source).to(equal('source2'))
            expect(attributions[1].medium).to(equal('medium2'))
            expect(attributions[1].term).to(equal('term2'))
            expect(attributions[1].content).to(equal('content2'))
            expect(attributions[1].name).to(equal('name2'))
            expect(attributions[1].identifier).to(equal('id2'))
            expect(attributions[1].date).to(equal('2018-03-10 03:04:05'))
            expect(attributions[1].is_first).to(be_false)
            expect(attributions[1].is_last).to(be_true)
            expect(attributions[1].index).to(equal(1))

    def test_can_get_if_signup_is_activable(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            pool = self.openerp.pool
            cursor = txn.cursor
            user = txn.user
            setup_tarifa(pool, cursor, user)
            content = get_serialized_signup('default.json')

            # Act
            vals = {
                'signup_serialized': content
            }
            signup_request_obj = pool.get('lucera.signup.request')
            signup_request_id = signup_request_obj.create(cursor, user, vals)
            results = signup_request_obj.commit(cursor, user, signup_request_id)
            result = results[signup_request_id]
            signup_id = result['signup_id']

            # Assert
            expect(signup_id).to(be_above(0))

    def test_can_get_if_polissa_is_activable(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            pool = self.openerp.pool
            cursor = txn.cursor
            user = txn.user
            setup_tarifa(pool, cursor, user)
            content = get_serialized_signup('user_holder_and_payer_are_the_same_and_supply_point_found.json')

            # Act
            vals = {
                'signup_serialized': content
            }
            signup_request_obj = pool.get('lucera.signup.request')
            signup_request_id = signup_request_obj.create(cursor, user, vals)
            results = signup_request_obj.commit(cursor, user, signup_request_id)
            result = results[signup_request_id]
            signup_id = result['signup_id']

            # Assert
            expect(signup_id).to(be_above(0))

    def test_can_get_switching_info(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            pool = self.openerp.pool
            cursor = txn.cursor
            user = txn.user
            setup_tarifa(pool, cursor, user)
            signup_electricity_obj = pool.get('lucera.signup.electricity')

            # Act
            tests = [({'holder_change': False, 'has_current_atr_contract': True, 'is_atr_contract_change': False},
                      {'process_code': 'C1', 'config_vals': {'activacio_cicle': 'N'}}),

                     ({'holder_change': True, 'has_current_atr_contract': True, 'is_atr_contract_change': False},
                      {'process_code': 'C2', 'config_vals': {'change_type': 'owner'}}),

                     ({'holder_change': False, 'has_current_atr_contract': True, 'is_atr_contract_change': True},
                      {'process_code': 'C2', 'config_vals': {'change_type': 'tarpot'}}),

                     ({'holder_change': True, 'has_current_atr_contract': True, 'is_atr_contract_change': True},
                      {'process_code': 'C2', 'config_vals': {'change_type': 'both'}}),

                     ({'holder_change': False, 'has_current_atr_contract': False, 'is_atr_contract_change': False},
                      {'process_code': 'A3', 'config_vals': None}),
                     ]

            for t in tests:
                vals = t[0]
                vals.update({'active': True})
                s_id = signup_electricity_obj.create(cursor, user, vals)

                result = t[1]
                code = signup_electricity_obj.get_switching_process_code(cursor, user, s_id)
                expect(code).to(equal(result['process_code']))
                # TODO: Now we need to create a polissa to get the switching config vals
                # config_vals = signup_electricity_obj.get_switching_config_vals(cursor, user, s_id)
                # if not config_vals:
                #     expect(config_vals).to(equal(result['config_vals']))
                # else:
                #     for key in config_vals.keys():
                #         expect(config_vals[key]).to(equal(result['config_vals'][key]))

    def test_can_get_switching_data(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            pool = self.openerp.pool
            cursor = txn.cursor
            user = txn.user
            setup_tarifa(pool, cursor, user)
            signup_electricity_obj = pool.get('lucera.signup.electricity')

            # Act
            tests = [({'holder_change': False, 'has_current_atr_contract': True, 'is_atr_contract_change': False},
                      {'process_code': 'C1', 'config_vals': {'activacio_cicle': 'N'}}),

                     ({'holder_change': True, 'has_current_atr_contract': True, 'is_atr_contract_change': False},
                      {'process_code': 'C2', 'config_vals': {'change_type': 'owner'}}),

                     ({'holder_change': False, 'has_current_atr_contract': True, 'is_atr_contract_change': True},
                      {'process_code': 'C2', 'config_vals': {'change_type': 'tarpot'}}),

                     ({'holder_change': True, 'has_current_atr_contract': True, 'is_atr_contract_change': True},
                      {'process_code': 'C2', 'config_vals': {'change_type': 'both'}}),

                     ({'holder_change': False, 'has_current_atr_contract': False, 'is_atr_contract_change': False},
                      {'process_code': 'A3', 'config_vals': None}),
                     ]

            for t in tests:
                vals = t[0]
                vals.update({'active': True})
                s_id = signup_electricity_obj.create(cursor, user, vals)

                result = t[1]
                code = signup_electricity_obj.get_switching_process_code(cursor, user, s_id)
                expect(code).to(equal(result['process_code']))
                # TODO: Now we need to create a polissa to get the switching config vals
                # config_vals = signup_electricity_obj.get_switching_config_vals(cursor, user, s_id)
                # if not config_vals:
                #     expect(config_vals).to(equal(result['config_vals']))
                # else:
                #     for key in config_vals.keys():
                #         expect(config_vals[key]).to(equal(result['config_vals'][key]))