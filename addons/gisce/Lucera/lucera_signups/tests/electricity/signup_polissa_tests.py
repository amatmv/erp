# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from expects import *
import lucera_signups.tests.utils


class SignUpPolissaTests(testing.OOTestCase):

    def test_signup_is_not_copied_and_bus_event_is_not_sent_when_cloning_a_polissa(self):
        with Transaction().start(self.database) as txn:
            # Arrange
            pool = self.openerp.pool
            cursor = txn.cursor
            user = txn.user

            result = lucera_signups.tests.utils.create_test_electricity_polissa_with_signup(pool, cursor, user)
            polissa_id = result['polissa_id']
            signup_id = result['signup_id']

            # Act
            polissa_obj = pool.get('giscedata.polissa')
            copied_polissa_id = polissa_obj.copy(cursor, user, polissa_id, default={})

            # Assert
            signup_obj = pool.get('lucera.signup.electricity')
            signup_ids = signup_obj.search(cursor, user, [('polissa_id', '=', copied_polissa_id)])
            expect(signup_ids).to(be_empty)

            # Bus message
            bus_message_obj = self.openerp.pool.get('lucera.bus.message')
            bus_message_ids = bus_message_obj.search(cursor, user,
                                                     [('name', '=', 'giscedata.polissa.created')],
                                                     order='create_date desc')
            expect(bus_message_ids).not_to(be_empty)
            expect(len(bus_message_ids)).to(equal(1))

            bus_message = bus_message_obj.browse(cursor, user, bus_message_ids[0])
            expect(bus_message.name).to(equal('giscedata.polissa.created'))
            expect(str(polissa_id) in bus_message.content).to(be_true)
            expect(str(copied_polissa_id) in bus_message.content).to(be_false)


