# -*- coding: utf-8 -*-
import os
import json
import random
import uuid
from lucera_common.fake import *
from lucera_common.testing import *
from lucera_common.testing.electricity import *


def get_serialized_signup(file_name, randomize=True):

    dir_path = os.path.dirname(os.path.realpath(__file__))
    file_path = os.path.join(dir_path, 'data', file_name)
    content = open(file_path, 'r').read()

    if not randomize:
        return content

    signup = json.loads(content)

    signup['id'] = random.randint(1, 999999999)

    if 'electricity' in signup:
        if signup['electricity']['supply_point']:
            signup['electricity']['supply_point']['code'] = get_random_cups_code()

    if 'gas' in signup:
        if signup['gas']['supply_point']:
            signup['gas']['supply_point']['code'] = get_random_cups_code()  # TODO: Use Gas distri code

    if signup['user']['rel'] == 'none':
        signup['user']['identity_document']['code'] = get_random_nif()
        signup['user']['email'] = get_random_email()
        signup['user']['phone'] = get_random_phone()

    if signup['fiscal_customer']['rel'] == 'none':
        if signup['fiscal_customer']['identity_document']:
            signup['fiscal_customer']['identity_document']['code'] = get_random_nif()
        signup['fiscal_customer']['email'] = get_random_email()
        signup['fiscal_customer']['phone'] = get_random_phone()

    if signup['payment_method']['bank_account']['owner']['rel'] == 'none':
        if signup['payment_method']['bank_account']['owner']['identity_document']:
            signup['payment_method']['bank_account']['owner']['identity_document']['code'] = get_random_nif()
        signup['payment_method']['bank_account']['owner']['email'] = get_random_email()
        signup['payment_method']['bank_account']['owner']['phone'] = get_random_phone()

    return json.dumps(signup)


def setup_tarifa(pool, cursor, uid):
    user = pool.get('res.users').browse(cursor, uid, uid)
    if not user.company_id.partner_id.ref:
        partner_id = pool.get('ir.model.data').get_object_reference(
            cursor, uid, 'base', 'main_partner'
        )[1]
        pool.get('res.partner').write(cursor, uid, [partner_id], {
            'ref': '0971'
        })

    # giscedata_polissa_tarifa_obj = pool.get('giscedata.polissa.tarifa')
    #
    # # Add relation between giscedata_polissa_tarifa and pricelist
    # sql = """insert into giscedata_polissa_tarifa_pricelist (tarifa_id, pricelist_id)
    #          select a.id, (select id from product_pricelist where name = '{}' and active=True limit 1)
    #          from (select DISTINCT id from giscedata_polissa_tarifa) a """.format(LUCERA_PRODUCT_PRICE_LIST_NAME)
    # cursor.execute(sql)


def create_test_electricity_polissa_with_signup(pool, cursor, uid, electricity_signup_vals=None, **kwargs):

    # Create electricity signup
    vals = {'active': True}
    if electricity_signup_vals:
        vals.update(electricity_signup_vals)
    signup_electricity_obj = pool.get('lucera.signup.electricity')
    electricity_signup_id = signup_electricity_obj.create(cursor, uid, vals)

    # Create global signup
    signup_obj = pool.get('lucera.signup')
    vals = {
        'external_id': uuid.uuid4().hex,
        'original_signup_serialized': '',
        'electricity_signup_id': electricity_signup_id
    }
    signup_id = signup_obj.create(cursor, uid, vals)

    # Create polissa
    result = create_test_electricity_polissa(pool, cursor, uid, **kwargs)

    # Update electricity signup with polissa_id
    signup_electricity_obj.write(cursor, uid, electricity_signup_id, {'polissa_id': result['polissa_id']})

    result['signup_id'] = signup_id
    result['electricity_signup_id'] = electricity_signup_id

    return result


def create_test_gas_polissa_with_signup(pool, cursor, uid, gas_signup_vals=None, **kwargs):

    # Create electricity signup
    vals = {'active': True}
    if gas_signup_vals:
        vals.update(gas_signup_vals)
    signup_gas_obj = pool.get('lucera.signup.gas')
    gas_signup_id = signup_gas_obj.create(cursor, uid, vals)

    # Create global signup
    signup_obj = pool.get('lucera.signup')
    vals = {
        'external_id': uuid.uuid4().hex,
        'original_signup_serialized': '',
        'gas_signup_id': gas_signup_id
    }
    signup_id = signup_obj.create(cursor, uid, vals)

    # Create polissa
    result = create_test_gas_polissa(pool, cursor, uid, **kwargs)

    # Update electricity signup with polissa_id
    signup_gas_obj.write(cursor, uid, gas_signup_id, {'polissa_id': result['gas_polissa_id']})

    result['signup_id'] = signup_id
    result['gas_signup_id'] = gas_signup_id

    return result
