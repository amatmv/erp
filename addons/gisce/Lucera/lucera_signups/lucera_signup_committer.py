# -*- coding: utf-8 -*-
import json

from osv import osv
from fuzzywuzzy import fuzz
from giscedata_cups.dso_cups.cups import get_dso as get_electricity_dso
from giscegas_cups.dso_cups.cups import get_dso as get_gas_dso
from lucera_common.config import *
from lucera_common import pricelist_selector
from lucera_common import check
from tools.translate import _
from utils import *


class LuceraSignUpCommitter(osv.osv_memory):
    _name = 'lucera.signup.committer'

    def _get_electricity_distri_id_by_cups(self, cursor, uid, code):
        if not code:
            return False
        code = normalize_cups(code)
        dso = get_electricity_dso(code)
        return self._get_electricity_distri_id_by_dso(cursor, uid, dso)

    def _get_electricity_distri_id_by_dso(self, cursor, uid, dso):
        if not dso:
            return False

        # Try to search by participant
        participant_obj = self.pool.get('giscemisc.participant')
        participant_id = participant_obj.get_id_from_ree(cursor, uid, dso)
        if participant_id:
            partner_id = participant_obj.get_partner_id(cursor, uid, participant_id)
            return partner_id

        # Try to search by partner
        partner_obj = self.pool.get('res.partner')
        search_params = [('ref', '=', dso), ('active', '=', True)]
        partner_ids = partner_obj.search(cursor, uid, search_params)
        if len(partner_ids) == 0:
            raise osv.except_osv(_('ValidateError'), _('Distribution company for dso {} not found!').format(dso))

        return partner_ids[0]

    def _get_gas_distri_id_by_cups(self, cursor, uid, code):
        if not code:
            return False
        code = normalize_cups(code)
        participant_obj = self.pool.get('giscemisc.participant')
        dso = get_gas_dso(code)
        return self._get_gas_distri_id_by_dso(cursor, uid, dso)

    def _get_gas_distri_id_by_dso(self, cursor, uid, dso):
        if not dso:
            return False
        participant_obj = self.pool.get('giscemisc.participant')
        participant_id = participant_obj.get_id_from_sifco(cursor, uid, dso)
        if not participant_id:
            raise osv.except_osv(_('ValidateError'), _('Distribution company for cups {} not found!').format(dso))
        partner_id = participant_obj.get_partner_id(cursor, uid, participant_id)
        return partner_id

    def _create_electricity_cups(self, cursor, uid, supply_point, sips_data, context):
        if not supply_point:
            return False

        cups_code = normalize_cups(supply_point.get('code', False))
        location = supply_point.get('location', {})
        if not cups_code and not location:
            raise osv.except_osv(_('ValidateError'), _('Cups must have a location if cups code is not set!'))

        vals = {
            'dp': location.get('postal_code', False),
        }

        if not cups_code:
            vals['active'] = False
        else:
            vals['active'] = True
            vals['name'] = cups_code
            if supply_point.get('sips_exists', False) and sips_data and sips_data.get('company_code'):
                vals['distribuidora_id'] = self._get_electricity_distri_id_by_dso(cursor, uid,
                                                                                  sips_data.get('company_code'))
            if 'distribuidora_id' not in vals or not vals['distribuidora_id']:
                vals['distribuidora_id'] = self._get_electricity_distri_id_by_cups(cursor, uid, cups_code)

        # Address
        location_address = location.get('address', {})

        if isinstance(location_address, basestring):
            vals.update({
                'nv': location_address
            })
        else:
            vals.update({
                'nv': location_address.get('street_name', False),
                'pnp': location_address.get('number', False),
                'es': location_address.get('stair', False),
                'pt': location_address.get('floor', False),
                'pu': location_address.get('door', False),
                'aclarador': location_address.get('clarifying', False),
            })

        # Id municipi
        town_ine_code = location.get('town_code', False)
        if town_ine_code:
            municipi = get_municipi_by_ine_code(self.pool, cursor, uid, location['town_code'])
            vals['id_municipi'] = municipi.id

        # Search for the cups even if it is not active
        cups_obj = self.pool.get('giscedata.cups.ps')
        if cups_code:
            cups_ids = cups_obj.search(cursor, uid, [('name', '=', cups_code)], context={'active_test': False})
            if cups_ids:
                if self.pool.get('giscedata.polissa').search_count(
                        cursor, uid, [('cups', 'in', cups_ids), ('active', '=', True)]):
                    raise osv.except_osv(_('ValidateError'), _('El CUPS ya existe en un contrato activo'))
                cups_id = cups_ids[0]
                cups_obj.write(cursor, uid, cups_id, vals)
            else:
                cups_id = cups_obj.create(cursor, uid, vals)
        else:
            cups_id = cups_obj.create(cursor, uid, vals)

        return cups_id

    def _create_gas_cups(self, cursor, uid, supply_point, sips_data, context):
        if not supply_point:
            return False

        cups_code = normalize_cups(supply_point.get('code', False))
        location = supply_point.get('location', {})
        if not cups_code and not location:
            raise osv.except_osv(_('ValidateError'), _('Cups must have a location if cups code is not set!'))

        vals = {
            'dp': location.get('postal_code', False),
        }

        if not cups_code:
            vals['active'] = False
        else:
            vals['active'] = True
            vals['name'] = cups_code
            if supply_point.get('sips_exists', False) and sips_data and sips_data.get('company_code'):
                vals['distribuidora_id'] = self._get_gas_distri_id_by_dso(cursor, uid, sips_data.get('company_code'))
            if 'distribuidora_id' not in vals or not vals['distribuidora_id']:
                vals['distribuidora_id'] = self._get_gas_distri_id_by_cups(cursor, uid, cups_code)

        # Address
        location_address = location.get('address', {})

        if isinstance(location_address, basestring):
            vals.update({
                'nv': location_address
            })
        else:
            vals.update({
                'nv': location_address.get('street_name', False),
                'pnp': location_address.get('number', False),
                'es': location_address.get('stair', False),
                'pt': location_address.get('floor', False),
                'pu': location_address.get('door', False),
                'aclarador': location_address.get('clarifying', False),
            })

        # Id municipi
        town_ine_code = location.get('town_code', False)
        if town_ine_code:
            municipi = get_municipi_by_ine_code(self.pool, cursor, uid, location['town_code'])
            vals['id_municipi'] = municipi.id

        # Search for the cups even if it is not active
        cups_obj = self.pool.get('giscegas.cups.ps')
        if cups_code:
            cups_ids = cups_obj.search(cursor, uid, [('name', '=', cups_code)], context={'active_test': False})
            if cups_ids:
                if self.pool.get('giscegas.polissa').search_count(
                        cursor, uid, [('cups', 'in', cups_ids), ('active', '=', True)]):
                    raise osv.except_osv(_('ValidateError'), _('Cups already in active contract'))
                cups_id = cups_ids[0]
                cups_obj.write(cursor, uid, cups_id, vals)
            else:
                cups_id = cups_obj.create(cursor, uid, vals)
        else:
            cups_id = cups_obj.create(cursor, uid, vals)

        return cups_id

    def _get_partner_and_address(self, cursor, uid, partner_id, is_end_user=None):
        # Check that user exists and is end user
        search_params = [('id', '=', partner_id)]
        if is_end_user is not None:
            search_params.append(('is_end_user', '=', is_end_user))
        partner_obj = self.pool.get('res.partner')
        partner_ids = partner_obj.search(cursor, uid, search_params)
        if not partner_ids:
            raise osv.except_osv(_('ValidateError'), _('Partner with id {} does not exists'.format(partner_id)))
        partner_id = partner_ids[0]
        partner = partner_obj.browse(cursor, uid, partner_id)
        return partner.id, partner.address[0].id

    def _create_partner_address(self, cursor, uid, partner_id, contact_data, location, is_end_user):
        """Creates partner address"""
        partner = self.pool.get('res.partner').browse(cursor, uid, partner_id)
        if partner.address:
            raise osv.except_osv(_('ValidateError'), _('Partner already has an existing address!'))

        vals = {
            'active': True,
            'partner_id': partner_id,
            'name': normalize_name(contact_data['name'], contact_data.get('surnames', False)),
            'phone': normalize_phone(contact_data.get('phone', False)),
            'email': normalize_email(contact_data.get('email', False))
        }

        # If is end user or we are passing an email
        if (is_end_user or vals['email']) and not is_valid_email(vals['email']):
            raise osv.except_osv(_('ValidateError'), _('End users must have a valid email!'))

        if location:
            # Check that existing address is the one from the user
            if location.get('rel', False) == 'existing':
                existing_partner_address_id = location.get('id', False)
                if not existing_partner_address_id:
                    raise osv.except_osv(_('ValidateError'), _('Partner rel existing without id!'))
                copied = self.pool.get('res.partner.address').copy(cursor, uid, existing_partner_address_id, vals)
                return copied

            try:
                address_location = map_to_erp_location(self.pool, cursor, uid, location)
            except ValidateException as e:
                raise osv.except_osv(e.name, e.value)

            vals.update(address_location)

        partner_address_obj = self.pool.get('res.partner.address')

        return partner_address_obj.create(cursor, uid, vals)

    def _create_partner(self, cursor, uid, partner_data, is_end_user=False, analytics_user_id=False):
        """Creates a new partner"""
        partner_obj = self.pool.get('res.partner')
        # Normalize values
        vat = normalize_vat(partner_data['identity_document']['code'])
        name = normalize_name(partner_data['name'], partner_data.get('surnames', None))
        # TODO: Get default payment_type_customer, and property_payment_term
        payment_type_customer = 1
        property_payment_term = 3

        vals = {
            'active': True,
            'name': name,
            # 'lang': 'es_ES',
            'customer': True,
            'supplier': False,
            'is_end_user': is_end_user,
            'vat': vat,
            'vat_subjected': True,
            'payment_type_customer': payment_type_customer,
            'include_in_mod347': True,
            'property_payment_term': property_payment_term,
            'analytics_user_id': analytics_user_id
        }

        return partner_obj.create(cursor, uid, vals)

    def _create_partner_and_address(self, cursor, uid, partner_data, location, is_end_user=False,
                                    analytics_user_id=False):
        partner_id = self._create_partner(cursor, uid, partner_data, is_end_user, analytics_user_id)
        address_id = self._create_partner_address(cursor, uid, partner_id,
                                                  partner_data, location, is_end_user)
        return partner_id, address_id

    def _update_address_location_on_match(self, cursor, uid, address_id, new_location):
        if new_location.get('rel') == 'existing' and new_location.get('id') == str(address_id):
            return True
        address_obj = self.pool.get('res.partner.address')
        location = map_to_erp_location(self.pool, cursor, uid, new_location)
        address = address_obj.browse(cursor, uid, address_id)

        # If we didn't have the postal code we can update the address
        if not address.zip:
            address_obj.write(cursor, uid, address_id, location)
        else:
            street_name = normalize_street_name(address.nv)
            location_street_name = normalize_street_name(location.get('nv'))
            if street_name and location_street_name:
                is_street_name_match = fuzz.ratio(street_name, location_street_name) > 70
            else:
                is_street_name_match = True

            # If it is a match, we don't have to do anything if it matches
            match_result = {
                'id_municipi': address.id_municipi.id == location['id_municipi'],
                'zip': address.zip == location['zip'],
                'nv': address.nv and is_street_name_match
            }
            is_match = all(x for x in match_result.values())
            # If it does not match, there is a problem because we don't know the valid one
            if not is_match:
                error_msg = 'There is a full match with address {} but locations do not match'.format(address_id)
                raise osv.except_osv(_('ValidateError'), error_msg)

        return True

    def _create_or_get_user_and_address(self, cursor, uid, user, location, analytics_user_id=False, context=None):
        """Creates or gets user and address"""
        if not context:
            context = {}

        partner_obj = self.pool.get('res.partner')

        # User already exists
        if user['rel'] == 'existing':
            # Check that user exists and is end user
            partner_id, address_id = self._get_partner_and_address(cursor, uid, user['id'], True)
            partner_obj.set_analytics_user_id_if_empty(cursor, uid, partner_id, analytics_user_id)
            return partner_id, address_id

        # It may be an exact match
        match_params = {
            'name': normalize_name(user['name'], user.get('surnames', None)),
            'vat': user['identity_document']['code'],
            'email': user['email'],
            'phone': user.get('phone', '')
        }

        match_results = partner_obj.match_end_users(cursor, uid, match_params)

        if match_results:
            full_match = next((x for x in match_results if x['match_type'] == 'full'), None)
            if full_match:
                partner_id, address_id = self._get_partner_and_address(cursor, uid, full_match['id'], True)
                if not context.get('canceled', False):
                    if location:  # If it is a full match, only update if it is not False
                        self._update_address_location_on_match(cursor, uid, address_id, location)
                    partner_obj.set_analytics_user_id_if_empty(cursor, uid, partner_id, analytics_user_id)
                return partner_id, address_id
            else:
                if context.get('canceled', False):
                    # If it is canceled and we have a conflict, we create user and partner but not end user
                    return self._create_partner_and_address(cursor, uid, user, location)
                else:
                    raise osv.except_osv(_('ValidateError'), _('Ya existe algún gestor con este email, '
                                                               'teléfono o DNI/CIF. '
                                                               'Modifica el alta o el gestor para que '
                                                               'coincidan los datos'))
        # User does not exist, so we create it and the address
        return self._create_partner_and_address(cursor, uid, user, location, True, analytics_user_id)

    def _create_partner_bank_account(self, cursor, uid, result, bank_json):
        """Creates or returns a partner bank account"""
        partner_bank_obj = self.pool.get('res.partner.bank')
        user_id = result['user_id']
        partner_id = result['fiscal_customer_id']
        bank_account_owner_id = result['bank_account_owner_id']

        if bank_json['rel'] == 'existing':
            partner_bank_id = int(bank_json['id'])
            bank = partner_bank_obj.browse(cursor, uid, partner_bank_id)
            if not bank:
                raise osv.except_osv(_('ValidateError'), _('Existing parter bank account must exist!'))
            # Check that is owner or partner of this bank account to avoid hacks
            partner_obj = self.pool.get('res.partner')
            bank_account_ids = partner_obj.get_related_bank_accounts_ids(cursor, uid, user_id)

            if partner_bank_id not in bank_account_ids:
                raise osv.except_osv(_('ValidateError'), _('Existing parter bank account must exist for this user!'))
            result['bank_account_owner_id'] = bank.owner_id.id
            if bank.partner_id.id == partner_id and (not bank_account_owner_id
                                                     or bank.owner_id.id == bank_account_owner_id):
                return partner_bank_id
            else:
                # We must create a copy of the bank account, but changing the partner and owner
                defaults = {'partner_id': partner_id, 'owner_id': (bank_account_owner_id or bank.owner_id.id)}
                partner_bank_id = partner_bank_obj.copy(cursor, uid, bank_json['id'], defaults)
                return partner_bank_id
        else:
            if 'iban_code' in bank_json or 'account_number' in bank_json:
                iban_code = normalize_bank_account_number(bank_json.get('iban_code', bank_json.get('account_number')))
                if not iban_code:
                    return False
                country_id = get_country_id_by_code(self.pool, cursor, uid, 'ES')
                # Try to find existing matching account
                search_params = [('partner_id', '=', partner_id),
                                 ('owner_id', '=', bank_account_owner_id),
                                 ('iban', '=', iban_code)]
                bank_account_ids = partner_bank_obj.search(cursor, uid, search_params, order='id desc', limit=1)
                if bank_account_ids:
                    return bank_account_ids[0]
                # If it does not exist we create it

                res_bank_obj = self.pool.get('res.bank')
                bank_code = iban_code[4:8]
                bank_ids = res_bank_obj.search(cursor, uid, [('code', '=', bank_code)], limit=1)
                if not bank_ids:
                    raise osv.except_osv(_('ValidateError'), _('Bank {} not found!'.format(bank_code)))

                vals = {
                    'name': '',
                    'state': 'iban',
                    'country_id': country_id,
                    'partner_id': partner_id,
                    'owner_id': bank_account_owner_id,
                    'iban': iban_code,
                    'acc_country_id': country_id,
                    'bank': bank_ids[0]
                }
                partner_bank_id = partner_bank_obj.create(cursor, uid, vals)
                return partner_bank_id
            else:
                return False

    def _create_electricity_payment_mandate(self, cursor, uid, polissa_id, data_firma=None):
        polissa_obj = self.pool.get('giscedata.polissa')
        mandate_vals = polissa_obj.update_mandate(cursor, uid, polissa_id)
        mandate_obj = self.pool.get('payment.mandate')
        mandate_id = mandate_obj.create(cursor, uid, mandate_vals)
        return mandate_id

    def _create_gas_payment_mandate(self, cursor, uid, polissa_id, data_firma=None):
        polissa_obj = self.pool.get('giscegas.polissa')
        mandate_vals = polissa_obj.update_mandate(cursor, uid, polissa_id)
        mandate_obj = self.pool.get('payment.mandate')
        mandate_id = mandate_obj.create(cursor, uid, mandate_vals)
        return mandate_id

    def _create_electricity_polissa(self, cursor, uid, signup, result, context=None):
        """Creates the polissa"""
        if not context:
            context = {}

        if 'electricity' not in signup:
            return False

        user = signup['user']
        category = signup['category']
        electrity_contract = signup['electricity']
        fiscal_customer = signup['fiscal_customer']
        # bank_account_owner = signup['payment_method']['bank_account']['owner']
        supply_point = electrity_contract['supply_point']
        access_contract = electrity_contract['product']['access_contract']
        promotion = signup.get('promotion', {})
        affiliate = promotion.get('affiliate', {})

        res_partner_obj = self.pool.get('res.partner')

        user = res_partner_obj.browse(cursor, uid, result['user_id'])
        fiscal_customer = res_partner_obj.browse(cursor, uid, result['fiscal_customer_id'])
        bank_account_owner = res_partner_obj.browse(cursor, uid, result['bank_account_owner_id'])

        fiscal_customer_vat_enterprise = res_partner_obj.is_enterprise_vat(fiscal_customer.vat)

        # Get cnae and tipus_vivenda
        tipus_vivenda = 'no_habitual'
        if not fiscal_customer_vat_enterprise:
            if category['premises_type'] == 'primary_residence':
                cnae_code = '9820'
                tipus_vivenda = 'habitual'
            elif category['premises_type'] == 'secondary_residence':
                cnae_code = '9821'
            else:  # Others will be considered as secondary residence
                cnae_code = '9821'
        else:
            if fiscal_customer.vat and fiscal_customer.vat[:3] == 'ESH':
                cnae_code = '9810'
            elif 'activity_code' not in category:
                # raise osv.except_osv(_('ValidateError'), _('Business does not have an activity code!'))  # notrequired
                cnae_code = False
            else:
                cnae_code = category['activity_code']

        cnae_id = get_cnae_id_by_code(self.pool, cursor, uid, cnae_code) if cnae_code else False

        # Defaults
        conf_obj = self.pool.get('res.config')
        payment_mode_name = conf_obj.get(cursor, uid, 'lucera_signups_payment_mode_name')
        payment_mode_ids = self.pool.get('payment.mode').search(cursor, uid, [('name', '=', payment_mode_name)])
        payment_mode_id = payment_mode_ids[0]
        invoice_period_id = 1
        payment_type_customer_id = 1,
        payment_term_id = 3
        facturacio_distri = 1  # Mensual
        payment_type_id = self.pool.get('payment.type').search(
            cursor, uid, [('code', '=', LUCERA_PAYMENT_TYPE_CODE)])[0]
        price_list_id = False
        tariff_id = False

        # Tariff and Power
        power = False
        tariff_code = False
        if access_contract.get('changes', False):
            power = access_contract['target'].get('power', False)
            tariff_code = access_contract['target'].get('tariff_code', False)
        elif 'current' in access_contract:
            power = access_contract['current'].get('power', False)
            tariff_code = access_contract['current'].get('tariff_code', False)

        if power > 0:
            power = get_power_as_kilowatts(power)
            # if power > LUCERA_ALLOWED_MAXIMUM_POWER_IN_KW:
            #     raise osv.except_osv(_('ValidateError'), _('Tariff not allowed!'))
            if tariff_code not in LUCERA_ELECTRICITY_ALLOWED_TARIFF_OCSUM_CODES:
                raise osv.except_osv(_('ValidateError'), _('Tariff not allowed!'))

        # Price list
        price_list_id = signup['electricity']['product'].get('price_list_id')
        price_list_kind = signup['electricity']['product'].get('kind', 'variable_price')
        if not price_list_id:
            signup_date = signup['context']['created']['date']
            price_list_id, price_list_version_id = \
                pricelist_selector.get_current_price_list_by_kind(self.pool, cursor, uid, 'electricity',
                                                                  price_list_kind, signup_date)

        if tariff_code:
            check.is_valid_power_and_tariff(self.pool, cursor, uid, tariff_code, power)

        # Create contract
        cups_id = result['cups_id']
        cups = self.pool.get('giscedata.cups.ps').read(cursor, uid, cups_id, ['distribuidora_id']) if cups_id else None

        if result['user_id'] == result['fiscal_customer_id']:
            notificacio = 'pagador'
            altre_p = False
        elif result['user_id'] == result['atr_holder_id']:
            notificacio = 'titular'
            altre_p = False
        else:
            notificacio = 'altre_p'
            altre_p = result['user_id']

        active = True
        state = 'esborrany'
        if context.get('canceled', False):
            state = 'cancelada'
            active = False

        power_product_uom = self.pool.get('product.uom').search(cursor, uid, [('name', '=', 'kW/mes')])[0]

        # Link electricity and gas contract
        gas_polissa_id = result.get('gas_polissa_id', False) or electrity_contract.get('related_gas_contract_id', False)
        if gas_polissa_id:
            gas_polissa = self.pool.get('giscegas.polissa').browse(cursor, uid, gas_polissa_id)
            if not gas_polissa:
                raise osv.except_osv('Error', 'Could not create link between electricity and gas '
                                              'contract because gas contract does not exist')
            if gas_polissa.direccio_notificacio.id != result['user_address_id']:
                raise osv.except_osv('Error', 'Could not create link between electricity and gas'
                                              'contract because direccio_notificacio does not match'
                                              ' with current user address')

        vals = {
            'electricity_signup_id': result['electricity_signup_id'],
            'cnae': cnae_id,
            'bank': result['bank_account_id'],
            'comercialitzadora': 1,
            'distribuidora': cups['distribuidora_id'][0] if cups and cups.get('distribuidora_id') else False,
            'cups': cups_id,
            'notificacio': notificacio,
            'direccio_notificacio': result['user_address_id'],
            'direccio_pagament': result['fiscal_customer_address_id'],
            'llista_preu': price_list_id,
            'titular': result['atr_holder_id'],
            'state': state,
            'facturacio_distri': facturacio_distri,
            'tipus_vivenda': tipus_vivenda,
            'pagador_sel': 'titular',
            'facturacio': invoice_period_id,
            'payment_mode_id': payment_mode_id,
            'enviament': 'email',
            'pagador': result['fiscal_customer_id'],
            'tipo_pago': payment_type_id,
            'affiliate_code': affiliate.get('code', False),
            'affiliate_external_id': affiliate.get('external_id', False),
            'altre_p': altre_p,
            'lot_facturacio': False,
            'tarifa': False,
            'active': active,
            'property_unitat_potencia': power_product_uom,
            'pending_state': 'Correct',
            'pending_amount': 0,
            'debt_amount': 0,
            'versio_primera_factura': False,
            'gas_polissa_id': gas_polissa_id,
            'collective': category.get('collective', False)
        }

        polissa_obj = self.pool.get('giscedata.polissa')
        polissa_id = polissa_obj.create(cursor, uid, vals)

        return polissa_id

    def _create_motius_canvi(self, cursor, uid, signup, result, context=None):
        code = signup.get('reason', {}).get('code')

        if not code:
            return False

        # We can only createm motius canvis if there is a polissa (not with gas) yet
        motiu_canvi_obj = self.pool.get('giscedata.motiu.canvi')
        motiu_canvi_ids = motiu_canvi_obj.search(cursor, uid, [('codi', '=', code), ('type', '=', 'alta')])

        if not motiu_canvi_ids:
            return False

        motiu_canvi_id = motiu_canvi_ids[0]

        if 'polissa_id' in result:
            polissa_motiu_canvi_obj = self.pool.get('giscedata.polissa.motiu.canvi')

            vals = {
                'tipus_motiu': 'alta',
                'motiu_id': motiu_canvi_id,
                'polissa_id': result['polissa_id'],
                'observacions': signup.get('reason', {}).get('comments', False)
            }

            return polissa_motiu_canvi_obj.create(cursor, uid, vals)

        # TODO: Create motiu canvi por gas
        return False

    def _create_gas_polissa(self, cursor, uid, signup, result, context=None):
        """Creates the polissa"""
        if not context:
            context = {}

        if 'gas' not in signup:
            return False

        user = signup['user']
        category = signup['category']
        gas_signup = signup['gas']
        fiscal_customer = signup['fiscal_customer']
        # bank_account_owner = signup['payment_method']['bank_account']['owner']
        supply_point = gas_signup['supply_point']
        access_contract = gas_signup['product']['access_contract']
        promotion = signup.get('promotion', {})
        affiliate = promotion.get('affiliate', {})

        res_partner_obj = self.pool.get('res.partner')

        user = res_partner_obj.browse(cursor, uid, result['user_id'])
        fiscal_customer = res_partner_obj.browse(cursor, uid, result['fiscal_customer_id'])
        bank_account_owner = res_partner_obj.browse(cursor, uid, result['bank_account_owner_id'])

        fiscal_customer_vat_enterprise = res_partner_obj.is_enterprise_vat(fiscal_customer.vat)

        # Get cnae and tipus_vivenda
        tipus_vivenda = 'no_habitual'
        if not fiscal_customer_vat_enterprise:
            if category['premises_type'] == 'primary_residence':
                cnae_code = '9820'
                tipus_vivenda = 'habitual'
            elif category['premises_type'] == 'secondary_residence':
                cnae_code = '9821'
            else:  # Others will be considered as secondary residence
                cnae_code = '9821'
        else:
            if fiscal_customer.vat and fiscal_customer.vat[:3] == 'ESH':
                cnae_code = '9810'
            elif 'activity_code' not in category:
                # raise osv.except_osv(_('ValidateError'), _('Business does not have an activity code!'))  # notrequired
                cnae_code = False
            else:
                cnae_code = category['activity_code']

        cnae_id = get_cnae_id_by_code(self.pool, cursor, uid, cnae_code) if cnae_code else False

        # Defaults
        conf_obj = self.pool.get('res.config')
        payment_mode_name = conf_obj.get(cursor, uid, 'lucera_signups_payment_mode_name')
        payment_mode_ids = self.pool.get('payment.mode').search(cursor, uid, [('name', '=', payment_mode_name)])
        payment_mode_id = payment_mode_ids[0]
        invoice_period_id = 1
        payment_type_customer_id = 1,
        payment_term_id = 3
        facturacio_distri = 'M'  # Mensual (TAULA_PERIODICIDAD_LECTURA_FACTURACION)
        payment_type_id = self.pool.get('payment.type').search(
            cursor, uid, [('code', '=', LUCERA_PAYMENT_TYPE_CODE)])[0]
        price_list_id = False
        tariff_id = False

        # Tariff and Power

        tariff_code = False
        if access_contract.get('changes', False):
            # power = access_contract['target'].get('power', False)
            tariff_code = access_contract['target'].get('tariff_code', False)
        elif 'current' in access_contract:
            # power = access_contract['current'].get('power', False)
            tariff_code = access_contract['current'].get('tariff_code', False)

        # Price list
        price_list_id = signup['gas']['product'].get('price_list_id')
        price_list_kind = signup['gas']['product'].get('kind', 'variable_price')
        if not price_list_id:
            signup_date = signup['context']['created']['date']
            price_list_id, price_list_version_id = \
                pricelist_selector.get_current_price_list_by_kind(self.pool, cursor, uid, 'gas',
                                                                  price_list_kind, signup_date)

        daily_flow = False
        if tariff_code:
            tarifa_obj = self.pool.get('giscegas.polissa.tarifa')
            tariff_id = tarifa_obj.search(cursor, uid, [('codi_ocsum', '=', tariff_code)])[0]
            tariff = tarifa_obj.browse(cursor, uid, tariff_id)
            if not tariff:
                raise osv.except_osv(_('ValidateError'), _('Tariff with code {} not found!'.format(tariff_code)))
            daily_flow = round(tariff.consumo_max/365.0, 7)
            # if tariff.pot_min > power:
            #     raise osv.except_osv(_('ValidateError'), _('Power < pot_min!'))
            # if tariff.pot_max < power:
            #     raise osv.except_osv(_('ValidateError'), _('Power > pot_max!'))

        # Create contract
        cups_id = result['gas_cups_id']
        cups = self.pool.get('giscegas.cups.ps').read(cursor, uid, cups_id, ['distribuidora_id']) if cups_id else None

        if result['user_id'] == result['fiscal_customer_id']:
            notificacio = 'pagador'
            altre_p = False
        elif result['user_id'] == result['atr_holder_id']:
            notificacio = 'titular'
            altre_p = False
        else:
            notificacio = 'altre_p'
            altre_p = result['user_id']

        active = True
        state = 'esborrany'
        if context.get('canceled', False):
            state = 'cancelada'
            active = False

        # Fixed term uom
        fixed_term_uom_id = self.pool.get('product.uom').search(cursor, uid,
                                                                [('name', '=', LUCERA_GAS_FIXED_TERM_UOM_NAME)])[0]

        # Link electricity and gas contract
        electricity_polissa_id = int(result.get('polissa_id', False) or gas_signup.get(
            'related_electricity_contract_id', False))
        if electricity_polissa_id:
            electricity_polissa = self.pool.get('giscedata.polissa').browse(cursor, uid, electricity_polissa_id)
            if not electricity_polissa:
                raise osv.except_osv('Error', 'Could not create link between gas contract and electricity '
                                              'contract because electricity contract does not exist')
            if electricity_polissa.direccio_notificacio.id != result['user_address_id']:
                raise osv.except_osv('Error', 'Could not create link between gas contract and electricity '
                                              'contract because direccio_notificacio does not match with '
                                              'current user address')

        vals = {
            'gas_signup_id': result['gas_signup_id'],
            'cnae': cnae_id,
            'bank': result['bank_account_id'],
            'comercialitzadora': 1,
            'distribuidora': cups['distribuidora_id'][0] if cups and cups.get('distribuidora_id') else False,
            'cups': cups_id,
            'notificacio': notificacio,
            'direccio_notificacio': result['user_address_id'],
            'direccio_pagament': result['fiscal_customer_address_id'],
            'llista_preu': price_list_id,
            'titular': result['atr_holder_id'],
            'state': state,
            'facturacio_distri': facturacio_distri,
            'tipus_vivenda': tipus_vivenda,
            'pagador_sel': 'titular',
            'facturacio': invoice_period_id,
            'payment_mode_id': payment_mode_id,
            'enviament': 'email',
            'pagador': result['fiscal_customer_id'],
            'tipo_pago': payment_type_id,
            'affiliate_code': affiliate.get('code', False),
            'affiliate_external_id': affiliate.get('external_id', False),
            'altre_p': altre_p,
            'lot_facturacio': False,
            'tarifa': tariff_id,
            'caudal_diario': daily_flow,
            'active': active,
            'pending_state': 'Correct',
            'pending_amount': 0,
            'debt_amount': 0,
            'versio_primera_factura': False,
            'electricity_polissa_id': electricity_polissa_id,
            'property_unitat_terme_fix': fixed_term_uom_id,
            'collective': category.get('collective', False)
        }

        polissa_obj = self.pool.get('giscegas.polissa')
        polissa_id = polissa_obj.create(cursor, uid, vals)

        return polissa_id

    def _create_motius_canvi(self, cursor, uid, signup, result, context=None):
        code = signup.get('reason', {}).get('code')

        if not code:
            return False

        motiu_canvi_obj = self.pool.get('giscedata.motiu.canvi')
        motiu_canvi_ids = motiu_canvi_obj.search(cursor, uid, [('codi', '=', code), ('type', '=', 'alta')])

        if not motiu_canvi_ids:
            return False

        motiu_canvi_id = motiu_canvi_ids[0]

        polissa_motiu_canvi_obj = self.pool.get('giscedata.polissa.motiu.canvi')
        polissa_motiu_canvis_ids = polissa_motiu_canvi_obj.search(cursor, uid, [
            ('polissa_id', '=', result['polissa_id']),
            ('tipus_motiu', '=', 'alta')])

        if polissa_motiu_canvis_ids:
            return False

        vals = {
            'tipus_motiu': 'alta',
            'motiu_id': motiu_canvi_id,
            'polissa_id': result['polissa_id'],
            'observacions': signup.get('reason', {}).get('comments', False)
        }

        return polissa_motiu_canvi_obj.create(cursor, uid, vals)

    def _parse_electricity_product(self, cursor, uid, signup):
        product = signup['electricity']['product']
        supply_point = signup['electricity']['supply_point']
        access_contract = product['access_contract']

        vals = {
            'holder_change': False,
            'has_current_atr_contract': supply_point.get('has_supply', True),
            'is_atr_contract_change': product['access_contract'].get('changes', False),
            'no_supply_reason': supply_point.get('no_supply_reason', False),
            'supply_point_code_source': supply_point.get('code_source', False)
        }

        tarifa_obj = self.pool.get('giscedata.polissa.tarifa')

        if 'current' in access_contract:
            if access_contract['current']:
                if 'power' in access_contract['current']:
                    vals['current_power'] = get_power_as_kilowatts(access_contract['current']['power'])
                if 'tariff_code' in access_contract['current']:
                    vals['current_tariff'] = tarifa_obj.search(
                        cursor, uid, [('codi_ocsum', '=', access_contract['current']['tariff_code'])])[0]
        if 'target' in access_contract:
            target_tariff_code = access_contract['target'].get('tariff_code',
                                                               access_contract.get('current', {}).get('tariff_code',
                                                                                                      '001'))
            vals.update({
                'target_power': get_power_as_kilowatts(access_contract['target']['power']),
                'target_tariff': tarifa_obj.search(
                    cursor, uid, [('codi_ocsum', '=', target_tariff_code)])[0]
            })

        return vals

    def _parse_gas_product(self, cursor, uid, signup):
        product = signup['gas']['product']
        supply_point = signup['gas']['supply_point']
        access_contract = product['access_contract']

        vals = {
            'holder_change': False,
            'has_current_atr_contract': supply_point.get('has_supply', True),
            'is_atr_contract_change': product['access_contract'].get('changes', False),
            'no_supply_reason': supply_point.get('no_supply_reason', False),
            'supply_point_code_source': supply_point.get('code_source', False)
        }

        tarifa_obj = self.pool.get('giscegas.polissa.tarifa')

        if 'current' in access_contract:
            if access_contract['current']:
                # if 'power' in access_contract['current']:
                #     vals['current_power'] = get_power_as_kilowatts(access_contract['current']['power'])
                if 'tariff_code' in access_contract['current']:
                    vals['current_tariff'] = tarifa_obj.search(
                        cursor, uid, [('codi_ocsum', '=', access_contract['current']['tariff_code'])])[0]
        if 'target' in access_contract:
            target_tariff_code = access_contract['target'].get('tariff_code',
                                                               access_contract.get('current', {}).get('tariff_code',
                                                                                                      '001'))
            vals.update({
                # 'target_power': get_power_as_kilowatts(access_contract['target']['power']),
                'target_tariff': tarifa_obj.search(
                    cursor, uid, [('codi_ocsum', '=', target_tariff_code)])[0]
            })

        return vals

    def _parse_electricity_signup(self, cursor, uid, signup):
        vals = {}
        vals.update(self._parse_electricity_product(cursor, uid, signup))
        return vals

    def _parse_gas_signup(self, cursor, uid, signup):
        vals = {}
        vals.update(self._parse_gas_product(cursor, uid, signup))
        return vals

    def _parse_via(self, cursor, uid, signup):
        if 'via' not in signup:
            return {}

        via = signup['via']
        lead = via.get('lead', {})
        invoice_study = via.get('invoice_study', {})
        invoice_study_recommendation = invoice_study.get('electricity', {}).get('recommendation', {})
        personal_study = via.get('personal_study', {})
        personal_study_recommendation = personal_study.get('electricity', {}).get('recommendation', {})

        vals = {
            'via_channel': via.get('channel', False),
            'via_lead_id': lead.get('id', False),
            'via_lead_type': lead.get('type', False),
            'via_invoice_study_id': invoice_study.get('id', False),
            'via_invoice_study_date': convert_date(invoice_study.get('date', False)),
            'via_invoice_study_electricity_recommendation_power': invoice_study_recommendation.get('power', {}),
            'via_invoice_study_electricity_recommendation_tariff_code':
                invoice_study_recommendation.get('tariff_code', False),
            'via_personal_study_id': personal_study.get('id', False),
            'via_personal_study_date': convert_date(personal_study.get('date', False)),
            'via_personal_study_electricity_recommendation_power':
                personal_study_recommendation.get('power', False),
            'via_personal_study_electricity_recommendation_tariff_code':
                personal_study_recommendation.get('tariff_code', False)
        }

        return vals

    def create_signup(self, cursor, uid, signup_serialized, context=None):
        """Create a new signup parsing the serialized json"""

        if isinstance(signup_serialized, basestring):
            signup_json = json.loads(signup_serialized)
        else:
            signup_json = signup_serialized

        if signup_json.get('deleted', False):
            raise osv.except_osv(_('ValidateError'), _('Cannot commit a deleted signup'))

        signup_obj = self.pool.get('lucera.signup')
        signup_vals = {
            'external_id': signup_json['id'],
            'original_signup_serialized': json.dumps(signup_json, indent=4, ensure_ascii=False).encode('utf8'),
            'date': signup_json['context']['completed']['date']
        }

        # Parse Via info
        signup_vals.update(self._parse_via(cursor, uid, signup_json))

        # Create or write signup
        signup_ids = signup_obj.search(cursor, uid, [('external_id', '=', signup_vals['external_id'])])
        if signup_ids:
            signup_obj.write(cursor, uid, signup_ids[0], signup_vals)
            result = {
                'signup_id': signup_ids[0]
            }
        else:
            result = {
                'signup_id': signup_obj.create(cursor, uid, signup_vals)
            }

        signup = signup_obj.browse(cursor, uid, result['signup_id'])

        # Update attributions
        self._update_signup_attributions(cursor, uid, signup)

        # Electricty info
        if 'electricity' in signup_json:
            signup_electricity_obj = self.pool.get('lucera.signup.electricity')
            signup_electricity_vals = {
                'active': True,
                'signup_id': result['signup_id']}
            signup_electricity_vals.update(
                self._parse_electricity_signup(cursor, uid, signup_json))

            if not signup.electricity_signup_id:
                # Create signup electricity
                result['electricity_signup_id'] = signup_electricity_obj.create(cursor, uid, signup_electricity_vals)
                # Update signup with electricity signup id
                signup_obj.write(cursor, uid, result['signup_id'],
                                 {'electricity_signup_id': result['electricity_signup_id']})
            else:
                result['electricity_signup_id'] = signup.electricity_signup_id.id
                # Override existing signup electricity data
                del (signup_electricity_vals['signup_id'])
                signup_electricity_obj.write(cursor, uid, result['electricity_signup_id'], signup_electricity_vals)

        if 'gas' in signup_json:
            signup_gas_obj = self.pool.get('lucera.signup.gas')
            signup_gas_vals = {
                'active': True,
                'signup_id': result['signup_id']}
            signup_gas_vals.update(
                self._parse_gas_signup(cursor, uid, signup_json))

            if not signup.gas_signup_id:
                # Create signup gas
                result['gas_signup_id'] = signup_gas_obj.create(cursor, uid, signup_gas_vals)
                # Update signup with gas signup id
                signup_obj.write(cursor, uid, result['signup_id'],
                                 {'gas_signup_id': result['gas_signup_id']})
            else:
                result['gas_signup_id'] = signup.gas_signup_id.id
                # Override existing signup gas data
                del (signup_gas_vals['signup_id'])
                signup_gas_obj.write(cursor, uid, result['gas_signup_id'], signup_gas_vals)

        return result

    def create_entities(self, cursor, uid, signup_serialized, result, context=None):
        if not context:
            context = {}
        # Load signup_json from json
        if isinstance(signup_serialized, basestring):
            signup_json = json.loads(signup_serialized)
        else:
            signup_json = signup_serialized.copy()

        # Main entities
        user = signup_json['user']
        if 'electricity' in signup_json:
            electricity_contract = signup_json['electricity']
            electricity_supply_point = electricity_contract['supply_point']
            if electricity_supply_point.get('sips_exists', False):
                electricity_sips_data = signup_json.get('sips', {}).get('electricity', {})
            else:
                electricity_sips_data = {}
        else:
            electricity_contract = {}
            electricity_supply_point = {}
            electricity_sips_data = {}

        if 'gas' in signup_json:
            gas_contract = signup_json['gas']
            gas_supply_point = gas_contract['supply_point']
            if gas_supply_point.get('sips_exists', False):
                gas_sips_data = signup_json.get('sips', {}).get('gas', {})
            else:
                gas_sips_data = {}
        else:
            gas_contract = {}
            gas_supply_point = {}
            gas_sips_data = {}

        fiscal_customer = signup_json['fiscal_customer']
        bank_account = signup_json['payment_method']['bank_account']
        bank_account_owner = bank_account.get('owner', {})
        promotion = signup_json.get('promotion', {})
        affiliate = promotion.get('affiliate', {})
        analytics_user_id = signup_json.get('via', {}).get('analytics_user_id', False)

        # Intrinsic validations
        if fiscal_customer['rel'] != 'user' and not fiscal_customer['user_declares_representative_permission']:
            raise osv.except_osv(_('ValidateError'),
                                 _('User must have representative permission from fiscal customer!'))

        if bank_account_owner and (bank_account_owner['rel'] not in ('user', 'fiscal_customer', 'existing')
                                   and not bank_account_owner['user_declares_owner_permission']):
            raise osv.except_osv(_('ValidateError'), _('User must have bank account owner permission!'))

        # Conditional rels
        supply_point_location = gas_supply_point.get('location', electricity_supply_point.get('location', {}))
        if fiscal_customer['location']['rel'] == 'supply_point':
            fiscal_customer_location = supply_point_location
        else:
            fiscal_customer_location = fiscal_customer.get('location', {})

        bank_account_owner_location = False
        if bank_account_owner:
            if bank_account_owner['location']['rel'] == 'supply_point':
                bank_account_owner_location = supply_point_location
            elif bank_account_owner['location']['rel'] == 'fiscal_customer':
                bank_account_owner_location = fiscal_customer_location
            else:
                bank_account_owner_location = bank_account_owner.get('location', {})

        if fiscal_customer['rel'] == 'user':
            user_location = fiscal_customer_location
        elif bank_account_owner.get('rel') == 'user':
            user_location = bank_account_owner_location
        else:
            user_location = False

        user_id, user_address_id = self._create_or_get_user_and_address(cursor, uid, user, user_location,
                                                                        analytics_user_id, context)


        result.update({
            'external_id': signup_json['id'],
            'user_id': user_id,
            'user_address_id': user_address_id,
            'cups_id': self._create_electricity_cups(cursor, uid, electricity_supply_point, electricity_sips_data, context),
            'gas_cups_id': self._create_gas_cups(cursor, uid, gas_supply_point, gas_sips_data, context)
        })

        # Create or get fiscal_customer
        if fiscal_customer['rel'] == 'user':
            result['fiscal_customer_id'] = result['user_id']
            result['fiscal_customer_address_id'] = result['user_address_id']
        elif fiscal_customer['rel'] == 'existing':
            result['fiscal_customer_id'], result['fiscal_customer_address_id'] = \
                self._get_partner_and_address(cursor, uid, fiscal_customer['id'])
        else:  # None
            if fiscal_customer_location.get('rel', False) == 'existing':
                existing_partner_address_id = fiscal_customer_location.get('id', False)
                if existing_partner_address_id != str(user_address_id):
                    raise osv.except_osv(_('ValidateError'),
                                         _('Location rel existing for fiscal customer but id from other user!'))
            result['fiscal_customer_id'], result['fiscal_customer_address_id'] = \
                self._create_partner_and_address(cursor, uid, fiscal_customer, fiscal_customer_location)

        # For now, the electricity contract holder is always the customer_id
        result['atr_holder_id'] = result['fiscal_customer_id']
        result['atr_holder_address_id'] = result['fiscal_customer_address_id']

        # Create or get the bank owner
        if bank_account_owner:
            if bank_account_owner['rel'] == 'user':
                result['bank_account_owner_id'] = result['user_id']
                result['bank_account_owner_address_id'] = result['user_address_id']
            elif bank_account_owner['rel'] == 'fiscal_customer':
                result['bank_account_owner_id'] = result['fiscal_customer_id']
                result['bank_account_owner_address_id'] = result['fiscal_customer_address_id']
            elif bank_account_owner['rel'] == 'existing':
                result['bank_account_owner_id'], result['bank_account_owner_address_id'] = \
                    self._get_partner_and_address(cursor, uid, bank_account_owner['id'])
            elif bank_account_owner['rel'] == 'none':
                if fiscal_customer_location.get('rel', False) == 'existing':
                    existing_partner_address_id = fiscal_customer_location.get('id', False)
                    if existing_partner_address_id != str(user_address_id) and \
                            existing_partner_address_id != str(result['fiscal_customer_address_id']):
                        raise osv.except_osv(_('ValidateError'),
                                             _('Location rel existing for bank account owner but id from other user!'))
                result['bank_account_owner_id'], result['bank_account_owner_address_id'] = \
                    self._create_partner_and_address(cursor, uid, bank_account_owner, bank_account_owner_location)
            else:
                raise NotImplementedError("Rel for bank account owner not implemented: {}"
                                          .format(bank_account_owner['rel']))
        else:
            result['bank_account_owner_id'] = False  #

        # Create or get fiscal customer bank account
        result['bank_account_id'] = self._create_partner_bank_account(cursor, uid, result,
                                                                      signup_json['payment_method']['bank_account'])

        # Creates the electricity polissa
        result['polissa_id'] = self._create_electricity_polissa(cursor, uid, signup_json, result, context)

        # Creates the gas polissa
        result['gas_polissa_id'] = self._create_gas_polissa(cursor, uid, signup_json, result, context)

        # Creates the motius_canvi
        result['polissa_motiu_canvi_id'] = self._create_motius_canvi(cursor, uid, signup_json, result, context)

        # Creates the mandate
        if result['bank_account_id']:
            if result['polissa_id']:
                result['payment_mandate_id'] = self._create_electricity_payment_mandate(cursor, uid,
                                                                                        result['polissa_id'])
            if result['gas_polissa_id']:
                result['gas_payment_mandate_id'] = self._create_gas_payment_mandate(cursor, uid,
                                                                                    result['gas_polissa_id'])

        # Updates the signup
        self._update_signup(cursor, uid, result)

        # Return result
        return result

    def _update_signup_electricity(self, cursor, uid, result):
        if not result['polissa_id']:
            return
        signup_electricity_obj = self.pool.get('lucera.signup.electricity')
        vals = {'polissa_id': result['polissa_id']}

        # Computed checks
        polissa_obj = self.pool.get('giscedata.polissa')
        related_polissa_ids = polissa_obj.find_related_contracts_with_debt(cursor, uid,
                                                                           result['polissa_id'], context=None)
        if related_polissa_ids:
            vals['has_previous_debt'] = True

        signup_electricity_obj.write(cursor, uid, result['electricity_signup_id'], vals)

    def _update_signup_gas(self, cursor, uid, result):
        if not result['gas_polissa_id']:
            return
        signup_gas_obj = self.pool.get('lucera.signup.gas')
        vals = {'polissa_id': result['gas_polissa_id']}

        # Computed checks
        polissa_obj = self.pool.get('giscegas.polissa')
        related_polissa_ids = polissa_obj.find_related_contracts_with_debt(cursor, uid,
                                                                           result['gas_polissa_id'], context=None)
        if related_polissa_ids:
            vals['has_previous_debt'] = True

        signup_gas_obj.write(cursor, uid, result['gas_signup_id'], vals)

    def _update_signup(self, cursor, uid, result):
        self._update_signup_electricity(cursor, uid, result)
        self._update_signup_gas(cursor, uid, result)

    def commit(self, cursor, uid, signup_serialized, context=None):
        """Creates a signup_json, contract and users from json
        :returns:
            {
                electricity_signup_id: id,
                external_id: 2,
                user_id: 3,
                user_address_id: 4,
                cups_id: 5,
                fiscal_customer_id: 6,
                fiscal_customer_address_id: 7,
                atr_holder_id: 8,
                atr_holder_address_id: 9,
                bank_account_owner_id: 10,
                bank_account_owner_address_id: 11,
                bank_account_id: 12,
                polissa_id: 13,
                payment_mandate_id: 14

            }
        """

        result = self.create_signup(cursor, uid, signup_serialized, context)

        result = self.create_entities(cursor, uid, signup_serialized, result, context)

        # Send bus message
        self.pool.get('lucera.bus').send(cursor, uid,
                                         'lucera.signup.committed',
                                         {'signup_id': result['signup_id']}, 'erp')

        return result

    def update_signup_attributions(self, cursor, uid, signup_ids):
        if not isinstance(signup_ids, (list, tuple)):
            signup_ids = [signup_ids]
        signup_obj = self.pool.get('lucera.signup')
        updated = 0
        for signup in signup_obj.browse(cursor, uid, signup_ids):
            if self._update_signup_attributions(cursor, uid, signup):
                updated += 1

        return updated

    def _update_signup_attributions(self, cursor, uid, signup):
        signup_id = signup.id
        if not signup.original_signup_serialized:
            return False

        signup_json = json.loads(signup.original_signup_serialized)

        if not signup_json:
            return False

        signup_obj = self.pool.get('lucera.signup')
        attribution_obj = self.pool.get('lucera.signup.attribution')

        # Process route attributions or signature attributions
        route_json = signup_json.get('via', {}).get('attribution', {}).get('route', [])
        if not route_json and signup.signature_attribution_serialized:
            signature_attribution_json = json.loads(signup.signature_attribution_serialized)
            route_json = signature_attribution_json.get('route', [])

        if not route_json:
            return False

        # Create
        attributions = [attribution_obj.parse_from_json(r) for r in route_json]

        # If we a date in all attributions, we can sort by date
        if all([a.get('date') for a in attributions]):
            attributions = sorted(attributions, key=lambda k: k['date'])
        else:  # If we don't have date, they are supposed to be from newer to older so we reverse them
            attributions = list(reversed(attributions))

        # Update index and if it is first or last attribution
        for i, a in enumerate(attributions):
            a.update({
                'signup_id': signup_id,
                'index': i,
                'is_first': (i == 0),
                'is_last': (i == (len(attributions) - 1))
            })

        # Update attributions
        existing_attribution_ids = attribution_obj.search(cursor, uid, [('signup_id', '=', signup_id)])
        if existing_attribution_ids:
            attribution_obj.unlink(cursor, uid, existing_attribution_ids)

        for a in attributions:
            attribution_obj.create(cursor, uid, a)

        if attributions:
            return True
        else:
            return False


LuceraSignUpCommitter()
