# -*- coding: utf-8 -*-
from osv import osv, fields
from datetime import datetime
from dateutil import parser
from oorq.decorators import job
import lucera_signups.utils
import logging


class BasePolissa(osv.osv):

    def _has_cups(self, cursor, uid, ids, field_name, arg, context):
        res = {}
        for polissa in self.browse(cursor, uid, ids, context=context):
            res[polissa.id] = polissa.cups.id is not False \
                              and polissa.cups.active \
                              and polissa.cups.name is not False
        return res
