# -*- coding: utf-8 -*-
{
  "name": "Manage signups",
  "description": """Manage and create signups""",
  "version": "0-dev",
  "author": "Lucera",
  "category": "Lucera",
  "depends": ['base',
              'account',
              'account_payment',
              'base_bank_extended',
              'l10n_ES_remesas',
              'giscedata_polissa',
              'giscedata_polissa_comer',
              'giscedata_cups_comer',
              'giscemisc_cnae',
              'giscedata_switching',
              'giscedata_facturacio',
              'giscedata_facturacio_impagat',
              'giscedata_facturacio_comer_lucera',
              'giscegas_polissa',
              'giscegas_polissa_comer',
              'giscedata_polissa_motius_canvis',
              'giscedata_polissa_motius_canvis_lucera',
              'giscegas_cups',
              'giscegas_cups_comer',
              'giscegas_facturacio',
              'giscegas_facturacio_comer',
              'giscegas_atr',
              'giscegas_facturacio_atr',
              'lucera_common',
              'lucera_asnef',
              'lucera_polissa_simple_search',
              'lucera_partner_end_user',
              'lucera_bus',
              'lucera_extend_polissa_comer',
              'lucera_gas_extend_polissa_comer'
              ],
  "init_xml": [],
  "demo_xml":   ['data/lucera_signup_demo.xml'],
  "update_xml": ['views/lucera_signup_view.xml',
                 'views/lucera_signup_request_view.xml',
                 'views/lucera_signup_electricity_polissa_view.xml',
                 'views/lucera_signup_gas_polissa_view.xml',
                 # 'views/lucera_signup_request_workflow.xml',
                 'data/lucera_signup_data.xml',
                 'security/ir.model.access.csv'],
  "active": False,
  "installable": True
}
