# -*- coding: utf-8 -*-
from osv import osv, fields
from datetime import datetime
from utils import *
import json


class LuceraSignUpRequest(osv.osv):
    """Request to create a signup"""

    _name = 'lucera.signup.request'

    _SIGNUP_STATES = [('received', 'Recibida'),
                      ('committed', 'Sincronizada'),
                      ('failed', 'Fallada'),
                      ('canceled', 'Cancelada')]
    _columns = {
        # External data
        'date': fields.datetime('Fecha alta', required=True),
        'committed_date': fields.datetime('Fecha sincronización'),
        'external_id': fields.char('Identificador externo del alta', size=36),
        'signup_serialized': fields.text('Datos del alta serializados'),

        # State
        'state': fields.selection(_SIGNUP_STATES, 'Estado', required=True),

        'signup_id': fields.many2one('lucera.signup', 'Alta')
    }

    _defaults = {
        'state': lambda *a: 'received',
        'date': lambda *a: datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
    }

    _rec_name = 'external_id'

    _order = 'date DESC'

    # Workflow
    def cnd_commit(self, cursor, uid, ids):
        """We check that it can be changed to committed"""
        for signup in self.browse(cursor, uid, ids):
            if signup.state in ['received', 'failed']:
                return True
            else:
                return False
        return False

    def wkf_received(self, cursor, uid, ids):
        self.write(cursor, uid, ids, {'state': 'received'})
        return True

    def wkf_committed(self, cursor, uid, ids):
        # We don't need to check if it can be committed because that is defined in the
        # workflow and it is checked by cnd_commit
        self.write(cursor, uid, ids, {'state': 'committed', 'committed_date': datetime.now()})
        return True

    def wkf_failed(self, cursor, uid, ids):
        self.write(cursor, uid, ids, {'state': 'failed', 'committed_date': False})
        return True

    def wkf_canceled(self, cursor, uid, ids):
        self.write(cursor, uid, ids, {'state': 'canceled', 'committed_date': False})
        return True

    def commit(self, cursor, uid, ids, context=None):
        """ Parses the serialized signup and creates required signup, contracts, partners, cups, etc.."""
        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        res = {}
        committer_obj = self.pool.get('lucera.signup.committer')
        signup_requests = self.read(cursor, uid, ids, ['signup_serialized'], context=context)

        if not signup_requests:
            raise Exception('Signup requests not found')

        # Commit
        for signup_request in signup_requests:
            # Commit signup
            result = committer_obj.commit(cursor, uid, signup_request['signup_serialized'], context=context)
            if result:
                vals = {'state': 'committed',
                        'external_id': result['external_id'],
                        'committed_date': datetime.now(),
                        'signup_id': result['signup_id']}
                self.write(cursor, uid, signup_request['id'], vals)

            res[signup_request['id']] = result

        return res

    def cancel(self, cursor, uid, ids, context=None):
        """ Cancels the signup request"""
        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        res = {}

        # Cancel
        for signup_request in self.read(cursor, uid, ids, ['state'], context=context):
            # Commit signup
            if signup_request['state'] not in ['committed', 'canceled']:
                vals = { 'state': 'canceled'}
                self.write(cursor, uid, signup_request['id'], vals)
                res[signup_request['id']] = False
            else:
                res[signup_request['id']] = True

        return res


LuceraSignUpRequest()