# -*- coding: utf-8 -*-
from osv import osv, fields
import re

from tools.translate import _


class GiscedataPolissa(osv.osv):
    """Pòlissa per afegir el camp de soci.
    """
    _name = 'giscegas.polissa'
    _inherit = 'giscegas.polissa'

    def _free_search(self, cursor, uid, ids, field_name, arg, context):
        res = {}
        for polissa_id in ids:
            res[polissa_id] = ''
        return res

    # noinspection PyMethodMayBeStatic
    def _free_search_search(self, cursor, uid, obj, name, args, context):
        """Search in common fields"""
        keywords = args[0][2]

        if keywords:
            keywords = keywords.strip()

        # TODO: Smart search based on input type (ex: do not search phone if is not numeric)
        # TODO: Should we use SQL instead of this?
        search_params = []

        return ['|', '|', '|', '|', '|', '|', '|', '|', '|', '|',
                ('name', 'ilike', keywords),
                ('notification_name', 'ilike', keywords),
                ('notification_vat', 'ilike', keywords),
                ('notification_email', 'ilike', keywords),
                ('notification_phone', 'ilike', keywords),
                ('titular.name', 'ilike', keywords),
                ('titular.vat', 'ilike', keywords),
                ('pagador.name', 'ilike', keywords),
                ('pagador.vat', 'ilike', keywords),
                ('cups.name', 'ilike', keywords),
                ('cups_direccio', 'ilike', keywords)]

    def _vat_search(self, cursor, uid, ids, field_name, arg, context):
        res = {}
        for polissa_id in ids:
            res[polissa_id] = ''
        return res

    # noinspection PyMethodMayBeStatic
    def _vat_search_search(self, cursor, uid, obj, name, args, context):
        """Search in vat fields of holder, payer or notified people"""
        keywords = args[0][2]

        if keywords:
            keywords = keywords.strip()

        search_params = []

        return ['|', '|',
                ('notification_vat', 'ilike', keywords),
                ('titular.vat', 'ilike', keywords),
                ('pagador.vat', 'ilike', keywords)]

    _columns = {
        'notification_email': fields.related('direccio_notificacio', 'email',
                                             type='char', size=240,
                                             string='E-mail contacto', readonly=True),
        'notification_phone': fields.related('direccio_notificacio', 'phone',
                                             type='char', size=32,
                                             string='Teléfono contacto', readonly=True),
        'notification_name': fields.related('direccio_notificacio', 'partner_id', 'name',
                                            type='char', size=240,
                                            string='Persona contacto', readonly=True),
        'notification_vat': fields.related('direccio_notificacio', 'partner_id', 'vat',
                                            type='char', size=32,
                                            string='NIF/CIF contacto', readonly=True),
        'fiscal_address_phone': fields.related('direccio_pagament', 'phone',
                                             type='char', size=32,
                                             string='Teléfono dirección fiscal', readonly=True),
        'free_search': fields.function(_free_search,
                                       method=True,
                                       type='char',
                                       size=240,
                                       string='Buscar',
                                       fnct_search=_free_search_search),
        'vat_search': fields.function(_vat_search,
                                       method=True,
                                       type='char',
                                       size=32,
                                       string='DNI/CIF',
                                       fnct_search=_vat_search_search)

    }


GiscedataPolissa()
