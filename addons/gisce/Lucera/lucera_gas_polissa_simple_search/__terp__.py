# -*- coding: utf-8 -*-
{
  "name": "Lucera gas polissa simple search",
  "description": """Simplifies searching for gas polissas""",
  "version": "0-dev",
  "author": "Lucera",
  "category": "Lucera",
  "depends": ['base',
              'giscegas_facturacio',
              'giscegas_polissa_comer'],
  "init_xml": [],
  "demo_xml": [],
  "update_xml": ['lucera_extend_polissa_view.xml',
                 'lucera_polissa_comer_view_simple_search.xml'],
  "active": False,
  "installable": True
}
