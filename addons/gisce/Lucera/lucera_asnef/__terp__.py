# -*- coding: utf-8 -*-
{
  "name": "Lucera ASNEF",
  "description": """Queries and stores ASNEF risk information""",
  "version": "0-dev",
  "author": "Lucera",
  "category": "Lucera",
  "depends": [ 'lucera_common',
               'giscedata_polissa' ],
  "init_xml": [],
  "demo_xml": [],
  "update_xml": [
    'security/ir.model.access.csv',
    'lucera_asnef_request_wizard.xml',
    'lucera_asnef_view.xml'
  ],
  "active": False,
  "installable": True
}
