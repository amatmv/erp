Lucera ASNEF
============

This module can query the Equifax/ASNEF API to check the solvency index given
some personal data.

Calling the API
---------------

To use the API, simply call ``query_info`` with the appropriate parameters.

Example:

.. code:: python

  import asnef_client

  response = asnef_client.query_info('00000000T', '00000')

Configuration
-------------

To avoid storing sensitive data in the database, the configuration is made via
configuration file.

The relevant settings are:

* ``equifax_insights_url``: API url
* ``equifax_insights_orchestration_code``: Orchestration code
* ``equifax_insights_user``: User
* ``equifax_insights_password``: Password

The main url should be ``https://uat2.equifax.es/icflex/api`` for testing and
``https://www2.equifax.es/icflex/api`` for production.

The other values must be provided by Equifax.