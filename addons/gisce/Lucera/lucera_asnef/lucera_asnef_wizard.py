# -*- coding: utf-8 -*-
import json
from asnef_client import AsnefClient
from lucera_common import validate
from osv import fields, osv


class AsnefRequestWizard(osv.osv_memory):
    """
    Wizard para realizar peticiones a ASNEF
    """
    _name = 'wizard.asnef.request'

    def _get_partner_nif(self, cursor, uid, context=None):
        if context is None:
            context = {}

        partner_id = context.get('active_id')

        if partner_id:
            partner_obj = self.pool.get('res.partner')
            partner = partner_obj.browse(cursor, uid, partner_id)

            return partner.vat

    def _get_partner_postal_code(self, cursor, uid, context=None):
        if context is None:
            context = {}

        partner_id = context.get('active_id')

        if partner_id:
            partner_obj = self.pool.get('res.partner')
            address_ids = partner_obj.address_get(cursor, uid, [partner_id])

            address_id = address_ids.get('default')

            if address_id:
                partner_address_obj = self.pool.get('res.partner.address')
                partner_address = partner_address_obj.browse(cursor, uid, address_id)

                return partner_address.zip

    def do_request(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        wiz = self.browse(cursor, uid, ids[0], context)

        if not validate.is_valid_vat(wiz.nif):
            self.write(cursor, uid, ids, {'state': 'error', 'error_message': 'NIF incorrecto'}, context)
            return

        if not validate.is_valid_postal_code(wiz.postal_code):
            self.write(cursor, uid, ids, {'state': 'error', 'error_message': 'Código postal incorrecto'}, context)
            return

        try:
            partner_obj = self.pool.get('res.partner')
            partner_id = context.get('active_id')

            partner_requests = partner_obj.create_asnef_request(cursor, uid, [partner_id], context)

            request_id = partner_requests.get(partner_id)
            request_obj = self.pool.get('lucera.asnef.request')

            request = request_obj.browse(cursor, uid, request_id, context)

            if request.success:
                self.write(cursor, uid, ids, {'state': 'end'}, context)
            else:
                self.write(cursor, uid, ids, {'state': 'error', 'error_message': 'Hay un error en la respuesta'}, context)


        except Exception, e:
            self.write(cursor, uid, ids, {'state': 'error', 'error_message': e.message }, context)
            return

        self.write(cursor, uid, ids, { 'state': 'end' }, context)

    _columns = {
        'state': fields.selection([('init', 'Init'),
                                   ('error', 'Error'),
                                   ('end', 'End')], 'Estado'),
        'nif': fields.char('NIF', required=True, readonly=True, size=20),
        'postal_code': fields.char('Código postal', required=True, readonly=True, size=5),
        'error_message': fields.text('Error')
    }

    _defaults = {
        'state': lambda *a: 'init',
        'nif': _get_partner_nif,
        'postal_code': _get_partner_postal_code
    }


AsnefRequestWizard()