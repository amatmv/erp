# -*- coding: utf-8 -*-

import json
import requests
import tools
from lucera_common import normalize, validate


ERROR_CODES = {
    '000': 'Respuesta generada correctamente',
    '102': 'Identificador no informado',
    '103': 'Identificador incorrecto',
    '150': 'Tipo de solicitud incorrecta',
    '171': 'Campos de dirección no informados',
    '172': 'Código postal no informado',
    '174': 'Fecha de nacimiento no informado o incorrecto',
    '175': 'Menor de edad',
    '176': 'Error interno',
    '177': 'Error de servicio',
    '500': 'LOGON / LOGOFF ya activo',
    '501': 'Solicitud ejecutada sin iniciar sesión',
    '502': 'El código de la entidad no existe',
    '503': 'El usuario utilizado en la consulta no existe',
    '504': 'Datos no autorizados para la entidad',
    '801': 'Servicio no disponible. Problemas de comunicación',
    '802': 'Servicio no disponible. Problemas de aplicación',
    '803': 'El usuario de facturación no existe',
    '999': 'Error de seguridad'
}


class AsnefClient:

    def __init__(self, url = None, orchestration_code=None, user=None, password=None):
        """
        Initializes a new client. If parameters are not passed, credentials will load from configuration file

        :param url: Url of the API
        :type url: str
        :param orchestration_code: Orchestration code (provided by Equifax)
        :type orchestration_code: str
        :param user:  Username (provided by Equifax)
        :type user: str
        :param password: Password (provided by Equifax)
        :type password: str
        """

        self._url = url if url else tools.config.get('equifax_insights_url')
        self._orchestration_code = orchestration_code if orchestration_code else tools.config.get('equifax_insights_orchestration_code')
        self._user = user if user else tools.config.get('equifax_insights_user')
        self._password = password if password else tools.config.get('equifax_insights_password')


    def query(self, vat, postal_code, date_of_birth=None):
        """
        Query for ASNEF rating information

        :param vat: Personal ID code of the person
        :type vat:str
        :param postal_code: Postal code where the person lives
        :type postal_code:str
        :param date_of_birth: Date of birth of the person
        :type date_of_birth:str
        :return:A JSON object with the API response
        :rtype: str
        """

        if not self._url:
            raise ValueError('Equifax url is not initialized')

        vat = normalize.normalize_vat(vat)

        if not validate.is_valid_vat(vat):
            raise ValueError('Invalid vat value {0}'.format(vat))

        if validate.is_valid_cif(vat):
            raise ValueError('The vat {0} is a CIF and cannot be checked'.format(vat))

        postal_code = normalize.normalize_postal_code(postal_code)

        if not validate.is_valid_postal_code(postal_code):
            raise ValueError('Invalid postal code value {0}'.format(postal_code))

        vat = vat.lstrip('ES')

        request = \
            {"applicants": {
                "primaryConsumer": {
                    "personalInformation": {
                        "idCountryCode": "724",
                        "idType": "ID",
                        "idCode": vat
                    }
                }
            }
            }

        if postal_code:
            request['applicants']['primaryConsumer']['personalInformation']. \
                setdefault('addresses', [{'postalCode': postal_code}])

        if date_of_birth:
            request['applicants']['primaryConsumer']['personalInformation']. \
                setdefault('dateOfBirth', date_of_birth)

        headers = {
            'Content-Type': 'application/json; charset=UTF-8',
            'dptOrchestrationCode': self._orchestration_code}
        auth = (self._user, self._password)

        r = requests.post(self._url, data=json.dumps(request), auth=auth, headers=headers)

        if r.status_code == 200:
            return r.content
        else:
            raise ValueError("Response code is %s" % r.status_code, r.content)

