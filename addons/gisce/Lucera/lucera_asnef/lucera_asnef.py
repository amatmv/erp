# -*- coding: utf-8 -*-
import json
import logging
from datetime import datetime
from asnef_client import AsnefClient
from lucera_common import normalize, validate
from osv import fields, osv

logger = logging.getLogger('lucera.asnef')

ASNEF_RATINGS = [
    ('1', 'Muy alto (D)'),
    ('2', 'Alto (C)'),
    ('3', 'Medio alto (B)'),
    ('4', 'Medio (A)'),
    ('5', 'Medio bajo (AA)'),
    ('6', 'Bajo (AAA)')]

BAD_RATINGS = ['1']

class LuceraAsnefRequest(osv.osv):

    _name = 'lucera.asnef.request'

    def create_for_vat(self, cursor, uid, vat, postal_code, context = None):
        vat = normalize.normalize_vat(vat)

        if not validate.is_valid_vat(vat):
            raise ValueError('Invalid vat value {0}'.format(vat))

        client = AsnefClient()
        response = client.query(vat, postal_code)
        parsed = json.loads(response)

        transaction_state = parsed.get('transactionState')
        risk = parsed \
            .get('applicants', {}) \
            .get('primaryConsumer', {}) \
            .get('dataSourceResponses', {}) \
            .get('EIPG', {}) \
            .get('RISK', {}) \

        return_code = risk.get('returnCode')
        success = transaction_state == "COMPLETED" and return_code == '000'

        request = {
            'vat': vat,
            'success': success,
            'present': risk.get('present'),
            'rating': risk.get('rating'),
            'response': json.dumps(parsed, indent=2)
        }

        return self.create(cursor, uid, request, context)


    _columns = {
        'vat': fields.char('NIF', required=True, size=12),
        # 'partner_id': fields.many2one('res.partner', 'Partner', required=True, select=True),
        'date': fields.datetime('Fecha de consulta', required=True),
        'success': fields.boolean('Éxito', required=True),
        'present': fields.selection([('00', 'Hay presencia'),
                                     ('01', 'No hay')], 'Presencia en el registro'),
        'rating': fields.selection(ASNEF_RATINGS, 'Riesgo'),
        'response': fields.text('Respuesta')
    }

    _order = 'date desc'

    _defaults = {
        'date': lambda *a: datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
        'success': False
    }

LuceraAsnefRequest()

class ResPartner(osv.osv):
    _name = 'res.partner'
    _inherit = 'res.partner'


    def _get_requests_by_vat(self, cursor, uid, ids, field_name, arg, context):
        if not context:
            context = {}

        res = {}

        request_obj = self.pool.get('lucera.asnef.request')

        for partner in self.browse(cursor, uid, ids, context=None):
            res[partner.id] = []

            requests = request_obj.search(cursor, uid,
                            [('vat', '=', partner.vat)], context=context)

            res[partner.id] = [l for l in requests]

        return res

    def create_asnef_request(self, cursor, uid, ids, context=None):
        request_obj = self.pool.get('lucera.asnef.request')

        res = {}
        for partner in self.browse(cursor, uid, ids, context):
            res[partner.id] = request_obj.create_for_vat(cursor, uid, partner.vat, partner.address[0].zip)

        return res

    def _asnef_rating(self, cursor, uid, ids, field_name, arg, context):
        res = {}

        for partner in self.browse(cursor, uid, ids, context):
            if partner.asnef_requests:
                res[partner.id] = partner.asnef_requests[0].rating
            else:
                res[partner.id] = None

        return res

    def _has_bad_solvency_rating(self, cursor, uid, ids, field_name, arg, context):
        ratings = self._asnef_rating(cursor, uid, ids, field_name, arg, context)

        res = {}

        for partner_id in ratings:
            res[partner_id] = ratings[partner_id] in BAD_RATINGS

        return res

    _columns = {
        'asnef_requests': fields.function(_get_requests_by_vat,
                                          method=True,
                                          type='one2many',
                                          obj='lucera.asnef.request',
                                          string='Consultas de solvencia'),
        'asnef_rating': fields.function(_asnef_rating,
                                        method=True,
                                        type='selection',
                                        selection=ASNEF_RATINGS,
                                        string='Rating ASNEF',
                                        readonly=True),
        'has_bad_solvency_rating': fields.function(_has_bad_solvency_rating,
                                                   method=True,
                                                   type='boolean',
                                                   string='Rating de solvencia malo',
                                                   readonly=True)
    }


ResPartner()
