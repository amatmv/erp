# -*- coding: utf-8 -*-
{
    "name": "Invoice debt partner id",
    "description": """Allows to assign a debt recovery partner to every invoice""",
    "version": "0-dev",
    "author": "Lucera",
    "category": "Lucera",
    "depends":[
        "base",
        "giscedata_polissa",
        "giscedata_facturacio_comer",
        "giscedata_polissa_comer"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_facturacio_comer_view.xml",
        "giscedata_facturacio_comer_data.xml"
    ],
    "active": False,
    "installable": True
}
