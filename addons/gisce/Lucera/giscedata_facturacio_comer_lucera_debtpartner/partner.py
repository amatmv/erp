# coding=utf-8
from osv import osv, fields
from operator import itemgetter


class ResPartner(osv.osv):
    _name = 'res.partner'
    _inherit = 'res.partner'

    def _fnct_total_debt(self, cursor, uid, ids, field_names, arg, context):
        res = dict.fromkeys(ids, 0)
        # Calculem per vat, per tant hem de recalcular per tots els partner_ids
        # amb els vats dels ids que estem consultant originalment
        vats = [info['vat'] for info in self.read(cursor, uid, ids, ['vat'], context=context) if info['vat']]
        other_part_ids = self.search(cursor, uid, [('vat', 'in', vats), ('id', 'not in', ids)], context=context)
        if not other_part_ids:
            other_part_ids = []
        ids = list(set(ids+other_part_ids))

        partners = self.read(cursor, uid, ids, ['property_account_debtor'])
        accounts = set(
            p['property_account_debtor'][0] for p in partners
            if p['property_account_debtor']
        )
        query = self.pool.get('account.move.line')._query_get(
            cursor, uid, context=context
        )

        # Primer els que no tenen vat
        cursor.execute("""\
            SELECT l.partner_id, SUM(l.debit-l.credit)
            FROM account_move_line l
            INNER JOIN account_account a ON (l.account_id=a.id)
            LEFT JOIN res_partner part ON (l.partner_id=part.id)
            WHERE
              a.id IN %s
              AND part.vat is NULL
              AND l.partner_id IN %s
              AND l.reconcile_id IS NULL
              AND """ + query + """
              GROUP BY l.partner_id
            """, (tuple(accounts), tuple(ids))
        )
        sense_vat = cursor.fetchall()
        for pid, value in sense_vat:
            if pid in res:
                res[pid] = value

        # Despres els que tenen vat, que es tenen en compte els deutes de tots
        # els partners que comparteixen vat
        if len(vats):
            cursor.execute("""\
                SELECT 
                    (select array(select paux.id from res_partner paux where paux.vat=part.vat)) as same_vat_ids,
                    part.vat, 
                    SUM(l.debit-l.credit)
                FROM account_move_line l
                INNER JOIN account_account a ON (l.account_id=a.id)
                LEFT JOIN res_partner part ON (l.partner_id=part.id)
                WHERE
                  a.id IN %s
                  AND part.vat is NOT NULL
                  AND l.partner_id IN %s
                  AND l.reconcile_id IS NULL
                  AND """ + query + """
                  GROUP BY part.vat
                """, (tuple(accounts), tuple(ids))
            )
            amb_vat = cursor.fetchall()
            for pids, vat, value in amb_vat:
                for pid in pids:
                    if pid in res:
                        res[pid] = value
        return res

    def _fnct_has_debt(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        res = dict.fromkeys(ids, 0)
        # Calculem per vat, per tant hem de recalcular per tots els partner_ids
        # amb els vats dels ids que estem consultant originalment
        vats = [info['vat'] for info in self.read(cursor, uid, ids, ['vat'], context=context) if info['vat']]
        other_part_ids = self.search(cursor, uid, [('vat', 'in', vats), ('id', 'not in', ids)], context=context)
        if not other_part_ids:
            other_part_ids = []
        ids = list(set(ids+other_part_ids))

        query = self.pool.get('account.move.line')._query_get(
            cursor, uid, context=context
        )
        partners = self.read(cursor, uid, ids, ['property_account_debtor'])
        accounts = set(
            p['property_account_debtor'][0] for p in partners
            if p['property_account_debtor']
        )
        # Primer els que no tenen vat
        cursor.execute("""\
            SELECT l.partner_id, SUM(l.debit-l.credit)
            FROM account_move_line l
            INNER JOIN account_account a ON (l.account_id=a.id)
            LEFT JOIN res_partner part ON (l.partner_id=part.id)
            WHERE
              a.id IN %s
              AND part.vat is NULL
              AND l.partner_id IN %s
              AND l.reconcile_id IS NULL
              AND """ + query + """
              GROUP BY l.partner_id
              HAVING SUM(l.debit-l.credit) > 0
            """, (tuple(accounts), tuple(ids))
        )
        sense_vat = cursor.fetchall()
        for pid, value in sense_vat:
            if pid in res:
                res[pid] = 1

        # Despres els que tenen vat, que es tenen en compte els deutes de tots
        # els partners que comparteixen vat
        if len(vats):
            cursor.execute("""\
                SELECT 
                    (select array(select paux.id from res_partner paux where paux.vat=part.vat)) as same_vat_ids,
                    part.vat, 
                    SUM(l.debit-l.credit)
                FROM account_move_line l
                INNER JOIN account_account a ON (l.account_id=a.id)
                LEFT JOIN res_partner part ON (l.partner_id=part.id)
                WHERE
                  a.id IN %s
                  AND part.vat is NOT NULL
                  AND l.partner_id IN %s
                  AND l.reconcile_id IS NULL
                  AND """ + query + """
                  GROUP BY part.vat
                  HAVING SUM(l.debit-l.credit) > 0
                """, (tuple(accounts), tuple(ids))
            )
            amb_vat = cursor.fetchall()
            for pids, vat, value in amb_vat:
                for pid in pids:
                    if pid in res:
                        res[pid] = 1
        return res

    def _fnct_has_debt_search(self, cursor, uid, obj, name, args, context=None):
        if not context:
            context = {}
        if not args:
            return [('id', '=', 0)]
        has_debt = args[0][2]
        query = self.pool.get('account.move.line')._query_get(
            cursor, uid, context=context
        )
        ids = self.search(cursor, uid, [], context=context)
        partners = self.read(cursor, uid, ids, ['property_account_debtor'])
        accounts = set(
            p['property_account_debtor'][0] for p in partners
            if p['property_account_debtor']
        )
        # Primer els que no tenen vat
        cursor.execute("""\
            SELECT l.partner_id, SUM(l.debit-l.credit)
            FROM account_move_line l
            INNER JOIN account_account a ON (l.account_id=a.id)
            LEFT JOIN res_partner part ON (l.partner_id=part.id)
            WHERE
              a.id IN %s
              AND part.vat is NULL
              AND l.reconcile_id IS NULL
              AND """ + query + """
              GROUP BY l.partner_id
              HAVING {} SUM(l.debit-l.credit) > 0
            """.format(not has_debt and 'NOT' or ''), (tuple(accounts),)
        )
        partner_ids = [p[0] for p in cursor.fetchall()]
        # Despres els que tenen vat, que es tenen en compte els deutes de tots
        # els partners que comparteixen vat
        cursor.execute("""\
            SELECT 
                (select array(select paux.id from res_partner paux where paux.vat=part.vat)) as same_vat_ids,
                part.vat, 
                SUM(l.debit-l.credit)
            FROM account_move_line l
            INNER JOIN account_account a ON (l.account_id=a.id)
            LEFT JOIN res_partner part ON (l.partner_id=part.id)
            WHERE
              a.id IN %s
              AND part.vat is NOT NULL
              AND l.reconcile_id IS NULL
              AND """ + query + """
              GROUP BY part.vat
              HAVING {} SUM(l.debit-l.credit) > 0
            """.format(not has_debt and 'NOT' or ''), (tuple(accounts),)
        )
        for pids, vat, value in cursor.fetchall():
            partner_ids += pids

        return [('id', 'in', partner_ids)]

    def _credit_debit_get(self, cr, uid, ids, field_names, arg, context):
        res = {}
        for id in ids:
            res[id] = {}.fromkeys(field_names, 0)
        # Calculem per vat, per tant hem de recalcular per tots els partner_ids
        # amb els vats dels ids que estem consultant originalment
        vats = [info['vat'] for info in self.read(cr, uid, ids, ['vat'], context=context) if info['vat']]
        other_part_ids = self.search(cr, uid, [('vat', 'in', vats), ('id', 'not in', ids)], context=context)
        if not other_part_ids:
            other_part_ids = []
        ids = list(set(ids+other_part_ids))
        query = self.pool.get('account.move.line')._query_get(cr, uid, context=context)
        maps = {'receivable': 'credit', 'payable': 'debit'}

        # Primer els que no tenen vat
        cr.execute("""
            SELECT l.partner_id, a.type, SUM(l.debit-l.credit)
            FROM account_move_line l
            LEFT JOIN account_account a ON (l.account_id=a.id)
            LEFT JOIN res_partner part ON (l.partner_id=part.id)
            WHERE a.type IN ('receivable','payable')
            AND part.vat is NULL
            AND l.partner_id in %s
            AND l.reconcile_id IS NULL
            AND """ + query + """
            GROUP BY l.partner_id, a.type
            """, (tuple(ids),)
        )
        for pid, type, val in cr.fetchall():
            if pid not in res:
                continue
            if val is None:val=0
            res[pid][maps[type]] = (type=='receivable') and val or -val

        # Despres els que tenen vat, que es tenen en compte els deutes de tots
        # els partners que comparteixen vat
        if len(vats):
            cr.execute("""
                SELECT
                    (select array(select paux.id from res_partner paux where paux.vat=part.vat)) as same_vat_ids,
                    part.vat, 
                    a.type,
                    SUM(l.debit-l.credit)
                FROM account_move_line l
                LEFT JOIN account_account a ON (l.account_id=a.id)
                LEFT JOIN res_partner part ON (l.partner_id=part.id)
                WHERE a.type IN ('receivable','payable')
                AND part.vat is NOT NULL
                AND l.partner_id in %s
                AND l.reconcile_id IS NULL
                AND """ + query + """
                GROUP BY part.vat, a.type
                """, (tuple(ids),)
            )
            amb_vat = cr.fetchall()
            for pids, vat, type, val in amb_vat:
                for pid in pids:
                    if pid in res:
                        if val is None:val=0
                        res[pid][maps[type]] = (type=='receivable') and val or -val
        return res

    def _asset_difference_search(self, cr, uid, obj, name, type, args,
                                 context=None):
        if not len(args):
            return []
        having_values = tuple(map(itemgetter(2), args))
        where = ' AND '.join(
            map(lambda x: '(SUM(debit-credit) %(operator)s %%s)' % {
                                'operator':x[1]},
                args))
        query = self.pool.get('account.move.line')._query_get(cr, uid,
                                                              context=context)
        # Primer els que no tenen vat
        cr.execute(
            ('SELECT partner_id '
             'FROM account_move_line l '
             'LEFT JOIN res_partner part ON (l.partner_id=part.id) '
             'WHERE account_id IN ('
             '  SELECT id '
             '  FROM account_account '
             '  WHERE type=%s AND active'
             ') '
             'AND reconcile_id IS NULL '
             'AND ' + query +
             ' AND partner_id IS NOT NULL '
             'AND part.vat is NULL '
             'GROUP BY partner_id HAVING ' + where),
            (type,) + having_values
        )
        partner_ids = [p[0] for p in cr.fetchall()]

        # Despres els que tenen vat, que es tenen en compte els deutes de tots
        # els partners que comparteixen vat
        cr.execute(
            ('SELECT '
             '(select array(select paux.id from res_partner paux where paux.vat=part.vat)) as same_vat_ids, '
             'part.vat '
             'FROM account_move_line l '
             'LEFT JOIN res_partner part ON (l.partner_id=part.id) '
             'WHERE account_id IN ('
             '  SELECT id '
             '  FROM account_account '
             '  WHERE type=%s AND active'
             ') '
             'AND reconcile_id IS NULL '
             'AND '+query+
             ' AND partner_id IS NOT NULL '
             'AND part.vat IS NOT NULL '
             'GROUP BY part.vat HAVING '+where),
            (type,) + having_values
        )
        for pids, vat in cr.fetchall():
            partner_ids += pids
        if not len(partner_ids):
            return [('id','=','0')]
        return [('id','in',  partner_ids)]

    def _credit_search(self, cr, uid, obj, name, args, context):
        return self._asset_difference_search(
            cr, uid, obj, name, 'receivable', args, context=context)

    def _debit_search(self, cr, uid, obj, name, args, context):
        return self._asset_difference_search(
            cr, uid, obj, name, 'payable', args, context=context)

    _columns = {
        'total_debt': fields.function(
            _fnct_total_debt,
            method=True,
            string='Total Debt',
            help="Total amount this customer debts you."
        ),
        'has_debt': fields.function(
            _fnct_has_debt,
            type='boolean',
            fnct_search=_fnct_has_debt_search,
            method=True,
            string='Has debt',
        ),
        'credit': fields.function(_credit_debit_get,
                                  fnct_search=_credit_search, method=True,
                                  string='Total Receivable', multi='dc',
                                  help="Total amount this customer owes you."),
        'debit': fields.function(_credit_debit_get, fnct_search=_debit_search,
                                 method=True, string='Total Payable',
                                 multi='dc',
                                 help="Total amount you have to pay to this supplier."),
    }


ResPartner()
