# -*- coding: utf-8 -*-
{
    "name": "Póliza Panticosa",
    "description": """Módulo personalizado para las pólizas de Panticosa""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Panticosa",
    "depends": [
        "giscedata_polissa_distri",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "giscedata_polissa_view.xml",
    ],
    "active": False,
    "installable": True
}
