# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscedataPolissa(osv.osv):

    _name = "giscedata.polissa"
    _inherit = "giscedata.polissa"

    _columns = {
        'archivo': fields.char('Archivo', size=8)
    }

    def wkf_activa(self, cursor, uid, ids):
        """Asigna un número de archivo cuando una vez activada la póliza.
        Si ya tiene uno asignado, entonces no le asigna ninguno."""
        res = super(GiscedataPolissa, self).wkf_activa(cursor, uid, ids)
        if res:
            for polissa_id in ids:
                archivo = self.read(cursor, uid, polissa_id, ['archivo'])['archivo']
                if not archivo:
                    archivo = self.get_archivo_disponible(cursor, uid)
                    polissa_wv = {'archivo': archivo}
                    self.write(cursor, uid, polissa_id, polissa_wv)

        return res

    def wkf_cancelar(self, cursor, uid, ids):
        """Limpia el campo del número de archivo cuando se cancela la póliza"""
        res = super(GiscedataPolissa, self).wkf_cancelar(cursor, uid, ids)
        if res:
            polissa_wv = {'archivo': ''}
            self.write(cursor, uid, ids, polissa_wv)

        return res

    def wkf_baixa(self, cursor, uid, ids):
        """Limpia el campo del número de archivo cuando se da de baja la
        póliza"""
        res = super(GiscedataPolissa, self).wkf_baixa(cursor, uid, ids)
        if res:
            polissa_wv = {'archivo': ''}
            self.write(cursor, uid, ids, polissa_wv)

        return res

    def get_archivo_disponible(self, cursor, uid, context=None):
        """
        Obtiene un número de archivo disponible (que no esté asignado en ninguna
        otra póliza) teniendo en cuenta el orden si hay agujeros.
        :return: Número de archivo. Es positivo y nunca 0.
        :rtype: int
        """
        polissa_ids = self.search(cursor, uid, [], context=context)
        polissa_f = ['archivo']
        polissa_vs = self.read(cursor, uid, polissa_ids, polissa_f, context=context)

        archivos = []
        for polissa_v in polissa_vs:
            archivo = polissa_v['archivo']
            if archivo:
                archivo = int(archivo)
                if archivo < len(archivos):
                    archivos[archivo] = True
                else:
                    tail_length = archivo - len(archivos)
                    tail = [False] * (tail_length + 1)
                    tail[-1] = True
                    archivos = archivos + tail

        res = 1
        if archivos:
            res = next((archivo + 1 for archivo, exists in enumerate(archivos[1:]) if not exists), len(archivos))

        return res


GiscedataPolissa()
