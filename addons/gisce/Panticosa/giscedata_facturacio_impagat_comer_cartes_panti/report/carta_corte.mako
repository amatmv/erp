<%inherit file="/giscedata_facturacio_impagat_comer_cartes/report/carta_tall.mako"/>
<%def name="compte_bancari(partner)">
    <div class="condicions">
        ${_(u"Puede efectuar el pago de las cantidades adeudadas en la siguiente cuenta bancaria poniendo como observación / concepto el número de factura:")}
        <ul>
            <li>${partner.bank_ids[0].iban}</li>
        </ul>
    </div>
</%def>