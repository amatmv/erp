# -*- coding: utf-8 -*-
{
    "name": "Cartas Corte Panticosa Comercializadora",
    "description": """Módulo que personaliza las cartas de corte de Panticosa Comercializadora""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Panticosa",
    "depends": [
        "giscedata_facturacio_impagat_comer_cartes",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "giscedata_facturacio_impagat_comer_cartes_report.xml",
    ],
    "active": False,
    "installable": True
}