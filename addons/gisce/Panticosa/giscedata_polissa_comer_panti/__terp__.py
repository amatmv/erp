# -*- coding: utf-8 -*-
{
    "name": "Panticosa Comercializadora",
    "description": """Módulo custom para las pólizas de Panticosa Comercializadora""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Panticosa",
    "depends": [
        "giscedata_facturacio_comer",
        "giscedata_polissa_condicions_generals",
        "l10n_ES_remesas",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
    ],
    "active": False,
    "installable": True
}