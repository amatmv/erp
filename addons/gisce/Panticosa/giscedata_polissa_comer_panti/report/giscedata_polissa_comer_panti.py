# -*- coding: utf-8 -*-
from giscedata_polissa_condicions_generals.report import ReportContracteCondicionsGenerals
from l10n_ES_remesas.report.mandato import MandatoWebkitParser


class PanticosaWebKitParserAppendPDF(ReportContracteCondicionsGenerals):

    def get_attachment_datas(self, pool, cursor, uid, model_id, datas, context=None):
        """
        Obté el mandat (en pdf preparat per ser escrit a un fitxer) relacionat amb
        la pòlissa.
        :param pool:
        :param cursor:
        :param uid:
        :param model_id: id de la pòlissa.
        :type model_id: long
        :param context:
        :return: mandat en format PDF binari si hi ha algun mandat actiu relacionat
        amb la pòlissa. Si no en troba retorna False.
        """
        mandate_o = pool.get('payment.mandate')
        res = super(PanticosaWebKitParserAppendPDF, self).get_attachment_datas(
            pool, cursor, uid, model_id, datas, context=context
        )

        dmn = [
            ('date_end', '=', False),
            ('reference', '=', 'giscedata.polissa,{}'.format(model_id))
        ]
        mandate_id = mandate_o.search(cursor, uid, dmn, context=context)

        if len(mandate_id) == 1:
            ctx = context.copy()
            ctx.update({'active_ids': [], 'active_id': False})
            datas = {
                'model': 'payment.mandate',
                'id': mandate_id[0],
                'report_type': 'webkit',
            }
            res.append(
                MandatoWebkitParser().create(
                    cursor, uid, mandate_id, datas, context=context
                )[0]
            )

        return res


PanticosaWebKitParserAppendPDF()
