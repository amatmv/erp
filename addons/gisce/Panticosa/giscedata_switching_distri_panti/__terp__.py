# -*- coding: utf-8 -*-
{
    "name": "Switching Panticosa (Distribucion)",
    "description": """El modulo implementa las siguientes funcionalidades
  * Nuevo asistente para generar el resumen de casos ATR con fechas de activacion
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends": [
        "giscedata_switching",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "wizard/wizard_generar_resumen_cambios_atr.xml",
        "security/ir.model.access.csv",
    ],
    "active": False,
    "installable": True
}
