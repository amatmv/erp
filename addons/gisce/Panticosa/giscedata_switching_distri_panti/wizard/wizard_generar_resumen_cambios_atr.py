# -*- coding: utf-8 -*-
from osv import osv, fields
from datetime import datetime, timedelta
import csv
import tempfile
import base64
import traceback
from gestionatr.defs import TABLA_17

PASOS_ATR_CON_FECHA_ACTIVACION = (
    'a3.05', 'b1.05', 'c1.05', 'c2.05', 'm1.05', #'d1.01',
)
TARIFA_POR_CODIGO = dict(TABLA_17)
PASOS_ATR_CAMBIO_TITULAR = ('c2.01', 'm1.01')
PASOS_ATR_CAMBIO_COMERCIALIZADORA = ('c1.01', 'c2.01')
CAMBIO_TITULAR_01 = ('T', 'S')
CSV_COLS = (
    'CLIENTE', 'CUPS', 'SOLICITUD', 'ACTIVACION', 'NUEVA POT', 'NUEVA TARIFA',
    'NUEVA COM'
)

# TODO
DESCRIPCION_PROCESO = {
    'A3': 'ALTA', 'B1': 'BAJA', 'C1': 'CAMBIO ',
    'C2': 'CAMBIO ', 'D1': 'D1', 'R1': 'R1',
    'M1': 'CAMBIO '
}

CONTROL_POTENCIA = {
    '1': 'icp',
    '2': 'max'
}


def get_potencias_from_step(step):
    """
    Obtiene la potencia mas alta que tenga ligada el paso ATR
    :param step: Paso ATR
    :type step: osv.orm.browse_record
    :return: Potencia mas grande.
    :rtype: int
    """
    potencias = []
    for p in step.pot_ids:
        potencias.append(p.potencia)

    if potencias:
        res = max(potencias)
    else:
        res = 0
    return res


class WizardGenerarResumenCambiosAtr(osv.osv_memory):
    _name = 'wizard.generar.resumen.cambios.atr'

    def treat_pasos_atr_cambio_titular(self, cursor, uid, model_name, pas_id, context=None):
        """
        Obtiene la informacion de la solicitud.
        :param cursor: DB Cursor.
        :type cursor: sql_db.Cursor
        :param uid: Identificador del usuario.
        :type uid: int
        :param model_name: nombre del modelo del paso ATR
        (giscedata.switching.xx.xx).
        :type model_name: str
        :param pas_id: identificador del paso ATR.
        :type pas_id: int
        :param context: OpenERP context.
        :type context: dict
        :return: Obtiene la informacion complementaria de la solicitud. El
        segundo valor de la tupla es un diccionario reservado a posibles
        nuevos valores que se asignen a alguna de las columnas del csv.
        :rtype: (list[str], dict)
        """
        pas_o = self.pool.get(model_name)
        pas = pas_o.browse(cursor, uid, pas_id, context=context)
        res = {}
        solicitud_info = []
        if pas.canvi_titular in CAMBIO_TITULAR_01:
            solicitud_info.append('TITULAR')

        return solicitud_info, res

    def treat_pasos_atr_con_fecha_activacion(self, cursor, uid, polissa_id, pas_name, model_name, pas_id, context=None):
        """
        Obtiene la informacion de la solicitud, la fecha de activacion (si la
        hay), la nueva tarifa (si hay cambio de tarifa), la nueva potencia (si
        cambio de potencia) y la nueva comercializadora en caso de que sea asi.
        :param cursor: DB Cursor.
        :type cursor: sql_db.Cursor
        :param uid: Identificador del usuario.
        :type uid: int
        :param polissa_id: Identificador de la poliza asociada con el caso ATR.
        :type polissa_id: int
        :param pas_name: Nombre del paso ATR (xx.xx)
        :type pas_name: str
        :param model_name: nombre del modelo del paso ATR
        (giscedata.switching.xx.xx).
        :type model_name: str
        :param pas_id: identificador del paso ATR.
        :type pas_id: int
        :param context: OpenERP context.
        :type context: dict
        :return: Tupla en el que el primer valor es una lista con las palabras
        clabe de la informacion de solicitud, y el segundo valor es un
        diccionario donde la llave es el nombre de la columna del csv y el
        valor correspondiente.
        :rtype: (list[str], dict)
        """
        polissa_o = self.pool.get('giscedata.polissa')
        pas_o = self.pool.get(model_name)
        pas = pas_o.browse(cursor, uid, pas_id, context=context)
        ctx = context.copy()
        solicitud_info = []

        res = {
            'activacion': '',           'nueva_tarifa': '',
            'nueva_pot': '',            'nueva_com': '',
        }
        if pas.data_activacio:
            data_activacio = pas.data_activacio
            data_alta = False

            if polissa_id:
                data_alta = polissa_o.read(
                    cursor, uid, polissa_id, ['data_alta'], context=ctx
                )['data_alta']

            activacion = datetime.strptime(data_activacio, '%Y-%m-%d')
            res['activacion'] = activacion.strftime('%d/%m/%Y')

            if data_alta and data_activacio > data_alta:
                ctx['date'] = activacion - timedelta(days=1)

        potencia_pas = get_potencias_from_step(pas)

        if pas_name == 'a3.05':
            res['nueva_pot'] = potencia_pas / 1000.0
            res['nueva_tarifa'] = TARIFA_POR_CODIGO[pas.tarifaATR]
            if pas.control_potencia == '2':
                res['nueva_tarifa'] += ' CON MAX'

        elif pas_name in ('c2.05', 'm1.05'):
            polissa_old = polissa_o.browse(cursor, uid, polissa_id, context=ctx)
            if polissa_old.potencia != potencia_pas:
                solicitud_info.append('POTENCIA')
                res['nueva_pot'] = potencia_pas / 1000.0

            if polissa_old.tarifa.codi_ocsum != pas.tarifaATR:
                solicitud_info.append('TARIFA')
                res['nueva_tarifa'] = TARIFA_POR_CODIGO[pas.tarifaATR]

            if polissa_old.facturacio_potencia != 'max' and pas.control_potencia == '2':
                res['nueva_tarifa'] += ' CON MAX'

        if pas_name in ('c1.05', 'c2.05'):
            res['nueva_com'] = pas.receptor_id.name

        return solicitud_info, res

    def fetch_cambios_atr(self, cursor, uid, sw_id, context=None):
        """
        Obtiene la informacion del caso ATR que se debe serializar en el CSV.
        :param cursor: DB Cursor.
        :type cursor: sql_db.Cursor
        :param uid: Identificador del usuario.
        :type uid: int
        :param sw_id: Identificador del caso ATR.
        :type sw_id: int
        :param context: OpenERP context.
        :type context: dict
        :return: Lista donde cada posicion contiene, ordenadamente segun la
        estructura del CSV, los valores a serializar del caso ATR.
        :rtype: list[str]
        """
        sw_o = self.pool.get('giscedata.switching')

        sw = sw_o.browse(cursor, uid, sw_id, context=context)
        solicitud = DESCRIPCION_PROCESO[sw.proces_id.name]
        activacion = ''
        nueva_pot = ''
        nueva_tarifa = ''
        nueva_com = ''
        to_append_solicitud = []

        for step in sw.step_ids:
            model_name, pas_id = step.pas_id.split(',')
            pas_id = int(pas_id)
            pas_name = model_name[-5:]

            if pas_name in PASOS_ATR_CAMBIO_COMERCIALIZADORA:
                to_append_solicitud.append('COMERCIALIZADORA')

            if pas_name in PASOS_ATR_CAMBIO_TITULAR:
                info_solicitud, res = self.treat_pasos_atr_cambio_titular(
                    cursor, uid, model_name, pas_id, context=context
                )
                to_append_solicitud += info_solicitud

            elif pas_name in PASOS_ATR_CON_FECHA_ACTIVACION:
                info_solicitud, res = self.treat_pasos_atr_con_fecha_activacion(
                    cursor, uid, sw.cups_polissa_id.id, pas_name, model_name,
                    pas_id, context=context
                )
                to_append_solicitud += info_solicitud
                activacion = res['activacion']
                nueva_pot = res['nueva_pot']
                nueva_tarifa = res['nueva_tarifa']
                nueva_com = res['nueva_com']

        solicitud += ' + '.join(to_append_solicitud)

        # Ultimos 4 numeros antes de las letras. Ejemplo:
        # ES0349000000001640NJ0F → 1640
        cups_resum = ''
        if sw.cups_id.name:
            cups_resum = sw.cups_id.name[-8:-4]

        res = [
            sw.titular_polissa.name, cups_resum, solicitud,
            activacion, nueva_pot, nueva_tarifa, nueva_com,
        ]

        return res

    def action_generar_csv(self, cursor, uid, ids, context=None):
        """
        Genera y guarda el fichero de resumen de cambios ATR en el campo
        'file' del asistente.
        :param cursor: DB Cursor.
        :type cursor: sql_db.Cursor
        :param uid: Identificador del usuario.
        :type uid: int
        :param ids: Identificador del asistente.
        :type ids: int
        :param context: OpenERP context.
        :type context: dict
        :return: True
        :rtype: bool
        """
        if isinstance(ids, list):
            if len(ids) != 1:
                raise NotImplementedError
            else:
                ids = ids[0]

        wiz = self.browse(cursor, uid, ids, context=context)
        sw_o = self.pool.get('giscedata.switching')
        dmn = []
        if wiz.fecha_inicio:
            dmn.append(('data_sollicitud', '>=', wiz.fecha_inicio))
        if wiz.fecha_final:
            dmn.append(('data_sollicitud', '<=', wiz.fecha_final))

        sw_ids = sw_o.search(cursor, uid, dmn, context=context)

        tmpfile = tempfile.TemporaryFile()
        csv_writer = csv.writer(tmpfile, delimiter=';')
        csv_writer.writerow(CSV_COLS)

        try:
            for sw_id in sw_ids:
                row = self.fetch_cambios_atr(cursor, uid, sw_id, context=context)
                csv_writer.writerow(row)

            tmpfile.seek(0)
            content = base64.b64encode(tmpfile.read())
            tmpfile.close()
        except Exception as e:
            traceback.print_exc()
            tmpfile.close()
            raise e

        wiz.write({'file': content})

        return True

    _columns = {
        'fecha_inicio': fields.date('Desde'),
        'fecha_final': fields.date('Hasta'),
        'file': fields.binary('Fichero',  filters='*.csv')
    }


WizardGenerarResumenCambiosAtr()
