# -*- coding: utf-8 -*-
from c2c_webkit_report import webkit_report
from report import report_sxw
from giscedata_facturacio_comer.report.giscedata_facturacio_comer import FacturaReportWebkitParserHTML, OPTIMIZED_INVOICES
from tools import config


webkit_report.WebKitParser(
    'report.giscedata.facturacio.factura.carta.requeriment',
    'giscedata.facturacio.factura',
    'giscedata_facturacio_comer_panti/report/carta_requerimiento.mako',
    parser=report_sxw.rml_parse
)

webkit_report.WebKitParser(
    'report.giscedata.facturacio.factura.segona.carta.requeriment',
    'giscedata.facturacio.factura',
    'giscedata_facturacio_comer_panti/report/carta_requerimiento.mako',
    parser=report_sxw.rml_parse
)

webkit_report.WebKitParser(
    'report.giscedata.facturacio.factura.carta.tall.bo.social',
    'giscedata.facturacio.factura',
    'giscedata_facturacio_comer_panti/report/carta_requerimiento.mako',
    parser=report_sxw.rml_parse
)

FacturaReportWebkitParserHTML(
    rml='giscedata_facturacio_comer_panti/report/factura.mako',
)

OPTIMIZED_INVOICES.append('giscedata_facturacio_comer_panti/report/factura.mako')