## -*- coding: utf-8 -*-
<%
    from operator import attrgetter, itemgetter
    from datetime import datetime, timedelta
    from giscedata_facturacio_comer.report.utils import clean_nif
    from giscedata_facturacio_comer.giscedata_facturacio_report import origen_energia_espanya_2017
    from base_iban.base_iban import _printed_iban_format
    import json, re

    report_o = pool.get('giscedata.facturacio.factura.report')
    factura_o = pool.get('giscedata.facturacio.factura')
    banner_obj = pool.get('report.banner')

    energy_origin_colors = ['#579834', '#579834', '#89c563', '#d7ef81', '#d9d82e', '#eea620', '#fc6230', '#ee3e26']

    median_consumption_mssg = _(
        u"El seu consum mitjà diari en el període facturat ha sigut de {} € "
        u"que corresponent a {} kWh/dia ({} dies)"
    )

    historic_average_consumption_mssg = _(
        u"El seu consum mitjà diari els últims {} mesos ({} dies) ha estat de "
        u"{} € que corresponen a {} kWh/dia "
    )

    accumulated_consumption_mssg = _(
        u"El seu consum acumulat el darrer any ha sigut de {} kWh"
    )


    def get_total_invoice_line_formatted(invoice_line):
        discount = invoice_line.get('discount', '')
        if discount:
            discount = _(u"{}% Dto: ").format(discount)

        return "{}{}".format(discount, formatLang(invoice_line['total']))
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_panti/report/factura.css"/>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_panti/report/assets/pie-chart.css"/>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_panti/report/assets/consumption-chart.css"/>
        <script src="${assets_path}/js/d3.min.js"></script>
        <script src="${assets_path}/js/c3.min.js"></script>
        <script src="${addons_path}/giscedata_facturacio_comer/report/assets/environmental_impact.js"></script>
         <style>
            @font-face {
                font-family: "Roboto-Regular";
                src: url("${assets_path}/fonts/Roboto/Roboto-Regular.ttf") format('truetype');
                font-weight: normal;
            }
        </style>
    </head>
    <%
        # Company information
        comer = report_o.get_company_info(cursor, uid, company.id)[company.id]
        lateral_info =  comer['lateral_info'] or ''
    %>
    <body>
        %for factura in factura_data:
            <%
                contact = factura['contact']
                detailed_lines = factura['detailed_lines']
                historic = factura['historic']
                historical = historic['historic']
                holder = factura['holder']
                invoiced_power = factura['invoiced_power']
                payer = factura['payer']
                periods_and_readings = factura['periods_and_readings']
                periods = periods_and_readings['periods']
                periods_a = periods['active']
                periods_r = periods['reactive']
                periods_m = periods['maximeter']
                readings = periods_and_readings['readings']
                readings_a = readings['active']
                readings_r = readings['reactive']
                readings_m = readings['maximeter']
                pie = factura['pie']
                pie_regulats = pie['regulats']
                pie_impostos = pie['impostos']
                pie_costos = pie['costos']
                dades_reparto = pie['dades_reparto']
                for repartiment in dades_reparto:
                    repartiment[3] = formatLang(repartiment[3])
                policy = factura['policy']
                power_excess_and_other_detailed_info = factura['power_excess_and_other_detailed_info']
                other_lines = power_excess_and_other_detailed_info['other_lines']
                meters_rent = power_excess_and_other_detailed_info['meters_rent']
                power_lines_with_excess = power_excess_and_other_detailed_info['lines_with_power_excess']
                supplier = factura['supplier']
                access_contract = supplier['ref'] or ''
                taxes = factura['taxes']

                median_consumption_full_mssg = median_consumption_mssg.format(
                    formatLang(detailed_lines['energy']['total']/(factura['days'] or 1.0)),
                    formatLang(factura['diari_factura_actual_kwh']),
                    factura['days'] or 1
                )

                accumulated_consumption_full_mssg = accumulated_consumption_mssg.format(
                    formatLang(historical['year_consumption'], digits=0)
                )

                historic_average_consumption_full_mssg = historic_average_consumption_mssg.format(
                    historical['months'], historical['days'],
                    formatLang(historical['average_cost']),
                    formatLang(historical['average_consumption'])
                )

                total_energy_toll = 0
                total_power_toll = 0

                factura_date = datetime.strptime(factura['date'], '%d/%m/%Y').strftime('%Y-%m-%d')
                setLang(factura['lang_partner'] or 'es_ES')

                fact_potencia = report_o.get_invoiced_power(cursor, uid, factura['id'], {factura['id']: periods_m})[factura['id']]
## _____________________________________________________________________________

                # Banner images
                banner_img = banner_obj.get_banner(
                    cursor, uid, 'giscedata.facturacio.factura',
                    factura_date, code='main_banner'
                )

                domiciliado = factura['payment_type_code'] == 'RECIBO_CSB'
                mail_format = True

                environmental_impact_co2 = banner_obj.get_banner(
                    cursor, uid, 'giscedata.facturacio.factura',
                    factura_date, code='environmental_impact_co2'
                )
                environmental_impact_radioactive = banner_obj.get_banner(
                    cursor, uid, 'giscedata.facturacio.factura',
                    factura_date, code='environmental_impact_radioactive'
                )
            %>

<%def name="invoice_summary(factura)">
    <!-- RESUM FACTURA -->
    <table id="resum-factura" class="styled-table">
        <thead>
            <tr>
                <th colspan="2">${_(u"RESUMEN FACTURA")}</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="bold">${_(u"Por energía")}</td>
                <td class="right">${formatLang(detailed_lines['energy']['total'])}</td>
            </tr>
            <tr>
                <td class="bold">${_(u"Por potencia")}</td>
                <td class="right">${formatLang(detailed_lines['power']['total'])}</td>
            </tr>
            % if detailed_lines['reactive']['total'] > 0:
                <tr>
                    <td class="bold">${_("Penalización por energía reactiva")}</td>
                    <td class="right">${formatLang(detailed_lines['reactive']['total'])}</td>
                </tr>
            % endif
            <tr class="row-separator">
                <td class="bold">${_(u"Impuesto electricidad")}</td>
                <td class="right">${formatLang(taxes['iese']['total'])}</td>
            </tr>
            <tr>
                <td class="bold">${_(u"Alquiler contador")}</td>
                <td class="right">${factura['rent']}</td>
            </tr>
            % if other_lines['total'] != 0:
                <tr>
                    <td class="bold">${_(u"Otros conceptos")}</td>
                    <td class="right">${formatLang(other_lines['total'])}</td>
                </tr>
            % endif
            % for tax in taxes['iva']['lines']:
                    <tr>
                        <td class="bold">${tax['name']}</td>
                        <td class="right">${formatLang(tax['amount'])}</td>
                    </tr>
            % endfor
            <tr class="row-separator">
                <td class="bold">${_(u"TOTAL")}</td>
                <td class="bold right">${"{} €".format(formatLang(factura['amount']))}</td>
            </tr>
        </tbody>
    </table>
</%def>
<%def name="banner_first_page(factura)">
    <table class="styled-table" id="factura">
        <thead>
            <tr>
                <th colspan="2">${_(u"FACTURA")}</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="bold">${_(u"Número de factura")}</td>
                <td>${factura.get('name', '')}</td>
            </tr>
            <tr>
                <td class="bold">${_(u"Fecha de emisión")}</td>
                <td>${formatLang(factura['date'], date=True)}</td>
            </tr>
            <tr>
                <td class="bold">${_(u"Período de facturación")}</td>
                <td>${factura['init_date']} - ${factura['end_date']} (Días: ${factura['days']})</td>
            </tr>
            %if not domiciliado:
                <tr>
                    <td class="bold">${_(u"Vencimento")}</td>
                    <td>${formatLang(factura['due_date'], date=True)}</td>
                </tr>
            %endif
            <tr>
                <td class="bold">${_(u"Pagador")}</td>
                <td>${payer['name']}</td>
            </tr>
            <tr>
                <td class="bold">${_(u"NIF")}</td>
                <td colspan="3">${payer['vat']}</td>
            </tr>
            %if domiciliado:
                <tr>
                    <td class="bold">${_(u"Entidad")}</td>
                    <td colspan="3">${factura['bank']['name']}</td>
                </tr>
                <tr>
                    <td class="bold">${_(u"IBAN")}</td>
                    <td colspan="3">${_printed_iban_format(factura['bank']['iban'])[:-4]} ****</td>
                </tr>
            %endif
        </tbody>
    </table>
</%def>
<%def name="tec271_info(factura, supplier)">
    <%
        sup_territorials_tec_271_comer_o = pool.get(
            'giscedata.suplements.territorials.2013.tec271.comer'
        )
        html_table = sup_territorials_tec_271_comer_o.get_info_html(cursor, uid, factura['cups']['name'])
    %>
        <div class="supl">
            <h1>${_(u"TABLA DETALLADA DE LOS SUPLEMENTOS AUTONOMICOS 2013 (*)")}</h1>
            ${html_table}
            <br>
            En caso de que el importe total de la regularización supere los dos euros, sin incluir impuestos, el mismo será fraccionado en partes iguales superiores a 1€ por las empresas comercializadoras en las facturas que se emitan en el plazo de 12 meses a partir de la primera regularización
            <br>
            * Tabla informativa conforme a lo establecido en la TEC/271/2019 de 6 de marzo, por la cual le informamos de los parámetros para el cálculo de los suplementos territoriales facilitados por su empresa distribuidora ${supplier['name']}
            <br>
            <%
                text = sup_territorials_tec_271_comer_o.get_info_text(cursor, uid, factura['cups']['name'])
                if text:
                    text = text.format(distribuidora=supplier['name'])
            %>
            ${text}
        </div>
</%def>
            <div id="lateral-info" class="center">${lateral_info}</div>
            <div id="first-page">
                <!-- HEADER -->
                <table id="header">
                    <colgroup>
                        <col width="55%"/>
                        <col>
                    </colgroup>
                    <tbody>
                        <tr>
                            <%
                                rowspan_logo = 'rowspan="4"'
                                if mail_format:
                                    rowspan_logo = 'rowspan="5"'
                            %>
                            <td ${rowspan_logo}><img src="data:image/jpeg;base64,${comer['logo']}"/></td>
                            <td>${comer['name']}</td>
                        </tr>
                        <tr>
                            <td>${clean_nif(comer['vat'])}</td>
                        </tr>
                        <tr>
                            <td>${comer['street'].replace('..', '.')}</td>
                        </tr>
                        <tr>
                            <td>${comer['zip']}, ${comer['poblacio']} (${comer['state']})</td>
                        </tr>
                        %if mail_format:
                            <tr>
                                <td>
                                    <!-- FINESTRETA -->
                                    <table id="finestreta">
                                        <tbody>
                                            <tr>
                                                <td>${contact['name']}</td>
                                            </tr>
                                            <tr>
                                                <td>${contact['street'].replace('..', '.')}</td>
                                            </tr>
                                            <tr>
                                                <td>${contact['zip']} ${contact['city']}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <!-- END FINESTRETA -->
                                </td>
                            </tr>
                        %endif
                    </tbody>
                </table>
                <!-- END HEADER -->
                <!-- BANNER + (TITULAR + RESUM FACTURA)-->
                <table id="banner-holder-summary">
                    <colgroup>
                        <col width="60%"/>
                        <col width="40%"/>
                    </colgroup>
                    <tbody>
                        <tr>
                            <td id="banner-cell" valign="top">
                                ${banner_first_page(factura)}
                            </td>
                            <td valign="top">
                                %if not mail_format:
                                    <!-- DESTINATARI -->
                                    <table class="styled-table">
                                        <thead>
                                            <tr>
                                                <th colspan="2">${_(u"DESTINATARIO")}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="bold">${_(u"Nombre")}</td>
                                                <td>${contact['name']}</td>
                                            </tr>
                                            <tr>
                                                <td class="bold">${_(u"Dirección")}</td>
                                                <td colspan="3">${contact['street'].replace('..', '.')} ${contact['zip']} ${contact['city']}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <!-- END DESTINATARI -->
                                %endif
                                ${invoice_summary(factura)}
                            </td>
                        </tr>
                    </tbody>
                </table>
                <!-- END BANNER + (TITULAR + RESUM FACTURA)-->
                <!-- CONTRACTE -->
                <table class="styled-table">
                    <thead>
                        <tr>
                            <th colspan="4">${_(u"CONTRATO")}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="bold">${_(u"Titular")}</td>
                            <td>${holder['name']}</td>
                            <td class="bold">${_(u"NIF")}</td>
                            <td>${holder['vat']}</td>
                        </tr>
                        <tr>
                            <td class="bold">${_(u"Contrato del suministro")}</td>
                            <td>${policy['name']}</td>
                            <td class="bold">${_(u"Contrato de acceso")}</td>
                            <td>${access_contract}</td>
                        </tr>
                        <tr>
                            <td rowspan="2" class="bold">${_(u"Fecha alta")}</td>
                            <td rowspan="2">${formatLang(policy['start_date'], date=True)}</td>
                            <td class="bold">${_(u"Fecha final")}</td>
                            <td>${formatLang(policy['end_date'], date=True)}</td>
                        </tr>
                        <tr>
                            <td colspan="2" class="little">(${_(u"renovación anual automática)")})</td>
                        </tr>
                        <tr>
                            <td class="bold">${_(u"Tarifa")}</td>
                            <td>${factura['tariff']}</td>
                            <td class="bold">${_(u"CNAE")}</td>
                            <td>${policy['cnae']}</td>
                        </tr>
                        <tr>
                            <td class="bold">${_(u"Producto Comer")}</td>
                            <td colspan="3">${policy['pricelist']}</td>
                        </tr>
                        <tr>
                            <td class="bold">${_(u"CUPS")}</td>
                            <td colspan="3">${factura['cups']['name']}</td>
                        </tr>
                        <tr>
                            <td class="bold">${_(u"Distribuidora")}</td>
                            <td colspan="3">${supplier['name']}</td>
                        </tr>
                        <tr>
                            <td class="bold">${_(u"Dirección de suministro")}</td>
                            <td colspan="3">${factura['cups']['address']}</td>
                        </tr>
                    </tbody>
                </table>
                <!-- END CONTRACTE -->
                <!-- HISTORIC -->
                <table class="styled-table">
                    <thead>
                        <tr>
                            <th>${_(u"HISTORIAL ÚLTIMOS 14 MESES")}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="background-color">
                                <div class="chart_consum" id="chart_consum_${factura['id']}"></div>
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td class="italic">
                                ${historic_average_consumption_full_mssg}<br/>
                                ${accumulated_consumption_full_mssg}
                            </td>
                        </tr>
                    </tfoot>
                </table>
                <!-- END HISTORIC -->
                <!-- AVARIES-URGENCIES-ATENCIO CLIENT -->
                <table id="avaries-urgencies-atencio-client">
                    <colgroup>
                        <col width="50%"/>
                        <col width="50%"/>
                    </colgroup>
                    <tbody>
                        <tr>
                            <td valign="top">
                                <table class="styled-table-with-caption">
                                    <caption>${_(u"AVERIAS Y URGENCIAS")}</caption>
                                    <colgroup>
                                        <col width="40%"/>
                                    </colgroup>
                                    <tbody>
                                        <tr>
                                            <th>${_(u"Empresa distribuidora")}</th>
                                            <td>${supplier['name']}</td>
                                        </tr>
                                        <tr>
                                            <th>${_(u"Teléfono gratuito")}</th>
                                            <td>${supplier['phone']}</td>
                                        </tr>
                                        <tr>
                                            <th>${_(u"Contrato de acceso")}</th>
                                            <td>${access_contract}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td valign="top">
                                <table class="styled-table-with-caption ">
                                    <caption>${_(u"ATENCIÓN AL CLIENTE")}</caption>
                                    <colgroup>
                                        <col width="40%"/>
                                    </colgroup>
                                    <tbody>
                                        <tr>
                                            <th>${_(u"Empresa comercializadora")}</th>
                                            <td>${comer['name']}</td>
                                        </tr>
                                        <tr>
                                            <th>${_(u"Teléfono gratuito")}</th>
                                            <td>${comer['phone']}</td>
                                        </tr>
                                        <tr>
                                            <th>${_(u"Email")}</th>
                                            <td>${comer['email']}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <!-- AVARIES-URGENCIES-ATENCIO CLIENT -->
            </div>
            <!-- END FIRST PAGE -->
            <p style="page-break-after:always; clear:both"></p>
            <!-- SECOND PAGE -->
            <div id="second-page">
                <!-- DETALLE FACTURA -->
                <table id="detall-factura" class="styled-table center-th">
                    <colgroup>
                        <col width="32%" class="background-color"/>
                        <col width="20%"/>
                        <col/>
                        <col width="10%"/>
                    </colgroup>
                    <thead>
                        <tr>
                            <th colspan="3">${_(u"DETALLE FACTURA")}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="3">
                                <!-- ENERGIA -->
                                <table class="styled-inner-table">
                                    <colgroup>
                                        <col class="detall-factura-inner-table-col-1"/>
                                        <col class="detall-factura-inner-table-col-2"/>
                                        <col class="detall-factura-inner-table-col-3"/>
                                        <col class="detall-factura-inner-table-col-4"/>
                                        <col class="detall-factura-inner-table-col-5"/>
                                    </colgroup>
                                    <thead>
                                        <tr>
                                            <th>${_(u"Término de energia")}</th>
                                            <th>${_(u"Consumo")} (kWh)</th>
                                            <th>${_(u"Precio")} (€/kWh)</th>
                                            <th colspan="2">${_(u"Total")} (€)</th>
                                         </tr>
                                    </thead>
                                    <tbody>
                                        % for energy_line in detailed_lines['energy']['lines']:
                                            <tr>
                                                <td class="bold center">${energy_line['name']}</td>
                                                <td class="right">${int(energy_line['consumption'])}</td>
                                                <td class="right">${formatLang(energy_line['price'], digits=6)}</td>
                                                <td class="right">${get_total_invoice_line_formatted(energy_line)}</td>
                                                <td class="left white toll italic">(${_(u"de los cuales peajes {}").format(formatLang(energy_line['toll']))})</td>
                                            </tr>
                                        % endfor
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr class="row-separator">
                            <td colspan="4" class="bold right white">${formatLang(detailed_lines['energy']['total'])} €</td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <!-- POTENCIA -->
                                <table class="styled-inner-table">
                                    <colgroup>
                                        <col width="20%"/>
                                        <col width="20%"/>
                                        <col width="20%"/>
                                        <col/>
                                        <col/>
                                        <col width="20%"/>
                                    </colgroup>
                                    <thead>
                                        <tr>
                                            <th>${_(u"Término de potencia")}</th>
                                            <th>${_(u"Consumo")} (kW)</th>
                                            <th>${_(u"Precio")} (€/kW-${_(u"dia")})</th>
                                            % if monthly_invoice:
                                                <th>${_(u"Meses")}</th>
                                            %else:
                                                <th>${_(u"Días")}</th>
                                            %endif
                                            <th colspan="2">${_(u"Total")} (€)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        % for power_line in detailed_lines['power']['lines']:
                                            <tr>
                                                <td class="bold center white">${power_line['name']}</td>
                                                <td class="right white">${formatLang(power_line['consumption'], digits=3)}</td>
                                                <td class="right white">${formatLang(power_line['price'], digits=6)}</td>
                                                <td class="center white">${int(power_line['time'])}</td>
                                                <td class="right white">${get_total_invoice_line_formatted(power_line)}</td>
                                                <td class="left white italic toll">(${_(u"de los cuales peajes {}").format(formatLang(power_line['toll']))})</td>
                                            </tr>
                                        % endfor
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr class="row-separator">
                            <td colspan="5" class="bold right white">${formatLang(detailed_lines['power']['total'])} €</td>
                        </tr>
                        %if detailed_lines['reactive']['lines']:
                            <tr>
                                <td colspan="3">
                                    <!-- ENERGIA REACTIVA  -->
                                    <table class="styled-inner-table">
                                        <colgroup>
                                            <col class="detall-factura-inner-table-col-1"/>
                                            <col class="detall-factura-inner-table-col-2"/>
                                            <col class="detall-factura-inner-table-col-3"/>
                                            <col class="detall-factura-inner-table-col-4"/>
                                            <col class="detall-factura-inner-table-col-5"/>
                                        </colgroup>
                                        <thead>
                                            <tr>
                                                <th>${_(u"Energía reactiva")}</th>
                                                <th>${_(u"Exceso")} (kVArh)</th>
                                                <th>${_(u"Precio")} (€/kVArh)</th>
                                                <th colspan="2">${_(u"Total")} (€)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            % for reactive_line in detailed_lines['reactive']['lines']:
                                                <tr>
                                                    <td class="bold center">${reactive_line['name']}</td>
                                                    <td class="right">${formatLang(reactive_line['consumption'])}</td>
                                                    <td class="right">${formatLang(reactive_line['price'], digits=6)}</td>
                                                    <td class="right">${get_total_invoice_line_formatted(reactive_line)}</td>
                                                    <td class="left white toll italic">(${_(u"de los cuales peajes {}").format(formatLang(reactive_line['toll']))})</td>
                                                </tr>
                                            % endfor
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr class="row-separator">
                                <td colspan="4" class="bold right white">${formatLang(detailed_lines['reactive']['total'])} €</td>
                            </tr>
                        %endif
                        %if power_lines_with_excess:
                            <tr>
                                <td colspan="3">
                                    <!-- EXCES POTENCIA -->
                                    <table class="styled-inner-table">
                                        <colgroup>
                                            <col class="detall-factura-inner-table-col-1"/>
                                            <col class="detall-factura-inner-table-col-2"/>
                                            <col class="detall-factura-inner-table-col-3"/>
                                            <col class="detall-factura-inner-table-col-4"/>
                                            <col class="detall-factura-inner-table-col-5"/>
                                        </colgroup>
                                        <thead>
                                            <tr>
                                                <th>${_(u"Exceso potencia")}</th>
                                                <th>${_(u"Exceso")} (kVArh)</th>
                                                <th>${_(u"Precio")} (€/kVArh)</th>
                                                <th colspan="2">${_(u"Total")} (€)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <% total_exces = 0.0 %>
                                            % for power_line_with_excess in power_lines_with_excess:
                                                <tr>
                                                    <td class="bold center">${power_line_with_excess['name']}</td>
                                                    <td class="right">${formatLang(power_line_with_excess['excess'])}</td>
                                                    <td class="right">${formatLang(power_line_with_excess['price'], digits=6)}</td>
                                                    <td class="center" colspan="2">${power_line_with_excess['total']}</td>
                                                </tr>
                                                <% total_exces += power_line_with_excess['total'] %>
                                            % endfor
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr class="row-separator">
                                <td colspan="4" class="bold right white">${formatLang(total_exces)} €</td>
                            </tr>
                        %endif
                        % for iese_line in taxes['iese']['lines']:
                            <tr class="row-separator">
                                <th>${_(u"Impuesto de la electricidad")}</th>
                                <td class="right">${formatLang(iese_line['base_amount'])} €</td>
                                <td class="right">${u"5,11269632%"}</td>
                                <td class="bold right">${formatLang(iese_line['tax_amount'])} €</td>
                            </tr>
                        % endfor
                        % for meter_rent in meters_rent:
                            <tr class="row-separator">
                                <th>${_(u"Alquiler contador")}</th>
                                <td></td>
                                <td></td>
                                <td class="bold right">${formatLang(meter_rent['amount'])} €</td>
                            </tr>
                        % endfor
                        %if other_lines['lines']:
                            <tr class="row-separator">
                                <% rowspan = len(other_lines['lines']) + 1 %>
                                <th rowspan="${rowspan}">${_(u"Otros")}</th>
                                <td colspan="3"></td>
                            </tr>
                            % for other_line in other_lines['lines']:
                                <tr>
                                    <td colspan="2" class="bold">${other_line['name']}</td>
                                    <td class="bold right">${formatLang(other_line['price'])} €</td>
                                </tr>
                            % endfor
                        %endif
                        % for iva_line in taxes['iva']['lines']:
                            <tr class="row-separator">
                                <th>${iva_line['name']}</th>
                                <td class="right">${formatLang(iva_line['base'])} €</td>
                                <td class="right">${iva_line['percentage']}</td>
                                <td class="bold right">${formatLang(iva_line['amount'])} €</td>
                            </tr>
                        % endfor
                        <tr class="row-separator">
                            <th>${_(u"TOTAL FACTURA")}</th>
                            <td></td>
                            <td></td>
                            <td class="bold right">${formatLang(factura['amount'])} €</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="4" class="white">${_(u"Los precios de los términos de peajes de acceso son los publicados en la ORDEN ETU/1976/2016")}</td>
                        </tr>
                        <tr>
                            <td colspan="4" class="white">${_(u"Los precios de alquiler de los contadores son los establecidos en la ORDEN IET/1491/2013")}</td>
                        </tr>
                    </tfoot>
                </table>
                <!-- END DETALLE FACTURA -->
                <!-- LECTURES -->
                <table id="consum-electric" class="styled-table">
                    <thead>
                        <tr>
                            <th></th>
                            <th>${_(u"CONSUMO ELÉCTRICO")}</th>
                            % for period_name in periods_a:
                                <th>${period_name}</th>
                            % endfor
                        </tr>
                    </thead>
                    <tbody>
                        <!-- ACTIVA -->
                        %if readings_a['readings']:
                            <tr>
                                <th rowspan="5" class="rotate h65px"><div>${_(u"ENERGÍA")}</div></th>
                            </tr>
                            % for meter in sorted(readings_a['readings']):
                                <%
                                    meter_readings =  readings_a['readings'][meter]
                                    previous_reading_title = _(u"Lectura anterior {} ({})").format(
                                        formatLang(meter_readings['P1']['previous_date'], date=True),
                                        meter_readings['P1']['previous_origin']
                                    )
                                    current_reading_title = _(u"Lectura actual {} ({})").format(
                                        formatLang(meter_readings['P1']['current_date'], date=True),
                                        meter_readings['P1']['current_origin']
                                    )
                                %>
                                <tr>
                                    <td class="bold light-blue">${_(u"Número de contador")}</td>
                                    % for period_name in periods_a:
                                        <td class="center light-blue">${meter}</td>
                                    % endfor
                                </tr>
                                <tr>
                                    <td class="bold">${previous_reading_title}</td>
                                    % for period in periods_a:
                                        <td class="right">${formatLang(meter_readings[period]['previous_reading'], digits=0)} kWh</td>
                                    % endfor
                                </tr>
                                <tr>
                                    <td class="bold">${current_reading_title}</td>
                                    % for period in periods_a:
                                        <td class="right">${formatLang(meter_readings[period]['current_reading'], digits=0)} kWh</td>
                                    % endfor
                                </tr>
                            % endfor
                            <tr>
                                <td class="bold">${_(u"Total periodo")}</td>
                                % for period in periods_a:
                                    <td class="right">${formatLang(readings_a['total_consumption'][period], digits=0)} kWh</td>
                                %endfor
                            </tr>
                        %endif
                        <!-- ACTIVA -->
                        <!-- REACTIVA -->
                        % if readings_r['readings'] or te_maximetre:
                            <tr>
                                <th rowspan="5" class="rotate h65px"><div>${_(u"REACTIVA")}</div></th>
                            </tr>
                            % for meter in sorted(readings_r['readings']):
                                <%
                                    meter_readings =  readings_r['readings'][meter]
                                    previous_reading_title = _(u"Lectura anterior {} ({})").format(
                                        formatLang(meter_readings['P1']['previous_date'], date=True),
                                        meter_readings['P1']['previous_origin']
                                    )
                                    current_reading_title = _(u"Lectura actual {} ({})").format(
                                        formatLang(meter_readings['P1']['current_date'], date=True),
                                        meter_readings['P1']['current_origin']
                                    )
                                %>
                                <tr class="row-separator">
                                    <td class="bold light-blue">${_(u"Número de contador")}</td>
                                    % for period_name in periods_r:
                                            <td class="center light-blue">${meter}</td>
                                    % endfor
                                </tr>
                                <tr>
                                    <td class="bold">${previous_reading_title}</td>
                                    % for period in periods_r:
                                        <td class="right">${formatLang(meter_readings[period]['previous_reading'], digits=0)} kWh</td>
                                    % endfor
                                </tr>
                                <tr>
                                    <td class="bold">${current_reading_title}</td>
                                    % for period in periods_r:
                                        <td class="right">${formatLang(meter_readings[period]['current_reading'], digits=0)} kWh</td>
                                    % endfor
                                </tr>
                            % endfor
                            <tr>
                                <td class="bold">${_(u"Total periodo")}</td>
                                % for period_name in periods_r:
                                    <td class="right">${formatLang(readings_r['total_consumption'][period], digits=0)} kWh</td>
                                % endfor
                            </tr>
                        %endif
                        <!-- REACTIVA -->
                        <!-- MAXIMETRE -->
                        % if policy['has_maximeter']:
                            <tr class="header">
                                <th rowspan="4" class="rotate h65px"> <div>${_(u"MAXÍMETRO")}</div></th>
                            </tr>
                            <tr class="row-separator">
                                <td class="bold">${_(u"Potencia contratada")}</td>
                                % for period in periods_m:
                                    <td class="right">${formatLang(readings_m[period]['power'], digits=3)} kW</td>
                                % endfor
                            </tr>
                            <tr>
                                <td class="bold">${_(u"Lectura maxímetro")}</td>
                                % for period in periods_m:
                                    <td class="right">${formatLang(readings_m[period]['maximeter'], digits=3)} kW</td>
                                % endfor
                            </tr>
                            <tr>
                                <td class="bold">${_(u"Potencia facturada periodo")}</td>
                                % for period in periods_m:
                                    <td class="right">${formatLang(factura['invoiced_power'][period], digits=3)} kW</td>
                                % endfor
                            </tr>
                        %endif
                        <!-- MAXIMETRE -->
                    </tbody>
                    <tfoot>
                        <tr>
                            <% colspan =  len(periods_a) + 2 %>
                            <td colspan="${colspan}" class="center">
                                ${median_consumption_full_mssg}
                            </td>
                        </tr>
                    </tfoot>
                </table>
                <!-- END LECTURES -->
                %if len(periods_a) <= 3:
                    <!-- DESTI -->
                    <table id="desti-factura" class="styled-table">
                        <thead>
                            <tr>
                                <th class="seccio">${_(u"DESTÍNO DE LA FACTURA")}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>${_(u"El destino del importe de su factura ({} €) es el siguiente:").format(formatLang(factura['amount']))}</td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="chart_desti" id="chart_desti_${factura['id']}"></div>
                                </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td class="italic">${_(u"A los importes indicados en el diagrama se les tiene que añadir, en su caso, el alquiler de los equipos de medida y control")}</td>
                            </tr>
                        </tfoot>
                    </table>
                    <!-- END DESTI -->
                %endif
##                <div id="energy-origin-banner">
##                    <img src="data:image/jpeg;base64,${image_energy_origin}"/>
##                </div>
##                <table width="94%" align="center" class="styled-table">
##                    <thead>
##                        <tr>
##                            <th colspan="3">${_(u"Origen de la electricidad")}</th>
##                        </tr>
##                    </thead>
##                    <tbody>
##                        <tr>
##                            <td>
##                                <div id="energy-origin-comer"></div>
##                            </td>
##                            <td>
##                                <table id="energy-origin">
##                                    <colgroup>
##                                        <col width="5%"/>
##                                        <col width="43%"/>
##                                        <col width="26%"/>
##                                        <col width="26%"/>
##                                    </colgroup>
##                                    <thead>
##                                        <tr>
##                                            <th colspan="2">${_(u"Origen")}</th>
##                                            <th>${_(u"Energías de Panticosa")}</th>
##                                            <th>${_(u"Media sistema eléctrico español (2017)")}</th>
##                                        </tr>
##                                    </thead>
##                                    <tbody>
##                                        <tr>
##                                            <td><div class="square color-renovable"></div></td>
##                                            <td>${_(u"Renovable")}</td>
##                                            <td class="center">5,2%</td>
##                                            <td class="center">${origen_energia_espanya_2017['Renovable']}%</td>
##                                        </tr>
##                                        <tr>
##                                            <td><div class="square color-cogeneracio-alta-eficiencia"></div></td>
##                                            <td>${_(u"Cogeneración de Alta Eficiencia")}</td>
##                                            <td class="center">0,1%</td>
##                                            <td class="center">${origen_energia_espanya_2017['Cogeneración de Alta Eficiencia']}%</td>
##                                        </tr>
##                                        <tr>
##                                            <td><div class="square color-cogeneracio"></div></td>
##                                            <td>${_(u"Cogeneración")}</td>
##                                            <td class="center">14,4%</td>
##                                            <td class="center">${origen_energia_espanya_2017['Cogeneración']}%</td>
##                                        </tr>
##                                        <tr>
##                                            <td><div class="square color-cc-gas-natural"></div></td>
##                                            <td>${_(u"CC Gas Natural")}</td>
##                                            <td class="center">20,2%</td>
##                                            <td class="center">${origen_energia_espanya_2017['CC Gas Natural']}%</td>
##                                        </tr>
##                                        <tr>
##                                            <td><div class="square color-carbo"></div></td>
##                                            <td>${_(u"Carbón")}</td>
##                                            <td class="center">24,6%</td>
##                                            <td class="center">${origen_energia_espanya_2017['Carbón']}%</td>
##                                        </tr>
##                                        <tr>
##                                            <td><div class="square color-fuel-gas"></div></td>
##                                            <td>${_(u"Fuel/Gas")}</td>
##                                            <td class="center">3,8%</td>
##                                            <td class="center">${origen_energia_espanya_2017['Fuel/Gas']}%</td>
##                                        </tr>
##                                        <tr>
##                                            <td><div class="square color-nuclear"></div></td>
##                                            <td>${_(u"Nuclear")}</td>
##                                            <td class="center">30,3%</td>
##                                            <td class="center">${origen_energia_espanya_2017['Nuclear']}%</td>
##                                        </tr>
##                                        <tr>
##                                            <td><div class="square color-altres"></div></td>
##                                            <td>${_(u"Otras")}</td>
##                                            <td class="center">1,4%</td>
##                                            <td class="center">${origen_energia_espanya_2017['Otras']}%</td>
##                                        </tr>
##                                    </tbody>
##                                </table>
##                            </td>
##                            <td>
##                                <div id="energy-origin-country"></div>
##                            </td>
##                        </tr>
##                    </tbody>
##                </table>
                %if factura_o.has_tec271_line(cursor, uid, factura['id']):
                    ${tec271_info(factura, supplier)}
                %else:
                    <table id="environmental-impact" class="styled-table">
                        <colgroup>
                            <col width="40%"/>
                            <col width="20%"/>
                            <col width="40%"/>
                        </colgroup>
                        <thead>
                            <tr>
                                <th colspan="3">${_(u"Impacto medioambiental")}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div id="environmental-impact-co2">
                                        %if environmental_impact_co2:
                                            <img src="data:image/jpeg;base64,${environmental_impact_co2}">
                                        %else:
                                            <img src="${addons_path}/giscedata_facturacio_comer/report/assets/images/environmental_impact_co2.jpg">
                                        %endif
                                    </div>
                                </td>
                                <td id="environmental-impact-descr" class="center">
                                    ${_(u"El impacto medioambiental de su electricidad consumida depende de las fuentes de energía utilizadas para su  generación.")}
                                </td>
                                <td>
                                    <div id="environmental-impact-radioactive">
                                        %if environmental_impact_radioactive:
                                            <img src="data:image/jpeg;base64,${environmental_impact_radioactive}">
                                        %else:
                                            <img src="${addons_path}/giscedata_facturacio_comer/report/assets/images/environmental_impact_radioactive.jpg">
                                        %endif
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                %endif
                <script>
                    var donut_height = 170;
                    var colors = ${energy_origin_colors};
                    var cols = [
                        ['Renovable', 5.2],
                        ['Cogeneración de Alta Eficiencia', 0.1],
                        ['Cogeneración', 14.4],
                        ['CC Gas Natural', 20.2],
                        ['Carbón', 24.6],
                        ['Fuel/Gas', 3.8],
                        ['Nuclear', 30.3],
                        ['Otras', 1.4]
                    ];
                    donut_chart(donut_height, donut_height, cols, colors, '#energy-origin-comer', false, 'Panticosa');
                    cols = [
                        ['Renovable', ${origen_energia_espanya_2017['Renovable']}],
                        ['Cogeneración de Alta Eficiencia', ${origen_energia_espanya_2017['Cogeneración de Alta Eficiencia']}],
                        ['Cogeneración', ${origen_energia_espanya_2017['Cogeneración']}],
                        ['CC Gas Natural', ${origen_energia_espanya_2017['CC Gas Natural']}],
                        ['Carbón', ${origen_energia_espanya_2017['Carbón']}],
                        ['Fuel/Gas', ${origen_energia_espanya_2017['Fuel/Gas']}],
                        ['Nuclear', ${origen_energia_espanya_2017['Nuclear']}],
                        ['Otras', ${origen_energia_espanya_2017['Otras']}],
                    ];
                    donut_chart(donut_height, donut_height, cols, colors, '#energy-origin-country', false, 'España')
                </script>
            </div>

            <script>
                // BAR PLOT VARIABLES
                var barplot_height = 200;
                var barplot_width = 715;

                var grad_colors_1 = [['#badcff', '#4D77A4']];

                var grad_colors_3 = [
                    ['#badcff', '#4D77A4'], ['#badcff', '#4D77A4'], ['#badcff', '#4D77A4'],
                    ['#badcff', '#4D77A4'], ['#badcff', '#4D77A4'], ['#badcff', '#4D77A4']
                ];

                var factura_id = ${factura['id']};
                var data_consum = ${json.dumps(sorted(historic['historic_js'], key=lambda h: h['mes']))};

                // PIE CHART VARIABLES
                var piechart_height = 120;
                var piechart_width = 700;

                //pie start_position
                var pie_left = 130;

                var pie_total = ${pie['total']};
                var pie_data = [
                    {val: ${pie_regulats}, perc: 30, code: "REG"},
                    {val: ${pie_costos}, perc: 55, code: "PROD"},
                    {val: ${pie_impostos},  perc: 15 ,code: "IMP"}
                ];

                var pie_etiquetes = {
                    'REG': {t: ['${formatLang(float(pie_regulats))} €','${_(u"Costos regulats")}'], w:100},
                    'IMP': {t: ['${formatLang(float(pie_impostos))} €','${_(u"Impostos aplicats")}'], w:100},
                    'PROD': {t: ['${formatLang(float(pie_costos))} €','${_(u"Costos de producció electricitat")}'], w:150}
                };

                var reparto = ${json.dumps(pie['reparto'])};
                var dades_reparto = ${json.dumps(dades_reparto)};
            </script>
            <script src="${addons_path}/giscedata_facturacio_comer/report/assets/consumption-chart.js"></script>
            <script src="${addons_path}/giscedata_facturacio_comer/report/assets/pie-chart.js"></script>
            % if factura != factura_data[-1]:
                <p style="page-break-after:always; clear:both"></p>
            % endif
        %endfor
    </body>
</html>
