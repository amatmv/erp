<%inherit file="/giscedata_facturacio_impagat_comer_cartes/report/carta_requeriment.mako"/>
<%block name="custom_css">
    <style>
        body {
            font-size: 13px;
        }
        p {
            font-size: 13px;
        }
    </style>
</%block>
<%block name="forma_pagament">
    <div>
        <p>Puede efectuar el pago de las cantidades adeudadas en la siguiente cuenta bancaria poniendo como observación / concepto el número de factura:</p>
        <ul>
            <li>ES41 2085 2462 23 0330150678</li>
        </ul>
    </div>
</%block>
