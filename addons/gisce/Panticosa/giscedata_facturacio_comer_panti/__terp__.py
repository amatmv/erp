# -*- coding: utf-8 -*-
{
    "name": "Facturación Comercialitzadora",
    "description": """Facturación Panticosa Comercialitzadora""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Panticosa",
    "depends": [
        "giscedata_facturacio_comer",
        "giscedata_facturacio_comer_bono_social",
        "giscedata_facturacio_impagat_comer_cartes"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "giscedata_facturacio_comer_panti_report.xml"
    ],
    "active": False,
    "installable": True
}
