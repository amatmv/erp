import logging

from osv import osv
import pooler
from slugify import slugify
from .utils import data_merge


class DeveloperToolsExport(osv.osv_memory):
    _name = 'developer.tools.export'

    def model_exists(self, model):
        obj = self.pool.get(model)
        if model is None:
            raise Exception('Model {0} does not exist!'.format(model))
        return obj

    def get_model_base_module(self, cursor, uid, model):
        imd_obj = self.pool.get('ir.model.data')
        obj = self.model_exists(model)
        name = 'model_{0}'.format(obj._table)
        ids = imd_obj.search(cursor, uid, [
            ('name', '=', name),
            ('model', '=', 'ir.model')
        ])
        return imd_obj.read(cursor, uid, ids[0], ['module'])['module']

    def get_field_base_module(self, cursor, uid, model, field):
        imd_obj = self.pool.get('ir.model.data')
        obj = self.model_exists(model)
        name = 'field_{0}_{1}'.format(obj._table, field)
        ids = imd_obj.search(cursor, uid, [
            ('name', '=', name),
            ('model', '=', 'ir.model.fields')
        ])
        if not ids:
            raise Exception('field {0} on model {1} not found'.format(
                field, model
            ))
        return imd_obj.read(cursor, uid, ids[0], ['module'])['module']

    def get_ref(self, cursor, uid, model, res_id, context=None):
        imd_obj = self.pool.get('ir.model.data')
        self.model_exists(model)
        ids = imd_obj.search(cursor, uid, [
            ('res_id', '=', res_id),
            ('model', '=', model)
        ])
        if ids:
            data = imd_obj.read(cursor, uid, ids[0], ['module', 'name'])
            return '{module}.{name}'.format(**data)
        else:
            return False

    def export_model(self, cursor, uid, model, res_id, context=None):
        if context is None:
            context = {}
        imd_obj = self.pool.get('ir.model.data')
        logger = logging.getLogger('openerp.devtools.export_model')
        new_cursor = context.get('new_cursor', True)
        if new_cursor:
            cursor = pooler.get_db(cursor.dbname).cursor()
            context['new_cursor'] = False
        res = {}
        model_obj = self.model_exists(model)
        module = self.get_model_base_module(cursor, uid, model)
        res_ref = self.get_ref(cursor, uid, model, res_id)
        logger.info('Exporting model {0}:{1}'.format(model, res_id))
        if not res_ref:
            res_ref = '{0}_{1}_{2}'.format(
                model_obj._table,
                slugify(model_obj.name_get(cursor, uid, [res_id])[0][1]),
                res_id
            )
            imd_obj.create(cursor, uid, {
                'name': res_ref,
                'model': model,
                'module': module,
                'res_id': res_id
            })
        res_module_fields = []
        res[module] = [{
            'id': res_ref,
            'model': model,
            'fields': res_module_fields
        }]
        fields_def = model_obj.fields_get(cursor, uid)
        for field, value in model_obj.read(cursor, uid, res_id).items():
            if field not in fields_def:
                continue
            if not fields_def[field].get('required', False):
                continue
            f_def = fields_def[field]
            f_type = f_def['type']
            field_module = self.get_field_base_module(cursor, uid, model, field)
            if field_module != module:
                if field_module not in res:
                    field_module_fields = []
                    res[field_module] = [{
                        'id': '{0}.{1}'.format(module, res_ref),
                        'model': model,
                        'fields': field_module_fields
                    }]
                data = field_module_fields  # sortcut
            else:
                data = res_module_fields  # sortcut
            if f_type == 'many2many':
                continue
            elif f_type == 'one2many':
                # relation = f_def['relation']
                # for o2mid in value:
                #     data_merge(res, self.export_model(
                #         cursor, uid, relation, o2mid, context=context
                #     ))
                continue
            elif f_type == 'many2one':
                if not value:
                    data.append({'name': field, 'value': False})
                else:
                    relation = f_def['relation']
                    ref = self.get_ref(cursor, uid, relation, value[0])
                    if ref: # si ja existeix la referencia al XML s'afegeix
                        data.append({'name': field, 'ref': ref})
                    else:
                        # si no existeix es crida recursivament per crear
                        # el nou xml
                        data_merge(res, self.export_model(
                            cursor, uid, relation, value[0], context=context
                        ))
            else:
                data.append({'name': field, 'value': value})
        if new_cursor:
            cursor.rollback()
        return res

DeveloperToolsExport()
