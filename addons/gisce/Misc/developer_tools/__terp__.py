# -*- coding: utf-8 -*-
{
    "name": "Developer tools",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Developer tools
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
