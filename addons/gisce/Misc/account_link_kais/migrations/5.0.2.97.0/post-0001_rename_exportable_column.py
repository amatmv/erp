# coding=utf-8

import logging
from oopgrade.oopgrade import column_exists


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')
    logger.info('Checking if exportable column migration is necessary...')
    if column_exists(cursor, 'account_journal', 'kais_exportable'):
        logger.info('Updating new exportable column...')
        q = '''
            UPDATE account_journal SET account_link_exportable = kais_exportable
        '''
        cursor.execute(q)
        logger.info('Exportable value updated succesfully. Droping unnecessary column...')
        q = '''
            ALTER TABLE account_journal DROP COLUMN  kais_exportable
        '''
        cursor.execute(q)
    else:
        logger.info('Migration no needed.')


def down(cursor, installed_version):
    pass


migrate = up
