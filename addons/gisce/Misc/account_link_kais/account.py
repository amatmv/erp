# coding=utf-8
from __future__ import absolute_import, unicode_literals
import re
from osv import osv, fields
from addons import get_module_resource


def fix_refunds_sign(data):
    for row in data:
        if row['invoice_type'] in ('out_refund', 'in_refund'):
            row['credit'], row['debit'] = -row['debit'], -row['credit']
            if row['base_iva']:
                row['base_iva'] *= -1
            if row['total_iva']:
                row['total_iva'] *= -1
        # Force if account is from 43, 44 group credit must be in debit
        # negative and 400 debit must be in credit negative
        elif not row['invoice_type']:
            if (re.match('^(43|44)', row['account_code']) and row['credit']
                    and row['kais_codi_cto'] == '01'):
                row['credit'], row['debit'] = -row['debit'], -row['credit']
            if (re.match('^(40)', row['account_code']) and row['debit']
                    and row['kais_codi_cto'] == '26'):
                row['credit'], row['debit'] = -row['debit'], -row['credit']


class AccountMoveLine(osv.osv):
    _name = 'account.move.line'
    _inherit = 'account.move.line'

    def extract_data(self, cursor, uid, ids, context=None):
        sql_file = get_module_resource(
            'account_link_kais', 'sql', 'extract_data.sql'
        )
        with open(sql_file, 'r') as f:
            sql = f.read()

        cursor.execute(sql, (tuple(ids), ))

        data = cursor.dictfetchall()
        fix_refunds_sign(data)
        return data


AccountMoveLine()


class AccountTax(osv.osv):
    _name = 'account.tax'
    _inherit = 'account.tax'

    _columns = {
        'kais_taxes_ids': fields.one2many(
            'account.link.kais.tax', 'tax_id', 'KAIS taxes'
        )

    }

AccountTax()


class AccountJournal(osv.osv):
    _name = 'account.journal'
    _inherit = 'account.journal'

    _columns = {
        'kais_codi_cto_id': fields.many2one(
            'account.link.kais.codicto',
            'KAIS Código Contable'
        ),
        'kais_taxes_ids': fields.one2many(
            'account.link.kais.tax', 'journal_id', 'KAIS taxes'
        )

    }

AccountJournal()
