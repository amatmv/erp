select
       ml.id as id,
       a.code as account_code,
       case when kt.kais_serie_iva is not null then p.name else a.name end as account_name,
       codicto.codigo as kais_codi_cto,
       codicto.descripcion as kais_codi_cto_desc,
       j.code as journal_code,
       i.type as invoice_type,
       ml.move_id as move_id,
       ml.ref as ref,
       ml.credit as credit,
       ml.debit as debit,
       ml.date as date,
       case when kt.kais_serie_iva is not null then kt.kais_serie_iva else  null end as kais_serie_iva,
       case when kt.kais_serie_iva is not null then kt.kais_tipo_base else  null end as kais_tipo_base,
       case when kt.kais_serie_iva is not null then kt.kais_annex_iva else  null end as kais_annex_iva,
       case when kt.kais_serie_iva is not null then tl.base else  null end as base_iva,
       case when kt.kais_serie_iva is not null then tl.amount else  null end as total_iva,
       case when kt.kais_serie_iva is not null then p.name else  null end as cliente,
       case when kt.kais_serie_iva is not null then p.vat else  null end as cliente_nif,
       case when kt.kais_serie_iva is not null then i.amount_total else  null end as total_factura,
       case when kt.kais_serie_iva is not null then t.amount * 100 else  null end as iva_perc,
       case when kt.kais_serie_iva is not null then pa.code else  null end as cliente_account
from
     account_move_line ml
       left join account_account a on ml.account_id = a.id
       left join account_move m on ml.move_id = m.id
       left join account_journal j on m.journal_id = j.id
       left join account_invoice i on ml.move_id = i.move_id
       left join account_invoice_tax tl on tl.invoice_id = i.id and tl.account_id = ml.account_id
       left join res_partner p on i.partner_id = p.id
       left join account_tax t on tl.tax_id = t.id
       left join account_link_kais_tax kt on kt.journal_id = j.id and kt.tax_id = t.id
       left join account_link_kais_codicto codicto on j.kais_codi_cto_id = codicto.id
       left join account_account pa on i.account_id = pa.id
where
       ml.id in %s
       and j.account_link_exportable = True
