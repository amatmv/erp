# -*- coding: utf-8 -*-
{
  "name": "Account Link KAIS",
  "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Exportación movimientos contables al sistema KAIS
""",
  "version": "0-dev",
  "author": "GISCE",
  "category": "Master",
  "depends": [
      'account',
      'account_link'
  ],
  "init_xml": [],
  "demo_xml": [],
  "update_xml": [
      'account_link_kais_view.xml',
      'account_view.xml',
      'wizard/export_account_moves_view.xml',
      'security/ir.model.access.csv',
  ],
  "active": False,
  "installable": True
}
