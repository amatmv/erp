# coding=utf-8
from datetime import datetime
from destral import testing
import netsvc


class TestExportingKais(testing.OOTestCaseWithCursor):

    def test_extract_data(self):
        cursor = self.txn.cursor
        uid = self.txn.user
        pool = self.openerp.pool

        ml_obj = pool.get('account.move.line')
        imd = pool.get('ir.model.data')

        invoice = imd._get_obj(
            cursor, uid, 'account', 'test_invoice_1'
        )
        lines_ids = [x.id for x in invoice.move_id.line_id]
        data = ml_obj.extract_data(cursor, uid, lines_ids)
        for row in data:
            if row['account_code'] == 'x 701000':
                self.assertGreater(row['credit'], 0)
                self.assertEqual(row['debit'], 0)
            if row['account_code'] == 'x 40000':
                self.assertGreater(row['debit'], 0)
                self.assertEqual(row['credit'], 0)

    def test_on_out_refund_invoice_should_be_in_debit_negative(self):
        cursor = self.txn.cursor
        uid = self.txn.user
        pool = self.openerp.pool

        imd = pool.get('ir.model.data')
        ml_obj = pool.get('account.move.line')
        inv_obj = pool.get('account.invoice')
        invoice = imd._get_obj(
            cursor, uid, 'account', 'test_invoice_1'
        )
        refund_invoice_id = invoice.refund()[0]
        refund_invoice = inv_obj.browse(cursor, uid, refund_invoice_id)
        wf_service = netsvc.LocalService("workflow")
        wf_service.trg_validate(
            uid, 'account.invoice', refund_invoice_id, 'invoice_open', cursor
        )

        lines_ids = [x.id for x in refund_invoice.move_id.line_id]
        data = ml_obj.extract_data(cursor, uid, lines_ids)

        for row in data:
            if row['account_code'] == 'x 701000':
                self.assertLess(row['credit'], 0)
                self.assertEqual(row['debit'], 0)
            if row['account_code'] == 'x 40000':
                self.assertLess(row['debit'], 0)
                self.assertEqual(row['credit'], 0)

    def test_on_in_refund_invoice_should_be_in_credit_negative(self):
        cursor = self.txn.cursor
        uid = self.txn.user
        pool = self.openerp.pool

        imd = pool.get('ir.model.data')
        ml_obj = pool.get('account.move.line')
        inv_obj = pool.get('account.invoice')
        invoice = imd._get_obj(
            cursor, uid, 'account', 'test_invoice_1'
        )
        in_invoice_id = inv_obj.copy(cursor, uid, invoice.id, {
            'type': 'in_invoice',
            'check_total': invoice.amount_total
        })
        wf_service = netsvc.LocalService("workflow")
        wf_service.trg_validate(
            uid, 'account.invoice', in_invoice_id, 'invoice_open', cursor
        )
        in_invoice = inv_obj.browse(cursor, uid, in_invoice_id)
        self.assertEqual(in_invoice.type, 'in_invoice')
        self.assertEqual(in_invoice.state, 'open')

        refund_in_invoice_id = in_invoice.refund()[0]
        refund_in_invoice = inv_obj.browse(cursor, uid, refund_in_invoice_id)
        refund_in_invoice.write({'check_total': refund_in_invoice.amount_total})
        wf_service = netsvc.LocalService("workflow")
        wf_service.trg_validate(
            uid, 'account.invoice', refund_in_invoice_id, 'invoice_open', cursor
        )

        lines_ids = [x.id for x in refund_in_invoice.move_id.line_id]
        data = ml_obj.extract_data(cursor, uid, lines_ids)

        for row in data:
            if row['account_code'] == 'x 701000':
                self.assertEqual(row['credit'], 0)
                self.assertLess(row['debit'], 0)
            if row['account_code'] == 'x 40000':
                self.assertEqual(row['debit'], 0)
                self.assertLess(row['credit'], 0)

    def test_extract_data_works_without_invoice(self):
        cursor = self.txn.cursor
        uid = self.txn.user
        pool = self.openerp.pool

        move_obj = pool.get('account.move')
        ml_obj = pool.get('account.move.line')
        period_obj = pool.get('account.period')
        imd_obj = pool.get('ir.model.data')

        name = 'Test move'
        amount = 50
        number = 'TEST0001'
        journal_id = imd_obj.get_object_reference(
            cursor, uid, 'account', 'sales_journal'
        )[1]
        partner_id = imd_obj.get_object_reference(
            cursor, uid, 'base', 'res_partner_asus'
        )[1]
        credit_account = imd_obj.get_object_reference(
            cursor, uid, 'account', 'a_sale'
        )[1]
        debit_account = imd_obj.get_object_reference(
            cursor, uid, 'account', 'a_recv'
        )[1]
        date_move = datetime.now().strftime('%Y-%m-%d')
        period_id = period_obj.find(cursor, uid, date_move)[0]

        l1 = [0, 0, {
            'name': name,
            'debit': amount,
            'credit': 0,
            'account_id': debit_account,
            'partner_id': partner_id,
            'ref': number,
            'date': date_move,
            'amount_currency': amount
        }]
        l2 = [0, 0, {
            'name': name,
            'debit': 0,
            'credit': amount,
            'account_id': credit_account,
            'partner_id': partner_id,
            'ref': number,
            'date': date_move,
            'amount_currency': amount,
        }]
        move_id = move_obj.create(cursor, uid, {
            'ref': number,
            'line_id': [l1, l2],
            'journal_id': journal_id,
            'period_id': period_id,
            'date': date_move
        })

        move = move_obj.browse(cursor, uid, move_id)

        lines_ids = [x.id for x in move.line_id]
        data = ml_obj.extract_data(cursor, uid, lines_ids)

        for row in data:
            if row['account_code'] == 'x 701000':
                self.assertEqual(row['credit'], 50)
                self.assertEqual(row['debit'], 0)
            if row['account_code'] == 'x 40000':
                self.assertEqual(row['debit'], 50)
                self.assertEqual(row['credit'], 0)
