# -*- coding: utf-8 -*-
try:
    from StringIO import StringIO
except ImportError:
    from cStringIO import StringIO
import base64
from datetime import datetime
import pandas as pd
from kais.export import AccountMoveLine

from osv import osv, fields


class WizardExportAccountMoves(osv.osv_memory):
    _name = 'wizard.export.account.moves'
    _inherit = 'wizard.export.account.moves'

    def export_account_moves_data(self, cursor, uid, wiz_id, data, context=None):

        if context is None:
            context = {}

        content = StringIO()
        detail_content = StringIO()
        wiz = self.browse(cursor, uid, wiz_id)

        data_df = pd.DataFrame(data)

        with pd.ExcelWriter(detail_content, engine='xlsxwriter') as writer:
            data_df.to_excel(writer, index=None, sheet_name='Detall')
            # pylint: disable=no-member
            if wiz.group_moves:
                grp_data_df = data_df.copy()
                grp_data_df.fillna({
                    'journal_code': '',
                    'account_code': '',
                    'account_name': '',
                    'kais_codi_cto': '',
                    'kais_codi_cto_desc': '',
                    'move_id': 0,
                    'ref': '',
                    'credit': 0,
                    'debit': 0,
                    'date': '',
                    'kais_serie_iva': '',
                    'kais_tipo_base': '',
                    'kais_annex_iva': '',
                    'base_iva': 0,
                    'cliente': '',
                    'cliente_nif': '',
                    'total_factura': 0,
                    'iva_perc': 0,
                    'cliente_account': ''
                }, inplace=True)

                grp_data_df['direccion'] = grp_data_df.apply(
                    lambda row: row['debit'] and 'D' or 'H', axis='columns'
                )
                grp_data_df['account_name'] = grp_data_df.apply(
                    lambda row: row['account_code'] == '477000000102' and 'DEUTORS REBUTS COMERCIALITZADORA' or row['account_name'], axis='columns'
                )
                grp_data_df['cliente'] = grp_data_df.apply(
                    lambda row: row['account_code'] == '477000000102' and 'DEUTORS REBUTS COMERCIALITZADORA' or row['cliente'], axis='columns'
                )
                grp_data_df['cliente_nif'] = grp_data_df.apply(
                    lambda row: row['account_code'] == '477000000102' and 'A17000837' or row['cliente_nif'], axis='columns'
                )

                grp_move_ids = grp_data_df.groupby([
                    'kais_codi_cto',
                    'date'
                ]).agg({
                    'move_id': 'min'
                }).reset_index()

                grp_data_df = grp_data_df.groupby([
                    'account_code',
                    'account_name',
                    'date',
                    'kais_codi_cto',
                    'kais_codi_cto_desc',
                    'kais_annex_iva',
                    'kais_serie_iva',
                    'kais_tipo_base',
                    'cliente',
                    'cliente_nif',
                    'iva_perc',
                    'cliente_account',
                    'direccion'
                ]).agg({
                    'id': 'min',
                    'credit': 'sum',
                    'debit': 'sum',
                    'base_iva': 'sum',
                    'total_iva': 'sum',
                    'total_factura': 'sum',
                    'ref': 'min'
                }).reset_index()

                grp_data_df = pd.merge(
                    grp_data_df,
                    grp_move_ids,
                    how='left',
                    on=['kais_codi_cto', 'date']
                )

                grp_data_df.sort_values(['kais_codi_cto', 'date'], inplace=True)

                grp_data_df.to_excel(writer, index=None, sheet_name='Agrupat')
                data_df = grp_data_df

        for _, line in data_df.iterrows():
            computed_date = datetime.strptime(line['date'], '%Y-%m-%d').date()
            content.write(
                AccountMoveLine(
                    data_assent=computed_date,
                    data_doc=computed_date,
                    documentoampliado=line['ref'],
                    codi_cto=line['kais_codi_cto'],
                    descr_cto=line['kais_codi_cto_desc'],
                    deure=line['debit'],
                    haver=line['credit'],
                    codi_compte=line['account_code'],
                    diari="01",
                    divisa="001",
                    codigopaiscliente="001",
                    tipo_base1=line['kais_tipo_base'],
                    perc_iva1=line['iva_perc'],
                    base1=line['base_iva'],
                    imp_iva1=line['total_iva'],
                    annex_iva=line['kais_annex_iva'],
                    serie_iva=line['kais_serie_iva'],
                    nom_prov=line['cliente'],
                    nifampliado=line['cliente_nif'],
                    num_provisional=line['move_id'],
                    total_fra=line['total_factura'],
                    descr_compte=line['account_name'],
                    cta_prv_iva=line['cliente_account']
                ).to_record() + '\n'
            )
        # pylint: disable=no-member
        wiz.write({
            'kais_file': base64.b64encode(content.getvalue()),
            'kais_file_name': 'KAIS.txt',
            'detail_file': base64.b64encode(detail_content.getvalue()),
            'detail_file_name': 'KAIS_detall.xlsx',
            'state': 'end'
        })

    _columns = {
        'kais_file_name': fields.char('Nom arxiu KAIS', size=256),
        'kais_file': fields.binary('Arxiu KAIS'),
        'detail_file_name': fields.char('Nom arxiu detall', size=256),
        'detail_file': fields.binary('Arxiu detall'),
        'group_moves': fields.boolean('Agrupar moviments')
    }

    _defaults = {
        'group_moves': lambda *a: True,
    }


WizardExportAccountMoves()
