# coding=utf-8
from osv import osv, fields


class AccountLinkKaisCodicto(osv.osv):
    _name = 'account.link.kais.codicto'

    def name_get(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        res = []
        for cto in self.read(cursor, uid, ids, context=context):
            res.append(
                (cto['id'], '{codigo} - {descripcion}'.format(**cto))
            )
        return res

    _columns = {
        'codigo': fields.char('Código Contable', size=2, required=True),
        'descripcion': fields.char('Descripción', size=30, required=True)
    }

AccountLinkKaisCodicto()


class AccountLinkKaisTax(osv.osv):
    _name = 'account.link.kais.tax'

    _columns = {
        'journal_id': fields.many2one(
            'account.journal', 'Journal', required=True
        ),
        'tax_id': fields.many2one(
            'account.tax', 'Tax', required=True
        ),
        'kais_serie_iva': fields.char('KAIS Serie IVA', size=2),
        'kais_tipo_base': fields.char('KAIS Tipo base', size=1),
        'kais_annex_iva': fields.char('KAIS Annex IVA', size=2)
    }

AccountLinkKaisTax()
