# *-* coding: utf-8 *-*
"""Camp CNAE pels partners."""
from osv import fields, osv

class ResPartner(osv.osv):
    """Afegim el camp CNAE al partner."""
    _name = 'res.partner'
    _inherit = 'res.partner'

    _columns = {
      'cnae': fields.many2one('giscemisc.cnae', 'CNAE'),
    }

ResPartner()
