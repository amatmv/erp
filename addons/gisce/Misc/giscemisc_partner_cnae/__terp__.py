# -*- coding: utf-8 -*-
{
    "name": "Camp CNAE pel Partner",
    "description": """Afegeix el camp CNAE per un partner""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Partner",
    "depends":[
        "base",
        "giscemisc_cnae"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscemisc_partner_cnae_view.xml"
    ],
    "active": False,
    "installable": True
}
