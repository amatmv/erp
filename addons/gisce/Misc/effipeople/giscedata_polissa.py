# -*- coding: utf-8 -*-
from datetime import datetime

import netsvc
from osv import osv
from tools import config
from tools.translate import _
from errors import log_sync_error
from effipy import EffiPeople
from libsaas import http
from oorq.decorators import job
from convert import datestring_to_iso, datestring_to_epoch, false_to_none


class GiscedataPolissa(osv.osv):
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def wkf_activa(self, cursor, uid, ids):
        # Search for reactivations
        react = []
        for polissa in self.browse(cursor, uid, ids):
            if not polissa.modcontractual_activa.active:
                react.append(polissa.id)
        # Call the parent
        res = super(GiscedataPolissa, self).wkf_activa(cursor, uid, ids)
        # Do the normals
        ids = list(set(ids) - set(react))
        if ids:
            self.effi_cou(cursor, uid, ids)
        # Reactivations
        if react:
            self.effi_cou(cursor, uid, react, {'react': True})
        return res

    def wkf_baixa(self, cursor, uid, ids):
        # Call the parent
        res = super(GiscedataPolissa, self).wkf_baixa(cursor, uid, ids)
        self.effi_delete(cursor, uid, ids)
        return res

    @job(queue='effipeople')
    def effi_cou(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        ef = EffiPeople(config['effipeople_token'],
                        config.get('effipeople_version', 'v1'),
                        config.get('effipeople_test', False))
        res = []
        for polissa in self.browse(cursor, uid, ids):
            data = polissa.effi_data()[0]
            # Check if customer exists
            customer = polissa.pagador
            customer.effi_cou({'address_id': polissa.direccio_pagament.id})
            p_data = customer.effi_data(
                {'address_id': polissa.direccio_pagament.id}
            )[0]
            # Check CUPS
            polissa.cups.effi_cou()
            try:
                endpoint = ef.customer(data['customerId']).contract(data['id'])
                # If is a reactivation we neet to send all the versions
                if context.get('react', False):
                    mods_to_send = [m.id for m in polissa.modcontractuals_ids]
                else:
                    mods_to_send = [polissa.modcontractual_activa.id]
                for mod in reversed(mods_to_send):
                    ctx = context.copy()
                    ctx['modcon_id'] = mod
                    data = polissa.effi_data(context=ctx)[0]
                    x = endpoint.version(data['version']).update(data)
                    res.append(x)
            except http.HTTPError, err:
                if err.code == 404:
                    x = ef.customer(p_data['id']).contracts().create(data)
                    res.append(x)
                else:
                    log_sync_error(err)
                    raise

    @job(queue='effipeople')
    def effi_delete(self, cursor, uid, ids, context=None):
        ef = EffiPeople(config['effipeople_token'],
                        config.get('effipeople_version', 'v1'),
                        config.get('effipeople_test', False))
        res = []
        for data in self.effi_data(cursor, uid, ids, context):
            ef.customer(data['customerId']).contract(data['id']).delete()

    def effi_data(self, cursor, uid, ids, context=None):
        """Convert a contract to a EffiPeople format

        {
          "id": "IdentificadorContrato",
          "customerId": "IdentificadorCliente",
          "start": 1332806400,
          "end": 1362009600,
          "tariffId": "2.0A",
          "power": 3300,
          "version": "2",
          "activityCode": "CNAE",
          "usagePointId": "ES0987543210987654ZF",
          "customerType": null
        }
        """
        modcon_obj = self.pool.get('giscedata.polissa.modcontractual')
        if not context:
            context = {}
        res = []
        for polissa in self.browse(cursor, uid, ids, context):
            if 'modcon_id' in context:
                modcon = modcon_obj.browse(cursor, uid, context['modcon_id'])
            else:
                modcon = polissa.modcontractual_activa
            res.append(false_to_none({
                'id': polissa.name,
                'customerId': modcon.pagador.id,
                'start': datestring_to_epoch(modcon.data_inici),
                'end': datestring_to_epoch(modcon.data_final),
                'tariffId': modcon.llista_preu.id,
                'power': int(modcon.potencia * 1000),
                'version': modcon.name,
                'activityCode': modcon.cnae.name,
                'usagePointId': modcon.cups.name,
            }, context))
        return res

GiscedataPolissa()


class GiscedataPolissaModcontractual(osv.osv):
    _name = 'giscedata.polissa.modcontractual'
    _inherit = 'giscedata.polissa.modcontractual'

    def write(self, cursor, uid, ids, vals, context=None):
        res = super(GiscedataPolissaModcontractual,
                    self).write(cursor, uid, ids, vals, context)
        # Només actualizem les modificacions contractuals si és
        # tema de dates
        if set(('data_inici', 'data_final')) & set(vals.keys()):
            self.effi_cou(cursor, uid, ids, context)
        return res

    @job(queue='effipeople')
    def effi_cou(self, cursor, uid, ids, context=None):
        ef = EffiPeople(config['effipeople_token'],
                        config.get('effipeople_version', 'v1'),
                        config.get('effipeople_test', False))
        res = []
        for data in self.effi_data(cursor, uid, ids, context):
            endpoint = ef.customer(data['customerId']).contract(data['id'])
            x = endpoint.version(data['version']).update(data)
            res.append(x)
        return res

    def effi_data(self, cursor, uid, ids, context=None):
        if not isinstance(ids, (tuple, list)):
            ids = [ids]
        if not context:
            context = {}
        res = []
        for modcon in self.browse(cursor, uid, ids, context):
            # Don't sync inactive contracts
            if not modcon.polissa_id.active:
                continue
            ctx = context.copy()
            ctx.update({'modcon_id': modcon.id})
            res += modcon.polissa_id.effi_data(context=ctx)
        return res

GiscedataPolissaModcontractual()
