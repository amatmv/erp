# -*- coding: utf-8 -*-

from osv import osv
from tools import cache, config
from convert import datestring_to_epoch, false_to_none, fix_float
from oorq.decorators import job
from errors import log_sync_error
from effipy import EffiPeople
from libsaas import http



class ProductPricelist(osv.osv):
    _name = 'product.pricelist'
    _inherit = 'product.pricelist'

    def write(self, cursor, uid, ids, vals, context=None):
        res = super(ProductPricelist, self).write(cursor, uid, ids, vals,
                                                   context)
        if 'active' in vals and not vals['active']:
            self.effi_delete(cursor, uid, ids, context)
        return res

    @job(queue='effipeople')
    def effi_delete(self, cursor, uid, ids, context=None):
        ef = EffiPeople(config['effipeople_token'],
                        config.get('effipeople_version', 'v1'),
                        config.get('effipeople_test', False))
        res = []
        for t_id in ids:
            ef.tariff(t_id).delete()

    @cache()
    def get_tarifa_type(self, cursor, uid, ids, context=None):
        ftr = ['llistes_preus_comptatibles', 'name']
        for pricelist in self.browse(cursor, uid, ids, context):
            tarifa_obj = self.pool.get('giscedata.polissa.tarifa')
            tar_ids = tarifa_obj.search(cursor, uid, [], context=context)
            for tarifa in tarifa_obj.read(cursor, uid, tar_ids, ftr, context):
                if pricelist.id in tarifa['llistes_preus_comptatibles']:
                    return (tarifa['id'], tarifa['name'])
            return (False, False)

ProductPricelist()


class ProductPricecelistVersion(osv.osv):
    """Hooks effipeople para las listas de precios.
    """
    _name = 'product.pricelist.version'
    _inherit = 'product.pricelist.version'

    def create(self, cursor, uid, vals, context=None):
        res_id = super(ProductPricecelistVersion,
                       self).create(cursor, uid, vals, context)
        self.effi_cou(cursor, uid, [res_id], context)
        return res_id

    def write(self, cursor, uid, ids, vals, context=None):
        if not context:
            context = {}
        res = super(ProductPricecelistVersion,
                    self).write(cursor, uid, ids, vals, context)
        if 'active' in vals and not vals['active']:
            ctx = context.copy()
            ctx['delete'] = True
            self.effi_delete(cursor, uid, ids, ctx)
        else:
            self.effi_cou(cursor, uid, ids, context)
        return res

    @job(queue='effipeople')
    def effi_delete(self, cursor, uid, ids, context=None):
        ef = EffiPeople(config['effipeople_token'],
                        config.get('effipeople_version', 'v1'),
                        config.get('effipeople_test', False))
        res = []
        for data in self.effi_data(cursor, uid, ids, context):
            ef.tariff(data['id']).version(data['version']).delete()

    @job(queue='effipeople')
    def effi_cou(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        ef = EffiPeople(config['effipeople_token'],
                        config.get('effipeople_version', 'v1'),
                        config.get('effipeople_test', False))
        res = []
        for version in self.browse(cursor, uid, ids):
            data = version.effi_data()[0]
            endpoint = ef.tariff(data['id']).version(data['version'])
            try:
                x = endpoint.update(data)
                res.append(x)
            except http.HTTPError, err:
                if err.code == 404:
                    x = ef.tariffs().create(data)
                    res.append(x)
                else:
                    log_sync_error(err)
                    raise

    def effi_data(self, cursor, uid, ids, context=None):
        """Converts a pricelist version to effipeople data.

        {
          "id": "2.0DHA",
          "name": "Luz 10 (2.0A)",
          "tariffType": "2.0DHA",
          "version": "2",
          "start": 1363301230,
          "end": 1363304568,
          "period1Prices": {
            "activeEnergyPrice": 0.167658,
            "powerPrice": 1.824432
          },
          "period2Prices": {
            "activeEnergyPrice": 0.05719,
            "powerPrice": 1.824432
          }
        }
        """
        tar_obj = self.pool.get('giscedata.polissa.tarifa')
        if not context:
            context = {}
        res = []
        for version in self.browse(cursor, uid, ids, context):
            periodes = {}
            tarifa_id, tarifa_type = version.pricelist_id.get_tarifa_type()
            if tarifa_id and not context.get('delete', False):
                price_get = version.pricelist_id.price_get
                ctx = {'date': version.date_start}
                tarifa = tar_obj.browse(cursor, uid, tarifa_id)
                for p in tarifa.periodes:
                    # No fem cas dels períodes agrupats
                    if p.agrupat_amb:
                        continue
                    period_key = p.name.lower()
                    periodes.setdefault(period_key, {})
                    if p.tipus == 'te':
                        price_key = 'activeEnergyPrice'
                    elif p.tipus == 'tp':
                        price_key = 'powerPrice'
                    price =  price_get(p.product_id.id, 1,
                                        context=ctx)[version.pricelist_id.id]
                    periodes[period_key].update({
                        price_key: fix_float(price, 6)
                    })
            res.append(false_to_none({
              'id': version.pricelist_id.id,
              'name': version.pricelist_id.name,
              'tariffType': tarifa_type,
              'version': version.name,
              'start': datestring_to_epoch(version.date_start),
              'end': datestring_to_epoch(version.date_end),
              'periods': periodes
            }, context))
        return res

ProductPricecelistVersion()
