# -*- coding: utf-8 -*-
{
    "name": "EffiPeople API",
    "description": """
    This module provide :
        * Integration between EffiPeople service
    """,
    "version": "0-dev",
    "author": "GISCE-TI, S.L.",
    "category": "Misc",
    "depends":[
        "base",
        "giscedata_polissa_comer",
        "giscedata_facturacio_comer",
        "giscedata_lectures_comer",
        "giscedata_cups_comer",
        "oorq"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
