# -*- coding: utf-8 -*-

from osv import osv
from tools import config
from convert import datestring_to_epoch
from errors import log_sync_error
from convert import false_to_none
from effipy import EffiPeople
from libsaas import http
from oorq.decorators import job


LECTURES_TIPUS = {
    'A': 'Active',
    'R': 'Reactive',
    'P': 'Power'
}

class GiscedataLecturesComptador(osv.osv):

    _name = 'giscedata.lectures.comptador'
    _inherit = 'giscedata.lectures.comptador'

    def write(self, cursor, uid, ids, vals, context=None):
        res = super(GiscedataLecturesComptador,
                    self).write(cursor, uid, ids, vals, context)
        if 'active' in vals and not vals['active']:
            self.effi_delete(cursor, uid, ids, context)
        else:
            self.effi_cou(cursor, uid, ids, context)
        return res

    def create(self, cursor, uid, vals, context=None):
        res_id = super(GiscedataLecturesComptador, self).create(cursor, uid,
                                                              vals, context)
        self.effi_cou(cursor, uid, [res_id], context)
        return res_id

    @job(queue='effipeople')
    def effi_delete(self, cursor, uid, ids, context=None):
        ef = EffiPeople(config['effipeople_token'],
                        config.get('effipeople_version', 'v1'),
                        config.get('effipeople_test', False))
        res = []
        for data in self.effi_data(cursor, uid, ids, context):
            ef.usagepoint(data['usagePointId']).meter(data['id']).delete()

    @job(queue='effipeople')
    def effi_cou(self, cursor, uid, ids, context=None):
        """Create or Update measure in EffiPeople.
        """
        data = self.effi_data(cursor, uid, ids, context)
        ef = EffiPeople(config['effipeople_token'],
                        config.get('effipeople_version', 'v1'),
                        config.get('effipeople_test', False))
        res = []
        for comptador in self.browse(cursor, uid, ids, context):
            ef_data = comptador.effi_data()[0]
            try:
                # Check CUPS
                comptador.polissa.cups.effi_cou()
                cups_id = comptador.polissa.cups.effi_data()[0]['id']
                ef.usagepoint(cups_id).meter(ef_data['id']).get()
            except http.HTTPError, err:
                if err.code == 404:
                    res.append(ef.usagepoint(cups_id).meters().create(ef_data))
                else:
                    log_sync_error(err)
                    raise
        return res

    def effi_data(self, cursor, uid, ids, context=None):
        """Convert a meter to EffiPeople Format.

        {
          "id": "sample string 1",
          "usagePointId": "sample string 2"
        }
        """
        if not context:
            context = {}
        res = []
        for comptador in self.browse(cursor, uid, ids, context):
            res.append(false_to_none({
                'id': comptador.name,
                'usagePointId': comptador.polissa.cups.name,
            }, context))
        return res

#GiscedataLecturesComptador()


class GiscedataLecturesLectura(osv.osv):
    _name = 'giscedata.lectures.lectura'
    _inherit = 'giscedata.lectures.lectura'

    def write(self, cursor, uid, ids, vals, context=None):
        res = super(GiscedataLecturesLectura, self).write(cursor, uid, ids,
                                                          vals, context)
        self.effi_cou(cursor, uid, ids, context)
        return res

    def create(self, cursor, uid, vals, context=None):
        if not context:
            context = {}
        res_id = super(GiscedataLecturesLectura, self).create(cursor, uid,
                                                              vals, context)
        if context.get('trigger', False):
            self.effi_cou(cursor, uid, [res_id], context)
        return res_id

    @job(queue='effipeople')
    def effi_cou(self, cursor, uid, ids, context=None):
        """Create or Update measure in EffiPeople.
        """
        data = self.effi_data(cursor, uid, ids, context)
        ef = EffiPeople(config['effipeople_token'],
                        config.get('effipeople_version', 'v1'),
                        config.get('effipeople_test', False))
        res = []
        for lectura in self.browse(cursor, uid, ids, context):
            ef_data = lectura.effi_data()[0]
            try:
                # Check CUPS
                lectura.comptador.polissa.cups.effi_cou()
                # Check Meter
                lectura.comptador.effi_cou()
                res.append(ef.measure(ef_data['id']).update(ef_data))
            except http.HTTPError, err:
                if err.code == 404:
                    res.append(ef.measures().create(ef_data))
                else:
                    log_sync_error(err)
                    raise
        return res

    def effi_data(self, cursor, uid, ids, context=None):
        """Convert a measure to EffiPeople Format.

        {
          "id": null,
          "usagePointId": "ES0987543210987654ZF",
          "meterId": "M20300335",
          "period": 1,
          "date": 1363301230,
          "reading": 3030,
          "consumption": 70,
          "duration": 90061,
          "tariffId": "2.0A",
          "cost": 0.1734,
          "measureType": "Active",
          "source": "Visual",
          "isProvisional": "true",
        }
        """
        if not context:
            context = {}
        res = []
        for lectura in self.browse(cursor, uid, ids, context):
            res.append(false_to_none({
                'id': 'L%s' % lectura.id,
                'usagePointId': lectura.comptador.polissa.cups.name,
                'meterId': lectura.comptador.name,
                'period': lectura.periode.name[-1],
                'date': datestring_to_epoch(lectura.name),
                'reading': lectura.lectura,
                'consumption': lectura.consum,
                'duration': 0,
                'tariffId': lectura.periode.tarifa.name,
                'cost': 0,
                'measureType': LECTURES_TIPUS[lectura.tipus],
                'source': lectura.origen_id.name,
                'isProvisional': True,
            }, context))
        return res

#GiscedataLecturesLectura()


class GiscedataLecturesPotencia(osv.osv):
    _name = 'giscedata.lectures.potencia'
    _inherit = 'giscedata.lectures.potencia'

    def write(self, cursor, uid, ids, vals, context=None):
        res = super(GiscedataLecturesPotencia, self).write(cursor, uid, ids,
                                                          vals, context)
        self.effi_cou(cursor, uid, ids, context)
        return res

    def create(self, cursor, uid, vals, context=None):
        res_id = super(GiscedataLecturesPotencia, self).create(cursor, uid,
                                                              vals, context)
        self.effi_cou(cursor, uid, [res_id], context)
        return res_id

    @job(queue='effipeople')
    def effi_cou(self, cursor, uid, ids, context=None):
        """Create or Update measure in EffiPeople.
        """
        data = self.effi_data(cursor, uid, ids, context)
        ef = EffiPeople(config['effipeople_token'],
                        config.get('effipeople_version', 'v1'),
                        config.get('effipeople_test', False))
        res = []
        for lectura in self.browse(cursor, uid, ids, context):
            ef_data = lectura.effi_data()[0]
            try:
                # Check CUPS
                lectura.comptador.polissa.cups.effi_cou()
                # Check Meter
                lectura.comptador.effi_cou()
                res.append(ef.measure(ef_data['id']).update(ef_data))
            except http.HTTPError, err:
                if err.code == 404:
                    res.append(ef.measures().create(ef_data))
                else:
                    log_sync_error(err)
                    raise
        return res

    def effi_data(self, cursor, uid, ids, context=None):
        """Convert a measure to EffiPeople Format.

        {
          "id": null,
          "usagePointId": "ES0987543210987654ZF",
          "meterId": "M20300335",
          "period": 1,
          "date": 1363301230,
          "reading": 3030,
          "consumption": 70,
          "duration": 90061,
          "tariffId": "2.0A",
          "cost": 0.1734,
          "measureType": "Active",
          "source": "Visual",
          "isProvisional": true
        }
        """
        if not context:
            context = {}
        res = []
        for lectura in self.browse(cursor, uid, ids, context):
            res.append(false_to_none({
                'id': 'P%s' % lectura.id,
                'usagePointId': lectura.comptador.polissa.cups.name,
                'meterId': lectura.comptador.name,
                'period': lectura.periode.name[-1],
                'date': datestring_to_epoch(lectura.name),
                'reading': lectura.lectura,
                'consumption': 0,
                'duration': 0,
                'tariffId': lectura.periode.tarifa.name,
                'cost': 0,
                'measureType': LECTURES_TIPUS['P'],
                'source': '',
                'isProvisional': True,
            }, context))
        return res

#GiscedataLecturesPotencia()
