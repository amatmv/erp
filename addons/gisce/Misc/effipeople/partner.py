# -*- coding: utf-8 -*-

from osv import osv
from tools import config
from errors import log_sync_error
from convert import false_to_none

from effipy import EffiPeople
from libsaas import http
from oorq.decorators import job


class ResPartner(osv.osv):
    _name = 'res.partner'
    _inherit = 'res.partner'

    def write(self, cursor, uid, ids, vals, context=None):
        res = super(ResPartner, self).write(cursor, uid, ids, vals, context)
        if 'active' in vals and not vals['active']:
            self.effi_delete(cursor, uid, ids, context)
        else:
            self.effi_cou(cursor, uid, ids, context)
        return res

    @job(queue='effipeople')
    def effi_delete(self, cursor, uid, ids, context=None):
        ef = EffiPeople(config['effipeople_token'],
                        config.get('effipeople_version', 'v1'),
                        config.get('effipeople_test', False))
        res = []
        for data in self.effi_data(cursor, uid, ids, context):
            ef.customer(data['id']).delete()

    @job(queue='effipeople')
    def effi_cou(self, cursor, uid, ids, context=None):
        """Create or Update customer in EffiPeople.
        """
        data = self.effi_data(cursor, uid, ids, context)
        ef = EffiPeople(config['effipeople_token'],
                        config.get('effipeople_version', 'v1'),
                        config.get('effipeople_test', False))
        res = []
        for p_data in data:
            try:
                res.append(ef.customer(p_data['id']).update(p_data))
            except http.HTTPError, err:
                if err.code == 404:
                    res.append(ef.customers().create(p_data))
                else:
                    log_sync_error(err)
                    raise
        return res

    def effi_data(self, cursor, uid, ids, context=None):
        """Convert a partner to EffiPeople Format.

        {
          "id": "sample string 1",
          "fiscalId": "sample string 2",
          "firstName": "sample string 3",
          "firstSurname": "sample string 4",
          "secondSurname": "sample string 5",
          "email": "sample string 6",
          "defaultLanguage": "ca",
          "address": {
            "street": "sample string 1",
            "postalCode": "sample string 2",
            "city": "sample string 3",
            "cityCode": "sample string 4",
            "province": "sample string 5",
            "provinceCode": "sample string 6",
            "country": "sample string 7",
            "countryCode": "sample string 8",
            "parcelNumber": "sample string 9"
          }
        }
        """
        if not isinstance(ids, (tuple, list)):
            ids = [ids]
        addr_obj = self.pool.get('res.partner.address')
        if not context:
            context = {}
        res = []
        for partner in self.browse(cursor, uid, ids, context):
            vat = len(partner.vat) == 9 and partner.vat or partner.vat[2:]
            if (vat[0] not in ('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                               'J', 'U', 'V', 'N', 'P', 'Q', 'R', 'S', 'W')
                    and ',' in partner.name):
                first_name = partner.name.split(',')[-1].strip()
                first_surname = ' '.join([
                    x.strip() for x in partner.name.split(',')[:-1]
                ])
            else:
                first_name = partner.name
                first_surname = ''
            if 'address_id' in context:
                addr = addr_obj.browse(cursor, uid, context['address_id'])
            else:
                addr = partner.address[0]
            if partner.lang:
                lang = partner.lang.split('_')[0]
            else:
                lang = False
            res.append(false_to_none({
                'id': partner.id,
                'fiscalId': vat,
                'firstName': first_name,
                'firstSurname': first_surname,
                'email': addr.email,
                'defaultLanguage': lang,
                'address': {
                    'street': addr.nv,
                    'postalCode': addr.zip,
                    'city': addr.id_municipi.name,
                    'cityCode': addr.id_municipi.ine,
                    'province': addr.state_id.name,
                    'provinceCode': addr.state_id.code,
                    'country': addr.country_id.name,
                    'countryCode': addr.country_id.code,
                    'parcelNumber': addr.pnp
                }
            }, context))
        return res

ResPartner()
