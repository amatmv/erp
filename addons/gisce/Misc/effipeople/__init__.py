# -*- coding: utf-8 -*-
import os
from tools import config
from tools.translate import _
from osv import osv

if (not config.get('effipeople_token', False)
        and not os.getenv('EFFIPEOPLE_TOKEN', False)):
    raise osv.except_osv(
        _('Error'),
        _('You must configure effipeople token in your config file')
    )
import partner
import giscedata_polissa
import giscedata_cups
import giscedata_facturacio
import pricelist
