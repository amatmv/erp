# -*- coding: utf-8 -*-
import re

from convert import duration, get_lectura_periode, datestring_to_epoch
from convert import false_to_none, fix_float
from .giscedata_lectures import LECTURES_TIPUS 
from osv import osv
from tools import config
from oorq.decorators import job
from errors import log_sync_error
from effipy import EffiPeople
from libsaas import http


class GiscedataFacturacioFactura(osv.osv):
    _name = 'giscedata.facturacio.factura'
    _inherit = 'giscedata.facturacio.factura'

    def is_estimated(self, cursor, uid, ids, context=None):
        """Try to guess if a measurement is estimated or not.
        """
        lect_obj = self.pool.get('giscedata.lectures.lectura')
        for factura in self.browse(cursor, uid, ids, context):
            dates = [
                l.data_actual for l in factura.lectures_energia_ids
                    if l.magnitud == 'AE'
            ]
            compts = factura.polissa_id.comptadors_actius(factura.data_inici,
                                                          factura.data_final)
            if dates:
                lectures = lect_obj.search(cursor, uid, [
                    ('name', 'in', dates),
                    ('comptador.id', 'in', compts)
                ], context={'active_test': False})
                for lectura in lect_obj.browse(cursor, uid, lectures):
                    if lectura.origen_id.codi == '40':
                        return True
            return False

    def invoice_open(self, cursor, uid, ids, context=None):
        """Confirm measures to EffiPeople platform.
        """
        res = super(GiscedataFacturacioFactura,
                     self).invoice_open(cursor, uid, ids, context)
        # Només volem les factures de client d'energia
        tipus = {'A': [], 'N': [], 'R': [], 'B': []}
        for factura in self.read(cursor, uid, ids, ['tipo_rectificadora',
                                                    'type']):
            if not factura['type'].startswith('out_'):
                continue
            tipus[factura['tipo_rectificadora']].append(factura['id'])
        del_ids = tipus['A'] + tipus['B']
        if del_ids:
            for del_id in [x['ref'][0]
                       for x in self.read(cursor, uid, del_ids, ['ref'])]:
                self.effi_delete(cursor, uid, [del_id], context)
        cou_ids = tipus['N'] + tipus['R']
        for cou_id in cou_ids:
            self.effi_cou(cursor, uid, [cou_id], context)
        return res

    @job(queue='effipeople')
    def effi_delete(self, cursor, uid, ids, context=None):
        ef = EffiPeople(config['effipeople_token'],
                        config.get('effipeople_version', 'v1'),
                        config.get('effipeople_test', False))
        res = []
        for data in self.effi_data(cursor, uid, ids, context):
            c = ef.customer(data['customerId']).contract(data['contractId'])
            c.invoice(data['number']).delete()

    @job(queue='effipeople')
    def effi_cou(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        ef = EffiPeople(config['effipeople_token'],
                        config.get('effipeople_version', 'v1'),
                        config.get('effipeople_test', False))
        res = []
        for factura in self.browse(cursor, uid, ids, context):
            data = factura.effi_data()[0]
            # Check contract exists
            factura.polissa_id.effi_cou()
            customer = ef.customer(factura.partner_id.id)  
            contract = customer.contract(factura.polissa_id.name)
            try:
                x = contract.invoice(factura.number).update(data)
                res.append(x)
            except http.HTTPError, err:
                if err.code == 404:
                    x = contract.invoices().create(data)
                    res.append(x)
                else:
                    log_sync_error(err)
                    raise
    
    def effi_data(self, cursor, uid, ids, context=None):
        """Convert an invoice to a EffiPeople format

        {
          "number": "Número de factura",
          "customerId": "Identificador único del cliente",
          "contractId": "Identificador único del contrato",
          "usagePointId": "ES0987543210987654ZF",
          "activityCode": "Código CNAE",
          "tariffId": "2.0A",
          "power": 5500,
          "activeEnergyConsumption": 400,
          "reactiveEnergyConsumption": 225,
          "isConsumptionEstimated": false,
          "activeEnergyCost": 200.0,
          "reactiveEnergyCost": 100.0,
          "powerCost": 300.0,
          "servicesCost": 75.0,
          "rentalsCost": 100.0,
          "otherCost": 50.0,
          "subtotal": 825.0,
          "otherTaxes": 6.75,
          "currencyCode": "Euro",
          "invoiceDate": 1363301230,
          "periodStart": 1363301020,
          "periodEnd": 13633012000,
          "vat": 173.25,
          "totalTaxes": 180.0,
          "total": 1005.0,
          "tariffType": 1
        }
        """
        pol_obj = self.pool.get('giscedata.polissa')
        if not context:
            context = {}
        res = []
        for factura in self.browse(cursor, uid, ids, context):
            polissa = pol_obj.browse(cursor, uid, factura.polissa_id.id,
                                     {'date': factura.data_inici})
            activa = sum([l.quantity for l in factura.linies_energia])
            reactiva = sum([l.quantity for l in factura.linies_reactiva])
            other = (factura.amount_untaxed - factura.total_energia -
                     factura.total_reactiva - factura.total_lloguers
                     - factura.total_potencia)
            if other < 0:
                other = 0
            vat = sum([t.amount for t in factura.tax_line
                       if t.name.startswith('IVA')])
            other_taxes = factura.amount_tax - vat
            if other_taxes < 0:
                other_taxes = 0
            ctx = context.copy()
            ctx['skip'] = ['isConsumptionEstimated']
            res.append(false_to_none({
                'number': factura.number,
                'customerId': factura.partner_id.id,
                'contractId': factura.polissa_id.name,
                'usagePointId': factura.cups_id.name,
                'activityCode': polissa.cnae.name,
                'tariffId': factura.llista_preu.id,
                'tariffType': factura.llista_preu.get_tarifa_type()[1],
                'power': int(factura.potencia * 1000),
                'activeEnergyConsumption': activa,
                'reactiveEnergyConsumption': reactiva,
                'isConsumptionEstimated': factura.is_estimated(),
                'activeEnergyCost': fix_float(factura.total_energia, 2),
                'reactiveEnergyCost': fix_float(factura.total_reactiva, 2),
                'powerCost': fix_float(factura.total_potencia, 2),
                'servicesCost': 0,
                'rentalsCost': fix_float(factura.total_lloguers, 2),
                'otherCost': fix_float(other, 2),
                'subtotal': fix_float(factura.amount_untaxed, 2),
                'otherTaxes': fix_float(other_taxes, 2),
                'currencyCode': 'Euro',
                'invoiceDate': datestring_to_epoch(factura.date_invoice),
                'periodStart': datestring_to_epoch(factura.data_inici),
                'periodEnd': datestring_to_epoch(factura.data_final),
                'vat': fix_float(vat, 2),
                'totalTaxes': fix_float(factura.amount_tax, 2),
                'total': fix_float(factura.amount_total, 2)
            }, ctx))
        return res

GiscedataFacturacioFactura()
