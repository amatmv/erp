# -*- coding: utf-8 -*-
from osv import osv
from tools import config
from errors import log_sync_error
from convert import false_to_none
from effipy import EffiPeople
from libsaas import http
from oorq.decorators import job


class GiscedataCupsPs(osv.osv):
    _name = 'giscedata.cups.ps'
    _inherit = 'giscedata.cups.ps'

    def write(self, cursor, uid, ids, vals, context=None):
        res = super(GiscedataCupsPs, self).write(cursor, uid, ids, vals,
                                                 context)
        if 'active' in vals and not vals['active']:
            self.effi_delete(cursor, uid, ids, context)
        else:
            self.effi_cou(cursor, uid, ids, context)
        return res

    @job(queue='effipeople')
    def effi_delete(self, cursor, uid, ids, context=None):
        ef = EffiPeople(config['effipeople_token'],
                        config.get('effipeople_version', 'v1'),
                        config.get('effipeople_test', False))
        res = []
        for data in self.effi_data(cursor, uid, ids, context):
            ef.usagepoint(data['id']).delete()

    @job(queue='effipeople')
    def effi_cou(self, cursor, uid, ids, context=None):
        data = self.effi_data(cursor, uid, ids, context)
        ef = EffiPeople(config['effipeople_token'],
                        config.get('effipeople_version', 'v1'),
                        config.get('effipeople_test', False))
        res = []
        for p_data in data:
            try:
                res.append(ef.usagepoint(p_data['id']).update(p_data))
            except http.HTTPError, err:
                if err.code == 404:
                    res.append(ef.usagepoints().create(p_data))
                else:
                    log_sync_error(err)
                    raise
        return res

    def effi_data(self, cursor, uid, ids, context=None):
        """Convert CUPS to EffiPeople data.

        {
          "id": "sample string 1",
          "address": {
            "street": "sample string 1",
            "postalCode": "sample string 2",
            "city": "sample string 3",
            "cityCode": "sample string 4",
            "province": "sample string 5",
            "provinceCode": "sample string 6",
            "country": "sample string 7",
            "countryCode": "sample string 8",
            "parcelNumber": "sample string 9"
          },
          "meters": [
            {
              "id": "sample string 1",
              "usagePointId": "sample string 2"
            },
            {
              "id": "sample string 1",
              "usagePointId": "sample string 2"
            },
            {
              "id": "sample string 1",
              "usagePointId": "sample string 2"
            }
          ]
        }
        """
        if not context:
            context = {}
        res = []
        for cups in self.browse(cursor, uid, ids, context):
            street = []
            street_name = u''
            if cups.cpo or cups.cpa:
                street = u'Polígono %s Parcela %s' % (cups.cpo, cups.cpa)
            else:
                if cups.tv:
                    street.append(cups.tv.name)
                if cups.nv:
                    street.append(cups.nv)
                street_name += u' '.join(street)
                street = [street_name]
                for f_name, f in [(u'número', 'pnp'), (u'escalera', 'es'),
                          (u'planta', 'pt'), (u'puerta', 'pu')]:
                    val = getattr(cups, f)
                    if val:
                        street.append(u'%s %s' % (f_name, val))
                street_name = ', '.join(street)
            res.append(false_to_none({
                'id': cups.name,
                'address': {
                    'street': street_name,
                    'postalCode': cups.dp,
                    'city': cups.id_municipi.name,
                    'cityCode': cups.id_municipi.ine,
                    'province': cups.id_municipi.state.name,
                    'provinceCode': cups.id_municipi.state.code,
                    'country': cups.id_municipi.state.country_id.name,
                    'countryCode': cups.id_municipi.state.country_id.code,
                    'parcelNumber': cups.ref_catastral
                }
            }, context))
        return res

GiscedataCupsPs()
