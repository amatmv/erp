# -*- coding: utf-8 -*-
import re
from datetime import datetime


def datestring_to_epoch(date_string):
    if not date_string:
        return None
    return datetime.strptime(date_string, '%Y-%m-%d').strftime('%s')


def datestring_to_iso(date_string):
    if not date_string:
        return None
    return datetime.strptime(date_string, '%Y-%m-%d').isoformat()


def duration(start, end):
    return (datetime.strptime(end, '%Y-%m-%d')
            - datetime.strptime(start, '%Y-%m-%d')).days * 24 * 3600


def get_lectura_periode(lect_desc):
    periode = re.findall('(P[1-6])', lect_desc)
    if periode:
        periode = periode[0]
    else:
        periode = None
    return periode


def fix_float(value, r=2):
    return str(round(value, r))


def false_to_none(struct, context=None):
    if not context:
        context = {}
    skip = context.get('skip', [])
    if 'xmlrpc' in context:
        return struct
    converted = struct.copy()
    for key, value in struct.items():
        if key in skip:
            continue
        if isinstance(value, dict):
            converted[key] = false_to_none(value, context)
        else:
            if isinstance(value, bool) and not value:
                converted[key] = None
    return converted
