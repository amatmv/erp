import netsvc
import json

def log_sync_error(httperr, level=netsvc.LOG_ERROR):
    logger = netsvc.Logger()
    code = httperr.code
    try:
        body = json.loads(httperr.body)
    except ValueError:
        body = {}
    logger.notifyChannel(
        'effipeople',
        level,
        'HTTP %s ERROR: %s' % (code, body.get('exceptionMessage', 'No message'))
    )
