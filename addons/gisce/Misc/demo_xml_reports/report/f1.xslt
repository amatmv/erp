<?xml version="1.0" encoding="ISO-8859-1" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

<xsl:template match="/">
  <xsl:apply-templates select="factures" />
</xsl:template>

<xsl:template match="factura">
  <FacturaATR>
	  <DatosGeneralesFacturaATR>
		  <Cliente><xsl:value-of select="client"/></Cliente>
			<Contrato><xsl:value-of select="contracte"/></Contrato>
      <DatosGeneralesFactura>
			  <NumeroFactura><xsl:value-of select="numero"/></NumeroFactura>
			</DatosGeneralesFactura>
			<DatosFacturaATR>
			  <CodigoTarifa><xsl:value-of select="tarifa/name"/></CodigoTarifa>
			  <Periodo>
				  <FechaDesdeFactura><xsl:value-of select="data_inici"/></FechaDesdeFactura>
					<FechaHastaFactura><xsl:value-of select="data_final"/></FechaHastaFactura>
				</Periodo>
			</DatosFacturaATR>
		</DatosGeneralesFacturaATR>
		<Potencia>
		</Potencia>
		<Energia>
		</Energia>
	</FacturaATR>
</xsl:template>

<xsl:template match="factures">
	<MensajeFacturacion>
	  <xsl:attribute name="AgenteSolicitante">3333</xsl:attribute>
	  <Cabecera>
		  <CodigoREEEmpresaEmisora></CodigoREEEmpresaEmisora>
			<CodigoREEEmpresaDestino></CodigoREEEmpresaDestino>
			<CodigoDelProceso>F1</CodigoDelProceso>
      <CodigoDePaso></CodigoDePaso>
      <CodigoDeSolicitud></CodigoDeSolicitud>
      <SecuencialDeSolicitud></SecuencialDeSolicitud>
      <Codigo><xsl:value-of select="numero"/></Codigo><!-- CUPS o # factura -->
      <FechaSolicitud></FechaSolicitud>
      <Version>01</Version>
		</Cabecera>
		<Facturas>
		  <xsl:apply-templates select="factura"/>
		</Facturas>
	</MensajeFacturacion>
</xsl:template>
</xsl:stylesheet>

