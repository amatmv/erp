<?xml version="1.0" encoding="ISO-8859-1" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

<xsl:template match="/">
  <xsl:apply-templates select="factures" />
</xsl:template>

<xsl:template match="factures">
	<xsl:apply-templates select="factura"/>
</xsl:template>

<xsl:template match="factura">
<MensajeSaldoLecturasFacturacion>
	<xsl:attribute name="AgenteSolicitante">3333</xsl:attribute>
	<Cabecera>
		<CodigoREEEmpresaEmisora></CodigoREEEmpresaEmisora>
		<CodigoREEEmpresaDestino></CodigoREEEmpresaDestino>
		<CodigoDelProceso>Q1</CodigoDelProceso>
      		<CodigoDePaso></CodigoDePaso>
		<CodigoDeSolicitud></CodigoDeSolicitud>
		<SecuencialDeSolicitud></SecuencialDeSolicitud>
		<Codigo></Codigo>
		<FechaSolicitud></FechaSolicitud>
		<Version>01</Version>
	</Cabecera>
	<Medidas>
		<CodUnificadoPuntoSuministro><xsl:value-of select="cups" /></CodUnificadoPuntoSuministro>
		<Aparato>
		<xsl:apply-templates select="aparato" />
		</Aparato>
	</Medidas>
</MensajeSaldoLecturasFacturacion>
</xsl:template>

<xsl:template match="aparato">
<Tipo>CG</Tipo>
<Marca>199</Marca>
<NumeroSerie><xsl:value-of select="comptador/numero" /></NumeroSerie>
<CodigoDH><xsl:value-of select="comptador/dh" /></CodigoDH>
<xsl:apply-templates select="lectura_energia" />
<xsl:apply-templates select="lectura_potencia" />
</xsl:template>

<xsl:template match="lectura_energia">
<Integrador>
<Magnitud><xsl:if test="tipus='activa'">AE</xsl:if><xsl:if test="tipus='reactiva'">R1</xsl:if></Magnitud>
<CodigoPeriodo><xsl:value-of select="nom" /></CodigoPeriodo>
<ConstanteMultiplicadora>1</ConstanteMultiplicadora>
<NumeroRuedasEnteras>9</NumeroRuedasEnteras>
<NumeroRuedasDecimales>00</NumeroRuedasDecimales>
<ConsumoCalculado><xsl:value-of select="consum" /></ConsumoCalculado>
<LecturaDesde>
	<FechaHora><xsl:value-of select="data_anterior" /></FechaHora>
	<Procedencia>30</Procedencia>
	<Lectura><xsl:value-of select="lect_anterior" /></Lectura>
</LecturaDesde>
<LecturaHasta>
	<FechaHora><xsl:value-of select="data_actual" /></FechaHora>
	<Procedencia>30</Procedencia>
	<Lectura><xsl:value-of select="lect_actual" /></Lectura>
	</LecturaHasta>
</Integrador>
</xsl:template>

<xsl:template match="lectura_potencia">
<Integrador>
<Magnitud>PM</Magnitud>
<CodigoPeriodo><xsl:value-of select="nom" /></CodigoPeriodo>
<ConstanteMultiplicadora>1</ConstanteMultiplicadora>
<NumeroRuedasEnteras>9</NumeroRuedasEnteras>
<NumeroRuedasDecimales>00</NumeroRuedasDecimales>
<ConsumoCalculado><xsl:value-of select="pot_maximetre * 1000" /></ConsumoCalculado>
<LecturaDesde>
	<FechaHora><xsl:value-of select="data_anterior" /></FechaHora>
	<Procedencia>30</Procedencia>
	<Lectura>0</Lectura>
</LecturaDesde>
<LecturaHasta>
	<FechaHora><xsl:value-of select="data_actual" /></FechaHora>
	<Procedencia>30</Procedencia>
	<Lectura><xsl:value-of select="pot_maximetre" /></Lectura>
	</LecturaHasta>
</Integrador>
</xsl:template>

</xsl:stylesheet>

