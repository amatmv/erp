# -*- coding: utf-8 -*-
{
    "name": "Demo XML Reports",
    "description": """
Demo XML Reports
  """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Misc",
    "depends":[
        "base",
        "giscemisc_xml_reports"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "report.xml"
    ],
    "active": False,
    "installable": True
}
