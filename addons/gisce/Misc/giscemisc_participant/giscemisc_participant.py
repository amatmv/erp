# -*- encoding: utf-8 -*-

from osv import osv, fields
from tools.translate import _

PARTICIPANT_TYPES = [
    ('comer', 'Comercialitzadora'),
    ('distri', 'Distribuidora'),
    ('representant', 'Representant'),
    ('directe', 'Consumidors Directes')
]


class GiscemiscParticipant(osv.osv):

    _name = 'giscemisc.participant'

    _inherits = {'res.partner': 'partner_id'}

    def onchange_lopd_active(self, cursor, uid, ids, lopd_active, lopd_data_alta):
        pid = self.get_partner_id(cursor, uid, ids)
        return self.pool.get("res.partner").onchange_lopd_active(cursor, uid, pid, lopd_active,lopd_data_alta)

    def get_id_from_partner(self, cursor, uid, partner_id, context=None):
        pids = self.search(cursor, uid, [('partner_id', '=', partner_id)])
        if len(pids) > 1:
            raise osv.except_osv(
                "Error",
                _(u"S'ha trobat mes de un agent del mercat per "
                  u"l'empresa amb ID {0}.").format(partner_id)
            )
        elif not len(pids):
            return False
        else:
            return pids[0]

    def get_id_from_sifco(self, cursor, uid, codi, context=None):
        pids = self.search(cursor, uid, [('codi_sifco', '=', codi)])
        if len(pids) > 1:
            raise osv.except_osv(
                "Error",
                _(u"S'ha trobat mes de un agent del mercat amb "
                  u"el codi SIFCO {0}.").format(codi)
            )
        elif not len(pids):
            return False
        else:
            return pids[0]

    def get_id_from_ree(self, cursor, uid, codi, context=None):
        pids = self.search(cursor, uid, [('ref', '=', codi)])
        if len(pids) > 1:
            raise osv.except_osv(
                "Error",
                _(u"S'ha trobat mes de un agent del mercat amb "
                  u"el codi REE (ref) {0}.").format(codi)
            )
        elif not len(pids):
            return False
        else:
            return pids[0]

    def get_from_partner(self, cursor, uid, partner_id, context=None):
        pid = self.get_id_from_partner(cursor, uid, partner_id, context)
        if not pid:
            raise osv.except_osv(
                "Error",
                _(u"No s'ha trobat cap agent del mercat per "
                  u"l'empresa amb ID {0}.").format(partner_id)
            )
        else:
            return self.browse(cursor, uid, pid, context)

    def get_from_sifco(self, cursor, uid, codi, context=None):
        pid = self.get_id_from_sifco(cursor, uid, codi, context)
        if not pid:
            raise osv.except_osv(
                "Error",
                _(u"No s'ha trobat cap agent del mercat amb "
                  u"codi SIFCO {0}.").format(codi)
            )
        else:
            return self.browse(cursor, uid, pid, context)

    def get_from_ree(self, cursor, uid, codi, context=None):
        pid = self.get_id_from_ree(cursor, uid, codi, context)
        if not pid:
            raise osv.except_osv(
                "Error",
                _(u"No s'ha trobat cap agent del mercat amb "
                  u"codi REE (ref) {0}.").format(codi)
            )
        else:
            return self.browse(cursor, uid, pid, context)

    def get_partner_id(self, cursor, uid, participant_id, context=None):
        if not participant_id:
            return False
        if isinstance(participant_id, (list, tuple)):
            participant_id = participant_id[0]
        return self.read(
            cursor, uid, participant_id, ['partner_id']
        ).get('partner_id', [False, False])[0]

    def vat_change(self, cr, uid, ids, value, context={}):
        pids = self.read(cr, uid, ids, ['partner_id'])
        pids = [p['partner_id'][0] for p in pids]
        return self.pool.get("res.partner").vat_change(cr, uid, pids, value, context)

    _columns = {
        'participant_type': fields.selection(PARTICIPANT_TYPES, 'Tipus de Participant'),
        'start_date': fields.date('Data d\'alta'),
        'end_date': fields.date('Data de baixa'),
        'sector_gas': fields.boolean('Gas'),
        'sector_electric': fields.boolean('Elèctric'),
        'codi_sifco': fields.char('Codi SIFCO', size=4),
        'partner_id': fields.many2one('res.partner', 'partner_id'),
        'cor': fields.boolean('Comercialitzadora de Referècia'),
        'tlf_autolecturas': fields.char('Telèfon d\'autolectures', size=64),
        'tlf_averias': fields.char('Telèfon d\'averies', size=64),
    }


GiscemiscParticipant()


class ResPartner(osv.osv):
    _name = 'res.partner'
    _inherit = 'res.partner'

    def _energy_sector(self, cursor, uid, ids, field_name, arg, context=None):
        "Retorna un text amb els motius del rebuig"
        res = dict.fromkeys(ids, 'undefined')
        cursor.execute(
            "SELECT rp.id, mp.sector_gas, mp.sector_electric"
            " FROM res_partner AS rp RIGHT JOIN giscemisc_participant AS mp"
            " ON rp.id = mp.partner_id"
            " WHERE rp.id in %s",
            (tuple(ids),)
        )
        ult = cursor.fetchall()

        for row in ult:
            is_elec = row[2]
            is_gas = row[1]
            selection_case = 'undefined'

            if is_elec and is_gas:
                selection_case = 'elegas'
            elif is_elec:
                selection_case = 'electric'
            elif is_gas:
                selection_case = 'gas'

            res[row[0]] = selection_case

        return res

    def _energy_sector_search(self, cursor, uid, obj, name, args, context=None):
        if not context:
            context = {}
        if not args:
            return []
        else:
            operator = args[0][1]
            search_name = args[0][2]

            where_params = ''
            if operator == '=':
                if search_name == 'electric':
                    where_params = 'mp.sector_electric AND (NOT mp.sector_gas OR mp.sector_gas IS null)'
                elif search_name == 'gas':
                    where_params = 'mp.sector_gas AND (NOT mp.sector_electric OR mp.sector_electric IS null)'
                elif search_name == 'elegas':
                    where_params = 'mp.sector_electric AND mp.sector_gas'
                else:
                    where_params = '(NOT mp.sector_electric AND NOT mp.sector_gas) ' \
                                   'OR (NOT mp.sector_electric AND mp.sector_gas IS null) ' \
                                   'OR (mp.sector_electric IS null AND NOT mp.sector_gas) ' \
                                   'OR (mp.sector_electric IS null AND mp.sector_gas IS null)'
            elif operator == 'in':
                where_params = []
                if 'electric' in search_name:
                    where_params.append('(mp.sector_electric AND (NOT mp.sector_gas OR mp.sector_gas IS null))')
                if 'gas' in search_name:
                    where_params.append('(mp.sector_gas AND (NOT mp.sector_electric OR mp.sector_electric IS null))')
                if 'elegas' in search_name:
                    where_params.append('(mp.sector_electric AND mp.sector_gas)')
                if 'undefined' in search_name:
                    where_params.append('((NOT mp.sector_electric AND NOT mp.sector_gas) ' \
                                   'OR (NOT mp.sector_electric AND mp.sector_gas IS null) ' \
                                   'OR (mp.sector_electric IS null AND NOT mp.sector_gas) ' \
                                   'OR (mp.sector_electric IS null AND mp.sector_gas IS null))'
                                        )
                where_params = ' OR '.join(where_params)

            query = "SELECT rp.id FROM res_partner AS rp " \
                    "RIGHT JOIN giscemisc_participant AS mp " \
                    "ON rp.id = mp.partner_id WHERE {}".format(where_params)

            cursor.execute(
                query
            )
            ult = cursor.fetchall()
            partner_ids = [p[0] for p in ult]

            return [('id', 'in', partner_ids)]

    _columns = {
        'energy_sector': fields.function(_energy_sector,
                                         method=True,
                                         type='selection',
                                         selection=[('electric', 'Electric'),
                                                    ('gas', 'Gas'),
                                                    ('elegas', 'Electric & Gas'),
                                                    ('undefined', 'Indefinit')
                                                    ],
                                         string='Sector d\'energía',
                                         fnct_search=_energy_sector_search,
                                         ),
    }


ResPartner()
