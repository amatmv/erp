# coding=utf-8

import logging


def up(cursor, installed_version):
    logger = logging.getLogger('openerp.migration')
    aplicable = not installed_version
    if not aplicable:
        sql = """select * from giscemisc_participant where partner_id=1;"""
        cursor.execute(sql)
        aplicable = cursor.fetchall()
    if len(aplicable) > 0:
        logger.info('Default participant no aplicable')
        return

    logger.info('Creating default participant for ID=1.')

    sql = """insert into giscemisc_participant ("partner_id") VALUES (1);"""
    cursor.execute(sql)


def down(cursor, installed_version):
    pass


migrate = up
