# -*- coding: utf-8 -*-
{
    "name": "Giscemisc Participant",
    "description": """ 
        This module is an extension of the res_partner to index the information of the Electric and Gas Companies in Spain.
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends": [
        'base_extended'
    ],
    "init_xml": [],
    "demo_xml": ["giscemisc_participant_demo.xml"],
    "update_xml": [
        "giscemisc_participant_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
