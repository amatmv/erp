# -*- coding: utf-8 -*-

from osv import fields, osv
import tools

class task(osv.osv):
    _name = "project.task"
    _inherit = "project.task"

    def write(self, cr, uid, ids, vals, context={}):
        for t in self.browse(cr, uid, ids):
            if vals.has_key('state'):
                print '  => Comprovant si hi ha una regle per %s a %s' % (t.state, vals['state'])
                avisos = self.pool.get('giscemisc.project.mailing').search(cr, uid,\
                  [('state_from', '=', t.state), ('state_to', '=', vals['state']),\
                   ('active', '=', True)])
                if len(avisos):
                    for avis in self.pool.get('giscemisc.project.mailing').browse(cr, uid, avisos):
                        if avis.sender == 'project_responsible':
                            remitent = self.pool.get('res.users').browse(cr, uid, t.project_id.manager.id)
                        elif avis.sender == 'task_responsible':
                            remitent = self.pool.get('res.users').browse(cr, uid, t.user_id.id)
                        print '    => TROBAT! (%s) <=' % (avis.name)
                        emails_to = []
                        email_from = "%s <%s>" % (remitent.name, remitent.address_id.email)
                        if avis.receiver == 'project_responsible':
                            receiver = self.pool.get('res.users').browse(cr, uid, t.project_id.manager.id)
                        elif avis.receiver == 'task_responsible':
                            receiver = self.pool.get('res.users').browse(cr, uid, t.user_id.id)
                        emails_to.append("%s <%s>" % (receiver.name, receiver.address_id.email))
                        for user in avis.users:
                            emails_to.append("%s <%s>" % (user.name, user.address_id.email))
                        data = {
                          'task_name': t.name or '',
                          'task_notes': t.notes or '',
                          'task_description': t.description or '',
                          'assumpte_pressupost': t.cust_desc or '',
                        }
                        subject = avis.subject % data
                        body = avis.text % data
                        if remitent.signature:
                            body += '\n\n'+remitent.signature
                        if avis.attachments:
                            attach_ids = self.pool.get('ir.attachment').search(cr, uid,
                              [('res_model', '=', 'sale.order'),
                                                                              ('res_id', '=', p.id)])
                            res = self.pool.get('ir.attachment').read(cr, uid,
                              attach_ids, ['datas_fname','datas'])
                            res = map(lambda x: (x['datas_fname'],
                              base64.decodestring(x['datas'])), res)
                            tools.email_send_attach(email_from, emails_to, subject, body, attach=res)
                        else:
                            tools.email_send(email_from, emails_to, subject, body)
        return super(osv.osv, self).write(cr, uid, ids, vals, context)

task()


class project(osv.osv):
    _name = "project.project"
    _inherit = "project.project"


    # set active value for a project, its sub projects and its tasks
    def open(self, cr, uid, id, context={}):
        for proj in self.browse(cr, uid, id, context):
            self.write(cr, uid, [proj.id], {'state': 'open'}, context)
            cr.execute('select id from project_task where project_id=%d', (proj.id,))
            tasks_id = [x[0] for x in cr.fetchall()]
            self.pool.get('project.task').write(cr, uid, tasks_id, {'state': 'open'}, context)
            project_ids = [x[0] for x in cr.fetchall()]
            for child in project_ids:
                self.open(cr, uid, [child], context)
            return True

project()
