# -*- coding: utf-8 -*-
from osv import osv,fields

class giscemisc_project_mailing(osv.osv):
    _name = 'giscemisc.project.mailing'

    _buddies = [
      ('project_responsible', 'Project Responsible'),
      ('task_responsible', 'Task Responsible'),
    ]

    def _name_field(self, cr, uid, ids, field_name, args={}, context={}):
        res = {}
        for mailing in self.browse(cr, uid, ids, context):
            res[mailing.id] = '%s => %s' % (mailing.state_from, mailing.state_to)
        return res

    _columns = {
      'name': fields.function(_name_field, method=True, type='char', string='Name'),
      'state_from': fields.char('From', size=60, required=True),
      'state_to': fields.char('To', size=60, required=True),
      'subject': fields.char('Subject', size=60, required=True),
      'text': fields.text('Missatge', required=True),
      'users': fields.many2many('res.users', 'giscemisc_project_mailing_users',\
        'mailing_id', 'user_id', 'Other users'),
      'active': fields.boolean('Active'),
      'attachments': fields.boolean('Send attached archives'),
      'sender': fields.selection(_buddies, 'Mail from'),
      'receiver': fields.selection(_buddies, 'Mail to'),
    }


    def name_get(self, cr, uid, ids, context={}):
        if not len(ids):
            return []
        res = []
        for mailing in self.browse(cr, uid, ids):
            name = '%s => %s' % (mailing.state_from,  mailing.state_to)
            res.append((mailing.id, name))
        return res

giscemisc_project_mailing()
