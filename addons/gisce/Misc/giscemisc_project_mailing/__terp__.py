# -*- coding: utf-8 -*-
{
    "name": "GISCE Project Mailing",
    "description": """Mailing system depending on project/tasks states change""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Projects",
    "depends":[
        "project"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscemisc_project_mailing_view.xml",
        "project_view.xml"
    ],
    "active": False,
    "installable": True
}
