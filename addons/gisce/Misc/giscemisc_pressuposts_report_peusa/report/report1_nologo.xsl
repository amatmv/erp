<?xml version="1.0" encoding="iso-8859-15"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

  <xsl:decimal-format name="ca_ES" decimal-separator="," grouping-separator="."/>
  
  <xsl:template match="/">
    <xsl:apply-templates select="pressuposts"/>
  </xsl:template>

  <xsl:template match="pressuposts">
    <xsl:apply-templates select="pressupost" />
  </xsl:template>

  <xsl:template match="pressupost">
    <document debug="1" compression="1">
      <template pageSize="(21cm, 29.7cm)">
        <pageTemplate id="main">
        <!--
          <pageGraphics>
          	<image file="addons/custom/logo.png" width="20mm" height="20mm" x="1cm" y="26.6cm" />
          	<setFont name="Helvetica-Bold" size="14" />
          	<drawString x="3.2cm" y="26.6cm">productora el�ctrica urgelense s.a.</drawString>
          	<setFont name="Helvetica" size="8" />
          	<drawRightString x="20cm" y="26.2cm"><xsl:value-of select="//corporate-header/corporation/address[type='default']/street" /> - <xsl:value-of select="concat(//corporate-header/corporation/address[type='default']/zip, ' ', //corporate-header/corporation/address[type='default']/city)" /> (<xsl:value-of select="//corporate-header/corporation/address[type='default']/state" />) - tel. <xsl:value-of select="//corporate-header/corporation/address[type='default']/phone" /> - fax. <xsl:value-of select="//corporate-header/corporation/address[type='default']/fax" /> - e-mail: <xsl:value-of select="//corporate-header/corporation/address[type='default']/email" /></drawRightString>
          	<rotate degrees="90" />
            <setFont name="Helvetica" size="7" />
            <drawString x="60mm" y="-4mm"><xsl:value-of select="//corporate-header/corporation/rml_footer2" /></drawString>
          </pageGraphics>
          -->
          <frame id="first" x1="1cm" y1="1cm" width="19cm" height="247mm"/>
        </pageTemplate>
        
        <!-- Plantilla pel llistat -->
        <pageTemplate id="llistat">
          <pageGraphics>
          	<!--
          	<image file="addons/custom/logo.png" width="20mm" height="20mm" x="1cm" y="26.6cm" />
          	<setFont name="Helvetica-Bold" size="30" />
          	<drawString x="3.2cm" y="27.8cm"><xsl:value-of select="//corporate-header/corporation/rml_header1" /></drawString>
          	<setFont name="Helvetica" size="10" />
          	<drawString x="3.2cm" y="27.1cm"><xsl:value-of select="//corporate-header/corporation/rml_footer1" /></drawString>
          	<lineMode width="2" />
          	<stroke color="blue"/>
          	<lines>
          		3.2cm 27cm 20cm 27cm
          	</lines>
          	<setFont name="Helvetica" size="8" />
          	<drawString x="3.2cm" y="26.6cm"><xsl:value-of select="//corporate-header/corporation/name" /></drawString>
          	<drawRightString x="20cm" y="26.7cm"><xsl:value-of select="//corporate-header/corporation/address[type='default']/street" /></drawRightString>
          	<drawRightString x="20cm" y="26.4cm"><xsl:value-of select="concat(//corporate-header/corporation/address[type='default']/zip, ' ', //corporate-header/corporation/address[type='default']/city)" /> (<xsl:value-of select="//corporate-header/corporation/address[type='default']/state" />)</drawRightString>
          	<drawRightString x="20cm" y="26.1cm">Tel. <xsl:value-of select="//corporate-header/corporation/address[type='default']/phone" /> - Fax <xsl:value-of select="//corporate-header/corporation/address[type='default']/fax" /></drawRightString>
          	<drawRightString x="20cm" y="25.8cm">E-mail: <xsl:value-of select="//corporate-header/corporation/address[type='default']/email" /></drawRightString>
          	-->
          	<!-- Repetici� pel llistat -->
          	<setFont name="Helvetica" size="10" />
            <drawString x="13.2cm" y="23.1cm"><xsl:value-of select="partner/name" /></drawString>
            <drawString x="13.2cm" y="22.7cm"><xsl:value-of select="sollicitud/partner_address/adreca" /></drawString>
            <drawString x="13.2cm" y="22.3cm"><xsl:value-of select="concat(sollicitud/partner_address/municipi, '  ')" /> <xsl:value-of select="sollicitud/partner_address/cp" /></drawString>
            <place x="1.2cm" y="17.4cm" width="19cm" height="5cm">
            	<blockTable style="taula" colWidths="5cm,13cm">
            		<tr>
            			<td>N�m. estudi t�cnic econ�mic:</td>
            			<td><xsl:value-of select="name" /></td>
            		</tr>
            		<tr>
            			<td>Data:</td>
            			<td><xsl:value-of select="concat(substring(data, 9, 2),'/',substring(data, 6, 2),'/',substring(data, 1, 4))" /></td>
            		</tr>
            		<tr>
            			<td>Obra:</td>
            			<td><para style="text"><b><xsl:value-of select="assumpte" /></b></para></td>
            		</tr>
            		<tr>
            			<td>Descripci�:</td>
            			<td><para style="text"><xsl:value-of select="descripcio" /></para></td>
            		</tr>
            		<tr>
            			<td>N. Obra:</td>
            			<td><para style="text"><xsl:value-of select="obra" /></para></td>
            		</tr>
            		<xsl:if test="sollicitud/name != ''">
            		<tr>
            			<td><para style="text">Pot�ncia Sol.:</para></td>
            			<td><para style="text"><xsl:value-of select="sollicitud/potencia" /> kW.</para></td>
            		</tr>
            		</xsl:if>
            	</blockTable>
            </place>
          	<!-- Final dades llistat -->
          	<rotate degrees="90" />
            <setFont name="Helvetica" size="7" />
            <drawString x="60mm" y="-4mm"><xsl:value-of select="//corporate-header/corporation/rml_footer2" /></drawString>
          </pageGraphics>
          <frame id="llista" x1="1cm" y1="1cm" width="19cm" height="17cm"/>
        </pageTemplate>
      </template>
      
      <stylesheet> 
      	<paraStyle name="text"
      		fontName="Helvetica"
      		fontSize="10" />
      		
      	<paraStyle name="vermell"
      		textColor="darkred"
      		fontName="Helvetica-Bold"
      		fontSize="10" />
      		
      	<paraStyle name="blau"
      		textColor="darkblue"
      		fontName="Helvetica-Bold"
      		fontSize="10"
      		leftIndent="0.5cm" />
      		
      	<blockTableStyle id="taula">
          <blockFont name="Helvetica" size="10" />
        </blockTableStyle>
      		
      	<blockTableStyle id="taula_preus">
          <blockFont name="Helvetica" size="10" />
          <lineStyle kind="GRID" start="0,0" stop="-1,0" colorName="black" />
          <blockBackground colorName="silver" start="0,0" stop="-1,0" />
          <lineStyle kind="BOX" start="0,1" stop="-1,-1" colorName="black"/>
          <lineStyle kind="LINEAFTER" start="0,1" stop="-1,-1" colorName="black"/>
        </blockTableStyle>

      		
      </stylesheet>
    
      <story>
      	<!-- Adre�a per la carta -->
      	<spacer length="2cm" />
        <para style="text" leftIndent="12cm"><xsl:value-of select="partner/name" /></para>
        <para style="text" leftIndent="12cm"><xsl:value-of select="sollicitud/partner_address/adreca" /></para>
        <para style="text" leftIndent="12cm"><xsl:value-of select="concat(sollicitud/partner_address/municipi, '  ')" /> <xsl:value-of select="sollicitud/partner_address/cp" /></para>
        <!-- Final adre�a per carta -->
        <spacer length="20" />
        <para style="text"><u>N�m. estudi t�cnic econ�mic:</u> <xsl:value-of select="name" /></para>
        <spacer length="10" />
        <blockTable colWidths="2.5cm,7cm,3cm,6.5cm">
          <tr>
            <td><para style="text">Sol�licitant:</para></td><td><para style="text"><xsl:value-of select="partner/name" /></para></td>
            <td><para style="text">N. Sol�licitud:</para></td><td><para style="text"><xsl:value-of select="sollicitud/name" /></para></td>
          </tr>
          <tr>
            <td><para style="text">Domicili:</para></td><td><para style="text"><xsl:value-of select="sollicitud/partner_address/adreca" /></para></td>
            <td><para style="text">N. Obra:</para></td><td><para style="text"><xsl:value-of select="obra" /></para></td>
          </tr>
          <tr>
            <td><para style="text">Tel�fon:</para></td><td><para style="text"><xsl:value-of select="sollicitud/partner_address/phone" /></para></td>
            <td><para style="text">Pot�ncia Sol.:</para></td>
            <td><para style="text"><xsl:value-of select="sollicitud/potencia" /> kW.</para></td>
          </tr>
          <tr>
          	<td></td><td></td>
            <td><para style="text">Data:</para></td><td><para style="text"><xsl:value-of select="concat(substring(data, 9, 2),'/',substring(data, 6, 2),'/',substring(data, 1, 4))" /></para></td>
          </tr>
        </blockTable>
        <spacer length="15" />
        <para style="text">OBRA: <xsl:value-of select="assumpte" /></para>
        <spacer length="10" />
        <para style="text"><t t="1">DESCRIPCI�</t>: <xsl:value-of select="descripcio" /></para>
        <spacer length="10" />
        <!-- Resum cap�tols -->
        <blockTable style="taula_preus" colWidths="16.5cm,2.5cm" repeatRows="1">
          <tr>
            <td><para style="text" background="pink"><b>Concepte</b></para></td>
            <td><para style="text" alignment="right"><b>Import</b></para></td>
          </tr>
          <xsl:for-each select="capitols/capitol">
          <tr>
      		<td><para style="blau"><xsl:value-of select="nom" /></para></td>
		    <td><para style="vermell" alignment="right"><xsl:value-of select="format-number(preu, '###.##0,00', 'ca_ES')" /></para></td>
		  </tr>
          </xsl:for-each>
	      <tr>
	      	<td><para style="text" alignment="right">TOTAL (impostos apart)</para></td>
	      	<td><para style="text" alignment="right"><b><xsl:value-of select="format-number(preu_sense_taxes, '###.##0,00', 'ca_ES')" /></b></para></td>
	      </tr>
	    </blockTable>
	    <spacer length="20" />
	    <xsl:if test="obs_client!=''">
	    <para style="text" fontName="Helvetica-Bold">OBSERVACIONS:</para>
	    <xpre style="text"><xsl:value-of select="obs_client" /></xpre>
	    </xsl:if>
	    <!-- Final resum cap�tols -->
	    
        <xsl:if test="sollicitud/name != ''">
        <nextPage />
        <para style="text"><b><u>INFORME ECON�MIC</u></b></para>
        <spacer length="10" />
        <para style="text"><u>Instal�laci� d'extenci�</u></para>
        <blockTable style="taula">
          <tr>
            <td>Import</td>
            <td><xsl:value-of select="format-number(../pressupost/preu_sense_taxes, '###.##0,00', 'ca_ES')" /> �</td>
          </tr>
          <tr>
            <td><xsl:value-of select="sollicitud/taxa" /></td>
            <td><xsl:value-of select="format-number(../pressupost/taxes, '###.##0,00', 'ca_ES')" /> �</td>
          </tr>
          <tr>
            <td fontName="Courier">Total</td>
            <td><xsl:value-of select="format-number(../pressupost/preu_total, '###.##0,00', 'ca_ES')" /> �</td>
          </tr>
        </blockTable>
        
        <spacer length="30" />
        <para style="text">Condicions de pagament:</para>
        <spacer length="10" />
        <xpre style="text" leftIndent="2cm"><xsl:value-of select="sollicitud/cond_pagament" /></xpre>
        
        <spacer length="30" />
        <para style="text">Quota d'acc�s</para>
        <spacer length="10" />
        <para style="text" leftIndent="2cm"><xsl:value-of select="sollicitud/quota" /> .-�. kW. contractat</para>
        
        <spacer length="30" />
        <para style="text" ><xsl:value-of select="sollicitud/text_final" /></para>
        <spacer length="60" />
        <blockTable style="taula" colWidths="9cm, 9cm">
        	<tr>
        		<td><para style="text" alignment="center"><b><xsl:value-of select="//corporate-header/corporation/name" /></b></para></td>
        		<td><para style="text" alignment="center"><b>El Client</b></para></td>
        	</tr>
        	<tr>
        		<td></td>
        		<td><para style="text" alignment="center"><xsl:value-of select="partner/name" /></para></td>
        	</tr>
        </blockTable>
        </xsl:if>
        <nextPage />
        <spacer length="1cm" />
      	<para style="text" alignment="right"><xsl:value-of select="company/partner/addrs/addr/city" />, <xsl:value-of select="data_llarga" /></para>
      	<spacer length="10" />
        <para style="text" fontSize="14">N�m. estudi t�cnic econ�mic: <xsl:value-of select="name" /></para>
        <spacer length="10" />
        <blockTable style="taula" colWidths="5cm,12cm">
          <tr>
            <td>Assumpte:</td><td><para style="text"><xsl:value-of select="assumpte" /></para></td>
          </tr>
          <tr>
            <td>Sol�licitant:</td><td><para style="text"><xsl:value-of select="partner/name" /></para></td>
          </tr>
          <tr>
            <td>N. Sol�licitud:</td><td><para style="text"><xsl:value-of select="sollicitud/name" /></para></td>
          </tr>
          <tr>
            <td>NIF:</td><td><para style="text"><xsl:value-of select="partner/nif" /></para></td>
          </tr>
          <tr>
            <td>Adre�a de subministrament:</td><td><para style="text"><xsl:value-of select="sollicitud/partner_address/adreca" /></para></td>
          </tr>
          <tr>
            <td>Poblaci�:</td><td><para style="text"><xsl:value-of select="sollicitud/partner_address/municipi" /></para></td>
          </tr>
          <tr>
          	<td>C.P:</td><td><para style="text"><xsl:value-of select="sollicitud/partner_address/cp" /></para></td>
          </tr>
          <tr>
            <td>Tel�fon:</td><td><para style="text"><xsl:value-of select="sollicitud/partner_address/phone" /></para></td>
          </tr>
          <tr>
            <td>Pot�ncia prevista:</td><td><para style="text"><xsl:value-of select="sollicitud/potencia" /> kW.</para></td>
          </tr>
          <tr>
            <td>Tensi�:</td><td><para style="text"><xsl:value-of select="sollicitud/tensio" /> V.</para></td>
          </tr>
        </blockTable>
        
        <spacer length="20" />
        <para style="text"><b><u>INFORME T�CNIC</u></b></para>
        <spacer length="10" />
	    <xpre style="text"><xsl:value-of select="informe" /></xpre>

        <setNextTemplate name="llistat" />
        <nextPage />
        <blockTable style="taula_preus" colWidths="11.5cm,2.5cm,2.5cm,2.5cm" repeatRows="1">
          <tr>
            <td><para style="text"><b>Concepte</b></para></td>
            <td><para style="text" alignment="right"><b>Quantitat</b></para></td>
            <td><para style="text" alignment="right"><b>Preu</b></para></td>
            <td><para style="text" alignment="right"><b>Import</b></para></td>
          </tr>
          <xsl:apply-templates match="capitols" mode="story" />
          <tr>
            <td><para style="vermell" t="1">TOTAL</para></td>
            <td></td>
            <td></td>
            <td><para style="vermell" alignment="right"><xsl:value-of select="format-number(preu_sense_taxes, '###.##0,00', 'ca_ES')" /></para></td>
          </tr>
        </blockTable>
        <spacer length="20" />
	    <xsl:if test="obs_client!=''">
	    <para style="text" fontName="Helvetica-Bold">OBSERVACIONS:</para>
	    <xpre style="text"><xsl:value-of select="obs_client" /></xpre>
	    </xsl:if>
      </story>
    </document>
  </xsl:template>

  <xsl:template match="capitols" mode="story">
	<xsl:apply-templates match="capitol" mode="story" />
  </xsl:template>

  <xsl:template match="capitol" mode="story">
  	<xsl:if test="preu&gt;0">
    <tr>
      <td><para style="blau"><xsl:value-of select="nom" /></para></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <xsl:apply-templates match="olines" mode="story" />
    <tr>
    	<td></td>
    	<td></td>
    	<td></td>
    	<td><para style="vermell" alignment="right"><xsl:value-of select="format-number(preu, '###.##0,00', 'ca_ES')" /></para></td>
    </tr>
    </xsl:if>
  </xsl:template>

  <xsl:template match="olines" mode="story">
  	<xsl:if test="oline/preu&gt;0">
    	<xsl:apply-templates match="oline" mode="story" />
    </xsl:if>
  </xsl:template>

  <xsl:template match="oline" mode="story">
    <tr>
      <td><para style="text" leftIndent="1cm"><xsl:value-of select="producte" /></para></td>
      <td><para style="text" alignment="right"><xsl:value-of select="format-number(quantitat, '###.##0,00', 'ca_ES')" /></para></td>
      <td><para style="text" alignment="right"><xsl:value-of select="format-number(preu, '###.##0,00', 'ca_ES')" /></para></td>
      <td><para style="text" alignment="right"><xsl:value-of select="format-number(preu * quantitat,  '###.##0,00', 'ca_ES')" /></para></td>
    </tr>
  </xsl:template>
  
</xsl:stylesheet>
