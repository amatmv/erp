# -*- coding: utf-8 -*-
{
    "name": "Pressuposts Report (PEUSA)",
    "description": """Reports de pressuposts adaptats per PEUSA""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Pressuposts",
    "depends":[
        "giscemisc_pressuposts"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscemisc_pressupost_report.xml",
        "giscemisc_pressupost_view.xml"
    ],
    "active": False,
    "installable": True
}
