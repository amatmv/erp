# -*- coding: utf-8 -*-
{
    "name": "Reports joiner",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * API to join multiple reports
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
