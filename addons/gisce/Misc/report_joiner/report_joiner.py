import tempfile

from report import interface
import netsvc

import pypdftk


class ReportJoiner(interface.report_int):
    """Report Joiner.

    Report type to join more than one report into a single pdf.
    :param name: report name
    :param reports: reports names to join
    """
    def __init__(self, name, reports):
        super(ReportJoiner, self).__init__(name)
        self.reports = reports

    def create(self, cursor, uid, ids, datas, context=None):
        """Create reports and join.

        :param cursor: Database cursor
        :param uid: User identifier
        :param ids: records to print into the report
        :param data: Data passed to report
        :param context: Application context
        :returns a tuple with the type and the content of the report
        """
        # Force PDF report
        report_type = datas.get('report_type', 'pdf')
        if report_type != 'pdf':
            datas['report_type'] = 'pdf'

        to_join_final = []
        for res_id in ids:
            to_join = []
            for report in self.reports:
                report_obj = netsvc.service_exist(report)
                if not report_obj:
                    raise Exception(
                        'Report {0} does not exist in SERVICES'.format(report)
                    )
                res = report_obj.create(cursor, uid, [res_id], datas, context)
                if res[1] != 'pdf':
                    continue
                res_path = tempfile.mkstemp('-join.pdf', 'report-')[1]
                with open(res_path, 'w') as f:
                    f.write(res[0])
                to_join.append(res_path)
            out = pypdftk.concat(files=to_join)
            to_join_final.append(out)
        out = pypdftk.concat(files=to_join_final)
        with open(out, 'r') as f:
            content = f.read()
        return content, 'pdf'
