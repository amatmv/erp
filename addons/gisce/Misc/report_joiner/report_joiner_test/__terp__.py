# -*- coding: utf-8 -*-
{
    "name": "Report joiner test",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Test report joiner
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "account",
        "report_joiner"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "report_joiner_test_report.xml"
    ],
    "active": False,
    "installable": True
}
