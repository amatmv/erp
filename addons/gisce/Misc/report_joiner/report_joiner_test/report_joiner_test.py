from addons.report_joiner import ReportJoiner


ReportJoiner(
    'report.joiner.test',
    [
        'report.res.partner.address',
        'report.account.overdue'
    ]
)
