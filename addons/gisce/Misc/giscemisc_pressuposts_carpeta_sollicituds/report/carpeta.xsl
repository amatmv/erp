<?xml version="1.0" encoding="iso-8859-15"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="sollicituds"/>
  </xsl:template>

  <xsl:template match="sollicituds">
    <xsl:apply-templates select="sollicitud" />
  </xsl:template>

  <xsl:template match="sollicitud">
    <document debug="1" compression="1">
      <template pageSize="(21cm, 29.7cm)">
        <pageTemplate id="main">
          <pageGraphics>
  	         <image file="addons/custom/logo.png" width="20mm" height="20mm" x="1cm" y="26.7cm" />
          	 <setFont name="Helvetica-Bold" size="30" />
          	  <drawString x="3cm" y="27.5cm"><xsl:value-of select="//corporate-header/corporation/rml_header1" /></drawString>
          	 <setFont name="Helvetica" size="10" />
          	  <drawString x="3cm" y="27.1cm"><xsl:value-of select="//corporate-header/corporation/rml_footer1" /></drawString>
          	  <setFont name="Helvetica-Bold" size="10" />
          	  <drawRightString x="20cm" y="3.6cm"><xsl:value-of select="name" /> - <xsl:value-of select="assumpte" /></drawRightString>
          	  <setFont name="Helvetica" size="10"/>
          	  <drawRightString x="20cm" y="3.2cm"><xsl:value-of select="partner/name" /></drawRightString>
          	  <drawRightString x="20cm" y="2.8cm"><xsl:value-of select="partner_address/adreca" /></drawRightString>
          	  <drawRightString x="20cm" y="2.4cm"><xsl:value-of select="concat(partner_address/cp, ' ',partner_address/municipi)" /></drawRightString>
          	 <lineMode width="2" />
          	 <stroke color="lightgreen"/>
          	 <lines>
          		3cm 27cm 20cm 27cm
          	 </lines>
           </pageGraphics>
           <frame id="first" x1="1cm" y1="1cm" width="19cm" height="247mm"/>
        </pageTemplate>
      </template>
      
      <stylesheet> 
      	<paraStyle name="text"
      		fontName="Helvetica"
      		fontSize="10" />
      		
      		
      	<blockTableStyle id="taula">
          <blockFont name="Helvetica" size="10" />
        </blockTableStyle>
      		
      		
      </stylesheet>
    
      <story>
      </story>
    </document>
  </xsl:template>
  
</xsl:stylesheet>
