# -*- coding: utf-8 -*-
{
    "name": "GISCE Pressuposts Carpeta Sol·licituds",
    "description": """Impressió de la carpeta per la sol·licitud""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Pressuposts",
    "depends":[
        "giscemisc_pressuposts"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscemisc_pressuposts_carpeta_sollicituds_report.xml"
    ],
    "active": False,
    "installable": True
}
