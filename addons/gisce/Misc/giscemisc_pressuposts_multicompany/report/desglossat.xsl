<?xml version="1.0" encoding="iso-8859-15"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

  <xsl:key name="materials-per-ot" match="material" use="ot" />

  <xsl:template match="/">
    <xsl:apply-templates select="aprovisionaments"/>
  </xsl:template>

  <xsl:template match="aprovisionaments">
    <xsl:apply-templates select="aprovisionament" />
  </xsl:template>

  <xsl:template match="aprovisionament">
    <document debug="1" compression="1">
      <template pageSize="(21cm, 29.7cm)">
        <pageTemplate id="main">
          <pageGraphics>
          	<image file="addons/giscemisc_pressuposts_multicompany/report/logo_2.png" width="20mm" height="20mm" x="1cm" y="26.7cm" />
          	<setFont name="Helvetica-Bold" size="30" />
          	<drawString x="3cm" y="27.5cm"><xsl:value-of select="company/h1" /></drawString>
          	<setFont name="Helvetica" size="10" />
          	<drawString x="3cm" y="27.1cm"><xsl:value-of select="company/h2" /></drawString>
          	<lineMode width="2" />
          	<stroke color="lightgreen"/>
          	<lines>
          		3cm 27cm 20cm 27cm
          	</lines>
          	<setFont name="Helvetica" size="8" />
          	<drawString x="3cm" y="26.6cm"><xsl:value-of select="company/name" /></drawString>
          	<drawRightString x="20cm" y="26.7cm"><xsl:value-of select="company/partner/addrs/addr/street" /></drawRightString>
          	<drawRightString x="20cm" y="26.4cm">Tel. <xsl:value-of select="company/partner/addrs/addr/tel" /> - Fax <xsl:value-of select="company/partner/addrs/addr/fax" /></drawRightString>
          	<drawRightString x="20cm" y="26.1cm"><xsl:value-of select="concat(company/partner/addrs/addr/cp, ' ')" /> <xsl:value-of select="company/partner/addrs/addr/municipi" /> (<xsl:value-of select="company/partner/addrs/addr/provincia" />)</drawRightString>
          	<drawRightString x="20cm" y="25.8cm">E-mail: <xsl:value-of select="company/partner/addrs/addr/email" /></drawRightString>
          	<!--<translate dx="50mm" dy="50mm" />-->
            <rotate degrees="90" />
            <setFont name="Helvetica" size="7" />
            <drawString x="60mm" y="-4mm"><xsl:value-of select="company/f1" /></drawString>
          </pageGraphics>
          <frame id="first" x1="1cm" y1="1cm" width="19cm" height="247mm"/>
        </pageTemplate>
      </template>
      
      <stylesheet> 
      	<paraStyle name="text"
      		fontName="Helvetica"
      		fontSize="10" />
      		
      	<paraStyle name="vermell"
      		textColor="darkred"
      		fontName="Helvetica-Bold"
      		fontSize="10" />
      		
      	<paraStyle name="blau"
      		textColor="darkblue"
      		fontName="Helvetica-Bold"
      		fontSize="10"
      		leftIndent="0.5cm" />
      		
      	<blockTableStyle id="taula">
          <blockFont name="Helvetica" size="10" />
          <lineStyle kind="GRID" colorName="silver" start="0,1"/>
          <blockBackground colorName="grey" start="0,1" stop="3,1" />
          <blockFont name="Helvetica-Bold" size="10" start="0,1" stop="3,1"/>
        </blockTableStyle>

      		
      </stylesheet>
    
      <story>
        <para style="text" alignment="right"><xsl:value-of select="substring(data_llarga, 3)" /></para>
      	<para style="text">Pressupost: <xsl:value-of select="name" /></para>
      	<para style="text"><b><xsl:value-of select="assumpte" /></b></para>
      	<spacer length="15" />
      	<para style="text">Data d'entrega: <xsl:value-of select="concat(substring(data, 9, 2),'/',substring(data, 6, 2),'/',substring(data, 1, 4), substring(data, 11, 13))" /></para>
      	<!-- Hem de comen�ar agrupant per ordres de treball -->
      	<xsl:for-each select="materials/material[count(. | key('materials-per-ot', ot)[1]) = 1]">
        	<xsl:sort select="ot" />
        	<spacer length="20" />
        	<blockTable style="taula" colWidths="2.5cm,12.5cm,2.5cm,1.5cm" repeatRows="2">
        		<tr>
        			<td><para style="text"><b><xsl:value-of select="ot" /></b></para></td>
        			<td><para style="text"><b><xsl:value-of select="ot_name" /></b></para></td>
        			<td></td>
        			<td></td>
        			<td></td>
        		</tr>
        		<tr>
        			<td>Codi</td>
        			<td>Material</td>
        			<td>Quantitat</td>
        			<td></td>
        		</tr>
        		<xsl:for-each select="key('materials-per-ot', ot)">
        			<tr>
  		<td><para style="text"><xsl:value-of select="codi" /></para></td>
  		<td><para style="text"><xsl:value-of select="nom" /></para></td>
  		<td><para style="text" alignment="right"><xsl:value-of select="format-number(qty, '###,###.00')" /></para></td>
  		<td><para style="text"><xsl:value-of select="uom" /></para></td>
  	</tr>
        		</xsl:for-each>
        	</blockTable>
        </xsl:for-each>
        
      </story>
    </document>
  </xsl:template>
 

</xsl:stylesheet>
