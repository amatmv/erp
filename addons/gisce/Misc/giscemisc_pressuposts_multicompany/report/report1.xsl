<?xml version="1.0" encoding="iso-8859-15"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="pressuposts"/>
  </xsl:template>

  <xsl:template match="pressuposts">
    <xsl:apply-templates select="pressupost" />
  </xsl:template>

  <xsl:template match="pressupost">
    <document debug="1" compression="1">
      <template pageSize="(21cm, 29.7cm)">
        <pageTemplate id="main">
          <pageGraphics>
          	<image file="addons/giscemisc_pressuposts_multicompany/report/logo_2.png" width="20mm" height="20mm" x="1cm" y="26.7cm" />
          	<setFont name="Helvetica-Bold" size="30" />
          	<drawString x="3cm" y="27.5cm"><xsl:value-of select="company/h1" /></drawString>
          	<setFont name="Helvetica" size="10" />
          	<drawString x="3cm" y="27.1cm"><xsl:value-of select="company/h2" /></drawString>
          	<lineMode width="2" />
          	<stroke color="lightgreen"/>
          	<lines>
          		3cm 27cm 20cm 27cm
          	</lines>
          	<setFont name="Helvetica" size="8" />
          	<drawString x="3cm" y="26.6cm"><xsl:value-of select="company/name" /></drawString>
          	<drawRightString x="20cm" y="26.7cm"><xsl:value-of select="company/partner/addrs/addr/street" /></drawRightString>
          	<drawRightString x="20cm" y="26.4cm">Tel. <xsl:value-of select="company/partner/addrs/addr/tel" /> - Fax <xsl:value-of select="company/partner/addrs/addr/fax" /></drawRightString>
          	<drawRightString x="20cm" y="26.1cm"><xsl:value-of select="concat(company/partner/addrs/addr/cp, ' ')" /> <xsl:value-of select="company/partner/addrs/addr/municipi" /> (<xsl:value-of select="company/partner/addrs/addr/provincia" />)</drawRightString>
          	<drawRightString x="20cm" y="25.8cm">E-mail: <xsl:value-of select="company/partner/addrs/addr/email" /></drawRightString>
          	<!--<translate dx="50mm" dy="50mm" />-->
            <rotate degrees="90" />
            <setFont name="Helvetica" size="7" />
            <drawString x="60mm" y="-4mm"><xsl:value-of select="company/f1" /></drawString>
          </pageGraphics>
          <frame id="first" x1="3cm" y1="1cm" width="17cm" height="247mm"/>
        </pageTemplate>
      </template>
      
      <stylesheet> 
      	<paraStyle name="text"
      		fontName="Helvetica"
      		fontSize="10" />
      		
      	<paraStyle name="vermell"
      		textColor="darkred"
      		fontName="Helvetica-Bold"
      		fontSize="10" />
      		
      	<paraStyle name="blau"
      		textColor="darkblue"
      		fontName="Helvetica-Bold"
      		fontSize="10"
      		leftIndent="0.5cm" />
      		
      	<blockTableStyle id="taula">
          <blockFont name="Helvetica" size="10" />
        </blockTableStyle>

      		
      </stylesheet>
    
      <story>
      	<spacer length="1cm" />
      	<para style="text" alignment="right"><xsl:value-of select="company/partner/addrs/addr/city" />, <xsl:value-of select="data_llarga" /></para>
      	<spacer length="10" />
        <para style="text" fontSize="14">Pressupost n�mero: <xsl:value-of select="name" /></para>
        <spacer length="10" />
        <blockTable style="taula" colWidths="6cm,11cm">
          <tr>
            <td>Assumpte:</td><td><para style="text"><xsl:value-of select="assumpte" /></para></td>
          </tr>
          <tr>
            <td>Sol�licitant:</td><td><para style="text"><xsl:value-of select="partner/name" /></para></td>
          </tr>
          <tr>
            <td>NIF:</td><td><para style="text"><xsl:value-of select="partner/nif" /></para></td>
          </tr>
          <tr>
            <td>Adre�a de subministrament:</td><td><para style="text"><xsl:value-of select="sollicitud/partner_address/adreca" /></para></td>
          </tr>
          <tr>
            <td>Poblaci�:</td><td><para style="text"><xsl:value-of select="sollicitud/partner_address/municipi" /></para></td>
          </tr>
          <tr>
          	<td>C.P:</td><td><para style="text"><xsl:value-of select="sollicitud/partner_address/cp" /></para></td>
          </tr>
          <tr>
            <td>Tel�fon:</td><td><para style="text"><xsl:value-of select="sollicitud/partner_address/phone" /></para></td>
          </tr>
          <tr>
            <td>Pot�ncia prevista:</td><td><para style="text"><xsl:value-of select="sollicitud/potencia" /> kW.</para></td>
          </tr>
          <tr>
            <td>Tensi�:</td><td><para style="text"><xsl:value-of select="sollicitud/tensio" /> V.</para></td>
          </tr>
        </blockTable>
        
        <spacer length="20" />
        <para style="text"><b><u>INFORME T�CNIC</u></b></para>
        <spacer length="10" />
	    <xpre style="text"><xsl:value-of select="informe" /></xpre>

        <xsl:if test="sollicitud/name != ''">
        <nextPage />
        <para style="text"><b><u>INFORME ECON�MIC</u></b></para>
        <spacer length="10" />
        <para style="text"><u>Instal�laci� d'extenci�</u></para>
        <blockTable style="taula">
          <tr>
            <td>Import</td>
            <td><xsl:value-of select="format-number(../pressupost/preu_sense_taxes, '###,###.00')" /> �</td>
          </tr>
          <tr>
            <td><xsl:value-of select="sollicitud/taxa" /></td>
            <td><xsl:value-of select="format-number(../pressupost/taxes, '###,###.00')" /> �</td>
          </tr>
          <tr>
            <td fontName="Courier">Total</td>
            <td><xsl:value-of select="../pressupost/preu_total" /> �</td>
          </tr>
        </blockTable>
        
        <spacer length="30" />
        <para style="text">Condicions de pagament:</para>
        <spacer length="10" />
        <xpre style="text" leftIndent="2cm"><xsl:value-of select="sollicitud/cond_pagament" /></xpre>
        
        <spacer length="30" />
        <para style="text">Quota d'acc�s</para>
        <spacer length="10" />
        <para style="text" leftIndent="2cm"><xsl:value-of select="sollicitud/quota" /> .-�. kW. contractat</para>
        
        <spacer length="30" />
        <para style="text" ><xsl:value-of select="sollicitud/text_final" /></para>
        <spacer length="60" />
        <blockTable style="taula" colWidths="8.5cm, 8.5cm">
        	<tr>
        		<td><para style="text" alignment="center"><b><xsl:value-of select="company/name" /></b></para></td>
        		<td><para style="text" alignment="center"><b>El Client</b></para></td>
        	</tr>
        	<tr>
        		<td></td>
        		<td><para style="text" alignment="center"><xsl:value-of select="partner/name" /></para></td>
        	</tr>
        </blockTable>
        </xsl:if>

        <nextPage />
        <spacer length="2cm" />
        <para style="text" leftIndent="10cm"><xsl:value-of select="partner/name" /></para>
        <para style="text" leftIndent="10cm"><xsl:value-of select="sollicitud/partner_address/adreca" /></para>
        <para style="text" leftIndent="10cm"><xsl:value-of select="concat(sollicitud/partner_address/municipi, '  ')" /> <xsl:value-of select="sollicitud/partner_address/cp" /></para>
        <spacer length="30" />

        <blockTable style="taula" colWidths="3cm, 14cm">
          <tr>
            <td><para style="text" alignment="left">N�m.Pressupost:</para></td><td><para style="text" alignment="left"><xsl:value-of select="name" /></para></td>
          </tr>
          <tr>
            <td><para style="text" alignment="left">Data:</para></td><td><para style="text" alignment="left"><xsl:value-of select="concat(substring(data, 9, 2),'/',substring(data, 6, 2),'/',substring(data, 1, 4))" /></para></td>
          </tr>
        </blockTable>
        
        <spacer length="25" />
        <para style="text" fontSize="12">Projecte: <b><xsl:value-of select="assumpte" /></b></para>
        <spacer length="10" />
        <para style="vermell" fontName="Helvetica">Total pressupost: <xsl:value-of select="format-number(preu_sense_taxes, '###,###.00')" /> �</para>
        <spacer length="15" />
        <blockTable style="taula" colWidths="9.5cm,2.5cm,2.5cm,2.5cm" repeatRows="1">
          <tr>
            <td><para style="text"><b>Concepte</b></para></td>
            <td><para style="text" alignment="right"><b>Quantitat</b></para></td>
            <td><para style="text" alignment="right"><b>Preu</b></para></td>
            <td><para style="text" alignment="right"><b>Import</b></para></td>
          </tr>
          <tr>
            <td><para style="vermell"><xsl:value-of select="assumpte" /></para></td>
            <td></td>
            <td></td>
            <td><para style="vermell" alignment="right"><xsl:value-of select="format-number(preu_sense_taxes, '###,###.00')" /></para></td>
          </tr>
          <xsl:apply-templates match="capitols" mode="story" />
        </blockTable>
      </story>
    </document>
  </xsl:template>

  <xsl:template match="capitols" mode="story">
	<xsl:apply-templates match="capitol" mode="story" />
  </xsl:template>

  <xsl:template match="capitol" mode="story">
  	<xsl:if test="preu&gt;0">
    <tr>
      <td><para style="blau"><xsl:value-of select="nom" /></para></td>
      <td></td>
      <td></td>
      <td><para style="vermell" alignment="right"><xsl:value-of select="format-number(preu, '###,###.00')" /></para></td>
    </tr>
    <xsl:apply-templates match="olines" mode="story" />
    </xsl:if>
  </xsl:template>

  <xsl:template match="olines" mode="story">
  	<xsl:if test="oline/preu&gt;0">
    	<xsl:apply-templates match="oline" mode="story" />
    </xsl:if>
  </xsl:template>

  <xsl:template match="oline" mode="story">
    <tr>
      <td><para style="text" leftIndent="1cm"><xsl:value-of select="producte" /></para></td>
      <td><para style="text" alignment="right"><xsl:value-of select="format-number(quantitat, '###,###.00')" /></para></td>
      <td><para style="text" alignment="right"><xsl:value-of select="format-number(preu, '###,###.00')" /></para></td>
      <td><para style="text" alignment="right"><xsl:value-of select="format-number(preu * quantitat,  '###,###.00')" /></para></td>
    </tr>
  </xsl:template>
</xsl:stylesheet>
