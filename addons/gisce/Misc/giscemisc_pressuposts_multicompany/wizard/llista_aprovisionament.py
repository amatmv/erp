# -*- coding: utf-8 -*-
import wizard
import pooler
from osv import osv, fields

_init_form = """<?xml version="1.0"?>
<form string="Informe d'aprovisionament de materials" >
  <image name="terp-mrp" />
  <group>
    <field name="pressupost" required="1" colspan="4" on_change="pressupost_change(pressupost)"/>
    <field name="data" colspan="4" />
  </group>
</form>"""

_init_fields = {
  'pressupost': {'string': 'Pressupost', 'type':'many2one', 'relation': 'sale.order'},
  'data': {'string': 'Data d\'entrega', 'type':'datetime'},
}

class wizard_giscemisc_pressuposts_aprovisionament_llista(osv.osv):
  _name = 'wizard.giscemisc.pressuposts.aprovisionament.llista'
  _auto = False
  
  def pressupost_change(self, cr, uid, ids, pressupost, context={}):
    s = pooler.get_pool(cr.dbname).get('sale.order').browse(cr, uid, pressupost)
    apr_obj = pooler.get_pool(cr.dbname).get('giscemisc.aprovisionament')
    apr_ids = apr_obj.search(cr, uid, [('name', '=', s.name)])
    if len(apr_ids):
      return {'value': {'data': apr_obj.browse(cr, uid, apr_ids[0]).data}}
    else:
      return {}

wizard_giscemisc_pressuposts_aprovisionament_llista()

def _buscar_materials(self, cr, uid, data, context={}):
  apr_obj = pooler.get_pool(cr.dbname).get('giscemisc.aprovisionament')
  mat_obj = pooler.get_pool(cr.dbname).get('giscemisc.aprovisionament.materials')
  s = pooler.get_pool(cr.dbname).get('sale.order').browse(cr, uid, data['form']['pressupost'])
  apr_ids = apr_obj.search(cr, uid, [\
    ('name', '=', s.name)])
  if len(apr_ids):
    print 'No crear...'
    apr_id = apr_ids[0]
    if data['form']['data'] != apr_obj.browse(cr, uid, apr_id).data:
      print 'Modificar data...'
      apr_obj.write(cr, uid, apr_id, {'data': data['form']['data']})
    else:
      print 'No modificar data...'
  else:
    print 'Crear...'
    apr_id = apr_obj.create(cr, uid, {'name': s.id, 'data': data['form']['data']})
    for capitol in s.capitols:
      print '* Buscant material al capitol %s' % (capitol.name)
      for ol in capitol.order_lines:
        if len(ol.escandall) and ol.type == 'make_to_order':
          for mat in ol.escandall:
            if mat.product_id.type == 'product' and mat.product_id.procure_method == 'make_to_stock':
              print'  => %s - %f' % (mat.product_id.name, mat.product_qty)
              vals = {'name': mat.product_id.id,
                 'qty': mat.product_qty * ol.product_uom_qty,
                 'ot': capitol.ot,
                 'aprovisionament_id': apr_id}
              mat_obj.create(cr, uid, vals)
        elif ol.type == 'make_to_stock':
          print '  => %s - %f' % (ol.product_id.name, ol.product_uom_qty)
          vals = {'name': ol.product_id.id,
             'qty': ol.product_uom_qty,
             'ot': capitol.ot,
             'aprovisionament_id': apr_id}
          mat_obj.create(cr, uid, vals)
  return {}

_mostrar_materials_form = """<?xml version="1.0"?>
<form string="Actualització de preus de cost" >
  <field name="pressupost" readonly="1" colspan="4"/>
  <separator colspan="4" />
  <field name="materials" colspan="4" nolabel="1"/>
</form>"""

_mostrar_materials_fields = {
  'pressupost': {'string': 'Pressupost', 'type':'many2one', 'relation': 'sale.order'},
  'materials': {'string': 'Materials', 'type': 'one2many', 'relation': 'giscemisc.aprovisionament.materials'},
}

def _mostrar_materials_action(self, cr, uid, data, context={}):
  s = pooler.get_pool(cr.dbname).get('sale.order').browse(cr, uid, data['form']['pressupost'])
  apr_obj = pooler.get_pool(cr.dbname).get('giscemisc.aprovisionament')
  mat_obj = pooler.get_pool(cr.dbname).get('giscemisc.aprovisionament.materials')
  apr_ids = apr_obj.search(cr, uid, [('name', '=', s.name)])
  materials =  mat_obj.search(cr, uid, [('aprovisionament_id', '=', apr_ids[0])])
  return {'materials': materials}

def _print(self, cr, uid, data, context={}):
  s = pooler.get_pool(cr.dbname).get('sale.order').browse(cr, uid, data['form']['pressupost'])
  apr_obj = pooler.get_pool(cr.dbname).get('giscemisc.aprovisionament')
  apr_ids = apr_obj.search(cr, uid, [('name', '=', s.name)])
  return {'ids': apr_ids}



class wizard_giscemisc_pressupots_aprovisionament(wizard.interface):

  states = {
    'init': {
    	'actions': [],
      'result': {'type': 'form', 'arch': _init_form, 'fields': _init_fields, 'state': [('end', 'Cancel·lar', 'gtk-cancel'), ('buscar_materials', 'Continuar', 'gtk-go-forward')]}
    },
    'buscar_materials': {
      'actions': [_buscar_materials],
      'result': {'type': 'state', 'state': 'print'}
    },
    'mostrar_materials': {
      'actions': [_mostrar_materials_action],
      'result': {'type': 'form', 'arch': _mostrar_materials_form, 'fields': _mostrar_materials_fields, 'state': [('end', 'Cancel·lar', 'gtk-cancel'), ('end', 'Continuar', 'gtk-go-forward')]}
    },
     'print': {
      'actions': [_print],
      'result': {'type': 'print', 'report':
      'giscemisc.aprovisionament.llistat1', 'get_id_from_action':True, \
      'state':'end'}
    },
  }

wizard_giscemisc_pressupots_aprovisionament('giscemisc.pressuposts.aprovisionament.llista')

