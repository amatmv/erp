# -*- coding: utf-8 -*-
from osv import osv,fields

class giscemisc_aprovisionament(osv.osv):
  _name = 'giscemisc.aprovisionament'
  
  _columns = {
    'name': fields.many2one('sale.order', 'Pressupost'),
    'data': fields.datetime('Data'),
    'materials': fields.one2many('giscemisc.aprovisionament.materials', 'aprovisionament_id', 'Materials'),
  }

giscemisc_aprovisionament()

class giscemisc_aprovisionament_materials(osv.osv):
  _name = 'giscemisc.aprovisionament.materials'

  _columns = {
    'name': fields.many2one('product.product', 'Material', required=True),
    'qty': fields.float('Quantitat', required=True),
    'ot': fields.char('Ordre treball', size=11, required=True),
    'ot_name': fields.char('Nom Ordre Treball', size=256),
    'aprovisionament_id': fields.many2one('giscemisc.aprovisionament', 'Aprovisionament')
  }

  def create(self, cr, uid, vals, context={}):
    ids = self.search(cr, uid, [('name', '=', vals['name']), ('ot', '=', vals['ot'])])
    if len(ids):
      print 'ja existeix..'
      self.write(cr, uid, ids[0], {'qty': self.browse(cr, uid, ids[0]).qty + vals['qty']})
      return ids[0]
    else:
      print 'no existeix'
      return super(osv.osv, self).create(cr, uid, vals, context)
giscemisc_aprovisionament_materials()
