# -*- coding: utf-8 -*-
{
    "name": "GISCE Pressuposts multicompany",
    "description": """Multi-company support for Sales Orders""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Multi Company",
    "depends":[
        "giscemisc_pressuposts"
    ],
    "init_xml":[
        "record_rules.xml"
    ],
    "demo_xml": [],
    "update_xml":[
        "giscemisc_pressuposts_multicompany_view.xml",
        "giscemisc_pressuposts_multicompany_wizard.xml",
        "giscemisc_pressuposts_multicompany_report.xml"
    ],
    "active": False,
    "installable": True
}
