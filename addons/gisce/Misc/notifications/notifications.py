# -*- coding: utf-8 -*-
from datetime import datetime

from osv import osv, fields


class LoggerCompat(object):
    def __getattribute__(self, item):
        import netsvc
        log_level = 'LOG_{}'.format(item.upper())
        if hasattr(netsvc, log_level):
            def log(msg):
                logger = netsvc.Logger()
                level = getattr(netsvc, log_level)
                logger.notifyChannel(__name__, level, msg)
            return log
        else:
            return lambda *args, **kwargs: None


_notifications_type = [('poweremail.mailbox', 'Mail')]

_state_notification = [('draft', 'Draft'),
                       ('outgoing', 'Outgoing'),
                       ('sent', 'Sent'),
                       ('received', 'Received'),
                       ('exception', 'Delivery Failed'),
                       ('cancel', 'Cancelled'),
                       ]


class NotificationCategory(osv.osv):

    _name = 'notification.category'
    _description = 'Notification Category'
    _rec_name = 'name'
    _logger = LoggerCompat()

    _columns = {
        'name': fields.char('Nombre', size=256),
        'code': fields.char(u'Código', size=256, select=1)
    }

NotificationCategory()


class Notification(osv.osv):

    _name = 'notification'
    _description = 'Notification'
    _rec_name = 'name'
    _logger = LoggerCompat()

    def unlink(self, cursor, uid, ids, context=None):
        '''Ampliem la funcionalitat del unlink
        per esborrar l'adjunt del document'''
        if not context:
            context = {}
        if not isinstance(ids, list):
            ids = [ids, ]

        for notification in self.browse(cursor, uid, ids, context=context):
            type_model, type_id = notification.type.split(',')
            res_id = int(type_id)
            super(Notification, self).unlink(
                cursor, uid, notification.id, context=context)
            model_obj = self.pool.get(type_model)
            model_obj.unlink(cursor, uid, [res_id])

        return True

    def _get_models_id(self, cursor, uid, ids, context=None):
        return_ids = []
        model = '%s' % self._name
        notification_obj = self.pool.get('notification')
        for notif in ids:
            search_by_ref = '%s,%s' % (model, notif)
            return_ids += notification_obj.search(
                cursor, uid, [('type', '=', search_by_ref)])
        return return_ids


    def _ff_state(self, cursor, uid, ids, field_name, arg,
                                  context=None):
        res = {}
        types = {}
        mapping = {}
        for notif in self.read(cursor, uid, ids, ['type']):
            self._logger.info('Get state from %s ' % notif['type'])
            model, res_id = notif['type'].split(',')
            res_id = int(res_id)
            types.setdefault(model, [])
            types[model].append(res_id)
            mapping[res_id] = notif['id']

        for model, model_ids in types.iteritems():
            model_obj = self.pool.get(model)
            states = model_obj.get_notification_state(cursor, uid, model_ids)
            for res_id, state in states.iteritems():
                res[mapping[res_id]] = state
        return res


    def _ff_get_column(self, cursor, uid, ids, field_name, arg,
                                  context=None):
        res = {}
        for notif in self.read(cursor, uid, ids, ['type']):
            self._logger.info('Get %s from %s ' % (arg['column'],notif['type']))
            model, res_id = notif['type'].split(',')
            res_id = int(res_id)
            model_obj = self.pool.get(model)
            obj_res_id = model_obj.read(
                cursor, uid, res_id, [arg['column']])[arg['column']]
            res[notif['id']] = obj_res_id
        return res

    def _ff_model(self, cursor, uid, ids, field_name, arg,
                              context=None):
        res = {}
        for notif in self.read(cursor, uid, ids, ['type']):
            self._logger.info('Get model from %s ' % notif['type'])
            model, res_id = notif['type'].split(',')
            res_id = int(res_id)
            model_obj = self.pool.get(model)
            res_model = model_obj.read(
                cursor, uid, res_id, ['reference'])['reference']
            res[notif['id']] = res_model.split(',')[0]
        return res

    def _ff_res_id(self, cursor, uid, ids, field_name, arg,
                              context=None):
        res = {}
        for notif in self.read(cursor, uid, ids, ['type']):
            self._logger.info('Get res_id from %s ' % notif['type'])
            model, res_id = notif['type'].split(',')
            res_id = int(res_id)
            model_obj = self.pool.get(model)
            obj_res_id = model_obj.read(
                cursor, uid, res_id, ['res_id'])['res_id']
            res[notif['id']] = int(obj_res_id.split(',')[1])
        return res

    def _ff_search_user_id(self, cursor, uid, obj, name, args, context=None):
        type_search = []
        for model in getattr(obj, '_STORE_STATUS'):
            print model
            model_obj = self.pool.get(model)
            ids = model_obj.search(cursor, uid, args)
            type_search += ['%s,%s' % (model, x) for x in ids]
        return [('type', 'in', type_search)]

    def _ff_type_str(self, cursor, uid, ids, field_name, arg,
                              context=None):
        res = {}.fromkeys(ids, False)
        for notif in self.read(cursor, uid, ids, ['type']):
            model, res_id = notif['type'].split(',')
            res[notif['id']] = model
        return res

    def _type_str_search(self, cursor, uid, obj, name, args, context=None):
        if not context:
            context = {}
        if not args:
            return [('id', '=', 0)]
        return [('type', 'like', args[0][2])]

    def _ff_subject(self, cursor, uid, ids, field_name, arg,
                              context=None):
        res = {}
        types = {}
        mapping = {}
        for notif in self.read(cursor, uid, ids, ['type']):
            self._logger.info('Get subject from %s ' % notif['type'])
            model, res_id = notif['type'].split(',')
            res_id = int(res_id)
            types.setdefault(model, [])
            types[model].append(res_id)
            mapping[res_id] = notif['id']

        for model, model_ids in types.iteritems():
            model_obj = self.pool.get(model)
            states = model_obj.get_subject(cursor, uid, model_ids)
            for res_id, state in states.iteritems():
                res[mapping[res_id]] = state
        return res

    def send(self, cursor, uid, ids, context=None):
        for notif in self.read(cursor, uid, ids, ['type']):
            self._logger.info('Send notification id:%s ' % notif['id'])
            model, res_id = notif['type'].split(',')
            res_id = int(res_id)
            model_obj = self.pool.get(model)
            model_obj.send(cursor, uid, [res_id])
        return True

    _columns = {
        'name': fields.datetime('Fecha notificación'),
        'type': fields.reference('Tipo',
                                 selection=_notifications_type,
                                 size=512),
        'type_str': fields.function(
            _ff_type_str,
            string='Tipo',
            type='selection',
            selection=_notifications_type,
            fnct_search=_type_str_search,
            method=True
        ),
        'state': fields.function(_ff_state,
                                 type='selection',
                                 selection=_state_notification,
                                 string='Estado',
                                 method=True,
        ),
        'model': fields.function(_ff_model,
                                 type='char',
                                 string='Related model',
                                 size=512,
                                 method=True),
        'subject': fields.function(_ff_subject,
                                 type='char',
                                 string='Asunto',
                                 size=512,
                                 method=True),
        'res_id': fields.function(_ff_res_id,
                                 type='integer',
                                 string='Related ID',
                                 method=True),
        'user_id': fields.many2one('res.users', 'User', required=True),
        'category_id': fields.many2one('notification.category',
                                       'Categoria'),
    }

    _defaults = {
        'name': lambda *a: datetime.now().strftime('%Y-%m-%d %H:%M:00'),
        'user_id': lambda *a: a[2]
    }

Notification()



