# -*- coding: utf-8 -*-
from osv import osv


class PoweremailMailbox(osv.osv):
    _name = 'poweremail.mailbox'
    _inherit = 'poweremail.mailbox'

    def send(self, cursor, uid, ids, auto_commit=False, context=None):
        """This method is only for API compatibility.
        """
        if context is None:
            context = {}
        return self.send_this_mail(cursor, uid, ids, context=context)

    def get_notification_state(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        res = {}
        for mailbox in self.browse(cursor, uid, ids, context=context):
            folder = mailbox.folder
            if folder == 'drafts':
                state = 'draft'
            elif folder == 'outbox':
                state = 'outgoing'
            elif folder == 'sent':
                state = 'sent'
            elif folder == 'inbox':
                state = 'received'
            elif folder == 'trash':
                state = 'cancel'
            elif mailbox.state == 'na':
                state = 'exception'
            else:
                state = False
            res[mailbox.id] = state
        return res

    def get_subject(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        res = {}
        for mailbox in self.read(cursor, uid, ids, ['pem_subject'],
                                 context=context):
            res[mailbox['id']] = mailbox['pem_subject']
        return res

PoweremailMailbox()

