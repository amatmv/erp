# -*- coding: utf-8 -*-
{
    "name": "Notification system",
    "description": """
        OpenERP Notifications
        """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Misc",
    "depends":[
        "base",
        "poweremail",
        "poweremail_references"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/notification_security.xml",
        "security/ir.model.access.csv",
        "notifications_data.xml",
        "notifications_menu.xml",
        "notifications_view.xml",
        "notifications_category_view.xml"
    ],
    "active": False,
    "installable": True
}
