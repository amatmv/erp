import wizard
import tools
import base64
import StringIO
import csv
import pooler

view_form_init="""<?xml version="1.0"?>
<form string="Export language">
	<image name="gtk-dialog-info" colspan="2"/>
	<group colspan="2" col="4">
		<separator string="Export translation file" colspan="4"/>
		<label align="0.0" string="Choose a language to export:" colspan="4"/>
		<field name="lang" colspan="4"/>
	</group>
</form>"""

view_form_finish="""<?xml version="1.0"?>
<form string="Export language">
	<image name="gtk-dialog-info" colspan="2"/>
	<group colspan="2" col="4">
		<separator string="Export done" colspan="4"/>
		<field name="data" readonly="1" colspan="3"/>
		<label align="0.0" string="Save this document to a .CSV file and open it with\n your favourite spreadsheet software. The file\n encoding is UTF-8. You have to translate the latest\n column before reimporting it." colspan="4"/>
	</group>
</form>"""

class wizard_module_translation_generation(wizard.interface):
	def _get_language(self, cr, uid, context):
		lang_obj=pooler.get_pool(cr.dbname).get('res.lang')
		ids=lang_obj.search(cr, uid, [('active', '=', True),])
		langs=lang_obj.browse(cr, uid, ids)
		return [(lang.code, lang.translatable and lang.name or 'New language') for lang in langs]

	def _get_file(self, cr, uid, data, context):
		module = pooler.get_pool(cr.dbname).get('ir.module.module').browse(cr, uid, data['ids'][0])
		file=tools.trans_generate(data['form']['lang'], module.name, dbname=cr.dbname)
		buf=StringIO.StringIO()
		writer=csv.writer(buf, 'TINY')
		for row in file:
			writer.writerow(row)
		del file
		out=base64.encodestring(buf.getvalue())
		buf.close()
		return {'data': out}

	fields_form={
		'lang': {'string':'Language', 'type':'selection', 'selection':_get_language,},
	}
	fields_form_finish={
		'data': {'string':'File', 'type':'binary', 'readonly': True,},
	}
	states={
		'init':{
			'actions': [],
			'result': {'type': 'form', 'arch': view_form_init, 'fields': fields_form,
				'state': [
					('end', 'Cancel', 'gtk-cancel'),
					('finish', 'Ok', 'gtk-ok', True)
				]
			}
		},
		'finish':{
			'actions': [_get_file],
			'result': {'type': 'form', 'arch': view_form_finish, 
				'fields': fields_form_finish,
				'state': [
					('end', 'Close', 'gtk-cancel', True)
				]
			}
		},
	}
wizard_module_translation_generation('module.translation.generation')
