# -*- coding: utf-8 -*-
{
    "name": "Wizard to generate string translations from a module",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Translations",
    "depends":[
        "base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "module_translation_generation_wizard.xml"
    ],
    "active": False,
    "installable": True
}
