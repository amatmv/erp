# -*- encoding: utf-8 -*-

from osv import osv, fields

class PartnerBankUnique(osv.osv):
    
    _name = 'res.partner.bank'
    _inherit = 'res.partner.bank'
    
    _sql_constraints = [
        ('account_unique', 
         'unique (partner_id, acc_number)', 
         u"The account number already exists for this partner"),
        ('iban_unique', 
         'unique (partner_id, iban)', 
         u"The iban number already exists for this partner"),
        
    ]
    
PartnerBankUnique()   