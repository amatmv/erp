# -*- coding: utf-8 -*-
{
    "name": "Partner bank unique",
    "description": """Partner bank unique:
    * Cannot repeat the same account number for one partner
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Misc",
    "depends":[
        "base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
