# -*- coding: utf-8 -*-
{
    "name": "report_banner",
    "description": """Banner management""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "mongodb_backend"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/report_banner_security.xml",
        "security/ir.model.access.csv",
        "report_banner_view.xml",
    ],
    "active": False,
    "installable": True
}
