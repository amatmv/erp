# -*- coding: utf-8 -*-
from osv import osv, fields
from mongodb_backend import fields as mongodb_fields
from ast import literal_eval


def mongoize(vals):
    vals['image_mongo'] = vals['image']
    vals['image'] = False


def unmongoize(vals):
    vals['image'] = vals['image_mongo']
    del vals['image_mongo']



class ReportBanner(osv.osv):

    _name = 'report.banner'

    def unlink(self, cursor, uid, ids, context=None):
        # Remove the file from mongodb
        self.write(cursor, uid, ids, {'image_mongo': False})
        return super(ReportBanner, self).unlink(cursor, uid, ids, context)

    def get_banner(self, cursor, uid, model, date, code=None, model_id=None, context=None):
        """
        Get the banner that corresponds given a model and a date.

        :param cursor
        :param uid
        :param model: name of the model we want to filter by
        :param date: date in which we want to choose the banner
        :param code: name of the banner to select.
        :param model_id: If we want to filter the banner by its domain
        :param context
        :return: the field image_mongo of the banner
        """
        if context is None:
            context = {}

        model_obj = self.pool.get(model)

        search_params = [
            ('start_date', '<=', date),
            ('end_date', '>=', date),
            ('res_model', '=', model),
        ]

        if code:
            search_params.append(('code', '=', code))

        # Search all banners given a date
        banners = self.q(cursor, uid).read(
            ['id', 'domain', 'sequence'], order_by=('sequence.asc',)
        ).where(search_params)

        banner_id = None
        # If there are banners and we have some model_id to check the domain
        if banners:
            for banner in banners:
                b_domain = False if not banner['domain'] else literal_eval(banner['domain'])
                # If there is some domain and it's literally-evaluated as True
                if b_domain:
                    if not model_id:
                        continue
                    b_domain.append(['id', '=', model_id])
                    sql = model_obj.q(cursor, uid).select(['id']).where(b_domain)
                    cursor.execute(*sql)
                    # If the model doesn't satisfy the domain, we must check
                    # another banner
                    if not cursor.rowcount:
                        continue
                banner_id = banner['id']
                break
        else:
            sql = self.q(cursor, uid).select(['id']).where([
                ('res_model', '=', model), ('default', '=', True)
            ])
            cursor.execute(*sql)
            banner_id = cursor.fetchone()
            if banner_id:
                banner_id = banner_id[0]
        if banner_id:
            banner_img = self.read(
                cursor, uid, banner_id, ['image_mongo']
            )['image_mongo']
            return banner_img
        return False

    _columns = {
        'name': fields.char('Nom del banner', size=250, required=True, select=1),
        'code': fields.char('Codi', size=32, select=1),
        'sequence': fields.integer('Ordre'),
        'res_model': fields.char('Model associat', size=64, required=True, select=1),
        'start_date': fields.date('Data inicial', required=True),
        'end_date': fields.date('Data final'),
        'default': fields.boolean('Per defecte'),
        'image_mongo': mongodb_fields.gridfs('Imatge PNG o JPG'),
        'description': fields.text('Descripció'),
        'domain': fields.text('Domini de selecció')
    }

    _defaults = {
        'default': lambda *a: False,
    }

    _order = 'sequence asc'


ReportBanner()
