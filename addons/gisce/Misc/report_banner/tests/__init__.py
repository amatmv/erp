# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction


class TestsReportBanners(testing.OOTestCase):

    def setUp(self):
        self.pool = self.openerp.pool

    def test_correct_banner_is_selected(self):

        partner_obj = self.pool.get('res.partner')
        banner_obj = self.pool.get('report.banner')

        with Transaction().start(self.database) as txn:

            cursor = txn.cursor
            uid = txn.user

            partner_1_id = partner_obj.create(cursor, uid, {'name': 'Partner 1'})
            partner_2_id = partner_obj.create(cursor, uid, {'name': 'Partner 2'})

            banner_1 = banner_obj.create(cursor, uid, {
                'name': '1st June Banner',
                'sequence': '0',
                'res_model': 'res.partner',
                'start_date': '2018-06-01',
                'end_date': '2018-06-30',
                'default': False,
                'image_mongo': 'img1',
                'description': False,
                'domain': '[["name", "=", "Partner 1"]]',
            })

            banner_2 = banner_obj.create(cursor, uid, {
                'name': '1st July Banner',
                'sequence': '1',
                'res_model': 'res.partner',
                'start_date': '2018-07-01',
                'end_date': '2018-07-31',
                'default': False,
                'image_mongo': 'img2',
                'description': False,
                'domain': '[["name", "=", "Partner 2"]]',
            })

            banner_default = banner_obj.create(cursor, uid, {
                'name': 'Default Banner',
                'sequence': '2',
                'res_model': 'res.partner',
                'start_date': '2018-01-01',
                'end_date': '9999-12-31',
                'default': True,
                'image_mongo': 'default',
                'description': False,
                'domain': '[]',
            })

            # Default banner
            res = banner_obj.get_banner(cursor, uid, 'res.partner', '2018-06-15')

            self.assertEqual(res, 'default')

            res = banner_obj.get_banner(
                cursor, uid, 'res.partner', '2018-06-15', model_id=partner_1_id,
            )

            self.assertEqual(res, 'img1')

            res = banner_obj.get_banner(
                cursor, uid, 'res.partner', '2018-07-15', model_id=partner_2_id
            )

            self.assertEqual(res, 'img2')

            # We create a banner that is both on june and july, but has a high
            # sequence, so won't be chosen
            banner_obj.create(cursor, uid, {
                'name': 'Partner Banner',
                'sequence': '3',
                'res_model': 'res.partner',
                'start_date': '2018-06-01',
                'end_date': '2018-07-31',
                'default': False,
                'image_mongo': 'img3',
                'description': False,
                'domain': '[["name", "in", ["Partner 1", "Partner 2"]]]',
            })

            res = banner_obj.get_banner(
                cursor, uid, 'res.partner', '2018-06-15', model_id=partner_1_id
            )

            self.assertEqual(res, 'img1')
