# -*- coding: utf-8 -*-
{
    "name": "crm_campaing",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Migrated crm.case.resource.type from v6.1
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "crm"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "crm_campaign_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
