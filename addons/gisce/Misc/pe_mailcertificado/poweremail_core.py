# -*- encoding: utf-8 -*-

from osv import osv, fields
from mailcertificado.api import MailCertificado
import tools


class PoweremailCoreMailCertificado(osv.osv):

    _name = 'poweremail.core_accounts'
    _inherit = 'poweremail.core_accounts'

    def mc_get_attachments(self, cr, uid, mc_mail, context=None):

        attachments = []
        for attachment in mc_mail.pem_attachments_ids:
            file_name = attachment.datas_fname or attachment.name
            attachments.append({'name': file_name,
                                'data': attachment.datas})
        return attachments            

    def mc_send_agreement(self, cr, uid, mc_mail, context=None):
        '''send mail as mailcertificado agreement'''
        user = tools.config.get('mc_user', '')
        password = tools.config.get('mc_pass', '')
        mc_instance = MailCertificado(user, password)
        attachments = self.mc_get_attachments(cr, uid, mc_mail, context)
        accept_method = mc_mail.mc_accept_sms and 1 or None
        accept_sms_number = (mc_mail.mc_accept_sms
                             and mc_mail.mc_accept_sms_number
                             or '')
        notify_sms_number = mc_mail.mc_sms and mc_mail.mc_sms_number or None
        notify_sms_body = mc_mail.mc_sms and mc_mail.mc_sms_body or ''
        result = mc_instance.send_agreement(mc_mail.pem_to,
                                            mc_mail.pem_subject,
                                            mc_mail.pem_body_text,
                                            attachments,
                                            accept_method,
                                            accept_sms_number,
                                            notify_sms_number,
                                            notify_sms_body)
        return result

    def mc_send_agreement_sms(self, cr, uid, mc_mail, context=None):
        '''send mail as mailcertificado sms agreement'''
        user = tools.config.get('mc_user', '')
        password = tools.config.get('mc_pass', '')
        mc_instance = MailCertificado(user, password)
        attachments = self.mc_get_attachments(cr, uid, mc_mail, context)
        phone = self.get_clean_number(cr, uid, mc_mail.pem_to)
        result = mc_instance.send_agreement_sms(phone,
                                                mc_mail.pem_subject,
                                                mc_mail.pem_body_text,
                                                attachments)
        return result

    def mc_send_mail(self, cr, uid, mc_mail, context=None):
        '''send mail as mailcertificado mail'''
        user = tools.config.get('mc_user', '')
        password = tools.config.get('mc_pass', '')
        mc_instance = MailCertificado(user, password)
        attachments = self.mc_get_attachments(cr, uid, mc_mail, context)
        notify_sms_number = mc_mail.mc_sms and mc_mail.mc_sms_number or None
        notify_sms_body = mc_mail.mc_sms and mc_mail.mc_sms_body or ''
        result = mc_instance.send_mail(mc_mail.pem_to,
                                       mc_mail.pem_subject,
                                       mc_mail.pem_body_text,
                                       attachments,
                                       notify_sms_number,
                                       notify_sms_body)
        return result

    def mc_send_sms(self, cr, uid, mc_mail, context=None):
        '''send sms as mailcertificado sms'''
        user = tools.config.get('mc_user', '')
        password = tools.config.get('mc_pass', '')
        mc_instance = MailCertificado(user, password)
        sms_phone = self.get_clean_number(cr, uid, mc_mail.pem_subject)
        result = mc_instance.send_registered_sms(sms_phone,
                                       mc_mail.pem_body_text)
        return result

    def get_clean_number(self, cursor, uid, text):
        return ''.join([x for x in text if x.isdigit()])

PoweremailCoreMailCertificado()
