# -*- encoding: utf-8 -*-

from osv import osv, fields


class MailCertificadoHistory(osv.osv):

    _name = 'mc.history'

    _columns = {
        'name': fields.char('Message ID', size=20, required=True),
        'transaction_id': fields.char('Transaction ID', size=20),
        'status': fields.char('State', required=True, size=20),
        'date': fields.datetime('Date', required=True)
    }

MailCertificadoHistory()
