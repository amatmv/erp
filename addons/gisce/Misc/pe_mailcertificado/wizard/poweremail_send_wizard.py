# -*- encoding: utf-8 -*-

from osv import osv, fields
from pe_mailcertificado import defs
from poweremail.poweremail_template import get_value
from tools.translate import _

class PoweremailSendWizardMailCertificado(osv.osv_memory):

    _name = 'poweremail.send.wizard'
    _inherit = 'poweremail.send.wizard'

    def send_mail(self, cursor, uid, ids, context=None):

        if not context:
            context = {}
        wizard = self.browse(cursor, uid, ids[0])

        if wizard.mc_type != 'nocert':
            if len(wizard.to.split(',')) > 1:
                raise osv.except_osv('Error',
                                     _(u"You cannot send certified mails with "
                                       u"more than one addresse"))

            context.update({'folder': 'mc_outbox'})

        return super(PoweremailSendWizardMailCertificado,
                     self).send_mail(cursor, uid, ids, context=context)

    def get_generated(self, cursor, uid, ids, context=None):

        if not context:
            context = {}
        wizard = self.browse(cursor, uid, ids[0])
        if wizard.mc_type != 'nocert':
            context.update({'folder': 'mc_outbox'})
        return super(PoweremailSendWizardMailCertificado,
                     self).get_generated(cursor, uid, ids, context=context)

    def save_to_mailbox(self, cursor, uid, ids, context=None):

        mail_obj = self.pool.get('poweremail.mailbox')

        def get_end_value(id, value):
            if len(context['src_rec_ids']) > 1: # Multiple Mail: Gets value from the template
                template = self._get_template(cursor, uid, context)
                if not template:
                    return False
                return self.get_value(cursor, uid, template, value, context, id)
            else:
                return value

        mail_ids = super(PoweremailSendWizardMailCertificado,
                     self).save_to_mailbox(cursor, uid, ids, context=context)
        wizard = self.browse(cursor, uid, ids[0])
        for mail_id in mail_ids:
            mail = mail_obj.browse(cursor, uid, mail_id)
            source_id = mail.reference.split(',')[1]
            values = {
                'mc_type': wizard.mc_type,
            }
            if wizard.mc_type != 'nocert':
                mc_vals = {
                    'mc_sms': get_end_value(source_id, wizard.mc_sms),
                    'mc_sms_number': get_end_value(source_id,
                                                   wizard.mc_sms_number),
                    'mc_sms_body': get_end_value(source_id,
                                                 wizard.mc_sms_body),
                    'mc_accept_sms': get_end_value(source_id,
                                                   wizard.mc_accept_sms),
                    'mc_accept_sms_number': get_end_value(source_id,
                                                wizard.mc_accept_sms_number),
                }
                values.update(mc_vals)
            mail.write(values, context=context)

        return mail_ids

    def _mc_get_template_value(self, cr, uid, field, context=None):
        template = self._get_template(cr, uid, context)
        if not template:
            return False
        if len(context['src_rec_ids']) > 1: # Multiple Mail: Gets original template values for multiple email change
            return getattr(template, field)
        else: # Simple Mail: Gets computed template values
            return self.get_value(cr, uid, template, getattr(template, field), context)

    _columns = {
        'mc_type': fields.selection(defs._MC_TYPE, 'MC Type',
                                    help="MailCertificado sending type"),
        'mc_sms': fields.boolean('MC SMS'),
        'mc_sms_number': fields.char('MC SMS Number', size=250),
        'mc_sms_body': fields.char('MC SMS Body', size=250),
        'mc_accept_sms': fields.boolean('MC Accept SMS'),
        'mc_accept_sms_number': fields.char('MC Accept SMS Number', size=250),
    }

    _defaults = {
        'mc_type': lambda self, cr, uid, ctx: self._mc_get_template_value(cr, uid, 'mc_def_type', ctx),
        'mc_sms': lambda self, cr, uid, ctx: self._mc_get_template_value(cr, uid, 'mc_def_sms', ctx),
        'mc_sms_number': lambda self, cr, uid, ctx: self._mc_get_template_value(cr, uid, 'mc_def_sms_number', ctx),
        'mc_sms_body': lambda self, cr, uid, ctx: self._mc_get_template_value(cr, uid, 'mc_def_sms_body', ctx),
        'mc_accept_sms': lambda self, cr, uid, ctx: self._mc_get_template_value(cr, uid, 'mc_def_accept_sms', ctx),
        'mc_accept_sms_number': lambda self, cr, uid, ctx: self._mc_get_template_value(cr, uid, 'mc_def_accept_sms_number', ctx),
    }

PoweremailSendWizardMailCertificado()
