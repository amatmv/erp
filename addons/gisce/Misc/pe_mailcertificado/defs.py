# -*- encoding: utf-8 -*-

_MC_TYPE = [
    ('nocert', 'No certified'),
    ('mail', 'Certified E-mail'),
    ('agreement', 'Certified Agreement'),
    ('agreement_sms', 'SMS Agreement'),
    ('sms', 'Certified sms'),
]