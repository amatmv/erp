# -*- encoding: utf-8 -*-

from osv import osv, fields
import defs


class PoweremailTemplateMailCertificado(osv.osv):

    _name = 'poweremail.templates'
    _inherit = 'poweremail.templates'

    _columns = {
        'mc_def_type': fields.selection(defs._MC_TYPE, 'MC Type',
                                        help="MailCertificado sending type"),
        'mc_def_sms': fields.boolean('MC SMS'),
        'mc_def_sms_number': fields.char('MC SMS Number', size=250),
        'mc_def_sms_body': fields.char('MC SMS Body', size=250),
        'mc_def_accept_sms': fields.boolean('MC Accept SMS'),
        'mc_def_accept_sms_number': fields.char('MC Accept SMS Number', size=250),
    }

    _defaults = {
        'mc_def_type': lambda *a: 'nocert',
    }

PoweremailTemplateMailCertificado()
