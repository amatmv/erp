# -*- encoding: utf-8 -*-

from osv import osv, fields
import defs
import tools
from tools.translate import _
import pooler
import time
from mailcertificado.api import MailCertificado
import base64


class PoweremailMailboxMailCertificado(osv.osv):

    _name = 'poweremail.mailbox'
    _inherit = 'poweremail.mailbox'

    def copy(self, cursor, uid, id, default=None, context=None):

        if default is None:
            default = {}

        default.update({
            'mc_type': 'nocert'
        })

        return super(PoweremailMailboxMailCertificado,
                     self).copy(cursor, uid, id, default=default,
                                context=context)

    def mc_run_mail_scheduler(self, cr, uid, context=None):

        try:
            self.mc_send_all_mail(cr, uid, context=context)
        except Exception, e:
            LOGGER = netsvc.Logger()
            LOGGER.notifyChannel(
                _("Power Email"),
                netsvc.LOG_ERROR,
                _("Error sending MailCertificado: %s") % str(e))

    def mc_send_all_mail(self, cr, uid, context=None):
        '''send mails in mailcertificado outbox'''

        if context is None:
            context = {}
        filters = [('folder', '=', 'mc_outbox'),
                   ('state', '!=', 'sending')]
        if 'filters' in context.keys():
            for each_filter in context['filters']:
                filters.append(each_filter)
        limit = context.get('limit', None)
        order = "priority desc, date_mail desc"
        ids = self.search(cr, uid, filters,
                          limit=limit, order=order,
                          context=context)
        # To prevent resend the same emails in several send_all_mail() calls
        # We put this in a new cursor/transaction to avoid concurrent
        # transaction isolation problems
        db = pooler.get_db_only(cr.dbname)
        cr_tmp = db.cursor()
        try:
            self.write(cr_tmp, uid, ids, {'state': 'sending'}, context)
            cr_tmp.commit()
        except:
            cr_tmp.rollback()
        finally:
            cr_tmp.close()
        #send mails one by one
        self.mc_send_this_mail(cr, uid, ids, context)
        return True

    def mc_send_this_mail(self, cr, uid, ids, context=None):
        '''send mails using mailcertificado one by one'''

        core_obj = self.pool.get('poweremail.core_accounts')
        if not context:
            context = {}
        for id in ids:
            try:
                mc_mail = self.browse(cr, uid, id)
                # Do not allow resending a message if a message_id is given
                if mc_mail.mc_message_id:
                    raise osv.except_osv('Error',
                                         u"This message already have "
                                         u"a MC message ID")
                action = 'mc_send_%s' % mc_mail.mc_type
                result = getattr(core_obj, action)(cr, uid, mc_mail)
                if result:
                    now = time.strftime("%Y-%m-%d %H:%M:%S")
                    mc_mail.write({'folder': 'mc_sent',
                                   'mc_message_id': result,
                                   'date_mail': now}, context)
                    self.historise(cr, uid, [id],
                                   _(u"MC Email sent successfully"),
                                   context)
            except osv.except_osv, error:
                error_msg = 'Error: %s' % error.value
                context = {'notify_errors': True}
                self.historise(cr, uid, [id], error_msg, context, error=True)
            except Exception, error:
                error_msg = 'Error: %s' % error
                context = {'notify_errors': True}
                self.historise(cr, uid, [id], error_msg, context, error=True)
            self.write(cr, uid, id, {'state': 'na'}, context)
        return True

    def send_this_mail(self, cr, uid, ids, context=None):

        search_params = [('mc_type', '<>', 'nocert'),
                         ('id', 'in', ids)]
        mc_ids = self.search(cr, uid, search_params)
        no_mc_ids = list(set(ids) - set(mc_ids))
        if mc_ids:
            self.mc_send_this_mail(cr, uid, mc_ids, context=context)
        if no_mc_ids:
            super(PoweremailMailboxMailCertificado,
                  self).send_this_mail(cr, uid, no_mc_ids, context=context)
        return True

    def mc_get_certificate(self, cr, uid, ids, context=None):
        '''downloads and attachs the certificate'''

        attach_obj = self.pool.get('ir.attachment')

        user = tools.config.get('mc_user', '')
        password = tools.config.get('mc_pass', '')
        mc_instance = MailCertificado(user, password)

        for id in ids:
            message = self.browse(cr, uid, id)
            if message.mc_type == 'nocert':
                raise osv.except_osv('Error',
                                     _(u"This is not a MailCertificado "
                                       u"message"))
            mc_message_id = message.mc_message_id
            if not mc_message_id:
                raise osv.except_osv('Error',
                                     _(u"No MailCertificado "
                                       u"message identifier"))
            #Search if the certificate already exists
            cert_file_name = 'certificado_%s.pdf' % mc_message_id
            search_params = [('datas_fname', '=', cert_file_name),
                             ('res_model', '=', 'poweremail.mailbox'),
                             ('res_id', '=', id)]
            attach_ids = attach_obj.search(cr, uid, search_params)
            if attach_ids:
                continue
            res = mc_instance.get_message_certificate(mc_message_id)
            #Attach the certificate
            attach_vals = {
                'name': _(u"Certificate for %s") % mc_message_id,
                'datas': base64.b64encode(base64.b64decode(res['data'])),
                'datas_fname': res['name'],
                'description': _("No Description"),
                'res_model': 'poweremail.mailbox',
                'res_id': id
            }
            attach_obj.create(cr, uid, attach_vals)
        return True

    _folders = [
        ('inbox', 'Inbox'),
        ('drafts', 'Drafts'),
        ('outbox', 'Outbox'),
        ('trash', 'Trash'),
        ('followup', 'Follow Up'),
        ('sent', 'Sent Items'),
        ('mc_outbox', 'MC Outbox'),
        ('mc_sent', 'MC Sent')
    ]

    _columns = {
        'mc_message_id': fields.char('Message ID', size=20, readonly=True,
                                     help="MailCertificado Message ID"),
        'mc_type': fields.selection(defs._MC_TYPE, 'MC Type',
                                    help="MailCertificado sending type"),
        'mc_sms': fields.boolean('MC SMS'),
        'mc_sms_number': fields.char('MC SMS Number', size=15),
        'mc_sms_body': fields.char('MC SMS Body', size=140),
        'mc_accept_sms': fields.boolean('MC Accept SMS'),
        'mc_accept_sms_number': fields.char('MC Accept SMS Number', size=15),
        'folder': fields.selection(_folders, 'Folder', required=True),
    }

PoweremailMailboxMailCertificado()
