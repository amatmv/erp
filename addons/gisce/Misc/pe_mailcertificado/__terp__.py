# -*- coding: utf-8 -*-
{
    "name": "Poweremail Certificated",
    "description": """
    Sends mails using mailcertificado API
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Misc",
    "depends":[
        "poweremail",
        "poweremail_references"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "poweremail_mailbox_view.xml",
        "poweremail_template_view.xml",
        "mc_objects_view.xml",
        "poweremail_scheduler_data.xml",
        "wizard/poweremail_send_wizard_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
