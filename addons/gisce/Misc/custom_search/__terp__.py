# -*- coding: utf-8 -*-
{
    "name": "Custom Search",
    "description": """
Allows running sql queries and get results in tree view
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Misc",
    "depends":[
        "base"
    ],
    "init_xml": [],
    "demo_xml":[
        "custom_search_demo.xml"
    ],
    "update_xml":[
        "security/custom_search_security.xml",
        "custom_search_view.xml",
        "wizard/wizard_execute_custom_search_view.xml",
        "wizard/wizard_export_custom_search_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
