# -*- coding: utf-8 -*-
import base64
import unittest

from destral import testing
from destral.transaction import Transaction
from expects import expect, raise_error
from osv.orm import except_orm
from addons import get_module_resource
import time
from tools.misc import cache


class CustomSearchTests(testing.OOTestCase):
    def test_get_fields_returns_fields_in_order_when_defined(self):
        custom_search_obj = self.openerp.pool.get('custom.search')
        column_obj = self.openerp.pool.get('custom.search.column')
        imd_obj = self.openerp.pool.get('ir.model.data')

        expected_params = ['Name', 'Credit Limit', 'Website']
        new_expected_params = ['Credit Limit', 'Name', 'Website']

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            custom_search_id = imd_obj.get_object_reference(
                cursor, uid, 'custom_search', 'custom_search_0001'
            )[1]

            recived_params = custom_search_obj.get_fields(
                cursor, uid, custom_search_id
            )

            assert recived_params == expected_params

            name_column_id = imd_obj.get_object_reference(
                cursor, uid, 'custom_search', 'custom_search_0001_column_0001'
            )[1]

            column_obj.write(cursor, uid, name_column_id, {'sequence': 15})

            new_recived_params = custom_search_obj.get_fields(
                cursor, uid, custom_search_id
            )

            assert new_recived_params == new_expected_params

    def test_get_fields_returns_fields_when_not_defined(self):
        custom_search_obj = self.openerp.pool.get('custom.search')
        imd_obj = self.openerp.pool.get('ir.model.data')

        expected_params = ['Name', 'Credit Limit', 'Website']

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            custom_search_id = imd_obj.get_object_reference(
                cursor, uid, 'custom_search', 'no_columns_custom_search'
            )[1]

            recived_params = custom_search_obj.get_fields(
                cursor, uid, custom_search_id
            )

            assert len(expected_params) == len(recived_params)
            for param in recived_params:
                assert param in expected_params

    def test_csv_generates_file_correctly_with_defined_columns(self):
        wizard_obj = self.openerp.pool.get('wizard.export.custom.search')
        imd_obj = self.openerp.pool.get('ir.model.data')

        search_result_path = get_module_resource(
            'custom_search', 'tests', 'fixtures', 'search_result.csv')
        with open(search_result_path, 'r') as f:
            search_result = f.read()

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            custom_search_id = imd_obj.get_object_reference(
                cursor, uid, 'custom_search', 'custom_search_0002'
            )[1]

            wizard_id = wizard_obj.create(cursor, uid, {
                'search_id': custom_search_id,
                'file_type': 'csv',
            })

            wizard_obj.build_report(cursor, uid, [wizard_id])

            wizard = wizard_obj.browse(cursor, uid, wizard_id)

            assert base64.b64decode(wizard.report) == search_result

    def test_Excel_generates_file_correctly_with_defined_columns(self):
        wizard_obj = self.openerp.pool.get('wizard.export.custom.search')
        imd_obj = self.openerp.pool.get('ir.model.data')

        import pandas as pd
        from StringIO import StringIO

        search_result_path = get_module_resource(
            'custom_search', 'tests', 'fixtures', 'search_result.xlsx')

        df1 = pd.read_excel(search_result_path)

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            custom_search_id = imd_obj.get_object_reference(
                cursor, uid, 'custom_search', 'custom_search_0002'
            )[1]

            wizard_id = wizard_obj.create(cursor, uid, {
                'search_id': custom_search_id,
                'file_type': 'xls',
            })

            wizard_obj.build_report(cursor, uid, [wizard_id])

            wizard = wizard_obj.browse(cursor, uid, wizard_id)

            report = StringIO(base64.b64decode(wizard.report))
            df2 = pd.read_excel(report)

            self.assertTrue(df1.equals(df2))
