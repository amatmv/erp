# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) Joan M. Grande <jgrande@el-gas.es>
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields
from tools.translate import _
from string import Template
import pooler
from unidecode import unidecode

class CustomSearch(osv.osv):
    
    _name = 'custom.search'
    
    def execute_query(self, cursor, uid, ids, context=None):
        
        if not context:
            context = {}
        context.update({'search_id': ids[0]})
        
        vals = {
            'name': 'Results',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'view_id': False,
            'res_model': 'custom.search.results',
            'type': 'ir.actions.act_window',
            'context': context,
        }

        return vals
    
    _columns = {
        'name':fields.char('Name', size=200, required=True),
        'query':fields.text('Query', required=True),
    }
    
CustomSearch()

class CustomSearchParams(osv.osv):
    
    _name = 'custom.search.param'
    
    _columns = {
        'name':fields.char('Name', size=100, required=True),
        'short_desc':fields.char('Short Description', size=240),
        'desc':fields.text('Description'),
        'value':fields.char('Value', size=100, required=True),
        'search_id':fields.many2one('custom.search', 'Search', required=True)
    }
    
CustomSearchParams()

class CustomSearchColumn(osv.osv):

    _name = 'custom.search.column'
    _order = 'sequence asc'

    _columns = {
        'name':fields.char('Name', size=100, required=True),
        'sequence':fields.integer('Sequence', required=True),
        'search_id':fields.many2one('custom.search', 'Search', required=True)
    }

CustomSearchColumn()

class CustomSearch2(osv.osv):

    _name = 'custom.search'
    _inherit = 'custom.search'

    def get_fields(self, cursor, uid, search_id, context=None):
        """
        Returns the name of the fields of the query

        :param cursor: Database cursor
        :param uid: User id
        :param search_id:
        :param context:
        :return:
        """

        if context is None:
            context = {}

        search = self.browse(cursor, uid, search_id, context)

        if search.column_ids:
            column_obj = self.pool.get('custom.search.column')
            columns_ids = [x.id for x in search.column_ids]
            columns = column_obj.read(cursor, uid, columns_ids, ['name'])
            return [x['name'] for x in columns]
        template = Template(search.query)
        #Build a dict with the values of this search
        substitution = {}
        for param in search.param_ids:
            substitution.update({param.name: param.value})

        query = template.substitute(substitution)
        if query[-1] == ";":
            query = query[:-1]
        # limit to one result only to speed up the query
        query += " limit 1"
        # Open a readonly cursor to prevent changes in db
        try:
            ro_cursor = pooler.get_db_only(cursor.dbname).cursor()
            ro_cursor.execute("set session default_transaction_read_only to true;")
            ro_cursor.execute(query)
            res = ro_cursor.fetchone()
            columns = [d.name for d in ro_cursor.description]
            ro_cursor.execute("set session default_transaction_read_only to false;")
            ro_cursor.commit()
        except Exception as e:
            ro_cursor.rollback()
            raise osv.except_osv(u'Error', '%s'%(e))
        finally:
            ro_cursor.close()
            
        if not res:
            raise osv.except_osv(_('Warning'), _('No results for this query'))
        return columns
    
    def get_query(self, cursor, uid, search_id, context=None):
        '''returns the query with parameter substitution'''
        if context is None:
            context = {}
        
        search = self.browse(cursor, uid, search_id) 
        
        template = Template(search.query)
        #Build a dict with the values of this search
        substitution = {}
        for param in search.param_ids:
            substitution.update({param.name: param.value})
        
        query = template.substitute(substitution)
        #Open a readonly cursor to prevent changes in db
        try:
            ro_cursor = pooler.get_db_only(cursor.dbname).cursor()
            ro_cursor.execute("set session default_transaction_read_only to true;")
            ro_cursor.execute(query)
            res = ro_cursor.dictfetchall()
            ro_cursor.execute("set session default_transaction_read_only to false;")
            ro_cursor.commit()
        except Exception, e:
            ro_cursor.rollback()
            raise osv.except_osv(u'Error', '%s'%(e))
        finally:
            ro_cursor.close()

        if not res:
            raise osv.except_osv(_('Warning'), _('No results for this query'))
        return res
    
    _columns = {
        'param_ids':fields.one2many('custom.search.param','search_id',
                                    'Parameters'),
        'column_ids':fields.one2many('custom.search.column','search_id',
                                    'Columns')

    }
    
CustomSearch2()

class CustomSearchResults(osv.osv):
    
    _name = 'custom.search.results'
    _table = 'custom_search'
    _inherit = 'custom.search'
    
    def create(self, cursor, uid, vals, context=None):
        raise osv.except_osv('Error !', 'You cannot add an entry to this view !')

    def unlink(self, *args, **argv):
        raise osv.except_osv('Error !', 'You cannot delete an entry of this view !')
    
    def write(self, cursor, uid, ids, vals, context=None):
        raise osv.except_osv('Error !', 'You cannot write an entry in this view !')
    
    def read(self, cursor, uid, ids, fields=None, context=None, load='_classic_read'):
        '''execute query and read values'''
        
        #result = super(CustomSearchResults,self).read(cursor, uid, ids, 
        #                                              fields,context)
        search_obj = self.pool.get('custom.search')
        
        if not context:
            context = {}
        search_id = context.get('search_id',False)
        if not search_id:
            raise osv.except_osv(_('Error'), _('Search id not found'))
        #Add id and last_update field for all the records
        result = search_obj.get_query(cursor, uid, search_id, context)

        res = []
        for i in range(len(result)):
            res.append({'id':i+1, '__last_update': False})
            for key, item in result[i].iteritems():
                if key not in fields:
                    key = unidecode(u'{}'.format(key))
                if item is None:
                    res[i][key] = False
                else:
                    res[i][key] = item
        return res
        
    
    def fields_get(self, cursor, uid, fields=None, 
                   context=None, read_access=True):
        '''return dynamic field list from search'''
        
        #result = super(CustomSearchResults,self).fields_get(cursor, uid, 
        #                                         fields, context)
        result = {}
        search_obj = self.pool.get('custom.search')
        
        if not context:
            context = {}
        search_id = context.get('search_id',False)
        if not search_id:
            raise osv.except_osv(_('Error'), _('Search id not found'))
        
        fields = search_obj.get_fields(cursor, uid, search_id)
        for field in fields:
            result[field] = {'string': field,'type': 'char','size': 50}

        return result
    
    def fields_view_get(self, cursor, uid, view_id=None, 
                        view_type='form', context=None, toolbar=False):
        '''return dynamic form view'''
        
        #result = super(CustomSearchResults,self).fields_view_get(cursor, uid,
        #                                         view_id, view_type, context)
        result = {}
        search_obj = self.pool.get('custom.search')
        
        if not context:
            context = {}

        search_id = context.get('search_id',False)
        if not search_id:
            raise osv.except_osv(_('Error'), _('Search id not found'))
        fields = search_obj.get_fields(cursor, uid, search_id)
        xml = u'''<?xml version="1.0" encoding="utf-8"?>
                     <%s editable="top"> ''' % (view_type,)
        for field in fields:
            xml += u'''<field name="%s"/>''' % (field, )
        xml += u'''</%s>''' % (view_type,)
        result['arch'] = unidecode(xml)
        result_fields = {}
        for field, values in self.fields_get(cursor, uid, [], context).items():
            values['string'] = unidecode(values['string'])
            result_fields[unidecode(field)] = values
        result['fields'] = result_fields
        return result
        
        
CustomSearchResults()