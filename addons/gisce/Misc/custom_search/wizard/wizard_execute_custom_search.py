# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) Joan M. Grande
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv, fields

class ExecuteCustomSearch(osv.osv_memory):
    
    _name = "wizard.execute.custom.search"
    
    def action_execute(self, cursor, uid, ids, context=None):
        
        search_obj = self.pool.get('custom.search.results')
        wizard = self.browse(cursor, uid, ids[0])
        
        if not context:
            context = {}
        context.update({'search_id': wizard.search_id.id})
        
        vals = {
            'name': wizard.search_id.name,
            'view_type': 'form',
            'view_mode': 'tree',
            'view_id': False,
            'res_model': 'custom.search.results',
            'type': 'ir.actions.act_window',
            'context': context,
        }

        return vals

    _columns = {
        'search_id':fields.many2one('custom.search','Custom Search', required=True),
    }

ExecuteCustomSearch()