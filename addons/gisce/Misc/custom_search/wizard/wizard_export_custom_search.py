from osv import osv, fields
import StringIO
import csv
import base64
import pandas as pd
from slugify import slugify


class ExportCustomSearch(osv.osv_memory):
    _name = "wizard.export.custom.search"

    def build_report(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        search_obj = self.pool.get('custom.search')
        results_obj = self.pool.get('custom.search.results')
        wizard = self.browse(cursor, uid, ids[0], context=context)

        context.update({'search_id': wizard.search_id.id})
        query_fields = search_obj.get_fields(
            cursor, uid, wizard.search_id.id, context
        )
        data = results_obj.read(cursor, uid, ids, query_fields, context)
        name = slugify(wizard.search_id.name)
        if wizard.file_type == "csv":
            report = self.build_report_csv(
                data, query_fields, delimiter=wizard.delimiter,
                use_quoting=wizard.use_quotes,
                decimal_separator=wizard.decimal_separator
            )
            extension = '.csv'
        else:
            report = self.build_report_xls(data, query_fields)
            extension = '.xlsx'
        wizard.write({
            'report': base64.b64encode(report),
            'buscat': True,
            'filename_report': '{0}{1}'.format(name, extension)
        })
        return True

    def build_report_csv(self, search_result, query_fields, delimiter=',',
                         use_quoting=True, decimal_separator=','):
        df = pd.DataFrame(search_result, columns=query_fields)
        output_report = StringIO.StringIO()
        quoting = csv.QUOTE_MINIMAL
        if use_quoting:
            quoting = csv.QUOTE_NONNUMERIC
        df.to_csv(
            output_report, sep=delimiter, quoting=quoting,
            decimal=decimal_separator, index=None
            )
        value = output_report.getvalue()
        output_report.close()
        return value

    def build_report_xls(self, search_result, query_fields):
        output_report = StringIO.StringIO()
        df = pd.DataFrame(search_result, columns=query_fields)
        writer = pd.ExcelWriter(output_report, engine='xlsxwriter')
        df.to_excel(writer, sheet_name='Sheet1', index=None)
        writer.save()
        value = output_report.getvalue()
        output_report.close()
        return value

    _columns = {
        'search_id': fields.many2one('custom.search', 'Custom Search',
                                     required=True),
        'report': fields.binary('Search Result'),
        'buscat': fields.boolean(),
        'file_type': fields.selection([('csv', 'CSV'), ('xls', 'Excel')],
                                      string='Tipus fitxer'),
        'filename_report': fields.char('Nom fitxer exportat', size=256),
        'delimiter': fields.char('Utilitzar el delimitador', size=1),
        'use_quotes': fields.boolean('Utilitzar cometes'),
        'decimal_separator': fields.selection(
                [(',', ','), ('.', '.')], 'Separador decimal'
        )
    }

    _defaults = {
        'buscat': lambda *a: False,
        'file_type': lambda *a: 'csv',
        'delimiter': lambda *a: ',',
        'use_quotes': lambda *a: True,
        'decimal_separator': lambda  *a: ',',
    }


ExportCustomSearch()
