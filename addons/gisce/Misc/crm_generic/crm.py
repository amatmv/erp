# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright Joan M. Grande <jgrande@el-gas.es>
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields
from tools.translate import _


class CrmExtended(osv.osv):

    _name = 'crm.case'
    _inherit = 'crm.case'

    def create_case_generic(self, cursor, uid, model_ids, context=None,
                                  description='Generic Case',
                                  section='CASE',
                                  category=None,
                                  assign=None,
                                  extra_vals=None):
        '''create a crm case from any model'''

        if not context:
            context = {}
        if not extra_vals:
            extra_vals = {}

        section_obj = self.pool.get('crm.case.section')
        categ_obj = self.pool.get('crm.case.categ')
        user_obj = self.pool.get('res.users')
        #Search the section
        section_id = extra_vals.get('section_id')
        if section_id is None:
            search_params = [('code', '=', section)]
            section_ids = section_obj.search(cursor, uid, search_params)
            if len(section_ids) == 1:
                section_id = section_ids[0]
            else:
                raise osv.except_osv(_(u"Error"),
                                     _(u"Section %s not found" % section))

        #Search the category
        search_params = [('categ_code', '=', category),
                         ('section_id', '=', section_id)]
        categ_ids = categ_obj.search(cursor, uid, search_params)
        if len(categ_ids) == 1:
            categ_id = categ_ids[0]
        else:
            categ_id = None

        #Search the user
        search_params = [('login', '=', assign)]
        user_ids = user_obj.search(cursor, uid, search_params)
        if len(user_ids) == 1:
            user_id = user_ids[0]
        else:
            user_id = None

        case_ids = []
        vals = {'name': description,
                'description': description,
                'section_id': section_id,
                'categ_id': categ_id,
                'user_id': user_id,
                }
        #Update vals with extra_vals values
        vals.update(extra_vals)
        model = context.get('model', False)
        if model:
            #Search if we can associate the model with a case
            link_obj = self.pool.get('res.request.link')
            search_params = [('object', '=', '%s' % model)]
            link_ids = link_obj.search(cursor, uid, search_params)
            if not link_ids:
                raise osv.except_osv(_(u"Error"),
                                     _(u"Model %s cannot be associated"
                                       u" to a CRM case" % (model)))

            model_obj = self.pool.get(model)
            for objecte in model_obj.browse(cursor, uid,
                                            model_ids, context=context):
                vals.update({'ref': '%s,%i' % (model, objecte.id)})
                case_ids.append(self.create(cursor, uid, vals, context))
        else:
            case_ids.append(self.create(cursor, uid, vals, context))

        return case_ids

    _columns = {
        'create_uid': fields.many2one('res.users', 'User',
                                     required=False, readonly=True),

    }

CrmExtended()

class crm_case_categ(osv.osv):
    _name = "crm.case.categ"
    _inherit = "crm.case.categ"

    _columns = {
      'categ_code': fields.char('Category Code', size=64),
    }

crm_case_categ()
