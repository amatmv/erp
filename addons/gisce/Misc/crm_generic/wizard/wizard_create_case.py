# -*- encoding: utf-8 -*-

from osv import osv
from osv import fields
from datetime import datetime


class WizardCreateCase(osv.osv_memory):

    _name = 'wizard.create.case'

    def action_case_generic(self, cursor, uid, ids, context=None):
        '''creates a crm case from any model'''

        wizard = self.browse(cursor, uid, ids[0])
        case_obj = self.pool.get('crm.case')

        if not context:
            context = {}

        model_ids = context.get('active_ids', [])
        today = datetime.strftime(datetime.now(), '%Y-%m-%d')
        extra_vals = {
            'date': wizard.date or today,
            'section_id': wizard.section_id.id
        }

        case_ids = case_obj.create_case_generic(cursor, uid,
                    model_ids,
                    context=context,
                    description=wizard.title,
                    section=wizard.section_id.code,
                    category=wizard.categ_id and \
                                wizard.categ_id.categ_code or None,
                    assign=wizard.user_id.login,
                    extra_vals=extra_vals)

        vals = {
            'name': 'Cases',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'crm.case',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', case_ids)]
        }

        return vals

    _columns = {
        'title': fields.char('Títle', size=128, required=True,
                            help='Description of all the generated cases', ),
        'section_id': fields.many2one('crm.case.section',
                                     'Section', required=True),
        'categ_id': fields.many2one('crm.case.categ',
                                   'Category', required=False),
        'user_id': fields.many2one('res.users', 'Assign', required=True),
        'date': fields.date('Date', help=u"Blank for using today's date"),

    }

WizardCreateCase()
