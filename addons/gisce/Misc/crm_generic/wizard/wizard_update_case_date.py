# -*- encoding: utf-8 -*-

from osv import osv
from osv import fields


class WizardUpdateCaseDate(osv.osv_memory):

    _name = 'wizard.update.case.date'

    def action_update_date(self, cursor, uid, ids, context=None):

        if not context:
            context = {}

        case_obj = self.pool.get('crm.case')

        wizard = self.browse(cursor, uid, ids[0])
        case_ids = context.get('active_ids', [])
        new_date = wizard.new_date
        case_obj.write(cursor, uid, case_ids, {'date': new_date},
                       context=context)
        return {}

    _columns = {
        'new_date': fields.datetime('New date', required=True),
    }

WizardUpdateCaseDate()
