# -*- coding: utf-8 -*-
{
    "name": "CRM Extended",
    "description": """
Extend crm funcionalities
  * Generic wizard for creating an order
  * Uid in case view
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "CRM",
    "depends":[
        "crm"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "crm_data.xml",
        "crm_view.xml",
        "wizard/wizard_create_case_view.xml",
        "wizard/wizard_update_case_date_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
