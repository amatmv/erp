# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) Joan M. Grande <jgrande@el-gas.es>
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields
from tools.translate import _
from giscedata_comerdist import OOOPPool, Sync
from datetime import datetime

class WizardCreateCaseComerdist(osv.osv_memory):

    _name = 'wizard.create.case'
    _inherit = 'wizard.create.case'

    def action_case_generic(self, cursor, uid, ids, context=None):
        '''creates a crm case from any model'''
        
        if not context:
            context = {}
            
        wizard = self.browse(cursor, uid, ids[0])        
        case_obj = self.pool.get('crm.case')
        
        model_ids = context.get('active_ids',[])
        today = datetime.strftime(datetime.now(), '%Y-%m-%d')
        extra_vals = {'date': wizard.date or today}

        if not wizard.sync:

            case_ids = case_obj.create_case_generic(cursor, uid, 
                        model_ids,
                        context=context,
                        description = wizard.title,
                        section = wizard.section_id.code, 
                        category = wizard.categ_id and wizard.categ_id.categ_code or None,
                        assign=wizard.user_id.login,
                        extra_vals=extra_vals)
       
            vals = {
            'name': 'Cases',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'crm.case',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', case_ids)],
            }

            return vals
        else:
            #Create de case using comerdist
            if not context.get('model', False):
                raise osv.except_osv(_(u"Error"),
                                 _(u"Model not found"))
                return False
            else:
                model = context.get('model',False)
            model_obj = self.pool.get(model)
            if not hasattr(model_obj, 'must_be_synched'):
                raise osv.except_osv(_(u"Error"),
                                     _(u"Cannot sincronize %s model"%model))
            #Get sync config and remote object
            remote_ids = []
            for object in model_obj.browse(cursor, uid, model_ids):
                if object.must_be_synched(context):
                    config_id = object.get_config().id
                    ooop = OOOPPool.get_ooop(cursor, uid, config_id)
                    sync = Sync(cursor, uid, ooop, model, config=config_id)
                    obj = sync.get_object(object.id)
                    remote_ids.append(obj.id)
            #If results found, create case with sync
            if remote_ids:
                proxy = OOOPPool.get_ooop(cursor, uid, config_id)
                case_proxy = proxy.CrmCase
                case_proxy.create_case_generic(remote_ids, context,
                                          wizard.title,
                                          wizard.section_id.code, 
                                          wizard.categ_id and wizard.categ_id.name or '',
                                          wizard.user_id.login,
                                          extra_vals)
            return {}            
    
    _columns = {
        'sync':fields.boolean('Sync', required=False,
                              help='Create a case remotely'),               
    }

    _defaults = {
        'sync': lambda *a: False,
        
    }

WizardCreateCaseComerdist()
