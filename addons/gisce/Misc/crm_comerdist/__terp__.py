# -*- coding: utf-8 -*-
{
    "name": "CRM Comerdist",
    "description": """
Create crm case with sync
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "CRM",
    "depends":[
        "crm_generic",
        "giscedata_comerdist"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/wizard_create_case_view.xml"
    ],
    "active": False,
    "installable": True
}
