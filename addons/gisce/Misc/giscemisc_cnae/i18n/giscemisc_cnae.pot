# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* giscemisc_cnae
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.14\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2018-06-22 08:24\n"
"PO-Revision-Date: 2018-06-22 08:24\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: giscemisc_cnae
#: field:giscemisc.cnae,descripcio:0
msgid "Descripció"
msgstr ""

#. module: giscemisc_cnae
#: constraint:ir.ui.view:0
msgid "Invalid XML for View Architecture!"
msgstr ""

#. module: giscemisc_cnae
#: model:ir.actions.act_window,name:giscemisc_cnae.action_giscemisc_cnae_form1
msgid "giscemisc.cnae.form1"
msgstr ""

#. module: giscemisc_cnae
#: constraint:ir.model:0
msgid "The Object name must start with x_ and not contain any special character !"
msgstr ""

#. module: giscemisc_cnae
#: field:giscemisc.cnae,intgr:0
msgid "Intgr"
msgstr ""

#. module: giscemisc_cnae
#: model:ir.module.module,shortdesc:giscemisc_cnae.module_meta_information
msgid "Modul CNAE base"
msgstr ""

#. module: giscemisc_cnae
#: view:giscemisc.cnae:0
msgid "Codi CNAE"
msgstr ""

#. module: giscemisc_cnae
#: field:giscemisc.cnae,name:0
msgid "Codi"
msgstr ""

#. module: giscemisc_cnae
#: model:ir.model,name:giscemisc_cnae.model_giscemisc_cnae
msgid "giscemisc.cnae"
msgstr ""

#. module: giscemisc_cnae
#: constraint:ir.actions.act_window:0
msgid "Invalid model name in the action definition."
msgstr ""

#. module: giscemisc_cnae
#: model:ir.module.module,description:giscemisc_cnae.module_meta_information
msgid "Taula amb tots els CNAEs"
msgstr ""

#. module: giscemisc_cnae
#: field:giscemisc.cnae,active:0
msgid "Actiu"
msgstr ""

#. module: giscemisc_cnae
#: view:giscemisc.cnae:0
#: model:ir.ui.menu,name:giscemisc_cnae.menu_giscemisc_cnae
msgid "Codis CNAE"
msgstr ""

