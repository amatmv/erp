# -*- coding: utf-8 -*-
"""Mòdul per gestionar les activitats (CNAEs)."""
from osv import fields, osv


class GiscemiscCNAE(osv.osv):
    """CNAE."""
    _name = 'giscemisc.cnae'

    def name_get(self, cursor, uid, ids, context=None):
        res = []
        for cnae in self.read(cursor, uid, ids, ['name', 'descripcio']):
            res.append((cnae['id'], '{name} - {descripcio}'.format(**cnae)))
        return res

    def name_search(self, cursor, uid, name='', args=None, operator='ilike',
                    context=None, limit=80):
        ids = self.search(cursor, uid, [
            '|',
            ('name', operator, name),
            ('descripcio', operator, name)
        ])
        return self.name_get(cursor, uid, ids, context)

    _columns = {
        'name': fields.char('Codi', size=10, select=True),
        'intgr': fields.char('Intgr', size=10),
        'descripcio': fields.text('Descripció'),
        'active': fields.boolean('Actiu')
    }

    _order = 'name'

    _defaults = {
        'active': lambda *a: 1
    }

GiscemiscCNAE()
