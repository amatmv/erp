# -*- coding: utf-8 -*-
{
    "name": "Modul CNAE base",
    "description": """Taula amb tots els CNAEs""",
    "version": "0-dev",
    "author": "Misc",
    "category": "Partner",
    "depends":[
        "base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscemisc_cnae_view.xml",
        "giscemisc_cnae_data.xml",
        "ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
