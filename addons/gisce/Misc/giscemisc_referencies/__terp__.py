# -*- coding: utf-8 -*-
{
    "name": "Referències",
    "description": """Gestió de les referències""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "CRM",
    "depends":[
        "crm"
    ],
    "init_xml":[
        "giscemisc_referencia_sequence.xml"
    ],
    "demo_xml": [],
    "update_xml":[
        "security/ir.model.access.csv",
        "security/giscemisc_referencies_security.xml",
        "giscemisc_referencia_view.xml",
        "giscemisc_referencia_workflow.xml"
    ],
    "active": False,
    "installable": True
}
