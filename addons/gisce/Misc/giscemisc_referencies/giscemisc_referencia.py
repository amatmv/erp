# -*- coding: utf-8 -*-
from osv import fields,osv

class crm_case(osv.osv):
    _name = "crm.case"
    _inherit = "crm.case"

    def get_users_that_can_write(self, cr, uid, case, vals, context=None):
        """
        Determina quins usuaris poden fer write al cas. Té el comportament per
        defecte i afegeix un nou possible usuari modificador. Aquest possible
        usuari només podria escriure si pertany al grup de GISCEMISC Referencies
        / Manager.
        """
        res = super(crm_case, self).get_users_that_can_write(
            cr, uid, case, vals, context=context
        )
        # This is the only way I found to allow the users with the referencies
        # permissions to change the state when the validar button is pressed.
        # The ideal way should have been declare this function at the referencia
        # model.
        irmd_o = self.pool.get('ir.model.data')
        group_o = self.pool.get('res.groups')
        referencies_group_id = irmd_o.get_object_reference(
            cr, uid, 'giscemisc_referencies',
            'group_giscemisc_referencies_u'
        )[1]
        referencies_managers = group_o.read(
            cr, uid, referencies_group_id, ['users'], context=context
        )['users']
        if uid in referencies_managers:
            res.append(uid)
        return res

    _columns = {
      'state': fields.char('State', size=16, readonly=True),
      'name': fields.char('Description', size=128, required=True),
    }

crm_case()

class giscemisc_referencia(osv.osv):
    _name = "giscemisc.referencia"
    _inherits = {'crm.case': 'case_id'}


    _columns = {
      'referencia': fields.char('Referència', size=30, readonly=True),
                  'case_id': fields.many2one('crm.case', 'Cas', required=True),
    }

    def case_open(self, cr, uid, ids, *args):
        res = True
        for ref in self.browse(cr, uid, ids):
            if not ref.referencia:
                nref = self.pool.get('ir.sequence').get(cr, uid, 'giscemisc.referencia')
                res = self.write(cr, uid, ref.id, {'referencia': nref})
            res = res and ref.case_id.case_open(args)
        return res

    def case_cancel(self, cr, uid, ids, *args):
        res = True
        for ref in self.browse(cr, uid, ids):
            res = res and ref.case_id.case_cancel(*args)
        return res

    def case_pending(self, cr, uid, ids, *args):
        res = True
        for ref in self.browse(cr, uid, ids):
            res = res and ref.case_id.case_pending(*args)
        return res

    def case_reset(self, cr, uid, ids, *args):
        res = True
        for ref in self.browse(cr, uid, ids):
            res = res and ref.case_id.case_reset(*args)
        return res

    def case_close(self, cr, uid, ids, *args):
        res = True
        for ref in self.browse(cr, uid, ids):
            res = res and ref.case_id.case_close(*args)
        return res

    def case_log(self, cr, uid, ids,context=None, email=False, *args):
        res = True
        for ref in self.browse(cr, uid, ids):
            res = res and ref.case_id.case_log(context, email, *args)
        return res

    def add_reply(self, cr, uid, ids, context=None):
        res = True
        for ref in self.browse(cr, uid, ids):
            res = res and ref.case_id.add_reply(context)
        return res

    def case_log(self, cr, uid, ids,context=None, email=False, *args):
        res = True
        for ref in self.browse(cr, uid, ids):
            res = res and ref.case_id.case_log(context, email, *args)
        return res

    def case_log_reply(self, cr, uid, ids, context=None, email=False, *args):
        res = True
        for ref in self.browse(cr, uid, ids):
            res = res and ref.case_id.case_log_reply(context, email, *args)
        return res

    def emails_get(self, cr, uid, id, context=None):
        for ref in self.browse(cr, uid, id):
            return ref.case_id.emails_get(context)

    def remind_partner(self, cr, uid, ids, context=None, attach=False):
        for ref in self.browse(cr, uid, ids):
            return ref.case_id.remind_partner(context, attach)

    def remind_user(self, cr, uid, ids, context=None, attach=False, destination=True):
        for ref in self.browse(cr, uid, ids):
            subject = 'Referència [%s]' % (ref.referencia)
            return ref.case_id.remind_user(context, attach, destination, subject)

    def email_send(self, cr, uid, ids, emails, body, context=None):
        for ref in self.browse(cr, uid, ids):
            return ref.case_id.email_send(emails, body, context)

    def onchange_partner_address_id(self, cr, uid, ids, part):
        if not part:
            return {'value':{}}
        data = {}
        data['email_from'] = self.pool.get('res.partner.address').browse(cr, uid, part).email
        return {'value':data}


giscemisc_referencia()
