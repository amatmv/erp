# -*- coding: iso-8859-1 -*-
from osv import osv, fields

class product_peusa_product(osv.osv):
  _name = 'product.peusa.product'
  
  _columns = {
    'name': fields.char('Producte', size=64, select=True, required=True),
    'ref_proveidor': fields.char('Referència proveïdor', size=64, select=True, required=True),
    'preu': fields.float('Preu', required=True),
    'partner_id': fields.many2one('res.partner', 'Proveïdor', required=True),
  }

product_peusa_product()
