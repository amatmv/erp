# -*- coding: iso-8859-1 -*-
import wizard
import pooler
import base64
import csv
from osv import osv, fields


_load_csv_form = """<?xml version="1.0"?>
<form string="Relacionar Referències" >
  <field name="partner" />
  <field name="csv" colspan="4"/>
</form>"""

_load_csv_fields = {
  'csv': {'string': 'Fitxer CSV', 'type': 'binary'},
  'partner': {'string': 'Proveïdor', 'type': 'many2one', 'relation': 'res.partner', 'required': True},
}

_main_form ="""<?xml version="1.0"?>
<form string="Relacionar Referències" >
  <group colspan="4" string="Proveïdor">
    <field name="partner" /><newline />
    <field name="product_peusa" width="400" domaing="[('partner_id', '=', partner)]" on_change="product_change(product_peusa)"/>
    <!--
    <group colspan="2">
      <button string="" icon="gtk-go-back" />
      <button string="" name="next" type="object" icon="gtk-go-forward" />
    </group>
    -->
    <newline />
    <field name="ref_proveidor" />
    <field name="preu" />
  </group>
  <group string="Local" colspan="4">
    <field name="product_local" />
  </group>
  <group colspan="4">
    <label string=" " colspan="4" />
    <label string=" " colspan="3" />
    <button name="relacionar" type="object" string="Relacionar" icon="gtk-save"/>
  </group>
</form>"""

_main_fields = {
  'partner': {'string': 'Proveïdor', 'type': 'many2one', 'relation': 'res.partner', 'required': True},
  'product_peusa': {'string': 'Producte', 'type': 'many2one', 'relation': 'product.peusa.product'},
  'ref_proveidor': {'string': 'Referencia', 'type': 'char', 'size': 64, 'readonly': True},
  'preu': {'string': 'Preu', 'type': 'float', 'readonly': True},
  'product_local': {'string': 'Producte', 'type': 'many2one', 'relation': 'product.product'},
}

def _init(self, cr, uid, data, context={}):
  
  return {}

def _load_models(self, cr, uid, data, context={}):
  if data['form']['csv']:
    # Reiniciem els valors de les taules temporals
    cr.execute("DELETE FROM product_peusa_product WHERE partner_id = %s", (int(data['form']['partner']), ))
    cr.commit()
    # Escriure el CSV al servidor per despres llegir-lo
    _CSV_FILE_PATH = '/tmp/referencies_proveidor.csv'
    f = open(_CSV_FILE_PATH, 'w')
    f.write(base64.b64decode(data['form']['csv']))
    f.close()
    # Llegim el CSV
    reader = csv.reader(open(_CSV_FILE_PATH, "rUb"))
    iterator = 0
    # Tractem el CSV per importar-lo als models
    dades = False
    for row in reader:
      if len(row) > 6 and row[2] and row[4] and row[6]:
        # Si es pot passar be a float son dades bones
        try:
          vals = {}
          vals['name'] = row[4]
          vals['ref_proveidor'] = row[2]
          vals['preu'] = float(row[6])
          vals['partner_id'] = data['form']['partner']
          if pooler.get_pool(cr.dbname).get('product.peusa.product').create(cr, uid, vals):
            print 'Producte %s-%s importat.' % (vals['ref_proveidor'], vals['name'])
        except:
          pass
  return {}


def _load_first(self, cr, uid, data, context={}):
  cr.execute("SELECT min(id) FROM product_peusa_product where partner_id = %s", (int(data['form']['partner']), ))
  id = cr.fetchone()
  if id:
    cr.execute("SELECT id,ref_proveidor,preu FROM product_peusa_product where id = %s", (int(id[0]), ))
    product = cr.dictfetchone()
    return {'product_peusa': product['id'], 'preu': product['preu'], 'ref_proveidor': product['ref_proveidor']}
  else:
    return {}

class wizard_product_peusa_referencies_proveidors(osv.osv):
  _name = 'wizard.product.peusa.referencies.proveidors'
  _auto = False
  _vals = {}
  _partner = None
  
  def create(self, cr, uid, vals, context={}):
    if vals.has_key('partner'):
      self._partner = vals['partner']
    self._vals.update(vals)
    n_vals = {}
    supplier_obj = pooler.get_pool(cr.dbname).get('product.supplierinfo')
    product_peusa_obj = pooler.get_pool(cr.dbname).get('product.peusa.product')
    product_local_obj = pooler.get_pool(cr.dbname).get('product.product')
    ppeusa = product_peusa_obj.read(cr, uid, self._vals['product_peusa'])
    plocal = product_local_obj.browse(cr, uid, self._vals['product_local'])
    n_vals['product_id'] = plocal.product_tmpl_id.id
    n_vals['product_code'] = ppeusa['ref_proveidor']
    n_vals['product_name'] = ppeusa['name']
    n_vals['name'] = ppeusa['partner_id'][0]
    print 'Self: %s' % self._vals

    si_ids = supplier_obj.search(cr, uid, [('product_id', '=', n_vals['product_id']),\
      ('name', '=', self._partner)])
    print si_ids
    if si_ids and len(si_ids):
      supplier_obj.write(cr, uid, si_ids[0], n_vals, context)
    else:
      supplier_obj.create(cr, uid, n_vals, context)
    return 1
  
  def write(self, cr, uid, ids, vals, context={}):
    self._vals.update(vals)
    return self.create(cr, uid, vals, context)

  def relacionar(self, cr, uid, ids, args):
    return True

  def product_change(self, cr, uid, ids, product, context={}):
    values = {}
    if product:
      product_obj = pooler.get_pool(cr.dbname).get('product.peusa.product').browse(cr, uid, product)
      values['preu'] = product_obj.preu
      values['ref_proveidor'] = product_obj.ref_proveidor
    else:
      values['preu'] = False
      values['ref_proveidor'] = False
      values['product_local'] = False
    self._vals = values
    return {'value': values}
  

wizard_product_peusa_referencies_proveidors()

class wizard_relacionar_referencies(wizard.interface):

  states = {
    'init': {
    	'actions': [_init],
      'result': {'type': 'state', 'state': 'load_csv'}
    },
    'load_csv': {
    	'actions': [],
    	'result': {'type': 'form', 'arch': _load_csv_form,'fields': _load_csv_fields, 'state':[('main', 'Continuar', 'gtk-go-forward')]}
    },
    'main': {
    	'actions': [_load_models, _load_first],
    	'result': {'type': 'form', 'arch': _main_form,'fields': _main_fields, 'state':[('end', 'Finalitzar', 'gtk-cancel')]}
    },
    'guardar': {
    	'actions': [],
    	'result': { 'type' : 'state', 'state' : 'end' },
    },
  }

wizard_relacionar_referencies('product.peusa.referencies.proveidors')


