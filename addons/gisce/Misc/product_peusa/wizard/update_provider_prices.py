# -*- coding: iso-8859-1 -*-
import wizard
import pooler
import base64
import csv
from osv import osv, fields


_load_csv_form = """<?xml version="1.0"?>
<form string="Actualitzar preus del proveïdor" >
  <group>
    <image name="gtk-refresh" />
  </group>
  <group>
    <field name="partner" />
    <field name="csv" colspan="4"/>
  </group>
</form>"""

_load_csv_fields = {
  'csv': {'string': 'Fitxer CSV', 'type': 'binary'},
  'partner': {'string': 'Proveïdor', 'type': 'many2one', 'relation': 'res.partner', 'required': True},
}


def _init(self, cr, uid, data, context={}):
  
  return {}

def _load_models(self, cr, uid, data, context={}):
  # Escriure el CSV al servidor per despres llegir-lo
  _CSV_FILE_PATH = '/tmp/provider_codes.csv'
  f = open(_CSV_FILE_PATH, 'w')
  f.write(base64.b64decode(data['form']['csv']))
  f.close()
  # Llegim el CSV
  reader = csv.reader(open(_CSV_FILE_PATH, "rb"))
  for row in reader:
    if len(row) == 2:
      # Si es pot passar be a float son dades bones
      try:
        form = data['form']
        vals = {}
        vals['ref_proveidor'] = row[0]
        vals['preu'] = float(row[1])
        suplier_obj = pooler.get_pool(cr.dbname).get('product.supplierinfo')
        s_ids = suplier_obj.search(cr, uid, [
          ('name', '=', form['partner']),\
          ('product_code', '=', vals['ref_proveidor'])])
        if len(s_ids):
          provider = suplier_obj.browse(cr, uid, s_ids[0])
          print 'Producte: %s' % (provider.product_id.name)
        else:
          print 'No s\'ha trobat cap proveïdor amb la ref de producte %s' % (vals['ref_proveidor'])
      except:
        pass
  return {}



class wizard_product_peusa_update_provider_price(wizard.interface):

  states = {
    'init': {
    	'actions': [_init],
      'result': {'type': 'state', 'state': 'load_csv'}
    },
    'load_csv': {
    	'actions': [],
    	'result': {'type': 'form', 'arch': _load_csv_form,'fields': _load_csv_fields, 'state':[('guardar', 'Continuar', 'gtk-go-forward')]}
    },
    'guardar': {
    	'actions': [_load_models],
    	'result': { 'type' : 'state', 'state' : 'end' },
    },
  }

wizard_product_peusa_update_provider_price('product.peusa.update.provider.price')


