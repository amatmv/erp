# -*- coding: utf-8 -*-
{
    "name": "Adaptacio per a PEUSA del modul product",
    "description": """Adaptacions PEUSA""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Product",
    "depends":[
        "product"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "product_peusa_wizard.xml",
        "product_peusa_view.xml"
    ],
    "active": False,
    "installable": True
}
