# -*- coding: utf-8 -*-
{
    "name": "Attachments tab",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Auto attachments
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "attachments_tab_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
