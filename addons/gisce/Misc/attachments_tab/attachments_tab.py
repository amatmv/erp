# -*- coding: utf-8 -*-
import types
import logging

import report
import pooler
from osv import osv, fields
from tools.translate import _
from lxml import etree


class AttachmentsTab(osv.osv):
    _name = 'attachments.tab'

    def create(self, cr, user, vals, context=None):
        res = super(AttachmentsTab, self).create(cr, user, vals, context)
        att = self.browse(cr, user, res)
        obj = self.pool.get(att.model.model)
        patch_object(obj)
        return res

    def write(self, cr, user, ids, vals, context=None):
        for att in self.browse(cr, user, ids):
            obj = self.pool.get(att.model.model)
            unpatch_object(obj)
        res = super(AttachmentsTab, self).write(cr, user, ids, vals, context)
        for att in self.browse(cr, user, ids):
            obj = self.pool.get(att.model.model)
            patch_object(obj)
        return res

    def unlink(self, cr, uid, ids, context=None):
        for att in self.browse(cr, uid, ids):
            obj = self.pool.get(att.model.model)
            unpatch_object(obj)
        res = super(AttachmentsTab, self).unlink(cr, uid, ids, context)
        return res

    _columns = {
        'model': fields.many2one('ir.model', 'Model', required=True)
    }

AttachmentsTab()


def _ff_attachment_field(self, cursor, uid, ids, field_name, arg, context=None):
    if not context:
        context = {}
    res = dict.fromkeys(ids, False)
    att_obj = self.pool.get('ir.attachment')
    for res_id in ids:
        res[res_id] = att_obj.search(cursor, uid, [
            ('res_model', '=', self._name),
            ('res_id', '=', res_id)
        ])
    return res


attachments_field = fields.function(
    _ff_attachment_field, method=True, string='Attachments',
    type='one2many', relation='ir.attachment'
)


def fields_view_get(self, cursor, user, view_id=None, view_type='form',
                            context=None, toolbar=False):
    res = self.fields_view_get_unpatched(cursor, user, view_id, view_type,
                                         context, toolbar)
    res['fields'].update(self.fields_get(cursor, user, ['_attachments_field']))
    xml = etree.fromstring(res['arch'])
    notebook = xml.xpath('//notebook')
    if notebook:
        string = _('Adjunts')
        notebook[0].append(etree.fromstring("""
            <page string="{0}">
                <field name="_attachments_field" colspan="4" nolabel="1"/>
            </page>
        """.format(string)))
        res['arch'] = etree.tostring(xml)
    return res


old_register_all = report.interface.register_all


def patch_object(obj):
    # Only patch once
    if '_attachments_field' not in obj._columns:
        # Add the column
        obj._columns['_attachments_field'] = attachments_field
        # Monkeypatch fields_view_get
        method_obj = getattr(obj, 'fields_view_get_unpatched', None)
        if not callable(method_obj):
            obj.fields_view_get_unpatched = obj.fields_view_get
        obj.fields_view_get = types.MethodType(
            fields_view_get, obj, osv.osv
        )
    return True


def unpatch_object(obj):
    if '_attachments_field' in obj._columns:
        del obj._columns['_attachments_field']
    if hasattr(obj, 'fields_view_get_unpatched'):
        obj.fields_view_get = obj.fields_view_get_unpatched
    return True


def attachments_tab_register(db):
    uid = 1
    value = old_register_all(db)

    cr = db.cursor()
    pool = pooler.get_pool(cr.dbname)
    model_obj = pool.get('ir.model')
    sql_model = ''' SELECT model from attachments_tab;'''
    try:
        cr.execute(sql_model)
        models = cr.fetchall()
        for model in model_obj.read(cr, uid, models, ['model']):
            obj = pool.get(model['model'])
            if obj:
                patch_object(obj)
    except:
        logger = logging.getLogger('openerp')
        logger.error('Error getting attachments_tab models')
    finally:
        cr.close()
    return value

report.interface.register_all = attachments_tab_register
