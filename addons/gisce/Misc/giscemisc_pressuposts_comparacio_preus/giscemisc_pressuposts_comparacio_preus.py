# *-* coding
from osv import osv, fields

class sale_order(osv.osv):

    _name = 'sale.order'
    _inherit = 'sale.order'

    def _total_preu_cost_st(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for sale in self.browse(cr, uid, ids):
            preu = 0.0
            for capitol in sale.capitols:
                preu += capitol.preu_cost
            res[sale.id] = preu
        return res

    _columns = {
      'total_preu_cost_st': fields.function(_total_preu_cost_st, type='float', string='Preu Cost', method=True),
    }

sale_order()

class giscemisc_pressuposts_capitol(osv.osv):

    _name = 'giscemisc.pressuposts.capitol'
    _inherit = 'giscemisc.pressuposts.capitol'

    def _preu_cost(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for capitol in self.browse(cr, uid, ids):
            preu = 0.0
            for o in capitol.order_lines:
                if o.product_id:
                    preu += o.product_id.standard_price * o.product_uom_qty
                else:
                    preu += o.price_subtotal
            res[capitol.id] = preu
        return res

    _columns = {
      'preu_cost': fields.function(_preu_cost, type='float', string='Preu Cost', method=True),
    }

giscemisc_pressuposts_capitol()
