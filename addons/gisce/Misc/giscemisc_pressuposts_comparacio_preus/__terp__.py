# -*- coding: utf-8 -*-
{
    "name": "Comparació preus de cost i preus pressupost",
    "description": """Report on es mostra la taula final amb tots els materials diferenciant el preu de cost i el preu del pressupost""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Misc",
    "depends":[
        "base",
        "giscemisc_pressuposts_report_peusa"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscemisc_pressuposts_comparacio_preus_report.xml"
    ],
    "active": False,
    "installable": True
}
