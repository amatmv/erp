<?xml version="1.0" encoding="iso-8859-15"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" version="1.0">
    <xsl:decimal-format name="ca_ES" decimal-separator="," grouping-separator="."/>
    <xsl:template match="/">
        <xsl:apply-templates select="pressuposts"/>
    </xsl:template>
    <xsl:template match="pressuposts">
        <xsl:apply-templates select="pressupost"/>
    </xsl:template>
    <xsl:template match="pressupost">
        <document debug="1" compression="1">
            <template pageSize="(21cm, 29.7cm)" showBoundary="0">
                <pageTemplate id="main">
                    <pageGraphics>
                        <image file="addons/custom/logo.png" width="20mm" height="20mm" x="1cm" y="26.6cm"/>
                        <setFont name="Helvetica-Bold" size="14"/>
                        <drawString x="3.2cm" y="26.6cm">productora el�ctrica urgelense s.a.</drawString>
                        <setFont name="Helvetica" size="8"/>
                        <drawRightString x="20cm" y="26.2cm"><xsl:value-of select="//corporate-header/corporation/address[type='default']/street"/> - <xsl:value-of select="concat(//corporate-header/corporation/address[type='default']/zip, ' ', //corporate-header/corporation/address[type='default']/city)"/> (<xsl:value-of select="//corporate-header/corporation/address[type='default']/state"/>) - tel. <xsl:value-of select="//corporate-header/corporation/address[type='default']/phone"/> - fax. <xsl:value-of select="//corporate-header/corporation/address[type='default']/fax"/> - e-mail: <xsl:value-of select="//corporate-header/corporation/address[type='default']/email"/></drawRightString>
                        <!-- Repetici� pel llistat -->
                        <setFont name="Helvetica" size="10"/>
                        <drawString x="13.2cm" y="23.1cm">
                            <xsl:value-of select="partner/name"/>
                        </drawString>
                        <drawString x="13.2cm" y="22.7cm">
                            <xsl:value-of select="sollicitud/partner_address/adreca"/>
                        </drawString>
                        <drawString x="13.2cm" y="22.3cm">
                            <xsl:value-of select="concat(sollicitud/partner_address/municipi, '  ')"/>
                            <xsl:value-of select="sollicitud/partner_address/cp"/>
                        </drawString>
                        <place x="1.2cm" y="17.4cm" width="19cm" height="5cm">
                            <blockTable style="taula" colWidths="5cm,13cm">
                                <tr>
                                    <td>N�m. estudi t�cnic econ�mic:</td>
                                    <td>
                                        <xsl:value-of select="name"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Data:</td>
                                    <td>
                                        <xsl:value-of select="concat(substring(data, 9, 2),'/',substring(data, 6, 2),'/',substring(data, 1, 4))"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Obra:</td>
                                    <td>
                                        <para style="text">
                                            <b>
                                                <xsl:value-of select="assumpte"/>
                                            </b>
                                        </para>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Descripci�:</td>
                                    <td>
                                        <para style="text">
                                            <xsl:value-of select="descripcio"/>
                                        </para>
                                    </td>
                                </tr>
                                <tr>
                                    <td>N. Obra:</td>
                                    <td>
                                        <para style="text">
                                            <xsl:value-of select="obra"/>
                                        </para>
                                    </td>
                                </tr>
                                <xsl:if test="sollicitud/name != ''">
                                    <tr>
                                        <td>
                                            <para style="text">Pot�ncia Sol.:</para>
                                        </td>
                                        <td>
                                            <para style="text"><xsl:value-of select="sollicitud/potencia"/> kW.</para>
                                        </td>
                                    </tr>
                                </xsl:if>
                            </blockTable>
                        </place>
                        <!-- Final dades llistat -->
                        <rotate degrees="90"/>
                        <setFont name="Helvetica" size="7"/>
                        <drawString x="60mm" y="-4mm">
                            <xsl:value-of select="//corporate-header/corporation/rml_footer2"/>
                        </drawString>
                    </pageGraphics>
                    <frame id="llista" x1="1cm" y1="1cm" width="19cm" height="16.5cm"/>
                </pageTemplate>
            </template>
            <stylesheet>
                <paraStyle name="text" fontName="Helvetica" fontSize="10"/>
                <paraStyle name="vermell" textColor="darkred" fontName="Helvetica-Bold" fontSize="10"/>
                <paraStyle name="blau" textColor="darkblue" fontName="Helvetica-Bold" fontSize="10" leftIndent="0.5cm"/>
                <blockTableStyle id="taula">
                    <blockFont name="Helvetica" size="10"/>
                </blockTableStyle>
                <blockTableStyle id="taula_preus">
                    <blockFont name="Helvetica" size="10"/>
                    <lineStyle kind="GRID" start="0,0" stop="-1,0" colorName="black"/>
                    <blockBackground colorName="silver" start="0,0" stop="-1,0"/>
                    <lineStyle kind="BOX" start="0,1" stop="-1,-1" colorName="black"/>
                    <lineStyle kind="LINEAFTER" start="0,1" stop="-1,-1" colorName="black"/>
                </blockTableStyle>
            </stylesheet>
            <story>
                <blockTable style="taula_preus" colWidths="10cm,2cm,1.5cm,2cm,1.5cm,2cm" repeatRows="1">
                    <tr>
                        <td>
                            <para style="text">
                                <b>Concepte</b>
                            </para>
                        </td>
                        <td>
                            <para style="text" alignment="right">
                                <b>Quantitat</b>
                            </para>
                        </td>
                        <td>
                            <para style="text" alignment="right">
                                <b>Preu</b>
                            </para>
                        </td>
                        <td>
                            <para style="text" alignment="right">
                                <b>Import</b>
                            </para>
                        </td>
                        <td>
                            <para style="text" alignment="right">
                                <b>Preu Cost</b>
                            </para>
                        </td>
                        <td>
                            <para style="text" alignment="right">
                                <b>Import Cost</b>
                            </para>
                        </td>
                    </tr>
                    <xsl:apply-templates match="capitols" mode="story"/>
                    <tr>
                        <td>
                            <para style="vermell" t="1">TOTAL</para>
                        </td>
                        <td/>
                        <td/>
                        <td>
                            <para style="vermell" alignment="right">
                                <xsl:value-of select="format-number(preu_sense_taxes, '###.##0,00', 'ca_ES')"/>
                            </para>
                        </td>
                        <td/>
                        <td>
                            <para style="vermell" alignment="right">
                                <xsl:value-of select="format-number(preu_cost_sense_taxes, '###.##0,00', 'ca_ES')"/>
                            </para>
                        </td>
                    </tr>
                </blockTable>
                <spacer length="20"/>
                <xsl:if test="obs_client!=''">
                    <para style="text" fontName="Helvetica-Bold">OBSERVACIONS:</para>
                    <xpre style="text">
                        <xsl:value-of select="obs_client"/>
                    </xpre>
                </xsl:if>
            </story>
        </document>
    </xsl:template>
    <xsl:template match="capitols" mode="story">
        <xsl:apply-templates match="capitol" mode="story"/>
    </xsl:template>
    <xsl:template match="capitol" mode="story">
        <xsl:if test="preu&gt;0">
            <tr>
                <td>
                    <para style="blau">
                        <xsl:value-of select="nom"/>
                    </para>
                </td>
                <td/>
                <td/>
                <td/>
                <td/>
                <td/>
            </tr>
            <xsl:apply-templates match="olines" mode="story"/>
            <tr>
                <td/>
                <td/>
                <td/>
                <td>
                    <para style="vermell" alignment="right">
                        <xsl:value-of select="format-number(preu, '###.##0,00', 'ca_ES')"/>
                    </para>
                </td>
                <td/>
                <td>
                    <para style="vermell" alignment="right">
                        <xsl:value-of select="format-number(preu_cost, '###.##0,00', 'ca_ES')"/>
                    </para>
                </td>
            </tr>
        </xsl:if>
    </xsl:template>
    <xsl:template match="olines" mode="story">
        <xsl:if test="oline/preu&gt;0">
            <xsl:apply-templates match="oline" mode="story"/>
        </xsl:if>
    </xsl:template>
    <xsl:template match="oline" mode="story">
        <tr>
            <td>
                <para style="text" leftIndent="1cm">
                    <xsl:value-of select="producte"/>
                </para>
            </td>
            <td>
                <para style="text" alignment="right">
                    <xsl:value-of select="format-number(quantitat, '###.##0,00', 'ca_ES')"/>
                </para>
            </td>
            <td>
                <para style="text" alignment="right">
                    <xsl:value-of select="format-number(preu, '###.##0,00', 'ca_ES')"/>
                </para>
            </td>
            <td>
                <para style="text" alignment="right">
                    <xsl:value-of select="format-number(preu * quantitat,  '###.##0,00', 'ca_ES')"/>
                </para>
            </td>
            <td>
                <para style="text" alignment="right">
                    <xsl:value-of select="format-number(preu_cost, '###.##0,00', 'ca_ES')"/>
                </para>
            </td>
            <td>
                <para style="text" alignment="right">
                    <xsl:value-of select="format-number(preu_cost * quantitat, '###.##0,00', 'ca_ES')"/>
                </para>
            </td>
        </tr>
    </xsl:template>
</xsl:stylesheet>
