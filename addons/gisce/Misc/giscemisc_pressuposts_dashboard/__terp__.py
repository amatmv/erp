# -*- coding: utf-8 -*-
{
    "name": "Dashboard pels pressuposts",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Board/Sales & Purchase",
    "depends":[
        "board",
        "giscemisc_pressuposts"
    ],
    "init_xml": [],
    "demo_xml":[
        "board_sale_demo.xml"
    ],
    "update_xml":[
        "giscemisc_pressupost_dashboard_view.xml"
    ],
    "active": False,
    "installable": True
}
