# -*- coding: utf-8 -*-
from osv import fields,osv

class report_giscemisc_pressuposts_creacio_per_mes(osv.osv):
    _name = "report.giscemisc.pressuposts.creacio.per.mes"
    _description = "Creació de pressuposts per mes"
    _auto = False
    _columns = {
      'id': fields.integer('Id'),
      'qty': fields.integer('Quantitat'),
      'name': fields.char('Mes',size=10),
    }

    def init(self, cr):
        cr.execute("""
          create or replace view report_giscemisc_pressuposts_creacio_per_mes as (
                              select id,1 as qty,extract(month from date_order) as name from sale_order where extract(year from date_order) = extract(year from NOW()) order by name asc
          )""")

report_giscemisc_pressuposts_creacio_per_mes()
