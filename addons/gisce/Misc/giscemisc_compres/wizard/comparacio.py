import wizard

def _print(self, cr, uid, data, context={}):
    print '* PRINT'
    cr.execute("select id from purchase_order_line where order_id in (%s)" % (','.join(map(str, map(int, data['ids'])))))
    lines = cr.fetchall()
    return {'ids': [a[0] for a in lines]}

class wizard_comparacio_ofertes(wizard.interface):

    states = {
      'init': {
          'actions': [],
        'result': {'type': 'state', 'state': 'print'}
      },
      'print': {
        'actions': [_print],
        'result': {'type': 'print', 'report':\
        'giscemisc.compres.comparacio', 'get_id_from_action':True, \
        'state':'end'}
      },
    }

wizard_comparacio_ofertes('giscemisc.compres.comparacio.ofertes')
