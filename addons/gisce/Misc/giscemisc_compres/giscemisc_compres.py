# -*- coding: iso-8859-1 -*-
from osv import osv,fields

class purchase_order_line(osv.osv):
    _name = 'purchase.order.line'
    _inherit = 'purchase.order.line'

    def _supplier_code(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for ol in self.browse(cr, uid, ids):
            cr.execute("select s.product_code from product_supplierinfo s, purchase_order_line ol, purchase_order o, product_product p, product_template pt where s.product_id = pt.id and p.product_tmpl_id = pt.id and p.id = ol.product_id and ol.order_id = o.id and o.partner_id = s.name and o.id = %i and p.id = %i", (ol.order_id.id, ol.product_id.id))
            supplier = cr.dictfetchone()
            print supplier
            if supplier:
                res[ol.id] = supplier['product_code']
            else:
                res[ol.id] = ''
        return res

    _columns = {
      'supplier_code': fields.function(_supplier_code, type='char', method=True, string='Supplier code'),
    }

purchase_order_line()
