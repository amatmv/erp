<?xml version="1.0" encoding="iso-8859-15"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" version="1.0">
    <xsl:key name="proveidors-per-producte" match="producte" use="nom"/>
    <xsl:key name="productes-per-proveidor" match="producte" use="proveidor"/>
    <xsl:key name="producte-per-id" match="producte" use="id"/>
    <xsl:template match="/">
        <xsl:apply-templates select="linies"/>
    </xsl:template>
    <xsl:template match="linies">
        <document compression="1">
            <template pageSize="(21cm, 29.7cm)">
                <pageTemplate id="main">
                    <pageGraphics>
                        <image file="addons/custom/logo.png" width="20mm" height="20mm" x="1cm" y="26.6cm"/>
                        <setFont name="Helvetica-Bold" size="30"/>
                        <drawString x="3.2cm" y="27.8cm">
                            <xsl:value-of select="//corporate-header/corporation/rml_header1"/>
                        </drawString>
                        <setFont name="Helvetica" size="10"/>
                        <drawString x="3.2cm" y="27.1cm">
                            <xsl:value-of select="//corporate-header/corporation/rml_footer1"/>
                        </drawString>
                        <lineMode width="2"/>
                        <stroke color="blue"/>
                        <lines>
                3.2cm 27cm 20cm 27cm
            </lines>
                        <setFont name="Helvetica" size="8"/>
                        <drawString x="3.2cm" y="26.6cm">
                            <xsl:value-of select="//corporate-header/corporation/name"/>
                        </drawString>
                        <drawRightString x="20cm" y="26.7cm">
                            <xsl:value-of select="//corporate-header/corporation/address[type='default']/street"/>
                        </drawRightString>
                        <drawRightString x="20cm" y="26.4cm"><xsl:value-of select="concat(//corporate-header/corporation/address[type='default']/zip, ' ', //corporate-header/corporation/address[type='default']/city)"/> (<xsl:value-of select="//corporate-header/corporation/address[type='default']/state"/>)</drawRightString>
                        <drawRightString x="20cm" y="26.1cm">Tel. <xsl:value-of select="//corporate-header/corporation/address[type='default']/phone"/> - Fax <xsl:value-of select="//corporate-header/corporation/address[type='default']/fax"/></drawRightString>
                        <drawRightString x="20cm" y="25.8cm">E-mail: <xsl:value-of select="//corporate-header/corporation/address[type='default']/email"/></drawRightString>
                        <!--
            <rotate degrees="90" />
            <setFont name="Helvetica" size="7" />
            <drawString x="60mm" y="-4mm"><xsl:value-of select="//corporate-header/corporation/rml_footer2" /></drawString>
            -->
                    </pageGraphics>
                    <frame id="first" x1="1cm" y1="1cm" width="19cm" height="247mm"/>
                </pageTemplate>
            </template>
            <stylesheet>
                <paraStyle name="text" fontName="Helvetica" fontSize="10"/>
                <paraStyle name="vermell" textColor="darkred" fontName="Helvetica-Bold" fontSize="10"/>
                <paraStyle name="blau" textColor="darkblue" fontName="Helvetica-Bold" fontSize="10" leftIndent="0.5cm"/>
                <blockTableStyle id="taula">
                    <blockFont name="Helvetica" size="10"/>
                </blockTableStyle>
                <blockTableStyle id="taula_comparacio">
                    <blockFont name="Helvetica" size="10"/>
                    <lineStyle kind="GRID" colorName="black" start="1,0" stop="-1,-1"/>
                    <lineStyle kind="GRID" colorName="black" start="0,1" stop="-1,-1"/>
                    <blockBackground colorName="silver" start="1,0" stop="-1,0"/>
                    <blockBackground colorName="silver" start="0,1" stop="0,-1"/>
                    <blockAlignment value="CENTER" start="1,1" stop="1,1"/>
                </blockTableStyle>
            </stylesheet>
            <story>
                <para style="text" fontSize="20" t="1">Comparació d'Ofertes</para>
                <spacer length="20"/>
                <!-- Agrupem per proveidor -->
                <blockTable style="taula_comparacio">
                    <tr>
                        <td/>
                        <xsl:for-each select="linia/producte[count(. | key('productes-per-proveidor', proveidor)[1]) = 1]">
                            <td>
                                <para style="text" alignment="center">
                                    <xsl:value-of select="proveidor"/>
                                </para>
                            </td>
                        </xsl:for-each>
                    </tr>
                    <xsl:for-each select="linia/producte[count(. | key('proveidors-per-producte', nom)[1]) = 1]">
                        <tr>
                            <td>
                                <para style="text">
                                    <xsl:value-of select="nom"/>
                                </para>
                            </td>
                            <xsl:variable name="prod_nom" select="nom"/>
                            <xsl:for-each select="//linia/producte[count(. | key('productes-per-proveidor', proveidor)[1]) = 1]">
                                <xsl:variable name="prod_prov" select="proveidor"/>
                                <td>
                                    <xsl:if test="count(//linia/producte[proveidor=$prod_prov and nom=$prod_nom]/preu)&gt;0">
                                        <para style="text" alignment="center"><xsl:value-of select="//linia/producte[proveidor=$prod_prov and nom=$prod_nom]/preu"/> ¤</para>
                                        <para style="text" fontSize="8" alignment="center">(<xsl:value-of select="concat(//linia/producte[proveidor=$prod_prov and nom=$prod_nom]/quantitat, ' ', //linia/producte[proveidor=$prod_prov and nom=$prod_nom]/unitat)"/>)</para>
                                    </xsl:if>
                                </td>
                            </xsl:for-each>
                        </tr>
                    </xsl:for-each>
                </blockTable>
                <spacer length="20"/>
                <para style="text" alignment="right" fontSize="8">
                    <i>
                        <xsl:value-of select="//linies/data_llarga"/>
                    </i>
                </para>
            </story>
        </document>
    </xsl:template>
</xsl:stylesheet>
