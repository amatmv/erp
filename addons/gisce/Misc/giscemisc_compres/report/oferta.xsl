<?xml version="1.0" encoding="iso-8859-15"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" version="1.0">
    <xsl:template match="/">
        <xsl:apply-templates select="compres"/>
    </xsl:template>
    <xsl:template match="compres">
        <xsl:apply-templates select="compra"/>
    </xsl:template>
    <xsl:template match="compra">
        <document debug="1" compression="1">
            <template pageSize="(21cm, 29.7cm)">
                <pageTemplate id="main">
                    <pageGraphics>
                        <image file="addons/custom/logo.png" width="20mm" height="20mm" x="1cm" y="26.6cm"/>
                        <setFont name="Helvetica-Bold" size="30"/>
                        <drawString x="3.2cm" y="27.8cm">
                            <xsl:value-of select="//corporate-header/corporation/rml_header1"/>
                        </drawString>
                        <setFont name="Helvetica" size="10"/>
                        <drawString x="3.2cm" y="27.1cm">
                            <xsl:value-of select="//corporate-header/corporation/rml_footer1"/>
                        </drawString>
                        <lineMode width="2"/>
                        <stroke color="blue"/>
                        <lines>
                3.2cm 27cm 20cm 27cm
            </lines>
                        <setFont name="Helvetica" size="8"/>
                        <drawString x="3.2cm" y="26.6cm">
                            <xsl:value-of select="//corporate-header/corporation/name"/>
                        </drawString>
                        <drawRightString x="20cm" y="26.7cm">
                            <xsl:value-of select="//corporate-header/corporation/address[type='default']/street"/>
                        </drawRightString>
                        <drawRightString x="20cm" y="26.4cm"><xsl:value-of select="concat(//corporate-header/corporation/address[type='default']/zip, ' ', //corporate-header/corporation/address[type='default']/city)"/> (<xsl:value-of select="//corporate-header/corporation/address[type='default']/state"/>)</drawRightString>
                        <drawRightString x="20cm" y="26.1cm">Tel. <xsl:value-of select="//corporate-header/corporation/address[type='default']/phone"/> - Fax <xsl:value-of select="//corporate-header/corporation/address[type='default']/fax"/></drawRightString>
                        <drawRightString x="20cm" y="25.8cm">E-mail: <xsl:value-of select="//corporate-header/corporation/address[type='default']/email"/></drawRightString>
                        <rotate degrees="90"/>
                        <setFont name="Helvetica" size="7"/>
                        <drawString x="60mm" y="-4mm">
                            <xsl:value-of select="//corporate-header/corporation/rml_footer2"/>
                        </drawString>
                    </pageGraphics>
                    <frame id="first" x1="1cm" y1="1cm" width="19cm" height="247mm"/>
                </pageTemplate>
            </template>
            <stylesheet>
                <paraStyle name="text" fontName="Helvetica" fontSize="10"/>
                <paraStyle name="vermell" textColor="darkred" fontName="Helvetica-Bold" fontSize="10"/>
                <paraStyle name="blau" textColor="darkblue" fontName="Helvetica-Bold" fontSize="10" leftIndent="0.5cm"/>
                <blockTableStyle id="taula">
                    <blockFont name="Helvetica" size="10"/>
                </blockTableStyle>
                <blockTableStyle id="taula_preus">
                    <blockFont name="Helvetica" size="10"/>
                    <lineStyle kind="GRID" start="0,0" stop="-1,0" colorName="black"/>
                    <blockBackground colorName="silver" start="0,0" stop="-1,0"/>
                    <lineStyle kind="BOX" start="0,1" stop="-1,-1" colorName="black"/>
                    <lineStyle kind="LINEAFTER" start="0,1" stop="-1,-1" colorName="black"/>
                </blockTableStyle>
            </stylesheet>
            <story>
                <spacer length="2cm"/>
                <para style="text" leftIndent="12cm">
                    <xsl:value-of select="partner/nom"/>
                </para>
                <para style="text" leftIndent="12cm">
                    <xsl:value-of select="partner/adreca/nom"/>
                </para>
                <para style="text" leftIndent="12cm">
                    <xsl:value-of select="partner/adreca/carrer"/>
                </para>
                <para style="text" leftIndent="12cm"><xsl:value-of select="concat(partner/adreca/cp, ' ', partner/adreca/ciutat)"/> (<xsl:value-of select="partner/adreca/municipi"/>)</para>
                <para style="text" leftIndent="12cm">Tel. <xsl:value-of select="partner/adreca/telf"/> Fax. <xsl:value-of select="partner/adreca/fax"/></para>
                <spacer length="30"/>
                <blockTable style="taula" colWidths="3cm,16cm">
                    <tr>
                        <td>Sol�licitud d'Oferta:</td>
                        <td>
                            <xsl:value-of select="numero"/>
                        </td>
                    </tr>
                </blockTable>
                <spacer length="15"/>
                <blockTable style="taula_preus" colWidths="3.5cm,12cm,3.5cm" repeatRows="1">
                    <tr>
                        <td>
                            <para style="text">
                                <b>Codi</b>
                            </para>
                        </td>
                        <td>
                            <para style="text">
                                <b>Producte</b>
                            </para>
                        </td>
                        <td>
                            <para style="text" alignment="right">
                                <b>Quantitat</b>
                            </para>
                        </td>
                    </tr>
                    <xsl:apply-templates match="linies/producte" mode="story"/>
                </blockTable>
                <spacer length="25"/>
                <para style="text">Atentament,</para>
                <para style="text">
                    <xsl:value-of select="//corporate-header/user/name"/>
                </para>
            </story>
        </document>
    </xsl:template>
    <xsl:template match="linies/producte" mode="story">
        <tr>
            <td>
                <para style="text" leftIndent="1cm">
                    <xsl:value-of select="codi_proveidor"/>
                </para>
            </td>
            <td>
                <para style="text" leftIndent="1cm"><xsl:value-of select="codi"/> - <xsl:value-of select="nom"/></para>
            </td>
            <td>
                <para style="text" alignment="right">
                    <xsl:value-of select="format-number(quantitat, '###,###.00')"/>
                    <xsl:value-of select="unitat"/>
                </para>
            </td>
        </tr>
    </xsl:template>
</xsl:stylesheet>
