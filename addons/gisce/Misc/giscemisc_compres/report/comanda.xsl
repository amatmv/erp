<?xml version="1.0" encoding="iso-8859-15"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" version="1.0">
    <xsl:decimal-format name="ca_ES" decimal-separator="," grouping-separator="."/>
    <xsl:template match="/">
        <xsl:apply-templates select="compres"/>
    </xsl:template>
    <xsl:template match="compres">
        <xsl:apply-templates select="compra"/>
    </xsl:template>
    <xsl:template match="compra">
        <document debug="1" compression="1">
            <template pageSize="(21cm, 29.7cm)">
                <pageTemplate id="main">
                    <pageGraphics>
                        <image file="addons/custom/logo.png" width="20mm" height="20mm" x="1cm" y="26.6cm"/>
                        <setFont name="Helvetica-Bold" size="30"/>
                        <drawString x="3.2cm" y="27.8cm">
                            <xsl:value-of select="//corporate-header/corporation/rml_header1"/>
                        </drawString>
                        <setFont name="Helvetica" size="10"/>
                        <drawString x="3.2cm" y="27.1cm">
                            <xsl:value-of select="//corporate-header/corporation/rml_footer1"/>
                        </drawString>
                        <lineMode width="2"/>
                        <stroke color="blue"/>
                        <lines>
                3.2cm 27cm 20cm 27cm
            </lines>
                        <setFont name="Helvetica" size="8"/>
                        <drawString x="3.2cm" y="26.6cm">
                            <xsl:value-of select="//corporate-header/corporation/name"/>
                        </drawString>
                        <drawRightString x="20cm" y="26.7cm">
                            <xsl:value-of select="//corporate-header/corporation/address[type='default']/street"/>
                        </drawRightString>
                        <drawRightString x="20cm" y="26.4cm"><xsl:value-of select="concat(//corporate-header/corporation/address[type='default']/zip, ' ', //corporate-header/corporation/address[type='default']/city)"/> (<xsl:value-of select="//corporate-header/corporation/address[type='default']/state"/>)</drawRightString>
                        <drawRightString x="20cm" y="26.1cm">Tel. <xsl:value-of select="//corporate-header/corporation/address[type='default']/phone"/> - Fax <xsl:value-of select="//corporate-header/corporation/address[type='default']/fax"/></drawRightString>
                        <drawRightString x="20cm" y="25.8cm">E-mail: <xsl:value-of select="//corporate-header/corporation/address[type='default']/email"/></drawRightString>
                        <rotate degrees="90"/>
                        <setFont name="Helvetica" size="7"/>
                        <drawString x="60mm" y="-4mm">
                            <xsl:value-of select="//corporate-header/corporation/rml_footer2"/>
                        </drawString>
                    </pageGraphics>
                    <frame id="first" x1="1cm" y1="1cm" width="19cm" height="247mm"/>
                </pageTemplate>
            </template>
            <stylesheet>
                <paraStyle name="text" fontName="Helvetica" fontSize="9"/>
                <paraStyle name="vermell" textColor="darkred" fontName="Helvetica-Bold" fontSize="10"/>
                <paraStyle name="blau" textColor="darkblue" fontName="Helvetica-Bold" fontSize="10" leftIndent="0.5cm"/>
                <blockTableStyle id="taula">
                    <blockFont name="Helvetica" size="9"/>
                </blockTableStyle>
                <blockTableStyle id="taula_preus">
                    <blockFont name="Helvetica" size="10"/>
                    <lineStyle kind="GRID" start="0,0" stop="-1,0" colorName="black"/>
                    <blockBackground colorName="silver" start="0,0" stop="-1,0"/>
                    <lineStyle kind="BOX" start="0,1" stop="-1,-1" colorName="black"/>
                    <lineStyle kind="LINEAFTER" start="0,1" stop="-1,-1" colorName="black"/>
                </blockTableStyle>
            </stylesheet>
            <story>
                <spacer length="2cm"/>
                <para style="text" leftIndent="12cm">
                    <xsl:value-of select="partner/nom"/>
                </para>
                <para style="text" leftIndent="12cm">
                    <xsl:value-of select="partner/adreca/nom"/>
                </para>
                <para style="text" leftIndent="12cm">
                    <xsl:value-of select="partner/adreca/carrer"/>
                </para>
                <para style="text" leftIndent="12cm"><xsl:value-of select="concat(partner/adreca/cp, ' ', partner/adreca/ciutat)"/> (<xsl:value-of select="partner/adreca/municipi"/>)</para>
                <para style="text" leftIndent="12cm">Tel. <xsl:value-of select="partner/adreca/telf"/> Fax. <xsl:value-of select="partner/adreca/fax"/></para>
                <spacer length="30"/>
                <blockTable style="taula" colWidths="5cm,14cm">
                    <tr>
                        <td>
                            <para style="text" t="1">Sol�licitud d'Oferta:</para>
                        </td>
                        <td>
                            <xsl:value-of select="numero"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <para style="text" t="1">Refer�ncia prove�dor:</para>
                        </td>
                        <td>
                            <xsl:value-of select="ref_proveidor"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <para style="text" t="1">Refer�ncia nostra:</para>
                        </td>
                        <td>
                            <xsl:value-of select="ref"/>
                        </td>
                    </tr>
                </blockTable>
                <spacer length="15"/>
                <blockTable style="taula_preus" colWidths="1.5cm,8cm,3.5cm,2cm,2cm,2cm" repeatRows="1">
                    <tr>
                        <td>
                            <para style="text" fontName="Helvetica-Bold" t="1">Codi</para>
                        </td>
                        <td>
                            <para style="text" fontName="Helvetica-Bold" t="1">Producte</para>
                        </td>
                        <td>
                            <para style="text" fontName="Helvetica-Bold" t="1">Taxes</para>
                        </td>
                        <td>
                            <para style="text" fontName="Helvetica-Bold" alignment="right" t="1">Quantitat</para>
                        </td>
                        <td>
                            <para style="text" fontName="Helvetica-Bold" alignment="right" t="1">Preu</para>
                        </td>
                        <td>
                            <para style="text" fontName="Helvetica-Bold" alignment="right" t="1">Subtotal</para>
                        </td>
                    </tr>
                    <xsl:apply-templates match="linies/producte" mode="story"/>
                </blockTable>
                <blockTable colWidths="17cm,2cm">
                    <tr>
                        <td>
                            <para style="text" fontName="Helvetica-Bold" alignment="right" t="1">Total sense taxes</para>
                        </td>
                        <td>
                            <para style="text" alignment="right">
                                <xsl:value-of select="format-number(preu_sense_taxes, '###.###,00', 'ca_ES')"/>
                            </para>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <para style="text" fontName="Helvetica-Bold" alignment="right" t="1">Taxes</para>
                        </td>
                        <td>
                            <para style="text" alignment="right">
                                <xsl:value-of select="format-number(preu_amb_taxes, '###.###,00', 'ca_ES')"/>
                            </para>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <para style="text" fontName="Helvetica-Bold" alignment="right" t="1">Total</para>
                        </td>
                        <td>
                            <para style="text" alignment="right">
                                <xsl:value-of select="format-number(preu_total, '###.###,00', 'ca_ES')"/>
                            </para>
                        </td>
                    </tr>
                </blockTable>
                <xsl:if test="notes!=''">
                    <spacer length="25"/>
                    <para style="text" fontName="Helvetica-Bold" t="1">Notes:</para>
                    <xpre style="text">
                        <xsl:value-of select="notes"/>
                    </xpre>
                </xsl:if>
                <spacer length="25"/>
                <para style="text">Atentament,</para>
                <para style="text">
                    <xsl:value-of select="//corporate-header/user/name"/>
                </para>
            </story>
        </document>
    </xsl:template>
    <xsl:template match="linies/producte" mode="story">
        <tr>
            <td>
                <para style="text">
                    <xsl:value-of select="codi_proveidor"/>
                </para>
            </td>
            <td>
                <para style="text">
                    <xsl:value-of select="nom"/>
                </para>
            </td>
            <td>
                <xsl:for-each select="taxes/taxa">
                    <para style="text">
                        <xsl:value-of select="nom"/>
                    </para>
                </xsl:for-each>
            </td>
            <td>
                <para style="text" alignment="right">
                    <xsl:value-of select="concat(format-number(quantitat, '###.##0,00', 'ca_ES'), ' ', unit)"/>
                </para>
            </td>
            <td>
                <para style="text" alignment="right">
                    <xsl:value-of select="format-number(preu, '###.##0,00', 'ca_ES')"/>
                </para>
            </td>
            <td>
                <para style="text" alignment="right">
                    <xsl:value-of select="format-number(subtotal, '###.##0,00', 'ca_ES')"/>
                </para>
            </td>
        </tr>
    </xsl:template>
</xsl:stylesheet>
