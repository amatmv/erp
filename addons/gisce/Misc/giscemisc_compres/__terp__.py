# -*- coding: utf-8 -*-
{
    "name": "Compres",
    "description": """Creació de compres""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Compres",
    "depends":[
        "purchase",
        "product_extended"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscemisc_compres_report.xml",
        "giscemisc_compres_wizard.xml"
    ],
    "active": False,
    "installable": True
}
