# -*- coding: utf-8 -*-
{
    "name": "base_email",
    "description": """Habilita la comprovació del email.""",
    "version": "0-dev",
    "author": "Bartomeu Miro",
    "category": "Generic Modules/Base",
    "depends":[
        "base_extended"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "base_email_data.xml"
    ],
    "active": False,
    "installable": True
}
