# -*- encoding: utf-8 -*-

from osv import osv, fields
import re
from tools.translate import _

EMAIL_REGEX = re.compile(
    r'^[a-z0-9çñ]+([a-z0-9-._çñ]+)*@[a-z0-9çñ]+([a-z0-9-._çñ]+)*(.[a-z]{2,4})$'
        .decode('utf-8'))


class ResPartnerAddress(osv.osv):
    _name = 'res.partner.address'
    _inherit = 'res.partner.address'

    def check_email(self, cursor, uid, ids):
        '''
        Check if email is correct
        '''
        cfg_obj = self.pool.get('res.config')
        is_check_email = int(cfg_obj.get(cursor, uid, 'check_email', '1'))
        if is_check_email:
            for partner_address in self.browse(cursor, uid, ids):
                if not partner_address.email:
                    continue

                for email in partner_address.email.split(','):
                    if not re.match(EMAIL_REGEX, email):
                        return False

        return True

    def create(self, cr, user, vals, context=None):
        '''
        Overwrite method create
        '''
        if vals and 'email' in vals and vals['email']:
           #  Delete all white spaces character (\t \n \r \f \v) and put
           # lower case
           vals['email'] = re.sub(r"\s+", "", vals['email']).lower()

        return super(ResPartnerAddress,
                     self).create(cr, user, vals, context=context)

    def write(self, cr, user, ids, vals, context=None):
        '''
        Overwrite method write
        '''
        if vals and 'email' in vals and vals['email']:
            #  Delete all white spaces character (\t \n \r \f \v) and put
            # lower case
            vals['email'] = re.sub(r"\s+", "", vals['email']).lower()

        return super(ResPartnerAddress,
                     self).write(cr, user, ids, vals, context=context)

    _constraints = [(check_email,
                     _(u'Correu electrònic incorrecte.'),
                     ['email'])]

ResPartnerAddress()
