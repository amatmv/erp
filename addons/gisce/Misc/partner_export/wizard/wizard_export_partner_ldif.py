# -*- coding: iso-8859-1 -*-
import sys
import wizard
import pooler
import base64
import StringIO
import ldif

view_form="""<?xml version="1.0"?>
<form string="Export Partners to LDIF">
  <image name="gtk-info" size="64" colspan="2"/>
  <group colspan="2" col="4">
    <separator string="Export done" colspan="4"/>
    <field name="data" readonly="1" colspan="3"/>
    <label align="0.0" string="Save this document to a .LDIF file and import it wit
h\n your favourite email/agenda software. The file\n encoding is UTF-8." colspan="4"/>
  </group>
</form>"""

class wizard_export_partner_ldif(wizard.interface):
  
  def _get_file(self, cr, uid, data, context):
    
    def _partner_name(partner):
      name = ""
      if partner.name:
        name = unicode(partner.name, errors='ignore')
      return name      

    def _partner_phone(partner):
      phone = ""
      if partner.phone:
        phone = partner.phone
      return phone

    def _partner_mobile(partner):
      mobile = ""
      if partner.mobile:
        mobile = partner.mobile
      return mobile

    def _partner_fax(partner):
      fax = ""
      if partner.fax:
        fax = partner.fax
      return fax

    def _partner_email(partner):
      email = ""
      if partner.email:
        email = partner.email
      return email
  
    def _partner_title(partner):
      title = ""
      if partner.title:
        email = partner.title
      return title

    def _partner_partner(partner):
      name_partner = ""
      if partner.partner:
        name_partner = partner.partner
      return name_partner

    def _partner_address(partner):
      address = {
        'street': '',
        'street2': '',
        'state': '',
        'city': '',
        'zip': '',
        'country': '',
      }
      if partner.street:
        address["street"]=unicode(partner.street,errors='ignore')
      if partner.street2:
        address["street2"]=unicode(partner.street2,errors='ignore')
      if partner.state:
        address["state"]=unicode(partner.state,errors='ignore')
      if partner.city:
        address["city"]=unicode(partner.city,errors='ignore')
      if partner.zip:
        address["zip"]=str(partner.zip)
      if partner.country:
        address["country"]=unicode(partner.country,errors='ignore')
      return address      

    partner_obj=pooler.get_pool(cr.dbname).get('res.partner.address')
    ids = partner_obj.search(cr, uid, [])
    partners=partner_obj.browse(cr, uid, ids)
    buf=StringIO.StringIO()
    for partner in partners:
      address = _partner_address(partner)
      entry={
        'objectClass':['top', 'person', 
        'organizationalPerson', 'inetOrgPerson'], 
        'cn':[_partner_name(partner)], 
        'sn': [''],
        'mail': [_partner_email(partner)],
        'givenName': [_partner_name(partner)],
        'telephoneNumber': [_partner_phone(partner)],
        'mobile': [_partner_mobile(partner)],
        'fax': [_partner_fax(partner)],
        'street': [address["street"]],
        'l':[address["city"]],
        'st':[address["state"]],
        'postalCode':[address["zip"]],
        'c':[address["country"]],
        'title': [_partner_title(partner)],
        'company': [_partner_partner(partner)],
      }
      dn='cn='+_partner_name(partner)+',mail='+_partner_email(partner)
      ldif_writer=ldif.LDIFWriter(buf)
      ldif_writer.unparse(dn,entry)
    out = base64.encodestring(buf.getvalue())
    buf.close()
    return {'data': out}

  fields_form={
    'data': {'string':'File', 'type':'binary', 'readonly': True,},
  }

  states={
    'init':{
      'actions':[_get_file],
      'result':{
        'type':'form', 
        'arch':view_form, 
        'fields': fields_form, 
        'state':[('end', 'Close', 'gtk-cancel')]}
    },
  }

wizard_export_partner_ldif('wizard_export_partner_ldif.export')
