# -*- coding: iso-8859-1 -*-
import wizard
import pooler
import base64
import StringIO
import vobject

view_form="""<?xml version="1.0"?>
<form string="Export Partners to VCARD">
  <image name="gtk-info" size="64" colspan="2"/>
  <group colspan="2" col="4">
    <separator string="Export done" colspan="4"/>
    <field name="data" readonly="1" colspan="3"/>
    <label align="0.0" string="Save this document to a .VCARD file and import it wit
h\n your favourite email/agenda software. The file\n encoding is UTF-8." colspan="4"/>
  </group>
</form>"""

class wizard_export_partner_vcard(wizard.interface):
  
  def _get_file(self, cr, uid, data, context):
    
    def _partner_name(partner):
      name = ""
      if partner.name:
        name = unicode(partner.name, errors='ignore')
      return name      

    def _partner_phone(partner):
      phone = ""
      if partner.phone:
        phone = partner.phone
      return phone

    def _partner_mobile(partner):
      mobile = ""
      if partner.mobile:
        mobile = partner.mobile
      return mobile

    def _partner_email(partner):
      email = ""
      if partner.email:
        email = partner.email
      return email      

    def _partner_address(partner):
      address = {
        'street': '',
        'street2': '',
        'state': '',
        'city': '',
        'zip': '',
        'country': '',
      }
      if partner.street:
        address["street"]=unicode(partner.street,errors='ignore')
      if partner.street2:
        address["street2"]=unicode(partner.street2,errors='ignore')
      if partner.state:
        address["state"]=unicode(partner.state,errors='ignore')
      if partner.city:
        address["city"]=unicode(partner.city,errors='ignore')
      if partner.zip:
        address["zip"]=str(partner.zip)
      if partner.country:
        address["country"]=unicode(partner.country,errors='ignore')
      return address      

    partner_obj=pooler.get_pool(cr.dbname).get('res.partner.address')
    ids = partner_obj.search(cr, uid, [])
    partners=partner_obj.browse(cr, uid, ids)
    buf=StringIO.StringIO()
    for partner in partners:
      vcard = vobject.vCard()
      vcard.add('n')
      vcard.n.value = vobject.vcard.Name(_partner_name(partner))
      vcard.add('fn')
      vcard.fn.value = _partner_name(partner)
      vcard.add('email')
      vcard.email.value = _partner_email(partner)
      vcard.email.type_param = 'INTERNET'
      vcard.add('phone')
      vcard.phone.value = _partner_phone(partner)
      vcard.phone.type_param = 'WORK'
      vcard.add('adr')
      address = _partner_address(partner)
      vcard.adr.value = vobject.vcard.Address(street=address["street"],city=address["city"],region=address["state"],code='',country=address["country"], box=address["zip"], extended=address["street2"])
      buf.write(vcard.serialize())
    out = base64.encodestring(buf.getvalue())
    buf.close()
    return {'data': out}

  fields_form={
    'data': {'string':'File', 'type':'binary', 'readonly': True,},
  }

  states={
    'init':{
      'actions':[_get_file],
      'result':{
        'type':'form', 
        'arch':view_form, 
        'fields': fields_form, 
        'state':[('end', 'Close', 'gtk-cancel')]}
    },
  }

wizard_export_partner_vcard('wizard_export_partner_vcard.export')
