# -*- coding: utf-8 -*-
{
    "name": "Partner export",
    "description": """Export partners to VCARD or LDIF files""",
    "version": "0-dev",
    "author": "Raül Pérez",
    "category": "Generic Modules/Partners",
    "depends":[
        "base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "partner_export_wizard.xml"
    ],
    "active": False,
    "installable": True
}
