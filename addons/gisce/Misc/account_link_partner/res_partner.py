# -*- coding: utf-8 -*-

from osv import osv, fields
from osv.orm import OnlyFieldsConstraint
from tools.translate import _

CODIGO_CONTABLE_CALC_FIELDS = ['property_account_receivable', 'codigo_contable']


class ResPartner(osv.osv):
    _name = 'res.partner'
    _inherit = 'res.partner'

    def default_codigo_contable(self, cursor, uid, context=None):

        return self.pool.get('ir.sequence').get_next(
            cursor, uid, code='res.partner.codigo.contable'
        )

    def create(self, cursor, uid, vals, context=None):
        if context is None:
            context = {}
        conf_obj = self.pool.get('res.config')
        partner_auto_code_enabled = bool(int(conf_obj.get(
            cursor, uid, 'partner_auto_account_code', False
        )))
        if not vals.get('codigo_contable', False) and partner_auto_code_enabled:
            vals['codigo_contable'] = (
                self.default_codigo_contable(cursor, uid, context)
            )
        return super(ResPartner, self).create(cursor, uid, vals, context)

    def _codigo_contable_unico(self, cursor, uid, ids):

        # Comprobar si hay partners con el mismo código contable
        for partner_data in self.read(
                cursor, uid, ids, CODIGO_CONTABLE_CALC_FIELDS):
            if not partner_data['codigo_contable']:
                continue
            partner_id = partner_data['id']
            cuenta_clientes_id = partner_data['property_account_receivable'][0]
            partner_dupl_ids = self.search(cursor, uid, [
                ('id', '!=', partner_id),
                ('codigo_contable', '=', partner_data['codigo_contable']),
                ('property_account_receivable', '=', cuenta_clientes_id),
            ])
            if partner_dupl_ids:
                return False
        return True

    _columns = {
        'codigo_contable': fields.char(
            string=u'Código contable', readonly=False, size=5,
            required=False
        ),
    }

    _constraints = [
        OnlyFieldsConstraint(
            _codigo_contable_unico,
            _(u'Solo puede haber un código contable único'),
            CODIGO_CONTABLE_CALC_FIELDS
        )
    ]


ResPartner()
