# -*- coding: utf-8 -*-

from destral import testing
from destral.transaction import Transaction


class TestPartner(testing.OOTestCase):
    def test_partner_has_codigo_contable_with_config_enabled(self):
        """Comprobar que al crear un partner se crea con/sin código contable en
        función de la variable"""
        imd_obj = self.openerp.pool.get('ir.model.data')
        conf_obj = self.openerp.pool.get('res.config')
        partner_obj = self.openerp.pool.get('res.partner')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            config_id = imd_obj.get_object_reference(
                cursor, uid, 'account_link_partner', 'partner_auto_account_code'
            )[1]

            # inv_obj = self.openerp.pool.get('account.invoice')

            # Desactivar variable de configuración
            conf_obj.write(cursor, uid, [config_id], {'value': '0'})
            partner_without_code_id = partner_obj.create(
                cursor, uid, {'name': 'Partner sin código contable'}
            )

            # Comprobar que el código contable no se ha rellenado
            codigo_contable = partner_obj.read(
                cursor, uid, partner_without_code_id, ['codigo_contable']
            )['codigo_contable']
            self.assertEqual(codigo_contable, False)

            # Activar variable de configuración

            conf_obj.write(cursor, uid, [config_id], {'value': '1'})
            partner_with_code_id = partner_obj.create(
                cursor, uid, {'name': 'Partner sin código contable'}
            )

            # Comprobar que el código contable se ha rellenado
            codigo_contable = partner_obj.read(
                cursor, uid, partner_with_code_id, ['codigo_contable']
            )['codigo_contable']
            self.assertNotEqual(codigo_contable, False)
