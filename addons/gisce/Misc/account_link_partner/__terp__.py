# -*- coding: utf-8 -*-
{
    "name": "",
    "description": """This module adds the following features:
  * Adds accounting code to partner
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends": [
        "base_extended",
        "account",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "res_partner_data.xml",
        "res_partner_view.xml",
    ],
    "active": False,
    "installable": True
}
