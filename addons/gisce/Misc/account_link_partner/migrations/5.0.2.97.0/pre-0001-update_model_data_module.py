# coding=utf-8

import logging


def up(cursor, installed_version):

    logger = logging.getLogger('openerp.migration')

    logger.info(
        'Updating ir.model.data from account_link_%% modules '
        'to account_link_partner'
    )

    cursor.execute("""
        UPDATE ir_model_data
        SET module = 'account_link_partner'
        WHERE module LIKE 'account_link_%%'
        AND name IN (
            'seq_type_res_partner_codigo_contable', 
            'seq_res_partner_codigo_contable',
            'field_res_partner_codigo_contable'
        )
    """)

    logger.info('Updated ir.model.data successfully!')


def down(cursor, installed_version):
    pass


migrate = up
