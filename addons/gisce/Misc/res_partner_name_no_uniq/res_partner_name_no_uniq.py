from osv import fields, osv


class res_partner_name_no_uniq(osv.osv):

  _name = 'res.partner'
  _auto = False


  def init(self, cr):
    cr.execute("SELECT * from information_schema.constraint_table_usage where constraint_name = 'res_partner_name_uniq'")
    constraint = cr.fetchall()
    if constraint and len(constraint):
      cr.execute("ALTER TABLE res_partner DROP CONSTRAINT res_partner_name_uniq")
      cr.commit()

res_partner_name_no_uniq()
