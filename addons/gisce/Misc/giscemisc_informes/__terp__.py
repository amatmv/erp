# -*- coding: utf-8 -*-
{
    "name": "Giscemisc Informes",
    "description": """ 
        This module is used to generate generic simply informes without 
        know how works pandas library.
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends": [
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
    ],
    "active": False,
    "installable": True
}
