# -*- encoding: utf-8 -*-
import pandas as pd
import StringIO
# import __builtin__
from base64 import b64encode

# __all__ = ['Informe']


class Informe(object):

    def __init__(self, data_dict, name=None, type='csv', columns=None, rename_columns={}, fill_na=None):
        """

        :param data_dict: dictionary with data to generate dataframe
                ex. {'key_1': value, ...}
        :param name: file name with or without extension, in last case will
        use type to complete name with extension
                ex. file_name.csv or file_name
        :param type: csv or xlsx
        :param columns: List with keys with spected order to generate dataframe
                ex. [column_name_1, column_name_3, column_name_2]
        :param rename_columns: dict used to rename columns name
                ex.{'ori_column_1': 'new_column_1'...}
        """
        if type not in ('csv', 'xlsx', 'xls'):
            raise Exception('File type should be csv, xlsx or xls')
        self._type = type
        if not name:
            self._name = 'unknow_name.{}'.format(type)
        else:
            if name.endswith('.csv') or name.endswith('.xlsx') or name.endswith('.xls'):
                self._name = name
            else:
                self._name = '{}.{}'.format(name, type)

        self._columns = columns
        if columns:
            self._data_frame = pd.DataFrame(data=data_dict, columns=columns)
        else:
            self._data_frame = pd.DataFrame(data=data_dict)
        if rename_columns:
            self._data_frame.rename(columns=rename_columns, inplace=True)

        if fill_na:
            self._data_frame.fillna(value=fill_na, inplace=True)

    def generate_informe(self, encoding='utf-8-sig', separator=';', decimal=',', float_format='%.2f', use_headers=True):
        generated_file = StringIO.StringIO()
        if self._type == 'csv':
            if not use_headers:
                self._data_frame.to_csv(
                    generated_file, index=False, header=None, sep=separator,
                    decimal=decimal, encoding=encoding,
                    float_format=float_format
                )
            else:
                self._data_frame.to_csv(
                    generated_file, sep=separator, decimal=decimal,
                    encoding=encoding, float_format=float_format, index=None
                )
        elif self._type in ('xlsx', 'xls'):
            writer = pd.ExcelWriter(generated_file)
            if not use_headers:
                self._data_frame.to_excel(excel_writer=writer, index=False, header=None, float_format=float_format, encoding=encoding)
            else:
                self._data_frame.to_excel(excel_writer=writer, float_format=float_format, index=None, encoding=encoding)
            writer.close()

        res = generated_file.getvalue()
        generated_file.close()

        return res

    def generate_informe_b64(self, encoding='utf-8-sig', separator=';', decimal=',', float_format='%.2f', use_headers=True):
        return b64encode(
            self.generate_informe(
                encoding, separator, decimal, float_format, use_headers
            )
        )

    @property
    def file_name(self):
        return self._name

    @property
    def df(self):
        return self._data_frame

# __builtin__.Informe = Informe
