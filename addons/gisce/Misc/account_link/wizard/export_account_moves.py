# -*- coding: utf-8 -*-
from tools.translate import _
from osv import osv, fields
import pandas as pd


class WizardExportAccountMoves(osv.osv_memory):
    _name = 'wizard.export.account.moves'

    def export_account_moves(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        wiz = self.browse(cursor, uid, ids[0])

        acc_move_obj = self.pool.get('account.move')
        acc_move_line_obj = self.pool.get('account.move.line')

        # Get account moves to export
        acc_move_ids = acc_move_obj.get_move_ids_to_export(
            cursor, uid, wiz.initial_date, wiz.end_date,
            wiz.include_exported_moves, context=context
        )

        if acc_move_ids:
            # Get account move lines to get data
            acc_move_line_ids = acc_move_line_obj.q(cursor, uid).read(['id']).where(
                [('move_id', 'in', acc_move_ids)]
            )
            acc_move_line_ids = [x['id'] for x in acc_move_line_ids]
            acc_move_line_data = acc_move_line_obj.extract_data(
                cursor, uid, acc_move_line_ids, context=context)
            self.export_account_moves_data(
                cursor, uid, wiz.id, acc_move_line_data, context=context)

            # Mark account moves as exported
            acc_move_obj.mark_as_exported(
                cursor, uid, acc_move_ids, context=context)
        else:
            raise osv.except_osv(
                _(u"No data to export"),
                _("There are not account moves to export between the specified "
                  "dates")
            )

        wiz.write({'state': 'end'})

    def process_join_rows(self, cursor, uid, rows, group_by=[], join={}, where=None, context=None):
        if context is None:
            context = {}
        df = pd.DataFrame(rows)
        if not len(group_by) and len(join):
            group_by = list(set(df.columns.values) - set(join.keys()))
        if where:
            df_to_join = df.loc[eval("df['{0}'] {1} '{2}'".format(*where))]
            df_not_join = df.loc[~df.index.isin(list(df_to_join.index))]
        else:
            df_to_join = df.copy()
            df_not_join = pd.DataFrame([])
        df_to_join.fillna(0, inplace=True)
        df_to_join = df_to_join.groupby(group_by).aggregate(join).reset_index()
        df = pd.concat([df_to_join, df_not_join])
        return df.to_dict('records')

    _columns = {
        'initial_date': fields.date(string='Initial date', required=True),
        'end_date': fields.date(string='End date', required=True),
        'state': fields.selection([('init', 'Init'),
                                   ('error', 'Error'),
                                   ('end', 'End')], 'State'),
        'include_exported_moves': fields.boolean(
            string='Include exported moves')
    }

    _defaults = {
        'state': lambda *a: 'init',
        'include_exported_moves': lambda *a: False
    }


WizardExportAccountMoves()
