# -*- coding: utf-8 -*-

from destral import testing
from destral.transaction import Transaction


class TestsAccountMove(testing.OOTestCase):

    def test_move_lines_to_export(self):
        irmd_o = self.openerp.pool.get('ir.model.data')
        move_o = self.openerp.pool.get('account.move')
        journal_o = self.openerp.pool.get('account.journal')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            journal_id = irmd_o.get_object_reference(
                cursor, uid, 'account', 'sales_journal'
            )[1]
            initial_date = '1970-01-01'
            end_date = '2100-01-01'
            all_move_ids_to_export = move_o.get_move_ids_to_export(
                cursor, uid, initial_date, end_date
            )
            dmn = [('journal_id', '=', journal_id)]
            sales_journal_move_ids = move_o.search(cursor, uid, dmn)

            journal_wv = {'account_link_exportable': False}
            journal_o.write(cursor, uid, journal_id, journal_wv)

            some_move_ids_to_export = move_o.get_move_ids_to_export(
                cursor, uid, initial_date, end_date
            )

            self.assertEqual(
                set(sales_journal_move_ids),
                set(all_move_ids_to_export) - set(some_move_ids_to_export)
            )
