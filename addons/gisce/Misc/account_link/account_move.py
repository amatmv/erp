# -*- coding: utf-8 -*-

from osv import osv, fields
from datetime import datetime


class AccountMove(osv.osv):
    _name = 'account.move'
    _inherit = 'account.move'

    def mark_as_exported(self, cursor, uid, acc_move_ids, date_exported=None,
                         context=None):
        if context is None:
            context = {}
        if not isinstance(acc_move_ids, list):
            acc_move_ids = [acc_move_ids]
        if acc_move_ids:
            if not date_exported:
                date_exported = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

            sql = (
                """
                    UPDATE account_move
                    SET exported = True,
                    date_exported = %(date_exported)s,
                    write_uid = %(write_uid)s,
                    write_date = %(write_date)s
                    WHERE id IN %(ids)s
                """
            )
            cursor.execute(sql, dict(
                date_exported=date_exported, write_uid=uid,
                write_date=date_exported, ids=tuple(acc_move_ids)
            ))

    def get_move_ids_to_export(self, cursor, uid, initial_date, end_date,
                               include_exported=False, context=None):
        search_params = [
            ('date', '>=', initial_date),
            ('date', '<=', end_date),
            ('journal_id.account_link_exportable', '=', True),
        ]
        if not include_exported:
            search_params.append(('exported', '=', False))
        result = self.q(cursor, uid).read(['id']).where(search_params)
        return [x['id'] for x in result]

    _columns = {
        'exported': fields.boolean(string='Exported', readonly=True),
        'date_exported': fields.datetime(string='Date exported', readonly=True)
    }

    _defaults = {
        'exported': lambda *a: False
    }


AccountMove()


class AccountMoveLine(osv.osv):
    _name = 'account.move.line'
    _inherit = 'account.move.line'




AccountMoveLine()


class AccountJournal(osv.osv):
    _name = 'account.journal'
    _inherit = 'account.journal'

    _columns = {
        'account_link_exportable': fields.boolean('Account link exportable'),
    }

    _defaults = {
        'account_link_exportable': lambda *a: True,
    }


AccountJournal()
