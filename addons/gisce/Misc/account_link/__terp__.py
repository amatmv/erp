# -*- coding: utf-8 -*-
{
    "name": "",
    "description": """This module adds the following features:
  * Allows to export account moves
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "account"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "account_move_view.xml",
        "security/ir.model.access.csv",
        "wizard/export_account_moves_view.xml"
    ],
    "active": False,
    "installable": True
}
