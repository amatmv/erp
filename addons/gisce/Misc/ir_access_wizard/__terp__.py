# -*- coding: utf-8 -*-
{
    "name": "Wizard to create access rules",
    "description": """Wizard that allows to quickly set the acces rules""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "IR",
    "depends":[
        "base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/wizard_ir_model_access_fast_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
