# coding=utf-8
import csv
from StringIO import StringIO

from osv import osv, fields


CSV_HEADER = ["id", "name", "model_id:id", "group_id:id", "perm_read",
              "perm_write", "perm_create", "perm_unlink"]


class WizardIrModelAccessFast(osv.osv_memory):
    _name = 'wizard.ir.model.access.fast'

    _columns = {
        'name': fields.char('Permission name', size=64),
        'models': fields.many2many(
            'ir.model', 'wizard_model_access_rel', 'wiz_id', 'model_id',
            string='Models'
        ),
        'groups': fields.many2many(
            'res.groups', 'wizard_model_access_group_rel', 'wiz_id', 'group_id',
            string='Groups'
        ),
        'r': fields.boolean('Read'),
        'c': fields.boolean('Create'),
        'w': fields.boolean('Write'),
        'd': fields.boolean('Delete'),
        'ir_model_access_csv': fields.text('CSV', readonly=True)
    }

    def get_permissions(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        permissions = []
        wiz = self.browse(cursor, uid, ids[0], context=context)
        for model in wiz.models:
            perm = 'access_{}_{}'.format(
                    model.model.replace('.', '_'),
                    wiz.get_rcwd().lower()
            )
            for grup in wiz.groups:
                permissions.append({
                    'name': perm,
                    'group_id': grup.id,
                    'model_id': model.id,
                    'perm_read': wiz.r,
                    'perm_unlink': wiz.d,
                    'perm_write': wiz.w,
                    'perm_create': wiz.c
                })
        return permissions

    def get_rcwd(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        wiz = self.browse(cursor, uid, ids[0], context=context)
        return '{}{}{}{}'.format(
                    wiz.r and 'R' or '',
                    wiz.c and 'C' or '',
                    wiz.w and 'W' or '',
                    wiz.d and 'D' or ''
        )

    def get_csv(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        model_obj = self.pool.get('ir.model')

        fileobj = StringIO()

        csv_w = csv.writer(fileobj, delimiter=',', quoting=csv.QUOTE_NONNUMERIC)
        csv_w.writerow(CSV_HEADER)

        wiz = self.browse(cursor, uid, ids[0], context=context)

        imd_obj = self.pool.get('ir.model.data')

        for permission in wiz.get_permissions():
            model = model_obj.browse(cursor, uid, permission['model_id'])

            group_ref = imd_obj.search(cursor, uid, [
                ('model', '=', 'res.groups'),
                ('res_id', '=', permission['group_id'])
            ])
            if group_ref:
                imd = imd_obj.browse(cursor, uid, group_ref[0])
                group = '{}.{}'.format(imd.module, imd.name)
            else:
                group = ''

            csv_w.writerow([
                permission['name'],
                model.model,
                'model_{}'.format(model.model.replace('.', '_')),
                group
            ] + [p in wiz.get_rcwd() and 1 or 0 for p in 'RWCD'])
        wiz.write({'ir_model_access_csv': fileobj.getvalue()})
        fileobj.close()
        return True

    def save_perms(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        access_obj = self.pool.get('ir.model.access')
        wiz = self.browse(cursor, uid, ids[0], context=context)
        for permission in wiz.get_permissions():
            access_obj.create(cursor, uid, permission)
        return {}

    _defaults = {
        'r': lambda *a: 0,
        'c': lambda *a: 0,
        'w': lambda *a: 0,
        'd': lambda *a: 0
    }

WizardIrModelAccessFast()
