# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* ir_access_wizard
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.14\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2012-03-28 16:07:38+0000\n"
"PO-Revision-Date: 2012-03-28 16:07:38+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: ir_access_wizard
#: wizard_field:wizard.ir.model.access.fast,init,c:0
msgid "Crear"
msgstr ""

#. module: ir_access_wizard
#: model:ir.module.module,shortdesc:ir_access_wizard.module_meta_information
msgid "Wizard to create access rules"
msgstr ""

#. module: ir_access_wizard
#: wizard_button:wizard.ir.model.access.fast,init,guardar:0
msgid "Continuar"
msgstr ""

#. module: ir_access_wizard
#: model:ir.actions.wizard,name:ir_access_wizard.wizard_ir_model_access_fast
msgid "Crear normes d'accés"
msgstr ""

#. module: ir_access_wizard
#: wizard_field:wizard.ir.model.access.fast,init,models:0
msgid "Models"
msgstr ""

#. module: ir_access_wizard
#: wizard_button:wizard.ir.model.access.fast,init,end:0
msgid "Cancelar"
msgstr ""

#. module: ir_access_wizard
#: wizard_field:wizard.ir.model.access.fast,init,w:0
msgid "Escriure"
msgstr ""

#. module: ir_access_wizard
#: wizard_field:wizard.ir.model.access.fast,init,r:0
msgid "Lectura"
msgstr ""

#. module: ir_access_wizard
#: wizard_field:wizard.ir.model.access.fast,init,group:0
msgid "Grup"
msgstr ""

#. module: ir_access_wizard
#: wizard_view:wizard.ir.model.access.fast,init:0
msgid "Informacio d'entrada"
msgstr ""

#. module: ir_access_wizard
#: wizard_view:wizard.ir.model.access.fast,init:0
msgid "Permisos"
msgstr ""

#. module: ir_access_wizard
#: wizard_field:wizard.ir.model.access.fast,init,d:0
msgid "Eliminar"
msgstr ""

