# -*- coding: utf-8 -*-
{
    "name": "Automatic calculation price for products",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Generic Modules/Inventory Control",
    "depends":[
        "product_extended"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "product_extended_auto_wizard.xml"
    ],
    "active": False,
    "installable": True
}
