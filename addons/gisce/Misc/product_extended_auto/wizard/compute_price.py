# -*- coding: utf-8 -*-
import wizard
import pooler


_init_form = """<?xml version="1.0"?>
<form string="Actualització de preus de cost" >
  <image name="gtk-refresh" colspan="1" />
  <label string="Es procedirà a actualitzar el preu de cost dels productes que tinguin activada la casella de 'calcular preu' i que tinguin un desglossat correcte." colspan="3" />
</form>"""

_init_fields = {}

def _compute(self, cr, uid, data, context={}):
  p_obj = pooler.get_pool(cr.dbname).get('product.product')
  p_obj.compute_price(cr, uid, p_obj.search(cr, uid, [('calculate_price', '=', True)]))
  return {}

_compute_form = """<?xml version="1.0"?>
<form string="Actualització de preus de cost" >
  <image name="gtk-ok" colspan="1" />
  <label string="S'han actualitzat tots els productes correctament." colspan="3" />
</form>"""

_compute_fields = {}



class wizard_product_extended_auto_compute(wizard.interface):

  states = {
    'init': {
    	'actions': [],
      'result': {'type': 'form', 'arch': _init_form, 'fields': _init_fields, 'state': [('end', 'Cancel·lar', 'gtk-cancel'), ('compute', 'Continuar', 'gtk-go-forward')]}
    },
    'compute': {
    	'actions': [_compute],
      'result': {'type': 'form', 'arch': _compute_form, 'fields': _compute_fields, 'state': [('end', 'Tancar', 'gtk-close')]}
    },
  }

wizard_product_extended_auto_compute('product.extended.auto.compute')


