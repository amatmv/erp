# -*- coding: utf-8 -*-
{
    "name": "GISCE Misc CTS Google Map",
    "description": """Afegeix la imatge treta del google maps static image""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Centres Transformadors",
    "depends":[
        "giscegis_cts"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscemisc_cts_googlemap_wizard.xml",
        "giscemisc_cts_googlemap_view.xml"
    ],
    "active": False,
    "installable": True
}
