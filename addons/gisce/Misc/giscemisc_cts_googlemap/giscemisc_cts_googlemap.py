# -*- coding: utf-8 -*-

from osv import fields, osv

class giscemisc_cts_googlemap(osv.osv):

    _name = 'giscedata.cts'
    _inherit = 'giscedata.cts'

    _columns = {
      'map': fields.binary('Imatge del mapa'),
    }


giscemisc_cts_googlemap()
