from LatLongUTMconversion import UTMtoLL
import wizard
import urllib
import base64
import pooler

def _download(self, cr, uid, data, context={}):
    ct_obj = pooler.get_pool(cr.dbname).get('giscedata.cts')
    if data['model'] != 'giscedata.cts':
        cr.execute("SELECT ct.id, ct.name, v.x, v.y from giscegis_vertex v, giscegis_blocs_ctat bct, giscedata_cts ct where bct.ct = ct.id and bct.vertex = v.id and bct.id is not null limit 10")
    else:
        cr.execute("SELECT ct.id, ct.name, v.x, v.y from giscegis_vertex v, giscegis_blocs_ctat bct, giscedata_cts ct where bct.ct = ct.id and bct.vertex = v.id and bct.id is not null and ct.id in ("+','.join(map(str, map(int ,data['ids'])))+")")
    cts = cr.dictfetchall()

    # UTMtoLL(23, Y, X, '31T')
    for ct in cts:
        (lat, lon) = UTMtoLL(23, ct['y'], ct['x'], '31T')
        url = 'http://maps.google.com/staticmap?maptype=hybrid&format=PNG&center=%s,%s&zoom=16&markers=%s,%s&size=600x400&key=ABQIAAAAawEUu5p3RI2u55wBXxStURQk1aylAj3bEIyFlSrOp_cDfXM2WhTR_RHAq6N8JrtSNh7JqvSv9Hsjxg&sensor=false' % (lat, lon, lat, lon)
        f = urllib.urlopen(url)
        ct_obj.write(cr, uid, [ct['id']], {'map': base64.encodestring(f.read())})
        print 'Saving map for %s' % ct['name']

    return {}

class wizard_giscemisc_cts_googlemap_download_maps(wizard.interface):

    states = {
      'init': {
          'actions': [_download],
        'result': {'type': 'state', 'state': 'end'}
      },
    }

wizard_giscemisc_cts_googlemap_download_maps('giscemisc.cts.googlemap.download.maps')
