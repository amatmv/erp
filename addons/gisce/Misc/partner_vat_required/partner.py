# -*- encoding: utf-8 -*-

from osv import osv, fields
from tools.translate import _


class PartnerVatRequired(osv.osv):

    _name = 'res.partner'
    _inherit = 'res.partner'

    def create(self, cursor, uid, vals, context=None):
        '''no permetre crear partners sense vat'''

        vat = vals.get('vat', False)

        if (not vat
            or vat.find('99999999R') != -1
            or vat.find('12345678Z') != -1):
            raise osv.except_osv('Error',
                                 _(u"El nif/cif és obligatori"))

        return super(PartnerVatRequired,
                     self).create(cursor, uid, vals, context=context)

PartnerVatRequired()
