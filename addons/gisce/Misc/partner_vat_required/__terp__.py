# -*- coding: utf-8 -*-
{
    "name": "Partner Vat Required",
    "description": """New partners will require vat to be created
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Misc",
    "depends":[
        "base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
