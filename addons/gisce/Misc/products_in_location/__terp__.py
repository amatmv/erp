# -*- coding: utf-8 -*-
{
    "name": "Products in locacion",
    "description": """
    This module provide :
      * When you look for products in a location only shows the products which qty_available is greater than 0.
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Extra",
    "depends":[
        "base",
        "product",
        "stock"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "products_in_location_wizard.xml",
        "products_in_location_view.xml"
    ],
    "active": False,
    "installable": True
}
