import wizard
import pooler

def _filter(self, cr, uid, data, context=None):
  location_obj = pooler.get_pool(cr.dbname).get('stock.location')
  product_obj = pooler.get_pool(cr.dbname).get('product.product')
  lid = data['id']
  data['location'] = location_obj.browse(cr, uid, lid).name
  context.update({'location': lid})
  
  # Ara naveguem per tots els productes i mirarem quins s'han de mostrar i quins no.
  ids = product_obj.search(cr, uid, [])
  data['p_ids'] = []
  for p in product_obj.read(cr, uid, ids, ['qty_available', 'virtual_available'], context):
    if p['qty_available'] != 0 or p['virtual_available'] != 0:
      data['p_ids'].append(p['id'])
  return {}

def _mostrar_tab(self, cr, uid, data, context={}):
  action = {
    'domain': "[('id','in', ["+','.join(map(str,map(int, data['p_ids'])))+"])]",
    'view_type': 'form',
    'view_mode': 'tree,form',
    'res_model': 'product.product',
    'name': 'Productos en %s' % (data['location']),
    'view_id': False,
    'limit': len(data['p_ids']),
    'type': 'ir.actions.act_window'
  }
  return action

class products_in_location_wizard(wizard.interface):

  states = {
      'init': {
        'actions': [_filter],
        'result': {'type': 'action', 'action': _mostrar_tab, 'state': 'end'}
      },
  }
  
products_in_location_wizard('products.in.location')