# -*- coding: utf-8 -*-
{
    "name": "User department",
    "description": """Adds the department field to user""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Misc",
    "depends":[
        "hr"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "res_user_view.xml"
    ],
    "active": False,
    "installable": True
}
