from osv import osv, fields


class ResUsers(osv.osv):
    """
    Model that extends res.users adding the department field
    """
    _name = "res.users"
    _inherit = "res.users"
    _columns = {
        "department_id": fields.many2one(
            "hr.department",
            "Department"
            ),
    }

ResUsers()
