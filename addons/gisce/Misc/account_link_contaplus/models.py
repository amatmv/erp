# -*- coding: utf-8 -*-

from datetime import datetime
from marshmallow import Schema, fields, post_dump
from marshmallow import ValidationError


class CustomDateStringField(fields.String):
    def _validate(self, value):
        if value is None:
            return None
        try:
            datetime.strptime(value, '%Y-%m-%d')
        except (ValueError, AttributeError):
            raise ValidationError('Invalid date string', value)

    @post_dump
    def _serialize(self, value, attr, obj):
        return datetime.strptime(value, '%Y-%m-%d').strftime('%Y%m%d')


class MoveLine(Schema):
    class Meta:
        ordered = True
    # number of digits field.Decimal(places=6)
    Asien = fields.Decimal(default=0.0)  # 1
    Fecha = CustomDateStringField(default='')  # 2
    SubCta = fields.String(default='')  # 3
    Contra = fields.String(default='')  # 4
    PtaDebe = fields.Decimal(default=0.0)  # 5
    Concepto = fields.String(default='')  # 6
    PtaHaber = fields.Decimal(default=0.0)  # 7
    Factura = fields.String(default='')  # 8 TODO definición como N (número)
    Baseimpo = fields.Decimal(default=0.0)  # 9
    IVA = fields.Decimal(default=0.0)  # 10
    Recequiv = fields.Decimal(default=0.0)  # 11
    Documento = fields.String(default='')  # 12
    Departa = fields.String(default='')  # 13
    Clave = fields.String(default='')  # 14
    Estado = fields.String(default='')  # 15
    Ncasado = fields.Decimal(default=0.0)  # 16
    TCasado = fields.Decimal(default=0.0)  # 17
    Trans = fields.Decimal(default=0.0)  # 18
    Cambio = fields.Decimal(default=0.0)  # 19
    DebeME = fields.Decimal(default=0.0)  # 20
    HaberME = fields.Decimal(default=0.0)  # 21
    Auxiliar = fields.String(default='')  # 22
    Serie = fields.String(default='')  # 23
    Sucursal = fields.String(default='')  # 24
    CodDivisa = fields.String(default='')  # 25
    ImpAuxME = fields.Decimal(default=0.0)  # 26
    MonedaUso = fields.String(default='')  # 27
    EuroDebe = fields.Decimal(default=0.0)  # 28
    EuroHaber = fields.Decimal(default=0.0)  # 29
    BaseEuro = fields.Decimal(default=0.0)  # 30
    NoConv = fields.Boolean(default='')  # 31
    NumeroInv = fields.String(default='')  # 32
    Serie_RT = fields.String(default='')  # 33
    Factu_RT = fields.Decimal(default=0.0)  # 34
    BaseImp_RT = fields.Decimal(default=0.0)  # 35
    BaseImp_RF = fields.Decimal(default=0.0)  # 36
    Rectifica = fields.Boolean(default='')  # 37
    Fecha_RT = CustomDateStringField(default='')  # 38
    NIC = fields.String(default='')  # 39
    Libre = fields.Boolean(default='')  # 40
    Libre2 = fields.Decimal(default=0.0)  # 41
    lInterrump = fields.Boolean(default='')  # 42
    SegActiv = fields.String(default='')  # 43
    SegGeog = fields.String(default='')  # 44
    lRect349 = fields.Boolean(default='')  # 45
    Fecha_OP = CustomDateStringField(default='')  # 46
    Fecha_EX = CustomDateStringField(default='')  # 47
    Departa5 = fields.String(default='')  # 48
    Factura10 = fields.String(default='')  # 49
    Porcen_Ana = fields.Decimal(default=0.0)  # 50
    Porcen_Seg = fields.Decimal(default=0.0)  # 51
    NumApunte = fields.Decimal(default=0.0)  # 52
    EuroTotal = fields.Decimal(default=0.0)  # 53
    RazonSoc = fields.String(default='')  # 54
    Apellido1 = fields.String(default='')  # 55
    Apellido2 = fields.String(default='')  # 56
    TipoOpe = fields.String(default='')  # 57
    nFacTick = fields.Decimal(default=0.0)  # 58
    NumAcuIni = fields.String(default='')  # 59
    NumAcuFin = fields.String(default='')  # 60
    TerIdNif = fields.Decimal(default=0.0)  # 61
    TerNif = fields.String(default='')  # 62
    TerNom = fields.String(default='')  # 63
    TerNif14 = fields.String(default='')  # 64
    TBienTran = fields.Boolean(default='')  # 65
    TBienCod = fields.String(default='')  # 66
    TransInm = fields.Boolean(default='')  # 67
    Metal = fields.Boolean(default='')  # 68
    MetalImp = fields.Decimal(default=0.0)  # 69
    Cliente = fields.String(default='')  # 70
    OpBienes = fields.Decimal(default=0.0)  # 71
    FacturaEx = fields.String(default='')  # 72
    TipoFac = fields.String(default='')  # 73
    TipoIVA = fields.String(default='')  # 74
    GUID = fields.String(default='')  # 75
    L340 = fields.Boolean(default='')  # 76
    MetalEje = fields.Decimal(default=0.0)  # 77
    Document15 = fields.String(default='')  # 78
    ClienteSup = fields.String(default='')  # 79
    FechaSub = CustomDateStringField(default='')  # 80
    ImporteSup = fields.Decimal(default=0.0)  # 81
    DocSup = fields.String(default='')  # 82
    ClientePro = fields.String(default='')  # 83
    FechaPro = CustomDateStringField(default='')  # 84
    ImportePro = fields.Decimal(default=0.0)  # 85
    DocPro = fields.String(default='')  # 86
    nClaveIRPF = fields.Decimal(default=0.0)  # 87
    lArrend347 = fields.Boolean(default='')  # 88
    nSitInmueb = fields.Decimal(default=0.0)  # 89
    cRefCatast = fields.String(default='')  # 90
    Concil347 = fields.Decimal(default=0.0)  # 91
    TipoRegula = fields.Decimal(default=0.0)  # 92
    nCritCaja = fields.Decimal(default=0.0)  # 93
    lCritCaja = fields.Boolean(default='')  # 94
    dMaxLiqui = CustomDateStringField(default='')  # 95
    nTotalFac = fields.Decimal(default=0.0)  # 96
    IdFactura = fields.String(default='')  # 97
    nCobrPago = fields.Decimal(default=0.0)  # 98
    nTipoIG = fields.Decimal(default=0.0)  # 99
    DevoIVAid = fields.String(default='')  # 100
    lDevoluIVA = fields.Boolean(default='')  # 101
    MedioCrit = fields.String(default='')  # 102
    CuentaCrit = fields.String(default='')  # 103
    lConAc = fields.Boolean(default='')  # 104
    GuidSPAY = fields.String(default='')  # 105
    TipoEntr = fields.Decimal(default=0.0)  # 106
    Mod140 = fields.Decimal(default=0.0)  # 107
    FechaAnota = CustomDateStringField(default='')  # 108
    nTipo140 = fields.Decimal(default=0.0)  # 109
    Cuenta140 = fields.String(default='')  # 110
    Importe140 = fields.Decimal(default=0.0)  # 111
    lDepAduan = fields.Boolean(default='')  # 112
    lDifAduan = fields.Boolean(default='')  # 113
    nInter303 = fields.Decimal(default=0.0)  # 114
    IdRecargo = fields.String(default='')  # 115
    EstadoSII = fields.Decimal(default=0.0)  # 116
    TipoClave = fields.Decimal(default=0.0)  # 117
    TipoExenci = fields.Decimal(default=0.0)  # 118
    TipoNoSuje = fields.Decimal(default=0.0)  # 119
    TipoFact = fields.Decimal(default=0.0)  # 120
    NAcuIniSII = fields.String(default='')  # 121
    nAcuFinSII = fields.String(default='')  # 122
    TipoRectif = fields.Decimal(default=0.0)  # 123
    BImpCoste = fields.Decimal(default=0.0)  # 124
    lEmiTercer = fields.Boolean(default='')  # 125
    nEntrPrest = fields.Decimal(default=0.0)  # 126
    Decrecen = CustomDateStringField(default='')  # 127
    FactuEx_RT = fields.String(default='')  # 128
    TipoClave1 = fields.Decimal(default=0.0)  # 129
    TipoClave2 = fields.Decimal(default=0.0)  # 130
    lTAI = fields.Boolean(default='')  # 131
    lExcl303 = fields.Boolean(default='')  # 132
    ConcepNew = fields.String(default='')  # 133
    TerNifNew = fields.String(default='')  # 134
    TerNomNew = fields.String(default='')  # 135
    SII_1415 = fields.Boolean(default='')  # 136
    cAutoriza = fields.String(default='')  # 137
    lEmiTerDis = fields.Boolean(default='')  # 138
    NifSuced = fields.String(default='')  # 139
    RazonSuced = fields.String(default='')  # 140
    lFSimplifi = fields.Boolean(default='')  # 141
    lFSinIdent = fields.Boolean(default='')  # 142


class Account(Schema):
    class Meta:
        ordered = True

    Cod = fields.String(default='')  # 1
    Titulo = fields.String(default='')  # 2
    Nif = fields.String(default='')  # 3
    Domicilio = fields.String(default='')  # 4
    Poblacion = fields.String(default='')  # 5
    Provincia = fields.String(default='')  # 6
    CodPostal = fields.String(default='')  # 7
    Divisa = fields.Boolean(default='')  # 8
    CodDivisa = fields.String(default='')  # 9
    Documento = fields.Boolean(default='')  # 10
    AjustaME = fields.Boolean(default='')  # 11
    TipoIVA = fields.String(default='')  # 12
    Proye = fields.String(default='')  # 13
    SubEquiv = fields.String(default='')  # 14
    SubCierre = fields.String(default='')  # 15
    LInterrump = fields.Boolean(default='')  # 16
    Segmento = fields.String(default='')  # 17
    TPC = fields.Decimal(default=0.0)  # 18
    RecEquiv = fields.Decimal(default=0.0)  # 19
    Fax01 = fields.String(default='')  # 20
    Email = fields.String(default='')  # 21
    TituloL = fields.String(default='')  # 22
    IdNif = fields.Decimal(default=0.0)  # 23
    CodPais = fields.String(default='')  # 24
    Rep14NIF = fields.String(default='')  # 25
    Rep14Nom = fields.String(default='')  # 26
    MetCobro = fields.Boolean(default='')  # 27
    MetCobFre = fields.Boolean(default='')  # 28
    Suplido = fields.Boolean(default='')  # 29
    Provision = fields.Boolean(default='')  # 30
    lEsIRPF = fields.Boolean(default='')  # 31
    nIRPF = fields.Decimal(default=0.0)  # 32
    nClaveIRPF = fields.Decimal(default=0.0)  # 33
    lEsMod130 = fields.Boolean(default='')  # 34
    lDeducible = fields.Boolean(default='')  # 35
    lCritCaja = fields.Boolean(default='')  # 36
    cSubIVAas = fields.String(default='')  # 37
    lPefectivo = fields.Boolean(default='')  # 38
    lIngGasto = fields.Boolean(default='')  # 39
    nTipoIG = fields.Decimal(default=0.0)  # 40
    MedioCrit = fields.String(default='')  # 41
    SubCtaCon = fields.String(default='')  # 42
    nTipoExist = fields.Decimal(default=0.0)  # 43
    SubCtaVar = fields.String(default='')  # 44
    lMod140 = fields.Boolean(default='')  # 45
    lIRPF140 = fields.Boolean(default='')  # 46
    SctaBanco = fields.String(default='')  # 47
    CCodMun = fields.String(default='')  # 48
    NifNew = fields.String(default='')  # 49
    TituloLNew = fields.String(default='')  # 50
