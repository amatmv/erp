# -*- coding: utf-8 -*-
import base64

from account_link_contaplus.models import MoveLine, Account
from osv import osv, fields as fields
import pandas as pd


class WizardExportAccountMoves(osv.osv_memory):
    _name = 'wizard.export.account.moves'
    _inherit = 'wizard.export.account.moves'

    def build_report_csv(self, rows, headers=None):
        df = pd.DataFrame(rows, columns=headers)
        return df.to_csv(
            index=False, header=False, sep=';')

    def export_account_moves_data(self, cursor, uid, wiz_id, acc_move_line_data,
                                  context=None):

        if context is None:
            context = {}

        acc_move_line_obj = self.pool.get('account.move.line')
        inv_obj = self.pool.get('account.invoice')
        acc_inv_tax_obj = self.pool.get('account.invoice.tax')

        def get_cuenta_cliente(cuenta_contable, codigo_contable, digits=4):
            codigo_cuenta_base = cuenta_contable
            if codigo_cuenta_base.startswith('43'):
                codigo_cliente = codigo_contable or ''
                if codigo_cuenta_base.startswith('4315'):
                    # Sustituir cuenta 4315 por 4300
                    codigo_cuenta_base = '4300' + codigo_cuenta_base[4:]
                codigo_cuenta = codigo_cuenta_base.ljust(4, '0')[:4] + codigo_cliente.ljust(digits, '0')
            else:
                codigo_cuenta = codigo_cuenta_base
            return codigo_cuenta

        formato_fecha = '%Y%m%d'
        move_lines_data = []
        accounts_data = []
        seen_accounts = set()

        for line in acc_move_line_data:
            fecha_asiento = line['date']
            # line_import = line['credit'] or line['debit'] or 0.0
            # cash_journal = ['journal_id.type'] == 'cash'
            iva_soportado_line = line['account_id.code'].startswith('472')
            iva_repercutido_line = line['account_id.code'].startswith('477')
            iva_line = iva_soportado_line or iva_repercutido_line
            customer_account = line['account_id.code'].startswith('43')

            if customer_account:
                codigo_cuenta = get_cuenta_cliente(
                    line['account_id.code'], line['partner_id.codigo_contable']
                )
            else:
                codigo_cuenta = line['account_id.code']
            invoice = inv_obj.q(cursor, uid).read([
                'id', 'number', 'type', 'account_id', 'account_id.code',
                'origin'
            ]).where(
                [('move_id', '=', line['move_id'])]
            )
            serie = ''
            tipo_factura = ''
            concepto = line['name']
            if invoice:
                invoice = invoice[0]
                if invoice['type'].startswith('out_'):
                    tipo_factura = 'E'
                    invoice_number = invoice['number']
                elif invoice['type'].startswith('in_'):
                    tipo_factura = 'R'
                    invoice_number = invoice['origin']
                if invoice['type'].endswith('_refund'):
                    serie = 'A'
                else:
                    serie = 'E'
                # Cuando la línea es la del importe total de la factura
                # mostrar el número de factura en vez del contrato
                if invoice['account_id'] == line['account_id']:
                    concepto = line['ref']

            tipo_iva = ''
            cuenta_contrapartida_iva = ''
            if iva_line:
                if iva_repercutido_line:
                    tipo_iva = 'G'
                    search_params = [
                        ('id', '!=', line['id']),
                        ('move_id', '=', line['move_id']),
                        ('account_id.code', 'like', '43%'),
                    ]
                    cuenta_clientes_iva_data = acc_move_line_obj.q(cursor, uid).read(
                        ['account_id.code', 'partner_id.codigo_contable']
                    ).where([
                        ('id', '!=', line['id']),
                        ('move_id', '=', line['move_id']),
                        ('account_id.code', 'like', '43%'),
                    ])
                    if cuenta_clientes_iva_data:
                        cuenta_clientes_iva_data = cuenta_clientes_iva_data[0]
                        cuenta_contrapartida_iva = get_cuenta_cliente(
                            cuenta_clientes_iva_data['account_id.code'],
                            cuenta_clientes_iva_data['partner_id.codigo_contable']
                        )
                elif iva_soportado_line:
                    tipo_iva = 'O'
                    cuenta_contrapartida_iva = invoice['account_id.code']

                base_iva = acc_inv_tax_obj.q(cursor, uid).read(
                    ['base_amount']
                ).where([
                    ('invoice_id', '=', invoice['id']),
                    ('account_id', '=', line['account_id'])
                ])

            move_line_data = MoveLine().dump(dict(
                Asien=line['move_id'],
                Fecha=fecha_asiento,
                SubCta=codigo_cuenta,
                Contra=cuenta_contrapartida_iva if iva_line else '',
                Concepto=concepto,
                Factura=invoice_number if invoice else line['ref'],  # if not cash_journal else '',
                IVA=21 if iva_line else 0,
                Documento=concepto,  # En línea de remesa dejar vacío ?? TODO
                Serie='',  # 'E',  # E = facturas cliente, A = anuladoras, D = devolución remesa ?? , V = facturas derechos TODO
                MonedaUso=2,  # 1.- Pesetas 2.- Euros
                EuroDebe=line['debit'],
                EuroHaber=line['credit'],
                BaseEuro=abs(base_iva[0]['base_amount']) if iva_line else 0.0,  # line_import * 100/21 if iva_line else 0.0,
                RazonSoc=line['partner_id.name'] if iva_line else '',  # Sin Uso
                FacturaEx=invoice_number if invoice else '',  # line['ref'] if not cash_journal else '',  # Revisar TODO
                TipoFac=tipo_factura,  # “E” = Emitida, “R” = Recibida  TODO
                TipoIVA=tipo_iva if iva_line else '',  # 'G' = Repercutido, 'O' = Soportado  TODO
                L340='T',
                Document15='',  # Sin uso
                Decrecen=fecha_asiento,
                ConcepNew=concepto,
            )).data
            move_lines_data.append(move_line_data)
            if customer_account and codigo_cuenta not in seen_accounts:
                seen_accounts.add(codigo_cuenta)
                account_data = Account().dump(dict(
                    Cod=codigo_cuenta,
                    Titulo=line['partner_id.name'],
                    Nif=line['partner_id.vat'],
                    Domicilio='',  # TODO
                    Poblacion='',  # TODO
                    Provincia='',  # TODO
                    CodPostal='',  # TODO
                    TipoIVA=tipo_iva,  # 'G' = Repercutido, 'O' = Soportado  TODO
                    Fax01='',  # TODO
                    Email='',  # TODO
                    TituloL=['account_id.name'],  # TODO no se usa
                    NifNew=line['partner_id.vat'],
                )).data
                accounts_data.append(account_data)

        wiz = self.browse(cursor, uid, wiz_id)
        wiz.write({
            'archivo_contaplus_apuntes': base64.b64encode(pd.DataFrame(move_lines_data, columns=MoveLine().fields.keys()).to_csv(sep=';', header=False, index=False)),
            'archivo_contaplus_apuntes_filename': 'ContaPlusApuntes.csv',
            'archivo_contaplus_subcuentas': base64.b64encode(pd.DataFrame(accounts_data, columns=Account().fields.keys()).to_csv(sep=';', header=False, index=False)),
            'archivo_contaplus_subcuentas_filename': 'ContaPlusSubcuentas.csv',
            'state': 'end',
        })

    _columns = {
        'archivo_contaplus_apuntes': fields.binary(
            string='Archivo ContaPlus Apuntes', readonly=True),
        'archivo_contaplus_apuntes_filename': fields.char(
            string='Nombre archivo ContaPlus', size=128),
        'archivo_contaplus_subcuentas': fields.binary(
            string='Archivo ContaPlus Subcuentas', readonly=True),
        'archivo_contaplus_subcuentas_filename': fields.char(
            string='Nombre archivo ContaPlus Subcuentas', size=128),
    }


WizardExportAccountMoves()
