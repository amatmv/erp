# -*- coding: utf-8 -*-

from osv import osv


class AccountMoveLine(osv.osv):
    _name = 'account.move.line'
    _inherit = 'account.move.line'

    def extract_data(self, cursor, uid, acc_move_line_ids, context=None):
        return self.q(cursor, uid).read([
            'account_id',
            'account_id.name',
            'account_id.code',
            'credit',
            'debit',
            'date',
            'move_id',
            'name',
            'ref',
            'partner_id.codigo_contable',
            'partner_id.name',
            'partner_id.vat',
        ]).where([('id', 'in', acc_move_line_ids)])


AccountMoveLine()
