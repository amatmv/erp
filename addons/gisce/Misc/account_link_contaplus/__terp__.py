# -*- coding: utf-8 -*-
{
    "name": "",
    "description": """Este módulo añade las siguientes funcionalidades:
  * Permite exportar los movimientos contables a través de distintos archivos
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends": [
        "base",
        "account_link",
        "account_link_partner",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "wizard/export_account_moves_view.xml",
    ],
    "active": False,
    "installable": True
}
