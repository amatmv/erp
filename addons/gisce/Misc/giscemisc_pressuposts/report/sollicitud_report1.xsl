<?xml version="1.0" encoding="iso-8859-15"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="sollicituds"/>
  </xsl:template>

  <xsl:template match="sollicituds">
    <xsl:apply-templates select="sollicitud" />
  </xsl:template>

  <xsl:template match="sollicitud">
    <document debug="1" compression="1">
      <template pageSize="(21cm, 29.7cm)">
        <pageTemplate id="main">
          <pageGraphics>
          	<image file="addons/crm_extended/report/logo.png" width="20mm" height="20mm" x="1cm" y="26.7cm" />
          	<setFont name="Helvetica-Bold" size="30" />
          	<drawString x="3cm" y="27.5cm">Bassols</drawString>
          	<setFont name="Helvetica" size="10" />
          	<drawString x="3cm" y="27.1cm">DISTRIBUCI� D'ENERGIA EL�CTRICA</drawString>
          	<lineMode width="2" />
          	<stroke color="lightgreen"/>
          	<lines>
          		3cm 27cm 20cm 27cm
          	</lines>
          	<setFont name="Helvetica" size="8" />
          	<drawString x="3cm" y="26.6cm">BASSOLS ENERGIA, S.A</drawString>
          	<drawRightString x="20cm" y="26.7cm">Avinguda de Girona, 2</drawRightString>
          	<drawRightString x="20cm" y="26.4cm">Tel. 972 26 01 50 - Fax 972 26 98 99</drawRightString>
          	<drawRightString x="20cm" y="26.1cm">17800 OLOT (Girona)</drawRightString>
          	<drawRightString x="20cm" y="25.8cm">E-mail: bassols@bassols-energia.es</drawRightString>
          </pageGraphics>
          <frame id="first" x1="1cm" y1="1cm" width="19cm" height="247mm"/>
        </pageTemplate>
      </template>
      
      <stylesheet> 
      	<paraStyle name="text"
      		fontName="Helvetica"
      		fontSize="10" />
      		
      	<paraStyle name="vermell"
      		textColor="darkred"
      		fontName="Helvetica-Bold"
      		fontSize="10" />
      		
      	<paraStyle name="blau"
      		textColor="darkblue"
      		fontName="Helvetica-Bold"
      		fontSize="10"
      		leftIndent="0.5cm" />
      		
      	<blockTableStyle id="taula">
          <blockFont name="Helvetica" size="10" />
        </blockTableStyle>
      		
      	<blockTableStyle id="taula_preus">
          <blockFont name="Helvetica" size="10" />
          <lineStyle kind="GRID" start="0,0" stop="-1,0" colorName="black" />
          <blockBackground colorName="silver" start="0,0" stop="-1,0" />
          <lineStyle kind="BOX" start="0,1" stop="-1,-1" colorName="black"/>
          <lineStyle kind="LINEAFTER" start="0,1" stop="-1,-1" colorName="black"/>
        </blockTableStyle>
        
        <blockTableStyle id="cap">
        	<blockFont name="Helvetica-Bold" size="10" />
        	<lineStyle kind="LINEBELOW" start="0,0" stop="-1,0" colorName="black" />
        </blockTableStyle>

      		
      </stylesheet>
    
      <story>
        <spacer length="20" />
        <blockTable style="cap" colWidths="5cm,14cm">
        	<tr>
        		<td><para style="text"><b>Sol�licitud</b></para></td>
        		<td></td>
        	</tr>
        	<tr>
        		<td><para style="text">N�mero:</para></td>
        		<td><para style="text"><xsl:value-of select="name" /></para></td>
        	</tr>
        	<tr>
        		<td><para style="text">Assumpte:</para></td>
        		<td><para style="text"><xsl:value-of select="assumpte" /></para></td>
        	</tr>
        	<tr>
        		<td><para style="text">Autor:</para></td>
        		<td><para style="text"><xsl:value-of select="autor" /></para></td>
        	</tr>
        </blockTable>
        <spacer length="15" />
        <blockTable style="cap" colWidths="5cm,14cm">
        	<tr>
        		<td>Dades del Client</td>
        		<td></td>
        	</tr>
        	<tr>
        		<td><para style="text">Nom:</para></td>
        		<td><para style="text"><xsl:value-of select="partner/name" /></para></td>
        	</tr>
        	<tr>
        		<td><para style="text">N.I.F.:</para></td>
        		<td><para style="text"><xsl:value-of select="partner/nif" /></para></td>
        	</tr>
        	<tr>
        		<td><para style="text">Tel�fon:</para></td>
        		<td><para style="text"><xsl:value-of select="partner_address/phone" /></para></td>
        	</tr>
        </blockTable>
        <spacer length="15" />
        <blockTable style="cap" colWidths="5cm,14cm">
        	<tr>
        		<td>Adre�a de subministrament</td>
        		<td></td>
        	</tr>
        	<tr>
        		<td><para style="text">Adre�a:</para></td>
        		<td><para style="text"><xsl:value-of select="partner_address/adreca" /></para></td>
        	</tr>
        	<tr>
        		<td><para style="text">C.P.:</para></td>
        		<td><para style="text"><xsl:value-of select="partner_address/cp" /></para></td>
        	</tr>
        	<tr>
        		<td><para style="text">Municipi:</para></td>
        		<td><para style="text"><xsl:value-of select="partner_address/municipi" /></para></td>
        	</tr>
        </blockTable>
        <spacer length="15" />
        <blockTable style="cap" colWidths="5cm,14cm">
        	<tr>
        		<td>Dades del contacte</td>
        		<td></td>
        	</tr>
        	<tr>
        		<td><para style="text">Nom:</para></td>
        		<td><para style="text"><xsl:value-of select="contacte/nom" /></para></td>
        	</tr>
        	<tr>
        		<td><para style="text">Tel�fons:</para></td>
        		<td><para style="text"><xsl:value-of select="concat(contacte/telf,'  ',contacte/mov)" /></para></td>
        	</tr>
        	<tr>
        		<td><para style="text">E-mail:</para></td>
        		<td><para style="text"><xsl:value-of select="contacte/email" /></para></td>
        	</tr>
        </blockTable>
        <spacer length="15" />
        <blockTable style="cap" colWidths="5cm,14cm">
        	<tr>
        		<td><para style="text"><b>Dades de la petici�</b></para></td>
        		<td></td>
        	</tr>
        	<tr>
        		<td><para style="text">Pot�ncia:</para></td>
        		<td><para style="text"><xsl:value-of select="potencia" /></para></td>
        	</tr>
        	<tr>
        		<td><para style="text">Tensi�:</para></td>
        		<td><para style="text"><xsl:value-of select="tensio" /></para></td>
        	</tr>
        	<tr>
        		<td><para style="text">Quota d'acc�s:</para></td>
        		<td><para style="text"><xsl:value-of select="quota" /></para></td>
        	</tr>
        	<tr>
        		<td><para style="text">Taxa:</para></td>
        		<td><para style="text"><xsl:value-of select="taxa" /></para></td>
        	</tr>
        	<tr>
        		<td><para style="text">Condicions de pagament:</para></td>
        		<td><para style="text"><xsl:value-of select="cond_pagament" /></para></td>
        	</tr>
        	<tr>
        		<td><para style="text">Text Final:</para></td>
        		<td><para style="text"><xsl:value-of select="text_final" /></para></td>
        	</tr>
        </blockTable>
        <spacer length="15" />
        <blockTable style="cap" colWidths="19cm">
        	<tr>
        		<td>Observacions</td>
        	</tr>
        	<tr>
        		<td><para style="text"><xsl:value-of select="observacions" /></para></td>
        	</tr>
        </blockTable>
      </story>
    </document>
  </xsl:template>
  
</xsl:stylesheet>
