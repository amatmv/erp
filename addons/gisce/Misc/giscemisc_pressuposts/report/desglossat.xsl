<?xml version="1.0" encoding="iso-8859-15"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

  <xsl:key name="capitols-per-ot" match="capitol" use="ot" />

  <xsl:template match="/">
    <xsl:apply-templates select="pressuposts"/>
  </xsl:template>

  <xsl:template match="pressuposts">
    <xsl:apply-templates select="pressupost" />
  </xsl:template>

  <xsl:template match="pressupost">
    <document debug="1" compression="1">
      <template pageSize="(21cm, 29.7cm)">
        <pageTemplate id="main">
          <pageGraphics>
          	<image file="addons/crm_extended/report/logo.png" width="20mm" height="20mm" x="1cm" y="26.7cm" />
          	<setFont name="Helvetica-Bold" size="30" />
          	<drawString x="3cm" y="27.5cm">Bassols</drawString>
          	<setFont name="Helvetica" size="10" />
          	<drawString x="3cm" y="27.1cm">DISTRIBUCI� D'ENERGIA EL�CTRICA</drawString>
          	<lineMode width="2" />
          	<stroke color="lightgreen"/>
          	<lines>
          		3cm 27cm 20cm 27cm
          	</lines>
          	<setFont name="Helvetica" size="8" />
          	<drawString x="3cm" y="26.6cm">BASSOLS ENERGIA, S.A</drawString>
          	<drawRightString x="20cm" y="26.7cm">Avinguda de Girona, 2</drawRightString>
          	<drawRightString x="20cm" y="26.4cm">Tel. 972 26 01 50 - Fax 972 26 98 99</drawRightString>
          	<drawRightString x="20cm" y="26.1cm">17800 OLOT (Girona)</drawRightString>
          	<drawRightString x="20cm" y="25.8cm">E-mail: bassols@bassols-energia.es</drawRightString>
          </pageGraphics>
          <frame id="first" x1="1cm" y1="1cm" width="19cm" height="247mm"/>
        </pageTemplate>
      </template>
      
      <stylesheet> 
      	<paraStyle name="text"
      		fontName="Helvetica"
      		fontSize="10" />
      		
      	<paraStyle name="vermell"
      		textColor="darkred"
      		fontName="Helvetica-Bold"
      		fontSize="10" />
      		
      	<paraStyle name="blau"
      		textColor="darkblue"
      		fontName="Helvetica-Bold"
      		fontSize="10"
      		leftIndent="0.5cm" />
      		
      	<blockTableStyle id="taula">
          <blockFont name="Helvetica" size="10" />
          <lineStyle kind="GRID" colorName="silver" start="0,1"/>
          <blockBackground colorName="grey" start="0,1" stop="3,1" />
          <blockFont name="Helvetica-Bold" size="10" start="0,1" stop="3,1"/>
        </blockTableStyle>

      		
      </stylesheet>
    
      <story>
      	<para style="text">Pressupost: <xsl:value-of select="name" /></para>
      	<para style="text"><b><xsl:value-of select="assumpte" /></b></para>
      	<!-- Hem de comen�ar agrupant per ordres de treball -->
      	<xsl:for-each select="capitols/capitol[count(. | key('capitols-per-ot', ot)[1]) = 1]">
        	<xsl:sort select="ot" />
        	<spacer length="20" />
        	<blockTable style="taula" colWidths="2.5cm,12.5cm,2.5cm,1.5cm" repeatRows="2">
        		<tr>
        			<td><para style="text"><b><xsl:value-of select="ot" /></b></para></td>
        			<td></td>
        			<td></td>
        			<td></td>
        		</tr>
        		<tr>
        			<td>Codi</td>
        			<td>Material</td>
        			<td>Quantitat</td>
        			<td></td>
        		</tr>
        		<xsl:for-each select="key('capitols-per-ot', ot)">
        			<xsl:apply-templates match="olines" mode="story" />
        		</xsl:for-each>
        	</blockTable>
        </xsl:for-each>
        
      </story>
    </document>
  </xsl:template>
  
  <xsl:template match="olines" mode="story">
  	<xsl:apply-templates match="oline" mode="story" />
  </xsl:template>
  
  <xsl:template match="oline" mode="story">
 	<xsl:if test="type='make_to_stock'">
  		<tr>
  			<td><para style="text"><xsl:value-of select="codi" /></para></td>
  			<td><para style="text"><xsl:value-of select="producte" /></para></td>
  			<td><para style="text" alignment="right"><xsl:value-of select="format-number(quantitat, '###,###.00')" /></para></td>
  			<td><para style="text"><xsl:value-of select="uom" /></para></td>
  		</tr>
  	</xsl:if>
  	<xsl:if test="type='make_to_order' and tprod='product' and count(desglossat/material)&gt;0">
  		<xsl:apply-templates match="desglossat/material" mode="story" />
  	</xsl:if>
  </xsl:template>
  
  <xsl:template match="desglossat/material" mode="story">
  	<tr>
  		<td><para style="text"><xsl:value-of select="code" /></para></td>
  		<td><para style="text"><xsl:value-of select="name" /></para></td>
  		<td><para style="text" alignment="right"><xsl:value-of select="concat(format-number(qty * ../../quantitat, '###,###.00'), ' ')" /></para></td>
  		<td><para style="text"><xsl:value-of select="uom" /></para></td>
  	</tr>
  </xsl:template>

</xsl:stylesheet>
