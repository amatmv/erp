from report import print_fnc

def _data_string(arg):
  import locale
  import time
  print arg
  locale.setlocale(locale.LC_TIME, 'ca_ES.UTF-8')
  s = time.strftime('a %d de %B de %Y')
  u = unicode( s, "utf-8" )
  return u

print_fnc.functions['data_string'] = _data_string
