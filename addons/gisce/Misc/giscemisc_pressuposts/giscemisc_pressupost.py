# -*- coding: utf-8 -*-
from osv import osv,fields
import pooler
import ir
import tools
import base64

class sale_order(osv.osv):
    _name = 'sale.order'
    _inherit = 'sale.order'

    def _order_line_capitols(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for id in ids:
            res[id] = self.pool.get('sale.order.line').search(cr, uid,\
              [('order_id', '=', id)])
        return res

    _columns = {
                  'name': fields.char('Order Description', size=64, required=True, select=True,\
        readonly=True, states={'borrador': [('readonly', False)]}),
      'sollicitud': fields.many2one('giscemisc.pressuposts.sollicitud', 'Sol·licitud',\
        readonly=True, states={'borrador': [('readonly', False)], 'sense desar': [('readonly', False)]}),
      'assumpte': fields.char('Assumpte', size=256, readonly=True,\
        states={'borrador': [('readonly', False)], 'sense desar': [('readonly', False)]}),
      'informe': fields.text('Informe tècnic', readonly=True,\
        states={'borrador': [('readonly', False)]}),
      'capitols': fields.one2many('giscemisc.pressuposts.capitol', 'pressupost', 'Capitols',\
        readonly=True, states={'borrador': [('readonly', False)], 'verificacio': [('readonly', False)], 'assignar ot': [('readonly', False)]}),
                  'pricelist_id':fields.many2one('product.pricelist', 'Pricelist', required=True, readonly=True, states={'borrador':[('readonly',False)], 'sense desar': [('readonly', False)]}),
      'order_line': fields.function(_order_line_capitols, type='one2many', obj='sale.order.line',\
        string='Order lines', method=True),
      'date_order':fields.date('Date Ordered', required=True,\
        readonly=True, states={'borrador':[('readonly',False)]}),
      'partner_id':fields.many2one('res.partner', 'Partner',\
        readonly=True, states={'borrador':[('readonly',False)], 'sense desar': [('readonly', False)]}, change_default=True, select=True),
      'partner_order_id':fields.many2one('res.partner.address',\
        'Ordering Contact', readonly=True, required=True,\
        states={'borrador':[('readonly',False)], 'sense desar': [('readonly', False)]},\
        help="The name and address of the contact that requested the order or quotation."),
      'order_policy': fields.selection([
          ('prepaid','Invoice before delivery'),
          ('manual','Shipping & Manual Invoice'),
          ('postpaid','Automatic Invoice after delivery'),
          ('picking','Invoice from the packings'),
        ], 'Shipping Policy', required=True, readonly=True,\
        states={'borrador':[('readonly',False)], 'sense desar': [('readonly', False)]},\
        help="""The Shipping Policy is used to synchronise invoice and delivery operations.
    - The 'Pay before delivery' choice will first generate the invoice and then generate the packing order after the payment of this invoice.
    - The 'Shipping & Manual Invoice' will create the packing order directly and wait for the user to manually click on the 'Invoice' button to generate the draft invoice.
    - The 'Invoice after delivery' choice will generate the draft invoice after the packing list have been finished.
    - The 'Invoice from the packings' choice is used to create an invoice during the packing process."""),
      'state': fields.char('State', size=16, readonly=True),
      'obs_client': fields.text('Observacions pel client'),
    }

    _defaults = {
      'state': lambda *a: 'sense desar',
    }

    def partner_change(self, cr, uid, ids, part, pl):
        dict = self.onchange_partner_id(cr, uid, ids, part)
        if pl and part:
            del dict['value']['pricelist_id']
        return dict

    def sollicitud_change(self, cr, uid, ids, sol, assumpte):
        value = {}
        if sol:
            sol = self.pool.get('giscemisc.pressuposts.sollicitud').browse(cr, uid, sol)
            addrs = sol.partner.address_get(cr, uid, [sol.partner.id], ['delivery','invoice'])
            value['name'] = '%s/%i' % (sol.name, len(sol.pressuposts) + 1)
            value['partner_id'] = sol.partner.id
            value['partner_order_id'] = sol.contacte.id
            value['partner_invoice_id'] = addrs['invoice']
            value['partner_shipping_id'] = addrs['delivery']
            # Actualitzem els productes que ja teniem el pressupost a l'IVA de la solÂ·licitud
            for order in self.browse(cr, uid, ids):
                for l in order.order_line:
                    if sol.taxa:
                        cr.execute('update sale_order_tax set tax_id = %i where order_line_id = %i',\
                          (sol.taxa.id, l.id))
                        cr.commit()
            if not assumpte:
                value['assumpte'] = sol.assumpte
        else:
            value['name'] = self.pool.get('ir.sequence').get(cr, uid, 'sale.order')
            value['partner_id'] = False
            value['partner_order_id'] = False
            value['partner_invoice_id'] = False
            value['partner_shipping_id'] = False
        return {'value': value}

    def pricelist_change(self, cr, uid, ids, pricelist):
        if pricelist:
            for sale in self.browse(cr, uid, ids):
                for line in sale.order_line:
                    if line.product_id:
                        price = self.pool.get('product.pricelist').price_get(cr, uid, [pricelist],
                                                line.product_id.id, line.product_uom_qty or 1.0, sale.partner_id.id, {
                                                      'uom': line.product_uom.id,
                                                    'date': sale.date_order,
                                                    })[pricelist]
                        line.write(cr, uid, [line.id], {'price_unit': price})
        return {}

    def write(self, cr, uid, ids, vals, context={}):
        for p in self.browse(cr, uid, ids):
            if vals.has_key('state'):
                print '  => Comprovant si hi ha una regle per %s a %s' % (p.state, vals['state'])
                avisos = self.pool.get('giscemisc.pressuposts.mail').search(cr, uid,\
                  [('estat1', '=', p.state), ('estat2', '=', vals['state']),\
                   ('active', '=', True)])
                if len(avisos):
                    for avis in self.pool.get('giscemisc.pressuposts.mail').browse(cr, uid, avisos):
                        remitent = self.pool.get('res.users').browse(cr, uid, uid)
                        print '    => TROBAT! (%s) <=' % (avis.name)
                        emails_to = []
                        email_from = "%s <%s>" % (remitent.name, remitent.address_id.email)
                        for user in avis.usuaris:
                            emails_to.append("%s <%s>" % (user.name, user.address_id.email))
                        data = {
                          'n_pressupost': p.name or '',
                          'assumpte': p.assumpte or '',
                          'obra': p.origin or '',
                        }
                        subject = avis.assumpte % data
                        body = avis.text % data
                        if remitent.signature:
                            body += '\n\n'+remitent.signature
                        if avis.adjunts:
                            attach_ids = self.pool.get('ir.attachment').search(cr, uid,
                              [('res_model', '=', 'sale.order'),
                                                                              ('res_id', '=', p.id)])
                            res = self.pool.get('ir.attachment').read(cr, uid,
                              attach_ids, ['datas_fname','datas'])
                            res = map(lambda x: (x['datas_fname'],
                              base64.decodestring(x['datas'])), res)
                            tools.email_send_attach(email_from, emails_to, subject, body, attach=res)
                        else:
                            tools.email_send(email_from, emails_to, subject, body)
        return super(osv.osv, self).write(cr, uid, ids, vals, context)

    def create(self, cr, uid, vals, context={}):
        # La botiga l'hem d'agafar dels defaults: Important configurar
        order_defaults = ir.ir_get(cr, uid, 'default', False, ['sale.order'])
        for default in order_defaults:
            if not vals.has_key(default[1]):
                vals[default[1]] = default[2]
        # Si hi ha una solÂ·licitud agafem les dades d'aquesta
        if vals['sollicitud']:
            sol = self.pool.get('giscemisc.pressuposts.sollicitud').browse(\
              cr, uid, vals['sollicitud'])
            addrs = sol.partner.address_get(cr, uid, [sol.partner.id], ['delivery','invoice'])
            vals['partner_invoice_id'] = addrs['invoice']
            vals['partner_shipping_id'] = addrs['delivery']
        # Si no, haurem d'agafar les dades del partner
        else:
            partner = self.pool.get('res.partner').browse(\
              cr, uid, vals['partner_id'])
            addrs = partner.address_get(cr, uid, [partner.id], ['delivery','invoice'])
            vals['partner_invoice_id'] = addrs['invoice']
            vals['partner_shipping_id'] = addrs['delivery']
        # Cridem el pare, amb els valors que posem nosaltres
        return super(osv.osv, self).create(cr, uid, vals, context)

    def is_extern(self, cr, uid, ids, *args):
        for p in self.browse(cr, uid, ids):
            if p.sollicitud:
                return True
            else:
                return False

    def copy(self, cr, uid, id, default=None, context={}):
        if not default:
            default = {}
        default.update({
          'order_line': [],
          'capitols': [],
          'sollicitud': None,
        })
        id_copy = super(sale_order, self).copy(cr, uid, id, default, context)
        for capitol in self.browse(cr, uid, id).capitols:
            c_id = capitol.copy(cr, uid, capitol.id, {'pressupost': id_copy, 'order_lines': [], 'compte_analitic': None})
            for item in capitol.order_lines:
                i_id = item.copy(cr, uid, item.id, {'chapter': c_id, 'invoiced': False, 'order_id': id_copy})
        return id_copy

    def buscar_antics(self, cr, uid, user_id=3):
        print '* Buscant pressuposts antics...'
        mail_body = 'PRESSUPOSTOS\n\n'
        cr.execute("SELECT assumpte,name,date_order from sale_order where date_order < NOW() - INTERVAL '6 month' and state = 'per acceptar'")
        pressuposts = cr.dictfetchall()
        for p in pressuposts:
            mail_body += '* %s - %s - %s\n' % (p['assumpte'], p['name'], p['date_order'])

        user = self.pool.get('res.users').browse(cr, uid, user_id)

        if len(pressuposts) and user.address_id and user.address_id.email:
            # Si hi ha pressuposts antics enviem el correu a l'usuari responsable del
            # cron
            email_from = 'GISCE ERP <noreply@bassols-energia.es>'
            emails_to = []
            emails_to.append('%s <%s>' % (user.name, user.address_id.email))
            subject = 'RepÃ s de pressuposts antics'
            tools.email_send(email_from, emails_to, subject, mail_body)
            print '  => Enviant email a %s' % (user.address_id.email)






sale_order()

class giscemisc_pressuposts_capitol(osv.osv):

    _name = 'giscemisc.pressuposts.capitol'

    def update_preu(self, cr, uid, ids, ols):
        value = {}
        value['preu'] = 0.0
        print ids
        for ol in ols:
            print ol
            value['preu'] += ol[2]['price_unit'] * ol[2]['product_uom_qty']
        return {'value': value}

    def _preu(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for capitol in self.browse(cr, uid, ids):
            preu = 0.0
            for o in capitol.order_lines:
                preu += o.price_subtotal
            res[capitol.id] = preu
        return res

    def change_chapter_name(self, cr, uid, ids, pressu=False):
        if not pressu:
            raise osv.except_osv(
                                                      'No es pot afegir el capÃ­tol',
                                                      'S\'ha de guardar el pressupost abans d\'afegir els capÃ­tols.')
        return {}

    _columns = {
      'name': fields.char('Nom', size=60),
      'compte_analitic': fields.many2one('account.analytic.account', 'Compte analÃ­tic'),
      'pressupost': fields.many2one('sale.order', 'Pressupost'),
      'order_lines': fields.one2many('sale.order.line', 'chapter', 'Sales order lines'),
      'preu': fields.function(_preu, type='float', string='Preu', method=True),
      'sequence': fields.integer('Ordre'),
    }

    _order = 'sequence'

    _defaults = {
      'sequence': lambda *a: 10,
    }

giscemisc_pressuposts_capitol()

class sale_order_line(osv.osv):

    _name = 'sale.order.line'
    _inherit = 'sale.order.line'

    def copy(self, cr, uid, id, default=None, context={}):
        if not default:
            default = {}
        default.update({
          'escandall':[]
        })
        return super(sale_order_line, self).copy(cr, uid, id, default, context)


    def create(self, cr, uid, vals, context={}):
        capitol = self.pool.get('giscemisc.pressuposts.capitol').browse(\
          cr, uid, vals['chapter'])
        vals['order_id'] = capitol.pressupost.id
        return super(osv.osv, self).create(cr, uid, vals, context)

    def product_change(self, cr, uid, ids, prod, qty, pressupost, lang=False):
        # CapÃ§alera del change (parent)
        #  product_id_change(pricelist, product, qty=0, uom=False, qty_uos=0,
        #                    uos=False, name='', partner_id=False,
                  #                    lang=False, update_tax=True, date_order=False)
        if not pressupost:
            raise osv.except_osv('No hi ha cap pressupost definit',\
              "S'ha de desar el pressupost actual i el capÃ­tol abans d'introduÃ¯r productes")
        order = self.pool.get('sale.order').browse(cr, uid, pressupost)

        res = self.product_id_change(cr, uid, ids,\
          pricelist=order.pricelist_id.id, product=prod, qty=qty,\
          partner_id=order.partner_id.id,date_order=order.date_order,lang=lang)

        bom_obj = self.pool.get('mrp.bom')
        value = {}
        if prod:
            bom = bom_obj.search(cr, uid, [('product_id', '=', prod)])
            if bom and len(bom):
                value['escandall'] = []
                for p in bom_obj.browse(cr, uid, bom)[0].bom_lines:
                    value['escandall'].append(p.id)
            else:
                value['escandall'] = False
        else:
            value['escandall'] = False
        res['value'].update(value)
        # Si estem a l'estat de verificaciÃ³ hem de posar el preu = 0
        if order.state == 'verificacio':
            res['value']['price_unit'] = 0.0
        # Agafem l'IVA de la solÂ·licitud si n'hi ha.
        if order.sollicitud and order.sollicitud.taxa:
            res['value']['tax_id'] = [order.sollicitud.taxa.id]
        return {'value': res['value'], 'domain': res['domain']}


    def _escandall(self, cr, uid, ids, field_name, arg, context):
        res = {}
        bom_obj = self.pool.get('mrp.bom')
        for ol in self.browse(cr, uid, ids):
            if ol.product_id:
                value = self.product_change(cr, uid, ids, ol.product_id.id, ol.product_uom_qty, ol.order_id.id)
                if value['value']['escandall']:
                    res[ol.id] = value['value']['escandall']
                else:
                    res[ol.id] = []
            else:
                res[ol.id] = []
        return res

    _columns = {
      'chapter': fields.many2one('giscemisc.pressuposts.capitol', 'Chapter', ondelete='cascade'),
      'escandall': fields.function(_escandall, type='one2many', obj='mrp.bom', string='Desglossat', method=True),
    }

sale_order_line()
