# Edit with tabular editor

from osv import fields, osv, orm

class mrp_procurement(osv.osv):
    _name = "mrp.procurement"
    _inherit = "mrp.procurement"

    def action_produce_assign_service(self, cr, uid, ids, context={}):
        for procurement in self.browse(cr, uid, ids):
            # Firstly find if the project for this sale exists
            # if not create a project for this sale.order
            project_ids = self.pool.get('project.project').search(cr, uid, [
                    ('name', 'like', procurement.origin)
            ])
            if not len(project_ids):
                project_id = self.pool.get('project.project').create(cr, uid, {
                        'name': procurement.origin,
                        'state': 'pending'
                })
            else:
                project_id = project_ids[0]

            # Check if this service have a BoM (bom._find)
            bom = self.pool.get('mrp.bom').search(cr, uid, [
                    ('product_id', '=', procurement.product_id.id)
            ])
            if bom and len(bom):
                # Have a BoM
                # create a subproject for this BoM
                subproject_id = self.pool.get('project.project').create(cr, uid, {
                        'name': procurement.product_id.name,
                        'parent_id': project_id,
                        'state': 'pending',
                })
                properties = [x.id for x in procurement.property_ids]
                bom_id = self.pool.get('mrp.bom')._bom_find(cr, uid, procurement.product_id.id,\
                        procurement.product_uom.id, properties)
                for p in self.pool.get('mrp.bom').browse(cr, uid, bom_id).bom_lines:
                    task_id = self.pool.get('project.task').create(cr, uid, {
                            'name': p.product_id.name,
                            'date_deadline': procurement.date_planned,
                            'state': 'draft',
                            'planned_hours': procurement.product_qty,
                            'user_id': p.product_id.product_manager.id,
                            'notes': procurement.origin,
                            #'procurement_id': procurement.id,
                            'project_id': subproject_id
                    })
            else:
                # Simply create the task in the project
                task_id = self.pool.get('project.task').create(cr, uid, {
                        'name': procurement.product_id.name,
                        'date_deadline': procurement.date_planned,
                        'state': 'draft',
                        'planned_hours': procurement.product_qty,
                        'user_id': procurement.product_id.product_manager.id,
                        'notes': procurement.origin,
                        #'procurement_id': procurement.id,
                        'project_id': project_id
                })
            self.write(cr, uid, [procurement.id], {'state':'running'})
        return project_id
mrp_procurement()
