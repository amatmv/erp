# -*- coding: utf-8 -*-
{
    "name": "Pressuposts",
    "description": """Creació de pressuposts""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Pressuposts",
    "depends":[
        "base",
        "sale",
        "product"
    ],
    "init_xml":[
        "giscemisc_pressupost_data.xml",
        "giscemisc_sollicitud_sequence.xml",
        "giscemisc_pressupost_sequence.xml"
    ],
    "demo_xml": [],
    "update_xml":[
        "giscemisc_sollicitud_view.xml",
        "giscemisc_pressupost_wizard.xml",
        "giscemisc_pressupost_workflow.xml",
        "giscemisc_pressupost_report.xml",
        "giscemisc_pressupost_view.xml",
        "giscemisc_pressupost_mails_view.xml",
        "giscemisc_sollicitud_report.xml"
    ],
    "active": False,
    "installable": True
}
