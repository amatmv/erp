# -*- coding: utf-8 -*-
from osv import osv,fields

class giscemisc_pressuposts_mail(osv.osv):
  _name = 'giscemisc.pressuposts.mail'

  _columns = {
    'name': fields.char('Nom', size=60, required=True),
    'estat1': fields.char('Desde', size=60, required=True),
    'estat2': fields.char('A', size=60, required=True),
    'assumpte': fields.char('Assumpte', size=60, required=True),
    'text': fields.text('Missatge', required=True),
    'usuaris': fields.many2many('res.users', 'giscemisc_pressuposts_avisos',\
      'avis_id', 'user_id', 'Usuaris a avisar'),
    'active': fields.boolean('Actiu'),
    'adjunts': fields.boolean('Enviar fitxers adjuntats'),
  }

giscemisc_pressuposts_mail()

