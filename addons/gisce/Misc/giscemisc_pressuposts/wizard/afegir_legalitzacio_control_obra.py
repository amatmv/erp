# -*- coding: utf-8 -*-

import wizard
import netsvc
import pooler
from osv import osv

def _params(self, cr, uid, data, context={}):
    res = {
      'increment': 0.15,
    }
    return res

_params_form = """<?xml version="1.0"?>
<form string="" col="2">
  <label string="Escull la proporció que vols aplicar (entre 0 i 1)" colspan="4" />
  <field name="increment" required="1"/>
</form>"""

_params_fields = {
  'increment': {'string': 'Proporció', 'type': 'float', 'required': True},
}


def _afegir(self, cr, uid, data, context={}):
    form = data['form']
    pressupost_obj = pooler.get_pool(cr.dbname).get('sale.order')
    line_obj = pooler.get_pool(cr.dbname).get('sale.order.line')
    product_obj = pooler.get_pool(cr.dbname).get('product.product')
    pid = product_obj.search(cr, uid, [('default_code', '=', 'LCO_0000')])
    if not len(pid):
        raise osv.except_osv('Error !', 'No existeix el producte de Legalització i control d\'obra (LCO_0000)')
    product = product_obj.browse(cr, uid, pid[0])
    p = pressupost_obj.browse(cr, uid, data['id'], context)
    # Fem dues vegades el bucle, una per si te la linia LOC eliminar-la
    for l in p.order_line:
        if l.product_id.id == product.id:
            line_obj.unlink(cr, uid, [l.id])

    # Busquem el total del pressupost
    total = p.amount_untaxed
    # Calculem la proporció
    proporcio = total*form['increment']
    # Mirem quants capítols hi ha associats per repartir-ho entre ells
    n_capitols = len(p.capitols)
    # Calculem el que hem d'afegir a cada capítol
    afegir = proporcio / n_capitols
    vals = {
      'order_id': p.id,
      'name': product.name,
      'product_id': product.id,
      'product_uom_qty': 1,
      'price_unit': afegir,
      'chapter': None,
      'product_uom': product.uom_id.id
    }

    # i un altre per afegir-ho
    for c in p.capitols:
        vals['chapter'] = c.id
        line_obj.create(cr, uid, vals)

    return {}

def _continuar(self, cr, uid, data, context={}):
    id = data['id']
    wf_service = netsvc.LocalService("workflow")
    wf_service.trg_validate(uid, 'sale.order', id, 'per_aprovar', cr)
    return {}


class afegir_legalitzacio_control_obra(wizard.interface):

    states = {
      'init': {
        'actions': [],
        'result': {'type': 'state', 'state': 'params'}
      },
      'params': {
        'actions': [_params],
        'result': {'type': 'form', 'arch': _params_form, 'fields': _params_fields, 'state': [('end', 'Cancel·lar'), ('afegir', 'Afegir'), ('continuar', 'Continuar sense afegir')]}
      },
      'afegir': {
        'actions': [_afegir],
        'result': {'type': 'state', 'state': 'continuar'},
      },
      'continuar': {
        'actions': [_continuar],
        'result': {'type': 'state', 'state': 'end'}
      },
      'end': {
        'actions': [],
        'result': {'type': 'state', 'state': 'end'}
      }
    }


afegir_legalitzacio_control_obra('giscemisc.pressupost.afegir_lco')
