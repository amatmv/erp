# -*- coding: utf-8 -*-
import wizard
import pooler

def _init(self, cr, uid, data, context={}):
    return {}

_init_form = """<?xml version="1.0"?>
<form string="Escollir un capítol" col="2">
  <label string="Escull un capítol per afegir en el pressupost actual." colspan="2" />
  <field name="capitol" required="1"/>
</form>"""

_init_fields = {
  'capitol': {'string': "Capítol", 'type': 'many2one', 'relation':'giscemisc.pressuposts.capitol'},
}


def _copiar(self, cr, uid, data, context={}):
    print '* Copiant el capítol:  *'
    capitol_obj = pooler.get_pool(cr.dbname).get('giscemisc.pressuposts.capitol')
    defaults = {'pressupost': data['id'], 'order_lines': []}
    id_c = capitol_obj.copy(cr, uid, data['form']['capitol'], defaults)
    for item in capitol_obj.browse(cr, uid, data['form']['capitol']).order_lines:
        item.copy(cr, uid, item.id, {'chapter': id_c})
    return {}

class wizard_giscemisc_pressuposts_copiar_capitol(wizard.interface):

    states = {
      'init': {
        'actions': [_init],
        'result': {
          'type': 'form',
          'arch': _init_form,
          'fields': _init_fields,
          'state':[
            ('end', 'Cancel·lar', 'gtk-cancel'),
            ('copiar', 'Afegir', 'gtk-add')
          ]
        }
      },
      'copiar': {
        'actions': [_copiar],
        'result': {
          'type': 'state',
          'state': 'end',
        }
      },
      'end': {
        'actions': [],
        'result': {
          'type':'state',
          'state': 'end'
        }
      },
    }

wizard_giscemisc_pressuposts_copiar_capitol('giscemisc.pressupost.copiar_capitol')
