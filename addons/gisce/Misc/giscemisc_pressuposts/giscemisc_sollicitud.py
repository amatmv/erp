# -*- coding: utf-8 -*-
from osv import osv,fields
import time
import tools

class giscemisc_pressuposts_textemail(osv.osv):
    _name = 'giscemisc.pressuposts.textemail'

    _columns = {
      'name': fields.char('Descripció', size=60),
      'assumpte': fields.char('Assumpte', size=60),
      'text': fields.text('Text del correu'),
    }
giscemisc_pressuposts_textemail()

class giscemisc_pressuposts_textfinal(osv.osv):
    _name = 'giscemisc.pressuposts.textfinal'

    _columns = {
      'name': fields.char('Nom', size=60),
      'text': fields.text('Text'),
    }

giscemisc_pressuposts_textfinal()

class giscemisc_pressuposts_quotaacces(osv.osv):
    _name = 'giscemisc.pressuposts.quotaacces'

    _columns = {
      'name': fields.float('Quota d\'accés (€/kW)', digits=(16,6)),
      'v_min': fields.float('Tensió mínima (V)', digits=(16,2)),
      'v_max': fields.float('Tensió màxima (V)', digits=(16,2)),
      'data_inici': fields.date('Data inici'),
      'data_final': fields.date('Data final'),
    }

giscemisc_pressuposts_quotaacces()

class giscemisc_pressposts_tensio(osv.osv):
    _name = 'giscemisc.pressuposts.tensio'

    _columns = {
      'name': fields.float('Tensió (V)'),
    }

giscemisc_pressposts_tensio()

class giscemisc_pressuposts_sollicitud(osv.osv):

    _name = 'giscemisc.pressuposts.sollicitud'
    _description = 'Sol·licituds per pressuposts'

    def _partner_address(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for s in self.browse(cr, uid, ids):
            res[s.id] = []
            if s.partner_address_id:
                res[s.id].append(s.partner_address_id.id)
        return res

    _columns = {
      'name': fields.char('Id. Petició', size=60, states={'per pressupostar':[('readonly', True)]}),
      'partner': fields.many2one('res.partner', 'Nom', required=True),
      'partner_address': fields.function(_partner_address, type='one2many',\
        obj='res.partner.address', string='Adreça', method=True, required="True"),
      'partner_address_id': fields.many2one('res.partner.address', 'Adreça'),
      'contacte': fields.many2one('res.partner.address', 'Nom'),
      'assumpte': fields.char('Assumpte', size=256),
      'user': fields.many2one('res.users', 'Autor'),
      'data': fields.date('Data'),
      'potencia': fields.float('Potència'),
      'tensio': fields.many2one('giscemisc.pressuposts.tensio', 'Tensió (V)'),
      'observacions': fields.text('Observacions'),
      'taxa': fields.many2one('account.tax', 'Taxa'),
      'cond_pagament': fields.many2one('account.payment.term', 'Condicions de pagament'),
      'text_final': fields.many2one('giscemisc.pressuposts.textfinal', 'Texte final'),
      'quota': fields.many2one('giscemisc.pressuposts.quotaacces', 'Quota d\'accés'),
      'state': fields.char('Estat', size=60, readonly=True),
      'pressuposts': fields.one2many('sale.order', 'sollicitud', 'Pressuposts', readonly=True,\
       states={'borrador': [('invisible', True)], 'per pressupostar': [('readonly', False), ('invisible', False)]}),
      'usuaris_avisats': fields.many2many('res.users', 'giscemisc_pressuposts_sollicitud_avisos',\
        'sol_id', 'user_id', 'Usuaris a avisar'),
      'text_email': fields.many2one('giscemisc.pressuposts.textemail', 'Text'),
    }

    _defaults = {
      'name': lambda obj, cr, uid, context:\
        obj.pool.get('ir.sequence').get(cr, uid, 'giscemisc.pressuposts.sollicitud'),
                  'data': lambda *a: time.strftime('%Y-%m-%d'),
      'user': lambda obj, cr, uid, context: uid,
      'state': lambda *a: 'borrador',
    }

    def write(self, cr, uid, ids, vals, context={}):
        for sol in self.browse(cr, uid, ids):
            if vals.has_key('partner_address') and len(vals['partner_address']):
                address = vals['partner_address'][0][2]
                self.pool.get('res.partner.address').write(cr,\
                  uid, [sol.partner_address_id.id], address)
        return super(osv.osv, self).write(cr, uid, ids, vals, context)


    def create(self, cr, uid, vals, context={}):
        if vals['partner_address']:
            address = vals['partner_address'][0][2]
            address['partner_id'] = vals['partner']
            if vals['partner']:
                id = self.pool.get('res.partner.address').create(cr, uid, address)
                vals['partner_address_id'] = id
        return super(osv.osv, self).create(cr, uid, vals, context)

    def copy(self, cr, uid, id, default=None,context={}):
        if not default:
            default = {}
        default.update({
          'partner_address': [],
          'state': 'borrador',
          'pressuposts': [],
          'name': self.pool.get('ir.sequence').get(cr, uid, 'giscemisc.pressuposts.sollicitud'),
          'partner_address': False,
        })
        print "Adress: %s" % self.pool.get('res.partner.address').read(cr, uid, id)
        return super(giscemisc_pressuposts_sollicitud, self).copy(cr, uid, id, default, context)

    def tensio_change(self, cr, uid, ids, data, ten):
        if data and ten:
            tensio = self.pool.get('giscemisc.pressuposts.tensio').read(cr,\
              uid, ten)
            tensio = tensio['name']
            result = self.pool.get('giscemisc.pressuposts.quotaacces').search(cr,\
              uid, [('data_inici', '<=', data), ('data_final', '>=', data),\
              ('v_min', '<', tensio), ('v_max', '>=', tensio)])
            if result and len(result):
                return {'value': {'quota': result[0]}}
            else:
                raise osv.except_osv('No hi ha cap quota d\'acces que concordi amb aquesta tensió',\
                  'Haurà de crear una quota d\'accés per aquesta tensió')
                return {'value': {'quota': False}}
        else:
            return {'value': {'quota': False}}

    def to_borrador(self, cr, uid, ids, *args):
        for sol in self.browse(cr, uid, ids):
            if sol.pressuposts and len(sol.pressuposts):
                raise osv.except_osv('Ja hi ha pressuposts adjuntats.',\
                "No es pot tornar a passar la sol·licitud a borrador degut a que ja hi ha pressuposts creats per aquesta sol·licitud.")
                return False
            else:
                return True

    def per_pressupostar(self, cr, uid, ids, context={}):
        # Enviem els mails a la gent que està avisada i posem l'estat
        # a 'per pressupostar'
        print ids
        for s in self.browse(cr, uid, ids):
            print s.name
            if s.text_email and s.user.address_id and s.user.address_id.email:
                emails_to = []
                email_from = "%s <%s>" % (s.user.name, s.user.address_id.email)
                for user in s.usuaris_avisats:
                    emails_to.append("%s <%s>" % (user.name, user.address_id.email))
                data = {
                  'peticio_id': s.name or '',
                  'assumpte': s.assumpte or '',
                  'client': s.partner.name or '',
                  'data': s.data or '',
                  'municipi_sub': s.partner_address_id.municipi.name or '',
                  'nom_contacte': s.contacte.name or '',
                  'telf_contacte': s.contacte.phone or '',
                  'mobil_contacte': s.contacte.mobile or '',
                  'mail_contacte': s.contacte.email or '',
                  'observacions': s.observacions or '',
                }
                subject = s.text_email.assumpte % data
                body = s.text_email.text % data
                if len(emails_to):
                    tools.email_send(email_from, emails_to, subject, body)
            s.write(cr, uid, ids, {'state': 'per pressupostar'})
        return True


giscemisc_pressuposts_sollicitud()
