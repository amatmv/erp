# *-* coding: utf-8 *-*

from osv import osv, fields

class product_template(osv.osv):

  _name = 'product.template'
  _inherit = 'product.template'

  _columns = {
    'name': fields.char('Name', size=64, required=True, select=True),
  }

product_template()
