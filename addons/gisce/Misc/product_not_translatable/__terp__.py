# -*- coding: utf-8 -*-
{
    "name": "Producte no traduïble",
    "description": """Fer que el producte no sigui traduïble""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Product",
    "depends":[
        "product"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
