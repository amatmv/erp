# -*- coding: utf-8 -*-
{
    "name": "SMS Template",
    "description": """OpenERP SMS Template""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Misc",
    "depends":[
        "base",
        "notifications_sms"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/ir.model.access.csv",
        "sms_template_view.xml"
    ],
    "active": False,
    "installable": True
}
