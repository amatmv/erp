# -*- coding: utf-8 -*-
from osv import osv, fields
import tools
import netsvc
from urllib import quote as quote

LOGGER = netsvc.Logger()

try:
    from mako.template import Template as MakoTemplate
except ImportError:
    LOGGER.notifyChannel(
        "sms_template",
        netsvc.LOG_ERROR,
        "sms_template: mako templates not available, "
        "templating features will not work!"
    )

class SMSTemplate(osv.osv):

    _name = 'sms.template'
    _description = 'SMS Templates'
    _rec_name = 'name'

    def onchange_model_id(self, cr, uid, ids, model_id, context=None):
        mod_name = False
        if model_id:
            mod_name = self.pool.get('ir.model').browse(cr, uid, model_id, context).model
        return {'value': {'model': mod_name}}

    _columns = {
        'name': fields.char('Name', size=256, select=1),
        'code': fields.char(u'Código plantilla', size=256, select=1),
        'model_id': fields.many2one('ir.model', 'Modelo relacionado'),
        'sms_from': fields.char('De', size=512),
        'sms_to': fields.char('Para', size=512),
        'content': fields.char('SMS Text', size=160),
        'user_id': fields.char('Usuario asignado', size=512),
        'partner_address_id': fields.char('Dirección de contacto', size=512),
    }
    def render_template(self, cursor, uid, template, model, res_id, context=None):
        """Render the given template text, replace mako expressions ``${expr}``
           with the result of evaluating these expressions with
           an evaluation context containing:

                * ``user``: browse_record of the current user
                * ``object``: browse_record of the document record this mail is
                              related to
                * ``context``: the context passed to the mail composition wizard

           :param str template: the template text to render
           :param str model: model name of the document record this mail is related to.
           :param int res_id: id of the document record this mail is related to.
        """
        if not template: return u""
        if context is None:
            context = {}
        try:
            template = tools.ustr(template)
            record = None
            if res_id:
                record = self.pool.get(model).browse(
                    cursor, uid, res_id, context=context)
            user = self.pool.get('res.users').browse(
                cursor, uid, uid, context=context)
            result = MakoTemplate(template).render_unicode(
                object=record, user=user, ctx=context, quote=quote,
                format_exceptions=True)
            if result == u'False':
                result = u''
            return result
        except Exception:
            LOGGER.notifyChannel(
                "sms_template",
                netsvc.LOG_ERROR,
                "failed to render mako template value %r" % template
            )
            return u""

    def generate_sms(self, cursor, uid, template_id, res_id, context=None):
        """Generates an sms from the template for given (model, res_id) pair.

           :param template_id: id of the template to render.
           :param res_id: id of the record to use for rendering the template (model
                          is taken from template definition)
           :returns: a dict containing all relevant fields for creating a new
                     mail.message entry, with the addition one additional
                     special key ``attachments`` containing a list of
        """
        template_context = {}
        if context is None:
            context = {}
        else:
            template_context = context.copy()

        if not template_id:
            return {}

        template = self.browse(cursor, uid, template_id, context)
        model = template.model_id.model
        values = {'sms_from': False,
                  'sms_to': False,
                  'content': False,
                  'model': model,
                  'res_id': res_id,
                  'user_id': False,
                  'partner_address_id': False,
                  }
        for field in ['sms_from', 'sms_to', 'content',
                      'user_id', 'partner_address_id']:
            values[field] = self.render_template(
                cursor, uid, getattr(template, field),
                model, res_id, context=template_context
            ) or False
        if values['user_id']:
            values['user_id'] = int(values['user_id'])

        for field in ['user_id', 'partner_address_id']:
            if values[field]:
                values[field] = int(values[field])
        return values

SMSTemplate()
