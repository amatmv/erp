# -*- coding: utf-8 -*-
{
    "name": "XML Reports",
    "description": """
Treu reports amb XML en comptes de PDF. Això en serveix per generar XML de forma fàcil.
  """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Misc",
    "depends":[
        "base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
