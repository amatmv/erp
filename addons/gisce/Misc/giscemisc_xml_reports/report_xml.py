# -*- coding: utf-8 -*-
from report import interface, print_xml
import os
import netsvc
import tools

class report_xml(interface.report_int):

    def __init__(self, name, table, tmpl, xsl=None, header=True):
        super(report_xml, self).__init__(name)
        self.table = table
        self.tmpl = tmpl
        self.xsl = xsl
        self.header = header
        self.bin_datas = {}

    def create(self, cr, uid, ids, datas, context):
        xml = self.create_xml(cr, uid, ids, datas, context)
        import libxml2
        import libxslt
        import urllib
        import release
        styledoc = libxml2.parseDoc(tools.file_open(os.path.join(tools.config['root_path'], self.xsl)).read())
        # hack from report/interface.py to alter the <xsl:import/> href attribute
        xsl_path, tail = os.path.split(self.xsl)
        for child in styledoc.children:
            if child.name == 'import':
                if child.hasProp('href'):
                    imp_file = child.prop('href')
                    if int(release.version[0]) == 4:
                        imp_file = os.path.normpath(os.path.join(xsl_path, imp_file))
                    elif int(release.version[0]) >= 5:
                        _x, imp_file = tools.file_open(imp_file, subdir=xsl_path, pathinfo=True)
                    child.setProp('href', urllib.quote(str(imp_file)))

        style = libxslt.parseStylesheetDoc(styledoc)
        doc = libxml2.parseMemory(xml, len(xml))
        res = style.applyStylesheet(doc, None)
        _xml = style.saveResultToString(res)
        style.freeStylesheet()
        doc.freeDoc()
        return(_xml, 'xml')

    def post_process_xml_data(self, cr, uid, xml, context=None):
        import re
        if not context:
            context={}
        # find the position of the 3rd tag
        # (skip the <?xml ...?> and the "root" tag)
        iter = re.finditer('<[^>]*>', xml)
        i = iter.next()
        i = iter.next()
        pos_xml = i.end()

        doc = print_xml.document(cr, uid, {}, {})
        tmpl_path = os.path.join(tools.config['root_path'], 'addons/custom/corporate_defaults.xml')
        doc.parse(tmpl_path, [uid], 'res.users', context)
        corporate_header = doc.xml_get()
        doc.close()

        # find the position of the tag after the <?xml ...?> tag
        iter = re.finditer('<[^>]*>', corporate_header)
        i = iter.next()
        pos_header = i.end()

        return xml[:pos_xml] + corporate_header[pos_header:] + xml[pos_xml:]



    def create_xml(self, cr, uid, ids, datas, context=None):
        if not context:
            context={}
        doc = print_xml.document(cr, uid, datas, {})
        self.bin_datas = doc.bin_datas
        doc.parse(self.tmpl, ids, self.table, context)
        xml = doc.xml_get()
        doc.close()
        if self.header:
            return self.post_process_xml_data(cr, uid, xml, context)
        else:
            return xml


old_report_register = interface.register_all

def new_register_all(db):
    import release
    opj = os.path.join
    cr = db.cursor()
    cr.execute("SELECT * FROM ir_act_report_xml WHERE auto = True ORDER BY id")
    result = cr.dictfetchall()
    cr.close()
    if release.version[0] == '4':
        svc = netsvc._service
    elif int(release.version[0]) >= 5:
        svc = netsvc.SERVICES
    for r in result:
        name = 'report.%s' % r['report_name']
        if r['report_xsl'] and r['report_xsl'].endswith('.xslt'):
            if netsvc.service_exist(name) and not isinstance(svc[name], report_xml):
                del netsvc.svc[name]
            if not name in svc:
                report_xml(name, r['model'], opj('addons',r['report_xml']), r['report_xsl'] and opj('addons',r['report_xsl']), header=r['header'])

    old_report_register(db)


interface.register_all = new_register_all
