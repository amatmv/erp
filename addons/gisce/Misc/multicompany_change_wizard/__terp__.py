# -*- coding: utf-8 -*-
{
    "name": "Wizard to change working company",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Multi-Company",
    "depends": [],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "multicompany_change_wizard.xml"
    ],
    "active": False,
    "installable": True
}
