# -*- coding: iso-8859-1 -*-
import wizard
import pooler

def _empreses_selection(self, cr, uid, context={}):
    cr.execute("select id,name from res_company")
    return cr.fetchall()

_init_form = """<?xml version="1.0"?>
<form string="Informacio d'entrada">
  <field name="empresa" required="1"/>
</form>"""

_init_fields = {
  'empresa': {'string': 'Empresa', 'type': 'selection', 'selection':_empreses_selection, 'relation': 'res.company'},
}

def _init(self, cr, uid, data, context={}):
  empresa = pooler.get_pool(cr.dbname).get('res.users').browse(cr, uid, uid).company_id.id
  return {'empresa': empresa}


def _guardar(self, cr, uid, data, context={}):
  pooler.get_pool(cr.dbname).get('res.users').write(cr, uid, uid,{'company_id': data['form']['empresa']})
  return {}

class wizard_change_company(wizard.interface):

  states = {
    'init': {
    	'actions': [_init],
      'result': {'type': 'form', 'arch': _init_form,'fields': _init_fields, 'state':[('guardar', 'Continuar', 'gtk-go-forward')]}
    },
    'guardar': {
    	'actions': [_guardar],
    	'result': { 'type' : 'state', 'state' : 'end' },
    },
  }

wizard_change_company('change.company')
