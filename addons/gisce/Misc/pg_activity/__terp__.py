# -*- coding: utf-8 -*-
{
    "name": "pg_activity",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * PostgreSQL server activity monitoring
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "pg_activity_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
