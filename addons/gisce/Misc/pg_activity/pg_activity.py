# coding=utf-8
from collections import namedtuple

from osv import osv, fields
from tools import config
from tools.translate import _

from pgactivity.Data import Data


Activity = namedtuple('Activity', [
    'id', 'database', 'client', 'duration', 'wait', 'user', 'query'
])

Waiting = namedtuple('Waiting', [
    'id', 'database', 'user', 'mode', 'type', 'relation', 'duration', 'query'
])

Blocking = namedtuple('Blocking', [
    'id', 'database', 'user', 'relation', 'mode', 'type', 'duration', 'query'
])


def remove_none(lst):
    for idx, x in enumerate(lst):
        if x is None:
            lst[idx] = False


class DataManager(object):
    def __init__(self, *args, **kwargs):
        self.data = Data()
        self.data.pg_connect(*args, **kwargs)
        self.data.pg_get_num_version(self.data.pg_get_version())

    def __enter__(self):
        return self.data

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.data.pg_conn.close()


def get_db_params():
    params = {
        'host': config['db_host'],
        'port': config['db_port'],
        'user': config['db_user'],
        'password': config['db_password'],
        'database': config['db_name']
    }
    return dict((k, v) for k, v in params.items() if v)


class PgActivityBase(osv.osv):
    _name = 'pg.activity.base'
    _auto = False

    def button_terminate(self, cursor, uid, ids, context=None):
        with DataManager(*get_db_params()) as d:
            for pid in ids:
                d.pg_terminate_backend(pid)
        return True

    def create(self, cursor, uid, vals, context=None):
        raise osv.except_osv(
            _('Error'),
            _('You cannot create')
        )

    def unlink(self, *args, **argv):
        raise osv.except_osv(
            _('Error'),
            _('You cannot unlink')
        )

    def write(self, cursor, uid, ids, vals, context=None):
        raise osv.except_osv(
            _('Error'),
            _('You cannot write')
        )

    def search(self, cursor, uid, args, offset=0, limit=None, order=None,
               context=None, count=False):
        return [False]

PgActivityBase()


class PgActivityActivity(osv.osv):
    _name = 'pg.activity.activity'
    _inherit = 'pg.activity.base'
    _auto = False

    def read(self, cursor, uid, ids, fields=None, context=None):
        res = []
        with DataManager(**get_db_params()) as d:
            for act in d.pg_get_activities():
                remove_none(act)
                res.append(dict(Activity(*act)._asdict()))
        return res

    _columns = {
        'id': fields.integer('Pid'),
        'database': fields.char('Database', size=256),
        'client': fields.char('Client', size=256),
        'duration': fields.float('Duration'),
        'wait': fields.boolean('Waiting'),
        'user': fields.char('User', size=256),
        'query': fields.text('Query')
    }

PgActivityActivity()


class PgActivityWaiting(osv.osv):
    _name = 'pg.activity.waiting'
    _inherit = 'pg.activity.base'
    _auto = False

    def read(self, cr, user, ids, fields=None, context=None):
        res = []
        with DataManager(**get_db_params()) as d:
            for act in d.pg_get_waiting():
                remove_none(act)
                res.append(dict(Waiting(*act)._asdict()))
        return res

    _columns = {
        'id': fields.integer('Pid'),
        'database': fields.char('Database', size=256),
        'mode': fields.char('Mode', size=256),
        'type': fields.char('Type', size=256),
        'relation': fields.char('Relation', size=256),
        'duration': fields.float('Duration'),
        'user': fields.char('User', size=256),
        'query': fields.text('Query')
    }

PgActivityWaiting()


class PgActivityBlocking(osv.osv):
    _name = 'pg.activity.blocking'
    _inherit = 'pg.activity.base'
    _auto = False

    def read(self, cr, user, ids, fields=None, context=None):
        res = []
        with DataManager(**get_db_params()) as d:
            for act in d.pg_get_blocking():
                remove_none(act)
                res.append(dict(Waiting(*act)._asdict()))
        return res

    _columns = {
        'id': fields.integer('Pid'),
        'database': fields.char('Database', size=256),
        'mode': fields.char('Mode', size=256),
        'type': fields.char('Type', size=256),
        'relation': fields.char('Relation', size=256),
        'duration': fields.float('Duration'),
        'user': fields.char('User', size=256),
        'query': fields.text('Query')
    }

PgActivityBlocking()
