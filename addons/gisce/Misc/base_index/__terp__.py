# -*- coding: utf-8 -*-
{
    "name": "Base index",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Add index capabilites to OpenERP
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "oorq",
        "base_extended"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "base_index_view.xml",
        "security/base_index_security.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
