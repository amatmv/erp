import copy
import json
from urlparse import urlparse, urljoin
import pooler

import netsvc
from osv import osv, fields
from oorq.decorators import job
from tools import config
import requests
from lxml import objectify


def rec_getattr(obj, attr):
    return reduce(getattr, attr.split('.'), obj)


def log(msg, level=netsvc.LOG_INFO):
    logger = netsvc.Logger()
    logger.notifyChannel("index", level, msg)


def fix_unicode(**data):
    unicoded = data.copy()
    for k, v in unicoded.iteritems():
        if isinstance(v, str):
            unicoded[k] = unicode(v)
    return unicoded


class IndexClient(object):
    def __init__(self, url):
        self.url = urlparse(url)

    def index(self, **data):
        data = fix_unicode(**data)
        if self.url.scheme in ('http', 'https'):
            index_url = urljoin(self.url.geturl(), 'index')
            data = json.dumps(data)
            headers = {'content-type': 'application/json'}
            r = requests.post(index_url, data=data, headers=headers)
            if r.status_code != 200:
                error_message = 'Error indexing {0} to {1} status code returned: {2} content: {3}'
                msg_error = error_message.format(
                    data, index_url, r.status_code, r.content)
                log(msg_error, netsvc.LOG_ERROR)
                raise Exception("Not indexed:\n{}".format(error_message))
        else:
            raise Exception(
                'URL: {0} is not supported for the index backend'.format(
                    self.url.geturl())
            )

    def deindex(self, path):
        if not self.url.scheme and self.url.path:
            idx = index.open_dir(self.url.path)
            writer = idx.writer()
            writer.delete_by_term('path', path)
            writer.commit()
            idx.close()
        elif self.url.scheme in ('http', 'https'):
            index_url = urljoin(self.url.geturl(), '{0}/{1}'.format(
                'index', path
            ))
            r = requests.delete(index_url)
            if r.status_code != 200:
                log("Error deindexing {0} to {1}".format(path, index_url),
                    netsvc.LOG_ERROR)
        else:
            raise Exception(
                'URL: {0} is not supported for the index backend'.format(
                    self.url.geturl())
            )

    def search(self, query, type=None, limit=80, offset=0):
        if type is not None:
            query += ' AND gistype:{0}'.format(type)
        if self.url.scheme in ('http', 'https'):
            """
            <feed xmlns:giscegis="http://www.gisce.net"
                  xmlns:opensearch="http://a9.com/-/spec/opensearch/1.1/"
                  xmlns:georss="http://www.georss.org/georss"
                  xmlns="http://www.w3.org/2005/Atom">
                <title>Title</title>
                <updated>2015-03-20T07:16:00.353080Z</updated>
                <author>
                    <name>GISCE-TI, S.L.</name>
                </author>
                <id>urn:uuid:f5fde211-ced0-11e4-931a-23b167fc202c</id>
                <opensearch:totalResults>1</opensearch:totalResults>
                <opensearch:startPage>1</opensearch:startPage>
                <opensearch:itemsPerPage>10</opensearch:itemsPerPage>
                <entry>
                    <georss:point>-0.828765654117 42.4200103928</georss:point>
                    <title>222</title>
                    <link href="http://example.org"/>
                    <content>
                        222 - CELDA 3 SUBESTACION - CONVERSION CASETA SUBESTACION
                    </content>
                    <title>222</title>
                    <updated>2015-03-20T07:16:00.353080Z</updated>
                    <id>urn:uuid:f5fde640-ced0-11e4-9c58-b1bb90975cb3</id>
                    <giscegis:id>222</giscegis:id>
                    <giscegis:type>giscedata.at.tram</giscegis:type>
                </entry>
            </feed>
            """
            res = []
            if limit is None:
                limit = 80
            pagenum = int(offset / limit) + 1
            index_url = self.url.geturl()
            params = {
                'q': query,
                'type': type,
                'startPage': pagenum,
            }
            r = requests.get(index_url, params=params)
            if r.status_code != 200:
                log("Error {} searching {} on index to {}".format(
                    r.status_code, params, index_url
                ), netsvc.LOG_ERROR)
            else:
                results = objectify.fromstring(r.content)
                type_ = '{http://www.gisce.net}type'
                id_ = '{http://www.gisce.net}id'
                point_ = '{http://www.georss.org/georss}point'
                totalResults = '{http://a9.com/-/spec/opensearch/1.1/}totalResults'
                n_results = int(results[totalResults])
                if not n_results:
                    return res
                for r_id, result in enumerate(results.entry):
                    ref = "{0},{1}".format(result[type_], result[id_])
                    lat, lon = 0, 0
                    point = result[point_]
                    if point:
                        lat, lon = point.text.split(" ")
                    res.append({
                        'id': r_id + 1,
                        'ref': ref,
                        'name': result['title'].text,
                        'summary': result['content'].text,
                        'type': result[type_].text,
                        'lat': lat,
                        'lon': lon
                    })
            return res
        else:
            raise Exception(
                'URL: {0} is not supported for the index backend'.format(
                    self.url.geturl())
            )


class BaseIndex(osv.osv):

    def createInstance(cls, pool, module, cr):
        """
        This method is overrided from osv.osv
        :param pool:
        :param module:
        :param cr: Database cursor
        :return:
        """

        parent_name = hasattr(cls, '_inherit') and cls._inherit
        if parent_name:
            parent_class = pool.get(parent_name).__class__
            assert pool.get(parent_name),\
                "parent class %s does not exist in module %s !" % (
                    parent_name, module
                )
            nattr = {}
            for s in ('_columns', '_defaults', '_inherits', '_constraints',
                      '_sql_constraints', '_index_fields'):
                pool_obj = pool.get(parent_name)
                if not hasattr(pool_obj, s):
                    continue
                new = copy.copy(getattr(pool_obj, s))
                if hasattr(new, 'update'):
                    new.update(cls.__dict__.get(s, {}))
                else:
                    if s == '_constraints':
                        for c in cls.__dict__.get(s, []):
                            exist = False
                            for c2 in range(len(new)):
                                # For _constraints, we should check field and
                                # methods as well
                                if new[c2][2] == c[2] and new[c2][0] == c[0]:
                                    new[c2] = c
                                    exist = True
                                    break
                            if not exist:
                                new.append(c)
                    else:
                        new.extend(cls.__dict__.get(s, []))
                nattr[s] = new
            name = hasattr(cls, '_name') and cls._name or cls._inherit
            if cls in parent_class.mro():
                cls = parent_class
            else:
                cls = type(name, (cls, parent_class), nattr)
        obj = object.__new__(cls)
        obj.__init__(pool, cr)
        return obj

    createInstance = classmethod(createInstance)

    _index_title = '{obj.name}'
    _index_summary = '{obj.name}'
    _index_fields = {}

    # To index all the active elements when the module is installed
    _index_on_install = True

    def get_lat_lon(self, cursor, uid, item, context=None):
        """
        Gets the lat and lon of an element

        :param cursor: Database cursor
        :param uid: User id
        :param item: Item to get the lat and lon
        :param context: OpenERP context
        :return: lat, lon
        """
        return 0, 0

    def get_path(self, cursor, uid, item_id, context=None):
        """
        Gets the path of the element

        :param cursor: Database cursor
        :param uid: User id
        :param item_id: Item id
        :param context: OpenERP context
        :return: Path as str
        """
        return '{0}/{1}'.format(self._name, item_id)

    def get_schema(self, cursor, uid, item, context=None):
        """
        Gets the index schema

        :param cursor: Database cursor
        :param uid: User id
        :param item: item to index
        :param context: OpenERP context
        :return: Dict with the schema
        """
        lat, lon = self.get_lat_lon(cursor, uid, item, context=context)
        content = self.get_content(cursor, uid, item, context=context)
        if content:
            return {
                'id': item.id,
                'path': self.get_path(cursor, uid, item.id, context=context),
                'title': self._index_title.format(obj=item),
                'content': ' '.join(set(content)),
                'gistype': self._name,
                'summary': self._index_summary.format(obj=item),
                'lat': lat,
                'lon': lon
            }
        return False

    def get_content(self, cursor, uid, item, context=None):
        """
        Gets the content to index

        :param cursor: Database cursor
        :param uid: User id
        :param item: Item to index
        :param context: OpenERP context
        :return: content as list
        """
        content = []
        for field, value in self._index_fields.items():
            try:
                data = rec_getattr(item, field)
            except AttributeError:
                continue
            if callable(value):
                data = value(self, data)
            if data:
                content.append(str(data))
        return content

    def is_indexable(self, context):
        return True

    @job(queue='index')
    def index(self, cursor, uid, ids, context=None):
        """
        Indexes the element

        :param cursor: Database cursor
        :param uid: User id
        :param ids: Ids to index
        :type ids: int, list
        :param context: OpenERP context
        :type: dict
        :return: True
        :rtype: bool
        """
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        if context is None:
            context = {}
        if not self.is_indexable(context):
            return True
        url = config.get('index_url', '')
        client = IndexClient(url)
        page_size = context.get('page_size', 80)
        start = 0
        end = page_size
        items = ids[start:end]
        while items:
            for item in self.browse(cursor, uid, items, context=context):

                schema = self.get_schema(cursor, uid, item, context=context)
                log("Indexing to URL: {0} data: {1}".format(url, schema),
                    netsvc.LOG_DEBUG)
                if schema:
                    client.index(**schema)
            start = end
            end += page_size
            items = ids[start:end]
        return True

    @job(queue='index')
    def deindex(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        if not self.is_indexable(context):
            return True
        url = config.get('index_url', '')
        client = IndexClient(url)
        for item_id in ids:
            path = self.get_path(cursor, uid, item_id, context=context)
            log("Removing from index URL: {0} path: {1}".format(url, path),
                netsvc.LOG_DEBUG)
            client.deindex(path)

    def _store_set_values(self, cr, uid, ids, fields, context):
        updated_ids = super(BaseIndex, self)._store_set_values(
            cr, uid, ids, fields, context
        )
        self.index(cr, uid, ids, context=context)
        return updated_ids

    def create(self, cursor, uid, vals, context=None):
        res = super(BaseIndex, self).create(cursor, uid, vals, context=context)
        self.index(cursor, uid, [res], context=context)
        return res

    def write(self, cursor, uid, ids, vals, context=None):
        if context is None:
            context = {}
        res = super(BaseIndex, self).write(cursor, uid, ids, vals,
                                           context=context)
        if not vals.get('active', True):
            self.deindex(cursor, uid, ids, context=context)
        else:
            self.index(cursor, uid, ids, context=context)
        return res

    def unlink(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        res = super(BaseIndex, self).unlink(cursor, uid, ids, context=context)
        self.deindex(cursor, uid, ids, context=context)
        return res

    def is_installed(self, cursor, module_name):
        """
        Checks if the module is installed

        :param cursor: Database cursor
        :param module_name: Name of the module
        :return: True if the module is installed
        :rtype: bool
        """

        sql_is_installed = """
        SELECT count(*)
        FROM ir_module_module
        WHERE (state='installed' OR state='to upgrade')
        AND name=%s
        """
        cursor.execute(sql_is_installed, (module_name, ))
        res = cursor.fetchone()
        return res[0] != 0

    def init(self, cursor):
        """
        Called when the module is installed

        :param cursor: Database cursor
        :return: Number of indexed elements
        :rtype: int
        """

        installed = self.is_installed(cursor, self._module)
        if self._index_on_install and not installed:
            self.index_all(cursor, 1)

    def index_all(self, cursor, uid, context=None):
        """
        Indexes all the elements of the model

        :param cursor: Database cursor
        :param uid: User id
        :param context: OpenERP context
        :return: Number of indexed elements
        :rtype: int
        """

        if context is None:
            context = {}

        if not pooler.pool_dic[cursor.dbname]._ready:
            return 0

        ids = self.search(cursor, uid, [], context=context)
        for identifier in ids:
            self.index(cursor, uid, identifier, context=context)

        return len(ids)


class BaseIndexSearch(osv.osv):
    _name = 'base.index.search'
    _auto = False

    def search(self, cursor, user, args, offset=0, limit=None, order=None,
               context=None, count=False):
        url = config.get('index_url', '')
        client = IndexClient(url)
        allowed_args = ('type', 'content')
        search_type = None
        query = None
        for arg in args:
            if arg[0] not in allowed_args:
                continue
            elif arg[0] == "type":
                search_type = arg[2]
            elif arg[0] == "content":
                query = arg[2]
        if query:
            res = client.search(query, type=search_type,
                                offset=offset, limit=limit)
            if count:
                res = len(res)
        else:
            res = []
        return res

    def read(self, cursor, user, ids, fields=None, context=None,
             load='_classic_read'):
        return ids

    def _objects(self, cursor, uid, context=None):
        cursor.execute("""
          select m.model, m.name from ir_model m order by m.model
        """)
        return cursor.fetchall()

    _columns = {
        'name': fields.text('Title'),
        'summary': fields.text('Summary'),
        'content': fields.text('Content', select=1),
        'lat': fields.char('Latitude', size=256),
        'lon': fields.char('Longitude', size=256),
        'ref': fields.reference('Reference', _objects, size=256),
        'type': fields.selection(_objects, 'Type', select=2)
    }


BaseIndexSearch()
