# -*- encoding: utf-8 -*-

from osv import osv
from osv import fields
from tools.translate import _


class partner_extended(osv.osv):

    _name = 'res.partner'
    _inherit = 'res.partner'

    def _get_default_country(self, cr, uid, context={}):
        '''Retornem espanya com a nacionalitat per defecte'''

        country_obj = self.pool.get('res.country')
        search_params = [('code', '=', 'ES')]

        country_ids = country_obj.search(cr, uid, search_params)
        if country_ids:
            return country_ids[0]

        return False

    _columns = {
        'country_id': fields.many2one('res.country', 'Nacionalidad',
                                     required=False,
                                     help='Indique el pais de nacionalidad'),

    }

    _defaults = {
        'country_id': _get_default_country,

    }

partner_extended()


class partner_address_extended(osv.osv):

    _name = 'res.partner.address'
    _inherit = 'res.partner.address'
    
    def fields_get(self, cursor, uid, fields=None, context=None):
        '''extend the search size of the partner_id field
        in the addresses view'''

        res = super(partner_address_extended,
                     self).fields_get(cursor, uid, fields=fields,
                                      context=context)
        if 'partner_id' in res:
            res['partner_id']['size'] = 40
        return res

partner_address_extended()
