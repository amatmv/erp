# -*- coding: utf-8 -*-
{
    "name": "Partner extension",
    "description": """Partner extension:
    * Add nacionality
    * Add street to address tree view
    * Add vat to partner tree view
    * Add active field to address form view
    * Wizard for copying an address
    """,
    "version": "0-dev",
    "author": "GISCE Enginyeria, SL",
    "category": "GISCEMaster",
    "depends":[
        "base_vat"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "partner_extended_view.xml",
        "wizard/wizard_duplicate_address_view.xml",
        "wizard/wizard_add_attachment_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
