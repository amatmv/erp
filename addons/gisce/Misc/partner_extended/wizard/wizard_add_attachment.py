# -*- encoding: utf-8 -*-

from osv import osv, fields
from tools.translate import _


class WizardPartnerAddAttachment(osv.osv_memory):

    _name = 'wizard.partner.add.attachment'

    def add_attachment(self, cursor, uid, ids, context=None):

        partner_obj = self.pool.get('res.partner')
        attach_obj = self.pool.get('ir.attachment')

        if not context:
            context = {}
        partner_id = context.get('partner_id', 0)

        if not partner_id:
            raise osv.except_osv('Error',
                                 _(u"No s'ha trobat cap client"))

        wizard = self.browse(cursor, uid, ids[0])

        vals = {
            'name': wizard.attachment_name,
            'datas_fname': wizard.filename,
            'res_id': partner_id,
            'res_model': 'res.partner',
            'category_id': wizard.category_id.id,
            'datas': wizard.file,
        }

        attach_obj.create(cursor, uid, vals)

        return {}

    _columns = {
        'file': fields.binary('Fitxer', required=True),
        'filename': fields.char('Nom del fitxer', size=100),
        'attachment_name': fields.char('Nom de l\'adjunt', size=100,
                                       required=True),
        'category_id': fields.many2one('ir.attachment.category',
                                       'Categoria'),        
    }

WizardPartnerAddAttachment()
