# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields

class soller_duplicate_partner_address(osv.osv_memory):

    _name = 'wizard.duplicate.address'

    
    def action_duplicate_address(self, cr, uid, ids, context=None):

        """funcion para duplicar una direccion de un partner"""

        wizard = self.browse(cr, uid, ids[0])
        address_obj = self.pool.get('res.partner.address')

        if not wizard.partner_id:
            new_address_id = address_obj.copy(cr, uid, wizard.address_id.id)    
        else:
            vals = {'partner_id': wizard.partner_id.id,
                    'name': wizard.partner_id.name,
                   }
            new_address_id = address_obj.copy(cr, uid, wizard.address_id.id, vals)

        return {}
        

    _columns = {
        'address_id':fields.many2one('res.partner.address', 'Adreça', required=True),
        'partner_id':fields.many2one('res.partner', 'Client', required=False, help="En blanc per copiar dins el mateix client"),
            
    }


soller_duplicate_partner_address()



