<?xml version="1.0" encoding="iso-8859-15"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

  <xsl:template match="/">
    <xsl:apply-templates select="pressuposts"/>
  </xsl:template>

  <xsl:template match="pressuposts">
    <xsl:apply-templates select="pressupost" />
  </xsl:template>

  <xsl:template match="pressupost">
  	<document debug="1" compression="1">
  	   <template pageSize="(42cm,29.7cm)">
  	     <pageTemplate id="carpeta">
  	       <pageGraphics>
  	         <image file="addons/custom/logo.png" width="20mm" height="20mm" x="22cm" y="26.6cm" />
          	 <setFont name="Helvetica-Bold" size="30" />
          	  <drawString x="24cm" y="27.5cm"><xsl:value-of select="//corporate-header/corporation/rml_header1" /></drawString>
          	 <setFont name="Helvetica" size="10" />
          	  <drawString x="24cm" y="27.1cm"><xsl:value-of select="//corporate-header/corporation/rml_footer1" /></drawString>
          	 <lineMode width="2" />
          	 <stroke color="lightgreen"/>
          	 <lines>
          		24cm 27cm 40cm 27cm
          	 </lines>
          	 <setFont name="Helvetica-Bold" size="14" />
          	 <stroke color="black" />
          	 <drawString x="23cm" y="1.5cm"><xsl:value-of select="name" /></drawString>
          	 <circle x="40cm" y="1.6cm" radius="1.5cm" />
          	 <drawRightString x="41cm" y="1.5cm"><xsl:value-of select="carpeta_tmp" /></drawRightString>
           </pageGraphics>
           <frame id="portada" x1="22cm" y1="3cm" width="19cm" height="23cm" />
         </pageTemplate>
       </template>
       
      <stylesheet> 
      	<paraStyle name="text"
      		fontName="Helvetica"
      		fontSize="10" />
      </stylesheet>
    
      <story>
      	<blockTable>
      		<tr>
      			<td><para style="text"><b>Pressupost:</b><xsl:value-of select="concat('  ',name)" /></para></td>
      			<td><para style="text"><b>Carpeta Temporal:</b><xsl:value-of select="concat('  ', carpeta_tmp)" /></para></td>
      		</tr>
      		<tr>
      			<td><para style="text"><b>Data inici:</b><xsl:value-of select="concat('  ', concat(substring(data, 9, 2),'/',substring(data, 6, 2),'/',substring(data, 1, 4)))" /></para></td>
      			<td></td>
      		</tr>
      	</blockTable>
      	<spacer length="20" />
      	<para style="text"><b>Responsable</b>:<xsl:value-of select="concat('  ',responsable)" /></para>
      	<spacer length="40" />
      	<para style="text"><b>Descripci�</b></para>
      	<para style="text" leftIndent="1cm"><xsl:value-of select="assumpte" /></para>
      	<spacer length="20" />
      	<blockTable colWidths="3cm,15cm">
      		<tr>
      			<td><para style="text"><b>Sol�licitant:</b></para></td>
      			<td><para style="text"><xsl:value-of select="sollicitud/client/nom" /></para></td>
      		</tr>
      		<tr>
      			<td><para style="text"><b>Adre�a:</b></para></td>
      			<td><para style="text"><xsl:value-of select="sollicitud/client/adreca/carrer" /></para></td>
      		</tr>
      		<tr>
      			<td><para style="text"><b>Municipi:</b></para></td>
      			<td><para style="text"><xsl:value-of select="sollicitud/client/adreca/municipi" /></para></td>
      		</tr>
      		<tr>
      			<td><para style="text"><b>Contacte:</b></para></td>
      			<td><para style="text"><xsl:value-of select="sollicitud/contacte/nom" /></para></td>
      		</tr>
      		<tr>
      			<td><para style="text"><b>Sol�licitant</b></para></td>
      			<td><para style="text"><xsl:value-of select="concat(sollicitud/contacte/telefon, ' ',sollicitud/contacte/mobil)" /></para></td>
      		</tr>
      		<xsl:if test="sollicitud/contacte/fax!=''">
      		<tr>
      			<td><para style="text"><b>Fax:</b></para></td>
      			<td><para style="text"><xsl:value-of select="sollicitud/contacte/fax" /></para></td>
      		</tr>
      		</xsl:if>
      		<tr>
      			<td><para style="text"><b>E-mail:</b></para></td>
      			<td><para style="text"><xsl:value-of select="sollicitud/contacte/email" /></para></td>
      		</tr>
      	</blockTable>
      </story>
    </document>
  </xsl:template>
</xsl:stylesheet>
