# -*- coding: utf-8 -*-
{
    "name": "GISCE Pressuposts Carpeta",
    "description": """Impressió de la carpeta pel projecte del pressupost""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Pressuposts",
    "depends":[
        "giscemisc_pressuposts"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscemisc_pressuposts_carpeta_report.xml",
        "giscemisc_pressuposts_carpeta_view.xml"
    ],
    "active": False,
    "installable": True
}
