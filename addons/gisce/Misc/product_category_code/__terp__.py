# -*- coding: utf-8 -*-
{
    "name": "Product Category Code",
    "description": """Adding an optional code to the model product.category""",
    "version": "0-dev",
    "author": "Miquel Frontera",
    "category": "Product",
    "depends":[
        "product"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "product_category_view.xml"
    ],
    "active": False,
    "installable": True
}
