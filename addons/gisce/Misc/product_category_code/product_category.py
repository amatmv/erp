# -*- encoding: utf-8 -*-

from osv import osv, fields


class ProductCategory(osv.osv):

    _name = "product.category"
    _inherit = "product.category"

    _columns = {
        'code': fields.char("Category Code", size=10)
    }

ProductCategory()
