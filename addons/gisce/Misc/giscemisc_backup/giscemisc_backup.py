# -*- coding: utf-8 -*-

from osv import osv, fields

class giscemisc_backup(osv.osv):

    _name = 'giscemisc.backup'

    _columns = {
      'name': fields.char('Nombre fichero', size=256, required=True),
      'checksum': fields.char('Checksum', size=40, required=True),
      'create_date': fields.datetime('Fecha creación'),
      'create_uid': fields.many2one('res.users', 'Usuario')
    }

    _order = 'create_date desc'


giscemisc_backup()
