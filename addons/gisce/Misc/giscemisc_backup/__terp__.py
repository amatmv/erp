# -*- coding: utf-8 -*-
{
    "name": "Backup export",
    "description": """
    This module provide :
        A menu to export the database
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Misc",
    "depends":[
        "base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscemisc_backup_wizard.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
