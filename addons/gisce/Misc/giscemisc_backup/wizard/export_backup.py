# -*- coding: utf-8 -*-

import wizard
import pooler
import tools
import netsvc
import time
import bz2
import base64

def _init(self, cr, uid, data, context=None):
    return {}

_init_form = """<?xml version="1.0"?>
<form string="Creación de backup">
  <label string="¡¡¡IMPORTANTE!!!" colspan="4" />
  <label string="Antes de empezar la exportación del backup asegúrese de que se está conectando a través del puerto %s y el protocolo XML-RPC." colspan="4"/>
  <label string="¿Todo correcto?" />
</form>""" % (tools.config['port'])

_init_fields = {}

def _backup(self, cr, uid, data, context=None):
    data = time.strftime('%Y%m%d%H%M%S')
    ext = 'bz2'
    name = '%s_%s.sql.%s' % (cr.dbname, data, ext)
    logger = netsvc.Logger()
    logger.notifyChannel("backup", netsvc.LOG_INFO, 'Making a backup from database to %s...' % (name,))
    cmd = ['pg_dump']
    if tools.config['db_user']:
        cmd.append('--username=' + tools.config['db_user'])
    if tools.config['db_host']:
        cmd.append('--host=' + tools.config['db_host'])
    if tools.config['db_port']:
        cmd.append('--port=' + tools.config['db_port'])
    cmd.append(cr.dbname)

    stdin, stdout = tools.exec_pg_command_pipe(*tuple(cmd))
    stdin.close()
    sql = stdout.read()
    sql_bz2 = bz2.compress(sql)
    file_b64 = base64.b64encode(sql_bz2)
    stdout.close()
    import sys
    if sys.version.split(' ')[0] >= '2.5.0':
        import hashlib
        checksum = hashlib.sha1(sql_bz2).hexdigest()
    else:
        import sha
        checksum = sha.new(sql_bz2).hexdigest()

    # Guardem el backup
    backup_obj = pooler.get_pool(cr.dbname).get('giscemisc.backup')
    vals = {
      'name': name,
      'checksum': checksum
    }

    backup_obj.create(cr, uid, vals)

    name = '%s_%s.%s.%s' % (cr.dbname, checksum, data, ext)

    return {
      'name': name,
      'file': file_b64,
      'checksum': checksum
    }

_backup_form = """<?xml version="1.0"?>
<form string="Creación de backup">
  <field name="name" readonly="1" colspan="4" width="550"/>
  <label string="Guarde el fichero en un lugar seguro. Contiene toda la información de su empresa en el ERP." colspan="4" />
  <field name="file" readonly="1" colspan="4" />
  <field name="checksum" readonly="1" colspan="4" />
</form>"""

_backup_fields = {
  'name': {'string': 'Nombre fichero', 'type': 'char', 'size': 256},
  'file': {'string': 'Fichero', 'type': 'binary'},
  'checksum': {'string': 'Checksum', 'type': 'char', 'size': 40}
}

class giscemisc_backup_export_wizard(wizard.interface):

    states = {
      'init': {
        'actions': [_init],
        'result': {'type': 'form', 'arch': _init_form, 'fields': _init_fields,  'state': [('end', 'Cancelar', 'gtk-cancel'), ('backup', 'Crear Backup', 'gtk-go-forward')]},
      },
      'backup': {
        'actions': [_backup],
        'result': {'type': 'form', 'arch': _backup_form, 'fields': _backup_fields,  'state': [('end', 'Finalizar', 'gtk-ok')]},
      },
    }

giscemisc_backup_export_wizard('giscemisc.backup.export')
