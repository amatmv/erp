# -*- coding: utf-8 -*-
{
    "name": "Partner representante",
    "description": """Partner representante
    - Añade la opción de añadir un representante a las empresas
    """,
    "version": "0-dev",
    "author": "GISCE-TI, S.L.",
    "category": "Misc",
    "depends":[
        "base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "partner_view.xml"
    ],
    "active": False,
    "installable": True
}
