from osv import osv, fields


class ResPartner(osv.osv):
    _name = 'res.partner'
    _inherit = 'res.partner'

    _columns = {
        'representante_id': fields.many2one('res.partner', 'Representante')
    }

ResPartner()
