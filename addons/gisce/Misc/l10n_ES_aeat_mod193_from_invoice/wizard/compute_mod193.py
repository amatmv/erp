# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2013 GISCE-TI, S.L. All Rights Reserved
#
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv, fields
from tools.translate import _
import re

NO_FISICAL_PERSON = ('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'N', 'P',
                     'Q', 'R', 'S', 'U', 'V', 'W')


class WizardComputeMod193Invoice(osv.osv_memory):

    _name = 'wizard.compute.mod193.invoice'

    def _get_percent(self, cursor, uid, invoice, context=None):
        tax_obj = self.pool.get('account.tax')
        for tax_line in invoice.tax_line:
            search_params = [('name', '=', tax_line.name)]
            tax_ids = tax_obj.search(cursor, uid, search_params,
                                     context=context)
            if tax_ids:
                tax = tax_obj.browse(cursor, uid, tax_ids[0])
                return abs(tax.amount * 100)
        return False

    def action_compute(self, cursor, uid, ids, context=None):

        report_obj = self.pool.get('l10n.es.aeat.mod193.report')
        record_obj = self.pool.get('l10n.es.aeat.mod193.record')
        invoice_obj = self.pool.get('account.invoice')

        if not context:
            context = {}
        wizard = self.browse(cursor, uid, ids[0])
        report_id = context.get('report_id', 0)
        report = report_obj.browse(cursor, uid, report_id,
                                   context=context)
        date_start = report.fiscalyear_id.date_start
        date_end = report.fiscalyear_id.date_stop
        if wizard.recompute:
            search_params = [('report_id', '=', report_id)]
            record_ids = record_obj.search(cursor, uid, search_params)
            record_obj.unlink(cursor, uid, record_ids)
        #For each line in the libro search for invoices
        common_vals = {
                       'report_id': report.id,
                       'lr_vat': '',
                       'mediator': False,
                       'code_key': '1',
                       'code_value': report.company_vat,
                       'incoming_key': 'B',
                       'nature': '03',
                       'payment': '1',
                       'code_type': False,
                       'code_bank': '',
                       'pending': ' ',
                       'fiscal_year_id': report.fiscalyear_id.id,
                       'incoming_type': '1'
                       }

        #Search for all in invoices made for this cabecera
        search_params = [('type', '=', 'in_invoice'),
                         ('date_invoice', '>=', date_start),
                         ('date_invoice', '<=', date_end),
                         ('journal_id.id', '=', wizard.journal_id.id)]
        if wizard.only_paid:
            search_params += [('state', '=', 'paid')]
        invoice_ids = invoice_obj.search(cursor, uid, search_params,
                                         order="date_invoice",
                                         context=context)
        #If no invoices, nothing to do
        if not invoice_ids:
            return {}
        for invoice in invoice_obj.browse(cursor, uid, invoice_ids,
                                          context=context):
            percent = self._get_percent(cursor, uid,
                                        invoice, context=context)

            #Search for state_id
            if (invoice.address_invoice_id.state_id and
                invoice.address_invoice_id.country_id.code == 'ES'):
                state_id = invoice.address_invoice_id.state_id.id

            else:
                raise osv.except_osv(
                    _('Error'),
                    _("L'adreça de la factura %s no té província")
                    % invoice.number
                )
            record_vals = common_vals.copy()
            record_vals.update({
                'amount': invoice.amount_untaxed,
                'amount_base': invoice.amount_untaxed,
                'amount_tax': abs(invoice.amount_tax),
                'partner_id': invoice.partner_id.id,
                'partner_vat': (invoice.partner_id.vat and
                                re.match("(ES){0,1}(.*)",
                                         invoice.partner_id.vat).groups()[1]
                                ),
                'state_id': state_id,
                'tax_percent': percent,
                'postal_address_id': invoice.address_contact_id.id,
                'fiscal_address_id': invoice.address_invoice_id.id
            })
            # Check if this partner exists
            record_id = record_obj.search(cursor, uid, [
                ('report_id.id', '=', record_vals['report_id']),
                ('partner_vat', '=', record_vals['partner_vat'])
            ])
            if record_id:
                keys = ['amount', 'amount_base', 'amount_tax']
                vals = {}
                record = record_obj.read(cursor, uid, record_id[0], keys)
                for key in keys:
                    vals[key] = record[key] + record_vals[key]
                record_obj.write(cursor, uid, record_id, vals)
            else:
                record_obj.create(cursor, uid, record_vals, context=context)

        #Write state computed in report
        report.write({'state': 'computed'})
        return {}

    _columns = {
        'journal_id': fields.many2one('account.journal', 'Diari',
                                      required=True),
        'only_paid': fields.boolean(
            'Només factures pagades',
            help=u"Només tenir en compte les factures que estan marcades com "
                 u"a pagades."),
        'recompute': fields.boolean(
            'Recalcular',
            help=u"If checked, all the records in the report will be deleted "
                 u"before creating new ones.")
    }
WizardComputeMod193Invoice()
