# -*- coding: utf-8 -*-
{
    "name": "Modelo 193 desde facturas",
    "description": """
    This module provide :
        * Generación del modelo 193 a través de las facturas.
    """,
    "version": "0-dev",
    "author": "GISCE-TI, S.L.",
    "category": "Localisation/Accounting",
    "depends":[
        "base",
        "account",
        "l10n_ES_aeat_mod193"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/compute_mod193_view.xml"
    ],
    "active": False,
    "installable": True
}
