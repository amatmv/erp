# -*- coding: utf-8 -*-
import logging

from twilio.rest import Client
from twilio.base.exceptions import TwilioRestException

from tools import config
from osv import osv, fields

_sms_states = [('draft', 'Draft'),
               ('outgoing', 'Outgoing'),
               ('sent', 'Sent'),
               ('received', 'Received'),
               ('exception', 'Delivery Failed'),
               ('cancel', 'Cancelled'),
               ]

class NotificationSMS(osv.osv):

    _name = 'notification.sms'
    _description = 'SMS notification'

    _rec_name = 'subject'

    def _ff_subject(self, cursor, uid, ids, field_name, arg,
                              context=None):
        res = {}
        for sms in self.read(cursor, uid, ids, ['content']):
            res[sms['id']] = sms['content']
        return res

    def _get_models(self, cursor, uid, context={}):
        cursor.execute('select m.model, m.name from ir_model m order by m.model')
        return cursor.fetchall()

    _columns = {
        'partner_address_id': fields.many2one(
            'res.partner.address',
            'Related address partner'),
        'user_id': fields.many2one('res.users', 'Related user', readonly=1),
        'state': fields.selection(_sms_states, 'State'),
         'reference': fields.reference('Source Object', selection=_get_models,
                                      size=128),
        #fields.char('Related model', size=512, select=1, readonly=1),
        'res_id': fields.integer('Related ID', select=1, readonly=1),
        'sms_from': fields.char('From', size=12),
        'sms_to': fields.char('To', size=12),
        'content': fields.char('Content', size=160),
        'subject': fields.function(
            _ff_subject,
            type='char',
            string='Subject',
            size=160,
            method=True
        )
    }

    _defaults = {
        'state': lambda *a: 'draft',
        'sms_from': lambda *a: config.get('twilio_sms_from', False)
    }

    def send(self, cursor, uid, ids):
        logger = logging.getLogger(__name__)
        account_sid = config.get('twilio_account_sid', False)
        auth_token = config.get('twilio_auth_token', False)
        if not account_sid or not auth_token:
            logger.error('''
            Send Message Exception: You need to configure
            Twilio account and/or auth_token  ''')
            return False

        client = Client(account_sid, auth_token)

        for sms in self.browse(cursor, uid, ids):
            logger.info('Send SMS id:%s from:%s to:%s content: %s' % (
                sms.id, sms.sms_from, sms.sms_to, sms.content))
            try:
                if sms.sms_from == '+34983060589':
                    from_ = sms.sms_from
                else:
                    from_ = '+34518880651'
                message = client.messages.create(
                    body=sms.content,
                    to=sms.sms_to,
                    from_=from_#sms.sms_from
                )
                sms.write({'state': 'sent'})
                logger.info('Send SMS id:%s result: %s' % (
                sms.id, message))
            except TwilioRestException, e:
                logger.error(
                    'Exception send SMS id:%s with the follow error:\n %s' %(
                        sms.id, str(e)))
                sms.write({'state': 'exception'})

    def get_notification_state(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        res = {}
        for sms in self.browse(cursor, uid, ids, context=context):
            res[sms.id] = sms.state
        return res

    def get_subject(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        res = self._ff_subject(cursor, uid, ids, False, False, context=context)
        return res


NotificationSMS()


class Notification(osv.osv):

    _name = 'notification'
    _inherit = 'notification'

    def __init__(self, pool, cursor):
        """Init to add new states."""
        super(Notification, self).__init__(pool, cursor)
        notification_sms = ('notification.sms', 'SMS')
        if notification_sms not in self._columns['type'].selection:
            self._columns['type'].selection.append(notification_sms)

Notification()