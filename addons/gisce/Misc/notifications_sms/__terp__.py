# -*- coding: utf-8 -*-
{
    "name": "Notification SMS",
    "description": """
        OpenERP Notifications SMS
        """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Misc",
    "depends":[
        "base",
        "notifications"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/ir.model.access.csv",
        "notifications_sms_view.xml"
    ],
    "active": False,
    "installable": True
}
