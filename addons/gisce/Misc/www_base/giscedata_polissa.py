# -*- coding: utf-8 -*-
from osv import osv
from datetime import datetime
import netsvc


def _format_iban(string):
    '''
    This function removes all characters from given 'string' that isn't a
    alpha numeric and converts it to lower case.
    '''
    res = ""
    for char in string:
        if char.isalnum():
            res += char.upper()
    return res


class GiscedataPolissa(osv.osv):
    """Extensió de la pòlissa amb les dades bàsiques de facturació.
    """
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def update_current_mod_con(self, cursor, uid, polissa_id):
        wz_crear_mc_obj = self.pool.get('giscedata.polissa.crear.contracte')
        ctx = {'active_id': polissa_id}
        params = {'accio': 'modificar'}
        wz_id = wz_crear_mc_obj.create(cursor, uid, params, ctx)
        wiz = wz_crear_mc_obj.browse(cursor, uid, wz_id, ctx)
        wiz.action_crear_contracte(ctx)

    def validacio_bank(self, cursor, uid, polissa, bank):
        partner_bank_obj = self.pool.get('res.partner.bank')
        search = [('id', '=', bank),
                  ('partner_id', '=', polissa['pagador'][0])]
        bank_ids = partner_bank_obj.search(cursor, uid, search)
        if bank_ids:
            return True
        else:
            logger = netsvc.Logger()
            logger.notifyChannel("objects", netsvc.LOG_ERROR,
                                 "El id bank %s no pertany al pagador %s "
                                 % (bank, polissa['pagador']))
            return False
    def actualitzar_mandato(self, cursor, uid, polissa):
        mandate_obj = self.pool.get('payment.mandate')
        search = [('reference', '=', 'giscedata.polissa,%s' % polissa['id'])]
        mandate_id = mandate_obj.search(cursor, uid, search)
        if mandate_id:
            vals = {'date_end': datetime.now().strftime('%Y-%m-%d')}
            mandate_obj.write(cursor, uid, mandate_id, vals)

        vals = {'reference': 'giscedata.polissa,%s' % polissa['id']}
        mandate_obj.create(cursor, uid, vals)


    def www_actualitzar_polissa(self, cursor, uid, ids, values,
                                modifcacio=False, data_inici=False,
                                data_final=False,  context=None):
        try:
            if isinstance(ids, (list, set, tuple)):
                polissa_id = ids[0]
            else:
                polissa_id = ids
            logger = netsvc.Logger()
            logger.notifyChannel("objects", netsvc.LOG_INFO,
                                     "www_actualitzar_polissa id %s"
                                     % (polissa_id))
            polissa_obj = self.pool.get('giscedata.polissa')
            polissa = polissa_obj.read(cursor, uid, polissa_id,
                                           ['pagador', 'modcontractuals_ids'])
            polissa_obj.write(cursor, uid, polissa_id, values)
            polissa_obj.generar_periodes_potencia(cursor, uid, [polissa_id])

            if 'bank' in values and \
               not self.validacio_bank(cursor, uid, polissa, values['bank']):
               return False

            #TODO: En prinicipi d'aquesta manera es permet modificar les dades
            #de la pòlissa abans que estigui activa sense modificacions contractuals.
            if 'bank' in values:
                self.actualitzar_mandato(cursor, uid, polissa)
            if polissa['modcontractuals_ids']:
                #Activem la modificació contractual
                wkf_service = netsvc.LocalService('workflow')
                wkf_service.trg_validate(uid, 'giscedata.polissa', polissa_id,
                                         'modcontractual', cursor)
                canvis = None
                canvis = polissa_obj.get_changes(cursor, uid, polissa_id)

                if not canvis:
                    wkf_service.trg_validate(uid, 'giscedata.polissa',
                                                polissa_id, 'undo_modcontractual',
                                                cursor)
                    return True
                else:
                    wz_crear_mc_obj = self.pool.get('giscedata.polissa.crear.contracte')
                    ctx = {'active_id': polissa_id}
                    # I a bank modification, we should modify current modcon
                    if modifcacio or 'bank' in values:
                        self.update_current_mod_con(cursor, uid, polissa_id)
                        return True
                    else:
                        params = {'duracio': 'nou',
                                  'accio': 'nou'}
                        wz_id = wz_crear_mc_obj.create(cursor, uid, params, ctx)
                        wiz = wz_crear_mc_obj.browse(cursor, uid, wz_id, ctx)
                        if data_inici:
                            wiz.write({'data_inici': data_inici})
                        res = wz_crear_mc_obj.onchange_duracio(cursor, uid,
                                                           [wz_id], wiz.data_inici,
                                                           wiz.duracio, ctx)
                        if not res.get('warning', False):
                            wiz_data_final = res['value']['data_final']
                            if data_final:
                                wiz_data_final = data_final

                            wiz.write({'data_final': wiz_data_final})
                            wiz.action_crear_contracte(ctx)
                            return True
                        else:
                            self.update_current_mod_con(cursor, uid, polissa_id)
                            return True
            else:
                return True
        except Exception, e:
            sentry = self.pool.get('sentry.setup')
            if sentry is not None:
                sentry.client.captureException()
            return False

    def www_periodes_tarifa(self, cursor, uid, ids):
        if isinstance(ids, (list, set, tuple)):
            polissa_id = ids[0]
        else:
            polissa_id = ids

        polissa_obj = self.pool.get('giscedata.polissa')
        tarfia_obj = self.pool.get('giscedata.polissa.tarifa')
        dades = polissa_obj.read(cursor, uid, polissa_id, ['tarifa'])
        return tarfia_obj.get_num_periodes(cursor, uid, [dades['tarifa'][0]])

    def www_get_iban(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        if isinstance(ids, (list, set, tuple)):
            polissa_id = ids[0]
        else:
            polissa_id = ids

        polissa_obj = self.pool.get('giscedata.polissa')
        polissa_data = polissa_obj.read(cursor, uid, polissa_id, ['bank'])
        iban = ''
        if polissa_data and polissa_data.get('bank', False):
            iban = polissa_data['bank'][1]
            iban = iban[:-4] + '****'
        return iban

    def www_check_iban(self, cursor, uid, iban, context=None):
        if not context:
            context = {}
        iban = _format_iban(iban)
        bank_obj = self.pool.get('res.partner.bank')
        country_obj = self.pool.get('res.country')
        country_id = country_obj.search(cursor, uid, [('code', '=', iban[:2])])
        if country_id:
            country_id = country_id[0]
        else:
            #El pais del iban no existeix
            return False
        vals_bank = bank_obj.onchange_banco(
            cursor, uid, [], iban[4:], country_id, context)
        if 'warning' in vals_bank:
            # Error del número de compte
            return False

        check_iban = bank_obj.calculate_iban(
            cursor, uid, iban[4:], iban[:2])

        if check_iban != iban:
            # Iban generat es diferent al iban passat
            return False
        return iban

    def www_set_iban(self, cursor, uid, ids, iban, context=None):
        if not context:
            context = {}
        if isinstance(ids, (list, set, tuple)):
            polissa_id = ids[0]
        else:
            polissa_id = ids

        polissa_obj = self.pool.get('giscedata.polissa')
        bank_obj = self.pool.get('res.partner.bank')
        country_obj = self.pool.get('res.country')
        polissa_data = polissa_obj.read(cursor, uid, polissa_id, ['pagador'])
        iban = self.www_check_iban(cursor, uid, iban)
        if not iban:
            return False
        country_id = country_obj.search(
            cursor, uid, [('code', '=', iban[:2])])[0]
        vals_bank = bank_obj.onchange_banco(
            cursor, uid, [], iban[4:], country_id, context)
        pagador_id = polissa_data['pagador'][0]
        bank_ids = bank_obj.search(
            cursor, uid, [('iban', '=', iban), ('partner_id', '=', pagador_id)])
        if bank_ids:
            bank_id = bank_ids[0]
        else:
            vals = vals_bank['value']
            vals.update({
                'state': 'iban',
                'country_id': country_id,
                'partner_id': pagador_id,
                'acc_country_id': country_id,
                'iban': iban
            })
            bank_id = bank_obj.create(cursor, uid, vals)
        return self.www_actualitzar_polissa(
            cursor, uid, polissa_id, {'bank': bank_id})

GiscedataPolissa()
