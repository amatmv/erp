# -*- coding: utf-8 -*-
{
    "name": "WWW Base",
    "description": """
        Mòdul base per oficienes virtuals
    """,
    "version": "0-dev",
    "author": "GISCE-TI, S.L.",
    "category": "www",
    "depends":[
        "base",
        "giscedata_polissa"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
