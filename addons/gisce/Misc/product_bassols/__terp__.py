# -*- coding: utf-8 -*-
{
    "name": "Adaptacio per bassols del modul product",
    "description": """Adaptacio per bassols""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Product",
    "depends":[
        "product"
    ],
    "init_xml":[
        "data.xml"
    ],
    "demo_xml": [],
    "update_xml":[
        "product_bassols_wizard.xml",
        "product_bassols_view.xml",
        "product_bassols_data.xml",
        "product_bassols_report.xml"
    ],
    "active": False,
    "installable": True
}
