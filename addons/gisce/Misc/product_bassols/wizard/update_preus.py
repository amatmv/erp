# -*- coding: utf-8 -*-

from osv import osv, fields
import wizard
import pooler
import math

def _init(self, cr, uid, data, context={}):
  data['n_productes'] = len(data['ids'])
  data['contador'] = 0
  return {}

def _mes_productes(self, cr, uid, data, context={}):
  if data['contador'] >= data['n_productes']:
    return 'end'
  else:
    return 'operacio'
  

def _operacio(self, cr, uid, data, context={}):

  product_obj = pooler.get_pool(cr.dbname).get('product.product')
  product = product_obj.browse(cr, uid, data['ids'][data['contador']])

  if data['form'].has_key('notes'):
    notes = data['form']['notes']
  else:
    notes = ''
  if data['form'].has_key('operacio') and data['form']['operacio'] != False:
    operacio = data['form']['operacio']
    pa = product.list_price
    preu_final = eval(operacio)
  else:
    operacio = ''
    pa = product.list_price
    preu_final = pa

  return {
    'producte': product.id,
    'preu_actual': pa,
    'operacio': operacio,
    'preu_final': preu_final,
    'notes': notes,
    }

_operacio_form = """<?xml version="1.0"?>
<form string="Actualització de preus">
  <field name="producte" colspan="4" width="500"/>
  <field name="preu_actual" readonly="1"/>
  <newline />
  <field name="operacio" colspan="4" on_change="operacio_change(preu_actual, operacio)"/>
  <field name="preu_final" readonly="1" />
  <button type="object" name="calcular" icon="gtk-refresh" string="Calcular" />
  <group string="Notes" colspan="4">
    <field name="notes" nolabel="1" colspan="4" />
  </group>
</form>"""


_operacio_fields = {
  'producte': {'string': 'Producte', 'type': 'many2one', 'relation': 'product.product'},
  'preu_actual': {'string': 'Preu actual', 'type': 'float'},
  'operacio': {'string': 'Operació', 'type': 'char', 'size': 255, 'help': 'En aquest camp podem introduïr una operació matemàtica per tal de construïr el nou preu. Podem utilitzar la variable (pa) com al valor del preu actual.\n  - Exemple augmentar el preu actual un 20%: pa*1.20\nPer tal de posar un preu fixe l\'hem d\'escriure directament al camp Operació.\nPer saber quin serà el resultat que ens quedarà hem d\'apretar a "Calcular"'},
  'preu_final': {'string': 'Preu final', 'type': 'float'},
  'notes': {'string': 'Notes', 'type': 'text'},
}

def _anterior(self, cr, uid, data, context={}):
  data['contador'] -= 1
  return {}

def _seguent(self, cr, uid, data, context={}):
  data['contador'] += 1
  return {}

def _guardar(self, cr, uid, data, context={}):
  product_obj = pooler.get_pool(cr.dbname).get('product.product')
  product = product_obj.browse(cr, uid, data['form']['producte'])
  price_obj = pooler.get_pool(cr.dbname).get('product.price.historic')
  price_obj.create(cr, uid, {'price': data['form']['preu_final'], 'info': data['form']['notes'], 'product_template_id': product.product_tmpl_id.id})
  data['contador'] += 1
  return {}

class wizard_update_preus(wizard.interface):

  states = {
    'init': {
    	'actions': [_init],
      'result': {'type': 'state', 'state': 'mes_productes'}
    },
    'mes_productes': {
      'actions': [],
      'result': {'type': 'choice', 'next_state': _mes_productes}
    },
    'operacio': {
      'actions': [_operacio],
      'result': {'type': 'form', 'arch': _operacio_form, 'fields': _operacio_fields, 'state': [('anterior', 'Anterior', 'gtk-go-back'), ('seguent', 'Següent', 'gtk-go-forward'), ('guardar', 'Guardar i Següent', 'gtk-save'), ('end', 'Tancar', 'gtk-close')]}
    },
    'anterior': {
      'actions': [_anterior],
      'result': {'type': 'state', 'state': 'mes_productes'}
    },
    'seguent': {
      'actions': [_seguent],
      'result': {'type': 'state', 'state': 'mes_productes'}
    },
    'guardar': {
      'actions': [_guardar],
      'result': {'type': 'state', 'state': 'mes_productes'}
    },
    'end': {
      'actions': [],
      'result': {'type': 'state', 'state': 'end'}
    },
  }

wizard_update_preus('product.bassols.update.preus')


class wizard_product_bassols_update_preus(osv.osv):
  _name = 'wizard.product.bassols.update.preus'
  _auto = False

  def create(self, cr, uid, vals, context={}):
    return 1
  
  def write(self, cr, uid, ids, vals, context={}):
    return 1

  def calcular(self, cr, uid, ids, args):
    return True

  def operacio_change(self, cr, uid, ids, pa, operacio, context={}):
    if operacio:
      preu_final = eval(operacio)
    else:
      preu_final = pa
    return {'value': {'preu_final': preu_final}}

wizard_product_bassols_update_preus()

