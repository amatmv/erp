# -*- coding: utf-8 -*-
from osv import fields,osv
from tools import config
from datetime import *

class product_product(osv.osv):
  _name = 'product.product'
  _inherit = 'product.product'

  def _desglossat(self, cr, uid, ids, field_name, arg, context):
    res = {}
    bom = self.pool.get('mrp.bom')
    for p in self.browse(cr, uid, ids):
      bom_ids = bom.search(cr, uid, [('product_id', '=', p.id)])
      res[p.id] = []
      if bom_ids and len(bom_ids):
        for m in bom.browse(cr, uid, bom_ids[0]).bom_lines:
          res[p.id].append(m.id)
    return res

  def _desglossat_inv(self, cr, uid, id, name, values, arg, context):
    
    def add(values, bom_id):
      for v in values:
        if v[2]: # Add
          v[2]['bom_id'] = bom_id
          self.pool.get('mrp.bom').create(cr, uid, v[2])
        else: # Delete
          self.pool.get('mrp.bom').unlink(cr, uid, v[1])

    bom_ids = self.pool.get('mrp.bom').search(cr, uid, [('product_id', '=', id)]) 
    if bom_ids and len(bom_ids):
      # Ja hi ha desglossat creat
      # Hi afegim els materials nous
      add(values, bom_ids[0])
    else:
      # No hi ha desglossat creat, en creem un per aquest producte
      vals = {}
      p = self.browse(cr, uid, id)
      vals['name'] = p.name
      vals['product_id'] = p.id
      vals['product_uom'] = p.uom_id.id
      if len(values):
        bom_id = self.pool.get('mrp.bom').create(cr, uid, vals)
        add(values, bom_id)
        

  _columns = {
    'id_antic': fields.integer('ID antic'),
    'desglossat': fields.function(_desglossat, fnct_inv=_desglossat_inv, type='one2many', obj='mrp.bom', string='Desglossat', method=True),
  }

  def search(self, cr, user, args, offset=0, limit=None, order=None, context=None):
    i = 0
    if args:
      for arg in args:
        if not arg[0] == 'categ_id':
          i += 1
        else:
          args[i] = ['categ_id', 'child_of', args[i][2]]
    return super(osv.osv, self).search(cr, user, args, offset, limit, order, context)

  def unlink(self, cr, uid, ids):
    self.write(cr, uid, ids, {'active': False})
    return True

  def copy(self, cr, uid, id, default=None, context={}):
    if not default:
      default = {}
    default.update({
      'desglossat':[]
    })
    return super(product_product, self).copy(cr, uid, id, default, context)

  def update_trans_names(self, cr, uid, user_id=False):
    print '* Running cron user %s' % (user_id)
    report = 'DESGLOSSATS\n'
    cr.execute("select b.id,b.name,t.res_id,t.value from ir_translation t, product_template pt, mrp_bom b where t.name = 'product.template,name' and t.lang = 'ca_ES' and t.res_id = pt.id and b.name != t.value and b.product_id = t.res_id;")
    names = cr.dictfetchall()
    for name in names:
      report += '* %s a %s [%i]\n' % (name['name'], name['value'], name['id'])
      cr.execute("update mrp_bom set name = %s where id = %i", (name['value'], name['id']))
    cr.commit()
    report += '\nPRODUCTES\n'
    cr.execute("select t.res_id,t.value,pt.name from ir_translation t, product_template pt where t.name = 'product.template,name' and t.lang = 'ca_ES' and t.res_id = pt.id and pt.name != t.value")
    p_names = cr.dictfetchall()
    for name in p_names:
      report += '* %s a %s [%i]\n' % (name['name'], name['value'], name['res_id'])
      cr.execute("update product_template set name = %s where id = %i", (name['value'], name['res_id']))
    cr.commit()

    # Creació del request
    print '* Creant report...'
    request = self.pool.get('res.request')
    request.create(cr, uid,
			{'name' : "Informe de sincronització de noms.",
				'act_from' : user_id,
				'act_to' : user_id,
				'body': report,
			})

product_product()

class product_template(osv.osv):

  def _get_last_historic_price(self, cr, uid, ids, name, arg, context={}):
    res = {}
    for id in ids:
      cr.execute("select price from product_price_historic where product_template_id = %s order by name desc limit 1" % id)
      price = cr.fetchone()
      if price:
        res[id] = price[0]
      else:
        res[id] = 0.0
    return res

  
  _name = 'product.template'
  _inherit = 'product.template'

  _columns = {
    'list_price': fields.function(_get_last_historic_price, method=True, type='float', string='List Price', readonly=True),
    'prices_historic': fields.one2many('product.price.historic', 'product_template_id', 'Prices'),
  }

product_template()

class product_price_historic(osv.osv):
  _name = 'product.price.historic'

  _columns = {
		'price': fields.float('Price', required=True, digits=(16, int(config['price_accuracy']))),
    'name': fields.datetime('Data'),
    'info': fields.char('Notes', size=255),
    'product_template_id': fields.many2one('product.template', 'Product'),
  }

  _defaults = {
    'name': lambda *x: (datetime.now()).strftime('%Y-%m-%d %H:%M:%S'),
  }

  _order = "name desc"

product_price_historic()
