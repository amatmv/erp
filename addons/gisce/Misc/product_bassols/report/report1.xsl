<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="productes"/>
  </xsl:template>

  <xsl:template match="productes">
    <document>
      <template>
        <pageTemplate id="main">
          <frame id="first" x1="1cm" y1="1cm" width="19cm" height="277mm"/>
        </pageTemplate>
      </template>

      <stylesheet>
        <paraStyle name="text" fontName="Helvetica" fontsize="10" spaceBefore="0" spaceAfter="0"/>
        
        <paraStyle name="titol"
        	fontName="Helvetica-Bold"
        	fontSize="14"
        	leading="28"
        />
        
        <blockTableStyle id="productes">
      		<blockBackground colorName="grey" start="0,0" stop="-1,0" />
      		<blockFont name="Helvetica" size="10" />
       		<blockFont name="Helvetica-Bold" size="10" start="0,0" stop="-1,0"/>
       		<lineStyle kind="GRID" colorName="silver" />
      	</blockTableStyle>
      </stylesheet>

      <story>
   	    <para style="titol">Llistat de productes</para>
   	    <blockTable style="productes" repeatRows="1" colWidths="12cm,4cm,3cm">
  			<tr>
  				<td>Nom</td>
  				<td>Categoria</td>
  				<td>Preu</td>
  			</tr>
  			<xsl:apply-templates select="producte" mode="story">
  				<xsl:sort select="category" />
  			</xsl:apply-templates>
	  	</blockTable>
      </story>
    </document>
  </xsl:template>

  <xsl:template match="producte" mode="story">
  	<tr>
  		<td><para style="text"><xsl:value-of select="name" /></para></td>
  		<td><para style="text"><xsl:value-of select="category" /></para></td>
  		<td><para style="text" alignment="right"><xsl:value-of select="format-number(list_price, '###,###.00')" /></para></td>
  	</tr>
  </xsl:template>

</xsl:stylesheet>
