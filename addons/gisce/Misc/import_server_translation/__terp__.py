# -*- coding: utf-8 -*-
{
    "name": "Wizard to import string translations from other server",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Translations",
    "depends":[
        "base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "import_server_translation_wizard.xml"
    ],
    "active": False,
    "installable": True
}
