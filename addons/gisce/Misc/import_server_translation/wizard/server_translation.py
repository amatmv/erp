# -*- coding: iso-8859-1 -*-
import wizard
import pooler
import xmlrpclib

_init_form = """<?xml version="1.0"?>
<form string="Servidor" col="2">
  <field name="server" required="1" width="200"/>
  <field name="bbdd" required="1" />
  <field name="user" required="1" />
  <field name="passwd" hidden="1" required="1" />
  <field name="lang" required="1" />
</form>"""

_init_fields = {
  'server': {'string': 'Servidor', 'type': 'char', 'size': 50},
  'bbdd': {'string': 'Base de dades', 'type': 'char', 'size': 50},
  'user': {'string': 'Usuari', 'type': 'char', 'size': 50},
  'passwd': {'string': 'Contrassenya', 'type': 'char', 'size': 50, 'invisible': True},
  'lang': {'string': 'Idioma', 'type': 'char', 'size': 5},
}

def _init(self, cr, uid, data, context={}):
  return {}


def _login(self, cr, uid, data, context={}):
  form = data['form']
  username = form['user']
  pwd = form['passwd']
  dbname = form['bbdd']
  sock_common = xmlrpclib.ServerProxy('%s/xmlrpc/common' % (form['server']))
  uid = sock_common.login(dbname, username, pwd)
  if uid:
    data['remote_uid'] = uid
    return 'import'
  else:
    return 'login_failed'


def _import(self, cr, uid, data, context={}):
  data['updated'] = 0
  data['added'] = 0
  form = data['form']
  sock = xmlrpclib.ServerProxy('%s/xmlrpc/object' % (form['server']))
  args = [('type', '!=', 'model'), ('src', '!=', 'False'), ('lang', '=', form['lang'])]
  count = sock.execute(form['bbdd'], data['remote_uid'], form['passwd'], 'ir.translation', 'search_count', args)
  ids = sock.execute(form['bbdd'], data['remote_uid'], form['passwd'], 'ir.translation', 'search', args)
  fields = ['src', 'name', 'value', 'lang', 'type', 'res_id']
  remote_data = sock.execute(form['bbdd'], data['remote_uid'], form['passwd'], 'ir.translation', 'read', ids, fields)
  # Comencem l'actualitzacio
  local = pooler.get_pool(cr.dbname).get('ir.translation')
  for trans in remote_data:
    lids = local.search(cr, uid, [('lang', '=', form['lang']), ('src', '=', trans['src']), ('name', '=', trans['name'])])
    if len(lids):
      local.write(cr, uid, lids, {'value': trans['value']})
      data['updated'] += 1
    else:
      del trans['id']
      local.create(cr, uid, trans)
      data['added'] += 1
  print '%i strings actualitzades' % (data['updated'])
  print '%i strings afegides' % (data['added'])
  return {}

def _result(self, cr, uid, data, context={}):
  return {'updated': data['updated'], 'added': data['added']}

_result_form = """<?xml version="1.0"?>
<form string="Resultat" col="2">
  <field name="updated" width="150" readonly="1" />
  <field name="added" width="150" readonly="1" />
</form>"""

_result_fields = {
  'updated': {'string': 'Actualitzades', 'type': 'integer'},
  'added': {'string': 'Afegides', 'type': 'integer'},
}

_login_failed_form = """<?xml version="1.0"?>
<form string="Error en el login" col="3">
  <image name="gtk-dialog-error" />
  <label colspan="2" string="Usuari/Contrassenya incorrectes" />
</form>"""

_login_failed_fields = {}


class wizard_server_translation(wizard.interface):

  states = {
    'init': {
    	'actions': [_init],
      'result': {'type': 'form', 'arch': _init_form,'fields': _init_fields, 'state':[('login', 'Continuar', 'gtk-go-forward')]}
    },
    'login': {
    	'actions': [],
    	'result': { 'type' : 'choice', 'next_state' : _login },
    },
    'import': {
      'actions': [_import],
      'result': {'type': 'state', 'state': 'result'},
    },
    'result': {
    	'actions': [_result],
      'result': {'type': 'form', 'arch': _result_form,'fields': _result_fields, 'state':[('end', 'Tancar', 'gtk-close')]}
    },
    'login_failed': {
    	'actions': [],
      'result': {'type': 'form', 'arch': _login_failed_form,'fields': _login_failed_fields, 'state':[('init', 'Reintantar', 'gtk-redo'),('end', 'Tancar', 'gtk-close')]}
    },
    'end': {
      'actions': [],
      'result': {'type': 'state', 'state': 'end'},
    }
  }

wizard_server_translation('import.server.translation')
