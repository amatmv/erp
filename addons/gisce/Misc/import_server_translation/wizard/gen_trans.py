# -*- coding: iso-8859-1 -*-
import wizard
import pooler
import tools


def _init(self, cr, uid, data, context={}):
  return {}

_init_form = """<?xml version="1.0"?>
<form string="Servidor" col="2">
  <field name="lang" required="1" />
</form>"""

_init_fields = {
  'lang': {'string': 'New Lang', 'type': 'char', 'size': 5},
}


def _generate(self, cr, uid, data, context={}):

  out = tools.trans_generate(data['form']['lang'], 'all', cr.dbname) 
  print out
  return {}


class wizard_server_translation_generation(wizard.interface):

  states = {
    'init': {
    	'actions': [_init],
      'result': {'type': 'form', 'arch': _init_form,'fields': _init_fields, 'state':[('generate', 'Continuar', 'gtk-go-forward')]}
    },
    'generate': {
      'actions': [_generate],
      'result': {'type': 'state', 'state': 'end'},
    },
    'end': {
      'actions': [],
      'result': {'type': 'state', 'state': 'end'},
    }
  }

wizard_server_translation_generation('server.translation.generation')
