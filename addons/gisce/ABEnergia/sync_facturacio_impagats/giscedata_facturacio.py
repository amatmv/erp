# -*- coding: utf-8 -*-
from osv import osv, fields


_AUDINFOR_STATE = [
    ('0', u'Sin carta de aviso'),
    ('1', u'Carta de aviso 1ª'),
    ('2', u'Carta de aviso 2ª'),
    ('C', u'Carta de corte'),
    ('CA', u'Cerrado anterior a 01/01/2012'),
    ('D', u'Bajo dudoso cobro'),
    ('J', u'Reclamación judicial'),
    ('O', u'Cortado'),
    ('P', u'Pagado'),
    ('X', u'Sistema anterior'),
    ]

class GiscedataFacturacioFactura(osv.osv):
    """Classe base de les factures
    """

    _name = 'giscedata.facturacio.factura'
    _inherit = 'giscedata.facturacio.factura'

    _columns = {
        'audinfor_state': fields.selection(_AUDINFOR_STATE, string='Estado Audinfor')
    }

    _defaults = {
        'audinfor_state': lambda *a: '0'
    }

GiscedataFacturacioFactura()