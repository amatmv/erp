# -*- coding: utf-8 -*-
from osv import osv, fields


_file_states = [
    ('new', 'Nuevo'),
    ('run', 'En marxa'),
    ('error', 'Error'),
    ('done', 'Procesado')
]
_line_states = [
    ('new', 'Nuevo'),
    ('error', 'Error'),
    ('done', 'Procesada'),
    ('skip', 'No importar')
]


class FileInvoiceState(osv.osv):

    _name = 'sync.file.invoice'
    _description = '''File which contains all lines to sync'''
    _order = "name desc"

    _columns = {
        'name': fields.datetime(u"Fecha de importación"),
        'directori': fields.char('Directorio', size=1024),
        'nom_fitxer': fields.char('Nombre del fichero', size=128, select=True),
        'lines': fields.one2many('sync.line.invoice', 'name', u'Líneas'),
        'total_lines': fields.integer("TOTAL"),
        'lines_ok': fields.integer("OK"),
        'lines_error': fields.integer("ERROR"),
        'state': fields.selection(_file_states, 'Estado', readonly=True, select=True),
        'status_message': fields.text('Mensaje de estado'),
    }

    _defaults = {
        'nom_fitxer': lambda *a: False,
        'state': lambda *a: 'new',
        'total_lines': lambda *a: False,
        'lines_ok': lambda *a: False,
        'lines_error': lambda *a: False,
    }

FileInvoiceState()


class LineInvoiceState(osv.osv):

    _name = 'sync.line.invoice'
    _description = '''lines to sync'''
    _order = "ordre desc"

    _columns = {
        'name': fields.many2one('sync.file.invoice', 'Nombre del fichero',
                                required=True, ondelete='cascade', select=1),
        'case_id': fields.many2one('crm.impagos', 'Impago', select=1),
        'ordre': fields.integer(u'Núm. línea'),
        'original_line': fields.text(u'Línea original'),
        'polissa_name': fields.char('Cliente', size=32),
        'partner_name': fields.char('Nombre', size=256),
        'city': fields.char('Poblacion', size=32),
        'invoice_number': fields.char('Factura', size=32),
        'invoice_date': fields.char('Fecha factura', size=32),
        'receipt': fields.char('Recibo', size=32),
        'unpayment_date': fields.char('Fecha impago', size=32),
        'invoice_state': fields.char('Estado factura', size=32),
        'state': fields.selection(_line_states, 'Estado', readonly=True,
                                   select=1),
        'status_message': fields.text('Mensaje de estado'),
    }

    _defaults = {
        'state': lambda *a: 'new',
    }

LineInvoiceState()