# -*- coding: utf-8 -*-
from osv import osv, fields


class CRMImpagos(osv.osv):
    _name = 'crm.impagos'
    _inherit = 'crm.impagos'

    _columns = {
        'lines': fields.one2many('sync.line.invoice', 'case_id', u'Impago')
    }

CRMImpagos()