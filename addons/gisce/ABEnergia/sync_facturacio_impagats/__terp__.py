# -*- coding: utf-8 -*-
{
    "name": "Actualització factures impagades",
    "description": """Estructures per la sincronització de factures impagades""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE_SOA",
    "depends":[
        "giscedata_facturacio_comer",
        "crm_elec_unpaid_invoice",
        "notifications",
        "sms_template"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/ir.model.access.csv",
        "giscedata_facturacio_view.xml",
        "crm_elec_unpaid_view.xml",
        "sms_template_data.xml",
        "email_template_data.xml",
        "sync_facturacio_impagats_view.xml"
    ],
    "active": False,
    "installable": True
}
