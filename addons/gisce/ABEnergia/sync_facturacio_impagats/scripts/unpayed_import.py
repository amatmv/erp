# -*- coding: utf-8 -*-
import csv
import multiprocessing
import os
import sys
from datetime import datetime
try:
    from cStringIO import StringIO
except:
    from StringIO import StringIO
import traceback

try:
    from raven import Client
except:
    Client = None
from progressbar import ProgressBar, ETA, Percentage, Bar


N_PROC = int(os.getenv('N_PROC', multiprocessing.cpu_count() + 1))
VERSION = '0.0.1'

AUDINFOR_STATES = ['0', '1', '2', 'C', 'D', 'J', 'P']

class MultiprocessBased(object):

    def __init__(self, **kwargs):
        self.file_output = kwargs.pop('output', False)
        self.connection = kwargs.pop('connection')
        self.num_proc = max(1, kwargs.pop('num_proc', N_PROC))
        self.content = None
        self.input_q = multiprocessing.JoinableQueue()
        self.output_q = multiprocessing.JoinableQueue()
        self.progress_q = multiprocessing.Queue()
        self.quiet = kwargs.pop('quiet', False)
        self.interactive = kwargs.pop('interactive', False)
        self.process_name = ''
        self.base_object = ''
        if 'SENTRY_DSN' in os.environ and Client:
            self.raven = Client()
            self.raven.tags_context({'version': VERSION})
        else:
            self.raven = None
        self.content = ''


    def get_sequence(self):
        raise NotImplementedError()

    def producer(self, sequence):
        """Posem els items que serviran per fer l'informe.
        """
        for item in sequence:
            self.input_q.put(item)

    def progress(self, total):
        """Rendering del progressbar de l'informe.
        """
        widgets = ['Informe %s: ' % self.process_name,
                   Percentage(), ' ', Bar(), ' ', ETA()]
        pbar = ProgressBar(widgets=widgets, maxval=total).start()
        done = 0
        while True:
            self.progress_q.get()
            done += 1
            pbar.update(done)
            if done >= total:
                pbar.finish()

    def writer(self):
        if self.file_output:
            fio = open(self.file_output, 'wb')
        else:
            fio = StringIO()
        fitxer = csv.writer(fio, delimiter=';', lineterminator='\n')
        while True:
            try:
                item = self.output_q.get()
                if item == 'STOP':
                    break
                msg = map(lambda x: type(x)==unicode and x.encode('utf-8') or x, item)
                fitxer.writerow(msg)
            except:
                traceback.print_exc()
                if self.raven:
                    self.raven.captureException()
            finally:
                self.output_q.task_done()

        if not self.file_output:
            self.content = fio.getvalue()
        fio.close()

    def consumer(self):
        raise NotImplementedError()

    def pre_calc(self):
        pass

    def post_calc(self):
        pass

    def execute(self):
        # Alias for calc
        self.calc()

    def calc(self):
        self.pre_calc()
        sequence = []
        sequence += self.get_sequence()
        if not self.quiet or self.interactive:
            sys.stderr.write("S'han trobat %s %s.\n" % (self.base_object, len(sequence)))
            sys.stderr.flush()
        if self.interactive:
            sys.stderr.write("Correcte? ")
            raw_input()
            sys.stderr.flush()
        start = datetime.now()
        processes = [multiprocessing.Process(target=self.consumer)
                     for _ in range(0, self.num_proc)]
        if not self.quiet:
            processes += [
                multiprocessing.Process(
                    target=self.progress, args=(len(sequence),)
                )
            ]
        processes.append(multiprocessing.Process(target=self.writer))
        self.producer(sequence)
        for proc in processes:
            proc.daemon = True
            proc.start()
            if not self.quiet:
                sys.stderr.write("^Starting process PID (%s): %s\n" %
                                 (proc.name, proc.pid))
        sys.stderr.flush()
        self.input_q.join()
        self.output_q.put('STOP')
        if not self.quiet:
            sys.stderr.write("Time Elapsed: %s\n" % (datetime.now() - start))
            sys.stderr.flush()
        self.output_q.join()
        self.output_q.close()
        self.input_q.close()
        self.post_calc()


class InvoiceImportExcpetion(Exception):
    pass

class InvoiceUnpaidFile(MultiprocessBased):

    def __init__(self, **kwargs):
        super(InvoiceUnpaidFile, self).__init__(**kwargs)
        self.file_input = kwargs.pop('file_input')
        self.file_invoice_id = False
        self.section_id = False
        self.audinfor_states = {}
        self.account_id = False
        self.journal_id = False


    def pre_calc(self):
        o = self.connection
        vals =  {
            'name': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            'directori': os.path.dirname(self.file_input),
            'nom_fitxer': os.path.basename(self.file_input)
        }
        self.file_invoice_id = o.SyncFileInvoice.create(vals)
        self.account_id = o.AccountAccount.search([('code', '=', '570000')])[0]
        self.journal_id = o.AccountJournal.search([('code', '=', 'CAJA')])[0]
        section_comer_id = o.CrmCaseSection.search(
            [('code', '=', 'IMP_COMER')])[0]
        section_juri_id = o.CrmCaseSection.search(
            [('code', '=', 'IMP_JURI')])[0]

        self.notification_category_id = o.NotificationCategory.search(
            [('code', '=', 'IMPAGOS')])[0]

        self.audinfor_states.update(
            {'0':
                 {'stage_id': o.CrmCaseStage.search(
                     [('section_ids', '=', section_comer_id),
                      ('name', '=', 'Factura impagada')])[0],
                  'case_state': 'open',
                  'section_id': section_comer_id,
                  'stage_name': 'Factura impagada',
                  'sms_template': 'sms_factura_impago',
                  'email_template': 'email_factura_impago',
                 }
            })
        self.audinfor_states.update(
            {'1':
                 {'stage_id': o.CrmCaseStage.search(
                     [('section_ids', '=', section_comer_id),
                      ('name', '=', '1a Carta de aviso mandada')])[0],
                  'case_state': 'open',
                  'section_id': section_comer_id,
                  'stage_name': '1a Carta de aviso mandada',
                  'sms_template': 'sms_1_carta_aviso',
                  'email_template': 'email_1_carta_aviso',
                 }
            })
        self.audinfor_states.update(
            {'2':
                 {'stage_id': o.CrmCaseStage.search(
                     [('section_ids', '=', section_comer_id),
                      ('name', '=', '2a Carta de aviso mandada')])[0],
                  'case_state': 'open',
                  'section_id': section_comer_id,
                  'sms_template': 'sms_2_carta_aviso',
                  'email_template': 'email_2_carta_aviso',
                 }
            })
        self.audinfor_states.update(
            {'C':
                 {'stage_id': o.CrmCaseStage.search(
                     [('section_ids', '=', section_comer_id),
                      ('name', '=', 'Carta de corte mandada')])[0],
                  'case_state': 'open',
                  'section_id': section_comer_id,
                  'stage_name': 'Carta de corte mandada',
                  'sms_template': 'sms_carta_corte',
                  'email_template': 'email_carta_corte',
                 }
            })
        self.audinfor_states.update(
            {'O':
                 {'stage_id': o.CrmCaseStage.search(
                     [('section_ids', '=', section_comer_id),
                      ('name', '=', 'Suministro Cortado')])[0],
                  'case_state': 'open',
                  'section_id': section_juri_id,
                  'stage_name': 'Suministro Cortado',
                  'sms_template': 'sms_suministro_cortado',
                  'email_template': 'email_suministro_cortado',
                 }
            })
        self.audinfor_states.update(
            {'D':
                 {'stage_id': o.CrmCaseStage.search(
                     [('section_ids', '=', section_juri_id),
                      ('name', '=', 'Dudoso Cobro')])[0],
                  'case_state': 'open',
                  'section_id': section_juri_id,
                  'stage_name': 'Dudoso Cobro',
                 }
            })
        self.audinfor_states.update(
            {'J':
                 {'stage_id': o.CrmCaseStage.search(
                     [('section_ids', '=', section_juri_id),
                      ('name', '=', u'Reclamación judicial')])[0],
                  'case_state': 'open',
                  'section_id': section_juri_id,
                  'stage_name': u'Reclamación judicial ',
                 }
            })
        self.audinfor_states.update(
            {'P':
                 {'stage_id': o.CrmCaseStage.search(
                     [('section_ids', '=', section_comer_id),
                      ('name', '=', 'Pagado')])[0],
                  'case_state': 'done',
                  'section_id': section_comer_id,
                  'stage_name': 'Pagado',
                  'sms_template': 'sms_factura_pagada',
                  'email_template': 'email_factura_pagada',
                 }
            })

    def post_calc(self):
        o = self.connection
        search_params = [
            ('name', '=', self.file_invoice_id),
            ('state', '=', 'error')]
        error_lines = o.SyncLineInvoice.search_count(search_params)
        search_params = [
            ('name', '=', self.file_invoice_id),
            ('state', '=', 'done')]
        done_lines = o.SyncLineInvoice.search_count(search_params)
        search_params = [
            ('name', '=', self.file_invoice_id)]
        total_lines = o.SyncLineInvoice.search_count(search_params)
        vals = {'lines_error': error_lines,
                'lines_ok': done_lines,
                'total_lines': total_lines}
        if error_lines != 0:
            vals.update({'state': 'error'})
        else:
            vals.update({'state': 'done'})
        o.SyncFileInvoice.write(
            [self.file_invoice_id], vals)

    def date_convert(self, date_string):
        return datetime.strptime(date_string,'%d/%m/%Y').strftime('%Y-%m-%d')

    def get_sequence(self):
        file_input = open(self.file_input)
        i = 0
        invoices = {}
        for line in file_input.readlines():
            line = line.strip().split(';')
            invoice_key = '%s_%s' % (line[3], line[0])
            if invoice_key not in invoices:
                invoices[invoice_key] = [[i]+line]
            else:
                invoices[invoice_key] += [[i]+line]
            i += 1
        sequence = [value for (key, value) in invoices.items()]
        file_input.close()
        self.connection.SyncFileInvoice.write(
            [self.file_invoice_id], {'state': 'run'})
        return sequence

    def get_polissa_id(self, vals_line):
        try:
            polissa_id = self.connection.GiscedataPolissa.search(
                [('name', '=', vals_line['polissa_name'])])[0]
            return polissa_id
        except:
            raise InvoiceImportExcpetion('No existe una póliza con este nombre')

    def get_data_form_polissa(self, polissa_id):
        try:
            data = self.connection.GiscedataPolissa.read(
                polissa_id, ['user_id', 'observacions',
                             'sms_notification', 'email_notification', 'cups'])
            return data
        except:
            raise InvoiceImportExcpetion(u'No existe datos para esta póliza')

    def get_factura_id(self, vals_line):
        try:
            o = self.connection
            factura_id = o.GiscedataFacturacioFactura.search(
                [('number', '=', vals_line['invoice_number'])])[0]
            return factura_id
        except:
            raise InvoiceImportExcpetion(
                'No existe una factura con este number')

    def get_data_from_invoice(self, factura_id):
        try:
            vals = {'partner_id': False, 'partner_address_id': False}
            data = self.connection.GiscedataFacturacioFactura.read(
                 factura_id, ['partner_id', 'address_contact_id']
            )
            if data:
                vals['partner_id'] = data['partner_id'][0]
                vals['partner_address_id'] = data['address_contact_id'][0]
            return vals
        except:
            raise InvoiceImportExcpetion('No existe datos para esta factura')

    def pay_and_reconcile(self, factura_id):
        o = self.connection
        invoice_id = o.GiscedataFacturacioFactura.read(
            factura_id, ['invoice_id'])['invoice_id'][0]
        invoice_data = o.AccountInvoice.read(
            invoice_id, ['amount_total', 'period_id'])
        o.AccountInvoice.pay_and_reconcile(
            [invoice_id], invoice_data['amount_total'], self.account_id,
            invoice_data['period_id'][0], self.journal_id,
            self.account_id, invoice_data['period_id'][0], self.journal_id)

    def unpay_invoice(self, factura_id):
        self.connection.GiscedataFacturacioFactura.undo_payment([factura_id])

    def prepare_sms_notification(self, lead_id, line):
        o = self.connection
        audinfor_state = line['invoice_state']
        sms_template = self.audinfor_states[audinfor_state].get(
            'sms_template', False)
        if sms_template:
            template_id = o.IrModelData._get_id(
                'sync_facturacio_impagats', sms_template)
            sms_vals = o.SmsTemplate.generate_sms(template_id[1], lead_id)
            sms_id = o.NotificationSms.create(sms_vals)
            o.Notification.create(
                {'type': 'notification.sms,%s' % sms_id,
                 'category_id': self.notification_category_id})

    def generate_email(self, template_id, res_id):
        #Busquem la plantilla de mail d'activació
        o = self.connection
        template = o.PoweremailTemplates.get(template_id)
        if template.enforce_from_account:
            mail_from = template.enforce_from_account.id
        else:
            message = (u"No se ha creado notificación porque no existe "
                      u"una cuenta valida para la línia de impago %s") % res_id
            raise Exception('InvalidMailAccount', message)

        ctx = {'active_ids': [res_id], 'active_id': res_id,
               'template_id': template_id, 'src_rec_ids': [res_id],
               'src_model': 'crm.impagos', 'from': mail_from,
               'state': 'single', 'priority': '0'}
        params = {'state': 'single', 'priority': '0', 'from': ctx['from']}
        pwswz_obj = o.PoweremailSendWizard
        pwswz_id = pwswz_obj.create(params, ctx)
        mails_id = pwswz_obj.save_to_mailbox([pwswz_id], ctx)
        o.PoweremailMailbox.write(mails_id, {'folder': 'drafts'})
        return mails_id[0]

    def prepare_mail_notification(self, lead_id, data_factura, user_id,
                                  line):
        o = self.connection
        audinfor_state = line['invoice_state']
        email_template = self.audinfor_states[audinfor_state].get(
            'email_template', False)
        if email_template:
            template_id = o.IrModelData.get_object_reference(
                'sync_facturacio_impagats', email_template)[1]
            mail_id = self.generate_email(template_id, lead_id)
            o.Notification.create({'type': 'poweremail.mailbox,%s' % mail_id,
                               'category_id': self.notification_category_id})

    def send_notification(self, lead_id, polissa_data, factura_id, line):
        if polissa_data.get('user_id', False):
            user_id = polissa_data['user_id'][0]
        else:
            user_id = False
        factura_data = self.get_data_from_invoice(factura_id)
        if polissa_data.get('email_notification', False):
            self.prepare_mail_notification(
                lead_id, factura_data, user_id, line)

        if polissa_data.get('sms_notification', False):
            self.prepare_sms_notification(lead_id, line)

    def add_historic_polissa(self, polissa_id, message):
        data_polissa = self.connection.GiscedataPolissa.read(
            polissa_id, ['observacions'])
        if data_polissa['observacions'] and data_polissa['observacions'].strip():
            observacions = '%s \n %s' % (
                data_polissa['observacions'], message)
        else:
            observacions = message
        self.connection.GiscedataPolissa.write(
            [polissa_id], {'observacions': observacions})

    def add_historic_lead(self, lead_id, message):
        case_id = self.connection.CrmImpagos.read(
            lead_id, ['case_id'])['case_id'][0]
        wiz = self.connection.CrmImpagos.message_append(case_id, message)

    def create_lead(self, vals_impago, vals_line, factura_id, polissa_data):
        vals_lead = {
            'name': 'Impago factura %s' % vals_line['invoice_number'],
            'section_id': self.audinfor_states[
                vals_line['invoice_state']]['section_id'],
            'stage_id': self.audinfor_states[
                vals_line['invoice_state']]['stage_id'],
            'type': 'opportunity',
            'ref': '%s,%s' % ('giscedata.polissa', polissa_data['id']),
            'ref2': '%s,%s' % (
                'giscedata.facturacio.factura',  factura_id),
            'ref3': '%s,%s' % (
                'giscedata.cups.ps',  polissa_data['cups'][0]),
            'state': self.audinfor_states[
                vals_line['invoice_state']]['case_state']
        }
        vals_lead.update(vals_impago)

        data_from_invoice = self.get_data_from_invoice(factura_id)
        vals_lead.update(data_from_invoice)
        if polissa_data:
            if polissa_data['user_id']:
                vals_lead.update({'user_id': polissa_data['user_id'][0]})
        lead_id = self.connection.CrmImpagos.create(vals_lead)
        return lead_id

    def update_factura(self, factura_id, line):
        self.connection.GiscedataFacturacioFactura.write(
            [factura_id], {'audinfor_state': line['invoice_state']})

    def update_lead(self, lead_id, vals_line):
        vals = {
            'stage_id': self.audinfor_states[
                vals_line['invoice_state']]['stage_id'],
            'state': self.audinfor_states[
                vals_line['invoice_state']]['case_state'],
            'section_id': self.audinfor_states[
                vals_line['invoice_state']]['section_id']
        }
        self.connection.CrmImpagos.write([lead_id], vals)

    def get_lead(self, factura_id, polissa_id):
        lead_id = self.connection.CrmImpagos.search(
            [('ref', '=', 'giscedata.polissa,%s' % polissa_id),
             ('ref2', '=', 'giscedata.facturacio.factura,%s' % factura_id)])
        if lead_id:
            return lead_id[0]
        return False

    def consumer(self):
        while True:
            try:
                item = self.input_q.get()
                o = self.connection
                self.progress_q.put(item)
                audinfor_valid_states = self.audinfor_states.keys()
                for values in sorted(item, key=lambda ordre: ordre[12].lower()):
                    line = {
                        'name': self.file_invoice_id,
                        'ordre': values[0],
                        'original_line': ';'.join(map(str,values)),
                        'polissa_name': values[1],
                        'partner_name': values[2],
                        'city': values[3],
                        'invoice_number': values[4],
                        'invoice_date': values[5],
                        'receipt': values[6],
                        'unpayment_date': values[11],
                        'invoice_state': values[12],
                    }
                    vals_impago = {
                        'factura': values[4],
                        'fecha_factura': self.date_convert(values[5]),
                        'fecha_impago': self.date_convert(values[11]),
                        'recibo': values[6].replace(',', '.'),
                        'nominal': values[7].replace(',', '.'),
                        'gastos': values[8].replace(',', '.'),
                        'total': values[9].replace(',', '.'),
                        'parcial': values[10].replace(',', '.'),
                        'tarifa': values[16],
                        'potencia': values[17].replace(',', '.'),
                    }
                    id_line = o.SyncLineInvoice.create(line)
                    if line['invoice_state'] not in audinfor_valid_states:
                        o.SyncLineInvoice.write(id_line, {'state': 'skip'})
                    else:
                        polissa_id = self.get_polissa_id(line)
                        polissa_data = self.get_data_form_polissa(polissa_id)

                        factura_id = self.get_factura_id(line)

                        lead_id = self.get_lead(factura_id, polissa_id)

                        if lead_id:
                            self.update_lead(lead_id, line)
                            if line['invoice_state'] == 'P':
                                self.pay_and_reconcile(factura_id)
                                self.add_historic_polissa(
                                        polissa_id,
                                        '%s - Pago factura %s' % (
                                            datetime.now().strftime('%d/%m/%Y'),
                                            line['invoice_number']
                                        )
                                )
                            else:
                                self.unpay_invoice(factura_id)
                        else:
                            if line['invoice_state'] != 'P':
                                if line['invoice_state'] in AUDINFOR_STATES:
                                    lead_id = self.create_lead(
                                        vals_impago, line, factura_id,
                                        polissa_data)
                                    self.unpay_invoice(factura_id)
                                    self.add_historic_polissa(
                                        polissa_id,
                                        '%s - Impago factura %s' % (
                                            datetime.now().strftime('%d/%m/%Y'),
                                            line['invoice_number']
                                        )
                                    )
                                    self.add_historic_lead(
                                        lead_id,
                                        'Etapa inicial: %s' % (
                                            self.audinfor_states[
                                                line['invoice_state']
                                            ]['stage_name']))
                            else:
                                raise InvoiceImportExcpetion(
                                    'Factura %s pagada sin caso previo' % line[
                                        'invoice_number'])

                        o.SyncLineInvoice.write(id_line, {'state': 'done',
                                                          'case_id': lead_id})
                        if lead_id:
                            self.update_factura(factura_id, line)
                            self.send_notification(
                                lead_id, polissa_data, factura_id,
                                line)

            except InvoiceImportExcpetion, e:
                o.SyncLineInvoice.write(
                    id_line,
                    {'state': 'error', 'status_message': str(e)})
            except Exception, e:
                traceback.print_exc()
                if self.raven:
                    self.raven.captureException()
                o.SyncLineInvoice.write(
                    id_line, {'state': 'error', 'status_message': str(e)})
            finally:
                self.input_q.task_done()


import click
from ooop import OOOP

@click.command()
@click.option('-q', '--quiet', default=False, help="No mostrar missatges de status per stderr")
@click.option('--interactive/--no-interactive', default=True, help="Deshabilitar el mode interactiu")
@click.option('-s', '--server', default='http://localhost',
              help=u'Adreça servidor ERP')
@click.option('-p', '--port', default=8069, help='Port servidor ERP', type=click.INT)
@click.option('-u', '--user', default='admin', help='Usuari servidor ERP')
@click.option('-w', '--password', default='admin',
              help='Contrasenya usuari ERP')
@click.option('-d', '--database', help='Nom de la base de dades')
@click.option('--num-proc', default=N_PROC, type=click.INT)
@click.option('-f',  '--file-input', type=click.Path(exists=True))
def import_invoice_unpaid(**kwargs):
    O = OOOP(dbname=kwargs['database'], user=kwargs['user'],
             pwd=kwargs['password'], port=kwargs['port'],
             uri=kwargs['server'])
    proc = InvoiceUnpaidFile(
        quiet=kwargs['quiet'],
        interactive=kwargs['interactive'],
        connection=O,
        num_proc=kwargs['num_proc'],
        file_input=kwargs['file_input']
    )
    proc.execute()

if __name__ == '__main__':
    import_invoice_unpaid()


