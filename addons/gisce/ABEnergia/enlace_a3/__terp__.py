# -*- coding: utf-8 -*-
{
  "name": "Enlace A3",
  "description": """Mòdul per generar fitxers enllac per al programa de comptabilitat
      a partir de fitxers normalitzats""",
  "version": "2.91.3",
  "author": "abenergia",
  "category": "",
  "depends": ['base',
              'account',
              'l10n_ES_partner',
              'l10n_ES_aeat_sii',
              ],
  "init_xml": [],
  "demo_xml": [],
  "update_xml": [
                 "enlace_codigo_view.xml",
                 "wizard/wizard_norma43_to_a3_view.xml",
                 "wizard/wizard_generar_fichero_enlace_a3_view.xml",
                 "security/ir.model.access.csv",
                 ],
  "active": False,
  "installable": True
}
