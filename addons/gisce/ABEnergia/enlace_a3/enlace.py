# -*- coding: utf-8 -*-

class EnlaceContable(object):

    def crear_cabecera_apunte_iva(self,
                                  amount,
                                  cuenta,
                                  empresa,
                                  contraparte,
                                  inv_type,
                                  fecha_fra,
                                  direccion,
                                  numeracion,
                                  ):

        if inv_type == u'Factura':
            tipo_registro = u'1'
        elif inv_type == u'Abono':
            tipo_registro = u'2'

        if direccion == u'proveedor':
            tipo_factura = u'2'
        elif direccion == u'cliente':
            tipo_factura = u'1'

        fecha_contable = fecha_fra.strftime(u'%Y%m%d')
        cuenta_format = cuenta.ljust(12, u'0')
        descr_cta = ''.rjust(30)
        numeracion = numeracion[-10:]
        numeracion = numeracion.rjust(10, ' ')
        contraparte = contraparte[:30]
        descripcion_apunte = contraparte.ljust(30)

        enter, decimal = str(amount).split('.')
        enter = enter.rjust(10, '0')
        decimal = decimal[:2].ljust(2, '0')
        reserva = ''.rjust(395)
        # reserva = ''.rjust(62)
        # nif = ''.rjust(14)
        # nombre = ''.rjust(40)
        # cp = ''.rjust(5)
        # reserva2 = ''.rjust(2)
        # fecha_oper = ''.rjust(8)
        # fecha_fra = ''.rjust(8)
        # num_ampliado = ''.rjust(60)
        # reserva3 = ''.rjust(196)

        linia = \
            u'5{0}{1}{2}{3}{4}{5}{6}I{7}+{8}.{9}{10}EN\r\n'.format(
                empresa,
                fecha_contable,
                tipo_registro,
                cuenta_format,
                descr_cta,
                tipo_factura,
                numeracion,
                descripcion_apunte,
                enter,
                decimal,
                reserva,
            )

        return linia

    def crear_detalle_apunte_iva(self,
                                 iva,
                                 amount,
                                 cuenta_gasto,
                                 empresa,
                                 contraparte,
                                 inv_type,
                                 fecha_fra,
                                 numeracion,
                                 ultima,
                                 ):

        if inv_type == u'Factura':
            tipo = u'C'
        elif inv_type == u'Abono':
            tipo = u'A'

        base = round(amount/(1 + iva), 2)
        cuota = round(amount - base, 2)
        iva = str(iva).split('.')[1]
        iva = iva.ljust(2, '0')
        porcentaje_iva = u"{0}.00".format(iva)

        fecha_contable = fecha_fra.strftime(u'%Y%m%d')
        cuenta_gasto = cuenta_gasto.ljust(12, '0')
        descr_cta = ''.rjust(30)
        numeracion = numeracion[-10:]
        numeracion = numeracion.rjust(10, ' ')
        contraparte = contraparte[:30]
        descr_apunte = contraparte.ljust(30)

        linea_apunte = u'M'
        if ultima:
            linea_apunte = u'U'

        subtipo = u'01'
        enter_base, decimal_base = str(base).split('.')
        enter_base = enter_base.rjust(10, '0')
        decimal_base = decimal_base[:2].ljust(2, '0')

        enter_cuota, decimal_cuota = str(cuota).split('.')
        enter_cuota = enter_cuota.rjust(10, '0')
        decimal_cuota = decimal_cuota[:2].ljust(2, '0')

        resto1 = ''.rjust(38)
        impreso = u'01'
        sujeta = u'S'
        resto2 = ''.rjust(333)

        linia = \
            u'5{0}{1}9{2}{3}{4}{5}{6}{7}{8}+{9}.{10}' \
            u'{11}+{12}.{13}{14}{15}{16}{17}EN\r\n'.format(
                empresa,
                fecha_contable,
                cuenta_gasto,
                descr_cta,
                tipo,
                numeracion,
                linea_apunte,
                descr_apunte,
                subtipo,
                enter_base,
                decimal_base,
                porcentaje_iva,
                enter_cuota,
                decimal_cuota,
                resto1,
                impreso,
                sujeta,
                resto2,
            )

        return linia

    def crear_apunte_iva(self,
                         iva,
                         amount,
                         cuenta_prov,
                         cuenta_gasto,
                         empresa,
                         contraparte,
                         inv_type,
                         fecha_fra,
                         direccion,
                         numeracion,
                         ):


        cabecera = self.crear_cabecera_apunte_iva(amount,
                                                  cuenta_prov,
                                                  empresa,
                                                  contraparte,
                                                  inv_type,
                                                  fecha_fra,
                                                  direccion,
                                                  numeracion,
                                                  )

        detalle = self.crear_detalle_apunte_iva(iva,
                                                amount,
                                                cuenta_gasto,
                                                empresa,
                                                contraparte,
                                                inv_type,
                                                fecha_fra,
                                                numeracion,
                                                u'U',
                                                )
        apunte = [cabecera, detalle]
        return apunte


    def crear_linea_apunte_sin_iva(self,
                                     amount,
                                     empresa,
                                     fecha_operacion,
                                     referencia,
                                     tipo,
                                     cta_origen,
                                     cta_destino):
        '''a tipo rebrem 'inicial' o 'ultima', segons si és
        línia inicial o línia final de l'apunte'''

        if tipo == u'inicial':
            tipo_importe = u'D' # deber/haber
            tipo_linea = u'I'
            cuenta = cta_origen
        else:
            tipo_importe = u'H' # deber/haber
            tipo_linea = u'U'
            cuenta = cta_destino

        constante_inicial = u'5'
        empresa = empresa
        fecha_contable = fecha_operacion.strftime(u'%Y%m%d')
        tipo_registro = u'0'                 # constante '0' en apuntes sin IVA
        cuenta_format = cuenta.ljust(12, '0')
        descr_cta = ''.rjust(30)
        ref_doc = ''.rjust(10)
        descripcion_apunte = referencia.ljust(30)
        importe = str(abs(amount)).rjust(13, '0')
        reserva = ''.rjust(395)

        linia_enllac = \
            u'{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}+{10}{11}EN\r\n'.format(
                constante_inicial,
                empresa,
                fecha_contable,
                tipo_registro,
                cuenta_format,
                descr_cta,
                tipo_importe,
                ref_doc,
                tipo_linea,
                descripcion_apunte,
                importe,
                reserva,
            )

        return linia_enllac

    def crear_apunte_sin_iva(self,
                             amount,
                             empresa,
                             fecha_operacion,
                             referencia,
                             cta_origen, cta_destino):
        '''Retorna 2 linies d'alta de apunte sin IVA pel fitxer d'enllac
        '''

        cta_deber = cta_origen
        cta_haber = cta_destino
        if amount < 0:
            cta_deber = cta_destino
            cta_haber = cta_origen

        linia_inicial = self.crear_linea_apunte_sin_iva(
            amount,
            empresa,
            fecha_operacion,
            referencia,
            u'inicial',
            cta_deber,
            cta_haber,
        )

        linia_final = self.crear_linea_apunte_sin_iva(
            amount,
            empresa,
            fecha_operacion,
            referencia,
            u'ultima',
            cta_deber,
            cta_haber,
        )

        apunte = u'{0}{1}'.format(linia_inicial, linia_final)

        return apunte
