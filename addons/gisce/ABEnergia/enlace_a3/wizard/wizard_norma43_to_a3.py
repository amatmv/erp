# -*- coding: utf-8 -*-

import base64
import StringIO
from osv import osv
from osv import fields

from retrofix import c43
from ..enlace import EnlaceContable

class WizardNorma43ToA3(osv.osv_memory):
    '''Wizard per pujar un fitxer norma43 i convertirlo al fitxer enllaç
    per carregar a l'A3'''

    _name = 'wizard.norma43_to_a3'

    _states_selection = [
        ('init', 'Init'),
        ('end', 'End'),
    ]

    def transformar(self, cursor, uid, ids,
                    parsed_c43,
                    codes,
                    empresa,
                    context=None):

        a3 = EnlaceContable()

        resum = {'total_registres_ppals': 0,
                 'total_registres_ppals_convertits': 0,
                 'per_dates': {},
                 }

        enlace = []

        for registre in parsed_c43:
            # mirar el tipus de record_code que és,
            # els registres que comencen per 22 són els apunts
            # els registres que comencen per 23 són detalls aclaratoris
            if registre.record_code == '22':
                resum['total_registres_ppals'] += 1
                concepte = registre.bank_concept_code

                if concepte in codes:
                    com = codes[concepte].get('com_visa', 0.0)
                    if com:
                        amount_brut = float(registre.amount) * (1 + com)
                        amount_brut = round(amount_brut, 2)
                        amount_com = amount_brut - float(registre.amount)
                        amount_com = round(amount_com, 2)
                        registre.__setattr__('amount', repr(amount_brut))

                    referencia = registre.reference_1 + registre.reference_2

                    if codes[concepte]['usar_descripcion']:
                        referencia = codes[concepte].get('descripcion', '')

                    cta_origen = codes[concepte]['cuentadeber']
                    cta_destino = codes[concepte]['cuentahaber']
                    transformada = a3.crear_apunte_sin_iva(
                        registre.amount,
                        empresa,
                        registre.operation_date,
                        referencia,
                        cta_origen,
                        cta_destino,
                    )

                    enlace.append(transformada)

                    if com:
                        registre.__setattr__('amount', repr(amount_com))

                        cta_origen = codes[concepte]['cuentadeber_com']
                        cta_destino = codes[concepte]['cuentahaber_com']
                        transformada = a3.crear_apunte_sin_iva(registre.amount,
                                                               empresa,
                                                               registre.operation_date,
                                                               referencia,
                                                               cta_origen,
                                                               cta_destino,
                                                               )
                        enlace.append(transformada)

                    resum['total_registres_ppals_convertits'] += 1
                    if registre.operation_date not in resum['per_dates']:
                        resum['per_dates'][registre.operation_date] = 0
                    resum['per_dates'][registre.operation_date] += 1

                else: # la resta de conceptes de moment no els tractem
                    pass
            else: # tot el que no siguin 22 no els tractem
                pass

        return enlace, resum

    def convertir(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        bank_obj = self.pool.get('res.bank')
        cod_obj = self.pool.get('enlace.contable.codigo')

        wizard = self.browse(cursor, uid, ids[0])

        fitxer = StringIO.StringIO(base64.b64decode(wizard.norma43_file))
        content = fitxer.read()
        parsed = c43.read(content)

        cabecera = parsed.pop()

        bank_ids = bank_obj.search(cursor, uid,
                                   [('code', '=', cabecera.bank_code)])

        bank = bank_obj.browse(cursor, uid, bank_ids[0])

        params = [('bank_id', '=', bank_ids[0]),
                  ('cuenta_empresa_id.name', '=', cabecera.account_number),
                  ]

        cod = {}
        cod_ids = cod_obj.search(cursor, uid, params)
        if not cod_ids:
            raise osv.except_osv(u"Error", u"No hi ha codis al banc "
                                           u"{0}".format(cabecera.bank_code))

        for cod_id in cod_ids:
            cod_br = cod_obj.browse(cursor, uid, cod_id)
            cod[cod_br.codigo_propio] = {'cuentadeber': cod_br.cuentadeber,
                                         'cuentahaber': cod_br.cuentahaber,
                                         'com_visa': cod_br.com_visa,
                                         'cuentadeber_com': cod_br.cuentadeber_com,
                                         'cuentahaber_com': cod_br.cuentahaber_com,
                                         'descripcion': cod_br.descripcion,
                                         'usar_descripcion': cod_br.usar_descripcion,
                                         }

        # generem el fitxer i l'escrivim al wizard
        empresa = cod_br.cuenta_empresa_id.empresa_id.name
        descr_empresa = cod_br.cuenta_empresa_id.empresa_id.descripcion

        fitxer_enllac, resum = self.transformar(cursor, uid, ids, parsed,
                                                cod, empresa, context)

        output = StringIO.StringIO()
        for linia in fitxer_enllac:
            output.write(str(linia))
        enlace_file = base64.b64encode(output.getvalue())
        output.close()

        dates = sorted(resum['per_dates'])
        num_reg_perdata = ''
        for data in dates:
            num_reg_perdata += '{0}: {1}\n'.format(data.strftime('%Y-%m-%d'),
                                                   resum['per_dates'][data])

        info = 'Entidad\t{0}\n' \
               'Oficina\t{1}\n' \
               'Cuenta\t{2}\n' \
               'Empresa\t{3}, {4}\n\n' \
               'Convertidos {5} registros de {6} registros principales\n' \
               'Fechas desde {7} hasta {8}\n\n' \
               'Numero de registros por fecha operacion:\n{9}'.format(
            bank.name,
            cabecera.bank_office,
            cabecera.account_number,
            empresa,
            descr_empresa,
            resum['total_registres_ppals_convertits'],
            resum['total_registres_ppals'],
            min(resum['per_dates']).strftime('%Y-%m-%d'),
            max(resum['per_dates']).strftime('%Y-%m-%d'),
            num_reg_perdata,
        )

        vals = {'info': info,
                'state': 'end',
                'enlace_file': enlace_file,
                }
        self.write(cursor, uid, ids, vals)

        return True

    _defaults = {
        'state': lambda *a: 'init',
    }

    _columns = {
        'state': fields.selection(_states_selection,
                                  u'Estat',
                                  size=64,
                                  readonly=True),
        'norma43_file': fields.binary(u'Fichero Norma43',
                                      required=True,
                                      filters=None),
        'enlace_file': fields.binary(u'Fichero enlace'),
        'info': fields.text(u'Info'),
    }

WizardNorma43ToA3()
