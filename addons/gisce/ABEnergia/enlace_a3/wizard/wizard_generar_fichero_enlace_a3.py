# -*- encoding: utf-8 -*-
import base64
from datetime import datetime
from unidecode import unidecode
from collections import defaultdict

from osv import osv, fields

from ..enlace import EnlaceContable

empresa = u'00324'
cuenta_gasto = '6000001100'

class WizardGenerarFicheroEnlaceA3Facturas(osv.osv_memory):
    _name = 'wizard.export.invoices.data.sii'
    _inherit = 'wizard.export.invoices.data.sii'

    def get_first_tax_from_fra(self, cursor, uid, type, invoice_number):
        '''busca el primer impost de la primera linia de factura que troba'''
        inv_obj = self.pool.get('account.invoice')
        line_obj = self.pool.get('account.invoice.line')
        params = [('number', '=', invoice_number)]
        if type == 'in':
            params = [('origin', '=', invoice_number)]
        inv_id = inv_obj.search(cursor, uid, params)
        line_ids = line_obj.search(cursor, uid, [('invoice_id', '=', inv_id)])
        line = line_obj.browse(cursor, uid, line_ids[0])
        amount_tax = line.invoice_line_tax_id[0].amount

        return amount_tax

    def gen_enlace_facturas(self, cursor, uid, results, numfra, tax,
                            cuenta_prov, direccion):

        a3 = EnlaceContable()

        enlace_lines = []

        for factura in results:
            fecha_fra = datetime.strptime(factura['fecha_factura'], '%Y-%m-%d')
            amount = factura['total_factura']
            contraparte = factura['contraparte']
            contraparte = unidecode(contraparte)
            inv_type = 'Factura'
            if amount < 0:
                inv_type = 'Abono'

            amount = abs(factura['total_factura'])
            numeracion = factura[numfra]

            apunte = a3.crear_apunte_iva(tax,
                                         amount,
                                         cuenta_prov,
                                         cuenta_gasto,
                                         empresa,
                                         contraparte,
                                         inv_type,
                                         fecha_fra,
                                         direccion,
                                         numeracion,
                                         )
            enlace_lines.extend(apunte)

        enlace = ''.join(enlace_lines)

        return enlace

    def gen_enlace_remesa(self, cursor, uid, results, tax,
                          cuenta_prov, direccion, remesa):
        a3 = EnlaceContable()

        data_remesa = datetime.strptime(results[0]['fecha_factura'], '%Y-%m-%d')
        contraparte = results[0]['contraparte']
        contraparte = unidecode(contraparte)

        total = 0.0
        for factura in results:
            total += factura['total_factura']

        inv_type = 'Factura'
        if total < 0:
            inv_type = 'Abono'

        lineas = a3.crear_apunte_iva(tax,
                                     total,
                                     cuenta_prov,
                                     cuenta_gasto,
                                     empresa,
                                     contraparte,
                                     inv_type,
                                     data_remesa,
                                     direccion,
                                     remesa,
                                     )
        enlace_line = ''.join(lineas)
        return enlace_line

    def can_gen_enlace(self, cursor, uid, ids, results, wizard, context):
        # Ha d'haver resultats, ha d'estar seleccionar un proveidor
        # si alguna de les factures dels resultats té remesa informada,
        # s'ha d'informar una remesa al wizard
        if wizard.invoices_type == u'out':
            raise osv.except_osv(u'Error generando enlace',
                                 u'No puede generarse con facturas de cliente.')
            return False
        if len(results) == 0:
            raise osv.except_osv(u'Error generando enlace',
                                 u'No hay resultados.')
            return False
        if not wizard.partner:
            raise osv.except_osv(u'Error generando enlace',
                                 u'No se ha seleccionado proveedor.')
            return False
        return True

    def export_invoices_data(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        results = super(WizardGenerarFicheroEnlaceA3Facturas, self).\
            export_invoices_data(cursor, uid, ids, context=context)

        # Nomes generem el fitxer enllac si esta marcada la opció
        # sempre que el puguem generar
        wizard = self.browse(cursor, uid, ids[0], context=context)
        if not wizard.gen_enlace:
            return False
        if not self.can_gen_enlace(cursor, uid, ids, results, wizard, context):
            return False

        if wizard.invoices_type == u'in':
            numfra = u'origen'
            direccion = u'proveedor'
        cuenta_prov = wizard.partner.property_account_payable.code

        pos = results[0]['posicion_fiscal']
        if 'endesa' in wizard.partner.name.lower() and 'canarias' in pos.lower():
            tax = 0.0
        else:
            # assumim que no mesclen productes amb diferents taxes
            tax = self.get_first_tax_from_fra(cursor, uid, wizard.invoices_type,
                                              results[0][numfra])

        remesas = defaultdict(list)
        for fra in results:
            r = str(fra.get('remesa'))
            if r != 'None':
                remesas[r].append(fra)

        # O bé van totes remesades o totes sense remesar, es segons proveidor
        # No hi ha el cas que puguin venir mesclades fres amb remesa i sense
        enlace = []
        if remesas:
            for rem in remesas:
                enlace_remesa = self.gen_enlace_remesa(cursor, uid,
                                                       remesas[rem],
                                                       tax,
                                                       cuenta_prov,
                                                       direccion,
                                                       rem,
                                                       )
                enlace.extend(enlace_remesa)

        else:
            enlace = self.gen_enlace_facturas(cursor, uid,
                                              results,
                                              numfra,
                                              tax,
                                              cuenta_prov,
                                              direccion,
                                              )
        enlace = ''.join(enlace)

        enlace_file_name = u'suenlace{:%Y-%d-%m_%H-%M-%S}.dat'.format(
            datetime.now(),
        )
        wizard.write({
            'file_enlace': base64.b64encode(enlace),
            'enlace_filename': enlace_file_name,
            'state': 'end',
        })

        return True

    _columns = {
        'gen_enlace': fields.boolean(u"Generar fichero enlace"),
        'file_enlace': fields.binary(string=u'Archivo enlace'),
        'enlace_filename': fields.char(u'Nombre archivo enlace', size=64),
    }

WizardGenerarFicheroEnlaceA3Facturas()
