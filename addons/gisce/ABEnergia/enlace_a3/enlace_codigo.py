# -*- coding: utf-8 -*-
from osv import osv, fields

class EnlaceContableEmpresa(osv.osv):

    _name = 'enlace.contable.empresa'

    _columns = {
        'name': fields.char('Empresa', size=5),
        'descripcion': fields.char('Descripcion', size=32),
    }

EnlaceContableEmpresa()

class EnlaceContableCuenta(osv.osv):

    _name = 'enlace.contable.cuenta'

    _columns = {
        'name': fields.char('Cuenta', size=10),
        'empresa_id': fields.many2one('enlace.contable.empresa', 'Empresa',
                                      required=True),
    }

EnlaceContableCuenta()

class EnlaceContableCodigo(osv.osv):

    _name = 'enlace.contable.codigo'

    _columns = {
        'codigo_propio': fields.char('Codigo', size=3),
        'descripcion': fields.char('Descripcion', size=30),
        'cuenta_empresa_id': fields.many2one(
            'enlace.contable.cuenta',
            'Cuenta',
            ),
        'empresa': fields.related('cuenta_empresa_id', 'empresa_id',
                                  'descripcion',
                                  type='char',
                                  string="Empresa",
                                  ),
        'cuentadeber': fields.char('Deber', size=20),
        'cuentahaber': fields.char('Haber', size=20),
        'bank_id': fields.many2one('res.bank', 'Bank',
                                   required=True
                                   ),
        'com_visa': fields.float('Comisión visa', digits=(12, 4)),
        'cuentadeber_com': fields.char('Deber Com.', size=20),
        'cuentahaber_com': fields.char('Haber Com.', size=20),
        'usar_descripcion': fields.boolean(
            'Usar descripción configurada'
        ),
    }

EnlaceContableCodigo()
