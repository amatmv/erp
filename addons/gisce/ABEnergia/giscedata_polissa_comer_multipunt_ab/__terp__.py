# -*- coding: utf-8 -*-
{
    "name": "Extension for polissa (ABEnergia)",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Adds a report for ABEnergia contract
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "c2c_webkit_report",
        "giscedata_polissa_comer_multipunt"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_polissa_comer_multipunt_ab_report.xml"
    ],
    "active": False,
    "installable": True
}
