<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<head>
		<style type="text/css">
	    ${css}
	    </style>
        <link rel="stylesheet" href="${addons_path}/giscedata_polissa_comer_multipunt_ab/report/base.css"/>
        <link rel="stylesheet" href="${addons_path}/giscedata_polissa_comer_multipunt_ab/report/multipunt.css"/>
	</head>
	<%def name="header_contracte(polissa_multi)">
		<div id="header">
		    <div id="logo">
		    	<img src="${addons_path}/giscedata_polissa_comer_multipunt_ab/report/abenergia-logo.png"/>
		    </div>
		    <br>
		    <div class="titol">
		    	<table class="taula_titol">
		    		<tr>
		    			<td id="titol_1">
		    				${_("Contrato de Suministro de Energía Eléctrica")}
		    			</td>
		    		</tr>
		    		<tr>
		    			<td id="titol_2">
		    				${_("Modalidad Multipunto")}
		    			</td>
		    		</tr>
		    	</table>
		    </div>
			<div style="clear:both"></div>
			<br>
		</div>
	</%def>
	<%def name="header_contracte_2(polissa_multi)">
		<i>
		<div id="header">
		    <div id="logo">Espai per el logo</div>
			<div style="clear:both"></div>
			<br>
		</div>
		</i>
	</%def>
    <%
        def clean_text(text):
            return text or ''
    %>
	<body>
		%for polissa_multi in objects:
            <%
                pool = polissa_multi.pool
                mandato_obj = pool.get('payment.mandate')
                reference = 'giscedata.polissa.multipunt,{0}'.format(
                                polissa_multi.id)
                mandato_id = mandato_obj.search(
                    cursor, uid, [
                        ('reference', '=', reference),
                        ('date_end', '=', False)
                    ],
                    0, 1, 'date desc')
                mandato = None
                if mandato_id:
                    mandato = mandato_obj.browse(cursor, uid, mandato_id[0])


            %>
        	${header_contracte(polissa_multi)}
        	<br>
        	<div class="seccio">
            	${_("DATOS DEL CLIENTE")}
                <br>
                <table style="margin-top: 5px;">
                    <tr>
                        <td class="label" style="width: 16%;" colspan="2">${_("Razón Social/Nombre:")}</td>
                        <td colspan="6">${polissa_multi.partner_id.name}</td>
                        <td class="label">${_("CIF/NIF")}</td>
                        <td colspan="2">${clean_text(polissa_multi.partner_id.vat and polissa_multi.partner_id.vat.replace('ES', '') or '')}</td>
                    </tr>
                    <tr>
                    	<td class="label" style="width: 9%;">${_("Dirección:")}</td>
                    	<td colspan="9">${polissa_multi.partner_address_id.street}</td>
                    </tr>
                    <tr>
                    	<td class="label">${_("Población:")}</td>
                    	<td colspan="4">${polissa_multi.partner_address_id.id_poblacio.name}</td>
                    	<td class="label" colspan="2">${_("Código Postal:")}</td>
                    	<td>${polissa_multi.partner_address_id.zip}</td>
                    	<td class="label">${_("Provincia:")}</td>
                    	<td colspan="2">${polissa_multi.partner_address_id.state_id.name}</td>
                    </tr>
                    <tr>
                    	<td class="label">${_("Teléfono/s:")}</td>
                    	<td colspan="3">${clean_text(polissa_multi.partner_address_id.phone)}</td>
                    	<td class="label">${_("Teléfono 2:")}</td>
                    	<td colspan="3">${clean_text(polissa_multi.partner_address_id.mobile)}</td>
                    	<td class="label">${_("Fax:")}</td>
                    	<td colspan="2">${clean_text(polissa_multi.partner_address_id.fax)}</td>
                    </tr>
                    <tr>
                    	<td class="label">${_("E-Mail:")}</td>
                    	<td colspan="6">${clean_text(polissa_multi.partner_address_id.email)}</td>
                    	<td class="label" style="width: 10%;">${_("E-Mail 2:")}</td>
                    	<td colspan="4"></td>
                    </tr>
                </table>
            </div>
            <br>
            <div class="seccio">
            	${_("DATOS DE NOTIFICACIONES Y ENVÍO DE COMUNICACIÓN")}
            	<br>
            	<table style="margin-top: 5px;">
                    <tr>
                        <td class="label" style="width: 16%;" colspan="2">${_("Persona de Contacto:")}</td>
                        <td colspan="9">${polissa_multi.contact_address_id.name}</td>
                    </tr>
                    <tr>
                    	<td class="label" style="width: 9%;">${_("Dirección:")}</td>
                    	<td colspan="9">${polissa_multi.contact_address_id.street}</td>
                    </tr>
                    <tr>
                    	<td class="label">${_("Población:")}</td>
                    	<td colspan="4">${polissa_multi.contact_address_id.id_poblacio.name}</td>
                    	<td class="label" colspan="2">${_("Código Postal:")}</td>
                    	<td>${polissa_multi.contact_address_id.zip}</td>
                    	<td class="label">${_("Provincia:")}</td>
                    	<td colspan="2">${polissa_multi.contact_address_id.state_id.name}</td>
                    </tr>
                    <tr>
                    	<td class="label">${_("Teléfono/s:")}</td>
                    	<td colspan="3">${clean_text(polissa_multi.contact_address_id.phone)}</td>
                    	<td class="label">${_("Teléfono 2:")}</td>
                    	<td colspan="3">${clean_text(polissa_multi.contact_address_id.mobile)}</td>
                    	<td class="label">${_("Fax:")}</td>
                    	<td colspan="2">${clean_text(polissa_multi.contact_address_id.fax)}</td>
                    </tr>
                    <tr>
                    	<td class="label">${_("E-Mail:")}</td>
                    	<td colspan="6">${clean_text(polissa_multi.contact_address_id.email)}</td>
                    	<td class="label" style="width: 10%;">${_("E-Mail 2:")}</td>
                    	<td colspan="4"></td>
                    </tr>
                </table>
            </div>
            <br>
            <div class="seccio">
            	${_("CONDICIONES ECONÓMICAS")}
            	<br>
            	<div id="contenidor_energia">
            		<div id="preu_energia">
            			${_("PRECIO DE LA ENERGÍA*")}
            			<br>
            			<table class="energia">
            				<tr>
            					<td class="label" style="text-align: left;">${_("Tarifa")}</td>
            					<td class="label">${_("P1")}</td>
            					<td class="label">${_("P2")}</td>
            					<td class="label">${_("P3")}</td>
            					<td class="invisible"></td>
            				</tr>
            				<tr>
            					<td class="label" style="text-align: left;">${_("2.0A")}</td>
            					<td>0,129458</td>
            					<td class="invisible"></td>
            					<td class="invisible"></td>
            					<td>${_("€/kWh")}</td>
            				</tr>
            				<tr>
            					<td class="label" style="text-align: left;">${_("2.0DHA")}</td>
            					<td>0,15217</td>
            					<td>0,064356</td>
            					<td class="invisible"></td>
            					<td>${_("€/kWh")}</td>
            				</tr>
            				<tr>
            					<td class="label" style="text-align: left;">${_("2.1A")}</td>
            					<td>0,142791</td>
            					<td class="invisible"></td>
            					<td class="invisible"></td>
            					<td>${_("€/kWh")}</td>
            				</tr>
            				<tr>
            					<td class="label" style="text-align: left;">${_("2.1DHA")}</td>
            					<td>0,164726</td>
            					<td>0,075333</td>
            					<td class="invisible"></td>
            					<td>${_("€/kWh")}</td>
            				</tr>
            				<tr>
            					<td class="label" style="text-align: left;">${_("3.0A")}</td>
            					<td>0,01181</td>
            					<td>0,098141</td>
            					<td>0,064983</td>
            					<td>${_("€/kWh")}</td>
            				</tr>
            			</table>
            			<div class="nota">
            				${_("*Los precios incluyen todos los términos regulados excepto el IVA y el IEE")}
            			</div>
            		</div>
            		<div id="preu_potencia">
            			${_("PRECIO DE LA POTENCIA**")}
            			<br>
            			<table class="energia">
            				<tr>
            					<td class="label" style="text-align: left;">${_("Tarifa")}</td>
            					<td class="label">${_("P1")}</td>
            					<td class="label">${_("P2")}</td>
            					<td class="label">${_("P3")}</td>
            					<td class="invisible"></td>
            				</tr>
            				<tr>
            					<td class="label" style="text-align: left;">${_("2.0A")}</td>
            					<td>42,0434</td>
            					<td class="invisible"></td>
            					<td class="invisible"></td>
            					<td>${_("€/kWh y Año")}</td>
            				</tr>
            				<tr>
            					<td class="label" style="text-align: left;">${_("2.0DHA")}</td>
            					<td>42,0434</td>
            					<td></td>
            					<td class="invisible"></td>
            					<td>${_("€/kWh y Año")}</td>
            				</tr>
            				<tr>
            					<td class="label" style="text-align: left;">${_("2.1A")}</td>
            					<td>44,4447</td>
            					<td class="invisible"></td>
            					<td class="invisible"></td>
            					<td>${_("€/kWh y Año")}</td>
            				</tr>
            				<tr>
            					<td class="label" style="text-align: left;">${_("2.1DHA")}</td>
            					<td>44,4447</td>
            					<td></td>
            					<td class="invisible"></td>
            					<td>${_("€/kWh y Año")}</td>
            				</tr>
            				<tr>
            					<td class="label" style="text-align: left;">${_("3.0A")}</td>
            					<td>40,7289</td>
            					<td>24,4373</td>
            					<td>16,2916</td>
            					<td>${_("€/kWh y Año")}</td>
            				</tr>
            			</table>
            			<div class="nota">
            				${_("**Los precios no incluyen el IEE ni IVA")}
            			</div>
            		</div>
            	</div>
            </div>
            <div style="clear:both"></div>
            <br>
        	<div class="seccio">
    			${_("DATOS DE LOS PUNTOS DE SUMINISTRO")}
    			<br>
    			<div class="nota" style="margin-top: 5px;">
    				${_("La relación de Puntos de Suministro, junto con los datos técnicos e identificativos de cada uno de ellos se describen en el anexo adjunto a este contrato.")}
    				<br>
                    ${_("La fecha de inicio del contrato de suministro será notificada a Ab energía 1903, S.L.U. por la empresa distribuidora.")}
    			</div>
    		</div>
    		<br>
    		<div class="seccio">
    			${_("DATOS BANCARIOS")}
    			<br>
    			<table style="margin-top: 5px;">
    				<tr>
    					<td class="label" colspan="2" >${_("Titular de la Cuenta:")}</td>
    					<td colspan="2">${polissa_multi.bank_id.owner_name}</td>
    					<td class="label">${_("CIF/NIF:")}</td>
    					<td >${clean_text(polissa_multi.bank_id.owner_id.vat and polissa_multi.bank_id.owner_id.vat.replace('ES', '') or '')}</td>
    				</tr>
    				<tr>
    					<td class="label" colspan="2">${_("IBAN:")}</td>
    					<td colspan="4">${polissa_multi.bank_id.printable_iban}</td>
    				</tr>
    				<tr>
    					<td class="label" colspan="2">${_("Código Mandato:")}</td>
    					<td>
                            %if mandato:
                                ${mandato.name}
                            %endif
                        </td>
    					<td class="label">${_("BIC Code:")}</td>
    					<td colspan="2">${polissa_multi.bank_id.bank.bic}</td>
    				</tr>
    			</table>
    		</div>
    		<br>
    		<div>
    			${_("AUTORIZACIÓN DE DOMICILIACIÓN BANCARIA")}
    			<br>
    			<div class="nota" style="margin-top: 5px;">
                    ${_("Por la presente autorizo a Ab energía 1903, S.L.U. para que pueda efectuar el cargo de sus facturas correspondientes al Suministro de energía eléctrica, en la domiciliación arriba indicada.")}
    			</div>
    		</div>
    		<br>
    		<div>
    			${_("AUTORIZACIÓN A LA DISTRIBUIDORA")}
    			<br>
    			<div class="nota" style="margin-top: 5px;">
                    ${_("Autorizo Expresamente a Ab energía 1903, S.L.U., (Inscrita en el Reg. Mercantil de Huesca Tomo: 614. Libro: 0, Folio: 47. Sección: 8, Hoja HU-10282, Inscripción: 23 - CIF: B22350466. Domicilio Social en Escuelas Pías 12, 22300 Barbastro e inscrita en el Registro Administrativo de Distribuidores, Comercializadores y Consumidores Directos en Mercado, con el número de identificación R2-366.) para que actúe como sustituto de esta sociedad a la hora de contratar el acceso a las redes con la empresa distribuidora, de tal forma que la posición de Ab energía 1903, S.L.U. en el contrato de acceso suscrito sea a todos los efectos la de esta sociedad a la que represento.")}
    				<br><br>
    				${_("Y para que quede constancia de ello, firmo el presente escrito, en Barbastro a 10 del Agosto de 2014")}
    			</div>
    		</div>
    		<br>
    		<div style="clear:both"></div>
            <div class="signatura">
                <div style="position:absolute; top: 0px; min-width:100%;"><b>${_("Ayuntamiento de Daganzo de Arriba")}</b></div>
                <div style="position:absolute; bottom: 0px; min-width:100%;"><b>Fdo: Ana María Arranz Rosco<br>Concejal Delegada de Economía y Hacienda</b></div>
            </div>
            <div class="signatura">
                <div style="position:absolute; top: 0px; min-width:100%;"><b>${_("Ab energía 1903, S.L.U.")}</b></div>
                <img class="firma" src="${addons_path}/giscedata_polissa_comer_multipunt_ab/report/firmaDireccion.png"/>
                <div style="position:absolute; bottom: 0px; min-width:100%;"><b>Fdo.- Pablo Bravo Vidal</b></div>
            </div>
        %endfor
	</body>
</html>