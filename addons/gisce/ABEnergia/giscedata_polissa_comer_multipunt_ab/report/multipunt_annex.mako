<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<head>
		<style type="text/css">
	    ${css}

	    </style>
        <link rel="stylesheet" href="${addons_path}/giscedata_polissa_comer_multipunt_ab/report/base.css"/>
        <link rel="stylesheet" href="${addons_path}/giscedata_polissa_comer_multipunt_ab/report/multipunt_annex.css"/>
    </head>
	<%def name="header_contracte(case)">
		<div id="header">
		    <div id="logo">
		    	<img src="${addons_path}/giscedata_polissa_comer_multipunt_ab/report/abenergia-logo.png"/>
		    </div>
		    <br>
		    <div class="titol">
		    	${_("ANEXO A LAS CONDICIONES PARTICULARES DEL CONTRATO DE SUMINISTRO ENERGIA ELECTRICA")}
		    </div>
			<div style="clear:both"></div>
			<br>
		</div>
	</%def>
	<%def name="nose()">
		?????
	</%def>
	<body>
		%for case in objects:
        	${header_contracte(case)}
            <table>
                <tr>
                    <td class="label">${_("Nombre/Razón Social:")}</td>
                    <td class="label">AYUNTAMIENTO DE DAGANZO</td>
                    <td class="label">${_("NIF-CIF:")}</td>
                    <td>P-2805300-G</td>
                </tr>
            </table>
            <br>
            <table style="margin-top: 5px;">
                <thead>
                    <tr>
                        <th rowspan="2" class="table_title">${_("Titular Suministro")}</th>
                        <th rowspan="2" class="table_title">${_("CUPS")}</th>
                        <th rowspan="2" class="table_title">${_("Tarifa")}</th>
                        <th class="table_title" colspan="3">${_("Potencia Contratada (kW)")}</th>
                        <th rowspan="2" class="table_title">${_("Domicilio Suministro")}</th>
                        <th rowspan="2" class="table_title">${_("Código Postal")}</th>
                        <th rowspan="2" class="table_title">${_("Población")}</th>
                        <th rowspan="2" class="table_title">${_("Província")}</th>
                        <th rowspan="2" class="table_title">${_("E. Distribuidora")}</th>
                        <th class="table_title" colspan="4">${_("Consumo Anual Estimado (kWh)")}</th>
                        <th rowspan="2" class="table_title">${_("Descripción Cliente")}</th>
                    </tr>
                    <tr>
                        <th class="table_title">${_("P1")}</th>
                        <th class="table_title">${_("P2")}</th>
                        <th class="table_title">${_("P3")}</th>
                        <th class="table_title">${_("P1")}</th>
                        <th class="table_title">${_("P2")}</th>
                        <th class="table_title">${_("P3")}</th>
                        <th class="table_title">${_("Total")}</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="field">AYUNTAMIENTO DE DAGANZO</td>
                        <td class="field">ES00250145897455320014ES</td>
                        <td class="field">2.0A</td>
                        <td class="field">7.700</td>
                        <td class="field"></td>
                        <td class="field"></td>
                        <td class="field">C/ SAN JUAN 17, Bajo</td>
                        <td class="field">28814</td>
                        <td class="field">Daganzo</td>
                        <td class="field">Madrid</td>
                        <td class="field">Iberdrola Distribución</td>
                        <td class="field">19.362</td>
                        <td class="field">-</td>
                        <td class="field">-</td>
                        <td class="field">19.362</td>
                        <td class="field">Alumbrado Público</td>
                    </tr>
                    <tr>
                        <td class="field">AYUNTAMIENTO DE DAGANZO</td>
                        <td class="field">ES00250145897455320014ES</td>
                        <td class="field">2.0A</td>
                        <td class="field">7.700</td>
                        <td class="field"></td>
                        <td class="field"></td>
                        <td class="field">C/ SAN JUAN 17, Bajo</td>
                        <td class="field">28814</td>
                        <td class="field">Daganzo</td>
                        <td class="field">Madrid</td>
                        <td class="field">Iberdrola Distribución</td>
                        <td class="field">19.362</td>
                        <td class="field">-</td>
                        <td class="field">-</td>
                        <td class="field">19.362</td>
                        <td class="field">Alumbrado Público</td>
                    </tr>
                    <tr>
                        <td class="field">AYUNTAMIENTO DE DAGANZO</td>
                        <td class="field">ES00250145897455320014ES</td>
                        <td class="field">2.0A</td>
                        <td class="field">7.700</td>
                        <td class="field"></td>
                        <td class="field"></td>
                        <td class="field">C/ SAN JUAN 17, Bajo</td>
                        <td class="field">28814</td>
                        <td class="field">Daganzo</td>
                        <td class="field">Madrid</td>
                        <td class="field">Iberdrola Distribución</td>
                        <td class="field">19.362</td>
                        <td class="field">-</td>
                        <td class="field">-</td>
                        <td class="field">19.362</td>
                        <td class="field">Alumbrado Público</td>
                    </tr>
                    <tr>
                        <td class="field">AYUNTAMIENTO DE DAGANZO</td>
                        <td class="field">ES00250145897455320014ES</td>
                        <td class="field">2.0A</td>
                        <td class="field">7.700</td>
                        <td class="field"></td>
                        <td class="field"></td>
                        <td class="field">C/ SAN JUAN 17, Bajo</td>
                        <td class="field">28814</td>
                        <td class="field">Daganzo</td>
                        <td class="field">Madrid</td>
                        <td class="field">Iberdrola Distribución</td>
                        <td class="field">19.362</td>
                        <td class="field">-</td>
                        <td class="field">-</td>
                        <td class="field">19.362</td>
                        <td class="field">Alumbrado Público</td>
                    </tr>
                    <tr>
                        <td class="field">AYUNTAMIENTO DE DAGANZO</td>
                        <td class="field">ES00250145897455320014ES</td>
                        <td class="field">2.0A</td>
                        <td class="field">7.700</td>
                        <td class="field"></td>
                        <td class="field"></td>
                        <td class="field">C/ SAN JUAN 17, Bajo</td>
                        <td class="field">28814</td>
                        <td class="field">Daganzo</td>
                        <td class="field">Madrid</td>
                        <td class="field">Iberdrola Distribución</td>
                        <td class="field">19.362</td>
                        <td class="field">-</td>
                        <td class="field">-</td>
                        <td class="field">19.362</td>
                        <td class="field">Alumbrado Público</td>
                    </tr>
                    <tr>
                        <td class="field">AYUNTAMIENTO DE DAGANZO</td>
                        <td class="field">ES00250145897455320014ES</td>
                        <td class="field">2.0A</td>
                        <td class="field">7.700</td>
                        <td class="field"></td>
                        <td class="field"></td>
                        <td class="field">C/ SAN JUAN 17, Bajo</td>
                        <td class="field">28814</td>
                        <td class="field">Daganzo</td>
                        <td class="field">Madrid</td>
                        <td class="field">Iberdrola Distribución</td>
                        <td class="field">19.362</td>
                        <td class="field">-</td>
                        <td class="field">-</td>
                        <td class="field">19.362</td>
                        <td class="field">Alumbrado Público</td>
                    </tr>
                    <tr>
                        <td class="field">AYUNTAMIENTO DE DAGANZO</td>
                        <td class="field">ES00250145897455320014ES</td>
                        <td class="field">2.0A</td>
                        <td class="field">7.700</td>
                        <td class="field"></td>
                        <td class="field"></td>
                        <td class="field">C/ SAN JUAN 17, Bajo</td>
                        <td class="field">28814</td>
                        <td class="field">Daganzo</td>
                        <td class="field">Madrid</td>
                        <td class="field">Iberdrola Distribución</td>
                        <td class="field">19.362</td>
                        <td class="field">-</td>
                        <td class="field">-</td>
                        <td class="field">19.362</td>
                        <td class="field">Alumbrado Público</td>
                    </tr>
                    <tr>
                        <td class="field">AYUNTAMIENTO DE DAGANZO</td>
                        <td class="field">ES00250145897455320014ES</td>
                        <td class="field">2.0A</td>
                        <td class="field">7.700</td>
                        <td class="field"></td>
                        <td class="field"></td>
                        <td class="field">C/ SAN JUAN 17, Bajo</td>
                        <td class="field">28814</td>
                        <td class="field">Daganzo</td>
                        <td class="field">Madrid</td>
                        <td class="field">Iberdrola Distribución</td>
                        <td class="field">19.362</td>
                        <td class="field">-</td>
                        <td class="field">-</td>
                        <td class="field">19.362</td>
                        <td class="field">Alumbrado Público</td>
                    </tr>
                    <tr>
                        <td class="field">AYUNTAMIENTO DE DAGANZO</td>
                        <td class="field">ES00250145897455320014ES</td>
                        <td class="field">2.0A</td>
                        <td class="field">7.700</td>
                        <td class="field"></td>
                        <td class="field"></td>
                        <td class="field">C/ SAN JUAN 17, Bajo</td>
                        <td class="field">28814</td>
                        <td class="field">Daganzo</td>
                        <td class="field">Madrid</td>
                        <td class="field">Iberdrola Distribución</td>
                        <td class="field">19.362</td>
                        <td class="field">-</td>
                        <td class="field">-</td>
                        <td class="field">19.362</td>
                        <td class="field">Alumbrado Público</td>
                    </tr>
                    <tr>
                        <td class="field">AYUNTAMIENTO DE DAGANZO</td>
                        <td class="field">ES00250145897455320014ES</td>
                        <td class="field">2.0A</td>
                        <td class="field">7.700</td>
                        <td class="field"></td>
                        <td class="field"></td>
                        <td class="field">C/ SAN JUAN 17, Bajo</td>
                        <td class="field">28814</td>
                        <td class="field">Daganzo</td>
                        <td class="field">Madrid</td>
                        <td class="field">Iberdrola Distribución</td>
                        <td class="field">19.362</td>
                        <td class="field">-</td>
                        <td class="field">-</td>
                        <td class="field">19.362</td>
                        <td class="field">Alumbrado Público</td>
                    </tr>
                    <tr>
                        <td class="field">AYUNTAMIENTO DE DAGANZO</td>
                        <td class="field">ES00250145897455320014ES</td>
                        <td class="field">2.0A</td>
                        <td class="field">7.700</td>
                        <td class="field"></td>
                        <td class="field"></td>
                        <td class="field">C/ SAN JUAN 17, Bajo</td>
                        <td class="field">28814</td>
                        <td class="field">Daganzo</td>
                        <td class="field">Madrid</td>
                        <td class="field">Iberdrola Distribución</td>
                        <td class="field">19.362</td>
                        <td class="field">-</td>
                        <td class="field">-</td>
                        <td class="field">19.362</td>
                        <td class="field">Alumbrado Público</td>
                    </tr>
                    <tr>
                        <td class="field">AYUNTAMIENTO DE DAGANZO</td>
                        <td class="field">ES00250145897455320014ES</td>
                        <td class="field">2.0A</td>
                        <td class="field">7.700</td>
                        <td class="field"></td>
                        <td class="field"></td>
                        <td class="field">C/ SAN JUAN 17, Bajo</td>
                        <td class="field">28814</td>
                        <td class="field">Daganzo</td>
                        <td class="field">Madrid</td>
                        <td class="field">Iberdrola Distribución</td>
                        <td class="field">19.362</td>
                        <td class="field">-</td>
                        <td class="field">-</td>
                        <td class="field">19.362</td>
                        <td class="field">Alumbrado Público</td>
                    </tr>
                    <tr>
                        <td class="field">AYUNTAMIENTO DE DAGANZO</td>
                        <td class="field">ES00250145897455320014ES</td>
                        <td class="field">2.0A</td>
                        <td class="field">7.700</td>
                        <td class="field"></td>
                        <td class="field"></td>
                        <td class="field">C/ SAN JUAN 17, Bajo</td>
                        <td class="field">28814</td>
                        <td class="field">Daganzo</td>
                        <td class="field">Madrid</td>
                        <td class="field">Iberdrola Distribución</td>
                        <td class="field">19.362</td>
                        <td class="field">-</td>
                        <td class="field">-</td>
                        <td class="field">19.362</td>
                        <td class="field">Alumbrado Público</td>
                    </tr>
                    <tr>
                        <td class="field">AYUNTAMIENTO DE DAGANZO</td>
                        <td class="field">ES00250145897455320014ES</td>
                        <td class="field">2.0A</td>
                        <td class="field">7.700</td>
                        <td class="field"></td>
                        <td class="field"></td>
                        <td class="field">C/ SAN JUAN 17, Bajo</td>
                        <td class="field">28814</td>
                        <td class="field">Daganzo</td>
                        <td class="field">Madrid</td>
                        <td class="field">Iberdrola Distribución</td>
                        <td class="field">19.362</td>
                        <td class="field">-</td>
                        <td class="field">-</td>
                        <td class="field">19.362</td>
                        <td class="field">Alumbrado Público</td>
                    </tr>
                    <tr>
                        <td class="field">AYUNTAMIENTO DE DAGANZO</td>
                        <td class="field">ES00250145897455320014ES</td>
                        <td class="field">2.0A</td>
                        <td class="field">7.700</td>
                        <td class="field"></td>
                        <td class="field"></td>
                        <td class="field">C/ SAN JUAN 17, Bajo</td>
                        <td class="field">28814</td>
                        <td class="field">Daganzo</td>
                        <td class="field">Madrid</td>
                        <td class="field">Iberdrola Distribución</td>
                        <td class="field">19.362</td>
                        <td class="field">-</td>
                        <td class="field">-</td>
                        <td class="field">19.362</td>
                        <td class="field">Alumbrado Público</td>
                    </tr>
                    <tr>
                        <td class="field">AYUNTAMIENTO DE DAGANZO</td>
                        <td class="field">ES00250145897455320014ES</td>
                        <td class="field">2.0A</td>
                        <td class="field">7.700</td>
                        <td class="field"></td>
                        <td class="field"></td>
                        <td class="field">C/ SAN JUAN 17, Bajo</td>
                        <td class="field">28814</td>
                        <td class="field">Daganzo</td>
                        <td class="field">Madrid</td>
                        <td class="field">Iberdrola Distribución</td>
                        <td class="field">19.362</td>
                        <td class="field">-</td>
                        <td class="field">-</td>
                        <td class="field">19.362</td>
                        <td class="field">Alumbrado Público</td>
                    </tr>
                    <tr>
                        <td class="field">AYUNTAMIENTO DE DAGANZO</td>
                        <td class="field">ES00250145897455320014ES</td>
                        <td class="field">2.0A</td>
                        <td class="field">7.700</td>
                        <td class="field"></td>
                        <td class="field"></td>
                        <td class="field">C/ SAN JUAN 17, Bajo</td>
                        <td class="field">28814</td>
                        <td class="field">Daganzo</td>
                        <td class="field">Madrid</td>
                        <td class="field">Iberdrola Distribución</td>
                        <td class="field">19.362</td>
                        <td class="field">-</td>
                        <td class="field">-</td>
                        <td class="field">19.362</td>
                        <td class="field">Alumbrado Público</td>
                    </tr>
                    <tr>
                        <td class="field">AYUNTAMIENTO DE DAGANZO</td>
                        <td class="field">ES00250145897455320014ES</td>
                        <td class="field">2.0A</td>
                        <td class="field">7.700</td>
                        <td class="field"></td>
                        <td class="field"></td>
                        <td class="field">C/ SAN JUAN 17, Bajo</td>
                        <td class="field">28814</td>
                        <td class="field">Daganzo</td>
                        <td class="field">Madrid</td>
                        <td class="field">Iberdrola Distribución</td>
                        <td class="field">19.362</td>
                        <td class="field">-</td>
                        <td class="field">-</td>
                        <td class="field">19.362</td>
                        <td class="field">Alumbrado Público</td>
                    </tr>
                    <tr>
                        <td class="field">AYUNTAMIENTO DE DAGANZO</td>
                        <td class="field">ES00250145897455320014ES</td>
                        <td class="field">2.0A</td>
                        <td class="field">7.700</td>
                        <td class="field"></td>
                        <td class="field"></td>
                        <td class="field">C/ SAN JUAN 17, Bajo</td>
                        <td class="field">28814</td>
                        <td class="field">Daganzo</td>
                        <td class="field">Madrid</td>
                        <td class="field">Iberdrola Distribución</td>
                        <td class="field">19.362</td>
                        <td class="field">-</td>
                        <td class="field">-</td>
                        <td class="field">19.362</td>
                        <td class="field">Alumbrado Público</td>
                    </tr>
                    <tr>
                        <td class="field">AYUNTAMIENTO DE DAGANZO</td>
                        <td class="field">ES00250145897455320014ES</td>
                        <td class="field">2.0A</td>
                        <td class="field">7.700</td>
                        <td class="field"></td>
                        <td class="field"></td>
                        <td class="field">C/ SAN JUAN 17, Bajo</td>
                        <td class="field">28814</td>
                        <td class="field">Daganzo</td>
                        <td class="field">Madrid</td>
                        <td class="field">Iberdrola Distribución</td>
                        <td class="field">19.362</td>
                        <td class="field">-</td>
                        <td class="field">-</td>
                        <td class="field">19.362</td>
                        <td class="field">Alumbrado Público</td>
                    </tr>
                    <tr>
                        <td class="field">AYUNTAMIENTO DE DAGANZO</td>
                        <td class="field">ES00250145897455320014ES</td>
                        <td class="field">2.0A</td>
                        <td class="field">7.700</td>
                        <td class="field"></td>
                        <td class="field"></td>
                        <td class="field">C/ SAN JUAN 17, Bajo</td>
                        <td class="field">28814</td>
                        <td class="field">Daganzo</td>
                        <td class="field">Madrid</td>
                        <td class="field">Iberdrola Distribución</td>
                        <td class="field">19.362</td>
                        <td class="field">-</td>
                        <td class="field">-</td>
                        <td class="field">19.362</td>
                        <td class="field">Alumbrado Público</td>
                    </tr><tr>
                        <td class="field">AYUNTAMIENTO DE DAGANZO</td>
                        <td class="field">ES00250145897455320014ES</td>
                        <td class="field">2.0A</td>
                        <td class="field">7.700</td>
                        <td class="field"></td>
                        <td class="field"></td>
                        <td class="field">C/ SAN JUAN 17, Bajo</td>
                        <td class="field">28814</td>
                        <td class="field">Daganzo</td>
                        <td class="field">Madrid</td>
                        <td class="field">Iberdrola Distribución</td>
                        <td class="field">19.362</td>
                        <td class="field">-</td>
                        <td class="field">-</td>
                        <td class="field">19.362</td>
                        <td class="field">Alumbrado Público</td>
                    </tr>
                    <tr>
                        <td class="field">AYUNTAMIENTO DE DAGANZO</td>
                        <td class="field">ES00250145897455320014ES</td>
                        <td class="field">2.0A</td>
                        <td class="field">7.700</td>
                        <td class="field"></td>
                        <td class="field"></td>
                        <td class="field">C/ SAN JUAN 17, Bajo</td>
                        <td class="field">28814</td>
                        <td class="field">Daganzo</td>
                        <td class="field">Madrid</td>
                        <td class="field">Iberdrola Distribución</td>
                        <td class="field">19.362</td>
                        <td class="field">-</td>
                        <td class="field">-</td>
                        <td class="field">19.362</td>
                        <td class="field">Alumbrado Público</td>
                    </tr>
                    <tr>
                        <td class="field">AYUNTAMIENTO DE DAGANZO</td>
                        <td class="field">ES00250145897455320014ES</td>
                        <td class="field">2.0A</td>
                        <td class="field">7.700</td>
                        <td class="field"></td>
                        <td class="field"></td>
                        <td class="field">C/ SAN JUAN 17, Bajo</td>
                        <td class="field">28814</td>
                        <td class="field">Daganzo</td>
                        <td class="field">Madrid</td>
                        <td class="field">Iberdrola Distribución</td>
                        <td class="field">19.362</td>
                        <td class="field">-</td>
                        <td class="field">-</td>
                        <td class="field">19.362</td>
                        <td class="field">Alumbrado Público</td>
                    </tr>
                    <tr>
                        <td class="field">AYUNTAMIENTO DE DAGANZO</td>
                        <td class="field">ES00250145897455320014ES</td>
                        <td class="field">2.0A</td>
                        <td class="field">7.700</td>
                        <td class="field"></td>
                        <td class="field"></td>
                        <td class="field">C/ SAN JUAN 17, Bajo</td>
                        <td class="field">28814</td>
                        <td class="field">Daganzo</td>
                        <td class="field">Madrid</td>
                        <td class="field">Iberdrola Distribución</td>
                        <td class="field">19.362</td>
                        <td class="field">-</td>
                        <td class="field">-</td>
                        <td class="field">19.362</td>
                        <td class="field">Alumbrado Público</td>
                    </tr>
                    <tr>
                        <td class="field">AYUNTAMIENTO DE DAGANZO</td>
                        <td class="field">ES00250145897455320014ES</td>
                        <td class="field">2.0A</td>
                        <td class="field">7.700</td>
                        <td class="field"></td>
                        <td class="field"></td>
                        <td class="field">C/ SAN JUAN 17, Bajo</td>
                        <td class="field">28814</td>
                        <td class="field">Daganzo</td>
                        <td class="field">Madrid</td>
                        <td class="field">Iberdrola Distribución</td>
                        <td class="field">19.362</td>
                        <td class="field">-</td>
                        <td class="field">-</td>
                        <td class="field">19.362</td>
                        <td class="field">Alumbrado Público</td>
                    </tr>
                </tbody>
                <tfoot></tfoot>
            </table>
            <br>
            <div style="clear:both"></div>
            <div class="signatura">
                <div style="position:absolute; bottom: 0px; min-width:100%;"><b>El Cliente (Firma y Sello)/b></div>
            </div>
            <div class="signatura">
                <div style="position:absolute; bottom: 0px; min-width:100%; text-align: right;"><b>Ab energía 1903, S.L.U.</b></div>
            </div>
        %endfor
	</body>
</html>