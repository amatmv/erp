# -*- coding: utf-8 -*-
{
    "name": "Pagos por capacidad Agosto 2015 ABEnergia",
    "description": """
  Actualització de les tarifes de peatges a les tarifes agost-desembre 2015 abenergia.
""",
    "version": "0-dev",
    "author": "ABEnergia",
    "category": "",
    "depends":[
        "giscedata_polissa",
        "giscedata_pagos_capacidad"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "tarifas_pagos_capacidad_201508_201512_abenergia_data.xml"
    ],
    "active": False,
    "installable": True
}
