<%
    from datetime import datetime, date
    import calendar
    from dateutil.relativedelta import relativedelta
    from operator import attrgetter, itemgetter
    from addons.crm_elec.crm_polissa import get_consums_anuals_periode

    def dateToString(data):
        if data:
            fecha = datetime.strptime(data, '%Y-%m-%d')
            return fecha.strftime('%d/%m/%Y')
        return data

    def getTarifa(tarf):
        TARIFA = [(u'31', u'3.1'),
                  (u'32', u'3.2'),
                  (u'33', u'3.3'),
                  (u'34', u'3.4'),
                  ]
        for elem in TARIFA:
            if elem[0] == tarf:
                return elem[1]
        return ''
%>

<html>
<head>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type">
    <title></title>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="${addons_path}/crm_gas_ab/report/report_crm_gas_ab_styles.css"/>
</head>
<body>
    %for oferta in objects:
        <%
        setLang(oferta.lang_id.code)
        # Duracio del descompte
        tarifa_name = getTarifa(oferta.tarifa)
        num_meses_energia = 12
        num_meses_fijo = 12
        descripcion = oferta.desc_termino_fijo.description
        if not descripcion:
            descripcion = ''
        if ('3.1' in tarifa_name or '3.2' in tarifa_name) and 'HUESCA 50' in descripcion:
            num_meses_fijo = 3
        %>
        <!-- Si no hi ha telèfon, DNI o data no permetre treure el report -->
        <% oferta.check_telf_dni_fecha(oferta.id) %>
        <!-- Comprovar que els descomptes son aplicables per les tarifes -->
        <% oferta.check_descompte(oferta.id) %>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 width100">
                    <table>
                        <tr>
                            <td></td>
                            <td rowspan="2">
                                <div class="logo">
                                    <img src="${addons_path}/crm_gas_ab/report/img/AB_ENERGIA_GAS.png"/>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="titol">
                                    ${_("OFERTA DE SUMINISTRO DE GAS")}
                                    <br/>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="pd2"></div>
            <div class="row">
                <div class="col-xs-6 no-gutter no_padding">
                    <div class="izda">
                        <div class="titol_datos_border_left datos_cliente border_verde">${_("Datos cliente")}</div>
                        <div class="white_border"></div>
                        <div class="cuadro_datos_verde h_117">
                            <b>${_("Nombre")}:</b> ${oferta.cliente_nombre or ''}
                            </br><b>${_("Apellidos")}:</b> ${oferta.cliente_apellidos or ''}
                            </br><b>${_("CIF / NIF")}:</b> ${oferta.cliente_vat or ''}
                            </br><b>${_("Dirección suministro")}:</b> ${oferta.suministro_direccion or ''}
                            </br><b>${_("Código postal")}:</b> ${oferta.suministro_cp or ''}
                            </br><b>${_("Municipio")}:</b> ${oferta.suministro_municipio or ''}
                            </br><b>${_("Fecha estimada de inicio")}:</b> ${dateToString(oferta.fecha_estimada_inicio_ctto) or ''}
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 no-gutter no_padding">
                    <div class="titol_datos_border_left datos_suministro border_verde">${_("Datos Suministro")}</div>
                    <div class="white_border"></div>
                    <div class="cuadro_datos_verde">
                        <b>${_("CUPS")}:</b> ${oferta.suministro_cups_name or ''}
                        </br>
                        <b>${_("Consumo anual estimado (kWh)")}</b>: ${oferta.consumo_anual_estimado or ''}
                    </div>
                    <div class="cuadro_tarifa mt_5">
                        <div class="titol_datos_border_left datos_suministro border_verde">${_("Tarifa")}</div>
                        <div class="white_border_small_2"></div>
                        <div class="cuadro_datos_verde">
                            <div class="tarifa">${getTarifa(oferta.tarifa) or ''}</div>
                        </div>
                    </div>
                    <div class="cuadro_fecha mt_5 ml_5">
                        <div class="titol_datos_border_left datos_suministro border_verde">${_("Fecha oferta")}</div>
                        <div class="white_border_small"></div>
                        <div class="cuadro_datos_verde">
                            <div class="fecha">${dateToString(oferta.fecha_oferta) or '&nbsp;'}</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pd2 clear"></div>
            <div class="row">
                <div class="full col-xs-6 no_padding no-gutter">
                    <div class="titol_datos_border_left datos_cliente border_verde">${_("Precio gas estándar")}</div>
                    <div class="white_border"></div>
                    <div class="cuadro_datos_verde">
                        <table class="ml_5l font_12">
                            <tr>
                                <td>
                                    <b>${_("Término energía")}:</b>
                                </td>
                                <td>
                                    ${formatLang(oferta.precio_termino_energia, digits=6)} €/kWh
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b>${_("Término fijo")}:</b>
                                </td>
                                <td>
                                    ${formatLang(oferta.precio_termino_fijo, digits=6)} €/mes
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="pd2"></div>
            <%
                hidden_energy_discount = False
                hidden_fixed_discount = False
                hidden_elec_discount = False
                if not oferta.desc_termino_energia:
                    hidden_energy_discount = True
                if not oferta.desc_termino_fijo:
                    hidden_fixed_discount = True
                if not oferta.desc_termino_energia_electricidad:
                    hidden_elec_discount = True
            %>
            %if not (hidden_energy_discount and hidden_fixed_discount):
                <div class="row">
                    <div class="full col-xs-6 no_padding no-gutter">
                        <div class="titol_datos_border_left datos_cliente border_verde">${_("Descuento promoción")}</div>
                        <div class="white_border"></div>
                        <div class="cuadro_datos_verde">
                            <table class="ml_5l font_12">
                                %if not hidden_energy_discount:
                                    <tr>
                                        <td>
                                            <b>${_("Término energía")}:</b>
                                        </td>
                                        <td>
                                            - ${oferta.desc_termino_energia.name}%
                                        </td>
                                    </tr>
                                %endif
                                %if not hidden_fixed_discount:
                                    <tr>
                                        <td>
                                            <b>${_("Término fijo")}:</b>
                                        </td>
                                        <td>
                                            - ${oferta.desc_termino_fijo.name}%
                                        </td>
                                    </tr>
                                %endif
                            </table>
                        </div>
                    </div>
                </div>
            %endif
            <div class="pd2"></div>
            %if not (hidden_elec_discount and hidden_energy_discount and hidden_fixed_discount):
                <div class="row">
                    <div class="full col-xs-6 no_padding no-gutter">
                        <div class="titol_datos_border_left datos_cliente border_verde">${_("Promoción contratada")}</div>
                        <div class="white_border"></div>
                        <div class="cuadro_datos_verde font_12">
                            <div class="ml_2l">
                                %if not hidden_energy_discount:
                                    ${_("Descuento del ")}${oferta.desc_termino_energia.name}% ${_("en el término de energía aplicable durante")} ${num_meses_energia} ${_("meses a partir de la fecha de inicio del contrato.")}
                                    </br>
                                %endif
                                %if not hidden_fixed_discount:
                                    ${_("Descuento del ")} ${oferta.desc_termino_fijo.name}% ${_("en el término fijo aplicable durante")} ${num_meses_fijo} ${_("meses a partir de la fecha de inicio del contrato.")}
                                    </br>
                                %endif
                            </div>
                        </div>
                    </div>
                </div>
            %endif
            <div class="pd2"></div>
            <div class="row">
                <div class="full col-xs-6 no_padding no-gutter">
                    <div class="titol_datos_border_left datos_cliente border_azul">${_("Tu asesor personal")}</div>
                    <div class="white_border"></div>
                    <div class="cuadro_datos_azul font_12">
                        <div class="ml_2l">
                            ${oferta.user_id.name}
                            </br>
                            ${oferta.user_id.address_id.email}
                            </br>
                            ${oferta.user_id.address_id.phone}
                        </div>
                    </div>
                </div>
            </div>
            <div class="pd2"></div>
            <div class="row">
                <div class="validez">
                    ${_("Aceptación de la oferta")}
                </div>
                <div class="info_legal">
                    <ul>
                        <li>
                            ${_("Días de validez de la oferta: 7")}
                        </li>
                        <li>
                            ${_("Se repercutirán las variaciones a la baja o al alza en las tarifas de acceso, los valores regulados que pueden ser aprobados por la Administración para su aplicación durante la duración del contrato, y los aumentos significativos del precio de compra imputable a alteraciones no previstas en el mercado energético.")}
                        </li>
                        <li>
                            <b>${_("ab energía 1903, S.L.U.")}</b>${_(" se reserva la posibilidad de solicitar garantías financieras para proceder a la activación del punto de suministro.")}
                        </li>
                        <li>
                            ${_("Los precios incluyen todas las tasas excepto el IVA y el Impuesto Especial de Hidrocarburos. Puede consultar el detalle de la oferta en el desglose de la oferta. Oferta válida para España peninsular.")}
                        </li>
                        <li>
                            ${_("La oferta ha sido calculada a partir de los datos suministrados por el cliente, referentes a su consumo energético.")}
                        </li>
                        <li>
                            ${_("La activación del punto de suministro con ")}<b>${_("ab energía 1903, S.L.U.")}</b>${_(" es concedida por la empresa distribuidora de la zona, quien lo notificará a ")}<strong>${_("ab energía 1903, S.L.U.")}</strong>${_(".")}
                        </li>
                    </ul>
                </div>
            </div>

            <div class="row">
                <div class="firma_cliente">
                    ${_("Por el cliente")}
                </div>
                <div class="firma_cliente ml_30">
                    ${_("Por ")}<strong>${_("ab")}</strong>${_(" energía")}
                    <div class="firma">
                        <img class="img_firma" src="${addons_path}/crm_gas_ab/report/img/firmaDireccion.png"></img>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
            <div class="row mt_10 font_9">
                ${_("Fdo")}:
                </br>
                ${_("DNI")}:
                </br>
                <div class="float_left">
                    ${_("En")}
                </div>
                <div class="float_left subrayado w_200">&nbsp;</div>
                <div class="float_left">
                    ${_("a")}:
                </div>
                <div class="float_left subrayado w_100">&nbsp;</div>
                <div class="float_left">
                    ${_("de")}:
                </div>
                <div class="float_left subrayado w_200">&nbsp;</div>
                <div class="float_left">
                    de ${datetime.today().year}
                </div>
            </div>

            <p style="page-break-after:always"></p>
            <!-- Part posterior -->

            <div class="posterior">
                <div class="col-xs-12 no-gutter no_padding anexo-1">
                    <div class="titol_datos_border_left border_azul">${_("Anexo a las condiciones generales del contrato de gas")}</div>
                    <div class="white_border w_430"></div>
                    <div class="cuadro_datos_azul">
                        <div class="font_10p center_annex">
                            <div class="float_left bold" style="width: 190px;">
                                ${_("Nombre del representante legal")}
                            </div>
                            <div class="float_left subrayado" style="width: 275px">&nbsp;</div>
                            <div class="float_left bold">
                                ${_("DNI")}
                            </div>
                            <div class="float_left subrayado" style="width: 140px">&nbsp;</div>
                            <div class="clear" style="height: 5px;"></div>
                            <div class="float_left bold" style="width: 140px;">
                                ${_("Nombre de la empresa")}
                            </div>
                            <div class="float_left subrayado" style="width: 325px">&nbsp;</div>
                            <div class="float_left bold">
                                ${_("CIF")}
                            </div>
                            <div class="float_left subrayado" style="width: 145px">&nbsp;</div>
                            <div class="clear" style="height: 5px;"></div>
                            <div class="float_left bold" style="width: 125px;">
                                ${_("Teléfono de contacto")}
                            </div>
                            <div class="float_left subrayado" style="width: 220px">&nbsp;</div>
                            <div class="float_left bold">
                                ${_("CUPS")}
                            </div>
                            <div class="float_left subrayado" style="width: 253px">&nbsp;</div>
                            <div class="clear" style="height: 5px;"></div>
                            <div class="float_left bold">
                                ${_("Mail")}
                            </div>
                            <div class="float_left subrayado" style="width: 614px">&nbsp;</div>
                        </div>
                        <div class="clear"></div>
                        <div class="texto-autoriza">
                            <p class="head">${_("Autoriza")}</p>
                            <p class="texto">
                                ${_("A la compañía Ab energía 1903, S.L.U., con C.I.F. B-22350466, o cualquier empresa que ésta designe para tal fin, a formalizar por su cuenta y en nombre del cliente, en calidad de sustituto, el correspondiente contrato de acceso a las redes de distribución y a modificar, si lo estima oportuno, los parámetros energéticos de contratación del punto de suministro; así como autorizar a ab energía 1903, S.L.U., o cualquier empresa autorizada por ésta, a solicitar toda aquella información histórica relativa al punto de suministro energético que en calidad de consumidor deba, conforme a derecho, proporcionar la compañía distribuidora a sus abonados y a presentar ante la distribuidora y en mi nombre, cualesquiera reclamaciones consideradas oportunas. Estando sujeta la respuesta de la distribuidora, a los plazos de atención de reclamaciones establecidas en las legislación vigente, para reclamaciones presentadas por los titulares de los puntos de suministro. Sírvase admitir este poder, para su bastanteo, otorgando a los efectos de los dispuesto en el Real Decreto 1955/2000, de 1 de diciembre, en el Real Decreto 1434/2002 y 1435/2002, ambas del 27 de diciembre y demás disposiciones aplicables.")}
                            </p>
                        </div>
                        <div class="clear"></div>
                        <div class="font_10p firma_annex mt_10 float_left">
                            <strong>${_("Firma del representante de la compañía")}</strong></br>
                            <div class="float_left bold mt_5">
                                ${_("Fdo:")}
                            </div>
                            <div class="float_left subrayado" style="width: 200px">&nbsp;</div>
                            <div class="clear"></div>
                            <div class="float_left bold mt_5">
                                ${_("DNI:")}
                            </div>
                            <div class="float_left subrayado" style="width: 200px">&nbsp;</div>
                        </div>
                        <div class="float_right mt_10">
                            <div class="cuadro_firma_titol">${_("Por ")}<span>${_("el cliente")}</span></div>
                            <div class="cuadro_firma_azul"> </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="col-xs-12 no-gutter no_padding anexo-2">
                    <div class="titol_datos_border_left border_verde titulo_anexo">${_("Confirmación del procedimiento de pago del cliente")}<br>
                        <span>${_("Reglamento 260/2012 del Parlamento Europeo y del Consejo del 14 de marzo 2012")}</span>
                    </div>
                    <div class="white_border w_590"></div>
                    <div class="cuadro_datos_verde">
                        <ul class="condicions superior">
                            <li>
                                <div class="filera">
                                    <div class="nom">
                                        <span class="desc">${_("Nombre del cliente")}</span><br>
                                    </div>
                                    <div>
                                        <div class="linia"></div>
                                    </div>
                                    <span class="eng">${_("Debtor’s name")}</span>
                                </div>
                            </li>
                            <li>
                                <div class="filera">
                                    <div class="nom">
                                        <span class="desc">${_("Dirección del cliente")}</span><br>
                                    </div>
                                    <div>
                                        <div class="linia"></div>
                                    </div>
                                    <span class="eng">${_("Adress of the debtor")}</span>
                                </div>
                            </li>
                            <li>
                                <div class="filera">
                                    <div class="nom">
                                        <span class="desc">${_("Entidad bancaria")}</span><br>
                                    </div>
                                    <div>
                                        <div class="linia"></div>
                                    </div>
                                    <span class="eng">${_("Entity name")}</span>
                                </div>
                            </li>
                            <li>
                                <div class="filera">
                                    <div class="nom">
                                        <span class="desc">${_("Número de cuenta (IBAN)")}</span><br>
                                    </div>
                                    <div>
                                        %for elem in range(1, 5):
                                            <div class="iban_box" style="left: -${elem}px"></div>
                                        %endfor
                                        <div class="iban_box_white"></div>
                                        %for elem in range(1, 5):
                                            <div class="iban_box" style="left: -${elem}px"></div>
                                        %endfor
                                        <div class="iban_box_white"></div>
                                        %for elem in range(1, 5):
                                            <div class="iban_box" style="left: -${elem}px"></div>
                                        %endfor
                                        <div class="iban_box_white"></div>
                                        %for elem in range(1, 3):
                                            <div class="iban_box" style="left: -${elem}px"></div>
                                        %endfor
                                        <div class="iban_box_white"></div>
                                        %for elem in range(1, 11):
                                            <div class="iban_box" style="left: -${elem}px"></div>
                                        %endfor
                                    </div>
                                    <span class="eng">${_("Account number - IBAN")}</span>
                                </div>
                            </li>
                            <li>
                                <div class="filera">
                                    <div class="nom">
                                        <span class="desc">${_("Fecha y localidad de la firma")}</span><br>
                                    </div>
                                    <div class="linia"></div>
                                    <span class="eng">${_("Date and location in which you are signing")}</span>
                                </div>
                            </li>
                        </ul>
                        <div class="texto-autoriza">
                            <p class="head">${_("Autoriza")}</p>
                            <p class="texto">
                                ${_("Mediante la firma de esta orden de domiciliación, el deudor autoriza (A) al acreedor a enviar instrucciones a la entidad del deudor para adeudar su cuenta y (B) a la entidad para efectuar los adeudos en su cuenta siguiendo las instrucciones del acreedor. Como parte de sus derechos, el deudor está legitimado al reembolso por su entidad en los términos y condiciones del contrato suscrito con la misma. La solicitud de reembolso deberá efectuarse dentro de las ocho semanas que siguen a la fecha de adeudo en cuenta. Puede obtener información adicional sobre sus derechos en su entidad financiera.")}
                                <br><span class="eng">${_("By signing this mandate form, you authorize (A) the Creditor to send instructions to your bank to debit your account and (B) your bank to debit your account in accordance with the instructions from the Creditor. As part of your rights, you are entitled to a refund from your bank under the terms and conditions of your agreement with your bank. A refund must be claimed withing eight weeks starting from the date on which your account was debited. Your rights are explained in a statement that you can obtain from you bank.")}</span>
                            </p>
                        </div>
                        <div class="clear"></div>
                        <div class="font_10p firma_annex mt_10 float_left">
                            <strong>${_("Firma del representante de la compañía")}</strong></br>
                            <div class="float_left bold mt_5">
                                ${_("Fdo:")}
                            </div>
                            <div class="float_left subrayado" style="width: 200px">&nbsp;</div>
                            <div class="clear"></div>
                            <div class="float_left bold mt_5">
                                ${_("DNI:")}
                            </div>
                            <div class="float_left subrayado" style="width: 200px">&nbsp;</div>
                        </div>
                        <div class="float_right mt_10">
                            <div class="cuadro_firma_titol">${_("Por ")}<span>${_("el cliente")}</span></div>
                            <div class="cuadro_firma_azul"> </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-7 no-gutter">
                        <div class="cuadro_desestimiento">
                            <span class="head">${_("Sólo debe cumplimentar y enviar el presente formulario si desea desistir del contrato")}</span><br>
                            <div class="text">
                                <p>${_("A la atención de Ab energía 1903, S.L.U. Escuelas Pías 12, 22300 de Barbastro, fax 974 314 744 y email info@abenergia.es")}<br>
                                   ${_("Por la presente le comunico/comunicamos que desisto de mi/desistimos de nuestro contrato de suministro de gas.")}
                                </p>
                                <p class="sign">
                                    ${_("Pedido/ recibido el:")}<br>
                                    ${_("Nombre completo del consumidor:")}<br>
                                    ${_("Domicilio del consumidor:")}<br>
                                    ${_("Firma del consumidor")}<br>
                                </p>
                                ${_("Fecha:")}<br>
                                ${_("DNI/NIF:")}<br>
                                ${_("No Contrato:")}<br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    %endfor
</body>
</html>
