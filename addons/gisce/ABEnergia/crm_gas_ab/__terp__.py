# -*- coding: utf-8 -*-
{
    "name": "CRM GAS Custom ABenergia",
    "description": """CRM per ofertes de GAS""",
    "version": "0-dev",
    "author": "Abenergia",
    "category": "CRM",
    "depends":[
        "base",
        "c2c_webkit_report"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/crm_gas_ab_security.xml",
        "security/ir.model.access.csv",
        "crm_gas_view.xml",
        "crm_gas_ab_data.xml",
        "crm_gas_ab_report.xml"
    ],
    "active": False,
    "installable": True
}
