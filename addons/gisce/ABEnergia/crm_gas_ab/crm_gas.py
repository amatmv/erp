# -*- coding: utf-8 -*-
from osv import osv, fields

TIPO_PERSONA = [(u'fisica', u'Persona física'),
                (u'juridica', u'Persona jurídica')]

TARIFA = [(u'31', u'3.1'),
          (u'32', u'3.2'),
          (u'33', u'3.3'),
          (u'34', u'3.4'),
          ]

class CrmGasDescuentoTerminoFijo(osv.osv):

    _name = 'crm.gas.descuento.fijo'

    _order = 'name asc'

    _columns = {
        'name': fields.integer(u'Descuento término fijo (%)', required=True),
        'description': fields.char(string=u'Descripción', size=128),
    }

    _sql_constraints = [('name_uniq', 'unique (name)',
                         u'Ya existe este descuento.')]

CrmGasDescuentoTerminoFijo()

class CrmGasDescuentoTerminoEnergia(osv.osv):

    _name = 'crm.gas.descuento.energia'

    _order = 'name asc'

    _columns = {
        'name': fields.integer(u'Descuento término energía (%)', required=True),
        'description': fields.char(string=u'Descripción', size=128),
    }

    _sql_constraints = [('name_uniq', 'unique (name)',
                         u'Ya existe este descuento.')]

CrmGasDescuentoTerminoEnergia()

class CrmGasPrice(osv.osv):

    _name = 'crm.gas.price'

    _order = 'position asc'

    TERMINO = [(u'fijo', u'Término fijo'),
               (u'energia', u'Término energía')]
    TARIFA = [(u'31', u'3.1'),
              (u'32', u'3.2'),
              (u'33', u'3.3'),
              (u'34', u'3.4'),
              ]

    _columns = {
        'name': fields.float(u'Precio', digits=(15, 6),required=True),
        'termino': fields.selection(TERMINO,
                                        string=u'Término',
                                        required=True),
        'tarifa': fields.selection(TARIFA, string=u'Tarifa', required=True),
        'description': fields.char(string=u'Descripción', size=128),
        'position': fields.integer('Orden')
    }

    _defaults = {
        'position': lambda *a: 10
    }

    _sql_constraints = [('name_uniq', 'unique (name)',
                         u'Ya existe este descuento.')]

CrmGasPrice()

class CrmGasEtapa(osv.osv):

    _name = 'crm.gas.etapa'

    _order = 'name asc'

    _columns = {
        'name': fields.char(u'Etapa', required=True, size=32),
        'description': fields.char(string=u'Descripción', size=128),
    }

    _sql_constraints = [('name_uniq', 'unique (name)',
                         u'Ya existe esta etapa.')]

CrmGasEtapa()

class CrmGas(osv.osv):
    _name = 'crm.gas'
    _description = 'Oferta de subministrament de gas'

    def _default_etapa_id(self, cursor, uid, context=None):
        if not context:
            context = {}
        etapa_obj = self.pool.get('crm.gas.etapa')
        etapa = etapa_obj.search(cursor, uid, [('name', '=', 'Pendiente')])
        return etapa[0]

    def _default_lang_id(self, cursor, uid, context=None):
        #Fixa el language a español a totes les ofertes noves de gas
        if not context:
            context = {}
        lang_obj = self.pool.get('res.lang')
        castella = lang_obj.search(cursor, uid, [('code', '=', 'es_ES')])
        return castella[0]

    def onchange_tarifa_sel(self, cursor, uid, ids, tarifa_sel, context=None):
        res = {'value': {}, 'domain': {}, 'warning': {}}
        price_obj = self.pool.get('crm.gas.price')
        params_fijo = [('tarifa', '=', tarifa_sel),
                       ('termino','=', 'fijo')]
        params_energia = [('tarifa', '=', tarifa_sel),
                          ('termino','=', 'energia')]
        preu_ene = price_obj.search(cursor, uid, params_energia)
        preu_e = price_obj.read(cursor, uid, preu_ene, ['name'])[0]['name']
        preu_fixe = price_obj.search(cursor, uid, params_fijo)
        preu_f = price_obj.read(cursor, uid, preu_fixe, ['name'])[0]['name']
        res['value'].update({'precio_termino_fijo': preu_f,
                             'precio_termino_energia': preu_e})
        return res

    def aceptar_oferta(self, cursor, uid, oferta_id, context=None):
        gasoffer_obj = self.pool.get('crm.gas')
        etapa_obj = self.pool.get('crm.gas.etapa')

        dni = gasoffer_obj.read(cursor, uid, oferta_id, ['vat_attached'])
        dni_adjunt = dni[0]['vat_attached']
        if not dni_adjunt:
            msg = 'No se puede aceptar una oferta sin adjuntar el DNI firmante.'
            raise osv.except_osv('Error', msg)
            return False
        params = [('name','=','Aceptada')]
        etapa_aceptada_id = etapa_obj.search(cursor, uid, params)[0]
        vals = {'etapa_id': etapa_aceptada_id}
        gasoffer_obj.write(cursor, uid, oferta_id, vals)
        return True

    def presentar_oferta(self, cursor, uid, oferta_id, context=None):
        gasoffer_obj = self.pool.get('crm.gas')
        etapa_obj = self.pool.get('crm.gas.etapa')
        etapa_actual = gasoffer_obj.read(cursor, uid, oferta_id, ['etapa_id'])
        etapa_act_id = etapa_actual[0]['etapa_id'][0]
        etapa_act_name = etapa_obj.read(cursor, uid, etapa_act_id, ['name'])
        etapa = etapa_act_name['name']
        if etapa == 'Aceptada':
            msg = 'La oferta ya está Aceptada.'
            raise osv.except_osv('Error', msg)
            return False
        params = [('name','=','Presentada')]
        etapa_presentada_id = etapa_obj.search(cursor, uid, params)[0]
        vals = {'etapa_id': etapa_presentada_id}
        gasoffer_obj.write(cursor, uid, oferta_id, vals)
        return True

    def check_telf_dni_fecha(self, cursor, uid, ids, context=None):
        '''Comprova si el telèfon i el DNI es troben introduits
        '''
        oferta = self.read(cursor, uid, ids, ['cliente_vat',
                                              'cliente_telefono',
                                              'fecha_oferta'])[0]
        vat = oferta['cliente_vat']
        telf = oferta['cliente_telefono']
        fecha = oferta['fecha_oferta']
        if not (vat and telf and fecha):
            raise osv.except_osv(u'Error',
                                 u'No se puede imprimir una oferta sin'
                                 u' introducir DNI, teléfono y fecha oferta.')
            return False
        return True

    def check_descompte(self, cursor, uid, ids, context=None):
        '''Comprova que els descomptes son aplicables
        '''
        oferta = self.read(cursor, uid, ids, ['desc_termino_fijo',
                                              'tarifa',
                                              ])[0]
        desc_fijo = oferta.get('desc_termino_fijo')
        if desc_fijo:
            desc_fijo = desc_fijo[1]
        tarifa = oferta['tarifa']
        if tarifa == u'31' and desc_fijo == u'10':
            raise osv.except_osv(u'Error',
                                 u'No se puede imprimir una oferta 3.1'
                                 u' con descuento sobre término fijo del 10%.')
            return False
        return True

    _columns = {
        'name': fields.char(u'Número de oferta', size=200),
        'clte_tipo_persona': fields.selection(TIPO_PERSONA,
                                              string=u'Tipo persona'
                                              ),
        'cliente_vat': fields.char(u'CIF/NIF', size=32, required=True),
        'cliente_nombre': fields.char(u'Nombre', size=128),
        'cliente_apellidos': fields.char(u'Apellidos', size=256),
        'cliente_direccion': fields.char(u'Dirección cliente', size=256),
        'cliente_telefono': fields.char(u'Teléfono', size=256, required=True),
        'cliente_email': fields.char(u'E-mail', size=256),
        'cliente_cp': fields.char(u'Código postal', size=5),
        'cliente_municipio': fields.char(u'Municipio', size=128),
        'cliente_provincia': fields.char(u'Provincia', size=128),

        'suministro_direccion': fields.char(u'Dirección suministro', size=256),
        'suministro_cp': fields.char(u'Código postal', size=5),
        'suministro_municipio': fields.char(u'Municipio', size=128),
        'suministro_provincia': fields.char(u'Provincia', size=128),
        'fecha_estimada_inicio_ctto': fields.date(
                            string=u'Fecha estimada inicio contrato'),
        'suministro_cups_name': fields.char(u'CUPS', size=22),
        'consumo_anual_estimado': fields.integer(
            string=u'Consumo anual estimado (kWh)'
        ),
        'tarifa': fields.selection(TARIFA, string=u'Tarifa'),
        'fecha_oferta': fields.date(string=u'Fecha oferta'),
        'precio_termino_energia': fields.float(u'Término energía (€/kWh)',
                                               digits=(15, 6)),
        'precio_termino_fijo': fields.float(u'Término fijo (€/mes)',
                                            digits=(15, 6)),
        'desc_termino_energia': fields.many2one(
            'crm.gas.descuento.energia',
            string=u'Descuento término energía (%)'),
        'desc_termino_fijo': fields.many2one(
            'crm.gas.descuento.fijo',
            string=u'Descuento término fijo (%)'),
        'desc_termino_energia_electricidad': fields.boolean(
            u'Descuento término energía en factura eléctrica'),
        'user_id': fields.many2one('res.users', u'Comercial'),
        'comentarios_oferta': fields.text(string=u'Comentarios'),
        'distribuidor_codigo': fields.char(u'Código distribuidor', size=4),
        'distribuidor_nombre': fields.char(u'Nombre distribuidor', size=64),
        'qh': fields.float(u'Qh'),
        'qmax': fields.float(u'QMax'),
        'derecho_tur': fields.char(u'Derecho TUR', size=1),
        'fecha_ultima_inspeccion': fields.date(
            string=u'Fecha última inspección'),
        'resultado_ultima_inspeccion': fields.char(
            u'Resultado última inspección',
            size=2),
        'fecha_ultimo_cambio_tarifa': fields.date(
            string=u'Fecha último cambio tarifa'),
        'fecha_ultimo_cambio_comer': fields.date(
            string=u'Fecha último cambio comercializador'),
        'vivienda_habitual': fields.char(u'Vivienda habitual', size=1),
        'impago': fields.char(u'Impago', size=1),
        'impago_fecha': fields.date(string=u'Fecha impago'),
        'etapa_id': fields.many2one(
            'crm.gas.etapa', u'Estado'),
        'vat_attached': fields.boolean(u'Escaneo DNI firmante adjunto'),
        'numero_ctto_gas': fields.char(u'Numero contrato Gas', size=16),
        'lang_id': fields.many2one('res.lang',
                                   'Language',
                                   domain=[('active', '=', True),
                                           ('translatable', '=', True)],),
    }

    _defaults = {
        'etapa_id': _default_etapa_id,
        'lang_id': _default_lang_id,
    }

CrmGas()
