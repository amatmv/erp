# -*- coding: utf-8 -*-
from osv import osv
from datetime import datetime
from datetime import timedelta

class GiscedataFacturacioFactura(osv.osv):

    _name = 'giscedata.facturacio.factura'
    _inherit = 'giscedata.facturacio.factura'

    def invoice_open(self, cursor, uid, ids, context=None):
        """Custom ab distri
        Set date_due to today + days configured in fact_date_due_invoice var
        If var is 0 don't update the invoice's date_due
        """

        if not context:
            context = {}
        if not isinstance(ids, list):
            ids = [ids]

        res = super(
            GiscedataFacturacioFactura, self
        ).invoice_open(cursor, uid, ids, context=context)

        today = datetime.today()
        vals = {}
        cfg_obj = self.pool.get('res.config')
        date_due_days = int(cfg_obj.get(cursor, uid,
                                        'fact_date_due_invoice','0'))
        if date_due_days:
            vals.update({'date_due': today + timedelta(days=date_due_days)})

        self.write(cursor, uid, ids, vals)

        return res

GiscedataFacturacioFactura()
