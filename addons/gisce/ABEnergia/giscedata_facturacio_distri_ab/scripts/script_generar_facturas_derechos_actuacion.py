# -*- coding: utf-8 -*-
import logging
from erppeek import Client
import click
import pandas as pd
from datetime import datetime
from tqdm import tqdm

logging_name = 'facturacion_derechos_DEMICBT_log'


def setup_log(instance, filename):
    log_file = filename
    logger = logging.getLogger(instance)
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr = logging.FileHandler(log_file)
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(logging.INFO)


def get_product_reconnection(client):
    """Returns product in reconnection category if exist only one product
    into them"""
    imd_obj = client.model('ir.model.data')

    prod_id = imd_obj.get_object_reference(
        'giscedata_contractacio_distri',
        'acc_quota_enganche_bt'
    )[1]
    if prod_id:
        return prod_id

    return False


def create_reconnection_invoice(client, polissa_id, date_inv, quantitat=1):
    contractacio_obj = client.model('contractacio.distri')
    contractacio_line_obj = client.model('contractacio.distri.line')
    contractacio_concept_obj = client.model('contractacio.distri.concept')
    imd_obj = client.model('ir.model.data')

    contractacio_values = {
        'polissa_id': polissa_id,
        'name': 'Modificación DH',
        'date': date_inv,
        'factura_comer': True,
    }

    ctx = {'polissa_id': polissa_id}

    contractacio_id = contractacio_obj.create(contractacio_values, ctx)

    if contractacio_id:
        contractacio_id = contractacio_id.id
        # Get contractació object by browse
        contractacio = contractacio_obj.browse(contractacio_id)
        prod_id = get_product_reconnection(client)

        if prod_id:
            type_c = imd_obj.get_object_reference(
                'giscedata_contractacio_distri',
                'contractacio_type_generic'
            )[1]

            if type_c:
                # price = contractacio.get_price(
                #     'enganche', prod_id, {}
                # )

                # HARDCODED VALUE
                price = 9.044760

                contractacio_line_values = {
                    'contractacio_id': contractacio_id,
                    'type_id': type_c,
                    'product_id': prod_id,
                    'quantity': quantitat,
                    'price_unit': price,
                    'amount_untaxed': price * quantitat
                }
                con_lin_id = contractacio_line_obj.create(
                    contractacio_line_values
                )
                if con_lin_id:
                    contractacio.action_facturar_giscedata()
                    contractacio = contractacio_obj.browse(
                        contractacio_id
                    )
                    len_fact_lis = len(contractacio.factures_ids)
                    if len_fact_lis:
                        return contractacio.factures_ids[
                            len_fact_lis - 1].id
    return False


def generate_reconnection(client, polizas, fecha_fact, logger):
    pol_obj = client.model('giscedata.polissa')
    facturas_generades = []
    error_generar = []
    for pol in tqdm(polizas):
        factura = None
        # Generar Factura
        factura = create_reconnection_invoice(
            client, pol['id'], fecha_fact
        )
        if factura:
            facturas_generades.append((pol['name'], factura))
        else:
            error_generar.append(pol['name'])
    if facturas_generades:
        logger.info(
            '\nGeneradas las siguientes facturas:\n'
        )
        for f in facturas_generades:
            logger.info('\nPoliza:{} Factura id:{}\n'.format(f[0], f[1]))

    if error_generar:
        logger.info(
            '\nError al generar facturas para las siguientes polizas:\n'
        )
        logger.info('\n'.join(error_generar))


def read_and_parse_csv(client, file_path, logger):
    polizas = []
    polizas_no_encontradas = False
    if file_path:
        df = pd.read_csv(file_path, sep=';')
        # Index should named as 'contrato'
        # Avoid repeated
        contratos_list = [str(con) for con in df.contrato.unique()]
        pol_obj = client.model('giscedata.polissa')
        pol_ids = pol_obj.search([('name', 'in', contratos_list)])
        polizas = pol_obj.read(pol_ids, ['name', 'tarifa', 'state'])
        if len(pol_ids) != len(contratos_list):
            polizas_encontradas = [p['name'] for p in polizas]
            polizas_no_encontradas = list(
                set(contratos_list) - set(polizas_encontradas)
            )
        if polizas_no_encontradas:
            logger.info('\nPolizas no encontradas:')
            logger.info('\n'.join(polizas_no_encontradas))

    return polizas


@click.command()
@click.option('-s', '--server', default='http://localhost',
              help='Servidor ERP')
@click.option('-p', '--port', default=8069, help='Puerto servidor ERP',
              type=click.INT)
@click.option('-u', '--user', default='admin', help='Usuario servidor ERP')
@click.option('-w', '--password', default='admin',
              help='Contraseña usuario ERP')
@click.option('-d', '--database', help='Nombre de la base de datos')
@click.option('-f', '--fecha', default=None, help='Fecha de facturacion')
@click.option('-c', '--csv', default=None, help='Fichero CSV')
def generar_facturas(**kwargs):
    """EJEMPLO DE FICHERO (¡¡¡¡¡Respetar header contrato!!!!):
        contrato
        12345678
        47855446
        45884889
        41855255
    """
    server = '{}:{}'.format(kwargs['server'], kwargs['port'])
    c = Client(
        server=server,
        db=kwargs['database'],
        user=kwargs['user'],
        password=kwargs['password']
    )

    if not c:
        click.echo(u'Impossible to connect to ERP')
        return False

    setup_log(logging_name,
              '/tmp/facturacion_derechos_DEMICBT.log')
    logger = logging.getLogger(logging_name)
    logger.info('#######Start now {0}'.format(datetime.today()))

    file_path = kwargs['csv']
    fecha_fact = kwargs['fecha']  # "%Y-%m-%d"

    polizas = read_and_parse_csv(c, file_path, logger)

    generate_reconnection(c, polizas, fecha_fact, logger)


if __name__ == '__main__':
    generar_facturas()
