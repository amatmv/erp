<%
    from operator import attrgetter, itemgetter
    from datetime import datetime, timedelta
    import json
%>
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<head>
    <style type="text/css">
        ${css}
    </style>
    <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_distri_ab/report/factura.css"/>
</head>
<body>
    <%
        pool = objects[0].pool
        polissa_obj = objects[0].pool.get('giscedata.polissa')
        comptador_factures = 0;
        def get_mes(month):
          months = {
            '1': 'ENERO','2': 'FEBRERO','3': 'MARZO',
            '4': 'ABRIL','5': 'MAYO','6': 'JUNIO',
            '7': 'JULIO','8': 'AGOSTO','9': 'SEPTIEMBRE',
            '10': 'OCTUBRE','11': 'NOVIEMBRE','12': 'DICIEMBRE',
          }
          return months[str(month)]
        def pretty_date(data, type='normal'):
            any, mes, dia = data.split('-')
            if type == 'normal':
                return '%s/%s/%s' %(dia, mes, any)
            elif type == 'text':
                return '%s de %s de %s' %(dia, get_mes(int(mes)).lower(), any)

        def get_discriminador(factura_id):
            sql = """SELECT count(*) as total FROM giscedata_facturacio_factura_linia
            WHERE tipus='energia' and factura_id = %s
            """ % factura_id
            cursor.execute(sql)
            return cursor.dictfetchall()[0]['total']

        def get_historics(polissa_id, data_inici, data_final):
            sql = """SELECT * FROM (
                        SELECT mes AS mes,
                        periode AS periode,
                        sum(suma_fact) AS facturat,
                        sum(suma_consum) AS consum,
                        min(data_ini) AS data_ini,
                        max(data_fin) AS data_fin
                        FROM (
                        SELECT f.polissa_id AS polissa_id,
                               to_char(f.data_inici, 'YYYY/MM') AS mes,
                               pt.name AS periode,
                               COALESCE(SUM(il.quantity*(fl.tipus='energia')::int*(CASE WHEN i.type='out_refund' THEN -1 ELSE 1 END)),0.0) as suma_consum,
                               COALESCE(SUM(il.price_subtotal*(fl.tipus='energia')::int*(CASE WHEN i.type='out_refund' THEN -1 ELSE 1 END)),0.0) as suma_fact,
                               MIN(f.data_inici) data_ini,
                               MAX(f.data_final) data_fin
                               FROM
                                    giscedata_facturacio_factura f
                                    LEFT JOIN account_invoice i on f.invoice_id = i.id
                                    LEFT JOIN giscedata_facturacio_factura_linia fl on f.id=fl.factura_id
                                    LEFT JOIN account_invoice_line il on il.id=fl.invoice_line_id
                                    LEFT JOIN product_product pp on il.product_id=pp.id
                                    LEFT JOIN product_template pt on pp.product_tmpl_id=pt.id
                               WHERE
                                    fl.tipus = 'energia' AND
                                    f.polissa_id = %(p_id)s AND
                                    f.data_inici <= %(data_inicial)s AND
                                    f.data_inici >= date_trunc('month', date %(data_final)s) - interval '14 month'
                                    AND i.type in('out_invoice', 'out_refund')
                               GROUP BY
                                    f.polissa_id, pt.name, f.data_inici
                               ORDER BY f.data_inici DESC ) AS consums
                        GROUP BY polissa_id, periode, mes
                        ORDER BY mes DESC, periode ASC
                        ) consums_ordenats
                        ORDER BY mes ASC
                """
            cursor.execute(
                sql,
                {'p_id':polissa_id,
                 'data_inicial': data_inici,
                 'data_final': data_final,
                 })
            return cursor.dictfetchall()


        def get_linies_tipus(factura_id, tipus):
            sql = """SELECT
                     giscedata_facturacio_factura_linia."price_unit_multi" AS giscedata_facturacio_factura_linia_price_unit_multi,
                     giscedata_facturacio_factura_linia."factura_id" AS giscedata_facturacio_factura_linia_factura_id,
                     giscedata_facturacio_factura_linia.tipus AS giscedata_facturacio_factura_linia_tipus,
                     giscedata_facturacio_factura_linia.multi AS giscedata_facturacio_factura_linia_multi,
                     account_invoice_line."name" AS account_invoice_line_name,
                     account_invoice_line."discount" AS account_invoice_line_discount,
                     account_invoice_line."quantity" AS account_invoice_line_quantity,
                     coalesce(product_uom."name",'') AS product_uom_name,
                     account_invoice_line."price_subtotal" AS account_invoice_line_price_subtotal
                FROM
                     "public"."account_invoice_line" account_invoice_line INNER JOIN "public"."giscedata_facturacio_factura_linia" giscedata_facturacio_factura_linia ON account_invoice_line."id" = giscedata_facturacio_factura_linia."invoice_line_id"
                     LEFT JOIN "public"."product_uom" product_uom ON account_invoice_line."uos_id" = product_uom."id"
                WHERE giscedata_facturacio_factura_linia.factura_id  = %s
                      and giscedata_facturacio_factura_linia.tipus in (%s)
                ORDER BY giscedata_facturacio_factura_linia.tipus,account_invoice_line.name

                """ % (factura_id,"'"+"','".join(tipus)+"'")
            cursor.execute(sql)
            return cursor.dictfetchall()


        def get_num_comptador(id_polissa):
            polissa_obj = pool.get('giscedata.polissa')
            polissa = polissa_obj.browse(cursor, uid, id_polissa)
            for comptador in polissa.comptadors:
                if comptador.active:
                   return comptador.name
            return '-'

        def get_lectures_periodes(factura_id):
            sql = """SELECT giscedata_facturacio_lectures_energia.name as periode
            FROM
                 giscedata_facturacio_lectures_energia giscedata_facturacio_lectures_energia
            WHERE giscedata_facturacio_lectures_energia.factura_id = %s
            GROUP BY giscedata_facturacio_lectures_energia.name
            ORDER BY giscedata_facturacio_lectures_energia.name""" % factura_id
            cursor.execute(sql)
            return cursor.dictfetchall()

        def get_lectures(factura_id):
            sql = """SELECT
                 giscedata_facturacio_lectures_energia.factura_id AS giscedata_facturacio_lectures_energia_factura_id,
                 giscedata_facturacio_lectures_energia.name AS giscedata_facturacio_lectures_energia_name,
                 giscedata_facturacio_lectures_energia.lect_actual AS giscedata_facturacio_lectures_energia_lect_actual,
                 giscedata_facturacio_lectures_energia.consum AS giscedata_facturacio_lectures_energia_consum,
                 giscedata_facturacio_lectures_energia.tipus AS giscedata_facturacio_lectures_energia_tipus,
                 giscedata_facturacio_lectures_energia.lect_anterior AS giscedata_facturacio_lectures_energia_lect_anterior
            FROM
                 giscedata_facturacio_lectures_energia giscedata_facturacio_lectures_energia
            WHERE giscedata_facturacio_lectures_energia.factura_id = %s
            ORDER BY giscedata_facturacio_lectures_energia.name,giscedata_facturacio_lectures_energia.tipus""" % factura_id

            cursor.execute(sql)
            return cursor.dictfetchall()

        def get_lectura_periode_tipus(factura_id, tipus, periode):
            sql = """SELECT
                 giscedata_facturacio_lectures_energia.factura_id AS giscedata_facturacio_lectures_energia_factura_id,
                 giscedata_facturacio_lectures_energia.name AS giscedata_facturacio_lectures_energia_name,
                 giscedata_facturacio_lectures_energia.lect_actual AS lect_actual,
                 giscedata_facturacio_lectures_energia.consum AS giscedata_facturacio_lectures_energia_consum,
                 giscedata_facturacio_lectures_energia.tipus AS giscedata_facturacio_lectures_energia_tipus,
                 giscedata_facturacio_lectures_energia.lect_anterior AS lect_anterior
            FROM
                 giscedata_facturacio_lectures_energia giscedata_facturacio_lectures_energia
            WHERE giscedata_facturacio_lectures_energia.factura_id = %s and giscedata_facturacio_lectures_energia.name = '%s'
                  and giscedata_facturacio_lectures_energia.tipus = '%s'
            ORDER BY giscedata_facturacio_lectures_energia.name,giscedata_facturacio_lectures_energia.tipus""" % (
            factura_id, periode, tipus)
            cursor.execute(sql)
            return cursor.dictfetchall()



        def get_giscedata_fact(invoice):
            sql = """SELECT * FROM giscedata_facturacio_factura WHERE id=%s""" % invoice.id
            cursor.execute(sql)
            return cursor.dictfetchall()

        def clean_vat(vat):
            if vat:
                return vat.replace('ES', '')
            else:
                return ''
     %>
    <%
        from datetime import datetime
        from bankbarcode.cuaderno57 import Recibo507
    %>
    %for inv in objects :
        <%
            def get_origen_lectura(cursor, uid, lectura):
                """Busquem l'origen de la lectura cercant-la a les lectures de facturació"""
                res = {lectura.data_actual: '',
                       lectura.data_anterior: ''}

                lectura_obj = lectura.pool.get('giscedata.lectures.lectura')
                tarifa_obj = lectura.pool.get('giscedata.polissa.tarifa')
                origen_obj = lectura.pool.get('giscedata.lectures.origen')

                # Recuperar l'estimada
                estimada_id = origen_obj.search(cursor, uid, [('codi', '=', '40')])[0]

                #Busquem la tarifa
                if lectura and lectura.name:
                    tarifa_id = tarifa_obj.search(cursor, uid, [('name', '=',
                                                            lectura.name[:-5])])
                else:
                    tarifa_id = False
                if tarifa_id:
                    tipus = lectura.tipus == 'activa' and 'A' or 'R'

                    search_vals = [('comptador', '=', lectura.comptador_id.id),
                                   ('periode.name', '=', lectura.name[-3:-1]),
                                   ('periode.tarifa', '=', tarifa_id[0]),
                                   ('tipus', '=', tipus),
                                   ('name', 'in', [lectura.data_actual,
                                                   lectura.data_anterior])]
                    lect_ids = lectura_obj.search(cursor, uid, search_vals)
                    lect_vals = lectura_obj.read(cursor, uid, lect_ids,
                                                 ['name', 'origen_comer_id', 'origen_id'])
                    for lect in lect_vals:
                        # En funció dels origens, escrivim el text
                        # Si Estimada (40)
                        # La resta: Real
                        origen_txt = _(u"real")
                        if lect['origen_id'][0] == estimada_id:
                            origen_txt = _(u"estimada")

                        res[lect['name']] = "%s" % (origen_txt)

                return res
            lectures_a = {}
            lectures_r = {}
            lectures_m = []

            for lectura in inv.lectures_energia_ids:
                origens = get_origen_lectura(cursor, uid, lectura)
                if lectura.tipus == 'activa':
                    lectures_a.setdefault(lectura.comptador_id.name,[])
                    lectures_a[lectura.comptador_id.name].append((lectura.name[-3:-1],
                                                        lectura.lect_anterior,
                                                        lectura.lect_actual,
                                                        lectura.consum,
                                                        lectura.data_anterior,
                                                        lectura.data_actual,
                                                        origens[lectura.data_anterior],
                                                        origens[lectura.data_actual],
                                                        ))
                elif lectura.tipus == 'reactiva':
                    lectures_r.setdefault(lectura.comptador_id.name,[])
                    lectures_r[lectura.comptador_id.name].append((lectura.name[-3:-1],
                                                        lectura.lect_anterior,
                                                        lectura.lect_actual,
                                                        lectura.consum,
                                                        lectura.data_anterior,
                                                        lectura.data_actual,
                                                        origens[lectura.data_anterior],
                                                        origens[lectura.data_actual],
                                                        ))

            for lectura in inv.lectures_potencia_ids:
                lectures_m.append((lectura.name, lectura.pot_contract,
                                   lectura.pot_maximetre, lectura.exces ))
            print lectures_a
        %>
    <div class="background">
    </div>
    <div class="wrapper">
        <div class="page_princial" >

            <%
                factura = get_giscedata_fact(inv)[0]

                dies_factura = 1 + (datetime.strptime(inv.data_final, '%Y-%m-%d') - datetime.strptime(inv.data_inici, '%Y-%m-%d')).days
                diari_factura_actual_eur = inv.total_energia / (dies_factura or 1.0)
                diari_factura_actual_kwh = (inv.energia_kwh * 1.0) / (dies_factura or 1.0)

                data_inici  = datetime.strptime(factura['data_inici'],'%Y-%m-%d')
                data_fi = datetime.strptime(factura['data_final'],'%Y-%m-%d')
                delta = data_fi - data_inici
                dies = delta.days
                periodes_a = sorted(list(set([lectura.name[-3:-1]
                                for lectura in inv.lectures_energia_ids
                                if lectura.tipus == 'activa'])))
                polissa = polissa_obj.browse(cursor, uid, inv.polissa_id.id,
                                 context={'date': inv.data_final.val})
            %>

            <% direccio = '%s %s %s %s %s %s %s' %(inv.polissa_id.cups.nv,
                                             inv.polissa_id.cups.pnp,
                                             inv.polissa_id.cups.es,
                                             inv.polissa_id.cups.pt,
                                             inv.polissa_id.cups.pu,
                                             inv.polissa_id.cups.cpo,
                                             inv.polissa_id.cups.cpa
                                             )
            %>

            <% setLang(inv.partner_id.lang) %>
            <!-- capcalera -->
            <div class="bloc capcalera">
                <div class="esquerra">
                    <img class="logo" src="${addons_path}/giscedata_facturacio_distri_ab/report/abenergia-logo.png">
                </div>
                <div class="dreta">
                   <div class="box">
                        <table class="dades">
                            <tr><td ><b>${inv.partner_id.title or ''|entity}  ${inv.partner_id.name |entity}</b></td></tr>
                            <tr><td>${inv.address_invoice_id.street or ''|entity}</td></tr>
                            <tr><td>${inv.address_invoice_id.street2 or ''|entity}</td></tr>
                            <tr><td>${inv.address_invoice_id.zip or ''|entity} ${inv.address_invoice_id.city or ''|entity}</td></tr>
                        </table>
                    </div>
                </div>
                <div class="clear fi_bloc"></div>
            </div><!-- Fi capcalera -->
            <!-- part central -->
            <div class="bloc part_central">
                <div id="outer">
                    <p id="text">
                    Eléctrica de Barbastro, S.A. Inscrita en el Reg. Mercantil de Huesca Tomo: 612. Libro: 0, Folio: 44. Sección: 8, Hoja: HU-3777. Inscripción: 43 - CIF: A22194856. Domicilio social: Escuelas Pías, 12, 22300 Barbastro
                    </p>
                </div>
                <div class="esquerra">
                    <div class="header box">Datos de la factura </div>
                    <div class="box">
                        <table class="dades">
                            <tr class="header">
                                <td>Núm. Factura</td>
                                <td>Fecha</td>
                                <td>Importe</td>
                                <td>NIF/DNI</td>
                            </tr>
                            <tr class="response">
                                <td>${inv.number or ''|entity}</td>
                                <td>${inv.date_invoice}</td>
                                <td>${formatLang(inv.amount_total, digits=2)}</td>
                                <td>${clean_vat(inv.partner_id.vat)}</td>
                            </tr>
                            %if inv.tipo_rectificadora in ['B', 'A', 'BRA']:
                                <tr class="response"><td colspan="4">Esta factura anula la factura ${inv.ref.number or ''}</td></tr>
                            %endif

                            %if inv.tipo_rectificadora in ['R', 'RA']:
                                <tr class="response"><td colspan="4">Esta factura rectifica la factura ${inv.ref.number or ''}</td></tr>
                            %endif
                            </table>
                        <table class="dades">
                            <tr class="header">
                                <td>Póliza Abonado</td>
                                <td>Código cliente</td>
                                <td>C.N.A.E</td>
                            </tr>
                            <tr class="response">
                                <td>${inv.polissa_id.name}</td>
                                <td>${inv.polissa_id.titular.ref}</td>
                                <td>${polissa.cnae.name}</td>
                            </tr>
                            </table>
                        <table class="dades">
                            <tr class="header"><td colspan="2">C.U.P.S.</td></tr>
                            <tr class="response"><td colspan="2" class="desc">${polissa.cups.name}</td></tr>
                            <tr class="header" ><td colspan="2">Periodo de Facturación</td></tr>
                            <tr class="response"><td colspan="2"> Del ${pretty_date(factura['data_inici'])|entity} al ${pretty_date(factura['data_final'])|entity}</td></tr>
                        </table>
                    </div>
                    <div class="header box"> Datos del suministro </div>
                    <div class="box">
                        <table class="dades">
                            <tr><td class="desc">Titular y domicilio del suministro</td></tr>
                            <tr>
                                <td >
                                    ${inv.partner_id.title or ''|entity}  ${inv.polissa_id.titular.name |entity}
                                    <br>
                                ${inv.polissa_id.cups.direccio}
                                </td>
                            </tr>
                            <tr><td class="desc">Ref. Catastral</td></tr>
                            %if inv.polissa_id.cups.ref_catastral:
                                <tr><td>${inv.polissa_id.cups.ref_catastral}</td></tr>
                            %endif
                         </table>
                    </div>
                    <div class="header box"> Datos de la contratación </div>
                    <div class="box">
                        <table class="dades">
                            <tr>
                                <td >Tarifa:</td><td class="desc" >${polissa.tarifa.name or ''|entity}</td>
                                <td >BOE:</td><td class="desc">${inv.date_boe or ''|entity}</td>
                            </tr>
                        </table>

                            %for comptador in sorted(lectures_a):
                                <table class="dades">
                                    <tr>
                                        <td colspan="4">Contador <span class="desc">${comptador or ''|entity}</span></td>
                                    </tr>
                                    <% lect = lectures_a[comptador][0]    %>
                                </table>
                                <table class="dades">
                                    <tr>
                                        <td >Lect. anterior</td>
                                        <td >Lect. actual</td>
                                        <td >Tipo ant.</td>
                                        <td >Tipo act.</td>
                                    </tr>
                                    <tr>
                                        <td >${lect[4]}</td>
                                        <td >${lect[5]}</td>
                                        <td >${lect[6]}</td>
                                        <td >${lect[7]}</td>
                                    </tr>
                                </table>
                            %endfor
                        <table class="dades">
                            <tr>
                                <td>Tensió suministro <span class="desc">${polissa.tensio or ''|entity}</span></td>
                            </tr>
                         </table>
                    </div>
                    <div class="header box"> Datos del pago </div>
                    <div class="box">
                        <strong>El importe de ha de hacer efectivo a</strong><br>
                        Oficinas Eléctrica de Barbastro, S.A.<br>
                        Escuelas Pías, 12<br>
                        <strong>Plazo de pago</strong>

                    </div>
                    <div class="header box margin-0"> Resumen de Lecturas Tarifa Accesso </div>
                    <div >
                        <table class="dades_lectures" >
                           <colgroup>
                              <col style="width: 15%">
                              <col style="width: 15%">
                              <col style="width: 15%">
                              <col style="width: 15%">
                           </colgroup>
                           <thead class="border-bottom">
                              <tr class="head_table">
                                 <th colspan="4">Activa</th>
                              </tr>
                              <tr>
                                 <th>Periode</th>
                                 <th>Anterior</th>
                                 <th>Actual</th>
                                 <th class="border-right">Consumo</th>
                              </tr>
                           </thead>
                           <tbody>
                                % for comptador, lects in lectures_a.items():
                                   %for lect in lects:
                                   <tr>
                                     <td class="border-right">${lect[0]}</td>
                                     <td>${int(lect[1])}</td>
                                     <td>${int(lect[2])}</td>
                                     <td class="border-right">${int(lect[3])}</td>
                                    </tr>
                                    %endfor
                                %endfor
                           </tbody>
                        </table>
                        <table class="dades_lectures">
                           <colgroup>
                              <col style="width: 15%">
                              <col style="width: 15%">
                              <col style="width: 15%">
                              <col style="width: 15%">
                           </colgroup>
                           <thead class="border-bottom">
                              <tr class="head_table">
                                 <th colspan="4">Reactiva</th>
                              </tr>
                              <tr>
                                 <th>Periodo</th>
                                 <th>Anterior</th>
                                 <th>Actual</th>
                                 <th class="border-right">Consumo</th>
                              </tr>
                           </thead>
                           <tbody>
                                % for comptador, lects in lectures_r.items():
                                   %for lect in lects:
                                   <tr>
                                     <td class="border-right">${lect[0]}</td>
                                     <td>${int(lect[1])}</td>
                                     <td>${int(lect[2])}</td>
                                     <td class="border-right">${int(lect[3])}</td>
                                    </tr>
                                    %endfor
                                %endfor
                           </tbody>
                        </table>
                        <table class="dades_lectures">
                           <colgroup>
                              <col style="width: 15%">
                              <col style="width: 15%">
                              <col style="width: 15%">
                              <col style="width: 15%">
                           </colgroup>
                           <thead class="border-bottom">
                              <tr class="head_table">
                                 <th colspan="4">Potencia</th>
                              </tr>
                              <tr>
                                 <th>Periodo</th>
                                 <th>Potencia</th>
                                 <th>Máximetro</th>
                                 <th>Exceso</th>
                              </tr>
                           </thead>
                           <tbody>
                                % for lect in lectures_m:
                                   <tr>
                                     <td>${lect[0]}</td>
                                     <td>${lect[1]}</td>
                                     <td>${lect[2]}</td>
                                     <td >${lect[3]}</td>
                                    </tr>
                                %endfor
                           </tbody>
                        </table>
                    </div>
                </div>
                <div class="dreta">
                    <div class="header box"> Cálculo de la factura </div>
                    <div class="box dades_calculs">
                        <div class="calculs">
                            <div class="titol_taula">Término de Potencia</div>
                            <table class="dades_calculs"  >
                               <colgroup>
                                  <col style="width: 10%">
                                  <col style="width: 60%">
                                  <col style="width: 10%">
                                  <col style="width: 15%">
                               </colgroup>
                               <tbody>
                                  %for pot in get_linies_tipus(factura['id'], ['potencia']):
                                      <tr>
                                         <td>${pot['account_invoice_line_name']}</td>
                                         <td colspan="2">${pot['account_invoice_line_quantity']} KW x ${pot['giscedata_facturacio_factura_linia_multi']} Días x ${pot['giscedata_facturacio_factura_linia_price_unit_multi']} €</td>
                                         <td class="total">${formatLang(pot['account_invoice_line_price_subtotal'], digits=2)}</td>
                                      </tr>
                                  %endfor
                               </tbody>
                            </table>
                        </div>
                        <div class="calculs">
                            <div class="titol_taula">Excesos de potencia (Maximetros)</div>
                            <table class="dades_calculs"  >
                               <colgroup>
                                  <col style="width: 10%">
                                  <col style="width: 50%">
                                  <col style="width: 15%">
                                  <col style="width: 25%">
                               </colgroup>
                               <tbody>
                                  <% x = 0 %>
                                  %for pot in get_linies_tipus(factura['id'], ['exces_potencia']):
                                      <% x += 1 %>
                                      <tr>
                                         <td>${pot['account_invoice_line_name']}</td>
                                         <td colspan="2">${pot['account_invoice_line_quantity']} ${pot['product_uom_name']} x ${pot['giscedata_facturacio_factura_linia_multi']} x ${pot['giscedata_facturacio_factura_linia_price_unit_multi']} €</td>
                                         <td class="total">${pot['account_invoice_line_price_subtotal']}</td>
                                      </tr>
                                  %endfor
                                  % if x == 0:
                                    <tr> <td></td><td></td><td></td><td></td></tr>
                                  % endif
                               </tbody>
                            </table>
                        </div>
                        <div class="calculs">
                            <div class="titol_taula">Término Energía</div>
                            <table class="dades_calculs"  >
                               <colgroup>
                                  <col style="width: 10%">
                                  <col style="width: 50%">
                                  <col style="width: 15%">
                                  <col style="width: 25%">
                               </colgroup>
                               <tbody>
                                  <% x = 0 %>
                                  %for ene in get_linies_tipus(factura['id'], ['energia']):
                                      <% x += 1 %>
                                      <tr>
                                         <td>${ene['account_invoice_line_name']}</td>
                                         <td colspan="2">${ene['account_invoice_line_quantity']} kWh x ${ene['giscedata_facturacio_factura_linia_price_unit_multi']} €
                                             %if ene['account_invoice_line_discount'] != 0:
                                               - ${formatLang(ene['account_invoice_line_discount'], digits=2)}% Dto
                                             %endif
                                         </td>
                                         <td class="total">${formatLang(ene['account_invoice_line_price_subtotal'], digits=2)}</td>
                                      </tr>
                                  %endfor
                                  % if x == 0:
                                    <tr> <td></td><td></td><td></td><td></td></tr>
                                  % endif
                               </tbody>
                            </table>
                        </div>
                        <div class="reactiva">
                            <div class="titol_taula">Término de Reactiva</div>
                            <table class="dades_calculs" >
                               <colgroup>
                                  <col style="width: 10%">
                                  <col style="width: 50%">
                                  <col style="width: 15%">
                                  <col style="width: 25%">
                               </colgroup>
                               <tbody>

                                  <% x = 0 %>
                                  %for ene in get_linies_tipus(factura['id'], ['reactiva']):
                                      <% x += 1 %>
                                      <tr>
                                         <td>${ene['account_invoice_line_name']}</td>
                                         <td colspan="2">${ene['account_invoice_line_quantity']} kVArh x ${ene['giscedata_facturacio_factura_linia_price_unit_multi']} €
                                             %if ene['account_invoice_line_discount'] != 0:
                                               - ${formatLang(ene['account_invoice_line_discount'], digits=2)}% Dto
                                             %endif

                                         </td>
                                         <td class="total">${formatLang(ene['account_invoice_line_price_subtotal'], digits=2)}</td>
                                      </tr>
                                  %endfor
                                  % if x == 0:
                                    <tr> <td></td><td></td><td></td><td></td></tr>
                                  % endif
                               </tbody>
                            </table>
                            <div class="dades_impostos" style="position: absolute; bottom: 0">
                                <table class="dades_impostos" >
                                    <colgroup>
                                        <col style="width: 85%">
                                        <col style="width: 15%">
                                    </colgroup>
                                    <tbody>
                                    %if inv.tax_line :
                                      <%
                                          base = 0.0
                                          total_iese = 0.0
                                      %>
                                        %for t in inv.tax_line :
                                          %if 'especial sobre la electricidad'.upper() in t.name.upper():
                                                <%
                                                    base +=  t.base
                                                    total_iese += t.amount
                                                %>
                                          %endif
                                        %endfor
                                          %if total_iese:
                                              <tr>
                                                  <td colspan="2">
                                                      <div class="titol_taula">Impuesto Electricidad</div>
                                                  </td>
                                              </tr>
                                              <tr>
                                                <td>5,11269632 % sobre ${base}€</td>
                                                <td class="total">${total_iese}</td>
                                              </tr>
                                          %endif
                                    %endif
                                    %for alq in get_linies_tipus(factura['id'], ['lloguer']):
                                        <tr>
                                            <td>Importe alquiler</td>
                                            <td class="total">${formatLang(alq['account_invoice_line_price_subtotal'], digits=2)}</td>
                                        </tr>
                                    %endfor
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="box">
                        <table class="imports_total">
                           <colgroup>
                              <col style="width: 50%">
                              <col style="width: 20%">
                              <col style="width: 15%">
                              <col style="width: 20%">
                           </colgroup>
                           <tbody>
                              %if inv.tax_line :
                                %for t in inv.tax_line :
                                    %if 'IVA' in t.name.upper():
                                    <tr>
                                     <td>BASE IMPONIBLE</td>
                                     <td class="requadre">${ formatLang(t.base, digits=2)|entity}</td>
                                     <td class="iva">${ t.name.upper().replace('IVA','') }</td>
                                     <td class="total">${ formatLang(t.amount, digits=2) }</td>
                                    </tr>
                                  %endif
                                %endfor
                                %endif
                              <tr>
                                     <th colspan="3">IMPORTE TOTAL EUROS</td>
                                     <td  class="total bold">${ formatLang(inv.amount_total, digits=2)|entity}</td>
                                    </tr>
                           </tbody>
                        </table>
                    </div>
                    <div class="box">
                        <table class="dades dades_bancaries">
                            <tr><td class="desc">Forma de pago:</td><td >${polissa.tipo_pago.name or ''|entity}</td></tr>

                         </table>
                    </div>
                </div>
                <div class="clear fi_bloc"></div>
            </div> <!-- part central -->
        </div>
        <%
        comptador_factures += 1;
        %>
        % if comptador_factures<len(objects):
            <p style="page-break-after:always"></p>
        % endif
    </div>
    %endfor
</body>
</html>