<%
    from operator import attrgetter, itemgetter
    from datetime import datetime, timedelta
    import json
%>
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<head>
    <style type="text/css">
        ${css}
    </style>
    <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_distri_ab/report/factura_concepto.css"/>
</head>
<body>
    <%
        pool = objects[0].pool
        polissa_obj = objects[0].pool.get('giscedata.polissa')
        comptador_factures = 0
        from datetime import datetime

        def clean(text):
            return text or ''
    %>
     %for inv in objects :
    <div class="wrapper">
        <div class="page_princial">

            <%
                altres_lines = [l for l in inv.linia_ids]
                total_altres = sum([l.price_subtotal for l in altres_lines])

                periodes_a = sorted(list(set([lectura.name[-3:-1]
                                for lectura in inv.lectures_energia_ids
                                if lectura.tipus == 'activa'])))
                polissa = polissa_obj.browse(cursor, uid, inv.polissa_id.id)
                setLang(inv.partner_id.lang)
            %>
            <!-- capcalera -->
            <div class="bloc capcalera">
                <div class="esquerra">
                    <img class="logo" src="${addons_path}/giscedata_facturacio_distri_ab/report/abenergia-logo.png">
                </div>
                <div class="dreta">
                   <div class="box address">
                       <p>${inv.partner_id.name}</p>
                       <p>${inv.address_invoice_id.street}</p>
                       <div>
                           <span style="float:left; max-width: 100%; ">${inv.address_invoice_id.zip}</span>
                           <span style="float:left; max-width: 100%; display:block; padding-left: 15px;">
                            ${inv.address_invoice_id.id_poblacio.name}
                               <br>
                            ${inv.address_invoice_id.state_id.name}
                           </span>
                       </div>
                    </div>
                </div>
                <div class="clear fi_bloc"></div>
            </div>
            <!-- Fi capcalera -->
            <!-- part central -->
            <div class="bloc part_central">
                <div class="outer">
                    <p class="text">
                    Eléctrica de Barbastro, S.A. Inscrita en el Reg. Mercantil de Huesca Tomo: 612. Libro: 0, Folio: 44. Sección: 8, Hoja: HU-3777. Inscripción: 43 - CIF: A22194856. Domicilio social: Escuelas Pías, 12, 22300 Barbastro
                    </p>
                </div>
                <div class="conceptos">
                    <table class="desc_factura">
                        <thead>
                            <tr>
                                <td>Factura Nº</td>
                                <td>Fecha</td>
                                <td>DNI/CIF</td>
                                <td>Nº Póliza</td>
                                <td>Fecha Alta</td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>${clean(inv.number)}</td>
                                <td>${inv.date_invoice}</td>
                                <td>${inv.partner_id.vat.lstrip('ES')} ${clean(inv.partner_id.name)}</td>
                                <td>${clean(polissa.name)}</td>
                                <td>${formatLang(polissa.data_alta, date=True)}</td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="datos_ps">
                        <thead>
                            <tr>
                                <td colspan="2">Datos del Punto de Suministro</td>
                            </tr>
                            <tr>
                                <td>Nombre del Titular</td>
                                <td>Ciudad</td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>${clean(polissa.titular.name)}</td>
                                <td>${clean(polissa.cups.id_poblacio.name)}</td>
                            </tr>
                            <tr class="fake_head">
                                <td>Domicilio</td>
                                <td>CUPS</td>
                            </tr>
                            <tr>
                                <td>${clean(polissa.cups.direccio)}</td>
                                <td>${clean(polissa.cups.name)}</td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="content-concepts">
                        <table class="concepts">
                            <thead>
                                <tr>
                                    <td class="concept">Descripción del Concepto</td>
                                    <td class="iva">% I.V.A.</td>
                                    <td class="import">Importe</td>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    iva_set = set()
                                %>
                                %for l in altres_lines:
                                    <tr>
                                        <td>${l.name}</td>
                                        <td class="number">
                                            %for tax in l.invoice_line_tax_id:
                                                %if 'IVA' in tax.name:
                                                    ${formatLang(tax.amount * 100.0)}
                                                    <%
                                                        iva_set.add(tax.amount * 100.0)
                                                    %>
                                                %endif
                                            %endfor
                                        </td>
                                        <td class="number">${"%s" % formatLang(l.price_subtotal)}</td>
                                    </tr>
                                %endfor
                            </tbody>
                        </table>
                    </div>
                    <table class="summary">
                        <thead>
                            <tr>
                                <td>Suma Importes</td>
                                <td>Base Imponible</td>
                                <td>% IVA</td>
                                <td>Importe IVA</td>
                                <td class="total">TOTAL FACTURA</td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="number">${formatLang(total_altres)}</td>
                                <td class="number">${formatLang(inv.amount_untaxed)}</td>
                                <td class="number">${len(list(iva_set)) and formatLang(list(iva_set)[0]) or ''}</td>
                                <td class="number">${formatLang(inv.amount_tax)}</td>
                                <td class="number total">${formatLang(inv.amount_total)} €</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- part central -->
            <!-- part peu -->
            <div class="peu">
                <div class="esquerra">
                    <table>
                        <tbody>
                            <tr>
                                <td>Forma de pago</td>
                                <td>${clean(inv.polissa_id.tipo_pago.name)}</td>
                            </tr>
                            <tr>
                                <td>Domiciliación</td>
                                %if inv.polissa_id.bank:
                                    ${inv.polissa_id.bank.name}
                                %else:
                                    <td></td>
                                %endif

                            </tr>
                            <tr>
                                <td>Nº de Cuenta</td>
                                %if inv.polissa_id.bank:
                                    <td>${inv.polissa_id.bank.iban[4:]}</td>
                                %else:
                                    <td></td>
                                %endif
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="dreta">
                </div>
                <div class="clear fi_bloc"></div>
            </div>
            <!-- fi peu -->
            <%
            comptador_factures += 1;
            %>
            % if comptador_factures<len(objects):
                <p style="page-break-after:always;"></p>
            % endif

        </div> <!-- page -->
      </div> <!-- wrapper -->
    %endfor
</body>
</html>
