# -*- coding: utf-8 -*-
{
    "name": "Reports Facturació ABenergia (Distribuidora)",
    "description": """Reports Facturació ABenergia (Distribuidora) i custom facturació ab""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Extrareports",
    "depends":[
        "base",
        "giscedata_facturacio_distri",
        "giscedata_polissa_distri",
        "c2c_webkit_report",
        "giscedata_facturacio"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_facturacio_distri_data.xml",
        "giscedata_facturacio_distri_report.xml"
    ],
    "active": False,
    "installable": True
}
