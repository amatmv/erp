# -*- coding: utf-8 -*-
{
    "name": "crm_comercial_custom_ab",
    "description": """
        Personalització entorn CRM per comercial
        """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "CRM",
    "depends":[
        "board",
        "crm_elec",
        "crm_elec_unpaid_invoice",
        "giscedata_switching",
        "giscedata_polissa",
        "giscedata_facturacio_comer",
        "giscedata_polissa_custom_ab",
        "giscedata_energia_prevista",
        "notifications",
        "notifications_sms",
        "sms_template",
        "comercial_colaboraciones"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "crm_contratacion_board.xml",
        "portal_data.xml",
        "crm_administracion.xml",
        "security/crm_comercial_custom_ab_security.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
