from osv import osv, fields


class GiscedataPolissa(osv.osv):
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def _default_name(self, cursor, uid, context=None):
        if context is None:
            context = {}
        seq_obj = self.pool.get('ir.sequence')
        return seq_obj.get(cursor, uid, 'giscedata.polissa')

    def copy(self, cursor, uid, id, default=None, context=None):
        """El copy de giscedata.polissa treu el name (sequencia). Li afegim."""
        res_id = super(GiscedataPolissa, self).copy(cursor, uid, id, default,
                                                    context)
        if not default.get('name', False):
            n_pol = self.pool.get('ir.sequence').get(cursor,uid,
                                                 'giscedata.polissa')
        polissa_obj = self.pool.get('giscedata.polissa')
        polissa_obj.write(cursor, uid, res_id, {'name': n_pol})
        return res_id

    _columns = {
        'titular': fields.many2one(
            'res.partner', 'Titular', readonly=True,
            states={
                'esborrany':[('readonly', False)],
                'validar': [
                    ('readonly', False),
                    ('required', True)
                    ],
                'modcontractual': [
                    ('readonly', False),
                    ('required', True)
                    ]
                }, size=40)
    }

    _defaults = {
        'name': _default_name,
    }

GiscedataPolissa()