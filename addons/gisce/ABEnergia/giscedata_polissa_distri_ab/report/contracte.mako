<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<head>
		<style type="text/css">
	    ${css}
            .esquerra{
                float: left;
                width: 49%;
                margin-right: 12px;
            }
            .dreta{
                float: left;
                width: 49%;
            }
            .margin-top-5,
            table.open-section{
                margin-top: 5px;
            }
            .col_nif{
                width: 130px;
            }
            table.joined{
                margin-top: -1px;
            }
            .check{
                font-size: 12px;
            }
            .titol{
                margin-top:10px;
                margin-bottom: 10px;
            }
            .seccio{
                margin-top:5px;
            }
            .seccio .header{
                margin-bottom: 5px;
            }
            td{
                padding:5px;
            }
            .num_contracte {
                padding-top: 25%;
                float: right;
                font-size: 1.2em;
            }
	    </style>
	</head>
    <%
        from collections import namedtuple
        from datetime import datetime
        from giscedata_polissa.report.utils import datetime_to_date

        def get_mes(month):
          months = {
            '1': 'ENERO','2': 'FEBRERO','3': 'MARZO',
            '4': 'ABRIL','5': 'MAYO','6': 'JUNIO',
            '7': 'JULIO','8': 'AGOSTO','9': 'SEPTIEMBRE',
            '10': 'OCTUBRE','11': 'NOVIEMBRE','12': 'DICIEMBRE',
          }
          return months[str(month)]

        def pretty_date(data, type='text'):
            any, mes, dia = data.split('-')
            if type == 'normal':
                return '%s/%s/%s' %(dia, mes, any)
            elif type == 'text':
                return '%s de %s de %s' %(dia, get_mes(int(mes)).capitalize(), any)

        Direccio = namedtuple('Direccio',
                      ['tv', 'nv', 'pnp', 'pt', 'es', 'pu', 'cpo', 'cpa',
                       'ref_catastral', 'aclarador'])
        def get_direccio_cups(cups):
            direccio = Direccio(
                tv=cups.tv,
                nv=cups.nv,
                pnp=cups.pnp,
                pt=cups.pt,
                es=cups.es,
                pu=cups.pu,
                cpo=cups.cpo,
                cpa=cups.cpa,
                ref_catastral=cups.ref_catastral,
                aclarador=cups.aclarador
            )
            return direccio

        def clean_text(text):
            return text or ''
        comptador_polisses = 0
    %>
	<%def name="header_contracte(polissa)">
		<div id="header">
            <div>
                <div class="esquerra">
                    <div id="logo">
                        <img src="${addons_path}/giscedata_polissa_distri_ab/report/abenergia-logo.png"/>
                    </div>
                </div>
                <div class="dreta">
                    <div class="num_contracte">
                        Número de contrato: <strong>${polissa.name}</strong>
                    </div>
                </div>
            </div>
            <div style="clear:both"></div>
            <div class="titol">${_("CONTRATO DE ACCESO A LA RED DE DISTRIBUCIÓN EN BAJA TENSIÓN")}</div>
            <div style="clear:both"></div>
		</div>


	</%def>
	<%def name="clean(text)">
		${text or ''}
	</%def>

    <%def name="enviament(diferent, text)">
        %if not diferent:
            ${clean(text)}
        %endif
    </%def>

	<body>
		%for polissa in objects:
        	${header_contracte(polissa)}
        	<div class="seccio">
        		<div class="header">
                    ${_("DISTRIBUIDORA")}
                </div>
        		<table class="open-section">
        			<tr>
        				<td>
        					<span class="label">${_("Nombre/Razón Social:")}</span>
        					<span class="field">${polissa.distribuidora.name}</span>
        				</td>
        				<td class="col_nif">
        					<span class="label">${_("NIF/CIF:")}</span>
        					<span class="field">${polissa.distribuidora.vat and polissa.distribuidora.vat.replace('ES', '') or ''}</span>
        				</td>
        			</tr>
        		</table>
        		<table class="joined">
                    <% direccio_titular = polissa.distribuidora.address[0]%>
        			<tr>
        				<td>
        					<span class="label">${_("Dirección:")}</span>
        					<span class="field">${clean(direccio_titular.tv.abr)} ${direccio_titular.nv}</span>
        				</td>
        				<td style="width: 250px;">
        					<span class="label">${_("Nº:")}</span>
        					<span class="field">
                                ${clean(direccio_titular.pnp)} ${clean(direccio_titular.pt)} ${clean(direccio_titular.aclarador)}
                            </span>
        				</td>
        			</tr>
        			<tr>
        				<td>
        					<span class="label">${_("Código Postal/Población:")}</span>
        					<span class="field">${clean(direccio_titular.zip)} ${clean(direccio_titular.id_poblacio.name)}</span>
        				</td>
        				<td style="width: 250px;">
        					<span class="label">${_("Provincia/Pais:")}</span>
        					<span class="field">${clean(direccio_titular.state_id.name)} ${clean(direccio_titular.country_id.name)}</span>
        				</td>
        			</tr>
        		</table>
        		<table class="joined">
        			<tr>
        				<td style="width: 300px;">
        					<span class="label">${_("Teléfono:")}</span>
        					<span class="field">${clean(direccio_titular.phone)}</span>
        				</td>
        				<td>
        					<span class="label">${_("Teléfono 2:")}</span>
        					<span class="field">${clean(direccio_titular.mobile)}</span>
        				</td>
        				<td style="width: 150px;">
        					<span class="label">${_("Fax:")}</span>
        					<span class="field">${clean(direccio_titular.fax)}</span>
        				</td>
        			</tr>
                </table>
                <table class="joined">
        			<tr>
        				<td>
        					<span class="label">${_("E-Mail:")}</span>
        					<span class="field">${clean(direccio_titular.email)}</span>
        				</td>
        				<td>
        					<span class="label">${_("Referencia Registral:")}</span>
        					<span class="field">R1-055</span>
        				</td>
        			</tr>
        		</table>
                <table class="open-section">
        			<tr>
        				<td>
        					Ref. Registral T463 L140 F119 HU3777-21, Persona fisica que lo representa: PABLO BRAVO VIDAL, DNI: 18014266E, según escritura de porder otorgada
ante el notario de Barcelona D. Enrique Jaime Ventura Paradinas Lopez en fecha 15/11/2011 y con protrocolo nº1162
        				</td>
        			</tr>
        		</table>
        	</div>
        	<br>
        	<div class="seccio">
        		<div class="header" >
                    ${_("COMERCIALIZADORA")}
        		</div>
                <%
                    direccio_pagament = polissa.direccio_pagament
                %>
        		<table class="open-section">
        			<tr>
        				<td>
        					<span class="label">${_("Nombre/Razón Social:")}</span>
        					<span class="field">${polissa.pagador.name}</span>
        				</td>
                        <td class="col_nif">
        					<span class="label">${_("NIF/CIF:")}</span>
        					<span class="field">${polissa.pagador.vat and clean_text(polissa.pagador.vat.replace('ES', ''))}</span>
        				</td>
        			</tr>
        		</table>
        		<table class="joined">
        			<tr>
        				<td>
        					<span class="label">${_("Dirección:")}</span>
        					<span class="field">
                                ${'{0} {1}'.format(
                                    clean_text(direccio_pagament.tv.abr), clean_text(direccio_pagament.nv)
                                )}
                            </span>
        				</td>
        				<td style="width: 250px;">
        					<span class="label">${_("Nº:")}</span>
        					<span class="field">
                                ${'{0} {1} {2} {3}'.format(
                                    clean_text(direccio_pagament.pnp),
                                    clean_text(direccio_pagament.es),
                                    clean_text(direccio_pagament.pt),
                                    clean_text(direccio_pagament.aclarador)
                                )}
                            </span>
        				</td>
        			</tr>
        			<tr>
        				<td>
        					<span class="label">${_("Código Postal/Población:")}</span>
        					<span class="field">
                                ${'{0} {1}'.format(
                                    clean_text(direccio_pagament.zip),
                                    clean_text(direccio_pagament.id_poblacio.name)
                                )}
                            </span>
        				</td>
        				<td style="width: 250px;">
        					<span class="label">${_("Provincia/Pais:")}</span>
        					<span class="field">
                                ${'{0} {1}'.format(
                                    clean_text(direccio_pagament.state_id.name), clean_text(direccio_pagament.country_id.name)
                                )}
                            </span>
        				</td>
        			</tr>
        		</table>
        		<table class="joined">
        			<tr>
        				<td>
        					<span class="label">${_("Teléfono:")}</span>
        					<span class="field">${clean_text(direccio_pagament.phone)}</span>
        				</td>
        				<td style="width: 150px;">
        					<span class="label">${_("Fax:")}</span>
        					<span class="field">${clean_text(direccio_pagament.fax)}</span>
        				</td>
        			</tr>
        			<tr>
        				<td>
        					<span class="label">${_("E-Mail:")}</span>
        					<span class="field">${enviament(diferent,
                                '{0}'.format(
                                    clean_text(direccio_pagament.email)
                                )
                            )}</span>
        				</td>
                        <td style="width: 50%;">
        					<span class="label">${_("Referencia registral:")}</span>
        					<span class="field">${clean_text(polissa.pagador.ref2)}</span>
        				</td>
        			</tr>
        		</table>
        	</div>
        	<div class="titol">
        		${_("CONDICIONES ESPECÍFICAS DEL CONTRATO")}
        	</div>
        	<div class="seccio">
        		<div class="header" >
                    ${_("DATOS DEL PUNTO DE SUMINISTRO")}
                </div>
                <% direccio_ps = polissa.cups %>
        		<table class="open-section">
                    <tr>
                        <td>
        					<span class="label">${_("Consumidor cualificado:")}</span>
        					<span class="field">${polissa.titular.name}</span>
        				</td>
        				<td class="col_nif">
        					<span class="label">${_("NIF/CIF:")}</span>
        					<span class="field">${clean_text(polissa.titular.vat.replace('ES', ''))}</span>
        				</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <span class="label">${_("Domicilio Social:")}</span>
        					<span class="field">${polissa.titular.address[0].street}</span>
                        </td>
                    </tr>
                </table>
                <table class="joined">
                    <tr>
        				<td>
        					<span class="label">${_("Dirección de Suministro:")}</span>
        					<span class="field">
                                ${clean(direccio_ps.tv.abr)} ${clean(direccio_ps.nv)},
                                ${clean(direccio_ps.pnp)} ${clean(direccio_ps.pt)} ${clean(direccio_ps.aclarador)}
                                ${clean(direccio_ps.cpo)} ${clean(direccio_ps.cpa)}
                            </span>
        				</td>
                        <td>
        					<span class="label">${_("Código Postal/Población:")}</span>
        					<span class="field">${clean(direccio_ps.dp)} ${clean(direccio_ps.id_poblacio.name)}</span>
        				</td>
        			</tr>
                </table>
                <table class="joined">
        			<tr>
        				<td>
        					<span class="label">${_("CUPS:")}</span>
        					<span class="field">${polissa.cups.name}</span>
        				</td>
                        <td>
                            <span class="label">${_("Fecha del contrato del suministro de energía subscrito con la comercializadora:")}</span>
                            <span class="field">${formatLang(datetime_to_date(polissa.data_firma_contracte) or polissa.data_alta, date=True)}</span>
                        </td>
                    </tr>
                </table>
                <table class="joined">
        			<tr>
        				<td>
                            %if polissa.tarifa_codi.startswith(('3.1A', '6.')):
                                Baja tensión <span class="check">&#x2610;</span>  Alta tensión <span class="check">&#9746;</span>
                            %else:
                                Baja tensión <span class="check">&#9746;</span>  Alta tensión <span class="check">&#x2610;</span>
                            %endif
        				</td>
        				<td>
        					<span class="label">${_("Tensión Nominal(V):")}</span>
        					<span class="field">${clean(polissa.tensio)}</span>
        				</td>
        			</tr>
        		</table>
        		<table class="open-section">
        			<tr>
        				<td>
        					<span class="label">${_("Tarifa de acceso:")}</span>
        					<span class="field">${clean(polissa.tarifa_codi)}</span>
        				</td>
        				<td>
        					<span class="label">${_("Potencia Contratada(kW):")}</span>
        					<span class="field">${clean(formatLang(polissa.potencia, digits=3))}</span>
        				</td>
                        <%
                            potencies = polissa.potencies_periode
                            periodes = []
                            for i in range(0, 6):
                                if i < len(potencies):
                                    periode = potencies[i]
                                else:
                                    periode = False
                                periodes.append((i+1, periode))
                        %>
                        %for p in periodes:
                            %if p[1]:
                            <td>
                                <span class="label">${"P{0}".format(p[0])}</span>
                                <span class="field">${p[1] and formatLang(p[1].potencia, digits=3) or ' '}</span>
                            </td>
                            %endif
                        %endfor
        			</tr>
        		</table>
                <table class="joined">
                    <tr>
        				<td>
        					<span class="label">${_("CNAE:")}</span>
        					<span class="field">${clean(polissa.cnae.name)}</span>
        				</td>
        				<td>
        					<span class="label">${_("Actividad Principal:")}</span>
        					<span class="field">${clean(polissa.cnae.descripcio)}</span>
        				</td>
        			</tr>
                </table>
        	</div>
        	<div class="seccio">
        		<div class="header">
                    ${_("EQUIPO DE MEDIDA")}:
        		</div>
        		<table class="open-section">
                    <tr>
                        <td colspan="2">
                            <span class="label">${_("Nombre del Instalador Autorizado:")}</span>
                            <span class="field">${polissa.butlletins and clean_text(polissa.butlletins[-1].partner_id.name)}</span>
                        </td>
                        <td>
                            <span class="label">${_("Referencia No:")}</span>
                            <span class="field">${polissa.butlletins and clean_text(polissa.butlletins[-1].name)}</span>
                        </td>
                    </tr>
                    %for comptador in polissa.comptadors:
                        %if comptador.active:
                            <tr>
                                <td>
                                    <span class="label">${_("Características:")}</span>
                                    <span class="field">${comptador.product_id and clean_text(comptador.product_id.name) or ''}</span>
                                </td>
                                <td>
                                    <span class="label">${_("Número de serie:")}</span>
                                    <span class="field">${comptador.name}</span>
                                </td>
                                <td>
                                    <span class="label">${_("Propiedad:")}</span>
                                    %if comptador.propietat == 'client':
                                        <span class="field">Cliente</span>
                                    %else:
                                        <span class="field">Empresa</span>
                                    %endif
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="label">${_("LECTURAS Y FACTURACIÓN")}</span>
                                    <span class="field"> </span>
                                </td>
                                <td>
                                    <span class="label">${_("MENSUAL")} </span>
                                    %if polissa.facturacio == 1:
                                        <span class="check">&#9746;</span>
                                    %else:
                                        <span class="check">&#x2610;</span>
                                    %endif
                                </td>
                                <td>
                                    <span class="label">${_("BIMESTRAL")} </span>
                                    %if polissa.facturacio == 2:
                                        <span class="check">&#9746;</span>
                                    %else:
                                        <span class="check">&#x2610;</span>
                                    %endif
                                </td>
                            </tr>
                        %endif
                    %endfor
        		</table>
        	</div>
        	<div class="seccio">
        		<div class="header">
                    ${_("FORMA DE PAGO DE LA COMERCIALIZADORA:")}
                </div>

        		<table class="open-section">
                    <tr>
        				<td>
        					<span class="label">${_("Nombre/Razón Social:")}</span>
        					<span class="field">${polissa.distribuidora.name}</span>
        				</td>
        				<td class="col_nif">
        					<span class="label">${_("NIF/CIF:")}</span>
        					<span class="field">${polissa.distribuidora.vat and polissa.distribuidora.vat.replace('ES', '') or ''}</span>
        				</td>
        			</tr>
                </table>
                <table class="joined">
                    %if polissa.tipo_pago.code == 'TRANSFERENCIA_CSB':
                        <%
                            bank = [b for b in polissa.distribuidora.bank_ids if b.default_bank]
                            iban = bank[0].iban[4:]
                        %>
                    %else:
                        <% iban = polissa.bank.iban[4:] %>
                    %endif

                    <tr>
                        <td>
                            <span class="label">${_("Entidad Financiera:")}</span>
                            <span class="field">${iban[0:4]}</span>
                        </td>
                        <td>
                            <span class="label">${_("Sucursal:")}</span>
                            <span class="field">${iban[4:8]}</span>
                        </td>
                        <td>
                            <span class="label">${_("DC:")}</span>
                            <span class="field">${iban[8:10]}</span>
                        </td>
                        <td>
                            <span class="label">${_("Nº Cuenta:")}</span>
                            <span class="field">${iban[10:]}</span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <span class="label">${_("Forma de pago:")}</span>
                            %if polissa.tipo_pago.code == 'TRANSFERENCIA_CSB':
                                <span class="field">${_("TRANSFERENCIA BANCARIA A LA DISTRIBUIDORA")}</span>
                            %elif polissa.tipo_pago.code == 'RECIBO_CSB':
                                <span class="field">${_("RECIBO DOMICILIADO A LA COMERCIALIZADORA")}</span>
                            %else:
                                <span class="field">${polissa.tipo_pago.name.upper()}</span>
                            %endif

                        </td>
                    </tr>
        		</table>
        	</div>
        	<br>
        	<div id="legal">
        		<br>
                <% data_contracte = datetime_to_date(polissa.data_firma_contracte) or polissa.data_alta or datetime.now().strftime('%Y-%m-%d') %>
        		<center>
        			BARBASTRO, ${pretty_date(data_contracte)}
        		</center>
        	</div>
        	<br>
        	<div style="clear:both"></div>
        	<div class="signatura">
        		<div style="position:absolute; top: 0px; min-width:100%;">${polissa.pagador.name}</div>
                <div style="position:absolute; bottom: 0px; min-width:100%;text-align:left;">EJEMPLAR PARA LA DISTRIBUIDORA</div>
        	</div>
        	<div class="signatura">
                <div style="position:absolute; top: 0px; min-width:100%;">${_("ELECTRICA DE BARBASTRO, S.A.")}</div>
                <img style="" src="${addons_path}/giscedata_polissa_distri_ab/report/firmaDireccion.png"/>
        		<div style="position:absolute; bottom: 0px; min-width:100%;">Fdo.- Pablo Bravo Vidal</div>
        	</div>
        <%
        comptador_polisses += 1;
        %>
        % if comptador_polisses<len(objects):
            <p style="page-break-after:always"></p>
        % endif
        %endfor
	</body>
</html>