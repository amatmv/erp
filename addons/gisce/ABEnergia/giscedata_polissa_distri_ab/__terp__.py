# -*- coding: utf-8 -*-
{
    "name": "Extension for polissa (Abenergia)",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Adds a report for ABEnergia contract
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "c2c_webkit_report",
        "giscedata_polissa",
        "giscedata_polissa_distri",
        "giscedata_polissa_condicions_generals"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_polissa_distri_ab_report.xml"
    ],
    "active": False,
    "installable": True
}
