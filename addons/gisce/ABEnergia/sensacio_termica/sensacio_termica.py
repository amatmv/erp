# -*- coding: utf-8 -*-

from osv import osv, fields
from mongodb_backend import osv_mongodb

class Temperatures(osv_mongodb.osv_mongodb):
    _name = 'temperatures'

    _columns = {
        'poblacion': fields.char(
            'Población', size=50, required=True, select=True, readonly=True
        ),
        'dia': fields.datetime('Fecha', readonly=True),
        'h1': fields.float('h1', size=12, readonly=True),
        'h2': fields.float('h2', size=12, readonly=True),
        'h3': fields.float('h3', size=12, readonly=True),
        'h4': fields.float('h4', size=12, readonly=True),
        'h5': fields.float('h5', size=12, readonly=True),
        'h6': fields.float('h6', size=12, readonly=True),
        'h7': fields.float('h7', size=12, readonly=True),
        'h8': fields.float('h8', size=12, readonly=True),
        'h9': fields.float('h9', size=12, readonly=True),
        'h10': fields.float('h10', size=12, readonly=True),
        'h11': fields.float('h11', size=12, readonly=True),
        'h12': fields.float('h12', size=12, readonly=True),
        'h13': fields.float('h13', size=12, readonly=True),
        'h14': fields.float('h14', size=12, readonly=True),
        'h15': fields.float('h15', size=12, readonly=True),
        'h16': fields.float('h16', size=12, readonly=True),
        'h17': fields.float('h17', size=12, readonly=True),
        'h18': fields.float('h18', size=12, readonly=True),
        'h19': fields.float('h19', size=12, readonly=True),
        'h20': fields.float('h20', size=12, readonly=True),
        'h21': fields.float('h21', size=12, readonly=True),
        'h22': fields.float('h22', size=12, readonly=True),
        'h23': fields.float('h23', size=12, readonly=True),
        'h24': fields.float('h24', size=12, readonly=True)
    }

Temperatures()
