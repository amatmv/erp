# -*- coding: utf-8 -*-
from osv import osv, fields
import StringIO
import csv
import base64
from datetime import datetime
from operator import itemgetter

class WizardSensacioTermicaExportar(osv.osv_memory):
    _name = 'wizard.sensacio_termica.exportar'

    def _num_registres_a_exportar(self, cursor, uid, context=None):
        if not context:
            context = {}
        return len(context.get('active_ids', []))

    def exportar(self, cursor, uid, ids, context=None):
        '''genera un csv amb els records seleccionats i l'escriu al wizard'''

        if not context:
            context = {}

        wiz = self.browse(cursor, uid, ids[0])

        # obtenir llista d'ids dels records sleccionats
        ids_actius = context.get('active_ids')

        # model actiu
        temperatures_obj = self.pool.get('temperatures')

        # obtenir els documents seleccionats
        resultats = temperatures_obj.read(
                                    cursor, uid, ids_actius, context=context)

        # ordenem els resultats per població i depsrés data ascendent
        resultats = sorted(resultats,
                           key=itemgetter('poblacion', 'dia'),
                           )

        # indiquem l'ordre dels headers
        headers = ['poblacion','dia',
                   'h1','h2','h3','h4','h5','h6','h7','h8','h9',
                   'h10','h11','h12','h13','h14','h15','h16','h17','h18',
                   'h19','h20','h21','h22','h23','h24'
                   ]

        # inicialitzem el csv a retornar
        linies = []
        linies.append(headers)

        # agafem de cada document les dades en l'ordre indicat
        for i in range(0,len(resultats)):
            linia = []
            for header in headers:
                valor = resultats[i].get(header,'no-data')
                linia.append(valor)
            linies.append(linia)

        # generem el fitxer i l'escribim al wizard
        output = StringIO.StringIO()
        writer = csv.writer(output, delimiter=';', quoting=csv.QUOTE_NONE)
        for line in linies:
            writer.writerow(line)
        file = base64.b64encode(output.getvalue())
        output.close()
        wiz.write({'file': file,
                   'name': 'sens_termica_{0}.csv'.
                            format(datetime.now().strftime('%Y%m%d-%H%M%S'))
                   })

    _columns = {
        'num_registres': fields.integer('Numero de registros a exportar',
                                        readonly=True),
        'name': fields.char('Nombre', size=60, readonly=True),
        'file': fields.binary('Fichero'),
    }
    _defaults = {
        'num_registres': _num_registres_a_exportar,
    }

WizardSensacioTermicaExportar()
