# -*- coding: utf-8 -*-
{
    "name": "Sensación térmica",
    "description": """
        Mòdul per a la consulta de l'històric de previsions
        de sensació tèrmica.
        """,
    "version": "0-dev",
    "author": "Abenergia",
    "category": "",
    "depends":[
        "base",
        "mongodb_backend",
        "oorq"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "sensacio_termica_view.xml",
        "wizard/wizard_sensacio_termica_exportar.xml"
    ],
    "active": False,
    "installable": True
}
