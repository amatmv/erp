# -*- coding: utf-8 -*-
{
    "name": "Extension for polissa (ABEnergia)",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Adds a report for ABEnergia contract
  * Adds wizard to retrieve certain polissa fields
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "c2c_webkit_report",
        "giscedata_polissa",
        "giscedata_polissa_comer",
        "giscedata_polissa_condicions_generals",
        "report_joiner",
        "giscedata_polissa_custom_ab"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_polissa_comer_abenergia_report.xml"
        ,"wizard/wizard_copiar_datos_cartera_view.xml"
        ,"security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
