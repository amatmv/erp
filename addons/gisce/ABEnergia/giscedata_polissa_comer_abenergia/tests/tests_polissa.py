# -*- coding: utf-8 -*-

from destral import testing
from destral.transaction import Transaction
from giscedata_polissa.tests import utils
from osv import osv


class TestsUndoLastModcontractual(testing.OOTestCase):

    def test_able_to_undo_last_modcontractual(self):
        """
        Checks if undo last modcontractual can be performed by the user.
        :return:
        """
        with Transaction().start(self.database) as txn:
            pool = self.openerp.pool
            cursor = txn.cursor
            uid = txn.user

            polissa_obj = pool.get('giscedata.polissa')
            cnae_obj = pool.get('giscemisc.cnae')
            imd_obj = pool.get('ir.model.data')

            # Importing contract 1
            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]
            # # to avoid the raise to the expected anual consumption
            vals = {'consumo_anual_previsto': 1}
            polissa_obj.write(cursor, uid, polissa_id, vals)
            # Policy activation
            utils.activar_polissa(pool, cursor, uid, polissa_id)

            # Do a modcontractual changing a the cnae
            cnae_9820_id = cnae_obj.search(cursor, uid, [('name', '=', '9820')])
            vals = {'cnae': cnae_9820_id[0]}
            utils.crear_modcon(
                pool, cursor, uid, polissa_id, vals, '2016-03-02', '2016-05-02'
            )

            # lets try to undo the previous mocontractual
            self.assertTrue(
                polissa_obj.undo_last_modcontractual(cursor, uid, polissa_id)
            )

    def test_unable_to_undo_last_modcontractual(self):
        """
        Checks if undo last modcontractual can't be performed by the user
        :return:
        """
        with Transaction().start(self.database) as txn:
            pool = self.openerp.pool
            cursor = txn.cursor
            uid = txn.user

            polissa_obj = pool.get('giscedata.polissa')
            cnae_obj = pool.get('giscemisc.cnae')
            imd_obj = pool.get('ir.model.data')
            user_obj = pool.get('res.users')
            resgroups_obj = pool.get('res.groups')

            group_id = resgroups_obj.search(
                cursor, uid, [('name', '=', 'Product / Manager')]
            )[0]

            # Importing contract 1
            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]
            # # to avoid the raise to the expected anual consumption
            vals = {'consumo_anual_previsto': 1}
            polissa_obj.write(cursor, uid, polissa_id, vals)
            # Policy activation
            utils.activar_polissa(pool, cursor, uid, polissa_id)

            # Do a modcontractual changing a the cnae
            cnae_9820_id = cnae_obj.search(cursor, uid, [('name', '=', '9820')])
            vals = {'cnae': cnae_9820_id[0]}
            utils.crear_modcon(
                pool, cursor, uid, polissa_id, vals, '2016-03-02', '2016-05-02'
            )

            # lets try to undo the previous modcontractual with an user that is
            # not in Product / Manager
            user_id = user_obj.search(
                cursor, uid, [('groups_id', 'not in', [group_id])]
            )[0]

            # But before do that, the user must be in a group that allows read
            # the invoice model
            invoice_group_id = resgroups_obj.search(
                cursor, uid, [('name', '=', 'Account Invoice / User')]
            )[0]
            vals = {'groups_id': [(4, invoice_group_id)]}
            user_obj.write(cursor, uid, user_id, vals)

            def undo_modcontractual():
                return polissa_obj.undo_last_modcontractual(
                    cursor, user_id, polissa_id
                )

            expected_message = ".*No tiene permisos para realizar esta acción..*"
            self.assertRaisesRegexp(
                osv.except_osv, expected_message, undo_modcontractual
            )
