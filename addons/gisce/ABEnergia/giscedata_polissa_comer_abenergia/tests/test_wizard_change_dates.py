# -*- coding: utf-8 -*-

from destral import testing
from destral.transaction import Transaction
from giscedata_polissa.tests import utils
from osv import osv


class TestChangeDates(testing.OOTestCase):

    def test_able_to_change_dates(self):
        """
        Tests if change dates can be performed by the user
        :return:
        """
        with Transaction().start(self.database) as txn:
            pool = self.openerp.pool
            cursor = txn.cursor
            uid = txn.user

            polissa_obj = pool.get('giscedata.polissa')
            imd_obj = pool.get('ir.model.data')
            wz_canviar_dates = pool.get('wizard.canviar.dates')

            # Importing contract 1
            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]
            # # to avoid the raise to the expected anual consumption
            vals = {'consumo_anual_previsto': 1}
            polissa_obj.write(cursor, uid, polissa_id, vals)

            # policy activation
            utils.activar_polissa(pool, cursor, uid, polissa_id)

            # let's try invoke the wizard
            modcon_id = polissa_obj.read(
                cursor, uid, polissa_id, ['modcontractuals_ids']
            )['modcontractuals_ids'][0]
            ctx = {'active_id': modcon_id}

            wz_id = wz_canviar_dates.create(cursor, uid, {}, ctx)
            self.assertIsInstance(wz_id, int)

    def test_unable_to_change_dates(self):
        """
        Tests if change dates can't be performed by the user
        :return:
        """
        with Transaction().start(self.database) as txn:
            pool = self.openerp.pool
            cursor = txn.cursor
            uid = txn.user

            polissa_obj = pool.get('giscedata.polissa')
            imd_obj = pool.get('ir.model.data')
            wz_canviar_dates = pool.get('wizard.canviar.dates')
            resgroups_obj = pool.get('res.groups')
            user_obj = pool.get('res.users')

            # Importing contract 1
            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]
            # # to avoid the raise to the expected anual consumption
            vals = {'consumo_anual_previsto': 1}
            polissa_obj.write(cursor, uid, polissa_id, vals)

            # policy activation
            utils.activar_polissa(pool, cursor, uid, polissa_id)

            # let's try invoke the wizard with an user that doesn't have the
            # privileges
            group_id = resgroups_obj.search(
                cursor, uid, [('name', '=', 'Product / Manager')]
            )[0]
            user_id = user_obj.search(
                cursor, uid, [('groups_id', 'not in', [group_id])]
            )[0]

            modcon_id = polissa_obj.read(
                cursor, uid, polissa_id, ['modcontractuals_ids']
            )['modcontractuals_ids'][0]
            ctx = {'active_id': modcon_id}

            def invoke_wizard():
                return wz_canviar_dates.create(cursor, user_id, {}, ctx)

            xpctd_mssg = ".*No tiene permisos para realizar esta acción..*"
            self.assertRaisesRegexp(osv.except_osv, xpctd_mssg, invoke_wizard)
