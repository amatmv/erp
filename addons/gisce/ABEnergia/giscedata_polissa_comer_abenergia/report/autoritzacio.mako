<%
    from datetime import datetime
    from numbertoletters import number_to_letters

    def localize_period(period, locale, mode):
        if period != 'False':
            import babel
            from datetime import datetime
            dtf = datetime.strptime(period, '%Y-%m-%d')
            dtf = dtf.strftime("%y%m%d")
            dt = datetime.strptime(dtf, '%y%m%d')
            if mode == 'dia':
                return babel.dates.format_datetime(dt, 'EEEE', locale=locale)
            elif mode == 'mes':
                return babel.dates.format_datetime(dt, 'LLLL', locale=locale)
            else:
                return babel.dates.format_datetime(dt, 'd LLLL Y', locale=locale)
        else:
            return ''
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
    	<style type="text/css">
        ${css}
            body{
                font-family: helvetica;
                font-size: 10px;
                margin-right: 50px;
                margin-left: 50px;
                background-image: url(${addons_path}/giscedata_polissa_comer_abenergia/report/FACTURA_ABE.png);
                background-size: 100%;
                background-repeat:no-repeat;
                margin:80px;
            }
            table{
                font-size: 10px;
                font-weight: bold;
                width: 100%;
                border-collapse: collapse;
            }
            td{
                padding-left: 10px;
                padding-right: 10px;
            }
            td.visible{
                font-style: italic;
            }
            .visible{
                position: relative;
                float: left;
                border: 1px solid black;
                padding-left: 5px;
                padding-right: 5px;
                margin-top: 3px;
                margin-bottom: 3px;
            }
            .invisible{
                position: relative;
                float: left;
                padding-left: 5px;
                padding-right: 5px;
                margin-top: 3px;
                margin-bottom: 3px;
            }
            .stack{
                margin-top: 10px;
                font-weight: normal;
            }
            .firma{
                position: relative;
                float: left;
                width: 46%;
                top: 80px;
                left: 15px;
                padding: 10px;
            }
            .caixa_firma{
                border: 1px solid black;
                padding-left: 5px;
                padding-right: 5px;
                margin-top: 3px;
                margin-bottom: 3px;
                width: 150px;
                height: 60px;
            }
            .text_cos{
                position: relative;
                padding-left: 10px;
                font-style: italic;
                font-weight: bold;
            }
            #contents{
                position: relative;
                top: 240px;
            }
            #capcalera{
                position: relative;
                width: 70%;
                font-size: 16px;
                font-weight: bold;
            }
            #data{
                position: relative;
                float: right;
            }
            #titular{
                position: relative;
                top: 45px;
            }
            #lopd{
                position: relative;
                top: 200px;
                border: 1px solid black;
                padding: 5px;
                text-align: justify;
                font-size: 6px;
            }
        </style>
    </head>
    <body>
        %for pol in objects:
            <div id="contents">
                <center>
                    <div id="capcalera">
                        AUTORIZACIÓN PARA LA CONTRATACIÓN
                        <br>EN REPRESENTACIÓN DE
                        <br>
                        <table style="margin-top: 10px;">
                            <tr>
                                <td class="visible" style="font-style: normal;"><center>${pol.titular.name}</center></td>
                            </tr>
                        </table>
                    </div>
                </center>
                <br><br>
                <div class="text_cos">
                    A rellenar por el/la persona autorizada
                </div>
                <br><br>
                <div style="clear:both"></div>
                <div id="data">
                    <div class="invisible">En ${localize_period(datetime.now().strftime("%Y-%m-%d"), 'es_ES', 'dia')} a ${datetime.now().strftime("%d")} de ${localize_period(datetime.now().strftime("%Y-%m-%d"), 'es_ES', 'mes')} de ${datetime.now().strftime("%Y")}</div>
                </div>
                <div style="clear:both"></div>
                <div id="titular">
                    <table class="stack">
                        <tr>
                            <td style="width: 25px;">
                                D./Dª
                            </td>
                            <td class="visible" style="width: 250px;"></td>
                            <td style="width: 40px;">
                                con NIF:
                            </td>
                            <td class="visible" style="width: 100px;"></td>
                            <td>
                                , actuando en nombre y
                            </td>
                        </tr>
                    </table>
                    <table class="stack">
                        <tr>
                            <td style="width: 80px;">
                                representación de
                            </td>
                            <td class="visible" style="width: 250px;">
                                ${pol.titular.name}
                            </td>
                            <td style="width: 60px;">
                                con NIF/CIF:
                            </td>
                            <td class="visible">
                                ${pol.titular.vat.replace('ES', '')}
                            </td>
                        </tr>
                    </table>
                    <table class="stack">
                        <tr>
                            <td style="width: 70px;">
                                en condición de
                            </td>
                            <td class="visible" style="width: 100px;"></td>
                            <td>
                                declaro que estoy autorizado/a por el arriba mencionado para gestionar la contratación
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="padding-top: 10px;">
                                y/o modificación de el/los suministro/s de electricidad con <b>ab</b> energía.
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="clear:both"></div>
                <center>
                    %for elem in [1, 2]:
                        <div class="firma">
                            <div class="caixa_firma"></div>
                            %if elem == 1:
                                Firma, nombre y apellidos del Autorizado
                            %else:
                                Firma, sello, nombre y apellidos del representado
                            %endif
                            <table class="stack">
                                <tr>
                                    <td style="width: 10px;">
                                        D/ª
                                    </td>
                                    <td class="visible" style="width: 200px;"></td>
                                    <td></td>
                                </tr>
                            </table>
                        </div>
                    %endfor
                </center>
                <div class="text_cos" style="top: 120px;">
                    Este documento se debe entregar firmado junto con la fotocopia del DNI del
                    autorizado y del representado y en su caso CIF de la sociedad representada.
                </div>
                <div id="lopd">
                    En virtud de lo establecido en la Ley 15/1999, y la LSSICE 34/2002, le informamos que sus datos forman parte de un fichero titularidad de
                    <b>Ab energía 1903, S.L.U.</b>. La información registrada se utilizará para informarle por cualquier medio electrónico o postal de
                    nuestras novedades comerciales. Puede ejercer los derechos de acceso, rectificación, cancelación y oposición en: <b>C/ ESCUELAS PÍAS, 12
                    - 22300 BARBASTRO (HUESCA) sac@abenergia.es</b>
                </div>
            </div>
        %endfor
    </body>
</html>
