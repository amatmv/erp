<%
    from datetime import datetime
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
    	<style type="text/css">
        ${css}
            body{
                font-family: helvetica;
                font-size: 11px;
                margin-right: 50px;
                margin-left: 50px;
                background-image: url(${addons_path}/giscedata_polissa_comer_abenergia/report/FACTURA_ABE.png);
                background-size: 100%;
                background-repeat:repeat-y;
                margin:80px;
            }
            table{
                font-size: 10px;
                width: 100%;
                border-collapse: collapse;
                margin-top: 5px;
            }
            td{
                padding-left: 10px;
                padding-right: 10px;
            }
            .field{
                border-bottom: 1px solid black;
                font-style: italic;
                padding-left: 5px;
                padding-right: 5px;
            }
            .visible{
                position: relative;
                float: left;
                border: 1px solid black;
                padding-left: 5px;
                padding-right: 5px;
                margin-top: 3px;
                margin-bottom: 3px;
                font-style: italic;
            }
            .invisible{
                position: relative;
                float: left;
                padding-left: 5px;
                padding-right: 5px;
                margin-top: 3px;
                margin-bottom: 3px;
            }
            .stack{
                margin-top: 8px;
            }
            .checkbox{
                width: 5px;
            }
            .motius_canvi{
                border-collapse: separate !important;
                border-spacing: 3px;
                margin-left: 35px;
                margin-top: 5px;
            }
            #titol{
                position: relative;
                top: 210px;
                font-weight: bold;
                font-size: 16px;
                text-align: center;
            }
            #contingut{
                position: relative;
                top: 220px;
            }
            #peu{
                position: relative;
                top: 240px;
                text-align: justify;
            }
            #lopd{
                position: relative;
                border: 1px solid black;
                padding: 5px;
                text-align: justify;
                top: 320px;
                font-size: 6px;
            }
            #dummy{
                height: 713px;
            }
            #canvi_titular{
                margin-top: 10px;
            }
            #modificacio_tarifa{
                margin-top: 15px;
            }
        </style>
    </head>
    <body>
        %for pol in objects:
            <div id="titol">
                SOLICITUD DE MODIFICACIÓN EN LA CONTRATACIÓN DE UN SUMINISTRO
            </div>
            <div id="contingut">
                <table>
                    <tr>
                        <td style="width: 25px;">Don</td>
                        <td class="field" style="width: 355px;">&nbsp;</td>
                        <td>provisto del DNI</td>
                        <td class="field" style="width: 100px;">&nbsp;</td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td style="width: 150px;">en representación de la empresa</td>
                        <td class="field" style="width: 300px;"></td>
                        <td style="width: 35px;">con CIF</td>
                        <td class="field"></td>
                    </tr>
                </table>
                <table>
                    <td style="width: 515px;">solicita a Ab energía 1903, S.L.U. una modificación en la contratación del suministro, con número de póliza</td>
                    <td class="field">${pol.name}</td>
                </table>
                <table>
                    <td style="width: 35px;">sito en</td>
                    <td class="field">${pol.cups_direccio}</td>
                </table>
                <table>
                    <td style="width: 175px;">en zona de distribución de la empresa</td>
                    <td class="field">${pol.distribuidora.name}</td>
                </table>
                <br>
                Señale la opción que corresponda:
                <table id="modificacio_tarifa">
                    <tr>
                        <td class="visible" style="width: 5 px;"></td>
                        <td>Modificación en la tarifa / potencia a contratar</td>
                    </tr>
                </table>
                <table class="stack">
                    <tr>
                        <td style="width: 10px;"></td>
                        <td style="width: 55px;">
                            Tarifa actual
                        </td>
                        <td class="visible" style="width: 92px;">
                            ${pol.tarifa.name}
                        </td>
                        <td style="width: 60px;">
                            Nueva Tarifa
                        </td>
                        <td class="visible" style="width: 92px;"></td>
                        <td></td>
                    </tr>
                </table>
                %for elem in range(1, 4):
                    <table class="stack">
                        <tr>
                            <td style="width: 10px;"></td>
                            <td style="width: 97px;">
                                Potencia actual en P${elem}
                            </td>
                            <td class="visible" style="width: 50px;">
                                %if len(pol.potencies_periode) >= elem:
                                    ${pol.potencies_periode[elem-1].potencia}
                                %endif
                            </td>
                            <td style="width: 102px;">
                                Nueva Potencia en P${elem}
                            </td>
                            <td class="visible" style="width: 50px;"></td>
                            <td></td>
                        </tr>
                    </table>
                %endfor
                <table class="stack">
                    <tr>
                        <td style="width: 10px;"></td>
                        <td style="width: 45px;">
                            Maxímetro
                        </td>
                        <td class="visible" style="width: 20px;"></td>
                        <td></td>
                    </tr>
                </table>
                <table class="stack">
                    <tr>
                        <td class="visible" style="width: 5 px;"></td>
                        <td>Modificación en el titular del suministro</td>
                    </tr>
                </table>
                <table id="canvi_titular">
                    <tr>
                        <td style="width: 95px;">
                            Motivo de cambio del titular
                        </td>
                    </tr>
                </table>
                <table class="motius_canvi">
                    <tr>
                        <td class="visible checkbox">
                            &nbsp;
                        </td>
                        <td>
                            Traspaso: solicita antiguo titular y el nuevo no se hace cargo de lo anterior
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="visible checkbox">
                            &nbsp;
                        </td>
                        <td>
                            Justo titulo: solicita nuevo titular y no se hace cargo de lo anterior
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="visible checkbox">
                            &nbsp;
                        </td>
                        <td>
                            Subrogación: no hay cambio de actividad y el nuevo se hace cargo de todo lo anterior
                        </td>
                        <td></td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td style="width: 125px;">
                            Antiguo titular del suministro
                        </td>
                        <td class="field" style="width: 300px;">
                            ${pol.titular.name}
                        </td>
                        <td></td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td style="width: 30px;">
                            NIF/CIF
                        </td>
                        <td class="field" style="width: 150px;">
                            ${pol.titular.vat.replace('ES', '')}
                        </td>
                        <td></td>
                    </tr>
                </table>
                <table>
                    <td style="width: 125px;">
                        Nuevo titular del suministro
                    </td>
                    <td class="field" style="width: 300px;"></td>
                    <td></td>
                </table>
                <table>
                    <tr>
                        <td style="width: 30px;">
                            NIF/CIF
                        </td>
                        <td class="field" style="width: 100px;"></td>
                        <td style="width: 95px;">
                            Teléfono de contacto
                        </td>
                        <td class="field" style="width: 70px;"></td>
                        <td style="width: 85px;">
                            Correo electrónico
                        </td>
                        <td class="field"></td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td style="width: 100px;">
                            Domiciliación bancaria
                        </td>
                        <td class="field" style="width: 350px;"></td>
                        <td></td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td style="width: 135px;">
                            Dirección de envío de facturas
                        </td>
                        <td class="field" style="width: 350px;">
                            ${pol.cups_direccio}
                        </td>
                        <td></td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td style="width: 75px;">
                            Nueva dirección
                        </td>
                        <td class="field" style="width: 350px;"></td>
                        <td></td>
                    </tr>
                </table>
                <table style="margin-top: 20px;">
                    <tr>
                        <td style="width: 145px;">
                            Fecha de solicitud: ${datetime.now().strftime("%d-%m-%Y")}
                        </td>
                        <td style="width: 300px;"></td>
                        <td>
                            Firma nuevo titular
                        </td>
                    </tr>
                </table>
            </div>
            <div id="lopd">
                En virtud de lo establecido en la Ley 15/1999, y la LSSICE 34/2002, le informamos que sus datos forman parte de un fichero titularidad de
                <b>AB ENERGÍA 1903, S.L.U.</b>. La información registrada se utilizará para informarle por cualquier medio electrónico o postal de
                nuestras novedades comerciales. Puede ejercer los derechos de acceso, rectificación, cancelación y oposición en: <b>C/ ESCUELAS PÍAS, 12
                - 22300 BARBASTRO (HUESCA) sac@abenergia.es</b>
            </div>
            <div style="clear:both"></div>
            <p style="page-break-after:always"></p>
            <div id="peu">
                Documentación a presentar:
                <br><br>
                Para modificaciones de titularidad:
                <ol>
                    <li>Fotocopia del NIF</li>
                    <li>Certificado de la propiedad (contrato de arrendamiento, recibo de IBI, escritura de compraventa...)</li>
                    <li>Nº Referencia Catastral</li>
                    <li>En caso de que se trate una actividad comercial: Copia de la licencia de actividad</li>
                    <li>
                        Boletín de instalación eléctrica, en caso de que: No disponga la empresa distribuidora, que el boletín que disponga la distribuidora
                        tenga más de 20 años, que la nueva potencia contratada supere la máxima admisible, que se trate de un cambio del uso de la instalación
                    </li>
                </ol>
                <br>
                Para modificaciones de potencia:
                <ol>
                    <li>Boletín de instalación eléctrica, en caso de que: No disponga la empresa distribuidora, que el boletín que disponga la distribuidora
                    tenga más de 20 años, que la nueva potencia contratada supere la máxima admisible, que se trate de un cambio del uso de la instalación</li>
                </ol>
            </div>
            <div id="dummy"></div>
        %endfor
    </body>
</html>
