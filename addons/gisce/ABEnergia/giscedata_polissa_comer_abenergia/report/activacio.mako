<%
    from datetime import datetime
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
    	<style type="text/css">
        ${css}
            body{
                font-family: helvetica;
                font-size: 15px;
                margin-right: 50px;
                margin-left: 50px;
                background-image: url(${addons_path}/giscedata_polissa_comer_abenergia/report/FACTURA_ABE.png);
                background-size: 100%;
                background-repeat:no-repeat;
                margin:80px;
            }
            table{
                font-size: 10px;
                font-weight: bold;
                width: 100%;
                border-collapse: collapse;
                border: none;
            }
            p{
                text-align: justify;
            }
            #continguts{
                position: relative;
                top: 115px;
            }
            #capcalera{
                position: relative;
                float: right;
                width: 100%;
                text-align: right;
            }
            #carta{
                position: relative;
                top: 90px;
            }
        </style>
    </head>
    <body>
        %for pol in objects:
        <div id="continguts">
            <div id="capcalera">
                ${pol.pagador.name}<br>
                ${pol.direccio_pagament.street}<br>
                ${pol.direccio_pagament.zip} ${pol.direccio_pagament.city}<br>
                ${pol.cups.name}
            </div>
            <div style="clear:both"></div>
            <div id="carta">
                Estimado cliente,<br>
                <p>
                    Desde <b>ab</b> energía le damos la bienvenida y agradecemos la confianza depositada en nuestra empresa.
                </p>
                <p>
                    Somos una empresa de servicios con más de 100 años de antigüedad y experiencia en el suministro de luz y agua a miles de usuarios.
                </p>
                <p>
                    Le confirmamos que su empresa distribuidora nos ha comunicado con fecha la activación de cambio de comercializadora a <b>ab</b> energía el ${formatLang(pol.data_alta, date=True)},
                    por lo que a partir de dicha fecha le facturaremos la energía.
                </p>
                <p>
                    Para cualquier consulta no dude en contactar con nosotros por teléfono a través de la línea gratuita 900 1903 00.
                </p>
                <p>
                    Así mismo le recordamos que también estamos a su disposición en nuestras oficinas de la C/Escuelas Pías 12, de Barbastro.
                </p>
                <br>
                Atentamente,
                <br><br><br><br>
                <b>ab</b> energía
            </div>
        </div>
            %if loop.index + 1 < len(objects):
                <p style="page-break-after:always"></p>
            %endif
        %endfor
    </body>
</html>
