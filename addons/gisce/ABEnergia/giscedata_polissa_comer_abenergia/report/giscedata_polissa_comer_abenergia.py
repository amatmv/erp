# -*- coding: utf-8 -*-
import tempfile
import base64
import calendar
from datetime import date
from datetime import datetime

from c2c_webkit_report import webkit_report
from report_joiner import ReportJoiner

from report import report_sxw
from tools import config
import pooler
import netsvc

import pypdftk


def get_atr_potencia_price(cursor, uid, polissa, pname, tipus):
    pool = pooler.get_pool(cursor.dbname)
    pricelist_obj = pool.get('product.pricelist')
    pricelistver_obj = pool.get('product.pricelist.version')
    pricelistitem_obj = pool.get('product.pricelist.item')

    product_uom_obj = pool.get('product.uom')
    product_id = polissa['tarifa'].get_periodes_producte(tipus)[pname]
    ctx = {
        'date': date.today()
    }
    if tipus == 'tp':
        uom_id = product_uom_obj.search(cursor, uid, [('factor', '=', 365), ('name', '=', 'kW/dia')])[0]
        if calendar.isleap(datetime.today().year):
            imd_obj = pool.get('ir.model.data')
            uom_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'uom_pot_elec_dia_traspas'
            )[1]
            ctx['uom'] = uom_id
        ctx.update({
            'uom': uom_id
        })
    pricelist = polissa['llista_preu']
    discount = 0
    if pricelist.visible_discount:
        date_price = date.today().strftime('%Y-%m-%d')
        version = pricelistver_obj.search(cursor, uid,
            [
                ('pricelist_id', '=', pricelist.id),
                '|',
                ('date_start', '=', False),
                ('date_start', '<=', date_price),
                '|',
                ('date_end', '=', False),
                ('date_end', '>=', date_price)

            ], order='id', limit=1)
        if version:
            items = pricelistitem_obj.search(
                    cursor, uid, [
                    ('price_version_id', '=', version[0]),
                    ('product_id', '=', product_id)
                ]
            )
            if items:
                params_to_read = ['price_discount']
                for item in pricelistitem_obj.read(
                        cursor, uid, items, params_to_read):
                    if item['price_discount']:
                        discount = item['price_discount']
                        break

    price_atr = pricelist_obj.price_get(cursor, uid, [pricelist.id],
                                        product_id, 1,
                                        1,
                                        ctx)[pricelist.id]
    if discount:
        price_atr = price_atr / (1.0 + discount)
        discount = abs(discount)
    return price_atr, discount


def get_tipus_persona(tipus):
    if tipus == 'CI':
        return "juridica"
    elif tipus == 'NI':
        return "fisica"
    return ""


def localize_period_month(period, locale):
    if period != 'False':
        import babel
        from datetime import datetime
        dtf = datetime.strptime(period, '%Y-%m-%d')
        dtf = dtf.strftime("%y%m%d")
        dt = datetime.strptime(dtf, '%y%m%d')
        return babel.dates.format_datetime(dt, 'LLLL', locale=locale)
    else:
        return ''


def get_mes(month):
    months = {
        '1': 'ENERO', '2': 'FEBRERO', '3': 'MARZO',
        '4': 'ABRIL', '5': 'MAYO', '6': 'JUNIO',
        '7': 'JULIO', '8': 'AGOSTO', '9': 'SEPTIEMBRE',
        '10': 'OCTUBRE', '11': 'NOVIEMBRE', '12': 'DICIEMBRE',
    }
    return months[str(month)]


def pretty_date(data, type='normal'):
    any, mes, dia = data.split('-')
    if type == 'normal':
        return '%s/%s/%s' %(dia, mes, any)
    elif type == 'text':
        return '%s de %s de %s' %(dia, get_mes(int(mes)).lower(), any)


class report_webkit_html(report_sxw.rml_parse):
    def __init__(self, cursor, uid, name, context):
        super(report_webkit_html, self).__init__(cursor, uid, name,
                                                 context=context)
        self.localcontext.update({
            'cursor': cursor,
            'uid': uid,
            'addons_path': config['addons_path'],
            'pretty_date': pretty_date,
            'get_mes': get_mes,
            'get_atr_potencia_price': get_atr_potencia_price,
            'get_tipus_persona': get_tipus_persona,
            'localize_period_month': localize_period_month
        })

    def set_context(self, objects, data, ids, report_type=None):
        polissa_date = data.get('form', {}).get('polissa_date')
        if polissa_date:
            ctx = {'date': polissa_date}
            polissa_obj = self.pool.get('giscedata.polissa')
            objects = polissa_obj.browse(self.cr, self.uid, ids, context=ctx)
        super(report_webkit_html, self).set_context(
            objects, data, ids, report_type
        )


class ReportContracteCondicionsGenerals(webkit_report.WebKitParser):

    def add_condicions_generals(self, polissa):
        res = False
        attachment = polissa.condicions_generals_id.attachment_id
        if attachment and attachment.datas:
            res_path = tempfile.mkstemp('-join.pdf', 'report-')[1]
            with open(res_path, 'w') as f:
                f.write(base64.b64decode(attachment.datas))
                res = res_path
        return res

    def create(self, cursor, uid, ids, datas, context=None):
        """Create reports and join.

        :param cursor: Database cursor
        :param uid: User identifier
        :param ids: records to print into the report
        :param data: Data passed to report
        :param context: Application context
        :returns a tuple with the type and the content of the report
        """
        if context is None:
            context = {}
        insert_annexe_preus = context.get('insert_annexe_preus', False)
        insert_sepa_atr = context.get('insert_sepa_atr', True)
        to_join_final = []
        pool = pooler.get_pool(cursor.dbname)
        polissa_model = 'giscedata.polissa'
        if datas['model'] == 'crm.polissa':
            polissa_model = 'crm.polissa'
        polissa_obj = pool.get(polissa_model)
        polissa_date = datas.get('form', {}).get('polissa_date')
        before_sepa = False
        ctx = context.copy()
        if polissa_date:
            ctx['date'] = polissa_date
        for polissa in polissa_obj.browse(cursor, uid, ids, context=ctx):
            to_join = []
            res = super(ReportContracteCondicionsGenerals, self).create(
                cursor, uid, [polissa.id], datas, context
            )
            if res[1] != 'pdf':
                continue
            res_path = tempfile.mkstemp('-join.pdf', 'report-')[1]
            with open(res_path, 'w') as f:
                f.write(res[0])
            to_join.append(res_path)
            reports = []
            if insert_annexe_preus:
                reports.append('report.giscedata.polissa.comer.abenergia.preus')
            if insert_sepa_atr:
                reports.append('report.giscedata_polissa_atr_sepa')
                before_sepa = True
            for report in reports:
                if "sepa" in report and polissa.condicions_generals_id:
                    to_join.append(self.add_condicions_generals(polissa))
                ann = netsvc.SERVICES[report]
                res = ann.create(cursor, uid, [polissa.id], datas, context)
                if res[1] != 'pdf':
                    continue
                res_path = tempfile.mkstemp('-join.pdf', 'report-')[1]
                with open(res_path, 'w') as f:
                    f.write(res[0])
                to_join.append(res_path)
            if not before_sepa and polissa.condicions_generals_id:
                to_join.append(self.add_condicions_generals(polissa))
            if len(to_join) > 1:
                out = pypdftk.concat(files=to_join)
            else:
                out = res_path
            to_join_final.append(out)
        if len(to_join_final) > 1:
            out = pypdftk.concat(files=to_join_final)
        else:
            out = to_join_final[0]
        with open(out, 'r') as f:
            content = f.read()
        return content, 'pdf'


ReportContracteCondicionsGenerals(
    'report.giscedata.polissa',
    'giscedata.polissa',
    'giscedata_polissa_comer_abenergia/report/contracte.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.polissa.comer.abenergia.preus',
    'giscedata.polissa',
    'giscedata_polissa_comer_abenergia/report/preus.mako',
    parser=report_webkit_html
)


webkit_report.WebKitParser(
    'report.giscedata.polissa.baixa',
    'giscedata.polissa',
    'giscedata_polissa_comer_abenergia/report/baixa.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.polissa.activacio',
    'giscedata.polissa',
    'giscedata_polissa_comer_abenergia/report/activacio.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.polissa.reclamacio',
    'giscedata.polissa',
    'giscedata_polissa_comer_abenergia/report/reclamacio.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.polissa.autoritzacio',
    'giscedata.polissa',
    'giscedata_polissa_comer_abenergia/report/autoritzacio.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.polissa.modificacio',
    'giscedata.polissa',
    'giscedata_polissa_comer_abenergia/report/modificacio.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata_polissa_atr_sepa',
    'giscedata.polissa',
    'giscedata_polissa_comer_abenergia/report/atr_sepa.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.polissa.renuncia.bono.social',
    'giscedata.polissa',
    'giscedata_polissa_comer_abenergia/report/renuncia_bono_social.mako',
    parser=report_webkit_html
)
