<%
    from datetime import datetime
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
    	<style type="text/css">
        ${css}
            body{
                font-family: helvetica;
                font-size: 12px;
                margin-right: 50px;
                margin-left: 50px;
                background-image: url(${addons_path}/giscedata_polissa_comer_abenergia/report/FACTURA_ABE.png);
                background-size: 100%;
                background-repeat:no-repeat;
                margin:80px;
            }
            table{
                font-size: 10px;
                font-weight: bold;
                width: 100%;
                border-collapse: collapse;
            }
            .field{
                border-bottom: 1px dotted black;
                font-weight: normal;
                font-style: italic;
                padding-left: 5px;
            }
            .title{
                font-weight: bold;
            }
            .manual{
                border: 1px solid black;
                padding: 5px;
                font-weight: bold;
                height: 150px;
            }
            #contents{
                position: relative;
                top: 215px;
            }
            #titular{
                border: 1px solid black;
                padding: 5px;
                padding-top: 15px;
                padding-bottom: 15px;
            }
            #documents{
                border: 1px solid black;
                padding: 5px;
                font-weight: bold;
                height: 90px;
            }
            #footer{
                padding-left: 10px;
                position: relative;
                top: 5px;
            }
            #lopd{
                position: relative;
                top: 65px;
                border: 1px solid black;
                padding: 5px;
                text-align: justify;
                font-size: 6px;
            }
        </style>
    </head>
    <body>
        %for pol in objects:
            <div id="contents">
                <div id="titular">
                    <table>
                        <tr>
                            <td class="title" style="width: 90px;">Nombre del titular:</td>
                            <td class="field">${pol.titular.name}</td>
                            <td class="title" style="width: 40px;">DNI/CIF:</td>
                            <td class="field" style="width: 140px;">${pol.titular.vat.replace('ES', '')}</td>
                        </tr>
                    </table>
                    <table>
                        <tr>
                            <td class="title" style="width: 50px;">Nº Póliza:</td>
                            <td class="field">${pol.name}</td>
                        </tr>
                    </table>
                    <table>
                        <tr>
                            <td class="title" style="width: 120px;">Dirección de suministro:</td>
                            <td class="field">${pol.cups_direccio}</td>
                        </tr>
                    </table>
                    <table>
                        <tr>
                            <td class="title" style="width: 35px;">CUPS:</td>
                            <td class="field">${pol.cups.name}</td>
                        </tr>
                    </table>
                    <table>
                        <tr>
                            <td class="title" style="width: 85px;">Teléfono y email:</td>
                            <td class="field">${pol.direccio_pagament.phone or pol.direccio_pagament.mobile or ''} - ${pol.direccio_pagament.email or ''}</td>
                        </tr>
                    </table>
                </div>
                <br>
                <div class="manual">
                    EXPONE:
                </div>
                <br>
                <div class="manual">
                    SOLICITA:
                </div>
                <br>
                <div id="documents">
                    DOCUMENTOS QUE SE ADJUNTAN:
                </div>
                <div id="footer">
                    Fecha: ${datetime.today().strftime('%d-%m-%Y')}
                    <br><br><br><br>
                    Firma.
                </div>
                <div id="lopd">
                    En virtud de lo establecido en la Ley 15/1999, y la LSSICE 34/2002, le informamos que sus datos forman parte de un fichero titularidad de
                    <b>Ab energía 1903, S.L.U.</b>. La información registrada se utilizará para informarle por cualquier medio electrónico o postal de
                    nuestras novedades comerciales. Puede ejercer los derechos de acceso, rectificación, cancelación y oposición en: <b>C/ ESCUELAS PÍAS, 12
                    - 22300 BARBASTRO (HUESCA) sac@abenergia.es</b>
                </div>
            </div>
        %endfor
    </body>
</html>
