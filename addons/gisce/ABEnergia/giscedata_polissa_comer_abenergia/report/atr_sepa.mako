<%
    from datetime import datetime, date
    import calendar
    from dateutil.relativedelta import relativedelta
    from operator import attrgetter, itemgetter
    from addons.crm_elec.crm_polissa import get_consums_anuals_periode

    pool = objects[0].pool
    crm_polissa_obj = pool.get('crm.polissa')

    def get_fields(model, oferta):
        fields = {}
        fields['firmante'] = False
        fields['firmante_nif'] = False
        if model == 'crm.polissa':
            fields['cliente_tipo_persona'] = oferta.cliente_tipo_persona
            fields['cliente_nombre'] = oferta.cliente_nombre or ''
            fields['cliente_apellidos'] = oferta.cliente_apellidos or ''
            fields['cliente_vat'] = oferta.cliente_vat
            fields['persona_firmante'] = oferta.pers_firmante_id
            fields['cups_sips'] = oferta.cups_sips
            fields['cups_name'] = oferta.cups_name
            fields['pagador_autorizacion_sepa'] = oferta.pagador_autorizacion_sepa.name
            fields['direccion_cliente_sepa'] = oferta.direccion_clte_sepa.street
            fields['banco'] = oferta.entidad_bancaria
            fields['num_cuenta'] = oferta.num_cta.iban
            fields['pagador'] = oferta.partner_id.name
            fields['pagador_nif'] = oferta.partner_id.vat[2:]
            if oferta.pers_firmante_id and oferta.pers_firmante_nif:
                fields['firmante'] = oferta.pers_firmante_id.name
                fields['firmante_nif'] = oferta.pers_firmante_nif[2:]
        elif model == 'giscedata.polissa':
            fields['cliente_tipo_persona'] = get_tipus_persona(oferta.titular.cifnif)
            fields['cliente_nombre'] = oferta.titular.name
            fields['cliente_apellidos'] = ""
            fields['cliente_vat'] = oferta.titular.vat
            fields['persona_firmante'] = oferta.firmante_id
            fields['cups_sips'] = ""
            fields['cups_name'] = oferta.cups.name
            fields['pagador_autorizacion_sepa'] = oferta.pagador.name
            fields['direccion_cliente_sepa'] = oferta.direccio_pagament.street
            fields['banco'] = oferta.bank.bank.lname
            fields['num_cuenta'] = oferta.bank.iban
            fields['prop_cuenta_bancaria'] = oferta.bank.owner_id.name
            fields['prop_cuenta_bancaria_nif'] = oferta.bank.owner_id.vat[2:]
            fields['pagador'] = oferta.pagador.name
            fields['pagador_nif'] = oferta.pagador_nif[2:]
            if oferta.firmante_id and oferta.firmante_vat:
                fields['firmante'] = oferta.firmante_id.name
                fields['firmante_nif'] = oferta.firmante_vat[2:]
        return fields
%>

<%def name="firma_contratos(mode=None)">
    <%
        signatura = fields['prop_cuenta_bancaria']
        dni_signatura = fields['prop_cuenta_bancaria_nif']
        if mode == "contracte":
            if fields['firmante'] and fields['firmante_nif'] and ((fields['cliente_tipo_persona'] == 'juridica' and data['model'] == 'crm.polissa') or (data['model'] == 'giscedata.polissa')):
                signatura = fields['firmante'] or ''
                dni_signatura = fields['firmante_nif'] or ''
            else:
                if fields['cliente_tipo_persona'] == 'fisica':
                    signatura = fields['pagador'] or ''
                    dni_signatura = fields['pagador_nif'] or ''
        else:
            if fields['firmante'] and fields['firmante_nif']:
                signatura = fields['firmante']
                dni_signatura = fields['firmante_nif']
            elif (not signatura or not dni_signatura) and fields['cliente_tipo_persona'] == 'juridica':
                signatura = ''
                dni_signatura = ''

    %>
    <div class="firma">
        <div class="row">
            <div class="col-xs-5">
                <p class="head">Por el representante de la compañía</p>
                <ul class="condicions">
                    <li>
                        <div class="filera">
                            <div class="nom">
                                <span class="desc">Fdo:</span><br>
                            </div>
                              <span class="filled-text">
                                  ${signatura}
                              </span>
                        </div>
                    </li>
                    <li>
                        <div class="filera">
                            <div class="nom">
                                <span class="desc">DNI:</span><br>
                            </div>
                              <span class="filled-text">
                                  ${dni_signatura}
                              </span>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col-xs-3"></div>
            <div class="col-xs-4">
                <div class="cuadro_firma_titol"><span>Firma</span></div>
                <div class="cuadro_firma"> </div>
            </div>
        </div>
    </div>
</%def>
<html>
  <head>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type">
    <title></title>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="${addons_path}/crm_elec/report/oferta.css"/>
  </head>
  <body>
    %for oferta in objects:
        <%
            if data['model'] == 'crm.polissa':
                oferta = crm_polissa_obj.browse(cursor, uid, data['id'])
        %>
        %if data['model'] == 'crm.polissa' and not oferta.partner_id:
            El campo "Cliente existente" es obligatorio en el documento de autorización.
        %else:
            <%
                dades = {}
                if data['model'] == 'crm.polissa':
                    oferta = crm_polissa_obj.browse(cursor, uid, data['id'])
                    dades = oferta.get_info_oferta(oferta.id)
                else:
                    dades['debtor_name'] = oferta.pagador.name
                    dades['debtor_address'] = False
                    if oferta.bank.owner_address_id:
                        dades['debtor_address'] = oferta.bank.owner_address_id.street
                    dades['debtor_iban_print'] = ""
                fields = get_fields(data['model'], oferta)

                # dades persona firmant segons si és física o jurídica
                if fields['cliente_tipo_persona'] == 'fisica':
                    firmante = fields['cliente_nombre'] or ''
                    firmante += ' '
                    firmante += fields['cliente_apellidos'] or ''
                    niffirmante = fields['cliente_vat']
                else:
                    firmante = ''
                    niffirmante = ''
                    if fields['persona_firmante']:
                        firmante = fields['persona_firmante'].name.replace('\r\n','').replace('\n','') or ''
                        niffirmante = fields['persona_firmante'].vat
                if data['model'] == 'crm.polissa':
                    fields['prop_cuenta_bancaria'] = firmante
                    fields['prop_cuenta_bancaria_nif'] = niffirmante
            %>
            <div class="posterior atr_sepa_container">
                    <div class="col-xs-12 no-gutter no_padding anexo-1">
                        <div class="titol_datos_border_left border_azul">Anexo a las condiciones generales del contrato de electricidad</div>
                        <div class="white_border"></div>
                        <div class="cuadro_datos_azul">
                            <ul class="condicions superior">
                                <li>
                                    <div class="column width-60">
                                        <div class="filera">
                                            <div class="nom">
                                                <span class="desc">Nombre del representante legal</span><br>
                                            </div>
                                            %if fields['cliente_tipo_persona']  == 'juridica' and firmante:
                                               <span class="filled-text">
                                                 ${firmante}
                                               </span>
                                            %else:
                                               <div class="linia"></div>
                                            %endif
                                        </div>
                                    </div>
                                    <div class="column width-40">
                                        <div class="filera">
                                            <div class="nom">
                                                <span class="desc">DNI</span><br>
                                            </div>
                                            %if fields['cliente_tipo_persona']  == 'juridica' and niffirmante:
                                               <span class="filled-text">
                                                 ${niffirmante}
                                               </span>
                                            %else:
                                               <div class="linia"></div>
                                            %endif
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="column width-60">
                                        <div class="filera">
                                            <div class="nom">
                                                <span class="desc">Nombre de la empresa</span><br>
                                            </div>
                                              <span class="filled-text">
                                                <%
                                                nombre_empresa = fields['cliente_nombre'] or ''
                                                nombre_empresa += ' '
                                                nombre_empresa += fields['cliente_apellidos']
                                                %>
                                                ${nombre_empresa}
                                              </span>
                                        </div>
                                    </div>
                                    <div class="column width-40">
                                        <div class="filera">
                                            <div class="nom">
                                                <span class="desc">CIF</span><br>
                                            </div>
                                            %if fields['cliente_vat']:
                                               <span class="filled-text">
                                                 ${fields['cliente_vat']}
                                               </span>
                                            %else:
                                               <div class="linia"></div>
                                            %endif
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="column width-40">
                                        <div class="filera">
                                            <div class="nom">
                                                <span class="desc">CUPS</span><br>
                                            </div>
                                            <div>
                                              <span class="filled-text">
                                                ${fields['cups_sips'] or fields['cups_name'] or ''}
                                              </span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <div class="texto-autoriza">
                                <p class="head">Autoriza</p>
                                <p class="texto">
                                    A la compañía AB ENERGÍA 1903, S.L.U., con C.I.F. B-22350466 a formalizar por mi cuenta y en su nombre, en calidad de sustituto, el correspondiente contrato de acceso a las redes de distribución y a modificar, si lo estima oportuno, los parámetros eléctricos de contratación del punto de suministro; así como autorizar a AB ENERGÍA a solicitar toda aquella información histórica relativa al punto de suministro eléctrico (consumo, potencias contratadas y consumidas, etc.) que en calidad de consumidor deba, conforme a derecho, proporcionar la compañía distribuidora a sus abonados y a presentar ante la distribuidora y en mi nombre, cualesquiera reclamaciones consideraran oportunas. Estando sujeta la respuesta de la distribuidora, a los plazos de atención de reclamaciones establecidas en las legislación vigente, para reclamaciones presentadas por los titulares de los puntos de suministro.
                                    <br>Sírvase admitir este poder, para su bastanteo, otorgando a los efectos de los dispuesto en el Real Decreto 1955/2000, de 1 de diciembre, en el Real Decreto 1435/2002, de 27 de diciembre y demás disposiciones aplicables.
                                </p>
                            </div>
                            ${firma_contratos("contracte")}
                        </div>
                    </div>
                    <div class="col-xs-12 no-gutter no_padding anexo-2">
                        <div class="titol_datos_border_left border_verde titulo_anexo">Confirmación del procedimiento de pago del cliente<br>
                            <span>Reglamento 260/2012 del Parlamento Europeo y del Consejo del 14 de marzo 2012</span>
                        </div>
                        <div class="white_border"></div>
                        <div class="cuadro_datos_verde">
                            <ul class="condicions superior">
                                <li>
                                    <div class="filera">
                                        <div class="nom">
                                            <span class="desc">Nombre del cliente</span><br>
                                        </div>
                                        <div>
                                            <span class="text-over-line"> ${dades['debtor_name'] or fields['pagador_autorizacion_sepa'] or ''} </span>
                                            <div class="linia"></div>
                                        </div>
                                        <span class="eng">Debtor’s name</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="filera">
                                        <div class="nom">
                                            <span class="desc">Dirección del cliente</span><br>
                                        </div>
                                        <div>
                                            <span class="text-over-line"> ${dades['debtor_address'] or fields['direccion_cliente_sepa'] or ''} </span>
                                            <div class="linia"></div>
                                        </div>
                                        <span class="eng">Adress of the debtor</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="filera">
                                        <div class="nom">
                                            <span class="desc">Entidad bancaria</span><br>
                                        </div>
                                        <div>
                                            <span class="text-over-line"> ${fields['banco'] or ''} </span>
                                            <div class="linia"></div>
                                        </div>
                                        <span class="eng">Entity name</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="filera">
                                        <div class="nom">
                                            <span class="desc">Número de cuenta (IBAN)</span><br>
                                        </div>
                                        <div>
                                            <span class="text-over-line"> ${fields['num_cuenta'] or dades['debtor_iban_print']} </span>
                                            <div class="linia"></div>
                                        </div>
                                        <span class="eng">Account number - IBAN</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="filera">
                                        <div class="nom">
                                            <span class="desc">Fecha y localidad de la firma</span><br>
                                        </div>
                                        <div class="linia"></div>
                                        <span class="eng">Date and location in which you are signing</span>
                                    </div>
                                </li>
                            </ul>
                            <div class="texto-autoriza">
                                <p class="head">Autoriza</p>
                                <p class="texto">
                                    Mediante la firma de esta orden de domiciliación, el deudor autoriza (A) al acreedor a enviar instrucciones a la entidad del deudor para adeudar su cuenta y (B) a la entidad para efectuar los adeudos en su cuenta siguiendo las instrucciones del acreedor. Como parte de sus derechos, el deudor está legitimado al reembolso por su entidad en los términos y condiciones del contrato suscrito con la misma. La solicitud de reembolso deberá efectuarse dentro de las ocho semanas que siguen a la fecha de adeudo en cuenta. Puede obtener información adicional sobre sus derechos en su entidad financiera.
                                    <br><span class="eng">By signing this mandate form, you authorize (A) the Creditor to send instructions to your bank to debit your account and (B) your bank to debit your account in accordance with the instructions from the Creditor. As part of your rights, you are entitled to a refund from your bank under the terms and conditions of your agreement with your bank. A refund must be claimed within eight weeks starting from the date on which your account was debited. Your rights are explained in a statement that you can obtain from your bank.</span>
                                </p>
                            </div>
                            ${firma_contratos()}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-7 no-gutter">
                            <div class="cuadro_desestimiento">
                                <span class="head">Sólo debe cumplimentar y enviar el presente formulario si desea desistir del contrato</span><br>
                                <div class="text">
                                    <p>A la atención de Ab energía 1903, S.L.U. Escuelas Pías 12, 22300 de Barbastro, fax 974 314 744 y email info@benergia.es<br>
                                    Por la presente le comunico/comunicamos que desisto de mi/desistimos de nuestro
                                    contrato de suministro eléctrico.
                                    </p>
                                    <p class="sign">
                                        Pedido/ recibido el:<br>
                                        Nombre completo del consumidor:<br>
                                        Domicilio del consumidor:<br>
                                        Firma del consumidor<br>
                                    </p>
                                    Fecha:<br>
                                    DNI/NIF:<br>
                                    No Contrato:<br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        %endif
    %endfor
  </body>
</html>
