<%
    from datetime import datetime
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
    	<style type="text/css">
        ${css}
        body{
            font-family: helvetica;
            font-size: 11px;
            margin-right: 50px;
            margin-left: 50px;
            background-image: url(${addons_path}/giscedata_polissa_comer_abenergia/report/FACTURA_ABE.png);
            background-size: 100%;
            background-repeat:repeat-y;
            margin:80px;
        }
            table{
                font-size: 9px;
                width: 100%;
                border-collapse: collapse;
                border: none;
            }
            td{
                padding-left: 8px;
            }
            #contingut{
              position: relative;
              top: 230px;
            }
            #titol{
              text-align: center;
              font-weight: bold;
              font-size: 16px;
              width: 100%;
            }
            #escrit{
                position: relative;
                font-size: 9px;
                font-weight: bold;
                top: 25px;
                padding-left: 8px;
            }
            #lopd{
                position: relative;
                border: 1px solid black;
                padding: 5px;
                text-align: justify;
                top: 500px;
                font-size: 6px;
            }
            .format_text{
                position: relative;
                font-size: 9px;
                top: 20px;
                padding-left: 8px;
                margin-bottom: 35px;
            }
            .field{
              border-bottom: 1px solid black;
              font-style: italic;
            }
            .margin_top_table{
              margin-top: 25px;
            }
            .small_margin{
              margin-top: 5px;
            }
            .big_margin{
                margin-top: 25px;
            }
            .big_margin_left{
                margin-top: 25px;
                margin-left: 25px;
                border-collapse: separate !important;
                border-spacing: 3px;
            }
            .big_margin_left_others{
                border-collapse: separate !important;
                border-spacing: 3px;
                margin-left: 25px;
            }
            .textbox{
                border: 1px solid black;
                font-style: italic;
            }
            .textbox_others{
                border: 1px solid black;
                font-style: italic;
                width: 300px;
                height: 70px;
            }
            .checkbox{
                width: 5px;
            }
        </style>
    </head>
    <body>
        %for pol in objects:
            <div id="contingut">
                <div id="titol">
                    ${_("SOLICITUD DE BAJA DEL CONTRATO DE SUMINISTRO ELÉCTRICO")}
                </div>
                <table class="margin_top_table">
                    <tr>
                        <td width="90px">
                            ${_("Titular del suministro")}
                        </td>
                        <td class="field">
                            ${pol.titular.name}
                        </td>
                        <td width="75px">
                            ${_("Número de póliza")}
                        </td>
                        <td class="field">
                            ${pol.name}
                        </td>
                    </tr>
                </table>
                <table class="small_margin">
                    <tr>
                        <td width="20px">
                            ${_("CIF")}
                    </td>
                    <td class="field" width="170px">
                        ${pol.titular.vat.replace('ES','')}
                    </td>
                    <td width="30px">
                        ${_("CUPS")}
                    </td>
                    <td class="field">
                        ${pol.cups.name}
                    </td>
                </tr>
                </table>
                <table class="small_margin">
                    <tr>
                        <td width="100px">
                            ${_("Dirección del suministro")}
                        </td>
                        <td class="field" width="70%">
                            ${pol.cups.direccio}
                        </td>
                        <td width="25px">
                            ${_("C.P.")}
                        </td>
                        <td class="field">
                            ${pol.cups.dp}
                        </td>
                    </tr>
                </table>
                <table class="small_margin">
                    <tr>
                        <td width="45px">
                            ${_("Población")}
                        </td>
                        <td class="field" width="60%">
                            ${pol.cups.id_poblacio.name}
                        </td>
                        <td width="40px">
                            ${_("Provincia")}
                        </td>
                        <td class="field">
                            ${pol.cups.id_provincia.name}
                        </td>
                    </tr>
                </table>
                <div id="escrit">
                    ${_("Como titular del contrato del suministro arriba indicado, solicito a la empresa comercializadora Ab energía 1903, S.L.U. la baja del contrato del mencionado suministro eléctrico.")}
                </div>
                <br><br>
                <div class="format_text">
                    ${_("Motivo de la solicitud de baja")}
                </div>
                <table class="big_margin_left">
                    <tr>
                        <td class="textbox checkbox">
                        </td>
                        <td width="210px;">
                            ${_("Baja sin desconexión definitiva de instalaciones")}
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="textbox">
                        </td>
                        <td width="200px;">
                            ${_("Cese definitivo de suministro")}
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="textbox">
                        </td>
                        <td width="200px;">
                            ${_("Rescisión de contrato de energía")}
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
                <table class="big_margin_left_others">
                    <tr>
                        <td style="width: 30px; padding: 0px; margin-bottom: 2px;">
                            ${_("Otros")}
                        </td>
                    </tr>
                    <tr>
                        <td class="textbox_others">
                            &nbsp;
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
                <table class="big_margin">
                    <tr>
                        <td width="75px">
                            ${_("Observaciones")}
                        </td>
                        <td class="field">
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <table class="big_margin">
                    <tr>
                        <td width="50px">
                            ${_("Fecha de solicitud")}
                        </td>
                        <td class="textbox" width="50px">
                            ${datetime.now().strftime("%d-%m-%Y")}
                        </td>
                        <td width="190px">&nbsp;</td>
                        <td width="100px;">
                            ${_("Firma del titular del suministro")}
                        </td>
                    </tr>
                </table>
            </div>
            <div id="lopd">
                <b>AB ENERGÍA 1903, S.L.U.</b> es el Responsable del tratamiento de los datos personales
                proporcionados bajo su consentimiento y le informa que los mismos serán tratados de conformidad con
                lo dispuesto en las normativas vigentes en protección de datos personales, el Reglamento (UE)
                2016/679 de 27 de abril de 2016 (GDPR), con la finalidad de mantener una relación comercial y
                conservados mientras exista un interés mutuo para mantener el fin del tratamiento y cuando ya no sea
                necesario para tal fin, se suprimirán con medidas de seguridad adecuadas para garantizar la
                seudonimización de los datos o la destrucción total de los mismos. No se comunicarán los datos a
                terceros, salvo obligación legal. Asimismo, se informa que puede ejercer los derechos de acceso,
                rectificación, portabilidad y supresión de sus datos y los de limitación y oposición a su tratamiento
                dirigiéndose a <b>AB ENERGÍA 1903, S.L.U. en C/ Escuelas Pías, 12 - 22300 BARBASTRO
                (HUESCA). Email: info@abenergia.es y el de reclamación a www.aepd.es</b>
            </div>
        %endfor
    </body>
</html>
