<%
    from datetime import datetime
    from osv import osv
    from numbertoletters import number_to_letters
    pool = objects[0].pool
    crm_polissa_obj = pool.get('crm.polissa')
    payment_type_obj = pool.get('payment.type')
    pricelist_obj = pool.get('product.pricelist')

    def clean(text):
        return text or '&nbsp;'

    def enviament(diferent, text):
        if not diferent:
            return clean(text)
        else:
            return '&nbsp'

    def is_tarifa_ofertable(fields):
        idtarifa = pricelist_obj.search(cursor, uid, [('name', '=', fields['llista_preu'].name)])
        try:
            ofertable = pricelist_obj.read(cursor, uid, idtarifa[0], ['ofertable'])['ofertable']
        except IndexError:
            raise osv.except_osv(_('Atención'),
                                 _(u'El nombre de tarifa no tiene las traducciones correctas. Contacte con el responsable de generación de precios.'))
        return ofertable

    def get_fields(model, pol):
        fields = {}
        fields['firmante'] = False
        fields['firmante_nif'] = False
        if model == 'crm.polissa':
            fields['tipus_persona'] = pol.cliente_tipo_persona
            fields['bank'] = pol.num_cta
            ptype_id = payment_type_obj.search(cursor, uid, [('code', '=', 'RECIBO_CSB')])[0]
            fields['tipo_pago'] = payment_type_obj.browse(cursor, uid, ptype_id)
            fields['name'] = pol.contrato_name
            fields['tarifa'] = pol.tarifa_oferta
            fields['pagador'] = pol.partner_id.name
            fields['pagador_nif'] = pol.partner_id.vat[2:]
            fields['direccio_pagament'] = pol.cliente_direccion
            fields['direccio_notificacio'] = pol.contacto_direccion
            if pol.cups_id:
                fields['codigo_postal_ps'] = pol.cups_id.dp
                fields['poblacion_ps'] = pol.cups_id.id_poblacio.name
                fields['provincia_ps'] = pol.cups_id.id_provincia.name
                fields['pais_ps'] = pol.cups_id.id_provincia.country_id.name
                fields['cups'] = pol.cups_id.name
            else:
                fields['codigo_postal_ps'] = pol.cups_dp
                fields['poblacion_ps'] = pol.cups_id_poblacio.name
                fields['provincia_ps'] = pol.cups_id_provincia.name
                fields['pais_ps'] = pol.cups_id_provincia.country_id.name
                fields['cups'] = pol.cups_name
            fields['ref_catastral'] = pol.cups_ref_catastral
            fields['cups_direccio'] = pol.sips_direccion
            if not fields['cups_direccio']:
                if pol.cups_id:
                    fields['cups_direccio'] = pol.cups_id.direccio
                else:
                    stringtotal = ''
                    if pol.cups_tv:
                        stringtotal += pol.cups_tv.name + ' '
                    if pol.cups_nv:
                        stringtotal += pol.cups_nv + ' '
                    if pol.cups_pnp:
                        stringtotal += 'Nº' + pol.cups_pnp + ' '
                    if pol.cups_es:
                        stringtotal += pol.cups_es + ' '
                    if pol.cups_pt:
                        stringtotal += pol.cups_pt + ' '
                    if pol.cups_pu:
                        stringtotal += pol.cups_pu + ' '
                    if pol.cups_aclarador:
                        stringtotal += pol.cups_aclarador + ' '
                    fields['cups_direccio']  = stringtotal
            fields['distribuidora'] = pol.cups_distribuidora_id
            fields['tarifa_codi'] = pol.tarifa_oferta.name
            fields['potencies_periode'] = pol.oferta_potencies_periode
            fields['potencia'] = pol.potencia_ofertada
            if pol.pers_firmante_id and pol.pers_firmante_nif:
                fields['firmante'] = pol.pers_firmante_id.name
                fields['firmante_nif'] = pol.pers_firmante_nif[2:]
            fields['consumo_anual_previsto_directo'] = pol.consumo_anual_previsto_directo
            fields['envio_postal'] = pol.envio_postal
        elif model == 'giscedata.polissa':
            fields['tipus_persona'] = get_tipus_persona(pol.titular.cifnif)
            fields['bank'] = pol.bank
            fields['tipo_pago'] = pol.tipo_pago
            fields['name'] = pol.name
            fields['tarifa'] = pol.tarifa
            fields['pagador'] = pol.pagador.name
            fields['pagador_nif'] = pol.pagador_nif[2:]
            fields['direccio_pagament'] = pol.direccio_pagament
            fields['direccio_notificacio'] = pol.direccio_notificacio
            fields['codigo_postal_ps'] = pol.cups.dp
            fields['poblacion_ps'] = pol.cups.id_poblacio.name
            fields['provincia_ps'] = pol.cups.id_provincia.name
            fields['pais_ps'] = pol.cups.id_provincia.country_id.name
            fields['cups'] = pol.cups.name
            fields['ref_catastral'] = pol.cups.ref_catastral
            fields['cups_direccio'] = pol.cups_direccio
            fields['distribuidora'] = pol.distribuidora
            fields['tarifa_codi'] = pol.tarifa_codi
            fields['potencies_periode'] = pol.potencies_periode
            fields['potencia'] = pol.potencia
            if pol.firmante_id and pol.firmante_vat:
                fields['firmante'] = pol.firmante_id.name
                fields['firmante_nif'] = pol.firmante_vat[2:]
        fields['cnae'] = pol.cnae.name
        fields['mode_facturacio'] = pol.mode_facturacio
        fields['llista_preu'] = pol.llista_preu
        fields['consumo_anual_previsto'] = pol.consumo_anual_previsto
        fields['coeficient_k'] = pol.coeficient_k
        fields['coeficient_d'] = pol.coeficient_d
        fields['nombre_producto'] = pol.llista_preu.name
        if fields['mode_facturacio'] == 'index':
            fields['nombre_producto']= 'Indexado'
        fields['envio_postal'] = False
        return fields

%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
    	<style type="text/css">
        ${css}
        </style>
        <link rel="stylesheet" href="${addons_path}/giscedata_polissa_comer_abenergia/report/estil_ab.css"/>
    </head>
    <body>
        %for pol in objects:
            <%
            if data['model'] == 'crm.polissa':
                pol = crm_polissa_obj.browse(cursor, uid, data['id'])
                if not pol.partner_id:
                    raise osv.except_osv(_('Atención'),
                                         _(u'El campo "Cliente existente" es obligatorio en el contrato.'))
            fields = get_fields(data['model'], pol)
            if not fields['bank'] and fields['tipo_pago'].code == "RECIBO_CSB":
                    raise osv.except_osv(_('Atención'), _(u'La póliza {} no '
                    u'tiene ninguna cuenta bancaria '
                    u'especificada.'.format(fields['name'])))
            # Comprovem que la tarifa sigui ofertable al immprimir des d'un contracte en esborrany
            if not is_tarifa_ofertable(fields):
                raise osv.except_osv(_('Atención'), _(u'La tarifa {} no'
                u' es ofertable.'.format(fields['llista_preu'].name)))
            %>
            <div id="capcalera">
                <div id="titol">
                    ${_("CONDICIONES PARTICULARES DEL ")}<br>
                    ${_("CONTRATO DE SUMINISTRO DE ENERGÍA ")}<br>
                    <%
                        tarifa = fields['tarifa'].codi_ocsum
                    %>
                    % if tarifa in ['001', '003', '004', '005', '006', '007', '008']:
                        ${_("ELÉCTRICA DE BAJA TENSIÓN")}
                    % else:
                        ${_("ELÉCTRICA DE ALTA TENSIÓN")}
                    % endif

                </div>
                <div id="logo">
                    <img src="data:image/jpeg;base64,${company.logo}"/>
                </div>
            </div>
            <div style="clear: both;"></div>
            <div id="continguts">
                <div class="seccio_blau">
                    <div class="titol_seccio">
                        ${_("Datos cliente")}
                    </div>
                    <div class="cos_seccio">
                        <table class="borderless">
                            <tr>
                                <td class="td_seccio w_90">${_("Nombre/Razón social:")}</td>
                                <td class="td_seccio w_380 field">${fields['pagador']}</td>
                                <td class="td_seccio w_35">${_("NIF/CIF:")}</td>
                                <td class="td_seccio field">${fields['pagador_nif']}</td>
                            </tr>
                        </table>
                        <% direccio_titular = fields['direccio_pagament'] %>
                        <table class="borderless">
                            <tr>
                                <td class="td_seccio w_45">${_("Dirección:")}</td>
                                <td class="td_seccio field">${clean(direccio_titular.street)}</td>
                            </tr>
                        </table>
                        <table class="borderless">
                            <tr>
                                <td class="td_seccio w_60">${_("Código Postal:")}</td>
                                <td class="td_seccio field w_110">${clean(direccio_titular.zip)}</td>
                                <td class="td_seccio w_45">${_("Población:")}</td>
                                <td class="td_seccio field w_110">${clean(direccio_titular.id_poblacio.name)}</td>
                                <td class="td_seccio w_45">${_("Província:")}</td>
                                <td class="td_seccio field w_92">${clean(direccio_titular.state_id.name)}</td>
                                <td class="td_seccio w_25">${_("País:")}</td>
                                <td class="td_seccio field">${clean(direccio_titular.country_id.name)}</td>
                            </tr>
                        </table>
                        <table class="borderless">
                            <tr>
                                <td class="td_seccio w_50">${_("Teléfono 1:")}</td>
                                <td class="td_seccio field w_279">${clean(direccio_titular.mobile)}</td>
                                <td class="td_seccio w_50">${_("Teléfono 2:")}</td>
                                <td class="td_seccio field w_87">${clean(direccio_titular.phone)}</td>
                                <td class="td_seccio w_25">${_("Fax:")}</td>
                                <td class="td_seccio field">${clean(direccio_titular.fax)}</td>
                            </tr>
                        </table>
                        <table class="borderless">
                            <tr>
                                <td class="td_seccio w_30">${_("Email:")}</td>
                                <td class="td_seccio field">${clean(direccio_titular.email)}</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <%
                    firmante_juridic = fields['tipus_persona'] == 'juridica'
                    firmante_fisic = data['model'] == 'giscedata.polissa' and fields['tipus_persona'] == 'fisica' and fields['firmante'] and fields['firmante_nif']

                %>
                % if firmante_fisic or firmante_juridic:
                    <div class="seccio_blau">
                        <div class="titol_seccio">
                            ${_("Datos persona firmante")}
                        </div>
                        <div class="cos_seccio">
                            <table class="borderless">
                                <tr>
                                    <td class="td_seccio w_90">${_("Nombre/Razón social:")}</td>
                                    <td class="td_seccio w_380 field">${fields['firmante'] or ''}</td>
                                    <td class="td_seccio w_35">${_("NIF/CIF:")}</td>
                                    <td class="td_seccio field">${fields['firmante_nif'] or ''}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                % endif
                <%
                    direccio_envio = fields['direccio_notificacio']
                    diferent = (fields['direccio_notificacio'] == fields['direccio_pagament'])
                %>
                <div class="seccio_blau">
                    <div class="titol_seccio">
                        ${_("Datos de contacto")}
                    </div>
                    <div class="subtitol_seccio">
                        ${_("(envío de factura, otras comunicaciones... Omitir solamente si no difiere de los datos de cliente)")}
                    </div>
                    <div class="cos_seccio">
                        <table class="borderless">
                            <tr>
                                <td class="td_seccio w_90">${_("Nombre/Razón social:")}</td>
                                <td class="td_seccio field">${enviament(diferent, direccio_envio.name)}</td>
                            </tr>
                        </table>
                        <% direccio_titular = fields['direccio_pagament'] %>
                        <table class="borderless">
                            <tr>
                                <td class="td_seccio w_45">${_("Dirección:")}</td>
                                <td class="td_seccio field">${enviament(diferent, direccio_envio.street)}</td>
                            </tr>
                        </table>
                        <table class="borderless">
                            <tr>
                                <td class="td_seccio w_60">${_("Código Postal:")}</td>
                                <td class="td_seccio field w_110">${enviament(diferent, direccio_envio.zip)}</td>
                                <td class="td_seccio w_45">${_("Población:")}</td>
                                <td class="td_seccio field w_110">${enviament(diferent, direccio_envio.id_poblacio.name)}</td>
                                <td class="td_seccio w_45">${_("Província:")}</td>
                                <td class="td_seccio field w_92">${enviament(diferent, direccio_envio.state_id.name)}</td>
                                <td class="td_seccio w_25">${_("País:")}</td>
                                <td class="td_seccio field">${enviament(diferent, direccio_envio.country_id.name)}</td>
                            </tr>
                        </table>
                        <table class="borderless">
                            <tr>
                                <td class="td_seccio w_50">${_("Teléfono 1:")}</td>
                                <td class="td_seccio field w_279">${enviament(diferent, direccio_envio.mobile)}</td>
                                <td class="td_seccio w_50">${_("Teléfono 2:")}</td>
                                <td class="td_seccio field w_87">${enviament(diferent, direccio_envio.phone)}</td>
                                <td class="td_seccio w_25">${_("Fax:")}</td>
                                <td class="td_seccio field">${enviament(diferent, direccio_envio.fax)}</td>
                            </tr>
                        </table>
                        <table class="borderless">
                            <tr>
                                <td class="td_seccio w_30">${_("Email:")}</td>
                                <td class="td_seccio field">${enviament(diferent, direccio_envio.email)}</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div id="titol_mig">
                    ${_("Condiciones técnico-económicas")}
                </div>
                <%
                    direccio_cups = fields['cups_direccio']
                    idx_pob = direccio_cups.rfind('(')
                    if idx_pob != -1:
                        direccio_cups = direccio_cups[:idx_pob]
                %>
                <div class="seccio_verd">
                    <div class="titol_seccio">
                        ${_("Datos del punto de suministro")}
                    </div>
                    <div class="subtitol_seccio">
                        ${_("(Omitir solamente si no difiere de los datos de cliente)")}
                    </div>
                    <div class="cos_seccio">
                        <% direccio_titular = fields['direccio_pagament'] %>
                        <table class="borderless">
                            <tr>
                                <td class="td_seccio w_45">${_("Dirección:")}</td>
                                <td class="td_seccio field">${direccio_cups}</td>
                            </tr>
                        </table>
                        <table class="borderless">
                            <tr>
                                <td class="td_seccio w_60">${_("Código Postal:")}</td>
                                <td class="td_seccio field w_110">${clean(fields['codigo_postal_ps'])}</td>
                                <td class="td_seccio w_45">${_("Población:")}</td>
                                <td class="td_seccio field w_110">${clean(fields['poblacion_ps'])}</td>
                                <td class="td_seccio w_45">${_("Província:")}</td>
                                <td class="td_seccio field w_92">${clean(fields['provincia_ps'])}</td>
                                <td class="td_seccio w_25">${_("País:")}</td>
                                <td class="td_seccio field">${clean(fields['pais_ps'])}</td>
                            </tr>
                        </table>
                        <table class="borderless">
                            <tr>
                                <td class="td_seccio w_30">${_("CUPS:")}</td>
                                <td class="td_seccio field w_145">${fields['cups']}</td>
                                <td class="td_seccio w_60">${_("Ref.Catastral:")}</td>
                                <td class="td_seccio field w_110">${fields['ref_catastral'] or ''}</td>
                                <td class="td_seccio w_30">${_("CNAE:")}</td>
                                <td class="td_seccio field w_87">${clean(fields['cnae'])}</td>
                                <td class="td_seccio w_55">${_("Contrato nº:")}</td>
                                <td class="td_seccio field">${clean(fields['name'])}</td>
                            </tr>
                        </table>
                        <table class="borderless">
                            <tr>
                                <td class="td_seccio w_98">${_("Empresa distribuidora:")}</td>
                                <td class="td_seccio field">${clean(fields['distribuidora'].name)}</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="seccio_verd">
                    <div class="titol_seccio">
                        ${_("Tarifa de acceso")}
                    </div>
                    <div class="subtitol_seccio">
                        ${_("(Definidas en R.D. 1164/2001 del 26 de octubre de 2001)")}
                    </div>
                    <div class="cos_seccio">
                        <table class="borderless">
                            <tr>
                                <td class="td_seccio w_30">${_("Tarifa:")}</td>
                                <td class="td_seccio field w_60">${clean(fields['tarifa_codi'])}</td>
                                <td class="td_seccio w_90">${_("Potencia contratada:")}</td>
                                <td class="td_seccio field">${clean(formatLang(fields['potencia'], 3))}</td>
                            </tr>
                        </table>
                        <%
                            potencies = fields['potencies_periode']
                            periodes = []
                            for i in range(0, 6):
                                if i < len(potencies):
                                    periode = potencies[i]
                                else:
                                    periode = False
                                periodes.append(periode)
                        %>
                        <table class="borderless">
                            <tr>
                                % for elem in range(0,6):
                                    <td class="td_seccio w_15">P${elem+1}:</td>
                                    % if periodes[elem]:
                                        <td class="td_seccio field w_75">${clean(formatLang(periodes[elem].potencia, 3))}</td>
                                    % else:
                                        <td class="td_seccio field w_75">&nbsp;</td>
                                    % endif
                                % endfor
                                <td></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="seccio_verd">
                    <div class="titol_seccio">
                        ${_("Producto contratado con Ab energía 1903, S.L.U.")}
                    </div>
                    <div class="cos_seccio">
                        <table class="borderless">
                            <tr>
                                <td class="td_seccio w_45">${_("Producto:")}</td>
                                <td class="td_seccio field w_145">${fields['nombre_producto']}</td>
                                <td class="td_seccio w_45">${_("Duración:")}</td>
                                <td class="td_seccio field w_110">Anual</td>
                                <!-- A la oferta tenim el consumo_anual_previsto_directo -->
                                %if data['model'] == 'crm.polissa':
                                    %if fields['consumo_anual_previsto_directo'] > 0:
                                    <td class="td_seccio">${_("Consumo anual previsto (kWh):")}</td>
                                    <td class="td_seccio field">${fields['consumo_anual_previsto_directo']}</td>
                                    %elif fields['consumo_anual_previsto'].isdigit() and fields['consumo_anual_previsto'] > 0:
                                    <td class="td_seccio">${_("Consumo anual previsto (kWh):")}</td>
                                    <td class="td_seccio field">${fields['consumo_anual_previsto']}</td>
                                    %endif
                                <!-- A la polissa NO tenim el consumo_anual_previsto_directo -->
                                %elif data['model'] == 'giscedata.polissa':
                                    %if fields['consumo_anual_previsto'] > 0:
                                    <td class="td_seccio">${_("Consumo anual previsto (kWh):")}</td>
                                    <td class="td_seccio field">${fields['consumo_anual_previsto']}</td>
                                    %endif
                                %endif
                            </tr>
                        </table>
                    </div>
                </div>
                <div id="info_checkboxes">
                    <table class="borderless">
                        <tr>
                            %if fields['envio_postal']:
                                <td class="td_seccio w_10"><div class="custom_checkbox_checked w_10"/></td>
                            %else:
                                <td class="td_seccio w_10"><div class="custom_checkbox w_10"/></td>
                            %endif
                            <td class="td_seccio field w_160 pl_3">${_("Quiero recibir mi factura en papel")}</td>
                            <td class="td_seccio w_10"><div class="custom_checkbox w_10"/></td>
                            <td class="td_seccio field pl_3">${_("No acepto uso comercial de datos")}</td>
                            <td class="td_seccio">&nbsp;</td>
                        </tr>
                    </table>
                </div>
                <div class="nota t_10">
                    ${_("Barbastro, a ")}${datetime.now().strftime("%d")} de ${localize_period_month(datetime.now().strftime("%Y-%m-%d"), 'es_ES')} de ${datetime.now().strftime("%Y")}
                </div>
                    <div class="signatura">
                <center>
                    <div class="cos_signatura"></div>
                    <div class="text_signatura">
                        <%
                            signatura = ''
                            dni_signatura = ''
                            if fields['firmante'] and fields['firmante_nif'] and ((fields['tipus_persona'] == 'juridica' and data['model'] == 'crm.polissa') or (data['model'] == 'giscedata.polissa')):
                                signatura = fields['firmante'] or ''
                                dni_signatura = fields['firmante_nif'] or ''
                            else:
                                if fields['tipus_persona'] == 'fisica':
                                    signatura = fields['pagador'] or ''
                                    dni_signatura = fields['pagador_nif'] or ''
                        %>
                        Fdo. ${signatura}
                        <br>
                        DNI Firmante: ${dni_signatura}
                        <br>
                    </div>
                </center>
                </div>
                <div class="signatura" style="float: right !important;">
                        <center>
                            <div class="cos_signatura verd">
                                <img id="firma_direccion" src="${addons_path}/giscedata_polissa_comer_abenergia/report/firmaDireccion.png"/>
                            </div>
                            <div class="text_signatura">
                                Fdo. Pablo Bravo Vidal
                            </div>
                        </center>
                </div>
                </div>
            </div>
        %endfor
    </body>
</html>
