<%
    from datetime import datetime
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
    	<style type="text/css">
        ${css}
            body{
                background-image: url(${addons_path}/giscedata_polissa_comer_abenergia/report/FACTURA_ABE.png);
                background-size: 100%;
                background-repeat:no-repeat;
                color: black !important;
            }
            li{
              margin: 15px 0;
            }
            #container{
                margin-top: 210px;
                padding-left: 35px;
                height: 800px;
                text-align: justify;
            }
            #titol_renuncia{
                position: relative;
                top: 20px;
                font-weight: bold;
                text-align: center;
            }
            #contingut_text_renuncia{
                position: relative;
                top: 40px;
            }
        </style>
        <link rel="stylesheet" href="${addons_path}/giscedata_polissa_comer_abenergia/report/estil_ab.css"/>
    </head>
    <body>
        %for pol in objects:
            <div id="container">
                <div id="titol_renuncia">
                    Renuncia a la aplicación del bono social
                </div>
                <div id="contingut_text_renuncia">
                    <p>
                        Mediante la firma del presente documento, como titular del contrato de suministro de energía eléctrica
                        número ${pol.name} con la empresa ${company.name}, RENUNCIO
                        expresamente a la aplicación del bono social y por tanto, renuncio al correspondiente descuento
                        en la factura sobre el precio voluntario para el pequeño consumidor (PVPC) a la que tendría derecho si
                        yo, o mi unidad familiar, cumpliese alguno de los requisitos siguientes:
                    </p>
                    <ul>
                        <li>
                            Cumplir los umbrales de renta establecidos, teniendo en cuenta la posible concurrencia de las circunstancias especiales que aumentan dichos umbrales (discapacidad reconocida igual o superior al 33%, condición de violencia de género, condición de víctima de terrorismo).
                        </li>
                        <li>
                            Estar en posesión del título de familia numerosa.
                        </li>
                        <li>
                            Ser pensionista, o que todos los miembros de mi unidad familiar lo sean, del Sistema de la Seguridad Social por jubilación o incapacidad permanente, percibiendo la cuantía mínima vigente en cada momento.
                        </li>
                    </ul>
                    En ${pol.delegacion.name}, a ${datetime.now().strftime("%d")} de ${localize_period_month(datetime.now().strftime("%Y-%m-%d"), 'es_ES')} de ${datetime.now().strftime("%Y")}
                    <br>
                    Firmado,
                    <br>
                    ${pol.titular.name}
                    <br>
                    <%
                        vat = ''
                        if pol.titular.vat:
                            vat = pol.titular.vat[2:]
                    %>
                    DNI: ${vat}
                </div>
            </div>
        %endfor
    </body>
</html>