<%
    from datetime import datetime
    from numbertoletters import number_to_letters
    pool = objects[0].pool
    tarifa_obj = pool.get('giscedata.polissa.tarifa')
    crm_polissa_obj = pool.get('crm.polissa')

    def localize_period_month(period, locale):
        if period != 'False':
            import babel
            from datetime import datetime
            dtf = datetime.strptime(period, '%Y-%m-%d')
            dtf = dtf.strftime("%y%m%d")
            dt = datetime.strptime(dtf, '%y%m%d')
            return babel.dates.format_datetime(dt, 'LLLL', locale=locale)
        else:
            return ''

    dict_tarifes = {
        '001': '2.0A',
        '005': '2.1A',
        '004': '2.0DHA',
        '006': '2.1DHA',
        '003': '3.0A',
        '011': '3.1A',
        '012': '6.1A',
    }

    def calcular_coeficients(fields, coef):
        if coef == 'D':
            return fields['coeficient_d'] / 1000.0
        else:
            return fields['coeficient_k'] / 1000.0

    def get_fields(model, pol):
        fields = {}
        fields['firmante'] = False
        fields['firmante_nif'] = False
        model_obj = pool.get(model)
        if model == 'crm.polissa':
            fields['tipus_persona'] = pol.cliente_tipo_persona
            fields['razon_social'] = pol.partner_id.name
            fields['nif'] = pol.partner_id.vat
            fields['cups'] = pol.cups_sips if pol.cups_sips else pol.cups_name
            fields['ref_catastral'] = pol.cups_ref_catastral
            fields['cnae'] = pol.cnae.name
            fields['num_contrato'] = pol.contrato_name
            fields['potencias_periodo'] = []
            for elem in pol.oferta_potencies_periode:
                fields['potencias_periodo'].append(elem)
            fields['tarifa'] = pol.tarifa_oferta
            fields['llista_preu'] = pol.llista_preu
            fields['modo_facturacion'] = pol.mode_facturacio
            fields['coeficient_d'] = pol.coeficient_d
            fields['coeficient_k'] = pol.coeficient_k
            fields['fact_potencia_100'] = pol.fact_potencia_100
            fields['firmante'] = pol.pers_firmante_id.name
            fields['firmante_nif'] = pol.pers_firmante_nif
            fields['distri'] = pol.cups_distribuidora_id.name
            fields['tension_normalizada'] = ''
            fields['pagador'] = pol.partner_id.name
            fields['pagador_nif'] = pol.partner_id.vat
            if pol.pers_firmante_id and pol.pers_firmante_nif:
                fields['firmante'] = pol.pers_firmante_id.name
                fields['firmante_nif'] = pol.pers_firmante_nif
        elif model == 'giscedata.polissa':
            fields['tipus_persona'] = get_tipus_persona(pol.titular.cifnif)
            fields['razon_social'] = pol.pagador.name
            fields['nif'] = pol.pagador_nif
            fields['cups'] = pol.cups.name
            fields['ref_catastral'] = pol.cups.ref_catastral
            fields['cnae'] = pol.cnae.name
            fields['num_contrato'] = pol.name
            fields['potencias_periodo'] = []
            for elem in pol.potencies_periode:
                fields['potencias_periodo'].append(elem)
            fields['tarifa'] = pol.tarifa
            fields['llista_preu'] = pol.llista_preu
            fields['modo_facturacion'] = pol.mode_facturacio
            fields['coeficient_d'] = pol.coeficient_d
            fields['coeficient_k'] = pol.coeficient_k
            fields['fact_potencia_100'] = pol.fact_potencia_100
            fields['firmante'] = pol.firmante_id.name
            fields['firmante_nif'] = pol.firmante_vat
            fields['distri'] = pol.distribuidora.name
            tensio = ""
            if pol.tensio_normalitzada.name != 'Desconocido':
                tensio = pol.tensio_normalitzada.name + " (V)"
            fields['tension_normalizada'] = tensio
            fields['pagador'] = pol.pagador.name
            fields['pagador_nif'] = pol.pagador_nif
            if pol.firmante_id and pol.firmante_vat:
                fields['firmante'] = pol.firmante_id.name
                fields['firmante_nif'] = pol.firmante_vat

        return fields

%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
    	<style type="text/css">
        ${css}
        </style>
        <link rel="stylesheet" href="${addons_path}/giscedata_polissa_comer_abenergia/report/estil_ab.css"/>
    </head>
    <body>
        %for pol in objects:
            <%
                descompte_energia_global = 0.0
                descompte_potencia_global = 0.0
                if data['model'] == 'crm.polissa':
                    pol = crm_polissa_obj.browse(cursor, uid, data['id'])
            %>
            %if data['model'] == 'crm.polissa' and not pol.partner_id:
                <div style="font-size: 10pt">
                    El campo "Cliente existente" es obligatorio en el anexo de precios.
                </div>
            %else:
                <%
                    fields = get_fields(data['model'], pol)
                    single_price = fields['llista_preu'].single_price()
                    tarifa = dict_tarifes[fields['tarifa'].codi_ocsum]
                    num_meses_energia = 12
                    num_meses_potencia = 12
                    if 'HUESCA 50' in fields['llista_preu'].name and '2.0' in fields['tarifa'].name:
                        num_meses_potencia = 3
                    elif 'HUESCA 50' in fields['llista_preu'].name and '2.1' in fields['tarifa'].name:
                        num_meses_potencia = 2
                %>
                <div id="capcalera">
                    <div id="titol">
                        ${_("ANEXO DE PRECIOS")}
                    </div>
                    <div id="logo">
                        <img src="data:image/jpeg;base64,${company.logo}"/>
                    </div>
                </div>
                <div style="clear: both;"></div>
                <div id="continguts">
                    <div class="seccio_blau">
                        <div class="titol_seccio">
                            ${_("Datos cliente")}
                        </div>
                        <div class="cos_seccio">
                            <table class="borderless">
                                <tr>
                                    <td class="td_seccio w_100">${_("Nombre/Razón social:")}</td>
                                    <td class="td_seccio w_380 field">${fields['razon_social']}</td>
                                    <td class="td_seccio w_40">${_("NIF/CIF:")}</td>
                                    <td class="td_seccio field">${fields['nif']}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="seccio_blau">
                        <div class="titol_seccio">
                            ${_("Datos del punto de suministro")}
                        </div>
                        <div class="cos_seccio">
                            <table class="borderless">
                                <tr>
                                    <td class="td_seccio w_100">${_("Empresa distribuidora:")}</td>
                                    <td class="td_seccio w_380 field">${fields['distri'] or ''}</td>
                                    <td class="td_seccio w_70">${_("Tensión nominal:")}</td>
                                    <td class="td_seccio field">${fields['tension_normalizada']}</td>
                                </tr>
                            </table>
                            <table class="borderless">
                                <tr>
                                    <td class="td_seccio w_30">${_("CUPS:")}</td>
                                    <td class="td_seccio field w_145">${fields['cups']}</td>
                                    <td class="td_seccio w_60">${_("Ref. Catastral:")}</td>
                                    <td class="td_seccio field w_145">${fields['ref_catastral'] or ''}</td>
                                    <td class="td_seccio w_30">${_("CNAE:")}</td>
                                    <td class="td_seccio field w_60">${fields['cnae'] or ''}</td>
                                    <td class="td_seccio w_50">${_("Contrato nº:")}</td>
                                    <td class="td_seccio field">${fields['num_contrato']}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="seccio_verd">
                        <div class="titol_seccio">
                            ${_("Término de potencia")}
                        </div>
                        <div class="cos_seccio">
                            <table class="inner_section">
                                <tr>
                                    <td class="left borderless w_145"></td>
                                    % for pot in fields['potencias_periodo']:
                                        % if data['model'] == 'crm.polissa':
                                            <td class="text_inner_section">${pot.periode}</td>
                                        % else:
                                            <td class="text_inner_section">${pot.periode_id.name}</td>
                                        % endif
                                    % endfor
                                </tr>
                                <tr>
                                    <td class="left borderless w_145">${_("Potencia contratada")}</td>
                                    % for pot in fields['potencias_periodo']:
                                        <%
                                            potencia = formatLang(pot.potencia, 3)
                                        %>
                                        <td class="text_inner_section not_bold">${potencia}</td>
                                    % endfor
                                </tr>
                                <tr>
                                    <td class="left borderless w_165">${_("Precio por periodo €/kW/día")}</td>
                                    %for pot in fields['potencias_periodo']:
                                        <%
                                            if data['model'] == 'crm.polissa':
                                                nom_periode =  pot.periode
                                            else:
                                                nom_periode = pot.periode_id.name
                                            endif
                                            price, descompte_potencia = get_atr_potencia_price(cursor, uid, fields, nom_periode, 'tp')
                                            if descompte_potencia and not descompte_potencia_global:
                                                descompte_potencia_global = descompte_potencia * 100.0
                                        %>
                                        <td class="text_inner_section not_bold">
                                            ${formatLang(price, 6)}
                                        </td>
                                    %endfor
                                </tr>
                            </table>
                            %if descompte_potencia_global:
                                <div class="descompte">
                                    Descuento ${formatLang(descompte_potencia_global, digits=1)}% durante los ${num_meses_potencia} primeros meses
                                </div>
                            %endif
                        </div>
                    </div>
                    <div class="nota">
                        ${_("Se aplica el precio y condiciones de la facturación del término de potencia de la tarifa de acceso ")}
                        ${tarifa}
                        ${_(" vigente.")}
                        %if descompte_potencia_global:
                            <br>
                            Descuento aplicable sobre el término de potencia durante los ${num_meses_potencia} primeros meses a partir de la activación del contrato.
                        %endif
                        %if fields['fact_potencia_100']:
                            <br>
                            ${_("La potencia se facturará como producto del término de facturación de potencia por la potencia "
                            "contratada. En caso de lectura por maxímetro, si la potencia demandada sobrepasa en cualquier período"
                            " horario la potencia contratada en el mismo, la potencia a facturar en ese periodo se calculará en "
                            "base al cálculo de los excesos establecidos en el RD1164/2001 o normativa que lo sustituya.")}
                        %endif
                    </div>
                    <div class="seccio_verd">
                        <div class="titol_seccio">
                            ${_("Precio de la energía")}
                        </div>
                        <div class="cos_seccio">
                            % if fields['modo_facturacion'] == 'index':
                                <div style="clear: both;"/>
                                ${_("Tarifa indexada")}
                                <br>
                                ${_("La tarifa indexada tiene como objetivo facturar según el coste de la energía cada hora"
                                " y todos los gastos asociados a la comercialización de la energía eléctrica según la fórmula"
                                " siguiente:")}
                                <br>
                                <br>
                                <strong>PHF = IMU * [(PMD + POS + SI + PC3 + OMIE_REE) * PERD + K + D + FONDO_EFI] + PA</strong>
                                <p class="font_9px">
                                      PHF: Precio horario final. IMU: Impuesto municipal. PMD: Precio mercado diario. POS:
                                      Costes y servicios del sistema. SI: Servicio de interrumpibilidad. PC3: Pagos por capacidad.
                                      OMIE REE: Remuneración del operador del sistema y del operador de mercado. PERD: Perdidas de
                                      distribución. K: Costes de gestión. D: Desvíos. FONDO_EFI: Coste destinado al fondo
                                      de eficiencia. PA: Peajes de acceso a la red.
                                </p>
                                <table class="borderless">
                                    <tr>
                                        <td class="w_10 field"><strong>K:</strong></td>
                                        <td class="w_100 field">${_("Costes de gestión")}</td>
                                        <td class="w_60 field">${formatLang(calcular_coeficients(fields, 'K'), 5)}</td>
                                        <td class="w_60 field">€/Kwh</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                </table>

                            % else:
                                <%
                                    periodes_agrupats = tarifa_obj.get_grouped_periods(cursor, uid, fields['tarifa'].id)
                                    energia_periodes = tarifa_obj.get_periodes_producte(cursor, uid, fields['tarifa'].id, 'te')
                                    sorted_periodes = sorted(list(set(energia_periodes) - set(periodes_agrupats)))
                                    if single_price:
                                        old_sorted_periodes = sorted_periodes
                                        sorted_periodes = [sorted_periodes[0]]
                                %>
                                <table class="inner_section">
                                    <tr>
                                        <td class="left borderless w_145"></td>
                                        %for periode in sorted_periodes:
                                            <td class="text_inner_section">
                                                %if single_price:
                                                    ${u' / '.join(old_sorted_periodes)}
                                                %else:
                                                    ${periode}
                                                %endif
                                            </td>
                                        %endfor
                                    </tr>
                                    <tr>
                                        <td class="left borderless w_165">${_("Energía: Precio por periodo €/kWh")}</td>
                                        %for periode in sorted_periodes:
                                            <%
                                                price, descompte_energia = get_atr_potencia_price(cursor, uid, fields, periode, 'te')
                                                if descompte_energia and not descompte_energia_global:
                                                    descompte_energia_global = descompte_energia * 100.0
                                            %>
                                            <td class="text_inner_section not_bold">
                                                ${formatLang(price, 6)}
                                            </td>
                                        %endfor
                                    </tr>
                                </table>
                            % endif
                            %if descompte_energia_global:
                                <div class="descompte">
                                    Descuento ${formatLang(descompte_energia_global, digits=1)}% durante los ${num_meses_energia} primeros meses
                                </div>
                            %endif
                        </div>
                    </div>
                    <div class="nota">
                        ${_("El precio de la energía incorpora el precio de la tarifa de acceso ")}
                        ${tarifa}
                        ${_(" vigente, a aplicar sobre la energía consumida en el período horario correspondiente.")}
                        %if descompte_energia_global:
                            <br>
                            %if fields['llista_preu'].variable_discount:
                               El descuento a aplicar será del ${formatLang(fields['llista_preu'].conditional_discount, digits=1)}% para
                               aquellos periodos de facturación cuyo peso de consumo en valle sea
                               superior al ${formatLang(fields['llista_preu'].p2_precent, digits=1)}%.
                            %else:
                               Descuento aplicable sobre el término de energía durante los ${num_meses_energia} primeros meses a partir de la activación del contrato.
                            %endif
                        %endif
                        <%
                            tarifes = ['3.0A','3.1A','3.1A LB', '6.1A']
                        %>
                    </div>
                    % if tarifa not in ['2.0A', '2.1A'] and not single_price:
                        <div class="seccio_verd">
                            <div class="titol_seccio">
                                ${_("Calendario de aplicación de períodos")}
                            </div>
                            % if fields['modo_facturacion'] == 'index':
                                <div class="cos_seccio">
                                    <div style="clear: both;"/>
                                    ${_("Los horarios de aplicación de periodos se fijan según el Real Decreto 1164/2001, de 26 de octubre,"
                                    " y sus modificaciones, por el que se establecen las tarifas de acceso a las redes de transporte y "
                                    " distribución de energía eléctrica.")}
                                    <br>
                                    ${_("* Tabla de horarios de acceso en la Oficina Virtual.")}
                                </div>
                            % else:
                                % if tarifa not in ['2.0DHA', '2.1DHA']:
                                    <div class="separador">&nbsp</div>
                                % else:
                                    <div class="separador" style="left: 1px;">&nbsp</div>
                                % endif
                                <div class="cos_seccio no_edges_padding">
                                    <table class="inner_section no_edges_padding">
                                        <tr>
                                            <%
                                                if tarifa not in ['2.0DHA', '2.1DHA']:
                                                    val = 3
                                                else:
                                                    val = 2
                                            %>
                                                <td class="borderless no_edges_padding" colspan=${val}>${_("Invierno")}</td>
                                                <td class="no_edges_padding" colspan=${val}>${_("Verano")}</td>

                                        </tr>
                                        <tr>
                                            <td class="borderless no_edges_padding">${_("P1")}</td>
                                            <td class="no_edges_padding">${_("P2")}</td>
                                            % if tarifa not in ['2.0DHA', '2.1DHA']:
                                                <td class="no_edges_padding">${_("P3")}</td>
                                            % endif
                                            <td class="no_edges_padding">${_("P1")}</td>
                                            <td class="no_edges_padding">${_("P2")}</td>
                                            % if tarifa not in ['2.0DHA', '2.1DHA']:
                                                <td class="no_edges_padding">${_("P3")}</td>
                                            % endif
                                        </tr>
                                        <tr>
                                            % if tarifa not in ['2.0DHA', '2.1DHA']:
                                                <td class="borderless no_edges_padding">${_("18 - 22h.")}</td>
                                                <td class="no_edges_padding">${_("08-18 / 22-24 h.")}</td>
                                                <td class="no_edges_padding">${_("00 - 08 h.")}</td>
                                                <td class="no_edges_padding">${_("11 - 15 h.")}</td>
                                                <td class="no_edges_padding">${_("08-11 / 15-24 h.")}</td>
                                                <td class="no_edges_padding">${_("00 - 08 h.")}</td>
                                            % else:
                                                <td class="borderless no_edges_padding">${_("12:00 - 22:00 h.")}</td>
                                                <td class="no_edges_padding">${_("00:00 - 12:00 / 22:00 - 00:00 h.")}</td>
                                                <td class="no_edges_padding">${_("13:00 - 23:00 h.")}</td>
                                                <td class="no_edges_padding">${_("00:00 - 13:00 / 23:00 - 00:00 h.")}</td>
                                            % endif
                                        </tr>
                                    </table>
                                </div>
                            % endif
                        </div>
                        <div class="nota">
                            ${_("Actualmente establecido en el segundo punto 3 del Anexo II Orden ITC-2794/2007. "
                            "El cambio de horario de invierno a verano y viceversa coincidirá con la fecha del cambio oficial de hora.")}
                        </div>
                    % endif
                    <div class="seccio_verd">
                        <div class="titol_seccio">
                            ${_("Energía reactiva")}
                        </div>
                        <div class="cos_seccio">
                            <table class="inner_section">
                                <tr>
                                    <td class="padding left_only no_padding_left  borderless">${_("Energía reactiva")}</td>
                                    <td class="padding left_only ">${_("Si coseno de φ >= 0.95")}</td>
                                    <td class="padding left_only ">${_("0.8 <= coseno de φ < 0.95")}</td>
                                    <td class="padding left_only ">${_("Si coseno de φ < 0.8")}</td>
                                </tr>
                                <tr>
                                    <td class="padding left_only no_padding_left borderless">${_("€/kVArh")}</td>
                                    <td class="text_inner_section">${_("No se penaliza")}</td>
                                    <td class="text_inner_section">${_("Se penaliza a ")}0.041554</td>
                                    <td class="text_inner_section">${_("Se penaliza a ")}0.062332</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="nota">
                        ${_("Se aplican los precios y condiciones de facturación de la energía reactiva restablecidas por la tarifa de acceso ")}
                        ${tarifa}
                        ${_(" vigente actualmente R.D. 1164/2001). Se aplicará a todos los periodos tarifarios, excepto el periodo valle, "
                        "siempre que el consumo de energía reactiva exceda el 33% del consumo de la energía activa durante el proceso de "
                        "facturación considerado.")}
                    </div>
                    <div class="h_50"></div>
                    <div style="clear: both;"></div>
                    % if fields['modo_facturacion'] == 'index':
                        <div class="nota t_20 bold">
                    % else:
                        <div class="nota t_80 bold">
                    %endif
                        ${_("Precio sujeto a modificaciones según la disposición que sigue de aplicación.")}
                    </div>
                    <div class="nota gran bold">
                        ${_("Facturación de las tarifas de acceso según reglamentación vigente en cada momento.")}<br>
                        ${_("Cada uno de los precios incluidos en este anexo traen incorporados los correspondientes impuestos excepto I.E e I.V.A.")}
                    </div>
                    <div class="nota">
                        ${_("En caso de que no se cumpla alguna de las condiciones requeridas para acogerse al descuento comercial y se haya producido ")}
                        ${_("alguna devolución en el pago del recibo durante la duración del contrato, el comercializador podrá dejar de aplicar el importe correspondiente al descuento comercial.")}
                    </div>
                    <div class="nota t_10">
                        ${_("Barbastro, a ")}${datetime.now().strftime("%d")} de ${localize_period_month(datetime.now().strftime("%Y-%m-%d"), 'es_ES')} de ${datetime.now().strftime("%Y")}
                    </div>
                </div>
                <div class="signatura_annex">
                    <center>
                        <div class="cos_signatura"></div>
                        <div class="text_signatura">
                            <%
                                signatura = ''
                                dni_signatura = ''
                                if fields['firmante'] and fields['firmante_nif'] and ((fields['tipus_persona'] == 'juridica' and data['model'] == 'crm.polissa') or (data['model'] == 'giscedata.polissa')):
                                    signatura = fields['firmante'] or ''
                                    dni_signatura = fields['firmante_nif'] or ''
                                else:
                                    if fields['tipus_persona'] == 'fisica':
                                        signatura = fields['pagador'] or ''
                                        dni_signatura = fields['pagador_nif'] or ''
                            %>
                            Fdo. ${signatura}
                            <br>
                            DNI Firmante: ${dni_signatura}
                            <br>
                        </div>
                    </center>
                </div>
                <div class="signatura_annex" style="float: right !important;">
                    <center>
                        <div class="cos_signatura verd">
                            <img id="firma_direccion" src="${addons_path}/giscedata_polissa_comer_abenergia/report/firmaDireccion.png"/>
                        </div>
                        <div class="text_signatura">
                            Fdo. Pablo Bravo Vidal
                        </div>
                    </center>
                </div>
            %endif
        %endfor
    </body>
</html>
