# -*- coding: utf-8 -*-
from osv import osv
from tools.translate import _


class GiscedataPolissa(osv.osv):

    _name = "giscedata.polissa"
    _inherit = 'giscedata.polissa'

    def undo_last_modcontractual(self, cursor, uid, ids, context=None):
        if not isinstance(ids, list):
            ids = [ids]

        resgroups_obj = self.pool.get('res.groups')

        user_obj = self.pool.get('res.users')
        group_ids = user_obj.read(
            cursor, uid, uid, ['groups_id'], context=context
        )['groups_id']
        group_id = resgroups_obj.search(
            cursor, uid, [('name', '=', 'Product / Manager')]
        )[0]

        if group_id not in group_ids:
            raise osv.except_osv(
                _(u"Error"),
                _(u"No tiene permisos para realizar esta acción.")
            )

        return super(GiscedataPolissa, self).undo_last_modcontractual(
            cursor, uid, ids, context=context
        )


GiscedataPolissa()
