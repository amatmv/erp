# -*- encoding: utf-8 -*-

import collections

from osv import osv, fields


class WizardCopiarDatosCartera(osv.osv_memory):

    _name = 'wizard.copiar.datos.cartera'

    def action_obtener_datos_poliza(self, cursor, uid, ids, context=None):
        '''update data_alta in polissa
        and data_inici in modcontractual'''
        if not context:
            context = {}
        context.update({'sync': False,
                        'active_test': False})

        partner_obj = self.pool.get('res.partner')
        cups_obj = self.pool.get('giscedata.cups.ps')
        tar_acces_obj = self.pool.get('giscedata.polissa.tarifa')
        pricelist_obj = self.pool.get('product.pricelist')
        polissa_obj = self.pool.get('giscedata.polissa')
        modcon_obj = self.pool.get('giscedata.polissa.modcontractual')
        pobl_obj = self.pool.get('res.poblacio')
        muni_obj = self.pool.get('res.municipi')

        wizard = self.browse(cursor, uid, ids[0])
        polissa = wizard.polissa_id.id

        camps = ['titular',
                 'cups',
                 'tarifa_codi',
                 'consumo_anual_previsto',
                 'consumo_anual_calculado',
                 'llista_preu',
                 ]
        dades_polissa = polissa_obj.read(cursor, uid, polissa, camps, context)

        # obtenir les dades necessàries
        titular = dades_polissa['titular'][1]
        cups = dades_polissa['cups'][1]
        consumo_firmado = dades_polissa.get('consumo_anual_previsto', '0')
        if consumo_firmado:
            consumo_firmado = str(consumo_firmado)
        else:
            #Si el previsto es 0, se coge el consumo anual calculado
            consumo_calculado = dades_polissa.get('consumo_anual_calculado', '')
            if consumo_calculado:
                consumo_firmado = str(consumo_calculado)
            else:
                consumo_firmado = '0'
        tarifa_acces = dades_polissa['tarifa_codi']

        loc_cups = cups_obj.read(cursor, uid, dades_polissa['cups'][0],
                                 ['id_municipi','id_poblacio'])
        poblacio_id = loc_cups.get('id_poblacio', False)
        poblacio = ''
        if poblacio_id:
            poblacio = pobl_obj.read(cursor, uid, poblacio_id[0], ['name']
                                     )['name']
        llista_preu = dades_polissa['llista_preu'][1]
        modcon_activa = modcon_obj.search(cursor, uid,
                                          [('polissa_id', '=', polissa),
                                           ('active', '=', True)])
        if modcon_activa:
            fecha_vto = modcon_obj.read(cursor, uid,
                                        modcon_activa[0],
                                        ['data_final'])['data_final']
        else:
            fecha_vto = ''

        dades = collections.OrderedDict()
        dades['Cliente'] = titular
        dades['Cups'] = cups
        dades['Tarifa peaje'] = tarifa_acces
        dades['Consumo anual firmado'] = consumo_firmado
        dades['Distribución consumo'] = ''
        dades['Lista de precios'] = llista_preu
        dades['Actividad cliente'] = ''
        dades['Localidad PS'] = poblacio
        dades['Cliente actual'] = ''
        dades['Fecha vencimiento'] = fecha_vto
    
        headers = ';'.join(dades.keys())
        dades = ';'.join(dades.values())
        wizard.write({'output': "{0}\n{1}".format(headers, dades)
                    })
        return

    _columns = {
        'polissa_id': fields.many2one('giscedata.polissa', 'Contrato'),
        'output': fields.text('Datos'),
    }

WizardCopiarDatosCartera()
