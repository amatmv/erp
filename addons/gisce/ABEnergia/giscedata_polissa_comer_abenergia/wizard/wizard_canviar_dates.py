# -*- coding: utf-8 -*-
from osv import osv
from tools.translate import _


class WizardCanviarDates(osv.osv_memory):

    _name = "wizard.canviar.dates"
    _inherit = "wizard.canviar.dates"

    def create(self, cursor, uid, vals, context=None):
        resgroups_obj = self.pool.get('res.groups')
        user_obj = self.pool.get('res.users')
        group_ids = user_obj.read(
            cursor, uid, uid, ['groups_id'], context=context
        )['groups_id']
        group_id = resgroups_obj.search(
            cursor, uid, [('name', '=', 'Product / Manager')]
        )[0]
        if group_id not in group_ids:
            raise osv.except_osv(
                _(u"Error"),
                _(u"No tiene permisos para realizar esta acción.")
            )
        return super(WizardCanviarDates, self).create(
            cursor, uid, vals, context=context
        )


WizardCanviarDates()
