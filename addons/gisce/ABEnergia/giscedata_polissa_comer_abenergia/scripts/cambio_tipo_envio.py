# -*- coding: utf-8 -*-
import os
try:
    # Due: https://www.python.org/dev/peps/pep-0476
    import ssl
    try:
        _create_unverified_https_context = ssl._create_unverified_context
    except AttributeError:
        # Legacy Python that doesn't verify HTTPS certificates by default
        pass
    else:
        # Handle target environment that doesn't support HTTPS verification
        ssl._create_default_https_context = _create_unverified_https_context
except ImportError:
    pass

import click
import logging
from datetime import datetime, timedelta
from erppeek import Client
import tqdm


# row format
# [0] - polissa (name)

def activar_polissa(c, id_polissa):
    polissa_obj = c.model('giscedata.polissa')
    polissa_obj.send_signal(
        [id_polissa], 'modcontractual'
    )
    params = {'duracio': 'actual', 'accio': 'modificar'}
    wz_crear_mc_obj = 'giscedata.polissa.crear.contracte'
    ctx = {'active_id': id_polissa, 'lang': 'es_ES'}

    wz_id = c.model(wz_crear_mc_obj).create(params, ctx)
    wiz = c.model(wz_crear_mc_obj).read(wz_id.id, [])

    obs = wiz['observacions']
    obs = u'\nCambio envío a mail campaña sin papel\n' + obs
    c.model(wz_crear_mc_obj).write(
            [wz_id.id], {'observacions': obs}
    )
    c.model(wz_crear_mc_obj).action_crear_contracte(
        [wz_id.id], ctx)


def setup_log(instance, filename):
    log_file = filename
    logger = logging.getLogger(instance)
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr = logging.FileHandler(log_file)
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(logging.INFO)


def update_polissa(c, row):
    logger = logging.getLogger('cambio_tipo_envio')
    polissa = c.GiscedataPolissa.search([('name', '=', row[0])])
    if polissa:
        polissa_id = polissa[0]
        pol_dades = c.GiscedataPolissa.browse(polissa_id)
        polissa_obj = c.model('giscedata.polissa')
        try:
            polissa_obj.send_signal(
                [polissa_id], 'modcontractual'
            )
            pol_dades.write({
                'enviament': 'email',
            })
            activar_polissa(
                c, polissa_id
            )
            logger.info('{0} OK'.format(row[0]))
        except Exception as exc:
            polissa_obj.send_signal(
                [polissa_id], 'undo_modcontractual'
            )
            logger.error(
                'FAIL polissa: {} {} {}'.format(
                    row[0], row[1], exc
                )
            )
    else:
        logger.error(
            'NO FOUND polissa: {}'.format(
                *row
            ))


def cambio_tipo_envio(connection, csv_file, delimitador=';'):
    import csv
    f = open(csv_file, 'rb')
    reader = csv.reader(f, delimiter=delimitador)
    for line in tqdm.tqdm(reader):
        update_polissa(connection, line)
    f.close()

@click.command()
@click.option('-s', '--server', default='http://localhost',
              help=u'Adreça servidor ERP')
@click.option('-p', '--port', default=8069, help='Port servidor ERP',
              type=click.INT)
@click.option('-u', '--user', default='admin', help='Usuari servidor ERP')
@click.option('-w', '--password', default='admin',
              help='Contrasenya usuari ERP')
@click.option('-d', '--database', help='Nom de la base de dades')
@click.option('-f', '--file-input', type=click.Path(exists=True))
def work(**kwargs):

    server = '{}:{}'.format(kwargs['server'], kwargs['port'])
    c = Client(server=server, db=kwargs['database'], user=kwargs['user'],
               password=kwargs['password'])
    filename = os.path.basename(kwargs['file_input'])
    setup_log('cambio_tipo_envio', '/tmp/{}.log'.format(filename))
    logger = logging.getLogger('cambio_tipo_envio')
    logger.info('#######Start now {0}'.format(datetime.today()))

    cambio_tipo_envio(c, kwargs['file_input'])


if __name__ == '__main__':
    work()
