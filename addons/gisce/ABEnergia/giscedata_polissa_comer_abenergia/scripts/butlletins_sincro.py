# -*- coding: utf-8 -*-
import click
import logging
from datetime import datetime
from uuid import uuid1

from progressbar import ProgressBar, ETA, Percentage, Bar
from tqdm import tqdm
from erppeek import Client


REMOTE_CONNECTION = None
PARTNER_CATEGORY = None


def setup_log(instance, filename):
    log_file = filename
    logger = logging.getLogger(instance)
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr = logging.FileHandler(log_file)
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(logging.INFO)


def setup_remote_connection(connection):
    '''
    This function find from comerdist connection and setup a remote_connection
    :param c: Connection client
    '''
    global REMOTE_CONNECTION
    logger = logging.getLogger(__name__)
    comerdist_config = connection.model('giscedata.comerdist.config')
    comerdist_atr = comerdist_config.search([
        ('partner_id.ref', '=', '0116')
    ], limit=1)
    if comerdist_atr:
        # Hem d'obtenir l'XML
        config = comerdist_config.browse(comerdist_atr[0])
        REMOTE_CONNECTION = Client(
            "http://%s:%s" % (config.host, config.port),
            db=config.dbname, user=config.user,
            password=config.password
        )
        logger.info('Connection successfull to {0}'.format(
            config.dbname
        ))
        return True
    else:
        logger = logging.getLogger(__name__)
        logger.error('Error Connection: Not exist comerdist configuracion')
        return False


def get_category(category=None):
    logger = logging.getLogger(__name__)
    global PARTNER_CATEGORY
    if PARTNER_CATEGORY:
        return PARTNER_CATEGORY
    else:
        categ_obj = REMOTE_CONNECTION.model('res.partner.category')
        categ_id = categ_obj.search([('name', '=', category)])
        if categ_id:
            PARTNER_CATEGORY = categ_id[0]
            categ_id = categ_id[0]
            logger.info('Partner Category {} found with id: {}'.format(
                category, categ_id
            ))
        else:
            categ_id = categ_obj.create({'name': category}).id
            PARTNER_CATEGORY = categ_id
            logger.info('Create Partner Category {} with id: {}'.format(
                category, categ_id
            ))
        return categ_id


def get_partner(connection, partner_id):
    logger = logging.getLogger(__name__)
    partner_obj = connection.model('res.partner')
    parnter = partner_obj.read(partner_id, ['name', 'vat'])
    re_parnter_obj = REMOTE_CONNECTION.model('res.partner')
    partner = re_parnter_obj.search([('name', '=', parnter['name'])])
    if partner:
        partner_id = partner[0]
    else:
        partner_id = re_parnter_obj.create({'name': parnter['name'],
                                            'vat': parnter['vat']}).id
        logger.info(
            'Create Partner {name} with vat {vat} with new id: {p_id}'.format(
                name=parnter['name'], vat=parnter['vat'], p_id=partner_id
            )
        )
        category_id = get_category()
        partner = re_parnter_obj.browse(partner_id)
        partner.write({'category_id': [category_id]})
    return partner_id


def get_activitat_us(activitat_us):
    logger = logging.getLogger(__name__)
    re_activitat_us_obj = REMOTE_CONNECTION.model(
        'giscedata.butlleti.activitat')
    act_us = re_activitat_us_obj.search([('name', '=', activitat_us)])
    if act_us:
        return act_us[0]
    else:
        act_us = re_activitat_us_obj.create({'name': activitat_us})
        logger.info('Create new activitat us: {name}'.format(
            name=activitat_us[1])
        )
        return act_us.id


def sync_attachment(connection, attach_id, res_id):
    logger = logging.getLogger(__name__)
    attach_obj = connection.model('ir.attachment')
    re_attach_obj = REMOTE_CONNECTION.model('ir.attachment')
    fields_to_read = [
        'name', 'link', 'res_id', 'res_model',
        'datas', 'datas_fname', 'description'
    ]
    data_attach = attach_obj.read(attach_id, fields_to_read)
    data_attach.pop('id', None)
    data_attach['res_id'] = res_id
    re_attach_id = re_attach_obj.create(data_attach)
    logger.info('Create new attachment: {0}'.format(
        re_attach_id.id)
    )
    return re_attach_id.id


def update_attachment(connection, butlleti_id, re_butlleti_id):
    attach_obj = connection.model('ir.attachment')
    re_attach_obj = REMOTE_CONNECTION.model('ir.attachment')
    search_param = [
        ('res_id', '=', butlleti_id), ('res_model', '=', 'giscedata.butlleti')
    ]
    attachments_ids = attach_obj.search(search_param)
    if not attachments_ids:
        return False
    for attach in attach_obj.read(attachments_ids, ['name']):
        search_param = [
            ('name', '=', attach['name']), ('res_model', '=', 'giscedata.butlleti')
        ]
        re_attach = re_attach_obj.search(search_param)
        if not re_attach:
            re_attach_id = sync_attachment(
                connection, attach['id'], re_butlleti_id
            )
    return True


def create_attachments(connection, butlleti_id, re_butlleti_id):
    attach_obj = connection.model('ir.attachment')
    search_param = [
        ('res_id', '=', butlleti_id), ('res_model', '=', 'giscedata.butlleti')
    ]
    attachments_ids = attach_obj.search(search_param)
    for attach_id in attachments_ids:
        re_attach_id = sync_attachment(
            connection, attach_id, re_butlleti_id
        )


def create_remote_butlleti(connection, butlleti, remote_cups_id):
    logger = logging.getLogger(__name__)
    logger.info('Create remote butlleti: {name}'.format(
        name=butlleti['name'])
    )
    re_butlleti_obj = REMOTE_CONNECTION.model('giscedata.butlleti')
    # remove id
    butlleti_id = butlleti.pop('id', None)
    # get remote info for partner_id, installador_id, cups_id, activitat_us
    partner_id = butlleti.pop('partner_id', None)
    butlleti.pop('partner_address_id', None)
    installador_id = butlleti.pop('installador_id', None)
    activitat_us = butlleti.pop('activitat_us', None)
    butlleti['cups_id'] = remote_cups_id
    # look for partners

    if partner_id:
        try:
            butlleti['partner_id'] = get_partner(connection, partner_id[0])
        except Exception as e:
            # quan el dni sigui incorrecte no el faci pero no es pari
            logger.error('Could not sync butlleti id {0}'.format(butlleti_id))
            return True
    if installador_id:
        try:
            butlleti['installador_id'] = get_partner(connection, installador_id[0])
        except Exception as e:
            # quan el dni sigui incorrecte no el faci pero no es pari
            logger.error('Could not sync butlleti id {0}'.format(butlleti_id))
            return True
    # activitat us
    if activitat_us:
        butlleti['activitat_us'] = get_activitat_us(activitat_us[1])

    re_butlleti = re_butlleti_obj.create(butlleti)
    create_attachments(connection, butlleti_id, re_butlleti.id)

    return True


def sync_butlleti(connection, butlleti_id):
    logger = logging.getLogger(__name__)
    butlleti_obj = connection.model('giscedata.butlleti')
    re_butlleti_obj = REMOTE_CONNECTION.model('giscedata.butlleti')
    re_cups_obj = REMOTE_CONNECTION.model('giscedata.cups.ps')
    butlleti = butlleti_obj.read(butlleti_id, [])
    cups_name = butlleti['cups_id'][1]
    search_params = [('name', '=like', '{0}%'.format(cups_name[:20]))]
    re_cups = re_cups_obj.search(search_params)
    if not re_cups:
        logger.error('CUPS {name} not found'.format(name=cups_name))
        return False
    else:
        if len(re_cups) > 1:
            logger.error('CUPS {name} with more than one register'.format(
                name=cups_name))
            return False
        else:
            search_params = [('name', '=', butlleti['name']),
                             ('cups_id', '=', re_cups[0])]
            re_butlleti = re_butlleti_obj.search(search_params)
            if re_butlleti:
                logger.info(
                    'Butlleti {name} found'.format(name=butlleti['name'])
                )
                update_attachment(connection, butlleti['id'], re_butlleti[0])
            else:
                logger.info('>>>Butlleti {name} with CUPS {cups} not found'.format(
                    name=butlleti['name'], cups=cups_name)
                )
                create_remote_butlleti(connection, butlleti, re_cups[0])
            return True
    return False


@click.command()
@click.option('-s', '--server', default='http://localhost',
              help=u'Adreça servidor ERP')
@click.option('-p', '--port', default=8069, help='Port servidor ERP',
              type=click.INT)
@click.option('-u', '--user', default='admin', help='Usuari servidor ERP')
@click.option('-w', '--password', default='admin',
              help='Contrasenya usuari ERP')
@click.option('-d', '--database', help='Nom de la base de dades')
@click.option('-r', '--reference', envvar='REFERENCE',
              help='R1 distribuidora')
@click.option('-c', '--category', default='Instaladores',
              help='Categoria de los instaladores')
@click.option('-f', '--file-log', envvar='LOG_FILE',
              default="/tmp/butlletins_sincro.log")
def butlletins_sincro(**kwargs):
    server = '{}:{}'.format(kwargs['server'], kwargs['port'])
    connection = Client(server=server, db=kwargs['database'], user=kwargs['user'],
               password=kwargs['password'])
    setup_log(__name__, kwargs['file_log'])
    logger = logging.getLogger(__name__)
    sync_id = uuid1()
    logger.info('#######Sync id: {0} Start at {1}'.format(
            sync_id, datetime.today()
        )
    )
    setup_remote_connection(connection)
    get_category(kwargs['category'])
    butlleti_obj = connection.model('giscedata.butlleti')
    butlletins = butlleti_obj.search(
        [('cups_id.distribuidora_id.ref', '=', kwargs['reference'])]
    )
    # define pbar
    logger.info('Total sync butlletins {0}'.format(len(butlletins)))
    for butlleti in tqdm(butlletins):
        sync_butlleti(connection, butlleti)

    logger.info('#######Sync id: {0} finish at {1}'.format(
            sync_id, datetime.today()
        )
    )

if __name__ == '__main__':
    butlletins_sincro(auto_envvar_prefix='OPENERP')