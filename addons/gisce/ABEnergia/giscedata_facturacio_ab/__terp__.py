# -*- coding: utf-8 -*-
{
    "name": "Custom AB facturacio",
    "description": """Customitzacions AB facturacio""",
    "version": "0-dev",
    "author": "ABEnergia",
    "category": "",
    "depends":[
        "giscedata_facturacio",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_facturacio_data.xml",
        "security/ir.model.access.csv",
    ],
    "active": False,
    "installable": True
}
