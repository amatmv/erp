# -*- coding: utf-8 -*-
from osv import osv

class GiscedataFacturacioFacturador(osv.osv):
    _name = 'giscedata.facturacio.facturador'
    _inherit = 'giscedata.facturacio.facturador'

    def config_facturador(
            self, cursor, uid, facturador, polissa_id, context=None):
        """Reactiva fact from 50% in conf variable instead of 33%
        for 2.X
        :param facturador: Tariff Class
        :return: True
        """
        super(GiscedataFacturacioFacturador, self).config_facturador(
            cursor, uid, facturador, polissa_id, context=context
        )
        cfg_obj = self.pool.get('res.config')
        fact_react = float(cfg_obj.get(cursor, uid, 'fact_react_2X', '0'))
        if fact_react:
            if facturador.code.startswith('2.'):
                facturador.conf['fact_reactiva'] = fact_react

        return True

GiscedataFacturacioFacturador()
