# -*- coding: utf-8 -*-
import logging
from erppeek import Client
import click
import pandas as pd
from datetime import datetime
from tqdm import tqdm
logging_name = 'actualizacion_tarifas_log'


def setup_log(instance, filename):
    log_file = filename
    logger = logging.getLogger(instance)
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr = logging.FileHandler(log_file)
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(logging.INFO)


def do_modcon(client, pol_id, fecha_inicio):
    wz_crear_mc_obj = client.model('giscedata.polissa.crear.contracte')
    pol_obj = client.model('giscedata.polissa')

    ctx = {'active_id': pol_id, 'active_ids': [pol_id]}

    params = {
        'data_inici': fecha_inicio,
        'duracio': 'actual',
        'accio': 'nou'
    }
    wiz_mod_id = wz_crear_mc_obj.create(params, context=ctx).id
    wizard = wz_crear_mc_obj.browse(wiz_mod_id, context=ctx)

    res = wz_crear_mc_obj.onchange_duracio(
        wiz_mod_id, wizard.data_inici,
        wizard.duracio, context=ctx
    )

    fecha_final = res['value'].get('data_final', False)

    if res['warning'] and not fecha_final:
        return False, res['warning']

    wizard.write({'data_inici': fecha_inicio, 'data_final': fecha_final})
    wizard.action_crear_contracte(context=ctx)

    return True, False


def canvi_tarifa_contractes(client, list_polizas_dict, fecha, logger):
    # tarifa_20A_new -> tarifa_20DHA_new (2.0A -> 2.0 DHA)
    # tarifa_20A -> tarifa_20DHA (2.1A -> 2.1DHA)
    imd_obj = client.model('ir.model.data')
    pol_obj = client.model('giscedata.polissa')
    a_20 = imd_obj.get_object_reference('giscedata_polissa', 'tarifa_20A_new')[1]
    dha_20 = imd_obj.get_object_reference('giscedata_polissa', 'tarifa_20DHA_new')[1]
    a_21 = imd_obj.get_object_reference('giscedata_polissa', 'tarifa_20A')[1]
    dha_21 = imd_obj.get_object_reference('giscedata_polissa', 'tarifa_20DHA')[1]

    pol_corrects = []
    pol_incorrect_state = []
    pol_incorrect_tarifa_inicial = []
    pol_incorrect_do_modcon = []

    for pol in tqdm(list_polizas_dict):
        tarifa_actual = pol['tarifa'][0]
        pol_id = pol['id']
        pol_state = pol['state']
        pol_name = pol['name']
        tarifa_desti = False

        if pol_state != 'activa':
            pol_incorrect_state.append(pol_name)
            continue

        if tarifa_actual == a_20:
            tarifa_desti = dha_20
        elif tarifa_actual == a_21:
            tarifa_desti = dha_21
        else:
            pol_incorrect_tarifa_inicial.append(pol_name)
            continue

        if tarifa_desti:
            pol_obj.send_signal([pol_id], ['modcontractual'])
            pol_obj.write(pol_id, {'tarifa': tarifa_desti})

            try:
                res_mod = do_modcon(client, pol_id, fecha)
            except Exception as e:
                res_mod = False, True

            if not res_mod[0]:
                pol_incorrect_do_modcon.append(pol)
            else:
                pol_corrects.append(pol_name)

    if pol_incorrect_do_modcon:
        p_err_ids = [p['id'] for p in pol_incorrect_do_modcon]
        impossible_undo = []
        for pe_i in p_err_ids:
            try:
                pol_obj.send_signal(
                    [pe_i], ['undo_modcontractual']
                )
            except Exception as e:
                pp_name = pol_obj.read(pe_i, ['name'])['name']
                impossible_undo.append(pp_name)

        logger.info('\nImpossible realizar la modificacion para:\n')
        logger.info('\n'.join([p['name'] for p in pol_incorrect_do_modcon]))

        if impossible_undo:
            logger.info('\nImpossible deshacer modificacion para:\n')
            logger.info('\n'.join(impossible_undo))

    if pol_incorrect_tarifa_inicial:
        logger.info('\nTarifa inicial no corresponde a 2.0A o 2.1A:\n')
        logger.info('\n'.join(pol_incorrect_tarifa_inicial))

    if pol_incorrect_state:
        logger.info('\nLas polizas siguientes no estan activas:\n')
        logger.info('\n'.join(pol_incorrect_state))

    if pol_corrects:
        logger.info('\nLas polizas siguientes se han actualizado correctamente:\n')
        logger.info('\n'.join(pol_corrects))


def read_and_parse_csv(client, file_path, logger):
    polizas = []
    polizas_no_encontradas = False
    if file_path:
        df = pd.read_csv(file_path, sep=';')
        # Index should named as 'contrato'
        # Avoid repeated
        contratos_list = [str(con) for con in df.contrato.unique()]

        pol_obj = client.model('giscedata.polissa')

        pol_ids = pol_obj.search([('name', 'in', contratos_list)])

        polizas = pol_obj.read(pol_ids, ['name', 'tarifa', 'state'])

        if len(pol_ids) != len(contratos_list):
            polizas_encontradas = [p['name'] for p in polizas]
            polizas_no_encontradas = list(
                set(contratos_list) - set(polizas_encontradas)
            )

        if polizas_no_encontradas:
            logger.info('\nPolizas no encontradas:')
            logger.info('\n'.join(polizas_no_encontradas))

    return polizas


@click.command()
@click.option('-s', '--server', default='http://localhost',
              help='Servidor ERP')
@click.option('-p', '--port', default=8069, help='Puerto servidor ERP',
              type=click.INT)
@click.option('-u', '--user', default='admin', help='Usuario servidor ERP')
@click.option('-w', '--password', default='admin',
              help='Contraseña usuario ERP')
@click.option('-d', '--database', help='Nombre de la base de datos')
@click.option('-f', '--fecha', default=None, help='Fecha de inicio de la modcon')
@click.option('-c', '--csv', default=None, help='Fichero CSV')
def update_contracts(**kwargs):
    """EJEMPLO DE FICHERO (¡¡¡¡¡Respetar header contrato!!!!):

        contrato
        12345678
        47855446
        45884889
        41855255

    """
    server = '{}:{}'.format(kwargs['server'], kwargs['port'])
    c = Client(
        server=server,
        db=kwargs['database'],
        user=kwargs['user'],
        password=kwargs['password']
    )
    if not c:
        click.echo(u'Impossible to connect to ERP')
        return False

    setup_log(logging_name,
              '/tmp/actualizacion_tarifas.log')
    logger = logging.getLogger(logging_name)

    logger.info('#######Start now {0}'.format(datetime.today()))

    file_path = kwargs['csv']
    fecha_modcon = kwargs['fecha']  # "%Y-%m-%d"

    to_update_polizas = read_and_parse_csv(c, file_path, logger)
    canvi_tarifa_contractes(c, to_update_polizas, fecha_modcon, logger)

if __name__ == '__main__':
    update_contracts()
