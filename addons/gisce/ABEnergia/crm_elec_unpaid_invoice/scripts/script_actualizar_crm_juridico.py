# -*- coding: utf-8 -*-
import click

from erppeek import Client

'''Script Comer que fa el següent:

- Crea els casos de CRM Jurídico i tanca els de CRM Comercial
  per les pòlisses que compleixen les següents condicions:

    * Estan donades de baixa
    * Tenen deuda_viva (pending_amount) > 0
    * No estan actives
    * Tenen l'estat pendent de 'Corte planificado'

- Posa els casos de CRM Juridico en estat pendent que tenen
  deuda_viva (pending_amount) = 0
'''


@click.command()
@click.option('-uc', '--user_comer', default='admin', help='Usuari servidor ERP COMER')
@click.option('-wc', '--password_comer', help='Contrasenya usuari ERP COMER')
@click.option('-sc', '--server_comer', default='localhost', help=u'IP servidor ERP COMER')
@click.option('-pc', '--port_comer', default=18069, help='Port servidor ERP COMER', type=click.INT)
@click.option('-dc', '--database_comer', default='abenergia', help='Nom de la base de dades de COMER')
def actualizar_crm_juridico(**kwargs):

    server = 'http://{0}:{1}'.format(kwargs['server_comer'],
                                     kwargs['port_comer'])
    client = Client(server=server,
                    db=kwargs['database_comer'],
                    user=kwargs['user_comer'],
                    password=kwargs['password_comer'])

    actualizar_crm_juridico_obj = client.model('actualizar.crm.juridico')
    actualizar_crm_juridico_obj.actualizar_casos_crm_juridico()

    return True

if __name__ == '__main__':
    actualizar_crm_juridico()
