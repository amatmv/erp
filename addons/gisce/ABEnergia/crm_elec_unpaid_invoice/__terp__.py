# -*- coding: utf-8 -*-
{
    "name": "CRM Custom for unpaid invoces",
    "description": """Customitzations for CRM""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "CRM",
    "depends":[
        "crm",
        "crm_case_stage",
        "giscedata_polissa_crm",
        "giscedata_facturacio_impagat",
        "poweremail"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/crm_elec_unpaid_invoice_security.xml",
        "security/ir.model.access.csv",
        "crm_elec_unpaid_data.xml",
        "crm_elec_unpaid_view.xml",
        "crm_juridico_view.xml"
    ],
    "active": False,
    "installable": True
}
