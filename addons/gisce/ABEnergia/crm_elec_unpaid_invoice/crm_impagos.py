# -*- coding: utf-8 -*-
from osv import osv, fields


class CRMImpagos(osv.OsvInherits):
    _name = 'crm.impagos'
    _inherits = {'crm.case': 'case_id'}

    _columns = {
        'case_id': fields.many2one('crm.case', 'Case', required=True,
                                   ondelete='cascade'),
        'factura': fields.char(u'Nº factura', size=64),
        'fecha_factura': fields.date('Fecha factura'),
        'fecha_impago': fields.date('Fecha impago'),
        'recibo': fields.char('Recibo',size=64),
        'nominal': fields.float('Nominal'),
        'gastos': fields.float('Gastos'),
        'total': fields.float('Total'),
        'parcial': fields.float('Parcial'),
        'tarifa': fields.char('Tarifa', size=20),
        'potencia': fields.float('Potencia'),
        'phone': fields.char("Phone", size=64),
    }

CRMImpagos()
