# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _
# Get from addons.crm.crm
def _links_get(self, cr, uid, context={}):
    obj = self.pool.get('res.request.link')
    ids = obj.search(cr, uid, [])
    res = obj.read(cr, uid, ids, ['object', 'name'], context)
    return [(r['object'], r['name']) for r in res]


class crm_case(osv.osv):
    _name = 'crm.case'
    _inherit = 'crm.case'

    _columns = {
        'ref3': fields.reference('Reference 3',
                                 selection=_links_get,
                                 size=128),
    }

crm_case()