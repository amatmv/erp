# -*- coding: utf-8 -*-
from osv import osv, fields


class CRMJuridico(osv.OsvInherits):
    _name = 'crm.juridico'
    _inherits = {'crm.case': 'case_id'}

    def _ff_deuda_inicial(
            self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        res = dict.fromkeys(ids, False)
        polizas_obj = self.pool.get('giscedata.polissa')

        for case in self.read(cursor, uid, ids, ['polissa_id']):
            poliza_id = case['polissa_id'][0]
            poliza = polizas_obj.read(
                cursor, uid, poliza_id, ['pending_amount'])
            res[case['id']] = poliza['pending_amount']
        return res

    def _trg_recalcular_deuda(self, cursor, uid, ids, context=None):
        return ids

    def create(self, cursor, uid, values, context=None):
        if context is None:
            context = {}
        res = super(CRMJuridico, self).create(
            cursor, uid, values, context=context)
        res_deuda_inicial = self._ff_deuda_inicial(
            cursor, uid, [res], None, None, context=context)
        deuda_inicial = res_deuda_inicial[res]
        values.update({'deuda_inicial': deuda_inicial})
        super(CRMJuridico, self).write(
            cursor, uid, res, values, context=context)
        return res

    def _get_default_stage(self, cursor, uid, context=None):
        if not context:
            context = {}
        imd_obj = self.pool.get('ir.model.data')
        stage_id = imd_obj.get_object_reference(
            cursor, uid, 'crm_elec_unpaid_invoice', 'crm_inicial_stage'
        )[1]
        return stage_id

    def _get_default_section(self, cursor, uid, context=None):
        if not context:
            context = {}
        imd_obj = self.pool.get('ir.model.data')
        section_id = imd_obj.get_object_reference(
            cursor, uid, 'crm_elec_unpaid_invoice', 'unpaid_juri_section'
        )[1]
        return section_id

    def onchange_polissa_id(self, cursor, uid, ids, polissa_id):
        res = {
            'value': {
                'cups_id': False, 'cups_direccion': False,
                'partner_id': False, 'partner_address_id': False,
                'deuda_inicial': False, 'deuda_viva': False
            }
        }
        if polissa_id:
            polizas_obj = self.pool.get('giscedata.polissa')
            fields_to_read = [
                'cups', 'titular', 'pagador', 'altre_p', 'pending_amount'
            ]
            poliza = polizas_obj.read(
                cursor, uid, polissa_id, fields_to_read)

            cups_id = poliza['cups'][0]
            deuda = poliza['pending_amount']

            cups_obj = self.pool.get('giscedata.cups.ps')
            cups_direccion = cups_obj.read(
                cursor, uid, cups_id, ['direccio'])['direccio']

            res['value'].update({
                'cups_id': cups_id,
                'cups_direccion': cups_direccion,
                'deuda_inicial': deuda,
                'deuda_viva': deuda
            })
        return res

    def onchange_partner_address_id(self, cursor, uid, ids, partner_address_id):
        res = {'value': {}}
        if partner_address_id:
            partner_address_obj = self.pool.get('res.partner.address')
            fields_to_read = ['name', 'email', 'phone', 'mobile']
            partner_address = partner_address_obj.read(
                cursor, uid, partner_address_id, fields_to_read)
            res['value'].update({
                'persona_contacto': partner_address['name'],
                'telefono': partner_address['phone'],
                'movil': partner_address['mobile'],
                'email': partner_address['email']
            })

        return res

    def onchange_partner_id(self, cursor, uid, ids, partner_id, email_from):
        case_obj = self.pool.get('crm.case')
        res = case_obj.onchange_partner_id(
            cursor, uid, [], partner_id, email_from)
        res_address = self.onchange_partner_address_id(
            cursor, uid, ids, res['value']['partner_address_id'])
        res['value'].update(res_address['value'])
        return res

    _columns = {
        'case_id': fields.many2one('crm.case', 'Caso', required=True,
                                   ondelete='cascade'),
        'cups_id': fields.many2one(
            'giscedata.cups.ps', 'CUPS', readonly=True,
            states={
                'draft': [('readonly', False)],
            }),
        'cups_direccion': fields.related(
            'cups_id', 'direccio', type='char', relation='giscedata.cups.ps',
            string="Dirección CUPS", store=False, readonly=True, size=256
        ),
        'persona_contacto': fields.char('Persona de contacto', size=256),
        'telefono': fields.char('Teléfono', size=64),
        'movil': fields.char('Móvil', size=64),
        'email': fields.char('E-Mail', size=240),
        'deuda_inicial': fields.function(
            _ff_deuda_inicial, method=True, type='float', digits=(10, 2),
            string='Deuda Inicial', store={
                'crm.juridico': (
                    _trg_recalcular_deuda, ['polissa_id'], 10)
            }),
        'deuda_viva': fields.related(
            'polissa_id', 'pending_amount', type='float',
            string='Deuda viva', readonly=True)
    }

    _defaults = {
        'stage_id': _get_default_stage,
        'section_id': _get_default_section
    }

CRMJuridico()
