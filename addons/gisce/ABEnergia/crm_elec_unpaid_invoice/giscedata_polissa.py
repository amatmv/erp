# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscedataPolissa(osv.osv):

    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    _columns = {
        'email_notification': fields.boolean('Notificación por email'),
        'sms_notification': fields.boolean('Notificación por SMS')
    }

    _defaults = {
        'email_notification': lambda * a: 1,
        'sms_notification': lambda * a: 0
    }

GiscedataPolissa()