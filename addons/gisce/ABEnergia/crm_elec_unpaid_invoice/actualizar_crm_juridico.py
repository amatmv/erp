# -*- coding: utf-8 -*-
from datetime import date, datetime

from dateutil.relativedelta import relativedelta

from osv import osv


class ActualizarCRMJuridico(osv.osv_memory):

    _name = 'actualizar.crm.juridico'

    def cerrar_casos_crm_comercial(self, cursor, uid, polizas_ids,
                                   context=None):
        if context is None:
            context = {}

        if not isinstance(polizas_ids, (list, tuple)):
            polizas_ids = [polizas_ids]

        if polizas_ids:
            imd_obj = self.pool.get('ir.model.data')
            crm_impagos_comercial_section_id = imd_obj.get_object_reference(
                cursor, uid, 'crm_elec_unpaid_invoice', 'unpaid_comer_section'
            )[1]

            ref_polizas = ['giscedata.polissa,{0}'.format(pol_id)
                           for pol_id in polizas_ids]

            crm_case_obj = self.pool.get('crm.case')
            crm_case_to_close_ids = crm_case_obj.search(cursor, uid, [
                ('state', '!=', 'done'),
                ('section_id', '=', crm_impagos_comercial_section_id),
                '|',
                ('ref', 'in', ref_polizas),
                ('ref2', 'in', ref_polizas),
            ])
            if crm_case_to_close_ids:
                crm_case_obj.case_close(cursor, uid, crm_case_to_close_ids)

    def crear_casos_crm_juridico(self, cursor, uid, context=None):
        if context is None:
            context = {}

        conf_obj = self.pool.get('res.config')
        dias_baja = int(conf_obj.get(
            cursor, uid, 'crm_juridico_dias_tras_baja', 10))

        polizas_obj = self.pool.get('giscedata.polissa')
        polizas_search_params = [
            ('state', '=', 'baixa'),
            ('data_baixa', '!=', False),
            ('active', '=', False),
            ('debt_amount', '>', 0)
        ]
        polizas_ids = polizas_obj.search(
            cursor, uid, polizas_search_params, context={'active_test': False}
        )
        crm_juridico_obj = self.pool.get('crm.juridico')
        data_actual = date.today()
        data_actual_str = data_actual.strftime('%d/%m/%Y')
        data_minima = (
            data_actual - relativedelta(days=dias_baja)
        ).strftime('%Y-%m-%d')
        polizas_to_close_crm_comercial_ids = []
        bono_process_id = polizas_obj.get_bono_social_process(cursor, uid)
        for pol_id in polizas_ids:
            crm_juridico_ids = crm_juridico_obj.search(
                cursor, uid, [('polissa_id', '=', pol_id)])
            if not crm_juridico_ids:
                poliza_datos = polizas_obj.read(cursor, uid, pol_id, [
                    'data_baixa', 'titular', 'process_id'
                ])
                data_baixa = poliza_datos['data_baixa']
                if poliza_datos['process_id'][0] == bono_process_id:
                    bono = 'BONO '
                else:
                    bono = ''

                if data_minima >= data_baixa:
                    vals = {
                        'polissa_id': pol_id,
                        'name': "{}Caso creado el {}".format(
                            bono,
                            data_actual_str
                        )
                    }
                    polissa_vals = crm_juridico_obj.onchange_polissa_id(
                        cursor, uid, [], pol_id)
                    partner_id = poliza_datos['titular'][0]
                    partner_vals = crm_juridico_obj.onchange_partner_id(
                        cursor, uid, [], partner_id, email_from=False
                    )
                    vals.update(polissa_vals['value'])
                    vals.update(partner_vals['value'])
                    vals.update({'partner_id': partner_id})
                    crm_juridico_obj.create(cursor, uid, vals, context=context)
                    polizas_to_close_crm_comercial_ids.append(pol_id)

        self.cerrar_casos_crm_comercial(
            cursor, uid, polizas_to_close_crm_comercial_ids, context=context)

    def cerrar_casos_crm_juridico(self, cursor, uid, context=None):
        if context is None:
            context = {}

        crm_juridico_obj = self.pool.get('crm.juridico')
        crm_juridico_ids = crm_juridico_obj.search(
            cursor, uid, [('state', 'in', ['open', 'draft'])])
        crm_juridico_cases = crm_juridico_obj.read(
            cursor, uid, crm_juridico_ids, ['case_id', 'deuda_viva'])
        crm_to_close_ids = [
            crm_case['case_id'][0]
            for crm_case in crm_juridico_cases
            if crm_case['deuda_viva'] == 0
        ]

        crm_case_obj = self.pool.get('crm.case')
        if crm_to_close_ids:
            crm_case_obj.case_pending(cursor, uid, crm_to_close_ids)

    def actualizar_casos_crm_juridico(self, cursor, uid, context=None):
        if context is None:
            context = {}

        self.cerrar_casos_crm_juridico(cursor, uid, context=context)
        self.crear_casos_crm_juridico(cursor, uid, context=context)

ActualizarCRMJuridico()
