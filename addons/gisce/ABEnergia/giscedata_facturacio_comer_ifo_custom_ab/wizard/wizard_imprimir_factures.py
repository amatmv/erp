# -*- coding: utf-8 -*-
"""
================================================================================
Custom Wizard per imprimir les factures ordenades i opció per imprimir factures
agrupades per zones
================================================================================

"""
from osv import fields, osv
from addons import get_module_resource
try:
    from ast import literal_eval
except ImportError:
    literal_eval = eval

from itertools import chain



class WizardImprimirFactures(osv.osv_memory):
    """Wizard per imprimir les factures ordenades
    """
    _name = 'imprimir.factures'
    _inherit = 'imprimir.factures'

    def get_invoices(self, cursor, uid, ids, context=None):
        """Cerca les factures de comercial segons lot de facturació i període
        i en guarda els IDs ordenats segons la zona i l'ordre dels CUPS
        corresponents de distribuidora a context['factures_imprimir']

        + Si agrupar_zona esta a True agrupa las facturas en ficheros por zona
        ejecutando la query almacenada en sql/sscc_facturas_zona.sql
        """

        if context is None:
            context = {}

        res = super(WizardImprimirFactures, self).get_invoices(
            cursor, uid, ids, context=context
        )

        wiz = self.browse(cursor, uid, ids, context)[0]

        if wiz.agrupar_zona:
            # factures are a dict key: zona value: list of facturas ids
            factures = literal_eval(wiz.factures)
            # List with all factures ids
            fact_ids = tuple(chain.from_iterable(factures.values()))

            sql_name = 'sscc_facturas_zona.sql'
            sql_path = get_module_resource(
                'giscedata_facturacio_comer_ifo_custom_ab', 'sql', sql_name
            )
            query = open(sql_path).read()

            params = {
                'fact_ids': fact_ids,
            }
            if query:
                cursor.execute(query, params)
                q_res = cursor.dictfetchall()

                factures_ordenades = {}
                total_facturas = 0
                summary = []
                for group in q_res:
                    name = group['zona_correos']
                    l_factures = group['facturas']
                    factures_ordenades.update({name: l_factures})
                    n_fact = len(l_factures)
                    total_facturas += n_fact
                    summary.append(
                        '{0}: {1} facturas'.format(name, n_fact)
                    )
                summary = '\n'.join(summary)
                summary = 'Se imprimiran:\n{0}'.format(summary)
                wiz.write({'state': 'resum', 'n_factures': total_facturas,
                           'factures': str(factures_ordenades),
                           'info_zonas_correos': summary
                           })
                return True

        return res

    _columns = {
        'agrupar_zona': fields.boolean('Dividir en zonas de correos'),
        'info_zonas_correos': fields.text('Información de impresión',
                                          readonly=True
                                          )
    }

    _defaults = {
        'agrupar_zona': lambda *a: False,
        'info_zonas_correos': lambda *a: '',
    }
WizardImprimirFactures()
