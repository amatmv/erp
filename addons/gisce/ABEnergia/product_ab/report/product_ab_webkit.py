# -*- coding: utf-8 -*-

from c2c_webkit_report import webkit_report
from report import report_sxw
from tools import config

class report_webkit_html(report_sxw.rml_parse):
    def __init__(self, cursor, uid, name, context):
        super(report_webkit_html, self).__init__(cursor, uid, name,
                                                 context=context)
        self.localcontext.update({
            'cursor': cursor,
            'uid': uid,
            'addons_path': config['addons_path'],
        })

    def set_context(self, objects, data, ids, report_type=None):
        prod_obj = self.pool.get('product.product')
        objects = prod_obj.browse(self.cr, self.uid, ids)
        super(report_webkit_html, self).set_context(
            objects, data, ids, report_type
        )

webkit_report.WebKitParser(
    'report.product.etiqueta',
    'product.product',
    'addons/product_ab/report/etiqueta.mako',
    parser=report_webkit_html
)
