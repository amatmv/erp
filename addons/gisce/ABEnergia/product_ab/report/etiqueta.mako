<%
    from datetime import datetime
    import barcode
    import random
    import string
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
    	<style type="text/css">
        ${css}
            body{
                font-family: helvetica;
                font-size: 12px;
                margin-right: 50px;
                margin-left: 50px;
                background-size: 100%;
                background-repeat:no-repeat;
                margin:80px;
            }
            table{
                font-size: 10px;
                font-weight: bold;
                width: auto;
                border-collapse: collapse;
            }
        </style>
    </head>
    <body>
       <%
       def randomword(length):
           return ''.join(random.choice(string.lowercase) for i in range(length))
       %>
        %for prod in objects:
            <div id="contents">
                <div id="etiqueta">
                    <div id="logo">

                    </div>
                    <table border=1>
                        <%
                            CODE128 = barcode.get_barcode_class('code128')
                            codigo = CODE128(prod.code)
                            fitxer = randomword(8)
                            ruta = codigo.save('/tmp/{}'.format(fitxer))
                        %>
                        <tr>
                            <td class="title">${prod.name}</td>
                        </tr>
                        <tr>
                            <td><img alt="barres" src="${ruta}"/></td>
                        </tr>
                        </br>
                    </table>
                </div>
            </div>
        %endfor
    </body>
</html>
