# -*- coding: utf-8 -*-
{
    "name": "Product Abenergia",
    "description": """Afegeix funcionalitats custom per als productes""",
    "version": "0-dev",
    "author": "Abenergia",
    "category": "",
    "depends":[
        "product",
        "c2c_webkit_report"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "product_ab_report.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
