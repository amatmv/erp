# -*- coding: utf-8 -*-
{
    "name": "Polissa (Comer) ABEnergia",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Adaptacions per ABEnergia del giscedata_polissa_comer
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "giscedata_polissa_comer",
        "giscedata_polissa_category"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_polissa_view.xml"
    ],
    "active": False,
    "installable": True
}
