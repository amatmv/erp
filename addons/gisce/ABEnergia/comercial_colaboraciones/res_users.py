# -*- coding: utf-8 -*-

from osv import osv, fields

class res_users(osv.osv):
    _name = 'res.users'
    _inherit = 'res.users'

    def _ff_usuarios_colab(self, cr, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        if not isinstance(ids, list):
            ids = [ids, ]
        res = dict.fromkeys(ids, False)
        user_obj = self.pool.get('res.users')
        for user_id in ids:
            params = [('id', '=', user_id)]
            id_user = user_obj.search(cr, uid, params)
            colabs = user_obj.read(cr, uid, id_user, ['colaboradores'])
            if colabs:
                res[user_id] = colabs[0]['colaboradores']
        return res

    _columns = {
        'colaboradores': fields.many2many('res.users',
                                           'res_user_colab_rel',
                                           'user_id',
                                           'colab_id',
                                           'Usuarios colaboradores'),
        'colaboradores_ids': fields.function(
            _ff_usuarios_colab, type='char', method=True,
            string='Colaboradores list', size=200
        ),
    }

res_users()
