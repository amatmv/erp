# -*- coding: utf-8 -*-

from osv import osv, fields

class crm_case(osv.osv):
    _name = 'crm.case'
    _inherit = 'crm.case'

    def _ff_colabs(self, cr, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        if not isinstance(ids, list):
            ids = [ids, ]
        res = dict.fromkeys(ids, False)
        user_obj = self.pool.get('res.users')
        crmcase_obj = self.pool.get('crm.case')
        config_obj = self.pool.get('res.config')

        colabs_user = user_obj.read(
            cr, uid, uid, ['colaboradores_ids'])['colaboradores_ids']

        # busquem quines seccions de casos hem de filtrar
        sect_codes = config_obj.get(
            cr, uid, 'crm_case_filter_colab_section_codes', '[]')
        codes_list = eval(sect_codes)
        if codes_list:
            params_case = [
                ('section_id.code', 'in', codes_list),
                ('user_id', 'in', colabs_user)
            ]
            cases_ids = crmcase_obj.search(cr, uid, params_case)
            res_cases = dict.fromkeys(cases_ids, True)
            res.update(res_cases)
        return res

    def _fnct_search_colabs(self, cr, uid, obj, name, args, context=None):
        user_obj = self.pool.get('res.users')
        colabs_loguejat = user_obj.read(cr, uid, uid, ['colaboradores_ids'])

        if colabs_loguejat:
            user_obj = self.pool.get('res.users')
            crmcase_obj = self.pool.get('crm.case')
            config_obj = self.pool.get('res.config')
            colabs_user = user_obj.read(cr, uid, uid,['colaboradores_ids']
                                        )['colaboradores_ids']

            sect_codes = config_obj.get(cr, uid,
                                        'crm_case_filter_colab_section_codes',
                                        '[]')
            codes_list = eval(sect_codes)
            if codes_list:
                params_case = [
                    ('section_id.code', 'in', codes_list),
                    ('user_id', 'in', colabs_user)
                ]
                cases_ids = crmcase_obj.search(cr, uid, params_case)
                if cases_ids:
                    return [('id', 'in', cases_ids)]

        return [('id', '=', '0')]

    _columns = {
        'in_user_colabs': fields.function(
            _ff_colabs, type='boolean', method=True,
            fnct_search=_fnct_search_colabs,
        ),
    }

crm_case()
