# -*- coding: utf-8 -*-

from osv import osv, fields

class crm_polissa(osv.osv):
    _name = 'crm.polissa'
    _inherit = 'crm.polissa'

    def _ff_colabs_crmpol(self, cr, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        if not isinstance(ids, list):
            ids = [ids, ]
        res = dict.fromkeys(ids, False)
        user_obj = self.pool.get('res.users')

        colabs_user = user_obj.read(cr, uid, uid, ['colaboradores_ids']
                                    )['colaboradores_ids']
        if colabs_user:
            search_params = [('user_id', 'in', colabs_user)]
            crmpolissa_ids = self.search(cr, uid, search_params, context=context)
            if crmpolissa_ids:
                res_pol = dict.fromkeys(crmpolissa_ids, True)
                res.update(res_pol)

        return res

    def _fnct_search_colabs_crmpol(self, cursor, uid, obj, name, args,
                                   context=None):
        crmpol_obj = self.pool.get('crm.polissa')
        user_obj = self.pool.get('res.users')
        colabs_user = user_obj.read(cursor, uid, uid, ['colaboradores_ids'])
        if colabs_user:
            colabs_user = colabs_user['colaboradores_ids']
            crmpol_ids = crmpol_obj.search(cursor, uid, [
                ('user_id', 'in', colabs_user)
            ], context=context)
            if crmpol_ids:
                return [('id', 'in', crmpol_ids)]
        return [('id', '=', '0')]

    _columns = {
        'in_user_colabs': fields.function(
            _ff_colabs_crmpol, type='boolean', method=True,
            fnct_search=_fnct_search_colabs_crmpol,
        ),
    }

crm_polissa()
