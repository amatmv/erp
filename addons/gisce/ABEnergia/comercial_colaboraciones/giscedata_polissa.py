# -*- coding: utf-8 -*-

from osv import osv, fields


class giscedata_polissa(osv.osv):
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def _ff_colabs(self, cr, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        if not isinstance(ids, list):
            ids = [ids, ]
        res = dict.fromkeys(ids, False)
        user_obj = self.pool.get('res.users')
        colaboradores = user_obj.read(cr, uid, uid, ['colaboradores_ids']
                                      )['colaboradores_ids']

        search_params = [('user_id', 'in', colaboradores)]
        pol_ids = self.search(cr, uid, search_params, context=context)
        if pol_ids:
            res_polissas = dict.fromkeys(pol_ids, True)
            res.update(res_polissas)

        return res

    def _fnct_search_colabs(self, cursor, uid, obj, name, args,
                                     context=None):
        pol_obj = self.pool.get('giscedata.polissa')
        user_obj = self.pool.get('res.users')
        colaboradores = user_obj.read(cursor, uid, uid, ['colaboradores_ids'])
        if colaboradores:
            colaboradores_ids = colaboradores['colaboradores_ids']
            polisses_id = pol_obj.search(cursor, uid, [
                ('user_id', 'in', colaboradores_ids)
            ], context=context)
            if polisses_id:
                return [('id', 'in', polisses_id)]

        return [('id', '=', '0')]

    _columns = {
        'in_user_colabs': fields.function(
            _ff_colabs, type='boolean', method=True,
            fnct_search=_fnct_search_colabs,
            string='Colaboraciones usuario'
        ),
    }

giscedata_polissa()
