# -*- coding: utf-8 -*-
{
    "name": "Colaboración entre comerciales",
    "description": """
        Afegeix la possibilitat d'indicar als usuaris amb quins usuaris colaboren comercialment.
        """,
    "version": "0-dev",
    "author": "abenergia",
    "category": "",
    "depends":[
        "base",
        "giscedata_polissa_comer_custom_ab",
        "crm_elec",
        "hr_custom_ab"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "res_users_view.xml",
        "comercial_colab_data.xml"
    ],
    "active": False,
    "installable": True
}
