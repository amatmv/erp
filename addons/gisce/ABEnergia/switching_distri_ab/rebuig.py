# -*- coding: utf-8 -*-
from __future__ import absolute_import
from giscedata_switching.rebuig import Rebuig
from enerdata.contracts.normalized_power import NormalizedPower
from enerdata.contracts.tariff import get_tariff_by_code, are_powers_ascending
from enerdata.contracts.tariff import (
    NotNormalizedPower, IncorrectMaxPower, IncorrectMinPower, IncorrectPowerNumber
)


class Rebuig(Rebuig):

    def check_52(self, pool, cursor, uid, sw):
        tariff_obj = pool.get('giscedata.polissa.tarifa')
        sw02 = sw.get_pas()
        if sw02.tarifaATR:
            tar_id = tariff_obj.get_tarifa_from_ocsum(
                cursor, uid, sw02.tarifaATR)
            tariff = tariff_obj.browse(cursor, uid, tar_id)
            num_periods = tariff.get_num_periodes(tipus='tp')
            powers = []
            try:
                powers = [pot.potencia / 1000.0 for pot in sw02.pot_ids]
                tarif_eval = get_tariff_by_code(tariff.name)()
                if sum(powers[num_periods:]) != 0:
                    num_periods = len(powers)
                tarif_eval.evaluate_powers(powers[:num_periods])
            except NotNormalizedPower:
                return True
            except IncorrectMinPower:
                return True
            except IncorrectPowerNumber:
                # If perque la CNMC resulta que diu que en una tarifa 2.0 DHA que te 2 periodes de energia i
                # 1 de potencia es pot enviar dos periodes de potencia i que ens ho hem de menjar peruqe sino
                # iberdrola es queixa
                if sw02.tarifaATR == "004" and len(powers) == 2:
                    return True
                else:
                    return False
            except:
                return False
        return True
