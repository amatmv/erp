# -*- coding: utf-8 -*-
from __future__ import absolute_import
from osv import osv, fields
from .rebuig import Rebuig


class GiscedataSwitching(osv.osv):

    _name = 'giscedata.switching'
    _inherit = 'giscedata.switching'

    def get_rebuig_checker(self, cursor, uid, context):
        return Rebuig()

GiscedataSwitching()

