# -*- coding: utf-8 -*-

import click

from erppeek import Client

'''Script Comer que marca les factures de gestion cobrador com a impagades
quan la data de venciment és igual al dia anterior a l'execució de l'script'''

@click.command()
@click.option('-uc', '--user_comer', default='admin', help='Usuari servidor ERP COMER')
@click.option('-wc', '--password_comer', default='adminabe', help='Contrasenya usuari ERP COMER')
@click.option('-sc', '--server_comer', default='localhost', help=u'IP servidor ERP COMER')
@click.option('-pc', '--port_comer', default=18069, help='Port servidor ERP COMER', type=click.INT)
@click.option('-dc', '--database_comer', default='abenergia', help='Nom de la base de dades de COMER')
@click.option('-to', '--to_addr', help='Destinatari del mail')
@click.option('-cc', '--to_cc', default=False, help='Destinatari en copia del mail')

def marcar_factures_cobrador_retornades(**kwargs):

    server = 'http://{0}:{1}'.format(kwargs['server_comer'],
                                           kwargs['port_comer'])
    client = Client(server=server,
                      db=kwargs['database_comer'],
                      user=kwargs['user_comer'],
                      password=kwargs['password_comer'])

    to_addr = kwargs['to_addr']
    cc_addr = kwargs['to_cc']

    retornar_obj = client.model('cobrador.marcar.retornat')

    retornar_obj.marcar_factures_cobrador_retornades(to_addr, cc_addr)

    return True

if __name__ == '__main__':
    marcar_factures_cobrador_retornades()
