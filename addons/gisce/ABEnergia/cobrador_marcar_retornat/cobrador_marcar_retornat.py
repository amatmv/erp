# -*- coding: utf-8 -*-

import logging
from datetime import datetime
from datetime import timedelta
from calendar import weekday

import pooler
from tools.translate import _
from osv.orm import except_orm
from osv import osv, fields


class CobradorMarcarRetornat(osv.osv_memory):

    _name = 'cobrador.marcar.retornat'

    def send_email(
            self, cursor, uid, email_values, email_from=False, context=None):
        if not context:
            context = {}
        user_obj = self.pool.get('res_users')
        acc_obj = self.pool.get('poweremail.core_accounts')
        mail_obj = self.pool.get('poweremail.mailbox')

        if not email_values.get('pem_account_id', False):
            if context.get('from_account', False):
                acc_id = context.get('from_account')
            else:
                search_values = [('user', '=', uid), ('state', '=', 'approved')]
                acc_id = acc_obj.search(
                    cursor, uid, search_values,context=context)
                if not acc_id:
                    err = _(u"ERROR: No existeix compte de correu d'enviament")
                    raise except_orm('Error', err)
                if isinstance(acc_id, (list, tuple)):
                    acc_id = acc_id[0]
            email_values.update({'pem_account_id': acc_id})

        emailfrom = acc_obj.read(
            cursor, uid, acc_id, ['email_id'])['email_id']
        email_values.update({'pem_from': emailfrom})

        # Using new cursor to persist mail before use
        db = pooler.get_db_only(cursor.dbname)
        pbcr = db.cursor()
        mail_id = mail_obj.create(pbcr, uid, email_values, context)
        pbcr.commit()
        pbcr.close()
        mail_obj.send_this_mail(cursor, uid, [mail_id], context)

        return mail_id

    def enviar_mail_a_responsable_cartes_tall(self, cr, uid, ids, to_addr,
                                              cc_addr=False, context=None):
        if not context:
            context = {}

        logger = logging.getLogger(
            'openerp.{0}.enviar_mail_a_responsable_cartes_tall'.format(__name__)
        )

        fra_obj = self.pool.get('giscedata.facturacio.factura')
        fra_ids = fra_obj.search(cr, uid, [('invoice_id', 'in', ids)])
        fras = fra_obj.read(cr, uid, fra_ids, ['number', 'polissa_id'])

        fras_cttos = [str(fra['number']) + ': ' + str(fra['polissa_id'][1])
                      for fra in fras]

        body_html = '<p>Listado factura: contrato</p>'
        body_html += ''.join('<p>'+fra_ctto+'</p>' for fra_ctto in fras_cttos)

        email_values = {
            'pem_subject': 'Contratos pasados a estado impagado gestión cobrador',
            'pem_body_text': body_html,
            'pem_cc': cc_addr,
            'pem_to': to_addr,
        }

        mail_id = self.send_email(cr, uid, email_values, to_addr, context=context)

        logger.info(
            'Enviat email id {0} contractes gestion cobrador passats a impagats'
            'en data {1}'.format(mail_id,str(datetime.today())))

        return mail_id

    def marcar_factures_cobrador_retornades(self, cr, uid, to_addr, cc_addr,
                                            context=None):
        '''Passa a estat impagat les factures de gestion cobrador que
        estan obertes i que han passat 6 dies des de la data de venciment en
        la data d'execucio'''
        if not context: context = {}

        invoice_obj = self.pool.get('account.invoice')
        tipo_pago_obj = self.pool.get('payment.type')
        journal_obj = self.pool.get('account.journal')

        # busquem les fres amb tipo de pago gestion cobrador obertes passades
        # de data limit

        # La data limit és la de venciment + 5 dies en el cas de cobrador
        # (restem 7 perquè passaran a impagats 2 dies després de la data limit)
        # En el cas de gestió cobrador, poden pagar amb el codi de barres

        cobrador_id = tipo_pago_obj.search(cr, uid,
                                           [('name', 'like', '%COBRADOR%')])
        diaris_energia = journal_obj.search(cr, uid,
                                              [('code', '=', 'ENERGIA')]
                                              )
        avui = datetime.today()
        if weekday(avui.year, avui.month, avui.day) in [6,0]:
            # No generem el tall en cap de setmana (ni diumenge ni dilluns)
            ids_inv = []
        elif weekday(avui.year, avui.month, avui.day) == 1: # dimarts
            limit1 = (avui - timedelta(days = 7)).strftime('%Y-%m-%d')
            limit2 = (avui - timedelta(days = 8)).strftime('%Y-%m-%d')
            limit3 = (avui - timedelta(days = 9)).strftime('%Y-%m-%d')
            ids_inv = invoice_obj.search(cr, uid,
                                         [('payment_type', '=', cobrador_id),
                                          ('state', '=', 'open'),
                                          ('journal_id', 'in', diaris_energia),
                                          ('date_due', 'in',
                                           [limit1, limit2, limit3])])
        else:
            data_limit = (avui - timedelta(days = 7)).strftime('%Y-%m-%d')
            ids_inv = invoice_obj.search(cr, uid,
                                         [('payment_type', '=', cobrador_id),
                                          ('state', '=', 'open'),
                                          ('journal_id', 'in', diaris_energia),
                                          ('date_due', '=', data_limit)])
        if ids_inv:
            invoice_obj.go_on_pending(cr, uid, ids_inv)
            devol_obj = self.pool.get('giscedata.facturacio.devolucio')
            devol_obj.generate_despeses_devolucio(cr, uid, ids_inv)
            self.enviar_mail_a_responsable_cartes_tall(cr, uid, ids_inv, to_addr,
                                                       cc_addr, context=context)

        return True

CobradorMarcarRetornat()
