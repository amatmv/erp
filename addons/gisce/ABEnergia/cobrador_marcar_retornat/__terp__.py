# -*- coding: utf-8 -*-
{
    "name": "Crm elec 'retornar' fres cobrador",
    "description": """Afegeix un script per marcar com a retornades les factures de gestió cobrador quan passa la data de venciment""",
    "version": "0-dev",
    "author": "Abenergia",
    "category": "CRM",
    "depends":[
        "crm_elec",
        "giscedata_facturacio",
        "giscedata_facturacio_devolucions",
        "giscedata_polissa"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
