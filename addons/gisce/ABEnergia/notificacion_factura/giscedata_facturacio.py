# -*- coding: utf-8 -*-

from osv import osv, fields


class GiscedataFacturacioFactura(osv.osv):

    _name = 'giscedata.facturacio.factura'
    _inherit = 'giscedata.facturacio.factura'

    def invoice_open(self, cursor, uid, ids, context=None):
        """
        Custom para eliminar las notificaciones cuando la factura se abre.
        """

        if context is None:
            context = {}

        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        invoices_created = super(
            GiscedataFacturacioFactura, self
        ).invoice_open(cursor, uid, ids, context=context)

        notificacion_factura_obj = self.pool.get('notificacion.factura')

        # Info we want to get from the invoices that are being opened
        q_get_info = self.q(cursor, uid).select(
            ['id', 'polissa_id', 'llista_preu']
        ).where([
            ('id', 'in', ids),
            # We don't want to include invoices with journal different from
            # exactly 'ENERGIA', that are, the rectifying invoices
            ('invoice_id.journal_id.code', '=', 'ENERGIA')
        ])

        cursor.execute(*q_get_info)
        for factura in (fact for fact in cursor.dictfetchall()):
            # Search for a notification that has the same contract and
            # pricelist
            q_search_notifications = notificacion_factura_obj.q(
                cursor, uid
            ).select(['id', 'texto_informativo']).where([
                ('llista_preus', '=', factura['llista_preu']),
                ('polissa_id', '=', factura['polissa_id'])
            ])

            cursor.execute(*q_search_notifications)
            # If there's any pending notification for that invoice, we set the
            # field nota informativa and we delete it
            pending_notification = cursor.dictfetchone()

            if pending_notification:
                current_text = self.read(
                    cursor, uid, factura['id'], ['nota_informativa']
                )['nota_informativa']

                nota = pending_notification['texto_informativo']
                if current_text:
                   nota = '{current}\n{new}'.format(
                       current=current_text,
                       new=nota
                   )

                self.write(
                    cursor, uid, factura['id'], {
                        'nota_informativa': nota
                    }
                )
                notificacion_factura_obj.unlink(
                    cursor, uid, pending_notification['id']
                )

        return invoices_created


GiscedataFacturacioFactura()


class NotificacionFactura(osv.osv):

    _name = 'notificacion.factura'

    _columns = {
        'polissa_id': fields.many2one(
            'giscedata.polissa', 'Póliza', required=True, select=True
        ),
        'llista_preus': fields.many2one(
            'product.pricelist', 'Lista de Precios', required=True
        ),
        'texto_informativo': fields.text('Texto Informativo', required=True),
    }

    _order = 'id, polissa_id'


NotificacionFactura()
