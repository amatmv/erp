# -*- coding: utf-8 -*-

from destral import testing
from destral.transaction import Transaction


class TestsNotificacionFactura(testing.OOTestCase):

    def test_notificacion_applies_when_opening_invoice(self):

        imd_obj = self.openerp.pool.get('ir.model.data')
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        notif_obj = self.openerp.pool.get('notificacion.factura')
        journal_obj = self.openerp.pool.get('account.journal')

        with Transaction().start(self.database) as txn:

            cursor = txn.cursor
            uid = txn.user

            # We get the models we need
            factura_1_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0001'
            )[1]
            factura_2_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0002'
            )[1]
            polissa_2_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0002'
            )[1]

            # Setting the contract 2 to the invoice 2
            fact_obj.write(cursor, uid, factura_2_id, {
                'polissa_id': polissa_2_id
            })

            # Getting the info we need from the two invoices
            res_fact_1 = fact_obj.read(
                cursor, uid, factura_1_id, ['polissa_id', 'llista_preu']
            )

            res_fact_2 = fact_obj.read(
                cursor, uid, factura_2_id, ['polissa_id', 'llista_preu']
            )

            # Demo text we set to the notifications
            nota_1_p1 = u'Notificación de prueba 1, poliza 1.'
            nota_2_p1 = u'Notificación de prueba 2, poliza 1.'
            nota_1_p2 = u'Notificación de prueba 1, poliza 2.'
            nota_2_p2 = u'Notificación de prueba 2, poliza 2.'

            # Creating the notifications, two for each invoice
            notif_1 = notif_obj.create(cursor, uid, {
                'polissa_id': res_fact_1['polissa_id'][0],
                'llista_preus': res_fact_1['llista_preu'][0],
                'texto_informativo': nota_1_p1
            })

            notif_2 = notif_obj.create(cursor, uid, {
                'polissa_id': res_fact_1['polissa_id'][0],
                'llista_preus': res_fact_1['llista_preu'][0],
                'texto_informativo': nota_2_p1
            })

            notif_3 = notif_obj.create(cursor, uid, {
                'polissa_id': res_fact_2['polissa_id'][0],
                'llista_preus': res_fact_2['llista_preu'][0],
                'texto_informativo': nota_1_p2
            })

            notif_4 = notif_obj.create(cursor, uid, {
                'polissa_id': res_fact_2['polissa_id'][0],
                'llista_preus': res_fact_2['llista_preu'][0],
                'texto_informativo': nota_2_p2
            })

            fact_obj.invoice_open(cursor, uid, factura_1_id)
            fact_obj.invoice_open(cursor, uid, factura_2_id)

            nota_factura_1 = fact_obj.read(
                cursor, uid, factura_1_id, ['nota_informativa']
            )['nota_informativa']

            nota_factura_2 = fact_obj.read(
                cursor, uid, factura_2_id, ['nota_informativa']
            )['nota_informativa']

            # Given that the notifications are chosen by FIFO order, the
            # notifications that have been asigned to each invoice must be the
            # oldest ones for their contracts
            self.assertEqual(nota_factura_1, nota_1_p1)
            self.assertEqual(nota_factura_2, nota_1_p2)

            # Also, the notifications must have been deleted
            notifications_res = notif_obj.search(cursor, uid, [
                ('id', 'in', [notif_1, notif_2, notif_3, notif_4])
            ])
            self.assertNotIn(notif_1, notifications_res)
            self.assertIn(notif_2, notifications_res)
            self.assertNotIn(notif_3, notifications_res)
            self.assertIn(notif_4, notifications_res)

            # If we cancel the invoice and we re-open it, the next notification
            # in the queue should be appended to the end with a break-line
            # between.
            journal_id = fact_obj.read(
                cursor, uid, factura_1_id, ['journal_id']
            )['journal_id'][0]
            journal_obj.write(cursor, uid, journal_id, {
                'update_posted': True
            })
            fact_obj.invoice_cancel(cursor, uid, [factura_1_id])
            fact_obj.action_cancel_draft(cursor, uid, [factura_1_id])
            fact_obj.invoice_open(cursor, uid, factura_1_id)

            nota_factura_1 = fact_obj.read(
                cursor, uid, factura_1_id, ['nota_informativa']
            )['nota_informativa']
            self.assertEqual(
                nota_factura_1,
                '{}\n{}'.format(nota_1_p1, nota_2_p1)
            )