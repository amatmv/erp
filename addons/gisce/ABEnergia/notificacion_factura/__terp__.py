# -*- coding: utf-8 -*-
{
    "name": "Notificación Facturas",
    "description": """Notificación facturas de ABenergia """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Extrareports",
    "depends": [
        "giscedata_facturacio_comer_ab"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "giscedata_facturacio_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
