# -*- coding: utf-8 -*-
"""
Wizard per resum de les devolucions per correos 
===============================================

"""

import base64
# cStringIO is faster :)
import logging
import tempfile

from osv import fields, osv
from osv.osv import except_osv
from tools.translate import _
try:
    from xlwt import Workbook, easyxf, XFStyle
except ImportError:
    raise except_osv("Error", "You must intsall xlutils:\n"
                     "pip install xlutils")

try:
    from nameparser import HumanName
except ImportError:
    raise except_osv("Error", "You must intsall nameparser:\n"
                     "pip install parsername")

ESTATS_RESUMS_CORREOS = [
    ('inici', 'inici'),
    ('fitxer', 'fitxer')
]


class WizardResumCorreosCartaTall(osv.osv_memory):
    """
    Wizard per generar resum de les cartes de tall per enviar a correus
    """
    _name = "resum.correos.carta.tall"

    _columns = {
        'name': fields.char('Nom', size=128),
        'state': fields.selection(ESTATS_RESUMS_CORREOS, 'Estat'),
        'generated_file': fields.binary('Resum fitxer'),
        'message': fields.text('Informació', readonly=True),
        'header_file': fields.boolean(u'Capçalera del fitxer')
    }

    def _default_message(self, cursor, uid, context=None):
        if context is None:
            context = {}
        active_ids = context.get('active_ids')
        total = 0
        if active_ids:
            total = len(active_ids)
        return _(u'Es farà el resum per Correus de les {0} factures '
                 u'seleccionades '.format(total)
                 )

    def get_resum_xls(
            self, cursor, uid, factures_ids, header=False, context=None):
        fact_obj = self.pool.get('giscedata.facturacio.factura')
        partner_obj = self.pool.get('res.partner')
        workbook = Workbook(encoding='utf-8')
        sheet = workbook.add_sheet('datos')

        style = easyxf(
            'align: vertical center, horizontal center;'
            'font: colour black, bold True;'
            'pattern: pattern solid, fore_colour lime;'
        )
        headers = [
            'NOMBRE', 'APELLIDOS', 'JURIDICO', 'DIRECCION',
            'CP', 'POBLACION', 'PROVINCIA',
            'ES', 'CODIGO', '1', '0', '0']
        fila = 0
        if header:
            for idx, header in enumerate(headers):
                sheet.write(fila, idx, header, style)
            fila += 1
        for factura in fact_obj.browse(cursor, uid, factures_ids):
            vat = factura.partner_id.vat
            empresa = partner_obj.vat_es_empresa(cursor, uid, vat)
            nom = ''
            cognoms = ''
            juridic = ''
            columna = 0
            if not empresa:
                name = HumanName(factura.address_contact_id.name)
                nom = name.first
                if ',' in factura.address_contact_id.name:
                    nom += ' ' + name.middle
                    cognoms = name.last
                else:
                    cognoms += name.middle + ' ' + name.last
            else:
                juridic = factura.address_contact_id.name
            sheet.write(fila, columna, nom)
            columna += 1
            sheet.write(fila, columna, cognoms)
            columna += 1
            sheet.write(fila, columna, juridic)
            columna += 1
            sheet.write(fila, columna, factura.address_contact_id.street)
            columna += 1
            sheet.write(fila, columna, factura.address_contact_id.zip)
            columna += 1
            if factura.address_contact_id.id_poblacio:
                sheet.write(
                    fila, columna, factura.address_contact_id.id_poblacio.name
                )
            else:
                sheet.write(fila, columna, '')
            columna += 1
            if factura.address_contact_id.state_id:
                sheet.write(
                    fila, columna, factura.address_contact_id.state_id.name)
            else:
                sheet.write(fila, columna, '')
            columna += 1
            sheet.write(fila, columna, 'ES')
            columna += 1
            sheet.write(fila, columna, factura.number)
            columna += 1
            sheet.write(fila, columna, '1')
            columna += 1
            sheet.write(fila, columna, '0')
            columna += 1
            sheet.write(fila, columna, '0')
            fila += 1
        return workbook

    def generate_resum(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        logger = logging.getLogger('openerp.resum.correos.generate_resum')
        wiz = self.browse(cursor, uid, ids[0], context=context)
        active_ids = context.get('active_ids', [])
        logger.info(
            'Generate breif for Correos with following ids: {0}'.format(
                active_ids)
        )
        workbook = self.get_resum_xls(
            cursor, uid, active_ids, wiz.header_file, context=context)
        temp_file = tempfile.mkstemp()[1]
        logger.info(
            'Save XLS to file: {0}'.format(temp_file)
        )
        workbook.save(temp_file)
        xls_file = None
        filename = _('resum_cartes_tall.xls')
        with open(temp_file, 'r') as f:
            xls_file = base64.b64encode(f.read())

        info = _(u'Fitxer generat correctament: {0}'.format(filename))
        wiz.write(
            {
                'state': ESTATS_RESUMS_CORREOS[-1],
                'message': info,
                'generated_file': xls_file,
                'name': filename
            }
        )

    _defaults = {
        'state': lambda *a: ESTATS_RESUMS_CORREOS[0],
        'message': _default_message,
    }

WizardResumCorreosCartaTall()
