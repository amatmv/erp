# -*- coding: utf-8 -*-
{
    "name": "Devolucions exportació resum correos en XLS",
    "description": """
Resum de XLS per a correos
    """,
    "version": "0-dev",
    "author": "GISCE-TI",
    "category": "Facturació",
    "depends":[
        "giscedata_facturacio_devolucions"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/wizard_export_correos_xls.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
