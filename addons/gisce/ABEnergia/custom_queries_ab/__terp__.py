# -*- coding: utf-8 -*-
{
    "name": "custom_queries_ab",
    "description": """
        Aquest mòdul afegeix les següents funcionalitats:
            * Afegeix unes custom_search que permeten buscar els detalls de
            facturacio per energia i contractacio
     """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "custom_search"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "invoicing_detail_data.xml",
        "atr_custom_search_data.xml"
    ],
    "active": False,
    "installable": True
}
