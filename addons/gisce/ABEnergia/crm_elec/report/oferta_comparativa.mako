<%
    from osv import osv
    from datetime import datetime
    from libfacturacioatr.tarifes import maximetre

    def get_fields(cursor, uid, ids, pool):
        crm_obj = pool.get('crm.polissa')
        comp_pot_obj = pool.get('comparativa.potencia.periode')
        comp_ener_obj = pool.get('comparativa.consums.periode')

        vals_to_read = [
            'comparativa_info',
            'comparativa_data_inici',
            'comparativa_data_fi',
            'comparativa_razon_social',
            'comparativa_dni',
            'comparativa_cups',
            'comparativa_direccion',
            'comparativa_potencia_contratada',
            'comparativa_poblacio',
            'comparativa_iva',
            'comparativa_tarifa_acces',
            'comparativa_tarifa_comercialitzadora',
            'comparativa_import_competencia',
            'comparativa_import_lloguer',
            'comparativa_cost_reactiva',
            'comparativa_import_otros',
            'comparativa_impost_electric',
            'comparativa_total_sense_iva',
            'comparativa_total_amb_iva',
            'comparativa_diferencia_preus',
            'comparativa_oferta_consums_periode',
            'comparativa_oferta_potencia_periode',
            'comparativa_maximetre',
            'comparativa_observacions',
            'consumo_anual_previsto',
            'consumo_anual_previsto_directo']

        fields = crm_obj.read(cursor, uid, ids, vals_to_read)

        if fields['comparativa_info'] != 'Datos copiados con éxito.':
            raise osv.except_osv('Error', 'No se han corregido las incidencias '
            'de la comparativa.')
        for field in fields:
            if not fields[field]:
             if field == 'comparativa_observacions':
                fields[field] = ''
             else:
                fields[field] = 0

        fields['comparativa_diferencia_perc'] = 100 * abs(fields['comparativa_diferencia_preus'] / fields['comparativa_total_amb_iva'])
        fields['comparativa_cost_iva'] = fields['comparativa_total_amb_iva'] - fields['comparativa_total_sense_iva']

        datainici = fields['comparativa_data_inici']
        datafi = fields['comparativa_data_fi']
        intervaldies = datetime.strptime(datafi, '%Y-%m-%d') - datetime.strptime(datainici, '%Y-%m-%d')
        intervaldies = intervaldies.days

        """Posem en el terme de potencia una llista de llistes tal com:
        [[Nom del periode, potencia facturada, dies facturats, cost x kw, preu],...]"""

        matriu_potencies = []
        for id in fields['comparativa_oferta_potencia_periode']:
            dades = comp_pot_obj.read(cursor, uid, id, ['periode',
                                                        'potencia',
                                                        'preu',
                                                        'lectura',
                                                        'import'])
            if fields['comparativa_maximetre']:
                potenciafacturada = maximetre(
                    potencia_contractada=dades['potencia'],
                    potencia_maximetre=dades['lectura'])
            else:
                potenciafacturada = dades['potencia']

            fila = [dades['periode'],
                    str(potenciafacturada) + 'kW',
                    str(intervaldies) + 'días',
                    str(dades['preu']) + '€/kWdía',
                    str(dades['import']) + '€']

            matriu_potencies.append(fila)
        fields['comparativa_oferta_potencia_periode'] = matriu_potencies

        """Posem en el terme d'energia una llista de llistes tal com:
        [[Nom del periode, consum, dies facturats, cost x kw, preu], ...]"""

        matriu_energia = []
        for id in fields['comparativa_oferta_consums_periode']:
            dades = comp_ener_obj.read(cursor, uid, id, ['periode',
                                                         'consum',
                                                         'preu',
                                                         'import'])
            consumkw = dades['consum'] if dades['consum'] else 0
            fila = [dades['periode'],
                    str(consumkw) + 'kWh',
                    str(dades['preu']) + '€/kWh',
                    str(dades['import']) + '€']

            matriu_energia.append(fila)

        fields['comparativa_oferta_consums_periode'] = matriu_energia

        if fields['consumo_anual_previsto_directo']:
            fields['consumo_anual'] = fields['consumo_anual_previsto_directo']
        elif fields['consumo_anual_previsto']:
            fields['consumo_anual'] = fields['consumo_anual_previsto']
        else:
            fields['consumo_anual'] = 0

        fields['cost_electric'] = fields['comparativa_total_sense_iva'] - fields['comparativa_import_lloguer']

        return fields
%>

<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
         <link rel="stylesheet" href="${addons_path}/crm_elec/report/oferta_comparativa.css"/>
        <style type="text/css">

        </style>
    </head>
    <body>
        %for oferta in objects :
            <%
                fields = get_fields(cursor, uid, oferta.id, oferta.pool)
            %>
            <div>
                <div id="header" class="row">
                    <div class="column">
                        <br/>
                        <img id="logo" height="150px" src="${addons_path}/crm_elec/report/img/abenergia-logo.png"/>
                    </div>

                    <div class="column">
                        <br/><br/>
                        <h1>Estudio comparativo de suministro eléctrico</h1>
                    </div>
                </div>

                <div class="row">
                    <div class="column">
                        <div>
                            <div class="title green">
                                Datos del cliente
                            </div>
                            <table>
                                <colgroup>
                                    <col style="width: 45%"/>
                                    <col style="width: auto"/>
                                </colgroup>
                                <tr>
                                    <td>
                                        Nombre / Razón social:
                                    </td>
                                    <td class="bold">
                                        ${fields['comparativa_razon_social']}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Dirección:
                                    </td>
                                    <td class="bold">
                                        ${fields['comparativa_direccion']}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Población:
                                    </td>
                                    <td class="bold">
                                        ${fields['comparativa_poblacio']}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        NIF / DNI:
                                    </td>
                                    <td class="bold">
                                        ${fields['comparativa_dni']}
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div>
                            <div class="title green">
                                Datos del suministro
                            </div>
                            <table>
                                <tr>
                                    <td>
                                        CUPS:
                                    </td>
                                    <td class="bold">
                                        ${fields['comparativa_cups']}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Potencia contratada:
                                    </td>
                                    <td class="bold">
                                        ${fields['comparativa_potencia_contratada']}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Tarifa:
                                    </td>
                                    <td class="bold">
                                        ${fields['comparativa_tarifa_acces']}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Consumo anual estimado:
                                    </td>
                                    <td class="bold">
                                        ${fields['consumo_anual']}
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div>
                            <div class="title turquoise">
                                Importe factura comercializadora actual
                            </div>
                            <table>
                                <tr>
                                    <td>
                                        ${formatLang(fields['comparativa_import_competencia'], 2)}
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div>
                            <div class="title turquoise">
                                Importe factura abenergía
                            </div>
                            <table>
                                <tr>
                                    <td>
                                        ${formatLang(fields['comparativa_total_amb_iva'], 2)} €
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div>
                            <div class="title turquoise">
                                Ahorro previsto
                            </div>
                            <%
                                ahorro = fields['comparativa_diferencia_preus']
                            %>
                            <table>
                                <tr>
                                    <td>
                                        %if ahorro > 0:
                                            ${formatLang(ahorro, 2)} €
                                        %else:
                                            <span class="bold" style="color: red">
                                                ${formatLang(ahorro, 2)} €
                                            </span>
                                        %endif
                                    </td>
                                    <td>
                                        ${formatLang(fields['comparativa_diferencia_perc'], 2)}%
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="column">
                        <div>
                            <div class="title blue">
                                Cálculo de la factura
                            </div>
                        </div>
                        <div>
                            <div class="subtitle blue">
                                Término de potencia
                            </div>
                            <table class="subtitle_table">
                                <colgroup>
                                    <col style="width: 15%;"/>
                                    <col style="width: 15%;"/>
                                    <col style="width: auto;"/>
                                    <col style="width: auto;"/>
                                    <col style="width: 10%;"/>
                                </colgroup>
                                %for i in range(0, len(fields['comparativa_oferta_potencia_periode'])):
                                    <tr>
                                        <td>
                                            <% periode = fields['comparativa_oferta_potencia_periode'][i][0]%>
                                            ${periode}
                                        </td>
                                        <td>
                                            <% potencia = fields['comparativa_oferta_potencia_periode'][i][1]%>
                                            ${potencia}
                                        </td>
                                        <td>
                                            <% dies = fields['comparativa_oferta_potencia_periode'][i][2]%>
                                            ${dies}
                                        </td>
                                        <td>
                                            <%
                                            s_preu = fields['comparativa_oferta_potencia_periode'][i][3].split("€")
                                            n_preu = str(formatLang(float(s_preu[0]),2))
                                            preu = n_preu + " €/kW año"
                                            %>
                                            ${preu}
                                        </td>
                                        <td>
                                            <% importe = fields['comparativa_oferta_potencia_periode'][i][4]%>
                                            ${importe}
                                        </td>
                                    </tr>
                                %endfor
                            </table>
                        </div>
                        <div>
                            <div class="subtitle blue">
                                Término de energía
                            </div>
                            <table class="subtitle_table">
                                <colgroup>
                                    <col style="width: 15%;">
                                    <col style="width: 15%;">
                                    <col style="width: auto;">
                                    <col style="width: 45%;">
                                    <col style="width: 10%;">
                                </colgroup>
                                %for i in range(0, len(fields['comparativa_oferta_consums_periode'])):
                                    <tr>
                                        <td class="align_left">
                                            <% periode = fields['comparativa_oferta_consums_periode'][i][0]%>
                                            ${periode}
                                        </td>
                                        <td class="align_left">
                                            <% consum = fields['comparativa_oferta_consums_periode'][i][1]%>
                                            ${consum}
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td class="align_left">
                                            <%
                                                s_preu = fields['comparativa_oferta_consums_periode'][i][2].split("€")
                                                n_preu = str(formatLang(float(s_preu[0]),2))
                                                preu = n_preu + "€" + s_preu[1]
                                            %>
                                            ${preu}
                                        </td>
                                        <td style="text-align: right;">
                                            <%
                                                s_preu = fields['comparativa_oferta_consums_periode'][i][3].split("€")
                                                n_preu = str(formatLang(float(s_preu[0]),2))
                                                importe = n_preu + "€" + s_preu[1]
                                            %>
                                            ${importe}
                                        </td>
                                    </tr>
                                %endfor
                            </table>
                        </div>
                        <div>
                            <div class="subtitle blue">
                                Otros conceptos
                            </div>
                            <table class="subtitle_table">
                                <tr>
                                    <td style="text-align: right;">
                                        ${formatLang(fields['comparativa_import_otros'], 2)} €
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div>
                            <div class="subtitle blue">
                                Importe alquiler
                            </div>
                            <table class="subtitle_table">
                                <tr>
                                    <td style="text-align: right;">
                                        ${formatLang(fields['comparativa_import_lloguer'], 2)} €
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div>
                            <div class="subtitle blue">
                                Impuesto electricidad
                            </div>
                            <table class="subtitle_table">
                                <tr>
                                    <td>
                                        5,11269632 %
                                    </td>
                                    <td>
                                        sobre
                                    </td>
                                    <td>
                                        ${formatLang(fields['cost_electric'], 2)}
                                    </td>
                                    <td style="text-align: right;">
                                        ${formatLang(fields['comparativa_impost_electric'], 2)} €
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <br/>
                        <div id="globus">
                            <table class="padding-10">
                                <tr>
                                    <td>
                                        BASE IMPONIBLE
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td style="text-align: right;">
                                        ${formatLang(fields['comparativa_total_sense_iva'], 2)} €
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        IVA
                                    </td>
                                    <td>
                                        ${fields['comparativa_iva'][1]}
                                    </td>
                                    <td style="text-align: right;">
                                        ${formatLang(fields['comparativa_cost_iva'], 2)} €
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        IMPORTE TOTAL EUROS
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td style="text-align: right;">
                                        ${formatLang(fields['comparativa_total_amb_iva'], 2)} €
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="title green margin-side-20">
                    Observaciones
                </div>
                <div id="observaciones">
                    ${fields['comparativa_observacions']}
                </div>
            </div>
        %endfor
    </body>
</html>
