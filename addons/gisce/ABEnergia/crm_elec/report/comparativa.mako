<%
    from osv import osv
    from datetime import datetime
    from libfacturacioatr.tarifes import maximetre

    def get_fields(cursor, uid, ids, pool):
        crm_obj = pool.get('crm.polissa')
        comp_pot_obj = pool.get('comparativa.potencia.periode')
        comp_ener_obj = pool.get('comparativa.consums.periode')

        vals_to_read = [
            'comparativa_info',
            'comparativa_data_inici',
            'comparativa_data_fi',
            'comparativa_razon_social',
            'comparativa_dni',
            'comparativa_cups',
            'comparativa_direccion',
            'comparativa_potencia_contratada',
            'comparativa_poblacio',
            'comparativa_iva',
            'comparativa_tarifa_acces',
            'comparativa_tarifa_comercialitzadora',
            'comparativa_import_competencia',
            'comparativa_import_lloguer',
            'comparativa_cost_reactiva',
            'comparativa_import_otros',
            'comparativa_impost_electric',
            'comparativa_total_sense_iva',
            'comparativa_total_amb_iva',
            'comparativa_diferencia_preus',
            'comparativa_oferta_consums_periode',
            'comparativa_oferta_potencia_periode',
            'comparativa_maximetre',
            'comparativa_observacions',
            'consumo_anual_previsto',
            'consumo_anual_previsto_directo']

        fields = crm_obj.read(cursor, uid, ids, vals_to_read)

        if fields['comparativa_info'] != 'Datos copiados con éxito.':
            raise osv.except_osv('Error', 'No se han corregido las incidencias '
            'de la comparativa.')

        for field in fields:
            if not fields[field]:
             if field == fields['comparativa_observacions']:
                fields[field] = ''
             else:
                fields[field] = 0

        fields['comparativa_diferencia_perc'] = 100 * abs(fields['comparativa_diferencia_preus'] / fields['comparativa_total_amb_iva'])
        fields['comparativa_cost_iva'] = fields['comparativa_total_amb_iva'] - fields['comparativa_total_sense_iva']

        datainici = fields['comparativa_data_inici']
        datafi = fields['comparativa_data_fi']
        intervaldies = datetime.strptime(datafi, '%Y-%m-%d') - datetime.strptime(datainici, '%Y-%m-%d')
        intervaldies = intervaldies.days

        """Posem en el terme de potencia una llista de llistes tal com:
        [[Nom del periode, potencia facturada, dies facturats, cost x kw, preu],...]"""

        matriu_potencies = []
        for id in fields['comparativa_oferta_potencia_periode']:
            dades = comp_pot_obj.read(cursor, uid, id, ['periode',
                                                        'potencia',
                                                        'preu',
                                                        'lectura',
                                                        'import'])
            if fields['comparativa_maximetre']:
                potenciafacturada = maximetre(
                    potencia_contractada=dades['potencia'],
                    potencia_maximetre=dades['lectura'])
            else:
                potenciafacturada = dades['potencia']

            fila = [dades['periode'],
                    str(potenciafacturada) + 'kW',
                    str(intervaldies) + 'días',
                    str(dades['preu']) + '€/kWdía',
                    str(dades['import']) + '€']

            matriu_potencies.append(fila)
        fields['comparativa_oferta_potencia_periode'] = matriu_potencies

        """Posem en el terme d'energia una llista de llistes tal com:
        [[Nom del periode, consum, dies facturats, cost x kw, preu], ...]"""

        matriu_energia = []
        for id in fields['comparativa_oferta_consums_periode']:
            dades = comp_ener_obj.read(cursor, uid, id, ['periode',
                                                         'consum',
                                                         'preu',
                                                         'import'])
            fila = [dades['periode'],
                    str(dades['consum']) + 'kWh',
                    str(dades['preu']) + '€/kWh',
                    str(dades['import']) + '€']

            matriu_energia.append(fila)

        fields['comparativa_oferta_consums_periode'] = matriu_energia

        if fields['consumo_anual_previsto_directo']:
            fields['consumo_anual'] = fields['consumo_anual_previsto_directo']
        elif fields['consumo_anual_previsto']:
            fields['consumo_anual'] = fields['consumo_anual_previsto']
        else:
            fields['consumo_anual'] = 0

        fields['cost_electric'] = fields['comparativa_total_sense_iva'] - fields['comparativa_import_lloguer']

        return fields
%>


<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
        <style type="text/css">
            ${css}
        </style>
    </head>
    <body>
        %for oferta in objects :
            <%
                fields = get_fields(cursor, uid, oferta.id, oferta.pool)
            %>
            <img src="${addons_path}/crm_elec/report/img/LOGO-ABE-electricidad-comparativa.png"/>
            <h1>Estudio comparativo de suministro eléctrico</h1>
            <h2>Datos del cliente</h2>
                <p>Nombre / Razón social: ${fields['comparativa_razon_social']}</p>
                <p>Dirección: ${fields['comparativa_direccion']}</p>
                <p>Población: ${fields['comparativa_poblacio']}</p>
                <p>NIF / DNI: ${fields['comparativa_dni']}</p>

            <h2>Datos del suministro</h2>
                <p>CUPS: ${fields['comparativa_cups']}</p>
                <p>Potencia contratada: ${fields['comparativa_potencia_contratada']} kW</p>
                <p>Tarifa: ${fields['comparativa_tarifa_acces']}</p>
                <p>Consumo anual estimado: ${fields['consumo_anual']} kW/año</p>

            <h2>Importe factura comercializadora actual</h2>
                <p>${fields['comparativa_import_competencia']} €</p>

            <h2>Importe factura ab energía</h2>
                <p>${fields['comparativa_total_amb_iva']} €</p>

            <h2>Ahorro previsto</h2>
                <p>${fields['comparativa_diferencia_preus']} €     ${fields['comparativa_diferencia_perc']}%</p>

            <h2>Cálculo de la factura</h2>
            <h2>Término de potencia</h2>
            %for i in range(len(fields['comparativa_oferta_potencia_periode'])):
                <% periode = fields['comparativa_oferta_potencia_periode'][i][0]%>
                <% potencia = fields['comparativa_oferta_potencia_periode'][i][1]%>
                <% dies = fields['comparativa_oferta_potencia_periode'][i][2]%>
                <% preu = fields['comparativa_oferta_potencia_periode'][i][3]%>
                <% importe = fields['comparativa_oferta_potencia_periode'][i][4]%>

                <p>${periode}     ${potencia}     ${dies}     ${preu}     ${importe}</p>
            %endfor

            <h2>Término de energía</h2>
            %for i in range(len(fields['comparativa_oferta_consums_periode'])):
                <% periode = fields['comparativa_oferta_potencia_periode'][i][0]%>
                <% consum = fields['comparativa_oferta_potencia_periode'][i][1]%>
                <% preu = fields['comparativa_oferta_potencia_periode'][i][3]%>
                <% importe = fields['comparativa_oferta_potencia_periode'][i][4]%>

                <p>${periode}     ${consum}     ${preu}     ${importe}</p>
            %endfor

            <h2>Otros conceptos</h2>
                <p>${fields['comparativa_import_otros']} €</p>

            <h2>Importe alquiler</h2>
                <p>${fields['comparativa_import_lloguer']} €</p>

            <h2>Impuesto electricidad</h2>
                <p>5,11269632% sobre ${fields['cost_electric']} €     ${fields['comparativa_impost_electric']} €</p>

            <p>BASE IMPONIBLE   ${fields['comparativa_total_sense_iva']} €</p>
            <p>${fields['comparativa_iva'][1]}%  ${fields['comparativa_cost_iva']} €</p>
            <p>IMPORTE TOTAL EUROS     ${fields['comparativa_total_amb_iva']}</p>

            <h2>Observaciones</h2>
                <p>${fields['comparativa_observacions']}</p>
        %endfor
    </body>
</html>