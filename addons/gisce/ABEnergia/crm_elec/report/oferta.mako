<%
    from datetime import datetime, date
    import calendar
    from dateutil.relativedelta import relativedelta
    from operator import attrgetter, itemgetter
    from addons.crm_elec.crm_polissa import get_consums_anuals_periode
%>


<%def name="firma_contratos()">
    <div class="firma">
        <div class="row">
            <div class="col-xs-5">
                <p class="head">Firma del representante de la compañía</p>
                <ul class="condicions">
                    <li>
                        <div class="filera">
                            <div class="nom">
                                <span class="desc">Fdo:</span><br>
                            </div>
                            %if firmante:
                              <span class="filled-text">
                                ${firmante}
                              </span>
                            %else:
                               <div class="linia"></div>
                            %endif
                        </div>
                    </li>
                    <li>
                        <div class="filera">
                            <div class="nom">
                                <span class="desc">DNI:</span><br>
                            </div>
                            %if niffirmante:
                              <span class="filled-text">
                                ${niffirmante}
                              </span>
                            %else:
                               <div class="linia"></div>
                            %endif
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col-xs-3"></div>
            <div class="col-xs-4">
                <div class="cuadro_firma_titol">Por <span>el cliente</span></div>
                <div class="cuadro_firma"> </div>
            </div>
        </div>
    </div>
</%def>
<html>
  <head>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type">
    <title></title>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="${addons_path}/crm_elec/report/oferta.css"/>
  </head>
  <body>
    %for oferta in objects:
    <!-- Recuperar les dades de crm_polissa -->
    <% dades = oferta.get_info_oferta(oferta.id) %>
    <%
        single_price = oferta.llista_preu.single_price()
        # dades persona firmant segons si és física o jurídica
        if oferta.cliente_tipo_persona == 'fisica':
            firmante = oferta.cliente_nombre or ''
            firmante += ' '
            firmante += oferta.cliente_apellidos or ''
            niffirmante = oferta.cliente_vat
        else:
            firmante = ''
            niffirmante = ''
            if oferta.pers_firmante_id:
                firmante = oferta.pers_firmante_id.name.replace('\r\n','').replace('\n','') or ''
                niffirmante = oferta.pers_firmante_id.vat
        # Comprovem si hi ha descompte
        descompte_energia = dades['descompte_energia']
        descompte_potencia = dades['descompte_potencia']

        # Per quants mesos ha d'apareixer aplicable el descompte
        num_meses_energia = 12
        num_meses_potencia = 12
        if 'HUESCA 50' in oferta.llista_preu.name and '2.0' in oferta.tarifa_oferta.name:
            num_meses_potencia = 3
        if 'HUESCA 50' in oferta.llista_preu.name and '2.1' in oferta.tarifa_oferta.name:
            num_meses_potencia = 2

        condicion_detalles = not single_price or (single_price and 'DH' not in oferta.tarifa_oferta.name.upper())
    %>

    <div class="container">
      <div class="row">
        <div class="col-xs-12 width100">
            <table>
              <tr>
                <td>
                  <div class="titol">
                    OFERTA DE SUMINISTRO DE ELECTRICIDAD
                    <br/>
                    % if oferta.mode_facturacio != 'index':
                        A PRECIO FIJO EN
                        % if oferta.tarifa_oferta.codi_ocsum in ['001', '003', '004', '005', '006', '007', '008']:
                            ${_(" BAJA TENSIÓN")}
                        % else:
                            ${_(" ALTA TENSIÓN")}
                        % endif
                    % else:
                        INDEXADO
                    % endif
                  </div>
                </td>
                <td rowspan="2">
                  <div class="logo">
                    <img src="data:image/jpeg;base64,${company.logo}"/>
                  </div>
                </td>
              </tr>
              <tr>
                <td></td>
              </tr>
            </table>
        </div>
      </div>
      % if 'numero_ctto' in dades:
          <div class="row no_padding">RENOVACIÓN CONTRATO: <b>${dades['numero_ctto']}</b></div>
      % endif
      %if "6." not in oferta.tarifa_oferta.name and not (descompte_energia or descompte_potencia):
          <div class="pd2"></div>
      %endif
      <div class="row">
        <div class="col-xs-6 no-gutter no_padding">
            <div class="izda">
                <div class="titol_datos_border_left datos_cliente border_azul">Datos cliente</div>
                <div class="white_border"></div>
                <div class="cuadro_datos_azul">
                    <b>Cliente:</b> ${oferta.cliente_nombre + ' ' if oferta.cliente_nombre else ''}${oferta.cliente_apellidos or ''}
                    </br><b>CIF/NIF:</b> ${oferta.cliente_vat or ''}
                    </br><b>Dir. Suministro:</b> ${oferta.cups_tv.name + ' ' if oferta.cups_tv.name else ''}${oferta.cups_nv + ' ' if oferta.cups_nv else ''}${oferta.cups_pnp + ' ' if oferta.cups_pnp else ''}${oferta.cups_es + ' ' if oferta.cups_es else ''}${oferta.cups_pt + ' ' if oferta.cups_pt else ''}${oferta.cups_pu + ' ' if oferta.cups_pu else ''}${oferta.cups_cpo + ' ' if oferta.cups_cpo else ''}${oferta.cups_cpa + ' ' if oferta.cups_cpa else ''}${oferta.cups_aclarador or ''}
                    </br><b>Código postal:</b> ${oferta.cups_dp or ''}
                    </br><b>Población:</b> ${oferta.cups_id_poblacio.name}
                    </br><b>Fecha estimada de inicio: ${oferta.fecha_estimada_inicio_ctto if oferta.fecha_estimada_inicio_ctto else ''}</b>
                </div>
            </div>
        </div>
        <div class="col-xs-6 no-gutter no_padding">
            <div class="dcha">
                <div class="titol_datos_border_left datos_suministro border_azul">Datos Suministro</div>
                <div class="white_border"></div>
                <div class="cuadro_datos_azul">
                    <b>CUPS:</b>${oferta.cups_sips or oferta.cups_name}
                    <div class="centered">
                        <table>
                        <!-- taula dinàmica segons si són 6/3/2/1 periodes -->
                        <!--############ 3 o menys periodes ############### -->
                        <tr>
                        %if len(dades['nom_periodes_potencia_oferta']) <= 3:
                          <!--#### primer fem les tablecells dels periodes que tinguem -->
                          %for i in range(len(dades['nom_periodes_potencia_oferta'])):
                            <% periode = dades['nom_periodes_potencia_oferta'][i] %>
                            <td class="titol_potencies"><b>${periode}</b></td><td class="potencia_cttada">${dades['potencies_contractades'][periode]} kW</td>
                          %endfor
                          <!--###### després omplim amb espais fins a 3 perquè no es moguin #####-->
                          <% resta = 3 - len(dades['nom_periodes_potencia_oferta']) %>
                          %for i in range(0,resta):
                            <td class="titol_potencies"> </b></td><td class="potencia_cttada"> </td>
                          %endfor
                        </tr>
                        <!--############ 6 periodes ############### -->
                        %else:
                          <tr> <!-- P1 P2 P3 -->
                          %for i in range(3):
                            <% periode = dades['nom_periodes_potencia_oferta'][i] %>
                            <td class="titol_potencies"><b>${periode}</b></td><td class="potencia_cttada">${dades['potencies_contractades'][periode]}</td>
                          %endfor
                          </tr>
                          <tr> <!-- P4 P5 P6 -->
                          %for i in range(3,6):
                            <% periode = dades['nom_periodes_potencia_oferta'][i] %>
                            <td class="titol_potencies"><b>${periode}</b></td><td class="potencia_cttada">${dades['potencies_contractades'][periode]}</td>
                          %endfor
                          </tr>
                        %endif
                        </table>
                    </div>
                    %if oferta.consumo_anual_previsto_directo > 0:
                        <b>Consumo anual estimado (kWh)</b>: ${oferta.consumo_anual_previsto_directo}
                    %else:
                        <b>Consumo anual estimado (kWh)</b>: ${oferta.consumo_anual_previsto}
                    %endif
                </div>
            </div>
        </div>
      </div>
      %if "6." in oferta.tarifa_oferta.name and (descompte_energia or descompte_potencia):
          <div class="pd4"></div>
      %else:
          <div class="pd2"></div>
      %endif
      <div class="row">
        <div class="col-xs-6 no-gutter">
          <div class="row">
            <div class="col-xs-4">
                <div class="tarifa_fecha_titol">Tarifa</div>
                <div class="cuadro_tarifa_fecha">
                    <div class="tarifa">${oferta.tarifa_oferta.name}</div>
                </div>
            </div>
            <div class="col-xs-4"></div>
            <div class="col-xs-4">
                <div class="tarifa_fecha_titol">Fecha oferta</div>
                <div class="cuadro_tarifa_fecha">
                    <div class="fecha">${dades['data']}</div>
                </div>
            </div>
          </div>
        </div>
      </div>
      %if "6." in oferta.tarifa_oferta.name and (descompte_energia or descompte_potencia):
          <div class="pd4"></div>
      %else:
          <div class="pd2"></div>
      %endif
      <div class="row">

      <!-- Terme energia i potencia dinàmics, en 2 rows si es una tarifa 6.x o en una si es una tarifa 3 -->
      <!-- En aquest cas hem de posar divs 'float: left' perquè entre les td hi ha la línia verda vertical -->

      <!-- #####################  1/2/3 periodes ############################## -->
        %if len(dades['nom_periodes_energia_oferta']) <= 3 and oferta.mode_facturacio != 'index':
            <!-- ####### terme energia ######## -->
            <div class="col-xs-6 no_padding">
              <div class="izda">
                  <div class="titol_datos_border_left border_verde">Término de energía</div>
                  <div class="white_border"></div>
                  <div class="cuadro_datos_verde centered">
                      <%
                          periodes = dades['nom_periodes_energia_oferta']
                          if single_price:
                            old_periodes = periodes
                            periodes = [periodes[0]]
                      %>
                      <table class="igual_altura_energies_potencies">
                        <tr>
                          <div class="table_e_p">
                              %for periode in periodes:
                                %if single_price:
                                    <td class="table_headers">${u' / '.join(old_periodes)}</td>
                                %else:
                                    <td class="table_headers">${periode}</td>
                                %endif
                              %endfor
                          </div>
                        </tr>
                        <tr>
                            <div class="table_e_p">
                                %for periode in periodes:
                                    <td>${dades['preus_energia_pvp'][periode]} €/kWh</td>
                                %endfor
                            </div>
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                        </tr>
                      </table>
                      %if descompte_energia:
                          <div class="azul descompte_energia"><b>Descuento ${formatLang(descompte_energia, digits=1)}%</b>
                          %if oferta.llista_preu.variable_discount:
                             <span sytle="font-size:10px;">
                                <br>El descuento a aplicar será del ${formatLang(oferta.llista_preu.conditional_discount, digits=1)}% para
                                       aquellos periodos de facturación cuyo peso de consumo en valle sea
                                       superior al ${formatLang(oferta.llista_preu.p2_precent, digits=1)}%.
                             </span>
                          %endif
                          </div>
                      %else:
                        %if descompte_potencia:
                            <tr>
                            <td colspan="2">&nbsp;</td>
                            </tr>
                        %endif
                      %endif
                  </div>
              </div>
            </div>
            <!-- ####### terme potencia ######## -->
            <div class="col-xs-6 no_padding">
              <div class="dcha">
                  <div class="titol_datos_border_left border_verde">Término de potencia</div>
                  <div class="white_border"></div>
                  <div class="cuadro_datos_verde centered">
                      <table class="igual_altura_energies_potencies">
                        <tr>
                          <div class="table_e_p">
                              %for periode in dades['nom_periodes_potencia_oferta']:
                                <td class="table_headers">${periode}</td>
                              %endfor
                          </div>
                        </tr>
                        <tr>
                          <div class="table_e_p">
                              %for periode in dades['nom_periodes_potencia_oferta']:
                                <td class="table_data_e_p">${dades['preu_pot_cttades'][periode]} €/kW año</td>
                              %endfor
                          </div>
                        </tr>
                        <tr>
                            <div class="table_e_p">
                            <% dies = 365 %>
                            %if calendar.isleap(datetime.today().year):
                              <% dies = 366 %>
                            %endif
                              %for periode in dades['nom_periodes_potencia_oferta']:
                                <% pot_per_dia = "{0:.6f}".format(float(dades['preu_pot_cttades'][periode]) / dies ) %>
                                <td>(${pot_per_dia} €/kW dia)</td>
                              %endfor
                            %if oferta.llista_preu.variable_discount:
                               <br><br>
                            %endif
                            </div>
                        </tr>
                      </table>
                      %if descompte_potencia:
                          <div class="azul descompte_energia"><b>Descuento ${formatLang(descompte_potencia, digits=1)}%</b>
                          </div>
                      %else:
                        % if descompte_energia:
                            <div>&nbsp;</div>
                        % endif:
                      %endif
                  </div>
              </div>
            </div>
        %else:
<!-- ######################### 6 periodes ######################### -->
            %if oferta.mode_facturacio != 'index':
                <!-- ####### terme energia ######## -->
                <div class="col-xs-12 no_padding">
                  <div class="titol_datos_border_left border_verde">Término de energía</div>
                  <div class="white_border"></div>
                  <div class="cuadro_datos_verde centered">
                      <table>
                        <tr>
                          <div class="table_e_p">

                              %for periode in dades['nom_periodes_energia_oferta']:
                                  <td class="table_headers">${periode}</td>
                              %endfor
                          </div>
                        </tr>
                        <tr>
                          <div class="table_e_p">
                              %for periode in dades['nom_periodes_energia_oferta']:
                                <td>${dades['preus_energia_pvp'][periode]} €/kWh</td>
                              %endfor
                          </div>
                        </tr>
                      </table>
                  </div>
                </div>
            <div class="pd2"></div>
            %endif
            <!-- ####### terme potencia ######## -->
            <div class="col-xs-12 no_padding">
              <div class="titol_datos_border_left border_verde">Término de potencia</div>
              <div class="white_border"></div>
              <div class="cuadro_datos_verde centered">
                  <table>
                    <tr>
                        <div class="table_e_p">

                          %for periode in dades['nom_periodes_potencia_oferta']:
                            <td class="table_headers">${periode}</td>
                          %endfor
                        </div>
                    </tr>
                    <tr>
                        <div class="table_e_p">
                          %for periode in dades['nom_periodes_potencia_oferta']:
                            <td>${dades['preu_pot_cttades'][periode]} €/kW año</td>
                          %endfor
                        </div>
                    </tr>
                    <tr>
                        <div class="table_e_p">
                          %for periode in dades['nom_periodes_potencia_oferta']:
                            <% pot_per_dia = "{0:.6f}".format(float(dades['preu_pot_cttades'][periode]) / 365 ) %>
                            <td>(${pot_per_dia} €/kW dia)</td>
                          %endfor
                        </div>
                    </tr>
                  </table>
                  % if descompte_potencia:
                    <div class="azul descompte_energia"><b>Descuento ${formatLang(descompte_potencia, digits=1)}%</b>
                    aplicable durante ${num_meses_potencia} meses a partir de la fecha de inicio del contrato.
                  </div>
                  % endif
              </div>
            </div>
        %endif
      </div>

      % if oferta.mode_facturacio != 'index':
          <!-- ###################### DESGLOSE OFERTA ##################### -->
          %if "6." in oferta.tarifa_oferta.name or (descompte_energia or descompte_potencia):
              <div class="pd4"></div>
          %else:
              <div class="pd1"></div>
          %endif
          %if condicion_detalles:
              <div class="row">
                <div class="col-xs-12 no_padding">
                  <div class="titol_datos_border_left border_verde">Desglose oferta</div>
                  <div class="white_border"></div>
                  <div class="cuadro_datos_verde centered">
                    <div class="width100">
                      <div class="transparencia_60">
                      <table>
                        <tr>
                          <div class="desglose_float_left">
                              <td class="desglose_1_col"></td>
                              %for periode in dades['nom_periodes_energia_oferta']:
                                <td class="desglose_headers desglose_cols">${periode}</td>
                              %endfor
                          </div>
                        </tr>
                        <tr>
                          <div class="desglose_float_left">
                              <td class="desglose_1_col"><b>Peajes vigentes €/kWh</b></td>
                              %for periode in dades['nom_periodes_energia_oferta']:
                                <td class="desglose_cols">${dades['preu_peatges_energia'][periode]}</td>
                              %endfor
                          </div>
                        </tr>
                        <tr>
                          <div class="desglose_float_left">
                              <td class="desglose_1_col"><b>Solo energía €/kWh</b></td>
                              %for periode in dades['nom_periodes_energia_oferta']:
                                <td class="desglose_cols">${dades['nomes_energia'][periode]}</td>
                              %endfor
                          </div>
                        </tr>
                      </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          %else:
              %if (descompte_energia or descompte_potencia) and single_price and oferta.mode_facturacio != 'index':
                  <!-- ### Mostrem el quadre de les condicions de la oferta ### -->
                  <div class="pd4"></div>
                    <div class="row">
                      <div class="col-xs-12 no_padding">
                          <div class="titol_datos_border_left border_verde">Condiciones oferta</div>
                          <div class="white_border"></div>
                          <div class="cuadro_datos_verde azul">
                              <p>
                                  % if descompte_energia:
                                    Descuento del ${formatLang(descompte_energia, 1)}% en el término de energía aplicable durante ${num_meses_energia} meses a partir de la fecha de inicio del contrato.
                                  % endif
                                  % if descompte_potencia:
                                    </br>
                                    Descuento del ${formatLang(descompte_potencia, 1)}% en el término de potencia aplicable durante ${num_meses_potencia} meses a partir de la fecha de inicio del contrato.
                                  % endif
                                  %if oferta.llista_preu.variable_discount:
                                     <br>El descuento a aplicar será del ${formatLang(oferta.llista_preu.conditional_discount, digits=1)}% para
                                               aquellos periodos de facturación cuyo peso de consumo en valle sea
                                               superior al ${formatLang(oferta.llista_preu.p2_precent, digits=1)}%.
                                  %endif
                              </p>
                          </div>
                      </div>
                    </div>
                  <div style="clear: both;"></div>
              %elif oferta.mode_facturacio == 'index':
                  <div class="pd4"></div>
              %else:
                  <div class="empty_desglose"></div>
              %endif
          %endif
          <!-- ############## END desglose ################## -->
      %else:
          <!-- ############## Precio de la energía ################## -->
          <div class="pd2"></div>
          <div class="row">
              <div class="col-xs-12 no_padding">
                  <div class="titol_datos_border_left border_verde">Precio de la energía</div>
                  <div class="white_border"></div>
                  <div class="cuadro_datos_verde azul">
                      <p><b>Tarifa indexada</b><br>
                          La tarifa indexada tiene como objetivo facturar según el
                          coste de la energía cada hora y todos los gastos asociados
                          a la comercialización de energía eléctrica según la formula
                          siguiente:
                      </p>
                      <p>
                          <b>PHF = IMU * [(PMD + POS + SI + PC3 + OMIE_REE) * PERD + K + D + FONDO_EFI] + PA</b>
                      </p>
                      <p class="font_9px">
                          PHF: Precio horario final. IMU: Impuesto municipal. PMD: Precio mercado diario. POS:
                          Costes y servicios del sistema. SI: Servicio de interrumpibilidad. PC3: Pagos por capacidad.
                          OMIE REE: Remuneración del operador del sistema y del operador de mercado. PERD: Perdidas de
                          distribución. K: Costes de gestión. D: Desvíos. FONDO_EFI: Coste destinado al fondo
                          de eficiencia. PA: Peajes de acceso a la red.
                      </p>
                      <p class="no_margin_bottom">
                          <table class="coeficients">
                              <tr>
                                  <td class="no-bold"><b>K:</b> Costes de gestión</td>
                                  <td class="no-bold">${formatLang(oferta.coeficient_k / 1000.0, 5)}</td>
                                  <td class="no-bold" style="width: 60px;">€/Kwh</td>
                              </tr>
                          </table>
                      </p>
                  </div>
              </div>
          </div>
          <!-- ############## END precio energía ################## -->
      %endif
      %if (descompte_energia or descompte_potencia) and condicion_detalles and oferta.mode_facturacio != 'index':
          <!-- ### Mostrem el quadre de les condicions de la oferta ### -->
          <div class="pd4"></div>
            <div class="row">
              <div class="col-xs-12 no_padding">
                  <div class="titol_datos_border_left border_verde">Condiciones oferta</div>
                  <div class="white_border"></div>
                  <div class="cuadro_datos_verde azul">
                      <p>
                          % if descompte_energia:
                            Descuento del ${formatLang(descompte_energia, 1)}% en el término de energía  aplicable durante ${num_meses_energia} meses a partir de la fecha de inicio del contrato.
                          % endif
                          % if descompte_potencia:
                            </br>
                            Descuento del ${formatLang(descompte_potencia, 1)}% en el término de potencia  aplicable durante ${num_meses_potencia} meses a partir de la fecha de inicio del contrato.
                          % endif
                          %if oferta.llista_preu.variable_discount:
                             <br>El descuento a aplicar será del ${formatLang(oferta.llista_preu.conditional_discount, digits=1)}% para
                                       aquellos periodos de facturación cuyo peso de consumo en valle sea
                                       superior al ${formatLang(oferta.llista_preu.p2_precent, digits=1)}%.
                          %endif
                      </p>
                  </div>
              </div>
            </div>
          <div style="clear: both;"></div>
       %elif oferta.mode_facturacio == 'index':
           <div class="pd4"></div>
       %else:
              <div class="empty_desglose"></div>

      %endif
      %if "6." in oferta.tarifa_oferta.name or (descompte_energia or descompte_potencia):
          <div class="pd4"></div>
      %else:
          <div class="pd1"></div>
      %endif
      <!-- ############## Te podemos ayudar? ################### -->
      <div class="row">
        <div class="col-xs-6 no-gutter no_padding">
            <div class="validez">
              Aceptación de la oferta
            </div>
            <div class="info_legal">
                <ul>
                    <li>
                        Días de validez de la oferta: ${dades['dias_validez']}
                    </li>
                    <li>
                        Se repercutirán las variaciones a la baja o al alza en las tarifas de acceso, los valores regulados que pueden ser aprobados por la Administración para su aplicación durante la duración del contrato, y los aumentos significativos del precio de compra imputable a alteraciones no previstas en el mercado eléctrico.
                    </li>
                    <li>
                        <b>Ab energía 1903, S.L.U.</b> se reserva la posibilidad de solicitar garantías financieras para proceder a la activación del punto de suministro.
                    </li>
                    <li>
                        Los precios incluyen todas las tasas excepto el IVA. y el Impuesto Eléctrico. Puede consultar el detalle de la oferta en el desglose de la oferta. Oferta válida para España peninsular.
                    </li>
                    <li>
                        La oferta ha sido calculada a partir de los datos suministrados por el cliente, referentes a su consumo eléctrico.
                    </li>
                    <li>
                        Los excesos de potencia se facturarán de acuerdo con el artículo 9 apartado 1.1 del RD1164/2001
                    </li>
                    %if report_name == 'crm_elec_oferta_vinculant':
                        <li>
                            La presente oferta tiene carácter vinculante, y las condiciones en ella reflejadas serán contenidas en el contrato que le será remitido por Ab energía 1903, S.L.U. tan pronto el Cliente facilite a Ab energía 1903, S.L.U. la documentación previa correspondiente (especificada previamente en la presente oferta).
                        </li>
                    %endif
                    <li>
                        *La activación del punto de suministro con <b>ab energía 1903, S.L.U.</b> es concedida por la empresa distribuidora de la zona, quien lo notificará a <b>ab energía 1903, S.L.U.</b>.
                    </li>
                </ul>
                %if report_name == 'crm_elec_oferta_vinculant':
                    <div class="checkbox">
                        <div class="box">&nbsp;</div>
                        <div class="checkbox_text">
                            Acepto que la firma y formalización del contrato se realice de forma telemática mediante el proceso de firma digital.
                        </div>
                    </div>
                %endif
            </div>
        </div>
        <div class="col-xs-6 no-gutter no_padding">
            <div class="titol_datos_border_left border_azul">¿Te podemos ayudar?</div>
            <div class="white_border"></div>
            <div class="cuadro_datos_azul" style="height: 80px !important;">
                <b>Tu asesor comercial:</b> ${dades['nom_comercial']}
                </br><b>E mail:</b> ${dades['work_email']}
                </br><b>Teléfono:</b> ${dades['mobile_phone']}
                </br><b>Dirección:</b> ${dades['office_address']}
            </div>
            <!-- FIRMA -->
            %if (descompte_energia or descompte_potencia):
                <div class="pd4"></div>
            %else:
                <div class="pd1"></div>
            %endif
            <div class="col-xs-6 padding_right"  style="padding-right: 7px;" >
                <div class="cuadro_firma_titol">Por <span>el cliente</span></div>
                <div class="cuadro_firma_azul"> </div>
            </div>
            <div class="col-xs-6 padding_left"  style="padding-left:7px;" >
                <div class="cuadro_firma_titol">Por <span>ab energía</span></div>
                <div class="cuadro_firma_verde">
                    <center>
                        <img class="img_firma" src="${addons_path}/crm_elec/report/img/firmaDireccion.png"></img>
                    </center>
                </div>
            </div>
            <div class="pie_firma">
                <div class="col-xs-6 no-gutter no_padding margin_top_5">
                    <ul class="firma_dades width-250">
                        <li>
                            <div class="column width-240">
                                <div class="filera">
                                  %if firmante:
                                       Fdo:
                                       <span class="filled-text">
                                         ${firmante}
                                       </span>
                                  %else:
                                   <div class="nom_peu_firma">
                                       Fdo:
                                   </div>
                                   <div class="linia">
                                   </div>
                                  %endif
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="column width-190">
                                <div class="filera">
                                  %if niffirmante:
                                   DNI:
                                   <span class="filled-text">
                                       ${niffirmante}
                                   </span>
                                  %else:
                                   <div class="nom_peu_firma">
                                       DNI:
                                   </div>
                                   <div class="linia"></div>
                                  %endif
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="column width-80">
                                <div class="filera width-80">
                                    <div class="nom_peu_firma">
                                        En
                                    </div>
                                    <div class="linia width-80"></div>
                                </div>
                            </div>
                            <div class="column width-20">
                                <div class="filera">
                                    <div class="nom_peu_firma">
                                        a:
                                    </div>
                                    <div class="linia"></div>
                                </div>
                            </div>
                            <div class="column width-80">
                                <div class="filera width-80">
                                    <div class="nom_peu_firma">
                                        de:
                                    </div>
                                    <div class="linia width-80"></div>
                                </div>
                            </div>
                            <div class="column width-35">
                                <div class="filera">
                                    <div class="nom_peu_firma">
                                        de ${datetime.now().strftime('%Y')}
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
      </div>
      <!-- ############## END Te podemos ayudar? ###################-->
      %if (descompte_energia or descompte_potencia):
          <div class="pd4"></div>
      %else:
          <div class="pd1"></div>
      %endif
      <div class="row">
          <div class="col-xs-6 no-padding">
          </div>
        <div class="col-xs-6 no_padding">

        </div>
        %if report_name == 'crm_elec_oferta_vinculant':
            <div style="clear: both"></div>
            <p style="page-break-after:always"></p>
            <!-- Part posterior -->
            <div class="col-xs-6 no-gutter no_padding documentacion_container w_100">
                <div class="titol_datos_border_left border_azul">Documentación</div>
                <div class="white_border"></div>
                <div class="cuadro_datos_azul h_auto">
                    <%
                        docs = [
                            ('NIF del titular del punto de suministro.', oferta.doc_requerida_nif),
                            ('Certificado de titularidad de la cuenta bancaria.', oferta.doc_requerida_certificado_titularidad_cuenta),
                            ('Certificado de propiedad o contrato de alquiler de la localización del punto de suministro.', oferta.doc_requerida_certificado_propiedad_o_alquiler),
                            ('Referencia catastral de la localización del punto de suministro.', oferta.doc_requerida_referencia_catastral),
                            ('Tarjeta CIF de la empresa.', oferta.doc_requerida_tarjeta_cif_empresa),
                            ('Autorización de empresa para la contratación del suministro (en papel corporativo).', oferta.doc_requerida_autorizacion_contratacion_empresa),
                            ('Código de la actividad que se realizará (CNAE).', oferta.doc_requerida_cnae),
                            ('Copia del acta de la comunidad de propietarios aprobando la contratación.', oferta.doc_requerida_copia_acta_comunidad),
                            ('Autorización o copia de poderes por parte del titular para gestiones sobre el punto de suministro.', oferta.doc_requerida_autorizacion_poderes),
                            ('Cédula de habitabilidad o licencia de puesta en marcha de la actividad.', oferta.doc_requerida_cedula_habitabilidad),
                            ('Documentación técnica eléctrica en base a su tensión (BIE, APM, ELEC-6... etc).', oferta.doc_requerida_doc_tecnica)
                        ]
                    %>
                    % for doc in docs:
                        <div class="checkbox_docu">
                            <div class="box_docu">
                                <span class="check_mark">
                                    % if doc[1]:
                                        ✔
                                    % else:
                                        &nbsp;
                                    % endif
                                </span>
                            </div>
                            <div class="checkbox_text_docu">
                                ${doc[0]}
                            </div>
                        </div>
                        <div style="clear: both"></div>
                    % endfor
                    <br>
                    <div id="observaciones">
                        Observaciones:
                    </div>
                </div>
            </div>
            <!--<div class="num_oferta" style="margin-top: 7px;">Oferta ${oferta.name}</div>-->
        %endif
      </div>
    </div>
  %endfor
  </body>
</html>
