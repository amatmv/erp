<%
    from datetime import datetime, date
    from dateutil.relativedelta import relativedelta
    from operator import attrgetter, itemgetter
    from addons.crm_elec.crm_polissa import get_consums_anuals_periode
    from addons.crm_elec.crm_polissa import get_info_oferta
%>



<html>
  <head>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type">
    <title></title>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="${addons_path}/crm_elec/report/oferta.css"/>
  </head>
  <body>
    %for oferta in objects:
    <% fecha_hoy = date.today()%>


    <% tp_obj = oferta.pool.get('giscedata.tp') %>
    <% llista_preus_peatges_energia = [tp_obj.get_peatge_price_from_period_id(oferta._cr, oferta._uid, periode.id)
                                    for periode in oferta.tarifa_oferta.periodes
                                     if periode.tipus == 'te'
                                     and not periode.agrupat_amb
                                  ]%>

    <div class="container">
      <div class="row row_first">
          <div id="id_logo" class="col-xs-4">
            <div class="logo">
            </div>
          </div>
          <div id="id_titol" class="col-xs-8">
            <div class="titol">
              OFERTA DE SUMINISTRO DE ELECTRICIDAD
            </div>
          </div>
      </div>
      <div class="row row_intermedia">
        <div class="col-xs-12">
          <div class="frase1">
            En ab energía hemos valorado tus necesidades y a continuación <b>te presentamos una oferta personalizada que se adapta a tu suministro eléctrico</b>, siempre con las mejores condiciones y el mejor servicio.
            </div>
          </div>
      </div>
      <div class="row row_intermedia col-xs-12">
        <div class="col-xs-2">
          <div class="desc_oferta">
          <div class="tarifa_oferta">Oferta<br/>Tarifa ${oferta.tarifa_oferta.name}</div>
          <div class="fecha_oferta">Fecha Oferta<br/>${fecha_hoy}</div>
          </div>
        </div>
        <div class="col-xs-5">
          <div class="termino_energia centered">
            <div class="titol_taula_energia_potencia">
              Término de Energía
            </div>
            <div class="container_taula">
              <table>
                <% llista_periodes = [] %>
                %for periode in oferta.oferta_energies_periode:
                  <% llista_periodes.append(periode.periode) %>
                  <tr><td>${periode.periode}</td><td>${periode.preu}</td><td>€/KWh</td></tr>
                %endfor
              </table>
            </div>
          </div>
        </div>
        <div class="col-xs-5">
          <div class="termino_potencia centered">
            <div class="titol_taula_energia_potencia">
              Término de Potencia
            </div>
            <div class="container_taula">
              <table>
                %for periode in oferta.oferta_potencies_periode:
                <tr><td>${periode.periode}</td><td>${periode.preu}</td><td>€/KW año</td></tr>
                %endfor
              </table>
            </div>
          </div>
        </div>
      </div>

      <div class="row row_intermedia">
        <div class="col-xs-3 por_que">¿Por qué ab energía?</div>
      </div>
      <div class="row">
        <div class="col-xs-1"></div>
        <div class="col-xs-11 centered">
          <div class="beneficios">
            <table>
              <tr><td>Somos transparentes en nuestros precios.</td><td>Disponemos de un teléfono de atención gratuito.</td></tr>
              <tr><td>Tenemos unos precios muy competitivos.</td><td>Te ayudamos en cualquier duda referente a tu suministro.</td></tr>
              <tr><td>Puedes venir a tu oficina más cercana.</td><td>Una web llena de contenido en www.abenergia.es.</td></tr>
            </table>
          </div>
        </div>
      </div>
      <div class="row row_intermedia">
        <div class="col-xs-2"></div>
        <div class="col-xs-8 frase_exclamacion">¡Siempre estamos a tu lado para asesorarte en lo que necesites!</div>
      </div>
      <div class="row row_intermedia">
        <div class="col-xs-4 titol_desglose">Desglose términos oferta</div>
        <div class="col-xs-12">
          <div class="tabla_desglose_oferta cuadro_datos">
            <table>

              <tr> <!-- row de titols-->
                <td></td><!-- Primera cel.la buida -->
                <!-- Iterar els Periodes de la tarifa que estem ofertant-->
                %for periode in oferta.oferta_potencies_periode:
                  <td>${periode.periode}</td>
                %endfor
              </tr>

              <tr>
              <td><b>Coste con IEE (€/KWh)</b></td>
                %for pos in range(len(oferta.oferta_energies_periode)):
                  <% preu = oferta.oferta_energies_periode[pos].preu * 1.051127 %>
                  <% preu_arrodonit = "{:10.5f}".format(preu) %>
                  <td>${preu_arrodonit}</td>
                %endfor
              </tr>

              <tr>
              <td><b>Peajes vigentes (€/KWh)</b></td>
                %for pos in range(len(llista_preus_peatges_energia)):
                  <% preu = llista_preus_peatges_energia[pos]%>
                  <% preu_arrodonit = "{:10.5f}".format(preu) %>
                  <td>${preu_arrodonit}</td>
                %endfor
              </tr>

              <tr>
              <td><b>Coste SOLO Energía</b></td>
                <!-- Energia menys peajes i sense IEE-->
                %for pos in range(len(oferta.oferta_energies_periode)):
                  <% preu = oferta.oferta_energies_periode[pos].preu - llista_preus_peatges_energia[pos]%>
                  <% preu_arrodonit = "{:10.5f}".format(preu) %>
                  <td>${preu_arrodonit}</td>
                %endfor
              </tr>

              <tr>
              <td><b>Coste SOLO Energía con IEE</b></td>
                <!-- Energia menys peajes però multiplicat el IEE-->
                %for pos in range(len(oferta.oferta_energies_periode)):
                  <% preu = oferta.oferta_energies_periode[pos].preu - llista_preus_peatges_energia[pos]%>
                  <% preu_amb_iee = preu * 1.051127 %>
                  <% preu_arrodonit = "{:10.5f}".format(preu_amb_iee) %>
                  <td>${preu_arrodonit}</td>
                %endfor
              </tr>

            </table>
          </div>
        </div>
      </div>

      <div class="row row_intermedia">
        <div class="col-xs-6 izda no-gutter">
          <div class="col-xs-12">
            <div class="titol_datos">Datos cliente</div>
            <div class="cuadro_datos centered">
              <table>
                <tr><td>NOMBRE:</td><td>${oferta.cliente_nombre} ${oferta.cliente_apellidos}</td></tr>
                <tr><td>${oferta.cliente_tipo_documento}:</td><td>asdasdsa</td></tr>
                <tr><td>CUPS:</td><td>${oferta.cups_sips}</td></tr>
                <tr><td>DIR. SUMINISTRO:</td><td>${oferta.cups_tv.abr}${oferta.cups_nv}${oferta.cups_pnp}${oferta.cups_es}${oferta.cups_pt}${oferta.cups_pu}${oferta.cups_cpo}${oferta.cups_cpa}</td></tr>
                <tr><td>CÓDIGO POSTAL:</td><td>${oferta.cups_dp}</td></tr>
                <tr><td>POBLACIÓN:</td><td>${oferta.cups_id_poblacio.name}</td></tr>
                <tr><td>FECHA DE INICIO:</td><td>asdasdsa</td></tr>
              </table>
            </div>
          </div>
          <div class="col-xs-12">
            <div class="comentarios">
              <div class="titol_datos">Comentarios</div>
              <div class="cuadro_datos"></div>
            </div>
          </div>
        </div>
        <div class="col-xs-6 dcha">
          <div class="titol_datos col-xs-12">¿Te podemos ayudar?</div>
          <div class="cuadro_datos col-xs-12">
            <table>
              <tr><td>Asesor Comercial:</td><td></td></tr>
              <tr><td>E mail:</td><td>email@email.com</td></tr>
              <tr><td>Teléfono:</td><td>123456789</td></tr>
            </table>
          </div>
          <div class="consumos col-xs-12">
            <table>
              <tr><td colspan="2">POTENCIA CONTRATADA</td><td colspan="2">CONSUMO ANUAL ESTIMADO</td></tr>
              <tr><td>P1</td><td>asdasdasd</td><td>P1</td><td>asdasdasd</td></tr>
              <tr><td>P2</td><td>adasdadasdasd</td><td>P2</td><td>asdasdasd</td></tr>
              <tr><td>P3</td><td>asdasdasd</td><td>P3</td><td>asdasdasd</td></tr>
            </table>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="info_legal">
          Se repercutirán las variaciones a la baja o al alza en las tarifas de acceso, los valores regulados que pueden ser aprobados por la Administración para su aplicación durante la duración del contrato (y los aumentos significativos del precio de compra imputable a alteraciones no previstas en el mercado eléctrico).
          <br/>Incluido: Peajes de acceso vigentes (Orden IET/107/2014 de 1 de febrero de 2014), el precio de la Energía a suministrar y las pérdidas de transporte.
          <br/>Los precios incluyen todas las tasas excepto el I.V.A. y el Impuesto Eléctrico. Puede consultar el detalle de la oferta en el desglose de la oferta.
          <br/>La oferta ha sido calculada a partir de los datos suministrados por el cliente, referentes a su consumo eléctrico.
          <br/>Los excesos de potencia se facturarán de acuerdo con el artículo 9 apartado 1.1 del RD1164/2001
          <br/>*La activación del punto de suministro con <b>ab energia</b> es concedida por la empresa distribuidora de la zona, quien lo notificará a <b>ab energia</b>.
          <br/>Días de valodez de la oferta: 15.
          <br/>Oferta válida para España peninsular, salvo indicación expresa.
        </div>
      </div>
    </div>
    %endfor
  </body>
</html>