import tempfile
import pypdftk
import base64

from report import report_sxw
from c2c_webkit_report import webkit_report
from tools import config
import pooler


class report_webkit_html(report_sxw.rml_parse):
    def __init__(self, cursor, uid, name, context):
        super(report_webkit_html, self).__init__(cursor, uid, name,
                                                 context=context)
        self.localcontext.update({
            'cursor': cursor,
            'uid': uid,
            'addons_path': config['addons_path'],
            'report_name': name,
        })

class ReportOfertaCondicionsGenerals(webkit_report.WebKitParser):

    def create(self, cursor, uid, ids, datas, context=None):
        """Create reports and join.

        :param cursor: Database cursor
        :param uid: User identifier
        :param ids: records to print into the report
        :param data: Data passed to report
        :param context: Application context
        :returns a tuple with the type and the content of the report
        """
        if context is None:
            context = {}
        to_join_final = []
        insert_condicions_generals = context.get('insert_condicions_generals', True)
        pool = pooler.get_pool(cursor.dbname)
        crm_polissa_obj = pool.get('crm.polissa')
        polissa_date = datas.get('form', {}).get('polissa_date')
        ctx = context.copy()
        if polissa_date:
            ctx['date'] = polissa_date
        for crm_polissa in crm_polissa_obj.browse(cursor, uid, ids, context=ctx):
            to_join = []
            res = super(ReportOfertaCondicionsGenerals, self).create(
                cursor, uid, [crm_polissa.id], datas, context
            )
            if res[1] != 'pdf':
                continue
            res_path = tempfile.mkstemp('-join.pdf', 'report-')[1]
            with open(res_path, 'w') as f:
                f.write(res[0])
            to_join.append(res_path)
            if crm_polissa.condicions_generals_id and insert_condicions_generals:
                attachment = crm_polissa.condicions_generals_id.attachment_id
                if attachment and attachment.datas:
                    res_path = tempfile.mkstemp('-join.pdf', 'report-')[1]
                    with open(res_path, 'w') as f:
                        f.write(base64.b64decode(attachment.datas))
                    to_join.append(res_path)
            if len(to_join) > 1:
                out = pypdftk.concat(files=to_join)
            else:
                out = res_path
            to_join_final.append(out)
        if len(to_join_final) > 1:
            out = pypdftk.concat(files=to_join_final)
        else:
            out = to_join_final[0]
        with open(out, 'r') as f:
            content = f.read()
        return content, 'pdf'


ReportOfertaCondicionsGenerals(
    'report.crm_elec_oferta',
    'crm.polissa',
    'addons/crm_elec/report/oferta.mako',
    parser=report_webkit_html
)

ReportOfertaCondicionsGenerals(
    'report.crm_elec_oferta_vinculant',
    'crm.polissa',
    'addons/crm_elec/report/oferta.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser('report.crm_elec_contrato',
                           'crm.polissa',
                           'addons/crm_elec/report/contrato.mako',
                           parser=report_webkit_html)

webkit_report.WebKitParser('report.crm_elec_comparativa',
                           'crm.polissa',
                           'addons/crm_elec/report/comparativa.mako',
                           parser=report_webkit_html)

webkit_report.WebKitParser(
    'report.report_crm_elec_polissa_oferta',
    'crm.polissa',
    'giscedata_polissa_comer_abenergia/report/contracte.mako',
    parser=report_webkit_html)

