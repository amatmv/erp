<%
    from datetime import datetime, date
    from dateutil.relativedelta import relativedelta
    from operator import attrgetter, itemgetter
    from addons.crm_elec.crm_polissa import get_consums_anuals_periode, TIPO_RENOVACION, TIPO_FACTURACION
    SINO = {True: 'Si', False:'No'}

    pool = objects[0].pool
    model_obj = pool.get('ir.model.data')
    tarifa_obj = pool.get('product.pricelist')

    model_ids = model_obj.search(cursor, uid,
                                 [('module','=','giscedata_facturacio'),
                                  ('name','=','pricelist_tarifas_electricidad')])
    tarifa_elect_atr = model_obj.read(cursor, uid, model_ids[0],['res_id'])['res_id']

    def get_atr_potencia_price(cursor, uid, pricelist, tarifa, pname):
        pricelist_obj = tarifa.pool.get('product.pricelist')

        product_id = tarifa.get_periodes_producte('tp')[pname]


        price_atr = pricelist_obj.price_get(cursor, uid, [pricelist],
                                            product_id, 1,
                                            oferta.user_id.company_id.partner_id.id,
                                            {'date': date.today()})[pricelist]
        return price_atr
%>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
        <style type="text/css">
            ${css}
        </style>
        <link rel="stylesheet" href="${addons_path}/crm_elec/report/contrato.css"/>
    </head>
    <body>
        %for oferta in objects :
            <% setLang('es_ES') %>
            <h1>Condiciones particulares del Contrato de Suministro de energía Eléctrica a Precio Fijo</h1>

            <h2>DATOS DEL CLIENTE</h2>
<table>
    <tr>
        <th>Razón Social/Nombre:</th><td colspan="5">${oferta.cliente_nombre or ''} ${oferta.cliente_apellidos or ''}</td>
        <th>CIF/NIF:</th><td>${oferta.cliente_vat or ''}</td></tr>
    <tr>
        <th>Dirección:</th><td colspan="5">${oferta.domc_tipovia_id.abr  or ''} ${oferta.domc_street or ''} ${oferta.domc_street2 or ''}</td>
        <th>Nº</th><td>${oferta.domc_numero or ''} ${oferta.domc_escalera or ''} ${oferta.domc_piso or ''} ${oferta.domc_puerta or ''}</td>
    </tr>
    <tr>
        <th>Población:</th><td colspan="2">${oferta.domc_municipi_id.name or ''}</td>
        <th>Código Postal:</th><td>${oferta.domc_cp or ''}</td>
        <th>Provincia:</th><td colspan="2">${oferta.domc_state_id.name or ''}</td>
    </tr>
    <tr>
        <th>Teléfono/s:</th><td>${oferta.domc_telefono_1 or ''}</td>
        <th>Teléfono 2:</th><td colspan="2">${oferta.domc_telefono_2 or ''}</td>
        <th>Fax:</th><td colspan="2">&nbsp;</td>
    </tr>
    <tr>
        <th>E-Mail:</th><td colspan="3">${oferta.domc_email_1 or ''}</td>
        <th>E-Mail 2:</th><td colspan="3">${oferta.domc_email_2 or ''}</td>
    </tr>
</table>
            <h2>DATOS DE NOTIFICACIONES Y ENVÍO DE COMUNICACIÓN</h2>
<table>
    <tr>
        <th>Razón Social/Nombre:</th><td colspan="7">${oferta.contacto_title.name or ''} ${oferta.contacto_nombre or ''} ${oferta.contacto_apellidos or ''}</td>
        <!--<th>CIF/NIF:</th><td>${oferta.cliente_vat or ''}</td>-->
    </tr>
    <tr>
        <th>Dirección:</th><td colspan="5">${oferta.domf_tipovia_id.abr  or ''} ${oferta.domf_street or ''} ${oferta.domf_street2 or ''}</td>
        <th>Nº</th><td>${oferta.domf_numero or ''} ${oferta.domf_escalera or ''} ${oferta.domf_piso or ''} ${oferta.domf_puerta or ''}</td>
    </tr>
    <tr>
        <th>Población:</th><td colspan="2">${oferta.domf_municipi_id.name or ''}</td>
        <th>Código Postal:</th><td>${oferta.domf_cp or ''}</td>
        <th>Provincia:</th><td colspan="2">${oferta.domf_state_id.name or ''}</td>
    </tr>
    <tr>
        <th>Teléfono/s:</th><td>${oferta.domf_telefono_1 or ''}</td>
        <th>Teléfono 2:</th><td colspan="2">${oferta.domf_telefono_2 or ''}</td>
        <th>Fax:</th><td colspan="2">&nbsp;</td>
    </tr>
    <tr>
        <th>E-Mail:</th><td colspan="3">${oferta.domf_email_1 or ''}</td>
        <th>E-Mail 2:</th><td colspan="3">${oferta.domf_email_2 or ''}</td>
    </tr>
</table>
            <h1>CONDICIONES TÉCNICO-ECONÓMICAS</h1>
<h2>DATOS DEL PUNTO DE SUMINISTRO</h2>
<table>
    <tr>
        <th>Dirección:</th><td colspan="6">${oferta.domf_tipovia_id.abr  or ''} ${oferta.domf_street or ''} ${oferta.domf_street2 or ''}</td>
        <th>Nº</th><td class="w50px">${oferta.domf_numero or ''} ${oferta.domf_escalera or ''} ${oferta.domf_piso or ''} ${oferta.domf_puerta or ''}</td>
    </tr>
    <tr>
        <th>Población:</th><td colspan="3">${oferta.domf_municipi_id.name or ''}</td>
        <th>Código Postal:</th><td>${oferta.domf_cp or ''}</td>
        <th>Provincia:</th><td colspan="2">${oferta.domf_state_id.name or ''}</td>
    </tr>
    <tr>
        <th>CUPS:</th><td colspan="2">${oferta.cups_name or ''}</td>
        <th>Ref. Catastral:</th><td colspan="3">${oferta.cups_ref_catastral or ''}</td>
        <th>CNAE:</th><td>&nbsp;</td>
    </tr>
    <tr>
        <th>Empresa Distribuidora:</th><td colspan="4">${oferta.cups_distribuidora_id.name or ''}</td>
        <th colspan="2">Tensión Nominal (V):</th><td colspan="2">&nbsp;</td>
    </tr>
    <tr>
        <%
            fecha_hoy = date.today()
            if oferta.contrato_duracion:
                num_month = oferta.contrato_duracion.num_month
            else:
                num_month = 12
        %>
        <th>Fecha Inicio<sup>*</sup></th><td>${fecha_hoy}</td>
        <th>Fecha Fin</th><td colspan="2">${fecha_hoy + relativedelta(months=num_month)}</td>
        <th colspan="2">Duración del Contrato:</th><td colspan="2">${num_month} meses</td>
    </tr>
</table>
<p><sub>*Será notificada por la empresa distribuidora.</sub></p>
<table>
    <%
      sorted_pot = sorted(oferta.client_potencies_periode, key=attrgetter('periode'))
    %>
    <tr><th rowspan="2">Potencia Contratada</th>
    %for potencia in sorted_pot:
        <th>${potencia.periode}</th>
    %endfor
    </tr>
    <tr>
    %for potencia in sorted_pot:
        <td>${potencia.potencia} kW</td>
    %endfor
    </tr>
</table>
<table>
<%
    consums = get_consums_anuals_periode(oferta.client_consums_periode)
    total = sum([c[1] for c in consums.items()])
%>
    <tr><th rowspan="2">Consumo Anual Estimado</th>
    %for periode in sorted(consums.keys()):
        <th>${periode}</th>
    %endfor
    <th>Total</th>
    <tr>
    %for periode in sorted(consums.keys()):
        <td>${consums[periode]} kWh</td>
    %endfor
    <td>${total} kWh</td>
    </tr>
</table>
<p style="page-break-after:always"></p>

<h2>PRECIOS TARIFA <strong>${oferta.tarifa_oferta.name}</strong></h2>
<table class="centered">
<tr><th colspan="2">Precio de la Potencia<sup>**</sup></th><th class="w50px" rowspan="${len(oferta.pts_oferta) + 1}">&nbsp;</th><th colspan="2">Precio de la Energía<sup>**</sup></th></tr>
%for pot in oferta.pts_oferta:
    <tr><th>${pot.periode}</th>
        <td>${formatLang(get_atr_potencia_price(cursor, uid, tarifa_elect_atr,
                                                oferta.tarifa_oferta, pot.periode), 6)} €/kW y año</td>
        <th>${pot.periode}</th>
        <td>${pot.name} €/kWh</td></tr>
%endfor
</table>



            <p><strong>Nombre: </strong>${oferta.cliente_nombre}</p>
            <p><strong>Apellidos: </strong>${oferta.cliente_apellidos}</p>
            <p><strong>CIF/NIF: </strong>${oferta.cliente_vat}</p>
            <p><strong>IBAN: </strong>${oferta.contrato_iban}</p>
            <p><strong>Plazo de pago: </strong>${_(oferta.contrato_plazo_pago.name)}</p>
            <br>
            <h2>Domicilio físcal:</h2>
            <hr />
            <p><strong>Tipo vía: </strong>${oferta.domc_tipovia_id.abr}</p>
            <p><strong>Calle: </strong>${oferta.domc_street} ${oferta.domc_street2 or ''}</p>
            <p><strong>Número: </strong>${oferta.domc_numero}</p>
            <p><strong>Escalera: </strong>${oferta.domc_escalera}</p>
            <p><strong>Piso: </strong>${oferta.domc_piso}</p>
            <p><strong>Puerta: </strong>${oferta.domc_puerta}</p>
            <p><strong>CP: </strong>${oferta.domc_cp}</p>
            <p><strong>Municipio: </strong>${oferta.domc_municipi_id.name}</p>
            <p><strong>Provincia: </strong>${oferta.domc_state_id.name}</p>
            <br>
            <h2>Datos suministro:</h2>
            <hr />
            <p><strong>CUPS: </strong>${oferta.cups_name}</p>
            <p><strong>Tarifa: </strong>${oferta.sips_tarifa}</p>
            <p><strong>Potencias contratadas: </strong></p>
            <ul>
            %for potencia in oferta.client_potencies_periode:
                <li>Periodo ${potencia.periode}: <strong>${potencia.potencia}</strong></li>
            %endfor
            </ul>
            <p><strong>Consumos Anuales previstos: </strong></p>
            <ul>
            %for consum in get_consums_anuals_periode(oferta.client_consums_periode).items():
                <li>Periodo ${consum[0]}: <strong>${consum[1]}</strong></li>
            %endfor
            </ul>
            <br>
            <h2>Domicilio suministro:</h2>
            <hr />
            <p><strong>Titular Suministro: </strong>${oferta.sips_apellidos}, ${oferta.sips_titular}</p>
            <p><strong>CIF/NIF del titular del suministro: </strong>${oferta.sips_cif}</p>
            <p><strong>Tipo vía: </strong>${oferta.cups_tv.abr}</p>
            <p><strong>Calle: </strong>${oferta.cups_nv}</p>
            <p><strong>Número: </strong>${oferta.cups_pnp}</p>
            <p><strong>Escalera: </strong>${oferta.cups_es}</p>
            <p><strong>Piso: </strong>${oferta.cups_pt}</p>
            <p><strong>Puerta: </strong>${oferta.cups_pu}</p>
            %if oferta.cups_cpo:
                <p><strong>Poligono: </strong>${oferta.cups_cpo}</p>
            %endif
            %if oferta.cups_cpa:
                <p><strong>Parcela: </strong>${oferta.cups_cpa}</p>
            %endif
            %if oferta.cups_aclarador:
                <p><strong>Aclarador: </strong>${oferta.cups_aclarador}</p>
            %endif
            <p><strong>CP: </strong>${oferta.cups_dp}</p>
            <p><strong>Ref. Catastral: </strong>${oferta.cups_ref_catastral}</p>
            <p><strong>Municipio: </strong>${oferta.cups_id_municipi.name}</p>
            <p><strong>Provincia: </strong>${oferta.cups_id_provincia.name}</p>
            <br>
            <h2>Domicilio comunicación:</h2>
            <hr />
            <p><strong>Tipo vía: </strong>${oferta.domf_tipovia_id.abr}</p>
            <p><strong>Calle: </strong>${oferta.domf_street} ${oferta.domf_street2 or ''}</p>
            <p><strong>Número: </strong>${oferta.domf_numero}</p>
            <p><strong>Escalera: </strong>${oferta.domf_escalera}</p>
            <p><strong>Piso: </strong>${oferta.domf_piso}</p>
            <p><strong>Puerta: </strong>${oferta.domf_puerta}</p>
            <p><strong>CP: </strong>${oferta.domf_cp}</p>
            <p><strong>Municipio: </strong>${oferta.domf_municipi_id.name}</p>
            <p><strong>Provincia: </strong>${oferta.domf_state_id.name}</p>
            <p><strong>Teléfono 1: </strong>${oferta.domf_telefono_1}</p>
            <p><strong>Teléfono 2: </strong>${oferta.domf_telefono_2}</p>
            <p><strong>Email 1: </strong>${oferta.domf_email_1}</p>
            <p><strong>Email 2: </strong>${oferta.domf_email_2}</p>
            <br>
            <% fecha_hoy = date.today()%>
            %for doc in oferta.docs_ofertas:
                <p><strong>Fecha legislación: </strong>${doc.data_creacio}</p>
                <% break %>
            %endfor
            <p><strong>Fecha inicio contrato: </strong>${fecha_hoy}</p>
            <%
                if oferta.contrato_duracion:
                   num_month = oferta.contrato_duracion.num_month
                else:
                   num_month = 12
            %>
            <p><strong>Fecha fin contrato: </strong>${fecha_hoy + relativedelta(months=num_month)}</p>
            <p><strong>Comercial asignado: </strong>${oferta.user_id.name}</p>
            <p><strong>Delegación asignada: </strong>${oferta.delegacion.name}</p>
            <p><strong>Grupo: </strong>${oferta.cliente_group.name}</p>
            <p><strong>Tipo facturación: </strong>${dict(TIPO_FACTURACION).get(oferta.contrato_tipo_facturacion,'')}</p>
            <p><strong>Tipo renovación: </strong>${dict(TIPO_RENOVACION).get(oferta.contrato_tipo_renovacion,'')}</p>
            <p><strong>Facturación estimada: </strong>${SINO.get(oferta.contrato_facturacion_estimable, '')}</p>
            <p><strong>Cortable: </strong>${SINO[oferta.contrato_cortable]}</p>
            <p><strong>Cliente garante: </strong>${oferta.contrato_cliente_garante}</p>
        %endfor
    </body>
</html>