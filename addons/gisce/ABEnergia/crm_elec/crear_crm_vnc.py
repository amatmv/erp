# -*- coding: utf-8 -*-

from datetime import date, datetime, timedelta
from dateutil.relativedelta import relativedelta
from osv import osv


class CrearCRMVnc(osv.osv_memory):
    _name = 'crear.crm.vnc'

    def get_vnc_section_id(self, cursor, uid, context=None):
        section_obj = self.pool.get('crm.case.section')
        return section_obj.search(
                    cursor, uid, [('code', '=', 'VNC')], context
                )[0]

    def get_all_fields(self, cursor, uid, pol_id, context=None):
        '''This method try to get all necessary fiels to create case'''
        if not context:
            context = {}

        pol_obj = self.pool.get('giscedata.polissa')
        section_obj = self.pool.get('crm.case.section')
        polissa = pol_obj.browse(cursor, uid, pol_id, context)

        vnc_fields = {}
        hisctoric_params = {}
        name_params = {}

        if polissa:
            hisctoric_params['titular'] = polissa.titular
            hisctoric_params['titular_nif'] = polissa.titular_nif
            hisctoric_params['pagador'] = polissa.pagador
            hisctoric_params['pagador_nif'] = polissa.pagador_nif

            name_params['contract_name'] = polissa.name
            name_params['tarifa'] = polissa.tarifa.name
            name_params['tarifa_comer'] = polissa.llista_preu.name
            name_params['date_deadline'] = polissa.modcontractual_activa.data_final

            vnc_fields['polissa_id'] = pol_id
            vnc_fields['date_deadline'] = polissa.modcontractual_activa.data_final
            vnc_fields['user_id'] = polissa.user_id.id
            vnc_fields['email_from'] = polissa.notificacio_email
            vnc_fields['partner_id'] = polissa.titular.id
            vnc_fields['partner_address_id'] = polissa.direccio_notificacio.id
            vnc_fields['section_id'] = section_obj.search(
                cursor, uid, [('code', '=', 'VNC')], context
            )[0]
            vnc_fields['name'] = self.generate_form_description(
                cursor, uid, name_params, context
            )
            historic = self.generate_historic_description(
                cursor, uid, hisctoric_params, context
            )
            if historic:
                vnc_fields['history_line'] = historic

        return vnc_fields

    def generate_form_description(self, cursor, uid, params, context=None):
        if not context:
            context = {}

        if params:
            description_template = 'Contrato: {0}, Tarifa: {1}, ' \
                                   'Tarifa comer: {2}, ' \
                                   'fecha fin actual modcontractual: {3}'

            return description_template.format(params['contract_name'],
                                               params['tarifa'],
                                               params['tarifa_comer'],
                                               params['date_deadline'],
                                               )
        return False

    def generate_historic_description(self, cursor, uid, params, context=None):
        '''
        Requiered 'titular', 'titular_nif, 'pagador' and 'pagador_nif' keys in
        params.
        :param params: dictionary with 'titular' and payer in a contract.
        :return: string with payer info.
        '''
        if not context:
            context = {}

        if params:
            titular_template = 'Titular: {0}, NIF titular: {1}'
            pagador_template = 'Pagador: {0}, NIF pagador: {1}'

            if params['titular'] != params['pagador']:
                return titular_template.format(
                    params['titular'], params['titular_nif']
                ) + '\n' + pagador_template.format(
                    params['pagador'], params['pagador_nif']
                )
        return False

    def get_delay_config_var(self, cursor, uid, context=None):
        '''
        :return: configuration var for VNC cases.
        '''
        config_obj = self.pool.get('res.config')
        if not context:
            context = {}
        return int(config_obj.get(cursor, uid, 'dies_antelacio_avis_venciment'))

    def get_contratos_renovar(self, cursor, uid, context=None):
        """Retorna els ids dels contractes que es renovaran.
        """
        mod_obj = self.pool.get('giscedata.polissa.modcontractual')
        tarif_obj = self.pool.get('giscedata.polissa.tarifa')

        if not context:
            context = {}
        exclude_tariff_ids = tarif_obj.search(
            cursor, uid, [('name', 'in', ('2.0A', '2.0DHA'))]
        )
        # Això ens dirà que s'han de renovar en aquests moments
        delay = self.get_delay_config_var(cursor, uid, context)
        day = (datetime.now() + timedelta(days=delay)).strftime('%Y-%m-%d')
        search_params = [('state', '=', 'actiu'),
                         ('data_final', '=', day),
                         ('polissa_id.active', '=', 1),
                         ('polissa_id.renovacio_auto', '=', 1),
                         ('modcontractual_seg', '=', False),
                         ('tarifa', 'not in', exclude_tariff_ids),
                         ]

        ids = mod_obj.search(cursor, uid, search_params,
                             order="data_inici asc",
                             context={'active_test': False}
                             )
        pol_ids = []
        mods = mod_obj.read(cursor, uid, ids, ['polissa_id'])

        for mod in mods:
            pol_ids.append(mod['polissa_id'][0])

        return pol_ids

    def crear_casos_vnc(self, cursor, uid, context=None):
        '''
        This method create all possible VNC cases for contract that are going to
        expiry in configuration variable days.
        :return: list of created cases
        '''

        if not context:
            context = {}

        pol_ids = self.get_contratos_renovar(cursor, uid, context)
        case_ids = []

        if pol_ids:
            case_obj = self.pool.get('crm.case')
            for pol_id in pol_ids:
                case_params = self.get_all_fields(cursor, uid, pol_id, context)
                if case_params:
                    search_params = [
                        ('polissa_id', '=', pol_id),
                        ('date_deadline', '=', case_params['date_deadline']),
                        ('section_id', '=', case_params['section_id']),
                    ]
                    exist_case_id = case_obj.search(
                        cursor, uid, search_params, context
                    )
                    if not exist_case_id:
                        history_vals = {}
                        history_value = case_params.get('history_line')
                        if history_value:
                            history_vals['description'] = history_value
                            case_params.pop('history_line')

                        case_id = case_obj.create(
                            cursor, uid, case_params, context
                        )
                        if case_id:
                            case_ids.append(case_id)
                            if history_vals:
                                case_history_obj = self.pool.get(
                                    "crm.case.history")
                                history_vals['name'] = (u'Añadida información '
                                                        u'adicional')
                                history_vals['case_id'] = case_id
                                case_history_obj.create(
                                    cursor, uid, history_vals, context
                                )
        return case_ids

    def cerrar_casos_vnc(self, cursor, uid, context=None):
        '''
        This method close out dated VNC cases.
        :return: list of closed VNC cases
        '''
        pol_obj = self.pool.get('giscedata.polissa')
        mod_obj = self.pool.get('giscedata.polissa.modcontractual')
        case_obj = self.pool.get('crm.case')
        if not context:
            context = {}

        current_day = datetime.today()
        search_params = [('state', '!=', 'done'),
                         ('section_id',
                          '=',
                          self.get_vnc_section_id(cursor, uid, context)
                          )
                         ]
        case_ids = case_obj.search(cursor, uid, search_params, context)
        close_cases = []
        if case_ids:
            for case in case_obj.read(
                    cursor, uid, case_ids,
                    ['polissa_id', 'date_deadline'], context
            ):
                polissa = pol_obj.read(
                    cursor, uid, case['polissa_id'][0],
                    ['modcontractual_activa'], context
                )
                modcon = mod_obj.read(
                    cursor, uid, polissa['modcontractual_activa'][0],
                    ['data_final'], context
                )

                dead_line_case = datetime.strptime(
                    case['date_deadline'], '%Y-%m-%d %H:%M:%S'
                )
                final_date_mod = datetime.strptime(
                    modcon['data_final'], '%Y-%m-%d'
                )
                modcon_out_of_date = (
                    dead_line_case != final_date_mod

                )
                case_deadline_passed = dead_line_case < current_day

                if case_deadline_passed or modcon_out_of_date:
                    close_cases.append(case['id'])

            if close_cases:
                case_obj.case_close(cursor, uid, close_cases)

        return close_cases

    def actualizar_casos_vnc(self, cursor, uid, context=None):
        '''This method create all possible VNC cases and close cases
            out of date.
        '''

        if not context:
            context = {}
        self.crear_casos_vnc(cursor, uid, context=context)
        self.cerrar_casos_vnc(cursor, uid, context=context)

CrearCRMVnc()
