# -*- coding: utf-8 -*-
from osv import osv


class IrAttachment(osv.osv):
    _name = 'ir.attachment'
    _inherit = 'ir.attachment'

    def import_attachment(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        crm_documents_obj = self.pool.get('crm.documentacio')

        if 'attach_id' in context and 'crm_polissa' in context:
            attach_id = context['attach_id']
            crm_pol_id = context['crm_polissa']
            vals = {'attach_id': attach_id,
                        'crm_polissa': crm_pol_id,
                        'tipus_document': 'altres',
                        'tipologia': 'documentos_importados',
                        'versio': 0,}
            crm_documents_obj.create(cursor, uid, vals)
        res = True
        return res

IrAttachment()
