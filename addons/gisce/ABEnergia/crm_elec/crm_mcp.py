# -*- coding: utf-8 -*-
from osv import osv, fields


class CRMMCP(osv.osv):

    _name = 'crm.mcp'

    _columns = {
        'name': fields.char(string='Nombre', size=128),
        'description': fields.char(string=u'Descripción', size=128),
        'minimum_price': fields.float(
            u'Margen mínimo (€/kwh)',
            required=1,
            digits=(16, 3)
        ),
        'percentage': fields.float('Porcentage sobre el impuesto electrico (%)')
    }

    _sql_constraints = [('name_uniq', 'unique (name)',
                         'Ya existe una MCP con este nombre')]
CRMMCP()