# -*- coding: utf-8 -*-
from osv import osv, fields


class WizardPolissaToOpportunity(osv.osv_memory):
    _name = 'wizard.polissa.to.opportunity'

    def _default_num(self, cursor, uid, context=None):
        if not context:
            context = {}
        return len(context.get('active_ids', []))

    def convert(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        wiz = self.browse(cursor, uid, ids[0])
        pol_ids = context.get('active_ids', [])

        polissa_obj = self.pool.get('giscedata.polissa')

        crm_ids = polissa_obj.create_cas_potencial(cursor, uid, pol_ids,
                                                   context=context)

        wiz.write({'num_converted': len(crm_ids), 'state': 'done'})
        return True

    _columns = {
        'num': fields.integer('Número de sips a importar', readonly=True),
        'num_converted': fields.integer('Número de sips importats',
                                         readonly=True),
        'state': fields.char('Estat', size=16)
    }

    _defaults = {
        'num': _default_num,
        'state': lambda *a: 'init'
    }

WizardPolissaToOpportunity()

