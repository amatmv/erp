# -*- coding: utf-8 -*-
from datetime import datetime
from oorq.oorq import AsyncMode

from osv import osv, fields


class crm_compose_message(osv.osv_memory):

    _name = 'crm.compose.message'
    _inherit = 'poweremail.send.wizard'
    _description = 'E-mail composition wizard'

    def default_get(self, cursor, uid, fields, context=None):
        """Overridden to provide specific defaults depending on the context
           parameters.

           :param dict context: several context values will modify the behavior
                                of the wizard, cfr. the class description.
        """
        if context is None:
            context = {}
        crm_doc_id = context.get('crm_document_id', False)
        crm_polissa_id = context.get('crm_polissa_id', False)
        crm_document_obj = self.pool.get('crm.documentacio')
        crm_pol_obj = self.pool.get('crm.polissa')
        email_vals = {}
        email_template = False
        if crm_doc_id:
            email_template = crm_document_obj.get_template(
                cursor, uid, crm_doc_id)
            doc = crm_document_obj.read(
                cursor, uid, crm_doc_id, ['crm_polissa', 'attach_id'])
            crm_polissa_id = doc['crm_polissa'][0]
            attachments_ids = [doc['attach_id'][0], ]
            if attachments_ids:
                email_vals.update(
                    {'attachment_ids': attachments_ids})
        if email_template:
            imd = self.pool.get('ir.model.data')
            template_id = imd.get_object_reference(
                    cursor, uid, 'crm_elec', email_template)
            context.update({'template_id': template_id[1]})

        if crm_polissa_id:
            context.update({'src_rec_ids': [crm_polissa_id]})

            crm_polissa_fields = ['user_id', 'cliente_email_2',
                                  'contacto_email_1', 'contacto_email_2']
            crm_polissa = crm_pol_obj.read(
                cursor, uid, crm_polissa_id, crm_polissa_fields)
            user_id = crm_polissa['user_id'][0]
            emails = [crm_polissa.get('cliente_email_2', False),
                      crm_polissa.get('contacto_email_1', False),
                      crm_polissa.get('contacto_email_2', False)]
            emails_to = ','.join([x for x in emails if x])
            if emails_to:
                email_vals.update({'cc': emails_to})
            email_vals.update({'user_id': user_id,
                               'folder': 'draft',
                               'date': datetime.today().strftime(
                                   '%Y-%m-%d %H:%M:%S')})

        result = super(crm_compose_message, self).default_get(cursor, uid, fields, context=context)
        result.update(dict([x for x in email_vals.items() if x[0] in fields]))
        return result

    def enviar_email(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        src_rec_ids = context.get('src_rec_ids', False)
        src_model = context.get('src_model', False)
        model_obj = self.pool.get(src_model)
        mail_obj = self.pool.get('poweremail.mailbox')
        for mail in self.browse(cursor, uid, ids, context=context):
            body = mail.body_text
            body_html = ''.join('<p>'+line+'</p>' for line in body.split('\n'))
            body_html = mail.body_html.format(contingut=body_html)
            mail.write({'body_html': body_html})
            with AsyncMode('sync') as asmode:
                # This code will be sync for all the jobs running here and
                mail_id = self.save_to_mailbox(
                    cursor, uid, ids, context=context
                )
                mail_id = mail_id[0]
                model_obj.send_from_email(
                    cursor, uid, src_rec_ids, mail_id, context=context)

        return {'type': 'ir.actions.act_window_close'}

crm_compose_message()
