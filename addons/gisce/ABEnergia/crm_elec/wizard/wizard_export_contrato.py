# -*- coding: utf-8 -*-
try:
    from cStringIO import StringIO
except:
    from StringIO import StringIO
import types
import csv
import base64
from operator import itemgetter
from datetime import datetime
from calendar import monthrange
from dateutil.relativedelta import relativedelta
from addons.giscedata_energia_prevista.energia import Normalizer, Consum


from osv import osv, fields
from addons.crm_elec.crm_polissa import get_consums_anuals_periode

def _format_iban(string):
    '''
    This function removes all characters from given 'string' that isn't a
    alpha numeric and converts it to upper case.
    '''
    if string:
        res = ""
        for char in string:
            if char.isalnum():
                res += char.upper()
    else:
        res = string
    return res


def get_acc_from_iban(iban):
    bank = {'banco': '', 'sucursal': '', 'DC': '', 'cc_number': ''}
    if iban:
        iban = _format_iban(iban)
        acc_number = iban[4:]
        bank.update({
            'banco': acc_number[0:4],
            'sucursal': acc_number[4:8],
            'DC': acc_number[8:10],
            'cc_number': acc_number[10:]
        })
    return bank


def wh_fix_vat(vat):
    vat = ''.join([x for x in vat.upper() if x.isalnum()])
    if vat and vat[0].isdigit():
        vat = vat.zfill(9)
    if not vat.startswith('ES'):
        vat = 'ES%s' % vat
    return vat

class WizardExportContrato(osv.osv_memory):
    _name = 'wizard.export.contrato'

    def get_consums_anuals(self, cursor, uid, crm_polissa_id):
        sum_consums = {}
        crm_polissa_obj = self.pool.get('crm.polissa')
        crm_polissa = crm_polissa_obj.browse(cursor, uid, crm_polissa_id)
        return get_consums_anuals_periode(crm_polissa.client_consums_periode)

    def get_potencies_periodes(self, cursor, uid, crm_polissa_id):
        potencies = dict.fromkeys(['P%s' % x for x in range(1, 7)], '')
        crm_potencies_obj = self.pool.get('crm.potencia.contractada.periode')
        potencies_id = crm_potencies_obj.search(cursor, uid, [
            ('crm_polissa', '=', crm_polissa_id), ('tipus', '=', 'client')])
        potencies_pol = sorted(crm_potencies_obj.read(
            cursor, uid, potencies_id, ['periode', 'potencia']),
               key=itemgetter('periode'))
        for pot in potencies_pol:
            if pot['periode'] in potencies:
                potencies[pot['periode']] = pot['potencia']

        return potencies

    def create_or_get_bank(self, cursor, uid, partner_id, country_id, iban, bic):
        partner_bank_obj = self.pool.get('res.partner.bank')
        iban = _format_iban(iban)
        search = [('partner_id', '=', partner_id),
                  ('state', '=', 'iban'),
                  ('acc_number', '=', iban)]
        old_bank = partner_bank_obj.search(cursor, uid, search)
        if old_bank:
            return old_bank[0]
        else:
            bank_obj = self.pool.get('res.bank')
            bank_id = bank_obj.search(cursor, uid, [('bic', '=', bic)])
            if bank_id:
                bank_id = bank_id[0]
            else:
                bank_id = False
            vals = {
                'state': 'iban', 'iban': iban,
                'partner_id': partner_id, 'acc_country_id': country_id,
                'bank': bank_id,  'default_bank': True, 'bank_bic': bic,
            }
            return partner_bank_obj.create(cursor, uid, vals)
        return True

    def create_mandate(self, cursor, uid, polissa_id):
        mandate_obj = self.pool.get('payment.mandate')
        search = [('reference', '=', 'giscedata.polissa,%s' % polissa_id)]
        mandate_id = mandate_obj.search(cursor, uid, search)
        if mandate_id:
            vals = {'date_end': datetime.now().strftime('%Y-%m-%d')}
            mandate_obj.write(cursor, uid, mandate_id, vals)

        vals = {'reference': 'giscedata.polissa,%s' % polissa_id}
        mandate_obj.create(cursor, uid, vals)

    def create_polissa(self, cursor, uid, crm_polissa_id, context=None):
        if not context:
            context = {}
        crm_polissa_obj = self.pool.get('crm.polissa')
        polissa_obj = self.pool.get('giscedata.polissa')
        crm_polissa = crm_polissa_obj.browse(cursor, uid, crm_polissa_id)
        partner_address_obj = self.pool.get('res.partner.address')
        direccio_pag = partner_address_obj.search(
            cursor, uid, [('partner_id', '=', crm_polissa.partner_id.id),
                          ('type', '=', 'invoice')])[0]
        direccio_env = partner_address_obj.search(
            cursor, uid, [('partner_id', '=', crm_polissa.partner_id.id),
                          ('type', '=', 'delivery')])[0]
        payment_mode_obj = self.pool.get('payment.mode')
        search_vals = [('name', '=', 'Direct Payment')]

        grup_pagament_ids = payment_mode_obj.search(
            cursor, uid, search_vals
        )
        grup_pagament = False
        if grup_pagament_ids:
            grup_pagament = grup_pagament_ids[0]

        bank_id = self.create_or_get_bank(
            cursor, uid, crm_polissa.partner_id.id,
            crm_polissa.domc_country_id.id, crm_polissa.contrato_iban,
            crm_polissa.contrato_bic_code)
        vals = {
            'active': 1,
            'renovacio_auto': 1,
            'codi': self.pool.get('ir.sequence').get(
                cursor, uid, 'giscedata.polissa'),
            #data exportació
            'data_alta': datetime.now(),
            'data_firma_contracte': datetime.now(),
            'data_baixa': False,
            'potencia': crm_polissa.potencia_client,
            'observacions': 'Pendiente de Activación ATR',
            'tarifa': crm_polissa.tarifa_client.id,
            'titular': crm_polissa.partner_id.id,
            'cups': crm_polissa.cups_id.id,
            'distribuidora': crm_polissa.cups_distribuidora_id.id,
            'facturacio': crm_polissa.contrato_tipo_facturacion,
            #'facturacio_potencia': facturacio_potencia,
            'payment_mode_id': grup_pagament,
            #'tipo_pago': tipo_pago,
            #'llista_preu': tarifa_comercialitzadora,
            'pagador': crm_polissa.partner_id.id,
            'direccio_pagament': direccio_pag,
            'bank': bank_id,
            'pagador_sel': 'titular',
            # 'enviament':'email',
            'notificacio': 'titular',
            'direccio_notificacio': direccio_env,
            'user_id': crm_polissa.user_id.id,
            'preavis': crm_polissa.contrato_plazo_preaviso.id,
            'plazo_pago': crm_polissa.contrato_plazo_pago.id,
            'mcp': crm_polissa.mcp_definitivo
        }
        polissa_fields = self.pool.get('giscedata.polissa').fields_get(
            cursor, uid).keys()
        if 'email_notification' in polissa_fields:
            vals.update({'email_notification': True})
        if 'sms_notification' in polissa_fields:
            vals.update({'sms_notification': True})

        polissa_id = polissa_obj.create(cursor, uid, vals)
        self.create_mandate(cursor, uid, polissa_id)
        crm_polissa.write({'polissa_id': polissa_id})
        return polissa_id


    def create_or_update_cups(self, cursor, uid, crm_polissa_id, context=None):
        if not context:
            context = {}
        crm_polissa_obj = self.pool.get('crm.polissa')
        fields = ['cups_name', 'cups_id_municipi', 'cups_id_poblacio',
                  'cups_id_provincia', 'cups_tv', 'cups_nv', 'cups_pnp',
                  'cups_es', 'cups_pt', 'cups_pu', 'cups_cpo', 'cups_cpa',
                  'cups_dp', 'cups_aclarador', 'cups_ref_catastral',
                  'cups_distribuidora_id']
        cups_obj = self.pool.get('giscedata.cups.ps')
        crm_pol_cups = crm_polissa_obj.read(cursor, uid, crm_polissa_id, fields)
        cups_id = cups_obj.search(
            cursor, uid, [('name', '=', crm_pol_cups['cups_name'])])
        tpv_id = crm_pol_cups['cups_tv'] and crm_pol_cups['cups_tv'][0] or False
        vals = {
            'active': 1,
            'name': crm_pol_cups['cups_name'],
            'id_municipi': crm_pol_cups['cups_id_municipi'][0],
            'id_poblacio': crm_pol_cups['cups_id_poblacio'][0],
            'nv': crm_pol_cups['cups_nv'],
            'pnp': crm_pol_cups['cups_pnp'],
            'es': crm_pol_cups['cups_es'],
            'pt': crm_pol_cups['cups_pt'],
            'pu': crm_pol_cups['cups_pu'],
            'tv': tpv_id,
            'ref_catastral': crm_pol_cups['cups_ref_catastral'],
            'aclarador': crm_pol_cups['cups_aclarador'],
            'cpo': crm_pol_cups['cups_cpo'],
            'cpa': crm_pol_cups['cups_cpa'],
            'dp': crm_pol_cups['cups_dp'],
            'distribuidora_id': crm_pol_cups['cups_distribuidora_id'][0],
            }
        if cups_id:
            cups_id = cups_id[0]
            vals.pop('name')
            cups_obj.write(cursor, uid, [cups_id], vals)
        else:
            cups_id = cups_obj.create(cursor, uid, vals)

        crm_polissa_obj.write(cursor, uid, [crm_polissa_id],
                              {'cups_id': cups_id})
        return cups_id

    def create_or_update_partner(self, cursor, uid, crm_polissa_id, context=None):
        def get_direccion(prefix, crm_polissa):
            direccion = []
            tipovia = getattr(crm_polissa, prefix+'_tipovia_id')
            if tipovia:
                direccion.append('%s' % ( tipovia.abr or tipovia.codi))
            direccion.append(getattr(crm_polissa, prefix+'_street') or '')
            direccion.append(getattr(crm_polissa, prefix+'_street2') or '')
            direccion.append(getattr(crm_polissa, prefix+'_numero') or '')
            direccion.append(getattr(crm_polissa, prefix+'_escalera') or '')
            direccion.append(getattr(crm_polissa, prefix+'_piso') or '')
            direccion.append(getattr(crm_polissa, prefix+'_puerta') or '')
            street = ' '.join(direccion).strip()
            if len(street) > 128:
                street1 = street[1:street.rfind(' ', 1, 128)]
                street2 = street[street.rfind(' ', 1, 128):]
            else:
                street1 = street
                street2 = ''
            return street1, street2

        if not context:
            context = {}

        partner_obj = self.pool.get('res.partner')
        partner_address_obj = self.pool.get('res.partner.address')
        crm_polissa_obj = self.pool.get('crm.polissa')
        crm_polissa = crm_polissa_obj.browse(cursor, uid, crm_polissa_id)
        partner_name = '%s, %s' % (crm_polissa.cliente_apellidos,
                                   crm_polissa.cliente_nombre)
        vat = wh_fix_vat(crm_polissa.cliente_vat)
        partner_id = partner_obj.search(
            cursor, uid, [('name', '=', partner_name), ('vat', '=', vat)],
            order='write_date desc, id desc')
        if partner_id:
            partner_id = partner_id[0]
        else:
            vals = {
                'name': partner_name,
                'vat': vat,
                'customer': True,
            }
            partner_id = partner_obj.create(cursor, uid, vals)
        street1, street2 = get_direccion('domc', crm_polissa)
        vals_partner_address = {
            'name': partner_name,
            'partner_id': partner_id,
            'type': 'contact',
            'phone': crm_polissa.domc_telefono_1,
            'mobile': crm_polissa.domc_telefono_2,
            'email': crm_polissa.domc_email_1,
            'id_municipi': crm_polissa.domc_municipi_id.id,
            'id_poblacio': crm_polissa.domc_city.id,
            'country_id': crm_polissa.domc_country_id.id,
            'state_id': crm_polissa.domc_state_id.id,
            'zip': crm_polissa.domc_cp,
            'street': street1,
            'street2': street2
        }

        search_addrees = [('partner_id', '=', partner_id),
                          ('type', '=', 'contact'),
                          ('street', '=', street1)]
        old_addresses_ids = partner_address_obj.search(
            cursor, uid, search_addrees)
        if not old_addresses_ids:
            partner_address_obj.create(
                cursor, uid, vals_partner_address)
        else:
            partner_address_obj.write(
                cursor, uid, [old_addresses_ids[0]], vals_partner_address)

        search_addrees = [('partner_id', '=', partner_id),
                          ('type', '=', 'invoice'),
                          ('street', '=', street1)]
        old_addresses_ids = partner_address_obj.search(
            cursor, uid, search_addrees)
        vals_partner_address['type'] = 'invoice'
        if not old_addresses_ids:
            partner_address_obj.create(
                cursor, uid, vals_partner_address)
        else:
            partner_address_obj.write(
                cursor, uid, [old_addresses_ids[0]], vals_partner_address)

        street1, street2 = get_direccion('domf', crm_polissa)
        search_addrees = [('partner_id', '=', partner_id),
                          ('type', '=', 'delivery'),
                          ('street', '=', street1)]
        old_addresses_ids = partner_address_obj.search(
            cursor, uid, search_addrees)
        if not old_addresses_ids:
            vals_partner_address = {
                'name': partner_name,
                'partner_id': partner_id,
                'type': 'delivery',
                'id_municipi': crm_polissa.domf_municipi_id.id,
                'id_poblacio': crm_polissa.domf_city,
                'country_id': crm_polissa.domf_country_id.id,
                'state_id': crm_polissa.domf_state_id.id,
                'zip': crm_polissa.domf_cp,
                'street': street1,
                'street2': street2,
                'phone': crm_polissa.domf_telefono_1,
                'mobile': crm_polissa.domf_telefono_2,
                'email': crm_polissa.domf_email_1,
            }
            partner_address_obj.create(
                cursor, uid, vals_partner_address)
        else:
            partner_address_obj.write(
                cursor, uid, [old_addresses_ids[0]], vals_partner_address)

        if crm_polissa.contacto_nombre:
            vals_partner_address = {
                'name': '%s, %s' % (
                    crm_polissa.contacto_apellidos,
                    crm_polissa.contacto_nombre),
                'partner_id': partner_id,
                'type': 'default',
                'title': crm_polissa.contacto_title.shortcut,
                'phone': crm_polissa.contacto_telefono_1,
                'mobile': crm_polissa.contacto_telefono_2,
                'email': crm_polissa.contacto_email_1,
                }
            search_addrees = [('partner_id', '=', partner_id),
                              ('type', '=', 'default'),
                              ('name', '=', vals_partner_address['name']),
                              ]
            old_addresses_ids = partner_address_obj.search(
                cursor, uid, search_addrees)
            if not old_addresses_ids:
                partner_address_obj.create(cursor, uid, vals_partner_address)
            else:
                partner_address_obj.write(
                    cursor, uid, [old_addresses_ids[0]], vals_partner_address)

        crm_polissa.write({'partner_id': partner_id})
        return partner_id

    def exportar_consums_audinfor(self, cursor, uid, year, crm_polissa_id,
                                  cups_id, cups_name, context=None):
        if not context:
            context = {}
        energia_obj = self.pool.get('giscedata.energia.prevista.consums')
        crm_polissa_obj = self.pool.get('crm.polissa')
        head_line = [''] * 13

        output = StringIO()
        fout = csv.writer(output, delimiter=';', quoting=csv.QUOTE_ALL)
        start = '%s-01-01' % year
        end = '%s-12-31' % year
        ids = energia_obj.search(cursor, uid, [
            ('cups', '=', cups_id),
            ('data_inici', '>=', start),
            ('data_final', '<=', end)
        ])
        consums = energia_obj.read(
            cursor, uid, ids, ['periode', 'consum', 'data_inici', 'data_final'])
        consums = sorted(consums, key=itemgetter('data_inici'))
        consums_data = {}
        for consum in consums:
            key = '{0}_{1}'.format(consum['data_inici'], consum['data_final'])
            if key not in consums_data:
                consums_data.update({key: {
                    consum['periode']: consum['consum']}})
            else:
                if consum['periode'] not in consums_data[key]:
                    consums_data[key].update(
                        {consum['periode']: consum['consum']})
                else:
                    consums_data[key][consum['periode']] = consum['consum']
        head = []
        lines = []
        for consum in consums_data.items():
            key = consum[0]
            value = consum[1]
            data_inici, data_final = key.split('_')
            year, month, day = data_final.split('-')
            line = [''] * 13
            i = 0
            head_line[i] = u'Cliente'
            i += 1
            head_line[i] = u'Fecha inicial lectura'
            line[i] = data_inici
            i += 1
            head_line[i] = u'Fecha final lectura'
            line[i] = data_final
            i += 1
            head_line[i] = u'Año lectura'
            line[i] = year
            i += 1
            head_line[i] = u'Periodo lectura'
            line[i] = month
            i += 1
            head_line[i] = u'Aclarador'
            i += 1
            for x in range(1, 7):
                head_line[i] = u'Consumo P1'
                if 'P%s' % x in value:
                    line[i] = value.get('P%s' % x)
                i += 1
            head_line[i] = u'CUPS'
            line[i] = cups_name
            i += 1
            if len(head) == 0:
                head.append(head_line)

            for n, elem in enumerate(line):
                if elem:
                    if isinstance(elem, types.StringTypes):
                        line[n] = elem.replace('False', '').strip() or ''
                else:
                    line[n] = ''
            lines.append(line)

        fout.writerow(head_line)
        fout.writerows(lines)
        output.seek(0)

        mfile = base64.b64encode(''.join(output.readlines()).encode('utf-8'))
        filename = 'consumo_exportado_%s_%s.csv' % (
            cups_name,
            datetime.now().strftime('%Y%m%d%H%M%S')
        )
        crm_polissa_obj.write(
            cursor, uid, [crm_polissa_id],
            {'export_connsums_file': mfile, 'audinfor_consums_file_name': filename})
        output.close()

    def create_consums(self, cursor, uid, crm_polissa_id, context=None):
        if not context:
            context = {}
        crm_polissa_obj = self.pool.get('crm.polissa')
        energia_obj = self.pool.get('giscedata.energia.prevista.consums')
        consums_obj = self.pool.get('crm.consums.periode')

        crm_polissa = crm_polissa_obj.read(
            cursor, uid, crm_polissa_id,
            ['cups_id', 'client_consums_periode', 'consumo_anual_previsto'])
        cups_id = crm_polissa['cups_id'][0]
        cups_name = crm_polissa['cups_id'][1]

        consums = consums_obj.browse(cursor, uid, crm_polissa['client_consums_periode'])

        years = {}

        for consum in consums:
            year = int(consum.data_final.split('-')[0])
            if years.get(year, False):
                years[year] += 1
            else:
                years[year] = 1

        year = sorted(years.items(), key=itemgetter(1), reverse=True)[0][0]
        end_date = datetime(year, 12, 31)
        start_date = datetime(year, 1, 1)

        for p, consum in get_consums_anuals_periode(consums).items():
            months = {}
            c = Consum(cups_id, p, start_date, end_date, consum)
            norm = Normalizer([c], 'consum')
            for day in norm.normalize():
                month = day[0].month
                months.setdefault(month, 0)
                months[month] += day[1]

            for month, consum_mes in months.items():
                data_inici = '%s-%02i-01' % (year, month)
                data_final = '%s-%02i-%s' % (year, month, monthrange(year, month)[1])
                vals = {
                    'cups': cups_id,
                    'data_inici': data_inici,
                    'data_final': data_final,
                    'periode': p,
                    'consum': consum_mes
                }
                energia_obj.create(cursor, uid, vals)

        energia_obj.reproduce(cursor, uid, cups_id, year, -1)
        energia_obj.reproduce(cursor, uid, cups_id, year, 1)
        self.exportar_consums_audinfor(cursor, uid, year, crm_polissa_id,
                                       cups_id, cups_name)
        return True

    def get_nom_complet(self, nom, cognoms):
        return ', '.join([x for x in [cognoms, nom] if x])

    def exportar_contracte_audinfor(self, cursor, uid, ids, crm_polissa_id,
                                    context=None):
        if not context:
            context = {}
        crm_polissa_obj = self.pool.get('crm.polissa')
        crm_polissa = crm_polissa_obj.read(cursor, uid, crm_polissa_id, [])
        head_line = [''] * 84
        line = [''] * 84
        output = StringIO()
        fout = csv.writer(output, delimiter=';', quoting=csv.QUOTE_ALL)
        i = 0
        head_line[i] = u'Cups'
        line[i] = crm_polissa['cups_name']
        i += 1
        head_line[i] = u'Nombre del titular'
        line[i] = self.get_nom_complet(crm_polissa['sips_titular'],
                                       crm_polissa['sips_apellidos'])
        i += 1
        head_line[i] = u'Dirección completa'
        direccion = []
        direccion_fields = ['cpo', 'cpa', 'nv', 'pnp', 'es', 'pt', 'pu',
                            'aclarador', 'dp']
        if crm_polissa.get('cups_tv', False):
            tpv = self.pool.get('res.tipovia').read(
                cursor, uid, crm_polissa['cups_tv'][0], ['codi', 'abr'])
            direccion.append('%s' % tpv['abr'] or tpv['codi'])
        for field in direccion_fields:
            field_value = crm_polissa.get('cups_%s' % field, False)
            if field_value:
                direccion.append('%s' % field_value)
        if crm_polissa.get('cups_id_poblacio', False):
            direccion.append(u'(%s)' % crm_polissa['cups_id_poblacio'][1])
        elif crm_polissa.get('cups_id_municipi', False):
            direccion.append(u'(%s)' % crm_polissa['cups_id_municipi'][1])

        line[i] = ' '.join(direccion)
        i += 1
        head_line[i] = u'Número'
        line[i] = '%s' % (crm_polissa['cups_pnp'])
        i += 1
        head_line[i] = u'Extensión'
        line[i] = ''
        i += 1
        head_line[i] = u'Aclarador'
        line[i] = '%s' % (crm_polissa['cups_aclarador'])
        i += 1
        head_line[i] = u'Código Postal'
        line[i] = '%s' % (crm_polissa['cups_dp'])
        i += 1
        head_line[i] = u'Población'
        if crm_polissa.get('cups_id_poblacio', False):
            poblacion = self.pool.get('res.poblacio').read(
                cursor, uid, crm_polissa['cups_id_poblacio'][0], ['name'])
            line[i] = '%s' % poblacion['name']
        i += 1
        head_line[i] = u'Provincia'
        if crm_polissa.get('cups_id_provincia', False):
            provincia = self.pool.get('res.country.state').read(
                cursor, uid, crm_polissa['cups_id_provincia'][0], ['name'])
            line[i] = '%s' % provincia['name']
        i += 1
        head_line[i] = u'Teléfono'
        line[i] = '%s' % (crm_polissa['cliente_telefono_1'])
        i += 1
        head_line[i] = u'Móvil'
        line[i] = '%s' % (crm_polissa['cliente_telefono_2'])
        i += 1
        head_line[i] = u'E-mail'
        line[i] = '%s' % (crm_polissa['cliente_email_1'])
        i += 1
        head_line[i] = u'Distribuidora'
        i += 1
        head_line[i] = u'Comercializadora'
        line[i] = ''
        i += 1
        head_line[i] = u'Tarifa acceso'
        if crm_polissa.get('tarifa_client', False):
            line[i] = '%s' % crm_polissa['tarifa_client'][1]
        i += 1
        head_line[i] = u'Potencia contratada'
        line[i] = '%s' % crm_polissa['potencia_client']
        i += 1
        consums = self.get_consums_anuals(cursor, uid, crm_polissa_id)
        for periode in range(1, 7):
            head_line[i] = u'Consumo Anual P%s' % periode
            if 'P%s' % periode in consums:
                line[i] = consums['P%s' % periode]
            i += 1
        head_line[i] = u'Fecha vencimiento'
        if crm_polissa.get('contrato_duracion', False):
            num_month = self.pool.get('crm.polissa.duracion').read(
                cursor, uid, crm_polissa['contrato_duracion'][0], ['num_month'])
            num_month = num_month['num_month']
        else:
            num_month = 12
        if crm_polissa.get('docs_contratos', False):
            data_creacio = self.pool.get('crm.documentacio').read(
                cursor, uid, crm_polissa['docs_contratos'][0], ['data_creacio']
            )['data_creacio']
            data_creacio = datetime.strptime(data_creacio, '%Y-%m-%d %H:%M:%S')
        else:
            data_creacio = datetime.now()
        fecha_vencimiento = data_creacio + relativedelta(months=num_month)
        line[i] = '%s' % fecha_vencimiento.strftime('%Y-%m-%d')
        i += 1
        head_line[i] = u'NIF/DNI'
        line[i] = '%s' % (crm_polissa['sips_cif'])
        i += 1
        head_line[i] = u'Nombre facturación'
        line[i] = self.get_nom_complet(crm_polissa['contrato_nombre'],
                                       crm_polissa['contrato_apellidos'])
        i += 1
        head_line[i] = u'Calle facturación'
        direccion = []
        if crm_polissa.get('domc_tipovia_id', False):
            tpv = self.pool.get('res.tipovia').read(
                cursor, uid, crm_polissa['domc_tipovia_id'][0],
                ['codi', 'abr'])
            direccion.append('%s' % tpv['abr'] or tpv['codi'])
        direccion.append(crm_polissa['domc_street'] or '')
        direccion.append(crm_polissa['domc_street2'] or '')
        line[i] = ' '.join(direccion)
        i += 1
        head_line[i] = u'Número facturación'
        line[i] = '%s' % (crm_polissa['domc_numero'])
        i += 1
        head_line[i] = u'Extensión facturación'
        line[i] = '%s %s %s' % (crm_polissa['domc_escalera'],
                                crm_polissa['domc_piso'],
                                crm_polissa['domc_puerta'])
        i += 1
        head_line[i] = u'Codigo postal facturación'
        line[i] = '%s' % (crm_polissa['domc_cp'])
        i += 1
        head_line[i] = u'Población facturación'
        if crm_polissa.get('domc_city', False):
            poblacion = self.pool.get('res.poblacio').read(
                cursor, uid, crm_polissa['domc_city'][0], ['name'])
            line[i] = '%s' % poblacion['name']
        i += 1
        head_line[i] = u'Provincia facturación'
        if crm_polissa.get('domc_state_id', False):
            provincia = self.pool.get('res.country.state').read(
                cursor, uid, crm_polissa['domc_state_id'][0], ['name'])
            line[i] = '%s' % provincia['name']
        i += 1
        head_line[i] = u'Email facturación'
        line[i] = '%s' % (crm_polissa['cliente_email_1'])
        i += 1
        head_line[i] = u'NIF/DNI facturación'
        line[i] = '%s' % (crm_polissa['cliente_vat'])
        i += 1
        head_line[i] = u'Nombre envio'
        line[i] = self.get_nom_complet(crm_polissa['cliente_nombre'],
                                       crm_polissa['cliente_apellidos'])
        i += 1
        head_line[i] = u'Calle envio'
        direccion = []
        if crm_polissa.get('domf_tipovia_id', False):
            tpv = self.pool.get('res.tipovia').read(
                cursor, uid, crm_polissa['domf_tipovia_id'][0],
                ['codi', 'abr'])
            direccion.append('%s' % tpv['abr'] or tpv['codi'])
        direccion.append(crm_polissa['domf_street'] or '')
        direccion.append(crm_polissa['domf_street2'] or '')
        line[i] = ' '.join(direccion)
        i += 1
        head_line[i] = u'Número envio'
        line[i] = '%s' % (crm_polissa['domf_numero'])
        i += 1
        head_line[i] = u'Extensión envio'
        line[i] = '%s %s %s' % (crm_polissa['domf_escalera'],
                                crm_polissa['domf_piso'],
                                crm_polissa['domf_puerta'])
        i += 1
        head_line[i] = u'Codigo postal envio'
        line[i] = '%s' % (crm_polissa['domf_cp'])
        i += 1
        head_line[i] = u'Población envio'
        if crm_polissa.get('domf_city', False):
            poblacion = self.pool.get('res.poblacio').read(
                cursor, uid, crm_polissa['domf_city'][0], ['name'])
            line[i] = '%s' % poblacion['name']
        i += 1
        head_line[i] = u'Provincia envio'
        if crm_polissa.get('domf_state_id', False):
            provincia = self.pool.get('res.country.state').read(
                cursor, uid, crm_polissa['domf_state_id'][0], ['name'])
            line[i] = '%s' % provincia['name']
        i += 1
        head_line[i] = u'Email envio'
        line[i] = '%s' % (crm_polissa['domc_email_1'])
        i += 1
        head_line[i] = u'NIF/DNI envio'
        line[i] = '%s' % (crm_polissa['cliente_vat'])
        i += 1
        head_line[i] = u'Nombre pagador'
        line[i] = self.get_nom_complet(crm_polissa['cliente_nombre'],
                                       crm_polissa['cliente_apellidos'])
        i += 1
        head_line[i] = u'Tarifa ML'
        i += 1
        head_line[i] = u'Grupo Tarifa ML'
        i += 1
        head_line[i] = u'Forma de Pago'
        i += 1
        acc_number = get_acc_from_iban(crm_polissa['contrato_iban'])
        head_line[i] = u'Banco'
        line[i] = acc_number['banco']
        i += 1
        head_line[i] = u'Sucursal'
        line[i] = acc_number['sucursal']
        i += 1
        head_line[i] = u'DC'
        line[i] = acc_number['DC']
        i += 1
        head_line[i] = u'Número de cuenta'
        line[i] = acc_number['cc_number']
        i += 1
        head_line[i] = u'Periodo facturación'
        line[i] = crm_polissa['contrato_tipo_facturacion']
        i += 1
        head_line[i] = u'Tipo impresión factura'
        i += 1
        head_line[i] = u'Agente comercial'
        if crm_polissa.get('user_id', False):
            line[i] = crm_polissa['user_id'][1]
        i += 1
        head_line[i] = u'Agente comercial'
        i += 1
        potencies = self.get_potencies_periodes(cursor, uid, crm_polissa_id)
        for periode in range(1, 7):
            head_line[i] = u'POT%s' % periode
            line[i] = potencies['P%s' % periode]
            i += 1
        head_line[i] = u'Código Cliente'
        i += 1
        head_line[i] = u'Referencia Catastral'
        line[i] = crm_polissa['cups_ref_catastral']
        i += 1
        head_line[i] = u'Reactiva<15'
        i += 1
        head_line[i] = u'Maximetro<15'
        i += 1
        head_line[i] = u'Medida baja'
        i += 1
        head_line[i] = u'Medida baja'
        i += 1
        head_line[i] = u'KVA Transformador'
        i += 1
        head_line[i] = u'Tipo alquiler'
        i += 1
        head_line[i] = u'Tipo cliente'
        i += 1
        head_line[i] = u'Referencia externa'
        i += 1
        head_line[i] = u'Apellido 1'
        line[i] = crm_polissa['cliente_apellidos']
        i += 1
        head_line[i] = u'Apellido 2'
        i += 1
        head_line[i] = u'Nombre'
        line[i] = crm_polissa['cliente_nombre']
        i += 1
        head_line[i] = u'Cidudad Titular'
        if crm_polissa.get('domc_municipi_id', False):
            line[i] = crm_polissa['domc_municipi_id'][1]
        i += 1
        head_line[i] = u'Provincia Titular'
        if crm_polissa.get('domc_state_id', False):
            provincia = self.pool.get('res.country.state').read(
                cursor, uid, crm_polissa['domc_state_id'][0], ['name'])
            line[i] = '%s' % provincia['name']
        i += 1
        head_line[i] = u'Calle titular'
        direccion = []
        if crm_polissa.get('domc_tipovia_id', False):
            tpv = self.pool.get('res.tipovia').read(
                cursor, uid, crm_polissa['domc_tipovia_id'][0],
                ['codi', 'abr'])
            direccion.append('%s' % tpv['abr'] or tpv['codi'])
        direccion.append(crm_polissa['domc_street'] or '')
        direccion.append(crm_polissa['domc_street2'] or '')
        line[i] = ' '.join(direccion)
        i += 1
        head_line[i] = u'Nº Calle'
        line[i] = crm_polissa['domc_numero']
        i += 1
        head_line[i] = u'Extensión'
        line[i] = '%s %s %s' % (crm_polissa['domc_escalera'],
                                crm_polissa['domc_piso'],
                                crm_polissa['domc_puerta'])
        i += 1
        head_line[i] = u'Aclarador'
        i += 1
        head_line[i] = u'CP Titular'
        line[i] = crm_polissa['domc_cp']
        i += 1
        head_line[i] = u'Tel 1'
        line[i] = crm_polissa['domc_telefono_1']
        i += 1
        head_line[i] = u'Móvil'
        line[i] = crm_polissa['domc_telefono_2']
        i += 1
        head_line[i] = u'FAX'
        i += 1
        head_line[i] = u'Email'
        line[i] = crm_polissa['domc_email_1']
        i += 1
        for n, elem in enumerate(line):
            if elem:
                if isinstance(elem, types.StringTypes):
                    line[n] = elem.replace('False', '').strip() or ''
            else:
                line[n] = ''

        fout.writerow(head_line)
        fout.writerow(line)
        output.seek(0)

        mfile = base64.b64encode(''.join(output.readlines()).encode('utf-8'))
        filename = 'contrato_exportado_%s_%s.csv' % (
            crm_polissa['cups_sips'],
            datetime.now().strftime('%Y%m%d%H%M%S')
        )
        self.write(cursor, uid, ids, {'file': mfile, 'name': filename})
        crm_polissa_obj.write(
            cursor, uid, [crm_polissa_id],
            {'export_file': mfile, 'audinfor_file_name': filename})
        output.close()

        return True

    def _default_file(self, cursor, uid, context=None):
        if not context:
            context = {}
        if context.get('crm_polissa_id', False):
            crm_polissa_id = context.get('crm_polissa_id')
            crm_polissa_obj = self.pool.get('crm.polissa')
            file = crm_polissa_obj.read(cursor, uid, crm_polissa_id, ['export_file'])
            return file.get('export_file', False)
        else:
            return False

    def _default_filename(self, cursor, uid, context=None):
        if not context:
            context = {}
        if context.get('crm_polissa_id', False):
            crm_polissa_id = context.get('crm_polissa_id')
            crm_polissa_obj = self.pool.get('crm.polissa')
            filename = crm_polissa_obj.read(cursor, uid, crm_polissa_id, ['audinfor_file_name'])
            return filename.get('audinfor_file_name', False)
        else:
            return False



    def export(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        if context.get('crm_polissa_id', False):
           crm_polissa_id = context.get('crm_polissa_id')
        else:
           raise osv.except_osv('Error', 'Indicate crm_polissa_id on context')

        self.create_or_update_cups(cursor, uid, crm_polissa_id)
        self.create_or_update_partner(cursor, uid, crm_polissa_id)
        self.create_polissa(cursor, uid, crm_polissa_id)
        self.create_consums(cursor, uid, crm_polissa_id)
        self.exportar_contracte_audinfor(cursor, uid, ids, crm_polissa_id)

        return True

    _columns = {
         'file': fields.binary('Contrato'),
         'name': fields.char('Nombre fichero', size=128)
    }

    _defaults ={
        'file': _default_file,
        'name': _default_filename
    }

WizardExportContrato()
