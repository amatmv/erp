# -*- coding: utf-8 -*-
from osv import osv, fields

class WizardGoToContrato(osv.osv_memory):

    _name = 'wizard.go.to.contrato'

    def go_to_contrato(self, cursor, uid, ids, context=None):
        ''' wizard per obrir el contracte creat amb un botó des de la oferta
        '''

        if not context:
            return False

        # busquem el numero de ctte (name) que correspon a la oferta oberta
        polissa_obj = self.pool.get('giscedata.polissa')
        crm_polissa_obj = self.pool.get('crm.polissa')
        crm_polissa_id = context.get('active_id')
        crm_polissa = crm_polissa_obj.read(cursor,
                                           uid,
                                           [crm_polissa_id],
                                           ['name', 'polissa_id'])[0]
        polissa_id = crm_polissa['polissa_id'][0]
        contracte = polissa_obj.read(cursor, uid, [polissa_id], ['name',
                                                                 'titular',
                                                                 'cups'])[0]
        num_ctte = contracte['name']
        domain =  [('name', '=', num_ctte)]
        return {
                'name': 'Contrato',
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'giscedata.polissa',
                'type': 'ir.actions.act_window',
                'domain': domain,
                }

    _columns = {
        'crm_polissa_id': fields.integer('Polissa id'),
    }

WizardGoToContrato()
