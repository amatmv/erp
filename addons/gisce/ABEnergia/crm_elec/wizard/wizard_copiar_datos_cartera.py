# -*- encoding: utf-8 -*-

import collections
from osv import osv, fields

class WizardCopiarDatosOferta(osv.osv_memory):

    _name = 'wizard.copiar.datos.oferta'

    def action_obtener_datos_oferta(self, cursor, uid, ids, context=None):
        '''Copy the fields in the offer'''
        if not context:
            context = {}
        context.update({'sync': False,
                        'active_test': False})
        crm_obj = self.pool.get('crm.polissa')
        wizard = self.browse(cursor, uid, ids[0])
        offer_id = wizard.offer_id.id

        # obtenir les dades necessàries
        camps = ['cliente_nombre',
                 'cliente_apellidos',
                 'cups_sips',
                 'cups_name',
                 'tarifa_oferta',
                 'consumo_anual_previsto',
                 'consumo_anual_previsto_directo',
                 'llista_preu',
                 'cups_id_poblacio']
        dades_crm = crm_obj.read(cursor, uid, offer_id, camps, context)
        surnames = dades_crm['cliente_apellidos']
        name = dades_crm['cliente_nombre']
        if not surnames:
            surnames = ''
        if not name:
            name = ''
        titular = surnames + ', ' + name

        tarifa_acces = dades_crm['tarifa_oferta']

        if dades_crm['cups_sips']:
            cups = dades_crm['cups_sips']
        else:
            cups = dades_crm['cups_name']

        if dades_crm['consumo_anual_previsto_directo']:
            consumo_firmado = dades_crm['consumo_anual_previsto_directo']
        elif dades_crm['consumo_anual_previsto'].isdigit():
            consumo_firmado = dades_crm['consumo_anual_previsto']
        else:
            consumo_firmado = ''

        llista_preu = dades_crm['llista_preu']
        poblacio = dades_crm['cups_id_poblacio']

        #Generem un dict ordenat per escriure-ho
        dades = collections.OrderedDict()
        dades['Cliente'] = titular
        dades['Cups'] = cups
        dades['Tarifa peaje'] = tarifa_acces[1]
        dades['Consumo anual firmado'] = str(consumo_firmado)
        dades['Distribución consumo'] = ''
        dades['Lista de precios'] = llista_preu[1]
        dades['Actividad cliente'] = ''
        dades['Localidad PS'] = poblacio[1]
        dades['Cliente actual'] = ''
        dades['Fecha vencimiento'] = ''

        headers = ';'.join(dades.keys())
        dades = ';'.join(dades.values())
        wizard.write({'output': "{0}\n{1}".format(headers, dades)
                    })
        return


    _columns = {
        'offer_id': fields.many2one('crm.polissa', 'Id de la oferta'),
        'output': fields.text('Datos'),
    }

WizardCopiarDatosOferta()
