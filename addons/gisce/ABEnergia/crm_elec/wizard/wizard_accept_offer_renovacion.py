# -*- coding: utf-8 -*-

from osv import osv, fields
from gestionatr.defs import TABLA_17, CONTROL_POTENCIA, TABLA_8

class WizardAcceptOfferRenovacion(osv.osv_memory):

    _name = 'wizard.accept.offer.renovacion'

    tarifa_oferta = ''
    tarifa_client = ''
    potencia_oferta = {'power_p1': 0,
                       'power_p2': 0,
                       'power_p3': 0,
                       'power_p4': 0,
                       'power_p5': 0,
                       'power_p6': 0,
                       }
    potencia_client = {'power_p1': 0,
                       'power_p2': 0,
                       'power_p3': 0,
                       'power_p4': 0,
                       'power_p5': 0,
                       'power_p6': 0,
                       }

    def solicitar_m1(self, cursor, uid, ids, context=None):
        if not context:
            return False

        imd = self.pool.get('ir.model.data')
        crm_polissa_obj = self.pool.get('crm.polissa')
        polissa_obj = self.pool.get('giscedata.polissa')
        modconwz_obj = self.pool.get('giscedata.switching.mod.con.wizard')
        oferta = crm_polissa_obj.read(cursor, uid, context['active_ids'])[0]
        params = [('name', '=', oferta['numero_ctto'])]
        polissa_id = polissa_obj.search(cursor, uid, params)[0]
        context['pol_id'] = polissa_id
        context['cas'] = 'M1'

        wizard_data = self.read(cursor, uid, ids)[0]

        for tarifa in TABLA_17:
            if wizard_data['tarifa_oferta'].name == tarifa[1]:
                tarifa_sel = tarifa[0]
                break

        vals = {'tariff': tarifa_sel,
                'activacio_cicle': 'A',
                'firma_nous_preus': 'c',
                'cnae': oferta['cnae'][0],
                'change_retail_tariff': True,
                'retail_tariff': oferta['llista_preu'][0],
                'power_p1': int(wizard_data['oferta_power_p1']),
                'power_p2': int(wizard_data['oferta_power_p2']),
                'power_p3': int(wizard_data['oferta_power_p3']),
                'power_p4': int(wizard_data['oferta_power_p4']),
                'power_p5': int(wizard_data['oferta_power_p5']),
                'power_p6': int(wizard_data['oferta_power_p6']),
                }

        wz_id = modconwz_obj.create(cursor, uid, vals, context)
        res = modconwz_obj.onchange_atr(cursor, uid, ids[0],
                                        vals['tariff'],
                                        int(self.potencia_oferta['power_p1']),
                                        int(self.potencia_oferta['power_p2']),
                                        int(self.potencia_oferta['power_p3']),
                                        int(self.potencia_oferta['power_p4']),
                                        int(self.potencia_oferta['power_p5']),
                                        int(self.potencia_oferta['power_p6']),
                                        CONTROL_POTENCIA[0],
                                        polissa_id,
                                        vals['change_retail_tariff'],
                                        vals['retail_tariff'],
                                        context,
                                        )

        wizard_vals = res['value']
        wizard_vals.update({'retail_tariff':  oferta['llista_preu'][0]})
        modconwz_obj.write(cursor, uid, [wz_id], wizard_vals, context)

        stage_id = imd.get_object_reference(
            cursor, uid, 'crm_elec', 'crm_oferta_solicitud_atr_stage')[1]
        vals = {'stage_id': stage_id
                }
        crm_polissa_obj.write(cursor, uid, context['active_ids'], vals)

        return {
            'type': 'ir.actions.act_window',
            'res_model': 'giscedata.switching.mod.con.wizard',
            'view_mode': 'form',
            'view_type': 'form',
            'res_id': wz_id,
            'target': 'new',
            'context': context,
        }

    def renovar_contrato(self, cursor, uid, ids, context=None):
        ''' wizard que s'obre al acceptar una oferta de renovació
        '''
        if context is None:
            context = {}

        oferta_id = context['active_id']
        crm_polissa_obj = self.pool.get('crm.polissa')
        crm_polissa_obj.accept_offer_renovacion(cursor, uid, [oferta_id],
                                                context)

        vals = {
            'state': 'end',
            'info': 'Contrato renovado correctamente',
        }
        self.write(cursor, uid, ids, vals, context)

    def comprova_diferencies(self, cursor, uid, ids, context=None):

        potencia_oferta = {'power_p1': 0,
                           'power_p2': 0,
                           'power_p3': 0,
                           'power_p4': 0,
                           'power_p5': 0,
                           'power_p6': 0,
                           }
        potencia_client = {'power_p1': 0,
                           'power_p2': 0,
                           'power_p3': 0,
                           'power_p4': 0,
                           'power_p5': 0,
                           'power_p6': 0,
                           }

        diferencia_tarifa = False
        diferencia_potencias = False

        crm_polissa_obj = self.pool.get('crm.polissa')
        polissa_obj = self.pool.get('giscedata.polissa')
        wz_obj = self.pool.get('wizard.accept.offer.renovacion')
        crm_pot_obj = self.pool.get('crm.potencia.contractada.periode')
        pot_periode_polissa_obj = self.pool.get(
                        'giscedata.polissa.potencia.contractada.periode')

        crm_polissa_id = context.get('active_id')
        oferta = crm_polissa_obj.browse(cursor, uid, crm_polissa_id)
        params = [('name', '=', oferta.numero_ctto)]
        polissa_id = polissa_obj.search(cursor, uid, params)[0]
        polissa = polissa_obj.browse(cursor, uid, polissa_id)

        # Mirem si la tarifa es diferent
        tarifa_oferta = oferta.tarifa_oferta
        tarifa_client = polissa.tarifa

        if polissa.tarifa != oferta.tarifa_oferta:
            diferencia_tarifa = True

        # Mirem si les potències contractades varien
        pots_polissa = polissa_obj.read(cursor, uid, polissa_id,
                                        ['potencies_periode'])['potencies_periode']

        params = [('crm_polissa.id', '=', crm_polissa_id),
                  ('tipus', '=', 'oferta'),
                  ]
        pot_ids = crm_pot_obj.search(cursor, uid, params)

        for pot_ofertada in crm_pot_obj.browse(cursor, uid, pot_ids):
            nom_lower = pot_ofertada.periode.lower()
            # Per 1000 per passar-les a W
            pot = int(pot_ofertada.potencia * 1000)
            potencia_oferta['power_' + nom_lower] = pot

        for periode_id in pots_polissa:
            periode = pot_periode_polissa_obj.browse(cursor, uid, periode_id)
            nom_periode = periode.periode_id.name
            pot_client = periode.potencia
            nom_lower = nom_periode.lower()

            # Per 1000 per passar-les a W
            potencia_client['power_' + nom_lower] = int(pot_client * 1000)

        # comprovar si son diferents
        for pot in potencia_oferta:
            if potencia_oferta[pot] != potencia_client[pot]:
                diferencia_potencias = True
                break

        info_tarifapotencia = '''
            Tarifa actual: {0}
            Tarifa oferta: {1}\n
            Potencias  actuales:  P1: {2}, P2: {3}, P3: {4}, P4: {5}, P5: {6}, P6: {7}   
            Potencias ofertadas: P1: {8}, P2: {9}, P3: {10}, P4: {11}, P5: {12}, P6: {13}\n
            ''' .format(tarifa_client.name,
                        tarifa_oferta.name,
                        potencia_client['power_p1'],
                        potencia_client['power_p2'],
                        potencia_client['power_p3'],
                        potencia_client['power_p4'],
                        potencia_client['power_p5'],
                        potencia_client['power_p6'],
                        potencia_oferta['power_p1'],
                        potencia_oferta['power_p2'],
                        potencia_oferta['power_p3'],
                        potencia_oferta['power_p4'],
                        potencia_oferta['power_p5'],
                        potencia_oferta['power_p6'],
                        )

        info = 'La tarifa y potencias ofertadas son iguales a las actuales.' \
               'No es necesario solicitar M1. \n' \
               'Al hacer clic en Aceptar, se aplicarán los precios al contrato.' \
               'Los precios se aplicarán en la siguiente factura de cliente.\n'

        if diferencia_potencias or diferencia_tarifa:
            info = 'La tarifa y/o potencias ofertadas son diferentes ' \
                   'a las actuales.\nHaga clic en Generar M1 para ' \
                   'solicitar un M1. Los nuevos precios se aplicarán con ' \
                   'la activación de los cambios por ATR.'

        info_tarifapotencia += (info)

        wz_obj.write(cursor, uid, ids,
                     {'info': info_tarifapotencia,
                      'state': 'obert',
                      'need_m1': diferencia_tarifa or diferencia_potencias,
                      'tarifa_client': tarifa_client,
                      'tarifa_oferta': tarifa_oferta,
                      'client_power_p1': potencia_client['power_p1'],
                      'client_power_p2': potencia_client['power_p2'],
                      'client_power_p3': potencia_client['power_p3'],
                      'client_power_p4': potencia_client['power_p4'],
                      'client_power_p5': potencia_client['power_p5'],
                      'client_power_p6': potencia_client['power_p6'],
                      'oferta_power_p1': potencia_oferta['power_p1'],
                      'oferta_power_p2': potencia_oferta['power_p2'],
                      'oferta_power_p3': potencia_oferta['power_p3'],
                      'oferta_power_p4': potencia_oferta['power_p4'],
                      'oferta_power_p5': potencia_oferta['power_p5'],
                      'oferta_power_p6': potencia_oferta['power_p6'],
                      }
        )

        return diferencia_tarifa or diferencia_potencias

    _defaults = {
        'state': lambda *a: 'init',
    }

    _states_selection = [
        ('init', 'Init'),
        ('obert', 'Obert'),
        ('end', 'End'),
    ]

    _columns = {
        'info': fields.text('Info'),
        'state': fields.selection(_states_selection, 'Estat', size=32,
                                  required=True, readonly=True),
        'need_m1': fields.boolean('Generar M1'),
        'tarifa_client': fields.many2one('product.pricelist'),
        'tarifa_oferta': fields.many2one('product.pricelist'),
        'client_power_p1': fields.integer('Cliente P1'),
        'client_power_p2': fields.integer('Cliente P2'),
        'client_power_p3': fields.integer('Cliente P3'),
        'client_power_p4': fields.integer('Cliente P4'),
        'client_power_p5': fields.integer('Cliente P5'),
        'client_power_p6': fields.integer('Cliente P6'),
        'oferta_power_p1': fields.integer('Oferta P1'),
        'oferta_power_p2': fields.integer('Oferta P2'),
        'oferta_power_p3': fields.integer('Oferta P3'),
        'oferta_power_p4': fields.integer('Oferta P4'),
        'oferta_power_p5': fields.integer('Oferta P5'),
        'oferta_power_p6': fields.integer('Oferta P6'),
    }

WizardAcceptOfferRenovacion()

