# -*- coding: utf-8 -*-
import click

from erppeek import Client

'''Script para comercializadoras que hace los siguiente:


'''

@click.command()
@click.option('-uc', '--user_comer', default='admin', help='Usuari servidor ERP COMER')
@click.option('-wc', '--password_comer', help='Contrasenya usuari ERP COMER')
@click.option('-sc', '--server_comer', default='localhost', help=u'IP servidor ERP COMER')
@click.option('-pc', '--port_comer', default=18069, help='Port servidor ERP COMER', type=click.INT)
@click.option('-dc', '--database_comer', default='abenergia', help='Nom de la base de dades de COMER')
def actualizar_crm_vnc(**kwargs):

    server = 'http://{0}:{1}'.format(kwargs['server_comer'],
                                     kwargs['port_comer'])
    client = Client(server=server,
                    db=kwargs['database_comer'],
                    user=kwargs['user_comer'],
                    password=kwargs['password_comer'])

    actualizar_crm_vnc_obj = client.model('crear.crm.vnc')
    actualizar_crm_vnc_obj.actualizar_casos_vnc()

    return True

if __name__ == '__main__':
    actualizar_crm_vnc()
