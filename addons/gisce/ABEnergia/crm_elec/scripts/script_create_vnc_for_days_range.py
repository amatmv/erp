# -*- coding: utf-8 -*-
import logging
from erppeek import Client
from datetime import datetime, timedelta

import click
from progressbar import ProgressBar, ETA, Percentage, Bar


def setup_log(instance, filename):
    log_file = filename
    logger = logging.getLogger(instance)
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr = logging.FileHandler(log_file)
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(logging.INFO)


def set_config_variable(connection, day):
    '''
    Sets dies_antelacio_avis_venciment with day value
    :param connection: connection to server-db
    :param day: integer param sets config variable
    :return: True
    '''
    config_obj = connection.model('res.config')
    config_var_id = config_obj.search(
        [('name', '=', 'dies_antelacio_avis_venciment')]
    )
    config_obj.write(config_var_id, {'value': day})
    return True


def create_vnc_on_range(connection, end_day):
    '''
    This function create VNC cases for range(today -> today+end_day) and finally
    close cases for original value of config var.
    :param connection: connection to server-db
    :param end_day: integer param final value for configuration variable
    :return: tuple with created and closed VNC cases
    '''
    actualizar_crm_vnc_obj = connection.model('crear.crm.vnc')
    stored_config = actualizar_crm_vnc_obj.get_delay_config_var()
    vnc_cases = []
    for day in range(0, end_day+1):
        set_config_variable(connection, day)
        vnc_cases = vnc_cases + actualizar_crm_vnc_obj.crear_casos_vnc()
    set_config_variable(connection, stored_config)
    closed_cases = actualizar_crm_vnc_obj.cerrar_casos_vnc()

    return (vnc_cases, closed_cases)


@click.command()
@click.option('-s', '--server', default='http://localhost',
              help=u'Adreça servidor ERP')
@click.option('-p', '--port', default=8069, help='Port servidor ERP',
              type=click.INT)
@click.option('-u', '--user', default='admin', help='Usuari servidor ERP')
@click.option('-w', '--password', help='Contrasenya usuari ERP', prompt=True,
              hide_input=True)
@click.option('-d', '--database', help='Nom de la base de dades')
@click.option('-fd', '--finalday', default=90,
              help='Enter que defineix el dia final')
def crear_casos_rang(**kwargs):
    server = '{}:{}'.format(kwargs['server'], kwargs['port'])
    c = Client(server=server, db=kwargs['database'], user=kwargs['user'],
               password=kwargs['password'])
    current_date = datetime.now()
    end_date = current_date + timedelta(days=kwargs['finalday'])
    current_date = unicode(current_date.replace(microsecond=0))
    end_date = unicode(end_date.replace(microsecond=0))

    log_name = 'casos_vnc_on_range_{0}_{1}'.format(current_date, end_date)

    setup_log(log_name, '/tmp/'+log_name+'.log')

    logger = logging.getLogger(log_name)
    logger.info('#######Start now {0}'.format(datetime.today()))

    vnc_cases = create_vnc_on_range(c, kwargs['finalday'])
    logger.info('S\'han creat {0} casos amb id\'s:'.format(len(vnc_cases[0])))
    logger.info(str(vnc_cases[0])[1:-1])
    logger.info('S\'han tancat {0} casos amb id\'s:'.format(len(vnc_cases[1])))
    logger.info(str(vnc_cases[1])[1:-1])
    logger.info('#######End process at {0}'.format(datetime.today()))


if __name__ == '__main__':
    crear_casos_rang()
