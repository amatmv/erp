# -*- coding: utf-8 -*-
import base64
from operator import itemgetter, attrgetter
from datetime import datetime
from calendar import isleap
from ast import literal_eval

import netsvc
from osv import osv, fields
from addons.giscedata_sips.giscedata_sips import (
    get_distri, get_poblacio, get_tarifa,
)
from addons.giscedata_energia_prevista.energia import *
from dateutil.relativedelta import relativedelta
from datetime import timedelta, datetime
from addons.oorq.decorators import job
from libfacturacioatr.tarifes import maximetre

import pypdftk
import tempfile


TIPUS_SELECTION = [('client', 'client'), ('oferta', 'oferta')]

TIPOLOGIA_SELECTION = [('documentacio_client', 'DOCUMENTACIÓN CLIENTE'),
                       ('documentos_generados', 'DOCUMENTOS GENERADOS'),
                       ('documentos_finales', 'DOCUMENTOS FIRMADOS/FINALES'),
                       ('documentos_importados', 'DOCUMENTOS IMPORTADOS'),
                       ('documentos_oferta_cliente', 'OFERTAS CLIENTES')]

DADES_MODIFICAR_SELECTION = [('client_energia',
                              'Datos cliente - Término energia'),
                             ('client_potencia',
                              'Datos cliente - Término potencia'),
                             ('client_consums',
                              'Datos cliente - Consums'),
                             ('oferta_energia',
                              'Datos oferta - Término energia'),
                             ('oferta_potencia',
                              'Datos oferta - Término potencia'),
                             ('oferta_consums',
                              'Datos oferta - Consums'),
                             ('dades_polissa',
                              u'Datos completos de la póliza')]

TIPUS_DOCUMENT_SELECTION = [('certificado', 'Certificado'),
                            ('cif_nif', 'CIF/NIF'), ('boletin', 'Boletín'),
                            ('comparativa', 'Comparativa'),
                            ('contrato', 'Contrato'),
                            ('factura', 'Factura consumo'),
                            ('licencia', 'Licencia'),
                            ('mandato', 'Mandato bancario'),
                            ('oferta', 'Oferta'),
                            ('otros', 'Otros'),
                            ('plantilla', 'Plantilla'),
                            ('ref_catastral', 'Ref. catastral'),
                            ('solicitud', 'Solicitud baja'), ]

TIPUS = {'client': 'client', 'oferta': 'oferta'}

TIPOLOGIA = {'documentacio_client': 'documentacio_client',
             'documentos_generados': 'documentos_generados',
             'documentos_finales': 'documentos_finales',
             'documentos_importados': 'documentos_importados',
             'documentos_oferta_cliente': 'documentos_oferta_cliente'}

TIPO_OFERTA = [('simple', 'Oferta simple'),
               ('renovacion', 'Oferta para renovación'),
               ('vuelve', 'Oferta cliente vuelve')]

TIPO_PERSONA = [('fisica', 'Persona física'),
                ('juridica', 'Persona jurídica')]

TIPO_DOCUMENTO = [('nif', 'NIF'), ('cif', 'CIF'), ('nie', 'NIE')]

ESTADO_SIPS = [('encontrado', 'SIPS encontrado'),
               ('no_encontrado', 'SIPS no encontrado'),
               ('importado', 'SIPS importado')]

PERIODES = [('P%s' % p,) * 2 for p in range(1, 7)]

SUBSISTEMES = [('CEUTA', 'Ceuta'),
               ('FUERTEVENTURA-LANZAROTE', 'Fuerteventura-Lanzarote'),
               ('GRAN CANARIA', 'Gran Canaria'), ('HIERRO', 'Hierro'),
               ('IBIZA-FORMENTERA', 'Ibiza-Formentera'),
               ('LA GOMERA', 'La Gomera'), ('LA PALMA', 'La Palma'),
               ('MALLORCA-MENORCA', 'Mallorca-Menorca'),
               ('MELILLA', 'Melilla'), ('PENINSULA', 'Península'),
               ('TENERIFE', 'Tenerife')]

FACTURACION_ATR_INDEXADA = [('atr', 'ATR'),
                            ('index', 'Indexada')]


class consums_one2many(fields.one2many):
    def get(self, cr, obj, ids, name, user=None, offset=0, context=None, values=None):
        if not context:
            context = {}
        if self._context:
            context = context.copy()
        context.update(self._context)
        if not values:
            values = {}
        res = {}
        for id in ids:
            res[id] = obj.pool.get(self._obj).search(cr, user, [(self._fields_id, 'in', ids)], limit=self._limit, context=context)
        return res


class CrmPolissaValidez(osv.osv):

    _name = 'crm.polissa.validez'

    _order = 'position asc'

    _columns = {
        'name': fields.char(string='Nombre', size=32),
        'description': fields.char(string=u'Descripción', size=128),
        'num_days': fields.integer(u'Nº días'),
        'position': fields.integer('Orden')
    }

    _defaults = {
        'position': lambda *a: 10
    }

    _sql_constraints = [('name_uniq', 'unique (name)',
                         u'Ya existe un periodo de validez con este nombre')]
CrmPolissaValidez()


class CrmPolissaConfig(osv.osv):

    _name = 'crm.polissa.config'

    _columns = {
        'name': fields.char(string='Nombre', size=32,
                            readonly="1",
                            ),
        'value': fields.char(string=u'Valor', size=128,
                             write=['crm_elec.crm_director_comercial'],
                             ),
    }

CrmPolissaConfig()

class CrmPolissaDuracion(osv.osv):

    _name = 'crm.polissa.duracion'

    _order = 'position asc'

    _columns = {
        'name': fields.char(string='Nombre', size=32),
        'num_month': fields.integer(u'Nº meses'),
        'position': fields.integer('Orden')
    }

    _defaults = {
        'position': lambda *a: 10
    }

    _sql_constraints = [('name_uniq', 'unique (name)',
                         u'Ya existe una duración con este nombre')]
CrmPolissaDuracion()


def get_consums_anuals_periode(consums):
    sum_consums = {}
    consums = sorted(consums, key=attrgetter('periode'))
    years = {}
    for consum in consums:
        cons = getattr(consum, 'consum')
        periode = getattr(consum, 'periode')
        data_inici = datetime.strptime(getattr(consum, 'data_inici'),
                                       '%Y-%m-%d')
        data_final = datetime.strptime(getattr(consum, 'data_final'),
                                       '%Y-%m-%d')
        try:
            sum_consums[periode].append(Consum('CUPS', periode, data_inici,
                                               data_final, cons))
        except KeyError:
            sum_consums[periode] = []
            sum_consums[periode].append(Consum('CUPS', periode, data_inici,
                                               data_final, cons))
        try:
            years[data_final.year] += 1
        except KeyError:
            years[data_final.year] = 1
    consums_periode = {}
    if not years:
        return consums_periode

    year = sorted(years.items(), key=itemgetter(1), reverse=True)[0][0]
    for consum in sum_consums.items():
        periode = consum[0]
        totals = Consum.totalizer(consum[1])
        norm = Normalizer(totals, 'consum')
        cons = norm.normalize()
        cons_anual = 0
        for c in cons:
            if datetime(year, 1, 1) <= c[0] <= datetime(year, 12, 31):
                cons_anual += c[1]
        consums_periode[periode] = cons_anual
    return consums_periode


class CrmPolissa(osv.osv):
    """Polissa per fer ofertes a través del CRM.
    """
    _name = 'crm.polissa'
    _inherits = {'poweremail.conversation': 'conversation_id'}

    _order = 'id desc'
    _polissa_states_selection = [
        ('esborrany', 'Esborrany'),
        ('validar', 'Validar'),
        ('pendent', 'Pendent'),
        ('activa', 'Activa'),
        ('contracte', 'Activació Contracte'),
        ('novapolissa', 'Creació nova pòlissa'),
        ('modcontractual', 'Modificació Contractual'),
        ('impagament', 'Impagament'),
        ('tall', 'Tall'),
        ('baixa', 'Baixa')
    ]

    def write(self, cursor, uid, ids, vals, context=None):
        if 'active_id' in vals:
            context = vals
            vals = {}
        if 'stage_id' in vals:
            stage_obj = self.pool.get('crm.case.stage')
            stage = stage_obj.read(cursor, uid, vals.get('stage_id'), ['name'])
            message = 'Cambio de estado a %s ' % (stage['name'])
            self.message_append(cursor, uid, ids, text=message)
        return super(CrmPolissa, self).write(cursor, uid, ids, vals,
                                                      context)

    def _default_section_id(self, cursor, uid, context=None):
        if not context:
            context = {}
        imd = self.pool.get('ir.model.data')
        return imd.get_object_reference(
            cursor, uid, 'crm_elec', 'crm_oferta_section')[1]

    def _default_stage_id(self, cursor, uid, context=None):
        if not context:
            context = {}
        imd = self.pool.get('ir.model.data')
        return imd.get_object_reference(
            cursor, uid, 'crm_elec', 'crm_oferta_solicitud_oferta_stage')[1]

    def _default_delegacion(self, cursor, uid, context=None):
        employee_obj = self.pool.get('hr.employee')
        employee_id = employee_obj.search(cursor, uid, [('user_id', '=', uid)])
        if employee_id:
            employee_office = employee_obj.read(cursor, uid, employee_id[0],
                                                ['work_office'])
            if employee_office.get('work_office', False):
                return employee_office['work_office'][0]
        return False

    def _default_campaign_id(self, cursor, uid, context=None):
        campanya_obj = self.pool.get('crm.case.resource.type')
        campanya_ids_list = campanya_obj.search(cursor, uid, [('name', '=', 'General')])
        if campanya_ids_list:
           return campanya_ids_list[0]
        return False

    def _default_canal(self, cursor, uid, context=None):
        canal_model = self.pool.get('res.partner.canal')
        canal_list = canal_model.search(cursor, uid, [('name', '=', 'Directo')])
        if canal_list:
           return canal_list[0]
        return False

    def _default_numero_oferta(self, cursor, uid, context=None):
        # Afegim el numero de sequencia si no el tenim i l'escrivim
        num_oferta = self.pool.get('ir.sequence').get(cursor, uid,
                                            'crm.polissa')
        return num_oferta

    def generar_periodes_potencia_oferta(self, cursor, uid, ids, context=None):
        if 'tipus' in context:
            tipus = TIPUS[context['tipus']]
        else:
            tipus = TIPUS['client']

        crm_pot_obj = self.pool.get('crm.potencia.contractada.periode')
        giscedata_tp_obj = self.pool.get('giscedata.tp')
        # Eliminem els periodes de potencia actuals
        for crm in self.browse(cursor, uid, ids, context=context):
            p_ids = crm_pot_obj.search(cursor, uid,
                                   [('crm_polissa.id', '=', crm.id),
                                    ('tipus','=', tipus)],
                                   context={'active_test': False})
            crm_pot_obj.unlink(cursor, uid, p_ids)
            for periode in crm.tarifa_oferta.periodes: # periodes ofertats
                if periode.tipus == 'tp' and not periode.agrupat_amb:
                    if context and context['tipus'] == 'oferta':
                        # si estem creant una oferta, per defecte els preus
                        # de tp que siguin els preus dels peatges sobre el tp
                        peatje_price = giscedata_tp_obj.\
                                        get_peatge_price_from_period_id(cursor,
                                                                    uid,
                                                                    periode.id)
                        preu = peatje_price
                    else:
                        preu = 0.0
                    crm_pot_obj.create(cursor, uid, {
                    'periode': periode.name,
                    'crm_polissa': crm.id,
                    'potencia': crm.potencia_client,
                    'preu': preu,
                    'tipus': tipus
                    })
        return True

    def generar_periodes_potencia(self, cursor, uid, ids, context=None):
        crm_pot_obj = self.pool.get('crm.potencia.contractada.periode')
        # Eliminem els periodes de potencia actuals
        for crm in self.browse(cursor, uid, ids, context=context):
            p_ids = crm_pot_obj.search(cursor, uid,
                                   [('crm_polissa.id', '=', crm.id),
                                    ('tipus','=', TIPUS['oferta'])],
                                   context={'active_test': False})
            crm_pot_obj.unlink(cursor, uid, p_ids)
            for periode in crm.tarifa_client.periodes:
                if periode.tipus == 'tp' and not periode.agrupat_amb:
                    crm_pot_obj.create(cursor, uid, {
                    'periode_id': periode.id,
                    'crm_polissa': crm.id,
                    'potencia': crm.potencia_client,
                    'preu': 0.0,
                    'tipus': TIPUS['oferta']
                    })
        return True

    def import_attachment(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        mail_message_obj = self.pool.get('mail.message')
        crm_documents_obj = self.pool.get('crm.documentacio')
        for crm_pol_id in ids:
            if 'current_missatge' in context:
                missatge_id = context['current_missatge']
                vals = {'attach_id': False,
                        'crm_polissa': crm_pol_id,
                        'tipus_document': 'altres',
                        'tipologia': TIPOLOGIA['documentos_importados'],
                        'versio': 0,}
                mail = mail_message_obj.read(cursor, uid, missatge_id,
                                             ['attachment_ids'])
                for attach_id in mail['attachment_ids']:
                    vals.update({'attach_id': attach_id})
                    crm_documents_obj.create(cursor, uid, vals)

        return True

    def find_sips(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        sips_obj = self.pool.get('giscedata.sips.ps.fmtjul16')
        crm_pol_obj = self.pool.get('crm.polissa')
        sips_name = context.get('cups_sips', False)
        if sips_name:
            search_param = [('name', '=', sips_name)]
            sips_id = sips_obj.search(cursor, uid, search_param)
            if sips_id:
                vals = {'sips_estado': 'encontrado'}
                crm_pol_obj.write(cursor, uid, ids, vals)
            else:
                vals = {'sips_estado': 'no_encontrado'}
                crm_pol_obj.write(cursor, uid, ids, vals)

        return True

    def import_consums_from_sips(
            self, cursor, uid, crm_polissa_id, id_tarifa, cups, tipus):
        # si no tenim tarifa, sortim sense importar consums
        if id_tarifa == None:
            return False
        sips_consums_obj = self.pool.get('giscedata.sips.consums.fmtjul16')
        crm_consums_obj = self.pool.get('crm.consums.periode')

        # si ja n'hi havien importats, els esborrem abans
        c_ids = crm_consums_obj.search(cursor, uid,
                               [('crm_polissa.id', '=', crm_polissa_id)],
                               context={'active_test': False})
        if c_ids:
            crm_consums_obj.unlink(cursor, uid, c_ids)

        # comprovar que està instal·lat el mòdul
        if not sips_consums_obj:
            raise osv.except_osv('Error', 'You must install SIPS module')

        # buscar els consums d'aquell cups
        lectures_id = sips_consums_obj.search(cursor, uid, [
            ('name', '=', cups),
        ], order='data_final asc')

        # Si tenim cap o només 1 consum per aquell CUPS, sortim
        if not lectures_id or len(lectures_id) == 1:
            return False

        periodes_obj = self.pool.get('giscedata.polissa.tarifa.periodes')
        rel_obj = self.pool.get('crm.consums.periode.ui.rel')
        search_params = [('tarifa.id', '=', id_tarifa), ('tipus', '=', 'te')]
        periodes = periodes_obj.search_count(cursor, uid, search_params)

        lectures_desc = sorted(sips_consums_obj.read(cursor, uid, lectures_id),
                              key=itemgetter('data_final'), reverse=True)

        data_mes_recent = datetime.strptime(
                lectures_desc[0]['data_final'],'%Y-%m-%d') # lectura més recent

        # portem els 14 mesos més recents de consums
        # volem iterar a partir de la segona lectura, per poder tenir
        # la lectura inicial dels periodes de lectura (que serà la data final
        # del periode anterior + 1 dia)
        for pos in range (1, len(lectures_desc)):

            lectura = lectures_desc[pos]
            lectura_anterior = lectures_desc[pos - 1]

            # la lectura inicial d'un periode serà la lectura final del periode
            # anterior més 1 dia
            lectura_anterior['data_inicial'] = datetime.strptime(
                lectura['data_final'],'%Y-%m-%d') + timedelta(days=1)

            # crear el periode corresponent a la lect anterior
            for x in range(1, periodes + 1):
                consum = lectura_anterior.get('activa_%s' % x)

                vals = {
                    'crm_polissa': crm_polissa_id,
                    'periode': 'P%s' % x,
                    'data_inici': lectura_anterior.get('data_inicial'),
                    'data_final': lectura_anterior.get('data_final'),
                    'consum': consum,
                    'tipus': tipus
                }
                crm_consums_obj.create(cursor, uid, vals)

            rel_obj.create(cursor, uid, {
                'crm_polissa': crm_polissa_id,
                'data_inici': lectura_anterior.get('data_inicial')
            })

            # les lectures anteriors als 14 mesos mes recents no les volem
            if datetime.strptime(lectura['data_final'],'%Y-%m-%d') < \
                data_mes_recent - (timedelta(days = 30 * 14)):
                break

        return True

    def import_potencies_from_sips(self, cursor, uid, crm_polissa_id, sips,
                                   tipus):
        crm_pot_obj = self.pool.get('crm.potencia.contractada.periode')

        # si ja n'hi havien importats, els esborrem abans
        p_ids = crm_pot_obj.search(cursor, uid,
                               [('crm_polissa.id', '=', crm_polissa_id)],
                               context={'active_test': False})
        if p_ids:
            crm_pot_obj.unlink(cursor, uid, p_ids)

        # creem les potencies per cada periode
        for x in xrange(1, 7):
            pot = sips['pot_cont_p%s' % x]
            if pot:
                vals = {
                    'crm_polissa': crm_polissa_id,
                    'periode': 'P%s' % x,
                    'potencia': pot,
                    'tipus': tipus
                }
                crm_pot_obj.create(cursor, uid, vals)
        return True

    def btn_import_sips(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        if not isinstance(ids, list):
            ids = ids[0]
        if context.get('tipus', False):
           tipus = context.get('tipus')
        else:
           raise osv.except_osv('Error', 'Indicate tipus on context')
        if context.get('cups_sips', False):
           sips_name = context.get('cups_sips', 'client')
        else:
           raise osv.except_osv('Error', 'Indicate sips_name on context')

        self.find_sips(cursor, uid, ids, context=context)
        for crm_polissa in self.read(cursor, uid, ids, ['sips_estado']):
            if crm_polissa['sips_estado'] == 'encontrado':
                self.import_sips(cursor, uid, ids, sips_name, tipus)
        return True


    def import_sips(self, cursor, uid, ids, sips_name, tipus, context=None):
        sips_obj = self.pool.get('giscedata.sips.ps.fmtjul16')
        if not sips_obj:
            raise osv.except_osv('Error', 'You must install SIPS module')

        muni_obj = self.pool.get('res.municipi')
        pobl_obj = self.pool.get('res.poblacio')
        crm_pol_obj = self.pool.get('crm.polissa')
        prov_obj = self.pool.get('res.country.state')

        if not context:
            context = {}
        if isinstance(ids, list):
            crm_polissa_id = ids[0]
        else:
            crm_polissa_id = ids

        # guardem els errors dels valors que no puguem recuperar de sips
        errors = []

        search_param = [('name', '=', sips_name)]
        sips_id = sips_obj.search(cursor, uid, search_param)[0]
        if sips_id:
            # init values
            id_poblacio = False
            id_municipi = False
            id_provincia = False

            sips = sips_obj.read(cursor, uid, sips_id, context=context)
            ine_provincia = sips.get('ine_provincia', '')
            ine_municipio = sips.get('ine_municipio', '')
            pobl_name = sips.get('poblacio', '')

            param = [('code', '=', ine_provincia)]
            prov = prov_obj.search(cursor, uid, param)
            if prov:
                id_provincia = prov[0]

            param = [('ine', '=', ine_municipio)]
            muni = muni_obj.search(cursor, uid, param)
            if muni:
                id_municipi = muni[0]

            param = [('municipi_id', '=', id_municipi),
                     ('name', 'ilike', pobl_name)]
            pobl = pobl_obj.search(cursor, uid, param)
            if pobl:
                id_poblacio = pobl[0]
            else:
                id_poblacio = get_poblacio(
                    self, cursor, uid, id_provincia, pobl_name)

            try:
                id_tarifa = get_tarifa(self, cursor, uid, sips['tarifa'])
            except:
                id_tarifa = None; errors.append('tarifa')

            potencia_contractada = 0.0
            try:
                for x in range(1, 7):
                    if sips['pot_cont_p%s' % x] > potencia_contractada:
                        potencia_contractada = sips['pot_cont_p%s' % x]
            except:
                errors.append('potencia contratada')

            # mirem si la distri pertany a una de les grans, i si és el cas
            # posem el id de la distri pare
            id_distri = get_distri(self, cursor, uid, sips['distri'],
                                   sips['cod_distri'])
            partner_obj = self.pool.get('res.partner')
            has_parent = partner_obj.read(cursor, uid, id_distri,
                                          ['parent_id'])['parent_id']
            if has_parent: id_distri = has_parent[0]

            vals = {'cups_name': sips['name'],
                    'cups_distribuidora_id': id_distri,
                    'cups_id_municipi': id_municipi,
                    'cups_id_poblacio': id_poblacio,
                    'cups_id_provincia': id_provincia,
                    'cups_dp': sips.get('codi_postal',''),
                    'sips_direccion': sips.get('direccio',''),
                    'sips_titular': sips.get('nom',''),
                    'sips_apellidos': sips.get('cognom',''),
                    'sips_tarifa': sips.get('tarifa',''), # a HC no hi és
                    'tarifa_client': id_tarifa,
                    'tarifa_oferta': id_tarifa,
                    'potencia_client': potencia_contractada,
                    'consumo_anual_previsto_directo':sips.get('promig_total',0),
                    }
            try:
                self.import_potencies_from_sips(
                    cursor, uid, crm_polissa_id, sips,tipus)
            except:
                errors.append('potencias contratadas')

            try:
                self.import_consums_from_sips(cursor, uid, crm_polissa_id,
                                              id_tarifa, sips_name, tipus)
            except Exception as e:
                errors.append('importación consumos')

            if errors:
                # formatem els errors en una string
                str_e = ', '.join(map(str, errors))

                vals.update({'sips_import_state':
                    'Errores en la importación: {0}'.format(str_e)})
            else:
                vals.update({'sips_import_state': 'Ok'})

            crm_pol_obj.write(cursor, uid, [crm_polissa_id], vals)

        return True

    def set_field_function(self, cursor, uid, model, value, extra_values):
        # Arriben els valors a value
        # Values:
        # (0, 0,  { fields })    create
        # (1, ID, { fields })    update (write fields to ID)
        # (2, ID)                remove (calls unlink on ID, that will also delete the relationship because of the ondelete)
        # (3, ID)                unlink (delete the relationship between the two objects but does not delete ID)
        # (4, ID)                link (add a relationship)
        for val in value:
            if val[0] == 0:
                val[2].update(extra_values)
                model.create(cursor, uid, val[2])
            elif val[0] == 1:
                model.write(cursor, uid, val[1], val[2])
            elif val[0] == 2:
                model.unlink(cursor, uid, val[1])

    def _ff_potencies(self, cursor, uid, ids, field_name, arg,
                              context=None):
        res = {}
        crm_pot_obj = self.pool.get('crm.potencia.contractada.periode')
        for oferta_id in ids:
            res[oferta_id] = []
            search_param = [('crm_polissa', '=', oferta_id),
                            ('tipus', '=', arg['tipus'])]
            pots_ids = crm_pot_obj.search(cursor, uid, search_param)
            res[oferta_id] = [pot for pot in pots_ids]
        return res

    def _set_potencies(self, cursor, uid, ids, name, value, arg, context=None):
        if not isinstance(ids, list):
            crm_ids = [ids, ]
        else:
            crm_ids = ids
        crm_pot_obj = self.pool.get('crm.potencia.contractada.periode')
        for crm_id in crm_ids:
            extra_values = {'crm_polissa': crm_id, 'tipus': arg['tipus']}
            self.set_field_function(
                cursor, uid, crm_pot_obj, value,
                extra_values)

    def _ff_energia(self, cursor, uid, ids, field_name, arg,
                              context=None):
        res = {}
        crm_ene_obj = self.pool.get('crm.energia.contractada.periode')
        for oferta_id in ids:
            res[oferta_id] = []
            search_param = [('crm_polissa', '=', oferta_id),
                            ('tipus', '=', arg['tipus'])]
            pots_ids = crm_ene_obj.search(cursor, uid, search_param)
            res[oferta_id] = [pot for pot in pots_ids]
        return res

    def _ff_documents(self, cursor, uid, ids, field_name, arg,
                              context=None):
        res = {}
        crm_docs_obj = self.pool.get('crm.documentacio')
        tipus_document = arg.get('tipus_document', False)
        for oferta_id in ids:
            res[oferta_id] = []
            search_param = [('crm_polissa', '=', oferta_id),
                            ('tipologia', '=', arg['tipologia'])]
            if tipus_document:
                search_param.append(('tipus_document', '=', tipus_document))
            docs_ids = crm_docs_obj.search(cursor, uid, search_param)
            res[oferta_id] = [pot for pot in docs_ids]
        return res

    def _set_energia(self, cursor, uid, ids, name, value, arg, context=None):
        if not isinstance(ids, list):
            crm_ids = [ids, ]
        else:
            crm_ids = ids
        crm_ene_obj = self.pool.get('crm.energia.contractada.periode')
        for crm_id in crm_ids:
            extra_values = {'crm_polissa': crm_id, 'tipus': arg['tipus']}
            self.set_field_function(
                cursor, uid, crm_ene_obj, value, extra_values)

    def _ff_titular(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        if not isinstance(ids, list):
            ids = [ids, ]
        res = dict.fromkeys(ids, False)
        crm_polissa_obj = self.pool.get('crm.polissa')
        for crm_polissa in crm_polissa_obj.read(
                cursor, uid, ids, ['cliente_nombre', 'cliente_apellidos']):
            nombre = crm_polissa.get('cliente_nombre', '') or ''
            apellidos = crm_polissa.get('cliente_apellidos', '') or ''

            res[crm_polissa['id']] = '%s' % apellidos
            if nombre:
                if res[crm_polissa['id']]:
                    res[crm_polissa['id']] += ', %s' % nombre
                else:
                    res[crm_polissa['id']] = nombre
        return res

    def _ff_stage_name(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        if not isinstance(ids, list):
            ids = [ids, ]
        res = dict.fromkeys(ids, False)
        crm_polissa_obj = self.pool.get('crm.polissa')
        for crm_polissa in crm_polissa_obj.read(
                cursor, uid, ids, ['stage_id']):
            if crm_polissa.get('stage_id', False):
                name = crm_polissa.get('stage_id', '')[1]
            else:
                name = False
            res[crm_polissa['id']] = '%s' % name
        return res

    def _ff_tarifa_cliente(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        if not isinstance(ids, list):
            ids = [ids, ]
        res = dict.fromkeys(ids, False)
        crm_polissa_obj = self.pool.get('crm.polissa')
        for crm_polissa in crm_polissa_obj.read(
                cursor, uid, ids, ['tarifa_client']):
            if crm_polissa.get('tarifa_client', False):
                res[crm_polissa['id']] = crm_polissa.get('tarifa_client')[0]

        return res

    def _calc_consum_anual(self, cursor, uid, crm_polissa_id):
        consums_obj = self.pool.get('crm.consums.periode')

        crm_polissa = self.read(cursor, uid, crm_polissa_id,
                                ['client_consums_periode', 'cups_name'])
        fields = ['consum', 'data_inici', 'data_final', 'periode']
        client_consums = sorted(consums_obj.read(
            cursor, uid, crm_polissa['client_consums_periode'], fields),
            key=itemgetter('data_inici'))
        if not client_consums:
            return 'Faltan los consumos'
        skipped = []
        consums = []
        interval = False
        years = {}
        for consum in client_consums:
            if not consum['data_inici']:
                skipped.append(consum)
                continue
            else:
                data_final = datetime.strptime(
                    consum['data_final'], '%Y-%m-%d')
                data_inici = datetime.strptime(
                        consum['data_inici'], '%Y-%m-%d')
                if not interval:
                    days = (data_final-data_inici).days
                    interval = days
                if years.get(data_final.year, False):
                    years[data_final.year] += 1
                else:
                    years[data_final.year] = 1

            consums.append(Consum(crm_polissa['cups_name'], consum['periode'],
                                  data_inici, data_final, consum['consum']))
        for consum in skipped:
            data_inici = datetime.strptime(
                consum['data_final'], '%Y-%m-%d') - relativedelta(days=interval)
            data_final = datetime.strptime(
                    consum['data_final'], '%Y-%m-%d')
            consums.append(Consum(crm_polissa['cups_name'], consum['periode'],
                                  data_inici, data_final, consum['consum']))

        totals = Consum.totalizer(consums) # no ens interessen els periodes
        start_date = totals[0].data_inici
        end_date = totals[len(totals) - 1].data_final
        dies_totals = (end_date - start_date).days

        consum_total = 0
        for c in consums:
            consum_total += c.consum
        consum_anualitzat = int( consum_total / dies_totals ) * 365

        return '{0}'.format(consum_anualitzat)

    def _ff_consumo_anual_previsto(self, cursor, uid, ids, field_name, arg,
                                   context=None):
        if not context:
            context = {}
        if not isinstance(ids, list):
            ids = [ids, ]

        res = dict.fromkeys(ids, False)
        for crm_polissa_id in ids:
            res[crm_polissa_id] = self._calc_consum_anual(
                cursor, uid, crm_polissa_id)
        return res

    def _ff_consums(self, cursor, uid, ids, field_name, arg,
                              context=None):
        res = {}
        crm_ene_obj = self.pool.get('crm.consums.periode')
        for oferta_id in ids:
            res[oferta_id] = []
            search_param = [('crm_polissa', '=', oferta_id),
                            ('tipus', '=', arg['tipus'])]
            pots_ids = crm_ene_obj.search(cursor, uid, search_param)
            res[oferta_id] = [pot for pot in pots_ids]
        return res

    def _set_consums(self, cursor, uid, ids, name, value, arg, context=None):
        if not isinstance(ids, list):
            crm_ids = [ids, ]
        else:
            crm_ids = ids
        crm_consums_obj = self.pool.get('crm.consums.periode')
        for crm_id in crm_ids:
            extra_values = {'crm_polissa': crm_id, 'tipus': arg['tipus']}
            self.set_field_function(
                cursor, uid, crm_consums_obj, value, extra_values)

    def _set_documents(self, cursor, uid, ids, name, value, arg, context=None):
        if not isinstance(ids, list):
            crm_ids = [ids, ]
        else:
            crm_ids = ids
        crm_doc_obj = self.pool.get('crm.documentacio')
        for crm_id in crm_ids:
            extra_values = {'crm_polissa': crm_id,
                            'tipologia': arg['tipologia'],
                            'tipus_document': arg['tipus_document']
                            }
            self.set_field_function(
                cursor, uid, crm_doc_obj, value, extra_values)

    def _ff_missatges_selection(self, cursor, uid, ids, field_name, arg,
                                context=None):
        crm_polissa_obj = self.pool.get('crm.polissa')
        res = {}
        for oferta_id in ids:
            dades_pol = crm_polissa_obj.read(
                cursor, uid, oferta_id, ['current_missatge'])
            if dades_pol and dades_pol['current_missatge']:
                res[oferta_id] = [dades_pol['current_missatge'], ]
            else:
                res[oferta_id] = [0, ]

        return res

    def _missatges_selection(self, cursor, uid, context=None):
        res = []
        crm_lead_obj = self.pool.get('crm.lead')
        crm_polissa_obj = self.pool.get('crm.polissa')
        mail_message_obj = self.pool.get('mail.message')
        if 'active_id' in context and context.get('active_id', False):
            oferta_id = context['active_id']
            case_ids = crm_polissa_obj.read(cursor, uid, oferta_id, ['case_id'])

            if case_ids['case_id']:
                message_ids = crm_lead_obj.read(
                    cursor, uid, case_ids['case_id'][0], ['message_ids'])
                mails = mail_message_obj.read(cursor, uid, message_ids['message_ids'],
                                              ['subject', 'id'])
                res = [(0, 'Selecciona un mensaje'), ]
                res += [(message['id'], message['subject']) for message in mails]
            else:
                res = [(0, 'Selecciona un mensaje'), ]
        return res

    def _ff_body_text(self, cursor, uid, ids, field_name, arg,
                                context=None):
        crm_polissa_obj = self.pool.get('crm.polissa')
        mail_message_obj = self.pool.get('mail.message')
        res = dict.fromkeys(ids, False)
        for oferta_id in ids:
            dades_pol = crm_polissa_obj.read(
                cursor, uid, oferta_id, ['current_missatge'])
            if dades_pol and dades_pol['current_missatge']:
                mail = mail_message_obj.read(
                    cursor, uid, dades_pol['current_missatge'],
                    ['body_text'])
                res[oferta_id] = mail['body_text']
            else:
                res[oferta_id] = ''

        return res

    def _ff_attachment_ids(self, cursor, uid, ids, field_name, arg,
                                context=None):
        crm_polissa_obj = self.pool.get('crm.polissa')
        mail_message_obj = self.pool.get('mail.message')
        res = dict.fromkeys(ids, False)
        for oferta_id in ids:
            dades_pol = crm_polissa_obj.read(
                cursor, uid, oferta_id, ['current_missatge'])
            if dades_pol and dades_pol['current_missatge']:
                mail = mail_message_obj.read(
                    cursor, uid, dades_pol['current_missatge'],
                    ['attachment_ids'])
                res[oferta_id] = mail['attachment_ids']
            else:
                res[oferta_id] = []
        return res

    def _ff_perm_to_modify(self, cursor, uid, ids, field_name, arg,
                           context=None):
        res = dict.fromkeys(ids, False)
        if not context:
            context = {}
        user_obj =self.pool.get('res.users')
        imd_obj = self.pool.get('ir.model.data')
        gid_id = imd_obj.get_object_reference(
            cursor, uid, 'crm_elec', 'crm_director_comercial')[1]
        groups = user_obj.read(cursor, uid, uid, ['groups_id'])['groups_id']
        perm = False
        if gid_id in groups:
            perm = True
        for crm_id in ids:
            res[crm_id] = perm
        return res

    def onchange_missatge(self, cursor, uid, ids, missatge_id, context=None):
        mail_message_obj = self.pool.get('mail.message')
        crm_polissa_obj = self.pool.get('crm.polissa')
        res = {'value': {'body_text': False, 'attachment_ids': []}}
        if missatge_id and missatge_id != 0:
            mail = mail_message_obj.read(cursor, uid, missatge_id,
                                         ['body_text', 'attachment_ids'])
            res['value']['body_text'] = mail['body_text']
            res['value']['attachment_ids'] = mail['attachment_ids']
            crm_polissa_obj.write(
                cursor, uid, ids, {'current_missatge': missatge_id})

        return res

    def onchange_mcp_base(self, cursor, uid, ids, mcp_base, mcp_definitivo,
                          context=None):
        if not context:
            context = {}
        res = {'value': {}}
        if mcp_base:
            mcp_obj = self.pool.get('crm.mcp')
            m_price = mcp_obj.read(
                cursor, uid, mcp_base, ['minimum_price'])['minimum_price']
            if mcp_definitivo:
                res = {'value': {}}
            else:
                res['value']['mcp_definitivo'] = m_price
        else:
            if mcp_definitivo:
                res = {'value': {}}
        return res

    def onchange_mcp_def(self, cursor, uid, ids, mcp_base, mcp_definitivo,
                                        tarifa_oferta, tipus, context=None):
        if context is None:
            context = {}
        res = {}
        if mcp_base:
            mcp_obj = self.pool.get('crm.mcp')
            m_price = mcp_obj.read(
                cursor, uid, mcp_base, ['minimum_price'])['minimum_price']
            if mcp_definitivo < m_price:
                warning = {'title': u'MCP mínimo',
                           'message': u'El MCP definitivo es menor al mínimo '
                                      u'marcado por el MCP base que es '
                                      u'de %s' % m_price}
                res.update({'warning': warning})

        return res

    def onchange_check_cups_code(self, cursor, uid, ids, cupsname,
                                 context=None):
        if not context:
            context = {}
        res = {}
        validation = True
        if cupsname:
            cups_obj = self.pool.get('giscedata.cups.ps')
            try:
                if not cups_obj.check_cups_code(cursor, uid, cupsname):
                    validation = False
            except:
                validation = False
        if not validation:
            warning = {'title': u'Error código CUPS',
                       'message': u'El código CUPS introducido no es '
                                  u'válido'}
            res.update({'warning': warning})
        return res

    def check_cups(self, cursor, uid, ids):
        """Comprova si un CUPS és correcte."""
        cups_obj = self.pool.get('giscedata.cups.ps')
        validation = True
        for cups in self.read(cursor, uid, ids, ['cups_sips', 'cups_name']):
            try:
                if cups['cups_sips']:
                    if not cups_obj.check_cups_code(cursor, uid, cups['cups_sips']):
                        validation = False
            except:
                validation = False
            try:
                if cups['cups_name']:
                    if not cups_obj.check_cups_code(cursor, uid, cups['cups_name']):
                        validation = False
            except:
                validation = False
        if not validation:
            raise osv.except_osv("Error", u"El código del CUPS no es válido.")
        return True

    def unlink_preus(self, cursor, uid, ids, context=None):
        # eliminar els preus existents
        # de energia / transfer / potencia

        crm_pot_obj = self.pool.get('crm.potencia.contractada.periode')
        preus_transferencia_periode = self.pool.get('crm.pt.periode')
        crm_ene_obj = self.pool.get('crm.energia.contractada.periode')

        for crm in self.browse(cursor, uid, ids, context=context):
            potencia_ids = crm_pot_obj.search(cursor, uid,
                                              [('crm_polissa.id', '=', crm.id),
                                               ],
                                              context={'active_test': False})
            crm_pot_obj.unlink(cursor, uid, potencia_ids)

            transferencia_ids = preus_transferencia_periode.search(cursor, uid,
                                                                   [(
                                                                    'crm_polissa.id',
                                                                    '=',
                                                                    crm.id),
                                                                    ],
                                                                   context={
                                                                       'active_test': False})
            preus_transferencia_periode.unlink(cursor, uid, transferencia_ids)

            # esborrem el/s antics
            params = [('crm_polissa.id', '=', crm.id)]
            ene_periodes_ids = crm_ene_obj.search(cursor, uid, params)
            crm_ene_obj.unlink(cursor, uid, ene_periodes_ids)

        return True

    def unlink_preus_comparativa(self, cr, uid, ids, context=None):
        # eliminar els preus existents de energia  / potencia de la comparativa

        pot_comp_obj = self.pool.get('comparativa.potencia.periode')
        con_comp_obj = self.pool.get('comparativa.consums.periode')

        for crm in self.browse(cr, uid, ids, context=context):
            potencia_ids = pot_comp_obj.search(cr, uid,
                                               [('crm_polissa.id', '=', crm.id)],
                                               context={'active_test': False})
            pot_comp_obj.unlink(cr, uid, potencia_ids)

            consums_ids = con_comp_obj.search(cr, uid,
                                               [('crm_polissa.id', '=', crm.id)],
                                               context={'active_test': False})
            con_comp_obj.unlink(cr, uid, consums_ids)

        return True

    def mostrar_preus(self, cursor, uid, ids, context=None):
        '''anem a buscar els preus de la llista seleccionada'''

        if not context: context = {}

        # necessari la data pel price_get
        if 'date' not in context: context['date'] = datetime.today()

        crm_polissa = self.pool.get('crm.polissa')
        pl_obj = self.pool.get('product.pricelist')
        product_obj = self.pool.get('product.product')
        crm_pot_obj = self.pool.get('crm.potencia.contractada.periode')
        crm_ene_obj = self.pool.get('crm.energia.contractada.periode')
        pt_periode = self.pool.get('giscedata.polissa.tarifa.periodes')

        # esborrar els preus anteriors
        self.unlink_preus(cursor, uid, ids)

        # treure les dades que necessitem de la oferta
        oferta_id = crm_polissa.search(cursor, uid, [('id', '=', ids[0])])[0]
        oferta = crm_polissa.read(cursor, uid, oferta_id)
        pl_id = oferta['llista_preu'][0]
        tarifa_oferta_id = oferta['tarifa_oferta'][0]
        potencia_ofertada = oferta['potencia_ofertada']

        # mostrar els periodes potencia
        search_params = [('tarifa.id', '=', tarifa_oferta_id),
                         ('agrupat_amb', '=', False),
                         ('tipus', '=', 'tp')]
        periodes_ids = pt_periode.search(cursor, uid, search_params)

        # mirem si ja existeix algun periode de potencia de client,
        # (que hagues vingut a partir de SIPS o de les potencies actuals
        # si es tracta d'oferta de renovacio)
        params = [('crm_polissa','=',ids[0])]

        for p_id in periodes_ids: # periodes ofertats potencia

            periode = pt_periode.read(cursor, uid, p_id)
            nom_periode = periode['name']

            categ_id = periode['categ_id'][0]

            # a partir del periode busquem l'id del producte
            # tindrà la mateixa categoria i nom de periode
            prod_id = product_obj.search(cursor, uid,
                                        [('categ_id', '=', categ_id),
                                         ('name', '=', nom_periode)])[0]

            # price_get mètode de la llista de preu, que calcula el preu final
            # estigui o no basada sobre una altra llista
            # la llista de preu pl_id és la tarifa de comer escollida
            preu = pl_obj.price_get(cursor, uid, [pl_id],
                                prod_id, 1, context=context)[pl_id]

            # buscar el crm_pot_obj que tenim creat de quan importem sips
            # o quan portem les potències del client, el copiem i li escrivim
            # el preu i el tipus (oferta en comptes de client) mantenint la
            # potencia per periode
            crmpot_id = crm_pot_obj.search(cursor, uid,
                                           [('crm_polissa','=',ids[0]),
                                            ('periode','=',nom_periode)])
            if crmpot_id:
                crmpot_id = crmpot_id[0]
                crmpotoffer_id = crm_pot_obj.copy(cursor, uid, crmpot_id)
                vals = {'preu': preu,
                        'potencia': potencia_ofertada,
                        'tipus': 'oferta'}
                crm_pot_obj.write(cursor, uid, crmpotoffer_id, vals)

            else: # els creem tots a partir de potencia ofertada

                crm_pot_obj.create(cursor, uid, {
                    'crm_polissa': ids[0],
                    'periode': nom_periode,
                    'potencia': potencia_ofertada,
                    'preu': preu,
                    'tipus': 'oferta',
                })

        # periodes energia
        search_params = [('tarifa.id', '=', tarifa_oferta_id),
                         ('agrupat_amb', '=', False),
                         ('tipus', '=', 'te')]
        periodes_ids = pt_periode.search(cursor, uid, search_params)

        for p_id in periodes_ids: # periodes ofertats energia

            periode = pt_periode.read(cursor, uid, p_id)
            nom_periode = periode['name']
            cat_id = periode['categ_id'][0]

            # a partir del periode busquem l'id del producte
            # tindrà la mateixa categoria i nom de periode
            prod_id = product_obj.search(cursor, uid,
                                        [('categ_id', '=', cat_id),
                                         ('name', '=', nom_periode)])[0]

            # price_get mètode de la llista de preu, que calcula el preu final
            # estigui o no basada sobre una altra llista
            # la llista de preu és la tarifa de comercialitzadora escollida
            preu = pl_obj.price_get(cursor, uid, [pl_id],
                                prod_id, 1, context=context)[pl_id]

            crm_ene_obj.create(cursor, uid, {
            'periode': nom_periode,
            'crm_polissa': oferta_id,
            'preu': preu,
            'tipus': 'oferta'
            })

        return True

    def generar_all_preus(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        self.generar_periodes_potencia_oferta(cursor, uid, ids, context)
        self.generar_preus_transferencia(cursor, uid, ids, context)
        self.generar_periodes_tarifa(cursor, uid, ids, context)

        return True

    def generar_preus_transferencia(self, cursor, uid, ids, context=None):
        '''El que s'està generant en realitat és el preu de l'energia PVP.
        El preu de transferència és el preu de cost de l'energia, a partir
        només de les Ks i dels peatges. Al aplicar-li el mcp és el terme energia'''
        if context is None or 'tarifa' not in context:
            return False
        else:
            tarifa_id = context['tarifa']

        tp_obj = self.pool.get('giscedata.tp')
        pt_periode = self.pool.get('giscedata.polissa.tarifa.periodes')

        preus_transferencia_periode = self.pool.get('crm.pt.periode')
        search_params = [('tarifa.id', '=', tarifa_id),
                         ('agrupat_amb', '=', False),
                         ('tipus', '=', 'te')]
        periodes_id = pt_periode.search(cursor, uid, search_params)

        for crm in self.browse(cursor, uid, ids, context=context):

            p_ids = preus_transferencia_periode.search(cursor, uid,
                                   [('crm_polissa.id', '=', crm.id),
                                    ],
                                   context={'active_test': False})
            preus_transferencia_periode.unlink(cursor, uid, p_ids)

            for periode in pt_periode.read(
                    cursor, uid, periodes_id, ['name']):

                if context and context['tipus'] == 'oferta':

                    # 'preu' serà el preu de transferència
                    preu_transfer = tp_obj.calc_price(
                        cursor, uid, periode['id'], context=context)

                    preus_transferencia_periode.create(cursor, uid, {
                        'periode': periode['name'],
                        'periode_id': periode['id'],
                        'crm_polissa': crm.id,
                        'name': preu_transfer
                    })

        return True

    def generar_periodes_tarifa(self, cursor, uid, ids, context={}):
        '''Preus PVP d'energia = ((MPC/1000) x 1,015) + preu transfer'''
        if not context and context['tarifa']:
            return False
        else:
            tarifa_id = context['tarifa']

        if 'tipus' in context:
            tipus =  TIPUS[context['tipus']]
        else:
            tipus = TIPUS['client']

        if not tarifa_id:
            return False

        crm_ene_obj = self.pool.get('crm.energia.contractada.periode')
        pt_obj = self.pool.get('crm.pt.periode')

        for crm in self.browse(cursor, uid, ids, context=context):

            # esborrem el/s antics
            params = [('crm_polissa.id', '=', crm.id),('tipus','=','oferta')]
            ene_periodes_ids = crm_ene_obj.search(cursor, uid, params)
            crm_ene_obj.unlink(cursor, uid, ene_periodes_ids)

            mcp_def = crm.mcp_definitivo

            # per cada preu de transfer que ja teniem calculat
            params = [('crm_polissa', '=', crm.id)]
            pt_ids = pt_obj.search(cursor, uid, params)
            for pt_id in pt_ids:

                # obtenir el preu de transferencia i crear el pvp
                pt = pt_obj.read(cursor, uid, pt_id, ['name','periode'])

                # li sumem el MPC
                preu = pt['name']
                preu_pvp = preu + ((mcp_def / 1000) * 1.015)

                crm_ene_obj.create(cursor, uid, {
                    'periode': pt['periode'],
                    'crm_polissa': crm.id,
                    'preu': preu_pvp,
                    'tipus': tipus
                })

        return True

    def open_document_viewer(self, cursor, uid, ids, context=None):
        view_obj = self.pool.get('ir.ui.view')
        view_id = view_obj.search(
            cursor, uid, [('name', '=', 'crm_document_viewer.form'),
                          ('model', '=', 'crm.polissa')]
        )[0]
        return {
            'type': 'ir.actions.act_window',
            'name': 'Carga de documentos',
            'view_mode': 'form',
            'view_type': 'form',
            'res_model': 'crm.polissa',
            'nodestroy': 'true',
            'target': 'new',
            'res_id': ids[0],
            'views': [(view_id, 'form')],
        }

    def exportar_contrato(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        crm_polissa_id = ids[0]
        crm_polissa_obj = self.pool.get('crm.polissa')
        fields = ['cups_name', 'cliente_nombre', 'cliente_vat',
                  'contrato_iban', 'contrato_bic_code']
        crm_polissa = crm_polissa_obj.read(
            cursor, uid, crm_polissa_id, fields)
        fields_names = crm_polissa_obj.fields_get(cursor, uid, fields)
        message = ''
        for val in crm_polissa.items():
            if not val[1]:
                message += '%s \n' % (fields_names[val[0]]['string'])
        if message:
            message = '''Los siguientes campos son obligatorios
                         para la exportación:\n\n''' + message

            raise osv.except_osv('Campos obligatorios', message)
        context.update({'crm_polissa_id': crm_polissa_id})
        wiz_obj = self.pool.get('wizard.export.contrato')
        wiz_id = wiz_obj.create(cursor, uid, {})
        wiz_obj.export(cursor, uid, [wiz_id], context=context)
        self.normalize_energia_prevista(cursor, uid, crm_polissa_id, context)

        return True

    def crea_cups(self, cursor, uid, oferta):
        # TODO afegir tots els camps que trobem de la oferta
        cups_obj = self.pool.get('giscedata.cups.ps')

        vals_cups = {}
        vals_cups['name'] = oferta['cups_name']

        # comprovem camps minims
        try:
            vals_cups['id_municipi'] = oferta['cups_id_municipi'][0]
            vals_cups['distribuidora_id'] = oferta['cups_distribuidora_id'][0]
        except TypeError:
            raise osv.except_osv("",
                "Los campos municipio y distribuidora son campos requeridos.")

        # afegim tots els camps que podem recuperar de la oferta
        tpv_id = oferta['cups_tv'] and oferta['cups_tv'][0] or False
        vals = {
            'active': 1,
            'name': oferta['cups_name'],
            'id_municipi': oferta['cups_id_municipi'][0],
            'id_poblacio': oferta['cups_id_poblacio'][0],
            'nv': oferta['cups_nv'],
            'pnp': oferta['cups_pnp'],
            'es': oferta['cups_es'],
            'pt': oferta['cups_pt'],
            'pu': oferta['cups_pu'],
            'tv': tpv_id,
            'ref_catastral': oferta['cups_ref_catastral'],
            'aclarador': oferta['cups_aclarador'],
            'cpo': oferta['cups_cpo'],
            'cpa': oferta['cups_cpa'],
            'dp': oferta['cups_dp'],
            'distribuidora_id': oferta['cups_distribuidora_id'][0],
            }
        vals_cups.update(vals)
        cups_id = cups_obj.create(cursor, uid, vals_cups)

        return cups_id

    def onchange_vat_check_existing_partner(self, cursor, uid, ids,
                                            partner_id, vat, context=None):
        def wh_fix_vat(vat):
            vat = ''.join([x for x in vat.upper() if x.isalnum()])
            if vat and vat[0].isdigit():
                vat = vat.zfill(9)
            if not vat.startswith('ES'):
                vat = 'ES%s' % vat
            return vat

        res = {}
        if vat:
            partner_obj = self.pool.get('res.partner')
            # crm_polissa_obj = self.pool.get('crm.polissa')
            # oferta = crm_polissa_obj.read(cursor, uid, ids[0])
            # cliente_existente_indicado = oferta['partner_id']
            vat = wh_fix_vat(vat)
            partner_existente = partner_obj.search(cursor, uid,
                                                        [('vat','=',vat)])
            if partner_existente:
                id_partner_existente = partner_existente[0]
                res = self.onchange_partner(cursor, uid, ids, id_partner_existente)
                res['value'].update({'partner_id': id_partner_existente})

        return res

    def find_or_create_cups(self, cursor, uid, oferta):

        search_params = [('name', '=', oferta['cups_name'])]
        cups_obj = self.pool.get('giscedata.cups.ps')
        cups = cups_obj.search(cursor, uid, search_params)

        # si existeix treiem el id, no l'updatem
        if cups:
            cups_id = cups[0]

        # si no existeix el creem a partir de les dades de la oferta
        else:
            cups_id = self.crea_cups(cursor, uid, oferta)

        return cups_id

    def find_or_create_partner(self, cursor, uid, oferta, context=None):

        adr_obj = self.pool.get('res.partner.address')

        def wh_fix_vat(vat):
            vat = ''.join([x for x in vat.upper() if x.isalnum()])
            if vat and vat[0].isdigit():
                vat = vat.zfill(9)
            if not vat.startswith('ES'):
                vat = 'ES%s' % vat
            return vat

        # PARTNER #############################################################

        # detectar errors:
        if oferta['cliente_tipo_persona'] == 'juridica':
            if oferta['cliente_nombre']:
                if (oferta['cliente_nombre']).strip() != u'':
                    raise osv.except_osv("",
                    "Al tratarse de persona jurídica, por favor "
                    "inserte el nombre en el campo 'apellidos'")

        if not oferta['cliente_tipo_persona']:
            raise osv.except_osv("",
                "Por favor, indique si es persona física o jurídica")

        # un cop aquí ja tenim el nom del client correctament
        partner_obj = self.pool.get('res.partner')
        if oferta['cliente_tipo_persona'] == 'fisica':
            partner_name = '{0}, {1}'.format(oferta['cliente_apellidos'],
                                             oferta['cliente_nombre'])
        elif oferta['cliente_tipo_persona'] == 'juridica':
            partner_name = oferta['cliente_apellidos']

        fixed_vat = wh_fix_vat(oferta['cliente_vat'])

        # si hem intriduit un nº de document, busquem el partner
        cal_crear_partner = True
        if oferta['cliente_vat']:
            partners = partner_obj.search(
                cursor, uid, [('vat', '=', fixed_vat)],
                order='write_date desc, id desc')
            for partner in partners:
                adrs = adr_obj.search(cursor, uid, [('partner_id', '=', partner)])
                if adrs:
                    partner_id = partner
                    cal_crear_partner = False

        if cal_crear_partner:
            # quan no tenim nº de document o no el trobem amb el nº introduit
            vals = {
                'name': partner_name,
                'vat': fixed_vat,
                'customer': True,
                'lang': 'es_ES'
            }
            # creem el partner
            partner_id = partner_obj.create(cursor, uid, vals)

            # ADRECES ########################################################
            partner_address_obj = self.pool.get('res.partner.address')

            vals_default_address = {
                'pt': oferta['cups_pt'],
                'pu': oferta['cups_pu'],
                'es': oferta['cups_es'],
                'pnp': oferta['cups_pnp'],
                'zip': oferta['cups_dp'],
                'cpo': oferta['cups_cpo'],
                'cpa': oferta['cups_cpa'],
                'state_id': oferta['cups_id_provincia'][0],
                'tv': oferta['cups_tv'][0],
                'id_poblacio': oferta['cups_id_poblacio'][0],
                'nv': oferta['cups_nv'],
                'aclarador': oferta['cups_aclarador'],
                'id_municipi': oferta['cups_id_municipi'][0],
                'name': partner_name,
                'partner_id': partner_id,
                'phone': oferta['cliente_telefono_1'] or '',
                'mobile': oferta['cliente_telefono_2'] or '',
                'email': oferta['cliente_email_1'] or ''
            }

            vals_default_address.update({'type': 'default'})
            partner_address_obj.create(cursor, uid, vals_default_address)

        return partner_id

    def get_group(self, cursor, uid, cups_id):
        '''Obte el grup de facturacio a partir de la distribuidora del
        cups i la variable de configuracio'''
        res_config = self.pool.get('res.config')
        group_obj = self.pool.get('giscedata.polissa.group')
        distri_grup = res_config.get(cursor, uid,
                                     'crm_elec_oferta_grup_facturacio', 1)
        distri_grup = literal_eval(distri_grup)

        cursor.execute("""SELECT pt.ref
        FROM giscedata_cups_ps c
        JOIN res_partner pt ON pt.id = c.distribuidora_id
        WHERE c.id = %s""", (cups_id,))
        codi_distri = cursor.fetchall()[0][0]

        if codi_distri in distri_grup:
            grup = distri_grup.get(codi_distri)
        else:
            grup = distri_grup.get('default', '')
        grup = group_obj.search(cursor, uid, [('name', '=', grup)])
        if grup:
            grup_id = grup[0]
        else:
            grup_id = False
        return grup_id

    def create_polissa(self, cursor, uid, oferta, n_pol, cups_id, partner_id):

        vals = {}
        consum_anual = self.get_consumo_anual_previsto(cursor, uid, oferta)
        if oferta['cliente_group']:
            group_id = oferta['cliente_group'][0]
        else:
            group_id = self.get_group(cursor, uid, cups_id)
        vals['consumo_anual_previsto'] = consum_anual
        vals['state'] = 'esborrany'
        vals['name_auto'] = True
        vals['cups'] = cups_id
        vals['active'] = True
        vals['titular'] = partner_id
        vals['distribuidora'] = oferta['cups_distribuidora_id'][0]
        vals['tarifa'] = oferta['tarifa_oferta'][0]
        vals['user_id'] = oferta['user_id'][0]
        vals['delegacion'] = oferta['delegacion'][0]
        vals['potencia'] = oferta['potencia_ofertada']
        vals['mode_facturacio'] = oferta['mode_facturacio']
        vals['fact_potencia_100'] = oferta['fact_potencia_100']
        vals['coeficient_k'] = oferta['coeficient_k']
        vals['coeficient_d'] = oferta['coeficient_d']
        vals['llista_preu'] = oferta['llista_preu'][0]
        vals['data_firma_contracte'] = oferta['fecha_firma'] or datetime.today()
        vals['condicions_generals_id'] = oferta['condicions_generals_id'][0]
        vals['group_id'] = group_id
        vals['enviament'] = 'email' if not oferta['envio_postal'] else 'postal'
        vals['notificacio_email'] = oferta['cliente_email_1']
        if oferta['num_cta']:
            vals['bank'] = oferta['num_cta'][0]
        cnae = oferta.get('cnae', False)
        if cnae:
            vals['cnae'] = cnae[0]
        if oferta['cliente_tipo_persona'] == 'juridica':
            if oferta['pers_firmante_id']:
                vals['firmante_id'] = oferta['pers_firmante_id'][0]
        elif oferta['cliente_tipo_persona'] == 'fisica':
            vals['firmante_id'] = partner_id

        # Subsistemas islas canarias
        sbs = ['FUERTEVENTURA-LANZAROTE',
               'GRAN CANARIA',
               'HIERRO',
               'IBIZA-FORMENTERA',
               'LA GOMERA',
               'LA PALMA',
               'TENERIFE']
        if oferta['subsistema_oferta'] in sbs:
            acc_fsc_pos_obj = self.pool.get('account.fiscal.position')
            params = [('name','=',u'Régimen Islas Canarias')]
            fp_id = acc_fsc_pos_obj.search(cursor, uid, params)[0]
            vals['fiscal_position_id'] = fp_id

        # li afegim el numero de sequencia
        vals['name'] = n_pol

        # creem la polissa amb les vals de la oferta
        polissa_obj = self.pool.get('giscedata.polissa')
        polissa_id = polissa_obj.create(cursor, uid, vals)

        vals = {}
        # per defecte posem com a persona fiscal el mateix titular
        values = polissa_obj.onchange_pagador_sel(
                            cursor, uid, polissa_id, 'titular', partner_id,
                            partner_id, None, None, None)['value']
        vals.update(values)
        # posem com a persona de notificació el mateix titular
        values = polissa_obj.onchange_notificacio(
                            cursor, uid, polissa_id, 'titular', partner_id,
                            partner_id, None, None)['value']
        vals.update(values)
        vals.update({'notificacio': 'titular'})
        # posem la direcció de pagament i de notificació
        if oferta['contacto_direccion']:
            vals.update(
                {'direccio_notificacio': oferta['contacto_direccion'][0]}
            )
        if oferta['cliente_direccion']:
            vals.update(
                {'direccio_pagament': oferta['cliente_direccion'][0]}
            )

        polissa_obj.write(cursor, uid, polissa_id, vals)

        return polissa_id

    def escriure_potencies_ofertades(self, cr, uid, oferta_id, polissa_id,
                                     context=None):
        # buscar els periodes de potència generats a la pòlissa per
        # sobreescriure'ls amb les potències ofertades a la oferta
        pot_per_polissa = self.pool.get(
            'giscedata.polissa.potencia.contractada.periode')
        crm_pot_obj = self.pool.get('crm.potencia.contractada.periode')
        tarifaperiodes = self.pool.get('giscedata.polissa.tarifa.periodes')
        params = [('polissa_id', '=', polissa_id)]
        ids_periodes_pol = pot_per_polissa.search(cr, uid, params)

        for id_periode in ids_periodes_pol:
            periode = pot_per_polissa.read(cr, uid, id_periode, ['periode_id'])
            per_id = periode['periode_id'][0]
            nameper = tarifaperiodes.read(cr, uid, per_id, ['name'])['name']
            paramscrm = [('crm_polissa', '=', oferta_id),
                         ('tipus', '=', 'oferta'),
                         ('periode', '=', nameper)
                         ]
            id_pot = crm_pot_obj.search(cr, uid, paramscrm)
            pot = crm_pot_obj.read(cr, uid, id_pot[0], ['potencia'])['potencia']
            pot_per_polissa.write(cr, uid, id_periode, {'potencia': pot})
        return True

    def get_consumo_anual_previsto(self, cursor, uid, oferta_d, context=None):
        #Get the expected annual consumption in the one of the two fields
        consumo = oferta_d['consumo_anual_previsto']
        consumodirecto = oferta_d['consumo_anual_previsto_directo']

        if consumodirecto and consumodirecto > 0:
            return consumodirecto
        elif consumo.isdigit() and consumo > 0:
            return consumo
        raise osv.except_osv("Campos de consumo anual previsto requeridos",
                             "Debes completar uno de los campos de consumo "
                             "anual previsto")

    def check_camps_minims(self, cursor, uid, oferta_id, context=None):
        if context is None:
            context = {}

        oferta = self.read(cursor, uid, oferta_id)[0]

        if oferta['cliente_vat'] == 'ES':
            raise osv.except_osv("", "Nº de documento {} inválido".format(
                oferta['cliente_vat']
            ))
        if oferta['zona_carta_id'] == False and oferta['envio_postal']:
            raise osv.except_osv("", "Es necesario indicar la Zona de carta")

        if oferta['condicions_generals_id'] == False:
            raise osv.except_osv("", "Es necesario indicar Condiciones Generales")

        if oferta['delegacion'] == False:
            raise osv.except_osv("", "Es necesario indicar la Delegación")

        if oferta['cliente_email_1'] == False and not oferta['envio_postal']:
            raise osv.except_osv("", "Es necesario indicar un email")

        return True

    def accept_offer(self, cursor, uid, oferta_id, context=None):

        self.check_k_d(cursor, uid, oferta_id)
        self.check_camps_minims(cursor, uid, oferta_id)

        crm_polissa_obj = self.pool.get('crm.polissa')
        polissa_obj = self.pool.get('giscedata.polissa')
        oferta = crm_polissa_obj.read(cursor, uid, oferta_id)[0]

        # busquem o creem el cups
        cups_id = self.find_or_create_cups(cursor, uid, oferta)

        # busquem o creem el partner
        partner_id = self.find_or_create_partner(cursor, uid, oferta)

        n_pol = oferta['contrato_name']
        if not n_pol:
            n_pol = self.pool.get('ir.sequence').get(cursor,uid,'giscedata.polissa')
        # creem la pòlissa en estat esborrany
        polissa_id = self.create_polissa(cursor, uid, oferta, n_pol, cups_id,
                                         partner_id)

        # generar els periodes de potència de la pòlissa
        polissa_obj.generar_periodes_potencia(cursor, uid, [polissa_id],
                                              context={'force_genpot': True})
        self.escriure_potencies_ofertades(cursor, uid, oferta_id, polissa_id)

        # marquem la oferta com a acceptada i li escribim la polissa creada
        crm_stage_obj = self.pool.get('crm.case.stage')
        search_params = [('name','=','Oferta aceptada')]
        stage_id = crm_stage_obj.search(cursor, uid, search_params)[0]
        vals_crm_polissa = {'polissa_id': polissa_id,
                            'stage_id': stage_id}
        crm_polissa_obj.write(cursor, uid, oferta_id, vals_crm_polissa)

        #Generem el mandato si l'IBAN està indicat
        iban = crm_polissa_obj.read(cursor, uid, oferta_id, ['num_cta'])[0]
        if iban['num_cta']:
            self.genera_mandato(cursor, uid, polissa_id, context=None)

        return polissa_id

    def accept_offer_renovacion(self, cursor, uid, oferta_id, context=None):
        if context is None:
            context = {}

        self.check_k_d(cursor, uid, oferta_id)
        self.check_camps_minims(cursor, uid, oferta_id)

        # busquem l'id de la polissa existent
        crm_polissa_obj = self.pool.get('crm.polissa')
        polissa_obj = self.pool.get('giscedata.polissa')

        oferta = crm_polissa_obj.read(cursor, uid, oferta_id)[0]
        params = [('name', '=', oferta['numero_ctto'])]
        polissa_id = polissa_obj.search(cursor, uid, params)[0]
        polissa = polissa_obj.browse(cursor, uid, polissa_id)

        # La data d'activació es sempre la data del dia en curs
        data_activacio = datetime.today().date()

        # fem la modificació contractual al contracte
        vals_mod = {
            'llista_preu': oferta['llista_preu'][0],
            'condicions_generals_id': oferta['condicions_generals_id'][0],
            'mode_facturacio': oferta['mode_facturacio'],
            'coeficient_k': oferta['coeficient_k'],
            'coeficient_d': oferta['coeficient_d'],
            'data_firma_contracte': oferta['fecha_firma'] or datetime.today(),
            'fact_potencia_100': oferta['fact_potencia_100'],
            'enviament': 'email' if not oferta['envio_postal'] else 'postal',
            'notificacio_email': oferta['cliente_email_1'],
            }

        if oferta['num_cta']:
            vals_mod.update({'bank': oferta['num_cta'][0]})
            #Guardem l'IBAN anterior per si hem de crear el mandato després
            anterioriban = polissa_obj.read(cursor, uid, polissa_id, ['bank'])
            anterioriban = anterioriban['bank']
            if not anterioriban:
                #Si no disposavem d'IBAN abans ens evitem un error d'índex
                anterioriban = (False, False)

        data_final = data_activacio + timedelta(days=364)

        # afegim el consum anual previst
        consum_anual = self.get_consumo_anual_previsto(cursor, uid, oferta)
        vals_mod.update({'consumo_anual_previsto': consum_anual})

        try:
            polissa.send_signal('modcontractual')
            polissa.write(vals_mod)
            wz_crear_mc_obj = self.pool.get('giscedata.polissa.crear.contracte')
            ctx = {'active_id': polissa_id, 'lang': 'es_ES'}
            # Oferta de renovació: indiquem que tipus de renovació expressa
            params = {'duracio': 'nou',
                      'accio': 'nou',
                      'tipus_renovacio': 'expressa',
                      }
            wiz_id = wz_crear_mc_obj.create(cursor, uid, params, context=ctx)
            wiz = wz_crear_mc_obj.browse(cursor, uid, [wiz_id])[0]
            res = wz_crear_mc_obj.onchange_duracio(cursor, uid, [wiz.id],
                                str(data_activacio), wiz.duracio, context=ctx)
            if res.get('warning', False):
                polissa.send_signal('undo_modcontractual')
                raise osv.except_osv('Error', res['warning'])
            else:
                wiz.write({'data_inici': str(data_activacio),
                           'data_final': str(data_final)
                           })
                wiz.action_crear_contracte()
        except Exception as e:
            polissa.send_signal('undo_modcontractual')
            raise osv.except_osv('Error', str(e))

        if oferta['num_cta']:
            #Si canvia el número de compte s'ha de generar un nou mandato
            compte = self.read(cursor, uid, oferta_id, ['num_cta'])[0]
            iban = compte['num_cta']
            #Comparem els codis IBAN, que segur que són únics.
            if iban and anterioriban[1] != iban[1]:
                self.genera_mandato(cursor, uid, polissa_id, context=None)

        # marquem la oferta com a acceptada i li escribim la polissa existent
        crm_stage_obj = self.pool.get('crm.case.stage')
        search_params = [('name','=','Oferta aceptada')]
        stage_id = crm_stage_obj.search(cursor, uid, search_params)[0]
        vals_crm_polissa = {'polissa_id': polissa_id,
                            'stage_id': stage_id}
        crm_polissa_obj.write(cursor, uid, oferta_id, vals_crm_polissa)

        return polissa_id

    def accept_offer_vuelve(self, cursor, uid, oferta_id, context=None):

        self.check_k_d(cursor, uid, oferta_id)

        # no revisem si existeix algun contracte amb aquell cups
        # perquè això ja es revisa quan s'activa el contracte
        polissa_obj = self.pool.get('giscedata.polissa')
        crm_polissa_obj = self.pool.get('crm.polissa')
        oferta = crm_polissa_obj.read(cursor, uid, oferta_id)[0]

        # Duplicarem el contracte existent i després li modifiquem els valors
        num_ctto = oferta['numero_ctto']
        params = [('name', '=', num_ctto)]
        polissa_id_old = polissa_obj.search(cursor, uid, params,
                                            context={'active_test': False})[0]

        # El copy() de giscedata.polissa li elimina name, periodes_potencia,
        # modcontractuals_ids, modcontractual_activa
        defaults = {}
        polissa_id_new = polissa_obj.copy(cursor, uid, polissa_id_old, defaults)
        valors_modificar = {}

        # buscar el partner per si es nou
        partner_id = self.find_or_create_partner(cursor, uid, oferta,
                                                  context=context)

        # li hem de modificar també les dates / numero contracte /
        # lectures / estat impagat? / llista de preu / mcp
        if oferta.get('contrato_name', False):
            name = oferta.get('contrato_name')
        else:
            name = self.pool.get('ir.sequence').get(cursor,uid,'giscedata.polissa')
        valors_modificar['name'] = name
        valors_modificar['active'] = True
        valors_modificar['state'] = 'esborrany'
        valors_modificar['data_activacio'] = False
        valors_modificar['data_alta'] = False
        valors_modificar['data_baixa'] = False
        valors_modificar['data_firma_contracte'] = oferta['fecha_firma'] \
                                                   or datetime.today()
        consum_anual = self.get_consumo_anual_previsto(cursor, uid, oferta)
        valors_modificar['consumo_anual_previsto'] = consum_anual
        valors_modificar['data_ultima_lectura'] = False
        valors_modificar['data_ultima_lectura_estimada'] = False
        valors_modificar['pending_amount'] = 0.0
        valors_modificar['pending_state'] = False
        valors_modificar['proxima_facturacio'] = False
        valors_modificar['versio_primera_factura'] = False
        valors_modificar['llista_preu'] = oferta['llista_preu'][0]
        valors_modificar['condicions_generals_id'] = oferta['condicions_generals_id'][0]
        valors_modificar['titular'] = partner_id
        valors_modificar['user_id'] = oferta['user_id'][0]
        valors_modificar['delegacion'] = oferta['delegacion'][0]
        valors_modificar['tarifa'] = oferta['tarifa_oferta'][0]
        valors_modificar['potencia'] = oferta['potencia_client']
        valors_modificar['distribuidora'] = oferta['cups_distribuidora_id'][0]
        valors_modificar['name_auto'] = True
        valors_modificar['renovacio_auto'] = True
        valors_modificar['mode_facturacio'] = oferta['mode_facturacio']
        valors_modificar['coeficient_k'] = oferta['coeficient_k']
        valors_modificar['coeficient_d'] = oferta['coeficient_d']
        valors_modificar['fact_potencia_100'] = oferta['fact_potencia_100']
        if not oferta['cliente_vat'] or oferta['cliente_vat'] == 'ES':
            raise osv.except_osv("", 'Nº de documento inválido')
        else:
            valors_modificar['pagador_nif'] = oferta['cliente_vat']
        if oferta['num_cta']:
            #Mirem si l'oferta té indicat l'IBAN
            valors_modificar['bank'] = oferta['num_cta'][0]
        if oferta['cnae']:
            valors_modificar['cnae'] = oferta['cnae'][0]
        # posem la direcció de pagament i de notificació
        if oferta['contacto_direccion']:
            valors_modificar.update(
                {'direccio_notificacio': oferta['contacto_direccion'][0]}
            )
        if oferta['cliente_direccion']:
            valors_modificar.update(
                {'direccio_pagament': oferta['cliente_direccion'][0]}
            )

        # TODO quan es crein preus a partir de mcp, veure que es fa amb el mcp
        # valors_modificar['mcp'] = oferta['mcp'][0]

        polissa_obj.write(cursor, uid, polissa_id_new, valors_modificar)

        # generar els periodes de potència a la polissa
        polissa_obj.generar_periodes_potencia(cursor, uid, [polissa_id_new],
                                              context={'force_genpot': True})
        self.escriure_potencies_ofertades(cursor, uid, oferta_id, polissa_id_new)

        # marquem la oferta com a acceptada i li escribim la polissa creada
        crm_stage_obj = self.pool.get('crm.case.stage')
        search_params = [('name', '=', 'Oferta aceptada')]
        stage_id = crm_stage_obj.search(cursor, uid, search_params)[0]
        vals_crm_polissa = {'polissa_id': polissa_id_new,
                            'stage_id': stage_id}
        crm_polissa_obj.write(cursor, uid, oferta_id, vals_crm_polissa)

        #Es genera un nou mandato si l'IBAN està indicat
        iban = crm_polissa_obj.read(cursor, uid, oferta_id, ['num_cta'])[0]
        if iban['num_cta']:
            self.genera_mandato(cursor, uid, polissa_id_new, context=None)

        return polissa_id_new

    def decline_offer(self, cursor, uid, oferta_id, context=None):

        # busquem l'estat d'oferta rebutjada
        crm_stage_obj = self.pool.get('crm.case.stage')
        search_params = [('name','=','Oferta no aceptada')]
        stage_id = crm_stage_obj.search(cursor, uid, search_params)[0]

        # indiquem a la oferta que s'ha rebutjat
        crm_polissa_obj = self.pool.get('crm.polissa')
        vals_crm_polissa = {'stage_id': stage_id}
        crm_polissa_obj.write(cursor, uid, oferta_id, vals_crm_polissa)

        return

    @job(queue='energia_prevista_normalizer')
    def normalize_energia_prevista(self, cursor, uid, oferta_id, context=None):
        if context is None:
            context = {}
        ene_obj = self.pool.get('giscedata.energia.prevista.diaria')
        oferta = self.browse(cursor, uid, oferta_id)
        cups = oferta.cups_name
        consums = []
        for consum in oferta.client_consums_periode:
            di = datetime.strptime(consum.data_inici, '%Y-%m-%d').date()
            df = datetime.strptime(consum.data_final, '%Y-%m-%d').date()
            c = Consum(cups, consum.periode, di, df, consum.consum)
            consums.append(c)
        totals = Consum.totalizer(consums)
        norm = Normalizer(totals, 'consum')
        for n in norm.normalize():
            val = {'date': n[0].strftime('%Y-%m-%d'), 'consum': n[1],
                   'cups': cups}
            ene_obj.create(cursor, uid, val)
        return True

    def _ff_probability(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        res = dict.fromkeys(ids, False)
        crm_polissa_obj = self.pool.get('crm.polissa')
        crm_stage_obj = self.pool.get('crm.case.stage')
        ofertas_data = crm_polissa_obj.read(cursor, uid, ids, ['stage_id'])
        for oferta in ofertas_data:
            res[oferta['id']] = crm_stage_obj.read(
                cursor, uid,
                oferta['stage_id'][0], ['probability']).get('probability', 0.0)
        return res

    def generate_data(self, cursor, uid, ids, reports, context=None):
        if not context:
            context = {}
        data = []
        for report in reports:
            service = netsvc.LocalService(report)
            result, format = service.create(cursor, uid, ids, {
                'model': self._name,
                'id': ids[0]
            }, context)
            data.append(result)
        return data

    def generar_oferta(self, cursor, uid, ids, context=None, report="report.crm_elec_oferta"):
        if not context:
            context = {}

        self.check_k_d(cursor, uid, ids)

        tipus_documents = {
            'report.crm_elec_comparativa': 'comparativa',
            'report.crm_elec_oferta_comparativa': 'comparativa',
            'report.crm_elec_oferta': 'oferta',
            'report.crm_elec_oferta_vinculant': 'oferta',
            'report.giscedata.polissa': 'oferta',
        }
        # Recalculem els preus de transferència quan es genera la oferta
        if 'insert_condicions_generals' not in context.keys():
            context['insert_condicions_generals'] = False
        pts_oferta_obj = self.pool.get('crm.pt.periode')
        oferta = self.read(cursor, uid, ids[0], ['pts_oferta','name'])
        pts_oferta_obj.recalc_price(cursor, uid, oferta['pts_oferta'])
        prefix = 'Oferta'
        if report == 'report.giscedata.polissa':
            prefix = 'Contrato'
            context['insert_annexe_preus'] = True
            context['insert_sepa_atr'] = False
            data = self.generate_data(cursor, uid, ids, [
                'report.giscedata.polissa',
                'report.giscedata_polissa_atr_sepa'
            ], context)
            to_join = []
            for elem in data:
                res_path = tempfile.mkstemp('-join.pdf', 'report-')[1]
                with open(res_path, 'w') as f:
                    f.write(elem)
                to_join.append(res_path)
            merged_pdfs = pypdftk.concat(files=to_join)
            with open(merged_pdfs, 'r') as f:
                data = f.read()
        else:
            data = self.generate_data(cursor, uid, ids, [report], context)[0]
        data = base64.b64encode(data)
        attach_obj = self.pool.get('ir.attachment')
        doc_obj = self.pool.get('crm.documentacio')

        search_ofertas = [('crm_polissa', '=', ids[0]),
                          ('tipologia', '=', 'documentos_generados'),
                          ('tipus_document', '=', tipus_documents[report])]
        versio_count = doc_obj.search_count(cursor, uid, search_ofertas)
        versio_count += 1
        name = '%s_%s_%s_%s' % (
            prefix,
            oferta['name'],
            versio_count,
            datetime.now().strftime('%Y%m%d%H%M%S')
        )
        attach_id = attach_obj.create(cursor, uid, {
            'datas': data,
            'name': name,
            'datas_fname': '%s.pdf' % name,
            'type': 'binary'})
        doc_id = doc_obj.create(cursor, uid, {
            'attach_id': attach_id,
            'crm_polissa': ids[0],
            'tipus_document': tipus_documents[report],
            'tipologia': 'documentos_generados',
            'versio': versio_count
        })
        message = 'Creación de la oferta %s ' % name
        self.message_append(cursor, uid, ids, text=message)
        return True

    def generar_oferta_vinculant(self, cursor, uid, ids, context=None):
        self.generar_oferta(cursor, uid, ids, context, "report.crm_elec_oferta_vinculant")

    def generar_document_contracte(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        context['insert_condicions_generals'] = True
        if self._description == 'crm.polissa':
            model = self.pool.get('crm.polissa')
            offer = model.read(cursor, uid, ids[0], ['type', 'contrato_name',
                                                     'potencia_ofertada',
                                                     ])
            if offer['potencia_ofertada'] <= 0.0:
                msg = 'No se puede imprimir una oferta con potencia ofertada' \
                      ' {}'.format(offer['potencia_ofertada'])
                raise osv.except_osv('Error', msg)
            if not offer['contrato_name']:
                raise osv.except_osv('Error',
                                     'Es necesario obtener número '
                                     'de contrato para imprimir contrato')
        self.generar_oferta(cursor, uid, ids, context, "report.giscedata.polissa")

    def generar_document_comparativa(self, cursor, uid, ids, context=None):
        self.generar_oferta(cursor, uid, ids, context,
                            "report.crm_elec_oferta_comparativa")

    def calcular_comparativa(self, cr, uid, ids, context=None):
        if not context:
            context = {}
        camps_comparativa = ['comparativa_data_inici',
                             'comparativa_data_fi',
                             'comparativa_iva',
                             'comparativa_import_competencia',
                             'comparativa_import_lloguer',
                             'comparativa_cost_reactiva',
                             'comparativa_import_otros',
                             'comparativa_oferta_consums_periode',
                             'comparativa_oferta_potencia_periode',
                             'comparativa_maximetre',
                             ]
        dades = self.read(cr, uid, ids[0], camps_comparativa)

        datainici= dades['comparativa_data_inici']
        datafi = dades['comparativa_data_fi']
        diesany = 366 if isleap(datetime.today().year) else 365
        intervaldies = datetime.strptime(datafi, '%Y-%m-%d') - \
                       datetime.strptime(datainici, '%Y-%m-%d')
        intervaldies = intervaldies.days

        #Calculem l'import elèctric:
        con_comp_obj = self.pool.get('comparativa.consums.periode')
        pot_comp_obj = self.pool.get('comparativa.potencia.periode')

        importe_electrico = 0.
        for id_con in dades['comparativa_oferta_consums_periode']:
            dadescon = con_comp_obj.read(cr, uid, id_con, ['consum', 'preu'])
            importperiode = dadescon['consum'] * dadescon['preu']
            importe_electrico += importperiode

            con_comp_obj.write(cr, uid, id_con, {'import': importperiode})

        for id_pot in dades['comparativa_oferta_potencia_periode']:
            if dades['comparativa_maximetre'] == True:
                dadespot = pot_comp_obj.read(
                    cr, uid, id_pot, ['potencia', 'preu', 'lectura'])
                if not dadespot['lectura']:
                    dadespot['lectura'] = 0.
                potenciafacturada = maximetre(
                    potencia_contractada=dadespot['potencia'],
                    potencia_maximetre=dadespot['lectura'],
                )
                preuxkwxperiode = dadespot['preu']/diesany * intervaldies
                importperiode = potenciafacturada * preuxkwxperiode
                importe_electrico += importperiode

                pot_comp_obj.write(cr, uid, id_pot, {'import': importperiode})
            else:
                dadespot = pot_comp_obj.read(cr, uid, id_pot,
                                             ['potencia', 'preu'])
                preuxkwxperiode = dadespot['preu'] / diesany * intervaldies
                importperiode = dadespot['potencia'] * preuxkwxperiode
                importe_electrico += importperiode

                pot_comp_obj.write(cr, uid, id_pot, {'import': importperiode})

        importe_electrico += dades['comparativa_cost_reactiva']
        impuesto_electrico = importe_electrico * 0.051127

        totalsiniva = importe_electrico + impuesto_electrico + \
                      dades['comparativa_import_lloguer'] + \
                      dades['comparativa_import_otros']

        iva = self.pool.get('account.tax').read(cr, uid,
                                                dades['comparativa_iva'][0],
                                                ['amount'])['amount']
        total = totalsiniva * (1 + iva)
        dif = dades['comparativa_import_competencia'] - total
        #Escribim els valors a la comparativa
        self.write(cr, uid, ids, {
            'comparativa_impost_electric': impuesto_electrico,
            'comparativa_total_sense_iva': totalsiniva,
            'comparativa_total_amb_iva': total,
            'comparativa_diferencia_preus': dif
        })
        return True

    def _default_IVA(self, cr, uid, context=None):
        taxes_obj = self.pool.get('account.tax')
        return taxes_obj.search(cr, uid, [('name', '=', 'IVA 21%')])[0]


    def copiar_datos_oferta_comparativa(self, cr, uid, ids, context=None):
        """Copia les dades de la oferta cap a la pestanya: Comparativa"""

        if not context:
            context = {}

        def redacta_error(llista_errors):
            if llista_errors:
                string = "Se han producido las siguientes incidencias:\n"
                for error in llista_errors:
                    string += '- ' + error + '\n'
                return string
            else:
                return 'Datos copiados con éxito.'

        vals_to_read = ['cliente_tipo_persona',
                        'comparativa_data_inici',
                        'comparativa_data_fi',
                        'partner_id',
                        'cliente_nombre',
                        'cliente_apellidos',
                        'cliente_vat',
                        'cups_sips',
                        'cups_name',
                        'cups_tv',
                        'cups_nv',
                        'cups_pnp',
                        'cups_es',
                        'cups_pt',
                        'cups_pu',
                        'cups_dp',
                        'cups_aclarador',
                        'cups_id_poblacio',
                        'potencia_client',
                        'tarifa_oferta',
                        'llista_preu',
                        ]
        vals_to_write = {'comparativa_info': False,
                         'comparativa_razon_social': False,
                         'comparativa_dni': False,
                         'comparativa_cups': False,
                         'comparativa_direccion': False,
                         'comparativa_potencia_contratada': False,
                         'comparativa_poblacio': False,
                         'comparativa_tarifa_acces': False,
                         'comparativa_tarifa_comercialitzadora': False,
                         }
        dades = self.read(cr, uid, ids[0], vals_to_read)

        datainici = dades['comparativa_data_inici']
        datafi = dades['comparativa_data_fi']
        if not (datainici and datafi):
            raise osv.except_osv("Fecha de inicio y fin incompletas",
                                 "Por favor, introduce la fecha de inicio y fin"
                                 " de la factura de la competencia.")
        missatge_error = []
        #Gestío de la razón social/nombre
        tipopersona = dades['cliente_tipo_persona']
        if dades['partner_id']:
            vals_to_write['comparativa_razon_social'] = dades['partner_id'][1]
        elif tipopersona == 'juridica' and dades['cliente_apellidos']:
            vals_to_write['comparativa_razon_social'] = dades['cliente_apellidos']
        elif dades['cliente_apellidos'] and dades['cliente_nombre']:
            vals_to_write['comparativa_razon_social'] = \
                dades['cliente_apellidos'] + ', ' + dades['cliente_nombre']
        else:
            missatge_error.append('Nombre/razón social del cliente incorrecta.')

        #Gestió del DNI
        if dades['cliente_vat']:
            vals_to_write['comparativa_dni'] = dades['cliente_vat']
        else:
            missatge_error.append('NIF / DNI del cliente no indicado.')

        #Gestió del CUPS
        if dades['cups_name']:
            vals_to_write['comparativa_cups'] = dades['cups_name']
        elif dades['cups_sips']:
            vals_to_write['comparativa_cups'] = dades['cups_sips']
        else:
            missatge_error.append('CUPS de la dirección de suministro incorrecto.')

        #Gestió de la direcció
        stringtotal = ''
        if dades['cups_tv']:
            stringtotal += dades['cups_tv'][1] + ' '
        if dades['cups_nv']:
            stringtotal += dades['cups_nv'] + ' '
        if dades['cups_pnp']:
            stringtotal += 'Nº' + dades['cups_pnp'] + ' '
        if dades['cups_es']:
            stringtotal += dades['cups_es'] + ' '
        if dades['cups_pt']:
            stringtotal += dades['cups_pt'] + ' '
        if dades['cups_pu']:
            stringtotal += dades['cups_pu'] + ' '
        if dades['cups_aclarador']:
            stringtotal += dades['cups_aclarador']
        if stringtotal:
            vals_to_write['comparativa_direccion'] = stringtotal
        else:
            missatge_error.append('Dirección de suministro vacía.')

        #Gestió de la població
        stringtotal = ''
        if dades['cups_id_poblacio'] and dades['cups_dp']:
            stringtotal = '{0} {1}'.format(dades['cups_dp'],
                                           dades['cups_id_poblacio'][1])
            vals_to_write['comparativa_poblacio'] = stringtotal
        else:
            missatge_error.append('Población del suministro y/o CP incompleto.')

        # Gestió de la potència
        if dades['potencia_client']:
            vals_to_write['comparativa_potencia_contratada'] = \
                dades['potencia_client']
        else:
            missatge_error.append('Potencia contratada incompleta.')

        #Gestió de les tarifes ofertada i comercialitzadora
        if dades['tarifa_oferta']:
            vals_to_write['comparativa_tarifa_acces'] = \
                dades['tarifa_oferta'][1]
        else:
            missatge_error.append('Tarifa de acceso incompleta.')
        if dades['llista_preu']:
            vals_to_write['comparativa_tarifa_comercialitzadora'] = \
                dades['llista_preu'][1]
        else:
            missatge_error.append('Tarifa comercializadora incompleta.')

        #Gestió del missatge d'error
        vals_to_write['comparativa_info'] = redacta_error(missatge_error)
        self.write(cr, uid, ids, vals_to_write)

        #Si s'ha introduït la tarifa de preus, es copien els períodes
        if 'Tarifa comercializadora incompleta.' in missatge_error:
            return False
        else:
            self.unlink_preus_comparativa(cr, uid, ids, context=None)

        #Models de les taules de la comparativa
        pot_comp_obj = self.pool.get('comparativa.potencia.periode')
        con_comp_obj = self.pool.get('comparativa.consums.periode')

        #Models de les taules de les ofertes
        pot_oferta_obj = self.pool.get('crm.potencia.contractada.periode')
        con_oferta_obj = self.pool.get('crm.energia.contractada.periode')

        potcon = self.read(cr, uid, ids[0], [
            'oferta_potencies_periode',      #Potencies de la pestanya oferta
            'oferta_energies_periode'])      #Consums de la pestanya oferta
        potenciaperiode = potcon['oferta_potencies_periode']
        consumsperiode = potcon['oferta_energies_periode']
        if not potenciaperiode or not consumsperiode:
            """Si ja estàn generades és millor no tornar a fer-ho"""
            self.mostrar_preus(cr, uid, ids, context=None)
            potcon = self.read(cr, uid, ids[0], [
                'oferta_potencies_periode',
                'oferta_energies_periode'])
            potenciaperiode = potcon['oferta_potencies_periode']
            consumsperiode = potcon['oferta_energies_periode']

        for id_pot in potenciaperiode:
            dades = pot_oferta_obj.read(cr, uid, id_pot, [
                'periode', 'potencia', 'preu'])

            pot_comp_obj.create(cr, uid, {
                'crm_polissa': ids[0],
                'periode': dades['periode'],
                'data_inici': datainici,
                'data_final': datafi,
                'potencia': dades['potencia'],
                'preu': dades['preu']})

        for id_con in consumsperiode:
            dades = con_oferta_obj.read(cr, uid, id_con, [
                'periode', 'preu'])

            con_comp_obj.create(cr, uid, {
                'crm_polissa': ids[0],
                'periode': dades['periode'],
                'data_inici': datainici,
                'data_final': datafi,
                'preu': dades['preu']})

        return True

    def message_append(self, cr, uid, ids, text=None, mail_id=None):
        if not isinstance(ids, list):
            ids = [ids, ]
        crm_note_obj = self.pool.get('crm.note')
        crm_note_obj.message_append(
            cr, uid, ids, text=text, mail_id=mail_id)
        return True

    def prepare_mail_notification(
            self, cursor, uid, crm_polissa_id, email_template,
            attachments_ids=[], context=None):
        if not context:
            context = {}
        if isinstance(crm_polissa_id, list):
            crm_polissa_id = crm_polissa_id[0]
        if email_template:
            imd = self.pool.get('ir.model.data')
            ctx = {'default_state': 'single',
                   'src_model': 'crm.polissa',
                   'src_rec_ids': [crm_polissa_id],
                   'priority': '0',
                   'crm_polissa_id': crm_polissa_id,
                   'active_id': crm_polissa_id,
                   'active_ids': [crm_polissa_id],
                   'src_rec_ids': [crm_polissa_id]}

            pwetmpl_obj = self.pool.get('poweremail.templates')
            template_id = imd.get_object_reference(cursor, uid, 'crm_elec',
                                                   email_template)[1]
            ctx.update({'template_id': template_id})
            template = pwetmpl_obj.browse(cursor, uid, template_id)
            if template.enforce_from_account:
                mail_from = template.enforce_from_account.id
            else:
                info = (u"No se ha enviado el mail de notificación "
                         u"al usuario de la oferta '%s' porque no hay "
                         u"una cuenta de correo válida "
                         u"para la plantilla '%s'") % (
                    crm_polissa_id, email_template)
                return (u'WARN', info)
            #TODO: Continuar afegint adjunts
            crm_compose_obj = self.pool.get('crm.compose.message')
            wiz_id = crm_compose_obj.create(
                cursor, uid, {'from': mail_from}, context=ctx)
            if attachments_ids:
                crm_compose_obj.write(cursor, uid, [wiz_id],
                              {'attachment_ids': attachments_ids})
            crm_compose_obj.enviar_email(cursor, uid, [wiz_id], context=ctx)
            return True

    def prepare_notification_from_email(self, cursor, uid, crm_polissa_id,
                                        message_id, context=None):
        if not context:
            context = {}
        if isinstance(crm_polissa_id, list):
            crm_polissa_id = crm_polissa_id[0]
        if message_id:
            crm_polissa = self.read(
                cursor, uid, crm_polissa_id, ['conversation_id'])
            mail_obj = self.pool.get('poweremail.mailbox')
            not_obj = self.pool.get('notification')

            category_id = self.pool.get('notification.category').search(
                cursor, uid, [('code', '=', 'OFERTAS')])[0]
            not_obj.create(cursor, uid, {'type': 'poweremail.mailbox,%s' % message_id,
                                         'category_id': category_id})
            vals = {'conversation_id': crm_polissa['conversation_id'][0]}
            mail_obj.write(cursor, uid, [message_id], vals)
            self.message_append(cursor, uid, crm_polissa_id, mail_id=message_id)
            return True

    def send_from_email(self, cursor, uid, ids, mail_id, context=None):
        if not context:
            context = {}
        self.prepare_notification_from_email(
            cursor, uid, ids, mail_id, context=context)
        return True

    def onchange_partner(self, cursor, uid, ids, partner_id, context=None):

        res = {'value': {}}

        # si l'esborrem, buidem les celles corresponents
        if not partner_id:
            res['value']['cliente_nombre'] = False
            res['value']['cliente_apellidos'] = False
            res['value']['cliente_telefono_1'] = False
            res['value']['cliente_telefono_2'] = False
            res['value']['cliente_email_1'] = False
            res['value']['cliente_tipo_documento'] = False
            res['value']['cliente_vat'] = False
            res['value']['cliente_tipo_persona'] = False
            return res

        # models necessaris
        partner_mdl = self.pool.get('res.partner')
        address_mdl = self.pool.get('res.partner.address')

        # llegir el partner, obtenim un diccionari
        partner_read = partner_mdl.read(cursor, uid, [partner_id])[0]

        # agafem les dades de client de la primera adreca que trobem (defecte)
        nom_cognoms = partner_read['name']
        try:
            adreca_id = partner_read['address'][0]
            adreca = address_mdl.read(cursor, uid, [adreca_id])[0]
            correu_e = adreca['email']
            telefon = adreca['phone']
            mobil = adreca['mobile']
        except IndexError: # si no té cap adreca portem els camps buits
            correu_e = ''
            telefon = ''

        n_c = partner_mdl.separa_cognoms(cursor, uid, nom_cognoms)
        nom = n_c['nom']
        cognoms = "{0} {1}".format(n_c['cognoms'][0],n_c['cognoms'][1])

        # document i numero de document
        document = partner_read['vat']
        tipus_document = 'cif'
        if partner_read['cifnif'] == 'NI':
            tipus_document = 'nif'

        if partner_read['cifnif'] == 'NI':
            tipo_persona = 'fisica'
        else:
            tipo_persona = 'juridica'

        #posar-les als camps de la oferta
        res['value']['cliente_nombre'] = nom
        res['value']['cliente_apellidos'] = cognoms
        if tipo_persona == 'juridica':
            res['value']['cliente_nombre'] = False
            res['value']['cliente_apellidos'] = nom_cognoms
        res['value']['cliente_telefono_1'] = telefon
        res['value']['cliente_telefono_2'] = mobil
        res['value']['cliente_email_1'] = correu_e
        res['value']['cliente_tipo_documento'] = tipus_document
        res['value']['cliente_tipo_persona'] = tipo_persona
        res['value']['cliente_vat'] = document

        return res

    def get_dades_mandato(self, cursor, uid, ids, num_ctto, context=None):

        crmpol_obj = self.pool.get('crm.polissa')
        crmpol = crmpol_obj.read(cursor, uid, ids, ['mostrar_sepa_actual'])[0]
        if not crmpol['mostrar_sepa_actual']:
            return {}

        pol = self.pool.get('giscedata.polissa')
        params = [('name', '=', num_ctto)]
        pol_id = pol.search(cursor, uid, params)[0]
        mnd_obj = self.pool.get('payment.mandate')
        params_mnd = [('reference', '=', 'giscedata.polissa,{0}'.format(pol_id)),
                      ('date_end', '=', False)]
        mnd_id = mnd_obj.search(cursor, uid, params_mnd)[0]
        fields = ['debtor_iban_print', 'debtor_name', 'debtor_address']
        mandato = mnd_obj.read(cursor, uid, mnd_id, fields)
        mandato.pop('id')

        return mandato

    def genera_mandato(self, cursor, uid, polissa_id, context=None):
        """Si hi ha un mandato existent, el finalitza amb la data d'ahir i en
        crea un de nou, i si no n'hi havia cap, en crea un de nou"""

        if not context:
            context = {}

        mandate_obj = self.pool.get('payment.mandate')
        search = [('reference', '=', 'giscedata.polissa,%s' % polissa_id),
                  ('date_end', '=', False)]
        mandate_id = mandate_obj.search(cursor, uid, search)

        if mandate_id:
            date_end = (datetime.now() - timedelta(days=1)).strftime('%Y-%m-%d')
            vals = {'date_end': date_end}
            mandate_obj.write(cursor, uid, mandate_id, vals)

        vals = {'reference': 'giscedata.polissa,%s' % polissa_id}
        mandate_obj.create(cursor, uid, vals)

    def get_info_oferta(self, cursor, uid, ids, context=None):
        '''Per facilitar la llegibilitat del report oferta, generem aquí les
        dades necessàries.

        Retorna un diccionari.
        Formata a 6 decimals totes les dades de preus
        '''
        dades_report_oferta = {}

        # nom de la tarifa ofertada
        oferta = self.read(cursor, uid, ids, [
            'tarifa_oferta',
            'validez_oferta',
            'oferta_energies_periode',
            'oferta_potencies_periode',
            'type',
            'numero_ctto',
            'user_id',
            'llista_preu',
            'potencia_ofertada',
            'oferta_potencies_periode'
            'id'])[0]

        # Si la potencia ofertada es 0 no deixem imprimir
        if oferta['potencia_ofertada'] <= 0.0:
            msg = 'No se puede imprimir una oferta con potencia ofertada' \
                  ' {}'.format(oferta['potencia_ofertada'])
            raise osv.except_osv('Error', msg)

        # data d'avui
        dades_report_oferta['data'] = datetime.today().strftime('%d/%m/%Y')

        # nom de la tarifa ofertada
        nom_tarifa_oferta = oferta['tarifa_oferta'][1]
        dades_report_oferta['tarifa_ofertada'] = nom_tarifa_oferta

        dades_report_oferta['debtor_iban_print'] = ''
        dades_report_oferta['debtor_name'] = ''
        dades_report_oferta['debtor_address'] = ''

        # si es oferta de renovacio, indicarem el contracte a renovar al report
        if oferta['type'] == 'renovacion':
            dades_report_oferta['numero_ctto'] = oferta['numero_ctto']
            try:
                # portarem les dades sepa del mandato actual a la trasera
                # nom del client, adreca del client, iban
                d = self.get_dades_mandato(cursor, uid, ids, oferta['numero_ctto'])
                dades_report_oferta.update(d)
            except IndexError as e: # no té mandato, es queden els camps buits
                pass

        # preus peatge energia ofertada i noms periodes oferta
        nom_periodes_energia_oferta = []
        preu_peatges_energia = {}
        periodes_obj = self.pool.get('giscedata.polissa.tarifa.periodes')
        tp_obj = self.pool.get('giscedata.tp')
        params = [('tarifa.id','=',oferta['tarifa_oferta'][0]),
                  ('tipus','=','te'),
                  ('agrupat_amb','=',False)]
        periodes_e_ids = periodes_obj.search(cursor, uid, params)

        for periode_id in periodes_e_ids:
            peatje_price = tp_obj.\
                            get_peatge_price_from_period_id(cursor,
                                                        uid,
                                                        periode_id)
            nom_periode = periodes_obj.read(
                            cursor, uid, [periode_id], ['name'])[0]['name']
            preu_s = "{0:.6f}".format(peatje_price)
            preu_peatges_energia[nom_periode] = preu_s
            nom_periodes_energia_oferta.append(nom_periode)
        dades_report_oferta['preu_peatges_energia'] = preu_peatges_energia
        dades_report_oferta['nom_periodes_energia_oferta'] = nom_periodes_energia_oferta

        # Descomptes
        tarifa_obj = self.pool.get('giscedata.polissa.tarifa')
        pricelist_obj = self.pool.get('product.pricelist')
        pricelistver_obj = self.pool.get('product.pricelist.version')
        pricelistitem_obj = self.pool.get('product.pricelist.item')
        pot_periode_obj = self.pool.get('crm.potencia.contractada.periode')

        pricelist_id = oferta['llista_preu'][0]
        global_discount = 0.0
        global_discount_potencia = 0.0
        discount_energia = 0.0
        discount_potencia = 0.0
        if pricelist_obj.read(
                cursor, uid, pricelist_id, ['visible_discount']
        )['visible_discount']:
            date_price = date.today().strftime('%Y-%m-%d')
            product_id = 1
            for pot_id in oferta['oferta_potencies_periode']:
                pot = pot_periode_obj.read(
                    cursor, uid, pot_id, ['periode']
                )['periode']
                version = pricelistver_obj.search(
                    cursor,
                    uid,
                    [
                        ('pricelist_id', '=', pricelist_id),
                        '|',
                        ('date_start', '=', False),
                        ('date_start', '<=', date_price),
                        '|',
                        ('date_end', '=', False),
                        ('date_end', '>=', date_price)
                    ], order='id', limit=1
                )
                tarifa = tarifa_obj.browse(cursor, uid,
                                           oferta['tarifa_oferta'][0])
                # Descompte energia
                product_energia_id = tarifa.get_periodes_producte('te')[pot]
                if version:
                    items = pricelistitem_obj.search(
                        cursor, uid, [
                            ('price_version_id', '=', version[0]),
                            ('product_id', '=', product_energia_id)
                        ]
                    )
                    if items:
                        params_to_read = ['price_discount']
                        for item in pricelistitem_obj.read(
                                cursor, uid, items, params_to_read):
                            if item['price_discount']:
                                discount_energia = item['price_discount']
                                break
                if discount_energia and not global_discount:
                    global_discount = discount_energia * 100.0

                # Descompte potencia
                product_potencia_id = tarifa.get_periodes_producte('tp')[pot]
                if version:
                    items = pricelistitem_obj.search(
                        cursor, uid, [
                            ('price_version_id', '=', version[0]),
                            ('product_id', '=', product_potencia_id)
                        ]
                    )
                    if items:
                        params_to_read = ['price_discount']
                        for item in pricelistitem_obj.read(
                                cursor, uid, items, params_to_read):
                            if item['price_discount']:
                                discount_potencia = item['price_discount']
                                break
                if discount_potencia and not global_discount_potencia:
                    global_discount_potencia = discount_potencia * 100.0

        if global_discount:
            global_discount = abs(global_discount)
        dades_report_oferta['descompte_energia'] = global_discount

        if global_discount_potencia:
            global_discount_potencia = abs(global_discount_potencia)
        dades_report_oferta['descompte_potencia'] = global_discount_potencia

        # preu peatge potencia i valors potencies contractades
        pot_obj = self.pool.get('crm.potencia.contractada.periode')
        search_params = [('crm_polissa.id', '=', oferta['id'])]
        pot_cttada_ids = pot_obj.search(cursor, uid, search_params)
        potencies_contractades = {}
        preu_pot_cttades = {}
        nom_periodes_potencia_oferta = []
        for pot_id in pot_cttada_ids:
            pot = pot_obj.read(cursor, uid, pot_id)
            if pot['tipus'] == 'oferta':
                preu_orig = pot['preu'] / (1-abs(discount_potencia))
                preu_s = "{0:.6f}".format(preu_orig)
                preu_pot_cttades[pot['periode']] = preu_s
                potencies_contractades[pot['periode']] = pot['potencia']
                nom_periodes_potencia_oferta.append(pot['periode'])
        dades_report_oferta['preu_pot_cttades'] = preu_pot_cttades
        dades_report_oferta['potencies_contractades'] = potencies_contractades
        dades_report_oferta['nom_periodes_potencia_oferta'] = nom_periodes_potencia_oferta

        # preus d'energia PVP
        ene_obj = self.pool.get('crm.energia.contractada.periode')
        pt_obj = self.pool.get('crm.pt.periode')
        preus_energia_pvp = {}
        for p_e_id in oferta['oferta_energies_periode']:
            pe = ene_obj.read(cursor, uid, p_e_id, ['preu','periode'])
            preu = pe['preu'] / (1-abs(discount_energia))
            preu_s = "{0:.6f}".format(preu)
            preus_energia_pvp[pe['periode']] = preu_s
        dades_report_oferta['preus_energia_pvp'] = preus_energia_pvp

        # calcul nomes energia i energia amb IEE
        nomes_energia = {}
        amb_iee = {}
        for periode in nom_periodes_energia_oferta:
            oferta['oferta_energies_periode']

            preu_nomes_energia = float(preus_energia_pvp[periode]) - \
                                        float(preu_peatges_energia[periode])
            nomes_energia_s = "{0:.6f}".format(preu_nomes_energia)
            nomes_energia[periode] = nomes_energia_s
            preu_amb_iee = float(preus_energia_pvp[periode]) * 1.051127
            preu_iee_s = "{0:.6f}".format(preu_amb_iee)
            amb_iee[periode] = preu_iee_s
        dades_report_oferta['nomes_energia'] = nomes_energia
        dades_report_oferta['amb_iee'] = amb_iee

        # dades del comercial assessor: nom, mail, telf, adreça delegació
        # buscar el employee a partir de l'usuari, per treure el mail i telèfon
        # oferta['user_id'] és una tupla: (id, u'nom del comercial')
        search_params = [('user_id', '=', oferta['user_id'][0])]
        employee_obj = self.pool.get('hr.employee')
        emp_id = employee_obj.search(cursor, uid, search_params)
        try:
            employee = employee_obj.read(cursor, uid, emp_id, ['work_office',
                                         'mobile_phone',
                                         'work_email',
                                         'first_name',
                                         'middle_name'])[0]
        except IndexError:
            user_obj = self.pool.get('res.users')
            nom_user = user_obj.read(cursor, uid, oferta['user_id'][0],['name'])
            msg = 'No se puede imprimir una oferta con comercial {0}'.format(
                    nom_user['name'])
            raise osv.except_osv('Error', msg)
            return False

        nom = '{0} {1}'.format(employee['first_name'],employee['middle_name'])
        dades_report_oferta['nom_comercial'] = nom
        dades_report_oferta['mobile_phone'] = employee['mobile_phone'] or ''
        dades_report_oferta['work_email'] = employee['work_email'] or ''

        # buscar hr.office a partir del work_office del employee per
        # treure l'adreça de la delegació
        hr_office_obj = self.pool.get('hr.office')
        office = hr_office_obj.read(cursor, uid, employee['work_office'][0])
        direcc_oficina = office.get('office_address','')
        if direcc_oficina == False: direcc_oficina = ''
        dades_report_oferta['office_address'] = direcc_oficina

        # afegim validez de la oferta
        validez = self.pool.get('crm.polissa.validez')
        dias = validez.read(cursor, uid, oferta['validez_oferta'][0], ['name'])
        dades_report_oferta['dias_validez'] = dias['name']

        return dades_report_oferta

    def onchange_type(self, cursor, uid, id, context=None):
        res = {'value': {}}
        tipo = self.read(cursor, uid, id, ['type'])
        if tipo:
            if tipo[0]['type'] == 'renovacion':
                res['value']['contrato_name'] = False
        return res

    def onchange_numero_ctto(self, cursor, uid, id, context=None):

        # esborrar totes les dades de partner/cups/distri que hi hagues
        res = {'value': {}}
        res['value']['partner_id'] = False
        res['value']['cups_id'] = False
        res['value']['cups_distribuidora_id'] = False
        res['value']['tarifa_oferta'] = False
        res['value']['cups_name'] = False
        res['value']['cups_id_municipi'] = False
        res['value']['cups_id_poblacio'] = False
        res['value']['cups_nv'] = False
        res['value']['cups_pnp'] = False
        res['value']['cups_es'] = False
        res['value']['cups_pt'] = False
        res['value']['cups_pu'] = False
        res['value']['cups_tv'] = False
        res['value']['cups_ref_catastral'] = False
        res['value']['cups_distribuidora_id'] = False
        res['value']['cups_dp'] = False
        res['value']['cups_aclarador'] = False
        res['value']['cups_cpo'] = False
        res['value']['cups_cpa'] = False
        res['value']['cups_id_provincia'] = False
        res['value']['cliente_nombre'] = False
        res['value']['cliente_apellidos'] = False
        res['value']['cliente_telefono_1'] = False
        res['value']['cliente_email_1'] = False
        res['value']['cliente_tipo_documento'] = False
        res['value']['cliente_vat'] = False
        res['value']['consumo_anual_previsto_directo'] = False
        res['value']['pagador_autorizacion_sepa'] = False
        res['value']['direccion_clte_sepa'] = False
        res['value']['num_cta'] = False
        res['value']['entidad_bancaria'] = False
        res['value']['contrato_name'] = False
        res['value']['contacto_direccion'] = False
        res['value']['cliente_direccion'] = False
        res['value']['cnae'] = False
        res['value']['zona_carta_id'] = False

        return res

    def onchange_reload_deleg(self, cursor, uid, ids, user_id, context=None):
        ''' Assignem la delegacio que tingui l'usuari/empleat.
        Si l'usuari no té delegació, assumim que es tracta d'un colaborador i
        si existeix la delegacio 'Colaboradores', li assignem.
        En qualsevol altre cas, esborrem la delegació que hi hagués'''
        empl_obj = self.pool.get('hr.employee')
        employee_id = empl_obj.search(cursor, uid, [('user_id', '=', user_id)])
        res = {'value': {'delegacion': False}}
        if employee_id:
            employee_office = empl_obj.read(cursor, uid, employee_id[0],
                                                ['work_office'])
            if employee_office.get('work_office', False):
                delegacion =  employee_office['work_office'][0]
                res['value'] = {'delegacion': delegacion}
            else:
                hr_office_obj = self.pool.get('hr.office')
                search_params = [('name', '=', 'Colaboradores')]
                oficina_colab = hr_office_obj.search(cursor, uid, search_params)[0]
                if oficina_colab:
                    res['value'] = {'delegacion': oficina_colab}
        return res

    def onchange_bring_cups_info(self, cursor, uid, ids, cups_id, context=None):
        if not context:
            context = {}
        res = {'value': {}}
        cups_obj = self.pool.get('giscedata.cups.ps')

        # si l'esborrem, esborrem les celles crresponents
        if not cups_id:
            res['value']['cups_name'] = False
            res['value']['cups_id_municipi'] = False
            res['value']['cups_id_poblacio'] = False
            res['value']['cups_nv'] = False
            res['value']['cups_pnp'] = False
            res['value']['cups_es'] = False
            res['value']['cups_pt'] = False
            res['value']['cups_pu'] = False
            res['value']['cups_tv'] = False
            res['value']['cups_ref_catastral'] = False
            res['value']['cups_distribuidora_id'] = False
            res['value']['cups_dp'] = False
            res['value']['cups_aclarador'] = False
            res['value']['cups_cpo'] = False
            res['value']['cups_cpa'] = False
            res['value']['cups_id_provincia'] = False
            res['value']['potencia_client'] = False
            return res

        cups = cups_obj.read(cursor, uid, cups_id, [])

        # mostrem les dades que podem portar del cups existent
        res['value']['cups_name'] = cups.get('name', '')
        res['value']['cups_id_municipi'] = cups.get('id_municipi')
        res['value']['cups_id_poblacio'] = cups.get('id_poblacio')
        res['value']['cups_nv'] = cups.get('nv', '')
        res['value']['cups_pnp'] = cups.get('pnp', '')
        res['value']['cups_es'] = cups.get('es', '')
        res['value']['cups_pt'] = cups.get('pt', '')
        res['value']['cups_pu'] = cups.get('pu', '')
        res['value']['cups_tv'] = cups.get('tv')
        res['value']['cups_ref_catastral'] = cups.get('ref_catastral', '')
        res['value']['cups_distribuidora_id'] = cups.get('distribuidora_id')
        res['value']['cups_dp'] = cups.get('dp', '')
        res['value']['cups_aclarador'] = cups.get('aclarador', '')
        res['value']['cups_cpo'] = cups.get('cpo', '')
        res['value']['cups_cpa'] = cups.get('cpa', '')
        res['value']['cups_id_provincia'] = cups.get('id_provincia')
        res['value']['potencia_client'] = cups.get('polissa_potencia', 0.0)

        if res['value']['cups_id_municipi']:
            res['value']['cups_id_municipi'] = cups.get('id_municipi')[0]
        if res['value']['cups_id_poblacio']:
            res['value']['cups_id_poblacio'] = cups.get('id_poblacio')[0]
        if res['value']['cups_distribuidora_id']:
            res['value']['cups_distribuidora_id'] = cups.get('distribuidora_id')[0]
        if res['value']['cups_id_provincia']:
            res['value']['cups_id_provincia'] = cups.get('id_provincia')[0]
        if res['value']['cups_tv']:
            res['value']['cups_tv'] = cups.get('tv')[0]


        return res

    def onchange_load_entidad_bancaria(self, cursor, uid, ids, ptbank_id,
                                           context=None):
        if not context:
            context = {}
        res = {'value': {}}
        ptbank_obj = self.pool.get('res.partner.bank')
        bank_obj = self.pool.get('res.bank')
        if not ptbank_id:
            res['value']['entidad_bancaria'] = ''
        else:
            bankid = ptbank_obj.read(cursor, uid, ptbank_id, ['bank'])['bank'][0]
            entidad = bank_obj.read(cursor, uid, bankid, ['lname'])['lname']
            res['value']['entidad_bancaria'] = entidad
        return res

    def crear_pot_periode(self, cursor, uid, ids, polissa_id):

        pot_periode_polissa_obj = self.pool.get(
            'giscedata.polissa.potencia.contractada.periode')
        polissa_obj = self.pool.get('giscedata.polissa')
        tarifa_periodes_obj = self.pool.get('giscedata.polissa.tarifa.periodes')
        crm_pot_obj = self.pool.get('crm.potencia.contractada.periode')
        pots = polissa_obj.read(cursor, uid, polissa_id,
                                ['potencies_periode'])['potencies_periode']

        # esborrar-los si existien abans
        for crm in self.browse(cursor, uid, ids):
            search_params = [('crm_polissa', '=', crm.id),
                             ('tipus', '=', 'client')]
            c_ids = crm_pot_obj.search(cursor, uid, search_params)
            crm_pot_obj.unlink(cursor, uid, c_ids)

        # crear els nous
        for periode_id in pots:
            # copiar els periodes de la polissa als periodes de la oferta
            periode = pot_periode_polissa_obj.read(cursor, uid, periode_id)
            id_per = periode['periode_id'][0]
            params = [('id', '=', id_per)]
            per_id = tarifa_periodes_obj.search(cursor, uid, params)
            per = tarifa_periodes_obj.read(cursor, uid, per_id)
            per_nom = per[0]['name']

            crm_pot_obj.create(cursor, uid, {
                'periode': per_nom,
                'crm_polissa': ids[0],
                'potencia': periode['potencia'],
                'tipus': 'client',
                }
            )

        return True

    def btn_copiar_ctto_actual(self, cursor, uid, id, context=None):

        # obtenir el numero de contracte
        crm_polissa_obj = self.pool.get('crm.polissa')
        crm_polissa_id = id
        crm_pol = crm_polissa_obj.read(cursor, uid, crm_polissa_id,
                                       ['numero_ctto'])[0]
        num_ctto = crm_pol['numero_ctto']

        # llegir el contracte i obtenir distri, titular, cups
        polissa_obj = self.pool.get('giscedata.polissa')
        bank_obj = self.pool.get('res.partner.bank')
        search_params = [('name','=', num_ctto)]
        polissa_id = polissa_obj.search(cursor, uid, search_params,
                                        context={'active_test': False})[0]
        fields = [
            'cups',
            'titular',
            'tarifa',
            'distribuidora',
            'cnae',
            'direccio_notificacio',
            'direccio_pagament',
            'bank',
            'consumo_anual_calculado',
            'fact_potencia_100',
            'zona_carta_id',
        ]
        polissa = polissa_obj.read(cursor, uid, polissa_id, fields)

        # crear els crm_pot_obj a partir de les potències contractades actuals
        self.crear_pot_periode(cursor, uid, id, polissa_id)
        polissa_bank_id = polissa['bank'][0]
        polissa_bank_owner_id = bank_obj.read(
            cursor, uid, polissa_bank_id, ['owner_id']
        )['owner_id'][0]
        # Recuperem l'entitat bancaria a traves del banc
        banc = bank_obj.browse(cursor, uid, polissa['bank'][0])
        vals = {
            'polissa_id': polissa_id,
            'cups_id': polissa['cups'][0],
            'partner_id': polissa['titular'][0],
            'cups_distribuidora_id': polissa['distribuidora'][0],
            'tarifa_oferta': polissa['tarifa'][0],
            'cnae': polissa['cnae'][0],
            'contacto_direccion': polissa['direccio_notificacio'][0],
            'cliente_direccion': polissa['direccio_pagament'][0],
            'direccion_clte_sepa': polissa['direccio_pagament'][0],
            'pagador_autorizacion_sepa': polissa_bank_owner_id,
            'num_cta': polissa['bank'][0],
            'entidad_bancaria': banc.bank.lname,
            'consumo_anual_previsto_directo': polissa['consumo_anual_calculado'],
            'fact_potencia_100': polissa['fact_potencia_100'],
            'contrato_name': num_ctto,
            'zona_carta_id': polissa['zona_carta_id'][0]
                                if polissa['zona_carta_id'] else False,
        }

        # cridar els onchange pel partner/cups
        v_cups = self.onchange_bring_cups_info(
                        cursor, uid, id, polissa['cups'][0], context=None)
        v_ptnr = self.onchange_partner(
                        cursor, uid, id, polissa['titular'][0], context=None)

        vals.update(v_cups['value'])
        vals.update(v_ptnr['value'])

        # escriure-les a la oferta
        crm_polissa_obj.write(cursor, uid, crm_polissa_id, vals)

        return True

    def obtener_numero_contrato(self, cursor, uid, ids, context=None):

        num_contrato = self.pool.get('ir.sequence').get(cursor, uid,
                                                      'giscedata.polissa')
        self.write(cursor, uid, ids, {'contrato_name': num_contrato})
        return num_contrato

    def copiar_potencia_actual_a_ofertada(self, cursor, uid, ids, context=None):
        # obtenir la potencia contractada actual
        crm_polissa_obj = self.pool.get('crm.polissa')
        pot_actual = crm_polissa_obj.read(cursor, uid, ids,
                                       ['potencia_client'])
        pot_actual = pot_actual[0].get('potencia_client', 0.0)
        self.write(cursor, uid, ids, {'potencia_ofertada': pot_actual})
        return True

    def get_tariff_domain(self, cursor, uid, tariff_id, context=None):
        """return the sale pricelists domain compatible with a tariff"""
        tariff_obj = self.pool.get('giscedata.polissa.tarifa')
        mdata_obj = self.pool.get('ir.model.data')

        tariff_fields = ['llistes_preus_comptatibles']
        tariff_vals = tariff_obj.read(cursor, uid, tariff_id, tariff_fields)

        price_lists = tariff_vals['llistes_preus_comptatibles']

        tariff_domain = [('type', '=', 'sale')]

        tariff_domain.append(('id', 'in', price_lists))

        return tariff_domain, price_lists

    def onchange_tarifa_oferta(self, cursor, uid, ids, tarifa, context=None):
        if not context:
            context = {}
        if not tarifa:
            return
        res = {'value': {}, 'domain': {}, 'warning': {}}
        tariff_obj = self.pool.get('giscedata.polissa.tarifa')
        tarifa = tariff_obj.browse(cursor, uid, tarifa)
        tarif_inf = tariff_obj.read(cursor, uid, tarifa.id,
                                    ['tipus', 'mesura_ab'])
        tariflb = tarif_inf['tipus'] == 'AT' and \
                  tarif_inf['mesura_ab'] == 'B'

        tariff_id = tariff_obj.get_tarifa_from_ocsum(cursor, uid, tarifa.codi_ocsum, tariflb)
        tariff_domain, price_lists = self.get_tariff_domain(
            cursor, uid, tariff_id, context=context
        )
        tariff_domain.append(('ofertable', '=', 'True'))
        res['domain'].update({'llista_preu': tariff_domain})
        return res

    def check_k_d(self, cursor, uid, ids, context=None):
        imd = self.pool.get('ir.model.data')
        conf = self.pool.get('crm.polissa.config')

        camps = self.read(cursor, uid, ids[0], ['mode_facturacio',
                                                'coeficient_k',
                                                'coeficient_d'])
        mode = camps['mode_facturacio']
        k = camps['coeficient_k']
        d = camps['coeficient_d']
        if mode == 'index':
            minidx_id = imd.get_object_reference(
                cursor, uid, "crm_elec", 'crm_elec_min_idx'
            )[1]
            max_d_id = imd.get_object_reference(
                cursor, uid, "crm_elec", 'crm_elec_max_d'
            )[1]
            minidx = float(conf.read(cursor, uid, minidx_id)['value'])
            maxd = float(conf.read(cursor, uid, max_d_id)['value'])
            if k+d < minidx or d > maxd:
                raise osv.except_osv('Error',
                                     'K + D deben sumar mínimo {} '
                                     '€/MWh. D no puede ser superior a {}'.
                                     format(minidx, maxd))
        return

    _columns = {
        'case_id': fields.many2one(
            'crm.case',
            'Oportunidad',
            ondelete='cascade'
        ),
        'user_id': fields.many2one('res.users', 'Comercial', select=1),
        'delegacion': fields.many2one('hr.office', 'Delegación'),
        'director_comercial': fields.function(
            _ff_perm_to_modify, type='boolean', method=True,
            string=u'Modificar delegación', size=40),
        'type': fields.selection(TIPO_OFERTA, string='Tipo oferta', select=1),
        #camps creats exportació
        'cups_id': fields.many2one(
            'giscedata.cups.ps', 'CUPS existente'),
        # relacional a partners, per copiar dades de client de partner existent
        'partner_id': fields.many2one(
            'res.partner', 'Cliente existente'),
        'polissa_id': fields.many2one(
            'giscedata.polissa', u'Póliza creada', select=1),
        'export_file': fields.binary('Contrato audinfor'),
        'audinfor_file_name': fields.char('Filename audinfor', size=64),
        'export_connsums_file': fields.binary('Consumos audinfor'),
        'audinfor_consums_file_name': fields.char('Filename consumos audinfor',
                                                  size=64),
        #camps de giscedata.polissa
        'tarifa_oferta': fields.many2one(
            'giscedata.polissa.tarifa', "Tarifa ofertada",
            readonly=True,
            states={
                'esborrany': [('readonly', False)],
                'validar': [('readonly', False),
                            ('required', True)],
                'modcontractual': [('readonly', False),
                                   ('required', True)]
            }),
        'titular': fields.function(
            _ff_titular, type='char', method=True,
            string='Titular', size=40),
        'state': fields.selection(
            _polissa_states_selection, 'Estat',
            required=True, readonly=True),
        'potencia_client': fields.float('Potencia contratada actual (kW)', digits=(16,3)),
        'potencia_ofertada': fields.float('Potencia ofertada (kW)', digits=(16, 3)),
        #camps pel cups
        'numero_ctto': fields.char('Nº Contrato', size=23),
        'cups_name': fields.char('CUPS', size=23),
        'cups_distribuidora_id': fields.many2one(
            'res.partner', 'Distribuidora'),
        'cups_id_municipi': fields.many2one('res.municipi', 'Municipio'),
        'cups_id_poblacio': fields.many2one('res.poblacio', 'Población'),
        'cups_id_provincia': fields.many2one(
            'res.country.state', 'Provincia',
            domain="[('country_id','=',cliente_country_id)]"),
        'cups_tv': fields.many2one('res.tipovia', 'Tipo vía'),
        'cups_nv': fields.char('Nombre vía', size=256),
        'cups_pnp': fields.char(u'Número', size=9),
        'cups_es': fields.char('Escalera', size=4),
        'cups_pt': fields.char('Planta', size=4),
        'cups_pu': fields.char('Puerta', size=4),
        'cups_cpo': fields.char('Poligono', size=4),
        'cups_cpa': fields.char('Parcela', size=4),
        'cups_dp': fields.char('CP', size=5),
        'cups_aclarador': fields.char('Aclarador', size=256),
        'cups_ref_catastral': fields.char('Ref Catastral (c)', size=20),
        'link_catastro': fields.char('Acceso al Catastro', size=200),

        # especifics d'oferta
        'section_id': fields.many2one(
            'crm.case.section', u'Sección'),
        'stage_id': fields.many2one(
            'crm.case.stage', 'Etapa',
            domain="[('section_ids', '=', section_id)]", select=True,),
        'stage_name': fields.function(
            _ff_stage_name, type='char', method=True,
            string='Nombre etapa'),
        'probability': fields.function(_ff_probability,
                                       type='float',
                                       string='Progreso (%)',
                                       method=True),
        'fecha_prevista_envio_oferta': fields.date(
            string='Fecha prevista envio oferta'),
        'campaign_id': fields.many2one(
            'crm.case.resource.type', u'Campaña',
            domain="[('section_id','=',section_id)]",
            ),
        'channel_id': fields.many2one('res.partner.canal', 'Canal'),
        'cliente_tipo_persona': fields.selection(TIPO_PERSONA, string='Tipo persona'),
        'cliente_nombre': fields.char('Nombre', size=128),
        'cliente_apellidos': fields.char('Apellidos', size=256),
        'cliente_tipo_documento': fields.selection(
            TIPO_DOCUMENTO, string='Tipo documento'),
        'cliente_vat': fields.char(u'Nº Documento', size=32),
        'cliente_telefono_1': fields.char(u'Teléfono', size=15),
        'cliente_telefono_2': fields.char(u'Teléfono 2', size=15),
        'cliente_email_1': fields.char(u'E-mail', size=240),
        'cliente_group': fields.many2one(
            'giscedata.polissa.group', 'Grupo facturación', select=1),
        'cups_sips': fields.char('CUPS', size=22),
        'sips_estado': fields.selection(ESTADO_SIPS, 'Estado SIPS'),
        'sips_direccion': fields.char(u'SIPS Dirección Suministro', size=80),
        'sips_tarifa': fields.char('Tarifa de acceso SIPS', size=15),
        'sips_titular': fields.char('Titular suministro SIPS', size=128),
        'sips_apellidos': fields.char('Apellidos suministro SIPS', size=128),
        'sips_cif': fields.char('CIF del titular SIPS', size=128),
        'contacto_comentario': fields.text('Comentario'),
        'tarifa_client': fields.many2one('giscedata.polissa.tarifa', "Tarifa actual cliente",
                                  readonly=True,
                                  states={
                                     'esborrany':[('readonly', False)],
                                     'validar': [('readonly', False),
                                                 ('required', True)],
                                     'modcontractual': [('readonly', False),
                                                        ('required', True)]
                                  }),
        'tarifa_client_comparativa': fields.function(
            _ff_tarifa_cliente, type='many2one', method=True,
            obj='giscedata.polissa.tarifa', string='Tarifa actual cliente'
        ),
        'subsistema_oferta': fields.selection(SUBSISTEMES, string='Subsistema'),
        'comentarios_oferta': fields.text(string='Comentarios'),
        'condicions_generals_id': fields.many2one(
            'giscedata.polissa.condicions.generals', 'Condiciones generales'),
        'validez_oferta': fields.many2one(
            'crm.polissa.validez', string='Validez oferta'),
        'llista_preu': fields.many2one('product.pricelist',
                            'Tarifa Comercializadora',
                            domain=[('ofertable', '=', 'True')]),
        'fact_potencia_100': fields.boolean(u'Fact. maxímetro 100%'),
        'mcp_base': fields.many2one('crm.mcp', 'MCP base'),
        'mcp_definitivo': fields.float('MCP definitivo'),
        'pts_oferta': fields.one2many(
            'crm.pt.periode', 'crm_polissa', string='PTS'),
        'client_potencies_periode': fields.function(
            _ff_potencies, type='one2many', method=True,
            fnct_inv=_set_potencies,  fnct_inv_arg={'tipus': TIPUS['client']},
            obj='crm.potencia.contractada.periode',
            arg={'tipus': TIPUS['client']}, string='Potencies contratadas cliente'
        ),
        'oferta_potencies_periode': fields.function(
            _ff_potencies, type='one2many', method=True,
            fnct_inv=_set_potencies,  fnct_inv_arg={'tipus': TIPUS['oferta']},
            obj='crm.potencia.contractada.periode',
            arg={'tipus': TIPUS['oferta']}, string='Potencies contratadas oferta'
        ),
        'client_energies_periode': fields.function(
            _ff_energia, type='one2many', method=True,
            fnct_inv=_set_energia, fnct_inv_arg={'tipus': TIPUS['client']},
            obj='crm.energia.contractada.periode',
            arg={'tipus': TIPUS['client']}, string='Energia contratadas cliente'
        ),
        'oferta_energies_periode': fields.function(
            _ff_energia, type='one2many', method=True,
            fnct_inv=_set_energia, fnct_inv_arg={'tipus': TIPUS['oferta']},
            obj='crm.energia.contractada.periode',
            arg={'tipus': TIPUS['oferta']}, string='Energia contratadas oferta'
        ),
        'client_consums_periode': fields.function(
            _ff_consums, type='one2many', method=True,
            fnct_inv=_set_consums, fnct_inv_arg={'tipus': TIPUS['client']},
            obj='crm.consums.periode',
            arg={'tipus': TIPUS['client']}, string='Consumos cliente'
        ),
        'client_consums_periode_ui': consums_one2many(
            'crm.consums.periode.ui', 'crm_polissa',
            string='Consumos cliente'
        ),
        'consumo_anual_previsto': fields.function(
            _ff_consumo_anual_previsto, type='char', method=True, size=250,
            string='Consumo anual previsto calculado en base a consumos '
                   'introducidos'
        ),
        'consumo_anual_previsto_directo': fields.integer(
         string='Consumo anual previsto introducido manualmente'
        ),
        'documentacio': fields.one2many(
            'crm.documentacio', 'crm_polissa', u'Documentación'
        ),
        'oferta_consums_periode': fields.function(
            _ff_consums, type='one2many', method=True,
            fnct_inv = _set_consums, fnct_inv_arg = {'tipus': TIPUS['oferta']},
            obj = 'crm.consums.periode',
            arg = {'tipus': TIPUS['oferta']}, string = 'Consumos oferta'
        ),
        'propostes': fields.function(
            _ff_documents, type='one2many', method=True,
            #fnct_inv=_set_consums, fnct_inv_arg={'tipologis': TIPOLOGIA['documentos_generados']},
            obj='crm.documentacio',
            arg={'tipologia': TIPOLOGIA['documentos_generados']},
            string='Propuesta'),
        'docs_client': fields.one2many(
            'crm.documentacio', 'crm_polissa', u'Documentación cliente',
                domain=[('tipologia', '=', 'documentacio_client')]),
        'docs_final': fields.function(
            _ff_documents, type='one2many', method=True,
            #fnct_inv=_set_consums, fnct_inv_arg={'tipologis': TIPOLOGIA['documentos_generados']},
            obj='crm.documentacio',
            arg={'tipologia': TIPOLOGIA['documentos_finales']},
            string='Documentos finales '),
        'docs_ofertas': fields.function(
            _ff_documents, type='one2many', method=True,
            fnct_inv=_set_documents,
            fnct_inv_arg={'tipologia': TIPOLOGIA['documentos_generados'],
                          'tipus_document': 'oferta'},
            obj='crm.documentacio',
            arg={'tipologia': TIPOLOGIA['documentos_generados'],
                 'tipus_document': 'oferta'},
            string='Ofertas'),
        'docs_comparativas': fields.function(
            _ff_documents, type='one2many', method=True,
            fnct_inv=_set_documents,
            fnct_inv_arg={'tipologia': TIPOLOGIA['documentos_generados'],
                          'tipus_document': 'comparativa'},
            obj='crm.documentacio',
            arg={'tipologia': TIPOLOGIA['documentos_generados'],
                 'tipus_document': 'comparativa'},
            string='Comparativas'),
        'docs_contratos': fields.function(
            _ff_documents, type='one2many', method=True,
            fnct_inv=_set_documents,
            fnct_inv_arg={'tipologia': TIPOLOGIA['documentos_generados'],
                          'tipus_document': 'contrato'},
            obj='crm.documentacio',
            arg={'tipologia': TIPOLOGIA['documentos_generados'],
                 'tipus_document': 'contrato'},
            string='Contratos'),

        'tipologia_documents': fields.selection(
            [('documentacio_client', 'DOCUMENTACIÓN CLIENTE'),
             ('documentos_generados', 'DOCUMENTOS GENERADOS')],
            'Tipologia documentos',),
        'dades_modificar': fields.selection(DADES_MODIFICAR_SELECTION,
                                            'Datos a modificar'),

        'missatges_selection': fields.function(
          _ff_missatges_selection, type='selection', method=True,
          selection=_missatges_selection, string='Mostrar'
        ),
        'current_missatge': fields.integer(string='Mensaje actual'),

        'body_text': fields.function(_ff_body_text, type='text', method=True,
                                     string='Cuerpo del mensaje'),
        'attachment_ids': fields.function(
            _ff_attachment_ids, type='one2many', method=True,
            #fnct_inv=_set_consums, fnct_inv_arg={'tipologis': TIPOLOGIA['documentos_generados']},
            obj='ir.attachment', string='Adjuntos del mensaje'),
        'document_viewer': fields.function(
            lambda *a: dict.fromkeys(a[3], ''),
            type='char',
            size=256,
            string='Document viewer',
            method=True
        ),
        'pers_firmante_id': fields.many2one('res.partner', 'Persona firmante'),
        'pers_firmante_nif': fields.related('pers_firmante_id', 'vat',
                                            type='char', size=32,
                                            string=u'Nº Documento firmante',
                                            readonly=True),
        'pagador_autorizacion_sepa': fields.many2one('res.partner',
                                                     'Propietario cuenta'),
        'direccion_clte_sepa': fields.many2one('res.partner.address',
                                               'Dirección cliente SEPA'),
        'num_cta': fields.many2one('res.partner.bank', u'Número cuenta'),
        'entidad_bancaria': fields.char('Entidad bancaria',
                                        size=240),
        'conversation_id': fields.many2one('poweremail.conversation', 'Messages'),
        'notes_id': fields.one2many('crm.note', 'crm_polissa', string='Notas'),
        'alarms_ids': fields.many2many('calendar.alarm', 'crm_polissa_alarm', 'crm_polissa_id', 'alarm_id', 'Alarms'),
        'sips_import_state': fields.char(u'Estado importación SIPS', size=300, readonly=True),
        'mostrar_sepa_actual': fields.boolean(
                            'Mostrar datos SEPA actual en oferta renovación'),
        'fecha_estimada_inicio_ctto': fields.date(
                            string='Fecha estimada inicio contrato'),
        'mode_facturacio': fields.selection(FACTURACION_ATR_INDEXADA,
                                            'Tipo de facturación'),
        'coeficient_k': fields.float('Coeficiente K en €/MWh', digits=(4, 2)),
        'coeficient_d': fields.float('Coeficiente D en €/MWh', digits=(4, 2)),
        'fecha_firma': fields.date(string='Fecha de firma'),
        'cnae': fields.many2one('giscemisc.cnae', 'CNAE'),
        'zona_carta_id': fields.many2one('giscedata.polissa.carta.zona', 'Zona carta'),
        'envio_postal': fields.boolean('Cliente desea facturas por correo postal'),

        # especifics de contrato
        'contrato_name': fields.char('Número de contrato', size=240),
        'contacto_direccion': fields.many2one('res.partner.address', 'Dirección de contacto'),
        'cliente_direccion': fields.many2one('res.partner.address', 'Dirección del cliente'),
        'doc_requerida_nif': fields.boolean('NIF del titular del punto de suministro'),
        'doc_requerida_certificado_titularidad_cuenta': fields.boolean('Certificado de titularidad de la cuenta bancaria'),
        'doc_requerida_certificado_propiedad_o_alquiler': fields.boolean('Certificado de propiedad o contrato de alquiler de la localización del punto de suministro'),
        'doc_requerida_referencia_catastral': fields.boolean('Referencia catastral de la localización del punto de suministro'),
        'doc_requerida_tarjeta_cif_empresa': fields.boolean('Tarjeta CIF de la empresa'),
        'doc_requerida_autorizacion_contratacion_empresa': fields.boolean('Autorización de empresa para la contratación del suministro (en papel corporativo)'),
        'doc_requerida_cnae': fields.boolean('Código de la actividad que se realizará (CNAE)'),
        'doc_requerida_copia_acta_comunidad': fields.boolean('Copia del acta de la comunidad de propietarios aprobando la contratación'),
        'doc_requerida_autorizacion_poderes': fields.boolean('Autorización o copia de poderes por parte del titular para gestiones sobre el punto de suministro'),
        'doc_requerida_cedula_habitabilidad': fields.boolean('Cédula de habitabilidad o licencia de puesta en marcha de la actividad'),
        'doc_requerida_doc_tecnica': fields.boolean('Documentación técnica eléctrica en base a su tensión (BIE, APM, ELEC-6...etc)'),

        #especifics de l'oferta comparativa
        'comparativa_info': fields.text('info', readonly=True),
        'comparativa_data_inici': fields.date('Fecha de inicio'),
        'comparativa_data_fi': fields.date('Fecha final'),
        'comparativa_razon_social': fields.char('razón social', size=32, readonly=True),
        'comparativa_dni': fields.char('NIF/DNI', size=32, readonly=True),
        'comparativa_cups': fields.char('CUPS', size=32, readonly=True),
        'comparativa_direccion': fields.text('direcció', size=256, readonly=True),
        'comparativa_potencia_contratada': fields.float('potencia_contractada', readonly=True),
        'comparativa_poblacio': fields.char('poblacio', size=16, readonly=True),
        'comparativa_iva': fields.many2one('account.tax', 'IVA'),
        'comparativa_tarifa_acces': fields.char('tarifa accés', size=16, readonly=True),
        'comparativa_tarifa_comercialitzadora': fields.char('tarifa comercialitzadora', size=16, readonly=True),
        'comparativa_import_competencia': fields.float('import competencia'),
        'comparativa_import_lloguer': fields.float('import lloguer'),
        'comparativa_cost_reactiva': fields.float('cost reactiva'),
        'comparativa_import_otros': fields.float('altres'),
        'comparativa_impost_electric': fields.float('import elèctric', readonly=True),
        'comparativa_total_sense_iva': fields.float('total sense IVA', readonly=True),
        'comparativa_total_amb_iva': fields.float('total amb IVA', readonly=True),
        'comparativa_diferencia_preus': fields.float('diferencia de preus', readonly=True),
        'comparativa_oferta_consums_periode': fields.one2many('comparativa.consums.periode', 'crm_polissa', string='Consumos'),
        'comparativa_oferta_potencia_periode': fields.one2many('comparativa.potencia.periode', 'crm_polissa', string='Periodo'),
        'comparativa_maximetre': fields.boolean('Maxímetre'),
        'comparativa_observacions': fields.text('Observaciones')
    }

    _constraints = [(check_cups, u'El código del CUPS no es correcto.',
                     ['cups_name', 'cups_sips']),
                   ]

    _defaults = {
        'name': _default_numero_oferta,
        'channel_id': _default_canal,
        'type': 'simple',
        'state': lambda *a: 'esborrany',
        'section_id': _default_section_id,
        'stage_id': _default_stage_id,
        'campaign_id': _default_campaign_id,
        'sips_estado': lambda *a: 'no_encontrado',
        'link_catastro': lambda *a: 'https://www.sedecatastro.gob.es/',
        'subsistema_oferta': lambda *a: 'PENINSULA',
        'user_id': lambda self, cursor, uid, context=None: uid,
        'delegacion': _default_delegacion,
        'coeficient_k': lambda *a: 7.0,
        'coeficient_d': lambda *a: 0.0,
        'mode_facturacio': lambda *a: 'atr',
        'comparativa_iva': _default_IVA,
        'fact_potencia_100': lambda *a: False,
    }


CrmPolissa()


class CrmNote(osv.osv):

    _name = 'crm.note'
    _order = 'date_event desc'

    def default_get(self, cursor, uid, fields, context=None):
        if not context:
            context = {}
        result = super(CrmNote, self).default_get(
            cursor, uid, fields, context=context)
        if 'crm_polissa_id' in context:
            result.update({'crm_polissa': context.get('crm_polissa_id', False)})
        return result

    def get_resum(self, text):
        max_len = 128
        if len(text) > max_len:
            result = '%s ...' % (text[:(max_len-4)])
        else:
            result = text[:max_len]
        result = result.replace('\n', ' ')
        return result


    def _ff_get_resum(self, cursor, uid, ids, field_name, args, context=None):
        if not context:
            context = {}
        res = dict.fromkeys(ids, '')
        for note in self.read(cursor, uid, ids, ['ref', 'text_note'],
                              context=context):
            if note['ref']:
                res_model, res_id = note['ref'].split(',')
                res_id = int(res_id)
                model_obj = self.pool.get(res_model)
                subject = model_obj.get_subject(cursor, uid, [res_id])
                res[note['id']] = self.get_resum(subject[res_id])
            elif note['text_note']:
                res[note['id']] = self.get_resum(note['text_note'])
        return res



    def save_note(self, cursor, uid, ids, context=None ):
        return {'type': 'ir.actions.act_window_close'}

    def message_append(self, cursor, uid, crm_pol_ids, text=None, mail_id=None):
        res = []
        vals = {}
        if text:
            vals.update({'text_note': text})
        if mail_id:
            vals.update({'ref': 'poweremail.mailbox,%s' % mail_id})
        for crm_pol_id in crm_pol_ids:
            vals.update({'crm_polissa': crm_pol_id})
            res.append(self.create(cursor, uid, vals))
        return res


    _columns = {
        'name': fields.function(
            _ff_get_resum, type='char', string='Resum', method=True, size=128),
        'crm_polissa': fields.many2one(
            'crm.polissa', 'Oferta', required=True, ondelete='cascade'),
        'text_note': fields.text('Text nota'),
        'date_event': fields.datetime('Data event'),
        'ref': fields.reference(
            'Reference', selection=[('poweremail.mailbox', 'Email')], size=128),
    }

    _defaults = {
        'date_event': datetime.now()
    }

CrmNote()



class CrmPTPeriode(osv.osv):
    """Preus transferencia energia periode.
    El preu de transferència és el preu de cost de la energia"""
    _name = 'crm.pt.periode'

    def recalc_price(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        tp_obj = self.pool.get('giscedata.tp')
        for period in self.browse(cursor, uid, ids, context=context):
            periode_id = period.periode_id.id
            price = tp_obj.calc_price(cursor, uid, periode_id, context=context)
            period.write({'name': price})
        return True

    def on_change_periode_id(self, cursor, uid, ids, periode_id, context=None):
        if context is None:
            context = {}
        res = {'value': {}}
        tp_obj = self.pool.get('giscedata.tp')
        price = tp_obj.calc_price(cursor, uid, periode_id, context=context)
        res['value']['name'] = price
        return res

    _columns = {
        #'periode': fields.selection(PERIODES, 'Periodo', required=True),
        'periode': fields.related(
            'periode_id', 'name', type='selection', selection=PERIODES
        ),
        'periode_id': fields.many2one(
            'giscedata.polissa.tarifa.periodes', 'Periodo'
        ),
        'crm_polissa': fields.many2one(
            'crm.polissa', 'Oferta', required=True, ondelete='cascade'),
        'name': fields.float('Precio transferencia €/kWh', digits=(15, 6), required=True)
    }

CrmPTPeriode()


class CrmPotenciaContractadaPeriode(osv.osv):
    """Potències contractades per periode."""
    _name = 'crm.potencia.contractada.periode'
    _rec_name = 'periode'

    _columns = {
        'periode': fields.selection(PERIODES, 'Periodo', required=True),
        'crm_polissa': fields.many2one(
            'crm.polissa', 'Oferta', required=True, ondelete='cascade'),
        'potencia': fields.float('Potencia ofertada', digits=(15, 3),
                                 required=True),
        'preu': fields.float(u'Precio Término potencia €/kW año', digits=(15, 6)),
        'tipus': fields.selection(TIPUS_SELECTION, 'Tipo', required=True),
    }

CrmPotenciaContractadaPeriode()


class CrmEnergiaContractadaPeriode(osv.osv):
    """Potències contractades per periode."""
    _name = 'crm.energia.contractada.periode'
    _rec_name = 'periode'

    _columns = {
        'periode': fields.selection(PERIODES, 'Periodo', required=True),
        'crm_polissa': fields.many2one(
            'crm.polissa', 'Oferta', required=True, ondelete='cascade'),
        'preu': fields.float(
            u'Precio energía €/kWh', digits=(15, 6), required=True),
        'tipus': fields.selection(TIPUS_SELECTION, 'Tipo', required=True),
    }

CrmEnergiaContractadaPeriode()

class CrmConsumsPeriode(osv.osv):
    """Consum energia periode."""
    _name = 'crm.consums.periode'
    _rec_name = 'periode'

    _order = 'data_inici asc, periode asc'

    _columns = {
        'periode': fields.selection(PERIODES, 'Periodo', required=True),
        'crm_polissa': fields.many2one(
            'crm.polissa', 'Oferta', required=True, ondelete='cascade'),
        'data_inici': fields.date('Data inicio'),
        'data_final': fields.date('Data final'),
        'consum': fields.float('Consumo', digits=(15, 3), required=True),
        'tipus': fields.selection(TIPUS_SELECTION, 'Tipo', required=True),
    }

CrmConsumsPeriode()


class CrmConsumsPeriodeUIRel(osv.osv):
    _name = 'crm.consums.periode.ui.rel'
    _columns = {
        'data_inici': fields.date('Date', required=True),
        'crm_polissa': fields.many2one(
            'crm.polissa', 'CRM Polissa', required=True
        )
    }

CrmConsumsPeriodeUIRel()


class CrmConsumsPeriodeUI(osv.osv):
    _name = 'crm.consums.periode.ui'

    def create(self, cursor, uid, vals, context=None):
        crm_consums = self.pool.get('crm.consums.periode')
        rel = self.pool.get('crm.consums.periode.ui.rel')
        consums = {'tipus': 'client'}
        for k in ('crm_polissa', 'data_inici', 'data_final'):
            consums[k] = vals.pop(k, False)
        for p in PERIODES:
            periode = p[0].lower()
            consums['consum'] = vals.pop(periode, 0)
            consums['periode'] = p[0]
            crm_consums.create(cursor, uid, consums)
        return rel.create(cursor, uid, {
            'crm_polissa': consums['crm_polissa'],
            'data_inici': consums['data_inici']
        })

    def search(self, cursor, uid, args, offset=0, limit=None, order=None,
            context=None, count=False):
        rel_obj = self.pool.get('crm.consums.periode.ui.rel')
        order = 'data_inici desc'
        return rel_obj.search(cursor, uid, args, offset, limit, order,
                              context, count)

    def write(self, cursor, uid, ids, vals, context=None):
        crm_consums = self.pool.get('crm.consums.periode')
        rel_obj = self.pool.get('crm.consums.periode.ui.rel')
        for rel in rel_obj.read(cursor, uid, ids):
            search_params = [
                ('crm_polissa', '=', rel['crm_polissa'][0]),
                ('data_inici', '=', rel['data_inici'])
            ]
            c_ids = crm_consums.search(cursor, uid, search_params)
            if c_ids:
                crm_consums.unlink(cursor, uid, c_ids)
            vals['crm_polissa'] = rel['crm_polissa'][0]
            self.create(cursor, uid, vals)
        rel_obj.unlink(cursor, uid, ids)
        return True

    def read(self, cursor, uid, ids, fields=None, context=None,
             load='_classic_read'):
        crm_consums = self.pool.get('crm.consums.periode')
        rel_obj = self.pool.get('crm.consums.periode.ui.rel')
        res = []
        for rel in rel_obj.read(cursor, uid, ids):
            search_params = [
                ('crm_polissa', '=', rel['crm_polissa'][0]),
                ('data_inici', '=', rel['data_inici'])
            ]
            consum_ui = rel.copy()
            for consum in crm_consums.search_reader(cursor, uid, search_params):
                consum_ui[consum['periode'].lower()] = consum['consum']
                consum_ui['data_final'] = consum['data_final']
            res.append(consum_ui)
        return res

    def unlink(self, cursor, uid, ids, context=None):
        crm_consums = self.pool.get('crm.consums.periode')
        rel_obj = self.pool.get('crm.consums.periode.ui.rel')
        for rel in rel_obj.read(cursor, uid, ids):
            search_params = [
                ('crm_polissa', '=', rel['crm_polissa'][0]),
                ('data_inici', '=', rel['data_inici'])
            ]
            c_ids = crm_consums.search(cursor, uid, search_params)
            if c_ids:
                crm_consums.unlink(cursor, uid, c_ids)
        rel_obj.unlink(cursor, uid, ids)
        return True

    _columns = {
        'crm_polissa': fields.many2one(
            'crm.polissa', 'Oferta'
        ),
        'data_inici': fields.date('Data inicio', required=True),
        'data_final': fields.date('Data final', required=True),
        'p1': fields.float('P1'),
        'p2': fields.float('P2'),
        'p3': fields.float('P3'),
        'p4': fields.float('P4'),
        'p5': fields.float('P5'),
        'p6': fields.float('P6'),
    }

CrmConsumsPeriodeUI()


class CrmDocumentacio(osv.osv):
    """Potències contractades per periode."""
    _name = 'crm.documentacio'

    _order = 'data_creacio desc'

    EMAIL_TEMPLATES = {
        'oferta': {'template': 'email_crm_polissa_oferta',
                   'stage_xml_id': 'crm_oferta_enviada_stage'},
        'contrato': {'template': 'email_crm_polissa_contrato',
                     'stage_xml_id': 'crm_oferta_contrato_enviado_stage'},
        'comparativa': {
            'template': 'email_crm_polissa_comparativa', 'stage_xml_id': False}
    }

    def _ff_documents(self, cursor, uid, ids, field_name, arg,
                              context=None):
        if not context:
            context = {}

        res = dict.fromkeys(ids, False)
        crm_docs_obj = self.pool.get('crm.documentacio')
        attach_obj = self.pool.get('ir.attachment')
        for doc in crm_docs_obj.read(cursor, uid, ids, ['attach_id']):
            if doc['attach_id']:
                attach_id = doc['attach_id'][0]
                res[doc['id']] = attach_obj.read(
                    cursor, uid, attach_id, ['datas'])['datas']
        return res

    def unlink(self, cursor, uid, ids, context=None):
        '''Ampliem la funcionalitat del unlink
        per esborrar l'adjunt del document'''
        if not isinstance(ids, list):
            ids = [ids, ]
        else:
            ids = ids
        if not context:
            context = {}

        attach_obj = self.pool.get('ir.attachment')

        for document in self.browse(cursor, uid, ids, context=context):
            attach_id = document.attach_id.id
            super(CrmDocumentacio, self).unlink(
                cursor, uid, document.id, context=context)
            attach_obj.unlink(cursor, uid, [attach_id])

        return True

    def create(self, cursor, uid, vals, context=None):
        '''Ampliem la funcionalitat del create
        per crear el document dinamicament si tenim el upload_file.
        Només per la vista de creació de fitxers no generats.
        '''
        if not context:
            context = {}
        attach_obj = self.pool.get('ir.attachment')
        if 'upload_file' in vals:
            file_name = vals.get('file_name', '')
            attach_id = attach_obj.create(cursor, uid, {
             'datas': vals['upload_file'],
             'name': file_name,
             'datas_fname': file_name,
             'type': 'binary'})
            vals.update({'attach_id': attach_id})
            vals.pop('upload_file', None)
            vals.pop('file_name', None)

        res_id = super(CrmDocumentacio, self).create(cursor, uid, vals, context)
        return res_id

    def prepare_mail_notification(self, cursor, uid, document_id,
                                  crm_polissa_id):

        document = self.read(cursor, uid, document_id,
                             ['tipus_document', 'attach_id'])
        email_template = self.EMAIL_TEMPLATES.get(
            document['tipus_document'], False)
        if email_template and email_template.get('template', False):
            email_template = email_template.get('template', False)
            crm_polissa_obj = self.pool.get('crm.polissa')
            crm_polissa_obj.prepare_mail_notification(
                cursor, uid, crm_polissa_id, email_template,
                [document['attach_id'][0], ])

    def udpate_oferta_stage(self, cursor, uid, crm_polissa_id, stage_xml_id,
                            context=None):
        if not context:
            context = {}
        crm_obj = self.pool.get('crm.polissa')
        imd_obj = self.pool.get('ir.model.data')
        stage_id = imd_obj.get_object_reference(
            cursor, uid, 'crm_elec', stage_xml_id)[1]
        crm_obj.write(cursor, uid, crm_polissa_id, {'stage_id': stage_id})

    def get_template(self, cursor, uid, doc_id, context=None):
        if not context:
            context = {}
        fields = ['crm_polissa', 'tipus_document']
        doc = self.read(cursor, uid, doc_id, fields)
        template = self.EMAIL_TEMPLATES.get(doc['tipus_document'], False)
        if template:
            return template.get('template', False)
        return False

    def documentacio_enviada(self, cursor, uid, doc_id, doc_tipus_document,
                             crm_polissa_id):
        self.write(cursor, uid, doc_id, {'enviat_client': 'enviada'})
        template = self.EMAIL_TEMPLATES.get(doc_tipus_document, False)
        if template and template.get('stage_xml_id', False):
            self.udpate_oferta_stage(cursor, uid, crm_polissa_id,
                                     template['stage_xml_id'])

    def send_from_email(self, cursor, uid, ids, mail_id, context=None):
        if not context:
            context = {}
        fields = ['crm_polissa', 'tipus_document']
        for doc in self.read(cursor, uid, ids, fields):
            crm_polissa_id = doc['crm_polissa'][0]
            self.documentacio_enviada(
                cursor, uid, doc['id'], doc['tipus_document'], crm_polissa_id)
        return True

    def send_docs(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        fields = ['crm_polissa', 'tipus_document']
        for doc in self.read(cursor, uid, ids, fields):
            crm_polissa_id = doc['crm_polissa'][0]
            self.prepare_mail_notification(cursor, uid, doc['id'],
                                           crm_polissa_id)
            self.documentacio_enviada(
                cursor, uid, doc['id'], doc['tipus_document'], crm_polissa_id)
        return True

    _rec_name = 'file_name'

    _columns = {
        'attach_id': fields.many2one('ir.attachment', 'Adjunto',
                                     required=False, ondelete='restrict'),
        'attach_view': fields.function(
            _ff_documents, type='binary', method=True,
            string=u'Documento'),
        'crm_polissa': fields.many2one('crm.polissa', 'Oferta',
                                       required=True, ondelete='cascade',
                                       select=1),
        'tipus_document': fields.selection(TIPUS_DOCUMENT_SELECTION,
                                           'Tipo documento', required=True,
                                           select=1),
        'tipologia': fields.selection(TIPOLOGIA_SELECTION,
                                      'Tipologia documentos', required=True,
                                      select=1),
        'versio': fields.integer(u'Versión'),
        'comentari': fields.char(u'Comentario', size=240),
        'enviat_client': fields.selection(
            [('enviada', 'Enviado'),
             ('no_enviada', 'No enviado')],
            string=u'Enviado cliente'),
        'data_creacio': fields.datetime(string=u'Fecha creación'),
        'upload_file': fields.binary('Fichero'),
        'file_name': fields.char('nombre fichero', size=512)
    }

    _defaults = {
        'versio': lambda *a: 1,
        'enviat_client': lambda *a: 'no_enviada',
        'data_creacio': lambda *a: datetime.today().strftime('%Y-%m-%d %H:%M:%S')
    }

CrmDocumentacio()

class ComparativaConsumsPeriode(osv.osv):

    _name = 'comparativa.consums.periode'
    _order = 'data_inici asc, periode asc'

    _columns = {
        'periode': fields.selection(PERIODES, 'Período', required=True),
        'crm_polissa': fields.many2one(
            'crm.polissa', 'Oferta', required=True, ondelete='cascade'),
        'data_inici': fields.date('Data inicio', required=True),
        'data_final': fields.date('Data final', required=True),
        'consum': fields.float('Consumo (kw)', digits=(15, 2)),
        'preu': fields.float('Precio (€/kwh)', digits=(15, 6), readonly=True),
        'import': fields.float('Importe (€)', digits=(15, 2), readonly=True),
    }

ComparativaConsumsPeriode()


class ComparativaPotenciaPeriode(osv.osv):

    _name = 'comparativa.potencia.periode'
    _order = 'data_inici asc, periode asc'

    _columns = {
        'periode': fields.selection(PERIODES, 'Período', required=True),
        'crm_polissa': fields.many2one(
            'crm.polissa', 'Oferta', required=True, ondelete='cascade'),
        'data_inici': fields.date('Data inicio', required=True),
        'data_final': fields.date('Data final', required=True),
        'potencia': fields.float('Potencia', digits=(15, 3)),
        'preu': fields.float('Precio (€/kw año)', digits=(15, 6), readonly=True),
        'lectura': fields.float('Lectura maxímetro (kwh)', digits=(15, 3)),
        'import': fields.float('Importe (€)', digits=(15, 2), readonly=True),
    }

ComparativaPotenciaPeriode()

