# -*- coding: utf-8 -*-
from osv import osv


class GiscedataSwitching(osv.osv):
    _name = 'giscedata.switching'
    _inherit = 'giscedata.switching'

    def udpate_oferta_stage(self, cursor, uid, ids, stage_xml_id, context=None):
        if not context:
            context = {}
        super(GiscedataSwitching, self).udpate_oferta_stage(
            cursor, uid, ids, stage_xml_id, context)
        crm_obj = self.pool.get('crm.polissa')
        for sw in self.browse(cursor, uid, ids, context=context):
            oferta_id = crm_obj.search(cursor, uid, [
                ('polissa_id', '=', sw.cups_polissa_id.id)
            ])
            if stage_xml_id == 'crm_oferta_activado_atr_stage':
                crm_obj.prepare_mail_notification(
                    cursor, uid, oferta_id, 'email_crm_polissa_alta_suministro'
                )


GiscedataSwitching()