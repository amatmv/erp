# -*- coding: utf-8 -*-
from osv import osv, fields


class CRMLead(osv.osv):
    _name = 'crm.lead'
    _inherit = 'crm.lead'

    def _get_section(self, cursor, uid, context=None):
        """Gives section id for current User
        """
        section_obj = self.pool.get('crm.case.section')
        if not context:
            context = {}
        section_code = context.get('section_code', 'False')
        if section_code:
            section_id = section_obj.search(cursor, uid, [
                ('code', '=', section_code)
            ])
            if section_id:
                return section_id[0]
        return super(CRMLead, self)._get_section(cursor, uid, context)

    def _default_ref(self, cursor, uid, context=None):
        if not context:
            context = {}
        section_code = context.get('section_code', False)
        if section_code in ('ATC', 'COM'):
            return 'giscedata.polissa,'
        else:
            return False

    def _default_stage_id(self, cursor, uid, context=None):
        stage_obj = self.pool.get('crm.case.stage')
        if not context:
            context = {}
        section_code = context.get('section_code', False)
        stage_type = context.get('stage_type', False)
        if section_code == 'COM' and stage_type == 'opportunity':
            # TODO: Canviar el nom per buscar la referència que crei el data
            stage_id = stage_obj.search(cursor, uid, [
                ('section_ids.code', '=', 'COM'),
                ('name', '=', 'Interesado')
            ])
            if stage_id:
                return stage_id[0]
        return False

    def onchange_contrato(self, cursor, uid, ids, ref, context=None):
        if not context:
            context = {}
        polissa_obj = self.pool.get('giscedata.polissa')
        partner_obj = self.pool.get('res.partner')
        res = {'value': {
            'partner_id': False,
            'contact_name': False,
            'title': False,
            'street': False,
            'phone': False,
            'mobile': False,
            'email': False,
            'city': False,
            'state_id': False
        }}
        if ref:
            try:
                obj, obj_id = ref.split(',')
                obj_id = int(obj_id)
            except ValueError:
                return res
            dades = None
            if obj == 'giscedata.polissa':
                polissa = polissa_obj.browse(cursor, uid, obj_id,
                                             context=context)
                dades = polissa.titular
            elif obj == 'res.partner':
                dades = partner_obj.browse(cursor, uid, obj_id,
                                           context=context)
                res['value'].update({
                    'partner_id': dades.id,
                })
            if dades and dades.address:
                address = dades.address[0]
                res['value'].update({
                    'contact_name': address.name,
                    'title': address.title and address.title.id or False,
                    'street': address.street,
                    'phone': address.phone,
                    'mobile': address.mobile,
                    'email': address.email,
                    'city': address.city,
                    'state_id': (address.state_id and address.state_id.id
                                 or False)
                })
        return res

    def create(self, cursor, uid, vals, context=None):
        res_id = super(CRMLead, self).create(cursor, uid, vals, context)
        # Mirem si estem en ATC
        section_id = vals.get('section_id', False)
        section_obj = self.pool.get('crm.case.section')
        atc_id = section_obj.search(cursor, uid, [('code', '=', 'ATC')])[0]
        if atc_id == vals.get('section_id', False):
            self.write(cursor, uid, [res_id], {'name': 'ATC-%06d' % res_id})
        return res_id

    def _last_note(self, cursor, uid, ids, field_name, args, context=None):
        res = dict.fromkeys(ids, '')
        for crm in self.browse(cursor, uid, ids, context=context):
            if crm.message_ids:
                res[crm.id] = crm.message_ids[0].display_text
        return res

    def _stage_id_selection(self, cursor, uid, context=None):
        if not context:
            context = {}
        stage_obj = self.pool.get('crm.case.stage')
        section_code = context.get('section_code', False)
        search_params = []
        if section_code:
            search_params += [('section_ids.code', '=', section_code)]
        stage_ids = stage_obj.search(cursor, uid, search_params)
        res = [(x['id'], x['name']) for x in
                stage_obj.read(cursor, uid, stage_ids, context=context)]
        return res

    def _stage_id_for_section(self, cursor, uid, ids, field_name, arg,
                              context=None):
        return dict.fromkeys(ids, '')

    def _stage_id_for_section_search(self, cursor, uid, obj, name, args,
                                     context=None):
        crm_obj = self.pool.get('crm.lead')
        lead_ids = crm_obj.search(cursor, uid, [
            ('stage_id', args[0][1], args[0][2])
        ], context=context)
        if lead_ids:
            return [('id', 'in', lead_ids)]
        else:
            return [('id', '=', '0')]

    _columns = {
        'last_note': fields.function(
            _last_note,
            string='Último historial',
            type='text',
            method=True
        ),
        'stage_id_for_section': fields.function(
            _stage_id_for_section,
            type='selection',
            string='Etapa',
            fnct_search=_stage_id_for_section_search,
            selection=_stage_id_selection,
            method=True,
        ),
        'oferta_ids': fields.one2many(
            'crm.polissa',
            'case_id',
            'Ofertas'
        ),
    }

    _defaults = {
        'section_id': _get_section,
        'ref': _default_ref,
        'stage_id': _default_stage_id,
    }

CRMLead()