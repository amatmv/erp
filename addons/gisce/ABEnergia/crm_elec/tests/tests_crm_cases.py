# -*- coding: utf-8 -*-

from destral import testing
from destral.transaction import Transaction

from datetime import datetime, timedelta


class TestsCrmVncCases(testing.OOTestCase):

    def open_polissa(self, txn):
        cursor = txn.cursor
        uid = txn.user
        pool = self.openerp.pool
        polissa_obj = pool.get('giscedata.polissa')
        imd_obj = pool.get('ir.model.data')
        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]
        # Como usa poliza ab tenemos que modificar el consumo anual previsto
        # para poder activar la poliza sim que salte la restricción.

        polissa_obj.write(cursor, uid, polissa_id,
                          {'consumo_anual_previsto': 200}
                          )

        polissa_obj.send_signal(cursor, uid, [polissa_id], [
            'validar', 'contracte'
        ])

    def crear_modcon(self, txn, potencia, ini, fi, tariff_id=None):
        cursor = txn.cursor
        uid = txn.user
        pool = self.openerp.pool
        polissa_obj = pool.get('giscedata.polissa')
        imd_obj = pool.get('ir.model.data')
        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]
        pol = polissa_obj.browse(cursor, uid, polissa_id)
        pol.send_signal(['modcontractual'])
        polissa_obj.write(cursor, uid, polissa_id, {'potencia': potencia})

        if tariff_id:
            polissa_obj.write(cursor, uid, polissa_id, {'tarifa': tariff_id})

        wz_crear_mc_obj = pool.get('giscedata.polissa.crear.contracte')

        ctx = {'active_id': polissa_id}
        params = {'duracio': 'nou'}

        wz_id_mod = wz_crear_mc_obj.create(cursor, uid, params, ctx)
        wiz_mod = wz_crear_mc_obj.browse(cursor, uid, wz_id_mod, ctx)
        res = wz_crear_mc_obj.onchange_duracio(
            cursor, uid, [wz_id_mod], wiz_mod.data_inici, wiz_mod.duracio,
            ctx
        )
        wiz_mod.write({
            'data_inici': ini,
            'data_final': fi
        })
        wiz_mod.action_crear_contracte(ctx)
        return polissa_id

    def test_create_close_no_generate_new_case_crm_vnc_cases(self):
        '''
        Step 1: open polissa and made modcontract OK
        Step 2: call create vnc cases methoth and check if case is created OK
        Step 3: call close cases method, don't close the new case OK
        Step 4: call another time the method -> doesn't generate new case NOT OK
        Step 5: make a new modcontract, now when create vcn cases is called cas
        should be closed OK
        '''

        wiz_obj = self.openerp.pool.get('crear.crm.vnc')
        case_obj = self.openerp.pool.get('crm.case')
        case_vnc_obj = self.openerp.pool.get('crear.crm.vnc')
        tarif_obj = self.openerp.pool.get('giscedata.polissa.tarifa')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            self.open_polissa(txn)
            delay_config_var = wiz_obj.get_delay_config_var(cursor, uid, {})
            current_date = datetime.now().strftime('%Y-%m-%d')
            current_date = datetime.strptime(current_date, '%Y-%m-%d')
            # TODO date problems, remove hours, min, sec
            final_date = current_date + timedelta(
                days=delay_config_var
            )

            tarifa_id = tarif_obj.search(
                cursor, uid, [('name', '=', '2.1A')]
            )[0]

            expected_pol_id = self.crear_modcon(
                txn, 5500, current_date.strftime('%Y-%m-%d'),
                final_date.strftime('%Y-%m-%d'), tarifa_id
            )

            pol_id = wiz_obj.get_contratos_renovar(cursor, uid)
            self.assertIn(expected_pol_id, pol_id)
            open_cases = wiz_obj.crear_casos_vnc(cursor, uid)
            self.assertTrue(open_cases)

            case = case_obj.read(
                cursor, uid, open_cases[0], ['id', 'section_id']
            )
            self.assertTrue(case)
            self.assertEqual(open_cases[0], case['id'])
            self.assertEqual(
                case['section_id'][0], case_vnc_obj.get_vnc_section_id(
                    cursor, uid
                )
            )

            closed_cases = wiz_obj.cerrar_casos_vnc(cursor, uid)
            self.assertFalse(closed_cases)

            open_cases = wiz_obj.crear_casos_vnc(cursor, uid)
            self.assertFalse(open_cases)

            final_date = final_date + timedelta(
                days=20
            )
            expected_pol_id = self.crear_modcon(txn, 6000,
                                                current_date.strftime(
                                                    '%Y-%m-%d'),
                                                final_date.strftime('%Y-%m-%d'))
            closed_cases = wiz_obj.cerrar_casos_vnc(cursor, uid)
            self.assertIn(case['id'], closed_cases)
