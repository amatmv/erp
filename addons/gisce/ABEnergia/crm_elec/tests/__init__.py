# -*- coding: utf-8 -*-
import unittest
from destral import testing
from destral.transaction import Transaction
from expects import *
from tests_crm_cases import *


class CRMPolissaTests(testing.OOTestCase):
    @unittest.skip(reason="crm polissa attributes modiffied")
    def test_domicili_fiscal(self):
        crm_polissa_obj = self.openerp.pool.get('crm.polissa')
        country_obj = self.openerp.pool.get('res.country')
        state_obj = self.openerp.pool.get('res.country.state')
        mun_obj = self.openerp.pool.get('res.municipi')
        pob_obj = self.openerp.pool.get('res.poblacio')
        tipovia_obj = self.openerp.pool.get('res.tipovia')
        wiz_export_obj = self.openerp.pool.get('wizard.export.contrato')
        partner_obj = self.openerp.pool.get('res.partner')
        par_adr_obj = self.openerp.pool.get('res.partner.address')
        with Transaction().start(self.database) as txn:
            country_id = country_obj.search(txn.cursor, txn.user,
                                            [('code', '=', 'ES')])[0]
            state_id = state_obj.search(txn.cursor, txn.user,
                                        [('country_id', '=', country_id)])[0]
            municipi_id = mun_obj.search(
                txn.cursor, txn.user, [('state', '=', state_id)])[0]
            municipi = mun_obj.read(txn.cursor, txn.user, municipi_id, ['name'])

            pob_id = pob_obj.create(txn.cursor, txn.user, {
                'name': municipi['name'],
                'municipi_id': municipi_id
            })
            tipovia_id = tipovia_obj.create(txn.cursor, txn.user, {
                'codi': 'C',
                'name': 'Carrer'
            })
            tipovia = tipovia_obj.read(txn.cursor, txn.user, tipovia_id, [])

            # Create a new
            oferta_id = crm_polissa_obj.create(txn.cursor, txn.user, {
                'cliente_nombre': 'John',
                'cliente_apellidos': 'Smith',
                'cliente_vat': '12291804Y',
                })

            oferta = crm_polissa_obj.read(txn.cursor, txn.user, oferta_id)

            wiz_export_obj.create_or_update_partner(txn.cursor, txn.user,
                                                    oferta_id)
            partner_id = partner_obj.search(txn.cursor, txn.user, [
                ('name', '=', 'Smith, John'),
                ('vat', '=', 'ES12291804Y')
            ])
            expect(partner_id).to(have_len(1))
            partner = partner_obj.read(txn.cursor, txn.user, partner_id[0], [])
            expect(partner).to(have_keys(
                name='Smith, John',
                vat='ES12291804Y',
            ))
            adr_id = par_adr_obj.search(txn.cursor, txn.user, [
                ('partner_id', '=', partner_id[0]),
                ('type', '=', 'contact')
            ])
            expect(adr_id).to(have_len(1))
            adr = par_adr_obj.read(txn.cursor, txn.user, adr_id, [])[0]
            expect(adr).to(have_keys(name='Smith, John'))
            expect(adr['country_id']).to(contain(country_id))
            expect(adr['state_id']).to(contain(state_id))
            expect(adr['id_poblacio']).to(contain(pob_id))
            expect(adr['id_municipi']).to(contain(municipi_id))
            expect(adr['street']).to(contain(
                u'%s Street Street2 numero esc piso puerta' % tipovia['codi']))
            adr_id = par_adr_obj.search(txn.cursor, txn.user, [
                ('partner_id', '=', partner_id[0]),
                ('type', '=', 'invoice')
            ])
            expect(adr_id).to(have_len(1))
            adr = par_adr_obj.read(txn.cursor, txn.user, adr_id, [])[0]
            expect(adr).to(have_keys(name='Smith, John'))
            expect(adr['country_id']).to(contain(country_id))
            expect(adr['state_id']).to(contain(state_id))
            expect(adr['id_poblacio']).to(contain(pob_id))
            expect(adr['id_municipi']).to(contain(municipi_id))
            expect(adr['street']).to(contain(
                u'%s Street Street2 numero esc piso puerta' % tipovia['codi']))

    def test_consums_ui(self):
        crm_polissa_obj = self.openerp.pool.get('crm.polissa')
        consums_p_obj = self.openerp.pool.get('crm.consums.periode')
        consums_p_ui_obj = self.openerp.pool.get('crm.consums.periode.ui')
        with Transaction().start(self.database) as txn:
            crm_polissa_id = crm_polissa_obj.create(txn.cursor, txn.user, {
                'client_consums_periode_ui': [(0, 0, {
                    'p1': 1,
                    'p2': 2,
                    'p3': 3,
                    'p4': 4,
                    'p5': 5,
                    'p6': 6,
                    'data_inici': '2015-01-01',
                    'data_final': '2015-01-31'
                })]
            })
            c_ids = consums_p_obj.search(txn.cursor, txn.user, [
                ('crm_polissa', '=', crm_polissa_id)
            ])
            expect(c_ids).to(have_len(6))
