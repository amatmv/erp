# -*- coding: utf-8 -*-
{
    "name": "Llista de preus ofertable",
    "description": """
    This module provide :
      * Indicar si la llista de preus és ofertable o no
    """,
    "version": "0-dev",
    "author": "Abenergia",
    "category": "CRM",
    "depends":[
        "base",
        "product"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "pricelist_view.xml"
    ],
    "active": False,
    "installable": True
}
