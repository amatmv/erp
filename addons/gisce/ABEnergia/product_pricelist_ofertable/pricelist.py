# -*- coding: utf-8 -*-

from osv import osv, fields


class ProductPricelist(osv.osv):
    """Afegim l'opció de marcar la llista com a ofertable o no.
    """
    _name = 'product.pricelist'
    _inherit = 'product.pricelist'

    _columns = {
        'ofertable': fields.boolean('Ofertable'),
    }

    _defaults = {
        'ofertable': lambda *a: 0,
    }

ProductPricelist()
