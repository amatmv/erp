# -*- coding: utf-8 -*-
{
    "name": "Marge factura",
    "description": """Script per obtenir el marge de facturació sense impostos sobre energia i potencia de les factures entre un rang de dates podent filtrar per comercial i/o delegacio""",
    "version": "0-dev",
    "author": "Abenergia",
    "category": "",
    "depends":[
        "giscedata_polissa",
        "giscedata_facturacio_comer",
        "giscedata_polissa_custom_ab",
        "giscedata_polissa_responsable"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
