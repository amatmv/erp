# -*- coding: utf-8 -*-

import csv
from datetime import datetime

from osv import osv, fields


class MargeFacturacio(osv.osv):

    _name = 'marge.facturacio'

    def calcula_margen(self, cr, uid, delegacion, comercial,
               datainici, datafinal, outfile, resumfile,
               context=None):

        if context == None:
            context = {}

        # seleccionar les factures que pertanyin a les polisses de X delegacio
        # o bé filtrar per comercial
        pl_obj = self.pool.get('product.pricelist')
        pol_obj = self.pool.get('giscedata.polissa')
        fra_obj = self.pool.get('giscedata.facturacio.factura')
        lin_obj = self.pool.get('giscedata.facturacio.factura.linia')

        search_pol = []
        if delegacion:
            search_pol.extend([('delegacion', 'in', delegacion)])
        if comercial:
            search_pol.extend([('user_id', 'in', comercial)])
        pol_ids = pol_obj.search(cr, uid, search_pol,
                                 context={'active_test': False})

        search_inv = [('type', '=', 'out_invoice'),
                      ('date_invoice', '>=', datainici),
                      ('date_invoice', '<=', datafinal),
                      ('polissa_id', 'in', pol_ids),
                      ('state', 'in', ['open', 'paid'])
                     ]

        ids_factures = fra_obj.search(cr, uid, search_inv)

        # busquem la llista de preus de cost
        pl_id = pl_obj.search(cr, uid,
                              [('name', '=', 'TARIFAS ELECTRICIDAD')])[0]

        # # necessari la data pel price_get
        if 'date' not in context:
            context['date'] = datetime.today()

        diferencias = {}
        consums = {}

        with open(outfile, "w") as output, open(resumfile, "w") as resum:
            outwriter = csv.writer(output,
                                   delimiter=';',
                                   )
            resumwriter = csv.writer(resum,
                                     delimiter=';',
                                     )
            outwriter.writerow(['numfactura',
                                'descripcio',
                                'orig_priceunit',
                                'orig_pricesubtotal',
                                'quantitat',
                                'data_desde',
                                'data_fins',
                                'priceunitcoste',
                                'pricesubtotalcoste',
                                'diferencia',
                                ])
            resumwriter.writerow(['cups',
                                  'contrato',
                                  'comercial',
                                  'delegacion',
                                  'lista_precios',
                                  'numero_factura',
                                  'consumo',
                                  'fecha_factura',
                                  'fecha_inicio',
                                  'fecha_final',
                                  'diferencia',
                                  ])

            for id_fra in ids_factures:

                fra = fra_obj.read(cr, uid, id_fra)
                numfra = fra['number']
                datafra = fra['date_invoice']
                datainicial = fra['data_inici']
                datafinal = fra['data_final']
                polissa_id = fra['polissa_id'][0]

                polissa = pol_obj.read(cr, uid, polissa_id,
                                       ['name', 'user_id', 'delegacion',
                                        'cups', 'llista_preu'])
                contrato = polissa['name']
                ccial = polissa['user_id'][1]
                deleg = polissa['delegacion'][1]
                cups = polissa['cups'][1]
                llista = fra['llista_preu'][1]

                search_lin = [('factura_id', '=', id_fra)]
                ids_linia_factura = lin_obj.search(cr, uid, search_lin)

                factor = 1
                if fra['type'] == 'out_refund':
                    factor = -1

                for id_linia in ids_linia_factura:

                    linia = lin_obj.read(cr, uid, id_linia)

                    if linia['tipus'] not in ('energia', 'potencia'):
                        # el lloguer  sortirà diferent però no és marge
                        # reactiva és cost i els drets són cost, els 6e per
                        # impagament és costos
                        continue

                    # obtenir el producte, price_get en la data de factura
                    # amb la tarifa base i treure la diferència

                    orig_pricesubtotal = linia['price_subtotal'] * factor
                    orig_priceunit = linia['price_unit'] * factor
                    qtat = linia['quantity']
                    descripcio = '{0} - {1}'.format(linia['tipus'],
                                                    linia['name'])

                    context = {'date': datafra}

                    prod_id = linia['product_id'][0]
                    uos_id = linia['uos_id'][0]
                    context['uom'] = uos_id

                    puc = (pl_obj.price_get(cr, uid, [pl_id], prod_id, 1,
                                            context=context))[pl_id] * factor
                    pricesubtotal = puc * qtat * linia['multi'] * factor
                    dif = orig_pricesubtotal - pricesubtotal

                    if linia['price_subtotal'] < 0:
                        # linia de descompte, el descompte es la diferencia
                        dif = orig_pricesubtotal


                    outwriter.writerow([numfra,
                                        descripcio,
                                        orig_priceunit,
                                        orig_pricesubtotal,
                                        qtat,
                                        linia['data_desde'],
                                        linia['data_fins'],
                                        puc,
                                        pricesubtotal,
                                        dif,
                                        ])

                    diferencias[numfra] = diferencias.get(numfra, 0) + dif
                    if linia['tipus'] == 'energia':
                        consums[numfra] = qtat
                if numfra in consums and numfra in diferencias:
                    resumwriter.writerow([cups,
                                          contrato,
                                          ccial,
                                          deleg,
                                          llista,
                                          numfra,
                                          consums[numfra],
                                          datafra,
                                          datainicial,
                                          datafinal,
                                          diferencias[numfra],
                                          ])

            output.close()
            resum.close()

MargeFacturacio()
