# -*- coding: utf-8 -*-
{
    "name": "Ordres de treball AB Energia",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Reports",
    "depends":[
        "c2c_webkit_report",
        "giscedata_polissa_crm",
        "giscedata_switching_distri"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_ot_abenergia_report.xml",
        "giscedata_ot_abenergia_data.xml"
    ],
    "active": False,
    "installable": True
}
