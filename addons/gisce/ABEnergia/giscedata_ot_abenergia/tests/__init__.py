# -*- coding: utf-8 -*-
from destral.transaction import Transaction
from addons import get_module_resource
from giscedata_switching.tests.common_tests import TestSwitchingImport
from giscedata_switching_distri.tests import TEST_NULLIFIER_OT_ATR_CONNECTION


class BaseTest(TestSwitchingImport):

    def test_proces_ot_atr_connection_nullifier_ots(self):
        sw_obj = self.openerp.pool.get('giscedata.switching')
        case_obj = self.openerp.pool.get('crm.case')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            from giscedata_switching_distri.crm import OT_ATR_CONNECTION
            OT_ATR_CONNECTION.update(TEST_NULLIFIER_OT_ATR_CONNECTION)

            res_link_obj = self.openerp.pool.get("res.request.link")
            res_link_obj.create(cursor, uid, {
                'name': "Meter", "object": "giscedata.lectures.comptador"
            })

            a306_xml_path = get_module_resource(
                'giscedata_switching', 'tests', 'fixtures', 'a306_new.xml'
            )
            with open(a306_xml_path, 'r') as f:
                a306_xml = f.read()

            self.switch(txn, 'comer', other_id_name='res_partner_asus')
            uid = txn.user
            cursor = txn.cursor

            # create 01
            contract_id = self.get_contract_id(txn)
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)
            self.assignar_factura_potencia(txn, 'icp')

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'A3', '01'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.a3.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            a301 = step_obj.browse(cursor, uid, step_id)
            a3 = sw_obj.browse(cursor, uid, a301.sw_id.id)

            # This will create an OT with OTA section
            sw_obj.activa_polissa_cas_atr(cursor, uid, a3.id)
            case_ids = case_obj.search(
                cursor, uid,
                [('ref', '=', 'giscedata.switching,{0}'.format(a3.id))]
            )
            self.assertEqual(len(case_ids), 1)
            case = case_obj.browse(cursor, uid, case_ids[0])
            self.assertEqual(case.section_id.code, "OTA")

            # Now we create a nulifier OT for OTA: ANOTA
            # In order to do it, we will import a A3-06 and then activate
            # changes
            self.switch(txn, 'distri', other_id_name='res_partner_asus')
            a306_xml = a306_xml.replace(
                "<CodigoDeSolicitud>201607211259",
                "<CodigoDeSolicitud>{0}".format(a3.codi_sollicitud)
            )
            sw_obj.importar_xml(cursor, uid, a306_xml, 'a306.xml')
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'A3'),
                ('step_id.name', '=', '06'),
                ('codi_sollicitud', '=', a3.codi_sollicitud)
            ])
            self.assertEqual(len(res), 1)
            a3 = sw_obj.browse(cursor, uid, res[0])
            sw_obj.activa_polissa_cas_atr(cursor, uid, a3.id)
            case_ids = case_obj.search(
                cursor, uid,
                [
                    ('ref', '=', 'giscedata.switching,{0}'.format(a3.id)),
                    ('section_id.code', '=', 'ANOTA')
                ]
            )
            self.assertEqual(len(case_ids), 1)
            null_case = case_obj.browse(cursor, uid, case_ids[0])

            # Now we close the Nullifier OT. This will pass the ATR to pending,
            # create a A3-07, cancel the original OT (OTA) ignoring
            # automatization (we won't create a 02-rej step) and historize
            # a message on it
            null_case.case_close()
            case = case.browse()[0]
            null_case = null_case.browse()[0]
            a3 = a3.browse()[0]
            self.assertEqual(case.state, "cancel")
            self.assertEqual(case.email_last, "Solicitud anulada")
            self.assertEqual(a3.state, "pending")
            self.assertEqual(a3.step_id.name, "07")
            self.assertEqual(null_case.state, "done")

    def test_proces_ot_atr_connection_nullifier_ots_ignore_cn10(self):
        sw_obj = self.openerp.pool.get('giscedata.switching')
        case_obj = self.openerp.pool.get('crm.case')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            from giscedata_switching_distri.crm import OT_ATR_CONNECTION
            OT_ATR_CONNECTION.update(TEST_NULLIFIER_OT_ATR_CONNECTION)

            res_link_obj = self.openerp.pool.get("res.request.link")
            res_link_obj.create(cursor, uid, {
                'name': "Meter", "object": "giscedata.lectures.comptador"
            })

            c1_xml_path = get_module_resource(
                'giscedata_switching', 'tests', 'fixtures', 'c101_new.xml'
            )
            with open(c1_xml_path, 'r') as f:
                c1_xml = f.read()
            c108_xml_path = get_module_resource(
                'giscedata_switching', 'tests', 'fixtures', 'c108_new.xml'
            )
            with open(c108_xml_path, 'r') as f:
                c108_xml = f.read()

            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)
            sw_obj.importar_xml(
                cursor, uid, c1_xml, 'c101.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C1'),
                ('step_id.name', '=', '01'),
                ('codi_sollicitud', '=', '201607211259')
            ])
            self.assertEqual(len(res), 1)
            c1 = sw_obj.browse(cursor, uid, res[0])

            # This will create an OT with ICML section
            sw_obj.activa_polissa_cas_atr(cursor, uid, c1.id)
            case_ids = case_obj.search(
                cursor, uid, [('ref', '=', 'giscedata.switching,{0}'.format(c1.id))]
            )
            self.assertEqual(len(case_ids), 1)
            case = case_obj.browse(cursor, uid, case_ids[0])
            self.assertEqual(case.section_id.code, "ICML")

            # Now we create a nulifier OT for ICML: ANICML
            # In order to do it, we will import a C1-08 and then activate
            # changes
            c108_xml = c108_xml.replace(
                "<CodigoDeSolicitud>201607211259",
                "<CodigoDeSolicitud>{0}".format(c1.codi_sollicitud)
            )
            sw_obj.importar_xml(cursor, uid, c108_xml, 'c108.xml')
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C1'),
                ('step_id.name', '=', '08'),
                ('codi_sollicitud', '=', c1.codi_sollicitud)
            ])
            self.assertEqual(len(res), 1)
            c1 = sw_obj.browse(cursor, uid, res[0])
            sw_obj.activa_polissa_cas_atr(cursor, uid, c1.id)
            case_ids = case_obj.search(
                cursor, uid,
                [
                    ('ref', '=', 'giscedata.switching,{0}'.format(c1.id)),
                    ('section_id.code', '=', 'ANICML')
                ]
            )
            self.assertEqual(len(case_ids), 1)
            null_case = case_obj.browse(cursor, uid, case_ids[0])

            # Now we close the Nullifier OT. This will pass the ATR to pending,
            # create a Cn-09 and don't create Cn-10 because original OT is in
            # draft, cancel the original OT (ICML) ignoring automatization
            # (we won't create a 02-rej step) and historize a message on it
            null_case.case_close()
            case = case.browse()[0]
            null_case = null_case.browse()[0]
            c1 = c1.browse()[0]
            self.assertEqual(case.state, "cancel")
            self.assertEqual(case.email_last, "Solicitud anulada")
            self.assertEqual(c1.state, "pending")
            self.assertEqual(c1.step_id.name, "09")
            self.assertEqual(null_case.state, "done")