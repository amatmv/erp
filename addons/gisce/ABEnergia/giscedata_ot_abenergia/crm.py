# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _
from giscedata_switching_distri.crm import OT_ATR_CONNECTION

ABE_OT_ATR_CONNECTION = {
    'open': [
        {
            'ot_old_state': '',  # empty old state means any old state
            'ot_sections': ['AR', 'OTA', 'OTB', 'OTF', 'ICML'],
            'related_ot_states': [],
            'required_steps': {'default': []},
            'atr_steps': {'default': ['02']},
            'atr_state': 'open',
        },
        {
            'ot_old_state': '',
            'ot_sections': ['ICML'],
            'related_ot_states': [],
            'required_steps': {'default': []},
            'atr_steps': {'default': ['11']},
            'atr_state': 'open',
        },
        {
            'ot_old_state': '',
            'ot_sections': ['ANAR', 'ANICML', 'ANRIAM', 'ANOTA', 'ANRCAM', 'ANOTB', 'ANOTT', 'ANOTF', 'OTR'],
            'related_ot_states': [],
            'required_steps': {'default': []},
            'atr_steps': {'default': []},
            'atr_state': 'open',
        }
    ],
    'pending': [
        {
            'ot_old_state': '',
            'ot_sections': ['AR', 'OTA', 'ICML'],
            'related_ot_states': [],
            'required_steps': {'default': []},
            'atr_steps': {'default': ['03']},
            'atr_state': 'pending',
        },
        {
            'ot_old_state': '',
            'ot_sections': ['OTF', 'OTB', 'OTC'],
            'related_ot_states': [],
            'required_steps': {'default': []},
            'atr_steps': {'default': ['06']},
            'atr_state': 'pending',
        },
        {
            'ot_old_state': '',
            'ot_sections': ['ANAR', 'ANICML', 'ANRIAM', 'ANOTA', 'ANRCAM', 'ANOTB', 'ANOTT', 'ANOTF', 'OTR'],
            'related_ot_states': [],
            'required_steps': {'default': []},
            'atr_steps': {'default': []},
            'atr_state': 'pending',
        }
    ],
    'done': [
        {
            'ot_old_state': '',
            'ot_sections': ['OTC', 'OTL', 'OTT'],
            'related_ot_states': [],
            'required_steps': {'default': []},
            'atr_steps': {'default': ['02']},
            'atr_state': 'pending',
        },
        {
            'ot_old_state': '',
            'ot_sections': ['AR', 'OTA', 'OTB', 'OTF', 'ICML'],
            'related_ot_states': [],
            'required_steps': {'default': ['02']},
            'atr_steps': {'default': ['05']},
            'atr_state': 'pending',
        },
        {
            'ot_old_state': '',
            'ot_sections': ['ICML'],
            'required_steps': {'default': ['02']},
            'related_ot_states': [],
            'atr_steps': {'default': ['06']},
            'atr_state': 'pending',
        },
        {
            'ot_old_state': '',
            'ot_sections': ['ANAR', 'ANRIAM', 'ANOTA', 'ANRCAM', 'ANOTB', 'ANOTT', 'ANOTF'],
            'related_ot_states': [],
            'required_steps': {'default': []},
            'atr_steps': {'default': [], 'C1': ['09', '10'], 'C2': ['09', '10'], 'A3': ['07'], 'M1': ['07'], 'B1': ['04']},
            'atr_state': 'pending',
        },
        {
            'ot_old_state': '',
            'ot_sections': ['ANICML'],
            'related_ot_states': ['open', 'pending', 'done', 'cancel'],
            'required_steps': {'default': []},
            'atr_steps': {'default': [], 'C1': ['09', '10'], 'C2': ['09', '10']},
            'atr_state': 'pending',
        },
        {
            'ot_old_state': '',
            'ot_sections': ['ANICML'],
            'related_ot_states': ['draft'],
            'required_steps': {'default': []},
            'atr_steps': {'default': [], 'C1': ['09'], 'C2': ['09']},
            'atr_state': 'pending',
        },
        {
            'ot_old_state': '',
            'ot_sections': ['OTR'],
            'related_ot_states': [],
            'required_steps': {'default': []},
            'atr_steps': {'default': [], 'B1': ['04']},
            'atr_state': '',
        },
    ],
    'cancel': [
        {
            'ot_old_state': 'draft',
            'ot_sections': [],
            'related_ot_states': [],
            'required_steps': {'default': []},
            'atr_steps': {'default': ['02_rej']},
            'atr_state': 'pending',
        },
        {
            'ot_old_state': 'pending',
            'ot_sections': ['OTF', 'OTB', 'OTC'],
            'related_ot_states': [],
            'required_steps': {'default': []},
            'atr_steps': {'default': ['07']},
            'atr_state': 'pending',
        },
        {
            'ot_old_state': 'pending',
            'ot_sections': ['AR', 'OTA', 'ICML'],
            'related_ot_states': [],
            'required_steps': {'default': []},
            'atr_steps': {'default': ['04_rej']},
            'atr_state': 'pending',
        },
        {
            'ot_old_state': 'pending',
            'ot_sections': ['ICML'],
            'related_ot_states': [],
            'required_steps': {'default': []},
            'atr_steps': {'default': ['12']},
            'atr_state': 'pending',
        },
        {
            'ot_old_state': 'open',
            'ot_sections': ['OTF', 'OTB', 'OTC'],
            'related_ot_states': [],
            'required_steps': {'default': []},
            'atr_steps': {'default': ['07']},
            'atr_state': 'pending',
        },
        {
            'ot_old_state': 'open',
            'ot_sections': ['AR', 'OTA', 'ICML'],
            'related_ot_states': [],
            'required_steps': {'default': []},
            'atr_steps': {'default': ['04_rej']},
            'atr_state': 'pending',
        },
        {
            'ot_old_state': 'open',
            'ot_sections': ['ICML'],
            'related_ot_states': [],
            'required_steps': {'default': []},
            'atr_steps': {'default': ['12']},
            'atr_state': 'pending',
        },
        {
            'ot_old_state': '',
            'ot_sections': ['ANAR', 'ANICML', 'ANRIAM', 'ANOTA', 'ANRCAM', 'ANOTB', 'ANOTT', 'ANOTF'],
            'related_ot_states': [],
            'required_steps': {'default': []},
            'atr_steps': {'default': [''], 'C1': ['09_rej-42'], 'C2': ['09_rej-42'], 'A3': ['07_rej-42'], 'M1': ['07_rej-42'], 'B1': ['04_rej-42']},
            'atr_state': 'draft',
        },
        {
            'ot_old_state': '',
            'ot_sections': ['OTR'],
            'related_ot_states': [],
            'required_steps': {'default': []},
            'atr_steps': {'default': [''], 'C1': ['09_rej-43'], 'C2': ['09_rej-43'], 'A3': ['07_rej-43'], 'M1': ['07_rej-43'], 'B1': ['04_rej-43']},
            'atr_state': '',
        },
    ]
}

OT_ATR_CONNECTION.update(ABE_OT_ATR_CONNECTION)


class crm_case_section(osv.osv):
    _name = 'crm.case.section'
    _inherit = 'crm.case.section'

    _columns = {
        'report_id': fields.many2one(
            'ir.actions.report.xml', 'Report ID', required=False)
    }

    _defaults = {
        'report_id': lambda * a: False
    }

crm_case_section()

class GiscedataSwitchingCrmAb(osv.osv):
    _name = 'crm.case'
    _inherit = 'crm.case'

    SAME_CUPS_TEXT = _(u"Existencia de OT en curso para el mismo CUPS en las secciones:")

    def create_case_generic(self, cursor, uid, model_ids, context=None,
                                  description='Generic Case',
                                  section='CASE',
                                  category=None,
                                  assign=None,
                                  extra_vals=None):

        case_ids = super(GiscedataSwitchingCrmAb, self).create_case_generic(
                                  cursor, uid, model_ids, context,
                                  description, section,
                                  category, assign, extra_vals)

        if not context:
            context = {}
        model = context.get('model', False)

        if model == 'giscedata.switching':
            sw_obj = self.pool.get('giscedata.switching')
            cups_obj = self.pool.get('giscedata.cups.ps')
            sect_obj = self.pool.get('crm.case.section')
            for case in self.browse(cursor, uid, case_ids, context):
                ref_spl = case.ref.split(',')
                if len(ref_spl) == 2:
                    sw_id = int(ref_spl[1])
                    sw = sw_obj.read(cursor, uid, sw_id,
                                     ['cups_id', 'titular_polissa'])

                    # afegim el CUPS i adreça de subministrament a la descripcio
                    cups_id = sw['cups_id'][0]
                    cupsr = cups_obj.read(cursor, uid, [cups_id],
                                          ['name', 'direccio'])
                    cups = cupsr[0]['name']
                    adr = cupsr[0]['direccio']
                    desc = case.name
                    vals = {'name': '{0}: {1}, {2}'.format(desc, cups, adr)}

                    # afegim el titular de la polissa relacionada a la OT
                    empresa_id = sw['titular_polissa'][0]
                    vals.update({'partner_id': empresa_id})
                    case.write(vals)

                    # comprovar altres OTs amb mateix CUPS
                    ot_ids = self.get_ots_cups_coincident(cursor, uid, cups_id, case.id)
                    if ot_ids:
                        ot_ids = [case.id] + ot_ids
                        # Get the sections
                        sec_inf = self.read(cursor, uid, ot_ids, ['section_id'])
                        sec_ids = [s['section_id'][0] for s in sec_inf]
                        sections = sect_obj.read(cursor, uid, sec_ids, ['code'])
                        sec_msg = ""
                        for sec in sections:
                            sec_msg = "{0}, {1}".format(sec_msg, sec['code'])
                        sec_msg = sec_msg[1:]
                        # Historize current description of cases
                        self.case_log(cursor, uid, ot_ids, context)
                        # Write and historize the comment about CUPS
                        self.write(cursor, uid, ot_ids, {
                            'description': self.SAME_CUPS_TEXT+sec_msg
                        })
                        self.case_log(cursor, uid, ot_ids, context)
        return case_ids

    def get_ots_cups_coincident(self, cursor, uid, cups_id, case_id=None,
                                context=None):
        """ Returns OTs (not closed) with the specified cups"""
        case_obj = self.pool.get("crm.case")
        imd_obj = self.pool.get('ir.model.data')
        ot_section = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_ot_abenergia', 'ot_ordenes_de_trabajo'
        )[1]
        ot_ids = case_obj.search(cursor, uid, [
            ('polissa_id.cups', '=', cups_id),
            ('section_id', 'child_of', [ot_section]),
            ('id', '!=', case_id),
            ('state', 'not in', ('done', 'cancel'))
        ])
        return ot_ids

    def get_non_commented_ots(self, cursor, uid, ot_ids, context=None):
        """ Returns ot ids which don't have the comment SAME_CUPS_TEXT """
        case_history_obj = self.pool.get("crm.case.history")
        res_ids = []
        for ot_id in ot_ids:
            history_lines = case_history_obj.search(cursor, uid, [
                ('case_id', '=', ot_id),
                ('description', '=', self.SAME_CUPS_TEXT)
            ])
            if not len(history_lines):
                res_ids.append(ot_id)
        return res_ids

    def write(self, cr, uid, ids, vals, context=None):
        if context is None:
            context = {}
        ignore_ids = []
        if 'state' in vals.keys() and vals['state'] == 'done':
            ignore_ids = self.get_ignore_ot_atr_connection(cr, uid, ids, context=context)
            context.update({'ignore_ot_atr_connection': ignore_ids})
        res = super(GiscedataSwitchingCrmAb, self).write(cr, uid, ids, vals, context=context)
        if ignore_ids:
            self.historize_msg(cr, uid, ignore_ids, u"Solicitud anulada")
            self.write(cr, uid, ignore_ids, {'state': 'cancel'}, context=context)
        return res

    def get_ignore_ot_atr_connection(self, cursor, uid, ids, context=None):
        """
         Given a list of ot ids, returns the ot ids of cases that must be
         canceled and ignored in the ot_atr_connection.
         This OTs are the ots related to the same ATR cases than 'ids' for those
         ids where the section is nulifier section.
        """
        res_ids = []

        section_obj = self.pool.get('crm.case.section')
        nulifier_section_ids = section_obj.search(
            cursor, uid,
            [('code', 'in', ['ANAR', 'ANICML', 'ANRIAM', 'ANOTA', 'ANRCAM', 'ANOTB', 'ANOTT', 'ANOTF'])]
        )

        for ot_info in self.read(cursor, uid, ids, ['section_id', 'ref']):
            if not ot_info['ref'] or not ot_info['section_id']:
                continue
            if ot_info['section_id'][0] not in nulifier_section_ids:
                continue

            other_ots = self.search(
                cursor, uid,
                [
                    ('id', '!=', ot_info['id']),
                    ('ref', '=', ot_info['ref']),
                    ('state', '!=', 'cancel')
                ]
            )

            res_ids.extend(other_ots)

        return res_ids

    def get_related_ots(self, cursor, uid, ot_id, context=None):
        res_ids = super(GiscedataSwitchingCrmAb, self).get_related_ots(cursor, uid, ot_id)
        original_ot = self.browse(cursor, uid, ot_id)
        if original_ot.section_id.code not in ['ANAR', 'ANICML', 'ANRIAM',
                                               'ANOTA', 'ANRCAM', 'ANOTB',
                                               'ANOTT', 'ANOTF']:
            return res_ids

        related_ots = self.search(
            cursor, uid,
            [
                ('id', 'not in', res_ids+[ot_id]),
                ('ref', '=', original_ot.ref),
                ('section_id.code', '=', original_ot.section_id.code[2:])
            ]
        )
        res_ids.extend(related_ots)
        return res_ids

    def _history(self, cr, uid, cases, keyword, history=False, email=False, context=None):
        # Historize the same text in the retated ATR if we have one
        sw_obj = self.pool.get("giscedata.switching")
        if history:
            for case in cases:
                if case.ref and case.ref.split(",")[0] == "giscedata.switching":
                    sw_id = int(case.ref.split(",")[1])
                    sw_obj.historize_msg(
                        cr, uid, sw_id, case.description, context=context
                    )
        return super(GiscedataSwitchingCrmAb, self)._history(cr, uid, cases, keyword, history=history, email=email, context=context)

GiscedataSwitchingCrmAb()
