# -*- coding: utf-8 -*-
from osv import osv, fields
from giscedata_switching_distri.giscedata_switching import (
    AUTOMATIZATION_PROCESS
)

AUTOMATIZATION_PROCESS.update({
    'A3-01': {
        'default': {
            'ot_section_code': 'OTA',
            'title': 'Alta nuevo suministro: {0}'
        }
    },
    'B1-01': {
        'conditions':
            [
                {
                    'field_name': 'motiu',
                    'value': '01',
                    'ot_section_code': 'OTB',
                    'title': 'Baja por cese: {0}'
                },
                {
                    'field_name': 'motiu',
                    'value': '02',
                    'ot_section_code': 'OTF',
                    'title': 'Fin contrato suministro: {0}'
                },
                {
                    'field_name': 'motiu',
                    'value': '03',
                    'ot_section_code': 'OTC',
                    'title': 'Corte de suministro: {0}'
                },
                {
                    'field_name': 'motiu',
                    'value': '04',
                    'ot_section_code': 'OTB',
                    'title': 'Baja por impago: {0}'
                },
            ]
    },
    'C1-01': {
        'default': {
            'ot_section_code': 'ICML',
            'title': 'C.C sin cambios: {0}'
        }
    },
    'W1-01': {
        'default': {
            'ot_section_code': 'OTL',
            'title': 'Autolectura: {0}'
        }
    },
    'C2-01': {
        'conditions':
            [
                {
                    'field_name': 'sollicitudadm',
                    'value': 'S',
                    'ot_section_code': 'ICML',
                    'title': 'C.C. con cambios Titular: {0}'
                },
                {
                    'field_name': 'sollicitudadm',
                    'value': 'N',
                    'ot_section_code': 'ICML',
                    'title': 'C.C. con cambios Potencia: {0}'
                },
                {
                    'field_name': 'sollicitudadm',
                    'value': 'A',
                    'ot_section_code': 'ICML',
                    'title': 'C.C. con cambios Potencia y titular: {0}'
                },
                {
                    'field_name': 'sollicitudadm',
                    'value': 'P',
                    'ot_section_code': 'ICML',
                    'title': 'C.C. con cambios Periodicidad: {0}'
                },
            ]
    },
    'M1-01': {
        'conditions':
            [
                {
                    'field_name': 'sollicitudadm',
                    'value': 'S',
                    'ot_section_code': 'OTT',
                    'title': 'Mod. Titular: {0}'
                },
                {
                    'field_name': 'sollicitudadm',
                    'value': 'N',
                    'ot_section_code': 'AR',
                    'title': 'Mod. Potencia: {0}'
                },
                {
                    'field_name': 'sollicitudadm',
                    'value': 'A',
                    'ot_section_code': 'AR',
                    'title': 'Mod. Potencia y titular: {0}'
                },
                {
                    'field_name': 'sollicitudadm',
                    'value': 'P',
                    'ot_section_code': 'OTG',
                    'title': 'Mod. Periodicidad: {0}'
                },
            ]
    },
})


class GiscedataSwitching(osv.osv):

    _name = 'giscedata.switching'
    _inherit = 'giscedata.switching'

    def importar_xml_post_hook(self, cursor, uid, sw_id, context=None):
        res = super(GiscedataSwitching, self).importar_xml_post_hook(cursor, uid, sw_id, context=context)
        sw_obj = self.pool.get('giscedata.switching')
        crm_obj = self.pool.get('crm.case')
        sw = sw_obj.browse(cursor, uid, sw_id)
        pas_desc = "{0}-{1}".format(sw.proces_id.name, sw.step_id.name)
        if pas_desc in ['C1-08', 'C2-08', 'M1-06', 'A3-06', 'B1-03']:
            is_b1_03 = False
            b101_obj = self.pool.get('giscedata.switching.b1.01')
            b101_id = b101_obj.search(cursor, uid, [('sw_id', '=', sw.id)])
            if len(b101_id):
                pas01_info = b101_obj.read(cursor, uid, b101_id[0], ['motiu'])
                is_b1_03 = pas01_info['motiu'] == '03'
            related_ot = self.get_related_ots(cursor, uid, sw.id, context=context)
            ot_states = crm_obj.read(cursor, uid, related_ot, ['state'])
            correct_ot_state = (len(related_ot) and
                                ot_states[0]['state'] in ['done', 'cancel'])
            if not is_b1_03 and correct_ot_state:
                self.create_reject_sol_finalitzada(cursor, uid, sw, context=context)
            else:  # is_b1_03 or not correct_ot_state
                sw.write({'state': 'draft'})
        return res

    def get_related_ots(self, cursor, uid, sw_id, context=None):
        crm_obj = self.pool.get('crm.case')
        sw_ref_desc = "giscedata.switching,{0}".format(sw_id)
        return crm_obj.search(
            cursor, uid,
            ['|', ('ref', '=', sw_ref_desc), ('ref2', '=', sw_ref_desc)]
        )

    def create_reject_sol_finalitzada(self, cursor, uid, sw, context=None):
        mrej_obj = self.pool.get("giscedata.switching.motiu.rebuig")
        new_step_name = '0{0}'.format(int(sw.step_id.name)+1)
        step_name = 'giscedata.switching.{0}.{1}'.format(
            sw.proces_id.name.lower(), new_step_name
        )
        step_obj = self.pool.get(step_name)
        header_obj = self.pool.get("giscedata.switching.step.header")
        if getattr(step_obj, 'dummy_create_reb', False):
            rid = mrej_obj.search(cursor, uid, [('name', '=', '43')])
            rej = mrej_obj.browse(cursor, uid, rid)
            pas_id = step_obj.dummy_create_reb(cursor, uid, sw.id, rej, context)

            header_id = step_obj.read(cursor, uid, pas_id, ['header_id'])
            header_obj.write(cursor, uid, header_id['header_id'][0], {'enviament_pendent': True})
            sw.create_step(new_step_name, pas_id, context=context)

    def activa_polissa_cas_atr(self, cursor, uid, sw_id, context=None):
        if context is None:
            context = {}
        res = super(GiscedataSwitching, self).activa_polissa_cas_atr(cursor, uid, sw_id, context=context)

        sw_obj = self.pool.get('giscedata.switching')
        crm_obj = self.pool.get('crm.case')
        section_obj = self.pool.get('crm.case.section')
        sw = sw_obj.browse(cursor, uid, sw_id)
        pas_desc = "{0}-{1}".format(sw.proces_id.name, sw.step_id.name)
        if pas_desc in ['C1-08', 'C2-08', 'M1-06', 'A3-06', 'B1-03']:
            is_b1_03 = False
            b101_obj = self.pool.get('giscedata.switching.b1.01')
            b101_id = b101_obj.search(cursor, uid, [('sw_id', '=', sw.id)])
            if len(b101_id):
                pas01_info = b101_obj.read(cursor, uid, b101_id[0], ['motiu'])
                is_b1_03 = pas01_info['motiu'] == '03'
            related_ot = self.get_related_ots(cursor, uid, sw.id, context=context)
            if not is_b1_03 and len(related_ot):
                crm_obj.historize_msg(
                    cursor, uid, related_ot,
                    u"Se ha recibido solicitud de anulación por parte de la comercializadora"
                )
                self.create_anulacio_ots(cursor, uid, sw.id, related_ot, context=context)
            elif not is_b1_03 and not len(related_ot):
                sw.write({'state': 'open'})
                self.create_acceptacio_anulacio(cursor, uid, sw, context=context)
            elif is_b1_03 and len(related_ot):
                all_ots = related_ot
                for related_ot in all_ots:
                    ot_state = crm_obj.read(cursor, uid, related_ot, ['state'])['state']
                    if ot_state == 'draft':
                        crm_obj.historize_msg(
                            cursor, uid, related_ot, u"Solicitud anulada"
                        )
                        self.create_acceptacio_anulacio(cursor, uid, sw, context=context)
                        context.update({'ignore_ot_atr_connection': all_ots})
                        crm_obj.write(cursor, uid, related_ot, {'state': 'cancel'}, context=context)
                    elif ot_state in ['done', 'open']:
                        crm_obj.historize_msg(
                            cursor, uid, related_ot,
                            u"Solicitud reconexión por parte de la comercializadora"
                        )
                        otr_sect = section_obj.search(cursor, uid, [('code', '=', 'OTR')])
                        case_info = crm_obj.read(
                            cursor, uid, related_ot,
                            ['section_id', 'name', 'polissa_id', 'ref',
                             'partner_id', 'partner_address_id']
                        )
                        vals = {
                            'section_id': otr_sect[0],
                            'name': u"Reconexión " + case_info['name'],
                            'description': u"Reconexión  " + case_info['name'],
                            'polissa_id': case_info['polissa_id'][0],
                            'ref': case_info['ref'],
                            'partner_id': case_info['partner_id'][0],
                            'partner_address_id': case_info['partner_address_id']
                        }
                        crm_obj.create_case_generic(
                            cursor, uid, [sw_id], context=context, extra_vals=vals
                        )
                    elif ot_state == 'cancel':
                        self.create_reject_sol_finalitzada(cursor, uid, sw, context=context)
            else:  # is_b1_03 and not len(related_ot)
                self.create_acceptacio_anulacio(cursor, uid, sw, context=context)

        return res

    def create_anulacio_ots(self, cursor, uid, sw_id, crm_ids, context=None):
        crm_obj = self.pool.get('crm.case')
        section_obj = self.pool.get('crm.case.section')
        for case_info in crm_obj.read(cursor, uid, crm_ids, ['section_id', 'name', 'polissa_id', 'ref', 'partner_id', 'partner_address_id']):
            sect_code = section_obj.read(cursor, uid, case_info['section_id'][0], ['code'])['code']
            anul_sect = section_obj.search(cursor, uid, [('code', '=', "AN"+sect_code)])
            if not len(anul_sect):
                continue
            vals = {
                'section_id': anul_sect[0],
                'name': u"Anulación " + case_info['name'],
                'description': u"Anulación " + case_info['name'],
                'polissa_id': case_info['polissa_id'][0],
                'ref': case_info['ref'],
                'partner_id': case_info['partner_id'][0],
                'partner_address_id': case_info['partner_address_id']
            }
            crm_obj.create_case_generic(
                cursor, uid, [sw_id], context=context, extra_vals=vals
            )

    def create_acceptacio_anulacio(self, cursor, uid, sw, context=None):
        new_step_name = '0{0}'.format(int(sw.step_id.name) + 1)
        step_name = 'giscedata.switching.{0}.{1}'.format(
            sw.proces_id.name.lower(), new_step_name
        )
        step_obj = self.pool.get(step_name)
        header_obj = self.pool.get("giscedata.switching.step.header")
        if getattr(step_obj, 'dummy_create', False):
            pas_id = step_obj.dummy_create(cursor, uid, sw.id, context)
            header_id = step_obj.read(cursor, uid, pas_id, ['header_id'])
            header_obj.write(cursor, uid, header_id['header_id'][0], {'enviament_pendent': True})
            sw.create_step(new_step_name, pas_id, context=context)

GiscedataSwitching()
