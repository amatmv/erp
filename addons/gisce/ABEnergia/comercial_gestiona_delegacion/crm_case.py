# -*- coding: utf-8 -*-

from osv import osv, fields

class crm_case(osv.osv):
    _name = 'crm.case'
    _inherit = 'crm.case'

    def _ff_delegs(self, cr, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        if not isinstance(ids, list):
            ids = [ids, ]
        res = dict.fromkeys(ids, False)
        user_obj = self.pool.get('res.users')
        crmcase_obj = self.pool.get('crm.case')
        section_obj = self.pool.get('crm.case.section')
        pol_obj = self.pool.get('giscedata.polissa')

        delegs_user = user_obj.read(cr, uid, uid,['deleg_gestionades']
                                    )['deleg_gestionades']

        ids = crmcase_obj.search(cr, uid, [])
        cases = crmcase_obj.read(cr, uid, ids,
                                 ['section_id', 'ref', 'user_id'])

        section_ids = [case['section_id'][0] for case in cases]
        section_codes = {}
        sections = section_obj.read(cr, uid, section_ids, ['code'])
        for section in sections:
            section_codes[section['id']] = section['code']

        polisses_ids = []
        for case in cases:
            if section_codes[case['section_id'][0]] == 'IMP_COMER':
                pol_id = int(case['ref'].split(',')[1])
                polisses_ids.append(pol_id)

        polisses_delegacio = pol_obj.read(cr, uid, polisses_ids, ['delegacion'])
        pol_deleg = {}
        for polissa in polisses_delegacio:
            pol_deleg[polissa['id']] = polissa['delegacion']

        for case in cases:
            if section_codes[case['section_id'][0]] == 'IMP_COMER':
                pol_id = int(case['ref'].split(',')[1])
                if pol_deleg[pol_id][0] in delegs_user:
                    res[case['id']]=True

        return res

    def _fnct_search_delegs(self, cr, uid, obj, name, args,
                                     context=None):
        user_obj = self.pool.get('res.users')
        delegs_loguejat = user_obj.read(cr, uid, uid, ['deleg_gestionades'])
        if not delegs_loguejat:
            return [('id', '=', '0')]
        else:
            ids_casos = []

            user_obj = self.pool.get('res.users')
            crmcase_obj = self.pool.get('crm.case')
            section_obj = self.pool.get('crm.case.section')
            pol_obj = self.pool.get('giscedata.polissa')

            delegs_user = user_obj.read(cr, uid, uid,['deleg_gestionades']
                                        )['deleg_gestionades']

            ids = crmcase_obj.search(cr, uid, [])
            cases = crmcase_obj.read(cr, uid, ids,
                                     ['section_id', 'ref', 'user_id'])

            section_ids = [case['section_id'][0] for case in cases]
            section_codes = {}
            sections = section_obj.read(cr, uid, section_ids, ['code'])
            for section in sections:
                section_codes[section['id']] = section['code']

            polisses_ids = []
            for case in cases:
                if section_codes[case['section_id'][0]] == 'IMP_COMER':
                    pol_id = int(case['ref'].split(',')[1])
                    polisses_ids.append(pol_id)

            polisses_delegacio = pol_obj.read(cr, uid, polisses_ids, ['delegacion'])
            pol_deleg = {}
            for polissa in polisses_delegacio:
                pol_deleg[polissa['id']] = polissa['delegacion']

            for case in cases:
                if section_codes[case['section_id'][0]] == 'IMP_COMER':
                    pol_id = int(case['ref'].split(',')[1])

                    if pol_deleg[pol_id][0] in delegs_user:
                        ids_casos.append(case['id'])

        if ids_casos:
            return [('id', 'in', ids_casos)]

        else:
            return [('id', '=', '0')]

    _columns = {
        'in_user_delegs': fields.function(
            _ff_delegs, type='boolean', method=True,
            fnct_search=_fnct_search_delegs,
        ),
    }

crm_case()
