# -*- coding: utf-8 -*-

from osv import osv, fields


class giscedata_polissa(osv.osv):
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def _ff_delegs(self, cr, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        if not isinstance(ids, list):
            ids = [ids, ]
        res = dict.fromkeys(ids, False)
        user_obj = self.pool.get('res.users')
        delegs_user = user_obj.read(cr, uid, uid,['deleg_gestionades']
                            )['deleg_gestionades']
        pols = self.read(cr, uid, ids, ['delegacion'])
        for pol in pols:

            # mirem quins ccials gestionen aquesta polissa segons la delegacio
            # de la polissa i les delegacions gestionades per cada ccial
            deleg_polissa = pol['delegacion'][0]

            # si la delegacio de la polissa està entre les gestionades per
            # l'usuari loguejat
            if deleg_polissa in delegs_user:
                res[pol['id']] = True
        return res

    def _fnct_search_delegs(self, cursor, uid, obj, name, args,
                                     context=None):
        pol_obj = self.pool.get('giscedata.polissa')
        user_obj = self.pool.get('res.users')
        delegs_user = user_obj.read(cursor, uid, uid, ['deleg_gestionades'])
        if not delegs_user:
            return [('id', '=', '0')]
        else:
            delegs_user = delegs_user['deleg_gestionades']
            polisses_id = pol_obj.search(cursor, uid, [
                ('delegacion', 'in', delegs_user)
            ], context=context)
        if polisses_id:
            return [('id', 'in', polisses_id)]
        else:
            return [('id', '=', '0')]

    _columns = {
        'pol_in_user_delegs': fields.function(
            _ff_delegs, type='boolean', method=True,
            fnct_search=_fnct_search_delegs,
            string='Delegaciones usuario'
        ),
    }

giscedata_polissa()
