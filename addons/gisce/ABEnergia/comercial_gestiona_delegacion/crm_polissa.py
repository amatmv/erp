# -*- coding: utf-8 -*-

from osv import osv, fields

class crm_polissa(osv.osv):
    _name = 'crm.polissa'
    _inherit = 'crm.polissa'

    def _ff_delegs_crmpol(self, cr, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        if not isinstance(ids, list):
            ids = [ids, ]
        res = dict.fromkeys(ids, False)
        user_obj = self.pool.get('res.users')
        for crmpol_id in ids:

            # mirem quins ccials porten aquesta oferta segons la delegacio
            # de la oferta i les delegacions gestionades per cada ccial
            deleg_crmpolissa = self.read(cr, uid, crmpol_id, ['delegacion']
                                      )['delegacion'][0]
            delegs_user = user_obj.read(cr, uid, uid,['deleg_gestionades']
                                        )['deleg_gestionades']

            # si la delegacio de la polissa està entre les gestionades per
            # l'usuari loguejat
            if deleg_crmpolissa in delegs_user:
                res[crmpol_id] = True
        return res

    def _fnct_search_delegs_crmpol(self, cursor, uid, obj, name, args,
                                     context=None):
        crmpol_obj = self.pool.get('crm.polissa')
        user_obj = self.pool.get('res.users')
        delegs_user = user_obj.read(cursor, uid, uid, ['deleg_gestionades'])
        if not delegs_user:
            return [('id', '=', '0')]
        else:
            delegs_user = delegs_user['deleg_gestionades']
            polisses_id = crmpol_obj.search(cursor, uid, [
                ('delegacion', 'in', delegs_user)
            ], context=context)
        if polisses_id:
            return [('id', 'in', polisses_id)]
        else:
            return [('id', '=', '0')]

    _columns = {
        'crmpol_in_user_delegs': fields.function(
            _ff_delegs_crmpol, type='boolean', method=True,
            fnct_search=_fnct_search_delegs_crmpol,
        ),
    }

crm_polissa()
