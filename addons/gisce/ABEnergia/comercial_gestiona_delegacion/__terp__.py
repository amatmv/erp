# -*- coding: utf-8 -*-
{
    "name": "Delegaciones gestionadas por el comercial",
    "description": """
        Afegeix a l'hr.employee les delegacions que gestiona (que poden ser la mateixa a la qual pertany o més i diferents).
        Afegeix a la hr.office els Employees que la gestionen.
        """,
    "version": "0-dev",
    "author": "abenergia",
    "category": "RRHH",
    "depends":[
        "base",
        "hr",
        "hr_office",
        "giscedata_polissa_comer_custom_ab",
        "crm_elec",
        "hr_custom_ab"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "hr_employee_view.xml",
        "hr_office_view.xml"
    ],
    "active": False,
    "installable": True
}
