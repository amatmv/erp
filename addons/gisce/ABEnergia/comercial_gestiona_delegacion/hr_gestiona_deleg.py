# -*- coding: utf-8 -*-

from osv import osv, fields


class hr_office(osv.osv):
    _name = 'hr.office'
    _inherit = 'hr.office'
    _columns = {
        'gestionada_por_ids' : fields.many2many('hr.employee',
                                                'hr_office_employee_rel',
                                                'hr_office',
                                                'hr_employee',
                                                'Gestionada por')
    }

    def get_gestionada_por(self):
        return self.gestionada_por_ids

hr_office()

class hr_employee(osv.osv):
    _name = 'hr.employee'
    _inherit = 'hr.employee'
    _columns = {
        'deleg_que_gestiona_ids': fields.many2many('hr.office',
                                                   'hr_office_employee_rel',
                                                   'hr_employee',
                                                   'hr_office',
                                                   'Delegaciones que gestiona')
    }

hr_employee()