# -*- coding: utf-8 -*-

from osv import osv, fields

class res_users(osv.osv):
    _name = 'res.users'
    _inherit = 'res.users'

    def _ff_deleg_gestionades(self, cr, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        if not isinstance(ids, list):
            ids = [ids, ]
        res = dict.fromkeys(ids, False)
        empl_obj = self.pool.get('hr.employee')
        for user_id in ids:
            params = [('user_id', '=', user_id)]
            id_empl = empl_obj.search(cr, uid, params)
            delegs = empl_obj.read(cr, uid, id_empl, ['deleg_que_gestiona_ids'])
            if delegs:
                res[user_id] = delegs[0]['deleg_que_gestiona_ids']
        return res

    _columns = {
        'deleg_gestionades': fields.function(
            _ff_deleg_gestionades, type='char', method=True,
            string='Delegaciones gestionadas', size=200
        ),
    }

res_users()
