# -*- coding: utf-8 -*-
from osv import osv, fields
import os
import pooler
from datetime import datetime, timedelta
from mongodb_backend.mongodb2 import mdbpool
from tools.translate import _
import threading
import csv
import shutil
import zipfile


class WizardExportSipsFromMongo(osv.osv_memory):
    _name = u'wizard.export.sips.from.mongo'
    _description = u'Exporta los SIPS de los CPs especificados'

    def get_file_store_paths(self, cursor, uid, context=None):
        cfg_obj = self.pool.get('res.config')

        download_path = cfg_obj.get(
            cursor, uid, 'comer_ifo_download_path', False
        )
        download_url = cfg_obj.get(
            cursor, uid, 'comer_ifo_download_url', False
        )

        return download_path, download_url

    def import_data_from_mongo(self, dbname, uid, ids, context=None):
        if context is None:
            context = {}

        cursor = pooler.get_db(dbname).cursor()

        cp_codes = self.read(
            cursor, uid, ids, ['cp_codes'], context=context
        )[0]['cp_codes']

        cp_codes = [x.strip() for x in cp_codes.split(',') if x.strip()]

        sips_fmt = mdbpool.get_collection('giscedata_sips_ps_fmtjul16')

        counter = sips_fmt.find(
            {
                'codi_postal': {'$in': cp_codes},
            },
            {
                '_id': 1
            }
        ).count()

        if counter < 1:
            raise osv.except_osv("Error", "No se han encontrado resultados")

        results = sips_fmt.find(
            {
             'codi_postal': {'$in': cp_codes},
             },
            {
                '_id': 0, 'name': 1, 'codi_postal': 1, 'provincia': 1, 'poblacio': 1,
                'tarifa': 1, 'nom': 1, 'cognom': 1,
                'direccio': 1,
                'promig_total': 1, 'data_ult_canv': 1, 'data_ulti_mov': 1,
                'pot_cont_p1': 1, 'pot_cont_p2': 1, 'pot_cont_p3': 1,
                'indicatiu_icp': 1, 'tensio': 1, 'distri': 1,
                'cod_distri': 1, 'persona_fj': 1
            }
        )
        iteration_count = float(0.0)
        counter = float(counter)
        row_fields = ['nom', 'data_ulti_mov', 'provincia', 'name',
                      'data_ult_canv', 'promig_total', 'indicatiu_icp',
                      'cognom', 'codi_postal', 'pot_cont_p3', 'tensio',
                      'distri', 'pot_cont_p1', 'pot_cont_p2', 'tarifa',
                      'cod_distri', 'persona_fj', 'direccio', 'poblacio'
                      ]

        download_path, download_url = self.get_file_store_paths(
            cursor, uid, context=context
        )

        if download_path and download_url:
            current_date = datetime.today().strftime("%d_%m_%Y")
            dirname = '/tmp/sips_cp_%s_%s_etc' % (current_date, cp_codes[0])
            # si existeix el directori, l'eliminem, així segur que comencem
            # de zero
            if os.path.exists(dirname):
                if os.path.isdir(dirname):
                    shutil.rmtree(dirname)
                else:
                    os.remove(dirname)
            os.mkdir(dirname)

            zip_name = 'sips_cp_%s_%s_etc.zip' % (current_date, cp_codes[0])

            zip_path = '%s/%s' % (dirname, zip_name)
            zip_file_obj = open(zip_path, 'w')
            sips_zip = zipfile.ZipFile(zip_file_obj, 'w')

            file_name = 'sips_cp_%s_%s_etc.csv' % (current_date, cp_codes[0])

            with open(file_name, 'w') as file_csv:

                writer = csv.DictWriter(
                    file_csv, delimiter=';', quoting=csv.QUOTE_NONE,
                    escapechar='\\',
                    fieldnames=row_fields
                )
                writer.writeheader()

                for row in results:
                    writer.writerow(row)
                    iteration_count += 1
                    percentage = float(iteration_count*float(100.0)/counter)
                    self.write(
                        cursor, uid, ids, {'progres': percentage}, context=context
                    )

                file_csv.close()

            sips_zip.write(file_name)

            sips_zip.close()

            new_path = os.path.join(download_path, zip_name)
            os.rename(zip_path, new_path)
            os.chmod(new_path, 0o777)

            self.write(cursor, uid, ids, {'download_url': '{}/{}'.format(download_url, zip_name)}, context=context)

            if os.path.exists(dirname):
                if os.path.isdir(dirname):
                    shutil.rmtree(dirname)
                else:
                    os.remove(dirname)

        else:
            raise osv.except_osv(
                "Error",
                "No estan configuradas las variables comer_ifo_download_*"
            )

        self.write(cursor, uid, ids, {'state': 'endfitxer'})

    def importacio(self, cursor, uid, ids, context=None):
        """Usamos threading para poder actualizar la barra de progreso"""
        if context is None:
            context = {}

        print_thread = threading.Thread(
            target=self.import_data_from_mongo,
            args=(cursor.dbname, uid, ids, context)
        )
        print_thread.start()
        self.write(cursor, uid, ids, {'state': 'end'})
        return True

    def do_nothing(self, cursor, uid, ids, context=None):
        pass

    _columns = {
        'state': fields.selection(
            [(u'init', u'Init'), (u'end', u'End'), (u'endfitxer', u'End File')], u'Estado'
        ),
        'cp_codes': fields.text(
            u'Códigos postales',
            required=True,
            help=u'Introduce los codigos postales separados '
                 u'por comas 17220, 17004...'
        ),
        'progres': fields.float(
            u'Progreso de importación del mongo', readonly=True
        ),
        'download_url': fields.char(u'Download', size=1000),
    }

    _defaults = {
        'cp_codes': lambda *a: u'',
        'state': lambda *a: u'init',
        'progres': lambda *a: 0.0,
        'download_url': lambda *a: 'github.com'
    }

WizardExportSipsFromMongo()