# -*- coding: utf-8 -*-
{
    "name": "Sips ABEnergia (Comercialitzadora)",
    "description": """Sips ABEnergia (Comercialitzadora)""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Facturacio",
    "depends":[
        "giscedata_facturacio_comer_ifo",
        "giscedata_sips",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/wizard_export_sips_from_mongo_to_csv_zip_view.xml",
        "security/giscedata_sips_custom_ab_security.xml",
        "security/ir.model.access.csv",
    ],
    "active": False,
    "installable": True
}