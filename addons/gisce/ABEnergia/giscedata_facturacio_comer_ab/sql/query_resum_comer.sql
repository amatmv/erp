select d.name AS "Distribuidora"
    , d.ref AS "Ref. Distribuidora"
    , p.name AS "Comercializadora"
    , p.ref AS "Cod. Comer."
    , pol.name as "Póliza"
    , pt.name
    , u.name AS Comercial
    , replace(tit.name,'"', '') as "Nombre cliente"
    , SUBSTRING(c.name,0,21) AS CUPS
    , replace(c.direccio,'"', '') as "dirección"
    , c.dp as "CP"
    , t.name AS "Tarifa Acceso"
    , i.number AS factura
    , i.origin AS origen
    , po.reference AS remesa
    , i.date_invoice AS "Fecha Factura"
    , CASE WHEN i.state = 'cancel' THEN 'Cancelada'
   WHEN i.state = 'paid' THEN 'Pagada'
   WHEN i.state = 'open' THEN 'Abierta'
   WHEN i.state = 'draft' THEN 'Borrador'
   END AS "Estado"
    , CASE WHEN f.tipo_rectificadora='A' THEN 'Anuladora'
   WHEN f.tipo_rectificadora='B' THEN 'Anuladora+Rectificadora'
   WHEN f.tipo_rectificadora='R' THEN 'Rectificadora'
   WHEN f.tipo_rectificadora='RA' THEN 'Rectificadora sin Anuladora'
   WHEN f.tipo_rectificadora='BRA' THEN 'Anuladora (ficticia)'
   ELSE 'Normal'
   END AS "tipo factura"
    , f.data_inici AS "fecha inicial"
    , f.data_final AS "fecha final"
    , (f.data_final -f.data_inici) + 1 as "num. días"
    , d.vat
    , (
		SELECT SUM(consum)
		FROM giscedata_facturacio_lectures_energia
		WHERE factura_id = f.id
		AND name LIKE '%%P1%%'
		AND tipus = 'activa'
    GROUP BY name
	) * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END)
	AS "Consumo P1"
    , (
		SELECT SUM(consum)
		FROM giscedata_facturacio_lectures_energia
		WHERE factura_id = f.id
		AND name LIKE '%%P2%%'
		AND tipus = 'activa'
    GROUP BY name
	) * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END)
	AS "Consumo P2"
    , (
		SELECT SUM(consum)
		FROM giscedata_facturacio_lectures_energia
		WHERE factura_id = f.id
		AND name LIKE '%%P3%%'
		AND tipus = 'activa'
    GROUP BY name
	) * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END)
	AS "Consumo P3"
    , (
		SELECT SUM(consum)
		FROM giscedata_facturacio_lectures_energia
		WHERE factura_id = f.id
		AND name LIKE '%%P4%%'
		AND tipus = 'activa'
    GROUP BY name
	) * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END)
	AS "Consumo P4"
    , (
		SELECT SUM(consum)
		FROM giscedata_facturacio_lectures_energia
		WHERE factura_id = f.id
		AND name LIKE '%%P5%%'
		AND tipus = 'activa'
    GROUP BY name
	)  * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END)
	AS "Consumo P5"
    , (
		SELECT SUM(consum)
		FROM giscedata_facturacio_lectures_energia
		WHERE factura_id = f.id
		AND name LIKE '%%P6%%'
		AND tipus = 'activa'
    GROUP BY name
	)  * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END)
	AS "Consumo P6"
    , ene.energia * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS energia_kW
    , f.total_potencia * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS potencia
    , f.total_energia * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS energia
    , f.total_reactiva * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END)AS reactiva
    , altres.altres * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS "otros conceptos"
    , (altres.altres -  refact.refacturacio) * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS "otros sin refacturacion"
    , refact.refacturacio * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS refacturacion
    , refact_T4.refacturacio * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS refacturacion_T4
    , refact_T1.refacturacio * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END)  AS refacturacion_T1
    , f.total_lloguers * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END)  AS alquiler
    , i.amount_untaxed * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END)  AS "total conceptos"
    , it_18.name AS IVA1 
    , COALESCE(it_18.base,0) * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END)  AS "Base IVA1"
    , COALESCE(it_18.amount,0) * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END)  AS "total iva1"
    , it_21.name AS IVA2
    , COALESCE(it_21.base,0) * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END)  AS "Base IVA2"
    , COALESCE(it_21.amount,0) * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END)  AS "total iva2"
    , COALESCE(iese_18.base,0) * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END)   AS "Base IESE IVA1"
    , COALESCE(iese_18.amount,0) * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END)  AS "IESE IVA1"
    , (COALESCE(it_iese.base,0) - COALESCE(iese_18.base,0)) * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END)   AS "Base IESE IVA2"
    , (COALESCE(it_iese.amount,0) - COALESCE(iese_18.amount,0))  * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END)  AS "IESE IVA2"
    , COALESCE(it_iese.base,0)  * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END)  AS "Base IESE"
    , COALESCE(it_iese.amount,0) * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END)  AS "total IESE"
    , i.amount_untaxed * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END)  AS Base
    , i.amount_tax * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END)  AS iva
    , i.amount_total * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END)  AS TOTAL
    , rb.name AS Banco
FROM giscedata_facturacio_factura f 
LEFT JOIN giscedata_cups_ps c ON (f.cups_id=c.id)
LEFT JOIN giscedata_polissa pol ON (f.polissa_id=pol.id)
LEFT JOIN account_invoice i ON (i.id=f.invoice_id) 
LEFT JOIN giscedata_polissa_tarifa t ON (t.id=f.tarifa_acces_id)
LEFT JOIN (
SELECT lf.factura_id AS factura_id,SUM(li.quantity) AS energia
FROM giscedata_facturacio_factura_linia lf 
LEFT JOIN account_invoice_line li ON (li.id=lf.invoice_line_id)
WHERE lf.tipus='energia' AND lf.isdiscount = False
GROUP BY lf.factura_id
) AS ene ON (ene.factura_id=f.id)
LEFT JOIN (
SELECT lf.factura_id AS factura_id,SUM(li.price_subtotal) AS refacturacio
FROM giscedata_facturacio_factura_linia lf 
LEFT JOIN account_invoice_line li ON (li.id=lf.invoice_line_id)
WHERE lf.tipus='altres' AND li.name LIKE 'Refact%%'
GROUP BY lf.factura_id
) AS refact ON (refact.factura_id=f.id)
LEFT JOIN (
SELECT lf.factura_id AS factura_id,SUM(li.price_subtotal) AS refacturacio
FROM giscedata_facturacio_factura_linia lf 
LEFT JOIN account_invoice_line li ON (li.id=lf.invoice_line_id)
WHERE lf.tipus='altres' AND li.name LIKE 'Refact%%T4%%'
GROUP BY lf.factura_id
) AS refact_T4 ON (refact_T4.factura_id=f.id)
LEFT JOIN (
SELECT lf.factura_id AS factura_id,SUM(li.price_subtotal) AS refacturacio
FROM giscedata_facturacio_factura_linia lf 
LEFT JOIN account_invoice_line li ON (li.id=lf.invoice_line_id)
WHERE lf.tipus='altres' AND li.name LIKE 'Refact%%T1%%'
GROUP BY lf.factura_id
) AS refact_T1 ON (refact_T1.factura_id=f.id)
LEFT JOIN (
SELECT lf.factura_id AS factura_id,SUM(li.price_subtotal) AS altres
FROM giscedata_facturacio_factura_linia lf
LEFT JOIN account_invoice_line li ON (li.id=lf.invoice_line_id)
WHERE lf.tipus='altres'
GROUP BY lf.factura_id
) AS altres ON (altres.factura_id=f.id)
LEFT JOIN (
SELECT lf.factura_id AS factura_id
-- , STRING_AGG(li.name,',') AS linies
, SUM(li.price_subtotal) AS base 
, ROUND(SUM(li.price_subtotal) * 1.051130 * 0.04864,2) AS amount
FROM giscedata_facturacio_factura_linia lf 
LEFT JOIN account_invoice_line li ON (li.id=lf.invoice_line_id)
LEFT JOIN account_invoice_line_tax it_18 ON (li.id=it_18.invoice_line_id)
LEFT JOIN account_tax t_18 ON (it_18.tax_id=t_18.id AND t_18.name LIKE 'IVA 18%%')
LEFT JOIN account_invoice_line_tax iese ON (li.id=iese.invoice_line_id )
LEFT JOIN account_tax t_iese ON (iese.tax_id=t_iese.id AND t_iese.name LIKE '%%electricidad%%')
WHERE t_iese.id IS NOT NULL and t_18.id IS NOT NULL
GROUP BY lf.factura_id
) AS iese_18 ON (iese_18.factura_id=f.id)
LEFT JOIN account_invoice_tax it_18 ON (it_18.invoice_id=i.id AND it_18.name LIKE 'IVA 18%%')
LEFT JOIN account_invoice_tax it_21 ON (it_21.invoice_id=i.id AND it_21.name LIKE 'IVA 21%%')
LEFT JOIN account_invoice_tax it_iese ON (it_iese.invoice_id=i.id AND it_iese.name LIKE '%%electricidad%%')
LEFT JOIN res_partner d ON (d.id=c.distribuidora_id) 
LEFT JOIN res_partner p ON (p.id=i.company_id)
LEFT JOIN res_partner tit on (tit.id = i.partner_id)
LEFT JOIN payment_type pt on (pol.tipo_pago = pt.id)
LEFT JOIN res_users u ON (pol.user_id = u.id)
LEFT JOIN payment_order po ON (i.payment_order_id = po.id)
LEFT JOIN res_partner_bank rpb ON (rpb.id= pol.bank)
LEFT JOIN res_bank rb ON (rb.id= rpb.bank)
WHERE
f.id in (%s)
ORDER BY p.ref,CUPS
