# -*- coding: utf-8 -*-
from datetime import datetime

from osv import osv
import pooler


class GiscedataSuplementsTerritorials2013Comer(osv.osv):
    _name = 'giscedata.suplements.territorials.2013.comer'
    _inherit = 'giscedata.suplements.territorials.2013.comer'

    @staticmethod
    def get_info_html(cursor, uid, cups):
        pool = pooler.get_pool(cursor.dbname)
        sups_obj = pool.get('giscedata.suplements.territorials.2013.comer')

        schema = sups_obj.get_info_data(cursor, uid, cups)
        if schema:
            values = schema['columns']
            periodes = schema['periods']
            fields = values.keys()
            html = '''
                    <style type="text/css">
                        table.supl, table.supl th, table.supl td {
                            border-collapse: collapse;
                            border-spacing: unset;
                            font-family:sans-serif;
                            font-size: 7px;
                        }
                        table.supl td, table.supl th {
                            border: 1px solid black !important;
                            padding: 2px;
                        }
                        .c {
                            text-align:center;
                        }
                    </style>
                    <table class="supl">
                    <tr>
                        <th></th>
                        <th>Importe (€)</th>
                        <th></th>
                        <th>Valores</th>
                        <th>Imp. parcial €</th>
                    </tr>
                    '''

            for i in range(periodes):
                html += sups_obj.get_report_html_for_period(
                    {j: values[j][i] for j in fields}
                )

            html += '''
                        <tr>
                            <th>Importe Total</th>
                            <td style="text-align:right;">{}</td>
                            <td colspan="8"</td>
                        </tr>
                        </table>
                    '''.format(schema['total'])
            return html
        return ''

    @staticmethod
    def get_report_html_for_period(period, show_zeroes=False):

        value_potencia = 0
        value_energia = 0
        for fieldname in sorted(period.keys()):
            value = period[fieldname]
            if fieldname.startswith('media_pond'):
                value_potencia = value_potencia + int(value)
            elif fieldname.startswith('valor_energia_activa'):
                value_energia = value_energia + float(value)
        if not show_zeroes and value_potencia == 0:
            value_potencia = '-'
        if not show_zeroes and value_energia == 0:
            value_energia = '-'
        html = '''
                <tr>
                    <th rowspan="2">Periodo del {}<br> al {}</th>
                    <td style="text-align:right;" rowspan="2">{}</td>
                    <th>Energía (kWh)</th>
                    <td class="c">{}</td>
                    <td class="c">{}</td>
                </tr>
                <tr>
                    <th>Potencia (W)</th>
                    <td class="c">{}</td>
                    <td class="c">{}</td>
                </tr>
        '''.format(
            datetime.strptime(
                period['fecha_desde'], '%Y-%m-%d'
            ).strftime('%d/%m/%Y'),
            datetime.strptime(
                period['fecha_hasta'], '%Y-%m-%d'
            ).strftime('%d/%m/%Y'),
            period['importe_total_a_regularizar'],
            value_energia,
            period['importe_total_suplemento_termino_energia'],
            value_potencia,
            period['importe_total_suplemento_termino_potencia']
        )

        return html

GiscedataSuplementsTerritorials2013Comer()
