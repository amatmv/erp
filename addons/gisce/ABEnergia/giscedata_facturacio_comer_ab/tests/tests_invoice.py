# -*- coding: utf-8 -*-

from destral import testing
from destral.transaction import Transaction
from datetime import datetime


class TestAccountInvoiceSII(testing.OOTestCase):
    def test_invoice_is_possible_to_send_sii(self):
        """Comprobar que las facturas con fecha de factura original
        superior a 01/01/2017 se pueden enviar al SII"""
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            inv_obj = self.openerp.pool.get('account.invoice')
            invoice_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'invoice_0001'
            )[1]
            inv_obj.write(
                cursor, uid, [invoice_id],
                {
                    'type': 'in_invoice',
                    'date_invoice': '2017-07-01',
                    'origin_date_invoice': '2016-12-31',
                }
            )
            invoice = inv_obj.browse(cursor, uid, invoice_id)
            self.assertEqual(inv_obj.is_possible_to_send_sii(invoice), False)

            inv_obj.write(
                cursor, uid, [invoice_id],
                {
                    'date_invoice': '2017-01-01',
                    'origin_date_invoice': '2017-01-01',
                }
            )
            invoice = inv_obj.browse(cursor, uid, invoice_id)
            self.assertEqual(inv_obj.is_possible_to_send_sii(invoice), True)


class TestDuoDate(testing.OOTestCase):

    def test_duo_date_invoice_open_only_change_with_payment_type_cobrador(self):
        imd_obj = self.openerp.pool.get('ir.model.data')
        cfg_obj = self.openerp.pool.get('res.config')
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        inv_obj = self.openerp.pool.get('account.invoice')
        payment_obj = self.openerp.pool.get('payment.type')
        self.openerp.install_module(
            'crm_elec_unpaid_invoice'
        )
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            # Activamos la variable de configuración
            cfg_obj.set(cursor, uid, 'fact_date_due_invoice', '1')

            fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0001'
            )[1]

            fact2_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0002'
            )[1]

            payment_demo_id = imd_obj.get_object_reference(
                cursor, uid, 'account_payment', 'payment_type_demo'
            )[1]

            payment_cob_id = imd_obj.get_object_reference(
                cursor, uid, 'l10n_ES_remesas', 'payment_type_transferencia0'
            )[1]

            payment_obj.write(cursor, uid, payment_cob_id,
                              {'name': 'GESTION COBRADOR',
                               'code': 'GESTION COBRADOR'}
                              )

            fact1_info = fact_obj.read(
                cursor, uid, fact_id,
                ['invoice_id', 'payment_type', 'date_due']
            )

            fact2_info = fact_obj.read(
                cursor, uid, fact2_id,
                ['invoice_id', 'payment_type', 'date_due']
            )

            # Write payment types

            invoice_id = fact1_info['invoice_id'][0]

            data_due_factura_1 = fact1_info['date_due']
            avui = datetime.today()

            inv_obj.write(cursor, uid, [invoice_id],
                          {
                              'payment_type': payment_demo_id
                          })
            payment_ch = inv_obj.read(cursor, uid, invoice_id, ['payment_type'])['payment_type'][0]

            self.assertEqual(payment_ch, payment_demo_id)

            fact_obj.invoice_open(cursor, uid, fact_id)

            fact1_results = fact_obj.read(
                cursor, uid, fact_id, ['state', 'date_due']
            )

            fact1_res_duo_date = fact1_results['date_due']

            fact1_state = fact1_results['state']

            self.assertEqual(fact1_state, 'open')

            self.assertEqual(data_due_factura_1, fact1_res_duo_date)

            es_primer_periode = avui.day <= 15

            invoice2_id = fact2_info['invoice_id'][0]

            payment_info_changed = payment_obj.read(
                cursor, uid, payment_cob_id, ['name', 'code']
            )

            self.assertEqual(payment_info_changed['code'], 'GESTION COBRADOR')
            self.assertEqual(payment_info_changed['name'], 'GESTION COBRADOR')

            inv_obj.write(
                cursor, uid, invoice2_id, {'payment_type': payment_cob_id}
            )

            fact_obj.invoice_open(cursor, uid, fact2_id)

            fact2_results = fact_obj.read(
                cursor, uid, fact2_id, ['state', 'date_due']
            )

            fact2_state = fact2_results['state']

            self.assertEqual(fact2_state, 'open')

            fact2_duo_date = fact2_results['date_due']

            date_res = datetime.strptime(fact2_duo_date, "%Y-%m-%d")

            if es_primer_periode:
                # 20
                self.assertEqual(date_res.day, 20)
            else:
                # 5
                self.assertEqual(date_res.day, 5)
