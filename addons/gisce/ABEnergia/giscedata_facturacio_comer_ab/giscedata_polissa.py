# -*- coding: utf-8 -*-
from __future__ import absolute_import
from datetime import datetime, timedelta
from osv import osv, fields
from osv.expression import OOQuery
from tools.translate import _
from giscedata_facturacio.giscedata_polissa import INTERVAL_INVOICING_FIELDS

INTERVAL_INVOICING_FIELDS += ['fact_potencia_100']


class GiscedataPolissa(osv.osv):
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def get_last_out_invoice(self, cursor, uid, ids, context=None):
        """
        Obtains the last out energy invoice according to the begining date of
        the invoices.
        :param cursor:
        :param uid:
        :param ids: policy identifiers
        :type [long]
        :param context:
        :return: dictionary where the key is the policy identifier and the
        value is the out energy invoice identifier.
        """

        inv_o = self.pool.get('giscedata.facturacio.factura')
        q = OOQuery(inv_o, cursor, uid).select(['id'], order_by=('data_inici.desc',), limit=1)

        res = dict.fromkeys(ids, False)
        for self_id in ids:
            dmn = [('polissa_id', '=', self_id),
                   ('invoice_id.type', '=', 'out_invoice')]
            query = q.where(dmn)
            cursor.execute(*query)
            inv_v = cursor.dictfetchall()
            res[self_id] = inv_v[0]['id']

        return res

    def get_nullifier_or_rectifier_invoices(self, cursor, uid, ids, offset_date=None, context=None):
        """
        Obtains the energy invoices' ids that are 'nullifier' or 'rectifier'
        of a policy.
        :param cursor:
        :param uid:
        :param ids:
        :param offset_date: the returned invoices will have the 'data_inici'
        field greater than this parameter.
        :param context:
        :return: dictionary where the key is the policy id and the value a list
        with the energy invoices' ids.
        """

        if offset_date is None:
            offset_date = "1900-01-01"

        fact_o = self.pool.get('giscedata.facturacio.factura')

        cols = ['id', 'polissa_id']
        journals = ['ENERGIA.A', 'ENERGIA.B', 'ENERGIA.R']
        dmn = [('polissa_id', 'in', ids), ('data_inici', '>=', offset_date),
               ('invoice_id.journal_id.code', 'in', journals)]
        q = OOQuery(fact_o, cursor, uid).select(cols).where(dmn)
        cursor.execute(*q)
        res = {}

        for self_id in ids:
            res[self_id] = []

        for fact_v in cursor.dictfetchall():
            policy_id = fact_v['polissa_id']
            fact_id = fact_v['id']
            res[policy_id].append(fact_id)

        return res

    def get_punishable_kwh(self, cursor, uid, ids, offset_date=None, context=None):
        """
        Obtains the punishable kwh.
        :param cursor:
        :param uid:
        :param ids: policy identifiers
        :type: [long]
        :param offset_date: the begining date of the invoices used to calculate
        the punishable kwh will be equal or newer than offset_date. Format:
        %Y-%m-%d
        :type: str
        :param context:
        :return: dictionary where the key is the policy identifier and the value
        a dictionary with the following structure:
            - consumption: long.
            - info: str. if not empty, then something went wrong and the other
            fields of the dictionary may not exist. Error described in this
            field.
            - invoices_consumption: list of invoices names.
            - nullifiers_or_rectifiers: flag that indicates if the policy has
                nullifier or rectifier invoices since the offset_date.
            - planned_consumption: int.
            - planned_consumption_type: str. Possible values: anual_previsto,
                generado, anual_calculado.
            - punishable_kwh: long.
        """

        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        if offset_date is None:
            offset_date = "1900-01-01"

        res = {}
        cols = ['consumo_anual_previsto', 'consumo_anual_calculado']
        for pol in self.read(cursor, uid, ids, cols, context=context):
            pol_id = pol['id']
            if not pol:
                raise osv.except_osv(
                    _(u"Error"),
                    _(u"The policy id: {} does not exist").format(pol_id)
                )

            planned_consumption = pol['consumo_anual_previsto']
            planned_consumption_type = 'anual_previsto'

            if not planned_consumption:
                planned_consumption_type = 'anual_calculado'
                planned_consumption = pol['consumo_anual_calculado']

                if not planned_consumption:
                    q = """
                        SELECT sum(al.quantity)/sum(el.data_fins - el.data_desde)*365 AS extrapolated_consumption
                        FROM giscedata_facturacio_factura_linia AS el
                        INNER JOIN account_invoice_line al ON el.invoice_line_id = al.id
                        INNER JOIN giscedata_facturacio_factura f ON el.factura_id = f.id
                        INNER JOIN account_invoice AS i on f.invoice_id = i.id
                        INNER JOIN account_journal j ON i.journal_id = j.id
                        WHERE
                            el.tipus = 'energia' AND el.isdiscount = False AND
                            f.polissa_id = %s AND f.data_inici >= %s AND
                            j.code = 'ENERGIA' AND
                            i.state in ('open', 'paid')
                    """
                    cursor.execute(q, (pol_id, offset_date))
                    q_res = cursor.dictfetchall()
                    extrapolated_consumption = q_res[0]['extrapolated_consumption']

                    if extrapolated_consumption:
                        planned_consumption_type = 'generado'
                        planned_consumption = extrapolated_consumption
                    else:
                        info = ("No hay consumos informados o no hay facturas "
                                "emitidas")
                        res[pol_id] = {'info': info}
                        continue

            q = """
                SELECT i.number AS factura, sum(al.quantity) AS consumed_energy
                FROM account_invoice_line AS al
                INNER JOIN account_invoice AS i on al.invoice_id = i.id
                INNER JOIN giscedata_facturacio_factura_linia el ON  el.invoice_line_id = al.id
                INNER JOIN giscedata_facturacio_factura f ON el.factura_id = f.id
                INNER JOIN account_journal j ON i.journal_id = j.id
                WHERE
                    el.tipus = 'energia' AND el.isdiscount = False AND
                    f.polissa_id = %s AND f.data_inici >= %s AND
                    j.code = 'ENERGIA' AND
                    i.state in ('open', 'paid')
                GROUP BY i.id
            """

            cursor.execute(q, (pol_id, offset_date))
            q_res = cursor.dictfetchall()

            consumption = 0
            invoices_consumption = []
            for consumption_by_invoice in q_res:
                consumption += consumption_by_invoice['consumed_energy']
                invoices_consumption.append(consumption_by_invoice['factura'])

            punishable_kwh = (planned_consumption - consumption) * 0.05
            res[pol_id] = {
                'planned_consumption': planned_consumption,
                'planned_consumption_type': planned_consumption_type,
                'consumption': consumption,
                'invoices_consumption': invoices_consumption,
                'punishable_kwh': punishable_kwh,
            }

        nullifier_or_rectifier_invoices_by_policy = self.get_nullifier_or_rectifier_invoices(
            cursor, uid, ids, offset_date, context=context
        )
        for pol_id, nullifiers_or_rectifiers_ids in nullifier_or_rectifier_invoices_by_policy.items():
            res[pol_id]['nullifiers_or_rectifiers'] = bool(nullifiers_or_rectifiers_ids)

        return res

    def get_out_invoices_with_same_tariff(self, cursor, uid, ids, offset_date=None, context=None):
        """
        Obtains all the invoices that have the same tariff as the active modcon.
        If the active modcon does not have  invoices, then will search the
        the invoices that have the same tariff as the previous modcon
        (recursively).
        :param cursor:
        :param uid:
        :param ids: policy identifiers
        :type: [long]
        :param offset_date: the begining date of the obatained invoices will be
        equal or newer than offset_date. Format: %Y-%m-%d
        :type: str
        :param context:
        :return: dictionary where the key is the policy identifier and the
        value is a dictionary with the following sctructure:
            - info: str. if not empty, then something went wrong and the other
            fields of the dictionary may not exist. Error described in this
            field.
            - tariff: tariff name of the modcon used
            - policy_price_list_id: identifier of the used modcon's pricelist
            - invoice_ids: list of the invoices' identifiers found.
        """

        def search_inv_ids(tarifa_id):
            """
            Obtains the energy invoices' ids from a given policy, tariff and
            offset date where the invoice type is 'client'.
            :param tarifa_id:
            :return:
            """
            domain = dmn + [('tarifa_acces_id', '=', tarifa_id), ('polissa_id.id', '=', pol_id)]
            inv_q_f = inv_q.where(domain)

            cursor.execute(*inv_q_f)
            facts = cursor.dictfetchall()
            fact_ids = [fact['id'] for fact in facts]
            return fact_ids

        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        if offset_date is None:
            offset_date = "1900-01-01"

        modcon_obj = self.pool.get('giscedata.polissa.modcontractual')
        inv_obj = self.pool.get('giscedata.facturacio.factura')

        cols = ['modcontractual_activa', 'llista_preu', 'tarifa']
        res = {}

        inv_q = OOQuery(inv_obj, cursor, uid).select(['id'])
        dmn = [('invoice_id.journal_id.code', '=', 'ENERGIA'),
               ('data_inici', '>=', offset_date),
               ('invoice_id.state', 'in', ('open', 'paid')),
               ]

        for pol in self.read(cursor, uid, ids, cols, context=context):
            pol_id = pol['id']
            pol_price_list_id = pol['llista_preu'][0]
            tariff = pol['tarifa'][1]

            inv_ids = search_inv_ids(pol['tarifa'][0])

            use_previous_modcon = not bool(inv_ids)

            if use_previous_modcon:
                if pol['modcontractual_activa']:
                    active_modcon = pol['modcontractual_activa'][0]
                    has_prev_modcon = modcon_obj.read(
                        cursor, uid, active_modcon, ['modcontractual_ant'],
                        context=context
                    )['modcontractual_ant']

                    mdcn_cls = ['tarifa', 'llista_preu', 'modcontractual_ant']
                    while has_prev_modcon and use_previous_modcon:
                        selected_modcon = has_prev_modcon[0]
                        selected_modcon = modcon_obj.read(
                            cursor, uid, selected_modcon, mdcn_cls,
                            context=context
                        )

                        pol_price_list_id = selected_modcon['llista_preu'][0]
                        tariff_id = selected_modcon['tarifa'][0]
                        tariff = selected_modcon['tarifa'][1]

                        inv_ids = search_inv_ids(tariff_id)

                        use_previous_modcon = not bool(inv_ids)
                        has_prev_modcon = selected_modcon['modcontractual_ant']

                    if not has_prev_modcon and use_previous_modcon:
                        offset_date_i = datetime.strptime(
                            offset_date, "%Y-%m-%d %H:%M:%S"
                        )
                        offset_date_i = offset_date_i.strftime("%d-%m-%Y")
                        info = ("No hay facturas emitidas de cliente a partir "
                                "de {} que tengan la tarifa actual de la "
                                "póliza o de modificaciones contractuales "
                                "anteriores.".format(offset_date_i))
                        res[pol_id] = {'info': info}
                    else:
                        res[pol_id] = {
                            'tariff': tariff,
                            'policy_price_list_id': pol_price_list_id,
                            'invoice_ids': inv_ids
                        }
                else:
                    info = "No hay modificaciones contractuales."
                    res[pol_id]['info'] = info
            else:
                res[pol_id] = {
                    'tariff': tariff,
                    'policy_price_list_id': pol_price_list_id,
                    'invoice_ids': inv_ids
                }

        return res

    def get_penalty_from_prev(self, cursor, uid, ids, offset_date=None,
                              data_baixa=None, context=None):

        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        if offset_date is None:
            offset_date = "1900-01-01"

        res_config = self.pool.get('res.config')
        pol_obj = self.pool.get('giscedata.polissa')

        percent = float(res_config.get(cursor, uid, 'penalty_percent', 0.0))
        min_days = int(res_config.get(cursor, uid, 'penalty_min_days', 330))
        consum_types = res_config.get(cursor, uid, 'penalty_cons_types', '[]')
        tipus_consums = list(eval(consum_types))

        min_days = timedelta(days=min_days)
        if data_baixa is None:
            pols_data_baixa = pol_obj.read(cursor, uid, ids, ['data_baixa'])
            dates_baixa = {pol['id']: {
                'data_baixa': datetime.strptime(
                    pol['data_baixa'], '%Y-%m-%d')
                } for pol in pols_data_baixa
            }
            offset_date = datetime.strptime(offset_date, '%Y-%m-%d %H:%M:%S')

        res = self.get_penalty(cursor, uid, ids, offset_date, context)
        for pol_id in ids:
            # eliminem les quantitats calculades amb el mode per diferència
            res[pol_id]['total_penalty'] = 0
            res[pol_id]['penalties_by_period'] = {}
            for prd, pen in res[pol_id]['penalties_by_period'].items():
                res[pol_id]['penalties_by_period'][prd] = 0

            if data_baixa is None:
                data_baixa = dates_baixa[pol_id]['data_baixa']

            # recalculem només si tenim consum anual previst i fa menys que
            # min_dies que va renovar
            data_baixa = datetime.strptime(data_baixa, '%Y-%m-%d %H:%M:%S')
            data_offset = datetime.strptime(offset_date, '%Y-%m-%d %H:%M:%S')
            diferencia_dies = data_baixa - data_offset
            if diferencia_dies > min_days:
                res[pol_id]['info'] = 'No se calcula penalización, hace más ' \
                                      'de {} días desde la renovación'.format(
                    min_days
                )
                continue
            if res[pol_id]['planned_consumption_type'] not in tipus_consums:
                res[pol_id]['info'] = 'No se calcula penalización porque el ' \
                                      'tipo de consumo es {} pero solo se puede ' \
                                      'calcular con {}'.format(
                    res[pol_id]['planned_consumption_type'],
                    consum_types,
                )
                continue

            # calculem la proporció a aplicar que dependrà del número de dies
            proporcio = float((365 - diferencia_dies.days) / 365.0)
            res[pol_id]['proporcion'] = "(365 - {}) / 365 = {}".format(
                diferencia_dies.days,
                proporcio,
            )
            cons_previst = res[pol_id]['planned_consumption']
            penalty_kwh = cons_previst * percent * proporcio
            res[pol_id]['punishable_kwh'] = penalty_kwh
            total_penalty = 0
            for prd, pes in res[pol_id].get('periods_distribution', {}).items():
                penalty_per = pes * penalty_kwh * res[pol_id]['period_price'][prd]
                res[pol_id]['penalties_by_period'][prd] = penalty_per
                total_penalty += penalty_per
            res[pol_id]['total_penalty'] = total_penalty

        return res

    def get_penalty(self, cursor, uid, ids, offset_date=None, context=None):
        """
        Obtains the data relative to the policy's penalty.
        :param cursor:
        :param uid:
        :param ids: policy identifiers
        :type: [long]
        :param offset_date:
        :param context:
        :return: dictionary where the key is the policy identifier and the value
        a dictionary with the following structure:
            - consumption: long.
            - info: str. if not empty, then something went wrong and the other
            fields of the dictionary may not exist. Error described in this
            field.
            - invoice_ids: list of invoices identifiers.
            - invoices_consumption: list of invoices names.
            - policy_price_list_id: price list identifier.
            - invoices_tariff: list of invoices names.
            - nullifiers_or_rectifiers: flag that indicates if the policy has
                nullifier or rectifier invoices since the offset_date.
            - penalties_by_period: dictionary where the key is a period name
                and the value is the absolute consumption.
            - periods_distribution: dictionary where the key is a period name
                and the value is the relative consumption.
            - planned_consumption: int.
            - planned_consumption_type: str. Possible values: anual_previsto,
                generado, anual_calculado.
            - price_lists: dictionary where the key is the period name and the
                value is the price list name.
            - punishable_kwh: long.
            - tariff: str. tariff name.
            - total_penalty: long.
        """

        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        if offset_date is None:
            offset_date = "1900-01-01"

        inv_obj = self.pool.get('giscedata.facturacio.factura')
        line_o = self.pool.get('giscedata.facturacio.factura.linia')
        plist_obj = self.pool.get('product.pricelist')
        res_config = self.pool.get('res.config')
        tipo = res_config.get(cursor, uid, 'penalty_calculation_type', 'diff')

        res = self.get_punishable_kwh(
            cursor, uid, ids, offset_date, context=context
        )
        cols = ['mode_facturacio']

        pols_next_stage = []

        for pol_id in ids:
            if not res[pol_id].get('info', False):
                if tipo == 'prev' and res[pol_id]['punishable_kwh'] < 0:
                    info = ("Consumo mas grande del previsto. "
                            "No habrá penalización. ")
                    res[pol_id]['info'] = info
                else:
                    pols_next_stage.append(pol_id)

        invoices_same_tariff = self.get_out_invoices_with_same_tariff(
            cursor, uid, pols_next_stage, offset_date, context=context
        )

        for pol in self.read(cursor, uid, pols_next_stage, cols, context=context):
            pol_id = pol['id']
            res[pol_id].update(invoices_same_tariff[pol_id])

            if res[pol_id].get('info', False):
                continue

            inv_ids = res[pol_id]['invoice_ids']
            pol_price_list_id = res[pol_id]['policy_price_list_id']
            punishable_kwh = res[pol_id]['punishable_kwh']

            mode_facturacio = pol['mode_facturacio']

            invs = inv_obj.read(cursor, uid, inv_ids, ['number'], context=context)
            invoices_tariff = [i['number'] for i in invs]

            q = """
                SELECT al.name AS period, al.product_id AS product, sum(al.quantity) AS consumed
                FROM account_invoice_line AS al
                INNER JOIN account_invoice AS i on al.invoice_id = i.id
                INNER JOIN giscedata_facturacio_factura_linia el ON  el.invoice_line_id = al.id
                INNER JOIN giscedata_facturacio_factura f ON el.factura_id = f.id
                WHERE
                    el.tipus = 'energia' AND el.isdiscount = False AND
                    f.id IN %s
                GROUP BY al.name, al.product_id
            """
            cursor.execute(q, (tuple(inv_ids),))
            q_res = cursor.dictfetchall()

            total_energy = 0
            period_price = {}
            penalties_by_period = {}
            periods_distribution = {}
            price_lists = {}

            for energy in q_res:
                total_energy += energy['consumed']
                periods_distribution[energy['period']] = 0

            if mode_facturacio == 'index':
                inv_id = self.get_last_out_invoice(
                    cursor, uid, [pol_id], context=context
                )[pol_id]
                ln_cls = ['price_unit_multi', 'invoice_line_id.name',
                          'factura_id.llista_preu.name']
                ln_dmn = [('factura_id', '=', inv_id),
                          ('tipus', '=', 'energia'),
                          ('isdiscount', '=', False)]
                q = OOQuery(line_o, cursor, uid).select(ln_cls).where(ln_dmn)
                cursor.execute(*q)
                line_vs = cursor.dictfetchall()

                for line_v in line_vs:
                    period = line_v['invoice_line_id.name']
                    price_lists[period] = line_v['factura_id.llista_preu.name']
                    period_price[period] = line_v['price_unit_multi']
            else:
                for energy in q_res:
                    period = energy['period']
                    plist_id, period_price[period] = plist_obj.price_get(
                        cursor, uid, [pol_price_list_id], energy['product'], 1,
                        context={'date': datetime.today().strftime("%Y-%m-%d")}
                    ).items()[0]

                    plist_v = plist_obj.read(cursor, uid, plist_id, ['name'])
                    price_lists[period] = plist_v['name']

            total_penalty = 0
            for energy in q_res:
                period = energy['period']
                if energy['consumed'] == 0 and total_energy == 0:
                    distribution = 0.0
                else:
                    distribution = energy['consumed'] / total_energy
                periods_distribution[period] = round(distribution, 2)
                penalty = distribution * punishable_kwh * period_price[period]
                penalties_by_period[period] = penalty
                total_penalty += penalty

            res[pol_id].update({
                'invoices_tariff': invoices_tariff,
                'periods_distribution': periods_distribution,
                'price_lists': price_lists,
                'penalties_by_period': penalties_by_period,
                'total_penalty': total_penalty,
                'period_price': period_price,
                'info': "",
            })

        return res

    def penalitzacio(self, cursor, uid, ids, a_partir_de=None, context=None):
        """
        Obté les dades relatives a la penalitazió del contracte.
        :param cursor:
        :param uid:
        :param ids: identificadors de la pòlissa.
        :param a_partir_de: pel càlcul de la penalització es faran servir les
        factures que tinguin data d'inici més gran que aquest paràmetre.
        Format: %Y-%m-%d
        :type: str
        :param context:
        :return: Diccionari on la clau és l'identificador de la pòlissa i el
        valor un diccionari amb les dades relatives a la penalització:
            - consumption: long.
            - info: str. si no està buit indica que hi ha hagut algun error.
            En aquest cas algun dels camps del diccionari que es retorna pot no
            existir. L'error es indica en aquest camp.
            - invoices_consumption: llista amb els números de factura que s'han
                fet servir per calcular el consum.
            - invoices_tariff: llista amb els número de factura que tenen
                la tarifa amb la qual s'ha fet el càlcul de la penalització.
            - nullifiers_or_rectifiers: flag que indica si la pòlissa té
                factures (des de la data a_partir_de) que són rectificadores o
                anuladores.
            - penalties_by_period: diccionari on la clau és el nom del periòde
            i el valor és el consum absolut de totes les factures indicades
            per aquell periòde.
            - periods_distribution: diccionari on la clau és el nom del periòde
            i el valor és el consum relatiu (%) de totes les factures indicades
            per aquell periòde.
            - planned_consumption: int.
            - planned_consumption_type: str. Possibles valors: anual_previsto,
                generado, anual_calculado.
            - price_lists: diccionari on la clau és el nom del periode i el
                valor el nom de la tarifa amb la qual s'ha calculat el preu
                de l'energia per cada periode.
            - punishable_kwh: long. kWh que són penalitzables.
            - tariff: str. Nom de la tarifa.
            - total_penalty: long. Penalització.
        """

        penalties = self.get_penalty(
            cursor, uid, ids, a_partir_de,  context=context
        )
        penalties_by_name = {}
        for pol_id, penalty in penalties.items():
            pol_v = self.read(cursor, uid, pol_id, ['name'], context=context)
            if penalty.get('policy_price_list_id', False):
                del penalty['policy_price_list_id']
            if penalty.get('invoice_ids', False):
                del penalty['invoice_ids']
            penalties_by_name[pol_v['name']] = penalty

        return penalties_by_name

    def renovacio_tacita(self, cursor, uid, pol_id, context=None):
        listas_ext_obj = self.pool.get('correspondencies.extincions.tarifa')
        pol_obj = self.pool.get('giscedata.polissa')
        modcon_obj = self.pool.get('giscedata.polissa.modcontractual')
        rv_obj = self.pool.get('registro.vencimiento')
        fields_read = ['data_activacio', 'data_ultima_lectura', 'llista_preu', 'modcontractual_activa']

        if context is None:
            context = {}

        if isinstance(pol_id, (list, tuple)):
            pol_id = pol_id[0]

        pol = self.read(cursor, uid, pol_id, fields_read, context=context)

        tipo_renovacion = 'tacita'

        upd_params = {
            'tipus_renovacio': tipo_renovacion
        }

        tarifa_desti = listas_ext_obj.get_tarifa_correspondencia(
            cursor, uid, tarifa_origen_id=pol['llista_preu'][0],
            contracte_id=pol_id,
        )

        if tarifa_desti:

            # MODCON START
            upd_params.update({'llista_preu': tarifa_desti[0]})

            self.send_signal(cursor, uid, [pol_id], ['modcontractual'])

            self.write(cursor, uid, pol_id, upd_params, context=context)

            wz_crear_mc_obj = self.pool.get('giscedata.polissa.crear.contracte')

            ctx = context.copy()
            ctx.update({'active_id': pol_id, 'active_ids': [pol_id]})
            ctx.update({'renovacio_cron': True})

            params = {
                'tipus_renovacio': 'tacita',
                'duracio': 'nou',
                'accio': 'nou'
            }

            wiz_mod_id = wz_crear_mc_obj.create(
                cursor, uid, params, context=ctx
            )

            wizard = wz_crear_mc_obj.browse(cursor, uid, wiz_mod_id,
                                            context=ctx)

            res = wz_crear_mc_obj.onchange_duracio(
                cursor, uid, [wiz_mod_id], wizard.data_inici,
                wizard.duracio, context=ctx
            )

            data_final = res['value'].get('data_final', False)

            if data_final:
                wizard.write({'data_final': data_final}, context=ctx)

            wizard.action_crear_contracte(context=ctx)

            # END MODCON

            # Dont deactivate registro.vencimiento, it will be deactivated on
            # invoice generation with the new pricelist

            # START UPDATE OBSEVACIONS
            old_modcon_fecha_final = modcon_obj.read(
                cursor, uid, pol['modcontractual_activa'][0], ['data_final']
            )['data_final']

            q = OOQuery(pol_obj, cursor, uid)
            sql = q.select(
                ['modcontractual_activa.id',
                 'modcontractual_activa.observacions',
                 'modcontractual_activa.data_inici'
                 ]
            ).where(
                [('id', '=', pol_id)]
            )

            cursor.execute(*sql)

            mod_selected_info = cursor.dictfetchall()

            mod_current_observacions = mod_selected_info[0]['modcontractual_activa.observacions']
            fecha_inicio_mod = mod_selected_info[0]['modcontractual_activa.data_inici']
            mod_id = mod_selected_info[0]['modcontractual_activa.id']

            obs_params = [
                '/'.join(list(reversed(fecha.split('-'))))
                for fecha in
                [fecha_inicio_mod, old_modcon_fecha_final, data_final]
            ]
            new_observacions = (
                u'* Renovación tacita automatica a {0}:\n '
                u' - Fecha final anterior {1} -> Nueva fecha final {2}'
            ).format(*obs_params)

            new_observacions = u'{0}\n{1}'.format(
                (mod_current_observacions or ''), new_observacions
            )

            mod_vals = {
                'observacions': new_observacions
            }

            modcon_obj.write(cursor, uid, mod_id, mod_vals, context=context)

            # END UPDATE OBSERVACIONS

            return True

        # Search for a register with current contract
        q = OOQuery(rv_obj, cursor, uid)
        sql = q.select(['id']).where([
            ('polissa_id', '=', pol_id),
            ('active', '=', True),
        ])

        cursor.execute(*sql)

        regs = cursor.dictfetchall()

        reg_ids = [reg['id'] for reg in regs]

        if reg_ids:
            rv_obj.write(
                cursor, uid, reg_ids,
                {'active': False}, context=context
            )

        return False

    _columns = {
        'fact_potencia_100': fields.boolean(
            u'Fact. maxímetro 100%',
            readonly=True,
            states={
                'esborrany': [('readonly', False)],
                'validar': [('readonly', False)],
                'modcontractual': [('readonly', False)]
            }
        )
    }

    _defaults = {
        'fact_potencia_100': lambda *a: False
    }

    def _cnt_fact_maximetre(self, cursor, uid, ids, context=None):
        for polissa in self.browse(cursor, uid, ids, context=context):
            if polissa.facturacio_potencia == 'max':
                if (polissa.tarifa.name.startswith('3.1')
                    or polissa.tarifa.name.startswith('6.1')) \
                                and polissa.fact_potencia_100:
                    return False
        return True

    _constraints = [
        (
            _cnt_fact_maximetre,
            u"La facturación del 100% de potencia no puede "
            u"estar activada si la tarifa es 3.1 o 6.1",
            ['fact_potencia_100']
        ),
    ]

GiscedataPolissa()


class GiscedataPolissaModcontractual(osv.osv):
    _name = 'giscedata.polissa.modcontractual'
    _inherit = 'giscedata.polissa.modcontractual'

    def renovar(self, cursor, uid, ids, context=None):
        """
        Extensión del metodo de renovacion usado en el cron de renovaciones
        para hacer automaticamente los cambios de listas de precios
        tambien automaticamente.

        Renovem el contracte amb els valors actuals de la pòlissa.
        """

        pol_obj = self.pool.get('giscedata.polissa')

        for mod_data in self.read(cursor, uid, ids, ['polissa_id'], context=context):
            polissa_id = mod_data['polissa_id'][0]
            renovat = pol_obj.renovacio_tacita(cursor, uid, polissa_id, context=context)

            if not renovat:
                super(GiscedataPolissaModcontractual, self).renovar(
                    cursor, uid, [mod_data['id']], context=context
                )

        return True

    _columns = {
        'fact_potencia_100': fields.boolean(u'Fact. maxímetro 100%')
    }

    _defaults = {
        'fact_potencia_100': lambda *a: False
    }


GiscedataPolissaModcontractual()
