# -*- coding: utf-8 -*-

from osv import osv, fields
from osv.expression import OOQuery
from tools.translate import _


SECTIONS_ALLOWED = ['SAL_PENALIZABLE', 'SW_SALIENTE']


class WizardGetPenalty(osv.osv_memory):

    _name = "wizard.get.penalty"
    _columns = {
        'info': fields.text('Información', readonly=True)
    }

    def get_penalty(self, cursor, uid, ids, context=None):
        if len(ids) > 1:
            raise osv.except_osv(
                "Error", "El asistente solamente permite una instancia."
            )

        crm_o = self.pool.get('crm.case')
        policy_o = self.pool.get('giscedata.polissa')

        crm_ids = context['active_ids']
        crm_f = ['id', 'polissa_id.id', 'date', 'section_id.code', 'state']
        crm_dmn = [('id', 'in', crm_ids)]

        q = OOQuery(crm_o, cursor, uid).select(crm_f).where(crm_dmn)
        cursor.execute(*q)
        self_info = ""
        for crm_v in cursor.dictfetchall():
            section_c = crm_v['section_id.code']
            offset_date = crm_v['date']

            if section_c in SECTIONS_ALLOWED and offset_date:
                policy_id = crm_v['polissa_id.id']
                crm_id = crm_v['id']
                crm_state = crm_v['state']

                penalty = policy_o.get_penalty(
                    cursor, uid, policy_id, offset_date=offset_date,
                    context=context
                )[policy_id]

                if penalty['info']:
                    self_info += "Caso {}: {}\n".format(crm_id, penalty['info'])
                elif penalty['nullifiers_or_rectifiers']:
                    info = "Encontradas facturas rectificadoras o anuladoras."
                    self_info += "Caso {}: {}\n".format(crm_id, info)
                else:
                    description = self.format_penalty(penalty)

                    crm_wv = {
                        'planned_revenue': penalty['total_penalty'],
                        'description': description,
                    }
                    crm_o.write(cursor, uid, crm_id, crm_wv, context=context)

                    if crm_state == 'draft':
                        crm_o.case_open(cursor, uid, [crm_id])

            else:
                raise osv.except_osv(
                    _(u"Error"),
                    _(u"No se trata de una caso CRM saliente o saliente"
                      u"penalizable o bien no hay fecha para poder hacer el"
                      u"cálculo.")
                )
        if self_info:
            self.write(cursor, uid, ids, {'info': self_info}, context=context)
        else:
            return {}

    @staticmethod
    def format_penalty(penalty):
        invoices_consumption = ', '.join(penalty.get('invoices_consumption', ''))
        invoices_tariff = ', '.join(penalty.get('invoices_tariff', ''))

        price_lists = ""

        for period, price_list in penalty.get('price_lists', {'': ''}).items():
            price_lists += "\t{}: {}".format(period, price_list)

        periods_distribution = ""

        for period, distribution in penalty.get('periods_distribution', {'': 0.0}).items():
            periods_distribution += "\t{}: {}".format(period, distribution)

        penalties_by_period = ""

        for period, p in penalty.get('penalties_by_period', {'': 0.0}).items():
            penalties_by_period += "\t{}: {}".format(period, round(p, 2))

        res = (
            "Tipo de consumo: {}\n"
            "Consumo previsto: {} kWh\n"
            "Consumo efectuado: {} kWh\n"
            "Facturas utilizadas para calcular el consumo: {}\n"
            "Tarifa: {}\n"
            "Facturas con la tarifa utilizada: {}\n"
            "Lista de precios por periodo:\n{}\n"
            "Distribución del consumo por periodo:\n{}\n"
            "Proporción: {}\n"
            "Total kWh penalizables: {}\n"
            "Penalización por periodo (€):\n{}\n"
            "-----------------------------------------------------\n"
            "Penalización total: {} €\n"
            "".format(
                penalty['planned_consumption_type'],
                penalty['planned_consumption'],
                penalty['consumption'],
                invoices_consumption,
                penalty.get('tariff', 'Error'),
                invoices_tariff,
                price_lists,
                periods_distribution,
                penalty.get('proporcion', ''),
                penalty.get('punishable_kwh', -1),
                penalties_by_period,
                round(penalty['total_penalty'], 2)
            )
        )

        return res


WizardGetPenalty()
