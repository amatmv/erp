# -*- coding: utf-8 -*-

from datetime import date, datetime, timedelta
from dateutil.relativedelta import relativedelta
from osv import osv
from osv.expression import OOQuery


class CalcularRegistrosVencimiento(osv.osv_memory):
    _name = 'calcular.registros.vencimiento'
    _description = 'Funciones para el calculo de registros de vencimiento'

    def buscar_contratos_vencidos(self, cursor, uid, context=None):
        cfg_obj = self.pool.get('res.config')
        lim_days = int(
            cfg_obj.get(cursor, uid, 'dias_acotamiento_vencimiento', '60')
        )

        current_date = datetime.strftime(date.today(), "%Y-%m-%d")

        sql = '''
        SELECT p.id, m.data_final FROM giscedata_polissa AS p
        LEFT JOIN giscedata_polissa_modcontractual AS m
        ON (m.id = p.modcontractual_activa)
        WHERE Date(m.data_final) = Date(%s) +  interval '%s days'
        AND p.state not in  ('baixa','cancelada')
        '''
        cursor.execute(sql, (current_date, lim_days))

        contratos = cursor.dictfetchall()

        return [i['id'] for i in contratos]

    def crear_registros_vencimiento(self, cursor, uid, pol_ids, context=None):

        reg_obj = self.pool.get('registro.vencimiento')
        pol_obj = self.pool.get('giscedata.polissa')

        nuevos_registos = []
        registros_actualizados = []

        if not pol_ids:
            return nuevos_registos, registros_actualizados

        if not isinstance(pol_ids, (list, tuple)):
            pol_ids = [pol_ids]

        q = OOQuery(pol_obj, cursor, uid)
        sql = q.select(['id', 'llista_preu',
                        'modcontractual_activa.data_final',
                        'modcontractual_activa.llista_preu']).where([
                         ('id', 'in', pol_ids)
                        ])

        cursor.execute(*sql)

        polisses = cursor.dictfetchall()
        # Convert list of dicts to dict of dicts with polissa id by key
        polisses = {d["id"]: d for d in polisses}

        # q = OOQuery(reg_obj, cursor, uid)
        # sql = q.select(['id', 'polissa_id', 'llista_preus_id',
        #                 'data_venciment', 'active']
        #                ).where([
        #                 ('llista_preus_id', '=', Column('polissa_id.modcontractual_activa.llista_preu.id')),
        #                 ('polissa_id', 'in', pol_ids)
        #                 ])
        sql = '''SELECT rv.id, rv.polissa_id, rv.llista_preus_id, rv.data_venciment, rv.active 
                  FROM registro_vencimiento AS rv
                  LEFT JOIN giscedata_polissa_modcontractual AS mod ON rv.polissa_id = mod.polissa_id
                  WHERE mod.llista_preu = rv.llista_preus_id AND rv.active = true AND rv.polissa_id in %s and mod.active = true'''
        cursor.execute(sql, (tuple(pol_ids),))

        registros = cursor.dictfetchall()

        # get polissas in registers
        polisses_registros_ids = [p['polissa_id'] for p in registros]

        polisses_sin_registro_ids = list(set(pol_ids) - set(polisses_registros_ids))

        # Crear nuevos registros para las polizas sin registro

        for pol_id in polisses_sin_registro_ids:
            pol_fields = polisses[pol_id]

            reg_obj.create(cursor, uid, {'polissa_id': pol_id,
                                         'llista_preus_id': pol_fields['modcontractual_activa.llista_preu'],
                                         'data_venciment': pol_fields['modcontractual_activa.data_final']
                                        })
            nuevos_registos.append((pol_id, pol_fields['modcontractual_activa.llista_preu'], pol_fields['modcontractual_activa.data_final']))

        # Modifica registros ya existentes

        for reg in registros:
            pol_fields = polisses[reg['polissa_id']]
            reg_obj.write(
                cursor, uid, reg['id'],
                {'data_venciment': pol_fields['modcontractual_activa.data_final']},
                context=context
            )
            registros_actualizados.append((reg['polissa_id'], pol_fields['modcontractual_activa.llista_preu'], pol_fields['modcontractual_activa.data_final']))

        return nuevos_registos, registros_actualizados

CalcularRegistrosVencimiento()
