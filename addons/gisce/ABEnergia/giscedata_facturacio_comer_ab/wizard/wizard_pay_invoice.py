# -*- coding: utf-8 -*-
import logging

from osv import osv


class WizardPayInvoice(osv.osv_memory):
    """Wizard class
    """
    _name = "facturacio.pay.invoice"
    _inherit = 'facturacio.pay.invoice'

    def _default_cal_reposicio(self, cursor, uid, context=None):
        '''
        Originalment es mira si la pòlissa estava en tall.
        Si ho està, s'indicava que cal reposició.
        Indicant que cal reposicio, l'assistent de pagar factura passa per un
        estat intermig (state 'reposicio') on hi ha la opció de generar factura
        de reconexió.
        Volem que deixi d'existir la opció de generar factura de reconexió
        anticipadament a la de Distribuidora, per això marquem per defecte
        que no cal reposició i comentem la funció generar_reposicio
        :param cursor:
        :param uid:
        :param context:
        :return:
        '''
        return 0

    # def generar_reposicio(self, cursor, uid, ids, context=None):
    #     """Acció per generar la factura de reposició
    #     """
    #     logger = logging.getLogger('openerp.{}.generar_reposicio'.format(
    #         __name__
    #     ))
    #     if context is None:
    #         context = {}
    #     res = super(WizardPayInvoice, self).generar_reposicio(
    #         cursor, uid, ids, context=context
    #     )
    #     wiz = self.browse(cursor, uid, ids[0], context=context)
    #     # Get generated invocies
    #     factures_ids = res['domain'][0][2]
    #     # Append first id for the third report
    #     factures_ids.append(factures_ids[0])
    #     if wiz.reposicio_agrupar:
    #         # Fem els reports i passem a l'estat pagar
    #         logger.info('Generant report agrupat per ids: {0}'.format(
    #             factures_ids
    #         ))
    #         return {
    #             'type': 'ir.actions.report.xml',
    #             'report_name': 'reposicio.agrupada',
    #             'datas': {'ids': factures_ids}
    #         }
    #     return res


WizardPayInvoice()

