# -*- coding: utf-8 -*-
from osv import osv
from tools.translate import _
from giscedata_facturacio_comer.wizard.wizard_informes_facturacio_comer import (
    INFORMES_CSV
)
from addons import get_module_resource


NEW_INFORMES_CSV = {
    'export_factures_desglossat': {
         'header': [
             _(u"Distribuidora"),
             _(u"Ref. Distribuidora"),
             _(u"Comercialitzadora"),
             _(u"Cod. Comer."),
             _(u'Contracte'),
             _(u'Tipo pago'),
             _(u'Comercial'),
             _(u'Nom client'),
             _(u"CUPS"),
             _(u"Direcció"),
             _(u"CP"),
             _(u"Tarifa d'acces"),
             _(u"factura"),
             _(u"origen"),
             _(u"remesa"),
             _(u"Data factura"),
             _(u"Estado"),
             _(u"tipus factura"),
             _(u"Data inicial"),
             _(u"Data final"),
             _(u"núm. díes"),
             _(u"vat"),
             _(u"Consumo P1"),
             _(u"Consumo P2"),
             _(u"Consumo P3"),
             _(u"Consumo P4"),
             _(u"Consumo P5"),
             _(u"Consumo P6"),
             _(u"energia_kw"),
             _(u"potencia"),
             _(u"energia"),
             _(u"reactiva"),
             _(u"altres conceptes"),
             _(u"altres sense refacturacio"),
             _(u"refacturacio"),
             _(u"refacturacio_t4"),
             _(u"refacturacio_t1"),
             _(u"lloguer"),
             _(u"total conceptes"),
             _(u"iva1"),
             _(u"Base IVA1"),
             _(u"total iva1"),
             _(u"iva2"),
             _(u"Base IVA2"),
             _(u"total iva2"),
             _(u"Base IESE IVA1"),
             _(u"total IESE IVA1"),
             _(u"Base IESE IVA2"),
             _(u"total IESE IVA2"),
             _(u"Base IESE"),
             _(u"total IESE"),
             _(u"base"),
             _(u"iva"),
             _(u"total"),
             _(u"Banco")],
         'query': get_module_resource(
            'giscedata_facturacio_comer_ab', 'sql', 'query_resum_comer.sql'
         )
    }
}


class WizardInformesFacturacioComer(osv.osv_memory):

    _name = "wizard.informes.facturacio"

    _inherit = "wizard.informes.facturacio"

    INFORMES_CSV.update(NEW_INFORMES_CSV)

    _informes_csv = INFORMES_CSV

WizardInformesFacturacioComer()
