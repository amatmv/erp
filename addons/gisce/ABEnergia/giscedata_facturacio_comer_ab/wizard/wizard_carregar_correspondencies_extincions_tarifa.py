# -*- coding: utf-8 -*-

from osv import osv, fields
from StringIO import StringIO
from datetime import datetime
import base64
import csv

class UploadCorrespondenciesExtincionsTarifa(osv.osv_memory):
    """Assistent que puja les correspondències de cada tarifa."""

    _name = 'wizard.carregar.correspondencies.extincions.tarifa'

    def action_upload_correspondencies(self, cursor, uid, ids, context=None):
        """Carrega les correpondencies a la base de dades"""

        if context is None:
            context = {}

        wizard = self.browse(cursor, uid, ids[0], context=context)
        polissa_obj = self.pool.get('giscedata.polissa')
        pricelist_obj = self.pool.get('product.pricelist')
        correspondencies_obj = self.pool.get('correspondencies.extincions.tarifa')

        fitxer = StringIO(base64.b64decode(wizard.fitxer_correspondencies))
        reader = csv.reader(fitxer, delimiter=';')
        for linia in reader:
            if linia not in [['', ''], []]:  #Evitem les línies buides i espais
                tarifaorigen = linia[0]
                tarifadesti = linia[1]
                contracte = False
                if len(linia) == 3:
                    contracte = linia[2]

                if contracte:
                    id_pol = polissa_obj.search(cursor, uid,
                                                [('name', '=', contracte),
                                                 ('active', '=', 't')])
                    if not id_pol:
                        raise osv.except_osv("Atención",
                                             "Contrato '{0}' no encontrado.".
                                         format(contracte))

                #Evitem problemes de tarifes inventades/errònies
                try:
                    id_origen = pricelist_obj.search(cursor, uid,
                                                   [('name', '=', tarifaorigen),
                                                    ('type', '=', 'sale')])
                    id_desti = pricelist_obj.search(cursor, uid,
                                                   [('name', '=', tarifadesti),
                                                    ('type', '=', 'sale')])
                except IndexError:
                    raise osv.except_osv("Tarifas de venta incorrectas.",
                                         "Revisa que las tarifas estén bien "
                                         "introducidas y no haya espacios. "
                                         "Problema en la linea:\n{0}".format(
                                            tarifaorigen + ' -> ' + tarifadesti))

                if len(id_origen) != 1:
                    raise osv.except_osv("Atención",
                                         "Tarifa origen {0} incorrecta.".
                                         format(tarifaorigen))
                if len(id_desti) != 1:
                    raise osv.except_osv("Atención",
                                         "Tarifa destino {0} incorrecta.".
                                         format(tarifadesti))
                id_origen = id_origen[0]
                id_desti = id_desti[0]

                params = [('tarifaorigen', '=', id_origen),
                          ('tarifadesti', '=', id_desti),
                          ('data', '=', datetime.now().strftime("%Y-%m-%d")),
                          ]
                if contracte and id_pol:
                    params.append(('polissa_id', '=', id_pol))
                else:
                    params.append(('polissa_id', '=', False))
                #Si ja hi ha una correspondencia igual, no la tenim en compte
                if correspondencies_obj.search(cursor, uid, params):
                    continue

                vals = {
                    'tarifaorigen': id_origen,
                    'tarifadesti': id_desti,
                    'data': datetime.now().strftime("%Y-%m-%d"),
                }
                if contracte and id_pol:
                    vals.update({'polissa_id': id_pol[0]})
                correspondencies_obj.create(cursor, uid, vals)

        wizard.write({'state': 'end'})
        return

    def get_default_info(self, cursor, uid, context=None):
        if context is None:
            context = {}
        return ("Introduce un fichero csv, con las correspondencias de "
                "las extinciones de tarifa. El formato tiene que ser el "
                "siguiente:\ntarifaorigen;tarifadestino\ntarifaorigen;tarifa"
                "destino\n...\n"
                "Para crear una correspondencia para un contrato concreto, "
                "indicarlo al final de la línea con formato:\n"
                "tarifaorigen;tarifadestino;contrato\n")

    _columns = {
        'info': fields.text('Información', readonly=True),
        'fitxer_correspondencies': fields.binary('Fichero con las '
                                                 'correspondencias:'),
        'state': fields.selection([('init', 'Init'), ('end', 'End')], 'State'),
    }

    _defaults = {
        'info': get_default_info,
        'state': lambda *a: 'init',
    }

UploadCorrespondenciesExtincionsTarifa()
