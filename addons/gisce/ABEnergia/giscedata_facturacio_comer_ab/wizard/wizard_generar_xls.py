# -*- coding: utf-8 -*-

from osv import osv, fields
from osv.osv import except_osv
import base64
from datetime import datetime


class WizardGenerarXls(osv.osv_memory):
    _name = 'wizard.generar.xls'

    def obtenir_impost(self, factura, context=None):
        res = {}
        res['impost'] = 0
        res['iva'] = 0
        for l in factura.tax_line:
            if 'IVA' not in l.name:
                res['impost'] = l.amount
            else:
                res['iva'] = l.amount
        return res

    def obtenir_potencia(self, factura, periode, context=None):
        res = {}
        res['potencia_contractada'] = 0
        res['potencia_demandada'] = 0
        res['exces'] = 0
        tarifa = factura.tarifa_acces_id.name
        tarifes = {
            'P1': ['P1'], 'P2': ['P2'], 'P3': ['P3'],
            'P4': ['P4'], 'P5': ['P5'], 'P6': ['P6']
        }
        if '3.' in tarifa:
            tarifes['P1'].append('P4')
            tarifes['P2'].append('P5')
            tarifes['P3'].append('P6')
            del tarifes['P4']
            del tarifes['P5']
            del tarifes['P6']
        for linia in factura.lectures_potencia_ids:
            if linia.name in tarifes[periode]:
                if linia.name in periode:
                    res['potencia_contractada'] = linia.pot_contract
                if res['potencia_demandada']:
                    res['potencia_demandada'] = max(
                        res['potencia_demandada'], linia.pot_maximetre)
                else:
                    res['potencia_demandada'] = linia.pot_maximetre
                res['exces'] = linia.exces
        return res

    def obtenir_reactiva(self, factura, periode, context=None):
        res = {}
        tarifa = factura.tarifa_acces_id.name
        tarifes = {
            'P1': ['P1'], 'P2': ['P2'], 'P3': ['P3'],
            'P4': ['P4'], 'P5': ['P5'], 'P6': ['P6']
        }
        if '3.' in tarifa:
            tarifes['P1'].append('P4')
            tarifes['P2'].append('P5')
            tarifes['P3'].append('P6')
            del tarifes['P4']
            del tarifes['P5']
            del tarifes['P6']
        res['reactiva_consum'] = 0
        linies_reactiva = [x for x in factura.lectures_energia_ids
                                if x.tipus =='reactiva']
        for linia in linies_reactiva:
            if linia.name[-3:-1] in tarifes[periode]:
                res['reactiva_consum'] += linia.consum
        return res

    def obtenir_energia(self, factura, periode, tipus, context=None):
        res = {}
        quantitat = 0
        preu_kw = 0
        subtotal = 0
        descompte = 0
        res['quantitat'] = 0
        res['preu_kw'] = 0
        res['subtotal'] = 0
        res['descompte'] = 0
        lectures = []
        for linia in factura.linia_ids:
            if ((linia.tipus == 'energia' and tipus == 'activa') or
                    (linia.tipus == 'reactiva' and tipus == 'reactiva') or
                    (linia.tipus == 'potencia' and tipus == 'potencia') or
                    (linia.tipus == 'lloguer' and tipus == 'lloguer')) \
                    and periode in linia.name:
                if not linia.isdiscount:
                    quantitat += linia.quantity
                    preu_kw += linia.price_unit_multi
                    subtotal += linia.price_subtotal
                    lectures.append(linia)
                else:
                    descompte = linia.discount

        if lectures:
            res['quantitat'] = quantitat
            if len(lectures) > 1:
                if quantitat:
                    res['preu_kw'] = round(subtotal / quantitat, 6)
                else:
                    res['preu_kw'] = round(preu_kw / len(lectures), 6)
            else:
                res['preu_kw'] = preu_kw
            res['subtotal'] = subtotal
            res['descompte'] = round(descompte, 2)
        return res

    def get_exces_potencia(self, factura):
        linies = []
        for linia in factura.linia_ids:
            if linia.tipus == 'exces_potencia':
                linies.append(linia)
        return linies


    def calcular_descompte(self, energies, context=None):
        descompte_total = 0
        for energia in energies:
            descompte_total += energia['descompte'] * (
                energia['quantitat'] * energia['preu_kw']
            )
        return descompte_total

    def generate_xls(self, cursor, uid, ids, context=None):
        wiz = self.browse(cursor, uid, ids[0], context=context)
        # Generar XLS
        try:
            from xlwt import Workbook, easyxf, XFStyle
        except ImportError:
            raise except_osv("Error", "You must intsall xlutils:\n"
                             "pip install xlutils")
        w = Workbook(encoding='utf-8')
        ws = w.add_sheet('CONTROL')
        wr = w.add_sheet('RESUMEN')
        style = easyxf(
          'align: vertical center, horizontal center;'
          'font: colour black, bold True;'
          'pattern: pattern solid, fore_colour lime;'
        )
        # Busquem els IDs de totes les factures agrupades
        factures = []
        fact_act_id = context['active_id']
        fact_obj = self.pool.get('giscedata.facturacio.factura')
        fact = fact_obj.browse(cursor, uid, fact_act_id, context)
        if fact.group_move_id:
            for l in fact.group_move_id.line_id:
                if l.invoice:
                    id_fact = fact_obj.search(
                            cursor, uid, [('invoice_id', '=', l.invoice.id)]
                    )[0]
                    factures.append(id_fact)
            if factures:
                agrupades = fact_obj.browse(cursor, uid, factures)
            dict_fact = {}

            for elem in agrupades:
                if elem.fiscal_position.id not in dict_fact.keys():
                    dict_fact[elem.fiscal_position.id] = []
                dict_fact[elem.fiscal_position.id].append(elem)

            # Ja tenim totes les factures agrupades, generem l'XLS
            wr.write_merge(0, 0, 0, 2, fact.partner_id.name, style)
            wr.write(0, 3, 'CIF', style)
            wr.write(0, 4, fact.partner_id.vat.replace('ES', ''), style)
            wr.write_merge(1, 1, 0, 2, 'FECHA FRA: '+fact.date_invoice, style)
            wr.write_merge(1, 1, 3, 4, '', style)
            wr.write(5, 0, 'REFERENCIA', style)
            wr.write(5, 1, 'NUMEROFACTURA', style)
            wr.write(5, 2, 'IMPORTE_SIN_IVA_e', style)
            wr.write(5, 3, 'IVA_e', style)
            wr.write(5, 4, 'TOTAL_IMPORTE_e', style)
            sin_iva_pen = 0
            iva_pen = 0
            total_pen = 0
            headers = [
                'CIF',
                'SOCIEDAD',
                'CUPS',
                'DIRECCION',
                'MUNICIPIO',
                'REFERENCIA',
                'NUMERO_FACTURA',
                'FECHA_FACTURA',
                'DESDE',
                'HASTA',
                'TARIFA',
                'ACT_P1_KWh',
                'ACT_P2_KWh',
                'ACT_P3_KWh',
                'TOTAL_ACTIVA_KWh',
                'ACT_P1_eKw',
                'ACT_P2_eKw',
                'ACT_P3_eKw',
                'ACT_P4_eKw',
                'ACT_P5_eKw',
                'ACT_P6_eKw',
                'TOTAL_ACTIVA_e',
                'TOTAL_DESCUENTO',
                'REACT_P1_KWh',
                'REACT_P2_KWh',
                'REACT_P3_KWh',
                'TOTAL_REACT_KWh',
                'TOTAL_REACT_e',
                'POT_CONTRATADA_P1_KW',
                'POT_CONTRATADA_P2_KW',
                'POT_CONTRATADA_P3_KW',
                'POT_DEM_P1_KW',
                'POT_DEM_P2_KW',
                'POT_DEM_P3_KW',
                'EXCESO_P1_KW',
                'EXCESO_P2_KW',
                'EXCESO_P3_KW',
                'POTENCIA_P1_eKw',
                'POTENCIA_P2_eKw',
                'POTENCIA_P3_eKw',
                'POTENCIA_P4_eKw',
                'POTENCIA_P5_eKw',
                'POTENCIA_P6_eKw',
                'TOTAL_POTENCIA_e',
                'TOTAL_EXCESO_POT_e',
                'TOTAL_POTENCIAS_e',
                'IMPUESTO_e',
                'ALQUILER_e',
                'IMPORTE_SIN_IVA_e',
                'IVA_e',
                'TOTAL_IMPORTE_e'
            ]
            for idx, header in enumerate(headers):
                ws.write(0, idx, header, style)
            col = 0
            iteracio = 0
            for llista in dict_fact.values():
                for elem in llista:
                    col += 1
                    ws.write(col, 0, elem.partner_id.vat.replace('ES', ''))
                    ws.write(col, 1, elem.partner_id.name)
                    ws.write(col, 2, elem.cups_id.name)
                    ws.write(col, 3, elem.cups_id.direccio)
                    ws.write(col, 4, elem.cups_id.id_municipi.name)
                    ws.write(col, 5, elem.polissa_id.name)
                    wr.write(col + 5 + iteracio, 0, elem.polissa_id.name)
                    ws.write(col, 6, elem.number)
                    wr.write(col + 5 + iteracio, 1, elem.number)
                    date_inv = datetime.strptime(elem.date_invoice, '%Y-%m-%d')
                    ws.write(col, 7, date_inv.strftime('%d-%m-%Y'))
                    date_from = datetime.strptime(elem.data_inici, '%Y-%m-%d')
                    ws.write(col, 8, date_from.strftime('%d-%m-%Y'))
                    date_to = datetime.strptime(elem.data_final, '%Y-%m-%d')
                    ws.write(col, 9, date_to.strftime('%d-%m-%Y'))
                    ws.write(col, 10, elem.tarifa_acces_id.name)
                    act_p1_kwh = self.obtenir_energia(elem, 'P1', 'activa')
                    act_p2_kwh = self.obtenir_energia(elem, 'P2', 'activa')
                    act_p3_kwh = self.obtenir_energia(elem, 'P3', 'activa')
                    act_p4_kwh = self.obtenir_energia(elem, 'P4', 'activa')
                    act_p5_kwh = self.obtenir_energia(elem, 'P5', 'activa')
                    act_p6_kwh = self.obtenir_energia(elem, 'P6', 'activa')
                    ws.write(col, 11, act_p1_kwh['quantitat'])
                    ws.write(col, 12, act_p2_kwh['quantitat'])
                    ws.write(col, 13, act_p3_kwh['quantitat'])
                    total_act_kwh = act_p1_kwh['quantitat'] + act_p2_kwh[
                        'quantitat'] + act_p3_kwh['quantitat']
                    ws.write(col, 14, total_act_kwh)
                    ws.write(col, 15, act_p1_kwh['preu_kw'])
                    ws.write(col, 16, act_p2_kwh['preu_kw'])
                    ws.write(col, 17, act_p3_kwh['preu_kw'])
                    ws.write(col, 18, act_p4_kwh['preu_kw'])
                    ws.write(col, 19, act_p5_kwh['preu_kw'])
                    ws.write(col, 20, act_p6_kwh['preu_kw'])
                    total_activa_preu = act_p1_kwh['subtotal'] \
                                        + act_p2_kwh['subtotal'] \
                                        + act_p3_kwh['subtotal'] \
                                        + act_p4_kwh['subtotal'] \
                                        + act_p5_kwh['subtotal'] \
                                        + act_p6_kwh['subtotal']
                    ws.write(col, 21, total_activa_preu)
                    energies = []
                    energies.append(act_p1_kwh)
                    energies.append(act_p2_kwh)
                    energies.append(act_p3_kwh)
                    descompte_total = self.calcular_descompte(energies)
                    ws.write(col, 22, descompte_total)
                    react_p1_kwh = self.obtenir_reactiva(elem, 'P1')
                    react_p2_kwh = self.obtenir_reactiva(elem, 'P2')
                    react_p3_kwh = self.obtenir_reactiva(elem, 'P3')
                    ws.write(col, 23, react_p1_kwh['reactiva_consum'])
                    ws.write(col, 24, react_p2_kwh['reactiva_consum'])
                    ws.write(col, 25, react_p3_kwh['reactiva_consum'])
                    total_react_kwh = round(
                        sum(
                            [x.quantity for x in elem.linia_ids
                                if x.tipus == 'reactiva']
                        )
                        , 2
                    )
                    ws.write(col, 26, total_react_kwh)
                    total_react_preu = round(
                        sum(
                            [x.price_subtotal for x in elem.linia_ids
                                if x.tipus == 'reactiva']
                        )
                        , 2
                    )
                    ws.write(col, 27, total_react_preu)
                    p1_pot = self.obtenir_energia(elem, 'P1', 'potencia')
                    p2_pot = self.obtenir_energia(elem, 'P2', 'potencia')
                    p3_pot = self.obtenir_energia(elem, 'P3', 'potencia')
                    p4_pot = self.obtenir_energia(elem, 'P4', 'potencia')
                    p5_pot = self.obtenir_energia(elem, 'P5', 'potencia')
                    p6_pot = self.obtenir_energia(elem, 'P6', 'potencia')
                    p1_pot_lect = self.obtenir_potencia(elem, 'P1')
                    p2_pot_lect = self.obtenir_potencia(elem, 'P2')
                    p3_pot_lect = self.obtenir_potencia(elem, 'P3')
                    ws.write(col, 28, p1_pot_lect['potencia_contractada'])
                    ws.write(col, 29, p2_pot_lect['potencia_contractada'])
                    ws.write(col, 30, p3_pot_lect['potencia_contractada'])
                    ws.write(col, 31, p1_pot_lect['potencia_demandada'])
                    ws.write(col, 32, p2_pot_lect['potencia_demandada'])
                    ws.write(col, 33, p3_pot_lect['potencia_demandada'])
                    ws.write(col, 34, p1_pot_lect['exces'])
                    ws.write(col, 35, p2_pot_lect['exces'])
                    ws.write(col, 36, p3_pot_lect['exces'])
                    ws.write(col, 37, p1_pot['preu_kw'])
                    ws.write(col, 38, p2_pot['preu_kw'])
                    ws.write(col, 39, p3_pot['preu_kw'])
                    ws.write(col, 40, p4_pot['preu_kw'])
                    ws.write(col, 41, p5_pot['preu_kw'])
                    ws.write(col, 42, p6_pot['preu_kw'])
                    total_potencia_preu = p1_pot['subtotal'] + \
                                          p2_pot['subtotal'] + \
                                          p3_pot['subtotal'] + \
                                          p4_pot['subtotal'] + \
                                          p5_pot['subtotal'] + \
                                          p6_pot['subtotal']
                    ws.write(col, 43, total_potencia_preu)
                    l_exces = self.get_exces_potencia(elem)
                    total_exces = sum([l.quantity for l in l_exces])
                    ws.write(col, 44, total_exces)
                    ws.write(col, 45, p6_pot['preu_kw'])
                    imp = self.obtenir_impost(elem)
                    ws.write(col, 46, imp['impost'])
                    lloguer = self.obtenir_energia(elem, 'ALQ', 'lloguer')
                    ws.write(col, 47, lloguer['subtotal'])

                    #Importe sin iva, iva e importe total en CONTROL
                    #Cambiamos los signos si son de reembolso
                    if elem.type == 'out_refund':
                        importe = - abs(elem.amount_untaxed + imp['impost'])
                        iva = - abs(imp['iva'])
                        importeconiva = - abs(elem.amount_total)
                    else:
                        importe = abs(elem.amount_untaxed + imp['impost'])
                        iva = abs(imp['iva'])
                        importeconiva = abs(elem.amount_total)

                    ws.write(col, 48, importe)
                    ws.write(col, 49, iva)
                    ws.write(col, 50, importeconiva)

                    # Importe sin iva, iva e importe total en RESUMEN
                    wr.write(col + 5 + iteracio, 2, importe)
                    wr.write(col + 5 + iteracio, 3, iva)
                    wr.write(col + 5 + iteracio, 4, importeconiva)

                    #Acumulaciones para hacer el resumen
                    sin_iva_pen += importe
                    iva_pen += iva
                    total_pen += importeconiva

                wr.write_merge(col + 6 + iteracio, col + 6 + iteracio, 0, 1,
                               llista[0].fiscal_position.name, style)
                wr.write(col + 6 + iteracio, 2, sin_iva_pen, style)
                wr.write(col + 6 + iteracio, 3, iva_pen, style)
                wr.write(col + 6 + iteracio, 4, total_pen, style)
                sin_iva_pen = 0
                iva_pen = 0
                total_pen = 0
                iteracio += 1
            wr.set_panes_frozen(True)
            wr.set_horz_split_pos(6)
            dt = datetime.strptime(str(datetime.now()), '%Y-%m-%d %H:%M:%S.%f')
            dt = dt.strftime('%d-%m-%Y')
            filename = 'multipunto_'+dt+'.xls'
            w.save('/tmp/'+filename)
            f = open('/tmp/'+filename, 'r')
            xls_file = base64.b64encode(f.read())
            f.close()
        else:
            filename = ''
            xls_file = ''

        wiz.write({
            'state': 'done',
            'file_out': xls_file,
            'nom_fitxer': filename
        })

    _columns = {
        'state': fields.char('Estado', size=16),
        'nom_fitxer': fields.char('Nom fitxer', size=16),
        'file_out': fields.binary('Resultado')
    }

    _defaults = {
        'state': lambda *a: 'init',
    }

WizardGenerarXls()
