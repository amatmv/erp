# -*- coding: utf-8 -*-
import types
import logging

from datetime import datetime
from dateutil.relativedelta import relativedelta
from collections import Counter

from osv import osv, fields
from tools.misc import cache
from tools import config, float_round

from workalendar.europe.spain import Spain


class GiscedataFacturacioSwitchingHelper(osv.osv):
    _name = 'giscedata.facturacio.switching.helper'
    _inherit = 'giscedata.facturacio.switching.helper'

    # def get_values_in_ext(self, cursor, uid, fact_id, fact_name, ori_lines,
    #                       context=None):
    #     """Comprovem si alguna de les línies és de reposició i és de la "nostra"
    #        distri
    #     """
    #     values = super(
    #         GiscedataFacturacioSwitchingHelper, self
    #     ).get_values_in_ext(cursor, uid, fact_id, fact_name, ori_lines, context)
    #
    #     imd_obj = self.pool.get('ir.model.data')
    #     comerdist_obj = self.pool.get('giscedata.comerdist.config')
    #     fact_obj = self.pool.get('giscedata.facturacio.factura')
    #
    #     con13_id = imd_obj.get_object_reference(
    #         cursor, uid, 'giscedata_facturacio_comer', 'concepte_13'
    #     )[1]
    #
    #     distris = []
    #     cfg_ids = comerdist_obj.search(cursor, uid, [])
    #     for config in comerdist_obj.read(cursor, uid, cfg_ids, ['partner_id']):
    #         distris.append(config['partner_id'][0])
    #
    #     fact_vals = fact_obj.read(cursor, uid, fact_id, ['partner_id'])
    #     partner_id = fact_vals['partner_id'][0]
    #     if partner_id in distris:
    #         new_values = []
    #         for vals in values:
    #             if vals['product_id'] != con13_id:
    #                 new_values.append(vals)
    #         return new_values
    #     else:
    #         return values
    #     return values

GiscedataFacturacioSwitchingHelper()


class GiscedataFacturacioFactura(osv.osv):

    _name = 'giscedata.facturacio.factura'
    _inherit = 'giscedata.facturacio.factura'

    def copy_data(self, cursor, uid, ids, default=None, context=None):
        """ No interesa que se copie la nota informativa """
        if default is None:
            default = {}

        data, x = super(GiscedataFacturacioFactura, self).copy_data(
            cursor, uid, ids, default, context
        )

        data.update({'nota_informativa': False})

        return data, x

    def check_if_gen_non_payment_case(self, cursor, uid, factura, context=None):
        pol_obj = self.pool.get('giscedata.polissa')
        bono_social_process = pol_obj.get_bono_social_process(cursor, uid)
        if factura.pending_state.process_id.id == bono_social_process:
            imd_obj = self.pool.get('ir.model.data')
            pendent_enviament_c1 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_bono_social',
                'carta_1_pendent_pending_state'
            )[1]
            return factura.pending_state.id == pendent_enviament_c1
        return super(
            GiscedataFacturacioFactura, self
        ).check_if_gen_non_payment_case(cursor, uid, factura, context)

    def invoice_open(self, cursor, uid, ids, context=None):
        """Custom ab"""
        if not context:
            context = {}
        if not isinstance(ids, list):
            ids = [ids]

        original = self.read(cursor, uid, ids,
                             ['type',
                              'date_invoice',
                              'state',
                              'payment_type'
                              ],
                             context=context)[0]

        # si la factura ja ha estat oberta, no permetre obrir-la
        if original['state'] != 'draft':
            raise osv.except_osv('Error',
                                 'La factura id {0} ja està oberta'.format(ids))

        res = super(
            GiscedataFacturacioFactura, self
        ).invoice_open(cursor, uid, ids, context=context)

        vals = {}

        # Sets date due to day 20 current month if opened between 1 and 15.
        # If opened after, set it to 5 next month.
        # Mirem si s'ha d'aplicar la logica
        cfg_obj = self.pool.get('res.config')
        add_date_due = int(cfg_obj.get(cursor, uid, 'fact_date_due_invoice', '0'))

        # Search for payment type cobrador
        ptype_obj = self.pool.get('payment.type')
        cobrador_id = ptype_obj.search(cursor, uid,
                                       [('name', 'ilike', 'cobrador')],
                                       context=context)[0]
        original_payment_type = original['payment_type']

        if original_payment_type:
            original_payment_type = original_payment_type[0]

        if add_date_due and original_payment_type and original_payment_type == cobrador_id:
            avui = datetime.today()
            if avui.day >= 1 and avui.day <= 15:
                date_due = datetime(year=avui.year, month=avui.month, day=20)
            else:
                date_due = datetime(year=avui.year, month=avui.month, day=5)
                date_due = date_due + relativedelta(months=1)
            vals.update({'date_due': date_due})

        # If it's an installation coupling charges invoice, date_due should be
        # the same day and payment type should be 'gestion cobrador'
        if self.is_coupling_charges_invoice(cursor, uid, ids, context=context):
            date_due = datetime.today()

            vals.update({'date_due': date_due,
                         'payment_type': cobrador_id,
                         })

        # If it's a provider invoice, set date_due to origin date_invoice+15
        if original['type'] in ['in_invoice', 'in_refund']:
            date_inv = datetime.strptime(original['date_invoice'], "%Y-%m-%d")
            date_due = date_inv + relativedelta(days=15)
            vals.update({'date_due': date_due,
                         })

        self.write(cursor, uid, ids, vals)
        return res

    def is_coupling_charges_invoice(self, cursor, uid, ids, context=None):

        if not isinstance(ids, list):
            ids = [ids]

        # Check invoice journal
        journal = self.read(cursor, uid, ids, ['journal_id'])
        journal_id = journal[0]['journal_id'][0]
        jnl_obj = self.pool.get('account.journal')
        ids_jnl = jnl_obj.search(cursor, uid, [('name', 'like', 'contrataci')])

        # check invoice line, product should be coupling charge
        lin_obj = self.pool.get('giscedata.facturacio.factura.linia')
        line_id = lin_obj.search(cursor, uid, [('factura_id', '=', ids[0])])
        product = lin_obj.read(cursor, uid, line_id, ['product_id'])

        if len(product) == 1 and product[0]['product_id']:
            # les fres de reposició només tenen una línia
            # si la línia no està associada a un producte, no és de reposició
            producte = product[0]['product_id'][1].lower()
            if 'reposici' in producte and journal_id in ids_jnl:
               return True
        return False

    def check_if_generate_b1(self, cursor, uid, factura, atr_enabled, b1_from_invoices):
        imd_obj = self.pool.get('ir.model.data')
        bono_social_process_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio_comer_bono_social',
            'bono_social_pending_state_process'
        )[1]
        if factura.pending_state.process_id.id == bono_social_process_id:
            carta_tall_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_bono_social',
                'carta_avis_tall_pending_state'
            )[1]
            polissa = factura.polissa_id
            return (factura.pending_state.id == carta_tall_id and
                polissa.active and atr_enabled and b1_from_invoices)
        else:
            return super(GiscedataFacturacioFactura, self).check_if_generate_b1(
                cursor, uid, factura, atr_enabled, b1_from_invoices)

    def get_plantilla_text(self, cursor, uid, factura_id, context=None):
        if context is None:
            context = {}

        fact_params = ['amount_total', 'number', 'polissa_id']
        fact_reads = self.read(cursor, uid, factura_id, fact_params,
                               context=context)
        plantilla = (
            'Apreciado cliente,\n '
            'Desde ab energía le recordamos que tiene una deuda pendiente '
            'que asciende a {0} euros por la factura nº {1} de '
            'su contrato nº {2}.\n'
            'Rogamos procedan a regularizar la situación a la mayor '
            'brevedad posible.\n'
            'Un saludo,\n ab energía'
        ).format(
            fact_reads['amount_total'],
            fact_reads['number'],
            fact_reads['polissa_id'][1],
        )

        return plantilla

    def get_cutoff_limit_date(self, date_limit, num_days=-1):
        origin_date = datetime.strptime(date_limit, '%Y-%m-%d')
        calendar = Spain()
        new_date = calendar.add_working_days(origin_date, delta=num_days)
        return new_date.strftime('%Y-%m-%d')


    @cache(timeout=10)
    def _fnc_per_enviar(self, cursor, uid, ids, field_name, args,
                        context=None):
        """Retorna si una factura està marcada per enviar o no. Originalment
        es marcava com a pendent d'enviar si la data final del periode facturat
        quedava dins d'una modificacio contractual que tingués enviament per
        email.
        Aquí fem que segons l'estat actual de la pòlissa es marqui com a
        pendent o no sense tenir en compte el periode facturat.
        """
        res = dict([(x, 'postal') for x in ids])
        cursor.execute("""
            select
              f.id,
              p.enviament
            from
              giscedata_polissa p,
              giscedata_facturacio_factura f
            where
              p.id = f.polissa_id and
              f.id in %s
        """, (tuple(ids), ))
        res.update(dict([(x[0], x[1]) for x in cursor.fetchall()]))
        return res

    def onchange_polissa(self, cursor, uid, ids, polissa_id, type_,
                         context=None):
        """
        Volem que el camp name, que ara s'informa amb el numero de contracte,
        informi del grup de facturació i el nif del titular, per facilitar
        operativa de facturació de certs multipunts
        """
        if context is None:
            context = {}
        res = super(GiscedataFacturacioFactura, self).onchange_polissa(
            cursor, uid, ids, polissa_id, type_,
            context=context)
        if type_ in ('out_invoice', 'out_refund'):
            if polissa_id:
                polissa = self.pool.get('giscedata.polissa').browse(cursor, uid,
                                                                    polissa_id,
                                                                    context)
                res['value'].update({
                    'name': "{}, {}".format(polissa.group_id.name,
                                            polissa.pagador_nif)
                    })
        return res

    _columns = {
        'per_enviar': fields.function(
            _fnc_per_enviar,
            method=True,
            type='selection',
            selection=[('email', 'E-mail'), ('postal', 'Correu postal')],
            string='Tipus d\'enviament',
            store=True,
            select=2
        ),
        'nota_informativa': fields.text('Nota informativa'),
    }

GiscedataFacturacioFactura()


def maximetre_100(self, potencia_contractada, potencia_maximetre):
    interval = (potencia_maximetre / potencia_contractada) * 100
    if potencia_maximetre <= potencia_contractada:
        return potencia_contractada
    elif interval > 100 and interval < 105:
        return potencia_maximetre
    else:
        return potencia_maximetre + (2 * (potencia_maximetre - (1.05 * potencia_contractada)))


class GiscedataFacturacioFacturador(osv.osv):

    _name = 'giscedata.facturacio.facturador'
    _inherit = 'giscedata.facturacio.facturador'

    def _apply_variable_discount(self, cursor, uid, factura, context=None):
        llista_preu = factura.llista_preu
        discount_lines = {}
        energy_lines = {}
        energy_quantity = Counter()
        for linia in factura.linies_energia:
            key = '{}_{}'.format(linia.name, linia.quantity)
            if linia.isdiscount:
                discount_lines.setdefault(key, [])
                discount_lines[key].append(linia)
            else:
                energy_lines.setdefault(key, [])
                energy_lines[key].append(linia)
                energy_quantity[linia.name] += linia.quantity

        total_energy = sum([v for k, v in energy_quantity.items()])
        p2_energy = energy_quantity['P2']
        percent = 0
        if total_energy > 0:
            percent = round((p2_energy / total_energy) * 100.0)

        if percent >= llista_preu.p2_precent:
            llista_preu = factura.llista_preu
            new_discount = (llista_preu.conditional_discount)
            for key, lines in energy_lines.items():
                base_price = lines[0].price_unit_multi
                discount_price = ((-1 * base_price) +
                                  base_price *
                                  (1 - (new_discount) / 100.0))
                discount_price = float_round(discount_price,
                            int(config['price_accuracy']))
                for d_line in discount_lines[key]:
                    d_line.write(
                        {
                            'price_unit_multi': discount_price,
                            'note': "-{:.2f} % sobre {:6f}".format(llista_preu.conditional_discount, base_price)
                        }
                    )
            # Update invoice taxes
            factura_obj = self.pool.get('giscedata.facturacio.factura')
            factura_obj.button_reset_taxes(cursor, uid, [factura.id])

    def fact_via_lectures(self, cursor, uid, polissa_id, lot_id, context=None):
        """We override the method to update discount when pricelist is single_price"""
        if not context:
            context = {}

        factura_obj = self.pool.get('giscedata.facturacio.factura')

        invoices = super(GiscedataFacturacioFacturador, self). \
            fact_via_lectures(cursor, uid, polissa_id, lot_id, context)

        ctx = context.copy()

        self.procesar_facturas_notas_informativas(
            cursor, uid, invoices, context=context
        )

        for factura in factura_obj.browse(cursor, uid, invoices, context):
            if factura.journal_id.code in ('ENERGIA', 'ENERGIA.R'):
                if factura.llista_preu.variable_discount:
                    self._apply_variable_discount(cursor, uid, factura, context=context)

        return invoices

    def config_facturador(
            self, cursor, uid, facturador, polissa_id, context=None):
        pol_obj = self.pool.get('giscedata.polissa')
        data_mod = context['data_inici_periode_f']
        ctx = {'date': data_mod}
        polissa_values = pol_obj.read(
            cursor, uid, polissa_id, ['tarifa', 'fact_potencia_100',
                                      'facturacio_potencia'],
            context=ctx
        )
        super(GiscedataFacturacioFacturador, self).config_facturador(
            cursor, uid, facturador, polissa_id, context=context
        )
        tariff_name = polissa_values['tarifa'][1]
        valid_tariff = '3.1' not in tariff_name and '6.1' not in tariff_name

        if polissa_values['fact_potencia_100'] and valid_tariff and \
                polissa_values['facturacio_potencia'] == 'max':
            logger = logging.getLogger('openerp.config_facturador')
            facturador.maximetre_tarifa = types.MethodType(
                maximetre_100, facturador)
            logger.info(
                u"Monkey patch maximetre potencia for polissa id:{} "
                u"and date interval {} - {}".format(
                    polissa_id,
                    context['data_inici_periode_f'],
                    context['data_final_periode_f']
                )
            )
        return True

    def procesar_facturas_notas_informativas(self, cursor, uid, fact_ids, context=None):
        fact_obj = self.pool.get('giscedata.facturacio.factura')
        reg_obj = self.pool.get('registro.vencimiento')

        if context is None:
            context = {}
        if not isinstance(fact_ids, (list, tuple)):
            fact_ids = [fact_ids]

        journal_obj = self.pool.get('account.journal')

        journal_id = journal_obj.search(
            cursor, uid, [('code', '=', 'ENERGIA')]
        )[0]

        sql = '''
        SELECT f.id, f.polissa_id, f.data_final, f.llista_preu,
        rv.llista_preus_id, rv.data_venciment, rv.id AS reg_id
        FROM giscedata_facturacio_factura AS f
        LEFT JOIN registro_vencimiento AS rv ON rv.polissa_id = f.polissa_id
        RIGHT JOIN account_invoice AS i ON f.invoice_id = i.id
        WHERE rv.active = true AND f.id in %s
        AND i.journal_id = %s
        '''

        cursor.execute(sql, (tuple(fact_ids), journal_id,))

        fact_with_regs = cursor.dictfetchall()

        for fact in fact_with_regs:
            if fact['llista_preu'] == fact['llista_preus_id']:

                data_final = datetime.strptime(fact['data_final'], '%Y-%m-%d')
                data_venciment = datetime.strptime(fact['data_venciment'], '%Y-%m-%d')
                if data_final <= data_venciment:
                    nota1 = ("Le comunicamos que a partir de la fecha de "
                             "vencimiento de su contrato el próximo {0}, "
                             "con arreglo a lo previsto contractualmente y a "
                             "la normativa vigente, el precio de la tarifa que "
                             "tiene contratada en la actualidad podrá ser "
                             "actualizado. Si necesita más información, no dude "
                             "en ponerse en contacto con nosotros en nuestras "
                             "delegaciones comerciales o en el teléfono gratuito "
                             "de atención al cliente 900 1903 00.")
                    params = {
                        'nota_informativa': nota1.format(
                            datetime.strftime(data_venciment, "%d/%m/%y")
                        )
                    }

                    fact_obj.write(cursor, uid, fact['id'], params, context=context)
                else:
                    nota2 = ("Le comunicamos que en la siguiente factura, con "
                             "arreglo a lo previsto contractualmente y a la "
                             "normativa vigente, el precio de la tarifa que "
                             "tiene contratada en la actualidad será actualizado. "
                             "Si necesita más información, no dude en ponerse en "
                             "contacto con nosotros en nuestras delegaciones "
                             "comerciales o en el teléfono gratuito de atención al "
                             "cliente 900 1903 00.")
                    fact_obj.write(
                        cursor, uid, fact['id'], {'nota_informativa': nota2},
                        context=context
                    )

            else:
                reg_obj.write(
                    cursor, uid, fact['reg_id'],
                    {'active': False}, context=context
                )

GiscedataFacturacioFacturador()
