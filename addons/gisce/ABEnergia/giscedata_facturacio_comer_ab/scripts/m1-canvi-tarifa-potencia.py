# -*- encoding: utf-8 -*-
import click
import csv
from csv import reader
from erppeek import Client
from collections import namedtuple
from tqdm import tqdm
from datetime import datetime

effective_date = None


def clean_tel_number(tel_number):
    """
    Strips non-number chars from telephone number except "+"
    Copied function from giscedata_switching
    :param tel_number:
    :return:
    """
    res = {'tel': tel_number, 'pre': '', 'fuzzy': False}
    if not tel_number:
        return res
    tel_net = ''.join([c for c in tel_number if c.isdigit()])
    tel = tel_net
    pre = '34'
    fuzzy = False
    if len(tel_net) > 9:
        pre = tel_net[:-9][-2:]
        tel = tel_net[-9:]
        fuzzy = len(tel) + len(pre) < len(tel_net)

    res = {'tel': tel, 'pre': pre, 'fuzzy': fuzzy}
    return res


def create_m1_01_changing_tariffs_and_powers(c, policy_id):
    """
    2.0A → 2.0DHA
    2.1A → 2.1DHA
    :param c:
    :type c: erppeek client
    :param policy_id:
    :return:
    """
    def get_phone_number(partner_id):
        addr_ids = addr_obj.search(
            [
                ('partner_id', '=', partner_id),
                '|', ('phone', '!=', ''), ('mobile', '!=', '')
            ]
        )
        phone = '999999999'
        if addr_ids:
            numbers = addr_obj.read(
                addr_ids, ['phone', 'mobile']
            )
            for number in numbers:
                if number['phone'] and len(number['phone']) > 5:
                    phone = number['phone']
                    break
                elif len(number['mobile']) > 5:
                    phone = number['mobile']
                    break

        tel_str = clean_tel_number(phone)
        res = {}
        res.update({'phone_num': tel_str['tel']})
        if tel_str['pre']:
            res.update({'phone_pre': tel_str['pre']})
        return res

    wizard_m1_01 = c.model('giscedata.switching.mod.con.wizard')
    policy_obj = c.model('giscedata.polissa')
    addr_obj = c.model('res.partner.address')

    policy_vals = policy_obj.read(policy_id, ['titular', 'tarifa'])

    policy_titular_id = policy_vals['titular'][0]
    policy_tarifa_code = policy_vals['tarifa'][1]

    if policy_tarifa_code == '2.0A':
        new_tariff = '004'                  # 2.0DHA
    elif policy_tarifa_code == '2.1A':
        new_tariff = '006'                  # 2.1DHA
    else:
        raise Exception('Tarifa no valida: {}\n'.format(policy_tarifa_code))

    phone_d = get_phone_number(policy_titular_id)

    wiz_vals = {
        'change_atr': True,                 # Cambio Tarifa/Potencia
        'change_adm': False,                # Cambio Titular
        'tariff': new_tariff,
        'firma_nous_preus': 't',            # Tácito
        'phone_num': phone_d['phone_num'],
        'phone_pre': phone_d['phone_pre'],
        'activacio_cicle': 'F',             # La activación se producirá según
                                            # la fecha fija solicitada
        'data_accio': effective_date,
        'change_retail_tariff': True
    }
    # L'ordre és important, després d'haver fet moltes proves la manera correcta
    # és aquesta: primer s'invoca l'assistent indicat que SI es vol canviar de
    # tarifa comercialitzadora i tot seguit se'l sobrescriu aquest camp per
    # indicar que finalment no ho volem fer. D'aquesta manera ens detecta
    # correctament la compatibilitat entre tarifes.
    # Si per exemple només invoquem el wizard indicant que no volem canviar de
    # tarifa comercialitzadora (el camp a False al vals del write), no detectarà
    # correctament si hi ha incompatibilitats.
    wiz = wizard_m1_01.create(
        wiz_vals, {'cas': 'M1', 'pol_id': policy_id}
    )
    wiz.write({'change_retail_tariff': False})
    wiz.genera_casos_atr()
    return wiz.info


@click.command()
@click.option('-f', '--fromcsv', help='CSV file path', type=str, required=True,
              show_default=True)
@click.option('-s', '--server',
              default='http://localhost', help=u'ERP address', required=True)
@click.option('-p', '--port',
              default=8069, help='ERP port', type=click.INT, required=True)
@click.option('-u', '--user', default='admin', help='ERP user', required=True)
@click.option('-w', '--password', prompt=True, confirmation_prompt=False,
              hide_input=True, help='ERP user password')
@click.option('-d', '--database', help='Database name', required=True)
@click.option('-e', '--effective_date', required=True, type=click.STRING,
              help='Effective date')
def change_powers_m1(**kwargs):

    def count_lines(filepath):
        with open(filepath, 'r') as file_to_read:
            return len(file_to_read.readlines())

    def import_from_csv(filepath, separator=';'):
        print ("Creating M1-01 started.")
        total = count_lines(filepath)
        with open(filepath, 'r') as csvfile:
            csvreader = reader(csvfile, delimiter=separator)
            for vals in tqdm(csvreader, desc='Creating...', total=total):
                row = Row(*vals)
                parse_line(row)
        print ("Creation finished.")

    def parse_line(row):
        policy_id = policy_obj.search([('name', '=', row.policy)])
        if policy_id:
            try:
                result = create_m1_01_changing_tariffs_and_powers(c, policy_id[0])
                if "ERROR" in result.upper():
                    result = result.replace('ERROR', '')
                    if incompatible_message in result:
                        llista_preu = policy_obj.read(
                            policy_id[0], ['llista_preu']
                        )['llista_preu'][1]
                        result = "Lista de precios {} incompatible con " \
                                 "DH".format(llista_preu)
                        incompatibles.append({
                            "policy": row.policy, "message": result
                        })
                    else:
                        errores_de_generacion.append({
                            "policy": row.policy, "message": result
                        })
                else:
                    llista_preu = policy_obj.read(
                        policy_id[0], ['llista_preu']
                    )['llista_preu'][0]
                    params = {
                        'refacturacio_pendent': True,
                        'category_id': [(4, fcdh_category_id)]
                    }
                    policy_obj.write(policy_id, params)
                    vals = {
                        'polissa_id': policy_id[0],
                        'texto_informativo': row.message,
                        'llista_preus': llista_preu
                    }
                    notification_obj.create(vals)
            except Exception as e:
                errores_de_generacion.append({
                    "policy": row.policy, "message": e.message
                })

        else:
            errores_de_generacion.append({
                "policy": row.policy, "message": 'La póliza no existe.\n'
            })

    path = kwargs['fromcsv']
    server = kwargs['server']           # 'http://localhost'
    port = kwargs['port']               # '8069'
    database = kwargs['database']
    user = kwargs['user']
    password = kwargs['password']

    global effective_date
    effective_date = kwargs['effective_date']

    server_name = '{}:{}'.format(server, port)
    c = Client(server=server_name, db=database, user=user, password=password)
    print ("Connected succsessfully")

    headers = ['policy', 'message']

    Row = namedtuple('CSVRow', field_names=headers)

    policy_obj = c.model('giscedata.polissa')
    notification_obj = c.model('notificacion.factura')
    category_obj = c.model('giscedata.polissa.category')

    incompatible_message = "Tarifa de comercialitzadora incompatible con la " \
                           "tarifa ATR escogida"

    fcdh_category_id = category_obj.search([('code', '=', 'FCDH')])[0]

    errores_de_generacion = []
    incompatibles = []
    import_from_csv(path)

    log = ""

    for incompatible in incompatibles:
        log += "{policy}, {message}\n".format(**incompatible)

    for error_de_generacion in errores_de_generacion:
        log += "{policy}, Error de generación: {message}".format(
            **error_de_generacion
        )

    with open('./canvi-tarifa-potencia.log', 'w') as log_file:
        log_file.write(log)


if __name__ == '__main__':
    change_powers_m1()
