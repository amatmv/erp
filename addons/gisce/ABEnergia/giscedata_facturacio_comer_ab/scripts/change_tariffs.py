# -*- coding: utf-8 -*-
import os
try:
    # Due: https://www.python.org/dev/peps/pep-0476
    import ssl
    try:
        _create_unverified_https_context = ssl._create_unverified_context
    except AttributeError:
        # Legacy Python that doesn't verify HTTPS certificates by default
        pass
    else:
        # Handle target environment that doesn't support HTTPS verification
        ssl._create_default_https_context = _create_unverified_https_context
except ImportError:
    pass

import click
import logging
from datetime import datetime, timedelta
from erppeek import Client
import tqdm


# row format
# [0] - polissa (name)
# [1] - data ultima lectura
# [2] - tarifa (name)
TARIFES = {}


def get_tarifa(c, tarifa_name):
    tarifa = TARIFES.get(tarifa_name)
    if not tarifa:
        price_list_obj = c.model('product.pricelist')
        price_id = price_list_obj.search([('name', '=', tarifa_name)])
        if price_id:
            TARIFES[tarifa_name] = price_id[0]
            tarifa = price_id[0]
        else:
            tarifa = None
    return tarifa


def activar_polissa(c, id_polissa, data_canvi):
    polissa_obj = c.model('giscedata.polissa')
    polissa_obj.send_signal(
        [id_polissa], 'modcontractual'
    )

    wz_crear_mc_obj = 'giscedata.polissa.crear.contracte'
    ctx = {'active_id': id_polissa, 'lang': 'es_ES'}
    params = {'duracio': 'nou', 'data_inici': data_canvi,
              'tipus_renovacio': 'tacita'
              }

    wz_id = c.model(wz_crear_mc_obj).create(params, ctx)
    wiz = c.model(wz_crear_mc_obj).read(wz_id.id, [])
    res = c.model(wz_crear_mc_obj).onchange_duracio(
        [wz_id.id], wz_id.data_inici, wz_id.duracio, ctx
    )
    obs = wiz['observacions']
    obs = u'Renovación de tarifa automáticamente\n\n' + obs
    c.model(wz_crear_mc_obj).write(
            [wz_id.id], {'observacions': obs}
    )
    c.model(wz_crear_mc_obj).write(
        [wz_id.id], {'data_final': res['value']['data_final']})
    c.model(wz_crear_mc_obj).action_crear_contracte(
        [wz_id.id], ctx)


def setup_log(instance, filename):
    log_file = filename
    logger = logging.getLogger(instance)
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr = logging.FileHandler(log_file)
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(logging.INFO)


def update_polissa(c, row):
    logger = logging.getLogger('update_polissa_tarifa')
    polissa = c.GiscedataPolissa.search([('name', '=', row[0])])
    if polissa:
        polissa_id = polissa[0]
        pol_dades = c.GiscedataPolissa.browse(polissa_id)
        data_ultima_lectura = pol_dades.data_ultima_lectura
        data_act_modcon = pol_dades.modcontractual_activa.data_inici
        date_2018 = '2018-01-01'
        min_days = 365
        num_days = datetime.today() - datetime.strptime(
            pol_dades.data_alta, '%Y-%m-%d')

        print '{}  {}  {}'.format(pol_dades.name, data_ultima_lectura, data_act_modcon)
        if num_days.days < min_days:
            logger.warning(
                'NOT CHANGE '
                u'Duración del contrato {} menor que {} para polissa: {}'.format(
                    num_days, min_days, row[0]
                ))
        elif data_act_modcon >= data_ultima_lectura:
            logger.warning(
                'NOT AUTOMATIC CHANGE '
                'data_ult_lectura menor que data mod_contractual '
                ' activa on polissa: {}'.format(row[0]))
        elif data_ultima_lectura < date_2018:
            logger.warning(
                'NOT CHANGE '
                'data_ult_lectura {} < {} on polissa: {}'.format(
                    data_ultima_lectura, date_2018, row[0]
                ))
        else:
            data_activacio = datetime.strptime(
                data_ultima_lectura, '%Y-%m-%d') + timedelta(days=1)
            polissa_obj = c.model('giscedata.polissa')
            tarifa_id = get_tarifa(c, row[2])
            ultima_lectura_fitxer = row[1]
            if tarifa_id:
                try:
                    if ultima_lectura_fitxer == data_ultima_lectura:
                        polissa_obj.send_signal(
                            [polissa_id], 'modcontractual'
                        )
                        pol_dades.write({
                            'llista_preu': tarifa_id,
                        })
                        activar_polissa(
                            c, polissa_id, '{:%Y-%m-%d}'.format(data_activacio)
                        )
                    else:
                        logger.error(
                            'FAIL polissa ultima lectura facturada distinta: {} {} {}'.format(
                                row[0], row[1], row[2]
                            )
                        )
                except Exception as exc:
                    polissa_obj.send_signal(
                        [polissa_id], 'undo_modcontractual'
                    )
                    logger.error(
                        'FAIL polissa: {} {} {}'.format(
                            row[0], row[1], exc
                        )
                    )
            else:
                logger.error(
                    'FAIL tarifa name not exist on pricelist: {} {}'.format(
                        row[0], row[1]
                    )
                )
    else:
        logger.error(
            'NO FOUND polissa: {} {}'.format(
                *row
            ))


def update_polissa_tarifa(connection, csv_file, delimitador=';'):
    import csv
    f = open(csv_file, 'rb')
    reader = csv.reader(f, delimiter=delimitador)
    for line in tqdm.tqdm(reader):
        update_polissa(connection, line)
    f.close()

@click.command()
@click.option('-s', '--server', default='http://localhost',
              help=u'Adreça servidor ERP')
@click.option('-p', '--port', default=8069, help='Port servidor ERP',
              type=click.INT)
@click.option('-u', '--user', default='admin', help='Usuari servidor ERP')
@click.option('-w', '--password', default='admin',
              help='Contrasenya usuari ERP')
@click.option('-d', '--database', help='Nom de la base de dades')
@click.option('-f', '--file-input', type=click.Path(exists=True))
def work(**kwargs):

    server = '{}:{}'.format(kwargs['server'], kwargs['port'])
    c = Client(server=server, db=kwargs['database'], user=kwargs['user'],
               password=kwargs['password'])
    filename = os.path.basename(kwargs['file_input'])
    setup_log('update_polissa_tarifa', '/tmp/{}.log'.format(filename))
    logger = logging.getLogger('update_polissa_tarifa')
    logger.info('#######Start now {0}'.format(datetime.today()))

    update_polissa_tarifa(c, kwargs['file_input'])


if __name__ == '__main__':
    work()
