# -*- coding: utf-8 -*-
import logging
from erppeek import Client
import click
from datetime import datetime


def setup_log(instance, filename):
    log_file = filename
    logger = logging.getLogger(instance)
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr = logging.FileHandler(log_file)
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(logging.INFO)


@click.command()
@click.option('-s', '--server', default='http://localhost',
              help=u'Adreça servidor ERP')
@click.option('-p', '--port', default=8069, help='Port servidor ERP',
              type=click.INT)
@click.option('-u', '--user', default='admin', help='Usuari servidor ERP')
@click.option('-w', '--password', default='admin',
              help='Contrasenya usuari ERP')
@click.option('-d', '--database', help='Nom de la base de dades')
@click.option('--state', default=None, help='Select only this state')
def update_registros(**kwargs):
    server = '{}:{}'.format(kwargs['server'], kwargs['port'])
    c = Client(
        server=server,
        db=kwargs['database'],
        user=kwargs['user'],
        password=kwargs['password']
    )
    if not c:
        click.echo(u'Impossible to connect to ERP')
        return False

    setup_log('update_registros_vencimiento', '/home/erp/var/log/update_registros_vencimiento.log')
    logger = logging.getLogger('update_registros_vencimiento')
    logger.info('#######Start now {0}'.format(datetime.today()))

    upd_reg_obj = c.model('calcular.registros.vencimiento')

    contratos_vencimientos = upd_reg_obj.buscar_contratos_vencidos()
    logger.info(
        ('Se han encontrado los siguientes contratos que van a vencer:\n{0}'
         ).format(','.join(str(x) for x in contratos_vencimientos)))

    res = upd_reg_obj.crear_registros_vencimiento(contratos_vencimientos)

    for new_r in res[0]:
        logger.info(('Se ha creado un registro para la póliza {0} con '
                     'lista de precios {1} y fecha de vencimiento {2}'
                     ).format(*new_r))

    for upd_r in res[1]:
        logger.info(('Se ha actualizado el registro de la póliza {0}, '
                     'lista de precios {1} con la fecha de vencimiento {2}'
                     ).format(*upd_r))

    logger.info('#######End process at {0}'.format(datetime.today()))

if __name__ == '__main__':
    update_registros()
