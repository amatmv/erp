# -*- coding: utf-8 -*-
from osv import osv


class AccountInvoice(osv.osv):
    _name = 'account.invoice'
    _inherit = 'account.invoice'

    def is_possible_to_send_sii(self, invoice):
        # Supplier invoices with origin_date_invoice before STARTING_SII_DATE
        # should not be possible to be sent to SII
        if invoice.type in ['in_invoice', 'in_refund']:
            return invoice.origin_date_invoice >= self.STARTING_SII_DATE
        # Invoices with date_invoice before STARTING_SII_DATE should not be
        # possible to be sent to SII
        return invoice.date_invoice >= self.STARTING_SII_DATE


AccountInvoice()
