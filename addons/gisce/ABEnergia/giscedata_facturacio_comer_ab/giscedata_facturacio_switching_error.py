# -*- coding: utf-8 -*-
from osv import osv, fields
from datetime import datetime, timedelta


class GiscedataFacturacioSwitchingValidator(osv.osv):
    _name = 'giscedata.facturacio.switching.validator'
    _inherit = 'giscedata.facturacio.switching.validator'

    def check_mode_facturacio(self, cursor, uid, line, f1, params=None):
        pol_obj = self.pool.get('giscedata.polissa')
        lect_help_obj = self.pool.get('giscedata.lectures.switching.helper')

        errors = super(
            GiscedataFacturacioSwitchingValidator, self).check_mode_facturacio(
            cursor, uid, line, f1, params=params
        )
        if errors and len(errors) == 1:
            for fact_xml in f1.facturas_atr:
                start_date = datetime.strptime(
                    fact_xml.datos_factura.fecha_desde_factura, '%Y-%m-%d'
                )
                start_date += timedelta(days=1)
                fecha_desde = start_date.strftime('%Y-%m-%d')
                fecha_hasta = fact_xml.datos_factura.fecha_hasta_factura
                break

            polissa_id = lect_help_obj.find_polissa_with_cups_id(
                cursor, uid, line.cups_id.id, fecha_desde, fecha_hasta
            )
            modcons = pol_obj.get_modcontractual_intervals(
                cursor, uid, polissa_id, fecha_desde, fecha_hasta)

            if len(modcons) > 2:
                errors[0]['data_nova'] += (
                    '. No se hace la modificación automática debido a que '
                    'hay más de una modificación contractual activa para el '
                    'rango de fechas')
                return errors

            polissa = pol_obj.browse(cursor, uid, polissa_id)
            mod_activa = polissa.modcontractual_activa
            mod_por_fecha = [x for x in polissa.modcontractuals_ids
                             if x.data_inici == errors[0]['data_canvi']][0]
            if mod_activa.id != mod_por_fecha.id:
                errors[0]['data_nova'] += (
                    '. No se hace la modificación automática debido a que '
                    'hay más la modificación contractual activa es distinta '
                    'a la modificación contractual afectada')
                return errors

            wiz_obj = self.pool.get('wizard.canviar.dates')
            ctx = {'active_id': mod_por_fecha.id}
            wiz_id = wiz_obj.create(cursor, uid, {}, context=ctx)
            data_nova = errors[0]['data_nova']
            data_inici_original = mod_por_fecha.data_inici
            wiz_obj.write(cursor, uid, [wiz_id], {'data_inici': data_nova})
            wiz_obj.change_date(cursor, uid, [wiz_id], context=ctx)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            # En cas que hi hagi error en l'assistent que retorni els resultats
            # perquè falli la validació i aturi la importació del F1
            if wiz.state == 'error':
                errors[0]['data_nova'] += (
                    '. No se hace la modificación automática debido a que el '
                    'assistente de cambio de fechas no puede hacer el cambio '
                    'automáticamente')
                return errors
            else:
                obs = mod_por_fecha.observacions
                message = ('Cambio automático de fechas por importación de '
                           'fichero F1 {f1_name} con fecha desde '
                           '{f1_fecha_ini} y fecha hasta {f1_fecha_hasta} '
                           'con cambio de modo de facturación. Fecha original '
                           'de cambio de modo de facturación: {fecha_orig}, '
                           'nueva fecha inicial: {fecha_nueva}'
                           ).format(
                    f1_name=line.name, f1_fecha_ini=fecha_desde,
                    f1_fecha_hasta=fecha_hasta, fecha_orig=data_inici_original,
                    fecha_nueva=data_nova
                )
                obs = '{obs}\n{message}'.format(obs=obs, message=message)
                mod_por_fecha.write({'observacions': obs})
                return []
        return errors


GiscedataFacturacioSwitchingValidator()
