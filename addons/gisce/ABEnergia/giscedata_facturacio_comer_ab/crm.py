# -*- coding: utf-8 -*-

from osv import osv
from giscedata_facturacio_comer_ab.wizard.wizard_get_penalty import WizardGetPenalty


class crm_case(osv.osv):
    _name = 'crm.case'
    _inherit = 'crm.case'

    def set_penalty(self, cursor, uid, ids, context=None):
        """
        Calcula i escriu al cas la penalització.
        :param cursor:
        :param uid:
        :param ids:
        :param context:
        :return: Diccionari on la clau es l'id del cas i el valor es un text
        informatiu en cas de que no s'hagi pogut calcular la penalització: si
        es False llavors s'ha calculat i guardat correctament la penalització.
        """
        if not isinstance(ids, list):
            ids = [ids]

        res_config = self.pool.get('res.config')
        policy_o = self.pool.get('giscedata.polissa')
        C1_06_o = self.pool.get('giscedata.switching.c1.06')
        C1_11_o = self.pool.get('giscedata.switching.c1.11')
        C2_06_o = self.pool.get('giscedata.switching.c2.06')
        C2_11_o = self.pool.get('giscedata.switching.c2.11')

        fields = ['ind_bono_social']

        sw_o = self.pool.get('giscedata.switching')
        sw_f = ['step_id.name', 'proces_id.name']
        sw_select = sw_o.q(cursor, uid).select(sw_f)

        crm_case_f = ['date', 'state', 'polissa_id.id', 'id', 'ref',
                      'date_action_next']
        dmn = [('id', 'in', ids)]
        modo = res_config.get(cursor, uid, 'penalty_calculation_type', 'diff')

        q = self.q(cursor, uid).select(crm_case_f).where(dmn)
        cursor.execute(*q)

        res = {}

        for crm_case_v in cursor.dictfetchall():
            policy_id = crm_case_v['polissa_id.id']
            crm_id = crm_case_v['id']
            crm_wv = {}

            model_name, model_id = crm_case_v['ref'].split(',')
            model_id = int(model_id)
            if model_name == 'giscedata.switching':

                dmn = [('id', '=', model_id)]
                q = sw_select.where(dmn)
                cursor.execute(*q)
                sw_v = cursor.dictfetchall()[0]

                isC1 = sw_v['proces_id.name'] == 'C1'
                isC2 = sw_v['proces_id.name'] == 'C2'
                is11 = sw_v['step_id.name'] == '11'
                is06 = sw_v['step_id.name'] == '06'

                if (isC1 or isC2):
                    if isC1 and is11: obj = C1_11_o
                    if isC1 and is06: obj = C1_06_o
                    if isC2 and is11: obj = C2_11_o
                    if isC2 and is06: obj = C2_06_o

                    dmn = [('header_id.sw_id', '=', model_id)]
                    q = obj.q(cursor, uid).select(fields).where(dmn)
                    cursor.execute(*q)
                    vals = cursor.dictfetchall()[0]

                    renovacio_expressa = crm_case_v['date']
                    desti_bono_social = vals['ind_bono_social'] == 'S'
                    msg = ''
                    if not renovacio_expressa:
                        msg += (
                            "No se ha calculado la penalización ya que la "
                            "renovación es tácita.\n"
                        )
                    if desti_bono_social:
                        msg += (
                            "No se ha calculado la penalización ya que el "
                            "cliente se va a Bono Social.\n"
                        )
                    if renovacio_expressa and not desti_bono_social:
                        if modo == 'diff':
                            penalty = policy_o.get_penalty(
                                cursor, uid, policy_id,
                                offset_date=crm_case_v['date'],
                                context=context)[policy_id]
                        if modo == 'prev':
                            penalty = policy_o.get_penalty_from_prev(
                                cursor, uid, policy_id,
                                offset_date=crm_case_v['date'],
                                data_baixa=crm_case_v['date_action_next'],
                                context=context)[policy_id]

                        msg = penalty['info'] or penalty['nullifiers_or_rectifiers']
                        resum = WizardGetPenalty.format_penalty(
                            penalty)
                        if not msg:
                            crm_wv.update(
                                {'planned_revenue': penalty['total_penalty'],
                                 'description': "{0}".format(resum)}
                            )
                        else:
                            crm_wv.update({'description': "{0}\n{1}".format(msg, resum)})
                else:
                    msg = (
                        "No se ha calculado la penalización ya que no se trata "
                        "de un C1 o C2."
                    )
            else:
                msg = "El caso no tiene una referencia ATR"
            res[crm_id] = msg
            self.write(cursor, uid, crm_id, crm_wv, context=context)
        return res

crm_case()
