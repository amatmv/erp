# -*- coding: utf-8 -*-
from osv import osv, fields


class ProductPricelist(osv.osv):
    """Llistes de preus.
    """
    _name = 'product.pricelist'
    _inherit = 'product.pricelist'

    def single_price(self, cursor, uid, ids, context=None):
        if isinstance(ids, (list, tuple)):
            pricelist_id = ids[0]
        else:
            pricelist_id = ids
        counts = self.search(
            cursor, uid, [('id', '=', pricelist_id),
                          '|',
                          ('name', 'like', '20E'),
                          ('name', 'like', '21E')
                         ]
        )
        if counts:
            return True
        return False

    _columns = {
        'variable_discount': fields.boolean('Descuento variable'),
        'p2_precent': fields.float('Porcentage P2 %', digits=(5, 2)),
        'conditional_discount': fields.float('Descuento condicional %', digits=(5, 2))
    }


ProductPricelist()
