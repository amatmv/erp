# -*- coding: utf-8 -*-

from osv import osv, fields
from datetime import datetime


class CorrespondenciesExtincionsTarifa(osv.osv):

    _name = 'correspondencies.extincions.tarifa'

    _order = 'id desc'

    #Retorna la tarifa de correspondencia
    def get_tarifa_correspondencia(self, cursor, uid, tarifa_origen_id,
                                   contracte_id):
        # primer mirem si hi ha una correspondencia per el contracte concret
        correspon_ids = self.search(cursor, uid,
                                    [('tarifaorigen', '=', tarifa_origen_id),
                                     ('polissa_id', '=', contracte_id),
                                     ],
                                    limit=1, order='data DESC')

        # si no, busquem la primera correspondència genèrica per la
        # llista orígen (la més recent que no estigui associada a un
        # contracte concret)
        if not correspon_ids:
            correspon_ids = self.search(cursor, uid,
                                        [('tarifaorigen', '=', tarifa_origen_id),
                                         ('polissa_id', '=', False),
                                         ],
                                        limit=1, order='data DESC')

        desti_id = False
        if correspon_ids:
            desti_id = self.read(cursor, uid, correspon_ids, [
                'tarifadesti'])[0]['tarifadesti']
        return desti_id

    _columns = {
        'polissa_id': fields.many2one('giscedata.polissa',
                                      u'Contrato',
                                      ),
        'tarifaorigen': fields.many2one('product.pricelist',
                                        u'Tarifa origen',
                                        required=True,
                                        domain=[('type', '=', 'sale')]),
        'tarifadesti': fields.many2one('product.pricelist',
                                       u'Tarifa destino',
                                       required=True,
                                       domain=[('type', '=', 'sale')]),
        'data': fields.date(string="Fecha de la correspondencia",
                            required=True,),
        }

CorrespondenciesExtincionsTarifa()