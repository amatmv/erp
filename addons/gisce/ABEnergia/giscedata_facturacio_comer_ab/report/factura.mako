## -*- coding: utf-8 -*-
<%
    from datetime import datetime, timedelta
    from operator import attrgetter, itemgetter
    from bankbarcode.cuaderno57 import Recibo507
    import json, re
    from giscedata_facturacio_comer.report.utils import (
          get_giscedata_fact,get_other_info,get_discriminador,
          get_distri_phone,get_historics,get_linies_tipus,
          get_line_discount
    )
    from account_invoice_base.report.utils import localize_period

    pool = objects[0].pool
    conf_obj = pool.get('res.config')
    polissa_obj = pool.get('giscedata.polissa')
    tarifa_obj = pool.get('giscedata.polissa.tarifa')
    banner_obj = pool.get('report.banner')
    sup_territorials_2013_tec271_comer_obj = pool.get('giscedata.suplements.territorials.2013.tec271.comer')
    polissa_categ_obj = pool.get('giscedata.polissa.category')

    def check_tax(tax, tipus):
        res = '-'
        if tipus is not None:
            if "IVA" in tax.name:
                res = tax.name.replace("IVA ", "")
                res = res.replace("%", "")
                res = "{} %".format(res)
        elif tax:
            if "IVA" in tax.name:
                res = "{} %".format(formatLang(tax.amount * 100, digits=0))
        return res

    def get_iva(linia, tipus=None):
        res  = '-'
        if tipus is not None:
            res = check_tax(linia, tipus)
        elif linia.invoice_line_tax_id:
            for tax in linia.invoice_line_tax_id:
                res = check_tax(tax, tipus)
        return res

    def check_ajuste(linia_energia, lectures_energia, tipus, id_tarifa):
        '''
        Aquest mètode comprova si, per una línia d'energia d'una factura existeix' \
        'alguna lectura d'energia amb ajuste integrador
        :param linia_energia: línia d'energia d'una factura a comprovar
        :param lectures_energia: lectures d'energia de la factura la qual pertany' \
        'la línia
        :param tipus: pot prendre dos valors: activa o reactiva
        :param id_tarifa: id de la tarifa d'accés
        :return: retorna CERT si existeix alguna lectura d'energia amb ajuste integrador' \
        'que es correspongui amb la línia d'energia. Per exemple, si tenim una lectura 3.0A(P3)' \
        'amb ajuste integrador i del tipus "Activa", buscara una línia d'energia de la factura' \
        'que sigui del tipus 'Energia (Activa)' i del període 1. Si existeix, retornarà cert.
        '''
        agrupades = tarifa_obj.get_grouped_periods(cursor, uid, id_tarifa)
        for lect in lectures_energia:
            if lect.tipus == tipus and lect.ajust > 0:
                name = re.findall('(P[0-9])', lect.name)[0]
                if name in agrupades:
                    name = agrupades[name]
                if linia_energia.name in name:
                    return True
        return False
    
    show_ajust = False
    banner_addicional = bool(int(conf_obj.get(cursor, uid, 'factura_banner_addicional', False)))

    def get_origen(code):
        if code in ['40', '99']:
            result = 'Estimada'
        else:
            result = 'Real'
        return result


%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en"><html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
    	<style type="text/css">
        ${css}
        </style>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_ab/report/factura.css"/>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_ab/report/consum.css"/>
    </head>
    <body>
        %if not objects[0].payment_type:
            <div id="alerta">
                ${("La factura ")}${objects[0].number}${_(" no tiene ningún tipo de pago asignado.")}
            </div>
        %else:
            <%
                num_factures = 0
            %>
            %for inv in objects:
            <%
                num_factures += 1
                factura = get_giscedata_fact(cursor, inv)[0]
                dies_factura = 1 + (datetime.strptime(inv.data_final, '%Y-%m-%d') - datetime.strptime(inv.data_inici, '%Y-%m-%d')).days
                diari_factura_actual_eur = inv.total_energia / (dies_factura or 1.0)
                diari_factura_actual_kwh = (inv.energia_kwh * 1.0) / (dies_factura or 1.0)
                ref = '{}'.format(inv.id).zfill(11) # id factura
                entity = ''.join([c for c in inv.polissa_id.comercialitzadora.vat if c.isdigit()])
                due_date = str(inv.date_due) # plazo pago = fecha factura + payment_term
                if due_date:
                    suffix = '501'
                    days_limit = 15
                    if 'COBRADOR' in inv.payment_type.name:
                        days_limit = 5
                    due_date = datetime.strptime(inv.date_due, '%Y-%m-%d') + timedelta(days=days_limit)
                else:
                    suffix = '101'
                    due_date = None
                sentido = 1.0
                polissa = polissa_obj.browse(cursor, uid, inv.polissa_id.id, context={'date': inv.data_final.val})
                direccio = "{} {}".format(inv.address_invoice_id.street, inv.address_invoice_id.zip if inv.address_invoice_id.zip else "")
                poblacio = inv.address_invoice_id.id_poblacio.name
                cups = polissa.cups.name
                direccio_cups = polissa.cups.direccio
                idx_pob = direccio_cups.rfind('(')
                if idx_pob != -1:
                    direccio_cups = direccio_cups[:idx_pob]
                pot_periode = dict([('P%s' % p,'') for p in range(1, 7)])
                for pot in polissa.potencies_periode:
                    pot_periode[pot.periode_id.name] = pot.potencia
                info = get_other_info(cursor, factura['id'])[0]
                dphone = get_distri_phone(cursor, uid, polissa)
                cphone = inv.company_id.partner_id.address[0].phone
                try:
                    distri_phone = ''.join([dphone[i:i+3] for i in range(0,len(dphone),3)])
                except:
                    raise Exception(
                        u'La distribuidora no tiene definido el teléfono de contacto'
                    )
                entidad_bancaria = ''
                bank_acc = ''
                if inv.payment_type.code == 'RECIBO_CSB':
                    if not inv.partner_bank:
                        raise Exception(
                            u'La factura {0} no tiene número de cuenta definido.'.format(
                            inv.number)
                        )
                    else:
                        entidad_bancaria = inv.partner_bank.bank.lname or inv.partner_bank.bank.name or inv.partner_bank.name
                        bank_acc = inv.partner_bank.iban[:-4].replace(' ','')  + '****'
                else:
                    if inv.partner_bank:
                        entidad_bancaria = inv.partner_bank.bank.lname or inv.partner_bank.bank.name or inv.partner_bank.name
                        bank_acc = inv.partner_bank.iban[:-4].replace(' ','') + '****'
                notice = '000000'
                amount = '{0:.2f}'.format(inv.amount_total) # amount factura
                recibo507 = Recibo507(entity, suffix, ref, notice, amount, due_date)
                periodes_a = sorted(list(set([lectura.name[-3:-1]
                                for lectura in inv.lectures_energia_ids
                                if lectura.tipus == 'activa'])))
            %>
            <div id="continguts">
                <div id="table_fact" class="_mt_25">
                    <table>
                        <tr>
                            %if inv.type in ['out_refund', 'in_refund']:
                                <% sentido = -1.0 %>
                            %endif
                            %if inv.tipo_rectificadora in ['R', 'RA']:
                                <td class="bold f_14 w_115">${_("FACTURA Nº:")}</td>
                                <td class="f_12 pt_4">${inv.number or ''|entity}</td>
                                <td class="f_8 pt_5">${_("Esta factura rectifica la factura")} ${inv.ref.number or ''}</td>
                            %elif inv.tipo_rectificadora in ['B', 'A', 'BRA']:
                                <td class="bold f_14 w_115">${_("FACTURA Nº:")}</td>
                                <td class="f_12">${inv.number or ''|entity}</td>
                                <td colspan="3" class="f_8 pt_5">${_("Esta factura anula la factura")} ${inv.ref.number or ''}</td>
                            %else:
                                <td class="bold w_115 f_14">${_("FACTURA Nº:")}</td>
                                <td colspan="3" class="f_12">${inv.number or ''|entity}</td>
                            %endif
                        </tr>
                    </table>
                    <table>
                        <tr>
                            <td class="bold w_90 f_10">${_("Fecha de emisión")}</td>
                            <td rowspan="2" class="w_30">&nbsp</td>
                            <td class="bold f_10">${_("Periodo de facturación")}</td>
                        </tr>
                        <tr>
                            <td class="w_145 f_10">${localize_period(inv.date_invoice, 'es_ES')}</td>
                            <%
                                factura = get_giscedata_fact(cursor, inv)[0]
                                data_inici = datetime.strptime(factura['data_inici'], '%Y-%m-%d')
                                data_inici_str = data_inici.strftime('%d/%m/%Y')
                                data_final = datetime.strptime(factura['data_final'], '%Y-%m-%d')
                                data_final_str = data_final.strftime('%d/%m/%Y')
                            %>
                            <td class="f_10">del ${data_inici_str|entity} al ${data_final_str|entity}</td>
                        </tr>
                    </table>
                </div>
                <div class="total_factura background_verd_clar _mt_25">
                    <table style="height: 100%;">
                        <tr>
                            <td class="bold f_12 center align_bottom">${_("Total factura")}</td>
                        </tr>
                        <tr>
                            <td class="bold center f_18">${formatLang(sentido * inv.amount_total, digits=2)} €</td>
                        </tr>
                    </table>
                </div>
                <div style="clear: both"></div>
                <div id="logo">
                    <img id="img_logo" src="data:image/jpeg;base64,${company.logo}"/>
                </div>
                <div id="dades_client">
                    <table>
                        <tr>
                            <% info = get_other_info(cursor, factura['id'])[0]%>
                            <td class="f_12">${inv.address_contact_id.name or ''|entity}</td>
                        </tr>
                        <tr>
                            <td class="f_12">${inv.address_contact_id.street or ''|entity}</td>
                        </tr>
                        <tr>
                            <td class="f_12">${inv.address_contact_id.zip or ''|entity} ${inv.address_contact_id.city or ''|entity}</td>
                        </tr>
                        %if inv.address_contact_id.country_id.code != 'ES':
                            <tr><td class="f_12">${inv.address_contact_id.country_id.name or ''}</td></tr>
                        %endif
                    </table>
                    <div id="barcode">
                        %if inv.payment_type.code != 'RECIBO_CSB':
                                ${ recibo507.svg(writer_options={'module_height': 7, 'font_size': 7, 'text_distance': 3, 'module_width': 0.25}) }
                        %endif
                    </div>
                </div>
                <div style="clear: both;"></div>
                <div id="info">
                    <div id="cups">
                        ${_("C.U.P.S: ")}${cups}
                    </div>
                    <div class="contenidor_boto">
                        <div class="boto h_48">
                            <img id="telefon_img" src="${addons_path}/giscedata_facturacio_comer_ab/report/telefon.png" >
                        </div>
                        <div class="info_boto">
                            <table class="h_48 f_8">
                                <tr>
                                    <td style="vertical-align: bottom;">
                                        ${_("Atención al cliente")}
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bold" style="vertical-align: top;">
                                        ${_("900 1903 00")}
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="contenidor_boto">
                        <div class="boto h_48">
                            <img class="altres_img" src="${addons_path}/giscedata_facturacio_comer_ab/report/sobre.png" >
                        </div>
                        <div class="info_boto">
                            <table class="h_48 f_8">
                                <tr>
                                    <td class="bold">
                                        ${_("sac@abenergia.es")}
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="contenidor_boto">
                        <div class="boto h_48">
                            <img class="altres_img" src="${addons_path}/giscedata_facturacio_comer_ab/report/mouse.png" >
                        </div>
                        <div class="info_boto">
                            <table class="h_48 f_8">
                                <tr>
                                    <td class="bold">
                                        ${_("www.abenergia.es")}
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div style="clear: both;"></div>
                <div id="dades_factura">
                    <div class="w_48p float_left _mr_12 top-10">
                        <div class="head_datos background_verd">
                            <p class="bold">${_("Datos cliente")}</p>
                        </div>
                        <div style="vertical-align: middle;" class="min_height_100 body_datos border_verd">
                            <table class="table_datos">
                                <tr>
                                    <th class="w_60">${_('Titular:')}</th>
                                    <td>${inv.partner_id.title or ''|entity}  ${info['res_partner_name'] |entity}</td>
                                </tr>
                                <tr>
                                    <th>${_('Dirección:')}</th>
                                    <td>${direccio}</td>
                                </tr>
                                <tr>
                                    <th>${_('Población:')}</th>
                                    <td>${poblacio}</td>
                                </tr>
                                <tr>
                                    <th>${_('NIF/DNI:')}</th>
                                    <td>${info['res_partner_vat']}</td>
                                </tr>
                            </table>
                            <table class="table_datos">
                                <tr>
                                    <th class="w_115">${_('Referencia catastral:')}</th>
                                    <td>${info['giscedata_cups_ps_ref_cat']}</td>
                                </tr>
                                <tr>
                                    <th>${_('Código cliente:')}</th>
                                    <td>${info['giscedata_polissa_name'] or '' | entity}</td>
                                </tr>
                                <tr>
                                    <th>${_('C.N.A.E.')}</th>
                                    <td>${info['giscedata_polissa_cnae'] or ''|entity}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="w_48p float_right _ml_12 top-10">
                        <div class="head_datos background_verdblau">
                            <p class="bold">${_("Datos suministro")}</p>
                        </div>
                        <div class="min_height_100 body_datos border_verdblau">
                            <table class="table_datos">
                                <tr>
                                    <th>${_('Dirección')}:</th>
                                    <td colspan="3">${direccio_cups}</td>
                                </tr>
                                <tr>
                                    <th>${_('Distribuidora:')}</th>
                                    <td colspan="3">${inv.polissa_id.distribuidora.name}</td>
                                </tr>
                                <tr>
                                    <th>${_('Potencia:')}</th>
                                    <td colspan="3">
                                        <%
                                            primer = True
                                        %>
                                        % for periode, potencia in sorted(pot_periode.items(),key=itemgetter(0)):
                                            % if potencia:
                                            %if not primer:
                                                -
                                            %else:
                                                <%
                                                    primer = False
                                                %>
                                            %endif
                                            ${periode}:
                                            ${formatLang(potencia, digits=3)}

                                            % endif
                                        % endfor
                                    </td>
                                </tr>
                                <tr>
                                    <th>${_('Tensión:')}</th>
                                    <td colspan="3">${info['giscedata_polissa_tensio']}</td>
                                </tr>
                                <tr>
                                    <th>${_('Tarifa:')}</th>
                                    <td colspan="3">${polissa.tarifa.name or ''|entity}</td>
                                </tr>
                            </table>
                            <table class="table_datos">
                                <tr>
                                    <th class="w_145">${_('Vencimiento del contrato:')}</th>
                                    <td>${inv.polissa_id.modcontractual_activa.data_final}</td>
                                </tr>
                                <tr>
                                    <th>${_('Teléfono averías:')}</th>
                                    <td>${distri_phone}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div style="clear: both;"></div>
                </div>
                <div style="clear: both;"></div>

                <div class="w_48p float_left _mr_12 top-10" style="text-align: center;">
                    %if banner_addicional:
                        <% style_img = 'max-width: 95%; position: absolute; top: 0; bottom:0; margin:auto; ' %>
                        <img src="${addons_path}/giscedata_facturacio_comer_ab/report/banners/logo115.png" style="height: 50px; max-width: 100%; margin-bottom: 10px;">
                        <div class="body_publi border_verd" style="padding-top: 10px;position:relative; height: 220px; text-align: center; overflow: hidden;">
                    %else:
                        <% style_img = 'max-width: 95%; position: absolute;top:0; bottom:0; margin:auto;' %>
                        <div class="body_publi border_verd" style="padding-top:15px; position: relative; height: 275px; text-align: center;overflow: hidden;">
                    %endif
                        <%
                            banner_img = banner_obj.get_banner(cursor, uid, 'giscedata.facturacio.factura',inv.date_invoice.val, model_id=inv.id)
                        %>
                        %if banner_img:
                            <img src="data:image/jpeg;base64,${banner_img}" style="${style_img}">
                        %else:
                            <img src="${addons_path}/giscedata_facturacio_comer_ab/report/banners/default.png" style="${style_img}">
                        %endif
                    </div>
                </div>

                <div class="w_48p float_right _ml_12 top-20">
                    <div class="head_datos background_blau">
                        <p class="bold">${_("Datos de pago")}</p>
                    </div>
                    <div class="body_datos border_blau" style="height: 149px;">
                        <table class="table_datos">
                            <tr>
                                <th class="w_100">${_('Forma de pago:')}</th>
                                <td>${inv.payment_type and inv.payment_type.name or ''|entity}</td>
                            </tr>
                        </table>
                        <table class="table_datos">
                            <tr>
                                <th class="w_100">${_('Entidad bancaria:')}</th>
                                <td>${entidad_bancaria}</td>
                            </tr>
                        </table>
                        <table class="table_datos">
                            <tr>
                                <th class="w_100">${_('Nº de cuenta:')}</th>
                                <td>${bank_acc}</td>
                            </tr>
                            %if inv.payment_type.code != 'RECIBO_CSB':
                                <tr>
                                    <th>${_('Emisora:')}</th>
                                    <td>${entity} ${suffix}</td>
                                </tr>
                                <tr>
                                    <th>${_('Referencia:')}</th>
                                    <td>${ref}</td>
                                </tr>
                                <tr>
                                    <th>${_('Identificación:')}</th>
                                    <td>${recibo507.notice}</td>
                                </tr>
                            %endif
                            <tr>
                                <th>${_('Importe:')}</th>
                                <td>${formatLang(sentido * inv.amount_total, digits=2)} €</td>
                            </tr>
                            <tr>
                                <th>${_('Fecha de cobro:')}</th>
                                <td>${inv.date_due or ''}</td>
                            </tr>
                        </table>
                    </div>
                </div>

            <%
                base = 0.0
                total_iese = 0.0
                iva = '-'
                total_exenta = inv.amount_untaxed
            %>
            %for t in inv.tax_line :
                %if 'especial sobre la electricidad'.upper() in t.name.upper():
                    <%
                        base +=  t.base
                        total_iese += t.amount
                        total_exenta += t.amount
                    %>
                %elif 'IVA' in t.name.upper():
                    <%
                        iva = t.name.upper().replace('IVA','')
                        total_exenta -= t.base
                    %>
                %endif
            %endfor
                <% total_exenta = round(total_exenta, 2) %>
                <div class="border_base w_313 float_right _ml_12 f_10 top-15">
                    <table class="taula_base_imponible">
                        <tr>
                            <td class="w_20px">&nbsp;</td>
                            <td class="bold align_right">${_("Base")}</td>
                            <td class="bold align_right w_35">${_("% IVA")}</td>
                            <td class="bold align_right">${_("IVA")}</td>
                            <td class="bold align_right w_75">${_("Total Factura")}</td>
                        </tr>
                        %for t in inv.tax_line :
                            %if 'IVA' in t.name.upper():
                                <tr>
                                    <td class="bold">${_("Base imponible")}</td>
                                    <td class="align_right">${formatLang(sentido * t.base, digits=2)|entity}</td>
                                    <td class="align_right">${t.name.upper().replace('IVA','')}</td>
                                    <td class="align_right">${formatLang(sentido * t.amount, digits=2)}</td>
                                    %if not total_exenta:
                                        <td class="align_right bold">${formatLang(sentido * (t.base + t.amount), digits=2)}</td>
                                    %else:
                                        <td class="align_right">${formatLang(sentido * (t.base + t.amount), digits=2)}</td>
                                    %endif
                                </tr>
                            %endif
                        %endfor
                        %if not total_exenta:
                            <tr>
                                %for elem in range(1, 6):
                                    <td>&nbsp;</td>
                                %endfor
                            </tr>
                        %else:
                            <tr>
                                <td class="bold">${_("Base exenta")}</td>
                                <td class="align_right">${formatLang(sentido * total_exenta, digits=2)}</td>
                                <td class="align_right">-</td>
                                <td class="align_right">-</td>
                                <td class="align_right">${formatLang(sentido * total_exenta, digits=2)}</td>
                            </tr>
                        %endif
                        <tr>
                            %if base_exenta == "0,00":
                                <td colspan="5">&nbsp;</td>
                            %else:
                                <td colspan="4">&nbsp;</td>
                                <td class="bold align_right">${formatLang(sentido * inv.amount_total, digits=2)|entity}</td>
                            %endif
                        </tr>
                    </table>
                    <table class="taula_base_imponible">
                        <tr>
                            <td>${_("*Ver detalle al dorso")}</td>
                            <td colspan="4">&nbsp;</td>
                        </tr>
                    </table>
                </div>

                <div style="clear: both;"></div>
                <div class="head_datos background_verd top-10">
                    <p class="bold">${_("Historial de consumo")}</p>
                </div>
                <div style="clear: both;"></div>
                <div class="body_datos top-10">
                    <div class="chart_consum_container">
                        <div class="chart_consum" id="chart_consum_${inv.id}"></div>
                    </div>
                    <div id="text_consums">
                        <%
                            historic = get_historics(cursor, info['giscedata_polissa_id'],inv.data_inici, inv.data_final)
                            historic_graf = {}
                            periodes_graf = []
                            consums = {}
                            historic_dies = 0
                            total_historic_eur = 0

                            for row in historic:
                                    historic_graf.setdefault(row['mes'],{})
                                    historic_graf[row['mes']].update({row['periode']: row['consum']})
                                    periodes_graf.append(row['periode'])

                            periodes_graf = list(set(periodes_graf))
                            consums_mig = dict.fromkeys(list(set(periodes_graf)), 0)
                            consums_mig.update({'total':0})
                            historic_js = []
                            for mes, consums in historic_graf.items():
                                    p_js = {'mes': mes}
                                    for p in periodes_graf:
                                        p_js.update({p: consums.get(p,0.0)})
                                        consums_mig[p] += consums.get(p,0.0)
                                    historic_js.append(p_js)
                                    consums_mig['total'] += 1
                            if consums:
                                    consums_mig = dict((k, v/consums_mig['total']) for k, v in consums.items() if k.startswith('P'))
                                    consums_mig = sorted(consums_mig.items(),key=itemgetter(1),reverse=True)
                            if historic:
                                    total_historic_kw = sum([h['consum'] for h in historic])
                                    total_historic_eur = sum([h['facturat'] for h in historic])
                                    data_ini = min([h['data_ini'] for h in historic])
                                    data_fin = max([h['data_fin'] for h in historic])
                                    historic_dies = 1 + (datetime.strptime(data_fin, '%Y-%m-%d') - datetime.strptime(data_ini, '%Y-%m-%d')).days

                            mes_any_inicial = (datetime.strptime(inv.data_inici,'%Y-%m-%d') - timedelta(days=365)).strftime("%Y/%m")
                            total_any = sum([h['consum'] for h in historic if h['mes'] > mes_any_inicial])
                            altres_lines = [l for l in inv.linia_ids if l.tipus in 'altres']
                            exces_lines = [l for l in inv.linia_ids if l.tipus == 'exces_potencia']
                        %>
                        <table id="taula_text_consums">
                            <tr>
                                <td>
                                    ${_('Su consumo medio diario en el periodo facturado ha sido de')} <b>${formatLang(diari_factura_actual_kwh, digits=0)} kWh</b>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    ${'Su consumo medio diario en los últimos %.0f meses ha sido de <b>%s kWh</b>'% (
                                    (historic_dies*1.0 / 30), formatLang((total_historic_eur * 1.0) / (historic_dies or 1.0))
                                    )}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    ${_('Su consumo acumulado del último año ha sido de')} <b>${formatLang(total_any, digits=0)} kWh</b>
                                </td>
                            </tr>
                        </table>
                        <div class="efr_img">
                            <img class="altres_img" src="${addons_path}/giscedata_facturacio_comer_ab/report/efr.png" >
                        </div>
                    </div>
                </div>
                <div id="info_costat">
                    ${_("Ab energía 1903, S.L.U. Inscrita en el Reg. Mercantil de Huesca Tomo: 614. Libro: 0. Folio: 47. Sección: 8. Hoja: HU-10282. Inscripción: 23 - CIF: B22350466. Domicilio social: Escuelas Pías, 12, 22300 Barbastro")}
                </div>
                <div class="nota top-2">
                    ${_("Para reclamaciones sobre el contrato de suministro o facturaciones podrá dirigirse en el caso de tratarse de una persona física a la Conserjería del órgano competente en materia de consumo de su Comunidad Autónoma")}
                    <br><br>
                    ${_("De conformidad con los dispuesto en las normativas vigentes en protección de datos personales, el Reglamento (UE) 2016/679 de 27 de abril de 2016 (RGPD), le informamos que sus datos personales son tratados bajo la responsabilidad de Ab energía 1903, S.L.U. para el envío de comunicaciones sobre nuestros productos y servicios y se conservarán mientras exista un interés mutuo para ello. Puede ejercer los derechos de acceso, rectificación, portabilidad y supresión de sus datos y los de limitación y oposición a su tratamiento dirigiéndose a C/ Escuelas Pías 12, 22300 – Barbastro (Huesca) / info@abenergia.es")}
                </div>
                <p style="page-break-after:always"></p>
            <div class="back_page">
                <div class="w_48p float_left _mr_12" >
                    <div class="head_datos background_verdblau">
                        <p class="bold">${_("Cálculo de la factura")}</p>
                    </div>
                    <div class="body_datos border_verdblau">
                        <table id="table_potencies">
                            <%
                                potencies = sorted(sorted(inv.linies_potencia, key=attrgetter('data_desde')), key=attrgetter('name'))
                                total_factura = 0
                            %>
                            %if potencies:
                                <tr>
                                    <td class="bold zero_padding pl_5">
                                        ${_('Término de potencia')}
                                    </td>
                                    <td class="bold zero_padding pl_5">
                                        ${_("Tipo de IVA")}
                                    </td>
                                    <td></td>
                                </tr>
                            %for pot in potencies:
                                <tr>
                                    <td class="pl_15">${pot.name}

                                    %if pot.discount != 0:
                                        ${formatLang(pot.discount, digits=0)}% Dto:
                                    %elif pot.isdiscount:
                                        ${formatLang(get_line_discount(pot)[0], digits=1)}% Dto:
                                    %endif

                                    ${pot.quantity} KW x ${pot.multi} Días x ${formatLang(pot.price_unit_multi, digits=6)} €</td>
                                    <%
                                        act = (sentido * pot.price_subtotal)
                                        total_factura += act
                                        iva_actual = get_iva(pot)
                                    %>
                                    <td class="align_right zero_padding pr_5">${iva_actual}</td>
                                    <td class="align_right w_25p">${formatLang(sentido * pot.price_subtotal, digits=2)} €</td>
                                </tr>
                            %endfor

                                <tr>
                                    <td colspan="3"></td>
                                </tr>
                            %endif
                            <%
                                #exces_potencies = get_linies_tipus(cursor, factura['id'], ['exces_potencia'])
                            %>
                            %if exces_lines:
                                <tr>
                                    <td colspan="3" class="bold zero_padding pl_5">
                                        ${_('Excesos de potencia (Maxímetros)')}
                                    </td>
                                </tr>
                            %for exc in exces_lines:
                                <tr>
                                    <td class="pl_15">${exc.name} ${exc.quantity} ${exc.uos_id.name} x ${exc.multi} x ${formatLang(exc.price_unit_multi, digits=6)} €</td>
                                    <%
                                        act = (sentido * exc.price_subtotal)
                                        total_factura += act
                                        iva_actual = get_iva(exc)
                                    %>
                                    <td class="align_right zero_padding pr_5">${iva_actual}</td>
                                    <td class="align_right w_25p">${formatLang(sentido * exc.price_subtotal, digits=2)} €</td>
                                </tr>
                            %endfor

                                <tr>
                                    <td colspan="3"></td>
                                </tr>
                            %endif
                            <%
                                energia = sorted(sorted(inv.linies_energia, key=attrgetter('price_unit_multi'), reverse=True), key=attrgetter('name'))
                            %>
                            %if energia:
                                <tr>
                                    <td colspan="3" class="bold zero_padding pl_5">
                                        ${_('Término de energía')}
                                    </td>
                                </tr>
                            <%
                                total_energia = 0
                                total_energia_discount = 0
                                single_price_total_kwh = 0
                                single_price_total_kwh_discount = 0
                                single_price_unit = 0
                                single_price_unit_discount = 0
                                total_single_price = 0
                                total_single_price_discount = 0
                                total_single_discount = 0
                                single_price = inv.llista_preu.single_price()
                                if single_price:
                                    single_price_unit = energia[0].price_unit_multi
                                    for ene in energia:
                                        if ene.discount != 0:
                                            total_single_discount = formatLang(ene.discount, digits=1)
                                            single_price_unit_discount = ene.price_unit_multi
                                            break
                                        elif ene.isdiscount:
                                            total_single_discount = formatLang(get_line_discount(ene)[0], digits=1)
                                            single_price_unit_discount = ene.price_unit_multi
                                            break
                            %>
                            %for ene in energia:
                                <%
                                    act = (sentido * ene.price_subtotal)
                                    total_factura += act
                                    iva_actual = get_iva(ene)
                                %>
                                <%
                                    if single_price:

                                        if ene.discount != 0 or ene.isdiscount:
                                            total_single_price_discount += ene.price_subtotal
                                            single_price_total_kwh_discount += ene.quantity
                                        else:
                                            total_single_price += ene.price_subtotal
                                            single_price_total_kwh += ene.quantity
                                %>
                                %if not single_price:
                                    <tr>
                                        <td class="pl_15">
                                            ${ene.name}
                                            %if ene.discount != 0:
                                              <%
                                                total_energia_discount += int(ene.quantity)
                                              %>
                                            ${formatLang(ene.discount, digits=0)}% Dto:
                                            %elif ene.isdiscount:
                                            ${formatLang(get_line_discount(ene)[0], digits=1)}% Dto:
                                              <%
                                                total_energia_discount += int(ene.quantity)
                                              %>
                                            %else:
                                            <%
                                                total_energia += int(ene.quantity)
                                            %>
                                            %endif
                                            ${int(ene.quantity)} kWh x ${formatLang(ene.price_unit_multi, digits=6)} €/kWh
                                            %if check_ajuste(ene, inv.lectures_energia_ids, 'activa', inv.tarifa_acces_id.id):
                                                *
                                                <%
                                                    show_ajust = True
                                                %>
                                            %endif
                                        </td>
                                        <td class="align_right zero_padding pr_5">${iva_actual}</td>
                                        <td class="align_right w_25p">${formatLang(sentido * ene.price_subtotal, digits=2)} €</td>
                                    </tr>
                                %endif
                            %endfor
                            %if single_price:
                                <%
                                   name_periods = '+'.join([x.name for x in energia if not (x.isdiscount or x.discount !=0)])
                                %>
                                <tr>
                                    <td class="pl_15">
                                        ${name_periods} ${int(single_price_total_kwh)} kWh x ${formatLang(single_price_unit, digits=6)} €/kWh
                                    </td>
                                    <td class="align_right zero_padding pr_5">${iva_actual}</td>
                                    <td class="align_right w_25p">${formatLang(sentido * total_single_price, digits=2)} €</td>
                                </tr>
                                %if total_single_discount > 0:
                                    <tr>
                                        <td class="pl_15">
                                            Dto: ${total_single_discount}% ${name_periods} ${int(single_price_total_kwh_discount)} kWh x ${formatLang(single_price_unit_discount, digits=6)} €/kWh
                                        </td>
                                        <td class="align_right zero_padding pr_5">${iva_actual}</td>
                                        <td class="align_right w_25p">${formatLang(sentido * total_single_price_discount, digits=2)} €</td>
                                    </tr>
                                %endif
                            %endif

                                <tr>
                                    <td colspan="3"></td>
                                </tr>
                            %endif
                            <%
                                react = sorted(sorted(inv.linies_reactiva, key=attrgetter('data_desde')), key=attrgetter('name'))
                            %>
                            %if react:
                                <tr>
                                    <td colspan="3" class="bold zero_padding pl_5">
                                        ${_('Reactiva')}
                                    </td>
                                </tr>
                            %for ene in react:
                                <tr>
                                    <td class="pl_15">
                                        ${ene.name}
                                        %if ene.discount != 0:
                                        ${formatLang(ene.discount, digits=0)}% Dto:
                                        %elif ene.isdiscount:
                                        ${formatLang(get_line_discount(ene)[0], digits=1)}% Dto:
                                        %endif
                                        ${ene.quantity} kVArh x ${formatLang(ene.price_unit_multi, digits=6)} €/kVArh
                                        %if check_ajuste(ene, inv.lectures_energia_ids, 'reactiva', inv.tarifa_acces_id.id):
                                            *
                                            <%
                                                show_ajust = True
                                            %>
                                        %endif
                                    </td>
                                    <%
                                        act = (sentido * ene.price_subtotal)
                                        total_factura += act
                                        iva_actual = get_iva(ene)
                                    %>
                                    <td class="align_right zero_padding pr_5">${iva_actual}</td>
                                    <td class="align_right w_25p">${formatLang(sentido * ene.price_subtotal, digits=2)} €</td>
                                </tr>
                            %endfor

                                <tr>
                                    <td colspan="3"></td>
                                </tr>
                            %endif
                            %if altres_lines:
                                <tr>
                                    <td colspan="3" class="bold zero_padding pl_5">
                                        ${_('Otros')}
                                    </td>
                                </tr>
                            %for ene in altres_lines:
                                <tr>
                                    <td class="pl_15">${ene.name}</td>
                                    <%
                                        act = (sentido * ene.price_subtotal)
                                        total_factura += act
                                        iva_actual = get_iva(ene)
                                    %>
                                    <td class="align_right zero_padding pr_5">${iva_actual}</td>
                                    <td class="align_right w_25p">${formatLang(sentido * ene.price_subtotal, digits=2)} €</td>
                                </tr>
                            %endfor

                                <tr>
                                    <td colspan="3"></td>
                                </tr>
                            %endif
                            %for alq in inv.linies_lloguer:
                                <tr>
                                    <td class="bold zero_padding pl_5">${_('Importe alquiler')}</td>
                                    <%
                                        act = (sentido * alq.invoice_line_id.price_subtotal)
                                        total_factura += act
                                        iva_actual = get_iva(alq)
                                    %>
                                    <td class="align_right zero_padding pr_5">${iva_actual}</td>
                                    <td class="align_right w_25p">${formatLang(sentido * alq.invoice_line_id.price_subtotal, digits=2)} €</td>
                                </tr>
                            %endfor
                            %if inv.tax_line:
                                <tr>
                                    <td colspan="3" class="bold zero_padding pl_5">
                                        ${_('Impuesto Electricidad')}
                                    </td>
                                </tr>
                            <%
                                base = 0.0
                                total_iese = 0.0
                                total_iva = 0.0
                            %>
                            %for t in inv.tax_line :
                                %if 'especial sobre la electricidad'.upper() in t.name.upper():
                                    <%
                                        base +=  t.base
                                        total_iese += t.amount
                                    %>
                                %elif 'IVA' in t.name.upper():
                                    <% total_iva += t.amount %>
                                %endif
                                <%
                                    iva_actual = get_iva(t, 'iese')
                                %>
                            %endfor
                                <tr>
                                    <%
                                       fiscal_percent = ''
                                       cl_percent='pl_15'
                                       full_excempt = False
                                    %>
                                    %if inv.fiscal_position and 'IESE' in inv.fiscal_position.name :
                                        <%
                                            fiscal_percent = 'Exento 100% de '
                                            cl_percent='pl_fiscal_position'
                                            full_excempt = True
                                        %>
                                        %if '90' in inv.fiscal_position.name :
                                          <%
                                              fiscal_percent = 'Exento 90% del 85% de '
                                              full_excempt = False
                                          %>
                                        %elif '95' in inv.fiscal_position.name :
                                          <%
                                              fiscal_percent = 'Exento 95% del 85% de '
                                              full_excempt = False
                                          %>
                                        %elif '98' in inv.fiscal_position.name :
                                          <%
                                              fiscal_percent = 'Exento 98% del 85% de '
                                              full_excempt = False
                                          %>
                                        %elif '100' in inv.fiscal_position.name :
                                          <%
                                              fiscal_percent = 'Exento 100% del 85% de '
                                              full_excempt = False
                                          %>
                                        %elif '85' in inv.fiscal_position.name :
                                          <%
                                              fiscal_percent = 'Exento 85% de '
                                              full_excempt = True
                                          %>
                                        %endif
                                    %endif
                                    % if not full_excempt:
                                        <td class="${cl_percent}">${fiscal_percent}5,11269632 % ${_('sobre')} ${formatLang(sentido * base, digits=2)}€</td>
                                    % else:
                                        <td class="${cl_percent}">${fiscal_percent}5,11269632 %</td>
                                    % endif
                                    <%
                                        act = (sentido * total_iese)
                                        total_factura += act
                                    %>
                                    <td class="align_right zero_padding pr_5">${iva_actual}</td>
                                    <td class="align_right w_25p">${formatLang(sentido * total_iese, digits=2)} €</td>
                                </tr>
                            %endif
                        </table>
                        <table class="_mt_10 f_9">
                            <tr>
                                <td class="w_170"></td>
                                <td class="zero_padding p_10 bold align_left">
                                    ${_('Total')}
                                </td>
                                <td class="zero_padding p_10 bold align_right w_70">
                                    ${formatLang(total_factura, 2)}
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td class="zero_padding p_10 bold align_left">
                                    ${_('Total IVA')}
                                </td>
                                <td class="zero_padding p_10 bold align_right w_70">
                                    ${formatLang(sentido * total_iva, digits=2)}
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td class="zero_padding p_10 bold align_left">
                                    ${_('Total factura')}
                                </td>
                                <td class="zero_padding p_10 bold align_right w_70">
                                    ${formatLang(sentido * inv.amount_total, digits=2)|entity}
                                </td>
                            </tr>
                        </table>
                        %if polissa.mode_facturacio == 'index':
                            <%
                                coef_k = inv.polissa_id.coeficient_k
                                honoraris = total_energia * (coef_k / 1000.0)
                                show_honors = inv.has_minim_honors_idx()
                            %>
                            %if show_honors and inv.polissa_id.facturar_honorarios:
                            <table>
                                <tr>
                                    <td class="zero_padding pl_5 f_7 pt_5">${_("Importe correspondiente a Honorarios de Gestión: {} €".format(formatLang(honoraris, 2)))}</td>
                                </tr>
                            </table>
                            %endif
                        %endif
                    </div>
                    <div id="nota_reactiva">
                        %if show_ajust:
                            <p>${_("* Este consumo está fijado con un ajuste por periodo por parte de la Distribuidora")}</p>
                        %endif
                    </div>
                </div>

                <div class="contenidor_resum float_right _ml_12">
                    <div class="head_datos background_verd">
                        <p class="bold">${_("Resumen de lecturas (en kWh)")}</p>
                    </div>
                </div>
            <div class="w_48p float_right _ml_12 f_9">
                <div class="p_5 a1 w_6p border_verd_1px float_left">
                    <table class="center bold">
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                    </table></div>
                <div class="p_5 b1 w_105 border_verd_1px float_left">
                    <table class="center bold">
                        <tr>
                            <td colspan="2">${_("Activa")}</td>
                        </tr>
                        <tr>
                            <td>${_("Anterior")}</td>
                            <td>${_("Actual")}</td>
                        </tr>
                    </table>
                </div>
                <div class="p_5 c1 w_105 border_verd_1px float_left">
                    <table class="center bold">
                        <tr>
                            <td colspan="2">${_("Reactiva")}</td>
                        </tr>
                        <tr>
                            <td>${_("Anterior")}</td>
                            <td>${_("Actual")}</td>
                        </tr>
                    </table>
                </div>
                <div class="p_5 d1 w_44 border_verd_1px float_left">
                    <table class="center bold">
                        <tr>
                            <td>${_("Punta")}</td>
                        </tr>
                        <tr>
                            <td>${_("Máx.")}</td>
                        </tr>
                    </table>
                </div>

            <%
                periodes_a = sorted(sorted(
                                list([lectura for lectura in inv.lectures_energia_ids
                                                if lectura.tipus == 'activa']),
                                key=attrgetter('name')
                                ), key=attrgetter('comptador'), reverse=True
                            )

                periodes_r = sorted(sorted(
                                list([lectura for lectura in inv.lectures_energia_ids
                                            if lectura.tipus == 'reactiva']),
                                            key=attrgetter('name')
                                            ), key=attrgetter('comptador'), reverse=True
                                         )

                periodes_m = sorted(sorted(list([lectura
                                              for lectura in inv.lectures_potencia_ids]
                                            ),
                                            key=attrgetter('name')
                                            ), key=attrgetter('comptador'), reverse=True
                                        )

                lectures=map(None, periodes_a, periodes_r, periodes_m)

                comptador_actual = None
                comptador_anterior = None

                comptadors = {}
                llista_lect = []
                data_actual = None
                for lect in lectures:
                    if lect[0]:
                        comptador_actual = lect[0].comptador
                        data_ant = lect[0].data_anterior
                        data_act = lect[0].data_actual
                    elif lect[1]:
                        comptador_actual = lect[1].comptador
                        data_ant = lect[1].data_anterior
                        data_act = lect[1].data_actual
                    elif lect[2]:
                        comptador_actual = lect[2].comptador
                        data_ant = lect[2].data_anterior
                        data_act = lect[2].data_actual

                    if comptador_anterior and comptador_actual != comptador_anterior:
                        comptadors[(comptador_anterior, data_actual_pre, data_ant_pre)] = llista_lect
                        llista_lect = []

                    if data_act:
                        comptadors[(comptador_actual, data_act, data_ant)] = llista_lect
                        llista_lect.append((lect[0], lect[1], lect[2]))

                        comptador_anterior = comptador_actual
                        data_actual_pre = data_act
                        data_ant_pre = data_ant
            %>
            <% x = 0 %>
            %if not comptadors:
                <div style="clear: both"></div>
                <div class="p_5 a2_fin w_6p border_verd_1px float_left">&nbsp;</div>
                <div class="p_5 b2_fin w_105 border_verd_1px float_left">&nbsp;</div>
                <div class="p_5 c2_fin w_105 border_verd_1px float_left">&nbsp;</div>
                <div class="p_5 d2_fin w_44 border_verd_1px float_left">&nbsp;</div>
            %endif
            %for compt_act in comptadors:
                <div style="clear: both"></div>
                <div class="contador p_5 float_left">${_('Contador')} ${compt_act[0]} &nbsp;&nbsp;&nbsp; ${compt_act[2] or ''}
                    ${get_origen(comptadors[compt_act][0][0].origen_anterior_id.codi)}
                    <% pleft = 67 + round(len(compt_act[0]) * 5.72) %>
                    <div style="padding-left: ${pleft}px;    clear: both;    display: inline;">
                       ${compt_act[1] or ''} ${get_origen(comptadors[compt_act][0][0].origen_id.codi)}
                    </div>
                </div>

            %if x == len(comptadors)-1:
                <div class="p_5 a2_fin w_6p border_verd_1px float_left">
            %else:
                <div class="p_5 a2 w_6p border_verd_1px float_left">
            %endif
                <table>
                    %for lect in comptadors[compt_act]:
                        <tr>
                            <td class="center">
                                % if lect[0]:
                                                ${lect[0].name[-3:-1]}
                                % elif lect[1]:
                                                 ${lect[1].name[-3:-1]}
                                % elif lect[2]:
                                                 ${lect[2].name}
                                % endif
                            </td>
                        </tr>
                    %endfor
                </table>
            </div>

            %if x == len(comptadors)-1:
                <div class="p_5 b2_fin w_105 border_verd_1px float_left">
            %else:
                <div class="p_5 b2 w_105 border_verd_1px float_left">
            %endif
                <table>
                    %for lect in comptadors[compt_act]:
                        <tr>
                            %if lect[0]:
                                <td class="align_right">${int(lect[0].lect_anterior)}</td>
                                <td class="align_right">${int(lect[0].lect_actual)}</td>
                            %else:
                                <td>&nbsp;</td><td>&nbsp;</td>
                            %endif
                        </tr>
                    %endfor
                </table>
            </div>

            %if x == len(comptadors)-1:
                <div class="p_5 c2_fin w_105 border_verd_1px float_left">
            %else:
                <div class="p_5 c2 w_105 border_verd_1px float_left">
            %endif
                <table>
                    %for lect in comptadors[compt_act]:
                        <tr>
                            %if lect[1]:
                                <td class="align_right">${int(lect[1].lect_anterior)}</td>
                                <td class="align_right">${int(lect[1].lect_actual)}</td>
                            %else:
                                <td>&nbsp;</td><td>&nbsp;</td>
                            %endif
                        </tr>
                    %endfor
                </table>
            </div>

            %if x == len(comptadors)-1:
                <div class="p_5 d2_fin w_44 border_verd_1px float_left">
            %else:
                <div class="p_5 d2 w_44 border_verd_1px float_left">
            %endif
                <table>
                    %for lect in comptadors[compt_act]:
                        <tr>
                            %if lect[2]:
                                <td class="align_right">${formatLang(lect[2].pot_maximetre, digits=2)}</td>
                            %else:
                                <td>&nbsp;</td><td>&nbsp;</td>
                            %endif
                        </tr>
                    %endfor
                </table>
            </div>
            <% x += 1 %>
            %endfor
                % if inv.has_tec271_line():
                    <div class="no_float">
                        <%
                            cups = polissa_obj.read(cursor, uid, inv.polissa_id.id, ['cups'])['cups'][1]
                        %>
                        <div id="report_sups">
                            <p>Tabla detallada Suplementos Autonómicos 2013 (*)</p>
                            <div style="border-right:1px solid black;">
                                ${sup_territorials_2013_tec271_comer_obj.get_info_html(cursor, uid, cups)}
                            </div>
                            <p>En caso de que el importe total de la regularización supere los dos euros, sin incluir impuestos, el mismo será fraccionado en partes iguales superiores a 1€ por las empresas comercializadoras en las facturas que se emitan en el plazo de 12 meses a partir de la primera regularización.</p>
                            <p>* Tabla informativa conforme a lo establecido en la TEC/271/2019 de 6 de marzo, por la cual le informamos de los parámetros para el cálculo de los suplementos territoriales facilitados por su empresa distribuidora ${polissa.distribuidora.name}.</p>
			% if inv.polissa_id.cups.id_provincia.code in ('08', '17', '43', '25'):
			    <p>Por ello Aguas de Barbastro Energía, S.L. va a proceder a regularizar las cantidades que resultan de la diferencia entre la aplicación del valor fijado en concepto de suplemento territorial de dicha Comunidad Autónoma, respecto a las cantidades que provisionalmente fueron refacturadas en virtud de lo establecido en la Orden ETU 35/2017, de 23 de enero.</p>
			% endif
                        </div>
                    </div>
                % endif
            </div>
            <div style="clear: both;"></div>
            % if inv.nota_informativa:
                <div class="no_float margin_top-20">
                    <div class="head_datos background_verdoli">
                        <p><b>${_("ab energía")}</b> ${_("informa")}</p>
                    </div>
                    <div class="body_datos border_verdoli_1px">
                        <div class="p_5 f_9">
                            ${(inv.nota_informativa or '').replace('\n', '<br>')}
                        </div>
                    </div>
                </div>
                <div style="clear: both;"></div>
            % endif
        </div> <!-- END BACKPAGE -->
                <div class="no_float margin_top-20">
                    <div class="head_datos background_gris">
                        <p class="bold">${_("Información sobre su electricidad")}</p>
                    </div>
                    <p class="text_gris p_5 top-10">
                        ${_("Si bien la energía eléctrica que llega a nuestros hogares es indistinguible de la que consumen nuestros vecinos u otros consumidores ")}
                        ${_("conectados al mismo sistema eléctrico, ahora sí es posible garantizar el origen de la producción de energía eléctrica que usted consume.")}
                        <br>
                        ${_("A estos efectos se proporciona el desglose de la mezcla de tecnologias de producción nacional para así comparar los porcentajes del promedio nacional ")}
                        ${_("con los correspondientes a la energía vendida por <b>ab energía</b>.")}
                    </p>
                </div>
                <div class="float_left w_33p _mr_12 margin_top-30">
                    <div class="head_datos background_gris">
                        <p class="bold">${_("Origen de la electricidad")}</p>
                    </div>
                    <p class="text_gris p_5 f_7" style="position: relative; top: 10px;">
                    <table class="text_gris">
                        <tr>
                            <td class="bold">${_("Origen")}</td>
                            <td class="bold w_47">${_("ab energía")}</td>
                            <td class="bold w_75">${_("Mezcla de producción sist. eléctrico español")}</td>
                        </tr>
                        <tr>
                            <td>${_("Renovable")}</td>
                            <td class="align_right">5,2 %</td>
                            <td class="align_right">38,2 %</td>
                        </tr>
                        <tr>
                            <td>${_("Cogeneración de Alta Eficiencia")}</td>
                            <td class="align_right">0,7 %</td>
                            <td class="align_right">4,4 %</td>
                        </tr>
                        <tr>
                            <td>${_("Cogeneración")}</td>
                            <td class="align_right">11,3 %</td>
                            <td class="align_right">6,9 %</td>
                        </tr>
                        <tr>
                            <td>${_("CC Gas Natural")}</td>
                            <td class="align_right">19,1 %</td>
                            <td class="align_right">11,7 %</td>
                        </tr>
                        <tr>
                            <td>${_("Carbón")}</td>
                            <td class="align_right">23,7 %</td>
                            <td class="align_right">14,5 %</td>
                        </tr>
                        <tr>
                            <td>${_("Fuel/Gas")}</td>
                            <td class="align_right">4,3 %</td>
                            <td class="align_right">2,6 %</td>
                        </tr>
                        <tr>
                            <td>${_("Nuclear")}</td>
                            <td class="align_right">33,9 %</td>
                            <td class="align_right">20,7 %</td>
                        </tr>
                        <tr>
                            <td>${_("Otras")}</td>
                            <td class="align_right">1,8 %</td>
                            <td class="align_right">1 %</td>
                        </tr>
                    </table>
                    </p>
                </div>
                <div class="float_right w_65p margin_top-30">
                    <div class="head_datos background_gris">
                        <p class="bold">${_("Impacto medioambiental")}</p>
                    </div>
                    <p class="text_gris p_5 top-5">
                        ${_("El impacto ambiental de su electricidad depende de las fuentes energéticas utilizadas para su generación. <br>")}
                            ${_("En una escala de A a G donde A indica el mínimo impacto ambiental y G el máximo, y que el valor medio nacional corresponde ")}
                            ${_("al nivel D, la energía comercializada por <b>ab energía</b> tiene los siguientes valores:")}
                    <div id="emisions">
                        <table class="quadre_imp">
                            <tr>
                                <td colspan="2" class="h_20">${_("Emisiones de dióxido de carbono <br><b>ab energía</b>")}</td>
                            </tr>
                            <tr>
                                <td class="h_60 no_right_padding">
                                    <div class="barra_grisa w_25">A</div>
                                    <div class="punta">&nbsp;</div>
                                    <div style="clear: both;"></div>
                                    <div class="barra_grisa w_40">B</div>
                                    <div class="punta">&nbsp;</div>
                                    <div style="clear: both;"></div>
                                    <div class="barra_grisa w_55">C</div>
                                    <div class="punta">&nbsp;</div>
                                    <div style="clear: both;"></div>
                                    <div class="barra_blanca w_70">D</div>
                                    <div class="punta_gris">&nbsp;</div>
                                    <div class="punta_blanc">&nbsp;</div>
                                    <div style="clear: both;"></div>
                                    <div class="barra_grisa w_85">E</div>
                                    <div class="punta">&nbsp;</div>
                                    <div style="clear: both;"></div>
                                    <div class="barra_grisa w_100">F</div>
                                    <div class="punta">&nbsp;</div>
                                    <div style="clear: both;"></div>
                                    <div class="barra_grisa w_115">G</div>
                                    <div class="punta">&nbsp;</div>
                                    <div style="clear: both;"></div>
                                </td>
                                <td class="h_60 w_28 zero_padding" style="position: relative; top: 18px; left: -4px;">
                                    <div style="clear: both;"></div>
                                    <div class="barra_blanca_inv w_10">E</div>
                                    <div class="punta_gris_inv">&nbsp;</div>
                                    <div class="punta_blanc_inv">&nbsp;</div>
                                    <div style="clear: both;"></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="h_45">
                                    ${_("Contenido de carbono")}<br>
                                    ${_("Kilogramo dióxido de carbono por kWh")}
                                </td>
                                <td class="h_45">
                                    <b>0,40</b>
                                </td>
                            </tr>
                        </table>
                        <div>
                            <table class="mitjana_nacional">
                                <tr>
                                    <td class="separador">${_("Media<br>Nacional")}</td>
                                </tr>
                                <tr>
                                    <td class="pl_12">0,30</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div id="residus">
                        <table class="quadre_imp">
                            <tr>
                                <td colspan="2" class="h_20">${_("Residuos radiactivos Alta Actividad <br><b>ab energía</b>")}</td>
                            </tr>
                            <tr>
                                <td class="h_60 no_right_padding">
                                    <div class="barra_grisa w_25">A</div>
                                    <div class="punta">&nbsp;</div>
                                    <div style="clear: both;"></div>
                                    <div class="barra_grisa w_40">B</div>
                                    <div class="punta">&nbsp;</div>
                                    <div style="clear: both;"></div>
                                    <div class="barra_grisa w_55">C</div>
                                    <div class="punta">&nbsp;</div>
                                    <div style="clear: both;"></div>
                                    <div class="barra_blanca w_70">D</div>
                                    <div class="punta_gris">&nbsp;</div>
                                    <div class="punta_blanc">&nbsp;</div>
                                    <div style="clear: both;"></div>
                                    <div class="barra_grisa w_85">E</div>
                                    <div class="punta">&nbsp;</div>
                                    <div style="clear: both;"></div>
                                    <div class="barra_grisa w_100">F</div>
                                    <div class="punta">&nbsp;</div>
                                    <div style="clear: both;"></div>
                                    <div class="barra_grisa w_115">G</div>
                                    <div class="punta">&nbsp;</div>
                                    <div style="clear: both;"></div>
                                </td>
                                <td class="h_60 w_28 zero_padding" style="position: relative; top: 35px; left: -4px;">
                                    <div style="clear: both;"></div>
                                    <div class="barra_blanca_inv w_10">F</div>
                                    <div class="punta_gris_inv">&nbsp;</div>
                                    <div class="punta_blanc_inv">&nbsp;</div>
                                    <div style="clear: both;"></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="h_45">
                                    ${_("Residuos radiactivos")}<br>
                                    ${_("Miligramos por kWh")}
                                </td>
                                <td class="h_45">
                                    <b>0,65</b>
                                </td>
                            </tr>
                        </table>
                        <div>
                            <table class="mitjana_nacional">
                                <tr>
                                    <td class="separador">${_("Media<br>Nacional")}</td>
                                </tr>
                                <tr>
                                    <td class="pl_12">0,48</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    </p>
                </div>
                % if 'VE' in inv.llista_preu.name:
                    <div style="clear: both;"></div>
                    <div class="no_float">
                        <p class="text_gris" style="margin-top: 7px;">
                            ${_("Ab energía ha adquirido Garantías de Origen para la energía que le estamos suministrando, esto se traduce en que:")}
                            <br>
                            ${_("El 100% de su suministro procede de fuentes de energías renovables y de cogeneración de alta eficiencia.")}
                        </p>
                    </div>
                % endif
            </div>
                <div style="clear: both;"></div>
            % if num_factures < len(objects):
                <p style="page-break-after:always"></p>
            % endif
            <script>
            var factura_id = ${inv.id}
            var data_consum = ${json.dumps(sorted(historic_js, key=lambda h: h['mes']))}
            var es30 = ${len(periodes_a)>3 and 'true' or 'false'}

            </script>
            <script src="${addons_path}/giscedata_facturacio_comer/report/assets/d3.min.js"></script>
            <script src="${addons_path}/giscedata_facturacio_comer_ab/report/consum.js"></script>
            %endfor
        %endif
    </body>
</html>
