<%
    from datetime import datetime
    from bankbarcode.cuaderno57 import Recibo507

    pool = objects[0].pool
    fact_obj = pool.get('giscedata.facturacio.factura')
    inv_obj = pool.get('account.invoice')

    def clean(text):
        return text or ''

    comptador_factures = 0

    def obtenir_due_date(invoice_id):
    	f = inv_obj.browse(cursor, uid, invoice_id.id)
    	return f.date_due

    def localize_period(period, locale):
	    import babel
	    return babel.dates.format_datetime(period, 'd LLLL Y', locale=locale)
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
        <style type="text/css">
        ${css}
            body{
                font-family: helvetica;
                font-size: 11px;
                margin-right: 50px;
                margin-left: 50px;
            }
            thead{
            	display: table-header-group
            }
			tfoot{
				display: table-row-group
			}
			tr{
				page-break-inside: avoid
			}
            table{
                font-size: 10px;
                width: 98%;
                float: right;
                border-collapse: collapse;
                border: 1px solid black;
            }
            table td{
            	/*border: 1px solid black;*/
                border-bottom: 1px solid #ddd;
            	padding-left: 5px;
            	padding-right: 5px;
            }
            .taula_total{
            	position: relative;
            	top: 10px;
            	width: 300px;
            	border: 0px solid;
            	font-weight: bold;
                font-size: 16px;
            }
            div.background {
			    position    : absolute;
			    top         : -2px;
			    left        : 0px;
			    width       : 100%;
			    height      : 100%;
			    z-index:-1;
			}
            div.background img{
			    width: 100%;
			}
			#contenidor{
				position: relative;
				top: 120px;
			}
			#client{
				float: right;
				border: 1px solid #CCCCCC;
				width: 260px;
				padding: 5px;
				font-weight: bold;
				padding: 10px;
			}
			#missatge{
				position: relative;
				top: 100px;
				width: 80%;
				text-align: justify;
			}
			.capcalera{
                font-weight: bold;
                margin: 5px 10px 0 0 ;
                font-size: 10px;
                float:left;
                text-align: left;
            }
            .capcalera img{
                width: 100px;
            }
			.titol{
                top: 10px;
                width: 60%;
                font-weight: bold;
                /*border: 1px solid black;*/
                text-align: center;
                font-size: 16px;
            }
            .table_title{
            	font-weight: bold;
            	text-align: center;
            }
            .table_header{
            	margin-top: 20px;
                margin-bottom: 20px;
            	width: 70%;
                border: 2px solid #78be20;
            }
            table.table_header td{
                text-align: center;
            }
            tr.head_dades_rebut{
                border: 2px solid #78be20;
                background-color: #78be20;
            }
            tbody.dades_rebuts{
                border: 2px solid #78be20;
            }
            #rebuts{
            	position: relative;
            	top: 13px;
            }

            .header_fact{
            	border: 1px solid white;
            	border-bottom: 1px solid black;
            }
            .text_dreta{
            	text-align: right;
            }
            .text_esquerra{
            	text-align: left;
            }
            .borderless{
            	border: none;
            }
        </style>
    </head>
    <body>
    	<%def name="header_factura(fact)">
    		<center>
                <div>
                    <div class="capcalera">
                        <img src="data:image/jpeg;base64,${company.logo}"/>
                    </div>
                    <table class="taula_total">
                        <tr>
                            <td class="borderless">&nbsp;</td>
                        </tr>
                    </table>
                    <div style="clear:both"></div>
                    <div class="titol">
                        ${_("INFORMACIÓN DE LA AGRUPACIÓN DE RECIBOS")}
                    </div>
                    <div style="clear:both"></div>
                </div>
                <table class="table_header">
                    <tr class="head_dades_rebut">
                        <td class="table_title">${_("Agrupación Nº")}</td>
                        <td class="table_title">${_("CIF")}</td>
                        <td class="table_title">${_("Sociedad")}</td>
                        <td class="table_title">${_("Fecha Factura")}</td>
                    </tr>
                    <tr>
                        <td>${clean(fact.group_move_id.ref)}</td>
                        <td>${fact.group_move_id.partner_id.vat and clean(fact.group_move_id.partner_id.vat.replace('ES',''))}</td>
                        <td>${fact.group_move_id.partner_id.name}</td>
                        <td>${fact.group_move_id.date}</td>
                    </tr>
                </table>
			</center>
		</%def>
        %if objects[0].group_move_id:
            %for fact in objects:
                <%
                    setLang(fact.group_move_id.partner_id.lang)
                    cargo = True
                    total_agrupado = amount_grouped(fact, 'amount_total')
                    if total_agrupado < 0:
                        cargo = False
                %>
            	<div class="background">
    		        <img src="${addons_path}/giscedata_facturacio_comer_ab/report/FACTURA_ABE.png" >
    		    </div>
    		    <div id="contenidor">
    			    <div id="client">
                        <%
                            for l in fact.group_move_id.line_id:
                                if l.invoice:
                                    id_fact = fact_obj.search(cursor, uid, [('invoice_id', '=', l.invoice.id)])
                                    factura = fact_obj.browse(
                                        cursor, uid, id_fact[0], context={'lang':fact.group_move_id.partner_id.lang}
                                    )
                                    break
                        %>
    			    	${factura.address_contact_id.name}<br>
                        ${factura.address_contact_id.street}<br>
                        ${factura.address_contact_id.zip}&emsp;${factura.address_contact_id.city or ''|entity}&emsp;${factura.address_contact_id.state_id.name or ''|entity}
                        %if factura.address_contact_id.country_id.code != 'ES':
                            <br>${_(factura.address_contact_id.country_id.name) or ''}
                        %endif
    			    </div>
    			    <div style="clear:both"></div>
    			    <center>
    				    <div id="missatge">
    				    	<p>${_("Fecha: ")}Barbastro a ${localize_period(datetime.now(), 'es_ES')}</p>
    				    	<p>${_("Agrupación nº ")}${clean(fact.group_move_id.ref)}</p>
    				    	<br>
    				    	<p>${_("Muy Sr./a Nuestro/a:")}</p>
    				    	<p>${_("Hemos procedido a agrupar ")}${len(clean(fact.group_move_id.line_id))-1}
                                ${_("recibos correspondientes a las facturas adjuntadas de las que es titular el CIF")}
                                ${fact.group_move_id.partner_id.vat and clean(fact.group_move_id.partner_id.vat.replace('ES',''))}
                                ${_("cuyo importe total asciende a")} ${formatLang(amount_grouped(fact, 'amount_total'), monetary=True, digits=2)}€
                                ${_("con el fin de emitir un único ")}
                                %if cargo:
                                    cargo
                                %else:
                                    abono
                                %endif
                                ${_(" a su domiciliación bancaria el próximo día")} ${formatLang(obtenir_due_date(fact.invoice_id), date=True) or ''}.</p>
    				    	<p>${_("Sin otro particular quedamos a su disposición ante cualquier eventualidad que pueda surgir a través de su gestor comercial")}</p>
    				    	<p>${_("Atentamente,")}</p>
                            <p>${_("AB ENERGÍA 1903, S.L.U.")}</p>
    					</div>
    				</center>
    			</div>
    			<div style="clear:both"></div>
    			<p style="page-break-after:always"></p>
    			<div id="rebuts">
    				<table>
    					<thead>
    						<tr>
    							<td colspan="5" class="header_fact">
                                    ${header_factura(fact)}
                                </td>
    						</tr>
    						<tr class="head_dades_rebut">
    							<td class="table_title">${_("Dirección")}</td>
    							<td class="table_title text_esquerra">${_("Población")}</td>
    							<td class="table_title">${_("Referencia")}</td>
    							<td class="table_title">${_("Nº Factura")}</td>
    							<td class="table_title">${_("Importe")}</td>
    						</tr>
    					</thead>
    					<tbody class="dades_rebuts">
    						% for l in fact.group_move_id.line_id:
    							% if l.invoice:
    								<%
    									id_fact = fact_obj.search(cursor, uid, [('invoice_id', '=', l.invoice.id)])
    									factura = fact_obj.browse(cursor, uid, id_fact[0])
    								%>
    								<tr><%
                                            sentit = 1.0
                                            if factura.type in['out_refund', 'in_refund']:
                                                sentit = -1.0
                                            direccio = factura.polissa_id.cups.direccio
                                            idx_pob = direccio.rfind('(')
                                            if idx_pob != -1:
                                                direccio = direccio[:idx_pob]
                                            if factura.polissa_id.cups.id_poblacio:
                                                poblacio_cups = factura.polissa_id.cups.id_poblacio.name
                                            else:
                                                poblacio_cups = factura.polissa_id.cups.id_municipi.name

                                        %>
    									<td>${direccio}</td>
    									<td class="text_esquerra">${poblacio_cups}</td>
    									<td class="text_dreta">${factura.polissa_id.name}</td>
    									<td>
                                            ${l.invoice.number}
                                            % if factura.tipo_rectificadora in ['B', 'A', 'BRA']:
                                                abonadora de ${factura.ref.number or ''}
                                            % elif factura.tipo_rectificadora in ['R', 'RA']:
                                                rectificadora de ${factura.ref.number or ''}
                                            % endif
                                        </td>
    									<td class="text_dreta">${formatLang(float(sentit * l.invoice.amount_total), digits=2)}</td>
    								</tr>
    							% endif
    						% endfor
    					</tbody>
    				</table>
    				<table class="taula_total">
    					<tr>
    						<td class="borderless">${_("Importe Total")}</td>
    						<td class="table_title">${formatLang(amount_grouped(fact, 'amount_total'), monetary=True, digits=2)}</td>
    					</tr>
    				</table>
    			</div>
            %endfor
        %else:
            <br><br><br>
            <center>
                <b><font size="4">${_("La factura ")}${objects[0].number}${_(" no está agrupada.")}</font></b>
            </center>
        %endif
    </body>
</html>
