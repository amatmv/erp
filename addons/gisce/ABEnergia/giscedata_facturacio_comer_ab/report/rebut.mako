<%
    from datetime import datetime
    from numbertoletters import number_to_letters

    pool = objects[0].pool
    users_obj = pool.get('res.users')
    fact_obj = pool.get('giscedata.facturacio.factura')
    def clean(text):
        return text or ''

    def localize_period(period, locale):
        if period != 'False':
            import babel
            from datetime import datetime
            dtf = datetime.strptime(period, '%Y-%m-%d')
            dtf = dtf.strftime("%y%m%d")
            dt = datetime.strptime(dtf, '%y%m%d')
            return babel.dates.format_datetime(dt, 'd LLLL Y', locale=locale)
        else:
            return ''

    def get_localitat():
        usuari = users_obj.browse(cursor, uid, uid)
        return usuari.address_id.id_municipi.name
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
        <style type="text/css">
        ${css}
            body{
                font-family: helvetica;
                font-size: 11px;
                margin-right: 50px;
                margin-left: 50px;
            }
            table{
                font-size: 10px;
                width: 98%;
                float: right;
                border-collapse: collapse;
                border: 0px;
            }
            table td{
                padding-left: 5px;
                padding-right: 5px;
            }
            table td.titol{
                font-size: 7px;
                border-top: 1px solid black;
                border-left: 1px solid black;
                border-right: 1px solid black;
            }
            table td.field{
                border-bottom: 1px solid black;
                border-left: 1px solid black;
                border-right: 1px solid black;
                font-weight: bold;
                font-size: 12px;
            }
            table td.field_blank{
                font-weight: bold;
                font-size: 12px;
            }
            table td.field_bottom{
                border-bottom: 1px solid black;
                font-weight: bold;
                font-size: 12px;
            }
            table td.bold_clear{
                font-weight: bold;
                font-size: 12px;
            }
            #logo{
                position: relative;
                float: left;
                width: 10%;
                height: 400px;
            }
            #img_logo{
                position: relative;
                -ms-transform: rotate(-90deg); /* IE 9 */
                -webkit-transform: rotate(-90deg); /* Chrome, Safari, Opera */
                transform: rotate(90deg);
                left: -130px;
                top: 115px;
                height: 115px;
            }
            #rebut{
                position: relative;
                float: left;
                height: 400px;
                width: 90%;
            }
            .bank_table{
                border-bottom: 1px solid black;
                border-left: 1px solid black;
                border-right: 1px solid black;
            }
            #adreca{
                position: relative;
                top: 20px;
                float: left;
                width: 380px;
                left: 5px;
            }
            #signatura{
                position: relative;
                top: 20px;
                float: left;
                left: 15px;
                width: 220px;
            }
            .space{
                margin-top: 25px;
            }
            ul {
                list-style: none;
                margin-left:10px;
                padding:0px;
            }
            ul li:before {
                color: #60B014;
                content: '» ';
                font-size: 1.2em;
                font-weight: bold;
            }
        </style>
    </head>
    <body>
        %for fact in objects:
            <div id="logo">
                <img id="img_logo" src="data:image/jpeg;base64,${company.logo}"/>
            </div>
            <div id="rebut">
                <table class="space">
                    <tr>
                        <td class="titol" style="width: 90px;">RECIBO</td>
                        <td class="titol">LOCALIDAD DE EXPEDICIÓN</td>
                        <td class="titol" style="width: 120px;">IMPORTE TOTAL €uros</td>
                    </tr>
                    <tr>
                        % if fact.group_move_id:
                            <td class="field">${fact.group_move_id.ref}</td>
                        % elif fact.number:
                            <td class="field">${fact.number}</td>
                        % else:
                            <td class="field"></td>
                        % endif
                        <td class="field">${get_localitat()}</td>
                        <td class="field" style="text-align: right;">${formatLang(amount_grouped(fact, 'amount_total'), monetary=True)}</td>
                    </tr>
                </table>
                <table style="margin-top: -1px;">
                    <tr>
                        <td class="titol" style="width: 90px;">FECHA EMISIÓN</td>
                        <td class="titol">FECHA VENCIMIENTO</td>
                    </tr>
                    <tr>
                        <td class="field" style="width: 50%;">${localize_period(datetime.now().strftime("%Y-%m-%d"), 'es_ES')}</td>
                        <td class="field">${localize_period(fact.date_due, 'es_ES')}</td>
                    </tr>
                </table>
                <table class="space">
                    <tr>
                        <td style="width: 65px;"></td>
                        <td class="field_bottom" style="width: 150px;">Factura Nº:
                        % if fact.group_move_id:
                            ${fact.group_move_id.ref}
                        % else:
                             ${fact.number}
                        % endif
                        </td>
                        <td class="field_bottom">Fecha Factura:&emsp;${fact.date_invoice}</td>
                        <td class="field_bottom">Cliente:&emsp;${fact.polissa_id.name}</td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td class="field_bottom" style="width: 65px;">€UROS</td>
                        <td class="field_bottom">${number_to_letters(amount_grouped(fact, 'amount_total')).upper()}</td>
                    </tr>
                </table>
                <table style="border: 1px solid black; margin-top: 5px;">
                    <tr style="border: 1px solid black;">
                        <td style="border: 1px solid black;"><i>Domicilio de pago</i></td>
                        <td><i>NUM. CUENTA</i></td>
                    </tr>
                    <tr>
                        <td style="border-right: 1px solid black;">PERSONA O ENTIDAD &nbsp;&nbsp;<font size="2px"><b>${fact.partner_id.name}</b></font></td>
                        <td style="text-align: center;"><font size="2px"><b>${fact.partner_bank.iban or ''}</b></font></td>
                    </tr>
                    <tr>
                        <td class="bank_table">DIRECCIÓN &nbsp;&nbsp;<font size="2px"><b>${fact.address_invoice_id.street}</b></font></td></td>
                        <td class="bank_table"></td>
                    </tr>
                </table>
                <div style="clear:both"></div>
                <div id="adreca">
                    <table style="border: 1px solid black;">
                        <tr>
                            <td>NOMBRE Y DOMICILIO DEL PAGADOR</td>
                        </tr>
                        <tr>
                            <td class="field_blank">
                            %if fact.partner_bank.owner_id:
                                ${fact.partner_bank.owner_id.name}
                            %else:
                                <!-- Obtenir nom i domicili del pagador des del contracte -->
                                ${fact.polissa_id.pagador.name}
                            %endif
                            </td>
                        </tr>
                        <tr>
                            <td class="field_blank">
                            % if fact.partner_bank.owner_address_id:
                                ${fact.partner_bank.owner_address_id.street}
                            % else:
                                ${fact.polissa_id.pagador.address.street[0]}
                            % endif
                            </td>
                        </tr>
                        <tr>
                            <td class="field_blank">
                            % if fact.partner_bank.owner_address_id:
                                ${fact.partner_bank.owner_address_id.zip}&emsp;${fact.partner_bank.owner_address_id.state_id.name}
                            % else:
                                ${fact.polissa_id.pagador.address.zip[0]}&emsp;${fact.polissa_id.pagador.address.state_id.name[0]}
                            % endif
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="signatura">
                    <i>
                        AB ENERGÍA 1903, S.L.U.
                        <br>
                    </i>
                    P.P.
                </div>
            </div>
            % if fact.group_move_id:
                <%namespace name="helper" file="/giscedata_facturacio_comer_ab/report/group_move_helper.mako" />
                ${helper.resum_factures(fact, pool)}
                <h3><strong>Total: ${formatLang(amount_grouped(fact, 'amount_total'), monetary=True)} &euro;</strong></h3>
            % endif
        %endfor
    </body>
</html>
