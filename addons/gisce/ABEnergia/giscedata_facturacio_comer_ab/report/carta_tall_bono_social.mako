<%
    from calendar import weekday
    from datetime import datetime, timedelta
    from bankbarcode.cuaderno57 import Recibo507
    from ast import literal_eval
    import babel
    from workalendar.europe import Spain
    import time

    pool = objects[0].pool
    conf_obj = pool.get('res.config')
    pending_history_obj = pool.get('account.invoice.pending.history')
    pending_state_obj = pool.get('account.invoice.pending.state')
    account_invoice_obj = pool.get('account.invoice')
    fact_obj = pool.get('giscedata.facturacio.factura')
    imd_obj = pool.get('ir.model.data')

    imd_obj = pool.get('ir.model.data')
    prod_obj = pool.get('product.product')
    devolucions_product_id = imd_obj.get_object_reference(
        cursor, uid, 'giscedata_facturacio_devolucions',
        'product_devolucions'
    )[1]
    devolucions_product = prod_obj.browse(
        cursor, uid, devolucions_product_id
    )
    cost = int(devolucions_product.list_price)

    def clean(text):
        return text or ''
    comptador_factures = 0


    def is_printable(fact):
        bono_social_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio_comer_bono_social',
                'bono_social_pending_state_process'
        )[1]
        return fact.pending_state and fact.pending_state.process_id.id == bono_social_id and fact.pending_state.weight > 0

    banner_addicional = bool(int(conf_obj.get(cursor, uid, 'factura_banner_addicional', False)))

    carta_corte_id = imd_obj.get_object_reference(
        cursor, uid, 'giscedata_facturacio_comer_bono_social',
        'carta_avis_tall_pending_state')[1]
    dies_no_permesos = literal_eval(
        conf_obj.get(cursor, uid, 'atr_b1_dies_no_permesos', '[4, 5, 6]')
    )
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    	<link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_ab/report/carta_requerimiento_pago.css"/>
    </head>
    <body>
        <%
            contador = 0
        %>
        %for fact in objects:
            % if not is_printable(fact):
                <div class="header" style="font-size: 15px">
                    <h1>La factura ${clean(fact.number)} no está en uno de los estados pendientes de bono social distinto a Correcto</h1>
                </div>
            % else:
                <%
                    data_pago = account_invoice_obj.get_last_change_date(cursor, uid, fact.invoice_id.id, carta_corte_id, context)

                    if data_pago:
                        data_pago = datetime.strptime(data_pago, '%Y-%m-%d')
                    else:
                        data_pago = datetime.now()

                    data_possible_tall = fact.pending_state.process_id.get_cutoff_day(
                        not_allowed_days=dies_no_permesos, start_day=data_pago
                    )

                    direccio = fact.polissa_id.cups.direccio
                    data_max_pagament = fact_obj.get_cutoff_limit_date(
                                                data_possible_tall.strftime('%Y-%m-%d')
                                            )
                    data_max_pagament = datetime.strptime(
                        data_max_pagament, '%Y-%m-%d'
                    )

                %>
                <div style="clear: both;"></div>
                <div class="header">
                    <div class="logo">
                        <img class="logo_img" src="data:image/jpeg;base64,${company.logo}"/>
                    </div>
                    <div class="contacto">
                        900 1903 00 <br/>
                        www.abenergia.es
                    </div>
                </div>
                <div id="outer">
                        <p id="vertical_text">
                        Ab energía 1903, S.L.U. Inscrita en el Reg. Mercantil de Huesca Tomo: 614. Libro: 0. Folio: 47. Sección: 8. Hoja: HU-10282. Inscripción : 1-CIF: B22350466. Domicilio social: Escuelas Pías, 12, 22300 Barbastro
                        </p>
                </div>
                <div class="container">
                    <div class="title">
                        <h1>Requerimiento fehaciente de pago para consumidores en mercado libre</h1>
                    </div>
                        ${_("En Barbastro a")} ${data_pago.strftime('%d/%m/%Y')}, <br/>
                        Apreciado/a <b> ${fact.partner_id.name or ''} </b>,
                    <p>
                        <table cellspacing="0" class="contingut_taula">
                            <tr>
                                <td>
                                    Mediante la presente, se le requiere el pago de las cantidades adeudadas en concepto de consumo de energía eléctrica.
                                    <br/>
                                    A día de hoy, no ha satisfecho el pago de la factura ${fact.number} del contrato ${fact.polissa_id.name} del suministro situado en ${clean(direccio)}, emitida con fecha ${fact.date_invoice}, que asciende a un importe de ${formatLang(fact.amount_total, digits=2)} euros.
                                    <br/>
                                    De no abonarse la cantidad adeudada, a partir del día ${data_possible_tall.strftime('%d/%m/%Y')}, la empresa distribuidora podrá suspender su suministro de electricidad.
                                </td>
                            </tr>
                            <tr>
                                <td id="top_white">
                                    Puede efectuar el pago de la factura anteriormente indicada con tarjeta de crédito en cualquiera de nuestras oficinas de atención al público, o mediante el siguiente código de barras, <b>antes de las 14:00 horas del día ${(data_max_pagament).strftime('%d/%m/%Y')}</b>, a través de los cajeros de BBVA las 24 horas del día.
                                    <br/>
                                    <%def name="get_codi_barres(fact)">
                                        <%
                                            # bar code
                                            entity = ''.join([c for c in fact.polissa_id.comercialitzadora.vat if c.isdigit()])
                                            suffix = '501'
                                            due_date = datetime.combine(data_max_pagament, datetime.min.time())

                                            ref = '{}'.format(fact.id).zfill(11) # id factura
                                            notice = '000000'
                                            if fact.group_move_id:
                                                grouped = amount_grouped(fact, 'amount_total')
                                                amount = '{0:.2f}'.format(grouped) # amount grouped factura
                                            else:
                                                amount = '{0:.2f}'.format(fact.amount_total) # amount factura
                                            recibo507 = Recibo507(entity, suffix, ref, notice, amount, due_date)
                                            return recibo507
                                        %>
                                    </%def>
                                    <br/>
                                    <div class="codi_barres">
                                        <div class="w50">
                                            <%
                                                recibo507 = get_codi_barres(fact)
                                            %>
                                            <table class="codi_barres_table" style="width:100%">
                                                <tr class="codi_barres_table">
                                                    <td class="codi_barres_table bold"> Nif emisora </td>
                                                    <td class="codi_barres_table">&nbsp;</td> <!-- Suffix -->
                                                    <td class="codi_barres_table bold"> Referencia </td>
                                                    <td class="codi_barres_table bold"> Ident. </td>
                                                    <td class="codi_barres_table bold"> Importe </td>
                                                </tr>
                                                <tr class="codi_barres_table">
                                                    <td class="codi_barres_table">
                                                        ${recibo507.entity}
                                                    </td>
                                                    <td class="codi_barres_table">
                                                        ${recibo507.suffix}
                                                    </td>
                                                    <td class="codi_barres_table">
                                                        ${recibo507.ref}
                                                    </td>
                                                    <td class="codi_barres_table">
                                                        ${recibo507.notice}
                                                    </td>
                                                    <td class="codi_barres_table">
                                                        % if fact.group_move_id:
                                                            ${formatLang(amount_grouped(fact, 'amount_total'), monetary=True, digits=2)}
                                                        % else:
                                                            ${formatLang(fact.amount_total, digits=2)}
                                                        % endif
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="float_r w50 negative_margin_top">
                                            ${ recibo507.svg(writer_options={'module_height': 6, 'font_size': 7, 'text_distance': 3, 'module_width': 0.25}) }
                                        </div>
                                        <br/>
                                        Asimismo le informamos que en el caso de reincidencia en el impago de sus facturas, le será cargado un importe de ${cost} € + IVA en concepto de devolución y/o reclamación.
                                    </div>

                                </td>
                            </tr>
                        </table>
                    </p>

                    <p>
                        Sin perjuicio de lo anterior, si usted cumple los requisitos para ser consumidor vulnerable,
                        puede solicitar a una de las empresas comercializadoras de referencia acogerse al bono social,
                        que supone un descuento sobre el precio voluntario para el pequeño consumidor (PVPC).<br>
                        El cambio de modalidad en el contrato para pasar a PVPC, siempre que no se modifiquen los
                        parámetros recogidos en el contrato de acceso de terceros a la red, se llevará a cabo sin
                        ningún tipo de penalización ni coste adicional.<br>
                        Siempre que se hayan acreditado los requisitos para ser consumidor vulnerable, una vez acogido
                        al PVPC, el plazo para que su suministro de electricidad pueda ser suspendido, de no haber
                        sido abonada la cantidad adeudada, pasará a ser <b>4 meses</b> (contados siempre desde la recepción
                        del presente requerimiento fehaciente de pago).<br>
                        El enlace a la página web de la CNMC donde encontrará los datos necesarios para contactar
                        con la comercializadora de referencia es el siguiente:
                    </p>
                    <p>
                        <a href="https://www.cnmc.es/ambitos-de-actuacion/energia/mercado-electrico#listados">
                            https://www.cnmc.es/ambitos-de-actuacion/energia/mercado-electrico#listados
                        </a>
                    </p>
                    <p>
                        Adicionalmente, si usted cumple los requisitos para ser vulnerable severo, puede ponerse en
                        contacto con los servicios sociales del municipio y comunidad autónoma donde reside, para que
                        le informen sobre la posibilidad de atender el pago de su suministro de electricidad.
                        Los requisitos para ser consumidor vulnerable vienen recogidos en el artículo 3 del Real
                        Decreto 897/2017, de 6 de octubre, por el que se regula la figura del consumidor vulnerable,
                        el bono social y otras medidas de protección para los consumidores domésticos de energía eléctrica.
                    </p>
                    <p class="italic">
                        [Artículo 3. Definición de consumidor vulnerable.<br>
                        <br>
                        1. A los efectos de este real decreto y demás normativa de aplicación, tendrá la consideración
                        de consumidor vulnerable el titular de un punto de suministro de electricidad en su
                        vivienda habitual que, siendo persona física, esté acogido al precio voluntario para el
                        pequeño consumidor (PVPC) y cumpla los restantes requisitos del presente artículo.
                    </p>
                    <div class="badges_box">
                        %if banner_addicional:
                            <img class="badge" style="padding-right: 170px" src="${addons_path}/giscedata_facturacio_comer_ab/report/banners/logo115.png" />
                        %endif
                        <img class="badge" src="${addons_path}/giscedata_facturacio_comer_ab/report/efr.png" />
                        <!--<img class="badge" src="${addons_path}/giscedata_facturacio_comer_ab/report/aseme.png" />-->
                    </div>
                </div>
                <p style="page-break-after:always"></p>
                <div style="clear: both;"></div>
                <div class="header">
                    <div class="logo">
                        <img src="data:image/jpeg;base64,${company.logo}"/>
                    </div>
                    <div class="contacto">
                        900 1903 00 <br/>
                        www.abenergia.es
                    </div>
                </div>
                <div id="outer">
                        <p id="vertical_text">
                        Ab energía 1903, S.L.U. Inscrita en el Reg. Mercantil de Huesca Tomo: 614. Libro: 0. Folio: 47. Sección: 8. Hoja: HU-10282. Inscripción : 1-CIF: B22350466. Domicilio social: Escuelas Pías, 12, 22300 Barbastro
                        </p>
                </div>
                <div class="container_2">
                    <p class="italic">
                        2. Para que un consumidor de energía eléctrica pueda ser considerado consumidor vulnerable,
                        deberá cumplir alguno de los requisitos siguientes:
                    </p>
                    <p class="italic">
                        a) Que su renta o, caso de formar parte de una unidad familiar, la renta conjunta anual de la unidad familiar a que pertenezca sea igual o inferior:
                        <ul>
                            <li>
                                a 1,5 veces el Indicador Público de Renta de Efectos Múltiples (IPREM) de 14 pagas, en el caso de que no forme parte de una unidad familiar o no haya ningún menor en la unidad familiar;
                            </li>
                            <li>
                                a 2 veces el índice IPREM de 14 pagas, en el caso de que haya un menor en la unidad familiar;
                            </li>
                            <li>
                                a 2,5 veces el índice IPREM de 14 pagas, en el caso de que haya dos menores en la unidad familiar.
                            </li>
                        </ul>
                    </p>
                    <p class="italic">
                        A estos efectos, se considera unidad familiar a la constituida conforme a lo dispuesto en la
                        Ley 35/2006, de 28 de noviembre, del Impuesto sobre la Renta de las Personas Físicas y de
                        modificación parcial de las leyes de los Impuestos sobre Sociedades, sobre la Renta de no
                        Residentes y sobre el Patrimonio.
                    </p>
                    <p class="italic">
                        b) Estar en posesión del título de familia numerosa.<br>
                        c) Que el propio consumidor y, en el caso de formar parte de una unidad familiar, todos los
                        miembros de la misma que tengan ingresos, sean pensionistas del Sistema de la Seguridad Social
                        por jubilación o incapacidad permanente, percibiendo la cuantía mínima vigente en cada momento
                        para dichas clases de pensión, y no perciban otros ingresos.
                    </p>
                    <p class="italic">
                        3. Los multiplicadores de renta respecto del índice IPREM de 14 pagas establecidos en el
                        apartado 2.a) se incrementarán, en cada caso, en 0,5, siempre que concurra alguna
                        de las siguientes circunstancias especiales:
                    </p>
                    <p class="italic">
                        a) Que el consumidor o alguno de los miembros de la unidad familiar tenga discapacidad
                        reconocida igual o superior al 33%.<br>
                        b) Que el consumidor o alguno de los miembros de la unidad familiar acredite la situación
                        de violencia de género, conforme a lo establecido en la legislación vigente.<br>
                        c) Que el consumidor o alguno de los miembros de la unidad familiar tenga la condición de
                        víctima de terrorismo, conforme a lo establecido en la legislación vigente.
                    </p>
                    <p class="italic">
                        4. Cuando, cumpliendo los requisitos anteriores, el consumidor y, en su caso, la unidad
                        familiar a la que pertenezca, tenga una renta anual inferior o igual al 50% de los umbrales
                        establecidos en el apartado 2.a), incrementados en su caso conforme a lo dispuesto en el
                        apartado 3, el consumidor será considerado vulnerable severo. Asimismo, también será considerado
                        vulnerable severo cuando el consumidor, y, en su caso, la unidad familiar a que pertenezca, tenga
                        una renta anual inferior o igual a una vez el IPREM a 14 pagas o dos veces el mismo, en el caso
                        de que se encuentre en la situación del apartado 2.c) o 2.b), respectivamente.
                    </p>
                    <p class="italic">
                        5. En todo caso, para que un consumidor sea considerado vulnerable deberá acreditar el
                        cumplimiento de los requisitos recogidos en el presente artículo en los términos que se
                        establezcan por orden del Ministro de Energía, Turismo y Agenda Digital.]
                    </p>
                </div>
                <div class="badges_box">
                        %if banner_addicional:
                            <img class="badge" style="padding-right: 120px" src="${addons_path}/giscedata_facturacio_comer_ab/report/banners/logo115.png" />
                        %endif
                    <img class="badge" src="${addons_path}/giscedata_facturacio_comer_ab/report/efr.png" />
                    <!--<img class="badge" src="${addons_path}/giscedata_facturacio_comer_ab/report/aseme.png" />-->
                </div>
            % endif
            <%
                contador += 1
            %>
            %if contador < len(objects):
                <p style="page-break-after:always"></p>
                <div style="clear: both;"></div>
            %endif
        %endfor
    </body>
</html>
