<%
    from datetime import datetime
    from bankbarcode.cuaderno57 import Recibo507

    pool = objects[0].pool

    def clean(text):
        return text or ''
    comptador_factures = 0
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
        <style type="text/css">
        ${css}
            body{
                font-family: "Liberation Sans";
                font-size: 11px;
                border: 0;
                margin: 0;
            }
            table{
                font-size: 10px;                
                width: 85%;
                float: right;
                border-collapse: collapse;
                border: 1px solid black;
            }
            table td{
            	padding-left: 5px;
            	padding-right: 5px;
            }
            table td.titol{
            	font-weight: bold;
            	border: 1px solid black;
            	text-align: center;
            }
            table td.field{
            	border: 1px solid black;
            	text-align: right;
            }
            table td.import{
            	text-align: right;
            	font-size: 18px;
            	font-weight: none;
            	padding-right: 10px;
            }
            .wrapper{
                margin: 0;
                padding: 0;
                padding-top: 30px;
                height: 1060px;
            }
            .back{
                background-image: url(${addons_path}/giscedata_facturacio_comer_ab/report/FACTURA_ABE.png);
                background-size: 100%;
                background-position: 0px -30px;
                background-repeat:no-repeat;
                margin:0px;
                height: 100%;
                padding:1px;
                z-index: -1;
            }
			#contenidor{
                margin-top: 190px;
                margin-bottom: 60px;
                margin-right: 50px;
                margin-left: 50px;
			}
			#client{
				position: relative;
				float: right;
				border: 1px solid black;
				width: 240px;
				padding: 10px;
			}
			#titol{
				width: 85%;
				font-weight: bold;
				font-size: 18px;
				float: right;
				text-align: center;
				border: 1px solid black;
				background-color: #C6C6C6;
			}
			#barcode{
				width: 280px;
				height: 25px;
			}
			#barcode_number{
				font-size: 9px;
			}
			#text{
				width: 85%;
				float: right;
				font-size: 18px;
				text-align: justify;
			}
			table.taula_import{
				border: none;
			}
            .resum_factures_agrupades{
                position: relative;
                top: 50px;
                width: 85%;
                float: right;
            }
            h2{
                position: relative;
                width: 100%;
                text-align: center;
                top: 50px;
            }
        </style>
    </head>
    <body>
    	<%
	        from bankbarcode.cuaderno57 import Recibo507
    	%>
        %for fact in objects:
        	<%
                # bar code
                entity = ''.join([c for c in fact.polissa_id.comercialitzadora.vat if c.isdigit()])
                due_date = datetime.now().strftime('%Y-%m-%d')
                suffix = '501'
                ref = '{}'.format(fact.id).zfill(11) # id factura
                notice = '000000'
                if fact.group_move_id:
                    grouped = amount_grouped(fact, 'amount_total')
                    amount = '{0:.2f}'.format(grouped) # amount grouped factura
                else:
                    amount = '{0:.2f}'.format(fact.amount_total) # amount factura
                recibo507 = Recibo507(entity, suffix, ref, notice, amount, due_date)
            %>
            <%
            comptador_factures += 1;
            if fact.group_move_id:
                notfound = grouped <= 0
            else:
                notfound = fact.amount_total <=0
            %>
            % if notfound:
                <h2>${_("No se ha encontrado ningún importe a ingresar para la factura")} ${fact.number}</h2>
                % if comptador_factures<len(objects):
                    <p style="page-break-after:always"></p>
                % endif
            %else:
                <div class="wrapper">
                    <div class="back">
                        <div id="contenidor">
                            <div id="client">
                                ${fact.partner_id.title or ''|entity}  ${fact.partner_id.name |entity}<br>
                                ${fact.address_invoice_id.street or ''|entity}<br>
                                ${fact.address_invoice_id.zip or ''|entity}&emsp;&emsp;${fact.address_invoice_id.city or ''|entity}
                            </div>
                            <div style="clear:both"></div>
                            <br><br>
                            <div id="titol">
                                ${_("DOCUMENTO DE INGRESO EN CUENTA")}
                            </div>
                            <br><br><br>
                            <center>
                                <div id="barcode">
                                </div>
                                <div id="barcode_number">
                                    ${ recibo507.svg(writer_options={'module_height': 7, 'font_size': 7, 'text_distance': 3, 'module_width': 0.25}) }
                                </div>
                            </center>
                            <br><br><br>
                            <div id="text">
                                ${_("Documento informativo para el ingreso en cuenta a favor de Ab energía 1903, S.L.U. de la ")}
                                % if fact.group_move_id:
                                    agrupación
                                % else:
                                   factura
                                % endif
                                ${_(" que a continuación se detalla.")}
                            </div>
                            <table style="margin-top: 50px;">
                                <tr>
                                    <td class="titol">
                                        % if fact.group_move_id:
                                            Nº Agrupación
                                        % else:
                                            Nº Factura
                                        % endif
                                    </td>
                                    <td class="titol">Fecha Factura</td>
                                    <td class="titol">Importe</td>
                                </tr>
                                <tr>
                                    <td class="field" style="text-align: center;">
                                        % if fact.group_move_id:
                                            ${clean(fact.group_move_id.ref)}
                                        % else:
                                            ${clean(fact.number)}
                                        % endif
                                    </td>
                                    <td class="field" style="text-align: center;">${clean(fact.date_invoice)}</td>
                                    <td class="field" style="text-align: center;">
                                        % if fact.group_move_id:
                                            ${formatLang(amount_grouped(fact, 'amount_total'), monetary=True, digits=2)}
                                        % else:
                                            ${formatLang(fact.amount_total, digits=2)}
                                        % endif
                                    </td>
                                </tr>
                            </table>
                            <table style="margin-top: 20px;" class="taula_import">
                                <tr>
                                    <td class="import">
                                        Importe a ingresar
                                    </td>
                                    <td style="width: 103px; font-size: 18px; border: 1px solid black; text-align: right;">
                                        % if fact.group_move_id:
                                            ${formatLang(amount_grouped(fact, 'amount_total'), monetary=True, digits=2)}
                                        % else:
                                            ${formatLang(fact.amount_total, digits=2)}
                                        % endif
                                    </td>
                                </tr>
                            </table>
                            <div class="resum_factures_agrupades">
                                <%namespace name="helper" file="/giscedata_facturacio_comer_ab/report/group_move_helper.mako" />
                                ${helper.resum_factures(fact, pool)}
                            </div>
                            <div style="clear: both;"></div>
                        </div>
                    </div>
                </div>
                % if comptador_factures<len(objects):
                    <p style="page-break-after:always"></p>
                % endif
            % endif
        %endfor
    </body>
</html>