<%
    from calendar import weekday
    from datetime import datetime, timedelta
    from bankbarcode.cuaderno57 import Recibo507

    pool = objects[0].pool
    conf_obj = pool.get('res.config')
    fac_obj = pool.get('giscedata.facturacio.factura')

    imd_obj = pool.get('ir.model.data')
    prod_obj = pool.get('product.product')
    devolucions_product_id = imd_obj.get_object_reference(
        cursor, uid, 'giscedata_facturacio_devolucions',
        'product_devolucions'
    )[1]
    devolucions_product = prod_obj.browse(
        cursor, uid, devolucions_product_id
    )
    cost = int(devolucions_product.list_price)

    def clean(text):
        return text or ''
    comptador_factures = 0

    def localize_period(period, locale):
        import babel
        return babel.dates.format_datetime(period, "d LLLL 'de' YYYY", locale=locale)

    def obtenir_dia_pagament(date_due):
        dtf = fac_obj.get_cutoff_limit_date(date_due)
        dtf = datetime.strptime(dtf, '%Y-%m-%d')
        return dtf

    def check_baixa_tall(id_factura):
        fact_obj = pool.get('giscedata.facturacio.factura')
        fact = fact_obj.browse(cursor, uid, id_factura)
        return fact.pending_state.id == 2 and fact.polissa_id.data_baixa and fact.polissa_id.state == 'baixa'

    banner_addicional = bool(int(conf_obj.get(cursor, uid, 'factura_banner_addicional', False)))
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
        <style type="text/css">
        ${css}
            body{
                font-family: helvetica;
                font-size: 11px;
                margin-right: 50px;
                margin-left: 50px;
            }
            table{
                font-size: 10px;
                font-weight: bold;
                width: 100%;
                border-collapse: collapse;
            }
            table td{
                padding-left: 5px;
                padding-right: 5px;
            }
            table#taula_peu{
                font-size: 7px;
            }
            #capcalera{
                width: 100%;
                height: 200px;
                padding-top: 50px;
            }
            #logo{
                position: relative;
                float: left;
                width: 50%;
                height: 100%;
            }
            #logo img{
                height: 110px;
            }
            #client{
                position: relative;
                float: left;
                width: 40%;
                top: 50%;
                left: 10%;
                height: 50%;
            }
            #contingut{
                position: relative;
                top: 90px;
            }
            #peu{
                position: relative;
                float: right;
                top: 20px;
                left: -30px;
                width: 300px;
                height: 50px;
            }
            #today{
                position: relative;
                float: right;
            }
            #contingut2{
                position: relative;
                margin-bottom: 35px;
                text-align: justify;
                width: 600px;
            }
            .codi_barres{
                width: 290px;
                height: 25px;
            }
            .resum_factures_agrupades{
                position: absolute;
                left: -338px;
                top: -20px;
                width: 300px;
                font-size: 7px;
            }
            .resum_factures_agrupades h3{
                font-size: 9px;
            }
            .resum_factures_agrupades li{
                position: relative;
                left: -25px
            }
            ul {
                list-style: none;
            }

            ul li:before {
                color: #60B014;
                content: '» ';
                font-size: 1.2em;
                font-weight: bold;
            }
            .dades_impagament{
                text-align: center;
                font-size: 12px;
                border-bottom: 1px solid black;
            }
            .width_field_imp{
                width: 100px;
            }
            .remarcar{
                font-weight: bold;
                text-decoration: underline;
            }
        </style>
    </head>
    <body>
        <%def name="get_codi_barres(fact)">
        <%
            # bar code
            entity = ''.join([c for c in fact.polissa_id.comercialitzadora.vat if c.isdigit()])
            suffix = '101'
            due_date = None
            if not check_baixa_tall(fact.id):
                due_date = str(date_due) # plazo pago = fecha factura + payment_term
                if due_date:
                    suffix = '501'
                    due_date = fecha_pago
            ref = '{}'.format(fact.id).zfill(11) # id factura
            notice = '000000'
            if fact.group_move_id:
                grouped = amount_grouped(fact, 'amount_total')
                amount = '{0:.2f}'.format(grouped) # amount grouped factura
            else:
                amount = '{0:.2f}'.format(fact.amount_total) # amount factura
            recibo507 = Recibo507(entity, suffix, ref, notice, amount, due_date)
            return recibo507
        %>
    </%def>
        <%def name="capcalera(fact)">
        <div id="capcalera">
            <div id="logo">
                <img src="data:image/jpeg;base64,${company.logo}"/>
            </div>
            <div id="client">
                <table class="dades">
                        <tr><td ><b>${fact.address_contact_id.name |entity}</b></td></tr>
                        <tr><td>${fact.address_contact_id.street or ''|entity}</td></tr>
                        <tr><td>${fact.address_contact_id.street2 or ''|entity}</td></tr>
                        <tr><td>${fact.address_contact_id.zip or ''|entity} ${fact.address_contact_id.city or ''|entity}</td></tr>
                        <tr><td>${fact.polissa_id.name}</td></tr>
                </table>
                % if not check_baixa_tall(fact.id):
                    <div class="codi_barres">
                        <%
                            recibo507 = get_codi_barres(fact)
                        %>
                        ${ recibo507.svg(writer_options={'module_height': 6, 'font_size': 7, 'text_distance': 3, 'module_width': 0.25}) }
                    </div>
                % endif
            </div>
        </div>
        <div style="clear:both"></div>
    </%def>
        <%def name="peu_document(fact)">
        <%
            recibo507 = get_codi_barres(fact)
        %>
        ${_("Atentamente,")}
        <br><br>
        Ab energía 1903, S.L.U.
        <br><br><br><br>
        <table>
            <tr>
                <td class="dades_impagament" colspan="6">
                    ${_("DATOS DEL IMPAGO")}
                </td>
            </tr>
            <tr>
                <td colspan="6">&nbsp;</td>
            </tr>
            <tr>
                <td class="width_field_imp">
                    ${_("Código Cliente:")}
                </td>
                <td colspan="5">
                    ${fact.polissa_id.name}
                </td>
            </tr>
            <tr>
                <td class="width_field_imp">
                    ${_("Datos suministro:")}
                </td>
                <td colspan="5">
                    ${fact.polissa_id.titular.name}
                </td>
            </tr>
            <tr>
                <td class="width_field_imp">
                </td>
                <td colspan="5">
                    <%
                        direccio = fact.polissa_id.cups.direccio
                        try:
                            index_zip = direccio.index(fact.polissa_id.cups.dp)
                            direccio = direccio[:index_zip]
                        except:
                            pass
                    %>

                    ${clean(direccio)}<br>
                </td>
            </tr>
            <tr>
                <td class="width_field_imp">
                </td>
                <td colspan="5">
                    ${clean(fact.polissa_id.cups.dp)} ${clean(fact.polissa_id.cups.id_poblacio.name)}<br>
                </td>
            </tr>
            <tr>
                <td class="width_field_imp">
                    % if fact.group_move_id:
                        ${_("Número Agrupación:")}
                    % else:
                        ${_("Número Factura:")}
                    % endif
                </td>
                <td>
                    % if fact.group_move_id:
                        ${clean(fact.group_move_id.ref)}
                    % else:
                        ${clean(fact.number)}
                    % endif
                </td>
                <td class="width_field_imp">
                    ${_("Fecha Factura:")}
                </td>
                <td>
                    ${clean(fact.date_invoice)}
                </td>
                <td class="width_field_imp">
                    ${_("Importe:")}
                </td>
                <td>
                    % if fact.group_move_id:
                        ${formatLang(amount_grouped(fact, 'amount_total'), monetary=True, digits=2)}
                    % else:
                        ${formatLang(fact.amount_total, digits=2)} €
                    % endif
                </td>
            </tr>
        </table>
        <div id="peu">
            <table id="taula_peu">
                <tr>
                    <td>
                        ${_("NIF Emisora")}
                    </td>
                    <td>

                    </td>
                    <td>
                        ${_("Referencia")}
                    </td>
                    <td>
                        ${_("Ident.")}
                    </td>
                    <td>
                        ${_("Importe")}
                    </td>
                </tr>
                <tr>
                    <td>
                        ${recibo507.entity}
                    </td>
                    <td>
                        ${recibo507.suffix}
                    </td>
                    <td>
                        ${recibo507.ref}
                    </td>
                    <td>
                        ${recibo507.notice}
                    </td>
                    <td>
                        % if fact.group_move_id:
                            ${formatLang(amount_grouped(fact, 'amount_total'), monetary=True, digits=2)}
                        % else:
                            ${formatLang(fact.amount_total, digits=2)}
                        % endif
                    </td>
                </tr>
            </table>
            <br>
            %if fact.group_move_id:
                <div class="resum_factures_agrupades">
                    <%namespace name="helper" file="/giscedata_facturacio_comer_ab/report/group_move_helper.mako" />
                    ${helper.resum_factures(fact, pool)}
                </div>
            %endif
            <div class="codi_barres">
                % if check_baixa_tall(fact.id):
                    ${ recibo507.svg(writer_options={'module_height': 6, 'font_size': 7, 'text_distance': 3, 'module_width': 0.25}) }
                % else:
                    ${ recibo507.svg(writer_options={'module_height': 6, 'font_size': 0, 'text_distance': 3, 'module_width': 0.25}) }
                % endif
            </div>
        </div>
    </%def>
    <% total_factures = len(objects) %>
        %for fact in objects:
            <!-- we don't want to generate the report for admin B1 -->
            <% b1_ids = find_B1(cursor, uid, fact.id, context={'only_cuts': 1}) %>
            % if not b1_ids:
                % if len(objects) == 1:
                    <div id="contingut">
                        <h1>No se ha detectado ningún corte para la factura ${clean(fact.number)}</h1>
                    </div>
                    <p style="page-break-after:always"></p>
                % endif
                <%
                    total_factures -= 1
                    continue
                %>
            %else:
                <%
                    b1_01_obj = pool.get('giscedata.switching.b1.01')
                    swh_ids = b1_01_obj.search(cursor, uid, [('sw_id', '=', b1_ids[0])], limit=1)
                    swh_init = b1_01_obj.browse(cursor, uid, swh_ids[0])
                    date_due = datetime.strptime(swh_init.data_accio, '%Y-%m-%d')
                    fecha_pago = obtenir_dia_pagament(swh_init.data_accio)
                %>
            % endif
            <%
                recibo507 = False
            %>
            ${capcalera(fact)}
            % if check_baixa_tall(fact.id):
                <div id="contingut2">
                    <div id="today">
                        ${_("Barbastro a ")}${localize_period(datetime.now(), 'es_ES')}
                    </div>
                    <br><br>
                    ${_("Muy Sres. Nuestros:")}
                    <br>
                    <p>
                        ${_("Le recordamos que Ud. tenía contratado el suministro eléctrico con Ab energía en la dirección ")}
                        <b>${fact.polissa_id.cups.direccio}</b>
                        ${_("y que se ha dado de baja en fecha ")}
                        <%
                            data_baixa = datetime.strptime(fact.polissa_id.data_baixa, '%Y-%m-%d')
                            str_baixa = data_baixa.strftime('%d/%m/%Y')
                        %>
                        <b>${str_baixa}</b>.
                    </p>
                    <p>
                        ${_("Le informamos que tiene impagado el recibo de suministro correspondiente a la factura ")}
                        <b>
                            Nº${fact.number}, ${_("de fecha ")}
                            <%
                                data_fact = datetime.strptime(fact.date_invoice, '%Y-%m-%d')
                                str_fact = data_fact.strftime('%d/%m/%Y')
                            %>
                            ${str_fact} ${_("e importe")} ${formatLang(fact.amount_total, digits=2)} €.
                        </b>
                    </p>
                    <p>
                        ${_("Rogamos procedan a abonar la cantidad adecuada por alguna de las siguientes modalidades:")}
                        <ul>
                            <li>
                                ${_("Mediante pago exclusivo con <b>tarjeta de crédito</b> en cualquiera de nuestras oficinas de atención al público.")}
                            </li>
                            <li>
                                ${_("A través de <b>cajeros del BBVA</b> las 24h del día. (OPCION PAGO DE RECIBOS CON CODIGO DE BARRAS).")}
                            </li>
                        </ul>
                        ${_("De igual modo esta parte se reserva el derecho de tomar las medidas legales que le puedan corresponder, si en un breve periodo de tiempo ")}
                        ${_("no liquidan el importe adecuado.")}
                    </p>
                    <p>
                        ${_("En caso de querer volver a conectar el suministro, podrá tramitar un alta nueva en cualquiera de nuestras oficinas.")}
                    </p>
            %else:
            <div id="contingut">
                ${_("Barbastro a ")}${localize_period(datetime.now(), 'es_ES')}
                <br><br>
                ${_("Muy Sr./a Nuestro/a:")}
                <br>
                <p>
                    ${_("Le comunicamos, mediante carta certificada, que se encuentra impagado el recibo del suministro de energía eléctrica correspondiente a los datos abajo reseñados.")}
                </p>
                <p>
                    ${_("Se ha iniciado el proceso de suspensión de suministro por impago, siendo efectivo el corte a partir de las 00:00 del día")}
                    <b>${date_due.strftime('%d/%m/%Y')}.</b>
                    ${_("Su Distribuidora procederá a cortar el suministro de energía eléctrica.")}
                </p>
                <p>
                    ${_("Le rogamos que,")} <span class="remarcar">${_("antes de las 14:00 horas del día ")}
                    % if date_due != 'False':
                        ${formatLang(fecha_pago.date(), date=True)}
                    % endif
                    </span>&nbsp;&nbsp;${_("haga efectivo el pago por alguna de las siguientes modalidades, con el fin de evitar la situación de suspensión del suministro eléctrico:")}
                    <ul>
                        <li>${_("Mediante pago exclusivo con <b>tarjeta de crédito</b> en cualquiera de nuestras oficinas de atención al público.")}</li>
                        <li>${_("A través de los <b>cajeros del BBVA</b> las 24 horas del día. (OPCIÓN PAGO DE RECIBOS CON CODIGO DE BARRAS)")}</li>
                    </ul>
                </p>
                <p>
                    ${_("Asimismo le informamos que en el caso de reincidencia en el impago de sus facturas, le será cargado un importe de")}
                     <b>${cost}</b>
                    ${_("€ + IVA en concepto de devolución y/o reclamación.</span>")}
                </p>
                <p>
                    % if 'ES0116' in fact.polissa_id.cups.name:
                        ${_("Le recordamos que una vez cortado el suministro, podrá realizarse la baja administrativa a partir de los 10 días, si no indica nada contrario.")}
                    %else:
                        ${_("Le recordamos que una vez cortado el suministro, podrá realizarse la baja administrativa a partir de los 16 días, si no indica nada contrario.")}
                    % endif
                </p>
                % if fact.polissa_id.cups.id_municipi.state.comunitat_autonoma.codi == '02':
                    <p>${_("Igualmente le informamos que de conformidad con la Ley 9/2016, de 3 de noviembre, de reducción de pobreza energética de Aragón, puede acudir a los Servicios Sociales competentes para comunicar su situación.")}</p>

                % elif fact.polissa_id.cups.id_municipi.state.comunitat_autonoma.codi == '09':
                    <p>${_("Igualmente le informamos que está en vigor la Ley 24/2015, de 29 de julio, de medidas urgentes para afrontar la emergencia en el ámbito de vivienda y pobreza energética, por lo que, si no comunica nada contrario, procederemos a enviar sus datos a los Servicios Sociales.")}</p>
                % endif
                <p>
                    ${_("De igual modo esta parte se reserva el derecho de tomar las medidas legales que le puedan corresponder, si en un breve periodo de tiempo no liquidan los importes adeudados.")}
                </p>
                <br>
             %endif
            ${peu_document(fact)}
            %if banner_addicional:
                <img class="badge" style="max-height: 45; padding-right: 170px" src="${addons_path}/giscedata_facturacio_comer_ab/report/banners/logo115.png" />
            %endif
            </div><!-- DIV CONTINGUT -->

        <%
        comptador_factures += 1;
        %>
        % if comptador_factures< total_factures:
            <p style="page-break-after:always"></p>
        % endif
        %endfor
    </body>
</html>
