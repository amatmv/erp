<%
    from operator import attrgetter, itemgetter
    from datetime import datetime, timedelta
    import json
    from giscedata_facturacio_comer_ab.report.utils import pretty_date,get_giscedata_fact,get_other_info,get_discriminador,get_distri_phone,get_historics,get_linies_tipus,get_discounts
    from account_invoice_base.report.utils import localize_period
%>
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<head>
    <style type="text/css">
        ${css}
    </style>
    <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_ab/report/factura_antiga.css"/>
    <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_ab/report/consum_antic.css"/>
</head>
<body>
    <%
        pool = objects[0].pool
        polissa_obj = objects[0].pool.get('giscedata.polissa')
        comptador_factures = 0;


        def get_num_comptador(id_polissa):
            polissa_obj = pool.get('giscedata.polissa')
            polissa = polissa_obj.browse(cursor, uid, id_polissa)
            for comptador in polissa.comptadors:
                if comptador.active:
                   return comptador.name
            return '-'

        def get_lectures_periodes(factura_id):
            sql = """SELECT giscedata_facturacio_lectures_energia.name as periode
            FROM
                 giscedata_facturacio_lectures_energia giscedata_facturacio_lectures_energia
            WHERE giscedata_facturacio_lectures_energia.factura_id = %s
            GROUP BY giscedata_facturacio_lectures_energia.name
            ORDER BY giscedata_facturacio_lectures_energia.name""" % factura_id
            cursor.execute(sql)
            return cursor.dictfetchall()

        def get_lectures(factura_id):
            sql = """SELECT
                 giscedata_facturacio_lectures_energia.factura_id AS giscedata_facturacio_lectures_energia_factura_id,
                 giscedata_facturacio_lectures_energia.name AS giscedata_facturacio_lectures_energia_name,
                 giscedata_facturacio_lectures_energia.lect_actual AS giscedata_facturacio_lectures_energia_lect_actual,
                 giscedata_facturacio_lectures_energia.consum AS giscedata_facturacio_lectures_energia_consum,
                 giscedata_facturacio_lectures_energia.tipus AS giscedata_facturacio_lectures_energia_tipus,
                 giscedata_facturacio_lectures_energia.lect_anterior AS giscedata_facturacio_lectures_energia_lect_anterior
            FROM
                 giscedata_facturacio_lectures_energia giscedata_facturacio_lectures_energia
            WHERE giscedata_facturacio_lectures_energia.factura_id = %s
            ORDER BY giscedata_facturacio_lectures_energia.name,giscedata_facturacio_lectures_energia.tipus""" % factura_id

            cursor.execute(sql)
            return cursor.dictfetchall()

        def get_lectura_periode_tipus(factura_id, tipus, periode):
            sql = """SELECT
                 giscedata_facturacio_lectures_energia.factura_id AS giscedata_facturacio_lectures_energia_factura_id,
                 giscedata_facturacio_lectures_energia.name AS giscedata_facturacio_lectures_energia_name,
                 giscedata_facturacio_lectures_energia.lect_actual AS lect_actual,
                 giscedata_facturacio_lectures_energia.consum AS giscedata_facturacio_lectures_energia_consum,
                 giscedata_facturacio_lectures_energia.tipus AS giscedata_facturacio_lectures_energia_tipus,
                 giscedata_facturacio_lectures_energia.lect_anterior AS lect_anterior
            FROM
                 giscedata_facturacio_lectures_energia giscedata_facturacio_lectures_energia
            WHERE giscedata_facturacio_lectures_energia.factura_id = %s and giscedata_facturacio_lectures_energia.name = '%s'
                  and giscedata_facturacio_lectures_energia.tipus = '%s'
            ORDER BY giscedata_facturacio_lectures_energia.name,giscedata_facturacio_lectures_energia.tipus""" % (
            factura_id, periode, tipus)
            cursor.execute(sql)
            return cursor.dictfetchall()
     %>
    <%
        from datetime import datetime
        from bankbarcode.cuaderno57 import Recibo507
    %>
    %for inv in objects :
    <div class="background">
    </div>
    <div class="wrapper">
        <div class="back" style="
                background-image: url(${addons_path}/giscedata_facturacio_comer_ab/report/FACTURA_ABE.png);
                background-size: 100%;
                background-position: 0px -30px;
                background-repeat:no-repeat;
                margin:0px;
                padding:1px;
                ">
            <div class="page_princial" >

            <%
                factura = get_giscedata_fact(cursor, inv)[0]
                discounts = get_discounts(cursor, inv)
                dies_factura = 1 + (datetime.strptime(inv.data_final, '%Y-%m-%d') - datetime.strptime(inv.data_inici, '%Y-%m-%d')).days
                diari_factura_actual_eur = inv.total_energia / (dies_factura or 1.0)
                diari_factura_actual_kwh = (inv.energia_kwh * 1.0) / (dies_factura or 1.0)

                data_inici  = datetime.strptime(factura['data_inici'],'%Y-%m-%d')
                data_fi = datetime.strptime(factura['data_final'],'%Y-%m-%d')
                delta = data_fi - data_inici
                dies = delta.days
                # bar code
                entity = ''.join([c for c in inv.polissa_id.comercialitzadora.vat if c.isdigit()])
                due_date = str(inv.date_due) # plazo pago = fecha factura + payment_term
                if due_date:
                    suffix = '501'
                    days_limit = 15
                    if 'COBRADOR' in inv.payment_type.name:
                        days_limit = 5
                    due_date = datetime.strptime(inv.date_due, '%Y-%m-%d') + timedelta(days=days_limit)
                else:
                    suffix = '101'
                    due_date = None
                ref = '{}'.format(inv.id).zfill(11) # id factura
                notice = '000000'
                amount = '{0:.2f}'.format(inv.amount_total) # amount factura
                recibo507 = Recibo507(entity, suffix, ref, notice, amount, due_date)
                periodes_a = sorted(list(set([lectura.name[-3:-1]
                                for lectura in inv.lectures_energia_ids
                                if lectura.tipus == 'activa'])))
                polissa = polissa_obj.browse(cursor, uid, inv.polissa_id.id,
                                 context={'date': inv.data_final.val})
                dphone = get_distri_phone(cursor, uid, polissa)
                cphone = inv.company_id.partner_id.address[0].phone
                try:
                    distri_phone = ''.join([dphone[i:i+3] for i in range(0,len(dphone),3)])
                except:
                    raise Exception(
                        u'La distribuidora no tiene definido el teléfono de contacto'
                    )
                try:
                    comer_phone = '.'.join([cphone[i:i+3] for i in range(0,len(cphone),3)])
                except:
                    raise Exception(
                        u'La comercializadora no tiene definido el teléfono de contacto'
                    )
                entidad_bancaria = ''
                bank_acc = ''
                if inv.payment_type.code == 'RECIBO_CSB':
                    if not inv.partner_bank:
                        raise Exception(
                            u'La factura {0} no tiene número de cuenta definido.'.format(
                            inv.number)
                        )
                    else:
                        entidad_bancaria = inv.partner_bank.name or inv.partner_bank.bank.lname
                        bank_acc = inv.partner_bank.iban[:-4].replace(' ','')  + '****'
                else:
                    if inv.partner_bank:
                        entidad_bancaria = inv.partner_bank.name or inv.partner_bank.bank.lname
                        bank_acc = inv.partner_bank.iban[:-4].replace(' ','') + '****'

                pot_periode = dict([('P%s' % p,'') for p in range(1, 7)])
                for pot in polissa.potencies_periode:
                    pot_periode[pot.periode_id.name] = pot.potencia
            %>
            <% info = get_other_info(cursor, factura['id'])[0]%>
            <%
                direccio = polissa.cups.direccio
                idx_pob = direccio.rfind('(')
                if idx_pob != -1:
                    direccio = direccio[:idx_pob]
                if polissa.cups.id_poblacio:
                    poblacio_cups = polissa.cups.id_poblacio.name
                else:
                    poblacio_cups = polissa.cups.id_municipi.name
            %>

            <% setLang(inv.partner_id.lang) %>
            <!-- capcalera -->
            <div class="bloc capcalera">
                <div class="esquerra">
                    <span>&nbsp;</span>
                </div>
                <div class="dreta">
                    <% sentido = 1.0 %>
                    %if inv.type in ['out_refund', 'in_refund']:
                        <% sentido = -1.0 %>
                    %endif
                    %if inv.tipo_rectificadora in ['B', 'A', 'BRA']:
                        <div class="tipus_factura">
                            <span class="big">FACTURA DE ABONO</span><br>
                            <span>Esta factura anula la factura ${inv.ref.number or ''}</span>
                        </div>
                    %endif
                    %if inv.tipo_rectificadora in ['R', 'RA']:
                        <div class="tipus_factura">
                            <span class="big">FACTURA RECTIFICATIVA</span><br>
                            <span>Esta factura rectifica la factura ${inv.ref.number or ''}</span>
                        </div>
                    %endif
                   <div class="box">
                        <table class="dades">
                            <tr><td ><b>${inv.address_contact_id.name |entity}</b></td></tr>
                            <tr><td>${inv.address_contact_id.street or ''|entity}</td></tr>
                            <tr><td>${inv.address_contact_id.street2 or ''|entity}</td></tr>
                            <tr><td>${inv.address_contact_id.zip or ''|entity} ${inv.address_contact_id.city or ''|entity}</td></tr>
                            %if inv.address_contact_id.country_id.code != 'ES':
                                <tr><td>${inv.address_contact_id.country_id.name or ''}</td></tr>
                            %endif
                        </table>
                    </div>
                    %if inv.payment_type.code != 'RECIBO_CSB':
                        ${ recibo507.svg(writer_options={'module_height': 7, 'font_size': 7, 'text_distance': 3, 'module_width': 0.25}) }
                    %else:
                        <div class="no_bankcode"></div>
                    %endif

                </div>
                <div class="clear fi_bloc"></div>
            </div><!-- Fi capcalera -->
            <!-- part central -->
            <div class="bloc part_central">
                <div class="esquerra">
                    <div class="header box"><h2>Datos de la factura</h2></div>
                    <div class="box">
                        <table class="dades">
                            <tr><td class="desc">Fecha Emisión:</td><td>${localize_period(inv.date_invoice, 'es_ES' )}</td></tr>
                            <tr><td class="desc">Periodo de Facturación:</td><td>Del ${pretty_date(factura['data_inici'])|entity} al ${pretty_date(factura['data_final'])|entity}</td></tr>
                            <tr><td class="desc">Número de factura</td><td>${inv.number or ''|entity}</td></tr>
                            <tr><td class="desc"><span class="big">Total Factura</span></td><td class="desc"><span class="big">${formatLang(sentido * inv.amount_total, digits=2)} Euros</span></td></tr>
                        </table>
                    </div>
                    <div class="header box">Datos del suministro</div>
                    <div class="box">
                        <table class="dades">
                            <tr><td class="desc">Titular:</td><td >${inv.partner_id.title or ''|entity}  ${info['res_partner_name'] |entity}</td></tr>
                            <tr><td class="desc">Dirección:</td><td>${direccio}</td></tr>
                            <tr><td class="desc">Población:</td><td>${poblacio_cups}</td></tr>
                            <tr><td class="desc">NIF/DNI:</td><td class="desc">${info['res_partner_vat']}</td></tr>
                        </table>
                        <table class="dades">
                            <tr><td class="desc">Referencia catastral:</td><td class="desc">${info['giscedata_cups_ps_ref_cat']}</td></tr>
                        </table>
                        <table class="dades">
                            <tr><td class="desc">C.U.P.S:</td><td class="desc">${info['giscedata_cups_ps_name']}</td></tr>
                            <tr><td>C.N.A.E: ${info['giscedata_polissa_cnae'] or ''|entity}</td>
                                <td><span class="desc">Código Cliente</span> ${info['giscedata_polissa_name'] or '' | entity}</td>
                            </tr>
                        </table>
                        <table class="dades">
                            <tr><td class="desc">Vencimiento del contrato:</td><td>${inv.polissa_id.modcontractual_activa.data_final}</td></tr>
                         </table>
                         <div class="separator"><hr></div>
                         <table class="dades">
                            <tr><td class="desc">Potencia(kW):</td>
                                <td>
                                    % for periode, potencia in sorted(pot_periode.items(),key=itemgetter(0)):
                                        % if potencia:
                                            ${periode}
                                            ${formatLang(potencia, digits=3)}
                                        % endif
                                    % endfor
                                </td>
                            </tr>
                         </table>
                         <table class="dades">
                            <tr><td class="desc">Tensión:</td><td>${info['giscedata_polissa_tensio']}</td></tr>
                            <tr><td><span class="desc">Tarifa:</span> ${polissa.tarifa.name or ''|entity}</td><td><span class="desc">Discriminador</span> ${get_discriminador(cursor, factura['id'])}</td></tr>
                         </table>
                    </div>
                    <div class="header box">Resumen de Lecturas Tarifa Accesso</div>
                    <div >
                        <table class="dades_lectures">
                           <colgroup>
                              <col style="width: 16%">
                              <col style="width: 15%">
                              <col style="width: 15%">
                              <col style="width: 15%">
                              <col style="width: 15%">
                              <col style="width: 15%">
                           </colgroup>
                           <thead class="border-bottom">
                              <tr>
                                 <th class="border-right"></th>
                                 <th colspan="2">Activa</th>
                                 <th colspan="2">Reactiva</th>
                                 <th class="border-right">Punta</th>
                              </tr>
                              <tr>
                                 <th class="border-right">${polissa.tarifa.name}</th>
                                 <th>Anterior</th>
                                 <th>Actual</th>
                                 <th>Anterior</th>
                                 <th>Actual</th>
                                 <th class="border-right">Max.</th>
                              </tr>
                           </thead>
                           <tbody>
                           <%
                                periodes_a = sorted(sorted(
                                    list([lectura for lectura in inv.lectures_energia_ids
                                                    if lectura.tipus == 'activa']),
                                    key=attrgetter('name')
                                    ), key=attrgetter('comptador'), reverse=True
                                )

                                periodes_r = sorted(sorted(
                                    list([lectura for lectura in inv.lectures_energia_ids
                                                if lectura.tipus == 'reactiva']),
                                                key=attrgetter('name')
                                                ), key=attrgetter('comptador'), reverse=True
                                             )

                                periodes_m = sorted(sorted(list([lectura
                                                  for lectura in inv.lectures_potencia_ids]
                                                ),
                                                key=attrgetter('name')
                                                ), key=attrgetter('comptador'), reverse=True
                                            )
                                comptador_actual = None
                                comptador_anterior = None
                           %>
                              %for lect in map(None, periodes_a, periodes_r, periodes_m):
                                  <%
                                      if lect[0]:
                                          comptador_actual = lect[0].comptador
                                          data_ant = lect[0].data_anterior
                                          data_act = lect[0].data_actual
                                      elif lect[1]:
                                          comptador_actual = lect[1].comptador
                                          data_ant = lect[1].data_anterior
                                          data_act = lect[1].data_actual
                                      elif lect[2]:
                                          comptador_actual = lect[2].comptador
                                          data_ant = lect[2].data_anterior
                                          data_act = lect[2].data_actual
                                  %>
                                  % if comptador_actual != comptador_anterior:
                                      <tr class="contador">
                                          <td colspan="6"> contador: ${comptador_actual}
                                          <span class="dates_comptadors">${data_ant or ''} - ${data_act or ''}</span></td>
                                      </tr>
                                  % endif
                                  <tr>
                                     <th class="border-right">
                                         % if lect[0]:
                                            ${lect[0].name[-3:-1]}
                                         % elif lect[1]:
                                             ${lect[1].name[-3:-1]}
                                         % elif lect[2]:
                                             ${lect[2].name}
                                         % endif
                                     </th>
                                      <%
                                          lect_activa = lect[0]
                                          lect_reactiva = lect[1]
                                          lect_potencia = lect[2]
                                      %>
                                     %if lect_activa:
                                         <td>${int(lect_activa.lect_anterior)}</td>
                                         <td>${int(lect_activa.lect_actual)}</td>
                                     %else:
                                         <td></td><td></td>
                                     %endif
                                     %if lect_reactiva:
                                         <td>${int(lect_reactiva.lect_anterior)}</td>
                                         <td>${int(lect_reactiva.lect_actual)}</td>
                                     %else:
                                         <td></td><td></td>
                                     %endif
                                     %if lect_potencia:
                                         <td>${lect_potencia.pot_maximetre}</td>
                                     %else:
                                         <td></td>
                                     %endif
                                  </tr>
                                  <% comptador_anterior = comptador_actual %>
                              %endfor
                           </tbody>
                        </table>
                    </div>
                    <div class="clear"></div>
                    <div class="header box"><h2>Historial de Consumo</h2></div>

                        <%
                            historic = get_historics(cursor, info['giscedata_polissa_id'],inv.data_inici, inv.data_final)
                            historic_graf = {}
                            periodes_graf = []
                            consums = {}
                            historic_dies = 0
                            total_historic_eur = 0

                            for row in historic:
                                historic_graf.setdefault(row['mes'],{})
                                historic_graf[row['mes']].update({row['periode']: row['consum']})
                                periodes_graf.append(row['periode'])

                            periodes_graf = list(set(periodes_graf))
                            consums_mig = dict.fromkeys(list(set(periodes_graf)), 0)
                            consums_mig.update({'total':0})
                            historic_js = []
                            for mes, consums in historic_graf.items():
                                p_js = {'mes': mes}
                                for p in periodes_graf:
                                    p_js.update({p: consums.get(p,0.0)})
                                    consums_mig[p] += consums.get(p,0.0)
                                historic_js.append(p_js)
                                consums_mig['total'] += 1
                            if consums:
                                consums_mig = dict((k, v/consums_mig['total']) for k, v in consums.items() if k.startswith('P'))
                                consums_mig = sorted(consums_mig.items(),key=itemgetter(1),reverse=True)
                            if historic:
                                total_historic_kw = sum([h['consum'] for h in historic])
                                total_historic_eur = sum([h['facturat'] for h in historic])
                                data_ini = min([h['data_ini'] for h in historic])
                                data_fin = max([h['data_fin'] for h in historic])
                                historic_dies = 1 + (datetime.strptime(data_fin, '%Y-%m-%d') - datetime.strptime(data_ini, '%Y-%m-%d')).days

                            mes_any_inicial = (datetime.strptime(inv.data_inici,'%Y-%m-%d') - timedelta(days=365)).strftime("%Y/%m")
                            total_any = sum([h['consum'] for h in historic if h['mes'] > mes_any_inicial])
                            altres_lines = [l for l in inv.linia_ids if l.tipus in 'altres']
                          %>
                        <div class="chart_consum_container">
                            <div class="chart_consum" id="chart_consum_${inv.id}"></div>
                            <div class="desc_text">
                                <p>${'Su consumo medio diario en el periodo facturado ha sido de %s kWh' % (
                                      formatLang(diari_factura_actual_kwh, digits=0))}</p>
                                <p>${'Su consumo medio diario en los últimos %.0f meses ha sido de %s kWh'% (
                                    (historic_dies*1.0 / 30), formatLang((total_historic_eur * 1.0) / (historic_dies or 1.0))
                                    )}
                                </p>
                                <p>${'Su consumo acumulado del último año ha sido de %s kWh' % (
                                    formatLang(total_any, digits=0) )}
                                </p>
                            </div>
                          </div>
                </div>
                <div class="dreta">
                    <div class="header box"><h2>Cálculo de la factura</h2></div>
                    <div class="box dades_calculs">
                        <div class="calculs">
                            <div class="titol_taula"><h3>Término de Potencia</h3></div>
                            <table class="dades_calculs"  >
                               <colgroup>
                                  <col style="width: 6%">
                                  <col style="width: 54%">
                                  <col style="width: 15%">
                                  <col style="width: 25%">
                               </colgroup>
                               <tbody>
                                  %for pot in sorted(sorted(inv.linies_potencia, key=attrgetter('data_desde')), key=attrgetter('name')):
                                      <tr>
                                         <td>${pot.name}</td>
                                         <td colspan="2">${pot.quantity} KW x ${pot.multi} Días x ${formatLang(pot.price_unit_multi, digits=6)} €</td>
                                         <td class="total">${formatLang(sentido * pot.price_subtotal, digits=2)}</td>
                                      </tr>
                                  %endfor
                               </tbody>
                            </table>
                        </div>
                        <div class="calculs">
                            <div class="titol_taula"><h3>Excesos de potencia (Maximetros)</h3></div>
                            <table class="dades_calculs"  >
                               <colgroup>
                                  <col style="width: 6%">
                                  <col style="width: 54%">
                                  <col style="width: 15%">
                                  <col style="width: 25%">
                               </colgroup>
                               <tbody>
                                  <% x = 0 %>
                                  %for pot in get_linies_tipus(cursor, factura['id'], ['exces_potencia']):
                                      <% x += 1 %>
                                      <tr>
                                         <td>${pot['account_invoice_line_name']}</td>
                                         <td colspan="2">${pot['account_invoice_line_quantity']} ${pot['product_uom_name']} x ${pot['giscedata_facturacio_factura_linia_multi']} x ${pot['giscedata_facturacio_factura_linia_price_unit_multi']} €</td>
                                         <td class="total">${sentido * pot['account_invoice_line_price_subtotal']}</td>
                                      </tr>
                                  %endfor
                                  % if x == 0:
                                    <tr> <td></td><td></td><td></td><td></td></tr>
                                  % endif
                               </tbody>
                            </table>
                        </div>
                        <div class="calculs">
                            <div class="titol_taula"><h3>Término Energía</h3></div>
                            <table class="dades_calculs"  >
                               <colgroup>
                                  <col style="width: 6%">
                                  <col style="width: 54%">
                                  <col style="width: 15%">
                                  <col style="width: 25%">
                               </colgroup>
                               <tbody>
                                  <% x = 0 %>
                                  %for ene in sorted(sorted(inv.linies_energia, key=attrgetter('price_unit_multi'), reverse=True), key=attrgetter('name')):
                                      <% x += 1 %>
                                      <tr>
                                         <td>
                                             ${ene.name}
                                         </td>
                                         <td colspan="2">
                                             %if ene.discount != 0:
                                                ${formatLang(ene.discount, digits=0)}% Dto:
                                             %elif ene.isdiscount:
                                                ${formatLang(discounts.get(ene.product_id.id), digits=0)}% Dto:
                                             %endif
                                             ${int(ene.quantity)} kWh x ${formatLang(ene.price_unit_multi, digits=6)} €/kWh
                                         </td>
                                         <td class="total">${formatLang(sentido * ene.price_subtotal, digits=2)}</td>
                                      </tr>
                                  %endfor
                                  % if x == 0:
                                    <tr> <td></td><td></td><td></td><td></td></tr>
                                  % endif
                               </tbody>
                            </table>
                        </div>
                        <div class="calculs">
                            <div class="titol_taula"><h3>Término de Reactiva</h3></div>
                            <table class="dades_calculs small" >
                               <colgroup>
                                  <col style="width: 6%">
                                  <col style="width: 54%">
                                  <col style="width: 15%">
                                  <col style="width: 25%">
                               </colgroup>
                               <tbody>

                                  <% x = 0 %>
                                  %for ene in sorted(sorted(inv.linies_reactiva, key=attrgetter('data_desde')), key=attrgetter('name')):
                                      <% x += 1 %>
                                      <tr>
                                         <td>
                                             ${ene.name}
                                         </td>
                                         <td colspan="2">
                                             %if ene.discount != 0:
                                                ${formatLang(ene.discount, digits=0)}% Dto:
                                             %elif ene.isdiscount:
                                                ${formatLang(discounts.get(ene.product_id.id), digits=0)}% Dto:
                                             %endif
                                             ${ene.quantity} kVArh x ${formatLang(ene.price_unit_multi, digits=6)} €/kVArh
                                         </td>
                                         <td class="total">${formatLang(sentido * ene.price_subtotal, digits=2)}</td>
                                      </tr>
                                  %endfor
                                  % if x == 0:
                                    <tr> <td></td><td></td><td></td><td></td></tr>
                                  % endif
                               </tbody>
                            </table>
                            </div>
                            <div class="calculs">
                                <div class="titol_taula"><h3>Otros conceptos</h3></div>
                                <table class="dades_calculs small" >
                                   <colgroup>
                                        <col style="width: 70%">
                                        <col style="width: 30%">
                                    </colgroup>
                                   <tbody>
                                      <% x = 0 %>
                                      %for ene in altres_lines:
                                          <% x += 1 %>
                                          <tr>
                                            <td>${ene.name}</td>
                                            <td class="total">${formatLang(sentido * ene.price_subtotal, digits=2)}</td>
                                          </tr>
                                      %endfor
                                      % if x == 0:
                                        <tr> <td></td><td></td></tr>
                                      % endif
                                   </tbody>
                                </table>
                            </div>
                            <div class="dades_impostos" style="position: absolute; bottom: 0">
                                <div class="titol_taula"><h3>Impuesto Electricidad</h3></div>
                                <table class="dades_impostos" >
                                    <colgroup>
                                        <col style="width: 70%">
                                        <col style="width: 30%">
                                    </colgroup>
                                    <tbody>
                                    %if inv.tax_line :
                                      <%
                                          base = 0.0
                                          total_iese = 0.0
                                      %>
                                    %for t in inv.tax_line :

                                      %if 'especial sobre la electricidad'.upper() in t.name.upper():
                                            <%
                                                base +=  t.base
                                                total_iese += t.amount
                                            %>

                                      %endif
                                    %endfor
                                        <tr>
                                            %if inv.fiscal_position and 'IESE' in inv.fiscal_position.name :
                                                <td>0,766904448 % sobre ${formatLang(sentido * base, digits=2)}€</td>
                                            %else:
                                                <td>5,11269632 % sobre ${formatLang(sentido * base, digits=2)}€</td>
                                            %endif
                                            <td class="total">${formatLang(sentido * total_iese, digits=2)}</td>
                                        </tr>
                                    %endif

                                        %for alq in get_linies_tipus(cursor, factura['id'], ['lloguer']):
                                            <tr>
                                                <td>Importe alquiler</td>
                                                <td class="total">${formatLang(sentido * alq['account_invoice_line_price_subtotal'], digits=2)}</td>
                                            </tr>
                                        %endfor
                                    </tbody>
                                </table>
                            </div>
                    </div>
                    <div class="box">
                        <table class="imports_total">
                           <colgroup>
                              <col style="width: 50%">
                              <col style="width: 20%">
                              <col style="width: 15%">
                              <col style="width: 20%">
                           </colgroup>
                           <tbody>
                              %if inv.tax_line :
                                %for t in inv.tax_line :
                                    %if 'IVA' in t.name.upper():
                                    <tr>
                                     <td>BASE IMPONIBLE</td>
                                     <td class="requadre">${ formatLang(sentido * t.base, digits=2)|entity}</td>
                                     <td class="iva">${ t.name.upper().replace('IVA','') }</td>
                                     <td class="total">${ formatLang(sentido * t.amount, digits=2) }</td>
                                    </tr>
                                  %endif
                                %endfor
                                %endif
                              <tr>
                                     <th colspan="3">IMPORTE TOTAL EUROS</td>
                                     <td  class="total bold">${ formatLang(sentido * inv.amount_total, digits=2)|entity}</td>
                                    </tr>
                           </tbody>
                        </table>
                    </div>
                    <div class="box">
                        <table class="dades dades_bancaries">
                            <tr><td class="desc">Forma de pago:</td><td >${inv.payment_type and inv.payment_type.name or ''|entity}</td></tr>
                            %if inv.payment_type.code == 'RECIBO_CSB':
                                <tr><td class="desc">Entidad bancaria:</td><td>${entidad_bancaria}</td></tr>
                                <tr><td class="desc">Nº de cuenta:</td><td>${bank_acc}</td></tr>
                            %endif
                         </table>
                    </div>
                    <div class="dades_barcode">
                        <table class="dades">
                            %if inv.payment_type.code == 'RECIBO_CSB':
                                <tr>
                                    <td class="desc">Fecha Cobro:</td><td>${inv.date_due or ''}</td>
                                </tr>
                                <tr></tr>
                            %else:
                                <tr>
                                    <td class="desc">Emisora:</td><td >${entity}</td>
                                    <td>${suffix}</td><td></td>
                                </tr>
                                <tr>
                                    <td class="desc">Referencia:</td><td>${ref}</td>
                                    <td class="desc">Identificación:</td><td>${recibo507.notice}</td>
                                </tr>
                                <tr>
                                    <td class="desc">Importe:</td><td>${formatLang(inv.amount_total, digits=2)}</td>
                                    <td class="desc">Fecha Cobro:</td><td>${inv.date_due or ''}</td>
                                </tr>
                            %endif
                         </table>
                    </div>
                </div>
                <div class="clear fi_bloc"></div>
            </div> <!-- part central -->
            <div class="peu emergency_complaints"> <!-- part peu -->
                <div class="header box"> Observaciones </div>
                <div class="esquerra emergency">
                    <h2>Averías y Urgencias</h2>
                        <dl>
                            <dt>Distribuidora:</dt>
                            <dd>${inv.polissa_id.distribuidora.name}</dd>
                            <dt>Averías:</dt>
                            <dd>${distri_phone}</dd>
                            <dt>Contrato de acceso:</dt>
                            <dd>${inv.polissa_id.ref_dist}</dd>
                        </dl>
                </div>
                <div class="dreta complaints">
                    <h2>Información ab energía</h2>
                    <dl>
                        <dt>Atención al Cliente Teléfono:</dt>
                        <dd>974 310 195</dd>
                        <dt>Incidencias:</dt>
                        <dd>900 190 300 / sac@abenergia.es</dd>
                    </dl>
                </div>
                <div class="clear fi_bloc"></div>
            </div> <!-- fi peu -->
            <p class="new_page"></p>

        </div>
        </div>
        <div class="last_page">
            <div class="new_page">
                <img style="width: 95%; position: absolute; top:0" src="${addons_path}/giscedata_facturacio_comer_ab/report/FACTURA TRASERA_ABE.png" >
            </div>
        </div>
        <script>
        var factura_id = ${inv.id}
        var data_consum = ${json.dumps(sorted(historic_js, key=lambda h: h['mes']))}
        var es30 = ${len(periodes_a)>3 and 'true' or 'false'}

        </script>
        <script src="${addons_path}/giscedata_facturacio_comer_ab/report/d3.min.js"></script>
        <script src="${addons_path}/giscedata_facturacio_comer_ab/report/consum_antic.js"></script>
        <%
        comptador_factures += 1;
        %>
        % if comptador_factures<len(objects):
            <p style="page-break-after:always"></p>
        % endif
    </div>
    %endfor

</body>
</html>
