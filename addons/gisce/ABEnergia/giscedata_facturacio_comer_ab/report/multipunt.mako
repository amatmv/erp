<%
    from datetime import datetime
    from bankbarcode.cuaderno57 import Recibo507

    def clean(text):
        return text or ''
    comptador_factures = 0
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<head>
    <style type="text/css">
            ${css}
        body {
            font-family: helvetica;
            font-size: 11px;
            margin-right: 50px;
            margin-left: 50px;
        }

        table {
            font-size: 10px;
            width: 98%;
            float: right;
            border-collapse: collapse;
            border: 1px solid black;
        }

        table td {
            border: 1px solid #ddd;
            padding-left: 5px;
            padding-right: 5px;
        }

        table td.blank {
            font-weight: bold;
            border-color: white;
            border-left-color: #C1C1C1;
            border-right-color: #C1C1C1;
            border-bottom: none;
            border-top: none;
            font-size: 9px;
        }

        table td.right {
            font-weight: bold;
            border-color: white;
            border-left-color: #C1C1C1;
            border-right-color: #C1C1C1;
            border-bottom: none;
            border-top: none;
            font-size: 9px;
            text-align: right;
        }

        table td.end {
            border-bottom-color: #C1C1C1;
            border-left-color: #C1C1C1;
            border-right-color: #C1C1C1;
            border-top: none;
        }

        div.background {
            position: absolute;
            top: -2px;
            left: 0px;
            width: 100%;
            height: 100%;
            z-index: -1;
        }

        div.background img {
            width: 100%;
        }

        #contenidor {
            position: relative;
            top: 250px;
        }

        #client {
            position: relative;
            float: right;
            border: 1px solid #C6C6C6;
            width: 260px;
            padding: 5px;
            font-weight: bold;
        }

        .title {
            background-color: #78be20;
            font-weight: bold;
            text-align: center;
            border: 1px solid #78be20;
        }

        .title2 {
            background-color: #78be20;
            border: 1px solid #78be20;
            font-weight: bold;
            text-align: center;
        }

        .total {
            text-align: right;
        }

        #forma_pagament {
            position: relative;
            width: 59%;
            max-width: 98%;
            margin-top: 15px;
            float: left;
            left: 14px;
            font-size: 9px;
        }

        .bold {
            font-weight: bold;
        }

        .center {
            text-align: center;
        }
    </style>
</head>
<body>
    <%
        pool = objects[0].pool
        fact_obj = pool.get('giscedata.facturacio.factura')
        IVA = 21
    %>
    %if objects[0].group_move_id:
        %for fact in objects:
            <div class="background">
                <img src="${addons_path}/giscedata_facturacio_comer_ab/report/FACTURA_ABE.png">
            </div>
            <div id="contenidor">
                <div id="client">
                    <%
                        factura = None
                        for l in fact.group_move_id.line_id:
                            if l.invoice:
                                id_fact = fact_obj.search(cursor, uid, [('invoice_id', '=', l.invoice.id)])
                                factura = fact_obj.browse(
                                    cursor, uid, id_fact[0], context={'lang':fact.group_move_id.partner_id.lang}
                                )
                                break
                    %>
                    <p>${factura.address_contact_id.name}</p>

                    <p>${factura.address_contact_id.street}</p>

                    <div>
                        <span style="float:left; max-width: 100%; ">${factura.address_contact_id.zip}</span>
    						<span style="float:left; max-width: 100%; display:block; padding-left: 15px;">
                                ${factura.address_contact_id.city or ''|entity}&emsp;${factura.address_contact_id.state_id.name or ''|entity}
                            </span>
                    </div>
                </div>
                <table style="margin-top: 20px;">
                    <tr>
                        <td class="title"
                            style="width: 25%;">${_("AGRUPADA Nº")}</td>
                        <td class="title" style="width: 25%;">${_("Fecha")}</td>
                        <td class="title"
                            style="width: 25%;">${_("Nº Cliente")}</td>
                        <td class="title"
                            style="width: 25%;">${_("DNI/CIF")}</td>
                    </tr>
                    <tr>
                        <td class="center">${clean(fact.group_move_id.ref)}</td>
                        <td class="center">${fact.date_invoice}</td>
                        <td class="center">${fact.polissa_id.name}</td>
                        <td class="center">${fact.partner_id.vat and clean(fact.partner_id.vat.replace('ES',''))}</td>
                    </tr>
                </table>
                <table style="margin-top: 5px;">
                    <tr>
                        <td class="title">${_("Descripción del Concepto")}</td>
                        <td class="title">${_("% I.V.A.")}</td>
                        <td class="title">${_("IMPORTE")}</td>
                    </tr>
                    <%
                        factures = []
                        energia = 0
                        potencia = 0
                        reactiva = 0
                        impost = 0
                        altres = 0
                        lloguer = 0
                        import_iva = 0
                        amount_total = 0
                        amount_untaxed = 0
                        amount_tax = 0
                    %>
                    % for linia in fact.group_move_id.line_id:
                        % if linia.invoice:
                            <%
                                factures.append(linia.invoice.id)
                            %>
                        % endif
                    % endfor
                    <%
                        id_fact = []
                        id_facts = fact_obj.search(cursor, uid, [('invoice_id', 'in', factures)])

                    %>
                    % for f in fact_obj.browse(cursor, uid, id_facts):
                        <%
                            sentit = 1.0
                            if f.type in['out_refund', 'in_refund']:
                                sentit = -1.0
                            en = [l for l in f.linia_ids if l.tipus in 'energia']
                            energia +=  sentit * sum([l.price_subtotal for l in en])
                            po = [l for l in f.linia_ids if l.tipus in 'potencia']
                            potencia +=  sentit * sum([l.price_subtotal for l in po])
                            re = [l for l in f.linia_ids if l.tipus in 'reactiva']
                            reactiva +=  sentit * sum([l.price_subtotal for l in re])
                            llo = [l for l in f.linia_ids if l.tipus in 'lloguer']
                            lloguer +=  sentit * sum([l.price_subtotal for l in llo])
                            laltres = [l for l in f.linia_ids if l.tipus in 'altres']
                            altres += sentit * sum([l.price_subtotal for l in laltres])
                            iese_lines = [l for l in f.tax_line if 'IVA' not in l.name]
                            impost += sentit * sum([l.amount for l in iese_lines])
                            iva_lines = [l for l in f.tax_line if 'IVA' in l.name]
                            import_iva += sentit * sum([l.amount for l in iva_lines])
                            amount_total += sentit * f.amount_total
                            amount_untaxed += sentit * f.amount_untaxed
                            amount_tax += sentit * f.amount_tax
                        %>
                    % endfor
                    <tr>
                        <td class="blank">${_("Término de Potencia")}</td>
                        <td class="right">${formatLang(IVA,digits=2)}</td>
                        <td class="right">${formatLang(potencia,digits=2)}</td>
                    </tr>
                    <tr>
                        <td class="blank">${_("Término de Energía")}</td>
                        <td class="right">${formatLang(IVA,digits=2)}</td>
                        <td class="right">${formatLang(energia,digits=2)}</td>
                    </tr>
                    <tr>
                        <td class="blank">${_("Término de Reactiva")}</td>
                        <td class="right">${formatLang(IVA,digits=2)}</td>
                        <td class="right">${formatLang(reactiva,digits=2)}</td>
                    </tr>
                    <tr>
                        <td class="blank">${_("Impuesto Eléctrico")}</td>
                        <td class="right">${formatLang(IVA,digits=2)}</td>
                        <td class="right">${formatLang(impost,digits=2)}</td>
                    </tr>
                    <tr>
                        <td class="blank">${_("Alquiler Contador")}</td>
                        <td class="right">${formatLang(IVA,digits=2)}</td>
                        <td class="right">${formatLang(lloguer,digits=2)}</td>
                    </tr>
                    <tr>
                        <td class="blank">${_("Otros conceptos")}</td>
                        <td class="right">${formatLang(IVA,digits=2)}</td>
                        <td class="right">${formatLang(altres,digits=2)}</td>
                    </tr>
                    <tr>
                        <td class="end" style="height: 200px;"></td>
                        <td class="end"></td>
                        <td class="end"></td>
                    </tr>
                </table>
                <table style="margin-top: 10px;">
                    <tr>
                        <td class="title2">${_("Suma Importes")}</td>
                        <td class="title2">${_("Impuesto eléctrico")}</td>
                        <td class="title2">${_("Base Imponible")}</td>
                        <td class="title2">${_("% IVA")}</td>
                        <td class="title2">${_("Importe IVA")}</td>
                        <td class="title2">${_("TOTAL FACTURA")}</td>
                    </tr>
                    <tr>
                        <%
                            suma = energia + potencia + reactiva + lloguer + altres
                            base = amount_total - import_iva
                        %>
                        <td class="total">${formatLang(suma, digits=2)}</td>
                        <td class="total">${formatLang(impost,digits=2)}</td>
                        <td class="total">${formatLang(base,digits=2)}</td>
                        <td class="total">${formatLang(IVA,digits=2)}</td>
                        <td class="total">${formatLang(import_iva,digits=2)}</td>
                        <td class="total bold">${formatLang(amount_total,digits=2)} €
                        </td>
                    </tr>
                </table>
                <table id="forma_pagament">
                    <tr>
                        <td class="bold">${_("Forma de pago")}</td>
                        <td>${clean(fact.polissa_id.tipo_pago.name)}</td>
                    </tr>
                    <tr>
                        <td class="bold">${_("Domiciliación")}</td>
                        <td>${fact.polissa_id.bank.bank.lname}</td>
                    </tr>
                    <tr>
                        <td class="bold">${_("Nº de Cuenta")}</td>
                        <td>${fact.polissa_id.bank.iban[4:][:-4]}****</td>
                    </tr>
                </table>
            </div>
        %endfor
    %else:
        <br><br><br>
        <center>
            <b><font
                    size="4">${_("La factura ")}${objects[0].number}${_(" no está agrupada.")}</font></b>
        </center>
    %endif
</body>
</html>
