<%
    from datetime import datetime
    import base64
    try:
        from cStringIO import StringIO
    except:
        from StringIO import StringIO
    import csv

    pool = objects[0].pool
    att_obj = pool.get('ir.attachment')
    cursor = objects[0]._cr
    uid = user.id
    polissa_obj = pool.get('giscedata.polissa')
    polissa = polissa_obj.browse(cursor, uid, objects[0].polissa_id.id, context={'date': objects[0].data_final.val})
    cups = polissa.cups.name
    direccio = polissa.cups.direccio

    def getCsvData(mode):
        data_csv = ''
        ids = att_obj.search(cursor, uid, [
            ('res_model', '=', 'giscedata.facturacio.factura'),
            ('res_id', '=', objects[0].id),
            ('datas_fname', '=like', '{}_%'.format(mode.upper()))
        ])
        if ids:
            elem = ids[0]
            line = att_obj.read(cursor, uid, elem, ['datas','datas_fname'])
            base64_data = line['datas']
            data_csv = StringIO(base64.b64decode(base64_data))
        return data_csv

%>
<%def name="header(mode)">\
    <div class="header">
        <div class="dades_factura">
            <div class="detalls">
                %if mode == "phf":
                    <h2>DETALLE FACTURA ${objects[0].number or ''}</h2>
                    <br>
                    <table>
                        <tr>
                            <td class="bold">CUPS</td>
                            <td>${cups or ''}</td>
                            <td style="width: 25px;">&nbsp;</td>
                            <td class="bold">Dirección PS</td>
                            <td>${direccio or ''}</td>
                        </tr>
                    </table>
                    <div class="subtitol">
                        Precios horario final (€/MWh)
                    </div>
                %elif mode == 'curve':
                    <div class="subtitol">
                        Consumos (kWh)
                    </div>
                %endif
            </div>
        </div>
        <div class="logo">
            <img class="img_logo" src="data:image/jpeg;base64,${company.logo}"/>
        </div>
    </div>
</%def>
<%def name="notFound()">\
    <h2>No se ha podido leer el archivo adjunto</h2>
</%def>
<%def name="continguts(mode)">\
    <div>
        <%
            data_csv = getCsvData(mode)
            if data_csv:
                getTable(data_csv)
            else:
                notFound()
        %>
    </div>
</%def>

<%def name="getPHF()">
    <%
        header = False
        data_act = None
    %>
    <table class="taula_dades">
        <tr>
            <td>&nbsp;</td>
            %for elem in range(1, 26):
                <td class="field">${elem}</td>
            %endfor
        </tr>
        <tr>
        <%
            contador = 1
            phf_data_csv = getCsvData('phf')
            curve_data_csv = getCsvData('curve')
            phf_data = []
            curve_data = []
            for line in csv.reader(phf_data_csv, delimiter=';'):
                if len(line) < 2:
                    continue
                phf_data.append(line)

            for line in csv.reader(curve_data_csv, delimiter=';'):
                if len(line) < 2:
                    continue
                curve_data.append(line)
            zipped = zip(phf_data, curve_data)
            phf_final = []
            for elem in zipped:
                consum = float(elem[1][1])
                preu = float(elem[0][1])
                res = round(preu / (consum / 1000.0), 4)
                phf_final.append((elem[0][0], res))

        %>
        %for line in phf_final:
            <%
                if len(line) < 2:
                    continue
                reading = line[1]
            %>
            %if (data_act is not None) and data_act != str.split(line[0], ' ')[0]:
                <%
                    header = False
                %>
                %if contador <= 25:
                    %for elem in range(contador, 26):
                        <td>&nbsp;</td>
                    %endfor
                %endif
                </tr><tr>
                <%
                    contador = 1
                %>
            %endif
            %if not header:
                <%
                    header = True
                    data_act = line[0].split(' ')[0]
                %>
                <td class="title">${data_act}</td>
            %endif
            <td class="field">${reading}</td>
            <%
                contador += 1
            %>
        %endfor
        %if contador <= 25:
            %for elem in range(contador, 26):
                <td>&nbsp;</td>
            %endfor
        %endif
        </tr>
    </table>
</%def>

<%def name="getTable(data_csv)">
    <%
        header = False
        data_act = None
    %>
    <table class="taula_dades">
        <tr>
            <td>&nbsp;</td>
            %for elem in range(1, 26):
                <td class="field">${elem}</td>
            %endfor
        </tr>
        <tr>
        <%
            contador = 1
        %>
        %for line in csv.reader(data_csv, delimiter=';'):
            <%
                if len(line) < 2:
                    continue
                reading = line[1]
            %>
            %if (data_act is not None) and data_act != str.split(line[0], ' ')[0]:
                <%
                    header = False
                %>
                %if contador <= 25:
                    %for elem in range(contador, 26):
                        <td>&nbsp;</td>
                    %endfor
                %endif
                </tr><tr>
                <%
                    contador = 1
                %>
            %endif
            %if not header:
                <%
                    header = True
                    data_act = line[0].split(' ')[0]
                %>
                <td class="title">${data_act}</td>
            %endif
            <td class="field">${reading}</td>
            <%
                contador += 1
            %>
        %endfor
        %if contador <= 25:
            %for elem in range(contador, 26):
                <td>&nbsp;</td>
            %endfor
        %endif
        </tr>
    </table>
</%def>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
    	<style type="text/css">
        ${css}
        </style>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_ab/report/detalle_factura_indexada.css"/>
    </head>
    <body>
        <%
            mode = "phf"
            header(mode)
            getPHF()
        %>
        <div style="clear: both;"></div>
        <p style="page-break-after:always"></p>
        <%
            mode = "curve"
            header(mode)
            continguts(mode)
        %>
</html>