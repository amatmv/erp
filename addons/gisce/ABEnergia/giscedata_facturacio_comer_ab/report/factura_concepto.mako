<%
    from operator import attrgetter, itemgetter
    from datetime import datetime, timedelta
    import json
%>
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<head>
    <style type="text/css">
        ${css}
    </style>
    <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_ab/report/factura_concepto.css"/>
</head>
<body>
    <%
        pool = objects[0].pool
        polissa_obj = objects[0].pool.get('giscedata.polissa')
        comptador_factures = 0
        from datetime import datetime
        from bankbarcode.cuaderno57 import Recibo507

        def clean(text):
            return text or ''
    %>
     %for inv in objects :
    <div class="back" style="position:relative; margin:0; padding:0;">
    <div class="background">
        <img src="${addons_path}/giscedata_facturacio_comer_ab/report/FACTURA_ABE.png" >
    </div>
    <div class="wrapper">
        <div class="page_princial">

            <%
                altres_lines = [l for l in inv.linia_ids]
                total_altres = sum([l.price_subtotal for l in altres_lines])

                # bar code
                entity = ''.join([c for c in inv.polissa_id.comercialitzadora.vat if c.isdigit()])
                due_date = str(inv.date_due) # plazo pago = fecha factura + payment_term
                if due_date:
                    suffix = '501'
                    due_date = inv.date_due
                else:
                    suffix = '101'
                    due_date = None
                ref = '{}'.format(inv.id).zfill(11) # id factura
                notice = '000000'
                amount = '{0:.2f}'.format(inv.amount_total) # amount factura
                recibo507 = Recibo507(entity, suffix, ref, notice, amount, due_date)
                periodes_a = sorted(list(set([lectura.name[-3:-1]
                                for lectura in inv.lectures_energia_ids
                                if lectura.tipus == 'activa'])))
                polissa = polissa_obj.browse(cursor, uid, inv.polissa_id.id)
                setLang(inv.partner_id.lang)
            %>
            <!-- capcalera -->
            <div class="bloc capcalera">
                <div class="esquerra">
                    <span>&nbsp;</span>
                </div>
                <div class="dreta">
                   <div class="box address">
                       <p>${inv.partner_id.name}</p>
                       <p>${inv.address_contact_id.street}</p>
                       <div>
                           <span style="float:left; max-width: 100%; ">${inv.address_contact_id.zip}</span>
                           <span style="float:left; max-width: 100%; display:block; padding-left: 15px;">
                            ${inv.address_contact_id.id_poblacio.name}
                               <br>
                            ${inv.address_contact_id.state_id.name}
                           </span>
                       </div>
                    </div>
                </div>
                <div class="clear fi_bloc"></div>
            </div><!-- Fi capcalera -->
            <!-- part central -->
            <div class="bloc part_central">
                <table class="desc_factura">
                    <thead>
                        <tr>
                            <td>Factura Nº</td>
                            <td>Fecha</td>
                            <td>Nº Cliente</td>
                            <td>DNI/CIF</td>
                            <td>Nº Póliza</td>
                            <td>Fecha Alta</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>${clean(inv.number)}</td>
                            <td>${inv.date_invoice}</td>
                            <td>${clean(polissa.name)}</td>
                            <td>${inv.partner_id.vat and clean(inv.partner_id.name.replace('ES',''))}</td>
                            <td>${clean(polissa.name)}</td>
                            <td>${clean(polissa.data_alta)}</td>
                        </tr>
                    </tbody>
                </table>
                <table class="datos_ps">
                    <thead>
                        <tr>
                            <td colspan="2">Datos del Punto de Suministro</td>
                        </tr>
                        <tr>
                            <td>Nombre del Titular</td>
                            <td>Ciudad</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>${clean(polissa.titular.name)}</td>
                            <td>${clean(polissa.cups.id_poblacio.name)}</td>
                        </tr>
                        <tr class="fake_head">
                            <td>Domicilio</td>
                            <td>CUPS</td>
                        </tr>
                        <tr>
                            <td>${clean(polissa.cups.direccio)}</td>
                            <td>${clean(polissa.cups.name)}</td>
                        </tr>
                    </tbody>
                </table>
                <div class="content-concepts">
                    <table class="concepts">
                        <thead>
                            <tr>
                                <td class="concept">Descripción del Concepto</td>
                                <td class="iva">% I.V.A.</td>
                                <td class="import">Importe</td>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                iva_set = set()
                            %>
                            %for l in altres_lines:
                                <tr>
                                    <td>${l.name}</td>
                                    <td class="number">
                                        %for tax in l.invoice_line_tax_id:
                                            %if 'IVA' in tax.name:
                                                ${formatLang(tax.amount * 100.0)}
                                                <%
                                                    iva_set.add(tax.amount * 100.0)
                                                %>
                                            %endif
                                        %endfor
                                    </td>
                                    <td class="number">${"%s" % formatLang(l.price_subtotal)}</td>
                                </tr>
                            %endfor
                        </tbody>
                    </table>
                </div>
                <table class="summary">
                    <thead>
                        <tr>
                            <td>Suma Importes</td>
                            <td>Base Imponible</td>
                            <td>% IVA</td>
                            <td>Importe IVA</td>
                            <td class="total">TOTAL FACTURA</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="number">${formatLang(total_altres)}</td>
                            <td class="number">${formatLang(inv.amount_total)}</td>
                            <td class="number">${len(list(iva_set)) and formatLang(list(iva_set)[0]) or ''}</td>
                            <td class="number">${formatLang(inv.amount_tax)}</td>
                            <td class="number total">${formatLang(inv.amount_total)} €</td>
                        </tr>
                    </tbody>
                </table>
            </div> <!-- part central -->
            <div class="peu"> <!-- part peu -->
                <div class="esquerra">
                    <table>
                        <tbody>
                            <tr>
                                <td>Forma de pago</td>
                                <td>${clean(polissa.tipo_pago.name)}</td>
                            </tr>
                            <tr>
                                <td>Domiciliación</td>
                                <td>${polissa.bank.name}</td>
                            </tr>
                            <tr>
                                <td>Nº de Cuenta</td>
                                <td>${polissa.bank.iban[4:]}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="dreta">
                    ${ recibo507.svg(writer_options={
                        'background':'transparent',
                        'module_height': 7,
                        'font_size': 7,
                        'text_distance': 3,
                        'module_width': 0.25})}
                </div>
                <div class="clear fi_bloc"></div>
            </div> <!-- fi peu -->
            <%
            comptador_factures += 1;
            %>
            % if comptador_factures<len(objects):
                <p style="page-break-after:always;"></p>
            % endif

        </div> <!-- page -->
      </div> <!-- wrapper -->
    </div> <!-- back -->
    %endfor
</body>
</html>