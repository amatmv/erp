# -*- coding: utf-8 -*-

from c2c_webkit_report import webkit_report
from report import report_sxw
from tools import config

from giscedata_facturacio_impagat.giscedata_facturacio import find_B1
from account_invoice_base.account_invoice import amount_grouped


class report_webkit_html(report_sxw.rml_parse):
    def __init__(self, cursor, uid, name, context):
        super(report_webkit_html, self).__init__(cursor, uid, name,
                                                 context=context)
        self.localcontext.update({
            'cursor': cursor,
            'uid': uid,
            'addons_path': config['addons_path'],
            'find_B1': find_B1,
            'amount_grouped': amount_grouped,
            'lang': context['lang']
        })


webkit_report.WebKitParser(
    'report.giscedata.facturacio.factura',
    'giscedata.facturacio.factura',
    'addons/giscedata_facturacio_comer_ab/report/factura.mako',
    parser=report_webkit_html)

webkit_report.WebKitParser(
    'report.giscedata.facturacio.factura_sin_fondo',
    'giscedata.facturacio.factura',
    'addons/giscedata_facturacio_comer_ab/report/factura.mako',
    parser=report_webkit_html)

webkit_report.WebKitParser(
    'report.giscedata.facturacio.factura_concepto',
    'giscedata.facturacio.factura',
    'addons/giscedata_facturacio_comer_ab/report/factura_concepto.mako',
    parser=report_webkit_html)

webkit_report.WebKitParser(
    'report.giscedata.facturacio.factura.carta_tall',
    'giscedata.facturacio.factura',
    'addons/giscedata_facturacio_comer_ab/report/carta_tall.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.facturacio.factura.ingres',
    'giscedata.facturacio.factura',
    'addons/giscedata_facturacio_comer_ab/report/ingres.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.facturacio.factura.multipunt',
    'giscedata.facturacio.factura',
    'addons/giscedata_facturacio_comer_ab/report/multipunt.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.facturacio.factura.rebut_agrupat',
    'giscedata.facturacio.factura',
    'addons/giscedata_facturacio_comer_ab/report/rebut_agrupat.mako',
    parser=report_webkit_html
)
webkit_report.WebKitParser(
    'report.giscedata.facturacio.factura.antiga',
    'giscedata.facturacio.factura',
    'addons/giscedata_facturacio_comer_ab/report/factura_antiga.mako',
    parser=report_webkit_html
)
webkit_report.WebKitParser(
    'report.giscedata.facturacio.factura.detall.indexada',
    'giscedata.facturacio.factura',
    'addons/giscedata_facturacio_comer_ab/report/detalle_factura_indexada.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.facturacio.factura.carta.requeriment',
    'giscedata.facturacio.factura',
    'addons/giscedata_facturacio_comer_ab/report/carta_requerimiento_pago.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.facturacio.factura.segona.carta.requeriment',
    'giscedata.facturacio.factura',
    'addons/giscedata_facturacio_comer_ab/report/carta_requerimiento_pago.mako',
    parser=report_webkit_html
)
