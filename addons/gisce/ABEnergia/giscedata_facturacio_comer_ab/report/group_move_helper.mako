<%def name="resum_factures(fact, pool)">
    <%
        fact_obj = pool.get('giscedata.facturacio.factura')
    %>
    % if fact.group_move_id:
            <h3>Resumen facturas agrupadas:</h3>
            <ul>
                <%
                    inv_ids = [l.invoice.id for l in
                                    fact.group_move_id.line_id if l.invoice]
                    fact_ids = fact_obj.search(
                        cursor, uid, [('invoice_id', 'in', inv_ids)])
                %>
                % for l in fact_obj.browse(cursor, uid, fact_ids):
                    <li>
                        <%
                            sentit = 1.0
                            if l.type in['out_refund', 'in_refund']:
                                sentit = -1.0

                            if l.tipo_rectificadora in ['B', 'A' 'BRA']:
                                text = ' (Factura abonadora de la factura {0})'.format(
                                    l.ref.number or '')
                            elif l.tipo_rectificadora in ['R', 'RA']:
                                text = ' (Factura rectificadora de la factura {0})'.format(
                                    l.ref.number or '')
                            else:
                                text = ''
                        %>
                        ${l.number} total: ${formatLang(sentit * l.amount_total, monetary=True)} &euro; ${text}
                    </li>
                % endfor
            </ul>
        % endif
</%def>