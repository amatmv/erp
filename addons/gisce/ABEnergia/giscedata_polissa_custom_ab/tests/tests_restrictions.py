# -*- coding: utf-8 -*-

from destral import testing
from destral.transaction import Transaction
from osv.osv import except_osv


class TestsPolissaRestrictions(testing.OOTestCase):
    def open_polissa(self, txn):
        cursor = txn.cursor
        uid = txn.user
        pool = self.openerp.pool
        polissa_obj = pool.get('giscedata.polissa')
        imd_obj = pool.get('ir.model.data')
        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]

        polissa_obj.send_signal(cursor, uid, [polissa_id], [
            'validar', 'contracte'
        ])

        return True

    def test_raise_error_if_activate_with_consumo_anual_previsto_0(self):
        pool = self.openerp.pool
        polissa_obj = pool.get('giscedata.polissa')
        imd_obj = pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            polissa_obj.write(cursor, uid, polissa_id,
                              {'consumo_anual_previsto': 0}
                              )
            with self.assertRaises(except_osv) as validate_error:
                self.open_polissa(txn)

            expected_error = ('warning -- Warning!\n\n'
                              'El consum anual ha de ser més gran que 0 en '
                              'estat validar!')

            self.assertEqual(expected_error, validate_error.exception.message)


            polissa_obj.write(cursor, uid, polissa_id,
                              {'consumo_anual_previsto': 200}
                              )
            self.assertTrue(self.open_polissa(txn))
