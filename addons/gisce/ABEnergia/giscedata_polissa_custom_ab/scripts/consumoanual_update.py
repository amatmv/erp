# -*- coding: utf-8 -*-
import click
from datetime import datetime
from datetime import timedelta
from tqdm import tqdm
import logging

from erppeek import Client

def setup_log(instance, filename):
    """Es pot consultar un fitxer en /tmp on es mostra el progress del proccés
    i les incidències"""
    log_file = filename
    logger = logging.getLogger(instance)
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr = logging.FileHandler(log_file)
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(logging.INFO)

@click.command()
@click.option('-s', '--server', default='http://localhost',
              help=u'Adreça servidor ERP')
@click.option('-p', '--port', default=18069, help='Port servidor ERP',
              type=click.INT)
@click.option('-u', '--user', default='admin', help='Usuari servidor ERP')
@click.option('-w', '--password', help='Contrasenya usuari ERP')
@click.option('-d', '--database', default='abenergia',
              help='Nom de la base de dades')
@click.option('-f', '--file_log', envvar='LOG_FILE',
              default="/tmp/consumoanual_update.log")
def update_consumos_anuales_polizas(**kwargs):

    setup_log(__name__, kwargs['file_log'])
    logger = logging.getLogger(__name__)
    server = '{}:{}'.format(kwargs['server'], kwargs['port'])
    connection = Client(server=server, db=kwargs['database'],
                        user=kwargs['user'], password=kwargs['password'])
    polissa_obj = connection.model('giscedata.polissa')
    facturas_obj = connection.model('giscedata.facturacio.factura')
    logger.info('#######Update consumo_anual.Start at {}'.
                format(datetime.today()))

    now = datetime.now()
    yearandhalfago = (now - timedelta(days=550)).strftime("%Y-%m-%d")
    onemonthago = (now - timedelta(days=31)).strftime("%Y-%m-%d")

    #Nomes considerarem les pòlisses actives
    l_pol = polissa_obj.search()

    #Nomes considerem les factures que tinguin com a molt 1 any d'antiguitat
    llista_factures = facturas_obj.search([
        ('polissa_id', 'in', l_pol),
        ('type', 'like', 'out_%'),
        ('date_invoice', '>=', yearandhalfago)])

    if not llista_factures:
        # no hi ha res a actualitzar
        logger.info('No shan trobat factures')
        return

    dades = facturas_obj.read(llista_factures,
                              ['energia_kwh',
                               'dies',
                               'polissa_id',
                               'type'])
    #Acumularem els consums i els dies en un diccionari
    dic_consumo = dict((key, [0, 0]) for key in l_pol)
    incidencies = False
    for diccionari in tqdm(dades, desc='Carregant factures'):
        try:
            factura_id = diccionari['id']
            tipus = diccionari['type']
            polissa_id = diccionari['polissa_id'][0]
            energia = diccionari['energia_kwh']
            dies = diccionari['dies']

            if tipus == 'out_invoice':
                dic_consumo[polissa_id][0] += abs(energia)
                dic_consumo[polissa_id][1] += abs(dies)
            else:
                dic_consumo[polissa_id][0] -= abs(energia)
                dic_consumo[polissa_id][1] -= abs(dies)
        except Exception as e:
            logger.info('Incidència amb la factura {} de la polissa {}.'
                        ' Error:{}'.format(polissa_id, factura_id, e.message))
    if not incidencies:
        logger.info('Sense incidències')

    #Calculem els consumsanualitzats a partir del consum total
    for polissa_id in tqdm(dic_consumo, desc='Calculant consums'):
        energiatotal, tempstotal = dic_consumo[polissa_id]
        if energiatotal and tempstotal:
            consum = 365 * energiatotal / tempstotal
            polissa_obj.write(polissa_id, {'consumo_anual_calculado': consum})

    logger.info('#######Update consumo_anual.  Finished at {}'.
                format(datetime.today()))
if __name__ == '__main__':
    update_consumos_anuales_polizas()