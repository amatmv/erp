# -*- coding: utf-8 -*-
{
    "name": "giscedata_polissa_custom_ab",
    "description": """
        Personalització pòlissa per AB Enegia
        """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Custom",
    "depends":[
        "base",
        "giscedata_polissa_comer",
        "giscedata_facturacio_comer",
        "hr",
        "hr_office",
        "giscedata_polissa_responsable"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_polissa_view.xml",
        "security/giscedata_polissa_security.xml",
        "wizard/actualitzar_estat_documentacio_view.xml",
        "security/ir.model.access.csv",
        "wizard/wizard_crear_contracte_view.xml"
    ],
    "active": False,
    "installable": True
}
