# -*- encoding: utf-8 -*-

from osv import osv, fields

class WizardUpdateEstatDocumentacio(osv.osv_memory):

    _name = 'wizard.actualitzar.estat.documentacio'

    def action_update_estat_documentacio(self, cursor, uid, ids, context=None):
        '''actualitzar estat de la documentació (validada o no) a la pòlissa'''
        if not context:
            context = {}

        # comprovem que l'usuari té permisos
        # encara que no en tingui, com que es pot accedir al wizard des de la
        # pòlissa que hi pot accedir tothom pot fer servir el wizard
        user_obj = self.pool.get('res.users')
        imd_obj = self.pool.get('ir.model.data')
        grup_documentacio = imd_obj.get_object_reference(cursor, uid,
                            'giscedata_polissa_custom_ab',
                            'group_actualitzar_estat_documentacio_manager')[1]
        groups = user_obj.read(cursor, uid, uid, ['groups_id'])['groups_id']
        if not grup_documentacio in groups:
            raise osv.except_osv('Error', 'No hay permisos suficientes.')

        # Update estat documentació a la modcontractual
        wizard = self.browse(cursor, uid, ids[0])
        polissa = wizard.polissa_id
        if polissa.modcontractual_activa:
            modcontractual = polissa.modcontractual_activa
            modcontractual.write({'doc_validada': wizard.doc_validada},
                                  context=context)
        wizard.write({'state': 'end'})
        return True

    _columns = {
        'doc_validada': fields.boolean(string=u'Documentación validada'),
        'polissa_id': fields.many2one('giscedata.polissa', 'Polissa'),
        'state': fields.selection([('init', 'Init'),
                                   ('end', 'End')], 'State'),
    }

    _defaults = {
        'state': lambda *a: 'init',
    }

WizardUpdateEstatDocumentacio()
