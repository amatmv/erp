# -*- coding: utf-8 -*-
from tools.translate import _
from osv import osv, fields
from giscedata_polissa_custom_ab.giscedata_polissa import TIPUS_RENOVACIO
from osv.expression import OOQuery


class GiscedataPolissaCrearContracte(osv.osv_memory):
    """Extenció wizard per crear les modificacions contractuals.
    """
    _name = 'giscedata.polissa.crear.contracte'
    _inherit = 'giscedata.polissa.crear.contracte'

    def _default_price_list_changed(self, cursor, uid, context=None):
        polissa_id = context.get('active_id', False)
        polissa_obj = self.pool.get('giscedata.polissa')
        contracte_obj = self.pool.get('giscedata.polissa.modcontractual')
        polissa = polissa_obj.browse(cursor, uid, polissa_id, context)
        changes = polissa_obj.get_changes(cursor, uid, polissa.id)
        find_llista = False
        find_renovacio = False
        for field in changes.items():
            if field[0] == 'llista_preu':
                find_llista = True
            if field[0] == 'tipus_renovacio':
                find_renovacio = True

        return find_llista and not find_renovacio

    def action_crear_contracte(self, cursor, uid, ids, context=None):
        """Extensió del metode per crear modcons i actualitzar el camp
            data_activacio (Data última renovació activa) si s'han canviat
            les llistes de preus.
        """
        if context is None:
            context = {}
        pol_obj = self.pool.get('giscedata.polissa')
        mod_obj = self.pool.get('giscedata.polissa.modcontractual')

        wiz_fields = self.read(
            cursor, uid, ids, ['polissa_id', 'accio', 'price_list_changed',
                               'tipus_renovacio'],
            context=context
        )

        polissa_id = wiz_fields[0]['polissa_id']
        accio = wiz_fields[0]['accio']
        price_list_changed = wiz_fields[0]['price_list_changed']
        tipus_renovacio = wiz_fields[0]['tipus_renovacio']
        polissa = pol_obj.browse(cursor, uid, polissa_id)

        current_price_list_id = None
        if polissa.modcontractual_activa:
            current_price_list_id = polissa.modcontractual_activa.llista_preu
        else:
            current_price_list_id = polissa.llista_preu

        res = super(GiscedataPolissaCrearContracte, self).action_crear_contracte(
            cursor, uid, ids, context=context
        )

        polissa = pol_obj.browse(cursor, uid, polissa_id)
        after_price_list_id = None
        data_inici = None
        modificacio_activa = None
        if accio == 'modificar':
            # S'ha modificat la modificacio activa
            after_price_list_id = polissa.modcontractual_activa.llista_preu
            data_inici = polissa.modcontractual_activa.data_inici
            modificacio_activa = polissa.modcontractual_activa
        else:
            # S'haura creat modificacio
            after_price_list_id = polissa.modcontractuals_ids[0].llista_preu
            data_inici = polissa.modcontractuals_ids[0].data_inici
            modificacio_activa = polissa.modcontractuals_ids[0]

        # quan la renovació sigui per cron no volem modificar ni desactivar
        # els registres de venciment
        if not context.get('renovacio_cron', False):
            self.modificar_registros_vencimiento(
                cursor, uid, ids, current_price_list_id.id,
                after_price_list_id.id, price_list_changed, context=context
            )

        if current_price_list_id.id != after_price_list_id.id:
            if not self.check_user_manager(cursor, uid, context=context) \
                    and not after_price_list_id.ofertable:
                raise osv.except_osv('Error',
                                    'La lista de precios {0} no es ofertable'.
                                        format(after_price_list_id.name)
                                     )

            update_params = {'data_activacio': data_inici}

            if price_list_changed:

                # Si l'activació ve des d'ATR, amb un M1 amb canvi de preus,
                # indiquem renovació expressa.
                if context.get('activacio_from_atr', False):
                    tipus_renovacio = 'expressa'

                update_renovacio = {'tipus_renovacio': tipus_renovacio}

                pol_obj.write(
                    cursor, 1, polissa_id, update_renovacio, context=context
                )
                mod_obj.write(
                    cursor, 1, modificacio_activa.id, update_renovacio,
                    context=context
                )

            polissa.write(update_params)
            modificacio_activa.write(update_params)
        return res

    def check_user_manager(self, cursor, uid, context=None):
        sql = '''
        SELECT * FROM res_groups_users_rel AS rel
        LEFT JOIN res_groups AS g ON g.id = rel.gid
        WHERE g.name ilike '%%Product / Manager%%' and rel.uid = %s
        '''

        cursor.execute(sql, (uid,))

        return True if cursor.fetchone() else False

    def modificar_registros_vencimiento(self, cursor, uid, ids,
                                           before_prices, after_prices,
                                           price_list_changed, context=None):
        rv_obj = self.pool.get('registro.vencimiento')

        is_product_manager = self.check_user_manager(cursor, uid,
                                                     context=context)
        from_atr = context.get('activacio_from_atr', False)

        wiz_reads = self.read(cursor, uid, ids, ['accio', 'duracio', 'polissa_id'], context=context)[0]

        tipo_modcon = wiz_reads['accio']

        polissa_id = wiz_reads['polissa_id']

        periode = wiz_reads['duracio']

        # Search for a register with current contract
        q = OOQuery(rv_obj, cursor, uid)
        sql = q.select(['id', 'llista_preus_id', 'data_venciment']).where([
            ('polissa_id', '=', polissa_id),
            ('active', '=', True),
        ])

        cursor.execute(*sql)

        regs = cursor.dictfetchall()

        reg_ids = [reg['id'] for reg in regs]

        if tipo_modcon == 'nou':
            if periode == 'nou':
                if price_list_changed:
                    rv_obj.write(
                        cursor, uid, reg_ids,
                        {'active': False}, context=context
                    )
            else:
                if price_list_changed:
                    if not from_atr and not is_product_manager:
                        raise osv.except_osv(
                            _('Warning!'),
                            _('No es posible realizar '
                              'un cambio de precios sin un nuevo '
                              'período de contrato'))
                    rv_obj.write(
                        cursor, uid, reg_ids,
                        {'llista_preus_id': after_prices}, context=context
                    )
        else:
            if price_list_changed:
                # Si modificación sobre la modificación actual
                if not is_product_manager:
                    raise osv.except_osv(
                        _('Warning!'),
                        _('No se puede modificar las listas de precios '
                          'sin permisos de Product / Manager'))

                rv_obj.write(
                    cursor, uid, reg_ids,
                    {'llista_preus_id': after_prices}, context=context
                )

    # def modificar_registros_vencimiento(self, cursor, uid, ids, before_prices, after_prices, context=None):
    #     rv_obj = self.pool.get('registro.vencimiento')
    #
    #     is_product_manager = self.check_user_manager(cursor, uid,
    #                                                  context=context)
    #     from_atr = context.get('activacio_from_atr', False)
    #
    #     wiz_reads = self.read(cursor, uid, ids, ['accio', 'duracio', 'polissa_id'], context=context)[0]
    #
    #     tipo_modcon = wiz_reads['accio']
    #
    #     polissa_id = wiz_reads['polissa_id']
    #
    #     periode = wiz_reads['duracio']
    #
    #     # Search for a register with current contract
    #     q = OOQuery(rv_obj, cursor, uid)
    #     sql = q.select(['id', 'llista_preus_id', 'data_venciment']).where([
    #         ('polissa_id', '=', polissa_id),
    #         ('active', '=', True),
    #     ])
    #
    #     cursor.execute(*sql)
    #
    #     regs = cursor.dictfetchall()
    #
    #     if tipo_modcon == 'nou':
    #         # Si modificación contractual nueva
    #         if regs:
    #             # Si se ha encontrado algun registro activo
    #             for reg in regs:
    #                 if reg['llista_preus_id'] == before_prices:
    #                     if periode == 'nou':
    #                         # Si es un nou periode
    #                         rv_obj.write(
    #                             cursor, uid, reg['id'],
    #                             {'active': False}, context=context
    #                         )
    #                     else:
    #                         if not from_atr:
    #                             # Periode actual NO ATR
    #                             if not is_product_manager:
    #                                 raise osv.except_osv(
    #                                     _('Warning!'),
    #                                     _('No es posible realizar '
    #                                       'un cambio de precios sin un nuevo '
    #                                       'período de contrato'))
    #                             else:
    #                                 rv_obj.write(
    #                                     cursor, uid, reg['id'],
    #                                     {'active': False}, context=context
    #                                 )
    #                         else:
    #                             # Periode actual desde ATR
    #                             rv_obj.write(
    #                                 cursor, uid, reg['id'],
    #                                 {'active': False}, context=context
    #                             )
    #     else:
    #         # Si modificación sobre la modificación actual
    #         if not is_product_manager:
    #             raise osv.except_osv(
    #                 _('Warning!'),
    #                 _('No se puede modificar las listas de precios '
    #                   'sin permisos de Product / Manager'))
    #         for reg in regs:
    #             rv_obj.write(
    #                 cursor, uid, reg['id'],
    #                 {'llista_preus_id': after_prices}, context=context
    #             )

    _columns = {
        'price_list_changed': fields.boolean('Cambio de lista de precios?'),
        'tipus_renovacio': fields.selection(
            TIPUS_RENOVACIO, string=u'Tipo actualización precios'),
    }

    _defaults = {
        'price_list_changed': _default_price_list_changed,
        'tipus_renovacio': lambda *a: 'tacita',
    }


GiscedataPolissaCrearContracte()
