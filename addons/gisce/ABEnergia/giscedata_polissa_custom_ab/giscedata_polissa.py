# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _


TIPUS_RENOVACIO = [
    ('tacita', _(u'Tácita')),
    ('expressa', _('Expressa'))
]


class GiscedataPolissaPreavis(osv.osv):

    _name = 'giscedata.polissa.preavis'

    _columns = {
        'name': fields.char('Nom', size=256),
        'num_dies': fields.integer(u'Núm. dies')
    }

GiscedataPolissaPreavis()


class GiscedataPolissa(osv.osv):

    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def _ff_doc_validada(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        if not isinstance(ids, list):
            ids = [ids, ]
        res = dict.fromkeys(ids, False)
        modcon_obj = self.pool.get('giscedata.polissa.modcontractual')
        params = [('active', '=', True),
                  ('polissa_id', 'in', ids)]
        modcon_ids = modcon_obj.search(cursor, uid, params)
        fields = ['polissa_id',
                  'doc_validada']
        modcon = modcon_obj.read(cursor, uid, modcon_ids, fields)
        if len(modcon) == 1:
            res[modcon[0]['polissa_id'][0]] = modcon[0]['doc_validada']
        return res

    def _fnct_search_doc_validada(self, cr, uid, obj, name, args, context=None):
        modcon_obj = self.pool.get('giscedata.polissa.modcontractual')
        params = [('doc_validada', '=', False)]
        if args == [['doc_validada', '=', 1]]:
            params = [('doc_validada', '=', True)]
        modcons_ids = modcon_obj.search(cr, uid, params)
        pol_ids = [modcon['polissa_id'][0]
                   for modcon
                   in modcon_obj.read(cr, uid, modcons_ids, ['polissa_id'])]
        return [('id', 'in', pol_ids)]

    def onchange_notificacio(self, cursor, uid, ids, notificacio, titular,
                             pagador, altre_p, context=None):
        """Per defecte giscedata_polissa porta l'adreca de contacte.
        La canviem per la d'envio.
        """
        # No cridem al super perquè no en volem res
        partner_obj = self.pool.get('res.partner')
        if notificacio == 'titular' and titular:
            domain = [('partner_id', '=', titular)]
            value = partner_obj.address_get(cursor, uid, [titular],
                                            ['delivery'])['delivery']
        elif notificacio == 'pagador' and pagador:
            domain = [('partner_id', '=', pagador)]
            value = partner_obj.address_get(cursor, uid, [pagador],
                                            ['delivery'])['delivery']
        elif notificacio == 'altre_p' and altre_p:
            domain = [('partner_id', '=', altre_p)]
            value = partner_obj.address_get(cursor, uid, [altre_p],
                                            ['delivery'])['delivery']
        else:
            domain = []
            value = False
        res = {'domain': {'direccio_notificacio': domain},
                'value': {'direccio_notificacio': value}}
        if notificacio != 'altre_p':
            res['value'].update({'altre_p': False})
        return res

    def duplicar_potencies_contractades(self, cr, uid, pol_id_old, pol_id_new,
                                     context=None):
        """buscar els periodes de potència generats a la pòlissa per copiar-los
        a la nova pòlissa"""

        pot_per_polissa = self.pool.get(
            'giscedata.polissa.potencia.contractada.periode')

        params = [('polissa_id', '=', pol_id_old)]
        ids_periodes_pol = pot_per_polissa.search(cr, uid, params)

        # si els periodes ja estiguessin generats a la nova
        # per que s'afegis al base, no els tornem a crear
        params = [('polissa_id', '=', pol_id_new)]
        ids_periodes_pol_nova = pot_per_polissa.search(cr, uid, params)
        if len(ids_periodes_pol_nova) > 0:
            return False

        for id_periode in ids_periodes_pol:
            dades_periode = pot_per_polissa.read(cr, uid, id_periode)
            pot_per_polissa.create(cr, uid, {
                'periode_id': dades_periode['periode_id'][0],
                'polissa_id': pol_id_new,
                'potencia': dades_periode['potencia'],
            })
        return True

    def copy_data(self, cursor, uid, id, default=None, context=None):
        if default is None:
            default = {}
        if context is None:
            context = {}

        default.update({
            'data_activacio': False,
        })

        data, trans = super(GiscedataPolissa, self).copy_data(
            cursor, uid, id, default, context
        )

        no_copy = ['refacturacio_pendent', 'facturacio_suspesa', 'category_id',
                   'direccio_notificacio', 'notificacio', 'pagador_sel',
                   'direccio_pagament', 'cnae', 'llista_preu']
        for column in no_copy:
            data.pop(column, None)

        return data, trans

    def copy(self, cursor, uid, id, default=None, context=None):
        """El copy de giscedata.polissa treu el name (sequencia). Li afegim."""
        # També treiem lote de facturación, ultima lecturas facturadas,
        # versión primera facturación, fecha de alta y fecha firma contrato
        # Treiem el/s comptador/s
        if default is None:
            default = {}
        default.update({
            'lot_facturacio': False,
            'data_ultima_lectura': False,
            'data_ultima_lectura_estimada': False,
            'versio_primera_factura': False,
            'data_alta': False,
            'data_firma_contracte': False,
            'comptador': False,
            'comptadors': [],
            'consumo_anual_previsto': 0,
            'consumo_anual_calculado': 0,
            'observacions': '',
         })
        res_id = super(GiscedataPolissa, self).copy(cursor, uid, id, default,
                                                    context=context)

        if not default.get('name', False):
            n_pol = self.pool.get('ir.sequence').get(cursor,uid,
                                                 'giscedata.polissa')
        polissa_obj = self.pool.get('giscedata.polissa')
        self.duplicar_potencies_contractades(cursor, uid, id, res_id,
                                             context=context)
        polissa_obj.write(cursor, uid, res_id, {'name': n_pol})
        return res_id

    def _default_contract_number(self, cursor, uid, id, context=None):

        num_ctte = self.pool.get('ir.sequence').get(cursor, uid,
                                                    'giscedata.polissa')
        return num_ctte

    def _default_delegacion(self, cursor, uid, id, context=None):
        '''La delegacio de l'usuari connectat '''
        employee_obj = self.pool.get('hr.employee')
        employee_id = employee_obj.search(cursor, uid, [('user_id', '=', uid)])
        if employee_id:
            employee_office = employee_obj.read(cursor, uid, employee_id[0],
                                                ['work_office'])
            if employee_office.get('work_office', False):
                return employee_office['work_office'][0]
        return False

    def name_get(self, cursor, uid, ids, context={}):
        if not isinstance(ids, (tuple, list)):
            ids = [ids]

        res = []

        for polissa in self.read(cursor, uid, ids, ['name', 'codi']):
            if polissa['name']:
                name = polissa['name']
            else:
                name = _(u'Codi intern: %s') % polissa['codi']

            res.append((polissa['id'], name))

        return res

    def name_search(self, cursor, uid, name, args=None, operator='ilike',
                    context=None, limit=80):
        ''' Funció de cerca de nom '''
        if not args:
            args = []
        if not context:
            context = {}

        search_list = ['|', ('name', operator, name),
                       ('codi', operator, name)]
        ids = self.search(
            cursor, uid, search_list, limit=limit, context=context
        )

        return self.name_get(cursor, uid, ids, context=context)

    def wkf_overwrite_modcontractual(self, cursor, uid, ids):
        """Al sobreescriure la modificació contractual,
        indiquem el check documentació validada a False perquè pot fer falta
        tornar a validar en canvis de compte / persona fiscal / consum
        """
        super(GiscedataPolissa,
                     self).wkf_overwrite_modcontractual(cursor, uid, ids)
        for polissa in self.browse(cursor, uid, ids):
            mca = polissa.modcontractual_activa
            mca.write({'doc_validada': False})
        return True

    def cnd_validar_to_contracte(self, cursor, uid, ids):
        res = super(GiscedataPolissa, self).cnd_validar_to_contracte(cursor, uid, ids)
        for polissa in self.read(cursor, uid, ids,
                                 ['consumo_anual_previsto',
                                  'data_alta']):
            consum = polissa['consumo_anual_previsto']
            if not consum > 0:
                raise osv.except_osv(_('Warning!'),
                                     _('El consum anual ha de ser més '
                                       'gran que 0 en estat validar!'))

            # escrivim la data d'alta com a data d'última renovació activa
            # a la polissa. Només l'escrivim a la pòlissa, al crear la modcon
            # l'agafarà de la pòlissa
            data_renovacio = {'data_activacio': polissa['data_alta']}
            tipo_renovacion = {'tipus_renovacio': 'expressa'}
            self.write(cursor, 1, ids, tipo_renovacion)
            self.write(cursor, uid, ids, data_renovacio)

        return res

    _columns = {
        'firmante_id': fields.many2one('res.partner',
                                            string=u'Persona firmante'),
        'firmante_vat': fields.related('firmante_id', 'vat',
                                       type='char', size=32,
                                       string=u'Nº Documento firmante',
                                       readonly=True),
        'codi': fields.char(u'Codi intern', size=60, readonly=True),
        'tipus_renovacio': fields.selection(
            TIPUS_RENOVACIO,
            string=u'Tipo renovación precios', readonly=True,
            write=[
                'base.group_erp_manager'
            ],
            states={
                'esborrany': [('readonly', False)],
                'validar': [('readonly', False)],
                'modcontractual': [('readonly', False)]
            }
        ),
        'data_activacio': fields.date(u"Data última renovació"),
        'preavis': fields.many2one(
            'giscedata.polissa.preavis', u'Termini preavís'),
        'plazo_pago': fields.many2one(
            'account.payment.term', string=u'Termini de pagament'),
        'facturacion_estimada': fields.boolean(string=u'Facturació estimada'),
        'delegacion': fields.many2one('hr.office', 'Delegación'),
        'prescriptor': fields.many2one('res.partner', 'Prescriptor'),
        'consumo_anual_previsto': fields.integer(
            string='Consumo anual previsto (kWh)',
            readonly=True,
            states={
                'esborrany':[('readonly', False)],
                'validar': [('readonly', False),
                            ('required', True)],
                'modcontractual': [('readonly', False),
                                   ('required', True)],
            }
        ),
        'consumo_anual_calculado': fields.integer(
            string='Consumo anual calculado (kWh)'),
        'doc_validada': fields.function(_ff_doc_validada,
                                        type='boolean', method=True,
                                        string=u'Documentación validada',
                                        fnct_search=_fnct_search_doc_validada,
                                        ),
    }

    _defaults = {
        'name': _default_contract_number,
        'delegacion': _default_delegacion,
    }

GiscedataPolissa()


class GiscedataPolissaModcontractual(osv.osv):

    _name = 'giscedata.polissa.modcontractual'
    _inherit = 'giscedata.polissa.modcontractual'

    _columns = {
        'consumo_anual_previsto': fields.integer(
            string='Consumo anual previsto (kWh)'),
        'doc_validada': fields.boolean(string=u'Documentación validada'),
        'data_activacio': fields.date(u"Data última renovació"),
        'tipus_renovacio': fields.selection(
            TIPUS_RENOVACIO, string=u'Tipo renovación precios',
            write=[
                'base.group_erp_manager'
            ]),
    }

    _defaults = {
        'doc_validada': lambda *a: False,
    }

GiscedataPolissaModcontractual()


class RegistroVencimiento(osv.osv):

    _name = 'registro.vencimiento'
    _order = 'id desc'
    _description = 'Registro para mantener control del vencimiento de contratos'

    _columns = {
        'polissa_id': fields.many2one('giscedata.polissa', 'Polissa',
                                      required=True,
                                      select=True,
                                      ),
        'llista_preus_id': fields.many2one('product.pricelist',
                                           'Tarifa Comercializadora',
                                           required=True,
                                           ),
        'data_venciment': fields.date('Fecha de vencimiento',
                                      required=True,
                                      ),
        'active': fields.boolean('activo',
                                 )
    }

    _defaults = {
        'active': lambda *a: True,
    }

RegistroVencimiento()
