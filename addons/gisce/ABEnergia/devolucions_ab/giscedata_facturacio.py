# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from datetime import datetime
from collections import defaultdict
from dateutil.relativedelta import relativedelta

from osv import fields, osv




class GiscedataFacturacioDevolucio(osv.osv):
    """Customizaciones modelo de devoluciones
    """
    _name = 'giscedata.facturacio.devolucio'
    _inherit = 'giscedata.facturacio.devolucio'

    def obtenir_fres_afectades(self, cursor, uid, invoice_ids, extra_created):
        factures = extra_created.values()

        query = '''
                SELECT i.date_invoice,
                        i.number,
                        p.name
                FROM account_invoice i
                JOIN giscedata_facturacio_factura fra ON fra.invoice_id = i.id
                LEFT JOIN giscedata_polissa p on fra.polissa_id = p.id
                WHERE i.number in %s
                ORDER BY i.date_invoice
                '''
        cursor.execute(query, (tuple(factures),))
        f_processades = cursor.fetchall()
        min_date = f_processades[0][0]
        data_min = datetime.strptime(min_date, '%Y-%m-%d')
        data_minima = data_min - relativedelta(years=1)

        query = '''
        SELECT
            pol.name as num_ctto,
            i.number,
            i.date_invoice,  
            aips.name
        FROM account_invoice_pending_history pdh 
            LEFT JOIN account_invoice i ON i.id = pdh.invoice_id 
            LEFT JOIN giscedata_facturacio_factura gdf ON gdf.invoice_id = i.id 
            LEFT JOIN giscedata_polissa pol ON pol.id = gdf.polissa_id 
            LEFT JOIN account_invoice_pending_state aips ON aips.id = pdh.pending_state_id
        WHERE i.date_invoice >= %s
            AND i.type LIKE 'out_invoice'
            AND aips.name NOT ILIKE 'correct'
            AND aips.name NOT ILIKE 'correcte'
            AND aips.name NOT ILIKE 'correcto'
            AND pol.name IN (SELECT pol.name from giscedata_polissa pol 
                                JOIN giscedata_facturacio_factura fra ON fra.polissa_id = pol.id
                                JOIN account_invoice i ON i.id = fra.invoice_id 
                                WHERE i.number IN %s)
                '''
        cursor.execute(query, (data_minima, tuple(factures),))
        f_afectades = cursor.fetchall()

        factures_contracte = defaultdict(dict)
        for fra in f_afectades:
            factures_contracte[fra[0]][fra[1]] = fra[2]

        return f_processades, factures_contracte

    def obtenir_contractes_impagats(self, cursor, uid, ids, f_processades, f_ctte):

        contractes_impago = []
        for fra in f_processades:
            numero_contracte = fra[2]
            data_factura = fra[0]
            data_maxima = datetime.strptime(data_factura, '%Y-%m-%d') \
                          - relativedelta(days=1)
            data_minima = data_maxima - relativedelta(years=1)

            if numero_contracte not in contractes_impago:
                factures = f_ctte[numero_contracte]
                for num_fra in factures.keys():
                    fra_data = factures[num_fra]
                    fra_data = datetime.strptime(fra_data, '%Y-%m-%d')
                    if fra_data >= data_minima and fra_data <= data_maxima:
                        # hi ha alguna factura per aquest contracte
                        # impagada en el periode d'un any anterior a la
                        # data de factura que s'esta impagant ara
                        contractes_impago.append(numero_contracte)
                        break
        return contractes_impago

    def crear_extra_abono(self, cursor, uid, invoice_ids, extra_abonar):

        imd_obj = self.pool.get('ir.model.data')
        prod_obj = self.pool.get('product.product')
        extra_obj = self.pool.get('giscedata.facturacio.extra')

        extra_created = {}
        for extra in extra_abonar:
            price_unit = extra[0]
            name = extra[1]
            date_from = extra[2]
            date_to = extra[3]
            uos_id = extra[4]
            pol_id = extra[5]

            devolucions_product_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_devolucions',
                'product_devolucions'
            )[1]
            abono_gastos_product = prod_obj.browse(
                cursor, uid, devolucions_product_id
            )
            journal_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio',
                'facturacio_journal_energia'
            )[1]

            vals = {
                'name': u'Abono {0}'.format(name),
                'polissa_id': pol_id,
                'product_id': devolucions_product_id,
                'uos_id': uos_id,
                'date_from': date_from,
                'date_to': date_to,
                'price_unit': -price_unit,
                'quantity': 1,
                'term': 1,
                'account_id': abono_gastos_product.property_account_income.id,
                'tax_ids': [
                    (6, 0, [t.id for t in abono_gastos_product.taxes_id])
                ],
                'journal_ids': [(6, 0, [journal_id])]
            }
            id_extra = extra_obj.create(cursor, uid, vals)
            extra_created.update({id_extra: name})

        return extra_created

    def get_extra_abonar(self, cursor, uid, ids, extra_created, cttes_impago):
        ids_extra = extra_created.keys()

        if cttes_impago:

            query = '''
                    SELECT extra.price_unit,
                           extra.name,
                           extra.date_from,
                           extra.date_to,
                           extra.uos_id,
                           pol.id
                    FROM giscedata_facturacio_extra extra
                    JOIN giscedata_polissa pol ON pol.id = extra.polissa_id
                    WHERE extra.id in %s
                    AND pol.name not in %s
                    '''
            cursor.execute(query, (tuple(ids_extra), tuple(cttes_impago),))
            extra_abonar = cursor.fetchall()
        else:
            # s'han d'abonar totes
            query = '''
                    SELECT extra.price_unit,
                           extra.name,
                           extra.date_from,
                           extra.date_to,
                           extra.uos_id,
                           pol.id
                    FROM giscedata_facturacio_extra extra
                    JOIN giscedata_polissa pol ON pol.id = extra.polissa_id
                    WHERE extra.id in %s
                    '''
            cursor.execute(query, (tuple(ids_extra),))
            extra_abonar = cursor.fetchall()
        return extra_abonar

    def generate_despeses_devolucio(self, cursor, uid, invoice_ids, context=None):
        # generar líneas de abono del mismo importe que las generadas de gasto
        # si el impagador no es recurrente,
        # Per numero de contracte i enguany

        extra_created = super(GiscedataFacturacioDevolucio, self).\
            generate_despeses_devolucio(cursor, uid, invoice_ids,
                                        context=context
        )
        conf_obj = self.pool.get('res.config')
        abonar_no_recurrent = int(conf_obj.get(
            cursor, uid, 'despeses_devol_abonar_no_recurrents', '0'
        ))

        extra_abono = {}
        if abonar_no_recurrent:
            f_processades, factures_contracte = self.obtenir_fres_afectades(
                cursor, uid, invoice_ids, extra_created)

            cttes_impago = self.obtenir_contractes_impagats(
                cursor, uid, invoice_ids, f_processades, factures_contracte)

            # mirem de les línies extra generades, quines son de contractes
            # que no son impagadors recurrents
            extra_abonar = self.get_extra_abonar(cursor, uid, invoice_ids,
                                                 extra_created, cttes_impago)
            if extra_abonar:
                # generar linies equivalents en negatiu
                extra_abono = self.crear_extra_abono(cursor, uid, invoice_ids,
                                                     extra_abonar)
        return extra_created.update(extra_abono)

GiscedataFacturacioDevolucio()
