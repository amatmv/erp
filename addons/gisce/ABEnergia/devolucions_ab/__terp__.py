# -*- coding: utf-8 -*-
{
  "name": "Devolucions ABenergia",
  "description": """Customitzacions del modul devolucions""",
  "version": "0-dev",
  "author": "abenergia",
  "category": "",
  "depends": ['product',
              'devolucions_base',
              'giscedata_facturacio',
              ],
  "init_xml": [],
  "demo_xml": [],
  "update_xml": [
                 "devolucions_ab_data.xml",
                 "security/ir.model.access.csv",
                 ],
  "active": False,
  "installable": True
}