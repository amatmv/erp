from datetime import datetime, timedelta
from StringIO import StringIO
import logging
import re
import csv
from giscedata_polissa.giscedata_polissa import CONTRACT_IGNORED_STATES

from osv import osv
from osv.expression import OOQuery


from measures_conf import *

from giscedata_facturacio.giscedata_facturacio \
    import REFUND_RECTIFICATIVE_INVOICE_TYPES, \
    RECTIFYING_RECTIFICATIVE_INVOICE_TYPES


def filter_tarifa(tarifa):
    tarifa = tarifa.split(' ')[0]
    return tarifa


def filter_periode(periode):
    found = re.findall('(P[1-9])', periode)
    if found:
        return found[0]
    else:
        return None

EXCLUDE_ORIGINS = ['99', '40']


class ExtractPool(osv.osv):
    _name = 'extract.pool'
    _auto = False

    def get_polisses_ids(self, cursor, uid, date=None, context=None):
        logger = logging.getLogger('openerp.' + __name__ + '.get_polisses_ids')

        if context is None:
            context = {}
        if date is None:
            date = datetime.now().strftime('%Y-%m-%d')

        modcon_obj = self.pool.get('giscedata.polissa.modcontractual')

        ctx = context.copy()
        ctx['active_test'] = False
        search_params = [
            ('data_inici', '<=', date),
            ('data_final', '>=', date)
        ]
        modcon_ids = modcon_obj.search(cursor, uid, search_params, context=ctx)
        logger.info('Found {0} contracts'.format(len(modcon_ids)))
        polisses = []
        for modcon in modcon_obj.read(cursor, uid, modcon_ids, ['polissa_id']):
            polisses.append(modcon['polissa_id'][0])
        return polisses

    def contracts(self, cursor, uid, date=None, context=None):

        logger = logging.getLogger('openerp.' + __name__ + '.contracts')

        if context is None:
            context = {}
        if date is None:
            date = datetime.now().strftime('%Y-%m-%d')

        modcon_obj = self.pool.get('giscedata.polissa.modcontractual')

        ctx = context.copy()
        ctx['active_test'] = False
        search_params = [
            ('data_inici', '<=', date),
            ('data_final', '>=', date)
        ]
        modcon_ids = modcon_obj.search(cursor, uid, search_params, context=ctx)
        logger.info('Found {0} contracts on date {1}'.format(
            len(modcon_ids), date
        ))
        csv_file = StringIO()
        for modcon in modcon_obj.browse(cursor, uid, modcon_ids, context=ctx):

            estat = modcon.active and 'ACTIVO' or 'BAJA'
            potencies_periods = ['P%s' % x for x in range(1, 7)]
            potencies = modcon.get_potencies_dict()
            potencies_output = [potencies.get(p, '') for p in potencies_periods]
            tarifa = filter_tarifa(modcon.tarifa.name)

            line = ';'.join(map(str, [
                modcon.cups.name,
                modcon.distribuidora.ref,
                modcon.cups.dp,
                modcon.cups.id_municipi.name,
                modcon.polissa_id.data_alta,
                modcon.data_final,
                estat,
                modcon.data_inici,
                modcon.data_final,
                tarifa,
            ] + potencies_output + [
                modcon.data_inici,
                modcon.data_final
            ])) + '\n'
            csv_file.write(line)
        return csv_file.getvalue()

    def contracts2(self, cursor, uid, context=None):
        """
        Protocol 2 de contractes
        """

        logger = logging.getLogger('openerp.' + __name__ + '.contracts2')

        if context is None:
            context = {}

        polissa_obj = self.pool.get('giscedata.polissa')

        ctx = context.copy()
        ctx['active_test'] = False

        pol_ids = polissa_obj.search(cursor, uid, [], context=ctx)
        logger.info('Found {0} contracts (all contracts)'.format(
            len(pol_ids)
        ))
        csv_file = StringIO()
        for polissa in polissa_obj.browse(cursor, uid, pol_ids, context=ctx):
            if polissa.cups:
                modcon = polissa.modcontractual_activa
                if polissa.active:
                    if polissa.state in CONTRACT_IGNORED_STATES:
                        estat = 'PRESUNTO'
                    else:
                        estat = 'ACTIVO'
                else:
                    estat = 'BAJA'
                potencies_periods = ['P%s' % x for x in range(1, 7)]
                potencies = dict((p.periode_id.name, p.potencia) for p in polissa.potencies_periode)
                potencies_output = [potencies.get(p, '') for p in potencies_periods]
                tarifa = filter_tarifa(polissa.tarifa.name)

                line = ';'.join(map(str, [
                    polissa.cups.name,
                    polissa.distribuidora.ref,
                    polissa.cups.dp,
                    polissa.cups.id_municipi.name,
                    polissa.data_alta or '',
                    polissa.data_baixa or '',
                    estat,
                    tarifa,
                ] + potencies_output)) + '\n'
                csv_file.write(line)
            else:
                logger.warning(
                    'Skip contract with id {0} hasn\'t got CUPS'.format(
                        polissa.id)
                )
        value = csv_file.getvalue()
        csv_file.close()
        return value

    def contracts3(self, cursor, uid, context=None):
        """
        Protocol 3 de contractes
        """

        logger = logging.getLogger('openerp.' + __name__ + '.contracts3')

        if context is None:
            context = {}

        polissa_obj = self.pool.get('giscedata.polissa')

        ctx = context.copy()
        ctx['active_test'] = False

        ignored_states = [
            'esborrany',
            'validar',
            'pendent',
            'contracte',
            'novapolissa',
        ]

        search_params = [
            ('state', 'not in', ignored_states)
        ]

        pol_ids = polissa_obj.search(cursor, uid, search_params, context=ctx)
        logger.info('Found {0} contracts (all contracts)'.format(
            len(pol_ids)
        ))
        csv_file = StringIO()
        giscedata_polissa = self.pool.get('giscedata.polissa')
        for polissa in polissa_obj.browse(cursor, uid, pol_ids, context=ctx):
            # end date
            data_baixa = polissa.data_baixa or ''
            if polissa.cups:
                modcon = polissa.modcontractual_activa
                if polissa.active:
                    if polissa.state in CONTRACT_IGNORED_STATES:
                        estat = 'PRESUNTO'
                    elif polissa.state in ('tall',):
                        estat = 'CORTE'
                        # if cut off contract, puts today as end date
                        data_baixa = datetime.now().strftime('%Y-%m-%d')
                    else:
                        estat = 'ACTIVO'
                else:
                    estat = 'BAJA'
                potencies_periods = ['P%s' % x for x in range(1, 7)]
                potencies = dict((p.periode_id.name, p.potencia) for p in polissa.potencies_periode)
                potencies_output = [potencies.get(p, '') for p in potencies_periods]
                tarifa = filter_tarifa(polissa.tarifa.name)

                line = ';'.join(map(str, [
                    polissa.cups.name,
                    polissa.distribuidora.ref,
                    polissa.cups.dp,
                    polissa.cups.id_municipi.name,
                    polissa.data_alta or '',
                    data_baixa,
                    estat,
                    tarifa,
                ] + potencies_output)) + '\n'
                csv_file.write(line)
            else:
                logger.warning(
                    'Skip contract with id {0} hasn\'t got CUPS'.format(
                        polissa.id)
                )
        value = csv_file.getvalue()
        csv_file.close()
        return value

    def measures(self, cursor, uid, date=None, context=None):
        if context is None:
            context = {}
        if date is None:
            date = datetime.now()
        else:
            date = datetime.strptime(date, '%Y-%m-%d')

        date_from = (date - timedelta(days=365 * 2)).strftime('%Y-%m-%d')

        logger = logging.getLogger('openerp.' + __name__ + '.measures')

        polisses = self.get_polisses_ids(cursor, uid, date, context)

        lect_obj = self.pool.get('giscedata.facturacio.lectures.energia')
        fact_obj = self.pool.get('giscedata.facturacio.factura')
        crm_consums_obj = self.pool.get('crm.consums.periode')

        refund_invoices_sp = [
            ('data_final', '>=', date_from),
            ('tipo_rectificadora', 'in', REFUND_RECTIFICATIVE_INVOICE_TYPES),
            ('invoice_id.type', '=', 'in_refund'),
            ('invoice_id.state', 'in', ('open', 'paid')),
            ('invoice_id.journal_id.code', 'ilike', 'CENERGIA%')
        ]
        fact_ids = fact_obj.search(cursor, uid, refund_invoices_sp)
        refs = []
        if fact_ids:
            refs = [
                x['ref'][0]
                    for x in fact_obj.read(cursor, uid, fact_ids, ['ref'])
                        if x['ref']
            ]
        search_params = [
            ('data_actual', '>=', date_from),
            ('tipus', '=', 'activa'),
            ('factura_id.id', 'not in', refs),
            ('factura_id.invoice_id.journal_id.code', 'ilike', 'CENERGIA%'),
            ('factura_id.invoice_id.type', '=', 'in_invoice'),
            ('factura_id.tipo_rectificadora', 'in', RECTIFYING_RECTIFICATIVE_INVOICE_TYPES + ['N']),
            ('factura_id.invoice_id.state', 'in', ('open', 'paid'))
        ]
        csv_file = StringIO()
        lect_ids = lect_obj.search(cursor, uid, search_params)
        logger.info('Found {0} measures'.format(len(lect_ids)))
        for lectura_id in lect_ids:
            lectura = lect_obj.browse(cursor, uid, lectura_id)
            factura = lectura.factura_id

            if factura.polissa_id.id in polisses:
                polisses.remove(factura.polissa_id.id)

            line = ';'.join(map(str, [
                factura.cups_id.name,
                lectura.data_anterior,
                lectura.data_actual,
                filter_tarifa(lectura.name),
                'F1',
                factura.tarifa_acces_id.name == '3.1A LB' and 'S' or 'N',
                filter_periode(lectura.name),
                lectura.consum,
                lectura.lect_anterior,
                lectura.lect_actual
            ])) + '\n'
            csv_file.write(line)

        if polisses:
            logging.info('Checking for CRM measures')
            crm_consums_ids = crm_consums_obj.search(cursor, uid, [
                ('crm_polissa.polissa_id.id', 'in', polisses),
                ('data_final', '>=', date),
            ])
            for consum_id in crm_consums_ids:
                consum = crm_consums_obj.browse(cursor, uid, consum_id)
                polissa = consum.crm_polissa.polissa_id
                if polissa.id in polisses:
                    polisses.remove(polissa.id)
                line = ';'.join(map(str, [
                    polissa.cups.name,
                    consum.data_inici,
                    consum.data_final,
                    filter_tarifa(polissa.tarifa.name),
                    'P0',
                    polissa.tarifa.name == '3.1A LB' and 'S' or 'N',
                    filter_periode(consum.periode),
                    consum.consum,
                    0,
                    0
                ])) + '\n'
                csv_file.write(line)

        if polisses:
            logging.warning('Remaining {0} contracts without measure'.format(
                len(polisses)
            ))

        value = csv_file.getvalue()
        csv_file.close()
        return value

    def measures2(self, cursor, uid, date=None, context=None):
        """
        Protocol 2 del fitxer de LECTURES
        """
        if context is None:
            context = {}
        if date is None:
            date = datetime.now()
        else:
            date = datetime.strptime(date, '%Y-%m-%d')

        date_from = (date - timedelta(days=365 * 2)).strftime('%Y-%m-%d')

        logger = logging.getLogger('openerp.' + __name__ + '.measures2')

        lect_obj = self.pool.get('giscedata.facturacio.lectures.energia')
        fact_obj = self.pool.get('giscedata.facturacio.factura')
        crm_consums_obj = self.pool.get('crm.consums.periode')

        refund_invoices_sp = [
            ('data_final', '>=', date_from),
            ('tipo_rectificadora', 'in', REFUND_RECTIFICATIVE_INVOICE_TYPES),
            ('invoice_id.type', '=', 'in_refund'),
            ('invoice_id.state', 'in', ('open', 'paid')),
            ('invoice_id.journal_id.code', 'ilike', 'CENERGIA%')
        ]
        fact_ids = fact_obj.search(cursor, uid, refund_invoices_sp)
        refs = []
        if fact_ids:
            refs = [
                x['ref'][0]
                for x in fact_obj.read(cursor, uid, fact_ids, ['ref'])
                if x['ref']
                ]
        search_params = [
            ('data_actual', '>=', date_from),
            ('tipus', '=', 'activa'),
            ('factura_id.id', 'not in', refs),
            ('factura_id.invoice_id.journal_id.code', 'ilike', 'CENERGIA%'),
            ('factura_id.invoice_id.type', '=', 'in_invoice'),
            ('factura_id.tipo_rectificadora', 'in', RECTIFYING_RECTIFICATIVE_INVOICE_TYPES + ['N']),
            ('factura_id.invoice_id.state', 'in', ('open', 'paid'))
        ]
        csv_file = StringIO()
        lect_ids = lect_obj.search(cursor, uid, search_params)
        logger.info('Found {0} measures'.format(len(lect_ids)))
        for lectura_id in lect_ids:
            lectura = lect_obj.browse(cursor, uid, lectura_id)
            factura = lectura.factura_id

            line = ';'.join(map(str, [
                factura.cups_id.name,
                lectura.data_anterior,
                lectura.data_actual,
                filter_tarifa(lectura.factura_id.tarifa_acces_id.name),
                'F1',
                factura.tarifa_acces_id.name == '3.1A LB' and 'S' or 'N',
                filter_periode(lectura.name),
                lectura.consum,
                lectura.lect_anterior,
                lectura.lect_actual
            ])) + '\n'
            csv_file.write(line)

        logging.info('Checking for CRM measures')
        crm_consums_ids = crm_consums_obj.search(cursor, uid, [
            ('data_final', '>=', date_from),
        ])
        for consum_id in crm_consums_ids:
            consum = crm_consums_obj.browse(cursor, uid, consum_id)
            polissa = consum.crm_polissa.polissa_id
            if not polissa.cups:
                continue

            line = ';'.join(map(str, [
                polissa.cups.name,
                consum.data_inici,
                consum.data_final,
                filter_tarifa(polissa.tarifa.name),
                'P0',
                polissa.tarifa.name == '3.1A LB' and 'S' or 'N',
                filter_periode(consum.periode),
                consum.consum,
                0,
                0
            ])) + '\n'
            csv_file.write(line)

        value = csv_file.getvalue()
        csv_file.close()
        return value

    def measures3(self, cursor, uid, date=None, context=None):
        """
        Protocol 3 del fitxer de LECTURES
        """
        if context is None:
            context = {}
        if date is None:
            date = datetime.now()
        else:
            date = datetime.strptime(date, '%Y-%m-%d')

        date_from = (date - timedelta(days=365 * 2)).strftime('%Y-%m-%d')

        logger = logging.getLogger('openerp.' + __name__ + '.measures3')

        lect_obj = self.pool.get('giscedata.facturacio.lectures.energia')
        fact_obj = self.pool.get('giscedata.facturacio.factura')
        crm_consums_obj = self.pool.get('crm.consums.periode')

        refund_invoices_sp = [
            ('data_final', '>=', date_from),
            ('tipo_rectificadora', 'in', REFUND_RECTIFICATIVE_INVOICE_TYPES),
            ('invoice_id.type', '=', 'in_refund'),
            ('invoice_id.state', 'in', ('open', 'paid')),
            ('invoice_id.journal_id.code', 'ilike', 'CENERGIA%')
        ]
        q = OOQuery(fact_obj, cursor, uid)
        fact_sql = q.select(['ref']).where(refund_invoices_sp)
        cursor.execute(*fact_sql)
        refs = [x['ref'] for x in cursor.dictfetchall() if x['ref']]

        q = OOQuery(lect_obj, cursor, uid)
        search_params = [
            ('data_actual', '>=', date_from),
            ('tipus', '=', 'activa'),
            ('factura_id.id', 'not in', refs),
            ('factura_id.invoice_id.refund_by_id', '=', None),
            ('factura_id.invoice_id.state', 'in', ('open', 'paid')),
            ('factura_id.invoice_id.journal_id.code', 'ilike', 'CENERGIA%'),
            ('factura_id.invoice_id.type', '=', 'in_invoice'),
            ('factura_id.tipo_rectificadora', 'in',
             RECTIFYING_RECTIFICATIVE_INVOICE_TYPES + ['N']),
            '|',
            ('origen_id.codi', 'not in', EXCLUDE_ORIGINS),
            ('consum', '>', 0)
        ]
        lects_sql = q.select(
            ['id', 'factura_id.cups_id.name', 'data_anterior',
             'data_actual',
             'factura_id.tarifa_acces_id.name',
             'name', 'consum', 'lect_anterior', 'lect_actual'],
            only_active=False
        ).where(search_params)
        error_file = open(error_measures_path, 'w')
        error_csv = csv.writer(error_file, delimiter=';')
        csv_file = StringIO()
        logger.info('Start')
        cursor.execute(*lects_sql)
        logger.info('End')
        logger.info('Found {0} measures'.format(cursor.rowcount))
        percent = measures_percent
        percent_menor = (100 - percent)/100.0
        percent_major = (100 + percent)/100.0
        for lectura in cursor.dictfetchall():
            dif_lect = float(lectura['lect_actual']) - float(lectura['lect_anterior'])
            f_consum = float(lectura['consum'])
            row = [
                lectura['factura_id.cups_id.name'],
                lectura['data_anterior'],
                lectura['data_actual'],
                filter_tarifa(lectura['factura_id.tarifa_acces_id.name']),
                'F1',
                lectura['factura_id.tarifa_acces_id.name'] == '3.1A LB' and 'S' or 'N',
                filter_periode(lectura['name']),
                lectura['consum'],
                lectura['lect_anterior'],
                lectura['lect_actual']
            ]
            if lectura['consum'] < 0:
                error_csv.writerow(map(str, row))
            else:
                if dif_lect * percent_menor <= f_consum <= dif_lect * percent_major:
                    line = ';'.join(map(str, row)) + '\n'
                    csv_file.write(line)
                else:
                    error_csv.writerow(map(str, row))

        logging.info('Checking for CRM measures')
        crm_consums_ids = crm_consums_obj.search(cursor, uid, [
            ('data_final', '>=', date_from),
        ])
        for consum_id in crm_consums_ids:
            consum = crm_consums_obj.browse(cursor, uid, consum_id)
            polissa = consum.crm_polissa.polissa_id
            if not polissa.cups:
                continue

            line = ';'.join(map(str, [
                polissa.cups.name,
                consum.data_inici,
                consum.data_final,
                filter_tarifa(polissa.tarifa.name),
                'P0',
                polissa.tarifa.name == '3.1A LB' and 'S' or 'N',
                filter_periode(consum.periode),
                consum.consum,
                0,
                0
            ])) + '\n'
            if consum.consum < 0:
                error_csv.writerow(map(str, row))
            else:
                csv_file.write(line)
        error_file.close()
        value = csv_file.getvalue()
        csv_file.close()
        return value

ExtractPool()
