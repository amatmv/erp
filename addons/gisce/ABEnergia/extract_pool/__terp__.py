# -*- coding: utf-8 -*-
{
    "name": "",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Extract files to External Pool Application
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "giscedata_polissa_comer",
        "giscedata_facturacio_comer",
        "crm_elec"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
