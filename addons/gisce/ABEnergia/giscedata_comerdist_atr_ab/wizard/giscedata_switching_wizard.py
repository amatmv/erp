# -*- coding: utf-8 -*-
from osv import osv


class GiscedataSwitchingWizard(osv.osv_memory):
    _name = 'giscedata.switching.wizard'
    _inherit = 'giscedata.switching.wizard'

    def get_user_id(self, atr_case):
        user_id = False
        where = atr_case.whereiam
        if where == 'comer':
            if atr_case.user_id.login in ['sync', 'admin']:
                if atr_case.cups_polissa_id.user_id:
                    user_id = atr_case.cups_polissa_id.user_id.id
                else:
                    if atr_case.section_id.user_id:
                        user_id = atr_case.section_id.user_id.id
        elif where == 'distri':
            if atr_case.section_id.user_id:
                user_id = atr_case.section_id.user_id.id
        return user_id

GiscedataSwitchingWizard()
