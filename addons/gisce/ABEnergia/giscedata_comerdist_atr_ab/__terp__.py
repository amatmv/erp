# -*- coding: utf-8 -*-
{
    "name": "Comerdist ATR ABEnergia",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Customize how to assign responsible on ATR case
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Custom",
    "depends":[
        "giscedata_comerdist_atr"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
