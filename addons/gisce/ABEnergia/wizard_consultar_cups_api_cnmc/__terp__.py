# -*- coding: utf-8 -*-
{
    "name": "Assistent consultar CUPS API CNMC",
    "description": """
        Assistent que consulta les dades d'electricitat i gas dona't un cert CUPS. Genera un fitxer CSV a descarregar.
        """,
    "version": "0-dev",
    "author": "abenergia",
    "category": "",
    "depends":[
        "base_extended",
        "giscedata_sips",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/consultar_cups_security.xml",
        "wizard/wizard_consultar_cups_view.xml",
        "wizard_consultar_cups_data.xml",
        "security/ir.model.access.csv",
    ],
    "active": False,
    "installable": True
}
