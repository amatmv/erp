# -*- coding: utf-8 -*-

from __future__ import (absolute_import)
from osv import osv, fields
from StringIO import StringIO
from tools import config
from datetime import timedelta
import base64
import csv
import redis

from cnmc_client import Client

TYPE_ENERGY = [
        ('SIPS2_PS_ELECTRICIDAD','SIPS2_PS_ELECTRICIDAD'),
        ('SIPS2_CONSUMOS_ELECTRICIDAD', 'SIPS2_CONSUMOS_ELECTRICIDAD'),
        ('SIPS2_PS_GAS','SIPS2_PS_GAS'),
        ('SIPS2_CONSUMOS_GAS','SIPS2_CONSUMOS_GAS')]

TARIFFS_OCSUM = {
    '001': "2.0A",
    '003': "3.0A",
    '004': "2.0DHA",
    '005': "2.1A",
    '006': "2.1DHA",
    '007': "2.0DHS",
    '008': "2.1DHS",
    '011': "3.1A",
    '012': "6.1",
    '013': "6.2",
    '014': "6.3",
    '015': "6.4",
    '016': "6.5",
}

class WizardConsultarCups(osv.osv_memory):

    _name = 'wizard.consult.cups'

    def get_default_info(self, cursor, uid, context=None):
        #Instructions for enter the CUPS file
        if context is None:
            context = {}
        return ("Introduce un fichero csv, con los CUPS en la primera "
                "columna (máximo 10). Ejemplo:\nCUPS1\nCUPS2\nCUPS3")

    def action_consult_cups(self, cursor, uid, ids, context=None):
        """ Report """

        if context is None:
            context = {}

        wizard = self.browse(cursor, uid, ids[0], context=context)

        cups_names = wizard.cups_names
        type_energy = wizard.type_energy
        bolea_ftx = wizard.bolea_ftx

        if bolea_ftx:
            # Check the file
            fitxer = StringIO(base64.b64decode(wizard.listofcups_file))
            reader = csv.reader(fitxer, delimiter=';')
            list_cups = []
            for linia in reader:
                list_cups.extend(linia)
        else:
            # Check the string with CUPS
            list_cups = cups_names.replace(',', ';').\
                                   replace(' ', '').\
                                   split(';')

        if len(list_cups)>10:
            list_cups = list_cups[:10]

        # Comprovem primer que no s'hagi superat el limit/hora de consultes
        conf_obj = self.pool.get('res.config')
        limit = int(conf_obj.get(cursor, uid, 'cnmc_sips_limit_hora_api', '0'))
        if limit:
            client = redis.StrictRedis(host='db', port=6379)
            consultes = int(client.incr('comptador_api_cnmc'))
            if consultes == 1:
                client.expire('comptador_api_cnmc', timedelta(hours=1))
            elif consultes > limit:
                raise osv.except_osv("Límite de consultas alcanzado",
                                     "Se ha alcanzado el limite de consultas "
                                     "por hora. Por favor, inténtelo más "
                                     "tarde.")

        environment = self.pool.get('res.config').get(cursor, uid,
                                        'cnmc_sips_active_environment', 'prod')
        #Connectar amb servidor CNMC
        key = config.get('cnmcapi_key', '')
        secret = config.get('cnmcapi_secret', '')
        configuracio = {
            'environment': environment,
            'key': key,
            'secret': secret,
                        }
        client = Client(**configuracio)
        respon = client.fetch(cups=list_cups,
                              file_type=type_energy,
                              as_csv=False)

        codi = respon.code
        reportCNMC = list(respon.result)
        if codi != 200:
            raise osv.except_osv("Error",
                                 "{} {}".format(str(respon), str(reportCNMC)))

        if len(reportCNMC) == 1:
            raise osv.except_osv("Datos no encontrados",
                                 "No se han encontrado datos para este CUPS")

        respostareader = csv.DictReader(reportCNMC, delimiter=",", quotechar='"')
        capcaleres = respostareader.fieldnames

        # Escrivim el text del ftx
        report = "Consulta {}\n".format(type_energy)
        report += ';'.join(capcaleres)
        report += '\n'

        if type_energy == TYPE_ENERGY[0][0]:
            coditarifa = 'codigoTarifaATREnVigor'
        elif type_energy == TYPE_ENERGY[1][0]:
            coditarifa = 'codigoTarifaATR'

        for linia in respostareader:
            #Codi dedicat a mapejar CODIGOTARIFA (2.01A, etc...)
            if type_energy in (TYPE_ENERGY[0][0], TYPE_ENERGY[1][0]):
                codigo3nums = linia.get(coditarifa)
                linia[coditarifa] = TARIFFS_OCSUM[codigo3nums]
            report += ';'.join(linia.get(capcalera) for capcalera in capcaleres)
            report += '\n'

        wizard.write({
            'report': base64.b64encode(report),
            'filename_report': 'consultar_dades_api_cnmc.csv',
        })
        return

    _columns = {
        'cups_names': fields.text('CUPS'),
        'report': fields.binary('Fitxer informe'),
        'filename_report': fields.char('Nom del fitxer', size=256),
        'type_energy': fields.selection(TYPE_ENERGY, string="Tipus de dades"),
        'bolea_ftx': fields.boolean('Subir fichero con CUPS'),
        'listofcups_file': fields.binary('Fichero CUPS'),
        'info': fields.text('Informació', readonly=True)
    }

    _defaults = {
        'cups_names': lambda *a: '',
        'type_energy': lambda *a: "SIPS2_PS_ELECTRICIDAD",
        'info': get_default_info
    }

WizardConsultarCups()
