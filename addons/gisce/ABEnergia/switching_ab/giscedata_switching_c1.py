# -*- coding: utf-8 -*-

from osv import osv


class GiscedataSwitchingC1_06(osv.osv):
    _name = 'giscedata.switching.c1.06'
    _inherit = 'giscedata.switching.c1.06'

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Crida la funció de la superclasse i crea un cas CRM"""
        if context is None:
            context = {}

        pas_id = super(GiscedataSwitchingC1_06, self).create_from_xml(
            cursor, uid, sw_id, xml, context=context
        )
        sw_obj = self.pool.get('giscedata.switching')
        name = 'C.C. activado'
        crm_case_id = sw_obj.crear_crm_case_from_cas_switching(
            cursor, uid, sw_id, pas_id, self._name, name, context=context)

        crm_case_o = self.pool.get('crm.case')
        crm_case_o.set_penalty(
            cursor, uid, crm_case_id, context=context
        )

        return pas_id


GiscedataSwitchingC1_06()


class GiscedataSwitchingC1_10(osv.osv):
    _name = 'giscedata.switching.c1.10'
    _inherit = 'giscedata.switching.c1.10'

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        pas_id = super(GiscedataSwitchingC1_10, self).create_from_xml(
            cursor, uid, sw_id, xml, context=context
        )
        sw_obj = self.pool.get('giscedata.switching')
        sw_data = sw_obj.read(
            cursor, uid, sw_id, ['name', 'codi_sollicitud'])
        new_description = 'ANULADO {}'.format(sw_data['name'])
        historize_description = (
            'Importada anulación de cambio de comercializadora '
            '(código solicitud ATR {})'.format(sw_data['codi_sollicitud'])
        )
        sw_obj.actualitzar_descripcio_i_historitzar(
            cursor, uid, sw_id,
            new_description, historize_description, context=context
        )
        return pas_id


GiscedataSwitchingC1_10()


class GiscedataSwitchingC1_11(osv.osv):
    _name = 'giscedata.switching.c1.11'
    _inherit = 'giscedata.switching.c1.11'

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Crida la funció de la superclasse i crea un cas CRM"""
        if context is None:
            context = {}

        pas_id = super(GiscedataSwitchingC1_11, self).create_from_xml(
            cursor, uid, sw_id, xml, context=context
        )
        sw_obj = self.pool.get('giscedata.switching')
        name = 'C.C. pendiente'
        crm_case_id = sw_obj.crear_crm_case_from_cas_switching(
            cursor, uid, sw_id, pas_id, self._name, name, context=context)

        crm_case_o = self.pool.get('crm.case')
        crm_case_o.set_penalty(
            cursor, uid, crm_case_id, context=context
        )
        return pas_id


GiscedataSwitchingC1_11()


class GiscedataSwitchingC1_12(osv.osv):
    _name = 'giscedata.switching.c1.12'
    _inherit = 'giscedata.switching.c1.12'

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        pas_id = super(GiscedataSwitchingC1_12, self).create_from_xml(
            cursor, uid, sw_id, xml, context=context
        )
        sw_obj = self.pool.get('giscedata.switching')
        sw_data = sw_obj.read(
            cursor, uid, sw_id, ['name', 'codi_sollicitud'])
        new_description = 'RECHAZADO {}'.format(sw_data['name'])
        historize_description = (
            'Importado rechazo para el cambio de comercializadora '
            '(código solicitud ATR {})'.format(sw_data['codi_sollicitud'])
        )
        sw_obj.actualitzar_descripcio_i_historitzar(
            cursor, uid, sw_id,
            new_description, historize_description, context=context
        )
        return pas_id


GiscedataSwitchingC1_12()
