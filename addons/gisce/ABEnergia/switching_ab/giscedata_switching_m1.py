# -*- coding: utf-8 -*-

from __future__ import absolute_import
from osv import osv
from giscedata_switching.giscedata_switching import SwitchingException


class GiscedataSwitchingM1_01(osv.osv):
    _name = 'giscedata.switching.m1.01'
    _inherit = 'giscedata.switching.m1.01'

    def config_step_validation(self, cursor, uid, ids, vals, context=None):
        if context is None:
            context = {}

        res = super(GiscedataSwitchingM1_01, self).config_step_validation(
            cursor, uid, ids, vals, context=context
        )
        if vals.get('change_type') == 'tarpot':
            if not vals.get('firma_nous_preus'):
                raise SwitchingException("Faltan datos. Campo no completado.", ['firma_nous_preus'])

        return res

    def config_step(self, cursor, uid, ids, vals, context=None):
        if context is None:
            context = {}

        res = super(GiscedataSwitchingM1_01, self).config_step(
            cursor, uid, ids, vals, context=context
        )
        pas = self.browse(cursor, uid, ids, context=context)
        pas.sw_id.write({
            "nou_periode_modcon": vals.get('firma_nous_preus', False) == "c"
        })
        return res


GiscedataSwitchingM1_01()

