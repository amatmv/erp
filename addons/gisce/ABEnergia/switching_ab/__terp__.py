# -*- coding: utf-8 -*-
{
    "name": "Baixa per impagament 5 dies després del tall",
    "description": """Afegeix un script per crear un B1 de baixa per impagament 5 dies després del tall""",
    "version": "0-dev",
    "author": "Abenergia",
    "category": "CRM",
    "depends":[
        "giscedata_polissa",
        "giscedata_switching_comer",
        "product_pricelist_ofertable",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "switching_ab_data.xml",
        "giscedata_switching_view.xml",
        "giscedata_switching_mod_con_wizard_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
