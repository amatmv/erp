# -*- coding: utf-8 -*-
from osv import osv, fields

class GiscedataSwitchingModConWizard(osv.osv_memory):
    _name = 'giscedata.switching.mod.con.wizard'
    _inherit = 'giscedata.switching.mod.con.wizard'

    def get_tariff_domain(self, cursor, uid, tariff_id, context=None):
        # El get_tariff_domain original retorna les llistes de preus
        # compatibles amb la tarifa ATR
        # Volem restringir-ho perque només retorni les llistes ofertables o bé
        # mantenir la tarifa actual del client

        tariff_domain, price_lists = super(GiscedataSwitchingModConWizard, self).get_tariff_domain(
                      cursor, uid, tariff_id, context=context)
        plobj = self.pool.get('product.pricelist')
        params = [
            ('ofertable', '=', True),
        ]
        llistes_ofertables = plobj.search(cursor, uid, params)

        # afegim la llista actual del contracte a les ofertables
        llista_actual = super(GiscedataSwitchingModConWizard, self).\
            _get_default_retail_tariff(cursor, uid, context=context)
        llistes_ofertables.append(llista_actual)

        set_llistes = set(llistes_ofertables).intersection(set(price_lists))
        llistes = list(set_llistes)

        # afegim la nostra condició al domain
        tariff_domain.append(('id', 'in', llistes))
        return tariff_domain, llistes

    _columns = {
        'firma_nous_preus': fields.selection(
            [('t', u"Tácito"), ('c', u"Contrato Firmado")],
            "Firma nuevos precios"
        )
    }

    _defaults = {
        'firma_nous_preus': lambda *a: False
    }

GiscedataSwitchingModConWizard()
