# -*- coding: utf-8 -*-

import pooler
import logging
from osv import osv, fields
from tools import config
from tools.translate import _
from osv.orm import except_orm
from addons import get_module_resource

from datetime import datetime
from datetime import timedelta
from workalendar.europe import Spain

class CrearBaja5DiasTrasCorte(osv.osv_memory):

    _name = 'crear.baixa.administrativa'

    def crear_baixes_impagament(self, cr, uid, to_addr, cc_addr, context=None):
        if not context:
            context = {}

        casos_pols = self.buscar_atr_tall(cr, uid, context=context)

        info_obj = self.pool.get('giscedata.switching.step.info')

        casos_generats = {}

        # obtenir la data de fa x dies laborables
        calendar = Spain()
        cfg_obj = self.pool.get('res.config')
        dies = int(cfg_obj.get(cr, uid, 'sw_num_days_to_b1_admin', '10'))
        fa_x_dies = calendar.sub_working_days(datetime.today(), dies)
        fa_x_dies = datetime.strftime(fa_x_dies, '%Y-%m-%d')

        for cas in casos_pols.itervalues():

            # mirem la data prevista del tall en el pas 01
            params = [('sw_id', '=', cas['id']),
                      ('step_id', '=', '01')]
            id_infopas = info_obj.search(cr, uid, params)
            infopas = info_obj.read(cr, uid, id_infopas, context=context)[0]
            mdlstr, infoid = infopas['pas_id'].split(',')
            mdl = self.pool.get(mdlstr)
            pas = mdl.read(cr, uid, [infoid], ['data_accio'])[0]
            data_accio = pas['data_accio']

            # si han passat x dies des de la data del tall, generem la
            # baixa administrativa (baja por impago)
            # La data del tall considerem que es la data prevista del pas 01,
            # que es quan s'envien els xml de tall per impagament
            if data_accio == fa_x_dies:
                baixa = self.gen_baixa_administrativa(cr, uid,
                                                      cas['cups_polissa_id'][0],
                                                      cas['id'],
                                                      cas['ref'],
                                                      fa_x_dies,
                                                      context=context)
                casos_generats.update({cas['cups_id'][1]: baixa})

        if casos_generats:
            self.enviar_mail_a_responsable(cr, uid, casos_generats,
                                           to_addr, cc_addr, context=context)

        return True

    def buscar_atr_tall(self, cr, uid, context=None):
        '''Busca les polisses que tenen un B1:
         - OBERT
         - amb motiu 'corte de suministro'
         - que van lligats a una factura perquè s'hagi generat automàtic
        No cal que estiguin tallades ni el B1 acceptat (el grupo per exemple
        de vegades no envia 02)'''

        if not context:
            context = {}

        sw_obj = self.pool.get('giscedata.switching')
        stepinfo_obj = self.pool.get('giscedata.switching.step.info')

        params_sw = [('proces_id', '=', 'B1'),
                     ('state', '=', 'open'),
                     ('ref', '!=', False),
                     ('enviament_pendent', '=', False),
                     ('rebuig', '=', False)]
        ids_sw = sw_obj.search(cr, uid, params_sw)
        casos_sw = sw_obj.read(cr, uid, ids_sw, ['cups_polissa_id',
                                                 'create_date',
                                                 'ref',
                                                 'cups_id'])

        casos_pols = {}
        for cas in casos_sw:

            # mirem si al pas 01 es tracta d'un ATR motiu 'corte de suministro'
            polissa_id = cas['cups_polissa_id'][0]
            params = [('sw_id', '=', cas['id']),
                      ('step_id', '=', '01')]
            id_infopas = stepinfo_obj.search(cr, uid, params)
            infopas = stepinfo_obj.read(cr, uid, id_infopas, context=context)[0]
            mdlstr, infoid = infopas['pas_id'].split(',')
            mdl = self.pool.get(mdlstr)
            infopas = mdl.read(cr, uid, [infoid], ['data_acceptacio',
                                                   'motiu'])[0]
            # si es qualsevol altre motiu, no volem demanar la administrativa
            if not infopas['motiu'] == u'03':
                continue

            # si hi ha més d'un B1 amb pas 02 agafem el més recent,
            # no hauria de passar pero, pot voler dir que el tall mes antic
            # s'hagi anulat però no ens consti per algun motiu?
            if polissa_id in casos_pols:
                if casos_pols[polissa_id]['create_date'] < cas['create_date']:
                    casos_pols[polissa_id] = cas
            else:
                casos_pols[polissa_id] = cas

        return casos_pols

    def gen_baixa_administrativa(self, cr, uid, id_polissa, id_sw_tall,
                                 id_fra, fa_x_dies, context=None):
        logger = logging.getLogger(
            'openerp.{0}.gen_baixa_administrativa'.
                format(__name__)
        )
        if not context:
            context = {}
        pol_obj = self.pool.get('giscedata.polissa')
        sw_obj = self.pool.get('giscedata.switching')
        inv_obj = self.pool.get('giscedata.facturacio.factura')
        polissa = pol_obj.browse(cr, uid, [id_polissa], context=context)[0]
        conf_obj = self.pool.get('res.config')

        str_id_fra = id_fra.split(',')[1]
        number = inv_obj.read(cr, uid, int(str_id_fra), ['number'])['number']

        config = dict(
            data_accio = datetime.today().strftime('%Y-%m-%d'),
            motiu = '04',
            comments = _('Baja administrativa auto tras'
                         'ATR con id {0}, con id fra. origen {1}'.
                         format(id_sw_tall, number))
        )
        res = polissa.crear_cas_atr('B1', config, context=context)

        sw_id = res[2]
        if not res[2]:
            raise osv.except_osv('Error', res[1])
        # mirar en quin estat s'ha de crear la baixa administrativa
        state = 'open'
        if int(conf_obj.get(cr, uid, 'sw_b1_admin_draft', 0)):
            state = 'draft'
        sw_obj.write(cr, uid, [sw_id], {
            'ref': id_fra,
            'state': state
        })
        logger.info('Creada baixa administrativa id {0} al contracte amb id {1}'
                    ''.format(sw_id, id_polissa))
        return sw_id

    def crear_body(self, cr, uid, baixes_gen, context=None):

        llistacups = [cups for cups in baixes_gen.iterkeys()]
        sql_file = 'query_baixes_generades.sql'
        query = get_module_resource('switching_ab', 'sql', sql_file)
        sql = open(query).read()

        body_html = '<p>Listado de bajas generadas</p>'
        for cups in llistacups:
            # executar-ho una per una, no es pot tots de cop
            cr.execute(sql, (cups, ))
            for row in cr.fetchall(): # sera 1 sempre
                body_html += ''.join('<p>' + ', '.join(row) + '</p>')

        return body_html

    def enviar_mail_a_responsable(self, cr, uid, baixes_gen,
                                  to_addr, cc_addr, context=None):
        if not context:
            context = {}

        logger = logging.getLogger(
            'openerp.{0}.enviar_mail_a_responsable'.
                format(__name__)
        )

        body_html = self.crear_body(cr, uid, baixes_gen, context=None)

        values = {
            'pem_subject': 'Bajas administrativas creadas automáticamente',
            'pem_body_text': body_html,
            'pem_cc': cc_addr,
            'pem_to': to_addr,
        }

        mail_id = self.send_email(cr, uid, values, to_addr, context=context)

        logger.info(
            'Enviat email id {0} de baixes administratives creades '
            'automàtiques en data {1}'.format(mail_id,str(datetime.today())))

        return mail_id

    def send_email(
            self, cursor, uid, email_values, email_from=False, context=None):
        if not context:
            context = {}
        acc_obj = self.pool.get('poweremail.core_accounts')
        mail_obj = self.pool.get('poweremail.mailbox')

        if not email_values.get('pem_account_id', False):
            if context.get('from_account', False):
                acc_id = context.get('from_account')
            else:
                search_values = [('user', '=', uid), ('state', '=', 'approved')]
                acc_id = acc_obj.search(
                    cursor, uid, search_values,context=context)
                if not acc_id:
                    err = _(u"ERROR: No existeix compte de correu d'enviament")
                    raise except_orm('Error', err)
                if isinstance(acc_id, (list, tuple)):
                    acc_id = acc_id[0]
            email_values.update({'pem_account_id': acc_id})

        emailfrom = acc_obj.read(
            cursor, uid, acc_id, ['email_id'])['email_id']
        email_values.update({'pem_from': emailfrom})

        # Using new cursor to persist mail before use
        db = pooler.get_db_only(cursor.dbname)
        pbcr = db.cursor()
        mail_id = mail_obj.create(pbcr, uid, email_values, context)
        pbcr.commit()
        pbcr.close()
        mail_obj.send_this_mail(cursor, uid, [mail_id], context)

        return mail_id

CrearBaja5DiasTrasCorte()
