# -*- coding: utf-8 -*-

from osv import osv, fields
from datetime import datetime
from dateutil.relativedelta import relativedelta


class GiscedataSwitching(osv.osv):

    _name = 'giscedata.switching'
    _inherit = 'giscedata.switching'

    def activa_polissa_cas_atr(self, cursor, uid, sw_id, context=None):
        if context is None:
            context = {}
        res = super(GiscedataSwitching, self).activa_polissa_cas_atr(
                      cursor, uid, sw_id, context=context)

        # passar l'estat de documentació del cas ATR a la mod contractual
        sw = self.browse(cursor, uid, sw_id)
        polissa = sw.cups_polissa_id
        if polissa.modcontractual_activa:
            modcontractual = polissa.modcontractual_activa
            modcontractual.write({'doc_validada': sw.doc_validada},
                                  context=context)
        return res

    def crear_crm_case_from_cas_switching(self, cursor, uid, sw_id,
                                          pas_id, pas_model_name, name,
                                          context=None):
        """
        Només pels casos C1 i C2
        :param cursor:
        :param uid:
        :param sw_id:
        :param name: Breu descripció del cas.
        :param context:
        :return:
        """
        if context is None:
            context = {}

        crm_case_obj = self.pool.get('crm.case')
        crm_case_section_obj = self.pool.get('crm.case.section')
        pas_obj = self.pool.get(pas_model_name)
        data_activ_baixa = pas_obj.read(cursor, uid, pas_id,
                                        ['data_activacio'])['data_activacio']
        crm_case_section_id = crm_case_section_obj.search(
            cursor, uid, [('code', '=', 'SW_SALIENTE')])[0]
        sw_obj = self.pool.get('giscedata.switching')
        sw = sw_obj.browse(cursor, uid, sw_id)
        polissa = sw.cups_polissa_id

        if polissa.tipus_renovacio == 'expressa':
            data = polissa.data_activacio
        else:
            data = False
        if data:
            # Si pel que sigui fa més d'un any de la última renovació expressa
            # i encara no ha passat a tacita, no procedeix indicar data
            data = datetime.strptime(data, '%Y-%m-%d')  # %H:%M:%S')
            if data + relativedelta(years=1) < datetime.now():
                data = False
        name = "{}.  CNAE: {}. Tarifa: {}. Grupo de facturación: {}.".format(
            polissa.name, polissa.cnae.name, polissa.llista_preu.name,
            polissa.group_id.name
        )

        vals = {
            # Abonat: pòlissa del pas
            'polissa_id': polissa.id,
            # Responsable: comercial de la pòlissa
            'user_id': polissa.user_id.id,
            # Secció: secció que es digui 'SALIENTE'
            'section_id': crm_case_section_id,
            # Data: data ultima renovacio expressa
            'date': data,
            # Posem el camp date_deadline per mantenir l'ordre, del crm.case és
            # _order = 'priority, date_deadline desc, date desc,id desc'
            # Abans teniem sempre camp date, ara hi haurà casos que no en tenim
            # Al aplicar-ho s'ha migrat els casos de seccio saliente existents
            # per indicar la date_deadline amb la data de creació, per no perdre
            # l'ordre
            'date_deadline': datetime.now(),
            'date_action_next': data_activ_baixa,
            # Partner: titular del contracte
            'partner_id': polissa.titular.id,
            # Contacte partner: adreça de notificació del contracte
            'partner_address_id': polissa.direccio_notificacio.id,
            # Email empresa: email de notificació
            'email_from': polissa.notificacio_email,
            # Estat: esborrany
            'state': 'draft',
            'name': name,
            'ref': 'giscedata.switching,%s' % sw_id,
        }
        crm_case_id = crm_case_obj.create(cursor, uid, vals)
        return crm_case_id

    def actualitzar_descripcio_i_historitzar(self, cursor, uid, sw_id,
                                             description, historize_description,
                                             context=None):
        if context is None:
            context = {}
        sw_obj = self.pool.get('giscedata.switching')
        sw = sw_obj.browse(cursor, uid, sw_id)
        crm_obj = self.pool.get('crm.case')
        search_params = [
            ('section_id.code', '=', 'SW_SALIENTE'),
            ('ref', '=', 'giscedata.switching,%s' % sw_id)
        ]
        crm_ids = crm_obj.search(cursor, uid, search_params)
        if len(crm_ids) == 1:
            crm_obj.write(cursor, uid, crm_ids, {
                'name': description,
                'description': historize_description
            })
            crm_obj.case_log(cursor, uid, crm_ids)

    _columns = {
        'doc_validada': fields.boolean(u'Documentación validada'),
        'nou_periode_modcon': fields.boolean(
            u'Renovar contrato al activar',
            help=u"Cuando se active el caso ATR, si este genera una nueva "
                 u"modificación contractual se generará con un periodo nuevo "
                 u"completo. Si este campo no está marcado se mantendrá la "
                 u"fecha final del contrato."),
    }

    _defaults = {
        'nou_periode_modcon': lambda *a: False
    }

GiscedataSwitching()


class GiscedataSwitchingHelpers(osv.osv):

    _name = 'giscedata.switching.helpers'
    _inherit = 'giscedata.switching.helpers'

    def activar_polissa_from_m1(self, cursor, uid, sw_id, context=None):
        if context is None:
            context = {}

        # Li passem al context que la activació ve des d'ATR
        # i el tipus de renovacio
        sw_obj = self.pool.get("giscedata.switching")
        sw_info = sw_obj.read(cursor, uid, sw_id, ['nou_periode_modcon'],
                              context=context)
        context.update({
            'activacio_from_atr': True,
            'periode_modcon': 'nou' if sw_info['nou_periode_modcon'] else 'actual'
        })

        res = super(GiscedataSwitchingHelpers, self).activar_polissa_from_m1(
                      cursor, uid, sw_id, context=context)

        return res

GiscedataSwitchingHelpers()
