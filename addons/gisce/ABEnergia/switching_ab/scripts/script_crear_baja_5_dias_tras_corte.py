import click

from erppeek import Client

'''Script Comer que crea una baixa per impagament a les polisses que estiguin
en estat de tall des de fa 6 dies'''

@click.command()
@click.option('-uc', '--user_comer', default='admin', help='Usuari servidor ERP COMER')
@click.option('-wc', '--password_comer', default='adminabe', help='Contrasenya usuari ERP COMER')
@click.option('-sc', '--server_comer', default='localhost', help=u'IP servidor ERP COMER')
@click.option('-pc', '--port_comer', default=18069, help='Port servidor ERP COMER', type=click.INT)
@click.option('-dc', '--database_comer', default='abenergia', help='Nom de la base de dades de COMER')
@click.option('-to', '--to_addr', help='Destinatari del mail')
@click.option('-cc', '--to_cc', default=False, help='Destinatari en copia del mail')

def crear_baixa_impagament(**kwargs):

    server = 'http://{0}:{1}'.format(kwargs['server_comer'],
                                           kwargs['port_comer'])
    client = Client(server=server,
                      db=kwargs['database_comer'],
                      user=kwargs['user_comer'],
                      password=kwargs['password_comer'])

    to_addr = kwargs['to_addr']
    cc_addr = kwargs['to_cc']

    crear_baixa_obj = client.model('crear.baixa.administrativa')

    crear_baixa_obj.crear_baixes_impagament(to_addr, cc_addr)

    return True

if __name__ == '__main__':
    crear_baixa_impagament()