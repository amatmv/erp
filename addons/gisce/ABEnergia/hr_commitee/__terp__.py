# -*- coding: utf-8 -*-
{
    "name": "Commitee",
    "description": """
        Comités de dirección
        """,
    "version": "0-dev",
    "author": "abenergia",
    "category": "RRHH",
    "depends":[
        "hr"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/ir.model.access.csv",
        "hr_employee_view.xml",
        "hr_commitee_view.xml"
    ],
    "active": False,
    "installable": True
}
