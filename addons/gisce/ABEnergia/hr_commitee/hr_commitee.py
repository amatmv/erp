# -*- coding: utf-8 -*-

from osv import osv, fields


class hr_commitee(osv.osv):
    _name = 'hr.commitee'
    _description = 'Comité de dirección'
    _columns = {
        'name' : fields.char('Commitee Name', size=30, select=True),
        'employee_ids' : fields.many2many('hr.employee','hr_commitee_employee_rel','hr_commitee','hr_employee','Commitee')
    }

    _defaults = {
        'name': 'asdasd'
    }

    def get_employees(self):
        return self.employee_ids

hr_commitee()

class hr_employee(osv.osv):
    _name = 'hr.employee'
    _inherit = 'hr.employee'
    _columns = {
        'commitee_id': fields.many2many('hr.commitee','hr_commitee_employee_rel','hr_employee', 'hr_commitee','Commitee')
    }

hr_employee()