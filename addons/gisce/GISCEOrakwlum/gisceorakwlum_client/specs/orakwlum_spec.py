# -*- coding: utf-8 -*-
from __future__ import (absolute_import)
import tools
import vcr

config = {
    'protocol': 'http',
    'host': 'localhost',
    'port': '5000',
    'user': "test@gisce.net",
    'password': "test@gisce.net",
}

# Prepare needed ERP config vars
orakwlum_config_prefix = 'orakwlum_'
for parameter in config:
    erp_conf_parameter = orakwlum_config_prefix + parameter
    tools.config[erp_conf_parameter] = config[parameter]

# Workaround to extend current path and be able to instance orakwlum and reach local specs fixtures
import os
os.chdir(os.path.dirname(os.path.abspath(__file__)) + '/..')

from orakwlum_client import orakWlum_Client
from orakwlum import orakWlum

expected = {
    'url': 'http://localhost:5000/api/v1',
}

consumption_to_fetch = {
    "CUPS": 'ES0000000000000000AA',
    "date_start": 1472688000,
    "date_end": 1475280000,
}

consumption_expected = {
    'result': {
        'ES0000000000000000AA': {
            'consumption': 10.0,
            'total': 10.0,
        }
    }
}

fixtures_path = 'specs/fixtures/orakWlum/'

spec_VCR = vcr.VCR(
    record_mode='none',
    cassette_library_dir=fixtures_path
)

with description('A new'):
    with before.each:
        self.config = config
        self.expected = expected

    with context('orakWlum'):
        with context('initialization'):
            with it('must be performed as expected'):
                with spec_VCR.use_cassette('init.yaml'):
                    self.okW_expected = orakWlum_Client(**self.config)
                    self.okW = orakWlum

                    assert self.okW.API.url == self.okW_expected.API.url
                    assert self.okW.API.url == self.expected['url'], "URL must match"
                    assert self.okW.API.token == None, "Token must not be defined by default"

        with context('usage'):
            with before.each:
                with spec_VCR.use_cassette('init.yaml'):
                    self.okW = orakWlum

            with it('must return consumptions as expected'):
                with spec_VCR.use_cassette('consumptions.yaml'):
                    consumption = self.okW.consumptions.by_cups(**consumption_to_fetch)
                    assert consumption == consumption_expected, "Consumption do no match with the expected one. Expected: '{consumption_expected}' vs '{consumption}'".format(consumption_expected=consumption_expected, consumption=consumption)

                    # Assert required params to reach Consumption
                    for param in consumption_to_fetch:
                        tmp_config = dict(consumption_to_fetch)
                        del tmp_config[param]

                        works = True
                        try:
                            consumptions = self.okW.consumptions.by_cups(**tmp_config)
                            print (consumptions)
                        except:
                            works = False
                        assert not works, "okWClient.Consumptions must except if no {param} is provided".format(param=param)
