# -*- coding: utf-8 -*-
from __future__ import (absolute_import)

from orakwlum_client import orakWlum_Client
import tools

# The prefix for orakwlum config vars
orakwlum_config_prefix = 'orakwlum_'

# The prefix for orakwlum config vars
orakwlum_config = {
    'host': None,
    'port': None,
    'protocol': "https",
    'user': None,
    'password': None,
}

# Fetch the config
for parameter in orakwlum_config:
    expected_parameter = orakwlum_config_prefix + parameter
    orakwlum_config[parameter] = tools.config.get(expected_parameter, orakwlum_config[parameter])

# ¿todo? Review / validate config

# Instantiate the orakWlum Client with reached config
orakWlum = orakWlum_Client(**orakwlum_config)
