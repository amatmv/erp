# -*- coding: utf-8 -*-
{
  "name": "orakWlum Client",
  "description": """
    This module provide :
      * Mòdul client de l'orakWlum
    """,
  "version": "0-dev",
  "author": "GISCE",
  "category": "GISCEOrakwlum",
  "depends": ['base'],
  "init_xml": [],
  "demo_xml": [],
  "update_xml": [],
  "active": False,
  "installable": True
}
