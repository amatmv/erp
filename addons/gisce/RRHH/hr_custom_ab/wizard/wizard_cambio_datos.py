# -*- coding: utf-8 -*-

from osv import osv, fields

SUPERUSER_ID = 1


class WizardCambioDatos(osv.osv):
    _name = 'wizard.rrhh.cambio.datos'

    _columns = {
        'notes': fields.text('Notas'),
        'employee': fields.many2one('hr.employee', 'Tabajador')
    }

    def create_crm(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        crm_obj = self.pool.get('crm.lead')
        imd_obj = self.pool.get('ir.model.data')
        section_id = imd_obj.get_object_reference(
            cursor, uid, 'hr_custom_ab', 'section_modify_rrhh'
        )[1]
        wiz = self.browse(cursor, uid, ids[0], context=context)
        crm_id = crm_obj.create(cursor, SUPERUSER_ID, {
            'section_id': section_id,
            'name': 'Petición cambio de datos de %s' % wiz.employee.name,
            'contact_name': wiz.employee.name,
            'email_from': wiz.employee.work_email,
            'description': wiz.notes
        })
        return {}

    _defaults = {
        'employee': lambda *a: a[-1]['active_id']
    }

WizardCambioDatos()