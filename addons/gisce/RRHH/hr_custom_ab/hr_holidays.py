# -*- coding: utf-8 -*-
from osv import osv, fields
from tools import config


class HRHolidays(osv.osv):
    _name = 'hr.holidays'
    _inherit = 'hr.holidays'

    def _cron_send_justify_remind(self, cursor, uid, days):
        if not isinstance(days, (list, tuple)):
            days = [days]
        for n_days in days:
            reminders = self.justify_remind(cursor, uid, n_days)
            if reminders:
                self.send_email(cursor, 1, reminders, 'justify_reminder')

    def justify_remind(self, cursor, uid, days, context=None):
        if not context:
            context = {}
        sql = ('%s/hr_custom_ab/sql/reminder_justify.sql' % config['addons_path'])
        with open(sql) as f:
            cursor.execute(f.read(), ('%s days' % days, ))
        return [x[0] for x in cursor.fetchall()]

    def send_email(self, cursor, uid, ids, template, context=None):
        imd_obj = self.pool.get('ir.model.data')
        et_obj = self.pool.get('poweremail.templates')
        account_obj = self.pool.get('poweremail.core_accounts')
        if not context:
            context = {}
        for holiday in ids:
            res = imd_obj.get_object_reference(
                cursor, uid, 'hr_custom_ab', template
            )
            template = et_obj.browse(cursor, uid, res[1])
            ctx = context.copy()
            if not template.enforce_from_account:
                account_id = account_obj.search(cursor, uid, [
                    ('company', '=', 'yes'),
                    ('state', '=', 'approved')
                ])
                if not account_id:
                    raise osv.except_osv(
                        'Error', 'No email account defined'
                    )
                ctx['account_id'] = account_id[0]
            et_obj.generate_mail(cursor, 1, res[1], [holiday], context=ctx)


    def create(self, cursor, uid, values, context=None):
        if not context:
            context = {}
        if not values.get('name'):
            values['name'] = ' '

        return super(HRHolidays, self).create(cursor, uid, values,
                                              context=context)

    def write(self, cursor, uid, ids, vals, context=None):
        if not context:
            context = {}
        send_document_ids = []
        send_justified = []
        for record in self.browse(cursor, uid, ids):
            if record.number_of_days > 0:
                if record.holiday_status.document_required and vals.get('document_id'):
                    send_document_ids.append(record.id)
                if record.holiday_status.document_required and vals.get('justified'):
                    send_justified.append(record.id)
        self.send_email(cursor, 1, send_document_ids, 'email_document_attached',
                        context=context)
        self.send_email(cursor, 1, send_justified, 'email_justified',
                        context=context)
        res = super(HRHolidays, self).write(cursor, uid, ids, vals, context)
        return res

    def holidays_confirm(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        res = super(HRHolidays, self).holidays_confirm(cursor, uid, ids, context)
        send_ids = []
        for record in self.browse(cursor, uid, ids):
            if record.number_of_days > 0:
                send_ids.append(record.id)
        if send_ids:
            self.send_email(cursor, 1, send_ids, 'email_validacion',
                            context=context)
        return res

    def holidays_validate2(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        res = super(HRHolidays, self).holidays_validate2(cursor, uid, ids, context)
        send_ids = []
        for record in self.browse(cursor, uid, ids):
            if record.number_of_days > 0:
                send_ids.append(record.id)
        if send_ids:
            self.send_email(cursor, 1, send_ids, 'email_confirmacion',
                            context=context)
        return res

    def holidays_refuse(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        res = super(HRHolidays, self).holidays_refuse(cursor, uid, ids, context)
        send_ids = []
        for record in self.browse(cursor, uid, ids):
            if record.number_of_days > 0:
                send_ids.append(record.id)
        if send_ids:
            self.send_email(cursor, 1, send_ids, 'email_refuse',
                            context=context)
        return res

    def holidays_change_proposal(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        res = super(HRHolidays, self).holidays_change_proposal(cursor, uid, ids, context)
        send_ids = []
        for record in self.browse(cursor, uid, ids):
            if record.number_of_days > 0:
                send_ids.append(record.id)
        if send_ids:
            self.send_email(cursor, 1, send_ids, 'email_change_proposal',
                            context=context)
        return res


    def _def_holiday_status(self, cursor, uid, context=None):
        if not context:
            context = {}
        return context.get('holiday_status', False)

    _defaults = {
        'holiday_status': _def_holiday_status,
    }

HRHolidays()


class HRHolidaysStatus(osv.osv):
    _name = 'hr.holidays.status'
    _inherit = 'hr.holidays.status'

    def _ab_user_left_days(self, cursor, uid, ids, field_name, args,
                           context=None):
        if not context:
            context = {}
        res = dict([
            (x, {'requested': 0, 'available_to_request': 0}) for x in ids
        ])
        employee_ids = self.pool.get('hr.employee').search(cursor, uid, [
            ('user_id', '=', uid)
        ], context=context)
        if employee_ids:
            employee_id = employee_ids[0]
            days = self.get_days(cursor, uid, ids, employee_id, False, context)
            sql = ('%s/hr_custom_ab/sql/ab_user_left_days.sql'
                    % config['addons_path'])
            with open(sql) as f:
                cursor.execute(f.read(), (employee_id, ))
                sql_res = dict([
                    (x[1], {'requested': x[0]})for x in cursor.fetchall()
                ])
                res.update(sql_res)

            for id in ids:
                remaining = days.get(id, {}).get('remaining_leaves', 0)
                res[id]['available_to_request'] = (
                     remaining - res[id]['requested']
                )
        return res


    _columns = {
        'requested': fields.function(
            _ab_user_left_days,
            string='Remaining Leaves',
            multi='ab_user_left_days',
            type='integer',
            method=True
        ),
        'available_to_request': fields.function(
            _ab_user_left_days,
            string='Remaining Leaves',
            multi='ab_user_left_days',
            type='integer',
            method=True
        ),
    }

HRHolidaysStatus()