# -*- coding: utf-8 -*-
import base64
from datetime import datetime, timedelta

from destral import testing
from destral.transaction import Transaction
from expects import *


class HRCustomAbTests(testing.OOTestCase):

    def test_recordatori_justificants(self):
        account_obj = self.openerp.pool.get('poweremail.core_accounts')
        mail_obj = self.openerp.pool.get('poweremail.mailbox')
        imd = self.openerp.pool.get('ir.model.data')
        emp_obj = self.openerp.pool.get('hr.employee')
        hol_obj = self.openerp.pool.get('hr.holidays')
        hol_status_obj = self.openerp.pool.get('hr.holidays.status')
        per_user_obj = self.openerp.pool.get('hr.holidays.per.user')
        with Transaction().start(self.database) as txn:
            # Create a default company email
            account_id = account_obj.create(txn.cursor, txn.user, {
                'email_id': 'test@example.com',
                'company': 'yes',
                'smtpport': 0,
                'smtpserver': 'test'
            })
            account_obj.do_approval(txn.cursor, txn.user, [account_id])

            employee_id = emp_obj.create(txn.cursor, txn.user, {
                'name': 'Test employee',
                'work_email': 'employee@example.com'
            })
            status_id = hol_status_obj.search(
                txn.cursor, txn.user, [('name', '=', 'Visita Médica Propia')])
            # Check not empty
            expect(status_id).not_to(be_empty)
            # Check len == 1
            expect(status_id).to(have_len(1))

            #Create Holidays per user for Visita Médica Propia
            per_user_obj.create(txn.cursor, txn.user, {
                'employee_id': employee_id,
                'holiday_status': status_id[0],
                'max_leaves': 0
            })

            from_date = datetime.now() - timedelta(days=1)
            to_date = from_date + timedelta(days=3)
            hol_id = hol_obj.create(txn.cursor, txn.user, {
                'holiday_status': status_id[0],
                'date_from': from_date.strftime('%Y-%m-%d'),
                'date_to': to_date.strftime('%Y-%m-%d'),
                'number_of_days': 2,
                'document_required': True,
                'employee_id': employee_id
            })

            hol_obj._cron_send_justify_remind(txn.cursor, txn.user, (1, 5))
            # Check email content
            mail_id = mail_obj.search(txn.cursor, txn.user, [])
            expect(mail_id).to(equal([1]))

            mail = mail_obj.read(txn.cursor, txn.user, mail_id)
            expect(mail[0]).to(have_keys(
                pem_subject=u"Recordatorio justificación en la ausencia del tipo Visita Médica Propia",
                pem_to=u"employee@example.com",
                folder=u"outbox"
            ))