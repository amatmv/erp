# -*- coding: utf-8 -*-
{
    "name": "hr_custom_ab",
    "description": """
        Ocultar campos no requeridos
        """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "RRHH",
    "depends":[
        "board",
        "hr",
        "hr_holidays",
        "hr_holidays_backport",
        "hr_cups",
        "hr_office",
        "hr_split_name",
        "hr_holidays_availability",
        "hr_holidays_approve_directly",
        "hr_employee_job",
        "hr_employee_photo",
        "hr_holidays_office",
        "hr_holidays_require_document",
        "hr_holidays_response_time",
        "hr_holidays_status_group",
        "hr_holidays_terms",
        "hr_documents",
        "crm"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/hr_custom_ab_security.xml",
        "security/ir.model.access.csv",
        "wizard/wizard_cambio_datos_view.xml",
        "hr_view.xml",
        "hr_holidays_data.xml",
        "email_template_data.xml",
        "hr_holidays_view.xml",
        "portal_data.xml",
        "hr_custom_ab_data.xml",
        "hr_custom_ab_view.xml",
        "hr_custom_ab_cron.xml",
        "hr_office.xml"
    ],
    "active": False,
    "installable": True
}
