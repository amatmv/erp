SELECT
  -sum(number_of_days) as days,
  holiday_status as id
FROM
  hr_holidays
WHERE
   employee_id = %s
   AND number_of_days > 0
   AND state in ('confirm', 'validate1', 'dealing', 'change_proposal')
GROUP BY holiday_status_id