SELECT
  h.id
FROM
  hr_holidays h LEFT JOIN hr_holidays_status hs ON h.holiday_status = hs.id
WHERE h.number_of_days > 0 AND
      hs.document_required = TRUE AND h.justified = FALSE AND
      to_char(h.date_from + INTERVAL %s, 'yyyy-mm-dd') =
      to_char(now(), 'yyyy-mm-dd');