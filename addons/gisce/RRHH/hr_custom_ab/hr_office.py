# -*- coding: utf-8 -*-
from osv import osv, fields


class HROffice_address(osv.osv):
    _name = 'hr.office'
    _inherit= 'hr.office'

    _columns = {
        'office_address': fields.char('Dirección oficina', size=256),
    }

HROffice_address()