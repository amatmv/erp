# -*- coding: utf-8 -*-
from osv import osv, fields


class HREmployee(osv.osv):
    _name = 'hr.employee'
    _inherit = 'hr.employee'

    _columns = {
        'show_birthday': fields.boolean('Mostrar aniversario en los eventos de '
                                        'la intranet'),
        'mobile_phone': fields.char('Work Mobile', size=32, readonly=False),
        'photo': fields.binary('Photo'),
    }

    _defaults = {
        'show_birthday': lambda *a: 0
    }


HREmployee()