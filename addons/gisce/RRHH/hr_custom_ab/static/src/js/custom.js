function custom_ab() {
    //$('.oe_form_buttons').hide();
    $('.oe_vm_switch_page').hide();
    $('.oe_form_pager').hide();
    $('.toggle-sidebar').click();
    $('#holiday_title').html($('.oe_view_title_text').html());
}

function dias_vacaciones() {
    setTimeout(function() {
        var dias_disponibles = parseInt($('[name=available_to_request]')[0].value);
        $('#dias_disponibles').html('<h1>Quedan ' + dias_disponibles + ' días disponibles</h1>');
    }, 500);
}