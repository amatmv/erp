# -*- coding: utf-8 -*-
from osv import osv, fields


class hr_job(osv.osv):

    _name = "hr.job"
    _description = "Job Description"
    _columns = {
        'name': fields.char('Job Name', size=128, required=True, select=True),
        'employee_ids': fields.one2many('hr.employee', 'job_id', 'Employees'),
        'description': fields.text('Job Description')
    }

hr_job()


class hr_employee(osv.osv):
    _name = 'hr.employee'
    _inherit = 'hr.employee'

    _columns = {
        'job_id': fields.many2one('hr.job', 'Job'),
    }

hr_employee()