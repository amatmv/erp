# -*- coding: utf-8 -*-
{
    "name": "Add job to employee",
    "description": """

        """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "RRHH",
    "depends":[
        "hr"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/ir.model.access.csv",
        "hr_job_view.xml",
        "hr_employee_view.xml"
    ],
    "active": False,
    "installable": True
}
