# -*- coding: utf-8 -*-
from osv import osv, fields

NEW_STATES = [
    ('dealing', 'Dealing'),
    ('change_proposal', 'Change proposal')
]


class HRHolidays(osv.osv):
    _name = 'hr.holidays'
    _inherit = 'hr.holidays'

    def __init__(self, pool, cursor):
        for state in NEW_STATES:
            if state not in self._columns['state'].selection:
                self._columns['state'].selection.insert(2, state)
        field_states = {
            'dealing': [('readonly', False)],
            'change_proposal': [('readonly', False)]
        }
        for key, col in self._columns.items():
            if col.states and col.states.get('draft') == [('readonly', False)]:
                col.states.update(field_states)
        super(HRHolidays, self).__init__(pool, cursor)

    def holidays_confirm(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        res = super(HRHolidays, self).holidays_confirm(cursor, uid, ids,
                                                       context=context)
        # Write original values.
        for holiday in self.browse(cursor, uid, ids, context=context):
            holiday.write({
                'date_from_orig': holiday.date_from,
                'date_to_orig': holiday.date_to,
                'number_of_days_orig': abs(holiday.number_of_days)
            })
        return res

    def holidays_dealing(self, cursor, uid, ids, context=None):
        self.write(cursor, uid, ids, {'state': 'dealing'}, context=context)
        return True

    def holidays_change_proposal(self, cursor, uid, ids, context=None):
        self.check_holidays(cursor, uid, ids, context=context)
        self.write(cursor, uid, ids, {'state': 'change_proposal'},
                   context=context)
        return True

    _columns = {
        'date_from_orig': fields.datetime('Original start date', readonly=True),
        'date_to_orig': fields.datetime('Original end date', readonly=True),
        'number_of_days_orig': fields.integer('Original number of days',
                                              readonly=True),
    }

HRHolidays()