# -*- coding: utf-8 -*-
{
    "name": "Dealing holidays dates",
    "description": """
        T4.3 Modificación workflow vacaciones para devolver las vacaciones no aprovadas
        """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "RRHH",
    "depends": [],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "hr_holidays_view.xml",
        "hr_holidays_workflow.xml"
    ],
    "active": False,
    "installable": True
}
