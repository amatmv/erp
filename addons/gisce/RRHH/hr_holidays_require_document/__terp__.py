# -*- coding: utf-8 -*-
{
    "name": "Holidays require document",
    "description": """
        Require a document to justify some leave types.
        """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "RRHH",
    "depends":[
        "base",
        "hr_holidays"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "hr_holidays_view.xml"
    ],
    "active": False,
    "installable": True
}
