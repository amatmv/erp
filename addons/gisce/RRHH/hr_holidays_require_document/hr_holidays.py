# -*- coding: utf-8 -*-
from osv import osv, fields
from slugify import slugify


class HRHolidays(osv.osv):
    _name = 'hr.holidays'
    _inherit = 'hr.holidays'

    def _document_data_inv(self, cursor, uid, ids, name, value, arg,
                           context=None):
        if not context:
            context = {}
        attach_obj = self.pool.get('ir.attachment')
        holiday = self.browse(cursor, uid, ids, context=context)
        if holiday.date_from:
            date = holiday.date_from[:10].replace('-', '')
        else:
            date = ''
        status = holiday.holiday_status.name
        employee = holiday.employee_id.name

        if not value:
            if holiday.document_id:
                attach_obj.unlink(cursor, uid, [holiday.document_id.id])
            return

        # Try to guess the extension
        try:
            import magic
            import base64
            content_type = magic.from_buffer(base64.b64decode(value), mime=True)
            extension = content_type.split('/')[1][:3]
        except:
            extension = ''

        name = slugify('-'.join([date, status, employee, holiday.name]))

        if extension:
            name = '%s.%s' % (name, extension)

        vals = {
            'name': name,
            'datas_fname': name,
            'datas': value,
            'description': holiday.notes,
            'res_model': self._name,
            'res_id': holiday.id,
            'type': 'binary',
        }
        if holiday.document_id:
            attach_obj.write(cursor, uid, [holiday.document_id.id], vals)
        else:
            attach_id = attach_obj.create(cursor, uid, vals)
            holiday.write({'document_id': attach_id})

    def _document_data(self, cursor, uid, ids, field_name, args, context=None):
        if not context:
            context = {}
        res = dict.fromkeys(ids, False)
        for holiday in self.browse(cursor, uid, ids, context=context):
            if holiday.document_id:
                res[holiday.id] = holiday.document_id.datas
        return res

    def _document_required(self, cursor, uid, ids, field_name, args,
                                        context=None):
        if not context:
            context = {}
        res = dict.fromkeys(ids, 0)
        for holiday in self.browse(cursor, uid, ids, context=context):
            if holiday.holiday_status:
                res[holiday.id] = holiday.holiday_status.document_required
        return res

    def on_change_holidays_status_id(self, cursor, uid, ids, holidays_status_id,
                                     context=None):
        if not context:
            context = {}
        res = {'value': {}}
        status_obj = self.pool.get('hr.holidays.status')
        parent = super(HRHolidays, self)
        if hasattr(parent, 'on_change_holidays_status_id'):
            parent = parent.on_change_holidays_status_id
            res = parent(cursor, uid, ids, holidays_status_id, context=context)
        if not holidays_status_id:
            return res
        status = status_obj.browse(cursor, uid, holidays_status_id)
        res['value'].update({'document_required': status.document_required})
        return res

    _columns = {
        'document_required': fields.function(
            _document_required,
            type='boolean',
            string='Document required',
            method=True
        ),
        'document_data': fields.function(
            _document_data,
            fnct_inv=_document_data_inv,
            type='binary',
            string='Document',
            method=True
        ),
        'document_filename': fields.related(
            'document_id',
            'datas_fname',
            type='char',
            size=256
        ),
        'document_id': fields.many2one('ir.attachment', 'Document (id)'),
        'justified': fields.boolean('Justificado')
    }

    _defaults = {
        'justified': lambda *a: 0,
    }

HRHolidays()


class HRHolidaysStatus(osv.osv):
    _name = 'hr.holidays.status'
    _inherit = 'hr.holidays.status'

    _columns = {
        'document_required': fields.boolean('Document required')
    }

    _defaults = {
        'document_required': lambda *a: 0
    }

HRHolidaysStatus()