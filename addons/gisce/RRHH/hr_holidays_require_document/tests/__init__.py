# -*- coding: utf-8 -*-
import base64
from datetime import datetime

from destral import testing
from destral.transaction import Transaction
from expects import *

class HrHolidaysRequireDocumentTests(testing.OOTestCase):

    def test_document_required(self):
        holidays_obj = self.openerp.pool.get('hr.holidays')
        status_obj = self.openerp.pool.get('hr.holidays.status')
        with Transaction().start(self.database) as txn:
            status_id = status_obj.create(txn.cursor, txn.user, {
                'name': 'Status name',
                'document_required': True,
            })
            holidays_id = holidays_obj.create(txn.cursor, txn.user, {
                'name': 'Xmas holidays',
                'date_from': '2015-01-01',
                'date_to': '2015-01-07',
                'holiday_status': status_id,
                'number_of_days': 7
            })

            holidays = holidays_obj.read(txn.cursor, txn.user, holidays_id)
            expect(holidays).to(have_keys(
                document_required=True
            ))
