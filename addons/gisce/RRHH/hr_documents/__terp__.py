# -*- coding: utf-8 -*-
{
    "name": "Documents for HR Employee",
    "description": """
        Añadir adjuntos en los empleados
        """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "RRHH",
    "depends":[
        "hr",
        "poweremail"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "hr_view.xml",
        "hr_documents_data.xml",
        "hr_documents_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
