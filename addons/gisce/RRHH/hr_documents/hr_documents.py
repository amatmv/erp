# -*- coding: utf-8 -*-
from osv import osv, fields
from slugify import slugify


class HRDocumentsType(osv.osv):
    _name = 'hr.documents.type'

    _rec_name = 'code'

    _columns = {
        'code': fields.char('Code', size=8),
        'description': fields.char('Description', size=256),
        'notify': fields.boolean('Notify on create')
    }

    _defaults = {
        'notify': lambda *a: 1,
    }

HRDocumentsType()


class HRDocuments(osv.osv):
    _name = 'hr.documents'

    def create(self, cursor, uid, vals, context=None):
        imd_obj = self.pool.get('ir.model.data')
        et_obj = self.pool.get('poweremail.templates')
        account_obj = self.pool.get('poweremail.core_accounts')
        if not context:
            context = {}
        res = super(HRDocuments, self).create(cursor, uid, vals,
                                              context=context)
        doc = self.browse(cursor, uid, res, context=context)
        if doc.type.notify:
            user_id = doc.employee.user_id
            if (user_id and user_id.id != uid) or not user_id:
                res = imd_obj.get_object_reference(
                    cursor, uid, 'hr_documents', 'email_template_new_document'
                )
                template = et_obj.browse(cursor, uid, res[1])
                ctx = context.copy()
                if not template.enforce_from_account:
                    account_id = account_obj.search(cursor, uid, [
                        ('company', '=', 'yes'),
                        ('state', '=', 'approved')
                    ])
                    if not account_id:
                        raise osv.except_osv(
                            'Error', 'No email account defined'
                        )
                    ctx['account_id'] = account_id[0]
                et_obj.generate_mail(cursor, 1, res[1], [doc.id],
                                     context=ctx)
        return res

    def _document_data_inv(self, cursor, uid, ids, name, value, arg,
                           context=None):
        if not context:
            context = {}
        attach_obj = self.pool.get('ir.attachment')
        document = self.browse(cursor, uid, ids, context=context)
        code = document.type.code
        filename, extension = document.document_filename.rsplit('.', 1)
        name = '%s.%s' % (slugify('-'.join([code, filename])), extension)

        vals = {
            'name': name,
            'datas_fname': name,
            'datas': value,
            'description': document.notes,
            'res_model': 'hr.employee',
            'res_id': document.employee.id,
            'type': 'binary',
        }
        if document.document_id:
            attach_obj.write(cursor, uid, [document.document_id.id], vals)
        else:
            attach_id = attach_obj.create(cursor, uid, vals)
            document.write({'document_id': attach_id})
        self.write(cursor, uid, ids, {'document_filename': name})

    def _document_data(self, cursor, uid, ids, field_name, args, context=None):
        if not context:
            context = {}
        res = dict.fromkeys(ids, False)
        for document in self.browse(cursor, uid, ids, context=context):
            if document.document_id:
                res[document.id] = document.document_id.datas
        return res

    _columns = {
        'create_date': fields.datetime('Create date'),
        'create_uid': fields.many2one('res.users'),
        'type': fields.many2one(
            'hr.documents.type',
            'Type',
            required=True,
            ondelete='restrict'
        ),
        'notes': fields.text('Notes'),
        'employee': fields.many2one(
            'hr.employee',
            'Employee',
            required=True,
            ondelete='cascade'
        ),
        'document_data': fields.function(
            _document_data,
            fnct_inv=_document_data_inv,
            type='binary',
            string='Document',
        ),
        'document_filename': fields.char('Filename', size=256),
        'document_id': fields.many2one(
            'ir.attachment',
            'Document (id)',
            ondelete='cascade'
        )
    }

    _order = 'create_date desc'

HRDocuments()