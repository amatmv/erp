import base64

from destral import testing
from destral.transaction import Transaction
from expects import *


class HRDocumentsTests(testing.OOTestCase):

    def test_email_create(self):
        account_obj = self.openerp.pool.get('poweremail.core_accounts')
        mail_obj = self.openerp.pool.get('poweremail.mailbox')
        doc_type_obj = self.openerp.pool.get('hr.documents.type')
        doc_obj = self.openerp.pool.get('hr.documents')
        emp_obj = self.openerp.pool.get('hr.employee')
        with Transaction().start(self.database) as txn:
            # Create a default company email
            account_id = account_obj.create(txn.cursor, txn.user, {
                'email_id': 'test@example.com',
                'company': 'yes',
                'smtpport': 0,
                'smtpserver': 'test'
            })
            account_obj.do_approval(txn.cursor, txn.user, [account_id])

            tid = doc_type_obj.create(txn.cursor, txn.user, {
                'code': 'TEST',
                'description': 'Testing document type',
                'notify': True
            })
            employee_id = emp_obj.create(txn.cursor, txn.user, {
                'name': 'Test employee',
                'work_email': 'employee@example.com'
            })
            doc_obj.create(txn.cursor, txn.user, {
                'type': tid,
                'employee': employee_id,
                'document_data': base64.b64encode('Test content'),
                'document_filename': 'test.txt'
            })
            # Check email content
            mail_id = mail_obj.search(txn.cursor, txn.user, [])
            expect(mail_id).to(equal([1]))

            mail = mail_obj.read(txn.cursor, txn.user, mail_id)
            expect(mail[0]).to(have_keys(
                pem_subject=u"You have a new TEST document",
                pem_to=u"employee@example.com",
                folder=u"outbox"
            ))