# -*- coding: utf-8 -*-

from osv import osv, fields


class HREmployee(osv.osv):
    _name = 'hr.employee'
    _inherit = 'hr.employee'

    _columns = {
        'documents': fields.one2many(
            'hr.documents',
            'employee', 'Documents')
    }

HREmployee()