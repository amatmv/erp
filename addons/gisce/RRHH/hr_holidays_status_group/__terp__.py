# -*- coding: utf-8 -*-
{
    "name": "Holidays status group",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Group in status holidays
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "hr_holidays"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "hr_holidays_view.xml"
    ],
    "active": False,
    "installable": True
}
