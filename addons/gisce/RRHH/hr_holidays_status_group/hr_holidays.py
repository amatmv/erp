from osv import osv, fields


class HRHolidaysStatusGroup(osv.osv):
    _name = 'hr.holidays.status.group'

    _columns = {
        'name': fields.char('Name', size=256)
    }

HRHolidaysStatusGroup()


class HRHolidaysStatus(osv.osv):
    _name = 'hr.holidays.status'
    _inherit = 'hr.holidays.status'

    _columns = {
        'group_id': fields.many2one(
            'hr.holidays.status.group',
            'Group',
            ondelete='restrict',
        )
    }

HRHolidaysStatus()
