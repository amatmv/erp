# -*- coding: utf-8 -*-
{
    "name": "Holidays (available or not)",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Add available or not to holidays_status and holidays
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "hr_holidays"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "hr_holidays_view.xml"
    ],
    "active": False,
    "installable": True
}
