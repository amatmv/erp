from osv import osv, fields


class HRHolidaysStatus(osv.osv):
    _name = 'hr.holidays.status'
    _inherit = 'hr.holidays.status'

    _columns = {
        'available': fields.boolean(
            'Available', help="Set default status availability"
        )
    }

    _defaults = {
        'available': lambda *a: 0
    }

HRHolidaysStatus()


class HRHolidays(osv.osv):
    _name = 'hr.holidays'
    _inherit = 'hr.holidays'

    def on_change_holidays_status_id(self, cursor, uid, ids, holidays_status_id,
                                     context=None):
        if not context:
            context = {}
        res = {'value': {}}
        status_obj = self.pool.get('hr.holidays.status')
        parent = super(HRHolidays, self)
        if hasattr(parent, 'on_change_holidays_status_id'):
            parent = parent.on_change_holidays_status_id
            res = parent(cursor, uid, ids, holidays_status_id, context=context)
        if not holidays_status_id:
            return res
        status = status_obj.browse(cursor, uid, holidays_status_id)
        res['value'].update({'available': status.available})
        return res

    _columns = {
        'available': fields.boolean(
            'Available'
        )
    }

    _defaults = {
        'available': lambda *a: 0
    }

HRHolidays()