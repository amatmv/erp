from osv import orm
from destral import testing
from destral.transaction import Transaction
from expects import *


class HRHolidaysYearTests(testing.OOTestCase):

    def test_constraint(self):
        holidays_user_obj = self.openerp.pool.get('hr.holidays.per.user')
        employee_obj = self.openerp.pool.get('hr.employee')
        status_obj = self.openerp.pool.get('hr.holidays.status')
        with Transaction().start(self.database) as txn:
            status_id = status_obj.create(txn.cursor, txn.user, {
                'name': 'Test status',
            })
            employee_id = employee_obj.create(txn.cursor, txn.user, {
                'name': 'Employee test',
                'user_id': txn.user,
            })
            holidays_user_obj.create(txn.cursor, txn.user, {
                'employee_id': employee_id,
                'user_id': txn.user,
                'holiday_status': status_id,
                'max_leaves': 0,
                'leaves_taken': 0,
                'year': 2014
            })

            def callback():
                holidays_user_obj.create(txn.cursor, txn.user, {
                    'employee_id': employee_id,
                    'user_id': txn.user,
                    'holiday_status': status_id,
                    'max_leaves': 0,
                    'leaves_taken': 0,
                    'year': 2015
                })

            expect(callback).to(raise_error(orm.except_orm, 'ValidateError'))

    def test_pass_constraint(self):
        holidays_user_obj = self.openerp.pool.get('hr.holidays.per.user')
        employee_obj = self.openerp.pool.get('hr.employee')
        status_obj = self.openerp.pool.get('hr.holidays.status')
        with Transaction().start(self.database) as txn:
            status_id = status_obj.create(txn.cursor, txn.user, {
                'name': 'Test status',
            })
            employee_id = employee_obj.create(txn.cursor, txn.user, {
                'name': 'Employee test',
                'user_id': txn.user,
            })
            holidays_user_obj.create(txn.cursor, txn.user, {
                'employee_id': employee_id,
                'user_id': txn.user,
                'holiday_status': status_id,
                'max_leaves': 0,
                'leaves_taken': 0,
                'year': 2014,
                'active': 0
            })

            holidays_user_obj.create(txn.cursor, txn.user, {
                'employee_id': employee_id,
                'user_id': txn.user,
                'holiday_status': status_id,
                'max_leaves': 0,
                'leaves_taken': 0,
                'year': 2015
            })

            ids = holidays_user_obj.search(txn.cursor, txn.user, [])
            expect(ids).to(have_len(1))

            hpu = holidays_user_obj.read(txn.cursor, txn.user, ids[0])
            expect(hpu).to(have_key("year", 2015))

    def test_assign_year(self):
        assign_obj = self.openerp.pool.get('wizard.hr.holidays.assign.days')
        status_obj = self.openerp.pool.get('hr.holidays.status')
        employee_obj = self.openerp.pool.get('hr.employee')
        holidays_user_obj = self.openerp.pool.get('hr.holidays.per.user')
        with Transaction().start(self.database) as txn:
            status_id = status_obj.create(txn.cursor, txn.user, {
                'name': 'Test status',
                'max_response_time': 5
            })
            employee_id = employee_obj.create(txn.cursor, txn.user, {
                'name': 'Employee test',
                'user_id': txn.user,
            })
            assign = assign_obj.create(txn.cursor, txn.user, {
                'name': 'Assignement without fixed dates',
                'assign_type': 'employee',
                'employee_id': employee_id,
                'holiday_status_id': status_id,
                'year': 2015,
                'number_of_days': 20
            })
            assign_obj.assign(txn.cursor, txn.user, assign)
            ids = holidays_user_obj.search(txn.cursor, txn.user, [
                ('employee_id.id', '=', employee_id),
                ('holiday_status.id', '=', status_id)
            ])
            per_user = holidays_user_obj.read(txn.cursor, txn.user, ids[0])
            expect(per_user).to(have_key("year", 2015))

    def test_request_dates(self):
        assign_obj = self.openerp.pool.get('wizard.hr.holidays.assign.days')
        status_obj = self.openerp.pool.get('hr.holidays.status')
        employee_obj = self.openerp.pool.get('hr.employee')
        holidays_obj = self.openerp.pool.get('hr.holidays')
        with Transaction().start(self.database) as txn:
            status_id = status_obj.create(txn.cursor, txn.user, {
                'name': 'Test status',
                'max_response_time': 5
            })
            employee_id = employee_obj.create(txn.cursor, txn.user, {
                'name': 'Employee test',
                'user_id': txn.user,
            })
            assign = assign_obj.create(txn.cursor, txn.user, {
                'name': 'Assignement without fixed dates',
                'assign_type': 'employee',
                'employee_id': employee_id,
                'holiday_status_id': status_id,
                'year': 2015,
                'number_of_days': 20
            })
            assign_obj.assign(txn.cursor, txn.user, assign)

            def callback():
                holidays_obj.create(txn.cursor, txn.user, {
                    'holiday_status': status_id,
                    'employee_id': employee_id,
                    'name': 'Test',
                    'date_from': '2014-12-29',
                    'date_to': '2015-01-06',
                    'number_of_days': 8
                })

            expect(callback).to(raise_error(orm.except_orm, 'ValidateError'))

    def test_disable_year(self):
        assign_obj = self.openerp.pool.get('wizard.hr.holidays.assign.days')
        status_obj = self.openerp.pool.get('hr.holidays.status')
        employee_obj = self.openerp.pool.get('hr.employee')
        per_user_obj = self.openerp.pool.get('hr.holidays.per.user')
        wiz_deact = self.openerp.pool.get('wizard.hr.holidays.disable.year')
        with Transaction().start(self.database) as txn:
            status_id = status_obj.create(txn.cursor, txn.user, {
                'name': 'Test status',
                'max_response_time': 5
            })
            employee_id = employee_obj.create(txn.cursor, txn.user, {
                'name': 'Employee test',
                'user_id': txn.user,
            })
            employee_id2 = employee_obj.create(txn.cursor, txn.user, {
                'name': 'Employee test 2',
                'user_id': txn.user,
            })
            assign = assign_obj.create(txn.cursor, txn.user, {
                'name': 'Assignement without fixed dates',
                'assign_type': 'employee',
                'employee_id': employee_id,
                'holiday_status_id': status_id,
                'year': 2015,
                'number_of_days': 20
            })
            assign_obj.assign(txn.cursor, txn.user, assign)

            assign = assign_obj.create(txn.cursor, txn.user, {
                'name': 'Assignement without fixed dates',
                'assign_type': 'employee',
                'employee_id': employee_id2,
                'holiday_status_id': status_id,
                'year': 2015,
                'number_of_days': 20
            })
            assign_obj.assign(txn.cursor, txn.user, assign)

            ids = per_user_obj.search(txn.cursor, txn.user, [])
            expect(ids).to(have_len(2))

            wiz_fields = wiz_deact.fields_get(txn.cursor, txn.user, [])
            expect(wiz_fields['year']['selection']).to(equal([(2015, '2015')]))

            wiz_id = wiz_deact.create(txn.cursor, txn.user, {
                'year': 2015
            })
            wiz_deact.deactivate(txn.cursor, txn.user, wiz_id)

            ids = per_user_obj.search(txn.cursor, txn.user, [])
            expect(ids).to(be_empty)
