from osv import osv, fields


class HRHolidaysPeruser(osv.osv):
    _name = 'hr.holidays.per.user'
    _inherit = 'hr.holidays.per.user'

    _columns = {
        'year': fields.integer('Year')
    }

    def _cnt_year_status_employee(self, cursor, uid, ids):
        for record in self.read(cursor, uid, ids):
            count = self.search_count(cursor, uid, [
                ('employee_id.id', '=', record['employee_id'][0]),
                ('holiday_status.id', '=', record['holiday_status'][0])
            ])
            if count > 1:
                return False
        return True

    _constraints = [
        (_cnt_year_status_employee, "Already exists an active status for year",
         ["year"])
    ]

HRHolidaysPeruser()


class HRHolidays(osv.osv):
    _name = 'hr.holidays'
    _inherit = 'hr.holidays'

    def _cnt_same_year(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        for holiday in self.browse(cursor, uid, ids):
            year_from = holiday.date_from.split('-')[0]
            year_to = holiday.date_to.split('-')[0]
            if year_from != year_to:
                return False
        return True

    def _create_holiday(self, cursor, uid, ids):
        res = super(HRHolidays, self)._create_holiday(cursor, uid, ids)
        for holiday in self.browse(cursor, uid, ids):
            per_user = holiday.holiday_user_id
            if per_user and not per_user.year:
                year = int(holiday.date_from.split('-')[0])
                per_user.write({'year': year})
        return res

    _constraints = [
        (_cnt_same_year, "Date from and date to must be in the same year",
         ['date_from', 'date_to'])
    ]

HRHolidays()
