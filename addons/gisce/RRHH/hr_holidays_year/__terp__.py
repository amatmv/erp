# -*- coding: utf-8 -*-
{
    "name": "Holidays per year",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
      * Holidays per year not accumulated
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "hr_holidays_backport"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "hr_holidays_view.xml",
        "wizard/wizard_assign_days_view.xml",
        "wizard/wizard_disable_year_view.xml"
    ],
    "active": False,
    "installable": True
}
