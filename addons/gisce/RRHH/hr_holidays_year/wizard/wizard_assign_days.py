from datetime import datetime

from osv import osv, fields


class WizardHRHolidaysAssignDays(osv.osv_memory):
    _name = 'wizard.hr.holidays.assign.days'
    _inherit = 'wizard.hr.holidays.assign.days'

    _columns = {
        'year': fields.integer('Year'),
    }

    def assign(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        if isinstance(ids, (list, tuple)):
            ids = ids[0]
        wiz = self.browse(cursor, uid, ids)
        if not wiz.fixed_dates:
            wiz.write({'date_from': '{}-01-01'.format(wiz.year)})
        return super(WizardHRHolidaysAssignDays, self).assign(
            cursor, uid, ids, context=context
        )

    _defaults = {
        'year': lambda *a: datetime.now().year
    }

WizardHRHolidaysAssignDays()
