from osv import osv, fields
from tools.translate import _


class WizardHrHolidaysDisableYear(osv.osv_memory):
    _name = 'wizard.hr.holidays.disable.year'

    def _years(self, cursor, uid, context=None):
        if context is None:
            context = {}
        cursor.execute("""
            SELECT DISTINCT year FROM hr_holidays_per_user WHERE active is TRUE
        """)
        res = [(x[0], str(x[0])) for x in cursor.fetchall() if x[0] or False]
        return res

    def deactivate(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        if isinstance(ids, (list, tuple)):
            ids = ids[0]
        per_user_obj = self.pool.get('hr.holidays.per.user')
        wiz = self.browse(cursor, uid, ids, context=context)
        pu_ids = per_user_obj.search(cursor, uid, [
            ('year', '=', wiz.year)
        ])
        per_user_obj.write(cursor, uid, pu_ids, {'active': 0})
        wiz.write({
            'state': 'done',
            'status': _("Deactivation of year %s success") % wiz.year
        })
        return ids

    _columns = {
        'year': fields.selection(_years, 'Year', required=True),
        'state': fields.char('State', size=50),
        'status': fields.text('Status')
    }

    _defaults = {
        'state': lambda *a: 'init',
        'status': lambda *a: '',
    }

WizardHrHolidaysDisableYear()
