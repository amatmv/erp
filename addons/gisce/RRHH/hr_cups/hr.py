# -*- coding: utf-8 -*-

from osv import osv, fields


class HREmployee(osv.osv):
    _name = 'hr.employee'
    _inherit = 'hr.employee'

    _columns = {
        'cups': fields.many2one('giscedata.cups.ps', 'CUPS')
    }

HREmployee()