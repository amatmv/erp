# -*- coding: utf-8 -*-
{
    "name": "hr_cups",
    "description": """
        T2.2 Añadir campo CUPS en el empleado
        """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "RRHH",
    "depends":[
        "hr",
        "giscedata_cups"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "hr_view.xml"
    ],
    "active": False,
    "installable": True
}
