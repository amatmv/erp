# -*- coding: utf-8 -*-
from osv import osv, fields


class HROffice(osv.osv):
    _name = 'hr.office'

    _columns = {
        'name': fields.char('Name', size=256),
    }

HROffice()

class HREmployee(osv.osv):
    _name = 'hr.employee'
    _inherit = 'hr.employee'

    _columns = {
        'work_location': fields.related(
            'work_office',
            'name',
            string='Office Location',
            type='char',
            size=32
        ),
        'work_office': fields.many2one('hr.office', 'Office')
    }

HREmployee()