# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import  Transaction
from expects import *

class HREmployeeOfficeTests(testing.OOTestCase):

    def test_add_office_to_employee(self):
        hr_emp_obj = self.openerp.pool.get('hr.employee')
        hr_office_obj = self.openerp.pool.get('hr.office')
        with Transaction().start(self.database) as txn:
            #create new office
            office_name = 'new office'
            office_id = hr_office_obj.create(txn.cursor, txn.user,{
                'name': office_name
            })
            #create new employee associate to txn.user and new office
            empl_id = hr_emp_obj.create(txn.cursor, txn.user, {
                'name': 'new employee',
                'user_id': txn.user,
                'work_office': office_id
            })

            #Get info for new employee

            empl_data = hr_emp_obj.read(txn.cursor, txn.user, empl_id,
                                        ['work_office', 'work_location'])
            expect(empl_data['work_office']).to_not(be_empty)
            expect(empl_data['work_office'][0]).to(equal(office_id))
            expect(empl_data['work_location']).to_not(be_empty)
            expect(empl_data['work_location']).to(equal(office_name))