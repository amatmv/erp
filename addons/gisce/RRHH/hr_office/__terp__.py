# -*- coding: utf-8 -*-
{
    "name": "hr_office",
    "description": """
        Añadir campo ubicación/oficina para los festivos locales
        """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "RRHH",
    "depends":[
        "hr"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "hr_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
