# -*- coding: utf-8 -*-
from osv import osv, fields


class hr_employee(osv.osv):
    _name = 'hr.employee'
    _inherit = 'hr.employee'

    _columns = {
        'photo': fields.binary('Photo'),
    }

hr_employee()