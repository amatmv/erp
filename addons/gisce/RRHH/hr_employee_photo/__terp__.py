# -*- coding: utf-8 -*-
{
    "name": "Add photo to employee",
    "description": """

        """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "RRHH",
    "depends":[
        "hr"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "hr_employee_view.xml"
    ],
    "active": False,
    "installable": True
}
