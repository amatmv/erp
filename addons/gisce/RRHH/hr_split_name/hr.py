# -*- coding: utf-8 -*-

from osv import osv, fields


def build_name(employee):
    surnames = []
    if employee.get('middle_name'):
        surnames.append(employee['middle_name'])
    if employee.get('last_name'):
        surnames.append(employee['last_name'])
    name = [' '.join(surnames)]
    if employee.get('first_name'):
        name.append(employee['first_name'])
    name = ', '.join(name)
    return name


class HREmployee(osv.osv):
    _name = 'hr.employee'
    _inherit = 'hr.employee'

    def create(self, cursor, uid, vals, context=None):
        if not context:
            context = {}
        vals['name'] = build_name(vals)
        return super(HREmployee,
                     self).create(cursor, uid, vals,context=context)

    def write(self, cursor, uid, ids, vals, context=None):
        if not context:
            context = {}
        update_keys = vals.keys()
        fields_to_read = ['first_name', 'middle_name', 'last_name']
        if set(update_keys) & set(fields_to_read):
            for employee in self.read(cursor, uid, ids, fields_to_read):
                del employee['id']
                employee.update(vals)
                vals['name'] = build_name(employee)
        return super(HREmployee, self).write(cursor, uid, ids, vals,
                                             context=context)

    _columns = {
        'first_name': fields.char('First name', size=256),
        'middle_name': fields.char('Middle name', size=256),
        'last_name': fields.char('Last name', size=256)
    }

HREmployee()