# -*- coding: utf-8 -*-
{
    "name": "rrhh_split_name",
    "description": """
        Separación del Nombre a 3 campos: Nombre/Apellido1/Apellido2
        """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "RRHH",
    "depends":[
        "hr"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "hr_view.xml"
    ],
    "active": False,
    "installable": True
}
