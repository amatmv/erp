# -*- coding: utf-8 -*-
from osv import osv, fields


class HRHolidaysStatus(osv.osv):
    _name = 'hr.holidays.status'
    _inherit = 'hr.holidays.status'

    _columns = {
        'terms': fields.text('Type terms')
    }

HRHolidaysStatus()


class HRHolidays(osv.osv):
    _name = 'hr.holidays'
    _inherit = 'hr.holidays'

    def on_change_holidays_status_id(self, cursor, uid, ids, holidays_status_id,
                                     context=None):
        if not context:
            context = {}
        status_obj = self.pool.get('hr.holidays.status')
        res = {'value': {}}
        parent = super(HRHolidays, self)
        if hasattr(parent, 'on_change_holidays_status_id'):
            parent = parent.on_change_holidays_status_id
            res = parent(cursor, uid, ids, holidays_status_id, context=context)
        if not holidays_status_id:
            res['value'].update({'status_terms': False})
            return res
        status = status_obj.browse(cursor, uid, holidays_status_id)
        res['value'].update({'status_terms': status.terms})
        return res

    def _status_terms(self, cursor, uid, ids, field_name, arg, context=None):
        res = dict.fromkeys(ids, False)
        for holiday in self.browse(cursor, uid, ids, context=context):
            if holiday.holiday_status and holiday.holiday_status.terms:
                res[holiday.id] = holiday.holiday_status.terms
        return res

    _columns = {
        'status_terms': fields.function(
            _status_terms,
            type='text',
            string='Status terms',
            store=True,
            method=True,
        )
    }

HRHolidays()