# -*- coding: utf-8 -*-
{
    "name": "Holidays types terms",
    "description": """
        Añadir campo de texto con los criterios de RRHH a la hora de escoger días
        """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "RRHH",
    "depends":[
        "base",
        "hr",
        "hr_holidays"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "hr_holidays_view.xml"
    ],
    "active": False,
    "installable": True
}
