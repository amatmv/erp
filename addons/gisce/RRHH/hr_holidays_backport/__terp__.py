# -*- coding: utf-8 -*-
{
    "name": "HR Holidays (backport)",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Backport some functionalities from major versions
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "hr_holidays"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/ir.model.access.csv",
        "hr_holidays_view.xml",
        "wizard/wizard_assign_days_view.xml"
    ],
    "active": False,
    "installable": True
}
