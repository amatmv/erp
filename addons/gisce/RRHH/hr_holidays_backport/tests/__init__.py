import netsvc
from destral import testing
from destral.transaction import Transaction
from osv.osv import except_osv
from expects import *


class HrHolidaysBackport(testing.OOTestCase):
    def test_assing_more_days(self):
        assign_obj = self.openerp.pool.get('wizard.hr.holidays.assign.days')
        status_obj = self.openerp.pool.get('hr.holidays.status')
        employee_obj = self.openerp.pool.get('hr.employee')
        holidays_user_obj = self.openerp.pool.get('hr.holidays.per.user')
        with Transaction().start(self.database) as txn:
            status_id = status_obj.create(txn.cursor, txn.user, {
                'name': 'Test status',
                'max_response_time': 5
            })
            employee_id = employee_obj.create(txn.cursor, txn.user, {
                'name': 'Employee test',
                'user_id': txn.user,
            })
            assign = assign_obj.create(txn.cursor, txn.user, {
                'name': 'Assignement without fixed dates',
                'assign_type': 'employee',
                'employee_id': employee_id,
                'holiday_status_id': status_id,
                'number_of_days': 20
            })
            res = assign_obj.assign(txn.cursor, txn.user, assign)

            expect(res).to(have_keys(
                domain="[('id', 'in', [1])]",
                name='Assigned',
                view_type='form',
                view_mode='tree,form',
                res_model='hr.holidays',
                type='ir.actions.act_window',
            ))

            ids = holidays_user_obj.search(txn.cursor, txn.user, [
                ('employee_id.id', '=', employee_id),
                ('holiday_status.id', '=', status_id)
            ])
            per_user = holidays_user_obj.read(txn.cursor, txn.user, ids[0])
            expect(per_user).to(have_keys(
                max_leaves=20,
                leaves_taken=0,
                remaining_leaves=20
            ))

    def test_assing_fix_days(self):
        assign_obj = self.openerp.pool.get('wizard.hr.holidays.assign.days')
        status_obj = self.openerp.pool.get('hr.holidays.status')
        employee_obj = self.openerp.pool.get('hr.employee')
        holidays_user_obj = self.openerp.pool.get('hr.holidays.per.user')
        with Transaction().start(self.database) as txn:
            status_id = status_obj.create(txn.cursor, txn.user, {
                'name': 'Test status',
                'max_response_time': 5
            })
            employee_id = employee_obj.create(txn.cursor, txn.user, {
                'name': 'Employee test',
                'user_id': txn.user,
            })
            assign = assign_obj.create(txn.cursor, txn.user, {
                'name': 'Assignement without fixed dates',
                'assign_type': 'employee',
                'employee_id': employee_id,
                'holiday_status_id': status_id,
                'fixed_dates': 1,
                'date_from': '2015-09-11',
                'date_to': '2015-09-11'
            })
            res = assign_obj.assign(txn.cursor, txn.user, assign)

            expect(res).to(have_keys(
                domain="[('id', 'in', [1, 2])]",
                name='Assigned',
                view_type='form',
                view_mode='tree,form',
                res_model='hr.holidays',
                type='ir.actions.act_window',
            ))

            ids = holidays_user_obj.search(txn.cursor, txn.user, [
                ('employee_id.id', '=', employee_id),
                ('holiday_status.id', '=', status_id)
            ])
            per_user = holidays_user_obj.read(txn.cursor, txn.user, ids[0])
            expect(per_user).to(have_keys(
                max_leaves=1,
                leaves_taken=1,
                remaining_leaves=0
            ))

    def test_requested_days(self):
        assign_obj = self.openerp.pool.get('wizard.hr.holidays.assign.days')
        status_obj = self.openerp.pool.get('hr.holidays.status')
        employee_obj = self.openerp.pool.get('hr.employee')
        holidays_user_obj = self.openerp.pool.get('hr.holidays.per.user')
        holidays_obj = self.openerp.pool.get('hr.holidays')
        with Transaction().start(self.database) as txn:
            status_id = status_obj.create(txn.cursor, txn.user, {
                'name': 'Test status',
                'max_response_time': 5
            })
            employee_id = employee_obj.create(txn.cursor, txn.user, {
                'name': 'Employee test',
                'user_id': txn.user,
            })
            assign = assign_obj.create(txn.cursor, txn.user, {
                'name': 'Assignement without fixed dates',
                'assign_type': 'employee',
                'employee_id': employee_id,
                'holiday_status_id': status_id,
                'number_of_days': 10,
            })
            res = assign_obj.assign(txn.cursor, txn.user, assign)
            holiday_id = holidays_obj.create(txn.cursor, txn.user, {
                'name': 'Xmas holidays',
                'date_from': '2015-01-01',
                'date_to': '2015-01-07',
                'holiday_status': status_id,
                'number_of_days': 7,
                'employee_id': employee_id
            })
            wf_service = netsvc.LocalService('workflow')
            wf_service.trg_validate(
                txn.user, 'hr.holidays', holiday_id, 'confirm', txn.cursor
            )
            holiday = holidays_obj.read(txn.cursor, txn.user, holiday_id)
            expect(holiday).to(have_key('state', 'confirm'))

            pu_ids = holidays_user_obj.search(txn.cursor, txn.user, [
                ('employee_id.id', '=', employee_id),
                ('holiday_status.id', '=', status_id)
            ])
            expect(pu_ids).to(have_len(1))

            per_user = holidays_user_obj.read(txn.cursor, txn.user, pu_ids[0])
            expect(per_user).to(have_keys(
                requested_leaves=7,

            ))

            wf_service.trg_validate(
                txn.user, 'hr.holidays', holiday_id, 'validate', txn.cursor
            )

            per_user = holidays_user_obj.read(txn.cursor, txn.user, pu_ids[0])
            expect(per_user).to(have_keys(
                requested_leaves=0,
                leaves_taken=7
            ))

    def test_fail_if_no_status(self):
        status_obj = self.openerp.pool.get('hr.holidays.status')
        employee_obj = self.openerp.pool.get('hr.employee')
        holidays_obj = self.openerp.pool.get('hr.holidays')
        with Transaction().start(self.database) as txn:
            status_id = status_obj.create(txn.cursor, txn.user, {
                'name': 'Test status',
                'max_response_time': 5
            })
            employee_id = employee_obj.create(txn.cursor, txn.user, {
                'name': 'Employee test',
                'user_id': txn.user,
            })
            holiday_id = holidays_obj.create(txn.cursor, txn.user, {
                'name': 'Xmas holidays',
                'date_from': '2015-01-01',
                'date_to': '2015-01-07',
                'holiday_status': status_id,
                'number_of_days': 7,
                'employee_id': employee_id
            })
            wf_service = netsvc.LocalService('workflow')

            def callback():
                wf_service.trg_validate(
                    txn.user, 'hr.holidays', holiday_id, 'confirm', txn.cursor
                )

            expect(callback).to(raise_error(except_osv, 'warning'))