from datetime import datetime

import netsvc
from osv import osv, fields
from addons.hr_holidays_backport.hr_holidays import get_number_of_days


class WizardHRHolidaysAssignDays(osv.osv_memory):
    _name = 'wizard.hr.holidays.assign.days'

    def get_employee_by_assign_type(self, cursor, uid, ids, context=None):
        if isinstance(ids, (list, tuple)):
            ids = ids[0]
        employee_obj = self.pool.get('hr.employee')
        if context is None:
            context = {}
        wiz = self.browse(cursor, uid, ids, context=context)
        employee_ids = []
        if wiz.assign_type == 'employee':
            employee_ids = [wiz.employee_id.id]
        elif wiz.assign_type == 'category':
            employee_ids = employee_obj.search(cursor, uid, [
                ('category_id', 'child_of', wiz.category_id.id)
            ])
        return employee_ids

    def assign(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        holiday_obj = self.pool.get('hr.holidays')
        if isinstance(ids, (list, tuple)):
            ids = ids[0]
        wiz = self.browse(cursor, uid, ids)
        if wiz.fixed_dates:
            number_of_days = get_number_of_days(
                cursor, uid, wiz.date_from, wiz.date_to
            )
        else:
            number_of_days = wiz.number_of_days
        employee_ids = wiz.get_employee_by_assign_type()
        wf_service = netsvc.LocalService('workflow')
        assigned_ids = []
        if not wiz.date_from:
            assign_date = datetime.now().strftime('%Y-%m-%d 00:00:00')
            wiz.write({'date_from': assign_date})
            wiz = self.browse(cursor, uid, ids)
        for employee_id in employee_ids:
            assign_id = holiday_obj.create(cursor, uid, {
                'name': wiz.name,
                'date_from': wiz.date_from,
                'date_to': wiz.date_from,
                'holiday_status': wiz.holiday_status_id.id,
                'employee_id': employee_id,
                'number_of_days': number_of_days * -1
            })
            wf_service.trg_validate(
                uid, 'hr.holidays', assign_id, 'confirm', cursor
            )
            wf_service.trg_validate(
                uid, 'hr.holidays', assign_id, 'validate', cursor
            )
            assigned_ids.append(assign_id)
            if wiz.fixed_dates:
                request_id = holiday_obj.create(cursor, uid, {
                    'name': wiz.name,
                    'date_from': wiz.date_from,
                    'date_to': wiz.date_to,
                    'holiday_status': wiz.holiday_status_id.id,
                    'employee_id': employee_id,
                    'number_of_days': number_of_days
                })
                wf_service.trg_validate(
                    uid, 'hr.holidays', request_id, 'confirm', cursor
                )
                wf_service.trg_validate(
                    uid, 'hr.holidays', request_id, 'validate', cursor
                )
                assigned_ids.append(request_id)
        res_ids = ', '.join(map(str, assigned_ids))
        return {
            'domain': "[('id', 'in', [%s])]" % res_ids,
            'name': 'Assigned',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'hr.holidays',
            'type': 'ir.actions.act_window',
        }

    _columns = {
        'name': fields.char('Description', size=65, required=True),
        'assign_type': fields.selection([
            ('employee', 'Employee'),
            ('category', 'Category')
        ], 'Assign type', required=True),
        'employee_id': fields.many2one('hr.employee', 'Employee'),
        'category_id': fields.many2one('hr.employee.category', 'Category'),
        'holiday_status_id': fields.many2one(
            'hr.holidays.status', 'Status', required=True
        ),
        'fixed_dates': fields.boolean('Fixed dates'),
        'date_from': fields.date('Date from'),
        'date_to': fields.date('Date to'),
        'number_of_days': fields.integer('Number of days', required=True),
    }

    _defaults = {
        'fixed_dates': lambda *a: 0,
        'assign_type': lambda *a: 'employee'
    }

WizardHRHolidaysAssignDays()