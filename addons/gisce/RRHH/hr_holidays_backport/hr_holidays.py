from datetime import timedelta

import pooler
import workdays
import arrow
from osv import osv, fields
from tools.translate import _


def get_number_of_days(cursor, uid, date_from, date_to, employee_id=None,
                       context=None):
    """
    :param cursor: Database cursor
    :param uid: User performing the action
    :param date_from: Date from
    :param date_to: Date to
    :param employee_id: Employee to check if there are already holidays between
    date_from and date_to
    :param context:
    :return: Number of days
    """
    if context is None:
        context = {}
    pool = pooler.get_pool(cursor.dbname)
    holidays_obj = pool.get('hr.holidays')
    holidays = []
    if employee_id:
        # Append holidays
        holidays_ids = holidays_obj.search(cursor, uid, [
            ('number_of_days', '>', 0),
            ('state', '=', 'validate'),
            ('date_from', '>=', date_from)
        ])
        for holiday in holidays_obj.read(cursor, uid, holidays_ids,
                                         ['date_from', 'date_to']):
            start = arrow.get(holiday['date_from']).datetime
            start = start.date()
            end = arrow.get(holiday['date_to']).datetime
            end = end.date()
            delta = (end - start).days + 1
            for x in xrange(0, delta):
                holidays.append(start + timedelta(days=x))
    if isinstance(date_from, basestring):
        date_from = arrow.get(date_from).datetime.date()
    if isinstance(date_to, basestring):
        date_to = arrow.get(date_to).datetime.date()
    return workdays.networkdays(date_from, date_to, holidays)


class HRHolidays(osv.osv):
    _name = 'hr.holidays'
    _inherit = 'hr.holidays'

    def onchange_date_to(self, cursor, uid, ids, date_from, date_to):
        employee_obj = self.pool.get('hr.employee')
        n_days = 0
        if date_from and date_to:
            employee_id = employee_obj.search(cursor, uid, [
                ('user_id.id', '=', uid)
            ])
            if employee_id:
                employee_id = employee_id[0]
            else:
                employee_id = None
            n_days = get_number_of_days(cursor, uid, date_from, date_to,
                                        employee_id)
        return {'value': {'number_of_days': n_days}}

    def _create_holiday(self, cr, uid, ids):
        holidays_user_obj = self.pool.get('hr.holidays.per.user')
        holidays = self.browse(cr, uid, ids[0])
        if holidays.number_of_days > 0 and holidays.state != 'draft':
            user_obj = self.pool.get('res.users')
            ids_user_hdays = holidays_user_obj.search(cr, uid, [
                ('employee_id', '=', holidays.employee_id.id),
                ('holiday_status', '=', holidays.holiday_status.id)
            ])
            if not ids_user_hdays:
                status = holidays.holiday_status.name
                context = {'lang': user_obj.browse(cr, uid, uid).context_lang}
                raise osv.except_osv(
                    _('Warning'),
                    _('You can request days for status %s') % status
                )
        return super(HRHolidays, self)._create_holiday(cr, uid, ids)

HRHolidays()


class HRHolidaysPerUser(osv.osv):
    _name = 'hr.holidays.per.user'
    _inherit = 'hr.holidays.per.user'

    def _ff_requested_leaves(self, cursor, uid, ids, field_name, arg,
                             context=None):
        obj_holiday = self.pool.get('hr.holidays')
        if context is None:
            context = {}
        res = dict.fromkeys(ids, 0)

        fields_to_read = ['employee_id', 'holiday_status', 'leaves_taken']
        for holiday_user in self.read(cursor, uid, ids, fields_to_read):
            days = 0
            ids_request = obj_holiday.search(cursor, uid, [
                ('employee_id', '=', holiday_user['employee_id'][0]),
                ('state', '=', 'confirm'),
                ('holiday_status', '=', holiday_user['holiday_status'][0])
            ])
            if ids_request:
                for holiday in obj_holiday.read(cursor, uid, ids_request,
                                            ['number_of_days']):
                    if holiday['number_of_days'] > 0:
                        days += holiday['number_of_days']
            res[holiday_user['id']] = days
        return res

    _columns = {
        'requested_leaves': fields.function(
            _ff_requested_leaves,
            method=True,
            string='Requested Leaves',
            type='float'
        ),
    }

HRHolidaysPerUser()