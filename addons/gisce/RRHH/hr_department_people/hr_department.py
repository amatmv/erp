# -*- coding: utf-8 -*-

from osv import osv, fields


class HRDepartment(osv.osv):
    _name = 'hr.department'
    _inherit = 'hr.department'

    _order = 'sequence asc'

    _columns = {
        'people': fields.many2many(
            'hr.employee',
            'hr_department_people',
            'department_id',
            'employee_id',
            'People'
        ),
        'sequence': fields.integer('Sequence'),
    }

    _defaults = {
        'sequence': lambda *a: 10,
    }

HRDepartment()