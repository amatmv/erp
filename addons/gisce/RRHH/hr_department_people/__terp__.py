# -*- coding: utf-8 -*-
{
    "name": "Department people",
    "description": """

        """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "RRHH",
    "depends":[
        "hr"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "hr_department_view.xml"
    ],
    "active": False,
    "installable": True
}
