from destral import testing
from destral.transaction import Transaction
from expects import *


class HRDepartmentPeople(testing.OOTestCase):

    def test_sequence(self):
        dep_obj = self.openerp.pool.get('hr.department')
        with Transaction().start(self.database) as txn:
            dep_1 = dep_obj.create(txn.cursor, txn.user, {
                'name': 'Dep 1',
                'sequence': 20,
                'company_id': 1
            })

            dep_2 = dep_obj.create(txn.cursor, txn.user, {
                'name': 'Dep 2',
                'sequence': 0,
                'company_id': 1
            })

            ids = dep_obj.search(txn.cursor, txn.user, [])
            expect(ids).to(equal([2, 1]))
