import netsvc
from destral import testing
from destral.transaction import Transaction
from expects import *


class HRHolidaysResponseTimeTests(testing.OOTestCase):
    def test_send_email(self):
        account_obj = self.openerp.pool.get('poweremail.core_accounts')
        mail_obj = self.openerp.pool.get('poweremail.mailbox')
        status_obj = self.openerp.pool.get('hr.holidays.status')
        holidays_obj = self.openerp.pool.get('hr.holidays')
        employee_obj = self.openerp.pool.get('hr.employee')
        with Transaction().start(self.database) as txn:
            account_id = account_obj.create(txn.cursor, txn.user, {
                'email_id': 'test@example.com',
                'company': 'yes',
                'smtpport': 0,
                'smtpserver': 'test'
            })
            account_obj.do_approval(txn.cursor, txn.user, [account_id])
            status_id = status_obj.create(txn.cursor, txn.user, {
                'name': 'Test status',
                'max_response_time': 5
            })
            manager_id = employee_obj.create(txn.cursor, txn.user, {
                'name': 'Manager test',
                'user_id': txn.user,
                'work_email': 'manager@example.com'
            })
            employee_id = employee_obj.create(txn.cursor, txn.user, {
                'name': 'Employee test',
                'user_id': txn.user,
                'parent_id': manager_id
            })
            holiday_id = holidays_obj.create(txn.cursor, txn.user, {
                'holiday_status': status_id,
                'name': 'Holiday test',
                'date_from': '2015-01-01',
                'date_to': '2015-01-03',
                'employee_id': employee_id,
                'number_of_days': 3
            })
            wf_service = netsvc.LocalService('workflow')
            wf_service.trg_validate(
                txn.user, 'hr.holidays', holiday_id, 'confirm', txn.cursor
            )
            holiday = holidays_obj.read(txn.cursor, txn.user, holiday_id)
            expect(holiday).to(have_key('state', 'confirm'))

            txn.cursor.execute(
                "UPDATE hr_holidays SET write_date = now() - INTERVAL '6 days' "
                "WHERE id = %s", (holiday_id, )
            )

            expired = holidays_obj.response_time_expired(txn.cursor, txn.user)

            expect(expired).to(equal([holiday_id]))

            holidays_obj.send_response_reminder(txn.cursor, txn.user)

            wf_service.trg_validate(
                txn.user, 'hr.holidays', holiday_id, 'validate', txn.cursor
            )
            holiday = holidays_obj.read(txn.cursor, txn.user, holiday_id)
            expect(holiday).to(have_key('state', 'validate'))

            mail_id = mail_obj.search(txn.cursor, txn.user, [])
            expect(mail_id).to(equal([1]))

            mail = mail_obj.read(txn.cursor, txn.user, mail_id)
            expect(mail[0]).to(have_keys(
                pem_subject=u"Reminder: validate a leave request",
                pem_to=u"manager@example.com",
                folder=u"outbox"
            ))
