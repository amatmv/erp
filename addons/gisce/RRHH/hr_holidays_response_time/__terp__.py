# -*- coding: utf-8 -*-
{
    "name": "Holidays maximum resposne time",
    "description": """
        T4.6 Configuración tiempo máximo respuesta peticiones de vacaciones
        """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "RRHH",
    "depends":[
        "base",
        "hr",
        "hr_holidays",
        "poweremail"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "hr_holidays_view.xml",
        "hr_holidays_response_time_data.xml",
        "hr_holidays_response_time_cron.xml"
    ],
    "active": False,
    "installable": True
}
