# -*- coding: utf-8 -*-
from osv import osv, fields
import logging

logger = logging.getLogger('openerp.hr_holidays_response_time')


class HRHolidays(osv.osv):
    _name = 'hr.holidays'
    _inherit = 'hr.holidays'

    def _cron_send_remind(self, cursor, uid):
        self.send_response_reminder(cursor, uid)

    def send_response_reminder(self, cursor, uid):
        imd_obj = self.pool.get('ir.model.data')
        et_obj = self.pool.get('poweremail.templates')
        account_obj = self.pool.get('poweremail.core_accounts')
        holidays_remind = self.response_time_expired(cursor, uid)
        context = {}

        for holiday in holidays_remind:
            res = imd_obj.get_object_reference(
                cursor, uid, 'hr_holidays_response_time', 'email_remind'
            )
            template = et_obj.browse(cursor, uid, res[1])
            ctx = {}
            if not template.enforce_from_account:
                account_id = account_obj.search(cursor, uid, [
                    ('company', '=', 'yes'),
                    ('state', '=', 'approved')
                ])
                if not account_id:
                    raise osv.except_osv(
                        'Error', 'No email account defined'
                    )
                ctx['account_id'] = account_id[0]
            et_obj.generate_mail(cursor, 1, res[1], [holiday], context=ctx)
            logger.info("Sent a reminder for holiday id: %s" % holiday)
        return len(holidays_remind)


    def response_time_expired(self, cursor, uid, context=None):
        if not context:
            context = {}
        sql = ("SELECT h.id from hr_holidays h left join "
        "hr_holidays_status s on h.holiday_status = s.id where "
        "(s.max_response_time > 0 or s.max_response_time is not null) and "
        " h.state in ('confirm', 'validate1') and "
        "h.write_date + (s.max_response_time || ' days')::interval < ")
        if 'max_response_date' in context:
            expiring_date = context['max_response_date']
            sql += "%s"
            params = (expiring_date, )
        else:
            params = ()
            sql += "now()"
        cursor.execute(sql, params)
        return [x[0] for x in cursor.fetchall()]

HRHolidays()


class HRHolidaysStatus(osv.osv):
    _name = 'hr.holidays.status'
    _inherit = 'hr.holidays.status'

    _columns = {
        'max_response_time': fields.integer('Max response time (days)')
    }

    _defaults = {
        'max_response_time': lambda *a: 0
    }

HRHolidaysStatus()