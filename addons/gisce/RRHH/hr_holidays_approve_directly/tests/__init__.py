import netsvc
from destral import testing
from destral.transaction import Transaction
from expects import *


class HRHolidaysApproveDirectlyTests(testing.OOTestCase):

    def test_approve_directly(self):
        status_obj = self.openerp.pool.get('hr.holidays.status')
        employee_obj = self.openerp.pool.get('hr.employee')
        holidays_obj = self.openerp.pool.get('hr.holidays')
        with Transaction().start(self.database) as txn:
            status_id = status_obj.create(txn.cursor, txn.user, {
                'name': 'Test status',
                'approve_directly': 1
            })
            employee_id = employee_obj.create(txn.cursor, txn.user, {
                'name': 'Employee test',
                'user_id': txn.user,
            })
            holiday_id = holidays_obj.create(txn.cursor, txn.user, {
                'name': 'Xmas holidays',
                'date_from': '2015-01-01',
                'date_to': '2015-01-07',
                'holiday_status': status_id,
                'employee_id': employee_id,
                'number_of_days': 7
            })
            wf_service = netsvc.LocalService('workflow')
            wf_service.trg_validate(
                txn.user, 'hr.holidays', holiday_id, 'confirm', txn.cursor
            )
            holiday = holidays_obj.read(txn.cursor, txn.user, holiday_id)
            expect(holiday).to(have_key('state', 'validate'))

    def test_approve_not_directly(self):
        status_obj = self.openerp.pool.get('hr.holidays.status')
        employee_obj = self.openerp.pool.get('hr.employee')
        holidays_obj = self.openerp.pool.get('hr.holidays')
        with Transaction().start(self.database) as txn:
            status_id = status_obj.create(txn.cursor, txn.user, {
                'name': 'Test status',
                'approve_directly': 0
            })
            employee_id = employee_obj.create(txn.cursor, txn.user, {
                'name': 'Employee test',
                'user_id': txn.user,
            })
            holiday_id = holidays_obj.create(txn.cursor, txn.user, {
                'name': 'Xmas holidays',
                'date_from': '2015-01-01',
                'date_to': '2015-01-07',
                'holiday_status': status_id,
                'employee_id': employee_id,
                'number_of_days': 7
            })
            wf_service = netsvc.LocalService('workflow')
            wf_service.trg_validate(
                txn.user, 'hr.holidays', holiday_id, 'confirm', txn.cursor
            )
            holiday = holidays_obj.read(txn.cursor, txn.user, holiday_id)
            expect(holiday).to(have_key('state', 'confirm'))
