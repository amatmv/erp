# -*- coding: utf-8 -*-
{
    "name": "Approve directly holidays",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Approve directly holidays
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "hr_holidays"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "hr_holidays_workflow.xml",
        "hr_holidays_view.xml"
    ],
    "active": False,
    "installable": True
}
