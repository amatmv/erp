from osv import osv, fields


class HRHolidaysStatus(osv.osv):
    _name = 'hr.holidays.status'
    _inherit = 'hr.holidays.status'

    _columns = {
        'approve_directly': fields.boolean('Approve directly')
    }

    _defaults = {
        'approve_directly': lambda *a: 0
    }

HRHolidaysStatus()


class HRHolidays(osv.osv):
    _name = 'hr.holidays'
    _inherit = 'hr.holidays'

    def approve_directly(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        for holiday in self.browse(cursor, uid, ids, context=context):
            if holiday.holiday_status.approve_directly:
                return True
        return False

HRHolidays()