# -*- coding: utf-8 -*-
{
    "name": "Holidays for office",
    "description": """
        T4.2 Configuración calendarios por Oficina/Ubicación
        """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "RRHH",
    "depends":[
        "base",
        "hr_office",
        "hr_holidays_backport"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/wizard_assign_days_view.xml"
    ],
    "active": False,
    "installable": True
}
