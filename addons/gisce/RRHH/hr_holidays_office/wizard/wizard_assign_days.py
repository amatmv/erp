from osv import osv, fields


class WizardHRHolidaysAssignDays(osv.osv_memory):
    _name = 'wizard.hr.holidays.assign.days'
    _inherit = 'wizard.hr.holidays.assign.days'

    def __init__(self, pool, cursor):
        elem = ('office', 'Office')
        if elem not in self._columns['assign_type'].selection:
            self._columns['assign_type'].selection.append(elem)
        super(WizardHRHolidaysAssignDays, self).__init__(pool, cursor)

    def get_employee_by_assign_type(self, cursor, uid, ids, context=None):
        if isinstance(ids, (list, tuple)):
            ids = ids[0]
        employee_obj = self.pool.get('hr.employee')
        if context is None:
            context = {}
        wiz = self.browse(cursor, uid, ids, context=context)
        if wiz.assign_type == 'office':
            employee_ids = employee_obj.search(cursor, uid, [
                ('work_office', '=', wiz.office_id.id)
            ])
        else:
            parent = super(WizardHRHolidaysAssignDays, self)
            employee_ids = parent.get_employee_by_assign_type(
                cursor, uid, ids, context=context
            )
        return employee_ids

    _columns = {
        'office_id': fields.many2one('hr.office', 'Office')
    }

WizardHRHolidaysAssignDays()