# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function, unicode_literals

from datetime import datetime

from dateutil.relativedelta import relativedelta

from destral import testing
from destral.transaction import Transaction
from giscedata_facturacio.tests.utils import create_fiscal_year
from giscedata_polissa.tests import utils as utils_polissa


class TestsVariants(testing.OOTestCase):

    def setUp(self):
        self.pool = self.openerp.pool
        self.imd_obj = self.pool.get('ir.model.data')
        self.variant_obj = self.pool.get('giscedata.variants')
        self.fact_obj = self.pool.get('giscedata.facturacio.factura')
        self.polissa_obj = self.pool.get('giscedata.polissa')
        self.pol_modcon_obj = self.pool.get('giscedata.polissa.modcontractual')
        self.wiz_obj = self.pool.get('wizard.manual.invoice')
        self.journal_obj = self.pool.get('account.journal')
        self.lectura_obj = self.pool.get('giscedata.lectures.lectura')
        self.lectura_pot_obj = self.pool.get('giscedata.lectures.potencia')
        self.tax_obj = self.pool.get('account.tax')

        current_year = int(datetime.today().year)
        previous_year = current_year - 1

        self.openerp.install_module(
            'giscedata_tarifas_peajes_{}0101'.format(previous_year)
        )
        self.openerp.install_module(
            'giscedata_tarifas_pagos_capacidad_{}0101'.format(previous_year)
        )
        self.openerp.install_module(
            'giscedata_tarifas_peajes_{}0101'.format(current_year)
        )
        self.openerp.install_module(
            'giscedata_tarifas_pagos_capacidad_{}0101'.format(current_year)
        )

        self.invoice_start_date = '2018-12-01'
        self.invoice_end_date = '2019-02-01'

        self.txn = Transaction().start(self.database)

    def tearDown(self):
        self.txn.stop()

    def setup_contract(self, cursor, uid, context=None):
        if context is None:
            context = {}
        self.import_data(cursor, uid)
        polissa = self.polissa_obj.browse(
            cursor, uid, self.polissa_id
        )

        tarifa = self.imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'tarifa_20A_new'
        )[1]

        res_obj = self.pool.get('res.config')
        res_id = res_obj.search(cursor, uid, [
            ('name', '=', 'fact_uom_cof_to_multi')])
        res_obj.write(cursor, uid, res_id[0], {'value': '0'})

        factor_diari = self.imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'uom_pot_elec_dia'
        )[1]

        polissa.write({
            'potencia': context.get('potencia', 2),
            'llista_preu': self.pricelist_id,
            'tarifa': tarifa,
            'property_unitat_potencia': factor_diari,
        })
        polissa.send_signal([
            'validar', 'contracte'
        ])

        # Borramos todos los contadores activos para evitar interferencias
        for comptador in polissa.comptadors:
            for l in comptador.lectures:
                l.unlink(context={})
            for lp in comptador.lectures_pot:
                lp.unlink(context={})
            comptador.write({'lloguer': True, 'preu_lloguer': 0.5})

        comptador = polissa.comptadors[0]
        self.comptador_id = comptador.id

        vals = {
            'name': datetime.strftime(
                (
                    datetime.strptime(self.invoice_start_date, '%Y-%m-%d') +
                    relativedelta(days=-1)
                ), '%Y-%m-%d'
            ),
            'periode': self.periode_1_id,
            'lectura': 10,
            'tipus': 'A',
            'comptador': comptador.id,
            'observacions': '',
            'origen_id': self.origen_id,
        }
        self.lectura_obj.create(cursor, uid, vals)

        vals = {
            'name': datetime.strftime(
                (
                        datetime.strptime(self.invoice_start_date, '%Y-%m-%d') +
                        relativedelta(days=-1)
                ), '%Y-%m-%d'
            ),
            'periode': self.periode_2_id,
            'lectura': 10,
            'tipus': 'A',
            'comptador': comptador.id,
            'observacions': '',
            'origen_id': self.origen_id,
        }
        self.lectura_obj.create(cursor, uid, vals)

        vals = {
            'name': self.invoice_end_date,
            'periode': self.periode_1_id,
            'lectura': 200,
            'tipus': 'A',
            'comptador': comptador.id,
            'observacions': '',
            'origen_id': self.origen_id,
        }
        self.lectura_obj.create(cursor, uid, vals)

        vals = {
            'name': self.invoice_end_date,
            'periode': self.periode_2_id,
            'lectura': 200,
            'tipus': 'A',
            'comptador': comptador.id,
            'observacions': '',
            'origen_id': self.origen_id,
        }
        self.lectura_obj.create(cursor, uid, vals)

        utils_polissa.crear_modcon(
            self.pool, cursor, uid, self.polissa_id, {
                'potencies_periode': [
                    (1, polissa.potencies_periode[0].id, {'potencia': 3.450})
                ]
            }, '2018-01-01', '2019-12-31'
        )

        config_taxes_wiz_obj = self.pool.get('config.taxes.facturacio.comer')

        iva_venda = self.tax_obj.search(cursor, uid, [
            ('name', '=', 'IVA 21%'),
            ('type_tax_use', 'ilike', 'sale')
        ])[0]
        iva_compra = self.tax_obj.search(cursor, uid, [
            ('name', 'ilike', '%21% IVA Soportado (operaciones corrientes)%'),
            ('type_tax_use', 'ilike', 'purchase')
        ])[0]
        iese_venda = self.tax_obj.search(cursor, uid, [
            ('name', 'ilike', '%Impuesto especial sobre la electricidad%'),
        ])[0]
        wiz_id = config_taxes_wiz_obj.create(cursor, uid, {
            'iva_venda': iva_venda,
            'iese_venda': iese_venda,
            'iva_compra': iva_compra,
        })
        config_taxes_wiz_obj.action_set(cursor, uid, [wiz_id])

        create_fiscal_year(self.pool, cursor, uid, fiscal_year='2018')

        return polissa

    def current_period(self, cursor, uid, date=None):
        if date is None:
            date = datetime.today()
        return self.pool.get('account.period').find(
            cursor, uid, date
        )[0]

    def manual_invoice(self, cursor, uid):
        wiz_id = self.wiz_obj.create(
            cursor, uid, {}
        )
        wiz_fact = self.wiz_obj.browse(
            cursor, uid, wiz_id
        )
        wiz_fact.write({
            'polissa_id': self.polissa_id,
            'date_start': self.invoice_start_date,
            'date_end': self.invoice_end_date,
            'journal_id': self.journal_id
        })
        wiz_fact.action_manual_invoice()
        invoice_id = eval(wiz_fact.invoice_ids)[0]

        invoice = self.fact_obj.browse(
            cursor, uid, invoice_id
        )

        self.fact_obj.write(cursor, uid, invoice.id, {
            'period_id': self.current_period(
                cursor, uid, date=self.invoice_end_date
            )
        })

        wiz_fact.unlink()
        return invoice

    def import_data(self, cursor, uid):
        self.polissa_id = self.imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]
        self.pricelist_id = self.imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio',
            'pricelist_tarifas_electricidad'
        )[1]

        self.discount_product_id = self.imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio_comer_tajuna', 'prod_descuento'
        )[1]

        self.periode_1_id = self.imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'p1_e_tarifa_20DHA_new'
        )[1]
        self.periode_2_id = self.imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'p2_e_tarifa_20DHA_new'
        )[1]

        self.origen_id = self.imd_obj.get_object_reference(
            cursor, uid, 'giscedata_lectures', 'origen10'
        )[1]

        self.journal_id = self.journal_obj.search(
            cursor, uid, [('code', '=', 'ENERGIA')]
        )[0]

    def get_negative_lines(self, fact):
        return filter(lambda line: line.price_unit < 0, fact.linia_ids)

    def test_create_discount_line_when_creating_invoice(self):
        """
        Test that when the invoice is created and the contract has the "E-mail"
        send method, the invoice is generated with a discount line of -1€.
        """

        cursor = self.txn.cursor
        uid = self.txn.user
        self.setup_contract(cursor, uid)
        self.polissa_obj.write(cursor, uid, self.polissa_id, {'enviament': 'email'})
        modcon_id = self.pol_modcon_obj.search(cursor, uid, [('polissa_id', '=', self.polissa_id)])
        self.pol_modcon_obj.write(cursor, uid, modcon_id, {'enviament': 'email'})
        fact = self.manual_invoice(cursor, uid)

        negative_lines = self.get_negative_lines(fact)

        self.assertTrue(negative_lines)
        self.assertEquals(len(negative_lines), 1)
        self.assertEquals(negative_lines[0].name, 'Descuento envío factura por e-mail')
        self.assertEquals(round(negative_lines[0].price_unit), -1)
        self.assertEquals(negative_lines[0].product_id.id, self.discount_product_id)
