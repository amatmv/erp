# -*- coding: utf-8 -*-
from __future__ import absolute_import

from giscedata_facturacio_comer.report.giscedata_facturacio_comer import FacturaReportWebkitParserHTML, OPTIMIZED_INVOICES

FacturaReportWebkitParserHTML(
    rml='giscedata_facturacio_comer_panti/report/factura.mako',
)

OPTIMIZED_INVOICES.append('giscedata_facturacio_comer_tajuna/report/factura.mako')