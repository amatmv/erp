## -*- coding: utf-8 -*-
<%
    from datetime import datetime
    from giscedata_facturacio_comer.giscedata_facturacio_report import origen_energia_espanya_2017
    import json

    report_o = pool.get('giscedata.facturacio.factura.report')
    banner_obj = pool.get('report.banner')

    comer = report_o.get_company_info(cursor, uid, company.id)[company.id]

    def get_total_invoice_line_formatted(invoice_line):
        discount = invoice_line.get('note', '')
        if discount:
            discount = discount.split('%')[0]
            discount = "{}% Dto: ".format(discount)
        else:
            discount = ''

        return "{}{}".format(discount, formatLang(invoice_line['total']))

    # demo data
    comer_energy_origin = {
        'Renovable': 20,      'Cogeneración de Alta Eficiencia': 5,
        'Cogeneración': 12.5,   'CC Gas Natural': 12.5,
        'Carbón': 12.5,         'Fuel/Gas': 12.5,
        'Nuclear': 12.5,        'Otras': 12.5,
    }

%>
## GENERIC STUFF ---------------------------------------------------------------
<%def name="invoice_detail_energy(factura)">
    <table class="styled-inner-table">
        <colgroup>
            <col class="detall-factura-inner-table-col-1"/>
            <col class="detall-factura-inner-table-col-2"/>
            <col class="detall-factura-inner-table-col-3"/>
            <col class="detall-factura-inner-table-col-4"/>
            <col class="detall-factura-inner-table-col-5"/>
        </colgroup>
        <thead>
            <tr>
                <th>Término de energía</th>
                <th>Consumo (kWh)</th>
                <th>Precio (€/kWh)</th>
                <th colspan="2">Total (€)</th>
             </tr>
        </thead>
        <tbody>
            % for energy_line in factura['detailed_lines']['energy']['lines']:
                <tr>
                    <td class="bold center">${energy_line['name']}</td>
                    <td class="right">${int(energy_line['consumption'])}</td>
                    <td class="right">${formatLang(energy_line['price'], digits=6)}</td>
                    <td class="right">${get_total_invoice_line_formatted(energy_line)}</td>
                    <td class="left white toll italic">(de los cuales peajes ${formatLang(energy_line['toll'])})</td>
                </tr>
            % endfor
        </tbody>
    </table>
</%def>
<%def name="invoice_detail_power(factura)">
    <table class="styled-inner-table">
        <colgroup>
            <col width="20%"/>
            <col width="20%"/>
            <col width="20%"/>
            <col/>
            <col/>
            <col width="20%"/>
        </colgroup>
        <thead>
            <tr>
                <th>Término de potencia</th>
                <th>Consumo (kW)</th>
                <th>Precio (€/kW-dia)</th>
                <th>Días</th>
                <th colspan="2">Total (€)</th>
            </tr>
        </thead>
        <tbody>
            % for power_line in factura['detailed_lines']['power']['lines']:
                <tr>
                    <td class="bold center white">${power_line['name']}</td>
                    <td class="right white">${formatLang(power_line['consumption'], digits=3)}</td>
                    <td class="right white">${formatLang(power_line['price'], digits=6)}</td>
                    <td class="center white">${int(power_line['time'])}</td>
                    <td class="right white">${get_total_invoice_line_formatted(power_line)}</td>
                    <td class="left white italic toll">(de los cuales peajes ${formatLang(power_line['toll'])})</td>
                </tr>
            % endfor
        </tbody>
    </table>
</%def>
<%def name="invoice_detail_reactive(factura)">
    <table class="styled-inner-table">
        <colgroup>
            <col class="detall-factura-inner-table-col-1"/>
            <col class="detall-factura-inner-table-col-2"/>
            <col class="detall-factura-inner-table-col-3"/>
            <col class="detall-factura-inner-table-col-4"/>
            <col class="detall-factura-inner-table-col-5"/>
        </colgroup>
        <thead>
            <tr>
                <th>Energía reactiva</th>
                <th>Excés (kVArh)</th>
                <th>Preu (€/kVArh)</th>
                <th colspan="2">Total (€)</th>
            </tr>
        </thead>
        <tbody>
            % for reactive_line in factura['detailed_lines']['reactive']['lines']:
                <tr>
                    <td class="bold center">${reactive_line['name']}</td>
                    <td class="right">${formatLang(reactive_line['consumption'])}</td>
                    <td class="right">${formatLang(reactive_line['price'], digits=6)}</td>
                    <td class="right">${get_total_invoice_line_formatted(reactive_line)}</td>
                    <td class="left white toll italic">(de los cuales peajes${formatLang(reactive_line['toll'])})</td>
                </tr>
            % endfor
        </tbody>
    </table>
</%def>
<%def name="power_excess(factura)">
    <table class="styled-inner-table">
        <%
            power_excess_and_other_detailed_info = factura['power_excess_and_other_detailed_info']
            power_lines_with_excess = power_excess_and_other_detailed_info['lines_with_power_excess']
        %>
        <colgroup>
            <col class="detall-factura-inner-table-col-1"/>
            <col class="detall-factura-inner-table-col-2"/>
            <col class="detall-factura-inner-table-col-3"/>
            <col class="detall-factura-inner-table-col-4"/>
            <col class="detall-factura-inner-table-col-5"/>
        </colgroup>
        <thead>
            <tr>
                <th>Exceso potencia</th>
                <th>Exceso (kVArh)</th>
                <th>Precio (€/kVArh)</th>
                <th colspan="2">Total (€)</th>
            </tr>
        </thead>
        <tbody>
            <% total_exces = 0.0 %>
            % for power_line_with_excess in power_lines_with_excess:
                <tr>
                    <td class="bold center">${power_line_with_excess['name']}</td>
                    <td class="right">${formatLang(power_line_with_excess['excess'])}</td>
                    <td class="right">${formatLang(power_line_with_excess['price'], digits=6)}</td>
                    <td class="center" colspan="2">${power_line_with_excess['total']}</td>
                </tr>
                <% total_exces += power_line_with_excess['total'] %>
            % endfor
        </tbody>
    </table>
</%def>
<%def name="environmental_impact(cursor, uid, factura_date)">
    <table id="environmental-impact" class="styled-table">
        <%
            environmental_impact_co2 = banner_obj.get_banner(
                cursor, uid, 'giscedata.facturacio.factura', factura_date,
                code='environmental_impact_co2'
            )
            environmental_impact_radioactive = banner_obj.get_banner(
                cursor, uid, 'giscedata.facturacio.factura', factura_date,
                code='environmental_impact_radioactive'
            )

            if environmental_impact_co2:
                environmental_impact_co2 = '<img src="data:image/jpeg;base64,{}">'.format(environmental_impact_co2)
            else:
                environmental_impact_co2 = '<img src="{}/giscedata_facturacio_comer/report/assets/images/environmental_impact_co2.jpg">'.format(addons_path)

            if environmental_impact_radioactive:
                environmental_impact_radioactive = '<img src="data:image/jpeg;base64,{}">'.format(environmental_impact_radioactive)
            else:
                environmental_impact_radioactive = '<img src="{}/giscedata_facturacio_comer/report/assets/images/environmental_impact_radioactive.jpg">'.format(addons_path)
        %>
        <colgroup>
            <col width="40%"/>
            <col width="20%"/>
            <col width="40%"/>
        </colgroup>
        <thead>
            <tr>
                <th colspan="3">Impacto medioambiental</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <div id="environmental-impact-co2">${environmental_impact_co2}</div>
                </td>
                <td id="environmental-impact-descr" class="center">
                    L'impacte medioambiental de la seva electricitat consumida depèn de les fonts d'energía utilitzades per a la seva generació.
                </td>
                <td>
                    <div id="environmental-impact-radioactive">${environmental_impact_radioactive}</div>
                </td>
            </tr>
        </tbody>
    </table>
</%def>
## END GENERIC STUFF -----------------------------------------------------------
<%def name="header(factura, comer)">
    <table id="header">
        <colgroup>
            <col width="15%"/>
            <col>
            <col width="50%">
        </colgroup>
        <tbody>
            <tr>
                <td><img src="data:image/jpeg;base64,${comer['logo']}"/></td>
                <td>${informacion_electrica(comer, factura['supplier'])}</td>
                <td class="padding-left-10px align-table-top">${informacion_factura(factura)}</td>
            </tr>
        </tbody>
    </table>
</%def>
<%def name="informacion_electrica(comer, supplier)">
    <table id="info-electrica">
        <tr>
            <th>${comer['name']}</th>
        </tr>
        <tr>
            <td>${comer['street'].replace('..', '.')}</td>
        </tr>
        <tr>
            <td>${comer['zip']}, ${comer['poblacio']} (${comer['state']})</td>
        </tr>
        <tr>
            <td>Tel/Fax: ${comer['phone']}</td>
        </tr>
        <tr>
            <td>Tel Averías: ${supplier['phone']}</td>
        </tr>
        <tr>
            <td>e-mail: ${comer['email']}</td>
        </tr>
    </table>
</%def>
<%def name="ventanilla(factura)">
    <table id="finestreta">
        <% contact = factura['contact'] %>
        <tbody>
            %if factura['holder']['vat'] == 'ESP2811000E':
                <tr>
                    <td>AYUNTAMIENTO: ${factura['cups']['aclarador'].title()}</td>
                </tr>
            %else:
                <tr>
                    <td>${contact['name']}</td>
                </tr>
            %endif
            <tr>
                <td>${contact['street'].replace('..', '.')}</td>
            </tr>
            <tr>
                <td>${contact['zip']} ${contact['city']}</td>
            </tr>
        </tbody>
    </table>
</%def>
<%def name="banner(factura_date)">
    <div class="banner center-image">
        <%
            banner_img = banner_obj.get_banner(
                cursor, uid, 'giscedata.facturacio.factura',
                factura_date
            )
        %>
        %if banner_img:
            <img src="data:image/jpeg;base64,${banner_img}">
        %endif
    </div>
</%def>
<%def name="informacion_factura(factura)">
    <table class="styled-table summarized" id="factura">
        <thead>
            <tr>
                <th colspan="2">INFORMACIÓN FACTURA</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="bold">Fecha Emisión</td>
                <td>${factura['date']}</td>
            </tr>
            <tr>
                <td class="bold">Periodo de Facturación</td>
                <td>Del ${factura['init_date']} al ${factura['end_date']}</td>
            </tr>
            <tr>
                <td class="bold">Número de factura</td>
                <td>${factura.get('name', '')}</td>
            </tr>
            <tr>
                <td class="bold">Total Factura</td>
                <td>${formatLang(factura['amount'], monetary=True)} Euros</td>
            </tr>
        </tbody>
    </table>
</%def>
<%def name="informacion_pago(factura)">
    <table class="styled-table">
        <%
            iban = factura['bank']['iban']
            domiciliado = factura['payment_type_code'] == 'RECIBO_CSB'
        %>
        <thead>
            <tr>
                <th colspan="4">INFORMACIÓN SOBRE PAGO</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <% forma = "DOMICILIADO" if domiciliado else "TRANSFERENCIA" %>
                <td colspan="4"><b>Forma:</b> ${forma}</td>
            </tr>
            <tr>
                <% label_due_date = "Fecha prevista cargo" if domiciliado else "Fecha límite" %>
                <td colspan="4"><b>${label_due_date}:</b> ${factura['due_date']}</td>
            </tr>
            %if domiciliado:
                <tr>
                    <td colspan="4"><b>Banco:</b> ${factura['bank']['name']}</td>
                </tr>
                <tr>
                    <td class="bold">Entidad</td>
                    <td class="bold">Sucursal</td>
                    <td class="bold">DC</td>
                    <td class="bold">Número Cuenta</td>
                </tr>
                <tr>
                    <td>${iban[4:8]}</td>
                    <td>${iban[8:12]}</td>
                    <td>${iban[12:14]}</td>
                    <td>${iban[14:-4]} ****</td>
                </tr>
            %endif
            <tr>
                <td colspan="4"><b>Importe a Pagar:</b> ${formatLang(factura['amount'], monetary=True)} Euros</td>
            </tr>
        </tbody>
    </table>
</%def>
<%def name="informacion_pago_factor_3(factura)">
    <table class="styled-table">
        <%
            iban = factura['bank']['iban']
            domiciliado = factura['payment_type_code'] == 'RECIBO_CSB'
        %>
        <thead>
            <tr>
                <th colspan="4">INFORMACIÓN SOBRE PAGO</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <% forma = "DOMICILIADO" if domiciliado else "TRANSFERENCIA" %>
                <td colspan="2"><b>Forma:</b> ${forma}</td>
                <% label_due_date = "Fecha prevista cargo" if domiciliado else "Fecha límite" %>
                <td colspan="2"><b>${label_due_date}:</b> ${factura['due_date']}</td>
            </tr>
            %if domiciliado:
                <tr>
                    <td colspan="2"><b>Banco:</b> ${factura['bank']['name']}</td>
                    <td colspan="2"><b>Importe a Pagar:</b> ${formatLang(factura['amount'], monetary=True)} Euros</td>
                </tr>
                <tr>
                    <td class="bold">Entidad</td>
                    <td class="bold">Sucursal</td>
                    <td class="bold">DC</td>
                    <td class="bold">Número Cuenta</td>
                </tr>
                <tr>
                    <td>${iban[4:8]}</td>
                    <td>${iban[8:12]}</td>
                    <td>${iban[12:14]}</td>
                    <td>${iban[14:-4]} ****</td>
                </tr>
            %endif
        </tbody>
    </table>
</%def>
<%def name="informacion_cliente(factura)">
    <table class="styled-table">
        <%
            from giscedata_facturacio_comer.report.utils import clean_nif
            holder = factura['holder']
            policy = factura['policy']

            periods_power = policy['periods_power']
            periods_power_str = ""
            for period in sorted(periods_power):
                periods_power_str += "<b>{}:</b> {} &nbsp&nbsp&nbsp".format(period, formatLang(periods_power[period], digits=3))
        %>
        <thead>
            <tr>
                <th colspan="4">INFORMACIÓN CLIENTE Y PUNTO SUMINISTRO</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="bold">Titular</td>
                <td>${holder['name']}</td>
            </tr>
            <tr>
                <td class="bold">DNI/CIF</td>
                <td>${clean_nif(holder['vat'])}</td>
            </tr>
            <tr>
                <td class="bold">CUPS</td>
                <td colspan="3">${factura['cups']['name']}</td>
            </tr>
            <tr>
                <td class="bold">Dirección punto suministro</td>
                <td colspan="3">${factura['cups']['address']}</td>
            </tr>
            <tr>
                <td class="bold">Tarifa</td>
                <td>${factura['tariff']}</td>
            </tr>
            <tr>
                <td class="bold">Pot. Contratada</td>
                <td colspan="3">${periods_power_str}</td>
            </tr>
            <tr>
                <td class="bold">Póliza</td>
                <td colspan="3">${policy['name']}</td>
            </tr>
        </tbody>
    </table>
</%def>
<%def name="informacion_contrato(factura)">
    <table class="styled-table">
        <%
            policy = factura['policy']
            supplier = factura['supplier']
            access_contract = supplier['ref'] or ''
        %>
        <thead>
            <tr>
                <th colspan="4">INFORMACIÓN CONTRATO</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="bold">Contrato</td>
                <td>${policy['name']}</td>
            </tr>
            <tr>
                <td class="bold">Contrato de acceso</td>
                <td>${access_contract}</td>
            </tr>
            <tr>
                <td class="bold">Distribuidora</td>
                <td>${supplier['name']}</td>
            </tr>
        </tbody>
    </table>
</%def>
<%def name="historial_consumo(factura)">
    <table class="styled-table">
        <%
            historic = factura['historic']
        %>
        <thead>
            <tr>
                <th>Historial de Consumo</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <div class="chart_consum center-image" id="chart_consum_${factura['id']}"></div>
                </td>
            </tr>
        </tbody>
    </table>
    <script>
        // BAR PLOT VARIABLES
        var barplot_height = 90;
        var barplot_width = 300;

        var grad_colors_1 = [['#badcff', '#4D77A4']];

        var grad_colors_3 = [
            ['#badcff', '#4D77A4'],
            ['#badcff', '#4D77A4'],
            ['#badcff', '#4D77A4'],
            ['#badcff', '#4D77A4'],
            ['#badcff', '#4D77A4'],
            ['#badcff', '#4D77A4']
        ];

        var factura_id = ${factura['id']};
        var data_consum = ${json.dumps(sorted(historic['historic_js'], key=lambda h: h['mes']))};
    </script>
    <script src="${addons_path}/giscedata_facturacio_comer/report/assets/consumption-chart.js"></script>
</%def>
<%def name="energy_origin(comer_energy_origin, spain_energy_origin, iteration)">
    <table width="94%" align="center" class="styled-table">
        <thead>
            <tr>
                <th colspan="3">Mezcla de Producción en el sistema elétrico español (2017)</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <table id="energy-origin">
                        <tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <td class="bold">Tajuña</td>
                                <td class="bold">España</td>
                            </tr>
                            <tr>
                                <td><div class="square color-renovable"></div></td>
                                <td>Renovable</td>
                                <td class="center">${comer_energy_origin['Renovable']}%</td>
                                <td class="center">${spain_energy_origin['Renovable']}%</td>
                            </tr>
                            <tr>
                                <td><div class="square color-cogeneracio-alta-eficiencia"></div></td>
                                <td>Cogeneración de Alta Eficiencia</td>
                                <td class="center">${comer_energy_origin['Cogeneración de Alta Eficiencia']}%</td>
                                <td class="center">${spain_energy_origin['Cogeneración de Alta Eficiencia']}%</td>
                            </tr>
                            <tr>
                                <td><div class="square color-cogeneracio"></div></td>
                                <td>Cogeneración</td>
                                <td class="center">${comer_energy_origin['Cogeneración']}%</td>
                                <td class="center">${spain_energy_origin['Cogeneración']}%</td>
                            </tr>
                            <tr>
                                <td><div class="square color-cc-gas-natural"></div></td>
                                <td>CC Gas Natural</td>
                                <td class="center">${comer_energy_origin['CC Gas Natural']}%</td>
                                <td class="center">${spain_energy_origin['CC Gas Natural']}%</td>
                            </tr>
                            <tr>
                                <td><div class="square color-carbo"></div></td>
                                <td>Carbón</td>
                                <td class="center">${comer_energy_origin['Carbón']}%</td>
                                <td class="center">${spain_energy_origin['Carbón']}%</td>
                            </tr>
                            <tr>
                                <td><div class="square color-fuel-gas"></div></td>
                                <td>Fuel/Gas</td>
                                <td class="center">${comer_energy_origin['Fuel/Gas']}%</td>
                                <td class="center">${spain_energy_origin['Fuel/Gas']}%</td>
                            </tr>
                            <tr>
                                <td><div class="square color-nuclear"></div></td>
                                <td>Nuclear</td>
                                <td class="center">${comer_energy_origin['Nuclear']}%</td>
                                <td class="center">${spain_energy_origin['Nuclear']}%</td>
                            </tr>
                            <tr>
                                <td><div class="square color-altres"></div></td>
                                <td>Otros</td>
                                <td class="center">${comer_energy_origin['Otras']}%</td>
                                <td class="center">${spain_energy_origin['Otras']}%</td>
                            </tr>
                        </tbody>
                    </table>
                </td>
                <td colspan="9">
                    <table id="energy-origin-donut">
                        <tr>
                            <td colspan="2"><div id="energy-origin-country-${iteration}" class="test"></div></td>
                        </tr>
                        <tr>
                            <th>Tajuña</th>
                            <th>España</th>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <script src="${addons_path}/giscedata_facturacio_comer/report/assets/environmental_impact.js"></script>
    <script>
        var donut_height = 120;
        var donut_thickness = 30;
        var colors = [
            '#579834', '#63ad3b',
            '#89c563', '#d7ef81',
            '#d9d82e', '#eea620',
            '#fc6230', '#ee3e26',
            '#ffffff',

            '#ee3e26', '#fc6230',
            '#eea620', '#d9d82e',
            '#d7ef81', '#89c563',
            '#63ad3b', '#579834',
            '#ffffff'
        ];
        cols = [
            ['Renovable', ${spain_energy_origin['Renovable']}],
            ['Cogeneración de Alta Eficiencia', ${spain_energy_origin['Cogeneración de Alta Eficiencia']}],
            ['Cogeneración', ${spain_energy_origin['Cogeneración']}],
            ['CC Gas Natural', ${spain_energy_origin['CC Gas Natural']}],
            ['Carbón', ${spain_energy_origin['Carbón']}],
            ['Fuel/Gas', ${spain_energy_origin['Fuel/Gas']}],
            ['Nuclear', ${spain_energy_origin['Nuclear']}],
            ['Otras', ${spain_energy_origin['Otras']}],
            ['separator', 1],

            ['2Otras', ${comer_energy_origin['Otras']}],
            ['2Nuclear', ${comer_energy_origin['Nuclear']}],
            ['2Fuel/Gas', ${comer_energy_origin['Fuel/Gas']}],
            ['2Carbón', ${comer_energy_origin['Carbón']}],
            ['2CC Gas Natural', ${comer_energy_origin['CC Gas Natural']}],
            ['2Cogeneración', ${comer_energy_origin['Cogeneración']}],
            ['2Cogeneración de Alta Eficiencia', ${comer_energy_origin['Cogeneración de Alta Eficiencia']}],
            ['2Renovable', ${comer_energy_origin['Renovable']}],
            ['separator1', 1]
        ];
          donut_chart(donut_height, donut_height, cols, colors, '#energy-origin-country-${iteration}', false, '', donut_thickness)
    </script>
</%def>
<%def name="destino_factura_no_grafica(factura)">
    <%
        pie = factura['pie']
        pie_regulats = pie['regulats']
        pie_impostos = pie['impostos']
        pie_costos = pie['costos']
        dades_reparto = pie['dades_reparto']
        for repartiment in dades_reparto:
            repartiment[3] = formatLang(repartiment[3])

        invoice_destination_mssg = "Destino del importe total de la factura: " \
            "- Coste de producción de electricidad {} € " \
            "- Impuestos aplicados {} € " \
            "- Regulación eléctrica: " \
            "- Incentivos a las energías renovables, cogeneración i residuos: {} € " \
            "- Coste de las redes de distribución y transporte: {} € " \
            "- Otros costes regulados: {} €".format(
                formatLang(pie_costos),
                formatLang(pie_impostos),
                formatLang(pie_regulats*0.3628),
                formatLang(pie_regulats*0.3212),
                formatLang(pie_regulats*0.3160)
            )
        return invoice_destination_mssg
    %>
</%def>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_tajuna/report/factura.css"/>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer/report/assets/pie-chart.css"/>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_tajuna/report/assets/consumption-chart.css"/>
        <script src="${assets_path}/js/d3.min.js"></script>
        <script src="${assets_path}/js/c3.min.js"></script>
        <script src="${assets_path}/js/formatNumber.js"></script>
        <style>
            @font-face {
                font-family: "Roboto";
                src: url("${assets_path}/fonts/Roboto/Roboto-Regular.ttf") format('truetype');
                font-weight: normal;
            }
            @font-face {
                font-family: "Roboto";
                src: url("${assets_path}/fonts/Roboto/Roboto-Bold.ttf") format('truetype');
                font-weight: bold;
            }
        </style>
    </head>
    <%
        # Company information
        lateral_info =  comer['lateral_info'] or ''
    %>
    <body>
        %for factura in factura_data:
            <%
                contact = factura['contact']
                detailed_lines = factura['detailed_lines']
                invoiced_power = factura['invoiced_power']
                periods_and_readings = factura['periods_and_readings']
                periods = periods_and_readings['periods']
                periods_a = periods['active']
                periods_r = periods['reactive']
                periods_m = periods['maximeter']
                readings = periods_and_readings['readings']
                readings_a = readings['active']
                readings_r = readings['reactive']
                readings_m = readings['maximeter']
                policy = factura['policy']
                power_excess_and_other_detailed_info = factura['power_excess_and_other_detailed_info']
                other_lines = power_excess_and_other_detailed_info['other_lines']
                meters_rent = power_excess_and_other_detailed_info['meters_rent']
                power_lines_with_excess = power_excess_and_other_detailed_info['lines_with_power_excess']
                taxes = factura['taxes']
## _____________________________________________________________________________
                # Banner images
                factura_date = datetime.strptime(factura['date'], '%d/%m/%Y').strftime('%Y-%m-%d')
                banner_img = banner_obj.get_banner(
                    cursor, uid, 'giscedata.facturacio.factura',
                    factura_date, code='main_banner'
                )
                setLang(factura['lang_partner'])

                font_size_factor = 0

                n_energy_lines = len(factura['detailed_lines']['energy']['lines'])

                if '3' in factura['tariff']:
                    font_size_factor += 1 + len(readings_r['readings']) + len(readings_r['readings'])

                if n_energy_lines > 3:
                    font_size_factor += 1

                font_size_id = 'font-size-factor-' + str(font_size_factor)
            %>
            <div id="${font_size_id}">
            ${header(factura, comer)}
            <!-- BANNER + (TITULAR + RESUM FACTURA)-->
            <table id="bloc-1">
                <colgroup>
                    <col width="50%"/>
                    <col/>
                </colgroup>
                <tbody>
                    <tr>
                        <td class="padding-right-10px align-table-top" rowspan="2">
                            ${informacion_cliente(factura)}
##                                 ${informacion_contrato(factura)} TODO
                        </td>
                        <td class="padding-left-10px">
                            ${ventanilla(factura)}
                        </td>
                    </tr>
                    <tr>
                        <td>${banner(factura_date)}</td>
                    </tr>
                </tbody>
            </table>
            <!-- LECTURES -->
            <table id="consum-electric" class="styled-table">
                <thead>
                    <tr>
                        <th colspan="2">LECTURAS</th>
                        % for period_name in periods_a:
                            <th>${period_name}</th>
                        % endfor
                    </tr>
                </thead>
                <tbody>
                    <!-- ACTIVA -->
                    %if readings_a['readings']:
                        <tr>
                            <% rowspan_vertical_header = len(readings_a['readings']) * 3 + 2 %>
                            <th rowspan="${rowspan_vertical_header}" class="rotate h65px"><div>ENERGÍA</div></th>
                        </tr>
                        % for meter in sorted(readings_a['readings']):
                            <%
                                meter_readings =  readings_a['readings'][meter]
                                previous_reading_title = "Lectura anterior {} ({})".format(
                                    formatLang(meter_readings['P1']['previous_date'], date=True),
                                    meter_readings['P1']['previous_origin']
                                )
                                current_reading_title = "Lectura actual {} ({})".format(
                                    formatLang(meter_readings['P1']['current_date'], date=True),
                                    meter_readings['P1']['current_origin']
                                )
                            %>
                            <tr>
                                <td class="bold light-blue">Número de contador</td>
                                % for period_name in periods_a:
                                    <td class="center light-blue">${meter}</td>
                                % endfor
                            </tr>
                            <tr>
                                <td class="bold">${previous_reading_title}</td>
                                % for period in periods_a:
                                    <td class="right">${formatLang(meter_readings[period]['previous_reading'], digits=0)} kWh</td>
                                % endfor
                            </tr>
                            <tr>
                                <td class="bold">${current_reading_title}</td>
                                % for period in periods_a:
                                    <td class="right">${formatLang(meter_readings[period]['current_reading'], digits=0)} kWh</td>
                                % endfor
                            </tr>
                        % endfor
                        <tr>
                            <td class="bold">Consumo</td>
                            % for period in periods_a:
                                <td class="right">${formatLang(readings_a['total_consumption'][period], digits=0)} kWh</td>
                            %endfor
                        </tr>
                    %endif
                    <!-- END ACTIVA -->
                    <!-- REACTIVA -->
                    % if readings_r['readings'] or te_maximetre:
                        <tr>
                            <% rowspan_vertical_header = len(readings_r['readings']) * 3 + 2 %>
                            <th rowspan="${rowspan_vertical_header}" class="rotate h65px"><div>REACTIVA</div></th>
                        </tr>
                        % for meter in sorted(readings_r['readings']):
                            <%
                                meter_readings =  readings_r['readings'][meter]
                                previous_reading_title = "Lectura anterior {} ({})".format(
                                    formatLang(meter_readings['P1']['previous_date'], date=True),
                                    meter_readings['P1']['previous_origin']
                                )
                                current_reading_title = "Lectura actual {} ({})".format(
                                    formatLang(meter_readings['P1']['current_date'], date=True),
                                    meter_readings['P1']['current_origin']
                                )
                            %>
                            <tr class="row-separator">
                                <td class="bold light-blue">Número de contador</td>
                                % for period_name in periods_r:
                                        <td class="center light-blue">${meter}</td>
                                % endfor
                            </tr>
                            <tr>
                                <td class="bold">${previous_reading_title}</td>
                                % for period in periods_r:
                                    <td class="right">${formatLang(meter_readings[period]['previous_reading'], digits=0)} kWh</td>
                                % endfor
                            </tr>
                            <tr>
                                <td class="bold">${current_reading_title}</td>
                                % for period in periods_r:
                                    <td class="right">${formatLang(meter_readings[period]['current_reading'], digits=0)} kWh</td>
                                % endfor
                            </tr>
                        % endfor
                        <tr>
                            <td class="bold">Consumo</td>
                            % for period_name in periods_r:
                                <td class="right">${formatLang(readings_r['total_consumption'][period], digits=0)} kWh</td>
                            % endfor
                        </tr>
                    %endif
                    <!-- REACTIVA -->
                    <!-- MAXIMETRE -->
                    % if policy['has_maximeter']:
                        <tr class="header">
                            <th rowspan="4" class="rotate h65px"> <div>MAXÍMETRO</div></th>
                        </tr>
                        <tr class="row-separator">
                            <td class="bold">Potencia contratada</td>
                            % for period in periods_m:
                                <td class="right">${formatLang(readings_m[period]['power'], digits=3)} kW</td>
                            % endfor
                        </tr>
                        <tr>
                            <td class="bold">Lectura maxímetro</td>
                            % for period in periods_m:
                                <td class="right">${formatLang(readings_m[period]['maximeter'], digits=3)} kW</td>
                            % endfor
                        </tr>
                        <tr>
                            <td class="bold">Potencia facturada periodo</td>
                            % for period in periods_m:
                                <td class="right">${formatLang(factura['invoiced_power'][period], digits=3)} kW</td>
                            % endfor
                        </tr>
                    %endif
                    <!-- MAXIMETRE -->
                </tbody>
            </table>
            <!-- END LECTURES -->
            <!-- DETALLE FACTURA -->
            <table id="detall-factura" class="styled-table center-th">
                <colgroup>
                    <col width="32%" class="background-color"/>
                    <col width="20%"/>
                    <col/>
                    <col width="10%"/>
                </colgroup>
                <thead>
                    <tr>
                        <th colspan="4">FACTURACIÓN</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colspan="3">${invoice_detail_energy(factura)}</td>
                    </tr>
                    <tr class="row-separator">
                        <td colspan="4" class="bold right white">${formatLang(detailed_lines['energy']['total'])} €</td>
                    </tr>
                    <tr>
                        <td colspan="3">${invoice_detail_power(factura)}</td>
                    </tr>
                    <tr class="row-separator">
                        <td colspan="5" class="bold right white">${formatLang(detailed_lines['power']['total'])} €</td>
                    </tr>
                    %if detailed_lines['reactive']['lines']:
                        <tr>
                            <td colspan="3">${invoice_detail_reactive(factura)}</td>
                        </tr>
                        <tr class="row-separator">
                            <td colspan="4" class="bold right white">${formatLang(detailed_lines['reactive']['total'])} €</td>
                        </tr>
                    %endif
                    %if power_lines_with_excess:
                        <tr>
                            <td colspan="3">${power_excess(factura)}</td>
                        </tr>
                        <tr class="row-separator">
                            <td colspan="4" class="bold right white">${formatLang(total_exces)} €</td>
                        </tr>
                    %endif
                    % for iese_line in taxes['iese']['lines']:
                        <tr class="row-separator">
                            <th>Impuesto Electricidad</th>
                            <td class="right">${formatLang(iese_line['base_amount'])} €</td>
                            <td class="right">${u"5,11269632%"}</td>
                            <td class="bold right">${formatLang(iese_line['tax_amount'])} €</td>
                        </tr>
                    % endfor
                    % for meter_rent in meters_rent:
                        <tr class="row-separator">
                            <th>Alquiler contador</th>
                            <td class="right">${formatLang(meter_rent['days'], digits=0)} días</td>
                            <td class="right">${formatLang(meter_rent['price'], digits=6, monetary=True)} €/día</td>
                            <td class="bold right">${formatLang(meter_rent['amount'])} €</td>
                        </tr>
                    % endfor
                    %if other_lines['lines']:
                        <tr class="row-separator">
                            <% rowspan = len(other_lines['lines']) + 1 %>
                            <th rowspan="${rowspan}">Otros</th>
                            <td colspan="3"></td>
                        </tr>
                        % for other_line in other_lines['lines']:
                            <tr>
                                <td colspan="2" class="bold">${other_line['name']}</td>
                                <td class="bold right">${formatLang(other_line['price'])} €</td>
                            </tr>
                        % endfor
                    %endif
                    % for iva_line in taxes['iva']['lines']:
                        <tr class="row-separator">
                            <th>${iva_line['name']}</th>
                            <td class="right">${formatLang(iva_line['base'])} €</td>
                            <td class="right">${iva_line['percentage']}</td>
                            <td class="bold right">${formatLang(iva_line['amount'])} €</td>
                        </tr>
                    % endfor
                    <tr class="row-separator">
                        <th>TOTAL FACTURA</th>
                        <td></td>
                        <td></td>
                        <td class="bold right">${formatLang(factura['amount'])} €</td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="4" class="white">
                            Los precios de los términos de peaje de acceso son
                            los publicados en la ORDEN ETU/1976/2016. El precio
                            del alquiler de los contadores estan establecidos
                            en la ORDEN IET/1491/2013
                        </td>
                    </tr>
                </tfoot>
            </table>
            <!-- END DETALLE FACTURA -->
            <table id="bloc-2">
                <colgroup>
                    <col width="50%"/>
                    <col/>
                </colgroup>
                <tbody>
                    <tr>
                        %if font_size_factor < 3:
                            <td class="padding-right-10px">${informacion_pago(factura)}</td>
                        %else:
                            <td class="padding-right-10px">${informacion_pago_factor_3(factura)}</td>
                        %endif
                        <td class="padding-left-10px align-table-top" rowspan="2">${energy_origin(comer_energy_origin, origen_energia_espanya_2017, factura['id'])}</td>
                    </tr>
                    <tr>
                        <td class="padding-right-10px">${historial_consumo(factura)}</td>
                    </tr>
                </tbody>
            </table>
            <p id="info-footer">
                ${comer['lateral_info']}
                PROTECCIÓN DATOS: Reglamento Europeo de Proteccción de Datos
                2016/679 y Ley Orgánica 3/2018 de Proteccción de Datos Personales
                y Garantía de los Derechos Digitales: Responsable: ELECTRICA
                POPULAR S. COOP. MAD., C/ Imperial 3, 28540 de Perales de Tajuña
                (Madrid); Finalidad: Prestación del servicio solicitado y realizar
                la facturación del mismo; Legitimación: Relación contractual y/o
                consentimiento; Conservación: Sus datos se conservarán mientras
                se mantenga la relación o durante los años necesario para cumplir
                con las obligaciones legales; Destinatarios: Los datos no se cederán
                salvo que sea necesario para la realización del servicio o por
                obligación legal; Derechos: Puede ejercer dº acceso, rectificación,
                supresión, limitación, portabilidad, oposición y a presentar una
                reclamación ante la AEPD. Puede consultar información adicional
                en nuestras instalaciones o en www.electricapopular.org
                ${destino_factura_no_grafica(factura)}
            </p>
            </div>
##                 ${environmental_impact(cursor, uid, factura_date)}
            % if factura != factura_data[-1]:
                <p style="page-break-after:always; clear:both"></p>
            % endif
        %endfor
    </body>
</html>
