# -*- coding: utf-8 -*-
from __future__ import absolute_import

import tools
from giscedata_facturacio_comer.wizard.wizard_informes_facturacio_comer import (
    INFORMES_CSV
)
from giscedata_facturacio_comer.sql.sql_column_titles import QUERY_RESUM_COMER_TITLES

INFORMES_CSV.update({
    'export_factures_desglossat': {
        'header': QUERY_RESUM_COMER_TITLES,
        'query': "{}/giscedata_facturacio_comer_tajuna/sql/query_resum_comer.sql".format(tools.config['addons_path'])
    }
})
