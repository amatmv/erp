# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from tools.translate import _
from giscedata_facturacio_comer.sql.sql_column_titles import add_fields_query_resum

inheritations = {
    _("Cod. Comer."): [_("Referencia Distribuidora")],
    _("altres sense refacturacio"): [_("Descuentos envío e-mail")]
}

for inherit_field, fields_to_add in inheritations.items():
    add_fields_query_resum(
        inherit_field, fields_to_add
    )

