# -*- coding: utf-8 -*-
from osv import osv


class GiscedataFacturacioFacturaReport(osv.osv):

    _name = "giscedata.facturacio.factura.report"
    _inherit = "giscedata.facturacio.factura.report"

    def get_invoice_info(self, cursor, uid, ids, context=None):
        """
        Obtiene la información de la factura. Este método, además, obtiene el
        aclarador del CUPS asociado.
        :param cursor: DB Cursor.
        :type cursor: sql_db.Cursor
        :param uid: Usuario que realiza la acción.
        :type uid: int
        :param ids: Identificadores de las facturas.
        :type ids: list[int]
        :param context: OpenERP context.
        :type context: dict
        :return: El retorno de la función <get_invoice_info> del módulo
        <giscedata_facturacio_comer> agregando el aclarador del CUPS:
        retorno[identificador_factura]['cups']['aclarador'].
        :rtype: dict
        """
        res = super(GiscedataFacturacioFacturaReport, self).get_invoice_info(
            cursor, uid, ids, context=context
        )
        cups_o = self.pool.get('giscedata.cups.ps')
        for factura_id in res:
            cups_id = res[factura_id]['cups']['id']
            cups_v = cups_o.read(cursor, uid, cups_id, ['aclarador'], context=context)
            res[factura_id]['cups']['aclarador'] = cups_v['aclarador'] if cups_v['aclarador'] else ''

        return res


GiscedataFacturacioFacturaReport()
