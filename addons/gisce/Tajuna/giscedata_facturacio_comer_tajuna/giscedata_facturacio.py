# -*- encoding: utf-8 -*-
from __future__ import absolute_import, division, print_function, unicode_literals

from osv import osv


class GiscedataFacturacioFactura(osv.osv):

    _name = 'giscedata.facturacio.factura'
    _inherit = 'giscedata.facturacio.factura'

    def get_facts_to_discount(self, cursor, uid, ids, context=None):
        """
        Given a list of <giscedata.facturacio.factura> ids, returns a list of
        those whose contracts have the "send through e-mail" option activated.

        :param cursor: DB Cursor
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param ids: <giscedata.facturacio.factura> ids.
        :type ids: list[long]
        :param context: OpenERP context.
        :type context: dict | NoneType
        :return: ids of the invoices that fullfill the condition
        :rtype: list[long]
        """
        if context is None:
            context = {}

        fact_obj = self.pool.get('giscedata.facturacio.factura')
        res = fact_obj.search(cursor, uid, [
            ('per_enviar', 'ilike', 'email'),
            ('type', 'like', 'out_%'),
            ('id', 'in', ids)
        ])
        return res

    def create_discount_lines(self, cursor, uid, ids, context=None):
        """
        Create the discount line of 1€ to the invoices given.

        :param cursor: DB Cursor
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param ids: <giscedata.facturacio.factura> ids.
        :type ids: list[long]
        :param context: OpenERP context.
        :type context: dict | NoneType
        :return: None
        :rtype: NoneType
        """

        if context is None:
            context = {}

        if not isinstance(ids, (tuple, list)):
            ids = [ids]

        imd_obj = self.pool.get('ir.model.data')
        facturador_obj = self.pool.get('giscedata.facturacio.facturador')

        discount_product_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio_comer_tajuna', 'prod_descuento'
        )[1]

        vals = {
            'multi': 1,
            'product_id': discount_product_id,
            'name': 'Descuento envío factura por e-mail',
            'isdiscount': True,
            'tipus': 'altres',
            'quantity': 1
        }

        for fact_id in ids:
            facturador_obj.crear_linia(
                cursor, uid, fact_id, vals, context=context
            )


GiscedataFacturacioFactura()


class GiscedataFacturacioFacturador(osv.osv):

    _name = 'giscedata.facturacio.facturador'
    _inherit = 'giscedata.facturacio.facturador'

    def fact_via_lectures_pre_taxes_post_hook(
            self, cursor, uid, factura_id, context=None):
        """
        Override the method to discount 1€ to the invoices whose contracts
        have the "send through e-mail" option activated.

        :param cursor: DB Cursor
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param ids: <giscedata.facturacio.factura> ids.
        :type ids: list[long]
        :param context: OpenERP context.
        :type context: dict | NoneType
        :return: ids of the invoices processed
        :rtype: list[long]
        """
        if context is None:
            context = {}
        fact_obj = self.pool.get('giscedata.facturacio.factura')

        res = super(
            GiscedataFacturacioFacturador, self
        ).fact_via_lectures_pre_taxes_post_hook(cursor, uid, factura_id, context)

        fact_ids = fact_obj.get_facts_to_discount(
            cursor, uid, [factura_id], context)

        fact_obj.create_discount_lines(cursor, uid, fact_ids, context)

        return res


GiscedataFacturacioFacturador()
