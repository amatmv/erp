# -*- coding: utf-8 -*-
{
    "name": "Facturación Comercializadora Tajuña",
    "description": """Facturació Comercialitzadora Tajuña""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Tajuña",
    "depends": [
        "giscedata_facturacio_comer",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "giscedata_facturacio_comer_tajuna_report.xml",
        "product_data.xml",
    ],
    "active": False,
    "installable": True
}
