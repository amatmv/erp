# -*- coding: utf-8 -*-
{
  "name": "Mòdul d'instal·lacions de Règim Especial Tajuna",
  "description": """
  * Aquest mòdul afegeix les següents funcionalitats: Importador de curves de régim especial en format concret
""",
  "version": "0-dev",
  "author": "GISCE",
  "category": "GISCEMaster",
  "depends": [
      "giscedata_re"
  ],
  "init_xml": [],
  "demo_xml": [],
  "update_xml": [
      "wizard/importar_curva_re_from_file.xml"
  ],
  "active": False,
  "installable": True
}
