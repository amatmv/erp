# -*- coding: utf-8 -*-
from osv import osv, fields
import base64
import StringIO
import csv
from datetime import datetime


class ImportarCurvaREFromFile(osv.osv_memory):
    _name = 'giscedata.re.importar.curva.from.file'
    _inherit = 'giscedata.re.importar.curva.from.file'

    def importar_curva_re_from_file(self, cursor, uid, ids, context=None):
        """
        Importador de curvas a régimen especial con formato concreto
        :param cursor:
        :param uid:
        :param ids:
        :param context:
        :return:
        """
        re_obj = self.pool.get('giscedata.re')
        wizard = self.browse(cursor, uid, ids[0])
        if wizard.state != "init":
            raise osv.except_osv('Error', 'Fitxer ja importat!')

        txt = base64.decodestring(wizard.file)
        csv_file = StringIO.StringIO(txt)
        reader = csv.reader(csv_file, delimiter=';')
        # recuperar informacion del csv
        csv_data = self.get_csv_info(cursor, uid, ids, context, reader)
        # revisar que la curva sea completa
        dates_list = [x['time'] for x in csv_data]
        date_from = min(dates_list)
        date_to = max(dates_list)
        hours = re_obj.get_number_of_hours(date_from, date_to)
        if hours > len(csv_data):
            raise osv.except_osv("Error", "La curva no está completa, faltan horas")
        if hours < len(csv_data):
            raise osv.except_osv("Error", "La curva no está definida correctamente, sobran registros de horas")
        # crear registro de curva
        re_curva_id = self.crear_registro_curva(cursor, uid, ids, context, date_from, date_to)
        # crear registro de curva lectura
        self.crear_registro_curva_lectura(cursor, uid, ids, context, csv_data, re_curva_id)
        # actualizar estado importado
        info_str = "S'ha importat la corba correctament"
        self.write(cursor, uid, ids, {'state': 'end', 'info': info_str}, context)
        return True

    def crear_registro_curva_lectura(self, cursor, uid, ids, context, csv_data, re_curva_id):
        """
        Crea un registro curva lectura para la re_curva_id
        :param ids:
        :param context:
        :param uid:
        :param csv_data:
        :param cursor:
        :param re_curva_id:
        :return:
        """
        re_curva_lectura_obj = self.pool.get('giscedata.re.curva.lectura')
        re_obj = self.pool.get('giscedata.re')
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        wizard = self.browse(cursor, uid, ids[0])
        inst_re = context.get('active_id')
        measures = ['AE', 'AS', 'R1', 'R2', 'R3', 'R4']
        arrastres = dict.fromkeys(measures, 0)
        meter_id = wizard.meter_id
        meter_data = meter_obj.read(cursor, uid, meter_id.id, ['tg', 'technology_type'])
        origin = self._get_origin_meter(cursor, uid, meter_data.get('tg'), meter_data.get('technology_type'))
        for row in csv_data:
            timestamp = row.get('time')
            profile_v = {
                'ai': int(row.get('ai')),
                'ae': int(row.get('ae')),
                'r1': int(row.get('r1')),
                'r2': int(row.get('r2')),
                'r3': int(row.get('r3')),
                'r4': int(row.get('r4')),
                'timestamp': timestamp,
                'magn': 1000,
                'season': re_obj.get_estacio_ts(timestamp),
            }
            lecturas = re_obj.get_re_curva_lectura(cursor, uid, inst_re, profile_v, re_curva_id,
                                                   measures, arrastres, origin, context=context)
            for lectura in lecturas:
                re_curva_lectura_data = {
                    'curva': re_curva_id,
                    'name': lectura.get('name'),
                    'cups': lectura.get('cups'),
                    'year': lectura.get('year'),
                    'month': lectura.get('month'),
                    'day': lectura.get('day'),
                    'hour': lectura.get('hour'),
                    'estacio': lectura.get('estacio'),
                    'lectura': lectura.get('lectura'),
                    'upr': lectura.get('upr'),
                    'provincia': lectura.get('provincia'),
                    'tipo': lectura.get('tipo'),
                    'magnitud': lectura.get('magnitud'),
                }
                re_curva_lectura_obj.create(cursor, uid, re_curva_lectura_data)

    def _get_origin_meter(self, cursor, uid, is_tg, technology_type):
        origin = None
        if is_tg and technology_type == 'prime':
            origin = 'tg'
        elif technology_type in ('electronic', 'telemeasure'):
            origin = 'tm'
        return origin

    def crear_registro_curva(self, cursor, uid, ids, context, date_from, date_to):
        """
        Crea un registro curva RE
        :param ids:
        :param context:
        :param cursor:
        :param date_from:
        :param date_to:
        :param uid:
        :return: el id de la curva creada
        """
        re_curva_obj = self.pool.get('giscedata.re.curva')
        create_time = datetime.now().strftime('%d/%m/%Y %H:%M:%S')
        inst_re = context.get('active_id')
        re_curva_data = {
            'name': 'Curva creada el: {}'.format(create_time),
            'inici': date_from,
            'final': date_to,
            'inst_re': inst_re,
            'compact': True,
        }
        re_curva_id = re_curva_obj.create(cursor, uid, re_curva_data)
        return re_curva_id

    def get_csv_info(self, cursor, uid, ids, context, reader):
        """
        Lee un ficher de csv con una curva RE en un formato concreto
        :param context:
        :param ids:
        :param uid:
        :param cursor:
        :param reader: fichero csv
        :return: una estructura de datos con la información del csv
        """
        csv_data = []
        # recuperamos el año
        wizard = self.browse(cursor, uid, ids[0])
        year = str(wizard.year)
        # nos saltamos la cabecera
        next(reader)
        for row in reader:
            csv_time = row[0] + "-" + year + " " + row[1] + ":00"
            time = datetime.strptime(csv_time, "%d-%m-%Y %H:%M:%S").strftime("%Y-%m-%d %H:%M:%S")
            csv_data.append({
                'time': time,
                'ai': row[2],
                'ae': row[3],
                'r1': row[4],
                'r2': row[5],
                'r3': row[6],
                'r4': row[7],
            })
        # eliminar la fecha más pequeña si tiene hora 0
        date_min = min([x['time'] for x in csv_data])
        hour = datetime.strptime(date_min, "%Y-%m-%d %H:%M:%S").hour
        if hour == 0:
            csv_data = [x for x in csv_data if x['time'] != date_min]
        return csv_data

    _columns = {
        'year': fields.integer('Any', required=True),
        'meter_id': fields.many2one('giscedata.lectures.comptador', 'Meter', required=True),
        'state': fields.selection([('init', 'init'), ('end', 'end')]),
    }

    _defaults = {
        'year': lambda *a: datetime.now().year,
        'state': lambda *a: 'init',
        'info': lambda *a: '',
    }


ImportarCurvaREFromFile()
