# -*- encoding: utf-8 -*-
from osv import osv, fields


class product_pricelist(osv.osv):

    _name = "product.pricelist"
    _inherit = "product.pricelist"

    _columns = {
        'unique_price': fields.boolean(
            'Precio único',
            help='Indica si los precios de los productos son iguales por cada '
                 'versión'
        )
    }


product_pricelist()
