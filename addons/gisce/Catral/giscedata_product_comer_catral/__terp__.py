# -*- coding: utf-8 -*-
{
    "name": "Productos de las comercializadoras de Catral",
    "description": """Productos Catral (Comercializadora)""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Catral",
    "depends": [
        "product",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "pricelist_view.xml",
    ],
    "active": False,
    "installable": True
}
