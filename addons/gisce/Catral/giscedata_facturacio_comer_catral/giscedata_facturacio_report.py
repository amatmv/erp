# -*- coding: utf-8 -*-
from osv import osv


class GiscedataFacturacioFacturaReport(osv.osv):

    _name = "giscedata.facturacio.factura.report"
    _inherit = "giscedata.facturacio.factura.report"

    def get_lines_detailed_info(self, cursor, uid, ids, context=None):
        """
        Groups the energy lines if the invoice pricelist is marked as unique
        price.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id
        :type uid: long
        :param ids: <giscedata.facturacio.factura> ids.
        :type ids: list[long]
        :param context: OpenERP context.
        :type context: dict
        :return: See get_lines_detailed_info documentation at
        giscedata_facturacio_comer.
        :rtype: dict
        """
        if not isinstance(ids, list):
            ids = [ids]

        res = super(GiscedataFacturacioFacturaReport, self).get_lines_detailed_info(
            cursor, uid, ids, context=context
        )

        pricelist_o = self.pool.get('product.pricelist')
        factura_o = self.pool.get('giscedata.facturacio.factura')

        dmn = [('unique_price', '=', True)]
        pricelist_ids = pricelist_o.search(cursor, uid, dmn, context=context)

        dmn = [('id', 'in', ids), ('llista_preu', 'in', pricelist_ids)]
        factura_ids = factura_o.search(cursor, uid, dmn, context=context)

        for factura_id in factura_ids:
            lines = res[factura_id]
            energy_lines = lines['energy']['lines']
            energy_line_tmpl = {
                'name': 'Energía',
                'note': '',
                'consumption': 0,
                'price': 0,
                'toll': 0,
                'total': 0
            }

            new_energy_lines = {
                False: energy_line_tmpl, True: energy_line_tmpl.copy()
            }

            for energy_line in energy_lines:
                isdiscount = energy_line['isdiscount']
                new_energy_line = new_energy_lines[isdiscount]
                new_energy_line['consumption'] += energy_line['consumption']
                new_energy_line['price'] = energy_line['price']
                new_energy_line['toll'] += energy_line['toll']
                new_energy_line['total'] += energy_line['total']
                note = energy_line['note']
                if note and '%' in note:
                    new_energy_line['note'] = note.split('%')[0]

            if new_energy_lines[True]['total'] == 0:
                del new_energy_lines[True]
            else:
                factura_f = ['llista_preu', 'data_inici']
                factura_v = factura_o.read(
                    cursor, uid, factura_id, factura_f, context=context
                )
                pricelist_id = factura_v['llista_preu'][0]
                pricelist_v = pricelist_o.read(cursor, uid, pricelist_id, ['name'])
                pricelist_name = pricelist_v['name']
                discount_percentage = new_energy_lines[True]['note']  # '-7,00'
                discount_percentage = int(float(discount_percentage.replace(',', '.')))  # '-7'
                discount_percentage = abs(discount_percentage)  # 7

                discount_percentage_str = ''
                if discount_percentage:
                    discount_percentage_str = "{}% ".format(discount_percentage)

                new_energy_lines[True]['name'] = "{}Bonificación Tarifa {}".format(
                    discount_percentage_str, pricelist_name
                )

            lines['energy']['lines'] = new_energy_lines.values()

        return res


GiscedataFacturacioFacturaReport()
