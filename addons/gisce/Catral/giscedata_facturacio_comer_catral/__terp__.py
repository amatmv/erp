# -*- coding: utf-8 -*-
{
    "name": "Facturación Catral (Comercializadora)",
    "description": """Facturación Catral (Comercializadora)""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Catral",
    "depends": [
        "giscedata_facturacio_comer",
        "report_banner",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "giscedata_facturacio_comer_catral_report.xml",
        "wizard/wizard_aviso_pago_corte_view.xml",
        "security/ir.model.access.csv",
    ],
    "active": False,
    "installable": True
}
