# -*- coding: utf-8 -*-
from __future__ import absolute_import
from osv import osv, fields
from tools.translate import _


class WizardAvisoPagoCorte(osv.osv_memory):
    _name = 'wizard.aviso.pago.corte'

    def action_generate_report(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        wizard = self.browse(cursor, uid, ids[0])
        context.update({
            'tipo_carta': wizard.tipo_carta,
            'fecha_envio': wizard.fecha_envio,
            'fecha_limite': wizard.fecha_limite
        })
        return {
            'type': 'ir.actions.report.xml',
            'model': 'giscedata.facturacio.factura',
            'report_name': 'giscedata.facturacio.comer.catral.cartapago',
            'report_webkit': "'giscedata_facturacio_comer_catral/report/cartapago.mako'",
            'webkit_header': 'header_catral_base',
            'groups_id': [(6,0,[])],
            'multi': '0',
            'auto': '0',
            'header': '0',
            'report_rml': 'False',
            'context': context
        }

    _columns = {
        'tipo_carta': fields.selection(
            [('Aviso de Pago', 'Aviso de Pago'),
             ('Aviso de Corte', 'Aviso de Corte')], 'Tipo de informe',
        ),
        'fecha_envio': fields.date('Fecha del informe'),
        'fecha_limite': fields.date('Fecha del Pago/Corte'),
    }

WizardAvisoPagoCorte()
