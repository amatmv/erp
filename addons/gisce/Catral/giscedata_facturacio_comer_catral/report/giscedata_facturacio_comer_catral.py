# -*- coding: utf-8 -*-

from c2c_webkit_report import webkit_report
from report.report_sxw import rml_parse
from giscedata_facturacio_comer.report.giscedata_facturacio_comer import FacturaReportWebkitParserHTML

webkit_report.WebKitParser(
    'report.giscedata.facturacio.comer.catral.cartapago',
    'giscedata.polissa',
    'giscedata_facturacio_comer_catral/report/cartapago.mako',
    parser=rml_parse
)

FacturaReportWebkitParserHTML(
    rml='giscedata_facturacio_comer_catral/report/factura.mako',
)
