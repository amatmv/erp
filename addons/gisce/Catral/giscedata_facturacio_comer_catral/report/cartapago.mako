<%
from datetime import datetime

fact_obj = objects[0].pool.get('giscedata.facturacio.factura')

date_now = datetime.now().strftime('%Y-%m-%d')
nfacts = context.get('nfacts', 17)
model = context.get('model','giscedata.polissa')

company_address = company.partner_id.address[0]
if len(context.get('active_ids', [])):
    active_objects = objects[0].pool.get(model).browse(objects[0]._cr, user.id, context.get('active_ids', []))
else:
    active_objects = objects


def clean_nif(nif):
    if nif:
        return nif.replace('ES', '')
    else:
        return ''

tipo_carta = context.get('tipo_carta', "Aviso de Pago")
fecha_envio = context.get("fecha_envio", date_now)
fecha_envio = "{2}/{1}/{0}".format(*fecha_envio.split("-"))
fecha_limite = context.get("fecha_limite", date_now)
fecha_limite = "{2}/{1}/{0}".format(*fecha_limite.split("-"))
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_catral/report/cartapago.css"/>
        <script src="${addons_path}/giscedata_facturacio_comer_lersa/report/d3.min.js"></script>
    </head>
    <body>
        %for contrato in active_objects:
            <%
                current_object_id = contrato.id
                if model=='giscedata.facturacio.factura':
                    # es una factura
                    contrato = fact_obj.browse(cursor, uid, contrato.id).polissa_id
                use_lang = contrato.direccio_notificacio.partner_id.lang or 'es_ES'
                setLang(use_lang)
            %><%
                factura_ids = fact_obj.search(
                  cursor, uid,[
                    ('state', '=', 'open'),
                    ('polissa_id', '=', contrato.id),
                    ('type', '=', 'out_invoice')
                ], order="date_invoice desc")
            %><%
                facturas = fact_obj.read(
                    cursor, uid, factura_ids,
                    ['number', 'date_invoice', 'amount_total', 'residual'],
                    context={'lang': use_lang}
                )
                facturas.sort(key=lambda k: k['date_invoice'])
            %><%
                paginas_facturas = facturas if len(facturas)/nfacts == 1 else [
                  facturas[i*nfacts:i*nfacts+nfacts]
                  for i in range(0,len(facturas)/nfacts)
                ]
                last_page = [facturas[len(facturas)/nfacts*nfacts:]]
                if last_page:
                  paginas_facturas += last_page

                total_deuda = sum([f['residual'] for f in facturas])
            %>
            %for facturas in paginas_facturas:
                <!-- Company Header-->
                <table id="header" class="s10">
                    <colgroup>
                        <col width="15%"/>
                        <col/>
                        <col width="30%"/>
                    </colgroup>
                    <tbody>
                        <tr>
                            <td rowspan="6"><img src="data:image/jpeg;base64,${company.logo}"/></td>
                            <td rowspan="6" class="b16">
                              <center>
                                <p>${company.partner_id.name}</p>
                                <p class="s10">C.I.F.  ${clean_nif(company.partner_id.vat)}</p>
                              </center>
                            </td>
                            <td>${company_address.street.replace('..', '.')}</td>
                        </tr>
                        <tr>
                            <td>${company_address.zip}, ${company_address.id_municipi.name} (${company_address.state_id.name})</td>
                        </tr>
                        <tr>
                            <td>Tlf.: ${company_address.phone}</td>
                        </tr>
                        <tr>
                            <td>Fax: ${company_address.mobile}</td>
                        </tr>
                        <tr>
                            <td>${company.partner_id.website}</td>
                        </tr>
                        <tr>
                            <td>${company_address.email}</td>
                        </tr>
                    </tbody>
                </table>
                <div id="lateral-info" class="center">
                    ${company.rml_footer2 or ''}
                </div>
                <!-- Datos Header -->
                <div class="right-30">
                  <table class="w100p">
                      <tr>
                          <td>${_('Contrato:')}</td><td>${contrato.name}</td>
                      </tr>
                      <tr>
                          <td colspan="2">${contrato.direccio_notificacio.name}</td>
                      </tr>
                      <tr>
                          <td colspan="2">${contrato.direccio_notificacio.street.replace('..', '.')}</td>
                      </tr>
                      <tr>
                          <td colspan="2">${contrato.direccio_notificacio.zip} ${contrato.direccio_notificacio.id_municipi.name}</td>
                      </tr>
                      <tr>
                          <td colspan="2">${contrato.direccio_notificacio.state_id.name}</td>
                      </tr>
                  </table>
                </div>
                <div class="left-30 top-50">
                    <table class="styled-table">
                        <thead>
                            <tr>
                                <th colspan="2">DATOS TITULAR DEL CONTRATO</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="2">${contrato.titular.name}</td>
                            </tr><tr>
                                <td colspan="2">${contrato.titular.vat}</td>
                            </tr><tr>
                                <td colspan="2">${contrato.cups.name}</td>
                            </tr><tr>
                                <td colspan="2">${contrato.cups.direccio}</td>
                            </tr><tr>
                                <td><strong>${_('Contrato Cooperativa')}</strong></td><td><strong>${contrato.name}</strong></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <p style="clear:both"/>
                <p class="message">CATRAL A ${fecha_envio}</p>
                <div class="w100p titulo-carta"><center>${tipo_carta}</center></div>
                <div class="w100p">
                    <div class="paragraph s12">Muy Sres. Nuestros:</div>
                    <div class="paragraph-left s12">Por la presente le comunicamos que tenemos en nuestro poder los siguentes recibos de suministro eléctrico:</div>
                    <div class="w100p">
                      <table class="w100p light-border styled-table">
                        <thead class="s12">
                          <tr>
                            <th colspan="5" class="s14">Detalle Facturas</th>
                          </tr>
                          <tr>
                            <th>Referencia</th>
                            <th>Fecha Factura</th>
                            <th>Total Factura</th>
                            <th>Pagado</th>
                            <th>Pendiente</th>
                          </tr>
                        </thead>
                        <tbody>
                          %for i in range(0,nfacts):
                            <% factura = facturas[i] if len(facturas)>i else False %>
                            <tr class=".center">
                              %if factura:
                                  <td><center>${factura['number']}</center></td>
                                  <td><center>${formatLang(factura['date_invoice'], date=True)}</center></td>
                                  <td><center>${("{0:.2f}".format(factura['amount_total'])).replace('.', ',')}</center></td>
                                  <td><center>${("{0:.2f}".format(factura['amount_total']-factura['residual'])).replace('.', ',')}</center></td>
                                  <td><center>${("{0:.2f}".format(factura['residual'])).replace('.', ',')}</center></td>
                              %else:
                                  <td><center>&nbsp;</center></td>
                                  <td><center>&nbsp;</center></td>
                                  <td><center>&nbsp;</center></td>
                                  <td><center>&nbsp;</center></td>
                                  <td><center>&nbsp;</center></td>
                              %endif
                            </tr>
                          %endfor
                        </tbody>
                      </table>
                      <div class="right-50"><p class="b12">TOTAL DEUDA: ${("{0:.2f}".format(total_deuda)).replace('.', ',')}€ (EUROS)</p></div>
                    </div>
                </div>
                <div class="w100p s12 justify" style="clear:both">
                  <p>
                    Por lo expuesto, y de acuerdo con el punto 8º "SUSPENSIÓN DEL SUMINISTRO"
                    de las Condiciones Generales del Contrato de suministro de Energía Eléctrica
                    en el mercado liberalizado, se le concede un plazo determinado, para que proceda
                    a la cancelación úntegra de la citada deuda ingresando su importe en la citada
                    cuenta de esta mercantil "${company.name}":
                  </p>
                  <ul>
                    %for bank in company.partner_id.bank_ids:
                      <%
                      if bank.sequence != 999:
                          continue
                      %>
                      <li>
                        ${'{} {}'.format(bank.printable_iban, bank.name)}
                      </li>
                    %endfor
                  </ul>
                  <p class="b12">
                    Una vez efectuado el pago en el plazo señalado, deberá Vd. personarse
                    en las oficinas de esta entidad ${company_address.street.replace('..', '.')} a fin de retirar los recibos originales,
                    en la inteligencia de que pasado dicho plazo sin haberlo efectuado
                    procederemos al corte de suministro eléctrico, en el horario habitual de trabajo.
                    Efectuada la interrupción, se deberá abonar el servicio de reposición por un importe de 20€ (euros).
                  </p>
                  <div class="w100p">
                      <table class="styled-double-table">
                        <colgroup>
                            <col width="60%"/>
                            <col width="30%"/>
                            <col width="10%"/>
                        </colgroup>
                        <thead class="s14">
                          <tr>
                            <th>FECHA MÁXIMA PAGO DE FACTURA:</th>
                            <th>${fecha_limite}</th>
                            <th></th>
                          </tr>
                        </thead>
                      </table>
                  </div>
                  <div class="paragraph-left b14">Atentamente,</div>
                  <center>
                    <div class="center b14">${company.name}</div>
                  </center>
                </div>
                % if facturas != paginas_facturas[-1]:
                    <p style="page-break-after:always"></p>
                % endif
            %endfor
            % if current_object_id != active_objects[-1].id:
                <p style="page-break-after:always"></p>
            % endif
        %endfor
    </body>
</html>
