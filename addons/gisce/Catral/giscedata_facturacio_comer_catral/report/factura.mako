## -*- coding: utf-8 -*-
<%
    from operator import attrgetter, itemgetter
    from datetime import datetime, timedelta
    import json
    from giscedata_facturacio_comer.report.utils import has_setu35_line, clean_nif

    banner_o = pool.get('report.banner')
    polissa_obj = objects[0].pool.get('giscedata.polissa')
    model_obj = objects[0].pool.get('ir.model.data')
    sup_territorials_2013_comer_obj = pool.get('giscedata.suplements.territorials.2013.comer')
    report_o = objects[0].pool.get('giscedata.facturacio.factura.report')

    model_ids = model_obj.search(
        cursor, uid,
         [('module','=','giscedata_facturacio'),
         ('name','=','pricelist_tarifas_electricidad')]
    )
    tarifa_elect_atr = model_obj.read(cursor, uid, model_ids[0],['res_id'])['res_id']

    iet_peatges = "ORDEN IET/107/2014"
    itc_lloguer = "ORDEN ITC/3860/2007"

    comptador_factures = 0

    def get_exces_potencia(cursor, uid, factura):
        linia_obj = factura.pool.get('giscedata.facturacio.factura.linia')
        linies = linia_obj.search(
            cursor, uid,[('tipus', '=', 'exces_potencia'),('factura_id', '=', factura.id)]
        )
        if linies:
            return linia_obj.browse(cursor, uid, linies)
        else:
            return []


    def get_origen_lectura(cursor, uid, lectura):
        """Busquem l'origen de la lectura cercant-la a les lectures de facturació"""
        res = {lectura.data_actual: _(u"real"),
               lectura.data_anterior: _(u"real")}

        lectura_obj = lectura.pool.get('giscedata.lectures.lectura')
        tarifa_obj = lectura.pool.get('giscedata.polissa.tarifa')
        origen_obj = lectura.pool.get('giscedata.lectures.origen')

        # Recuperar l'estimada
        estimada_id = origen_obj.search(cursor, uid, [('codi', '=', '40')])[0]

        #Busquem la tarifa
        if lectura and lectura.name:
            tarifa_id = tarifa_obj.search(cursor, uid, [('name', '=',
                                                    lectura.name[:-5])])
        else:
            tarifa_id = False
        if tarifa_id:
            tipus = lectura.tipus == 'activa' and 'A' or 'R'

            search_vals = [('comptador', '=', lectura.comptador),
                           ('periode.name', '=', lectura.name[-3:-1]),
                           ('periode.tarifa', '=', tarifa_id[0]),
                           ('tipus', '=', tipus),
                           ('name', 'in', [lectura.data_actual,
                                           lectura.data_anterior])]
            lect_ids = lectura_obj.search(cursor, uid, search_vals)
            lect_vals = lectura_obj.read(cursor, uid, lect_ids,
                                         ['name', 'origen_comer_id', 'origen_id'])

            for lect in lect_vals:
                # En funció dels origens, escrivim el text
                # Si Estimada (40)
                # La resta: Real
                origen_txt = _(u"real")
                if lect['origen_id'][0] == estimada_id:
                    origen_txt = _(u"estimada")

                res[lect['name']] = origen_txt

        return res

    historic_sql = """
    SELECT * FROM (
        SELECT mes AS mes,
        periode AS periode,
        sum(suma_fact) AS facturat,
        sum(suma_consum) AS consum,
        min(data_ini) AS data_ini,
        max(data_fin) AS data_fin
        FROM (
            SELECT f.polissa_id AS polissa_id,
                   to_char(f.data_inici, 'YYYY/MM') AS mes,
                   pt.name AS periode,
                   COALESCE(SUM(il.quantity*(fl.tipus='energia')::int*(CASE WHEN i.type='out_refund' THEN -1 ELSE 1 END)),0.0) as suma_consum,
                   COALESCE(SUM(il.price_subtotal*(fl.tipus='energia')::int*(CASE WHEN i.type='out_refund' THEN -1 ELSE 1 END)),0.0) as suma_fact,
                   MIN(f.data_inici) data_ini,
                   MAX(f.data_final) data_fin
                   FROM
                        giscedata_facturacio_factura f
                        LEFT JOIN account_invoice i on f.invoice_id = i.id
                        LEFT JOIN giscedata_facturacio_factura_linia fl on f.id=fl.factura_id
                        LEFT JOIN account_invoice_line il on il.id=fl.invoice_line_id
                        LEFT JOIN product_product pp on il.product_id=pp.id
                        LEFT JOIN product_template pt on pp.product_tmpl_id=pt.id
                   WHERE
                        fl.tipus = 'energia' AND
                        f.polissa_id = %(p_id)s AND
                        f.data_inici <= %(data_inicial)s AND
                        f.data_inici >= date_trunc('month', date %(data_final)s) - interval '14 month'
                        AND (fl.isdiscount IS NULL OR NOT fl.isdiscount)
                        AND i.type in('out_invoice', 'out_refund')
                   GROUP BY
                        f.polissa_id, pt.name, f.data_inici
                   ORDER BY f.data_inici DESC ) AS consums
        GROUP BY polissa_id, periode, mes
        ORDER BY mes DESC, periode ASC
    ) consums_ordenats
    ORDER BY mes ASC"""

    # Repartiment segons BOE
    rep_BOE = {'i': 36.28, 'c': 32.12 ,'o': 31.60}
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_catral/report/extras/pie.css"/>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_catral/report/extras/consum.css"/>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_catral/report/factura.css"/>
        <script src="${addons_path}/giscedata_facturacio_comer/report/assets/d3.min.js"></script>
        <style>
            @font-face {
                font-family: "Roboto-Regular";
                src: url("${assets_path}/fonts/Roboto/Roboto-Regular.ttf") format('truetype');
                font-weight: normal;
            }
        </style>
    </head>
    <body>
        %for factura in objects:
            <%
                # Invoice detail related information
                detailed_lines = report_o.get_lines_detailed_info(cursor, uid, factura.id)[factura.id]
                energy_lines = detailed_lines['energy']
                power_lines = detailed_lines['power']
                reactive_lines = detailed_lines['reactive']
## _____________________________________________________________________________
## -----------------------------------------------------------------------------

                if factura.lang_partner not in ['es_ES', 'ca_ES']:
                    factura_lang = 'es_ES'
                else:
                    factura_lang = factura.lang_partner
                setLang(factura_lang)

                polissa = polissa_obj.browse(cursor, uid, factura.polissa_id.id,
                                             context={'date': factura.data_final.val})

                tarifa_elect_comer = polissa.llista_preu.id
                preu_unic = polissa.llista_preu.unique_price
                contracte_dacces = polissa.ref_dist or factura.polissa_id.ref_dist or ''

                dphone = polissa.distribuidora.address[0].phone
                cphone = factura.company_id.partner_id.address[0].phone
                distri_phone = ' '.join([dphone[i:i+3] for i in range(0,len(dphone),3)])
                comer_phone = ' '.join([cphone[i:i+3] for i in range(0,len(cphone),3)])

                cursor.execute(
                    historic_sql,
                    {
                        'p_id':factura.polissa_id.id,
                        'data_inicial': factura.data_inici,
                        'data_final': factura.data_final
                    }
                )
                historic = cursor.dictfetchall()

                historic_graf = {}
                periodes_graf = []

                for row in historic:
                    historic_graf.setdefault(row['mes'],{})
                    historic_graf[row['mes']].update({row['periode']: row['consum']})
                    periodes_graf.append(row['periode'])

                periodes_graf = list(set(periodes_graf))
                consums_mig = dict.fromkeys(list(set(periodes_graf)), 0)
                consums_mig.update({'total':0})
                historic_js = []
                for mes, consums in historic_graf.items():
                    p_js = {'mes': mes}
                    for p in periodes_graf:
                        if not preu_unic:
                            p_js.update({p: consums.get(p,0.0)})
                        else:
                            p1_val = p_js.get('P1')
                            if p1_val:
                                p_js['P1'] = p1_val + consums.get(p,0.0)
                            else:
                                p_js['P1'] = consums.get(p,0.0)
                        consums_mig[p] += consums.get(p,0.0)
                    historic_js.append(p_js)
                    consums_mig['total'] += 1

                consums_mig = dict((k, v/consums_mig['total']) for k, v in consums.items() if k.startswith('P'))
                consums_mig = sorted(consums_mig.items(),key=itemgetter(1),reverse=True)

                total_historic_kw = sum([h['consum'] for h in historic])
                total_historic_eur = sum([h['facturat'] for h in historic])
                data_ini = min([h['data_ini'] for h in historic])
                data_fin = max([h['data_fin'] for h in historic])
                historic_dies = 1 + (datetime.strptime(data_fin, '%Y-%m-%d') - datetime.strptime(data_ini, '%Y-%m-%d')).days

                mes_any_inicial = (datetime.strptime(factura.data_inici,'%Y-%m-%d') - timedelta(days=365)).strftime("%Y/%m")
                total_any = sum([h['consum'] for h in historic if h['mes'] > mes_any_inicial])

                iese_lines = [l for l in factura.tax_line if 'IVA' not in l.name]
                iva_lines = [l for l in factura.tax_line if 'IVA' in l.name]

                impostos = {}
                for l in factura.tax_line:
                    impostos.update({l.name: l.amount})

                iese = sum([v for k, v in impostos.items() if 'IVA' not in k])

                lloguer_lines = [l for l in factura.linia_ids if l.tipus in 'lloguer']
                altres_lines = [l for l in factura.linia_ids if l.tipus in 'altres'
                                and l.invoice_line_id.product_id.code != 'DN01']
                total_altres = sum([l.price_subtotal for l in altres_lines])
                periodes = dict([('P%s' % p,'') for p in range(1, 7)])
                periodes_a = sorted(list(set([lectura.name[-3:-1]
                                            for lectura in factura.lectures_energia_ids
                                            if lectura.tipus == 'activa'])))

                periodes_r = sorted(list(set([lectura.name[-3:-1]
                                            for lectura in factura.lectures_energia_ids
                                            if lectura.tipus == 'reactiva'])))

                periodes_m = sorted(list(set([lectura.name
                                              for lectura in factura.lectures_potencia_ids])))

                potencies_periode = dict([('P%s' % p,'') for p in range(1, 7)])
                for pot in polissa.potencies_periode:
                    potencies_periode[pot.periode_id.name] = pot.potencia

                fact_pot_txt = ((polissa.facturacio_potencia=='max' or len(periodes_a)>3)
                                    and _(u"facturación por maxímetro")
                                 or _(u"facturación por ICP"))

                # lectures (activa) ordenades per comptador i període
                lectures_a = {}
                lectures_r = {}
                lectures_m = []

                # Detall factures per excés potència

                exces_potencia = get_exces_potencia(cursor, uid, factura)

                for lectura in factura.lectures_energia_ids:
                    origens = get_origen_lectura(cursor, uid, lectura)
                    if lectura.tipus == 'activa':
                        lectures_a.setdefault(lectura.comptador,[])
                        lectures_a[lectura.comptador].append((lectura.name[-3:-1],
                                                            lectura.lect_anterior,
                                                            lectura.lect_actual,
                                                            lectura.consum,
                                                            lectura.data_anterior,
                                                            lectura.data_actual,
                                                            origens[lectura.data_anterior],
                                                            origens[lectura.data_actual],
                                                            ))
                    elif lectura.tipus == 'reactiva':
                        lectures_r.setdefault(lectura.comptador,[])
                        lectures_r[lectura.comptador].append((lectura.name[-3:-1],
                                                            lectura.lect_anterior,
                                                            lectura.lect_actual,
                                                            lectura.consum,
                                                            lectura.data_anterior,
                                                            lectura.data_actual,
                                                            origens[lectura.data_anterior],
                                                            origens[lectura.data_actual],
                                                            ))

                for lectura in factura.lectures_potencia_ids:
                    lectures_m.append((lectura.name, lectura.pot_contract,
                                       lectura.pot_maximetre ))

                for lects in lectures_a.values():
                    sorted(lectures_a, key=lambda x: x[0])

                for lects in lectures_r.values():
                    sorted(lectures_r, key=lambda x: x[0])

                for lects in lectures_m:
                    sorted(lectures_m, key=lambda x: x[0])

                total_lectures_a = dict([(p, 0) for p in periodes_a])
                total_lectures_r = dict([(p, 0) for p in periodes_r])
                fact_potencia = dict([(p,0) for p in periodes_m])

                dies_factura = 1 + (datetime.strptime(factura.data_final, '%Y-%m-%d') - datetime.strptime(factura.data_inici, '%Y-%m-%d')).days
                diari_factura_actual_eur = factura.total_energia / (dies_factura or 1.0)
                diari_factura_actual_kwh = (factura.energia_kwh * 1.0) / (dies_factura or 1.0)

                for c, ls in lectures_a.items():
                    for l in ls:
                        total_lectures_a[l[0]] += l[3]

                for c, ls in lectures_r.items():
                    for l in ls:
                        total_lectures_r[l[0]] += l[3]

                for p in [(p.product_id.name, p.quantity) for p in factura.linies_potencia]:
                    fact_potencia.update({p[0]: max(fact_potencia.get(p[0], 0), p[1])})

                #comprovem si té alguna lectura de maxímetre
                te_maximetre = (max([l[2] for l in lectures_m] or (0,)) > 0)


                # DADES PIE CHART
                pie_total = factura.amount_total - factura.total_lloguers
                pie_regulats = (factura.total_atr + total_altres)
                pie_impostos = float(factura.amount_tax)
                pie_costos = (pie_total - pie_regulats - pie_impostos )

                reparto = { 'i': ((pie_regulats * rep_BOE['i'])/100),
                            'c': ((pie_regulats * rep_BOE['c'])/100),
                            'o': ((pie_regulats * rep_BOE['o'])/100)
                           }

                dades_reparto = [
                    [[0, rep_BOE['i']], 'i', _(u"Incentivos a las energias renovables, cogeneración y residuos"), formatLang(reparto['i'])],
                     [[rep_BOE['i'] , rep_BOE['i'] + rep_BOE['c']], 'c', _(u"Coste de las redes de distribiución y transporte"), formatLang(reparto['c'])] ,
                     [[rep_BOE['i'] + rep_BOE['c'], 100.00], 'o', _(u"Otros costes regualdos (incluida la anualidad del déficit)"), formatLang(reparto['o'])]
                    ]
                company_address = factura.company_id.partner_id.address[0]
                titular_address = polissa.titular.address
                titular_street = ""
                titular_zip = ""
                titular_municipi = ""
                if titular_address:
                    titular_address = titular_address[0]
                    titular_street = titular_address.street.replace('..', '.')
                    titular_zip = titular_address.zip
                    titular_municipi = titular_address.id_municipi.name

                reverse = False
                if context.get('send_by_email', False):
                    reverse = banner_o.get_banner(
                        cursor, uid, 'giscedata.facturacio.factura',
                        factura.data_final.val, code='catral_revers', model_id=factura.id
                    )
            %>

            <div id="lateral-info" class="center">
                ${company.rml_footer2 or ''}
            </div>
            <div id="page">

            <table id="header">
                <colgroup>
                    <col width="60%"/>
                    <col>
                </colgroup>
                <tbody>
                    <tr>
                        <td rowspan="4"><img src="data:image/jpeg;base64,${company.logo}"/></td>
                        <td>${factura.company_id.partner_id.name}</td>
                    </tr>
                    <tr>
                        <td>${clean_nif(factura.company_id.partner_id.vat)}</td>
                    </tr>
                    <tr>
                        <td>${company_address.street.replace('..', '.')}</td>
                    </tr>
                    <tr>
                        <td>${company_address.zip}, ${company_address.id_poblacio.name} (${company_address.state_id.name})</td>
                    </tr>
                </tbody>
            </table>
            <!-- TITULAR -->
            <div class="left-50">
                <table class="styled-table">
                    <thead>
                        <tr>
                            <th colspan="4">${_(u"TITULAR")}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="bold">${_(u"Nombre/Razón social")}</td>
                            <td>${polissa.titular.name}</td>
                        </tr>
                        <tr>
                            <td class="bold">${_(u"NIF/CIF")}</td>
                            <td>${clean_nif(polissa.titular.vat)}</td>
                        </tr>
                        <tr>
                            <td class="bold">${_(u"Dirección")}</td>
                            <td colspan="3">${titular_street} ${titular_zip} ${titular_municipi}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- END TITULAR -->

            <!-- PART SUPERIOR -->
            <div class="right-50 top-30">
                <table class="w100p postal">
                    <tr>
                        <td>${factura.address_contact_id.name}</td>
                    </tr>
                    <tr>
                        <td>${factura.address_contact_id.street.replace('..', '.')}</td>
                    </tr>
                    <tr>
                        <td>${factura.address_contact_id.zip} ${factura.address_contact_id.id_municipi.name}</td>
                    </tr>
                    <tr>
                        <td>${factura.address_contact_id.state_id.name}</td>
                    </tr>
                </table>
            </div>
            <!-- END PART SUPERIOR -->
            <!-- CONTRATO  -->
            <div class="left-50">
                <table class="styled-table">
                    <colgroup>
                        <col width="30%"/>
                        <col width="32%"/>
                        <col width="20%"/>
                        <col/>
                    </colgroup>
                    <thead>
                        <tr>
                            <th colspan="4">${_(u"FACTURA")}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <% invoice_number = factura.number if factura.number else '' %>
                            <td class="bold">${_(u"Número de factura")}</td>
                            <td>${invoice_number}</td>
                            <td class="bold">${_(u"Fecha emisión")}</td>
                            <td>${factura.date_invoice}</td>
                        </tr>
                        %if factura.tipo_rectificadora in ['B', 'A', 'BRA']:
                            <% invoice_ref = factura.ref.number if factura.ref.number else ''%>
                            <tr>
                                <td></td>
                                <td colspan="2">${_('Esta factura anula la {}').format(invoice_ref)}</td>
                            </tr>
                        %endif
                        %if factura.tipo_rectificadora in ['R', 'RA']:
                            <% invoice_ref = factura.ref.number if factura.ref.number else ''%>
                            <tr>
                                <td></td>
                                <td>${_('Esta factura rectifica la {}').format(invoice_ref)}</td>
                            </tr>
                        %endif
                        <tr>
                            <td colspan="2" class="bold">${_(u"Periodo de facturación")}</td>
                            <td colspan="2">${"{} - {}".format(factura.data_inici, factura.data_final)}</td>
                        </tr>
                        <tr>
                            <td class="bold">${_(u"Vencimiento")}</td>
                            <td>${factura.date_due}</td>
                            <td class="bold">${_(u"Días")}</td>
                            <td>${dies_factura}</td>
                        </tr>
                        <tr>
                            <td class="bold">${_(u"Pagador")}</td>
                            <td colspan="3">${factura.partner_id.name}</td>
                        </tr>
                        <tr>
                            <td class="bold">${_(u"NIF")}</td>
                            <td>${clean_nif(factura.partner_id.vat)}</td>
                        </tr>
                        %if factura.partner_bank:
                            <tr>
                                <td class="bold">${_(u"Entitad")}</td>
                                <td colspan="3">${factura.partner_bank.bank.name}</td>
                            </tr>
                            <tr>
                                <td class="bold">${_(u"IBAN")}</td>
                                <td colspan="3">**** **** **** **** **** ${factura.partner_bank.printable_iban[-4:]}</td>
                            </tr>
                        %endif
                    </tbody>
                </table>
                <table id="resum-factura" class="styled-table">
                    <thead>
                        <tr>
                            <th colspan="2">${_(u"RESUMEN FACTURA")}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="bold">${_(u"Por potencia")}</td>
                            <td class="bold right">${"{}&euro;".format(formatLang(power_lines['total']))}</td>
                        </tr>
                        <tr>
                            <td class="bold">${_(u"Por energía")}</td>
                            <td class="bold right">${"{}&euro;".format(formatLang(energy_lines['total']))}</td>
                        </tr>
                        % if factura.total_reactiva > 0:
                            <tr>
                                <td class="bold">${_("Penalización por energía reactiva")}</td>
                                <td class="bold right">${"{}&euro;".format(formatLang(reactive_lines['total']))}</td>
                            </tr>
                        % endif
                            <tr class="row-separator">
                                <td class="bold">${_(u"Impuesto electricidad")}</td>
                                <td class="bold right">${"{}&euro;".format(formatLang(iese))}</td>
                            </tr>
                            <tr>
                                <td class="bold">${_(u"Alquiler contador")}</td>
                                <td class="bold right">${"{}&euro;".format(formatLang(factura.total_lloguers))}</td>

                            </tr>
                        % if total_altres != 0:
                            <tr>
                                <td class="bold">${_("Otros conceptos")}</td>
                                <td class="bold right">${"{}&euro;".format(formatLang(total_altres))}</td>
                            </tr>
                        % endif
                        % for n, v in impostos.items():
                            % if "IVA" in n:
                                <tr>
                                    <td class="bold">${n}</td>
                                    <td class="bold right">${"{}&euro;".format(formatLang(v))}</td>
                                </tr>
                            % endif
                        % endfor
                        <tr class="row-separator">
                            <td class="bold">${_(u"TOTAL")}</td>
                            <td class="bold right">${"{}&euro;".format(formatLang(factura.amount_total))}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="right-50">
                <table class="styled-table">
                    <thead>
                        <tr>
                            <th colspan="4">${_(u"CONTRATO")}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="bold">${_(u"Contrato de suministro")}</td>
                            <td>${factura.polissa_id.name}</td>
                            <td class="bold">${_(u"Contracto de acceso")}</td>
                            <td>${contracte_dacces}</td>
                        </tr>
                        <tr>
                            <td rowspan="2" class="bold">${_(u"Fecha de alta")}</td>
                            <td rowspan="2">${formatLang(polissa.data_alta, date=True)}</td>
                            <td class="bold">${_(u"Fecha final")}</td>
                            <td>${formatLang(polissa.modcontractual_activa.data_final, date=True)}</td>
                        </tr>
                        <tr>
                            <td colspan="2" class="little">${_("(renovación anual automática)")}</td>
                        </tr>
                        <tr>
                            <td class="bold">${_(u"Tarifa")}</td>
                            <td>${factura.tarifa_acces_id.name}</td>
                            <td class="bold">${_(u"CNAE")}</td>
                            <td>${polissa.cnae.name}</td>
                        </tr>
                        <tr>
                            <td class="bold">${_(u"Producto Comer")}</td>
                            <td colspan="3">${polissa.llista_preu.name}</td>
                        </tr>
                        <tr>
                            <td class="bold">${_(u"CUPS")}</td>
                            <td colspan="3">${factura.cups_id.name}</td>
                        </tr>
                        <tr>
                            <td class="bold">${_(u"Distribuidora")}</td>
                            <td colspan="3">${polissa.distribuidora.name}</td>
                        </tr>
                        <tr>
                            <td class="bold">${_(u"Dirección de suministro")}</td>
                            <td colspan="3">${factura.cups_id.direccio}</td>
                        </tr>

                    </tbody>
                </table>
                <table class="styled-inner-table">
                    <%
                        potencies_contractades = []
                        potencia_contractada_titol = _(u"Potencia contratada: {} (kW)").format(_(u"Maxímetro") if te_maximetre else _(u"ICP"))

                        for periode, potencia in sorted(potencies_periode.items(),key=itemgetter(0)):
                            if potencia:
                                potencies_contractades.append((periode, potencia))
                    %>
                    <thead>
                        <tr>
                           <th class="center" colspan="${len(potencies_contractades)}">${potencia_contractada_titol}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            % for periode, potencia in potencies_contractades:
                                <td class="bold center">${periode}</td>
                            % endfor
                        </tr>
                        <tr>
                            % for periode, potencia in potencies_contractades:
                                 <td class="center">${formatLang(potencia, digits=3)}</td>
                            % endfor
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- END CONTRATO -->
            <!-- LECTURES -->
            <table id="consum-electric" class="styled-table">
                <thead>
                    <tr>
                        <th></th>
                        <th>${_(u"CONSUMO ELECTRICO")}</th>
                        % for period_name in periodes_a:
                            <th>${period_name}</th>
                        % endfor
                    </tr>
                </thead>
                <tbody>
                    <!-- ACTIVA -->
                    %if lectures_a:
                        <tr>
                            <% rowspan_vertical_header = len(lectures_a) * 3 + 2  %>
                            <th rowspan="${rowspan_vertical_header}" class="rotate h65px"><div>${_("ENERGIA")}</div></th>
                        </tr>
                        % for comptador in sorted(lectures_a):
                            <tr>
                                <td class="bold">${_(u"Número de contador")}</td>
                                % for period_name in periodes_a:
                                    <td class="center">${comptador}</td>
                                % endfor
                            </tr>
                            <tr>
                                <td class="bold">${_(u"Lectura anterior")} ${lectures_a[comptador][0][4]} (${lectures_a[comptador][0][6]})</td>
                                % for i in range(len(periodes_a)):
                                    <td class="right">${formatLang(int(lectures_a[comptador][i][1]),digits=0)} kWh</td>
                                % endfor
                            </tr>
                            <tr>
                                <td class="bold">${_(u"Lectura actual")} ${lectures_a[comptador][0][5]} (${lectures_a[comptador][0][7]})</td>
                                % for i in range(len(periodes_a)):
                                    <td class="right">${formatLang(int(lectures_a[comptador][i][2]),digits=0)} kWh</td>
                                % endfor
                            </tr>
                        % endfor
                        <tr>
                            <td class="bold">${_(u"Total periodo")}</td>
                            % for period_name in periodes_a:
                                <td class="right">${formatLang(total_lectures_a[period_name], digits=0)} kWh</td>
                            %endfor
                        </tr>
                    %endif
                    <!-- ACTIVA -->
                    <!-- REACTIVA -->
                    % if len(periodes_r) or te_maximetre:
                        <tr>
                            <% rowspan_vertical_header = len(lectures_r) * 3 + 2  %>
                            <th rowspan="${rowspan_vertical_header}" class="rotate h65px"><div>${_("REACTIVA")}</div></th>
                        </tr>
                        % for comptador in sorted(lectures_r):
                            <tr class="row-separator">
                                <td class="bold">${_(u"Número de contador")}</td>
                                % for period_name in periodes_r:
                                        <td class="center">${comptador}</td>
                                % endfor
                            </tr>
                            <tr>
                                <td class="bold">${_(u"Lectura anterior")} ${lectures_r[comptador][0][4]} (${lectures_r[comptador][0][6]})</td>
                                % for i in range(len(periodes_r)):
                                    <td class="right">${formatLang(int(lectures_r[comptador][i][1]),digits=0)} kWh</td>
                                % endfor
                            </tr>
                            <tr>
                                <td class="bold">${_(u"Lectura actual")} ${lectures_r[comptador][0][5]} (${lectures_r[comptador][0][7]})</td>
                                % for i in range(len(periodes_r)):
                                    <td class="right">${formatLang(int(lectures_r[comptador][i][2]),digits=0)} kWh</td>
                                % endfor
                            </tr>
                        % endfor
                        <tr>
                            <td class="bold">${_(u"Total periodo")}</td>
                            % for period_name in periodes_r:
                                <td class="right">${formatLang(total_lectures_r[period_name], digits=0)} kWh</td>
                            % endfor
                        </tr>
                    %endif
                    <!-- REACTIVA -->
                    <!-- MAXIMETRE -->
                    % if te_maximetre:
                        <tr class="header">
                            <th rowspan="4" class="rotate h65px"> <div>${_("MAXIMETRO")}</div></th>
                        </tr>
                        <tr class="row-separator">
                            <td class="bold">${_(u"Potencia contratada")}</td>
                            % for lec in lectures_m:
                                <td class="right">${formatLang(lec[1], digits=3)} kW</td>
                            % endfor
                        </tr>
                        <tr>
                            <td class="bold">${_(u"Lectura maximetro")}</td>
                            % for lec in lectures_m:
                                <td class="right">${formatLang(lec[2], digits=3)} kW</td>
                            % endfor
                        </tr>
                        <tr>
                            <td class="bold">${_(u"Potencia facturada periodo")}</td>
                            % for period_name in periodes_m:
                                <td class="right">${formatLang(fact_potencia[period_name],digits=3)} kW</td>
                            % endfor
                        </tr>
                    %endif
                    <!-- MAXIMETRE -->
                </tbody>
                <tfoot>
                    <tr>
                        <% colspan =  len(periodes_a) + 2 %>
                        <td colspan="${colspan}" class="center">${_(u"Su consumo medio diario en el periodo facturado ha sido {} € que corresponden  a {} kWh/día ({} días)").format(formatLang(diari_factura_actual_eur), formatLang(diari_factura_actual_kwh), dies_factura or 1)}</td>
                    </tr>
                </tfoot>
            </table>
            <!-- END LECTURES -->
            <!-- HISTORIC -->
            <div class="left-50-clear">
                <%
                    mesos = int(historic_dies*1.0 / 30)
                    dies = historic_dies or 1.0
                    consum_preu = formatLang((total_historic_eur * 1.0) / (historic_dies or 1.0))
                    kwh_dia = formatLang((total_historic_kw * 1.0) / (historic_dies or 1.0))
                %>
                <table class="styled-table">
                    <thead>
                        <tr>
                            <th>${_(u"HISTORIAL ULTIMOS 14 MESES")}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="background-color">
                                <div class="chart_consum" id="chart_consum_${factura.id}"></div>
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td>
                                ${(_(u"Su consumo medio diario los últimos {} meses ({} días) ha sido {} € que corresponden a {} kWh/día ").format(mesos, dies, consum_preu, kwh_dia))}<br/>
                                ${_(u"Su consumo acumulado el último año ha sido {} kWh").format(formatLang(total_any, digits=0))}
                            </td>
                        </tr>
                    </tfoot>
                </table>
                <% last_div_classes = 'emergency-complaints' %>
                %if '2.' in factura.tarifa_acces_id.name:
                    <!-- DESTI -->
                    <% last_div_classes += ' last-tables-on-bottom' %>
                    <table id="desti-factura" class="styled-table">
                        <thead>
                            <tr>
                                <th class="seccio">${_(u"DESTINO DE LA FACTURA")}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>${_(u"El destino del importe de su factura ({} euros) es el siguiente:").format(formatLang(factura.amount_total))}</td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="chart_desti" id="chart_desti_${factura.id}"></div>
                                </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td class="italic">${_(u"A los importes indicados en el diagrama se les tiene que agregar, en su caso, el alquiler de los equipos de medidad y control")}</td>
                            </tr>
                        </tfoot>
                    </table>
                    <!-- END DESTI -->
                %else:
                    <% last_div_classes += ' last-tables-next-to-detall-factura' %>
                %endif
            </div>
            <!-- END HISTORIC -->
            <!-- LECTURES ACTIVA i GRÀFIC BARRES -->
            <!-- DETALLE FACTURA -->
            <div class="right-50">
                <table id="detall-factura" class="styled-table center-th">
                    <colgroup>
                        <col width="27%" class="background-color"/>
                        <col width="25%"/>
                        <col/>
                        <col width="18%"/>
                    </colgroup>
                    <thead>
                        <tr>
                            <th colspan="4">${_(u"DETALLE FACTURA")}</th>
                        </tr>
                    </thead>
                    <tbody>
                       <tr>
                            <td colspan="4">
                                <!-- POTENCIA -->
                                <table class="styled-inner-table">
                                    <colgroup>
                                        <col width="24%"/>
                                        <col width="17%"/>
                                        <col width="22%"/>
                                        <col/>
                                        <col/>
                                        <col width="22%"/>
                                    </colgroup>
                                    <thead>
                                        <%
                                            fact_messos = 'mes' in polissa.property_unitat_potencia.name
                                            unitat_temps = _(u"mes") if fact_messos else _(u"dia")
                                            preu_unitat = "(€/kW {})".format(unitat_temps)
                                        %>
                                        <tr>
                                            <th>${_(u"Término de potencia")}</th>
                                            <th>${_(u"Consumo")} (kW)</th>
                                            <th>${_(u"Precio")} ${preu_unitat} </th>
                                            % if fact_messos:
                                                <th>${_(u"Meses")}</th>
                                            %else:
                                                <th>${_(u"Días")}</th>
                                            %endif
                                            <th colspan="2">${_(u"Total")} (€)</th>
                                        </tr>
                                    </thead>
                                    <%
                                    total_toll = 0.0
                                    %>
                                    <tbody>
                                        % for power_line in power_lines['lines']:
                                            <tr>
                                                <td class="bold center white">${power_line['name']}</td>
                                                <td class="right white">${formatLang(power_line['consumption'], digits=3)}</td>
                                                <td class="right white">${formatLang(power_line['price'], digits=6)}</td>
                                                <td class="center white">${int(power_line['time'])}</td>
                                                <td class="right white" colspan="2">${formatLang(power_line['total'])}</td>
                                                ## <td class="left white italic toll">(${_(u"peatges: {}").format(formatLang(power_line['toll']))})</td>
                                                <%
                                                total_toll += power_line['toll']
                                                %>
                                            </tr>
                                        % endfor
                                        <tr>
                                            <td colspan="6" class="bold right white">${formatLang(power_lines['total'])}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <!-- ENERGIA -->
                                <table class="styled-inner-table">
                                    <colgroup>
                                        <col class="detall-factura-inner-table-col-1"/>
                                        <col class="detall-factura-inner-table-col-2"/>
                                        <col class="detall-factura-inner-table-col-3"/>
                                        <col class="detall-factura-inner-table-col-4"/>
                                        <!--<col class="detall-factura-inner-table-col-5"/>-->
                                    </colgroup>
                                    <thead>
                                        <tr>
                                            <th>${_(u"Término de energía")}</th>
                                            <th>${_(u"Consumo")} (kWh)</th>
                                            <th>${_(u"Precio")} (€/kWh)</th>
                                            <th colspan="2">${_(u"Total")} (€)</th>
                                         </tr>
                                    </thead>
                                    <tbody>
                                            % for energy_line in energy_lines['lines']:
                                                <tr>
                                                    <td class="bold center">${energy_line['name']}</td>
                                                    <td class="right">${int(energy_line['consumption'])}</td>
                                                    <td class="right">${formatLang(energy_line['price'], digits=6)}</td>
                                                    <td class="right" colspan="2">${formatLang(energy_line['total'])}</td>
                                                    ##<td class="left white toll italic">(${_(u"peajes: {}").format(formatLang(energy_line['toll']))})</td>
                                                    <%
                                                    total_toll += energy_line['toll']
                                                    %>
                                                </tr>
                                            % endfor
                                        <tr>
                                            <td colspan="5" class="bold right">${formatLang(energy_lines['total'])}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        %if factura.total_reactiva:
                            <tr>
                                <td colspan="4">
                                    <!-- ENERGIA REACTIVA  -->
                                    <table class="styled-inner-table">
                                        <colgroup>
                                            <col class="detall-factura-inner-table-col-1"/>
                                            <col class="detall-factura-inner-table-col-2"/>
                                            <col class="detall-factura-inner-table-col-3"/>
                                            <col class="detall-factura-inner-table-col-4"/>
                                            <col class="detall-factura-inner-table-col-5"/>
                                        </colgroup>
                                        <thead>
                                            <tr>
                                                <th>${_(u"Energía reactiva")}</th>
                                                <th>${_(u"Exceso")} (kVArh)</th>
                                                <th>${_(u"Precio")} (€/kVArh)</th>
                                                <th colspan="2">${_(u"Total")} (€)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            % for reactive_line in reactive_lines['lines']:
                                                <tr>
                                                    <td class="bold center">${reactive_line['name']}</td>
                                                    <td class="right">${formatLang(reactive_line['consumption'])}</td>
                                                    <td class="right">${formatLang(reactive_line['price'], digits=6)}</td>
                                                    <td class="right" colspan="2">${formatLang(reactive_line['total'])}</td>
                                                    ##<td class="left white toll italic">(${_(u"peajes: {}").format(formatLang(reactive_line['toll']))})</td>
                                                    <%
                                                    total_toll += reactive_line['toll']
                                                    %>
                                                </tr>
                                            % endfor
                                            <tr>
                                                <td colspan="5" class="bold right">${formatLang(reactive_lines['total'])}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        %endif
                        %if len(exces_potencia):
                            <tr>
                                <td colspan="4">
                                    <!-- EXCES POTENCIA -->
                                    <table class="styled-inner-table">
                                        <colgroup>
                                            <col class="detall-factura-inner-table-col-1"/>
                                            <col class="detall-factura-inner-table-col-2"/>
                                            <col class="detall-factura-inner-table-col-3"/>
                                            <col class="detall-factura-inner-table-col-4"/>
                                        </colgroup>
                                        <thead>
                                            <tr>
                                                <th>${_(u"Exceso potencia")}</th>
                                                <th>${_(u"Exceso")} (kVArh)</th>
                                                <th>${_(u"Precio")} (€/kVArh)</th>
                                                <th>${_(u"Total")} (€)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <% total_exces = 0.0 %>
                                            % for l in sorted(sorted(exces_potencia, key=attrgetter('data_desde')), key=attrgetter('name')):
                                                <tr>
                                                    <td class="bold center">${l.name}</td>
                                                    <td class="right">${formatLang(l.quantity)}</td>
                                                    <td class="right">${formatLang(l.price_unit_multi, digits=6)}</td>
                                                    <td class="right">${u"{} €".format(formatLang(l.price_subtotal))}</td>
                                                </tr>
                                                <% total_exces += l.price_subtotal %>
                                            % endfor
                                            <tr>
                                                <td colspan="4" class="bold right">${formatLang(total_exces)}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        %endif
                        % for l in iese_lines:
                            <tr>
                                <th>${_(u"Impuesto de la electricidad")}</th>
                                <td class="right">${u"{} €".format(formatLang(l.base_amount))}</td>
                                <td class="right">${u"5,11269632%"}</td>
                                <td class="bold right">${u"{} €".format(formatLang(l.tax_amount))}</td>
                            </tr>
                        % endfor
                        % for l in lloguer_lines:
                            <tr class="row-separator">
                                <th>${_(u"Alquiler contador")}</th>
                                <td></td>
                                <td></td>
                                <td class="bold right">${u"{} €".format(formatLang(l.price_subtotal))}</td>
                            </tr>
                        % endfor
                        %if altres_lines:
                            <tr class="row-separator">
                                <% rowspan = len(altres_lines) + 1 %>
                                <th rowspan="${rowspan}">${_(u"Otros")}</th>
                                <td colspan="3"></td>
                            </tr>
                            % for l in altres_lines:
                                <tr>
                                    <td colspan="2" class="altres-cell">${l.name}</td>
                                    <td class="bold right">${u"{} €".format(formatLang(l.price_subtotal))}</td>
                                </tr>
                            % endfor
                        %endif
                        % for l in iva_lines:

                            <tr class="row-separator">
                                <th>${l.name}</th>
                                <td class="right">${u"{} €".format(formatLang(l.base))}</td>
                                <td class="right">${l.name.replace('IVA','').strip()}</td>
                                <td class="bold right">${u"{} €".format(formatLang(l.amount))}</td>
                            </tr>
                        % endfor
                        <tr class="row-separator">
                            <th>${_(u"TOTAL FACTURA")}</th>
                            <td></td>
                            <td></td>
                            <td class="bold right">${u"{} &euro;".format(formatLang(factura.amount_total))}</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="4" class="white bold cursiva">${_(u"Los precios de los términos de peaje de acceso son los publicados en la ORDEN ETU/1976/2016")}</td>
                        </tr>
                        <tr>
                            <td colspan="4" class="white bold cursiva">${_(u"Los precios del alquiler de los contadores son los establecidos en la ORDEN IET/1491/2013")}</td>
                        </tr>
                        <tr>
                            <td colspan="4" class="white bold cursiva">${_(u"El importe de su facturación de acceso que nos repercute el distribuidor, ha sido de {0} € (Iptos excluidos).").format(formatLang(total_toll))}</td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <!-- END DETALL -->
            <div class="${last_div_classes}">
                <table class="styled-table-with-caption">
                    <caption>${_(u"AVERIAS Y URGENCIAS")}</caption>
                    <colgroup>
                        <col width="40%"/>
                    </colgroup>
                    <tbody>
                        <tr>
                            <th>${_(u"Empresa distribuidora")}</th>
                            <td>${polissa.distribuidora.name}</td>
                        </tr>
                        <tr>
                            <th>${_(u"Teléfono averias")}</th>
                            <td>${distri_phone}</td>
                        </tr>
                        <tr>
                            <th>${_(u"Contrato de acceso")}</th>
                            <td>${contracte_dacces}</td>
                        </tr>
                    </tbody>
                </table>
                <table class="styled-table-with-caption ">
                    <caption>${_(u"ATENCIÓN AL CLIENTE")}</caption>
                    <colgroup>
                        <col width="40%"/>
                    </colgroup>
                    <tbody>
                        <tr>
                            <th>${_(u"Empresa comercializadora")}</th>
                            <%
                                fit_comer_name = ""
                                if len(factura.company_id.partner_id.name) > 40:
                                    fit_comer_name = 'class="fit-comer-name"'
                            %>
                            <td ${fit_comer_name}>${factura.company_id.partner_id.name}</td>
                        </tr>
                        <tr>
                            <th>${_(u"Teléfono atención")}</th>
                            <td>${comer_phone}</td>
                        </tr>
                        <tr>
                            <th>${_(u"Email")}</th>
                            <td>${factura.company_id.partner_id.address[0].email}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="small-styled">
                ${_("El pago de la factura se justificará con el recibo o el cargo en nuestra cuenta bancaria.")}
                ${_("Su pago no presupono la liquidación de las anteriores.")}
            </div>
            <!-- END PAGE -->
            <script>
                var pie_total = ${pie_total};
                var pie_data = [{val: ${pie_regulats}, perc: 30, code: "REG"},
                                {val: ${pie_costos}, perc: 55, code: "PROD"},
                                {val: ${pie_impostos},  perc: 15 ,code: "IMP"}
                               ];

                var pie_etiquetes = {'REG': {t: ['${formatLang(float(pie_regulats))}€','${_(u"Costes regulados")}'], w:50},
                                     'IMP': {t: ['${formatLang(float(pie_impostos))}€','${_(u"Impuestos aplicados")}'], w:50},
                                     'PROD': {t: ['${formatLang(float(pie_costos))}€','${_(u"Costes de producción electricidad")}'], w:90}
                                    };

                var reparto = ${json.dumps(reparto)};
                var dades_reparto = ${json.dumps(dades_reparto)};

                var factura_id = ${factura.id};
                var data_consum = ${json.dumps(sorted(historic_js, key=lambda h: h['mes']))};
                var es30 = ${len(periodes_a)>9 and 'true' or 'false'};
                 ##  var es30 = ${len(periodes_a)>3 and 'true' or 'false'}
            </script>
            <script src="${addons_path}/giscedata_facturacio_comer_catral/report/extras/consum.js"></script>
            <script src="${addons_path}/giscedata_facturacio_comer_catral/report/extras/pie.js"></script>
            <script src="${addons_path}/giscedata_facturacio_comer_catral/report/extras/test.js"></script>
            </div>

            %if reverse:
                <p style="page-break-after:always; clear:both"></p>
                <div class="banner reverse-banners">
                    <img src="data:image/jpeg;base64,${reverse}">
                </div>
            %endif

            % if factura != objects[-1]:
                <p style="page-break-after:always; clear:both"></p>
            % endif
        %endfor
    </body>
</html>
