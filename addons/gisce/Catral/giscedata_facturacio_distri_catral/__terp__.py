# -*- coding: utf-8 -*-
{
    "name": "Facturación Catral (Distribuidora)",
    "description": """Facturación Catral (Distribuidora)""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Catral",
    "depends": [
        "giscedata_facturacio_distri",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
