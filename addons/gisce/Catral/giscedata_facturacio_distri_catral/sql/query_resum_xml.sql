select 
    d.name AS "Distribuidora"
    , d.ref AS "Ref. Distribuidora"
    , p.name AS "Comercializadora"
    , p.ref AS "Cod. Comer."
    , pol.name as "Póliza"
    , replace(tit.name,'"', '') as "Nombre cliente"
    , SUBSTRING(c.name,0,21) AS CUPS
    , c.et AS ET
    , replace(c.direccio,'"', '') as "dirección"
    , t.name AS "Tarifa Acceso"
    , i.number AS factura
    , i.date_invoice AS "Fecha Factura"
    , CASE WHEN f.tipo_rectificadora='A' THEN 'Anuladora'
   WHEN f.tipo_rectificadora='B' THEN 'Anuladora+Rectificadora'
   WHEN f.tipo_rectificadora='R' THEN 'Rectificadora'
   WHEN f.tipo_rectificadora='RA' THEN 'Rectificadora sin anuladora'
   WHEN f.tipo_rectificadora='BRA' THEN 'Anuladora (ficticia)'
   ELSE 'Normal'
   END AS "tipo factura"
    , f.data_inici AS "fecha inicial"
    , f.data_final AS "fecha final"
    , (f.data_final -f.data_inici) + 1 as "num. días"
    , d.vat
    , ene.energia * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS energia_kW
    , f.total_potencia * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS potencia
    , f.total_energia  * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS energia
    , f.total_reactiva  * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS reactiva
    , COALESCE(exces.exces_potencia, 0) * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS exces
    , altres.altres * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS "otros conceptos"
    , refact.refacturacio  * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS refacturacion
    , refact_T4.refacturacio  * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS refacturacion_T4
    , refact_T1.refacturacio  * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS refacturacion_T1
    , f.total_lloguers  * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS alquiler
    , COALESCE(fianza.fianza, 0) * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS fianza
    , COALESCE(no_iva_concepts.base_costs, 0)  * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END)  as conceptos_sin_iva
    , i.amount_untaxed  * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS "total conceptos"
    , it_18.name  AS IVA1
    , COALESCE(it_18.base,0)  * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS "Base IVA1"
    , COALESCE(it_18.amount,0)  * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS "total iva1"
    , it_21.name   AS IVA2
    , COALESCE(it_21.base,0)  * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS "Base IVA2"
    , COALESCE(it_21.amount,0)  * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS "total iva2"
    , i.amount_untaxed  * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS Base
    , i.amount_tax  * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS iva
    , i.amount_total  * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS TOTAL
FROM giscedata_facturacio_factura f 
LEFT JOIN giscedata_cups_ps c ON (f.cups_id=c.id) 
LEFT JOIN giscedata_polissa pol ON (f.polissa_id=pol.id) 
LEFT JOIN account_invoice i ON (i.id=f.invoice_id) 
LEFT JOIN giscedata_polissa_tarifa t ON (t.id=f.tarifa_acces_id)
LEFT JOIN (
SELECT lf.factura_id AS factura_id,SUM(li.quantity) AS energia
FROM giscedata_facturacio_factura_linia lf 
LEFT JOIN account_invoice_line li ON (li.id=lf.invoice_line_id)
WHERE lf.tipus='energia'
GROUP BY lf.factura_id
) AS ene ON (ene.factura_id=f.id)
LEFT JOIN (
SELECT lf.factura_id AS factura_id,SUM(li.price_subtotal) AS exces_potencia
FROM giscedata_facturacio_factura_linia lf
LEFT JOIN account_invoice_line li ON (li.id=lf.invoice_line_id)
WHERE lf.tipus ='exces_potencia'
GROUP BY lf.factura_id
) AS exces ON (exces.factura_id=f.id)
LEFT JOIN (
    SELECT lf.factura_id AS factura_id, SUM(li.price_subtotal) AS altres
    FROM giscedata_facturacio_factura_linia AS lf
    LEFT JOIN account_invoice_line AS li ON (li.id = lf.invoice_line_id)
    WHERE lf.tipus = 'altres'
    GROUP BY lf.factura_id
) AS altres ON (altres.factura_id = f.id)
LEFT JOIN (
SELECT lf.factura_id AS factura_id,SUM(li.price_subtotal) AS refacturacio
FROM giscedata_facturacio_factura_linia lf 
LEFT JOIN account_invoice_line li ON (li.id=lf.invoice_line_id)
WHERE lf.tipus='altres' AND li.name LIKE 'Refact%%'
GROUP BY lf.factura_id
) AS refact ON (refact.factura_id=f.id)
LEFT JOIN (
SELECT lf.factura_id AS factura_id,SUM(li.price_subtotal) AS refacturacio
FROM giscedata_facturacio_factura_linia lf 
LEFT JOIN account_invoice_line li ON (li.id=lf.invoice_line_id)
WHERE lf.tipus='altres' AND li.name LIKE 'Refact%%T4%%'
GROUP BY lf.factura_id
) AS refact_T4 ON (refact_T4.factura_id=f.id)
LEFT JOIN (
SELECT lf.factura_id AS factura_id,SUM(li.price_subtotal) AS refacturacio
FROM giscedata_facturacio_factura_linia lf 
LEFT JOIN account_invoice_line li ON (li.id=lf.invoice_line_id)
WHERE lf.tipus='altres' AND li.name LIKE 'Refact%%T1%%'
GROUP BY lf.factura_id
) AS refact_T1 ON (refact_T1.factura_id=f.id)
LEFT JOIN (
  SELECT fl.factura_id AS factura_id, SUM(il.price_subtotal) AS base_costs, 0 AS iva_costs
  FROM account_invoice_line il
  LEFT JOIN account_invoice_line_tax lt ON (lt.invoice_line_id = il.id)
  LEFT JOIN giscedata_facturacio_factura_linia fl ON (fl.invoice_line_id = il.id)
  WHERE lt.invoice_line_id IS NULL
    AND fl.tipus = 'altres'
  GROUP BY fl.factura_id
) no_iva_concepts ON (no_iva_concepts.factura_id = f.id)
LEFT JOIN (
  SELECT li.invoice_id AS invoice_id, SUM(li.price_subtotal) AS fianza
  FROM account_invoice_line li
  JOIN product_product p ON (li.product_id = p.id)
  WHERE p.default_code = 'FIANZA'
  GROUP BY li.invoice_id
) AS fianza ON (fianza.invoice_id = i.id)
LEFT JOIN account_invoice_tax it_18 ON (it_18.invoice_id=i.id AND it_18.name LIKE 'IVA 18%%')
LEFT JOIN account_invoice_tax it_21 ON (it_21.invoice_id=i.id AND it_21.name LIKE 'IVA 21%%')
LEFT JOIN account_invoice_tax it_iese ON (it_iese.invoice_id=i.id AND it_iese.name LIKE '%%electricidad%%')
LEFT JOIN res_partner p ON (p.id=i.partner_id) 
LEFT JOIN res_partner d ON (d.id=i.company_id)
LEFT JOIN res_partner tit on (tit.id = pol.titular)
WHERE 
f.id in (%s)
ORDER BY p.ref,CUPS
