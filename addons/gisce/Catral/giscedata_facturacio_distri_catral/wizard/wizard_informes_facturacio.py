# -*- coding: utf-8 -*-
from tools import config
from giscedata_facturacio_distri.wizard.wizard_informes_facturacio_distri import (
    INFORMES_CSV
)
from giscedata_facturacio_distri.sql.sql_column_titles import *

INFORMES_CSV.update({
    'export_factures_desglossat': {
        'header': QUERY_RESUM_DISTRI_TITLES,
        'query': "{}/giscedata_facturacio_distri_catral/sql/query_resum_xml.sql".format(config['addons_path'])
    }
})
