# *-* coding: utf-8 *-*

from osv import osv, fields

class giscegis_blocs_detectors_171(osv.osv):

    _name = 'giscegis.blocs.detectors.171'
    _auto = False
    _columns = {
      'x': fields.float('X', readonly=True),
      'y': fields.float('Y', readonly=True),
      'texte': fields.text('Texte', readonly=True),
      'blockname': fields.char('Blockname', size=50),
      'rotation': fields.float('Rotation'),
    }

    def init(self, cr):
        cr.execute("""drop view if exists giscegis_blocs_detectors_171""")
        cr.execute("""
          create or replace view giscegis_blocs_detectors_171 as (
          SELECT det.id, vertex.x, vertex.y, det.codi, blockname.name AS blockname, det.rotation
          FROM giscegis_blocs_detectors det
          LEFT JOIN giscegis_vertex vertex ON det.vertex = vertex.id
          LEFT JOIN giscegis_blocs_detectors_blockname blockname ON det.blockname = blockname.id)
        """)

giscegis_blocs_detectors_171()
