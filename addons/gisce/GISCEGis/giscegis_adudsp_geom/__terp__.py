# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS ADUDSP per a Giscegis 3",
    "description": """Vista dels ADUDSP per la versió Giscegis 3""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscedata_at",
        "giscegis_base",
        "giscegis_base_geom",
        "giscegis_blocs_adudsp",
        "giscegis_bt_caixes"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
