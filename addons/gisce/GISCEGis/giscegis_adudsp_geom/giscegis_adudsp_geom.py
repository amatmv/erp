# *-* coding: utf-8 *-*

from osv import osv, fields
from tools import config


class GiscegisAdudspGeom(osv.osv):
    """
    Model of giscegis.adudsp.geom for gis v3
    """
    _name = 'giscegis.adudsp.geom'
    _auto = False
    _columns = {
        'id': fields.integer('Id', readonly=True),
        'symbol': fields.char('Symbol', readonly=True, size=256),
        'rotation': fields.float('Rotation', readonly=True),
        'geom': fields.char('Geom', readonly=True, size=256),
        "materialize_date": fields.datetime("Fecha creación vista"),
    }

    def init(self, cursor):
        """
        Initializes the model and creates the materialized views

        :param cursor: Database cursor
        :return: True
        """

        srid = int(config.get("srid", 25830))
        cursor.execute("""
            DROP MATERIALIZED VIEW IF EXISTS giscegis_adudsp_geom;
        """)

        cursor.execute("""
            CREATE MATERIALIZED VIEW public.giscegis_adudsp_geom AS
                SELECT
                    block.id AS id,
                    blockname.name::text AS symbol,
                    block.rotation AS rotation,
                    ST_SETSRID(
                        ST_MAKEPOINT(vertex.x, vertex.y), %(srid)s
                    ) AS geom,
                    block.codi AS codi,
                    now() AS materialize_date
                FROM giscegis_blocs_adudsp block
                LEFT JOIN giscegis_vertex vertex ON block.vertex = vertex.id
                LEFT JOIN giscedata_bt_caixes_blockname AS blockname ON 
                    block.blockname = blockname.id;
        """, {"srid": srid})
        cursor.execute("""
        CREATE INDEX giscegis_adudsp_geom_geom
        ON giscegis_adudsp_geom USING gist (geom);
        """)
        return True

    def _auto_init(self, cursor, context):
        """
        To avoid the auto initialization of the model

        :param cursor: Database cursor
        :param context: OpenERP context
        :return: result
        """
        res = super(GiscegisAdudspGeom, self)._auto_init(cursor, context)
        return res

    def get_srid(self, cursor, uid):
        """
        Returns the srid of the model

        :param cursor: Database cursor
        :param uid: User id
        :return: SRID or false if the table is empty
        """
        return [config.get('srid', 25830)]

    def recreate(self, cursor, uid):
        """
        Method that recreates the materialized view

        :param cursor: Database cursor
        :param uid: User id
        :return: True
        """
        cursor.execute("""REFRESH MATERIALIZED VIEW giscegis_adudsp_geom;""")
        return True


GiscegisAdudspGeom()
