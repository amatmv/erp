# -*- coding: utf-8 -*-
from osv import fields
from giscegis_base_geom.fields import Point
from giscegis_base_geom.giscegis_session import SessionManaged
from tools import config


class GiscedataCondensador(SessionManaged):

    _name = 'giscedata.condensadors'
    _inherit = 'giscedata.condensadors'

    _columns = {
        'geom': Point("Geometria", srid=config.get('srid', 25830), select=True),
        'rotation': fields.integer("Rotació"),
        'node_id': fields.many2one("giscegis.nodes", "Node",
                                   ondelete='set null'),
        'xarxa': fields.selection(
            [('1', 'Alta'), ('2', 'Baixa')], 'Xarxa', required=True
        ),
    }

    _defaults = {
        'rotation': lambda *a: 0,
    }


GiscedataCondensador()
