# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Condensadors",
    "description": """QGIS suport for Condensadors""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "giscegis_base",
        "giscegis_base_geom",
        "giscegis_cataleg",
        "giscegis_nodes",
        "giscedata_condensadors"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscegis_condensadors_cataleg_data.xml",
        "giscegis_condensadors_view.xml"
    ],
    "active": False,
    "installable": True
}
