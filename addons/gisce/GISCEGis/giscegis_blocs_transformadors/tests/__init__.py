# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction


class TestTrafosCoord(testing.OOTestCase):
    def setUp(self):
        self.txn = Transaction().start(self.database)

    def test_retrieve_trafos_coord(self):
        """Test if the model retrieve correctly the coord values"""
        cursor = self.txn.cursor
        uid = self.txn.user

        img_obj = self.openerp.pool.get('ir.model.data')
        trafo_obj = self.openerp.pool.get('giscedata.transformador.trafo')

        trafo_id = img_obj.get_object_reference(
            cursor, uid, 'giscedata_transformadors', 'trafo_2'
        )

        trafo = trafo_obj.read(cursor, uid, trafo_id[1], ['x', 'y'])
        self.assertEqual(trafo['y'], 4664388.1307)
        self.assertEqual(trafo['x'], 981318.5818)

    def tearDown(self):
        self.txn.stop()
