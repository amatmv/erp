from osv import fields, osv
from psycopg2.extensions import AsIs
from tools.config import config
from giscegis_base_geom.wizard.giscegis_shp_loader \
    import GIS_MODELS_TO_UPDATE_GEOM_DATA


# Subscribe this model to the list to be updated if there is an AutoCAD dump
GIS_MODELS_TO_UPDATE_GEOM_DATA.append(
    {
        'model': 'giscedata.transformador.trafo',
        'functions': ['fill_geom', 'fill_rotation', 'fill_node_id']
    },
)


class GiscedataTransformadorTrafo(osv.osv):

    _name = 'giscedata.transformador.trafo'
    _inherit = 'giscedata.transformador.trafo'

    def _coords(self, cursor, uid, ids, field_name, arg, context):
        """
        Returns the X and Y of the Trafo
        :param cursor: Database cursor
        :type cursor: Psycopg2 cursor
        :param uid: User id
        :type uid: int
        :param ids: Trafo ids
        :type ids: list of int or int
        :return: X and Y of the Trafo
        :rtype: list of dict[str, float or False]
        """

        ret = {}

        sql = """
            SELECT t.id AS id,
                   ST_X(t.geom) AS geom_x,
                   ST_Y(t.geom) AS geom_y,
                   CASE WHEN t.reductor IS False THEN v.x
                        ELSE vr.x END AS vertex_x,
                   CASE WHEN t.reductor IS False THEN v.y
                        ELSE vr.y END AS vertex_y
            FROM giscedata_transformador_trafo t 
            LEFT JOIN giscegis_blocs_transformadors b ON b.transformadors = t.id
            LEFT JOIN giscegis_blocs_transformadors_reductors br ON 
                br.transformadors = t.id
            LEFT JOIN giscegis_vertex v ON v.id = b.vertex
            LEFT JOIN giscegis_vertex vr ON vr.id = br.vertex
            WHERE t.id = ANY(%s)
        """

        cursor.execute(sql, (ids,))
        data = cursor.dictfetchall()

        for line in data:
            trafo_id = line['id']

            # X
            if line['geom_x'] is not None:
                x = line['geom_x']
            elif line['vertex_x'] is not None:
                x = line['vertex_x']
            else:
                x = False

            # Y
            if line['geom_y'] is not None:
                y = line['geom_y']
            elif line['vertex_y'] is not None:
                y = line['vertex_y']
            else:
                y = False
                
            ret[trafo_id] = {"x": x, "y": y}
            ids.remove(trafo_id)

        for trafo_id in ids:
            ret[trafo_id] = {"x": False, "y": False}

        return ret

    def fill_geom_no_red(self, cursor, uid):
        """
        Function that runs a query to update the field geom of all the Trafos
        NoReductors
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """

        sql = """
            UPDATE giscedata_transformador_trafo AS trafo 
            SET geom = bloc.geom 
            FROM (
              SELECT transformadors AS id,
                     st_setsrid(
                         st_makepoint(vertex.x, vertex.y), %(srid)s
                     ) AS geom
              FROM giscegis_blocs_transformadors trafo
              LEFT JOIN giscegis_vertex vertex ON trafo.vertex = vertex.id
            ) AS bloc
            WHERE trafo.id=bloc.id
        """

        cursor.execute(sql, {"srid": config.get("srid", 25830)})

    def fill_rotation_no_red(self, cursor, uid):
        """
        Function that runs a query to update the field rotation of all Trafos
        NoReductors
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """

        sql = """
                UPDATE giscedata_transformador_trafo AS trafo 
                SET rotation = bloc.rotation 
                FROM (
                  SELECT transformadors AS id,
                         (360-rotation) AS rotation
                  FROM giscegis_blocs_transformadors trafo
                ) AS bloc
                WHERE trafo.id=bloc.id
            """

        cursor.execute(sql)

    def fill_node_id_no_red(self, cursor, uid):
        """
        Function that runs a query to update the field node_id of all the Trafos
        NoReductors
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """

        sql = """
            UPDATE giscedata_transformador_trafo AS trafo 
            SET node_id = bloc.node
            FROM (
              SELECT transformadors AS id,
                     node AS node
              FROM giscegis_blocs_transformadors trafo
            ) AS bloc
            WHERE trafo.id=bloc.id
        """

        cursor.execute(sql)

    def fill_geom(self, cursor, uid):
        """
        Function that fills the field geom of all the Trafos
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """
        self.fill_geom_no_red(cursor, uid)
        self.fill_geom_red(cursor, uid)
        sql = """
            UPDATE giscedata_transformador_trafo SET geom = St_SnapToGrid(
                ST_Translate(geom, 0.0000000001, 0.0000000001), 0.0001
            );
        """
        cursor.execute(sql)

    def fill_rotation(self, cursor, uid):
        """
        Function that fills the field rotation of all the Trafos
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """
        self.fill_rotation_no_red(cursor, uid)
        self.fill_rotation_red(cursor, uid)

    def fill_node_id(self, cursor, uid):
        """
        Function that fills the field node_id of all the Trafos
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """
        self.fill_node_id_no_red(cursor, uid)
        self.fill_node_id_red(cursor, uid)

    _columns = {
        'x': fields.function(
            _coords, method=True, type='float', string='x', multi="_coords"
        ),
        'y': fields.function(
            _coords, method=True, type='float', string='y', multi="_coords"
        )
    }


GiscedataTransformadorTrafo()
