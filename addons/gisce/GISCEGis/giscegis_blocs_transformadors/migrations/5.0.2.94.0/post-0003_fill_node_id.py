# coding=utf-8
import logging
import pooler


def migrate(cursor, installed_version):

    loger = logging.Logger("openerp.migration")
    loger.info("Omplint camp node_id de transformadors no reductors.")

    pool = pooler.get_pool(cursor.dbname)
    trafo_obj = pool.get("giscedata.transformador.trafo")
    trafo_obj.fill_node_id_no_red(cursor, 1)

    loger.info("Camp rotation de transformadors no reductors omplert")


up = migrate
