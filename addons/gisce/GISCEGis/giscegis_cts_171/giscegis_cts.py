# -*- coding: utf-8 -*-

from osv import fields, osv

class giscegis_cts_zoom_171(osv.osv):
    """Mòdul transicional pel GIS versió 1.7.1.
    Retorna una llista de CT's sense gaire informació, per tal que
    sigui una consulta rapida.
    """
    _name = "giscegis.cts.zoom_171"
    _description = "CT's Llistat per GIS (sense info)"
    _auto = False
    _columns = {
      'text': fields.char('Text', size=120, readonly=True),
      'id': fields.integer('Id', readonly=True),
      'x': fields.float('X', readonly=True),
      'y': fields.float('Y', readonly=True),
      'rotation': fields.float('Rotation', readonly=True),

    }

    _defaults = {

    }

    _order = 'id'

    def init(self,cr):
        cr.execute("""drop view if exists giscegis_cts_zoom_171""")
        cr.execute("""
    create or replace view giscegis_cts_zoom_171 as (select ct.id, ct.name, vertex.x, vertex.y, ctbloc.rotation from giscedata_cts ct LEFT JOIN giscegis_blocs_ctat ctbloc ON ctbloc.ct = ct.id LEFT JOIN giscegis_vertex vertex ON vertex.id = ctbloc.vertex WHERE (ct.ct_baixa = false OR ct.ct_baixa IS NULL) AND ct.active = true);
        """)

giscegis_cts_zoom_171()

class giscegis_cts_171(osv.osv):
    """Mòdul transicional pel GIS versió 1.7.1.
    Permet que el GIS es connecti a la base de dades de l'ERP enlloc d'anar
    cap al MS Access.
    """
    _name = "giscegis.cts.171"
    _description = "CT's Llistat per GIS"
    _auto = False
    _columns = {
      'text': fields.char('Text', size=120, readonly=True),
      'id': fields.integer('Id', readonly=True),
      'x': fields.float('X', readonly=True),
      'y': fields.float('Y', readonly=True),
      'rotation': fields.float('Rotation', readonly=True),
    }

    _defaults = {

    }
    _order = 'id'



    def init(self,cr):
        cr.execute("""drop view if exists giscegis_cts_171""")
        cr.execute("""
    create or replace view giscegis_cts_171 as (
    select ct.id, ct.name, ctbloc.rotation, v.x, v.y,
        (
        ct.name || E'\n' ||
        ct.descripcio || E'\n' ||
        (coalesce(
            (select coalesce(e.industria, 'S/E')|| ' ; ' || coalesce(e.industria_data::text, 'S/D')
            from giscedata_expedients_expedient e, giscedata_cts_expedients_rel r
            where r.ct_id = ct.id and r.expedient_id = e.id
            order by industria_data is not null, industria_data desc limit 1), 'S/E ; S/D')
        ) || E'\n' ||
        coalesce(sum(tr.potencia_nominal), 0) || ' kVA' || E'\n'
        ) as texte
    from giscedata_cts ct
    left join (
        select trr.id, trr.potencia_nominal, trr.ct from giscedata_transformador_trafo trr
        left join giscedata_transformador_estat tres on (trr.id_estat = tres.id and tres.codi=1)
    ) tr on (tr.ct = ct.id)
    left join giscegis_blocs_ctat ctbloc on ctbloc.ct = ct.id
    left join giscegis_vertex v on ctbloc.vertex = v.id
    WHERE (ct.ct_baixa = false OR ct.ct_baixa IS NULL) AND ct.active = true
    group by ct.name, ct.id, ct.descripcio, ctbloc.rotation, v.x, v.y
    order by ct.name
    );

    """)

giscegis_cts_171()
