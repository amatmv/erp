# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS CTS",
    "description": """Mòdul pel GIS 1.7.1, permet la connexió d'aquest a la base de dades del ERP, sense necessitar la BD d'Access""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscedata_cts",
        "giscedata_polissa",
        "giscedata_transformadors",
        "giscedata_expedients",
        "giscegis_base",
        "giscegis_blocs_ctat"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "ir.model.access.csv",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
