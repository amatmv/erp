# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS AT",
    "description": """Modul que exten els diferents elements d'AT per adaptar-los al GIS""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscedata_at",
        "giscegis_base",
        "giscegis_base_geom",
        "giscegis_cups",
        "giscegis_cataleg",
        "giscegis_edge"
    ],
    "init_xml": [],
    "demo_xml": [
        "giscegis_at_demo.xml"
    ],
    "update_xml":[
        "giscegis_cataleg_at_data.xml",
        "giscegis_at_data.xml",
        "giscegis_at_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
