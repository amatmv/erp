from giscegis_base_geom.giscegis_session import SessionManaged


class GiscegisEdge(SessionManaged):
    _name = 'giscegis.edge'
    _inherit = 'giscegis.edge'

    def clean_non_vinculated(self, cursor, uid, nodes=True, context=None):
        """
        Deletes the non vinculated edges related to an AT layer.
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :param nodes: Flag to clean nodes non referenced nodes
        :type nodes: bool
        :param context: OpenERP context
        :type context: dict[str, Any]
        :return: Number of deleted edges
        :rtype: int
        """

        sql_non_active_trams_at_edges_ids = """
            SELECT edge_id 
            FROM giscedata_at_tram
            WHERE NOT active
            AND edge_id IS NOT NULL
        """
        cursor.execute(sql_non_active_trams_at_edges_ids)
        tram_at_data = cursor.fetchall()
        to_delete_edges_ids = [row[0] for row in tram_at_data]

        sql_set_edge_at_id = """
            UPDATE giscedata_at_tram 
            SET edge_id = NULL 
            WHERE NOT active
            AND edge_id IS NOT NULL
        """
        cursor.execute(sql_set_edge_at_id)

        btlike = self.pool.get('giscegis.base.geom').get_btlike_layer(
            cursor, uid
        )
        autocad_edges_ids = """
            SELECT id 
            FROM giscegis_edge
            WHERE layer NOT LIKE %(btlike)s
            AND layer IS NOT NULL
        """
        cursor.execute(autocad_edges_ids, {"btlike": btlike})
        edges_data = cursor.fetchall()
        edges_ids_autocad = [row[0] for row in edges_data]

        to_delete_edges_ids = list(
            set(to_delete_edges_ids) - set(edges_ids_autocad)
        )

        if to_delete_edges_ids:
            sql_delete_edges = """
                DELETE FROM giscegis_edge
                WHERE id IN %(id)s
            """
            cursor.execute(sql_delete_edges, {"id": tuple(to_delete_edges_ids)})
        print("Deleting {} edges from AT".format(len(to_delete_edges_ids)))
        deleted_edges = len(to_delete_edges_ids) + super(
            GiscegisEdge, self
        ).clean_non_vinculated(cursor, uid, nodes=nodes, context=context)

        return deleted_edges


GiscegisEdge()
