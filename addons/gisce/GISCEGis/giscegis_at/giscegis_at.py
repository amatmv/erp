# -*- coding: utf-8 -*-
from giscegis_base_geom.fields import Line, Point
from giscegis_base_geom.giscegis_session import SessionManaged
from tools import config
from tools.translate import _
from psycopg2.extensions import AsIs
from osv import fields


class GiscedataAtTram(SessionManaged):

    _name = "giscedata.at.tram"
    _inherit = "giscedata.at.tram"

    def unlink(self, cursor, uid, ids, context=None):
        """ 
        Overwrites the unlink to set element inactive

        :param uid: User id
        :type uid: int
        :param ids: Elements ids
        :type ids: list(int) or int
        :param context: OpenERP context
        :type context: dict
        :return: True
        :rtype:bool
        """
        self.write(cursor,uid,ids,{"active": False})

        return True
        
    def get_next_name(self, cursor, uid, vals, context=None):
        """
        Returns the next name of a LAT

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param vals: Values to create
        :type vals: dict
        :param context: OpenERP context
        :type context: dict
        :return: Next name of LAT
        :rtype: int
        """

        if context is None:
            context = {}
        sql_next_name_no_session = """
          SELECT name
          FROM giscedata_at_tram
          ORDER BY name
          DESC LIMIT 1
          """
        cursor.execute(sql_next_name_no_session)
        codi = cursor.fetchone()
        sql_next_name_session = """
          SELECT data::json->'name'::text AS codi
          FROM giscegis_session_changes
          WHERE status='A'
          AND (data::json->'name')::text>%(codi)s::text
          AND res_model='giscedata.at.tram'
          ORDER BY (data::json->'name')::text
          limit 1;
          """

        if codi:
            cursor.execute(sql_next_name_session, {"codi": codi[0]})
            codi_sessio = cursor.fetchone()
            if codi_sessio:
                codi = codi_sessio
        if not codi:
            return 1
        else:
            return int(codi[0]) + 1

    def get_polyline(self, cursor, uid, ids, context=None):
        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        result = []
        btlike = self.pool.get('giscegis.base.geom').get_btlike_layer(
            cursor, uid
        )

        sql = '''
          SELECT e.id
          FROM giscegis_edge e
          LEFT JOIN giscedata_at_tram tram ON (tram.name=e.id_linktemplate)
          WHERE tram.id IN %(ids)s
          AND e.layer NOT LIKE %(btlike)s;
        '''

        cursor.execute(sql, {'ids': tuple(ids), 'btlike': btlike})
        sql_ret = cursor.dictfetchall()
        for line in sql_ret:
            result.append(line['id'])
        return result

    def get_geom(self, cursor, uid, ids, geom_format, srid=None, context=None):
        """
        Return the geometry of the given ids on the specified format
        :param cursor: Database cursor
        :type cursor: Psycopg2 cursor
        :param uid: User ID
        :type uid: int
        :param ids: Requested ids to compute the value
        :type ids: (list of int) or (int,) or int
        :param geom_format: Requested format to return the data. Aviable formats
        are: GeoJson, WKT and WKB
        :type geom_format: str
        :param srid: Return srid
        :type srid: int
        :param context: OpenERP context
        :type context: dict[str, Any]
        :return: The computed value in the requested format for each id on ids
        :rtype: dict[int, str or bool]
        """
        if context is None:
            context = {}

        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        # Ensure that the requested format is available
        geom_format = geom_format.lower()
        error_msg = _("Required format not valid. Valid formats are: "
                      "GeoJSON, WTK or WKB")
        assert geom_format in ("geojson", "wkb", "wkt"), error_msg

        # Initialize the return of the function
        ret = {str(object_id): False for object_id in ids}

        # Query declaration
        sql = '''
            SELECT id AS id, %(geom_op)s AS geo_field
            FROM giscedata_at_tram
            WHERE id IN %(ids)s
        '''

        # Initialize the geom formatter of the requested format
        if srid:
            if geom_format == "geojson":
                geom_op = 'ST_AsGeoJSON(ST_TRANSFORM(geom,{}))'.format(int(srid))
            elif geom_format == "wkb":
                geom_op = 'ST_AsBinary(ST_TRANSFORM(geom,{}))'.format(int(srid))
            else:
                geom_op = 'ST_AsText(ST_TRANSFORM(geom,{}))'.format(int(srid))
        else:
            if geom_format == "geojson":
                geom_op = 'ST_AsGeoJSON(geom)'
            elif geom_format == "wkb":
                geom_op = 'ST_AsBinary(geom)'
            else:
                geom_op = 'ST_AsText(geom)'

        # Execute the query and compute the result of each requested element
        cursor.execute(sql, {'ids': tuple(ids), 'geom_op': AsIs(geom_op)})
        query_res = cursor.dictfetchall()
        if geom_format == "wkb":
            for res in query_res:
                ret[str(res['id'])] = bytes(res['geo_field']).encode('hex')
        else:
            for res in query_res:
                ret[str(res['id'])] = res['geo_field']

        return ret

    def create(self, cr, uid, vals, context=None):
        """
        Overwrites the create to avoid duplicated names on LAT

        :param cr: Database cursor
        :param uid: User ids
        :type uid: int
        :param vals: Ct vals
        :type vals: dict
        :param context: OpenERP context
        :type context: dict
        :return: None
        :rtype: None
        """
        existing_name = self.search(
            cr, uid, [("name", "=", vals.get("name", False))]
        )

        if existing_name or ('name' not in vals):
            vals['name'] = self._get_nextnumber(cr, uid, context)

        return super(GiscedataAtTram, self).create(
            cr, uid, vals, context=context
        )

    def write(self, cursor, uid, ids, vals, context=None):
        """
        Overwrites the write to avoid overwritte names on Qgisce part line
        :param cursor: Database cursor
        :param uid: User ids
        :type uid: int
        :param ids:  IDS to write the vals
        :type ids: list of int
        :param vals: Tram AT vals
        :type vals: dict
        :param context: OpenERP context
        :type context: dict
        :return: True or False if it have been possible to write to the IDS
        :rtype: Boolean
        """

        if 'name' in vals:
            del vals['name']

        return super(GiscedataAtTram, self).write(
            cursor, uid, ids, vals, context=context
        )

    def _ff_longitud_cad(self, cursor, uid, ids, field_name, ar, context):
        """
        Fields function to calculate the longitud_cad

        :param cursor: Database cursor
        :param uid: User id
        :param ids: AT tram ids
        :param field_name: Field name
        :param ar: Arguments
        :param context: OpenERP context
        :return:
        :rtype: dict
        """

        ret = dict.fromkeys(ids)
        sql = """
        SELECT id,st_length(geom)
        FROM giscedata_at_tram
        WHERE id in %(ids)s
        """
        cursor.execute(sql, {"ids": tuple(ids)})
        data = cursor.fetchall()
        for line in data:
            ret[line[0]] = line[1]
        return ret

    def _longitud_cad_to_update(self, cursor, uid, ids, context=None):
        """
        Returns the list of cad to update

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: List of ids to calc
        :type ids: list(int)
        :param context: OpenERP context
        :return: List of elements to store
        :rtype: list(int)
        """

        return ids

    _columns = {
        'geom': Line("geometry", srid=config.get('srid', 25830), select=True),
        'edge_id': fields.many2one("giscegis.edge", "Aresta",
                                   ondelete='set null'),
        "longitud_cad": fields.function(
            _ff_longitud_cad, type="float", string="Longitud Autocad",
            readonly=True, method=True,
            store={
                'giscedata.at.tram': (
                _longitud_cad_to_update, ['geom'], 10)
            }
        ),
    }


GiscedataAtTram()


class GiscegisAtSuportTipus(SessionManaged):

    _name = 'giscegis.at.suport.tipus'
    _description = 'Tipus Suports Alta Tensió'
    _rec_name = 'description'

    def create(self, cr, uid, vals, context=None):
        """
        ToDo
        :param cr:
        :param uid:
        :param vals:
        :param context:
        :return:
        """
        if context is None:
            context = {}
        ids = self.search(cr, uid, [('name', '=', vals['name'])])
        if ids and len(ids):
            return ids[0]
        else:
            return super(GiscegisAtSuportTipus, self).create(cr, uid, vals)

    _columns = {
        'name': fields.char('Nom', size=64, required=True),
        'description': fields.char('Descripció', size=256),
    }

    _order = "name, id"


GiscegisAtSuportTipus()


class GiscegisAtSuport(SessionManaged):

    _name = "giscedata.at.suport"
    _inherit = "giscedata.at.suport"

    def _get_tipus_name(self, cr, uid, ids, field_name, arg, context):
        """
        ToDo
        :param cr:
        :param uid:
        :param ids:
        :param field_name:
        :param arg:
        :param context:
        :return:
        """
        res = {}
        sql = """
            SELECT s.id AS id, t.name AS t_name
            FROM giscedata_at_suport s
            LEFT JOIN giscegis_at_suport_tipus t ON t.id = s.tipus
            WHERE s.id IN %(ids)s;
        """
        cr.execute(sql, {'ids': tuple(ids)})
        query_result = cr.dictfetchall()
        for row in query_result:
            res[row['id']] = row['t_name']

        return res

    def _get_tipus_name_webgis(self, cr, uid, ids, field_name, arg, context):
        """
        ToDo
        :param cr:
        :param uid:
        :param ids:
        :param field_name:
        :param arg:
        :param context:
        :return:
        """
        res = {}
        sql = """
            SELECT s.id AS id, t.name AS t_name, mida_armat AS ma
            FROM giscedata_at_suport s
            LEFT JOIN giscegis_at_suport_tipus t ON t.id = s.tipus
            WHERE s.id IN %(ids)s;
        """
        cr.execute(sql, {'ids': tuple(ids)})
        query_result = cr.dictfetchall()
        for row in query_result:
            webgis_name = ''
            if row['t_name'] == 'PFO':
                webgis_name = 'PFO'
            elif row['t_name'] == 'PFO+P':
                webgis_name = 'PORTIC-PFO'
            elif row['t_name'] == 'PFO+R':
                webgis_name = 'PFOR'
            elif row['t_name'] == 'PFU':
                webgis_name = 'PFU'
            elif row['t_name'] == 'PFU+D':
                webgis_name = 'PFU-2'
            elif row['t_name'] == 'PFU+P':
                webgis_name = 'PORTIC-PFU'
            elif row['t_name'] == 'PFU+PZ':
                webgis_name = 'PORTIC-PFUZ'
            elif row['t_name'] == 'PFU+Z':
                webgis_name = 'PFUZ'
            elif row['t_name'] == 'PM':
                webgis_name = 'PM'
            elif row['t_name'] == 'PM+A':
                mida_armat = int(row.get('ma', 0.0)*10)
                if mida_armat == 18:
                    webgis_name = 'PM18-2'
                elif mida_armat == 20:
                    webgis_name = 'PM20'
                elif mida_armat == 25:
                    webgis_name = 'PM25-2'
                elif mida_armat == 30:
                    webgis_name = 'PM30-2'
                elif mida_armat == 40:
                    webgis_name = 'PM-40-4'
            elif row['t_name'] == 'PM+C':
                webgis_name = 'PM-40-4 creu'
            elif row['t_name'] == 'PM+P':
                webgis_name = 'PORTIC-PM'
            elif row['t_name'] == 'PM+R':
                webgis_name = 'JB'
            elif row['t_name'] == 'PM+Q':
                webgis_name = 'JBN'

            res[row['id']] = webgis_name

        return res

    def _get_suports(self, cursor, uid, ids, context=None):
        """
        ToDo
        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: List of ids to calc
        :type ids: list[int]
        :param context: OpenERP context
        :return: List of elements to store
        :rtype: list[int]
        """

        return ids

    _columns = {
        'geom': Point("geometry", srid=config.get('srid', 25830)),
        'rotation': fields.integer("Rotation"),
        'tipus': fields.many2one(
            "giscegis.at.suport.tipus", "Tipus Suport GIS"
        ),
        'tipus_name': fields.function(
            _get_tipus_name, method=True, type='char', size=60, string="Tipus"
        ),
        'webgis_tipus': fields.function(
            _get_tipus_name_webgis, method=True, type='char', size=60,
            string='Webgis Tipus', store={
                    'giscedata.at.suport': (
                        _get_suports, ['tipus', 'mida_armat'], 10
                    )
                },
        ),
        'mida_armat': fields.float("Mida Armat")
    }

    _defaults = {
        'rotation': lambda *a: 0,
        'mida_armat': lambda *a: 0.0
    }


GiscegisAtSuport()
