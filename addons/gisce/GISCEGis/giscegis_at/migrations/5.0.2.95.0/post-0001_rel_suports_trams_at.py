# coding=utf-8

import logging
import pooler


def up(cursor, installed_version):

    logger = logging.getLogger('openerp.migration')

    logger.info('Creating the relations between Suports AT and Trams AT.')

    pool = pooler.get_pool(cursor.dbname)
    sup_at_obj = pool.get("giscedata.at.suport")

    sql = """
        SELECT s.id AS s_id, t.id AS t_id
        FROM giscedata_at_suport s
        LEFT JOIN giscedata_at_linia l ON l.id = s.linia
        LEFT JOIN giscedata_at_tram t ON t.linia = l.id
        WHERE ST_INTERSECTS(ST_BUFFER(s.geom, 4),t.geom)
        ORDER BY s.id;
    """

    cursor.execute(sql)
    query_result = cursor.dictfetchall()
    for row in query_result:
        sup_at_obj.write(
            cursor, 1, row['s_id'], {'tram_at_ids': [(4, row['t_id'])]}
        )

    logger.info('Relations between Suports AT and Trams AT created succesfully')


def down(cursor, installed_version):
    pass


migrate = up
