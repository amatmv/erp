# coding=utf-8

import logging


def up(cursor, installed_version):

    logger = logging.getLogger('openerp.migration')

    logger.info('Standardizing Trams AT \'geom\' field precision.')

    sql = """
        UPDATE giscedata_at_tram 
        SET geom = St_SnapToGrid(
            ST_Translate(geom, 0.0000000001, 0.0000000001), 0.0001
        );
    """

    cursor.execute(sql)

    logger.info(
        'Standardization of Trams AT \'geom\' field precision '
        'made successfully.'
    )


def down(cursor, installed_version):
    pass


migrate = up
