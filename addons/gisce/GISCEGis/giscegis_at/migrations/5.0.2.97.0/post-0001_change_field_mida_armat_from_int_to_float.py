# coding=utf-8

import logging


def up(cursor, installed_version):

    logger = logging.getLogger('openerp.migration')

    logger.info('Change Suport AT field \'mida_armat\' from integer to float')

    sql = """
        ALTER TABLE giscedata_at_suport
        ALTER COLUMN mida_armat SET DATA TYPE double precision 
        USING mida_armat::double precision;
    """

    cursor.execute(sql)

    logger.info('Suport AT field \'mida_armat\' is now of type float')


def down(cursor, installed_version):
    pass


migrate = up
