from destral import testing
from destral.transaction import Transaction
import logging


logger = logging.getLogger(__name__)


__all__ = [
    'TestTramsAtGeomGetter'
]


class TestTramsAtGeomGetter(testing.OOTestCase):

    def setUp(self):
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user

    def tearDown(self):
        self.txn.stop()

    def test_multiformat_geom_getter_trams_at(self):
        """
        Test the differents geom formats aviable from the geom getter method
        get_geom of the model GiscedataAtTram
        :return: None
        """
        trams_obj = self.openerp.pool.get('giscedata.at.tram')

        tram_id = trams_obj.search(
            self.cursor, self.uid, [('name', '=', '1')]
        )[0]

        geo_json_format = trams_obj.get_geom(
            self.cursor, self.uid, tram_id, "GeoJSON"
        )

        self.assertEqual(
            geo_json_format[str(tram_id)],
            '{"type":"LineString",'
            '"coordinates":[[981392.4397,4664414.7722],'
            '[981396.0245,4664419.8605],'
            '[981379.5627,4664462.8441]]}'
        )

        wkt_format = trams_obj.get_geom(
            self.cursor, self.uid, tram_id, "WKT"
        )

        self.assertEqual(
            wkt_format[str(tram_id)],
            "LINESTRING(981392.4397 4664414.7722,"
            "981396.0245 4664419.8605,981379.5627 4664462.8441)"
        )

        wkb_format = trams_obj.get_geom(
            self.cursor, self.uid, tram_id, "WKB"
        )

        self.assertEqual(
            wkb_format[str(tram_id)],
            "010200000003000000c15b20e120f32d418db96bb117cb514196438b0c28f32d41"
            "986e12f718cb5141e3361a2007f32d4102bc05b623cb5141"
        )

    def test_multiformat_geom_getter_tams_at_wrong_formats(self):
        """
        Test if the exception throw behaviour is correct
        :return: None
        """

        trams_obj = self.openerp.pool.get('giscedata.at.tram')

        tram_id = trams_obj.search(
            self.cursor, self.uid, [('name', '=', '1')]
        )[0]

        # Bad format request
        try:
            trams_obj.get_geom(
                self.cursor, self.uid, tram_id, "rofl"
            )
            logger.error("Exception not raised on a bad format request.")
        except Exception:
            logger.info("Exception raised correctly due a bad format request.")

        # Good format request
        try:
            trams_obj.get_geom(
                self.cursor, self.uid, tram_id, "WKT"
            )
            logger.info("Exception not raised due a good format request.")
        except Exception:
            logger.error("Exception raised wrongly on a good format request.")
