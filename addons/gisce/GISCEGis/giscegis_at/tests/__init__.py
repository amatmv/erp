# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
import ast
from .test_session import *
from .test_geom_getter import *
from .test_edges_coherence import *
# from .test_topologia import *


class TramsAtGeomTest(testing.OOTestCase):

    def test_unlink(self):
        """
        Tests that if you unlink an element it is set to inactive
        """
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            tram_obj = self.openerp.pool.get('giscedata.at.tram')
            tram_id = tram_obj.create(cursor, uid, {"name": "123"})
            tram_obj.unlink(cursor, uid, tram_id, context={})
            data = tram_obj.read(cursor, uid, tram_id, ["active"], context={'active_test': False})
            self.assertFalse(data["active"])

    def test_catalog_elements_trams_at(self):
        """
        Test that are elements for Trams AT and how many of each type are
        searching with the catalog domain.
        :return: None
        """

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            # INICIALITZEM OBJECTES
            trams_at_obj = self.openerp.pool.get('giscedata.at.tram')
            cataleg_obj = self.openerp.pool.get('giscegis.cataleg.cataleg')
            sum_trams = 0

            # SELECCIONEM UNICAMENT ELS ELEMENTS DE CATALEG QUE SIGUIN
            # TRAMS AT
            ids_trams_cataleg = cataleg_obj.search(
                cursor, uid, [('model.model', '=', 'giscedata.at.tram')]
            )

            # PER A CADA ELEMENT DEL CATALEG COMPROVAREM QUE HI HAGIN ELEMENTS
            cataleg_trams_elems = cataleg_obj.read(
                cursor, uid, ids_trams_cataleg, ['name', 'domain']
            )

            for cataleg_elem in cataleg_trams_elems:

                trafo_ids = trams_at_obj.search(
                    cursor, uid, ast.literal_eval(cataleg_elem['domain'])
                )

                sum_trams += len(trafo_ids)
                if cataleg_elem['name'] == "Tram AT":
                    self.assertEqual(len(trafo_ids), 64)

            self.assertEqual(sum_trams, 64)
