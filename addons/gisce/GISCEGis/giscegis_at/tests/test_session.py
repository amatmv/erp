from destral import testing
from destral.transaction import Transaction
from giscegis_base_geom.giscegis_session import SessionSingleSavePoint


__all__ = [
    'TestTramsAtSession'
]


class TestTramsAtSession(testing.OOTestCase):

    def setUp(self):
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user

    def tearDown(self):
        self.txn.stop()

    def test_search_session_trams_at(self):
        """
        This test is for check the correct behaviour of the sessions and the
        Trams AT
        """
        # Load ERP models
        trams_obj = self.openerp.pool.get('giscedata.at.tram')
        session_obj = self.openerp.pool.get('giscegis.session')

        # Clean the sessions to avoid BBox collisions
        sql = """
            TRUNCATE giscegis_session CASCADE
        """
        self.cursor.execute(sql)

        # Create the sessions
        session1_vals = {
            "name": 'Testing session 1',
            "bbox": 'POLYGON((982449.3 4664776.0, 982760.4 4664776.0, '
                    '982760.4 4664950.2, 982449.3 4664950.2, '
                    '982449.3 4664776.0))',
            "state": 'open',
            "ttl": 500000000,
            "active": True
        }
        session2_vals = {
            "name": 'Testing session 2',
            "bbox": 'POLYGON((981216.7 4664152.6, 981771.6 4664152.6, '
                    '981771.6 4664463.2, 981216.7 4664463.2, '
                    '981216.7 4664152.6))',
            "state": 'open',
            "ttl": 500000000,
            "active": True
        }
        session1_id = session_obj.create(self.cursor, self.uid, session1_vals)
        session2_id = session_obj.create(self.cursor, self.uid, session2_vals)

        # Start the checks
        # Check the number of Trams AT in differents sessions and without
        # sessions
        # No session
        trams_ids = trams_obj.search(self.cursor, self.uid, [])
        self.assertEqual(len(trams_ids), 64)
        # Session 1
        trams_ids = trams_obj.search(
            self.cursor, self.uid, [], context={'session': session1_id}
        )
        self.assertEqual(len(trams_ids), 0)
        # Session 2
        trams_ids = trams_obj.search(
            self.cursor, self.uid, [], context={'session': session2_id}
        )
        self.assertEqual(len(trams_ids), 34)

        # Get 2 ids from the session 2
        tram1_id = trams_ids[0]
        tram2_id = trams_ids[1]

        # Check that the changes made on a session are only visible from the
        # session
        # Load data before changes
        trams_data_original = trams_obj.read(
            self.cursor, self.uid, [tram1_id, tram2_id], ['origen']
        )
        # Write new data on 2 Trams AT from session 2
        trams_write_vals = {
            "origen": "hola"
        }
        trams_obj.write(
            self.cursor, self.uid, [tram1_id, tram2_id],
            trams_write_vals, context={'session': session2_id}
        )
        # Reload data outside the session
        trams_data = trams_obj.read(
            self.cursor, self.uid, [tram1_id, tram2_id], ['origen']
        )
        # Compare data outside session before and after write data on session 2
        self.assertEqual(
            trams_data_original[0]['origen'],
            trams_data[0]['origen']
        )
        self.assertEqual(
            trams_data_original[1]['origen'],
            trams_data[1]['origen']
        )
        # Check changes inside the session
        trams_data = trams_obj.read(
            self.cursor, self.uid, [tram1_id, tram2_id], ['origen'],
            context={'session': session2_id}
        )
        for data in trams_data:
            self.assertEqual(data['origen'], "hola")

    def test_reserialize_if_needed(self):
        cursor = self.cursor
        uid = self.uid
        # Load ERP models
        trams_obj = self.openerp.pool.get('giscedata.at.tram')
        sup_obj = self.openerp.pool.get('giscedata.at.suport')
        sup_tipus_obj = self.openerp.pool.get('giscegis.at.suport.tipus')
        session_obj = self.openerp.pool.get('giscegis.session')
        cataleg_obj = self.openerp.pool.get('giscegis.cataleg.cataleg')
        session_changes_obj = self.openerp.pool.get('giscegis.session.changes')



        # Clean the sessions to avoid BBox collisions
        sql = """
            TRUNCATE giscegis_session CASCADE
        """
        self.cursor.execute(sql)

        # Search for catalogs
        tram_at_catalog = cataleg_obj.search(
            cursor, uid, [('name', '=', 'Tram AT')]
        )[0]

        sup_at_pfu_catalog = cataleg_obj.search(
            cursor, uid, [('name', '=', 'Poste Fusta')]
        )[0]

        # Search for PFU type
        sup_at_tipus_pfu_id = sup_tipus_obj.search(
            cursor, uid, [('name', '=', 'PFU')]
        )[0]

        # Elements creation
        session_vals = {
            "name": 'Testing Session',
            "bbox": 'POLYGON((0 0, 0 1000, 1000 1000, 1000 0, 0 0))',
            "state": 'open',
            "ttl": 500000000,
            "active": True
        }
        tram_vals = {
            "geom": "LINESTRING(1 1, 50 50)"
        }
        session_id = session_obj.create(cursor, uid, session_vals)
        context = {
            'session': session_id,
            'cataleg': tram_at_catalog
        }
        trams_obj.create(cursor, uid, tram_vals, context=context)

        # Check if it ask for reserialize on TramAt change
        with SessionSingleSavePoint(cursor):
            session_changes_ids = session_changes_obj.search(
                cursor, uid, [('session_id', '=', session_id)]
            )
            reserialize = session_changes_obj.apply(
                cursor, uid, session_changes_ids
            )
            self.assertTrue(reserialize)

        # Reset data
        sql = """
            TRUNCATE giscegis_session CASCADE
        """
        self.cursor.execute(sql)

        # Elements creation
        session_vals = {
            "name": 'Testing Session',
            "bbox": 'POLYGON((0 0, 0 1000, 1000 1000, 1000 0, 0 0))',
            "state": 'open',
            "ttl": 500000000,
            "active": True
        }
        sup_vals = {
            "name": "Test Sup AT",
            "geom": "POINT(50 50)",
            "tipus": sup_at_tipus_pfu_id
        }
        session_id = session_obj.create(cursor, uid, session_vals)
        context = {
            'session': session_id,
            'cataleg': sup_at_pfu_catalog
        }
        sup_obj.create(cursor, uid, sup_vals, context=context)

        # Check if it ask for reserialize on SupAt change
        with SessionSingleSavePoint(cursor):
            session_changes_ids = session_changes_obj.search(
                cursor, uid, [('session_id', '=', session_id)]
            )
            reserialize = session_changes_obj.apply(
                cursor, uid, session_changes_ids
            )
            self.assertFalse(reserialize)
