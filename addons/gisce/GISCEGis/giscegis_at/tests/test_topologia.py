from destral import testing
from destral.transaction import Transaction


class TestAtTopologia(testing.OOTestCase):

    def setUp(self):
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user
        self.model_name = "giscedata.at.tram"

    def tearDown(self):
        self.txn.stop()

    def _get_model_id(self):
        ir_model_obj = self.openerp.pool.get("ir.model")
        model_id = ir_model_obj.search(
            self.cursor, self.uid,
            [("model", "=", self.model_name)]
        )

        return model_id[0]

    def test_write(self):
        """
        Tests write of the geom field to ensure the giscegis.node.bloc.model
        is updated

        :return: None
        """

        nbm_mod = self.openerp.pool.get("giscegis.node.bloc.model")
        node_mod = self.openerp.pool.get("giscegis.nodes")
        at_mod = self.openerp.pool.get("giscedata.at.tram")

        model_id = self._get_model_id()

        node1_id = node_mod.create(
            self.cursor, self.uid,
            {
                "name": 1,
                "geom": "POINT (111.0 222.0)"
            }
        )

        node2_id = node_mod.create(
            self.cursor, self.uid,
            {
                "name": 2,
                "geom": "POINT (222.0 111.0)"
            }
        )

        nbm_mod.create(
            self.cursor, self.uid,
            {
                "res_id": 1,
                "model": model_id,
                "node": node1_id
            }
        )

        nbm_mod.create(
            self.cursor, self.uid,
            {
                "res_id": 1,
                "model": model_id,
                "node": node2_id
            }
        )

        at_mod.write(
            self.cursor, self.uid, [1], {"geom": "LineString (111.0 222.0, 222.0 111.0)"}
        )

        nbm_search_params = [
            ("res_id", "=", 1),
            ("model", "=", model_id)
        ]
        nbm_id = nbm_mod.search(self.cursor, self.uid, nbm_search_params)
        nbm_data = nbm_mod.read(self.cursor, self.uid, nbm_id, ["node"])

        node_search_params = [
            "|",
            ("geom", "=", "POINT (222.0 111.0)"),
            ("geom", "=", "POINT (111.0 222.0)")

        ]
        node_id = node_mod.search(self.cursor, self.uid, node_search_params)

        self.assertEquals(len(node_id), 2)
        self.assertIn(nbm_data[0]["node"][0], node_id)
        self.assertIn(nbm_data[1]["node"][0], node_id)

    def test_write_multiple(self):
        """
        Tests write of multiple geom field to ensure the
        giscegis.node.bloc.model is updated

        :return: None
        """

        nbm_mod = self.openerp.pool.get("giscegis.node.bloc.model")
        node_mod = self.openerp.pool.get("giscegis.nodes")
        at_mod = self.openerp.pool.get("giscedata.at.tram")

        model_id = self._get_model_id()

        node1_id = node_mod.create(
            self.cursor, self.uid,
            {
                "name": 1,
                "geom": "POINT (111.0 222.0)"
            }
        )

        node2_id = node_mod.create(
            self.cursor, self.uid,
            {
                "name": 2,
                "geom": "POINT (222.0 111.0)"
            }
        )

        nbm_mod.create(
            self.cursor, self.uid,
            {
                "res_id": 1,
                "model": model_id,
                "node": node1_id
            }
        )

        nbm_mod.create(
            self.cursor, self.uid,
            {
                "res_id": 1,
                "model": model_id,
                "node": node2_id
            }
        )

        at_mod.write(
            self.cursor, self.uid,
            [1, 2, 3], {"geom": "LineString (111.0 222.0, 222.0 111.0)"}
        )

        nbm_search_params = [
            ("res_id", "in", [1, 2, 3]),
            ("model", "=", model_id)
        ]
        nbm_id = nbm_mod.search(self.cursor, self.uid, nbm_search_params)
        nbm_data = nbm_mod.read(self.cursor, self.uid, nbm_id, ["node"])

        node_search_params = [
            ("geom", "=", "LineString (111.0 222.0, 222.0 111.0)")
        ]
        nodes_id = node_mod.search(self.cursor, self.uid, node_search_params)

        for ident in nodes_id:
            self.assertEquals(ident, nbm_data["node"])

    def test_unlink(self):
        """
        Tests unlink of the geom field to ensure the giscegis.node.bloc.model
        is updated

        :return: None
        """
        nbm_mod = self.openerp.pool.get("giscegis.node.bloc.model")
        cts_mod = self.openerp.pool.get("giscedata.at.tram")

        model_id = self._get_model_id()
        cts_mod.unlink(self.cursor, self.uid, [1])

        nbm_search_params = [
            ("res_id", "=", 1),
            ("model", "=", model_id)
        ]

        nbm_id = nbm_mod.search(self.cursor, self.uid, nbm_search_params)
        self.assertEquals(len(nbm_id), 0)
    
    def test_create(self):
        """
        Tests create of the geom field to ensure the giscegis.node.bloc.model
        is updated

        :return: None
        """

        nbm_mod = self.openerp.pool.get("giscegis.node.bloc.model")
        node_mod = self.openerp.pool.get("giscegis.nodes")
        at_mod = self.openerp.pool.get("giscedata.at.tram")

        node_mod.create(
            self.cursor, self.uid,
            {
                "name": 1,
                "geom": "POINT (111.0 222.0)"
            }
        )

        node_mod.create(
            self.cursor, self.uid,
            {
                "name": 2,
                "geom": "POINT (222.0 111.0)"
            }
        )

        at_data = {
            "name": "123",
            "geom": "LineString (111.0 222.0, 222.0 111.0)"
        }
        at_id = at_mod.create(self.cursor, self.uid, at_data)

        model_id = self._get_model_id()
        nbm_search_params = [
            ("res_id", "=", at_id),
            ("model", "=", model_id)
        ]
        nbm_id = nbm_mod.search(self.cursor, self.uid, nbm_search_params)
        nbm_data = nbm_mod.read(self.cursor, self.uid, nbm_id, ["node"])

        node_search_params = [
            "|", ("geom", "=", "POINT (111.0 222.0)"),
            ("geom", "=", "POINT (222.0 111.0)")
        ]

        node_ids = node_mod.search(self.cursor, self.uid, node_search_params)

        self.assertEquals(len(nbm_id), 2)
        self.assertIn(nbm_data[0]["node"][0], node_ids)
        self.assertIn(nbm_data[1]["node"][0], node_ids)

