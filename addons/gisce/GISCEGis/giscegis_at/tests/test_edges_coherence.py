from destral import testing
from destral.transaction import Transaction


__all__ = [
    'TestTramsAtEdgesCoherence'
]


class TestTramsAtEdgesCoherence(testing.OOTestCase):

    def setUp(self):
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user

    def tearDown(self):
        self.txn.stop()

    def test_trams_at_edges_coherence(self):
        """
        This test is about check that the demo data created from the xml files
        have coherence at topological level
        """

        # This query selects the start and the end point of a Tram AT and the
        # start and end point of the related edge of the Tram.
        sql = """
            SELECT t.id AS tram_id,
                   e.id AS edge_id, 
                   ST_AsText(ST_StartPoint(t.geom)) AS tram_start,
                   ST_AsText(e_ns.geom) AS edge_start,
                   ST_AsText(ST_EndPoint(t.geom)) AS tram_end,
                   ST_AsText(e_nf.geom) AS edge_end
            FROM giscedata_at_tram t
            LEFT JOIN giscegis_edge e ON e.id = t.edge_id
            LEFT JOIN giscegis_nodes e_ns ON e_ns.id = e.start_node
            LEFT JOIN giscegis_nodes e_nf ON e_nf.id = e.end_node
        """
        self.cursor.execute(sql)

        counter = 0

        # For each row of the query we check that if the Tram AT have a related
        # edge, the end and start points of both elements are the same so
        # they have coherence at topological level
        for row in self.cursor.dictfetchall():
            # If the columnys edge_start or edge_end are None it means that the
            # Tram AT don't have a related edge, so we skip it
            if row['edge_start'] is None or row['edge_end'] is None:
                counter += 1
                continue

            # Try to check if the Tram AT and the EDGE geometry fits from one
            # side or another
            coherence = False
            if row['tram_start'] == row['edge_start']:
                if row['tram_end'] == row['edge_end']:
                    coherence = True
            elif row['tram_start'] == row['edge_end']:
                if row['tram_end'] == row['edge_start']:
                    coherence = True

            # On this clause we check if the related edge of the Tram AT have
            # matched the geometry of the edge
            self.assertTrue(
                coherence,
                "The TramAT {} wich have the related Edge {} "
                "have an incoherence topology".format(
                    row['tram_id'], row['edge_id']
                )
            )

        # On this clause we check that there are no Trams AT without
        # a related Edge
        self.assertEqual(
            0, counter,
            "There are {} Trams AT without a related Edge".format(counter)
        )
