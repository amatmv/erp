# -*- coding: utf-8 -*-
from osv import fields, osv


class GiscegisBlocsStopNodesBt(osv.osv):
    """
    Mòdul que crea una taula amb FK's cap a ir.model,
    només els models corresponents a elements que separen
    la xarxa de BAIXA de la d'ALTA hi estaran relacionats.
    Es fan servir a la simulació, per saber-ne els límits.
    """
    _name = "giscegis.blocs.stop.nodes.bt"
    _description = "Elements Stop BT"
    _columns = {
        'name': fields.many2one('ir.model', 'Model'),
        'domain': fields.text("Domini d'inclusio"),
    }

    _defaults = {
        "domain": lambda *a: "[]",
    }
    _order = 'id'


GiscegisBlocsStopNodesBt()
