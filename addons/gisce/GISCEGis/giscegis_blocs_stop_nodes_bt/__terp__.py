# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Stop Nodes BT",
    "description": """ Mòdul que crea una taula amb FK's cap a ir.model.""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_base"
    ],
    "init_xml":[
        "data.xml"
    ],
    "demo_xml": [],
    "update_xml":[
        "ir.model.access.csv",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
