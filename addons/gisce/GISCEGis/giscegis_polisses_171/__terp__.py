# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Polisses",
    "description": """Mòdul pel GIS 1.7.1, obtencio de la fitxa d'un client i la seva polissa""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "giscedata_cups",
        "giscedata_polissa",
        "base",
        "giscedata_cts",
        "giscedata_lectures_distri",
        "giscegis_base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "ir.model.access.csv",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
