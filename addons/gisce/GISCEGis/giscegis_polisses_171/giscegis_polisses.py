# -*- coding: utf-8 -*-

from osv import fields, osv

class giscegis_polisses_171(osv.osv):
    """Vista per obtenir les dades de clients i polisses."""
    _name = "giscegis.polisses.171"
    _description = "Polisses Llistat per GIS"
    _auto = False
    _columns = {
      'nv': fields.char('Carrer', size=120, readonly=True),
      'linia': fields.char('Línia', size=10, readonly=True),
      'es': fields.char('Escala', size=5, readonly=True),
      'n_comptador': fields.char('Num. Comptador', size=20, readonly=True),
      'potencia': fields.float('Potencia', readonly=True),
      'escomesa': fields.char('Escomesa', size=10, readonly=True),
      'tensio': fields.char('Tensió', size=10, readonly=True),
      'titular': fields.char('Titular', size=128, readonly=True),
      'pnp': fields.char('Numero', size=10, readonly=True),
      'name': fields.char('Polissa', size=60, readonly=True),
      'pt': fields.char('Pis', size=10, readonly=True),
      'pu': fields.char('Porta', size=10, readonly=True),
      'id': fields.integer('Id', readonly=True),
      'cups': fields.char('CUPS', size=25, readonly=True),
      'municipi': fields.integer('ID Municipi', readonly=True),
      'x': fields.float('x', readonly=True),
      'y': fields.float('y', readonly=True),
      'rotation': fields.float('rotation', readonly=True),
      'et': fields.char('ET', size=60, readonly=True),
      'state': fields.char('Estat', size=64, readonly=True),
    }

    _defaults = {

    }
    _order = 'id'

    def init(self,cr):
        cr.execute("""DROP VIEW IF EXISTS giscegis_polisses_171""")
        cr.execute("""
    CREATE OR REPLACE VIEW giscegis_polisses_171 AS  (
    SELECT cups.nv, cups.linia, cups.es, l.name as n_comptador, p.potencia, 
    e.name AS escomesa, p.tensio, partner.name AS titular, cups.pnp, 
    p.name, cups.pt, cups.pu, p.id, cups.name AS cups,
    cups.id_municipi as municipi, 
    cups.et, v.x, v.y, be.rotation, p.state
    FROM giscedata_polissa p
    left join giscedata_lectures_comptador l on (l.polissa = p.id) and l.active
    left join res_partner partner on (p.titular = partner.id)
    left join giscedata_cups_ps cups on (p.cups = cups.id)
    left join giscedata_cups_escomesa e on (e.id = cups.id_escomesa)
    left join giscegis_blocs_escomeses be on (e.id=be.escomesa)
    left join giscegis_vertex v on (be.vertex=v.id)
    left join giscedata_cts cts on (cts.id::text = cups.et::text)
    )
    """)

giscegis_polisses_171()
