from giscegis_base_index.giscegis_base_index import GiscegisBaseIndex


class GiscegisCatastreParceles(GiscegisBaseIndex):
    """
    Class to index Parceles if no active session
    """
    _name = "giscegis.catastre.parceles"
    _inherit = "giscegis.catastre.parceles"

    _index_title = '{obj.name}'
    _index_summary = '{obj.partner_id.name}'

    _index_fields = {
        'name': True,
        'partner_id.name': True,
        'address_id.name': True,
        'address_id.phone': True,
        'address_id.email': True,
        'id_municipi.name': True,
        'num_poligon': True,
        'num_parcela': True
    }

    def get_lat_lon(self, cursor, uid, item, context=None):
        """
        Method to get latitude and longitude
        :param cursor: Database cursor
        :param uid: User id
        :param item: Item to index
        :param context: OpenERP context
        :return: Tuple with lat and lon
        """
        if context is None:
            context = {}

        srid = self.get_db_srid(cursor, uid)

        sql = """
                SELECT 
                    ST_Y(ST_TRANSFORM(ST_CENTROID(geom),4326)) as lat, 
                    ST_X(ST_TRANSFORM(ST_CENTROID(geom),4326)) as lon
                FROM giscegis_catastre_parceles
                WHERE id = %(id)s;
                """

        cursor.execute(sql, {'id': item.id})
        res = cursor.fetchone()
        if not res:
            res = (0, 0)
        return res


GiscegisCatastreParceles()
