# -*- coding: utf-8 -*-
{
    "name": "Catastre index per a GIS",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Index per parceles de catastre per a GIS
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "giscegis_catastre",
        "giscegis_base_index",
        "giscegis_search_type"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "search_type.xml"
    ],
    "active": False,
    "installable": True
}