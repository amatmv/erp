# -*- encoding: utf-8 -*-
from giscegis_base_geom.giscegis_session import SessionManaged
from tools import config
from osv import fields
from giscegis_base_geom.fields import Point


class GiscedataCtsSubestacions(SessionManaged):

    _name = 'giscedata.cts.subestacions'
    _inherit = 'giscedata.cts.subestacions'


GiscedataCtsSubestacions()


class GiscedataCtsSubestacionsPosicio(SessionManaged):
    _name = 'giscedata.cts.subestacions.posicio'
    _inherit = 'giscedata.cts.subestacions.posicio'

    _columns = {
        'geom': Point("Geometria", srid=config.get('srid', 25830), select=True),
        'rotation': fields.integer("Rotació"),
        "node_id": fields.many2one("giscegis.nodes", "Node",
                                   ondelete='set null')
    }

    _defaults = {
        'rotation': lambda *a: 0,
    }


GiscedataCtsSubestacionsPosicio()
