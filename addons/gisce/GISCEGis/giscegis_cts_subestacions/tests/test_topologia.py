from destral import testing
from destral.transaction import Transaction


class TestCtsTopologia(testing.OOTestCase):

    def setUp(self):
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user

    def tearDown(self):
        self.txn.stop()

    def test_write(self):
        """
        Tests write of the geom field to ensure the giscegis.node.bloc.model
        is updated

        :return: None
        """

        nbm_mod = self.openerp.pool.get("giscegis.node.bloc.model")
        node_mod = self.openerp.pool.get("giscegis.nodes")
        cts_mod = self.openerp.pool.get("giscegis.cts.subestacions")

        cts_mod.write([1], {"geom": "POINT (111.0 222.0)"})

        nbm_search_params = [
            ("res_id", "=", 1),
            ("model", "=", self._name)
        ]
        nbm_id = nbm_mod.search(nbm_search_params)
        nbm_data = nbm_mod.read(nbm_id, ["node"])

        node_search_params = [
            ("geom", "=", "POINT (111.0 222.0)")
        ]
        node_id = node_mod.search(node_search_params)
        self.assertEquals(node_id[0], nbm_data["node"])

    def test_write_multiple(self):
        """
        Tests write of multiple geom field to ensure the
        giscegis.node.bloc.model is updated

        :return: None
        """

        nbm_mod = self.openerp.pool.get("giscegis.node.bloc.model")
        node_mod = self.openerp.pool.get("giscegis.nodes")
        cts_mod = self.openerp.pool.get("giscegis.cts.subestacions")

        cts_mod.write([1, 2, 3], {"geom": "POINT (111.0 222.0)"})

        nbm_search_params = [
            ("res_id", "in", [1, 2, 3]),
            ("model", "=", self._name)
        ]
        nbm_id = nbm_mod.search(nbm_search_params)
        nbm_data = nbm_mod.read(nbm_id, ["node"])

        node_search_params = [
            ("geom", "=", "POINT (111.0 222.0)")
        ]
        nodes_id = node_mod.search(node_search_params)

        for ident in nodes_id:
            self.assertEquals(ident, nbm_data["node"])

    def test_unlink(self):
        """
        Tests unlink of the geom field to ensure the giscegis.node.bloc.model
        is updated

        :return: None
        """
        nbm_mod = self.openerp.pool.get("giscegis.node.bloc.model")
        cts_mod = self.openerp.pool.get("giscegis.cts.subestacions")

        cts_mod.unlink([1])

        nbm_search_params = [
            ("res_id", "=", 1),
            ("model", "=", self._name)
        ]

        nbm_id = nbm_mod.search(nbm_search_params)
        self.assertEquals(len(nbm_id), 0)
    
    def test_create(self):
        """
        Tests create of the geom field to ensure the giscegis.node.bloc.model
        is updated

        :return: None
        """

        nbm_mod = self.openerp.pool.get("giscegis.node.bloc.model")
        node_mod = self.openerp.pool.get("giscegis.nodes")
        cts_mod = self.openerp.pool.get("giscegis.cts.subestacions")

        ct_data = {
            "name":"test",
            "geom": "POINT (111.0 222.0)"
        }
        ct_id = cts_mod.create(ct_data)

        nbm_search_params = [
            ("res_id", "=", ct_id),
            ("model", "=", self._name)
        ]
        nbm_id = nbm_mod.search(nbm_search_params)
        nbm_data = nbm_mod.read(nbm_id, ["node"])

        node_search_params = [
            ("geom", "=", "POINT (111.0 222.0)")
        ]
        node_id = node_mod.search(node_search_params)
        self.assertEquals(node_id[0], nbm_data["node"])
