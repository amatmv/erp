# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
import ast
from .test_session import *


class SubestacionsGeomTest(testing.OOTestCase):

    def test_catalog_elements_se(self):
        """
        Test that are elements for Subestacions and how many of each type are
        searching with the catalog domain.
        :return: None
        """

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            # INICIALITZEM OBJECTES
            se_obj = self.openerp.pool.get('giscedata.cts.subestacions')
            cataleg_obj = self.openerp.pool.get('giscegis.cataleg.cataleg')
            sum_se = 0

            # SELECCIONEM UNICAMENT ELS ELEMENTS DE CATALEG QUE SIGUIN SE
            ids_se_cataleg = cataleg_obj.search(
                cursor, uid,
                [('model.model', '=', 'giscedata.cts.subestacions')]
            )

            # PER A CADA ELEMENT DEL CATALEG COMPROVAREM QUE HI HAGIN ELEMENTS
            cataleg_se_elems = cataleg_obj.read(
                cursor, uid, ids_se_cataleg, ['name', 'domain']
            )

            for cataleg_elem in cataleg_se_elems:

                se_ids = se_obj.search(
                    cursor, uid, ast.literal_eval(cataleg_elem['domain'])
                )

                sum_se += len(se_ids)
                # PER SI EN UN FUTUR S'HA D'AFEGIR ALGUNA DIFERENCIA EN LES
                # SE
                if cataleg_elem['name'] == "Subestacions":
                    self.assertEqual(len(se_ids), 1)

            self.assertEqual(sum_se, 1)
