from destral import testing
from destral.transaction import Transaction


__all__ = [
    'TestSeSession'
]


class TestSeSession(testing.OOTestCase):

    def setUp(self):
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user

    def tearDown(self):
        self.txn.stop()

    def test_search_session_se(self):
        """
        This test is for check the correct behaviour of the sessions and the SE
        """
        # Load ERP models
        se_obj = self.openerp.pool.get('giscedata.cts.subestacions')
        session_obj = self.openerp.pool.get('giscegis.session')

        # Clean the sessions to avoid BBox collisions
        sql = """
            TRUNCATE giscegis_session CASCADE
        """
        self.cursor.execute(sql)

        # Create the sessions
        session1_vals = {
            "name": 'Testing session 1',
            "bbox": 'POLYGON((982449.3 4664776.0, 982760.4 4664776.0, '
                    '982760.4 4664950.2, 982449.3 4664950.2, '
                    '982449.3 4664776.0))',
            "state": 'open',
            "ttl": 500000000,
            "active": True
        }
        session2_vals = {
            "name": 'Testing session 2',
            "bbox": 'POLYGON((980575 4664655, 980917 4664655, 980917 4664404, '
                    '980575 4664404, 980575 4664655))',
            "state": 'open',
            "ttl": 500000000,
            "active": True
        }
        session1_id = session_obj.create(self.cursor, self.uid, session1_vals)
        session2_id = session_obj.create(self.cursor, self.uid, session2_vals)

        # Start the checks
        # Check the number of SE in differents sessions and without sessions
        # No session
        se_ids = se_obj.search(self.cursor, self.uid, [])
        self.assertEqual(len(se_ids), 1)
        # Session 1
        se_ids = se_obj.search(
            self.cursor, self.uid, [], context={'session': session1_id}
        )
        self.assertEqual(len(se_ids), 0)
        # Session 2
        se_ids = se_obj.search(
            self.cursor, self.uid, [], context={'session': session2_id}
        )
        self.assertEqual(len(se_ids), 1)

        # Get 1 id
        se1_id = se_ids[0]

        # Check that the changes made on a session are only visible from the
        # session
        # Load data before changes
        se_data_original = se_obj.read(
            self.cursor, self.uid, [se1_id], ['rotation']
        )
        # Write new data on SE from session 2
        se_write_vals = {
            "rotation": 240
        }
        se_obj.write(
            self.cursor, self.uid, [se1_id],
            se_write_vals, context={'session': session2_id}
        )
        # Reload data outside the session
        se_data = se_obj.read(self.cursor, self.uid, [se1_id], ['rotation'])
        # Compare data outside session before and after write data on session 2
        self.assertEqual(
            se_data_original[0]['rotation'],
            se_data[0]['rotation']
        )
        # Check changes inside the session
        se_data = se_obj.read(
            self.cursor, self.uid, [se1_id], ['rotation'],
            context={'session': session2_id}
        )
        for data in se_data:
            self.assertEqual(data['rotation'], 240)
