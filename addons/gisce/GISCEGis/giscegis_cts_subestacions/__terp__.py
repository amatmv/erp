# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS SUBESTACIONS",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscedata_cts_subestacions",
        "giscegis_base",
        "giscegis_base_geom",
        "giscegis_cataleg",
        "giscegis_cts"
    ],
    "init_xml": [],
    "demo_xml": [
        "giscegis_cts_subestacions_demo.xml"
    ],
    "update_xml":[
        "giscegis_cataleg_cts_subestacions_data.xml"
    ],
    "active": False,
    "installable": True
}
