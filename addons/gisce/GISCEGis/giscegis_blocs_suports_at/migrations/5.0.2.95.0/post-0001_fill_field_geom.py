# coding=utf-8

import logging
import pooler


def up(cursor, installed_version):

    logger = logging.getLogger('openerp.migration')

    logger.info('Filling field \'geom\' of the model GiscedataAtSuport.')

    pool = pooler.get_pool(cursor.dbname)
    sup_obj = pool.get('giscedata.at.suport')
    sup_obj.fill_geom(cursor, 1)

    logger.info('Field \'geom\' filled correctly on GiscedataAtSuport.')


def down(cursor, installed_version):
    pass


migrate = up
