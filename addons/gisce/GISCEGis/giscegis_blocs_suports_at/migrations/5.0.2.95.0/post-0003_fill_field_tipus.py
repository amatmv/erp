# coding=utf-8

import logging
import pooler


def up(cursor, installed_version):

    logger = logging.getLogger('openerp.migration')

    logger.info('Filling field \'tipus\' of the model GiscedataAtSuport.')

    pool = pooler.get_pool(cursor.dbname)
    sup_obj = pool.get('giscedata.at.suport')
    sup_obj.fill_tipus(cursor, 1)

    logger.info('Field \'tipus\' filled correctly on GiscedataAtSuport.')


def down(cursor, installed_version):
    pass


migrate = up
