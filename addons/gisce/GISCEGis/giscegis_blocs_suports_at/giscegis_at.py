# -*- coding: utf-8 -*-
from tools import config
from osv import osv
from giscegis_base_geom.wizard.giscegis_shp_loader \
    import GIS_MODELS_TO_UPDATE_GEOM_DATA

# Subscribe this model to the list to be updated if there is an AutoCAD dump
GIS_MODELS_TO_UPDATE_GEOM_DATA.append(
    {
        'model': 'giscedata.at.suport',
        'functions': [
            'fill_geom', 'fill_rotation', 'fill_tipus', 'fill_attributes'
        ]
    },
)


class GiscegisAtSuport(osv.osv):

    _name = "giscedata.at.suport"
    _inherit = "giscedata.at.suport"

    def fill_geom(self, cursor, uid):
        """
        Function that runs a query to update the field geom of all the SupAT
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """

        sql = """
            UPDATE giscedata_at_suport AS sup 
            SET geom = bloc.geom 
            FROM (
              SELECT b.numsuport AS name,
                     st_setsrid(st_makepoint(v.x, v.y), %(srid)s) AS geom
              FROM giscegis_blocs_suports_at b
              LEFT JOIN giscegis_vertex v ON b.vertex = v.id
              WHERE b.numsuport IS NOT NULL
            ) AS bloc
            WHERE sup.name = bloc.name
        """

        cursor.execute(sql, {"srid": config.get("srid", 25830)})

        sql = """
            UPDATE giscedata_at_suport AS sup 
            SET geom = bloc.geom 
            FROM (
              SELECT b.numsuport1 AS name,
                     st_setsrid(st_makepoint(v.x, v.y), %(srid)s) AS geom
              FROM giscegis_blocs_suports_at b
              LEFT JOIN giscegis_vertex v ON b.vertex = v.id
              WHERE b.numsuport1 IS NOT NULL
            ) AS bloc
            WHERE sup.name = bloc.name
        """

        cursor.execute(sql, {"srid": config.get("srid", 25830)})

        sql = """
            UPDATE giscedata_at_suport AS sup 
            SET geom = bloc.geom 
            FROM (
              SELECT b.numsuport2 AS name,
                     st_setsrid(st_makepoint(v.x, v.y), %(srid)s) AS geom
              FROM giscegis_blocs_suports_at b
              LEFT JOIN giscegis_vertex v ON b.vertex = v.id
              WHERE b.numsuport2 IS NOT NULL
            ) AS bloc
            WHERE sup.name = bloc.name
        """

        cursor.execute(sql, {"srid": config.get("srid", 25830)})

        sql = """
            UPDATE giscedata_at_suport AS sup 
            SET geom = bloc.geom 
            FROM (
              SELECT b.numsuport2 AS name,
                     st_setsrid(st_makepoint(v.x, v.y), %(srid)s) AS geom
              FROM giscegis_blocs_suports_at b
              LEFT JOIN giscegis_vertex v ON b.vertex = v.id
              WHERE b.numsuport2 IS NOT NULL
            ) AS bloc
            WHERE sup.name = bloc.name
        """

        cursor.execute(sql, {"srid": config.get("srid", 25830)})

        sql = """
            UPDATE giscedata_at_suport AS sup 
            SET geom = bloc.geom 
            FROM (
              SELECT b.numsuport3 AS name,
                     st_setsrid(st_makepoint(v.x, v.y), %(srid)s) AS geom
              FROM giscegis_blocs_suports_at b
              LEFT JOIN giscegis_vertex v ON b.vertex = v.id
              WHERE b.numsuport3 IS NOT NULL
            ) AS bloc
            WHERE sup.name = bloc.name
        """

        cursor.execute(sql, {"srid": config.get("srid", 25830)})

        sql = """
            UPDATE giscedata_at_suport SET geom = St_SnapToGrid(
                ST_Translate(geom, 0.0000000001, 0.0000000001), 0.0001
            );
        """
        cursor.execute(sql)

    def fill_rotation(self, cursor, uid):
        """
        Function that runs a query to update the field rotation of all the
        SupAT
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """

        sql = """
            UPDATE giscedata_at_suport AS sup 
            SET rotation = (360 - bloc.rotation) 
            FROM (
              SELECT b.numsuport AS name,
                     b.rotation AS rotation
              FROM giscegis_blocs_suports_at b
              WHERE b.numsuport IS NOT NULL
            ) AS bloc
            WHERE sup.name = bloc.name
        """

        cursor.execute(sql)

        sql = """
            UPDATE giscedata_at_suport AS sup 
            SET rotation = (360 - bloc.rotation) 
            FROM (
              SELECT b.numsuport1 AS name,
                     b.rotation AS rotation
              FROM giscegis_blocs_suports_at b
              WHERE b.numsuport1 IS NOT NULL
            ) AS bloc
            WHERE sup.name = bloc.name
        """

        cursor.execute(sql)

        sql = """
            UPDATE giscedata_at_suport AS sup 
            SET rotation = (360 - bloc.rotation) 
            FROM (
              SELECT b.numsuport2 AS name,
                     b.rotation AS rotation
              FROM giscegis_blocs_suports_at b
              WHERE b.numsuport2 IS NOT NULL
            ) AS bloc
            WHERE sup.name = bloc.name
        """

        cursor.execute(sql)

        sql = """
            UPDATE giscedata_at_suport AS sup 
            SET rotation = (360 - bloc.rotation) 
            FROM (
              SELECT b.numsuport3 AS name,
                     b.rotation AS rotation
              FROM giscegis_blocs_suports_at b
              WHERE b.numsuport3 IS NOT NULL
            ) AS bloc
            WHERE sup.name = bloc.name
        """

        cursor.execute(sql)

    def fill_tipus(self, cursor, uid):
        """
        Function that runs a query to update the field tipus of all the SupAT
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """

        new_tipus = {}
        old_new_tipus_relation = {}
        mida_armats = {}

        # Map the new tipus {tipus_name: tipus_id}
        sql = """
            SELECT id, name
            FROM giscegis_at_suport_tipus
        """
        cursor.execute(sql)
        query_result = cursor.fetchall()
        for row in query_result:
            new_tipus[row[1]] = row[0]

        # Relate the old tipus with the new one {blockname_id: tipus_id}
        sql = """
            SELECT b.id, b.name 
            FROM giscegis_blocs_suports_at s 
            LEFT JOIN giscegis_blocs_suports_at_blockname b 
                ON b.id = s.blockname 
            GROUP BY b.name, b.id;
        """
        cursor.execute(sql)
        query_result = cursor.fetchall()
        for row in query_result:
            # Metall
            if row[1] == 'PM':
                old_new_tipus_relation[row[0]] = new_tipus['PM']
            elif row[1] in (
                    'PM18-2', 'PM20-2', 'PM20', 'PM25-2', 'PM-2', 'PM30-2',
                    'PM30-3', 'PM30-4', 'PM-40-4', 'PM18-3'
            ):
                old_new_tipus_relation[row[0]] = new_tipus['PM+A']
                if '18' in row[1]:
                    mida_armats[row[0]] = 1.8
                elif ('20' in row[1]) or (row[1] == 'PM-2'):
                    mida_armats[row[0]] = 2.0
                elif '25' in row[1]:
                    mida_armats[row[0]] = 2.5
                elif '30' in row[1]:
                    mida_armats[row[0]] = 3.0
                elif '40' in row[1]:
                    mida_armats[row[0]] = 4.0
            elif 'PM-40-4 creu' in row[1]:
                old_new_tipus_relation[row[0]] = new_tipus['PM+C']
            elif ('PORTIC' in row[1]) and ('PM' in row[1]):
                old_new_tipus_relation[row[0]] = new_tipus['PM+P']
            # Fusta
            elif row[1] == 'PFU':
                old_new_tipus_relation[row[0]] = new_tipus['PFU']
            elif row[1] == 'PFUZ':
                old_new_tipus_relation[row[0]] = new_tipus['PFU+Z']
            elif row[1] in ('PFU_2', 'PFU-2'):
                old_new_tipus_relation[row[0]] = new_tipus['PFU+D']
            elif row[1] == 'PORTIC-PFU':
                old_new_tipus_relation[row[0]] = new_tipus['PFU+P']
            elif row[1] == 'PORTIC-PFUZ':
                old_new_tipus_relation[row[0]] = new_tipus['PFU+PZ']
            # Formigo
            elif row[1] in ('PFO', 'PFO2'):
                old_new_tipus_relation[row[0]] = new_tipus['PFO']
            elif row[1] == 'PFOR':
                old_new_tipus_relation[row[0]] = new_tipus['PFO+R']
            elif row[1] == 'PORTIC-PFO':
                old_new_tipus_relation[row[0]] = new_tipus['PFO+P']
            elif row[1] == 'JB':
                old_new_tipus_relation[row[0]] = new_tipus['PM+R']
            elif row[1] == 'JBN':
                old_new_tipus_relation[row[0]] = new_tipus['PM+Q']
            else:
                old_new_tipus_relation[row[0]] = False

        # Fill fields tipus and mida_armat
        sql = """
            SELECT s.id AS sup_id, b.blockname AS bkname_id 
            FROM giscegis_blocs_suports_at b 
                LEFT JOIN giscedata_at_suport s ON s.name = b.numsuport
            WHERE s.id IS NOT NULL
        """
        cursor.execute(sql)
        query_result = cursor.dictfetchall()

        sql = """
            SELECT s.id AS sup_id, b.blockname AS bkname_id 
            FROM giscegis_blocs_suports_at b 
                LEFT JOIN giscedata_at_suport s ON s.name = b.numsuport1
            WHERE s.id IS NOT NULL
        """
        cursor.execute(sql)
        query_result += cursor.dictfetchall()

        sql = """
            SELECT s.id AS sup_id, b.blockname AS bkname_id 
            FROM giscegis_blocs_suports_at b 
                LEFT JOIN giscedata_at_suport s ON s.name = b.numsuport2
            WHERE s.id IS NOT NULL
        """
        cursor.execute(sql)
        query_result += cursor.dictfetchall()

        sql = """
            SELECT s.id AS sup_id, b.blockname AS bkname_id 
            FROM giscegis_blocs_suports_at b 
                LEFT JOIN giscedata_at_suport s ON s.name = b.numsuport2
            WHERE s.id IS NOT NULL
        """
        cursor.execute(sql)
        query_result += cursor.dictfetchall()

        for row in query_result:
            sup_vals = {}
            if row['bkname_id'] in mida_armats:
                sup_vals['mida_armat'] = mida_armats[row['bkname_id']]
            if row['bkname_id'] in old_new_tipus_relation:
                sup_vals['tipus'] = old_new_tipus_relation[row['bkname_id']]
            if sup_vals:
                self.write(cursor, uid, row['sup_id'], sup_vals)

    def fill_attributes(self, cursor, uid):
        """
        Fill the attributes of the Suports AT.
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return: None
        :rtype: NoneType
        """
        sup_at_obj = self.pool.get('giscedata.at.suport')

        sql = """
            SELECT bn.name AS tipus,
                ST_AsText(
                    St_SnapToGrid(
                        ST_Translate(
                            st_setsrid(
                                st_makepoint(v.x, v.y), %(srid)s),
                                0.0000000001, 0.0000000001
                        ), 0.0001
                    )
                ) AS geom
            FROM giscegis_blocs_suports_at b
            LEFT JOIN giscegis_vertex v ON b.vertex = v.id
            LEFT JOIN giscegis_blocs_suports_at_blockname bn
                ON b.blockname = bn.id
            WHERE bn.name IN ('TP', 'TIRANT')
        """

        cursor.execute(sql, {"srid": config.get("srid", 25830)})

        rows = cursor.dictfetchall()

        sql = """
            SELECT bn.name AS tipus,
                ST_AsText(
                    St_SnapToGrid(
                        ST_Translate(
                            st_setsrid(
                                st_makepoint(v.x, v.y), %(srid)s),
                                0.0000000001, 0.0000000001
                        ), 0.0001
                    )
                ) AS geom
            FROM giscegis_blocs_suports_bt b
            LEFT JOIN giscegis_vertex v ON b.vertex = v.id
            LEFT JOIN giscegis_blocs_suports_bt_blockname bn
                ON b.blockname = bn.id
            WHERE bn.name IN ('TP', 'TIRANT')
        """

        cursor.execute(sql, {"srid": config.get("srid", 25830)})

        rows = rows + cursor.dictfetchall()

        vals = {}
        for attr in rows:
            if attr['tipus'] == 'TIRANT':
                sup_at_id = sup_at_obj.search(
                    cursor, uid, [
                        ('geom', '&&', attr['geom']),
                        ('geom', '&&!', attr['geom'])
                    ]
                )
                if sup_at_id:
                    if sup_at_id[0] in vals:
                        vals[sup_at_id[0]]['tirant'] = True
                    else:
                        vals[sup_at_id[0]] = {'tirant': True}
            elif attr['tipus'] == 'TP':
                sup_at_id = sup_at_obj.search(
                    cursor, uid, [
                        ('geom', '&&', attr['geom']),
                        ('geom', '&&!', attr['geom'])
                    ]
                )
                if sup_at_id:
                    if sup_at_id[0] in vals:
                        vals[sup_at_id[0]]['tornapuntes'] = True
                    else:
                        vals[sup_at_id[0]] = {'tornapuntes': True}
        if vals:
            for sup_at_id, values in vals.iteritems():
                sup_at_obj.write(cursor, uid, [sup_at_id], values)


GiscegisAtSuport()
