from osv import osv, fields
from tools import config
from psycopg2.extensions import AsIs
from giscegis_base_geom.wizard.giscegis_shp_loader \
    import GIS_MODELS_TO_UPDATE_GEOM_DATA


# Subscribe this model to the list to be updated if there is an AutoCAD dump
GIS_MODELS_TO_UPDATE_GEOM_DATA.append(
    {
        'model': 'giscedata.cups.escomesa',
        'functions': [
            'fill_geom', 'fill_rotation', 'fill_last_geom', 'fill_node_id'
        ]
    },
)


class GiscedataCupsEscomesa(osv.osv):

    _name = 'giscedata.cups.escomesa'
    _inherit = 'giscedata.cups.escomesa'

    def fill_geom(self, cursor, uid):
        """
        Function that runs a query to update the field geom of all the
        Escomeses
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """

        sql = """
            UPDATE giscedata_cups_escomesa AS c 
            SET geom = b.geom 
            FROM (
              SELECT b.escomesa AS id,
                     st_setsrid(st_makepoint(v.x, v.y), %(srid)s) AS geom
              FROM giscegis_blocs_escomeses b
              LEFT JOIN giscegis_vertex v ON b.vertex = v.id
            ) AS b
            WHERE c.id=b.id;
            UPDATE giscedata_cups_escomesa SET geom = St_SnapToGrid(
                ST_Translate(geom, 0.0000000001, 0.0000000001), 0.0001
            );
        """
        cursor.execute(sql, {"srid": AsIs(config.get("srid", 25830))})

    def fill_rotation(self, cursor, uid):
        """
        Function that runs a query to update the field rotation of all the
        Escomeses
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """

        sql = """
            UPDATE giscedata_cups_escomesa AS c 
            SET rotation = b.rotation 
            FROM (
              SELECT b.escomesa AS id,
                     (360-rotation) AS rotation
              FROM giscegis_blocs_escomeses b
            ) AS b
            WHERE c.id=b.id
        """
        cursor.execute(sql)

    def fill_last_geom(self, cursor, uid):
        """
        Function that runs a query to update the field last_geom of all the
        Escomeses
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """

        sql = """
            UPDATE giscedata_cups_escomesa 
            SET last_geom = ST_SETSRID(
                ST_MAKEPOINT(ultima_x, ultima_y), %(srid)s
            )
        """

        cursor.execute(sql, {'srid': AsIs(config.get('srid', 25830))})

    def fill_node_id(self, cursor, uid):
        """
        Function that runs a query to update the field node_id of all the
        Escomeses
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """

        sql = """
            UPDATE giscedata_cups_escomesa AS c 
            SET node_id = b.node
            FROM (
              SELECT b.escomesa AS id,
                     node AS node
              FROM giscegis_blocs_escomeses b
            ) AS b
            WHERE c.id=b.id
        """
        cursor.execute(sql)

    def _coords(self, cursor, uid, ids, field_name, arg, context):
        """
        Returns the X and Y of the Escomesa
        :param cursor: Database cursor
        :type cursor: Psycopg2 cursor
        :param uid: User id
        :type uid: int
        :param ids: Escomesas ids
        :type ids: list of int or int
        :return: X and Y of the Escomeses
        :rtype: list of dict[str, float or False]
        """

        if isinstance(ids, int):
            ids = [ids]

        ret = dict((x, {'x': False, 'y': False}) for x in ids)

        sql = """
            SELECT e.id AS id,
                   ST_X(e.geom) AS geom_x, 
                   ST_Y(e.geom) AS geom_y,
                   v.x AS vertex_x,
                   v.y AS vertex_y
            FROM giscedata_cups_escomesa e
            LEFT JOIN giscegis_blocs_escomeses b ON b.escomesa = e.id
            LEFT JOIN giscegis_vertex v ON v.id = b.vertex
            WHERE e.id = ANY(%s);
        """

        cursor.execute(sql, (ids,))
        data = cursor.dictfetchall()

        for line in data:
            esc_id = line['id']
            if line['geom_x'] is not None:
                x = line['geom_x']
            elif line['vertex_x'] is not None:
                x = line['vertex_x']
            else:
                x = False

            if line['geom_y'] is not None:
                y = line['geom_y']
            elif line['vertex_y'] is not None:
                y = line['vertex_y']
            else:
                y = False

            ret[esc_id] = {"x": x, "y": y}

        return ret

    _columns = {
        'x': fields.function(_coords, method=True, type='float', string='x',
                             multi='_coords'),
        'y': fields.function(_coords, method=True, type='float', string='y',
                             multi='_coords')
    }


GiscedataCupsEscomesa()
