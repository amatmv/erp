# -*- coding: utf-8 -*-
from osv import osv, fields
from giscegis_base_geom.giscegis_base_geom import DROP_ON_AUTOCAD_DUMP

# Subscribe this model to the list to be updated if there is an AutoCAD dump
DROP_ON_AUTOCAD_DUMP.append('giscegis_blocs_escomeses')


class giscedata_cups_blockname(osv.osv):
    _name = 'giscedata.cups.blockname'
    _inherit = 'giscedata.cups.blockname'

    def create(self, cr, uid, vals, context=None):
        if not context:
            context = {}
        context.update({'sync': False})
        ids = self.search(cr, uid, [('name', '=', vals['name'])])
        if ids and len(ids):
            return ids[0]
        else:
            return super(osv.osv, self).create(cr, uid, vals, context)


giscedata_cups_blockname()


class giscedata_cups_escomesa(osv.osv):
    _name = 'giscedata.cups.escomesa'
    _inherit = 'giscedata.cups.escomesa'

    def get_abonats(self, cr, uid, escomeses):
        """
        Aquest mètode cerca els abonats que hi ha assignats a les escomeses donades.
        :param escomeses: llista amb ids d'escomeses (giscedata_cups_escomesa)
        """
        r = {}
        res = []
        ggbe_obj = self.pool.get('giscegis.blocs.escomeses')
        gdce_obj = self.pool.get('giscedata.cups.escomesa')
        gdp_obj = self.pool.get('giscedata.polissa')
        for _e in escomeses:
            e = gdce_obj.browse(cr, uid, _e)
            polisses_ids = gdp_obj.search(cr, uid, [('escomesa', '=', e.name),
                                                    ('state', 'not in', ['esborrany', 'pendent', 'tall', 'baixa'])])
            for p in gdp_obj.browse(cr, uid, polisses_ids):
                r['carrer'] = p.cups_direccio
                r['escala'] = p.cups.es
                r['pis'] = p.cups.pt
                r['porta'] = p.cups.pu
                comptadors = p.comptadors_actius(p.data_alta)
                if comptadors:
                    compt_obj = self.pool.get('giscedata.lectures.comptador')
                    r['n_comptador'] = compt_obj.read(cr, uid, [comptadors[0]],
                                                      ['name'])[0]['name']
                else:
                    r['n_comptador'] = '0000000'
                r['potencia'] = p.potencia
                r['escomesa'] = e.name
                r['tensio'] = p.tensio
                r['titular'] = p.titular.name
                r['et'] = p.et
                r['polissa'] = p.name
                r['numero'] = p.cups.pnp
                r['linia'] = p.linia
                ge = ggbe_obj.search(cr, uid, [('escomesa','=',_e)])
                if len(ge):
                    ge = ggbe_obj.browse(cr, uid, ge[0])
                if ge.vertex:
                    r['x'] = ge.vertex.x
                    r['y'] = ge.vertex.y
                else:
                    r['x'] = 0.0
                    r['y'] = 0.0
                res.append(r)
                r = {}
        return res


giscedata_cups_escomesa()


class GiscegisBlocsEscomeses(osv.osv):

    def create(self, cr, uid, vals, context=None):
        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'sync': False, 'active_test': False})

        # Si tenim un codi per el que buscar l'escomesa
        if 'codi' in vals:
            escomesa_ids = self.pool.get('giscedata.cups.escomesa').search(
                cr, uid, [('name', '=', vals['codi'])], context=ctx
            )
            escomesa_vals = {}
            if 'vertex' in vals and isinstance(vals['vertex'], list):
                # Tot i cridar sempre al create dels vertex
                # No els dupliquem ja que el create sobrescrit i abans de
                # crear busca si l'element ja existeix i en el cas de que
                # sigui aixi en retorna l'id.
                vertex_obj = self.pool.get('giscegis.vertex')
                vertex_id = vertex_obj.create(
                    cr, uid, vals['vertex'][0][2]
                )
                escomesa_vals['ultima_x'] = vals['vertex'][0][2]['x']
                escomesa_vals['ultima_y'] = vals['vertex'][0][2]['y']
                vals['vertex'] = vertex_id
            if 'blockname' in vals and isinstance(vals['blockname'], list):
                # Tot i cridar sempre al create del cups.blockname
                # No els dupliquem ja que el create del blockname està
                # sobrescrit i abans de crear busca si l'element ja existeix
                # i en el cas de que sigui aixi en retorna l'id.
                blockname_obj = self.pool.get('giscedata.cups.blockname')
                blockname_id = blockname_obj.create(
                        cr, uid, vals['blockname'][0][2]
                )
                escomesa_vals['blockname'] = blockname_id
                vals['blockname'] = blockname_id
            if escomesa_vals and escomesa_ids:
                vals['escomesa'] = escomesa_ids[0]
                self.pool.get('giscedata.cups.escomesa').write(
                        cr, uid, escomesa_ids, escomesa_vals
                )

        return super(osv.osv, self).create(cr, uid, vals, context)

    def write(self, cr, uid, ids, vals, context=None):
        if not context:
            context = {}

        context.update({'sync': False})

        if not(isinstance(ids, list)):
            ids = [ids]

        blocks_obj = self.pool.get('giscegis.blocs.escomeses')
        blocks = blocks_obj.read(cr, uid, ids, ['escomesa'])
        for block in blocks:
            escomesa_vals = {}
            if 'blockname' in vals and isinstance(vals['blockname'], list):
                # Tot i cridar sempre al create del cups.blockname
                # No els dupliquem ja que el create del blockname està
                # sobrescrit i abans de crear busca si l'element ja existeix
                # i en el cas de que sigui aixi en retorna l'id.
                blockname_obj = self.pool.get('giscedata.cups.blockname')
                blockname_id = blockname_obj.create(
                        cr, uid, vals['blockname'][0][2]
                )
                escomesa_vals['blockname'] = blockname_id
                vals['blockname'] = blockname_id
            if 'vertex' in vals and isinstance(vals['vertex'], list):
                # Tot i cridar sempre al create dels vertex
                # No els dupliquem ja que el create sobrescrit i abans de
                # crear busca si l'element ja existeix i en el cas de que
                # sigui aixi en retorna l'id.
                vertex_obj = self.pool.get('giscegis.vertex')
                vertex_id = vertex_obj.create(cr, uid, vals['vertex'][0][2])
                escomesa_vals['ultima_x'] = vals['vertex'][0][2]['x']
                escomesa_vals['ultima_y'] = vals['vertex'][0][2]['y']
                vals['vertex'] = vertex_id
            if escomesa_vals and block['escomesa']:
                self.pool.get('giscedata.cups.escomesa').write(
                        cr, uid, [block['escomesa'][0]], escomesa_vals
                )

        return super(osv.osv, self).write(cr, uid, ids, vals, context)

    _name = 'giscegis.blocs.escomeses'
    _description = 'Blocs Escomeses'
    _columns = {
      'name': fields.char('ObjectId', size=10, required=True),
      'vertex': fields.many2one('giscegis.vertex', 'Vertex',
                                ondelete='set null'),
      'width': fields.float('Width'),
      'height': fields.float('Height'),
      'rotation': fields.float('Rotation'),
      'codi': fields.char('ID Nodo', size=25),
      'escomesa': fields.many2one('giscedata.cups.escomesa', 'Escomesa'),
      'blockname': fields.many2one('giscedata.cups.blockname', 'BlockName'),
      'node': fields.many2one('giscegis.nodes', 'Node', ondelete='set null'),
    }

    _defaults = {

    }

    _order = "name, id"


GiscegisBlocsEscomeses()
