# coding=utf-8

import logging
import pooler


def up(cursor, installed_version):

    logger = logging.getLogger('openerp.migration')

    logger.info('Migrating the node_id values from blocs into Escomeses.')

    pool = pooler.get_pool(cursor.dbname)
    esc_obj = pool.get("giscedata.cups.escomesa")
    esc_obj.fill_node_id(cursor, 1)

    logger.info('Migration successfull')


def down(cursor, installed_version):
    pass


migrate = up
