# coding=utf-8

import logging
import pooler


def up(cursor, installed_version):

    logger = logging.getLogger('openerp.migration')
    logger.info('Migrating the X & Y values from vertex as a geom '
                'field into Escomeses.')

    pool = pooler.get_pool(cursor.dbname)
    esc_obj = pool.get("giscedata.cups.escomesa")
    esc_obj.fill_geom(cursor, 1)
    esc_obj.fill_last_geom(cursor, 1)

def down(cursor, installed_version):
    pass


migrate = up
