# Translation of OpenERP Server.
# This file contains the translation of the following modules:
# 
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: GISCE-ERP\n"
"Report-Msgid-Bugs-To: https://github.com/gisce/erp/issues\n"
"POT-Creation-Date: 2012-04-25 16:11:39+0000\n"
"PO-Revision-Date: 2012-03-28 15:54+0000\n"
"Last-Translator: <>\n"
"Language-Team: English (United States) (http://trad.gisce.net/projects/p/erp/language/en_US/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: en_US\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. module: giscegis_blocs_escomeses
#: field:giscegis.blocs.escomeses,node:0
msgid "Node"
msgstr ""

#. module: giscegis_blocs_escomeses
#: constraint:ir.model:0
msgid ""
"The Object name must start with x_ and not contain any special character !"
msgstr ""

#. module: giscegis_blocs_escomeses
#: field:giscegis.blocs.escomeses,name:0
msgid "ObjectId"
msgstr ""

#. module: giscegis_blocs_escomeses
#: field:giscegis.blocs.escomeses,blockname:0
msgid "BlockName"
msgstr ""

#. module: giscegis_blocs_escomeses
#: field:giscegis.blocs.escomeses,width:0
msgid "Width"
msgstr ""

#. module: giscegis_blocs_escomeses
#: field:giscegis.blocs.escomeses,vertex:0
msgid "Vertex"
msgstr ""

#. module: giscegis_blocs_escomeses
#: field:giscegis.blocs.escomeses,height:0
msgid "Height"
msgstr ""

#. module: giscegis_blocs_escomeses
#: field:giscegis.blocs.escomeses,codi:0
msgid "ID Nodo"
msgstr ""

#. module: giscegis_blocs_escomeses
#: model:ir.module.module,shortdesc:giscegis_blocs_escomeses.module_meta_information
msgid "GISCE GIS Blocs Escomeses"
msgstr ""

#. module: giscegis_blocs_escomeses
#: model:ir.model,name:giscegis_blocs_escomeses.model_giscegis_blocs_escomeses
msgid "Blocs Escomeses"
msgstr ""

#. module: giscegis_blocs_escomeses
#: field:giscegis.blocs.escomeses,rotation:0
msgid "Rotation"
msgstr ""

#. module: giscegis_blocs_escomeses
#: field:giscegis.blocs.escomeses,escomesa:0
msgid "Escomesa"
msgstr ""
