# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
import ast


class BaseTest(testing.OOTestCase):

    def test_escomeses_geom_integrity_on_create(self):
        """
        Tests if the value of geom and last_geom from escomese is recorded and
        updated properly when a bloc_escomesa is created
        :return: None
        """

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            # 1-BUSCAR O CREAR ELS ELEMENTS NECESARIS PER A CREAR ELS BLOCS DE
            # LES ESCOMESES
            vertex_obj = self.openerp.pool.get('giscegis.vertex')
            vertex1 = vertex_obj.read(cursor, uid, 1, ['x', 'y', 'name'])

            escomesa_obj = self.openerp.pool.get('giscedata.cups.escomesa')
            vals_escomesa_sense_geom = {
                'name': 'escomesa_t'
            }
            id_escomesa = escomesa_obj.create(
                cursor, uid, vals_escomesa_sense_geom
            )
            escomesa = escomesa_obj.read(
                cursor, uid, id_escomesa, ['geom', 'last_geom']
            )
            self.assertEqual(escomesa['geom'], False)
            self.assertEqual(escomesa['last_geom'], False)

            bloc_escomesa_obj = self.openerp.pool.get(
                'giscegis.blocs.escomeses'
            )

            # 2-CREAR ELS BLOCS
            vals_bloc_create = {
                'name': 'block_prova',
                'vertex': [["", "", vertex1]],
                'codi': 'escomesa_t',
                'blockname': [["", "", {'name': "CONTA-AT"}]]
            }
            bloc_escomesa_obj.create(
                cursor, uid, vals_bloc_create
            )

            # 3-MIRAR QUE ELS VALORS AFEGITS (GEOM I LAST_GEOM) DE LES ESCOMESES
            #  SIGUI CORRECTE
            escomesa_obj.fill_geom(cursor, 1)
            escomesa_obj.fill_last_geom(cursor, 1)
            escomesa = escomesa_obj.read(
                cursor, uid, id_escomesa,
                ['name', 'geom', 'last_geom', 'x', 'y']
            )
            self.assertEqual(round(vertex1['x'], 4), escomesa['x'])
            self.assertEqual(round(vertex1['y'], 3), escomesa['y'])
            self.assertEqual(
                escomesa['geom'], "POINT({} {})".format(
                    round(vertex1['x'], 4), round(vertex1['y'], 3)
                )
            )
            self.assertEqual(
                escomesa['last_geom'], "POINT({} {})".format(
                    vertex1['x'], vertex1['y']
                )
            )

    def test_escomeses_geom_integrity_on_write(self):
        """
        Tests if the value of geom and last_geom from escomeses is recorded and
        updated properly when a bloc_escomesa is writed/updated
        :return: None
        """

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            # 1-BUSCAR O CREAR ELS ELEMENTS NECESARIS PER A CREAR ELS BLOCS DE
            # LES ESCOMESES
            vertex_obj = self.openerp.pool.get('giscegis.vertex')
            vertex1 = vertex_obj.read(cursor, uid, 1, ['x', 'y'])
            vertex2 = vertex_obj.read(cursor, uid, 2, ['x', 'y'])

            escomesa_obj = self.openerp.pool.get('giscedata.cups.escomesa')
            vals_escomesa_sense_geom = {
                'name': 'escomesa_t'
            }
            id_escomesa = escomesa_obj.create(
                cursor, uid, vals_escomesa_sense_geom
            )

            bloc_escomesa_obj = self.openerp.pool.get(
                'giscegis.blocs.escomeses'
            )

            # 2-CREAR ELS BLOCS
            vals_bloc_create = {
                'name': 'block_prova',
                'vertex': [["", "", vertex1]],
                'codi': 'escomesa_t',
                'blockname': [["", "", {'name': "CONTA-AT"}]]
            }
            bloc_escomesa_id = bloc_escomesa_obj.create(
                cursor, uid, vals_bloc_create
            )

            # 3-ACTUALITZAR BLOCS
            vals_bloc_update = {
                'vertex': [["", "", vertex2]],
                'blockname': [["", "", {'name': "CONTA-AT"}]]
            }
            bloc_escomesa_obj.write(
                cursor, uid, bloc_escomesa_id, vals_bloc_update
            )

            # 4-MIRAR QUE EL CAMP GEOM I EL CAMP LAST_GEOM S'HAGIN ACTUALITZAT
            # CORRECTAMENT.
            escomesa_obj.fill_geom(cursor, 1)
            escomesa_obj.fill_last_geom(cursor, 1)
            escomesa = escomesa_obj.read(
                cursor, uid, id_escomesa,
                ['name', 'geom', 'last_geom', 'x', 'y']
            )
            self.assertEqual(round(vertex2['x'], 4), escomesa['x'])
            self.assertEqual(round(vertex2['y'], 4), escomesa['y'])
            self.assertEqual(
                escomesa['geom'], "POINT({} {})".format(
                    round(vertex2['x'], 4), round(vertex2['y'], 4)
                )
            )
            self.assertEqual(
                escomesa['last_geom'], "POINT({} {})".format(
                    vertex2['x'], vertex2['y']
                )
            )
