# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS (Base)",
    "description": """Mòdul base de GISCE GIS""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "base_extended"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscegis_base_view.xml",
        "giscegis_base_data.xml",
        "security/giscegis_base_security.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
