# *-* coding: utf-8 *-*

from osv import osv, fields


class GiscegisRasesGeom(osv.osv):
    _name = 'giscegis.rases.geom'
    _columns = {
        'gid': fields.integer('Gid', required=True),
        'id': fields.integer('Id'),
        'color': fields.integer('Color'),
        'prof': fields.integer('Profundictat'),
        'textstring': fields.text('Text'),
        'text_size': fields.float('Text Size'),
        'text_angle': fields.float('Text Angle'),
        "is_closed": fields.boolean("Closed geometry")
    }

    def exists_index(self, cursor, index_name):
        """
        Checks if a index exists

        :param cursor: Database cursor
        :param index_name:
        :return: If index exists
        :rtype: bool
        """
        sql_indx = """
        SELECT count(relname) FROM pg_class
        LEFT JOIN pg_index ON indexrelid = oid
        WHERE relname = %s
        """
        cursor.execute(sql_indx, (index_name,))
        res = cursor.fetchone()
        return res[0] > 0

    def init(self, cursor):
        conf_obj = self.pool.get('res.config')
        srid = int(conf_obj.get(cursor, 1, 'giscegis_srid', '25831'))

        sql_index_texstring = """
        CREATE INDEX giscegis_rases_geom_index_textstring
        ON giscegis_rases_geom (textstring)
        WHERE textstring IS NULL;
        """
        if not self.exists_index(cursor, "giscegis_rases_geom_index_textstring"):
            cursor.execute(sql_index_texstring)

        sql_index_geom = """
            CREATE INDEX giscegis_rases_geom_index_geom
            ON giscegis_rases_geom USING GIST (geom)
            """

        cursor.execute('''
            SELECT * FROM information_schema.columns
            WHERE table_name = 'giscegis_rases_geom'
            AND COLUMN_NAME = 'geom'

            ''')
        if not cursor.fetchone():
            sql_select = """SELECT
                AddGeometryColumn('public','giscegis_rases_geom',
                'geom',%(srid)s,'geometry',2, true);"""
            cursor.execute(sql_select, {'srid': srid})
            cursor.commit()

        if not self.exists_index(cursor, "giscegis_rases_geom_index_geom"):
            cursor.execute(sql_index_geom)
        return True

GiscegisRasesGeom()
