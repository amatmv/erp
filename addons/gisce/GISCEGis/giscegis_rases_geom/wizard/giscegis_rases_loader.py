# -*- coding: utf-8 -*-
import base64
import itertools
from StringIO import StringIO
from zipfile import ZipFile, BadZipfile
import os


from tools.translate import _
from osv import osv, fields
import shapefile
import netsvc

from tools.misc import email_send


class GiscegisRasesLoader(osv.osv_memory):

    _name = 'giscegis.rases.loader'

    def load_shp(self, cursor, uid, shp_data, dbf_data, update_diameter=False, update_closed=False):
        """
        Loads a shp file into the database

        :param cursor: Database cursor
        :param uid: User id
        :param shp_data: Data of the shp file(geometry)
        :param dbf_data: Dbf data(attributes)
        :param update_diameter: Updates the diameter character
        :type update_diameter: bool
        :param update_closed:  Updates the closed field
        :type update_closed: bool
        :return: None
        """
        base_geom = self.pool.get('giscegis.base.geom')
        sql_diameter = """
        UPDATE giscegis_rases_geom_tmp
        SET textstring=regexp_replace(textstring,'\%\%[c|C]','⌀', 'g');
        """
        sql_isclosed = """
        UPDATE giscegis_rases_geom_tmp
        SET "is_closed" = st_isclosed(geom)
        """
        sf = shapefile.Reader(shp=shp_data, dbf=dbf_data)
        shapes = sf.shapes()
        records = sf.records()

        conf_obj = self.pool.get('res.config')
        srid = int(conf_obj.get(cursor, uid, 'giscegis_srid', 25830))

        headers = {}
        for index, field in enumerate(sf.fields[1:]):
            headers[field[0].lower()] = index

        insert_sql = '''
                INSERT INTO giscegis_rases_geom_tmp
                (gid,id,text_angle,color,text_size,textstring,geom)
                VALUES (%(id)s,%(id)s,%(text_angle)s,%(color)s,%(text_size)s,
                %(textstring)s,ST_GeomFromText(%(geom)s,%(srid)s))
                '''
        cursor.execute('''SELECT MAX(id)+1 AS last_index
                       FROM giscegis_rases_geom_tmp;''')
        data = cursor.fetchone()[0]
        if data:
            last_index = data
        else:
            last_index = 0
        insert_data = []
        base_model = self.pool.get('giscegis.base.geom')
        for index, shape, record in itertools.izip(itertools.count(), shapes, records):
            if len(shapes[index].points) > 1:
                points = [shapes[index].points]
                wkt_geometry = base_model.to_wkt_multilinestring(points)
            elif len(shapes[index].points) == 1:
                centroide = records[index][headers['centroide']]
                if 'centroide' in headers and centroide.strip() != '':
                    p = [map(float, centroide.split(','))[0:2]]
                else:
                    p = shapes[index].points
                wkt_geometry = base_model.to_wkt_point([p])
            else:
                wkt_geometry = None

            row = {'geom': wkt_geometry,
                   'srid': srid,
                   'color': None,
                   'text_angle': None,
                   'text_size': None,
                   'textstring': None}

            if headers:
                if 'text_angle' in headers:
                    txt_angle = record[headers['text_angle']]
                    row['text_angle'] = base_geom.sanitize_record(txt_angle, 'float')
                if 'text_size' in headers:
                    txt_size = record[headers['text_size']]
                    row['text_size'] = base_geom.sanitize_record(txt_size, 'float')
                if 'textstring' in headers:
                    textstring = record[headers['textstring']]
                    row['textstring'] = textstring.decode('latin1')
                if 'color' in headers:
                    row['color'] = record[headers['color']]
            if 'id' in headers:
                record = records[index]
                row['id'] = record[headers['id']]
            else:
                row['id'] = index + last_index
            insert_data.append(row)

        cursor.executemany(insert_sql, insert_data)
        if update_diameter:
            cursor.execute(sql_diameter)
        if update_closed:
            cursor.execute(sql_isclosed)

    def import_data_shp(self, cursor, uid, ids, context=None):
        """
        Attends the load of the shapes from the wizard

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: Id of the wizard
        :type ids: int
        :param context: OpenERP context
        :type context: dict
        :return: None
        :rtype: None
        """

        if context is None:
            context = {}

        base_geom = self.pool.get('giscegis.base.geom')
        obj_msg = self.pool.get("giscegis.load.message")
        obj_config = self.pool.get("res.config")

        from_email = obj_config.get(
            cursor, uid,
            "email_from", "no-reply@gisce.net"
        )

        not_fount_msg = _(u"No s'han trobat fitxers .shp o .dbf en el fitxer,s'atura la importació")
        wizard = self.browse(cursor, uid, ids[0], context)
        wizard.write({'status': ''})
        logger = netsvc.Logger()

        sql_create = """
        CREATE TABLE giscegis_rases_geom_tmp (
         like giscegis_rases_geom
          including defaults
          including constraints
          including indexes
        );
        """
        srid = base_geom.get_srid(cursor, uid)
        cursor.execute(sql_create, (srid,))

        obj_users = self.pool.get("res.users")
        message_id = obj_msg.create(cursor, uid, {})
        message = obj_msg.browse(cursor, uid, message_id)
        message.start_load()

        user_data = obj_users.browse(cursor, uid, uid)
        user_email = user_data.address_id.email

        email_send(
            from_email,
            [user_email],
            _("Carrega de fitxers de rases"),
            message.get_message(),
            debug=True
        )

        sql_diameter = """
        UPDATE giscegis_rases_geom_tmp
        SET textstring=regexp_replace(textstring,'\%\%[c|C]','⌀', 'g');
        """
        sql_isclosed = """
        UPDATE giscegis_rases_geom_tmp
        SET "is_closed" = st_isclosed(geom)
        """

        if wizard.file_rases:
            data = base64.decodestring(wizard.file_rases)
        else:
            logger.notifyChannel(
                'SHPLoader', netsvc.LOG_INFO,
                _(u"No s'ha especificat el fitxer de rases"))

            message.log_message(_(u"No s'ha especificat el fitxer de rases"))
            message.end_load(_("Error"))

            base_geom.add_status(
                cursor, uid, ids,
                _(u"No s'ha especificat el fitxer de rases"), wizard)
        if wizard.file_rases:
            s = StringIO(data)
            try:
                input_zip = ZipFile(s)
            except BadZipfile:
                logger.notifyChannel(
                    'SHPLoader', netsvc.LOG_WARNING,
                    _('Fitxer de rases corrupte'))

                message.log_message(_(u"No s'ha especificat el fitxer de rases"))
                message.end_load(_("Error"))

                base_geom.add_status(
                    cursor, uid, ids, _('Fitxer LAT corrupte'), wizard)
                return
            filenames = sorted(input_zip.namelist())
            if filenames:
                base_geom.truncate_table(cursor, uid, 'giscegis_rases_geom_tmp')
                shp_data = None
                dbf_data = None
                for filename in filenames:
                    if os.path.splitext(filename)[1].lower() == '.dbf':
                        dbf_data = StringIO(input_zip.read(filename))
                    if os.path.splitext(filename)[1].lower() == '.shp':
                        shp_data = StringIO(input_zip.read(filename))
                    if shp_data and dbf_data:
                        logger.notifyChannel(
                            'SHPLoader',
                            netsvc.LOG_INFO,
                            _('Carregant  fitxer:{}').format(filename))

                        message.log_message(_('Carregant  fitxer:{}').format(filename))

                        self.load_shp(cursor, uid, shp_data, dbf_data,
                                      False, False)
                        shp_data = None
                        dbf_data = None
            else:
                logger.notifyChannel(
                    'SHPLoader', netsvc.LOG_WARNING, not_fount_msg)
                base_geom.add_status(cursor, uid, ids, not_fount_msg, wizard)

            cursor.execute(sql_diameter)
            cursor.execute(sql_isclosed)

            logger.notifyChannel(
                'SHPLoader', netsvc.LOG_INFO,
                _('Dades carregades correctament'))

            message.log_message(_('Dades carregades correctament'))

            base_geom.add_status(
                cursor, uid, ids,
                _('Dades carregades correctament'), wizard)
            base_geom.switch_table(
                cursor, uid, "giscegis_rases_geom_tmp","giscegis_rases_geom", context
            )
            status_cache = base_geom.remove_tms_cache(cursor, uid, 'rases')
            if status_cache:
                logger.notifyChannel(
                    'SHPLoader', netsvc.LOG_INFO,
                    _('Cache netejat correctament'))

                message.log_message(_('Cache netejat correctament'))
                message.end_load(_("Correcte"))

                base_geom.add_status(
                    cursor, uid, ids, _('Cache netejat correctament'), wizard)
            else:
                logger.notifyChannel(
                    'SHPLoader', netsvc.LOG_ERROR,
                    _('Error al eliminar el cache'))

                message.log_message(_('Error al eliminar el cache'))
                message.end_load(_("Error"))

                base_geom.add_status(
                    cursor, uid, ids, _('Error al eliminar el cache'), wizard)

            email_send(
                from_email,
                [user_email],
                _("Carrega de fitxers de rases"),
                message.get_message(),
                debug=True
            )

    def _get_default_url(self, cursor, uid, context=None):
        """
        Method to get the default url to load rases

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param context: OpenERP context
        :type context:dict
        :return: URL
        :rtype: str
        """
        url = self.pool.get("res.config").get(cursor, uid, "giscegis_gis_url")
        return "{}/load/rases".format(url)

    _columns = {
        'file_rases': fields.binary('Fixer Rases', help=()),
        'status': fields.text(_('Resultat')),
        "url": fields.text("URL")
    }
    _defaults = {
        'state': lambda *a: 'init',
        'url': _get_default_url
    }


GiscegisRasesLoader()
