# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS v3 Trafos",
    "description": """Mòdul pel GIS v3 de Trafos""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "giscegis_base",
        "giscedata_cts",
        "giscedata_transformadors",
        "giscegis_base_geom",
        "giscegis_blocs_transformadors",
        "giscegis_blocs_transformadors_reductors",
        "giscegis_trafos"
    ],
    "init_xml": [],
    "demo_xml": [
    ],
    "update_xml":[
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
