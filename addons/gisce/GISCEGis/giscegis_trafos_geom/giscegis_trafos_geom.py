# -*- coding: utf-8 -*-
from __future__ import absolute_import
from osv import fields, osv
from tools import config


class GiscegisTrafosGeom(osv.osv):
    _name = "giscegis.trafos.geom"
    _description = "Trafos Llistat per GIS v3"
    _auto = False
    _columns = {
        'id': fields.integer('Id', readonly=True),
        'name': fields.char('Text', size=120, readonly=True),
        'rotation': fields.float('Rotation', readonly=True),
        'geom': fields.char('Geom', size=120, readonly=True),
        'texte': fields.char('Texte', size=120, readonly=True),
        "materialize_date": fields.datetime("Fecha creación vista"),
    }

    _defaults = {

    }
    _order = 'id'

    def init(self, cursor):

        cursor.execute("""
        DROP MATERIALIZED VIEW IF EXISTS giscegis_trafos_geom;
        CREATE MATERIALIZED VIEW giscegis_trafos_geom AS
            SELECT ('1' || block.transformadors::varchar)::int AS id,
                   st_setsrid(st_makepoint(vertex.x, vertex.y), %(srid)s) AS geom,
                   'TRANSFORMADOR'::text AS symbol,
                   block.rotation - 90 AS rotation,
                   'TRF-' || trafo.name::text || E'\n'::text 
                   || COALESCE(trafo.potencia_nominal::text, 'S/P'::text)
                   || ' kVA' AS texte,
                   now() AS materialize_date
            FROM   giscegis_blocs_transformadors block
                   LEFT JOIN giscegis_vertex vertex
                          ON block.vertex = vertex.id
                   LEFT JOIN giscedata_transformador_trafo trafo
                          ON trafo.id = block.transformadors
            UNION
            SELECT ('2' || block.transformadors::varchar)::int AS id,
                   st_setsrid(st_makepoint(vertex.x, vertex.y), %(srid)s) AS geom,
                   'TRANSFORMADOR_R'::text AS symbol,
                   block.rotation - 90 AS rotation,
                   'TRF-' || trafo.name::text || E'\n'::text 
                   || COALESCE(trafo.potencia_nominal::text, 'S/P'::text)
                   || ' kVA' AS texte,
                   now() AS materialize_date
            FROM   giscegis_blocs_transformadors_reductors block
                   LEFT JOIN giscegis_vertex vertex
                          ON block.vertex = vertex.id
                   LEFT JOIN giscedata_transformador_trafo trafo
                          ON trafo.id = block.transformadors;
                          
        CREATE INDEX giscegis_trafos_geom_geom 
        ON giscegis_trafos_geom USING gist (geom);
        """, {'srid': config.get('srid', 25830)})

        cursor.execute("""
            DROP MATERIALIZED VIEW IF EXISTS giscegis_blocs_trafos_geom;
        """)

        cursor.execute("""
            CREATE MATERIALIZED VIEW public.giscegis_blocs_trafos_geom AS
                SELECT 
                    block.transformadors AS id,
                    st_setsrid(
                        st_makepoint(vertex.x, vertex.y), %(srid)s
                    ) AS geom,
                    'TRANSFORMADOR'::text AS symbol,
                    block.rotation - 90 AS rotation,
                    'TRF-' || trafo.name::text || E'\n'::text 
                    || COALESCE(trafo.potencia_nominal::text, 'S/P'::text)
                    || ' kVA' AS texte,
                    now() AS materialize_date
                FROM giscegis_blocs_transformadors block
                LEFT JOIN giscegis_vertex vertex ON vertex.id = block.vertex
                LEFT JOIN giscedata_transformador_trafo trafo ON 
                    trafo.id = block.transformadors;
                
            CREATE INDEX "giscegis_blocs_trafos_geom_geom" 
            ON giscegis_blocs_trafos_geom USING gist (geom);
        """, {'srid': config.get('srid', 25830)})

        cursor.execute("""
            DROP MATERIALIZED VIEW IF 
            EXISTS giscegis_blocs_trafos_reductors_geom;
        """)

        cursor.execute("""
            CREATE MATERIALIZED VIEW public.giscegis_blocs_trafos_reductors_geom AS
                SELECT 
                    block.transformadors as id,
                    st_setsrid(
                        st_makepoint(vertex.x, vertex.y), %(srid)s
                    ) AS geom,
                    'TRANSFORMADOR_R'::text AS blockname,
                    block.rotation - 90 AS rotation,
                    'TRF-' || trafo.name::text || E'\n'::text 
                    || COALESCE(trafo.potencia_nominal::text, 'S/P'::text)
                    || ' kVA' AS texte,
                    now() AS materialize_date
                FROM giscegis_blocs_transformadors_reductors block
                LEFT JOIN giscegis_vertex vertex ON vertex.id = block.vertex
                LEFT JOIN giscedata_transformador_trafo trafo ON 
                    trafo.id = block.transformadors;
                    
            CREATE INDEX "giscegis_blocs_trafos_reductors_geom_geom" ON 
            giscegis_blocs_trafos_reductors_geom USING gist (geom);
        """, {'srid': config.get('srid', 25830)})

    def get_srid(self, cursor, uid):
        """
        :param cursor: cursor Database cursor
        :param uid: integer User identifier
        :return: Returns a list of srids in geom field
        """

        return [config.get('srid', 25830)]

    def recreate(self, cursor, uid):
        cursor.execute("""
            REFRESH MATERIALIZED VIEW giscegis_blocs_trafos_geom;
            REFRESH MATERIALIZED VIEW giscegis_blocs_trafos_reductors_geom;
            REFRESH MATERIALIZED VIEW giscegis_trafos_geom;
        """)


GiscegisTrafosGeom()
