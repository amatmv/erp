# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Polyline",
    "description": """Modulo para representar polilineas en el GIS""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_vertex",
        "giscegis_base"
    ],
    "init_xml": [],
    "demo_xml": [
        "giscegis_polyline_demo.xml"
    ],
    "update_xml":[
        "giscegis_polyline_view.xml",
        "ir.model.access.csv",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
