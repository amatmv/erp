# -*- coding: utf-8 -*-
from osv import osv, fields
from giscegis_base_geom.giscegis_base_geom import DROP_ON_AUTOCAD_DUMP

# Subscribe this model to the list to be updated if there is an AutoCAD dump
DROP_ON_AUTOCAD_DUMP.append('giscegis_polyline')
DROP_ON_AUTOCAD_DUMP.append('giscegis_polyline_vertex')
DROP_ON_AUTOCAD_DUMP.append('giscegis_polyline_vertex_rel')


class giscegis_polyline_vertex(osv.osv):
    _name = "giscegis.polyline.vertex"
    _description = "Vertex Polilyne"

    _inherits = {'giscegis.vertex': 'vertex'}

    _columns = {
      'vertex': fields.many2one('giscegis.vertex', 'Vertex', required=True, 
                                ondelete='cascade'),
      'sequence': fields.integer('Ordre'),
    }

    _defaults = {
      'sequence': lambda *a: 0,
    }

    def create(self, cursor, uid, vals, context=None):
        """Mètode sobreescrit per escriure als camps many2one des
        d'aquí.

        Ens hem d'assegurar de què els mètodes create() dels objectes
        referenciats estiguin alhora sobreescrits per tal què comprovin
        si l'objecte existeix o no."""
        _fields = [('vertex', 'giscegis.vertex'),]
        for field in _fields:
            if field[0] in vals and isinstance(vals[field[0]], list):
                obj = self.pool.get(field[1])
                remote_id = obj.create(cursor, uid, vals[field[0]][2])
                if remote_id:
                    vals[field[0]] = remote_id
        return super(osv.osv, self).create(cursor, uid, vals)

    def clean(self, cr, uid):
        cr.execute("delete from giscegis_polyline_vertex")
        cr.commit()
        return True

giscegis_polyline_vertex()

class giscegis_polyline(osv.osv):
    _name = 'giscegis.polyline'
    _description = 'Polyline'
    _columns = {
      'name': fields.char('EHandle', size=10, required=True),
      'vertex_ids': fields.many2many('giscegis.polyline.vertex', 'giscegis_polyline_vertex_rel', 'polyline_id', 'vertex_id', 'Vertexs', ondelete='cascade'),
    }

    _order = "name, id"

    def clean(self, cr, uid):
        cr.execute("delete from giscegis_polyline")
        cr.commit()
        return True

giscegis_polyline()
