# -*- coding: iso-8859-1 -*-
from osv import osv,fields

class giscegis_blocs_caixesbt_171(osv.osv):

    _name = 'giscegis.blocs.caixesbt.171'
    _description = 'Blocs Caixesbt 171'
    _columns = {
      'id': fields.integer('id'),
      'name': fields.char('BlockName', size=50, required=True),
      'rotation': fields.float('Rotation'),
      'x': fields.float('X'),
      'y': fields.float('Y'),
      'codi': fields.char('Codi', size=50),
      'blockname': fields.char('blockname', size=50),
      'text': fields.char('text', size=128),
    }

    _auto = False

    _defaults = {

    }

    _order = "name, id"

    def init(self, cr):
        cr.execute("""drop view if exists giscegis_blocs_caixesbt_171""")
        cr.execute("""
          create or replace view giscegis_blocs_caixesbt_171 as (
            SELECT c.id, c.name AS caixa, v.x, v.y, c.rotation, b.name AS blockname, (b.name::text || ' '::text) ||
            CASE
                WHEN b.name::text ~~* 'ADU%'::text OR 
                     b.name::text ~~* 'CGP%'::text OR 
                     b.name::text ~~* 'DSP%'::text 
                THEN COALESCE(c.intensitat_caixa, 0::double precision)::text
                     || 'A\nFusible: '::text 
                     || COALESCE(c.intensitat_fusible, 0::double precision)::text 
                     || 'A'::text
                ELSE ' '::text
            END AS texte
            FROM giscegis_blocs_caixesbt c
            LEFT JOIN giscegis_vertex v ON c.vertex = v.id
            LEFT JOIN giscegis_blocs_caixesbt_blockname b ON b.id = c.blockname
            ORDER BY c.id
          )
        """)

giscegis_blocs_caixesbt_171()
