# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Blocs Caixes BT per v 1.7.1",
    "description": """Modul de Caixes BT per Autocad""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_blocs_caixesbt",
        "giscegis_vertex",
        "giscegis_nodes",
        "giscegis_base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "ir.model.access.csv",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
