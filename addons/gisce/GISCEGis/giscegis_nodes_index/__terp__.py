# -*- coding: utf-8 -*-
{
    "name": "Index Giscegis Nodes",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Index giscegis_nodes
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "base_index",
        "giscegis_base_geom",
        "giscegis_nodes",
        "giscegis_search_type"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscegis_search_type_data.xml"
    ],
    "active": False,
    "installable": True
}
