from giscegis_base_index.giscegis_base_index import GiscegisBaseIndex


class GiscegisNodes(GiscegisBaseIndex):
    """
    Module to allow to indexing the giscegis.nodes
    """
    _name = 'giscegis.nodes'
    _inherit = 'giscegis.nodes'

    def __init__(self, pool, cursor):
        super(GiscegisNodes, self).__init__(pool, cursor)

    _index_title = '{obj.name}'
    _index_summary = '{obj.name}'

    _index_fields = {
        'name': True
    }

    def get_lat_lon(self, cursor, uid, item, context=None):
        sql = '''SELECT
              ST_Y(ST_TRANSFORM(geom,4326)),
              ST_X(ST_TRANSFORM(geom,4326))
            FROM
              giscegis_nodes n
            WHERE
              n.id = %(id)s
        '''
        cursor.execute(sql, {"id": item.id})
        res = cursor.fetchone()
        if not res:
            res = (0, 0)
        return res

GiscegisNodes()
