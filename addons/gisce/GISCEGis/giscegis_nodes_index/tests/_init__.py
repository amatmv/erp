import json

from addons import get_module_resource
from destral import testing
from destral.transaction import Transaction


class GiscegisNodesIndexTest(testing.OOTestCase):
    def test_index(self):
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            nodes_obj = self.openerp.pool.get('giscegis.nodes')
            vertex_obj = self.openerp.pool.get('giscegis.vertex')
            vertex_json = get_module_resource(
                'giscegis_nodes_geom',
                'tests',
                'vertex.json'
            )
            with open(vertex_json, 'r') as f:
                vertex = json.load(f)
                v_ids = {}
                for v in vertex:
                    v_id = v.pop('id')
                    v_ids[v_id] = vertex_obj.create(cursor, uid, v)

            nodes_json = get_module_resource(
                'giscegis_nodes_geom',
                'tests',
                'nodes.json'
            )
            with open(nodes_json, 'r') as f:
                nodes = json.load(f)
                for node in nodes:
                    node.pop('id')
                    v = node.pop('vertex')
                    node['vertex'] = v_ids[v[0]]
                    nodes_obj.create(cursor, uid, node)
            ids = nodes_obj.search(cursor, uid, [])
            for identifier in ids:
                nodes_obj.index(identifier)
