# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS v3 Empalmes AT",
    "description": """Modul per la visualitzacio de empalmes AT al visor GIS""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_base_geom",
        "giscegis_vertex",
        "giscegis_blocs_empalmes_grup",
        "giscegis_empalmes_at"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
