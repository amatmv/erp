# *-* coding: utf-8 *-*
from osv import osv, fields
from tools import config


class GiscegisEmpalmesAtGeom(osv.osv):
    """
    Empalmes AT per Giscegis v3
    """

    _name = "giscegis.empalmes.at.geom"
    _auto = False
    _description = "Empalmes AT per Giscegis v3"
    _columns = {
        "id": fields.integer("Id", readonly=True),
        "symbol": fields.text("Symbol", readonly=True),
        "geom": fields.char("Geom", readonly=True, size=256),
        "materialize_date": fields.datetime("Fecha creación vista"),
    }

    def init(self, cursor):
        """
        Initializes the module creating the materialized veiw

        :param cursor: Database cursor
        :return: None
        :rtype: None
        """

        cursor.execute("""
            DROP MATERIALIZED VIEW IF EXISTS giscegis_empalmes_at_geom;
        """)

        cursor.execute("""
            CREATE MATERIALIZED VIEW giscegis_empalmes_at_geom AS
                SELECT 
                    block.id AS id,
                    blockname.name AS symbol,
                    ST_SETSRID(
                        ST_MAKEPOINT(vertex.x, vertex.y), %(srid)s
                    ) AS geom,
                    now() AS materialize_date
                FROM giscegis_blocs_empalmes_grup AS block
                LEFT JOIN giscegis_blocs_empalmes_grup_blockname blockname ON 
                    block.blockname = blockname.id
                LEFT JOIN giscegis_vertex AS vertex ON block.vertex = vertex.id;
        """, {'srid': config.get('srid', 25830)})
        cursor.execute("""
        DROP INDEX IF EXISTS giscegis_blocs_empalmes_at_geom_indx;
        CREATE INDEX giscegis_blocs_empalmes_at_geom_indx
          ON giscegis_empalmes_at_geom USING gist (geom);
        """)

    def recreate(self, cursor, uid):
        """
        Recreates the materialized view

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :return: True
        :rtype: bool
        """

        cursor.execute("""
        REFRESH MATERIALIZED VIEW giscegis_empalmes_at_geom;
        """)
        return True

    def get_srid(self, cursor, uid):
        """
        Returns the SRID of the geometry

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :return: SRID
        :rtype: str
        """
        cursor.execute("""
        SELECT ST_SRID(geom) as srid FROM giscegis_empalems_at_geom LIMIT 1;
        """)
        data = cursor.fetchall()
        if len(data) > 0:
            return data[0][0]
        return False


GiscegisEmpalmesAtGeom()
