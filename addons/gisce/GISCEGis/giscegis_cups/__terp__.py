# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS CUPS",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends": [
        "base",
        "giscedata_cups_distri",
        "giscegis_vertex",
        "giscegis_base",
        "giscegis_base_geom",
        "giscegis_cataleg",
        "giscegis_nodes",
        "giscegis_node_bloc_model",
        "giscegis_escomeses_traceability",
        "giscegis_colors"
    ],
    "init_xml": [],
    "demo_xml": [
        "giscegis_cups_demo.xml"
    ],
    "update_xml": [
        "security/ir.model.access.csv",
        "giscegis_cups_view.xml",
        "giscegis_cataleg_escomesa_data.xml"
    ],
    "active": False,
    "installable": True
}
