# -*- coding: utf-8 -*-
from osv import fields
from giscegis_base_geom.fields import Point
from giscegis_base_geom.giscegis_session import SessionManaged
from tools import config


class GiscedataCupsEscomesa(SessionManaged):

    _name = 'giscedata.cups.escomesa'
    _inherit = 'giscedata.cups.escomesa'

    def _get_traceability_values(self, cr, uid, ids, field_name, arg, context):
        """
        Return the values of the fields trafo_id, ct_id, linia and tensio
        for the model GiscedataCupsEscomesa
        :param cr: Database Cursor
        :type cr: Cursor
        :param uid: User ID
        :type uid: int
        :param ids: IDS of the elements that the fields must be calculated
        :type ids: list of int
        :param field_name: Not Used
        :type field_name: Not Used
        :param arg: Not Used
        :type arg: Not Used
        :param context: Not Used
        :type context: Not Used
        :return: The computed field values for each ID in IDS.
        :rtype dict[int,dict[str,int or str or bool]]
        """

        res = {}
        for esc_id in ids:
            res[esc_id] = {
                'ct_id': False,
                'trafo_id': False,
                'linia': False,
                'tensio': False,
                'color': False
            }

        sql = """
            SELECT
                ct.id AS ct,
                t.escomesa,
                t.sortida AS linia,
                t.tensio, t.trafo,
                color.color
            FROM giscegis_escomeses_traceability t
            LEFT JOIN giscedata_cts AS ct ON ct.id=t.ct
            LEFT JOIN giscegis_colors_bt color 
                ON t.sortida = color.name::text
            WHERE t.escomesa IN %(ids)s;
        """

        cr.execute(sql, {'ids': tuple(ids)})

        for row in cr.dictfetchall():
            res[row['escomesa']] = {
                'ct_id': int(row['ct']) if row['ct'] else False,
                'trafo_id': int(row['trafo']) if row['trafo'] else False,
                'linia': str(row['linia']) if row['linia'] else False,
                'tensio': str(row['tensio']) if row['tensio'] else False,
                'color': str(row['color']) if row['color'] else '#000000'
            }

        return res
      
    _columns = {
        'geom': Point("geometry", srid=config.get('srid', 25830), select=True),
        'last_geom': Point("geometry", srid=config.get('srid', 25830)),
        'rotation': fields.integer("Rotation"),
        'node_id': fields.many2one("giscegis.nodes", "Node",
                                   ondelete='set null'),
        'ct_id': fields.function(
            _get_traceability_values, method=True, string='CT', readonly=True,
            type='many2one', relation='giscedata.cts',
            multi='_get_traceability_values'
        ),
        'trafo_id': fields.function(
            _get_traceability_values, method=True, string='Trafo',
            type='many2one', readonly=True,
            relation='giscedata.transformador.trafo',
            multi='_get_traceability_values'
        ),
        'linia': fields.function(
            _get_traceability_values, method=True, string='Línia',
            readonly=True, type='char', size='64',
            multi='_get_traceability_values'
        ),
        'tensio': fields.function(
            _get_traceability_values, method=True, string='Tensió',
            readonly=True, type='char', size='64',
            multi='_get_traceability_values'
        ),
        'color': fields.function(
            _get_traceability_values, method=True, string='Color',
            readonly=True, type='char', size='64',
            multi='_get_traceability_values'
        )
    }

    _defaults = {
        'rotation': lambda *a: 0,
    }


GiscedataCupsEscomesa()
