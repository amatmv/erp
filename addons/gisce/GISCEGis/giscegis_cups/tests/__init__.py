# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
import ast
from .test_session import *
# from .test_topologia import *


class BaseTest(testing.OOTestCase):

    def test_catalog_elements_escomeses(self):
        """
        Test that are elements for every escomesa element of the catalog but
        the CONTA-SS to be able to test loading an empty layer on qgisce
        :return: None
        """

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            # INICIALITZEM OBJECTES
            escomesa_obj = self.openerp.pool.get('giscedata.cups.escomesa')
            cataleg_obj = self.openerp.pool.get('giscegis.cataleg.cataleg')
            sum_escomeses = 0
            # SELECCIONEM UNICAMENT ELS ELEMENTS DE CATALEG QUE SIGUIN ESCOMESES
            ids_esco_cataleg = cataleg_obj.search(
                cursor, uid, [('model.model', '=', 'giscedata.cups.escomesa')]
            )
            # PER A CADA ELEMENT DEL CATALEG COMPROVAREM QUE HI HAGIN ELEMENTS
            for id_esco_cataleg in ids_esco_cataleg:
                cataleg_esco = cataleg_obj.read(
                    cursor, uid, id_esco_cataleg,
                    ['name', 'domain']
                )
                esco_ids = escomesa_obj.search(
                    cursor, uid, ast.literal_eval(cataleg_esco['domain'])
                )
                sum_escomeses += len(esco_ids)
                # UNICAMENT ELS CONTA-SS A MODE DE PROVA NO TINDRA ELEMENTS
                if cataleg_esco['name'] == "CONTA-SS":
                    # NO HI HAN D'HAVER ELEMENTS
                    self.assertEqual(esco_ids, [])
                else:
                    # HI HA D'HAVER ELEMENTS
                    self.assertNotEqual(esco_ids, [])

            # VALIDEM QUE S'HAN REVISAT TOTES LES ESCOMESES SI HEM PASSAT PER
            # TOTS ELS ELEMETNS DEL CATALEG
            self.assertEqual(sum_escomeses, 40)
