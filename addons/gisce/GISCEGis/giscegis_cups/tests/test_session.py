from destral import testing
from destral.transaction import Transaction


__all__ = [
    'TestEscomesesSession'
]


class TestEscomesesSession(testing.OOTestCase):

    def setUp(self):
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user

    def tearDown(self):
        self.txn.stop()

    def test_search_session_escomeses(self):
        """
        This test is for check the correct behaviour of the sessions and the
        escomeses
        """
        # Load ERP models
        escomeses_obj = self.openerp.pool.get('giscedata.cups.escomesa')
        session_obj = self.openerp.pool.get('giscegis.session')

        # Clean the sessions to avoid BBox collisions
        sql = """
            TRUNCATE giscegis_session CASCADE
        """
        self.cursor.execute(sql)

        # Create the sessions
        session1_vals = {
            "name": 'Testing session 1',
            "bbox": 'POLYGON((982449.3 4664776.0, 982760.4 4664776.0, '
                    '982760.4 4664950.2, 982449.3 4664950.2, '
                    '982449.3 4664776.0))',
            "state": 'open',
            "ttl": 500000000,
            "active": True
        }
        session2_vals = {
            "name": 'Testing session 2',
            "bbox": 'POLYGON((981216.7 4664152.6, 981771.6 4664152.6, '
                    '981771.6 4664463.2, 981216.7 4664463.2, '
                    '981216.7 4664152.6))',
            "state": 'open',
            "ttl": 500000000,
            "active": True
        }
        session1_id = session_obj.create(self.cursor, self.uid, session1_vals)
        session2_id = session_obj.create(self.cursor, self.uid, session2_vals)

        # Start the checks
        # Check the number of Escomeses in differents sessions and without
        # sessions
        # No session
        escomeses_ids = escomeses_obj.search(self.cursor, self.uid, [])
        self.assertEqual(len(escomeses_ids), 40)
        # Session 1
        escomeses_ids = escomeses_obj.search(
            self.cursor, self.uid, [], context={'session': session1_id}
        )
        self.assertEqual(len(escomeses_ids), 0)
        # Session 2
        escomeses_ids = escomeses_obj.search(
            self.cursor, self.uid, [], context={'session': session2_id}
        )
        self.assertEqual(len(escomeses_ids), 17)

        # Get 2 ids from the session 2
        escomesa1_id = escomeses_ids[0]
        escomesa2_id = escomeses_ids[1]

        # Check that the changes made on a session are only visible from the
        # session
        # Load data before changes
        escomeses_data_original = escomeses_obj.read(
            self.cursor, self.uid, [escomesa1_id, escomesa2_id], ['rotation']
        )
        # Write new data on 2 Escomeses from session 2
        escomesa_write_vals = {
            "rotation": 240
        }
        escomeses_obj.write(
            self.cursor, self.uid, [escomesa1_id, escomesa2_id],
            escomesa_write_vals, context={'session': session2_id}
        )
        # Reload data outside the session
        escomeses_data = escomeses_obj.read(
            self.cursor, self.uid, [escomesa1_id, escomesa2_id], ['rotation']
        )
        # Compare data outside session before and after write data on session 2
        self.assertEqual(
            escomeses_data_original[0]['rotation'],
            escomeses_data[0]['rotation']
        )
        self.assertEqual(
            escomeses_data_original[1]['rotation'],
            escomeses_data[1]['rotation']
        )
        # Check changes inside the session
        escomeses_data = escomeses_obj.read(
            self.cursor, self.uid, [escomesa1_id, escomesa2_id], ['rotation'],
            context={'session': session2_id}
        )
        for data in escomeses_data:
            self.assertEqual(data['rotation'], 240)
