# Translation of OpenERP Server.
# This file contains the translation of the following modules:
# 
# Translators:
# Translators:
# Christopher Tari <ctari@gisce.net>, 2019.
# Eduard Berloso <eberloso@gisce.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: GISCE-ERP\n"
"Report-Msgid-Bugs-To: https://github.com/gisce/erp/issues\n"
"POT-Creation-Date: 2019-07-29 13:36\n"
"PO-Revision-Date: 2019-07-29 12:47+0000\n"
"Last-Translator: Eduard Berloso <eberloso@gisce.net>\n"
"Language-Team: Spanish (Spain) <erp@dev.gisce.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: es_ES\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. module: giscegis_catastre
#: constraint:ir.model:0
msgid ""
"The Object name must start with x_ and not contain any special character !"
msgstr "The Object name must start with x_ and not contain any special character !"

#. module: giscegis_catastre
#: code:addons/giscegis_catastre/wizard/giscegis_catastre_upload_parceles.py:86
#, python-format
msgid "S'han carregat {} parcel·les del catastre."
msgstr "Se han cargado {} parcelas del catastro."

#. module: giscegis_catastre
#: model:ir.actions.act_window,name:giscegis_catastre.action_giscegis_catastre_crm_case
#: model:ir.ui.menu,name:giscegis_catastre.wizard_giscegis_catastre_crm_case
msgid "Crear casos CRM"
msgstr "Crear casos CRM"

#. module: giscegis_catastre
#: constraint:ir.ui.view:0
msgid "Invalid XML for View Architecture!"
msgstr "Invalid XML for View Architecture!"

#. module: giscegis_catastre
#: field:giscegis.catastre.upload.parceles,filename:0
msgid "Filename"
msgstr "Nombre del archivo"

#. module: giscegis_catastre
#: model:ir.module.module,shortdesc:giscegis_catastre.module_meta_information
msgid "GISCE GIS Catastre"
msgstr "GISCE GIS Catastro"

#. module: giscegis_catastre
#: field:giscegis.catastre.crm.case,state:0
#: field:giscegis.catastre.upload.parceles,state:0
msgid "State"
msgstr "Estado"

#. module: giscegis_catastre
#: model:crm.case.section,name:giscegis_catastre.giscegis_catastre
msgid "Gestió de Tala"
msgstr "Gestión de Tala"

#. module: giscegis_catastre
#: view:giscegis.catastre.parceles:0
#: model:ir.actions.act_window,name:giscegis_catastre.action_gis_parceles_tree
#: model:ir.ui.menu,name:giscegis_catastre.menu_gis_base_parceles
msgid "Parcel·les"
msgstr "Parcelas"

#. module: giscegis_catastre
#: constraint:ir.actions.act_window:0
msgid "Invalid model name in the action definition."
msgstr "Invalid model name in the action definition."

#. module: giscegis_catastre
#: view:giscegis.catastre.crm.case:0 view:giscegis.catastre.upload.parceles:0
msgid "Cancel·lar"
msgstr "Cancelar"

#. module: giscegis_catastre
#: code:addons/giscegis_catastre/giscegis_catastre_parceles.py:128
#, python-format
msgid "La referència catastral ha de ser única."
msgstr "La referencia catastral tiene que ser única."

#. module: giscegis_catastre
#: code:addons/giscegis_catastre/wizard/giscegis_catastre_crm_case.py:11
#: field:giscegis.catastre.crm.case,trimestre:0
#, python-format
msgid "Trimestre de tala"
msgstr "Trimestre de tala"

#. module: giscegis_catastre
#: view:giscegis.catastre.parceles:0 view:res.partner:0
msgid "Parceles"
msgstr "Parcelas"

#. module: giscegis_catastre
#: code:addons/giscegis_catastre/wizard/giscegis_catastre_crm_case.py:104
#, python-format
msgid "- Num Poligon: {} Num Parcel·la: {} del municipi {} amb RefCat {}"
msgstr "- Num Poligon: {} Num Parcela: {} del municipio {} con RefCat {}"

#. module: giscegis_catastre
#: field:giscegis.catastre.parceles,x:0
msgid "x"
msgstr "x"

#. module: giscegis_catastre
#: code:addons/giscegis_catastre/wizard/giscegis_catastre_crm_case.py:14
#: field:giscegis.catastre.crm.case,missatge:0
#, python-format
msgid "Registre de casos CRM"
msgstr "Registro de casos CRM"

#. module: giscegis_catastre
#: field:giscegis.catastre.parceles,num_parcela:0
msgid "Num. de parcel·la"
msgstr "Núm. de parcela"

#. module: giscegis_catastre
#: constraint:crm.case.section:0
msgid "Error ! You cannot create recursive sections."
msgstr "¡Error! No se pueden crear secciones recursivas."

#. module: giscegis_catastre
#: view:giscegis.catastre.upload.parceles:0
msgid "Carregador de pareceles del catastre"
msgstr "Cargador de parcelas del catastro"

#. module: giscegis_catastre
#: model:ir.model,name:giscegis_catastre.model_giscegis_catastre_parceles
msgid "Cataleg d'elements pel catastre"
msgstr "Categ de elementos para el catastro"

#. module: giscegis_catastre
#: model:ir.model,name:giscegis_catastre.model_giscegis_catastre_crm_case
msgid "giscegis.catastre.crm.case"
msgstr "giscegis.catastre.crm.case"

#. module: giscegis_catastre
#: field:res.partner,parceles:0
msgid "Parcel·les del propietari"
msgstr "Parcelas del propietario"

#. module: giscegis_catastre
#: field:giscegis.catastre.parceles,partner_id:0
msgid "Propietari"
msgstr "Propietario"

#. module: giscegis_catastre
#: field:giscegis.catastre.parceles,num_poligon:0
msgid "Num. de poligon"
msgstr "Núm. de polígono"

#. module: giscegis_catastre
#: field:giscegis.catastre.parceles,id_municipi:0
msgid "Municipi"
msgstr "Municipio"

#. module: giscegis_catastre
#: field:giscegis.catastre.upload.parceles,file:0
msgid "Fitxer de parecel·les"
msgstr "Fichero de parcelas"

#. module: giscegis_catastre
#: view:giscegis.catastre.crm.case:0
msgid "Generardor de casos CRM per les tal·les"
msgstr "Generardor de casos CRM para las talas"

#. module: giscegis_catastre
#: field:giscedata.at.tram,parceles:0
msgid "Parcel·les afectades"
msgstr "Parcelas afectadas"

#. module: giscegis_catastre
#: model:ir.model,name:giscegis_catastre.model_giscegis_catastre_upload_parceles
msgid "giscegis.catastre.upload.parceles"
msgstr "giscegis.catastre.upload.parceles"

#. module: giscegis_catastre
#: view:giscegis.catastre.crm.case:0
msgid "Generar casos"
msgstr "Generar casos"

#. module: giscegis_catastre
#: model:ir.actions.act_window,name:giscegis_catastre.action_giscegis_upload_parceles
#: model:ir.ui.menu,name:giscegis_catastre.wizard_giscegis_upload_parceles
msgid "Upload catastre pareceles"
msgstr "Upload catrasto parcelas"

#. module: giscegis_catastre
#: code:addons/giscegis_catastre/wizard/giscegis_catastre_crm_case.py:12
#: field:giscegis.catastre.crm.case,linia:0
#, python-format
msgid "Línies pel cas"
msgstr "Líneas por el caso"

#. module: giscegis_catastre
#: code:addons/giscegis_catastre/wizard/giscegis_catastre_upload_parceles.py:219
#, python-format
msgid "Tria el fitxer ZIP per carregar les dades catastrals."
msgstr "Elija el fichero ZIP para cargar los datos catastrales."

#. module: giscegis_catastre
#: code:addons/giscegis_catastre/wizard/giscegis_catastre_crm_case.py:112
#, python-format
msgid "S'han generat {} casos CRM per la tala del trimestre {}"
msgstr "Se han generado {} casos CRM para la tala del trimestre {}"

#. module: giscegis_catastre
#: code:addons/giscegis_catastre/wizard/giscegis_catastre_crm_case.py:19
#, python-format
msgid "Tria la tala per la que vols generar els casos CRM"
msgstr "Elige la tala para la que quieres generar los casos CRM"

#. module: giscegis_catastre
#: field:giscegis.catastre.parceles,address_id:0
msgid "Contacte"
msgstr " Contacto"

#. module: giscegis_catastre
#: code:addons/giscegis_catastre/wizard/giscegis_catastre_crm_case.py:104
#, python-format
msgid " Contacte: "
msgstr " Contacto: "

#. module: giscegis_catastre
#: field:giscegis.catastre.upload.parceles,missatge:0
msgid "Registre de càrrega"
msgstr "Registro de carga"

#. module: giscegis_catastre
#: code:addons/giscegis_catastre/wizard/giscegis_catastre_crm_case.py:13
#: field:giscegis.catastre.crm.case,totes_linies:0
#, python-format
msgid "Totes les línies"
msgstr "Todas las líneas"

#. module: giscegis_catastre
#: model:ir.module.module,description:giscegis_catastre.module_meta_information
msgid "Representacio dels models del catastre"
msgstr "Representación de los modelos del catastro"

#. module: giscegis_catastre
#: view:res.partner:0
msgid "General"
msgstr "General"

#. module: giscegis_catastre
#: field:giscegis.catastre.parceles,geom:0
msgid "geometry"
msgstr "geometría"

#. module: giscegis_catastre
#: field:giscegis.catastre.parceles,y:0
msgid "y"
msgstr "y"

#. module: giscegis_catastre
#: field:giscegis.catastre.parceles,tram:0
msgid "Trams afectats"
msgstr "Tramos afectados"

#. module: giscegis_catastre
#: model:ir.ui.menu,name:giscegis_catastre.menu_catastre
msgid "Catastre"
msgstr "Catastro"

#. module: giscegis_catastre
#: view:giscegis.catastre.upload.parceles:0
msgid "Carregar"
msgstr "Cargar"

#. module: giscegis_catastre
#: field:giscegis.catastre.parceles,name:0
msgid "Referencia Catastral"
msgstr "Referencia Catastral"
