# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _
from datetime import datetime, timedelta


class ResPartner(osv.osv):

    _name = 'res.partner'
    _inherit = 'res.partner'

    def _get_parceles_by_partner(self, cursor, uid, ids, name, arg, context=None):
        """
        Return dictionary of 'parceles' that belong to the given 'partner_id', (key: partner_id, value: parcela_id)
        :param cursor: cursor from db
        :type cursor
        :param uid: current user identifier
        :type int
        :param ids: partner identifiers
        :type int
        :param name:
        :param arg:
        :param context:
        :return:
        :rtype dict
        """

        ret = {}

        if len(ids) == 0:
            return ret
        sql = """
            SELECT partner_id, id as parcela_id, "name"  
            FROM giscegis_catastre_parceles
            WHERE partner_id in %s
        """

        cursor.execute(sql, (tuple(ids),))

        for res in cursor.fetchall():
            partner_id = res[0]
            parcela_id = res[1]
            ret.setdefault(partner_id, []).append(parcela_id)
        return ret

    _columns = {
        "parceles": fields.function(_get_parceles_by_partner, type="one2many", method=True, relation="giscegis.catastre.parceles", string="Parcel·les del propietari"),
    }

ResPartner()