# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _


class GiscegisCatastreCrmCase(osv.osv_memory):

    _name = 'giscegis.catastre.crm.case'

    _columns = {
            'trimestre': fields.many2one('giscedata.trieni', _('Trimestre de tala')),
            "linia": fields.many2many("giscedata.tala.at", "giscegis_catastre_crm_linia_trimestre", "linia_id", "trimestre_id", string=_("Línies pel cas")),
            'totes_linies': fields.boolean(_('Totes les línies')),
            'missatge': fields.text(_('Registre de casos CRM')),
            'state': fields.char('State', size=4)
        }

    _defaults = {
        'missatge': lambda *a: _("Tria la tala per la que vols generar els casos CRM"),
        'state': lambda *a: 'init',
    }

    def generate_crm_cases(self, cursor, uid, wiz_id, context):
        """

        :param cursor:
        :param uid:
        :param wiz_id:
        :param context:
        :return:
        """
        crm_section_code = "TAL"

        wiz_obj = self.pool.get('giscegis.catastre.crm.case')
        trimestre_model = self.pool.get('giscedata.trieni')
        linia_model = self.pool.get('giscedata.tala.at')
        linia_trams_model = self.pool.get('giscedata.tala.at.tram')
        tram_model = self.pool.get('giscedata.at.tram')
        parcela_model = self.pool.get('giscegis.catastre.parceles')

        wizard = wiz_obj.browse(cursor, uid, wiz_id[0])
        totes_linies = (wizard.read(['totes_linies'])[0]['totes_linies'] == 1)
        trimestre_tala = (wizard.read(['trimestre'])[0]['trimestre'])

        if totes_linies:
            linia = linia_model.search(cursor, uid, [('trimestre', '=', trimestre_tala)])
        else:
            linia = (wizard.read(['linia'])[0]['linia'])

        trimestre_name = trimestre_model.read(cursor, uid, trimestre_tala, ['name']).get("name")

        linias_trams = []
        trams_tala = []
        parceles_tram = []
        while linia:
            a = (linia_model.search(cursor, uid, [('id', '=', linia[0])], context=context))
            linias_trams.append(a)
            tala_trams = linia_model.read(cursor, uid, linia, ['trams', 'id', 'codi'])

            for tram in tuple(tala_trams[0]['trams']):
                b = linia_trams_model.read(cursor, uid, tram, ['id', 'name', ])
                trams_tala.append(b)
                tram_model.read(cursor, uid, b['name'][0], ['id', 'name', 'parceles'])
                parceles_tram.append(tram_model.read(cursor, uid, b['name'][0], ['parceles']))

            linia.pop(0)

        contactes_tala = []
        dict_partner_id_parceles = {}

        for parcela_tram in parceles_tram:
            for parcela in parcela_tram['parceles']:
                info_parcela = parcela_model.read(cursor, uid, parcela, ["partner_id", "address_id", "name", "id_municipi", "num_poligon", "num_parcela"])
                dict_partner_id_parceles.setdefault(info_parcela["partner_id"], []).append((info_parcela["name"],
                                                                                            info_parcela["address_id"],
                                                                                            info_parcela["id_municipi"],
                                                                                            info_parcela["num_poligon"],
                                                                                            info_parcela["num_parcela"])
                                                                                           )
                contactes_tala.append(info_parcela)

        dict_contactes= {}
        crm_count = 0
        for key, value in dict_partner_id_parceles.items():
            parcela = ""

            if isinstance(key, (bool)):
                partner = key
                partner_id = ""
            else:
                partner = key[1]
                partner_id = key[0]

            for elem in value:
                if isinstance(elem[1], (bool)):
                    contacte = elem[1]
                    contacte_id = ""
                else:
                    contacte = elem[1][1]
                    contacte_id = elem[1][0]

                dict_contactes.setdefault(partner_id, []).append((contacte_id, contacte))

                parcela += _("- Num Poligon: {} Num Parcel·la: {} del municipi {} amb RefCat {}").format(elem[3], elem[4], elem[2][1], elem[0]) + _(" Contacte: ") + str(contacte) + "\n"

            dict_crm_case_data = self.fill_crm_case_data(cursor, uid, crm_section_code, partner, parcela, trimestre_name, partner_id, dict_contactes, context=None)

            is_saved = self.create_crm_case(cursor, uid, dict_crm_case_data, context)
            if is_saved > 0:
                crm_count += 1

        wizard_message = _("S'han generat {} casos CRM per la tala del trimestre {}").format(str(crm_count), str(trimestre_name))
        wizard.write({'missatge': wizard_message})

    def create_crm_case(self, cursor, uid, case_data, context):
        """

        :param cursor:
        :param uid:
        :param case_data:
        :param context:
        :return:
        """
        cas_model = self.pool.get('crm.case')
        created = cas_model.create(cursor, uid, {
            "description": case_data.get("crm_description"),
            "section_id": case_data.get("crm_section_id"),
            "name": case_data.get("crm_name"),
            "partner_id": case_data.get("crm_partner_id"),
            "partner_address_id": case_data.get("crm_partner_address_id")
        })
        return created

    def get_partner_address_id(self, cursor, uid, partner_id, dict_contactes, context):
        """

        :param cursor:
        :param uid:
        :param partner_id:
        :param dict_contactes:
        :param context:
        :return:
        """
        unique_partner_address_set = set()
        for value in dict_contactes.get(partner_id):
            unique_partner_address_set.add(value)

        if len(unique_partner_address_set) == 1:
            crm_partner_address_id = dict_contactes.get(partner_id)[0][0]
        else:
            crm_partner_address_id = ""
        return crm_partner_address_id

    def fill_crm_case_data(self, cursor, uid, crm_section_code, partner, parcela, trimestre_name, partner_id, dict_contactes, context):
        """

        :param cursor:
        :param uid:
        :param crm_section_code:
        :param partner:
        :param parcela:
        :param trimestre_name:
        :param partner_id:
        :param dict_contactes:
        :param context:
        :return:
        """
        crm_description = "Gestión de la tala del propietario " + str(partner) + " de las parcelas:\n" + str(parcela)
        crm_name = "Gestión de la tala " + str(partner) + " del trimestre " + str(trimestre_name)
        crm_section_id = self.get_crm_section_id_by_code(cursor, uid, crm_section_code)
        crm_partner_id = partner_id
        crm_partner_address_id = self.get_partner_address_id(cursor, uid, partner_id, dict_contactes, context)

        crm_case_values_dict = {
            "crm_description": crm_description,
            "crm_name": crm_name,
            "crm_section_id": crm_section_id,
            "crm_partner_id": crm_partner_id,
            "crm_partner_address_id": crm_partner_address_id,
        }

        return crm_case_values_dict

    def get_crm_section_id_by_code(self, cursor, uid, crm_section_code):
        """
        Returns crm_section_id by code
        :param cursor: cursor from db
        :type cursor
        :param uid: current user identifier
        :type int
        :param crm_section_code: the code(str with 3 chars in upper case) crm_section
        :type str
        :return: returns the crm_section identifier.
        :rtype int
        """
        crm_section_model = self.pool.get('crm.case.section')

        crm_section_id = crm_section_model.search(cursor, uid, [('code', '=', crm_section_code)])

        if len(crm_section_id) > 0:
            crm_section_id = crm_section_id[0]
        else:
            crm_section_id = 0

        return crm_section_id


GiscegisCatastreCrmCase()
