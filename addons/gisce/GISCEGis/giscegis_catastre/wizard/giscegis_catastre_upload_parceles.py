# -*- coding: utf-8 -*-
from osv import osv, fields
import base64
from zipfile import ZipFile, BadZipfile
from StringIO import StringIO
import os
from tqdm import tqdm
import shapefile
import pygeoif
import datetime
from tools import config
from tools.translate import _


class GiscegisCatastreUploadParceles(osv.osv_memory):

    _name = 'giscegis.catastre.upload.parceles'

    def st_union_multipolygons(self, cursor, multipol1, multipol2, context=None):
        """
        Makes the union of 2 multipolygons.
        :param cursor:
        :type cursor
        :param multipol1:
        :type list
        :param multipol2:
        :type list
        :param context:
        :return:
        :rtype
        """
        if context is None:
            context = {}
        sql = """
                SELECT 
                    ST_asText(ST_Multi(
                        ST_Union(
                            ST_GeomFromText(%s),
                            ST_GeomFromText(%s)
                            )
                        ))
            """
        cursor.execute(sql, (multipol1, multipol2))
        for res in cursor.fetchall():
            data = res[0]
        return data

    def multipolygon_union_business_logic(self, cursor, value):
        num_multipols = len(value)
        multipol_union = str(value[0])

        if num_multipols > 1:
            if num_multipols == 2:
                multipol_union = self.st_union_multipolygons(cursor, value[0], value[1])
            elif num_multipols > 2:
                current_multipol = 0
                while current_multipol <= (num_multipols - 1):
                    if current_multipol == 0:
                        multipol_union = self.st_union_multipolygons(cursor, value[current_multipol], value[current_multipol + 1])
                        current_multipol += 2
                    else:
                        multipol_union =self.st_union_multipolygons(cursor, value[current_multipol], multipol_union)
                        current_multipol += 1
        return multipol_union

    def load_zip_master(self, cursor, uid, wiz_id, context):
        wiz_obj = self.pool.get('giscegis.catastre.upload.parceles')

        br_wiz = wiz_obj.browse(cursor, uid, wiz_id[0])
        zip_data = br_wiz.file
        zip_decoded = base64.decodestring(zip_data)

        s = StringIO(zip_decoded)
        try:
            input_zip = ZipFile(s)
            filenames = sorted(input_zip.namelist())
            load_zip_response = 0
            if filenames:
                current_date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                for filename in filenames:
                    extensio = os.path.splitext(filename)[1].lower()
                    if os.path.splitext(filename)[1].lower() == '.zip':
                        zip_data = StringIO(input_zip.read(filename))
                        load_zip_response += self.load_zip_(cursor, uid, zip_data, context)
                        if load_zip_response > 0:
                            wizard_message = _("S'han carregat {} parcel·les del catastre.").format(load_zip_response)
                            br_wiz.write({'missatge': wizard_message})
                    else:
                        print "No s'han pogut carregar parcel·les del fitxer {}".format(filename)

                #Esborrar relacions entre parcel·les i trams dels trams que han sigut modificats
                self.pool.get("giscegis.catastre.parceles").delete_modified_parceles_tram_relations(cursor, uid, current_date)

                #Calcula només les interseccions entre les parcel·les que han canviat.
                self.pool.get("giscegis.catastre.parceles").vinculate_tram_parcela_only_modifieds_async(cursor, uid, current_date)
            else:
                pass
                # no hi ha fitxers

        except BadZipfile:
            pass
        # Fitxer corrupte

    def load_zip_(self, cursor, uid, zip_data, context):
        parcela_obj = self.pool.get("giscegis.catastre.parceles")
        try:
            shp_data = None
            dbf_data = None
            shx_data = None

            input_zip = ZipFile(zip_data)
            filenames = sorted(input_zip.namelist())
            if filenames:
                for filename in filenames:
                    if os.path.splitext(filename)[1].lower() == '.dbf':
                        dbf_data = StringIO(input_zip.read(filename))
                    if os.path.splitext(filename)[1].lower() == '.shp':
                        shp_data = StringIO(input_zip.read(filename))
                    if os.path.splitext(filename)[1].lower() == '.shx':
                        shx_data = StringIO(input_zip.read(filename))
                    if shp_data and dbf_data and shx_data:
                        sf = shapefile.Reader(shp=shp_data, dbf=dbf_data)
                        records = sf.records()
                        shapes = sf.shapes()

                        data = {}
                        dictionary_points = {}

                        for index, shape in tqdm(enumerate(shapes), total=len(shapes)):

                            m = pygeoif.MultiPolygon(pygeoif.geometry.as_shape(shape))
                            ref = records[index][22]
                            codi_provincia = records[index][1]
                            codi_municipi = records[index][2]

                            full_codi_municipi = str(codi_provincia) + str(codi_municipi).zfill(3)
                            name_municipi = self.pool.get("res.municipi").search(cursor, uid, [("ine", "=", full_codi_municipi)])

                            num_poligon = records[index][3]
                            num_parcela = records[index][5]
                            # Agrupem els points per referencia catastral
                            if ref in dictionary_points:
                                if "geom" in dictionary_points[ref]:
                                    dictionary_points[ref]["geom"].append(m.wkt)
                                else:
                                    dictionary_points[ref]["geom"] = [m.wkt]
                            else:
                                dictionary_points.setdefault(ref, {})["geom"] = [m.wkt]
                            dictionary_points.setdefault(ref, {})["codi_municipi"] = name_municipi[0]
                            dictionary_points.setdefault(ref, {})["num_poligon"] = num_poligon
                            dictionary_points.setdefault(ref, {})["num_parcela"] = num_parcela

                        index = 0
                        for key, value in dictionary_points.iteritems():
                            wkt_geometry = self.multipolygon_union_business_logic(cursor, value.get("geom"))
                            data[index] = {"geom": wkt_geometry}
                            data[index]["refcat"] = key
                            data[index]["codi_municipi"] = value.get("codi_municipi")
                            data[index]["num_poligon"] = value.get("num_poligon")
                            data[index]["num_parcela"] = value.get("num_parcela")
                            index += 1

                        for key, value in tqdm(data.items()):
                            self.upsert_parcela(cursor, uid, value)
                        return len(data)
            else:
                return 0
                pass
                # no hi ha fitxers
        except BadZipfile:
            return 0
            pass
            # Fitxer corrupte

    def upsert_parcela(self, cursor, uid, parcela_info):
        """
        Inserts or updates in 'giscegis_catastre_parceles' if satisfies the conditions.
        :param cursor:
        :param uid:
        :param parcela_info:
        :return:
        """
        srid = config.get('srid')

        sql = """
            INSERT
                INTO
                giscegis_catastre_parceles
                (create_uid, create_date, write_uid, geom, "name", id_municipi, num_poligon, num_parcela)
                VALUES(%(uid)s, CURRENT_TIMESTAMP, null, st_setsrid(st_geomfromtext(%(geom)s), %(srid)s), %(refcat)s, %(id_municipi)s, %(num_poligon)s, %(num_parcela)s)
            ON CONFLICT("name")
            DO UPDATE
            SET 
                write_date = CURRENT_TIMESTAMP, 
                write_uid = %(uid)s, 
                geom = st_setsrid(st_geomfromtext(%(geom)s), %(srid)s)
            WHERE(st_astext(giscegis_catastre_parceles.geom) <> st_astext(%(geom)s))
        """

        cursor.execute(sql,
                       {"uid": uid,
                        "geom": parcela_info["geom"],
                        "srid": srid,
                        "refcat": parcela_info["refcat"],
                        "id_municipi": parcela_info["codi_municipi"],
                        "num_poligon": parcela_info["num_poligon"],
                        "num_parcela": parcela_info["num_parcela"],
                        }
                       )

    _columns = {
        'file': fields.binary('Fitxer de parecel·les'),
        'missatge': fields.text('Registre de càrrega'),
        'filename': fields.char('Filename', size=256),
        'state': fields.char('State', size=4)
    }

    _defaults = {
        'missatge': lambda *a: _("Tria el fitxer ZIP per carregar les dades catastrals."),
        'state': lambda *a: 'init',
    }


GiscegisCatastreUploadParceles()
