# -*- coding: utf-8 -*-

from osv import fields, osv
from giscegis_base_geom.fields import Multipolygon
from tools import config
from oorq.decorators import job
from tools.translate import _


class GiscegisCatastreParceles(osv.osv):
    _name = "giscegis.catastre.parceles"
    _description = "Cataleg d'elements pel catastre"

    def _vinculate_tram_parcela_by_parcela_id(self, cursor, uid, ids, name, arg, context=None):
        """
        Checks if a 'parcela' intersects with a list of given 'parcela_id'. If intersects saves the relation between parcela_id and tram_id
        :param cursor:
        :param uid:
        :param ids:
        :param name:
        :param arg:
        :param context:
        :return:
        """
        if context is None:
            context = {}
        sql = """
        SELECT 
            tram.id as tram_lid, 
            parce.id as parce_id
            FROM giscedata_at_tram tram
            JOIN giscedata_at_cables cables ON tram.cable = cables.id
            JOIN giscedata_at_tipuscable tipuscable ON tipuscable.id = cables.tipus,
            giscegis_catastre_parceles parce	
            WHERE tram.geom is not null
            AND active
            AND tipuscable.codi <> 'E'
            AND tram.tipus = 1
            AND parce.id in %s
            AND ST_Intersects(st_buffer(st_setsrid(tram.geom, %s), 7, 'endcap=flat join=round'), st_setsrid(parce.geom, %s));
        """

        srid = config.get("srid", 25830)
        if isinstance(srid, str):
            srid = int(srid)
        cursor.execute(sql, (tuple(ids), srid, srid))

        ret = {}

        for res in cursor.fetchall():
            tram = res[0]
            parcela = res[1]
            ret.setdefault(parcela, []).append(tram)
        return ret

    def _coords(self, cursor, uid, ids, field_name, arg, context):
        """
        Returns the cetroid coords X and Y of the given Parcela
        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param ids:  ids
        :type ids: int or list of int
        :return: X and y of the geom
        :rtype: dict[str, float or False]
        """

        ret = {}

        sql = """
        select 
            id as id,
            st_x(st_centroid(
            geom
            )) as x, 
            st_y(st_centroid(
            geom
            )) as y
            from giscegis_catastre_parceles
               where id in %(ids)s;
        """

        if isinstance(ids, int):
            ids = [ids]
        cursor.execute(sql, {'ids': tuple(ids)})
        data = cursor.dictfetchall()

        for centroid in data:
            p_id = centroid['id']

            if centroid['x'] is not None:
                x = centroid['x']
            else:
                x = False

            if centroid['y'] is not None:
                y = centroid['y']
            else:
                y = False

            ret[p_id] = {"x": x, "y": y}

            ids.remove(p_id)

        for p_id in ids:
            ret[p_id] = {"x": False, "y": False}

        return ret

    _columns = {
        "name": fields.char("Referencia Catastral", size=256, required=True),
        "geom": Multipolygon("geometry", srid=config.get('srid', 25830)),
        "tram": fields.many2many("giscedata.at.tram", "giscedata_parcela_tram_rel", "parcela_id", "tram_id", string="Trams afectats"),
        "partner_id": fields.many2one("res.partner", "Propietari"),
        "address_id": fields.many2one("res.partner.address", "Contacte"),
        "x": fields.function(
            _coords, method=True, type='float', string='x', multi="_coords"
        ),
        "y": fields.function(
            _coords, method=True, type='float', string='y', multi="_coords"
        ),
        "id_municipi": fields.many2one("res.municipi", "Municipi"),#Camp MUNICIPIO(retorna el codi de municipi en número...
        "num_poligon": fields.integer("Num. de poligon", size=256),#Camp MASA del fitxer SHP
        "num_parcela": fields.integer("Num. de parcel·la", size=256),#Camp PARCELA del fitxer SHP
    }

    _sql_constraints = [(
        "name_unique", "unique(name)", _("La referència catastral ha de ser única.")
    )]

    @job(queue='gis', timeout='3600000')
    def vinculate_tram_parcela_only_modifieds_async(self, cursor, uid, last_modified_date, context=None):
        self.vinculate_tram_parcela_only_modifieds(cursor, uid, last_modified_date)

    def vinculate_tram_parcela_only_modifieds(self, cursor, uid, last_modified_date, context=None):
        """
        Checks if a 'parcela' intersects with a 'tram_id' when parcela create/write date is greater than the given 'last_modified_date'.
        If intersects saves the relation between parcela_id and tram_id
        :param cursor:
        :param uid:
        :param last_modified_date:
        :param context:
        :return:
        """
        if context is None:
            context = {}
        sql = """
        SELECT  
            tram.id as tram_id, 
            parce.id as parce_id
            FROM giscedata_at_tram tram
            JOIN giscedata_at_cables cables ON tram.cable = cables.id
            JOIN giscedata_at_tipuscable tipuscable ON tipuscable.id = cables.tipus,
            giscegis_catastre_parceles parce	
            WHERE tram.geom IS NOT NULL
            AND ((parce.create_date >= %s and parce.write_date IS NULL) OR (parce.write_date >= %s))
            AND active
            AND tipuscable.codi <> 'E'
            AND tram.tipus = 1
            AND ST_Intersects(st_buffer(st_setsrid(tram.geom, %s), 7, 'endcap=flat join=round'), st_setsrid(parce.geom, %s));
        """

        srid = config.get("srid", 25830)
        if isinstance(srid, str):
            srid = int(srid)
        cursor.execute(sql, (last_modified_date, last_modified_date, srid, srid))
        result_update = False
        for res in cursor.fetchall():
            tram = res[0]
            parcela = res[1]
            result_update = self.write(cursor, uid, parcela, {"tram": [(4, tram)]})

    def delete_modified_parceles_tram_relations(self, cursor, uid, last_modified_date):
        """
        Deletes relations between tram_id and parcela_id where his last creation/write_date is greater than given last_modified_date.
        In other words, if there are changes(new intersections, delete the current 'parcela' or 'tram') in stored geometry.
        :param cursor:
        :param uid:
        :param last_modified_date:
        :return:
        """
        sql = """
        DELETE 
            FROM giscedata_parcela_tram_rel parce_tram_rel
            USING giscegis_catastre_parceles parce
        WHERE
            ((parce.create_date >= %s AND parce.write_date IS NULL) OR (parce.write_date >= %s))
        AND 
            parce_tram_rel.parcela_id = parce.id
        """
        cursor.execute(sql, (last_modified_date, last_modified_date))

    def delete_parceles_tram_relations_by_tram_id(self, cursor, uid, ids):
        """
        Deletes relation between tram_id and parcela_id
        :param cursor:
        :param uid:
        :param ids:
        :return:
        """
        sql = """
        DELETE 
            FROM giscedata_parcela_tram_rel parce_tram_rel
        WHERE
            parce_tram_rel.tram_id IN %s
        """
        ids_ = tuple(ids)
        cursor.execute(sql, (ids_,))

    def vinculate_tram_parcela_by_tram_id(self, cursor, uid, ids, context=None):
        """
        Checks if a 'parcela' intersects with a list of given 'tram_id'. If intersects saves the relation between parcela_id and tram_id
        :param cursor:
        :param uid:
        :param ids:
        :param context:
        :return:
        :rtype boolean
        """
        if context is None:
            context = {}
        sql = """
        SELECT 
            tram.id as tram_lid, 
            parce.id as parce_id
            FROM giscedata_at_tram tram
            JOIN giscedata_at_cables cables ON tram.cable = cables.id
            JOIN giscedata_at_tipuscable tipuscable ON tipuscable.id = cables.tipus,
            giscegis_catastre_parceles parce	
            WHERE tram.geom is not null
            AND active
            AND tipuscable.codi <> 'E'
            AND tram.tipus = 1
            AND tram.id in %s
            AND ST_Intersects(st_buffer(st_setsrid(tram.geom,%s), 7, 'endcap=flat join=round'), st_setsrid(parce.geom, %s));
        """
        ids_ = tuple(ids)
        srid = config.get("srid", 25830)
        if isinstance(srid, str):
            srid = int(srid)
        cursor.execute(sql, (ids_, srid, srid))
        result_update = False
        for res in cursor.fetchall():
            tram = res[0]
            parcela = res[1]
            result_update = self.write(cursor, uid, parcela, {"tram": [(4, tram)]})

    def on_change_partner(self, cr, uid, ids, partner_id, address_id):
        """
        If partner has partner_address, fill the form field 'address_id' with the first partner_address_id.
        :param cr:
        :type cursor
        :param uid:
        :type
        :param ids:
        :type
        :param partner_id:
        :type
        :param address_id:
        :type
        :return:
        :rtype
        """
        partner_address = self.pool.get("res.partner.address")
        a = partner_address.search(cr, uid, [("partner_id", "=", partner_id)])
        if a:
            result = {'value': {
                'address_id': a[0], }
            }
        else:
            result = {'value': {
                'address_id': "", }
            }
        return result


GiscegisCatastreParceles()


class GiscedataAtTram(osv.osv):
    _name = 'giscedata.at.tram'
    _inherit = 'giscedata.at.tram'

    def _get_parceles_by_tram(self, cursor, uid, ids, name, arg, context=None):
        """
        Return dictionary of 'parceles' that intersects with the given 'tram_id', (key: tram_id, value: parcela_id)
        :param cursor: cursor from db
        :type cursor
        :param uid: current user identifier
        :type int
        :param ids: parceles identifiers
        :type int
        :param name:
        :param arg:
        :param context:
        :return:
        :rtype dict
        """

        ret = {}

        if len(ids) == 0:
            return ret
        sql = """
         SELECT tram_id, parcela_id 
         FROM giscedata_parcela_tram_rel
         WHERE tram_id in %s;
        """

        cursor.execute(sql, (tuple(ids),))

        for res in cursor.fetchall():
            tram = res[0]
            parcela = res[1]
            ret.setdefault(tram, []).append(parcela)
        return ret

    _columns = {
        "parceles": fields.function(_get_parceles_by_tram, type="many2many", method=True, relation="giscegis.catastre.parceles", string="Parcel·les afectades"),
    }

    def create(self, cr, uid, vals, context=None):
        ret = super(GiscedataAtTram, self).create(cr, uid, vals, context)
        #MIRAR SI VIENE CON GEOM!!, si no viene con geom no nos interesa hacer nada más.
        if 'geom' in vals:
            if ret > 0:
                if isinstance(ret, long):
                    ids = [ret]
            par_obj = self.pool.get("giscegis.catastre.parceles")
            par_obj.vinculate_tram_parcela_by_tram_id(cr, uid, ids)
        return ret

    def unlink(self, cr, uid, ids, context=None):
        ret = super(GiscedataAtTram, self).unlink(cr, uid, ids)
        par_obj = self.pool.get("giscegis.catastre.parceles")
        par_obj.delete_parceles_tram_relations_by_tram_id(cr, uid, ids)
        return ret

    def write(self, cr, uid, ids, values, context=None):
        ret = super(GiscedataAtTram, self).write(cr, uid, ids, values)
        if 'geom' in values:
            if isinstance(ids, int):
                ids = [ids]
            par_obj = self.pool.get("giscegis.catastre.parceles")
            par_obj.delete_parceles_tram_relations_by_tram_id(cr, uid, ids)
            par_obj.vinculate_tram_parcela_by_tram_id(cr, uid, ids)
        return ret


GiscedataAtTram()
