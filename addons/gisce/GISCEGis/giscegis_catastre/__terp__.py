# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Catastre",
    "description": """Representacio dels models del catastre""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_base",
        "giscegis_base_geom",
        "giscedata_at",
        "crm"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/ir.model.access.csv",
        "giscegis_catastre_parceles_view.xml",
        "wizard/giscegis_catastre_upload_parceles.xml",
        "wizard/giscegis_catastre_crm_case.xml",
        "crm_data.xml",
        "res_partner_view.xml"
    ],
    "active": False,
    "installable": True
}
