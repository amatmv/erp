# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from addons import get_module_resource
import base64


class LoadShapeCatastreTest(testing.OOTestCase):
    """"""

    def test_wizard_catastre_shp(self):
        """
        Tests to load the catastre shape
        :return: None
        """

        with Transaction().start(self.database) as txn:

            cursor = txn.cursor
            uid = txn.user

            upload_obj = self.openerp.pool.get('giscegis.catastre.upload.parceles')
            parceles_obj = self.openerp.pool.get('giscegis.catastre.parceles')

            wizard_id = [upload_obj.create(cursor, uid, {})]
            #wizard_id_list = []
            #wizard_id_list.append(wizard_id)

            shp_url = get_module_resource('giscegis_catastre', 'tests', 'fixtures', 'SUBPARCE_MASTER.zip')

            with open(shp_url) as fd_lat:
                shp_data = fd_lat.read()

            data_base64 = base64.encodestring(shp_data)
            upload_obj.write(cursor, uid, wizard_id, {"file": data_base64})
            ids = parceles_obj.search(cursor, uid, [])
            self.assertNotEqual(ids, 0)

