from osv import fields, osv
from giscegis_base_geom.fields import Point
from giscegis_base_geom.giscegis_session import SessionManaged
from tools import config


class GiscedataCts(SessionManaged):

    _name = 'giscedata.cts'
    _inherit = 'giscedata.cts'

    _columns = {
        'geom': Point("Geometria", srid=config.get('srid', 25830), select=True),
        'rotation': fields.integer("Rotation"),
        "node_id": fields.many2one("giscegis.nodes", "Node",
                                   ondelete='set null')
    }

    _defaults = {
        'rotation': lambda *a: 0,
    }


GiscedataCts()


class GiscegisTrafosCts(osv.osv):
    _name = "giscegis.trafos.cts"
    _description = "Llistat de Trafos per CT"
    _auto = False
    _columns = {
      'id': fields.integer('Id', readonly=True),
      'name': fields.char('Name', size=5, readonly=True),
      'idct': fields.integer('IdCT', readonly=True),
      'codict': fields.char('CodiCT', size=10, readonly=True),
      'toma': fields.char('Toma', size=3, readonly=True),
      'tensio_primari': fields.integer('Tensio Primari'),
      'tensio_b1': fields.integer('Tensio B1', readonly=True),
      'tensio_b2': fields.integer('Tensio B2', readonly=True),
      'amp_nom_b1': fields.integer('Intensitat B1', readonly=True),
      'amp_nom_b2': fields.integer('Intensitat B2', readonly=True),
      'potencia_nominal': fields.integer('Potencia Nominal', readonly=True),
    }

    _defaults = {
    }

    _order = 'name, id'

    def init(self, cr):
        cr.execute("""drop view if exists giscegis_trafos_cts""")
        cr.execute("""
          create or replace view giscegis_trafos_cts as (
    SELECT giscedata_transformador_trafo.id,
    giscedata_transformador_trafo.name,
    giscedata_cts.id AS idct,
    giscedata_cts.name AS codict,
    giscedata_transformador_connexio.name AS toma,
    giscedata_transformador_connexio.tensio_primari,
    giscedata_transformador_connexio.tensio_b1,
    giscedata_transformador_connexio.tensio_b2,
    giscedata_transformador_connexio.amp_nom_b1,
    giscedata_transformador_connexio.amp_nom_b2,
    giscedata_transformador_trafo.potencia_nominal
    FROM giscedata_transformador_trafo
    LEFT JOIN giscedata_cts ON (giscedata_transformador_trafo.ct = giscedata_cts.id)
    LEFT JOIN giscedata_transformador_connexio on (giscedata_transformador_connexio.trafo_id = giscedata_transformador_trafo.id)
    WHERE giscedata_transformador_connexio.conectada=true
    )
        """)


GiscegisTrafosCts()
