# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS CTS",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscedata_cts",
        "giscedata_transformadors",
        "giscegis_vertex",
        "giscegis_base",
        "giscegis_base_geom",
        "giscegis_cataleg",
        "giscegis_nodes",
        "giscegis_node_bloc_model"
    ],
    "init_xml": [],
    "demo_xml": [
        "giscegis_cts_demo.xml"
    ],
    "update_xml":[
        "giscegis_cts_view.xml",
        "ir.model.access.csv",
        "security/ir.model.access.csv",
        "giscegis_cataleg_cts_data.xml"
    ],
    "active": False,
    "installable": True
}
