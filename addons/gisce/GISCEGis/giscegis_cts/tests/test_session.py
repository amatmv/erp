from destral import testing
from destral.transaction import Transaction


__all__ = [
    'TestCtsSession'
]


class TestCtsSession(testing.OOTestCase):

    def setUp(self):
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user

    def tearDown(self):
        self.txn.stop()

    def test_search_session_cts(self):
        """
        This test is for check the correct behaviour of the sessions and the CTs
        """
        # Load ERP models
        cts_obj = self.openerp.pool.get('giscedata.cts')
        session_obj = self.openerp.pool.get('giscegis.session')

        # Clean the sessions to avoid BBox collisions
        sql = """
            TRUNCATE giscegis_session CASCADE
        """
        self.cursor.execute(sql)

        # Create the sessions
        session1_vals = {
            "name": 'Testing session 1',
            "bbox": 'POLYGON((982449.3 4664776.0, 982760.4 4664776.0, '
                    '982760.4 4664950.2, 982449.3 4664950.2, '
                    '982449.3 4664776.0))',
            "state": 'open',
            "ttl": 500000000,
            "active": True
        }
        session2_vals = {
            "name": 'Testing session 2',
            "bbox": 'POLYGON((981216.7 4664152.6, 981771.6 4664152.6, '
                    '981771.6 4664463.2, 981216.7 4664463.2, '
                    '981216.7 4664152.6))',
            "state": 'open',
            "ttl": 500000000,
            "active": True
        }
        session1_id = session_obj.create(self.cursor, self.uid, session1_vals)
        session2_id = session_obj.create(self.cursor, self.uid, session2_vals)

        # Start the checks
        # Check the number of CTs in differents sessions and without sessions
        # No session
        cts_ids = cts_obj.search(
            self.cursor, self.uid,
            ['|',
             ('id_installacio.name', '!=', 'SE'),
             ('id_installacio', '=', False)
             ]
        )
        self.assertEqual(len(cts_ids), 9)
        # Session 1
        cts_ids = cts_obj.search(
            self.cursor, self.uid,
            ['|',
             ('id_installacio.name', '!=', 'SE'),
             ('id_installacio', '=', False)
             ], context={'session': session1_id}
        )
        self.assertEqual(len(cts_ids), 0)
        # Session 2
        cts_ids = cts_obj.search(
            self.cursor, self.uid,
            ['|',
             ('id_installacio.name', '!=', 'SE'),
             ('id_installacio', '=', False)
             ], context={'session': session2_id}
        )
        self.assertEqual(len(cts_ids), 6)

        # Get 2 ids of the Session 2
        ct1_id = cts_ids[0]
        ct2_id = cts_ids[1]

        # Check that the changes made on a session are only visible from the
        # session
        # Write new data on 2 CT from session 2
        cts_write_vals = {
            "rotation": 240
        }
        cts_obj.write(
            self.cursor, self.uid, [ct1_id, ct2_id], cts_write_vals,
            context={'session': session2_id}
        )
        # Check the changes outside the session
        cts_data = cts_obj.read(
            self.cursor, self.uid, [ct1_id, ct2_id], ['rotation']
        )
        for data in cts_data:
            self.assertEqual(data['rotation'], 90)
        # Check the changes inside the session
        cts_data = cts_obj.read(
            self.cursor, self.uid, [ct1_id, ct2_id], ['rotation'],
            context={'session': session2_id}
        )
        for data in cts_data:
            self.assertEqual(data['rotation'], 240)
