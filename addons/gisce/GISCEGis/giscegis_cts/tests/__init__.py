# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
import ast
from .test_session import *
# from .test_topologia import *


class CtsGeomTest(testing.OOTestCase):

    def test_catalog_elements_cts(self):
        """
        Test that are elements for CTS and how many of each type are searching
        with the catalog domain.
        :return: None
        """

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            # INICIALITZEM OBJECTES
            cts_obj = self.openerp.pool.get('giscedata.cts')
            cataleg_obj = self.openerp.pool.get('giscegis.cataleg.cataleg')
            sum_cts = 0

            # SELECCIONEM UNICAMENT ELS ELEMENTS DE CATALEG QUE SIGUIN ESCOMESES
            ids_cts_cataleg = cataleg_obj.search(
                cursor, uid, [('model.model', '=', 'giscedata.cts')]
            )

            # PER A CADA ELEMENT DEL CATALEG COMPROVAREM QUE HI HAGIN ELEMENTS
            cataleg_cts_elems = cataleg_obj.read(
                cursor, uid, ids_cts_cataleg, ['name', 'domain']
            )

            for cataleg_elem in cataleg_cts_elems:

                cts_ids = cts_obj.search(
                    cursor, uid, ast.literal_eval(cataleg_elem['domain'])
                )

                sum_cts += len(cts_ids)
                # PER SI EN UN FUTUR S'HA D'AFEGIR ALGUNA DIFERENCIA EN ELS
                # CTS
                if cataleg_elem['name'] == "Centre Transformador":
                    self.assertEqual(len(cts_ids), 9)

            self.assertEqual(sum_cts, 9)
