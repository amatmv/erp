# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Tensió Bloc",
    "description": """GIS suport for Tensió Bloc""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "giscegis_base",
        "giscegis_base_geom",
        "giscegis_cataleg",
        "giscegis_nodes",
        "giscedata_tensions",
        "giscedata_cts",
    ],
    "init_xml": [],
    "demo_xml": [
        "giscegis_tensio_bloc_sequence_demo.xml",
        "giscegis_tensio_bloc_demo.xml"
    ],
    "update_xml":[
        "giscegis_tensio_bloc_sequence.xml",
        "giscegis_cataleg_tensio_bloc_data.xml",
        "security/ir.model.access.csv",
        "giscegis_tensio_bloc_view.xml"
    ],
    "active": False,
    "installable": True
}
