# -*- coding: utf-8 -*-
from osv import fields
from giscegis_base_geom.fields import Point
from giscegis_base_geom.giscegis_session import SessionManaged
from tools import config


class GiscegisTensioBloc(SessionManaged):

    _name = 'giscegis.tensio.bloc'
    _description = "Tensió Bloc"

    def _get_tensio_val(self, cursor, uid, ids, f_name, arg, context=None):
        res = {}
        tensio_norm_obj = self.pool.get("giscedata.tensions.tensio")
        tensio_norm_cache = {}
        for elem in self.read(cursor, uid, ids, ['tensio_id']):
            if elem['tensio_id'] in tensio_norm_cache:
                res[elem['id']] = tensio_norm_cache[elem['tensio_id'][0]]
            else:
                tensio_val = tensio_norm_obj.read(
                    cursor, uid, elem['tensio_id'][0], ['tensio']
                )['tensio']
                tensio_norm_cache[elem['tensio_id'][0]] = tensio_val
                res[elem['id']] = tensio_val
        return res

    def _tensio_blocs_to_update(self, cursor, uid, ids, context=None):
        return ids

    def _get_next_name(self, cursor, uid, context=None):
        """
        Function to get the next name of the sequence for the TensioBloc class
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :param context: OpenERP context
        :type context: dict[str, Any]
        :return: The next name for the TensioBloc set by the sequence
        :rtype: str
        """

        return self.pool.get('ir.sequence').get(
            cursor, uid, self._name
        )

    _columns = {
        'geom': Point("Geometria", srid=config.get('srid', 25830), select=True),
        'rotation': fields.integer("Rotation"),
        'node_id': fields.many2one("giscegis.nodes", "Node",
                                   ondelete='set null'),
        'tensio_id': fields.many2one(
            "giscedata.tensions.tensio", "Tensio Normalitzada", required=True
        ),
        'tensio': fields.function(
            _get_tensio_val, method=True, type='integer', string='Tensio',
            store={
                'giscegis.tensio.bloc': (
                    _tensio_blocs_to_update,
                    ['tensio_id'],
                    10
                ),
            }
        ),
        'codi': fields.char("Codi AutoCAD", size=64),
        'name': fields.char("Codi", size=64),
        "active": fields.boolean("Actiu")
    }

    _defaults = {
        'rotation': lambda *a: 0,
        'name': _get_next_name,
        'active': lambda *a: 1,
    }


GiscegisTensioBloc()
