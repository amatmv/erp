# *-* coding: utf-8 *-*

from osv import osv, fields


class GiscegisTerresGeom(osv.osv):
    _name = 'giscegis.terres.geom'
    _columns = {
        'id': fields.integer('Id', required=True),
        'color': fields.integer('Color'),
        'style': fields.text('Style'),
        'textstring': fields.text('Text'),
        'text_size': fields.float('Text Size'),
        'text_angle': fields.float('Text Angle'),
    }

    def init(self, cursor):
        conf_obj = self.pool.get('res.config')
        srid = int(conf_obj.get(cursor, 1, 'giscegis_srid', '25831'))
        cursor.execute('''
            SELECT * FROM information_schema.columns
            WHERE table_name = 'giscegis_terres_geom'
            AND COLUMN_NAME = 'geom'

            ''')
        if not cursor.fetchone():
            sql_query = """SELECT AddGeometryColumn(
            'public','giscegis_terres_geom',
            'geom',%(srid)s,'geometry',2,true);"""
            cursor.execute(sql_query, {'srid': srid})
        return True


GiscegisTerresGeom()


class GiscegisResistivitatGeom(osv.osv):
    _name = 'giscegis.resistivitat.geom'
    _columns = {
        'id': fields.integer('Id', required=True),
        'color': fields.integer('Color'),
        'style': fields.text('Style'),
        'textstring': fields.text('Text'),
        'text_size': fields.float('Text Size'),
        'text_angle': fields.float('Text Angle'),
    }

    def init(self, cursor):
        conf_obj = self.pool.get('res.config')
        srid = int(conf_obj.get(cursor, 1, 'giscegis_srid', '25831'))
        cursor.execute('''
            SELECT * FROM information_schema.columns
            WHERE table_name = 'giscegis_resistivitat_geom'
            AND COLUMN_NAME = 'geom'

            ''')
        if not cursor.fetchone():
            sql_query = """SELECT AddGeometryColumn(
            'public','giscegis_resistivitat_geom',
            'geom',%(srid)s,'geometry',2,true);"""
            cursor.execute(sql_query, {'srid': srid})
        return True


GiscegisResistivitatGeom()
