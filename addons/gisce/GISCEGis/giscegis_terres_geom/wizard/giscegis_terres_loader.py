# -*- coding: utf-8 -*-
import base64
from StringIO import StringIO
from zipfile import ZipFile, BadZipfile
import os
import itertools
from psycopg2._psycopg import AsIs
from tools.translate import _
from osv import osv, fields
import shapefile
import netsvc


class GiscegisTerresLoader(osv.osv_memory):

    _name = 'giscegis.terres.loader'

    def load_shp(self, cursor, uid, shp_data, dbf_data, table):
        base_geom = self.pool.get('giscegis.base.geom')
        sf = shapefile.Reader(shp=shp_data, dbf=dbf_data)
        shapes = sf.shapes()
        records = sf.records()
        if not records:
            records = [{}] * len(shapes)
        conf_obj = self.pool.get('res.config')
        insert_data = []
        headers = {}
        for index, field in enumerate(sf.fields[1:]):
            headers[field[0].lower()] = index

        srid = int(conf_obj.get(cursor, uid, 'giscegis_srid', 25830))
        insert_sql = """
            INSERT INTO %(table)s
            (id,color,style,textstring,text_size,text_angle,geom)
            VALUES (%(id)s,%(color)s,%(style)s,%(textstring)s,
            %(text_size)s,%(text_angle)s,
            ST_GeomFromText(%(geom)s,%(srid)s));
        """
        sql_data = """
            SELECT MAX(id)+1 AS last_index
            FROM %(table)s;
        """
        cursor.execute(sql_data, {"table": AsIs((table + "_tmp"))})
        data = cursor.fetchone()[0]
        if data:
            last_index = data
        else:
            last_index = 0
        max_text_angle = 0
        base_model = self.pool.get('giscegis.base.geom')

        for index, shape, record in (
                itertools.izip(itertools.count(), shapes, records)
        ):
            points = [shape.points]
            if len(shape.points) > 1:
                wkt_geometry = base_model.to_wkt_multilinestring(points)
            elif len(shapes[index].points) == 1:
                wkt_geometry = base_model.to_wkt_point(points)
            else:
                wkt_geometry = None

            row = {'geom': wkt_geometry,
                   'srid': srid,
                   'color': None,
                   'style': None,
                   'textstring': None,
                   'text_size': None,
                   'text_angle': None,
                   'table': AsIs((table + "_tmp"))}

            if headers:
                if 'text_angle' in headers:
                    text_angle = record[headers['text_angle']]
                    try:
                        row['text_angle'] = str(float(text_angle))
                        if float(text_angle) > max_text_angle:
                            max_text_angle = text_angle
                    except ValueError:
                        row['text_angle'] = text_angle

                if 'height' in headers:
                    height = record[headers['height']]
                    row['text_size'] = base_geom.sanitize_record(height, 'float')
                if 'textstring' in headers:
                    text_string = record[headers['textstring']]
                    row['textstring'] = text_string.decode('latin1')
                if 'style' in headers:
                    row['style'] = record[headers['style']]
                if 'color' in headers:
                    color = record[headers['color']]
                    row['color'] = base_geom.sanitize_record(color, 'int')
            if 'id' in headers:
                row['id'] = records[index][headers['id']]
            else:
                row['id'] = index + last_index
            insert_data.append(row)
        cursor.executemany(insert_sql, insert_data)

        if max_text_angle < 10.0:
            sql_update = """
                UPDATE %(table)s
                SET text_angle=degrees(text_angle);
            """
            cursor.execute(sql_update, {"table": AsIs((table + "_tmp"))})

    def import_data_shp(self, cursor, uid, ids, context=None):
        base_geom = self.pool.get('giscegis.base.geom')
        wizard = self.browse(cursor, uid, ids[0], context)
        wizard.write({'status': ''})
        logger = netsvc.Logger()
        no_file = False

        # Check if the terres file is loaded to the wizard
        if wizard.file_terres:
            data_terres = base64.decodestring(wizard.file_terres)
        else:
            logger.notifyChannel(
                'SHPLoader', netsvc.LOG_INFO,
                _("No s'ha especificat el fitxer de Terres"))
            wizard = self.browse(cursor, uid, ids[0], None)
            base_geom.add_status(
                cursor, uid, ids,
                _("No s'ha especificat el fitxer de Terres"),
                wizard)
            no_file = True

        # Check if the resistivitat file is loaded to the wizard
        if wizard.file_resis:
            data_resis = base64.decodestring(wizard.file_resis)
        else:
            logger.notifyChannel(
                'SHPLoader', netsvc.LOG_INFO,
                _("No s'ha especificat el fitxer de Resistivitat"))
            wizard = self.browse(cursor, uid, ids[0], None)
            base_geom.add_status(
                cursor, uid, ids,
                _("No s'ha especificat el fitxer de Resistivitat"),
                wizard)
            no_file = True

        # If we don't have one or any of the files, we finish the execution.
        if no_file:
            return

        # Retrieve SRID
        conf_obj = self.pool.get('res.config')
        srid = conf_obj.get(cursor, uid, 'giscegis_srid', 25830)

        # Create table template
        sql_create = """
            CREATE TABLE %(table)s (
                id integer,
                create_uid integer,
                create_date timestamp without time zone,
                write_date  timestamp without time zone,
                write_uid integer,
                style text,
                text_angle double precision,
                color integer,
                text_size double precision,
                textstring text,
                geom geometry(Geometry,%(srid)s)
            );
        """

        # Link files with the temporal table to save the data in the files
        data_table = {
            data_terres: 'giscegis_terres_geom',
            data_resis: 'giscegis_resistivitat_geom'
        }
        for (data, table) in data_table.iteritems():

            s = StringIO(data)
            try:
                input_zip = ZipFile(s)
            except BadZipfile:
                if data == data_terres:
                    logger.notifyChannel(
                        'SHPLoader', netsvc.LOG_WARNING,
                        _('Fitxer de cartografia de Terres corrupte'))
                    wizard = self.browse(cursor, uid, ids[0], None)
                    base_geom.add_status(
                        cursor, uid, ids,
                        _('Fitxer Terres corrupte'),
                        wizard)
                else:
                    logger.notifyChannel(
                        'SHPLoader', netsvc.LOG_WARNING,
                        _('Fitxer de cartografia de Resistivitat corrupte')
                    )
                    wizard = self.browse(cursor, uid, ids[0], None)
                    base_geom.add_status(
                        cursor, uid, ids,
                        _('Fitxer Resistivitat corrupte'),
                        wizard)
                return

            # Check if the ZIP file contains the necessary files to load
            # the shapes and load it to the temporal table of the DB
            filenames = sorted(input_zip.namelist())
            data_in_files = False
            if filenames:
                shp_data = None
                dbf_data = None
                for filename in filenames:
                    if os.path.splitext(filename)[1].lower() == '.dbf':
                        dbf_data = StringIO(input_zip.read(filename))
                    if os.path.splitext(filename)[1].lower() == '.shp':
                        shp_data = StringIO(input_zip.read(filename))
                    if shp_data and dbf_data:
                        if not data_in_files:
                            cursor.execute(
                                sql_create,
                                {"srid": srid, "table": AsIs((table + "_tmp"))}
                            )
                        self.load_shp(
                            cursor, uid, shp_data, dbf_data, table
                        )
                        shp_data = None
                        dbf_data = None
                        data_in_files = True
            # If the flag data_in_files is not True, it means that one of the
            # files nedded to load the shape is missing and the execution
            # is stoped.
            if not data_in_files:
                if data == data_terres:
                    logger.notifyChannel(
                        'SHPLoader', netsvc.LOG_WARNING,
                        _(u"No s'han trobat fitxers .shp o .dbf en el fitxer" +
                          u" de Terres. S'atura la importació")
                    )
                    wizard = self.browse(cursor, uid, ids[0], None)
                    base_geom.add_status(
                        cursor, uid, ids,
                        _(u"No s'han trobat fitxers .shp o .dbf en el fitxer" +
                          u" de Terres. S'atura la importació"),
                        wizard
                    )
                else:
                    logger.notifyChannel(
                        'SHPLoader', netsvc.LOG_WARNING,
                        _(u"No s'han trobat fitxers .shp o .dbf en el fitxer" +
                          u" de Resistivitat. S'atura la importació")
                    )
                    wizard = self.browse(cursor, uid, ids[0], None)
                    base_geom.add_status(
                        cursor, uid, ids,
                        _(u"No s'han trobat fitxers .shp o .dbf en el fitxer" +
                          u" de Resistivitat. S'atura la importació"),
                        wizard)
                return
            # At this point the data is loaded correctly to the temporal tables
            # of the DB and will be swaped to the real table.
            # The cache of the TMS will be cleaned too.
            if data == data_terres:
                logger.notifyChannel(
                    'SHPLoader', netsvc.LOG_INFO,
                    _('Dades de Terres carregades correctament'))
                base_geom.switch_table(
                    cursor, uid, (table + "_tmp"), table
                )
                wizard = self.browse(cursor, uid, ids[0], None)
                base_geom.add_status(
                    cursor, uid, ids,
                    _('Dades Terres carregades correctament'),
                    wizard)
                status_cache = base_geom.remove_tms_cache(cursor, uid, 'terres')
                if status_cache:
                    logger.notifyChannel(
                        'SHPLoader', netsvc.LOG_INFO,
                        _('Cache Terres netejat correctament'))
                    wizard = self.browse(cursor, uid, ids[0], None)
                    base_geom.add_status(
                        cursor, uid, ids,
                        _('Cache Terres netejat correctament'),
                        wizard)
                else:
                    logger.notifyChannel(
                        'SHPLoader', netsvc.LOG_ERROR,
                        _('Error al eliminar el cache de Terres'))
                    wizard = self.browse(cursor, uid, ids[0], None)
                    base_geom.add_status(
                        cursor, uid, ids,
                        _('Error al eliminar el cache de Terres'),
                        wizard)
            else:
                logger.notifyChannel(
                    'SHPLoader', netsvc.LOG_INFO,
                    _('Dades de Resisitivitat carregades correctament'))
                base_geom.switch_table(
                    cursor, uid, (table + "_tmp"), table
                )
                wizard = self.browse(cursor, uid, ids[0], None)
                base_geom.add_status(
                    cursor, uid, ids,
                    _('Dades Resisitivitat carregades correctament'),
                    wizard)
                status_cache = base_geom.remove_tms_cache(
                    cursor, uid, 'resistivitat'
                )
                if status_cache:
                    logger.notifyChannel(
                        'SHPLoader', netsvc.LOG_INFO,
                        _('Cache Resisitivitat netejat correctament'))
                    wizard = self.browse(cursor, uid, ids[0], None)
                    base_geom.add_status(
                        cursor, uid, ids,
                        _('Cache Resisitivitat netejat correctament'),
                        wizard)
                else:
                    logger.notifyChannel(
                        'SHPLoader', netsvc.LOG_ERROR,
                        _('Error al eliminar el cache de Resisitivitat'))
                    wizard = self.browse(cursor, uid, ids[0], None)
                    base_geom.add_status(
                        cursor, uid, ids,
                        _('Error al eliminar el cache de Resisitivitat'),
                        wizard)

    def _get_default_url(self, cursor, uid, context=None):
        url = self.pool.get("res.config").get(cursor, uid, "giscegis_gis_url")
        return "{}/load/terres_resis".format(url)

    _columns = {
        'file_terres': fields.binary('Fitxer shape Terres'),
        'file_resis': fields.binary('Fitxer shape Resistivitat'),
        'status': fields.text(_('Resultat')),
        'url': fields.text(_('URL')),
    }
    _defaults = {
        'state': lambda *a: 'init',
        'url': _get_default_url
    }


GiscegisTerresLoader()
