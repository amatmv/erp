# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from addons import get_module_resource
import base64


class GiscegisTerresGeomTest(testing.OOTestCase):

    def test_wizard_load_terres_resistivitat_shp(self):
        """
        Tests the shp load of the modules giscegis_terres_geom
        :return: None
        """

        self.openerp.install_module('giscegis_terres_geom')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            model = self.openerp.pool.get('giscegis.terres.loader')
            res_obj = self.openerp.pool.get('res.config')
            res_data = {
                'name': 'giscegis_gis_url',
                'value': '0'
            }
            res_obj.create(cursor, uid, res_data)
            terres_url = get_module_resource(
                'giscegis_terres_geom',
                'tests',
                'fixtures',
                'TEST_TERRES.zip'
            )
            resis_url = get_module_resource(
                'giscegis_terres_geom',
                'tests',
                'fixtures',
                'TEST_RESISTIVITAT.zip'
            )
            with open(terres_url) as fd_terres:
                terres_data = fd_terres.read()
            with open(resis_url) as fd_resis:
                resis_data = fd_resis.read()

            wizard_id = model.create(cursor, uid, {})
            wizard_load = model.browse(cursor, uid, wizard_id)
            model_data = {'file_terres': base64.encodestring(terres_data),
                          'file_resis': base64.encodestring(resis_data)}
            model.write(cursor, uid, [wizard_id], model_data)
            wizard_load.import_data_shp()

            obj_terres = self.openerp.pool.get('giscegis.terres.geom')
            ids_terres = obj_terres.search(cursor, uid, [])
            self.assertEqual(len(ids_terres), 2)

            obj_resis = self.openerp.pool.get('giscegis.resistivitat.geom')
            ids_resis = obj_resis.search(cursor, uid, [])
            self.assertEqual(len(ids_resis), 2)
