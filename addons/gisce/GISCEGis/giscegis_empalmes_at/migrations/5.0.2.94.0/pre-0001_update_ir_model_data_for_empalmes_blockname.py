# coding=utf-8

import logging


def up(cursor, installed_version):

    logger = logging.getLogger('openerp.migration')
    logger.info('Update the IrModelData module reference for the model '
                'giscegis.blocs.empalmes.grup.blockname')

    sql = """
        UPDATE ir_model_data
        SET module = 'giscegis_empalmes_at'
        WHERE name LIKE '%%giscegis_blocs_empalmes_grup_blockname%%'
        AND module = 'giscegis_blocs_empalmes_grup'
    """

    cursor.execute(sql)

    logger.info('Module reference changed from giscegis_blocs_empalmes_grup to '
                'giscegis_empalmes_at')


def down(cursor, installed_version):
    pass


migrate = up