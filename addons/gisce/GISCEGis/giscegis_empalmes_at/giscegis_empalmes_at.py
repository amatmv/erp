# -*- encoding: utf-8 -*-
from osv import osv, fields
from giscegis_base_geom.fields import Point
from giscegis_base_geom.giscegis_session import SessionManaged
from tools import config


class GiscegisBlocsEmpalmesGrupBlockname(osv.osv):

    _name = 'giscegis.blocs.empalmes.grup.blockname'
    _description = 'Tipus Grups d\'Empalmes AT'

    def create(self, cr, uid, vals, context=None):

        if context is None:
            context = {}

        ids = self.search(cr, uid, [('name', '=', vals['name'])])

        if ids and len(ids):
            return ids[0]
        else:
            return super(osv.osv, self).create(cr, uid, vals)

    _columns = {
      'name': fields.char('Nom', size=20, required=True),
      'description': fields.char('Descripció', size=255),
    }

    _order = "name, id"


GiscegisBlocsEmpalmesGrupBlockname()


class GiscedataAtEmpalmesGrup(SessionManaged):

    _name = "giscedata.at.empalmes.grup"
    _inherit = "giscedata.at.empalmes.grup"

    _columns = {
        'geom': Point("geometry", srid=config.get('srid', 25830), select=True),
        'rotation': fields.integer("Rotation"),
        'tipus_empalme': fields.many2one(
            'giscegis.blocs.empalmes.grup.blockname', 'Tipus Empalme'
        ),
        "node_id": fields.many2one("giscegis.nodes", "Node",
                                   ondelete='set null')
    }

    _defaults = {
        'rotation': lambda *a: 0
    }


GiscedataAtEmpalmesGrup()
