from destral import testing
from destral.transaction import Transaction


__all__ = [
    'TestEmpalmesATSession'
]


class TestEmpalmesATSession(testing.OOTestCase):

    def setUp(self):
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user

    def tearDown(self):
        self.txn.stop()

    def test_search_session_empalmes_at(self):
        """
        This test is for check the correct behaviour of the sessions and the
        Empalmes AT
        """
        # Load ERP models
        session_obj = self.openerp.pool.get('giscegis.session')
        empalmes_at_obj = self.openerp.pool.get('giscedata.at.empalmes.grup')

        # Clean the sessions to avoid BBox collisions
        sql = """
            TRUNCATE giscegis_session CASCADE
        """
        self.cursor.execute(sql)

        # Create the sessions
        session1_vals = {
            "name": 'Testing session 1',
            "bbox": 'POLYGON((982449.3 4664776.0, 982760.4 4664776.0, '
                    '982760.4 4664950.2, 982449.3 4664950.2, '
                    '982449.3 4664776.0))',
            "state": 'open',
            "ttl": 500000000,
            "active": True
        }
        session2_vals = {
            "name": 'Testing session 2',
            "bbox": 'POLYGON((981216.7 4664152.6, 981771.6 4664152.6, '
                    '981771.6 4664463.2, 981216.7 4664463.2, '
                    '981216.7 4664152.6))',
            "state": 'open',
            "ttl": 500000000,
            "active": True
        }
        session1_id = session_obj.create(self.cursor, self.uid, session1_vals)
        session2_id = session_obj.create(self.cursor, self.uid, session2_vals)

        # Start the checks
        # Check the number of Empalmes AT in differents sessions and without
        # session
        # No session
        empalmes_at_ids = empalmes_at_obj.search(self.cursor, self.uid, [])
        self.assertEqual(len(empalmes_at_ids), 6)
        # Session 1
        empalmes_at_ids = empalmes_at_obj.search(
            self.cursor, self.uid, [], context={'session': session1_id}
        )
        self.assertEqual(len(empalmes_at_ids), 0)
        # Session 2
        empalmes_at_ids = empalmes_at_obj.search(
            self.cursor, self.uid, [], context={'session': session2_id}
        )
        self.assertEqual(len(empalmes_at_ids), 2)

        # Get 2 ids of the Session 2
        empalme_at1_id = empalmes_at_ids[0]
        empalme_at2_id = empalmes_at_ids[1]

        # Check that the changes made on a session are only visible from the
        # session
        # Load data before changes
        empalmes_at_data_original = empalmes_at_obj.read(
            self.cursor, self.uid, [empalme_at1_id, empalme_at2_id],
            ['rotation']
        )
        # Write new data on 2 Empalmes AT from session 2
        empalmes_vals_to_write = {
            'rotation': 240
        }
        empalmes_at_obj.write(
            self.cursor, self.uid, [empalme_at1_id, empalme_at2_id],
            empalmes_vals_to_write, context={'session': session2_id}
        )
        # Check the changes outside the session
        empalmes_at_data = empalmes_at_obj.read(
            self.cursor, self.uid, [empalme_at1_id, empalme_at2_id],
            ['rotation']
        )
        # Compare data outside session before and after write data on session 2
        self.assertEqual(
            empalmes_at_data_original[0]['rotation'],
            empalmes_at_data[0]['rotation']
        )
        self.assertEqual(
            empalmes_at_data_original[1]['rotation'],
            empalmes_at_data[1]['rotation']
        )
        # Check the changes inside the session
        empalmes_at_data = empalmes_at_obj.read(
            self.cursor, self.uid, [empalme_at1_id, empalme_at2_id],
            ['rotation'],
            context={'session': session2_id}
        )
        for data in empalmes_at_data:
            self.assertEqual(data['rotation'], 240)
