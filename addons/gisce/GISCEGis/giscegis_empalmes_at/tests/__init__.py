# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
import ast
from .test_session import *


class ElementsTallGeomTest(testing.OOTestCase):

    def test_catalog_elements_empalmes_at(self):
        """
        Test that are elements for empalmes at and how many of each type
        are searching with the catalog domain.
        :return: None
        """

        with Transaction().start(self.database) as txn:

            cursor = txn.cursor
            uid = txn.user

            # INICIALITZEM OBJECTES
            empalmes_at_obj = self.openerp.pool.get(
                'giscedata.at.empalmes.grup'
            )
            cataleg_obj = self.openerp.pool.get('giscegis.cataleg.cataleg')
            sum_empalmes_at = 0

            # SELECCIONEM UNICAMENT ELS ELEMENTS DE CATALEG QUE SIGUIN
            # EMPALMES AT
            ids_empalmes_at_cataleg = cataleg_obj.search(
                cursor, uid,
                [('model.model', '=', 'giscedata.at.empalmes.grup')]
            )

            # PER A CADA ELEMENT DEL CATALEG COMPROVAREM QUE HI HAGIN ELEMENTS
            cataleg_empalmes_at_elems = cataleg_obj.read(
                cursor, uid, ids_empalmes_at_cataleg, ['name', 'domain']
            )
            for cataleg_elem in cataleg_empalmes_at_elems:
                empalmes_at_ids = empalmes_at_obj.search(
                    cursor, uid, ast.literal_eval(cataleg_elem['domain'])
                )
                sum_empalmes_at += len(empalmes_at_ids)
                if cataleg_elem['name'] == "Empalme Mitja Tensió":
                    self.assertEqual(len(empalmes_at_ids), 3)
                elif cataleg_elem['name'] == "Empalme Terminal":
                    self.assertEqual(len(empalmes_at_ids), 3)
            self.assertEqual(sum_empalmes_at, 6)
