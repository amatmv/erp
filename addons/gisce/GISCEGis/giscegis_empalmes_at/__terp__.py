# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS EMPALMES AT",
    "description": """Modul de GIS per Empalmes AT""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_base",
        "giscegis_base_geom",
        "giscegis_cataleg",
        "giscedata_at",
        "giscegis_nodes"
    ],
    "init_xml": [],
    "demo_xml": [
        "giscegis_empalmes_at_demo.xml"
    ],
    "update_xml":[
        "security/ir.model.access.csv",
        "giscegis_cataleg_empalmes_at_data.xml",
        "giscegis_empalmes_at_data.xml",
        "giscegis_empalmes_at_view.xml"
    ],
    "active": False,
    "installable": True
}
