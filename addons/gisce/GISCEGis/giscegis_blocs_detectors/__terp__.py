# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Blocs Detectors",
    "description": """
Afegir en el DefDWG:

* En l'apartat <DefDataBases><DefDataBase><DefTables>:

<DefTable ID="giscegis.blocs.detectors" TableName="giscegis.blocs.detectors" ForceDelete="false" CleanAfter="true"/>


* En l'apartat <DefBlockReferences>:

<DefBlockReference ID="DETECTORS" BlockNamePattern="^NORTROLL$" Table="giscegis.blocs.detectors"
        TableDBcreation="">
  <DefAttribute Tag="ID" Unique="true" Pattern=".+" DBfield="codi"/>
</DefBlockReference>""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_vertex",
        "giscegis_nodes",
        "giscedata_at_detectors",
        "giscegis_at_detectors"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscegis_blocs_detectors_view.xml",
        "giscedata_at_detectors_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
