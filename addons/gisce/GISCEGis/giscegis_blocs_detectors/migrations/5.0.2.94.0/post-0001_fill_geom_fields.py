# coding=utf-8

import logging
import pooler


def up(cursor, installed_version):

    logger = logging.getLogger('openerp.migration')
    logger.info('Migrating the X & Y values from vertex as a geom '
                'field into Detectors AT.')

    pool = pooler.get_pool(cursor.dbname)
    detectors_at_obj = pool.get("giscedata.at.detectors")
    detectors_at_obj.fill_geom(cursor, 1)


def down(cursor, installed_version):
    pass


migrate = up
