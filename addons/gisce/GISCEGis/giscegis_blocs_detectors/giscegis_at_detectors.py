# *-* coding: utf-8 *-*
from osv import osv, fields
from psycopg2.extensions import AsIs
from tools import config
from giscegis_base_geom.wizard.giscegis_shp_loader \
    import GIS_MODELS_TO_UPDATE_GEOM_DATA


# Subscribe this model to the list to be updated if there is an AutoCAD dump
GIS_MODELS_TO_UPDATE_GEOM_DATA.append(
    {
        'model': 'giscedata.at.detectors',
        'functions': ['fill_geom', 'fill_rotation']
    },
)


class GiscedataAtDetectors(osv.osv):

    _name = 'giscedata.at.detectors'
    _inherit = 'giscedata.at.detectors'

    def _coords(self, cursor, uid, ids, field_name, arg, context):
        """
        Returns the X & Y position of the Detector AT
        :param cursor: Database cursor
        :type cursor: Psycopg2 cursor
        :param uid: User id
        _type uid: int
        :param ids: Identifiers of the detector

        :param field_name:
        :param arg:
        :param context: OpenERP context
        :return:
        :rtype: dict{id: {x:x, y:y}}
        """

        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        resp = dict.fromkeys(ids, {"x": None, "y": None})

        sql = """
            SELECT d.id AS id,
                   COALESCE (ST_X(d.geom), v.x) AS x,
                   COALESCE (ST_Y(d.geom), v.y) AS y
            FROM giscedata_at_detectors d
            LEFT JOIN giscegis_blocs_detectors b ON b.detector = d.id
            LEFT JOIN giscegis_vertex v ON v.id = b.vertex
            WHERE d.id IN %(ids)s
        """

        cursor.execute(sql, {'ids': tuple(ids)})

        data = cursor.dictfetchall()

        for line in data:
            resp[line['id']] = {"x": line['x'], "y": line['y']}

        return resp

    def fill_geom(self, cursor, uid):
        """
        Function that runs a query to update the field geom of all the
        Detectors AT elements
        :param cursor: Database cursor
        :type cursor: Psycopg2 cursor
        :param uid: User ID
        :type uid: int
        """

        sql = """
            UPDATE giscedata_at_detectors AS d
            SET geom = b.geom
            FROM (
              SELECT b.detector AS id,
                     st_setsrid(st_makepoint(v.x, v.y), %(srid)s) AS geom
              FROM giscegis_blocs_detectors b
              LEFT JOIN giscegis_vertex v ON b.vertex = v.id
            ) AS b
            WHERE d.id=b.id;
            UPDATE giscedata_at_detectors SET geom = St_SnapToGrid(
                ST_Translate(geom, 0.0000000001, 0.0000000001), 0.0001
            );
        """

        cursor.execute(sql, {"srid": AsIs(config.get("srid", 25830))})

    def fill_rotation(self, cursor, uid):
        """
        Function that runs a query to update the field rotation of all the
        Detectors AT elements
        :param cursor: Database cursor
        :type cursor: Psycopg2 cursor
        :param uid: User ID
        :type uid: int
        """

        sql = """
            UPDATE giscedata_at_detectors AS d
            SET rotation = b.rotation
            FROM (
              SELECT b.detector AS id,
                     (360-rotation) AS rotation
              FROM giscegis_blocs_detectors b
            ) AS b
            WHERE d.id=b.id
        """

        cursor.execute(sql)

    _columns = {
        'x': fields.function(
            _coords, method=True, type='float', string='X', multi="coords"
        ),
        'y': fields.function(
            _coords, method=True, type='float', string='Y', multi="coords"
        ),
    }


GiscedataAtDetectors()
