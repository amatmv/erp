# -*- coding: utf-8 -*-
{
    "name": "Index CUPS (distri)",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Index for CUPS
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "base_index",
        "giscegis_cups_index",
        "giscedata_cups_distri",
        "giscedata_cups_distri_index",
        "giscegis_search_type"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscegis_search_type_data.xml"
    ],
    "active": False,
    "installable": True
}
