from giscegis_base_index.giscegis_base_index import GiscegisBaseIndex


class GiscedataCupsPs(GiscegisBaseIndex):
    """
    Class to index CUPS
    """
    _name = 'giscedata.cups.ps'
    _inherit = 'giscedata.cups.ps'


GiscedataCupsPs()


class GiscedataCupsEscomesa(GiscegisBaseIndex):
    """
    Class to index Escomesa
    """
    _name = 'giscedata.cups.escomesa'
    _inherit = 'giscedata.cups.escomesa'

    def get_lat_lon(self, cursor, uid, item, context=None):
        """
        Method to get the latitude and longitude of the escomesa to index

        :param cursor: Database cursor
        :param uid: User id
        :param item: Item ot index
        :param context: OpenERP context
        :return: Tuple with lat and lon
        """
        if context is None:
            context = {}
        cursor.execute("""
        SELECT
            ST_Y(ST_Transform(geom,4326)) AS lat,
            ST_X(ST_Transform(geom,4326)) AS lon
        FROM giscedata_cups_escomesa
        WHERE id = %(id)s
        """, {'id': item.id})
        res = cursor.fetchone()
        if not res:
            res = (0, 0)
        return res


GiscedataCupsEscomesa()
