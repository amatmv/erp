# -*- coding: utf-8 -*-
from osv import fields, osv
from giscegis_base_geom.giscegis_base_geom import DROP_ON_AUTOCAD_DUMP

# Subscribe this model to the list to be updated if there is an AutoCAD dump
DROP_ON_AUTOCAD_DUMP.append('giscegis_blocs_fronteres')


class giscedata_bt_fronteres_blockname(osv.osv):
    _name = 'giscedata.bt.fronteres.blockname'
    _inherit = 'giscedata.bt.fronteres.blockname'

    def create(self, cr, uid, vals, context={}):
        ids = self.search(cr, uid, [('name', '=', vals['name'])])
        if ids and len(ids):
            return ids[0]
        else:
            return super(osv.osv, self).create(cr, uid, vals)

giscedata_bt_fronteres_blockname()


class giscegis_blocs_fronteres(osv.osv):
    _name = 'giscegis.blocs.fronteres'
    _description = 'GISCEGis Blocs per FUS_ADU, FUS_DSP, CUCHILLAS i BORNA'

    _columns = {
      'name': fields.char('Name', size=50, required=True),
      'vertex': fields.many2one('giscegis.vertex', 'Vertex',
                                ondelete='set null'),
      'width': fields.float('Width'),
      'height': fields.float('Height'),
      'rotation': fields.float('Rotation'),
      'blockname': fields.many2one('giscedata.bt.fronteres.blockname',
                                   'Blockname'),
      'node': fields.many2one('giscegis.nodes', 'Node', ondelete='set null'),
      'codi': fields.char('Codi', size=50),
      'frontera': fields.many2one('giscedata.bt.fronteres', 'Frontera'),
    }

    def create(self, cursor, uid, vals, context=None):
        """Mètode sobreescrit per escriure als camps many2one des
        d'aquí.

        Ens hem d'assegurar de què els mètodes create() dels objectes
        referenciats estiguin alhora sobreescrits per tal què comprovin
        si l'objecte existeix o no."""
        _fields = [('vertex', 'giscegis.vertex'),
                   ('blockname', 'giscegis.bt.fronteres.blockname')]
        for field in _fields:
            if field[0] in vals and isinstance(vals[field[0]], list):
                obj = self.pool.get(field[1])
                remote_id = obj.create(cursor, uid, vals[field[0]][2])
                if remote_id:
                    vals[field[0]] = remote_id
        return super(osv.osv, self).create(cursor, uid, vals)

giscegis_blocs_fronteres()
