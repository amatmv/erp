# -*- coding: utf-8 -*-
{
    "name": "GISCEGis Blocs FUS_ADU FUS_DSP CUCHILLAS BORNA",
    "description": """Model pels blocs FUS_ADU, FUS_DSP, CUCHILLAS i BORNA""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GIS",
    "depends":[
        "base",
        "giscedata_bt_fronteres",
        "giscegis_base",
        "giscegis_nodes"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscegis_blocs_fronteres_view.xml",
        "ir.model.access.csv",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
