from osv import osv,fields

class giscegis_nodes_topologia_171(osv.osv):

    def get_time(self, cr, uid):
        import time
        return time.strftime('%Y-%m-%d %H:%M:%S')

    _name = 'giscegis.nodes.topologia.171'
    _description = 'Nodes per GIS'
    _auto = False
    _columns = {
      'id': fields.integer('ID', readonly=True),
      'name': fields.char('IDNode', size=10, readonly=True),
      'x': fields.float('x', readonly=True),
      'y': fields.float('y', readonly=True),
    }

    _defaults = {

    }
    def init(self, cr):
        cr.execute("""drop view if exists giscegis_nodes_topologia_171""")
        cr.execute("""
        create or replace view giscegis_nodes_topologia_171 as (
          select n.id, n.name, v.x, v.y
          from giscegis_nodes n
          left join giscegis_vertex v on (n.vertex = v.id)
        )
        """)

    _order = "name, id"

giscegis_nodes_topologia_171()
