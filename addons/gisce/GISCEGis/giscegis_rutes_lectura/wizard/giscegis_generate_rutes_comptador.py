# -*- coding: utf-8 -*-
from tools.translate import _
from osv import osv, fields


class GiscegisGenerateRutesComptador(osv.osv_memory):
    """
    Wizard to fill the table giscegis_rutes_comptadors
    """

    _name = 'giscegis.generate.rutes.comptador'

    def _generar_comptadors(self, cursor, uid, ids, context=None):
        """
        Method that handles the click button of the wizard

        :param cursor: Database cursor
        :param uid: User id
        :param ids: list of ids of the wizard
        :param context: Open ERP context
        :return: None
        :rtype: None
        """
        if context is None:
            context = {}

        mod_comp = self.pool.get("giscegis.rutes.comptadors")

        msg_state = _("El lot ha d'esta en estat obert per poder-se exportar")
        msg_nogeom = _("No s'ha trobat l'ubicacio dels seguentes comptadors:\n")

        lot = self.browse(cursor, uid, ids[0], context).lot_id.id
        state = self.browse(cursor, uid, ids[0], context).lot_id.state

        mod_comp.fill_data(cursor, uid, [lot])

        total = mod_comp.search(cursor, uid, [])
        no_geom = mod_comp.search(cursor, uid, [("geom", "=", False)])
        points = len(total) - len(no_geom)
        msg = _("Valors escrits correctament. S'han creat {} punts").format(points)
        if state != 'obert':
            msg += "\n{}".format(msg_state)

        data_no_geom = mod_comp.read(cursor, uid, no_geom, ["contador"])
        if no_geom:
            msg += "\n" + msg_nogeom
            for line in data_no_geom:
                if line["contador"]:
                    msg += "{}\n".format(line["contador"])

        write_data = {
            "status": msg,
        }
        self.write(cursor, uid, ids, write_data)

    def _default_lot_id(self, cursor, uid, context=None):
        """
        Generates the default value for lot_id field

        :param cursor: Database cursor, unused
        :param uid: User id
        :type uid: int
        :param context: Open ERP context
        :type context: dict
        :return: Active id of the view to set on the lot_id field
        :rtype: int
        """
        if context is None:
            context = {}
        return context.get('active_id', False)

    _columns = {
        "lot_id": fields.many2one("giscedata.facturacio.lot", "Lot"),
        'status': fields.text(_('Resultat')),
    }
    _defaults = {
        'state': lambda *a: 'init',
        'lot_id': _default_lot_id,
    }


GiscegisGenerateRutesComptador()
