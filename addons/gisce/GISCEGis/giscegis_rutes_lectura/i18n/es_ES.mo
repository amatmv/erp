��            )   �      �     �  ,   �     �     �     �  &   �       "   5     X     \  5   v  �   �     @  J   I  1   �     �     �     �     �  !   �       	             "     %     1     @     H  �  M     �  *   �          %     5  %   >     d  &   �     �     �  @   �  �     	   �  V   �  6   	     8	     E	     N	     W	  !   `	     �	     �	     �	     �	     �	     �	     �	     �	           
            	                                                                                                                   Cancel·lar Comptadors que no comuniquen per a les rutes Data final lot Estat contract Exportar Exportar contadors sense lectura a GIS GISCE GISv3 Rutas de lectura Invalid XML for View Architecture! Lot Model de rutas de lectura No s'ha trobat l'ubicacio dels seguentes comptadors:
 Només s'exportaran els contadors de tipus 5(2.0A,2.0DHA,2.1A,2.1DHA) en estat obert d'aquest lot de facturació i qu eno estiguin carregats en TPL Resultat The Object name must start with x_ and not contain any special character ! Valors escrits correctament. S'han creat {} punts concentrador contador contrato direccio giscegis.generate.rutes.comptador id last_read name tg tg_cnc_conn ultima lectura unknown zona Project-Id-Version: OpenERP Server 5.0.14
Report-Msgid-Bugs-To: support@openerp.com
POT-Creation-Date: 2018-08-09 14:15+0200
PO-Revision-Date: 2018-08-09 14:16+0200
Last-Translator: Gisce <devel@gisce.net>
Language-Team: 
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.7.1
 Cancelar Contadores que no comunican para las rutas Fecha final de lote Estado contrato Exportar Exportar contadores sin lectura a GIS GISCE GISv3 Rutas de lectura Arquitectura de la vista XML invalida! Lote Modelo de ruta de lectura No se ha encontrado la ubicación de los siguientes contadores:
 Solo se exportaran los contadores de tipo 5(2.0A,2.0DHA,2.1A,2.1DHA) en estado "abierto " de este lote de facturación y que no estén cargados en TPL. Resultado El nombre del objeto debe empezar con x_ y n opuede contener ningun caracter especial! Valores escritos correctamente.Se han creado {} puntos concentrador contador contrato direcion giscegis.generate.rutes.comptador id ultima lectura nombre tg tg_cnc_conn ultima lectura desconocido zona 