# -*- coding: utf-8 -*-
{
    "name": "GISCE GISv3 Rutas de lectura",
    "description": """Model de rutas de lectura""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends": [
        "base",
        "giscegis_base_geom",
        "giscegis_telegestio_geom"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/ir.model.access.csv",
        "wizard/giscegis_generate_rutes_comptador.xml"
    ],
    "active": False,
    "installable": True
}
