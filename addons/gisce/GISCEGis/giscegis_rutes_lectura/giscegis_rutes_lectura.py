from osv import osv, fields
from giscegis_base_geom import fields as geo_fields
from tools.config import config


class GiscegisRutesComptadors(osv.osv):
    """
    Model to show the escomeses that don't comunicate on the GIS
    """

    _name = 'giscegis.rutes.comptadors'
    _description = 'Comptadors que no comuniquen per a les rutes'
    _auto = False
    _rec_name = "geom"
    _columns = {
        "geom": geo_fields.Point(srid=config.get("srid", 25830)), #de la escomesa
        "cups_id": fields.char("id", size=256),
        "cups_name": fields.char("name", size=256),
        "cups_direccio": fields.char("direccio", size=256),
        "cups_zona": fields.char("zona", size=256),
        "direccio": fields.char("direccio", size=256),
        "zona": fields.char("zona", size=256),
        "contador": fields.char("contador", size=256),
        "concentrador": fields.char("concentrador", size=256),
        "tg_last_read": fields.char("last_read", size=256),
        "tg_cnc_conn": fields.char("tg_cnc_conn", size=256),
        "tg": fields.char("tg", size=256),
        "data_ultima_lectura": fields.char("ultima lectura", size=256),
        "contract": fields.char("contrato", size=256),
        "contract_state": fields.char("Estat contract", size=256),
        "lote_facturacion": fields.char("Lot", size=256),
        "data_final_lot": fields.char("Data final lot", size=256)
    }

    def fill_data(self, cursor, uid, ids, context=None):
        """
        Fills the data o f the model giscegis_rutes_comptadors

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: list of ids
        :type ids: list(int)
        :param context: OpenERP context
        :type context: dict
        :return:None
        :rtype: None
        """
        if isinstance(ids, list):
            ident = ids[0]
        else:
            ident = ids

        params = {
            "lote_facturacion": ident,
            "srid": config.get("srid", 25830)
        }
        cursor.execute("DROP MATERIALIZED VIEW IF EXISTS giscegis_rutes_comptadors")
        cursor.execute("""
          CREATE MATERIALIZED VIEW giscegis_rutes_comptadors AS (
            SELECT
              cups.id as id,
              cups.id AS cups_id,
              cups.name AS cups,
              cups.direccio AS direccion_cups,
              cups.zona AS ruta,
              c.name AS contador,
              con.name AS concentrador,
              c.tg_last_read AS ultimo_cierre_valido,
              c.tg_cnc_conn AS conectado,
              p.tg,
              p.data_ultima_lectura AS ultima_lectura_fact,
              p.name AS contrato,
              p.state AS estado_contrato,
              l.name AS lote_facturacion,
              esc.geom as geom,
              l.data_final AS data_final_lot
            FROM
            giscedata_facturacio_contracte_lot AS cl 
            LEFT JOIN giscedata_facturacio_lot AS l ON l.id = cl.lot_id
            LEFT JOIN giscedata_polissa AS p ON cl.polissa_id = p.id 
            JOIN 
            ( 
                 SELECT 
                       polissa, 
                       MAX(tg_cnc_id) tg_cnc_id,
                       MAX(tg_last_read) tg_last_read, 
                       MAX(name) AS name, 
                       MAX(tg_cnc_conn::int) = 1 tg_cnc_conn
                 FROM giscedata_lectures_comptador 
                 WHERE NOT in_tpl
                 GROUP BY polissa
            ) AS c ON c.polissa=p.id
            LEFT JOIN tg_concentrator AS con ON c.tg_cnc_id = con.id
            LEFT JOIN giscedata_cups_ps cups ON (p.cups = cups.id)
            LEFT JOIN giscedata_cups_escomesa AS esc ON (esc.id = cups.id_escomesa)
            LEFT JOIN giscedata_polissa_tarifa as tarifa ON (p.tarifa=tarifa.id)                      
            WHERE
                cl.lot_id = %(lote_facturacion)s AND
                cl.state = 'obert' AND
                tarifa.name NOT IN ('3.0A', '3.1A','6.1A')
            ORDER BY cups.name, cups.direccio ASC
            );
            """, params)

    def create_route(self, cursor, uid, ids, polygon, context=None):
        """
        Creates a rotue from a WKT polygon

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: unused
        :param polygon: Route polygon in WKT format
        :type polygon: str
        :param context: OpenERP context
        :type context: dict
        :return: number of cups
        :rtype: int
        """
        if context is None:
            context = {}

        wiz_model = self.pool.get("giscedata.lectures.pda.export")

        sql = """
            SELECT cups_id , data_final_lot
            FROM giscegis_rutes_comptadors
            WHERE ST_Intersects(geom, ST_GeomFromText(%s,%s))
            ORDER BY direccion_cups;
        """
        cursor.execute(sql, (polygon, config.get("srid", 25830)))
        data = cursor.fetchall()
        wiz_params = {
            "exportar_tall": True,
            "exportar_tg": True,
            "dia": data[0][1],
            "zona": "virtual_{}".format(data[0][1])
        }
        id_wiz = wiz_model.create(cursor, uid, wiz_params)
        cups_ids = [int(x[0]) for x in data]
        wiz_ret = wiz_model.generate_ruta(cursor, uid, [id_wiz], cups_ids)
        return wiz_ret

GiscegisRutesComptadors()

