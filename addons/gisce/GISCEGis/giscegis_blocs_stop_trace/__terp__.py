# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Blocs Stop Trace",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_vertex",
        "giscegis_nodes",
        "giscegis_base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscegis_blocs_stop_trace_view.xml",
        "ir.model.access.csv",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
