# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* giscegis_blocs_stop_trace
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.14\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2019-07-26 18:05\n"
"PO-Revision-Date: 2019-07-26 18:05\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: giscegis_blocs_stop_trace
#: field:giscegis.blocs.stop_trace,node:0
msgid "Node"
msgstr ""

#. module: giscegis_blocs_stop_trace
#: field:giscegis.blocs.stoptrace.blockname,description:0
msgid "Descripció"
msgstr ""

#. module: giscegis_blocs_stop_trace
#: model:ir.module.module,shortdesc:giscegis_blocs_stop_trace.module_meta_information
msgid "GISCE GIS Blocs Stop Trace"
msgstr ""

#. module: giscegis_blocs_stop_trace
#: field:giscegis.blocs.stop_trace,blockname:0
#: field:giscegis.blocs.stop_trace,name:0
#: field:giscegis.blocs.stoptrace.blockname,name:0
msgid "BlockName"
msgstr ""

#. module: giscegis_blocs_stop_trace
#: field:giscegis.blocs.stop_trace,vertex:0
msgid "Vertex"
msgstr ""

#. module: giscegis_blocs_stop_trace
#: field:giscegis.blocs.stop_trace,codi:0
msgid "Codi"
msgstr ""

#. module: giscegis_blocs_stop_trace
#: field:giscegis.blocs.stop_trace,width:0
msgid "Width"
msgstr ""

#. module: giscegis_blocs_stop_trace
#: model:ir.model,name:giscegis_blocs_stop_trace.model_giscegis_blocs_stoptrace_blockname
msgid "Tipus Blocs StopTrace"
msgstr ""

#. module: giscegis_blocs_stop_trace
#: constraint:ir.model:0
msgid "The Object name must start with x_ and not contain any special character !"
msgstr ""

#. module: giscegis_blocs_stop_trace
#: field:giscegis.blocs.stop_trace,rotation:0
msgid "Rotation"
msgstr ""

#. module: giscegis_blocs_stop_trace
#: field:giscegis.blocs.stop_trace,height:0
msgid "Height"
msgstr ""

#. module: giscegis_blocs_stop_trace
#: model:ir.model,name:giscegis_blocs_stop_trace.model_giscegis_blocs_stop_trace
msgid "Blocs Stop Trace"
msgstr ""

