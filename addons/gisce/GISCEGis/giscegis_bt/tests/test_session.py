from destral import testing
from destral.transaction import Transaction


__all__ = [
    'TestBtElementSession'
]


class TestBtElementSession(testing.OOTestCase):

    def setUp(self):
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user

    def tearDown(self):
        self.txn.stop()

    def test_search_session_lbt(self):
        """
        This test is for check the correct behaviour of the sessions and the LBT
        """
        # Load ERP models
        bt_obj = self.openerp.pool.get('giscedata.bt.element')
        session_obj = self.openerp.pool.get('giscegis.session')

        # Clean the sessions to avoid BBox collisions
        sql = """
            TRUNCATE giscegis_session CASCADE
        """
        self.cursor.execute(sql)

        # Create the sessions
        session1_vals = {
            "name": 'Testing session 1',
            "bbox": 'POLYGON((982449.3 4664776.0, 982760.4 4664776.0, '
                    '982760.4 4664950.2, 982449.3 4664950.2, '
                    '982449.3 4664776.0))',
            "state": 'open',
            "ttl": 500000000,
            "active": True
        }
        session2_vals = {
            "name": 'Testing session 2',
            "bbox": 'POLYGON((981216.7 4664152.6, 981771.6 4664152.6, '
                    '981771.6 4664463.2, 981216.7 4664463.2, '
                    '981216.7 4664152.6))',
            "state": 'open',
            "ttl": 500000000,
            "active": True
        }
        session1_id = session_obj.create(self.cursor, self.uid, session1_vals)
        session2_id = session_obj.create(self.cursor, self.uid, session2_vals)

        # Start the checks
        # Check the number of LBT in differents sessions and without sessions
        # No session
        bt_ids = bt_obj.search(self.cursor, self.uid, [])
        self.assertEqual(len(bt_ids), 38)
        # Session 1
        bt_ids = bt_obj.search(
            self.cursor, self.uid, [], context={'session': session1_id}
        )
        self.assertEqual(len(bt_ids), 0)
        # Session 2
        bt_ids = bt_obj.search(
            self.cursor, self.uid, [], context={'session': session2_id}
        )
        self.assertEqual(len(bt_ids), 38)

        # Get 2 ids of the Session 2
        lbt1_id = bt_ids[0]
        lbt2_id = bt_ids[1]

        # Check that the changes made on a session are only visible from the
        # session
        # Write new data on 2 LBT from session 2
        lbt_vals_to_write = {
            "voltatge": '420'
        }
        bt_obj.write(
            self.cursor, self.uid, [lbt1_id, lbt2_id], lbt_vals_to_write,
            context={'session': session2_id}
        )
        # Check the changes outside the session
        lbt_data = bt_obj.read(
            self.cursor, self.uid, [lbt1_id, lbt2_id], ['voltatge']
        )
        for data in lbt_data:
            self.assertEqual(data['voltatge'], '400')
        # Check the changes inside the session
        lbt_data = bt_obj.read(
            self.cursor, self.uid, [lbt1_id, lbt2_id], ['voltatge'],
            context={'session': session2_id}
        )
        for data in lbt_data:
            self.assertEqual(data['voltatge'], '420')
