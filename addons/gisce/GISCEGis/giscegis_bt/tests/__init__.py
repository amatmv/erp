# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
import ast
from .test_session import *
from .test_geom_getter import *
# from .test_topologia import *


class BtElementGeomTest(testing.OOTestCase):

    def test_catalog_elements_lbt(self):
        """
        Test that are elements for LBT and how many of each type are
        searching with the catalog domain.
        :return: None
        """

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            # INICIALITZEM OBJECTES
            elem_bt_obj = self.openerp.pool.get('giscedata.bt.element')
            cataleg_obj = self.openerp.pool.get('giscegis.cataleg.cataleg')
            sum_lbt = 0

            # SELECCIONEM UNICAMENT ELS ELEMENTS DE CATALEG QUE SIGUIN
            # LBT
            ids_lbt_cataleg = cataleg_obj.search(
                cursor, uid, [('model.model', '=', 'giscedata.bt.element')]
            )

            # PER A CADA ELEMENT DEL CATALEG COMPROVAREM QUE HI HAGIN ELEMENTS
            cataleg_lbt_elems = cataleg_obj.read(
                cursor, uid, ids_lbt_cataleg, ['name', 'domain']
            )

            for cataleg_elem in cataleg_lbt_elems:

                lbt_ids = elem_bt_obj.search(
                    cursor, uid, ast.literal_eval(cataleg_elem['domain'])
                )

                sum_lbt += len(lbt_ids)
                if cataleg_elem['name'] == "Línies Baixa Tensió":
                    self.assertEqual(len(lbt_ids), 38)

            self.assertEqual(sum_lbt, 38)
