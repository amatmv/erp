from destral import testing
from destral.transaction import Transaction
import logging


logger = logging.getLogger(__name__)


__all__ = [
    'TestTramsBtGeomGetter'
]


class TestTramsBtGeomGetter(testing.OOTestCase):

    def setUp(self):
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user

    def tearDown(self):
        self.txn.stop()

    def test_multiformat_geom_getter_trams_at(self):
        """
        Test the differents geom formats aviable from the geom getter method
        get_geom of the model GiscedataBtTram
        :return: None
        """
        trams_obj = self.openerp.pool.get('giscedata.bt.element')

        tram_id = trams_obj.search(
            self.cursor, self.uid, [('name', '=', '1')]
        )[0]

        geo_json_format = trams_obj.get_geom(
            self.cursor, self.uid, tram_id, "GeoJSON"
        )

        self.assertEqual(
            geo_json_format[str(tram_id)],
            '{"type":"LineString","coordinates":[[981670.4165,4664237.9972],'
            '[981672.5642,4664238.0524]]}'
        )

        wkt_format = trams_obj.get_geom(
            self.cursor, self.uid, tram_id, "WKT"
        )

        self.assertEqual(
            wkt_format[str(tram_id)],
            "LINESTRING(981670.4165 4664237.9972,981672.5642 4664238.0524)"
        )

        wkb_format = trams_obj.get_geom(
            self.cursor, self.uid, tram_id, "WKB"
        )

        self.assertEqual(
            wkb_format[str(tram_id)],
            "010200000002000000ee7c3fd54cf52d41f31fd27febca514189d2de2051f52d41"
            "88855a83ebca5141"
        )

    def test_multiformat_geom_getter_tams_at_wrong_formats(self):
        """
        Test if the exception throw behaviour is correct
        :return: None
        """

        trams_obj = self.openerp.pool.get('giscedata.bt.element')

        tram_id = trams_obj.search(
            self.cursor, self.uid, [('name', '=', '1')]
        )[0]

        # Bad format request
        try:
            trams_obj.get_geom(
                self.cursor, self.uid, tram_id, "rofl"
            )
            logger.error("Exception not raised on a bad format request.")
        except Exception:
            logger.info("Exception raised correctly due a bad format request.")

        # Good format request
        try:
            trams_obj.get_geom(
                self.cursor, self.uid, tram_id, "WKT"
            )
            logger.info("Exception not raised due a good format request.")
        except Exception:
            logger.error("Exception raised wrongly on a good format request.")
