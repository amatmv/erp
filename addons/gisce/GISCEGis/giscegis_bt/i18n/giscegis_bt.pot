# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* giscegis_bt
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.14\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2019-07-29 14:31\n"
"PO-Revision-Date: 2019-07-29 14:31\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: giscegis_bt
#: model:ir.module.module,description:giscegis_bt.module_meta_information
msgid "Extensió del model giscedata_bt_elements amb els camps geometrics necessaris."
msgstr ""

#. module: giscegis_bt
#: constraint:ir.ui.view:0
msgid "Invalid XML for View Architecture!"
msgstr ""

#. module: giscegis_bt
#: field:giscedata.bt.element,tensio:0
msgid "Tensió"
msgstr ""

#. module: giscegis_bt
#: view:giscedata.bt.element:0
msgid "Informació Elèctrica"
msgstr ""

#. module: giscegis_bt
#: model:ir.module.module,shortdesc:giscegis_bt.module_meta_information
msgid "GISCE GIS Elements BT"
msgstr ""

#. module: giscegis_bt
#: code:addons/giscegis_bt/giscegis_bt.py:64
#, python-format
msgid "Required format not valid. Valid formats are: GeoJSON, WTK or WKB"
msgstr ""

#. module: giscegis_bt
#: field:giscedata.bt.element,geom:0
#: field:giscedata.bt.suport,geom:0
msgid "Geometria"
msgstr ""

#. module: giscegis_bt
#: field:giscedata.bt.element,edge_id:0
msgid "Aresta"
msgstr ""

#. module: giscegis_bt
#: field:giscedata.bt.element,linia:0
msgid "Línia"
msgstr ""

#. module: giscegis_bt
#: field:giscedata.bt.suport,code:0
msgid "Codi AutoCAD"
msgstr ""

#. module: giscegis_bt
#: field:giscedata.bt.suport,rotation:0
msgid "Rotation"
msgstr ""

#. module: giscegis_bt
#: field:giscedata.bt.element,trafo_id:0
msgid "Trafo"
msgstr ""

#. module: giscegis_bt
#: field:giscedata.bt.element,ct_id:0
msgid "CT"
msgstr ""

