# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Elements BT",
    "description": """Extensió del model giscedata_bt_elements amb els camps geometrics necessaris.""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscedata_bt",
        "giscegis_base",
        "giscegis_base_geom",
        "giscegis_lbt_traceability",
        "giscegis_cataleg",
        "giscegis_edge",
        "giscegis_node_bloc_model",
        "giscegis_colors"
    ],
    "init_xml": [],
    "demo_xml": [
        "giscegis_bt_demo.xml"
    ],
    "update_xml":[
        "giscegis_cataleg_bt_data.xml",
        "giscegis_bt_view.xml"
    ],
    "active": False,
    "installable": True
}
