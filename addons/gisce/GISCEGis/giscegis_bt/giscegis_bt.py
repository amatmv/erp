# -*- coding: utf-8 -*-
from giscegis_base_geom.fields import Line, Point
from giscegis_base_geom.giscegis_session import SessionManaged
from tools import config
from osv import fields
from tools.translate import _
from psycopg2.extensions import AsIs


class GiscedataBtElement(SessionManaged):

    _name = 'giscedata.bt.element'
    _inherit = 'giscedata.bt.element'

    def get_polyline(self, cursor, uid, ids, context=None):
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        result = []
        btlike = self.pool.get('giscegis.base.geom').get_btlike_layer(
            cursor, uid
        )

        sql = '''
          SELECT * FROM giscegis_edge e
          WHERE id_linktemplate in
          (SELECT name FROM giscedata_bt_element WHERE id IN %(ids)s)
          AND e.layer LIKE %(btlike)s
        '''

        cursor.execute(sql, {'ids': tuple(ids), 'btlike': btlike})
        data = cursor.fetchall()
        for identifier in data:
            result.append(identifier[0])
        return result

    def get_geom(self, cursor, uid, ids, geom_format, srid=None, context=None):
        """
        Return the geometry of the given ids on the specified format
        :param cursor: Database cursor
        :type cursor: Psycopg2 cursor
        :param uid: User ID
        :type uid: int
        :param ids: Requested ids to compute the value
        :type ids: (list of int) or (int,) or int
        :param geom_format: Requested format to return the data. Aviable formats
        are: GeoJson, WKT and WKB
        :type geom_format: str
        :param srid: Return srid
        :type srid: int
        :param context: OpenERP context
        :type context: dict[str, Any]
        :return: The computed value in the requested format for each id on ids
        :rtype: dict[int, str or bool]
        """

        if context is None:
            context = {}

        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        # Ensure that the requested format is available
        geom_format = geom_format.lower()
        error_msg = _("Required format not valid. Valid formats are: "
                      "GeoJSON, WTK or WKB")
        assert geom_format in ("geojson", "wkb", "wkt"), error_msg

        # Initialize the return of the function
        ret = {str(object_id): False for object_id in ids}

        # Query declaration
        sql = '''
           SELECT id AS id, %(geom_op)s AS geo_field
           FROM giscedata_bt_element
           WHERE id IN %(ids)s
       '''

        # Initialize the geom formatter of the requested format
        if srid:
            if geom_format == "geojson":
                geom_op = 'ST_AsGeoJSON(ST_TRANSFORM(geom,{}))'.format(int(srid))
            elif geom_format == "wkb":
                geom_op = 'ST_AsBinary(ST_TRANSFORM(geom,{}))'.format(int(srid))
            else:
                geom_op = 'ST_AsText(ST_TRANSFORM(geom,{}))'.format(int(srid))
        else:
            if geom_format == "geojson":
                geom_op = 'ST_AsGeoJSON(geom)'
            elif geom_format == "wkb":
                geom_op = 'ST_AsBinary(geom)'
            else:
                geom_op = 'ST_AsText(geom)'

        # Execute the query and compute the result of each requested element
        cursor.execute(sql, {'ids': tuple(ids), 'geom_op': AsIs(geom_op)})
        query_res = cursor.dictfetchall()
        if geom_format == "wkb":
            for res in query_res:
                ret[str(res['id'])] = bytes(res['geo_field']).encode('hex')
        else:
            for res in query_res:
                ret[str(res['id'])] = res['geo_field']

        return ret

    def create(self, cr, uid, vals, context=None):
        """
        Overwrites the create to avoid duplicated names on Qgisce part line

        :param cr: Database cursor
        :param uid: User ids
        :type uid: int
        :param vals: Ct vals
        :type vals: dict
        :param context: OpenERP context
        :type context: dict
        :return: list of created ids
        :rtype: list(int)
        """

        existing_name = self.search(
            cr, uid, [("name", "=", vals.get("name", False))]
        )

        if existing_name or ('name' not in vals):
            vals['name'] = self._get_nextnumber(cr, uid, context)

        return super(GiscedataBtElement, self).create(
            cr, uid, vals, context=context)

    def write(self, cursor, uid, ids, vals, context=None):
        """
        Overwrites the write to avoid overwritte names on Qgisce part line
        :param cursor: Database cursor
        :param uid: User ids
        :type uid: int
        :param ids:  IDS to write the vals
        :type ids: list of int
        :param vals: LBT vals
        :type vals: dict
        :param context: OpenERP context
        :type context: dict
        :return: True or False if it have been possible to write to the IDS
        :rtype: Boolean
        """

        if 'name' in vals:
            del vals['name']

        return super(GiscedataBtElement, self).write(
            cursor, uid, ids, vals, context=context
        )

    def _ff_longitud_cad(self, cursor, uid, ids, field_name, ar, context):
        """
        Fields function to calculate the longitud_cad

        :param cursor: Database cursor
        :param uid: User id
        :param ids: AT tram ids
        :param field_name: Field name
        :param ar: Arguments
        :param context: OpenERP context
        :return:
        :rtype: dict
        """

        ret = dict.fromkeys(ids)
        sql = """
        SELECT id,st_length(geom)
        FROM giscedata_bt_element
        WHERE id in %(ids)s
        """
        cursor.execute(sql, {"ids": tuple(ids)})
        data = cursor.fetchall()
        for line in data:
            ret[line[0]] = line[1]
        return ret

    def _longitud_cad_to_update(self, cursor, uid, ids, context=None):
        """
        Returns the list of cad to update

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: List of ids to calc
        :type ids: list(int)
        :param context: OpenERP context
        :return: List of elements to store
        :rtype: list(int)
        """

        return ids

    def _get_traceability_values(self, cr, uid, ids, field_name, arg, context):
        """
        Return the values of the fields trafo_id, ct_id, linia and tensio
        for the model GiscedataBtElement
        :param cr: Database Cursor
        :type cr: Cursor
        :param uid: User ID
        :type uid: int
        :param ids: IDS of the elements that the fields must be calculated
        :type ids: list of int
        :param field_name: Not Used
        :type field_name: Not Used
        :param arg: Not Used
        :type arg: Not Used
        :param context: Not Used
        :type context: Not Used
        :return: The computed field values for each ID in IDS.
        :rtype dict[int,dict[str,int or str or bool]]
        """

        res = {}
        for lbt_id in ids:
            res[lbt_id] = {
                'ct_id': False,
                'trafo_id': False,
                'linia': False,
                'tensio': False,
                'color': False
            }

        sql = """
            SELECT 
                t.ct, 
                t.element, 
                t.sortida AS linia, 
                t.tensio, t.trafo, 
                color.color AS color
            FROM giscedata_bt_element bt_linia
            LEFT JOIN giscegis_lbt_traceability t 
                ON t.element = bt_linia.id
            LEFT JOIN giscegis_colors_bt color
                ON bt_linia.num_linia = color.name::text
            WHERE t.element IN %(ids)s;
        """

        cr.execute(sql, {'ids': tuple(ids)})

        for row in cr.dictfetchall():
            res[row['element']] = {
                'ct_id': int(row['ct']) if row['ct'] else False,
                'trafo_id': int(row['trafo']) if row['trafo'] else False,
                'linia': str(row['linia']) if row['linia'] else False,
                'tensio': str(row['tensio']) if row['tensio'] else False,
                'color': str(row['color']) if row['color'] else '#000000'
            }

        return res

    def _fn_trafo_id_search(self, cur, uid, obj, name, args, context={}):
        """
        Function to search on the trafo_id field

        :param cr: Database cursor
        :param uid: User id
        :type uid: int
        :type obj:
        :param obj:
        :type name:
        :param name:
        :type args:
        :param args: list(str)
        :param context:
        :type context: dict
        :rtype: list(int)
        """

        if args is None:
            return []

        out_args = []

        for arg in args:
            if arg[0] == "trafo_id":
                trafo_name = arg[2]
            else:
                out_args.append(arg)
        sql_search = """
        SELECT trac.element
        FROM giscegis_lbt_traceability as trac
        LEFT JOIN giscedata_transformador_trafo as trafo
            ON trac.trafo=trafo.id
        WHERE trafo.name like '%%%(trafo_name)s%%'
        """
        cur.execute(sql_search, {"trafo_name": AsIs(trafo_name)})
        ids = [element[0] for element in cur.fetchall()]
        out_args.append(("id", "in", ids))
        return out_args

    def _fn_tensio_search(self, cur, uid, obj, name, args, context={}):
        """
        Function to search on the tensio field

        :param cr: Database cursor
        :param uid: User id
        :type uid: int
        :type obj:
        :param obj:
        :type name:
        :param name:
        :param args:
        :type args: list(tuple)
        :param context:
        :type context: dict
        :rtype: list(int)
        """

        if args is None:
            return []

        out_args = []

        for arg in args:
            if arg[0] == "tensio":
                tensio = arg[2]
            else:
                out_args.append(arg)
        sql_search = """
        SELECT trac.element
        FROM giscegis_lbt_traceability as trac
        WHERE trac.tensio like '%%%(tensio)s%%'
        """
        cur.execute(sql_search, {"tensio": AsIs(tensio)})
        ids = [element[0] for element in cur.fetchall()]
        out_args.append(("id", "in", ids))
        return out_args

    def _fn_ct_id_search(self, cur, uid, obj, name, args, context={}):
        """
        Function to search on the ct_id field

        :param cr: Database cursor
        :param uid: User id
        :type uid: int
        :type obj:
        :param obj:
        :type name:
        :param name:
        :param args:
        :type args: list(tuple)
        :param context:
        :type context: dict
        :rtype: list(int)
        """

        if args is None:
            return []

        out_args = []

        for arg in args:
            if arg[0] == "ct_id":
                ct_id = arg[2]
            else:
                out_args.append(arg)
        sql_search = """
        SELECT trac.element
        FROM giscegis_lbt_traceability AS trac
        LEFT JOIN giscedata_cts AS ct ON (ct.id=trac.ct)
        WHERE trac.tensio like '%%%(ct)s%%'
        """
        cur.execute(sql_search, {"ct_id": AsIs(ct_id)})
        ids = [element[0] for element in cur.fetchall()]
        out_args.append(("id", "in", ids))
        return out_args

    def _fn_linia_search(self, cur, uid, obj, name, args, context={}):
        """
        Function to search on the linia field

        :param cr: Database cursor
        :param uid: User id
        :type uid: int
        :type obj:
        :param obj:
        :type name:
        :param name:
        :param args:
        :type args: list(tuple)
        :param context:
        :type context: dict
        :rtype: list(int)
        """

        if args is None:
            return []

        out_args = []

        for arg in args:
            if arg[0] == "linia":
                sortida = arg[2]
            else:
                out_args.append(arg)

        sql_search = """
        SELECT trac.element
        FROM giscegis_lbt_traceability AS trac
        WHERE trac.sortida like '%%%(sortida)s%%'
        """
        cur.execute(sql_search, {"sortida": AsIs(sortida)})
        ids = [element[0] for element in cur.fetchall()]
        out_args.append(("id", "in", ids))
        return out_args

    _columns = {
        "geom": Line("Geometria", srid=config.get("srid", 25830), select=True),
        "edge_id": fields.many2one("giscegis.edge", "Aresta",
                                   ondelete='set null'),
        "longitud_cad": fields.function(
            _ff_longitud_cad, type="float", string="Longitud Autocad",
            readonly=True, method=True,
            store={
                'giscedata.bt.element': (
                    _longitud_cad_to_update, ['geom'], 10)
            }
        ),
        'ct_id': fields.function(
            _get_traceability_values, method=True, string='CT', readonly=True,
            type='many2one', relation='giscedata.cts',
            multi='_get_traceability_values',
            fnct_search=_fn_ct_id_search
        ),
        'trafo_id': fields.function(
            _get_traceability_values, method=True, string='Trafo',
            type='many2one', readonly=True,
            relation='giscedata.transformador.trafo',
            multi='_get_traceability_values',
            fnct_search=_fn_trafo_id_search
        ),
        'linia': fields.function(
            _get_traceability_values, method=True, string='Línia',
            readonly=True, type='char', size='64',
            multi='_get_traceability_values',
            fnct_search=_fn_linia_search
        ),
        'tensio': fields.function(
            _get_traceability_values, method=True, string='Tensió',
            readonly=True, type='char', size='64',
            multi='_get_traceability_values',
            fnct_search=_fn_tensio_search
        ),
        'color': fields.function(
            _get_traceability_values, method=True, string='Color',
            readonly=True, type='char', size='64',
            multi='_get_traceability_values'
        )
    }


GiscedataBtElement()


class GiscedataBtSuport(SessionManaged):

    _name = 'giscedata.bt.suport'
    _inherit = 'giscedata.bt.suport'

    _columns = {
        "geom": Point("Geometria", srid=config.get("srid", 25830), select=True),
        "rotation": fields.integer("Rotation"),
        "code": fields.char("Codi AutoCAD", size=64)
    }

    _defaults = {
        "rotation": lambda *a: 0,
    }


GiscedataBtSuport()
