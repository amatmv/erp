# -*- coding: utf-8 -*-

from osv import fields, osv
import netsvc
"""
:mod:`giscegis_configdefault_bt` - Emmagatzemant l'estat de la xarxa BT
==========================================================================

El mòdul :mod:`giscegis_configdefault_bt` fa la funció de guardar l'estat de
la xarxa de baixa tensió.
"""


class GiscegisConfigdefaultBt(osv.osv):
    _name = "giscegis.configdefault.bt"
    _description = "Configuració interruptors oberts BT"
    _columns = {
        'name': fields.char('Nom', size=50, required=True),
        'node': fields.many2one('giscegis.nodes', 'Node', ondelete='cascade'),
        'codi': fields.char('Codi', size=50, required=True),
        'blockname_str': fields.char('Blockname', size=50, required=True),
    }

    _defaults = {
        'name': lambda *a: 'DEFAULT',
    }

    _sql_constraints = [('codi_uniq', 'unique(codi)',
                         'Ja existeix aquest bloc a la configuració de xarxa')]

    _order = "codi, blockname_str, name"

    def save_bt(self, cr, uid, config="DEFAULT", oberts_ids=[], tancats_ids=[], context={}):
        """
        Mètode per guardar l'estat de la xarxa de BT

        :param config: el nom de la configuració de xarxa a on aplica, per
        poder suportar diferents estats de xarxa (ara no s'utilitza encara)
        :param oberts_ids: llista amb els identificadors de nodes oberts
        (inclosos els 'a obrir')
        :param tancats_ids: llista amb els identificadors de nodes tancats
        (inclosos els 'a tancar')
        :returns: un diccionari amb la clau 'success' omplerta amb True o
        False segons si s'ha guardat bé o no, respectivament.
        :rtype: dict
        """
        logger = netsvc.Logger()
        """
          Algorisme:
            per cada O in oberts_ids:
              si O not in giscegis.configdefault.bt:
                afegir O a giscegis.configdefault.bt
              si no:
                pass
            per cada T in tancats_ids:
              si T in giscegis.configdefault.bt:
                esborrar T de giscegis.configdefault.bt
              si no:
                pass
        """
        try:
            cdb_obj = self.pool.get('giscegis.configdefault.bt')
            res = cdb_obj.search(cr, uid, [('name', '=', config)])
            nodes = cdb_obj.read(cr, uid, res, ['node'])
            a_taula = [a['node'][0] for a in nodes if a['node']]
            for O in oberts_ids:
                if O not in a_taula:
                    node = self.pool.get('giscegis.nodes').browse(cr, uid, O)
                    blockname = node.get_bloc().blockname.name
                    element = {
                        'name': config,
                        'node': O,
                        'codi': node.get_codi(),
                        'blockname_str': blockname
                    }
                    cdb_obj.create(cr, uid, element)

            for T in tancats_ids:
                if T in a_taula:
                    # s'esborra per id
                    ident = cdb_obj.search(cr, uid, [('node', '=', T)])
                    cdb_obj.unlink(cr, uid, ident)
            return {'success': True}
        except Exception as e:
            logger.notifyChannel('seve_bt', netsvc.LOG_DEBUG, e)
            sentry = self.pool.get('sentry.setup')
            if sentry is not None:
                sentry.client.captureException()
            return {'success': False}


GiscegisConfigdefaultBt()


class GiscegisOpenNodesBt(osv.osv):
    _name = 'giscegis.open.nodes.bt'
    _description = "Interruptors oberts de BT"
    _auto = False
    _columns = {
        'name': fields.char('Nom', size=50, readonly=True),
        'x': fields.float('X', readonly=True),
        'y': fields.float('Y', readonly=True),
    }

    def init(self, cr):
        """
        Creates a materialized view with the open nodes BT
        :param cr: Databas cursor
        :return: None
        """
        cr.execute("""DROP VIEW IF EXISTS giscegis_open_nodes_bt""")
        cr.execute("""
          CREATE OR REPLACE VIEW
            giscegis_open_nodes_bt AS (
            SELECT
              n.name,
              v.x,
              v.y
            FROM giscegis_configdefault_bt cd
            LEFT JOIN giscegis_nodes n on (n.id=cd.node)
            LEFT JOIN giscegis_vertex v on (n.vertex=v.id)
          )
        """)


GiscegisOpenNodesBt()
