# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Blocs Fusibles Caixes 171",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_nodes",
        "giscegis_vertex",
        "giscegis_blocs_fusibles_caixes",
        "giscegis_base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "ir.model.access.csv",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
