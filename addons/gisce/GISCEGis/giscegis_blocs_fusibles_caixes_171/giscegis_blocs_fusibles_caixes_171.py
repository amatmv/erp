# -*- coding: iso-8859-1 -*-
from osv import osv,fields

class giscegis_blocs_fusibles_caixes_171(osv.osv):

    _name = 'giscegis.blocs.fusibles.caixes.171'
    _description = 'Blocs Fusibles Caixes per MG'
    _columns = {
      'id' : fields.integer('ID', readonly=True),
      'x' : fields.float('x', readonly=True),
      'y': fields.float('y', readonly=True),
      'rotation' : fields.float('rotation', readonly=True),
      'blockname' : fields.char('blockname', size=20, readonly=True),
      'texte' : fields.char('texte', size=50, readonly=True),
    }
    _auto = False

    _order = "id,blockname"

    def init(self, cr):
        cr.execute("""drop view if exists giscegis_blocs_fusibles_caixes_171""")
        cr.execute("""
          create or replace view giscegis_blocs_fusibles_caixes_171 as (
    select b.id, v.x, v.y, b.rotation, bn.name as blockname, bn.name || ' ' || b.codi as texte from giscegis_blocs_fusibles_caixes b left join giscegis_blocs_fusiblescaixes_blockname bn on (b.blockname=bn.id) left join giscegis_vertex v on (b.vertex = v.id)
          )
        """)
giscegis_blocs_fusibles_caixes_171()
