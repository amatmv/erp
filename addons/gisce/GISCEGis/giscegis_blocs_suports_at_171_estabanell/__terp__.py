# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Blocs Suports 1.7.1 Estabanell",
    "description": """Vista dels suports at pel MG-1.7.1 per Estabanell""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_blocs_suports_at",
        "giscegis_base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
