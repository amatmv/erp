# -*- coding: utf-8 -*-
import base64
from StringIO import StringIO
from zipfile import ZipFile, BadZipfile
import os
import itertools

from tools.translate import _
from osv import osv, fields
import shapefile
import netsvc

from tools.misc import email_send

class GiscegisFibraOpticaLoader(osv.osv_memory):

    _name = 'giscegis.fibraoptica.loader'

    def _get_default_url(self, cursor, uid, context=None):
        """
        Return the URL of the web loader form

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param context: OpenERP context
        :type context: dict
        :return: URL
        :rtype: str
        """

        url = self.pool.get("res.config").get(cursor, uid,"giscegis_gis_url")
        return "{}/load/fibra".format(url)

    def load_shp(self, cursor, uid, shp_data, dbf_data):
        base_geom = self.pool.get('giscegis.base.geom')

        sf = shapefile.Reader(shp=shp_data, dbf=dbf_data)
        shapes = sf.shapes()
        records = sf.records()
        if not records:
            records = [{}] * len(shapes)
        conf_obj = self.pool.get('res.config')
        insert_data = []
        headers = {}
        for index, field in enumerate(sf.fields[1:]):
            headers[field[0].lower()] = index

        srid = int(conf_obj.get(cursor, uid, 'giscegis_srid', 25830))
        insert_sql = '''
                INSERT INTO giscegis_fibraoptica_geom_tmp
                (id,color,style,textstring,text_size,text_angle,geom)
                VALUES (%(id)s,%(color)s,%(style)s,%(textstring)s,
                %(text_size)s,%(text_angle)s,
                ST_GeomFromText(%(geom)s,%(srid)s))
                '''
        sql_data = '''
              SELECT MAX(id)+1 AS last_index
              FROM giscegis_fibraoptica_geom_tmp;'''
        cursor.execute(sql_data)
        data = cursor.fetchone()[0]
        if data:
            last_index = data
        else:
            last_index = 0
        max_text_angle = 0
        base_model = self.pool.get('giscegis.base.geom')

        for index, shape, record in itertools.izip(itertools.count(), shapes, records):
            points = [shape.points]
            if len(shape.points) > 1:
                wkt_geometry = base_model.to_wkt_multilinestring(points)
            elif len(shapes[index].points) == 1:
                wkt_geometry = base_model.to_wkt_point(points)
            else:
                wkt_geometry = None

            row = {'geom': wkt_geometry,
                   'srid': srid,
                   'gid': None,
                   'color': None,
                   'style': None,
                   'textstring': None,
                   'text_size': None,
                   'text_angle': None}

            if headers:
                if 'text_angle' in headers:
                    text_angle = record[headers['text_angle']]
                    try:
                        row['text_angle'] = str(float(text_angle))
                        if float(text_angle) > max_text_angle:
                            max_text_angle = text_angle
                    except ValueError:
                        row['text_angle'] = text_angle

                if 'height' in headers:
                    height = record[headers['height']]
                    row['text_size'] = base_geom.sanitize_record(height, 'float')
                if 'textstring' in headers:
                    text_string = record[headers['textstring']]
                    row['textstring'] = text_string.decode('latin1')
                if 'style' in headers:
                    row['style'] = record[headers['style']]
                if 'color' in headers:
                    color = record[headers['color']]
                    row['color'] = base_geom.sanitize_record(color, 'int')
            if 'id' in headers:
                row['id'] = records[index][headers['id']]
            else:
                row['id'] = index + last_index
            insert_data.append(row)
        cursor.executemany(insert_sql, insert_data)

        if max_text_angle < 10.0:
            sql_update = """
              UPDATE giscegis_fibraoptica_geom_tmp
              SET text_angle=degrees(text_angle);
            """
            cursor.execute(sql_update)

    def import_data_shp(self, cursor, uid, ids, context=None):
        """
        Attends the load of the shapes from the wizard

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: Wizard id
        :type ids: int
        :param context: Open ERP context
        :type context: dict
        :return: None
        :rtype: None
        """

        obj_users = self.pool.get("res.users")
        obj_msg = self.pool.get("giscegis.load.message")
        obj_config = self.pool.get("res.config")

        from_email = obj_config.get(
            cursor, uid,
            "email_from", "no-reply@gisce.net"
        )

        message_id = obj_msg.create(cursor, uid, {})
        message = obj_msg.browse(cursor, uid, message_id)
        message.start_load()

        user_data = obj_users.browse(cursor, uid, uid)
        user_email = user_data.address_id.email

        email_send(
            from_email,
            [user_email],
            _("Carrega de fitxers fibra optica"),
            message.get_message(),
            debug=True
        )

        base_geom = self.pool.get('giscegis.base.geom')
        not_found_message = _(u"No s'han trobat fitxers .shp o .dbf en el fitxer,s'atura la importació")
        wizard = self.browse(cursor, uid, ids[0], context)
        wizard.write({'status': ''})
        logger = netsvc.Logger()

        if wizard.file_fibra:
            data = base64.decodestring(wizard.file_fibra)
        else:
            logger.notifyChannel(
                'SHPLoader', netsvc.LOG_INFO,
                _(u"No s'ha especificat el fitxer de fibra"))
            wizard = self.browse(cursor, uid, ids[0], None)
            base_geom.add_status(
                cursor, uid, ids,
                _(u"No s'ha especificat el fitxer de fibra"),
                wizard)
        if wizard.file_fibra:
            s = StringIO(data)
            try:
                input_zip = ZipFile(s)
            except BadZipfile:
                logger.notifyChannel(
                    'SHPLoader', netsvc.LOG_WARNING,
                    _('Fitxer de fibra optica corrupte'))

                message.log_message(_('Fitxer de fibra corrupte'))
                message.end_load(_("Error"))

                wizard = self.browse(cursor, uid, ids[0], None)
                base_geom.add_status(
                    cursor, uid, ids, _('Fitxer fibra optica corrupte'),
                    wizard)
                return
            filenames = sorted(input_zip.namelist())
            if filenames:
                sql_create = """
                CREATE TABLE giscegis_fibraoptica_geom_tmp (
                  id integer,
                  create_uid integer,
                  create_date timestamp without time zone,
                  write_date  timestamp without time zone,
                  write_uid integer,
                  style text,
                  text_angle double precision,
                  color integer,
                  gid integer,
                  text_size double precision,
                  textstring text,
                  geom geometry(Geometry,%s)
                );
                """
                srid = base_geom.get_srid(cursor, uid)
                cursor.execute(sql_create, (srid,))
                shp_data = None
                dbf_data = None
                for filename in filenames:
                    if os.path.splitext(filename)[1].lower() == '.dbf':
                        dbf_data = StringIO(input_zip.read(filename))
                    if os.path.splitext(filename)[1].lower() == '.shp':
                        shp_data = StringIO(input_zip.read(filename))
                    if shp_data and dbf_data:
                        self.load_shp(cursor, uid, shp_data, dbf_data)
                        shp_data = None
                        dbf_data = None

            else:
                logger.notifyChannel(
                    'SHPLoader', netsvc.LOG_WARNING, not_found_message)
                wizard = self.browse(cursor, uid, ids[0], None)
                base_geom.add_status(
                    cursor, uid, ids, not_found_message,
                    wizard)
            logger.notifyChannel(
                'SHPLoader', netsvc.LOG_INFO,
                _('Dades carregades correctament'))
            base_geom.switch_table(
                cursor, uid, "giscegis_fibraoptica_geom_tmp", "giscegis_fibraoptica_geom"
            )

            message.log_message(_('Dades carregades correctament'))

            wizard = self.browse(cursor, uid, ids[0], None)
            base_geom.add_status(
                cursor, uid, ids,
                _('Dades carregades correctament'),
                wizard)
            status_cache = base_geom.remove_tms_cache(cursor, uid, 'fibra')
            if status_cache:
                logger.notifyChannel(
                    'SHPLoader', netsvc.LOG_INFO,
                    _('Cache netejat correctament'))

                message.log_message(_('Cache netejat correctament'))
                message.end_load(_("Correcte"))

                wizard = self.browse(cursor, uid, ids[0], None)
                base_geom.add_status(
                    cursor, uid, ids, _('Cache netejat correctament'),
                    wizard)
            else:
                logger.notifyChannel(
                    'SHPLoader', netsvc.LOG_ERROR,
                    _('Error al eliminar el cache'))

                message.log_message(_('Error al eliminar el cache'))
                message.end_load(_("Error"))

                wizard = self.browse(cursor, uid, ids[0], None)
                base_geom.add_status(
                    cursor, uid, ids, _('Error al eliminar el cache'),
                    wizard)

            email_send(
                from_email,
                [user_email],
                _("Carrega de fitxers fibra optica"),
                message.get_message(),
                debug=True
            )

    _columns = {
        'file_fibra': fields.binary('Fixer de Fibra optica', help=()),
        'status': fields.text(_('Resultat')),
        "url": fields.text(_("URL"))
    }
    _defaults = {
        'state': lambda *a: 'init',
        "url": _get_default_url
    }


GiscegisFibraOpticaLoader()
