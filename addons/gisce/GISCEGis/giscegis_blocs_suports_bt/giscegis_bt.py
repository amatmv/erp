from osv import osv
from tools import config
import logging
from giscegis_base_geom.wizard.giscegis_shp_loader \
    import GIS_MODELS_TO_UPDATE_GEOM_DATA


# Subscribe this model to the list to be updated if there is an AutoCAD dump
GIS_MODELS_TO_UPDATE_GEOM_DATA.append(
    {
        'model': 'giscedata.bt.suport',
        'functions': ['sync_elements_from_blocs']
    },
)


class GiscedataBtSuport(osv.osv):

    _name = 'giscedata.bt.suport'
    _inherit = 'giscedata.bt.suport'

    def eval_tipus(self, autocad_tipus):
        """
        Evaluate the tipus of BlocSuportBt and extract the new BtSuportTipus
        equivalent. It also return the related attributes that should have the
        BtSuport.
        :param autocad_tipus:The BlocSuportBt tipus defined on the AutoCAD
        :param autocat_tipus: str
        :return: Return a tuple composed by the code of the new tipus and the
        values of the related attributes.
        :rtype: tuple(str,dict[str,Any])
        """
        direct_eval = [
            'TUB', 'PFO', 'SM', 'SM1', 'PFU', 'PM', 'SM'
        ]
        if autocad_tipus in direct_eval:
            return (autocad_tipus, False)
        elif 'PM' in autocad_tipus:
            # ToDo: Aqui hauriem de conteplar les casuistiques dels armats i les mides a dibuixar
            return ('PM', False)
        else:
            return (False, False)

    def sync_elements_from_blocs(self, cursor, uid):
        """
        Syncronize the BtSuport elements with the BlocsSuportsBt elements
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return: Return the number of elements syncronized.
        :rtype: int
        """

        logger = logging.getLogger(
            'openerp.giscedata.bt.suport.sync_elements_from_blocs'
        )
        sup_bt_obj = self.pool.get('giscedata.bt.suport')
        sup_bt_tipus_obj = self.pool.get('giscedata.bt.suport.tipus')

        sql = """
            SELECT b.rotation, b.numsuport AS name, bn.name AS tipus,
                st_astext(st_setsrid(st_makepoint(v.x, v.y), %(srid)s)) AS geom
            FROM giscegis_blocs_suports_bt b
            LEFT JOIN giscegis_vertex v ON b.vertex = v.id
            LEFT JOIN giscegis_blocs_suports_bt_blockname bn 
                ON b.blockname = bn.id
        """

        cursor.execute(sql, {"srid": config.get("srid", 25830)})

        rows = cursor.dictfetchall()
        row_n = len(rows)
        attribute_blocknames = [
            'TIRANT', 'TP'
        ]

        # Format (COD_ATTR, GEOM)
        atributes_to_apply = []

        for row in rows:
            if row['tipus'] in attribute_blocknames:
                atributes_to_apply.append((row['tipus'], row['geom']))
                continue

            vals = {
                'rotation': row['rotation'],
                'code': row['name'],
                'geom': row['geom']
            }

            translated_tipus, derivated_attr = self.eval_tipus(row['tipus'])
            if not translated_tipus:
                logger.warning(
                    "Impossible to translate the type of GiscedataBtSuport "
                    "'{}'. Skiping the creation of the element with "
                    "AutoCAD code {}".format(
                        row['tipus'], row['name']
                    )
                )
                row_n -= 1
                continue

            sup_bt_tipus_id = sup_bt_tipus_obj.search(
                cursor, uid, [('code', '=', translated_tipus)]
            )

            if sup_bt_tipus_id:
                vals['tipus'] = sup_bt_tipus_id[0]
            else:
                logger.warning(
                    "Type of GiscedataBtSuport '{}' not found. Skiping the "
                    "creation of the element with AutoCAD code {}".format(
                        translated_tipus, row['name']
                    )
                )
                row_n -= 1
                continue

            if derivated_attr:
                for key, value in derivated_attr.iteritems():
                    vals[key] = value

            sup_bt_ids = sup_bt_obj.search(
                cursor, uid, [('code', '=', row['name'])]
            )

            if sup_bt_ids:
                sup_bt_obj.write(cursor, uid, sup_bt_ids[0], vals)
            else:
                sup_bt_obj.create(cursor, uid, vals)

        vals = {}
        for attr in atributes_to_apply:
            if attr[0] == 'TIRANT':
                sup_bt_id = sup_bt_obj.search(
                    cursor, uid, [
                        ('geom', '&&', attr[1]), ('geom', '&&!', attr[1])
                    ]
                )
                if sup_bt_id:
                    if sup_bt_id[0] in vals:
                        vals[sup_bt_id[0]]['tirant'] = True
                    else:
                        vals[sup_bt_id[0]] = {'tirant': True}
            elif attr[0] == 'TP':
                sup_bt_id = sup_bt_obj.search(
                    cursor, uid, [
                        ('geom', '&&', attr[1]), ('geom', '&&!', attr[1])
                    ]
                )
                if sup_bt_id:
                    if sup_bt_id[0] in vals:
                        vals[sup_bt_id[0]]['tornapuntes'] = True
                    else:
                        vals[sup_bt_id[0]] = {'tornapuntes': True}
        if vals:
            for sup_bt_id, values in vals.iteritems():
                sup_bt_obj.write(cursor, uid, sup_bt_id, values)

        return row_n

    def fill_attributes(self, cursor, uid):
        """
        Fill the attributes of the Suports BT
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return: None
        :rtype: NoneType
        """
        sup_bt_obj = self.pool.get('giscedata.bt.suport')

        sql = """
            SELECT bn.name AS tipus,
                ST_AsText(
                    St_SnapToGrid(
                        ST_Translate(
                            st_setsrid(
                                st_makepoint(v.x, v.y), %(srid)s),
                                0.0000000001, 0.0000000001
                        ), 0.0001
                    )
                ) AS geom
            FROM giscegis_blocs_suports_at b
            LEFT JOIN giscegis_vertex v ON b.vertex = v.id
            LEFT JOIN giscegis_blocs_suports_at_blockname bn
                ON b.blockname = bn.id
            WHERE bn.name IN ('TP', 'TIRANT')
        """

        cursor.execute(sql, {"srid": config.get("srid", 25830)})

        rows = cursor.dictfetchall()

        sql = """
            SELECT bn.name AS tipus,
                ST_AsText(
                    St_SnapToGrid(
                        ST_Translate(
                            st_setsrid(
                                st_makepoint(v.x, v.y), %(srid)s),
                                0.0000000001, 0.0000000001
                        ), 0.0001
                    )
                ) AS geom
            FROM giscegis_blocs_suports_bt b
            LEFT JOIN giscegis_vertex v ON b.vertex = v.id
            LEFT JOIN giscegis_blocs_suports_bt_blockname bn
                ON b.blockname = bn.id
            WHERE bn.name IN ('TP', 'TIRANT')
        """

        cursor.execute(sql, {"srid": config.get("srid", 25830)})

        rows += cursor.dictfetchall()

        vals = {}
        for attr in rows:
            if attr['tipus'] == 'TIRANT':
                sup_bt_id = sup_bt_obj.search(
                    cursor, uid, [
                        ('geom', '&&', attr['geom']),
                        ('geom', '&&!', attr['geom'])
                    ]
                )
                if sup_bt_id:
                    if sup_bt_id[0] in vals:
                        vals[sup_bt_id[0]]['tirant'] = True
                    else:
                        vals[sup_bt_id[0]] = {'tirant': True}
            elif attr['tipus'] == 'TP':
                sup_bt_id = sup_bt_obj.search(
                    cursor, uid, [
                        ('geom', '&&', attr['geom']),
                        ('geom', '&&!', attr['geom'])
                    ]
                )
                if sup_bt_id:
                    if sup_bt_id[0] in vals:
                        vals[sup_bt_id[0]]['tornapuntes'] = True
                    else:
                        vals[sup_bt_id[0]] = {'tornapuntes': True}
        if vals:
            for sup_bt_id, values in vals.iteritems():
                sup_bt_obj.write(cursor, uid, [sup_bt_id], values)


GiscedataBtSuport()
