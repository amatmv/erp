# coding=utf-8

import logging
import pooler


def up(cursor, installed_version):

    logger = logging.getLogger('openerp.migration')

    logger.info(
        'Sync BlocsSuportsBt with BtSuports'
    )

    pool = pooler.get_pool(cursor.dbname)
    sup_bt_obj = pool.get('giscedata.bt.suport')
    n_created_elements = sup_bt_obj.sync_elements_from_blocs(cursor, 1)

    if n_created_elements == 1:
        msg = '1 Suport Bt sync correctly with BlocsSuportsBt'
    else:
        msg = '{} Suports Bt sync correctly with BlocsSuportsBt'.format(
            n_created_elements
        )

    logger.info(msg)


def down(cursor, installed_version):
    pass


migrate = up
