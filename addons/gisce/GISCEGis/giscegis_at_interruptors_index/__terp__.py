# -*- coding: utf-8 -*-
{
    "name": "Index Interruptors AT",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Index als interruptors AT:
     - seccionadorunifilar
     - seccionadorat
     - fusiblesat
     - interruptorat
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "giscegis_base_index",
        "giscegis_blocs_seccionador_unifilar",
        "giscegis_blocs_seccionadorat",
        "giscegis_blocs_fusiblesat",
        "giscegis_blocs_interruptorat",
        "giscegis_search_type"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscegis_search_type_data.xml"
    ],
    "active": False,
    "installable": True
}
