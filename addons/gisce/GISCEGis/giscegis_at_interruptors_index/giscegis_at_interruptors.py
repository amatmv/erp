from giscegis_base_index.giscegis_base_index import GiscegisBaseIndex

class GiscedataCellesCella(GiscegisBaseIndex):
    _name = "giscedata.celles.cella"
    _inherit = "giscedata.celles.cella"

    _index_title = "{obj.name}"
    _index_title = "{obj.descripcio}"

    _index_fields = {
        "cini":True,
        "data_pm": True,
        "installacio_name": True,
        "name": True
    }

    def get_path(self, cursor, uid, item_id, context=None):
        """
        Implements the get_path

        :param cursor: Database cursor
        :param uid: User id
        :param item_id: Item id
        :param context: OpenERP context
        :return: Path as str
        """
        return 'giscegis.at.interruptors/1{0}'.format(item_id)

    def get_lat_lon(self, cursor, uid, item, context=None):
        """
        Returns the lat and lon of a cella

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param item: Item to get lat and lon
        :param context: OpenERP context
        :return: lat,lon
        :rtype: tuple(int, int)
        """

        if context is None:
            context = {}
        cursor.execute("""
        SELECT
            ST_Y(ST_GeometryN(ST_Transform(cel.geom,4326),1)
                 ) AS lat,
            ST_X(ST_GeometryN(ST_Transform(cel.geom,4326),1)
                 ) AS lon
        FROM giscedata_celles_cella cel
        WHERE
            cel.id = %s
        """, (item.id, ))
        res = cursor.fetchone()
        if not res:
            res = (0, 0)
        return res


GiscedataCellesCella()

class GiscegisBlocsSeccionadorUnifilar(GiscegisBaseIndex):
    _name = 'giscegis.blocs.seccionadorunifilar'
    _inherit = 'giscegis.blocs.seccionadorunifilar'

    _index_title = '{obj.name}'
    _index_summary = '{obj.blockname.name} {obj.codi}'

    _index_fields = {
        'codi': True,
        'blockname.name': True,
    }

    def get_path(self, cursor, uid, item_id, context=None):
        """
        Implements the get_path

        :param cursor: Database cursor
        :param uid: User id
        :param item_id: Item id
        :param context: OpenERP context
        :return: Path as str
        """
        return 'giscegis.at.interruptors/1{0}'.format(item_id)

    def get_schema(self, cursor, uid, item, context=None):
        """
        Gets the index schema

        :param cursor: Database cursor
        :param uid: User id
        :param item: item to index
        :param context: OpenERP context
        :return: Dict with the schema
        """
        lat, lon = self.get_lat_lon(cursor, uid, item, context=context)
        content = self.get_content(cursor, uid, item, context=context)
        return {
            'id': '1{}'.format(item.id),
            'path': self.get_path(cursor, uid, item.id, context=context),
            'title': self._index_title.format(obj=item),
            'content': ' '.join(set(content)),
            'gistype': 'giscedata.interruptors.at',
            'summary': self._index_summary.format(obj=item),
            'lat': lat,
            'lon': lon
        }

    def get_lat_lon(self, cursor, uid, item, context=None):
        """
        Returns the lat and lon of a interrruptor

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param item: Item to get lat and lon
        :param context: OpenERP context
        :return: lat,lon
        :rtype: tuple(int, int)
        """
        if context is None:
            context = {}
        cursor.execute("""
        SELECT
            ST_Y(ST_GeometryN(ST_Transform(gv.geom,4326),1)
                 ) AS lat,
            ST_X(ST_GeometryN(ST_Transform(gv.geom,4326),1)
                 ) AS lon
        FROM giscegis_interruptorsat_geom gv
        WHERE
            gv.id = %s
        """, (item.id, ))
        res = cursor.fetchone()
        if not res:
            res = (0, 0)
        return res


GiscegisBlocsSeccionadorUnifilar()


class GiscegisBlocsSeccionadorAt(GiscegisBaseIndex):
    _name = 'giscegis.blocs.seccionadorat'
    _inherit = 'giscegis.blocs.seccionadorat'
    _index_title = '{obj.name}'
    _index_summary = '{obj.blockname.name} {obj.codi}'
    _index_fields = {
        'codi': True,
        'blockname.name': True,
    }

    def get_path(self, cursor, uid, item_id, context=None):
        """
        Implements the get_path

        :param cursor: Database cursor
        :param uid: User id
        :param item_id: Item id
        :param context: OpenERP context
        :return: Path as str
        """
        return 'giscedata.interruptors.at/2{0}'.format(item_id)

    def get_schema(self, cursor, uid, item, context=None):
        """
        Gets the index schema

        :param cursor: Database cursor
        :param uid: User id
        :param item: item to index
        :param context: OpenERP context
        :return: Dict with the schema
        """
        lat, lon = self.get_lat_lon(cursor, uid, item, context=context)
        content = self.get_content(cursor, uid, item, context=context)
        return {
            'id': '2{}'.format(item.id),
            'path': self.get_path(cursor, uid, item.id, context=context),
            'title': self._index_title.format(obj=item),
            'content': ' '.join(set(content)),
            'gistype': 'giscedata.interruptors.at',
            'summary': self._index_summary.format(obj=item),
            'lat': lat,
            'lon': lon
        }

    def get_lat_lon(self, cursor, uid, item, context=None):
        """
        Returns lat and lon of a seccionador

        :param cursor: Database cursor
        :param uid: User id
        :param item: Item to check
        :param context: OpenERP context
        :return: lat,lon
        :rtype: tuple(float, float)
        """
        if context is None:
            context = {}
        cursor.execute("""
        SELECT
            ST_Y(ST_GeometryN(ST_Transform(gv.geom,4326),1)
                 ) AS lat,
            ST_X(ST_GeometryN(ST_Transform(gv.geom,4326),1)
                 ) AS lon
        FROM giscegis_interruptorsat_geom gv
        WHERE
            gv.id = 2%s
        """, (item.id, ))
        res = cursor.fetchone()
        if not res:
            res = (0, 0)
        return res

GiscegisBlocsSeccionadorAt()


class GiscegisBlocsFusiblesAt(GiscegisBaseIndex):
    _name = 'giscegis.blocs.fusiblesat'
    _inherit = 'giscegis.blocs.fusiblesat'

    _index_title = '{obj.name}'
    _index_summary = '{obj.blockname.name} {obj.codi}'

    _index_fields = {
        'codi': True,
        'blockname.name': True,
    }

    def get_path(self, cursor, uid, item_id, context=None):
        """
        Implements the get_path

        :param cursor: Database cursor
        :param uid: User id
        :param item_id: Item id
        :param context: OpenERP context
        :return: Path as str
        """
        return 'giscedata.interruptors.at/3{0}'.format(item_id)

    def get_schema(self, cursor, uid, item, context=None):
        """
        Gets the index schema

        :param cursor: Database cursor
        :param uid: User id
        :param item: item to index
        :param context: OpenERP context
        :return: Dict with the schema
        """
        lat, lon = self.get_lat_lon(cursor, uid, item, context=context)
        content = self.get_content(cursor, uid, item, context=context)
        return {
            'id': '3{}'.format(item.id),
            'path': self.get_path(cursor, uid, item.id, context=context),
            'title': self._index_title.format(obj=item),
            'content': ' '.join(set(content)),
            'gistype': 'giscedata.interruptors.at',
            'summary': self._index_summary.format(obj=item),
            'lat': lat,
            'lon': lon
        }

    def get_lat_lon(self, cursor, uid, item, context=None):
        """
        Returns the lat and lon of a fusible at

        :param cursor: Database cursor
        :param uid: User id
        :param item: Item to get the lat and lon
        :param context: OpenERP context
        :return: lat,lon
        :rtype: tuple(float, float)
        """
        if context is None:
            context = {}
        cursor.execute("""
        SELECT
            ST_Y(ST_GeometryN(ST_Transform(gv.geom,4326),1)
                 ) AS lat,
            ST_X(ST_GeometryN(ST_Transform(gv.geom,4326),1)
                 ) AS lon
        FROM giscegis_interruptorsat_geom gv
        WHERE
            gv.id = 3%s
        """, (item.id, ))
        res = cursor.fetchone()
        if not res:
            res = (0, 0)
        return res

GiscegisBlocsFusiblesAt()


class GiscegisBlocsInterruptorAt(GiscegisBaseIndex):
    _name = 'giscegis.blocs.interruptorat'
    _inherit = 'giscegis.blocs.interruptorat'
    _index_title = '{obj.name}'
    _index_summary = '{obj.blockname.name} {obj.codi}'

    _index_fields = {
        'codi': True,
        'blockname.name': True,
    }

    def get_path(self, cursor, uid, item_id, context=None):
        """
        Implements the get_path

        :param cursor: Database cursor
        :param uid: User id
        :param item_id: Item id
        :param context: OpenERP context
        :return: Path as str
        """
        return 'giscedata.interruptors.at/4{0}'.format(item_id)

    def get_schema(self, cursor, uid, item, context=None):
        """
        Gets the index schema

        :param cursor: Database cursor
        :param uid: User id
        :param item: item to index
        :param context: OpenERP context
        :return: Dict with the schema
        """
        lat, lon = self.get_lat_lon(cursor, uid, item, context=context)
        content = self.get_content(cursor, uid, item, context=context)
        return {
            'id': '4{}'.format(item.id),
            'path': self.get_path(cursor, uid, item.id, context=context),
            'title': self._index_title.format(obj=item),
            'content': ' '.join(set(content)),
            'gistype': 'giscedata.interruptors.at',
            'summary': self._index_summary.format(obj=item),
            'lat': lat,
            'lon': lon
        }

    def get_lat_lon(self, cursor, uid, item, context=None):
        if context is None:
            context = {}
        cursor.execute("""
        SELECT
            ST_Y(ST_GeometryN(ST_Transform(gv.geom,4326),1)
                 ) AS lat,
            ST_X(ST_GeometryN(ST_Transform(gv.geom,4326),1)
                 ) AS lon
        FROM giscegis_interruptorsat_geom gv
        WHERE
            gv.id = 4%s
        """, (item.id, ))
        res = cursor.fetchone()
        if not res:
            res = (0, 0)
        return res

GiscegisBlocsInterruptorAt()
