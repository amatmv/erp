# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Elements oberts BT per a Giscegis 3",
    "description": """Vista dels Elements oberts per la versió Giscegis 3""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_base",
        "giscegis_base_geom",
        "giscegis_configdefault_bt"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
