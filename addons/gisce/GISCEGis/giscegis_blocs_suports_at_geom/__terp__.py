# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Blocs Suports per GISCEGIS v3",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscedata_at",
        "giscegis_base",
        "giscegis_blocs_tensio",
        "giscegis_blocs_suports_at",
        "giscegis_base_geom",
        "giscegis_at"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
