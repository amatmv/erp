# *-* coding: utf-8 *-*

from osv import osv, fields
from tools import config


class GiscedataAtSuport(osv.osv):

    _name = "giscedata.at.suport"
    _inherit = "giscedata.at.suport"

    def _coords(self, cursor, uid, ids, field_name, arg, context):
        """
        Returns the x and y position of the suport AT in the ERP SRID

        :param cursor: Database cursor
        :param uid: User id
        :param ids: Identifiers of the CTs
        :param field_name:
        :param arg:
        :param context: OpenERP context
        :return:
        :rtype: dict{id: {x:x, y:y}}
        """
        resp = dict((x, {"x": False, "y": False}) for x in ids)
        sql = """
        SELECT id, st_x(geom), st_y(geom)
        FROM giscegis_blocs_suports_at_geom
        WHERE id in %s
        """
        cursor.execute(sql, (tuple(ids),))
        data = cursor.fetchall()
        for line in data:
            resp[line[0]]['x'] = line[1]
            resp[line[0]]['y'] = line[2]

        return resp

    _columns = {
      'x': fields.function(_coords, method=True, type='float', string='x', multi="coords"),
      'y': fields.function(_coords, method=True, type='float', string='y', multi="coords"),
    }


GiscedataAtSuport()


class GiscegisBlocsSuportsAtGeom(osv.osv):
    _name = 'giscegis.blocs.suports.at.geom'
    _auto = False
    _columns = {
        'id': fields.integer('Id', readonly=True),
        'texte': fields.text('Texte', readonly=True),
        'geom': fields.text('Geom', readonly=True),
        'numero': fields.integer('Numero', readonly=True),
        'blockname': fields.text('Blockname', readonly=True),
        'rotation': fields.integer('Rotation', readonly=True),
        "materialize_date": fields.datetime("Fecha creación vista"),
    }

    def init(self, cursor):
        cursor.execute("""
            DROP MATERIALIZED VIEW IF EXISTS giscegis_blocs_suports_at_geom;
            
            CREATE MATERIALIZED VIEW public.giscegis_blocs_suports_at_geom AS
                SELECT 
                    suport.id AS id,
                    suport.name || ' ' || poste.name AS texte,
                    ST_SETSRID(
                        ST_MAKEPOINT(vertex.x, vertex.y), %(srid)s
                    ) AS geom,
                    substring(
                        block.numsuport::text, 
                        position('-' in block.numsuport::text) + 1
                    ) AS numero,
                    blockname.name AS blockname, 
                    360 - block.rotation AS rotation,
                    now() AS materialize_date
                FROM giscegis_blocs_suports_at block
                LEFT JOIN giscedata_at_suport suport ON 
                    block.numsuport::text = suport.name::text
                LEFT JOIN giscedata_at_poste poste ON suport.poste = poste.id
                LEFT JOIN giscegis_vertex vertex ON block.vertex = vertex.id
                LEFT JOIN giscegis_blocs_suports_at_blockname blockname ON
                    block.blockname = blockname.id;
        """, {'srid': config.get('srid', 25830)})

        return True

    def _auto_init(self, cursor, context):
        s = super(GiscegisBlocsSuportsAtGeom, self)
        return s._auto_init(cursor, context)

    def get_srid(self, cursor, uid):

        return config.get('srid', 25830)

    def recreate(self, cursor, uid):
        cursor.execute("""
            REFRESH MATERIALIZED VIEW giscegis_blocs_suports_at_geom;
        """)
        return True


GiscegisBlocsSuportsAtGeom()
