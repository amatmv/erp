# -*- coding: utf-8 -*-
import base64
from StringIO import StringIO
from zipfile import ZipFile, BadZipfile
import os
import itertools

from tools.translate import _
from osv import osv, fields
import shapefile
import netsvc
from tools.misc import email_send


class GiscegisForaServeiLoader(osv.osv_memory):

    _name = "giscegis.fora.servei.loader"


    def import_data_shp(self, cursor, uid, ids, context=None):
        """
        Attends the laod of shapes

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: Wizard id
        :type ids: list of int
        :param context:OpenERP context
        :type context: dict
        :return: None
        """

        obj_users = self.pool.get("res.users")
        obj_msg = self.pool.get("giscegis.load.message")
        obj_config = self.pool.get("res.config")

        from_email = obj_config.get(
            cursor, uid,
            "email_from", "no-reply@gisce.net"
        )

        message_id = obj_msg.create(cursor, uid, {})
        message = obj_msg.browse(cursor, uid, message_id)
        message.start_load()

        user_data = obj_users.browse(cursor, uid, uid)
        user_email = user_data.address_id.email
        logger = netsvc.Logger()
        if user_email:
            email_send(
                from_email,
                [user_email],
                _("Carrega de fora de servei"),
                message.get_message(),
                debug=True
            )
        else:
            info_msg = "El usuari amb uid {} no te adreça de correu assignada".format(
                uid)
            logger.notifyChannel(
                'SHPLoader', netsvc.LOG_INFO,
                info_msg)

        base_geom = self.pool.get('giscegis.base.geom')
        not_found_message = _(u"No s'han trobat fitxers .shp o .dbf en el fitxer,s'atura la importació")
        wizard = self.browse(cursor, uid, ids[0], context)
        wizard.write({'status': ''})

        sql_create = """
        CREATE TABLE giscegis_fora_servei_geom_tmp  (
          id INTEGER,
          create_uid INTEGER,
          create_date timestamp without time zone,
          write_date timestamp without time zone,
          write_uid INTEGER,
          gid INTEGER,
          tipolinea text,
          capa text,
          geom geometry(Geometry,%s)
          );
        """
        srid = base_geom.get_srid(cursor, uid)
        cursor.execute(sql_create, (srid,))
        if wizard.file_fs:
            data = base64.decodestring(wizard.file_fs)
        else:
            logger.notifyChannel(
                'SHPLoader', netsvc.LOG_INFO,
                _(u"No s'ha especificat el fitxer de cartografia"))
            wizard = self.browse(cursor, uid, ids[0], None)
            base_geom.add_status(
                cursor, uid, ids,
                _(u"No s'ha especificat el fitxer de cartografia"),
                wizard)
            message.log_message(_("No s'ha especificat el fitxer de fora servei"))
            message.end_load(_("Error"))

        if wizard.file_fs:
            s = StringIO(data)
            try:
                input_zip = ZipFile(s)
            except BadZipfile:
                logger.notifyChannel(
                    'SHPLoader', netsvc.LOG_WARNING,
                    _('Fitxer de fora servei corrupte'))

                wizard = self.browse(cursor, uid, ids[0], None)
                base_geom.add_status(
                    cursor, uid, ids, _('Fitxer fora servei corrupte'),
                    wizard)

                message.log_message(_('Fitxer fora servei corrupte'))
                message.end_load(_("Error"))
                return
            filenames = sorted(input_zip.namelist())
            if filenames:
                base_geom.truncate_table(cursor, uid, "giscegis_fora_servei_geom")
                shp_data = None
                dbf_data = None
                for filename in filenames:
                    if os.path.splitext(filename)[1].lower() == ".dbf":
                        dbf_data = StringIO(input_zip.read(filename))
                    if os.path.splitext(filename)[1].lower() == ".shp":
                        shp_data = StringIO(input_zip.read(filename))
                    if shp_data and dbf_data:
                        fields_desc = {
                            "capa": "text",
                            "tipolinea": "text"
                        }
                        end_function = ("giscegis.fora.servei.geom",
                                        "end_load_shp", ([], context))
                        base_geom.load_shp(
                            cursor, uid, "giscegis_fora_servei_geom_tmp", shp_data,
                            dbf_data, fields_desc, ignore_id=True,
                            end_function=end_function)
                        shp_data = None
                        dbf_data = None

            else:
                logger.notifyChannel(
                    'SHPLoader', netsvc.LOG_WARNING, not_found_message)
                wizard = self.browse(cursor, uid, ids[0], None)
                base_geom.add_status(
                    cursor, uid, ids, not_found_message,
                    wizard)
                message.log_message(not_found_message)
                message.end_load(_("Error"))

            logger.notifyChannel(
                'SHPLoader', netsvc.LOG_INFO,
                _('Dades carregades correctament'))
            base_geom.switch_table(
                cursor, uid, "giscegis_fora_servei_geom_tmp", "giscegis_fora_servei_geom", context
            )
            wizard = self.browse(cursor, uid, ids[0], None)
            base_geom.add_status(
                cursor, uid, ids,
                _('Dades carregades correctament'),
                wizard)
            message.log_message(
                _('Dades carregades correctament'))


            status_cache = base_geom.remove_tms_cache(cursor, uid, 'propia')
            if status_cache:
                logger.notifyChannel(
                    'SHPLoader', netsvc.LOG_INFO,
                    _('Cache netejat correctament'))
                wizard = self.browse(cursor, uid, ids[0], None)
                base_geom.add_status(
                    cursor, uid, ids, _('Cache netejat correctament'),
                    wizard)
                message.log_message(_('Cache netejat correctament'))
                message.end_load(_("Correcte"))
            else:
                logger.notifyChannel(
                    'SHPLoader', netsvc.LOG_ERROR,
                    _('Error al eliminar el cache'))

                message.log_message(_('Cache netejat correctament'))
                message.end_load(_("Error"))

                wizard = self.browse(cursor, uid, ids[0], None)
                base_geom.add_status(
                    cursor, uid, ids, _('Error al eliminar el cache'),
                    wizard)
            if user_email:
                email_send(
                    from_email,
                    [user_email],
                    _("Carrega de fora de servei"),
                    message.get_message(),
                    debug = True
                )
            else:
                info_msg = "El usuari amb uid {} no te adreça de correu assignada".format(uid)
                logger.notifyChannel(
                    'SHPLoader', netsvc.LOG_INFO,
                    info_msg
                )

    def _get_default_url(self, cursor, uid, context=None):
        """
        Return the URL of the web loader form

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param context: OpenERP context
        :type context: dict
        :return: URL
        :rtype: str
        """
        url = self.pool.get("res.config").get(cursor, uid, "giscegis_gis_url")
        return "{}/load/fs".format(url)

    _columns = {
        'file_fs': fields.binary('Fixer de Cartografia', help=()),
        'status': fields.text(_('Resultat')),
        "url": fields.text(_("URL"))

    }
    _defaults = {
        'state': lambda *a: 'init',
        "url": _get_default_url
    }

GiscegisForaServeiLoader()
