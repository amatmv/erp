# *-* coding: utf-8 *-*

from osv import osv, fields


class GiscegisForaServeiGeom(osv.osv):
    _name = "giscegis.fora.servei.geom"
    _columns = {
        'gid': fields.integer('Gid'),
        'id': fields.integer('Id', required=True),
        'capa': fields.text('Capa'),
        'tipolinea': fields.text('Tipo Linea'),
    }

    def end_load_shp(self, cursor, uid, ids, context):
        """
        Function executed after load the SHP

        :param cursor: Database cursor
        :param uid: User id
        :param ids: Affected ids
        :type ids: list
        :param context: OpenERP context
        :type context: dict
        :return: None
        :rtype None
        """

        base_geom = self.pool.get('giscegis.base.geom')
        base_geom.remove_tms_cache(cursor, uid, "particulars")
        base_geom.remove_tms_cache(cursor, uid, "fora_servei")
        base_geom.remove_tms_cache(cursor, uid, "inutilitzable")
        base_geom.remove_tms_cache(cursor, uid, "inutilitzable_mt")
        base_geom.remove_tms_cache(cursor, uid, "no_maniobrable")
        base_geom.remove_tms_cache(cursor, uid, "reserva")
        base_geom.remove_tms_cache(cursor, uid, "suports_fs")

    def init(self, cursor):
        conf_obj = self.pool.get('res.config')
        srid = int(conf_obj.get(cursor, 1, 'giscegis_srid', '25831'))
        cursor.execute('''
            SELECT * FROM information_schema.columns
            WHERE table_name = 'giscegis_fora_servei_geom'
            AND COLUMN_NAME = 'geom'

            ''')
        if not cursor.fetchone():
            sql_query = """SELECT AddGeometryColumn(
            'public','giscegis_fora_servei_geom',
            'geom',%(srid)s,'geometry',2, true);"""
            cursor.execute(sql_query, {'srid': srid})
        return True

GiscegisForaServeiGeom()
