# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from addons import get_module_resource
import base64


class ForaServeiTest(testing.OOTestCase):
    """"""

    def test_wizard_shp_loader(self):
        """
        Tests the shp loader of the module giscegis_fora_servei_geom

        :return: None
        """

        # self.openerp.install_module('giscegis_lat_geom')
        # self.openerp.install_module('giscegis_lbt_geom')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            model = self.openerp.pool.get("giscegis.fora.servei.loader")
            res_obj = self.openerp.pool.get('res.config')
            res_data = {
                'name': 'giscegis_gis_url',
                'value': '0'
            }
            res_obj.create(cursor, uid, res_data)
            lat_url = get_module_resource(
                'giscegis_fora_servei_geom', 'tests', 'fixtures', 'fora_servei.zip')
            with open(lat_url) as fd_lat:
                lat_data = fd_lat.read()

            wizard_id = model.create(cursor, uid, {})
            wizard_load = model.browse(cursor, uid, wizard_id)
            model_data = {'file_fs': base64.encodestring(lat_data)}
            model.write(cursor, uid, [wizard_id], model_data)
            wizard_load.import_data_shp()
            obj_lay = self.openerp.pool.get('giscegis.fora.servei.geom')
            ids_lay = obj_lay.search(cursor, uid, [])
            self.assertEqual(len(ids_lay), 2)
