# -*- coding: utf-8 -*-
{
    "name": "GISCE  GIS v3 (fora de servei)",
    "description": """Mòdul de fora servei GISCE GIS v3""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "base_extended",
        "giscegis_base_geom"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/giscegis_fora_servei_loader.xml",
        "security/ir.model.access.csv",
        "giscegis_layers_fora_servei_data.xml"
    ],
    "active": False,
    "installable": True
}
