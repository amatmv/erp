# coding=utf-8

import logging
from tools import pip_install


def up(cursor, installed_version):

    logger = logging.getLogger('openerp.migration')
    logger.info('Setting-up the right pandapower version.')

    pip_install('pandapower==2.0.1', '--force')

    logger.info(
        'Pandapower 2.0.1 installed with the oficial version.'
    )


def down(cursor, installed_version):
    pass


migrate = up
