# coding=utf-8

import logging
import pooler
import base64


def up(cursor, installed_version):

    logger = logging.getLogger('openerp.migration')
    logger.info('Setting up the first serialized network.')

    pool = pooler.get_pool(cursor.dbname)
    network_obj = pool.get('giscegis.network')
    nbm_obj = pool.get('giscegis.node.bloc.model')
    network_ids = network_obj.search(cursor, 1, [])
    nbm_ids = nbm_obj.search(cursor, 1, [])
    if not nbm_ids:
        nbm_obj.generate(cursor, 1)
    if not network_ids:
        serialized_net = network_obj.serialize(cursor, 1)
        network_data = {
            'production_network': True,
            'name': 'initial_serialization',
            'network': base64.b64encode(serialized_net.getvalue())
        }
        network_obj.create(cursor, 1, network_data)
        logger.info('First network serialization created correctly.')
    else:
        logger.info(
            'A network serialization already exist. Not creating a new one.'
        )


def down(cursor, installed_version):
    pass


migrate = up
