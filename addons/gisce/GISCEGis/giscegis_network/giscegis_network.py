# *-* coding: utf-8 *-*
from osv import osv, fields
import pandapower_erp
import pandapower_validator


class GiscegisNetwork(osv.osv):
    _name = 'giscegis.network'

    _columns = {
        "name": fields.char(
            "Nom de la Serialització",
            size=256,
            required=True
        ),
        "network": fields.binary("Xarxa Serialitzada"),
        "production_network": fields.boolean("Xarxa de Producció"),
        "pandapower_erp_version": fields.char(
            "Versio de pandapower_erp",
            size=256
        ),
        "start": fields.datetime("Inici"),
        "end": fields.datetime("Fi"),
    }

    _defaults = {
        "production_network": lambda *a: False
    }

    def to_pandapower(self, cursor, uid, context=None):
        """
        It serializes the network using Pandapower ERP library
        """
        # TODO: clarify best way to deal with session_id
        config = {
            "pool": self.pool,
            "uid": uid,
            "cursor": cursor,
        }

        client = pandapower_erp.client.erp.Client(**config)
        return pandapower_erp.network.Network(client)

    def serialize(self, cursor, uid, context=None, network=None):
        """
        It serializes the network using Pandapower ERP library
        """
        if not network:
            # TODO: clarify best way to deal with session_id
            network = self.to_pandapower(cursor, uid, context=context)
        # TODO: @xbarnada review if can be interesting to reach an string with file contents
        # base64 representation? or just string
        return network.serialize()

    def validate(self, cursor, uid, context=None):
        """
        It validates a network using Pandapower Validator library
        """
        # TODO: clarify best way to deal with session_id
        # TODO: clarify best way to deal with respect_switches
        network = self.to_pandapower(cursor, uid, context=context)
        validator = pandapower_validator.Network(
            net=network.net,
        )
        return validator.validate(respect_switches=True)


GiscegisNetwork()
