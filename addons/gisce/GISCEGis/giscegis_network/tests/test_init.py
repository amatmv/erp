from destral import testing
from destral.transaction import Transaction

class GiscegisNetwork(testing.OOTestCase):

    def test_network_creation(self):
        """
        Test network creation
        :return: None
        """
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor

            network_obj = self.openerp.pool.get('giscegis.network')
            expected_fields = ['name', 'production_network']

            # Create a non production network
            expected_network_1 = {'name': 'Network 1', 'production_network': False}

            network_id_1 = network_obj.create(
                cursor, uid, expected_network_1
            )

            network_data_1 = network_obj.read(
                cursor, uid, network_id_1, expected_fields
            )

            for field in expected_fields:
                self.assertEqual(expected_network_1[field], network_data_1[field])

            # Create a production network
            expected_network_2 = {'name': 'Network 2', 'production_network': True}

            network_id_2 = network_obj.create(
                cursor, uid, expected_network_2
            )

            network_data_2 = network_obj.read(
                cursor, uid, network_id_2, expected_fields
            )

            for field in expected_fields:
                self.assertEqual(expected_network_2[field], network_data_2[field])

    def test_create_network(self):
        """
        Tests network serialization
        :return: None
        """

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor

            session_obj = self.openerp.pool.get('giscegis.session')
            net_obj = self.openerp.pool.get('giscegis.network')
            session_obj.create_new_network(cursor, uid, "test")
            net_id = net_obj.search(cursor, uid, [], order="id desc", limit=1)
            if isinstance(net_id, list):
                net_id = net_id[0]

            net_data = net_obj.read(cursor, uid, net_id, ["pandapower_erp_version", "start", "end"])

            self.assertGreater(net_data["end"], net_data["start"])
            self.assertIsNotNone(net_data["pandapower_erp_version"])
