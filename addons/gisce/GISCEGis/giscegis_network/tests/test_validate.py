from destral import testing
from destral.transaction import Transaction


class GiscegisNetworkSerialization(testing.OOTestCase):

    def test_validation(self):
        """
        Test network validation
        :return: None
        """
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor

            # prepare blocmodel
            # TODO: @adrinicolau disable this enfoce once the test data is properly filled
            bloc_obj = self.openerp.pool.get('giscegis.node.bloc.model')
            bloc_obj.generate(cursor, uid)

            network_obj = self.openerp.pool.get('giscegis.network')
            validated, details = network_obj.validate(cursor, uid)

            self.assertTrue(validated)
