# coding=utf-8
from __future__ import absolute_import
from osv import osv, fields
from giscegis_base_geom import fields as geo_fields
from giscegis_base_geom.giscegis_session import SessionManaged
from destral import testing
from destral.transaction import Transaction
import logging
import time

logger = logging.getLogger(__name__)


__all__ = [
    'NetworkAndSessionTests'
]


# Simple Points Tests
class PointModelBaseTest(osv.osv):
    _name = "point.model.test"
    _columns = {
        "name": fields.char("Name", size=64),
        "active": fields.boolean("Actiu")
    }
    _defaults = {
        'active': lambda *a: 1,
    }


class PointModelTest(SessionManaged):
    _name = "point.model.test"
    _inherit = "point.model.test"
    _columns = {
        "geom": geo_fields.Point("geom", srid=25830),
        "node_id": fields.many2one("giscegis.nodes", "Node"),
        "active": fields.boolean("Actiu")
    }
    _defaults = {
        'active': lambda *a: 1,
    }


class NetworkAndSessionTests(testing.OOTestCase):

    def setUp(self):
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user
        self.create_test_point_model()
        self.session_id = self.create_session()

    def tearDown(self):
        self.txn.stop()

    def create_session(self, values=None):
        session_obj = self.openerp.pool.get('giscegis.session')
        if values is None:
            values = {
                'name': 'Test Session',
                'bbox': 'POLYGON((0 0, 10 0, 10 10, 0 10, 0 0))'
            }
        return session_obj.create(self.cursor, self.uid, values)

    def create_test_point_model(self):
        PointModelBaseTest()
        osv.class_pool['point.model.test'].createInstance(
            self.openerp.pool, 'giscegis_base_geom', self.cursor
        )
        point_obj = self.openerp.pool.get('point.model.test')
        point_obj._auto_init(self.cursor)
        PointModelTest()
        osv.class_pool['point.model.test'].createInstance(
            self.openerp.pool, 'giscegis_base_geom', self.cursor
        )
        point_obj = self.openerp.pool.get('point.model.test')
        point_obj._auto_init(self.cursor)

    def create_points(self):
        point_obj = self.openerp.pool.get('point.model.test')
        created_points = [
            point_obj.create(self.cursor, self.uid, {
                "name": "Point 1",
                "geom": "POINT (5 5)"
            }),
            point_obj.create(self.cursor, self.uid, {
                "name": "Point 2",
                "geom": "POINT (2 5)"
            }),
            point_obj.create(self.cursor, self.uid, {
                "name": "Point 3",
                "geom": "POINT (3 7)"
            }),
            point_obj.create(self.cursor, self.uid, {
                "name": "Point 4",
                "geom": "POINT (8 8)"
            }),
            point_obj.create(self.cursor, self.uid, {
                "name": "Point 5",
                "geom": "POINT (11 11)"
            })
        ]
        return created_points

    def test_session_network_update(self):
        """
        This test is made to check if the function field network_id of the
        session model is updated correctly
        :return: None
        :rtype: NoneType
        """
        session_obj = self.openerp.pool.get('giscegis.session')
        network_obj = self.openerp.pool.get('giscegis.network')

        networks_ids = network_obj.search(self.cursor, self.uid, [])
        network_obj.unlink(self.cursor, self.uid, networks_ids)

        # No network available
        session_data = session_obj.read(
            self.cursor, self.uid, self.session_id, ['network_id']
        )
        self.assertFalse(session_data['network_id'])

        # One network available with production_network False
        net_id = network_obj.create(
            self.cursor, self.uid,
            {'name': 'Prova 1', 'production_network': False}
        )
        session_data = session_obj.read(
            self.cursor, self.uid, self.session_id, ['network_id']
        )
        self.assertFalse(session_data['network_id'])

        # One network available with production_network True
        network_obj.write(
            self.cursor, self.uid, net_id, {'production_network': True}
        )
        session_data = session_obj.read(
            self.cursor, self.uid, self.session_id, ['network_id']
        )
        self.assertEqual('Prova 1', session_data['network_id'][1])

        # Two network available with production_network True
        network_obj.create(
            self.cursor, self.uid,
            {'name': 'Prova 2', 'production_network': True}
        )
        session_data = session_obj.read(
            self.cursor, self.uid, self.session_id, ['network_id']
        )
        self.assertEqual('Prova 2', session_data['network_id'][1])

        # Three network available and just one have production_network True
        network_obj.write(
            self.cursor, self.uid, net_id,
            {'production_network': False}
        )
        network_obj.create(
            self.cursor, self.uid,
            {'name': 'Prova 3', 'production_network': False}
        )
        session_data = session_obj.read(
            self.cursor, self.uid, self.session_id, ['network_id']
        )
        self.assertEqual('Prova 2', session_data['network_id'][1])

        # Four network available with two of them have production_network True
        network_obj.create(
            self.cursor, self.uid,
            {'name': 'Prova 4', 'production_network': True}
        )
        session_data = session_obj.read(
            self.cursor, self.uid, self.session_id, ['network_id']
        )
        self.assertEqual('Prova 4', session_data['network_id'][1])

    def test_merge_session_should_apply_all_changes(self):
        point_obj = self.openerp.pool.get('point.model.test')
        session_obj = self.openerp.pool.get('giscegis.session')
        network_obj = self.openerp.pool.get('giscegis.network')
        cataleg_obj = self.openerp.pool.get('giscegis.cataleg.cataleg')
        nodes_obj = self.openerp.pool.get("giscegis.nodes")
        ir_model_obj = self.openerp.pool.get('ir.model')
        ir_model_test_point_id = ir_model_obj.search(
            self.cursor, self.uid, [('model', '=', 'point.model.test')]
        )[0]

        cataleg_vals = {
            'name': 'Test Point',
            'model': ir_model_test_point_id,
            'domain': '[]',
            'geom_type': 'Point',
            'fields': '',
            'not_topology': False
        }
        cataleg_id = cataleg_obj.create(self.cursor, self.uid, cataleg_vals)

        nodes_vals77 = {
            'name': "ProvaNode77",
            'geom': "POINT (2 2)"
        }

        nodes_vals88 = {
            'name': "ProvaNode88",
            'geom': "POINT (8 8)"
        }

        node88_id = nodes_obj.create(self.cursor, self.uid, nodes_vals88)
        node77_id = nodes_obj.create(self.cursor, self.uid, nodes_vals77)

        original_point_vals = {
            "name": "Point 4",
            "geom": "POINT (3 3)",
            "node_id": node88_id
        }
        modified_res_id = point_obj.create(
            self.cursor, self.uid, original_point_vals
        )

        modified_point_vals = {
            "geom": "POINT(7 7)",
            "node_id": node77_id
        }
        point_obj.write(
            self.cursor, self.uid, [modified_res_id], modified_point_vals,
            context={'session': self.session_id, 'cataleg': cataleg_id}
        )

        added_point_vals = {
            "name": "Point 4",
            "geom": "POINT(4 4)",
            "node_id": node88_id
        }
        added_res_id = point_obj.create(
            self.cursor, self.uid, added_point_vals,
            context={'session': self.session_id, 'cataleg': cataleg_id}
        )

        deleted_point_vals = {
            "name": "Point 4",
            "geom": "POINT(6 6)",
            "node_id": node88_id
        }
        deleted_res_id = point_obj.create(
            self.cursor, self.uid, deleted_point_vals
        )

        point_obj.unlink(
            self.cursor, self.uid, [deleted_res_id],
            context={'session': self.session_id, 'cataleg': cataleg_id}
        )

        session = session_obj.browse(self.cursor, self.uid, self.session_id)
        self.assertEqual(len(session.changes), 3)

        # Validate merge behaviour asserting new Network creation
        networks_pre_merge = network_obj.search(self.cursor, self.uid, [])

        # Fill node_bloc_model before the merge
        bloc_obj = self.openerp.pool.get('giscegis.node.bloc.model')
        bloc_obj.generate(self.cursor, self.uid)

        # Patch network creation to avoid issues with Transaction and
        # async task, just for the specs use the non async method
        # and avoid to dispatch Autoworkers
        to_be_patched = session.__dict__['_table']
        to_be_patched.create_new_network_async = to_be_patched.create_new_network

        def dummy_pass():
            pass

        to_be_patched.dispatch_worker = dummy_pass
        session.__dict__['_table'].create_new_network_async = session.__dict__['_table'].create_new_network

        session.merge()

        networks_post_merge = network_obj.search(self.cursor, self.uid, [])
        self.assertEqual(len(networks_pre_merge), len(networks_post_merge)-1)

        results = point_obj.search(self.cursor, self.uid, [
            ('id', '=', deleted_res_id)
        ])
        self.assertEqual(len(results), 0)

        results = point_obj.search(self.cursor, self.uid, [
            ('id', '=', added_res_id)
        ])
        self.assertEqual(len(results), 1)

        values = point_obj.read(
            self.cursor, self.uid, [modified_res_id], ['geom']
        )[0]
        self.assertEqual(values['geom'], modified_point_vals['geom'])
        uid = self.txn.user
        cursor = self.txn.cursor

        sql = """
            DELETE FROM giscegis_node_bloc_model
            WHERE id IN (
                SELECT nbm.id
                FROM giscegis_node_bloc_model nbm
                LEFT JOIN ir_model ir ON ir.id=nbm.model
                WHERE ir.model ilike '%test%'
            );
            DELETE FROM giscegis_session_changes;
            DELETE FROM giscegis_cataleg_cataleg WHERE name ilike '%test%';
            DELETE FROM ir_model WHERE model ilike '%test%';
            DELETE FROM giscegis_network;
            DELETE FROM giscegis_configdefault_at;
            DELETE FROM giscegis_configdefault_bt;
            DELETE FROM point_model_test;
        """

        cursor.execute(sql)
        nodes_obj.unlink(
            self.cursor, self.uid, [node77_id, node88_id]
        )
        nbm_obj = self.openerp.pool.get('giscegis.node.bloc.model')
        session_obj = self.openerp.pool.get('giscegis.session')
        nbm_obj.generate(cursor, uid)
        # cursor.commit()
        session_obj.create_new_network(
            cursor,
            uid,
            name="Final Serialization",
            production=True,
            context={},
        )

        cursor.commit()
