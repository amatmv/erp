from destral import testing
from destral.transaction import Transaction
import os


class GiscegisNetworkStateMaintainment(testing.OOTestCase):

    # AT
    def test_configdefault_at(self):
        """
        Test network creation
        :return: None
        """
        with Transaction().start(self.database) as txn:
            self.oorq = os.environ.get('OORQ_ASYNC', 'True')
            os.environ['OORQ_ASYNC'] = 'False'
            uid = txn.user
            cursor = txn.cursor

            network_obj = self.openerp.pool.get('giscegis.network')
            network_state_at = self.openerp.pool.get(
                'giscegis.configdefault.at'
            )
            networks_ids = network_obj.search(cursor, uid, [])
            network_obj.unlink(cursor, uid, networks_ids)

            at_state_vals = {
                'name': 'int_obert1',
                'codi': 'Elem. Tall 20',
                'blockname_str': 'INT_AT',
                'node': 100
            }
            net_state_id = network_state_at.create(cursor, uid, at_state_vals)
            networks_ids = network_obj.search(cursor, uid, [])
            self.assertEqual(len(networks_ids), 1)

            at_state_vals = {
                'name': 'int_obert1',
                'codi': 'Elem. Tall 26',
                'blockname_str': 'INT_AT',
                'node': 92
            }
            network_state_at.write(cursor, uid, [net_state_id], at_state_vals)
            networks_ids = network_obj.search(cursor, uid, [])
            self.assertEqual(len(networks_ids), 2)

            network_state_at.unlink(cursor, uid, net_state_id)
            networks_ids = network_obj.search(cursor, uid, [])
            self.assertEqual(len(networks_ids), 3)

            os.environ['OORQ_ASYNC'] = self.oorq

            sql = """
                DELETE FROM giscegis_node_bloc_model 
                WHERE id IN (
                    SELECT nbm.id 
                    FROM giscegis_node_bloc_model nbm 
                    LEFT JOIN ir_model ir ON ir.id=nbm.model 
                    WHERE ir.model ilike '%test%'
                );
                DELETE FROM giscegis_session_changes;
                DELETE FROM giscegis_cataleg_cataleg WHERE name ilike '%test%';
                DELETE FROM ir_model WHERE model ilike '%test%';
                DELETE FROM giscegis_network;
                DELETE FROM giscegis_configdefault_at;
                DELETE FROM giscegis_configdefault_bt;
            """
            cursor.execute(sql)
            nbm_obj = self.openerp.pool.get('giscegis.node.bloc.model')
            session_obj = self.openerp.pool.get('giscegis.session')
            nbm_obj.generate(cursor, uid)
            session_obj.create_new_network(
                cursor,
                uid,
                name="Final Serialization",
                production=True,
                context={},
            )
            cursor.commit()

    # BT
    def test_configdefault_bt(self):
        """
        Test network creation
        :return: None
        """
        with Transaction().start(self.database) as txn:
            self.oorq = os.environ.get('OORQ_ASYNC', 'True')
            os.environ['OORQ_ASYNC'] = 'False'
            uid = txn.user
            cursor = txn.cursor

            network_obj = self.openerp.pool.get('giscegis.network')
            network_state_bt = self.openerp.pool.get(
                'giscegis.configdefault.bt'
            )
            networks_ids = network_obj.search(cursor, uid, [])
            network_obj.unlink(cursor, uid, networks_ids)

            bt_state_vals_1 = {
                'name': 'int_obert1',
                'codi': 'CT-007-R07',
                'blockname_str': 'RF_BT',
                'node': 117
            }
            net_state_id = network_state_bt.create(cursor, uid, bt_state_vals_1)
            networks_ids = network_obj.search(cursor, uid, [])
            self.assertEqual(len(networks_ids), 1)

            bt_state_vals_2 = {
                'name': 'int_obert1',
                'codi': 'CT-007-R04',
                'blockname_str': 'RF_BT',
                'node': 115
            }
            network_state_bt.write(cursor, uid, [net_state_id], bt_state_vals_2)
            networks_ids = network_obj.search(cursor, uid, [])
            self.assertEqual(len(networks_ids), 2)

            network_state_bt.unlink(cursor, uid, net_state_id)
            networks_ids = network_obj.search(cursor, uid, [])
            self.assertEqual(len(networks_ids), 3)

            os.environ['OORQ_ASYNC'] = self.oorq

            sql = """
                DELETE FROM giscegis_node_bloc_model 
                WHERE id IN (
                    SELECT nbm.id 
                    FROM giscegis_node_bloc_model nbm 
                    LEFT JOIN ir_model ir ON ir.id=nbm.model 
                    WHERE ir.model ilike '%test%'
                );
                DELETE FROM giscegis_session_changes;
                DELETE FROM giscegis_cataleg_cataleg WHERE name ilike '%test%';
                DELETE FROM ir_model WHERE model ilike '%test%';
                DELETE FROM giscegis_network;
                DELETE FROM giscegis_configdefault_at;
                DELETE FROM giscegis_configdefault_bt;
            """
            cursor.execute(sql)
            nbm_obj = self.openerp.pool.get('giscegis.node.bloc.model')
            session_obj = self.openerp.pool.get('giscegis.session')
            nbm_obj.generate(cursor, uid)
            session_obj.create_new_network(
                cursor,
                uid,
                name="Final Serialization",
                production=True,
                context={},
            )
            cursor.commit()
