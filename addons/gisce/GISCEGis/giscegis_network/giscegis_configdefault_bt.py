# -*- coding: utf-8 -*-
from osv import osv
import os


class GiscegisConfigdefaultBt(osv.osv):
    _name = "giscegis.configdefault.bt"
    _inherit = "giscegis.configdefault.bt"

    def create(self, cursor, uid, vals, context=None):
        res = super(GiscegisConfigdefaultBt, self).create(
            cursor, uid, vals, context=context
        )
        nbm_obj = self.pool.get('giscegis.node.bloc.model')
        session_obj = self.pool.get('giscegis.session')
        network_name = "BT: Nou {} obert amb codi {}".format(
            vals['blockname_str'], vals['codi']
        )
        nbm_obj.generate(cursor, uid)
        oorq_async = (os.environ.get('OORQ_ASYNC', 'True') == 'True')
        if not oorq_async:
            cursor.commit()
        session_obj.create_new_network_async(
            cursor,
            uid,
            name=network_name,
            production=True,
            context=context,
        )

        return res

    def write(self, cursor, uid, ids, vals, context=None):
        res = super(GiscegisConfigdefaultBt, self).write(
            cursor, uid, ids, vals, context=context
        )
        nbm_obj = self.pool.get('giscegis.node.bloc.model')
        session_obj = self.pool.get('giscegis.session')
        if 'blockname_str' in vals and 'codi' in vals:
            network_name = "Modificació {} amb codi {}".format(
                vals['blockname_str'], vals['codi']
            )
        else:
            network_name = "BT: Modificació interruptor obert amb id {}".format(
                ids
            )
        nbm_obj.generate(cursor, uid)
        oorq_async = (os.environ.get('OORQ_ASYNC', 'True') == 'True')
        if not oorq_async:
            cursor.commit()
        session_obj.create_new_network_async(
            cursor,
            uid,
            name=network_name,
            production=True,
            context=context,
        )

        return res

    def unlink(self, cursor, uid, ids, context=None):
        if not isinstance(ids, list):
            ids = [ids]
        vals = self.read(cursor, uid, ids, ['blockname_str', 'codi'])[0]
        res = super(GiscegisConfigdefaultBt, self).unlink(
            cursor, uid, ids, context=context
        )
        nbm_obj = self.pool.get('giscegis.node.bloc.model')
        session_obj = self.pool.get('giscegis.session')
        network_name = "BT: Tancar {} obert amb codi {}".format(
            vals['blockname_str'], vals['codi']
        )
        nbm_obj.generate(cursor, uid)
        oorq_async = (os.environ.get('OORQ_ASYNC', 'True') == 'True')
        if not oorq_async:
            cursor.commit()
        session_obj.create_new_network_async(
            cursor,
            uid,
            name=network_name,
            production=True,
            context=context,
        )

        return res


GiscegisConfigdefaultBt()
