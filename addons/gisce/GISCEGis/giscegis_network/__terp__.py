# -*- coding: utf-8 -*-
{
    "name": "GiscegisNetwork",
    "description": """
        ERP model where the Serializaed Electrical Nerwork is stored
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_base",
        "giscegis_topologia",
        "giscegis_node_bloc_model",
        "giscegis_at",
        "giscegis_bt",
        "giscegis_lbt_traceability",
        "giscegis_escomeses_traceability",
        "giscegis_configdefault_at",
        "giscegis_configdefault_bt",
    ],
    "init_xml": [],
    "demo_xml": [
        "giscegis_network_demo.xml"
    ],
    "update_xml":[
        "security/giscegis_network_security.xml",
        "security/ir.model.access.csv",
        "giscegis_network_view.xml"
    ],
    "active": False,
    "installable": True
}
