from osv import osv
import base64
import os
from oorq.decorators import job
from autoworker import AutoWorker
from datetime import datetime
import pandapower_erp

# Async definitions
ASYNC_QUEUE = "giscegis_network_serialization"
# 3 hores
ASYNC_TIMEOUT = 10800


def dispatch_worker(queue=ASYNC_QUEUE):
    aw = AutoWorker(queue=queue)
    aw.work()


class GiscegisSession(osv.osv):
    _name = 'giscegis.session'
    _inherit = 'giscegis.session'

    @job(queue=ASYNC_QUEUE, timeout=ASYNC_TIMEOUT)
    def create_new_network_async(self, cursor, uid, name, production=True,
                                 context=None):
        return self.create_new_network(cursor, uid, name, production=production,
                                       context=context)

    def create_new_network(self, cursor, uid, name, production=True,
                           context=None):
        """
        Creates a new Network element

        :param cursor: Database cursor
        :param uid: User id
        :type: uid: int
        :param name: Network element name
        :type name: str
        :param production: Defines if this is the productive network (True)
        :type production: bool
        :param context: OpenERP context
        :type context: dict
        :return: True
        :rtype: bool
        """

        assert isinstance(name, str)
        assert isinstance(production, bool)
        start = datetime.now()
        net_obj = self.pool.get('giscegis.network')
        esc_trac_obj = self.pool.get("giscegis.escomeses.traceability")
        lbt_trac_obj = self.pool.get("giscegis.lbt.traceability")

        # TODO! Just update nbm and properties heritance if global network
        # is_a_session = 'session_id' in context

        # TODO: is needed to clean session_id from context before the serialize?
        network = net_obj.to_pandapower(cursor, uid, context=context)

        # Prepare the serialization
        serialization_file = net_obj.serialize(cursor, uid, context=context,
                                               network=network)
        end = datetime.now()
        pw_version = pandapower_erp.__version__
        net_id = net_obj.create(cursor, uid, {
            "name": name,
            "network": base64.encodestring(serialization_file.getvalue()),
            "production_network": production,
            "start": start,
            "end": end,
            "pandapower_erp_version": pw_version
        })

        esc_trac_obj.fill_from_network_async(cursor, uid, net_id, context=context)
        lbt_trac_obj.fill_from_network_async(cursor, uid, net_id, context=context)
        return True

    def merge(self, cursor, uid, ids, context=None):
        """
        Overwrites the session merge to update nbm

        :param cursor: Database cursor
        :param uid: User id
        :param ids: Session id
        :param context: OpenERP context
        :return: True
        :rtype: bool
        """
        if context is None:
            context = {}
        res = super(GiscegisSession, self).merge(
            cursor, uid, ids, context=context
        )

        reserialize = context.get('reserialize', False)
        if reserialize:
            # We make this check and commit because if we don't the
            # serialization is made with another transaction and the NBM table
            # is empty.
            oorq_async = (os.environ.get('OORQ_ASYNC', 'True') == 'True')
            if not oorq_async:
                cursor.commit()

            # Will be great to achieve a new Network for each merged session_id,
            # but this will increase a lot the merge time; that's why we just
            # create ONE new Network with the merge ending state.
            network_name = "Merged sessions: {}".format(ids)
            self.create_new_network_async(
                cursor,
                uid,
                name=network_name,
                production=True,
                context=context,
            )
            # dispatch_worker()
        return res


GiscegisSession()
