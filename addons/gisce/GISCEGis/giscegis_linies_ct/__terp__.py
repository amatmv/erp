# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Línies CTS",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_lbt_traceability",
        "giscegis_blocs_fusiblesbt",
        "giscegis_vertex",
        "giscegis_node_bloc_model",
        "giscegis_color_acadmapguide",
        "giscegis_base",
        "giscegis_configdefault_bt"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscegis_linies_ct_data.xml",
        "ir.model.access.csv",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
