# -*- encoding: utf-8 -*-

from osv import osv, fields

try:
    import json
except ImportError:
    import simplejson as json

class giscegis_linies_ct_stop(osv.osv):
    _name = "giscegis.linies.ct.stop"
    _description = "GISCEGis Linies CT Stop"
    _columns = {
      'name': fields.many2one('ir.model','Model'),
    }

giscegis_linies_ct_stop()

class giscegis_linies_ct(osv.osv):
    _name = "giscegis.linies.ct"
    _description = "GISCEGis Linies CT"
    _columns = {

    }

    def init(self, cr):
        cr.execute("select node from giscegis_node_bloc_model where model in (select name from giscegis_linies_ct_stop)")
        self.stop_nodes = [a[0] for a in cr.fetchall()]
        cr.execute("select node from giscegis_configdefault_bt where node is not null")
        self.open_nodes = [a[0] for a in cr.fetchall()]

    def get_cts(self, cr, uid):
        cr.execute("select distinct ct from giscegis_lbt_traceability where ct is not null")
        cts = cr.dictfetchall()
        res = []
        for ct in cts:
            cr.execute("select c.id, c.name, descripcio, v.x, v.y, b.id as bid from giscedata_cts c left join giscegis_blocs_ctat b on (b.ct = c.id) left join giscegis_vertex v on (b.vertex = v.id) where c.id = %s order by name" % (int(ct['ct']),))
            _ct = cr.dictfetchone()
            res.append({'cid': _ct['id'], 'name': ("%s - %s") % (_ct['name'], _ct['descripcio']), 'x': _ct['x'], 'y': _ct['y'], 'bid': _ct['bid']})
        return json.dumps({'count': len(cts), 'resultats': res})

    def get_linies_ct(self, cr, uid, ct):
        ct = int(ct[0])
        # obtenir fusibles del ct
        cr.execute("select distinct fusible from giscegis_lbt_traceability where ct = %s" % (int(ct),))
        fusibles = cr.dictfetchall()
        res = []
        for fus in fusibles:
            if fus['fusible'] is not None:
                cr.execute("select id, codi from giscegis_blocs_fusiblesbt where id = %d" % (int(fus['fusible']),))
                _fus = cr.dictfetchone()
                res.append({'id': _fus['id'], 'name': _fus['codi']})
        return json.dumps({'count': len(fusibles), 'resultats': res})

    def get_trams_ct_linia(self, cr, uid, ids_fusibles):
        ids_fusibles = json.loads(ids_fusibles)
        # Inicialització
        self.init(cr)
        # Carreguem topologia
        topo_obj = self.pool.get('giscegis.topologia')
        G = topo_obj.load(cr, uid, {})
        node_obj = self.pool.get('giscegis.nodes')
        # obtenir node el fusible id_fusible
        fus_obj = self.pool.get('giscegis.blocs.fusiblesbt')
        linies = []
        ct = ''
        for id_fusible in ids_fusibles:
            # obtenir número de línia i ct
            num_linia = int(fus_obj.browse(cr, uid, id_fusible).codi[-2:])
            ct = fus_obj.browse(cr, uid, id_fusible).codi[:fus_obj.browse(cr, uid, id_fusible).codi.rfind('-')]
            color_obj = self.pool.get('giscegis.color.acadmapguide')
            res = color_obj.search(cr, uid, [('name::int','=',num_linia),])
            if len(res):
                color = color_obj.browse(cr, uid, res)[0].mapguide
            else:
                color = 4 # default color: black
            node = fus_obj.browse(cr, uid, id_fusible).node
            nlist = [node]
            neighbors = G.neighbors
            seen = {}
            pre = []
            for source in nlist:
                if source in seen: continue
                lifo = [source]
                while lifo:
                    v = lifo.pop()
                    if v.id in self.open_nodes or (v.id in self.stop_nodes and v.id != node.id):
                        pre.append(v)
                        continue
                    if v in seen:
                        continue
                    pre.append(v)
                    seen[v] = True
                    lifo.extend((w for w in G.neighbors(v) if w not in seen))
            linia = G.subgraph(pre) # aquí tenim els nodes de la línia
            vertexs = []
            segments = []
            for edge in [e[2] for e in linia.edges()]:
                cr.execute("select v.x , v.y , vp.sequence from giscegis_vertex v, giscegis_polyline_vertex vp, giscegis_polyline_vertex_rel r where vp.vertex = v.id and r.vertex_id = vp.id and r.polyline_id = %s order by vp.sequence asc", (edge.polyline.id,))
                vertexs = cr.dictfetchall()
                i=0
                while i < len(vertexs)-1:
                    segments.append({'x1': vertexs[i]['x'], 'y1': vertexs[i]['y'], 'x2': vertexs[i+1]['x'], 'y2': vertexs[i+1]['y']})
                    i += 1
            linies.append({'nom': num_linia, 'color': color, 'segments': segments})
        return json.dumps({'ct':ct, 'linies': linies})

giscegis_linies_ct()
