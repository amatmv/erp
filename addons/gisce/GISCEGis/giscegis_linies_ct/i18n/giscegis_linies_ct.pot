# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* giscegis_linies_ct
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.14\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2012-04-25 16:11:37+0000\n"
"PO-Revision-Date: 2012-04-25 16:11:37+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: giscegis_linies_ct
#: constraint:ir.model:0
msgid "The Object name must start with x_ and not contain any special character !"
msgstr ""

#. module: giscegis_linies_ct
#: field:giscegis.linies.ct.stop,name:0
msgid "Model"
msgstr ""

#. module: giscegis_linies_ct
#: model:ir.model,name:giscegis_linies_ct.model_giscegis_linies_ct
msgid "GISCEGis Linies CT"
msgstr ""

#. module: giscegis_linies_ct
#: model:ir.model,name:giscegis_linies_ct.model_giscegis_linies_ct_stop
msgid "GISCEGis Linies CT Stop"
msgstr ""

#. module: giscegis_linies_ct
#: model:ir.module.module,shortdesc:giscegis_linies_ct.module_meta_information
msgid "GISCE GIS Línies CTS"
msgstr ""

