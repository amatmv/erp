# *-* coding: utf-8 *-*
from osv import osv


class GiscegisSimulacioBt(osv.osv):
    _name = "giscegis.simulacio.bt"
    _inherit = "giscegis.simulacio.bt"

    def simulacio_ids(self, cr, uid, ct, sim_open_nodes=[], context=None, load=False):
        """
        Ens arriba com a paràmetre un CT. Hem de buscar els trafos que
        té aquest CT i fer un subgraf per a cada trafo. Finalment fer una
        unió dels dos grafs
        :param ct: CT per la que volem simular
        :type ct: integer
        :param sim_open_nodes: Llista d'Ids de nodes que estan oberts per la
        simulacio
        :type sim_open_nodes: list(int)
        """

        if context is None:
            context = {}

        if isinstance(ct, list):
            ct = ct[0]

        # S'ha de guardar els paràmetres de la simulació ja que després
        # s'ha de poder recuperar els resultats en termes de nodes i no
        # només en arestes i vèrtexs.

        vals = {}
        config_obj = self.pool.get('giscegis.configsimulacio.bt')
        trafo_obj = self.pool.get("giscedata.transformador.trafo")
        esc_obj = self.pool.get("giscedata.cups.escomesa")
        open_nodes_obj = self.pool.get('giscegis.configsimulacio.bt.oberts')
        topo_obj = self.pool.get('giscegis.topologia')
        node_obj = self.pool.get('giscegis.nodes')
        trafo_bloc_obj = self.pool.get('giscegis.blocs.transformadors')
        bt_escomeses_obj = self.pool.get('giscegis.simulacio.bt.resultats.escomeses')
        ct_obj = self.pool.get('giscedata.cts')

        vals['name'] = self.pool.get('ir.sequence').get(cr, uid, 'giscegis.configsimulacio.bt')
        vals['ct'] = ct

        config_id = config_obj.create(cr, uid, vals, context)
        config_name = vals['name']

        # Un cop tenim el ID de la configuració, cal guardar la llista de nodes
        # oberts per aquesta simulació (sense tenir en compte els
        # propis de l'estat de la xarxa!)

        vals = {}

        vals['config'] = config_id
        # iterar sobre tots els ids de nodes que s'obren i obtenir
        # el objecte node

        sim_open_nodes_obj = node_obj.read(cr, uid, sim_open_nodes, ['name'])
        for n in sim_open_nodes_obj:
            n_id = n['id']
            vals['name'] = node_obj.get_codi(cr, uid, n_id) or n['name']
            vals['node'] = n_id
            open_nodes_obj.create(cr, uid, vals, context)

        # guardar els resultats a giscegis_simulacio_bt_resultats (config_id)
        vals = {}
        sql = """
          SELECT count(id) AS n
          FROM giscegis_simulacio_bt_resultats
          WHERE id = %s
        """
        cr.execute(sql, (int(config_id),))
        res = cr.dictfetchone()

        bt_resultats_obj = self.pool.get('giscegis.simulacio.bt.resultats')
        vals = {
            'name': '%s_%i' % (config_name, res['n']+1),
            'simulacio': config_id
        }
        bt_resultats_id = bt_resultats_obj.create(cr, uid, vals, context)

        # Inicialització
        self.init(cr)

        # Carreguem topologia
        G = topo_obj.load(cr, uid, context)

        # Busquem els tots trafos (blocs) que
        # pertanyen al CT que ens ha enviat per simular
        trafo_bloc_ids = trafo_bloc_obj.search(
            cr, uid, [('ct', '=', ct)], context
        )
        trams_on = []
        trams_off = []
        nodes_on = []
        nodes_off = []
        trafos_ct = trafo_bloc_obj.browse(cr, uid, trafo_bloc_ids, context)
        # Els nodes del trafos del ct
        if trafos_ct:
            nodes_trf = [t.node.id for t in trafos_ct]
        else:
            trafos_ids = trafo_obj.search(cr, uid, [("ct", "=", ct)], context=context)
            nodes_trf = []
            for trafo in trafo_obj.read(cr, uid, trafos_ids, ["node_id"]):
                nodes_trf.append(trafo["node_id"][0])

        ct_node = ct_obj.browse(cr, uid, ct).node_id.id
        # Per cada trafo crearem un subgraf_bt a partir del
        # seu node (trafo_bloc.node.id)
        for source in node_obj.browse(cr, uid, nodes_trf, context):
            # afegim els altres trafos del ct com a stop_nodes
            extra_nodes = [n for n in nodes_trf if n != source.id] + [ct_node]
            Gbt = self.subgraf(G, source, extra_nodes)
            # Simulem i ens retorna un graf amb els nodes amb corrent (on)
            Gon = self.dfs_continuitat(Gbt, source, sim_open_nodes)
            # Afegit totes les coordenades de les arestes al resultat (trams_on)
            for edge_id in [e[2]["edge_id"] for e in Gon.edges(data=True)]:
                trams_on.append(int(edge_id))

            # Fem una copia del graph on per poder-hi treballar
            Gaux = Gon.copy()
            # Borrem els nodes que siguin frontera
            # del graf amb corrent (nodes oberts)

            Gaux.remove_nodes_from(node_obj.browse(cr, uid, self.open_nodes, context))
            Gaux.remove_nodes_from(node_obj.browse(cr, uid, sim_open_nodes, context))

            # Creem una copia de tot el subgraf bt
            Goff = Gbt.copy()
            # Hi esborrem tots els nodes del graf que hem creat
            # abans (nodes sense corrent) i ens quedara un graf amb els
            # nodes que no en tenen + els nodes oberts (frontera)

            Goff.remove_nodes_from(Gaux.nodes())

            # Afegim totes les coordenades de les arestes
            # al resultat (trams_off)
            for edge_id in [e[2]["edge_id"] for e in Goff.edges(data=True)]:
                trams_off.append(int(edge_id))
            nodes_on += Gon.nodes()
            nodes_off += Goff.nodes()

        # Guardar els nodes escomesa del subgraf amb info
        # d'estat (ON/OFF) a giscegis_simulacio_bt_resultats_escomeses

        vals = {
            'simulacio': bt_resultats_id
        }
        # agafem el pool per giscegis.blocs.escomeses

        tmpnodes = []
        for node in nodes_on:
            if node not in tmpnodes:
                tmpnodes.append(node)
            nmodel_obj = node.get_model()  # Retorna False per no-blocs
            if nmodel_obj:
                if nmodel_obj.model == "giscedata.cups.escomesa":
                    search_params = [("node_id", "=", node.id)]
                    esc_id = esc_obj.search(cr, uid, search_params, context)
                    vals["name"] = esc_id[0]
                    vals["estat"] = 1
                    bt_escomeses_obj.create(cr, uid, vals, context)

        # OFF
        escomeses_off = []

        for node in nodes_off:
            nmodel_obj = node.get_model()  # Retorna False per no-blocs
            if nmodel_obj:
                if nmodel_obj.model == "giscedata.cups.escomesa":
                    search_params = [("node_id", "=", node.id)]
                    esc_id = esc_obj.search(cr, uid, search_params, context)
                    vals["name"] = esc_id[0]
                    vals["estat"] = -1
                    bt_escomeses_obj.create(cr, uid, vals, context)
                    escomeses_off.append(esc_id[0])

        # FIXME quan estigui provat, posar les agregacions dins els bucles que
        #  ja tenim adalt i ens estalviarem una passada per totes les escomeses
        #  busquem les pòlisses de les escomeses que tenim OFF

        num_polisses_off = 0
        potencia_contractada_off = 0
        for gescomesa in escomeses_off:
            escomesa = esc_obj.browse(cr, uid, gescomesa)
            num_polisses_off += len(escomesa.polisses_actives)
            potencia_contractada_off += escomesa.potencia_polisses

        return {
            'trams_on': trams_on,
            'trams_off': trams_off,
            'escomeses_off': escomeses_off,
            'simulacio': bt_resultats_id,
            'num_polisses_off': num_polisses_off,
            'potencia_contractada_off': float("%.2f" % potencia_contractada_off)
        }

    def get_nodes(self, cr, uid, ct, state='all', srid=None, context=None):
        """
        Funció per retornar tots els nodes d'un subgraf de bt

        :param cr: Database Cursor
        :type cr: Cursor
        :param uid: User ID
        :type uid: int
        :param ct: Id del CT que volem obtenir els nodes
        :type ct: list of int or int
        :param state: {all, open, closed} Estat en que volem els interruptors
        :type state: str
        :param srid: SRID of the geometry projection
        :type srid: int
        :param context: OpenERP context
        :type context: dict[str, Any]
        :return: Nodes d'un subgraf
        :rtype: dict[str, Any]
        """

        if context is None:
            context = {}

        self.init(cr)

        if isinstance(ct, (list, tuple)):
            ct = ct[0]

        if srid is None:
            srid = 4326

        topo_obj = self.pool.get('giscegis.topologia')
        node_obj = self.pool.get('giscegis.nodes')
        trafo_obj = self.pool.get("giscedata.transformador.trafo")
        ct_obj = self.pool.get('giscedata.cts')

        ct_node = ct_obj.browse(cr, uid, ct).node_id.id

        G = topo_obj.load(cr, uid, context)

        # Busquem els tots trafos (blocs) que pertanyen al CT que ens
        # ha enviat per simular
        trafo_ids = trafo_obj.search(cr, uid, [('ct', '=', ct)])

        list_nodes = []
        trafos_ct = trafo_obj.read(cr, uid, trafo_ids, ['node_id'], context)

        # Els nodes del trafo del ct
        nodes_trf = []
        for t in trafos_ct:
            if t['node_id']:
                nodes_trf.append(t["node_id"][0])

        # Per a cada transformador del CT
        for trafo_data in trafos_ct:
            source = node_obj.browse(cr, uid, trafo_data["node_id"][0], context)
            # afegim els altres trafos del ct com a stop_nodes
            extra_stop = [n for n in nodes_trf if n != source.id] + [ct_node]
            Gbt = self.subgraf(G, source, extra_stop)
            # Si ens demana els nodes tancats
            if state == 'closed':
                # Eliminem del graf tots els nodes que estiguin oberts
                Gbt.remove_nodes_from(node_obj.browse(cr, uid, self.open_nodes, context))
            # Si ens demana els oberts
            elif state == 'open':
                # Creem un array amb els ids dels nodes del graf
                nodes = [getattr(node, 'id') for node in Gbt.nodes()]
                # Eliminem d'aquest array tots els que estiguin oberts per
                # tal que l'array tingui només els tancats
                for n_open in self.open_nodes:
                    try:
                        nodes.remove(n_open)
                    except ValueError:
                        continue
                # Eliminem del graf tots els tancats (tots - tancats = oberts)
                Gbt.remove_nodes_from(node_obj.browse(cr, uid, nodes, context))

            # Retornem un dict {'node':n.id, 'codi':n.name, 'x':n.x, 'y':n.y}
            appended_nodes = []
            for node in Gbt.nodes():
                if node.id in appended_nodes:
                    continue
                appended_nodes.append(node.id)
            sql = """
                SELECT id AS node, name AS codi, 
                       st_x(st_transform(geom, %(srid)s)) AS x, 
                       st_y(st_transform(geom, %(srid)s)) AS y
                FROM giscegis_nodes
                WHERE id IN %(ids)s
            """
            if appended_nodes:
                cr.execute(sql, {'ids': tuple(appended_nodes), 'srid': srid})
                rows = cr.dictfetchall()
                list_nodes += rows
        return list_nodes


GiscegisSimulacioBt()
