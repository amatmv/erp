from osv import osv
from giscedata_cts_subestacions.giscedata_cts_subestacions import TIPUS_PARC
from tools import config


class GiscedataCts(osv.osv):

    _name = 'giscedata.cts'
    _inherit = 'giscedata.cts'

    def _default_tipus_installacio(self, cr, uid, context=None):
        if context is None:
            context = {}
        obj = self.pool.get('giscedata.cts.installacio')
        ids = obj.search(cr, uid, [])
        res = obj.read(cr, uid, ids, ['name', 'id'], context)
        default_tipus = res[0]['id']

        # Primer intentem retornar el tipus d'installacio CT
        for r in res:
            if r['name'] == 'CT':
                return r['id']
        # Si no s'ha trobat es retorna el primer tipus d'installacio que no
        # sigui una SE
        for r in res:
            if r['name'] != 'SE':
                return r['id']

        # En el pitjor dels casos retornarem el primer id
        return default_tipus

    def _default_municipi(self, cursor, uid, context=None):
        """
        Propose a value for the field municipi using the geom passed on the
        context
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :param context: OpenERP Cursor
        :type context: dict[str, Any]
        :return: The ID of the ResMunicipi where the CT is ubicated
        :rtype int or bool
        """

        if context is None:
            context = {}

        srid = config.get('srid', '25830')
        geom = context.pop("geom", False)

        if geom:
            query = """
                SELECT id FROM res_municipi
                WHERE ST_INTERSECTS(
                    geom, 
                    ST_SnapToGrid(
                        ST_Translate(
                           ST_GeomFromText(%(geom)s,%(srid)s),
                           0.0000000001,0.0000000001
                        ),
                        0.0001
                    )
                )
            """
            cursor.execute(query, {'geom': geom, 'srid': srid})
            query_result = cursor.dictfetchall()
            if query_result:
                return query_result[0]['id']
            else:
                return False
        else:
            return False

    _defaults = {
        'id_installacio': lambda self, cr, uid, context=None:
            self._default_tipus_installacio(cr, uid, context),
        'id_municipi': _default_municipi
    }


GiscedataCts()


class GiscedataCtsSubestacions(osv.osv):

    _name = 'giscedata.cts.subestacions'
    _inherit = 'giscedata.cts.subestacions'

    def _default_tipus_installacio(self, cr, uid, context=None):
        if context is None:
            context = {}
        obj = self.pool.get('giscedata.cts.installacio')
        ids = obj.search(cr, uid, [('name', '=', 'SE')])
        return ids[0]

    _defaults = {
        'id_installacio': lambda self, cr, uid, context=None:
            self._default_tipus_installacio(cr, uid, context),
        'tecnologia': lambda *a: TIPUS_PARC[1][0]
    }


GiscedataCtsSubestacions()


class GiscedataTrafo(osv.osv):

    _name = 'giscedata.transformador.trafo'
    _inherit = 'giscedata.transformador.trafo'

    def _default_tipus(self, cursor, uid, context=None):
        if context is None:
            context = {}

        cat_obj = self.pool.get("giscegis.cataleg.cataleg")

        cataleg_id = context.pop("cataleg", False)
        default_val = False

        if cataleg_id:
            cat_domain = cat_obj.read(
                cursor, uid, cataleg_id, ["domain"]
            )["domain"]
            if cat_domain:
                domain = eval(cat_domain)
                is_reductor = domain[0][2]
                return is_reductor
            else:
                return default_val
        else:
            return default_val

    _defaults = {
        'reductor': lambda self, cr, uid, context=None:
            self._default_tipus(cr, uid, context)
    }


GiscedataTrafo()


class GiscedataCupsEscomesa(osv.osv):
    """
    Extends the model giscedata.cups.escomesa to extend the
    default values of blockname
    """

    _name = 'giscedata.cups.escomesa'
    _inherit = 'giscedata.cups.escomesa'

    def _default_blockname(self, cursor, uid, context=None):
        """
        Returns the default value of blockname

        :param cursor: Database cursor
        :param uid: User id
        :param context: OpenERP context
        :type context: dict(str,object)
        :return: Block id
        :rtype: int
        """

        if context is None:
            context = {}

        cat_obj = self.pool.get("giscegis.cataleg.cataleg")
        blk_obj = self.pool.get('giscedata.cups.blockname')

        cataleg_id = context.pop("cataleg", False)
        default_id = blk_obj.search(cursor, uid, [])[0]

        if cataleg_id:
            cat_domain = cat_obj.read(
                cursor, uid, cataleg_id, ["domain"]
            )["domain"]
            if cat_domain:
                domain = eval(cat_domain)
                search_params = [("name", "=", domain[0][2])]
                return blk_obj.search(
                    cursor, uid, search_params
                )[0]
            else:
                return default_id
        else:
            return default_id

    _defaults = {
        'blockname': lambda self, cr, uid, context=None:
            self._default_blockname(cr, uid, context)
    }


GiscedataCupsEscomesa()


class GiscedataAtDetectors(osv.osv):

    _name = 'giscedata.at.detectors'
    _inherit = 'giscedata.at.detectors'

    def _default_tipus_detector(self, cursor, uid, context=None):
        if context is None:
            context = {}

        cat_obj = self.pool.get("giscegis.cataleg.cataleg")
        tipus_elem_obj = self.pool.get(
            "giscegis.blocs.detectors.blockname"
        )

        cataleg_id = context.pop("cataleg", False)
        default_id = tipus_elem_obj.search(cursor, uid, [])[0]

        if cataleg_id:
            cat_domain = cat_obj.read(
                cursor, uid, cataleg_id, ["domain"]
            )["domain"]
            if cat_domain:
                domain = eval(cat_domain)
                search_params = [("name", "=", domain[0][2])]
                return tipus_elem_obj.search(
                    cursor, uid, search_params
                )[0]
            else:
                return default_id
        else:
            return default_id

    _defaults = {
        'blockname': lambda self, cr, uid, context=None:
        self._default_tipus_detector(cr, uid, context),
    }


GiscedataAtDetectors()


class GiscedataCellesCella(osv.osv):

    _name = 'giscedata.celles.cella'
    _inherit = 'giscedata.celles.cella'

    def _default_tipus_pos(self, cr, uid, context=None):
        if context is None:
            context = {}
        obj = self.pool.get('giscedata.celles.tipus.posicio')
        ids = obj.search(cr, uid, [])
        return ids[0]

    def _default_aillament(self, cr, uid, context=None):
        if context is None:
            context = {}
        obj = self.pool.get('giscedata.celles.aillament')
        ids = obj.search(cr, uid, [])
        return ids[0]

    def _default_tipus_elem(self, cursor, uid, context=None):
        if context is None:
            context = {}

        cat_obj = self.pool.get("giscegis.cataleg.cataleg")
        tipus_elem_obj = self.pool.get("giscedata.celles.tipus.element")

        cataleg_id = context.pop("cataleg", False)
        default_id = tipus_elem_obj.search(cursor, uid, [])[0]

        if cataleg_id:
            cat_domain = cat_obj.read(
                cursor, uid, cataleg_id, ["domain"]
            )["domain"]
            if cat_domain:
                domain = eval(cat_domain)
                search_params = [("codi", "=", domain[0][2])]
                return tipus_elem_obj.search(
                    cursor, uid, search_params
                )[0]
            else:
                return default_id
        else:
            return default_id

    def _default_installacio(self, cr, uid, context=None):
        if context is None:
            context = {}
        obj = self.pool.get('giscedata.cts')
        id = obj.search(cr, uid, [])[0]
        ret = 'giscedata.cts,'+str(id)
        return ret

    def _default_tensio(self, cr, uid, context=None):
        if context is None:
            context = {}
        obj = self.pool.get('giscedata.tensions.tensio')
        id = obj.search(cr, uid, [])[0]
        return id

    _defaults = {
        'tipus_posicio': lambda self, cr, uid, context=None:
            self._default_tipus_pos(cr, uid, context),
        'inventari': lambda *a: 'fiabilitat',
        'aillament': lambda self, cr, uid, context=None:
            self._default_aillament(cr, uid, context),
        'tipus_element': lambda self, cr, uid, context=None:
            self._default_tipus_elem(cr, uid, context),
        'installacio': lambda self, cr, uid, context=None:
            self._default_installacio(cr, uid, context),
        'perc_financament': lambda *a: 100,
        'tensio': lambda self, cr, uid, context=None:
            self._default_tensio(cr, uid, context)
    }


GiscedataCellesCella()


class GiscedataAtEmpalmesGrup(osv.osv):

    _name = "giscedata.at.empalmes.grup"
    _inherit = "giscedata.at.empalmes.grup"

    def _default_tipus_empalme(self, cursor, uid, context=None):

        if context is None:
            context = {}

        cat_obj = self.pool.get("giscegis.cataleg.cataleg")
        blk_obj = self.pool.get("giscegis.blocs.empalmes.grup.blockname")
        cataleg_id = context.pop("cataleg", False)

        ids = blk_obj.search(cursor, uid, [],limit=1)
        default_id = 1
        if ids:
            default_id = ids[0]

        if cataleg_id:
            cat_domain = cat_obj.read(
                cursor, uid, cataleg_id, ["domain"]
            )["domain"]
            if cat_domain:
                domain = eval(cat_domain)
                search_params = [("name", "=", domain[0][2])]
                result = blk_obj.search(cursor, uid, search_params, limit=1)
                if result:
                    return result[0]
                else:
                    return 1
            else:
                return default_id
        else:
            return default_id

    _defaults = {
        'tipus_empalme': lambda self, cr, uid, context=None:
            self._default_tipus_empalme(cr, uid, context)
    }


GiscedataAtEmpalmesGrup()


class GiscegisPuntFrontera(osv.osv):

    _inherit = 'giscedata.punt.frontera'

    def _default_tipus_punt_frontera(self, cursor, uid, context=None):
        """
        Sets the default tipus of a PuntFrontera based on the catalog_id
        from the context, if it's available. If it's not, we suggest as a
        default value the first item of a search for an existing tipus.
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :param context: OpenERP Cursor
        :type context: dict[str, Any]
        :return: The ID of the PuntFronteraTipus that have been computed as a
                 default.
        :rtype int or bool
        """

        if context is None:
            context = {}

        cat_obj = self.pool.get("giscegis.cataleg.cataleg")
        tipus_obj = self.pool.get("giscedata.punt.frontera.tipus")
        cataleg_id = context.pop("cataleg", False)

        default_id = tipus_obj.search(cursor, uid, [])[0]

        if cataleg_id:
            cat_domain = cat_obj.read(
                cursor, uid, cataleg_id, ["domain"]
            )["domain"]
            if cat_domain:
                domain = eval(cat_domain)
                search_params = [("name", "=", domain[0][2])]
                return tipus_obj.search(
                    cursor, uid, search_params
                )[0]
            else:
                return default_id
        else:
            return default_id

    _defaults = {
        'tipus': _default_tipus_punt_frontera
    }


GiscegisPuntFrontera()


class GiscedataBtQuadreElement(osv.osv):

    _name = 'giscedata.bt.quadre.element'
    _inherit = 'giscedata.bt.quadre.element'

    def _default_tipus_bt_quadre_element(self, cursor, uid, context=None):
        """
        Sets the default tipus of a BtQuadreElement based on the catalog_id
        from the context, if it's available. If it's not, we suggest as a
        default value the first item of a search for an existing tipus.
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :param context: OpenERP Cursor
        :type context: dict[str, Any]
        :return: The ID of the BtQuadreElementTipus that have been computed as a
                 default.
        :rtype int or bool
        """

        if context is None:
            context = {}

        cat_obj = self.pool.get("giscegis.cataleg.cataleg")
        tipus_obj = self.pool.get("giscedata.bt.quadre.element.tipus")
        cataleg_id = context.pop("cataleg", False)

        default_id = tipus_obj.search(cursor, uid, [])[0]

        if cataleg_id:
            cat_domain = cat_obj.read(
                cursor, uid, cataleg_id, ["domain"]
            )["domain"]
            if cat_domain:
                domain = eval(cat_domain)
                search_params = [("code", "=", domain[0][2])]
                return tipus_obj.search(
                    cursor, uid, search_params
                )[0]
            else:
                return default_id
        else:
            return default_id

    _defaults = {
        'blockname': _default_tipus_bt_quadre_element
    }


GiscedataBtQuadreElement()


class GiscedataBtSuport(osv.osv):

    _name = 'giscedata.bt.suport'
    _inherit = 'giscedata.bt.suport'

    def _default_tipus_suport_bt(self, cursor, uid, context=None):
        """
        Sets the default tipus of a SuportBt based on the catalog_id
        from the context, if it's available. If it's not, we suggest as a
        default value the first item of a search for an existing tipus.
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :param context: OpenERP Cursor
        :type context: dict[str, Any]
        :return: The ID of the SuportBtTipus that have been computed as a
                 default.
        :rtype int or bool
        """

        if context is None:
            context = {}

        cat_obj = self.pool.get("giscegis.cataleg.cataleg")
        tipus_obj = self.pool.get("giscedata.bt.suport.tipus")
        cataleg_id = context.pop("cataleg", False)

        default_id = tipus_obj.search(cursor, uid, [])[0]

        if cataleg_id:
            cat_domain = cat_obj.read(
                cursor, uid, cataleg_id, ["domain"]
            )["domain"]
            if cat_domain:
                domain = eval(cat_domain)
                search_params = [("code", "=", domain[0][2])]
                return tipus_obj.search(
                    cursor, uid, search_params
                )[0]
            else:
                return default_id
        else:
            return default_id

    _defaults = {
        'tipus': _default_tipus_suport_bt
    }


GiscedataBtSuport()


class GiscedataBtCaixaElement(osv.osv):

    _name = 'giscedata.bt.caixa.element'
    _inherit = 'giscedata.bt.caixa.element'

    def _default_tipus_element_caixa_bt(self, cursor, uid, context=None):
        """
        Sets the default tipus of a BtCaixaElement based on the catalog_id
        from the context, if it's available. If it's not, we suggest as a
        default value the first item of a search for an existing tipus.
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :param context: OpenERP Cursor
        :type context: dict[str, Any]
        :return: The ID of the BtCaixaElementTipus that have been computed as a
                 default.
        :rtype int or bool
        """

        if context is None:
            context = {}

        cat_obj = self.pool.get("giscegis.cataleg.cataleg")
        tipus_obj = self.pool.get("giscedata.bt.caixa.element.tipus")
        cataleg_id = context.pop("cataleg", False)

        default_id = tipus_obj.search(cursor, uid, [])[0]

        if cataleg_id:
            cat_domain = cat_obj.read(
                cursor, uid, cataleg_id, ["domain"]
            )["domain"]
            if cat_domain:
                domain = eval(cat_domain)
                search_params = [("code", "=", domain[0][2])]
                return tipus_obj.search(
                    cursor, uid, search_params
                )[0]
            else:
                return default_id
        else:
            return default_id

    _defaults = {
        'tipus': _default_tipus_element_caixa_bt
    }


GiscedataBtCaixaElement()


class GiscedataBtCaixa(osv.osv):

    _name = 'giscedata.bt.caixa'
    _inherit = 'giscedata.bt.caixa'

    def _default_tipus_caixa_bt(self, cursor, uid, context=None):
        """
        Sets the default tipus of a BtCaixa based on the catalog_id
        from the context, if it's available. If it's not, we suggest as a
        default value the first item of a search for an existing tipus.
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :param context: OpenERP Cursor
        :type context: dict[str, Any]
        :return: The ID of the BtCaixaTipus that have been computed as a
                 default.
        :rtype int or bool
        """

        if context is None:
            context = {}

        cat_obj = self.pool.get("giscegis.cataleg.cataleg")
        tipus_obj = self.pool.get("giscedata.bt.caixa.tipus")
        cataleg_id = context.pop("cataleg", False)

        default_id = tipus_obj.search(cursor, uid, [])[0]

        if cataleg_id:
            cat_domain = cat_obj.read(
                cursor, uid, cataleg_id, ["domain"]
            )["domain"]
            if cat_domain:
                domain = eval(cat_domain)
                search_params = [("code", "=", domain[0][2])]
                return tipus_obj.search(
                    cursor, uid, search_params
                )[0]
            else:
                return default_id
        else:
            return default_id

    _defaults = {
        'tipus': _default_tipus_caixa_bt
    }


GiscedataBtCaixa()


class GiscedataBtElement(osv.osv):

    _name = 'giscedata.bt.element'
    _inherit = 'giscedata.bt.element'

    def _default_municipi(self, cursor, uid, context=None):
        """
        Propose a value for the field municipi using the geom passed on the
        context
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :param context: OpenERP Cursor
        :type context: dict[str, Any]
        :return: The ID of the ResMunicipi where the TramAT starts.
        :rtype int or bool
        """

        if context is None:
            context = {}

        srid = config.get('srid', '25830')
        geom = context.pop("geom", False)

        if geom:
            query = """
                SELECT id FROM res_municipi
                WHERE ST_INTERSECTS(
                    geom,
                    ST_StartPoint(
                        ST_SnapToGrid(
                            ST_Translate(
                               ST_GeomFromText(%(geom)s,%(srid)s),
                               0.0000000001,0.0000000001
                            ),
                            0.0001
                        )
                    )
                )
            """
            cursor.execute(query, {'geom': geom, 'srid': srid})
            query_result = cursor.dictfetchall()
            if query_result:
                return query_result[0]['id']
            else:
                return False
        else:
            return False

    _defaults = {
        'municipi': _default_municipi
    }


GiscedataBtElement()


class GiscedataAtSuport(osv.osv):

    _name = 'giscedata.at.suport'
    _inherit = 'giscedata.at.suport'

    def _default_tipus_suport_at(self, cursor, uid, context=None):
        """
        Sets the default tipus of a SuportAt based on the catalog_id
        from the context, if it's available. If it's not, we suggest as a
        default value the first item of a search for an existing tipus.
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :param context: OpenERP Cursor
        :type context: dict[str, Any]
        :return: The ID of the SuportAtTipus that have been computed as a
                 default.
        :rtype int or bool
        """

        if context is None:
            context = {}

        cat_obj = self.pool.get("giscegis.cataleg.cataleg")
        tipus_obj = self.pool.get("giscegis.at.suport.tipus")
        cataleg_id = context.pop("cataleg", False)

        default_id = tipus_obj.search(cursor, uid, [])[0]

        if cataleg_id:
            cat_domain = cat_obj.read(
                cursor, uid, cataleg_id, ["domain"]
            )["domain"]
            if cat_domain:
                domain = eval(cat_domain)
                search_params = [("name", "=", domain[0][2])]
                return tipus_obj.search(
                    cursor, uid, search_params
                )[0]
            else:
                return default_id
        else:
            return default_id

    _defaults = {
        'tipus': _default_tipus_suport_at
    }


GiscedataAtSuport()


class GiscedataCondensadors(osv.osv):

    _name = 'giscedata.condensadors'
    _inherit = 'giscedata.condensadors'

    def _default_xarxa(self, cursor, uid, context=None):
        """
        Sets the default xarxa of a Condensador based on the catalog_id
        from the context, if it's available. If it's not, we suggest as a
        default value the first item of a search for an existing xarxa.
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :param context: OpenERP Cursor
        :type context: dict[str, Any]
        :return: The xarxa value of the Condensador that have been computed as a
                 default.
        :rtype str or bool
        """

        if context is None:
            context = {}

        cat_obj = self.pool.get("giscegis.cataleg.cataleg")
        cataleg_id = context.pop("cataleg", False)

        # AT by default
        default_xarxa = '1'

        if cataleg_id:
            cat_domain = cat_obj.read(
                cursor, uid, cataleg_id, ["domain"]
            )["domain"]
            if cat_domain:
                domain = eval(cat_domain)
                for clause in domain:
                    if 'xarxa' in clause:
                        default_xarxa = clause[2]

        return default_xarxa

    def _default_tipus(self, cursor, uid, context=None):
        """
        Sets the default tipus of a Condensador based on the catalog_id
        from the context, if it's available. If it's not, we suggest as a
        default value the first item of a search for an existing tipus.
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :param context: OpenERP Cursor
        :type context: dict[str, Any]
        :return: The tipus value of the Condensador that have been computed as a
                 default.
        :rtype str or bool
        """

        if context is None:
            context = {}

        cat_obj = self.pool.get("giscegis.cataleg.cataleg")
        cataleg_id = context.pop("cataleg", False)

        # CONDENSADOR by default
        default_tipus = '2'

        if cataleg_id:
            cat_domain = cat_obj.read(
                cursor, uid, cataleg_id, ["domain"]
            )["domain"]
            if cat_domain:
                domain = eval(cat_domain)
                for clause in domain:
                    if 'tipus' in clause:
                        default_tipus = clause[2]

        return default_tipus

    _defaults = {
        'xarxa': _default_xarxa,
        'tipus': _default_tipus
    }


GiscedataCondensadors()
