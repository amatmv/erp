# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS QGISCE",
    "description": """Módulo virtual para desplegar todas las dependencias de QGISCE""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_base",
        "giscegis_cataleg",
        "giscegis_base_geom",
        "giscegis_cups",
        "giscegis_at_detectors",
        "giscegis_bt",
        "giscegis_trafos",
        "giscegis_at",
        "giscegis_cts",
        "giscegis_cts_subestacions",
        "giscegis_elements_tall",
        "giscegis_topologia",
        "giscegis_empalmes_at",
        "giscegis_punt_frontera",
        "giscegis_network",
        "giscegis_bt_quadre",
        "giscegis_tensio_bloc",
        "giscegis_bt_caixes",
        "giscegis_municipis",
        "giscegis_cts_subestacions",
        "giscegis_simulacio_at",
        "giscegis_simulacio_bt",
        "giscegis_condensadors"
    ],
    "init_xml": [],
    "demo_xml":[],
    "update_xml":[
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
