from destral import testing
from destral.transaction import Transaction


class GiscegisSimulacioBT(testing.OOTestCase):

    def test_stop_nodes(self):
        """
        Tests that BT stop nodes are filled properly
        :return:
        """

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor

            model_obj = self.openerp.pool.get("ir.model")
            sim_obj = self.openerp.pool.get('giscegis.simulacio.bt')
            nbm_obj = self.openerp.pool.get('giscegis.node.bloc.model')
            trf_obj = self.openerp.pool.get('giscedata.transformador.trafo')

            search_params_mod = [
                ("model", "=", "giscedata.transformador.trafo")
            ]

            trf_mdl_id = model_obj.search(cursor, uid, search_params_mod)
            nbm_obj.generate(cursor, uid)
            trf_id = trf_obj.search(cursor, uid, [])[0]
            trf_obj.write(cursor, uid, trf_id, {"reductor": True})

            search_params = [
                ("res_id", "=", trf_id),
                ("model", "=", trf_mdl_id)
            ]
            nbm_id = nbm_obj.search(cursor, uid, search_params)
            node = nbm_obj.read(cursor, uid, nbm_id, ["node"])[0]["node"]

            self.assertNotIn(node, sim_obj.get_stop_nodes(cursor, uid))

    def test_open_nodes_bt(self):
        """
        Tests that open nodes of simulacio BT don't have Nones
        :return:
        """
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor

            sim_obj = self.openerp.pool.get('giscegis.simulacio.bt')
            self.assertNotIn(None, sim_obj.get_open_nodes(cursor, uid))
            self.assertIsNotNone(sim_obj.get_open_nodes(cursor, uid))

    def test_all_conected(self):
        """
        Test simple BT simulation
        :return: None
        """
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            nodes_obj = self.openerp.pool.get('giscegis.nodes')
            vertex_obj = self.openerp.pool.get('giscegis.vertex')
            edge_obj = self.openerp.pool.get('giscegis.edge')
            sim_obj = self.openerp.pool.get('giscegis.simulacio.bt')
            ct_obj = self.openerp.pool.get("giscedata.cts")
            trafo_obj = self.openerp.pool.get("giscedata.transformador.trafo")
            esc_obj = self.openerp.pool.get("giscedata.cups.escomesa")
            nbm_obj = self.openerp.pool.get("giscegis.node.bloc.model")
            bt_obj = self.openerp.pool.get("giscedata.bt.element")

            v1 = vertex_obj.create(cursor, uid, {"name": "v1", "x": 1, "y": 1})
            v2 = vertex_obj.create(cursor, uid, {"name": "v2", "x": 2, "y": 2})
            v3 = vertex_obj.create(cursor, uid, {"name": "v3", "x": 3, "y": 3})

            n1 = nodes_obj.create(cursor, uid, {"name": "n1", "vertex": v1})
            n2 = nodes_obj.create(cursor, uid, {"name": "n2", "vertex": v2})
            n3 = nodes_obj.create(cursor, uid, {"name": "n3", "vertex": v3})

            e1 = edge_obj.create(
                cursor, uid,
                {
                    "name": "e1",
                    "start_node": n1,
                    "end_node": n2,
                }
            )
            e2 = edge_obj.create(
                cursor, uid,
                {
                    "name": "e2",
                    "start_node": n2,
                    "end_node": n3,
                }
            )

            ct_vals = {
                "name": "CT",
                "node_id": n1,
                "id_installacio": 1
            }

            bt_1_vals = {
                "edge_id": e1,
                "tipus_linia": 1,
                "geom": "LineString(1 1, 2 2)"
            }
            bt_obj.create(cursor, uid, bt_1_vals)

            bt_2_vals = {
                "edge_id": e2,
                "tipus_linia": 1,
                "geom": "LineString(2 2, 3 3)"
            }
            bt_obj.create(cursor, uid, bt_2_vals)

            ct = ct_obj.create(cursor, uid, ct_vals)
            trafo_vals = {
                "name": "10",
                "node_id": n2,
                "magatzem_entrada": 1,
                "ct": ct
            }
            trafo_obj.create(cursor, uid, trafo_vals,)

            esc_obj.create(cursor, uid, {"name": "CT", "node_id": n3})

            nbm_obj.generate(cursor, uid)

            ret = sim_obj.simulacio_ids(
                cursor, uid, ct, []
            )

            edges = [e1, e2]
            for edge in edges:
                self.assertIn(edge, ret["trams_on"])
            self.assertEqual(ret["trams_off"], [])

            ret_geom = sim_obj.simulacio_geom(cursor, uid, ct, [], srid=25830)
            geoms = [
                '{"type":"LineString","coordinates":[[2,2],[3,3]]}',
                '{"type":"LineString","coordinates":[[1,1],[2,2]]}'
            ]
            for geom in geoms:
                self.assertIn(geom, ret_geom["trams_on"])

            self.assertEqual(ret_geom["trams_off"], [])

            ret_geom = sim_obj.simulacio_geom(cursor, uid, ct, [n1, n2, n3], srid=25830)
            self.assertEqual(ret_geom["trams_on"], [])
            for geom in geoms:
                self.assertIn(geom, ret_geom["trams_off"])
