# *-* coding: utf-8 *-*
from osv import osv


class GiscegisSimulacioAt(osv.osv):
    _name = "giscegis.simulacio.at"
    _inherit = "giscegis.simulacio.at"

    def simulacio_ids(self, cr, uid, netsource, sim_open_nodes=[], context={},
                      load=False):
        """
        Ens arriba com a paràmetre un Source. Des d'allà creem un subgraf fins
        els nodes oberts de l'estat de la xarxa o els nodes 'stop'
        :param netsource: Punt d'entrada de corrent a la xarxa
        :param sim_open_nodes: llista d'Ids de nodes que estan oberts per la
        simulació
        :type sim_open_nodes: list(int)
        :param load: Diu si la simulació que es farà ve d'una ja guardada
        (carregada) o no, si load=True *no* s'ha de guardar res a la BD. (TODO)
        :type load: bool

        """
        if isinstance(netsource, (list, tuple)):
            netsource = netsource[0]
        # S'ha de guardar els paràmetres de la simulació per
        # poder-la recuperar deprés

        config_obj = self.pool.get('giscegis.configsimulacio.at')
        vals = {
            'name': 'SIMAT',
            'source': netsource
        }

        config_id = config_obj.create(cr, uid, vals, context)
        config_name = vals['name']

        # Un cop tenim l'ID de la configuració, cal guardar la llista de nodes
        # oberts per aquesta simulació

        open_nodes_obj = self.pool.get('giscegis.configsimulacio.at.oberts')
        vals = {'config': config_id}
        node_obj = self.pool.get('giscegis.nodes')
        trafo_obj = self.pool.get("giscedata.transformador.trafo")
        topo_obj = self.pool.get('giscegis.topologia')
        punt_obj = self.pool.get("giscedata.punt.frontera")
        at_escomeses_obj = self.pool.get('giscegis.simulacio.at.resultats.escomeses')
        at_trafos_obj = self.pool.get('giscegis.simulacio.at.resultats.transformadors')
        model_nodes = self.pool.get('giscegis.nodes')
        at_resultats_obj = self.pool.get('giscegis.simulacio.at.resultats')
        esc_obj = self.pool.get("giscedata.cups.escomesa")

        sim_open_nodes_r = node_obj.read(cr, uid, sim_open_nodes, context)
        nodes_ids = [n['id'] for n in sim_open_nodes_r]
        nodes_name = node_obj.get_codi_multi(cr, uid, nodes_ids)
        for n in sim_open_nodes_r:
            vals['name'] = nodes_name[n['id']] or n['name']
            vals['node'] = n['id']
            open_nodes_obj.create(cr, uid, vals)

        # ara creem l'objecte pels resultats
        sql = """
        SELECT count(id) AS n
        FROM giscegis_simulacio_at_resultats
        WHERE id = %s
        """
        cr.execute(sql, (int(config_id),))
        res = cr.dictfetchone()

        vals = {
            'name': '{}_{}'.format(config_name, res['n']+1),
            'simulacio': config_id
        }
        at_resultats_id = at_resultats_obj.create(cr, uid, vals, context)

        # Inicialització
        self.init(cr)
        # Carreguem topologia

        G = topo_obj.load(cr, uid, context)

        trams_on = []
        trams_off = []
        # Agafem el bloc del netsource que volem
        source_br = punt_obj.browse(cr, uid, netsource, context)
        source = node_obj.browse(cr, uid, source_br.node_id.id, context)
        # Fem el subgraf per aquell netsource (canviat des de subgraf_all)
        Gat = self.subgraf(G, source, self.stop_nodes)
        # Fem la continuitat tenint en compte tant els
        # interruptors oberts per la simulació com els STOP (trafos, ...)
        # ens retorna un graf amb els nodes per
        # on s'ha passat (nodes amb corrent)
        Gon = self.dfs_continuitat_2(G, source, sim_open_nodes)
        # Afegit totes les coordenades de les arestes al resultat (trams_on)
        for edge_id in [e[2]["edge_id"] for e in Gon.edges(data=True)]:
            trams_on.append(int(edge_id))
        # Fem una copia del graph on per poder-hi treballar
        Gaux = Gon.copy()
        # Borrem els nodes que siguin frontera del graf
        # amb corrent (nodes oberts)

        Gaux.remove_nodes_from(node_obj.browse(cr, uid, sim_open_nodes, context))
        # Creem una copia de tot el subgraf at
        Goff = Gat.copy()
        # Hi esborrem tots els nodes del graf que hem creat abans
        # (nodes sense corrent) i ens quedara un graf amb els nodes que no en
        # tenen + els nodes oberts (frontera)
        Goff.remove_nodes_from(Gaux.nodes())
        # Afegim totes les coordenades de les arestes al resultat (trams_off)
        for edge_id in [e[2]["edge_id"] for e in Goff.edges(data=True)]:
            trams_off.append(edge_id)

        # guardem resultats d'elements sense corrent:
        # *transformadors* i *escomeses*

        vals = {'simulacio': at_resultats_id}

        nodes_ids = []
        for node in Gon.nodes():
            nodes_ids.append(node.id)
        nodes_model = model_nodes.get_model_multi(cr, uid, nodes_ids)
        escomeses_node_ids = []
        trafos_node_ids = []
        for node_id, model in nodes_model.iteritems():
            if model:
                if model.model == 'giscedata.cups.escomesa':
                    escomeses_node_ids.append(node_id)
                elif model.model == 'giscedata.transformador.trafo':
                    trafos_node_ids.append(node_id)

        escomeses_ids = model_nodes.get_bloc_multi(cr, uid, escomeses_node_ids, ['id'])

        for escomesa_id in escomeses_ids:
            vals['name'] = escomesa_id["id"]
            vals['estat'] = 1
            at_escomeses_obj.create(cr, uid, vals, context)

        trafos_ids = model_nodes.get_bloc_multi(cr, uid, trafos_node_ids, ['id'])
        for trafo_id in trafos_ids:
            vals['name'] = trafo_id["id"]
            vals['estat'] = 1
            at_trafos_obj.create(cr, uid, vals, context)

        escomeses_off = []
        trafos_off = []
        # guardem què s'ha quedat sense corrent, primer el que són nodes
        # trobats pel graf: CONTA-AT i TRAFOS

        nodes_ids = []
        for node in Goff.nodes():
            nodes_ids.append(node.id)
        nodes_model = model_nodes.get_model_multi(cr, uid, nodes_ids)
        for node_id, model in nodes_model.iteritems():
            node.get_model()

            if model:
                if model.model == "giscedata.cups.escomesa":
                    search_params = [("node_id", "=", node_id)]
                    escomesa_ids = esc_obj.search(cr, uid, search_params)
                    if escomesa_ids:
                        escomesa_id = escomesa_ids[0]
                    vals['name'] = escomesa_id
                    vals['estat'] = -1
                    at_escomeses_obj.create(cr, uid, vals, context)
                    escomeses_off.append(escomesa_id)

                elif model.model == 'giscedata.transformador.trafo':
                    search_params = [("node_id", "=", node_id)]
                    trafo_ids = trafo_obj.search(cr, uid, search_params)
                    if trafos_ids:
                        trafo_id = trafo_ids[0]
                    vals['name'] = trafo_id
                    vals['estat'] = -1
                    at_trafos_obj.create(cr, uid, vals, context)
                    trafos_off.append(trafo_id)

        # ara busquem a la BD tots els clients (CONTA-* no -AT)
        # segons la taula de la traçabilitat

        if len(trafos_off) > 0:
            sql = """
                    SELECT DISTINCT(escomesa)
                    FROM giscegis_escomeses_traceability
                    WHERE trafo IN %(ids)s
                    """
            cr.execute(sql, {'ids': tuple(trafos_off)})
            for res in cr.dictfetchall():
                # no hem de posar els possibles CONTA-AT
                # que ja s'han trobat abans!!
                if res['escomesa'] not in escomeses_off:
                    at_escomeses_obj.create(
                            cr, uid, {'name': res['escomesa'],
                                      'estat': -1,
                                      'simulacio': at_resultats_id
                                      })
                    # id's escomeses per retornar
                    if res['escomesa']:
                        escomeses_off.append(res['escomesa'])

        # FIXME quan estigui provat, posar les agregacions dins els bucles
        # que ja tenim adalt i ens estalviarem una passada per totes les
        # escomeses busquem les pòlisses de les escomeses que tenim OFF
        potencia_contractada_off = 0
        num_polisses_off = 0
        model_escomesa = self.pool.get('giscedata.cups.escomesa')
        escomeses = model_escomesa.read(
                cr, uid, escomeses_off,
                ['polisses_actives', 'potencia_polisses']
        )
        for escomesa in escomeses:
            potencia_contractada_off += escomesa['potencia_polisses']
            num_polisses_off += len(escomesa['polisses_actives'])

        return {
            'trams_on': trams_on, 'trams_off': trams_off,
            'escomeses_off': escomeses_off, 'simulacio': at_resultats_id,
            'num_polisses_off': num_polisses_off,
            'potencia_contractada_off': float("%.2f" % potencia_contractada_off)
        }

    def get_nodes(
            self, cursor, uid, netsource=None, state='all', srid=None,
            context=None
    ):
        """
        Funció per retornar tots els nodes d'un subgraf d'at

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param netsource: Id del Source que volem obtenir els nodes, S'IGNORA !!
        :type netsource: int
        :param state: {all, open, closed} Estat en que volem els interruptors
        :type state: str
        :param srid:
        :param context: OpenERP context
        :return:
        """

        list_nodes = []
        ids = []

        if srid is None:
            srid = 4326

        if state == 'closed' or state == 'all':
            nodes_obj = self.pool.get('giscegis.configdefault.at.nodes')
            nodes_ids = nodes_obj.search(cursor, uid, [])
            nodes = nodes_obj.read(cursor, uid, nodes_ids)
            ids += [d['id'] for d in nodes]

        if state == 'open' or state == 'all':
            nodes_obj = self.pool.get('giscegis.configdefault.at')
            nodes_ids = nodes_obj.search(cursor, uid, [])
            nodes = nodes_obj.read(cursor, uid, nodes_ids)
            ids += [d['node'][0] for d in nodes]

        if ids:
            sql = """
                SELECT id AS node, name AS codi, 
                       st_x(st_transform(geom, %(srid)s)) AS x, 
                       st_y(st_transform(geom, %(srid)s)) AS y
                FROM giscegis_nodes
                WHERE id IN %(ids)s
            """
            cursor.execute(sql, {'ids': tuple(ids), 'srid': srid})
            list_nodes = cursor.dictfetchall()

        return list_nodes


GiscegisSimulacioAt()
