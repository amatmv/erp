# GISCE GIS QGISCE

It provides the virtual ERP module for the QGISCE!!

This will autoinstall all QGISCE dependencies at ERP, very useful to deploy a fresh instance of the ERP with just QGISCE-related data.

## How to start a fresh QGISCE ERP?

Just use `destral` to setup the DB with the related demo data:

```
# activate the virtualenv
$ workon erp # or cd $ERP_ROOT && bin/activate

# install destral
$ pip install destral

# update ERP addons symlinks
$ bash tools/link_addons.

# set our DB as "qgisce"
$ export OPENERP_DB_NAME="qgisce"

# pass our virtual module tests generating all demo data
$ destral -m giscegis_qgisce_full
```

