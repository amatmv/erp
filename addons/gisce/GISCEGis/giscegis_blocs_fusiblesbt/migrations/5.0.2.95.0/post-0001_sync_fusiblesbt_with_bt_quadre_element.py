# coding=utf-8

import logging
import pooler


def up(cursor, installed_version):

    logger = logging.getLogger('openerp.migration')

    logger.info(
        'Sync BtQuadreElement with BlocsFusiblesBt'
    )

    pool = pooler.get_pool(cursor.dbname)
    fus_obj = pool.get('giscedata.bt.quadre.element')
    n_created_elements = fus_obj.sync_fus_elements_from_blocs(cursor, 1)

    if n_created_elements == 1:
        msg = '1 BtQuadreElement sync correctly with BlocsFusiblesBt'
    else:
        msg = '{} BtQuadreElements sync correctly with BlocsFusiblesBt'.format(
            n_created_elements
        )

    logger.info(msg)


def down(cursor, installed_version):
    pass


migrate = up
