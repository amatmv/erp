# -*- coding: utf-8 -*-
from osv import osv, fields
from giscegis_base_geom.giscegis_base_geom import DROP_ON_AUTOCAD_DUMP

# Subscribe this model to the list to be updated if there is an AutoCAD dump
DROP_ON_AUTOCAD_DUMP.append('giscegis_blocs_fusiblesbt')

class giscegis_blocs_fusiblesbt_blockname(osv.osv):

    _name = 'giscegis.blocs.fusiblesbt.blockname'
    _description = 'Tipus Blocs FusiblesBT'

    def create(self, cr, uid, vals, context={}):
        ids = self.search(cr, uid, [('name', '=', vals['name'])])
        if ids and len(ids):
            return ids[0]
        else:
            return super(osv.osv, self).create(cr, uid, vals)

    _columns = {
      'name': fields.char('BlockName',size=20,required=True),
      'description': fields.char('Descripció',size=255),
    }

    _defaults = {

    }

    _order = "name, id"

giscegis_blocs_fusiblesbt_blockname()

class giscegis_blocs_fusiblesbt(osv.osv):

    _name = 'giscegis.blocs.fusiblesbt'
    _description = 'Blocs Fusibles BT'
    _columns = {
      'name': fields.char('Name', size=50, required=True),
      'vertex': fields.many2one('giscegis.vertex', 'Vertex',
                                ondelete='set null'),
      'width': fields.float('Width'),
      'height': fields.float('Height'),
      'rotation': fields.float('Rotation'),
      'codi': fields.char('Codi', size=50, select=1),
      'blockname': fields.many2one('giscegis.blocs.fusiblesbt.blockname',
                                   'BlockName'),
      'intensitat': fields.float('Intensitat'),
      'sortida_bt': fields.char('Sortida BT',size=2),
      'node': fields.many2one('giscegis.nodes', 'Node', ondelete='set null'),
    }

    _defaults = {

    }

    _order = "name, id"

    def create(self, cursor, uid, vals, context=None):
        """Mètode sobreescrit per escriure als camps many2one des
        d'aquí.

        Ens hem d'assegurar de què els mètodes create() dels objectes
        referenciats estiguin alhora sobreescrits per tal què comprovin
        si l'objecte existeix o no."""
        if vals.has_key('sortida_bt'):
            if vals['sortida_bt'][0] == 'L':
                vals['sortida_bt'] = vals['sortida_bt'][1:]

        _fields = [('vertex', 'giscegis.vertex'),
                   ('blockname', 'giscegis.blocs.fusiblesbt.blockname')]
        for field in _fields:
            if field[0] in vals and isinstance(vals[field[0]], list):
                obj = self.pool.get(field[1])
                remote_id = obj.create(cursor, uid, vals[field[0]][0][2])
                if remote_id:
                    vals[field[0]] = remote_id
        return super(osv.osv, self).create(cursor, uid, vals)

    def write(self, cursor, uid, ids, vals, context=None):
        """Mètode sobreescrit per escriure als camps many2one des d'aquí.

        Igual que amb el create() cal assabentar-se de què els write()s dels
        models referenciats estiguin sobreescrits 
        """
        # Aquí validem el camp sortida_bt
        if vals.has_key('sortida_bt'):
            if vals['sortida_bt'][0] == 'L':
                vals['sortida_bt'] = vals['sortida_bt'][1:]

        _fields = [('vertex', 'giscegis.vertex'),
                   ('blockname', 'giscegis.blocs.fusiblesbt.blockname')]
        for field in _fields:
            if field[0] in vals and isinstance(vals[field[0]], list):
                obj = self.pool.get(field[1])
                search_params = []
                for k, v in vals[field[0]][0][2].items():
                    search_params.append((k, '=', v))
                remote_id = obj.search(cursor, uid, search_params)
                if remote_id:
                    vals[field[0]] = remote_id[0]
                else:
                    remote_id = obj.create(cursor, uid, vals[field[0]][0][2])
                    vals[field[0]] = remote_id
        return super(osv.osv, self).write(cursor, uid, ids, vals)

giscegis_blocs_fusiblesbt()
