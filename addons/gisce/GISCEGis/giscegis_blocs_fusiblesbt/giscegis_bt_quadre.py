# -*- coding: utf-8 -*-
from osv import osv
from tools import config
import logging
from giscegis_base_geom.wizard.giscegis_shp_loader \
    import GIS_MODELS_TO_UPDATE_GEOM_DATA


# Subscribe this model to the list to be updated if there is an AutoCAD dump
GIS_MODELS_TO_UPDATE_GEOM_DATA.append(
    {
        'model': 'giscedata.bt.quadre.element',
        'functions': ['sync_fus_elements_from_blocs']
    },
)


class GiscedataBtQuadreElement(osv.osv):

    _name = 'giscedata.bt.quadre.element'
    _inherit = 'giscedata.bt.quadre.element'

    def sync_fus_elements_from_blocs(self, cursor, uid):
        """
        Syncronize the BtQuadreElement  with BlocsFusiblesBt
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return: Return the number of elements syncronized.
        :rtype: int
        """

        fus_obj = self.pool.get('giscedata.bt.quadre.element')
        fus_tipus_obj = self.pool.get('giscedata.bt.quadre.element.tipus')
        ct_obj = self.pool.get('giscedata.cts')
        trafo_obj = self.pool.get('giscedata.transformador.trafo')
        logger = logging.getLogger(
            'openerp.giscedata.bt.quadre.element.sync_fus_elements_from_blocs'
        )

        sql = """
            SELECT b.name, b.rotation, b.codi, b.node, b.sortida_bt, 
                b.intensitat, bn.name AS blockname, st_astext(
                    st_setsrid(st_makepoint(v.x, v.y), %(srid)s)
                ) AS geom
            FROM giscegis_blocs_fusiblesbt b
            LEFT JOIN giscegis_vertex v ON b.vertex = v.id
            LEFT JOIN giscegis_blocs_fusiblesbt_blockname bn 
                ON b.blockname = bn.id
        """

        cursor.execute(sql, {"srid": config.get("srid", 25830)})

        rows = cursor.dictfetchall()
        row_n = len(rows)

        for row in rows:
            # Buscar tipus a partir del blockname
            tipus_id = fus_tipus_obj.search(
                cursor, uid, [('code', '=', row['blockname'])]
            )
            if tipus_id:
                tipus_id = tipus_id[0]
            else:
                logger.warning(
                    "Type of GiscedataBtQuadreElement '{}' not found. Skiping "
                    "the creation of the element with code {}".format(
                        row['blockname'], row['codi']
                    )
                )
                continue

            # Buscar CTS if exist.
            tmp = row['codi'].split('-')
            if tmp:
                codi_ct = "%[]%".format(tmp[1])
            else:
                codi_ct = ""
            ct_id = ct_obj.search(
                cursor, uid, [('name', 'like', codi_ct)]
            )
            if ct_id:
                ct_id = ct_id[0]
            else:
                ct_id = None

            # Si hem trobat CT, buscar Trafo. Només ens val si en te 1 i prou.
            trafo_id = None
            if ct_id:
                trafo_id = trafo_obj.search(
                    cursor, uid, [('ct', '=', ct_id)]
                )
            if trafo_id and len(trafo_id) == 1:
                trafo_id = trafo_id[0]

            # Crear els valor de l'element a crear
            vals = {
                'node_id': row['node'],
                'sortida_bt': row['sortida_bt'],
                'codi': row['codi'],
                'intensitat': row['intensitat'],
                'rotation': 360-row['rotation'],
                'geom': row['geom'],
                'blockname': tipus_id,
                'ct_id': ct_id,
                'trafo_id': trafo_id
            }
            fus_ids = fus_obj.search(
                cursor, uid, [('codi', '=', row['codi'])]
            )
            if fus_ids:
                fus_obj.write(cursor, uid, fus_ids[0], vals)
            else:
                fus_obj.create(cursor, uid, vals)

        return row_n


GiscedataBtQuadreElement()
