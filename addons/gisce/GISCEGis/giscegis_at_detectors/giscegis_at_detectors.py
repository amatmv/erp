# *-* coding: utf-8 *-*
from osv import fields, osv
from giscegis_base_geom.fields import Point
from giscegis_base_geom.giscegis_session import SessionManaged
from tools import config


class GiscegisBlocsDetectorsBlockname(osv.osv):

    _name = 'giscegis.blocs.detectors.blockname'
    _description = 'Tipus Detectors AT'

    def create(self, cr, uid, vals, context=None):
        # This create checks if the element that is about to be created already
        # exists to avoid duplicated types of Detectors AT
        if context is None:
            context = {}

        ids = self.search(cr, uid, [('name', '=', vals['name'])])

        if ids and len(ids):
            return ids[0]
        else:
            return super(osv.osv, self).create(cr, uid, vals)

    _columns = {
      'name': fields.char('Tipus Detector AT', size=20, required=True),
      'description': fields.char('Descripció', size=255),
    }

    _defaults = {

    }

    _order = "name, id"


GiscegisBlocsDetectorsBlockname()


class GiscedataAtDetectors(SessionManaged):

    _name = 'giscedata.at.detectors'
    _inherit = 'giscedata.at.detectors'

    _columns = {
        'geom': Point("Geometria", srid=config.get('srid', 25830),select=True),
        'rotation': fields.integer("Rotació"),
        'blockname': fields.many2one('giscegis.blocs.detectors.blockname',
                                     'Tipus Detector AT'),
    }

    _defaults = {
        'rotation': lambda *a: 0,
    }


GiscedataAtDetectors()
