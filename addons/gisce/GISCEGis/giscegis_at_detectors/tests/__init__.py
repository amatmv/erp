# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
import ast
from .test_session import *


class TramsDetectorsAtGeomTest(testing.OOTestCase):

    def test_catalog_elements_detectors_at(self):
        """
        Test that are elements for Detectors AT and how many of each type are
        searching with the catalog domain.
        :return: None
        """

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            # INICIALITZEM OBJECTES
            detectors_at_obj = self.openerp.pool.get('giscedata.at.detectors')
            cataleg_obj = self.openerp.pool.get('giscegis.cataleg.cataleg')
            sum_detectors_at = 0

            # SELECCIONEM UNICAMENT ELS ELEMENTS DE CATALEG QUE SIGUIN
            # DETECTORS AT
            ids_detectors_at_cataleg = cataleg_obj.search(
                cursor, uid, [('model.model', '=', 'giscedata.at.detectors')]
            )

            # PER A CADA ELEMENT DEL CATALEG COMPROVAREM QUE HI HAGIN ELEMENTS
            cataleg_detectors_at_elems = cataleg_obj.read(
                cursor, uid, ids_detectors_at_cataleg, ['name', 'domain']
            )

            for cataleg_elem in cataleg_detectors_at_elems:

                detectors_at_ids = detectors_at_obj.search(
                    cursor, uid, ast.literal_eval(cataleg_elem['domain'])
                )

                sum_detectors_at += len(detectors_at_ids)
                if cataleg_elem['name'] == "NORTROLL":
                    self.assertEqual(len(detectors_at_ids), 9)

            self.assertEqual(sum_detectors_at, 9)
