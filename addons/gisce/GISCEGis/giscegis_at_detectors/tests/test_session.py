from destral import testing
from destral.transaction import Transaction


__all__ = [
    'TestDetectorsAtSession'
]


class TestDetectorsAtSession(testing.OOTestCase):

    def setUp(self):
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user

    def tearDown(self):
        self.txn.stop()

    def test_search_session_detectors_at(self):
        """
        This test is for check the correct behaviour of the sessions and the
        Detectors AT
        """
        # Load ERP models
        detectors_at_obj = self.openerp.pool.get('giscedata.at.detectors')
        session_obj = self.openerp.pool.get('giscegis.session')

        # Clean the sessions to avoid BBox collisions
        sql = """
            TRUNCATE giscegis_session CASCADE
        """
        self.cursor.execute(sql)

        # Create the sessions
        session1_vals = {
            "name": 'Testing session 1',
            "bbox": 'POLYGON((982449.3 4664776.0, 982760.4 4664776.0, '
                    '982760.4 4664950.2, 982449.3 4664950.2, '
                    '982449.3 4664776.0))',
            "state": 'open',
            "ttl": 500000000,
            "active": True
        }
        session2_vals = {
            "name": 'Testing session 2',
            "bbox": 'POLYGON((981216.7 4664152.6, 981771.6 4664152.6, '
                    '981771.6 4664463.2, 981216.7 4664463.2, '
                    '981216.7 4664152.6))',
            "state": 'open',
            "ttl": 500000000,
            "active": True
        }
        session1_id = session_obj.create(self.cursor, self.uid, session1_vals)
        session2_id = session_obj.create(self.cursor, self.uid, session2_vals)

        # Start the checks
        # Check the number of Detectors AT in differents sessions and without
        # sessions
        # No session
        detectors_at_ids = detectors_at_obj.search(self.cursor, self.uid, [])
        self.assertEqual(len(detectors_at_ids), 9)
        # Session 1
        detectors_at_ids = detectors_at_obj.search(
            self.cursor, self.uid, [], context={'session': session1_id}
        )
        self.assertEqual(len(detectors_at_ids), 0)
        # Session 2
        detectors_at_ids = detectors_at_obj.search(
            self.cursor, self.uid, [], context={'session': session2_id}
        )
        self.assertEqual(len(detectors_at_ids), 6)

        # Get 2 ids of the Session 2
        detector1_id = detectors_at_ids[0]
        detector2_id = detectors_at_ids[1]

        # Check that the changes made on a session are only visible from the
        # session
        # Write new data on 2 Detectors AT from session 2
        detectors_vals_to_write = {
            'rotation': 240
        }
        detectors_at_obj.write(
            self.cursor, self.uid, [detector1_id, detector2_id],
            detectors_vals_to_write, context={'session': session2_id}
        )
        # Check the changes outside the session
        detectors_at_data = detectors_at_obj.read(
            self.cursor, self.uid, [detector1_id, detector2_id], ['rotation']
        )
        for data in detectors_at_data:
            self.assertEqual(data['rotation'], 0)
        # Check the changes inside the session
        detectors_at_data = detectors_at_obj.read(
            self.cursor, self.uid, [detector1_id, detector2_id], ['rotation'],
            context={'session': session2_id}
        )
        for data in detectors_at_data:
            self.assertEqual(data['rotation'], 240)
