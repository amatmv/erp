# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS DETECTORS AT",
    "description": """Modul que exten els detectors d'AT per adaptar-los al GIS""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscedata_at_detectors",
        "giscegis_base",
        "giscegis_base_geom",
        "giscegis_cataleg",
        "giscegis_at"
    ],
    "init_xml": [],
    "demo_xml": [
        "giscegis_at_detectors_demo.xml"
    ],
    "update_xml":[
        "giscegis_cataleg_at_detectors_data.xml",
        "giscegis_at_detectors_blockname_data.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
