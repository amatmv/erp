# coding=utf-8

import logging


def up(cursor, installed_version):

    logger = logging.getLogger('openerp.migration')
    logger.info('Update the IrModelData module reference for the model '
                'giscegis.blocs.detectors')

    sql = """
        UPDATE ir_model_data
        SET module = 'giscegis_at_detectors'
        WHERE name LIKE '%%giscegis_blocs_detectors_blockname%%'
        AND module = 'giscegis_blocs_detectors'
    """

    cursor.execute(sql)

    logger.info('Module reference changed from giscegis_blocs_detectors to '
                'giscegis_at_detectors')


def down(cursor, installed_version):
    pass


migrate = up
