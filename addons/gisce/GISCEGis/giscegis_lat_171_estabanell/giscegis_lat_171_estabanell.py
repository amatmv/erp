# *-* coding: utf-8 *-*

from osv import osv, fields


class GiscegisLat171Estabanell(osv.osv):

    _name = 'giscegis.lat.171.estabanell'
    _auto = False
    _columns = {
        'id': fields.integer('Id', readonly=True),
        'texte': fields.text('Texte', readonly=True),
    }

    def init(self, cr):
        cr.execute("""drop view if exists giscegis_lat_171_estabanell""")
        cr.execute("""
          create or replace view giscegis_lat_171_estabanell as (
    select 
    att.name, 'AT ' || coalesce(lat.name, 'S/Linia')  || '\n' ||
    coalesce(cab.name, 'S/Cable') || ' ; ' || coalesce(mat.name, 'S/Material') || ' ; ' || coalesce(cab.aillament, 'S/Aillament') || '\n' ||
    coalesce(
    (
    SELECT coalesce(e.empresa, 'S/Exp') || ' ' || coalesce(e.industria_data::text, 'S/Data Exp')
    FROM giscedata_expedients_expedient e, giscedata_at_tram_expedient r
    WHERE r.tram_id = att.id AND r.expedient_id = e.id
    ORDER BY e.industria_data IS NOT NULL, e.industria_data DESC
    LIMIT 1
    ), 'S/Exp S/Data Exp') || '\n' ||
    coalesce(round(att.longitud_cad::numeric,2), 0) || 'm' as texte,
    case
    when 38000 <= lat.tensio and lat.tensio <= 42000
      then
      (
        case
        when tipus.codi = 'D' or tipus.codi = 'T' then 'A-40000'
        when tipus.codi = 'S' then 'S-40000'
        when tipus.codi = 'E' then 'EAT'
        else '-40000'
        end
      )
    when 23750 <= lat.tensio and lat.tensio <= 26250
      then
      (
        case
        when tipus.codi = 'D' or tipus.codi = 'T' then 'A-25000'
        when tipus.codi = 'S' then 'S-25000'
        when tipus.codi = 'E' then 'EAT'
        else '-25000'
        end
      )
    when 19000 <= lat.tensio and lat.tensio <= 21500
      then
      (
        case
        when tipus.codi = 'D' or tipus.codi = 'T' then 'A-20000'
        when tipus.codi = 'S' then 'S-20000'
        when tipus.codi = 'E' then 'EAT'
        else '-20000'
        end
      )
    when 4750 <= lat.tensio and lat.tensio <= 5250
      then
      (
        case
        when tipus.codi = 'D' or tipus.codi = 'T' then 'A-5000'
        when tipus.codi = 'S' then 'S-5000'
        when tipus.codi = 'E' then 'EAT'
        else '-5000'
        end
      )
    when 2850 <= lat.tensio and lat.tensio <= 3150
      then
      (
        case
        when tipus.codi = 'D' or tipus.codi = 'T' then 'A-3000'
        when tipus.codi = 'S' then 'S-3000'
        when tipus.codi = 'E' then 'EAT'
        else '-3000'
        end
      )
    end
    as theme

    from giscedata_at_tram att
    left join giscedata_at_linia lat on (att.linia = lat.id and lat.name != 1::char)
    left join giscedata_at_cables cab on (att.cable = cab.id)
    left join giscedata_at_material mat on (cab.material = mat.id)
    left join giscedata_at_tipuscable tipus on (cab.tipus = tipus.id)

    order by att.name

          )""")

GiscegisLat171Estabanell()


class giscegis_lat_171_circuits_estabanell(osv.osv):

    _name = 'giscegis.lat.171.circuits.estabanell'
    _auto = False
    _columns = {
      'id': fields.integer('Id', readonly=True),
      'texte': fields.text('Texte', readonly=True),
      'theme': fields.text('Tema', readonly=True),
    }

    def init(self, cr):
        cr.execute("""drop view if exists giscegis_lat_171_circuits_estabanell""")
        cr.execute("""
          create or replace view giscegis_lat_171_circuits_estabanell as (
    select
    att.name, 'AT ' || coalesce(lat.name, 'S/Linia')  || '\n' ||
    coalesce(cab.name, 'S/Cable') || ' ; ' || coalesce(mat.name, 'S/Material') || ' ; ' || coalesce(cab.aillament, 'S/Aillament') || '\n' ||
    coalesce(
    (
    SELECT coalesce(e.empresa, 'S/Exp') || ' ' || coalesce(e.industria_data::text, 'S/Data Exp')
    FROM giscedata_expedients_expedient e, giscedata_at_tram_expedient r
    WHERE r.tram_id = att.id AND r.expedient_id = e.id
    ORDER BY e.industria_data IS NOT NULL, e.industria_data DESC
    LIMIT 1
    ), 'S/Exp S/Data Exp') || '\n' ||
    coalesce(round(att.longitud_cad::numeric,2), 0) || 'm' as texte,
    (
      case
      when tipus.codi = 'D' or tipus.codi = 'T' then 'A-' || COALESCE(att.circuits::varchar, '0')
      when tipus.codi = 'S' or tipus.codi = 'E' then 'S-' || COALESCE(att.circuits::varchar, '0')
      else '-' || COALESCE(att.circuits::varchar, '0')
      end
    ) as theme

    from giscedata_at_tram att
    left join giscedata_at_linia lat on (att.linia = lat.id and lat.name != '1')
    left join giscedata_at_cables cab on (att.cable = cab.id)
    left join giscedata_at_material mat on (cab.material = mat.id)
    left join giscedata_at_tipuscable tipus on (cab.tipus = tipus.id)

    order by att.name

          )""")

giscegis_lat_171_circuits_estabanell()


class giscegis_lat_171_tensio_disseny_estabanell(osv.osv):

    _name = 'giscegis.lat.171.tensio.disseny.estabanell'
    _auto = False
    _columns = {
      'id': fields.integer('Id', readonly=True),
      'texte': fields.text('Texte', readonly=True),
      'theme': fields.text('Tema', readonly=True),
    }

    def init(self, cr):
        cr.execute("""drop view if exists giscegis_lat_171_tensio_disseny_estabanell""")
        cr.execute("""
          create or replace view giscegis_lat_171_tensio_disseny_estabanell as (
    select
    att.name, 'AT ' || coalesce(lat.name, 'S/Linia')  || '\n' ||
    coalesce(cab.name, 'S/Cable') || ' ; ' || coalesce(mat.name, 'S/Material') || ' ; ' || coalesce(cab.aillament, 'S/Aillament') || '\n' ||
    coalesce(
    (
    SELECT coalesce(e.empresa, 'S/Exp') || ' ' || coalesce(e.industria_data::text, 'S/Data Exp')
    FROM giscedata_expedients_expedient e, giscedata_at_tram_expedient r
    WHERE r.tram_id = att.id AND r.expedient_id = e.id
    ORDER BY e.industria_data IS NOT NULL, e.industria_data DESC
    LIMIT 1
    ), 'S/Exp S/Data Exp') || '\n' ||
    coalesce(round(att.longitud_cad::numeric,2), 0) || 'm' as texte,
    COALESCE(att.tensio_max_disseny::varchar, '0') as theme

    from giscedata_at_tram att
    left join giscedata_at_linia lat on (att.linia = lat.id and lat.name != '1')
    left join giscedata_at_cables cab on (att.cable = cab.id)
    left join giscedata_at_material mat on (cab.material = mat.id)
    left join giscedata_at_tipuscable tipus on (cab.tipus = tipus.id)

    order by att.name

          )""")

giscegis_lat_171_tensio_disseny_estabanell()
