# -*- coding: iso-8859-1 -*-
from osv import osv, fields
from giscegis_base_geom.giscegis_base_geom import DROP_ON_AUTOCAD_DUMP

# Subscribe this model to the list to be updated if there is an AutoCAD dump
DROP_ON_AUTOCAD_DUMP.append('giscegis_blocs_transformadors_reductors')


class giscegis_blocs_transformador_reductor_blockname(osv.osv):

    _name = 'giscegis.blocs.transformador.reductor.blockname'
    _description = 'Tipus Blocs Transformador Reductor'

    def create(self, cr, uid, vals, context={}):
        ids = self.search(cr, uid, [('name', '=', vals['name'])])
        if ids and len(ids):
            return ids[0]
        else:
            return super(osv.osv, self).create(cr, uid, vals)

    _columns = {
      'name': fields.char('BlockName',size=20,required=True),
      'description': fields.char('Descripció',size=255),
    }

    _defaults = {

    }

    _order = "name, id"

giscegis_blocs_transformador_reductor_blockname()

class giscegis_blocs_transformadors_reductors(osv.osv):


    def write(self, cr, uid, ids, vals, context={}):
        """
        Sobreescrivim el mètode write per comprovar camps i
        escriure'ls correctament a la bbdd.
        """
        if vals.has_key('ordre_transformador_ct'):
            if vals['ordre_transformador_ct'][0] == 'T':
                vals['ordre_transformador_ct'] = int(vals['ordre_transformador_ct'][1:])
        return super(osv.osv, self).write(cr, uid, ids, vals, context)


    def create(self, cr, uid, vals, context={}):
        """
        Sobreescrivim el mètode write per comprovar camps i
        escriure'ls correctament a la bbdd.
        """
        _fields = [('vertex', 'giscegis.vertex'),
                   ('blockname',
                    'giscegis.blocs.transformador.reductor.blockname')]
        for field in _fields:
            if field[0] in vals and isinstance(vals[field[0]], list):
                obj = self.pool.get(field[1])
                remote_id = obj.create(cr, uid, vals[field[0]][0][2])
                if remote_id:
                    vals[field[0]] = remote_id

        if vals.has_key('ordre_transformador_ct'):
            # Posem l'ordre com un 'integer'
            if vals['ordre_transformador_ct'][0] == 'T':
                vals['ordre_transformador_ct'] = int(vals['ordre_transformador_ct'][1:])
        # Busquem el ct (giscedata.cts) que li toca segons el codi_ct
        if vals.has_key('codi_ct') and not vals.has_key('ct'):
            ct_ids = self.pool.get('giscedata.cts').search(cr, uid, [('name', '=', vals['codi_ct'])], context)
            if ct_ids and len(ct_ids):
                vals['ct'] = ct_ids[0]
        # Busquem el trafo que li toca segons el ct i l'ordre en aquest ct
        if vals.has_key('ct') and vals.has_key('ordre_transformador_ct') and not vals.has_key('transformadors'):
            trafo_ids = self.pool.get('giscedata.transformador.trafo').search(cr, uid,
                [('ct', '=', vals['ct']), ('ordre_dins_ct', '=', vals['ordre_transformador_ct'])])
            if trafo_ids and len(trafo_ids):
                vals['transformadors'] = trafo_ids[0]
        return super(osv.osv, self).create(cr, uid, vals, context)

    _name = 'giscegis.blocs.transformadors.reductors'
    _description = 'Blocs Transformadors Reductors'
    _columns = {
      'name': fields.char('BlockName', size=50, required=True),
      'vertex': fields.many2one('giscegis.vertex', 'Vertex',
                                ondelete='set null'),
      'width': fields.float('Width'),
      'height': fields.float('Height'),
      'rotation': fields.float('Rotation'),
      'codi_ct': fields.char('Codi CT', size=30),
      'ordre_transformador_ct': fields.integer('Ordre'),
      'ct': fields.many2one('giscedata.cts', 'CT'),
      'transformadors': fields.many2one(
          'giscedata.transformador.trafo', 'Transformador'
      ),
      'tensio': fields.integer('Tensio'),
      'blockname': fields.many2one(
            'giscegis.blocs.transformador.reductor.blockname', 'BlockName'
      ),
      'node': fields.many2one('giscegis.nodes', 'Node', ondelete='set null'),
    }

    _defaults = {

    }

    _order = "name, id"


giscegis_blocs_transformadors_reductors()
