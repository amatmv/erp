from osv import fields, osv
from tools.config import config


class GiscedataTransformadorTrafo(osv.osv):

    _name = 'giscedata.transformador.trafo'
    _inherit = 'giscedata.transformador.trafo'

    def fill_geom_red(self, cursor, uid):
        """
        Function that runs a query to update the field geom of all the
        Transformadors Reductors
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """

        sql = """
            UPDATE giscedata_transformador_trafo AS trafo 
            SET geom = bloc.geom 
            FROM (
              SELECT transformadors AS id,
                     st_setsrid(st_makepoint(vertex.x, vertex.y), %(srid)s) AS geom
              FROM giscegis_blocs_transformadors_reductors trafo
              LEFT JOIN giscegis_vertex vertex ON trafo.vertex = vertex.id
            ) AS bloc
            WHERE trafo.id=bloc.id
        """

        cursor.execute(sql, {"srid": config.get("srid", 25830)})

    def fill_rotation_red(self, cursor, uid):
        """
        Function that runs a query to update the field rotation of all the
        Transformadors Reductors
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """

        sql = """
            UPDATE giscedata_transformador_trafo AS trafo 
            SET rotation = bloc.rotation 
            FROM (
              SELECT transformadors AS id,
                     (360-rotation) AS rotation
              FROM giscegis_blocs_transformadors_reductors trafo
            ) AS bloc
            WHERE trafo.id=bloc.id
        """

        cursor.execute(sql, {"srid": config.get("srid", 25830)})

    def fill_node_id_red(self, cursor, uid):
        """
        Function that runs a query to update the field node_id of all the
        Transformadors Reductors
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """

        sql = """
            UPDATE giscedata_transformador_trafo AS trafo 
            SET node_id = bloc.node
            FROM (
              SELECT transformadors AS id,
                     node AS node
              FROM giscegis_blocs_transformadors_reductors trafo
            ) AS bloc
            WHERE trafo.id=bloc.id
        """

        cursor.execute(sql, {"srid": config.get("srid", 25830)})


GiscedataTransformadorTrafo()
