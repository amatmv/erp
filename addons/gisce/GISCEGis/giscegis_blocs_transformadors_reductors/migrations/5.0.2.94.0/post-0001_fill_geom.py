# coding=utf-8
import logging
import pooler

def migrate(cursor, installed_version):
    """
    Fills the geom field of giscedata.transformador.trafo

    :param cursor: Database cursor
    :param installed_version: Installed version
    :return: None
    """

    loger = logging.Logger("openerp.migration")
    loger.info("Omplint camp geom de transformadors reductors.")
    pool = pooler.get_pool(cursor.dbname)
    trafo_obj = pool.get("giscedata.transformador.trafo")
    trafo_obj.fill_geom_red(cursor, 1)
    loger.info("Camp geom de transformadors reductors omplert")


up = migrate
