# coding=utf-8
import logging
import pooler


def migrate(cursor, installed_version):

    loger = logging.Logger("openerp.migration")
    loger.info("Omplint camp node_id de transformadors reductors.")

    pool = pooler.get_pool(cursor.dbname)
    trafo_obj = pool.get("giscedata.transformador.trafo")
    trafo_obj.fill_node_id_red(cursor, 1)

    loger.info("Camp node_id de transformadors reductors omplert")


up = migrate
