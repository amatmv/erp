# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Blocs Transformadors Reductors",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscedata_cts",
        "giscedata_transformadors",
        "giscegis_nodes",
        "giscegis_vertex",
        "giscegis_base",
        "giscegis_trafos"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscegis_blocs_transformadors_reductors_view.xml",
        "ir.model.access.csv",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
