# -*- encoding: utf-8 -*-
from osv import osv, fields


class ResUsers(osv.osv):
    _name = "res.users"
    _inherit = "res.users"

    def get_allowed_change_fields(self, cursor, uid, context=None):
        base_fields = super(ResUsers, self).get_allowed_change_fields(
            cursor, uid, context=context
        )
        return base_fields + [
            'last_location', 'webgis_base_layer'
        ]

    def _get_available_base_layers(self, cr, uid, context=None):
        """
        This function compute the available values for the selection field
        that match the desired criteria. The layers must have the flag is_base
        on True
        :param cr: Database cursor
        :type cr: Psycopg2 cursor
        :param uid: User ID
        :type uid: int
        :param context: OpenERP context
        :type context: dict[str, Any]
        :return: The available layers to be choosed on this field.
        :rtype: list of (str, str)
        """
        if context is None:
            context = {}

        layers_obj = self.pool.get('giscegis.layers')
        base_layers_ids = layers_obj.search(
            cr, uid, [('is_base', '=', True)], context=context
        )
        base_layers_data = layers_obj.read(
            cr, uid, base_layers_ids, ['webgis_code', 'description'], context
        )

        ret = [
            (data['webgis_code'], data['description'])
            for data in base_layers_data
        ]

        ret.sort(key=lambda x: x[1])

        return ret

    _columns = {
        'last_location': fields.text("Localització"),
        'webgis_base_layer': fields.selection(
            _get_available_base_layers, 'Capa Base per defecte al Webgis'
        )
    }

    _defaults = {
        'last_location': lambda *args: False,
    }


ResUsers()
