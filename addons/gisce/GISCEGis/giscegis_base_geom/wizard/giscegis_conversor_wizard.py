# -*- coding: utf-8 -*-
from osv import osv, fields

SelectionsSrid = [('23029', 'ED50 UTM 29'), ('23030', 'ED50 UTM 30'),
                  ('23031', 'ED50 UTM 31'), ('4258', 'ETRS89 Geo'),
                  ('25829', 'ETRS89 UTM 29'), ('25830', 'ETRS89 UTM 30'),
                  ('25831', 'ETRS89 UTM 31'), ('4326', 'WGS84')]


class GiscegisConversor(osv.osv_memory):

    _name = 'giscegis.conversor'

    def convert(self, cursor, uid, ids, context=None):
        wizard = self.browse(cursor, uid, ids[0], context)
        origin_srid = wizard.origin_srid
        destination_srid = wizard.destination_srid

        x = wizard.x
        y = wizard.y
        if not x:
            x = 0.0
        if not y:
            y = 0.0

        sql = """SELECT
        ST_X(ST_TRANSFORM(ST_SETSRID(ST_MAKEPOINT({0},{1}),{2}),
        {3})),ST_Y(ST_TRANSFORM(ST_SETSRID(ST_MAKEPOINT({0},{1}),{2}),{3}))
        """

        cursor.execute(sql.format(x, y, origin_srid, destination_srid))
        data = cursor.fetchall()
        xf = data[0][0]
        yf = data[0][1]

        wizard.write({'xf': str(xf), 'yf': str(yf), 'state': 'done'})

    _columns = {
        'x': fields.float('Longitud original', digits=(16, 8)),
        'y': fields.float('Latitud original', digits=(16, 8)),
        'xf': fields.char('Longitud final', size=16),
        'yf': fields.char('Latitud final', size=16),
        'origin_srid': fields.selection(
            SelectionsSrid, string='Projeccio Origen', help='SRID'),
        'destination_srid': fields.selection(
            SelectionsSrid, string='Projeccio Destí', help='SRID'),
        'state': fields.char('State', size=4)
    }

    _defaults = {
        'state': lambda *a: 'init',
        'origin_srid': lambda *a: '23030',
        'destination_srid': lambda *a: '23031',
    }
GiscegisConversor()
