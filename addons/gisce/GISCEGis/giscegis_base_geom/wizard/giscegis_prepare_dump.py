# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _


class GiscegisPrepareUpdate(osv.osv_memory):
    """
    Wizard to prepare Giscegis data update
    """
    _name = 'giscegis.prepare.update'

    def _prepare_mode_update(self, cursor, uid, ids, context=None):
        """
        Handles the update button of the wizard

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: Id of the wizard
        :type ids: int
        :param context: OpenERP context
        :type context: dict
        :return: None
        :rtype; None
        """
        obj_gis = self.pool.get("giscegis.base.geom")
        failed = obj_gis.prepare_update(cursor, uid, context)
        if failed:
            msg = _(
                "El procés no s'ha pogut completar. Les següents taules no han"
                " estat netejades: {}.\nTorna a executar l'assistent i si el "
                "problema persisteix posat en contacte amb el suport "
                "tècnic de l'ERP.".format(failed)
            )
        else:
            msg = _(
                    "Procés realitzat correctament.  Les dades GIS han estat "
                    "eliminades i el sistema esta a punt per a realitzar un "
                    "nou bolcat."
                )

        self.write(
            cursor, uid, ids,
            {
                'info': msg,
                'state': 'done'
            }
        )

    _columns = {
        'state': fields.char('State', size=4),
        'info': fields.text('Informació')
    }

    _defaults = {
        'state': lambda *a: 'init',
        'info': lambda *a: _("A punt per a iniciar el procés."),
    }


GiscegisPrepareUpdate()
