# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscegisLastDump(osv.osv_memory):

    _name = 'giscegis.last.dump'

    def _fill_wizard(self, cursor, uid, ids,  context=None):
        """
        Fills the last dump data on the wizard

        :param cursor: Database cursor
        :param uid: User id
        :param ids: Wizard ids
        :param context: OpenERP context
        :return: None
        """

        wizard = self.browse(cursor, uid, ids[0], context)
        sql = """
        SELECT GREATEST(create_date, write_date)::date
        FROM giscegis_nodes
        LIMIT 1;
        """
        cursor.execute(sql)
        result_nodes = cursor.fetchall()
        if result_nodes:
            result_nodes = result_nodes[0]
            wizard_data = {'last_dump': result_nodes[0]}
            wizard.write(wizard_data)


    _columns = {
        'last_dump': fields.char(string='Ultim bolcat', readonly=True, size=30),
        'state': fields.char('State', size=4)
    }

    _defaults = {
        'state': lambda *a: 'init',
        'last_dump': ''
    }
GiscegisLastDump()
