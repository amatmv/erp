# -*- encoding: utf-8 -*-
from osv import osv, fields
import base64
import os
from zipfile import ZipFile, BadZipfile
from StringIO import StringIO

import shapefile
from tools.translate import _
import logging
from tools.misc import email_send
from tools import config


# MODELS TO UPDATE THE GEOM AND ROTATION FIELDS
# Subscribed model format:
# [
#   {
#       'model': 'giscedata.cups.escomesa',
#       'functions': ['fill_geom', 'fill_rotation']
#   },
# ]
GIS_MODELS_TO_UPDATE_GEOM_DATA = []


class GiscegisSHPLoader(osv.osv_memory):
    _name = 'giscegis.shp.loader'

    def to_wkt_multilinestring(self, line):
        points = []
        for point in line[0]:
            points.append(' '.join(map(str, point)))
        return 'MULTILINESTRING(({0}))'.format(','.join(points))

    def load_shp(self, cursor, uid, shp_data, dbf_data, table):

        sf = shapefile.Reader(shp=shp_data, dbf=dbf_data)
        shapes = sf.shapes()
        records = sf.records()
        srid = config.get('srid', 25830)
        insert_sql = '''INSERT INTO {0}
                     VALUES (%s, %s, ST_GeomFromText(%s, %s))'''.format(table)
        fields = {}
        for index, field in enumerate(sf.fields):
            if field[0] != 'DeletionFlag':
                fields[field[0]] = index - 1
        for index, shape in enumerate(shapes):
            wkt_geometry = self.to_wkt_multilinestring([shape.points])
            identificador = int(records[index][fields['ID']])
            cursor.execute(
                insert_sql, (index, identificador, wkt_geometry, srid))

    def set_lbt_type(self, cursor, uid, shp_data, dbf_data):
        obj_bt = self.pool.get('giscedata.bt.element')
        obj_tipus = self.pool.get('giscedata.bt.tipuslinia')
        sf = shapefile.Reader(shp=shp_data, dbf=dbf_data)
        records = sf.records()
        fields = {}

        id_aerea_poste = obj_tipus.search(
            cursor, uid, [('name', '=like', 'A%P%')])[0]
        id_aerea_fachada = obj_tipus.search(
            cursor, uid, [('name', '=like', 'A%F%')])[0]
        id_subterranea = obj_tipus.search(
            cursor, uid, [('name', '=like', 'S%')])[0]
        for index, field in enumerate(sf.fields):
            if field[0] != 'DeletionFlag':
                fields[field[0]] = index - 1

        for index, element in enumerate(records):

            if element[fields['TIPOLINEA']] == 'Continuous':
                name = element[fields['ID']]
                identificador = obj_bt.search(
                    cursor, uid, [('name', '=', name)]
                )
                if identificador:
                    tipus_act = obj_bt.read(cursor, uid, identificador[0],['tipus_linia'])['tipus_linia'][1].upper()
                    if 'A' != tipus_act[0]:
                        # TIPUS Sub o cualquier otro no Aereo
                        obj_bt.write(
                            cursor, uid, identificador,
                            {'tipus_linia': id_aerea_poste})
            elif element[fields['TIPOLINEA']] == 'TRAZOS':
                name = element[fields['ID']]
                identificador = obj_bt.search(
                    cursor, uid, [('name', '=', name)])
                if identificador:
                    tipus_act = obj_bt.read(cursor, uid, identificador[0],['tipus_linia'])['tipus_linia'][1].upper()
                    if 'S' != tipus_act[0]:
                        #No es subterrani, ha de prevaldre el tipus del dibuix
                        obj_bt.write(
                            cursor, uid, identificador,
                            {'tipus_linia': id_subterranea})

    def import_data_shp(self, cursor, uid, ids, context=None):
        """
        Loads the data of the wizard

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: Wizard id
        :type ids: int
        :param context: OpenERP context
        :type context: dict
        :return: None
        :rtype: None
        """
        logger = logging.getLogger('openerp.SHPLoader')

        obj_users = self.pool.get("res.users")
        obj_msg = self.pool.get("giscegis.load.message")
        obj_config = self.pool.get("res.config")

        from_email = obj_config.get(
            cursor, uid,
            "email_from", "no-reply@gisce.net"
        )

        message_id = obj_msg.create(cursor, uid, {})
        message = obj_msg.browse(cursor, uid, message_id)
        message.start_load()

        user_data = obj_users.browse(cursor, uid, uid)
        user_email = user_data.address_id.email

        if user_email:
            email_send(
                from_email,
                [user_email],
                _("Carrega de fitxers LAT i LBT"),
                message.get_message(),
                debug=True
            )
        else:
            info_msg = "El usuari amb uid {} no te adreça de correu " \
                       "assignada".format(uid)
            logger.info(info_msg)
        base_geom = self.pool.get('giscegis.base.geom')
        wizard = self.browse(cursor, uid, ids[0], context)
        wizard.write({'status': ''})

        if wizard.file_lat:
            data_lat = base64.decodestring(wizard.file_lat)
        else:
            logger.info(_("No s'ha especificat el fitxer de LAT"))
            message.log_message(_("No s'ha especificat el fitxer de LAT"))
            wizard = self.browse(cursor, uid, ids[0], None)
            base_geom.add_status(
                cursor, uid, ids, _("No s'ha especificat el fitxer de LAT"),
                wizard
            )

        if wizard.file_lbt:
            data_lbt = base64.decodestring(wizard.file_lbt)
        else:
            logger.info(_("No s'ha especificat el fitxer de LBT"))
            message.log_message(_("No s'ha especificat el fitxer de LBT"))
            wizard = self.browse(cursor, uid, ids[0], None)
            base_geom.add_status(
                cursor, uid, ids, _("No s'ha especificat el fitxer de LBT"),
                wizard
            )

        if wizard.file_lat and wizard.file_lbt:
            srid = config.get('srid', 25830)
            sql_create_lat = """
            CREATE TABLE giscegis_lat_tmp(
              gid INTEGER,
              id DOUBLE PRECISION
            );
            SELECT AddGeometryColumn(
            'public','giscegis_lat_tmp',
            'geom',%(srid)s,'geometry',2, true);
            """
            sql_create_lbt = """
            CREATE TABLE giscegis_lbt_tmp(
              gid INTEGER,
              id DOUBLE PRECISION
            );
            SELECT AddGeometryColumn(
            'public','giscegis_lbt_tmp',
            'geom',%(srid)s,'geometry',2, true);

            """
            cursor.execute(sql_create_lat, {"srid": srid})
            cursor.execute(sql_create_lbt, {"srid": srid})
            data_table = {
                data_lat: 'giscegis_lat_tmp',
                data_lbt: 'giscegis_lbt_tmp'
            }
            data_in_files = True
            for (data, table) in data_table.iteritems():
                s = StringIO(data)
                try:
                    input_zip = ZipFile(s)
                except BadZipfile:
                    if data == data_lat:
                        logger.info(_('Fitxer LAT corrupte'))
                        message.log_message(_('Fitxer LAT corrupte'))
                        wizard = self.browse(cursor, uid, ids[0], None)
                        base_geom.add_status(
                            cursor, uid, ids, _('Fitxer LAT corrupte'),
                            wizard
                        )
                    else:
                        logger.info(_('Fitxer LBT corrupte'))
                        message.log_message(_('Fitxer LBT corrupte'))
                        message.end_load(_("Error"))
                        email_send(
                            from_email,
                            [user_email],
                            _("Carrega de fitxers LAT i LBT"),
                            message.get_message(),
                            debug=True
                        )
                        wizard = self.browse(cursor, uid, ids[0], None)
                        base_geom.add_status(
                            cursor, uid, ids, _('Fitxer LBT corrupte'),
                            wizard
                        )
                    return

                filenames = sorted(input_zip.namelist())
                if filenames:
                    if data_in_files:
                        data_in_files = True

                    base_geom.truncate_table(cursor, uid, table)
                    shp_data = None
                    dbf_data = None
                    for filename in filenames:
                        if os.path.splitext(filename)[1].lower() == '.dbf':
                            dbf_data = StringIO(input_zip.read(filename))
                        if os.path.splitext(filename)[1].lower() == '.shp':
                            shp_data = StringIO(input_zip.read(filename))
                        if shp_data and dbf_data:
                            self.load_shp(
                                cursor, uid, shp_data, dbf_data, table)
                            if table == 'giscegis_lbt_tmp':
                                self.set_lbt_type(
                                    cursor, uid, shp_data, dbf_data)
                            shp_data = None
                            dbf_data = None
                else:
                    if data == data_lat:
                        logger.info(
                            _("No s'han trobat fitxers .shp o .dbf en el fitxer"
                              " de LAT,s'atura l'importacio")
                        )
                        message.log_message(
                            _("No s'han trobat fitxers .shp o .dbf en el fitxer"
                              " de LAT,s'atura l'importacio")
                        )
                        wizard = self.browse(cursor, uid, ids[0], None)
                        base_geom.add_status(
                            cursor, uid, ids,
                            _("No s'han trobat fitxers .shp o .dbf en el fitxer"
                              " de LAT,s'atura l'importacio"),
                            wizard)
                    else:
                        logger.info(
                            _("No s'han trobat fitxers .shp o .dbf en el fitxer"
                              " de LBT,s'atura l'importacio")
                        )
                        message.log_message(
                            _("No s'han trobat fitxers .shp o .dbf en el fitxer"
                              " de LBT,s'atura l'importacio")
                        )
                        wizard = self.browse(cursor, uid, ids[0], None)
                        base_geom.add_status(
                            cursor, uid, ids,
                            _("No s'han trobat fitxers .shp o .dbf en el fitxer"
                              " de LBT,s'atura l'importacio"),
                            wizard)
                    data_in_files = False

            if data_in_files:
                logger.info(
                    _('Dades carregades correctament')
                )
                base_geom.switch_table(
                    cursor, uid, "giscegis_lat_tmp", "giscegis_lat", context
                )
                base_geom.switch_table(
                    cursor, uid, "giscegis_lbt_tmp", "giscegis_lbt", context
                )
                message.log_message(_('Dades carregades correctament'))
                wizard = self.browse(cursor, uid, ids[0], None)
                base_geom.add_status(
                    cursor, uid, ids, _('Dades carregades correctament'),
                    wizard
                )
                # The following function will fill all the geometric fields of
                # the GIS models
                self.fill_all_models_geom(cursor, 1)
                status_views = base_geom.recreate_gis_views(cursor, uid)
                status_cache_at = base_geom.remove_tms_cache(cursor, uid, 'at')
                status_cache_bt = base_geom.remove_tms_cache(cursor, uid, 'bt')
                status_cache_nodes = base_geom.remove_tms_cache(
                    cursor, uid, 'nodes'
                )
                # Recreate NBM if it's possible
                nbm_model = "giscegis_node_bloc_model"
                if self.module_installed(cursor, uid, nbm_model):
                    nbm_obj = self.pool.get('giscegis.node.bloc.model')
                    nbm_obj.generate(cursor, uid)

                if status_views:
                    logger.info(_('Vistes recreades correctament'))
                    message.log_message(_('Vistes recreades correctament'))
                    wizard = self.browse(cursor, uid, ids[0], None)
                    base_geom.add_status(
                        cursor, uid, ids, _('Vistes recreades correctament'),
                        wizard
                    )
                else:
                    logger.info(_('Cap vista recreada'))
                    message.log_message(_('Cap vista recreada'))
                    wizard = self.browse(cursor, uid, ids[0], None)
                    base_geom.add_status(
                        cursor, uid, ids, _('Cap vista recreada'),
                        wizard
                    )
                if status_cache_at and status_cache_bt and status_cache_nodes:
                    logger.info(_('Cache netejat correctament'))
                    message.log_message(_('Cache netejat correctament'))
                    message.end_load(_("Correcte"))
                    wizard = self.browse(cursor, uid, ids[0], None)
                    base_geom.add_status(
                        cursor, uid, ids, _('Cache netejat correctament'),
                        wizard
                    )
                else:
                    logger.info(_('Error al eliminar el cache'))
                    message.log_message(_('Error al eliminar el cache'))
                    message.end_load(_("Error"))
                    wizard = self.browse(cursor, uid, ids[0], None)
                    base_geom.add_status(
                        cursor, uid, ids, _('Error al eliminar el cache'),
                        wizard
                    )

            if user_email:
                email_send(
                    from_email,
                    [user_email],
                    _("Carrega de fitxers LAT i LBT"),
                    message.get_message(),
                    debug=True
                )
            else:
                info_msg = "El usuari amb uid {} no te adreça de correu " \
                           "assignada".format(uid)
                logger.info(info_msg)

    def module_installed(self, cursor, uid, module):
        ir_obj = self.pool.get('ir.module.module')
        id_module = ir_obj.search(cursor, uid, [('name', '=', module)])
        if id_module:
            return ir_obj.read(
                cursor, uid, id_module, ['state'])[0]['state'] == 'installed'
        else:
            return False

    def _get_default_url(self, cursor, uid, context=None):
        """
        Return the URL of the web loader form

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param context: OpenERP context
        :type context: dict
        :return: URL
        :rtype: str
        """

        url = self.pool.get("res.config").get(cursor, uid, "giscegis_gis_url")
        return "{}/load/atbt".format(url)

    def recreate_gis_views(self, cursor, uid):
        """
        Recreates the GIS materialized views

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :return: True if recreated
        :rtype: bool
        """
        gis_modules = [
            'giscegis_adudsp_geom',
            'giscegis_blocs_suports_at_geom',
            'giscegis_blocs_tensio_geom',
            'giscegis_blocs_trafos_reductors_geom',
            'giscegis_trafos_geom',
            'giscegis_caixabt_geom',
            'giscegis_cts_geom',
            'giscegis_cups_escomeses_tensio_geom',
            'giscegis_fusibls_caixes_geom',
            'giscegis_interruptorsat_geom',
            'giscegis_interruptorsbt_geom',
            'giscegis_lat_geom',
            'giscegis_lbt_geom',
            'giscegis_netsource_geom',
            'giscegis_seccionadorat_geom',
            'giscegis_nodes_geom'
        ]
        recreated = False
        for module in gis_modules:
            if self.module_installed(cursor, uid, module):
                mod_obj = self.pool.get(module.replace('_', '.'))
                mod_obj.recreate(cursor, uid)
                recreated = True
        return recreated

    def fill_all_models_geom(self, cursor, uid):
        """
        The following function will fill all the geometric fields of the GIS
        models subscribed on the list GIS_MODELS_TO_UPDATE_GEOM_DATA
        :param cursor: OpenERP DB cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        """

        logger = logging.getLogger('SHPLoader')
        logger.info(_('Filling all the GIS models geometric fields.'))
        # For each module on the list we run the functions that fill the
        # geometric fields of the models.
        for model in GIS_MODELS_TO_UPDATE_GEOM_DATA:
            mod_obj = self.pool.get(model['model'])
            if mod_obj is None:
                error_msg = _(
                    "The model {} is not currently installed but it's "
                    "subscribed to the list GIS_MODELS_TO_UPDATE_GEOM_DATA"
                )
                logger.warning(error_msg.format(model['model']))
                continue
            # Each function of the subscribed model will be executed
            for function in model['functions']:
                if hasattr(mod_obj, function):
                    fnct = getattr(mod_obj, function)
                    fnct(cursor, uid)

    _columns = {
        'file_lat': fields.binary('Fixer LAT', help=()),
        'file_lbt': fields.binary('Fixer LBT', help=()),
        'status': fields.text(_('Resultat')),
        "url": fields.text(_("URL"))
    }

    _defaults = {
        'state': lambda *a: 'init',
        "url": _get_default_url
    }


GiscegisSHPLoader()
