# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscegisModeUpdate(osv.osv_memory):

    _name = 'giscegis.mode.update'

    def _get_current_mode(self, cursor, uid, context=None):
        """
        Fills the last dump data on the wizard

        :param cursor: Database cursor
        :param uid: User id
        :param ids: Wizard ids
        :param context: OpenERP context
        :return: None
        """
        conf_obj = self.pool.get('res.config')

        return int(conf_obj.get(cursor, uid, 'mode_update_gis')) == 1

    def change_mode_update(self, cursor, uid, ids, context=None):
        """
        Changes the mode update

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: Id of the wizard
        :type ids: int
        :param context: OpenERP context
        :type context: dict
        :return:None
        :rtype: None
        """

        conf_obj = self.pool.get('res.config')
        wiz = self.browse(cursor, uid, ids[0])
        mode = (int(wiz.mode_update) != 1)
        if mode:
            conf_obj.set(cursor, uid, 'mode_update_gis', '1')
        else:
            conf_obj.set(cursor, uid, 'mode_update_gis', '0')
        return True

    _columns = {
        'mode_update': fields.boolean("Mode update"),
        'state': fields.char('State', size=4)
    }

    _defaults = {
        'state': lambda *a: 'init',
        'mode_update': _get_current_mode
    }
GiscegisModeUpdate()
