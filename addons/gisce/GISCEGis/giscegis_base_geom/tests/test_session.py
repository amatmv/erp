# coding=utf-8
from __future__ import absolute_import
from osv import osv, fields
from giscegis_base_geom import fields as geo_fields
from giscegis_base_geom.giscegis_session import (
    SessionManaged, BoundingBoxReservedException,
    ResourceOutOfBboxSessionException
)
from destral import testing
from destral.transaction import Transaction
import logging


logger = logging.getLogger(__name__)


__all__ = [
    'TestSessionManager',
    'TestSession',
    'TestSessionManagerInherits',
    'TestSessionManagerReference',
    'BaseSessionTestsFunction'
]


# Simple Points Tests
class PointModelBaseTest(osv.osv):
    _name = "point.model.test"
    _columns = {
        "name": fields.char("Name", size=64),
        "active": fields.boolean('Active')
    }

    _defaults = {
        "active": lambda *a: True
    }


class PointModelTest(SessionManaged):
    _name = "point.model.test"
    _inherit = "point.model.test"
    _columns = {
        "geom": geo_fields.Point("geom", srid=25830),
    }


class BaseSessionTests(testing.OOTestCase):

    def setUp(self):
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user
        self.create_test_point_model()
        self.session_id = self.create_session()

    def tearDown(self):
        self.txn.stop()

    def create_session(self, values=None):
        session_obj = self.openerp.pool.get('giscegis.session')
        if values is None:
            values = {
                'name': 'Test Session',
                'bbox': 'POLYGON((0 0, 10 0, 10 10, 0 10, 0 0))'
            }
        return session_obj.create(self.cursor, self.uid, values)

    def test_reject(self):
        """
        Tests that the session reject deletes all the dirty session changes
        :return: None
        """

        session_obj = self.openerp.pool.get('giscegis.session')
        changes_obj = self.openerp.pool.get('giscegis.session.changes')

        values = {
            'name': 'Test Session dirty',
            'bbox': 'POLYGON((11 11, 21 11, 21 21, 11 21, 11 11))'
        }
        session2_id = self.create_session(values)
        ids = changes_obj.search(self.cursor,self.uid,[("session_id","=",session2_id)])
        self.assertEqual(len(ids), 0)
        changes_obj.create(self.cursor, self.uid, {
            "session_id": session2_id,
            "res_model": "",
            "res_id": 1,
            "data": {},
            "cataleg_id": 1,
            "status": "A",
            "dirty": False

        })
        changes_obj.create(self.cursor, self.uid, {
            "session_id": session2_id,
            "res_model": "",
            "res_id": 1,
            "data": {},
            "cataleg_id": 1,
            "status": "A",
            "dirty": True
        })
        session_obj.reject_changes(
            self.cursor, self.uid,
            session2_id
        )
        ids_dirty = changes_obj.search(
            self.cursor, self.uid,
            [
                ("session_id", "=", session2_id),
                ("dirty", "=", True),
            ]
        )
        ids_no_dirty = changes_obj.search(
            self.cursor, self.uid,
            [
                ("session_id", "=", session2_id),
                ("dirty", "=", False),
            ]
        )
        self.assertEqual(len(ids_dirty), 0)
        self.assertEqual(len(ids_no_dirty), 1)

    def test_reject_cataleg(self):
        """
        Tests that the session reject deletes all the dirty session changes
        :return: None
        """

        session_obj = self.openerp.pool.get('giscegis.session')
        changes_obj = self.openerp.pool.get('giscegis.session.changes')

        values = {
            'name': 'Test Session dirty',
            'bbox': 'POLYGON((11 11, 21 11, 21 21, 11 21, 11 11))'
        }
        session2_id = self.create_session(values)
        ids = changes_obj.search(self.cursor,self.uid, [("session_id","=",session2_id)])
        self.assertEqual(len(ids), 0)
        changes_obj.create(self.cursor, self.uid, {
            "session_id": session2_id,
            "res_model": "",
            "res_id": 1,
            "data": {},
            "cataleg_id": 1,
            "status": "A",
            "dirty": False

        })
        changes_obj.create(self.cursor, self.uid, {
            "session_id": session2_id,
            "res_model": "",
            "res_id": 1,
            "data": {},
            "cataleg_id": 1,
            "status": "A",
            "dirty": True
        })

        changes_obj.create(self.cursor, self.uid, {
            "session_id": session2_id,
            "res_model": "",
            "res_id": 1,
            "data": {},
            "cataleg_id": 2,
            "status": "A",
            "dirty": False

        })
        changes_obj.create(self.cursor, self.uid, {
            "session_id": session2_id,
            "res_model": "",
            "res_id": 1,
            "data": {},
            "cataleg_id": 2,
            "status": "A",
            "dirty": True
        })

        session_obj.reject_changes(
            self.cursor, self.uid,
            session2_id, context={"cataleg_id": 2}
        )
        ids_dirty = changes_obj.search(
            self.cursor, self.uid,
            [
                ("session_id", "=", session2_id),
                ("dirty", "=", True),
                ("cataleg_id", "=", 2),
            ]
        )
        ids_no_dirty = changes_obj.search(
            self.cursor, self.uid,
            [
                ("session_id", "=", session2_id),
                ("dirty", "=", False),
                ("cataleg_id", "=", 2),
            ]
        )
        self.assertEqual(len(ids_dirty), 0)
        self.assertEqual(len(ids_no_dirty), 1)

    def test_confirm(self):
        """
        Tests that the session confirm sets the dirty session changes to False
        :return: None
        """
        session_obj = self.openerp.pool.get('giscegis.session')
        changes_obj = self.openerp.pool.get('giscegis.session.changes')

        values = {
            'name': 'Test Session dirty',
            'bbox': 'POLYGON((11 11, 21 11, 21 21, 11 21, 11 11))'
        }
        session2_id = self.create_session(values)
        ids = changes_obj.search(self.cursor, self.uid, [("session_id", "=", session2_id)])
        self.assertEqual(len(ids), 0)
        changes_obj.create(self.cursor, self.uid, {
            "session_id": session2_id,
            "res_model": "",
            "res_id": 1,
            "data": {},
            "cataleg_id": 1,
            "status": "A"
        })
        ids = changes_obj.search(
            self.cursor, self.uid,
            [
                ("session_id", "=", session2_id),
                ("dirty", "=", True),
             ]
        )
        self.assertEqual(len(ids), 1)
        session_obj.confirm_changes(self.cursor, self.uid, session2_id)
        ids = changes_obj.search(
            self.cursor, self.uid,
            [
                ("session_id", "=", session2_id),
                ("dirty", "=", False),
            ]
        )
        self.assertEqual(len(ids), 1)
        ids = changes_obj.search(
            self.cursor, self.uid,
            [
                ("session_id", "=", session2_id),
                ("dirty", "=", True),
            ]
        )
        self.assertEqual(len(ids), 0)


    def test_confirm_cataleg(self):
        """
        Tests that the session confirm sets the dirty session changes to False
        :return: None
        """
        session_obj = self.openerp.pool.get('giscegis.session')
        changes_obj = self.openerp.pool.get('giscegis.session.changes')

        values = {
            'name': 'Test Session dirty',
            'bbox': 'POLYGON((11 11, 21 11, 21 21, 11 21, 11 11))'
        }
        session_id = self.create_session(values)
        ids = changes_obj.search(self.cursor, self.uid, [("session_id", "=", session_id)])
        self.assertEqual(len(ids), 0)
        changes_obj.create(self.cursor, self.uid, {
            "session_id": session_id,
            "res_model": "",
            "res_id": 1,
            "data": {},
            "cataleg_id": 1,
            "status": "A"
        })
        changes_obj.create(self.cursor, self.uid, {
            "session_id": session_id,
            "res_model": "",
            "res_id": 1,
            "data": {},
            "cataleg_id": 2,
            "status": "A"
        })

        ids = changes_obj.search(
            self.cursor, self.uid,
            [
                ("session_id", "=", session_id),
                ("dirty", "=", True),
             ]
        )
        self.assertEqual(len(ids), 2)
        session_obj.confirm_changes(self.cursor, self.uid, session_id, {"cataleg_id": 1})
        ids = changes_obj.search(
            self.cursor, self.uid,
            [
                ("session_id", "=", session_id),
                ("dirty", "=", False),
                ("cataleg_id", "=", 1),
            ]
        )
        self.assertEqual(len(ids), 1)
        ids = changes_obj.search(
            self.cursor, self.uid,
            [
                ("session_id", "=", session_id),
                ("dirty", "=", True),
            ]
        )
        self.assertEqual(len(ids), 1)

    def create_test_point_model(self):
        PointModelBaseTest()
        osv.class_pool['point.model.test'].createInstance(
            self.openerp.pool, 'giscegis_base_geom', self.cursor
        )
        point_obj = self.openerp.pool.get('point.model.test')
        point_obj._auto_init(self.cursor)
        PointModelTest()
        osv.class_pool['point.model.test'].createInstance(
            self.openerp.pool, 'giscegis_base_geom', self.cursor
        )
        point_obj = self.openerp.pool.get('point.model.test')
        point_obj._auto_init(self.cursor)

    def create_points(self):
        point_obj = self.openerp.pool.get('point.model.test')
        created_points = [
            point_obj.create(self.cursor, self.uid, {
                "name": "Point 1",
                "geom": "POINT (5 5)"
            }),
            point_obj.create(self.cursor, self.uid, {
                "name": "Point 2",
                "geom": "POINT (2 5)"
            }),
            point_obj.create(self.cursor, self.uid, {
                "name": "Point 3",
                "geom": "POINT (3 7)"
            }),
            point_obj.create(self.cursor, self.uid, {
                "name": "Point 4",
                "geom": "POINT (8 8)"
            }),
            point_obj.create(self.cursor, self.uid, {
                "name": "Point 5",
                "geom": "POINT (11 11)"
            })
        ]
        return created_points


class TestSessionManager(BaseSessionTests):

    def test_search_during_session_adds_bbox_filter(self):
        point_obj = self.openerp.pool.get('point.model.test')
        ids = point_obj.search(self.cursor, self.uid, [])
        point_obj.unlink(self.cursor, self.uid, ids)
        self.create_points()

        results = point_obj.search(self.cursor, self.uid, [], context={
            'session': self.session_id
        })
        self.assertEqual(len(results), 4)

    def test_search_without_session_not_uses_bbox_filter(self):
        point_obj = self.openerp.pool.get('point.model.test')
        ids = point_obj.search(self.cursor, self.uid, [])
        point_obj.unlink(self.cursor, self.uid, ids)
        self.create_points()

        results = point_obj.search(self.cursor, self.uid, [])
        self.assertEqual(len(results), 5)

    def test_create_has_catalog_id(self):
        """
        Tests that creating an elemnt with "cataleg" in context it's saved on
        the catalog_ic column
        :return: None
        """

        cat_obj = self.openerp.pool.get('giscegis.cataleg.cataleg')
        mod_obj = self.openerp.pool.get('ir.model')
        id_mod = mod_obj.search(
            self.cursor, self.uid,
            [("name", "=", 'point.model.test')]
        )[0]
        id_cat = cat_obj.create(
            self.cursor, self.uid,
            {"name":"test_cat",
             "model": id_mod,
             "domain": "",
             "geom_type": "Point"


            })
        point_obj = self.openerp.pool.get( 'point.model.test')
        point_vals = {
            "name": "Point 4",
            "geom": "POINT (8 8)"
        }
        point_obj.create(
            self.cursor, self.uid, point_vals,
            context={
                'session': self.session_id,
                'cataleg': id_cat
            }
        )
        check_sql = """
        SELECT count(*) FROM giscegis_session_changes
        WHERE cataleg_id =%s;
        """
        self.cursor.execute(check_sql, (id_cat, ))
        data = self.cursor.fetchall()
        self.assertEquals(data[0][0], 1)



    def test_create_during_session_writes_to_session_and_returns_id(self):
        point_obj = self.openerp.pool.get('point.model.test')
        session_obj = self.openerp.pool.get('giscegis.session')
        point_vals = {
            "name": "Point 4",
            "geom": "POINT(8 8)"
        }
        res_id = point_obj.create(
            self.cursor, self.uid, point_vals,
            context={'session': self.session_id}
        )
        session = session_obj.browse(self.cursor, self.uid, self.session_id)
        self.assertEqual(len(session.changes), 1)

        changes = session.changes[0]

        self.assertEqual(changes.data, point_vals)
        self.assertEqual(changes.res_id, res_id)
        self.assertEqual(changes.res_model, 'point.model.test')

        record = point_obj.read(self.cursor, self.uid, res_id, context={
            'session': self.session_id
        })
        self.assertEqual(record['geom'], point_vals['geom'])
        self.assertEqual(record['name'], point_vals['name'])

        results = point_obj.search(self.cursor, self.uid, [('id', '=', res_id)])
        self.assertEqual(len(results), 0)

    def test_create_without_session_creates_the_object(self):
        point_obj = self.openerp.pool.get('point.model.test')
        session_obj = self.openerp.pool.get('giscegis.session')
        point_vals = {
            "name": "Point 4",
            "geom": "POINT(8 8)"
        }
        res_id = point_obj.create(
            self.cursor, self.uid, point_vals,
        )
        session = session_obj.browse(self.cursor, self.uid, self.session_id)
        self.assertEqual(len(session.changes), 0)

        results = point_obj.search(self.cursor, self.uid, [('id', '=', res_id)])
        self.assertEqual(len(results), 1)

    def test_write_during_session_writes_to_session(self):
        point_obj = self.openerp.pool.get('point.model.test')
        session_obj = self.openerp.pool.get('giscegis.session')
        original_point_vals = {
            "name": "Point 4",
            "geom": "POINT(8 8)"
        }
        res_id = point_obj.create(self.cursor, self.uid, original_point_vals)

        modified_point_vals = {
            "geom": "POINT(7 7)",
            "name": None
        }
        point_obj.write(
            self.cursor, self.uid, [res_id], modified_point_vals,
            context={'session': self.session_id}
        )

        expected_point_vals = {
            "geom": "POINT(7 7)",
            "name": False
        }

        session = session_obj.browse(self.cursor, self.uid, self.session_id)
        self.assertEqual(len(session.changes), 1)

        changes = session.changes[0]

        self.assertEqual(changes.data, expected_point_vals)
        self.assertEqual(changes.res_id, res_id)
        self.assertEqual(changes.res_model, 'point.model.test')

        record = point_obj.read(self.cursor, self.uid, res_id, context={
            'session': self.session_id
        })
        self.assertEqual(record['geom'], modified_point_vals['geom'])

        # Serialize None -> False
        self.assertIs(record['name'], False)

        modified_point_vals2 = {
            "geom": "POINT(5 5)",
            "name": None
        }

        expected_point_vals_point_vals2 = {
            "geom": "POINT(5 5)",
            "name": False
        }
        point_obj.write(
            self.cursor, self.uid, [res_id], modified_point_vals2,
            context={'session': self.session_id}
        )
        session = session_obj.browse(self.cursor, self.uid, self.session_id)
        self.assertEqual(len(session.changes), 2)

        changes = session.changes[1]

        self.assertEqual(changes.data, expected_point_vals_point_vals2)
        self.assertEqual(changes.res_id, res_id)
        self.assertEqual(changes.res_model, 'point.model.test')

        record = point_obj.read(self.cursor, self.uid, res_id, context={
            'session': self.session_id
        })
        self.assertEqual(record['geom'], modified_point_vals2['geom'])

        results = point_obj.read(self.cursor, self.uid, res_id, ['geom'])
        self.assertEqual(results['geom'], 'POINT(8 8)')

    def test_read_with_session_should_return_data_and_session_values(self):
        point_obj = self.openerp.pool.get('point.model.test')

        original_point_vals = {
            "name": "Point 4",
            "geom": "POINT(8 8)"
        }
        res_id = point_obj.create(self.cursor, self.uid, original_point_vals)

        modified_point_vals = {
            "geom": "POINT(7 7)"
        }
        point_obj.write(
            self.cursor, self.uid, [res_id], modified_point_vals,
            context={'session': self.session_id}
        )

        values = point_obj.read(self.cursor, self.uid, res_id, context={
            'session': self.session_id
        })
        self.assertEqual(values['geom'], modified_point_vals['geom'])

    def test_read_without_session_should_return_master_data(self):
        point_obj = self.openerp.pool.get('point.model.test')

        original_point_vals = {
            "name": "Point 4",
            "geom": "POINT(8 8)"
        }
        res_id = point_obj.create(self.cursor, self.uid, original_point_vals)

        modified_point_vals = {
            "geom": "POINT(7 7)"
        }
        point_obj.write(
            self.cursor, self.uid, [res_id], modified_point_vals,
            context={'session': self.session_id}
        )

        values = point_obj.read(self.cursor, self.uid, res_id)
        self.assertEqual(values['geom'], original_point_vals['geom'])

    def test_unlink_with_session_should_mark_this_element_to_unlink(self):
        point_obj = self.openerp.pool.get('point.model.test')
        changes_obj = self.openerp.pool.get('giscegis.session.changes')

        original_point_vals = {
            "name": "Point 4",
            "geom": "POINT (8 8)"
        }
        res_id = point_obj.create(self.cursor, self.uid, original_point_vals)

        point_obj.unlink(
            self.cursor, self.uid, [res_id],
            context={'session': self.session_id}
        )

        changes_ids = changes_obj.search(self.cursor, self.uid, [
            ('res_id', '=', res_id),
            ('status', '=', 'D'),
            ('session_id', '=', self.session_id),
            ('res_model', '=', point_obj._name)
        ])

        self.assertEqual(len(changes_ids), 1)

        record = point_obj.read(self.cursor, self.uid, res_id, context={
            'session': self.session_id
        })
        self.assertEqual(record, False)

        results = point_obj.search(self.cursor, self.uid, [
            ('id', '=', res_id)
        ])
        self.assertEqual(len(results), 1)

    def test_unlink_without_session_should_delete_records(self):
        point_obj = self.openerp.pool.get('point.model.test')

        original_point_vals = {
            "name": "Point 4",
            "geom": "POINT (8 8)"
        }
        res_id = point_obj.create(self.cursor, self.uid, original_point_vals)

        point_obj.unlink(self.cursor, self.uid, [res_id])

        results = point_obj.search(self.cursor, self.uid, [
            ('id', '=', res_id)
        ])
        self.assertEqual(len(results), 0)

    def test_search_returns_added_elements_in_the_session(self):
        point_obj = self.openerp.pool.get('point.model.test')
        ids = point_obj.search(self.cursor, self.uid, [])
        point_obj.unlink(self.cursor, self.uid, ids)
        point_vals = {
            "name": "Point 4",
            "geom": "POINT (8 8)"
        }
        res_id = point_obj.create(
            self.cursor, self.uid, point_vals,
            context={'session': self.session_id}
        )

        results = point_obj.search(
            self.cursor, self.uid, [], context={'session': self.session_id}
        )
        self.assertEqual(results, [res_id])

        results = point_obj.read(
            self.cursor, self.uid, results, context={'session': self.session_id}
        )
        self.assertEqual(
            results,
            [{
                'active': True,
                'id': res_id,
                'name': 'Point 4',
                'geom': 'POINT(8 8)'
            }]
        )

    def test_search_not_returns_removed_elements_in_the_session(self):
        point_obj = self.openerp.pool.get('point.model.test')
        ids = point_obj.search(self.cursor, self.uid, [])
        point_obj.unlink(self.cursor, self.uid, ids)
        point_vals = {
            "name": "Point 4",
            "geom": "POINT (8 8)"
        }
        res_id = point_obj.create(self.cursor, self.uid, point_vals)
        point_obj.unlink(self.cursor, self.uid, [res_id], context={
            'session': self.session_id
        })

        results = point_obj.search(
            self.cursor, self.uid, [], context={'session': self.session_id}
        )
        self.assertEqual(results, [])


class TestSession(BaseSessionTests):

    def test_unlink_int(self):
        """
        Test to unlink using a int as id

        :return: None
        """

        point_obj = self.openerp.pool.get('point.model.test')

        ids = []
        original_point_vals1 = {
            "name": "Point 1",
            "geom": "POINT (8 8)"
        }
        ident = point_obj.create(
            self.cursor, self.uid, original_point_vals1,
        )
        ids.append(ident)

        original_point_vals2 = {
            "name": "Point 2",
            "geom": "POINT (8 8)"
        }
        ident = point_obj.create(
            self.cursor, self.uid, original_point_vals2,

        )
        ids.append(ident)

        original_point_vals3 = {
            "name": "Point 3",
            "geom": "POINT (8 8)"
        }
        ident = point_obj.create(
            self.cursor, self.uid, original_point_vals3,

        )
        ids.append(ident)

        for ident in ids:
            point_obj.unlink(
                self.cursor, self.uid, ident,
                context={"session": int(self.session_id)}
            )

    def test_apply_m_changes_modifies_the_original_record(self):
        point_obj = self.openerp.pool.get('point.model.test')
        session_obj = self.openerp.pool.get('giscegis.session')
        original_point_vals = {
            "name": "Point 4",
            "geom": "POINT (8 8)"
        }
        res_id = point_obj.create(self.cursor, self.uid, original_point_vals)

        modified_point_vals = {
            "geom": "POINT(7 7)"
        }
        point_obj.write(
            self.cursor, self.uid, [res_id], modified_point_vals,
            context={'session': self.session_id}
        )

        session = session_obj.browse(self.cursor, self.uid, self.session_id)
        changes = session.changes[0]

        self.assertEqual(changes.status, 'M')

        changes.apply()

        values = point_obj.read(self.cursor, self.uid, res_id)
        self.assertEqual(values['geom'], modified_point_vals['geom'])

    def test_apply_a_changes_creates_the_record(self):
        point_obj = self.openerp.pool.get('point.model.test')
        session_obj = self.openerp.pool.get('giscegis.session')
        point_vals = {
            "name": "Point 4",
            "geom": "POINT (8 8)"
        }
        res_id = point_obj.create(
            self.cursor, self.uid, point_vals,
            context={'session': self.session_id}
        )
        session = session_obj.browse(self.cursor, self.uid, self.session_id)
        changes = session.changes[0]

        self.assertEqual(changes.status, 'A')

        changes.apply()

        results = point_obj.search(self.cursor, self.uid, [('id', '=', res_id)])
        self.assertEqual(len(results), 1)

    def test_apply_d_changes_removes_the_record(self):
        point_obj = self.openerp.pool.get('point.model.test')
        session_obj = self.openerp.pool.get('giscegis.session')

        original_point_vals = {
            "name": "Point 4",
            "geom": "POINT (8 8)"
        }
        res_id = point_obj.create(self.cursor, self.uid, original_point_vals)

        point_obj.unlink(
            self.cursor, self.uid, [res_id],
            context={'session': self.session_id}
        )

        session = session_obj.browse(self.cursor, self.uid, self.session_id)
        changes = session.changes[0]

        self.assertEqual(changes.status, 'D')

        changes.apply()

        results = point_obj.search(self.cursor, self.uid, [
            ('id', '=', res_id)
        ])
        self.assertEqual(len(results), 0)

    def test_merge_session_should_apply_all_changes(self):
        point_obj = self.openerp.pool.get('point.model.test')
        session_obj = self.openerp.pool.get('giscegis.session')
        original_point_vals = {
            "name": "Point 4",
            "geom": "POINT (8 8)"
        }
        modidied_res_id = point_obj.create(
            self.cursor, self.uid, original_point_vals
        )

        modified_point_vals = {
            "geom": "POINT(7 7)"
        }
        point_obj.write(
            self.cursor, self.uid, [modidied_res_id], modified_point_vals,
            context={'session': self.session_id}
        )

        added_point_vals = {
            "name": "Point 4",
            "geom": "POINT(8 8)"
        }
        added_res_id = point_obj.create(
            self.cursor, self.uid, added_point_vals,
            context={'session': self.session_id}
        )

        deleted_point_vals = {
            "name": "Point 4",
            "geom": "POINT(8 8)"
        }
        deleted_res_id = point_obj.create(
            self.cursor, self.uid, deleted_point_vals
        )

        point_obj.unlink(
            self.cursor, self.uid, [deleted_res_id],
            context={'session': self.session_id}
        )

        session = session_obj.browse(self.cursor, self.uid, self.session_id)
        self.assertEqual(len(session.changes), 3)
        session.merge()
        results = point_obj.search(self.cursor, self.uid, [
            ('id', '=', deleted_res_id)
        ])
        self.assertEqual(len(results), 0)

        results = point_obj.search(self.cursor, self.uid, [
            ('id', '=', added_res_id)
        ])
        self.assertEqual(len(results), 1)

        values = point_obj.read(self.cursor, self.uid, modidied_res_id)
        self.assertEqual(values['geom'], modified_point_vals['geom'])
        self.cursor.execute("""
            TRUNCATE point_model_test
        """)

    def test_after_merge_session_it_should_be_merged_and_not_active(self):
        session_obj = self.openerp.pool.get('giscegis.session')
        session_obj.merge(self.cursor, self.uid, [self.session_id])
        session = session_obj.browse(self.cursor, self.uid, self.session_id)
        self.assertEqual(session.state, 'commited')
        self.assertFalse(session.active)
        self.cursor.execute("""
            TRUNCATE point_model_test
        """)

    def test_after_discard_session_it_should_be_discarted_and_not_active(self):
        session_obj = self.openerp.pool.get('giscegis.session')
        session_obj.discard(self.cursor, self.uid, [self.session_id])
        session = session_obj.browse(self.cursor, self.uid, self.session_id)
        self.assertEqual(session.state, 'discarted')
        self.assertFalse(session.active)

    def test_on_create_session_should_fail_if_intersects_other_open_sessions(self):
        values = {
            'name': 'Test Session 2',
            'bbox': 'POLYGON((5 5, 10 5, 10 10, 5 10, 5 5))'
        }
        with self.assertRaises(BoundingBoxReservedException):
            self.create_session(values)

    def test_edit_session_raises_if_intersects_other_open_sessions(self):
        session_obj = self.openerp.pool.get('giscegis.session')
        values = {
            'name': 'Test Session 2',
            'bbox': 'POLYGON((11 11, 21 11, 21 21, 11 21, 11 11))'
        }
        session2_id = self.create_session(values)
        with self.assertRaises(BoundingBoxReservedException):
            session_obj.write(self.cursor, self.uid, [session2_id], {
                'bbox': 'POLYGON((5 5, 10 5, 10 10, 5 10, 5 5))'
            })

    def test_edit_session_raises_if_resourse_is_out_of_new_bbox(self):
        point_obj = self.openerp.pool.get('point.model.test')
        session_obj = self.openerp.pool.get('giscegis.session')
        point_vals = {
            "name": "Point 4",
            "geom": "POINT (8 8)"
        }
        point_obj.create(
            self.cursor, self.uid, point_vals,
            context={'session': self.session_id}
        )
        with self.assertRaises(ResourceOutOfBboxSessionException):
            session_obj.write(self.cursor, self.uid, [self.session_id], {
                'bbox': 'POLYGON((0 0, 7 0, 7 7, 0 7, 0 0))'
            })


# Points with function fields Tests
class PointModelTestFunction(SessionManaged):

    def _base_double(self, cr, uid, ids, field_name, ar, context):
        """Function to force a direct sql query to calculate the field value"""
        res = dict.fromkeys(ids, 0)

        sql = """
            SELECT id, base*2
            FROM point_model_test_fnct
            WHERE id IN %(ids)s
        """

        cr.execute(sql, {'ids': tuple(ids)})

        for elem in cr.fetchall():
            res[elem[0]] = elem[1]

        return res

    _name = "point.model.test.fnct"
    _columns = {
        "name": fields.char("Name", size=64),
        "geom": geo_fields.Point("geom", srid=25830),
        "base": fields.integer("Base number"),
        "base_double": fields.function(
            _base_double, method=True, type='integer',
            string="Base number multiplied by two")
    }


class BaseSessionTestsFunction(testing.OOTestCase):
    def setUp(self):
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user
        self.create_test_point_model()
        self.session_id = self.create_session()

    def tearDown(self):
        self.txn.stop()

    def create_session(self, values=None):
        session_obj = self.openerp.pool.get('giscegis.session')
        if values is None:
            values = {
                'name': 'Test Session for inherits',
                'bbox': 'POLYGON((0 0, 10 0, 10 10, 0 10, 0 0))'
            }
        return session_obj.create(self.cursor, self.uid, values)

    def create_test_point_model(self):
        PointModelTestFunction()
        osv.class_pool["point.model.test.fnct"].createInstance(
            self.openerp.pool, "giscegis_base_geom", self.cursor
        )
        point_obj = self.openerp.pool.get("point.model.test.fnct")
        point_obj._auto_init(self.cursor)


class TestSessionManagerFunction(BaseSessionTestsFunction):

    def test_read_element_added_on_session_with_function_field(self):
        # Carregar el model
        point_obj = self.openerp.pool.get("point.model.test.fnct")

        # Definir els valors de l'element a crear
        point_vals = {
            "name": "Point 1",
            "geom": "POINT (8 8)",
            "base": 15
        }

        # Creació del punt dins la sessió
        point_id = point_obj.create(
            self.cursor, self.uid, point_vals,
            context={'session': self.session_id}
        )

        # Llegir element creat a la sessió amb un camp funcio que fa una
        # consulta directa a la DB del propi element que encara no existeix
        # persistent-ment.
        data = point_obj.read(
            self.cursor, self.uid, point_id,
            ['name', 'geom', 'base', 'base_double'],
            context={'session': self.session_id}
        )


# Inherits Points Tests
class PointModelTestInheritsBase(SessionManaged):
    _name = "point.model.test.base"
    _columns = {
        "name": fields.char("Name", size=64),
        "geom": geo_fields.Point("geom", srid=25830),
        "active": fields.boolean("Active")
    }

    _defaults = {
        "active": lambda *a: True
    }


class PointModelTestInherits(SessionManaged):
    _name = "point.model.test.inherits"
    _inherits = {"point.model.test.base": "p_id"}
    _columns = {
        "p_id": fields.many2one(
            'point.model.test.base', 'BasePoint', required=True
        ),
        "description": fields.char("description", size=64)
    }


class BaseSessionTestsInherits(testing.OOTestCase):
    def setUp(self):
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user
        self.create_test_point_model()
        self.session_id = self.create_session()

    def tearDown(self):
        self.txn.stop()

    def create_session(self, values=None):
        session_obj = self.openerp.pool.get('giscegis.session')
        if values is None:
            values = {
                'name': 'Test Session for inherits',
                'bbox': 'POLYGON((0 0, 10 0, 10 10, 0 10, 0 0))'
            }
        return session_obj.create(self.cursor, self.uid, values)

    def create_test_point_model(self):
        PointModelTestInheritsBase()
        osv.class_pool['point.model.test.base'].createInstance(
            self.openerp.pool, 'giscegis_base_geom', self.cursor
        )
        point_obj = self.openerp.pool.get('point.model.test.base')
        point_obj._auto_init(self.cursor)

        PointModelTestInherits()
        osv.class_pool['point.model.test.inherits'].createInstance(
            self.openerp.pool, 'giscegis_base_geom', self.cursor
        )
        point_obj = self.openerp.pool.get('point.model.test.inherits')
        point_obj._auto_init(self.cursor)

    def create_points(self):
        point_obj = self.openerp.pool.get('point.model.test.inherits')
        created_points = [
            point_obj.create(self.cursor, self.uid, {
                "name": "Point 1",
                "geom": "POINT (5 5)",
                "description": "Point inside the session BB"
            }),
            point_obj.create(self.cursor, self.uid, {
                "name": "Point 2",
                "geom": "POINT (11 11)",
                "description": "Point outside the session BB"
            })
        ]
        return created_points


class TestSessionManagerInherits(BaseSessionTestsInherits):

    def test_search_during_session_adds_bbox_filter(self):
        self.create_points()
        point_obj = self.openerp.pool.get('point.model.test.inherits')

        results = point_obj.search(self.cursor, self.uid, [], context={
            'session': self.session_id
        })
        self.assertEqual(len(results), 1)

    def test_search_without_session_not_uses_bbox_filter(self):
        self.create_points()
        point_obj = self.openerp.pool.get('point.model.test.inherits')

        results = point_obj.search(self.cursor, self.uid, [])
        self.assertEqual(len(results), 2)

    def test_create_has_catalog_id(self):
        """
        Tests that creating an element with "cataleg" in context it's saved on
        the catalog_id column
        :return: None
        """

        cat_obj = self.openerp.pool.get('giscegis.cataleg.cataleg')
        mod_obj = self.openerp.pool.get('ir.model')
        id_mod = mod_obj.search(
            self.cursor, self.uid,
            [("name", "=", 'point.model.test.inherits')]
        )[0]
        id_cat = cat_obj.create(
            self.cursor, self.uid, {
                "name": "test_cat_inherits",
                "model": id_mod,
                "domain": "",
                "geom_type": "Point"
            }
        )

        point_obj = self.openerp.pool.get('point.model.test.inherits')
        point_vals = {
            "name": "Point 3",
            "geom": "POINT (8 8)",
            "description": "Point to test sessions"
        }

        point_obj.create(
            self.cursor, self.uid, point_vals,
            context={
                'session': self.session_id,
                'cataleg': id_cat
            }
        )

        check_sql = """
        SELECT count(*) FROM giscegis_session_changes
        WHERE cataleg_id =%s;
        """

        self.cursor.execute(check_sql, (id_cat, ))
        data = self.cursor.fetchall()
        self.assertEquals(data[0][0], 1)

    def test_create_during_session_writes_to_session_and_returns_id(self):

        point_obj = self.openerp.pool.get('point.model.test.inherits')
        session_obj = self.openerp.pool.get('giscegis.session')
        point_vals = {
            "name": "Point 4",
            "geom": "POINT(8 8)",
            "description": "Point to test sessions"
        }
        res_id = point_obj.create(
            self.cursor, self.uid, point_vals,
            context={'session': self.session_id}
        )
        session = session_obj.browse(self.cursor, self.uid, self.session_id)
        self.assertEqual(len(session.changes), 1)

        changes = session.changes[0]

        self.assertEqual(changes.data, point_vals)
        self.assertEqual(changes.res_id, res_id)
        self.assertEqual(changes.res_model, 'point.model.test.inherits')

        record = point_obj.read(
            self.cursor, self.uid, res_id, context={'session': self.session_id}
        )
        self.assertEqual(record['geom'], point_vals['geom'])
        self.assertEqual(record['name'], point_vals['name'])

        results = point_obj.search(self.cursor, self.uid, [('id', '=', res_id)])
        self.assertEqual(len(results), 0)

    def test_create_without_session_creates_the_object(self):
        point_obj = self.openerp.pool.get('point.model.test.inherits')
        session_obj = self.openerp.pool.get('giscegis.session')
        point_vals = {
            "name": "Point 5",
            "geom": "POINT (8 8)",
            "description": "Point to test sessions"
        }
        res_id = point_obj.create(
            self.cursor, self.uid, point_vals,
        )
        session = session_obj.browse(self.cursor, self.uid, self.session_id)
        self.assertEqual(len(session.changes), 0)

        results = point_obj.search(self.cursor, self.uid, [('id', '=', res_id)])
        self.assertEqual(len(results), 1)

    def test_write_during_session_writes_to_session(self):
        point_obj = self.openerp.pool.get('point.model.test.inherits')
        session_obj = self.openerp.pool.get('giscegis.session')
        original_point_vals = {
            "name": "Point 6",
            "geom": "POINT(8 8)",
            "description": "Point to test sessions"
        }
        res_id = point_obj.create(self.cursor, self.uid, original_point_vals)
        modified_point_vals = {
            "geom": "POINT(7 7)",
            "name": None,
            "description": "Point modified to test sessions"
        }
        point_obj.write(
            self.cursor, self.uid, [res_id], modified_point_vals,
            context={'session': self.session_id}
        )

        expected_point_vals = {
            "geom": "POINT(7 7)",
            "name": False,
            "description": "Point modified to test sessions"
        }

        session = session_obj.browse(self.cursor, self.uid, self.session_id)
        self.assertEqual(len(session.changes), 1)

        changes = session.changes[0]

        self.assertEqual(changes.data, expected_point_vals)
        self.assertEqual(changes.res_id, res_id)
        self.assertEqual(changes.res_model, 'point.model.test.inherits')

        record = point_obj.read(self.cursor, self.uid, res_id, context={
            'session': self.session_id
        })
        self.assertEqual(record['geom'], modified_point_vals['geom'])

        # Serialize None -> False
        self.assertIs(record['name'], False)

        modified_point_vals2 = {
            "geom": "POINT(5 5)",
            "name": None,
            "description": "Point modified twice to test sessions"
        }

        expected_point_vals_point_vals2 = {
            "geom": "POINT(5 5)",
            "name": False,
            "description": "Point modified twice to test sessions"
        }

        point_obj.write(
            self.cursor, self.uid, [res_id], modified_point_vals2,
            context={'session': self.session_id}
        )
        session = session_obj.browse(self.cursor, self.uid, self.session_id)
        self.assertEqual(len(session.changes), 2)

        changes = session.changes[1]

        self.assertEqual(changes.data, expected_point_vals_point_vals2)
        self.assertEqual(changes.res_id, res_id)
        self.assertEqual(changes.res_model, 'point.model.test.inherits')

        record = point_obj.read(self.cursor, self.uid, res_id, context={
            'session': self.session_id
        })
        self.assertEqual(record['geom'], modified_point_vals2['geom'])

        results = point_obj.read(self.cursor, self.uid, res_id, ['geom'])
        self.assertEqual(results['geom'], 'POINT(8 8)')

    def test_read_with_session_should_return_data_and_session_values(self):
        point_obj = self.openerp.pool.get('point.model.test.inherits')

        original_point_vals = {
            "name": "Point 7",
            "geom": "POINT (8 8)",
            "description": "Point to test sessions"
        }
        res_id = point_obj.create(self.cursor, self.uid, original_point_vals)

        modified_point_vals = {
            "geom": "POINT(7 7)",
            "description": "Point modified to test sessions"
        }
        point_obj.write(
            self.cursor, self.uid, [res_id], modified_point_vals,
            context={'session': self.session_id}
        )

        values = point_obj.read(self.cursor, self.uid, res_id, context={
            'session': self.session_id
        })
        self.assertEqual(values['geom'], modified_point_vals['geom'])
        self.assertEqual(
            values['description'], modified_point_vals['description']
        )

    def test_read_without_session_should_return_master_data(self):
        point_obj = self.openerp.pool.get('point.model.test.inherits')

        original_point_vals = {
            "name": "Point 8",
            "geom": "POINT(8 8)",
            "description": "Point to test sessions"
        }
        res_id = point_obj.create(self.cursor, self.uid, original_point_vals)

        modified_point_vals = {
            "geom": "POINT(7 7)",
            "description": "Point modified to test sessions"
        }
        point_obj.write(
            self.cursor, self.uid, [res_id], modified_point_vals,
            context={'session': self.session_id}
        )

        values = point_obj.read(self.cursor, self.uid, res_id)
        self.assertEqual(values['geom'], original_point_vals['geom'])
        self.assertEqual(
            values['description'], original_point_vals['description']
        )

    def test_unlink_with_session_should_mark_this_element_to_unlink(self):
        point_obj = self.openerp.pool.get('point.model.test.inherits')
        changes_obj = self.openerp.pool.get('giscegis.session.changes')

        original_point_vals = {
            "name": "Point 9",
            "geom": "POINT (8 8)",
            "description": "Point to test sessions"
        }
        res_id = point_obj.create(self.cursor, self.uid, original_point_vals)

        point_obj.unlink(
            self.cursor, self.uid, [res_id],
            context={'session': self.session_id}
        )

        changes_ids = changes_obj.search(self.cursor, self.uid, [
            ('res_id', '=', res_id),
            ('status', '=', 'D'),
            ('session_id', '=', self.session_id),
            ('res_model', '=', point_obj._name)
        ])

        self.assertEqual(len(changes_ids), 1)

        record = point_obj.read(
            self.cursor, self.uid, res_id, context={'session': self.session_id}
        )
        self.assertEqual(record, False)

        results = point_obj.search(self.cursor, self.uid, [
            ('id', '=', res_id)
        ])
        self.assertEqual(len(results), 1)

    def test_unlink_without_session_should_delete_records(self):
        point_obj = self.openerp.pool.get('point.model.test.inherits')

        original_point_vals = {
            "name": "Point 10",
            "geom": "POINT (8 8)",
            "description": "Point to test sessions"
        }

        res_id = point_obj.create(self.cursor, self.uid, original_point_vals)

        point_obj.unlink(self.cursor, self.uid, [res_id])

        results = point_obj.search(self.cursor, self.uid, [
            ('id', '=', res_id)
        ])

        self.assertEqual(len(results), 0)

    def test_search_returns_added_elements_in_the_session(self):
        point_obj = self.openerp.pool.get('point.model.test.inherits')

        point_vals = {
            "name": "Point 11",
            "geom": "POINT (8 8)",
            "description": "Point to test sessions"
        }

        res_id = point_obj.create(
            self.cursor, self.uid, point_vals,
            context={'session': self.session_id}
        )

        results = point_obj.search(
            self.cursor, self.uid, [], context={'session': self.session_id}
        )

        self.assertEqual(results, [res_id])

        results = point_obj.read(
            self.cursor, self.uid, results, context={'session': self.session_id}
        )[0]

        self.assertEqual(results['id'], res_id)
        self.assertEqual(results['name'], 'Point 11')
        self.assertEqual(results['geom'], 'POINT(8 8)')
        self.assertEqual(results['description'], 'Point to test sessions')
        self.assertEqual(results['p_id'][1], 'Point 11')

    def test_search_not_returns_removed_elements_in_the_session(self):
        point_obj = self.openerp.pool.get('point.model.test.inherits')

        point_vals = {
            "name": "Point 12",
            "geom": "POINT (8 8)",
            "description": "Point to test sessions"
        }

        res_id = point_obj.create(self.cursor, self.uid, point_vals)

        point_obj.unlink(
            self.cursor, self.uid, [res_id],
            context={'session': self.session_id}
        )

        results = point_obj.search(
            self.cursor, self.uid, [], context={'session': self.session_id}
        )
        self.assertEqual(results, [])

    def test_read_elements_in_a_session_with_empty_ids(self):
        point_obj = self.openerp.pool.get('point.model.test.inherits')
        session_obj = self.openerp.pool.get('giscegis.session')
        point_vals = {
            "name": "Point 13",
            "geom": "POINT (8 8)",
            "description": "Point to test sessions"
        }
        res_id = point_obj.create(
            self.cursor, self.uid, point_vals,
            context={'session': self.session_id}
        )
        session = session_obj.browse(self.cursor, self.uid, self.session_id)
        self.assertEqual(len(session.changes), 1)

        changes = session.changes[0]

        self.assertEqual(changes.data, point_vals)
        self.assertEqual(changes.res_id, res_id)
        self.assertEqual(changes.res_model, 'point.model.test.inherits')

        record = point_obj.read(
            self.cursor, self.uid, [], context={'session': self.session_id}
        )

        self.assertEqual(len(record), 0)


# Points with reference field Tests
class PointModelTestReferenced(SessionManaged):
    _name = "point.model.test.referenced"
    _columns = {
        "name": fields.char("Name", size=64),
        "geom": geo_fields.Point("geom", srid=25830),
        "description": fields.text("Description"),
        "active": fields.boolean('Active')
    }

    _defaults = {
        'active': lambda *a: True
    }


class PointModelTestRelated(SessionManaged):
    _name = "point.model.test.related"
    _columns = {
        "name": fields.char("Name", size=64),
        "geom": geo_fields.Point("geom", srid=25830),
        "description": fields.text("Description")
    }


RELATED_MODELS = [
    ('point.model.test.referenced', 'Referenced'),
]


class PointModelTestReferencing(SessionManaged):
    _name = "point.model.test.referencing"
    _columns = {
        "name": fields.char("Name", size=64),
        "geom": geo_fields.Point("geom", srid=25830),
        "reference": fields.reference(
            'Instal·lació', RELATED_MODELS, size=256, required=True
        ),
        "related_id": fields.related(
            'name', 'related_id', type='many2one',
            relation='point.model.test.referenced.two', store=True,
            readonly=True, string="Relacionat"
        ),
        "active": fields.boolean('Active')
    }

    _defaults = {
        'active': lambda *a: True
    }


class BaseSessionTestsReference(testing.OOTestCase):
    def setUp(self):
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user
        self.create_test_point_model()
        self.session_id = self.create_session()

    def tearDown(self):
        self.txn.stop()

    def create_session(self, values=None):
        session_obj = self.openerp.pool.get('giscegis.session')
        if values is None:
            values = {
                'name': 'Test Session for references',
                'bbox': 'POLYGON((0 0, 10 0, 10 10, 0 10, 0 0))'
            }
        return session_obj.create(self.cursor, self.uid, values)

    def create_test_point_model(self):
        PointModelTestReferenced()
        osv.class_pool['point.model.test.referenced'].createInstance(
            self.openerp.pool, 'giscegis_base_geom', self.cursor
        )
        point_obj = self.openerp.pool.get('point.model.test.referenced')
        point_obj._auto_init(self.cursor)

        PointModelTestRelated()
        osv.class_pool['point.model.test.related'].createInstance(
            self.openerp.pool, 'giscegis_base_geom', self.cursor
        )
        point_obj = self.openerp.pool.get('point.model.test.related')
        point_obj._auto_init(self.cursor)

        PointModelTestReferencing()
        osv.class_pool['point.model.test.referencing'].createInstance(
            self.openerp.pool, 'giscegis_base_geom', self.cursor
        )
        point_obj = self.openerp.pool.get('point.model.test.referencing')
        point_obj._auto_init(self.cursor)

    def create_points(self):
        referenced_obj = self.openerp.pool.get('point.model.test.referenced')
        related_obj = self.openerp.pool.get('point.model.test.related')
        referencing_obj = self.openerp.pool.get('point.model.test.referencing')

        id_referenced = referenced_obj.create(
            self.cursor, self.uid, {
                "name": "Point Referenced",
                "geom": "POINT (2 1)",
                "description": "Point to be referenced"
            }
        )

        id_related = related_obj.create(
            self.cursor, self.uid, {
                "name": "Point Realted",
                "geom": "POINT (9 1)",
                "description": "Point to be related"
            }
        )

        id_referencing = referencing_obj.create(
            self.cursor, self.uid, {
                "name": "Point Referencing",
                "geom": "POINT (4 5)",
                "reference":
                    "point.model.test.referenced,"+str(id_referenced),
                "related_id": id_related
            }
        )

        created_points = {
            'referenced': id_referenced,
            'referencing': id_referencing,
            'related': id_related
        }

        return created_points


class TestSessionManagerReference(BaseSessionTestsReference):

    def test_apply_changes_with_reference_fields(self):
        # Test per forçar la casuistica en que en una sessió s'elimina l'element
        # referenciat per un camp de tipus reference required i on al aplicar
        # els canvis de la sessió, l'element referenciador te també un camp
        # related que força la lectura d'aquest al aplicar els canvis.

        # Inicialitzar objectes i valors
        created_points = self.create_points()
        referenced_obj = self.openerp.pool.get('point.model.test.referenced')
        referencing_obj = self.openerp.pool.get('point.model.test.referencing')
        session_obj = self.openerp.pool.get('giscegis.session')
        session_changes_obj = self.openerp.pool.get('giscegis.session.changes')

        # Moure referenced
        referenced_obj.write(
            self.cursor, self.uid, [created_points['referenced']],
            {"geom": "POINT (5 5)"},
            context={'session': self.session_id}
        )

        # Moure referencing
        referencing_obj.write(
            self.cursor, self.uid, [created_points['referencing']],
            {"geom": "POINT (7 9)"},
            context={'session': self.session_id}
        )

        # Borrar referenced
        referenced_obj.unlink(
            self.cursor, self.uid, [created_points['referenced']],
            context={'session': self.session_id}
        )

        # Aplicar canvis de la sessio
        session = session_obj.read(
            self.cursor, self.uid, self.session_id, ['changes']
        )
        session_changes_obj.apply(self.cursor, self.uid, session['changes'])

        # Si no ha petat aplicant canvis es llegeix l'element per forçar
        # l'error
        referencing_elem = referencing_obj.read(self.cursor, self.uid, 1)
        model, ref_id = referencing_elem['reference'].split(',')
        self.assertEqual(ref_id, '0')
        self.assertEqual(model, 'point.model.test.referenced')

        # Mirar que el browse a un reference funciona bé.
        # Browse_reference TRUE
        context = {'browse_reference': True}
        referencing_elem_bro = referencing_obj.browse(
            self.cursor, self.uid, created_points['referencing'], context
        )
        reference = referencing_elem_bro.reference
        self.assertFalse(reference)
        # Browse_reference FALSE
        context = {'browse_reference': False}
        referencing_elem_bro = referencing_obj.browse(
            self.cursor, self.uid, created_points['referencing'], context
        )
        reference = referencing_elem_bro.reference
        model, ref_id = reference.split(',')
        self.assertEqual(ref_id, '0')
        self.assertEqual(model, 'point.model.test.referenced')
