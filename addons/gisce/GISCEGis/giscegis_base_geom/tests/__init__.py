# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from addons import get_module_resource
import base64

from .tests_fields import *
from .test_session import *
from .test_geom_update_subscription import *
from .test_qread import *
from tools import config


class BaseTest(testing.OOTestCase):

    def test_get_btlike(self):
        """
        Tests get_btlike_layer function

        :return: None
        """

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            base_geom = self.openerp.pool.get('giscegis.base.geom')
            res_config = self.openerp.pool.get('res.config')
            btlike = base_geom.get_btlike_layer(cursor, uid)
            self.assertEqual(btlike, 'LBT\_%')
            data = {'name': 'giscegis_btlike_layer',
                    'value': '1234'}
            res_config.create(cursor, uid, data)
            btlike = base_geom.get_btlike_layer(cursor, uid)
            self.assertEqual(btlike, '1234')

    def test_get_srid(self):
        """
        Tests get_srid method

        :return: None
        """

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            del config.options["srid"]
            base_geom = self.openerp.pool.get('giscegis.base.geom')
            res_config = self.openerp.pool.get('res.config')
            search_params = [('name', '=', 'giscegis_srid')]
            id_res = res_config.search(cursor, uid, search_params)
            res_config.unlink(cursor, uid, id_res)
            srid = base_geom.get_srid(cursor, uid)
            self.assertEqual(srid, 25830)

            res_config.set(cursor, uid, 'giscegis_srid', '25831')
            srid = base_geom.get_srid(cursor, uid)
            self.assertEqual(srid, 25831)

            res_config.set(cursor, uid, 'giscegis_srid', '25830')
            config.options["srid"] = "25831"
            srid = base_geom.get_srid(cursor, uid)
            self.assertEqual(srid, 25831)

            res_config.set(cursor, uid, 'giscegis_srid', '25831')
            config.options["srid"] = "25830"
            srid = base_geom.get_srid(cursor, uid)
            self.assertEqual(srid, 25830)

    def test_truncate_table(self):
        """
        Test truncate_table

        :return: None
        """

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            base_geom = self.openerp.pool.get('giscegis.base.geom')
            base_geom.truncate_table(cursor, uid, 'res_config')
            sql = """
            SELECT 1
            FROM res_config
            """
            cursor.execute(sql)
            data = cursor.fetchall()
            self.assertEqual(len(data), 0)

    def test_wkt(self):
        """
        Test to_wkt_point

        :return: None
        """

        base_geom = self.openerp.pool.get('giscegis.base.geom')
        wkt = base_geom.to_wkt_point([[[0, 1]]])
        self.assertEqual('POINT(0 1)', wkt)

    def test_to_wkt_multilinestring(self):
        """
        Test to_wkt_multilinestring

        :return: None
        """

        base_geom = self.openerp.pool.get('giscegis.base.geom')
        wkt = base_geom.to_wkt_multilinestring([[[0, 1], [2, 3], [4, 5]]])
        self.assertEqual('MULTILINESTRING((0 1,2 3,4 5))', wkt)

    def test_sanitize(self):
        """
        Test sanitize

        :return: None
        """
        record = 0.1
        base_geom = self.openerp.pool.get('giscegis.base.geom')
        srecord = base_geom.sanitize_record(record, 'int')
        self.assertEqual(srecord, '0')
        srecord = base_geom.sanitize_record(record, 'float')
        self.assertEqual(srecord, '0.1')
        srecord = base_geom.sanitize_record(record)
        self.assertEqual(srecord, '0.1')

    def test_wizard_load_lat_shp(self):
        """
        Tests the shp load

        :return: None
        """

        self.openerp.install_module('giscegis_lat_geom')
        self.openerp.install_module('giscegis_lbt_geom')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            cursor.execute("TRUNCATE giscegis_lat")
            cursor.execute("TRUNCATE giscegis_lbt")
            model = self.openerp.pool.get('giscegis.shp.loader')
            res_obj = self.openerp.pool.get('res.config')

            res_data = {
                'name': 'giscegis_gis_url',
                'value': '0'
            }

            res_obj.create(cursor, uid, res_data)
            lat_url = get_module_resource(
                'giscegis_base_geom', 'tests', 'fixtures', 'lat.zip')
            lbt_url = get_module_resource(
                'giscegis_base_geom', 'tests', 'fixtures', 'lbt.zip')
            with open(lat_url) as fd_lat:
                lat_data = fd_lat.read()
            with open(lbt_url) as fd_lbt:
                lbt_data = fd_lbt.read()

            wizard_id = model.create(cursor, uid, {})
            wizard_load = model.browse(cursor, uid, wizard_id)
            model_data = {
                'file_lat': base64.encodestring(lat_data),
                'file_lbt': base64.encodestring(lbt_data)
            }
            model.write(cursor, uid, [wizard_id], model_data)
            wizard_load.import_data_shp()
            sql_lat = """
                SELECT count(*) FROM giscegis_lat;
            """
            cursor.execute(sql_lat)
            num_lat = cursor.fetchall()[0][0]
            self.assertEqual(int(num_lat), 2)

            obj_lat = self.openerp.pool.get('giscegis.lat.geom')
            ids_lat = obj_lat.search(cursor, uid, [])
            self.assertEqual(len(ids_lat), 2)

            obj_lbt = self.openerp.pool.get('giscegis.lbt.geom')
            ids_lbt = obj_lbt.search(cursor, uid, [])
            self.assertEqual(len(ids_lbt), 2)

    def test_switch_table(self):
        """
        Test the swithc table method

        :return: None
        """

        sql_create = """
        CREATE TABLE test_table (
          test_field      varchar(40)
          );
        """

        sql_create2 = """
                CREATE TABLE test_table_tmp (
                  test_field      varchar(40)
                  );
                """
        sql_check = """
        SELECT * FROM test_table;
        """
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            model_base = self.openerp.pool.get('giscegis.base.geom')
            origin_table = "test_table_tmp"
            dest_table = "test_table"
            cursor.execute(sql_create)
            cursor.execute(sql_create2)

            model_base.switch_table(
                cursor, uid, origin_table, dest_table)

            cursor.execute(sql_check)

            try:
                cursor.execute("""
                SELECT * FROM test_table_tmp;
                """)
            except Exception as e:
                print(e.message)
                except_launched = True
            self.assertTrue(except_launched)

    def test_add_lat_geometry(self):
        """
        Test add_lat_geometry

        :return: None
        """

        self.openerp.install_module('giscegis_lat_geom')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            base_geom = self.openerp.pool.get('giscegis.base.geom')
            geom = 'MULTILINESTRING((774685.022175 4642526.43979,774691.478603 4642506.21103))'
            base_geom.add_lat_geometry(cursor, uid, 1, geom)
            sql = """
            SELECT count(*)
            FROM giscegis_lat
            """
            cursor.execute(sql)
            data = cursor.fetchall()
            self.assertEqual(data[0][0], 1L)

    def test_clear_lat(self):
        """
        Tests clear_lat

        :return: None
        """

        self.openerp.install_module('giscegis_lat_geom')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            base_geom = self.openerp.pool.get('giscegis.base.geom')
            geom = 'MULTILINESTRING((774685.022175 4642526.43979,774691.478603 4642506.21103))'
            base_geom.add_lat_geometry(cursor, uid, 1, geom)
            base_geom.add_lat_geometry(cursor, uid, 2, geom)
            base_geom.add_lat_geometry(cursor, uid, 2, geom)
            sql = """
            SELECT count(*)
            FROM giscegis_lat
            """
            cursor.execute(sql)
            data = cursor.fetchall()
            self.assertEqual(data[0][0], 3L)
            base_geom.clear_lat(cursor, uid)
            sql = """
            SELECT count(*)
            FROM giscegis_lat
            """
            cursor.execute(sql)
            data = cursor.fetchall()
            self.assertEqual(data[0][0], 0L)

    def test_add_lbt_geometry(self):
        """
        Test add_lbt_geometry

        :return: None
        """

        self.openerp.install_module('giscegis_lbt_geom')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            base_geom = self.openerp.pool.get('giscegis.base.geom')
            geom = 'MULTILINESTRING((774685.022175 4642526.43979,774691.478603 4642506.21103))'
            base_geom.add_lbt_geometry(cursor, uid, 1, geom)
            sql = """
            SELECT count(*)
            FROM giscegis_lbt
            """
            cursor.execute(sql)
            data = cursor.fetchall()
            self.assertEqual(data[0][0], 1L)

    def test_clear_lbt(self):
        """
        Tests clear_lbt

        :return: None
        """

        self.openerp.install_module('giscegis_lbt_geom')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            base_geom = self.openerp.pool.get('giscegis.base.geom')
            geom = 'MULTILINESTRING((774685.022175 4642526.43979,774691.478603 4642506.21103))'
            base_geom.add_lbt_geometry(cursor, uid, 1, geom)
            base_geom.add_lbt_geometry(cursor, uid, 2, geom)
            base_geom.add_lbt_geometry(cursor, uid, 2, geom)
            sql = """
            SELECT count(*)
            FROM giscegis_lbt
            """
            cursor.execute(sql)
            data = cursor.fetchall()
            self.assertEqual(data[0][0], 3L)
            base_geom.clear_lbt(cursor, uid)
            sql = """
            SELECT count(*)
            FROM giscegis_lbt
            """
            cursor.execute(sql)
            data = cursor.fetchall()
            self.assertEqual(data[0][0], 0L)

    def test_get_layers(self):
        """
        Tests get_layers

        :return: None
        """

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            base_geom = self.openerp.pool.get('giscegis.base.geom')
            self.assertTrue('at' in base_geom.get_layers(cursor, uid))
            self.assertTrue('bt' in base_geom.get_layers(cursor, uid))
