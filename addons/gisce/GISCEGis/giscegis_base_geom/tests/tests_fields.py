from __future__ import absolute_import
from osv import osv, fields
from giscegis_base_geom import fields as geo_fields
from destral import testing
from destral.transaction import Transaction
from oopgrade.oopgrade import column_exists


__all__ = [
    'TestLineField',
    'TestPointField',
    'TestPolygonField',
    'TestMultiPolygonField'
]


def describe_geom_table(cursor, table):
    """
    Returns the geometry description of the table

    :param cursor: Database cursor
    :param table: Table
    :return: Geom schema
    """

    sql = """
    SELECT
      f_geometry_column As col_name,
      type, srid,
      coord_dimension As ndims
    FROM geometry_columns
    WHERE f_table_name = %(table)s;
    """
    cursor.execute(sql, {"table": table})
    data = cursor.fetchall()
    res = {}
    for line in data:
        res[line[0]] = {
            "geom_type": line[1],
            "srid": line[2],
            "dimensions": line[3]
        }
    return res


class LineModelTest(osv.osv):
    _name = "line.model.test"
    _columns = {
        "name": fields.char("Name", size=64),
        "geom": geo_fields.Line("geom", srid=25830),
        'other_geom': geo_fields.Line("other_geom", srid=25831)
    }


class PointModelTest(osv.osv):
    _name = "point.model.test"
    _columns = {
        "name": fields.char("Name", size=64),
        "geom": geo_fields.Point("geom", srid=25830),
        'other_geom': geo_fields.Point("other_geom", srid=25831)
    }


class PolygonModelTest(osv.osv):
    _name = "polygon.model.test"
    _columns = {
        "name": fields.char("Name", size=64),
        "geom": geo_fields.Polygon("geom", srid=25830),
        'other_geom': geo_fields.Polygon("other_geom", srid=25831)
    }


class MultiPolygonModelTest(osv.osv):
    _name = "multipolygon.model.test"
    _columns = {
        "name": fields.char("Name", size=64),
        "geom": geo_fields.Multipolygon("geom", srid=25830),
        "other_geom": geo_fields.Multipolygon("other_geom", srid=25831)
    }


class TestPointField(testing.OOTestCase):

    def create_test_point_model(self):
        PointModelTest()
        osv.class_pool['point.model.test'].createInstance(
            self.openerp.pool, 'giscegis_base_geom', self.cursor
        )
        point_obj = self.openerp.pool.get('point.model.test')
        point_obj._auto_init(self.cursor)

    def setUp(self):
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user

        self.create_test_point_model()

    def tearDown(self):
        self.txn.stop()

    def test_defining_point_field_creates_geom_field_in_database(self):
        """
        Tests the creation geometry , read and write of the point field
        :return: None
        :rtype: None
        """

        exist_col_geom = column_exists(
            self.cursor, "point_model_test", "geom"
        )
        self.assertTrue(exist_col_geom)
        exother_g = column_exists(
            self.cursor, "point_model_test", "other_geom"
        )
        self.assertTrue(exother_g)
        description = describe_geom_table(self.cursor, "point_model_test")
        self.assertTrue("geom" in description)
        self.assertTrue("other_geom" in description)
        self.assertDictEqual(description["geom"], {
            "geom_type": u"POINT",
            "srid": 25830,
            "dimensions": 2,
        })
        self.assertDictEqual(description["other_geom"], {
            "geom_type": u"POINT",
            "srid": 25831,
            "dimensions": 2,
        })

    def test_write_false(self):
        """
        Tests that if you write a false it's stored on the DB
        :return:None
        """
        
        point_obj = self.openerp.pool.get('point.model.test')


        point_id = point_obj.create(self.cursor, self.uid, {
            "geom": "POINT (1 1)"
        })
        point_obj.write(self.cursor, self.uid, point_id, {
            "geom": False
        })
        point_data = point_obj.read(self.cursor, self.uid, point_id, ["geom"])
        self.assertFalse(point_data["geom"])

    def test_creating_a_record_with_point_values_stores_values(self):

        point_obj = self.openerp.pool.get('point.model.test')

        id_point = point_obj.create(self.cursor, self.uid, {
            "geom": "POINT (1 1)"
        })
        point_data = point_obj.read(self.cursor, self.uid, id_point, ["geom"])
        expected_data = {"id": id_point, "geom": "POINT(1 1)"}
        self.assertDictEqual(point_data, expected_data)

    def test_creating_a_record_with_incorrect_point_raises_exception(self):

        point_obj = self.openerp.pool.get('point.model.test')

        with self.assertRaises(Exception):
            point_obj.create(self.cursor, self.uid, {
                "geom": "hola"
            })

    def test_equal_operator(self):
        point_obj = self.openerp.pool.get('point.model.test')

        id_point = point_obj.create(self.cursor, self.uid, {
            "geom": "POINT (1 1)"
        })

        result = point_obj.search(self.cursor, self.uid, [
            ('geom', '=', 'POINT (1 1)')
        ])
        self.assertEqual(len(result), 1)
        self.assertEqual(result[0], id_point)

        id_point = point_obj.create(self.cursor, self.uid, {
            "geom": "POINT (2 1)"
        })
        result = point_obj.search(self.cursor, self.uid, [
            ('geom', '=', 'POINT (2 1)')
        ])
        self.assertEqual(len(result), 1)
        self.assertEqual(result[0], id_point)

    def test_bbox_operator(self):
        point_obj = self.openerp.pool.get('point.model.test')
        ids = point_obj.search(self.cursor, self.uid, [])
        point_obj.unlink(self.cursor, self.uid, ids)

        created_points = [
            point_obj.create(self.cursor, self.uid, {
                "name": "Point 1",
                "geom": "POINT (5 5)"
            }),
            point_obj.create(self.cursor, self.uid, {
                "name": "Point 2",
                "geom": "POINT (2 5)"
            }),
            point_obj.create(self.cursor, self.uid, {
                "name": "Point 3",
                "geom": "POINT (3 7)"
            }),
            point_obj.create(self.cursor, self.uid, {
                "name": "Point 4",
                "geom": "POINT (8 8)"
            }),
        ]

        point_obj.create(self.cursor, self.uid, {
            "name": "Point 5",
            "geom": "POINT (11 11)"
        })

        results = point_obj.search(self.cursor, self.uid, [
            ('geom', '&&', 'POLYGON((0 0, 10 0, 10 10, 0 10, 0 0))')
        ])
        self.assertEqual(len(results), 4)
        self.assertEqual(results, created_points)

        results = point_obj.search(self.cursor, self.uid, [
            ('name', '=', 'Point 1'),
            ('geom', '&&', 'POLYGON((0 0, 10 0, 10 10, 0 10, 0 0))')
        ])
        self.assertEqual(len(results), 1)
        self.assertEqual(results, [created_points[0]])

    def test_rounding_coords_on_point(self):

        point_obj = self.openerp.pool.get('point.model.test')

        id_point = point_obj.create(self.cursor, self.uid, {
            "geom": "POINT (780325.29350999998860 4640285.16626999992877)"
        })

        point_data = point_obj.read(self.cursor, self.uid, id_point, ["geom"])

        expected_data = {
            "id": id_point,
            "geom": "POINT(780325.2935 4640285.1663)"
        }

        self.assertDictEqual(point_data, expected_data)

        id_point = point_obj.create(self.cursor, self.uid, {
            "geom": "POINT(780325.9999999999 4640285.9999999999)"
        })

        point_data = point_obj.read(self.cursor, self.uid, id_point, ["geom"])

        expected_data = {
            "id": id_point,
            "geom": "POINT(780326 4640286)"
        }

        self.assertDictEqual(point_data, expected_data)

        id_point = point_obj.create(self.cursor, self.uid, {
            "geom": "POINT(780325.1587985679 4640285.9874568723)"
        })

        point_data = point_obj.read(self.cursor, self.uid, id_point, ["geom"])

        expected_data = {
            "id": id_point,
            "geom": "POINT(780325.1588 4640285.9875)"
        }

        self.assertDictEqual(point_data, expected_data)


class TestLineField(testing.OOTestCase):

    def create_test_line_model(self):
        """
        Creates the line model

        :return: None
        """
        LineModelTest()
        osv.class_pool['line.model.test'].createInstance(
            self.openerp.pool, 'giscegis_base_geom', self.cursor
        )
        line_obj = self.openerp.pool.get('line.model.test')
        line_obj._auto_init(self.cursor)

    def setUp(self):
        """
        Sets up the test

        :return: None
        """
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user

        self.create_test_line_model()
        self.cursor.execute(
            """
            DELETE FROM ir_model
            WHERE name like '%.test%'
            """
        )

    def tearDown(self):
        self.txn.stop()

    def test_defining_line_field_creates_geom_field_in_database(self):
        """
        Tests the creation geometry , read and write of the point field
        :return: None
        :rtype: None
        """

        exist_col_geom = column_exists(self.cursor, "line_model_test", "geom")
        self.assertTrue(exist_col_geom)
        exother_g = column_exists(self.cursor, "line_model_test", "other_geom")
        self.assertTrue(exother_g)
        description = describe_geom_table(self.cursor, "line_model_test")
        self.assertTrue("geom" in description)
        self.assertTrue("other_geom" in description)
        self.assertDictEqual(description["geom"], {
            "geom_type": u"LINESTRING",
            "srid": 25830,
            "dimensions": 2,
        })
        self.assertDictEqual(description["other_geom"], {
            "geom_type": u"LINESTRING",
            "srid": 25831,
            "dimensions": 2,
        })

    def test_creating_a_record_with_line_values_stores_values(self):
        """
        Test creates line with values
        :return: None
        """
        line_obj = self.openerp.pool.get('line.model.test')
        id_line = line_obj.create(self.cursor, self.uid, {
            "geom": "LINESTRING (1 1,2 2)"
        })
        line_data = line_obj.read(self.cursor, self.uid, id_line, ["geom"])
        expected_data = {"id": id_line, "geom": "LINESTRING(1 1,2 2)"}
        self.assertDictEqual(line_data, expected_data)

    def test_write_false(self):
        """
        Tests that if you write a false it's stored on the DB
        :return:None
        """

        line_obj = self.openerp.pool.get('line.model.test')

        line_id = line_obj.create(self.cursor, self.uid, {
            "geom": "LINESTRING (1 1, 2 2)"
        })
        line_obj.write(self.cursor, self.uid, line_id, {
            "geom": False
        })
        point_data = line_obj.read(self.cursor, self.uid, line_id, ["geom"])
        self.assertFalse(point_data["geom"])

    def test_creating_a_record_with_incorrect_values_raises_exception(self):
        """
        Crates a row with wrong geom
        :return: None
        """
        line_obj = self.openerp.pool.get('line.model.test')
        with self.assertRaises(Exception):
            line_obj.create(self.cursor, self.uid, {
                "geom": "hola"
            })

    def test_equal_operator(self):
        """
        Tests the equal operator
        :return: None
        """
        line_obj = self.openerp.pool.get('line.model.test')

        id_line = line_obj.create(self.cursor, self.uid, {
            "geom": "LINESTRING (1 1, 2 2)"
        })

        result = line_obj.search(self.cursor, self.uid, [
            ('geom', '=', 'LINESTRING (1 1, 2 2)')
        ])
        self.assertEqual(len(result), 1)
        self.assertEqual(result[0], id_line)

    def test_bbox_operator(self):
        """
        Tests the bbox operator

        :return: None
        """
        line_obj = self.openerp.pool.get('line.model.test')

        created_lines = [
            line_obj.create(self.cursor, self.uid, {
                "name": "Line 1",
                "geom": "LINESTRING (1 1, 2 2)"
            }),
            line_obj.create(self.cursor, self.uid, {
                "name": "Line 2",
                "geom": "LINESTRING (4 3, 5 5, 7 7)"
            }),
            line_obj.create(self.cursor, self.uid, {
                "name": "Line 3",
                "geom": "LINESTRING (0 0, 0 1, 0 2)"
            }),
            line_obj.create(self.cursor, self.uid, {
                "name": "Line 4",
                "geom": "LINESTRING (9 9, 10 10, 11 11)"
            }),
        ]

        line_obj.create(self.cursor, self.uid, {
            "name": "Line 5",
            "geom": "LINESTRING (12 12, 13 13, 15 15)"
        })

        results = line_obj.search(self.cursor, self.uid, [
            ('geom', '&&', 'POLYGON((0 0, 10 0, 10 10, 0 10, 0 0))')
        ])
        self.assertEqual(len(results), 4)
        self.assertEqual(results, created_lines)

        results = line_obj.search(self.cursor, self.uid, [
            ('name', '=', 'Line 1'),
            ('geom', '&&', 'POLYGON((0 0, 10 0, 10 10, 0 10, 0 0))')
        ])
        self.assertEqual(len(results), 1)
        self.assertEqual(results, [created_lines[0]])

    def test_bbox_intersection_between_line_and_polygon(self):
        line_obj = self.openerp.pool.get('line.model.test')

        created_lines = [
            line_obj.create(self.cursor, self.uid, {
                "name": "Line 1",
                "geom": "LINESTRING (1 1, 2 2)"
            }),
            line_obj.create(self.cursor, self.uid, {
                "name": "Line 1",
                "geom": "LINESTRING(5 11,12 11,12 -1,6 -1)"
            })
        ]
        results = line_obj.search(self.cursor, self.uid, [
            ('geom', '&&!', 'POLYGON((0 0, 10 0, 10 10, 0 10, 0 0))')
        ])
        self.assertEqual(len(results), 1)
        self.assertEqual(results, [created_lines[0]])

    def test_bbox_intersection_alias_between_line_and_polygon(self):
        line_obj = self.openerp.pool.get('line.model.test')

        created_lines = [
            line_obj.create(self.cursor, self.uid, {
                "name": "Line 1",
                "geom": "LINESTRING (1 1, 2 2)"
            }),
            line_obj.create(self.cursor, self.uid, {
                "name": "Line 1",
                "geom": "LINESTRING(5 11,12 11,12 -1,6 -1)"
            })
        ]
        results = line_obj.search(self.cursor, self.uid, [
            ('geom', 'ST_INTERSECTS', 'POLYGON((0 0, 10 0, 10 10, 0 10, 0 0))')
        ])
        self.assertEqual(len(results), 1)
        self.assertEqual(results, [created_lines[0]])

    def test_bbox_fast_two_constraints(self):
        line_obj = self.openerp.pool.get('line.model.test')

        created_lines = [
            line_obj.create(self.cursor, self.uid, {
                "name": "Line 1",
                "geom": "LINESTRING (1 1, 2 2)"
            }),
            line_obj.create(self.cursor, self.uid, {
                "name": "Line 1",
                "geom": "LINESTRING(5 11,12 11,12 -1,6 -1)"
            })
        ]
        results = line_obj.search(self.cursor, self.uid, [
            ('geom', '&&', 'POLYGON((0 0, 10 0, 10 10, 0 10, 0 0))'),
            ('geom', '&&!', 'POLYGON((0 0, 10 0, 10 10, 0 10, 0 0))')
        ])
        self.assertEqual(len(results), 1)
        self.assertEqual(results, [created_lines[0]])

    def test_rounding_coords_on_linestrings(self):
        """
        Test round on LineStrings
        :return: None
        """

        line_obj = self.openerp.pool.get('line.model.test')

        id_line = line_obj.create(self.cursor, self.uid, {
            "geom": "LINESTRING(780325.29350999998860 4640285.166269999928772,"
                    "780325.00160399999003857 4640284.02472000010311604,"
                    "780372.5696062040515244 4640105.61898914817720652)"
        })

        line_data = line_obj.read(self.cursor, self.uid, id_line, ["geom"])

        expected_data = {
            "id": id_line,
            "geom": "LINESTRING(780325.2935 4640285.1663,"
                    "780325.0016 4640284.0247,"
                    "780372.5696 4640105.619)"
        }

        self.assertDictEqual(line_data, expected_data)

        id_line = line_obj.create(self.cursor, self.uid, {
            "geom": "LINESTRING(780325.999999999 4640285.999999999,"
                    " 780325.999999999 4640284.999999999, "
                    "780372.999999999 4640105.999999999)"
        })

        line_data = line_obj.read(self.cursor, self.uid, id_line, ["geom"])

        expected_data = {
            "id": id_line,
            "geom": "LINESTRING(780326 4640286,780326 4640285,780373 4640106)"
        }

        self.assertDictEqual(line_data, expected_data)

        id_line = line_obj.create(self.cursor, self.uid, {
            "geom": "LINESTRING(780325.8798751231687 4640285.1489798456845,"
                    "780325.99999999999 4640284.987543126,"
                    "780372.231251654321 4640105.99910321321564)"
        })

        line_data = line_obj.read(self.cursor, self.uid, id_line, ["geom"])

        expected_data = {
            "id": id_line,
            "geom": "LINESTRING(780325.8799 4640285.149,"
                    "780326 4640284.9875,"
                    "780372.2313 4640105.9991)"
        }

        self.assertDictEqual(line_data, expected_data)


class TestPolygonField(testing.OOTestCase):

    def create_test_polygon_model(self):
        """
        Creates the polygon model

        :return: None
        """
        PolygonModelTest()
        osv.class_pool['polygon.model.test'].createInstance(
            self.openerp.pool, 'giscegis_base_geom', self.cursor
        )
        polygon_obj = self.openerp.pool.get('polygon.model.test')
        polygon_obj._auto_init(self.cursor)

    def setUp(self):
        """
        Sets up the test

        :return: None
        """
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user

        self.create_test_polygon_model()

    def tearDown(self):
        self.txn.stop()

    def test_defining_polygon_field_creates_geom_field_in_database(self):
        """
        Tests the creation geometry , read and write of the point field
        :return: None
        :rtype: None
        """

        exist_col_geom = column_exists(self.cursor, "polygon_model_test", "geom")
        self.assertTrue(exist_col_geom)
        exother_g = column_exists(self.cursor, "polygon_model_test", "other_geom")
        self.assertTrue(exother_g)
        description = describe_geom_table(self.cursor, "polygon_model_test")
        self.assertTrue("geom" in description)
        self.assertTrue("other_geom" in description)
        self.assertDictEqual(description["geom"], {
            "geom_type": u"POLYGON",
            "srid": 25830,
            "dimensions": 2,
        })
        self.assertDictEqual(description["other_geom"], {
            "geom_type": u"POLYGON",
            "srid": 25831,
            "dimensions": 2,
        })

    def test_creating_a_record_with_polygon_values_stores_values(self):
        """
        Test creates polygon with values
        :return: None
        """
        polygon_obj = self.openerp.pool.get('polygon.model.test')
        id_polygon = polygon_obj.create(self.cursor, self.uid, {
            "geom": "POLYGON ((1 1, 1 2, 2 2, 2 1, 1 1))"
        })
        polygon_data = polygon_obj.read(self.cursor, self.uid, id_polygon, ["geom"])
        expected_data = {
            "id": id_polygon,
            "geom": "POLYGON((1 1,1 2,2 2,2 1,1 1))"
        }
        self.assertDictEqual(polygon_data, expected_data)

    def test_creating_a_record_with_incorrect_values_raises_exception(self):
        """
        Crates a row with wrong geom
        :return: None
        """
        polygon_obj = self.openerp.pool.get('polygon.model.test')
        with self.assertRaises(Exception):
            polygon_obj.create(self.cursor, self.uid, {
                "geom": "hola"
            })

    def test_equal_operator(self):
        """
        Tests the equal operator
        :return: None
        """
        polygon_obj = self.openerp.pool.get('polygon.model.test')
        ids = polygon_obj.search(self.cursor, self.uid, [])
        polygon_obj.unlink(self.cursor, self.uid, ids)

        id_polygon = polygon_obj.create(self.cursor, self.uid, {
            "geom": "POLYGON((1 1,1 2, 2 2, 2 1, 1 1))"
        })

        result = polygon_obj.search(self.cursor, self.uid, [
            ('geom', '=', "POLYGON((1 1,1 2, 2 2, 2 1, 1 1))")
        ])
        self.assertEqual(len(result), 1)
        self.assertEqual(result[0], id_polygon)

    def test_bbox_operator(self):
        """
        Tests the bbox operator

        :return: None
        """
        polygon_obj = self.openerp.pool.get('polygon.model.test')
        ids = polygon_obj.search(self.cursor, self.uid, [])
        polygon_obj.unlink(self.cursor, self.uid, ids)

        created_polygons = [
            polygon_obj.create(self.cursor, self.uid, {
                "name": "Poligon 1",
                "geom": "POLYGON ((0 0, 1 0, 1 1, 0 1, 0 0))"
            }),
            polygon_obj.create(self.cursor, self.uid, {
                "name": "Poligon 2",
                "geom": "POLYGON ((1 1, 2 1, 2 2, 1 2, 1 1))"
            }),
            polygon_obj.create(self.cursor, self.uid, {
                "name": "Poligon 3",
                "geom": "POLYGON ((3 3, 4 3, 4 4, 3 4, 3 3))"
            }),
            polygon_obj.create(self.cursor, self.uid, {
                "name": "Poligon 4",
                "geom": "POLYGON ((3 3, 4 3, 4 4, 3 4, 3 3))"
            }),
        ]

        polygon_obj.create(self.cursor, self.uid, {
            "name": "Poligon 5",
            "geom": "POLYGON ((12 12, 13 13, 15 15, 12 12))"
        })

        results = polygon_obj.search(self.cursor, self.uid, [
            ('geom', '&&', 'POLYGON((0 0, 10 0, 10 10, 0 10, 0 0))')
        ])
        self.assertEqual(len(results), 4)
        self.assertEqual(results, created_polygons)

        results = polygon_obj.search(self.cursor, self.uid, [
            ('name', '=', 'Poligon 1'),
            ('geom', '&&', 'POLYGON((0 0, 10 0, 10 10, 0 10, 0 0))')
        ])
        self.assertEqual(len(results), 1)
        self.assertEqual(results, [created_polygons[0]])

    def test_rounding_coords_on_polygon(self):
        """
        Test rounding on polygons
        :return: None
        """

        polygon_obj = self.openerp.pool.get('polygon.model.test')

        id_polygon = polygon_obj.create(self.cursor, self.uid, {
            "geom": "POLYGON((780287.5258691403 4639963.841901521,"
                    "780505.2278730739 4639963.841901521,"
                    "780505.2278730739 4640166.794447193,"
                    "780287.5258691403 4640166.794447193,"
                    "780287.5258691403 4639963.841901521))"
        })

        polygon_data = polygon_obj.read(
            self.cursor, self.uid, id_polygon, ["geom"]
        )

        expected_data = {
            "id": id_polygon,
            "geom": "POLYGON((780287.5259 4639963.8419,"
                    "780505.2279 4639963.8419,"
                    "780505.2279 4640166.7944,"
                    "780287.5259 4640166.7944,"
                    "780287.5259 4639963.8419))"
        }

        self.assertDictEqual(polygon_data, expected_data)

        id_polygon = polygon_obj.create(self.cursor, self.uid, {
            "geom": "POLYGON((780287.999999999 4639963.999999999,"
                    "780505.999999999 4639963.999999999,"
                    "780505.999999999 4640166.999999999,"
                    "780287.999999999 4640166.999999999,"
                    "780287.999999999 4639963.999999999))"
        })

        polygon_data = polygon_obj.read(
            self.cursor, self.uid, id_polygon, ["geom"]
        )

        expected_data = {
            "id": id_polygon,
            "geom": "POLYGON((780288 4639964,780506 4639964,780506 4640167,"
                    "780288 4640167,780288 4639964))"
        }

        self.assertDictEqual(polygon_data, expected_data)


class TestRegisterStIntersectsOperator(testing.OOTestCase):
    def setUp(self):
        """
        Sets up the test

        :return: None
        """
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user
        self.drop_st_intersects_operator()

    def drop_st_intersects_operator(self):
        self.cursor.execute(
            "SELECT oprname FROM pg_operator WHERE oprname = '&&!'"
        )
        if self.cursor.rowcount:
            self.cursor.execute("DROP OPERATOR &&!(geometry, geometry)")

    def tearDown(self):
        self.txn.stop()

    def test_st_intersects_operator_is_registered(self):
        from giscegis_base_geom.giscegis_base_geom import register_st_intersects_operator
        register_st_intersects_operator(self.cursor)

        self.cursor.execute(
            "SELECT * FROM pg_operator WHERE oprname = '&&!'"
        )
        self.assertEqual(self.cursor.rowcount, 1)

    def test_st_intersects_operator_is_not_registered_if_already_exists(self):
        from giscegis_base_geom.giscegis_base_geom import register_st_intersects_operator
        register_st_intersects_operator(self.cursor)

        self.cursor.execute(
            "SELECT * FROM pg_operator WHERE oprname = '&&!'"
        )
        self.assertEqual(self.cursor.rowcount, 1)

        register_st_intersects_operator(self.cursor)


class TestMultiPolygonField(testing.OOTestCase):

    def create_test_multipolygon_model(self):
        """
        Creates the multipolygon model
        :return: None
        """
        MultiPolygonModelTest()
        osv.class_pool['multipolygon.model.test'].createInstance(
            self.openerp.pool, 'giscegis_base_geom', self.cursor
        )
        mpolygon_obj = self.openerp.pool.get('multipolygon.model.test')
        mpolygon_obj._auto_init(self.cursor)

    def setUp(self):
        """
        Sets up the test

        :return: None
        """
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user

        self.create_test_multipolygon_model()

    def tearDown(self):
        self.txn.stop()

    def test_defining_multipolygon_field_creates_geom_field_in_database(self):
        """
        Tests the creation geometry , read and write of the multipolygon field
        :return: None
        :rtype: None
        """

        exist_col_geom = column_exists(
            self.cursor, "multipolygon_model_test", "geom"
        )
        self.assertTrue(exist_col_geom)
        exother_g = column_exists(
            self.cursor, "multipolygon_model_test", "other_geom"
        )
        self.assertTrue(exother_g)
        description = describe_geom_table(
            self.cursor, "multipolygon_model_test"
        )
        self.assertTrue("geom" in description)
        self.assertTrue("other_geom" in description)
        self.assertDictEqual(description["geom"], {
            "geom_type": u"MULTIPOLYGON",
            "srid": 25830,
            "dimensions": 2,
        })
        self.assertDictEqual(description["other_geom"], {
            "geom_type": u"MULTIPOLYGON",
            "srid": 25831,
            "dimensions": 2,
        })

    def test_creating_a_record_with_multipolygon_values_stores_values(self):
        """
        Test creates multipolygon with values
        :return: None
        """
        mpolygon_obj = self.openerp.pool.get('multipolygon.model.test')
        id_polygon = mpolygon_obj.create(self.cursor, self.uid, {
            "geom": "MULTIPOLYGON(((1 1, 1 2, 2 2, 2 1, 1 1)), "
                    "((4 4, 4 5, 5 5, 5 4, 4 4)))"
        })
        mpolygon_data = mpolygon_obj.read(
            self.cursor, self.uid, id_polygon, ["geom"]
        )
        expected_data = {
            "id": id_polygon,
            "geom": "MULTIPOLYGON(((1 1,1 2,2 2,2 1,1 1)),"
                    "((4 4,4 5,5 5,5 4,4 4)))"
        }
        self.assertDictEqual(mpolygon_data, expected_data)

    def test_creating_a_record_with_incorrect_values_raises_exception(self):
        """
        Crates a row with wrong geom
        :return: None
        """
        mpolygon_obj = self.openerp.pool.get('multipolygon.model.test')
        with self.assertRaises(Exception):
            mpolygon_obj.create(self.cursor, self.uid, {
                "geom": "hola"
            })

    def test_equal_operator(self):
        """
        Tests the equal operator
        :return: None
        """
        mpolygon_obj = self.openerp.pool.get('multipolygon.model.test')

        id_mpolygon = mpolygon_obj.create(self.cursor, self.uid, {
            "geom": "MULTIPOLYGON(((1 1, 1 2, 2 2, 2 1, 1 1)), "
                    "((4 4, 4 5, 5 5, 5 4, 4 4)))"
        })

        result = mpolygon_obj.search(self.cursor, self.uid, [
            ('geom', '=', "MULTIPOLYGON(((1 1,1 2,2 2,2 1,1 1)),"
                          "((4 4,4 5,5 5,5 4,4 4)))"
             )
        ])
        self.assertEqual(len(result), 1)
        self.assertEqual(result[0], id_mpolygon)

    def test_bbox_operator(self):
        """
        Tests the bbox operator
        :return: None
        """
        mpolygon_obj = self.openerp.pool.get('multipolygon.model.test')

        created_polygons = [
            mpolygon_obj.create(self.cursor, self.uid, {
                "name": "MPoligon 1",
                "geom": "MULTIPOLYGON (((3 3,7 3,7 5,3 5,3 3)),"
                        "((7 5,9 5,9 7,7 7,7 5)))"
            }),
            mpolygon_obj.create(self.cursor, self.uid, {
                "name": "MPoligon 2",
                "geom": "MULTIPOLYGON (((9 8,13 8,13 11,9 11,9 8)))"
            }),
            mpolygon_obj.create(self.cursor, self.uid, {
                "name": "MPoligon 3",
                "geom": "MULTIPOLYGON (((15 13,20 13,20 16,15 16,15 13)))"
            })
        ]

        results = mpolygon_obj.search(self.cursor, self.uid, [
            ('geom', '&&', 'POLYGON((2 2,14 2,14 12,2 12,2 2))')
        ])
        self.assertEqual(len(results), 2)
        self.assertEqual(results, [created_polygons[0], created_polygons[1]])

        results = mpolygon_obj.search(self.cursor, self.uid, [
            ('name', '=', 'MPoligon 3'),
            ('geom', '&&', 'POLYGON((14 12,21 12,21 17,14 17,14 12))')
        ])
        self.assertEqual(len(results), 1)
        self.assertEqual(results, [created_polygons[2]])
