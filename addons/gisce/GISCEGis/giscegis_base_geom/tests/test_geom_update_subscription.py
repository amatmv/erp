# coding=utf-8
from destral import testing
from destral.transaction import Transaction
import logging


logger = logging.getLogger(__name__)


__all__ = [
    'TestUpdateGeomSubscription'
]


class TestUpdateGeomSubscription(testing.OOTestCase):

    def setUp(self):
        # Install all the modules that will be subscribed.
        # On the future this test can be updated by adding more modules
        # that will be subscribed.
        self.openerp.install_module('giscegis_blocs_tensio')
        self.openerp.install_module('giscegis_blocs_fusiblesbt')
        self.openerp.install_module('giscegis_blocs_caixesbt')
        self.openerp.install_module('giscegis_edge')
        self.openerp.install_module('giscegis_lbt_geom')
        self.openerp.install_module('giscegis_blocs_suports_at')
        self.openerp.install_module('giscegis_blocs_adudsp')
        self.openerp.install_module('giscegis_blocs_interruptorat')
        self.openerp.install_module('giscegis_blocs_interruptorat')
        self.openerp.install_module('giscegis_blocs_transformadors')
        self.openerp.install_module('giscegis_blocs_escomeses')
        self.openerp.install_module('giscegis_blocs_interruptorbt')
        self.openerp.install_module('giscegis_blocs_empalmes_grup')
        self.openerp.install_module('giscegis_blocs_ctat')
        self.openerp.install_module('giscegis_blocs_detectors')
        self.openerp.install_module('giscegis_blocs_fusibles_caixes')
        self.openerp.install_module('giscegis_blocs_netsource')
        self.openerp.install_module('giscegis_blocs_elements_at')
        self.openerp.install_module('giscegis_nodes')
        self.openerp.install_module('giscegis_lat_geom')
        self.openerp.install_module('giscegis_blocs_suports_bt')

        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user

    def tearDown(self):
        self.txn.stop()

    def test_update_geom_data_subscription(self):
        """
        This test is about to check if all the subscriptions made on the global
        variable GIS_MODELS_TO_UPDATE_GEOM_DATA are made correctly.
        """
        from giscegis_base_geom.wizard.giscegis_shp_loader \
            import GIS_MODELS_TO_UPDATE_GEOM_DATA

        # Check the ammount of models that should be subscribed.
        self.assertEqual(len(GIS_MODELS_TO_UPDATE_GEOM_DATA), 21)

        # Check if all the functions of the subscribed model are callable.
        for model in GIS_MODELS_TO_UPDATE_GEOM_DATA:
            mod_obj = self.openerp.pool.get(model['model'])
            for function in model['functions']:
                if mod_obj:
                    fnct = getattr(mod_obj, function)
                    self.assertTrue(callable(fnct))
