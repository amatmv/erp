# -*- coding: utf-8 -*-
from osv import osv, fields
from destral import testing
from destral.transaction import Transaction
from giscegis_base_geom.giscegis_session import SessionManaged

__all__ = [
    'TestQread'
]


class ModelTestC(osv.osv):
    _name = "model.test.c"
    _columns = {
        "name": fields.char("Name", size=64),
    }


class ModelTestManyToOneB(osv.osv):
    _name = "model.test.many.to.one.b"
    _columns = {
        "name": fields.char("Name", size=64),
        "c_id": fields.many2one("model.test.c", "C-Ref"),
    }


class ModelTestManyToOneA(SessionManaged):
    _name = "model.test.many.to.one.a"
    _columns = {
        "name": fields.char("Name", size=64),
        "b_id": fields.many2one("model.test.many.to.one.b", 'B-Ref'),
    }


class TestQread(testing.OOTestCase):

    def create_test_models(self):
        # ModelTestC
        ModelTestC()
        osv.class_pool['model.test.c'].createInstance(
            self.openerp.pool, 'giscegis_base_geom', self.cursor
        )
        self.openerp.pool.get('model.test.c')._auto_init(self.cursor)

        # ModelTestManyToOneB
        ModelTestManyToOneB()
        osv.class_pool['model.test.many.to.one.b'].createInstance(
            self.openerp.pool, 'giscegis_base_geom', self.cursor
        )
        self.openerp.pool.get(
            'model.test.many.to.one.b'
        )._auto_init(self.cursor)

        # ModelTestManyToOneA
        ModelTestManyToOneA()
        osv.class_pool['model.test.many.to.one.a'].createInstance(
            self.openerp.pool, 'giscegis_base_geom', self.cursor
        )
        self.openerp.pool.get(
            'model.test.many.to.one.a'
        )._auto_init(self.cursor)

    def setUp(self):
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user
        self.create_test_models()

    def tearDown(self):
        self.txn.stop()

    def test_qread_all_fields_filled(self):
        cursor = self.cursor
        uid = self.uid
        model_a_obj = self.openerp.pool.get('model.test.many.to.one.a')
        model_b_obj = self.openerp.pool.get('model.test.many.to.one.b')
        model_c_obj = self.openerp.pool.get('model.test.c')

        c_id = model_c_obj.create(cursor, uid, {"name": "C-Model"})
        b_id = model_b_obj.create(
            cursor, uid, {"name": "B-Model", 'c_id': c_id}
        )
        a_id = model_a_obj.create(
            cursor, uid, {"name": "A-Model", 'b_id': b_id}
        )

        res_a = model_a_obj.qread(
            cursor, uid, [a_id],
            ['name', 'b_id.id', 'b_id.name', 'b_id.c_id.id', 'b_id.c_id.name']
        )[0]

        expected_vals = {
            'id': a_id,
            'name': u'A-Model',
            'b_id.id': b_id,
            'b_id.name': u'B-Model',
            'b_id.c_id.id': c_id,
            'b_id.c_id.name': u'C-Model'
        }

        self.assertDictEqual(expected_vals, res_a)

    def test_qread_fields_partially_filled(self):
        cursor = self.cursor
        uid = self.uid
        model_a_obj = self.openerp.pool.get('model.test.many.to.one.a')
        model_b_obj = self.openerp.pool.get('model.test.many.to.one.b')
        model_c_obj = self.openerp.pool.get('model.test.c')

        c_id = model_c_obj.create(cursor, uid, {})

        b_id = model_b_obj.create(
            cursor, uid, {'c_id': c_id}
        )
        a_id = model_a_obj.create(
            cursor, uid, {'b_id': b_id}
        )

        res_a = model_a_obj.qread(
            cursor, uid, [a_id],
            ['name', 'b_id.id', 'b_id.name', 'b_id.c_id.id', 'b_id.c_id.name']
        )[0]

        expected_vals = {
            'id': a_id,
            'name': False,
            'b_id.id': b_id,
            'b_id.name': False,
            'b_id.c_id.id': c_id,
            'b_id.c_id.name': False
        }

        self.assertDictEqual(expected_vals, res_a)
