# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS v3 (Base)",
    "description": """
        Mòdul base de GISCE GIS v3
        
        Variables de configuracion:
        srid: Si no se indica se usará el SRID 25830 por defecto    
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "base_extended",
        "giscegis_base",
        "giscegis_layers",
        "giscegis_cataleg",
    ],
    "init_xml": [],
    "demo_xml": [
        "giscegis_base_geom_demo.xml"
    ],
    "update_xml":[
        "wizard/giscegis_conversor_wizard.xml",
        "security/ir.model.access.csv",
        "wizard/giscegis_shp_loader.xml",
        "giscegis_base_geom_data.xml",
        "wizard/giscegis_last_dump_wizard.xml",
        "giscegis_layers_base_data.xml",
        "wizard/giscegis_mode_update.xml",
        "wizard/giscegis_prepare_dump.xml",
        "giscegis_session_view.xml",
        "res_user_view.xml",
    ],
    "active": False,
    "installable": True
}
