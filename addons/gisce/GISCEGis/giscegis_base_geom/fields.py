from osv.fields import text, _column
from osv.expression import INTERNAL_OPS, OPS, OPS_ALIAS
from psycopg2.extensions import AsIs
from shapely.wkt import loads


if '&&' not in INTERNAL_OPS:
    INTERNAL_OPS.append('&&')
if '&&!' not in INTERNAL_OPS:
    INTERNAL_OPS.append('&&!')
    OPS_ALIAS['ST_INTERSECTS'] = '&&!'

if '&&' not in OPS:
    OPS.append('&&')
if '&&!' not in OPS:
    OPS.append('&&!')


class BaseGeomField(text):
    _symbol_c = "ST_ASTEXT(%s)"
    _symbol_f = "ST_ASTEXT(%s)"

    def __init__(self, string='unknown', srid=0, **args):
        """
        Class constructor

        :param string: Field string
        :type string: str
        :param srid: Point SRID
        :type self: int,str
        :param args: Extra arguments
        :type args: dict
        :param select: Enables index
        :type select: bool
        """
        _column.__init__(self, string=string, **args)
        self.srid = srid
        self._symbol_set = ('%s', self.convert_to_wkb)
        self.index_type = "gist"

    def convert_field(self, f, context=None):
        if context is None:
            context = {}
        return '%s AS %s' % (self._symbol_c, f) % (f,)

    def convert_to_wkb(self, value):
        """
        Returns the Postgis write conversion from WKT to WKB.
        Will be implemented on the SubClasses

        :param value: WKT to write
        :param value: str
        :return: Value to write
        :rtype: str
        """

        if (value is False) or (value is None):
            return AsIs("NULL")
        else:
            return AsIs(
                "ST_SnapToGrid(ST_Translate(ST_GeomFromText('{}',{}),"
                "0.0000000001,0.0000000001), 0.0001)".format(
                    value, self.srid
                )
            )


class Point(BaseGeomField):
    """
    Point field
    """

    _type = "point"


    @property
    def pg_type(self):
        """
        Returns the postgres type
        :return: The pg_type
        :rtype: tuple
        """
        return "point", "geometry(point,{})".format(self.srid)


class Line(BaseGeomField):
    """
    Line field
    """

    _type = "line"

    @property
    def pg_type(self):
        """
        Returns the postgres type
        :return: The pg_type
        :rtype: tuple
        """
        return "point", "geometry(LineString,{})".format(self.srid)


class Polygon(BaseGeomField):
    """
    Polygon field
    """

    _type = "polygon"


    @property
    def pg_type(self):
        """
        Returns the postgres type
        :return: The pg_type
        :rtype: tuple
        """
        return "polygon", "geometry(Polygon,{})".format(self.srid)


class Multipolygon(BaseGeomField):
    """
    Multipolygon field
    """

    _type = "multipolygon"

    @property
    def pg_type(self):
        """
        Returns the postgres type
        :return: The pg_type
        :rtype: tuple
        """
        return "multipolygon", "geometry(MultiPolygon,{})".format(self.srid)


class Geometry(text):
    """
    Geometry field
    """

    _type = "geometry"
    _symbol_c = '%s'

    @property
    def pg_type(self):
        """
        Returns the pg_type
        :return: pg_type with geometry and sql type of geometry
        :rtype: tuple
        """

        return "geometry", "geometry(Geometry,{})".format(self.srid)

    def __init__(self, string='unknown', srid=0, **args):
        """
        Class constructor

        :param string: Field string
        :type string: str
        :param srid: SRID of the geometry
        :type srid: int,str
        :param args: Exra arguments
        :type args: dict
        """
        _column.__init__(self, string=string, **args)
        self.srid = srid
