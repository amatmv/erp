# *-* coding: utf-8 *-*

import time

from osv import osv, fields
from tools.translate import _
import itertools

from tools.sql import is_postgis_db
from tools import config
import netsvc
import requests
import shapefile
from psycopg2.extensions import AsIs
import logging

# TABLES TO CLEAN ON PREPARE DUMP
# Subscribed model format:
# ['tableName1', 'tableName2', ..., 'tableNameN']
DROP_ON_AUTOCAD_DUMP = []


def register_st_intersects_operator(cursor):
    """
    Register a new operator &&! which uses ST_Intersects procedure instead of
    geometry_overlaps
    :param cursor: Database cursor
    """

    logger = logging.getLogger('openerp.postgis')
    cursor.execute("SELECT oprname FROM pg_operator WHERE oprname = '&&!'")
    if not cursor.rowcount:
        logger.info("Registering &&! operator to database")
        cursor.execute("""
            CREATE OPERATOR &&! (
                LEFTARG = geometry, RIGHTARG = geometry, PROCEDURE = st_intersects,
                COMMUTATOR = '&&!',
                RESTRICT = gserialized_gist_sel_2d,
                JOIN = gserialized_gist_joinsel_2d
            )
        """)


class GiscegisBaseGeom(osv.osv):

    _name = 'giscegis.base.geom'
    _auto = False
    _columns = {
        'name': fields.char('Name', readonly=True, size=1),
    }

    def __init__(self, pool, cr):
        """
        Overwrites the init to avoid start ERP without the SRID set

        :param pool: OSV Pool
        :param cr: Database cursor
        :return: None
        :rtype: None
        """

        if not config.get("srid", False):
            raise Exception("SRID not set in the configuration, set the value of the SRID in the configuration file")
        super(GiscegisBaseGeom, self).__init__(pool, cr)

    def init(self, cursor):
        logger = netsvc.Logger()
        if not is_postgis_db(cursor):
            logger.notifyChannel(
                "postgis", netsvc.LOG_INFO, "Postgis not enabled")
            cursor.execute("CREATE EXTENSION postgis")
            logger.notifyChannel(
                "postgis", netsvc.LOG_INFO, "Posgis loaded")
            cursor.execute("CREATE EXTENSION postgis_topology")
            logger.notifyChannel(
                "postgis", netsvc.LOG_INFO, "Posgis topology loaded")
            assert is_postgis_db(cursor)
        register_st_intersects_operator(cursor)
        return True

    def get_last_dump(self, cursor, uid, context=None):
        """
        Gets the last dump of the Giscegis v3

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param context: OpenERP context
        :type context: dict
        :return: Last dump
        :rtype: str
        """

        sql = """
        SELECT GREATEST(create_date, write_date)::date
        FROM giscegis_nodes
        LIMIT 1;
        """
        cursor.execute(sql)
        result_nodes = cursor.fetchall()
        if result_nodes:
            result_nodes = result_nodes[0]
        return result_nodes

    def module_installed(self, cursor, uid, module):
        """
        Checks if a module is installed

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param module: Module name
        :type module: str
        :return: True if is installed
        :rtype: bool
        """

        ir_obj = self.pool.get('ir.module.module')
        id_module = ir_obj.search(cursor, uid, [('name', '=', module)])
        if id_module:
            return ir_obj.read(
                cursor, uid, id_module, ['state'])[0]['state'] == 'installed'
        else:
            return False

    def recreate_gis_views(self, cursor, uid):
        """
        Recreates the GIS materialized views

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :return: True if recreated
        :rtype: bool
        """
        gis_modules = [
            'giscegis_adudsp_geom',
            'giscegis_blocs_suports_at_geom',
            'giscegis_blocs_tensio_geom',
            'giscegis_blocs_trafos_reductors_geom',
            'giscegis_trafos_geom',
            'giscegis_caixabt_geom',
            'giscegis_cts_geom',
            'giscegis_cups_escomeses_tensio_geom',
            'giscegis_fusibls_caixes_geom',
            'giscegis_interruptorsat_geom',
            'giscegis_interruptorsbt_geom',
            'giscegis_lat_geom',
            'giscegis_lbt_geom',
            'giscegis_netsource_geom',
            'giscegis_seccionadorat_geom',
            'giscegis_nodes_geom'
        ]
        recreated = False
        for module in gis_modules:
            if self.module_installed(cursor, uid, module):
                mod_obj = self.pool.get(module.replace('_', '.'))
                mod_obj.recreate(cursor, uid)
                recreated = True
        return recreated

    def get_btlike_layer(self, cursor, uid, context=None):
        """
        Returns the btlike_layer value

        :param cursor: OpenERP cursor
        :param uid: User id
        :param context: OpenERP context
        :return: Bt like layer as str
        """
        conf_obj = self.pool.get('res.config')
        return conf_obj.get(cursor, uid, 'giscegis_btlike_layer', 'LBT\_%')

    def get_srid(self, cursor, uid, context=None):
        """
        Returns the srid of the ERP

        :param cursor: Database cursor
        :param uid: User id
        :param context: OpenERP Context
        :return: srid
        :rtype: int
        """
        conf_obj = self.pool.get('res.config')
        res_config_srid = conf_obj.get(cursor, uid, 'giscegis_srid', 25830)
        return int(config.get("srid", res_config_srid))

    def add_status(self, cursor, uid, ids, status, wizard):
        """
        Adds the status to the wizard

        :param cursor: Database cursor
        :param uid: User id
        :param ids: Afectes elements id
        :param status: Status to add
        :return: None
        """
        if wizard.status:
            status = wizard.status + '\n' + status
        wizard.write({'status': status})

    def remove_tms_cache(self, cursor, uid, layer):
        """
        Deletes the cache of the layer on the TMS server

        :param cursor: Database cursor
        :param uid: User id
        :return: Boolean that indicates if the cache is properly removed
        """

        conf_obj = self.pool.get('res.config')
        gis_url = conf_obj.get(
            cursor, uid, 'giscegis_gis_url', 'http://localhost')
        if gis_url == '0':
            return True
        try:
            response = requests.get('{0}/cleancache/{1}'.format(gis_url, layer))
        except Exception:
            return False

        if response.status_code != 200:
            msg = "Error netejant el cache de la capa {} url:{}"
            sentry = self.pool.get('sentry.setup')
            if sentry is not None:
                sentry.client.captureMessage(
                    msg.format(layer, gis_url),
                    level=logging.WARNING)
        return response.status_code == 200

    def refresh_tiles(self, cursor, uid, layer, zooms, bbox, context=None):
        """
        Refreshes the cache of the TMS tiles

        :param cursor: Database cursor
        :param uid: User id
        :param layer: layer to refresh
        :param zooms: zooms to refresh
        :param bbox: bounding box of the zone (west, south, east, north) in WSG84
        :param context: OpenERP context
        :return: None
        """
        import mercantile
        import requests

        config_obj = self.pool.get("res.config")
        gis_base_url = config_obj.get(
            cursor, uid, 'giscegis_gis_url', 'http://localhost'
        )

        west, south, east, north = bbox
        vals = mercantile.tiles(west, south, east, north, zooms, truncate=False)
        for val in vals:
            dirty_url = "/tms/{}/{}/{}/{}.png/dirty".format(layer, val.z, val.x, val.y)
            final_url = requests.compat.urljoin(gis_base_url, dirty_url)
            requests.get(final_url)

    def truncate_table(self, cursor, uid, table):
        """
        Truncates the content of the table

        :param cursor: Database cursor
        :param uid: User id
        :param table: Table name
        :return: True
        """

        cursor.execute('TRUNCATE TABLE %(table)s', {"table": AsIs(table)})
        return True

    @staticmethod
    def to_wkt_point(point):
        """
        Converts a point to WKT

        :param point: List with a pair with x and y
        :return: WKT of the point
        """

        return 'POINT({0} {1})'.format(point[0][0][0], point[0][0][1])

    @staticmethod
    def to_wkt_multilinestring(line):
        points = []
        for point in line[0]:
            points.append(' '.join(map(str, point)))
        return 'MULTILINESTRING(({0}))'.format(','.join(points))

    def add_lat_geometry(self, cursor, uid, identifier, geom,
                         geom_format='WKT'):
        """
        Adds a geometry on giscegis_lat

        :param cursor: OpenERP cursor
        :param uid: User id
        :param identifier: id field to link with
        giscedata_at
        :param geom: Geometry as WKT by default
        :param geom_format: Format of the geometry. Allowed formats(WKT)

        :return: None
        """

        srid = self.get_srid(cursor, uid)
        if geom_format.lower() == 'wkt':
            sql = """
            INSERT INTO giscegis_lat (id, geom)
            VALUES (%s,ST_SetSRID(ST_Multi(ST_GeomFromText(%s)),%s))
            """
            cursor.execute(sql, (identifier, geom, srid))

    def clear_lat(self, cursor, uid):
        """
        Truncates the table giscegis_lat

        :param cursor: Database cursor
        :param uid: User identifier
        :return: True
        """
        sql = """
        TRUNCATE TABLE giscegis_lat;
        """
        cursor.execute(sql)
        return True

    def add_lbt_geometry(self, cursor, uid, identifier, geom,
                         geom_format='WKT'):
        """
        Adds a geometry on giscegis_lbt

        :param cursor: OpenERP cursor
        :param uid: User id
        :param identifier: id field to link with
        giscedata_at
        :param geom: Geometry as WKT by default
        :param geom_format: Format of the geometry. Allowed formats(WKT)

        :return: None
        """

        srid = self.get_srid(cursor, uid)
        if geom_format.lower() == 'wkt':
            sql = """
            INSERT INTO giscegis_lbt (id, geom)
            VALUES (%s,ST_SetSRID(ST_Multi(ST_GeomFromText(%s)),%s))
            """
            cursor.execute(sql, (identifier, geom, srid))

    def clear_lbt(self, cursor, uid):
        """
        Truncates the table giscegis_lbt

        :param cursor: Database cursor
        :param uid: User identifier
        :return: True
        """
        sql = """
        TRUNCATE TABLE giscegis_lbt;
        """
        cursor.execute(sql)
        return True

    def sanitize_record(self, record, field_format='float'):
        """
        Sanitizes a record

        :return: Sanitized record
        """

        if field_format == "float":
            func = float
        elif field_format == "int":
            func = int
        elif field_format == "text":
            func = str
        try:
            return str(func(record))
        except ValueError:
            return None

    def get_layers(self, cursor, uid, context=None):
        """
        Returnts the list of the layers

        :param cursor: OpenERP Cursor
        :param uid: User id
        :param context: Open ERP context
        :return: List of the layers
        """
        sql = """
        SELECT name
        FROM giscegis_layers
        """
        cursor.execute(sql)
        res = cursor.fetchall()
        return [i[0] for i in res]

    def switch_table(self, cursor, uid, origin_table, destination_table,
                     context=None):
        """
        Switches two tables

        :param cursor: Database cursor
        :param uid: User id
        :param origin_table:
        :param destination_table:
        :param context: OpenERP context
        :return:
        """

        logger = netsvc.Logger()
        message = "Switching table from {} to {}".format(origin_table, destination_table)
        logger.notifyChannel(
            "base.geom", netsvc.LOG_INFO, message)
        switch_sql = """
        TRUNCATE TABLE %(destination)s;
        INSERT INTO %(destination)s  SELECT * FROM %(origin)s;
        DROP TABLE IF EXISTS %(origin)s;
        """
        data = {
            "origin": AsIs(origin_table),
            "destination": AsIs(destination_table)
        }
        cursor.execute(switch_sql, data)

    def has_descarrecs(self, cursor, uid, context=None):
        """
        Returns if the module descarrecs is installed

        :param cursor: Database cursor
        :param uid: User id
        :param context: OpenERP context
        :return: Boolean
        """

        return self.has_module(cursor, uid, 'giscedata_descarrecs')

    def has_qualitat(self, cursor, uid, context=None):
        """
        Returns if the module qualitat is installed

        :param cursor: Database cursor
        :param uid: User id
        :param context: OpenERP context
        :return: Boolean
        """
        return self.has_module(cursor, uid, 'giscedata_qualitat')

    def has_module(self, cursor, uid, module, context=None):
        """
        Returns if the module qualitat is installed

        :param cursor: Database cursor
        :param uid: User id
        :param context: OpenERP context
        :return: Boolean
        """
        if context is None:
            context = {}

        mod_obj = self.pool.get('ir.module.module')
        search_params = [('state', '=', 'installed'),
                         ('name', '=', module)]
        return bool(mod_obj.search_count(cursor, 1, search_params))

    def load_shp(self, cursor, uid, table, shp_data, dbf_data, fields_desc,
                 fields_alias=None, end_function=None, ignore_id=False,
                 context=None):
        """
        Loads a SHP into a table

        :param cursor: Database cursor
        :param uid: User id
        :param table: Table to update
        :param shp_data: Data of the shp file
        :type shp_data: str
        :param dbf_data: Data of the dbf file
        :type dbf_data: str
        :param fields_desc: Dict with the fields to load
        :type fields_desc: dict(key, type)
        :param fields_alias: Alias of names
        :type fields_alias: dict
        :param end_function: End function to execute
        :type end_function: tuple (module, method,(list of arguments))
        :param context: OpenERP context
        :type context: dict
        :param ignore_id: Ignore id field of the shp
        :type context: Boolean by default false
        :return: Number of added elements
        :rtype: int
        """

        base_model = self.pool.get("giscegis.base.geom")
        conf_obj = self.pool.get("res.config")

        if fields_alias is None:
            fields_alias = {}
            for field in fields_desc.keys():
                fields_alias[field] = field

        sf = shapefile.Reader(shp=shp_data, dbf=dbf_data)
        shapes = sf.shapes()
        records = sf.records()
        if not records:
            records = [{}] * len(shapes)

        insert_data = []
        headers = {}
        for index, field in enumerate(sf.fields[1:]):
            headers[field[0].lower()] = index

        srid = int(conf_obj.get(cursor, uid, "giscegis_srid", 25830))

        insert_sql = """
        INSERT INTO %(tablename)s
        """
        insert_sql += "(id," + ",".join(fields_alias.values()) + ",geom)"
        insert_sql += "VALUES (%(id)s,"
        for k in fields_alias.values():
            insert_sql += "%(" + k + ")s,"
        insert_sql += "ST_GeomFromText(%(geom)s,%(srid)s))"

        sql_data = """
          SELECT MAX(id)+1 AS last_index
          FROM {};
        """.format(table)

        cursor.execute(sql_data)
        data = cursor.fetchone()[0]
        if data:
            last_index = data
        else:
            last_index = 0

        for index, shape, record in itertools.izip(itertools.count(), shapes,
                                                   records):
            points = [shape.points]
            if len(shape.points) > 1:
                wkt_geometry = base_model.to_wkt_multilinestring(points)
            elif len(shapes[index].points) == 1:
                wkt_geometry = base_model.to_wkt_point(points)
            else:
                wkt_geometry = None

            field_rows = dict.fromkeys(fields_desc.keys(), None)
            row = {
                "tablename": AsIs(table),
                "geom": wkt_geometry,
                "srid": srid
            }
            row.update(field_rows)

            if headers:
                for field_name, field_type in fields_desc.items():
                    if field_name in headers:
                        value = record[headers[field_name]]
                        dest_field_name = fields_alias[field_name]
                        row[dest_field_name] = base_model.sanitize_record(value, field_type)

            if "id" in headers and not ignore_id:
                row["id"] = records[index][headers["id"]]
            else:
                row["id"] = index + last_index
            insert_data.append(row)
        cursor.executemany(insert_sql, insert_data)

        if end_function is not None:
            obj, func, args = end_function

            obj = self.pool.get(obj)
            func = getattr(obj, func)
            if func is not None:
                func(cursor, uid, *args)

    def prepare_update(self, cursor, uid, context=None):
        """
        Prepares the data dump

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param context: OpenERP context
        :type context: dict
        :return: Failed elements
        """

        logger = logging.getLogger('openerp.prepare_dump')

        logger.info(
            'Preparing Autocad dump by cleaning the following database '
            'tables: {}'.format(DROP_ON_AUTOCAD_DUMP)
        )

        sql = """
            DELETE FROM %(table)s;
        """

        failed = []
        success = []

        for table in DROP_ON_AUTOCAD_DUMP:
            # Try to clean table and put on retry if failed
            try:
                cursor.savepoint('sp_{}'.format(table))
                cursor.execute(sql, {'table': AsIs(table)})
                logger.info(
                    'Table {} cleaned.'.format(table)
                )
                # Retry clean tables after a success if there is any on failed
                if failed:
                    for f_table in failed:
                        try:
                            cursor.savepoint('sp_{}'.format(f_table))
                            cursor.execute(sql, {'table': AsIs(f_table)})
                            logger.info(
                                'Failed table {} finally '
                                'cleaned.'.format(f_table)
                            )
                            success.append(f_table)
                        except Exception:
                            cursor.rollback(savepoint='sp_{}'.format(f_table))
                        finally:
                            cursor.release('sp_{}'.format(f_table))
                    for s_table in success:
                        failed.remove(s_table)
            except Exception as e:
                cursor.rollback(savepoint='sp_{}'.format(table))
                failed.append(table)
            finally:
                cursor.release('sp_{}'.format(table))

        # Show warning for those tables impossible to clean
        if failed:
            logger.warning(
                'Impossible to clean the following tables {}.'.format(failed)
            )
        return failed


GiscegisBaseGeom()


class GiscegisLoadMesage(osv.osv_memory):
    """
    Represents the message to load the shapes
    """

    _name = "giscegis.load.message"
    _template_message = log_message = _("""Hora d'inici: {start_date}\nHora de fi: {end_date}\nUsuari: {user_name}\nEstat: {state}\n\nInformacio\n----------\n{log}""")

    def start_load(self, cursor, uid, ids, context=None):
        """
        Starts the load message

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: Message id
        :type ids: int
        :param context: OpenERP context
        :type context: dict
        :return: None
        """

        if context is None:
            context = {}

        obj_users = self.pool.get("res.users")
        user_data = obj_users.browse(cursor, uid, uid)

        now = time.strftime("%d/%m/%Y %H:%M:%S")
        write_data = {
            "start_date": now,
            "user_name": user_data.name,
            "state": _("En curs")
        }
        self.write(cursor, uid, ids, write_data, context)

    def end_load(self, cursor, uid, ids, state, context=None):
        """
        Finishes the load

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: Message id
        :type ids: int
        :param state: End state
        :type state:str
        :param context:
        :return:
        """

        if context is None:
            context = {}

        now = time.strftime("%d/%m/%Y %H:%M:%S")
        write_data = {
            "end_date": now,
            "state": state
        }
        self.write(cursor, uid, ids, write_data, context)

    def log_message(self, cursor, uid, ids, message, context=None):
        """
        Appends a message on the log

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: Message id
        :type ids: int
        :param message: Message to append to the log
        :type message: str
        :param context: OpenERP context
        :type context: dict
        :return: None
        :rtype: None
        """

        actual_log = self.read(cursor, uid, ids[0], ["log"])[0]["log"]
        write_data = {
            "log": actual_log + message + "\n"
        }
        self.write(cursor, uid, ids, write_data, context)

    def get_message(self, cursor, uid, ids, context=None):
        """
        Gets the formatted message

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: Message ids
        :type ids: int
        :param context: OpenERP context
        :type context: dict
        :return: Formatted text
        :rtype: str
        """

        if context is None:
            context = {}

        return self._template_message.format(**self.read(cursor, uid, ids[0], context)[0])

    _columns = {
        "start_date": fields.text(),
        "end_date": fields.text(),
        "user_name": fields.text(),
        "state": fields.text(),
        "log": fields.text()
    }
    _defaults = {
        "start_date": "",
        "end_date": "",
        "user_name": "",
        "state": "",
        "log": ""
    }


GiscegisLoadMesage()
