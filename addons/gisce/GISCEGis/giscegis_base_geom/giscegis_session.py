# *-* coding: utf-8 *-*
from __future__ import absolute_import
import uuid
import json
import logging
import xmlrpclib
from osv import osv, fields
from shapely import wkt
from giscegis_base_geom.fields import Polygon
from tools.config import config

from osv.orm import AccessError
from tools.translate import _

SESSION_STATES = [
    ("open", "Oberta"),
    ("discarted", "Descartada"),
    ("commited", "Aplicada"),
    ("paused", "Pausada")
]

CHANGE_RESOURCE_STATUS = [
    ('M', 'Modificat'),
    ('A', 'Afegit'),
    ('D', 'Deleted')
]

logger = logging.getLogger('openerp.' + __name__)


class BaseSessionException(xmlrpclib.Fault):

    def __init__(self):
        super(BaseSessionException, self).__init__(
            self.faultCode, self.faultString
        )


class BoundingBoxReservedException(BaseSessionException):
    """This exception is when you try to lock a bounding box and it intersects
    with another active bounding box
    """
    faultCode = 423
    faultString = "This bounding box is already locked by another session"


class ResourceOutOfBboxSessionException(BaseSessionException):
    """This exception is when you try to redimension the bounding box and the
    session contains resources which will be keep out of the new bounding box
    """
    faultCode = 416
    faultString = "A resource is out of bounding box session"


class GiscegisSession(osv.osv):
    _name = 'giscegis.session'

    def get_refresh_config(self):
        refresh_gis = config.get('refresh_gis', False)
        return refresh_gis

    def reserved(self, cursor, uid, bbox, session_id=None, context=None):
        """
        Check if a bounding box is reserved by another one
        :param cursor: Database cursor
        :param uid: User id
        :param bbox: Bounding Box to check if is available.
                     eg. POLYGON((0 0, 10 0, 10 10, 0 10, 0 0))
        :param session_id: Session id (needed if is a redimension)
        :param context: Request context
        :return:
        """
        if context is None:
            context = {}
        search_params = [('bbox', '&&', bbox)]
        if session_id:
            search_params.append(('id', '!=', session_id))
        return self.search_count(cursor, uid, search_params, context=context)

    def create(self, cursor, uid, vals, context=None):
        """
        Create a new Session (always check if is possible to acquire)
        """
        if context is None:
            context = {}
        if self.reserved(cursor, uid, vals['bbox']):
            raise BoundingBoxReservedException
        vals.pop("id", False)
        return super(GiscegisSession, self).create(cursor, uid, vals, context)

    def write(self, cursor, uid, ids, vals, context=None):
        """
        Modifies an existing session, if bbox is changed will check it not
        intersects with another one and all the resources of the session keep
        inside the new bounding box
        """
        if context is None:
            context = {}
        changes_obj = self.pool.get('giscegis.session.changes')
        if 'bbox' in vals:
            for session_id in ids:
                if self.reserved(cursor, uid, vals['bbox'], session_id):
                    raise BoundingBoxReservedException
                # Check also if we have other resources out of the new bounding
                # box
                q = changes_obj.q(cursor, uid)
                sql = q.select(['res_model', 'res_id']).where([
                    ('session_id', '=', session_id)
                ])
                cursor.execute(*sql)
                for res_model, res_id in cursor.fetchall():
                    obj = self.pool.get(res_model)
                    resource = obj.read(
                        cursor, uid, res_id, ['geom'],
                        context={'session': session_id}
                    )
                    bbox = wkt.loads(vals['bbox'])
                    geom = wkt.loads(resource['geom'])
                    if not geom.intersects(bbox):
                        raise ResourceOutOfBboxSessionException
        vals.pop("id", False)
        return super(GiscegisSession, self).write(
            cursor, uid, ids, vals, context
        )

    def merge(self, cursor, uid, ids, context=None):
        """
        Merge the sessions (ids) to the "production" data

        After the merge the session will be deactivated and status changed to
        'commited'

        :param cursor: Database cursor
        :param uid: User id
        :type: uid: int
        :param ids: Session ids to merge
        :type ids: list(int)
        :param context: OpenERP context
        :type context: dict
        :return:
        """

        base_geom = self.pool.get('giscegis.base.geom')
        refresh_gis = self.get_refresh_config()

        if context is None:
            context = {"index": True}
        else:
            context["index"] = True
        changes_obj = self.pool.get('giscegis.session.changes')
        context['reserialize'] = False
        for session_id in ids:
            sql = changes_obj.q(cursor, uid).select(['id']).where([
                ('session_id', '=', session_id)
            ])
            cursor.execute(*sql)
            changes_ids = [x[0] for x in cursor.fetchall()]
            if changes_ids:
                context['reserialize'] = changes_obj.apply(
                    cursor, uid, changes_ids, context
                )
            self.write(cursor, uid, [session_id], {
                'state': 'commited',
                'active': 0
            })

        gis_models = [
            'giscegis.blocs.trafos.reductors.geom',
            'giscegis.trafos.geom',
            'giscegis.cts.geom',
            'giscegis.cups.escomeses.tensio.geom',
            'giscegis.fusibles.caixes.geom',
            'giscegis.interruptorsat.geom',
            'giscegis.interruptorsbt.geom',
            'giscegis.lat.geom',
            'giscegis.lbt.geom',
            'giscegis.nodes.geom'
        ]
        if refresh_gis:
            recreated = False
            for model in gis_models:
                mod_obj = self.pool.get(model)
                if mod_obj:
                    mod_obj.recreate(cursor, uid)
                    recreated = True

            if not recreated:
                raise Exception("Error refreshing materialized views")

            status_cache_at = base_geom.remove_tms_cache(cursor, uid, 'at')
            if not status_cache_at:
                raise Exception("Error cleaning AT layer cache")

            status_cache_bt = base_geom.remove_tms_cache(cursor, uid, 'bt')
            if not status_cache_bt:
                raise Exception("Error cleaning BT layer cache")

            status_cache_nodes = base_geom.remove_tms_cache(cursor, uid, 'nodes')
            if not status_cache_nodes:
                raise Exception("Error cleaning Nodes layer cache")

            status_cache_nodesgrid = base_geom.remove_tms_cache(cursor, uid, 'nodesgrid')
            if not status_cache_nodesgrid:
                raise Exception("Error clening Nodes grid layer cache")

        return True

    def discard(self, cursor, uid, ids, context=None):
        """
        Discard a session.

        After this the session will be deactivated and stated changed to
        'discarted'
        :param cursor:
        :param uid:
        :param ids:
        :param context:
        :return:
        """
        if context is None:
            context = {}
        return self.write(cursor, uid, ids, {
            'state': 'discarted',
            'active': 0
        })

    def _get_last_network_serialization(
            self, cr, uid, ids, field_name, arg, context={}
    ):
        """
        This function gets the ID of the last active serialization of the
        Electrical Network.
        :param cr: Database cursor
        :type cr: Cursor
        :param uid: User ID
        :type uid: int
        :param ids: Elements IDS
        :type ids: list of int
        :param field_name: Not Used
        :param arg: Not Used
        :param context: Not Used
        :return: The passed IDS related with the last Network Serialization
        :rtype: dict[int, (int,str)]
        """

        sql = """
            SELECT id, name
            FROM giscegis_network
            WHERE production_network
            ORDER BY create_date DESC, id DESC
            LIMIT 1
        """
        cr.execute(sql)

        last_network = cr.dictfetchall()

        if last_network:
            res = dict.fromkeys(
                ids, (last_network[0]['id'], last_network[0]['name'])
            )
        else:
            res = dict.fromkeys(ids, False)

        return res

    def reject_changes(self, cursor, uid, ids, context=None):
        """
        Reject the dirty changes of the session.
        If no catalog_id specified all the changes deleted

        :param cursor: Database cursor
        :param uid: User id
        :param ids: Session ids to reject the changes
        :type ids: list(int)
        :param context: OpenERP context if cataleg_id in context will be used
        :return: Number of rejected changes
        :rtype: int
        """

        if context is None:
            context = {}

        if not isinstance(ids, list):
            ids = [ids]

        if "cataleg_id" in context:
            cataleg_id = context.get("cataleg_id")
        else:
            cataleg_id = None

        obj_changes = self.pool.get("giscegis.session.changes")
        ret = 0

        for ident in ids:
            search_params = [
                ("session_id", "=", ident),
                ("dirty", "=", True),
            ]
            if cataleg_id:
                search_params.append(("cataleg_id", "=", cataleg_id))
            chg_ids = obj_changes.search(cursor, uid, search_params)
            obj_changes.unlink(cursor, uid, chg_ids)
            ret += len(chg_ids)
        return ret

    def confirm_changes(self, cursor, uid, ids, context=None):
        """
        Confirms the changes of the session and sets the dirty field
        to false
        If no catalog_id specified all the changes confirmed

        :param cursor: Database cursor
        :param uid: User id
        :param ids: Session ids to confirm the changes
        :type ids: list(int)
        :param cataleg_id: Specific cataleg_id to confirm
        :param context: OpenERP context if cataleg_id in context will be used
        :return: Number of confirmed changes
        :rtype: int
        """

        if context is None:
            context = {}

        if not isinstance(ids, list):
            ids = [ids]

        if "cataleg_id" in context:
            cataleg_id = context.get("cataleg_id")
        else:
            cataleg_id = None

        obj_changes = self.pool.get("giscegis.session.changes")
        ret = 0

        for ident in ids:
            search_params = [
                ("session_id", "=", ident),
                ("dirty", "=", True)
            ]
            if cataleg_id:
                search_params.append(("cataleg_id", "=", cataleg_id))
            chg_ids = obj_changes.search(cursor, uid, search_params)
            obj_changes.write(cursor, uid, chg_ids, {"dirty": False})
            ret += len(chg_ids)
        return ret

    _columns = {
        "name": fields.char("Nom de la sessio", size=256, required=True),
        "bbox": Polygon("Àrea reservada", srid=config.get("srid", 25830), required=True),
        "create_uid": fields.many2one("res.users", "Usuari"),
        "create_date": fields.datetime("Data de creació"),
        "write_uid": fields.many2one("res.users", "Ultim usuari a modificar"),
        "write_date": fields.datetime("Ultima modificacio"),
        "state": fields.selection(SESSION_STATES, string="Estat de la sessio"),
        "ttl": fields.integer("Segons de vida de la sessio"),
        "active": fields.boolean("Actiu"),
        "changes": fields.one2many(
            "giscegis.session.changes", "session_id", "Canvis"
        ),
        "screenshot": fields.binary("Captura"),
        "network_id": fields.function(
            _get_last_network_serialization, method=True, type='many2one',
            relation='giscegis.network', string='Xarxa Serialitzada'
        ),
    }

    _defaults = {
        "active": lambda *a: True,
        "state": lambda *a: "open",
    }


GiscegisSession()


class GiscegisSessionChanges(osv.osv):
    _name = 'giscegis.session.changes'
    _columns = {
        "session_id": fields.many2one("giscegis.session", string="Sessio",
                                      required=True, ondelete="cascade"),
        "res_model": fields.char("Objeto", 64, required=True, select=True),
        "res_id": fields.integer("Id del element", required=True, select=True),
        "data": fields.json(string="Canvis"),
        "create_uid": fields.many2one("res.users", "Usuari"),
        "create_date": fields.datetime("Data de creació"),
        "write_uid": fields.many2one("res.users", "Ultim usuari a modificar"),
        "write_date": fields.datetime("Ultima modificacio"),
        "status": fields.selection(
            CHANGE_RESOURCE_STATUS, "Estatus", required=True
        ),
        "cataleg_id": fields.many2one("giscegis.cataleg.cataleg", "Cataleg"),
        "applied": fields.boolean("Aplicat"),
        "dirty": fields.boolean("Canvi en brut")
    }

    _defaults = {
        "applied": lambda *a: False,
        "dirty": lambda *a: True
    }

    def apply(self, cursor, uid, ids, context=None):
        """
        Applies changes to the "master"
        :param cursor: Cursor databse
        :param uid: User id
        :param ids: Ids of changes to apply
        :type ids: list or int
        :param context: request context
        :return: Return True if the changes applied require a reserialization of
        the network and False otherwise
        :rtype Boolean
        """

        if not ids:
            return False

        if context is None:
            context = {}

        if not isinstance(ids, list):
            ids = [ids]

        changes_obj = self.pool.get("giscegis.session.changes")
        nbm_obj = self.pool.get("giscegis.node.bloc.model")

        # if there is a possibility to use the GREATEST fnct on OOQuery,
        # we can change back to it
        # sql = self.q(cursor, uid).select(
        #     ['id', 'res_model', 'res_id', 'data', 'status', 'session_id', ],
        #     ).where([('id', 'in', ids)]
        # )
        # cursor.execute(*sql)

        sql = """
            SELECT id, GREATEST(create_date, write_date) AS date, session_id,
                   res_model, res_id, status, data, applied, cataleg_id
            FROM giscegis_session_changes
            WHERE id IN %(ids)s
            ORDER BY id ASC;
        """
        cursor.execute(sql, {'ids': tuple(ids)})
        changes_to_apply = cursor.dictfetchall()
        reserialize = False
        for change in changes_to_apply:
            if change['applied']:
                continue
            catalog_obj = self.pool.get('giscegis.cataleg.cataleg')
            if change['cataleg_id']:
                not_topology = catalog_obj.read(
                    cursor, uid, change['cataleg_id'], ['not_topology']
                )['not_topology']
                if not not_topology:
                    reserialize = True
            obj = self.pool.get(change['res_model'])
            if change['data']:
                change['data'] = json.loads(change['data'])
            logger.debug(
                'Applying change [{status}] #{id} from session '
                '#{session_id} to {res_model}:{res_id} -> {data}'.format(
                    **change
                )
            )
            if change['status'] == 'M':
                change['data'].pop("id", False)
                obj.write(
                    cursor, uid, [change['res_id']], change['data']
                )
            elif change['status'] == 'A':
                change['data']['id'] = change['res_id']
                obj_id = obj.search(
                    cursor, uid, [('id', '=', change['res_id'])]
                )
                if not obj_id:
                    obj.create(cursor, uid, change['data'])
            elif change['status'] == 'D':
                # This will prevent NBM error on delete elements that set
                # to null node field
                if change['res_model'] == 'giscegis.nodes':
                    nbm_ids = nbm_obj.search(
                        cursor, uid, [('node', '=', change['res_id'])]
                    )
                    if nbm_ids:
                        nbm_obj.unlink(cursor, uid, nbm_ids)
                obj.unlink(cursor, uid, [change['res_id']])
            changes_obj.write(
                cursor, uid, change['id'], {'applied': True}
            )
        return reserialize


GiscegisSessionChanges()


class SessionSingleSavePoint(object):
    """Execute SQL commads withot effect

    with SessionSingleSavePoint(cursor):
        cursor.execute("UPDATE foo set bar='niu' WHERE id = 1")

    This statement won't have any effect as it is rollbacked on exit the
    with statement
    """

    savepoints = []

    def __init__(self, cursor):
        self.cursor = cursor

    def __enter__(self):
        savepoint_id = 'session_{}'.format(uuid.uuid4()).replace('-', '_')

        if not SessionSingleSavePoint.savepoints:
            self.cursor.savepoint(savepoint_id)

        SessionSingleSavePoint.savepoints.append(savepoint_id)

    def __exit__(self, exc_type, exc_val, exc_tb):
        savepoint_id = SessionSingleSavePoint.savepoints.pop()
        if not SessionSingleSavePoint.savepoints:
            self.cursor.rollback(savepoint=savepoint_id)


class SessionSavePoint(object):
    """Execute SQL commads withot effect

    with SessionSavePoint(cursor):
        cursor.execute("UPDATE foo set bar='niu' WHERE id = 1")

    This statement won't have any effect as it is rollbacked on exit the
    with statement
    """

    def __init__(self, cursor):
        self.cursor = cursor
        self.savepont = 'session_{}'.format(uuid.uuid4()).replace('-', '_')

    def __enter__(self):
        self.cursor.savepoint(self.savepont)

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.cursor.rollback(savepoint=self.savepont)


class SessionManaged(osv.osv):

    @staticmethod
    def get_context_session(context=None):
        if context is None:
            context = {}
        session_id = None
        if 'session' in context:
            try:
                session_id = int(context['session'])
            except ValueError:
                logger.error('Error getting session context to int: {}'.format(
                    context['session']
                ))
        return session_id

    def search(self, cursor, uid, args, offset=0, limit=None, order=None,
               context=None, count=False):
        if context is None:
            context = {}
        session_id = self.get_context_session(context)

        # Process session changes
        if session_id:
            session_obj = self.pool.get('giscegis.session')
            session = session_obj.read(cursor, uid, session_id, ['bbox'])

            # For performance first we discard using polygon overlaps and
            # then we force ST_INTERSECTS
            no_bbox = context.get('no_bbox_search', False)
            if 'geom' in self.fields_get(cursor, uid, ['geom']) and not no_bbox:
                args += [
                    ('geom', '&&', session['bbox']),
                    ('geom', '&&!', session['bbox'])
                ]

            # 1-Buscar els canvis relacionats amb el model i la sessió.
            changes_obj = self.pool.get('giscegis.session.changes')
            q = changes_obj.q(cursor, uid)
            sql = q.select(
                ['res_id', 'status', 'id']
            ).where([('session_id', '=', session_id)])
            cursor.execute(*sql)
            query_result = cursor.dictfetchall()
            change_ids = [x['id'] for x in query_result]

            # 2-Iniciar un savepoint per poder fer rollback dels canvis aplicats
            with SessionSavePoint(cursor):
                # 3-Aplicar els canvis si es que n'hi ha
                if change_ids:
                    changes_obj.apply(cursor, uid, change_ids, context)

                # 4-Buscar els elements amb els canvis ja aplicats
                result = super(SessionManaged, self).search(
                    cursor, uid, args, offset, limit, order, context, count
                )
                # 5-Rollback un cop tenim les ids dels elements "simulats" que
                #   han coincidit amb la cerca
        else:
            result = super(SessionManaged, self).search(
                cursor, uid, args, offset, limit, order, context, count
            )

        return result

    def create(self, cursor, uid, vals, context=None):
        if context is None:
            context = {}
        session_id = self.get_context_session(context)
        cataleg_id = context.get('cataleg', False)
        changes_obj = self.pool.get("giscegis.session.changes")

        if session_id:
            # This is made to avoid duplicated elements due a bug on koo-qgisce
            # On the future this bug must be solved on the Qgisce-side
            if 'geom' not in vals and 'geom' in self._columns:
                return False
            for key in vals:
                if '.' in key:
                    vals.pop(key)
            with SessionSingleSavePoint(cursor):
                search_params = [
                    ("session_id", "=", session_id)
                ]
                changes_ids = changes_obj.search(
                    cursor, uid, search_params, context=context
                )
                changes_obj.apply(cursor, uid, changes_ids, context=context)
                res_id = super(SessionManaged, self).create(
                    cursor, uid, vals, context
                )
            chages_obj = self.pool.get('giscegis.session.changes')
            session_changes_vals = {
                'session_id': session_id,
                'res_model': self._name,
                'res_id': res_id,
                'data': json.dumps(vals),
                'status': 'A',
            }
            if cataleg_id:
                session_changes_vals.update({'cataleg_id': cataleg_id})
            chages_obj.create(cursor, uid, session_changes_vals)
            return res_id
        else:
            return super(SessionManaged, self).create(
                cursor, uid, vals, context
            )

    def write(self, cursor, uid, ids, vals, context=None):
        if context is None:
            context = {}
        changes_obj = self.pool.get('giscegis.session.changes')
        session_id = self.get_context_session(context)
        cataleg_id = context.get('cataleg', False)
        # Mirem si es desactiva un element
        if 'active' in vals and not vals['active']:
            # Mirem si el model te el camp node_id
            if 'node_id' in self._columns:
                # El desvinculem del node que tingui assignat
                vals['node_id'] = False
            elif 'edge_id' in self._columns:
                vals['edge_id'] = False
        if session_id:
            if not ids:
                return True
            if not isinstance(ids, (list, tuple)):
                ids = [ids]
            if ids == [None]:
                raise AccessError(
                    _('AccessError'),
                    _(
                        'You try to write on an record that doesn\'t exist '
                        '(Document type: %s).'
                    ) % self._description
                )
            for key in vals:
                if '.' in key:
                    vals.pop(key)
            for res_id in ids:
                exist = self.search(
                    cursor, uid, [('id', '=', res_id)], context=context
                )
                if not exist:
                    raise AccessError(
                        _('AccessError'),
                        _(
                            'You try to write on an record that doesn\'t exist '
                            '(Document type: %s).'
                        ) % self._description
                    )
                else:
                    session_changes_vals = {
                        'session_id': session_id,
                        'res_model': self._name,
                        'res_id': res_id,
                        'data': json.dumps(vals),
                        'status': 'M'
                    }
                    if cataleg_id:
                        session_changes_vals.update({'cataleg_id': cataleg_id})
                    changes_obj.create(cursor, uid, session_changes_vals)
            return True
        else:
            return super(SessionManaged, self).write(
                cursor, uid, ids, vals, context
            )

    def read(self, cursor, uid, ids, fields=None, context=None,
             load='_classic_read'):
        if not ids:
            return []
        if context is None:
            context = {}
        session_id = self.get_context_session(context)
        if session_id:
            changes_obj = self.pool.get('giscegis.session.changes')
            q = changes_obj.q(cursor, uid)
            sql = q.select(
                ['res_id', 'status', 'id']
            ).where([('session_id', '=', session_id)])
            cursor.execute(*sql)
            query_result = cursor.dictfetchall()
            change_ids = [x['id'] for x in query_result]
            with SessionSavePoint(cursor):
                if change_ids:
                    changes_obj.apply(cursor, uid, change_ids, context)
                data = super(SessionManaged, self).read(
                    cursor, uid, ids, fields, context, load
                )
        else:
            data = super(SessionManaged, self).read(
                cursor, uid, ids, fields, context, load
            )
        return data

    def qread(self, cursor, uid, ids, fields=None, context=None,
              load='_classic_read'):
        """
        Read that let the user read fields using dot notation
        :param cursor: Database Cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :param ids: Ids of the elements to read
        :type ids: list of int
        :param fields: Fields to read
        :type fields: list fo str
        :param context: OpenERP Context
        :type context: dict[str, Any]
        :param load: Specify the kind of read for the relational fields
        :type load: str
        :return: The result of reading the asked fields for the specified ids
        :rtype: list of dict[str, Any]
        """
        if not ids:
            return []

        if isinstance(ids, (int, long)):
            select = [ids]
        else:
            select = ids

        if context is None:
            context = {}

        session_id = self.get_context_session(context)

        if session_id:
            changes_obj = self.pool.get('giscegis.session.changes')
            # Busuqem si hi han SessionChanges a aplicar
            q = changes_obj.q(cursor, uid)
            sql = q.select(
                ['res_id', 'status', 'id']
            ).where([('session_id', '=', session_id)])
            cursor.execute(*sql)
            query_result = cursor.dictfetchall()
            change_ids = [x['id'] for x in query_result]
            # Iniciem TNX
            with SessionSavePoint(cursor):
                # Si hi ha SessionChanges, s'apliquen
                if change_ids:
                    changes_obj.apply(cursor, uid, change_ids, context)
                # Dels fields a llegir, agrupem els que estan en dot notation
                ooquery_fields = [f for f in fields if '.' in f]
                ooquery_data = {}
                # Extraiem de fields els que son en dot notation. Afegim ID
                # als camps en dot notation per a poder casar posteriorment.
                if ooquery_fields:
                    fields = [f for f in fields if f not in ooquery_fields]
                    if 'id' not in ooquery_fields:
                        ooquery_fields.append('id')
                    # Amb OOQuery, llegim els valors en dot notation.
                    q = self.q(cursor, uid)
                    sql = q.select(
                        ooquery_fields
                    ).where([('id', 'in', select)])
                    cursor.execute(*sql)
                    ooquery_data = cursor.dictfetchall()
                    # Treiem l'id dels ooquery_fields
                    if 'id' in ooquery_fields:
                        ooquery_fields.remove('id')
                # Amb el read normal de l'ORM, llegim els camps que no estan en
                # dot notation
                data = super(SessionManaged, self).read(
                    cursor, uid, select, fields, context, load
                )
                # Casem els resultats del READ i de l'OOQuery a partir de l'ID
                for row in data:
                    if ooquery_data:
                        tmp = next(
                            (
                                elem for elem in ooquery_data
                                if elem["id"] == row['id']
                            ),
                            {}
                        )
                        # Si tmp esta ple, hem pogut casar un element
                        # d'ooquery_data amb un de data
                        if tmp:
                            for key in tmp:
                                if tmp[key] is None:
                                    tmp[key] = False
                            row.update(tmp)
                        # Else omplir els camps d'OOQuery demanats amb false.
                        else:
                            row.update(dict.fromkeys(ooquery_fields, False))
                    elif ooquery_fields:
                        row.update(dict.fromkeys(ooquery_fields, False))
        else:
            # Dels fields a llegir, agrupem els que estan en dot notation
            ooquery_fields = [f for f in fields if '.' in f]
            ooquery_data = {}
            # Extraiem de fields els que son en dot notation. Afegim ID
            # als camps en dot notation per a poder casar posteriorment.
            if ooquery_fields:
                fields = [f for f in fields if f not in ooquery_fields]
                if 'id' not in ooquery_fields:
                    ooquery_fields.append('id')
                # Amb OOQuery, llegim els valors en dot notation.
                q = self.q(cursor, uid)
                sql = q.select(
                    ooquery_fields
                ).where([('id', 'in', select)])
                cursor.execute(*sql)
                ooquery_data = cursor.dictfetchall()
                # Treiem l'id dels ooquery_fields
                if 'id' in ooquery_fields:
                    ooquery_fields.remove('id')
            # Amb el read normal de l'ORM, llegim els camps que no estan en
            # dot notation
            if fields:
                data = super(SessionManaged, self).read(
                    cursor, uid, select, fields, context, load
                )
            else:
                data = super(SessionManaged, self).read(
                    cursor, uid, select, ['id'], context, load
                )
            # Casem els resultats del READ i de l'OOQuery a partir de l'ID
            for row in data:
                if ooquery_data:
                    tmp = next(
                        (
                            elem for elem in ooquery_data
                            if elem["id"] == row['id']
                        ),
                        {}
                    )
                    # Si tmp esta ple, vol dir que hem pogut casar un element
                    # d'ooquery_data amb un de data
                    if tmp:
                        for key in tmp:
                            if tmp[key] is None:
                                tmp[key] = False
                        row.update(tmp)
                    # Else omplir els camps d'OOQuery demanats amb false.
                    else:
                        row.update(dict.fromkeys(ooquery_fields, False))
                elif ooquery_fields:
                    row.update(dict.fromkeys(ooquery_fields, False))

        if isinstance(ids, (int, long)):
            return data and data[0] or False
        else:
            return data

    def unlink(self, cursor, uid, ids, context=None):
        """
        Unlinks session managed element

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: Elements to unlink
        :type ids: int,list[int]
        :param context: OpenERP context (uses `session` value to indicate
        the session )
        :type context: dict
        :return:
        :rtype: bool
        """
        if not isinstance(ids, list):
            ids = [ids]
        if context is None:
            context = {}
        session_id = self.get_context_session(context)
        cataleg_id = context.get('cataleg', False)
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        if session_id:
            changes_obj = self.pool.get('giscegis.session.changes')
            for res_id in ids:
                active = False
                if 'active' in self._columns:
                    active = self.read(
                        cursor, uid, [res_id], ['active'], context
                    )[0]['active']
                elif self._inherits:
                    ref_model_name = self._inherits.keys()[0]
                    ref_field = self._inherits[ref_model_name]
                    ref_model_obj = self.pool.get(ref_model_name)
                    ref_model_id = self.read(
                        cursor, uid, [res_id], [ref_field], context
                    )[0][ref_field][0]
                    if 'active' in ref_model_obj._columns:
                        active = ref_model_obj.read(
                            cursor, uid, [ref_model_id], ['active'], context
                        )[0]['active']

                session_changes_vals = {
                    'session_id': session_id,
                    'res_model': self._name,
                    'res_id': res_id,
                    'status': 'D'
                }
                if cataleg_id:
                    session_changes_vals.update({'cataleg_id': cataleg_id})
                if active:
                    changes_obj.create(cursor, uid, session_changes_vals)
            return True
        else:
            for res_id in ids:
                active = False
                if 'active' in self._columns:
                    active = self.read(
                        cursor, uid, [res_id], ['active'], context
                    )[0]['active']
                elif self._inherits:
                    ref_model_name = self._inherits.keys()[0]
                    ref_field = self._inherits[ref_model_name]
                    ref_model_obj = self.pool.get(ref_model_name)
                    ref_model_id = self.read(
                        cursor, uid, [res_id], [ref_field], context
                    )[0][ref_field][0]
                    if 'active' in ref_model_obj._columns:
                        active = ref_model_obj.read(
                            cursor, uid, [ref_model_id], ['active'], context
                        )[0]['active']
                if active:
                    super(SessionManaged, self).unlink(
                        cursor, uid, [res_id], context
                    )
            return True
