import netsvc
from oopgrade import DataMigration


def migrate(cr, installed_version):
    """
    Migrates manually set variable giscegis_url as done from xml data

    :param cr: Database cursor
    :param installed_version: Installed version of the module
    :return: None
    """

    xml_content = '''<?xml version="1.0" ?>
    <openerp>
        <data noupdate="1">
            <record model="res.config" id="giscegis_gis_url">
                <field name="name">giscegis_gis_url</field>
                <field name="value">0</field>
            </record>
            <record model="res.config" id="giscegis_gis_url">
                <field name="name">giscegis_url</field>
                <field name="value">False</field>
            </record>
        </data>
    </openerp>
    '''
    dm = DataMigration(
        xml_content, cr, 'giscegis_base_geom',
        search_params={'res_config': ['name']}
    )
    dm.migrate()
