# coding=utf-8

import logging


def up(cursor, installed_version):

    logger = logging.getLogger('openerp.migration')

    logger.info('Standardizing Qgis Session \'bbox\' field precision.')

    sql = """
        UPDATE giscegis_session SET bbox = St_SnapToGrid(
            ST_Translate(bbox, 0.0000000001, 0.0000000001), 0.0001
        );
    """

    cursor.execute(sql)

    logger.info(
        'Standardization of Qgis Session \'bbox\' field precision '
        'made successfully.'
    )


def down(cursor, installed_version):
    pass


migrate = up
