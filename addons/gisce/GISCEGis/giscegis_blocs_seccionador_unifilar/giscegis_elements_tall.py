from osv import osv
from tools import config


class GiscedataCellesCella(osv.osv):

    _name = 'giscedata.celles.cella'
    _inherit = 'giscedata.celles.cella'

    def fill_geom_sec_uni(self, cursor, uid):
        """
        Function that runs a query to update the field geom of all the
        SeccionadorUnifilar elements
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """

        sql = """
            UPDATE giscedata_celles_cella AS c
            SET geom = bloc.geom
            FROM (
              SELECT codi,
                     st_setsrid(
                        st_makepoint(vertex.x, vertex.y), 
                        %(srid)s
                     ) AS geom
              FROM giscegis_blocs_seccionadorunifilar sec_uni
              LEFT JOIN giscegis_vertex vertex ON sec_uni.vertex = vertex.id
            ) AS bloc
            WHERE c.name = bloc.codi
        """

        cursor.execute(sql, {"srid": config.get("srid", 25830)})

    def fill_rotation_sec_uni(self, cursor, uid):
        """
        Function that runs a query to update the field rotation of all the
        SeccionadorUnifilar elements
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """

        sql = """
            UPDATE giscedata_celles_cella AS c
            SET rotation = bloc.rotation
            FROM (
              SELECT codi,
                     rotation
              FROM giscegis_blocs_seccionadorunifilar
            ) AS bloc
            WHERE c.name = bloc.codi
        """

        cursor.execute(sql)

    def fill_node_id_sec_uni(self, cursor, uid):
        """
        Function that runs a query to update the field node_id of all the
        SeccionadorUnifilar elements
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """

        sql = """
            UPDATE giscedata_celles_cella AS c
            SET node_id = bloc.node
            FROM (
              SELECT codi,
                     node
              FROM giscegis_blocs_seccionadorunifilar
            ) AS bloc
            WHERE c.name = bloc.codi
        """

        cursor.execute(sql)


GiscedataCellesCella()
