# -*- coding: iso-8859-1 -*-
from osv import osv, fields
from giscegis_base_geom.giscegis_base_geom import DROP_ON_AUTOCAD_DUMP

# Subscribe this model to the list to be updated if there is an AutoCAD dump
DROP_ON_AUTOCAD_DUMP.append('giscegis_blocs_seccionadorunifilar')

class giscegis_blocs_seccionador_unifilar_blockname(osv.osv):

    _name = 'giscegis.blocs.seccionador.unifilar.blockname'
    _description = 'Tipus Blocs CTAT'

    def create(self, cr, uid, vals, context={}):
        ids = self.search(cr, uid, [('name', '=', vals['name'])])
        if ids and len(ids):
            return ids[0]
        else:
            return super(osv.osv, self).create(cr, uid, vals)

    _columns = {
      'name': fields.char('BlockName',size=20,required=True),
      'description': fields.char('Descripció',size=255),
    }

    _defaults = {

    }

    _order = "name, id"

giscegis_blocs_seccionador_unifilar_blockname()

class giscegis_blocs_seccionador_unifilar(osv.osv):

    _name = 'giscegis.blocs.seccionadorunifilar'
    _description = 'Blocs Seccionador Unifilar'
    _columns = {
      'name': fields.char('BlockName', size=50, required=True),
      'vertex': fields.many2one('giscegis.vertex', 'Vertex',
                                ondelete='set null'),
      'width': fields.float('Width'),
      'height': fields.float('Height'),
      'rotation': fields.float('Rotation'),
      'codi': fields.char('Codi', size=50),
      'blockname': fields.many2one(
            'giscegis.blocs.seccionador.unifilar.blockname', 'BlockName'),
      'node': fields.many2one('giscegis.nodes', 'Node', ondelete='set null'),
    }

    _defaults = {

    }

    _order = "name, id"

    def create(self, cursor, uid, vals, context=None):
        """
        M�tode sobreescrit per escriure als camps many2one des d'aqui

        Ens hem d'assegurar de que els m�todes create() dels objectes
        referenciats estiguin alhora sobreescrits per tal que comprovin
        si l'objecte existeix o no.

        :param cursor:
        :param uid:
        :param vals:
        :param context:
        :return:
        """

        _fields = [('vertex', 'giscegis.vertex'),
                   ('blockname',
                    'giscegis.blocs.seccionador.unifilar.blockname')]
        for field in _fields:
            if field[0] in vals and isinstance(vals[field[0]], list):
                obj = self.pool.get(field[1])
                remote_id = obj.create(cursor, uid, vals[field[0]][0][2])
                if remote_id:
                    vals[field[0]] = remote_id
        return super(osv.osv, self).create(cursor, uid, vals)

    def write(self, cursor, uid, ids, vals, context=None):
        """
        M�tode sobreescrit per escriure als camps many2one des d'aqu�

        Igual que amb el create() cal assabentar-se de que els write()s dels
        models referenciats estiguin sobreescrits

        :param cursor:
        :param uid:
        :param ids:
        :param vals:
        :param context:
        :return:
        """

        _fields = [('vertex', 'giscegis.vertex'),
                   ('blockname',
                    'giscegis.blocs.seccionador.unifilar.blockname')]
        for field in _fields:
            if field[0] in vals and isinstance(vals[field[0]], list):
                obj = self.pool.get(field[1])
                search_params = []
                for k, v in vals[field[0]][0][2].items():
                    search_params.append((k, '=', v))
                remote_id = obj.search(cursor, uid, search_params)
                if remote_id:
                    vals[field[0]] = remote_id[0]
                else:
                    remote_id = obj.create(cursor, uid, vals[field[0]][0][2])
                    vals[field[0]] = remote_id

        return super(osv.osv, self).write(cursor, uid, ids, vals)

giscegis_blocs_seccionador_unifilar()
