from giscegis_base_index.giscegis_base_index import GiscegisBaseIndex


class GiscedataAtLinia(GiscegisBaseIndex):
    """
    Index for giscedata.at.linia
    """
    _name = 'giscedata.at.linia'
    _inherit = 'giscedata.at.linia'

    def get_lat_lon(self, cursor, uid, item, context=None):
        """
        Gets the latitude and longitude of the tram

        :param cursor: Database cursor
        :param uid: User id
        :param item: Item to index
        :param context: OpenERP context
        :return: Tuple lat,lon
        """
        if context is None:
            context = {}
        cursor.execute("""
        SELECT
            ST_Y(ST_LineInterpolatePoint(
                 ST_GeometryN(st_transform(
                    ST_LineMerge(ST_Union(gv.geom)),4326),1), 0.5)
                 ) AS lat,
            ST_X(ST_LineInterpolatePoint(
                 ST_GeometryN(st_transform(
                    ST_LineMerge(ST_Union(gv.geom)),4326),1), 0.5)
                 ) AS lon
        FROM giscegis_lat_geom gv
        LEFT JOIN giscedata_at_tram t ON t.id=gv.name
        WHERE
            t.linia = %s
        GROUP BY t.linia
        """, (item.id, ))
        res = cursor.fetchone()
        if not res:
            res = (0, 0)
        return res


GiscedataAtLinia()


class GiscedataAtTram(GiscegisBaseIndex):
    """
    Index for giscedata.at.tram
    """
    _name = 'giscedata.at.tram'
    _inherit = 'giscedata.at.tram'

    def get_lat_lon(self, cursor, uid, item, context=None):
        """
        Returns the lat and lon to index the tram AT

        :param cursor: Database cursor
        :param uid: User id
        :param item: Item to index
        :param context: OpenERP context
        :return: Tuple with lat,lon
        """
        if context is None:
            context = {}
        cursor.execute("""
        SELECT
            ST_Y(ST_LineInterpolatePoint(
                 ST_GeometryN(st_transform(geom,4326),1), 0.5)) AS lat,
            ST_X(ST_LineInterpolatePoint(
                 ST_GeometryN(st_transform(geom,4326),1), 0.5)) AS lon
        FROM giscegis_lat_geom
        WHERE
            name = %s
        """, (item.id, ))
        res = cursor.fetchone()
        if not res:
            res = (0, 0)
        return res


GiscedataAtTram()


class GiscedataAtSuport(GiscegisBaseIndex):
    """
    Index for giscedata.at.suport
    """
    _name = 'giscedata.at.suport'
    _inherit = 'giscedata.at.suport'

    def get_lat_lon(self, cursor, uid, item, context=None):
        """
        Gets the latitude and longitude of the support

        :param cursor: Database cursor
        :param uid: User id
        :param item: Item to index
        :param context: OpenERP context
        :return: Tuple lat,lon
        """
        if context is None:
            context = {}
        srid = self.get_db_srid(cursor, uid)
        sql = """
        SELECT
          ST_Y(
            ST_Transform(ST_SetSRID(ST_Point(v.x, v.y), %(srid)s), 4326)
          ) AS lat,
          ST_X(
            ST_Transform(ST_SetSRID(ST_Point(v.x, v.y), %(srid)s), 4326)
          ) AS lon
        FROM giscedata_at_suport AS s
        LEFT JOIN giscegis_blocs_suports_at AS sg ON sg.numsuport=s.name
        LEFT JOIN giscegis_vertex AS v ON v.id = sg.vertex
        WHERE
          s.id = %(id)s
        """
        cursor.execute(sql, {'id': item.id, 'srid': srid})
        res = cursor.fetchone()
        if not res or not res[0]:
            res = (0, 0)

        return res


GiscedataAtSuport()
