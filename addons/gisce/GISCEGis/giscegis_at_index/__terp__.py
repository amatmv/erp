# -*- coding: utf-8 -*-
{
    "name": "Index LAT per a GIS",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Index giscedata_at
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "giscedata_at_index",
        "giscegis_lat_geom",
        "giscegis_search_type"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscegis_search_type_data.xml"
    ],
    "active": False,
    "installable": True
}
