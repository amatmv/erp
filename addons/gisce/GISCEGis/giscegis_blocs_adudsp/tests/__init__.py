# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction

class TestAdudspCoord(testing.OOTestCase):
    def setUp(self):
        self.txn = Transaction().start(self.database)

    def test_retrieve_trafos_coord(self):
        """Test if the model retrieve correctly the coord values"""
        cursor = self.txn.cursor
        uid = self.txn.user

        img_obj = self.openerp.pool.get('ir.model.data')
        adudsp_obj = self.openerp.pool.get('giscegis.blocs.adudsp')

        adudsp_id = img_obj.get_object_reference(
            cursor, uid, 'giscegis_blocs_adudsp', 'bloc_adudsp_01'
        )

        adudsp = adudsp_obj.read(cursor, uid, adudsp_id[1], ['x','y'])
        self.assertEqual(adudsp['x'], 3333333333.0)
        self.assertEqual(adudsp['y'], 4444444444.0)

    def tearDown(self):
        self.txn.stop()