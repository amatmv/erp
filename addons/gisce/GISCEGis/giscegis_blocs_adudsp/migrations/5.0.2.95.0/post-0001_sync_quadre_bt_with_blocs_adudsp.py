# coding=utf-8

import logging
import pooler


def up(cursor, installed_version):

    logger = logging.getLogger('openerp.migration')

    logger.info(
        'Sync BtCaixa with BlocsAdudsp'
    )

    pool = pooler.get_pool(cursor.dbname)
    bt_caixa = pool.get('giscedata.bt.caixa')
    n_created_elements = bt_caixa.sync_elements_from_blocs(cursor, 1)

    if n_created_elements == 1:
        msg = "1 BtCaixa sync correctly with BlocsAdudsp"
    else:
        msg = "{} BtCaixa sync correctly with BlocsAdudsp".format(
            n_created_elements
        )

    logger.info(msg)


def down(cursor, installed_version):
    pass


migrate = up
