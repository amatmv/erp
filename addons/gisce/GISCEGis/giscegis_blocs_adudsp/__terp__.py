# -*- coding: utf-8 -*-
{
    "name": "GISCEGis Blocs ADU DSP",
    "description": """Model pels blocs ADU i DSP""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GIS",
    "depends":[
        "base",
        "giscedata_bt_caixes",
        "giscegis_base",
        "giscegis_nodes",
        "giscegis_bt_caixes"
    ],
    "init_xml": [],
    "demo_xml":[
        "giscegis_blocs_adudsp_demo.xml"
    ],
    "update_xml":[
        "giscegis_blocs_adudsp_view.xml",
        "ir.model.access.csv",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
