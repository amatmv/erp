# -*- coding: utf-8 -*-
from osv import fields, osv
from giscegis_base_geom.giscegis_base_geom import DROP_ON_AUTOCAD_DUMP

# Subscribe this model to the list to be updated if there is an AutoCAD dump
DROP_ON_AUTOCAD_DUMP.append('giscegis_blocs_adudsp')


class giscedata_bt_caixes_blockname(osv.osv):
    _name = 'giscedata.bt.caixes.blockname'
    _inherit = 'giscedata.bt.caixes.blockname'

    def create(self, cr, uid, vals, context={}):
        ids = self.search(cr, uid, [('name', '=', vals['name'])])
        if ids and len(ids):
            return ids[0]
        else:
            return super(osv.osv, self).create(cr, uid, vals)

giscedata_bt_caixes_blockname()


class giscegis_blocs_adudsp(osv.osv):
    _name = 'giscegis.blocs.adudsp'
    _description = 'GISCEGis Blocs per ADU i DSP'

    def _coords(self, cursor, uid, ids, field_name, arg, context):
        """
        Returns the x and y of the CT

        :param cursor: Database cursor
        :param uid: User id
        :param ids: Cts ids
        :type ids: list
        :param field_name: Asked fields
        :param arg:
        :param context: OpenERP context
        :type context: dict
        :return: X and y of the CTs
        :rtype: dict
        """
        if isinstance(ids, int):
            ids = [ids]
        ret = {}
        sql = """
        SELECT adudsp.id, vertex.x, vertex.y FROM giscegis_blocs_adudsp adudsp
          LEFT JOIN giscegis_vertex vertex ON adudsp.vertex = vertex.id   
        WHERE adudsp.id IN %s;
        """
        cursor.execute(sql, (tuple(ids),))
        data = cursor.fetchall()
        for line in data:
            ret[line[0]] = {"x": line[1], "y": line[2]}
            ids.remove(line[0])
        for identifier in ids:
            ret[identifier] = {"x": None, "y": None}
        return ret

    def _get_observacions(self, cursor, uid, ids, field_name, arg, context):
        """
        Returns 'observacions' field from giscedata_bt_caixa

        :param cursor: Database cursor
        :param uid: User id
        :param ids: adudsp ids
        :type ids: list
        :param field_name: Asked fields
        :param arg:
        :param context: OpenERP context
        :type context: dict
        :return: Observacions
        :rtype: dict
        """
        if isinstance(ids, int):
            ids = [ids]
        ret = {}
        sql = """
        SELECT 
            adu.id, caixa.observacions
        FROM giscegis_blocs_adudsp adu 
            LEFT JOIN giscedata_bt_caixa caixa ON caixa.codi = adu.codi
        WHERE adu.id IN %s;
        """
        cursor.execute(sql, (tuple(ids),))
        data = cursor.fetchall()
        for caixa in data:
            ret[caixa[0]] = caixa[1]
        return ret

    def _filter_bt_ids(self, cursor, uid, ids, context=None):
        cursor.execute("""
        SELECT 
            adu.id
        FROM giscegis_blocs_adudsp adu 
            LEFT JOIN giscedata_bt_caixa caixa ON caixa.codi = adu.codi
        WHERE caixa.id IN %s
        """, (tuple(ids), ))
        print("Fooo")
        return [x[0] for x in cursor.fetchall()]

    _columns = {
        'name': fields.char('Codi', size=50, required=True),
        'vertex': fields.many2one('giscegis.vertex', 'Vertex',
                                  ondelete='set null'),
        'width': fields.float('Width'),
        'height': fields.float('Height'),
        'rotation': fields.float('Rotation'),
        'blockname': fields.many2one(
            'giscedata.bt.caixes.blockname', 'Blockname'
        ),
        'node': fields.many2one('giscegis.nodes', 'Node', ondelete='set null'),
        'codi': fields.char('Codi', size=50),
        'intensitat_fusible': fields.integer('Intensitat'),
        'caixa': fields.many2one('giscedata.bt.caixes', 'Caixa'),
        'x': fields.function(_coords, method=True, type='float', string='x',
                             multi="coords"),
        'y': fields.function(_coords, method=True, type='float', string='y',
                             multi="coords"),
        'observacions': fields.function(_get_observacions, method=True, type='text', string='Observacions', store={
            'giscedata.bt.caixa': (_filter_bt_ids, ['observacions'], 10)
        }),
    }

    def create(self, cursor, uid, vals, context=None):
        """Mètode sobreescrit per escriure als camps many2one des
        d'aquí.

        Ens hem d'assegurar de què els mètodes create() dels objectes
        referenciats estiguin alhora sobreescrits per tal què comprovin
        si l'objecte existeix o no."""
        _fields = [('vertex', 'giscegis.vertex'),
                   ('blockname', 'giscedata.bt.caixes.blockname')]
        for field in _fields:
            if field[0] in vals and isinstance(vals[field[0]], list):
                obj = self.pool.get(field[1])
                remote_id = obj.create(cursor, uid, vals[field[0]][0][2])
                if remote_id:
                    vals[field[0]] = remote_id
        return super(osv.osv, self).create(cursor, uid, vals)


giscegis_blocs_adudsp()
