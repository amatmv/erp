# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Simulació BT",
    "description": """Simulacions BT""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "giscegis_topologia",
        "giscedata_cups",
        "base",
        "giscegis_configsimulacio_bt",
        "giscegis_configdefault_bt",
        "giscegis_blocs_maniobrables_bt",
        "giscegis_blocs_stop_nodes_bt",
        "giscegis_base",
        "giscegis_blocs_transformadors",
        "giscegis_lbt_geom",
        "giscegis_lat_geom",
        "giscegis_at",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "ir.model.access.csv",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
