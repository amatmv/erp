# *-* coding: utf-8 *-*

"""
:mod:`giscegis_simulacio_bt` - Simulacions en Baixa Tensió
============================================================

"""

from osv import osv, fields

from tools.sql import is_postgis_db
import logging
from networkx import Graph
from tools import config
import six


class GiscegisSimulacioBt(osv.osv):
    """
    Classe sense taula amb les funcions per la simulacio
    de la xarxa BT
    """

    _name = 'giscegis.simulacio.bt'
    _auto = False

    _columns = {}

    open_nodes = []
    stop_nodes = []

    def init(self, cr):
        """
        Loads stop_nodes and open_nodes

        :param cr: Database cursor
        :return: None
        """

        self.stop_nodes = self.get_stop_nodes(cr, 1)
        self.open_nodes = self.get_open_nodes(cr, 1)

    def get_stop_nodes(self, cursor, uid):
        """
        Returns a list of stop nodes

        :param cursor: Database cursor
        :param uid: User id
        :return: List of stop nodes
        :rtype: list(int)
        """

        ir_mod_obj = self.pool.get("ir.model")
        stop_obj = self.pool.get("giscegis.blocs.stop.nodes.bt")

        sql_nodes_no_domain = """
            SELECT node
            FROM giscegis_node_bloc_model
            WHERE model IN (
                SELECT name
                FROM giscegis_blocs_stop_nodes_bt
                WHERE
                    domain IS NOT NULL AND
                    domain = '[]'
                );
        """

        cursor.execute(sql_nodes_no_domain)
        ret = [a[0] for a in cursor.fetchall() if a[0] is not None]

        sql_nodes_domain = """
            SELECT
                node,
                res_id,
                model
            FROM giscegis_node_bloc_model
            WHERE model IN (
                SELECT name
                FROM giscegis_blocs_stop_nodes_bt
                WHERE
                    domain IS NOT NULL AND
                    domain != '[]'
            )
        """
        cursor.execute(sql_nodes_domain)

        domain_nodes = {}

        result = cursor.fetchall()

        for res in result:
            if res[2] not in domain_nodes:
                domain_nodes[res[2]] = [(res[0], res[1])]
            else:
                domain_nodes[res[2]].append((res[0], res[1]))

        for model_id in domain_nodes.keys():
            model_name = ir_mod_obj.read(model_id, ["model"])["model"]
            mod_obj = self.pool.get(model_name)
            domain = eval(stop_obj.read(model_id, ["domain"]))
            sql = mod_obj.q(cursor, 1).select(['id']).where(domain)
            inc_ids = [i[0] for i in cursor.execute(sql)]
            for res in domain_nodes[model_id]:
                if res[1] in inc_ids:
                    ret.append(res[0])
        return ret

    def get_open_nodes(self, cursor, uid):
        """
        Returns the open nodes

        :param cursor:
        :param uid:
        :return: List of nodes ids
        :rtype: list(int)
        """

        sql_config = """
                SELECT node
                FROM giscegis_configdefault_bt
                WHERE node IS NOT NULL;
                """
        cursor.execute(sql_config)

        # Comprovem el None per si hi ha entrats interruptors
        # que no s'han exportat!
        ret = [a[0] for a in cursor.fetchall() if a[0] is not None]
        if ret:
            return ret
        else:
            return []

    def get_simulation_geom(self, cursor, uid, resultat, method='poly', srid=False):
        """
        Function to generate the geometries of the simulation

        :param cursor: Database cursor
        :param uid: User id
        :param resultat: Result of the simulation
        :param method: postgis or poly
         poly: polyline for gis 1.x
         postgis GeoJSON for gis v3
        :param srid: SRID
        :return: GeoJSON
        """

        sentry = self.pool.get('sentry.setup')

        edge_obj = self.pool.get('giscegis.edge')

        # Generate polylines from edge id's
        trams = {'trams_on': [], 'trams_off': []}
        for list_name in ['trams_on', 'trams_off']:
            for edge_id in resultat[list_name]:
                if method == 'postgis':
                    element = edge_obj.get_geom(cursor, uid, edge_id, srid)
                    if element is None:
                        msg = 'Simulacio amb edge(id:{}) None a {} '.format(
                            list_name, edge_id)
                        logging.warn(msg)
                        if sentry is not None:
                            sentry.client.captureMessage(msg, level=logging.WARNING)
                    trams[list_name].append(element)
                else:
                    trams[list_name].append(
                        edge_obj.get_edge_polyline(cursor, uid, edge_id))

            resultat.update({list_name: trams[list_name]})

        return resultat

    def _subgraf(self, G, source, stop_nodes):
        nlist = [source]
        seen = {}
        pre = []
        for source in nlist:
            if source in seen: continue
            lifo = [source]
            while lifo:
                v = lifo.pop()
                if v in seen:
                    continue
                pre.append(v)
                seen[v] = True
                if v.id not in stop_nodes:
                    lifo.extend((w for w in G.neighbors(v) if w not in seen))
        return G.subgraph(pre)

    def subgraf(self, G, source, extra_stop=None):
        if not extra_stop:
            extra_stop = []
        nlist = [source]  # only process component with source
        neighbors = G.neighbors

        seen = {} # nodes seen
        pre = []  # list of nodes in a DFS preorder
        for source in nlist:
            if source in seen:
                continue
            lifo = [source]
            while lifo:
                v = lifo.pop()
                # Condicions de sortida
                # Si el node és del tipus 'stop' o està obert degut a l'estat
                # de la xarxa
                if v.id in self.stop_nodes or v.id in self.open_nodes or v.id in extra_stop:
                    pre.append(v)
                    continue
                if v in seen:
                    continue
                pre.append(v)
                seen[v] = True
                lifo.extend((w for w in G.neighbors(v) if w not in seen))
        return Graph(G.subgraph(pre))

    def dfs_continuitat(self, G, source, sim_open_nodes=[]):
        nlist = [source]  # only process component with source
        neighbors = G.neighbors

        seen = {} # nodes seen
        pre = []  # list of nodes in a DFS preorder
        for source in nlist:
            if source in seen: continue
            lifo = [source]
            while lifo:
                v = lifo.pop()
                # Condicions de sortida
                # Si està obert per la simulació
                if v.id in sim_open_nodes:
                    pre.append(v)
                    continue
                if v in seen: continue
                pre.append(v)
                seen[v] = True
                lifo.extend((w for w in neighbors(v) if w not in seen))
        return G.subgraph(pre)

    def dfs_escomeses_traceability(self, cr, uid, source):
        """
        Aquest mètode actualitzarà la taula giscegis_escomeses_traceability segons el
        (nou) estat de la xarxa.
        Es recorre el graf en profunditat i es va guardant el path per on es va passant,
        quan s'arriba a una escomesa, es guarda a la BD tot el path CT-Escomesa.
        @G networkx Graph, el graf de la xarxa
        @source id del ct (giscegis.blocs.ctat)
        """
        self.init(cr)
        ct_obj = self.pool.get('giscegis.blocs.ctat').browse(cr, uid, source)
        if ct_obj.node:
            source = self.pool.get('giscegis.nodes').browse(cr, uid, ct_obj.node.id)
        else:
            return -1
        G = self.pool.get('giscegis.topologia').load(cr, uid)
        nlist = []
        neighbors = G.neighbors
        seen = {}
        pre = []
        pila = []
        # camps per cada una de les escomeses
        vals = {} # sempre guardarà objectes, mai ID's
        vals_w = {} # contindrà ID's, amb el què escriurem
        vals['escomesa'] = 0
        vals['fusible'] = 0
        vals['tensio'] = 0
        vals['trafo'] = 0
        store_models = {
            'giscegis.blocs.ctat': 'ct',
            'giscegis.blocs.fusiblesbt': 'fusible',
            'giscegis.blocs.tensio': 'tensio',
            'giscegis.blocs.transformadors': 'trafo',
            'giscegis.blocs.escomeses': 'escomesa'
        }
        conta_ats = ['CONTA-AT', 'CONTA-ATC', 'CONTA-ATD', 'CONTA-CH']
        gnbm_obj = self.pool.get('giscegis.node.bloc.model')
        model_obj = self.pool.get('ir.model')
        get_obj = self.pool.get('giscegis.escomeses.traceability')
        ct_bloc_id = source.get_bloc().id
        ct = self.pool.get('giscegis.blocs.ctat').read(cr, uid, [ct_bloc_id], ['ct', 'node'])
        write = True
        if not ct[0]['ct']:
            return -1
        vals['ct'] = self.pool.get('giscegis.nodes').browse(cr, uid, ct[0]['node'][0])
        # Busquem els trafos del CT i construim grafs
        trafo_blocs = self.pool.get('giscegis.blocs.transformadors').search(cr, uid, [('ct', '=', ct[0]['ct'][0])])
        for trafo_bloc in self.pool.get('giscegis.blocs.transformadors').browse(cr, uid, trafo_blocs):
            vals['trafo'] = trafo_bloc
            nlist = [trafo_bloc.node]
            seen = {}
            pre = []
            for source in nlist:
                if source in seen: continue
                lifo = [source]
                while lifo:
                    v = lifo.pop()
                    if v in seen: continue
                    pre.append(v)
                    model = v.get_model()
                    if model:
                        model_name = model.model.model
                    else:
                        model_name = 'unknown'
                    if model_name == 'giscegis.blocs.tensio': vals['tensio'] = v.id
                    elif model_name == 'giscegis.blocs.fusiblesbt':
                        vals['fusible'] = v
                        # ara agafar el que penja de v
                        Gcopy = G.copy()
                        Gcopy.delete_nodes_from(seen.keys(), True) # eliminem tot el que hem vist per trencar l'edge cap amunt
                        Gfus = self.subgraf(G, v) # fem el subgraf partint de v, ha de quedar només el que penja de v
                        for n in Gfus.nodes():
                            model = n.get_model()
                            if model:
                                model_name = model.model.model
                            else:
                                model_name = 'unknown'
                            if model_name == 'giscegis.blocs.escomeses':
                                vals['escomesa'] = n.get_bloc().escomesa
                            seen[n] = True
                    seen[v] = True
                    if v.id not in self.stop_nodes and v.id not in self.open_nodes:
                        lifo.extend((w for w in G.neighbors(v) if w not in seen))
                    # aquí comença la chicha
                    # 2
                    if len(pila) > 0:
                        # 3
                        if model_name == 'giscegis.blocs.escomeses':
                            # mirar que NO sigui CONTA-AT! Hi ha algun darrera d'algun trafo...
                            bloc_escomesa = v.get_bloc()
                            if v.get_bloc().blockname.name not in conta_ats:
                                # 4
                                pare = self._intersection(cr, uid, Sg.neighbors(v), seen)[0]
                                # 5
                                while pare != pila[-1]:
                                    vv = pila.pop()
                                    # 6
                                    mmodel = vv.get_model()
                                    mmodel_name = ''
                                    if mmodel:
                                        mmodel_name = mmodel.model.model
                                    if mmodel_name in store_models.keys() and vals.has_key(store_models[mmodel_name]) and vv == vals[store_models[mmodel_name]]:
                                        del vals[store_models[mmodel_name]]
                                # 7
                                ids = get_obj.search(cr, uid, [('escomesa', '=', vals['escomesa'].get_bloc().escomesa.id)])
                                if vals.has_key('ct'):
                                    vals_w['ct'] = vals['ct'].get_bloc().ct.id
                                if vals.has_key('escomesa'):
                                    vals_w['escomesa'] = vals['escomesa'].get_bloc().escomesa.id
                                if vals.has_key('fusible'):
                                    vals_w['fusible'] = vals['fusible'].get_bloc().id
                                if vals.has_key('tensio'):
                                    vals_w['tensio'] = vals['tensio'].get_bloc().tensio
                                if vals.has_key('trafo'):
                                    # buscar el transformador del data segons
                                    # ct i ordre_dins_ct
                                    # (ordre_transformador_ct)
                                    data_trafos_obj = self.pool.get('giscedata.transformador.trafo')
                                    bloc_trafo = vals['trafo'].get_bloc()
                                    data_trafo_ids = data_trafos_obj.search(cr, uid, [('ct', '=', bloc_trafo.ct.id), ('ordre_dins_ct', '=', bloc_trafo.ordre_transformador_ct), ('id_estat.codi', '=', 1)])
                                    if data_trafo_ids:
                                        vals_w['trafo'] = data_trafo_ids[0]
                                    else:
                                        print "No s'ha pogut trobar el trafo al data"
                                if len(ids) and vals_w['escomesa'] != 0:
                                    get_obj.write(cr, uid, ids[0], vals_w)
                    pila.append(v)
                    lifo.extend((w for w in Sg.neighbors(v) if w not in seen))
        return 1

    def _intersection(self, cr, uid, a, b):
        int_dict = {}
        a_dict = {}
        for e in a:
            a_dict[e] = 1
        for e in b:
            if a_dict.has_key(e):
                int_dict[e] = 1
        return int_dict.keys()

    def simulacio_ids(
            self, cr, uid, ct, sim_open_nodes=None, context=None, load=False
    ):
        """
        Ens arriba com a paràmetre un CT. Hem de buscar els trafos que té aquest
        CT i fer un subgraf per a cada trafo. Finalment fer una unió dels dos
        grafs
        :param cr: Database cursor
        :type cr: Cursor
        :param uid: User ID
        :type uid: int
        :param ct: CT ID
        :type ct: list of int or int
        :param sim_open_nodes: IDS of the open nodes.
        :type sim_open_nodes: list of int
        :param context: OpenERP context
        :type context: dict[any,any]
        :param load: Not used
        :return: The results of the simulation
        :rtype: dict[any,any]
        """

        # S'ha de guardar els paràmetres de la simulació ja que després
        # s'ha de poder recuperar els resultats en termes de nodes i no
        # només en arestes i vèrtexs.
        if sim_open_nodes is None:
            sim_open_nodes = []

        if context is None:
            context = {}

        vals = {}
        config_obj = self.pool.get('giscegis.configsimulacio.bt')
        trafo_obj = self.pool.get("giscedata.transformador.trafo")
        esc_obj = self.pool.get("giscedata.cups.escomesa")
        bloc_ctat_obj = self.pool.get('giscegis.blocs.ctat')
        open_nodes_obj = self.pool.get('giscegis.configsimulacio.bt.oberts')
        topo_obj = self.pool.get('giscegis.topologia')
        node_obj = self.pool.get('giscegis.nodes')
        gis_escomesa_obj = self.pool.get('giscegis.blocs.escomeses')
        trafo_bloc_obj = self.pool.get('giscegis.blocs.transformadors')
        bt_escomeses_obj = self.pool.get(
            'giscegis.simulacio.bt.resultats.escomeses'
        )

        vals['name'] = self.pool.get('ir.sequence').get(
            cr, uid, 'giscegis.configsimulacio.bt'
        )

        if isinstance(ct,(tuple,list)):
            vals['ct'] = ct[0]
        else:
            vals['ct'] = ct

        config_id = config_obj.create(cr, uid, vals, context)
        config_name = vals['name']

        # Un cop tenim el ID de la configuració, cal guardar la llista de nodes
        # oberts per aquesta simulació (sense tenir en compte els
        # propis de l'estat de la xarxa!)

        vals = {}

        vals['config'] = config_id # la conf. de simulació que acabem de guardar.
        # iterar sobre tots els ids de nodes que s'obren i obtenir el objecte node

        sim_open_nodes_obj = node_obj.read(cr, uid, sim_open_nodes, ['name'])
        for n in sim_open_nodes_obj:
            n_id = n['id']
            vals['name'] = node_obj.get_codi(cr, uid, n_id) or n['name']
            vals['node'] = n_id
            open_nodes_obj.create(cr, uid, vals, context)

        # guardar els resultats a giscegis_simulacio_bt_resultats (config_id)
        vals = {}
        sql = """
          SELECT count(id) AS n
          FROM giscegis_simulacio_bt_resultats
          WHERE id = %s
        """
        cr.execute(sql, (int(config_id),))
        res = cr.dictfetchone()

        bt_resultats_obj = self.pool.get('giscegis.simulacio.bt.resultats')
        vals = {
            'name': '%s_%i' % (config_name, res['n']+1),
            'simulacio': config_id
        }
        bt_resultats_id = bt_resultats_obj.create(cr, uid, vals, context)

        # Inicialització
        self.init(cr)

        # Carreguem topologia
        G = topo_obj.load(cr, uid, context)

        # Busquem els tots trafos (blocs) que
        # pertanyen al CT que ens ha enviat per simular
        trafo_bloc_ids = trafo_bloc_obj.search(cr, uid, [('ct', '=', ct)], context)
        trams_on = []
        trams_off = []
        nodes_on = []
        nodes_off = []
        trafos_ct = trafo_bloc_obj.browse(cr, uid, trafo_bloc_ids, context)
        # Els nodes del trafos del ct
        if trafos_ct:
            nodes_trf = [t.node.id for t in trafos_ct]
        else:
            trafos_ids = trafo_obj.search(cr, uid, [("ct", "=", ct)], context=context)
            nodes_trf = [trafo["node_id"][0] for trafo in trafo_obj.read(cr, uid, trafos_ids, ["node_id"])]

        # Per cada trafo crearem un subgraf_bt a partir del
        # seu node (trafo_bloc.node.id)
        for source in node_obj.browse(cr, uid, nodes_trf, context):
            # afegim els altres trafos del ct com a stop_nodes
            Gbt = self.subgraf(G, source, [n for n in nodes_trf if n != source.id])
            # Simulem i ens retorna un graf amb els nodes amb corrent (on)
            Gon = self.dfs_continuitat(Gbt, source, sim_open_nodes)
            # Afegit totes les coordenades de les arestes al resultat (trams_on)
            for edge_id in [e[2]["edge_id"] for e in Gon.edges(data=True)]:
                trams_on.append(int(edge_id))

            # Fem una copia del graph on per poder-hi treballar
            Gaux = Gon.copy()
            # Borrem els nodes que siguin frontera
            # del graf amb corrent (nodes oberts)

            Gaux.remove_nodes_from(node_obj.browse(cr, uid, self.open_nodes, context))
            Gaux.remove_nodes_from(node_obj.browse(cr, uid, sim_open_nodes, context))

            # Creem una copia de tot el subgraf bt
            Goff = Gbt.copy()
            # Hi esborrem tots els nodes del graf que hem creat
            # abans (nodes sense corrent) i ens quedara un graf amb els
            # nodes que no en tenen + els nodes oberts (frontera)

            Goff.remove_nodes_from(Gaux.nodes())

            # Afegim totes les coordenades de les arestes
            # al resultat (trams_off)
            for edge_id in [e[2]["edge_id"] for e in Goff.edges(data=True)]:
                trams_off.append(int(edge_id))
            nodes_on += Gon.nodes()
            nodes_off += Goff.nodes()

        # Guardar els nodes escomesa del subgraf amb info
        # d'estat (ON/OFF) a giscegis_simulacio_bt_resultats_escomeses

        vals = {
            'simulacio': bt_resultats_id
        }
        # agafem el pool per giscegis.blocs.escomeses

        tmpnodes = []
        for node in nodes_on:
            if node not in tmpnodes:
                tmpnodes.append(node)
            nmodel_obj = node.get_model()  # Retorna False per no-blocs
            if nmodel_obj:
                if nmodel_obj.model == 'giscegis.blocs.escomeses':
                    # és una escomesa
                    # agafar id del bloc per poder trobar el data_cups_escomeses
                    bloc = node.get_bloc()
                    # buscar id giscedata.cups.escomesa
                    escomesa_ids = gis_escomesa_obj.read(cr, uid, bloc.id, ['escomesa'], context)
                    if escomesa_ids['escomesa']:
                        vals['name'] = escomesa_ids['escomesa'][0]
                        vals['estat'] = 1
                        bt_escomeses_id = bt_escomeses_obj.create(cr, uid, vals, context)
                    else:
                        # escomesa no trobada :(
                        pass
                elif nmodel_obj.model == "giscedata.cups.escomesa":
                    search_params = [("node_id", "=", node.id)]
                    esc_id = esc_obj.search(cr, uid, search_params, context)
                    vals["name"] = esc_id[0]
                    vals["estat"] = 1
                    bt_escomeses_obj.create(cr, uid, vals, context)
                else:
                    # NO és escomesa, no fer res de moment
                    pass

        # OFF
        escomeses_off = []

        for node in nodes_off:
            nmodel_obj = node.get_model()  # Retorna False per no-blocs
            if nmodel_obj:
                if nmodel_obj.model == 'giscegis.blocs.escomeses':
                    # és una escomesa
                    # agafar id del bloc per poder trobar el data_cups_escomeses
                    bloc = node.get_bloc()
                    # buscar id giscedata.cups.escomesa
                    escomesa = gis_escomesa_obj.read(cr, uid, bloc.id, ['escomesa'], context)
                    # escomesa['escomesa'] == giscedata_cups_escomesa.id
                    if escomesa['escomesa']:
                        vals['name'] = escomesa['escomesa'][0]
                        vals['estat'] = -1
                        bt_escomeses_obj.create(cr, uid, vals, context)
                        escomeses_off.append(escomesa['escomesa'][0])
                    else:
                        # escomesa no trobada :(
                        pass
                elif nmodel_obj.model == "giscedata.cups.escomesa":
                    search_params = [("node_id", "=", node.id)]
                    esc_id = esc_obj.search(cr, uid, search_params, context)
                    vals["name"] = esc_id[0]
                    vals["estat"] = -1
                    bt_escomeses_obj.create(cr, uid, vals, context)

                else:
                    # NO és escomesa, no fer res de moment
                    pass

        # FIXME quan estigui provat, posar les agregacions dins els bucles que
        #  ja tenim adalt i ens estalviarem una passada per totes les escomeses
        #  busquem les pòlisses de les escomeses que tenim OFF

        num_polisses_off = 0
        potencia_contractada_off = 0
        for gescomesa in escomeses_off:
            escomesa = esc_obj.browse(cr, uid, gescomesa)
            num_polisses_off += len(escomesa.polisses_actives)
            potencia_contractada_off += escomesa.potencia_polisses

        return {
            'trams_on': trams_on,
            'trams_off': trams_off,
            'escomeses_off': escomeses_off,
            'simulacio': bt_resultats_id,
            'num_polisses_off': num_polisses_off,
            'potencia_contractada_off': float("%.2f" %potencia_contractada_off)
        }

    def simulacio(self, cursor, uid, ct, sim_open_nodes=[], context={},
                  load=False):
        # simulació per gis 1.x
        resultat = self.simulacio_ids(cursor, uid, ct, sim_open_nodes,
                                      context, load)
        res = self.get_simulation_geom(cursor, uid, resultat)

        return res

    def simulacio_geom(self, cursor, uid, ct, sim_open_nodes=[], context={},
                       load=False, srid=False):
        """
        Makes a simulation and returns the geometries

        :param cursor: Database cursor
        :param uid: User id
        :param ct: CT to simulate
        :param sim_open_nodes: Simulation open nodes
        :param context: OpenERP context
        :param load:
        :param srid: SRID of the returint GeoJSON
        :return:
        """

        # simulació per gis 2015
        # return GeoJson in system srid or srid specified by param
        resultat = self.simulacio_ids(cursor, uid, ct, sim_open_nodes,
                                      context, load)
        res = self.get_simulation_geom(cursor, uid, resultat, method='postgis',
                                       srid=srid)

        return res

    def simulacio_descarrec_ids(self, cr, uid, ct, sol_descarrec_id, sim_open_nodes=[], context={}, load=False):
        sol_descarrec_id = int(sol_descarrec_id)
        # marcar descarrec com a 'simulant'
        sol_descarrec = self.pool.get('giscedata.descarrecs.sollicitud').browse(cr, uid, sol_descarrec_id)
        sol_descarrec.simulant()
        # simulem normal
        sim_open_nodes = map(int, sim_open_nodes)
        resultats = self.simulacio_ids(cr, uid, ct, sim_open_nodes, context, load)
        # agafem el id de la simulació
        sim_id = resultats['simulacio']
        # marquem la sol.licitud com a 'done'
        sol_descarrec.done()
        # crear descàrrec, estat 'simulat'
        descarrec_obj = self.pool.get('giscedata.descarrecs.descarrec')
        vals = {}

        # valors que venen de la sol.licitud
        vals = {
            'name': sol_descarrec.name,
            'sollicitud': sol_descarrec_id,
            'descripcio': sol_descarrec.descripcio,
            'data_inici': sol_descarrec.data_inici,
            'data_final': sol_descarrec.data_final,
            'observacions': sol_descarrec.observacions,
            'xarxa': sol_descarrec.xarxa
        }

        # guardar i obtenir id per poder crear els resultats
        descarrec_id = descarrec_obj.create(cr, uid, vals)
        # assignar resultats del descàrrec
        clients_obj = self.pool.get('giscedata.descarrecs.descarrec.clients')

        # conta-at: (FILTRAR, que estan junts amb la resta de CONTA's!!)
        sim_contas_obj = self.pool.get('giscegis.simulacio.bt.resultats.escomeses')
        escomesa_obj = self.pool.get('giscedata.cups.escomesa')
        cups_obj = self.pool.get('giscedata.cups.ps')
        conta_obj = self.pool.get('giscedata.lectures.comptador')
        # per la info dels CONTA
        trace_obj = self.pool.get('giscegis.escomeses.traceability')
        sim_contas_ids = sim_contas_obj.search(cr, uid, [('simulacio', '=', sim_id), ('estat', '=', -1)])
        for cid in sim_contas_ids:
            vals_i = {}
            vals_c = {}
            conta = escomesa_obj.browse(cr, uid, sim_contas_obj.browse(cr, uid, cid).name.id)
            trace_id = trace_obj.search(cr, uid, [('escomesa', '=', conta.id)])
            if len(trace_id) > 0:
                trace = trace_obj.browse(cr, uid, trace_id[0])

                # ens assegurem que no hi hagi cap block
                # CONTA-AT a la xarxa de BT
                if conta.blockname.code != 1:
                    # per cada cups de l'escomesa amb polissa...
                    cups_ids = cups_obj.search(cr, uid, [('id_escomesa', '=', conta.id)])
                    for cups in cups_obj.browse(cr, uid, cups_ids):
                        polissa = cups._get_polissa()
                        if polissa:
                            vals_c['descarrec_id'] = descarrec_id
                            vals_c['name'] = polissa.titular.name
                            vals_c['code_ct'] = trace.ct.name
                            vals_c['zone_ct_id'] = trace.ct.zona_id.id or 0
                            vals_c['codeine'] = cups.id_municipi.id
                            vals_c['poblacio'] = cups.id_poblacio.id
                            vals_c['codecustomer'] = polissa.name
                            vals_c['line'] = cups.linia
                            vals_c['cups'] = cups.name
                            vals_c['policy'] = polissa.id
                            vals_c['contracted_power'] = polissa.potencia
                            vals_c['tension'] = polissa.tensio
                            vals_c['pricelist'] = polissa.tarifa.name
                            vals_c['escomesa'] = conta.name
                            vals_c['escomesa_type'] = conta.blockname.name
                            vals_c['street'] = cups.nv
                            if cups.pnp:
                                vals_c['number'] = cups.pnp
                            else:
                                vals_c['number'] = cups.aclarador
                            vals_c['level'] = cups.pt
                            vals_c['appartment_number'] = cups.pu
                            comptadors = polissa.comptadors_actius(polissa.data_alta)
                            if comptadors:
                                compt_obj = self.pool.get('giscedata.lectures.comptador')
                                print(comptadors)
                                print(conta_obj.read(cr, uid, [comptadors[0]], ['name']))
                                vals_c['contador'] = conta_obj.read(
                                    cr, uid, [comptadors[0]],
                                    ['name'])[0]['name']
                            else:
                                vals_c['contador'] = '0000000'
                            vals_c['data_alta'] = polissa.data_alta
                            clients_obj.create(cr, uid, vals_c)
        descarrec_obj.observacions_descarrec(cr, uid, [descarrec_id])
        return resultats

    def simulacio_descarrec(self, cursor, uid, ct, sol_descarrec_id,
                            sim_open_nodes=[], context={}, load=False):
        # simulació per gis 1.x
        resultat = self.simulacio_descarrec_ids(
            cursor, uid, ct, sol_descarrec_id, sim_open_nodes, context, load
        )
        res = self.get_simulation_geom(cursor, uid, resultat)

        return res

    def simulacio_descarrec_geom(self, cursor, uid, ct, sol_descarrec_id,
                                 sim_open_nodes=[], context={}, load=False,
                                 srid=False):
        resultat = self.simulacio_descarrec_ids(
            cursor, uid, ct, sol_descarrec_id, sim_open_nodes, context, load
        )
        res = self.get_simulation_geom(cursor, uid, resultat, method='postgis',
                                       srid=srid)
        return res

    def simulacio_qds(self, cr, uid, netsource, sim_open_nodes=[], context={},
                      load=False):
        return self.simulacio(cr, uid, netsource, sim_open_nodes, context, load)

    def get_open_switches(self, cr, uid, srid=None, context=None):
        """
        Funció per retornar tots els nodes oberts de BT
        :param cr:
        :param uid:
        :param srid: SRID al qual es vol transformar
        :type: int
        :param context:
        :return: Llista de dict amb tots els nodes de BY que estan oberts
        :rtype: list[dict]
        """

        congig_srid = config.get('srid')
        if isinstance(congig_srid, str):
            congig_srid = int(congig_srid)

        if not srid:
            srid = config.get('srid')

        if context is None:
            context = {}

        sql = """
            SELECT 
                config_bt.id as config_id, 
                config_bt."name" as name, 
                config_bt.codi as codi, 
                ST_X(
                    ST_TRANSFORM(
                        ST_SETSRID(nodes.geom, %(congig_srid)s),%(srid)s
                        )
                    ) as x, 
                ST_Y(
                    ST_TRANSFORM(
                        ST_SETSRID(nodes.geom, %(congig_srid)s),%(srid)s
                        )
                    ) as y
            FROM giscegis_configdefault_bt config_bt
            JOIN giscegis_nodes nodes ON config_bt.node = nodes.id
        """
        cr.execute(sql, dict(srid=srid, congig_srid=congig_srid))

        list_nodes = []
        for res in cr.fetchall():
            config_id = res[0]
            name = res[1]
            codi = res[2]
            x = res[3]
            y = res[4]
            list_nodes.append({
                        'id': config_id,
                        'codi': codi,
                        "name": name,
                        'x': x,
                        'y': y
                    })
        return list_nodes

    def get_nodes(self, cr, uid, ct, state='all', srid=None, context=None):
        """
        Funció per retornar tots els nodes d'un subgraf de bt
        :param ct: Id del CT que volem obtenir els nodes
        :type ct: integer
        :param state: {all, open, closed} Estat en que volem els interruptors
        :type state: str
        """

        if not srid:
            srid = config.get('srid')
            if isinstance(srid, six.string_types):
                srid = int(srid)
            else:
                srid = 23031
        if context is None:
            context = {}

        self.init(cr)

        topo_obj = self.pool.get('giscegis.topologia')
        node_obj = self.pool.get('giscegis.nodes')
        trafo_bloc_obj = self.pool.get('giscegis.blocs.transformadors')
        trafo_obj = self.pool.get("giscedata.transformador.trafo")

        G = topo_obj.load(cr, uid, context)

        # Busquem els tots trafos (blocs) que pertanyen al CT que ens
        # ha enviat per simular
        trafo_bloc_ids = trafo_bloc_obj.search(cr, uid, [('ct', '=', ct)])
        trafo_ids = trafo_obj.search(cr, uid, [('ct', '=', ct)])

        list_nodes = []
        trafos_bloc_ct = trafo_bloc_obj.read(cr, uid, trafo_bloc_ids, ['node'], context)
        trafos_qgis_ct = trafo_obj.read(cr, uid, trafo_ids, ['node_id'], context)

        trafos_ct = trafos_bloc_ct + trafos_qgis_ct

        # Els nodes del trafo del ct
        nodes_trf = [t.get('node', t.get("node_id"))[0] for t in trafos_ct]
        # Per a cada transformador del CT
        model_nodes = self.pool.get('giscegis.nodes')
        for trafo_bloc in trafos_ct:
            source = node_obj.browse(cr, uid, trafo_bloc.get('node', trafo_bloc.get("node_id"))[0], context)
            # afegim els altres trafos del ct com a stop_nodes
            Gbt = self.subgraf(G, source, [n for n in nodes_trf if n != source.id])
            # Si ens demana els nodes tancats
            if state == 'closed':
                # Eliminem del graf tots els nodes que estiguin oberts
                Gbt.remove_nodes_from(node_obj.browse(cr, uid, self.open_nodes, context))
            # Si ens demana els oberts
            elif state == 'open':
                # Creem un array amb els ids dels nodes del graf
                nodes = [getattr(node, 'id') for node in Gbt.nodes()]
                # Eliminem d'aquest array tots els que estiguin oberts per
                # tal que l'array tingui només els tancats
                for n_open in self.open_nodes:
                    try:
                        nodes.remove(n_open)
                    except ValueError:
                        continue
                # Eliminem del graf tots els tancats (tots - tancats = oberts)
                Gbt.remove_nodes_from(node_obj.browse(cr, uid, nodes, context))

            # Retornem un diccionari {'nodeid':n, 'codi':c, 'x':x, 'y':y}
            appended_nodes = []
            for node in Gbt.nodes():
                if node.id in appended_nodes:
                    continue
                if is_postgis_db(cr):
                    n = model_nodes.get_coords(cr, uid, node.id, srid=srid)
                else:
                    n = {'x': 0, 'y': 0}

                list_nodes.append({
                    'node': node.id,
                    'codi': node_obj.get_codi(cr, uid, node.id) or node.name,
                    'x': n['x'],
                    'y': n['y']
                })
                appended_nodes.append(node.id)
        return list_nodes

    def get_abonats_off(self, cr, uid, simulation_results_id):
        """
        Aquest mètode cerca els abonats que s'han quedat sense servei
        en una simulació donada.

        :param simulation_results_id: id dels resultats de simulació bt
        """
        ggsbre_obj = self.pool.get('giscegis.simulacio.bt.resultats.escomeses')
        ggbe_obj = self.pool.get('giscegis.blocs.escomeses')
        search_params_ggsbre = [
            ('simulacio', '=', simulation_results_id),
            ('estat', '=', -1)
        ]
        ggsbre_ids = ggsbre_obj.search(cr, uid, search_params_ggsbre)
        escomeses_off = ggsbre_obj.browse(cr, uid, ggsbre_ids)

        ESTATS_POLISSA = ['esborrany', 'pendent', 'tall', 'baixa']
        cups_obj = self.pool.get('giscedata.cups.ps')
        polissa_obj = self.pool.get('giscedata.polissa')
        gpolissa_obj = self.pool.get('giscegis.polisses.171')

        r = {}
        res = []
        escomeses_ids = [a.name.id for a in escomeses_off]
        search_params_cups = [('id_escomesa', 'in', escomeses_ids)]
        cups_ids = cups_obj.search(cr, uid, search_params_cups)
        for cups in cups_obj.browse(cr, uid, cups_ids):
            # obtenir pòlissa activa associada, si en té
            search_params = [('cups', '=', cups.id),
                             ('state', 'not in', ESTATS_POLISSA)]
            polissa_id = polissa_obj.search(cr, uid, search_params)
            if polissa_id:
                polissa = polissa_obj.browse(cr, uid, polissa_id)[0]
                pol = {}
                pol['carrer'] = cups.nv
                pol['escala'] = cups.es
                comptadors = polissa.comptadors_actius(polissa.data_alta)
                if comptadors:
                    compt_obj = self.pool.get('giscedata.lectures.comptador')
                    pol['n_comptador'] = compt_obj.read(cr, uid,
                                                        [comptadors[0]],
                                                        ['name'])[0]['name']
                else:
                    pol['n_comptador'] = '0000000'
                pol['escomesa'] = cups.id_escomesa.name
                pol['tensio'] = polissa.tensio
                pol['titular'] = polissa.titular.name
                pol['et'] = polissa.et
                pol['linia'] = polissa.linia
                pol['polissa'] = polissa.name
                pol['numero'] = cups.pnp
                pol['potencia'] = polissa.potencia
                pol['planta'] = cups.pt
                pol['pis'] = cups.pu
                gpolissa_id = gpolissa_obj.search(cr, uid,
                                                  [('name', '=', polissa.name)])
                if gpolissa_id:
                    gpolissa = gpolissa_obj.browse(cr, uid, gpolissa_id[0])

                    if gpolissa.x != 0 and gpolissa.y != 0:
                        pol['x'] = gpolissa.x
                        pol['y'] = gpolissa.y
                        pol['posicionar'] = "<a href='#' onClick='javascript:zoom2()'>Zoom</a>"
                pol['detall'] = "<a href='#' onClick='javascript:detall2()'>Fitxa</a>"
                res.append(pol)
        return res


GiscegisSimulacioBt()


class GiscegisSimulacioBtResulats(osv.osv):

    """
    Classe per guardar els resultats d'una simulacio
    """

    _name = 'giscegis.simulacio.bt.resultats'
    _columns = {
        'name': fields.char('Name', size=128),
        'escomeses': fields.one2many(
            'giscegis.simulacio.bt.resultats.escomeses',
            'simulacio',
            'Escomeses'
        ),
        'simulacio': fields.many2one(
            'giscegis.configsimulacio.bt',
            'Config Simulació'
        ),
    }


GiscegisSimulacioBtResulats()


class GiscegisSimulacioBtResultatsEscomeses(osv.osv):

    """
    Classe per guardar escomeses sense corrent relacionades
    amb una simulacio
    """

    _name = 'giscegis.simulacio.bt.resultats.escomeses'

    _columns = {
        'name': fields.many2one('giscedata.cups.escomesa', 'Escomesa'),
        'simulacio': fields.many2one(
            'giscegis.simulacio.bt.resultats',
            'Simulacio',
            ondelete='cascade'
        ),
        'estat': fields.selection([(-1, 'Off'), (1, 'On')], 'Estat')
    }

    _sql_constraints = [
        ('name_simulacio_uniq',
         'unique (name, simulacio, estat)',
         'No es pot repetir l\'escomesa més d\'un cop a la mateixa simulació.')
    ]


GiscegisSimulacioBtResultatsEscomeses()
