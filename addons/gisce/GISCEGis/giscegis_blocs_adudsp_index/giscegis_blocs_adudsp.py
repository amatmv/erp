from base_index.base_index import BaseIndex


class GiscegisBlocsAdudsp(BaseIndex):
    _name = 'giscegis.blocs.adudsp'
    _inherit = 'giscegis.blocs.adudsp'

    def __init__(self, pool, cursor):
        super(GiscegisBlocsAdudsp, self).__init__(pool, cursor)

    _index_title = '{obj.name}'
    _index_summary = '{obj.blockname.name} {obj.codi} {obj.observacions}'

    _index_fields = {
        'codi': True,
        'blockname.name': True,
        'observacions': True
    }

    def get_lat_lon(self, cursor, uid, item, context=None):
        if context is None:
            context = {}
        cursor.execute("""
        SELECT
            ST_Y(ST_GeometryN(ST_Transform(gv.geom,4326),1)
                 ) AS lat,
            ST_X(ST_GeometryN(ST_Transform(gv.geom,4326),1)
                 ) AS lon
        FROM giscegis_adudsp_geom gv
        WHERE
            gv.id = %s
        """, (item.id, ))
        res = cursor.fetchone()
        if not res:
            res = (0, 0)
        return res

GiscegisBlocsAdudsp()
