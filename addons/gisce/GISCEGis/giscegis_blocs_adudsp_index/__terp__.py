# -*- coding: utf-8 -*-
{
    "name": "Index ADUDSP",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Index als ADU-DSP's
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "base_index",
        "giscegis_blocs_adudsp",
        "giscegis_search_type"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscegis_search_type_data.xml"
    ],
    "active": False,
    "installable": True
}
