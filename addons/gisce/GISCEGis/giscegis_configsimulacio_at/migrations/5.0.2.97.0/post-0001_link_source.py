# coding=utf-8
import logging

logger = logging.getLogger('openerp.' + __name__)


def up(cursor, installed_version):
    """
    Migration script that updates the data of source with the
    data of source_bloc_id

    :param cursor:
    :param installed_version:

    :return: None
    :rtype: None
    """

    if not installed_version:
        return

    sql = """UPDATE giscegis_configsimulacio_at set source = upd.source FROM (
        SELECT
            config.id AS id,
            punt.id AS source
        FROM giscegis_configsimulacio_at AS config
        LEFT JOIN giscegis_blocs_netsource AS b ON config.source_bloc_id = b.id
        LEFT JOIN giscedata_punt_frontera AS punt ON  punt.codi= b.codi
        ) AS upd
    WHERE giscegis_configsimulacio_at.id = upd.id
    """

    cursor.execute(sql)


def down(cursor, installed_version):
    pass

migrate = up
