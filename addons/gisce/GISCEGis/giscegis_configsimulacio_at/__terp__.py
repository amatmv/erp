# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Config Simulacio AT",
    "description": """Permet enmagatzemar les configuracions de la xarxa AT""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_blocs_netsource",
        "giscegis_base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscegis_configsimulacio_at_view.xml",
        "ir.model.access.csv",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
