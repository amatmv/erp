from osv import fields, osv


class GiscegisConfigsimulacioAt(osv.osv):
    _name = "giscegis.configsimulacio.at"
    _description = "Configuracio Simulacio AT"
    _columns = {
        'name': fields.char('Nom', size=50, required=True),
        'source': fields.many2one('giscedata.punt.frontera', 'Font'),
        'autosaved': fields.boolean('Autosaved'),
    }

    _defaults = {
        'autosaved': lambda *a: 1,
    }
    _order = "name, id"


GiscegisConfigsimulacioAt()


class GiscegisConfigsimulacioAtOberts(osv.osv):
    _name = "giscegis.configsimulacio.at.oberts"
    _description = "Configuracio Oberts AT"
    _columns = {
      'name': fields.char('Nom', size=50, required=True),
      'config': fields.many2one('giscegis.configsimulacio.at','Configuracio'),
      'node': fields.many2one('giscegis.nodes', 'Node', ondelete='set null'),
    }

    _defaults = {

    }
    _order = "name, id"


GiscegisConfigsimulacioAtOberts()

