from giscegis_base_index.giscegis_base_index import GiscegisBaseIndex


class GiscedataBtElement(GiscegisBaseIndex):
    _name = 'giscedata.bt.element'
    _inherit = 'giscedata.bt.element'
    _index_fields = {
        "obres": lambda self, data: ' '.join(['obra{0}'.format(d.name) or '' for d in data])
    }

GiscedataBtElement()
