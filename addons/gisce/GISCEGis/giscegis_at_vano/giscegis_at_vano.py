# -*- coding: utf-8 -*-
from osv import osv, fields
from giscegis_base_geom.fields import Line
from tools import config


class GiscegisAtVano(osv.osv):

    _inherit = 'giscedata.at.vano'
    _description = 'Vanos AT'

    def _longitud(self, cursor, uid, ids, field_name, ar, context):
        """
        Fields function to calculate the longitud of the Vano

        :param cursor: Database cursor
        :param uid: User id
        :param ids: AT tram ids
        :param field_name: Field name
        :param ar: Arguments
        :param context: OpenERP context
        :return:
        :rtype: dict
        """

        ret = dict.fromkeys(ids)

        sql = """
            SELECT id, st_length(geom)
            FROM giscedata_at_vano
            WHERE id in %(ids)s
        """

        cursor.execute(sql, {"ids": tuple(ids)})

        query_res = cursor.fetchall()

        for row in query_res:
            ret[row[0]] = row[1]

        return ret

    def _longituds_to_update(self, cursor, uid, ids, context=None):
        """
        Returns the ids of the Vanos which the field longitud must be updated

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: List of ids to calc
        :type ids: list(int)
        :param context: OpenERP context
        :return: List of elements to store
        :rtype: list(int)
        """

        return ids

    _columns = {
        "geom": Line("Geometria", srid=config.get('srid', 25830), select=True),
        "longitud": fields.function(
            _longitud, type="float", string="Longitud",
            readonly=True, method=True,
            store={
                'giscedata.at.vano': (_longituds_to_update, ['geom'], 10)
            }
        ),
    }


GiscegisAtVano()
