# -*- coding: utf-8 -*-
{
    "name": "GISCEGis AT Vano",
    "description": """Modul que implementa els camps GIS per Vanos d'AT""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Alta Tensió",
    "depends":[
        "giscedata_at_vano",
        "giscegis_base",
        "giscegis_base_geom"
    ],
    "init_xml":[],
    "demo_xml":[],
    "update_xml":[],
    "active": False,
    "installable": True
}
