# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS QGISCE-WEBGIS",
    "description": """Módulo virtual para convivencia de WebGis y Qgis""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "giscegis_qgisce_full",
        "giscegis_adudsp_geom",
        "giscegis_blocs_detectors_geom",
        "giscegis_blocs_suports_at_geom",
        "giscegis_blocs_suports_bt_geom",
        "giscegis_blocs_tensio_geom",
        "giscegis_caixabt_geom",
        "giscegis_cts_geom",
        "giscegis_cups_escomeses_tensio_geom",
        "giscegis_empalmes_at_geom",
        "giscegis_font_geom",
        "giscegis_fusibles_caixes_geom",
        "giscegis_interruptorsat_geom",
        "giscegis_interruptorsbt_geom",
        "giscegis_trafos_geom",
        "giscegis_nodes_geom",
        "giscegis_lat_geom",
        "giscegis_lbt_geom",
        "giscegis_er_geom"
    ],
    "init_xml": [],
    "demo_xml":[],
    "update_xml":[
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
