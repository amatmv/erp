# *-* coding: utf-8 *-*

from osv import osv
from tools import config


class GiscegisAdudspGeomQgis(osv.osv):
    """
    Model of giscegis.adudsp.geom for Webgis-Qgis
    """
    _inherit = 'giscegis.adudsp.geom'

    def init(self, cursor):
        """
        Overwrites the init method and creates a materialized view that can
        live along with Qgis without data incoherences
        :param cursor: Database cursor
        :return: True
        """

        cursor.execute("""
            DROP MATERIALIZED VIEW IF EXISTS giscegis_adudsp_geom;
        """)

        cursor.execute("""
        CREATE MATERIALIZED VIEW public.giscegis_adudsp_geom AS
        SELECT 
            caixa.id AS id,
            caixa_tipus.name::text AS symbol,
            360 - caixa.rotation AS rotation,
            caixa.geom AS geom,
            caixa.name AS codi,
            now() AS materialize_date
        FROM giscedata_bt_caixa AS caixa
        LEFT JOIN giscedata_bt_caixa_tipus AS caixa_tipus 
             ON caixa.tipus = caixa_tipus.id
        WHERE caixa.active;
        """)

        cursor.execute("""
            CREATE INDEX giscegis_adudsp_geom_geom
            ON giscegis_adudsp_geom USING gist (geom);
        """)

        return True


GiscegisAdudspGeomQgis()


class GiscegisBlocsDetectorGeomQgis(osv.osv):
    """
    Model of Blocs detector for Giscegis Webgis-Qgis
    """
    _inherit = 'giscegis.blocs.detector.geom'

    def init(self, cursor):
        """
        Overwrites the init method and creates a materialized view that can
        live along with Qgis without data incoherences
        :param cursor: Database cursor
        :return: None
        :rtype: None
        """

        cursor.execute("""
        DROP MATERIALIZED VIEW IF EXISTS giscegis_blocs_detector_geom;
        """)
        cursor.execute("""
        CREATE MATERIALIZED VIEW public.giscegis_blocs_detector_geom AS
        SELECT
            det.id,
            det_tipus.name AS symbol,
            det.geom AS geom,
            det.rotation as rotation,
            det.id AS detector,
            st_x(det.geom) AS x,
            st_y(det.geom) AS y,
            now() as materialize_date
        FROM giscedata_at_detectors AS det
        LEFT JOIN giscegis_blocs_detectors_blockname AS det_tipus 
            ON det_tipus.id=det.blockname
        WHERE det.active;
        """)

        return True


GiscegisBlocsDetectorGeomQgis()


class GiscegisBlocsSuportsAtGeomQgis(osv.osv):
    """
    Model of Blocs Suports AT for Giscegis Webgis-Qgis
    """
    _inherit = 'giscegis.blocs.suports.at.geom'

    def init(self, cursor):
        """
        Overwrites the init method and creates a materialized view that can
        live along with Qgis without data incoherences
        :param cursor: Database cursor
        :return: None
        :rtype: None
        """

        cursor.execute("""
            DROP MATERIALIZED VIEW IF EXISTS giscegis_blocs_suports_at_geom;
        """)
        cursor.execute("""
            CREATE MATERIALIZED VIEW public.giscegis_blocs_suports_at_geom AS
                SELECT
                    suport.id AS id,
                    suport.name || ' ' || poste.name AS texte,
                    suport.geom AS geom,
                    substring(
                        suport.name::text, position('-' in suport.name::text) + 1
                    ) AS numero,
                    suport.webgis_tipus AS blockname, 
                    360 - suport.rotation AS rotation,
                    now() AS materialize_date
                FROM giscedata_at_suport suport
                LEFT JOIN giscedata_at_poste poste ON suport.poste = poste.id
                WHERE suport.active;
        """)

        return True


GiscegisBlocsSuportsAtGeomQgis()


class GiscegisBlocsSuportsBtGeomQgis(osv.osv):
    """
    Model of Blocs Suports BT for Giscegis Webgis-Qgis
    """
    _inherit = 'giscegis.blocs.suports.bt.geom'

    def init(self, cursor):
        """
        Overwrites the init method and creates a materialized view that can
        live along with Qgis without data incoherences
        :param cursor: Database cursor
        :return: True
        :rtype: bool
        """

        cursor.execute("""
            DROP MATERIALIZED VIEW IF EXISTS giscegis_blocs_suports_bt_geom;
        """)

        cursor.execute("""
            CREATE MATERIALIZED VIEW giscegis_blocs_suports_bt_geom AS 
                SELECT
                    suport.id::int AS id,
                    suport.geom AS geom,
                    tipus.code || '-' || suport.name AS texte,
                    tipus.code AS blockname,
                    360 - suport.rotation AS rotation,
                    tipus.code AS symbol,
                    now() AS materialize_date
                FROM giscedata_bt_suport AS suport
                LEFT JOIN giscedata_bt_suport_tipus AS tipus
                    ON suport.tipus = tipus.id
                WHERE suport.active;
        """)

        return True


GiscegisBlocsSuportsBtGeomQgis()


class GiscegisBlocsTensioGeomQgis(osv.osv):
    """
    Model of Blocs Tensio for Giscegis Webgis-Qgis
    """
    _inherit = 'giscegis.blocs.tensio.geom'

    def init(self, cursor):
        """
        Overwrites the init method and creates a materialized view that can
        live along with Qgis without data incoherences
        :param cursor: Database cursor
        :return: True
        :rtype: bool
        """

        cursor.execute("""
            DROP MATERIALIZED VIEW IF EXISTS giscegis_blocs_tensio_geom;
        """)

        cursor.execute("""
            CREATE MATERIALIZED VIEW public.giscegis_blocs_tensio_geom AS
                SELECT 
                    tensio.id AS id,
                    tensio.geom AS geom,
                    'TENSION' || ' ' || tensio.tensio AS texte, 
                    'TENSION'|| tensio.tensio::text AS blockname, 
                    360 - tensio.rotation AS rotation,
                    now() AS materialize_date
                FROM giscegis_tensio_bloc tensio
                WHERE active;
        """)

        cursor.execute("""
            DROP INDEX IF EXISTS giscegis_blocs_tensio_geom_geom;
            CREATE INDEX giscegis_blocs_tensio_geom_geom
                ON giscegis_blocs_tensio_geom USING gist (geom);
        """)

        return True


GiscegisBlocsTensioGeomQgis()


class GiscegisCaixabtGeomQgis(osv.osv):
    """
    Model of Caixa BT for Giscegis Webgis-Qgis
    """
    _inherit = 'giscegis.caixabt.geom'

    def init(self, cursor):
        """
        Overwrites the init method and creates a materialized view that can
        live along with Qgis without data incoherences
        :param cursor: Database cursor
        :return: True
        :rtype: bool
        """

        cursor.execute("""
            DROP MATERIALIZED VIEW IF EXISTS giscegis_caixabt_geom;
        """)
        cursor.execute("""
            CREATE MATERIALIZED VIEW public.giscegis_caixabt_geom AS
                SELECT 
                    caixa.id AS id,
                    360 - caixa.rotation AS rotation,
                    caixa_tipus.name::text AS symbol,
                    caixa.intensitat AS intensitat_fusible,
                    caixa.intensitat_maxima AS intensitat_caixa,
                    caixa.geom AS geom,
                    now() AS materialize_date
                FROM giscedata_bt_caixa AS caixa
                LEFT JOIN giscedata_bt_caixa_tipus AS caixa_tipus 
                     ON caixa.tipus = caixa_tipus.id
                WHERE caixa.active;
        """)
        cursor.execute("""
            CREATE INDEX giscegis_caixabt_geom_geom
            ON giscegis_caixabt_geom USING gist (geom);
        """)

        return True


GiscegisCaixabtGeomQgis()


class GiscegisCtsGeomQgis(osv.osv):
    """
    Model of CTS for Giscegis Webgis-Qgis
    """
    _inherit = "giscegis.cts.geom"

    def init(self, cursor):
        """
        Overwrites the init method and creates a materialized view that can
        live along with Qgis without data incoherences
        :param cursor: Database cursor
        :return: True
        :rtype: bool
        """

        cursor.execute("""
        DROP MATERIALIZED VIEW IF EXISTS public.giscegis_cts_geom;
        """)
        cursor.execute("""
        CREATE MATERIALIZED VIEW public.giscegis_cts_geom AS
        SELECT
            ct.id,
            ct.id_subtipus,
            ct.name,
            360 - ct.rotation AS rotation,
            ct.name::text 
                || E'\n'::text 
                || ct.descripcio::text 
                || E'\n'::text 
                || COALESCE(
                    (SELECT 
                        COALESCE(e.industria,'S/E'::character varying)::text
                        || ' ; '::text 
                        || COALESCE( e.industria_data::text, 'S/D'::text)
                    FROM giscedata_expedients_expedient e, 
                        giscedata_cts_expedients_rel r
                    WHERE r.ct_id = ct.id AND r.expedient_id = e.id
                    ORDER BY e.industria_data IS NOT NULL, 
                        e.industria_data DESC LIMIT 1), 
                    'S/E ; S/D'::text) 
                || E'\n'::text 
                || COALESCE(
                    sum(tr.potencia_nominal), 0::bigint) 
                || ' kVA'::text 
                || E'\n'::text AS texte_complete,
            ct.name::text 
                || E'\n'::text 
                || ct.descripcio  AS texte_simple,
            ct.geom AS geom,
            ct.name::text 
                || E'\n'::text 
                || ct.descripcio  AS texte,
            now() as materialize_date
        FROM giscedata_cts ct
        LEFT JOIN (
            SELECT trr.id, trr.potencia_nominal, trr.ct
            FROM giscedata_transformador_trafo trr
            LEFT JOIN giscedata_transformador_estat tres 
                ON trr.id_estat = tres.id AND tres.codi = 1
            ) tr ON tr.ct = ct.id
        WHERE ct.active
        GROUP BY ct.name, ct.id, ct.descripcio, ct.rotation;
        """)
        cursor.execute("""
        DROP INDEX IF EXISTS giscegis_cts_geom_geom;
        CREATE INDEX giscegis_cts_geom_geom ON giscegis_cts_geom 
            USING gist (geom);
        """)

        return True


GiscegisCtsGeomQgis()


class GiscegisCupsEscomesesTensioGeomQgis(osv.osv):
    """
    Model of Escomeses for Giscegis Webgis-Qgis
    """
    _inherit = "giscegis.cups.escomeses.tensio.geom"

    def init(self, cursor):
        """
        Overwrites the init method and creates a materialized view that can
        live along with Qgis without data incoherences
        :param cursor: Database cursor
        :return: True
        :rtype: bool
        """

        cursor.execute("""
            DROP MATERIALIZED VIEW IF EXISTS 
            public.giscegis_cups_escomeses_tensio_geom;
        """)
        cursor.execute("""
            CREATE MATERIALIZED VIEW  giscegis_cups_escomeses_tensio_geom AS
                SELECT
                    esco.id,
                    esco.name::integer AS name,
                    blockname.name || ';' 
                    || COALESCE(trace.tensio::text, ''::text) AS blockname,
                    blockname.name || ' ; ' || esco.name::text 
                    || E'\n' || COALESCE(ct.name, 'S/CT') || ' ; ' 
                    || COALESCE(trafo.ordre_dins_ct::text, 'S/T') || ' ; ' 
                    || COALESCE(trace.sortida::text, 'S/S') || E'\n' 
                    || 'CUPS: ' || COALESCE(count(cups.id_escomesa), 0) 
                    || E'\nPcont: ' || COALESCE(round(sum(pol.potencia), 2), 0) 
                    || E' kW\nPconv: ' 
                    || COALESCE(round(sum(cups.potencia_conveni), 2), 0) 
                    || ' kW' AS texte,
                    ct.name AS ct,
                    trafo.ordre_dins_ct AS ordre_tr,
                    trace.sortida::text AS sortida,
                    esco.geom AS geom,
                    esco.rotation AS rotation,
                    blockname.name AS symbol,
                    SUM(pol.potencia) AS sum, 
                    count(cups.id_escomesa) AS count,
                    CASE 
                        WHEN colors_bt.color IS NOT NULL then colors_bt.color
                        ELSE ''
                    END AS color,
                    now() AS materialize_date
                FROM giscedata_cups_escomesa esco
                LEFT JOIN giscegis_escomeses_traceability trace ON 
                    esco.id = trace.escomesa
                LEFT JOIN giscedata_cts ct ON trace.ct = ct.id
                LEFT JOIN giscedata_transformador_trafo trafo ON 
                    trace.trafo = trafo.id
                LEFT JOIN giscedata_cups_blockname blockname ON 
                    esco.blockname = blockname.id
                LEFT JOIN giscedata_cups_ps cups ON 
                    esco.id = cups.id_escomesa AND cups.active
                LEFT JOIN giscedata_polissa pol ON 
                    cups.id = pol.cups AND pol.active = true
                LEFT JOIN giscegis_colors_bt colors_bt ON 
                    colors_bt."name"::text = trace.sortida
                WHERE esco.active
                GROUP BY esco.id, esco.name, blockname.name, ct.name, 
                    trafo.ordre_dins_ct, esco.rotation, trace.tensio, 
                    trace.sortida, colors_bt.color
                ORDER BY esco.name;
        """)
        cursor.execute("""
            CREATE INDEX giscegis_cups_escomeses_tensio_geom_geom
                ON giscegis_cups_escomeses_tensio_geom USING gist (geom);
        """)

        return True


GiscegisCupsEscomesesTensioGeomQgis()


class GiscegisEmpalmesAtGeomQgis(osv.osv):
    """
    Model of Empalmes AT for Giscegis Webgis-Qgis
    """
    _inherit = "giscegis.empalmes.at.geom"

    def init(self, cursor):
        """
        Overwrites the init method and creates a materialized view that can
        live along with Qgis without data incoherences
        :param cursor: Database cursor
        :return: True
        :rtype: bool
        """

        cursor.execute("""
            DROP MATERIALIZED VIEW IF EXISTS giscegis_empalmes_at_geom;
        """)

        cursor.execute("""
            CREATE MATERIALIZED VIEW giscegis_empalmes_at_geom AS
                SELECT 
                    empalme.id AS id,
                    empalme_tipus.name AS symbol,
                    empalme.geom AS geom,
                    now() AS materialize_date
                FROM giscedata_at_empalmes_grup AS empalme
                LEFT JOIN giscegis_blocs_empalmes_grup_blockname empalme_tipus
                    ON empalme.tipus_empalme=empalme_tipus.id
                WHERE empalme.active;
        """)
        cursor.execute("""
            DROP INDEX IF EXISTS giscegis_blocs_empalmes_at_geom_indx;
            CREATE INDEX giscegis_blocs_empalmes_at_geom_indx
                ON giscegis_empalmes_at_geom USING gist (geom);
        """)

        return True


GiscegisEmpalmesAtGeomQgis()


class GiscegisFontGeomQgis(osv.osv):
    """
    Model of Font/PuntFrontera for Giscegis Webgis-Qgis
    """
    _inherit = "giscegis.font.geom"

    def init(self, cursor):
        """
        Overwrites the init method and creates a materialized view that can
        live along with Qgis without data incoherences
        :param cursor: Database cursor
        :return: True
        :rtype: bool
        """

        cursor.execute("""
            DROP MATERIALIZED VIEW IF EXISTS giscegis_font_geom;
        """)

        cursor.execute("""
            CREATE MATERIALIZED VIEW public.giscegis_font_geom AS
                SELECT 
                    font.id AS id,
                    font.name AS name,
                    font.rotation AS rotation,
                    font.geom AS geom,
                    now() AS materialize_date
                FROM giscedata_punt_frontera AS font
                WHERE font.active;
        """)

        cursor.execute("""
            DROP INDEX IF EXISTS giscegis_font_geom_geom;
            CREATE INDEX giscegis_font_geom_geom ON giscegis_font_geom
                USING gist (geom);
        """)

        return True


GiscegisFontGeomQgis()


class GiscegisFusiblesCaixesGeomQgis(osv.osv):
    """
    Model of Fusibles Caixes BT for Giscegis Webgis-Qgis
    """
    _inherit = 'giscegis.fusibles.caixes.geom'

    def init(self, cursor):
        """
        Overwrites the init method and creates a materialized view that can
        live along with Qgis without data incoherences
        :param cursor: Database cursor
        :return: True
        :rtype: bool
        """

        cursor.execute("""
            DROP MATERIALIZED VIEW IF EXISTS giscegis_fusibles_caixes_geom;
        """)

        cursor.execute("""
            CREATE MATERIALIZED VIEW public.giscegis_fusibles_caixes_geom AS
                SELECT 
                    fus.id,
                    tipus_fus.code AS symbol,
                    fus.rotation,
                    fus.geom AS geom,
                    now() AS materialize_date
                FROM giscedata_bt_caixa_element AS fus
                LEFT JOIN giscedata_bt_caixa_element_tipus AS tipus_fus 
                    ON fus.tipus = tipus_fus.id
                WHERE fus.active;
        """)
        cursor.execute("""
        CREATE INDEX giscegis_fusibles_caixes_geom_geom
            ON giscegis_fusibles_caixes_geom USING gist (geom)
        """)

        return True


GiscegisFusiblesCaixesGeomQgis()


class GiscegisInterruptorsatGeomQgis(osv.osv):
    """
    Model of Elem.Tall i Posicions for Giscegis Webgis-Qgis
    """
    _inherit = 'giscegis.interruptorsat.geom'

    def init(self, cursor):
        """
        Overwrites the init method and creates a materialized view that can
        live along with Qgis without data incoherences
        :param cursor: Database cursor
        :return: True
        :rtype: bool
        """

        cursor.execute("""
        DROP MATERIALIZED VIEW IF EXISTS giscegis_interruptorsat_geom;
        """)
        cursor.execute("""
        CREATE MATERIALIZED VIEW public.giscegis_interruptorsat_geom AS
        SELECT 
            ('1' || cel.id::varchar)::int AS id,
            CASE
                WHEN tipus_element.codi = 'SEC' THEN 'SECCIONADOR'
                ELSE tipus_element.codi
            END AS symbol,
            cel.name as codi,
            cel.geom AS geom,
            360 - cel.rotation AS rotation,
            cel.descripcio AS descripcio,
            cel.installacio AS installacio,
            cel.inventari AS inventari,
            cel.data_pm AS data_pm,
            cel.cini AS cini,
            ti.descripcio AS ti,
            tipus_element.name AS tipus_element,
            stock.name AS numero_serie,
            '[' || product.default_code || ']' || ' ' 
            || COALESCE(prod_tmp.name,'') || ' -' 
            || COALESCE(product.variants,'') AS producte,
            NULL::double precision AS intensitat,
            now() as materialize_date
        FROM giscedata_celles_cella AS cel
        LEFT JOIN giscedata_tipus_installacio ti 
            ON cel.tipus_instalacio_cnmc_id = ti.id
        LEFT JOIN giscedata_celles_tipus_element tipus_element 
            ON cel.tipus_element = tipus_element.id
        LEFT JOIN stock_production_lot stock ON cel.serial = stock.id
        LEFT JOIN product_product product ON stock.product_id = product.id
        LEFT JOIN product_template prod_tmp 
            ON product.product_tmpl_id = prod_tmp.id
        WHERE cel.active
        UNION
        SELECT
            ('2' || pos.id::varchar)::int AS id,
            'INT_AUTO_AT' AS symbol,
            pos.name AS codi,
            pos.geom AS geom,
            360 - pos.rotation AS rotation,
            CASE WHEN pos.interruptor='1'::text THEN 'PAR.'
                 WHEN pos.interruptor='2'::text THEN 'INT. AUTO.'
                 ELSE 'S. INT.'
            END AS descripcio,
            'giscedata.cts.subestacions,'::text 
            || pos.subestacio_id::text AS installacio,
            ' '::text AS inventari,
            pos.data_pm AS data_pm,
            pos.cini AS cini,
            ti.descripcio AS ti,
            CASE WHEN pos.interruptor='1'::text THEN 'Parc'
                 WHEN pos.interruptor='2'::text THEN 'Interruptor Automàtic'
                 ELSE 'Sense Interruptor'
            END AS tipus_element,
            stock.name AS numero_serie,
            '[' || product.default_code || ']' || ' ' 
            || COALESCE(prod_tmp.name,'') || ' -' 
            || COALESCE(product.variants,'') AS producte,
            NULL::double precision AS intensitat,
            now() as materialize_date
        FROM giscedata_cts_subestacions_posicio AS pos
        LEFT JOIN giscedata_tipus_installacio ti 
            ON pos.tipus_instalacio_cnmc_id = ti.id
        LEFT JOIN stock_production_lot stock ON pos.serial = stock.id
        LEFT JOIN product_product product ON stock.product_id = product.id
        LEFT JOIN product_template prod_tmp 
            ON product.product_tmpl_id = prod_tmp.id
        WHERE pos.active
        UNION
        SELECT
            ('3' || c.id::varchar)::int AS id,
            'CONDENSADOR-AT' AS symbol,
            c.name AS codi,
            c.geom AS geom,
            360-c.rotation AS rotation,
            CASE WHEN c.tipus='1' AND c.xarxa='1' THEN 'REACT. AT'
                 WHEN c.tipus='1' AND c.xarxa='2' THEN 'REACT. BT'
                 WHEN c.tipus='2' AND c.xarxa='1' THEN 'COND. AT'
                 WHEN c.tipus='2' AND c.xarxa='1' THEN 'COND. BT'
                 ELSE ' '
            END AS descripcio,
            'giscedata.cts,'::text || c.ct_id::text AS installacio,
            ' '::text AS inventari,
            c.data_pm AS data_pm,
            c.cini AS cini,
            ti.descripcio AS ti,
            CASE WHEN c.tipus='1' AND c.xarxa='1' THEN 'REACT. AT'
                 WHEN c.tipus='1' AND c.xarxa='2' THEN 'REACT. BT'
                 WHEN c.tipus='2' AND c.xarxa='1' THEN 'COND. AT'
                 WHEN c.tipus='2' AND c.xarxa='1' THEN 'COND. BT'
                 ELSE ' '
            END AS tipus_element,
            stock.name AS numero_serie,
            '[' || product.default_code || ']' || ' ' 
            || COALESCE(prod_tmp.name,'') || ' -' 
            || COALESCE(product.variants,'') AS producte,
            NULL::double precision AS intensitat,
            now() as materialize_date
        FROM giscedata_condensadors AS c
        LEFT JOIN giscedata_tipus_installacio ti 
            ON c.tipus_instalacio_cnmc_id = ti.id
        LEFT JOIN stock_production_lot stock ON c.serial = stock.id
        LEFT JOIN product_product product ON stock.product_id = product.id
        LEFT JOIN product_template prod_tmp 
            ON product.product_tmpl_id = prod_tmp.id
        WHERE c.active
        AND c.tipus = '2'
        AND c.xarxa = '1';
        """)
        cursor.execute("""
        CREATE INDEX giscegis_interruptorsat_geom_geom
        ON giscegis_interruptorsat_geom USING gist (geom);
        """)

        return True


GiscegisInterruptorsatGeomQgis()


class GiscegisInterruptorsbtGeomQgis(osv.osv):
    """
    Model of Elem. Quadre BT for Giscegis Webgis-Qgis
    """
    _inherit = 'giscegis.interruptorsbt.geom'

    def init(self, cursor):
        """
        Overwrites the init method and creates a materialized view that can
        live along with Qgis without data incoherences
        :param cursor: Database cursor
        :return: True
        :rtype: bool
        """

        cursor.execute("""
            DROP MATERIALIZED VIEW IF EXISTS giscegis_interruptorsbt_geom;
        """)
        cursor.execute("""
            CREATE MATERIALIZED VIEW public.giscegis_interruptorsbt_geom AS
                SELECT
                    fus.id AS id,
                    fus.geom AS geom,
                    st_x(fus.geom) AS x,
                    st_y(fus.geom) AS y,
                    fus_tipus.code || ' ' || COALESCE(fus.codi, 'haha') || ' ' 
                    || COALESCE(fus.intensitat::text, '0') || 'A' AS texte,
                    fus_tipus.code AS blockname,
                    360-fus.rotation AS rotation,
                    fus_tipus.code AS symbol,
                    fus.intensitat AS intensitat,
                    NULL::text AS cini,
                    now() AS materialize_date,
                    '' AS sortida
                FROM giscedata_bt_quadre_element fus
                LEFT JOIN giscedata_bt_quadre_element_tipus fus_tipus
                    ON fus.blockname = fus_tipus.id
                WHERE fus.active
                UNION
                SELECT
                    c.id AS id,
                    c.geom AS geom,
                    st_x(c.geom) AS x,
                    st_y(c.geom) AS y,
                    'CONDENSADOR-BT' || ' ' || c.name AS texte,
                    'CONDENSADOR-BT' AS blockname,
                    360-c.rotation AS rotation,
                    'CONDENSADOR-BT' AS symbol,
                    NULL::double precision AS intensitat,
                    c.cini AS cini,
                    now() AS materialize_date,
                    '' AS sortida
                FROM giscedata_condensadors c
                WHERE c.active AND c.tipus = '2' AND c.xarxa = '2';
        """)

        return True


GiscegisInterruptorsbtGeomQgis()


class GiscegisTrafosGeomQgis(osv.osv):
    """
    Model of Trafos for Giscegis Webgis-Qgis
    """
    _inherit = "giscegis.trafos.geom"

    def init(self, cursor):
        """
        Overwrites the init method and creates a materialized view that can
        live along with Qgis without data incoherences
        :param cursor: Database cursor
        :return: True
        :rtype: bool
        """

        cursor.execute("""
        DROP MATERIALIZED VIEW IF EXISTS giscegis_trafos_geom;
        DROP MATERIALIZED VIEW IF EXISTS giscegis_blocs_trafos_geom;
        DROP MATERIALIZED VIEW IF EXISTS giscegis_blocs_trafos_reductors_geom;
        """)
        cursor.execute("""
            CREATE MATERIALIZED VIEW giscegis_trafos_geom AS
                SELECT 
                    trafo.id AS id,
                    trafo.geom AS geom,
                    CASE
                         WHEN trafo.reductor THEN 'TRANSFORMADOR_R'
                         ELSE 'TRANSFORMADOR'
                    END AS symbol,
                    trafo.rotation AS rotation,
                    'TRF-' || trafo.name::text || E'\n'::text 
                    || COALESCE(trafo.potencia_nominal::text, 'S/P'::text)
                    || ' kVA' AS texte,
                    now() AS materialize_date
                FROM giscedata_transformador_trafo trafo
                LEFT JOIN giscedata_transformador_estat trafo_estat 
                    ON trafo.id_estat = trafo_estat.id
                WHERE trafo.active AND trafo_estat.codi=1;
            
            CREATE MATERIALIZED VIEW public.giscegis_blocs_trafos_geom AS
                SELECT 
                    trafo.id AS id,
                    trafo.geom AS geom,
                    'TRANSFORMADOR'::text AS symbol,
                    trafo.rotation - 180 AS rotation,
                    'TRF-' || trafo.name::text || E'\n'::text 
                    || COALESCE(trafo.potencia_nominal::text, 'S/P'::text)
                    || ' kVA' AS texte,
                    now() AS materialize_date
                FROM giscedata_transformador_trafo trafo
                LEFT JOIN giscedata_transformador_estat trafo_estat 
                    ON trafo.id_estat = trafo_estat.id
                WHERE NOT trafo.reductor AND trafo.active 
                    AND trafo_estat.codi=1;
            
            CREATE MATERIALIZED VIEW public.giscegis_blocs_trafos_reductors_geom AS
                SELECT 
                    trafo.id AS id,
                    trafo.geom AS geom,
                    'TRANSFORMADOR_R'::text AS blockname,
                    trafo.rotation - 180 AS rotation,
                    'TRF-' || trafo.name::text || E'\n'::text 
                    || COALESCE(trafo.potencia_nominal::text, 'S/P'::text)
                    || ' kVA' AS texte,
                    now() AS materialize_date
                FROM giscedata_transformador_trafo trafo
                LEFT JOIN giscedata_transformador_estat trafo_estat 
                    ON trafo.id_estat = trafo_estat.id
                WHERE trafo.reductor AND trafo.active AND trafo_estat.codi=1;
        """)

        cursor.execute("""
            CREATE INDEX giscegis_trafos_geom_geom ON giscegis_trafos_geom 
                USING gist (geom);
            CREATE INDEX "giscegis_blocs_trafos_geom_geom" 
                ON giscegis_blocs_trafos_geom USING gist (geom);
            CREATE INDEX "giscegis_blocs_trafos_reductors_geom_geom" 
                ON giscegis_blocs_trafos_reductors_geom USING gist (geom);
        """)

        return True


GiscegisTrafosGeomQgis()


class GiscegisNodesGeomQgis(osv.osv):
    """
    Model of Nodes for Giscegis Webgis-Qgis
    """
    _inherit = 'giscegis.nodes.geom'

    def init(self, cursor):
        """
        Initializes the module and creates the materialized view
        :param cursor: cursor Database cursor
        :return: True
        :rtype: bool
        """
        cursor.execute("DROP MATERIALIZED VIEW IF EXISTS giscegis_nodes_geom;")
        cursor.execute("""
            CREATE MATERIALIZED VIEW giscegis_nodes_geom AS
                SELECT 
                    nodes.id as id,
                    nodes.name AS name,
                    nodes.geom AS geom,
                    now() AS materialize_date
                FROM giscegis_nodes AS nodes;
        """)
        return True


GiscegisNodesGeomQgis()


class GiscegisErGeomQgis(osv.osv):
    """
    Model of Subestacions for Giscegis Webgis-Qgis
    """
    _inherit = "giscegis.er.geom"

    def init(self, cursor):
        cursor.execute("""
            DROP MATERIALIZED VIEW IF EXISTS giscegis_er_geom;
        """)

        cursor.execute("""
            CREATE MATERIALIZED VIEW public.giscegis_er_geom AS
                SELECT 
                    se.id AS id,
                    se.ct_id AS ct_id,
                    se.tecnologia AS tipus_parc,
                    se.tecnologia AS tecnologia,
                    ct.geom AS geom,
                    ct.descripcio AS texte,
                    now() AS materialize_date
                FROM giscedata_cts_subestacions se
                LEFT JOIN giscedata_cts ct ON ct.id = se.ct_id;
        """)

        cursor.execute("""
            CREATE INDEX giscegis_er_geom_geom ON giscegis_er_geom
            USING gist (geom);
        """)


GiscegisErGeomQgis()


class GiscegisLatGeomQgis(osv.osv):
    _inherit = 'giscegis.lat.geom'

    def init(self, cursor):

        cursor.execute("""
            SELECT relname
            FROM pg_class
            WHERE relkind = 'S' AND relname = 'giscegis_lat_id_seq'
        """)
        res = cursor.fetchall()
        srid = config.get('srid', 25830)
        if not res:
            # Creacio de la sequencia si no existeix
            cursor.execute("""
                CREATE SEQUENCE public.giscegis_lat_id_seq
                    INCREMENT 1
                    MINVALUE 1
                    MAXVALUE 9223372036854775807
                    START 1
                    CACHE 1;
            """)

        sql_exists = """
            SELECT EXISTS (
                SELECT 1
                FROM   information_schema.tables
                WHERE  table_schema = 'public'
                AND    table_name = 'giscegis_lat'
            );
        """
        cursor.execute(sql_exists)
        res_exists = cursor.fetchall()[0][0]
        # Creacio de la taula giscegis_lat
        if not res_exists:
            sql_giscegis_lat = """
                CREATE TABLE IF NOT EXISTS  public.giscegis_lat (
                    gid integer NOT NULL DEFAULT
                    nextval('giscegis_lat_id_seq'::regclass),
                    id double precision,
                    CONSTRAINT giscegis_lat_pkey PRIMARY KEY (gid));

                SELECT AddGeometryColumn(
                    'public','giscegis_lat', 'geom', %(srid)s, 
                    'MULTILINESTRING',2, true
                );
            """
            cursor.execute(sql_giscegis_lat, {'srid': srid})

        cursor.execute("""
            DROP MATERIALIZED VIEW IF EXISTS giscegis_lat_geom;

            CREATE MATERIALIZED VIEW giscegis_lat_geom AS
                SELECT 
                    linia.id,
                    tram.id::int AS name,
                    tram.circuits AS circuits, 
                    tram.geom AS geom,
                    ST_Simplify(tram.geom, 0.5) AS simple_geom,
                    COALESCE(linia.name, 'S/Linia') AS texte,
                CASE
                    WHEN tipus_cable.codi = 'D' OR tipus_cable.codi = 'T'
                        THEN 'A-' || linia.tensio::text
                    WHEN tipus_cable.codi = 'S'
                        THEN 'S-' || linia.tensio::text
                    WHEN tipus_cable.codi = 'E' 
                        THEN 'EAT'
                    ELSE '-' || linia.tensio::text
                END AS theme,
                tram.cini AS cini,
                now() AS materialize_date
                FROM giscedata_at_tram AS tram
                LEFT JOIN giscedata_at_linia linia ON 
                    tram.linia = linia.id AND linia.name::text <> '1'::text
                LEFT JOIN giscedata_at_cables cable ON tram.cable = cable.id
                LEFT JOIN giscedata_at_tipuscable AS tipus_cable
                    ON cable.tipus = tipus_cable.id
                LEFT JOIN giscedata_tensions_tensio AS tensio ON 
                    tram.tensio_max_disseny_id = tensio.id
                WHERE tram.active;
        """)

        # Creacio d'indexs
        cursor.execute("""
            DROP INDEX IF EXISTS giscegis_lat_geom_idx;
            CREATE INDEX giscegis_lat_geos_geom 
            ON giscegis_lat_geom USING gist (geom);
        """)

        cursor.execute("""
            DROP INDEX IF EXISTS giscegis_lat_geom_idx;
            CREATE  INDEX giscegis_lat_geom_idx
            ON public.giscegis_lat USING gist(geom);
        """)

        return True


GiscegisLatGeomQgis()


class GiscegisLbtGeomQgis(osv.osv):
    _inherit = 'giscegis.lbt.geom'

    def init(self, cursor):
        cursor.execute("""
            SELECT relname
            FROM pg_class
            WHERE relkind = 'S' AND relname = 'giscegis_lbt_id_seq'
        """)
        res = cursor.fetchall()
        conf_obj = self.pool.get('res.config')
        srid_id = conf_obj.search(cursor, 1, [('name', '=', 'giscegis_srid')])
        if srid_id:
            srid = int(conf_obj.read(cursor, 1, srid_id[0], ['value'])['value'])
        else:
            srid = 23030

        if not res:
            cursor.execute("""
                CREATE SEQUENCE public.giscegis_lbt_id_seq
                    INCREMENT 1
                    MINVALUE 1
                    MAXVALUE 9223372036854775807
                    START 1
                    CACHE 1;
            """)

        sql_exists = """
            SELECT EXISTS (
                SELECT 1
                FROM   information_schema.tables
                WHERE  table_schema = 'public'
                AND    table_name = 'giscegis_lbt');
        """
        cursor.execute(sql_exists)
        res_exists = cursor.fetchall()[0][0]
        # Creacio de la taula giscegis_lbt
        if not res_exists:
            sql_giscegis_lbt = """
                CREATE TABLE IF NOT EXISTS  public.giscegis_lbt (
                    gid integer NOT NULL DEFAULT
                    nextval('giscegis_lbt_id_seq'::regclass),
                    id double precision,
                    CONSTRAINT giscegis_lbt_pkey PRIMARY KEY (gid));
                SELECT AddGeometryColumn('public','giscegis_lbt',
                    'geom',%(srid)s,'MULTILINESTRING',2, true);
            """
            cursor.execute(sql_giscegis_lbt, {'srid': srid})

        # Creacio de la taula giscegis_lbt_geom
        cursor.execute("""
            DROP MATERIALIZED VIEW IF EXISTS giscegis_lbt_geom;

            CREATE MATERIALIZED VIEW public.giscegis_lbt_geom AS
                SELECT
                    bt.id AS id,
                    bt.name AS name,
                    CASE 
                        WHEN colors_bt.color IS NOT NULL then colors_bt.color
                        ELSE ''::character varying
                    END AS color,
                    CASE
                        WHEN tipus_cable.codi = 'E' THEN 
                            'EBT'
                        WHEN substring(tipus_linia.name, 1, 1) = 'A' THEN 
                            'L' || fus.sortida_bt::text || '-A'
                        WHEN substring(tipus_linia.name, 1, 1) = 'S' THEN 
                            'L' || fus.sortida_bt::text || '-S'
                        ELSE NULL::text
                    END AS theme,
                    ct.name || '-' || fus.sortida_bt::text AS texte,
                    bt.geom AS geom,
                    st_length(bt.geom) AS length,
                    bt.voltatge AS voltatge,
                    cable.seccio AS seccio,
                    fus.sortida_bt AS sortida,
                    now() AS materialize_date
                FROM giscedata_bt_element bt 
                LEFT JOIN giscedata_bt_tipuslinia tipus_linia
                    ON tipus_linia.id = bt.tipus_linia
                LEFT JOIN giscegis_lbt_traceability trace ON 
                    bt.id = trace.element
                LEFT JOIN giscedata_bt_cables cable ON 
                    bt.cable = cable.id
                LEFT JOIN giscedata_bt_tipuscable tipus_cable ON 
                    cable.tipus = tipus_cable.id
                LEFT JOIN giscegis_blocs_fusiblesbt fus ON 
                    trace.fusible = fus.id
                LEFT JOIN giscedata_cts ct ON ct.id = trace.ct
                LEFT JOIN giscegis_colors_bt colors_bt ON 
                    colors_bt."name"::text = fus.sortida_bt
                WHERE bt.active
                GROUP BY bt.id, tipus_cable.codi, tipus_linia.name, 
                    fus.sortida_bt, ct.name, cable.seccio, fus.sortida_bt, 
                    colors_bt.color, bt.geom;
        """)

        # Creacio d'indexs pe a les geometries
        cursor.execute("""
            DROP INDEX IF EXISTS giscegis_lbt_geom_geom;
            CREATE INDEX giscegis_lbt_geom_geom
                ON giscegis_lbt_geom USING gist (geom);
        """)
        cursor.execute("""
            DROP INDEX IF EXISTS giscegis_lbt_geom_idx;
            CREATE INDEX giscegis_lbt_geom_idx ON 
            public.giscegis_lbt USING gist (geom);
        """)

        return True
