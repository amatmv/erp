from destral import testing
from destral.transaction import Transaction


class GiscegisSimulacioAT(testing.OOTestCase):

    def test_stop_nodes_at(self):
        """
        Tests that the stop_nodes of simulacio AT are correctly filled
        :return:
        """
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor

            model_obj = self.openerp.pool.get("ir.model")
            sim_obj = self.openerp.pool.get('giscegis.simulacio.at')
            nbm_obj = self.openerp.pool.get('giscegis.node.bloc.model')
            trf_obj = self.openerp.pool.get('giscedata.transformador.trafo')

            search_params_mod = [
                ("model", "=", "giscedata.transformador.trafo")
            ]

            trf_mdl_id = model_obj.search(cursor, uid, search_params_mod)
            nbm_obj.generate(cursor, uid)
            trf_id = trf_obj.search(cursor, uid, [])[0]
            trf_obj.write(cursor, uid, trf_id, {"reductor": True})

            search_params = [
                ("res_id", "=", trf_id),
                ("model", "=", trf_mdl_id)
            ]
            nbm_id = nbm_obj.search(cursor, uid, search_params)
            node = nbm_obj.read(cursor, uid, nbm_id, ["node"])[0]["node"]

            self.assertNotIn(node, sim_obj.get_stop_nodes(cursor, uid))

    def test_open_nodes_at(self):
        """
        Tests that open nodes of simulacio AT don't have Nones
        :return:
        """
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor

            sim_obj = self.openerp.pool.get('giscegis.simulacio.at')
            self.assertNotIn(None, sim_obj.get_open_nodes(cursor, uid))
            self.assertIsNotNone(sim_obj.get_open_nodes(cursor, uid))

    def test_all_conected(self):
        """
        Test simple AT simulation
        :return: None
        """
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            nodes_obj = self.openerp.pool.get('giscegis.nodes')
            vertex_obj = self.openerp.pool.get('giscegis.vertex')
            edge_obj = self.openerp.pool.get('giscegis.edge')
            sim_obj = self.openerp.pool.get('giscegis.simulacio.at')
            poly_obj = self.openerp.pool.get('giscegis.polyline')
            frontera_obj = self.openerp.pool.get('giscedata.punt.frontera')
            netsource_obj = self.openerp.pool.get('giscegis.blocs.netsource')

            v1 = vertex_obj.create(cursor, uid, {"name": "v1", "x": 1, "y": 1})
            v2 = vertex_obj.create(cursor, uid, {"name": "v2", "x": 2, "y": 2})
            v3 = vertex_obj.create(cursor, uid, {"name": "v3", "x": 3, "y": 3})
            v4 = vertex_obj.create(cursor, uid, {"name": "v4", "x": 4, "y": 4})

            n1 = nodes_obj.create(cursor, uid, {"name": "n1", "vertex": v1})
            n2 = nodes_obj.create(cursor, uid, {"name": "n2", "vertex": v2})
            n3 = nodes_obj.create(cursor, uid, {"name": "n3", "vertex": v3})
            n4 = nodes_obj.create(cursor, uid, {"name": "n4", "vertex": v4})

            netsource_obj.create(
                cursor, uid,
                {
                    "name": "font",
                    "node": n1
                }
            )

            net_id = frontera_obj.create(
                cursor, uid,
                {
                    "name": "font",
                    "node_id": n1,
                    "tipus": 1,
                    "codi": "F1"
                }
            )

            p1 = poly_obj.create(
                cursor, uid,
                {"name": "p1", "vertex_ids": [v1, v2]}
            )
            p2 = poly_obj.create(
                cursor, uid,
                {"name": "p2", "vertex_ids": [v2, v3]}
            )
            p3 = poly_obj.create(
                cursor, uid,
                {"name": "p3", "vertex_ids": [v3, v4]}
            )
            p4 = poly_obj.create(
                cursor, uid,
                {"name": "p4", "vertex_ids": [v4, v1]}
            )

            e1 = edge_obj.create(
                cursor, uid,
                {
                    "name": "e1",
                    "start_node": n1,
                    "end_node": n2,
                    "polyline": p1
                }
            )
            e2 = edge_obj.create(
                cursor, uid,
                {
                    "name": "e2",
                    "start_node": n2,
                    "end_node": n3,
                    "polyline": p2
                }
            )
            e3 = edge_obj.create(
                cursor, uid,
                {
                    "name": "e3",
                    "start_node": n3,
                    "end_node": n4,
                    "polyline": p3
                }
            )
            e4 = edge_obj.create(
                cursor, uid,
                {
                    "name": "e4",
                    "start_node": n4,
                    "end_node": n1,
                    "polyline": p4
                }
            )

            ret = sim_obj.simulacio_ids(
                cursor, uid, [net_id], []
            )
            self.assertEqual([], ret["trams_off"])
            self.assertEqual(
                set([int(e1), int(e2), int(e3), int(e4)]),
                set(ret["trams_on"])
            )
            self.assertEqual(0, ret["potencia_contractada_off"])
            self.assertEqual(0, ret["num_polisses_off"])

    def test_all_disconected(self):
        """
        Test simple AT simulation
        :return: None
        """
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            nodes_obj = self.openerp.pool.get('giscegis.nodes')
            vertex_obj = self.openerp.pool.get('giscegis.vertex')
            edge_obj = self.openerp.pool.get('giscegis.edge')
            sim_obj = self.openerp.pool.get('giscegis.simulacio.at')
            poly_obj = self.openerp.pool.get('giscegis.polyline')
            frontera_obj = self.openerp.pool.get('giscedata.punt.frontera')
            netsource_obj = self.openerp.pool.get('giscegis.blocs.netsource')

            v1 = vertex_obj.create(cursor, uid, {"name": "v1", "x": 1, "y": 1})
            v2 = vertex_obj.create(cursor, uid, {"name": "v2", "x": 2, "y": 2})
            v3 = vertex_obj.create(cursor, uid, {"name": "v3", "x": 3, "y": 3})
            v4 = vertex_obj.create(cursor, uid, {"name": "v4", "x": 4, "y": 4})

            n1 = nodes_obj.create(cursor, uid, {"name": "n1", "vertex": v1})
            n2 = nodes_obj.create(cursor, uid, {"name": "n2", "vertex": v2})
            n3 = nodes_obj.create(cursor, uid, {"name": "n3", "vertex": v3})
            n4 = nodes_obj.create(cursor, uid, {"name": "n4", "vertex": v4})

            netsource_obj.create(
                cursor, uid,
                {"name": "font", "node": n1}
            )

            net_id = frontera_obj.create(
                cursor, uid,
                {
                    "name": "font",
                    "node_id": n1,
                    "tipus": 1,
                    "codi": "F1"
                }
            )

            p1 = poly_obj.create(
                cursor, uid,
                {"name": "p1", "vertex_ids": [v1, v2]}
            )
            p2 = poly_obj.create(
                cursor, uid,
                {"name": "p2", "vertex_ids": [v2, v3]}
            )
            p3 = poly_obj.create(
                cursor, uid,
                {"name": "p3", "vertex_ids": [v3, v4]}
            )
            p4 = poly_obj.create(
                cursor, uid,
                {"name": "p4", "vertex_ids": [v4, v1]}
            )

            e1 = edge_obj.create(
                cursor, uid,
                {
                    "name": "e1",
                    "start_node": n1,
                    "end_node": n2,
                    "polyline": p1
                }
            )
            e2 = edge_obj.create(
                cursor, uid,
                {
                    "name": "e2",
                    "start_node": n2,
                    "end_node": n3,
                    "polyline": p2
                }
            )
            e3 = edge_obj.create(
                cursor, uid,
                {
                    "name": "e3",
                    "start_node": n3,
                    "end_node": n4,
                    "polyline": p3
                }
            )
            e4 = edge_obj.create(
                cursor, uid,
                {
                    "name": "e4",
                    "start_node": n4,
                    "end_node": n1,
                    "polyline": p4
                }
            )

            ret = sim_obj.simulacio_ids(
                cursor, uid, [net_id], [n1, n2, n3, n4]
            )
            self.assertEqual(set([e1, e2, e3, e4]), set(ret["trams_off"]))
            self.assertEqual([], ret["trams_on"])
            self.assertEqual(0, ret["potencia_contractada_off"])
            self.assertEqual(0, ret["num_polisses_off"])

    def test_open_switch(self):
        """
        Test simple AT simulation
        :return: None
        """
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor

            nodes_obj = self.openerp.pool.get('giscegis.nodes')
            vertex_obj = self.openerp.pool.get('giscegis.vertex')
            edge_obj = self.openerp.pool.get('giscegis.edge')
            sim_obj = self.openerp.pool.get('giscegis.simulacio.at')
            poly_obj = self.openerp.pool.get('giscegis.polyline')
            netsource_obj = self.openerp.pool.get('giscegis.blocs.netsource')
            frontera_obj = self.openerp.pool.get('giscedata.punt.frontera')
            net_tipus_obj = self.openerp.pool.get("giscedata.punt.frontera.tipus")
            gis_escomesa_obj = self.openerp.pool.get('giscegis.blocs.escomeses')
            model_obj = self.openerp.pool.get('ir.model')
            nbm_obj = self.openerp.pool.get('giscegis.node.bloc.model')
            esc_obj = self.openerp.pool.get('giscedata.cups.escomesa')
            cups_obj = self.openerp.pool.get('giscedata.cups.ps')
            pol_obj = self.openerp.pool.get('giscedata.polissa')
            frontera_obj = self.openerp.pool.get('giscedata.punt.frontera')

            cups_id = cups_obj.search(
                cursor, uid,
                [("name", "=", "ES1234000000000001JN0F")]
            )
            pol_id = pol_obj.create(cursor, uid,{"name": "pol1", "cups":cups_id[0],"potencia":3})
            esc_id = esc_obj.create(cursor, uid, {"name":"esc1", "polissa_polissa": pol_id})
            cups_obj.write(cursor,uid,cups_id,{"id_escomesa":esc_id})

            v1 = vertex_obj.create(cursor,uid, {"name": "v1", "x": 1, "y": 1})
            v2 = vertex_obj.create(cursor,uid, {"name": "v2", "x": 2, "y": 2})
            v3 = vertex_obj.create(cursor,uid, {"name": "v3", "x": 3, "y": 3})
            v4 = vertex_obj.create(cursor,uid, {"name": "v4", "x": 4, "y": 4})

            n1 = nodes_obj.create(cursor, uid, {"name": "n1", "vertex": v1})
            n2 = nodes_obj.create(cursor, uid, {"name": "n2", "vertex": v2})
            n3 = nodes_obj.create(cursor, uid, {"name": "n3", "vertex": v3})
            n4 = nodes_obj.create(cursor, uid, {"name": "n4", "vertex": v4})

            g_esc_id = gis_escomesa_obj.create(cursor,uid, {"node": n4,"name": "escomesa1", "escomesa": esc_id})

            netsource_obj.create(
                cursor, uid,
                {"name": "font", "node": n1}
            )

            net_id = frontera_obj.create(
                cursor, uid,
                {
                    "name": "font",
                    "node_id": n1,
                    "tipus": 1,
                    "codi": "F1"
                }
            )

            p1 = poly_obj.create(
                cursor, uid,
                {"name": "p1", "vertex_ids": [v1, v2]}
            )
            p2 = poly_obj.create(
                cursor, uid,
                {"name": "p2", "vertex_ids": [v2, v3]}
            )
            p3 = poly_obj.create(
                cursor, uid,
                {"name": "p3", "vertex_ids": [v3, v4]}
            )
            p4 = poly_obj.create(
                cursor, uid,
                {"name": "p4", "vertex_ids": [v4, v1]}
            )

            id_model_esc = model_obj.search(cursor,uid,[("model","=",'giscegis.blocs.escomeses')])
            nbm_obj.create(cursor,uid, {"node": n4, "model":id_model_esc[0], "res_id": g_esc_id})


            e1 = edge_obj.create(
                cursor,uid,
                {
                    "name":"e1",
                    "start_node": n1,
                    "end_node": n2,
                    "polyline": p1
                }
            )
            e2 = edge_obj.create(
                cursor,uid,
                {
                    "name":"e2",
                    "start_node": n2,
                    "end_node": n3,
                    "polyline":p2
                }
            )
            e3 = edge_obj.create(
                cursor, uid,
                {
                    "name": "e3",
                    "start_node": n3,
                    "end_node": n4,
                    "polyline": p3
                }
            )
            #e4 = edge_obj.create(
            #    cursor, uid,
            #    {
            #        "name": "e4",
            #        "start_node": n4,
            #        "end_node": n1,
            #        "polyline":p4
            #    }
            #)

            ret = sim_obj.simulacio_ids(
                cursor, uid, [net_id], [n2]
            )
            self.assertEqual(set([e3, e2]), set(ret["trams_off"]))
            self.assertEqual([e1], ret["trams_on"])
            self.assertEqual(0, ret["potencia_contractada_off"])
            self.assertEqual(0, ret["num_polisses_off"])
