# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Simulació AT",
    "description": """Simulacions AT""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_base",
        "giscegis_topologia",
        "giscegis_cups",
        "giscedata_cups_distri",
        "giscedata_transformadors",
        "giscegis_blocs_stop_nodes_at",
        "giscegis_blocs_transformadors",
        "giscegis_configsimulacio_at",
        "giscedata_polissa_distri",
        "giscegis_blocs_escomeses",
        "giscegis_node_bloc_model"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "ir.model.access.csv",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
