# *-* coding: utf-8 *-*

from osv import osv, fields

from tools.sql import is_postgis_db


class GiscegisSimulacioAt(osv.osv):

    """
    Classe sense taula amb les funcions per la simulacio
    de la xarxa AT
    """

    _name = 'giscegis.simulacio.at'
    _auto = False

    _columns = {}

    open_nodes = []
    stop_nodes = []

    def init(self, cr):
        """
        Initilizes the model loading the AT stop nodes

        :param cr: Database cursor
        :return: None
        :rtype: None
        """

        self.open_nodes = self.get_open_nodes(cr, 1)
        self.stop_nodes = self.get_stop_nodes(cr, 1)

    def get_open_nodes(self, cursor, uid):
        """
        Returns the open nodes

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :return: List of ids of open nodes
        :rtype: list(int)
        """

        sql_configdefault = """
            SELECT node
            FROM giscegis_configdefault_at
            WHERE node IS NOT NULL
        """
        cursor.execute(sql_configdefault)
        ret = [a[0] for a in cursor.fetchall()]
        if ret:
            return ret
        else:
            return []

    def get_stop_nodes(self, cursor, uid):
        """
        Returns the AT stop nodes

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :return: List of AT nodes
        :rtype: list(int)
        """

        ir_mod_obj = self.pool.get("ir.model")
        stop_obj = self.pool.get("giscegis.blocs.stop.nodes.at")

        sql_bloc_model_no_domain = """
          SELECT node
          FROM giscegis_node_bloc_model
          WHERE model IN (
              SELECT name
              FROM giscegis_blocs_stop_nodes_at
              WHERE domain IS NULL OR domain = '[]'
          )
        """

        cursor.execute(sql_bloc_model_no_domain)
        ret = [a[0] for a in cursor.fetchall()]

        sql_bloc_model_domain = """
            SELECT
                node,
                res_id,
                model
            FROM giscegis_node_bloc_model
            WHERE model IN (
                SELECT name
                FROM giscegis_blocs_stop_nodes_at
                WHERE domain IS NOT NULL AND domain != '[]'
            )
        """

        cursor.execute(sql_bloc_model_domain)
        domain_nodes = {}

        result = cursor.fetchall()

        for res in result:
            if res[2] not in domain_nodes:
                domain_nodes[res[2]] = [(res[0], res[1])]
            else:
                domain_nodes[res[2]].append((res[0], res[1]))

        for model_id in domain_nodes.keys():
            model_name = ir_mod_obj.read(cursor, uid, model_id, ["model"])["model"]
            mod_obj = self.pool.get(model_name)
            stop_id = stop_obj.search(cursor, uid, [("name", "=", model_id)])
            dom_str = stop_obj.read(cursor, uid, stop_id[0], ["domain"])["domain"]
            domain = eval(dom_str)
            query = mod_obj.q(cursor, 1).select(['id'], order_by=["id"]).where(domain)
            cursor.execute(*query)
            inc_ids = [element[0] for element in cursor.fetchall()]
            for res in domain_nodes[model_id]:
                if res[1] in inc_ids:
                    ret.append(res[0])
        return ret

    def subgraf(self, G, source, extra_nodes=None):
        """
        Aquest mètode retorna un subgraf de la xarxa AT, partint de la font `source`
        i parant tant als nodes STOP com als nodes oberts segons l'estat de la xarxa.

        :param G: Graf
        :param source: Source node
        :type source: int
        :param extra_nodes: Extra stop nodes
        :type extra_nodes: list(int)

        :return: Graf
        """

        if extra_nodes is None:
            extra_nodes = []

        nlist = [source]  # only process component with source

        seen = {}  # nodes seen
        pre = []  # list of nodes in a DFS preorder
        for source in nlist:
            if source in seen:
                continue
            lifo = [source]
            while lifo:
                v = lifo.pop()
                # Condicions de sortida
                # Si el node és del tipus 'stop' o està
                # obert degut a l'estat de la xarxa
                if v.id in self.stop_nodes or v.id in self.open_nodes or v.id in extra_nodes:
                    pre.append(v)
                    continue
                if v in seen:
                    continue
                pre.append(v)
                seen[v] = True
                lifo.extend((w for w in G.neighbors(v) if w not in seen))
        return G.subgraph(pre)

    def subgraf_all(self, cr, uid, G, source):
        """
        Aquest mètode retorna un subgraf de la xarxa AT, partint de la font
        `source` i parant *només* als nodes STOP.
        """
        nlist = [source]
        seen = {}
        pre = []
        for source in nlist:
            if source in seen:
                continue
            lifo = [source]
            while lifo:
                v = lifo.pop()
                if v.id in self.stop_nodes:
                    pre.append(v)
                    continue
                if v in seen:
                    continue
                pre.append(v)
                seen[v] = True
                lifo.extend((w for w in G.neighbors(v) if w not in seen))
        return G.subgraph(pre)

    def dfs_continuitat(self, G, source, sim_open_nodes=[]):
        """
        Aquest mètode retorna un subgraf de la xarxa AT, partint de la font `source`
        i parant *només* als nodes oberts per la simulació.
        """
        nlist = [source]  # only process component with source
        seen = {}  # nodes seen
        pre = []  # list of nodes in a DFS preorder
        for source in nlist:
            if source in seen:
                continue
            lifo = [source]
            while lifo:
                v = lifo.pop()
                # Condicions de sortida
                # Si està obert per la simulació
                if v.id in sim_open_nodes:
                    pre.append(v)
                    continue
                if v in seen:
                    continue
                pre.append(v)
                seen[v] = True
                lifo.extend((w for w in G.neighbors(v) if w not in seen))
        return G.subgraph(pre)

    def dfs_continuitat_2(self, G, source, sim_open_nodes=[]):
        """
        Aquest mètode retorna un subgraf de la xarxa AT, partint de la font `source`
        i parant tant als nodes oberts per la simulació i els STOP nodes.
        """
        nlist = [source]  # only process component with source
        seen = {}  # nodes seen
        pre = []  # list of nodes in a DFS preorder
        for source in nlist:
            if source in seen:
                continue
            lifo = [source]
            while lifo:
                v = lifo.pop()
                # Condicions de sortida
                # Si està obert per la simulació o és un node stop
                if v.id in sim_open_nodes or v.id in self.stop_nodes:
                    pre.append(v)
                    continue
                if v in seen:
                    continue
                pre.append(v)
                seen[v] = True
                lifo.extend((w for w in G.neighbors(v) if w not in seen))
        return G.subgraph(pre)

    def simulacio_descarrec_geom(self, cursor, uid, netsource, sol_descarrec_id,
                                 sim_open_nodes=[], context={}, load=False,
                                 srid=False):
        """
        Generates descarrec simulation

        :param cursor: Database cursor
        :param uid: User id
        :param netsource: Simulation netsource
        :param sol_descarrec_id: Id solicitud decarrec
        :param sim_open_nodes: List of open nodes
        :param context: OpenERP context
        :param load:
        :param srid: Response SRID
        :return: GeoJSON response
        """
        resultat = self.simulacio_descarrec_ids(
            cursor, uid, netsource, sol_descarrec_id, sim_open_nodes, context, load
        )
        res = self.get_simulation_geom(cursor, uid, resultat, method='postgis',
                                       srid=srid)
        return res

    def get_simulation_geom(self, cursor, uid, resultat, method='poly',
                            srid=False):
        # Updates edges list in from method:
        #  method 'poly': polyline (gis 1.x)
        #  method 'postgis': GeoJSON geometry (gis 2015)
        edge_obj = self.pool.get('giscegis.edge')

        # Generate polylines from edge id's
        trams = {'trams_on': [], 'trams_off': []}
        if method == 'postgis':
            for list_name in ['trams_on', 'trams_off']:
                geom = edge_obj.get_geom_multi(cursor, uid,
                                               resultat[list_name], srid, mergeGeoms=True)
                resultat.update({list_name: geom})
        else:
            for list_name in ['trams_on', 'trams_off']:
                for edge_id in resultat[list_name]:
                    trams[list_name].append(
                        edge_obj.get_edge_polyline(
                                cursor, uid, edge_id)
                    )
                resultat.update({list_name: trams[list_name]})
        return resultat

    def get_downstream(self, cursor, uid, netsource, circuit_node,
                       graph=None, context=None):
        """
        Ens arriba com a paràmetre una font, el node d'inici del cirtuit
        i els nodes de la resta d'interruptors

        :param cursor: Cursor de BD d'OpenERP
        :param uid: User id
        :param netsource: Punt d'entrada de corrent a la xarxa
        :param circuit_node: Node de que es vol calcular el circuit
        :param graph: Opcional, graph de la topologia de la xarxa,
        si es None es recalcula
        :param context: Context d'OpenERP
        :return: Llista de nodes d'un circuit
        """

        if not context:
            context = {}

        circuit_node = [circuit_node]

        nodes_obj = self.pool.get('giscegis.configdefault.at')
        nodes_ids = nodes_obj.search(cursor, uid, [])
        default_open_nodes = nodes_obj.read(cursor, uid, nodes_ids, ['node'])
        default_open_nodes = [x['node'][0] for x in default_open_nodes]

        obj_posicions = self.pool.get('giscedata.cts.subestacions.posicio')
        ids_posicions = obj_posicions.search(cursor, uid, [])
        posicions = obj_posicions.read(cursor, uid, ids_posicions, ['name'])
        names_interruptors = [x['name'] for x in posicions]

        obj_interruptors = self.pool.get('giscegis.blocs.interruptorat')
        int_filter = [('codi', 'in', names_interruptors)]
        ids_interruptors = obj_interruptors.search(cursor, uid, int_filter)
        posicions_nodes = obj_interruptors.read(
            cursor, uid, ids_interruptors, ['node', 'codi'])

        obj_interruptors = self.pool.get('giscegis.blocs.seccionadorunifilar')
        int_filter = [('codi', 'in', names_interruptors)]
        ids_interruptors = obj_interruptors.search(cursor, uid, int_filter)
        posicions_nodes = posicions_nodes + obj_interruptors.read(
            cursor, uid, ids_interruptors, ['node', 'codi'])
        posicions_nodes = [x['node'][0] for x in posicions_nodes]

        node_obj = self.pool.get('giscegis.nodes')
        self.init(cursor)

        if graph:
            G = graph
        else:
            topo_obj = self.pool.get('giscegis.topologia')
            G = topo_obj.load(cursor, uid, context)

        circuit_source = node_obj.browse(cursor, uid, circuit_node[0], context)
        posicions_nodes.remove(circuit_node[0])

        netsource_bloc_obj = self.pool.get('giscegis.blocs.netsource')
        netsource_bloc = netsource_bloc_obj.browse(
            cursor, uid, netsource, context)
        source = node_obj.browse(cursor, uid, netsource_bloc.node.id, context)

        Gnetsource = self.dfs_continuitat_2(
            G, source, circuit_node + default_open_nodes)
        Gcircuit = self.dfs_continuitat_2(
            G, circuit_source, posicions_nodes+default_open_nodes)

        trams_netsource = []
        for edgen in [e[2] for e in Gnetsource.edges()]:
            trams_netsource.append(edgen.id)

        trams_circuit = []

        for edge in [e[2] for e in Gcircuit.edges()]:
            if edge.id not in trams_netsource:
                trams_circuit.append(int(edge.id))

        # cts_obj = self.pool.get('giscegis.blocs.ctat')
        nodes_circuit = []
        for node in Gcircuit.nodes():
            if node not in Gnetsource.nodes():
                nodes_circuit.append(node.id)
        # cts_bloc_id = cts_obj.search(cursor, uid, [('node', 'in', nodes_circuit)])
        # cts = cts_obj.read(cursor, uid, cts_bloc_id, ['ct'])
        # cts = [x['ct'][0] for x in cts]
        return {'tram': trams_circuit, 'ct': nodes_circuit}

    def simulacio_ids(self, cr, uid, netsource, sim_open_nodes=[], context={},
                      load=False):
        """
        Ens arriba com a paràmetre un Source. Des d'allà creem un subgraf fins
        els nodes oberts de l'estat de la xarxa o els nodes 'stop'
        @netsource, Punt d'entrada de corrent a la xarxa
        @sim_open_nodes, llista d'Ids de nodes que estan oberts per la simulació
        @load, booleà que diu si la simulació que es farà ve d'una ja guardada
        (carregada) o no, si load=True *no* s'ha de guardar res a la BD. (TODO)
        """
        if isinstance(netsource, (list, tuple)):
            netsource = netsource[0]
        # S'ha de guardar els paràmetres de la simulació per
        # poder-la recuperar deprés

        frontera_obj = self.pool.get('giscedata.punt.frontera')
        node_obj = self.pool.get('giscegis.nodes')
        netsource_bloc_obj = self.pool.get('giscegis.blocs.netsource')

        frontera_ids = frontera_obj.search(cr, uid, [])
        if netsource in frontera_ids:
            font = frontera_obj.browse(cr, uid, netsource, context)
            source = node_obj.browse(cr, uid, font.node_id.id, context)
        else:
            font = netsource_bloc_obj.browse(cr, uid, netsource, context)
            source = node_obj.browse(cr, uid, font.node.id, context)
            netsource = frontera_ids[0]


        config_obj = self.pool.get('giscegis.configsimulacio.at')
        vals = {
            'name': 'SIMAT',
            'source': netsource
        }

        config_id = config_obj.create(cr, uid, vals, context)
        config_name = vals['name']

        # Un cop tenim l'ID de la configuració, cal guardar la llista de nodes
        # oberts per aquesta simulació

        open_nodes_obj = self.pool.get('giscegis.configsimulacio.at.oberts')
        vals = {'config': config_id}
        node_obj = self.pool.get('giscegis.nodes')
        gis_escomesa_obj = self.pool.get('giscegis.blocs.escomeses')
        transformador_obj = self.pool.get('giscegis.blocs.transformadors')
        data_trafo_obj = self.pool.get('giscedata.transformador.trafo')
        gis_escomesa_obj = self.pool.get('giscegis.blocs.escomeses')
        topo_obj = self.pool.get('giscegis.topologia')
        frontera_obj = self.pool.get('giscedata.punt.frontera')
        at_escomeses_obj = self.pool.get('giscegis.simulacio.at.resultats.escomeses')
        at_trafos_obj = self.pool.get('giscegis.simulacio.at.resultats.transformadors')
        model_nodes = self.pool.get('giscegis.nodes')
        at_resultats_obj = self.pool.get('giscegis.simulacio.at.resultats')
        frontera_obj = self.pool.get('giscedata.punt.frontera')
        esc_obj = self.pool.get("giscedata.cups.escomesa")

        sim_open_nodes_r = node_obj.read(cr, uid, sim_open_nodes, context)
        nodes_ids = [n['id'] for n in sim_open_nodes_r]
        nodes_name = node_obj.get_codi_multi(cr, uid, nodes_ids)
        for n in sim_open_nodes_r:
            vals['name'] = nodes_name[n['id']] or n['name']
            vals['node'] = n['id']
            open_nodes_obj.create(cr, uid, vals)

        # ara creem l'objecte pels resultats

        cr.execute("SELECT count(id) AS n FROM giscegis_simulacio_at_resultats WHERE id = %s", (int(config_id),))
        res = cr.dictfetchone()

        vals = {'name': '%s_%i' % (config_name, res['n']+1),
                'simulacio': config_id}
        at_resultats_id = at_resultats_obj.create(cr, uid, vals, context)

        # Inicialització
        self.init(cr)
        # Carreguem topologia

        G = topo_obj.load(cr, uid, context)

        trams_on = []
        trams_off = []
        frontera_ids = frontera_obj.search(cr, uid, [])
        if netsource in frontera_ids:
            font = frontera_obj.browse(cr, uid, netsource, context)
            source = node_obj.browse(cr, uid, font.node_id.id, context)
        else:
            font = netsource_bloc_obj.browse(cr, uid, netsource, context)
            source = node_obj.browse(cr, uid, font.node.id, context)

        # Fem el subgraf per aquell netsource (canviat des de subgraf_all)
        Gat = self.subgraf(G, source)
        # Fem la continuitat tenint en compte tant els
        # interruptors oberts per la simulació com els STOP (trafos, ...)
        # ens retorna un graf amb els nodes per
        # on s'ha passat (nodes amb corrent)
        Gon = self.dfs_continuitat_2(G, source, sim_open_nodes)
        # Afegit totes les coordenades de les arestes al resultat (trams_on)
        for edge_id in [e[2]["edge_id"] for e in Gon.edges(data=True)]:
            trams_on.append(int(edge_id))
        # Fem una copia del graph on per poder-hi treballar
        Gaux = Gon.copy()
        # Borrem els nodes que siguin frontera del graf
        # amb corrent (nodes oberts)

        Gaux.remove_nodes_from(node_obj.browse(cr, uid, sim_open_nodes, context))
        # Creem una copia de tot el subgraf at
        Goff = Gat.copy()
        # Hi esborrem tots els nodes del graf que hem creat abans
        # (nodes sense corrent) i ens quedara un graf amb els nodes que no en
        # tenen + els nodes oberts (frontera)
        Goff.remove_nodes_from(Gaux.nodes())
        # Afegim totes les coordenades de les arestes al resultat (trams_off)
        for edge_id in [e[2]["edge_id"] for e in Goff.edges(data=True)]:
            trams_off.append(edge_id)

        # guardem resultats d'elements sense corrent:
        # *transformadors* i *escomeses*

        vals = {'simulacio': at_resultats_id}
        escomeses_blocs = []
        trafos_blocs = []

        nodes_ids = []
        for node in Gon.nodes():
            nodes_ids.append(node.id)
        nodes_model = model_nodes.get_model_multi(cr, uid, nodes_ids)
        for node_id, model in nodes_model.iteritems():
            if model:
                if model.model == 'giscegis.blocs.escomeses':
                    escomeses_blocs.append(node_id)
                elif model.model == 'giscegis.blocs.transformadors':
                    trafos_blocs.append(node_id)
        escomeses_ids = model_nodes.get_bloc_multi(
                cr, uid, escomeses_blocs, ['id'])
        trafos_ids = model_nodes.get_bloc_multi(cr, uid, trafos_blocs, ['id'])
        escomeses_ids = [int(x['id']) for x in escomeses_ids]
        trafos_ids = [int(x['id']) for x in trafos_ids]

        escomeses_gon = gis_escomesa_obj.read(
                cr, uid,  escomeses_ids, ['escomesa'], context)
        for escomesa_ids in escomeses_gon:
            if escomesa_ids['escomesa']:
                vals['name'] = escomesa_ids['escomesa'][0]
                vals['estat'] = 1
                at_escomeses_obj.create(cr, uid, vals, context)

        trafos_gon = transformador_obj.read(
                cr, uid, trafos_ids, ['ct', 'ordre_transformador_ct'])

        for trafo_ids in trafos_gon:
            if trafo_ids['ct'] and trafo_ids['ordre_transformador_ct']:
                # buscar el transformador del data amb
                # ct i ordre igual al del bloc
                data_trafo_ids = data_trafo_obj.search(
                        cr, uid,
                        [
                            ('ct', '=', trafo_ids['ct'][0]),
                            ('ordre_dins_ct', '=', trafo_ids['ordre_transformador_ct'])
                        ]
                )
                if data_trafo_ids:
                    vals['name'] = data_trafo_ids[0]
                    vals['estat'] = 1
                    at_trafos_obj.create(cr, uid, vals, context)
                else:
                    print("No s'ha trobat el transformador al data")
            else:
                print('Impossible trobar data del bloc_trafo, ...')

        escomeses_off = []
        trafos_off = []
        # guardem què s'ha quedat sense corrent, primer el que són nodes
        # trobats pel graf: CONTA-AT i TRAFOS
        nodes_ids = []
        for node in Goff.nodes():
            nodes_ids.append(node.id)
        nodes_model = model_nodes.get_model_multi(cr, uid, nodes_ids)
        for node_id, model in nodes_model.iteritems():
            node.get_model()

            if model:
                if model.model == 'giscegis.blocs.escomeses':
                    # escomesa (Sera CONTA-AT!)
                    bloc = model_nodes.get_bloc(cr, uid, node_id)
                    escomesa_ids = gis_escomesa_obj.read(
                            cr, uid, bloc.id, ['escomesa'], context)
                    if escomesa_ids['escomesa']:
                        vals['name'] = escomesa_ids['escomesa'][0]
                        vals['estat'] = -1
                        at_escomeses_obj.create(cr, uid, vals, context)
                        escomeses_off.append(escomesa_ids['escomesa'][0])
                    else:
                        # escomesa no trobada
                        pass
                elif model.model == "giscedata.cups.escomesa":
                    search_params = [("node_id", "=", node_id)]
                    ids = esc_obj.search(cr, uid, search_params)
                    escomesa_data = gis_escomesa_obj.read(cr, uid, ids, ['name'], context)
                    if escomesa_data:
                        vals['name'] = escomesa_data[0]['name']
                        vals['estat'] = -1
                        at_escomeses_obj.create(cr, uid, vals, context)
                        escomeses_off.append(escomesa_data[0]['name'])
                    else:
                        # escomesa no trobada
                        pass

                elif model.model == 'giscegis.blocs.transformadors':
                    # trafo
                    bloc = model_nodes.get_bloc(cr, uid, node_id)
                    trafo_ids = transformador_obj.read(cr, uid, bloc.id)
                    if trafo_ids['ct'] and trafo_ids['ordre_transformador_ct']:
                        # buscar el transformador del data amb ct i ordre
                        # igual al del bloc
                        data_trafo_ids = data_trafo_obj.search(
                            cr, uid,
                            [
                                ('ct', '=', trafo_ids['ct'][0]),
                                ('ordre_dins_ct', '=', trafo_ids['ordre_transformador_ct'])
                            ]
                        )
                        if data_trafo_ids:
                            vals['name'] = data_trafo_ids[0]
                            vals['estat'] = -1
                            at_trafos_obj.create(
                                    cr, uid, vals, context)
                            trafos_off.append(data_trafo_ids[0])
                        else:
                            print ("No s'ha trobat el transformador al data")
                    else:
                        # trafo no trobat
                        print ("ATENCIÓ! impossible trobar data del bloc_trafo i és un sense corrent!")
                elif model.model == 'giscedata.transformador.trafo':
                    search_params = [("node_id", "=", node_id)]
                    data_trafo_ids = data_trafo_obj.search(cr, uid, search_params)
                    if data_trafo_ids:
                        vals['name'] = data_trafo_ids[0]
                        vals['estat'] = -1
                        at_trafos_obj.create(
                                cr, uid, vals, context)
                        trafos_off.append(data_trafo_ids[0])
                    else:
                        print ("No s'ha trobat el transformador al data")
                else:
                    # no és escomesa ni trafo
                    pass

        # ara busquem a la BD tots els clients (CONTA-* no -AT)
        # segons la taula de la traçabilitat

        if len(trafos_off) > 0:
            sql = """
                    SELECT DISTINCT(escomesa)
                    FROM giscegis_escomeses_traceability
                    WHERE trafo IN %(ids)s
                    """
            cr.execute(sql, {'ids': tuple(trafos_off)})
            for res in cr.dictfetchall():
                # no hem de posar els possibles CONTA-AT
                # que ja s'han trobat abans!!
                if res['escomesa'] not in escomeses_off:
                    at_escomeses_obj.create(
                            cr, uid, {'name': res['escomesa'],
                                      'estat': -1,
                                      'simulacio': at_resultats_id
                                      })
                    # id's escomeses per retornar
                    if res['escomesa']:
                        escomeses_off.append(res['escomesa'])

        # FIXME quan estigui provat, posar les agregacions dins els bucles que ja tenim adalt i ens estalviarem una passada per totes les escomeses
        # busquem les pòlisses de les escomeses que tenim OFF
        potencia_contractada_off = 0
        num_polisses_off = 0
        model_escomesa = self.pool.get('giscedata.cups.escomesa')
        escomeses = model_escomesa.read(
                cr, uid, escomeses_off,
                ['polisses_actives', 'potencia_polisses']
        )
        for escomesa in escomeses:
            potencia_contractada_off += escomesa['potencia_polisses']
            num_polisses_off += len(escomesa['polisses_actives'])

        return {
            'trams_on': trams_on, 'trams_off': trams_off,
            'escomeses_off': escomeses_off, 'simulacio': at_resultats_id,
            'num_polisses_off': num_polisses_off,
            'potencia_contractada_off': float("%.2f" % potencia_contractada_off)
        }

    def simulacio_geom(self, cursor, uid, netsource=None, sim_open_nodes=[],
                       context={}, load=False):
        if isinstance(netsource, (list, tuple)):
            netsource = netsource[0]
        if not netsource:
            frontera_obj = self.pool.get('giscedata.punt.frontera')
            netsource = frontera_obj.search(cursor, uid, [], limit=1)[0]
        resultat = self.simulacio_ids(
                cursor, uid, netsource, sim_open_nodes, context, load)
        res = self.get_simulation_geom(
                cursor, uid, resultat, method='postgis', srid=4326)
        return res

    def simulacio(self, cursor, uid, ct, sim_open_nodes=[],
                  context={}, load=False):
        # simulació per gis 1.x
        resultat = self.simulacio_ids(cursor, uid, ct, sim_open_nodes,
                                      context, load)
        res = self.get_simulation_geom(cursor, uid, resultat)
        return res

    def simulacio_descarrec_ids(self, cursor, uid, netsource, sol_descarrec_id,
                                sim_open_nodes=[], context={}, load=False):
        """
        Makes a simulation of descarrec, stores the descarrec and marks the
        solicitud as done

        :param cursor: Database cursor
        :param uid: User id
        :param netsource: Simulation netsource
        :param sol_descarrec_id: Id of the solicitud
        :param sim_open_nodes: Open Nodes to simulate
        :param context: OpenERP context
        :param load:
        :return: Simulation result
        """
        sol_descarrec_id = int(sol_descarrec_id)
        # marcar descarrec com a 'simulant'
        model_descarrecs = self.pool.get('giscedata.descarrecs.sollicitud')
        sol_descarrec = model_descarrecs.browse(cursor, uid, sol_descarrec_id)
        sol_descarrec.simulant(sol_descarrec_id)
        # simulem normal
        sim_open_nodes = map(int, sim_open_nodes)
        resultats = self.simulacio_ids(cursor, uid, netsource, sim_open_nodes, context, load)
        # agafem el id de la simulació
        sim_id = resultats['simulacio']
        # marquem la sol.licitud com a 'done'
        sol_descarrec.done()
        # crear descàrrec, estat 'simulat'
        descarrec_obj = self.pool.get('giscedata.descarrecs.descarrec')
        vals = {}
        sim_trafos_obj = self.pool.get('giscegis.simulacio.at.resultats.transformadors')
        sim_trafos_ids = sim_trafos_obj.search(cursor, uid, [('simulacio', '=', sim_id)])
        simulacio_obj = False
        for sim_trafo_id in sim_trafos_ids:
            resultats_obj = sim_trafos_obj.browse(cursor, uid, sim_trafo_id).simulacio
            simulacio_obj = resultats_obj.simulacio
            pass
        if simulacio_obj:
            vals['simulacio'] = simulacio_obj.id

        # valors que venen de la sol.licitud
        vals['name'] = sol_descarrec.name
        vals['sollicitud'] = sol_descarrec_id
        vals['descripcio'] = sol_descarrec.descripcio
        vals['data_inici'] = sol_descarrec.data_inici
        vals['data_final'] = sol_descarrec.data_final
        vals['observacions'] = sol_descarrec.observacions
        vals['xarxa'] = sol_descarrec.xarxa
        # guardar i obtenir id per poder crear els resultats
        descarrec_id = descarrec_obj.create(cursor, uid, vals)
        # assignar resultats del descàrrec
        installacions_obj = self.pool.get('giscedata.descarrecs.descarrec.installacions')
        clients_obj = self.pool.get('giscedata.descarrecs.descarrec.clients')
        # per fer instal.lacions: agafar trafos i conta-at
        # trafos:
        trafos_obj = self.pool.get('giscedata.transformador.trafo')
        searc_params_trf = [
            ('simulacio', '=', sim_id),
            ('estat', '=', -1)
        ]
        sim_trafos_ids = sim_trafos_obj.search(cursor, uid, searc_params_trf)
        for tid in sim_trafos_ids:
            vals_t = {}
            sim_t = sim_trafos_obj.browse(cursor, uid, tid)
            t = trafos_obj.browse(cursor, uid, sim_t.name.id)
            vals_t['name'] = t.name
            vals_t['descarrec_id'] = descarrec_id
            vals_t['type'] = 'T'
            vals_t['code_ct'] = t.ct.name
            vals_t['zone_ct_id'] = t.ct.zona_id.id
            vals_t['code_inst'] = t.id
            vals_t['power'] = t.potencia_nominal
            vals_t['codeine'] = t.ct.id_municipi.id
            vals_t['data'] = sol_descarrec.data_inici
            installacions_obj.create(cursor, uid, vals_t)

        # conta-at: (FILTRAR, que estan junts amb la resta de CONTA's!!)
        sim_contas_obj = self.pool.get('giscegis.simulacio.at.resultats.escomeses')
        contas_obj = self.pool.get('giscedata.cups.escomesa')
        cups_obj = self.pool.get('giscedata.cups.ps')
        trace_obj = self.pool.get('giscegis.escomeses.traceability')  # per la info dels CONTA
        block_obj = self.pool.get('giscedata.cups.blockname')
        sim_contas_ids = sim_contas_obj.search(
                cursor, uid, [('simulacio', '=', sim_id), ('estat', '=', -1)]
        )
        for cid in sim_contas_ids:
            vals_i = {}
            vals_c = {}
            # comprovar que sigui CONTA-AT (INSTAL·LACIONS)
            sim_conta_id = sim_contas_obj.browse(cursor, uid, cid).name.id
            conta = contas_obj.read(
                cursor, uid, sim_conta_id,
                ["id", "blockname", "potencia_polisses","name"]
            )
            trace_id = trace_obj.search(
                    cursor, uid, [('escomesa', '=', conta['id'])]
            )
            if len(trace_id) > 0:
                trace = trace_obj.browse(cursor, uid, trace_id[0])
                block_code = block_obj.read(
                    cursor, uid, conta['blockname'][0], ['code'])['code']
                if block_code == 1:
                    # guardar CONTA a INSTAL·LACIONS
                    vals_i['name'] = conta['name']
                    vals_i['descarrec_id'] = descarrec_id
                    vals_i['type'] = 'C'
                    vals_i['code_ct'] = trace.ct.name
                    vals_i['zone_ct_id'] = trace.ct.zona_id.id
                    vals_i['codeinst'] = conta['id']
                    vals_i['power'] = conta['potencia_polisses']
                    vals_i['codeine'] = trace.ct.id_municipi.id
                    vals_i['data'] = sol_descarrec.data_inici
                    installacions_obj.create(cursor, uid, vals_i)
                #else: # CLIENTS
                # Fins i tot pels CONTA-AT afegim els clients
                # per cada cups de l'escomesa amb polissa...
                cups_ids = cups_obj.search(
                    cursor, uid, [('id_escomesa', '=', conta['id'])])
                for cups in cups_obj.browse(cursor, uid, cups_ids):
                    polissa = cups._get_polissa()
                    if polissa:
                        vals_c['descarrec_id'] = descarrec_id
                        vals_c['name'] = polissa.titular.name
                        vals_c['code_ct'] = trace.ct.name
                        vals_c['zone_ct_id'] = trace.ct.zona_id.id or 0
                        vals_c['codeine'] = cups.id_municipi.id
                        vals_c['poblacio'] = cups.id_poblacio.id
                        vals_c['codecustomer'] = polissa.name
                        vals_c['line'] = cups.linia
                        vals_c['cups'] = cups.name
                        vals_c['policy'] = polissa.id
                        vals_c['contracted_power'] = polissa.potencia
                        vals_c['tension'] = polissa.tensio
                        vals_c['pricelist'] = polissa.tarifa.name
                        vals_c['escomesa'] = conta['name']
                        vals_c['escomesa_type'] = conta['blockname'][1]
                        vals_c['street'] = cups.nv
                        if cups.pnp:
                            vals_c['number'] = cups.pnp
                        else:
                            vals_c['number'] = cups.aclarador
                        vals_c['level'] = cups.pt
                        vals_c['appartment_number'] = cups.pu
                        vals_c['contador'] = polissa.comptador
                        vals_c['data_alta'] = polissa.data_alta
                        clients_obj.create(cursor, uid, vals_c)
        descarrec_obj.observacions_descarrec(cursor, uid, [descarrec_id])
        return resultats

    def simulacio_descarrec(self, cr, uid, netsource, sol_descarrec_id,
                            sim_open_nodes=[], context={}, load=False):

        sol_descarrec_id = int(sol_descarrec_id)
        # marcar descarrec com a 'simulant'
        model_descarrecs = self.pool.get('giscedata.descarrecs.sollicitud')
        sol_descarrec = model_descarrecs.browse(cr, uid, sol_descarrec_id)
        sol_descarrec.simulant(sol_descarrec_id)
        # simulem normal
        sim_open_nodes = map(int, sim_open_nodes)
        resultats = self.simulacio(cr, uid, netsource, sim_open_nodes, context, load)
        # agafem el id de la simulació
        sim_id = resultats['simulacio']
        # marquem la sol.licitud com a 'done'
        sol_descarrec.done()
        # crear descàrrec, estat 'simulat'
        descarrec_obj = self.pool.get('giscedata.descarrecs.descarrec')
        vals = {}
        sim_trafos_obj = self.pool.get('giscegis.simulacio.at.resultats.transformadors')
        sim_trafos_ids = sim_trafos_obj.search(cr, uid,
                                               [('simulacio', '=', sim_id)])
        simulacio_obj = False
        for sim_trafo_id in sim_trafos_ids:
            resultats_obj = sim_trafos_obj.browse(cr, uid, sim_trafo_id).simulacio
            simulacio_obj = resultats_obj.simulacio
        if simulacio_obj:
            vals['simulacio'] = simulacio_obj.id

        # valors que venen de la sol.licitud
        vals['name'] = sol_descarrec.name
        vals['sollicitud'] = sol_descarrec_id
        vals['descripcio'] = sol_descarrec.descripcio
        vals['data_inici'] = sol_descarrec.data_inici
        vals['data_final'] = sol_descarrec.data_final
        vals['observacions'] = sol_descarrec.observacions
        vals['xarxa'] = sol_descarrec.xarxa
        # guardar i obtenir id per poder crear els resultats
        descarrec_id = descarrec_obj.create(cr, uid, vals)
        # assignar resultats del descàrrec
        installacions_obj = self.pool.get('giscedata.descarrecs.descarrec.installacions')
        clients_obj = self.pool.get('giscedata.descarrecs.descarrec.clients')
        # per fer instal.lacions: agafar trafos i conta-at
        # trafos:
        trafos_obj = self.pool.get('giscedata.transformador.trafo')
        sim_trafos_ids = sim_trafos_obj.search(
                cr, uid, [('simulacio', '=', sim_id), ('estat', '=', -1)])
        for tid in sim_trafos_ids:
            vals_t = {}
            sim_t = sim_trafos_obj.browse(cr, uid, tid)
            t = trafos_obj.browse(cr, uid, sim_t.name.id)
            vals_t['name'] = t.name
            vals_t['descarrec_id'] = descarrec_id
            vals_t['type'] = 'T'
            vals_t['code_ct'] = t.ct.name
            vals_t['zone_ct_id'] = t.ct.zona_id.id
            vals_t['code_inst'] = t.id
            vals_t['power'] = t.potencia_nominal
            vals_t['codeine'] = t.ct.id_municipi.id
            vals_t['data'] = sol_descarrec.data_inici
            installacions_obj.create(cr, uid, vals_t)

        # conta-at: (FILTRAR, que estan junts amb la resta de CONTA's!!)
        sim_contas_obj = self.pool.get('giscegis.simulacio.at.resultats.escomeses')
        contas_obj = self.pool.get('giscedata.cups.escomesa')
        cups_obj = self.pool.get('giscedata.cups.ps')
        trace_obj = self.pool.get('giscegis.escomeses.traceability')  # per la info dels CONTA
        sim_contas_ids = sim_contas_obj.search(
                cr, uid, [('simulacio', '=', sim_id), ('estat', '=', -1)])
        for cid in sim_contas_ids:
            vals_i = {}
            vals_c = {}
            # comprovar que sigui CONTA-AT (INSTAL·LACIONS)
            conta = contas_obj.browse(
                    cr, uid, sim_contas_obj.browse(cr, uid, cid).name.id)
            trace_id = trace_obj.search(cr, uid, [('escomesa', '=', conta.id)])
            if len(trace_id) > 0:
                trace = trace_obj.browse(cr, uid, trace_id[0])
                if conta.blockname.code == 1:
                    # guardar CONTA a INSTAL·LACIONS
                    vals_i['name'] = conta.name
                    vals_i['descarrec_id'] = descarrec_id
                    vals_i['type'] = 'C'
                    vals_i['code_ct'] = trace.ct.name
                    vals_i['zone_ct_id'] = trace.ct.zona_id.id
                    vals_i['codeinst'] = conta.id
                    vals_i['power'] = conta.potencia_polisses
                    vals_i['codeine'] = trace.ct.id_municipi.id
                    vals_i['data'] = sol_descarrec.data_inici
                    installacions_obj.create(cr, uid, vals_i)
                # else: # CLIENTS
                # Fins i tot pels CONTA-AT afegim els clients
                # per cada cups de l'escomesa amb polissa...

                search_filter = [('cups.id_escomesa.id', '=', conta.id)]
                polissa_obj = self.pool.get('giscedata.polissa')
                polisses_ids = polissa_obj.search(cr, uid, search_filter)
                for polissa in polissa_obj.browse(cr, uid, polisses_ids):
                    vals_c['descarrec_id'] = descarrec_id
                    vals_c['name'] = polissa.titular.name
                    vals_c['code_ct'] = trace.ct.name
                    vals_c['zone_ct_id'] = trace.ct.zona_id.id or 0
                    vals_c['codeine'] = polissa.cups.id_municipi.id
                    vals_c['poblacio'] = polissa.cups.id_poblacio.id
                    vals_c['codecustomer'] = polissa.name
                    vals_c['line'] = polissa.cups.linia
                    vals_c['cups'] = polissa.cups.name
                    vals_c['policy'] = polissa.id
                    vals_c['contracted_power'] = polissa.potencia
                    vals_c['tension'] = polissa.tensio
                    vals_c['pricelist'] = polissa.tarifa.name
                    vals_c['escomesa'] = conta.name
                    vals_c['escomesa_type'] = conta.blockname.name
                    vals_c['street'] = polissa.cups.nv
                    if polissa.cups.pnp:
                        vals_c['number'] = polissa.cups.pnp
                    else:
                        vals_c['number'] = polissa.cups.aclarador
                    vals_c['level'] = polissa.cups.pt
                    vals_c['appartment_number'] = polissa.cups.pu
                    vals_c['contador'] = polissa.comptador
                    vals_c['data_alta'] = polissa.data_alta
                    clients_obj.create(cr, uid, vals_c)

        descarrec_obj.observacions_descarrec(cr, uid, [descarrec_id])
        return resultats

    def simulacio_qds(self, cr, uid, netsource, sim_open_nodes=[], context={},
                      load=False):
        """
        Funció per simular la QdS, és realment una simulació normal, però que retorna
        el id de simulació cap a la web junt amb els resultats
        @see simulacio
        """
        resultats = self.simulacio(
                cr, uid, netsource, sim_open_nodes, context, load)
        return resultats

    def get_all_nodes(self, cr, uid, state='all', context={}):
        """
        Funció que retorna tots els nodes de la topologia de la XAT en un estat
        determinat (all, open, closed).
        Els oberts són directament els que estan a l'estat de la xarxa i els tancats
        són tots, restant-hi els oberts (estat xarxa).
        @state: string = {all,open,closed}
        """
        list_nodes = []
        if state == 'closed' or state == 'all':
            nodes_obj = self.pool.get('giscegis.configdefault.at.nodes')
            nodes_ids = nodes_obj.search(cr, uid, [])
            for node in nodes_obj.read(cr, uid, nodes_ids, ['name']):
                list_nodes.append({'node': node['id'], 'codi': node['name']})

        if state == 'open' or state == 'all':
            _nodes_obj = self.pool.get('giscegis.configdefault.at')
            _nodes_ids = _nodes_obj.search(cr, uid, [])
            for node in _nodes_obj.read(cr, uid, _nodes_ids, ['codi']):
                list_nodes.append({'node': node['id'], 'codi': node['codi']})

        return list_nodes

    def get_nodes_descarrec(self, cr, uid, descarrec_id, state='all',
                            context={}):
        """
        Mètode per retornar els nodes referents a un descàrrec, si són tots, cridem
        directament a get_nodes, si és open o closed, mirem quins s'han obert pel
        descàrrec donat, els oberts de la xarxa i la resta i operem.
        @return [{'node' : node_id , 'codi' : codi } , ...]
        """
        # obtenir la netsource per descarrec_id
        descarrecs = self.pool.get('giscedata.descarrecs.descarrec')
        descarrec_obj = descarrecs.browse(cr, uid, descarrec_id)
        if descarrec_obj:
            # tenim descàrrec
            netsource_id = descarrec_obj.simulacio.source.id
        else:
            # sense descàrrec no farem res...
            return {}

        if state == 'all':
            return self.get_nodes(cr, uid, netsource_id, state)
        elif state == 'closed':
            # els tancats són tots els nodes per la netsource
            # menys els oberts per aquest descàrrec
            tots = self.get_nodes(cr, uid, netsource_id, 'all')
            oberts = self.get_nodes_descarrec(cr, uid, descarrec_id, 'open')
            oberts_ids = [node['node'] for node in oberts]
            tancats = []
            for t in tots:
                if t['node'] not in oberts_ids:
                    tancats.append(t)
            return tancats
        elif state == 'open':
            # buscar la configsimulació de descarrec_id
            simulacio_obj = descarrec_obj.simulacio
            # amb configsimulacio_id, seleccionar nodes
            # a configsimulacio_at_oberts
            config_model = self.pool.get('giscegis.configsimulacio.at.oberts')
            config_ids = config_model.search(
                    cr, uid, [('config', '=', simulacio_obj.id)])
            list_oberts = []
            model_configs = self.pool.get('giscegis.configsimulacio.at.oberts')
            configs = model_configs.read(cr, uid, config_ids, ["node"])
            nodes_ids = [config['node'][0] for config in configs]
            oberts = self.pool.get('giscegis.nodes').read(
                    cr, uid, nodes_ids, ['id', 'name'])
            node_obj = self.pool.get('giscegis.nodes')
            for o in oberts:
                list_oberts.append(
                        {'node': o['id'],
                         'codi': node_obj.get_codi(cr, uid, o['id']) or o['name']
                         })
            # retornar aquests nodes
            return list_oberts
        # return de recurs final
        return {}

    def get_nodes(self, cursor, uid, netsource, state='all',
                  srid=None, context={}):
        """
        Funció per retornar tots els nodes d'un subgraf d'at

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param netsource: Id del Source que volem obtenir els nodes, S'IGNORA !!
        :type netsource: int
        :param state: {all, open, closed} Estat en que volem els interruptors
        :type state: str
        :param srid:
        :param context: OpenERP context
        :return:
        """

        list_nodes = []
        is_postgis = is_postgis_db(cursor)
        conf_obj = self.pool.get('res.config')
        srid_db = int(conf_obj.get(cursor, uid, 'giscegis_srid', '25830'))

        if not srid:
            srid = srid_db

        model_nodes = self.pool.get('giscegis.nodes')
        if state == 'closed' or state == 'all':
            nodes_obj = self.pool.get('giscegis.configdefault.at.nodes')
            nodes_ids = nodes_obj.search(cursor, uid, [])
            nodes = nodes_obj.read(cursor, uid, nodes_ids)
            ids = [d['id'] for d in nodes]

            if is_postgis:
                vertexs = model_nodes.get_coords_multi(
                        cursor, uid, ids, srid=srid, srid_db=srid_db)
                for n in nodes:
                    if vertexs[n['id']]:
                        vertex = {'x': vertexs[n['id']][0], 'y': vertexs[n['id']][1]}

                    else:
                        vertex = {'x': 0, 'y': 0}
                    list_nodes.append(
                            {'node': n['id'],
                             'codi': n['name'],
                             'x': vertex['x'],
                             'y': vertex['y']})
            else:
                for n in nodes:
                    vertex = {'x': 0, 'y': 0}
                    list_nodes.append(
                            {'node': n['id'],
                             'codi': n['name'],
                             'x': vertex['x'],
                             'y': vertex['y']})
        if state == 'open' or state == 'all':
            nodes_obj = self.pool.get('giscegis.configdefault.at')
            nodes_ids = nodes_obj.search(cursor, uid, [])

            nodes = nodes_obj.read(cursor, uid, nodes_ids)
            ids = [d['node'][0] for d in nodes]
            if is_postgis:
                vertexs = model_nodes.get_coords_multi(
                        cursor, uid, ids, srid=srid, srid_db=srid_db)
            for n in nodes:
                if is_postgis:
                    if vertexs[n['node'][0]]:
                        vertex = {
                            'x': vertexs[n['node'][0]][0],
                            'y': vertexs[n['node'][0]][1]
                        }
                    else:
                        vertex = {'x': 0, 'y': 0}
                else:
                    vertex = {'x': 0, 'y': 0}
                list_nodes.append(
                    {'node': n['node'][0],
                     'codi': n['codi'],
                     'x': vertex['x'],
                     'y': vertex['y']})
        return list_nodes

    # TODO REPASSAR !!!
    def get_abonats_off(self, cr, uid, simulation_results_id):
        """
        Aquest mètode cerca els abonats que s'han quedat sense servei en una simulació donada.

        :param simulation_results_id: id dels resultats de simulació bt
        """
        ggsbre_obj = self.pool.get('giscegis.simulacio.at.resultats.escomeses')
        ggbe_obj = self.pool.get('giscegis.blocs.escomeses')
        ggsbre_ids = ggsbre_obj.search(
                cr, uid,
                [('simulacio', '=', simulation_results_id),
                 ('estat', '=', '-1')]
        )
        escomeses_off = ggsbre_obj.browse(cr, uid, ggsbre_ids)

        ESTATS_POLISSA = ['esborrany', 'pendent', 'tall', 'baixa']
        cups_obj = self.pool.get('giscedata.cups.ps')
        polissa_obj = self.pool.get('giscedata.polissa')
        gpolissa_obj = self.pool.get('giscegis.polisses.171')

        res = []
        escomeses_ids = [a.name.id for a in escomeses_off]
        cups_ids = cups_obj.search(
                cr, uid, [('id_escomesa', 'in', escomeses_ids)])
        model_escomesa = self.pool.get('giscedata.cups.escomesa')
        for cups in cups_obj.read(cr, uid, cups_ids, ['nv', 'es', 'pnp', 'pt', 'pu', 'id_escomesa']):
            # obtenir pòlissa activa associada, si en té
            search_params = [('cups', '=', cups['id']),
                             ('state', 'not in', ESTATS_POLISSA)]
            polissa_id = polissa_obj.search(cr, uid, search_params)
            if polissa_id:
                polissa = polissa_obj.browse(cr, uid, polissa_id)[0]
                pol = {}
                pol['carrer'] = cups['nv']
                pol['escala'] = cups['es']
                comptadors = polissa.comptadors_actius(polissa.data_alta)
                if comptadors:
                    compt_obj = self.pool.get('giscedata.lectures.comptador')
                    pol['n_comptador'] = compt_obj.read(cr, uid,
                                                        [comptadors[0]],
                                                        ['name'])[0]['name']
                else:
                    pol['n_comptador'] = '0000000'
                pol['escomesa'] = model_escomesa.read(
                        cr, uid, cups['id_escomesa'][0], ['name'])['name']
                pol['tensio'] = polissa.tensio
                pol['titular'] = polissa.titular.name
                pol['et'] = polissa.et
                pol['linia'] = polissa.linia
                pol['polissa'] = polissa.name
                pol['numero'] = cups['pnp']
                pol['potencia'] = polissa.potencia
                pol['planta'] = cups['pt']
                pol['pis'] = cups['pu']
                gpolissa_id = gpolissa_obj.search(
                        cr, uid,
                        [('name', '=', polissa.name)]
                )
                if gpolissa_id:
                    gpolissa = gpolissa_obj.read(cr, uid, gpolissa_id[0])

                    if gpolissa['x'] != 0 and gpolissa['y'] != 0:
                        pol['x'] = gpolissa['x']
                        pol['y'] = gpolissa['y']
                        pol['posicionar'] = "<a href='#' onClick='javascript:zoom2()'>Zoom</a>"
                pol['detall'] = "<a href='#' onClick='javascript:detall2()'>Fitxa</a>"

                res.append(pol)
        return res


GiscegisSimulacioAt()


class giscegis_simulacio_at_resulats(osv.osv):

    """
    Classe per guardar els resultats d'una simulacio
    """

    _name = 'giscegis.simulacio.at.resultats'

    _columns = {
        'name': fields.char('Name', size=128),
        'escomeses': fields.one2many(
                'giscegis.simulacio.at.resultats.escomeses',
                'simulacio',
                'Escomeses'),
        'transformadors': fields.one2many(
                'giscegis.simulacio.at.resultats.transformadors',
                'simulacio',
                'Transformadors'),
        'simulacio': fields.many2one(
                'giscegis.configsimulacio.at',
                'Config Simulació'),
    }


giscegis_simulacio_at_resulats()


class giscegis_simulacio_at_resultats_escomeses(osv.osv):

    """
    Classe per guardar escomeses sense corrent relacionades
    amb una simulacio
    """

    _name = 'giscegis.simulacio.at.resultats.escomeses'

    _columns = {
        'name': fields.many2one('giscedata.cups.escomesa', 'Escomesa'),
        'simulacio': fields.many2one('giscegis.simulacio.at.resultats'),
        'estat': fields.selection([(-1, 'Off'), (1, 'On')], 'Estat')
    }

    _sql_constraints = [
        ('name_simulacio_uniq',
         'unique (name, simulacio)',
         'No es pot repetir l\'escomesa més d\'un cop a la mateixa simulació.')]


giscegis_simulacio_at_resultats_escomeses()


class giscegis_simulacio_at_resultats_transformadors(osv.osv):

    """
    Classe per guardar transformadors sense corrent relacionats
    amb una simulacio
    """

    _name = 'giscegis.simulacio.at.resultats.transformadors'

    _columns = {
        'name': fields.many2one('giscedata.transformador.trafo',
                                'Transformador'),
        'simulacio': fields.many2one('giscegis.simulacio.at.resultats'),
        'estat': fields.selection([(-1, 'Off'), (1, 'On')], 'Estat')
    }

    _sql_constraints = [
        ('name_simulacio_uniq',
         'unique (name, simulacio)',
         'No es pot repetir el transformador més d\'un cop a la mateixa simulació.')]


giscegis_simulacio_at_resultats_transformadors()
