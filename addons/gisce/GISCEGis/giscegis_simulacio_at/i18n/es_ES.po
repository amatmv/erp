# Translation of OpenERP Server.
# This file contains the translation of the following modules:
# 
# Translators:
#   <misern@gisce.net>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: GISCE-ERP\n"
"Report-Msgid-Bugs-To: https://github.com/gisce/erp/issues\n"
"POT-Creation-Date: 2019-08-12 14:40\n"
"PO-Revision-Date: 2012-04-16 10:04+0000\n"
"Last-Translator: misern <misern@gisce.net>\n"
"Language-Team: Spanish (Spain) <erp@dev.gisce.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: es_ES\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. module: giscegis_simulacio_at
#: selection:giscegis.simulacio.at.resultats.escomeses,estat:0
#: selection:giscegis.simulacio.at.resultats.transformadors,estat:0
msgid "On"
msgstr "On"

#. module: giscegis_simulacio_at
#: selection:giscegis.simulacio.at.resultats.escomeses,estat:0
#: selection:giscegis.simulacio.at.resultats.transformadors,estat:0
msgid "Off"
msgstr "Off"

#. module: giscegis_simulacio_at
#: field:giscegis.simulacio.at.resultats,name:0
msgid "Name"
msgstr "Name"

#. module: giscegis_simulacio_at
#: model:ir.model,name:giscegis_simulacio_at.model_giscegis_simulacio_at_resultats
msgid "giscegis.simulacio.at.resultats"
msgstr "giscegis.simulacio.at.resultats"

#. module: giscegis_simulacio_at
#: field:giscegis.simulacio.at.resultats.escomeses,simulacio:0
#: field:giscegis.simulacio.at.resultats.transformadors,simulacio:0
msgid "unknown"
msgstr "unknown"

#. module: giscegis_simulacio_at
#: field:giscegis.simulacio.at.resultats,escomeses:0
msgid "Escomeses"
msgstr "Acometidas"

#. module: giscegis_simulacio_at
#: field:giscegis.simulacio.at.resultats.transformadors,name:0
msgid "Transformador"
msgstr "Transformador"

#. module: giscegis_simulacio_at
#: model:ir.model,name:giscegis_simulacio_at.model_giscegis_simulacio_at
msgid "giscegis.simulacio.at"
msgstr "giscegis.simulacio.at"

#. module: giscegis_simulacio_at
#: model:ir.module.module,description:giscegis_simulacio_at.module_meta_information
msgid "Simulacions AT"
msgstr "Simulaciones AT"

#. module: giscegis_simulacio_at
#: model:ir.model,name:giscegis_simulacio_at.model_giscegis_simulacio_at_resultats_transformadors
msgid "giscegis.simulacio.at.resultats.transformadors"
msgstr "giscegis.simulacio.at.resultats.transformadors"

#. module: giscegis_simulacio_at
#: field:giscegis.simulacio.at.resultats.escomeses,estat:0
#: field:giscegis.simulacio.at.resultats.transformadors,estat:0
msgid "Estat"
msgstr "Estado"

#. module: giscegis_simulacio_at
#: constraint:ir.model:0
msgid ""
"The Object name must start with x_ and not contain any special character !"
msgstr "The Object name must start with x_ and not contain any special character !"

#. module: giscegis_simulacio_at
#: field:giscegis.simulacio.at.resultats,simulacio:0
msgid "Config Simulació"
msgstr "Config Simulación"

#. module: giscegis_simulacio_at
#: field:giscegis.simulacio.at.resultats.escomeses,name:0
msgid "Escomesa"
msgstr "Acometida"

#. module: giscegis_simulacio_at
#: field:giscegis.simulacio.at.resultats,transformadors:0
msgid "Transformadors"
msgstr "Transformadores"

#. module: giscegis_simulacio_at
#: model:ir.model,name:giscegis_simulacio_at.model_giscegis_simulacio_at_resultats_escomeses
msgid "giscegis.simulacio.at.resultats.escomeses"
msgstr "giscegis.simulacio.at.resultats.escomeses"

#. module: giscegis_simulacio_at
#: model:ir.module.module,shortdesc:giscegis_simulacio_at.module_meta_information
msgid "GISCE GIS Simulació AT"
msgstr "GISCE GIS Simulación AT"
