# -*- coding: iso-8859-1 -*-
from osv import osv,fields

class giscegis_elements_tallsat(osv.osv):
    _name = "giscegis.elements.tallsat"
    _description = "tots els tallsat"
    _auto = False
    _columns = {
      'name': fields.char('BlockName', size=50, required=True),
      'vertex': fields.many2one('giscegis.vertex', 'Vertex',
                                ondelete='set null'),
      'width': fields.float('Width'),
      'height': fields.float('Height'),
      'rotation': fields.float('Rotation'),
      'codi': fields.char('Codi', size=50),
    }

    _defaults = {

    }

    _order = "name"

    def init(self,cr):
        cr.execute("""drop view if exists giscegis_elements_tallsat""")
        cr.execute("""
          create or replace view giscegis_elements_tallsat as (
            select * from giscegis_blocs_seccionadorat seccionador
          )
        """)
giscegis_elements_tallsat()
