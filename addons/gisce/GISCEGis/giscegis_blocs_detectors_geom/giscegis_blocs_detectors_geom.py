# *-* coding: utf-8 *-*
from osv import osv, fields


class GiscegisBlocsDetectorGeom(osv.osv):
    """
    Model of Blocs detector for Giscegis v3
    """
    _name = 'giscegis.blocs.detector.geom'
    _auto = False
    _description = 'Blocs Detector per Giscegis v3'

    def init(self, cursor):
        """
        Initializes the module creating the materialized views

        :param cursor: Database cursor
        :return: None
        :rtype: None
        """

        cursor.execute("""
        DROP MATERIALIZED VIEW IF EXISTS giscegis_blocs_detector_geom;
        """)
        cursor.execute("""
        CREATE MATERIALIZED VIEW public.giscegis_blocs_detector_geom AS
            SELECT
                detect.id,
                block.name AS symbol,
                detect.rotation AS rotation,
                ST_SETSRID(ST_MAKEPOINT(vertex.x, vertex.y),
                (SELECT value::int
                    FROM res_config
                    WHERE name='giscegis_srid')) AS geom,
                detect.detector AS detector,
                vertex.x AS x,
                vertex.y AS y,
                now() as materialize_date
        FROM giscegis_blocs_detectors AS detect
            LEFT JOIN giscegis_vertex AS vertex ON vertex.id = detect.vertex
            LEFT JOIN giscegis_blocs_detectors_blockname block ON block.id = detect.blockname;
        """)

    def recreate(self, cursor, uid):
        cursor.execute("""
        REFRESH MATERIALIZED VIEW giscegis_blocs_detector_geom;
        """)
        return True

    def get_srid(self, cursor, uid):
        cursor.execute("""
        SELECT ST_SRID(geom) as srid FROM giscegis_blocs_detector_geom LIMIT 1;
        """)
        data = cursor.fetchall()
        if len(data) > 0:
            return data[0][0]
        else:
            return False

    _columns = {
        'id': fields.integer('Id', readonly=True),
        'name': fields.char('Name', readonly=True, size=256),
        'geom': fields.char('Geom', readonly=True, size=256),
        "detector": fields.integer('Detector', readonly=True),
        'x': fields.float('X'),
        'y': fields.float('Y'),
        "materialize_date": fields.datetime("Fecha creación vista"),
    }


GiscegisBlocsDetectorGeom()
