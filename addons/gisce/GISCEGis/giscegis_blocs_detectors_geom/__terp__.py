# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS v3 Blocs Detectors ",
    "description": """Modelo de bloques detectores para Giscegis v3""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_base_geom",
        "giscegis_vertex",
        "giscegis_blocs_detectors",
        "giscegis_at_detectors"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
