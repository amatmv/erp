from osv import osv
from tools import config
from giscegis_base_geom.wizard.giscegis_shp_loader \
    import GIS_MODELS_TO_UPDATE_GEOM_DATA


# Subscribe this model to the list to be updated if there is an AutoCAD dump
GIS_MODELS_TO_UPDATE_GEOM_DATA.append(
    {
        'model': 'giscedata.cts.subestacions.posicio',
        'functions': ['fill_geom', 'fill_rotation', 'fill_node_id']
    },
)


class GiscedataCtsSubestacioPosicio(osv.osv):
    _name = 'giscedata.cts.subestacions.posicio'
    _inherit = 'giscedata.cts.subestacions.posicio'

    def fill_geom_int_at(self, cursor, uid):
        """
        Function that runs a query to update the field geom of all the
        Posicio elements.
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """

        sql = """
            UPDATE giscedata_cts_subestacions_posicio AS p
            SET geom = bloc.geom
            FROM (
              SELECT codi,
                     st_snaptogrid(
                         st_translate(
                             st_setsrid(
                                 st_makepoint(vertex.x, vertex.y), 
                                 %(srid)s
                             ), 
                             0.0000000001, 0.0000000001
                         ), 
                         0.0001
                     ) AS geom
              FROM giscegis_blocs_interruptorat int_at
              LEFT JOIN giscegis_vertex vertex ON int_at.vertex = vertex.id
            ) AS bloc
            WHERE p.name = bloc.codi
        """

        cursor.execute(sql, {"srid": config.get("srid", 25830)})

    def fill_rotation_int_at(self, cursor, uid):
        """
        Function that runs a query to update the field rotation of all the
        Posicio elements.
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """

        sql = """
            UPDATE giscedata_cts_subestacions_posicio AS p
            SET rotation = (360-bloc.rotation)
            FROM (
              SELECT codi,
                     rotation
              FROM giscegis_blocs_interruptorat
            ) AS bloc
            WHERE p.name = bloc.codi
        """

        cursor.execute(sql)

    def fill_node_id_int_at(self, cursor, uid):
        """
        Function that runs a query to update the field node_id of all the
        Posicio elements.
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """

        sql = """
            UPDATE giscedata_cts_subestacions_posicio AS p
            SET node_id = bloc.node
            FROM (
              SELECT codi,
                     node
              FROM giscegis_blocs_interruptorat
            ) AS bloc
            WHERE p.name = bloc.codi
        """

        cursor.execute(sql)

    def fill_geom(self, cursor, uid):
        """
        Function that fills the field geom of all the SE Pos
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """
        self.fill_geom_int_at(cursor, uid)
        self.fill_geom_fus_at(cursor, uid)
        self.fill_geom_sec_at(cursor, uid)
        self.fill_geom_sec_uni(cursor, uid)
        sql = """
            UPDATE giscedata_cts_subestacions_posicio SET geom = St_SnapToGrid(
                ST_Translate(geom, 0.0000000001, 0.0000000001), 0.0001
            );
        """
        cursor.execute(sql)

    def fill_rotation(self, cursor, uid):
        """
        Function that fills the field rotation of all the SE Pos
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """
        self.fill_rotation_int_at(cursor, uid)
        self.fill_rotation_fus_at(cursor, uid)
        self.fill_rotation_sec_at(cursor, uid)
        self.fill_rotation_sec_uni(cursor, uid)

    def fill_node_id(self, cursor, uid):
        """
        Function that fills the field node_id of all the SE Pos
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """
        self.fill_node_id_int_at(cursor, uid)
        self.fill_node_id_fus_at(cursor, uid)
        self.fill_node_id_sec_at(cursor, uid)
        self.fill_node_id_sec_uni(cursor, uid)


GiscedataCtsSubestacioPosicio()
