from osv import osv
from tools import config
from giscegis_base_geom.wizard.giscegis_shp_loader \
    import GIS_MODELS_TO_UPDATE_GEOM_DATA


# Subscribe this model to the list to be updated if there is an AutoCAD dump
GIS_MODELS_TO_UPDATE_GEOM_DATA.append(
    {
        'model': 'giscedata.celles.cella',
        'functions': ['fill_geom', 'fill_rotation', 'fill_node_id']
    },
)


class GiscedataCellesCella(osv.osv):

    _name = 'giscedata.celles.cella'
    _inherit = 'giscedata.celles.cella'

    def fill_geom_int_at(self, cursor, uid):
        """
        Function that runs a query to update the field geom of all the
        InterruptorAT elements.
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """

        sql = """
            UPDATE giscedata_celles_cella AS c
            SET geom = bloc.geom
            FROM (
              SELECT codi,
                     st_setsrid(
                       st_makepoint(vertex.x, vertex.y), %(srid)s
                     ) AS geom
              FROM giscegis_blocs_interruptorat int_at
              LEFT JOIN giscegis_vertex vertex ON int_at.vertex = vertex.id
            ) AS bloc
            WHERE c.name = bloc.codi
        """

        cursor.execute(sql, {"srid": config.get("srid", 25830)})

    def fill_rotation_int_at(self, cursor, uid):
        """
        Function that runs a query to update the field rotation of all the
        InterruptorAT elements.
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """

        sql = """
            UPDATE giscedata_celles_cella AS c
            SET rotation = bloc.rotation
            FROM (
              SELECT codi,
                     rotation
              FROM giscegis_blocs_interruptorat
            ) AS bloc
            WHERE c.name = bloc.codi
        """

        cursor.execute(sql)

    def fill_node_id_int_at(self, cursor, uid):
        """
        Function that runs a query to update the field node_id of all the
        InterruptorAT elements.
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """

        sql = """
            UPDATE giscedata_celles_cella AS c
            SET node_id = bloc.node
            FROM (
              SELECT codi,
                     node
              FROM giscegis_blocs_interruptorat
            ) AS bloc
            WHERE c.name = bloc.codi
        """

        cursor.execute(sql)

    def fill_geom(self, cursor, uid):
        """
        Function that fills the field geom of all the elements tall
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """
        self.fill_geom_int_at(cursor, uid)
        self.fill_geom_fus_at(cursor, uid)
        self.fill_geom_sec_at(cursor, uid)
        self.fill_geom_sec_uni(cursor, uid)
        sql = """
            UPDATE giscedata_celles_cella SET geom = St_SnapToGrid(
                ST_Translate(geom, 0.0000000001, 0.0000000001), 0.0001
            );
        """
        cursor.execute(sql)

    def fill_rotation(self, cursor, uid):
        """
        Function that fills the field rotation of all the elements tall
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """
        self.fill_rotation_int_at(cursor, uid)
        self.fill_rotation_fus_at(cursor, uid)
        self.fill_rotation_sec_at(cursor, uid)
        self.fill_rotation_sec_uni(cursor, uid)

    def fill_node_id(self, cursor, uid):
        """
        Function that fills the field node_id of all the elements tall
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """
        self.fill_node_id_int_at(cursor, uid)
        self.fill_node_id_fus_at(cursor, uid)
        self.fill_node_id_sec_at(cursor, uid)
        self.fill_node_id_sec_uni(cursor, uid)


GiscedataCellesCella()
