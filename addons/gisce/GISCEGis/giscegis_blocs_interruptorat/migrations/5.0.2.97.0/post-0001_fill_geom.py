# coding=utf-8
import logging
import pooler


def migrate(cursor, installed_version):

    loger = logging.Logger("openerp.migration")
    loger.info(
        "Filling field 'geom' from model GiscedataCtsSubestacionsPosicio"
    )

    pool = pooler.get_pool(cursor.dbname)
    pos_obj = pool.get('giscedata.cts.subestacions.posicio')
    pos_obj.fill_geom(cursor, 1)

    loger.info(
        "Field 'geom' from model GiscedataCtsSubestacionsPosicio filled "
        "succesfully"
    )


up = migrate
