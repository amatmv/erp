# coding=utf-8
import logging
import pooler


def migrate(cursor, installed_version):

    loger = logging.Logger("openerp.migration")
    loger.info("Omplint camp geom dels elements de tall(Interruptors AT).")

    pool = pooler.get_pool(cursor.dbname)
    elem_tall_obj = pool.get('giscedata.celles.cella')
    elem_tall_obj.fill_geom_int_at(cursor, 0)

    loger.info("Camp geom dels elements de tall(Interruptors AT) omplert.")


up = migrate
