# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Seccionadors AT vista 1.7.1",
    "description": """Vista dels seccionadors AT per la versió MG-1.7.1""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_vertex",
        "giscegis_blocs_seccionadorat",
        "giscegis_base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "ir.model.access.csv",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
