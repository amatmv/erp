# -*- coding: utf-8 -*-
{
    "name": "GISCE  GIS v3 (cartografia)",
    "description": """Mòdul de cartografia propia de GISCE GIS v3""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "base_extended",
        "giscegis_base_geom"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/giscegis_cartografia_loader.xml",
        "security/ir.model.access.csv",
        "giscegis_layers_cartografia_data.xml"
    ],
    "active": False,
    "installable": True
}
