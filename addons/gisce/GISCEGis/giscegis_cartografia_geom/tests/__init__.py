# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from addons import get_module_resource
import base64


class CartografiaTest(testing.OOTestCase):
    """"""

    def test_wizard_load_lat_shp(self):
        """
        Tests the shp load

        :return: None
        """

        self.openerp.install_module('giscegis_lat_geom')
        self.openerp.install_module('giscegis_lbt_geom')

        with Transaction().start(self.database) as txn:

            cursor = txn.cursor
            uid = txn.user

            usr_model = self.openerp.pool.get('res.users')

            usr = usr_model.browse(cursor, uid, uid)
            usr.address_id.write({"email": "tes@gisce.net"})
            
            model = self.openerp.pool.get('giscegis.cartografia.loader')
            res_obj = self.openerp.pool.get('res.config')
            res_data = {
                'name': 'giscegis_gis_url',
                'value': '0'
            }
            res_obj.create(cursor, uid, res_data)
            lat_url = get_module_resource(
                'giscegis_cartografia_geom', 'tests', 'fixtures', 'cartografia.zip')
            with open(lat_url) as fd_lat:
                lat_data = fd_lat.read()

            wizard_id = model.create(cursor, uid, {})
            wizard_load = model.browse(cursor, uid, wizard_id)
            model_data = {'file_cartografia': base64.encodestring(lat_data)}
            model.write(cursor, uid, [wizard_id], model_data)
            wizard_load.import_data_shp()
            obj_lat = self.openerp.pool.get('giscegis.cartografia.geom')
            ids_lat = obj_lat.search(cursor, uid, [])
            self.assertEqual(len(ids_lat), 2)
