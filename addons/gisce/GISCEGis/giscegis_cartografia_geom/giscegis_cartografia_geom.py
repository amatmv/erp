# *-* coding: utf-8 *-*

from osv import osv, fields


class GiscegisCartografiaGeom(osv.osv):
    _name = 'giscegis.cartografia.geom'
    _columns = {
        'gid': fields.integer('Gid'),
        'id': fields.integer('Id', required=True),
        'color': fields.integer('Color'),
        'style': fields.text('Style'),
        'textstring': fields.text('Text'),
        'text_size': fields.float('Text Size'),
        'text_angle': fields.float('Text Angle'),
    }

    def init(self, cursor):
        conf_obj = self.pool.get('res.config')
        srid = int(conf_obj.get(cursor, 1, 'giscegis_srid', '25831'))
        cursor.execute('''
            SELECT * FROM information_schema.columns
            WHERE table_name = 'giscegis_cartografia_geom'
            AND COLUMN_NAME = 'geom'
            ''')
        if not cursor.fetchone():
            sql_query = """SELECT AddGeometryColumn(
            'public','giscegis_cartografia_geom',
            'geom',%(srid)s,'geometry',2, true);"""
            cursor.execute(sql_query, {'srid': srid})
        sql_index = """
        DROP INDEX IF EXISTS indx_carto_propia; 
        CREATE INDEX indx_carto_propia ON giscegis_cartografia_geom USING gist(GEOM);
        """
        cursor.execute(sql_index)
        return True

GiscegisCartografiaGeom()
