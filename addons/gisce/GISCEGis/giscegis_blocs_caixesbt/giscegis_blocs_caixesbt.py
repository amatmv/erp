# coding=utf-8
from osv import osv, fields
from giscegis_base_geom.giscegis_base_geom import DROP_ON_AUTOCAD_DUMP

# Subscribe this model to the list to be updated if there is an AutoCAD dump
DROP_ON_AUTOCAD_DUMP.append('giscegis_blocs_caixesbt')


class giscegis_blocs_caixesbt_blockname(osv.osv):

    _name = 'giscegis.blocs.caixesbt.blockname'
    _description = 'Tipus Blocs CaixesBT'

    def create(self, cr, uid, vals, context={}):
        ids = self.search(cr, uid, [('name', '=', vals['name'])])
        if ids and len(ids):
            return ids[0]
        else:
            return super(osv.osv, self).create(cr, uid, vals)

    _columns = {
      'name': fields.char('BlockName',size=20,required=True),
      'description': fields.char('Descripció',size=255),
    }

    _defaults = {

    }

    _order = "name, id"

giscegis_blocs_caixesbt_blockname()

class giscegis_blocs_caixesbt(osv.osv):

    _name = 'giscegis.blocs.caixesbt'
    _description = 'Blocs Caixesbt'
    _columns = {
      'name': fields.char('BlockName', size=50, required=True),
      'vertex': fields.many2one('giscegis.vertex', 'Vertex',
                                ondelete='set null'),
      'width': fields.float('Width'),
      'height': fields.float('Height'),
      'rotation': fields.float('Rotation'),
      'codi': fields.char('Codi', size=50),
      'blockname': fields.many2one('giscegis.blocs.caixesbt.blockname', 
                                   'BlockName'),
      'intensitat_fusible': fields.float('Intensitat Fusible'),
      'intensitat_caixa': fields.float('Intensitat Caixa'),
      'node': fields.many2one('giscegis.nodes', 'Node', ondelete='set null'),
    }

    _defaults = {

    }

    _order = "name, id"

    def create(self, cursor, uid, vals, context=None):
       """Mètode sobreescrit per escriure als camps many2one des
       d'aquí.

       Ens hem d'assegurar de què els mètodes create() dels objectes
       referenciats estiguin alhora sobreescrits per tal què comprovin
       si l'objecte existeix o no."""
       _fields = [('vertex', 'giscegis.vertex'),
                  ('blockname', 'giscegis.blocs.caixesbt.blockname')]
                  
       for field in _fields:
           if field[0] in vals and isinstance(vals[field[0]], list):
               obj = self.pool.get(field[1])
               remote_id = obj.create(cursor, uid, vals[field[0]][0][2])
               if remote_id:
                   vals[field[0]] = remote_id
       return super(osv.osv, self).create(cursor, uid, vals)

    def write(self, cursor, uid, ids, vals, context=None):
        """Mètode sobreescrit per escriure als camps many2one des d'aquí.

        Igual que amb el create() cal assabentar-se de què els write()s dels
        models referenciats estiguin sobreescrits 
        """
        _fields = [('vertex', 'giscegis.vertex'),
                   ('blockname', 'giscegis.blocs.caixesbt.blockname')]
        for field in _fields:
            if field[0] in vals and isinstance(vals[fields[0]], list):
                obj = self.pool.get(field[1])
                search_params = []
                for k, v in field[0][0][2]:
                    search_params.append((k, '=', v))
                remote_id = obj.search(cursor, uid, search_params)
                if remote_id:
                    vals[field] = remote_id[0]
                else:
                    remote_id = obj.create(cursor, uid, field[0][0][2])
                    vals[field] = remote_id

        return super(osv.osv, self).write(cursor, uid, ids, vals,
                                          context={'sync': False})

giscegis_blocs_caixesbt()
