import netsvc
from oopgrade.oopgrade import column_exists


def migrate(cr, installed_version):
    """
    Migrates manually set variable giscegis_url as done from xml data

    :param cr: Database cursor
    :param installed_version: Installed version of the module
    :return: None
    """

    # Check if old and new column exists
    old_exists = column_exists(cr, 'giscegis_blocs_caixesbt', 'intesitat_fusible')
    new_exists = column_exists(cr, 'giscegis_blocs_caixesbt', 'intensitat_fusible')
    if old_exists and new_exists:
        # Fill intensitat_fusible if field is empty
        sql_update = """
        UPDATE giscegis_blocs_caixesbt
        SET intensitat_fusible= intesitat_fusible
        WHERE intensitat_fusible IS NULL;
        """
        cr.execute(sql_update)

up = migrate
