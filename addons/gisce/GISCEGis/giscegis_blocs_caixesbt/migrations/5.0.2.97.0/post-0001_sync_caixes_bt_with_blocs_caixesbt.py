# coding=utf-8

import logging
import pooler


def up(cursor, installed_version):

    logger = logging.getLogger('openerp.migration')

    logger.info(
        'Sync BtCaixa with BlocsCaixesbt'
    )

    pool = pooler.get_pool(cursor.dbname)
    bt_caixa = pool.get('giscedata.bt.caixa')
    n_created_elements = bt_caixa.sync_elements_from_blocs_caixesbt(cursor, 1)

    if n_created_elements == 1:
        msg = "1 BtCaixa sync correctly with BlocsCaixesbt"
    else:
        msg = "{} BtCaixa sync correctly with BlocsCaixesbt".format(
            n_created_elements
        )

    logger.info(msg)


def down(cursor, installed_version):
    pass


migrate = up
