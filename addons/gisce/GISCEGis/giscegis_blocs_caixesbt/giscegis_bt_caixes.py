# -*- coding: utf-8 -*-
from osv import osv
from tools import config
import logging
from giscegis_base_geom.wizard.giscegis_shp_loader \
    import GIS_MODELS_TO_UPDATE_GEOM_DATA


# Subscribe this model to the list to be updated if there is an AutoCAD dump
GIS_MODELS_TO_UPDATE_GEOM_DATA.append(
    {
        'model': 'giscedata.bt.caixa',
        'functions': ['sync_elements_from_blocs_caixesbt']
    },
)


class GiscedataBtCaixa(osv.osv):

    _name = 'giscedata.bt.caixa'
    _inherit = 'giscedata.bt.caixa'

    def sync_elements_from_blocs_caixesbt(self, cursor, uid):
        """
        Syncronize the BtCaixa elements with BlocsCaixesBt elements
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return: Return the number of elements syncronized.
        :rtype: int
        """

        bt_caixa = self.pool.get('giscedata.bt.caixa')
        bt_caixa_tipus = self.pool.get(
            'giscedata.bt.caixa.tipus'
        )
        logger = logging.getLogger(
            'openerp.giscedata.bt.caixa.sync_elements_from_blocs_caixesbt'
        )

        sql = """
            SELECT b.rotation, b.node, b.intensitat_fusible AS intensitat,
                b.name, bn.name AS tipus, b.codi, 
                st_astext(st_setsrid(st_makepoint(v.x, v.y), %(srid)s)) AS geom
            FROM giscegis_blocs_caixesbt b 
            LEFT JOIN  giscegis_blocs_caixesbt_blockname bn 
                ON bn.id = b.blockname
            LEFT JOIN giscegis_vertex v ON b.vertex = v.id
        """

        cursor.execute(sql, {"srid": config.get("srid", 25830)})

        rows = cursor.dictfetchall()
        row_n = len(rows)

        for row in rows:

            vals = {
                'rotation': 360-row['rotation'],
                'codi': row['codi'],
                'autocad_code': row['name'],
                'geom': row['geom'],
                'intensitat': row['intensitat'],
                'node_id': row['node']
            }

            if 'DSP' in row['tipus'] and len(row['tipus']) > 3:
                vals['observacions'] = "Old code: "+row['tipus']
                row['tipus'] = 'DSP'
            elif 'ADU' in row['tipus'] and len(row['tipus']) > 3:
                vals['observacions'] = "Old code: "+row['tipus']
                row['tipus'] = 'ADU'
            elif 'CGP' in row['tipus'] and len(row['tipus']) > 3:
                vals['observacions'] = "Old code: "+row['tipus']
                row['tipus'] = 'CGP'
            elif 'CD' in row['tipus'] and len(row['tipus']) > 2:
                vals['observacions'] = "Old code: "+row['tipus']
                row['tipus'] = 'CD'

            bt_caixa_elem_id = bt_caixa_tipus.search(
                cursor, uid, [('code', '=', row['tipus'])]
            )

            if bt_caixa_elem_id:
                vals['tipus'] = bt_caixa_elem_id[0]
            else:
                logger.warning(
                    "Type of GiscedataBtCaixa '{}' not found. Skiping the "
                    "creation of the element with code {}".format(
                        row['tipus'], row['codi']
                    )
                )
                row_n -= 1
                continue

            bt_caixa_ids = bt_caixa.search(
                cursor, uid, [('codi', '=', row['codi'])]
            )

            if bt_caixa_ids:
                vals.pop('observacions', False)
                bt_caixa.write(
                    cursor, uid, bt_caixa_ids[0], vals
                )
            else:
                vals['name'] = bt_caixa.default_get(
                    cursor, uid, ['name']
                )['name']
                bt_caixa.create(cursor, uid, vals)

        return row_n


GiscedataBtCaixa()
