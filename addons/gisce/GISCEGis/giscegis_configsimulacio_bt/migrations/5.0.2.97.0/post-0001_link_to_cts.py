# coding=utf-8
import logging

logger = logging.getLogger('openerp.' + __name__)


def up(cursor, installed_version):

    if not installed_version:
        return

    cursor.execute(
        "UPDATE giscegis_configsimulacio_bt set ct = upd.ct FROM ("
        "   SELECT c.id, b.ct"
        "   FROM giscegis_configsimulacio_bt c"
        "   LEFT JOIN giscegis_blocs_ctat b ON c.ct_bloc_id = b.id"
        ") AS upd "
        "WHERE giscegis_configsimulacio_bt.id = upd.id"
    )


def down(cursor, installed_version):
    pass


migrate = up
