# coding=utf-8
from oopgrade import oopgrade
import logging

logger = logging.getLogger('openerp.' + __name__)


def up(cursor, installed_version):

    logger.info('Renaming column ct to ct_bloc')

    oopgrade.rename_columns(
        cursor, {
            'giscegis_configsimulacio_bt': [
                ('ct', 'ct_bloc_id')
            ]
        }
    )

    logger.info('Done')


def down(cursor, installed_version):
    pass


migrate = up
