# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Config Simulacio BT",
    "description": """Permet enmagatzemar les configuracions de la xarxa BT""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_blocs_ctat",
        "giscegis_base",
        "giscegis_cups"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscegis_configsimulacio_bt_view.xml",
        "giscegis_configsimulacio_bt_sequence.xml",
        "ir.model.access.csv",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
