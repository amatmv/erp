from osv import fields, osv


class GiscegisConfigsimulacioBt(osv.osv):
    _name = "giscegis.configsimulacio.bt"
    _description = "Configuracio Simulacio BT"
    _columns = {
        'name': fields.char('Nom', size=50, required=True),
        'ct': fields.many2one('giscedata.cts', 'Font'),
        'autosaved': fields.boolean('Autosaved'),
    }

    _defaults = {
        'autosaved': lambda *a: 1,
    }
    _order = "name, id"


GiscegisConfigsimulacioBt()


class GiscegisConfigsimulacioBtOberts(osv.osv):
    _name = "giscegis.configsimulacio.bt.oberts"
    _description = "Configuracio Oberts BT"
    _columns = {
        'name': fields.char('Nom', size=50, required=True),
        'config': fields.many2one('giscegis.configsimulacio.bt', 'Configuracio'),
        'node': fields.many2one('giscegis.nodes', 'Node', ondelete='set null'),
    }

    _defaults = {

    }
    _order = "name, id"


GiscegisConfigsimulacioBtOberts()
