from osv import osv, fields


class GiscegisTelegestioComptadorGeom(osv.osv):
    _name = 'giscegis.telegestio.comptador.geom'
    _auto = False
    _description = 'Telegestio de comptadors'
    _columns = {
        'name': fields.char('Name', size=10, required=True),
        'tg_last_read': fields.char("Last read", size=256),
        'model': fields.char("Model", size=256),
        'tg': fields.boolean("TG"),
        'titular': fields.char("Titular", size=256),
        'direccio': fields.char("Direccio", size=256),
        'concentrador': fields.char("Concentrador", size=256),
        'firmware': fields.char("Firmware", size=256),
        'x': fields.float('X'),
        'y': fields.float('Y'),
        'cups': fields.char('CUPS', size=256),
        'polissa': fields.char('Polissa', size=256),
        "materialize_date": fields.datetime("Fecha creacion vista"),
    }

    def init(self, cursor):
        """
        Initializes the module and creates the materialized view
        :param cursor: cursor Database cursor
        :return:
        """
        cursor.execute('''
            DROP MATERIALIZED VIEW IF EXISTS giscegis_telegestio_comptador_geom;
        ''')
        cursor.execute('''
            CREATE MATERIALIZED VIEW giscegis_telegestio_comptador_geom AS
                SELECT
                    comp.id AS id,
                    comp.name AS name ,
                    comp.tg AS tg,
                    COALESCE(comp.tg_cnc_conn, false) AS tg_cnc_conn,
                    to_char(comp.tg_last_read,'DD/MM/YYYY') AS tg_last_read,
                    pol.tg AS tg_state,
                    extract('days' from (now() - comp.tg_last_read)) <= 3 AS online,
                    product_tmpl.name AS model,
                    cups.direccio AS direccio,
                    part.name AS titular,
                    con.name AS concentrador,
                    CASE WHEN comp.firmware_version IS NOT NULL AND  comp.prime_firmware_version IS NOT NULL THEN comp.firmware_version||' / '||comp.prime_firmware_version
                    ELSE NULL
                    END AS firmware,
                    st_x(esc.geom) AS x,
                    st_y(esc.geom) AS y,
                    pol.name as polissa,
                    cups.name as cups,
                    now() as materialize_date
                FROM giscedata_lectures_comptador AS comp
                    LEFT JOIN giscedata_polissa AS pol ON comp.polissa = pol.id
                    LEFT JOIN giscedata_cups_ps AS cups ON pol.cups = cups.id
                    LEFT JOIN giscedata_cups_escomesa AS esc ON cups.id_escomesa = esc.id
                    LEFT JOIN stock_production_lot p_lot ON comp.serial = p_lot.id
                    LEFT JOIN product_product AS product ON product.id = p_lot.product_id
                    LEFT JOIN product_template AS product_tmpl ON product_tmpl.id = product.product_tmpl_id
                    LEFT JOIN res_partner AS part ON pol.titular = part.id
                    LEFT JOIN tg_concentrator AS con ON con.id = comp.tg_cnc_id
                WHERE comp.tg is True and comp.active;
        ''')

        # Creacio d'indexs
        cursor.execute("""
          DROP INDEX IF EXISTS giscegis_telegestio_comptador_tg_idx;
          DROP INDEX IF EXISTS giscegis_telegestio_comptador_tg_cnc_conn_idx;
          DROP INDEX IF EXISTS giscegis_telegestio_comptador_tg_state_idx;
          DROP INDEX IF EXISTS giscegis_telegestio_comptador_tg_online_idx;
          CREATE INDEX giscegis_telegestio_comptador_tg_idx ON giscegis_telegestio_comptador_geom(tg);
          CREATE INDEX giscegis_telegestio_comptador_tg_cnc_conn_idx ON giscegis_telegestio_comptador_geom(tg_cnc_conn);
          CREATE INDEX giscegis_telegestio_comptador_tg_state_idx ON giscegis_telegestio_comptador_geom(tg_state);
          CREATE INDEX giscegis_telegestio_comptador_tg_online_idx ON giscegis_telegestio_comptador_geom(online);
                """)

        return True

    def get_srid(self, cursor, uid):
        """
        :param cursor: cursor Database cursor
        :param uid: integer User identifier
        :return: Returns a list of srids in geom field
        """
        cursor.execute('''
          SELECT DISTINCT (ST_SRID(geom)) AS srid FROM giscegis_telegestio_comptador_geom
        ''')
        res = cursor.fetchall()
        if not res:
            return False
        else:
            return [x[0] for x in res]

    def get_time(self, cr, uid):
        """
        :param cursor: cursor Database cursor
        :param uid: integer User identifier
        :return: Returns time
        """
        cr.execute("SELECT to_char(now(),'YYYY-MM-DD HH24:MI:SS')")
        res = cr.fetchone()
        return res[0]

    def get_coords(self, cursor, uid, ids, srid=None, context=None):
        """ Get the lat,lon of a node
        :param cursor: cursor Database cursor
        :param uid: integer User identifier
        :param ids: integer Single id of the node
        :param srid: integer Optional SRID for the output's projection
        :param context: context Context
        :return: X and Y of the node in a dict
        """
        if isinstance(ids, (list, tuple)):
            ids = ids[0]
        if not srid:
            conf_obj = self.pool.get('res.config')
            srid_id = conf_obj.search(
                cursor, uid, [('name', '=', 'giscegis_srid')])
            if srid_id:
                srid = int(conf_obj.read(cursor, uid, srid_id[0],
                                         ['value'])['value'])
        sql = '''SELECT
            ST_X(ST_TRANSFORM(geom,%(srid)s)),
            ST_Y(ST_TRANSFORM(geom,%(srid)s))
            FROM
              giscegis_telegestio_geom n
            WHERE
              n.id = %(ids)s
        '''
        sql_params = {
            'ids': str(ids),
            'srid': srid
        }
        cursor.execute(sql, sql_params)
        data = cursor.fetchone()
        if data is None:
            return {'x': 0, 'y': 0}
        return {'x': data[0], 'y': data[1]}

    def recreate(self, cursor, uid):
        """
        Refreshes the giscegis_telegestio_geom materialized view
        :param cursor: cursor Database cursor
        :param uid: integer User identifier
        :return:
        """
        cursor.execute('''
            REFRESH MATERIALIZED VIEW giscegis_telegestio_comptador_geom;
        ''')
        return True

GiscegisTelegestioComptadorGeom()
