# -*- coding: utf-8 -*-
{
    "name": "GISCE GISv3 Telegestio",
    "description": """Model de Telegestio per a Giscegis v3""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_base_geom",
        "giscedata_lectures",
        "giscedata_lectures_telegestio",
        "giscedata_cups",
        "giscedata_cups_distri",
        "giscegis_blocs_escomeses",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscegis_layers_data.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
