# *-* coding: utf-8 *-*

from osv import osv, fields

class giscegis_blocs_trafos_171(osv.osv):

    _name = 'giscegis.blocs.trafos.171'
    _auto = False
    _columns = {
      'x': fields.float('X', readonly=True),
      'y': fields.float('Y', readonly=True),
      'texte': fields.text('Texte', readonly=True),
      'blockname': fields.char('Blockname', size=50),
      'rotation': fields.float('Rotation'),
    }

    def init(self, cr):
        cr.execute("""drop view if exists
                      giscegis_blocs_transformadors_agregats cascade""")
        cr.execute("""
          create or replace view giscegis_blocs_transformadors_agregats as (
          (select tt.id,tt.create_uid, tt.create_date,tt.write_date,tt.write_uid,node,tt.name,bb.name as blockname,ordre_transformador_ct,vertex,height,width,codi_ct,rotation,transformadors,tensio,ct from giscegis_blocs_transformadors tt, giscegis_blocs_transformador_blockname bb where bb.id=tt.blockname) union (select t.id,t.create_uid, t.create_date,t.write_date,t.write_uid,node,t.name,b.name as blockname,ordre_transformador_ct,vertex,height,width,codi_ct,rotation,transformadors,tensio,ct from giscegis_blocs_transformadors_reductors t , giscegis_blocs_transformador_reductor_blockname b where b.id = t.blockname) order by codi_ct
          )
        """)
        cr.execute("""drop view if exists giscegis_blocs_trafos_171 cascade""")
        cr.execute("""
          create or replace view giscegis_blocs_trafos_171 as (
    select trafo.id, vertex.x, vertex.y, blockname, trafo.rotation, 'TRF-' ||  COALESCE(trf.name::text,trafo.name) || E'\n' || coalesce(trf.potencia_nominal::text, 'S/P') || ' kVA'  as texte
    from giscegis_blocs_transformadors_agregats trafo
      left join giscegis_vertex vertex on (trafo.vertex = vertex.id)
      left join giscedata_transformador_trafo trf on (trf.id=trafo.transformadors)

    )""")

giscegis_blocs_trafos_171()
