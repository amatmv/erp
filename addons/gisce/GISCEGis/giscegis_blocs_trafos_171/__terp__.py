# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Blocs Transformadors 1.7.1",
    "description": """Vista dels transformadors pel MG-1.7.1""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_blocs_transformadors",
        "giscegis_blocs_transformadors_reductors",
        "giscegis_base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "ir.model.access.csv",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
