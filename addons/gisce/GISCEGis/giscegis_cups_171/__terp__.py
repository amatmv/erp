# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS CTS",
    "description": """Moul pel GIS 1.7.1, permet la connexio d'aquest a la base de dades del ERP, sense necessitar la BD d'Access""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscedata_cups",
        "giscegis_vertex",
        "giscedata_polissa",
        "giscegis_blocs_escomeses",
        "giscegis_blocs_ctat",
        "giscegis_escomeses_traceability",
        "giscegis_base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "ir.model.access.csv",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
