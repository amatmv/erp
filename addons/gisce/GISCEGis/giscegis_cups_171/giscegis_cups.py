# -*- coding: utf-8 -*-
from osv import fields, osv

class giscegis_cups_escomeses_171_tensio(osv.osv):
    """Modul transicional pel GIS versio1.7.1.
    Permet que el GIS es connecti a la base de dades de l'ERP enlloc d'anar
    cap al MS Access.
    """
    _name = "giscegis.cups.escomeses.171.tensio"
    _description = "Escomeses Llistat per GIS s/tensio"
    _auto = False
    _columns = {
      'texte': fields.char('Text', size=120, readonly=True),
      'id': fields.integer('Id', readonly=True),
      'name': fields.char('Name', size=10, readonly=True),
      'x': fields.float('X', readonly=True),
      'y': fields.float('Y', readonly=True),
      'rotation': fields.float('Rotation', readonly=True),
      'blockname': fields.char('Blockname',size=30, readonly=True),
      'ct': fields.char('CT', size=10, readonly=True),
      'ordre_tr': fields.integer('Ordre en CT', readonly=True),
      'sortida': fields.char('Sortida', size=2, readonly=True),
      'sum': fields.float(u'Potència total comptadors', digits=(16, 3),
                          readonly=True),
      'count': fields.integer(u'Num. CUPS', readonly=True)

    }

    _defaults = {

    }
    _order = 'id'

    def init(self,cr):
        cr.execute("""drop view if exists giscegis_cups_escomeses_171_tensio""")
        cr.execute("""
    create or replace view giscegis_cups_escomeses_171_tensio as (
    select e.id as id, e.name::integer,
    (bb.name::text || ';' || coalesce(etr.tensio::text, ''::text)) as blockname,
    (
    bb.name || ' ; ' || e.name || E'\n' ||
      coalesce(ct.name, 'S/CT') || ' ; ' ||
      coalesce(tr.ordre_dins_ct::text, 'S/T') || ' ; ' ||
      coalesce(f.sortida_bt, 'S/S')
    || E'\n' ||
    'CUPS: ' || coalesce(count(cups.id_escomesa), 0) || '\nPcont: ' ||
    coalesce(round(sum(p.potencia)::numeric,2),0) || ' kW\nPconv: ' ||
    coalesce(round(sum(cups.potencia_conveni)::numeric, 2),0::numeric) || ' kW') as texte,
    ct.name as ct, tr.ordre_dins_ct as ordre_tr,
    f.sortida_bt as sortida, v.x, v.y, be.rotation, sum(p.potencia), count(cups.id_escomesa)
    from giscedata_cups_escomesa e

    left join giscegis_escomeses_traceability etr on e.id = etr.escomesa
    left join giscedata_cts ct on etr.ct = ct.id
    left join giscedata_transformador_trafo tr on etr.trafo = tr.id
    left join giscegis_blocs_fusiblesbt f on etr.fusible = f.id
    left join giscegis_blocs_escomeses be on e.id = be.escomesa
    left join giscedata_cups_blockname bb on be.blockname = bb.id
    left join giscegis_vertex v on be.vertex = v.id
    left join giscedata_cups_ps cups on e.id = cups.id_escomesa and cups.active
    left join giscedata_polissa p on (cups.id = p.cups and p.state not in ('esborrany', 'baixa', 'pendent'))
    group by e.id, e.name, bb.name, ct.name, tr.ordre_dins_ct, f.sortida_bt, v.x, v.y, be.rotation, etr.tensio
    order by e.name
    )
    """)

giscegis_cups_escomeses_171_tensio()

class giscegis_cups_escomeses_171_sortida(osv.osv):
    """Modul transicional pel GIS versio1.7.1.
    Permet que el GIS es connecti a la base de dades de l'ERP enlloc d'anar
    cap al MS Access.
    """

    _name = "giscegis.cups.escomeses.171.sortida"
    _description = "Escomeses Llistat per GIS s/sortida"
    _auto = False
    _columns = {
      'texte': fields.char('Text', size=120, readonly=True),
      'id': fields.integer('Id', readonly=True),
      'name': fields.char('Name', size=10, readonly=True),
      'x': fields.float('X', readonly=True),
      'y': fields.float('Y', readonly=True),
      'rotation': fields.float('Rotation', readonly=True),
      'blockname': fields.char('Blockname',size=30, readonly=True),
      'ct': fields.char('CT', size=10, readonly=True),
      'ordre_tr': fields.integer('Ordre en CT', readonly=True),
      'sortida': fields.char('Sortida', size=2, readonly=True),
      'sum': fields.float(u'Potència total comptadors', digits=(16, 3),
                          readonly=True),
      'count': fields.integer(u'Num. CUPS', readonly=True)
    }

    _defaults = {

    }

    _order = 'id'

    def init(self,cr):
        cr.execute("""drop view if exists
                      giscegis_cups_escomeses_171_sortida""")
        cr.execute("""
        create or replace view giscegis_cups_escomeses_171_sortida as (
    select e.id as id, e.name,
    (bb.name::text ||  coalesce(';L'||f.sortida_bt::text, ''::text)) as blockname,
    (
    bb.name || ' ; ' || e.name || E'\n' ||
      coalesce(ct.name, 'S/CT') || ' ; ' ||
      coalesce(tr.ordre_dins_ct::text, 'S/T') || ' ; ' ||
      coalesce('L'||f.sortida_bt, 'S/S')
    || E'\n' ||
    'CUPS: ' || coalesce(count(cups.id_escomesa), 0) || '\nPcont: ' ||
    coalesce(round(sum(p.potencia)::numeric,2),0::numeric) || ' kW\nPconv: ' ||
    coalesce(round(sum(cups.potencia_conveni)::numeric, 2),0::numeric) || ' kW')  as texte,
    ct.name as ct, tr.ordre_dins_ct as ordre_tr,
    f.sortida_bt as sortida, v.x, v.y, be.rotation, sum(p.potencia), count(cups.id_escomesa)
    from giscedata_cups_escomesa e

    left join giscegis_escomeses_traceability etr on e.id = etr.escomesa
    left join giscedata_cts ct on etr.ct = ct.id
    left join giscedata_transformador_trafo tr on etr.trafo = tr.id
    left join giscegis_blocs_fusiblesbt f on etr.fusible = f.id
    left join giscegis_blocs_escomeses be on e.id = be.escomesa
    left join giscedata_cups_blockname bb on be.blockname = bb.id
    left join giscegis_vertex v on be.vertex = v.id
    left join giscedata_cups_ps cups on e.id = cups.id_escomesa and cups.active
    left join giscedata_polissa p on (cups.id = p.cups and p.state not in ('esborrany', 'baixa', 'pendent'))
    group by e.id, e.name, bb.name, ct.name, tr.ordre_dins_ct, f.sortida_bt, v.x, v.y, be.rotation
    order by e.name
        )""")

giscegis_cups_escomeses_171_sortida()
