from osv import fields
from giscegis_base_geom.fields import Point
from giscegis_base_geom.giscegis_session import SessionManaged
from tools import config


class GiscedataBtQuadreElement(SessionManaged):

    _name = 'giscedata.bt.quadre.element'
    _inherit = 'giscedata.bt.quadre.element'

    _columns = {
        'geom': Point("Geometria", srid=config.get('srid', 25830), select=True),
        'rotation': fields.integer("Rotation"),
        'node_id': fields.many2one("giscegis.nodes", "Node",
                                   ondelete='set null')
    }

    _defaults = {
        'rotation': lambda *a: 0,
    }


GiscedataBtQuadreElement()
