# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Quadre BT",
    "description": """GIS suport for Quadre de Baixa Tensió""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "giscegis_base",
        "giscegis_base_geom",
        "giscegis_cataleg",
        "giscegis_nodes",
        "giscedata_bt_quadre"
    ],
    "init_xml": [],
    "demo_xml": [
        "giscegis_bt_quadre_demo.xml"
    ],
    "update_xml":[
        "giscegis_cataleg_bt_quadre_data.xml",
    ],
    "active": False,
    "installable": True
}
