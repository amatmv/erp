# -*- coding: utf-8 -*-

from osv import fields, osv
from tools.translate import _
from tools import config


class GiscegisCtsGeom(osv.osv):
    """
    Creates a materialized view for CTS
    """
    _name = "giscegis.cts.geom"
    _description = "CT's Llistat per GIS v3"
    _auto = False
    _columns = {
        'id': fields.integer('Id', readonly=True),
        'name': fields.char('Text', size=120, readonly=True),
        'rotation': fields.float('Rotation', readonly=True),
        'geom': fields.char('Geom', size=120, readonly=True),
        'texte': fields.char('Texte', size=120, readonly=True),
        "materialize_date": fields.datetime(_("Fecha creación vista")),
    }

    _defaults = {

    }
    _order = 'id'

    def init(self, cursor):
        """
        Initializes the module

        :param cursor: Database cursor
        :return: None
        """
        cursor.execute("""
        DROP MATERIALIZED VIEW IF EXISTS public.giscegis_cts_geom;
        """)
        cursor.execute("""
        DROP MATERIALIZED VIEW IF EXISTS giscegis_cts_geom;
        CREATE MATERIALIZED VIEW public.giscegis_cts_geom AS
        SELECT    ctbloc.ct as id,
                  ct.id_subtipus,
                  ctbloc.codi AS name,
                  ctbloc.rotation AS rotation,
                  ctbloc.codi::text
                            || e'\n'::text
                            || ct.descripcio::text
                            || e'\n'::text
                            || coalesce(
                                         (
                                         SELECT   (coalesce(e.industria, 'S/E'::CHARACTER varying)::text
                                                           || ' ; '::text)
                                                           || coalesce(e.industria_data::text, 'S/D'::text)
                                         FROM     giscedata_expedients_expedient e,
                                                  giscedata_cts_expedients_rel r
                                         WHERE    r.ct_id = ctbloc.ct
                                         AND      r.expedient_id = e.id
                                         ORDER BY e.industria_data IS NOT NULL,
                                                  e.industria_data DESC limit 1), 'S/E ; S/D'::text)
                            || e'\n'::text
                            || coalesce(SUM(tr.potencia_nominal), 0::bigint)
                            || ' kVA'::text
                            || e'\n'::text AS texte_complete,
                  ctbloc.codi::text
                            || e'\n'::text
                            || ct.descripcio AS texte_simple,
                  ST_SETSRID(
                      ST_MAKEPOINT(v.x,v.y), %(srid)s
                  ) AS geom,
                  ctbloc.codi::text
                            || e'\n'::text
                            || ct.descripcio AS texte,
                  now() AS materialize_date
        FROM      giscegis_blocs_ctat ctbloc
        left join
                  (
                            SELECT    trr.id,
                                      trr.potencia_nominal,
                                      trr.ct
                            FROM      giscedata_transformador_trafo trr
                            left join giscedata_transformador_estat tres
                            ON        trr.id_estat = tres.id
                            AND       tres.codi = 1) tr
        ON        tr.ct = ctbloc.ct
        left join giscedata_cts ct
        ON        ct.id = ctbloc.ct
        left join giscegis_vertex v
        ON        ctbloc.vertex = v.id
        WHERE     (
                            ct.ct_baixa = FALSE
                  OR        ct.ct_baixa IS NULL)
        AND       ct.active = TRUE
        GROUP BY  ctbloc.codi,
                  ctbloc.ct,
                  ctbloc.rotation,
                  ct.descripcio,
                  ct.id_subtipus,
                  v.x,
                  v.y;
        """, {'srid': config.get('srid', 25830)})
        cursor.execute("""
        DROP INDEX IF EXISTS giscegis_cts_geom_geom;
        CREATE INDEX giscegis_cts_geom_geom
        ON giscegis_cts_geom using gist (geom);
        """)

    def getSRID(self, cursor, uid):
        """
        Returns the SRID of the cts view

        :param cursor: Database cursor
        :param uid: User id
        :return: SRID as text
        """
        cursor.execute("""
        SELECT ST_SRID(geom) as srid FROM giscegis_cts_geom LIMIT 1;
        """)
        return cursor.fetchall()[0]

    def recreate(self, cursor, uid):
        """
        Recreates the materialized view

        :param cursor: Database cursor
        :param uid: User id
        :return: None
        """
        cursor.execute('''
        REFRESH MATERIALIZED VIEW giscegis_cts_geom;
        ''')

    def get_geom_sortides(self, cursor, uid, ids, sortida, out_format="geojson", srid=4326, context=None):
        """
        Gets the geometries of the LBT sortides of the CT

        :param cursor: Database cursor
        :param uid: User id
        :param ids: CT id
        :param sortida: Sortida to get the geometry
        :type sortida: int
        :param out_format:
        :param srid: Out SRID
        :type srid: int, str
        :param context: OpenEPR Context
        :type context: dict
        :return:
        :rtype: str
        """
        if out_format.lower() == "geojson":
            sql = """
            SELECT ST_AsGeoJSON(st_union(st_transform(geom, %s)))
            FROM giscegis_lbt_geom
            WHERE geom IS NOT NULL AND id IN (
              SELECT id from giscedata_bt_element
              WHERE ct IN %s AND num_linia::INT = %s
            );
            """
            cursor.execute(sql, (int(srid), tuple(ids), sortida))
            data = cursor.fetchall()
            return [x[0] for x in data]

    # TODO: Delete when is fixed on the webgis
    def get_geom_sorides(self, cursor, uid, ids, sortida, out_format="geojson", srid=4326, context=None):
        return self.get_geom_sortides(cursor, uid, ids, sortida, out_format="geojson", srid=4326, context=context)

    def get_num_sortides(self, cursor, uid, ids, context=None):
        """
        Return the avaible sortides of a CT

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: Id of the CT to get the sortides
        :type ids; int, list
        :param context: OpenERP context
        :type context: dict
        :return: Sortides of the CT
        :rtype: list
        """
        if isinstance(ids, list):
            ids = ids[0]
        sql = """
            SELECT DISTINCT(num_linia::INT), geom.color
               FROM giscedata_bt_element bt
               inner join giscegis_lbt_geom geom on bt.id=geom.id
               WHERE bt.ct=%s
                AND bt.num_linia IS NOT NULL
                AND geom.geom IS NOT NULL
            ORDER BY 1;
            """
        cursor.execute(sql, (int(ids),))
        data = cursor.fetchall()

        dict_data = {}
        for x in data:
            dict_data[str(x[0])] = x[1]
        return dict_data


GiscegisCtsGeom()
