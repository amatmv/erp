# -*- coding: utf-8 -*-
from osv import fields, osv
from tools import config
import logging
from giscegis_base_geom.wizard.giscegis_shp_loader \
    import GIS_MODELS_TO_UPDATE_GEOM_DATA


# Subscribe this model to the list to be updated if there is an AutoCAD dump
GIS_MODELS_TO_UPDATE_GEOM_DATA.append(
    {
        'model': 'giscedata.punt.frontera',
        'functions': ['sync_elements_from_blocs']
    },
)


class GiscegisPuntFrontera(osv.osv):

    def sync_elements_from_blocs(self, cursor, uid):
        """
        Syncronize the Netsource elements with the Punt Frontera elements
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return: Return the number of elements syncronized.
        :rtype: int
        """

        logger = logging.getLogger(
            'openerp.giscedata.punt.frontera.sync_elements_from_blocs'
        )
        pf_obj = self.pool.get('giscedata.punt.frontera')
        pf_tipus_obj = self.pool.get('giscedata.punt.frontera.tipus')
        tipus_generacio_id = pf_tipus_obj.search(
            cursor, uid, [('name', '=', 'Punt Frontera Font')]
        )[0]
        sql = """
            SELECT b.name, b.rotation, b.codi, b.node,
                st_astext(st_setsrid(st_makepoint(v.x, v.y), %(srid)s)) AS geom
            FROM giscegis_blocs_netsource b
            LEFT JOIN giscegis_vertex v ON b.vertex = v.id
        """

        cursor.execute(sql, {"srid": config.get("srid", 25830)})

        rows = cursor.dictfetchall()
        row_n = len(rows)

        for row in rows:
            if not row['codi']:
                logger.warning(
                    "Field 'codi' of the model GiscegisPuntFrontera can't be "
                    "empty. Skiping the creation of the element with AutoCAD"
                    " code {}".format(row['name'])
                )
                continue
            if not tipus_generacio_id:
                logger.warning(
                    "Type of GiscegisPuntFrontera 'Punt Frontera Font' not "
                    "found. Skiping the creation of the element with "
                    "code {}".format(
                        row['codi']
                    )
                )
                continue
            pf_vals = {
                'codi': row['codi'],
                'rotation': row['rotation'],
                'node_id': row['node'],
                'geom': row['geom'],
                'tipus': tipus_generacio_id,
                'netsource_bloc_name': row['name']
            }
            pf_ids = pf_obj.search(
                cursor, uid, [('codi', '=', row['codi'])]
            )
            if pf_ids:
                pf_obj.write(cursor, uid, pf_ids[0], pf_vals)
            else:
                pf_vals.update(pf_obj.default_get(cursor, uid, ['name']))
                pf_obj.create(cursor, uid, pf_vals)

        return row_n

    _inherit = 'giscedata.punt.frontera'

    _columns = {
        'netsource_bloc_name': fields.char('Camp Name Netsource Bloc', size=64)
    }


GiscegisPuntFrontera()
