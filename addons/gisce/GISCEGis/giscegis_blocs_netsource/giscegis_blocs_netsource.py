# -*- coding: utf-8 -*-
from osv import osv, fields
from giscegis_base_geom.giscegis_base_geom import DROP_ON_AUTOCAD_DUMP

# Subscribe this model to the list to be updated if there is an AutoCAD dump
DROP_ON_AUTOCAD_DUMP.append('giscegis_blocs_netsource')


class giscegis_blocs_netsource_blockname(osv.osv):

    _name = 'giscegis.blocs.netsource.blockname'
    _description = 'Tipus Blocs NetSource'

    def create(self, cr, uid, vals, context={}):
        ids = self.search(cr, uid, [('name', '=', vals['name'])])
        if ids and len(ids):
            return ids[0]
        else:
            return super(osv.osv, self).create(cr, uid, vals)

    _columns = {
      'name': fields.char('BlockName',size=20,required=True),
      'description': fields.char('Descripció',size=255),
    }

    _defaults = {

    }

    _order = "name, id"

giscegis_blocs_netsource_blockname()

class giscegis_blocs_netsource(osv.osv):
    _name = 'giscegis.blocs.netsource'
    _description = 'GisceGIS NetSource'
    _columns = {
      'name': fields.char('Nom', size=100, required=True),
      'width': fields.float('Width'),
      'height': fields.float('Height'),
      'rotation': fields.float('Rotation'),
      'codi': fields.char('Codi', size=50),
      'vertex': fields.many2one('giscegis.vertex', 'Vertex',
                                ondelete='set null'),
      'blockname': fields.many2one('giscegis.blocs.netsource.blockname',
                                   'BlockName'),
      'node': fields.many2one('giscegis.nodes', 'Node', ondelete='set null'),
    }

    _defaults = {

    }

    _order = "name, id"

    def create(self, cursor, uid, vals, context=None):
        """Mètode sobreescrit per escriure als camps many2one des
        d'aquí.

        Ens hem d'assegurar de què els mètodes create() dels objectes
        referenciats estiguin alhora sobreescrits per tal què comprovin
        si l'objecte existeix o no."""
        _fields = [('vertex', 'giscegis.vertex'),
                   ('blockname', 'giscegis.blocs.netsource.blockname')]
        for field in _fields:
            if field[0] in vals and isinstance(vals[field[0]], list):
                obj = self.pool.get(field[1])
                remote_id = obj.create(cursor, uid, vals[field[0]][0][2])
                if remote_id:
                    vals[field[0]] = remote_id
        return super(osv.osv, self).create(cursor, uid, vals)

    def write(self, cursor, uid, ids, vals, context=None):
        """Mètode sobreescrit per escriure als camps many2one des d'aquí.

        Igual que amb el create() cal assabentar-se de què els write()s dels
        models referenciats estiguin sobreescrits 
        """
        _fields = [('vertex', 'giscegis.vertex'),
                   ('blockname', 'giscegis.blocs.netsource.blockname')]
        for field in _fields:
            if field[0] in vals and isinstance(vals[field[0]], list):
                obj = self.pool.get(field[1])
                search_params = []
                for k, v in vals[field[0]][0][2].items():
                    search_params.append((k, '=', v))
                remote_id = obj.search(cursor, uid, search_params)
                if remote_id:
                    vals[field[0]] = remote_id[0]
                else:
                    remote_id = obj.create(cursor, uid, vals[field[0]][0][2])
                    vals[field[0]] = remote_id
        return super(osv.osv, self).write(cursor, uid, ids, vals)

giscegis_blocs_netsource()
