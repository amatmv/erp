# coding=utf-8

import logging
import pooler


def up(cursor, installed_version):

    logger = logging.getLogger('openerp.migration')

    logger.info(
        'Sync Punts Frontera with Netsource Blocs'
    )

    pool = pooler.get_pool(cursor.dbname)
    pf_obj = pool.get('giscedata.punt.frontera')
    n_created_elements = pf_obj.sync_elements_from_blocs(cursor, 1)

    if n_created_elements == 1:
        msg = '1 Punt Frontera sync correctly with Netsource Blocs'
    else:
        msg = '{} Punts Frontera sync correctly with Netsource Blocs'.format(
            n_created_elements
        )

    logger.info(msg)


def down(cursor, installed_version):
    pass


migrate = up
