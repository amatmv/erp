# coding=utf-8
import logging

logger = logging.getLogger('openerp.' + __name__)


def up(cursor, installed_version):
    """
    Migration script that updates the data of fusible with the
    data of fusible_bloc_id

    :param cursor: Database cursor
    :param installed_version: Installed version
    :return: None
    :rtype: None
    """

    if not installed_version:
        return

    sql = """UPDATE giscegis_escomeses_traceability set fusible = upd.fusible FROM (
        SELECT
            trace.id AS id,
            quadre.id AS fusible
        FROM giscegis_escomeses_traceability AS trace
        LEFT JOIN giscegis_blocs_fusiblesbt AS b ON trace.fusible = b.id
        LEFT JOIN giscedata_bt_quadre_element AS quadre ON  quadre.codi= b.codi
        ) AS upd
    WHERE giscegis_escomeses_traceability.id = upd.id
    """

    cursor.execute(sql)


def down(cursor, installed_version):
    pass

migrate = up
