# coding=utf-8
from oopgrade import oopgrade
import logging

logger = logging.getLogger('openerp.' + __name__)


def up(cursor, installed_version):
    """
    Migration script that renames the fusible column to fusible_bloc_id
    :param cursor: Database cursor
    :param installed_version: Installed version
    :type installed_verssion: str
    :return: None
    :rtype: None
    """

    if not installed_version:
        return

    logger.info("Renamign column source to source_bloc_id of giscegis.escomeses.traceability")

    oopgrade.rename_columns(
        cursor, {
            "giscegis_escomeses_traceability": [
                ('fusible', 'fusible_bloc_id')
            ]
        }
    )

    logger.info("Done")


def down(cursor, installed_version):
    pass


migrate = up