# -*- coding: utf-8 -*-
from osv import osv, fields
from giscedata_polissa.giscedata_polissa import CONTRACT_IGNORED_STATES

class GiscedataCts(osv.osv):
    _name = 'giscedata.cts'
    _inherit = 'giscedata.cts'

    def _ff_n_cups(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        cups_obj = self.pool.get('giscedata.cups.ps')
        res = dict.fromkeys(ids, False)
        for ct in self.read(cursor, uid, ids, ['name'], context=context):
            res[ct['id']] = cups_obj.search_count(cursor, uid, [
                ('et', '=', ct['name'])
            ])
        return res

    def _ff_potencia_cups(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        cups_obj = self.pool.get('giscedata.cups.ps')
        polissa_obj = self.pool.get('giscedata.polissa')
        res = dict.fromkeys(ids, False)
        for ct in self.read(cursor, uid, ids, ['name'], context=context):
            cups = cups_obj.search(cursor, uid, [('et', '=', ct['name'])])
            polisses = polissa_obj.search(cursor, uid, [
                ('cups.id', 'in', cups),
                ('state', 'not in', CONTRACT_IGNORED_STATES)
            ])
            potencia = sum([
                p['potencia'] for p in
                    polissa_obj.read(cursor, uid, polisses, ['potencia'])
            ])
            res[ct['id']] = potencia
        return res

    _columns = {
        'n_cups': fields.function(
            _ff_n_cups, string='Número de CUPS', type='integer',
            method=True
        ),
        'potencia_cups': fields.function(
            _ff_potencia_cups, string='Potència Total CUPS', type='float',
            method=True
        )
    }

GiscedataCts()