# -*- coding: utf-8 -*-
import base64
from oorq.decorators import job
from tqdm import tqdm
import pandapower_erp
from osv import fields, osv
from giscegis_base_geom.giscegis_base_geom import DROP_ON_AUTOCAD_DUMP

# Subscribe this model to the list to be updated if there is an AutoCAD dump
DROP_ON_AUTOCAD_DUMP.append('giscegis_escomeses_traceability')


class GiscegisEscomesesTraceability(osv.osv):
    """
    Model to know the traceability of a escomesa
    """

    _name = "giscegis.escomeses.traceability"
    _description = "Tracabilitat per les escomeses"

    @job(queue="traceability", timeout=3600)
    def fill_from_network_async(self, cursor, uid, network_id, context=None):
        self.fill_from_network(cursor, uid, network_id, context=context)

    def fill_from_network(self, cursor, uid, network_id, context=None):
        """
        Fills the LBT traceability from serialized network

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param network_id: Serialized network id
        :type network_id: int
        :param context: OpeneERP context
        :type context: dict
        :return:
        :rtype: Filled lbts
        """

        if context is None:
            context = {}

        obj_net = self.pool.get("giscegis.network")
        obj_esc = self.pool.get("giscedata.cups.escomesa")
        obj_trf = self.pool.get("giscedata.transformador.trafo")

        self.clean(cursor, uid)

        net_data = obj_net.read(
            cursor, uid, network_id, ["network"], context=context
        )
        if net_data:
            net = pandapower_erp.pp.from_json_string(
                base64.b64decode(net_data["network"])
            )
            te = osv.TransactionExecute(cursor.dbname, uid, self._name)
            df_fields = ["name", "trafo_id_erp", "tensio", "sortida"]
            info = "Calculating Escomeses traceability"
            rows_list = list(net.load[df_fields].iterrows())
            total = len(rows_list)
            for index, row in tqdm(rows_list, desc=info, total=total):
                if row["name"] and row['trafo_id_erp']:
                    escomesa_id = int(row['name'].split(",")[1])
                    trafo_id = int(row['trafo_id_erp'].split(",")[1])
                    ct_id = row.get('ct_id_erp', False)
                    if not ct_id:
                        trf_data = obj_trf.read(
                            cursor, uid, trafo_id, ["ct"], context=context
                        )
                        if trf_data["ct"]:
                            ct_id = trf_data["ct"][0]

                    esc_data = obj_esc.read(
                        cursor, uid, escomesa_id, ["name"], context=context
                    )

                    row_data = {
                        "escomesa": escomesa_id,
                        "name": esc_data["name"],
                        "trafo": trafo_id,
                        "tensio": row['tensio'],
                        "ct": ct_id,
                        "sortida": row['sortida']
                    }
                    te.create(row_data, context=context)
        return True

    def clean(self, cr, uid):
        cr.execute("delete from giscegis_escomeses_traceability;")
        return True

    def write(self, cr, uid, ids, vals, context=None):
        if not context:
            context = {}
        # forcem que no es faci sync
        context.update({'sync': False})
        escomesa = {}
        if 'ct' in vals:
            escomesa['et'] = self.pool.get(
                'giscedata.cts'
            ).browse(cr, uid, vals['ct']).name
        if 'fusible' in vals:
            escomesa['linia'] = self.pool.get(
                'giscedata.bt.quadre.element'
            ).browse(cr, uid, vals['fusible']).sortida_bt
        if len(escomesa):
            ids_cups = self.pool.get('giscedata.cups.ps').search(
                cr, uid, [('id_escomesa.id', '=', vals['escomesa'])]
            )
            self.pool.get('giscedata.cups.ps').write(
                cr, uid, ids_cups, escomesa
            )

        return super(osv.osv, self).write(cr, uid, ids, vals)

    def create(self, cr, uid, vals, context=None):
        """
        Overwrites the create method to write the values on the escomesa

        :param cr: Database cursor
        :param uid: User id
        :type uid: int
        :param vals: Values to write
        :type vals: dict
        :param context: OpenERP context
        :type context: dict
        :raises: None
        :rtype:None
        """

        if context is None:
            context = {}
        escomesa = {}

        if not vals:
            return False

        ct_obj = self.pool.get("giscedata.cts")
        cups_obj = self.pool.get("giscedata.cups.ps")
        fusbt_obj = self.pool.get("giscedata.bt.quadre.element")
        esc_obj = self.pool.get('giscedata.cups.escomesa')

        # This is for the Qgis behavior and will be used when the create is
        # called from the function fill_from_network
        if vals.get('ct', False):
            ct_data = ct_obj.read(cr, uid, int(vals["ct"]), ["name"])
            if ct_data:
                escomesa['et'] = ct_data["name"]
            else:
                escomesa["et"] = ""
        if vals.get('sortida', False):
            escomesa['linia'] = vals['sortida']
        if vals.get('escomesa', False):
            ids_cups = cups_obj.search(
                cr, uid, [('id_escomesa', '=', vals['escomesa'])]
            )
            cups_obj.write(
                cr, uid, ids_cups, escomesa, context={'sync': False}
            )

        # This is for the AutoCAD behavior, the keys will only exist if the
        # creation of this element is made throught AutoCAD
        if vals.get('codi_ct', False):
            ids = ct_obj.search(cr, uid, [('name', '=', vals['codi_ct'])])
            escomesa['et'] = vals['codi_ct']
            del vals['codi_ct']
            if ids and len(ids):
                vals['ct'] = ids[0]

        if vals.get('codi_fusible', False):
            ids = fusbt_obj.search(
                cr, uid, [('codi', '=', vals['codi_fusible'])]
            )
            vals.pop('codi_fusible', False)
            if ids and len(ids):
                vals['fusible'] = ids[0]
                escomesa['linia'] = fusbt_obj.browse(
                    cr, uid, vals['fusible']
                ).sortida_bt

        if vals.get('codi_escomesa', False):
            ids = esc_obj.search(cr, uid, [('name', '=', vals['codi_escomesa'])])
            del vals['codi_escomesa']
            if ids and len(ids):
                vals['escomesa'] = ids[0]
                ids_cups = cups_obj.search(cr, uid, [('id_escomesa.id', '=', vals['escomesa'])])
                cups_obj.write(cr, uid, ids_cups, escomesa, context={'sync': False})

        if vals.get('objid_transformador', False):
            ids = self.pool.get('giscegis.blocs.transformadors').search(cr, uid, [('name', '=', vals['objid_transformador'])])
            del vals['objid_transformador']
            if ids and len(ids):
                trafo = self.pool.get('giscegis.blocs.transformadors').read(cr, uid, ids[0], ['transformadors'])
                if trafo and trafo['transformadors']:
                    vals['trafo'] = trafo['transformadors'][0]

        return super(osv.osv, self).create(cr, uid, vals)

    _columns = {
        'name': fields.char('Nom', size=10),
        'escomesa': fields.many2one('giscedata.cups.escomesa', 'Escomesa'),
        'ct': fields.many2one('giscedata.cts', 'Centre Transformador'),
        'trafo': fields.many2one('giscedata.transformador.trafo', 'Transformador'),
        'fusible': fields.many2one("giscedata.bt.quadre.element", 'Fusible'),
        'tensio': fields.float('Tensio'),
        'sortida': fields.char('Sortida BT', size=10)
    }

    _defaults = {

    }

    _order = "name, id"


GiscegisEscomesesTraceability()
