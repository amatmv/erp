# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Escomesa Traceability",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscedata_cups_distri",
        "giscedata_cts",
        "giscedata_transformadors",
        "giscegis_blocs_fusiblesbt",
        "giscedata_bt_quadre",
        "giscegis_base",
        "giscedata_polissa"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "ir.model.access.csv",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
