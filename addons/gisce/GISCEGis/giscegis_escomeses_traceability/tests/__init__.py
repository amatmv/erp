# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction


class EscomesesTraceabilityTest(testing.OOTestCase):
    """
    Tets for escomeses traceability
    """

    def test_et_escomesa(self):
        """
        Tests that the writen value on the escomesa is the
        :return: None
        """

        with Transaction().start(self.database) as txn:

            cursor = txn.cursor
            uid = txn.user

            ct_obj = self.openerp.pool.get("giscedata.cts")
            trac_obj = self.openerp.pool.get("giscegis.escomeses.traceability")
            esc_obj = self.openerp.pool.get("giscedata.cups.escomesa")
            cups_obj = self.openerp.pool.get("giscedata.cups.ps")

            id_ct = ct_obj.search(cursor, uid, [], limit=1)[0]
            name_ct = ct_obj.read(cursor, uid, id_ct, ["name"])

            esc_id = esc_obj.search(cursor, uid, [], limit=1)[0]
            cups_id = cups_obj.search(cursor, uid, [], limit=1)
            cups_obj.write(cursor, uid, cups_id, {"id_escomesa": esc_id})

            trac_obj.create(cursor, uid, {"ct": id_ct, "escomesa": esc_id})

            esc_data = cups_obj.read(cursor, uid, cups_id, ["et"])
            self.assertEqual(name_ct["name"], esc_data[0]["et"])

    def test_ct_false(self):
        """
        Test when CT data is false
        """

        trac_obj = self.openerp.pool.get("giscegis.escomeses.traceability")
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            trac_obj.create(cursor, uid, {"ct": False})

    def test_create_emtpy(self):
        """
        Test to create escomesa traceability with empty values
        """

        trac_obj = self.openerp.pool.get("giscegis.escomeses.traceability")
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            trac_obj.create(cursor, uid, {})

