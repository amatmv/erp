# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Elements AT vista 1.7.1",
    "description": """Vista dels elements AT per la versió MG-1.7.1""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscedata_at",
        "giscegis_base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "ir.model.access.csv",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
