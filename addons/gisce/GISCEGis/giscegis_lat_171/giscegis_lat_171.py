# *-* coding: utf-8 *-*

from osv import osv, fields

class giscegis_lat_171(osv.osv):

    _name = 'giscegis.lat.171'
    _auto = False
    _columns = {
      'id': fields.integer('Id', readonly=True),
      'texte': fields.text('Texte', readonly=True),
    }

    def init(self, cr):
        cr.execute("""drop view if exists giscegis_lat_171""")
        cr.execute("""
          create or replace view giscegis_lat_171 as (
    select
    att.name, 'AT ' || coalesce(lat.name, 'S/Linia')  || '\n' ||
    coalesce(cab.name, 'S/Cable') || ' ; ' || coalesce(mat.name, 'S/Material') || ' ; ' || coalesce(cab.aillament, 'S/Aillament') || '\n' ||
    coalesce(
    (
    SELECT coalesce(e.industria, 'S/Exp') || ' ' || coalesce(e.industria_data::text, 'S/Data Exp')
    FROM giscedata_expedients_expedient e, giscedata_at_tram_expedient r
    WHERE r.tram_id = att.id AND r.expedient_id = e.id
    ORDER BY e.industria_data IS NOT NULL, e.industria_data DESC
    LIMIT 1
    ), 'S/Exp S/Data Exp') || '\n' ||
    coalesce(round(att.longitud_cad::numeric,2), 0) || 'm' as texte,
    (
      case
      when tipus.codi = 'D' or tipus.codi = 'T' then 'A-' || lat.tensio::text
      when tipus.codi = 'S' then 'S-' || lat.tensio::text
      when tipus.codi = 'E' then 'EAT'
      else '-'||lat.tensio::text
      end
    ) as theme

    from giscedata_at_tram att
    left join giscedata_at_linia lat on (att.linia = lat.id and lat.name != '1')
    left join giscedata_at_cables cab on (att.cable = cab.id)
    left join giscedata_at_material mat on (cab.material = mat.id)
    left join giscedata_at_tipuscable tipus on (cab.tipus = tipus.id)

    order by att.name

          )""")

giscegis_lat_171()


class giscegis_lat_171_circuits(osv.osv):

    _name = 'giscegis.lat.171.circuits'
    _auto = False
    _columns = {
      'id': fields.integer('Id', readonly=True),
      'texte': fields.text('Texte', readonly=True),
      'theme': fields.text('Tema', readonly=True),
    }

    def init(self, cr):
        cr.execute("""drop view if exists giscegis_lat_171_circuits""")
        cr.execute("""
          create or replace view giscegis_lat_171_circuits as (
    select
    att.name, 'AT ' || coalesce(lat.name, 'S/Linia')  || '\n' ||
    coalesce(cab.name, 'S/Cable') || ' ; ' || coalesce(mat.name, 'S/Material') || ' ; ' || coalesce(cab.aillament, 'S/Aillament') || '\n' ||
    coalesce(
    (
    SELECT coalesce(e.industria, 'S/Exp') || ' ' || coalesce(e.industria_data::text, 'S/Data Exp')
    FROM giscedata_expedients_expedient e, giscedata_at_tram_expedient r
    WHERE r.tram_id = att.id AND r.expedient_id = e.id
    ORDER BY e.industria_data IS NOT NULL, e.industria_data DESC
    LIMIT 1
    ), 'S/Exp S/Data Exp') || '\n' ||
    coalesce(round(att.longitud_cad::numeric,2), 0) || 'm' as texte,
    (
      case
      when tipus.codi = 'D' or tipus.codi = 'T' then 'A-' || COALESCE(att.circuits::varchar, '0')
      when tipus.codi = 'S' or tipus.codi = 'E' then 'S-' || COALESCE(att.circuits::varchar, '0')
      else '-' || COALESCE(att.circuits::varchar, '0')
      end
    ) as theme

    from giscedata_at_tram att
    left join giscedata_at_linia lat on (att.linia = lat.id and lat.name != '1')
    left join giscedata_at_cables cab on (att.cable = cab.id)
    left join giscedata_at_material mat on (cab.material = mat.id)
    left join giscedata_at_tipuscable tipus on (cab.tipus = tipus.id)

    order by att.name

          )""")

giscegis_lat_171_circuits()


class giscegis_lat_171_tensio_disseny(osv.osv):

    _name = 'giscegis.lat.171.tensio.disseny'
    _auto = False
    _columns = {
      'id': fields.integer('Id', readonly=True),
      'texte': fields.text('Texte', readonly=True),
      'theme': fields.text('Tema', readonly=True),
    }

    def init(self, cr):
        cr.execute("""drop view if exists giscegis_lat_171_tensio_disseny""")
        cr.execute("""
          create or replace view giscegis_lat_171_tensio_disseny as (
    select
    att.name, 'AT ' || coalesce(lat.name, 'S/Linia')  || '\n' ||
    coalesce(cab.name, 'S/Cable') || ' ; ' || coalesce(mat.name, 'S/Material') || ' ; ' || coalesce(cab.aillament, 'S/Aillament') || '\n' ||
    coalesce(
    (
    SELECT coalesce(e.industria, 'S/Exp') || ' ' || coalesce(e.industria_data::text, 'S/Data Exp')
    FROM giscedata_expedients_expedient e, giscedata_at_tram_expedient r
    WHERE r.tram_id = att.id AND r.expedient_id = e.id
    ORDER BY e.industria_data IS NOT NULL, e.industria_data DESC
    LIMIT 1
    ), 'S/Exp S/Data Exp') || '\n' ||
    coalesce(round(att.longitud_cad::numeric,2), 0) || 'm' as texte,
    COALESCE(att.tensio_max_disseny::varchar, '0') as theme

    from giscedata_at_tram att
    left join giscedata_at_linia lat on (att.linia = lat.id and lat.name != '1')
    left join giscedata_at_cables cab on (att.cable = cab.id)
    left join giscedata_at_material mat on (cab.material = mat.id)
    left join giscedata_at_tipuscable tipus on (cab.tipus = tipus.id)

    order by att.name

          )""")

giscegis_lat_171_tensio_disseny()
