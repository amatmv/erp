# Translation of OpenERP Server.
# This file contains the translation of the following modules:
# 
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: GISCE-ERP\n"
"Report-Msgid-Bugs-To: https://github.com/gisce/erp/issues\n"
"POT-Creation-Date: 2012-04-25 16:11:35+0000\n"
"PO-Revision-Date: 2012-03-28 16:12+0000\n"
"Last-Translator: <>\n"
"Language-Team: English (United States) (http://trad.gisce.net/projects/p/erp/language/en_US/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: en_US\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. module: giscegis_lat_171
#: constraint:ir.model:0
msgid ""
"The Object name must start with x_ and not contain any special character !"
msgstr ""

#. module: giscegis_lat_171
#: model:ir.module.module,description:giscegis_lat_171.module_meta_information
msgid "Vista dels elements AT per la versió MG-1.7.1"
msgstr ""

#. module: giscegis_lat_171
#: field:giscegis.lat.171,texte:0
msgid "Texte"
msgstr ""

#. module: giscegis_lat_171
#: model:ir.module.module,shortdesc:giscegis_lat_171.module_meta_information
msgid "GISCE GIS Elements AT vista 1.7.1"
msgstr ""

#. module: giscegis_lat_171
#: model:ir.model,name:giscegis_lat_171.model_giscegis_lat_171
msgid "giscegis.lat.171"
msgstr ""

#. module: giscegis_lat_171
#: field:giscegis.lat.171,id:0
msgid "Id"
msgstr ""
