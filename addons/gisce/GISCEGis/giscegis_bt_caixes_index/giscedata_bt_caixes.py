from giscegis_base_index.giscegis_base_index import GiscegisBaseIndex
import pooler


class GiscedataBtCaixaElement(GiscegisBaseIndex):
    """
    Class to index caixes bt if no active session
    """
    _name = 'giscedata.bt.caixa.element'
    _inherit = 'giscedata.bt.caixa.element'
    _index_title = '{obj.name}'
    _index_summary = '{obj.name} ({obj.tipus.name})'
    _index_fields = {
        'name': True,
        'data_pm': True,
        'intensitat': True
    }

    def get_schema(self, cursor, uid, item, context=None):
        """
        Gets the index schema

        :param cursor: Database cursor
        :param uid: User id
        :param item: item to index
        :param context: OpenERP context
        :return: Dict with the schema
        """
        lat, lon = self.get_lat_lon(cursor, uid, item, context=context)
        content = self.get_content(cursor, uid, item, context=context)
        return {
            'id': '1{}'.format(item.id),
            'path': self.get_path(cursor, uid, item.id, context=context),
            'title': self._index_title.format(obj=item),
            'content': ' '.join(set(content)),
            'gistype': 'giscedata.interruptors.bt',
            'summary': self._index_summary.format(obj=item),
            'lat': lat,
            'lon': lon
        }

    def index_all(self, cursor, uid, context=None):
        """
        Indexes all elements of the model
        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param context: OpenERP context
        :type context: dict
        """
        if context is None:
            context = {}

        if not pooler.pool_dic[cursor.dbname]._ready:
            return 0

        ids = self.search(cursor, uid, [], context=context)
        for identifier in ids:
            self.index(cursor, uid, identifier, context=context)

        return len(ids)

    def get_path(self, cursor, uid, item_id, context=None):
        """
        Implements the get_path

        :param cursor: Database cursor
        :param uid: User id
        :param item_id: Item id
        :param context: OpenERP context
        :return: Path as str
        """
        return 'giscedata.bt.caixa.element/4{0}'.format(item_id)

    def get_lat_lon(self, cursor, uid, item, context=None):
        """
        Method to get latitude and longitude

        :param cursor: Database cursor
        :param uid: User id
        :param item: Item to index
        :param context: OpenERP context
        :return: Tuple with lat and lon
        """
        if context is None:
            context = {}
        cursor.execute("""
        SELECT
            ST_Y(ST_TRANSFORM(geom,4326)) AS lat,
            ST_X(ST_Transform(geom, 4326)) AS lon
        FROM giscedata_bt_caixa_element
        WHERE id=%(id)s
        """, {'id': item.id})
        res = cursor.fetchone()
        if not res:
            res = (0, 0)
        return res


GiscedataBtCaixaElement()
