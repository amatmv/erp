# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Cerca Caixes de Baixa Tensió",
    "description": """Cerca de caixes de Baixa Tensió""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "giscegis_bt_caixes",
        "giscegis_base_index"
    ],
    "init_xml": [],
    "demo_xml": [
    ],
    "update_xml":[
    ],
    "active": False,
    "installable": True
}
