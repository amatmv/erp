# coding=utf-8
import logging
import pooler


def up(cursor, installed_version):

    logger = logging.getLogger('openerp.migration')

    logger.info(
        'Filling fields geom, rotation, node_id and xarxa from model'
        'giscedata_condensadors'
    )

    pool = pooler.get_pool(cursor.dbname)
    condensadors_obj = pool.get('giscedata.condensadors')
    condensadors_obj.fill_geom(cursor, 1)
    condensadors_obj.fill_rotation(cursor, 1)
    condensadors_obj.fill_node_id(cursor, 1)
    condensadors_obj.fill_xarxa(cursor, 1)

    logger.info(
        'Fields geom, rotation, node_id and xarxa filled for model '
        'giscedata_condensadors'
    )


def down(cursor, installed_version):
    pass


migrate = up
