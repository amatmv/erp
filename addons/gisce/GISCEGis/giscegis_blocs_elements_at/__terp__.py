# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Elements AT",
    "description": """Modul de Elements AT per a Autocad""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_vertex",
        "giscegis_nodes",
        "giscegis_base",
        "giscegis_condensadors",
        "giscegis_blocs_elements_bt"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscegis_blocs_elements_at_view.xml",
        "ir.model.access.csv",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
