# -*- coding: utf-8 -*-
from osv import osv
from tools import config
from giscegis_base_geom.wizard.giscegis_shp_loader \
    import GIS_MODELS_TO_UPDATE_GEOM_DATA


# Subscribe this model to the list to be updated if there is an AutoCAD dump
GIS_MODELS_TO_UPDATE_GEOM_DATA.append(
    {
        'model': 'giscedata.condensadors',
        'functions': [
            'fill_geom', 'fill_rotation', 'fill_node_id', 'fill_xarxa'
        ]
    },
)


class GiscedataCondensador(osv.osv):
    _name = 'giscedata.condensadors'
    _inherit = 'giscedata.condensadors'

    def fill_geom_at(self, cursor, uid):
        """
        Function that runs a query to update the field geom of all the
        Condensadors AT elements
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """
        sql = """
            UPDATE giscedata_condensadors AS c 
            SET geom = bloc.geom 
            FROM (
              SELECT b.codi AS name,
                     st_setsrid(st_makepoint(v.x, v.y), %(srid)s) AS geom
              FROM giscegis_elementsat b
              LEFT JOIN giscegis_vertex v ON b.vertex = v.id
            ) AS bloc
            WHERE c.name = bloc.name
        """

        cursor.execute(sql, {"srid": config.get("srid", 25830)})

    def fill_rotation_at(self, cursor, uid):
        """
        Function that runs a query to update the field rotation of all the
        Condensadors AT elements
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """
        sql = """
            UPDATE giscedata_condensadors AS c 
            SET rotation = 360-bloc.rotation 
            FROM (
              SELECT b.codi AS name,
                     b.rotation AS rotation
              FROM giscegis_elementsat b
            ) AS bloc
            WHERE c.name = bloc.name
        """

        cursor.execute(sql)

    def fill_node_id_at(self, cursor, uid):
        """
        Function that runs a query to update the field node_id of all the
        Condensadors AT elements
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """
        sql = """
            UPDATE giscedata_condensadors AS c 
            SET node_id = bloc.node_id 
            FROM (
              SELECT b.codi AS name,
                     b.node AS node_id
              FROM giscegis_elementsat b
            ) AS bloc
            WHERE c.name = bloc.name
        """

        cursor.execute(sql)

    def fill_xarxa_at(self, cursor, uid):
        """
        Function that runs a query to update the field xarxa of all the
        Condensadors AT elements
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """
        sql = """
            UPDATE giscedata_condensadors 
            SET xarxa = '1'
            WHERE name IN (
                SELECT codi AS name FROM giscegis_elementsat
            )
        """

        cursor.execute(sql)

    def fill_geom(self, cursor, uid):
        self.fill_geom_at(cursor, uid)
        self.fill_geom_bt(cursor, uid)
        sql = """
            UPDATE giscedata_condensadors SET geom = St_SnapToGrid(
                ST_Translate(geom, 0.0000000001, 0.0000000001), 0.0001
            );
        """
        cursor.execute(sql)

    def fill_rotation(self, cursor, uid):
        self.fill_rotation_at(cursor, uid)
        self.fill_rotation_bt(cursor, uid)

    def fill_node_id(self, cursor, uid):
        self.fill_node_id_at(cursor, uid)
        self.fill_node_id_bt(cursor, uid)

    def fill_xarxa(self, cursor, uid):
        self.fill_xarxa_at(cursor, uid)
        self.fill_xarxa_bt(cursor, uid)


GiscedataCondensador()
