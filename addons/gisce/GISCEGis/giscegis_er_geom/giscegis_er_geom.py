# -*- coding: utf-8 -*-

from osv import fields, osv
from tools import config


class GiscegisErGeom(osv.osv):
    _name = "giscegis.er.geom"
    _description = "Vista Materialitzada de Subestacions per GIS v3"
    _auto = False
    _columns = {
        'id': fields.integer('Id', readonly=True),
        'name': fields.char('Text', size=120, readonly=True),
        'rotation': fields.float('Rotation', readonly=True),
        'geom': fields.char('Geom', size=120, readonly=True),
        'texte': fields.char('Texte', size=120, readonly=True),
        "materialize_date": fields.datetime("Fecha creación vista"),
    }

    _defaults = {

    }
    _order = 'id'

    def init(self, cursor):

        cursor.execute("""
            DROP MATERIALIZED VIEW IF EXISTS giscegis_er_geom;
        """)

        cursor.execute("""
            CREATE MATERIALIZED VIEW public.giscegis_er_geom AS
                SELECT 
                    se.id AS id,
                    se.ct_id AS ct_id,
                    se.tecnologia AS tipus_parc,
                    se.tecnologia AS tecnologia,
                    ST_SETSRID(st_makepoint(v.x, v.y), %(srid)s) AS geom,
                    ct.descripcio AS texte,
                    now() AS materialize_date
                FROM giscedata_cts_subestacions se
                INNER JOIN giscegis_blocs_ctat block ON block.ct = se.ct_id
                LEFT JOIN giscedata_cts ct ON ct.id = se.ct_id
                LEFT JOIN giscegis_vertex v ON block.vertex = v.id;
        """, {'srid': config.get('srid', 25830)})

        cursor.execute("""
            CREATE INDEX giscegis_er_geom_geom ON giscegis_er_geom
            USING gist (geom);
        """)

    def get_srid(self, cursor, uid):
        """
        :param cursor: cursor Database cursor
        :param uid: integer User identifier
        :return: Returns a list of srids in geom field
        """

        return [config.get('srid', 25830)]

    def recreate(self, cursor, uid):

        cursor.execute("""
            REFRESH MATERIALIZED VIEW giscegis_er_geom;
        """)

        return True


GiscegisErGeom()
