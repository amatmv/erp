# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS v3 (Estacions reguladores)",
    "description": """Mòdul de estacions reguladores per a giscegis v3""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_base_geom",
        "giscedata_cts_subestacions",
        "giscegis_blocs_ctat"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
