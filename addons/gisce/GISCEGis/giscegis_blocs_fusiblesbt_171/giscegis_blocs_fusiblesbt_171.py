# *-* coding: utf-8 *-*

from osv import osv, fields

class giscegis_blocs_fusiblesbt_171(osv.osv):

    _name = 'giscegis.blocs.fusiblesbt.171'
    _auto = False
    _columns = {
      'x': fields.float('X', readonly=True),
      'y': fields.float('Y', readonly=True),
      'texte': fields.text('Texte', readonly=True),
      'blockname': fields.char('Blockname', size=50),
      'rotation': fields.float('Rotation'),
    }

    def init(self, cr):
        cr.execute("""drop view if exists giscegis_blocs_fusiblesbt_171""")
        cr.execute("""
          create or replace view giscegis_blocs_fusiblesbt_171 as (
            select vertex.x, vertex.y, blockname.name || ' ' || fusible.codi || E'\n' || fusible.intensitat || 'A' as texte, blockname.name as blockname, fusible.rotation
    from giscegis_blocs_fusiblesbt fusible
            left join giscegis_vertex vertex on (fusible.vertex = vertex.id)
            left join giscegis_blocs_fusiblesbt_blockname blockname on (fusible.blockname = blockname.id)
        )""")

giscegis_blocs_fusiblesbt_171()
