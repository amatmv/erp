# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Blocs Fusibles BT 1.7.1",
    "description": """Vista dels fusibles bt pel MG-1.7.1""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_blocs_fusiblesbt",
        "giscegis_base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
