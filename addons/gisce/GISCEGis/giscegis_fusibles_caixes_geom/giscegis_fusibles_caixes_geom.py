# *-* coding: utf-8 *-*

from osv import osv, fields
from tools import config


class GiscegisFusiblesCaixesGeom(osv.osv):
    _name = 'giscegis.fusibles.caixes.geom'
    _auto = False
    _columns = {
        'id': fields.integer('Id', readonly=True),
        'name': fields.integer('Id', readonly=True),
        'geom': fields.text('Texte', readonly=True),
        'simple_geom': fields.text('Texte', readonly=True),
        'texte': fields.text('Texte', readonly=True),
        'theme': fields.text('Texte', readonly=True),
        "materialize_date": fields.datetime("Fecha creación vista"),
    }

    def init(self, cursor):

        cursor.execute("""
            DROP MATERIALIZED VIEW IF EXISTS giscegis_fusibles_caixes_geom;
        """)

        cursor.execute("""
            CREATE MATERIALIZED VIEW public.giscegis_fusibles_caixes_geom AS
                SELECT 
                    block.id AS id,
                    blockname.name AS symbol,
                    block.rotation,
                    ST_SETSRID(
                        ST_MAKEPOINT(vertex.x, vertex.y), %(srid)s     
                    ) AS geom,
                    now() as materialize_date
                FROM giscegis_blocs_fusibles_caixes AS block
                LEFT JOIN giscegis_vertex vertex ON block.vertex = vertex.id
                LEFT JOIN giscegis_blocs_fusiblescaixes_blockname blockname
                    ON block.blockname = blockname.id;
        """, {'srid': config.get('srid', 25830)})

        cursor.execute("""
            CREATE INDEX giscegis_fusibles_caixes_geom_geom
                ON giscegis_fusibles_caixes_geom USING gist (geom)
        """)

        return True

    def _auto_init(self, cursor, context):
        res = super(GiscegisFusiblesCaixesGeom, self)._auto_init(cursor, context)
        return res

    def get_srid(self, cursor, uid):
        cursor.execute("""
        SELECT ST_SRID(geom) AS srid
          FROM giscegis_fusibles_caixes_geom LIMIT 1;
        """)
        data = cursor.fetchall()
        if len(data) > 0:
            return data[0][0]
        else:
            return False

    def recreate(self, cursor, uid):
        cursor.execute("""
        REFRESH MATERIALIZED VIEW giscegis_fusibles_caixes_geom;
        """)
        return True

GiscegisFusiblesCaixesGeom()
