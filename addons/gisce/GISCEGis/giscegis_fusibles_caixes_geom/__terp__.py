# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS fusibles caixes per a Giscegis v3",
    "description": """Vista de fusibles caixes per la versió Giscegis 3""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_base",
        "giscegis_base_geom",
        "giscegis_blocs_fusibles_caixes",
        "giscegis_bt_caixes"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
