# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS v3 (search type)",
    "description": """Mòdul GISCE GIS v3 de tipus de cerca""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscegis_search_type.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
