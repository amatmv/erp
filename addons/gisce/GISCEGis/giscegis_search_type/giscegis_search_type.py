# *-* coding: utf-8 *-*

from osv import osv, fields


class GiscegisSearchType(osv.osv):
    """
    Model of search types for GISCEGIS v3
    """
    _name = "giscegis.search.type"
    _columns = {
        "id": fields.integer("Id"),
        "model_name": fields.text("Model"),
        "search_name": fields.text("Value"),
    }

GiscegisSearchType()
