# -*- coding: utf-8 -*-
{
    "name": "Subestacions index per GIS",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Index per Subestacions per GIS
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "giscedata_cts_subestacions",
        "giscegis_base_index",
        "giscegis_search_type"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscegis_search_type_data.xml"
    ],
    "active": False,
    "installable": True
}
