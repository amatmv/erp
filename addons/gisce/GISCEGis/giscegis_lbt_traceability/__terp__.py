# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS LBT Traceability",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_blocs_elements_bt",
        "giscegis_blocs_ctat",
        "giscedata_transformadors",
        "giscegis_blocs_fusiblesbt",
        "giscegis_blocs_tensio",
        "giscegis_base",
        "giscedata_bt"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "ir.model.access.csv",
        "security/ir.model.access.csv",
        "giscedata_lbt_view.xml"
    ],
    "active": False,
    "installable": True
}
