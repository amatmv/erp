# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from addons import get_module_resource
import base64
import netsvc
from tools import config


class BaseTest(testing.OOTestCase):

    def test_clean(self):
        """
        Tests the clean function

        :return: None
        """
        logger = netsvc.Logger()

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            lbt_trac_model = self.openerp.pool.get('giscegis.lbt.traceability')
            lbt_trac_model.clean(cursor, uid)

    def test_num_lbt_traceability(self):
        """
        Test the number of lines created on the LBT traceability

        :return: None
        """

        self.openerp.install_module('giscegis_network')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            lbt_trac_model = self.openerp.pool.get('giscegis.lbt.traceability')
            net_model = self.openerp.pool.get('giscegis.network')
            ses_mod = self.openerp.pool.get('giscegis.session')
            lbt_model = self.openerp.pool.get('giscedata.bt.element')

            net_id = net_model.search(cursor, uid, [], order="id desc", limit=1)
            if isinstance(net_id, list):
                net_id = net_id[0]

            lbt_trac_model.clean(cursor, uid)
            ses_mod.create_new_network(cursor, uid, "test")

            len_trac = len(lbt_trac_model.search(cursor, uid, []))
            len_lbt = len(lbt_model.search(cursor, uid, []))

            self.assertLessEqual(len_trac, len_lbt)
