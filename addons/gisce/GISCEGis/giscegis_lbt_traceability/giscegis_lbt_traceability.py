# -*- coding: utf-8 -*-
from tqdm import tqdm
from osv import fields, osv
from oorq.decorators import job
import pandapower_erp
import base64
import math
from giscegis_base_geom.giscegis_base_geom import DROP_ON_AUTOCAD_DUMP

# Subscribe this model to the list to be updated if there is an AutoCAD dump
DROP_ON_AUTOCAD_DUMP.append('giscegis_lbt_traceability')


class GiscegisLbtTraceability(osv.osv):
    _name = "giscegis.lbt.traceability"
    _description = "Tracabilitat per les lbt"

    @job(queue="traceability", timeout=3600)
    def fill_from_network_async(self, cursor, uid, network_id, context=None):
        self.fill_from_network(cursor,uid,network_id,context=context)

    def fill_from_network(self, cursor, uid, network_id, context=None):
        """
        Fills giscegis.lbt.traceability with the data of a serialized network

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param network_id: Network id
        :type network_id: int
        :param context: Open ERP context
        :type context: dict
        :return: True if created
        """

        if context is None:
            context = {}

        obj_net = self.pool.get("giscegis.network")
        cts_obj = self.pool.get("giscedata.cts")
        lbt_obj = self.pool.get("giscedata.bt.element")

        self.clean(cursor, uid)

        net_data = obj_net.read(
            cursor, uid, network_id, ["network"], context=context
        )
        if net_data:
            net = pandapower_erp.pp.from_json_string(
                base64.b64decode(net_data["network"])
            )
            te = osv.TransactionExecute(cursor.dbname, uid, self._name)
            df_fields = [
                "name", "trafo_id_erp", "tensio", "sortida", "ct_id_erp"
            ]
            info = "Calculating LBT traceability"
            rows_list = list(
                net.line[net.line.tipus == "bt"][df_fields].iterrows()
            )
            total = len(rows_list)
            for index, row in tqdm(rows_list, total=total, desc=info):
                ct_id = cts_obj.search(
                    cursor, uid, [('name', '=', row['ct_id_erp'])],
                    context=context
                )
                if ct_id:
                    ct_id = ct_id[0]

                if row['name']:
                    edge_id = int(row['name'].split(',')[1])
                    lbt_id = lbt_obj.search(
                        cursor, uid, [('edge_id', '=', edge_id)],
                        context=context
                    )
                    if lbt_id:
                        lbt_id = lbt_id[0]
                else:
                    lbt_id = False

                if row["trafo_id_erp"]:
                    trafo_id = int(row["trafo_id_erp"].split(",")[1])
                else:
                    trafo_id = False

                vals = {
                    "element": lbt_id,
                    "tensio": row["tensio"],
                    "sortida": row["sortida"],
                    "trafo": trafo_id,
                    "ct": ct_id
                }
                te.create(vals, context=context)

        return True

    def clean(self, cr, uid):
        """
        Cleans the giscegis_lbt_traceability

        :param cr: Database cursor
        :param uid: User id
        :return: True if done
        :rtype: bool
        """

        cr.execute("DELETE FROM giscegis_lbt_traceability")
        cr.commit()
        return True

    def create(self, cr, uid, vals, context=None):

        trf_obj = self.pool.get('giscegis.blocs.transformadors')
        bt_obj = self.pool.get('giscedata.bt.element')
        fus_obj = self.pool.get('giscegis.blocs.fusiblesbt')

        # Diccionari per actualitzar el bt_element
        # Tensio del bt element
        bt_element = {}

        # This is for the Qgis behavior and will be used when the create is
        # called from the function fill_from_network
        if 'ct' in vals and vals['ct']:
            bt_element['ct'] = vals['ct']
        if 'sortida' in vals and vals['sortida']:
            bt_element['num_linia'] = vals['sortida']
        if 'tensio' in vals and vals['tensio']:
            if isinstance(vals['tensio'], float) and math.isnan(vals['tensio']):
                vals['tensio'] = 0.0
            bt_element['voltatge'] = int(vals['tensio'])
        if 'element' in vals and vals['element']:
            self.pool.get('giscedata.bt.element').write(
                cr, uid, vals['element'], bt_element
            )

        # This is for the AutoCAD behavior, the keys will only exist if the
        # creation of this element is made throught AutoCAD
        if 'longitud_cad' in vals:
            bt_element['longitud_cad'] = vals['longitud_cad']
            del vals['longitud_cad']

        if 'codi_ct' in vals:
            ct_obj = self.pool.get('giscedata.cts')
            ids = ct_obj.search_reader(cr, uid, [
                ('name', '=', vals['codi_ct'])
            ], ['id_municipi'])
            del vals['codi_ct']
            if ids:
                ct = ids[0]
                vals['ct'] = ct['id']
                if ct['id_municipi']:
                    bt_element['municipi'] = ct['id_municipi']
                # id del ct del bt element
                bt_element['ct'] = vals['ct']

        if 'codi_fusible' in vals:
            ids = fus_obj.search(cr, uid, [('codi', '=', vals['codi_fusible'])])
            del vals['codi_fusible']
            if ids and len(ids):
                vals['fusible'] = ids[0]
                # Llegim la linia del fusible
                bt_element['num_linia'] = fus_obj.browse(cr, uid, vals['fusible']).sortida_bt

        if vals.has_key('codi_elementbt'):
            ids = bt_obj.search(cr, uid, [('name', '=', vals['codi_elementbt'])])
            del vals['codi_elementbt']
            if ids and len(ids):
                vals['element'] = ids[0]
                bt_obj.write(cr, uid, ids, bt_element)

        if vals.has_key('objid_trafo'):
            ids = trf_obj.search(cr, uid, [('name', '=', vals['objid_trafo'])])
            del vals['objid_trafo']
            if ids and len(ids):
                trafo = trf_obj.read(cr, uid, ids[0], ['transformadors'])
                if trafo and trafo['transformadors']:
                    vals['trafo'] = trafo['transformadors'][0]

        return super(osv.osv, self).create(cr, uid, vals)

    _columns = {
        'name': fields.char('Nom', size=10),
        'element': fields.many2one(
            'giscedata.bt.element',
            'Linia BT',
            select="1",
            ondelete='set null'
        ),
        'ct': fields.many2one(
            'giscedata.cts',
            'Centre Transformador',
            select="1",
            ondelete='set null'
        ),
        'trafo': fields.many2one(
            'giscedata.transformador.trafo',
            'Transformador',
            select="1",
            ondelete='set null'
        ),
        'fusible': fields.many2one(
            'giscegis.blocs.fusiblesbt',
            'Fusible',
            select="1",
            ondelete='set null'
        ),
        'tensio': fields.float(
            'Tensió',
            select="1"
        ),
        'start': fields.boolean('Inici Tram'),
        'sortida': fields.char('Sortida BT', size=10),
    }

    _defaults = {
      'start': lambda *a: 0,
    }

    _order = "name, id"


GiscegisLbtTraceability()
