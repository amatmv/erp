# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Linies BT vista 1.7.1",
    "description": """Vista de les linies BT per la versió MG-1.7.1""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscedata_bt",
        "giscegis_lbt_traceability",
        "giscegis_base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "ir.model.access.csv",
        "security/ir.model.access.csv",
        "wizard/giscegis_lbt_tipus_assign_wizard.xml"
    ],
    "active": False,
    "installable": True
}
