# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscegisLbtTipusAssign(osv.osv_memory):

    _name = 'giscegis.lbt.171.tipus.assign'

    def handle_assign(self, cursor, uid, ids, context=None):
        wizard = self.browse(cursor, uid, ids[0], context)
        obj_lbt = self.pool.get('giscedata.bt.element')
        ids = obj_lbt.search(cursor, uid, [], context=context)
        obj_lbtg = self.pool.get('giscegis.lbt.171')
        sm = (wizard.read(['sm'])[0]['sm'] == 1)
        while ids:
            obj_lbtg.set_tipus(cursor, uid, ids[:50], sm)
            ids = ids[50:]
        obj_lbtg.set_tipus(cursor, uid, ids, sm)
        wizard.write({'status': "S'ha engegat un proces en segon pla per assignar el tipus de linia"})
        wizard.write({'state': 'done'})

    _columns = {
        'status': fields.text('Estat'),
        'state': fields.char('State', size=4),
        'sm': fields.boolean('Inclou suports SM*')
    }

    _defaults = {
        'state': lambda *a: 'init',
        'origin_srid': lambda *a: '23030',
        'destination_srid': lambda *a: '23031',
    }

GiscegisLbtTipusAssign()
