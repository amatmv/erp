# *-* coding: utf-8 *-*

from osv import osv, fields
from oorq.decorators import job


class GiscegisLbt171(osv.osv):

    _name = 'giscegis.lbt.171'
    _auto = False
    _columns = {
        'id': fields.integer('Id', readonly=True),
        'texte': fields.text('Texte', readonly=True),
    }

    def init(self, cr):
        cr.execute("""drop view if exists giscegis_lbt_171""")
        cr.execute("""
        create or replace view giscegis_lbt_171 as (
    select lbt.id, bt.name,
    (
      'BT ; ' || coalesce (ct.name, 'S/CT') || ' ; L' || coalesce(fus.sortida_bt, '0') || E'\n' ||
      coalesce(cab.name, 'S/Cable') || ' ; ' || coalesce(mat.name, 'S/Material') || E'\n' ||
      coalesce(round(bt.longitud_cad::numeric,2), 0) || 'm' || E'\n' ||
      coalesce(lbt.tensio, 0) || 'V'
    )  as texte,
    (
      case
      when tipus.codi = 'E' then 'EBT'
      when substring(tlinia.name from 1 for 1) = 'A' then 'L' || coalesce(fus.sortida_bt, '0') || '-A'
      when substring(tlinia.name from 1 for 1) = 'S' then 'L' || coalesce(fus.sortida_bt, '0') || '-S'
      end
    ) as theme
    from giscedata_bt_element bt
    left join giscedata_bt_tipuslinia tlinia on tlinia.id = bt.tipus_linia
    left join giscegis_lbt_traceability lbt on bt.id = lbt.element
    left join giscedata_cts ct on lbt.ct = ct.id
    left join giscedata_bt_cables cab on bt.cable = cab.id
    left join giscedata_bt_tipuscable tipus on cab.tipus = tipus.id
    left join giscedata_bt_material mat on cab.material = mat.id
    left join giscegis_blocs_fusiblesbt fus on lbt.fusible = fus.id

        )
        """)

    @job(queue='gis', timeout='6000')
    def set_tipus(self, cursor, uid, ids, sm, context=None):
        """
            Function that assings the type of line automatically

        :param cursor: Database cursor
        :param uid: User id
        :param ids: Ids to assign
        :param sm: Indicates if have to use check SM supports to set the type
        :param context: OpenERP context
        :return: Number of assigned lbts
        """
        if not ids:
            return 0
        if context is None:
            context = {}
        pending_ids = ids

        obj_tipus = self.pool.get('giscedata.bt.tipuslinia')
        obj_bt = self.pool.get('giscedata.bt.element')

        resultat = {'subterranea': [], 'aerea': [], 'facana': []}

        id_aerea = obj_tipus.search(
            cursor, uid, [('name', '=like', 'A%')])[0]

        sql_sm = """
            WITH
            sup AS (
            SELECT suport.id,
              replace(blockname.name::text, ' '::text, '_'::text) AS symbol,
              st_setsrid(ST_MAKEPOINT(vertex.x,vertex.y),
              (SELECT value
                FROM res_config
                WHERE name = 'giscegis_srid')::int) AS geom
            FROM giscegis_blocs_suports_bt suport
                LEFT JOIN giscegis_vertex vertex ON suport.vertex = vertex.id
                LEFT JOIN giscegis_blocs_suports_bt_blockname blockname
                    ON suport.blockname = blockname.id
            WHERE
              replace(blockname.name::text, ' '::text, '_'::text)
                  LIKE 'PM%%'
              OR replace(blockname.name::text, ' '::text, '_'::text)
                  LIKE 'PF%%'
              OR replace(blockname.name::text, ' '::text, '_'::text)
                  LIKE 'PO%%'
              OR replace(blockname.name::text, ' '::text, '_'::text)
                  LIKE 'JB%%'
              OR replace(blockname.name::text, ' '::text, '_'::text)
                  = 'PONTS_OBERTS'
              OR replace(blockname.name::text, ' '::text, '_'::text) = 'SM'
              OR replace(blockname.name::text, ' '::text, '_'::text) = 'SM1'
            UNION
              SELECT suport.id,
              replace(blockname.name::text, ' '::text, '_'::text) AS symbol,
              st_setsrid(ST_MAKEPOINT(vertex.x,vertex.y),
              (SELECT value
                FROM res_config
                WHERE name = 'giscegis_srid')::int) AS geom
            FROM giscegis_blocs_suports_at suport
                LEFT JOIN giscegis_vertex vertex ON suport.vertex = vertex.id
                LEFT JOIN giscegis_blocs_suports_at_blockname blockname
                    ON suport.blockname = blockname.id
            WHERE
              replace(blockname.name::text, ' '::text, '_'::text)
                  LIKE 'PM%%'
              OR replace(blockname.name::text, ' '::text, '_'::text)
                  LIKE 'PF%%'
              OR replace(blockname.name::text, ' '::text, '_'::text)
                  LIKE 'PO%%'
              OR replace(blockname.name::text, ' '::text, '_'::text)
                  LIKE 'JB%%'
              OR replace(blockname.name::text, ' '::text, '_'::text)
                  = 'PONTS_OBERTS'
              OR replace(blockname.name::text, ' '::text, '_'::text) = 'SM'
              OR replace(blockname.name::text, ' '::text, '_'::text) = 'SM1'
              ),
            lbt AS(
              SELECT bt.id,bt.cable,
                  ST_SETSRID(st_point(v.x, v.y),
                  (SELECT value
                  FROM res_config
                    WHERE name = 'giscegis_srid')::int) AS geom
              FROM giscedata_bt_element bt
                  LEFT JOIN giscegis_edge e ON e.id_linktemplate=bt.name
                  LEFT JOIN giscegis_polyline p ON e.polyline=p.id
                  LEFT JOIN giscegis_polyline_vertex_rel pvr
                      ON pvr.polyline_id=p.id
                  LEFT JOIN giscegis_polyline_vertex pv ON pv.id=pvr.vertex_id
                  LEFT JOIN giscegis_vertex v ON v.id=pv.vertex
                  LEFT JOIN giscedata_bt_cables cab ON bt.cable = cab.id
                  LEFT JOIN giscedata_bt_tipuslinia tlinea
                      ON tlinea.id = bt.tipus_linia
              WHERE  substring(tlinea.name::text, 1, 1) != 'S'
                  AND bt.id in %(ids)s
                  AND e.layer LIKE (
                    SELECT value
                    FROM res_config
                    WHERE name = 'giscegis_btlike_layer'
                  )
              )
              SELECT lbt.id
              FROM lbt,sup
              WHERE ST_BUFFER(sup.geom, 1.1) && lbt.geom
              GROUP BY lbt.id
        """

        sql = """
            WITH
            sup AS (
            SELECT suport.id,
              replace(blockname.name::text, ' '::text, '_'::text) AS symbol,
              st_setsrid(ST_MAKEPOINT(vertex.x,vertex.y),
              (SELECT value
                FROM res_config
                WHERE name = 'giscegis_srid')::int) AS geom
            FROM giscegis_blocs_suports_bt suport
                LEFT JOIN giscegis_vertex vertex ON suport.vertex = vertex.id
                LEFT JOIN giscegis_blocs_suports_bt_blockname blockname
                    ON suport.blockname = blockname.id
            WHERE
              replace(blockname.name::text, ' '::text, '_'::text)
                  LIKE 'PM%%'
              OR replace(blockname.name::text, ' '::text, '_'::text)
                  LIKE 'PF%%'
              OR replace(blockname.name::text, ' '::text, '_'::text)
                  LIKE 'PO%%'
              OR replace(blockname.name::text, ' '::text, '_'::text)
                  LIKE 'JB%%'
              OR replace(blockname.name::text, ' '::text, '_'::text)
                  = 'PONTS_OBERTS'
            UNION
              SELECT suport.id,
              replace(blockname.name::text, ' '::text, '_'::text) AS symbol,
              st_setsrid(ST_MAKEPOINT(vertex.x,vertex.y),
              (SELECT value
                FROM res_config
                WHERE name = 'giscegis_srid')::int) AS geom
            FROM giscegis_blocs_suports_at suport
                LEFT JOIN giscegis_vertex vertex ON suport.vertex = vertex.id
                LEFT JOIN giscegis_blocs_suports_at_blockname blockname
                    ON suport.blockname = blockname.id
            WHERE
              replace(blockname.name::text, ' '::text, '_'::text)
                  LIKE 'PM%%'
              OR replace(blockname.name::text, ' '::text, '_'::text)
                  LIKE 'PF%%'
              OR replace(blockname.name::text, ' '::text, '_'::text)
                  LIKE 'PO%%'
              OR replace(blockname.name::text, ' '::text, '_'::text)
                  LIKE 'JB%%'
              OR replace(blockname.name::text, ' '::text, '_'::text)
                  = 'PONTS_OBERTS'
              ),
            lbt AS(
              SELECT bt.id,bt.cable,
                  ST_SETSRID(st_point(v.x, v.y),
                  (SELECT value
                  FROM res_config
                    WHERE name = 'giscegis_srid')::int) AS geom
              FROM giscedata_bt_element bt
                  LEFT JOIN giscegis_edge e ON e.id_linktemplate=bt.name
                  LEFT JOIN giscegis_polyline p ON e.polyline=p.id
                  LEFT JOIN giscegis_polyline_vertex_rel pvr
                      ON pvr.polyline_id=p.id
                  LEFT JOIN giscegis_polyline_vertex pv ON pv.id=pvr.vertex_id
                  LEFT JOIN giscegis_vertex v ON v.id=pv.vertex
                  LEFT JOIN giscedata_bt_cables cab ON bt.cable = cab.id
                  LEFT JOIN giscedata_bt_tipuslinia tlinea
                      ON tlinea.id = bt.tipus_linia
              WHERE  substring(tlinea.name::text, 1, 1) != 'S'
                  AND bt.id in %(ids)s
                  AND e.layer LIKE(
                    SELECT value
                    FROM res_config
                    WHERE name = 'giscegis_btlike_layer'
                  )
              )
              SELECT lbt.id
              FROM lbt,sup
              WHERE ST_BUFFER(sup.geom, 1.1) && lbt.geom
              GROUP BY lbt.id
        """

        if sm:
            cursor.execute(sql_sm, {'ids': tuple(pending_ids)})
        else:
            cursor.execute(sql, {'ids': tuple(pending_ids)})
        data = cursor.fetchall()

        resultat['aerea'] = [x[0] for x in data]
        pending_ids = list(set(pending_ids) - set(resultat['aerea']))
        data_pending = obj_bt.read(
            cursor, uid, pending_ids, ['tipus_linia'], context=context)

        for linia in data_pending:
            if linia['tipus_linia'][0] == id_aerea:
                resultat['facana'].append(linia['id'])

        obj_bt.write(
            cursor, uid, resultat['aerea'],
            {'posada_facana': False, 'tipus_linia': id_aerea},
            context=context)
        obj_bt.write(
            cursor, uid, resultat['facana'],
            {'posada_facana': True, 'tipus_linia': id_aerea},
            context=context)

        len_aerea = len(resultat['aerea'])
        len_facana = len(resultat['facana'])
        len_subterranea = len(resultat['subterranea'])
        return len_aerea + len_facana + len_subterranea

GiscegisLbt171()


class giscegis_lbt_171_tensio(osv.osv):
    _name = 'giscegis.lbt.171.tensio'
    _auto = False
    _columns = {
        'name': fields.text('Name', readonly=True),
        'texte': fields.text('Texte', readonly=True),
        'theme': fields.text('Theme', readonly=True),
    }

    def init(self, cr):
        cr.execute("""drop view if exists giscegis_lbt_171_tensio""")
        cr.execute("""
        create or replace view giscegis_lbt_171_tensio as (
        SELECT lbt.id, bt.name, ((((((((((((('BT ; '::text || COALESCE(('('::text || bt.name::text) || '); '::text, ''::text)) || COALESCE(ct.name, 'S/CT'::character varying)::text) || ' ; L'::text) || COALESCE(fus.sortida_bt, '0'::character varying)::text) || E'\012'::text) || COALESCE(cab.name, 'S/Cable'::bpchar)::text) || ' ; '::text) || COALESCE(mat.name, 'S/Material'::character varying)::text) || E'\012'::text) || COALESCE(round(bt.longitud_cad::numeric, 2), 0::numeric)::text) || 'm'::text) || E'\012'::text) || COALESCE(lbt.tensio, 0::double precision)::text) || 'V'::text AS texte,
            CASE
                WHEN tipus.codi::text = 'E'::text THEN 'EBT'::text
                WHEN substring(tlinia.name from 1 for 1) = 'A' THEN 'A-'::text || COALESCE(lbt.tensio, 0::double precision)::text
                WHEN substring(tlinia.name from 1 for 1) = 'S' THEN 'S-'::text || COALESCE(lbt.tensio, 0::double precision)::text
                ELSE NULL::text
            END AS theme
       FROM giscedata_bt_element bt
       LEFT JOIN giscedata_bt_tipuslinia tlinia on tlinia.id = bt.tipus_linia
       LEFT JOIN giscegis_lbt_traceability lbt ON bt.id = lbt.element
       LEFT JOIN giscedata_cts ct ON lbt.ct = ct.id
       LEFT JOIN giscedata_bt_cables cab ON bt.cable = cab.id
       LEFT JOIN giscedata_bt_tipuscable tipus ON cab.tipus = tipus.id
       LEFT JOIN giscedata_bt_material mat ON cab.material = mat.id
       LEFT JOIN giscegis_blocs_fusiblesbt fus ON lbt.fusible = fus.id
        )
        """)

giscegis_lbt_171_tensio()

class giscegis_lbt_171_seccio(osv.osv):
    _name = 'giscegis.lbt.171.seccio'
    _auto = False
    _columns = {
        'name': fields.text('Name', readonly=True),
        'id': fields.integer('Id', readonly=True),
        'seccio': fields.text('Seccio', readonly=True),
        'nom_cable': fields.text('Nom Cable', readonly=True),
    }

    def init(self, cr):
        cr.execute("""drop view if exists giscegis_lbt_171_seccio""")
        cr.execute("""
        create or replace view giscegis_lbt_171_seccio as (
        SELECT e.id,e.name,
        CASE
            WHEN t.codi::text = 'I'::text THEN 0
            ELSE 1
        END AS seccio,
        c.name AS nom_cable,
        t.codi::text = 'I'::text
        FROM giscedata_bt_element e
        LEFT JOIN giscedata_bt_cables c ON c.id = e.cable
        LEFT JOIN giscedata_bt_tipuscable t ON t.id = c.tipus
        )
        """)

giscegis_lbt_171_seccio()
