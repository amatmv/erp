from osv import osv, fields


class GiscegisDefectesBTGeom(osv.osv):
    """
    Defects BT per a Giscegis
    """
    _name = 'giscegis.defectes.bt.geom'
    _description = 'Defectes BT per a Giscegis'

    def init(self, cursor):
        """
        Initializes the model

        :param cursor: Database cursor
        :return: If the is initialized
        :rtype: bool
        """
        conf_obj = self.pool.get("res.config")
        srid = int(conf_obj.get(cursor, 1, "giscegis_srid", "25831"))
        sql_has_geom = """
            SELECT * FROM information_schema.columns
            WHERE table_name = 'giscegis_defectes_bt_geom'
            AND COLUMN_NAME = 'geom';"""
        cursor.execute(sql_has_geom)
        if not cursor.fetchone():
            sql_add_geom = """SELECT AddGeometryColumn(
            'public','giscegis_defectes_bt_geom',
            'geom',%(srid)s,'geometry',2, true);"""
            cursor.execute(sql_add_geom, {'srid': srid})
        return True

    def _coords(self, cursor, uid, ids, field_name, arg, context):
        """
        Returns the X and Y of the defecte

        :param cursor: Database cursor
        :param uid: User id
        :param ids: Ids of defectes
        :param field_name:
        :param arg: Arguents
        :param context: OpenERP Context
        :return: X and Y of the defecte
        :rtype: dict
        """
        ret = {}
        sql = """
        SELECT id, ST_X(geom) ,ST_Y(geom)
        FROM giscegis_defectes_bt_geom
        WHERE id IN %s
        """
        cursor.execute(sql, (tuple(ids),))
        data = cursor.fetchall()
        for line in data:
            ret[line[0]] = {"x": line[1], "y": line[2]}
            ids.remove(line[0])
        for identifier in ids:
            ret[identifier] = {"x": None, "y": None}
        return ret

    _columns = {
        "id_defecte": fields.many2one(
            "giscedata.revisions.bt.defectes",
            "Defecte",
            required=True
        ),
        "codi_defecte": fields.text("Codi defecte"),
        "x": fields.function(_coords, method=True, type="float", string="x", multi="coords"),
        "y": fields.function(_coords, method=True, type="float", string="y", multi="coords"),
    }

GiscegisDefectesBTGeom()
