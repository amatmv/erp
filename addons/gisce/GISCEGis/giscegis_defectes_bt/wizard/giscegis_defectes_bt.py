# -*- coding: utf-8 -*-
import base64
from StringIO import StringIO
from zipfile import ZipFile, BadZipfile
import os
import itertools

from tools.translate import _
from osv import osv, fields
import shapefile
import netsvc
from tools.misc import email_send

class GiscegisDefectesBTLoader(osv.osv_memory):

    _name = "giscegis.defectes.bt.loader"

    def import_data_shp(self, cursor, uid, ids, context=None):
        """
        Attends the load of the shapes of defectes BT

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: Wizard id
        :type ids: int
        :param context: OpenERP context
        :type context: dict
        :return: None
        :rtype: None
        """

        obj_users = self.pool.get("res.users")
        obj_msg = self.pool.get("giscegis.load.message")
        obj_config = self.pool.get("res.config")
        base_geom = self.pool.get('giscegis.base.geom')

        from_email = obj_config.get(
            cursor, uid,
            "email_from", "no-reply@gisce.net"
        )

        not_found_message = _(u"No s'han trobat fitxers .shp o .dbf en el fitxer,s'atura la importació")
        wizard = self.browse(cursor, uid, ids[0], context)
        wizard.write({'status': ''})
        logger = netsvc.Logger()

        message_id = obj_msg.create(cursor, uid, {})
        message = obj_msg.browse(cursor, uid, message_id)
        message.start_load()

        user_data = obj_users.browse(cursor, uid, uid)
        user_email = user_data.address_id.email

        email_send(
            from_email,
            [user_email],
            _("Carrega de fitxers defectes BT"),
            message.get_message(),
            debug=True
        )

        if wizard.file_defectes:
            data = base64.decodestring(wizard.file_defectes)
        else:
            logger.notifyChannel(
                'SHPLoader', netsvc.LOG_INFO,
                _(u"No s'ha especificat el fitxer de defectes"))

            message.log(_(u"No s'ha especificat el fitxer de defectes"))
            message.end_load(_("Error"))

            wizard = self.browse(cursor, uid, ids[0], None)
            base_geom.add_status(
                cursor, uid, ids,
                _(u"No s'ha especificat el fitxer de defectes"),
                wizard)
        if wizard.file_defectes:
            s = StringIO(data)
            try:
                input_zip = ZipFile(s)
            except BadZipfile:
                logger.notifyChannel(
                    'SHPLoader', netsvc.LOG_WARNING,
                    _('Fitxer de defectes'))
                wizard = self.browse(cursor, uid, ids[0], None)

                message.log_message(_('Fitxer de defectes'))
                message.end_load(_("Error"))

                base_geom.add_status(
                    cursor, uid, ids, _('Fitxer de derectes corrupte'),
                    wizard)
                return
            filenames = sorted(input_zip.namelist())
            if filenames:
                base_geom.truncate_table(cursor, uid, "giscegis_defectes_bt_geom")
                shp_data = None
                dbf_data = None
                for filename in filenames:
                    if os.path.splitext(filename)[1].lower() == ".dbf":
                        dbf_data = StringIO(input_zip.read(filename))
                    if os.path.splitext(filename)[1].lower() == ".shp":
                        shp_data = StringIO(input_zip.read(filename))
                    if shp_data and dbf_data:
                        fields_desc = {
                            "defecte": "int",
                            "codidefect": "text"
                        }
                        fields_alias = {
                            "defecte": "id_defecte",
                            "codidefect": "codi_defecte"
                        }
                        base_geom.load_shp(
                            cursor, uid, "giscegis_defectes_bt_geom", shp_data,
                            dbf_data, fields_desc, fields_alias=fields_alias,
                            ignore_id=True, end_function=None)
                        shp_data = None
                        dbf_data = None

            else:
                logger.notifyChannel(
                    'SHPLoader', netsvc.LOG_WARNING, not_found_message)
                wizard = self.browse(cursor, uid, ids[0], None)
                base_geom.add_status(
                    cursor, uid, ids, not_found_message,
                    wizard)
            logger.notifyChannel(
                'SHPLoader', netsvc.LOG_INFO,
                _('Dades carregades correctament'))

            message.log_message(_('Dades carregades correctament'))
            message.end_load(_("Correcte"))

            wizard = self.browse(cursor, uid, ids[0], None)
            base_geom.add_status(
                cursor, uid, ids,
                _('Dades carregades correctament'),
                wizard)
            
            email_send(
                from_email,
                [user_email],
                _("Carrega de fitxers defectes BT"),
                message.get_message(),
                debug = True
            )

    _columns = {
        'file_defectes': fields.binary('Fixer de defectes', help=()),
        'status': fields.text(_('Resultat'))
    }
    _defaults = {
        'state': lambda *a: 'init'
    }

GiscegisDefectesBTLoader()
