# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Defectes BT",
    "description": """Modul defectes BT GIS v3""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_base_geom",
        "giscedata_revisions_bt"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/ir.model.access.csv",
        "giscegis_layers_data.xml",
        "wizard/giscegis_defectes_bt.xml"
    ],
    "active": False,
    "installable": True
}
