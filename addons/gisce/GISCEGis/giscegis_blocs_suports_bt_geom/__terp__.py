# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Blocs Suports BT per GISCEGIS v3",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_base_geom",
        "giscedata_bt",
        "giscegis_base",
        "giscegis_blocs_tensio",
        "giscegis_blocs_suports_bt",
        "giscegis_bt"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
