# *-* coding: utf-8 *-*

from osv import osv, fields
from tools import config


class GiscegisBlocsSuportsBtGeom(osv.osv):
    _name = 'giscegis.blocs.suports.bt.geom'
    _auto = False
    _columns = {
        'id': fields.integer('Id', readonly=True),
        'texte': fields.text('Texte', readonly=True),
        'geom': fields.text('Geom', readonly=True),
        'numero': fields.integer('Numero', readonly=True),
        'blockname': fields.text('Blockname', readonly=True),
        'rotation': fields.integer('Rotation', readonly=True),
        "materialize_date": fields.datetime("Fecha creación vista"),
    }

    def init(self, cursor):
        """
        Creates materialized view of giscegis_blocs_suports_bt_geom

        :param cursor: Database cursor
        :return: True
        :rtype: bool
        """

        cursor.execute("""
            DROP MATERIALIZED VIEW IF EXISTS giscegis_blocs_suports_bt_geom;
        
            CREATE MATERIALIZED VIEW giscegis_blocs_suports_bt_geom AS 
                SELECT
                    block.id::int AS id,
                    ST_SETSRID(
                        ST_MAKEPOINT(vertex.x,vertex.y), %(srid)s
                    ) AS geom,
                    blockname.name || '-' 
                    || COALESCE(
                        block.numsuport3, block.numsuport2, block.numsuport1,
                        block.numsuport, ' '
                    )::text AS texte,
                    blockname.name AS blockname,
                    block.rotation AS rotation,
                    blockname.name AS symbol,
                    now() AS materialize_date
                FROM giscegis_blocs_suports_bt AS block
                LEFT JOIN giscegis_vertex vertex ON block.vertex = vertex.id
                LEFT JOIN giscegis_blocs_suports_bt_blockname AS blockname ON  
                    block.blockname = blockname.id;
        """, {'srid': config.get('srid', 25830)})

        return True

    def _auto_init(self, cursor, context):
        s = super(GiscegisBlocsSuportsBtGeom, self)
        return s._auto_init(cursor, context)

    def get_srid(self, cursor, uid):
        """
        :param cursor: cursor Database cursor
        :param uid: integer User identifier
        :return: Returns a list of srids in geom field
        """

        return config.get('srid', 25830)

    def recreate(self, cursor, uid):
        cursor.execute("""
            REFRESH MATERIALIZED VIEW giscegis_blocs_suports_bt_geom;
        """)
        return True


GiscegisBlocsSuportsBtGeom()
