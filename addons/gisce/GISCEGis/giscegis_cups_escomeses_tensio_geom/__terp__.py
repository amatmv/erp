# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS CUPS Escomeses tensio",
    "description": """Modul cups escomeses tensio per GIS v3""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscedata_cts",
        "giscedata_polissa",
        "giscedata_transformadors",
        "giscedata_expedients",
        "giscegis_base",
        "giscegis_cups",
        "giscegis_escomeses_traceability",
        "giscegis_base_geom",
        "giscegis_blocs_escomeses",
        "giscegis_colors"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
