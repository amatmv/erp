# -*- coding: utf-8 -*-

from osv import fields, osv
from tools import config


class GiscegisCupsEscomesesTensioGeom(osv.osv):
    _name = "giscegis.cups.escomeses.tensio.geom"
    _description = "Modul per cups escomeses tensio per Giscegis v3"
    _auto = False
    _columns = {
        'id': fields.integer('Id', readonly=True),
        'name': fields.char('Name', readonly=True, size=256),
        'blockname': fields.char('Blockname', readonly=True, size=256),
        'texte': fields.char('Texte', readonly=True, size=256),
        'ct': fields.char('Ct', readonly=True, size=256),
        'order_tr': fields.integer('OrderTR', readonly=True),
        'sortida': fields.char('Sortida', readonly=True, size=256),
        'geom': fields.char('Sortida', readonly=True, size=256),
        'rotation': fields.float('Rotation', readonly=True),
        'symbol': fields.char('Symbol', readonly=True, size=256),
        'sum': fields.integer('Sum', readonly=True),
        'count': fields.integer('Count', readonly=True),
        "materialize_date": fields.datetime("Fecha creación vista"),
    }

    def init(self, cursor):
        cursor.execute("""
            DROP MATERIALIZED VIEW IF EXISTS
                public.giscegis_cups_escomeses_tensio_geom;
        """)
        cursor.execute("""
            CREATE MATERIALIZED VIEW  giscegis_cups_escomeses_tensio_geom AS
                SELECT
                    block.escomesa AS id,
                    esco.name::integer AS name,
                    blockname.name || ';' 
                    || COALESCE(trace.tensio::text, ''::text) AS blockname,
                    blockname.name || ' ; ' || esco.name || E'\n' 
                    || COALESCE(ct.name, 'S/CT') || ' ; ' 
                    || COALESCE(trafo.ordre_dins_ct::text, 'S/T') || ' ; ' 
                    || COALESCE(fus.sortida_bt::text, 'S/S') || E'\n' || 'CUPS: ' 
                    || COALESCE(count(cups.id_escomesa), 0) || E'\nPcont: ' 
                    || COALESCE(round(sum(pol.potencia), 2), 0) 
                    || E' kW\nPconv: '
                    || COALESCE(round(sum(cups.potencia_conveni), 2), 0) 
                    || ' kW' AS texte,
                    ct.name AS ct,
                    trafo.ordre_dins_ct AS ordre_tr,
                    trace.sortida AS sortida,
                    ST_SETSRID(ST_MAKEPOINT(v.x,v.y), %(srid)s) AS geom,
                    block.rotation AS rotation,
                    blockname.name AS symbol,
                    SUM(pol.potencia) AS sum, 
                    count(cups.id_escomesa) AS count,
                    CASE 
                        WHEN colors_bt.color IS NOT NULL then colors_bt.color
                        ELSE ''
                    END AS color,
                    now() AS materialize_date
                FROM giscegis_blocs_escomeses block
                INNER JOIN giscedata_cups_escomesa esco ON 
                    esco.id = block.escomesa
                LEFT JOIN giscegis_escomeses_traceability trace ON
                    block.escomesa = trace.escomesa
                LEFT JOIN giscedata_cts ct ON trace.ct = ct.id
                LEFT JOIN giscedata_transformador_trafo trafo ON 
                    trace.trafo = trafo.id
                LEFT JOIN giscedata_bt_quadre_element fus ON 
                    trace.fusible = fus.id
                LEFT JOIN giscedata_cups_blockname blockname ON 
                    block.blockname = blockname.id
                LEFT JOIN giscegis_vertex v ON block.vertex = v.id
                LEFT JOIN giscedata_cups_ps cups ON 
                    esco.id = cups.id_escomesa AND cups.active
                LEFT JOIN giscedata_polissa pol ON 
                    cups.id = pol.cups AND pol.active = true
                LEFT JOIN giscegis_colors_bt colors_bt ON 
                    colors_bt."name"::text = fus.sortida_bt::text
                GROUP BY esco.id, esco.name, blockname.name, ct.name, 
                    trafo.ordre_dins_ct, fus.sortida_bt, trace.sortida, 
                    v.x, v.y, block.rotation, trace.tensio, colors_bt.color, 
                    block.id
            WITH DATA;
        """, {'srid': config.get('srid', 25830)})

        cursor.execute("""
            CREATE INDEX giscegis_cups_escomeses_tensio_geom_geom
            ON giscegis_cups_escomeses_tensio_geom USING gist (geom);
        """)

    def recreate(self, cursor, uid):
        cursor.execute("""
            REFRESH MATERIALIZED VIEW giscegis_cups_escomeses_tensio_geom;
        """)


GiscegisCupsEscomesesTensioGeom()


class GiscegisColorsBt(osv.osv):
    _name = "giscegis.colors.bt"
    _inherit = "giscegis.colors.bt"

    def create(self, cr, uid, vals, context=None):
        """
        Overwrite create method, now adds extra feature like:
         removes tms cache for bt layer
         refresh vm giscegis_cups_escomeses_tensio_geom
        :param cr: db cursor
        :type cursor
        :param uid: user id
        :type int
        :param vals: {'color':'hex_value'}
        :type dict
        :param context:
        :return: result of create method
        :rtype bool
        """
        sortida = self.check_hashtag(vals)
        ret = super(GiscegisColorsBt, self).create(cr, uid, sortida, context)
        # Removes tms cache layer BT
        self.pool.get("giscegis.base.geom").remove_tms_cache(cr, uid, self._LAYER_BT)
        # Refresh VM giscegis_cups_escomeses_tensio_geom
        self.pool.get("giscegis.cups.escomeses.tensio.geom").recreate(cr, uid)
        return ret

    def unlink(self, cr, uid, ids, context=None):
        """
        Overwrite unlink method, now adds extra feature like:
         removes tms cache for bt layer
         refresh vm giscegis_cups_escomeses_tensio_geom
        :param cr: db cursor
        :type cursor
        :param uid: user id
        :type int
        :param ids:
        :param context:
        :return: result of unlink method
        :rtype bool
        """
        ret = super(GiscegisColorsBt, self).unlink(cr, uid, ids)
        # Removes tms cache layer BT
        self.pool.get("giscegis.base.geom").remove_tms_cache(cr, uid, self._LAYER_BT)
        # Refresh VM giscegis.cups.escomeses.tensio.geom
        self.pool.get("giscegis.cups.escomeses.tensio.geom").recreate(cr, uid)
        return ret

    def write(self, cr, uid, ids, values, context=None):
        """
        Overwrite writemethod, now adds extra feature like:
         removes tms cache for bt layer
         refresh vm giscegis_cups_escomeses_tensio_geom
        :param cr: db cursor
        :type cr: cursor
        :param uid: user id
        :type int
        :param ids: sortida value
        :type list
        :param values: {'color':'hex_value'}
        :type dict
        :param context:
        :return: result of write method
        :rtype bool
        """
        sortida = self.check_hashtag(values)
        ret = super(GiscegisColorsBt, self).write(cr, uid, ids, sortida)
        # Removes tms cache layer BT
        self.pool.get("giscegis.base.geom").remove_tms_cache(cr, uid, self._LAYER_BT)
        # Refresh VM giscegis.cups.escomeses.tensio.geom
        self.pool.get("giscegis.cups.escomeses.tensio.geom").recreate(cr, uid)
        return ret

    def check_hashtag(self, sortida):
        """
        Check if hexadecimal color value has #
        :param sortida: dictionary with color and hex value
        :return dict
        :return: sortida
        :rtype dict
        """
        if sortida.get('color'):
            color = sortida.get('color')
            if len(color) > 0:
                if color[0] is not '#':
                    sortida['color'] = '#' + color
        return sortida


GiscegisColorsBt()
