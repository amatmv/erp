# -*- coding: utf-8 -*-
from osv import osv, fields
from giscegis_base_geom.giscegis_base_geom import DROP_ON_AUTOCAD_DUMP

# Subscribe this model to the list to be updated if there is an AutoCAD dump
DROP_ON_AUTOCAD_DUMP.append('giscegis_blocs_locals')


class giscegis_blocs_locals_blockname(osv.osv):

    _name = 'giscegis.blocs.locals.blockname'
    _description = 'Tipus Blocs CTAT'

    def create(self, cr, uid, vals, context={}):
        ids = self.search(cr, uid, [('name', '=', vals['name'])])
        if ids and len(ids):
            return ids[0]
        else:
            return super(osv.osv, self).create(cr, uid, vals)

    _columns = {
      'name': fields.char('BlockName',size=20,required=True),
      'description': fields.char('Descripció',size=255),
    }

    _defaults = {

    }

    _order = "name, id"

giscegis_blocs_locals_blockname()

class giscegis_blocs_locals(osv.osv):

    _name = 'giscegis.blocs.locals'
    _description = 'Blocs Locals'
    _columns = {
      'name': fields.char('ObjectId', size=50),
      'codi': fields.char('Nom', size=50, required=True),
      'vertex': fields.many2one('giscegis.vertex', 'Vertex'),
      'width': fields.float('Width'),
      'height': fields.float('Height'),
      'rotation': fields.float('Rotation'),
      'blockname': fields.many2one('giscegis.blocs.locals.blockname', 
                                   'BlockName'),
      'node': fields.many2one('giscegis.nodes', 'Node', ondelete='set null'),
    }

    _defaults = {

    }

    _order = "name, id"

    def create(self, cursor, uid, vals, context=None):
        """Mètode sobreescrit per escriure als camps many2one des
        d'aquí.

        Ens hem d'assegurar de què els mètodes create() dels objectes
        referenciats estiguin alhora sobreescrits per tal què comprovin
        si l'objecte existeix o no."""
        _fields = [('vertex', 'giscegis.vertex'),
                   ('blockname', 'giscegis.blocs.locals.blockname')]
        for field in _fields:
            if field[0] in vals and isinstance(vals[field[0]], list):
                obj = self.pool.get(field[1])
                remote_id = obj.create(cursor, uid, vals[field[0]][2])
                if remote_id:
                    vals[field[0]] = remote_id
        return super(osv.osv, self).create(cursor, uid, vals)

giscegis_blocs_locals()


class giscegis_blocs_locals_171(osv.osv):
    _name = 'giscegis.blocs.locals.171'
    _description = 'Vista pels blocs LOCAL'
    _columns = {}
    _auto = False

    def init(self, cr):
        cr.execute("""DROP VIEW IF EXISTS giscegis_blocs_locals_171""")
        cr.execute("""CREATE OR REPLACE VIEW giscegis_blocs_locals_171 AS (
          SELECT l.id, l.codi, v.x, v.y, l.rotation FROM giscegis_blocs_locals l
          LEFT JOIN giscegis_vertex v ON (l.vertex = v.id)
        )""")


giscegis_blocs_locals_171()
