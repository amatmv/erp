# *-* coding: utf-8 *-*

from osv import osv,fields

class giscegis_blocs_suports(osv.osv):

    _name = 'giscegis.blocs.suports'
    _auto = False

    def create(self, cr, uid, vals, context={}):
        if vals.has_key('type') and vals['type'] == '2':
            # Creem el suport BT
            del vals['type']
            return self.pool.get('giscegis.blocs.suports.bt').create(cr, uid,
                                                                  vals, context)
        elif vals.has_key('type') and vals['type'] == '1':
            # Creem el suport AT
            del vals['type']
            return self.pool.get('giscegis.blocs.suports.at').create(cr, uid,
                                                                  vals, context)
        else:
            # Per defecte el crearem AT si no el troba
            vals['type'] = '1'
            self.create(cr, uid, vals, context)

giscegis_blocs_suports()
