# *-* coding: utf-8 *-*

from osv import osv, fields

class giscegis_blocs_suports_at_171(osv.osv):

    _name = 'giscegis.blocs.suports.at.171'
    _auto = False
    _columns = {
      'x': fields.float('X', readonly=True),
      'y': fields.float('Y', readonly=True),
      'texte': fields.text('Texte', readonly=True),
      'blockname': fields.char('Blockname', size=50),
      'rotation': fields.float('Rotation'),
    }

    def init(self, cr):
        cr.execute("""drop view if exists giscegis_blocs_suports_at_171""")
        cr.execute("""
          create or replace view giscegis_blocs_suports_at_171 as (
          SELECT suport.id, vertex.x, vertex.y,
          ((((((blockname.name::text || ' '::text) || COALESCE(suport.numsuport, ' '::character varying)::text) || E'\012'::text) || COALESCE(poste.altura, 0::double precision)::text) || 'm ; '::text) || COALESCE(poste.esforc, 0)::text) || 'kg'::text || (E'\012x:'::text || (vertex.x::int)::text) || (E'\012y:'::text || (vertex.y::int)::text) AS texte,
          blockname.name AS blockname,
          "substring"(suport.numsuport::text, "position"(suport.numsuport::text, '-'::text) + 1) AS numero,
          suport.rotation
          FROM giscegis_blocs_suports_at suport
          LEFT JOIN giscedata_at_suport suports ON suport.numsuport = suports.name and suports.active
          LEFT JOIN giscedata_at_poste poste ON suports.poste = poste.id
          LEFT JOIN giscegis_vertex vertex ON suport.vertex = vertex.id
          LEFT JOIN giscegis_blocs_suports_at_blockname blockname ON suport.blockname = blockname.id)
        """)

giscegis_blocs_suports_at_171()
