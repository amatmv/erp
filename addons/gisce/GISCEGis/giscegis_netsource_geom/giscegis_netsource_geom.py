# *-* coding: utf-8 *-*

from osv import fields, osv
from tools import config


class GiscegisNetsourceGeom(osv.osv):
    _name = 'giscegis.netsource.geom'
    _auto = False
    _columns = {
        'id': fields.integer('Id', readonly=True),
        'name': fields.integer('Id', readonly=True),
        'rotation': fields.float('Rotation', readonly=True),
        'geom': fields.char('Texte', readonly=True, size=256),
        "materialize_date": fields.datetime("Fecha creación vista"),
    }

    def init(self, cursor):

        cursor.execute("""
            DROP MATERIALIZED VIEW IF EXISTS giscegis_netsource_geom;
        """)

        cursor.execute("""
            CREATE MATERIALIZED VIEW public.giscegis_netsource_geom AS
                SELECT 
                    block.id AS id,
                    block.name AS name,
                    block.rotation AS rotation,
                    st_setsrid(
                        st_makepoint(vertex.x, vertex.y), %(srid)s
                    ) AS geom,
                    now() AS materialize_date
                FROM giscegis_blocs_netsource AS block
                LEFT JOIN giscegis_vertex vertex ON block.vertex = vertex.id;
        """, {'srid': config.get('srid', 25830)})

        cursor.execute("""
            CREATE INDEX giscegis_netsource_geom_geom
            ON giscegis_netsource_geom USING gist (geom);
        """)

        return True

    def _auto_init(self, cursor, context):
        res = super(GiscegisNetsourceGeom, self)._auto_init(cursor, context)
        return res

    def get_srid(self, cursor, uid):
        """
        :param cursor: cursor Database cursor
        :param uid: integer User identifier
        :return: Returns a list of srids in geom field
        """

        return [config.get('srid', 25830)]

    def recreate(self, cursor, uid):

        cursor.execute("""
            REFRESH MATERIALIZED VIEW giscegis_netsource_geom;
        """)

        return True


GiscegisNetsourceGeom()
