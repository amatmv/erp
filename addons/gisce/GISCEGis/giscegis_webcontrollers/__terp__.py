# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Web Controllers",
    "description": """Classes per servir els controladors del tomcat""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscedata_polissa",
        "giscedata_polissa_distri",
        "giscegis_base",
        "giscegis_edge"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "ir.model.access.csv",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
