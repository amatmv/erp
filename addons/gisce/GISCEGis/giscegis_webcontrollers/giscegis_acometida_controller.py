# -*- coding: utf-8 -*-
"""Modul giscegis_acometida_controller."""

try:
    import json
except ImportError:
    import simplejson as json
from osv import fields, osv

class GiscegisAcometidaController(osv.osv):
    """Executa les funcions del controlador d'escomeses del tomcat."""
    _name = "giscegis.acometida.controller"
    _description = __doc__
    _auto = False
    
    def search_acometida(self, cursor, uid, s_string, exacta):
        """Mètode públic
        String search_acometida(String searchstring, boolean exact_match)
        """
        
        res = {'total': 0, 'results': []}
        escomeses_obj = self.pool.get('giscedata.cups.escomesa')
        gescomeses_obj = self.pool.get('giscegis.blocs.escomeses')
        if exacta:
            oper = '='
        else:
            oper = 'ilike'
        escomeses = escomeses_obj.search(cursor, uid, [('name', oper, 
                                                        s_string)])
        if escomeses:
            for escomesa in escomeses_obj.browse(cursor, uid, escomeses):
                esc = {}
                gescomesa_id = gescomeses_obj.search(cursor, uid,
                                               [('escomesa', '=', escomesa.id)])
                esc['id'] = escomesa.id
                esc['name']= escomesa.name
                if gescomesa_id:
                    gescomesa = gescomeses_obj.browse(cursor, uid, 
                                                      gescomesa_id[0])
                    esc['x'] = gescomesa.vertex.x
                    esc['y'] = gescomesa.vertex.y
                else:
                    esc['x'] = 0
                    esc['y'] = 0
                res['results'].append(esc)
        res['total'] = len(res['results'])
        return json.dumps(res)
    
    def locate_acometida(self, cursor, uid, acometida_id):
        """Mètode públic
        String locate_acometida(String acometida_id)
        """
        
        res = {'total': 0, 'results': []}
        
        escomeses_obj = self.pool.get('giscedata.cups.escomesa')
        gescomeses_obj = self.pool.get('giscegis.blocs.escomeses')

        escomeses = escomeses_obj.search(cursor, uid, [('id', '=', 
                                                        acometida_id)])
        if escomeses:
            for escomesa in escomeses_obj.browse(cursor, uid, escomeses):
                esc = {}
                gescomesa_id = gescomeses_obj.search(cursor, uid,
                                               [('escomesa', '=', escomesa.id)])
                esc['id'] = escomesa.id
                esc['name']= escomesa.name
                if gescomesa_id:
                    gescomesa = gescomeses_obj.browse(cursor, uid, 
                                                      gescomesa_id[0])
                    esc['x'] = gescomesa.vertex.x
                    esc['y'] = gescomesa.vertex.y
                else:
                    esc['x'] = 0
                    esc['y'] = 0
                res['results'].append(esc)
        res['total'] = len(res['results'])
        return json.dumps(res)
    
    def list_abonats_acometidas_new(self, cursor, uid, simulation_results_id,
                                    xarxa):
        """Mètode
        String list_abonats_acometidas_new(String simulation_results_id, 
                                           String xarxa)
        """
        res = {'total': 0, 'results': []}
        simulation_results_id = int(simulation_results_id)
        model = 'giscegis.simulacio.%s' % (xarxa,)
        sim_obj = self.pool.get(model)
        abonats = sim_obj.get_abonats_off(cursor, uid, simulation_results_id)
        for abonat in abonats:
            if abonat['x'] != 0 and abonat['y'] != 0:
                abonat['posicionar'] = "<a href='#' onClick='javascript:zoom2()'>Zoom</a>"
            abonat['detall'] = "<a href='#' onClick='javascript:detall2()'>Fitxa</a>"
            res['results'].append(abonat)
        res['total'] = len(res['results'])
        return json.dumps(res)
    
    def list_abonats_acometidas(self, cursor, uid, escomeses_ids):
        """Mètode
        String list_abonats_acometidas(String escomesesids)
        """
        # patch
        if isinstance(escomeses_ids, str):
            escomeses_ids = json.loads(escomeses_ids)

        return self.list_abonats_acometidas_2(cursor, uid, escomeses_ids, 50)
        """
        res = {'total': 0, 'results': []}
        acometida_obj = self.pool.get('giscedata.cups.escomesa')
        abonats = acometida_obj.get_abonats(cursor, uid, escomeses_ids)
        for abonat in abonats:
            if abonat['x'] != 0 and abonat['y'] != 0:
                abonat['posicionar'] = "<a href='#' onClick='javascript:zoom2()'>Zoom</a>"
            abonat['detall'] = "<a href='#' onClick='javascript:detall2()'>Fitxa</a>"
            res['results'].append(abonat)
        res['total'] = len(res['results'])
        return json.dumps(res)
        """
        
    def list_abonats_acometidas_2(self, cursor, uid, ids, limit=None):
        """Mètode
        String list_abonats_acometidas_2(String acometidaids)
        """
        res = {'total': 0, 'results': []}
        ESTATS_POLISSA = ['esborrany', 'pendent', 'tall', 'baixa']
        # pedaç
        if not ids:
            ids = [-1]
        
        if ids == -1:
            return json.dumps(res)
        
        # limit the number of results
        if limit:
            if limit > len(ids):
                limit = len(ids)
        else:
            limit = len(ids)
        ids = ids[:limit]
        
        # TODO mirar de canviar a giscedata_polissa o ampliar giscegis_polises_171
        cups_obj = self.pool.get('giscedata.cups.ps')
        polissa_obj = self.pool.get('giscedata.polissa')
        gpolissa_obj = self.pool.get('giscegis.polisses.171')
        cups_ids = cups_obj.search(cursor, uid, [('id_escomesa', 'in', ids)])
        for cups in cups_obj.browse(cursor, uid, cups_ids):
            # obtenir pòlissa activa associada, si en té
            search_params = [('cups', '=', cups.id),
                             ('state', 'not in', ESTATS_POLISSA)]
            polissa_id = polissa_obj.search(cursor, uid, search_params)
            if polissa_id:
                polissa = polissa_obj.browse(cursor, uid, polissa_id)[0]
                pol = {}
                pol['carrer'] = cups.nv
                pol['escala'] = cups.es
                comptadors = polissa.comptadors_actius(polissa.data_alta)
                if comptadors:
                    compt_obj = self.pool.get('giscedata.lectures.comptador')
                    pol['n_comptador'] = compt_obj.read(cursor, uid, 
                                                        [comptadors[0]], 
                                                        ['name'])[0]['name']
                else:
                    pol['n_comptador'] = '0000000'
                pol['escomesa'] = cups.id_escomesa.name
                pol['tensio'] = polissa.tensio
                pol['titular'] = polissa.titular.name
                pol['et'] = polissa.et
                pol['linia'] = polissa.linia
                pol['polissa'] = polissa.name
                pol['numero'] = cups.pnp
                pol['potencia'] = polissa.potencia
                pol['planta'] = cups.pt
                pol['pis'] = cups.pu
                gpolissa_id = gpolissa_obj.search(cursor, uid,
                                                  [('name', '=', polissa.name)])
                if gpolissa_id:
                    gpolissa = gpolissa_obj.browse(cursor, uid, gpolissa_id[0])

                    if gpolissa.x != 0 and gpolissa.y != 0:
                        pol['x'] = gpolissa.x
                        pol['y'] = gpolissa.y
                        pol['posicionar'] = "<a href='#' onClick='javascript:zoom2()'>Zoom</a>"
                pol['detall'] = "<a href='#' onClick='javascript:detall2()'>Fitxa</a>"

                res['results'].append(pol)

        res['total'] = len(res['results'])
        return json.dumps(res)


GiscegisAcometidaController()
