# -*- coding: utf-8 -*-
"""Modul giscegis_btelement_controller."""

try:
    import json
except ImportError:
    import simplejson as json
from osv import fields, osv

class GiscegisBTelementController(osv.osv):
    """Executa les funcions del controlador de elements BT del tomcat."""
    _name = "giscegis.btelement.controller"
    _description = __doc__
    _auto = False
    
    def search_btelement(self, cursor, uid, name, context=None):
        """Mètode public String search_btelement(String parameter)
        del tomcat
        """
        res = {'total': 0, 'results': []}


        edge_obj = self.pool.get('giscegis.edge')
        conf_obj = self.pool.get('res.config')
        layer_name_template = conf_obj.get(cursor, uid,
                                           'webcontroller_bt_layer_template',
                                           'BT')

        search_vals = [('id_linktemplate', '=', name),
                       ('layer', 'ilike', layer_name_template)]
        e_ids = edge_obj.search(cursor, uid, search_vals)
        if e_ids:
            edge = edge_obj.browse(cursor, uid, e_ids[0])

            edge_vals = {}

            edge_vals['id'] = edge.id
            edge_vals['name'] = edge.id_linktemplate
            edge_vals['start_x'] = edge.start_node.vertex.x
            edge_vals['start_y'] = edge.start_node.vertex.y
            edge_vals['end_x'] = edge.end_node.vertex.x
            edge_vals['end_y'] = edge.end_node.vertex.y

            res['results'].append(edge_vals)

            res['total'] = len(res['results'])

        return json.dumps(res) 
            
GiscegisBTelementController()
