# -*- coding: utf-8 -*-
"""Modul giscegis_polissa_controller."""

try:
    import json
except ImportError:
    import simplejson as json
from osv import fields, osv

class GiscegisPolissaController(osv.osv):
    """Executa les funcions del controlador de pòlisses del tomcat."""
    _name = "giscegis.polissa.controller"
    _description = __doc__
    _auto = False
    
    def view_polissa(self, cursor, uid, name, context=None):
        """Mètode public String view_polissa(String parameter)
        del tomcat
        """

        res = {'total': 0, 'results': []}
        polisses_obj = self.pool.get('giscedata.polissa')
        polisses = polisses_obj.search(cursor, uid, [('name','=',name)])
        for polissa in polisses_obj.browse(cursor, uid, polisses):
            pol = {}
            pol['polissa'] = polissa.name
            pol['cups'] = polissa.cups.name
            pol['carrer'] = polissa.cups.nv
            pol['numero'] = polissa.cups.pnp
            pol['escala'] = polissa.cups.es
            pol['planta'] = polissa.cups.pt
            pol['pis'] = polissa.cups.pu
            pol['observacions'] = polissa.observacions
            pol['potencia'] = polissa.potencia
            pol['tarifa'] = polissa.tarifa.name
            pol['titular'] = polissa.titular.name
            pol['tensio_normalitzada'] = polissa.tensio_normalitzada.name
            pol['state'] = polissa.state
            pol['dalta'] = polissa.data_alta
            pol['dbaixa'] = polissa.data_baixa
            pol['cnae'] = polissa.cnae.name
            pol['codipolissa'] = polissa.codi
            pol['comercialitzadora'] = polissa.comercialitzadora.name
            pol['escomesa'] = polissa.cups.id_escomesa.name
            pol['zona'] = polissa.cups.zona
            pol['ordre'] = polissa.cups.ordre
            pol['et'] = polissa.cups.et
            pol['linia'] = polissa.cups.linia
            pol['icp'] = 0
            pol['consum'] = 0
            comptadors = polissa.comptadors_actius(polissa.data_alta)
            if comptadors:
                compt_obj = self.pool.get('giscedata.lectures.comptador')
                pol['n_comptador'] = compt_obj.read(cursor, uid, 
                                        [comptadors[0]], ['name'])[0]['name']
            else:
                pol['n_comptador'] = '0000000'
                
            pol['n_comptador_2'] = '0000000'
            pol['n_comptador_3'] = '0000000'
            pol['n_comptador_reactiva'] = '0000000'
            
            res['results'].append(pol)
            
        res['total'] = len(res['results'])
        return json.dumps(res) 
            
GiscegisPolissaController()
