# -*- coding: utf-8 -*-
"""Modul giscegis_client_controller."""

try:
    import json
except ImportError:
    import simplejson as json
from osv import fields, osv

class GiscegisClientController(osv.osv):
    """Executa les funcions del controlador de clients del tomcat."""
    _name = "giscegis.clients.controller"
    _description = __doc__
    _auto = False
    
    def search_client_new(self, cursor, uid, s_type, s_string, 
                          id_municipi=None, exacta=False, context=None):
        """Mètode públic 
        search_client_new(String searchstring, String searchtype, 
                          String municipiid, boolean exacta)
        del tomcat
        """

        if exacta:
            oper = '='
        else:
            oper = 'ilike'
        search_params = [(s_type, oper, s_string)]
        if s_type in ('nv', 'titular'):
            search_params.append(('municipi', '=', id_municipi))
        
        res = {'total': 0, 'results': []}
        pol_obj = self.pool.get('giscegis.polisses.171')
        for polissa_id in pol_obj.search(cursor, uid, search_params):
            polissa = pol_obj.browse(cursor, uid, polissa_id)
            pol = {}
            pol['cups'] = polissa.cups
            pol['escomesa'] = polissa.escomesa
            pol['polissa'] = polissa.name
            pol['titular'] = polissa.titular
            pol['carrer'] = polissa.nv
            pol['tensio'] = polissa.tensio
            pol['numero'] = polissa.pnp
            pol['planta'] = polissa.pt
            pol['pis'] = polissa.pu
            pol['n_comptador'] = polissa.n_comptador
            pol['et'] = polissa.et
            pol['x'] = polissa.x
            pol['y'] = polissa.y
            if polissa.x and polissa.y:
                pol['posicionar'] = "<a href='#' onClick='javascript:zoom()'>Zoom</a>"
            else:
                pol['posicionar'] = "Zoom"
            pol['detall'] = "<a href='#' onClick='javascript:detall()'>Fitxa</a>"
            pol['potencia'] = polissa.potencia
            res['results'].append(pol)
        res['total'] = len(res['results'])
        return json.dumps(res)

    def locate_client(self, cursor, uid, client_name):
        """Mètode públic
        locate_client(client_name)
        """
        res = {'total': 0, 'results': []}

        polissa_obj = self.pool.get('giscegis.polisses.171')
        polisses = polissa_obj.search(cursor, uid, [('titular', 'ilike', 
                                                     client_name)])
        for polissa in polisses:
            pol = {}
            pol['carrer'] = polissa.nv
            pol['escala'] = polissa.es
            pol['n_comptador'] = polissa.n_comptador
            pol['potencia'] = polissa.potencia
            pol['escomesa'] = polissa.escomesa
            pol['tensio'] = polissa.tensio
            pol['titular'] = polissa.titular
            pol['polissa'] = polissa.name
            pol['numero'] = polissa.pnp
            pol['ct'] = polissa.et
            pol['linia'] = polissa.linia
            res['results'].append(pol)
        res['total'] = len(res['results'])
        return json.dumps(res)

GiscegisClientController()
