# *-* coding: utf-8 *-*

from osv import osv, fields


class GiscegisOpenSwitchesAtGeom(osv.osv):
    """
    Model of giscegis.open.switches.at.geom for gis v3
    """
    _name = 'giscegis.open.switches.at.geom'
    _auto = False
    _columns = {
        'name': fields.text('Switch name', readonly=True),
        'geom': fields.text('Geometry', readonly=True),
    }

    def init(self, cursor):
        """
        Initializes the model and creates the materialized views

        :param cursor: Database cursor
        :return: True
        """
        sql_drop = """
            DROP MATERIALIZED VIEW IF EXISTS giscegis_open_switches_at_geom;
        """
        cursor.execute(sql_drop)
        sql_create = """
          CREATE MATERIALIZED VIEW giscegis_open_switches_at_geom AS (
            SELECT n.name,
            ST_SETSRID(ST_MAKEPOINT(v.x,v.y),
                    (SELECT value::int
                    FROM res_config
                    WHERE name='giscegis_srid')) AS geom
            FROM giscegis_configdefault_at cd
            LEFT JOIN giscegis_nodes n on (n.id=cd.node)
            LEFT JOIN giscegis_vertex v on (n.vertex=v.id))
        """
        cursor.execute(sql_create)
        sql_index = """
        CREATE INDEX giscegis_open_switches_at_geom_index
          ON giscegis_open_switches_at_geom USING gist (geom);
        """
        cursor.execute(sql_index)
        return True

    def get_srid(self, cursor, uid):
        """
        Returns the srid of the model

        :param cursor: Database cursor
        :param uid: User id
        :return: SRID or false if the table is empty
        """

        sql_srid = """
          SELECT ST_SRID(geom) AS srid
          FROM giscegis_open_switches_at_geom LIMIT 1;
        """
        cursor.execute(sql_srid)
        data = cursor.fetchall()
        if len(data) > 0:
            return data[0][0]
        else:
            return False

    def recreate(self, cursor, uid):
        """
        Method that recreates the materialized view

        :param cursor: Database cursor
        :param uid: User id
        :return: True
        """
        sql_refresh = """
        REFRESH MATERIALIZED VIEW giscegis_open_switches_at_geom;
        """
        cursor.execute(sql_refresh)
        return True


GiscegisOpenSwitchesAtGeom()
