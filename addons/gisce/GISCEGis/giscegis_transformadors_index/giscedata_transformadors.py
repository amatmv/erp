from giscegis_base_index.giscegis_base_index import GiscegisBaseIndex


class GiscedataTransformadorTrafo(GiscegisBaseIndex):
    _name = 'giscedata.transformador.trafo'
    _inherit = 'giscedata.transformador.trafo'

    def get_lat_lon(self, cursor, uid, item, context=None):
        """
        Returns the lat and lon of a trafo to index

        :param cursor: Database cursor
        :param uid: User id
        :param item: Item to index
        :param context: OpenERP context
        :return: Tuple with lat and lon
        """
        if context is None:
            context = {}
        if isinstance(item, list):
            item = item[0]

        sql = """
        SELECT
          ST_Y(
            ST_Transform(geom, 4326)
          ) AS lat,
          ST_X(
            ST_Transform(geom, 4326)
          ) AS lon
        FROM giscedata_transformador_trafo tt
        WHERE
            id = %(id)s
        """
        cursor.execute(sql, {'id': item.id})
        res = cursor.fetchone()
        if not res or not res[0]:
            res = (0, 0)

        return res


GiscedataTransformadorTrafo()
