# -*- coding: utf-8 -*-
{
    "name": "Transformadors index en Qgisce",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
* Index per Transformadors en Qgisce
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "giscedata_transformadors",
        "giscegis_base_index",
        "giscegis_blocs_transformadors",
        "giscegis_blocs_transformadors_reductors",
        "giscedata_transformadors_index",
        "giscegis_search_type"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscegis_search_type_data.xml"
    ],
    "active": False,
    "installable": True
}
