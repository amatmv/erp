# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* giscegis_transformadors_index
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.14\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2019-02-26 15:09\n"
"PO-Revision-Date: 2019-02-26 15:09\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: giscegis_transformadors_index
#: model:ir.module.module,description:giscegis_transformadors_index.module_meta_information
msgid "Aquest mòdul afegeix les següents funcionalitats:\n"
"* Index per Transformadors en Qgisce\n"
""
msgstr ""

#. module: giscegis_transformadors_index
#: model:ir.module.module,shortdesc:giscegis_transformadors_index.module_meta_information
msgid "Transformadors index en Qgisce"
msgstr ""

