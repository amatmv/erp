from giscegis_base_index.giscegis_base_index import GiscegisBaseIndex


class GiscedataCts(GiscegisBaseIndex):
    _name = 'giscedata.cts'
    _inherit = 'giscedata.cts'
    _index_fields = {
        "obres": lambda self, data: ' '.join(['obra{0}'.format(d.name) or '' for d in data])
    }

GiscedataCts()
