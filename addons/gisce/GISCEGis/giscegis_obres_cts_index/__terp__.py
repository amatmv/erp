# -*- coding: utf-8 -*-
{
    "name": "Index Obres per GIS",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Indexat de giscedata_obres_cts per a GIS
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "base_index",
        "giscedata_obres_cts"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
