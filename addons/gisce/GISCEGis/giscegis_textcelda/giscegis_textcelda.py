# -*- coding: utf-8 -*-
from osv import fields, osv
from giscegis_base_geom.giscegis_base_geom import DROP_ON_AUTOCAD_DUMP

# Subscribe this model to the list to be updated if there is an AutoCAD dump
DROP_ON_AUTOCAD_DUMP.append('giscegis_textcelda')


class giscegis_textcelda(osv.osv):
    _name = "giscegis.textcelda"
    _description = "Text Celda Llistat per GIS"

    def clean(self, cr, uid):
        cr.execute("truncate giscegis_textcelda")
        cr.commit()
        return True

    _columns = {
      'name': fields.char('Name', size=10, required=True),
      'vertex': fields.many2one('giscegis.vertex', 'Vertex',
                                ondelete='set null'),
      'text': fields.char('Text', size=255),
      'rotation': fields.float('Rotation'),
      'height': fields.float('Height'),
    }

    _defaults = {

    }
    _order = 'name, id'

    def create(self, cursor, uid, vals, context=None):
        """Mètode sobreescrit per escriure als camps many2one des
        d'aquí.

        Ens hem d'assegurar de què els mètodes create() dels objectes
        referenciats estiguin alhora sobreescrits per tal què comprovin
        si l'objecte existeix o no."""
        _fields = [('vertex', 'giscegis.vertex'),]
        for field in _fields:
            if field[0] in vals and isinstance(vals[field[0]], list):
                obj = self.pool.get(field[1])
                remote_id = obj.create(cursor, uid, vals[field[0]][0][2])
                if remote_id:
                    vals[field[0]] = remote_id
        return super(osv.osv, self).create(cursor, uid, vals)

giscegis_textcelda()
