# *-* coding: utf-8 *-*

import wizard
import pooler

def _generar_taula_nbm(self, cr, uid, data, context={}):
  s = ''
  s += '%i blocs creats.' % (pooler.get_pool(cr.dbname).get('giscegis.node.bloc.model').generate(cr, uid))
  return {'info': s}
  

_init_arch = """<?xml version="1.0"?>
<form string="Generació N-B-M">
  <label string="Taula generada correctament !" colspan="4" />
  <separator string="Info:" colspan="4" />
  <field name="info" colspan="4" readonly="1" nolabel="1"/>
</form>
"""

_init_fields = {
  'info': {'string': 'Info', 'type': 'text'}
}

class wizard_generar_nodes_blocs_models(wizard.interface):

  states = {
    'init': {
    	'actions': [_generar_taula_nbm],
      'result': {'type': 'form', 'fields': _init_fields, 'arch': _init_arch, 'state': [('end', 'D\'acord', 'gtk-ok')]},
    },
    'end': {
    	'actions': [],
    	'result': { 'type' : 'state', 'state' : 'end' },
    },
  }

wizard_generar_nodes_blocs_models('giscegis.node.bloc.model')
