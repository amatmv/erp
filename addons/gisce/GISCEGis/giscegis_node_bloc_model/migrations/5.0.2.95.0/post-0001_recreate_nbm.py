# coding=utf-8

import logging
import pooler


def up(cursor, installed_version):
    """
    Migration script to recreate giscegis.node.bloc.model

    :param cursor: Database cursor
    :param installed_version: Module installed version
    :type installed_version: str
    :return: None
    :rtype: None
    """
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')

    logger.info('Recreating giscegis.node.bloc.model')

    pool = pooler.get_pool(cursor.dbname)
    nbm_obj = pool.get('giscegis.node.bloc.model')
    nbm_obj.generate(cursor, 1)

    logger.info('giscegis.node.bloc.model recreated')


def down(cursor, installed_version):
    pass


migrate = up
