from destral import testing
from destral.transaction import Transaction
import os

class GiscegisNodesTest(testing.OOTestCase):

    require_demo_data = True

    def __test_vinculate_auto(self):
        """
        Test for automatic node vinculation

        :return:
        """
        self.oorq = os.environ.get('OORQ_ASYNC', 'True')
        os.environ['OORQ_ASYNC'] = 'False'

        imd = self.openerp.pool.get('ir.model.data')
        fields = ['name', 'codi_autocad_now', 'codi_autocad_old', 'vertex']
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            nodes_obj = self.openerp.pool.get('giscegis.nodes')
            node_id = imd.get_object_reference(
                cursor, uid, 'giscegis_nodes', '1476'
            )[1]
            cursor.execute("""
                UPDATE giscegis_nodes
                SET name=replace(name,'*','')
                WHERE id=%s
            """, (node_id, ))
            n_data = nodes_obj.read(cursor, uid, node_id, fields, None)
            n_data['name'] = 'holahola'
            nodes_obj.unlink(cursor, uid, node_id, None)
            n_data['vertex'] = n_data['vertex'][0]
            new_id = nodes_obj.create(cursor, uid, n_data, None)
            nodes_obj.backup_table(cursor, uid, [], None)
            num = nodes_obj.vinculate_orphands(cursor, uid, [new_id], None)
            self.assertEqual(num, len([new_id]))
            new_n_data = nodes_obj.read(
                cursor, uid, [new_id], ['name'], None)[0]
            self.assertEqual(new_n_data['name'], n_data['name'])
            self.assertFalse('*' in new_n_data['name'])


class GiscegisNodeBlocModel(testing.OOTestCase):
    """
    Tests for giscegis nodes bloc model
    """

    def test_generate(self):
        """
        Basic test for generate

        :return: None
        """
        self.openerp.install_module('giscegis_qgisce_full')
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor

            nbm_obj = self.openerp.pool.get('giscegis.node.bloc.model')
            mod_obj = self.openerp.pool.get('ir.model')
            ct_obj = self.openerp.pool.get('giscedata.cts')
            nod_obj = self.openerp.pool.get("giscegis.nodes")
            id_nod = nod_obj.create(cursor, uid, {"name": 1})

            ct_data = {
                "name": "test",
                "node_id": id_nod
            }
            id_ct = ct_obj.create(cursor, uid, ct_data)
            ct_obj.unlink(cursor, uid, id_ct)

            search_parms = [
                ("model", "=", "giscedata.cts.subestacions")
            ]
            id_sub = mod_obj.search(cursor, uid, search_parms)

            nbm_obj.generate(cursor, uid)

            search_parms_sub = [
                ("model", "=", id_sub[0])
            ]
            nbm_data = nbm_obj.search(cursor, uid, search_parms_sub)
            self.assertTrue(len(nbm_data) > 0)

            search_parms_ct = [
                ("node", "=", id_nod)
            ]
            nbm_data_cts = nbm_obj.search(cursor, uid, search_parms_ct)
            self.assertEqual(len(nbm_data_cts), 0)
