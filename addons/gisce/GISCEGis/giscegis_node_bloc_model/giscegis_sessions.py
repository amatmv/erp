from osv import osv

class GiscegisSession(osv.osv):
    _name = 'giscegis.session'
    _inherit = 'giscegis.session'

    def merge(self, cursor, uid, ids, context=None):
        """
        Overwrites the session merge to update nbm

        :param cursor: Database cursor
        :param uid: User id
        :param ids: Session id
        :param context: OpenERP context
        :return: True
        :rtype: bool
        """

        res = super(GiscegisSession, self).merge(
            cursor, uid, ids, context=context
        )
        obj_mod = self.pool.get('giscegis.node.bloc.model')
        obj_mod.generate(cursor, uid)
        return res


GiscegisSession()
