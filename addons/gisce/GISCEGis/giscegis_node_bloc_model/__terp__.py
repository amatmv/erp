# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Node Bloc Model",
    "description": """Taula per relacionar Nodes amb blocs """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "giscegis_nodes",
        "base",
        "giscegis_base",
        "giscegis_base_geom",
        "giscegis_cataleg"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/giscegis_node_bloc_model_wizard.xml",
        "ir.model.access.csv",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
