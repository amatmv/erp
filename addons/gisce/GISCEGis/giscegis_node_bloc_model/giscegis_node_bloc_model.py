# *-* coding: utf-8 *-*
from osv import fields, osv
from psycopg2.extensions import AsIs
import traceback
from giscegis_base_geom.giscegis_session import SessionManaged
from shapely.wkt import loads
import logging
from giscegis_base_geom.giscegis_base_geom import DROP_ON_AUTOCAD_DUMP

# Subscribe this model to the list to be updated if there is an AutoCAD dump
DROP_ON_AUTOCAD_DUMP.append('giscegis_node_bloc_model')


class GiscegisNodeBlocModel(SessionManaged):

    _name = 'giscegis.node.bloc.model'

    def name_get(self, cr, uid, ids, context=None):
        """
        Returns the name of the elemnt

        :param cr: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: Ids of the element
        :type ids: list(int)
        :param context: OpenERP context
        :type context: dict
        :return: Element names
        :rtype: list(tuple(int,str))
        """
        if context is None:
            context = {}
        if not len(ids):
            return []
        res = []
        for nbm in self.browse(cr, uid, ids):
            name = '%s: %s - %i' % (nbm.node.name, nbm.model.model, nbm.res_id)
            res.append((nbm.id, name))
        return res

    def generate(self, cr, uid):
        """
        Generates the table of block-model

        :param cr: Database cursor
        :param uid: User id
        :return:
        """

        models = ['%bloc%']
        ban_models = ['%171%',
                      '%geom%',
                      '%blockname',
                      'giscegis.node.bloc.model',
                      '%stop%',
                      '%maniobrable%',
                      'giscegis.blocs.suports',
                      "pg_activity%",
                      'giscegis.tensio.bloc'
                      ]
        like_sql = ' or '.join(['model like %s']*len(models))
        not_like_sql = ' and model not like %s'*len(ban_models)

        sql = '''
              SELECT id,model
              FROM ir_model
              WHERE
              '''
        sql += like_sql
        sql += '\n' + not_like_sql
        sql_params = models + ban_models

        cr.execute(sql, sql_params)

        blocs = cr.dictfetchall()

        cr.execute("delete from giscegis_node_bloc_model")
        sql_bloc = '''SELECT id,node FROM %s WHERE node IS NOT NULL'''
        for bloc in blocs:
            cr.execute(sql_bloc, (AsIs(bloc['model'].replace('.', '_')),))
            for r in cr.dictfetchall():
                vals = {
                    'node': r['node'],
                    'res_id': r['id'],
                    'model': bloc['id']
                }
                self.create(cr, uid, vals)
        qgisce_nodes = self.generate_qgisce(cr, uid, context={})
        return self.search_count(cr, uid, []) + qgisce_nodes

    def generate_qgisce(self, cursor, uid, truncate=False, context={}):
        """
        Generates the table of block-model using the Qgisce fields

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :return: Number of created nodes
        :rtype: int
        """

        obj_cataleg = self.pool.get("giscegis.cataleg.cataleg")
        obj_ir_model = self.pool.get("ir.model")

        search_params = [
            ("not_topology", "=", False),
        ]

        cat_ids = obj_cataleg.search(cursor, uid, search_params)
        data_cat = obj_cataleg.read(cursor, uid, cat_ids, ["model", "geom_type"])

        point_models = [res["model"][0] for res in data_cat if res["geom_type"] == "Point"]
        point_models = obj_ir_model.read(cursor, uid, point_models, ["model"])

        if truncate:
            cursor.execute("TRUNCATE giscegis_node_bloc_model")

        sql_point = "SELECT id,node_id FROM %s WHERE node_id IS NOT NULL and active"

        for bloc in point_models:
            obj_mod = self.pool.get(bloc["model"])
            if obj_mod is None:
                sentry = self.pool.get('sentry.setup')
                if sentry is not None:
                    msg = "giscegis.node.bloc.model skiping generation of model {}".format(bloc["model"])
                    sentry.client.captureMessage(msg,level=logging.WARNING)
                continue

            if obj_mod._inherits:
                ids = obj_mod.search(cursor, uid, [("node_id", "!=", False)])
                mod_data = obj_mod.read(cursor, uid, ids, ["node_id"])
                for r in mod_data:
                    vals = {
                        "node": r["node_id"][0],
                        "res_id": r["id"],
                        "model": bloc["id"]
                    }
                    self.create(cursor, uid, vals)
            else:
                cursor.execute(sql_point, (AsIs(bloc['model'].replace('.', '_')),))
                for r in cursor.dictfetchall():
                    vals = {
                        'node': r['node_id'],
                        'res_id': r['id'],
                        'model': bloc['id']
                    }
                    self.create(cursor, uid, vals)
        return self.search_count(cursor, uid, [])

    def update_nbm(
            self, cursor, uid, ids, geom, model, operation, context=None
    ):
        """
        With a g

        :param cursor: Database cursro
        :param uid:User id
        :type uid: int
        :param ids:
        :type ids: list(int)
        :param geom: Geometry
        :type geom: str
        :param model:
        :param operation:Operation done (write, create, unlink)
        :type operation: str
        :param context:
        :return: None
        """

        if context is None:
            context = {}
        if not isinstance(ids, list):
            ids = [ids]

        ir_model_obj = self.pool.get("ir.model")

        search_params_model = [
            ("model", "=", model)
        ]
        id_model = ir_model_obj.search(cursor, uid, search_params_model)

        if isinstance(id_model, list):
            id_model = id_model[0]

        if operation == "write":
            return self._update_nbm_write(
                cursor, uid, ids, geom, id_model, context=context
            )
        elif operation == "create":
            return self._update_nbm_create(
                cursor, uid, ids, geom, id_model, context=context
            )
        elif operation == "unlink":
            return self._update_nbm_unlink(
                cursor, uid, ids, id_model, context=context
            )

    def _update_nbm_write(self, cr, uid, ids, geom, model, context=None):
        """
        Updates the node block model on write operations

        :param cr: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: List of afected ids
        :type ids: list(int)
        :param geom: Element geomerty
        :type geom: str
        :param: model Id
        :type model: int
        :param context: OpenERP context
        :type context: dict
        :return: None
        :rtype: None
        """

        if context is None:
            context = {}

        if not isinstance(ids, list):
            ids = [ids]

        nbm_obj = self.pool.get("giscegis.node.bloc.model")
        n_obj = self.pool.get("giscegis.nodes")
        if geom.lower().startswith("point"):
            new_nid_sp = [
                ("geom", "=", geom),
            ]
            new_nid = n_obj.search(cr, uid, new_nid_sp, context=context)

            old_nbm_sp = [
                ("res_id", "in", ids),
                ("model", "=", model)
            ]
            old_nbm_ids = nbm_obj.search(cr, uid, old_nbm_sp, context=context)
            if old_nbm_ids:
                nbm_obj.write(
                    cr, uid, old_nbm_ids[0], {"node": new_nid[0]},
                    context=context
                )
        elif geom.lower().startswith("linestring"):
            first_wkt = "{}{}".format(
                "POINT", str(loads(geom).coords[0])
            ).replace(",", " ")

            last_wkt = "{}{}".format(
                "POINT", str(loads(geom).coords[-1])
            ).replace(",", " ")

            first_nid_sp = [
                ("geom", "=", first_wkt),
            ]
            first_nid = n_obj.search(cr, uid, first_nid_sp, context=context)

            last_nid_sp = [
                ("geom", "=", last_wkt),
            ]
            last_nid = n_obj.search(cr, uid, last_nid_sp, context=context)

            old_nbm_sp = [
                ("res_id", "in", ids),
                ("model", "=", model)
            ]
            old_nbm_ids = nbm_obj.search(cr, uid, old_nbm_sp, context=context)

            if len(old_nbm_ids) == 2:
                nbm_obj.write(
                    cr, uid, old_nbm_ids[0], {"node": first_nid[0]},
                    context=context
                )
                nbm_obj.write(
                    cr, uid, old_nbm_ids[1], {"node": last_nid[0]},
                    context=context
                )
            elif len(old_nbm_ids) == 0:
                pass
                # raise Exception("No node block module ids found")
            else:
                raise Exception("Too much node block model ids found")

        else:
            raise Exception("Geometry type not suported")

    def _update_nbm_unlink(self, cr, uid, ids, model_id, context=None):
        """
        Updates the node block model data on unlink opeations

        :param cr: Database cursor
        :param uid: User id
        :type uid: intt
        :param ids: List of the ids
        :type ids: list(int)
        :param model_id: Id of the model
        :type model_id: int
        :param context: OpenERP context
        :return: None
        :rtype: None
        """

        if context is None:
            context = {}

        nbm_obj = self.pool.get("giscegis.node.bloc.model")

        search_params = [
            ("model", "=", model_id),
            ("res_id", "in", ids)
        ]
        nbm_ids = nbm_obj.search(cr, uid, search_params, context=context)
        if nbm_ids:
            nbm_obj.unlink(cr, uid, nbm_ids, context=context)

    def _update_nbm_create(self, cr, uid, ids, geom, model, context=None):
        """
        Updates the node block model data when creates elemetns

        :param cr: Database cursor
        :param uid: User id
        :param ids: Ids of the elements
        :param context: OpenERP context
        :return:
        """

        if context is None:
            context = {}

        if isinstance(ids, list):
            ids = ids[0]

        nbm_obj = self.pool.get("giscegis.node.bloc.model")
        nodes_obj = self.pool.get("giscegis.nodes")

        if geom.lower().startswith("point"):
            nids = nodes_obj.search(
                cr, uid, [("geom", "=", geom)], context=context
            )
            for node_id in nids:
                write_vals = {
                    "node": node_id,
                    "model": model,
                    "res_id": ids
                }
                nbm_obj.create(cr, uid, write_vals, context=context)
        elif geom.lower().startswith("linestring"):
            first_wkt = "{}{}".format(
                "POINT",
                str(loads(geom).coords[0])
            ).replace(",", " ")

            last_wkt = "{}{}".format(
                "POINT",
                str(loads(geom).coords[-1])
            ).replace(",", " ")

            first_nid_sp = [
                ("geom", "=", first_wkt),
            ]
            first_nid = nodes_obj.search(cr, uid, first_nid_sp, context=context)

            last_nid_sp = [
                ("geom", "=", last_wkt),
            ]
            last_nid = nodes_obj.search(cr, uid, last_nid_sp, context=context)
            for node_id in [first_nid, last_nid]:
                write_vals = {
                    "node": node_id[0],
                    "model": model,
                    "res_id": ids
                }
                nbm_obj.create(cr, uid, write_vals, context=context)
        else:
            raise Exception("Geometry type not suported")

        return ids

    _columns = {
        'node': fields.many2one(
            'giscegis.nodes', 'Node', required=True, ondelete='cascade'
        ),
        'res_id': fields.integer('Id del recurs', selected=True),
        'model': fields.many2one('ir.model', 'Model', required=True),
        'active': fields.boolean('Actiu', select=True)
    }

    _defaults = {
        'active': lambda *a: 1
    }


GiscegisNodeBlocModel()


class GiscegisNodes(osv.osv):
    _name = 'giscegis.nodes'
    _inherit = 'giscegis.nodes'

    def backup_table(self, cursor, uid, ids, context=None):
        """
        Move giscegis_nodes table to giscegis_nodes_old

        :param cursor: Database cursor
        :param uid: User id
        :param ids: Afected ids
        :param context: OpenERP context
        :return: True if it's done properly
        """

        n_map_obj = self.pool.get('giscegis.nodes.map')

        try:
            sql_delete = '''
            DELETE FROM giscegis_nodes_map
            '''
            cursor.execute(sql_delete)
            sql_move = '''
            INSERT INTO giscegis_nodes_map (name,codi_autocad,x,y)
            SELECT n.name, n.codi_autocad_now, v.x ,v.y from giscegis_nodes n
            INNER JOIN  giscegis_vertex v ON v.id=n.vertex;
            '''
            cursor.execute(sql_move)
            sql_node_model = '''
            SELECT nmn.model,n.name,nmn.res_id
            FROM giscegis_node_bloc_model nmn
            INNER JOIN giscegis_nodes n ON nmn.node = n.id
            '''
            cursor.execute(sql_node_model)
            model_data = cursor.fetchall()
            model_node = {}
            obj_model = self.pool.get('ir.model')
            node_name = {}
            for model, node, res_id in model_data:
                model_node.setdefault(model, [])
                model_node[model].append(node)

                model_name = obj_model.read(
                    cursor, uid, model, ['model'], context)
                obj = self.pool.get(model_name['model'])
                old_name = obj.name_get(cursor, uid, [res_id], context=context)
                node_name[node] = old_name[0][1]

            for model, node_names in model_node.iteritems():
                ids = n_map_obj.search(
                    cursor, uid, [('name', 'in', node_names)])
                n_map_obj.write(cursor, uid, ids, {'model': model})

            for node_name, name in node_name.iteritems():
                identificador = n_map_obj.search(
                    cursor, uid, [('name', '=', node_name)]
                )
                n_map_obj.write(cursor, uid, identificador, {'old_name': name})

        except Exception:
            traceback.print_exc()
            return False

        return True


GiscegisNodes()
