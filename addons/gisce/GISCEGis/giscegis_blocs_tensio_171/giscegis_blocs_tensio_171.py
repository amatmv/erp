# *-* coding: utf-8 *-*

from osv import osv, fields

class giscegis_blocs_tensio_171(osv.osv):

    _name = 'giscegis.blocs.tensio.171'
    _auto = False
    _columns = {
      'x': fields.float('X', readonly=True),
      'y': fields.float('Y', readonly=True),
      'texte': fields.text('Texte', readonly=True),
      'blockname': fields.char('Blockname', size=50),
      'rotation': fields.float('Rotation'),
    }

    def init(self, cr):
        cr.execute("""drop view if exists giscegis_blocs_tensio_171""")
        cr.execute("""
          create or replace view giscegis_blocs_tensio_171 as (
            select tensio.id, vertex.x, vertex.y, blockname.name || ' ' || tensio.tensio as texte, blockname.name || tensio.tensio as blockname, tensio.rotation
    from giscegis_blocs_tensio tensio
            left join giscegis_vertex vertex on (tensio.vertex = vertex.id)
            left join giscegis_blocs_tensio_blockname blockname on (tensio.blockname = blockname.id)
        )""")

giscegis_blocs_tensio_171()
