# coding=utf-8
import logging
import pooler


def migrate(cursor, installed_version):

    loger = logging.Logger("openerp.migration")
    loger.info("Omplint camp rotation dels elements de tall(Seccionador AT).")

    pool = pooler.get_pool(cursor.dbname)
    elem_tall_obj = pool.get('giscedata.celles.cella')
    elem_tall_obj.fill_rotation_sec_at(cursor, 0)

    loger.info("Camp rotation dels elements de tall(Seccionador AT) omplert.")


up = migrate
