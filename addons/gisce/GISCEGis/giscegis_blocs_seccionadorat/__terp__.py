# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Blocs Seccionador AT",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_nodes",
        "giscegis_vertex",
        "giscegis_base",
        "giscegis_blocs_seccionador_unifilar",
        "giscegis_elements_tall",
        "giscegis_cts_subestacions"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscegis_blocs_seccionadorat_view.xml",
        "ir.model.access.csv",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
