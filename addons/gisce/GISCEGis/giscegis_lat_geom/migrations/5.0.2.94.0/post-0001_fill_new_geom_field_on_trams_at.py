# coding=utf-8

import logging
import pooler


def up(cursor, installed_version):

    logger = logging.getLogger('openerp.migration')

    logger.info('Filling field geom from giscedata_at_tram')

    pool = pooler.get_pool(cursor.dbname)
    trams_at_obj = pool.get('giscedata.at.tram')
    trams_at_obj.fill_geom(cursor, 1)

    logger.info('Field geom filled for model giscedata_at_tram.')


def down(cursor, installed_version):
    pass


migrate = up
