# *-* coding: utf-8 *-*

from osv import osv, fields
from tools import config


class GiscegisLatGeom(osv.osv):
    _name = 'giscegis.lat.geom'
    _auto = False
    _columns = {
        'id': fields.integer('Id', readonly=True),
        'name': fields.integer('Id', readonly=True),
        'geom': fields.text('Texte', readonly=True),
        'simple_geom': fields.text('Texte', readonly=True),
        'texte': fields.text('Texte', readonly=True),
        'theme': fields.text('Texte', readonly=True),
        "materialize_date": fields.datetime("Fecha creación vista"),
    }

    def init(self, cursor):

        cursor.execute("""
            SELECT relname
            FROM pg_class
            WHERE relkind = 'S' AND relname = 'giscegis_lat_id_seq'
        """)
        res = cursor.fetchall()
        srid = config.get('srid', 25830)
        if not res:
            # Creacio de la sequencia si no existeix
            cursor.execute("""
                CREATE SEQUENCE public.giscegis_lat_id_seq
                    INCREMENT 1
                    MINVALUE 1
                    MAXVALUE 9223372036854775807
                    START 1
                    CACHE 1;
            """)

        sql_exists = """
            SELECT EXISTS (
                SELECT 1
                FROM   information_schema.tables
                WHERE  table_schema = 'public'
                AND    table_name = 'giscegis_lat'
            );
        """
        cursor.execute(sql_exists)
        res_exists = cursor.fetchall()[0][0]
        # Creacio de la taula giscegis_lat
        if not res_exists:
            sql_giscegis_lat = """
                CREATE TABLE IF NOT EXISTS  public.giscegis_lat (
                    gid integer NOT NULL DEFAULT
                    nextval('giscegis_lat_id_seq'::regclass),
                    id double precision,
                    CONSTRAINT giscegis_lat_pkey PRIMARY KEY (gid));
                
                SELECT AddGeometryColumn(
                    'public','giscegis_lat', 'geom', %(srid)s, 
                    'MULTILINESTRING',2, true
                );
            """
            cursor.execute(sql_giscegis_lat, {'srid': srid})

        cursor.execute("""
            DROP MATERIALIZED VIEW IF EXISTS giscegis_lat_geom;
            
            CREATE MATERIALIZED VIEW giscegis_lat_geom AS
                SELECT 
                    linia.id,
                    tram.id::int AS name,
                    tram.circuits AS circuits, 
                    shape.geom AS geom,
                    ST_Simplify(shape.geom, 0.5) AS simple_geom,
                    COALESCE(linia.name, 'S/Linia') AS texte,
                CASE
                    WHEN tipus_cable.codi = 'D' OR tipus_cable.codi = 'T'
                        THEN 'A-' || linia.tensio::text
                    WHEN tipus_cable.codi = 'S'
                        THEN 'S-' || linia.tensio::text
                    WHEN tipus_cable.codi = 'E' 
                        THEN 'EAT'
                    ELSE '-' || linia.tensio::text
                END AS theme,
                tram.cini AS cini,
                now() AS materialize_date
                FROM giscegis_lat AS shape
                LEFT JOIN giscedata_at_tram AS tram ON shape.id = tram.name
                LEFT JOIN giscedata_at_linia linia ON 
                    tram.linia = linia.id AND linia.name::text <> '1'::text
                LEFT JOIN giscedata_at_cables cable ON tram.cable = cable.id
                LEFT JOIN giscedata_at_tipuscable AS tipus_cable
                    ON cable.tipus = tipus_cable.id
                WHERE tram.active;
        """)

        # Creacio d'indexs
        cursor.execute("""
            DROP INDEX IF EXISTS giscegis_lat_geom_idx;
            CREATE INDEX giscegis_lat_geos_geom 
            ON giscegis_lat_geom USING gist (geom);
        """)

        cursor.execute("""
            DROP INDEX IF EXISTS giscegis_lat_geom_idx;
            CREATE  INDEX giscegis_lat_geom_idx
            ON public.giscegis_lat USING gist(geom);
        """)

        return True

    def _auto_init(self, cursor, context):
        res = super(GiscegisLatGeom, self)._auto_init(cursor, context)
        return res

    def get_srid(self, cursor, uid):
        return config.get('srid', 25830)

    def recreate(self, cursor, uid):
        cursor.execute("""
            REFRESH MATERIALIZED VIEW giscegis_lat_geom;
        """)
        return True


GiscegisLatGeom()


class GiscedataAtLinia(osv.osv):

    _name = "giscedata.at.linia"
    _inherit = "giscedata.at.linia"

    def get_polyline(self, cursor, uid, ids, context=None):
        if type(ids) == int:
            ids = [ids]
        result = []
        btlike = self.pool.get('giscegis.base.geom').get_btlike_layer(
            cursor, uid
        )
        sql = '''
        SELECT e.id FROM giscegis_edge e LEFT JOIN giscedata_at_tram lat
        ON (lat.name=e.id_linktemplate) WHERE e.id_linktemplate IN
        (SELECT name FROM giscedata_at_tram WHERE linia IN %(ids)s)
        AND e.layer NOT LIKE %(btlike)s
        '''
        cursor.execute(sql, {'ids': tuple(ids), 'btlike': btlike})
        data = cursor.dictfetchall()
        for identifier in data:
            result.append(identifier['id'])
        return result


GiscedataAtLinia()
