from osv import osv
from giscegis_base_geom.wizard.giscegis_shp_loader \
    import GIS_MODELS_TO_UPDATE_GEOM_DATA


# Subscribe this model to the list to be updated if there is an AutoCAD dump
GIS_MODELS_TO_UPDATE_GEOM_DATA.append(
    {
        'model': 'giscedata.at.tram',
        'functions': ['fill_geom', 'fill_edge_id']
    },
)


class GiscedataAtTram(osv.osv):

    _name = "giscedata.at.tram"
    _inherit = "giscedata.at.tram"

    def fill_geom(self, cursor, uid):
        """
        Function that runs a query to update the field geom of all the
        TramsAT
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """

        sql = """
            SELECT
                t.id AS id,
                ST_ASTEXT(ST_GeometryN(t_shp.geom, 1)) AS geom
            FROM giscegis_lat t_shp
            INNER JOIN giscedata_at_tram t ON t_shp.id=t.name;
        """
        cursor.execute(sql)
        rows = cursor.fetchall()
        for row in rows:
            self.write(cursor, uid, [int(row[0])], {"geom": row[1]})

    def fill_edge_id(self, cursor, uid):
        """
        Function that runs a query to update the field edge_id of all the
        TramsAT
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """

        sql = """
            UPDATE giscedata_at_tram AS t
            SET edge_id = data.edge
            FROM (
              SELECT at.id AS id,
                     e.id AS edge
                FROM giscedata_at_tram at
                LEFT JOIN giscegis_edge e ON at.name=e.id_linktemplate
                WHERE e.layer NOT ILIKE (
                    SELECT COALESCE(
                        (
                            SELECT value
                            FROM res_config
                            WHERE name ILIKE '%giscegis_btlike_layer%'
                        ), '%BT\_%'
                    )
                )
                AND at.geom IS NOT NULL
            ) AS data
            WHERE t.id = data.id
        """

        cursor.execute(sql)


GiscedataAtTram()
