# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction


class LATFillGeomTest(testing.OOTestCase):
    """"""

    def test_fill_geom(self):
        """
        Tests the fill_geom of LAT

        :return: None
        """

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            bt_obj = self.openerp.pool.get("giscedata.at.tram")
            bt_id = bt_obj.create(cursor, uid, {"name": "42"})

            sql = """
            INSERT INTO giscegis_lat (id,geom)
            VALUES (%(id)s,ST_GeomFromText('{}',25830));
            """

            line1_geom = 'MULTILINESTRING((2.62487 41.96714,2.81217 41.98347))'
            line2_geom = 'MULTILINESTRING((2.73207 41.96698,2.74516 41.96745))'

            cursor.execute(sql.format(line1_geom), {"id": bt_id})
            bt_obj.fill_geom(cursor, uid)
            data = bt_obj.read(cursor, uid, bt_id, ["longitud_cad"])
            self.assertEqual(data["longitud_cad"], 0.19)

            cursor.execute(sql.format(line2_geom), {"id": bt_id})
            bt_obj.fill_geom(cursor, uid)
            data = bt_obj.read(cursor, uid, bt_id, ["longitud_cad"])
            self.assertEqual(data["longitud_cad"], 0.01)
