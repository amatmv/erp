# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Elements BT",
    "description": """Modul per elements BT per autocad""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_vertex",
        "giscegis_nodes",
        "giscegis_base",
        "giscegis_condensadors"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscegis_blocs_elements_bt_view.xml",
        "ir.model.access.csv",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
