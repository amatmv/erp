# -*- coding: utf-8 -*-
from osv import osv
from tools import config


class GiscedataCondensador(osv.osv):
    _name = 'giscedata.condensadors'
    _inherit = 'giscedata.condensadors'

    def fill_geom_bt(self, cursor, uid):
        """
        Function that runs a query to update the field geom of all the
        Condensadors BT elements
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """
        sql = """
            UPDATE giscedata_condensadors AS c 
            SET geom = bloc.geom 
            FROM (
              SELECT b.codi AS name,
                     st_setsrid(st_makepoint(v.x, v.y), %(srid)s) AS geom
              FROM giscegis_elementsbt b
              LEFT JOIN giscegis_vertex v ON b.vertex = v.id
            ) AS bloc
            WHERE c.name = bloc.name
        """

        cursor.execute(sql, {"srid": config.get("srid", 25830)})

    def fill_rotation_bt(self, cursor, uid):
        """
        Function that runs a query to update the field rotation of all the
        Condensadors BT elements
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """
        sql = """
            UPDATE giscedata_condensadors AS c 
            SET rotation = 360-bloc.rotation 
            FROM (
              SELECT b.codi AS name,
                     b.rotation AS rotation
              FROM giscegis_elementsbt b
            ) AS bloc
            WHERE c.name = bloc.name
        """

        cursor.execute(sql)

    def fill_node_id_bt(self, cursor, uid):
        """
        Function that runs a query to update the field node_id of all the
        Condensadors BT elements
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """
        sql = """
            UPDATE giscedata_condensadors AS c 
            SET node_id = bloc.node_id 
            FROM (
              SELECT b.codi AS name,
                     b.node AS node_id
              FROM giscegis_elementsbt b
            ) AS bloc
            WHERE c.name = bloc.name
        """

        cursor.execute(sql)

    def fill_xarxa_bt(self, cursor, uid):
        """
        Function that runs a query to update the field xarxa of all the
        Condensadors BT elements
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """
        sql = """
            UPDATE giscedata_condensadors 
            SET xarxa = '2'
            WHERE name IN (
                SELECT codi AS name FROM giscegis_elementsbt
            )
        """

        cursor.execute(sql)


GiscedataCondensador()
