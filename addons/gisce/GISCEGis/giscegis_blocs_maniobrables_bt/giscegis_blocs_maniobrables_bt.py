# -*- coding: utf-8 -*-
from osv import fields, osv


class giscegis_blocs_maniobrables_bt(osv.osv):
    """Mòdul que crea una taula amb FK's cap a ir.model,
    només els models corresponent a elements maniobrables
    de BT estaran relacionats.
    """
    _name = "giscegis.blocs.maniobrables.bt"
    _description = "Elements Maniobrables BT."
    _columns = {
      'name': fields.many2one('ir.model','Model'),
    }

    _defaults = {

    }
    _order = 'id'


giscegis_blocs_maniobrables_bt()
