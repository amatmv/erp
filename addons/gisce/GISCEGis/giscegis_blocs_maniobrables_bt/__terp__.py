# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Maniobrables BT",
    "description": """ Mòdul que crea una taula amb FK's cap a ir.model, només els models corresponent a elements maniobrables de BT estaran relacionats.""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
