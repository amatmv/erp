from destral import testing
from destral.transaction import Transaction


class ItemMock(object):
    def __init__(self, identfifer):
        self.identifier = identfifer

    @property
    def id(self):
        return self.identifier


class CtsGeomTest(testing.OOTestCase):

    def test_catalog_elements_cts(self):
        """
        Test that are elements for CTS and how many of each type are searching
        with the catalog domain.
        :return: None
        """

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            # INICIALITZEM OBJECTES
            cts_obj = self.openerp.pool.get('giscedata.cts')
            ident = cts_obj.search(cursor, uid, [])[0]
            item = ItemMock(ident)
            cts_obj.get_lat_lon(cursor, uid, item)
