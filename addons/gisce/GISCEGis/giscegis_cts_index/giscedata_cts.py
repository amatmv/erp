from giscegis_base_index.giscegis_base_index import GiscegisBaseIndex
from osv import osv


class GiscegisBlocsCtat(osv.osv):

    _name = "giscegis.blocs.ctat"
    _inherit = "giscegis.blocs.ctat"
    _fields_to_watch = ["vertex"]


GiscegisBlocsCtat()


class GiscedataCts(GiscegisBaseIndex):
    """
    Class to index CTs if no active session
    """
    _name = 'giscedata.cts'
    _inherit = 'giscedata.cts'

    def get_lat_lon(self, cursor, uid, item, context=None):
        """
        Method to get latitude and longitude

        :param cursor: Database cursor
        :param uid: User id
        :param item: Item to index
        :param context: OpenERP context
        :return: Tuple with lat and lon
        """
        if context is None:
            context = {}
        srid = self.get_db_srid(cursor, uid)
        cursor.execute("""
        SELECT
            ST_Y(ST_TRANSFORM(geom,4326)) AS lat,
            ST_X(ST_Transform(geom, 4326)) AS lon
        FROM giscedata_cts 
        WHERE id=%(id)s
        """, {'id': item.id})
        res = cursor.fetchone()
        if not res:
            res = (0, 0)
        return res


GiscedataCts()
