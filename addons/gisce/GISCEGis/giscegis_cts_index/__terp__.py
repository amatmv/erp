# -*- coding: utf-8 -*-
{
    "name": "CTS index per a GIS",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Index per CTs per a GIS
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "giscedata_cts",
        "giscedata_cts_index",
        "giscegis_base_index",
        "giscegis_blocs_ctat",
        "giscegis_search_type"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscegis_search_type_data.xml"
    ],
    "active": False,
    "installable": True
}
