# *-* coding: utf-8 *-*

from osv import osv, fields

class giscegis_blocs_empalmes_grup_171(osv.osv):

    _name = 'giscegis.blocs.empalmes.grup.171'
    _auto = False
    _columns = {
      'x': fields.float('X', readonly=True),
      'y': fields.float('Y', readonly=True),
      'texte': fields.text('Texte', readonly=True),
      'blockname': fields.char('Blockname', size=50),
      'rotation': fields.float('Rotation'),
    }

    def init(self, cr):
        cr.execute("""drop view if exists giscegis_blocs_empalmes_grup_171""")
        cr.execute("""
          create or replace view giscegis_blocs_empalmes_grup_171 as (
          SELECT grup.id, vertex.x, vertex.y, grup.codi, blockname.name AS blockname, grup.rotation
          FROM giscegis_blocs_empalmes_grup grup
          LEFT JOIN giscegis_vertex vertex ON grup.vertex = vertex.id
          LEFT JOIN giscegis_blocs_empalmes_grup_blockname blockname ON grup.blockname = blockname.id)
        """)

giscegis_blocs_empalmes_grup_171()
