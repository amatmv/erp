# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* giscegis_blocs_empalmes_grup_171
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.14\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2013-07-09 16:50:23+0000\n"
"PO-Revision-Date: 2013-07-09 16:50:23+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: giscegis_blocs_empalmes_grup_171
#: constraint:ir.model:0
msgid "The Object name must start with x_ and not contain any special character !"
msgstr ""

#. module: giscegis_blocs_empalmes_grup_171
#: model:ir.module.module,shortdesc:giscegis_blocs_empalmes_grup_171.module_meta_information
msgid "GISCE GIS Blocs Empalmes Grup 1.7.1"
msgstr ""

#. module: giscegis_blocs_empalmes_grup_171
#: field:giscegis.blocs.empalmes.grup.171,texte:0
msgid "Texte"
msgstr ""

#. module: giscegis_blocs_empalmes_grup_171
#: field:giscegis.blocs.empalmes.grup.171,blockname:0
msgid "Blockname"
msgstr ""

#. module: giscegis_blocs_empalmes_grup_171
#: model:ir.model,name:giscegis_blocs_empalmes_grup_171.model_giscegis_blocs_empalmes_grup_171
msgid "giscegis.blocs.empalmes.grup.171"
msgstr ""

#. module: giscegis_blocs_empalmes_grup_171
#: model:ir.module.module,description:giscegis_blocs_empalmes_grup_171.module_meta_information
msgid "Vista dels empalmes grup pel MG-1.7.1"
msgstr ""

#. module: giscegis_blocs_empalmes_grup_171
#: field:giscegis.blocs.empalmes.grup.171,y:0
msgid "Y"
msgstr ""

#. module: giscegis_blocs_empalmes_grup_171
#: field:giscegis.blocs.empalmes.grup.171,x:0
msgid "X"
msgstr ""

#. module: giscegis_blocs_empalmes_grup_171
#: field:giscegis.blocs.empalmes.grup.171,rotation:0
msgid "Rotation"
msgstr ""

