from giscegis_base_index.giscegis_base_index import GiscegisBaseIndex


class GiscedataTransformadorTrafo(GiscegisBaseIndex):
    _name = "giscedata.transformador.trafo"
    _inherit = "giscedata.transformador.trafo"
    _index_fields = {
        "obres": lambda self, data: ' '.join(['obra{0}'.format(d.name) or '' for d in data])
    }

GiscedataTransformadorTrafo()

