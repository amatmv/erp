# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
import ast
from .test_session import *


class TrafoGeomTest(testing.OOTestCase):

    def test_catalog_elements_trafo(self):
        """
        Test that are elements for Trafos and how many of each type are
        searching with the catalog domain.
        :return: None
        """

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            # INICIALITZEM OBJECTES
            trafo_obj = self.openerp.pool.get('giscedata.transformador.trafo')
            cataleg_obj = self.openerp.pool.get('giscegis.cataleg.cataleg')
            sum_trafos = 0

            # SELECCIONEM UNICAMENT ELS ELEMENTS DE CATALEG QUE SIGUIN TRAFOS
            ids_trafo_cataleg = cataleg_obj.search(
                cursor, uid,
                [('model.model', '=', 'giscedata.transformador.trafo')]
            )

            # PER A CADA ELEMENT DEL CATALEG COMPROVAREM QUE HI HAGIN ELEMENTS
            cataleg_trafos_elems = cataleg_obj.read(
                cursor, uid, ids_trafo_cataleg, ['name', 'domain']
            )

            for cataleg_elem in cataleg_trafos_elems:

                trafo_ids = trafo_obj.search(
                    cursor, uid, ast.literal_eval(cataleg_elem['domain'])
                )

                sum_trafos += len(trafo_ids)
                if cataleg_elem['name'] == "Transformador":
                    self.assertEqual(len(trafo_ids), 4)
                elif cataleg_elem['name'] == "Transformador Reductor":
                    self.assertEqual(len(trafo_ids), 5)

            self.assertEqual(sum_trafos, 9)
