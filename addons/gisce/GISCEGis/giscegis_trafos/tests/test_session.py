from destral import testing
from destral.transaction import Transaction


__all__ = [
    'TestTrafoSession'
]


class TestTrafoSession(testing.OOTestCase):

    def setUp(self):
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user

    def tearDown(self):
        self.txn.stop()

    def test_search_session_trafo(self):
        """
        This test is for check the correct behaviour of the sessions and the
        Trafos
        """
        # Load ERP models
        trafos_obj = self.openerp.pool.get('giscedata.transformador.trafo')
        session_obj = self.openerp.pool.get('giscegis.session')

        # Clean the sessions to avoid BBox collisions
        sql = """
            TRUNCATE giscegis_session CASCADE
        """
        self.cursor.execute(sql)

        # Create the sessions
        session1_vals = {
            "name": 'Testing session 1',
            "bbox": 'POLYGON((982449.3 4664776.0, 982760.4 4664776.0, '
                    '982760.4 4664950.2, 982449.3 4664950.2, '
                    '982449.3 4664776.0))',
            "state": 'open',
            "ttl": 500000000,
            "active": True
        }
        session2_vals = {
            "name": 'Testing session 2',
            "bbox": 'POLYGON((981216.7 4664152.6, 981771.6 4664152.6, '
                    '981771.6 4664463.2, 981216.7 4664463.2, '
                    '981216.7 4664152.6))',
            "state": 'open',
            "ttl": 500000000,
            "active": True
        }
        session1_id = session_obj.create(self.cursor, self.uid, session1_vals)
        session2_id = session_obj.create(self.cursor, self.uid, session2_vals)

        # Start the checks
        # Check the number of Trafos in differents sessions and without sessions
        # No session
        trafos_ids = trafos_obj.search(self.cursor, self.uid, [])
        self.assertEqual(len(trafos_ids), 9)
        # Session 1
        trafos_ids = trafos_obj.search(
            self.cursor, self.uid, [], context={'session': session1_id}
        )
        self.assertEqual(len(trafos_ids), 0)
        # Session 2
        trafos_ids = trafos_obj.search(
            self.cursor, self.uid, [], context={'session': session2_id}
        )
        self.assertEqual(len(trafos_ids), 6)

        # Get 2 ids from the session 2
        trafo1_id = trafos_ids[0]
        trafo2_id = trafos_ids[1]

        # Check that the changes made on a session are only visible from the
        # session
        # Load data before changes
        trafos_data_original = trafos_obj.read(
            self.cursor, self.uid, [trafo1_id, trafo2_id], ['rotation']
        )
        # Write new data on 2 Elements Tall from session 2
        trafos_write_vals = {
            "rotation": 240
        }
        trafos_obj.write(
            self.cursor, self.uid, [trafo1_id, trafo2_id],
            trafos_write_vals, context={'session': session2_id}
        )
        # Check the changes outside the session
        trafos_data = trafos_obj.read(
            self.cursor, self.uid, [trafo1_id, trafo2_id], ['rotation']
        )
        # Compare data outside session before and after write data on session 2
        self.assertEqual(
            trafos_data_original[0]['rotation'],
            trafos_data[0]['rotation']
        )
        self.assertEqual(
            trafos_data_original[1]['rotation'],
            trafos_data[1]['rotation']
        )
        # Check the changes inside the session
        trafos_data = trafos_obj.read(
            self.cursor, self.uid, [trafo1_id, trafo2_id], ['rotation'],
            context={'session': session2_id}
        )
        for data in trafos_data:
            self.assertEqual(data['rotation'], 240)
