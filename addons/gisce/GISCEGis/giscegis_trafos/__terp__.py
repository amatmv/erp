# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS v3 Trafos_coord",
    "description": """Mòdul pel GIS v3 de Trafos_coord""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "giscegis_base",
        "giscedata_transformadors",
        "giscegis_base_geom",
        "giscegis_nodes"
    ],
    "init_xml": [],
    "demo_xml":[
        "giscegis_trafos_demo.xml"
    ],
    "update_xml": [
        "giscegis_cataleg_transformador_data.xml",
    ],
    "active": False,
    "installable": True
}
