# -*- coding: utf-8 -*-

from osv import osv


class SwitchTrafo(osv.osv_memory):
    _name = 'giscedata.switch.trafo'
    _inherit = 'giscedata.switch.trafo'

    def switch_trafo(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        context['trafo_copy_fields'] = ["geom", "rotation", "node_id"]
        return super(SwitchTrafo, self).switch_trafo(cursor, uid, ids, context)


SwitchTrafo()
