from osv import fields
from giscegis_base_geom.fields import Point
from tools.config import config
from giscegis_base_geom.giscegis_session import SessionManaged


class GiscedataTransformadorTrafo(SessionManaged):

    _name = 'giscedata.transformador.trafo'
    _inherit = 'giscedata.transformador.trafo'

    _columns = {
        "geom": Point("Geometria", srid=config.get("srid", 25830), select=True),
        "rotation": fields.integer("Rotation"),
        "node_id": fields.many2one("giscegis.nodes", "Node",
                                   ondelete='set null')
    }

    _defaults = {
        'rotation': lambda *a: 0,
    }


GiscedataTransformadorTrafo()
