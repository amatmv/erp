# -*- coding: utf-8 -*-

from osv import fields, osv
from tools import config


class GiscegisFontGeom(osv.osv):
    _name = "giscegis.font.geom"
    _description = "Trafos Llistat per GIS v3"
    _auto = False
    _columns = {
        'id': fields.integer('Id', readonly=True),
        'name': fields.char('Text', size=120, readonly=True),
        'rotation': fields.float('Rotation', readonly=True),
        'geom': fields.char('Geom', size=120, readonly=True),
        'texte': fields.char('Texte', size=120, readonly=True),
        "materialize_date": fields.datetime("Fecha creación vista"),
    }

    _defaults = {

    }
    _order = 'id'

    def init(self, cursor):

        cursor.execute("""
            DROP MATERIALIZED VIEW IF EXISTS giscegis_font_geom;
        """)

        cursor.execute("""
            CREATE MATERIALIZED VIEW public.giscegis_font_geom AS
                SELECT 
                    block.id AS id,
                    block.name AS name,
                    block.rotation AS rotation,
                    st_setsrid(
                        st_makepoint(vertex.x, vertex.y), %(srid)s
                    ) AS geom,
                    now() AS materialize_date
                FROM giscegis_blocs_netsource AS block
                LEFT JOIN giscegis_vertex vertex ON block.vertex = vertex.id;
            
            CREATE INDEX giscegis_font_geom_geom ON giscegis_font_geom 
            USING gist (geom);
        """, {'srid': config.get('srid', 25830)})

    def get_srid(self, cursor, uid):
        """
        :param cursor: cursor Database cursor
        :param uid: integer User identifier
        :return: Returns a list of srids in geom field
        """

        return [config.get('srid', 25830)]

    def recreate(self, cursor, uid):
        cursor.execute("""
            REFRESH MATERIALIZED VIEW giscegis_font_geom;
        """)


GiscegisFontGeom()
