# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS v3 (Fonts)",
    "description": """Mòdul de fonts per a giscegis v3""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_base_geom",
        "giscegis_blocs_netsource"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
