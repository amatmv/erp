# -*- coding: utf-8 -*-

from osv import fields, osv
from base64 import b64decode, b64encode


class GiscegisCatalegCataleg(osv.osv):
    _name = "giscegis.cataleg.cataleg"
    _description = "Cataleg d'elements per el QGIS"

    GEOM_TYPES = [
        ('Point', 'Punt'),
        ('LineString', 'Linia'),
        ('Polygon', 'Poligon')
    ]

    def _convert_svg_png(self, cursor, uid, ids, field_name, ar, context):
        """
        Converts svg data to PNG

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: Ids of cataleg
        :type ids: list of int
        :param field_name: Name of the field
        :type field_name: str
        :param context: Open ERP context
        :type context: dict
        :return: Image converted to PNG
        :rtype: str
        """

        from wand.image import Image
        data_cataleg = self.read(cursor, uid, ids, ["symbol"])
        ret = dict.fromkeys(ids, False)

        for data in data_cataleg:
            if data["symbol"]:
                with Image(blob=b64decode(data["symbol"]), format="svg") as image:
                    ret[data["id"]] = b64encode(image.make_blob("png"))
        return ret

    def _get_styles(self, cursor, uid, ids, context=None):
        """
        Return the list of elements to store the style

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: List of ids to calc
        :type ids: list[int]
        :param context: OpenERP context
        :return: List of elements to store
        :rtype: list[int]
        """
        return ids

    def _preview_style(self, cursor, uid, ids, field_name, ar, context):
        """
        Convert QML style file content into text to preview it on the form.

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: Ids of cataleg
        :type ids: list of int
        :param field_name: Name of the field
        :type field_name: str
        :param context: Open ERP context
        :type context: dict
        :return: QML file content into plain text.
        :rtype: str
        """

        data_cataleg = self.read(cursor, uid, ids, ["style_file"])
        res = dict.fromkeys(ids, False)

        for data in data_cataleg:
            if data["style_file"]:
                temp = b64decode(data["style_file"])
            else:
                temp = "N/D"
            res[data["id"]] = temp
        return res

    _columns = {
        "name": fields.char("Nom", size=256, required=True),
        "model": fields.many2one("ir.model", "Model", required=True),
        "height": fields.integer("Alçada"),
        "width": fields.integer("Amplada"),
        "style_file": fields.binary("Fitxer d'Estil", filters="*.qml"),
        "style": fields.function(
            _preview_style, type="text", string="Estil",
            readonly=True, method=True, store={
                    'giscegis.cataleg.cataleg': (
                        _get_styles, ['style_file'], 10
                    )
                },
        ),
        "symbol": fields.binary("Simbol", filters="*.svg"),
        "preview": fields.function(
            _convert_svg_png, type="binary", string="Previsualitzacio",
            readonly=True, method=True, stored=True
        ),
        "domain": fields.text("Domini", required=True),
        "geom_type": fields.selection(
            GEOM_TYPES, 'Tipus de geometria', required=True
        ),
        "fields": fields.text(
            "Camps", size=256, required=True,
            help="Camps necessaris del model del catàleg separats per comes"
        ),
        "z_index": fields.integer("Z-Index"),
        "not_topology": fields.boolean("No s'usa en la topologia"),
        "read_only": fields.boolean("Read-Only"),
        "snap_to": fields.many2one("giscegis.cataleg.cataleg", "Snap"),
        "active": fields.boolean("Actiu")
    }

    _defaults = {
        "domain": lambda *a: '[]',
        "fields": lambda *a: "",
        "z_index": lambda *a: 0,
        "read_only": lambda *a: False,
        "active": lambda *a: True,
    }

    _order = "z_index desc, name"


GiscegisCatalegCataleg()
