# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Cataleg",
    "description": """Representacio dels models del GIS""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscegis_cataleg_view.xml",
        "security/ir.model.access.csv",
    ],
    "active": False,
    "installable": True
}
