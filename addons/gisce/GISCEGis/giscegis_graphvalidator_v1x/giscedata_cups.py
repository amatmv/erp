# -*- coding: utf-8 -*-
"""Classes pel mòdul giscedata_cups de distribució."""
from osv import osv, fields
from tools.translate import _


class GiscedataCupsPs(osv.osv):
    """Modificació a CUPS. Afegim funcions pels camps de la v4 segons
       diccionari ```cups_trans_table```
    """
    _name = 'giscedata.cups.ps'
    _inherit = 'giscedata.cups.ps'

    cups_trans_table = {'carrer': 'nv',
                        'numero': 'pnp',
                        'escala': 'es',
                        'pis': 'pt',
                        'porta': 'pu', }

    def _get_v4_field(self, cr, uid, ids, name, arg, context=None):
        """Camp v4 a partir de camp v5"""
        if not context:
            context = None
        res = {}
        v5_field = self.cups_trans_table[name]
        vals = self.read(cr, uid, ids, [v5_field], context)
        for v in vals:
            res[v['id']] = v[v5_field]
        return res

    _columns = {
        'carrer': fields.function(_get_v4_field, method=True, type='char',
                                  string='Carrer'),
        'numero': fields.function(_get_v4_field, method=True, type='char',
                                  string='Número'),
        'escala': fields.function(_get_v4_field, method=True, type='char',
                                  string='Escala'),
        'pis': fields.function(_get_v4_field, method=True, type='char',
                               string='Pis'),
        'porta': fields.function(_get_v4_field, method=True, type='char',
                                 string='Porta'),
    }

    _defaults = {

    }

GiscedataCupsPs()
