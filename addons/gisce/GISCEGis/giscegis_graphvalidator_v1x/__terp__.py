# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS adaptació ERP 5 per GraphValidator v1.x",
    "description": """Les eines antigues (v1.x) no funcionen correctament amb amb els ERP's migrats aversió 5 perquè alguns models (p.e. CUPS) han canviat. Aquest mòdul només s'ha d'instal·lar quan no es poden instal·lar al client les noves eines (v14.x o superior)
Els models que s'actualitzen són:
 * giscedata_cups_ps""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscedata_cups"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
