# coding=utf-8

import logging
import pooler


def up(cursor, installed_version):

    logger = logging.getLogger('openerp.migration')
    logger.info('Migrating the rotation field into Grup Empalmes AT.')

    pool = pooler.get_pool(cursor.dbname)
    empalmes_at_obj = pool.get("giscedata.at.empalmes.grup")
    empalmes_at_obj.fill_rotation(cursor, 1)


def down(cursor, installed_version):
    pass


migrate = up
