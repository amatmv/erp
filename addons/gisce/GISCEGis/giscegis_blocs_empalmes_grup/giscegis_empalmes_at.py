# -*- encoding: utf-8 -*-
from osv import osv, fields
from tools import config
from psycopg2.extensions import AsIs
from giscegis_base_geom.wizard.giscegis_shp_loader \
    import GIS_MODELS_TO_UPDATE_GEOM_DATA


# Subscribe this model to the list to be updated if there is an AutoCAD dump
GIS_MODELS_TO_UPDATE_GEOM_DATA.append(
    {
        'model': 'giscedata.at.empalmes.grup',
        'functions': [
            'fill_geom', 'fill_rotation', 'fill_tipus_empalme', 'fill_node_id'
        ]
    },
)


class GiscedataAtEmpalmesGrup(osv.osv):

    _name = "giscedata.at.empalmes.grup"
    _inherit = "giscedata.at.empalmes.grup"

    def fill_geom(self, cursor, uid):
        """
        Fill the field geom for all the Empalmes AT elements with a sql query
        :param cursor: Database cursor
        :type cursor: Psycopg2 cursor
        :param uid: User id
        :type uid: int
        """
        
        sql = """
            UPDATE giscedata_at_empalmes_grup AS e 
            SET geom = b.geom 
            FROM (
              SELECT b.codi AS codi,
                     st_setsrid(st_makepoint(v.x, v.y), %(srid)s) AS geom
              FROM giscegis_blocs_empalmes_grup b
              LEFT JOIN giscegis_vertex v ON b.vertex = v.id
            ) AS b
            WHERE e.name=b.codi;
            UPDATE giscedata_at_empalmes_grup SET geom = St_SnapToGrid(
                ST_Translate(geom, 0.0000000001, 0.0000000001), 0.0001
            );
        """
        cursor.execute(sql, {"srid": AsIs(config.get("srid", 25830))})

    def fill_rotation(self, cursor, uid):
        """
        Fill the field rotation for all the Empalmes AT elements with a sql
        query
        :param cursor: Database cursor
        :type cursor: Psycopg2 cursor
        :param uid: User id
        :type uid: int
        """

        sql = """
            UPDATE giscedata_at_empalmes_grup AS e 
            SET rotation = b.rotation 
            FROM (
              SELECT b.codi AS codi,
                     b.rotation AS rotation
              FROM giscegis_blocs_empalmes_grup b
              LEFT JOIN giscegis_vertex v ON b.vertex = v.id
            ) AS b
            WHERE e.name=b.codi
        """
        cursor.execute(sql)

    def fill_tipus_empalme(self, cursor, uid):
        """
        Fill the field tipus_empalme for all the Empalmes AT elements with a
        sql query
        :param cursor: Database cursor
        :type cursor: Psycopg2 cursor
        :param uid: User id
        :type uid: int
        """

        sql = """
            UPDATE giscedata_at_empalmes_grup AS e 
            SET tipus_empalme = b.tipus_empalme 
            FROM (
              SELECT b.codi AS codi,
                     b.blockname AS tipus_empalme
              FROM giscegis_blocs_empalmes_grup b
              LEFT JOIN giscegis_vertex v ON b.vertex = v.id
            ) AS b
            WHERE e.name=b.codi
        """
        cursor.execute(sql)

    def _coord(self, cr, uid, ids, field_name, ar, context=None):
        """
        Returns the X and Y of the Empalme AT
        :param cursor: Database cursor
        :type cursor: Psycopg2 cursor
        :param uid: User id
        :type uid: int
        :param ids: Empalmes AT ids
        :type ids: list of int or int
        :return: X and Y of the Empalme AT
        :rtype: dict[int, dict[str, float or False]]
        """

        if context is None:
            context = {}
        res = dict([(i, dict([('coord_x', False), ('coord_y', False)]))
                   for i in ids])
        gis = self.pool.get('giscegis.blocs.empalmes.grup')
        grup_gis_ids = gis.search(cr, uid, [('grup.id', 'in', ids)])
        grup_gis = gis.browse(cr, uid, grup_gis_ids)
        for grup in grup_gis:
            if grup.vertex:
                res[grup.grup.id] = {'coord_x': grup.vertex.x,
                                     'coord_y': grup.vertex.y}
        return res

    def fill_node_id(self, cursor, uid):
        """
        Function that runs a query to update the field node_id of all the
        EmpalmesAT elements
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """

        sql = """
            UPDATE giscedata_at_empalmes_grup AS e 
            SET node_id = b.node
            FROM (
              SELECT b.codi AS codi,
                     b.node AS node
              FROM giscegis_blocs_empalmes_grup b
            ) AS b
            WHERE e.name=b.codi
        """

        cursor.execute(sql)

    _columns = {
        'coord_x': fields.function(
            _coord, method=True, type='float', digits=(7, 6),
            string='X', multi='coord'
        ),
        'coord_y': fields.function(
            _coord, method=True, type='float', digits=(7, 6),
            string='Y', multi='coord'
        )
    }

GiscedataAtEmpalmesGrup()