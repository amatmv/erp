# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Blocs Empalmes Grup",
    "description": """
Model dels empalmes pel GIS.

Configurar el DefDWG.xml segons:

* En l'apartat  <DefDataBases><DefDataBase><DefTables> afegir:

<DefTable ID="giscegis.blocs.empalmes.grup" TableName="giscegis.blocs.empalmes.grup" ForceDelete="false" CleanAfter="true"/>

* En l'apartat <DefBlockReferences> afegir:

<DefBlockReference ID="EMPALMES_GRUP" BlockNamePattern="^EM_MT$" Table="giscegis.blocs.empalmes.grup" TableDBcreation="">
<DefAttribute Tag="ID" Unique="true" Pattern=".+" DBfield="codi"/>
</DefBlockReference>
  """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_vertex",
        "giscegis_nodes",
        "giscedata_at",
        "giscegis_base",
        "giscegis_empalmes_at"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscegis_blocs_empalmes_grup_view.xml",
        "ir.model.access.csv",
        "security/ir.model.access.csv",
        "giscegis_empalmes_at_view.xml"
    ],
    "active": False,
    "installable": True
}
