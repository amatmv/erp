# *-* coding: utf-8 *-*

from osv import osv, fields

class giscegis_blocs_seccionador_unifilar_171(osv.osv):

    _name = 'giscegis.blocs.seccionador.unifilar.171'
    _auto = False
    _columns = {
      'id': fields.integer('Id', readonly=True),
      'x': fields.float('X', readonly=True),
      'y': fields.float('Y', readonly=True),
      'texte': fields.text('Texte', readonly=True),
      'blockname': fields.char('Blockname', size=50),
      'rotation': fields.float('Rotation'),
    }

    def init(self, cr):
        cr.execute("""drop view if exists
                      giscegis_blocs_seccionador_unifilar_171""")
        cr.execute("""
          create or replace view giscegis_blocs_seccionador_unifilar_171 as (
            select vertex.x, vertex.y, blockname.name || ' ' || seccionador.codi as texte, blockname.name as blockname, seccionador.rotation
    from giscegis_blocs_seccionadorunifilar seccionador
            left join giscegis_vertex vertex on (seccionador.vertex = vertex.id)
            left join giscegis_blocs_seccionador_unifilar_blockname blockname on (seccionador.blockname = blockname.id)    )""")

giscegis_blocs_seccionador_unifilar_171()
