# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Config Default AT",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscedata_qualitat",
        "giscegis_nodes",
        "giscegis_node_bloc_model",
        "giscegis_blocs_maniobrables_at",
        "giscegis_base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscegis_configdefault_at_view.xml",
        "ir.model.access.csv",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
