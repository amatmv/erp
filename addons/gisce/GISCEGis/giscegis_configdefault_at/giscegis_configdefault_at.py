# -*- coding: utf-8 -*-

from osv import fields, osv


class GiscegisConfigdefaultAt(osv.osv):
    _name = "giscegis.configdefault.at"
    _description = "Configuració interruptors oberts AT"
    _columns = {
        'name': fields.char('Nom', size=50, required=True),
        'node': fields.many2one('giscegis.nodes', 'Node', ondelete='cascade'),
        'codi': fields.char('Codi', size=50, required=True),
        'blockname_str': fields.char('Blockname', size=50, required=True),
    }

    _defaults = {
        'name': lambda *a: 'DEFAULT',
    }
    _order = "codi, blockname_str, name"

    def save_at(self, cr, uid, config="DEFAULT", ids=[], context={}):
        """
        Guarda l'estat de la xarxa amb els elements donats com a oberts.
        De moment es guarda tot al configdefault amb name=DEFAULT
        @ids: ids de nodes oberts
        """
        # algorisme:
        # per cada (node N, config) existent a giscegis.configdefault.at:
        #   si N està també a ids:
        #     pass
        #   si N no està a ids:
        #     esborra'l de la taula
        # per cada node I a ids
        #   si I existeix a giscegis.configdefault.at:
        #     pass
        #   else:
        #     afegir-ho a la taula
        try:
            cda_obj = self.pool.get('giscegis.configdefault.at')
            res = cda_obj.search(cr, uid, [('name', '=', config)])
            for cda in cda_obj.read(cr, uid, res, ['id', 'name', 'node']):
                if cda['node'] and cda['node'][0] not in ids:
                    # esborrar de la taula
                    cda_obj.unlink(cr, uid, cda['id'])
            nodes = cda_obj.read(
                cr, uid, cda_obj.search(cr, uid, []), ['node'])
            a_taula = [a['node'][0] for a in nodes if a['node']]
            for i in ids:
                if i not in a_taula:
                    node = self.pool.get('giscegis.nodes').browse(cr, uid, i)
                    blockname = node.get_bloc().blockname.name
                    values = {
                        'name': config,
                        'node': i,
                        'codi': node.get_codi(),
                        'blockname_str': blockname
                    }
                    cda_obj.create(cr, uid, values)
            return {'success': True}
        except Exception as e:
            print(e)
            return {'success': False}


GiscegisConfigdefaultAt()


class GiscegisOpenNodesAt(osv.osv):
    _name = 'giscegis.open.nodes.at'
    _description = "Interruptors oberts d'AT"
    _auto = False
    _columns = {
        'name': fields.char('Nom', size=50, readonly=True),
        'x': fields.float('X', readonly=True),
        'y': fields.float('Y', readonly=True),
    }

    def init(self, cr):
        """
        Creates a view with the open AT nodes

        :param cr: Database cursor
        :return: None
        """
        cr.execute("""DROP VIEW IF  EXISTS giscegis_open_nodes_at""")
        cr.execute("""
          CREATE OR REPLACE VIEW giscegis_open_nodes_at AS (
            SELECT
              n.name,
              v.x,
              v.y
            FROM giscegis_configdefault_at  AS cd
            LEFT JOIN giscegis_nodes AS n ON  (n.id=cd.node)
            LEFT JOIN giscegis_vertex AS v ON (n.vertex=v.id)
          )
        """)


GiscegisOpenNodesAt()


class GiscegisConfigDefaultAtNodes(osv.osv):

    def _codi(self, cr, uid, ids, field_name, *args):
        node_obj = self.pool.get('giscegis.nodes')
        return node_obj.get_codi_multi(cr, uid, ids)

    _name = "giscegis.configdefault.at.nodes"
    _description = "Configuració Oberts AT"
    _auto = False
    _columns = {
        'id': fields.integer('Node ID', readonly=True),
        'model': fields.integer('Model ID', readonly=True),
        'res_id': fields.integer('Resource ID', readonly=True),
        'name': fields.function(_codi, type='char', size=50, method=True),
    }

    def init(self, cr):
        """
        Creates a view with the AT nodes
        :param cr: Database cursor
        :return: None
        """
        cr.execute("""DROP VIEW IF EXISTS giscegis_configdefault_at_nodes""")
        cr.execute("""
          CREATE OR REPLACE VIEW
            giscegis_configdefault_at_nodes AS (
              select
                nbm.node as id,
                nbm.model,
                nbm.res_id
            FROM giscegis_node_bloc_model AS nbm
            WHERE model IN (
                SELECT id
                FROM ir_model
                WHERE model in (
                    'giscedata.celles.cella',
                    'giscedata.cts.subestacions.posicio'
                )
            )
            AND node NOT IN (
              SELECT node FROM giscegis_configdefault_at
            )
          )
        """)


GiscegisConfigDefaultAtNodes()
