from giscegis_base_index.giscegis_base_index import GiscegisBaseIndex


class GiscedataCupsPs(GiscegisBaseIndex):
    """
    Module to index giscedata.cups.ps
    """
    _name = 'giscedata.cups.ps'
    _inherit = 'giscedata.cups.ps'

    _index_title = '{obj.name} {obj.titular:.20}'
    _index_summary = '{obj.direccio}'

    _index_fields = {
        'name': True,
        'id_municipi.name': True,
        'polissa_polissa.name': True,
        'polissa_comptador': True,
        'direccio': True,
        'titular': True,
        'zona': True,
        'ordre': True,
        'et': True,
        'linia': lambda self, data: data and '{0} L{0}'.format(data) or '',
        'id_escomesa.name': True,
    }

    def get_lat_lon(self, cursor, uid, item, context=None):
        """
        Function to get lat and lon to index

        :param cursor: OpenERP database cursor
        :param uid: User id
        :param item: Item to index
        :param context: OpenERP context
        :return: lat and lon as a tuple
        """
        if context is None:
            context = {}
        cursor.execute("""
        SELECT
            ST_Y(ST_Transform(geom,4326)) AS lat,
            ST_X(ST_Transform(geom,4326)) AS lon
        FROM giscedata_cups_escomesa AS escomesa
        LEFT JOIN giscedata_cups_ps AS cups ON cups.id_escomesa=escomesa.id
        WHERE cups.id=%(id)s
        """, {'id': item.id})
        res = cursor.fetchone()
        if not res:
            res = (0, 0)
        return res

    def get_schema(self, cursor, uid, item, context=None):
        """
        Gets the index schema

        :param cursor: Database cursor
        :param uid: User id
        :param item: item to index
        :param context: OpenERP context
        :return: Dict with the schema
        """

        lat, lon = self.get_lat_lon(
            cursor,
            uid,
            item.id_escomesa,
            context=context
        )
        content = self.get_content(cursor, uid, item, context=context)
        if item.titular:
            title = self._index_title.format(obj=item)
            if len(item.titular) > 20:
                title = '{} ...'.format(title)
        else:
            title = '{obj.name}'.format(obj=item)

        return {
            'id': item.id,
            'path': self.get_path(cursor, uid, item.id, context=context),
            'title': title,
            'content': ' '.join(set(content)),
            'gistype': self._name,
            'summary': self._index_summary.format(obj=item),
            'lat': lat,
            'lon': lon
        }


GiscedataCupsPs()
