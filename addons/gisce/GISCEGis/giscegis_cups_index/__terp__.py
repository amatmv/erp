# -*- coding: utf-8 -*-
{
    "name": "Index CUPS",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Index for CUPS
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "base_index",
        "giscegis_base_index",
        "giscedata_cups",
        "giscegis_cups",
        "giscegis_search_type",
        "giscedata_cups_index"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscegis_search_type_data.xml"
    ],
    "active": False,
    "installable": True
}
