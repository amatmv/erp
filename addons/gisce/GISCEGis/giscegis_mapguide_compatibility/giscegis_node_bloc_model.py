from giscegis_base_geom.giscegis_session import SessionManaged


class GiscegisNodeBlocModel(SessionManaged):

    _name = 'giscegis.node.bloc.model'
    _inherit = 'giscegis.node.bloc.model'

    def generate(self, cursor, uid):
        """
        Fills the node bloc model table with mapguide compatibility

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :return: Number of entries generated
        :rtype: int
        """

        obj_esc = self.pool.get("giscedata.cups.escomesa")
        obj_trf = self.pool.get("giscedata.transformador.trafo")

        obj_esc.fill_node_id(cursor, uid)
        obj_trf.fill_node_id(cursor, uid)

        return super(GiscegisNodeBlocModel, self).generate(cursor, uid)


GiscegisNodeBlocModel()
