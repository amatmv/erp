# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS compatibilidad para Mapguide",
    "description": """Modulo de compatibilidad para Mapguide""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_base_geom",
        "giscegis_blocs_escomeses",
        "giscegis_nodes"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[],
    "active": False,
    "installable": True
}
