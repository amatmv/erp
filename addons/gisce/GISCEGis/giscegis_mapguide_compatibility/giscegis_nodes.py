from giscegis_base_geom.giscegis_session import SessionManaged
from psycopg2.extensions import AsIs


class GiscegisNodes(SessionManaged):

    _name = 'giscegis.nodes'
    _inherit = 'giscegis.nodes'

    def clean_non_vinculated(self, cursor, uid, context=None):
        """
        Deletes the non vinculated nodes

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param context: OpenERP context
        :return: Number of deleted nodes
        :rtype: int
        """

        sql_nodes_ids = """
            SELECT id 
            FROM giscegis_nodes 
        """
        cursor.execute(sql_nodes_ids)
        nodes_ids = cursor.fetchall()

        non_linked_nodes_ids = [ident[0] for ident in nodes_ids]

        models = ['%bloc%']
        ban_models = [
            '%171%',
            '%geom%',
            '%blockname',
            '%stop%',
            '%maniobrable%',
            'pg_activity%',
            'giscegis.blocs.suports',
            'giscegis.node.bloc.model',
            'giscegis_tensio_bloc'
        ]

        like_sql = ' or '.join(['model like %s'] * len(models))
        not_like_sql = ' and model not like %s' * len(ban_models)

        sql = """
            SELECT model
            FROM ir_model
            WHERE
        """
        sql += like_sql
        sql += '\n' + not_like_sql
        sql_params = models + ban_models
        cursor.execute(sql, sql_params)
        models_erp = cursor.fetchall()

        for model in models_erp:
            sql_nodes_ids_erp = """
                SELECT node 
                FROM %(model)s
                WHERE node IS NOT NULL
            """
            cursor.execute(
                sql_nodes_ids_erp, {"model": AsIs(model[0].replace(".", "_"))}
            )
            erp_ids = cursor.fetchall()
            erp_ids = [ident[0] for ident in erp_ids]
            non_linked_nodes_ids = list(
                set(non_linked_nodes_ids) - set(erp_ids)
            )

        sql_edges = """
            SELECT start_node
            FROM giscegis_edge
            UNION
            SELECT end_node
            FROM giscegis_edge;
        """
        cursor.execute(sql_edges)
        edge_node_ids = cursor.fetchall()
        edge_node_ids = [ident[0] for ident in edge_node_ids]
        non_linked_nodes_ids = list(
            set(non_linked_nodes_ids) - set(edge_node_ids)
        )

        if non_linked_nodes_ids:
            sql_nbm_delete = """
                DELETE FROM giscegis_node_bloc_model 
                WHERE node IN %(id)s
            """
            cursor.execute(sql_nbm_delete, {"id": tuple(non_linked_nodes_ids)})

            sql_delete = """
                DELETE FROM giscegis_nodes
                WHERE id IN %(ids)s
            """
            cursor.execute(sql_delete, {"ids": tuple(non_linked_nodes_ids)})
        return len(non_linked_nodes_ids)


GiscegisNodes()
