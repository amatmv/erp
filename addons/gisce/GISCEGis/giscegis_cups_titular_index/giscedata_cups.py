from giscegis_base_index.giscegis_base_index import GiscegisBaseIndex


class GiscedataCupsPs(GiscegisBaseIndex):
    _name = 'giscedata.cups.ps'
    _inherit = 'giscedata.cups.ps'

    _index_fields = {
        'titular': True,
        'name': True,
        'id_municipi.name': True,
        'polissa_polissa.name': True,
        'polissa_comptador': True,
        'direccio': True,
        'titular': True,
        'zona': True,
        'ordre': True,
        'et': True,
        'linia': lambda self, data: data and '{0} L{0}'.format(data) or '',
        'id_escomesa.name': True,
    }


GiscedataCupsPs()
