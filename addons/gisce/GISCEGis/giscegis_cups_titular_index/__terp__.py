# -*- coding: utf-8 -*-
{
    "name": "Index CUPS (titular) per a GIS",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Index for CUPS per al GIS
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "giscegis_cups_index",
        "giscedata_cups_titular"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
