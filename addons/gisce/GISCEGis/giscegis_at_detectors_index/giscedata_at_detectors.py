from giscegis_base_index.giscegis_base_index import GiscegisBaseIndex


class GiscedataAtDetectors(GiscegisBaseIndex):
    """
    Index for giscedata.at.detectors
    """
    _name = 'giscedata.at.detectors'
    _inherit = 'giscedata.at.detectors'

    _index_title = '{obj.name}'
    _index_summary = '{obj.name}'

    _index_fields = {
        'name': lambda self, value: '{} {}'.format(value, value.lstrip('0')),
        'blockname.name': True,
        'tipus_prog.temps_pila': True
    }

    def get_lat_lon(self, cursor, uid, item, context=None):
        """
        Gets the latitude and longitude of the detector
        :param cursor: Database cursor
        :param uid: User id
        :param item: Item to index
        :param context: OpenERP context
        :return: Tuple lat,lon
        """
        if context is None:
            context = {}
        srid = self.get_db_srid(cursor, uid)
        sql = """
        SELECT
          ST_Y(
            ST_Transform(ST_SetSRID(ST_Point(det_geom.x, det_geom.y), %(srid)s), 4326)
          ) AS lat,
          ST_X(
            ST_Transform(ST_SetSRID(ST_Point(det_geom.x, det_geom.y), %(srid)s), 4326)
          ) AS lon
        FROM giscedata_at_detectors det JOIN 
        giscegis_blocs_detector_geom det_geom ON det_geom.detector = det.id
        WHERE
          det.id = %(id)s
        """
        cursor.execute(sql, {'id': item.id, 'srid': srid})
        res = cursor.fetchone()
        if not res or not res[0]:
            res = (0, 0)

        return res


GiscedataAtDetectors()
