from base_index.base_index import BaseIndex


class GiscegisBlocsTensio(BaseIndex):
    _name = 'giscegis.blocs.tensio'
    _inherit = 'giscegis.blocs.tensio'
    _index_title = '{obj.name}'
    _index_summary = '{obj.codi}'

    _index_fields = {
        'name': True,
        'tensio': True,
        'codi': True,
    }

    def get_lat_lon(self, cursor, uid, item, context=None):
        if context is None:
            context = {}
        base_geom = self.pool.get("giscegis.base.geom")
        srid = base_geom.get_srid(cursor, uid)

        sql_geom = """
        SELECT st_x(st_setsrid(st_makepoint(vertex.x, vertex.y), %s)) AS x,
        st_y(st_setsrid(st_makepoint(vertex.x, vertex.y), %s)) AS y
        FROM giscegis_blocs_tensio tensio
        LEFT JOIN giscegis_vertex vertex ON tensio.vertex = vertex.id
        LEFT JOIN giscegis_blocs_tensio_blockname blockname
            ON tensio.blockname = blockname.id;
        """
        cursor.execute(sql_geom, (srid, item.id, ))
        res = cursor.fetchone()
        if not res:
            res = (0, 0)
        return res

GiscegisBlocsTensio()

