# -*- coding: utf-8 -*-
{
    "name": "Index Blocs tensio",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Index als blocs tensio
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "base_index",
        "giscegis_blocs_tensio"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
