from osv import osv, fields

class giscegis_gis(osv.osv):
    _name = 'giscegis.gis'
    _auto = False
    _columns = {
      'id': fields.integer('Id', readonly=True),
      'name': fields.char('Name', size=64, readonly=True),
      'last_updated': fields.datetime('Last update', readonly=True),
    }

    def init(self, cr):
        cr.execute("""drop view if exists giscegis_gis""")
        cr.execute("""
          create or replace view giscegis_gis as (
            select
                    1 as id,
                    'gisconfig'::char(64) as name,
                    to_char(coalesce(greatest(max(write_date), max(create_date)), '1998/12/31'), 'DD/MM/YYYY')  as last_updated
            from giscegis_blocs_ctat
          )
        """)

giscegis_gis()
