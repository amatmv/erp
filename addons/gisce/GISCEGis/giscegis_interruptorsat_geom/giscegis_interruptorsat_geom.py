# -*- coding: utf-8 -*-
from osv import osv, fields
from tools import config


class GiscegisInterruptorsatGeom(osv.osv):
    _name = 'giscegis.interruptorsat.geom'
    _auto = False

    def _coords(self, cursor, uid, ids, field_name, arg, context):
        """
        Returns the x and y of the CT
        :param cursor: Database cursor
        :param uid: User id
        :param ids: Cts ids
        :type ids: list
        :param field_name: Asked fields
        :param arg:
        :param context: OpenERP context
        :type context: dict
        :return: X and y of the CTs
        :rtype: dict
        """
        if isinstance(ids, int):
            ids = [ids]
        ret = {}
        sql = """
        SELECT id, ST_X(geom), ST_Y(geom) FROM giscegis_interruptorsat_geom
        WHERE id IN %s;
        """
        cursor.execute(sql, (tuple(ids),))
        data = cursor.fetchall()

        for line in data:
            ret[line[0]] = {"x": line[1], "y": line[2]}
            ids.remove(line[0])
        for identifier in ids:
            ret[identifier] = {"x": None, "y": None}
        return ret

    _columns = {
        'id': fields.integer('Id', readonly=True),
        'symbol': fields.char('Symbol', readonly=True, size=256),
        'codi': fields.char('Codi', readonly=True, size=256),
        'geom': fields.char('Geom', readonly=True, size=256),
        'rotation': fields.float('Rotation', readonly=True),
        'descripcio': fields.char('Descripcio', readonly=True, size=256),
        'installacio': fields.char('Installacio', readonly=True, size=256),
        'inventari': fields.char('Inventari', readonly=True, size=256),
        'data_pm': fields.char('Data PM', readonly=True, size=256),
        'cini': fields.char('CINI', readonly=True, size=256),
        'ti': fields.char('TI', readonly=True, size=256),
        'tipus_element': fields.char('Tipus element', readonly=True, size=256),
        'numero_serie': fields.char('Numero serie', readonly=True, size=256),
        'producte': fields.char('Producte', readonly=True, size=256),
        'intensitat': fields.float('Intensitat', readonly=True),
        'x': fields.function(_coords, method=True, type='float', string='x',
                             multi="coords"),
        'y': fields.function(_coords, method=True, type='float', string='y',
                             multi="coords"),
        "materialize_date": fields.datetime("Fecha creación vista"),
    }

    def init(self, cursor):
        cursor.execute("""
        DROP MATERIALIZED VIEW IF EXISTS giscegis_interruptorsat_geom;
        """)
        cursor.execute("""
        CREATE MATERIALIZED VIEW public.giscegis_interruptorsat_geom AS
            SELECT
                ('1' || block.id::varchar)::int AS id,
                blockname.name AS symbol,
                block.codi AS codi,
                ST_SETSRID(st_makepoint(vertex.x, vertex.y), %(srid)s) AS geom,
                block.rotation AS rotation,
                cel.descripcio AS descripcio,
                cel.installacio AS installacio,
                cel.inventari AS inventari,
                cel.data_pm AS data_pm,
                cel.cini AS cini,
                ti.descripcio AS ti,
                tipus.name AS tipus_element,
                stock.name AS numero_serie,
                '[' || product.default_code || ']' || ' ' 
                || COALESCE(prod_tmp.name,'') || ' -' 
                || COALESCE(product.variants,'') AS producte,
                NULL::double precision AS intensitat,
                now() AS materialize_date
            FROM giscegis_blocs_seccionadorunifilar block
            JOIN giscedata_celles_cella cel ON 
                block.codi = cel.name
            LEFT JOIN giscegis_vertex vertex ON block.vertex = vertex.id
            LEFT JOIN giscegis_blocs_seccionador_unifilar_blockname blockname
                ON block.blockname = blockname.id
            LEFT JOIN giscedata_tipus_installacio ti
                ON cel.tipus_instalacio_cnmc_id = ti.id
            LEFT JOIN stock_production_lot stock ON
                cel.serial = stock.id
            LEFT JOIN product_product product ON
                stock.product_id = product.id
            LEFT JOIN product_template prod_tmp ON
                product.product_tmpl_id = prod_tmp.id
            LEFT JOIN giscedata_celles_tipus_element tipus ON
                tipus.id=cel.tipus_element
        UNION
            SELECT 
                ('2' || block.id::varchar)::int AS id,
                blockname.name AS symbol,
                block.codi AS codi,
                ST_SETSRID(st_makepoint(vertex.x, vertex.y), %(srid)s) AS geom,
                block.rotation AS rotation,
                cel.descripcio AS descripcio,
                cel.installacio AS installacio,
                cel.inventari AS inventari,
                cel.data_pm AS data_pm,
                cel.cini AS cini,
                ti.descripcio AS ti,
                tipus.name AS tipus_element,
                stock.name as numero_serie,
                '[' || product.default_code || ']' || ' ' 
                || COALESCE(prod_tmp.name, '') || ' -' 
                || COALESCE(product.variants,'') AS producte,
                NULL::double precision AS intensitat,
                now() AS materialize_date
            FROM giscegis_blocs_seccionadorat block
            JOIN giscedata_celles_cella cel ON 
                block.codi = cel.name
            LEFT JOIN giscegis_vertex vertex ON block.vertex = vertex.id
            LEFT JOIN giscegis_blocs_seccionadorat_blockname blockname ON 
                block.blockname = blockname.id
            LEFT JOIN giscedata_tipus_installacio ti
                ON cel.tipus_instalacio_cnmc_id = ti.id
            LEFT JOIN giscedata_celles_tipus_element tipus ON
                cel.tipus_element = tipus.id
            LEFT JOIN stock_production_lot stock ON
                cel.serial = stock.id
            LEFT JOIN product_product product ON
                stock.product_id = product.id
            LEFT JOIN product_template prod_tmp ON
                product.product_tmpl_id = prod_tmp.id
        UNION
            SELECT
                ('3' || block.id::varchar)::int AS id,
                blockname.name AS symbol,
                block.codi AS codi,
                ST_SETSRID(st_makepoint(vertex.x, vertex.y), %(srid)s) AS geom,
                block.rotation AS rotation,
                cel.descripcio AS descripcio,
                cel.installacio AS installacio,
                cel.inventari AS inventari,
                cel.data_pm AS data_pm,
                cel.cini AS cini,
                ti.descripcio AS ti,
                tipus.name AS tipus_element,
                stock.name AS numero_serie,
                '[' || product.default_code || ']' || ' ' 
                || COALESCE(prod_tmp.name,'') || ' -' 
                || COALESCE(product.variants,'') AS producte,
                block.intensitat AS intensitat,
                now() AS materialize_date
            FROM giscegis_blocs_fusiblesat block
            JOIN giscedata_celles_cella AS cel ON block.codi = cel.name 
            LEFT JOIN giscegis_vertex vertex ON block.vertex = vertex.id
            LEFT JOIN giscegis_blocs_fusiblesat_blockname blockname ON 
                block.blockname = blockname.id
            LEFT JOIN giscedata_tipus_installacio ti ON 
                cel.tipus_instalacio_cnmc_id = ti.id
            LEFT JOIN giscedata_celles_tipus_element tipus ON
                cel.tipus_element = tipus.id
            LEFT JOIN stock_production_lot stock ON
                cel.serial = stock.id
            LEFT JOIN product_product product ON
                stock.product_id = product.id
            LEFT JOIN product_template prod_tmp ON
                product.product_tmpl_id = prod_tmp.id
        UNION
            SELECT
                ('4' || block.id::varchar)::int AS id,
                blockname.name AS symbol,
                block.codi AS codi,
                ST_SETSRID(st_makepoint(vertex.x, vertex.y), %(srid)s) AS geom,
                block.rotation AS rotation,
                cel.descripcio AS descripcio,
                cel.installacio AS installacio,
                cel.inventari AS inventari,
                cel.data_pm AS data_pm,
                cel.cini AS cini,
                ti.descripcio AS ti,
                tipus.name AS tipus_element,
                stock.name AS numero_serie,
                '[' || product.default_code || ']' || ' '
                || COALESCE(prod_tmp.name,'') || ' -' 
                || COALESCE(product.variants,'') AS producte,
                NULL::double precision AS intensitat,
                now() AS materialize_date
            FROM giscegis_blocs_interruptorat block
            JOIN giscedata_celles_cella AS cel ON 
                block.codi = cel.name
            LEFT JOIN giscegis_vertex vertex ON block.vertex = vertex.id
            LEFT JOIN giscegis_blocs_interruptorat_blockname blockname ON
                block.blockname = blockname.id
            LEFT JOIN giscedata_tipus_installacio ti
                ON cel.tipus_instalacio_cnmc_id = ti.id
            LEFT JOIN giscedata_celles_tipus_element tipus ON
                cel.tipus_element = tipus.id
            LEFT JOIN stock_production_lot stock ON
                cel.serial = stock.id
            LEFT JOIN product_product product ON
                stock.product_id = product.id
            LEFT JOIN product_template prod_tmp ON
                product.product_tmpl_id = prod_tmp.id
        UNION
            SELECT
                ('5' || block.id::varchar)::int AS id,
                blockname.name AS symbol,
                block.codi AS codi,
                ST_SETSRID(st_makepoint(vertex.x, vertex.y), %(srid)s) AS geom,
                block.rotation AS rotation,
                CASE WHEN pos.id IS null THEN ' '
                     WHEN pos.interruptor='1'::text THEN 'PAR.'
                     WHEN pos.interruptor='2'::text THEN 'INT. AUTO.'
                     ELSE 'S. INT.'
                END AS descripcio,
                'giscedata.cts.subestacions.posicio,'::text 
                || pos.subestacio_id::text AS installacio,
                ' '::text AS inventari,
                pos.data_pm AS data_pm,
                pos.cini AS cini,
                ti.descripcio AS ti,
                CASE WHEN pos.id IS null THEN ' '
                     WHEN pos.interruptor='1'::text THEN 'Parc'
                     WHEN pos.interruptor='2'::text THEN 'Interruptor Automàtic'
                     ELSE 'Sense Interruptor'
                END AS tipus_element,
                stock.name AS numero_serie,
                '[' || product.default_code || ']' || ' '
                || COALESCE(prod_tmp.name,'') || ' -' 
                || COALESCE(product.variants,'') AS producte,
                NULL::double precision AS intensitat,
                now() AS materialize_date
            FROM giscegis_blocs_interruptorat block
            JOIN giscedata_cts_subestacions_posicio AS pos ON 
                block.codi = pos.name
            LEFT JOIN giscegis_vertex vertex ON block.vertex = vertex.id
            LEFT JOIN giscegis_blocs_interruptorat_blockname blockname ON
                block.blockname = blockname.id
            LEFT JOIN giscedata_tipus_installacio ti ON
                pos.tipus_instalacio_cnmc_id = ti.id
            LEFT JOIN stock_production_lot stock ON
                pos.serial = stock.id
            LEFT JOIN product_product product ON
                stock.product_id = product.id
            LEFT JOIN product_template prod_tmp ON
                product.product_tmpl_id = prod_tmp.id
        UNION
            SELECT
                ('6' || block.id::varchar)::int AS id,
                blockname.name AS symbol,
                block.codi AS codi,
                ST_SETSRID(st_makepoint(vertex.x, vertex.y), %(srid)s) AS geom,
                block.rotation AS rotation,
                CASE WHEN pos.id IS null THEN ' '
                     WHEN pos.interruptor='1'::text THEN 'PAR.'
                     WHEN pos.interruptor='2'::text THEN 'INT. AUTO.'
                     ELSE 'S. INT.'
                END AS descripcio,
                'giscedata.cts.subestacions.posicio,'::text 
                || pos.subestacio_id::text AS installacio,
                ' '::text AS inventari,
                pos.data_pm AS data_pm,
                pos.cini AS cini,
                ti.descripcio AS ti,
                CASE WHEN pos.id IS null THEN ' '
                     WHEN pos.interruptor='1'::text THEN 'Parc'
                     WHEN pos.interruptor='2'::text THEN 'Interruptor Automàtic'
                     ELSE 'Sense Interruptor'
                END AS tipus_element,
                stock.name as numero_serie,
                '[' || product.default_code || ']' || ' ' 
                || COALESCE(prod_tmp.name,'') || ' -' 
                || COALESCE(product.variants,'') AS producte,
                NULL::double precision AS intensitat,
                now() AS materialize_date
            FROM giscegis_blocs_fusiblesat block
            JOIN giscedata_cts_subestacions_posicio AS pos ON
                block.codi = pos.name 
            LEFT JOIN giscegis_vertex vertex ON block.vertex = vertex.id
            LEFT JOIN giscegis_blocs_interruptorat_blockname blockname
                ON block.blockname = blockname.id
            LEFT JOIN giscedata_tipus_installacio ti
                ON pos.tipus_instalacio_cnmc_id = ti.id
            LEFT JOIN stock_production_lot stock ON
                pos.serial = stock.id
            LEFT JOIN product_product product ON
                stock.product_id = product.id
            LEFT JOIN product_template prod_tmp ON
                product.product_tmpl_id = prod_tmp.id
        UNION
            SELECT
                ('7' || block.id::varchar)::int AS id,
                blockname.name AS symbol,
                block.codi AS codi,
                ST_SETSRID(st_makepoint(vertex.x, vertex.y), %(srid)s) AS geom,
                block.rotation AS rotation,
                CASE WHEN pos.id IS null THEN ' '
                     WHEN pos.interruptor='1'::text THEN 'PAR.'
                     WHEN pos.interruptor='2'::text THEN 'INT. AUTO.'
                     ELSE 'S. INT.'
                END AS descripcio,
                'giscedata.cts.subestacions.posicio,'::text 
                || pos.subestacio_id::text AS installacio,
                ' '::text AS inventari,
                pos.data_pm AS data_pm,
                pos.cini AS cini,
                ti.descripcio AS ti,
                CASE WHEN pos.id IS null THEN ' '
                     WHEN pos.interruptor='1'::text THEN 'Parc'
                     WHEN pos.interruptor='2'::text THEN 'Interruptor Automàtic'
                     ELSE 'Sense Interruptor'
                END AS tipus_element,
                stock.name AS numero_serie,
                '[' || product.default_code || ']' || ' '
                || COALESCE(prod_tmp.name,'') || ' -'
                || COALESCE(product.variants,'') AS producte,
                NULL::double precision AS intensitat,
                now() AS materialize_date
            FROM giscegis_blocs_seccionadorat block
            JOIN giscedata_cts_subestacions_posicio AS pos ON 
                block.codi = pos.name
            LEFT JOIN giscegis_vertex vertex ON block.vertex = vertex.id
            LEFT JOIN giscegis_blocs_interruptorat_blockname blockname
                ON block.blockname = blockname.id
            LEFT JOIN giscedata_tipus_installacio ti
                ON pos.tipus_instalacio_cnmc_id = ti.id
            LEFT JOIN stock_production_lot stock ON
                pos.serial = stock.id
            LEFT JOIN product_product product ON
                stock.product_id = product.id
            LEFT JOIN product_template prod_tmp ON
                product.product_tmpl_id = prod_tmp.id
        UNION
            SELECT
                ('8' || block.id::varchar)::int AS id,
                blockname.name AS symbol,
                block.codi AS codi,
                ST_SETSRID(st_makepoint(vertex.x, vertex.y), %(srid)s) AS geom,
                block.rotation AS rotation,
                CASE WHEN pos.id IS null THEN ' '
                     WHEN pos.interruptor='1'::text THEN 'PAR.'
                     WHEN pos.interruptor='2'::text THEN 'INT. AUTO.'
                     ELSE 'S. INT.'
                END AS descripcio,
                'giscedata.cts.subestacions.posicio,'::text
                || pos.subestacio_id::text AS installacio,
                ' '::text AS inventari,
                pos.data_pm AS data_pm,
                pos.cini AS cini,
                ti.descripcio AS ti,
                CASE WHEN pos.id IS null THEN ' '
                     WHEN pos.interruptor='1'::text THEN 'Parc'
                     WHEN pos.interruptor='2'::text THEN 'Interruptor Automàtic'
                     ELSE 'Sense Interruptor'
                END AS tipus_element,
                stock.name as numero_serie,
                '[' || product.default_code || ']' || ' ' 
                || COALESCE(prod_tmp.name,'') || ' -'
                || COALESCE(product.variants,'') AS producte,
                NULL::double precision AS intensitat,
                now() AS materialize_date
            FROM giscegis_blocs_seccionadorunifilar block
            JOIN giscedata_cts_subestacions_posicio AS pos ON
                block.codi = pos.name
            LEFT JOIN giscegis_vertex vertex ON block.vertex = vertex.id
            LEFT JOIN giscegis_blocs_interruptorat_blockname blockname
                ON block.blockname = blockname.id
            LEFT JOIN giscedata_tipus_installacio ti
                ON pos.tipus_instalacio_cnmc_id = ti.id
            LEFT JOIN stock_production_lot stock ON
                pos.serial = stock.id
            LEFT JOIN product_product product ON
                stock.product_id = product.id
            LEFT JOIN product_template prod_tmp ON
                product.product_tmpl_id = prod_tmp.id;
        """, {'srid': config.get('srid', 25830)})
        cursor.execute("""
        CREATE INDEX giscegis_interruptorsat_geom_geom
        ON giscegis_interruptorsat_geom USING gist (geom);
        """)

    def get_srid(self, cursor, uid):
        cursor.execute("""
        SELECT ST_SRID(geom) AS srid FROM giscegis_interruptorsat_geom LIMIT 1;
        """)
        data = cursor.fetchall()
        if len(data) > 0:
            return data[0][0]
        else:
            return False

    def recreate(self, cursor, uid):
        cursor.execute("""
        REFRESH MATERIALIZED VIEW giscegis_interruptorsat_geom
        """)
        return True


GiscegisInterruptorsatGeom()
