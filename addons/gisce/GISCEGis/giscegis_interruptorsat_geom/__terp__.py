# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS interruptors AT per v3",
    "description": """Vista d'interruptors AT per Giscegis v3""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_base",
        "giscegis_base_geom",
        "giscedata_celles_subestacions",
        "giscegis_blocs_seccionador_unifilar",
        "giscegis_blocs_seccionadorat",
        "giscegis_blocs_fusiblesat",
        "giscegis_blocs_interruptorat",
        "giscegis_elements_tall",
        "giscegis_cts_subestacions"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/ir.model.access.csv",
        "giscegis_layers_data.xml"
    ],
    "active": False,
    "installable": True
}
