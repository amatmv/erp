# *-* coding: utf-8 *-*
from osv import osv, fields
from giscegis_base_geom.giscegis_base_geom import DROP_ON_AUTOCAD_DUMP

# Subscribe this model to the list to be updated if there is an AutoCAD dump
DROP_ON_AUTOCAD_DUMP.append('giscegis_vertex')


class giscegis_vertex(osv.osv):
    _name = 'giscegis.vertex'
    _description = 'Vertex'

    def clean(self, cr, uid):
        cr.execute("update giscegis_vertex set active = False where active = True")
        cr.commit()
        return True

    def clean_no_actives(self, cr, uid):
        cr.execute("delete from giscegis_vertex where active = False")
        cr.commit()
        return True

    def name_get(self, cursor, uid, ids, context=None):
        if not len(ids):
            return []
        if context is None:
            context = {}
        res = []
        for vertex in self.read(cursor, uid, ids,['x', 'y'], context):
            name = '{},{}'.format(vertex['x'], vertex['y'])
            res.append([vertex['id'], name])
        return res

    def create(self, cr, uid, vals, context={}):
        """
        Sobreescrivim el mètode create per tal d'evitar duplicats'
        Així des de l'exterior podem cridar sempre el mètode create i ens retornarà
        l'id del vertex
        """
        id = self.search(cr, uid, [['x', '=', vals['x']], ['y', '=', vals['y']]], context={'active_test': False})
        if id and len(id):
            self.write(cr, uid, id, {'active': 1})
            return id[0]
        else:
            return super(osv.osv, self).create(cr, uid, vals, context)

    _columns = {
      'name': fields.char('IDNode', size=10),
      'x': fields.float('X', required=True),
      'y': fields.float('Y', required=True),
      'active': fields.boolean('Active', required=True),
    }

    _defaults = {
      'active': lambda *a: 1,
    }

    _order = "name, id"
giscegis_vertex()
