# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Vertex",
    "description": """Modulo para la gestion de vertices del GIS""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_base"
    ],
    "init_xml": [],
    "demo_xml":[
        "giscegis_vertex_demo.xml"
    ],
    "update_xml":[
        "ir.model.access.csv",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
