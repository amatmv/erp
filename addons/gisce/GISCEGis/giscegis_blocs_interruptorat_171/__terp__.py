# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Blocs Interruptors AT 1.7.1",
    "description": """Vista dels interruptors at pel MG-1.7.1""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_blocs_interruptorat",
        "giscegis_base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "ir.model.access.csv",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
