# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Municipis",
    "description": """
    Modul GIS per als municipis on s'els hi afegeix la geometria. 
    Dades obtingudes de l'IGNE: Instituto Geográfico Nacional de España.
    (La información geográfica digital comprendida en el Equipamiento Geográfico 
    de Referencia Nacional (EGRN), (artículo 1.1 de la Orden FOM/956/2008 ) y 
    los Metadatos de los datos geográficos producidos por el IGN y de los 
    servicios de información geográfica prestados por el IGN y el CNIG, 
    no requieren la aceptación de licencia , y su uso, en cualquier caso, 
    tendrá carácter libre y gratuito, siempre que se mencione al IGN 
    como origen y propietario de los datos)
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "base_extended",
        "giscegis_base",
        "giscegis_base_geom",
        "giscedata_cups"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "ir.model.access.csv",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
