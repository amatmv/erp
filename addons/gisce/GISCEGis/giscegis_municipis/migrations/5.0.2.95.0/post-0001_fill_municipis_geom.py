# coding=utf-8

import logging
import pooler


def up(cursor, installed_version):

    logger = logging.getLogger('openerp.migration')

    logger.info(
        'Filling field geom from res_municipi'
    )

    pool = pooler.get_pool(cursor.dbname)
    municipi_obj = pool.get('res.municipi')
    municipi_obj.update_municipis_geom(cursor, 1)

    logger.info(
        'Field geom filled for model res_municipi'
    )


def down(cursor, installed_version):
    pass


migrate = up
