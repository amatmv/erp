# -*- coding: utf-8 -*-

from osv import osv, fields
from giscegis_base_geom.giscegis_session import SessionManaged
from giscegis_base_geom.fields import Multipolygon
from tools import config

from addons import get_module_resource
import tqdm
import sys
import csv
from zipfile import ZipFile
import logging


class giscegis_municipis_installations_view(osv.osv):

    _name = "giscegis.municipis.installations.view"
    _description = "Municipis on hi ha instal·lacions"
    _auto = False
    _columns = {
      'id': fields.integer('Id'),
      'name': fields.char('Municipi', size=60, readonly=True),
      'ine': fields.char('Codi INE', size=10, readonly=True),
    }

    _order = "ine"

    def init(self, cr):
        cr.execute("""drop view if exists
                      giscegis_municipis_installations_view""")
        cr.execute("""
          create or replace view giscegis_municipis_installations_view as (
            select res_municipi.id as id, res_municipi.name as name, res_municipi.ine as ine from res_municipi where res_municipi.id in (select distinct giscedata_cups_ps.id_municipi from giscedata_cups_ps)
            )""")


giscegis_municipis_installations_view()


class ResMunicipi(SessionManaged):

    _name = 'res.municipi'
    _inherit = 'res.municipi'

    def update_municipis_geom(self, cursor, uid):
        """
        ToDo
        :param cursor:
        :param uid:
        :return:
        """
        csv.field_size_limit(sys.maxsize)
        srid = config.get('srid', '25830')
        municipi_obj = self.pool.get('res.municipi')
        logger = logging.getLogger('openerp.res.municipi.geom_update')

        if srid == '25830':
            input_file_zip = get_module_resource(
                'giscegis_municipis', 'resources', '25830', 'mun25830.zip'
            )
            input_file = "municipis_espanya_25830.csv"
        elif srid == '25831':
            input_file_zip = get_module_resource(
                'giscegis_municipis', 'resources', '25831', 'mun25831.zip'
            )
            input_file = "municipis_espanya_25831.csv"
        else:
            msg = "SRID {} no suportat. Abortant actualització.".format(
                srid
            )
            logger.error(msg)
            return

        with ZipFile(input_file_zip) as f_zip:
            with f_zip.open(input_file, "r") as f_in:
                csv_reader = csv.reader(f_in, delimiter=';', quotechar='"')
                total = 0
                for row in csv_reader:
                    total += 1

            with f_zip.open(input_file, "r") as f_in:
                csv_reader = csv.reader(f_in, delimiter=';', quotechar='"')
                for row in tqdm.tqdm(csv_reader, total=total):
                    # ROW[8] -> Codigo INE
                    # ROW[0] -> GEOM in WKT
                    # ROW[4] -> Nom Muninicipi
                    if row[8] == 'CODIGOINE':
                        continue
                    mun_id = municipi_obj.search(
                        cursor, uid, [('ine', '=', row[8])]
                    )
                    if mun_id:
                        mun_id = mun_id[0]
                    else:
                        msg = "Municipi \"{}\" amb INE {} no trobat.".format(
                            row[4], row[8]
                        )
                        logger.warning(msg)
                        continue
                    municipi_obj.write(
                        cursor, uid, [mun_id], {'geom': row[0]}
                    )

    _columns = {
        'geom': Multipolygon(
            "Geometria", srid=config.get('srid', 25830), select=True
        )
    }


ResMunicipi()
