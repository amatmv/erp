# -*- coding: utf-8 -*-

from osv import fields, osv
try:
    import json
except ImportError:
    import simplejson as json

class giscegis_camera_config(osv.osv):
    _name = "giscegis.camera.config"
    _description = "Configuració de les càmeres"
    _columns = {
      'name': fields.char('Mountpoint', size=128, required=True),
      'url': fields.char('Base URL', size=256, required=True),
      'active': fields.boolean('Activa', select=True),
    }
    _defaults = {
      'active': lambda *a: 1,
    }

    def write(self, cr, uid, ids, vals, context={}):
        """Sobreescribim el write per tal que regeneri la vista de càmeres."""
        res = super(osv.osv, self).write(cr, uid, ids, vals, context)
        self.pool.get('giscegis.cameres').init(cr)
        return res

    def get_active_mountpoint(self, cr, uid, context={}):
        cr.execute("SELECT name FROM giscegis_camera_config WHERE active = true ORDER BY id desc LIMIT 1")
        config = cr.dictfetchone()
        if config:
            return config.get('name')
        return None


giscegis_camera_config()

class giscegis_camera(osv.osv):
    _name = "giscegis.camera"
    _description = "Camera per Album"
    _columns = {
      'id': fields.integer('Id', readonly=True),
      'name': fields.char('Nom', size=50, required=True),
      'vertex': fields.many2one('giscegis.vertex', 'Vertex'),
      'active': fields.boolean('Activa'),
    }

    _defaults = {
      'active': lambda *a: 1,
    }
    _order = "name, id"
    _sql_constraint = [('name_unique', 'unique(name)', 'Ja existeix una càmera amb aquest nom.')]


    def create(self, cr, uid, vals, context={}):
        """Quan creem la càmera, creem també la carpeta."""
        cr.execute("SELECT name FROM giscegis_camera_config WHERE active = true ORDER BY id desc LIMIT 1")
        config = cr.dictfetchone()
        mp = None
        if config and config['name']:
            mp = config['name']
        if not mp:
            raise osv.except_osv('Error', 'Error creant la camera: no existeix cap configuració activa')
        id = super(osv.osv, self).create(cr, uid, vals, context)
        # ja tenim l'ID de la càmera, creem la carpeta
        import os
        try:
            os.mkdir("%s/%s" % (mp, id))
            os.chmod("%s/%s" % (mp, id), 0770)
        except OSError, ose:
            raise osv.except_osv('Error', 'Error creant la carpeta.')
        return id

    def gis_create(self, cr, uid, name, x, y):
        """Wrapper pel ''create'' per ser cridat des del GIS"""
        if not name or not x or not y:
            return json.dumps({'status': 'ERROR', 'msg': 'Cal donar un nom,, una x i una y'})
        vertex_obj = self.pool.get('giscegis.vertex')
        v = vertex_obj.search(cr, uid, [('x','=',x),('y','=',y)])
        if not len(v):
            v = [0]
            v[0] = vertex_obj.create(cr, uid, {'x': x, 'y': y})
        id = self.create(cr, uid, {'name': name, 'vertex': v[0]})
        if id>0:
            return json.dumps({'status': 'OK', 'msg': 'Càmera creada correctament amb l\'identificador %d' % (id,)})
        return json.dumps({'status': 'ERROR', 'msg': 'Error creant la càmera'})

    def get_cameres(self, cr, uid):
        """Retorna les càmeres en JSON amb id,name,x,y"""
        cr.execute("SELECT c.id,c.name,v.x,v.y FROM giscegis_camera c, giscegis_vertex v WHERE c.active = true AND c.vertex = v.id")
        res = []
        cameres = cr.dictfetchall()
        for c in cameres:
            res.append({'id': c['id'], 'name': c['name'], 'x': c['x'], 'y': c['y']})
        return json.dumps({'count': len(res), 'resultats': res})

giscegis_camera()

class giscegis_cameres(osv.osv):
    _name = "giscegis.cameres"
    _description = "Vista pel MapGuide i generador d'àlbums"
    _columns = {}
    _auto = False

    def init(self, cr):
        """Creem una vista pel MapGuide"""
        cr.execute("SELECT url FROM giscegis_camera_config WHERE active IS true LIMIT 1")
        r = cr.dictfetchone()
        if not r:
            r = {'url': ''}

        cr.execute("""DROP VIEW IF EXISTS giscegis_cameres""")
        cr.execute("""CREATE OR REPLACE VIEW giscegis_cameres AS (
          SELECT c.id, c.name, v.x, v.y, %s || c.id as url FROM giscegis_camera c
          LEFT JOIN giscegis_vertex v ON (v.id = c.vertex)
          WHERE c.active is true
        )
        """, (r['url'],))

    def crear_albums(self, cr, uid):
        """Mètode per ser cridat des del cronjob de l'ERP per generar els index.html dels àlbums"""
        import AlbumWebCronJob as awcj
        mp = self.pool.get('giscegis.camera.config').get_active_mountpoint(cr, uid)
        if not mp:
            return False
        ids = self.search(cr, uid, [])
        awcj.main(["%s/%s" % (mp, id) for id in ids])
        return True

giscegis_cameres()
