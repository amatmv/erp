#!/usr/bin/python

# GISCE Enginyeria S.L.
# AlbumWeb 1.0 -- versio "cron job"

from PIL import Image
import glob
import os
import sys

# defaults
COLS=3
EXTENSIONS = ["*.png", "*.jpg", "*.jpeg", "*.gif", "*.pdf", "*.PNG", "*.JPG", "*.JPEG", "*.GIF", "*.PDF"]
THUMBNAILS_SUBDIR = ".thumbnails"
THUMBNAIL_MAX_SIZE = (240, 240)
THUMBNAIL_FILTER = Image.ANTIALIAS
INDEX_FILENAME = "index.html"
THUMBNAIL_URL_BASE=".thumbnails/" # always add a TRAILING SLASH when non-empty!
THUMBNAIL_PDF_IMG="../pdfimg.png"
IMAGE_URL_BASE="" # always add a TRAILING SLASH when non-empty!

HTML_OPENING = """<html>
<head><title>Album</title>
<style>
body {
  font-family: sans-serif;
  font-size: 100%;
  background-color: #11303d;
  color: #ffffff;
  margin: 0;
  padding: 0;
  text-align: center;
}
table {
  border: 0px;
  margin-left: auto;
  margin-right: auto;
  border-spacing: 5px;
}
td {
  vertical-align: bottom;
}
.diapo {
  border-width: 1px;
  border-style: solid;
  padding: 4px;
  border-color: rgb(255, 255, 0);
  background-color: rgb(0, 0, 0);
  -moz-border-radius: 4px;
  font-family: sans-serif;
  font-size: 100%;
  text-align: center;
}
a {
  font-family: sans-serif;
  font-size: 100%;
  color: #ffffff;
  text-decoration: none;
}
a:hover {
  text-decoration: underline;
}
.album_caption {
  color: #ffffff;
  text-align: center;
  font-weight: bold;
}
.footer {
  /*background-color: #01202d;*/
  text-align: center;
  vertical-align: bottom;
  color: #61808d;
  margin-bottom: 0px;
  font-size: 80%;
}
img {
  border-style: none;
}
</style>
</head>
<body>
<!-- GISCE AlbumWeb 1.0 -->
"""
HTML_TABLE_HEADER = """<table>
<!--caption class="diapo">Album</caption-->
"""
HTML_TABLE_FOOTER = "</table>\n"
HTML_TABLE_ROW_START = "<tr>\n"
HTML_TABLE_ROW_END = "</tr>\n"
HTML_TABLE_PADDING_CELL = "<td>&nbsp;</td>\n"
HTML_FOOTER = """<p class=footer>Album Web 1.0 * GISCE Enginyeria S.L.&nbsp;</p>"""
HTML_CLOSING = "</body></html>"


# retrieve list of files
def retrieve_file_list(directory):
    files = []
    for extension in EXTENSIONS:
        files = files + glob.glob(os.path.join(directory, extension))
    files.sort()
    return files

# choose optimum layout for the given amount of images
def calc_optimum_columns(count):
    if count == 4: return 2
    if count <= 3: return count
    return COLS

def captionize(filename):
    """make a file name a bit more elegant"""
    return os.path.splitext(filename)[0].replace("_", " ")

def thumbnail(thumbnails_dir, image_file):
    """Creates a thumbnail in the given directory unless when there is one already and it is up to date.
    Arguments are:
      * thumbnail_dir -- the directory where the thumbnails are to be created;
      * image_file -- the full path of the source image for the thumbnail
    Return value:
      * True -- the thumbnail was already there, and it was up to date, or it has been successfully created"
      * False -- something happened that prevented to check the existing thumbnail or to create a current one."""

    try:
        if not os.path.exists(thumbnails_dir):
            os.mkdir(thumbnails_dir)

        thumbnail_file = os.path.join(thumbnails_dir, os.path.basename(image_file))

        if os.path.exists(thumbnail_file):
            otime = os.path.getmtime(image_file)
            ttime = os.path.getmtime(thumbnail_file)
            if(ttime >= otime):
                return True

        im = Image.open(image_file)
        im.thumbnail(THUMBNAIL_MAX_SIZE, THUMBNAIL_FILTER)
        im.save(thumbnail_file)
        return True
    except:
        return False

def html_image_cell(image_url, thumbnail_url, caption):
    """returns html code for a thumbnail with a caption, to be inserted inside a <TR>"""

    return """
      <td>
        <div class="diapo">
          <a href="%s"><img src="%s"></a>
          <p><a class="caption" href="%s">%s</a></p>
        </div>
      </td>\n""" % (image_url, thumbnail_url, image_url, caption)

def process_directory(album_dir):
    """Create thumbnails for the given directory"""
    images = retrieve_file_list(album_dir)
    columns = calc_optimum_columns(len(images))
    thumbnails_dir = os.path.join(album_dir, THUMBNAILS_SUBDIR)

    html = HTML_OPENING

    c=0
    r=0
    for image_file in images:

        if r==0 and c==0:
            html += HTML_TABLE_HEADER
        if c==0:
            html += HTML_TABLE_ROW_START

        basename = os.path.basename(image_file)
        caption = captionize(basename)
        thumbnails_dir = os.path.join(album_dir, THUMBNAILS_SUBDIR)
        image_url = IMAGE_URL_BASE + os.path.basename(image_file)
        thumbnail_url = THUMBNAIL_URL_BASE + basename
       
        (name, ext) = os.path.splitext(image_file)
        
        if ext != ".pdf" and ext != ".PDF":
          thumbnail(thumbnails_dir, image_file)
          html += html_image_cell(image_url, thumbnail_url, caption)
        else:
          html += html_image_cell(image_url, THUMBNAIL_PDF_IMG, caption)
    
        c = c + 1
    
        if (c==columns):
            html += HTML_TABLE_ROW_END
            r = r + 1
            c = 0
    
    if(c>0 and c<columns):
        while (c<columns):
            html += HTML_TABLE_PADDING_CELL
            c = c + 1
        html += HTML_TABLE_ROW_END
    
    html += HTML_TABLE_FOOTER
    html += HTML_FOOTER
    html += HTML_CLOSING
    
    index_file = os.path.join(album_dir, INDEX_FILENAME)
    f = open(index_file, "w")
    f.write(html)
    f.close()


def main(path):
  """Call the process_directory for every directory in path"""
  if type(path) == type([]):
    for p in path:
      main(p)
  else:
    d = os.path.abspath(path)
    if os.path.isdir(d):
      process_directory(d)
    else:
      sys.stderr.write("WARNING: Not a directory or does not exist: %s\n" % (d,))

