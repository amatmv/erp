# -*- coding: utf-8 -*-
{
    "name": "Index Interruptors BT",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Index als interruptors BT:
     - fusiblesbt
     - interruptorbt
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "base_index",
        "giscegis_blocs_fusiblesbt",
        "giscegis_blocs_interruptorbt",
        "giscegis_search_type"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscegis_search_type_data.xml"
    ],
    "active": False,
    "installable": True
}
