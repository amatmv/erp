from base_index.base_index import BaseIndex


class GiscegisBlocsFusiblesBt(BaseIndex):
    _name = 'giscegis.blocs.fusiblesbt'
    _inherit = 'giscegis.blocs.fusiblesbt'

    def __init__(self, pool, cursor):
        super(GiscegisBlocsFusiblesBt, self).__init__(pool, cursor)

    _index_title = '{obj.name}'
    _index_summary = '{obj.blockname.name} {obj.codi}'

    _index_fields = {
        'codi': True,
        'blockname.name': True,
    }

    def get_lat_lon(self, cursor, uid, item, context=None):
        if context is None:
            context = {}
        cursor.execute("""
        SELECT
            ST_Y(ST_GeometryN(ST_Transform(gv.geom,4326),1)
                 ) AS lat,
            ST_X(ST_GeometryN(ST_Transform(gv.geom,4326),1)
                 ) AS lon
        FROM giscegis_interruptorsbt_geom gv
        WHERE
            gv.id = %s
        """, ("2{0}".format(item.id), ))
        res = cursor.fetchone()
        if not res:
            res = (0, 0)
        return res

GiscegisBlocsFusiblesBt()


class GiscegisBlocsInterruptorBt(BaseIndex):
    _name = 'giscegis.blocs.interruptorbt'
    _inherit = 'giscegis.blocs.interruptorbt'

    def __init__(self, pool, cursor):
        super(GiscegisBlocsInterruptorBt, self).__init__(pool, cursor)

    _index_title = '{obj.name}'
    _index_summary = '{obj.blockname.name} {obj.codi}'

    _index_fields = {
        'codi': True,
        'blockname.name': True,
    }

    def get_lat_lon(self, cursor, uid, item, context=None):
        if context is None:
            context = {}
        cursor.execute("""
        SELECT
            ST_Y(ST_GeometryN(ST_Transform(gv.geom,4326),1)
                 ) AS lat,
            ST_X(ST_GeometryN(ST_Transform(gv.geom,4326),1)
                 ) AS lon
        FROM giscegis_interruptorsbt_geom gv
        WHERE
            gv.id = %s
        """, ("3{0}".format(item.id), ))
        res = cursor.fetchone()
        if not res:
            res = (0, 0)
        return res

GiscegisBlocsInterruptorBt()
