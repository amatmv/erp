# coding=utf-8

import logging
import pooler


def up(cursor, installed_version):

    logger = logging.getLogger('openerp.migration')

    logger.info(
        'Sync TensioBloc with BlocsTensio'
    )

    pool = pooler.get_pool(cursor.dbname)
    tb_obj = pool.get('giscegis.tensio.bloc')
    n_created_elements = tb_obj.sync_elements_from_blocs(cursor, 1)

    if n_created_elements == 1:
        msg = '1 TensioBloc sync correctly with BlocsTensio'
    else:
        msg = '{} TensioBlocs sync correctly with BlocsTensio'.format(
            n_created_elements
        )

    logger.info(msg)


def down(cursor, installed_version):
    pass


migrate = up
