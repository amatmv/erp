# -*- coding: utf-8 -*-
from osv import osv
from tools import config
from giscegis_base_geom.wizard.giscegis_shp_loader \
    import GIS_MODELS_TO_UPDATE_GEOM_DATA


# Subscribe this model to the list to be updated if there is an AutoCAD dump
GIS_MODELS_TO_UPDATE_GEOM_DATA.append(
    {
        'model': 'giscegis.tensio.bloc',
        'functions': ['sync_elements_from_blocs']
    },
)


class GiscegisTensioBloc(osv.osv):

    _name = 'giscegis.tensio.bloc'
    _inherit = 'giscegis.tensio.bloc'
    _description = "Tensió Bloc"

    def sync_elements_from_blocs(self, cursor, uid):
        """
        Syncronize the TensioBloc elements with the BlocsTensio elements.
        WARNING: This method will only work if the Autocadcode have not changed
        since the first sync.
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return: Return the number of elements syncronized.
        :rtype: int
        """

        tb_obj = self.pool.get('giscegis.tensio.bloc')
        tensio_norm_obj = self.pool.get('giscedata.tensions.tensio')

        sql = """
            SELECT b.rotation, b.node, b.tensio, b.name, b.codi,
                st_astext(st_setsrid(st_makepoint(v.x, v.y), %(srid)s)) AS geom
            FROM giscegis_blocs_tensio b
            LEFT JOIN giscegis_vertex v ON b.vertex = v.id
        """

        cursor.execute(sql, {"srid": config.get("srid", 25830)})

        rows = cursor.dictfetchall()
        row_n = len(rows)

        for row in rows:
            # Buscar tensio normalitzada
            tensio_norm_id = tensio_norm_obj.search(
                cursor, uid, [
                    "|", ('tensio', '=', row['tensio']),
                    ('name', '=', row['tensio'])
                ]
            )
            if tensio_norm_id:
                tensio_norm_id = tensio_norm_id[0]
            else:
                if int(row['tensio']) > 1000:
                    t_tipus = 'AT'
                else:
                    t_tipus = 'BT'

                tensio_norm_id = tensio_norm_obj.create(
                    cursor, uid, {
                        'l_inferior': row['tensio'],
                        'l_superior': row['tensio'],
                        'name': row['tensio'],
                        'tensio': row['tensio'],
                        'tipus': t_tipus
                    }
                )

            tb_vals = {
                'codi': row['codi'],
                'rotation': 360-row['rotation'],
                'node_id': row['node'],
                'geom': row['geom'],
                'tensio_id': tensio_norm_id
            }

            tb_ids = tb_obj.search(
                cursor, uid, [('codi', '=', row['codi'])]
            )

            if tb_ids:
                tb_obj.write(cursor, uid, tb_ids[0], tb_vals)
            else:
                tb_vals['name'] = tb_obj.default_get(
                    cursor, uid, ['name']
                )['name']
                tb_obj.create(cursor, uid, tb_vals)

        return row_n


GiscegisTensioBloc()
