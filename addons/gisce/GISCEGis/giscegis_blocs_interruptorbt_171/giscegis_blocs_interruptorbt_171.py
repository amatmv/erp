# *-* coding: utf-8 *-*

from osv import osv, fields

class giscegis_blocs_interruptorbt_171(osv.osv):

    _name = 'giscegis.blocs.interruptorbt.171'
    _auto = False
    _columns = {
      'x': fields.float('X', readonly=True),
      'y': fields.float('Y', readonly=True),
      'texte': fields.text('Texte', readonly=True),
      'blockname': fields.char('Blockname', size=50),
      'rotation': fields.float('Rotation'),
    }

    def init(self, cr):
        cr.execute("""drop view if exists giscegis_blocs_interruptorbt_171""")
        cr.execute("""
          create or replace view giscegis_blocs_interruptorbt_171 as (
            select vertex.x, vertex.y, blockname.name || ' ' || interruptor.codi as texte, blockname.name as blockname, interruptor.rotation
    from giscegis_blocs_interruptorbt interruptor
            left join giscegis_vertex vertex on (interruptor.vertex = vertex.id)
            left join giscegis_blocs_interruptorbt_blockname blockname on (interruptor.blockname = blockname.id)
        )""")

giscegis_blocs_interruptorbt_171()
