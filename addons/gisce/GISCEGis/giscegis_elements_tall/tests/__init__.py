# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
import ast
from .test_session import *
# from .test_topologia import *


class ElementsTallGeomTest(testing.OOTestCase):

    def test_catalog_elements_elements_tall(self):
        """
        Test that are elements for elements tall and how many of each type
        are searching with the catalog domain.
        :return: None
        """

        with Transaction().start(self.database) as txn:

            cursor = txn.cursor
            uid = txn.user

            # INICIALITZEM OBJECTES
            elem_tall_obj = self.openerp.pool.get('giscedata.celles.cella')
            cataleg_obj = self.openerp.pool.get('giscegis.cataleg.cataleg')
            sum_elem_tall = 0

            # SELECCIONEM UNICAMENT ELS ELEMENTS DE CATALEG QUE SIGUIN
            # ELEMENTS DE TALL
            ids_elems_tall_cataleg = cataleg_obj.search(
                cursor, uid, [('model.model', '=', 'giscedata.celles.cella')]
            )

            # PER A CADA ELEMENT DEL CATALEG COMPROVAREM QUE HI HAGIN ELEMENTS
            cataleg_elems_tall_elems = cataleg_obj.read(
                cursor, uid, ids_elems_tall_cataleg, ['name', 'domain']
            )

            for cataleg_elem in cataleg_elems_tall_elems:

                elems_tall_ids = elem_tall_obj.search(
                    cursor, uid, ast.literal_eval(cataleg_elem['domain'])
                )

                sum_elem_tall += len(elems_tall_ids)
                if cataleg_elem['name'] == "Fusible AT":
                    self.assertEqual(len(elems_tall_ids), 5)
                elif cataleg_elem['name'] == "Ruptofusible AT":
                    self.assertEqual(len(elems_tall_ids), 4)
                elif cataleg_elem['name'] == "Interruptor AT":
                    self.assertEqual(len(elems_tall_ids), 5)
                elif cataleg_elem['name'] == "Interruptor Automàtic AT":
                    self.assertEqual(len(elems_tall_ids), 4)
                elif cataleg_elem['name'] == "Seccionador Ganivetes":
                    self.assertEqual(len(elems_tall_ids), 8)
                elif cataleg_elem['name'] == "Seccionadors en Càrrega":
                    self.assertEqual(len(elems_tall_ids), 3)
                elif cataleg_elem['name'] == "Seccionador en Buit":
                    self.assertEqual(len(elems_tall_ids), 2)

            self.assertEqual(sum_elem_tall, 31)
