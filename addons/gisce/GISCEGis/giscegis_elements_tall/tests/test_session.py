from destral import testing
from destral.transaction import Transaction


__all__ = [
    'TestElementsTallSession'
]


class TestElementsTallSession(testing.OOTestCase):

    def setUp(self):
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user

    def tearDown(self):
        self.txn.stop()

    def test_search_session_elements_tall(self):
        """
        This test is for check the correct behaviour of the sessions and the
        elements tall
        """
        # Load ERP models
        elem_tall_obj = self.openerp.pool.get('giscedata.celles.cella')
        session_obj = self.openerp.pool.get('giscegis.session')

        # Clean the sessions to avoid BBox collisions
        sql = """
            TRUNCATE giscegis_session CASCADE
        """
        self.cursor.execute(sql)

        # Create the sessions
        session1_vals = {
            "name": 'Testing session 1',
            "bbox": 'POLYGON((982449.3 4664776.0, 982760.4 4664776.0, '
                    '982760.4 4664950.2, 982449.3 4664950.2, '
                    '982449.3 4664776.0))',
            "state": 'open',
            "ttl": 500000000,
            "active": True
        }
        session2_vals = {
            "name": 'Testing session 2',
            "bbox": 'POLYGON((981216.7 4664152.6, 981771.6 4664152.6, '
                    '981771.6 4664463.2, 981216.7 4664463.2, '
                    '981216.7 4664152.6))',
            "state": 'open',
            "ttl": 500000000,
            "active": True
        }
        session1_id = session_obj.create(self.cursor, self.uid, session1_vals)
        session2_id = session_obj.create(self.cursor, self.uid, session2_vals)

        # Start the checks
        # Check the number of Elements Tall in differents sessions and without
        # sessions
        # No session
        elem_tall_ids = elem_tall_obj.search(self.cursor, self.uid, [])
        self.assertEqual(len(elem_tall_ids), 31)
        # Session 1
        elem_tall_ids = elem_tall_obj.search(
            self.cursor, self.uid, [], context={'session': session1_id}
        )
        self.assertEqual(len(elem_tall_ids), 0)
        # Session 2
        elem_tall_ids = elem_tall_obj.search(
            self.cursor, self.uid, [], context={'session': session2_id}
        )
        self.assertEqual(len(elem_tall_ids), 17)

        # Get 2 ids from the session 2
        elem_tall1_id = elem_tall_ids[0]
        elem_tall2_id = elem_tall_ids[1]

        # Check that the changes made on a session are only visible from the
        # session
        # Load data before changes
        elem_tall_data_original = elem_tall_obj.read(
            self.cursor, self.uid, [elem_tall1_id, elem_tall2_id], ['rotation']
        )
        # Write new data on 2 Elements Tall from session 2
        elem_tall_write_vals = {
            "rotation": 240
        }
        elem_tall_obj.write(
            self.cursor, self.uid, [elem_tall1_id, elem_tall2_id],
            elem_tall_write_vals, context={'session': session2_id}
        )
        # Reload data outside the session
        elem_tall_data = elem_tall_obj.read(
            self.cursor, self.uid, [elem_tall1_id, elem_tall2_id], ['rotation']
        )
        # Compare data outside session before and after write data on session 2
        self.assertEqual(
            elem_tall_data_original[0]['rotation'],
            elem_tall_data[0]['rotation']
        )
        self.assertEqual(
            elem_tall_data_original[1]['rotation'],
            elem_tall_data[1]['rotation']
        )
        # Check changes inside the session
        elem_tall_data = elem_tall_obj.read(
            self.cursor, self.uid, [elem_tall1_id, elem_tall2_id], ['rotation'],
            context={'session': session2_id}
        )
        for data in elem_tall_data:
            self.assertEqual(data['rotation'], 240)
