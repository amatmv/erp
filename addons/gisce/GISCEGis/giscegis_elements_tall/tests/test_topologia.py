from destral import testing
from destral.transaction import Transaction


class TestCellesTopologia(testing.OOTestCase):

    def setUp(self):
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user
        self.model_name = "giscedata.celles.cella"

    def tearDown(self):
        self.txn.stop()

    def _get_model_id(self):
        ir_model_obj = self.openerp.pool.get("ir.model")
        model_id = ir_model_obj.search(
            self.cursor, self.uid,
            [("model", "=", self.model_name)]
        )

        return model_id[0]

    def test_write(self):
        """
        Tests write of the geom field to ensure the giscegis.node.bloc.model
        is updated

        :return: None
        """

        nbm_mod = self.openerp.pool.get("giscegis.node.bloc.model")
        node_mod = self.openerp.pool.get("giscegis.nodes")
        cella_mod = self.openerp.pool.get("giscedata.celles.cella")

        model_id = self._get_model_id()
        node1_id = node_mod.create(
            self.cursor, self.uid,
            {
                "name": 1,
                "geom": "POINT (111.0 222.0)"
            }
        )

        nbm_mod.create(
            self.cursor, self.uid,
            {
                "res_id": 1,
                "model": model_id,
                "node": node1_id
            }
        )

        node_mod.create(
            self.cursor, self.uid,
            {
                "name": 2,
                "geom": "POINT (222.0 111.0)"
            }
        )

        cella_mod.write(
            self.cursor, self.uid, [1], {"geom": "POINT (111.0 222.0)"}
        )

        nbm_search_params = [
            ("res_id", "=", 1),
            ("model", "=", model_id)
        ]
        nbm_id = nbm_mod.search(self.cursor, self.uid, nbm_search_params)
        nbm_data = nbm_mod.read(self.cursor, self.uid, nbm_id, ["node"])

        node_search_params = [
            ("geom", "=", "POINT (111.0 222.0)")
        ]
        node_id = node_mod.search(self.cursor, self.uid, node_search_params)
        self.assertEquals(node_id[0], nbm_data[0]["node"][0])

    def test_write_multiple(self):
        """
        Tests write of multiple geom field to ensure the
        giscegis.node.bloc.model is updated

        :return: None
        """

        nbm_mod = self.openerp.pool.get("giscegis.node.bloc.model")
        node_mod = self.openerp.pool.get("giscegis.nodes")
        cella_mod = self.openerp.pool.get("giscedata.celles.cella")

        model_id = self._get_model_id()

        node1_id = node_mod.create(
            self.cursor, self.uid,
            {
                "name": 1,
                "geom": "POINT (111.0 222.0)"
            }
        )

        node_mod.create(
            self.cursor, self.uid,
            {
                "name": 2,
                "geom": "POINT (222.0 111.0)"
            }
        )
        for res_id in [1,2,3]:
            nbm_mod.create(
                self.cursor, self.uid,
                {
                    "res_id": res_id,
                    "model": model_id,
                    "node": node1_id
                }
            )

        cella_mod.write(
            self.cursor, self.uid,
            [1, 2, 3], {"geom": "POINT (111.0 222.0)"}
        )

        nbm_search_params = [
            ("res_id", "in", [1, 2, 3]),
            ("model", "=", model_id)
        ]
        nbm_id = nbm_mod.search(self.cursor, self.uid, nbm_search_params)
        nbm_data = nbm_mod.read(self.cursor, self.uid, nbm_id, ["node"])

        node_search_params = [
            ("geom", "=", "POINT (111.0 222.0)")
        ]
        nodes_id = node_mod.search(self.cursor, self.uid, node_search_params)

        for ident in nodes_id:
            self.assertEquals(ident, nbm_data[0]["node"][0])

    def test_unlink(self):
        """
        Tests unlink of the geom field to ensure the giscegis.node.bloc.model
        is updated

        :return: None
        """
        nbm_mod = self.openerp.pool.get("giscegis.node.bloc.model")
        cella_mod = self.openerp.pool.get("giscedata.celles.cella")

        cella_mod.unlink(self.cursor, self.uid, [1])

        model_id = self._get_model_id()
        nbm_search_params = [
            ("res_id", "=", 1),
            ("model", "=", model_id)
        ]

        nbm_id = nbm_mod.search(self.cursor, self.uid, nbm_search_params)
        self.assertEquals(len(nbm_id), 0)

    def test_create(self):
        """
        Tests create of the geom field to ensure the giscegis.node.bloc.model
        is updated

        :return: None
        """

        nbm_mod = self.openerp.pool.get("giscegis.node.bloc.model")
        node_mod = self.openerp.pool.get("giscegis.nodes")
        cel_mod = self.openerp.pool.get("giscedata.celles.cella")

        node_mod.create(
            self.cursor, self.uid,
            {
                "name": 1,
                "geom": "POINT (111.0 222.0)"
            }
        )

        cel_data = {
            "name": "test",
            "geom": "POINT (111.0 222.0)",
            "tipus_posicio": 1,
            "tipus_element": 1,
            "installacio": "giscedata.cts,1",
            "inventari": "fiabilitat",
            "tensio": 1,
            "aillament": 1
        }
        cel_id = cel_mod.create(self.cursor, self.uid, cel_data)

        model_id = self._get_model_id()
        nbm_search_params = [
            ("res_id", "=", cel_id),
            ("model", "=", model_id)
        ]
        nbm_id = nbm_mod.search(self.cursor, self.uid, nbm_search_params)
        nbm_data = nbm_mod.read(self.cursor, self.uid, nbm_id, ["node"])

        node_search_params = [
            ("geom", "=", "POINT (111.0 222.0)")
        ]
        node_id = node_mod.search(self.cursor, self.uid, node_search_params)
        self.assertEquals(node_id[0], nbm_data[0]["node"][0])
