# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS ELEMENTS TALL",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_base",
        "giscegis_base_geom",
        "giscegis_cataleg",
        "giscedata_celles",
        "giscegis_nodes",
        "giscegis_node_bloc_model"
    ],
    "init_xml": [],
    "demo_xml": [
        "giscegis_elements_tall_demo.xml"
    ],
    "update_xml":[
        "giscegis_cataleg_elements_tall_data.xml"
    ],
    "active": False,
    "installable": True
}
