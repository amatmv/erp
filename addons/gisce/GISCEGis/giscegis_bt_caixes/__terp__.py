# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Caixa BT",
    "description": """GIS suport for Caixa Baixa Tensió""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "giscegis_base",
        "giscegis_base_geom",
        "giscegis_cataleg",
        "giscegis_nodes",
        "giscedata_bt_caixes"
    ],
    "init_xml": [],
    "demo_xml": [
        "giscegis_bt_caixes_demo.xml"
    ],
    "update_xml":[
        "giscegis_cataleg_bt_caixes_data.xml",
    ],
    "active": False,
    "installable": True
}
