from osv import fields
from giscegis_base_geom.fields import Point
from giscegis_base_geom.giscegis_session import SessionManaged
from tools import config


class GiscedataBtCaixaElement(SessionManaged):

    _name = 'giscedata.bt.caixa.element'
    _inherit = 'giscedata.bt.caixa.element'

    _columns = {
        'geom': Point("Geometria", srid=config.get('srid', 25830), select=True),
        'rotation': fields.integer("Rotation"),
        'node_id': fields.many2one("giscegis.nodes", "Node",
                                   ondelete='set null'),
        'autocad_code': fields.char("AutoCAD Code", size=64)
    }

    _defaults = {
        'rotation': lambda *a: 0,
    }


GiscedataBtCaixaElement()


class GiscedataBtCaixa(SessionManaged):

    _name = 'giscedata.bt.caixa'
    _inherit = 'giscedata.bt.caixa'

    _columns = {
        'geom': Point("Geometria", srid=config.get('srid', 25830), select=True),
        'rotation': fields.integer("Rotation"),
        'node_id': fields.many2one("giscegis.nodes", "Node",
                                   ondelete='set null'),
        'autocad_code': fields.char("Codi AutoCAD", size=64)
    }

    _defaults = {
        'rotation': lambda *a: 0,
    }


GiscedataBtCaixa()
