# *-* coding: utf-8 *-*

from osv import osv, fields


class GiscegisBlocsTrafosReductorsGeom(osv.osv):
    _name = 'giscegis.blocs.trafos.reductors.geom'
    _auto = False
    _columns = {
        'id': fields.integer('Id', readonly=True),
        'geom': fields.char('Geo', readonly=True, size=256),
        'blockname': fields.char('Blockname', readonly=True, size=256),
        'rotation': fields.integer('Rotation', readonly=True),
        'texte': fields.char('Texte', readonly=True, size=256),
        "materialize_date": fields.datetime("Fecha creación vista"),
    }

    def init(self, cursor):
        cursor.execute("""
        DROP MATERIALIZED VIEW IF EXISTS giscegis_blocs_trafos_reductors_geom;
        """)
        cursor.execute("""
        CREATE MATERIALIZED VIEW public.giscegis_blocs_trafos_reductors_geom AS
            SELECT trafo.id, ST_SETSRID(st_makepoint(vertex.x, vertex.y),
            (select value::int from res_config where name='giscegis_srid'))
                AS geom,
            'TRANSFORMADOR_R'::text as blockname,
            trafo.rotation -90 as rotation,
            ((('TRF-'::text || COALESCE(trf.name::character(1),
                trafo.name::bpchar)::text) || '
            '::text) || COALESCE(trf.potencia_nominal::text, 'S/P'::text)) ||
                ' kVA'::text AS texte,
                now() as materialize_date
        FROM giscegis_blocs_transformadors_reductors trafo
        LEFT JOIN giscegis_vertex vertex ON trafo.vertex = vertex.id
        LEFT JOIN giscedata_transformador_trafo trf
            ON trf.id = trafo.transformadors;
        """)
        cursor.execute("""
        CREATE INDEX giscegis_blocs_trafos_reductors_geom_geom
        ON giscegis_blocs_trafos_reductors_geom USING gist (geom);
        """)

    def recreate(self, cursor, uid):
        cursor.execute("""
        REFRESH MATERIALIZED VIEW giscegis_blocs_trafos_reductors_geom;
        """)
        return True

    def get_srid(self, cursor, uid):
        cursor.execute("""
        SELECT ST_SRID(geom) as srid
        FROM giscegis_blocs_trafos_reductors_geom_geom LIMIT 1;
        """)
        data = cursor.fetchall()
        if len(data) > 0:
            return data[0][0]
        else:
            return False

GiscegisBlocsTrafosReductorsGeom()
