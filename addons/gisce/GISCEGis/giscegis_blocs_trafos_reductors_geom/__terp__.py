# -*- coding: utf-8 -*-
{
    "name": "GISCE v3 GIS Blocs Transformadors Reductors ",
    "description": """vista per els transformadors reductors per la versio 3 de Giscegis""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_blocs_transformadors",
        "giscegis_blocs_transformadors_reductors",
        "giscegis_base",
        "giscegis_base_geom"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
