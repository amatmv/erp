# -*- coding: utf-8 -*-

from giscegis_base_index.giscegis_base_index import GiscegisBaseIndex


class GiscedataAtTram(GiscegisBaseIndex):
    """
    Index for giscedata.at.tram
    """
    _name = 'giscedata.at.tram'
    _inherit = 'giscedata.at.tram'

    _index_fields = {
        "obres": lambda self, data: ' '.join(['obra{0}'.format(d.name) or '' for d in data])
    }

GiscedataAtTram()
