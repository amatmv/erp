from giscegis_base_index.giscegis_base_index import GiscegisBaseIndex


class GiscedataBtElement(GiscegisBaseIndex):
    _name = 'giscedata.bt.element'
    _inherit = 'giscedata.bt.element'

    def __init__(self, pool, cursor):
        super(GiscedataBtElement, self).__init__(pool, cursor)

    _index_title = '{obj.name}'
    _index_summary = '{obj.name} - {obj.ct.name}-{obj.num_linia}'

    _index_fields = {
        'cini': True,
        'name': True,
        'ct.name': True,
        'ct.descripcio': True,
        'municipi.name': True,
        'num_linia': lambda self, data: data and '{0} L{0}'.format(data) or '',
        'cable.name': True,
        'cable.seccio': True,
        'cable.material.name': True,
        'voltatge': lambda self, data: data and '{0}V'.format(data) or '',
        'data_pm': True,
    }

    def get_lat_lon(self, cursor, uid, item, context=None):
        if context is None:
            context = {}
        cursor.execute("""
        SELECT
            ST_Y(ST_LineInterpolatePoint(
                 ST_GeometryN(st_transform(geom,4326),1), 0.5)) AS lat,
            ST_X(ST_LineInterpolatePoint(
                 ST_GeometryN(st_transform(geom,4326),1), 0.5)) AS lon
        FROM giscegis_lbt_geom
        WHERE
            id = %s
        """, (item.id, ))
        res = cursor.fetchone()
        if not res:
            res = (0, 0)
        return res


GiscedataBtElement()
