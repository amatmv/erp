# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* giscegis_caixabt_geom
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.14\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2015-09-07 08:48:14+0000\n"
"PO-Revision-Date: 2015-09-07 08:48:14+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: giscegis_caixabt_geom
#: constraint:ir.model:0
msgid "The Object name must start with x_ and not contain any special character !"
msgstr ""

#. module: giscegis_caixabt_geom
#: field:giscegis.caixabt.geom,symbol:0
msgid "Symbol"
msgstr ""

#. module: giscegis_caixabt_geom
#: model:ir.module.module,description:giscegis_caixabt_geom.module_meta_information
msgid "Vista de caixes BT per Giscegis v3"
msgstr ""

#. module: giscegis_caixabt_geom
#: field:giscegis.caixabt.geom,geom:0
msgid "Geom"
msgstr ""

#. module: giscegis_caixabt_geom
#: model:ir.model,name:giscegis_caixabt_geom.model_giscegis_caixabt_geom
msgid "giscegis.caixabt.geom"
msgstr ""

#. module: giscegis_caixabt_geom
#: field:giscegis.caixabt.geom,rotation:0
msgid "Rotation"
msgstr ""

#. module: giscegis_caixabt_geom
#: model:ir.module.module,shortdesc:giscegis_caixabt_geom.module_meta_information
msgid "GISCE GIS v3 Caixes BT"
msgstr ""

#. module: giscegis_caixabt_geom
#: field:giscegis.caixabt.geom,id:0
msgid "Id"
msgstr ""

