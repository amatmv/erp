# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS v3 Caixes BT",
    "description": """Vista de caixes BT per Giscegis v3""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_base",
        "giscegis_blocs_caixesbt",
        "giscegis_base_geom"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
