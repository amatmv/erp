# *-* coding: utf-8 *-*

from osv import osv, fields
from tools import config


class GiscegisCaixabtGeom(osv.osv):
    _name = 'giscegis.caixabt.geom'
    _auto = False
    _columns = {
        'id': fields.integer('Id', readonly=True),
        'rotation': fields.float('Rotation', readonly=True),
        'symbol': fields.char('Symbol', readonly=True, size=256),
        "intensitat_fusible": fields.float("Intensitat fusible"),
        "intensitat_caixa": fields.float("Intensitat caixa"),
        'geom': fields.char('Geom', readonly=True, size=256),
        "materialize_date": fields.datetime("Fecha creación vista"),
    }

    def init(self, cursor):
        srid = int(config.get("srid", 25830))
        cursor.execute("""
        DROP MATERIALIZED VIEW IF EXISTS giscegis_caixabt_geom;
        """)
        cursor.execute("""
            CREATE MATERIALIZED VIEW public.giscegis_caixabt_geom AS
                SELECT 
                    block.id AS id,
                    block.rotation AS rotation,
                    blockname.name AS symbol,
                    block.intensitat_fusible AS intensitat_fusible, 
                    block.intensitat_caixa AS intensitat_caixa, 
                    ST_SETSRID(
                        ST_MAKEPOINT(vertex.x, vertex.y), %(srid)s
                    ) AS geom,
                    now() AS materialize_date
                FROM giscegis_blocs_caixesbt AS block
                LEFT JOIN giscegis_vertex vertex ON block.vertex = vertex.id
                LEFT JOIN giscegis_blocs_caixesbt_blockname blockname ON 
                    block.blockname = blockname.id;
        """, {"srid": srid})
        cursor.execute("""
        CREATE INDEX giscegis_caixabt_geom_geom
            ON giscegis_caixabt_geom USING gist (geom);
        """)
        return True

    def _auto_init(self, cursor, context):
        res = super(GiscegisCaixabtGeom, self)._auto_init(cursor, context)
        return res

    def get_srid(self, cursor, uid):

        return [config.get('srid', 25830)]

    def recreate(self, cursor, uid):
        cursor.execute("""REFRESH MATERIALIZED VIEW giscegis_caixabt_geom;""")
        return True


GiscegisCaixabtGeom()
