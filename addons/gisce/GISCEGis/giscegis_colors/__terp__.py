{
    "name": "GISCE GIS Color Linies BT Transformadors",
    "description": """
    Module that adds the colors to the bt output lines
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_base",
        "giscegis_base_geom"
    ],
    "init_xml":[
        "giscegis_colors_bt_data.xml"
    ],
    "demo_xml": [],
    "update_xml":[
        "giscegis_colors_bt_view.xml",
        "security/ir.model.access.csv",
    ],
    "active": False,
    "installable": True
}