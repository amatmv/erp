from osv import osv,fields
from tools.translate import _


class GiscegisColorsBt(osv.osv):
    _LAYER_BT = "bt"
    _URL_HEX_CODE_COLOR_PICKER = "https://html-color-codes.info/"

    _name = "giscegis.colors.bt"
    _description = _("Colors de les linies de sortida dels transformadors")
    _columns = {
      "name": fields.integer(_("Sortida"), required=True),
      "color": fields.char(_("Color Sortida(HEX)"), required=True, size=256, help=_("Valor hexadecimal del color")),
      "link": fields.text("link", help=_("Omplir amb el valor hexadecimal del color que seleccionis"))
    }

    _defaults = {
        "link": lambda *a: "https://html-color-codes.info/",
    }


GiscegisColorsBt()
