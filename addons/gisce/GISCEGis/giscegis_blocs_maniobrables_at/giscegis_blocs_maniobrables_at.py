# -*- coding: utf-8 -*-
from osv import fields, osv


class giscegis_blocs_maniobrables_at(osv.osv):
    """Mòdul que crea una taula amb FK's cap a ir.model,
    només els models corresponent a elements maniobrables
    d'AT estaran relacionats.
    """
    _name = "giscegis.blocs.maniobrables.at"
    _description = "Elements Maniobrables AT"
    _columns = {
      'name': fields.many2one('ir.model','Model'),
    }

    _defaults = {

    }
    _order = 'id'


giscegis_blocs_maniobrables_at()
