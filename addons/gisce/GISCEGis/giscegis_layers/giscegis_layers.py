# *-* coding: utf-8 *-*

from osv import osv, fields


class GiscegisLayers(osv.osv):
    _name = 'giscegis.layers'
    _columns = {
        'name': fields.text('Name', readonly=True),
        'webgis_code': fields.text('Codi Webgis', readonly=True),
        'is_base': fields.boolean('Capa Base', readonly=True),
        'description': fields.text('Descripció', readonly=True),
        'active': fields.boolean('Capa Activa')
    }


GiscegisLayers()
