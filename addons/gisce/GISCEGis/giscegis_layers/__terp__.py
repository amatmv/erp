# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS v3",
    "description": """Mòdul de capes GISCE GIS v3""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
