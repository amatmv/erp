# *-* coding: utf-8 *-*

from osv import osv, fields

class giscegis_blocs_suports_bt_171(osv.osv):

    _name = 'giscegis.blocs.suports.bt.171'
    _auto = False
    _columns = {
      'x': fields.float('X', readonly=True),
      'y': fields.float('Y', readonly=True),
      'texte': fields.text('Texte', readonly=True),
      'blockname': fields.char('Blockname', size=50),
      'rotation': fields.float('Rotation'),
    }

    def init(self, cr):
        cr.execute("""drop view if exists giscegis_blocs_suports_bt_171""")
        cr.execute("""
          create or replace view giscegis_blocs_suports_bt_171 as (
            select vertex.x, vertex.y, blockname.name || ' ' || coalesce(suport.numsuport3, suport.numsuport2, suport.numsuport1, suport.numsuport, ' ') as texte, blockname.name as blockname, suport.rotation
    from giscegis_blocs_suports_bt suport
            left join giscegis_vertex vertex on (suport.vertex = vertex.id)
            left join giscegis_blocs_suports_bt_blockname blockname on (suport.blockname = blockname.id)
        )""")

giscegis_blocs_suports_bt_171()
