# -*- coding: iso-8859-1 -*-
from osv import osv, fields


class giscegis_blocs_adudsp_171(osv.osv):

    _name = 'giscegis.blocs.adudsp.171'
    _description = 'Blocs ADU DSP 171'

    def name_get(self, cursor, uid, ids, context=None):

        res = []
        for categ in self.browse(cursor, uid, ids, context=context):
            res.append((categ.id, '(%s) %s' % (categ.blockname, categ.texte)))
        return res

    _columns = {
        'id': fields.integer('id'),
        'rotation': fields.float('Rotation'),
        'x': fields.float('X'),
        'y': fields.float('Y'),
        'blockname': fields.char('blockname', size=50),
        'texte': fields.char('text', size=128),
    }

    _auto = False

    _defaults = {

    }

    _order = "blockname, id"

    def init(self, cr):
        cr.execute("""drop view if exists giscegis_blocs_adudsp_171""")
        cr.execute("""create or replace view giscegis_blocs_adudsp_171 as (
select c.id, v.x, v.y, c.rotation, b.name as blockname,
b.name || ' ' || c.codi || '
' || COALESCE(intensitat_fusible || ' A', '') as texte
from giscegis_blocs_adudsp c
left join giscegis_vertex v on c.vertex = v.id
left join giscedata_bt_caixes_blockname b on b.id=c.blockname
order by c.id
)
""")


giscegis_blocs_adudsp_171()
