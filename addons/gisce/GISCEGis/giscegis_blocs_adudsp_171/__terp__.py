# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Blocs ADU DSP BT per v 1.7.1",
    "description": """Modul de ADU DSP per a Autocad""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_blocs_adudsp",
        "giscegis_vertex",
        "giscedata_bt_caixes",
        "giscegis_base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "ir.model.access.csv",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
