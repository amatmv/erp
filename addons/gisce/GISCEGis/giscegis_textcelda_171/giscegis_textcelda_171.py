from osv import fields, osv

class giscegis_textcelda_171(osv.osv):
    _name = "giscegis.textcelda.171"
    _description = "Text Celda Vista per GIS"


    _columns = {
      'name': fields.char('Name', size=10, required=True),
      'text': fields.char('Text', size=255),
      'rotation': fields.float('Rotation'),
      'height': fields.float('Height'),
      'x': fields.float('X'),
      'y': fields.float('Y'),
    }

    _defaults = {

    }
    _order = 'name, id'

    _auto = False

    def init(self, cr):
        cr.execute("""drop view if exists giscegis_textcelda_171""")
        cr.execute("""
        create or replace view giscegis_textcelda_171 as (
          select tc.name, tc.text, tc.rotation, tc.height, v.x, v.y
          from giscegis_textcelda tc
          left join giscegis_vertex v on tc.vertex = v.id
        )
        """)

giscegis_textcelda_171()
