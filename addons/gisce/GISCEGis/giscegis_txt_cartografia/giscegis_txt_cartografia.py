# -*- coding: iso-8859-1 -*-
from osv import osv,fields

class giscegis_txt_cartografia(osv.osv):

    _name = 'giscegis.txt.cartografia'
    _description = 'Textes dels noms dels carrers'
    _columns = {
      'name': fields.char('Text',size=128,required=True),
      'height': fields.float('Height'),
      'rotation': fields.float('Rotation'),
      'x': fields.float('Vertex'),
      'y': fields.float('Vertex'),

    }

    _defaults = {

    }

    _order = "name"

giscegis_txt_cartografia()
