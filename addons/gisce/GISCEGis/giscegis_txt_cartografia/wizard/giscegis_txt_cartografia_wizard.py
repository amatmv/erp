# -*- coding: utf-8 -*-

import csv
import base64
import StringIO

from osv import osv, fields


class GiscegisImportarTxtCartografia(osv.osv_memory):
    """
    Classe per fer la importació de textos de cartografia.
    """
    _name = 'giscegis.importar.txt.cartografia'

    def action_exec_script(self, cursor, uid, ids, context=None):
        """
        Funció per importar els textos de la cartografia des d'un CSV amb
        el format: x,y,height,rotation,text

        La rotació ha d'estar en graus decimals
        El fitxer csv ha de tenir el format iso-8859-1
        """

        wizard = self.browse(cursor, uid, ids[0], context)
        txt = base64.decodestring(wizard.file)
        csv_file = StringIO.StringIO(txt)
        reader = csv.reader(csv_file, delimiter=',')
        result = []

        textos_afegits = 0

        carto = self.pool.get('giscegis.txt.cartografia')
        cursor.execute('truncate giscegis_txt_cartografia')
        for row in reader:
            vals = {}
            txt = unicode(row[4], 'utf-8').encode('utf-8')
            height = float(row[2])
            rotation = float(row[3])
            x = float(row[0])
            y = float(row[1])
            vals.update({'x': x,
                         'y': y,
                         'rotation': rotation,
                         'height': height,
                         'name': txt,
                         })
            carto.create(cursor, uid, vals)
            textos_afegits = textos_afegits + 1

        result.append("Importacio finalitzada")
        result.append(">> %i textos afegits" % (textos_afegits))
        wizard.write({'result': '\n'.join(result), 'state': 'done'})

    _columns = {
        'result': fields.text('Resultat'),
        'state': fields.char('State', size=4),
        'file': fields.binary('Fitxer'),
        'filename': fields.char('Nom', size=32),
    }

    _defaults = {
        'state': lambda *a: 'init',
    }

GiscegisImportarTxtCartografia()
