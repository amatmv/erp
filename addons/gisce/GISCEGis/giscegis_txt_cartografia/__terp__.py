# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Textos Cartografia 1.7.1",
    "description": """Taula per guardar els textos de la cartografia""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "ir.model.access.csv",
        "wizard/giscegis_txt_cartografia_wizard.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
