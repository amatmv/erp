# *-* coding: utf-8 *-*
from osv import osv, fields
from tools import config

class GiscegisBlocsTensioGeom(osv.osv):
    _name = 'giscegis.blocs.tensio.geom'
    _auto = False
    _description = 'Blocs Tensio per Giscegis v3'
    _columns = {
        'id': fields.integer('Id', readonly=True),
        'geom': fields.char('Geom', readonly=True, size=256),
        'texte': fields.char('Texte', readonly=True, size=256),
        'blockname': fields.char('Blockname', readonly=True, size=256),
        'rotation': fields.float('Rotation', readonly=True),
        "materialize_date": fields.datetime("Fecha creación vista"),
    }

    def init(self, cursor):

        cursor.execute("""
            DROP MATERIALIZED VIEW IF EXISTS giscegis_blocs_tensio_geom;
        """)

        cursor.execute("""
            CREATE MATERIALIZED VIEW public.giscegis_blocs_tensio_geom AS
                SELECT 
                    block.id AS id,
                    st_setsrid(
                        st_makepoint(vertex.x, vertex.y), %(srid)s
                    ) AS geom,
                    blockname.name::text || ' '::text || block.tensio AS texte,
                    blockname.name::text || block.tensio AS blockname, 
                    block.rotation AS rotation,
                    now() AS materialize_date
                FROM giscegis_blocs_tensio block
                LEFT JOIN giscegis_vertex vertex ON block.vertex = vertex.id
                LEFT JOIN giscegis_blocs_tensio_blockname blockname ON 
                    block.blockname = blockname.id;
        """, {'srid': config.get('srid', 25830)})

        cursor.execute("""
            DROP INDEX IF EXISTS giscegis_blocs_tensio_geom_geom;
            CREATE INDEX giscegis_blocs_tensio_geom_geom 
                ON giscegis_blocs_tensio_geom USING gist (geom);
        """)

    def recreate(self, cursor, uid):

        cursor.execute("""
            REFRESH MATERIALIZED VIEW giscegis_blocs_tensio_geom;
        """)
        return True

    def get_srid(self, cursor, uid):

        return config.get('srid', 25830)


GiscegisBlocsTensioGeom()
