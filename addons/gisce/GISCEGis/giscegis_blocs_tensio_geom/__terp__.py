# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS v3 Blocs Tensio ",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_vertex",
        "giscegis_nodes",
        "giscegis_base",
        "giscegis_blocs_tensio",
        "giscegis_base_geom",
        "giscegis_tensio_bloc",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
