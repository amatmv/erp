# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS v3 interruptors BT",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_base",
        "giscegis_blocs_suports_bt",
        "giscegis_blocs_elements_bt",
        "giscegis_blocs_fusiblesbt",
        "giscegis_blocs_interruptorbt",
        "giscegis_base_geom",
        "giscegis_bt_quadre"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
