# *-* coding: utf-8 *-*

from osv import fields, osv
from tools import config


class GiscegisInterruptorsbtGeom(osv.osv):
    _name = 'giscegis.interruptorsbt.geom'
    _auto = False
    _columns = {
        'id': fields.integer('Id', readonly=True),
        'geom': fields.char('Geom', readonly=True, size=256),
        'x': fields.float('X', readonly=True),
        'y': fields.float('Y', readonly=True),
        'texte': fields.char('Descripcio', readonly=True, size=256),
        'blockname': fields.char('Blockname', readonly=True, size=256),
        'rotation': fields.float('Rotation', readonly=True, size=256),
        'symbol': fields.char('Symbol', readonly=True, size=256),
        'intensitat': fields.float('Intensitat', readonly=True),
        'cini': fields.char('CINI', readonly=True, size=256),
        "materialize_date": fields.datetime("Fecha creación vista"),
        'sortida': fields.char('sortida', readonly=True, size=256),
    }

    def init(self, cursor):
        cursor.execute("""
            DROP MATERIALIZED VIEW IF EXISTS giscegis_interruptorsbt_geom;
        """)
        cursor.execute("""
            CREATE MATERIALIZED VIEW public.giscegis_interruptorsbt_geom AS
                SELECT 
                    ('1' || block.id::text)::int AS id,
                    ST_SETSRID(
                        ST_MAKEPOINT(vertex.x, vertex.y), %(srid)s
                    ) AS geom,
                    vertex.x AS x,
                    vertex.y AS y,
                    blockname.name AS texte, 
                    blockname.name AS blockname,
                    block.rotation,
                    REPLACE(blockname.name, ' ', '_') AS symbol,
                    NULL::double precision AS intensitat,
                    NULL::text AS cini,
                    now() AS materialize_date,
                    '' AS sortida
                FROM giscegis_elementsbt block
                LEFT JOIN giscegis_vertex vertex ON block.vertex = vertex.id
                LEFT JOIN giscegis_blocs_elementsbt_blockname blockname
                    ON block.blockname = blockname.id
                UNION
                SELECT 
                    ('2' || block.id::text)::int AS id,
                    ST_SETSRID(
                        ST_MAKEPOINT(vertex.x, vertex.y), %(srid)s
                    ) AS geom,
                    vertex.x AS x,
                    vertex.y AS y,
                    blockname.name || ' ' || block.codi || ' ' 
                    || block.intensitat || 'A' AS texte,
                    blockname.name AS blockname, 
                    block.rotation AS rotacio,
                    REPLACE(blockname.name, ' ', '_') AS symbol,
                    block.intensitat AS intensitat,
                    NULL::text AS cini,
                    now() AS materialize_date,
                    block.sortida_bt AS sortida
                FROM giscegis_blocs_fusiblesbt block
                LEFT JOIN giscegis_vertex vertex ON block.vertex = vertex.id
                LEFT JOIN giscegis_blocs_fusiblesbt_blockname blockname ON 
                    block.blockname = blockname.id
                UNION
                SELECT
                    ('3' || block.id::text)::int AS id,
                    ST_SETSRID(ST_MAKEPOINT(vertex.x, vertex.y), %(srid)s
                    ) AS geom,
                    vertex.x AS x,
                    vertex.y AS y,
                    blockname.name || ' ' || block.codi::text AS texte,
                    blockname.name AS blockname, 
                    block.rotation AS rotation,
                    REPLACE(blockname.name, ' ', '_') AS symbol,
                    NULL::double precision AS intensitat,
                    NULL::text AS cini,
                    now() AS materialize_date,
                    '' AS sortida
                FROM giscegis_blocs_interruptorbt block
                LEFT JOIN giscegis_vertex vertex ON block.vertex = vertex.id
                LEFT JOIN giscegis_blocs_interruptorbt_blockname blockname
                    ON block.blockname = blockname.id
        """, {'srid': config.get('srid', 25830)})

        return True

    def _auto_init(self, cursor, context):
        s = super(GiscegisInterruptorsbtGeom, self)
        return s._auto_init(cursor, context)

    def get_srid(self, cursor, uid):
        """
        :param cursor: cursor Database cursor
        :param uid: integer User identifier
        :return: Returns a list of srids in geom field
        """

        return config.get('srid', 25830)

    def recreate(self, cursor, uid):
        cursor.execute("""
            REFRESH MATERIALIZED VIEW giscegis_interruptorsbt_geom;
        """)

        return True


GiscegisInterruptorsbtGeom()
