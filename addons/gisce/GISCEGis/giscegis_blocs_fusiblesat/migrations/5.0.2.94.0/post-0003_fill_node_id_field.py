# coding=utf-8
import logging
import pooler


def migrate(cursor, installed_version):

    loger = logging.Logger("openerp.migration")
    loger.info("Filling fields node_id of the ElementsTall (Fusibles AT)")

    pool = pooler.get_pool(cursor.dbname)
    elem_tall_obj = pool.get('giscedata.celles.cella')
    elem_tall_obj.fill_node_id_fus_at(cursor, 1)

    loger.info("Migration successfull")


up = migrate
