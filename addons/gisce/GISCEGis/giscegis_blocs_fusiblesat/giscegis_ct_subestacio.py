from osv import osv
from tools import config


class GiscedataCtsSubestacioPosicio(osv.osv):
    _name = 'giscedata.cts.subestacions.posicio'
    _inherit = 'giscedata.cts.subestacions.posicio'

    def fill_geom_fus_at(self, cursor, uid):
        """
        Function that runs a query to update the field geom of all of those
        SE Pos that have the block as a fusibleat.
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """

        sql = """
            UPDATE giscedata_cts_subestacions_posicio AS p
            SET geom = bloc.geom
            FROM (
              SELECT codi,
                     st_snaptogrid(
                         st_translate(
                             st_setsrid(
                                 st_makepoint(vertex.x, vertex.y), 
                                 %(srid)s
                             ), 
                             0.0000000001, 0.0000000001
                         ), 
                         0.0001
                     ) AS geom
              FROM giscegis_blocs_fusiblesat int_at
              LEFT JOIN giscegis_vertex vertex ON int_at.vertex = vertex.id
            ) AS bloc
            WHERE p.name = bloc.codi
        """

        cursor.execute(sql, {"srid": config.get("srid", 25830)})

    def fill_rotation_fus_at(self, cursor, uid):
        """
        Function that runs a query to update the field rotation of all of those
        SE Pos that have the block as a fusibleat.
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """

        sql = """
            UPDATE giscedata_cts_subestacions_posicio AS p
            SET rotation = (360-bloc.rotation)
            FROM (
              SELECT codi,
                     rotation
              FROM giscegis_blocs_fusiblesat
            ) AS bloc
            WHERE p.name = bloc.codi
        """

        cursor.execute(sql)

    def fill_node_id_fus_at(self, cursor, uid):
        """
        Function that runs a query to update the field node_id of all of those
        SE Pos that have the block as a fusibleat.
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """

        sql = """
            UPDATE giscedata_cts_subestacions_posicio AS p
            SET node_id = bloc.node
            FROM (
              SELECT codi,
                     node
              FROM giscegis_blocs_fusiblesat
            ) AS bloc
            WHERE p.name = bloc.codi
        """

        cursor.execute(sql)


GiscedataCtsSubestacioPosicio()
