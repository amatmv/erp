# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Blocs CT AT",
    "description": """Modul de blocs CT per a Autocad""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscedata_cts",
        "giscegis_nodes",
        "giscegis_vertex",
        "giscegis_base",
        "giscegis_cts"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscegis_blocs_ctat_view.xml",
        "ir.model.access.csv",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
