from osv import fields, osv
from tools import config
from giscegis_base_geom.wizard.giscegis_shp_loader \
    import GIS_MODELS_TO_UPDATE_GEOM_DATA


# Subscribe this model to the list to be updated if there is an AutoCAD dump
GIS_MODELS_TO_UPDATE_GEOM_DATA.append(
    {
        'model': 'giscedata.cts',
        'functions': ['fill_geom', 'fill_rotation', 'fill_node_id']
    },
)


class GiscedataCts(osv.osv):

    _name = 'giscedata.cts'
    _inherit = 'giscedata.cts'

    def _coords(self, cursor, uid, ids, field_name, arg, context):
        """
        Returns the X and Y of the CTS
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :param ids: CTS IDs
        :type ids: int or list of int
        :param field_name: Not used
        :param arg: Not used
        :param context: Not used
        :return: X and Y of the CTs
        :rtype: dict[str, float or False]
        """

        if isinstance(ids, int):
            ids = [ids]

        ret = dict((x, {'x': False, 'y': False}) for x in ids)

        sql = """
            SELECT c.id AS id,
                   COALESCE (ST_X(c.geom), v.x) AS x,
                   COALESCE (ST_Y(c.geom), v.y) AS y
            FROM giscedata_cts c
            LEFT JOIN giscegis_blocs_ctat b ON b.ct = c.id
            LEFT JOIN giscegis_vertex v ON v.id = b.vertex
            WHERE c.id = ANY(%s)
        """

        cursor.execute(sql, (ids,))
        data = cursor.dictfetchall()

        for line in data:
            ct_id = line['id']

            if line['x'] is not None:
                x = line['x']
            else:
                x = False

            if line['y'] is not None:
                y = line['y']
            else:
                y = False

            ret[ct_id] = {"x": x, "y": y}

        return ret

    def fill_geom(self, cursor, uid):
        """
        Function that runs a query to update the field geom of all the CTS
        elements
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """
        sql = """
            UPDATE giscedata_cts AS cts 
            SET geom = bloc.geom 
            FROM (
              SELECT b.ct AS id,
                     st_setsrid(st_makepoint(v.x, v.y), %(srid)s) AS geom
              FROM giscegis_blocs_ctat b
              LEFT JOIN giscegis_vertex v ON b.vertex = v.id
            ) AS bloc
            WHERE cts.id = bloc.id;
            UPDATE giscedata_cts SET geom = St_SnapToGrid(
                ST_Translate(geom, 0.0000000001, 0.0000000001), 0.0001
            );
        """

        cursor.execute(sql, {"srid": config.get("srid", 25830)})

    def fill_rotation(self, cursor, uid):
        """
        Function that runs a query to update the field rotation of all the CTS
        elements
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """
        sql = """
            UPDATE giscedata_cts AS cts 
            SET rotation = (- bloc.rotation) 
            FROM (
              SELECT b.ct AS id,
                     b.rotation AS rotation
              FROM giscegis_blocs_ctat b
            ) AS bloc
            WHERE cts.id = bloc.id
        """

        cursor.execute(sql)

    def fill_node_id(self, cursor, uid):
        """
        Function that runs a query to update the field node_id of all the CTS
        elements
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """
        sql = """
            UPDATE giscedata_cts AS cts
            SET node_id = bloc.node
            FROM (
              SELECT b.ct AS id,
                     b.node AS node
              FROM giscegis_blocs_ctat b
            ) AS bloc
            WHERE cts.id = bloc.id
        """

        cursor.execute(sql)

    _columns = {
        'x': fields.function(
            _coords, method=True, type='float', string='x', multi="_coords"
        ),
        'y': fields.function(
            _coords, method=True, type='float', string='y', multi="_coords"
        )
    }


GiscedataCts()
