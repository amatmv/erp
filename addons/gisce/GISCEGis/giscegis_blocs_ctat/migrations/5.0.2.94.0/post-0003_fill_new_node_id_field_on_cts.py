# coding=utf-8

import logging
import pooler


def up(cursor, installed_version):

    logger = logging.getLogger('openerp.migration')

    logger.info(
        'Filling field node_id from giscedata_cts'
    )

    pool = pooler.get_pool(cursor.dbname)
    cts_obj = pool.get('giscedata.cts')
    cts_obj.fill_node_id(cursor, 1)

    logger.info(
        'Field node_id filled for model giscedata_cts'
    )


def down(cursor, installed_version):
    pass


migrate = up
