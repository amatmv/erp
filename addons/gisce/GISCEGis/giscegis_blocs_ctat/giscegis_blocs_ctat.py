# -*- coding: utf-8 -*-
from osv import osv, fields
from giscegis_base_geom.giscegis_base_geom import DROP_ON_AUTOCAD_DUMP

# Subscribe this model to the list to be updated if there is an AutoCAD dump
DROP_ON_AUTOCAD_DUMP.append('giscegis_blocs_ctat')


class giscegis_blocs_ctat_blockname(osv.osv):

    _name = 'giscegis.blocs.ctat.blockname'
    _description = 'Tipus Blocs CTAT'

    def create(self, cr, uid, vals, context={}):
        ids = self.search(cr, uid, [('name', '=', vals['name'])])
        if ids and len(ids):
            return ids[0]
        else:
            return super(osv.osv, self).create(cr, uid, vals)

    _columns = {
      'name': fields.char('BlockName',size=20,required=True),
      'description': fields.char('DescripciÃ³',size=255),
    }

    _defaults = {

    }

    _order = "name, id"

giscegis_blocs_ctat_blockname()


class GiscegisBlocsCtat(osv.osv):

    _name = 'giscegis.blocs.ctat'
    _description = 'Blocs CT AT'

    def name_get(self, cursor, uid, ids, context=None):
        """
        Function that overwrites original name_get

        :param cursor: Database cursor
        :param uid: User id
        :param ids: List of consulted ids
        :param context: OpenERP context
        :return: Dict {id:name}
        """

        if not isinstance(ids, list):
            ids = [ids]
        if not len(ids):
            return []
        if not context:
            context = {}

        res = []
        for o in self.browse(cursor, uid, ids, context=context):
            name = '%s' % (o.codi or o.name)
            res.append((o.id, name))
        return res

    _columns = {
      'name': fields.char('ObjectId', size=10, required=True),
      'blockname': fields.many2one('giscegis.blocs.ctat.blockname', 
                                   'BlockName'),
      'codi': fields.char('Codi', size=10),
      'vertex': fields.many2one('giscegis.vertex', 'Vertex',
                                ondelete='set null'),
      'width': fields.float('Width'),
      'height': fields.float('Height'),
      'rotation': fields.float('Rotation'),
      'ct': fields.many2one('giscedata.cts', 'Centre Transformador'),
      'node': fields.many2one('giscegis.nodes', 'Node', ondelete='set null'),
    }

    def create(self, cr, uid, vals, context={}):
        
        _fields = [('vertex', 'giscegis.vertex'),
                   ('blockname', 'giscegis.blocs.ctat.blockname')]
        for field in _fields:
            if field[0] in vals and isinstance(vals[field[0]], list):
                obj = self.pool.get(field[1])
                remote_id = obj.create(cr, uid, vals[field[0]][0][2])
                if remote_id:
                    vals[field[0]] = remote_id

        if vals.has_key('codi') and not vals.has_key('ct'):
            ct_ids = self.pool.get('giscedata.cts').search(cr, uid, 
                                        [('name', '=', vals['codi'])], context)
            if ct_ids and len(ct_ids):
                vals['ct'] = ct_ids[0]
        return super(osv.osv, self).create(cr, uid, vals, context)

    def write(self, cursor, uid, ids, vals, context=None):
        """MÃ¨tode sobreescrit per escriure als camps many2one des d'aquÃ­.

        Igual que amb el create() cal assabentar-se de quÃ¨ els write()s dels
        models referenciats estiguin sobreescrits 
        """
        _fields = [('vertex', 'giscegis.vertex'),
                   ('blockname', 'giscegis.blocs.ctat.blockname')]
        for field in _fields:
            if field[0] in vals and isinstance(vals[field[0]], list):
                obj = self.pool.get(field[1])
                search_params = []
                for k, v in vals[field[0]][0][2].items():
                    search_params.append((k, '=', v))
                remote_id = obj.search(cursor, uid, search_params)
                if remote_id:
                    vals[field[0]] = remote_id[0]
                else:
                    remote_id = obj.create(cursor, uid, vals[field[0]][0][2])
                    vals[field[0]] = remote_id

        """Ens assegurem que la vinculaciÃ³ amb el ct corresponent Ã©s
           correcte"""
        if not 'ct' in vals:
            ct_obj = self.pool.get('giscedata.cts')
            ct_codis = self.read(cursor, uid, ids, ['ct', 'codi'])
            if not isinstance(ids, list):
                ct_codis = [ct_codis]
            for c in ct_codis:
                if not c['ct']:
                    gdcd_id = ct_obj.search(cursor, uid,
                                            [('name', '=', c['codi'])])
                    if gdcd_id:
                        vals['ct'] = gdcd_id[0]

        return super(osv.osv, self).write(cursor, uid, ids, vals)

    _defaults = {

    }

    _order = "name, id"
GiscegisBlocsCtat()
