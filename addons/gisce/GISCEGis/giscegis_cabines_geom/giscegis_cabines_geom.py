# *-* coding: utf-8 *-*
from __future__ import absolute_import, unicode_literals
from osv import osv, fields


class GiscegisCabinesGeom(osv.osv):
    """
    Class for the model giscegis_cabines_geom
    """

    _name = 'giscegis.cabines.geom'
    _columns = {
        'gid': fields.integer('Gid'),
        'id': fields.integer('Id', required=True),
        'color': fields.integer('Color'),
        'style': fields.text('Style'),
        'textstring': fields.text('Text'),
        'text_size': fields.float('Text Size'),
        'text_angle': fields.float('Text Angle'),
    }

    def init(self, cursor):
        """
        Class contructor

        :param cursor: Database cursor
        :return: True
        """
        conf_obj = self.pool.get('res.config')
        srid = int(conf_obj.get(cursor, 1, 'giscegis_srid', '25831'))
        cursor.execute('''
            SELECT * FROM information_schema.columns
            WHERE table_name = 'giscegis_cabines_geom'
            AND COLUMN_NAME = 'geom'

            ''')
        if not cursor.fetchone():
            sql_query = """SELECT AddGeometryColumn(
            'public','giscegis_cabines_geom',
            'geom',%(srid)s,'geometry',2, true);"""
            cursor.execute(sql_query, {'srid': srid})
        return True

GiscegisCabinesGeom()
