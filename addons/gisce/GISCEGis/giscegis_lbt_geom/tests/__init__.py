# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction


class LBTFillGeomTest(testing.OOTestCase):
    """"""

    def test_fill_geom(self):
        """
        Tests longitud_cad of giscedata.bt.element

        :return: None
        """

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            bt_obj = self.openerp.pool.get("giscedata.bt.element")
            obj_tipus = self.openerp.pool.get('giscedata.bt.tipuslinia')

            id_aerea = obj_tipus.search(cursor, uid, [('name', '=like', 'A%')])[0]
            bt_id = bt_obj.create(cursor, uid, {"name": "42", "tipus_linia": id_aerea})

            sql = """
            INSERT INTO giscegis_lbt (id,geom)
            VALUES (%(id)s,ST_GeomFromText('{}',25830));
            """

            line1_geom = 'MULTILINESTRING((2.62487 41.96714,2.81217 41.98347))'

            cursor.execute(sql.format(line1_geom), {"id": 42})
            bt_obj.fill_geom(cursor, uid)
            data = bt_obj.read(cursor, uid, bt_id, ["longitud_cad"])
            self.assertEqual(data["longitud_cad"], 0.19)
