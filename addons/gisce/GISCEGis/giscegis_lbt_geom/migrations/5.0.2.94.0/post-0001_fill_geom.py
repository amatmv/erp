import logging
import pooler


def migrate(cr, installed_version):
    """
    Fills the geom field of giscedata_bt_element

    :param cr: Database cursor
    :param installed_version: Installed version of the module
    :return: None
    """

    logger = logging.getLogger("openerp.migration")

    logger.info('Filling field geom from giscedata_bt_element.')

    pool = pooler.get_pool(cr.dbname)
    elem_bt_obj = pool.get('giscedata.bt.element')
    elem_bt_obj.fill_geom(cr, 1)

    logger.info('Field geom filled for model giscedata_bt_element.')


def down(cr, installed_version):
    pass


up = migrate
