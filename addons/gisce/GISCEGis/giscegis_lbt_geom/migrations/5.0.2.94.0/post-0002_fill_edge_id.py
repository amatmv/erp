import logging
import pooler


def migrate(cr, installed_version):

    logger = logging.getLogger("openerp.migration")

    logger.info('Filling field edge_id of giscedata_bt_element.')

    pool = pooler.get_pool(cr.dbname)
    elem_bt_obj = pool.get('giscedata.bt.element')
    elem_bt_obj.fill_edge_id(cr, 1)

    logger.info('Field edge_id filled for model giscedata_bt_element.')


def down(cr, installed_version):
    pass


up = migrate
