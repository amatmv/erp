# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Elements BT per a Giscegis 3",
    "description": """Vista dels elements BT per la versió Giscegis 3""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscedata_bt",
        "giscegis_base",
        "giscegis_base_geom",
        "giscegis_lbt_traceability",
        "giscegis_bt",
        "giscegis_colors"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
