# *-* coding: utf-8 *-*
from osv import osv, fields
from tools import config


class GiscegisLbtGeom(osv.osv):

    _name = 'giscegis.lbt.geom'
    _auto = False
    _columns = {
        'id': fields.integer('Id', readonly=True),
        'name': fields.integer('Id', readonly=True),
        'theme': fields.text('Texte', readonly=True),
        'texte': fields.text('Texte', readonly=True),
        'geom': fields.text('Texte', readonly=True),
        'length': fields.integer('Id', readonly=True),
        "materialize_date": fields.datetime("Fecha creación vista"),
    }

    def init(self, cursor):
        cursor.execute("""
            SELECT relname
            FROM pg_class
            WHERE relkind = 'S' AND relname = 'giscegis_lbt_id_seq'
        """)
        res = cursor.fetchall()
        conf_obj = self.pool.get('res.config')
        srid_id = conf_obj.search(cursor, 1, [('name', '=', 'giscegis_srid')])
        if srid_id:
            srid = int(conf_obj.read(cursor, 1, srid_id[0], ['value'])['value'])
        else:
            srid = 23030

        if not res:
            cursor.execute("""
                CREATE SEQUENCE public.giscegis_lbt_id_seq
                    INCREMENT 1
                    MINVALUE 1
                    MAXVALUE 9223372036854775807
                    START 1
                    CACHE 1;
            """)

        sql_exists = """
            SELECT EXISTS (
                SELECT 1
                FROM   information_schema.tables
                WHERE  table_schema = 'public'
                AND    table_name = 'giscegis_lbt');
        """
        cursor.execute(sql_exists)
        res_exists = cursor.fetchall()[0][0]
        # Creacio de la taula giscegis_lbt
        if not res_exists:
            sql_giscegis_lbt = """
                CREATE TABLE IF NOT EXISTS  public.giscegis_lbt (
                    gid integer NOT NULL DEFAULT
                    nextval('giscegis_lbt_id_seq'::regclass),
                    id double precision,
                    CONSTRAINT giscegis_lbt_pkey PRIMARY KEY (gid));
                SELECT AddGeometryColumn('public','giscegis_lbt',
                    'geom',%(srid)s,'MULTILINESTRING',2, true);
            """
            cursor.execute(sql_giscegis_lbt, {'srid': srid})

        # Creacio de la taula giscegis_lbt_geom
        cursor.execute("""
            DROP MATERIALIZED VIEW IF EXISTS giscegis_lbt_geom;
            
            CREATE MATERIALIZED VIEW public.giscegis_lbt_geom AS
                SELECT
                    bt.id AS id,
                    bt.name AS name,
                    CASE 
                        WHEN colors_bt.color IS NOT NULL then colors_bt.color
                        ELSE ''::character varying
                    END AS color,
                    CASE
                        WHEN tipus_cable.codi = 'E' THEN 
                            'EBT'
                        WHEN substring(tipus_linia.name, 1, 1) = 'A' THEN 
                            'L' || fus.sortida_bt::text || '-A'
                        WHEN substring(tipus_linia.name, 1, 1) = 'S' THEN 
                            'L' || fus.sortida_bt::text || '-S'
                        ELSE NULL::text
                    END AS theme,
                    ct.name || '-' || fus.sortida_bt::text AS texte,
                    shape.geom AS geom,
                    st_length(shape.geom) AS length,
                    bt.voltatge AS voltatge,
                    cable.seccio AS seccio,
                    fus.sortida_bt AS sortida,
                    now() AS materialize_date
                FROM giscegis_lbt AS shape
                LEFT JOIN giscedata_bt_element bt ON 
                    shape.id::integer = bt.name::integer 
                LEFT JOIN giscedata_bt_tipuslinia tipus_linia
                    ON tipus_linia.id = bt.tipus_linia
                LEFT JOIN giscegis_lbt_traceability trace ON 
                    bt.id = trace.element
                LEFT JOIN giscedata_bt_cables cable ON 
                    bt.cable = cable.id
                LEFT JOIN giscedata_bt_tipuscable tipus_cable ON 
                    cable.tipus = tipus_cable.id
                LEFT JOIN giscegis_blocs_fusiblesbt fus ON 
                    trace.fusible = fus.id
                LEFT JOIN giscedata_cts ct ON ct.id = trace.ct
                LEFT JOIN giscegis_colors_bt colors_bt ON 
                    colors_bt."name"::text = fus.sortida_bt
                GROUP BY bt.id, tipus_cable.codi, tipus_linia.name, 
                    fus.sortida_bt, ct.name, cable.seccio, fus.sortida_bt, 
                    colors_bt.color, shape.geom;
        """)

        # Creacio d'indexs pe a les geometries
        cursor.execute("""
            DROP INDEX IF EXISTS giscegis_lbt_geom_geom;
            CREATE INDEX giscegis_lbt_geom_geom
                ON giscegis_lbt_geom USING gist (geom);
        """)
        cursor.execute("""
            DROP INDEX IF EXISTS giscegis_lbt_geom_idx;
            CREATE INDEX giscegis_lbt_geom_idx ON 
            public.giscegis_lbt USING gist (geom);
        """)
        return True

    def _auto_init(self, cursor, context):
        res = super(GiscegisLbtGeom, self)._auto_init(cursor, context)
        return res

    def get_srid(self, cursor, uid):
        """
        :param cursor: cursor Database cursor
        :param uid: integer User identifier
        :return: Returns a list of srids in geom field
        """

        return [config.get('srid', 25830)]

    def recreate(self, cursor, uid):
        cursor.execute("""
            REFRESH MATERIALIZED VIEW giscegis_lbt_geom;
        """)
        return True


GiscegisLbtGeom()


class GiscegisColorsBt(osv.osv):
    _name = "giscegis.colors.bt"
    _inherit = "giscegis.colors.bt"

    def create(self, cr, uid, vals, context=None):
        """
        Overwrite create method, now adds extra feature like:
         refresh vm giscegis_lbt_geom
        :param cr: db cursor
        :type cursor
        :param uid: user id
        :type uid: int
        :param vals: {'color':'hex_value'}
        :type vals: dict
        :param context:
        :return: result of create method
        :rtype bool
        """
        sortida = self.check_hashtag(vals)
        ret = super(GiscegisColorsBt, self).create(cr, uid, sortida, context)
        #Refresh VM giscegis_lbt_geom
        self.pool.get("giscegis.lbt.geom").recreate(cr, uid)
        return ret

    def unlink(self, cr, uid, ids, context=None):
        """
        Overwrite unlink method, now adds extra feature like:
         refresh vm giscegis_lbt_geom
        :param cr: db cursor
        :type cursor
        :param uid: user id
        :type int
        :param ids:
        :param context:
        :return: result of unlink method
        :rtype bool
        """
        ret = super(GiscegisColorsBt, self).unlink(cr, uid, ids)
        # Refresh VM giscegis_lbt_geom
        self.pool.get("giscegis.lbt.geom").recreate(cr, uid)
        return ret

    def write(self, cr, uid, ids, values, context=None):
        """
        Overwrite writemethod, now adds extra feature like:
         refresh vm giscegis_lbt_geom
        :param cr: db cursor
        :type cr: cursor
        :param uid: user id
        :type int
        :param ids: sortida value
        :type list
        :param values: {'color':'hex_value'}
        :type dict
        :param context:
        :return: result of write method
        :rtype bool
        """
        sortida = self.check_hashtag(values)
        ret = super(GiscegisColorsBt, self).write(cr, uid, ids, sortida)
        # Refresh VM giscegis_lbt_geom
        self.pool.get("giscegis.lbt.geom").recreate(cr, uid)
        return ret

    def check_hashtag(self, sortida):
        """
        Check if hexadecimal color value has #
        :param sortida: dictionary with color and hex value
        :return dict
        :return: sortida
        :rtype dict
        """
        if sortida.get('color'):
            color = sortida.get('color')
            if len(color) > 0:
                if color[0] is not '#':
                    sortida['color'] = '#'+color
        return sortida


GiscegisColorsBt()
