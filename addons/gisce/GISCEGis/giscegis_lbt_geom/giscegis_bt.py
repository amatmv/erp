from osv import osv
from giscegis_base_geom.wizard.giscegis_shp_loader \
    import GIS_MODELS_TO_UPDATE_GEOM_DATA


# Subscribe this model to the list to be updated if there is an AutoCAD dump
GIS_MODELS_TO_UPDATE_GEOM_DATA.append(
    {
        'model': 'giscedata.bt.element',
        'functions': ['fill_geom', 'fill_edge_id']
    },
)


class GiscedataBtElement(osv.osv):

    _name = 'giscedata.bt.element'
    _inherit = 'giscedata.bt.element'

    def fill_geom(self, cr, uid):
        """
        Function that runs a query to update the field geom of all the
        TramsBT
        :param cr: Database cursor
        :type cr: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """

        sql = """
            SELECT 
                bt.id, 
                ST_ASTEXT(ST_GeometryN(lbt.geom, 1))
            FROM giscegis_lbt lbt
            INNER JOIN giscedata_bt_element bt ON  bt.name=lbt.id;
        """
        cr.execute(sql)
        rows = cr.fetchall()
        for row in rows:
            self.write(cr, uid, [int(row[0])], {"geom": row[1]})

    def fill_edge_id(self, cr, uid):
        """
        Function that runs a query to update the field edge_id of all the
        TramsBT
        :param cr: Database cursor
        :type cr: Cursor
        :param uid: User ID
        :type uid: int
        :return None
        :rtype NoneType
        """

        sql_update = """
            UPDATE giscedata_bt_element AS bt
            SET edge_id=data.edge
            FROM (
                SELECT bt.id AS id,
                       e.id AS edge
                    FROM giscedata_bt_element bt
                    LEFT JOIN giscegis_edge e ON bt.name=e.id_linktemplate
                    WHERE e.layer ILIKE (
                        SELECT COALESCE(
                            (
                                SELECT value
                                FROM res_config
                                WHERE name ILIKE '%giscegis_btlike_layer%'
                            ), '%BT\_%'
                        )
                    )
                    AND bt.geom IS NOT NULL
            ) AS data
            WHERE bt.id=data.id
        """

        cr.execute(sql_update)


GiscedataBtElement()
