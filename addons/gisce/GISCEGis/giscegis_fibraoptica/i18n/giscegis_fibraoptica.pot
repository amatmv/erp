# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* giscegis_fibraoptica
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.14\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2012-03-28 16:08:13+0000\n"
"PO-Revision-Date: 2012-03-28 16:08:13+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: giscegis_fibraoptica
#: field:giscegis.fibraoptica,categoria:0
msgid "Categoria"
msgstr ""

#. module: giscegis_fibraoptica
#: field:giscegis.fibraoptica,name:0
#: field:giscegis.fibraoptica.categoria,name:0
msgid "Nom"
msgstr ""

#. module: giscegis_fibraoptica
#: model:ir.model,name:giscegis_fibraoptica.model_giscegis_fibraoptica_categoria
msgid "Categoria FibraOptica per GIS"
msgstr ""

#. module: giscegis_fibraoptica
#: field:giscegis.fibraoptica,handle:0
msgid "Handle"
msgstr ""

#. module: giscegis_fibraoptica
#: field:giscegis.fibraoptica,color:0
msgid "Color"
msgstr ""

#. module: giscegis_fibraoptica
#: model:ir.module.module,shortdesc:giscegis_fibraoptica.module_meta_information
msgid "GISCE GIS Fibra Optica"
msgstr ""

#. module: giscegis_fibraoptica
#: model:ir.model,name:giscegis_fibraoptica.model_giscegis_fibraoptica
msgid "FibraOptica per GIS"
msgstr ""

#. module: giscegis_fibraoptica
#: constraint:ir.model:0
msgid "The Object name must start with x_ and not contain any special character !"
msgstr ""

#. module: giscegis_fibraoptica
#: field:giscegis.fibraoptica,length:0
msgid "Longitud"
msgstr ""

