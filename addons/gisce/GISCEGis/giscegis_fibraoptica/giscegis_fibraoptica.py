from osv import fields, osv

class giscegis_fibraoptica_categoria(osv.osv):
    _name = "giscegis.fibraoptica.categoria"
    _description = "Categoria FibraOptica per GIS"
    _columns = {
      'name': fields.char('Nom', size=10),
    }

    _defaults = {

    }
    _order = 'name, id'

giscegis_fibraoptica_categoria()

class giscegis_fibraoptica(osv.osv):
    _name = "giscegis.fibraoptica"
    _description = "FibraOptica per GIS"
    _columns = {
      'name': fields.char('Nom', size=10),
      'categoria': fields.many2one('giscegis.fibraoptica.categoria', 'Categoria'),
      'handle': fields.char('Handle', size=10),
      'length': fields.float('Longitud'),
      'color': fields.many2one('giscegis.color.acadmapguide','Color'),
    }

    _defaults = {

    }

    _order = "name, id"
giscegis_fibraoptica()
