# -*- coding: utf-8 -*-
from osv import fields
from giscegis_base_geom.giscegis_session import SessionManaged
from giscegis_base_geom.fields import Point
from tools import config


class GiscegisPuntFrontera(SessionManaged):

    _inherit = 'giscedata.punt.frontera'

    _columns = {
        'geom': Point("geometry", srid=config.get('srid', 25830), select=True),
        'rotation': fields.integer("Rotation"),
        'node_id': fields.many2one("giscegis.nodes", "Node",
                                   ondelete='set null')
    }

    _defaults = {
        'rotation': lambda *a: 0
    }


GiscegisPuntFrontera()
