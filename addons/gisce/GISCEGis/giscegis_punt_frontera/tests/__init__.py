# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
import ast
from .test_session import *


class BaseTest(testing.OOTestCase):

    def test_catalog_elements_punts_frontera(self):
        """
        Test that are elements for every PuntFrontera element of the catalog
        :return: None
        """

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            # INICIALITZEM OBJECTES
            pf_obj = self.openerp.pool.get('giscedata.punt.frontera')
            cataleg_obj = self.openerp.pool.get('giscegis.cataleg.cataleg')
            sum_pf = 0
            # SELECCIONEM UNICAMENT ELS ELEMENTS DE CATALEG QUE SIGUIN PF
            ids_pf_cataleg = cataleg_obj.search(
                cursor, uid, [('model.model', '=', 'giscedata.punt.frontera')]
            )
            # PER A CADA ELEMENT DEL CATALEG COMPROVAREM QUE HI HAGIN ELEMENTS
            for id_pf_cataleg in ids_pf_cataleg:
                cataleg_pf = cataleg_obj.read(
                    cursor, uid, id_pf_cataleg,
                    ['name', 'domain']
                )
                pf_ids = pf_obj.search(
                    cursor, uid, ast.literal_eval(cataleg_pf['domain'])
                )
                sum_pf += len(pf_ids)
                self.assertEqual(len(pf_ids), 1)

            # COMPROVEM ELS ELEMENTS TOTALS
            self.assertEqual(sum_pf, 2)

    def test_catalog_flags_coherence_punts_frontera(self):
        """
        Test that all the catalog flags are set correctly by it's coherence
        :return:
        """

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            # INICIALITZEM OBJECTES
            pf_obj = self.openerp.pool.get('giscedata.punt.frontera')
            cataleg_obj = self.openerp.pool.get('giscegis.cataleg.cataleg')

            # SELECCIONEM UNICAMENT ELS ELEMENTS DE CATALEG QUE SIGUIN PF
            ids_pf_cataleg = cataleg_obj.search(
                cursor, uid, [('model.model', '=', 'giscedata.punt.frontera')]
            )
            # PER A CADA ELEMENT DEL CATALEG COMPROVAREM QUE ELS FLAGS SIGUIN
            # COHERENTS
            for id_pf_cataleg in ids_pf_cataleg:
                cataleg_pf = cataleg_obj.read(
                    cursor, uid, id_pf_cataleg,
                    ['not_topology', 'read_only', 'snap_to']
                )
                if cataleg_pf['read_only']:
                    self.assertTrue(cataleg_pf['not_topology'])
                else:
                    self.assertFalse(cataleg_pf['not_topology'])

                if cataleg_pf['not_topology']:
                    self.assertFalse(cataleg_pf['snap_to'])
                else:
                    self.assertTrue(cataleg_pf['snap_to'])
