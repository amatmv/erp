from destral import testing
from destral.transaction import Transaction


__all__ = [
    'TestPuntsFronteraSession'
]


class TestPuntsFronteraSession(testing.OOTestCase):

    def setUp(self):
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user

    def tearDown(self):
        self.txn.stop()

    def test_search_session_punt_frontera(self):
        """
        This test is for check the correct behaviour of the sessions and the
        PuntsFrontera
        """
        # Load ERP models
        pf_obj = self.openerp.pool.get('giscedata.punt.frontera')
        session_obj = self.openerp.pool.get('giscegis.session')

        # Clean the sessions to avoid BBox collisions
        sql = """
            TRUNCATE giscegis_session CASCADE
        """
        self.cursor.execute(sql)

        # Create the sessions
        session1_vals = {
            "name": 'Testing session 1',
            "bbox": 'POLYGON((975858.32716481 4662698.41414981,982861.661292283 '
                    '4662698.41414981,982861.661292283 4666554.0233022,'
                    '975858.32716481 4666554.0233022,975858.32716481 '
                    '4662698.41414981))',
            "state": 'open',
            "ttl": 500000000,
            "active": True
        }
        session2_vals = {
            "name": 'Testing session 2',
            "bbox": 'POLYGON((980421.820494505 4659689.90639627,'
                    '983923.487558242 4659689.90639627,983923.487558242 '
                    '4661617.71097246,980421.820494505 4661617.71097246,'
                    '980421.820494505 4659689.90639627))',
            "state": 'open',
            "ttl": 500000000,
            "active": True
        }
        session1_id = session_obj.create(self.cursor, self.uid, session1_vals)
        session2_id = session_obj.create(self.cursor, self.uid, session2_vals)

        # Start the checks
        # Check the number of PuntsFrontera in differents sessions and without
        # sessions
        # No session
        pf_ids = pf_obj.search(self.cursor, self.uid, [])
        self.assertEqual(len(pf_ids), 2)
        # Session 1
        pf_ids = pf_obj.search(
            self.cursor, self.uid, [], context={'session': session1_id}
        )
        self.assertEqual(len(pf_ids), 1)
        # Session 2
        pf_ids = pf_obj.search(
            self.cursor, self.uid, [], context={'session': session2_id}
        )
        self.assertEqual(len(pf_ids), 1)

        # Get 2 ids from the session 2
        pf1_id = pf_ids[0]

        # Check that the changes made on a session are only visible from the
        # session
        # Load data before changes
        pf_data_original = pf_obj.read(
            self.cursor, self.uid, [pf1_id], ['rotation']
        )
        # Write new data on a PuntFrontera
        pf_write_vals = {
            "rotation": 240
        }
        pf_obj.write(
            self.cursor, self.uid, [pf1_id],
            pf_write_vals, context={'session': session2_id}
        )
        # Reload data outside the session
        pf_data = pf_obj.read(
            self.cursor, self.uid, [pf1_id], ['rotation']
        )
        # Compare data outside session before and after write data on session 2
        self.assertEqual(
            pf_data_original[0]['rotation'],
            pf_data[0]['rotation']
        )

        # Check changes inside the session
        pf_data = pf_obj.read(
            self.cursor, self.uid, [pf1_id], ['rotation'],
            context={'session': session2_id}
        )

        self.assertEqual(pf_data[0]['rotation'], 240)
