# -*- coding: utf-8 -*-
{
    "name": "Punt Frontera GIS",
    "description": """
        Modul GIS del Punt Frontera de la Xarxa Elèctrica
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEGis",
    "depends":[
        "base",
        "giscegis_base",
        "giscegis_base_geom",
        "giscegis_cataleg",
        "giscegis_node_bloc_model",
        "giscedata_punt_frontera"
    ],
    "init_xml": [],
    "demo_xml": [
        "giscegis_punt_frontera_demo.xml"
    ],
    "update_xml":[
        "giscegis_punt_frontera_cataleg_data.xml"
    ],
    "active": False,
    "installable": True
}
