from tools import config
from base_index.base_index import BaseIndex


class GiscegisBaseIndex(BaseIndex):

    def is_indexable(self, context):
        return context.get("index", True) and 'session' not in context

    def get_db_srid(self, cursor, uid, context=None):
        return config.get('srid')
