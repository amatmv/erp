# -*- coding: utf-8 -*-
{
    "name": "Giscegis Base index",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Hace que los elementos del GIS no se indexen cuando estan en una session
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "base_index"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
