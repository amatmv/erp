from osv import osv, fields
from tools import config


class GiscegisNodesGeom(osv.osv):
    _name = 'giscegis.nodes.geom'
    _auto = False
    _description = 'Vertex'
    _columns = {
        'name': fields.char('IDNode', size=10, required=True),
        'vertex': fields.many2one('giscegis.vertex', 'Vertex'),
        "materialize_date": fields.datetime("Fecha creacion vista"),
    }

    def init(self, cursor):
        """
        Initializes the module and creates the materialized view
        :param cursor: cursor Database cursor
        :return: True
        :rtype: bool
        """
        cursor.execute("DROP MATERIALIZED VIEW IF EXISTS giscegis_nodes_geom;")
        cursor.execute("""
            CREATE MATERIALIZED VIEW giscegis_nodes_geom AS
                SELECT 
                    nodes.id as id,
                    nodes.name AS name,
                    ST_SETSRID(
                        ST_MAKEPOINT(vertex.x,vertex.y), %(srid)s
                    ) AS geom,
                    now() AS materialize_date
                FROM giscegis_nodes AS nodes
                LEFT JOIN giscegis_vertex AS vertex ON 
                    nodes.vertex=vertex.id;
        """, {'srid': config.get('srid', 25830)})
        return True

    def get_srid(self, cursor, uid):
        """
        :param cursor: cursor Database cursor
        :param uid: integer User identifier
        :return: Returns a list of srids in geom field
        """

        return [config.get('srid', 25830)]

    def get_time(self, cr, uid):
        """
        :param cursor: cursor Database cursor
        :param uid: integer User identifier
        :return: Returns time
        """
        cr.execute("SELECT to_char(now(),'YYYY-MM-DD HH24:MI:SS')")
        res = cr.fetchone()
        return res[0]

    def get_coords(self, cursor, uid, ids, srid=None, context=None):
        """ Get the lat,lon of a node
        :param cursor: cursor Database cursor
        :param uid: integer User identifier
        :param ids: integer Single id of the node
        :param srid: integer Optional SRID for the output's projection
        :param context: context Context
        :return: X and Y of the node in a dict
        """
        if isinstance(ids, (list, tuple)):
            ids = ids[0]
        if not srid:
            srid = config.get('srid', 25830)
        sql = """
            SELECT
                ST_X(ST_TRANSFORM(geom, %(srid)s)),
                ST_Y(ST_TRANSFORM(geom, %(srid)s))
            FROM giscegis_nodes_geom n
            WHERE n.id = %(ids)s;
        """
        sql_params = {
            'ids': str(ids),
            'srid': srid
        }
        cursor.execute(sql, sql_params)
        data = cursor.fetchone()
        if data is None:
            return {'x': 0, 'y': 0}
        return {'x': data[0], 'y': data[1]}

    def recreate(self, cursor, uid):
        """
        Refreshes the giscegis_nodes_geom materialized view
        :param cursor: cursor Database cursor
        :param uid: integer User identifier
        :return:
        """
        cursor.execute("""
            REFRESH MATERIALIZED VIEW giscegis_nodes_geom;
        """)
        return True

GiscegisNodesGeom()
