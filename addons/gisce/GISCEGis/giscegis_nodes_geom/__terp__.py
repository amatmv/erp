# -*- coding: utf-8 -*-
{
    "name": "GISCE GISv3 Nodes",
    "description": """Modulo para visualizar nodos en el visor web de Gisce Gis""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_vertex",
        "giscegis_nodes",
        "giscegis_base_geom"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscegis_nodes_geom_view.xml",
        "ir.model.access.csv",
        "security/ir.model.access.csv",
        "giscegis_layers_data.xml"
    ],
    "active": False,
    "installable": True
}
