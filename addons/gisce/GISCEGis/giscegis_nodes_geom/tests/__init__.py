from destral import testing
from destral.transaction import Transaction


class GiscegisNodesGeomTest(testing.OOTestCase):

    require_demo_data = True

    def test_get_srid(self):
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            nodes_obj = self.openerp.pool.get('giscegis.nodes')
            nodes_geom_obj = self.openerp.pool.get('giscegis.nodes.geom')
            if not nodes_obj.search(cursor, uid, []):
                self.assertFalse(nodes_geom_obj.get_srid(cursor, uid))
            else:
                self.assertGreater(nodes_geom_obj.get_srid(cursor, uid), 0)
            nodes_geom_obj.recreate(cursor, uid)
            self.assertGreater(nodes_geom_obj.get_srid(cursor, uid), 0)

    def test_get_coords(self):
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            nodes_obj = self.openerp.pool.get('giscegis.nodes')
            nodes_geom_obj = self.openerp.pool.get('giscegis.nodes.geom')

            nodes_geom_obj.recreate(cursor, uid)
            # Per seguir amb el funcionament que havia tingut fins ara, mirarem
            # nomes aquells nodes que en les dades de demo tenen vertex ja que
            # la vista materialitzada que no es de Qgis nomes sap crear geom
            # a partir dels vertex
            ids = nodes_obj.search(cursor, uid, [('vertex', '!=', None)])
            for identificador in ids:
                self.assertEqual(
                    nodes_obj.get_coords(cursor, uid, identificador),
                    nodes_geom_obj.get_coords(cursor, uid, identificador)
                )
