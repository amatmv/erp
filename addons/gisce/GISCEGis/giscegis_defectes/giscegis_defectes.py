# -*- coding: utf-8 -*-

from osv import fields, osv


class GiscedataRevisionsBtTipusvaloracio(osv.osv):

    _name = "giscedata.revisions.bt.tipusvaloracio"
    _description = 'Defectes de baixa tensió per a Gis'
    _inherit = "giscedata.revisions.bt.tipusvaloracio"
    _columns = {
        'name_gis': fields.text('Valoració Gis')
    }

GiscedataRevisionsBtTipusvaloracio()


class GiscedataRevisionsAtTipusvaloracio(osv.osv):
    _name = "giscedata.revisions.at.tipusvaloracio"
    _description = 'Defectes d\'alta tensió per a Gis'
    _inherit = "giscedata.revisions.at.tipusvaloracio"
    _columns = {
        'name_gis': fields.text('Valoració Gis')
    }

GiscedataRevisionsAtTipusvaloracio()


class GiscedataRevisionsCtTipusvaloracio(osv.osv):
    _name = "giscedata.revisions.ct.tipusvaloracio"
    _description = 'Defectes de centres transformadors per a Gis'
    _inherit = "giscedata.revisions.ct.tipusvaloracio"
    _columns = {
        'name_gis': fields.text('Valoració Gis')
    }

GiscedataRevisionsCtTipusvaloracio()


