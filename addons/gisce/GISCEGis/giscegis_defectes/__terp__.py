# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS v3 DEFECTES",
    "description": """Mòdul pel GIS v3 de defectes""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_base",
        "giscedata_revisions",
        "giscedata_revisions_bt",
        "giscegis_layers"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscegis_layers_data.xml"
    ],
    "active": False,
    "installable": True
}
