# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* giscegis_defectes
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.14\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2018-11-22 05:47\n"
"PO-Revision-Date: 2018-11-22 05:47\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: giscegis_defectes
#: field:giscedata.revisions.at.tipusvaloracio,name_gis:0
#: field:giscedata.revisions.bt.tipusvaloracio,name_gis:0
#: field:giscedata.revisions.ct.tipusvaloracio,name_gis:0
msgid "Valoració Gis"
msgstr ""

#. module: giscegis_defectes
#: model:ir.module.module,shortdesc:giscegis_defectes.module_meta_information
msgid "GISCE GIS v3 DEFECTES"
msgstr ""

#. module: giscegis_defectes
#: model:ir.module.module,description:giscegis_defectes.module_meta_information
msgid "Mòdul pel GIS v3 de defectes"
msgstr ""

