# coding=utf-8

import logging


def up(cursor, installed_version):

    logger = logging.getLogger('openerp.migration')
    logger.info(
        'Setting UP the GiscedataBtCaixaElement sequence for the field NAME.'
    )

    sql = """
        UPDATE ir_sequence AS seq 
        SET number_next = data.num
        FROM (
          SELECT coalesce(max(elem.name::int),0)+1 AS num,
                 seq.id AS id
          FROM giscedata_bt_caixa_element elem, ir_sequence seq
          WHERE seq.name='Numeració BtCaixaElement'
          AND seq.code='giscedata.bt.caixa.element'
          GROUP BY seq.id
        ) AS data
        WHERE seq.id=data.id
    """
    cursor.execute(sql)

    logger.info(
        'GiscedataBtCaixaElement NAME sequence set up with the current value'
    )


def down(cursor, installed_version):
    pass


migrate = up
