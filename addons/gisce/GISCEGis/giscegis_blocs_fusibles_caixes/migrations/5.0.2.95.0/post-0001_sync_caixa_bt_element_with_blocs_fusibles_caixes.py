# coding=utf-8

import logging
import pooler


def up(cursor, installed_version):

    logger = logging.getLogger('openerp.migration')

    logger.info(
        'Sync BtCaixaElement with BlocsFusiblesCaixes'
    )

    pool = pooler.get_pool(cursor.dbname)
    bt_caixa_element = pool.get('giscedata.bt.caixa.element')
    n_created_elements = bt_caixa_element.sync_elements_from_blocs(cursor, 1)

    if n_created_elements == 1:
        msg = '1 BtCaixaElement sync correctly with BlocsFusiblesCaixes'
    else:
        msg = "{} BtCaixaElements sync correctly with " \
              "BlocsFusiblesCaixes".format(n_created_elements)

    logger.info(msg)


def down(cursor, installed_version):
    pass


migrate = up
