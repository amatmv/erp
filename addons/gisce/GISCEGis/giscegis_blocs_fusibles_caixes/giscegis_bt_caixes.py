# -*- coding: utf-8 -*-
from osv import osv
from tools import config
import logging
from giscegis_base_geom.wizard.giscegis_shp_loader \
    import GIS_MODELS_TO_UPDATE_GEOM_DATA


# Subscribe this model to the list to be updated if there is an AutoCAD dump
GIS_MODELS_TO_UPDATE_GEOM_DATA.append(
    {
        'model': 'giscedata.bt.caixa.element',
        'functions': ['sync_elements_from_blocs']
    },
)


class GiscedataBtCaixaElement(osv.osv):

    _name = 'giscedata.bt.caixa.element'
    _inherit = 'giscedata.bt.caixa.element'

    def sync_elements_from_blocs(self, cursor, uid):
        """
        Syncronize the BtCaixaElement elements with BlocsFusiblesCaixes elements
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return: Return the number of elements syncronized.
        :rtype: int
        """

        bt_caixa_element = self.pool.get('giscedata.bt.caixa.element')
        bt_caixa_element_tipus = self.pool.get(
            'giscedata.bt.caixa.element.tipus'
        )
        logger = logging.getLogger('openerp.sync.bt.caixa.element')

        sql = """
            SELECT b.rotation, b.node, b.intensitat, b.name, bn.name AS tipus,
                b.codi,
                st_astext(st_setsrid(st_makepoint(v.x, v.y), %(srid)s)) AS geom
            FROM giscegis_blocs_fusibles_caixes b
            LEFT JOIN giscegis_blocs_fusiblescaixes_blockname bn 
                ON bn.id = b.blockname
            LEFT JOIN giscegis_vertex v ON b.vertex = v.id
        """

        cursor.execute(sql, {"srid": config.get("srid", 25830)})

        rows = cursor.dictfetchall()
        row_n = len(rows)

        for row in rows:

            vals = {
                'rotation': 360-row['rotation'],
                'codi': row['codi'],
                'geom': row['geom'],
                'intensitat': row['intensitat'],
                'node_id': row['node'],
                'autocad_code': row['name']
            }

            bt_caixa_elem_tipus_id = bt_caixa_element_tipus.search(
                cursor, uid, [('code', '=', row['tipus'])]
            )

            if bt_caixa_elem_tipus_id:
                vals['tipus'] = bt_caixa_elem_tipus_id[0]
            else:
                logger.warning(
                    "GiscedataBtCaixaElementTipus with code {} not found, "
                    "skiping the creation of the element from the model "
                    "GiscegisBlocsFusiblesCaixes with name {}".format(
                        row['tipus'], row['name']
                    )
                )
                row_n -= 1
                continue

            bt_caixa_element_ids = bt_caixa_element.search(
                cursor, uid, [('codi', '=', row['codi'])]
            )

            if bt_caixa_element_ids:
                bt_caixa_element.write(
                    cursor, uid, bt_caixa_element_ids[0], vals
                )
            else:
                vals['name'] = bt_caixa_element.default_get(
                    cursor, uid, ['name']
                )['name']
                bt_caixa_element.create(cursor, uid, vals)

        return row_n


GiscedataBtCaixaElement()
