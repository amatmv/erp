# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Edge",
    "description": """Modulo de aristas para GIS""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_vertex",
        "giscegis_polyline",
        "giscegis_base",
        "giscegis_base_geom",
        "giscegis_nodes"
    ],
    "init_xml": [],
    "demo_xml": [
        "giscegis_edge_demo.xml",
        "giscegis_edge_sequence_demo.xml"
    ],
    "update_xml":[
        "giscegis_edge_view.xml",
        "ir.model.access.csv",
        "security/ir.model.access.csv",
        "giscegis_cataleg_edge_data.xml",
        "giscegis_edge_sequence.xml"
    ],
    "active": False,
    "installable": True
}
