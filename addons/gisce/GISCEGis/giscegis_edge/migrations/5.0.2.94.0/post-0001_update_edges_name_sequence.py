# coding=utf-8

import logging
import pooler


def up(cursor, installed_version):

    logger = logging.getLogger('openerp.migration')
    logger.info(
        'Setting UP the Edges Name fields and the sequence for the field NAME.'
    )

    pool = pooler.get_pool(cursor.dbname)
    edges_obj = pool.get('giscegis.edge')
    edges_obj.update_name_and_sequence(cursor, 1)

    logger.info(
        'Edges NAME updated and NAME sequence set up with the current value'
    )


def down(cursor, installed_version):
    pass


migrate = up
