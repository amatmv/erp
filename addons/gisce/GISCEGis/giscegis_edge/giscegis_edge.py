# -*- coding: utf-8 -*-
from osv import osv, fields
from giscegis_base_geom.giscegis_session import SessionManaged
from tools import config
from tools.translate import _
from giscegis_base_geom.wizard.giscegis_shp_loader \
    import GIS_MODELS_TO_UPDATE_GEOM_DATA
from giscegis_base_geom.giscegis_base_geom import DROP_ON_AUTOCAD_DUMP

# Subscribe this model to the list to be updated if there is an AutoCAD dump
GIS_MODELS_TO_UPDATE_GEOM_DATA.append(
    {
        'model': 'giscegis.edge',
        'functions': ['update_name_and_sequence']
    },
)
DROP_ON_AUTOCAD_DUMP.append('giscegis_edge')


class GiscegisEdgetype(osv.osv):
    _name = "giscegis.edgetype"
    _description = "EdgeType"
    _columns = {
        'name': fields.char('Name', size=25, required=True),
        'codi': fields.integer('Codi'),
        'description': fields.text('Description'),
    }


GiscegisEdgetype()


class GiscegisEdge(SessionManaged):
    _name = 'giscegis.edge'
    _description = 'Edges'

    def _get_nextnumber(self, cursor, uid, context=None):
        """
        Returns the next node numbers from the sequence giscegis.edge
        :param cursor: Database Cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :param context: OpenERP context
        :type context: dic[str,Any]
        :return: Next GiscegisEdge name value
        :rtype: int
        """

        return self.pool.get('ir.sequence').get(
            cursor, uid, 'giscegis.edge'
        )

    def clean(self, cr, uid):
        obj_model = self.pool.get("ir.module.module")
        res_at = obj_model.search(
            cr, uid,
            [
                ("state", "=", "installed"),
                ("name", "=", "giscegis_at"),
            ])
        if res_at:
            cr.execute("UPDATE giscedata_at_tram SET edge_id=NULL")

        res_bt = obj_model.search(
            cr, uid,
            [
                ("state", "=", "installed"),
                ("name", "=", "giscegis_bt"),
            ])
        if res_bt:
            cr.execute("UPDATE giscedata_bt_element SET edge_id=NULL")
        cr.execute("DELETE FROM giscegis_edge")
        cr.execute("TRUNCATE giscegis_lbt_traceability")
        cr.commit()
        return True

    def get_edge_polyline_multi(self, cursor, uid, ids, context={}):
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        res = dict.fromkeys(ids, False)
        sql = '''SELECT e.id,v.x, v.y, vp.sequence
        FROM giscegis_edge e,
             giscegis_vertex v,
             giscegis_polyline_vertex vp,
             giscegis_polyline_vertex_rel r
        WHERE
          vp.vertex = v.id
          AND r.vertex_id = vp.id
          AND r.polyline_id = e.polyline
          AND e.id in %(ids)s
        ORDER BY vp.sequence ASC'''
        if ids:
            cursor.execute(sql, {'ids': tuple(ids)})
            data = cursor.dictfetchall()
            for row in data:
                identifier = row.pop('id')
                res[identifier] = row.values()
        print res
        return res

    def get_edge_polyline(self, cursor, uid, ids, context={}):
        ''' Returns polyline '''
        if isinstance(ids, (list, tuple)):
            ids = ids[0]

        sql = '''SELECT v.x, v.y, vp.sequence
        FROM giscegis_edge e,
             giscegis_vertex v,
             giscegis_polyline_vertex vp,
             giscegis_polyline_vertex_rel r
        WHERE
          vp.vertex = v.id
          AND r.vertex_id = vp.id
          AND r.polyline_id = e.polyline
          AND e.id = %s
        ORDER BY vp.sequence ASC'''

        cursor.execute(sql, (ids,))
        return cursor.dictfetchall()

    def get_geom_multi(self, cursor, uid, ids, srid=False, mergeGeoms=False):
        """
        Returns geom in GeoJSON

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: Edge ids
        :type ids: list(int)
        :param srid: Generated geoJSON srid
        :param mergeGeoms: If the geometries will be merged
        :type mergeGeoms: bool
        :return:
        """
        """  """
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        res = dict.fromkeys(ids, False)
        if ids:
            btlike = self.pool.get('giscegis.base.geom').get_btlike_layer(
                cursor, uid
            )
            if mergeGeoms:
                sql = """
                SELECT ST_AsGeoJSON(ST_Union(geom)) FROM (
                  SELECT ST_Transform(lbt.geom, %(srid)s) as geom
                    FROM giscegis_edge e
                    LEFT JOIN giscegis_lbt_geom lbt ON
                          (lbt.name=e.id_linktemplate)
                    WHERE
                        e.id in %(id)s AND
                        e.layer LIKE %(btlike)s AND
                        lbt.geom IS NOT NULL
                  UNION
                    SELECT ST_Transform(lat.geom, %(srid)s) as geom
                    FROM giscegis_edge e
                    LEFT JOIN giscedata_at_tram t ON (t.name=e.id_linktemplate)
                    LEFT JOIN giscegis_lat_geom lat ON (lat.name=t.id)
                    WHERE
                        e.id in %(id)s AND
                        e.layer NOT LIKE %(btlike)s AND
                        lat.geom IS NOT NULL
                  UNION
                    SELECT ST_Transform(bt.geom, %(srid)s) as geom
                        FROM giscedata_bt_element AS bt
                        WHERE
                            edge_id in %(id)s AND
                            bt.geom IS not NULL
                  UNION
                    SELECT ST_Transform(at.geom, %(srid)s) as geom
                        FROM giscedata_at_tram AS at
                        WHERE
                            at.edge_id in %(id)s AND
                            at.geom IS not NULL
                ) as t;
                """
            else:
                sql = """
                SELECT ST_AsGeoJSON(ST_Transform(lbt.geom, %(srid)s))
                    FROM giscegis_edge e
                    LEFT JOIN giscegis_lbt_geom lbt ON (lbt.name=e.id_linktemplate)
                    WHERE
                        e.id in %(id)s AND
                        e.layer LIKE %(btlike)s AND
                        lbt.geom IS NOT NULL
                UNION
                    SELECT ST_AsGeoJSON(ST_Transform(lat.geom, %(srid)s))
                    FROM giscegis_edge e
                    LEFT JOIN giscedata_at_tram t ON (t.name=e.id_linktemplate)
                    LEFT JOIN giscegis_lat_geom lat ON (lat.name=t.id)
                    WHERE
                        e.id in %(id)s AND
                        e.layer NOT LIKE %(btlike)s AND
                        lat.geom IS NOT NULL
                UNION
                    SELECT ST_Transform(bt.geom, %(srid)s) as geom
                        FROM giscedata_bt_element AS bt
                        WHERE
                            edge_id in %(id)s AND
                            bt.geom IS not NULL
                  UNION
                    SELECT ST_Transform(at.geom, %(srid)s) as geom
                        FROM giscedata_at_tram AS at
                        WHERE
                            at.edge_id in %(id)s AND
                            at.geom IS not NULL
                """
            if not srid:
                srid = config.get('srid', 25830)

            sql_params = {'srid': srid,
                          'id': tuple(ids),
                          'btlike': btlike
                          }
            cursor.execute(sql, sql_params)
            data = cursor.fetchall()
            if mergeGeoms:
                res = data[0]
            else:
                for row in data:
                    res[row[0]] = row[1:]
            return res
        else:
            return []

    def get_geom(self, cursor, uid, ids, srid=False):
        """
        Returns geom in GeoJSON

        :param cursor: Database cursor
        :param uid: User id
        :param ids: id of the geometry
        :param srid: Desired SRID
        :return: Geomerty in str in GEOJSON
        """

        if isinstance(ids, (list, tuple)):
            ids = ids[0]

        btlike = self.pool.get('giscegis.base.geom').get_btlike_layer(
            cursor, uid
        )
        sql = '''
            SELECT ST_AsGeoJSON(ST_Transform(lbt.geom, %(srid)s))
            FROM giscegis_edge e
            LEFT JOIN giscegis_lbt_geom lbt ON (lbt.name=(SELECT id_linktemplate FROM giscegis_edge WHERE id = %(id)s))
            WHERE e.id = %(id)s AND e.layer LIKE %(btlike)s
        UNION
            SELECT ST_AsGeoJSON(ST_Transform(lat_geom.geom, %(srid)s))
            FROM giscegis_edge e
            LEFT JOIN giscedata_at_tram t ON (t.name=(SELECT id_linktemplate FROM giscegis_edge WHERE id = %(id)s))
            LEFT JOIN giscegis_lat_geom lat_geom ON (lat_geom.name=t.id)
            WHERE e.id = %(id)s
            AND e.layer NOT LIKE %(btlike)s
        '''

        if not srid:
            srid = config.get('srid', 25830)

        cursor.execute(sql, {'srid': srid, 'id': ids, 'btlike': btlike})
        data = cursor.dictfetchall()

        if data and data[0]['st_asgeojson'] is not None:
            return data[0]['st_asgeojson']
        else:
            sql_geom = """
                    SELECT ST_AsGeoJSON(ST_Transform(geom, %(srid)s))
                    FROM giscedata_at_tram WHERE edge_id = %(ids)s
                UNION
                    SELECT ST_AsGeoJSON(ST_Transform(geom, %(srid)s))
                    FROM giscedata_bt_element WHERE edge_id = %(ids)s
            """
            cursor.execute(sql_geom, {"ids": ids, "srid": srid})
            data = cursor.dictfetchall()
            if data:
                return data[0]["st_asgeojson"]
        return data

    def _get_edge_geom(self, cr, uid, ids, field_name, arg, context):
        res = dict.fromkeys(ids, False)
        sql = """
            SELECT  e.id AS id,
                    ST_AsText(ST_MakeLine(e_ns.geom, e_nf.geom)) AS edge_geom
            FROM giscegis_edge e
                LEFT JOIN giscegis_nodes e_ns ON e_ns.id = e.start_node
                LEFT JOIN giscegis_nodes e_nf ON e_nf.id = e.end_node
            WHERE
                e.id IN %(edge_ids)s
        """
        cr.execute(sql, {"edge_ids": tuple(ids)})

        for row in cr.dictfetchall():
            res[row['id']] = row['edge_geom']

        return res

    def _search_edges(self, cursor, uid, obj, name, args, context=None):
        """
        This function adapt the search args to be able to search for the
        function field geom of the edges.
        :param cursor: Database cursor
        :type cursor: psycopg2 cursor
        :param uid: User ID
        :type uid: int
        :param obj: Object that we are about to search to
        :type obj: GiscegisEdge
        :param name: Field that have triggered the search
        :type name: str
        :param args: List of the search parameters
        :type args: list of (str, str, int or str)
        :param context: Openerp context
        :type context: dict[str,any]
        :return: Return the search params adapted to be able to execute it.
        :rtype: list of (str, str, int or str)
        """

        if context is None:
            context = {}

        if args:
            return [
                '|',
                ('start_node.geom', args[0][1], args[0][2]),
                ('end_node.geom', args[0][1], args[0][2])
            ]
        else:
            return []

    def update_name_and_sequence(self, cursor, uid):
        """
        This function updated the name of the Edges to make it only numeric and
        not alphanumeric. It updates the sequence of the Edges names to keep it
        updated and coherent with the edges data.
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return: None
        :rtype: NoneType
        """
        sql = """
            UPDATE giscegis_edge
            SET name = id
        """

        cursor.execute(sql)

        sql = """
            UPDATE ir_sequence AS seq
            SET number_next = data.num
            FROM (
              SELECT coalesce(max(n.name::int),0)+1 AS num,
                     seq.id AS id
              FROM giscegis_edge n, ir_sequence seq
              WHERE seq.name='Numeració Edges'
              AND seq.code='giscegis.edge'
              GROUP BY seq.id
            ) AS data
            WHERE seq.id=data.id
        """

        cursor.execute(sql)

    def clean_non_vinculated(self, cursor, uid, nodes=True, context=None):
        """
        Deletes the non vinculated edges and nodes if specified with the flag
        nodes.
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :param nodes: Flag to clean nodes non referenced nodes
        :type nodes: bool
        :param context: OpenERP context
        :type context: dict[str, Any]
        :return: Number of deleted edges
        :rtype: int
        """
        sql_update_bt = """
            UPDATE giscedata_bt_element
            SET edge_id=NULL
            WHERE NOT active;
        """

        sql_update_at = """
            UPDATE giscedata_bt_element
            SET edge_id=NULL
              WHERE NOT active;
        """

        cursor.execute(sql_update_bt)
        cursor.execute(sql_update_at)

        if context is None:
            context = {}

        sql_edges = """
            SELECT id
            FROM giscegis_edge
            WHERE id  NOT IN (
                    SELECT edge_id
                    FROM giscedata_bt_element
                    WHERE edge_id IS NOT NULL
                UNION
                    SELECT edge_id
                    FROM giscedata_at_tram
                    WHERE edge_id IS NOT NULL
            );
        """
        cursor.execute(sql_edges)
        result = cursor.fetchall()

        ids = [r[0] for r in result]

        self.unlink(cursor, uid, ids, context=context)

        if nodes:
            obj_nodes = self.pool.get("giscegis.nodes")
            obj_nodes.clean_non_vinculated(cursor, uid, context=context)

        return len(ids)

    _columns = {
        'name': fields.char('IdLink', size=10, required=True),
        'start_node':
            fields.many2one('giscegis.nodes', 'Start Node', required=True,
                            ondelete='set null'),
        'end_node':
            fields.many2one('giscegis.nodes', 'End Node', required=True,
                            ondelete='set null'),
        'polyline': fields.many2one(
            'giscegis.polyline', 'Polyline', ondelete='set null'
        ),
        'layer': fields.char('Layer', size=25),
        'id_linktemplate': fields.integer('ID Link Template'),
        'type': fields.many2one('giscegis.edgetype', 'Type'),
        'geom': fields.function(
            _get_edge_geom, type='Line', method=True, string='Geometria',
            fnct_search=_search_edges
        ),
        'active': fields.boolean('Actiu', select=True)
    }

    _defaults = {
        'name': _get_nextnumber,
        'active': lambda *a: 1
    }
    _order = "name, id"
    _sql_constraints = [
        (
            "start_end_diferent",
            "CHECK(start_node != end_node)",
            _("No es poden crear arestes amb el node d'inici igual al node final")
        )
    ]


GiscegisEdge()
