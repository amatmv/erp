from osv import osv

class GiscegisSession(osv.osv):
    _name = 'giscegis.session'
    _inherit = 'giscegis.session'

    def merge(self, cursor, uid, ids, context=None):
        """
        Overwrites the session merge to clean the edges and nodes

        :param cursor: Database cursor
        :param uid: User id
        :param ids: Session id
        :param context: OpenERP context
        :return: True
        :rtype: bool
        """
        res = super(GiscegisSession, self).merge(
            cursor, uid, ids, context=context
        )
        obj_edg = self.pool.get('giscegis.edge')
        obj_edg.clean_non_vinculated(cursor, uid, nodes=True, context=context)
        return res


GiscegisSession()
