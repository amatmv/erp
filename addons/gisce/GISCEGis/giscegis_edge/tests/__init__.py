# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from addons import get_module_resource
import base64
import netsvc
from tools import config


class BaseTest(testing.OOTestCase):

    def test_get_geom(self):
        """
        Tests the get_geom function

        :return: None
        """
        logger = netsvc.Logger()
        self.openerp.install_module('giscegis_lat_geom')
        self.openerp.install_module('giscegis_lbt_geom')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            node_model = self.openerp.pool.get('giscegis.nodes')
            edge_model = self.openerp.pool.get('giscegis.edge')
            poly_model = self.openerp.pool.get('giscegis.polyline')
            vertex_model = self.openerp.pool.get('giscegis.vertex')
            lat_geom = self.openerp.pool.get('giscegis.lat.geom')
            tram_model = self.openerp.pool.get('giscedata.at.tram')

            # Create vertex
            v1 = vertex_model.create(cursor, uid, {
                'name': '1',
                'x': 0,
                'y': 0
            })
            v2 = vertex_model.create(cursor, uid, {
                'name': '2',
                'x': 1,
                'y': 1
            })
            v3 = vertex_model.create(cursor, uid, {
                'name': '3',
                'x': 2,
                'y': 2
            })

            # Create nodes
            n1 = node_model.create(cursor, uid, {
                'name': '1',
                'vertex': v1
            })
            n2 = node_model.create(cursor, uid, {
                'name': '2',
                'vertex': v2
            })
            n3 = node_model.create(cursor, uid, {
                'name': '3',
                'vertex': v3
            })

            # Create polyline

            poly_model.create(cursor, uid, {
                'name': '1',
                'vertexs': (0, [v1, v2])
            })

            poly_model.create(cursor, uid, {
                'name': '2',
                'vertexs': (0, [v2, v3])
            })

            # Create trams
            t1 = tram_model.create(cursor, uid, {
                'name': '1'
            })
            t2 = tram_model.create(cursor, uid, {
                'name': '2'
            })

            srid = config.get('srid', 25830)

            cursor.execute("""
                SELECT ST_TRANSFORM('0105000020F75900000100000001020000000600000000700B79E0E85440A0CD00ACEA6964400090B9184CE7544062E3308CE269644000A0A9184CE75440B9097FC99069644000509C43A0E854407778A6EE7969644000507CC015E95440BD58B5779C69644000A0190C22E954406A6E3CB6CC696440'::geometry, 
                %s) AS new_geom
            """, (srid,))
            geom = cursor.dictfetchall()[0]['new_geom']
            cursor.execute("""
            INSERT INTO giscegis_lat VALUES (0, %s,	%s);
            """, (t1, geom,))

            cursor.execute("""
                SELECT ST_TRANSFORM('0105000020F75900000100000001020000000600000000D0B59998E75440350ABF3F316A644000204C352DE65440E3CA14CD136A644000A0B2635EE65440C9722BA18C6A644000E0AA098FE754409AA1B8B0906A644000A0C725CBE75440A8285DD9586A644000E0A095C1E7544059150FA62A6A6440'::geometry, 
                %s) AS new_geom
            """, (srid,))
            geom = cursor.dictfetchall()[0]['new_geom']
            cursor.execute("""
            INSERT INTO giscegis_lat VALUES (1, %s,  %s);
            """, (t2, geom,))

            lat_geom.recreate(cursor, uid)
            # Create edges
            e1 = edge_model.create(cursor, uid, {
                'name': '144',
                'id_linktemplate': t1,
                'start_node': n1,
                'end_node': n2,
                'polyline': 1,
                'layer': '_LINIES MT'
            })
            e2 = edge_model.create(cursor, uid, {
                'name': '4',
                'id_linktemplate': t2,
                'start_node': n2,
                'end_node': n3,
                'polyline': 2,
                'layer': '_LINIES MT'
            })

            r1 = edge_model.get_geom(cursor, uid, e1, 4326)
            self.assertEqual(r1, '{"type":"MultiLineString","coordinates":[[[-1.48871850933181,0.000378658057118],[-1.48871873044477,0.000378649112092],[-1.48871873044473,0.000378559096174],[-1.4887185444409,0.00037853393402],[-1.48871848019863,0.000378571956585],[-1.48871847347563,0.000378625072087]]]}')

            r2 = edge_model.get_geom(cursor, uid, e2, 4326)
            self.assertEqual(r2, '{"type":"MultiLineString","coordinates":[[[-1.48871868861263,0.000378735760074],[-1.48871888731549,0.000378703338393],[-1.48871886042344,0.000378836367016],[-1.48871869384171,0.000378840838118],[-1.48871866097353,0.000378779358633],[-1.48871866620253,0.000378728493339]]]}')
