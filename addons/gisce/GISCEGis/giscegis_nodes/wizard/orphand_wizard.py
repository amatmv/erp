# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _


class GiscegisOrphand(osv.osv_memory):

    _name = 'giscegis.nodes.orphand'

    def _default_new_name(self, cursor, uid, context=None):
        """
        Method that returns the default value for new_name for the wizard
        :param cursor: OpenERP cursor
        :param uid: User id
        :param context: OpenERP context
        :return: Default value as string
        """

        if len(context['active_ids']) > 1:
            return ''
        model_nodes = self.pool.get('giscegis.nodes')
        identifier = context['active_id']
        node_name = model_nodes.read(cursor, uid, identifier, ['name'], context=context)['name']
        return node_name.replace('*', '')

    def handle_orphand(self, cursor, uid, ids, context=None):
        """
        Method that handles oprhand nodes wizard

        :param cursor: Database cursor
        :param uid:User id
        :param ids: list of afected ids
        :param context: OpenERP context
        :return: True
        """
        if context is None:
            context = {}

        wizard = self.browse(cursor, uid, ids[0], context)
        node = wizard.new_name

        model_nodes = self.pool.get('giscegis.nodes')
        model_nodes.raw_write(
            cursor, uid, context['active_id'], {'name': node}, context)

        return True

    _columns = {
        'new_name': fields.char('Node', size=10),
        'status': fields.text(_('Resultat')),
        'state': fields.char('State', size=4),

    }

    _defaults = {
        'state': lambda *a: 'init',
        'new_name': _default_new_name
    }

GiscegisOrphand()
