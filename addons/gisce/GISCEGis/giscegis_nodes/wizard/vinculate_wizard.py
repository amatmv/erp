# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _


class GiscegisVinculateNodes(osv.osv_memory):

    _name = 'giscegis.vincular.nodes'

    def vincular(self, cursor, uid, ids, context=None):
        """
        Method that handles the action of vinculate from the view
        :param cursor: Database cursor
        :param uid: User id
        :param ids: Afected ids
        :param context: OpenERP context
        :return: True if backup is done
        """

        if context is None:
            context = {}
        wizard = self.browse(cursor, uid, ids[0], context)
        wizard.write({'status': ''})

        model_nodes = self.pool.get('giscegis.nodes')
        ids = model_nodes.search(cursor, uid, [])
        wizard.write({'status': _('Encuant tasca de vinculacio')})

        while ids:
            model_nodes.vinculate_orphands(cursor, uid, ids[:1000], context)
            ids = ids[1000:]

        return True

    _columns = {
        'status': fields.text(_('Resultat')),
        'state': fields.char('State', size=4)
    }

    _defaults = {
        'state': lambda *a: 'init'
    }
GiscegisVinculateNodes()
