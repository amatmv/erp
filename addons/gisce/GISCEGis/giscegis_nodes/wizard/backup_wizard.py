# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _


class GiscegisBackupNodes(osv.osv_memory):

    _name = 'giscegis.backup.nodes'

    def backup(self, cursor, uid, ids, context=None):
        """
        Method that handles the action of backup from the view
        :param cursor: Database cursor
        :param uid: User id
        :param ids: Afected ids
        :param context: OpenERP context
        :return: True if backup is done
        """

        if context is None:
            context = {}
        wizard = self.browse(cursor, uid, ids[0], context)
        wizard.write({'status': ''})

        model_nodes = self.pool.get('giscegis.nodes')
        wizard.write({'status': _('Movent taula de nodes')})
        ret_val = model_nodes.backup_table(cursor, uid, ids, context)
        if ret_val:
            wizard.write({'status': _('Proces correcte')})
            wizard.write({'state': 'done'})
        else:
            wizard.write({'status': _('El proces ha fallat')})
            wizard.write({'state': 'failed'})
        return ret_val

    _columns = {
        'status': fields.text(_('Resultat')),
        'state': fields.char('State', size=4)
    }

    _defaults = {
        'state': lambda *a: 'init'
    }
GiscegisBackupNodes()
