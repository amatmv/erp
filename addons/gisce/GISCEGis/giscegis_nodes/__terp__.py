# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Nodes",
    "description": """Modul per la gestio dels nodes de la xarxa""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_base",
        "giscegis_base_geom",
        "giscegis_vertex"
    ],
    "init_xml": [],
    "demo_xml":[
        "giscegis_nodes_demo.xml",
        "giscegis_nodes_sequence_demo.xml"
    ],
    "update_xml":[
        "giscegis_nodes_view.xml",
        "ir.model.access.csv",
        "security/ir.model.access.csv",
        "wizard/backup_wizard_view.xml",
        "wizard/orphand_wizard_view.xml",
        "wizard/vinculate_wizard_view.xml",
        "giscegis_nodes_data.xml",
        "giscegis_cataleg_node_data.xml",
        "giscegis_nodes_sequence.xml"
    ],
    "active": False,
    "installable": True
}
