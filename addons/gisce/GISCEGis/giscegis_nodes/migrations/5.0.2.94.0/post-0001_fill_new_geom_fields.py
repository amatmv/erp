# coding=utf-8

import logging
import pooler


def up(cursor, installed_version):

    logger = logging.getLogger('openerp.migration')
    logger.info('Migrating the X & Y values from vertex as a geom '
                'field into Nodes.')

    pool = pooler.get_pool(cursor.dbname)
    node_obj = pool.get("giscegis.nodes")
    node_obj.fill_geom(cursor, 1)


def down(cursor, installed_version):
    pass


migrate = up
