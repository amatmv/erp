# coding=utf-8

import logging
import pooler


def up(cursor, installed_version):

    logger = logging.getLogger('openerp.migration')
    logger.info('Setting UP the Nodes sequence for the field NAME.')

    pool = pooler.get_pool(cursor.dbname)
    nodes_obj = pool.get('giscegis.nodes')
    nodes_obj.update_sequence(cursor, 1)

    logger.info('Nodes NAME sequence set up with the current value')


def down(cursor, installed_version):
    pass


migrate = up
