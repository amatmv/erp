import os
import unittest

from destral import testing
from destral.transaction import Transaction
from destral.patch import PatchNewCursors
import tools
from tools.misc import cache


class GiscegisNodesTest(testing.OOTestCase):

    require_demo_data = True

    def setUp(self):
        super(GiscegisNodesTest, self).setUp()
        self.oorq = os.environ.get('OORQ_ASYNC', 'True')
        os.environ['OORQ_ASYNC'] = 'False'
        self.txn = Transaction().start(self.database)

    def tearDown(self):
        super(GiscegisNodesTest, self).tearDown()
        os.environ['OORQ_ASYNC'] = self.oorq
        self.txn.stop()

    def test_get_keep_nodes_state(self):
        """
        Tests the get_keep_nodes_state function

        :return: None
        """

        uid = self.txn.user
        cursor = self.txn.cursor

        obj_nodes = self.openerp.pool.get('giscegis.nodes')
        obj_res_config = self.openerp.pool.get('res.config')

        self.assertFalse(obj_nodes.get_keep_nodes_state(cursor, uid))
        search_params = [('name', '=', 'keep_nodes')]
        identifier = obj_res_config.search(cursor, uid, search_params)
        obj_res_config.write(cursor, uid, identifier, {'value': '1'})
        tools.cache.clean_caches_for_db(cursor.dbname)
        self.assertTrue(obj_nodes.get_keep_nodes_state(cursor, uid))

    def test_load(self):

        uid = self.txn.user
        cursor = self.txn.cursor
        nodes_obj = self.openerp.pool.get('giscegis.nodes')
        len_nodes = len(nodes_obj.search(cursor, uid, []))
        self.assertEqual(len_nodes, 153)

    def test_create(self):
        """
        Tests the creation of node

        :return: None
        """

        uid = self.txn.user
        cursor = self.txn.cursor

        # Test keeping nodes
        tools.cache.clean_caches_for_db(cursor.dbname)

        obj_res_config = self.openerp.pool.get('res.config')
        search_params = [('name', '=', 'keep_nodes')]
        id_keep = obj_res_config.search(cursor, uid, search_params)
        obj_res_config.write(cursor, uid, id_keep, {'value': '1'})

        nodes_obj = self.openerp.pool.get('giscegis.nodes')
        identificador = nodes_obj.create(cursor, uid, {'name': 'test'})
        fields = ['name', 'codi_autocad_now']
        data = nodes_obj.read(cursor, uid, identificador, fields)

        self.assertEqual(data['name'], 'test*')
        self.assertNotEqual(data['codi_autocad_now'], 'test')

        identificador = nodes_obj.create(cursor, uid, {'name': 'test2*'})
        fields = ['name', 'codi_autocad_now']
        data = nodes_obj.read(cursor, uid, identificador, fields)

        self.assertEqual(data['name'], 'test2*')
        nodes_obj.create(cursor, uid, {'name': 'test2'})

        # Test without keeping nodes
        cache.clean_caches_for_db(cursor.dbname)

        obj_res_config.write(cursor, uid, id_keep, {'value': '0'})

        identificador = nodes_obj.create(cursor, uid, {'name': 'test2'})
        data = nodes_obj.read(cursor, uid, identificador, fields)
        self.assertEqual(data['name'], 'test2')

    def test_vinculate_manualy(self):
        imd = self.openerp.pool.get('ir.model.data')
        fields = ['name', 'codi_autocad_now', 'codi_autocad_old', 'vertex']
        uid = self.txn.user
        cursor = self.txn.cursor
        nodes_obj = self.openerp.pool.get('giscegis.nodes')
        node_id = imd.get_object_reference(
            cursor, uid, 'giscegis_nodes', '1476'
        )[1]
        n_data = nodes_obj.read(cursor, uid, node_id, fields, None)
        n_data['name'] = 'holahola'
        n_data['vertex'] = n_data['vertex'][0]
        nodes_obj.unlink(cursor, uid, node_id, None)

        new_id = nodes_obj.create(cursor, uid, n_data, None)
        nodes_obj.vinculate(
            cursor, uid, [new_id], '123456789', None)
        new_n_data = nodes_obj.read(
            cursor, uid, [new_id], ['name'], None)[0]
        self.assertEqual(new_n_data['name'], '123456789')

    def test_get_orphand_nodes(self):

        uid = self.txn.user
        cursor = self.txn.cursor

        obj_res_config = self.openerp.pool.get('res.config')
        search_params = [('name', '=', 'keep_nodes')]
        id_keep = obj_res_config.search(cursor, uid, search_params)
        obj_res_config.write(cursor, uid, id_keep, {'value': '1'})

        cache.clean_caches_for_db(cursor.dbname)

        nodes_obj = self.openerp.pool.get('giscegis.nodes')
        orph_n_ids = nodes_obj.search(cursor, uid, [], None)

        # Mark all nodes as orphands
        for node_id in orph_n_ids:
            data = nodes_obj.read(cursor, uid, node_id, ['name'], None)
            nodes_obj.raw_write(
                cursor, uid, data['id'],  {'name': data['name']+'*'}, None)

        orph_nodes = nodes_obj.get_orphand_nodes(cursor, uid, None, None)
        self.assertEqual(len(orph_nodes), len(orph_n_ids))
        for onode in orph_nodes:
            datao = nodes_obj.read(cursor, uid, onode, ['name'], None)
            self.assertEqual(datao['name'][-1], '*')

    def test_small_move(self):

        uid = self.txn.user
        cursor = self.txn.cursor

        with PatchNewCursors():

            nodes_obj = self.openerp.pool.get('giscegis.nodes')
            vertex_obj = self.openerp.pool.get('giscegis.vertex')
            orph_n_ids = nodes_obj.search(
                cursor, uid, [('vertex', '!=', False)], None
            )

            sql_delete = '''
            DELETE FROM giscegis_nodes_map
            '''
            cursor.execute(sql_delete)
            sql_move = '''
            INSERT INTO giscegis_nodes_map (name,codi_autocad,x,y)
            SELECT replace(n.name,'*',''), n.codi_autocad_now, v.x ,v.y from giscegis_nodes n
            INNER JOIN  giscegis_vertex v ON v.id=n.vertex;
            '''
            cursor.execute(sql_move)

            node_fields = ['name', 'vertex']
            # Mark all nodes as orphands
            for node_id in orph_n_ids:
                data = nodes_obj.read(cursor, uid, node_id, node_fields, None)
                v = vertex_obj.read(
                    cursor, uid, data['vertex'][0], ['x', 'y'])
                v_data = {
                    'x': v['x']+0.00000001,
                    'y': v['y']+0.00000001
                }
                vertex_obj.write(cursor, uid, data['vertex'][0], v_data)
                nodes_obj.raw_write(
                    cursor, uid, node_id, {'name': data['name']+'*'}, None)
            orph_nodes = nodes_obj.get_orphand_nodes(cursor, uid, None, None)
            self.assertEqual(len(orph_nodes), len(orph_n_ids))
            for onode in orph_nodes:
                datao = nodes_obj.read(cursor, uid, onode, ['name'], None)
                self.assertEqual(datao['name'][-1], '*')
                nodes_obj.vinculate_orphands(cursor, uid, [onode])
                datao = nodes_obj.read(cursor, uid, onode, ['name'], None)
                self.assertNotEqual(datao['name'][-1], '*')

    @unittest.skip('This should be implemented in the future')
    def test_future_orphands(self):
        """
        Crec que seria bo en el futur impelmentar un test per revisar que els
        nodes es marquin coma orfes quan es borra un vertex
        """
        pass
