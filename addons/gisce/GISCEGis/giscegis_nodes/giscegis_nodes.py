# -*- coding: utf-8 -*-
from osv import osv, fields
from giscegis_base_geom.giscegis_session import SessionManaged
from giscegis_base_geom.fields import Point
from psycopg2.extensions import AsIs
import re
import netsvc
from oorq.decorators import job
from tools import config
import tools
import logging
from giscegis_base_geom.wizard.giscegis_shp_loader \
    import GIS_MODELS_TO_UPDATE_GEOM_DATA
from giscegis_base_geom.giscegis_base_geom import DROP_ON_AUTOCAD_DUMP

# Subscribe this model to the list to be updated if there is an AutoCAD dump
GIS_MODELS_TO_UPDATE_GEOM_DATA.append(
    {
        'model': 'giscegis.nodes',
        'functions': ['fill_geom', 'update_sequence']
    },
)
DROP_ON_AUTOCAD_DUMP.append('giscegis_nodes')


def register_nodes_unique_geom_constraint_funct(cursor):
    """
    Register a function to use on the unique geom contraint for the model
    GiscegisNodes. It also add the constraint to the model.
    :param cursor: Database cursor
    :type cursor: Cursorgeom_uniq
    :return None
    :rtype NoneType
    """

    logger = logging.getLogger('openerp.postgis')

    logger.info(
        "Registering 'nodes_unique_geom_constraint_funct' postgresql function"
    )
    cursor.execute("""
        CREATE OR REPLACE FUNCTION nodes_unique_geom_constraint_funct(
            id BIGINT,
            geom GEOMETRY
        )
        RETURNS boolean AS
            $$
                SELECT NOT EXISTS (
                    SELECT 1
                    FROM public.giscegis_nodes AS a
                    WHERE a.id <> $1
                    AND public.ST_Equals(a.geom, $2)
                );
            $$
            LANGUAGE sql
        ;
        CREATE OR REPLACE FUNCTION nodes_unique_geom_constraint_funct(
            id INT,
            geom GEOMETRY
        )
        RETURNS boolean AS
            $$
                SELECT NOT EXISTS (
                    SELECT 1
                    FROM public.giscegis_nodes AS a
                    WHERE a.id <> $1
                    AND public.ST_Equals(a.geom, $2)
                );
            $$
            LANGUAGE sql
        ;
    """)

    cursor.execute("""
        SELECT con.conname
        FROM pg_catalog.pg_constraint con
        INNER JOIN pg_catalog.pg_class rel ON rel.oid = con.conrelid
        INNER JOIN pg_catalog.pg_namespace nsp ON nsp.oid = connamespace
        WHERE conname = 'nodes_unique_geom_constraint';
    """)

    if not cursor.rowcount:
        cursor.execute("""
            ALTER TABLE giscegis_nodes
            ADD CONSTRAINT nodes_unique_geom_constraint
            CHECK (nodes_unique_geom_constraint_funct(id, geom));
        """)


class GiscegisNodes(SessionManaged):
    _name = 'giscegis.nodes'
    _description = 'Nodes'

    def init(self, cursor):
        register_nodes_unique_geom_constraint_funct(cursor)
        return True

    def _get_nextnumber(self, cursor, uid, context=None):
        """
        Returns the next node numbres from the sequence giscegis.nodes
        :param cursor: Database Cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :param context: OpenERP context
        :type context: dic[str,Any]
        :return: Next GiscegisNodes name value
        :rtype: int
        """

        return self.pool.get('ir.sequence').get(
            cursor, uid, 'giscegis.nodes'
        )

    def get_time(self, cr, uid):
        cr.execute("""SELECT to_char(now(),'YYYY-MM-DD HH24:MI:SS')""")
        res = cr.fetchone()
        return res[0]

    @tools.cache()
    def get_keep_nodes_state(self, cursor, uid):
        """
        Method to cache the keep_nodes value using cache

        :return: Boolean
        """

        model_config = self.pool.get('res.config')
        return model_config.get(cursor, uid, 'keep_nodes', '1') == '1'

    def create(self, cursor, uid, vals, context=None):
        """
            Overwrites the normal create beheivour,
            it adds a * after the name field

        :param cursor: Database cursor
        :param uid: User id
        :param vals: Dict with the values
        :param context: OpenERP context
        :return: id"""
        if self.get_keep_nodes_state(cursor, uid):
            vals['name'] = re.sub('\**$', '', str(vals['name'])) + '*'
            ids = super(GiscegisNodes, self).search(
                cursor, uid, [('name', '=', vals['name'])], context=context
            )
            if ids:
                super(GiscegisNodes, self).unlink(
                    cursor, uid, ids, context=context
                )
            res = super(GiscegisNodes, self).create(
                cursor, uid, vals, context=context
            )
        else:
            ids = super(GiscegisNodes, self).search(
                cursor, uid, [('name', '=', vals['name'])], context=context
            )
            if ids:
                res = ids[0]
                super(GiscegisNodes, self).write(
                    cursor, uid, res, vals, context=context
                )
            else:
                res = super(GiscegisNodes, self).create(
                    cursor, uid, vals, context=context
                )
        return res

    def raw_write(self, cursor, uid, ids, vals, context=None):
        """
        Function that allows to write directly

        :param cursor: Database cursor
        :param uid: User id
        :param ids: Wirte element id
        :param vals: Write values
        :param context: OpenERP context
        :return: id
        """
        res = super(GiscegisNodes, self).write(
            cursor, uid, ids, vals, context)
        return res

    def write(self, cursor, uid, ids, vals, context=None):
        """
        Overwrites the normal write beheivour, it

        :param cursor: Database curosr
        :param uid: User id
        :param ids: Afected ids
        :param vals: Dict with the values
        :param context: OpenERP Context
        :return: id
        """

        if self.get_keep_nodes_state(cursor, uid):
            if 'name' in vals:
                vals['codi_autocad_now'] = vals['name']
                vals.pop('name')
            res = super(GiscegisNodes, self).write(
                cursor, uid, ids, vals, context)
        else:
            ids = super(GiscegisNodes, self).search(
                cursor, uid, [('name', '=', vals['name'])], context=context
            )
            if ids:
                n_id = ids[0]
                res = super(GiscegisNodes, self).write(
                    cursor, uid, n_id, vals, context=context
                )
            else:
                res = super(GiscegisNodes, self).create(
                    cursor, uid, vals, context
                )
        return res

    def get_coords_multi(self, cursor, uid, ids, srid=None, srid_db=None,
                         context=None):
        """ Get lat,lon of a list of nodes
        :param cursor: cursor Database cursor
        :param uid: integer User identifier
        :param ids: integer Single id of the node
        :param srid: integer Optional SRID for the output's projection
        :param srid_db: integer Optional SRID of the database to speedup
        :param context: context Context
        :return:
        """
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        res = dict.fromkeys(ids, False)
        if ids:
            if not srid_db:
                conf_obj = self.pool.get('res.config')
                srid_db = int(conf_obj.get(cursor, uid,
                                           'giscegis_srid', '23031'))
            if not srid:
                srid = srid_db

            sql = '''SELECT n.id,
                ST_X(
                  ST_TRANSFORM(
                    ST_SETSRID(ST_MakePoint(v.x, v.y),%(srid_db)s ), %(srid)s))
                    as x,
                ST_Y(
                  ST_TRANSFORM(
                    ST_SETSRID(ST_MakePoint(v.x, v.y),%(srid_db)s ), %(srid)s))
                    as y
                FROM
                  giscegis_vertex v,
                  giscegis_nodes n
                WHERE
                  n.vertex = v.id AND
                  n.id in %(ids)s
            '''
            sql_params = {
                'ids': tuple(ids),
                'srid_db': srid_db,
                'srid': srid
            }

            cursor.execute(sql, sql_params)
            data = cursor.fetchall()
            for row in data:
                res[row[0]] = [row[1], row[2]]
        return res

    def vinculate(self, cursor, uid, ids, name, context=None):
        """
        Manually vinculates new nodes bypassing write

        :param cursor: Database cursor
        :param uid: User identifier
        :param ids: Vinculate nodes ids
        :param name: New name
        :param context: OpenERP Context
        :return: return of super write
        """
        vals = {'name': name}
        res = super(GiscegisNodes, self).write(
            cursor, uid, ids, vals, context)
        return res

    # Be careful this function is overwriten on giscegis_mapguide_compatibility
    def clean_non_vinculated(self, cursor, uid, context=None):
        """
        Deletes the non vinculated nodes

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param context: OpenERP context
        :return: Number of deleted nodes
        :rtype: int
        """

        sql_nodes_ids = """
            SELECT id 
            FROM giscegis_nodes 
        """
        cursor.execute(sql_nodes_ids)
        nodes_ids = cursor.fetchall()
        non_linked_nodes_ids = [ident[0] for ident in nodes_ids]

        sql_models_qgis = """
            SELECT distinct(ir.model) AS model 
            FROM giscegis_cataleg_cataleg AS cataleg
            LEFT JOIN ir_model AS ir ON (cataleg.model=ir.id) 
            WHERE geom_type = 'Point' AND NOT not_topology ;
        """
        cursor.execute(sql_models_qgis)
        models_qgis = cursor.fetchall()

        for model in models_qgis:
            sql_ids_model = """
                SELECT node_id 
                FROM %(model)s
                WHERE node_id IS NOT NULL 
            """
            model_obj = self.pool.get(model[0])
            if model_obj._inherits:
                continue
            cursor.execute(
                sql_ids_model, {"model": AsIs(model[0].replace(".", "_"))}
            )
            model_ids = [i[0] for i in cursor.fetchall()]
            non_linked_nodes_ids = list(
                set(non_linked_nodes_ids) - set(model_ids)
            )

        sql_edges = """
            SELECT start_node
            FROM giscegis_edge
            UNION
            SELECT end_node
            FROM giscegis_edge;
        """
        cursor.execute(sql_edges)
        edge_node_ids = cursor.fetchall()
        edge_node_ids = [ident[0] for ident in edge_node_ids]
        non_linked_nodes_ids = list(
            set(non_linked_nodes_ids) - set(edge_node_ids)
        )
        if non_linked_nodes_ids:
            sql_nbm_delete = """
                DELETE FROM giscegis_node_bloc_model 
                WHERE node IN %(id)s
            """
            cursor.execute(sql_nbm_delete, {"id": tuple(non_linked_nodes_ids)})

            sql_delete = """
                DELETE  FROM giscegis_nodes
                WHERE id IN %(ids)s
            """
            cursor.execute(sql_delete, {"ids": tuple(non_linked_nodes_ids)})

        return len(non_linked_nodes_ids)

    @job(queue='gis', timeout='6000')
    def vinculate_orphands(self, cursor, uid, ids, context=None):
        """
        Vinculates the orphands nodes with the vertext using id
        :param cursor: Database cursor
        :param uid: User id
        :param ids: to  vinculate nodes ids
        :param context: OpenERP context
        :return: Returns the number of vinculated  nodes
        """

        n_obj = self.pool.get('giscegis.nodes')
        n_map_obj = self.pool.get('giscegis.nodes.map')
        v_obj = self.pool.get('giscegis.vertex')
        logger = netsvc.Logger()

        if ids is None:
            search_term = [('name', '=like', '%*')]
            ids = n_obj.search(cursor, uid, search_term, context)
        nodes = n_obj.read(cursor, uid, ids, ['vertex', 'name'], context)
        afected = 0
        pending_nodes = []
        for node in nodes:
            v_data = v_obj.read(cursor, uid, node['vertex'][0], ['x', 'y'])
            v_search = [('x', '=', v_data['x']), ('y', '=', v_data['y'])]
            n_map_id = n_map_obj.search(cursor, uid, v_search, context)
            n_map_data = n_map_obj.read(cursor, uid, n_map_id, ['name'])
            if len(n_map_id) == 1:
                vinc_data = {
                    'name': n_map_data[0]['name']
                }
                super(GiscegisNodes, self).write(
                    cursor, uid, node['id'], vinc_data)
                afected += 1
            elif len(n_map_id) > 1:
                logger.notifyChannel(
                    'giscedata_nodes',
                    netsvc.LOG_WARNING,
                    "Mes d'un vertex amb x={0} y={1}".format(
                        v_data['x'], v_data['y']))
            else:
                pending_nodes.append(node['id'])
        sql_nodes_not_vinculated = """
            SELECT n.id,v.x,v.y FROM giscegis_nodes n
              LEFT JOIN giscegis_vertex v on v.id = n.vertex
            WHERE n.name like '%%*'
            AND n.id in %s
        """
        sql_nearest = """
            SELECT nodes.name
            FROM giscegis_nodes_map nodes
            WHERE (abs(nodes.x-%s)+abs(nodes.y-%s))<0.0001
            ORDER BY (abs(nodes.x-%s)+abs(nodes.y-%s)) ASC
            LIMIT 1;
        """
        cursor.execute(sql_nodes_not_vinculated, (tuple(pending_nodes),))
        pending_nodes = cursor.fetchall()
        for node in pending_nodes:
            cursor.execute(sql_nearest, (node[1], node[2], node[1], node[2]))
            nearest = cursor.fetchall()
            if nearest:
                vinc_data = {'name': nearest[0][0]}
                super(GiscegisNodes, self).write(
                    cursor, uid, node[0], vinc_data)
                afected += 1

        return afected

    def get_orphand_nodes(self, cursor, uid, ids, context):
        """
        Returns the orphand nodes
        :param cursor: Database cursor
        :param uid: User ids
        :param ids: Not used
        :param context: OpenERP database
        :return: list of orphand ids
        """
        n_obj = self.pool.get('giscegis.nodes')
        return n_obj.search(cursor, uid, [('name', '=like', '%*')], context)

    def _old_name(self, cursor, uid, ids, field, arg, context=None):
        """
        Function that gets the old name from giscegis_nodes_map

        :param cursor: Database cursor
        :param uid: User identifier
        :param ids: Asked ids
        :param field: Asked field
        :param arg: Arguments
        :param context: OpenERP context
        :return: Dict{id,value}
        """
        res = {}
        nodes_map_obj = self.pool.get('giscegis.nodes.map')
        nodes_obj = self.pool.get('giscegis.nodes')
        for identificador in ids:
            node_name = nodes_obj.read(
                cursor, uid, identificador, ['name'], context=context)['name']
            node_name = node_name.replace('*', '')
            id_map = nodes_map_obj.search(
                cursor, uid, [('name', '=', node_name)], context=context)
            if id_map:
                data = nodes_map_obj.read(
                    cursor, uid, id_map, ['old_name'], context)[0]
                if data['old_name']:
                    res[identificador] = data['old_name']
        return res

    def _old_model(self, cursor, uid, ids, field, arg, context=None):
        """
        Function that gets the mode from the giscegis_nodes_map

        :param cursor: Database cursor
        :param uid: User identifier
        :param ids: Asked ids
        :param field: Asked field
        :param arg: Arguments
        :param context: OpenERP Context
        :return: Dict{id:value}
        """
        res = {}
        nodes_map_obj = self.pool.get('giscegis.nodes.map')
        nodes_obj = self.pool.get('giscegis.nodes')
        for identificador in ids:
            node_name = nodes_obj.read(
                cursor, uid, identificador, ['name'], context=context)['name']
            node_name = node_name.replace('*', '')
            id_map = nodes_map_obj.search(
                cursor, uid, [('name', '=', node_name)], context=context)
            if id_map:
                data = nodes_map_obj.read(
                    cursor, uid, id_map, ['model'], context)[0]
                if data['model']:
                    res[identificador] = data['model'][1]
        return res

    def get_coords(self, cursor, uid, ids, srid=None, srid_db=None,
                   context=None):
        """ Get the lat,lon of a node
        :param cursor: cursor Database cursor
        :param uid: integer User identifier
        :param ids: integer Single id of the node
        :param srid: integer Optional SRID for the output's projection
        :param srid_db: integer Optional SRID of the database to speedup
        :param context: context Context
        :return:
        """
        if isinstance(ids, (list, tuple)):
            ids = ids[0]

        if not srid_db:
            conf_obj = self.pool.get('res.config')
            srid_db = int(conf_obj.get(cursor, uid, 'giscegis_srid', '23031'))
        if not srid:
            srid = srid_db

        has_vertex = True
        node_data = self.read(cursor, uid, ids, ["vertex"])
        if not node_data.get("vertex", False):
            has_vertex = False

        if has_vertex:
            sql = '''SELECT
                ST_X(
                  ST_TRANSFORM(
                    ST_SETSRID(ST_MakePoint(v.x, v.y),%(srid_db)s ), %(srid)s)),
                ST_Y(
                  ST_TRANSFORM(
                    ST_SETSRID(ST_MakePoint(v.x, v.y),%(srid_db)s ), %(srid)s))
                FROM
                  giscegis_vertex v,
                  giscegis_nodes n
                WHERE
                  n.vertex = v.id AND
                  n.id = %(ids)s
            '''
            sql_params = {
                'ids': ids,
                'srid_db': srid_db,
                'srid': srid
            }
            cursor.execute(sql, sql_params)
            data = cursor.fetchone()
            if data is None:
                return {'x': 0, 'y': 0}
        else:
            sql = """
            SELECT
                ST_X(ST_TRANSFORM(geom, %(srid)s)),
                ST_Y(ST_TRANSFORM(geom, %(srid)s))
            FROM
                giscegis_nodes n
            WHERE
                n.id = %(ids)s;
            """
            sql_params = {
                'ids': ids,
                'srid_db': srid_db,
                'srid': srid
            }
            cursor.execute(sql, sql_params)
            data = cursor.fetchone()
        if data is None:
            return {'x': 0, 'y': 0}
        return {'x': data[0], 'y': data[1]}

    def fill_geom(self, cursor, uid):
        """
        This function fills the field geom with the X & Y values stored on the
        related vertex of the node
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return: None
        :rtype: NoneType
        """
        sql = """
            UPDATE giscegis_nodes SET geom = null;
            UPDATE giscegis_nodes AS node
            SET geom = data.geom
            FROM (
              SELECT n.id AS id,
                     st_setsrid(st_makepoint(v.x, v.y), %(srid)s) AS geom
              FROM giscegis_nodes n
              LEFT JOIN giscegis_vertex v ON n.vertex = v.id
            ) AS data
            WHERE node.id=data.id;
            UPDATE giscegis_nodes SET geom = St_SnapToGrid(
                ST_Translate(geom, 0.0000000001, 0.0000000001), 0.0001
            );
        """

        cursor.execute(sql, {"srid": AsIs(config.get("srid", 25830))})

    def update_sequence(self, cursor, uid):
        """
        This function updates the sequence of the Nodes names to keep it updated
        and coherent with the nodes data
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :return: None
        :rtype: NoneType
        """

        sql = """
            UPDATE ir_sequence AS seq
            SET number_next = data.num
            FROM (
              SELECT coalesce(max(n.name::int),0)+1 AS num,
                     seq.id AS id
              FROM giscegis_nodes n, ir_sequence seq
              WHERE seq.name='Numeració Nodes'
              AND seq.code='giscegis.nodes'
              GROUP BY seq.id
            ) AS data
            WHERE seq.id=data.id
        """

        cursor.execute(sql)

    def _coords(self, cursor, uid, ids, field_name, arg, context):
        """
        Returns the x and y of the node

        :param cursor: Database cursor
        :param uid: User id
        :param ids: Node ids
        :type ids: list
        :param field_name: Field to calculate
        :param arg:
        :param context: OpenERP context
        :type context: dict
        :return: X and y of the Nodes
        :rtype: dict
        """

        if isinstance(ids, int):
            ids = [ids]
        ret = {}
        sql = """
        SELECT id,st_x(geom) as x, st_y(geom)
        FROM giscegis_nodes
        WHERE id IN %s;
        """
        cursor.execute(sql, (tuple(ids),))
        nodes = cursor.fetchall()
        ret = dict.fromkeys(ids)
        for node in nodes:
            ret[node[0]] = {"x": node[1], "y": node[2]}
            ids.remove(node[0])
        for identifier in ids:
            ret[identifier] = {"x": None, "y": None}
        return ret

    _columns = {
        'name': fields.char('Codi Node', size=10, required=True),
        'vertex': fields.many2one('giscegis.vertex', 'Vertex',
                                  ondelete='set null'),
        'codi_autocad_now': fields.char('Codi Autocad actual', size=256),
        'codi_autocad_old': fields.char('Codi Autocad antic', size=256),
        'old_model': fields.function(
            _old_model, string='Model', type='char', size=256, method=True
        ),
        'old_name': fields.function(
            _old_name, string='Nom antic', type='char', size=256, method=True
        ),
        'geom': Point("Geometria", srid=config.get('srid', 25830), select=True),
        'active': fields.boolean('Actiu', select=True),
        'x': fields.function(_coords, method=True, type='float', string='x',
                             multi="coords"),
        'y': fields.function(_coords, method=True, type='float', string='y',
                             multi="coords"),
    }

    _defaults = {
        'name': _get_nextnumber,
        'active': lambda *a: 1
    }
    _sql_constraints = [
        ('name_uniq', 'unique (name)', 'El nombre del nodo debe ser unico'),
    ]

    _order = "name, id"


GiscegisNodes()


class GiscegisNodesMap(osv.osv):
    _name = 'giscegis.nodes.map'

    _columns = {
        'name': fields.char('Id', size=10, required=True),
        'codi_autocad': fields.char('Codi Autocad', size=256),
        'x': fields.float('x', required=True),
        'y': fields.float('y', required=True),
        'model': fields.many2one('ir.model', 'Model'),
        'old_name': fields.char('Name antic', size=256)
    }

    _order = "name, id"


GiscegisNodesMap()
