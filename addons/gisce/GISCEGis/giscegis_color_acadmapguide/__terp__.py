# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Color ACAD Mapguide",
    "description": """
    Module that adds the realation between Autocad colors and Mapguide colors
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "base",
        "giscegis_base"
    ],
    "init_xml":[
        "giscegis_color_acadmapguide_data.xml"
    ],
    "demo_xml": [],
    "update_xml":[
        "giscegis_color_acadmapguide_view.xml",
        "ir.model.access.csv",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
