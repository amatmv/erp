from osv import osv,fields

class giscegis_color_acadmapguide(osv.osv):
    _name = 'giscegis.color.acadmapguide'
    _description = 'Color Acad Mapguide'
    _columns = {
      'name': fields.integer('Sortida',required=True),
      'acad': fields.integer('Color Autocad',required=True),
      'mapguide': fields.integer('Color Mapguide',required=True),
    }

    _defaults = {

    }

    _order = "name, id"
giscegis_color_acadmapguide()
