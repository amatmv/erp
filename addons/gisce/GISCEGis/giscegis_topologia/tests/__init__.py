from destral import testing
from destral.transaction import Transaction


class GiscegisTopology(testing.OOTestCase):

    def test_load(self):

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            top_obj = self.openerp.pool.get('giscegis.topologia')
            nbm_obj = self.openerp.pool.get("giscegis.node.bloc.model")
            edg_obj = self.openerp.pool.get("giscegis.edge")

            nbm_obj.generate(cursor, uid)
            edge_ids = edg_obj.search(cursor, uid, [])

            graph = top_obj.load(cursor, uid)

            self.assertEqual(len(edge_ids), len(graph.edges(data=True)))
            for edge in graph.edges(data=True):
                self.assertEqual(len(edge), 3)
                self.assertEqual(len(edge), 3)
                self.assertTrue("edge_id" in edge[2])
