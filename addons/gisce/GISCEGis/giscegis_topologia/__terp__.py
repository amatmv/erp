# -*- coding: utf-8 -*-
{
    "name": "GISCE GIS Topologia",
    "description": """Topologia des de l'ERP""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends":[
        "giscegis_node_bloc_model",
        "giscegis_edge",
        "giscegis_nodes",
        "giscegis_configdefault_at",
        "giscegis_configdefault_bt",
        "base",
        "giscegis_base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/giscegis_topologia_wizard.xml",
        "ir.model.access.csv",
        "security/ir.model.access.csv",
        "wizard/giscegis_circuits_wizard.xml"
    ],
    "active": False,
    "installable": True
}
