# coding=utf-8
import networkx


class Graph(networkx.Graph):

    def delete_nodes_from(self, nlist, silent=False):
        """Remove nodes in nlist from graph.

        nlist:
        an iterable or iterator containing valid node names.

        Attempting to delete a non-existent node will raise an exception.
        This could mean some nodes got deleted and other valid nodes did
        not.

        """
        for n in nlist:
            try:
                for u in self.adj[n].keys():
                    del self.adj[u][n]
                del self.adj[n]
            except KeyError:
                if silent:
                    continue
                raise networkx.NetworkXError, "node %s not in graph" % (n,)


