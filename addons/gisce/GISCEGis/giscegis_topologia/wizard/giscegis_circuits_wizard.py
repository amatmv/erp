# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _


class GiscegisCircuits(osv.osv_memory):

    _name = 'giscegis.circuits'

    def assign(self, cursor, uid, ids, context=None):
        """
            Function that handles the click on the wizard

        :param cursor: OpenERP Database cursor
        :param uid: User id
        :param ids: Afected ids
        :param context: OpenERP Context
        :return:
        """

        if context is None:
            context = {}
        wizard = self.browse(cursor, uid, ids[0], context)
        wizard.write({'status': ''})

        model_topologia = self.pool.get('giscegis.topologia')
        wizard.write({'status': _('Calculant posicions')})
        result = model_topologia.calculate_posicions(
            cursor, uid, None, context=context)
        wizard.write({'status': _('Assignant posicions')})
        for id_circuit in result.keys():
            data = {str(id_circuit): result[id_circuit]}
            model_topologia.assign_posicions(
                cursor, uid, data, {'circuit': id_circuit}, context=context)

        wizard.write({'state': 'done'})
        wizard.write({'status': _('Circuits assignats')})
    _columns = {
        'status': fields.text(_('Resultat')),
        'state': fields.char('State', size=4)
    }

    _defaults = {
        'state': lambda *a: 'init'
    }
GiscegisCircuits()
