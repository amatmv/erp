# *-* coding: utf-8 *-*

import wizard
import pooler
from networkx import *


def _carregar_topologia(self, cr, uid, data, context={}):
    topo_obj = pooler.get_pool(cr.dbname).get('giscegis.topologia')
    G = topo_obj.load(cr, uid, context)

    G.info()
    s = ''
    s += 'Nodes: %i\n' % len(G.nodes())
    s += 'Arestes: %i\n' % len(G.edges())

    return {'info': s}

_init_arch = """<?xml version="1.0"?>
<form string="Càrrega del graf">
  <label string="Graf carregat correctament!" colspan="4" />
  <separator string="Info:" colspan="4" />
  <field name="info" colspan="4" readonly="1" nolabel="1"/>
</form>
"""

_init_fields = {
    'info': {'string': 'Info', 'type': 'text'}
}


class wizard_carregar_topologia(wizard.interface):
    states = {
        'init': {
            'actions': [_carregar_topologia],
            'result': {
                'type': 'form',
                'fields': _init_fields,
                'arch': _init_arch,
                'state': [('end', 'D\'acord', 'gtk-ok')]
            },
        },
        'end': {
            'actions': [],
            'result': {
                'type': 'state',
                'state': 'end'
            },
        },
    }

wizard_carregar_topologia('giscegis.topologia.carregar')
