# *-* coding: utf-8 *-*
from __future__ import absolute_import
from collections import namedtuple

from osv import osv, fields
import networkx


NodeBlocModel = namedtuple('NodeBlocModel', ['res_id', 'model'])


class GiscegisTopologia(osv.osv):

    _name = 'giscegis.topologia'
    _auto = False

    _columns = {}

    def load(self, cr, uid, context=None):
        """
        Loads the graph using the topology

        :param cr: Database cursor
        :param uid: User id
        :param context: OpenERP context
        :return: Networkx graph
        """

        if context is None:
            context = {}

        nbm_obj = self.pool.get('giscegis.node.bloc.model')
        ids = nbm_obj.search(cr, uid, [])
        g = networkx.Graph()
        #G = networkx.Graph()
        for nmb in nbm_obj.browse(cr, uid, ids, context):
            g.add_node(nmb.node)
        edge_obj = self.pool.get('giscegis.edge')
        objects = edge_obj.search(cr, uid, [])
        for edge in edge_obj.browse(cr, uid, objects, context):
            g.add_edge(
                edge.start_node, edge.end_node, edge_id=edge.id
            )

        return g

    def assign_posicions(self, cursor, uid, positions_data, write_data,
                         context=None):
        """
        Assigns the position into the AT tram and CT

        :param cursor: Cursor de la base de dades
        :param uid: User ids
        :param positions_data: Dict {id name:{'tram':[tram ids],'ct':[ct ids]}}
        :param write_data: Dict with the data to write
        :param context: Contexte d'OpenERP
        :return: True
        """

        tram_obj = self.pool.get('giscedata.at.tram')
        ct_obj = self.pool.get('giscedata.cts')

        for identifier in positions_data:
            tram_ids = positions_data[identifier]['tram']
            ct_ids = positions_data[identifier]['ct']
            tram_obj.write(cursor, uid, tram_ids, write_data)
            ct_obj.write(cursor, uid, ct_ids, write_data)
        return True

    def calculate_posicions(self, cursor, uid, ids_name, context=None):
        """
        Calcula la posicio al tram

        :param cursor: Database cusros
        :param uid: User ids
        :param ids_name: Llistat dels noms dels nodes a calcular
        :param context: Contexte d'OpenERP
        :return: Dict {id posicio:{'tram':[tram ids],'ct':[ct ids]}}
        """
        tram_obj = self.pool.get('giscedata.at.tram')
        posicions_obj = self.pool.get('giscedata.cts.subestacions.posicio')

        posicions_ids = posicions_obj.search(cursor, uid, [])
        posicions = posicions_obj.read(
            cursor, uid, posicions_ids, ['id', 'name'])
        n_id_posicions = {}
        for pos in posicions:
            n_id_posicions[pos['name']] = pos['id']

        trams_ids = tram_obj.search(cursor, uid, [])
        trams = tram_obj.read(cursor, uid, trams_ids, ['id', 'name'])
        n_id_tram = {}
        for tram in trams:
            n_id_tram[tram['name']] = tram['id']

        nodes_obj = self.pool.get('giscegis.configdefault.at')
        nodes_ids = nodes_obj.search(cursor, uid, [])
        open_nodes = nodes_obj.read(cursor, uid, nodes_ids, ['node'])
        open_nodes = [x['node'][0] for x in open_nodes]

        if ids_name:
            names_interruptors = ids_name
        else:
            obj_posicions = self.pool.get('giscedata.cts.subestacions.posicio')
            ids_posicions = obj_posicions.search(cursor, uid, [])
            posicions = obj_posicions.read(
                cursor, uid, ids_posicions, ['name'])
            names_interruptors = [x['name'] for x in posicions]

        nodes = []
        obj_interruptors = self.pool.get('giscegis.blocs.interruptorat')
        int_filter = [('codi', 'in', names_interruptors)]
        ids_interruptors = obj_interruptors.search(cursor, uid, int_filter)
        nodes = nodes + obj_interruptors.read(
            cursor, uid, ids_interruptors, ['node', 'codi'])

        obj_interruptors = self.pool.get('giscegis.blocs.seccionadorunifilar')
        int_filter = [('codi', 'in', names_interruptors)]
        ids_interruptors = obj_interruptors.search(cursor, uid, int_filter)
        nodes = nodes + obj_interruptors.read(
            cursor, uid, ids_interruptors, ['node', 'codi'])

        netsource_bloc_obj = self.pool.get('giscegis.blocs.netsource')
        netsource = netsource_bloc_obj.search(cursor, uid, [])[0]
        simulacio_obj = self.pool.get('giscegis.simulacio.at')

        cts_bloc_obj = self.pool.get('giscegis.blocs.ctat')
        edge_obj = self.pool.get('giscegis.edge')

        res = {}
        topo_obj = self.pool.get('giscegis.topologia')
        graph = topo_obj.load(cursor, uid, context)
        for node in nodes:
            if node['node'][0] not in open_nodes:
                sim_open_nodes = node['node'][0]
                tmp_res = simulacio_obj.get_downstream(
                    cursor, uid, netsource, sim_open_nodes,
                    graph=graph, context=context)
                names_trams = edge_obj.read(
                    cursor, uid, tmp_res['tram'], ['id_linktemplate'])
                names_trams = [str(x['id_linktemplate']) for x in names_trams]
                trams_ids = tram_obj.search(
                    cursor, uid, [('name', 'in', names_trams)])
                tmp_res['ct'] = [x for x in tmp_res['ct']]
                cts_ids = cts_bloc_obj.search(cursor, uid,
                                              [('node', 'in', tmp_res['ct'])])
                cts_ids = cts_bloc_obj.read(cursor, uid, cts_ids, ['ct'])
                cts_ids = [x['ct'][0] for x in cts_ids]
                res[str(n_id_posicions[node['codi']])] = {'tram': trams_ids,
                                                          'ct': cts_ids}

        return res


GiscegisTopologia()


class GiscegisNodes(osv.osv):
    """
    Extends giscegis.noddes to be able to get the model of a node
    """

    _name = 'giscegis.nodes'
    _inherit = 'giscegis.nodes'

    def get_model(self, cr, uid, res_id):
        """
        Returns the model of

        :param cr: Database cursor
        :param uid: User id
        :type uid: int
        :param res_id: Resource id
        :type res_id: list of ids
        :return: Named tuple of red_id and model
        :rtype: namedtuple or bool
        """

        nbm_obj = self.pool.get('giscegis.node.bloc.model')
        model_obj = self.pool.get('ir.model')

        if isinstance(res_id, (list, tuple)):
            res_id = res_id[0]

        ids = nbm_obj.search(cr, uid, [('node', '=', res_id)])
        if ids:
            nbm = nbm_obj.read(cr, uid, ids[0], ['model', 'res_id'])
            model = model_obj.read(cr, uid, nbm['model'][0],  ['model'])
            return NodeBlocModel(nbm['res_id'], model['model'])
        else:
            return False

    def get_model_multi(self, cr, uid, ids):
        """
        Returnst the module given t a list of node ids  

        :param cr: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: Node id
        :type ids: list
        :return:
        :rtype: dict
        """

        nbm_obj = self.pool.get('giscegis.node.bloc.model')
        model_obj = self.pool.get('ir.model')

        cache = {}
        res = dict.fromkeys(ids, False)
        nbm_ids = nbm_obj.search(cr, uid, [('node', 'in', ids)])
        for nbm in nbm_obj.read(cr, uid, nbm_ids, ['model', 'res_id', 'node']):
            model_id = nbm['model'][0]
            if model_id in cache:
                model = cache[model_id]
            else:
                model = model_obj.read(cr, uid, model_id, ['model'])['model']
                cache[model_id] = model

            res[nbm['node'][0]] = NodeBlocModel(nbm['res_id'], model)
        return res

    def get_codi(self, cr, uid, ident):
        nbm = self.get_model(cr, uid, ident)
        if nbm and nbm.res_id:
            model = self.pool.get(nbm.model)
            obj = model.read(cr, uid, nbm.res_id, ['codi', 'name'])
            fields = model.fields_get(cr, uid)
            if 'codi' in fields:
                return obj['codi']
            elif 'name' in fields:
                return obj['name']
            else:
                return False
        else:
            return False

    def get_codi_multi(self, cr, uid, ids):
        """
        Returns the codi of the nodes

        :param cr: OpenERP cursor
        :param uid: User id
        :param ids: list of ids
        :return: Dict id, codi
        """

        grouped = {}
        ids_map = {}
        res = dict.fromkeys(ids, False)

        models = self.get_model_multi(cr, uid, ids)
        for n_id, nbm in models.items():
            if nbm and nbm.res_id:
                if nbm.model not in grouped:
                    grouped[nbm.model] = []
                    ids_map[nbm.model] = []
                grouped[nbm.model].append(nbm.res_id)
                ids_map[nbm.model].append(n_id)

        for model in grouped:
            t = dict()
            for res_ident, ident in zip(grouped[model], ids_map[model]):
                t[res_ident] = ident
            m_obj = self.pool.get(model)
            resp = m_obj.read(cr, uid, grouped[model], ['codi', "name"])
            for element in resp:
                if 'codi' in element:
                    res[t[element['id']]] = element['codi']
                elif "name" in element:
                    res[t[element['id']]] = element['name']
        return res

    def get_bloc_multi(self, cr, uid, ids, fields=[]):
        models = self.get_model_multi(cr, uid, ids)
        model_ids = {}
        res = []
        for id_element, model in models.items():
            if model and model.res_id:
                if model.model in model_ids:
                    model_ids[model.model].append(model.res_id)
                else:
                    model_ids[model.model] = [model.res_id]
        for model in model_ids.keys():
            m_obj = self.pool.get(model)
            res += m_obj.read(cr, uid, model_ids[model], fields)
        return res

    def get_bloc_read(self, cr, uid, id, fields):
        nbm = self.get_model(cr, uid, id)
        if nbm and nbm.res_id:
            model = self.pool.get(nbm.model)
            return model.read(cr, uid, nbm.res_id, fields)
        else:
            return None

    def get_bloc(self, cr, uid, id):
        nbm = self.get_model(cr, uid, id)
        if nbm and nbm.res_id:
            return self.pool.get(nbm.model).browse(cr, uid, nbm.res_id)
        else:
            return None


GiscegisNodes()


class GiscegisConfigdefaultBt(osv.osv):
    """
    Modle to store the default BT network state
    """
    _name = 'giscegis.configdefault.bt'
    _inherit = 'giscegis.configdefault.bt'

    def _blockname(self, cr, uid, ids, field_name, args, context=None):

        if context is None:
            context = {}
        res = {}
        for cbt in self.browse(cr, uid, ids, context):
            if cbt.node:
                bloc = cbt.node.get_bloc()
                if bloc and bloc.blockname:
                    res[cbt.id] = bloc.blockname.name
                else:
                    res[cbt.id] = ''
            else:
                res[cbt.id] = ''

        return res

    _columns = {
        'blockname': fields.function(_blockname, type='char',
                                     size=60, method=True, string='blockname'),
    }


GiscegisConfigdefaultBt()


class GiscegisConfigdefaultAt(osv.osv):
    """
    Model to store the default AT network
    """

    _name = 'giscegis.configdefault.at'
    _inherit = 'giscegis.configdefault.at'

    def _blockname(self, cr, uid, ids, field_name, args, context=None):

        if context is None:
            context = {}

        res = {}
        for cbt in self.browse(cr, uid, ids, context):
            if cbt.node:
                bloc = cbt.node.get_bloc()
                if bloc and bloc.blockname:
                    res[cbt.id] = bloc.blockname.name
                else:
                    res[cbt.id] = ''
            else:
                res[cbt.id] = ''

        return res

    _columns = {
        'blockname': fields.function(_blockname, type='char', size=60,
                                     method=True, string='blockname'),
    }


GiscegisConfigdefaultAt()
