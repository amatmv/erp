{
    "name": "GISCE GIS Color Linies AT",
    "description": """
    Module that adds the colors to the at lines
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE GIS",
    "depends": [
        "base",
        "giscegis_base",
        "giscegis_base_geom",
        "giscegis_lat_geom",
        "giscedata_tensions"
    ],
    "init_xml": [
    ],
    "demo_xml": [],
    "update_xml": [
        "giscegis_colors_at_view.xml",
        "security/ir.model.access.csv",
    ],
    "active": False,
    "installable": True
}