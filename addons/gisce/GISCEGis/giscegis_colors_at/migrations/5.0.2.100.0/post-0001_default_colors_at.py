# coding=utf-8
from __future__ import unicode_literals
import logging
import pooler
import re


def up(cursor, installed_version):
    _LAYER_AT = "at"
    tension = pooler.get_pool(cursor.dbname).get('giscedata.tensions.tensio')
    color_at = pooler.get_pool(cursor.dbname).get('giscegis.colors.at')
    l_ten = tension.search(cursor, 1, [('tipus', '=', 'AT')])

    logger = logging.getLogger('openerp.migration')

    logger.info('Cargar estilos tensiones normalizadas!')

    for x in l_ten:
        if (int(tension.read(cursor, 1, x)['name']) == 220000):
            color = '9152a5'
        elif (int(tension.read(cursor, 1, x)['name']) == 110000):
            color = '5c37ce'
        elif ((int(tension.read(cursor, 1, x)['name']) >= 40000) and ((int(tension.read(cursor, 1, x)['name']) <= 45000))):
            color = 'e24646'
        elif (int(tension.read(cursor, 1, x)['name']) == 25000):
            color = 'ffbf00'
        elif ((int(tension.read(cursor, 1, x)['name']) >= 20000) and ((int(tension.read(cursor, 1, x)['name']) <= 21500))):
            color = '0134ff'
        elif ((int(tension.read(cursor, 1, x)['name']) >= 30000) and ((int(tension.read(cursor, 1, x)['name']) <= 35000))):
            color = 'ffa73b'
        elif ((int(tension.read(cursor, 1, x)['name']) >= 10000) and ((int(tension.read(cursor, 1, x)['name']) <= 17500))):
            color = 'd42186'
        elif ((int(tension.read(cursor, 1, x)['name']) == 3000) or ((int(tension.read(cursor, 1, x)['name']) == 5000))):
            color = '00a500'
        elif (int(tension.read(cursor, 1, x)['name']) == 3150):
            color = '9152a5'
        elif (int(tension.read(cursor, 1, x)['name']) == 6000):
            color = '910096'
        else:
            color = '000000'

        color_at.create(cursor, 1, {
            'color': color,
            'tensio_id': x,
            'tipus': 'A'
        })

        color_at.create(cursor, 1, {
            'color': color,
            'tensio_id': x,
            'tipus': 'S'
        })

    color_at.create(cursor, 1, {
        'color': 'b000ff',
        'tipus': 'E'
    })


def down(cursor, installed_version):
    pass


migrate = up
