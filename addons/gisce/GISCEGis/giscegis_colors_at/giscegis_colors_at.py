# *-* coding: utf-8 *-*

from osv import osv, fields
from tools.translate import _
from tools import config


class GiscegisColorsAt(osv.osv):
    _LAYER_AT = "at"
    _URL_HEX_CODE_COLOR_PICKER = "https://html-color-codes.info/"
    _tipus_linia_at = [
        ('A', 'Aeri'),
        ('S', 'Subterrani'),
        ('E', 'Embarrat'),
    ]

    def _ff_name(self, cursor, uid, ids, field_name, arg, context=None):
        res = dict.fromkeys(ids, False)

        q = self.q(cursor, uid)
        values = q.read(['id', 'tensio_id.name', 'tipus']).where([('id', 'in', ids)])
        for value in values:
            res[value['id']] = '{} - {}'.format(value['tipus'], value['tensio_id.name'])
        return res

    _name = "giscegis.colors.at"
    _description = _("Colors de les línies de AT")

    def onchange_tipus(self, cursor, uid, ids, tipus, context=None):
        if context is None:
            context = {}
        res = {'value': {}}
        if tipus == 'E':
            res['value']['tensio_id'] = False
        return res

    _columns = {
        "name": fields.function(
            _ff_name,
            method=True,
            type='char',
            size=50,
            string='Nom AT - Tensió'),
        "color": fields.char(_("Color AT(HEX)"), required=True, size=50, help=_("Valor hexadecimal del color")),
        "tipus": fields.char('Tipus línia AT', selection=_tipus_linia_at, widget='selection', size=20, required=True),
        "tensio_id": fields.many2one(
            "giscedata.tensions.tensio",
            string="Tensió normalitzada",
            widget="selection",
            select=1,
            domain=[("tipus", "=", "AT")],
            ),
    }

    _sql_constraints = [(
        "tipus_tensio_unique", "unique(tipus,tensio_id)", _("La tensió normalitzada i el tipus han de ser únics.")
    )]

    def check_hashtag(self, sortida):
        """
        Check if hexadecimal color value has #
        :param sortida: dictionary with color and hex value
        :return dict
        :return: sortida
        :rtype dict
        """
        color = sortida.get('color', '')

        if color:
            if not color.startswith('#'):
                sortida['color'] = '#{}'.format(color)
        return sortida

    def create(self, cr, uid, vals, context=None):
        """
        Overwrite create method, now adds extra feature like:
         removes tms cache for at layer
         refresh vm giscegis_cups_escomeses_tensio_geom
        :param cr: db cursor
        :type cursor
        :param uid: user id
        :type int
        :param vals: {'color':'hex_value'}
        :type dict
        :param context:
        :return: result of create method
        :rtype bool
        """
        sortida = self.check_hashtag(vals)
        ret = super(GiscegisColorsAt, self).create(cr, uid, sortida, context)
        # Removes tms cache layer AT
        self.pool.get("giscegis.base.geom").remove_tms_cache(cr, uid, self._LAYER_AT)
        # Refresh VM giscegis_lat_geom
        self.pool.get("giscegis.lat.geom").recreate(cr, uid)
        return ret

    def unlink(self, cr, uid, ids, context=None):
        """
        Overwrite unlink method, now adds extra feature like:
         removes tms cache for at layer
         refresh vm giscegis_cups_escomeses_tensio_geom
        :param cr: db cursor
        :type cursor
        :param uid: user id
        :type int
        :param ids:
        :param context:
        :return: result of unlink method
        :rtype bool
        """
        ret = super(GiscegisColorsAt, self).unlink(cr, uid, ids)
        # Removes tms cache layer AT
        self.pool.get("giscegis.base.geom").remove_tms_cache(cr, uid, self._LAYER_AT)
        # Refresh VM giscegis.lat.geom
        self.pool.get("giscegis.lat.geom").recreate(cr, uid)
        return ret

    def write(self, cr, uid, ids, values, context=None):
        """
        Overwrite writemethod, now adds extra feature like:
         removes tms cache for at layer
         refresh vm giscegis_cups_escomeses_tensio_geom
        :param cr: db cursor
        :type cr: cursor
        :param uid: user id
        :type int
        :param ids: sortida value
        :type list
        :param values: {'color':'hex_value'}
        :type dict
        :param context:
        :return: result of write method
        :rtype bool
        """
        sortida = self.check_hashtag(values)
        ret = super(GiscegisColorsAt, self).write(cr, uid, ids, sortida)
        # Removes tms cache layer BT
        self.pool.get("giscegis.base.geom").remove_tms_cache(cr, uid, self._LAYER_AT)
        # Refresh VM giscegis.lat.geom
        self.pool.get("giscegis.lat.geom").recreate(cr, uid)
        return ret


GiscegisColorsAt()


class GiscegisLatGeomColors(osv.osv):
    _inherit = 'giscegis.lat.geom'

    def init(self, cursor):

        cursor.execute("""
            SELECT relname
            FROM pg_class
            WHERE relkind = 'S' AND relname = 'giscegis_lat_id_seq'
        """)
        res = cursor.fetchall()
        srid = config.get('srid', 25830)
        if not res:
            # Creacio de la sequencia si no existeix
            cursor.execute("""
                CREATE SEQUENCE public.giscegis_lat_id_seq
                    INCREMENT 1
                    MINVALUE 1
                    MAXVALUE 9223372036854775807
                    START 1
                    CACHE 1;
            """)

        sql_exists = """
            SELECT EXISTS (
                SELECT 1
                FROM   information_schema.tables
                WHERE  table_schema = 'public'
                AND    table_name = 'giscegis_lat'
            );
        """
        cursor.execute(sql_exists)
        res_exists = cursor.fetchall()[0][0]
        # Creacio de la taula giscegis_lat
        if not res_exists:
            sql_giscegis_lat = """
                CREATE TABLE IF NOT EXISTS  public.giscegis_lat (
                    gid integer NOT NULL DEFAULT
                    nextval('giscegis_lat_id_seq'::regclass),
                    id double precision,
                    CONSTRAINT giscegis_lat_pkey PRIMARY KEY (gid));

                SELECT AddGeometryColumn(
                    'public','giscegis_lat', 'geom', %(srid)s, 
                    'MULTILINESTRING',2, true
                );
            """
            cursor.execute(sql_giscegis_lat, {'srid': srid})

        cursor.execute("""
            DROP MATERIALIZED VIEW IF EXISTS giscegis_lat_geom;
    
            CREATE MATERIALIZED VIEW giscegis_lat_geom AS
                SELECT 
                    linia.id,
                    tram.id::int AS name,
                    tram.circuits AS circuits, 
                    shape.geom AS geom,
                    ST_Simplify(shape.geom, 0.5) AS simple_geom,
                    COALESCE(linia.name, 'S/Linia') AS texte,
                    CASE
                        WHEN tipus_cable.codi = 'D' OR tipus_cable.codi = 'T'
                            THEN 'A'
                        WHEN tipus_cable.codi = 'S'
                            THEN 'S'
                        WHEN tipus_cable.codi = 'E' 
                            THEN 'E'
                        ELSE '-'
                    END AS theme,
                    CASE	
                        WHEN tipus_cable.codi = 'D' OR tipus_cable.codi = 'T'
                            THEN (
                                SELECT color FROM giscegis_colors_at WHERE tensio_id = tensio.id AND tipus = 'A' LIMIT 1
                            )
                        WHEN tipus_cable.codi = 'S'
                            THEN (
                                SELECT color FROM giscegis_colors_at WHERE tensio_id = tensio.id AND tipus = 'S' LIMIT 1
                            )
                        WHEN tipus_cable.codi = 'E' 
                            THEN (
                                SELECT color FROM giscegis_colors_at WHERE tipus = 'E' LIMIT 1
                            )
                        ELSE '-' || linia.tensio::text
                    END AS color,
                    tram.cini AS cini,
                    now() AS materialize_date
                FROM giscegis_lat AS shape
                    LEFT JOIN giscedata_at_tram AS tram ON shape.id = tram.name
                    LEFT JOIN giscedata_at_linia linia ON 
                        tram.linia = linia.id AND linia.name::text <> '1'::text
                    LEFT JOIN giscedata_at_cables cable ON tram.cable = cable.id
                    LEFT JOIN giscedata_at_tipuscable AS tipus_cable
                        ON cable.tipus = tipus_cable.id
                    LEFT JOIN giscedata_tensions_tensio tensio ON tensio.name = linia.tensio::text
                    LEFT JOIN giscegis_colors_at cat ON cat.tensio_id = tensio.id
                    WHERE tram.active
                ;
        """)

        # Creacio d'indexs
        cursor.execute("""
            DROP INDEX IF EXISTS giscegis_lat_geom_idx;
            CREATE INDEX giscegis_lat_geos_geom 
            ON giscegis_lat_geom USING gist (geom);
        """)

        cursor.execute("""
            DROP INDEX IF EXISTS giscegis_lat_geom_idx;
            CREATE  INDEX giscegis_lat_geom_idx
            ON public.giscegis_lat USING gist(geom);
        """)

        return True


GiscegisLatGeomColors()
