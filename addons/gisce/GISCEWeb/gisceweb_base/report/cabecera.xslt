<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://localhost/elegibilidad">
  <xsl:template match="cabecera">
    <Cabecera>
      <CodigoREEEmpresaEmisora><xsl:value-of select="reeemisora"/></CodigoREEEmpresaEmisora>
      <CodigoREEEmpresaDestino><xsl:value-of select="reedestino"/></CodigoREEEmpresaDestino>
      <CodigoDelProceso><xsl:value-of select="codigoproceso"/></CodigoDelProceso>
      <CodigoDePaso><xsl:value-of select="format-number(codigopaso, '00')"/></CodigoDePaso> 
      <CodigoDeSolicitud><xsl:value-of select="format-number(codigosolicitud, '000000000000')"/></CodigoDeSolicitud>
      <SecuencialDeSolicitud><xsl:value-of select="format-number(secuencialsolicitud, '00')"/></SecuencialDeSolicitud>
      <!-- len(codigo) ha de ser = 22 ... -->
      <xsl:choose>
        <xsl:when test="string-length(codigo)=20">
          <Codigo><xsl:value-of select="concat(codigo, '  ')"/></Codigo>
        </xsl:when>
        <xsl:otherwise>
          <Codigo><xsl:value-of select="codigo"/></Codigo>
        </xsl:otherwise>
      </xsl:choose>
      <FechaSolicitud><xsl:value-of select="translate(fechasolicitud,' ','T')"/></FechaSolicitud>
      <Version><xsl:value-of select="format-number(version, '00')"/></Version> 
    </Cabecera>
  </xsl:template>
</xsl:stylesheet>

