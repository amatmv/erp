<?xml version="1.0" encoding="ISO-8859-1" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://localhost/elegibilidad">
  <xsl:template match="medidas">
    <Aparato>
      <Tipo><xsl:value-of select="aparato/tipo"/></Tipo>
      <Marca><xsl:value-of select="aparato/marca"/></Marca>
      <NumeroSerie><xsl:value-of select="substring(aparato/numeroserie,0,11)" /></NumeroSerie>
      <CodigoDH><xsl:value-of select="aparato/codigodh" /></CodigoDH>
      <xsl:apply-templates select="aparato/lecturas" />
    </Aparato>
  </xsl:template>
  <xsl:template match="lecturas">
    <Integrador>
      <Magnitud><xsl:value-of select="magnitud"/></Magnitud>
      <CodigoPeriodo><xsl:value-of select="codigoperiodo" /></CodigoPeriodo>
      <ConstanteMultiplicadora>1</ConstanteMultiplicadora>
      <NumeroRuedasEnteras>9</NumeroRuedasEnteras>
      <NumeroRuedasDecimales>00</NumeroRuedasDecimales>
      <ConsumoCalculado><xsl:value-of select="consumo" /></ConsumoCalculado>
      <LecturaDesde>
        <FechaHora><xsl:value-of select="translate(fechaanterior,' ','T')" /></FechaHora>
        <Procedencia>30</Procedencia>
        <Lectura><xsl:value-of select="lecturaanterior" /></Lectura>
      </LecturaDesde>
      <LecturaHasta>
        <FechaHora><xsl:value-of select="translate(fechaactual,' ','T')" /></FechaHora>
        <Procedencia>30</Procedencia>
        <Lectura><xsl:value-of select="lecturaactual" /></Lectura>
      </LecturaHasta>
    </Integrador>
  </xsl:template>
</xsl:stylesheet>

