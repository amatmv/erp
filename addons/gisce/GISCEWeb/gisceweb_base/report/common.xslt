<?xml version="1.0" encoding="ISO-8859-1" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://localhost/elegibilidad">
  <!-- afegeix els espais que toqui si el cups és de 20 digits -->
  <xsl:template name="get-cups">
    <xsl:param name="c" />
    <xsl:choose>
      <xsl:when test="string-length($c)=20">
        <xsl:value-of select="concat($c, '  ')"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$c"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>

