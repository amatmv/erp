# -*- coding: utf-8 -*-
{
    "name": "GISCEWeb Base",
    "description": """Codi base per altres mòduls GISCEWeb""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEWeb",
    "depends":[
        "base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "gisceweb_base_view.xml",
        "gisceweb_base_security.xml",
        "gisceweb_base_data.xml"
    ],
    "active": False,
    "installable": True
}
