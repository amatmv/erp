# -*- coding: utf-8 -*-

import wizard

def _print(self, cr, uid, data, context={}):
  return {'ids': [data['id']]}

class gisceweb_amrps_print(wizard.interface):
  states = {
    'init': {
      'actions': [], 
      'result': {'type': 'state', 'state': 'print'}
    },
    'print' : {
       'actions': [_print],
       'result': {'type': 'print', 'report': 'gisceweb.amrps.resposta', 'get_id_from_action': True, 'state': 'end' }
    }
  }


gisceweb_amrps_print('gisceweb.amrps.print_wizard')

