# -*- coding: utf-8 -*-
{
    "name": "GISCEWeb AMRPS",
    "description": """GISCEWeb acceso y modificación del registro de PS""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEWeb",
    "depends":[
        "base",
        "gisceweb_cups",
        "gisceweb_base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "gisceweb_amrps_wizard.xml",
        "gisceweb_amrps_view.xml",
        "gisceweb_amrps_report.xml",
        "gisceweb_amrps_security.xml",
        "gisceweb_amrps_workflow.xml"
    ],
    "active": False,
    "installable": True
}
