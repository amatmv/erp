# -*- coding: utf-8 -*-

from osv import osv, fields
import time

"""
GISCEWeb: acceso y mantenimiento del registro de puntos de suministro
=======================================================================

Aquest mòdul proveeix la funcionalitat de consulta del registre de PS 
via el Canal Web.


"""

_refus_code = [
('1','Punto de Suministro Inexistente'),
('2','Inexistencia de Contrato Regulado previo en vigor'),
('3','CIF/NIF de las solicitud No coincide con el CIF/NIF del Contrato en vigor'),
('4','CUPS No coincide con el CUPS del Contrato Regulado previo'),
('5','Nº de Contrato Regulado previo No coincide con el aportado'),
('6','Existencia de solicitud ATR previa en curso'),
('7','Tipo de Contrato sin informar (normal, eventual, temporada, socorro o adicional)'),
('8','Duración y/o Tipo de renovación del Contrato sin informar o no válida'),
('9','Existencia de sentencia judicial relativa a la deuda del cliente'),
('10','No ha trascurrido un año en Tarifa (salvo nuevos suministros)'),
('11','Comercializadora incorrecta'),
('12','Contrato regulado o de ATR en suspensión de suministro'),
('13','Potencias No normalizadas (Tarifas 2.0.A y 2.0.N.A)'),
('14','Potencia solicitada es superior a la Potencia máxima autorizada (BIE)'),
('15','Secuencia de Potencias incorrectas (Pdía>Pnoche, Pi>Pi+1)'),
('16','Tensión solicitada no es acorde con la Tensión existente'),
('17','Tarifa solicitada no es acorde con la Tensión y la Potencia existente'),
('18','Equipo nuevo propiedad del Cliente no cumple los criterios de Contratación ATR'),
('19','Cliente no aporta soporte documental acreditativo de firma de Contrato de Energía'),
('20','Instalación no disponible al no estar finalizada la acometida en una salida directa a Mercado Libre'),
('21','Existencia de Contrato previo en vigor (sea ATR o tarifa integral) en una salida directa a Mercado Libre'),
('22','Potencia solicitada es superior a los derechos de extensión (sólo en AT), en BT se cobran y no se abre expediente'),
('23','CNAE no informado en Altas nuevas'),
('24','Documentación incompleta (cédula de habitabilidad, licencia 1ª ocupación, etc..) en salidas directas a ML'),
('25','Cliente ausente de manera reiterada en más de dos visitas'),
('26','Acceso imposibilitado más de dos veces por causas ajenas a la Distribuidora'),
('27','Las deficiencias de la instalación no son subsanadas tras un plazo de un mes'),
('28','Rechazo por anormalidad o fraude contrastado'),
('29','No se dispone del equipo propiedad del cliente 15 días después de la primera petición'),
('99','Otros'),
]

class gisceweb_amrps(osv.osv):
  _name = "gisceweb.amrps"
  _description = "GISCEWeb: acceso y mantenimiento PS"
  _estats = [('draft','Borrador'), ('pendent_validacio','Pendiente de validación'), ('validada', 'Validada'), ('refusada','Rechazada'), 
    ('registrada','Registrada'), ('enviament','Envío de datos'),('facturacio','Facturación accesos')]

  _columns = {
    'ref_sollicitud': fields.integer('Ref. Solicitud'),
    'secuencia': fields.integer('Secuencial de solicitud'),
    'cups_sollicitat': fields.char('CUPS Solicitado', size=25, required=True), # és el 'codi' a la capçalera del XML
    'codi_contracte': fields.char('Código de contrato', size=32, required=True),
    'cups': fields.many2one('gisceweb.cups','CUPS'),
    'data_entrada': fields.datetime('Fecha entrada', readonly=True),
    'emisor': fields.many2one('res.company', 'Emisor', readonly=True),
    'receptor': fields.many2one('res.company', 'Receptor', readonly=True),
    'codi_refus': fields.selection(_refus_code, 'Código de rechazo'),
    'state': fields.selection(_estats, 'Estado', readonly=True),
  }

  _defaults = {
    'state': lambda *a: 'draft',
    'data_entrada': lambda *a: time.strftime("%Y-%m-%d %H:%M:%S"),
    'emisor': lambda self,cr,uid,c: self.pool.get('res.users').browse(cr, uid, uid, c).company_id.id,
    'receptor': lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, self.pool.get('res.users').search(cr, uid, [('login','=','admin')])[0], c).company_id.id,
  }
  _order = "id"

  def unlink(self, cr, uid, ids, context={}):
    amrpss = self.pool.get('gisceweb.amrps').browse(cr, uid, ids)
    for amrps in amrpss:
      if amrps.state == 'draft':
        super(osv.osv, self).unlink(cr, uid, [amrps.id], context)
      else:
        raise osv.except_osv('Error', 'Sólo se permite borrar solicitudes aún no enviadas.')
    return True

  def wkf_draft(self, cr, uid, ids):
    self.write(cr, uid, ids, { 'state' : 'draft' })
    return True

  def wkf_validada(self, cr, uid, ids):
    self.write(cr, uid, ids, {'state':'validada'})
    return True

  def wkf_pendent_validacio(self, cr, uid, ids):
    self.write(cr, uid, ids, {'state':'pendent_validacio'})
    amrpss = self.pool.get('gisceweb.amrps').browse(cr, uid, ids)
    cups_found = False
    for amrps in amrpss:
      cups = self.pool.get('gisceweb.cups').search(cr, uid, [('cups','=',amrps.cups_sollicitat)])
      if cups:
        cups_found = True
        self.pool.get('gisceweb.amrps').write(cr, uid, [amrps.id], {'cups': cups[0]})
    if not cups_found:
      self.write(cr, uid, ids, {'state':'refusada'})
      # faltaria generar comunicado de rechazo
    return True

  def wkf_refusada(self, cr, uid, ids):
    self.write(cr, uid, ids, {'state':'refusada'})
    return True

  def wkf_registrada(self, cr, uid, ids):
    self.write(cr, uid, ids, {'state':'registrada'})
    return True

  def wkf_enviament(self, cr, uid, ids):
    self.write(cr, uid, ids, {'state':'enviament'})
    amrpss = self.pool.get('gisceweb.amrps').browse(cr, uid, ids)
    for amrps in amrpss:
      cups = amrps.cups
    print(cups)
    return True

  def wkf_facturacio(self, cr, uid, ids):
    self.write(cr, uid, ids, {'state':'facturacio'})
    return True

  def busca_sollicituds(self, cr, uid, user_id=3):
    import tools
    user = self.pool.get('res.users').browse(cr, uid, user_id)
    cr.execute("select id from gisceweb_amrps where state = 'validada' and data_entrada < now() - interval '3 day'")
    for s in cr.dictfetchall():
      print("La sol.licitud %s està fora de plaç." % (s['id'],))
      if user.address_id.email:
        email_from = user.address_id.email
        emails_to = []
        emails_to.append('%s <%s>' % (user.name, user.address_id.email))
        subject = '[GISCE-ERP] Sol.licitud d\'informació de CUPS que caduca avui'
        mail_body = str(s['id'])
        tools.email_send(email_from, emails_to, subject, mail_body)
        print '  => Enviant email a %s' % (user.address_id.email)

gisceweb_amrps()

class gisceweb_amrps_respostes(osv.osv):
  _name = "gisceweb.amrps.respostes"
  _description = "GISCEWeb AMRPS Respostes"
  _columns = {
    'sollicitud': fields.many2one('gisceweb.amrps', 'Sol·licitud associada'),
  }
