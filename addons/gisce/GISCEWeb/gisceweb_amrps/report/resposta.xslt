<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://localhost/elegibilidad">

  <xsl:import href="../../gisceweb_base/report/cabecera.xslt"/>

  <xsl:template match="/amrpss">
    <xsl:apply-templates select="amrps" />
  </xsl:template>

  <xsl:template match="amrps">
    <EnvioInformacionAlRegistroDePuntosDeSuministro>
      <xsl:attribute name="AgenteSolicitante"><xsl:value-of select="agentesolicitante"/></xsl:attribute>
      <xsl:apply-templates select="cabecera"/>
      <xsl:apply-templates select="item" />
    </EnvioInformacionAlRegistroDePuntosDeSuministro>
  </xsl:template>

  <xsl:template match="item">
    <EnvioInformacionAlRegistroDePuntosDeSuministro>
      <xsl:apply-templates select="contrato" />
      <xsl:apply-templates select="localizacionps" />
      <xsl:apply-templates select="derechosreconocidos" />
      <xsl:apply-templates select="caracteristicas" />
      <xsl:apply-templates select="historia" />
      <xsl:apply-templates select="consumo" />
      <xsl:apply-templates select="equipo"/>
    </EnvioInformacionAlRegistroDePuntosDeSuministro>
  </xsl:template>

  <xsl:template match="contrato">
    <Contrato>
      <CondicionesContractuales>
        <TarifaATR><xsl:value-of select="tarifaatr"/></TarifaATR>
        <xsl:choose>
          <xsl:when test="string-length(tipocontrato)=2">
            <TipoContrato><xsl:value-of select="tipocontrato"/></TipoContrato>
          </xsl:when>
        </xsl:choose>
        <xsl:apply-templates select="potenciascontratadas" />
        <xsl:choose>
          <xsl:when test="string-length(modofacturacionpotencia)=1">
            <ModoFacturacionPotencia><xsl:value-of select="modofacturacionpotencia"/></ModoFacturacionPotencia>
          </xsl:when>
        </xsl:choose>
      </CondicionesContractuales>
    </Contrato>
  </xsl:template>

  <xsl:template match="potenciascontratadas">
    <PotenciasContratadas>
      <xsl:apply-templates select="potencia"/>
    </PotenciasContratadas>
  </xsl:template>

  <xsl:template match="potencia">
    <Potencia>
      <xsl:attribute name="Periodo"><xsl:value-of select="periodo"/></xsl:attribute>
      <xsl:value-of select="valor"/>
    </Potencia>
  </xsl:template>

  <xsl:template match="localizacionps">
    <LocalizacionPS>
      <Pais><xsl:value-of select="paiscups"/></Pais>
      <Provincia><xsl:value-of select="provinciacups"/></Provincia>
      <Municipio><xsl:value-of select="format-number(municipiocups, 0000000)"/></Municipio>
      <Poblacion><xsl:value-of select="format-number(poblacioncups, 0000000)"/></Poblacion>
      <xsl:choose>
        <xsl:when test="string-length(tipoviacups)=2">
          <TipoVia><xsl:value-of select="tipoviacups"/></TipoVia>
        </xsl:when>
      </xsl:choose>
      <xsl:choose>
        <xsl:when test="string-length(cpcups)=5">
          <CodPostal><xsl:value-of select="cpcups"/></CodPostal>
        </xsl:when>
      </xsl:choose>
    </LocalizacionPS>
  </xsl:template>

  <xsl:template match="derechosreconocidos">
    <DerechosReconocidos>
      <DerechosAcceso><xsl:value-of select="format-number(derechosacceso, 00000000000)"/></DerechosAcceso>
      <xsl:choose>
        <xsl:when test="string-length(fechalimitederechosacceso) &gt; 8">
          <FechaLimiteDerechosAcceso><xsl:value-of select="substring-before(fechalimitederechosacceso,' ')"/></FechaLimiteDerechosAcceso>
        </xsl:when>
      </xsl:choose>
      <DerechosExtension><xsl:value-of select="format-number(derechosextension, '00000000000')"/></DerechosExtension>
      <FechaLimiteDerechos><xsl:value-of select="substring-before(fechalimitederechosextension,' ')"/></FechaLimiteDerechos>
    </DerechosReconocidos>
  </xsl:template>

  <xsl:template match="caracteristicas">
    <Caracteristicas>
      <PotenciaMaximaAutorizadaPorBIE><xsl:value-of select="format-number(potbie, '00000000000')"/></PotenciaMaximaAutorizadaPorBIE>
      <PotenciaMaximaAutorizadaPorActa><xsl:value-of select="format-number(potacta, '00000000000')"/></PotenciaMaximaAutorizadaPorActa>
      <InstalacionICP><xsl:value-of select="icp"/></InstalacionICP>
      <xsl:choose>
        <xsl:when test="string-length(liberalizado)=1">
          <SuministroLiberalizado><xsl:value-of select="liberalizado"/></SuministroLiberalizado>
        </xsl:when>
      </xsl:choose>
      <TipoPuntoMedida><xsl:value-of select="tipopuntomedida"/></TipoPuntoMedida>
      <Tension><xsl:value-of select="format-number(tension,'######')"/></Tension>
    </Caracteristicas>
  </xsl:template>

  <xsl:template match="historia">
    <Historia>
      <xsl:choose>
        <xsl:when test="string-length(ult_mov_contr) &gt; 8">
          <FechaUltimoMovimientoContratacion><xsl:value-of select="substring-before(ult_mov_contr,' ')"/></FechaUltimoMovimientoContratacion>
        </xsl:when>
      </xsl:choose>
      <xsl:choose>
        <xsl:when test="string-length(ult_mov_comer) &gt; 8">
          <FechaCambioComercializador><xsl:value-of select="substring-before(ult_mov_comer,' ')"/></FechaCambioComercializador>
        </xsl:when>
      </xsl:choose>
      <xsl:choose>
        <xsl:when test="string-length(fechaalta) &gt; 8">
          <FechaTarifa><xsl:value-of select="substring-before(fechaalta,' ')"/></FechaTarifa>
        </xsl:when>
      </xsl:choose>
      <xsl:choose>
        <xsl:when test="string-length(fechaultimalectura) &gt; 8">
          <FechaUltimaLectura><xsl:value-of select="substring-before(fechaultimalectura,' ')"/></FechaUltimaLectura>
        </xsl:when>
      </xsl:choose>
    </Historia>
  </xsl:template>

  <xsl:template match="consumo">
    <Consumo>
      <TipoPerfilDeConsumo><xsl:value-of select="tipoperfilconsumo"/></TipoPerfilDeConsumo>
      <xsl:apply-templates select="periodos"/>
    </Consumo>
  </xsl:template>

  <xsl:template match="equipo">
    <Equipo>
      <xsl:choose>
        <xsl:when test="string-length(tipoaparato) &gt; 0">
          <TipoAparato><xsl:value-of select="tipoaparato"/></TipoAparato>
        </xsl:when>
      </xsl:choose>
      <xsl:choose>
        <xsl:when test="string-length(tipopropiedad) &gt; 0">
          <TipoPropiedad><xsl:value-of select="tipopropiedad"/></TipoPropiedad>
        </xsl:when>
      </xsl:choose>
    </Equipo>
  </xsl:template>

  <xsl:template match="periodos">
    <Periodos>
      <xsl:apply-templates select="periodo"/>
    </Periodos>
  </xsl:template>

  <xsl:template match="periodo">
    <xsl:if test="anyfacturacion != 1900 ">
    <Periodo>
      <AnioFacturacion><xsl:value-of select="anyfacturacion"/></AnioFacturacion>
      <TipoFacturacion><xsl:value-of select="tipofacturacion"/></TipoFacturacion>
      <FechaLecturaInicial><xsl:value-of select="substring-before(fechalecturainicial,' ')"/></FechaLecturaInicial>
      <FechaLecturaFinal><xsl:value-of select="substring-before(fechalecturafinal,' ')"/></FechaLecturaFinal>
      <CodigoTarifa><xsl:value-of select="codigotarifa"/></CodigoTarifa>
      <CodigoDH><xsl:value-of select="codigodh"/></CodigoDH>
      <xsl:apply-templates select="energiasph"/>
      <xsl:apply-templates select="reactivasph"/>
      <xsl:apply-templates select="potenciasph"/>
    </Periodo>
    </xsl:if>
  </xsl:template>

  <xsl:template match="energiasph">
    <EnergiasPH>
      <xsl:apply-templates select="energiaph"/>
    </EnergiasPH>
  </xsl:template>
  <xsl:template match="energiaph">
    <EnergiaPH>
      <xsl:attribute name="periodo"><xsl:value-of select="periodo"/></xsl:attribute>
      <xsl:value-of select="valor"/>
    </EnergiaPH>
  </xsl:template>

  <xsl:template match="reactivasph">
    <ReactivasPH>
      <xsl:apply-templates select="reactivaph"/>
    </ReactivasPH>
  </xsl:template>
  <xsl:template match="reactivaph">
    <ReactivaPH>
      <xsl:attribute name="periodo"><xsl:value-of select="periodo"/></xsl:attribute>
      <xsl:value-of select="valor"/>
    </ReactivaPH>
  </xsl:template>

  <xsl:template match="potenciasph">
    <PotenciasPH>
      <xsl:apply-templates select="potenciaph"/>
    </PotenciasPH>
  </xsl:template>
  <xsl:template match="potenciaph">
    <PotenciaPH>
      <xsl:attribute name="periodo"><xsl:value-of select="periodo"/></xsl:attribute>
      <xsl:value-of select="valor"/>
    </PotenciaPH>
  </xsl:template>

</xsl:stylesheet>

