# -*- coding: utf-8 -*-

from osv import osv,fields

class giscedata_cups_ps(osv.osv):
  
  _name = 'giscedata.cups.ps'
  _inherit = 'giscedata.cups.ps'

  def _last_date(self, cr, uid, ids, field_name, *args):
    res = {}
    cr.execute("select c.id,coalesce(to_char(greatest(c.create_date,c.write_date,p.create_date,p.write_date), 'YYYYMMDDHH24MISS'), '00000000000000') from giscedata_cups_ps c left join giscedata_polissa p on (p.cups = c.id) where c.id in (%s)" % ', '.join(map(str, map(int, ids))))
    for c in cr.fetchall():
      res[c[0]] = c[1]
    return res

  _columns = {
    'last_date': fields.function(_last_date, method=True, type='datetime', string='Last mod'),
  }

  def sync_web(self, cr, uid):
    # Primer de tot demanem els objectes remots
    client = self.pool.get('gisceweb.sync.client')
    remote_objects = client.get_objects(cr, uid, 'gisceweb.cups', 'cups')
    cups_obj = self.pool.get('giscedata.cups.ps')
    cups_ids = cups_obj.search(cr, uid, [('polissa_polissa', '!=', False)])

    # Algorisme diferencial
    result = {
      'write': [],
      'create': [],
      'unlink': []
    }
    for cups in cups_obj.browse(cr, uid, cups_ids):
      action = 'write'
      if cups.name in remote_objects.keys() and cups.last_date <= remote_objects[cups.name]['last_date']:
        continue
      elif cups.name in remote_objects.keys() and cups.last_date > remote_objects[cups.name]['last_date']:
        action = 'write'
      elif cups.name not in remote_objects.keys():
        action = 'create'

      wc = {}
      wc['cups'] = cups.name
      wc['poblacion'] = 0 # S'HA DE PASSAR UN CODI QUE NO TENIM
      if cups.id_municipi:
        wc['pais'] = cups.id_municipi.state.country_id.name
        wc['municipio'] = cups.id_municipi.ine
        wc['provincia'] = cups.id_municipi.state.code
      else:
        wc['pais'] = 'España'
        wc['municipio'] = 0
        wc['provincia'] = 0

      if cups.polissa_polissa:
        wc['derechosacceso'] =  cups.polissa_polissa.deracceso # SON BUITS AL ERP
        wc['derechosextension'] = cups.polissa_polissa.drets_extensio # SON BUITS AL ERP
        wc['fechalimitederechos'] = cups.polissa_polissa.data_limit_dret_extensio or '2009/12/31 23:59:59' 
        wc['tension'] = 0 # cups.polissa_polissa.tensio S'HA D'ARREGLAR EL CAMP TENSIÓ
        wc['tarifaatr'] = cups.polissa_polissa.tarifa.codi_cne
        # potencies
        pots = 1
        for p in cups.polissa_polissa.potencies:
          wc['potencia_p%s' % int(p.periode_id.name[1:])] = p.potencia
          pots += 1
        while pots < 11:
          wc['potencia_p%s' % (pots,)] = 0
          pots +=1

        # consums
        f_obj = self.pool.get('giscedata.factura')
        fs_ids = f_obj.search(cr, uid, [('polissa','=',cups.polissa_polissa.id)], 0, 12, 'data_inici desc')
        np = 1

        for f in f_obj.browse(cr, uid, fs_ids):
          nle = 1
          wc['p%s_anio' % (np,)] = 2009
          wc['p%s_tipo' % (np,)] = '0000'
          wc['p%s_lectura_inicial' % (np,)] = f.data_inici 
          wc['p%s_lectura_final' % (np,)] = f.data_final 
          wc['p%s_cod_tarifa' % (np,)] = f.tarifa.codi_cne
          wc['p%s_cod_dh' % (np,)] = cups.polissa_polissa.conta_codi_dh

          for le in f.lectures_energia:
            # cada una de les lectures d'energia de la factura
            if le.tipus == 'activa':
              wc['p%s_energia_p%s' % (np, le.name[1:])] = le.consum or 0
              nle += 1

          while nle < 8:
            wc['p%s_energia_p%s' % (np, nle)] = 0
            nle += 1

          nle = 1
          for le in f.lectures_energia:
            if le.tipus == 'reactiva':
              wc['p%s_reactiva_p%s' % (np, le.name[1:])] = le.consum or 0
              nle += 1

          # els períodes que han quedat buits
          while nle < 8:
            wc['p%s_reactiva_p%s' % (np, nle)] = 0
            nle += 1

          nlp = 1
          for lp in f.lectures_potencia:
            # cada una de les lectures de potència de la factura
            if lp.pot_maximetre > 0:
              wc['p%s_potencia_p%s' % (np, lp.name[1:])] = lp.pot_maximetre
            else:
              wc['p%s_potencia_p%s' % (np, lp.name[1:])] = lp.pot_contract + lp.exces
            nlp += 1
          # els períodes que han quedat buits
          while nlp < 8:
            wc['p%s_potencia_p%s' % (np, nlp)] = 0
            nlp += 1

          np += 1

        # Omplim fins a 12 períodes
        while np < 13:
          wc['p%s_anio' % (np,)] = 1900
          wc['p%s_tipo' % (np,)] = '0000'
          wc['p%s_lectura_inicial' % (np,)] = '1900/01/01 00:00:00'
          wc['p%s_lectura_final' % (np,)] = '1900/01/01 00:00:00'
          wc['p%s_cod_tarifa' % (np,)] = '130'
          wc['p%s_cod_dh' % (np,)] = '2'

          for i in range(7):
            wc['p%s_energia_p%s' % (np, i+1)] = 0
            wc['p%s_reactiva_p%s' % (np, i+1)] = 0
            wc['p%s_potencia_p%s' % (np, i+1)] = 0
          np += 1
        wc['potbie'] = cups.polissa_polissa.pot_max_installador
        wc['potacta'] = cups.polissa_polissa.pot_acta
        
        if cups.polissa_polissa.ic_instalacion == 'Y':
          if cups.polissa_polissa.icp_aportado_instalado == 'N':
            wc['instalacionicp'] = 4
          elif cups.polissa_polissa.icp_aportado_instalado == 'C':
            wc['instalacionicp'] = 1
          elif cups.polissa_polissa.icp_aportado_instalado == 'D':
            wc['instalacionicp'] = 2
        else:
          wc['instalacionicp'] = 3

        tpc = { 'cof_a': 'Pa', 'cof_b': 'Pb', 'cof_c': 'Pc' }
        if cups.polissa_polissa.tarifa.cof:
          wc['tipoperfilconsumo'] = tpc[cups.polissa_polissa.tarifa.cof]
        if cups.polissa_polissa.tipus_punt_mesura:
          wc['tipopuntomedida'] = cups.polissa_polissa.tipus_punt_mesura
        wc['fechalimitederechos'] = cups.polissa_polissa.data_limit_dret_extensio or '2009/12/31 23:59:59' # VE BUIT

        # no obligatoris
        wc['codpostal'] = cups.cp
        if action == 'write':
          result[action].append({'id': remote_objects[cups.name]['id'], 'vals': wc})
        else:
          result[action].append({'vals': wc})

    # Cridem al mètode d'actualització mitjançant el client Sync
    client.sync(cr, uid, 'gisceweb.cups', result)
    return True
    
giscedata_cups_ps()
