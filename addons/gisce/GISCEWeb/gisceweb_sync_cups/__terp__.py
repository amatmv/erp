# -*- coding: utf-8 -*-
{
    "name": "GISCEWeb Sync CUPS",
    "description": """GISCEWeb - Mòdul per enviar els CUPS""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEWeb",
    "depends":[
        "base",
        "gisceweb_sync_client",
        "giscedata_cups",
        "giscedata_lectures",
        "giscedata_lectures_perfils"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
