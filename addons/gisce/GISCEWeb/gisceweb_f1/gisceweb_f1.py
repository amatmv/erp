# -*- encoding: utf-8 -*-

from osv import fields, osv

_id_choices = [
  ('CI', 'CIF'),
  ('CT', 'Carta de Trabajo'),
  ('DN', 'DNI'),
  ('NI', 'NIF'),
  ('NV', 'N.I.V.A.'),
  ('OT', 'Otro'),
  ('PS', 'Pasaporte'),
]

_rect_choices = [
  ('N','Normal'), 
  ('R','Rectificadora'), 
  ('A','Anuladora'), 
  ('B','Anuladora con sustituyente'),
]

_moneda_choices = [
  ('01','Pesetas'),
  ('02','Euros'),
]

_tfacturacion_choices = [
   ('1','Regular (Periodo completo)'),
   ('2','Irregular (Periodo incompleto)'),
]

_tfac_choices = [
    ('01','Normal'),
    ('02','Modificación de Contrato'),
    ('03','Baja de Contrato'),
    ('04','Derechos de Contratacion'),
    ('05','Deposito de garantía'),
    ('06','Inspección'),
    ('07','Atenciones (verificaciones, )'),
    ('08','Indemnizacion'),
    ('09','Intereses de demora'),
    ('10','Servicios')
]

_ttarifaatr_choices = [
  ('001','2.0.A'),
  ('002','2.0.N.A'),
  ('003','3.0A'),
  ('004','2.0DHA'),
  ('011','3.1A'),
  ('012','6.1'),
  ('013','6.2'),
  ('014','6.3'),
  ('015','6.4'),
  ('016','6.5'),
]

_indicativo_choices = [
  ('S', 'Sí'),
  ('N', 'No'),
]

_procedencia_choices = [
  ('10','Telemedida'),
  ('11','Telemedida corregida'),
  ('20','TPL'),
  ('21','TPL corregida'),
  ('30','Visual'),
  ('31','Visual corregida'),
  ('40','Estimada'),
  ('50','Autolectura'),
  ('99','Sin lectura'),
]

_codigodh_choices = [
  ('0','Sin Discriminacion'),
  ('1','Sin Contador Discriminador'),
  ('2','Tarifa 2.0 nocturna o contador con doble tarifa'),
  ('3','Con contador triple tarifa sin discriminacion sabados y festivos'),
  ('4','Con contador triple tarifa y discriminacion sabados y festivos  '),
  ('5','Con contador de quintuple tarifa'),
  ('6','THP'),
]

class gisceweb_f1(osv.osv):
  _name = "gisceweb.f1"
  _description = "GISCEWeb F1"

  def _import_potencia(self, cr, uid, ids, field_name, *args):
    res = {}
    ip = 0
    for f in self.browse(cr, uid, ids):
      for p in f.ppotencia:
        ip += float(p.potencia_a_facturar/1000.0) * p.precio_potencia * f.coeficient_prop * (f.num_meses/12.0)
      res[f.id] = ip
      ip = 0
    return res

  def _import_eactiva(self, cr, uid, ids, field_name, *args):
    res = {}
    ie = 0
    for f in self.browse(cr, uid, ids):
      for e in f.penergia:
        ie += float(e.valor * e.precio)
      res[f.id] = ie
      ie = 0
    return res

  def _import_ereactiva(self, cr, uid, ids, field_name, *args):
    res = {}
    ie = 0
    for f in self.browse(cr, uid, ids):
      for e in f.preactiva:
        ie += float(e.valor * e.precio)
      res[f.id] = ie
      ie = 0
    return res

  _columns = {
    'cups': fields.char('CUPS',size=22),
    'lectures': fields.one2many('gisceweb.f1.lectures', 'f1', 'Lectures'),
    'ppotencia': fields.one2many('gisceweb.f1.ppotencia', 'f1', 'Potència'),
    'epotencia': fields.one2many('gisceweb.f1.pexceso', 'f1', 'Excesos potencia'),
    'penergia': fields.one2many('gisceweb.f1.pactiva', 'f1', 'Periodes Activa'),
    'preactiva': fields.one2many('gisceweb.f1.preactiva', 'f1', 'Periodes Reactiva'),
    'importe_total_potencia': fields.function(_import_potencia, type='float', method=True, string="Importe total potencia", readonly=True),
    'importe_total_activa': fields.function(_import_eactiva, type='float', method=True, string='Importe total energía activa', readonly=True),
    'importe_total_reactiva': fields.function(_import_ereactiva, type='float', method=True, string='Importe total energía reactiva', readonly=True),

    # tipus dades generals
    'numero_factura': fields.char('N Factura', size=26,required=True),
    'tipo_factura': fields.selection(_tfac_choices, 'Tipo Factura',required=True),
    'factura_rectificadora': fields.selection(_rect_choices, 'Factura rectificadora',required=True),
    'numero_factura_r': fields.char('N Factura Rect.', size=26),
    'fecha_factura': fields.date('Fecha factura', required=True),
    'cif_emisora': fields.char('CIF Emisora', size=12, required=True),
    'cifnif': fields.selection(_id_choices, 'Tipo de documento',required=True),
    'identificador': fields.char('Identificador', size=9, required=True),
    'contrato': fields.char('Contrato', size=12, required=True),
    'codigo_fiscal_f': fields.char('Código Fiscal Factura', size=17),
    'saldo_factura': fields.float('Saldo', digits=(12,2)),
    'saldo_cobro': fields.float('Saldo de cobro', digits=(12,2)),
    'tipo_facturacion': fields.selection(_tfacturacion_choices, 'Tipo Facturación', required=True),
    'fecha_boe':  fields.date('Fecha BOE', required=True),
    'codigo_tarifa': fields.selection(_ttarifaatr_choices, 'Tarifa ATR', required=True),
    # tipus facturaatr
    'municipio': fields.char('Municipio', size=7),
    'direccion_suministro': fields.char('Dirección de suministro', size=60),
    'medido_en_baja': fields.selection(_indicativo_choices, 'Alta con medida en baja'),
    'fecha_desde_factura': fields.date('Fecha Desde'),
    'fecha_hasta_factura': fields.date('Fecha Hasta'),
    'num_meses': fields.float('Número de meses', digits=(5,2)),
    'coeficient_prop': fields.float('Coeficient proporcionalitat'),

    # alquileres
    'alquileres': fields.float('Alquileres', digits=(14,2)),

    # conta
    'conta_tipo': fields.char('Conta Tipo',size=2),
    'conta_marca': fields.char('Conta Marca', size=3),
    'conta_numeroserie': fields.char('Conta numserie', size=12),
    'conta_codigo_dh': fields.selection(_codigodh_choices, 'Conta Código DH'),

    # registro fin
    'importe_total': fields.float('Importe total', digits=(15,2), required=True),
    'saldo_total_facturacion': fields.float('Saldo total facturación', digits=(15,2), required=True),
    'saldo_total_cobro': fields.float('Saldo total cobro', digits=(15,2), required=True),
    'total_recibos': fields.integer('Total de recibos del XML', required=True),
    'tipo_moneda': fields.selection(_moneda_choices, 'Tipo Moneda', required=True),
    'fecha_valor': fields.date('Fecha Valor', required=True),
    'fecha_limite_pago': fields.date('Fecha límite de pago', required=True),
    'banco': fields.char('Banco', size=12),
    'sucursal': fields.char('Sucursal', size=12),
    'dc': fields.char('DC', size=2),
    'cuenta': fields.char('CCC', size=12),
    'id_remesa': fields.char('Id Remesa', size=26, required=True),
    'base_imponible': fields.float('Base imponible', digits=(14,2), required=True),

    # iva
    'base_imponible_iva': fields.float('Base imponible IVA', digits=(14,2)),
    'porcentaje_iva': fields.float('% IVA', digits=(4, 2)),
    'importe_iva': fields.float('Importe IVA', digits=(14,2)),

    # impuesto eléctrico
    'coeficiente_ie': fields.float('Coeficiente', digits=(4, 2)),
    'porcentaje_ie': fields.float('% IE', digits=(4,2)),
    'base_imponible_ie': fields.float('Base imponible I.E.', digits=(14,2)),
    'importe_ie': fields.float('Importe I.E.', digits=(14,2)),
  }

  def name_get(self, cr, uid, ids, context={}):
    if not len(ids):
      return []
    res = []
    for f in self.read(cr, uid, ids, ['numero_factura']):
      res.append((f['id'], f['numero_factura']))     
    return res

gisceweb_f1()

class gisceweb_f1_lectures(osv.osv):
  _name = "gisceweb.f1.lectures"
  _description = "GISCEWeb F1 Lectures"
  _columns = {
    'f1': fields.many2one('gisceweb.f1', 'F1', ondelete='cascade'),
    'magnitud': fields.char('Magnitud', size=2),
    'codigo_periodo': fields.char('Código periodo', size=2),
    'ct_multiplicadora': fields.integer('Constante multiplicadora'),
    'nruedasenteras': fields.integer('N Ruedas Enteras'),
    'nruedasdecimales': fields.integer('N Ruedas decimales'),
    'consumo': fields.integer('Consumo'),
    'fecha_anterior': fields.datetime('Fecha lectura anterior'),
    'lectura_anterior': fields.integer('Lectura anterior'),
    'procedencia_anterior': fields.selection(_procedencia_choices, 'Procedencia anterior'),
    'fecha_actual': fields.datetime('Fecha lectura actual'),
    'lectura_actual': fields.integer('Lectura actual'),
    'procedencia_actual': fields.selection(_procedencia_choices, 'Procedencia actual'),
  }

gisceweb_f1_lectures()

class gisceweb_f1_ppotencia(osv.osv):
  _name = 'gisceweb.f1.ppotencia'
  _description = 'GISCEWeb F1 Període potència'
  _columns = {
    'potencia_contratada': fields.integer('Potencia contratada'),
    'potencia_max_demandada': fields.integer('Potencia máxima demandada'),
    'potencia_a_facturar': fields.integer('Potencia a facturar'),
    'precio_potencia': fields.float('Precio potencia', digits=(13,8)),
    'f1': fields.many2one('gisceweb.f1', 'Factura F1', ondelete='cascade'),
  }

gisceweb_f1_ppotencia()

class gisceweb_f1_exces_potencia(osv.osv):
  _name = 'gisceweb.f1.pexceso'
  _description = 'GISCEWeb F1 períodos exceso'
  _columns = {
    'valor_exceso': fields.integer('Valor exceso potencia'),
    'f1': fields.many2one('gisceweb.f1', 'Factura F1', ondelete='cascade'),
  }

gisceweb_f1_exces_potencia()

class gisceweb_f1_pactiva(osv.osv):
  _name = 'gisceweb.f1.pactiva'
  _description = 'GISCEWeb F1 período activa'
  _columns = {
    'valor': fields.float('Valor Activa', digits=(13,2)),
    'precio': fields.float('Precio', digits=(13, 8)),
    'f1': fields.many2one('gisceweb.f1', 'Factura F1'),
  }

gisceweb_f1_pactiva()

class gisceweb_f1_preactiva(osv.osv):
  _name = 'gisceweb.f1.preactiva'
  _description = 'GISCEWeb F1 período reactiva'
  _columns = {
    'valor': fields.float('Valor Reactiva', digits=(13,2)),
    'precio': fields.float('Precio', digits=(13, 8)),
    'f1': fields.many2one('gisceweb.f1', 'Factura F1'),
  }

gisceweb_f1_preactiva()

