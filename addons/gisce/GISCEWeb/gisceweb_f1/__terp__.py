# -*- coding: utf-8 -*-
{
    "name": "GISCEWeb F1",
    "description": """GISCEWeb acceso a la información de facturación""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEWeb",
    "depends":[
        "base",
        "gisceweb_cups",
        "giscemisc_xml_reports",
        "gisceweb_base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "gisceweb_f1_report.xml",
        "gisceweb_f1_view.xml",
        "gisceweb_f1_security.xml"
    ],
    "active": False,
    "installable": True
}
