# -*- coding: utf-8 -*-
{
    "name": "GISCEWeb AF1",
    "description": """GISCEWeb acceso al proceso F1""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEWeb",
    "depends":[
        "base",
        "gisceweb_cups",
        "gisceweb_f1",
        "gisceweb_base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "gisceweb_af1_view.xml",
        "gisceweb_af1_workflow.xml",
        "gisceweb_af1_security.xml",
        "gisceweb_af1_report.xml"
    ],
    "active": False,
    "installable": True
}
