<?xml version="1.0" encoding="ISO-8859-1" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://localhost/elegibilidad">

  <xsl:import href="../../gisceweb_base/report/cabecera.xslt"/>
  <xsl:import href="../../gisceweb_base/report/medidas.xslt"/>
  <xsl:import href="../../gisceweb_base/report/common.xslt"/>

  <xsl:template match="/afs1s">
    <xsl:apply-templates select="af1"/>
  </xsl:template>

  <xsl:template match="af1">
    <MensajeFacturacion>
      <xsl:attribute name="AgenteSolicitante">
        <xsl:value-of select="agentesolicitante"/>
      </xsl:attribute>
      <xsl:apply-templates select="cabecera"/>  
      <Facturas>
        <xsl:apply-templates select="item"/>
      </Facturas>
    </MensajeFacturacion>
  </xsl:template>

  <xsl:template match="item">
    <xsl:apply-templates select="facturaatr"/>
    <xsl:apply-templates select="registrofin"/>
  </xsl:template>

  <xsl:template match="facturaatr">
    <FacturaATR>
      <xsl:apply-templates select="datosgeneralesatr"/>
      <xsl:apply-templates select="potencia"/>
      <xsl:apply-templates select="eactiva"/>
      <xsl:apply-templates select="ereactiva"/>
      <xsl:apply-templates select="impuestoelectrico"/>
      <xsl:apply-templates select="alquileres"/>
      <xsl:apply-templates select="iva"/>
      <Medidas>
        <CodUnificadoPuntoSuministro>
        <xsl:call-template name="get-cups">
          <xsl:with-param name="c">
            <xsl:value-of select="medidas/cups"/>
          </xsl:with-param>
        </xsl:call-template>
        </CodUnificadoPuntoSuministro>
        <xsl:apply-templates select="medidas"/>
      </Medidas>
    </FacturaATR>
  </xsl:template>

  <xsl:template match="datosgeneralesatr">
    <DatosGeneralesFacturaATR>
      <xsl:apply-templates select="direccionsuministro"/>
      <xsl:apply-templates select="cliente"/>
      <Contrato><xsl:value-of select="contrato"/></Contrato>
      <xsl:apply-templates select="datosgenerales"/>
      <xsl:apply-templates select="datosfacturaatr"/>
    </DatosGeneralesFacturaATR>
  </xsl:template>

  <xsl:template match="direccionsuministro">
    <DireccionSuministro>
      <CUPS>
        <xsl:call-template name="get-cups">
          <xsl:with-param name="c">
            <xsl:value-of select="cups"/>
          </xsl:with-param>
        </xsl:call-template>
      </CUPS>
      <Municipio><xsl:value-of select="municipio"/></Municipio>
      <DirSuministro><xsl:value-of select="dirsuministro"/></DirSuministro>
    </DireccionSuministro>
  </xsl:template>
  <xsl:template match="cliente">
    <Cliente>
      <TipoCIFNIF><xsl:value-of select="cifnif"/></TipoCIFNIF>
      <Identificador><xsl:value-of select="identificador"/></Identificador>
    </Cliente>
  </xsl:template>

  <xsl:template match="datosgenerales">
    <DatosGeneralesFactura>
      <NumeroFactura><xsl:value-of select="numerofactura"/></NumeroFactura>
      <TipoFactura><xsl:value-of select="tipofactura"/></TipoFactura>
      <IndicativoFacturaRectificadora><xsl:value-of select="indfacturarectificadora"/></IndicativoFacturaRectificadora>
      <FechaFactura><xsl:value-of select="fechafactura"/></FechaFactura>
      <CIFEmisora><xsl:value-of select="cifemisora"/></CIFEmisora>
      <xsl:if test="string-length(codigofiscalfactura) &gt; 0"><CodigoFiscalFactura><xsl:value-of select="codigofiscalfactura"/></CodigoFiscalFactura></xsl:if>
      <xsl:if test="string-length(observaciones) &gt; 0"><Observaciones><xsl:value-of select="observaciones"/></Observaciones></xsl:if>
      <ImporteTotalFactura><xsl:value-of select="importetotalfactura"/></ImporteTotalFactura>
      <SaldoFactura><xsl:value-of select="../../../registrofin/saldototalfacturacion"/></SaldoFactura>
      <SaldoCobro><xsl:value-of select="../../../registrofin/saldototalcobro"/></SaldoCobro>
      <TipoMoneda><xsl:value-of select="tipomoneda"/></TipoMoneda>
    </DatosGeneralesFactura>
  </xsl:template>

  <xsl:template match="datosfacturaatr">
    <DatosFacturaATR>
      <TipoFacturacion><xsl:value-of select="tipofacturacion"/></TipoFacturacion>
      <FechaBOE><xsl:value-of select="fechaboe"/></FechaBOE>
      <CodigoTarifa><xsl:value-of select="codigotarifa"/></CodigoTarifa>
      <IndAltamedidoenBaja><xsl:value-of select="medidoenbaja"/></IndAltamedidoenBaja>
      <xsl:apply-templates select="periodo"/>
    </DatosFacturaATR>
  </xsl:template>

  <xsl:template match="periodo">
    <Periodo>
      <FechaDesdeFactura><xsl:value-of select="fechadesde"/></FechaDesdeFactura>
      <FechaHastaFactura><xsl:value-of select="fechahasta"/></FechaHastaFactura>
      <NumeroMeses><xsl:value-of select="nummeses"/></NumeroMeses>
    </Periodo>
  </xsl:template>
  
  <xsl:template match="potencia">
    <Potencia>
      <xsl:apply-templates select="terminopotencia"/>
      <ImporteTotalTerminoPotencia><xsl:value-of select="format-number(importetotalpotencia,'####.####')"/></ImporteTotalTerminoPotencia>
    </Potencia>
  </xsl:template>
 
  <xsl:template match="terminopotencia">
    <TerminoPotencia>
      <FechaDesde><xsl:value-of select="../../datosgeneralesatr/datosfacturaatr/periodo/fechadesde"/></FechaDesde>
      <FechaHasta><xsl:value-of select="../../datosgeneralesatr/datosfacturaatr/periodo/fechahasta"/></FechaHasta>
      <xsl:apply-templates select="ppotencia"/>
    </TerminoPotencia>
  </xsl:template>

  <xsl:template match="ppotencia">
    <Periodo>
      <PotenciaContratada><xsl:value-of select="potenciacontratada"/></PotenciaContratada>
      <PotenciaMaxDemandada><xsl:value-of select="potenciamaxdemandada"/></PotenciaMaxDemandada>
      <PotenciaAFacturar><xsl:value-of select="potenciaafacturar"/></PotenciaAFacturar>
      <PrecioPotencia><xsl:value-of select="preciopotencia"/></PrecioPotencia>
    </Periodo>
  </xsl:template>

  <xsl:template match="eactiva">
    <EnergiaActiva>
      <xsl:apply-templates select="terminoenergiaactiva"/>
      <ImporteTotalEnergiaActiva><xsl:value-of select="format-number(importetotaleactiva, '###.####')"/></ImporteTotalEnergiaActiva>
    </EnergiaActiva>
  </xsl:template>
 
  <xsl:template  match="terminoenergiaactiva">
    <TerminoEnergiaActiva>
      <FechaDesde><xsl:value-of select="../../datosgeneralesatr/datosfacturaatr/periodo/fechadesde"/></FechaDesde>
      <FechaHasta><xsl:value-of select="../../datosgeneralesatr/datosfacturaatr/periodo/fechahasta"/></FechaHasta>
      <Periodo>
        <ValorEnergiaActiva><xsl:value-of select="periodo/valorenergiaactiva"/></ValorEnergiaActiva>
        <PrecioEnergia><xsl:value-of select="periodo/precioenergia"/></PrecioEnergia>
      </Periodo>
    </TerminoEnergiaActiva>
  </xsl:template>

  <xsl:template match="ereactiva">
    <xsl:if test="count(terminoenergiaactiva) &gt; 0">
    <EnergiaReactiva>
      <xsl:apply-templates select="terminoenergiareactiva"/>
      <ImporteTotalEnergiaReactiva><xsl:value-of select="importetotalereactiva"/></ImporteTotalEnergiaReactiva>
    </EnergiaReactiva>
    </xsl:if>
  </xsl:template>
 
  <xsl:template match="terminoenergiareactiva">
    <TerminoEnergiaReactiva>
      <FechaDesde><xsl:value-of select="../../datosgeneralesatr/datosfacturaatr/periodo/fechadesde"/></FechaDesde>
      <FechaHasta><xsl:value-of select="../../datosgeneralesatr/datosfacturaatr/periodo/fechahasta"/></FechaHasta>
      <Periodo>
        <ValorEnergiaReactiva><xsl:value-of select="periodo/valorenergiareactiva"/></ValorEnergiaReactiva>
        <PrecioEnergiaReactiva><xsl:value-of select="periodo/precioenergia"/></PrecioEnergiaReactiva>
      </Periodo>
    </TerminoEnergiaReactiva>
  </xsl:template>

  <xsl:template match="excesopotencia">
    <ExcesoPotencia>
    <xsl:apply-templates select="periodoepotencia"/>
    </ExcesoPotencia>
  </xsl:template>

  <xsl:template match="periodoepotencia">
    <Periodo>
      <ValorExcesoPotencia><xsl:value-of select="valorexcesopotencia"/></ValorExcesoPotencia>
    </Periodo>
  </xsl:template>

  <xsl:template match="alquileres">
    <Alquileres>
      <ImporteFacturacionAlquileres><xsl:value-of select="importefacturacionalquileres"/></ImporteFacturacionAlquileres>
    </Alquileres>
  </xsl:template>
  
  <xsl:template match="iva">
    <IVA>
      <BaseImponible><xsl:value-of select="baseimponible"/></BaseImponible>
      <Porcentaje><xsl:value-of select="porcentaje"/></Porcentaje>
      <Importe><xsl:value-of select="importe"/></Importe>
    </IVA>
  </xsl:template>
  
  <xsl:template match="impuestoelectrico">
    <ImpuestoElectrico>
      <BaseImponible><xsl:value-of select="baseimponible"/></BaseImponible>
      <Coeficiente><xsl:value-of select="coeficiente"/></Coeficiente>
      <Porcentaje><xsl:value-of select="porcentaje"/></Porcentaje>
      <Importe><xsl:value-of select="importe"/></Importe>
    </ImpuestoElectrico>
  </xsl:template>

  <xsl:template match="registrofin">
    <RegistroFin>
      <ImporteTotal><xsl:value-of select="importetotal"/></ImporteTotal>
      <SaldoTotalFacturacion><xsl:value-of select="saldototalfacturacion"/></SaldoTotalFacturacion>
      <SaldoTotalCobro><xsl:value-of select="saldototalcobro"/></SaldoTotalCobro>
      <TotalRecibos><xsl:value-of select="totalrecibos"/></TotalRecibos>
      <TipoMoneda><xsl:value-of select="tipomoneda"/></TipoMoneda>
      <FechaValor><xsl:value-of select="fechavalor"/></FechaValor>
      <FechaLimitePago><xsl:value-of select="fechalimitepago"/></FechaLimitePago>
      <xsl:apply-templates select="cuentabancaria"/>
      <IdRemesa><xsl:value-of select="remesa"/></IdRemesa>
    </RegistroFin>
  </xsl:template>

  <xsl:template match="cuentabancaria">
    <xsl:if test="string-length(banco) &gt; 0">
    <CuentaBancaria>
      <Banco><xsl:value-of select="banco"/></Banco>
      <Sucursal><xsl:value-of select="sucursal"/></Sucursal>
      <DC><xsl:value-of select="dc"/></DC>
      <Cuenta><xsl:value-of select="cuenta"/></Cuenta>
    </CuentaBancaria>
    </xsl:if>
  </xsl:template>

</xsl:stylesheet>

