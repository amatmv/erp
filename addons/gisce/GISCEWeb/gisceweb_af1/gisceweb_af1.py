# -*- encoding: utf-8 -*-

from osv import fields, osv
import time

class gisceweb_af1(osv.osv):
  _name = 'gisceweb.af1'
  _description = 'GISCE Web AF1'
  _estats = [('draft','Borrador'), ('pendent_validacio','Pendiente de validación'), ('validada', 'Validada'), ('refusada','Rechazada'), 
    ('registrada','Registrada'), ('enviament','Envío de datos'),('facturacio','Facturación accesos')]

  _columns = {
    'cups': fields.many2one('gisceweb.cups', 'CUPS'),
    'factura': fields.many2one('gisceweb.f1', 'Factura'),

    'cups_sollicitat': fields.char('CUPS', size=22),
    'ref_sollicitud': fields.char('Ref Solicitud', size=16),
    'secuencia': fields.integer('Secuencial de solicitud'),
    'data_entrada': fields.datetime('Fecha solicitud'),
    'emisor': fields.many2one('res.company', 'Emisor', readonly=True),
    'receptor': fields.many2one('res.company', 'Receptor'),
    'state': fields.selection(_estats, 'Estado', readonly=True),
  }

  _defaults = {
    'state': lambda *a: 'draft',
    'data_entrada': lambda *a: time.strftime("%Y-%m-%d %H:%M:%S"),
    'emisor': lambda self,cr,uid,c: self.pool.get('res.users').browse(cr, uid, uid, c).company_id.id,

  }

  def wkf_f_draft(self, cr, uid, ids):
    self.write(cr, uid, ids, { 'state' : 'draft' })
    return True

  def wkf_f_validada(self, cr, uid, ids):
    self.write(cr, uid, ids, {'state':'validada'})
    return True

  def wkf_f_pendent_validacio(self, cr, uid, ids):
    self.write(cr, uid, ids, {'state':'pendent_validacio'})
    af1s = self.pool.get('gisceweb.af1').browse(cr, uid, ids)
    for af1 in af1s:
      cups = self.pool.get('gisceweb.cups').search(cr, uid, [('cups','=',af1.cups_sollicitat)])
      if cups:
        print(cups[0])
        self.pool.get('gisceweb.af1').write(cr, uid, af1.id, {'cups': cups[0]})
      factura = self.pool.get('gisceweb.f1').search(cr, uid, [('cups','=',af1.cups_sollicitat)])
      if factura:
        print(factura[0])
        self.pool.get('gisceweb.af1').write(cr, uid, af1.id, {'factura': factura[0]})
    return True

  def wkf_f_refusada(self, cr, uid, ids):
    self.write(cr, uid, ids, {'state':'refusada'})
    return True

  def wkf_f_registrada(self, cr, uid, ids):
    self.write(cr, uid, ids, {'state':'registrada'})
    return True

  def wkf_f_enviament(self, cr, uid, ids):
    self.write(cr, uid, ids, {'state':'enviament'})
    af1s = self.pool.get('gisceweb.f1').browse(cr, uid, ids)
    for af1 in af1s:
      cups = af1.cups
    return True

  def wkf_f_facturacio(self, cr, uid, ids):
    self.write(cr, uid, ids, {'state':'facturacio'})
    return True


gisceweb_af1()

