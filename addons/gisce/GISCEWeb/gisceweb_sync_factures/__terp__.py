# -*- coding: utf-8 -*-
{
    "name": "GISCEWeb Sync Factures",
    "description": """GISCEWeb - Mòdul per enviar les factures""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEWeb",
    "depends":[
        "base",
        "giscedata_lectures",
        "gisceweb_sync_client"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
