# -*- coding: utf-8 -*-

from osv import osv,fields
import time

class giscedata_factura(osv.osv):

  _name = 'giscedata.factura'
  _inherit = 'giscedata.factura'

  def _last_date(self, cr, uid, ids, field_name, *args):
    res = {}
    cr.execute("select f.id,coalesce(to_char(greatest(f.create_date,f.write_date,i.create_date,i.write_date), 'YYYYMMDDHH24MISS'), '00000000000000') from giscedata_factura f left join account_invoice i on (f.factura = i.id) where f.id in (%s)" % (', '.join(map(str, map(int, ids)))))
    for c in cr.fetchall():
      res[c[0]] = c[1]
    return res

  _columns = {
    'last_date': fields.function(_last_date, method=True, type='datetime', string='Last mod'),
  }

  def sync_web(self, cr, uid, context={}):
    # Primer de tot demanem els objectes remots
    client = self.pool.get('gisceweb.sync.client')
    remote_objects = client.get_objects(cr, uid, 'gisceweb.f1', 'numero_factura')
    fac_obj = self.pool.get('giscedata.factura')
    fac_ids = fac_obj.search(cr, uid, [])

    # Algorisme diferencial
    result = {
      'write': [],
      'create': [],
      'unlink': []
    }
    for fac in fac_obj.browse(cr, uid, fac_ids):
      action = 'write'
      if fac.name in remote_objects.keys() and fac.last_date <= remote_objects[fac.name]['last_date']:
        continue
      elif fac.name in remote_objects.keys() and fac.last_date > remote_objects[fac.name]['last_date']:
        action = 'write'
      elif fac.name not in remote_objects.keys():
        action = 'create'
      wf = {}
      if fac.tipo_rectificadora == 'N':
        wf['numero_factura'] = fac.name
      else:
        wf['numero_factura'] = 0
      wf['tipo_factura'] = fac.tipo_factura
      wf['factura_rectificadora'] = fac.tipo_rectificadora
      wf['tipo_facturacion'] = fac.tipo_facturacion
      if fac.tipo_rectificadora != 'N':
        wf['numero_factura_r'] = fac.name
      else:
        wf['numero_factura_r'] = 0

      wf['codigo_fiscal_f'] = fac.name # el codi fiscal és el número de factura ?
      wf['fecha_desde_factura'] = fac.data_inici
      wf['fecha_hasta_factura'] = fac.data_final
      wf['num_meses'] = fac.facturacio
      wf['coeficient_prop'] = fac.coficient
      wf['fecha_factura'] = fac.factura.date_invoice
      wf['base_imponible'] = fac.subtotal

      # registro fin
      wf['importe_total'] = fac.total
      wf['saldo_total_facturacion'] = fac.total
      wf['saldo_total_cobro'] = 0
      wf['total_recibos'] = 1
      wf['tipo_moneda'] = '02'
      wf['fecha_valor'] = fac.factura.date_invoice
      wf['fecha_limite_pago'] = fac.factura.date_due or fac.factura.date_invoice
      #wf['cuenta_bancaria'] = ''
      wf['id_remesa'] = ''
        
      # impuesto eléctrico -- sempre zero per Peajes
      wf['base_imponible_ie'] = 0
      wf['coeficiente_ie'] = 0
      wf['porcentaje_ie'] = 0
      wf['importe_ie'] = 0

      # iva
      wf['base_imponible_iva'] = fac.subtotal
      wf['porcentaje_iva'] = fac.tax.amount # % iva, no el valor en €
      wf['importe_iva'] =  fac.subtotal * fac.tax.amount # no posem fac.taxes per si hi ha altres impostos

      wf['conta_tipo'] = 'CC'
      wf['conta_marca'] = '199'
      wf['conta_codigo_dh'] = fac.polissa.conta_codi_dh # el tenim a la pòlissa

      
      # potencia
      # activa
      # reactiva

      # medidas ...

      # Les lectures nomes les importa al crear, si despres es modifiquen les lectures de la factura
      # s'ha d'eliminar del GISCEWeb i tornar-la a exportar

      if action == 'create':
        lectures = []
        dates_tmp = {}
        for ole in fac.lectures_energia:
          le = {}
          le['magnitud'] = {'activa': 'AE', 'reactiva': 'R1'}[ole.tipus]
          le['codigo_periodo'] = ole.name
          le['consumo'] = ole.consum
          le['fecha_anterior'] = ole.data_anterior
          le['lectura_anterior'] = ole.lect_anterior
          le['fecha_actual'] = ole.data_actual
          le['lectura_actual'] = ole.lect_actual
          dates_tmp[ole.name] = {
            'desde': ole.data_anterior,
            'hasta': ole.data_actual
          }
          lectures.append([0,0,le])

        lectures_potencia = []
        potencies_tmp = {}
        for olp in fac.lectures_potencia:
          lp = {}
          lm = {}
          lp['codigo_periodo'] = olp.name
          lp['magnitud'] = 'PM'
          lp['consumo'] = olp.pot_maximetre
          if dates_tmp.has_key(olp.name):
            lp['fecha_anterior'] = dates_tmp[olp.name]['desde']
            lp['fecha_actual'] = dates_tmp[olp.name]['hasta']
          lectures.append([0,0,lp])

          lm['codigo_periodo'] = olp.name
          lm['magnitud'] = 'EP'
          lm['consumo'] = olp.exces
          if dates_tmp.has_key(olp.name):
            lm['fecha_anterior'] = dates_tmp[olp.name]['desde']
            lm['fecha_actual'] = dates_tmp[olp.name]['hasta']
          lectures.append([0,0,lm])
          
          
          potencies_tmp[olp.name] = {
            'potencia_max_demandada': olp.pot_maximetre,
            'potencia_contratada': olp.pot_contract
          }

        wf['lectures'] = lectures


        # Línies de factura
        ppotencia = []
        epotencia = []
        penergia = []
        preactiva = []
        wf['alquileres'] = 0
        for lf in fac.linies:
          dlf = {}
          if lf.tipus == 'potencia':
            dlf['potencia_contratada'] = potencies_tmp[lf.name]['potencia_contratada']*1000
            dlf['potencia_max_demandada'] = potencies_tmp[lf.name]['potencia_max_demandada']*1000
            dlf['potencia_a_facturar'] = lf.quantitat*1000
            dlf['precio_potencia'] = lf.preu
            ppotencia.append([0,0,dlf])
          elif lf.tipus == 'exces_potencia':
            dlf['valor_del_exceso'] = lf.quantitat
          elif lf.tipus == 'reactiva':
            dlf['valor'] = lf.quantitat
            dlf['precio'] = lf.preu
            preactiva.append([0,0,dlf])
          elif lf.tipus == 'energia':
            dlf['valor'] = lf.quantitat 
            dlf['precio'] = lf.preu
            penergia.append([0,0,dlf])
          elif lf.tipus == 'lloguer':
            wf['alquileres'] += lf.subtotal
            

        wf['ppotencia'] = ppotencia
        wf['epotencia'] = epotencia
        wf['penergia'] = penergia
        wf['preactiva'] = preactiva

      wf['cif_emisora'] = fac.factura.company_id.partner_id.vat
      wf['identificador'] = fac.comercialitzadora.vat
      wf['cifnif'] = 'CI'

      if fac.polissa:
        wf['contrato'] = fac.polissa.name
        wf['municipio'] = fac.polissa.cups.id_municipi.ine
        wf['cups'] = fac.polissa.cups.name
      
      # wf['fecha_boe'] = time.strftime('%Y-%m-%d',time.strptime(fac.pricelist.split('-')[1].strip(), '%d/%m/%Y')) or '2008-12-31'
      wf['fecha_boe'] = '2008-12-29'
      wf['codigo_tarifa'] = fac.tarifa.codi_cne
      wf['conta_numeroserie'] = fac.comptador.name.name

      if fac.tarifa.facturacio_code == '3_1_A_LB':
        wf['medido_en_baja'] = 'S'
      else:
        wf['medido_en_baja'] = 'N'

      if action == 'write':
        result['write'].append({'id': remote_objects[fac.name]['id'], 'vals': wf})
      else:
        result['create'].append({'vals': wf})

    # Cridem al mètode d'actualització mitjançant el client Sync
    client.sync(cr, uid, 'gisceweb.f1', result)
    return True

giscedata_factura()
