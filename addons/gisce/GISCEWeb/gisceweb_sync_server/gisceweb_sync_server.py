# -*- coding: utf-8 -*-

from osv import fields, osv

class gisceweb_sync_server(osv.osv):
  _name = "gisceweb.sync.server"
  _description = "GISCEWeb Sync Server"
  _auto = False
  _columns = {}

  def get_objects(self, cr, uid, model, fields, search_params=[], context={}):
    # Retornem un diccionari amb {'compost': date}
    res = {}
    if type(fields) != type([]):
      fields = [fields]
    fields = map(str, fields)

    obj = self.pool.get(model)
    ids = obj.search(cr, uid, search_params, context=context)
    if not len(ids):
      return res
    cr.execute("select %s, coalesce(to_char(greatest(create_date,write_date), 'YYYYMMDDHH24MISS'), '00000000000000') as last_date,id from %s where id in (%s)" % (', '.join(fields), obj._table, ','.join(map(str, map(int, ids)))))
    for line in cr.dictfetchall():
      res[''.join(map(str, [line[field] for field in fields]))] = {'id': line['id'], 'last_date': line['last_date']}
    return res
      

gisceweb_sync_server()

