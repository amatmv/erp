# -*- coding: utf-8 -*-
{
    "name": "GISCEWeb Sync Server",
    "description": """GISCEWeb - Mòdul per enviar les dades cap al ERP Client""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEWeb",
    "depends":[
        "base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
