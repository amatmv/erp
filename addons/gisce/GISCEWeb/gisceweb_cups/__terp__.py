# -*- coding: utf-8 -*-
{
    "name": "GISCEWeb CUPS",
    "description": """GISCEWeb model d'intercanvi de dades de CUPS""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEWeb",
    "depends":[
        "base",
        "gisceweb_base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "gisceweb_cups_view.xml",
        "gisceweb_cups_security.xml"
    ],
    "active": False,
    "installable": True
}
