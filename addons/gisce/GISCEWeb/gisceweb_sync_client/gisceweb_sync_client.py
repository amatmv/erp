# -*- coding: utf-8 -*-

from osv import fields, osv
import xmlrpclib

class gisceweb_sync_client(osv.osv):
  _name = "gisceweb.sync.client"
  _description = "GISCEWeb Sync Client"
  _columns = {
    'server': fields.char('IP Servidor Web', size=15, required=True),
    'user': fields.char('Usuario', size=255, required=True),
    'password': fields.char('Password', size=255, required=True),
    'dbname': fields.char('Base de datos', size=255, required=True),
    'port': fields.integer('Puerto', required=True),
  }

  _defaults = {
    'server': lambda *a: 'localhost',
    'port': lambda *a: 8069,
  }

  def login(self, cr, uid):
    """
    Executa el login al ERP remot, retornant False si falla o un diccionari amb:
    'sock': el socket XMLRPC
    'dbname': el nom de la base de dades remota
    'uid': el uid 
    'pwd': el password
    """
    res = self.search(cr, uid, [])
    for gs_ in self.browse(cr, uid, res):
      sock = xmlrpclib.ServerProxy("http://%s:%s/xmlrpc/common" % (gs_.server, gs_.port))
      uid = sock.login(gs_.dbname, gs_.user, gs_.password)
      if uid:
        return { 'sock': xmlrpclib.ServerProxy("http://%s:%s/xmlrpc/object" % (gs_.server, gs_.port)), 'dbname': gs_.dbname, 'uid': uid, 'pwd': gs_.password }
      return False

  def get_objects(self, cr, uid, model, fields, search_params=[], context={}):
    s = self.login(cr, uid)
    if not s:
      return {} 
    else:
      res = s['sock'].execute(s['dbname'], s['uid'], s['pwd'], 'gisceweb.sync.server', 'get_objects', model, fields, search_params, context)
      return res

  def sync(self, cr, uid, model, result):
    s = self.login(cr, uid)
    status = {
      'writes': 0,
      'creates': 0
    }
    if not s:
      return False
    else:
      # Primer fem els writes
      for r in result['write']:
        s['sock'].execute(s['dbname'], s['uid'], s['pwd'], model, 'write', [r['id']], r['vals'])
        status['writes'] += 1
      for r in result['create']:
        s['sock'].execute(s['dbname'], s['uid'], s['pwd'], model, 'create', r['vals'])
        status['creates'] += 1
      return status

gisceweb_sync_client()
