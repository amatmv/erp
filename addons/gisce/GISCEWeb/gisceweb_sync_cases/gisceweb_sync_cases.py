# -*- coding: utf-8 -*-

from osv import osv,fields

# Aquest mòdul no tindrà cap taula, l'unic que farà serà connectar-se, descarregar-se les
# sol·licituds i crear els casos per cada sol·licitud.

# Hi haurà d'haver un mòdul per cada tipus de sol·licitud, que cridarà la funció de buscar
# sol·licituds al canl web.


class gisceweb_sync_cases(osv.osv):

  _name = 'gisceweb.sync.cases'
  _auto = False

  _columns = {}


  def get_cases(self, cr, uid, model, section_code, search_params=[], fields=[], context={}):
    """
    Buscarem els casos del servidor que coincideixin amb els paràmetres que ens han passat
    al @search_params
    """
    crm_obj = self.pool.get('crm.case')
    remote_server = self.pool.get('gisceweb.sync.client').login(cr, uid)
    if not remote_server:
      return []
    ids = remote_server['sock'].execute(remote_server['dbname'], remote_server['uid'], remote_server['pwd'], model, 'search', search_params)
    # Descarreguem la info per poder fer els casos
    sols = remote_server['sock'].execute(remote_server['dbname'], remote_server['uid'], remote_server['pwd'], model, 'read', ids, fields)

    # Busquem el responable de la secció d'aquest cas
    crm_sec_obj = self.pool.get('crm.case.section')
    section_id = crm_sec_obj.search(cr, uid, [('code', '=', section_code)])
    if len(section_id):
      section = crm_sec_obj.browse(cr, uid, section_id[0])
      if section.user_id:
        user_id = section.user_id.id
      else:
        user_id = uid

    # Creem el nou cas

    for sol in sols:
      description = "* PARAMETROS DE ENTRADA *\n\n"
      for key in sorted(sol):
        if not sol[key]:
          continue
        if type(sol[key]) == type([]):
          value = sol[key][1]
        else:
          value = sol[key]
        description += "  - %s: %s\n" % (key, value)
      vals = {
        'name': 'CS: %s CUPS: %s' % (sol['ref_sollicitud'], sol['cups_sollicitat']),
        'section_id': section.id,
        'description': description,
      }
      # Mirem si ja hi ha un cas creat amb aquest nom
      c_ids = crm_obj.search(cr, uid, [('name', '=', vals['name'])])
      if not len(c_ids):
        crm_obj.create(cr, uid, vals)

    return []


gisceweb_sync_cases()
