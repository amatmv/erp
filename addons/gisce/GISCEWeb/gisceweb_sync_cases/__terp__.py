# -*- coding: utf-8 -*-
{
    "name": "GISCEWeb Sync Cases",
    "description": """
Sincronització dels casos que tenim pendents d'atendre a través del canal Web. I així portar el control a través de l'ERP corporatiu i no des del Canal web.
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEWeb",
    "depends":[
        "base",
        "gisceweb_sync_client",
        "crm"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "gisceweb_sync_cases_view.xml"
    ],
    "active": False,
    "installable": True
}
