# -*- coding: utf-8 -*-
{
    "name": "GISCEWeb Sync Cases AMRPS (P0)",
    "description": """
Sincronització dels casos del proces P0.
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEWeb",
    "depends":[
        "base",
        "gisceweb_sync_cases",
        "crm"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "gisceweb_sync_cases_amrps_data.xml",
        "gisceweb_sync_cases_amrps_view.xml"
    ],
    "active": False,
    "installable": True
}
