# -*- coding: utf-8 -*-
{
    "name": "Giscedata Stock Agri Energia",
    "description": """Aquest mòdul afegeix les següents funcionalitats (Custom):
- Renombrar "Fila, Nivell, Columna" per "Passadís, Fila, prestatge"
- Incloure els mòduls necessaris per gestió de Stock AE
  - Productes
  - Fabricants de productes
  - Caracteristiques de productes
  - Stock
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends": [
        'product',
        'product_fabricant',
        'product_caracteristiques',
        'giscedata_stock',
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "product_view.xml",
        'wizard/wizard_inventory.xml'
    ],
    "active": False,
    "installable": True
}
