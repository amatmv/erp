# -*- encoding: utf-8 -*-
from osv import fields, osv
from tools.translate import _


class fill_inventory(osv.osv_memory):
    """Product Stock Location List """
    _name = "stock.wizard.fill.inventory"
    _inherit = "stock.wizard.fill.inventory"

    _columns = {
        'zone': fields.char('Line', size=32),
        'line': fields.char('Fila', size=32),
        'column': fields.char('Passadís', size=32),
        'level': fields.char('Prestatge', size=32),
    }

    def filter_product(
        self, cursor, uid, wizard_id, product_id, wizard_data, context=None
    ):
        res = super(fill_inventory, self).filter_product(
            cursor, uid, wizard_id, product_id, wizard_data, context
        )
        if res and wizard_data['zone']:
            product_obj = self.pool.get('product.product')
            zona = product_obj.read(cursor, uid, product_id,
                                    ['default_location'])['default_location']
            if wizard_data['zone'] != zona:
                return False
        return res


fill_inventory()
