# -*- encoding: utf-8 -*-
from osv import fields, osv


class product_template(osv.osv):
    """Producte"""

    _name = "product.template"
    _inherit = "product.template"

    _columns = {
        'default_location': fields.char('Zona', size=32),
        'loc_rack': fields.char('Passadís', size=32),
        'loc_row': fields.char('Fila', size=32),
        'loc_case': fields.char('Prestatge', size=32),
    }


product_template()
