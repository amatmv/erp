# -*- coding: utf-8 -*-
{
    "name": "Facturació Comercialitzadora",
    "description": """Facturació Agrienergia Comercialitzadora""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "AgriEnergia",
    "depends": [
        "giscedata_facturacio_comer",
        "giscedata_facturacio_comer_bono_social",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "giscedata_facturacio_comer_agri_report.xml",
        "account_journal_data.xml",
        "giscedata_polissa_view.xml",
        "giscedata_facturacio_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
