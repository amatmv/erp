# -*- coding: utf-8 -*-
from osv import osv


class GiscedataFacturacioFacturaReport(osv.osv):

    _auto = False
    _name = "giscedata.facturacio.factura.report"
    _inherit = "giscedata.facturacio.factura.report"

    def get_policy_info(self, cursor, uid, ids, context=None):
        """
        Obtains the policy info related to the invoice.
        :param cursor:
        :param uid:
        :param ids: <giscedata.facturacio.factura> ids
        :param context:
        :return: Dictionary where the key is the invoice id and the value is a
            dictionary with the following structure:
                - general_conditions (optional): general conditions name.
                - {other values from get_policy_info found at comer base}
        """
        if not isinstance(ids, list):
            ids = [ids]

        res = super(GiscedataFacturacioFacturaReport, self).get_policy_info(
            cursor, uid, ids, context=context
        )

        factura_o = self.pool.get('giscedata.facturacio.factura')

        factura_f = ['id', 'polissa_id.condicions_generals_id.name']
        dmn = [('id', 'in', ids)]
        factura_vs = factura_o.q(cursor, uid).read(factura_f).where(dmn)

        for factura_v in factura_vs:
            factura_id = factura_v['id']
            res[factura_id].update({
                'general_conditions': factura_v['polissa_id.condicions_generals_id.name']
            })

        return res

    def get_invoice_info(self, cursor, uid, ids, context=None):
        """
        Obtains the invoice info.
        :param cursor:
        :param uid:
        :param ids: <giscedata.facturacio.factura> ids
        :param context:
        :return: Dictionary where the key is the invoice id and the value is a
            dictionary with the following structure:
                - atr_amount: <<coste de los peajes>>.
                - {other values from get_invoice_info found at comer base}
        """
        if not isinstance(ids, list):
            ids = [ids]

        res = super(GiscedataFacturacioFacturaReport, self).get_invoice_info(
            cursor, uid, ids, context=context
        )
        polissa_o = self.pool.get('giscedata.polissa')
        factura_o = self.pool.get('giscedata.facturacio.factura')
        fields_to_read = ['total_atr', 'variant_id', 'descompte_variants', 'polissa_id']
        factura_vs = factura_o.read(
            cursor, uid, ids, fields_to_read, context=context
        )

        for factura_v in factura_vs:
            factura_id = factura_v['id']
            polissa_id = factura_v['polissa_id'][0]
            lang_partner = polissa_o.get_idioma(
                cursor, uid, [polissa_id], context=context
            )[polissa_id]
            res[factura_id].update({
                'atr_amount': factura_v['total_atr'],
                'variant_id': factura_v['variant_id'],
                'lang_partner': lang_partner,
                'descompte_variants': factura_v['descompte_variants']
            })

        return res


GiscedataFacturacioFacturaReport()
