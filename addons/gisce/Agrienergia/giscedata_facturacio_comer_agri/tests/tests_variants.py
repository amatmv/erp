# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction

from giscedata_polissa.tests import utils as utils_polissa
from giscedata_facturacio.tests.utils import create_fiscal_year
from osv.orm import ValidateException

from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta


class TestsVariants(testing.OOTestCase):

    def setUp(self):
        self.pool = self.openerp.pool
        self.imd_obj = self.pool.get('ir.model.data')
        self.variant_obj = self.pool.get('giscedata.variants')
        self.fact_obj = self.pool.get('giscedata.facturacio.factura')
        self.polissa_obj = self.pool.get('giscedata.polissa')
        self.wiz_obj = self.pool.get('wizard.manual.invoice')
        self.journal_obj = self.pool.get('account.journal')
        self.lectura_obj = self.pool.get('giscedata.lectures.lectura')
        self.lectura_pot_obj = self.pool.get('giscedata.lectures.potencia')
        self.tax_obj = self.pool.get('account.tax')

        self.openerp.install_module(
            'giscedata_tarifas_peajes_20180101'
        )
        self.openerp.install_module(
            'giscedata_tarifas_pagos_capacidad_20180101'
        )
        self.openerp.install_module(
            'giscedata_tarifas_peajes_20190101'
        )
        self.openerp.install_module(
            'giscedata_tarifas_pagos_capacidad_20190101'
        )

        self.START_DATE_VARIANT_1 = '2018-01-01'
        self.END_DATE_VARIANT_1 = '2019-12-31'
        self.INVOICE_START_DATE = '2018-12-01'
        self.INVOICE_END_DATE = '2019-02-01'

        self.txn = Transaction().start(self.database)

    def tearDown(self):
        self.txn.stop()

    def setup_contract(self, cursor, uid, context=None):
        if context is None:
            context = {}
        self.import_data(cursor, uid)
        polissa = self.polissa_obj.browse(
            cursor, uid, self.polissa_id
        )

        tarifa = self.imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'tarifa_20DHA_new'
        )[1]

        res_obj = self.pool.get('res.config')
        res_id = res_obj.search(cursor, uid, [
            ('name', '=', 'fact_uom_cof_to_multi')])
        res_obj.write(cursor, uid, res_id[0], {'value': '0'})

        factor_diari = self.imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'uom_pot_elec_dia'
        )[1]

        polissa.write({
            'potencia': context.get('potencia', 2),
            'llista_preu': self.pricelist_id,
            'tarifa': tarifa,
            'property_unitat_potencia': factor_diari
        })
        polissa.send_signal([
            'validar', 'contracte'
        ])

        # Borramos todos los contadores activos para evitar interferencias
        for comptador in polissa.comptadors:
            for l in comptador.lectures:
                l.unlink(context={})
            for lp in comptador.lectures_pot:
                lp.unlink(context={})
            comptador.write({'lloguer': True, 'preu_lloguer': 0.5})

        comptador = polissa.comptadors[0]
        self.comptador_id = comptador.id

        vals = {
            'name': datetime.strftime(
                (
                    datetime.strptime(self.INVOICE_START_DATE, '%Y-%m-%d') +
                    relativedelta(months=-1)
                ), '%Y-%m-%d'
            ),
            'periode': self.periode_1_id,
            'lectura': 10,
            'tipus': 'A',
            'comptador': comptador.id,
            'observacions': '',
            'origen_id': self.origen_id,
        }
        self.lectura_obj.create(cursor, uid, vals)

        vals = {
            'name': datetime.strftime(
                (
                        datetime.strptime(self.INVOICE_START_DATE, '%Y-%m-%d') +
                        relativedelta(months=-1)
                ), '%Y-%m-%d'
            ),
            'periode': self.periode_2_id,
            'lectura': 10,
            'tipus': 'A',
            'comptador': comptador.id,
            'observacions': '',
            'origen_id': self.origen_id,
        }
        self.lectura_obj.create(cursor, uid, vals)

        vals = {
            'name': datetime.strftime(
                (
                    datetime.strptime(self.INVOICE_END_DATE, '%Y-%m-%d') -
                    timedelta(days=1)
                ), '%Y-%m-%d'
            ),
            'periode': self.periode_1_id,
            'lectura': 200,
            'tipus': 'A',
            'comptador': comptador.id,
            'observacions': '',
            'origen_id': self.origen_id,
        }
        self.lectura_obj.create(cursor, uid, vals)

        vals = {
            'name': datetime.strftime(
                (
                        datetime.strptime(self.INVOICE_END_DATE, '%Y-%m-%d') -
                        timedelta(days=1)
                ), '%Y-%m-%d'
            ),
            'periode': self.periode_2_id,
            'lectura': 200,
            'tipus': 'A',
            'comptador': comptador.id,
            'observacions': '',
            'origen_id': self.origen_id,
        }
        self.lectura_obj.create(cursor, uid, vals)

        utils_polissa.crear_modcon(
            self.pool, cursor, uid, self.polissa_id, {
                'potencies_periode': [
                    (1, polissa.potencies_periode[0].id, {'potencia': 7.000})
                ]
             }, '2018-01-01', '2019-12-31'
        )

        config_taxes_wiz_obj = self.pool.get('config.taxes.facturacio.comer')

        iva_venda = self.tax_obj.search(cursor, uid, [
            ('name', '=', 'IVA 21%'),
            ('type_tax_use', 'ilike', 'sale')
        ])[0]
        iva_compra = self.tax_obj.search(cursor, uid, [
            ('name', 'ilike', '%21% IVA Soportado (operaciones corrientes)%'),
            ('type_tax_use', 'ilike', 'purchase')
        ])[0]
        iese_venda = self.tax_obj.search(cursor, uid, [
            ('name', 'ilike', '%Impuesto especial sobre la electricidad%'),
        ])[0]
        wiz_id = config_taxes_wiz_obj.create(cursor, uid, {
            'iva_venda': iva_venda,
            'iese_venda': iese_venda,
            'iva_compra': iva_compra,
        })
        config_taxes_wiz_obj.action_set(cursor, uid, [wiz_id])

        values = {
            'tipus': '1',
            'polissa_id': context.get('polissa_id', self.polissa_id),
            'data_inici': self.START_DATE_VARIANT_1,
            'data_final': self.END_DATE_VARIANT_1,
        }

        create_fiscal_year(self.pool, cursor, uid, fiscal_year='2018')

        self.variant_obj.create(cursor, uid, values)
        return polissa

    def current_period(self, cursor, uid, date=None):
        if date is None:
            date = datetime.today()
        return self.pool.get('account.period').find(
            cursor, uid, date
        )[0]

    def manual_invoice(self, cursor, uid):
        wiz_id = self.wiz_obj.create(
            cursor, uid, {}
        )
        wiz_fact = self.wiz_obj.browse(
            cursor, uid, wiz_id
        )
        wiz_fact.write({
            'polissa_id': self.polissa_id,
            'date_start': self.INVOICE_START_DATE,
            'date_end': self.INVOICE_END_DATE,
            'journal_id': self.journal_id
        })
        wiz_fact.action_manual_invoice()
        invoice_id = eval(wiz_fact.invoice_ids)[0]

        invoice = self.fact_obj.browse(
            cursor, uid, invoice_id
        )

        self.fact_obj.write(cursor, uid, invoice.id, {
            'period_id': self.current_period(cursor, uid,
                                             date=self.INVOICE_END_DATE)
        })

        wiz_fact.unlink()
        return invoice

    def import_data(self, cursor, uid):
        self.polissa_id = self.imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]
        self.pricelist_id = self.imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio',
            'pricelist_tarifas_electricidad'
        )[1]

        self.periode_1_id = self.imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'p1_e_tarifa_20DHA_new'
        )[1]
        self.periode_2_id = self.imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'p2_e_tarifa_20DHA_new'
        )[1]

        self.origen_id = self.imd_obj.get_object_reference(
            cursor, uid, 'giscedata_lectures', 'origen10'
        )[1]

        self.journal_id = self.journal_obj.search(
            cursor, uid, [('code', '=', 'ENERGIA')]
        )[0]

    def get_payment_lines_variants(self, invoice):
        return filter(
            lambda elem: 'variant' in elem.name,
            invoice.payment_ids
        )

    def check_payments_names(self, invoice):
        for pay_line in self.get_payment_lines_variants(invoice):
            self.assertTrue('variant' in pay_line.read(['name'])[0]['name'])

    def test_calculate_variants_discount_when_creating_invoice(self):
        """
        Checks that the discount is calculated each time a invoice is created
        before being opened.
        The field variant_id and descompte_variant will be set.
        """

        cursor = self.txn.cursor
        uid = self.txn.user

        self.setup_contract(cursor, uid)
        invoice = self.manual_invoice(cursor, uid)

        def get_discount(v_id, inv_id, ctx={}):
            from tools import float_round
            return round(float_round(
                self.fact_obj.get_amount_to_discount(
                    cursor, uid, fact_id=inv_id, context=ctx
                ), 2
            ), 2)

        # The discount must have been calculated
        self.assertTrue(invoice.variant_id)
        self.assertEqual(
            invoice.descompte_variants,
            get_discount(invoice.variant_id.id, invoice.id)
        )

        # We erase the invoice to change the variant and re-calculate the
        # discount
        self.fact_obj.unlink(cursor, uid, [invoice.id])

        self.variant_obj.write(cursor, uid, [invoice.variant_id.id], {
            'tipus': '2',
        })

        invoice = self.manual_invoice(cursor, uid)
        self.assertTrue(invoice.variant_id)
        self.assertEqual(
            invoice.descompte_variants,
            get_discount(invoice.variant_id.id, invoice.id)
        )

        self.fact_obj.unlink(cursor, uid, [invoice.id])

        self.variant_obj.write(cursor, uid, [invoice.variant_id.id], {
            'tipus': '3',
        })

        invoice = self.manual_invoice(cursor, uid)
        self.assertTrue(invoice.variant_id)
        self.assertEqual(
            invoice.descompte_variants,
            get_discount(invoice.variant_id.id, invoice.id)
        )

    def test_payment_is_done_when_opening_invoice(self):
        """
        Checks that the partial payment is done correctly when opening a invoice
        """

        cursor = self.txn.cursor
        uid = self.txn.user

        self.setup_contract(cursor, uid)
        invoice = self.manual_invoice(cursor, uid)

        discount_before_opening = invoice.descompte_variants

        invoice.write({'type': 'out_invoice'})
        invoice.invoice_open()

        variant_payment_lines = self.get_payment_lines_variants(invoice)

        self.assertEquals(len(variant_payment_lines), 1)
        self.assertEquals(
            discount_before_opening,
            variant_payment_lines[0].credit - variant_payment_lines[0].debit
        )

    def test_payment_is_redone_when_cancelling_and_opening(self):
        """
        Checks that the payment is re-done when cancelling the invoice and
        opening again.
        """
        cursor = self.txn.cursor
        uid = self.txn.user

        self.setup_contract(cursor, uid)
        invoice = self.manual_invoice(cursor, uid)

        invoice.write({'type': 'out_invoice'})
        invoice.invoice_open()
        self.journal_obj.write(cursor, uid, self.journal_id, {
            'update_posted': True
        })

        line_ids = [line.reconcile_partial_id.id for line in invoice.move_lines]
        self.pool.get('account.move.reconcile').unlink(cursor, uid, line_ids)

        invoice.action_cancel_draft()

        undo_payment_wiz_obj = self.pool.get('wizard.undo.payment')
        wiz_id = undo_payment_wiz_obj.create(cursor, uid, {})
        undo_payment_wiz_obj.undo_payments(cursor, uid, wiz_id, context={
            'active_id': invoice.id,
            'active_ids': [invoice.id],
            'model': 'giscedata.facturacio.factura'
        })

        self.assertFalse(self.get_payment_lines_variants(invoice))

        invoice.invoice_open()
        invoice = self.fact_obj.browse(cursor, uid, invoice.id)
        self.assertTrue(self.get_payment_lines_variants(invoice))

    def test_payment_isnt_done_when_variants_has_finished(self):
        """
        Test payment isn't done when variant finishes after the invoice
        """

        cursor = self.txn.cursor
        uid = self.txn.user

        self.setup_contract(cursor, uid)
        invoice = self.manual_invoice(cursor, uid)

        variant_id = self.variant_obj.search(cursor, uid, [
            ('polissa_id', '=', self.polissa_id)
        ])

        self.variant_obj.write(cursor, uid, variant_id, {
            'data_final': datetime.strftime(
                (
                    datetime.strptime(self.INVOICE_END_DATE, '%Y-%m-%d') -
                    timedelta(days=10)
                ), '%Y-%m-%d'
            )
        })

        invoice.write({'type': 'out_invoice'})
        invoice.invoice_open()

        self.assertFalse(self.get_payment_lines_variants(invoice))

    def test_payment_is_done_at_same_day_that_variants_finishes(self):
        """
        Test payment is done when variant finishes the same day as
        the invoice.
        """

        cursor = self.txn.cursor
        uid = self.txn.user

        self.setup_contract(cursor, uid)
        invoice = self.manual_invoice(cursor, uid)

        variant_id = self.variant_obj.search(cursor, uid, [
            ('polissa_id', '=', self.polissa_id)
        ])

        self.variant_obj.write(cursor, uid, variant_id, {
            'data_final': self.INVOICE_END_DATE
        })

        invoice.write({'type': 'out_invoice'})
        invoice.invoice_open()

        # We want the payment to be done if the invoices finishes the same day
        # as the variant
        self.assertTrue(self.get_payment_lines_variants(invoice))

    def test_payment_is_done_correctly_with_two_variants(self):
        """
        Test payment is done correctly when there is a variant configuration
        at the start date of the invoice and another at the end date.
        Graphically:
                                        Invoice
                |------------------------------------------------------|
        |-----------------|                                    |------------|
              Variant 1                                                Variant 2
        """

        cursor = self.txn.cursor
        uid = self.txn.user

        self.setup_contract(cursor, uid)
        invoice = self.manual_invoice(cursor, uid)

        variant_id = self.variant_obj.search(cursor, uid, [
            ('polissa_id', '=', self.polissa_id)
        ])

        # Setup the current variant to start before the invoice
        self.variant_obj.write(cursor, uid, variant_id, {
            'data_inici': datetime.strftime(
                (
                    datetime.strptime(self.INVOICE_START_DATE, '%Y-%m-%d') -
                    timedelta(days=3)
                ), '%Y-%m-%d'
            ),
            'data_final': datetime.strftime(
                (
                    datetime.strptime(self.INVOICE_START_DATE, '%Y-%m-%d') +
                    timedelta(days=3)
                ), '%Y-%m-%d'
            ),
        })

        # Create the second config of the variant to start before the end
        # date of the invoice.
        self.variant_obj.create(cursor, uid, {
            'tipus': '1',
            'polissa_id': self.polissa_id,
            'data_inici': datetime.strftime(
                (
                    datetime.strptime(self.INVOICE_END_DATE, '%Y-%m-%d') -
                    timedelta(days=3)
                ), '%Y-%m-%d'
            ),
            'data_final': datetime.strftime(
                (
                    datetime.strptime(self.INVOICE_END_DATE, '%Y-%m-%d') +
                    timedelta(days=3)
                ), '%Y-%m-%d'
            ),
        })

        invoice.write({'type': 'out_invoice'})
        invoice.invoice_open()

        self.assertTrue(self.get_payment_lines_variants(invoice))

    def test_payment_isnt_done_with_two_variants(self):
        """
        Test payment is done correctly when there is a variant configuration
        before the start date of the invoice and another after the end date.
        Graphically would be :
                                     Invoice
                    |-------------------------------------|
        |---------|                                          |----------|
           Variant 1                                               Variant 2
        """

        cursor = self.txn.cursor
        uid = self.txn.user

        self.setup_contract(cursor, uid)
        invoice = self.manual_invoice(cursor, uid)

        variant_id = self.variant_obj.search(cursor, uid, [
            ('polissa_id', '=', self.polissa_id)
        ])

        # Setup the current variant to end before the invoice
        self.variant_obj.write(cursor, uid, variant_id, {
            'data_inici': datetime.strftime(
                (
                    datetime.strptime(self.INVOICE_START_DATE, '%Y-%m-%d') -
                    timedelta(days=15)
                ), '%Y-%m-%d'
            ),
            'data_final': datetime.strftime(
                (
                    datetime.strptime(self.INVOICE_START_DATE, '%Y-%m-%d') -
                    timedelta(days=3)
                ), '%Y-%m-%d'
            ),
        })

        # Create the second config of the variant to start after the end
        # date of the invoice.
        self.variant_obj.create(cursor, uid, {
            'tipus': '1',
            'polissa_id': self.polissa_id,
            'data_inici': datetime.strftime(
                (
                    datetime.strptime(self.INVOICE_END_DATE, '%Y-%m-%d') +
                    timedelta(days=3)
                ), '%Y-%m-%d'
            ),
            'data_final': datetime.strftime(
                (
                    datetime.strptime(self.INVOICE_END_DATE, '%Y-%m-%d') +
                    timedelta(days=15)
                ), '%Y-%m-%d'
            ),
        })

        invoice.write({'type': 'out_invoice'})
        invoice.invoice_open()

        self.assertFalse(self.get_payment_lines_variants(invoice))

    def test_old_invoice_is_bonused(self):
        """
        Test payment is done correctly when there is a variant configuration
        after the one matching the final date of the invoice. This can be when
        we want to bonus an old invoice, for instance a rectifying invoice.
        Graphically would be:
                            Invoice
            |-------------------------------------|
                                             |---------||----------|
                                                Variant 1     Variant 2
        """

        cursor = self.txn.cursor
        uid = self.txn.user

        self.setup_contract(cursor, uid)
        invoice = self.manual_invoice(cursor, uid)

        variant_id = self.variant_obj.search(cursor, uid, [
            ('polissa_id', '=', self.polissa_id)
        ])

        self.variant_obj.write(cursor, uid, variant_id, {
            'data_inici': datetime.strftime(
                (
                    datetime.strptime(self.INVOICE_END_DATE, '%Y-%m-%d') -
                    timedelta(days=15)
                ), '%Y-%m-%d'
            ),
            'data_final': datetime.strftime(
                (
                    datetime.strptime(self.INVOICE_END_DATE, '%Y-%m-%d') +
                    timedelta(days=10)
                ), '%Y-%m-%d'
            ),
        })

        # Create the second config of the variant to start after the end
        # date of the invoice.
        self.variant_obj.create(cursor, uid, {
            'tipus': '1',
            'polissa_id': self.polissa_id,
            'data_inici': datetime.strftime(
                (
                    datetime.strptime(self.INVOICE_END_DATE, '%Y-%m-%d') +
                    timedelta(days=11)
                ), '%Y-%m-%d'
            ),
            'data_final': datetime.strftime(
                (
                    datetime.strptime(self.INVOICE_END_DATE, '%Y-%m-%d') +
                    timedelta(days=30)
                ), '%Y-%m-%d'
            ),
        })

        invoice.write({'type': 'out_invoice'})
        invoice.invoice_open()

        self.check_payments_names(invoice)

    def test_old_invoice_is_rectified(self):
        """
        Graphically would be:
              Invoice (2018)   2019      Random Inv      Refund and rectifying
        |---------------------| | |--------------------| |-----------------|
                                                         |-----------------|

        |---------------------| |-------------------------------------------|
              Variant 2018                      Variant 2019
        """

        cursor = self.txn.cursor
        uid = self.txn.user

        self.setup_contract(cursor, uid)

        pol = self.polissa_obj.browse(cursor, uid, self.polissa_id)

        pol.comptadors[0].lectures[0].write({
            'name': '2018-10-31',
            'lectura': 200,
        })
        pol.comptadors[0].lectures[1].write({
            'name': '2018-10-31',
            'lectura': 200,
        })
        pol.comptadors[0].lectures[2].write({
            'name': '2018-11-30',
            'lectura': 350,
        })
        pol.comptadors[0].lectures[3].write({
            'name': '2018-11-30',
            'lectura': 350,
        })

        self.lectura_obj.create(cursor, uid, {
            'name': '2018-12-31',
            'periode': self.periode_1_id,
            'lectura': 500,
            'tipus': 'A',
            'comptador': pol.comptadors[0].id,
            'origen_id': self.origen_id,
        })
        self.lectura_obj.create(cursor, uid, {
            'name': '2018-12-31',
            'periode': self.periode_2_id,
            'lectura': 500,
            'tipus': 'A',
            'comptador': pol.comptadors[0].id,
            'origen_id': self.origen_id,
        })

        self.lectura_obj.create(cursor, uid, {
            'name': '2019-03-31',
            'periode': self.periode_1_id,
            'lectura': 750,
            'tipus': 'A',
            'comptador': pol.comptadors[0].id,
            'origen_id': self.origen_id,
        })
        self.lectura_obj.create(cursor, uid, {
            'name': '2019-03-31',
            'periode': self.periode_2_id,
            'lectura': 750,
            'tipus': 'A',
            'comptador': pol.comptadors[0].id,
            'origen_id': self.origen_id,
        })

        self.lectura_obj.create(cursor, uid, {
            'name': '2019-04-30',
            'periode': self.periode_1_id,
            'lectura': 980,
            'tipus': 'A',
            'comptador': pol.comptadors[0].id,
            'origen_id': self.origen_id,
        })
        self.lectura_obj.create(cursor, uid, {
            'name': '2019-04-30',
            'periode': self.periode_2_id,
            'lectura': 980,
            'tipus': 'A',
            'comptador': pol.comptadors[0].id,
            'origen_id': self.origen_id,
        })

        variant_id_2018 = self.variant_obj.search(cursor, uid, [
            ('polissa_id', '=', self.polissa_id)
        ])

        self.variant_obj.write(cursor, uid, variant_id_2018, {
            'data_inici': '2018-01-01',
            'data_final': '2018-12-31'
        })

        # Create the second config of the variant
        variant_id_2019 = self.variant_obj.create(cursor, uid, {
            'tipus': '1',
            'polissa_id': self.polissa_id,
            'data_inici': '2019-01-01',
            'data_final': '2019-12-31'
        })

        self.INVOICE_START_DATE = '2018-11-01'
        self.INVOICE_END_DATE = '2018-11-30'
        old_fact = self.manual_invoice(cursor, uid)
        old_fact.invoice_open()

        self.INVOICE_START_DATE = '2018-12-01'
        self.INVOICE_END_DATE = '2018-12-31'
        fact = self.manual_invoice(cursor, uid)
        fact.invoice_open()

        self.INVOICE_START_DATE = '2019-03-01'
        self.INVOICE_END_DATE = '2019-03-31'
        random_inv = self.manual_invoice(cursor, uid)
        random_inv.invoice_open()

        # We create the rectifier and the refund
        created_invoices = fact.rectificar(tipus='R', context={})
        refund, rectifying = self.fact_obj.browse(cursor, uid, created_invoices)
        period_id = self.current_period(cursor, uid)
        refund.write({'period_id': period_id})
        rectifying.write({'period_id': period_id})
        refund.invoice_open()
        rectifying.invoice_open()

        self.check_payments_names(old_fact)

        self.check_payments_names(fact)

        self.check_payments_names(random_inv)

        self.check_payments_names(refund)

        self.check_payments_names(rectifying)

        self.assertEquals(old_fact.variant_id.id, variant_id_2018[0])
        self.assertEquals(fact.variant_id.id, variant_id_2018[0])
        self.assertEquals(random_inv.variant_id.id, variant_id_2019)
        self.assertEquals(refund.variant_id.id, variant_id_2018[0])
        self.assertEquals(rectifying.variant_id.id, variant_id_2018[0])

        context = {
            'exclude_ids': [fact.id],
        }

        res = self.polissa_obj.get_anual_consume(
            cursor, uid, self.polissa_id, date=fact.date_invoice,
            context=context
        )
        self.assertTrue(res[0] > 0)

    def test_check_accumulated_consume(self):
        """
        Graphically would be:
        2018       Inv.1                  Inv.2                 Inv.3
         |--------------------| |--------------------| |--------------------|
        """

        cursor = self.txn.cursor
        uid = self.txn.user

        self.setup_contract(cursor, uid)

        pol = self.polissa_obj.browse(cursor, uid, self.polissa_id)

        # Fact 1
        pol.comptadors[0].lectures[0].write({
            'name': '2018-08-31',
            'lectura': 200,
        })
        pol.comptadors[0].lectures[1].write({
            'name': '2018-08-31',
            'lectura': 200,
        })

        pol.comptadors[0].lectures[2].write({
            'name': '2018-09-30',
            'lectura': 2200,
        })
        pol.comptadors[0].lectures[3].write({
            'name': '2018-09-30',
            'lectura': 1400,
        })

        # Fact 2
        self.lectura_obj.create(cursor, uid, {
            'name': '2018-10-31',
            'periode': self.periode_1_id,
            'lectura': 4700,
            'tipus': 'A',
            'comptador': pol.comptadors[0].id,
            'origen_id': self.origen_id,
        })
        self.lectura_obj.create(cursor, uid, {
            'name': '2018-10-31',
            'periode': self.periode_2_id,
            'lectura': 1900,
            'tipus': 'A',
            'comptador': pol.comptadors[0].id,
            'origen_id': self.origen_id,
        })

        # Fact 3
        self.lectura_obj.create(cursor, uid, {
            'name': '2018-11-30',
            'periode': self.periode_1_id,
            'lectura': 6500,
            'tipus': 'A',
            'comptador': pol.comptadors[0].id,
            'origen_id': self.origen_id,
        })
        self.lectura_obj.create(cursor, uid, {
            'name': '2018-11-30',
            'periode': self.periode_2_id,
            'lectura': 2500,
            'tipus': 'A',
            'comptador': pol.comptadors[0].id,
            'origen_id': self.origen_id,
        })

        variant_id_2018 = self.variant_obj.search(cursor, uid, [
            ('polissa_id', '=', self.polissa_id)
        ])

        self.variant_obj.write(cursor, uid, variant_id_2018, {
            'data_inici': '2018-01-01',
            'data_final': '2018-12-31'
        })

        self.INVOICE_START_DATE = '2018-09-01'
        self.INVOICE_END_DATE = '2018-09-30'
        inv_1 = self.manual_invoice(cursor, uid)
        inv_1.invoice_open()

        self.INVOICE_START_DATE = '2018-10-01'
        self.INVOICE_END_DATE = '2018-10-31'
        inv_2 = self.manual_invoice(cursor, uid)
        inv_2.invoice_open()

        acc_cons, invoiced_cons = self.polissa_obj.get_anual_consume(
            cursor, uid, self.polissa_id, date='2018-11-01')

        computed_acc_cons = (2000 + 1200 + 2500 + 500)
        self.assertEquals(acc_cons, computed_acc_cons)
        self.assertEquals(invoiced_cons, computed_acc_cons * 0.05)

        self.INVOICE_START_DATE = '2018-11-01'
        self.INVOICE_END_DATE = '2018-11-30'
        inv_3 = self.manual_invoice(cursor, uid)
        inv_3.invoice_open()

        self.check_payments_names(inv_1)
        self.assertEquals(inv_1.consum_a_facturar_p1, 3200*0.05)
        self.assertEquals(inv_1.consum_a_facturar_p2, 0)

        self.check_payments_names(inv_2)
        self.assertEquals(inv_2.consum_a_facturar_p1, 3000 * 0.05)
        self.assertEquals(inv_2.consum_a_facturar_p2, 0)

        self.check_payments_names(inv_3)
        self.assertEquals(inv_3.consum_a_facturar_p1, 2400 * 0.05)
        self.assertEquals(inv_3.consum_a_facturar_p2, 1570)

        res = self.polissa_obj.get_anual_consume(cursor, uid, self.polissa_id,
                                                 date=self.INVOICE_END_DATE)
        self.assertTrue(res[0] > 0)

    def test_overlapped_variants(self):
        """
        Test overlapped variants doesn't create.

        Case 1
                            Variant 1
                         |---------|
                |------------|
                    Variant 2

        Case 2
                            Variant 1
                         |---------|
                              |--------------|
                                   Variant 2

        Case 3
                           Variant 1
                        |---------|
                   |-------------------|
                           Variant 2
        Case 4
                       Variant 1
                   |-------------|
                   |-------------|
                       Variant 2
        """

        cursor = self.txn.cursor
        uid = self.txn.user

        self.setup_contract(cursor, uid)

        variant_id = self.variant_obj.search(cursor, uid, [
            ('polissa_id', '=', self.polissa_id)
        ])

        self.variant_obj.write(cursor, uid, variant_id, {
            'data_inici': datetime.strftime(
                (
                    datetime.strptime(self.START_DATE_VARIANT_1, '%Y-%m-%d') -
                    timedelta(days=15)
                ), '%Y-%m-%d'
            ),
            'data_final': datetime.strftime(
                (
                    datetime.strptime(self.END_DATE_VARIANT_1, '%Y-%m-%d') +
                    timedelta(days=10)
                ), '%Y-%m-%d'
            ),
        })

        # Case 1
        with self.assertRaises(ValidateException):
            self.variant_obj.create(cursor, uid, {
                'tipus': '1',
                'polissa_id': self.polissa_id,
                'data_inici': datetime.strftime(
                    (
                        datetime.strptime(self.START_DATE_VARIANT_1, '%Y-%m-%d') -
                        timedelta(days=5)
                    ), '%Y-%m-%d'
                ),
                'data_final': datetime.strftime(
                    (
                        datetime.strptime(self.START_DATE_VARIANT_1, '%Y-%m-%d') +
                        timedelta(days=5)
                    ), '%Y-%m-%d'
                ),
            })

        # Case 2
        with self.assertRaises(ValidateException):
            self.variant_obj.create(cursor, uid, {
                'tipus': '1',
                'polissa_id': self.polissa_id,
                'data_inici': datetime.strftime(
                    (
                        datetime.strptime(self.END_DATE_VARIANT_1, '%Y-%m-%d') -
                        timedelta(days=5)
                    ), '%Y-%m-%d'
                ),
                'data_final': datetime.strftime(
                    (
                        datetime.strptime(self.END_DATE_VARIANT_1, '%Y-%m-%d') +
                        timedelta(days=5)
                    ), '%Y-%m-%d'
                ),
            })

        # Case 3
        with self.assertRaises(ValidateException):
            self.variant_obj.create(cursor, uid, {
                'tipus': '1',
                'polissa_id': self.polissa_id,
                'data_inici': datetime.strftime(
                    (
                        datetime.strptime(self.START_DATE_VARIANT_1, '%Y-%m-%d') -
                        timedelta(days=5)
                    ), '%Y-%m-%d'
                ),
                'data_final': datetime.strftime(
                    (
                        datetime.strptime(self.END_DATE_VARIANT_1, '%Y-%m-%d') +
                        timedelta(days=5)
                    ), '%Y-%m-%d'
                ),
            })

        # Case 4
        with self.assertRaises(ValidateException):
            self.variant_obj.create(cursor, uid, {
                'tipus': '1',
                'polissa_id': self.polissa_id,
                'data_inici': datetime.strptime(
                    self.START_DATE_VARIANT_1,'%Y-%m-%d'
                ),
                'data_final': datetime.strptime(
                    self.START_DATE_VARIANT_1, '%Y-%m-%d'
                )
            })
