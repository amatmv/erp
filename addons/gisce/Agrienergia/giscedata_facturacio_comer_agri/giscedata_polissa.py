# -*- coding: utf-8 -*-
from osv import osv, fields

from datetime import datetime


class GiscedataPolissa(osv.osv):

    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    @staticmethod
    def max_free_consume_variant_1():
        """
        Max free consume in kWh for the variant 1
        """
        return 6600

    def get_anual_consume(self, cursor, uid, ids, date=None, context=None):
        """
        Given a list of contracts, returns a dict with the total consume (kWh)
        of each one and the consume invoiced to the client, that is, the 5%
        of the total.
        :return: {
            contract_id_1: accumulated_cons_1 (kWh), invoiced_cons_1 (kWh),
            contract_id_2: accumulated_cons_2 (kWh), invoiced_cons_2 (kWh)
        }
        """
        if isinstance(ids, (tuple, list)):
            ids = ids[0]
        polissa_id = ids
        if context is None:
            context = {}

        if date is None:
            date = datetime.today()
        else:
            date = datetime.strptime(date, '%Y-%m-%d')

        year = date.year

        q = self.pool.get('giscedata.facturacio.factura').q(cursor, uid)
        search_params = [
            ('polissa_id', '=', polissa_id),
            ('invoice_id.type', 'like', 'out_%'),
            ('invoice_id.state', 'in', ('open', 'paid')),
            ('data_inici', '>=', '{}-01-01'.format(year)),
            ('data_final', '<=', '{}-12-31'.format(year)),
            ('data_final', '<=', date.strftime('%Y-%m-%d')),
        ]

        exclude_ids = context.get('exclude_ids', [])
        if exclude_ids:
            search_params += [
                ('id', 'not in', exclude_ids)
            ]

        if year == 2019:
            search_params += [
                ('invoice_id.date_invoice', '>=', '2019-03-01')
            ]

        # Diccionari de consums acumulats i regalats del 2019, a data de 1 de
        # març. Indexat per id de contracte, el seu valor és una tupla amb els
        # dos valors.
        consumes = {
            '16222018': 1917,
            '1992005': 833,
            '2161940': 882,
            '22161940': 1862,
            '2302003': 2633,
            '252002': 3340,
            '27321940': 851,
            '28391940': 2199,
            '29861940': 1361,
            '30851940': 2675,
            '3161997': 3476,
            '4131940': 2952,
            '41671940': 1255,
            '47391940': 1237,
            '5421940': 216,
            '5722004': 2816,
            '59781940': 948
        }

        polissa_name = self.read(cursor, uid, polissa_id, ['name'])['name']
        contract_consume = consumes.get(polissa_name)
        if contract_consume:
            res = [contract_consume, int(contract_consume * 0.05)]
        else:
            res = [0, 0]

        read_fields = ['energia_kwh', 'invoice_id.type',
                       'consum_a_facturar_p1', 'consum_a_facturar_p2']
        vals = q.read(read_fields).where(search_params)
        for fact in vals:
            energia_consumida = fact['energia_kwh'] or 0
            energia_facturada = int(fact['consum_a_facturar_p1'] or 0
                                    + fact['consum_a_facturar_p2'] or 0)
            if fact['invoice_id.type'] == 'out_refund':
                energia_consumida *= -1
                energia_facturada *= -1
            res[0] += energia_consumida
            res[1] += energia_facturada

        return res

    def copy_data(self, cursor, uid, id, default=None, context=None):
        """
        Not to copy the tariffs when duplicating contracts.
        """
        if default is None:
            default = {}
        if context is None:
            context = {}
        default_values = {
            'variants': False,
        }
        default.update(default_values)
        res_id = super(GiscedataPolissa, self).copy_data(
            cursor, uid, id, default, context)
        return res_id

    def _search_tariff(self, cursor, uid, obj, name, args, context=None):
        """
        Search for any worker tariff and returns its contract.
        """
        if context is None:
            context = {}
        if not args:
            return [('id', '=', 0)]

        variant_obj = self.pool.get('giscedata.variants')
        field, operator, value = args[0]

        polissa_ids = variant_obj.q(cursor, uid).read(['name', 'polissa_id']).where(
            [('name', operator, '%{}%'.format(value))]
        )

        if polissa_ids:
            polissa_ids = map(lambda res: res['polissa_id'], polissa_ids)
            return [('id', 'in', polissa_ids)]
        return [('id', '=', 0)]

    def _variants(self, cursor, uid, ids, field_name, arg, context=None):
        return dict.fromkeys(ids, False)

    _columns = {
        'variants': fields.one2many(
            'giscedata.variants', 'polissa_id', 'Variants'
        ),
        'buscar_variants': fields.function(
            _variants, type='char', string=u'Variants',
            fnct_search=_search_tariff, method=True
        )
    }


GiscedataPolissa()
