# -*- encoding: utf-8 -*-
from __future__ import absolute_import

from datetime import datetime, timedelta

from giscedata_facturacio.giscedata_facturacio import \
    ALL_RECTIFICATIVE_INVOICE_TYPES
from osv import osv, fields
from tools.translate import _
from .giscedata_polissa import GiscedataPolissa


class GiscedataVariants(osv.osv):
    _name = 'giscedata.variants'

    _variants_types = [
        ('1', u'Variant 1'),
        ('2', u'Variant 2'),
        ('3', u'Variant 3'),
    ]

    _STORE_NAME = {
        'giscedata.variants': (
            lambda self, cr, uid, ids, c=None: ids, ['tipus'], 10
        ),
    }

    def _get_variant_name(self, cursor, uid, ids, field_name, args,
                          context=None):
        if context is None:
            context = {}
        res = dict.fromkeys(ids, '')
        for vals in self.read(cursor, uid, ids, ['tipus']):
            variant_name = False
            for variant_k, variant_desc in self._variants_types:
                if vals['tipus'] == variant_k:
                    variant_name = variant_desc
                    break
            if variant_name:
                res[vals['id']] = variant_name
        return res

    _columns = {
        'name': fields.function(
            _get_variant_name, type='char', string='Nombre',
            size=64, store=_STORE_NAME, method=True
        ),
        'polissa_id': fields.many2one(
            'giscedata.polissa', 'Contracte', required=True
        ),
        'data_inici': fields.date('Data inici', required=True),
        'data_final': fields.date(
            'Data final', help=u'Deixar buit si es vol aplicar indefinidament.'
        ),
        'tipus': fields.selection(
            _variants_types, 'Tipus de Variant', required=True
        )
    }

    _order = 'data_inici desc'

    def _dates_order(self, cursor, uid, ids):
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        dates = self.read(cursor, uid, ids, ['data_inici', 'data_final'])
        for vals in dates:
            if vals['data_final'] and vals['data_inici'] > vals['data_final']:
                return False
        return True

    def _overlapped_variant(self, cursor, uid, ids):
        """
        For each id in ids, this method checks if it's overlapped with any
        other in the same contract. If it does, it returns False.
        :param ids: giscedata.variants ids.
        :return: False if there is any overlapped variant, else True.
        """
        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        for current_variant in self.read(
                cursor, uid, ids, ['polissa_id', 'data_inici', 'data_final']
        ):
            if self._return_overlapped_variant(
                    cursor, uid, current_variant['polissa_id'][0],
                    current_variant['data_inici'],
                    current_variant['data_final']
            ):
                return False
        return True

    def _search_existing_equal_variant(self, cursor, uid, polissa_id,
                                       start_date, final_date):
        """
        It searches a variant with the same dates in the same contract.
        """
        search_params = [
            ('polissa_id', '=', polissa_id),
            ('data_inici', '=', start_date),
            '|',
            ('data_final', '=', False),
            ('data_final', '=', final_date),
        ]
        return self.search(cursor, uid, search_params)

    def _return_overlapped_variant(self, cursor, uid, polissa_id, start_date,
                                   final_date):
        """
        :return: Given a contract, if there is any variant overlapped in it,
                 it returns its id.
        """

        def between(date_1, date_2, date_3):
            """
            :return: date_1 is between date_2 and date_3
            """
            return date_2 <= date_1 <= date_3

        # We get all the variant that apply to this contract
        params = [('polissa_id', '=', polissa_id)]
        variants_on_contract = self.search(
            cursor, uid, params, order='data_inici asc')

        equal_variants = self._search_existing_equal_variant(
            cursor, uid, polissa_id, start_date, final_date)
        if len(equal_variants) > 1:
            # If there are more than one variant equals, there are overlapped
            return True
        non_equal_variants = list(
            set(variants_on_contract) - set(equal_variants))

        # We need the dates of those variant
        variants_existings = self.read(
            cursor, uid, non_equal_variants, ['data_inici', 'data_final']
        )

        # Then, for each of them, we want to check that both start date and
        # end date, aren't between any of the variant of the contract.
        for variant_existing in variants_existings:
            try:
                # We consider a False end date as endless.
                final_date_variant_existing = (
                        variant_existing['data_final'] or '3000-01-01')
                final_date = final_date or '3000-01-01'

                # The start date is between an existing variant
                assert not between(start_date, variant_existing['data_inici'],
                                   final_date_variant_existing)

                # The end date is between an existing variant
                assert not between(final_date, variant_existing['data_inici'],
                                   final_date_variant_existing)

                # Both the start date and the end date is between an existing
                # variant
                assert not (
                        between(variant_existing['data_inici'], start_date,
                                final_date) and
                        between(final_date_variant_existing, start_date,
                                final_date)
                )
            except:
                return variant_existing['id']
        return False

    _constraints = [
        (
            _dates_order,
            'La data de inici no pot ser posterior a la data final.',
            ['data_inici', 'data_final']
        ),
        (
            _overlapped_variant,
            'Hi ha variants amb dates sobre-posades.',
            ['data_inici', 'data_final']
        )
    ]


GiscedataVariants()


class GiscedataFacturacioFactura(osv.osv):

    _name = 'giscedata.facturacio.factura'
    _inherit = 'giscedata.facturacio.factura'

    def do_partial_payment(self, cursor, uid, ids, payment_data=None,
                           context=None):
        """
        Do a payment to the invoice with the values given in the payment_date
        :param cursor: OpenERP cursor
        :type cursor: sql_db.Cursor
        :param uid: User id
        :type uid: long
        :param ids: <giscedata.facturacio.factura> ids
        :type ids: list
        :param payment_data: data that will be used to do the payment
        :type payment_data: dict
        :param context: OpenERP context
        :return: None
        :rtype: NoneType
        """
        if context is None:
            context = {}

        if isinstance(ids, list):
            fact_id = ids[0]
        else:
            fact_id = ids

        if payment_data is None:
            payment_data = {}

        wizard_pay_invoice = self.pool.get('facturacio.pay.invoice')
        ctx = {
            'active_id': fact_id, 'active_ids': [fact_id]
        }

        pay_amount = payment_data.get('amount', 0.0)
        pay_name = payment_data.get('name', 0.0)
        pay_date = payment_data.get('date', False)

        journal_id = payment_data.get('journal_id', False)
        pay_partner_id = payment_data.get('pay_partner_id', False)
        wiz_values = {
            'state': 'init',
            'name': pay_name,
            'amount': pay_amount,
            'journal_id': journal_id
        }
        if pay_date:
            wiz_values.update({'date': pay_date})

        wiz_id = wizard_pay_invoice.create(cursor, uid, wiz_values, context=ctx)

        ctx.update({'pay_partner_id': pay_partner_id})

        wizard_pay_invoice.action_pay_and_reconcile(
            cursor, uid, wiz_id, context=ctx
        )
        return True

    def invoice_open(self, cursor, uid, ids, context=None):
        """
        When the invoice is opened, we want to make a partial payment with the
        amount to discount.
        """

        if context is None:
            context = {}
        if not isinstance(ids, list):
            ids = [ids]

        imd_obj = self.pool.get('ir.model.data')

        fact_ids = super(GiscedataFacturacioFactura, self).invoice_open(
            cursor, uid, ids, context
        )

        # Primer escrivim el variant_id que correspongui
        self.set_variant_id_on_fact(cursor, uid, ids, context=context)

        journal_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio_comer_agri',
            'variants_journal'
        )[1]

        fact_q = self.q(cursor, uid)
        variants = fact_q.read(['variant_id', 'id']).where([('id', 'in', ids)])
        for fact_vals in variants:
            if fact_vals['variant_id']:
                # Obtenim els consums de P1 i P2 a facturar conforme la formula
                invoiced_consume_p1, invoiced_consume_p2 = self.get_variants_kwh_to_invoice(
                    cursor, uid, fact_vals['id']
                )
                self.write(cursor, uid, [fact_vals['id']], {
                    'consum_a_facturar_p1': invoiced_consume_p1,
                    'consum_a_facturar_p2': invoiced_consume_p2
                })

                # Una vegada els hem escrit, obtenim el descompte en euros i
                # fem el pagament
                discount = self.get_amount_to_discount(cursor, uid, fact_vals['id'])

                partner_field = 'invoice_id.company_id.partner_id.id'
                pay_partner_id = fact_q.read([partner_field]).where([
                    ('id', '=', fact_vals['id'])
                ])
                if not pay_partner_id:
                    raise NotImplementedError
                else:
                    pay_partner_id = pay_partner_id[0][partner_field]

                pay_data = {
                    'name': 'Descompte de variant',
                    'amount': discount,
                    'journal_id': journal_id,
                    'pay_partner_id': pay_partner_id,
                }
                self.do_partial_payment(cursor, uid, fact_vals['id'], pay_data)

                self.write(cursor, uid, [fact_vals['id']], {
                    'descompte_variants': discount,
                })

        return fact_ids

    def get_energy_lines_dha(self, cursor, uid, fact, context=None):
        """
        :param fact: browse_record of an invoice that MUST have at least the two
        lines of the DHA tariff.
        :return: two browse records of the lines of the two periods of a DHA
        tariff.
        """
        imd_obj = self.pool.get('ir.model.data')
        prod_obj = self.pool.get('product.product')

        __, categ_20dha = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'categ_e_t20DHA_new')

        p1_search_params = [('name', '=', 'P1'), ('categ_id', '=', categ_20dha)]
        p2_search_params = [('name', '=', 'P2'), ('categ_id', '=', categ_20dha)]
        product_p1 = prod_obj.search(cursor, uid, p1_search_params)[0]
        product_p2 = prod_obj.search(cursor, uid, p2_search_params)[0]

        try:
            line_p1 = next(line for line in fact.linies_energia
                           if line.product_id.id == product_p1)
        except StopIteration:
            raise osv.except_osv('Error', u'La factura no té línia de P1')

        try:
            line_p2 = next(line for line in fact.linies_energia
                           if line.product_id.id == product_p2)
        except StopIteration:
            raise osv.except_osv('Error', u'La factura no té línia de P2')

        return line_p1, line_p2

    @staticmethod
    def get_taxes_lines(fact):
        """"
        Get the lines of the IVA and IESE taxes without applying discounts.
        """
        try:
            iva_line = next(
                line for line in fact.tax_line if 'IVA 21%' in line.name
            )
        except StopIteration:
            raise osv.except_osv('Error', u'La factura no té aplicat l\'IVA')

        try:
            iese_line = next(
                line for line in fact.tax_line
                if 'Impuesto especial sobre la electricidad' in line.name
            )
        except StopIteration:
            raise osv.except_osv('Error',
                                 u'La factura no té aplicat l\'impost sobre '
                                 u'l\'electricitat.')

        return iva_line, iese_line

    @staticmethod
    def discount_per_period(variant):
        if variant in ('1', '2'):
            return .95, .95
        elif variant == '3':
            return .70, .30
        else:
            raise osv.except_osv('Error',
                                 _(u'La variant {} no està a la '
                                   u'selecció.'.format(variant)))

    def get_amount_to_discount(self, cursor, uid, fact_id, context=None):
        """
        Returns the discount of the variant
        :param fact_id: id of the invoice to calculate the discount
        :type fact_id: long
        :return: the discount to be applied (in €)
        :rtype: float
        """

        def calc_iese_amount(iese_ln, amount):
            """
            Returns the iese of the line applied to the base
            :param iese_ln: browse_record of a IESE line
            :param amount: amount over which the IESE will be applied
            :return: amount * %IESE
            """
            tax_compute_dict = {'price_unit': amount}
            exec iese_ln.tax_id.python_compute in tax_compute_dict
            return tax_compute_dict['result']

        if context is None:
            context = {}

        if isinstance(fact_id, (list, tuple)):
            fact_id = fact_id[0]

        fact = self.browse(cursor, uid, fact_id)

        line_p1, line_p2 = self.get_energy_lines_dha(cursor, uid, fact)
        price_p1, price_p2 = line_p1.price_unit_multi, line_p2.price_unit_multi

        to_invoice_eur_p1 = fact.consum_a_facturar_p1 * price_p1
        to_invoice_eur_p2 = fact.consum_a_facturar_p2 * price_p2

        discount_amount = 0.0
        if to_invoice_eur_p1 or to_invoice_eur_p2:
            if not len(fact.linies_potencia) == 1:
                raise osv.except_osv(
                    'Error', u"Hi hauria d'haver només una línia de potència."
                )

            line_power = fact.linies_potencia[0]
            power_to_invoice = line_power.quantity

            power_to_invoice_eur = (
                    power_to_invoice
                    * line_power.price_unit_multi
                    * line_power.multi
            )

            amount_to_invoice_rent = fact.linies_lloguer[0].price_subtotal

            iva_line, iese_line = self.get_taxes_lines(fact)

            base = (amount_to_invoice_rent
                    + power_to_invoice_eur
                    + to_invoice_eur_p1
                    + to_invoice_eur_p2)

            iese_when_discounted = calc_iese_amount(
                iese_line,
                power_to_invoice_eur + to_invoice_eur_p1 + to_invoice_eur_p2
            )

            iva_when_discounted = (
                    iva_line.tax_id.amount * (base + iese_when_discounted))

            discount_amount = (
                    fact.amount_total -
                    (base + iese_when_discounted + iva_when_discounted)
            )

        return discount_amount

    def get_applicable_variant(self, cursor, uid, ids, context=None):
        """
        Returns the id of the variant must be applied to an invoice
        :param cursor: OpenERP cursor
        :type cursor: sql_db.Cursor
        :param uid: User id
        :type uid: long
        :param ids: <giscedata.facturacio.factura> ids
        :type ids: list
        :param context: OpenERP context
        :return: None
        :rtype: NoneType
        :return: {
            fact_id: id of variant,
            fact_id_2: id of variant 2,
            ...
        }
        """

        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        res = dict.fromkeys(ids, False)
        non_rectificative_ids = self.search(cursor, uid, [
            ('id', 'in', ids),
            ('invoice_id.rectificative_type', 'not in',
             ALL_RECTIFICATIVE_INVOICE_TYPES)
        ])
        # First we get the variant_id that corresponds to the non rectificative
        # invoices
        if non_rectificative_ids:
            cursor.execute("""
                SELECT gff.id as fact_id, gv.id as var_id
                FROM giscedata_variants gv
                JOIN giscedata_polissa AS gdp ON gdp.id = gv.polissa_id
                JOIN giscedata_facturacio_factura gff ON gff.polissa_id = gdp.id
                JOIN account_invoice ai ON gff.invoice_id = ai.id
                WHERE gff.id IN %s
                AND gff.data_final BETWEEN gv.data_inici
                AND COALESCE(gv.data_final, '3000-01-01')
                AND ai.type LIKE 'out_%%'
                ORDER BY gv.data_final DESC
                """, (tuple(non_rectificative_ids),)
                           )
            query_result = cursor.dictfetchall()
            if query_result:
                for variant in query_result:
                    res[variant['fact_id']] = variant['var_id']

        # Then, for the rectificative invoices we will get the variant_id from
        # the invoice that are rectifying
        rectificative_ids = list(set(ids) - set(non_rectificative_ids))
        if rectificative_ids:
            sql = """
            SELECT 
                rectificada.variant_id AS variant_rectificada, 
                rectificativa.id AS rectificativa_id
            FROM giscedata_facturacio_factura rectificativa
            JOIN account_invoice inv_rectificativa 
                ON inv_rectificativa.id=rectificativa.invoice_id
            JOIN account_invoice inv_rectificada 
                ON inv_rectificada.id=inv_rectificativa.rectifying_id
            JOIN giscedata_facturacio_factura rectificada 
                ON rectificada.invoice_id=inv_rectificada.id
            WHERE rectificativa.id IN %s 
            """
            cursor.execute(sql, (tuple(rectificative_ids),))
            variants_rectificativa = cursor.dictfetchall()
            for variant in variants_rectificativa:
                res[variant['rectificativa_id']] = variant[
                    'variant_rectificada']

        return res

    def has_variant(self, cursor, uid, fact_id):
        """
        :return: True if the invoice has a variant applied
        """
        if isinstance(fact_id, (tuple, list)):
            fact_id = fact_id[0]
        q = self.q(cursor, uid)
        res = q.read(['variant_id']).where([('id', '=', fact_id)])
        return bool(res)

    def get_variants_kwh_to_invoice(self, cursor, uid, fact_id, context=None):
        """
        Returns the values that will be invoiced as P1 and P2 concepts.
        :param cursor: OpenERP cursor
        :type cursor: sql_db.Cursor
        :param uid: User id
        :type uid: long
        :param fact_id: <giscedata.facturacio.factura> id
        :type fact_id: long
        :param context: OpenERP context
        :return: the two P1 and P2 concepts
        :rtype: tuple[integer, integer]
        """
        if context is None:
            context = {}

        pol_obj = self.pool.get('giscedata.polissa')
        fact = self.browse(cursor, uid, [fact_id])[0]

        kwh_to_invoice_p1, kwh_to_invoice_p2 = 0, 0

        if fact.variant_id:
            variant_tipus = fact.variant_id.tipus

            percent_p1, percent_p2 = self.discount_per_period(variant_tipus)
            consumed_energy = fact.energia_kwh or 0.0

            if fact.type == 'out_refund':
                kwh_to_invoice_p1 = fact.consum_a_facturar_p1
                kwh_to_invoice_p2 = fact.consum_a_facturar_p2
            elif variant_tipus == '3':
                kwh_to_invoice_p1 = consumed_energy * (1 - percent_p1)
                kwh_to_invoice_p2 = consumed_energy * (1 - percent_p2)
            elif variant_tipus in ('1', '2'):
                line_p1_invoice, line_p2_invoice = self.get_energy_lines_dha(
                    cursor, uid, fact)

                # Si estem rectificant, no volem incloure en l'historic de
                # consums la factura que estem rectificant ni tampoc la que esta
                # abonant
                ctx = context.copy()
                if fact.rectificative_type in ('R', 'RA'):
                    rectifying_id = fact.rectifying_id.id
                    ctx.update({'exclude_ids': [rectifying_id]})

                kwh_to_invoice_p1 = max(20, (
                    line_p1_invoice.quantity * (1 - percent_p1)
                    + line_p2_invoice.quantity * (1 - percent_p2)
                ))
                # If the consume is less than 20, the kwh to invoice will be
                # that consume
                kwh_to_invoice_p1 = round(
                    consumed_energy if consumed_energy < 20
                    else kwh_to_invoice_p1
                )

                if variant_tipus == '1':
                    max_free_kwh = GiscedataPolissa.max_free_consume_variant_1()

                    consume_date = (datetime.strptime(fact.data_inici, '%Y-%m-%d')
                                    - timedelta(days=1)).strftime('%Y-%m-%d')
                    accumulated_total, accumulated_invoiced = pol_obj.get_anual_consume(
                        cursor, uid, fact.polissa_id.id, date=consume_date,
                        context=context
                    )

                    free_current_invoice = max(0, int(
                        max_free_kwh - (accumulated_total - accumulated_invoiced)
                    ))

                    kwh_to_invoice_p2 = max(0, int(
                        consumed_energy - kwh_to_invoice_p1 - free_current_invoice
                    ))
                else:
                    kwh_to_invoice_p2 = consumed_energy - kwh_to_invoice_p1

        return kwh_to_invoice_p1, kwh_to_invoice_p2

    def set_variant_id_on_fact(self, cursor, uid, ids, context=None):
        """
        Set the field variant_id with the corresponding value
        :param cursor: OpenERP cursor
        :type cursor: sql_db.Cursor
        :param uid: User id
        :type uid: long
        :param ids: <giscedata.facturacio.factura> ids
        :type ids: list
        :param context: OpenERP context
        :return: None
        :rtype: NoneType
        """
        if context is None:
            context = {}

        variants_applicable = self.get_applicable_variant(
            cursor, uid, ids
        )
        for fact_id, variant_id in variants_applicable.items():
            self.write(cursor, uid, fact_id, {
                'variant_id': variant_id
            })

    _columns = {
        'variant_id': fields.many2one('giscedata.variants', 'Variant aplicada',
                                      ondelete='restrict'),
        'descompte_variants': fields.float(
            'Import descomptat (€)', digits=(16, 2)),
        'consum_a_facturar_p1': fields.integer('Consum a facturar P1 (kWh)'),
        'consum_a_facturar_p2': fields.integer('Consum a facturar P2 (kWh)'),
    }

    _defaults = {
        'consum_a_facturar_p1': lambda *args: 0.0,
        'consum_a_facturar_p2': lambda *args: 0.0,
    }


GiscedataFacturacioFactura()


class GiscedataFacturacioFacturador(osv.osv):
    _name = 'giscedata.facturacio.facturador'
    _inherit = 'giscedata.facturacio.facturador'

    def fact_via_lectures(self, cursor, uid, polissa_id, lot_id, context=None):
        if context is None:
            context = {}
        fact_ids = super(GiscedataFacturacioFacturador, self).fact_via_lectures(
            cursor, uid, polissa_id, lot_id, context=context
        )

        fact_obj = self.pool.get('giscedata.facturacio.factura')

        fact_obj.set_variant_id_on_fact(cursor, uid, fact_ids, context=context)

        fact_q = fact_obj.q(cursor, uid)
        variants = fact_q.read(['variant_id']).where([('id', 'in', fact_ids)])
        for fact_values in variants:
            if fact_values['variant_id']:
                invoiced_consume_p1, invoiced_consume_p2 = fact_obj.get_variants_kwh_to_invoice(
                    cursor, uid, fact_values['id']
                )
                fact_obj.write(cursor, uid, [fact_values['id']], {
                    'consum_a_facturar_p1': invoiced_consume_p1,
                    'consum_a_facturar_p2': invoiced_consume_p2
                })

                discount = fact_obj.get_amount_to_discount(
                    cursor, uid, fact_values['id']
                )
                fact_obj.write(cursor, uid, [fact_values['id']], {
                    'descompte_variants': discount,
                })

        return fact_ids


GiscedataFacturacioFacturador()
