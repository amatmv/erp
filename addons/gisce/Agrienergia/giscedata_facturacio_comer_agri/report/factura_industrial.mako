## -*- coding: utf-8 -*-
<%
    from dateutil import parser
    from datetime import datetime
    from base_iban.base_iban import shadow_iban
    from bankbarcode.cuaderno57 import Recibo507
    from giscedata_facturacio_comer.report.utils import clean_nif, erp_date_to_dmy
    import json

    report_o = pool.get('giscedata.facturacio.factura.report')
    banner_obj = pool.get('report.banner')
    sup_territorials_tec_271_comer_obj = pool.get(
    'giscedata.suplements.territorials.2013.tec271.comer'
    )
    fact_obj = pool.get('giscedata.facturacio.factura')
    polissa_obj = pool.get('giscedata.polissa')
    partner_obj = pool.get('res.partner')

    colors = {
        'accumulated': '#cedc00',
        'P1': '#cedc00',
        'P2': '#DDE840',
        'P3': '#D6E324',
        'P4': '#EEF688',
        'P5': '#E5EE5F',
        'P6': '#faffb7'
    }

    def ae_formatted_cups(cups):
        numeric_block = ""
        first_block = cups[:2]
        fourth_block = ""
        flag = True
        for c in cups[2:]:
            if c.isdigit() and flag:
                numeric_block += c
            else:
                flag = False
                fourth_block += c

        second_block = numeric_block[:-6]
        third_block = numeric_block[-6:]

        return "{}{}<b>{}</b>{}".format(
            first_block, second_block, third_block, fourth_block
        )

    iban_indexes_to_hide = [2, 3, 12, 13, -3, -2, -1]
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
##         <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer/report/factura.css"/>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_agri/report/estils_agri.css"/>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_agri/report/factura_industrial.css"/>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_agri/report/assets/pie-chart.css"/>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_agri/report/assets/consumption-chart.css"/>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_agri/report/assets/bar-plot.css"/>
        <script src="${assets_path}/js/d3.min.js"></script>
        <script src="${assets_path}/js/c3.min.js"></script>
        <script src="${assets_path}/js/formatNumber.js"></script>
        <script src="${addons_path}/giscedata_facturacio_comer_agri/report/assets/bar-plot.js"></script>
        <style>
            @font-face {
                font-family: "Roboto-Regular";
                src: url("${assets_path}/fonts/Roboto/Roboto-Regular.ttf") format('truetype');
                font-weight: normal;
            }
        </style>
    </head>
    <%
        # Company information
        comer = report_o.get_company_info(cursor, uid, company.id)[company.id]
        lateral_info =  comer['lateral_info'] or ''
    %>
    <body>
        %for factura in factura_data:
            <%
                language = factura['lang_partner'] or 'ca_ES'
                setLang(language)

                median_consumption_mssg = _(
                    u"El seu consum mitjà diari en el període facturat ha sigut de {} € "
                    u"que corresponent a {} kWh/dia ({} dies)"
                )

                periods_translations = {
                    'P1': _(u"Període 1"),
                    'P2': _(u"Període 2"),
                    'P3': _(u"Període 3"),
                    'P4': _(u"Període 4"),
                    'P5': _(u"Període 5"),
                    'P6': _(u"Període 6"),
                }

                footer_invoicing_detail_mssg = _(
                    u"La factura inclou {}€ corresponents al peatge d'accés dels que un {}%"
                    u"corresponen al terme de potència i un {}% al terme d'energia. "
                    u"Regulats per l'Ordre ETU/1282/2017. Així mateix inclou {}€ del "
                    u"lloguer de l'equip de mesura."
                )

                code_bar_descr_mssg = _(
                    u"A través de qualsevol de les següents entitats bancàries "
                    u"(Banc Santander, Banco Popular, La Caixa), en el seu horari de "
                    u"pagament, presentant aquest escrit a finestreta fins el dia:<br/> "
                    u"<span class=\"red\"><b>Termini de pagament: {}</b></span><br/>i (BBVA), en el caixer automàtic a "
                    u"qualsevol hora.<br/>Si sou client d'alguna d'aquestes entitats, podeu "
                    u"fer el pagament a través de les seves oficines on-line les 24h."
                )

                contact = factura['contact']
                detailed_lines = factura['detailed_lines']
                historic = factura['historic']
                historical = historic['historic']
                holder = factura['holder']
                invoiced_power = factura['invoiced_power']
                periods_and_readings = factura['periods_and_readings']
                periods = periods_and_readings['periods']
                periods_a = periods['active']
                periods_r = periods['reactive']
                periods_m = periods['maximeter']
                readings = periods_and_readings['readings']
                readings_a = readings['active']
                readings_r = readings['reactive']
                readings_m = readings['maximeter']
                pie = factura['pie']
                pie_regulats = pie['regulats']
                pie_impostos = pie['impostos']
                pie_costos = pie['costos']
                dades_reparto = pie['dades_reparto']
                for repartiment in dades_reparto:
                    repartiment[3] = formatLang(repartiment[3])
                policy = factura['policy']
                power_excess_and_other_detailed_info = factura['power_excess_and_other_detailed_info']
                other_lines = power_excess_and_other_detailed_info['other_lines']
                meters_rent = power_excess_and_other_detailed_info['meters_rent']
                power_lines_with_excess = power_excess_and_other_detailed_info['lines_with_power_excess']
                supplier = factura['supplier']
                access_contract = supplier['ref'] or ''
                taxes = factura['taxes']

                median_consumption_full_mssg = median_consumption_mssg.format(
                    formatLang(detailed_lines['energy']['total']/(factura['days'] or 1.0)),
                    formatLang(factura['diari_factura_actual_kwh']),
                    factura['days'] or 1
                )

                total_energy_toll = 0
                total_power_toll = 0

                periods_power = policy['periods_power']
                periods_power_str = ""
                for period in sorted(periods_power):
                    periods_power_str += "<b>{}:</b> {} &nbsp&nbsp&nbsp".format(period, formatLang(periods_power[period], digits=3))

                due_date_erp_formated = datetime.strptime(factura['due_date'], '%d/%m/%Y').strftime("%Y-%m-%d")

                recibo507 = Recibo507(
                    comer['entity'], '500',
                    factura['ref_bar_code'], '000000',
                    '{0:.2f}'.format(factura['amount']), due_date_erp_formated
                )

                factura_date = datetime.strptime(factura['date'], '%d/%m/%Y').strftime('%Y-%m-%d')

                image_first_page = banner_obj.get_banner(
                    cursor, uid, 'giscedata.facturacio.factura',
                    factura_date, code='main_banner_industrial'
                )
                image_logo_code_bar = banner_obj.get_banner(
                    cursor, uid, 'giscedata.facturacio.factura',
                    factura_date, code='logo_code_bar'
                )
                image_environmental_impact = banner_obj.get_banner(
                    cursor, uid, 'giscedata.facturacio.factura',
                    factura_date, code='environmental_impact_industrial'
                )
                image_energy_origin = banner_obj.get_banner(
                    cursor, uid, 'giscedata.facturacio.factura',
                    factura_date, code='energy_origin_industrial'
                )

                columns, y_axis = report_o.historic_js_c3_formatted(
                    historic['historic_js'], True
                )

                cups_ae_formatted = ae_formatted_cups(factura['cups']['name'])

                general_conditions = policy.get('general_conditions', '')
                if general_conditions:
                    for i in range(0,40):
                        general_conditions += '&nbsp;'

                transferencia = factura['payment_type_code'] in ('TRANSFERENCIA_CSB', 'TRANSFERENCIA', 'FINESTRETA')
                code_bar_descr_full_mssg = code_bar_descr_mssg.format(factura['due_date'])

                pricelist = factura['pricelist']
                if '6' not in factura['tariff']:
                    pricelist = pricelist.split(" ")[0]

                forma_pagament = factura['payment_type'].lower()
                forma_pagament = _(u"cobrament al comptat") if forma_pagament == 'transferencia' else forma_pagament

                total_meter_rent = 0
                for meter_rent in meters_rent:
                    total_meter_rent += meter_rent['amount']
            %>
            <div id="lateral-info" class="center">${general_conditions}${lateral_info}</div>
            <div id="first-page">
                <!-- LOGO -->
                <div id="logo" class="left-50">
                    <img src="data:image/jpeg;base64,${comer['logo']}"/>
                </div>
                <!-- END LOGO -->
                <!-- IMPORT FACTURA -->
                <div class="right-50">
                    <table id="import-factura">
                        <tbody>
                            <tr>
                                <th>${_(u"Import factura")}:</th>
                                <td class="bold right">${formatLang(factura['amount'])} €</td>
                            </tr>
                            <tr>
                                <th>${_(u"Data factura")}:</th>
                                <td> ${factura['date']}</td>
                            </tr>
                            <tr>
                                <th>${_(u"Núm. factura")}:</th>
                                <td>${factura.get('name', '')}</td>
                            </tr>
                            <tr>
                                <th>${_(u"Període factura")}:</th>
                                <td> ${factura['init_date']} a ${factura['end_date']}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- END IMPORT FACTURA -->
                <!-- DADES CONTRACTE & SUBMINISTRAMENT -->
                <div class="left-50">
                    <!-- DADES CONTRACTE -->
                    <table id="dades-contracte" class="styled-table">
                        <thead>
                            <tr>
                                <th colspan="2">${_(u"DADES DEL CONTRACTE")}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="bold">${_(u"Titular")}:</td>
                                <td>${holder['name']}</td>
                            </tr>
                            <tr>
                                <td class="bold">${_(u"CIF/NIF")}:</td>
                                <td>${holder['vat']}</td>
                            </tr>
                            <tr>
                                <td class="bold">${_(u"Contracte")}:</td>
                                <td>${policy['name']}</td>
                            </tr>
                            <tr>
                                <td class="bold">${_(u"Adreça fiscal")}:</td>
                                <td>${factura['address']['street']} ${factura['address']['zip']} ${factura['address']['municipi']}</td>
                            </tr>
                        </tbody>
                    </table>
                    <!-- END DADES CONTRACTE -->
                    <!-- DADES SUBMINISTRAMENT -->
                    <table id="dades-subministrament" class="styled-table">
                        <colgroup>
                            <col width="25%"/>
                        </colgroup>
                        <thead>
                            <tr>
                                <th colspan="3">${_(u"DADES DEL SUBMINISTRAMENT")}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="bold">CUPS:</td>
                                <td>${cups_ae_formatted}</td>
                                <td class="bold">${_(u"Tarifa accés")}: ${factura['tariff']}</td>
                            </tr>
                            <tr>
                                <td class="bold">${_(u"Núm. comptador")}:</td>
                                <td>${", ".join(readings_a['readings'].keys())}</td>
                                <td class="bold">${_(u"Energía")} (kWh): ${formatLang(sum(readings_a['total_consumption'].values()), digits=0)}</td>
                            </tr>
                            <tr>
                                <td class="bold">${_(u"Import accés")}</td>
                                <td colspan="2">${formatLang(factura['atr_amount'])} € (${_(u"inclou lloguer equip de mesura")}: ${formatLang(total_meter_rent)} €)</td>
                            </tr>
                            <tr>
                                <td class="bold">${_(u"Adreça submin.")}:</td>
                                <td colspan="2">${factura['cups']['address']}</td>
                            </tr>
                            <tr>
                                <td class="bold">${_(u"Potència")} (kW)</td>
                                <td id="dades-subministrament-potencia" colspan="2">${periods_power_str}</td>
                            </tr>
                            <tr>
                                <td class="bold">${_(u"Contracte accés")}:</td>
                                <td colspan="2">${access_contract}</td>
                            </tr>
                        </tbody>
                    </table>
                    <!-- END DADES SUBMINISTRAMENT -->
                </div>
                <!-- END DADES CONTRACTE & SUBMINISTRAMENT -->
                <!-- FINESTRETA -->
                <div class="right-50">
                    <table id="finestreta">
                        <thead>
                            <tr>
                                <th colspan="2">${contact['name']}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="2">${contact['street'].replace('..', '.')}</td>
                            </tr>
                            <tr>
                                <td colspan="2">${contact['zip']} ${contact['city']}</td>
                            </tr>
                            <tr>
                                <td id="empty-separator-finestreta"></td>
                            </tr>
                            <tr>
                                <td>${_(u"Aclaridor")}:</td>
                                <td>${contact['aclarador']}</td>
                            </tr>
                            <tr>
                                <td>CUPS:</td>
                                <td>${cups_ae_formatted}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- END FINESTRETA -->
                <!-- DADES FACTURACIO -->
                <div id="dades-facturacio">
                    <table class="styled-table">
                        <colgroup>
                            <col width="24%"/>
                            <col/>
                            <col/>
                            <col/>
                            <col/>
                            <col/>
                            <col width="14%"/>
                            <col width="10%"/>
                            <col/>
                            <col width="10%"/>
                        </colgroup>
                        <thead>
                            <tr>
                                <th>${_(u"DADES DE FACTURACIÓ")}</th>
                                <td id="producte-cell" class="bold" colspan="9">${_(u"Producte")}: ${pricelist}</td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="row-separator">
                                <%
                                    rowspan_power_term = len(detailed_lines['power']['lines'])
                                %>
                                <td class="bold" rowspan="${rowspan_power_term}">${_(u"Terme de potència")}</td>
                                % for index, power_line in enumerate(detailed_lines['power']['lines']):
                                    <%
                                        total_power_toll += power_line['toll']
                                    %>
                                    <td class="center italic">${periods_translations.get(power_line['name'], power_line['name'])}</td>
                                    <td>${int(power_line['time'])} dies x</td>
                                    <td class="right white">${formatLang(power_line['consumption'], digits=3)}</td>
                                    <td>kW</td>
                                    <td>x</td>
                                    <td class="right white">${formatLang(power_line['price'], digits=6)}</td>
                                    <td>€/kW-dia</td>
                                    <td>=</td>
                                    <td class="right white bold">${formatLang(power_line['total'])} €</td>
                                    %if index >= 0:
                                        </tr>
                                        <tr>
                                    %endif
                                % endfor
                            </tr>
                            <tr class="row-top-margin">
                                <%
                                    rowspan_energy_term = len(detailed_lines['energy']['lines'])
                                %>
                                <td class="bold" rowspan="${rowspan_energy_term}">${_(u"Terme d'energia activa")}</td>
                                % for index, energy_line in enumerate(detailed_lines['energy']['lines']):
                                    <%
                                        total_energy_toll += energy_line['toll']
                                    %>
                                    <td class="center italic">${periods_translations.get(energy_line['name'], energy_line['name'])}</td>
                                    <td></td>
                                    <td class="right">${formatLang(int(energy_line['consumption']))}</td>
                                    <td>kWh</td>
                                    <td>x</td>
                                    <td class="right">${formatLang(energy_line['price'], digits=6)}</td>
                                    <td>€/kWh</td>
                                    <td>=</td>
                                    <td class="right bold">${formatLang(energy_line['total'])} €</td>
                                    %if index >= 0:
                                        </tr>
                                        <tr>
                                    %endif
                                % endfor
                            </tr>
                            %if len(detailed_lines['reactive']['lines']) > 0:
                                <tr class="row-top-margin">
                                    <%
                                        rowspan_reactive_term = len(detailed_lines['reactive']['lines'])
                                    %>
                                    <td class="bold" rowspan="${rowspan_reactive_term}">${_(u"Reactiva")}</td>
                                    % for index, reactive_line in enumerate(detailed_lines['reactive']['lines']):
                                        <%
                                            total_energy_toll += energy_line['toll']
                                        %>
                                        <td class="center italic">${periods_translations.get(reactive_line['name'], reactive_line['name'])}</td>
                                        <td></td>
                                        <td class="right">${formatLang(int(reactive_line['consumption']))}</td>
                                        <td>kWh</td>
                                        <td>x</td>
                                        <td class="right">${formatLang(reactive_line['price'], digits=6)}</td>
                                        <td>€/kWh</td>
                                        <td>=</td>
                                        <td class="right bold">${formatLang(reactive_line['total'])} €</td>
                                        %if index >= 0:
                                            </tr>
                                            <tr>
                                        %endif
                                    % endfor
                                </tr>
                            %endif
                            %if power_lines_with_excess:
                                <tr class="row-top-margin">
                                    <%
                                        rowspan_excess_term = len(power_lines_with_excess)
                                    %>
                                    <td class="bold" rowspan="${rowspan_excess_term}">${_(u"Terme d'excés")}</td>
                                    % for index, power_line_with_excess in enumerate(power_lines_with_excess):
                                        <%
                                            total_energy_toll += energy_line['toll']
                                        %>
                                        <td class="center italic">${periods_translations.get(power_line_with_excess['name'], power_line_with_excess['name'])}</td>
                                        <td></td>
                                        <td class="right">${formatLang(power_line_with_excess['excess'])}</td>
                                        <td>kWh</td>
                                        <td>x</td>
                                        <td class="right">${formatLang(power_line_with_excess['price'], digits=6)}</td>
                                        <td>€/kWh</td>
                                        <td>=</td>
                                        <td class="right bold">${formatLang(power_line_with_excess['total'])} €</td>
                                        %if index >= 0:
                                            </tr>
                                            <tr>
                                        %endif
                                    % endfor
                                </tr>
                            %endif
                            % for iese_line in taxes['iese']['lines']:
                                <%
                                    # Solucion cutre FTW. No hagais esto en casa niños:
                                    if language == 'ca_ES':
                                        iese_name = iese_line['name'].replace("Impuesto", "Impost").replace("la electricidad", "l'electricitat")
                                    else:
                                        iese_name = iese_line['name']
                                %>
                                <tr class="row-separator">
                                    <td class="bold">${iese_name}</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="right">5,11269632% x</td>
                                    <td class="right">${formatLang(iese_line['base_amount'])} €</td>
                                    <td>=</td>
                                    <td class="bold right">${formatLang(iese_line['tax_amount'])} €</td>
                                </tr>
                            % endfor
                            %for meter_rent in meters_rent:
                                <tr class="row-separator">
                                    <td class="bold">${_(u"Gestió de la mesura")}</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="bold right">${formatLang(meter_rent['amount'])} €</td>
                                </tr>
                            %endfor
                            %if other_lines['lines']:
                                <tr class="row-separator">
                                    <% rowspan = len(other_lines['lines']) + 1 %>
                                    <td class="bold" rowspan="${rowspan}">${_(u"Altres")}</td>
                                </tr>
                                % for other_line in other_lines['lines']:
                                    <tr class="row-separator">
                                        <td colspan="8">${other_line['name']}</td>
                                        <td class="bold right">${formatLang(other_line['price'])} €</td>
                                    </tr>
                                % endfor
                            %endif
                            <%
                                if len(taxes['iva']['lines']) != 1:
                                    raise NotImplementedError
                            %>
                            <tr></tr>
                            % for iva_line in taxes['iva']['lines']:
                                <tr class="row-separator">
                                    <td colspan="6" class="top-border-white"></td>
                                    <td class="bold bold-top-border">${_(u"BASE IMPOSABLE")}</td>
                                    <td colspan="2" class="bold-top-border"></td>
                                    <td class="bold right bold-top-border">${formatLang(iva_line['base'])} €</td>
                                </tr>
                                <tr>
                                    <td colspan="6" class="top-border-white"></td>
                                    <td class="bold">${iva_line['name']}</td>
                                    <td colspan="2"></td>
                                    <td class="bold right">${formatLang(iva_line['amount'])} €</td>
                                </tr>
                            % endfor
                            <tr class="row-separator">
                                <td colspan="6" class="top-border-white"></td>
                                <td class="bold bold-bottom-border">${_(u"IMPORT TOTAL")}</td>
                                <td colspan="2" class="bold-bottom-border"></td>
                                <td class="bold right bold-bottom-border">${formatLang(factura['amount'])} €</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- END DADES FACTURACIO -->
                <div class="left-50">
                    <table id="dades-pagament" class="styled-table">
                        <thead>
                            <tr>
                                <th>${_(u"DADES DE PAGAMENT")}</th>
                            </tr>
                        </thead>
                        <tbody>
                            %if transferencia:
                                <tr>
                                    <td>${code_bar_descr_full_mssg}</td>
                                </tr>
                            %else:
                                <tr>
                                    <td class="bold">${forma_pagament}</td>
                                </tr>
                                <tr>
                                    <td>${shadow_iban(factura['bank']['iban'], iban_indexes_to_hide)}</td>
                                </tr>
                                <tr>
                                    <td>(***) ${_(u"ocults per seguretat")}</td>
                                </tr>
                            %endif
                        </tbody>
                    </table>
                </div>
                <div class="right-50">
                    <!-- BANNER -->
                    % if fact_obj.has_tec271_line(cursor, uid, factura['id']):
                        <table id="dades-suplaments" class="styled-table">
                            <thead>
                                <tr>
                                    <th>${_(u"TAULA DETALLADA DELS SUPLEMENTS AUTONÒMICS 2013 ")}<span style="font-size: 12px; margin-left: 10px;">${(" (Segons informació de la CNMC)")}</span></th>
                                </tr>
                            </thead>
                            <%
                                polissa_id = fact_obj.read(cursor, uid, factura['id'], ['polissa_id'])['polissa_id'][0]
                                polissa_info = polissa_obj.read(cursor, uid, polissa_id, ['cups', 'distribuidora'])
                                cups = polissa_info['cups'][1]
                                distri_id = polissa_info['distribuidora'][0]
                                distri_name = partner_obj.read(cursor, uid, distri_id, ['name'])['name']
                            %>
                            <tbody>
                                <%
                                    html_table = sup_territorials_tec_271_comer_obj.get_info_html(cursor, uid, cups)
                                %>
                                ${html_table}
                                <div class="supl_indu">
                                En caso de que el importe total de la regularización supere los dos euros, sin incluir impuestos, el mismo será fraccionado en partes iguales superiores a 1€ por las empresas comercializadoras en las facturas que se emitan en el plazo de 12 meses a partir de la primera regularización
                                <br>
                                Tabla informativa conforme a lo establecido en la TEC/271/2019 de 6 de marzo, por la cual le informamos de los parámetros para el cálculo de los suplementos territoriales facilitados por su empresa distribuidora ${distri_name}
                                <br>
                                <%
                                    text = sup_territorials_tec_271_comer_obj.get_info_text(cursor, uid, cups)
                                    if text:
                                        text = text.format(distribuidora=distri_name)
                                %>
                                ${text}
                                </div>
                            </table>
                        </tbody>
                    %else:
                        <div class="banner">
                            %if image_first_page:
                                <img src="data:image/jpeg;base64,${image_first_page}"/>
                            %endif
                        </div>
                    %endif
                </div>
                <!-- END BANNER -->
                <!-- END DADES CONTRACTE -->
                <div id="first-page-bottom">
                    <!-- INFORMACIO INTERES -->
                    <table id="for-your-information">
                        <colgroup>
                            <col width="30%"/>
                            <col />
                            <col width="30%"/>
                        </colgroup>
                        <thead>
                            <tr>
                                <td><div></div></td>
                                <th class="center">${_(u"Informació del seu interès")}</th>
                                <td><div></div></td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="3" class="center">${_(u"Subministrament d'energía elèctrica efectuat en el mercat liberalitzat a l'empara de la Llei 54/1997 de 27 de novembre del Sector Elèctric")}.</td>
                            </tr>
                            <tr>
                                <td colspan="3" class="center">${_(u"El pagament de la present factura no pressuposa la liquidació de les anteriors i es justifica amb la possessió del rebut o amb el corresponent càrrec al compte bancari")} </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="center bold">${comer['name']} &nbsp&nbsp - &nbsp&nbsp ${comer['street']} &nbsp&nbsp - &nbsp&nbsp ${comer['zip']} ${comer['poblacio']} &nbsp&nbsp - &nbsp&nbsp Tel. ${comer['phone']} &nbsp&nbsp&nbsp&nbsp e-mail: ${comer['email']}</td>
                            </tr>
                            <tr>
                                <td colspan="3" class="center">${_(u"Telèfon per a incidències, reclamacions, queixes i informació sobre el servei contractat")}: 900 72 72 72</td>
                            </tr>
                            <tr>
                                <td colspan="3" class="center">${_(u"Avaries i incidències de seguretat a les instal·lacions de distribució (")}${supplier['name']}): ${supplier['phone']}</td>
                            </tr>
                        </tbody>
                    </table>
                    <!-- END INFORMACIO INTERES -->
                </div>
            </div>
            <!-- END FIRST PAGE -->
            <p style="page-break-after:always; clear:both"></p>
            <!-- SECOND PAGE -->
            <div id="second-page">
                 <!-- LECTURES -->
                <table id="dades-consums">
                    <caption>${_(u"DADES DE CONSUMS I LLEGIDES REALS")}</caption>
                    <colgroup>
                        <col width="10%"/>
                        <col width="12%"/>
                    </colgroup>
                    <thead>
                        <tr>
                            <th id="first-cell-no-border"></th>
                            <th class="bold-top-border bold-bottom-border bold-left-border"></th>
                            % for period_name in periods_a:
                                <th class="bold-top-border bold-bottom-border">${periods_translations[period_name]}</th>
                            % endfor
                            <th class="bold-top-border bold-right-border">${_(u"TOTAL")}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <%
                            lectura_init = False
                            lectura_end = False
                        %>
                        <!-- ACTIVA -->
                        %if readings_a['readings']:
                            <tr>
                                <th rowspan="4" class="h65px bold-top-border bold-left-border">${_(u"ACTIVA")}</th>
                            </tr>
                            % for meter in sorted(readings_a['readings']):
                                <%
                                    meter_readings =  readings_a['readings'][meter]

                                    lectura_end = meter_readings['P1']['current_date']
                                    lectura_init = meter_readings['P1']['previous_date']

                                    previous_reading_title = _(u"L. anterior ({})").format(
                                        meter_readings['P1']['previous_origin']
                                    )
                                    current_reading_title = _(u"Lectura ({})").format(
                                        meter_readings['P1']['current_origin']
                                    )
                                %>
                                <tr>
                                    <td class="bold h22px bold-top-border">${current_reading_title}</td>
                                    % for period in periods_a:
                                        <td class="right bold-top-border">${formatLang(meter_readings[period]['current_reading'], digits=0)}</td>
                                    % endfor
                                    <td class="bold-top-border bold-right-border"></td>
                                </tr>
                                <tr>
                                    <td class="bold h22px">${previous_reading_title}</td>
                                    % for period in periods_a:
                                        <td class="right">${formatLang(meter_readings[period]['previous_reading'], digits=0)}</td>
                                    % endfor
                                    <td class="bold-right-border"></td>
                                </tr>
                            % endfor
                            <tr>
                                <td class="bold h22px bold-bottom-border">${_(u"Consum")}</td>
                                <% sum_consumptions = 0 %>
                                % for period in periods_a:
                                    <%
                                        period_consumption = readings_a['total_consumption'][period]
                                        sum_consumptions += period_consumption
                                    %>
                                    <td class="right bold-bottom-border">${formatLang(period_consumption, digits=0)}</td>
                                %endfor
                                <td class="right bold-bottom-border bold-right-border">${formatLang(sum_consumptions, digits=0)}</td>
                            </tr>
                        %endif
                        <!-- ACTIVA -->
                        <!-- REACTIVA -->
                        % if readings_r['readings']:
                            <tr>
                                <th rowspan="4" class="h65px bold-top-border bold-left-border bold-bottom-border">${_(u"REACTIVA")}</th>
                            </tr>
                            % for meter in sorted(readings_r['readings']):
                                <%
                                    meter_readings =  readings_r['readings'][meter]
                                    previous_reading_title = _(u"L. anterior ({})").format(
                                        meter_readings['P1']['previous_origin']
                                    )
                                    current_reading_title = _(u"Lectura ({})").format(
                                        meter_readings['P1']['current_origin']
                                    )
                                %>
                                <tr>
                                    <td class="bold h22px">${current_reading_title}</td>
                                    % for period in periods_r:
                                        <td class="right">${formatLang(meter_readings[period]['current_reading'], digits=0)}</td>
                                    % endfor
                                    %for i in range(len(periods_r), len(periods_a) + 1):
                                        %if i == len(periods_a):
                                            <td class="bold-right-border"></td>
                                        %else:
                                            <td></td>
                                        %endif
                                    %endfor
                                </tr>
                                <tr>
                                    <td class="bold h22px">${previous_reading_title}</td>
                                    % for period in periods_r:
                                        <td class="right">${formatLang(meter_readings[period]['previous_reading'], digits=0)}</td>
                                    % endfor
                                    %for i in range(len(periods_r), len(periods_a) + 1):
                                        %if i == len(periods_a):
                                            <td class="bold-right-border"></td>
                                        %else:
                                            <td></td>
                                        %endif
                                    %endfor
                                </tr>
                            % endfor
                            <tr>
                                <td class="bold h22px bold-bottom-border">${_(u"Consum")}</td>
                                <% sum_consumptions = 0 %>
                                % for period_name in periods_r:
                                    <%
                                        period_consumption = readings_r['total_consumption'][period_name]
                                        sum_consumptions += period_consumption
                                    %>
                                    <td class="right bold-bottom-border">${formatLang(period_consumption, digits=0)}</td>
                                % endfor
                                %for i in range(len(periods_r), len(periods_a)):
                                    <td class="bold-bottom-border"></td>
                                %endfor
                                <td class="right bold-bottom-border bold-right-border">${formatLang(sum_consumptions, digits=0)}</td>
                            </tr>
                        %endif
                        <!-- REACTIVA -->
                        <!-- MAXIMETRE -->
                        % if policy['has_maximeter']:
                            <tr class="header">
                                <th class="bold-bottom-border bold-left-border">${_(u"MAXÍMETRE")}</th>
                                <td class="bold bold-bottom-border">${_(u"Lectura")}</td>
                                % for period in periods_m:
                                    <td class="right bold-bottom-border">${formatLang(readings_m[period]['maximeter'], digits=3)}</td>
                                % endfor
                                %for i in range(len(periods_m), len(periods_a) + 1):
                                    %if i == len(periods_a):
                                        <td class="bold-bottom-border bold-right-border"></td>
                                    %else:
                                        <td class="bold-bottom-border"></td>
                                    %endif
                                %endfor
                            </tr>
                        %endif
                        <!-- MAXIMETRE -->
                    </tbody>
                    %if lectura_init and lectura_end:
                        <tfoot>
                            <tr>
                                <%
                                    colspan =  len(periods_a) + 3
                                    periode_lectura_mssg = _(
                                        u"Període de lectura del {} al {}"
                                    ).format(
                                        erp_date_to_dmy(lectura_init),
                                        erp_date_to_dmy(lectura_end)
                                    )
                                %>
                                <td colspan="${colspan}" class="bold-left-border bold-bottom-border bold-right-border">
                                    ${periode_lectura_mssg}
                                </td>
                            </tr>
                        </tfoot>
                    %endif
                </table>
                <!-- END LECTURES -->
                <!-- HISTORIC -->
                <table id="historic-consums" class="styled-table">
                    <thead>
                        <tr>
                            <th>${_(u"HISTÒRIC DE CONSUMS")}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <!--<div class="chart_consum" id="chart_consum_${factura['id']}"></div>-->
                                <div id="consumption-bar-plot"></div>
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td class="italic center">
                                ${_(u"Consum mitjà de les {} últimes factures: {}").format(historical['months'], formatLang(historical['average_consumption']) )}<br/>
##                                     ${_(u"El seu consum acumulat el darrer any ha sigut de {} kWh").format(formatLang(historical['year_consumption'], digits=0))}
                            </td>
                        </tr>
                    </tfoot>
                </table>
                <!-- END HISTORIC -->
                <!-- INFORMACIO SOBRE LA SEVA ELECTRICITAT-->
                <table id="about-my-electricity" class="styled-table">
                    <thead>
                        <tr>
                            <th>${_(u"INFORMACIÓ SOBRE LA SEVA ELECTRICITAT")}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <p>${_(u"Si bé l'energia eléctrica que arriba a les nostres cases és indistingible de la que consumeixen els nostres veïns o altres consumidors connectats al mateix sistema elèctric, ara sí és possible garantitzar l'orígen de la producció de l'energía elèctrica que vostè consumeix.")}</p>
                                <p>${_(u"A aquests efectes es proporciona el detall de la barreja de tecnologies de producció nacional per tal de poder comparar els percentatges del promig nacional amb els corresponents a l'energia venuda per la seva companyia comercialitzadora.")}</p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <!-- ORIGEN DE L'ENERGIA -->
                                <div id="energy-origin">
                                    %if image_energy_origin:
                                        <img src="data:image/jpeg;base64,${image_energy_origin}">
                                    %endif
                                </div>
                                <!-- END ORIGEN DE L'ENERGIA -->
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <!-- IMPACTE AMBIENTAL -->
                                <div id="environmental-impact">
                                    %if image_environmental_impact:
                                        <img src="data:image/jpeg;base64,${image_environmental_impact}">
                                    %endif
                                </div>
                                <!-- END IMPACTE AMBIENTAL -->
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div id="second-page-bottom">
                    %if transferencia:
                        %if factura['payment_type'] != 'ADMINISTRACIO I GRANS CLIENTS':
                            <table id="table-bottom">
                                <tr>
                                    <td>
                                        <div>
                                            %if image_energy_origin:
                                                <img src="data:image/jpeg;base64,${image_logo_code_bar}" width="40px">
                                            %endif
                                        </div>
                                    </td>
                                    <td>
                                        <table id="table-bottom-1">
                                            <thead>
                                                <tr>
                                                    <th class="left">${_(u"RESGUARD PER L'ENTITAT BANCÀRIA")}</th>
                                                    <th class="right">${_(u"FACTURA DE SUBMINISTRAMENT")}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td colspan="2">
                                                        <table id="table-bottom-2">
                                                            <colgroup>
                                                                <col width="15%"/>
                                                            </colgroup>
                                                            <thead>
                                                                <tr>
                                                                    <th>${_(u"PERIODE DE PAGAMENT")}</th>
                                                                    <th>${_(u"EMISSORA")}</th>
                                                                    <th>${_(u"Nº DE REFERÈNCIA")}</th>
                                                                    <th>${_(u"IDENTIFICACIÓ")}</th>
                                                                    <th>${_(u"IMPORT")}</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td class="center">${factura['due_date']}</td>
                                                                    <td class="center">${comer['entity']}-${recibo507.suffix}</td>
                                                                    <td class="center">${factura['ref_bar_code']}${recibo507.checksum()}</td>
                                                                    <td class="center">${recibo507.notice}</td>
                                                                    <td class="right">${formatLang(factura['amount'])}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" rowspan="2">
                                                                        <table id="comer-data">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td>${comer['name']}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>${comer['street']}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>${comer['zip']} ${comer['poblacio']}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>CUPS ${cups_ae_formatted}</td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                    <td id="barcode-descr" class="center" colspan="3">
                                                                        ${_(u"CODI DE BARRES PER EFECTUAR EL PAGAMENT DES DELS EQUIPS AUTOSERVEI")}
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3" class="center">
                                                                        ${recibo507.svg(writer_options={'module_height': 10, 'font_size': 7, 'text_distance': 3, 'module_width': 0.25})}
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        %endif
                    %endif
                </div>
            </div>
            <!-- END SECOND PAGE -->
            <script>
                // BAR PLOT VARIABLES
                var barplot_height = 200;
                var barplot_width = 900;

                var grad_colors_1 = [['#faffb7', '#d3d800']];
                var grad_colors_3 = [
                    ['#faffb7', '#d3d800'], ['#faffb7', '#d3d800'], ['#faffb7', '#d3d800'],
                    ['#faffb7', '#d3d800'], ['#faffb7', '#d3d800'], ['#faffb7', '#d3d800']
                ];

                var factura_id = ${factura['id']};
                var data_consum = ${json.dumps(sorted(historic['historic_js'], key=lambda h: h['mes']))};

                // PIE CHART VARIABLES
                var piechart_height = 180;
                var piechart_width = 600;

                //pie start_position
                var pie_left = 150;

                var pie_total = ${pie['total']};
                var pie_data = [
                    {val: ${pie_regulats}, perc: 30, code: "REG"},
                    {val: ${pie_costos}, perc: 55, code: "PROD"},
                    {val: ${pie_impostos},  perc: 15 ,code: "IMP"}
                ];

                var pie_etiquetes = {
                    'REG': {t: ['${formatLang(float(pie_regulats))} €','${_(u"Costos regulats")}'], w:100},
                    'IMP': {t: ['${formatLang(float(pie_impostos))} €','${_(u"Impostos aplicats")}'], w:100},
                    'PROD': {t: ['${formatLang(float(pie_costos))} €','${_(u"Costos de producció electricitat")}'], w:150}
                };

                var reparto = ${json.dumps(pie['reparto'])};
                var dades_reparto = ${json.dumps(dades_reparto)};

                renderHistoricChart('#consumption-bar-plot', ${json.dumps(columns)}, ${json.dumps(y_axis)}, ${json.dumps(colors)}, 200, 900, 40, 0, false);
            </script>
            <script src="${addons_path}/giscedata_facturacio_comer/report/assets/consumption-chart.js"></script>
            <script src="${addons_path}/giscedata_facturacio_comer/report/assets/pie-chart.js"></script>
            % if factura != factura_data[-1]:
                <p style="page-break-after:always; clear:both"></p>
            % endif
        %endfor

    </body>
</html>
