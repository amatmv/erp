## -*- coding: utf-8 -*-
<%
    import babel
    from datetime import datetime, timedelta
    from bankbarcode.cuaderno57 import Recibo507
    from account_invoice_base.report.utils import localize_period
    from account_invoice_base.account_invoice import amount_grouped
    from workalendar.europe import Spain

    calendar = Spain()

    report_obj = pool.get('giscedata.facturacio.carta.requeriment.report')
    imd_obj = pool.get('ir.model.data')
    account_invoice_obj = pool.get('account.invoice')
%>

<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
        <title>Carta de Tall</title>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_agri/report/estils_agri.css"/>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_agri/report/estils_carta_tall.css"/>
        <style>
            @font-face {
                font-family: "Roboto-Regular";
                src: url("${addons_path}/giscedata_facturacio_comer/report/assets/fonts/roboto/Roboto-Regular.ttf") format('truetype');
                font-weight: normal;
            }
        </style>
    </head>
    <body>
        <%
            data_pago = calendar.add_working_days(datetime.today(), 1)
        %>
        <%def name="get_codi_barres(factura)">
            <%
                # bar code
                from datetime import datetime, timedelta
                import time
                entity = ''.join([c for c in factura.polissa_id.comercialitzadora.vat if c.isdigit()])
                suffix = '500'
                due_date = datetime.combine(data_pago, datetime.min.time())  # Convert data_pago to datetime

                ref = '{}'.format(factura.id).zfill(11) # id factura
                notice = '000000'
                grouped = amount_grouped(factura)
                amount = '{0:.2f}'.format(grouped) # amount grouped factura
                recibo507 = Recibo507(entity, suffix, ref, notice, amount, due_date)
                return recibo507
            %>
        </%def>
        %for factura in objects:
            <%
                grouped = formatLang(amount_grouped(factura), monetary=True, digits=2)
            %>
            <div id="outer">
                <p id="vertical_text">
                    ${company.rml_footer2}
                </p>
            </div>

            <div id="carta">
                <table id="cap">
                    <tr>
                        <td rowspan="2">
                            <img class="logo" height="35px" src="data:image/jpeg;base64,${company.logo}"/>
                        </td>
                        <td>
                            <img class="icona" src="${addons_path}/giscedata_facturacio_comer_agri/report/icones/phone.png"/>
                        </td>
                        <td>
                            972 58 00 58
                        </td>
                        <td>
                            <a href="www.agrienergia.com">
                                <img class="icona" src="${addons_path}/giscedata_facturacio_comer_agri/report/icones/world.png"/>
                            </a>
                        </td>
                        <td>
                            <a href="www.agrienergia.com">www.agrienergia.com</a>
                        </td>
                        <td>
                            <a href="https://www.facebook.com/agrienergia">
                                <img class="icona" src="${addons_path}/giscedata_facturacio_comer_agri/report/icones/fb.png"/>
                            </a>
                        </td>
                        <td>
                            <img class="icona" src="${addons_path}/giscedata_facturacio_comer_agri/report/icones/in.png"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="mailto:oficina@agrienergia.com">
                                <img class="icona" src="${addons_path}/giscedata_facturacio_comer_agri/report/icones/mail.png"/>
                            </a>
                        </td>
                        <td>
                            <a href="mailto:oficina@agrienergia.com">oficina@agrienergia.com</a>
                        </td>
                        <td>
                            <img class="icona" src="${addons_path}/giscedata_facturacio_comer_agri/report/icones/marker.png"/>
                        </td>
                        <td>
                            Av. Països Catalans, 140 · 17820 Banyoles
                        </td>
                        <td>
                            <a href="https://twitter.com/agri_energia">
                                <img class="icona" src="${addons_path}/giscedata_facturacio_comer_agri/report/icones/tw.png"/>
                            </a>
                        </td>
                        <td>
                            <img class="icona" src="${addons_path}/giscedata_facturacio_comer_agri/report/icones/insta.png"/>
                        </td>
                    </tr>
                </table>
                <h1 class="center titol_pagament"> CARTA DE PAGAMENT</h1>
               <div>
                   <div class="left-50 ">
                     <h2> Dades del contracte </h2>
                   </div>
                   <div class="right-35">
                     <h2> Dades de notificació </h2>
                   </div>
               </div>

                <div>
                    <div class="left-50 contact_info">
                        <div class="">
                            <table>
                               <tr>
                                    <td>
                                        <strong>Titular:</strong> ${factura.polissa_id.titular.name}
                                    </td>
                                </tr>
                               <tr>
                                    <td>
                                        <strong>NIF:</strong> ${factura.polissa_id.titular.vat.replace('ES','')}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${factura.cups_id.tv and factura.cups_id.tv.abr or ''} ${factura.cups_id.nv or ''} ${factura.cups_id.pnp or ''} ${factura.cups_id.es or ''} ${factura.cups_id.pt or ''} ${factura.cups_id.pu or ''}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                         ${factura.cups_id.cpo or ''} ${factura.cups_id.cpa or ''} ${factura.cups_id.dp or ''}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${factura.cups_id.id_poblacio.name + ',' or ''} ${factura.cups_id.id_municipi.name or ''}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${factura.cups_id.aclarador or ''}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p id="referencia">CUPS: ${factura.cups_id.name}</p>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="right-35 contact_info">
                        <div class="">
                            <table>
                                <tr>
                                    <td>
                                        ${factura.address_contact_id.name}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${factura.address_contact_id.street or ''}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${factura.address_contact_id.street2 or ''}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${factura.address_contact_id.zip or ''} ${factura.address_contact_id.city or ''}
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div style="clear:both;" ></div>
                <p>
                    ${_(u"Senyor/a,")}
                </p>
                <p>
                    ${_(u"Els informem que la factura corresponent al subministrament d'energia elèctrica que es relaciona tot seguit, està pendent de pagament.")}
                    <br/>
                    <table style="width: 100%; margin-top: 10px;">
                        <tr>
                            <th>${_(u"Núm. Factura")}</th>
                            <th>${_(u"Data Factura")}</th>
                            <th>${_(u"Import Total")}</th>
                        </tr>
                        <tr>
                            <td class="center">${factura.number}</td>
                            <td class="center">${factura.date_invoice}</td>
                            <td class="center">
                                ${grouped}
                            </td>
                        </tr>
                    </table>
                </p>
                <p class="bold font_15px">
                    ${_(u"L'IMPORT TOTAL PENDENT ÉS DE:")}
                    ${grouped}
                </p>
                <p>
                    ${_(u"El termini per efectuar el present pagament finalitzi el dia ")}${datetime.strftime(data_pago, '%d/%m/%Y')}
                </p>

                <div id="resum" class="box-border resum_pagament">
                    <p>
                        ${_(u"Li comuniquem que, en cas de no fer-se efectiu el pagament dins el termini establert, es declararà l'impagament i s'iniciarà el procés de tall.")}
                    </p>
                </div>
                <p>
                    ${_(u"L'esmentat pagament el pot fer efectiu a través de qualsevol de les següents entitats bancàries, en el seu horari de pagament, presentant aquest escrit a finestreta:")}
                    <ul>
                        <li>Banc Santander</li>
                        <li>Banco Popular</li>
                    </ul>
                </p>

                <p>
                    ${_(u"Banyoles, ")} ${datetime.strftime(datetime.now(), '%d/%m/%Y')}
                </p>
            </div>
            <div id="carta_tall_espai"></div>
            <hr class="ratllat"/>
            <div class="float_left">
                <img id="imatge_pagament" width="50px" alt="AgriEnergia" src="${addons_path}/giscedata_facturacio_comer_agri/report/icones/logo.png"/>
            </div>
            <%
                recibo507 = get_codi_barres(factura)
            %>
            <div>
                <table id="taula_peu">
                    <tr>
                        <th colspan="4" class="left">${_(u"RESGUARD PER L'ENTITAT BANCÀRIA")}</th>
                        <th>${_(u"CARTA DE PAGAMENT")}</th>
                    </tr>
                    <tr>
                        <th>
                            ${_(u"PERIODE DE PAGAMENT")}
                        </th>
                        <th>
                            ${_(u"EMISSORA")}
                        </th>
                        <th>
                            ${_(u"Nº DE REFERÈNCIA")}
                        </th>
                        <th>
                            ${_(u"IDENTIFICACIÓ")}
                        </th>
                        <th>
                            ${_(u"IMPORT")}
                        </th>
                    </tr>
                    <tr>
                        <td>
                            ${data_pago.strftime('%d/%m/%Y')}
                        </td>
                        <td>
                            ${recibo507.entity} - ${recibo507.suffix}
                        </td>
                        <td>
                            ${recibo507.ref}${recibo507.checksum()}
                        </td>
                        <td>
                            ${recibo507.notice}
                        </td>
                        <td>
                            ${grouped}
                        </td>
                    </tr>
                </table>
            </div>
            <div>
                <div style="text-align:center; padding-right:80px" class="float_right">
                    <span class="font_10px"> ${_(u"CODI DE BARRES PER EFECTUAR EL PAGAMENT")} </span>
                    <br/>
                    ${ recibo507.svg(writer_options={'module_height': 15, 'font_size': 7, 'text_distance': 3, 'module_width': 0.30}) }
                </div>
                <div class="payment_info float_left">
                    <table style="margin-left: 50px;">
                        <tr>
                            <td>
                                ${factura.polissa_id.titular.name}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                ${factura.address_contact_id.street or ''}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                ${factura.address_contact_id.street2 or ''}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                ${factura.address_contact_id.zip or ''} ${factura.address_contact_id.city or ''}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                ${factura.polissa_id.name}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                CUPS: ${factura.cups_id.name}
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="float_left" id="condicions">
                <div id="condicions_carta_pagament">
                    <b>${_(u"Pagament:")}</b>
                    ${_(u"A través de qualsevol de les següents entitats bancàries (Banc Santander, Banco Popular), en el seu horari de pagament, presentant a aquest escrit a finestreta fins el dia")}
                    <span class="red">${datetime.strftime(data_pago, '%d/%m/%Y')}</span>
                    ${_(u"i (BBVA i La Caixa), en el caixer automàtic a qualsevol hora.")}
                </div>
            </div>
            % if factura != objects[-1]:
                <p style="page-break-after:always; clear:both"></p>
            % endif
        %endfor
    </body>
</html>
