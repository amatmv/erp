## -*- coding: utf-8 -*-
<%
    from dateutil import parser
    from datetime import datetime
    from base_iban.base_iban import shadow_iban
    from bankbarcode.cuaderno57 import Recibo507
    from giscedata_facturacio_comer.report.utils import clean_nif
    import json, re

    report_o = pool.get('giscedata.facturacio.factura.report')
    banner_obj = pool.get('report.banner')
    sup_territorials_tec_271_comer_obj = pool.get(
    'giscedata.suplements.territorials.2013.tec271.comer'
    )
    fact_obj = pool.get('giscedata.facturacio.factura')
    polissa_obj = pool.get('giscedata.polissa')
    partner_obj = pool.get('res.partner')

    colors = {
        'accumulated': '#cedc00',
        'P1': '#cedc00',
        'P2': '#DDE840',
        'P3': '#D6E324',
        'P4': '#EEF688',
        'P5': '#E5EE5F',
        'P6': '#faffb7'
    }

    def ae_formatted_cups(cups):
        numeric_block = ""
        first_block = cups[:2]
        fourth_block = ""
        flag = True
        for c in cups[2:]:
            if c.isdigit() and flag:
                numeric_block += c
            else:
                flag = False
                fourth_block += c

        second_block = numeric_block[:-6]
        third_block = numeric_block[-6:]

        return "{}{}<b>{}</b>{}".format(
            first_block, second_block, third_block, fourth_block
        )

    iban_indexes_to_hide = [2, 3, 12, 13, -3, -2, -1]
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_agri/report/estils_agri.css"/>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_agri/report/factura_domestica.css"/>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_agri/report/assets/pie-chart.css"/>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_agri/report/assets/consumption-chart.css"/>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_agri/report/assets/bar-plot-domestica.css"/>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_agri/report/assets/bar-plot.css"/>
        <script src="${assets_path}/js/d3.min.js"></script>
        <script src="${assets_path}/js/c3.min.js"></script>
        <script src="${assets_path}/js/formatNumber.js"></script>
        <script src="${addons_path}/giscedata_facturacio_comer_agri/report/assets/bar-plot.js"></script>
        <style>
            @font-face {
                font-family: "Roboto-Regular";
                src: url("${assets_path}/fonts/Roboto/Roboto-Regular.ttf") format('truetype');
                font-weight: normal;
            }
        </style>
    </head>
    <%
        # Company information
        comer = report_o.get_company_info(cursor, uid, company.id)[company.id]
        lateral_info =  comer['lateral_info'] or ''
    %>
    <body>
        %for factura in factura_data:
            <%
                language = factura['lang_partner'] or 'ca_ES'
                setLang(language)

                median_consumption_mssg = _(
                    u"El seu consum mitjà diari en el període facturat ha sigut de {} € "
                    u"que corresponent a {} kWh/dia ({} dies)"
                )

                periods_translations = {
                    'P1': _(u"Període 1"),
                    'P2': _(u"Període 2"),
                    'P3': _(u"Període 3"),
                    'P4': _(u"Període 4"),
                    'P5': _(u"Període 5"),
                    'P6': _(u"Període 6"),
                }

                footer_invoicing_detail_mssg = _(
                    u"La factura inclou {}€ corresponents al peatge d'accés dels que un {}% "
                    u"corresponen al terme de potència i un {}% al terme d'energia. "
                    u"Regulats per l'Ordre ETU/1282/2017. Així mateix inclou {}€ del "
                    u"lloguer de l'equip de mesura."
                )

                code_bar_descr_mssg = _(
                    u"<b>Pagament:</b> A través de qualsevol de les següents entitats bancàries "
                    u"(Banc Santander, Banco Popular, La Caixa), en el seu horari de "
                    u"pagament, presentant aquest escrit a finestreta fins el dia <span class=\"red\"><b>{}</b></span> i "
                    u"(BBVA), en el caixer automàtic a qualsevol hora."
                )

                invoice_destination_mssg = _(
                    u"Destí de l'import total de la factura: "
                    u"- Cost de producció d'electricitat {}% "
                    u"- Impostos aplicats {}% "
                    u"- Regulació elèctrica: "
                    u"- Incentius a les energies renovables, cogeneració i residus: {}% "
                    u"- Cost de les xarxes de distribució i transport: {}%"
                    u"- Altres costos regulats: {}%"
                )

                contact = factura['contact']
                detailed_lines = factura['detailed_lines']
                historic = factura['historic']
                historical = historic['historic']
                holder = factura['holder']
                invoiced_power = factura['invoiced_power']
                periods_and_readings = factura['periods_and_readings']
                periods = periods_and_readings['periods']
                periods_a = periods['active']
                readings = periods_and_readings['readings']
                readings_a = readings['active']
                pie = factura['pie']
                pie_regulats = pie['regulats']
                pie_impostos = pie['impostos']
                pie_costos = pie['costos']
                dades_reparto = pie['dades_reparto']
                for repartiment in dades_reparto:
                    repartiment[3] = formatLang(repartiment[3])
                policy = factura['policy']
                power_excess_and_other_detailed_info = factura['power_excess_and_other_detailed_info']
                other_lines = power_excess_and_other_detailed_info['other_lines']
                meters_rent = power_excess_and_other_detailed_info['meters_rent']
                power_lines_with_excess = power_excess_and_other_detailed_info['lines_with_power_excess']
                supplier = factura['supplier']
                access_contract = supplier['ref'] or ''
                taxes = factura['taxes']

                import_factura = factura['amount']
                import_a_pagar = factura['amount']
                variant_aplicada = bool(factura['variant_id'])
                descompte_variants = factura['descompte_variants']
                if '_refund' in factura['invoice_type']:
                    descompte_variants *= -1

                if variant_aplicada:
                    import_a_pagar = factura['amount'] - descompte_variants

                median_consumption_full_mssg = median_consumption_mssg.format(
                    formatLang(detailed_lines['energy']['total']/(factura['days'] or 1.0)),
                    formatLang(factura['diari_factura_actual_kwh']),
                    factura['days'] or 1
                )

                total_energy_toll = 0
                total_power_toll = 0

                factura_date = datetime.strptime(factura['date'], '%d/%m/%Y').strftime('%Y-%m-%d')

                due_date_erp_formated = datetime.strptime(factura['due_date'], '%d/%m/%Y').strftime("%Y-%m-%d")

                recibo507 = Recibo507(
                    comer['entity'], '500',
                    factura['ref_bar_code'], '000000',
                    '{0:.2f}'.format(import_factura), due_date_erp_formated
                )

                image_under_window = banner_obj.get_banner(
                    cursor, uid, 'giscedata.facturacio.factura',
                    factura_date, code='main_banner_domestica'
                )
                image_environmental_impact = banner_obj.get_banner(
                    cursor, uid, 'giscedata.facturacio.factura',
                    factura_date, code='environmental_impact_domestica'
                )
                image_energy_origin = banner_obj.get_banner(
                    cursor, uid, 'giscedata.facturacio.factura',
                    factura_date, code='energy_origin_domestica'
                )
                image_logo_code_bar = banner_obj.get_banner(
                    cursor, uid, 'giscedata.facturacio.factura',
                    factura_date, code='logo_code_bar'
                )

                columns, y_axis = report_o.historic_js_c3_formatted(
                    historic['historic_js'], True
                )

                rotate_x = 0
                rotate_bar_labels = False
                if len(columns[0]) > 7:
                    rotate_bar_labels = True
                    rotate_x = -90

                cups_ae_formatted = ae_formatted_cups(factura['cups']['name'])

                # Thank you @destanyol. Definitely not elegant, but works.
                general_conditions = policy.get('general_conditions', '')
                if general_conditions:
                    for i in range(0,40):
                        general_conditions += '&nbsp;'

                transferencia = factura['payment_type_code'] in ('TRANSFERENCIA_CSB', 'TRANSFERENCIA', 'FINESTRETA')
                code_bar_descr_full_mssg = code_bar_descr_mssg.format(factura['due_date'])

                invoice_destination_full_mssg = invoice_destination_mssg.format(
                    formatLang(pie_costos/import_factura*100),
                    formatLang(pie_impostos/import_factura*100),
                    '17,75', '15,71', '15,46'
                )

                forma_pagament = factura['payment_type'].lower()
                forma_pagament = _(u"cobrament al comptat") if forma_pagament == 'transferencia' else forma_pagament

                total_meter_rent = 0
                for meter_rent in meters_rent:
                    total_meter_rent += meter_rent['amount']

                direccio_cups = factura['cups']['address']
                direccio_cups_second_token = re.findall("[0-9]{5}.*", direccio_cups)
                if direccio_cups_second_token:
                    direccio_cups_second_token = direccio_cups_second_token[0]
                    direccio_cups = (
                        direccio_cups.replace(direccio_cups_second_token, ''),
                        direccio_cups_second_token
                    )
                else:
                    direccio_cups = (direccio_cups, '')
            %>
            <div id="lateral-info" class="center">${general_conditions}${lateral_info}</div>
            <div id="first-page">
                <!-- LOGO -->
                <div id="logo" class="left-50">
                    <img src="data:image/jpeg;base64,${comer['logo']}"/>
                </div>
                <!-- END LOGO -->
                <!-- DADES FACTURA -->
                <div class="right-50">
                    <table id="dades-factura">
                        <thead>
                            <tr>
                                <th class="left" colspan="2">${_(u"DADES DE LA FACTURA")}</th>
                            </tr>
                        </thead>
                        <tbody class="light">
                            <tr>
                                % if variant_aplicada:
                                    <td id="import-factura" class="bold">${_(u"IMPORT A PAGAR")}:</td>
                                % else:
                                    <td id="import-factura" class="bold">${_(u"IMPORT FACTURA")}:</td>
                                % endif
                                <td class="bold right">${formatLang(import_a_pagar)} €</td>
                            </tr>
                            <tr>
                                <td colspan="2"><b>${_(u"Núm. factura")}:</b> ${factura.get('name', '')}</td>
                            </tr>
                            <tr>
                                <td colspan="2"><b>${_(u"Data factura")}:</b> ${factura['date']}</td>
                            </tr>
                            <tr>
                                <td colspan="2"><b>${_(u"Període factura")}:</b> ${factura['init_date']} a ${factura['end_date']}</td>
                            </tr>
                            <tr>
                                <td colspan="2"><b>${_(u"Límit de pagament")}:</b> ${factura['due_date']}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- END DADES FACTURA -->
                <!-- RESUM FACTURACIO -->
                <div class="left-50 no-clear">
                    <table id="resum-factura" class="styled-table">
                        <thead>
                            <tr>
                                <th colspan="2">${_(u"RESUM DE FACTURACIÓ")} *</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="resum-factura-high-padding">
                                <td>${_(u"Per potència contractada")}</td>
                                <td class="right">${formatLang(detailed_lines['power']['total'])} €</td>
                            </tr>
                            <tr id="white-space-below" class="resum-factura-high-padding">
                                <td>${_(u"Per energia consumida")}</td>
                                <td class="right">${formatLang(detailed_lines['energy']['total'])} €</td>
                            </tr>
                            <tr class="resum-factura-low-padding">
                                <td>${_(u"Impost sobre l'electricitat")}</td>
                                <td class="right">${formatLang(taxes['iese']['total'])} €</td>
                            </tr>
                            <tr class="resum-factura-low-padding">
                                <td>${_(u"Gestió de la mesura")}</td>
                                <td class="right">${formatLang(total_meter_rent)} €</td>
                            </tr>
                            <tr class="resum-factura-low-padding">
                            % if other_lines['total'] == 0:
                                    <td colspan="2">&nbsp</td>
                            % else:

                                    <td>${_(u"Altres conceptes")}</td>
                                    <td class="right">${formatLang(other_lines['total'])} €</td>
                            % endif
                            </tr>
                            % for tax, tax_amount in taxes['taxes'].items():
                                % if "IVA" in tax:
                                    <tr class="resum-factura-low-padding">
                                        <td>${tax}</td>
                                        <td class="right">${formatLang(tax_amount)} €</td>
                                    </tr>
                                % endif
                            % endfor
                            % if variant_aplicada:
                                <tr id="total-import-factura" style="border-top: solid 2px black; border-bottom: solid 2px black;">
                                    <td class="bold">${_(u"TOTAL IMPORT FACTURA")}</td>
                                    <td class="bold right">${"{} €".format(formatLang(import_factura))}</td>
                                </tr>
                                <tr id="total-import-a-pagar-variant" class="light">
                                    <td class="bold">${_(u"Abonament")}</td>
                                    <td class="bold right">${"{} €".format(formatLang(factura['descompte_variants']))}</td>
                                </tr>
                                <tr id="total-import-a-pagar-variant" class="light">
                                    <td class="bold">${_(u"IMPORT A PAGAR")}</td>
                                    <td class="bold right">${"{} €".format(formatLang(import_a_pagar))}</td>
                                </tr>
                            % else:
                                <tr id="total-import-factura" class="light">
                                    <td class="bold">${_(u"TOTAL IMPORT FACTURA")}</td>
                                    <td class="bold right">${"{} €".format(formatLang(import_factura))}</td>
                                </tr>
                            % endif
                        </tbody>
                    </table>
                </div>
                <!-- END RESUM FACTURA -->
                <!-- FINESTRETA -->
                <div class="right-50">
                    <table id="finestreta">
                        <thead>
                            <tr>
                                <th>${contact['name']}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>${contact['street'].replace('..', '.')}</td>
                            </tr>
                            <tr>
                                <td>${contact['zip']} ${contact['city']}</td>
                            </tr>
                        </tbody>
                    </table>
                    %if image_under_window:
                        <img src="data:image/jpeg;base64,${image_under_window}" height="100px"/>
                    %endif
                </div>
                <!-- END FINESTRETA -->
                <div class="left-50">
                    <!-- INFO CONSUM ELECTRIC -->
                    <table id="info-consum-electric" class="styled-table">
                        <colgroup>
                        %if len(periods_a) != 3:
                            <col width="50%"/>
                            <col width="25%"/>
                        %else:
                            <col width="46%"/>
                            <col width="18%"/>
                        %endif
                        </colgroup>
                        <thead>
                            <tr>
                                <th colspan="3">${_(u"INFORMACIÓ DEL CONSUM ELÈCTRIC")} *</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- ACTIVA -->
                            % for meter in sorted(readings_a['readings']):
                                <%
                                    meter_readings =  readings_a['readings'][meter]
                                    previous_reading_title = _(u"Lectura ant. ({}) {}").format(
                                        meter_readings['P1']['previous_origin'],
                                        formatLang(meter_readings['P1']['previous_date'], date=True)
                                    )
                                    current_reading_title = _(u"Lectura act. ({}) {}").format(
                                        meter_readings['P1']['current_origin'],
                                        formatLang(meter_readings['P1']['current_date'], date=True)
                                    )
                                %>
                                <tr>
                                    <td class="bold">${_(u"Núm. de comptador")}: ${meter}</td>
                                    % for period_name in periods_a:
                                        <td class="center underline">${periods_translations[period_name]}</td>
                                    % endfor
                                    %for missing_period in range(len(periods_a), 2):
                                        <td></td>
                                    %endfor
                                </tr>
                                <tr>
                                    <td>${current_reading_title}</td>
                                    % for period in periods_a:
                                        <td class="right">${formatLang(meter_readings[period]['current_reading'], digits=0)} <span class="span_kwh">kWh</span></td>
                                    % endfor
                                    %for missing_period in range(len(periods_a), 2):
                                        <td></td>
                                    %endfor
                                </tr>
                                <tr>
                                    <td>${previous_reading_title}</td>
                                    % for period in periods_a:
                                        <td class="right">${formatLang(meter_readings[period]['previous_reading'], digits=0)} <span class="span_kwh">kWh</span></td>
                                    % endfor
                                    %for missing_period in range(len(periods_a), 2):
                                        <td></td>
                                    %endfor
                                </tr>
                            % endfor
                            <tr class="light top-border">
                                <td>${_(u"Consum per període")}</td>
                                <% total_energy_consumption = 0 %>
                                % for period in periods_a:
                                    <%
                                        period_total_consumption = readings_a['total_consumption'][period]
                                        total_energy_consumption += period_total_consumption
                                    %>
                                    <td class="right">${formatLang(period_total_consumption, digits=0)} <span class="span_kwh">kWh</span></td>
                                %endfor
                                %for missing_period in range(len(periods_a), 2):
                                    <td></td>
                                %endfor
                            </tr>
                            %if len(periods_a) > 1:
                                <tr>
                                    <td></td>
                                    % for period in periods_a:
                                        <% consumption_percentage = readings_a['total_consumption'][period]/float(total_energy_consumption)*100 if total_energy_consumption > 0 else 0 %>
                                        <td class="right bold">${formatLang(consumption_percentage, digits=0)} %</td>
                                    %endfor
                                    %for missing_period in range(len(periods_a), 2):
                                        <td></td>
                                    %endfor
                                </tr>
                            %endif
                            <!-- END ACTIVA -->
                        </tbody>
                    </table>
                </div>
                <div class="right-50">
                    <!-- HISTORIC -->
                    <table id="historic-consums" class="styled-table">
                        <thead>
                            <tr>
                                <th>${_(u"HISTÒRIC DE CONSUMS")} *</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <!--<div class="chart_consum" id="chart_consum_${factura['id']}"></div>-->
                                    <div id="consumption-bar-plot"></div>
                                </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td class="italic center">
                                    ${_(u"Consum mitjà de les {} últimes factures: {}").format(historical['months'], formatLang(historical['average_consumption']) )}<br/>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                    <!-- END HISTORIC -->
                </div>
                <!-- DADES DEL CONTRACTE -->
                <table id="dades-contracte" class="styled-table">
                    <colgroup>
                        <col width="50%"/>
                        <col width="50%"/>
                    </colgroup>
                    <thead>
                        <tr>
                            <th>${_(u"DADES DEL CONTRACTE")}</th>
                            <td id="dades-contracte-false-header" class="italic bold center">
                                ${_(u"*Per a més informació, registra't a l'oficina virtual, a on podràs activar la factura electrònica")}
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="2"><b>${_(u"Titular")}:</b> ${holder['name']}    <b>${_(u"NIF")}:</b> ${holder['vat']}</td>
                        </tr>
                        <tr>
                            <td colspan="2"><b>${_(u"Adreça deL subministrament")}:</b> ${factura['cups']['address']}</td>
                        </tr>
                        <tr>
                            <td colspan="2"><b>${_(u"Núm. de comptador")}:</b> ${", ".join(readings_a['readings'].keys())}</td>
                        </tr>
                        <tr>
                            <td colspan="2"><b>${_(u"Potència contractada")}:</b> ${formatLang(factura['hired_power'])} kW</td>
                        </tr>
                        <tr>
                            <td colspan="2"><b>${_(u"Tarifa")}:</b> ${factura['tariff']}</td>
                        </tr>
                        <tr>
                            <td colspan="2"><b>${_(u"Forma de pagament")}:</b> ${forma_pagament}</td>
                        </tr>
                        <tr>
                            <td colspan="2">${_(u"Refrència del contracte de subministrament (Agri-Energia, S.A.)")}: ${policy['name']}</td>
                        </tr>
                        <tr>
                            <td colspan="2">${_(u"Referència del contracte d'accés (AGRI ENERGIA ELECTRICA)")}: ${access_contract}</td>
                        </tr>
                        <tr>
                            <td colspan="2"><b>${_(u"Codi Unificat de Punt de Subministrament (CUPS)")}:</b> ${cups_ae_formatted}</td>
                        </tr>
                    </tbody>
                </table>
                <!-- END DADES CONTRACTE -->
                <%
                    first_page_classes = ''
                    if transferencia:
                        first_page_classes = 'top-border-dotted'
                %>
                <div id="first-page-bottom" class="${first_page_classes}">
                    %if transferencia:
                        %if factura['payment_type'] != 'ADMINISTRACIO I GRANS CLIENTS':
                            <table id="table-bottom">
                                <tr>
                                    <td>
                                        <div>
                                            %if image_energy_origin:
                                                <img src="data:image/jpeg;base64,${image_logo_code_bar}" width="40px">
                                            %endif
                                        </div>
                                    </td>
                                    <td>
                                        <table id="table-bottom-1">
                                            <thead>
                                                <tr>
                                                    <th class="left">${_(u"RESGUARD PER L'ENTITAT BANCÀRIA")}</th>
                                                    <th class="right">${_(u"FACTURA DE SUBMINISTRAMENT")}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td colspan="2">
                                                        <table id="table-bottom-2">
                                                            <colgroup>
                                                                <col width="15%"/>
                                                            </colgroup>
                                                            <thead>
                                                                <tr>
                                                                    <th>${_(u"PERIODE DE PAGAMENT")}</th>
                                                                    <th>${_(u"EMISSORA")}</th>
                                                                    <th>${_(u"Nº DE REFERÈNCIA")}</th>
                                                                    <th>${_(u"IDENTIFICACIÓ")}</th>
                                                                    <th>${_(u"IMPORT")}</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td class="center">${factura['due_date']}</td>
                                                                    <td class="center">${comer['entity']}-${recibo507.suffix}</td>
                                                                    <td class="center">${factura['ref_bar_code']}${recibo507.checksum()}</td>
                                                                    <td class="center">${recibo507.notice}</td>
                                                                    <td class="right">${formatLang(import_factura)}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" rowspan="2">
                                                                        <table id="comer-data">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td>${holder['name']}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>${direccio_cups[0]}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>${direccio_cups[1]}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>CUPS ${cups_ae_formatted}</td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                    <td id="barcode-descr" class="center" colspan="3">
                                                                        ${_(u"CODI DE BARRES PER EFECTUAR EL PAGAMENT DES DELS EQUIPS AUTOSERVEI")}
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3" class="center">
                                                                        ${recibo507.svg(writer_options={'module_height': 10, 'font_size': 7, 'text_distance': 3, 'module_width': 0.25})}
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td id="pagament-descr" colspan="2">
                                                        ${code_bar_descr_full_mssg}
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        %endif
                    %else:
                        <table id="dades-pagament" class="styled-table">
                            <thead>
                                <tr>
                                    <th>${_(u"DADES DE PAGAMENT")}</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>${factura['payment_type']}</td>
                                </tr>
                                <tr>
                                    <td>${shadow_iban(factura['bank']['iban'], iban_indexes_to_hide)}</td>
                                </tr>
                                <tr>
                                    <td>(***) ${_(u"ocults per seguretat")}</td>
                                </tr>
                            </tbody>
                        </table>
                    %endif
                </div>
            </div>
            <!-- END FIRST PAGE -->
            <p style="page-break-after:always; clear:both"></p>
            <!-- SECOND PAGE -->
            <div id="second-page">
                <table id="about-my-electricity" class="styled-table">
                    <thead>
                        <tr>
                            <th>${_(u"INFORMACIÓ SOBRE LA SEVA ELECTRICITAT")}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <p>${_(u"Si bé l'energia eléctrica que arriba a les nostres cases és indistingible de la que consumeixen els nostres veïns o altres consumidors connectats al mateix sistema elèctric, ara sí és possible garantitzar l'orígen de la producció de l'energía elèctrica que vostè consumeix.")}</p>
                                <p>${_(u"A aquests efectes es proporciona el detall de la barreja de tecnologies de producció nacional per tal de poder comparar els percentatges del promig nacional amb els corresponents a l'energia venuda per la seva companyia comercialitzadora.")}</p>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <!-- ORIGEN DE L'ENERGIA -->
                <div id="energy-origin">
                    %if image_energy_origin:
                        <img src="data:image/jpeg;base64,${image_energy_origin}">
                    %endif
                </div>
                <!-- END ORIGEN DE L'ENERGIA -->
                <!-- IMPACTE AMBIENTAL -->
                <div id="environmental-impact">
                    %if image_environmental_impact:
                        <img src="data:image/jpeg;base64,${image_environmental_impact}">
                    %endif
                </div>
                <!-- END IMPACTE AMBIENTAL -->
                <!-- DETALL FACTURACIO -->
                <div id="detall-facturacio">
                    <table class="styled-table">
                        <colgroup>
                            <col width="28%"/>
                            <col/>
                            <col/>
                            <col/>
                            <col width="4%"/>
                            <col width="2%"/>
                            <col width="8%"/>
                            <col width="12%"/>
                            <col width="2%"/>
                            <col width="9%"/>
                        </colgroup>
                        <thead>
                            <tr>
                                <th>${_(u"DETALL FACTURACIÓ")}</th>
                                <td id="producte-cell" class="bold" colspan="9">${_(u"Producte")}: ${factura['pricelist'].split(" ")[0]}</td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="row-separator">
                                <%
                                    rowspan_power_term = len(detailed_lines['power']['lines'])
                                %>
                                <td class="bold" rowspan="${rowspan_power_term}">${_(u"Terme de potència")}</td>
                                % for index, power_line in enumerate(detailed_lines['power']['lines']):
                                    <%
                                        total_power_toll += power_line['toll']
                                    %>
                                    <td class="center italic">${periods_translations.get(power_line['name'], power_line['name'])}</td>
                                    <td>${int(power_line['time'])} dies x</td>
                                    <td class="right white">${formatLang(power_line['consumption'], digits=3)}</td>
                                    <td>kW</td>
                                    <td>x</td>
                                    <td class="right white">${formatLang(power_line['price'], digits=6)}</td>
                                    <td>€/kW-dia</td>
                                    <td>=</td>
                                    <td class="right white bold">${formatLang(power_line['total'])} €</td>
                                    %if index >= 0:
                                        </tr>
                                        <tr>
                                    %endif
                                % endfor
                            </tr>
                            <tr>
                                <%
                                    rowspan_energy_term = len(detailed_lines['energy']['lines'])
                                %>
                                <td class="bold" rowspan="${rowspan_energy_term}">${_(u"Terme d'energia activa")}</td>
                                % for index, energy_line in enumerate(detailed_lines['energy']['lines']):
                                    <%
                                        total_energy_toll += energy_line['toll']
                                    %>
                                    <td class="center italic">${periods_translations.get(energy_line['name'], energy_line['name'])}</td>
                                    <td></td>
                                    <td class="right">${formatLang(int(energy_line['consumption']))}</td>
                                    <td>kWh</td>
                                    <td>x</td>
                                    <td class="right">${formatLang(energy_line['price'], digits=6)}</td>
                                    <td>€/kWh</td>
                                    <td>=</td>
                                    <td class="right bold">${formatLang(energy_line['total'])} €</td>
                                    %if index >= 0:
                                        </tr>
                                        <tr>
                                    %endif
                                % endfor
                            </tr>
                            % for iese_line in taxes['iese']['lines']:
                                <%
                                    # Solucion cutre FTW. No hagais esto en casa niños:
                                    if language == 'ca_ES':
                                        iese_name = iese_line['name'].replace("Impuesto", "Impost").replace("la electricidad", "l'electricitat")
                                    else:
                                        iese_name = iese_line['name']
                                %>
                                <tr class="row-separator">
                                    <td class="bold">${iese_name}</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td colspan="3" class="right">5,11269632% x</td>
                                    <td class="right">${formatLang(iese_line['base_amount'])} €</td>
                                    <td>=</td>
                                    <td class="bold right">${formatLang(iese_line['tax_amount'])} €</td>
                                </tr>
                            % endfor
                            %for meter_rent in meters_rent:
                                <tr class="row-separator">
                                    <td class="bold">${_(u"Gestió de la mesura")}</td>
                                    <td></td>
                                    <td></td>
                                    <td class="right">${int(meter_rent['days'])}</td>
                                    <td>${_(u"dies")}</td>
                                    <td>x</td>
                                    <td></td>
                                    <td class="right">${formatLang(meter_rent['price'], digits=6)} €/${_(u"dia")}</td>
                                    <td>=</td>
                                    <td class="bold right">${formatLang(meter_rent['amount'])} €</td>
                                </tr>
                            %endfor
                            %if other_lines['lines']:
                                <tr class="row-separator">
                                    <% rowspan = len(other_lines['lines']) + 1 %>
                                    <td class="bold" rowspan="${rowspan}">${_(u"Altres")}</td>
                                </tr>
                                % for other_line in other_lines['lines']:
                                    <tr class="row-separator">
                                        <td colspan="8">${other_line['name']}</td>
                                        <td class="bold right">${formatLang(other_line['price'])} €</td>
                                    </tr>
                                % endfor
                            %endif
                            <%
                                if len(taxes['iva']['lines']) != 1:
                                    raise NotImplementedError
                            %>
                            <tr></tr>
                            % for iva_line in taxes['iva']['lines']:
                                <tr class="row-separator">
                                    <td colspan="4" class="top-border-white"></td>
                                    <td colspan="4" class="bold bold-top-border">${_(u"BASE IMPOSABLE")}</td>
                                    <td colspan="2" class="bold right bold-top-border">${formatLang(iva_line['base'])} €</td>
                                </tr>
                                <tr>
                                    <td colspan="4" class="top-border-white"></td>
                                    <td colspan="4" class="bold">${iva_line['name']}</td>
                                    <td colspan="2" class="bold right">${formatLang(iva_line['amount'])} €</td>
                                </tr>
                            % endfor
                            <tr class="row-separator">
                                <td colspan="4" class="top-border-white"></td>
                                <td colspan="4" class="bold bold-bottom-border">${_(u"IMPORT TOTAL")}</td>
                                <td colspan="2" class="bold right bold-bottom-border">${formatLang(import_factura)} €</td>
                            </tr>
                            % if variant_aplicada:
                                <tr class="row-separator">
                                    <td colspan="4" class="top-border-white"></td>
                                    <td colspan="4" class="bold bold-bottom-border">${_(u"Abonament")}</td>
                                    <td colspan="2" class="bold right bold-bottom-border">${formatLang(factura['descompte_variants'])} €</td>
                                </tr>
                                <tr class="row-separator">
                                    <td colspan="4" class="top-border-white"></td>
                                    <td colspan="4" class="bold bold-bottom-border">${_(u"IMPORT A PAGAR")}</td>
                                    <td colspan="2" class="bold right bold-bottom-border">${formatLang(import_a_pagar)} €</td>
                                </tr>
                            % endif
                        </tbody>
                    </table>
                    <!-- DESTI
                    <div id="detall-facturacio-bottom">
                        <table id="desti-factura" class="styled-table">
                            <thead>
                                <tr>
                                    <th class="seccio">${_(u"DESTÍ DE LA FACTURA")}</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>${_(u"El destí de l'import de la seva factura ({} €) és el següent:").format(formatLang(import_factura))}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="chart_desti" id="chart_desti_${factura['id']}"></div>
                                    </td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td class="italic center">${_(u"Als imports indicats en el diagrama se'ls ha d'afegir, en el seu cas, el lloguer dels equips de mesura i control")}</td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                     END DESTI -->
                     % if fact_obj.has_tec271_line(cursor, uid, factura['id']):
                        <table id="dades-suplaments" class="styled-table" style="padding-bottom:0px; margin-bottom:0px; ">
                            <thead>
                                <tr>
                                    <th>${_(u"TAULA DETALLADA DELS SUPLEMENTS AUTONÒMICS 2013 ")}<span style="font-size: 12px; margin-left: 10px;">${(" (Segons informació de la CNMC)")}</span></th>
                                </tr>
                            </thead>
                            <%
                                polissa_id = fact_obj.read(cursor, uid, factura['id'], ['polissa_id'])['polissa_id'][0]
                                polissa_info = polissa_obj.read(cursor, uid, polissa_id, ['cups', 'distribuidora'])
                                cups = polissa_info['cups'][1]
                                distri_id = polissa_info['distribuidora'][0]
                                distri_name = partner_obj.read(cursor, uid, distri_id, ['name'])['name']
                            %>
                            <tbody>
                            <style>
                                  table.supl td, table.supl th {
                                    font-size: 6px !important;
                                  }
                            </style>
                                <%
                                    html_table = sup_territorials_tec_271_comer_obj.get_info_html(cursor, uid, cups)
                                %>
                                ${html_table}
                                <div style="text-align: justify; font-size:7px !important;" class="supl_dom">
                                En caso de que el importe total de la regularización supere los dos euros, sin incluir impuestos, el mismo será fraccionado en partes iguales superiores a 1€ por las empresas comercializadoras en las facturas que se emitan en el plazo de 12 meses a partir de la primera regularización
                                Tabla informativa conforme a lo establecido en la TEC/271/2019 de 6 de marzo, por la cual le informamos de los parámetros para el cálculo de los suplementos territoriales facilitados por su empresa distribuidora ${distri_name}
                                <%
                                    text = sup_territorials_tec_271_comer_obj.get_info_text(cursor, uid, cups)
                                    if text:
                                        text = text.format(distribuidora=distri_name)
                                %>
                                ${text}
                                </div>
                            </tbody>
                        </table>
                % endif
                </div>
                <!-- END DETALLE FACTURA -->
                <!-- INFORMACIO INTERES -->
                <div id="second-page-bottom">
                    <table id="footer-detall-facturacio">
                        <tr>
                            <%
                                total_toll = total_power_toll + total_energy_toll
                                power_toll_percentage = 0
                                energy_toll_percentage = 0
                                if total_toll:
                                    power_toll_percentage = round(total_power_toll / total_toll * 100, 2)
                                    energy_toll_percentage = round(total_energy_toll / total_toll * 100, 2)

                                footer_invoicing_detail_full_mssg = footer_invoicing_detail_mssg.format(
                                    formatLang(total_toll),
                                    formatLang(power_toll_percentage),
                                    formatLang(energy_toll_percentage),
                                    formatLang(factura['rent'])
                                )
                            %>
                            <td class="italic" colspan="10">${footer_invoicing_detail_full_mssg}</td>
                        </tr>
                        <tr>
                            <td class="italic" colspan="10">${invoice_destination_full_mssg}</td>
                        </tr>
                    </table>
                    <table id="for-your-information">
                        <colgroup>
                            <col width="30%"/>
                            <col />
                            <col width="30%"/>
                        </colgroup>
                        <thead>
                            <tr>
                                <td><div></div></td>
                                <th class="center">${_(u"INFORMACIÓ DEL SEU INTERÈS")}</th>
                                <td><div></div></td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="3" class="center">${_(u"Atenció al client (Agri-Energia, S.A.)")} ${comer['phone']} ${comer['email']}</td>
                            </tr>
                            <tr>
                                <td colspan="3" class="center">${_(u"Per qüestions relacionades amb contractació i les seves incidències")} 900 72 72 72</td>
                            </tr>
                            <tr>
                                <td colspan="3" class="center">${_(u"Avaries i incidències de seguretat a les instal·lacions de distribució (")}${supplier['name']}): ${supplier['phone']}</td>
                            </tr>
                            <tr>
                                <td colspan="3" class="center">${_(u"Per reclamacions sobre el contracte de subministrament o facturacions podrà dirigir-se a la nostra oficina d'Agri-Energia S.A.")} (${comer['street']}, ${comer['zip']} ${comer['poblacio']}) ${_(u"o a l'Agència Catalana de Consum (ACC) en el telèfon 012 o a través de la seva pàgina web www.icconsum.org")})</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END SECOND PAGE -->
            <script>
                // BAR PLOT VARIABLES
                var barplot_height = 170;
                var barplot_width = 340;

                var grad_colors_1 = [['#faffb7', '#d3d800']];
                var grad_colors_3 = [
                    ['#faffb7', '#d3d800'], ['#faffb7', '#d3d800'], ['#faffb7', '#d3d800'],
                    ['#faffb7', '#d3d800'], ['#faffb7', '#d3d800'], ['#faffb7', '#d3d800']
                ];

                var factura_id = ${factura['id']};
                var data_consum = ${json.dumps(sorted(historic['historic_js'], key=lambda h: h['mes']))};

                // PIE CHART VARIABLES
                var piechart_height = 180;
                var piechart_width = 600;

                //pie start_position
                var pie_left = 150;

                var pie_total = ${pie['total']};
                var pie_data = [
                    {val: ${pie_regulats}, perc: 30, code: "REG"},
                    {val: ${pie_costos}, perc: 55, code: "PROD"},
                    {val: ${pie_impostos},  perc: 15 ,code: "IMP"}
                ];

                var pie_etiquetes = {
                    'REG': {t: ['${formatLang(float(pie_regulats))} €','${_(u"Costos regulats")}'], w:100},
                    'IMP': {t: ['${formatLang(float(pie_impostos))} €','${_(u"Impostos aplicats")}'], w:100},
                    'PROD': {t: ['${formatLang(float(pie_costos))} €','${_(u"Costos de producció electricitat")}'], w:150}
                };

                var reparto = ${json.dumps(pie['reparto'])};
                var dades_reparto = ${json.dumps(dades_reparto)};

                renderHistoricChart('#consumption-bar-plot', ${json.dumps(columns)}, ${json.dumps(y_axis)}, ${json.dumps(colors)}, 200, 350, 10, ${rotate_x}, false);
            </script>
            <script src="${addons_path}/giscedata_facturacio_comer/report/assets/consumption-chart.js"></script>
            <script src="${addons_path}/giscedata_facturacio_comer/report/assets/pie-chart.js"></script>
            % if factura != factura_data[-1]:
                <p style="page-break-after:always; clear:both"></p>
            % endif
        %endfor

    </body>
</html>
