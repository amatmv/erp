# -*- coding: utf-8 -*-

from giscedata_facturacio_comer.report.giscedata_facturacio_comer import FacturaReportWebkitParserHTML, FacturaReportWebkitHTML, OPTIMIZED_INVOICES
import tempfile
import pypdftk
import pooler
from c2c_webkit_report import webkit_report
from report import report_sxw


class AgriFacturaReportWebkitParserHTML(FacturaReportWebkitParserHTML):

    def __init__(self, name, table, rml=False, parser=False, header=True, store=False, factura_conceptes_path=None):

        factura_domestica_path = 'giscedata_facturacio_comer_agri/report/factura_domestica.mako'
        factura_industrial_path = 'giscedata_facturacio_comer_agri/report/factura_industrial.mako'

        if factura_domestica_path not in OPTIMIZED_INVOICES:
            OPTIMIZED_INVOICES.append(factura_domestica_path)

        if factura_industrial_path not in OPTIMIZED_INVOICES:
            OPTIMIZED_INVOICES.append(factura_industrial_path)

        super(AgriFacturaReportWebkitParserHTML, self).__init__(
            name, table, rml, parser, header, store
        )


    def create(self, cursor, uid, ids, data, context=None):
        if context is None:
            context = {}
        pool = pooler.get_pool(cursor.dbname)
        factura_o = pool.get('giscedata.facturacio.factura')
        to_join = []
        factura_vs = factura_o.q(cursor, uid).read(['tarifa_acces_id.name']).where([('id', 'in', ids)])

        ir_obj = pool.get('ir.actions.report.xml')
        report_xml_ids = ir_obj.search(
            cursor, uid, [('report_name', '=', self.name[7:])], context=context
        )

        for factura_v in factura_vs:
            if factura_v['tarifa_acces_id.name'] in ['2.0A', '2.1A', '2.0DHA', '2.0DHS', '2.1DHA', '2.1DHS']:
                report_path = 'giscedata_facturacio_comer_agri/report/factura_domestica.mako'
            else:
                report_path = 'giscedata_facturacio_comer_agri/report/factura_industrial.mako'

            ir_obj.write(
                cursor, uid, report_xml_ids, {'report_webkit': report_path},
                context=context
            )

            res = super(FacturaReportWebkitParserHTML, self).create(
                cursor, uid, [factura_v['id']], data, context=context
            )

            if res[1] != 'pdf':
                continue
            res_path = tempfile.mkstemp('-join.pdf', 'report-')[1]
            with open(res_path, 'w') as f:
                f.write(res[0])

            to_join.append(res_path)

        if len(to_join) > 1:
            out = pypdftk.concat(files=to_join)
        else:
            out = to_join[0]

        with open(out, 'r') as f:
            content = f.read()

        return content, 'pdf'


AgriFacturaReportWebkitParserHTML(
    'report.giscedata.facturacio.factura',
    'giscedata.facturacio.factura',
    'giscedata_facturacio_comer_agri/report/factura.mako',
    parser=FacturaReportWebkitHTML
)

webkit_report.WebKitParser(
    'report.giscedata.facturacio.factura.carta.requeriment',
    'giscedata.facturacio.factura',
    'giscedata_facturacio_comer_agri/report/carta_requeriment.mako',
    parser=report_sxw.rml_parse
)

webkit_report.WebKitParser(
    'report.giscedata.facturacio.factura.segona.carta.requeriment',
    'giscedata.facturacio.factura',
    'giscedata_facturacio_comer_agri/report/carta_requeriment.mako',
    parser=report_sxw.rml_parse
)

webkit_report.WebKitParser(
    'report.giscedata.facturacio.factura.carta.tall.bo.social',
    'giscedata.facturacio.factura',
    'giscedata_facturacio_comer_agri/report/carta_requeriment.mako',
    parser=report_sxw.rml_parse
)

webkit_report.WebKitParser(
    'report.giscedata.facturacio.factura.carta.tall',
    'giscedata.facturacio.factura',
    'giscedata_facturacio_comer_agri/report/carta_tall.mako',
    parser=report_sxw.rml_parse
)

webkit_report.WebKitParser(
    'report.giscedata.facturacio.factura.carta.pagament',
    'giscedata.facturacio.factura',
    'giscedata_facturacio_comer_agri/report/carta_pagament.mako',
    parser=report_sxw.rml_parse
)
