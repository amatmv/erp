## -*- coding: utf-8 -*-
<%
    import babel
    from datetime import datetime, timedelta
    from bankbarcode.cuaderno57 import Recibo507
    from account_invoice_base.report.utils import localize_period
    from workalendar.europe import Spain
    from account_invoice_base.account_invoice import amount_grouped

    calendar = Spain()

    report_obj = pool.get('giscedata.facturacio.carta.requeriment.report')
    imd_obj = pool.get('ir.model.data')
    account_invoice_obj = pool.get('account.invoice')
    ncarta = context['carta']
    if ncarta == 1:
        tipo_carta = 'req1'
    elif ncarta == 2:
        tipo_carta = 'req2'
    else:
        tipo_carta = 'corte_bs'
%>

<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
        <title>Carta de Tall</title>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_agri/report/estils_agri.css"/>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_agri/report/estils_carta_tall.css"/>
        <style>
            @font-face {
                font-family: "Roboto-Regular";
                src: url("${assets_path}/fonts/Roboto/Roboto-Regular.ttf") format('truetype');
                font-weight: normal;
            }
        </style>
    </head>
    <body>
        <%def name="get_codi_barres(factura)">
            <%
                # bar code
                from datetime import datetime, timedelta
                import time
                entity = ''.join([c for c in factura.polissa_id.comercialitzadora.vat if c.isdigit()])
                suffix = '500'
                due_date = data_pago

                ref = '{}'.format(factura.id).zfill(11) # id factura
                notice = '000000'
                grouped = amount_grouped(factura)
                amount = '{0:.2f}'.format(grouped) # amount grouped factura
                recibo507 = Recibo507(entity, suffix, ref, notice, amount, due_date)
                return recibo507
            %>
        </%def>
        %for factura in objects:
            <%
                data_inici = report_obj.get_start_day(
                    cursor, uid, factura.invoice_id.id, tipo_carta, context=context
                )[factura.invoice_id.id]

                if factura.lang_partner not in ['es_ES', 'ca_ES']:
                    factura_lang = 'ca_ES'
                else:
                    factura_lang = factura.lang_partner

                setLang(factura_lang)

                if ncarta == 1:
                    data_pago = datetime.strptime(str(calendar.add_working_days(data_inici, 10)), '%Y-%m-%d')
                elif ncarta == 2:
                    data_pago = datetime.strptime(str(calendar.add_working_days(data_inici, 10)), '%Y-%m-%d')
                else:
                    pending_obj = pool.get('account.invoice.pending.state.process')
                    bono_social_obj = pool.get('account.invoice.pending.state')
                    imd_obj = pool.get('ir.model.data')

                    tall_process = imd_obj.get_object_reference(
                        cursor, uid, 'giscedata_facturacio_comer_bono_social',
                        'bono_social_pending_state_process')[1]
                    avis_state = imd_obj.get_object_reference(
                        cursor, uid, 'giscedata_correos_bono_social',
                        'carta_tall_enviada_pending_state')[1]
                    avis_tipus = bono_social_obj.read(cursor, uid, avis_state, ['pending_days', 'pending_days_type'])
                    pending_days = avis_tipus['pending_days']
                    pending_type = avis_tipus['pending_days_type']
                    data_tall = pending_obj.get_cutoff_day(cursor, uid, tall_process, start_day=data_inici,
                        delta_days=pending_days, delta_type=pending_type, context={'sector': 'energia'})

                    data_pago = data_tall - timedelta(days=1)
                    data_pago = datetime.combine(data_pago, datetime.min.time())

                grouped = formatLang(amount_grouped(factura), monetary=True, digits=2)
            %>
            <div id="carta">
                <table id="cap">
                    <tr>
                        <td rowspan="2">
                            <img class="logo" height="35px" src="data:image/jpeg;base64,${company.logo}"/>
                        </td>
                        <td>
                            <img class="icona" src="${addons_path}/giscedata_facturacio_comer_agri/report/icones/phone.png"/>
                        </td>
                        <td>
                            972 58 00 58
                        </td>
                        <td>
                            <a href="www.agrienergia.com">
                                <img class="icona" src="${addons_path}/giscedata_facturacio_comer_agri/report/icones/world.png"/>
                            </a>
                        </td>
                        <td>
                            <a href="www.agrienergia.com">www.agrienergia.com</a>
                        </td>
                        <td>
                            <a href="https://www.facebook.com/agrienergia">
                                <img class="icona" src="${addons_path}/giscedata_facturacio_comer_agri/report/icones/fb.png"/>
                            </a>
                        </td>
                        <td>
                            <img class="icona" src="${addons_path}/giscedata_facturacio_comer_agri/report/icones/in.png"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="mailto:oficina@agrienergia.com">
                                <img class="icona" src="${addons_path}/giscedata_facturacio_comer_agri/report/icones/mail.png"/>
                            </a>
                        </td>
                        <td>
                            <a href="mailto:oficina@agrienergia.com">oficina@agrienergia.com</a>
                        </td>
                        <td>
                            <img class="icona" src="${addons_path}/giscedata_facturacio_comer_agri/report/icones/marker.png"/>
                        </td>
                        <td>
                            Av. Països Catalans, 140 · 17820 Banyoles
                        </td>
                        <td>
                            <a href="https://twitter.com/agri_energia">
                                <img class="icona" src="${addons_path}/giscedata_facturacio_comer_agri/report/icones/tw.png"/>
                            </a>
                        </td>
                        <td>
                            <img class="icona" src="${addons_path}/giscedata_facturacio_comer_agri/report/icones/insta.png"/>
                        </td>
                    </tr>
                </table>
                %if ncarta < 3:
                    <h1 class="center"> AVÍS DE TALL ${ncarta}</h1> <br/>
                %else:
                    <h1 class="center"> CARTA DE TALL</h1> <br/>
                %endif

               <div>
                   <div class="left-50 ">
                     <h2> Dades del contracte </h2>
                   </div>
                   <div class="right-35">
                     <h2> Dades de notificació </h2>
                   </div>
               </div>
                <div>
                    <div class="left-50 contact_info">
                        <div class="">
                            <table>
                               <tr>
                                    <td>
                                        <strong>Titular:</strong> ${factura.polissa_id.titular.name}
                                    </td>
                                </tr>
                               <tr>
                                    <td>
                                        <strong>NIF:</strong> ${factura.polissa_id.titular.vat.replace('ES','')}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${factura.cups_id.tv and factura.cups_id.tv.abr or ''} ${factura.cups_id.nv or ''} ${factura.cups_id.pnp or ''} ${factura.cups_id.es or ''} ${factura.cups_id.pt or ''} ${factura.cups_id.pu or ''}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                         ${factura.cups_id.cpo or ''} ${factura.cups_id.cpa or ''} ${factura.cups_id.dp or ''}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${factura.cups_id.id_municipi.name + ',' or ''} ${factura.cups_id.id_poblacio.name or ''}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${factura.cups_id.aclarador or ''}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p id="referencia">CUPS: ${factura.cups_id.name}</p>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="right-35 contact_info">
                        <div class="">
                            <table>
                                <tr>
                                    <td>
                                        ${factura.address_contact_id.name}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${factura.address_contact_id.street or ''}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${factura.address_contact_id.street2 or ''}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${factura.address_contact_id.zip or ''} ${factura.address_contact_id.city or ''}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <%
                    ##city = company.partner_id.city.capitalize() or ''
                    city = 'Banyoles'
                %>
                <div style="clear:both;" ></div>
                ${(city + ", ")} ${babel.dates.format_datetime(data_inici, "d LLLL 'de' YYYY,", locale='ca_ES')}
                <p>
                    ${_(u"Benvolgut client,")}
                </p>
                <div id="resum" class="box-border">
                    <p>
                        ${_(u"Mitjançant la present, se li requereix el pagament de les quantitats degudes en concepte de consum d'energia elèctrica.")}
                        <br/>
                        ${_(u"A dia d'avui, no ha satisfet el pagament de la ")}
                        <b>${_(u"factura número")} ${factura.number}</b>
                        ${_(u"emesa amb data ")} ${formatLang(factura.date_invoice, date=True)}
                        ${_(u", que ascendeix a un")}
                        <b>
                            ${_(u"import de ")}
                            ${grouped}
                        </b>
                        <br/>
                        ${_(u"En cas de no abonar-se la quantitat deguda,")}

                        %if ncarta in [1, 2]:
                            ${_(u"en un termini de 2 mesos a partir de la notificació del present requeriment,")}
                            <b>
                                ${_(u"l'empresa distribuïdora podrà suspendre el seu subministrament d'electricitat.")} <br/>
                                    ${_(u" Pot fer efectiu el pagament amb aquesta carta abans de ")}
                                    ${data_pago.strftime('%d/%m/%Y')}
                        %else:
                            ${_(u"a partir del dia")} ${data_tall.strftime('%d/%m/%Y')}
                            ${_(u"l'empresa distribuïdora podrà suspendre el seu subministrament d'electricitat.")}
                            <br/>
                                    ${_(u" Pot fer efectiu el pagament amb aquesta carta abans de ")}
                                    ${data_pago.strftime('%d/%m/%Y')}
                        %endif
                        </b>
                    </p>
                </div>
                <p>
                    %if ncarta == 3:
                        ${_(u"Li informem que, en cas de tall i en concepte de reconnexió, l'empresa distribuïdora li carregarà la quantitat de 21,89€ en la propera factura d'electricitat.")}
                    %else:
                        ${_(u"Li informem que, en concepte de gestió d'aquest impagament, se li carregarà la quantitat de 3,99€ en la propera factura d'electricitat.")} <br/>
                    %endif
                    <br/>
                    ${_(u"Sense perjudici de lo anterior, si vostè compleix els requisits per a ser consumidor vulnerable, pot sol·licitar a una de les empreses comercialitzadores de referència acollir-se al bo social, que suposa un descompte sobre el preu voluntari per al petit consumidor (PVPC).")}
                    <br/>
                    ${_(u"El canvi de modalitat en el contrace per passar a PVPC, sempre que no es modifiquin els paràmetres recollits en el contracte d'accés de tercers a la xarxa, es durà a terme sense cap tipus de penalització ni cost addicional.")}
                    <br/>
                    ${_(u"Sempre que s’hagin acreditat els requisits per a ser consumidor vulnerable, una vegada acollit al PVPC, el termini perquè el seu subministrament d’electricitat pugui ser suspès, de no haver estat abonada la quantitat deguda, passarà a ser de")}
                    <b>${_(u"4 mesos")}</b>
                    ${_(u"(contats sempre des de la recepció del present requeriment fefaent de pagament).")}
                    <br/>
                    ${_(u"L'enllaç a la pàgina web de la CNMC on trobarà les dades necessàries per contactar amb la comercialitzadora de referència és la següent:")}
                </p>
                <p>
                    <a href="https://www.cnmc.es/ambitos-de-actuacion/energia/mercado-electrico#listados">https://www.cnmc.es/ambitos-de-actuacion/energia/mercado-electrico#listados</a>
                </p>
                <p>
                    ${_(u"Addicionalment, si vostè compleix els requisits per a ser vulnerable sever, pot posar-se en contacte amb els serveis socials del municipi i comunitat autònoma on resideix, per tal que l’informin sobre la possibilitat d’atendre el pagament del seu subministrament d’electricitat.")}
                    <br/>
                    ${_(u"Els requisits per a ser consumidor vulnerable estan recollits en l’article 3 del Reial Decret 897/2017, de 6 d’octubre, pel qual es regula la figura del consumidor vulnerable, el bo social i altres mesures de protecció pels consumidors domèstics d’energia elèctrica. Els pot trobar al següent enllaç:")}
                </p>
                <p>
                    <a href="https://www.boe.es/diario_boe/txt.php?id=BOE-A-2017-11505">https://www.boe.es/diario_boe/txt.php?id=BOE-A-2017-11505</a>
                </p>
                <p>
                    ${_(u"Atentament.")} <br/>
                    Agri-Energia, SA
                </p>
            </div>
                %if True:
                    <hr class="ratllat"/>
                    <div class="float_left">
                        <img id="imatge_pagament" width="50px" alt="AgriEnergia" src="${addons_path}/giscedata_facturacio_comer_agri/report/icones/logo.png"/>
                    </div>
                    <%
                        recibo507 = get_codi_barres(factura)
                    %>
                    <div>
                        <table id="taula_peu">
                            <tr>
                                <th colspan="4" class="left">${_(u"RESGUARD PER L'ENTITAT BANCÀRIA")}</th>
                                <th>${_(u"CARTA DE PAGAMENT")}</th>
                            </tr>
                            <tr>
                                <th>
                                    ${_(u"PERIODE DE PAGAMENT")}
                                </th>
                                <th>
                                    ${_(u"EMISSORA")}
                                </th>
                                <th>
                                    ${_(u"Nº DE REFERÈNCIA")}
                                </th>
                                <th>
                                    ${_(u"IDENTIFICACIÓ")}
                                </th>
                                <th>
                                    ${_(u"IMPORT")}
                                </th>
                            </tr>
                            <tr>
                                <td>
                                    ${data_pago.strftime('%d/%m/%Y')}
                                </td>
                                <td>
                                    ${recibo507.entity} - ${recibo507.suffix}
                                </td>
                                <td>
                                    ${recibo507.ref}${recibo507.checksum()}
                                </td>
                                <td>
                                    ${recibo507.notice}
                                </td>
                                <td>
                                    ${grouped}
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div>
                        <div style="text-align:center; padding-right:80px" class="float_right">
                            <span class="font_10px"> ${_(u"CODI DE BARRES PER EFECTUAR EL PAGAMENT")} </span>
                            <br/>
                            ${ recibo507.svg(writer_options={'module_height': 10, 'font_size': 7, 'text_distance': 3, 'module_width': 0.30}) }
                        </div>
                        <div class="payment_info float_left">
                            <table style="margin-left: 50px;">
                                <tr>
                                    <td>
                                        ${factura.polissa_id.titular.name}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${factura.address_contact_id.street or ''}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${factura.address_contact_id.street2 or ''}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${factura.address_contact_id.zip or ''} ${factura.address_contact_id.city or ''}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${factura.polissa_id.name}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        CUPS: ${factura.cups_id.name}
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="float_left" id="condicions">
                        <div id="condicions_carta_pagament">
                            <b>${_(u"Pagament:")}</b>
                            ${_(u"A través de qualsevol de les següents entitats bancàries (Banc Santander, Banco Popular), en el seu horari de pagament, presentant a aquest escrit a finestreta fins el dia")}
                            <span class="red">${data_pago.strftime('%d/%m/%Y')}</span>
                            ${_(u"i (BBVA i La Caixa), en el caixer automàtic a qualsevol hora.")}
                        </div>
                    </div>
            %else:
                <div id="">
                    <img src="${addons_path}/giscedata_facturacio_comer_agri/report/icones/footer.svg" alt="footer" height="213px">
                </div>
            %endif
            % if factura != objects[-1]:
                <p style="page-break-after:always; clear:both"></p>
            % endif
        %endfor
    </body>
</html>
