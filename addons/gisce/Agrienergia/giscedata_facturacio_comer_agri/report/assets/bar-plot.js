function renderHistoricChart(div_id, periodes, mesos, colors, height, width, bar_width, rotate, rotate_bar_labels) {
    var bw = bar_width;
    if(bar_width < 0){
        bw = {
            ratio: bar_width
        }
    }

    var data = {
        onrendered: function () {
            d3.select("g")
                .attr("transform", function(d){
                    return "translate(20, 4.5)"
                })
            ;
        },
        bindto: div_id,
        size: {
            height: height,
            width: width
        },
        data: {
            columns: periodes,
            type: 'bar',
            labels: {
                format: function (v, id, i, j) { return formatNumber(v) }
            },
            colors: colors
        },
        legend: {
            show: false
        },
        bar: {
            width: bw
        },
        axis: {
            x:{
                type: 'category',
                categories: mesos,
                tick: {
                    rotate: rotate,
                    multiline: false
                }
            },
            y: {
                label: 'kWh'
            }
        }
    };

    if(rotate_bar_labels){
        data.onrendered = function () {
            d3.select("g")
                .attr("transform", function(d){
                    return "translate(20, 4.5)"
                })
            ;
            d3.selectAll(".c3-chart-texts text.c3-text")
                .attr("transform", function(d) {
                    var textSel = d3.select(this);
                    return "rotate(-90, "+textSel.attr("x")+", "+(textSel.attr("y"))+")";
                })
                .style("text-anchor", function(d) {
                    return (d.value && d.value > 0) ? "end" : "centre";
                })
            ;
        }
    }

    return c3.generate(data);
}