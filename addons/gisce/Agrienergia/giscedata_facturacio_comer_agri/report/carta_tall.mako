## -*- coding: utf-8 -*-
<%
    import babel
    from datetime import datetime, timedelta
    from bankbarcode.cuaderno57 import Recibo507
    from account_invoice_base.report.utils import localize_period
    from giscedata_facturacio_impagat.giscedata_facturacio import find_B1
    from workalendar.europe import Spain
    from account_invoice_base.account_invoice import amount_grouped

    report_obj = pool.get('giscedata.facturacio.carta.requeriment.report')
    imd_obj = pool.get('ir.model.data')
    account_invoice_obj = pool.get('account.invoice')

    def obtenir_dia_pagament(date_due):
        origin_date = datetime.strptime(date_due, '%Y-%m-%d')
        calendar = Spain()
        new_date = calendar.add_working_days(origin_date, delta=-1)
        return new_date
%>

<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
        <title>Carta de Tall</title>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_agri/report/estils_agri.css"/>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_agri/report/estils_carta_tall.css"/>
        <style>
            @font-face {
                font-family: "Roboto-Regular";
                src: url("${addons_path}/giscedata_facturacio_comer/report/assets/fonts/roboto/Roboto-Regular.ttf") format('truetype');
                font-weight: normal;
            }
        </style>
    </head>
    <body>
        <%def name="get_codi_barres(factura)">
            <%
                # bar code
                from datetime import datetime, timedelta
                import time
                entity = ''.join([c for c in factura.polissa_id.comercialitzadora.vat if c.isdigit()])
                suffix = '500'
                due_date = datetime.strptime(str(fecha_pago), '%Y-%m-%d')

                ref = '{}'.format(factura.id).zfill(11) # id factura
                notice = '000000'
                grouped = amount_grouped(factura)
                amount = '{0:.2f}'.format(grouped) # amount grouped factura
                recibo507 = Recibo507(entity, suffix, ref, notice, amount, due_date)
                return recibo507
            %>
        </%def>
        %for factura in objects:
            <%
                b1_ids = find_B1(cursor, uid, factura.id, context={'only_cuts': 1})
            %>
            % if not b1_ids:
                % if len(objects) == 1:
                    <div id="contingut">
                        <h1>${_(u"No s'ha detectat cap tall programat per aquesta factura:")} ${factura.number or ''}</h1>
                    </div>
                % endif
                <%
                    continue
                %>
            %else:
                <%
                    b1_01_obj = pool.get('giscedata.switching.b1.01')
                    swh_ids = b1_01_obj.search(cursor, uid, [('sw_id', '=', b1_ids[0])], limit=1)
                    swh_init = b1_01_obj.browse(cursor, uid, swh_ids[0])
                    date_due = datetime.strptime(swh_init.data_accio, '%Y-%m-%d')
                    fecha_pago = obtenir_dia_pagament(swh_init.data_accio)
                    grouped = formatLang(amount_grouped(factura), monetary=True, digits=2)
                %>
            %endif
            <div id="carta">
                <table id="cap">
                    <tr>
                        <td rowspan="2">
                            <img class="logo" height="35px" src="data:image/jpeg;base64,${company.logo}"/>
                        </td>
                        <td>
                            <img class="icona" src="${addons_path}/giscedata_facturacio_comer_agri/report/icones/phone.png"/>
                        </td>
                        <td>
                            972 58 00 58
                        </td>
                        <td>
                            <a href="www.agrienergia.com">
                                <img class="icona" src="${addons_path}/giscedata_facturacio_comer_agri/report/icones/world.png"/>
                            </a>
                        </td>
                        <td>
                            <a href="www.agrienergia.com">www.agrienergia.com</a>
                        </td>
                        <td>
                            <a href="https://www.facebook.com/agrienergia">
                                <img class="icona" src="${addons_path}/giscedata_facturacio_comer_agri/report/icones/fb.png"/>
                            </a>
                        </td>
                        <td>
                            <img class="icona" src="${addons_path}/giscedata_facturacio_comer_agri/report/icones/in.png"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="mailto:oficina@agrienergia.com">
                                <img class="icona" src="${addons_path}/giscedata_facturacio_comer_agri/report/icones/mail.png"/>
                            </a>
                        </td>
                        <td>
                            <a href="mailto:oficina@agrienergia.com">oficina@agrienergia.com</a>
                        </td>
                        <td>
                            <img class="icona" src="${addons_path}/giscedata_facturacio_comer_agri/report/icones/marker.png"/>
                        </td>
                        <td>
                            Av. Països Catalans, 140 · 17820 Banyoles
                        </td>
                        <td>
                            <a href="https://twitter.com/agri_energia">
                                <img class="icona" src="${addons_path}/giscedata_facturacio_comer_agri/report/icones/tw.png"/>
                            </a>
                        </td>
                        <td>
                            <img class="icona" src="${addons_path}/giscedata_facturacio_comer_agri/report/icones/insta.png"/>
                        </td>
                    </tr>
                </table>

                <h1 class="center"> AVÍS IMPORTANT </h1> <br/>

                <div>
                    <div class="left-50 ">
                        <h2> Dades del contracte </h2>
                    </div>
                    <div class="right-35"></div>
                </div>
                <div>
                    <div class="left-50 contact_info">
                        <div class="">
                            <table>
                               <tr>
                                    <td>
                                        <strong>Titular:</strong> ${factura.polissa_id.titular.name}
                                    </td>
                                </tr>
                               <tr>
                                    <td>
                                        <strong>NIF:</strong> ${factura.polissa_id.titular.vat.replace('ES','')}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${factura.cups_id.nv or ''} ${factura.cups_id.pnp or ''} ${factura.cups_id.es or ''} ${factura.cups_id.pt or ''} ${factura.cups_id.pu or ''}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                         ${factura.cups_id.cpo or ''} ${factura.cups_id.cpa or ''} ${factura.cups_id.dp or ''}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${factura.cups_id.id_municipi.name + ',' or ''} ${factura.cups_id.id_poblacio.name or ''}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${factura.cups_id.aclarador or ''}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p id="referencia">CUPS: ${factura.cups_id.name}</p>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="right-35 contact_info">
                        <div class="">
                            <table>
                                <tr>
                                    <td>
                                        ${factura.address_contact_id.name}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${factura.address_contact_id.street or ''}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${factura.address_contact_id.street2 or ''}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${factura.address_contact_id.zip or ''} ${factura.address_contact_id.city or ''}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <%
                    ##city = company.partner_id.city.capitalize() or ''
                    city = 'Banyoles'
                %>
                <div style="clear:both;" ></div>
                ${(city + ", ")} ${babel.dates.format_datetime(datetime.now(), "d LLLL 'de' YYYY,", locale='ca_ES')}
                <p>
                    ${_(u"Benvolgut client,")}
                </p>
                <div id="resum" class="box-border">
                    <p>
                        ${_(u"Mitjançant la present, se li requereix el pagament de les quantitats degudes en concepte de consum d'energia elèctrica.")}
                        <br/>
                        ${_(u"A dia d'avui, no ha satisfet el pagament de la ")}
                        <b>${_(u"factura número")} ${factura.number}</b>
                        ${_(u"emesa amb data ")} ${factura.date_invoice},
                        ${_(u"que ascendeix a un")}
                        <b>
                            ${_(u"import de ")}
                            ${grouped}
                        </b>
                        <br/>
                        ${_(u"En cas de no abonar-se la quantitat deguda,")}
                        <b>
                            ${_(u"a partir del dia")}
                            ${date_due.strftime('%d/%m/%Y')},
                            ${_(u"l'empresa distribuïdora podrà suspendre el seu subministrament d'electricitat.")} <br/>
                            ${_(u"Pot fer efectiu el pagament amb aquesta carta abans del dia")}
                            ${fecha_pago.strftime('%d/%m/%Y')}.
                        </b>
                    </p>
                </div>
                <p>
                    ${_(u"Li preguem que a la major brevetat possible, i per tal d’evitar el tall d’electricitat, procedeixi a l’abonament de les quantitats degudes.")} <br/>
                </p>
                <p>
                    ${_(u"Tingui en compte que, una vegada efectuat el pagament i quan nosaltres tinguem constància del mateix, sol·licitarem la reposició del subministrament a l’empresa distribuïdora, en un termini d’un dia hàbil.")}
                </p>
                <p>
                    ${_(u"Així mateix, li informem que, en concepte de gestió d’aquest impagament, se li carregarà la quantitat de 3,99€ en la propera factura d’electricitat.")}
                </p>
                <p>
                    ${_(u"Li preguem que consideri sense efecte aquesta comunicació en el cas que, en el transcurs de la seva tramitació s’hagi abonat l’import degut, així com indicar-li que aquesta notificació no anul·la ni modifica els efectes propis d’altre notificació anterior per impagament que vostè pogués haver rebut.")}
                </p>
                <p>
                    ${_(u"Restem a la seva disposició per a qualsevol consulta o aclariment.")}
                </p>
                    ${_(u"Atentament,")} <br/>
                    Agri-Energia, SA
                </p>
            </div>
            <div id="carta_tall_espai"></div>
            <hr class="ratllat"/>
            <div class="float_left">
                <img id="imatge_pagament" width="50px" alt="AgriEnergia" src="${addons_path}/giscedata_facturacio_comer_agri/report/icones/logo.png"/>
            </div>
            <%
                recibo507 = get_codi_barres(factura)
            %>
            <div>
                <table id="taula_peu">
                    <tr>
                        <th colspan="4" class="left">${_(u"RESGUARD PER L'ENTITAT BANCÀRIA")}</th>
                        <th>${_(u"CARTA DE PAGAMENT")}</th>
                    </tr>
                    <tr>
                        <th>
                            ${_(u"PERIODE DE PAGAMENT")}
                        </th>
                        <th>
                            ${_(u"EMISSORA")}
                        </th>
                        <th>
                            ${_(u"Nº DE REFERÈNCIA")}
                        </th>
                        <th>
                            ${_(u"IDENTIFICACIÓ")}
                        </th>
                        <th>
                            ${_(u"IMPORT")}
                        </th>
                    </tr>
                    <tr>
                        <td>
                            ${fecha_pago.strftime('%d/%m/%Y')}
                        </td>
                        <td>
                            ${recibo507.entity} - ${recibo507.suffix}
                        </td>
                        <td>
                            ${recibo507.ref}${recibo507.checksum()}
                        </td>
                        <td>
                            ${recibo507.notice}
                        </td>
                        <td>
                            ${grouped}
                        </td>
                    </tr>
                </table>
            </div>
            <div>
                <div style="text-align:center; padding-right:80px" class="float_right">
                    <span class="font_10px"> ${_(u"CODI DE BARRES PER EFECTUAR EL PAGAMENT")} </span>
                    <br/>
                    ${ recibo507.svg(writer_options={'module_height': 6, 'font_size': 7, 'text_distance': 3, 'module_width': 0.35}) }
                </div>
                <div class="payment_info float_left">
                    <table style="margin-left: 50px;">
                        <tr>
                            <td>
                                ${factura.polissa_id.titular.name}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                ${factura.address_contact_id.street or ''}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                ${factura.address_contact_id.street2 or ''}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                ${factura.address_contact_id.zip or ''} ${factura.address_contact_id.city or ''}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                ${factura.polissa_id.name}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                CUPS: ${factura.cups_id.name}
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="float_left" id="condicions">
                <div id="condicions_carta_pagament">
                    <b>${_(u"Pagament:")}</b>
                    ${_(u"A través de qualsevol de les següents entitats bancàries (Banc Santander, Banco Popular), en el seu horari de pagament, presentant a aquest escrit a finestreta fins el dia")}
                    <span class="red">${fecha_pago.strftime('%d/%m/%Y')}</span>
                    ${_(u"i (BBVA i La Caixa), en el caixer automàtic a qualsevol hora.")}
                </div>
            </div>
            % if factura != objects[-1]:
                <p style="page-break-after:always; clear:both"></p>
            % endif
        %endfor
    </body>
</html>
