# -*- coding: utf-8 -*-
{
    "name": "Facturacio ifo (Comercialitzadora)",
    "description": """Facturacio ifo Agri (Comercialitzadora)""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Facturacio",
    "depends":[
        "giscedata_facturacio_comer_ifo",
        "giscedata_sup_territorials_2013_tec271_comer"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/wizard_imprimir_factures_view.xml",
    ],
    "active": False,
    "installable": True
}