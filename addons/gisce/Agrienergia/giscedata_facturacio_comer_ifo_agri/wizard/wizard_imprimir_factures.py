# -*- coding: utf-8 -*-
"""
================================================================================
Custom Wizard per imprimir les factures ordenades i opció per imprimir factures
agrupades per zona i producte TEC
================================================================================
"""
from osv import fields, osv

try:
    from ast import literal_eval
except ImportError:
    literal_eval = eval

from itertools import tee, ifilterfalse


class WizardImprimirFactures(osv.osv_memory):
    """Wizard per imprimir les factures ordenades
    """
    _name = 'imprimir.factures'
    _inherit = 'imprimir.factures'

    def get_invoices(self, cursor, uid, ids, context=None):
        fact_obj = self.pool.get('giscedata.facturacio.factura')
        if context is None:
            context = {}

        res = super(WizardImprimirFactures, self).get_invoices(
            cursor, uid, ids, context=context
        )

        wiz = self.browse(cursor, uid, ids, context)[0]

        if wiz.agrupar_suplementos:
            # factures are a dict key: zona value: list of facturas ids
            factures = literal_eval(wiz.factures)

            filter_predicate = (
                lambda a: fact_obj.has_tec271_line(
                    cursor, uid, a, context=context
                )
            )

            for zona_key in factures.keys():
                aux_tec_zona_name = '{}_tec_271'.format(zona_key)
                # Copy list Iterators
                facts_without_tec271, facts_with_tec271 = tee(
                    factures[zona_key]
                )
                # Filters
                facts_without_tec271, facts_with_tec271 = (
                    list(ifilterfalse(filter_predicate, facts_without_tec271)),
                    list(filter(filter_predicate, facts_with_tec271))
                )

                if facts_with_tec271:
                    factures[zona_key] = facts_without_tec271
                    factures[aux_tec_zona_name] = facts_with_tec271

            wiz.write({
                'state': 'resum',
                'factures': str(factures),
                'info_suplementos': ''
            })

            return True

        return res

    _columns = {
        'agrupar_suplementos': fields.boolean('Dividir en suplementos'),
        'info_suplementos': fields.text(
            'Información de impresión', readonly=True
                                          )
    }

    _defaults = {
        'agrupar_suplementos': lambda *a: False,
        'info_suplementos': lambda *a: '',
    }


WizardImprimirFactures()
