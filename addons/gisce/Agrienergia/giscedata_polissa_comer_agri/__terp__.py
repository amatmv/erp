# -*- coding: utf-8 -*-
{
    "name": "Extensió polissa comercialitzadora AgriEnergia",
    "description": """Contractes d'Agrienergia Comercialitzadora""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "AgriEnergia",
    "depends": [
        "giscedata_polissa_comer",
        "giscedata_lectures_comer",
        "giscedata_facturacio_indexada",
        "giscedata_polissa_condicions_generals"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "giscedata_polissa_view.xml",
        "giscedata_polissa_comer_agri_report.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}