# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscedataPolissa(osv.osv):

    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def get_idioma(self, cursor, uid, ids, context=None):
        """
        Obtains the policy language.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.user> id
        :type uid: long
        :param ids: <giscedata.polissa> ids.
        :type ids: list[long]
        :param context: OpenERP context.
        :type context: dict
        :return: International code that indicates the language of the policy.
        :rtype: str
        """
        polissa_f = ['idioma']
        polissa_vs = self.read(cursor, uid, ids, polissa_f, context=context)
        res = dict.fromkeys(ids, None)
        for polissa_v in polissa_vs:
            idioma = polissa_v['idioma']
            if idioma:
                polissa_id = polissa_v['id']
                res[polissa_id] = idioma

        return res

    def _lang_get(self, cursor, uid, context=None):
        """
        Obtains all the available languages.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.user> id
        :type uid: long
        :param context: OpenERP context.
        :type context: dict
        :return: The ERP available languages as a selection structure.
        :rtype: list[tuple[str,str]]
        """
        lang_o = self.pool.get('res.lang')
        lang_ids = lang_o.search(cursor, uid, [], context=context)
        lang_f = ['code', 'name']
        res = lang_o.read(cursor, uid, lang_ids, lang_f, context=context)
        return [(r['code'], r['name']) for r in res] + [('', '')]

    _columns = {
        'idioma': fields.selection(_lang_get, 'Idioma', size=5),
    }


GiscedataPolissa()
