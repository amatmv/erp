# -*- coding: utf-8 -*-
from osv import osv


class GiscedataPolissaReport(osv.osv):

    _name = 'giscedata.polissa.report'
    _auto = False

    def get_supplier_info(self, cursor, uid, ids, context=None):
        """
        Obtains the supplier ("distribuidora") information related to the
        contract.
        :param cursor:
        :param uid:
        :param ids: <giscedata.polissa> ids
        :param context:
        :return: Dictionary where the key is the polissa id and the value is a
            dictionary with the following structure:
                - ref: "referencia de la distribuidora"
                - name: "nom de la distribuidora"
                - phone: "numero de telefon de la distribuidora" (from the
                    contact or default address).
                - address: "Adreça de la distribuidora" (from the contact or
                    default address).
        """
        if not isinstance(ids, list):
            ids = [ids]

        polissa_o = self.pool.get('giscedata.polissa')
        res_partner_o = self.pool.get('res.partner')
        res_partner_address_o = self.pool.get('res.partner.address')

        supplier_id = 'distribuidora.id'
        supplier_ref = 'ref_dist'
        supplier_name = 'distribuidora.name'

        polissa_f = [supplier_id, supplier_name, supplier_ref, 'id']
        dmn = [('id', 'in', ids)]

        q = polissa_o.q(cursor, uid).select(polissa_f).where(dmn)
        cursor.execute(*q)

        res = {}

        for polissa_v in cursor.dictfetchall():
            polissa_id = polissa_v['id']

            supplier_address_ids = res_partner_o.address_get(
                cursor, uid, [polissa_v[supplier_id]], ['default', 'contact']
            )

            supplier_address_id = supplier_address_ids.get('contact', False)
            use_default_address = not supplier_address_id
            if use_default_address:
                supplier_address_id = supplier_address_ids['default']

            distri = {}
            if supplier_address_id:
                distri = res_partner_address_o.read(
                    cursor, uid, supplier_address_id,
                    ['street', 'zip', 'state_id', 'country_id', 'city'])

            supplier_phone = res_partner_address_o.read(
                cursor, uid, supplier_address_id, ['phone'], context=context
            )['phone']

            res[polissa_id] = {
                'ref': polissa_v[supplier_ref],
                'name': polissa_v[supplier_name],
                'phone': supplier_phone,
                'address': supplier_address_id,
                'street': distri['street'] or '',
                'city': distri['city'] or '',
                'zip': distri['zip'] or '',
                'state': distri['state_id'][1] if distri['state_id'] else '',
                'country': distri['country_id'][1] if distri['country_id'] else '',
            }

        return res


GiscedataPolissaReport()
