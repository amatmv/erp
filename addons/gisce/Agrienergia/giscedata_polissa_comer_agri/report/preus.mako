<%
    from datetime import datetime, date
    from giscedata_polissa.report.utils import localize_period, datetime_to_date
    from giscedata_facturacio.report.utils import get_atr_price
    pool = objects[0].pool
    comptador_preus = 0
    model_obj = pool.get('ir.model.data')
    pricelist_obj = pool.get('product.pricelist')
    tarifa_obj = pool.get('giscedata.polissa.tarifa')
    fac_obj = pool.get('giscedata.facturacio.facturador')
    cfg = pool.get('res.config')
    uom_obj = pool.get('product.uom')
    product_obj = pool.get('product.product')
    model_ids = model_obj.search(cursor, uid,
                                 [('module','=','giscedata_facturacio'),
                                  ('name','=','pricelist_tarifas_electricidad')])
    tarifa_elect_atr = model_obj.read(cursor, uid, model_ids[0],['res_id'])['res_id']
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
        <style type="text/css">
            ${css}
            #outer{
                width: 12px;
                position: relative;
                float: left;
                font-size: 8px;
            }

            #vertical_text{
                -webkit-transform: rotate(-90deg);
                transform: rotate(-90deg);
                position: absolute;
                left: -40px;
                -webkit-transform-origin: left top;
                width: 900px;
                bottom: -1000px;
                padding: 12px;
                font-size: 8px;
                text-align: center;
            }
            .small{
                width: 85%;
            }
            .small td{
                text-align: center;
            }
            .nota{
                font-weight: bold;
                font-size: 8px;
            }
            #complements{
                border: 1px solid black;
                width: 450px;
                height: 70px;
                margin-left: 150px;
            }
            #equips_mesura{
                width: 60%;
                margin-left: 150px;
            }
            #equips_mesura td{
                padding-left: 5px;
            }
            body{
                font-family: helvetica;
                font-size: 10px;
                margin-right: 50px;
                margin-left: 50px;
            }
            table{
                border: 1px solid black;
                border-collapse: collapse;
                width: 100%;
            }
            table td{
                border: 1px solid black;
                text-align: left;
                font-size: 10px;
            }
            #header{
                margin-top: 20px;
            }
            #logo img{
                margin-top:10px;
                height: 80px;
            }
            .titol{
                width: 100%;
                text-align: center;
                font-weight: bold;
            }
            .seccio{
                font-weight: bold;
            }
            .label{
                font-weight: bold;
                margin-left: 5px;
            }
            .field{
                margin-left: 10px;
            }
            #legal{
                text-align: justify;
            }
            .signatura{
                position: relative;
                float: left;
                width: 50%;
                text-align: center;
                height: 110px;
            }
            .signatura img{
                margin-top:15px;
                height:80px;
            }
        </style>
    </head>
    <%def name="header_contracte(case)">
        <div id="header">
            <div id="logo">
                <img id="logo" height="50px" src="data:image/jpeg;base64,${company.logo}"/>
            </div>
            <br>
            <div class="titol">${_(u"ANNEX DE PREUS")}</div>
            <div style="clear:both"></div>
        </div>
    </%def>
    <%def name="clean(text)">
        ${text or ''}
    </%def>
    <body>
        %for polissa in objects:
            <%
                setLang(polissa.get_idioma()[polissa.id] or 'ca_ES')
                indexada = polissa.mode_facturacio == 'index'
                ctx = {'date': ''}
            %>
            <div id="outer">
                <p id="vertical_text">
                    ${company.rml_footer2}
                </p>
            </div>
            ${header_contracte(polissa)}
            <div class="seccio">
                ${_(u"DADES DEL CLIENT")}
                <br>
                <table style="margin-top: 5px;">
                    <tr>
                        <td>
                            <span class="label">${_(u"Nom/Raó Social:")}</span>
                            <span class="field">${polissa.pagador.name}</span>
                        </td>
                        <td style="width: 130px;">
                            <span class="label">${_(u"NIF/CIF:")}</span>
                            <span class="field">${polissa.pagador.vat and polissa.pagador.vat.replace('ES', '') or ''}</span>
                        </td>
                    </tr>
                </table>
            </div>
            <br>
            <div class="seccio">
                ${_(u"DADES DEL PUNT DE SUBMINISTRAMENT")}
                <br>
                <table style="margin-top: -1px;">
                    <tr>
                        <td>
                            <span class="label">${_(u"Empresa Distribuidora:")}</span>
                            <span class="field">${clean(polissa.distribuidora.name)}</span>
                        </td>
                        <td>
                            <span class="label">${_(u"CUPS:")}</span>
                            <span class="field">${clean(polissa.cups.name)}</span>
                        </td>
                    </tr>
                </table>
                %if indexada:
                    <table style="margin-top: 5px;">
                        <tr>
                            <td>
                                <span class="label">${_(u"Ref. Catastral:")}</span>
                                <span class="field">${clean(polissa.cups.ref_catastral)}</span>
                            </td>
                            <td>
                                <span class="label">${_(u"CNAE:")}</span>
                                <span class="field">${clean(polissa.cnae.name)}</span>
                            </td>
                            <td>
                                <span class="label">${_(u"Tensió Nominal(V):")}</span>
                                <span class="field">${clean(polissa.tensio)}</span>
                            </td>
                        </tr>
                    </table>
                %endif
            </div>
            <br>
            <div class="seccio">
                ${_(u"PRODUCTE CONTRACTAT AMB AGRI-ENERGIA, SA")}
                <%
                    llista_preus = polissa.llista_preu.name
                    if not polissa.tarifa.name.startswith('6'):
                        llista_preus = llista_preus.split(' ')[0]
                %>
                <br>
                <table style="margin-top: 5px;">
                    <tr>
                        <td>
                            <span class="label">${_(u"Producte")}</span>
                        </td>
                        <td>
                            <span class="field">${clean(llista_preus)}</span>
                        </td>
                        <td>
                            <span class="label">${_(u"Núm. contracte")}</span>
                        </td>
                        <td>
                            <span class="field">${clean(polissa.name)}</span>
                        </td>
                    </tr>
                </table>
            </div>
            <br>
            <div class="seccio">
                ${_(u"TERME DE POTÈNCIA")}
                <br>
                <table class="small" style="margin-top: 5px;" align="center">
                    <% potencies_periode = polissa.potencies_periode %>
                    <tr>
                        <td style="width: 30%;"></td>
                        %for pot in potencies_periode:
                            <td><b>${pot.periode_id.name.upper()}</b></td>
                        %endfor
                    </tr>
                    <tr>
                        <td><b>${_(u"Potència Contractada")}</b></td>
                        %for pot in potencies_periode:
                            <td>${formatLang(pot.potencia)}</td>
                        %endfor

                    </tr>
                    <tr>
                        <td><b>${_(u"Preu per període €/kW/dia")}</b></td>
                        %for pot in potencies_periode:
                            <td>
                                ${formatLang(get_atr_price(cursor, uid, polissa, pot.periode_id.name, 'tp', ctx)[0], 6)}
                            </td>
                        %endfor
                    </tr>
                </table>
                <br>
                <div class="nota">
                    ${_(u"S'aplica al preu i condicions de facturació del terme de potència de la tarifa d'accés {0} vigent.").format(
                        polissa.tarifa_codi
                    )}
                </div>
            </div>
            <br>
            <div class="seccio">
                ${_(u"PREU DE LA ENERGIA")}
                <br>
                <%
                    periodes_agrupats = tarifa_obj.get_grouped_periods(cursor, uid, polissa.tarifa.id)
                    energia_periodes = tarifa_obj.get_periodes_producte(cursor, uid, polissa.tarifa.id, 'te')
                    sorted_periodes = sorted(list(set(energia_periodes) - set(periodes_agrupats)))
                %>
                %if indexada:
                    <table class="small" align="center">
                        <tr>
                            <td>
                                ${_(u"Segons valors horaris OMIE amb despeses de gestió de 0,4 cent € / KWh.")}
                                ${_(u"(Detall del mètode al revers d'aquest annex.)")}
                            </td>
                        </tr>
                    </table>
                %else:
                    <table class="small" style="margin-top: 5px;" align="center">
                        <tr>
                            <td style="width: 30%;"></td>
                            %for periode in sorted_periodes:
                                <td><b>${periode}</b></td>
                            %endfor
                        </tr>
                        <tr>
                            <td><b>${_(u"Energia: Preu per període €/kWh")}</b></td>
                            %for periode in sorted_periodes:
                                <td>${formatLang(get_atr_price(cursor, uid, polissa, periode, 'te', ctx)[0], 6)}</td>
                            %endfor
                        </tr>
                    </table>
                %endif
                <br>
                <div class="nota">
                    ${_(u"El preu de la energia incorpora el preu de la "
                        "tarifa d'accés {0} vigent, a aplicar sobre la "
                        "energia consumida en el període horari "
                        "corresponent.").format(polissa.tarifa.name)}
                </div>
            </div>
            <br>
            <% tarifa_name = polissa.tarifa.name %>
            %if tarifa_name not in ['2.0A', '2.1A'] and not indexada:
                <div class="seccio">
                    ${_(u"CALENDARI D'APLICACIÓ DE PERÍODES")}
                    <br>
                    <table class="small" style="margin-top: 5px;" align="center">

                        %if 'DHA' in tarifa_name :
                            <tr>
                                <td colspan="2"><b>${_(u"Hivern")}</b></td>
                                <td colspan="2"><b>${_(u"Estiu")}</b></td>
                            </tr>
                            <tr>
                                <td style="width: 16%;"><b>${_(u"P1")}</b></td>
                                <td style="width: 16%;"><b>${_(u"P2")}</b></td>
                                <td style="width: 16%;"><b>${_(u"P1")}</b></td>
                                <td style="width: 16%;"><b>${_(u"P2")}</b></td>
                            </tr>
                            <tr>
                                <td><b>12-22</b></td>
                                <td><b>0-12 / 22-24</b></td>
                                <td><b>13-23</b></td>
                                <td><b>0-13 / 23-24</b></td>
                            </tr>
                        %elif 'DHS' in tarifa_name :
                            <tr>
                                <td colspan="3"><b>${_(u"Hivern i estiu")}</b></td>
                            </tr>
                            <tr>
                                <td style="width: 16%;"><b>${_(u"P1")}</b></td>
                                <td style="width: 16%;"><b>${_(u"P2")}</b></td>
                                <td style="width: 16%;"><b>${_(u"P3")}</b></td>
                            </tr>
                            <tr>
                                <td><b>13-23</b></td>
                                <td><b>23-1 / 7-13</b></td>
                                <td><b>1-7</b></td>
                            </tr>
                        %elif '3.1' in tarifa_name:
                            <tr>
                                <td colspan="3"><b>${_(u"Hivern")}</b></td>
                                <td colspan="3"><b>${_(u"Estiu")}</b></td>
                                <td colspan="2"><b>${_(u"Festius i caps de setmana (tot l'any)")}</b></td>
                            </tr>
                            <tr>
                                <td style="width: 12%;"><b>${_(u"P1")}</b></td>
                                <td style="width: 12%;"><b>${_(u"P2")}</b></td>
                                <td style="width: 12%;"><b>${_(u"P3")}</b></td>
                                <td style="width: 12%;"><b>${_(u"P1")}</b></td>
                                <td style="width: 12%;"><b>${_(u"P2")}</b></td>
                                <td style="width: 12%;"><b>${_(u"P3")}</b></td>
                                <td style="width: 12%;"><b>${_(u"P2")}</b></td>
                                <td style="width: 12%;"><b>${_(u"P3")}</b></td>
                            </tr>
                            <tr>
                                <td><b>17-23</b></td>
                                <td><b>8-17 / 23-24</b></td>
                                <td><b>0-8</b></td>
                                <td><b>10-16</b></td>
                                <td><b>8-10 / 16-24</b></td>
                                <td><b>0-8</b></td>
                                <td><b>18-24</b></td>
                                <td><b>0-18</b></td>
                            </tr>
                        %elif '6' not in tarifa_name:
                            <tr>
                                <td colspan="3"><b>${_(u"Hivern")}</b></td>
                                <td colspan="3"><b>${_(u"Estiu")}</b></td>
                            </tr>
                            <tr>
                                <td style="width: 16%;"><b>${_(u"P1")}</b></td>
                                <td style="width: 16%;"><b>${_(u"P2")}</b></td>
                                <td style="width: 16%;"><b>${_(u"P3")}</b></td>
                                <td style="width: 16%;"><b>${_(u"P1")}</b></td>
                                <td style="width: 16%;"><b>${_(u"P2")}</b></td>
                                <td style="width: 16%;"><b>${_(u"P3")}</b></td>
                            </tr>
                            <tr>
                                <td><b>18-22</b></td>
                                <td><b>8-18 / 22-24</b></td>
                                <td><b>0-8</b></td>
                                <td><b>11-15</b></td>
                                <td><b>8-11 / 15-24</b></td>
                                <td><b>0-8</b></td>
                            </tr>
                        %endif
                    </table>
                    <br>
                    <div class="nota">
                        ${_(u"Actualment establert en el segon punt 3 de l'Annexe II Ordre ITC 2794/2007. El canvi d'horari d'hivern a estiu i viceversa coincidirà amb la data del canvi oficial d'hora.")}
                    </div>
                    <br>
                </div>
            %endif
            %if '2.' not in tarifa_name:
                <div class="seccio">
                    ${_(u"ENERGIA REACTIVA")}
                    <br>
                    <table class="small" style="margin-top: 5px;" align="center">
                        <tr>
                            <td style="width: 20%"><b>${_(u"Energia reactiva")}</b></td>
                            <td style="width: 20%"><b>${_(u"Cos< 0,95<br>a cos = 0,90")}</b></td>
                            <td style="width: 20%"><b>${_(u"Cos < 0,90<br>a cos = 0,85")}</b></td>
                            <td style="width: 20%"><b>${_(u"Cos < 0,85<br>a cos = 0,80")}</b></td>
                            <td style="width: 20%"><b><br>${_(u"Cos < 0,80")}</b></td>
                        </tr>
                        <tr>
                            <td><b>${_(u"€/kVArh")}</b></td>
                            <td>0,041554</td>
                            <td>0,041554</td>
                            <td>0,041554</td>
                            <td>0,062332</td>
                        </tr>
                    </table>
                    <br>
                    <div class="nota">
                        ${_(u"S'apliquen els preus i condicions de facturació de"
                            " la energia reactiva establertes per la tarifa "
                            "d'accés {0} vigent (actualment R.D. 1164/2001). "
                            "S'aplicarà a tots els periodes tarifaris, "
                            "excepte el període vall, sempre que el consum "
                            "d'energia reactiva excedeixi el 33% del consum de "
                            "l'energia activa durant el període de "
                            "facturació considerat.".format(polissa.tarifa.name))}
                    </div>
                </div>
            %endif
            <br>
            <div class="seccio">
                ${_(u"COMPLEMENTS:")}
                <br>
                <div id="complements"></div>
            </div>
            <br>
            <div class="seccio">
                ${_(u"EQUIPS DE MESURA")}
                <br>
                <table id="equips_mesura" style="margin-top: 5px;" class="small" align="center">
                    <tr>
                        <td>${_(u"Facilitat per l'empresa Distribuïdora en règim de lloguer")}</td>
                    </tr>
                </table>
                <br>
                <div class="nota">
                    ${_(u"Preu subjecte a modificacions segons la disposició que sigui l'aplicació.")}
                </div>
            </div>
            <br>
            <b>${_(u"Facturació de les tarifes d'accés segons reglamentació vigent en cada moment.")}<br>
            ${_(u"Cada un del preus inclosos en aquest annex porten incorporats els corresponents impostos excepte I.E. i I.V.A.")}</b>
            <br>
            ${_(u"En cas de que no es compleixi alguna de les condicions requerides per acollirse al descompte comercial i s'hagi produït alguna devolució en el pagament del rebut durant la duració del contracte, el comercialitzador podrà deixar d'aplicar l'import corresponent al descompte comercial.")}<br>
            <br>
            <center>
                <%
                    if polissa.data_firma_contracte:
                        data_firma =  datetime.strptime(datetime_to_date(polissa.data_firma_contracte), '%Y-%m-%d')
                    else:
                        data_firma =  datetime.today()
                %>
                ${_(u"Banyoles, a {0}".format(localize_period(data_firma.today(), 'es_ES' ) ) )}
            </center>
            <br>
            <div style="clear:both"></div>
            <div class="signatura">
                <div style="position:absolute; top: 0px; min-width:100%;">${_(u"Firma del Client")}</div>
                <div style="position:absolute; bottom: 0px; min-width:100%;">Firmat.- ${polissa.pagador.name}</div>
            </div>
            <div class="signatura">
                <div style="position:absolute; top: 0px; min-width:100%;">${company.name}</div>
                   <img src="${addons_path}/giscedata_polissa_comer_agri/report/signatura.png"></img>
            </div>
        <%
        comptador_preus += 1;
        %>

        %if indexada:
            <p style="page-break-after: always"></p>
            <div style="height: 50px"></div>
            <h2>
               ${_(u"Detall càlcul Terme d'energia activa:")}
            </h2>
            <p>
                ${_(u"El terme d'energia es facturarà segons el número de períodes que correspongui a la tarifa d'accés.")}
            </p>
            <p>
                ${_(u"El preu de l'energia serà indexat horàriament al preu del mercat OMIE i contindrà els complements publicats per l'Operador del Sistema Eléctrico Español (Red Eléctrica de España),  que es poden obtenir de la web d'aquesta entitat ( https://www.esios.ree.es/es ), excepte el pagament per capacitat que és el fixat pel RDL 09/2015 de 10 de juliol, o pel que pugui substituir.")}
            </p>
            <p>
                ${_(u"El preu de l'energia contindrà doncs els següents conceptes:")}
                <ul>
                    <li>
                        ${_(u"Preu de l'energia en el mercat diari en l'hora (PMDi).")}
                    </li>
                    <li>
                        ${_(u"Pagaments per capacitat en l'hora (PCi).")}
                    </li>
                    <li>
                        ${_(u"Sobrecost del mercat en l'hora (Ci) (publicats per REE).")}
                    </li>
                    <li>
                        ${_(u"Fons d'eficiència Energètica (FE)")}
                    </li>
                    <li>
                        ${_(u"Pèrdues en l'hora (Pi).")}
                    </li>
                    <li>
                        ${_(u"Taxes municipals del 1,5% (TM).")}
                    </li>
                    <li>
                        ${_(u"Fee que es fixa en")} ${formatLang(polissa.coeficient_k)} ${("€/MWh (F).")}
                    </li>
                    <li>
                        ${_(u"Terme d'energia d'accés vigent en l'hora (PAi).")}
                    </li>
                </ul>
                ${_(u"Essent el preu final horari la combinació dels següents termes:")}
                <p class="center">
                    <table class="small" align="center">
                        <tr>
                            <td>
                               ${_(u"Preu energia = (( PMDi + PCi + Ci + FE) x (1 + Pi) + F) x ((1 + TM)/(1 - TM)) + PAi")}
                            </td>
                        </tr>
                    </table>
                </p>
            </p>
            ${_(u"Si durant la durada del contracte la legislació en aplicació modifiqués els costos regulats integrants del preu de l’energia, s’aplicarà íntegrament al contracte.")}

            ${_(u"Dins del preu final del terme d’energia, no s’inclou l'impost sobre l'electricitat.")}

            ${_(u"Les despeses de gestió del contracte seràn les que figurin en les condicions particulars.")}
        % endif
        % if comptador_preus<len(objects):
            <p style="page-break-after:always"></p>
        % endif
        %endfor
    </body>
</html>
