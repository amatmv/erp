## -*- coding: utf-8 -*-
<%
    from datetime import datetime
    from collections import namedtuple
    from giscedata_polissa.report.utils import localize_period, datetime_to_date

    pool = objects[0].pool
    report_o = pool.get('giscedata.polissa.report')

    def clean_text(text):
            return text or ''
    res = {'currency': company.currency_id.id}
    address = company.partner_id.address[0]
    for field in 'street street2 zip city email phone'.split():
        if address[field]:
            res[field] = address[field]
%>
<%def name="clean(text)">
    ${text or ''}
</%def>

<%def name="enviament(diferent, text)">
    %if not diferent:
        ${clean(text)}
    %endif
</%def>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
        <link rel="stylesheet" href="${addons_path}/giscedata_polissa_comer_agri/report/estils_agri.css"/>
        <link rel="stylesheet" href="${addons_path}/giscedata_polissa_comer_agri/report/estils_contracte.css"/>
        <style>
            @font-face {
                font-family: "Roboto-Regular";
                src: url("${assets_path}/fonts/Roboto/Roboto-Regular.ttf") format('truetype');
                font-weight: normal;
            }
        </style>
    </head>

    <body>
        %for polissa in objects:
            <%
                setLang(polissa.get_idioma()[polissa.id] or 'ca_ES')
                ## -----------------------------------------------------------------------------
                # Supplier ("distribuidora") information
                supplier = report_o.get_supplier_info(cursor, uid, polissa.id)[polissa.id]
            %>
            <div id="outer">
                <span id="vertical_text">
                    ${company.rml_footer2 or ''}
                </span>
            </div>
            <div id="dades_contracte">
                <div id="header">
                    <div>
##                         <div id="logo">
##                             <img id="logo" src="data:image/jpeg;base64,${company.logo}"/>
##                         </div>
                        <table id="cap">
                            <tr>
                                <td rowspan="2">
                                    <img class="logo" src="data:image/jpeg;base64,${company.logo}" height="50px"/>
                                </td>
                                <td>
                                    <img class="icona" src="${addons_path}/giscedata_polissa_comer_agri/report/icones/phone.png"/>
                                </td>
                                <td>
                                    972 58 00 58
                                </td>
                                <td>
                                    <img class="icona" src="${addons_path}/giscedata_polissa_comer_agri/report/icones/world.png"/>
                                </td>
                                <td>
                                    <a href="www.agrienergia.com">www.agrienergia.com</a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img class="icona" src="${addons_path}/giscedata_polissa_comer_agri/report/icones/mail.png"/>
                                </td>
                                <td>
                                    <a href="mailto:oficina@agrienergia.com">oficina@agrienergia.com</a>
                                </td>
                                <td>
                                    <img class="icona" src="${addons_path}/giscedata_polissa_comer_agri/report/icones/marker.png"/>
                                </td>
                                <td>
                                    Av. Països Catalans, 140 · 17820 Banyoles
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="titol">
                        <h1>
                            ${_(u"CONDICIONS PARTICULARS DEL CONTRACTE DE SUBMINISTRAMENT D'ENERGIA")}
                            <br/>
                            ${_(u"ELÈCTRICA EN BAIXA TENSIÓ")}
                        </h1>
                    </div>
                </div>
                <div class="seccio">
                    <h2>${_(u"DADES DEL CLIENT")}</h2>
                    <h2 style="text-align: right">
                        ${_(u"Núm. Contracte: ")} ${clean(polissa.name)}
                    </h2>
                        <% direccio_titular = polissa.direccio_pagament %>
                        <table>
                            <colgroup>
                              <col style="width: 15%"/>
                              <col style="width: 60%"/>
                              <col style="width: 10%"/>
                              <col style="width: 15%"/>
                            </colgroup>
                            <tr>
                                <td>
                                    <span class="label">${_(u"Nom/Motiu Social")}</span>
                                </td>
                                <td>
                                    <span class="field">${polissa.pagador.name}</span>
                                </td>
                                <td>
                                    <span class="label">${_(u"NIF/CIF")}</span>
                                </td>
                                <td>
                                    <span class="field">${polissa.pagador and (polissa.pagador.vat or '').replace('ES', '')}</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="label">${_(u"Activitat Principal")}</span>
                                </td>
                                <td>
                                    <span class="field">${clean(polissa.cnae.descripcio)}</span>
                                </td>
                                <td>
                                    <span class="label">${_(u"CNAE")}</span>
                                </td>
                                <td>
                                    <span class="field">${clean(polissa.cnae.name)}</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="label">${_(u"Adreça del client")}</span>
                                </td>
                                <td>
                                    <span class="field">${clean(direccio_titular.street)}</span>
                                </td>
                                <td>
                                    <span class="label">${_(u"Teléfon")}</span>
                                </td>
                                <td>
                                    <span class="field">${clean(direccio_titular.phone)}</span>
                                </td>
                            </tr>
                        <tr>
                            <td>
                                <span class="label">${_(u"Codi Postal - Població")}</span>
                            </td>
                            <td>
                                <span class="field">${clean(direccio_titular.zip)} - ${clean(direccio_titular.city)}</span>
                            </td>
                            <td>
                                <span class="label">${_(u"Mòbil")}</span>
                            </td>
                            <td>
                                <span class="field">${clean(direccio_titular.mobile)}</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="label">${_(u"Correu electrònic")}</span>
                            </td>
                            <td>
                                <span class="field">${clean(direccio_titular.email)}</span>
                            </td>
                            <td>
                                <span class="label">${_(u"Fax")}</span>
                            </td>
                            <td>
                                <span class="field">${clean(direccio_titular.fax)}</span>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="seccio">
                    <h2>
                        ${_(u"DADES DE L'ADREÇA D'ENVIAMENT")}
                    </h2>
                    <%
                        direccio_envio = polissa.direccio_notificacio
                        diferent = False
                    %>
                    <table>
                        <colgroup>
                            <col style="width: 15%"/>
                            <col style="width: 50%"/>
                            <col style="width: 15%"/>
                            <col style="width: 20%"/>
                        </colgroup>
                        <tr>
                            <td>
                                <span class="label">${_(u"Adreça fiscal")}</span>
                            </td>
                            <td colspan="3">
                                <span class="field">${enviament(diferent, direccio_envio.street)}</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="label">${_(u"Codi Postal/Població")}</span>
                            </td>
                            <td>
                                <span class="field">${enviament(diferent,
                                    '{0} {1}'.format(
                                        clean_text(direccio_envio.zip), clean_text(direccio_envio.city)
                                    )
                                )}</span>
                            </td>
                            <td>
                                <span class="label">${_(u"Provincia/País")}</span>
                            </td>
                            <td>
                                <span class="field">${enviament(diferent,
                                    '{0} {1}'.format(
                                        clean_text(direccio_envio.state_id.name), clean_text(direccio_envio.country_id.name)
                                    )
                                )}</span>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="seccio">
                    <h2>
                        ${_(u"DADES DEL PUNT DE SUBMINISTRAMENT")}
                    </h2>
                    <%
                        direccio_ps = polissa.cups
                        direccio_cups = polissa.cups_direccio
                        idx_pob = direccio_cups.rfind('(')
                        if idx_pob != -1:
                            direccio_cups = direccio_cups[:idx_pob]
                    %>
                    <table>
                        <colgroup>
                            <col style="width: 15%"/>
                            <col style="width: 35%"/>
                            <col style="width: 15%"/>
                            <col style="width: 35%"/>
                        </colgroup>
                        <tr>
                            <td>
                                <span class="label">${_(u"CUPS")}</span>
                            </td>
                            <td>
                                <span class="field">${polissa.cups.name}</span>
                            </td>
                            <td>
                                <span class="label">${_(u"Adreça")}</span>
                            </td>
                            <td>
                                <span class="field">${direccio_cups}</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="label">${_(u"Referència Catastral")}</span>
                            </td>
                            <td>
                                <span class="field">${clean(polissa.cups.ref_catastral)}</span>
                            </td>
                            <td>
                                <span class="label">${_(u"Codi Postal - Població")}</span>
                            </td>
                            <td>
                                <span class="field">${clean(direccio_ps.dp)} ${clean(direccio_ps.id_poblacio.name)}</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="label">${_(u"Tensió Nominal(V)")}</span>
                            </td>
                            <td>
                                <span class="field">${clean(polissa.tensio)}</span>
                            </td>
                            <td>
                                <span class="label">${_(u"Equip de mesura")}</span>
                            </td>
                            <%
                                have_lloguer = False
                                have_meters = False
                                if polissa.comptadors:
                                    have_meters = True
                                    for meter in polissa.comptadors:
                                        if meter.active and meter.lloguer:
                                           have_lloguer = True
                                lloguer = ''
                                comptador = ''
                                if have_lloguer:
                                    lloguer = _('En lloguer')
                                if have_meter:
                                    comptador = polissa.comptadors[0].name
                            %>
                            <td>
                                <span class="field">${lloguer}</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="label">${_(u"Num. comptador")}</span>
                            </td>
                            <td>
                                <span class="field">${comptador}</span>
                            </td>
                            <td>
                                <span class="label">${_(u"Lectura")}</span>
                            </td>
                            <td>
                                <span class="field"></span>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="seccio">
                    <h2>
                        ${_(u"DADES EMPRESA DISTRIBUÏDORA")}
                    </h2>
                    <table>
                        <colgroup>
                            <col style="width: 15%"/>
                            <col style="width: 35%"/>
                            <col style="width: 15%"/>
                            <col style="width: 35%"/>
                        </colgroup>
                        <tr>
                            <td>
                                <span class="label">${_(u"Distribuïdora")}</span>
                            </td>
                            <td>
                                <span class="field">${clean(polissa.distribuidora.name)}</span>
                            </td>
                            <td>
                                <span class="label">${_(u"Telèfon")}</span>
                            </td>
                            <td>
                                <span class="field">${clean(supplier['phone'])}</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="label">${_(u"Adreça")}</span>
                            </td>
                            <td>
                                <span class="field">${clean(supplier['street'])}</span>
                            </td>
                            <td>
                                <span class="label">${_(u"Codi postal - Població")}</span>
                            </td>
                            <td>
                                <span class="field">${clean(supplier['zip'])} - ${clean(supplier['city'])}</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="label">${_(u"Provincia - País")}</span>
                            </td>
                            <td colspan="3">
                                <span class="field">${clean(supplier['state'])} - ${clean(supplier['country'])}</span>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="seccio">
                    <h2>
                        ${_(u"DADES CONTRACTE ACCÉS")}
                    </h2>
                    <table class="taula_potencies">
                        <colgroup>
                            <col width="auto"/>
                            <col width="7%"/>
                            <col width="auto"/>
                        </colgroup>
                        <tr class="taula_potencies">
                            <td class="taula_potencies">
                                <table>
                                    <tr>
                                        <td>
                                            <span class="label">${_(u"Núm. Contracte accés")}</span>
                                        </td>
                                        <td>
                                            <span class="field">${clean(supplier['ref'])}</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="label">${_(u"Tarifa")}</span>
                                        </td>
                                        <td>
                                            <span class="field">${clean(polissa.tarifa_codi)}</span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="taula_potencies">&nbsp;</td>
                            <td class="taula_potencies">
                                <table>
                                    <tr>
                                        <td rowspan="2" class="">
                                            <span class="label">${_(u"Potència Contractada(kW)")}</span>
                                        </td>
                                        <%
                                            potencies = polissa.potencies_periode
                                            periodes = []
                                            for i in range(0, 6):
                                                if i < len(potencies):
                                                    periode = potencies[i]
                                                else:
                                                    periode = False
                                                periodes.append((i+1, periode))
                                        %>
                                        %for p in periodes:
                                            <td>
                                                <span class="label">${"P{0}".format(p[0])}</span>
                                            </td>
                                        %endfor
                                    </tr>
                                    <tr>
                                        %for p in periodes:
                                            <td>
                                                <span class="field">${p[1] and  formatLang(p[1].potencia) or ' '}</span>
                                            </td>
                                        %endfor
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="seccio">
                    <h2>
                        ${_(u"PRODUCTE CONTRACTAT AMB ")}
                        ${company.name}:
                    </h2>
                    <!-- GET COMPANY NAME AQUI -->
                    <table>
                        <colgroup>
                            <col style="width: 15%"/>
                            <col style="width: 35%"/>
                            <col style="width: 15%"/>
                            <col style="width: 35%"/>
                        </colgroup>
                        <tr>
                            <td>
                                <span class="label">${_(u"Producte")}</span>
                            </td>
                            <td colspan="">
                                <span class="field">${clean_text(polissa.llista_preu.name).split(' ')[0]}</span>
                            </td>
                            <td>
                                <span class="label">${_(u"Base formació preu")}</span>
                            </td>
                            <td>
                                <%
                                    if polissa.mode_facturacio == 'index':
                                        mode_facturacio = _(u"Preus indexats al mercat OMIE")
                                    else:
                                        mode_facturacio = _(u"Preus indicadors dels mercats de futurs elèctrics")
                                %>
                                <span class="field">${mode_facturacio}</span>
                            </td>
                        </tr>
                        <tr>
                            <%
                                data_inici = ''
                                data_final = ''
                                if 'form' in data.keys():
                                    form = data['form']
                                    data_inici = form['polissa_date']
                                    modcon_obj = pool.get('giscedata.polissa.modcontractual')
                                    modcon_id = modcon_obj.search(cursor, uid, [
                                        ('polissa_id', '=', polissa.id),
                                        ('data_inici', '=', data_inici)
                                    ], context={'active_test': False})
                                    data_final = modcon_obj.read(cursor, uid, modcon_id, ['data_final'])[0]['data_final']
                                else:
                                    data_inici = polissa.modcontractual_activa.data_inici
                                    data_final = polissa.modcontractual_activa.data_final
                                if not data_final:
                                    data_final = 'Indefinida'
                                else:
                                    data_final = formatLang(data_final, date=True)
                                renovacio = ''
                                if polissa.renovacio_auto:
                                    renovacio = 'amb renovació automàtica'
                            %>
                            <td>
                                <span class="label">${_(u"Durada contracte")}</span>
                            </td>
                            <td>
                                <span class="field">${data_final} ${renovacio}</span>
                            </td>
                            <td>
                                <span class="label">${_(u"Facturació")}</span>
                            </td>
                            <%
                                get_facturacio = {
                                    1: _('Mensual'),
                                    2: _('Bimestral'),
                                    12: _('Anual'),
                                    0: _('Tot')
                                }
                                facturacio = ''
                                if polissa.facturacio:
                                    facturacio = get_facturacio[polissa.facturacio]
                            %>
                            <td>
                                <span class="field">${clean(facturacio)}</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="label">${_(u"Observacions")}</span>
                            </td>
                            <td colspan="3">
                                <span class="field">${clean(polissa.observacions)}</span>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="seccio">
                    <h2>
                        ${_(u"DADES DE PAGAMENT")}
                    </h2>
                    <table>
                        <colgroup>
                            <col style="width: 15%"/>
                            <col style="width: 35%"/>
                            <col style="width: 15%"/>
                            <col style="width: 35%"/>
                        </colgroup>
                        <tr>
                            <td>
                                <%
                                owner_b = ''
                                if polissa.bank.owner_name:
                                    owner_b = polissa.bank.owner_name
                                %>
                                <span class="label">${_(u"Titular del Compte")}</span>
                            </td>
                            <td>
                                <span class="field">${owner_b}</span>
                            </td>
                            <td>
                                <span class="label">${_(u"NIF")}</span>
                                <%
                                    nif = ''
                                    bank_obj = pool.get('res.partner.bank')
                                    field = ['owner_id']
                                    exist_field = bank_obj.fields_get(
                                        cursor, uid, field)
                                    if exist_field:
                                        owner = polissa.bank.owner_id
                                        if owner:
                                            nif = owner.vat or ''
                                        nif = nif.replace('ES', '')
                                %>
                            </td>
                            <td>
                                <span class="field">${nif}</span>
                            </td>
                        </tr>
                        <% iban = polissa.bank and polissa.bank.iban or '' %>
                        <tr>
                            <td>
                                <span class="label">${_(u"IBAN")}</span>
                            </td>
                            <td colspan="3">
                                <span class="field">${iban}</span>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="seccio">
                    <h2>
                        ${_(u"FORMA D'ENVIAMENT")}
                    </h2>

                    <%
                    tipusEnviament = polissa.enviament

                    if "postal" in tipusEnviament:
                        notificationLabel = "Factura en paper"
                        notificationAddress = "<span style='padding: 0px 25px;font-size: 14px;'>&#10004;</span>"
                    elif "email" in tipusEnviament:
                        notificationLabel = "Adreça electrònica"
                        notificationAddress = polissa.notificacio_email
                    %>

                    <table>
                        <colgroup>
                        %if tipusEnviament == "postal+email":
                            <col style="width: 15%"/>
                            <col style="width: 5%"/>
                            <col style="width: 15%"/>
                            <col style="width: 65%"/>
                        %else:
                            <col style="width: 15%"/>
                            <col style="width: 85%"/>
                        %endif
                        </colgroup>
                        <tr>
                            %if tipusEnviament == "postal+email":
                                <td>
                                    <span class="label">${_(u"Factura en paper")}</span>
                                </td>
                                <td>
                                    <span class="field">${notificationAddress}</span>
                                </td>
                                <td>
                                    <span class="label">${_(u"Adreça electrònica")}</span>
                                </td>
                                <td>
                                    <span class="field">${polissa.notificacio_email or ''}</span>
                                </td>
                            %else:
                                <td>
                                    <span class="label">${notificationLabel}</span>
                                </td>
                                <td>
                                    <span class="field">${notificationAddress}</span>
                                </td>
                            %endif
                        </tr>
                    </table>
                </div>
                <div id="condicions">
                    ${_(u"Les Condicions Generals del contracte de subministrament que es troben al dors del present document així com a l'annex de preus que s'adjunten, han estat llegits pel Client, el qui estant conforme amb el contingut de les mateixes les subscriu.")}
                    <br/>
                    ${_(u"A tal efecte, autoritza expressament a Agri-Energia, SA a tramitar davant l'Empresa Distribuïdora la sol·licitud de canvi del subministrador o modificacions contractuals.")}
                    <br/>
                    ${_(u"El sol·licitant és responsable de la veracitat de les dades reflectides en aquest document i comunicades en tot moment a AGRI-ENERGIA, SA, i es compromet a sol·licitar la seva modificació en cas d'ésser necessari.")}
                    <br/>
                    <br/>
                    ${_(u"Agrienergia, S.A., per tal d'oferir-li un servei adequat a les seves necessitats pot enviar-li informació de tipus comercial fruit de l'anàlisi del seu perfil.")}
                    <br/>
                    __<u>X</u>__ ${_(u"Accepto l'enviament d'informació comercial")}
                </div>
                <div id="footer">
                    <div class="city_date">
                        <%
                            if polissa.data_firma_contracte:
                                data_firma =  datetime.strptime(datetime_to_date(polissa.data_firma_contracte), '%Y-%m-%d')
                            else:
                                data_firma =  datetime.today()
                        %>
                        ${company.partner_id.address[0]['city']}
                        ${_(u", a {0}".format(localize_period(data_firma,polissa.pagador.lang or 'es_ES' )))}
                    </div>
                    <div class="signatura" style="float: left; padding: 0 110px;">
                        <div>${_(u"Signatura Client")}</div>
                    </div>
                    <div class="signatura" style="float: right; padding: 0 110px">
                        <div>${company.name}</div>
                        <img height="50px" src="${addons_path}/giscedata_polissa_comer_agri/report/signatura.png" >
                    </div>
                    <div class="observacions" style="clear:both">
                        ${polissa.print_observations or ""}
                    </div>
                </div>
            </div>
            % if polissa != objects[-1]:
                <p style="page-break-after:always"></p>
            % endif
        %endfor
    </body>
</html>
