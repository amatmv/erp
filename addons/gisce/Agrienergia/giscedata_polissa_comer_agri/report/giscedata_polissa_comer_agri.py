# -*- coding: utf-8 -*-
from __future__ import absolute_import
from c2c_webkit_report import webkit_report
from report import report_sxw


webkit_report.WebKitParser(
    'report.giscedata.polissa.report.llista.preus',
    'giscedata.polissa',
    'giscedata_polissa_comer_agri/report/preus.mako',
    parser=report_sxw.rml_parse
)
