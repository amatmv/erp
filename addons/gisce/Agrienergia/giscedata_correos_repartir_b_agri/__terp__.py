# -*- coding: utf-8 -*-
{
    "name": "Giscedata Repartim Bé Extension",
    "description": """
    Modulo para aprovechar los modulos de correos para sistema Repartimbe:
    * Sistema de generacion e interpretacion de ficheros
    """,
    "version": "0-dev",
    "author": "Gisce",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_correos_repartir_b",
        "giscedata_facturacio_comer_agri"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[

    ],
    "active": False,
    "installable": True
}