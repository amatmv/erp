# -*- encoding: utf-8 -*-

from tools.translate import _
from osv import osv, fields
import pandas as pd
from StringIO import StringIO
from base64 import b64encode, b64decode
from operator import itemgetter
from dateutil import parser


# Codi Carta
# CUPS / abonat
# Nom complet
# Adreça
# Codi Postal
# Població
# Data Entrada
# Data Estat
# Estat
# Codi Correus


class WizardInterpretateRepartimBeResponseFile(osv.osv_memory):

    _name = 'wizard.interpretate.repartimbe.response.file'
    _inherit = 'wizard.interpretate.repartimbe.response.file'

    def get_parsed_full_path_and_sorted_ftp_paths(self, parsed_file_info_list):
        return sorted(parsed_file_info_list, key=lambda x: (parser.parse(x[7]), x[8]))

WizardInterpretateRepartimBeResponseFile()