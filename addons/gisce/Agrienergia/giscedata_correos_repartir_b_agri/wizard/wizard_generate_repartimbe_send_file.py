# -*- encoding: utf-8 -*-
from tools.translate import _
from osv import osv, fields
from giscedata_facturacio_comer_agri.report.giscedata_facturacio_comer_agri \
    import AgriFacturaReportWebkitParserHTML
from report import report_sxw
from base64 import b64encode
from PyPDF2 import PdfFileWriter, PdfFileReader
from StringIO import StringIO
import netsvc
from c2c_webkit_report import webkit_report


class WizardRepartimBeSendFile(osv.osv_memory):
    """Wizard per gestionar comandes de correus per mailing andalucia"""

    _name = 'wizard.repartimbe.send.file'
    _inherit = 'wizard.repartimbe.send.file'

    @staticmethod
    def merger(binary_files):

        pdf_writer = PdfFileWriter()

        for pdf_file in binary_files:
            pdf_to_add = StringIO(pdf_file)

            pdf_reader = PdfFileReader(pdf_to_add)

            for page in range(pdf_reader.getNumPages()):
                pdf_writer.addPage(pdf_reader.getPage(page))

        out_file_pdf = StringIO()

        pdf_writer.write(out_file_pdf)

        return out_file_pdf.getvalue()

    def get_fact_lang(self, cursor, uid, fact_id, context=None):
        select_lang_from_notificado = (
            "SELECT par.lang AS lang FROM giscedata_facturacio_factura AS f "
            "INNER JOIN giscedata_polissa p ON (f.polissa_id = p.id) "
            "INNER JOIN res_partner_address addr ON (p.direccio_notificacio = addr.id) "
            "INNER JOIN res_partner par ON (addr.partner_id = par.id) "
            "WHERE f.id = %s"
        )
        cursor.execute(select_lang_from_notificado, (fact_id, ))
        res = cursor.dictfetchone()

        if res:
            return res['lang']
        else:
            return 'es_ES'

    def generate_pdf_report(self, cursor, uid, line_ids, context=None):
        if context is None:
            context = {}

        error_ids = []
        pdf = False
        documents = []

        res, errores = super(WizardRepartimBeSendFile, self).generate_pdf_report(
            cursor, uid, line_ids, context=context
        )

        if not res:

            # Need to do like this to preserve order
            for line in line_ids:
                select_fact_from_line = (
                    "SELECT f.id AS fact_id, "
                    "ol.origin_invoice_state AS o_state "
                    "FROM giscedata_correos_order_line ol "
                    "RIGHT JOIN giscedata_facturacio_factura f "
                    "ON (ol.invoice_id = f.invoice_id) "
                    "WHERE ol.id = %s"

                )

                cursor.execute(select_fact_from_line, (line, ))

                info = cursor.dictfetchone()

                fact_id = info['fact_id']
                o_state = info['o_state']

                if o_state:
                    try:
                        document_binary = False
                        carta = self.get_tipo_carta(
                            cursor, uid, o_state, context=context
                        )

                        lang = self.get_fact_lang(cursor, uid, fact_id, context=context)

                        ctx = context.copy()
                        ctx.update({'carta': int(carta)})
                        ctx.update({'lang': lang})

                        data = {
                            'model': 'giscedata.facturacio.factura',
                            'id': fact_id,
                            'report_type': 'webkit'
                        }

                        report_printer = webkit_report.WebKitParser(
                            'report.giscedata.facturacio.factura.carta.requeriment',
                            'giscedata.facturacio.factura',
                            'giscedata_facturacio_comer_agri/report/carta_requeriment.mako',
                            parser=report_sxw.rml_parse
                        )

                        document_binary = report_printer.create(
                            cursor, uid, [fact_id], data,
                            context=ctx
                        )

                        if document_binary:
                            documents.append(document_binary[0])
                        else:
                            error_ids.append(line)
                    except Exception as e:
                        print e
                        error_ids.append(line)
                else:
                    error_ids.append(line)

        if documents:
            pdf = self.merger(documents)

        return pdf, error_ids

WizardRepartimBeSendFile()
