# -*- coding: utf-8 -*-
{
    "name": "Extensió Switching AgriEnergia",
    "description": """Modul custom de Switching per a AgriEnergia""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "AgriEnergia",
    "depends": [
        "giscedata_switching",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}