# -*- coding: utf-8 -*-
from osv import osv


class GiscedataSwitching(osv.osv):

    _name = 'giscedata.switching'
    _inherit = 'giscedata.switching'

    def get_idioma(self, cursor, uid, ids, context=None):
        """
        Obtains the switching language based on the policy language.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id
        :type uid: long
        :param ids: <giscedata.switching> ids
        :type ids: list
        :param context: OpenERP context.
        :type context: dict
        :return: switching language (xx_XX format).
        :rtype: str
        """
        polissa_o = self.pool.get('giscedata.polissa')
        switching_f = ['cups_polissa_id']
        switching_vs = self.read(cursor, uid, ids, switching_f, context=context)
        res = {}

        for switching_v in switching_vs:
            sw_id = switching_v['id']
            polissa_id = switching_v['cups_polissa_id']

            language = None

            if polissa_id:
                polissa_id = polissa_id[0]
                language = polissa_o.get_idioma(
                    cursor, uid, [polissa_id], context=context
                )[polissa_id]

            if language is None:
                language = super(GiscedataSwitching, self).get_idioma(
                    cursor, uid, [sw_id], context=context
                )[sw_id]

            res[sw_id] = language

        return res


GiscedataSwitching()
