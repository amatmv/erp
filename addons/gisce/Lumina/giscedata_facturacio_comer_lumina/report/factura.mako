## -*- coding: utf-8 -*-

<%
    from collections import Counter
    from datetime import datetime, timedelta
    from operator import attrgetter, itemgetter
    from bankbarcode.cuaderno57 import Recibo507
    from giscedata_facturacio_comer.report.utils import get_line_discount, get_giscedata_fact, get_other_info,get_distri_phone,get_discounts,get_historics
    from account_invoice_base.report.utils import localize_period
    import json, re

    pool = objects[0].pool
    polissa_obj = pool.get('giscedata.polissa')
    tarifa_obj = pool.get('giscedata.polissa.tarifa')
    banner_obj = pool.get('report.banner')
    total_periodes = 0
    comptador_factures = 0
    sup_territorials_tec_271_comer_obj = pool.get(
    'giscedata.suplements.territorials.2013.tec271.comer'
)
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en"><html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_lumina/report/bootstrap.css"/>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_lumina/report/pie.css"/>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_lumina/report/consum.css"/>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer_lumina/report/factura.css"/>
        <style type="text/css">
            ${css}
        </style>
    </head>
    <body>
        <%def name="consumption_table(comptadors)">
            <%
                ajust_fet = False
                motiu_ajust = ''
            %>
            <!--
                "comptadors" es un dicconario con la siguiente estructura:
                    - Clave: tupla con 3 valores: (Número de serie, fecha actual, fecha anterior)
                    - Valor: lista con 3 elementos:
                        * Primer elemento: lecturas activa
                        * Segundo elemento: lecturas reactiva
                        * Tercer elemento: potencia maxímetro
            -->
            %for key in comptadors.keys():
                <%
                    tipus_lectura = ''
                    if comptadors[key][0][0].origen_id.codi == '40':
                        tipus_lectura = _(u"(estimada)")
                    elif comptadors[key][0][0].origen_id.codi == '99':
                        tipus_lectura = _(u"(sense lectura)")
                    else:
                        tipus_lectura = _(u"(real)")
                %>

                <table class="small_text span12">

                    <tr class="bottomBorder">
                        <td>&nbsp;</td>
                        <td class="rightBorder">&nbsp;</td>
                        <td class="align_c">${_(u"Lectura anterior")}<br/> ${tipus_lectura}
                        <br/>${key[2]}</td>
                        <td class="align_c rightBorder">${_(u"Lectura actual")}<br/> ${tipus_lectura}
                        <br/>${key[1]}</td>
                        <td class="align_c rightBorder">${_(u"Consum")} <br/> ${_(u"en el període")}</td>
                        <td class="align_c">${_(u"Reactiva anterior")}<br/> ${tipus_lectura}
                            <br/> ${key[2]}
                        </td>
                        <td class="align_c rightBorder">${_(u"Reactiva actual")}<br/> ${tipus_lectura}
                            <br/> ${key[1]}
                        </td>
                        <td class="align_c">${_(u"Consum")} <br/> ${_(u"en el període")}</td>
                    </tr>
                    <%
                        period_counter = 1
                    %>
                    %for lectures in comptadors[key]:
                        <!-- Recorremos las lecturas del contador actual -->
                        <%
                            comptador = 0
                        %>
                        <tr>
                        %if period_counter == 1 and total_periodes == 6:
                            <td rowspan="3" class="align_c">${_(u"LABORABLE")}</td>
                            <td class="strong align_c rightBorder">P${period_counter}</td>
                        %elif period_counter == 4 and total_periodes == 6:
                            <td rowspan="3" class="align_c top_border">${_(u"FESTIU")}</td>
                            <td class="strong align_c rightBorder top_border">P${period_counter}</td>
                        %elif total_periodes == 6:
                            <td class="strong align_c rightBorder">P${period_counter}</td>
                        %else:
                            <td class="strong">&nbsp;</td>
                            <td class="strong align_c rightBorder">P${period_counter}</td>
                        %endif
                            %for lectura in lectures:
                                <!--
                                    Recorremos cada lectura individual. El contador (comptador) es utilizado
                                    para conocer cual de los 3 elementos estamos recorriendo (activa, reactiva o maxímetro).
                                -->
                                %if comptador in (0, 1) and lectura:
                                    %if comptador == 1:
                                        %if period_counter == 4:
                                            <td class="consumption_field align_c top_border">${formatLang(int(lectura.lect_anterior), 0)} kWh</td>
                                            <td class="consumption_field rightBorder align_c top_border">${formatLang(int(lectura.lect_actual), 0)} kWh</td>
                                        %else:
                                            <td class="consumption_field align_c">${formatLang(int(lectura.lect_anterior), 0)} kWh</td>
                                            <td class="consumption_field rightBorder align_c">${formatLang(int(lectura.lect_actual), 0)} kWh</td>
                                        %endif
                                    %else:
                                        %if period_counter == 4:
                                            <td class="consumption_field align_c top_border">${formatLang(int(lectura.lect_anterior), 0)} kWh</td>
                                            <td class="consumption_field rightBorder align_c top_border">${formatLang(int(lectura.lect_actual), 0)} kWh
                                        %else:
                                            <td class="consumption_field align_c">${formatLang(int(lectura.lect_anterior), 0)} kWh</td>
                                            <td class="consumption_field rightBorder align_c">${formatLang(int(lectura.lect_actual), 0)} kWh
                                        %endif
                                    %endif
                                    %if lectura.ajust and not ajust_fet:
                                        ${str(lectura.ajust) + "*"}
                                    %endif
                                    <%
                                    if lectura.motiu_ajust and not ajust_fet == '':
                                        ajust_fet = True
                                        motiu_ajust = lectura.motiu_ajust
                                    %>
                                    </td>

                                    %if comptador == 0:
                                        %if period_counter == 4:
                                            <td class="consumption_field rightBorder align_c top_border">${formatLang(int(lectura.consum), 0)} kWh</td>
                                        %else:
                                            <td class="consumption_field rightBorder align_c">${formatLang(int(lectura.consum), 0)} kWh</td>
                                        %endif
                                    %else:
                                        %if period_counter == 4:
                                            <td class="consumption_field align_c top_border">${formatLang(int(lectura.consum), 0)} kWh</td>
                                        %else:
                                            <td class="consumption_field align_c">${formatLang(int(lectura.consum), 0)} kWh</td>
                                        %endif
                                    %endif

                                %elif comptador in (0, 1) and not lectura:
                                    %if period_counter == 4:
                                        <td class="consumption_field align_c top_border">-</td>
                                        <td class="consumption_field align_c rightBorder top_border">-</td>
                                    %else:
                                        <td class="consumption_field align_c">-</td>
                                        <td class="consumption_field align_c rightBorder">-</td>
                                    %endif
                                %elif lectura:
                                    ## Escriure maxímetres
                                    ## <td class="consumption_field">${formatLang(lectura.pot_maximetre, digits=3)}<td>
                                %else:
                                    %if period_counter == 4:
                                        <td class="consumption_field align_c top_border">-</td>
                                    %else:
                                        <td class="consumption_field align_c">-</td>
                                    %endif
                                %endif
                                <%
                                    comptador += 1
                                %>
                            %endfor
                        </tr>
                        <%
                            period_counter += 1
                        %>
                    %endfor
                </table>
            %endfor
            %if motiu_ajust != '':
                <%
                    motivos = {
                        '01': u'Verificación equipo de medida',
                        '02': u'Avería en contador',
                        '03': u'Avería en Trafo de Tensión',
                        '04': u'Avería en Trafo de intensidad',
                        '05': u'Desbordamiento del Registrador',
                        '06': u'Problemas en la sincronización del registrador',
                        '07': u'Pérdida de alimentación del registrador',
                        '08': u'Manipulación de equipos',
                        '09': u'Servicio Directo (sin EM)',
                        '10': u'Punto de medida inaccesible',
                        '11': u'Punto de medida ilocalizable',
                        '99': u'Otros',
                    }
                %>
                <span class="separador_motiu">&nbsp;</span>
                <p> * ${_(u"Motiu d'ajustament:")} ${motivos.get(motiu_ajust)}</p>
            %endif
        </%def>

        %for inv in objects:
            <%
                setLang(inv.partner_id.lang)

                sentido = 1.0
                if inv.type in ['out_refund', 'in_refund']:
                    sentido = -1.0
                fact_obj = objects[0].pool.get('giscedata.facturacio.factura')
                factura = get_giscedata_fact(cursor, inv)[0]
                info = get_other_info(cursor, factura['id'])[0]
                polissa = polissa_obj.browse(cursor, uid, inv.polissa_id.id,
                             context={'date': inv.data_final.val})
                direccio = polissa.cups.direccio
                total_iese = 0.0
                total_iva = 0.0
                iva_dict = {}
                base = 0.0
                base_imponible = 0.0
                altres_lines = [l for l in inv.linia_ids if l.tipus not in ('lloguer', 'reactiva', 'energia', 'potencia')]
                total_altres = sum([l.price_subtotal for l in altres_lines])

                for t in inv.tax_line :
                    if 'especial sobre la electricidad'.upper() in t.name.upper():
                        base +=  t.base
                        total_iese += t.amount
                    elif 'IVA' in t.name.upper():
                        total_iva += t.amount
                        base_imponible += sentido * t.base
                        iva_dict[t.name] = t.base
            %>
            <div id="header" class="mb_10">
                <div id="logo">
                    <div id="img_logo">
                        <img src="${addons_path}/giscedata_facturacio_comer_lumina/report/assets/lumina-logo.svg" class="">
                        <table width="100%">
                            <tr>
                                <td class="small_text align_l">www.lumina.energy</td>
                                <td class="small_text align_r">${_(u"Energia verda, barata i d'aquí")}</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div id="invoice_details" class="">
                    <div id="green_box">
                        ${_(u"IMPORT FACTURA: ")} ${formatLang(sentido * inv.amount_total, digits=2)} €
                    </div>
                    <div id="details" class="pt_5">
                        <p>
                            ${_(u"Nº factura: ")} ${inv.number or ''} <br/>
                            ${_(u"Data emissió factura: ")} ${localize_period(inv.date_invoice, 'ca_ES')} <br/>
                            ${_(u"Període de consum: ")}
                            <%
                                factura = get_giscedata_fact(cursor, inv)[0]
                                data_inici = datetime.strptime(factura['data_inici'], '%Y-%m-%d')
                                data_inici_str = data_inici.strftime('%d/%m/%Y')
                                data_final = datetime.strptime(factura['data_final'], '%Y-%m-%d')
                                data_final_str = data_final.strftime('%d/%m/%Y')
                            %>
                            ${data_inici_str} al ${data_final_str}<br/>
                            %if inv.payment_type.code != 'RECIBO_CSB' and inv.state == 'open' and inv.type == 'out_invoice' and (not inv.group_move_id or grouped):
                                ${_(u"Data límit de pagament:")}
                                <br/>${localize_period(inv.date_due, 'ca_ES') or ''}<br/>
                            %else:
                                ${_(u"Data de càrrec:")}
                                <%
                                    if inv.payment_order:
                                        data_carrec = inv.payment_order.date_planned
                                    else:
                                        data_carrec = ''
                                %>
                                <br/>${data_carrec}<br/>
                            %endif
                        </p>
                    </div>
                </div>
            </div>

            <div id="outer">
                <span id="vertical_text">
                    ${company.rml_footer2 or ''}
                </span>
            </div>

            <div class="container-fluid _mt_15">
                <div class="row-fluid">
                    <div class="w_40p box_decoration span5">
                        <div style="height: 271px;">
                            <div class="align_c">
                                <%
                                    banner_img = banner_obj.get_banner(cursor, uid, 'giscedata.facturacio.factura',inv.date_invoice.val)
                                %>
                                %if banner_img:
                                    <img src="data:image/jpeg;base64,${banner_img}" style="max-width: 100%">
                                %else:
                                    <img id="publicitat" src="${addons_path}/giscedata_facturacio_comer_lumina/report/assets/publicitat_cat.png"/>
                                %endif
                            </div>
                        </div>
                    </div>

                    <div class="span7">
                        <div class="box_decoration">
                            <div style="">
                                <table width="100%">
                                    <tr class="">
                                        <td class="bold lh_12 pr_5 m_0 small_text">${_(u"Nom:")}</td>
                                        <td class="align_r lh_12 p_0 m_0 align_l postal_text">${inv.address_contact_id.name or ''}</td>
                                    </tr>
                                    <tr class="">
                                        <td class="bold lh_12 pr_5 m_0 small_text" valign="top">${_(u"Adreça:")}</td>
                                        <td class="align_r lh_12 p_0 m_0 align_l postal_text">
                                            ${inv.address_contact_id.street or ''}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td class="align_r lh_12 p_0 m_0 align_l postal_text">
                                            ${inv.address_contact_id.zip or ''}
                                            ${inv.address_contact_id.city or ''}
                                            ${inv.address_contact_id.country_id.name or ''}
                                        </td>
                                    </tr>
                                </table>
                                ##<p class="bold lh_13 p_0 m_0">
                                ##    Nom:
                                ##    ${inv.address_contact_id.name or ''}
                                ##</p>
                                ##<span class="font_10 lh_5">
                                ##    Adreça:
                                ##    ${inv.address_contact_id.street or ''}
                                ##    ${inv.address_contact_id.zip or ''}
                                ##    ${inv.address_contact_id.city or ''}
                                ##    ${inv.address_contact_id.country_id.name or ''}
                                ##</span>
                            </div>
                        </div>

                        <div class="pt_10">
                        <%
                            potencies = sorted(sorted(inv.linies_potencia, key=attrgetter('data_desde')), key=attrgetter('name'))
                            energia = sorted(sorted(inv.linies_energia, key=attrgetter('price_unit_multi'), reverse=True), key=attrgetter('name'))
                            reactiva = sorted(sorted(inv.linies_reactiva, key=attrgetter('price_unit_multi'), reverse=True), key=attrgetter('name'))
                            exceso_potencia = sorted(sorted([l for l in inv.linia_ids if l.tipus == 'exces_potencia'], key=attrgetter('price_unit_multi'), reverse=True), key=attrgetter('name'))
                            altres = [l for l in inv.linia_ids if l.tipus not in ('lloguer', 'reactiva', 'energia', 'potencia')]

                            discounts = get_discounts(cursor, inv)
                            discounts_pot = get_discounts(cursor, inv, 'potencia')
                            total_potencia_contractada = 0.0
                            total_energia_consumida = 0.0
                            total_reactiva_consumida = 0.0
                            total_exces_consumida = 0.0
                            total_lloguer = 0.0
                            dies_lloguer = 0
                            preu_lloguer = 0.0
                            total_altres = 0.0
                            if potencies:
                                for pot in potencies:
                                    total_potencia_contractada += pot.price_subtotal
                            if energia:
                                for ene in energia:
                                    total_energia_consumida += ene.price_subtotal
                            if reactiva:
                                for line in reactiva:
                                    total_reactiva_consumida += line.price_subtotal
                            if exceso_potencia:
                                for line in exceso_potencia:
                                    total_exces_consumida += line.price_subtotal
                            if inv.linies_lloguer:
                                for alq in inv.linies_lloguer:
                                    total_lloguer += alq.invoice_line_id.price_subtotal
                                    diff_dates = (datetime.strptime(alq.data_fins, '%Y-%m-%d') - datetime.strptime(alq.data_desde, '%Y-%m-%d'))
                                    # Sumem 1 dia al numero de dies perque el lloguer tambe s'ha de tenir en compte durant tot el primer dia
                                    dies_lloguer = diff_dates.days + 1
                                    preu_lloguer = alq.price_unit_multi
                            if altres:
                                for line in altres:
                                    total_altres += line.price_subtotal
                        %>
                            <h2> ${_(u"RESUM FACTURA")} </h2>
                            <div class="box_decoration p_15">
                                <table class="small_text" style="width: 100%">
                                    <tr class="h_17">
                                        <td>${_(u"CONCEPTE")}</td>
                                        <td class="align_r">${_(u"IMPORT")}</td>
                                    </tr>
                                    <tr class="h_17">
                                        <td>${_(u"Per potència contractada")}</td>
                                        <td class="align_r">${formatLang(sentido * total_potencia_contractada, digits=2)} €</td>
                                    </tr>
                                    <tr class="h_17">
                                        <td>${_(u"Per energia consumida")}</td>
                                        <td class="align_r">${formatLang(sentido * total_energia_consumida, digits=2)} €</td>
                                    </tr>
                                    %if reactiva:
                                        <tr class="h_17">
                                            <td>${_(u"Per energia reactiva")}</td>
                                            <td class="align_r">${formatLang(sentido * total_reactiva_consumida, digits=2)} €</td>
                                        </tr>
                                    %else:
                                        <tr class="h_17">
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    %endif
                                    <tr class="h_17">
                                        <td>${_(u"Impost d'electricitat")}</td>
                                        <td class="align_r">${formatLang(sentido * total_iese, digits=2)} €</td>
                                    </tr>
                                    <tr class="h_17">
                                        <td>${_(u"Lloguer d'equips de mesura i control")}</td>
                                        <td class="align_r">${formatLang(sentido * total_lloguer, digits=2)} €</td>
                                    </tr>
                                    <tr class="h_17">
                                        <td>${_(u"Altres despeses")}</td>
                                        <td class="align_r">${formatLang(sentido * total_altres, digits=2)} €</td>
                                    </tr>
                                    <tr class="h_17">
                                        <td>${_(u"IVA (21%)")}</td>
                                        <td class="align_r">${formatLang(sentido * total_iva, digits=2)} €</td>
                                    </tr>
                                </table>
                                <hr/>
                                <table  class="small_text" style="width:100%">
                                    <tr>
                                        <td class=""><b>${_(u"TOTAL IMPORT FACTURA")}</b></td>
                                        <td class="align_r"><b> ${formatLang(sentido * inv.amount_total, digits=2)} €</b></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section">
                <h2> ${_(u"INFORMACIÓ DEL CONSUM ELÈCTRIC")} </h2>
                <div class="box_decoration">
                    <div class="row-fluid">
                        <%
                            periodes_a = sorted(sorted(
                                list([lectura for lectura in inv.lectures_energia_ids
                                                if lectura.tipus == 'activa']),
                                key=attrgetter('name')
                                ), key=attrgetter('comptador'), reverse=True
                            )

                            periodes_r = sorted(sorted(
                                list([lectura for lectura in inv.lectures_energia_ids
                                            if lectura.tipus == 'reactiva']),
                                key=attrgetter('name')
                                ), key=attrgetter('comptador'), reverse=True
                            )

                            periodes_m = sorted(sorted(
                                list([lectura for lectura in inv.lectures_potencia_ids]),
                                key=attrgetter('name')
                                ), key=attrgetter('comptador'), reverse=True
                            )

                            lectures=map(None, periodes_a, periodes_r, periodes_m)

                            comptador_actual = None
                            comptador_anterior = None

                            comptadors = {}
                            llista_lect = []
                            data_actual = None
                            for lect in lectures:
                                if lect[0]:
                                    comptador_actual = lect[0].comptador
                                    data_ant = lect[0].data_anterior
                                    data_act = lect[0].data_actual
                                elif lect[1]:
                                    comptador_actual = lect[1].comptador
                                    data_ant = lect[1].data_anterior
                                    data_act = lect[1].data_actual
                                elif lect[2]:
                                    comptador_actual = lect[2].comptador
                                    data_ant = lect[2].data_anterior
                                    data_act = lect[2].data_actual

                                if comptador_anterior and comptador_actual != comptador_anterior:
                                    comptadors[(comptador_anterior, data_actual_pre, data_ant_pre)] = llista_lect
                                    llista_lect = []

                                if data_act:
                                    comptadors[(comptador_actual, data_act, data_ant)] = llista_lect
                                    llista_lect.append((lect[0], lect[1], lect[2]))

                                    comptador_anterior = comptador_actual
                                    data_actual_pre = data_act
                                    data_ant_pre = data_ant

                            total_periodes = len(comptadors[comptadors.keys()[0]])
                            canvi_comptador = len(comptadors) == 2
                            %>

                        ${consumption_table(comptadors)}
                        %if total_periodes == 6:
                            <div class="chart_consum span12 align_c" id="chart_consum_${inv.id}"></div>
                        %else:
                           <div class="chart_consum span6 align_c" id="chart_consum_${inv.id}"></div>
                        %endif
                    </div>
                </div>
            </div>
            %if (total_periodes == 6) and canvi_comptador:
                <p style="page-break-after: always">&nbsp;</p>
                <span class="p_15">&nbsp;</span>
            %endif
            <%
                pot_periode = dict([('P%s' % p,'') for p in range(1, 7)])
                dphone = get_distri_phone(cursor, uid, polissa)
                for pot in polissa.potencies_periode:
                    pot_periode[pot.periode_id.name] = pot.potencia
            %>
            <div class="section">
                <h2>${_(u"DADES DE CONTACTE")}</h2>
                <div class="box_decoration small_text">
                    <div class="container-fluid">
                        <div class="row-fluid">
                            <div class="contact_info box_decoration span5 offset1 p_0">
                                <div style="display:table-cell;vertical-align:middle;width:250px;height:85px;">
                                    <div style="text-align:center;">
                                        <h3 class="alert_text align_c p_0 m_0">${_(u"ATENCIÓ AL CLIENT I RECLAMACIONS")}</h3>
                                        <div class="align_c p_0 m_0 lh_11">
                                            <h4>
                                                900 52 59 69<br/>
                                                clients@lumina.energy
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="contact_info box_decoration span5 p_0">
                                <div style="display:table-cell;vertical-align:middle;width:250px;height:85px;">
                                    <div style="text-align:center;">
                                        <%
                                            try:
                                                distri_phone = ''.join([dphone[i:i+3] for i in range(0,len(dphone),3)])
                                            except:
                                                raise Exception(
                                                    u'La distribuidora no te numero de telefon definit!'
                                                )
                                        %>
                                        <h3 class="alert_text p_0 m_0">${_(u"AVARIES I URGÈNCIES")}</h3>
                                        <div class="p_0 m_0 lh_11">
                                            <h4>${distri_phone}</h4>
                                            ${_(u"Referència del contracte d'accés")} <br/>
                                            (${inv.polissa_id.distribuidora.name}):
                                            <span style="white-space: nowrap">${inv.polissa_id.ref_dist or ''}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <p class="smaller_text italic lh_8 pt_5">
                            ${_(u"Per a reclamacions sobre el contracte de subministrament o facturacions podrà dirigir-se, en el cas de tractar-se de una persona física, a la Conselleria de l'òrgan competent en matèria de consum de la Generalitat.")}
                            ## ${_(u"Així mateix, podrà acudir a la entitat de resolució alternativa de litigi xxxx en el telèfon 9xx xxx xxx")}
                        </p>
                    </div>
                </div>
            </div>

            %if not ((total_periodes == 6) and canvi_comptador):
                <p style="page-break-after: always">&nbsp;</p>
                <span class="p_15">&nbsp;</span>
            %endif

            <div class="section">
                <h2> ${_(u"DADES DEL CONTRACTE")} </h2>
                <div class="box_decoration small_text">
                    <table  class="small_text" style="width:100%">
                        <tr>
                            <td>
                                ${_(u"Titular:")}
                            </td>
                            <td>
                                ${info['res_partner_name'] or ' '}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                ${_(u"NIF:")}
                            </td>
                            <td>
                                ${info['res_partner_vat'][2:] or ''}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                ${_(u"Direcció de subministre:")}
                            </td>
                            <td>
                                ${direccio}
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                ${_(u"Tipus de contracte:")}
                            </td>
                            <%
                                discriminador = polissa.tarifa.get_num_periodes()
                            %>
                            <td class="strong" colspan="3">
                                 ${_(u"Contracte de subministrament al mercat lliure amb discriminació horària de ")} ${discriminador}
                                %if discriminador > 1:
                                    ${_(u"períodes.")}
                                %else:
                                    ${_(u"període.")}
                                %endif
                                </br>
                                ## ${_(u"Amb/sense comptador inteligent efectivament integrat amb el sistema de telegestió.")}<br/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                ${_(u"Peatje d'accés:")}
                            </td>
                            <td>
                                ${polissa.tarifa.name or ''}
                            </td>
                        <tr>
                            <td>
                                ${_(u"Potència contractada:")}
                            </td>
                            <td>

                                %for periode, potencia in sorted(pot_periode.items(),key=itemgetter(0)):
                                    %if potencia:
                                        ${periode}:
                                        ${formatLang(potencia, digits=3)}
                                    %endif
                                %endfor
                            </td>
                        </tr>
                        <tr>
                            %if polissa.data_baixa:
                                <td>
                                    ${_(u"Data final de contracte:")}
                                </td>
                                <td>
                                    ${localize_period(polissa.data_baixa, 'es_ES')}
                                </td>
                            %endif
                        </tr>
                    </table>

                    <span class="bold">Codi unificat de punt de subministrament CUPS:</span> ${inv.cups_id.name}
                    <div style="height: 40px" class="box_decoration">

                    <%def name="get_codi_barres(inv)">
                        <%
                            # bar code
                            from datetime import datetime, timedelta
                            import time
                            entity = ''.join([c for c in inv.polissa_id.comercialitzadora.vat if c.isdigit()])
                            suffix = '501'
                            due_date = datetime.strptime(inv.date_due, '%Y-%m-%d')

                            ref = '{}'.format(inv.id).zfill(11) # id factura
                            notice = '000000'
                            if inv.group_move_id:
                                grouped = amount_grouped(inv)
                                amount = '{0:.2f}'.format(grouped) # amount grouped factura
                            else:
                                amount = '{0:.2f}'.format(inv.amount_total) # amount factura
                            recibo507 = Recibo507(entity, suffix, ref, notice, amount, due_date)
                            return recibo507
                        %>
                    </%def>

                    <div class="row-fluid">
                        <%
                            entidad_bancaria = ''
                            bank_acc = ''
                            if inv.payment_type.code == 'RECIBO_CSB':
                                if not inv.partner_bank:
                                    raise Exception(
                                        u'La factura {0} no tiene número de cuenta definido.'.format(
                                        inv.number)
                                    )
                                else:
                                    entidad_bancaria = inv.partner_bank.bank.lname or inv.partner_bank.bank.name or inv.partner_bank.name
                                    bank_acc = inv.partner_bank.iban[:-4].replace(' ','')  + '****'
                            else:
                                if inv.partner_bank:
                                    entidad_bancaria = inv.partner_bank.bank.lname or inv.partner_bank.bank.name or inv.partner_bank.name
                                    bank_acc = inv.partner_bank.iban[:-4].replace(' ','') + '****'
                            recibo507 = get_codi_barres(inv)
                        %>

                        %if inv.payment_type.code != 'RECIBO_CSB' and inv.state == 'open' and inv.type == 'out_invoice':
                            <%
                                grouped = False
                                if inv.group_move_id:
                                    grouped = amount_grouped(inv)
                            %>

                            %if not inv.group_move_id or grouped:
                                <div class="span7">
                                    <table style="width: 100%">
                                        <tr class="">
                                            <td class="small_text bold"> ${_(u"NIF emissora")} </td>
                                            <td class="">&nbsp;</td> <!-- Suffix -->
                                            <td class="small_text bold"> ${_(u"Referència")} </td>
                                            <td class="small_text bold"> ${_(u"Ident.")} </td>
                                            <td class="small_text bold"> ${_(u"Import")} </td>
                                            <td class="small_text bold"> ${_(u"Data pagament")} </td>
                                        </tr>
                                        <tr class="">
                                            <td class="small_text">
                                                ${recibo507.entity}
                                            </td>
                                            <td class="small_text">
                                                ${recibo507.suffix}
                                            </td>
                                            <td class="small_text">
                                                ${recibo507.ref}
                                            </td>
                                            <td class="small_text">
                                                ${recibo507.notice}
                                            </td>
                                            <td class="small_text">
                                                % if inv.group_move_id:
                                                    ${formatLang(amount_grouped(inv), monetary=True, digits=2)}
                                                % else:
                                                    ${formatLang(inv.amount_total, digits=2)}
                                                % endif
                                            </td>
                                            <td class="small_text">
                                                ${recibo507.due_date.strftime('%d/%m/%Y')}
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="span5">
                                    ${ recibo507.svg(writer_options={'module_height': 6, 'font_size': 7, 'text_distance': 3, 'module_width': 0.22}) }
                                </div>
                            %endif
                        %else:
                            <div class="span12">
                                <table style="width:100%">
                                    <colgroup>
                                        <col style="width: 20%"/>
                                        <col style="width: auto"/>
                                    </colgroup>
                                    <tr>
                                        <td class="small_text bold">${_(u"Forma de pagament:")}</td>
                                        <td class="small_text" style="text-align: left">${inv.payment_type and inv.payment_type.name or ''}</td>
                                    </tr>
                                    <tr>
                                        <td class="small_text bold">${_(u"Entitat bancària:")}</td>
                                        <td class="small_text" style="text-align: left">${entidad_bancaria}</td>
                                    </tr>
                                    <tr>
                                        <td class="small_text bold">${_(u"Nº de Compte:")}</td>
                                        <td class="small_text" style="text-align: left">${bank_acc}</td>
                                    </tr>
                                </table>
                            </div>
                        %endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="section">
                <h2> ${_(u"DESTINACIÓ DE L'IMPORT DE LA FACTURA")} </h2>
                <div class="box_decoration small_text">
                    <div class="row-fluid">
                        <div class="span7 offset2">
                            <p>${_(u"El destí de l'import de la seva factura, ")} ${formatLang(sentido * inv.amount_total, digits=2)} ${_(u" euros, és el següent:")}
                            <div class="chart_desti" id="chart_desti_${inv.id}"></div>
                        </div>

                        ## <div class="span5 box_decoration align_c">
                        ##     ESPAI RESERVAT PER EXPLICACIÓ
                        ## </div>
                    </div>
                </div>
            </div>

            %if energia:
            <div class="section">
                <h2> ${_(u"ENERGIA CONSUMIDA")} </h2>
                <div class="box_decoration small_text">
                    <table class="small_text data_table" style="width: 100%">
                        <colgroup>
                            <col style="width: 20%"/>
                            <col style="width: auto"/>
                        </colgroup>
                        <tr>
                            <td class="bold align_r">${_(u"PERÍODE")}</td>
                            <td class="bold align_r">${_(u"ENERGIA CONSUMIDA")}</td>
                            <td class="bold align_r">${_(u"PREU UNITARI")}</td>
                            <td class="bold align_r">${_(u"TOTAL")}</td>
                        </tr>
                            <%
                                total_consum = 0
                            %>
                            %for ene in energia:
                                <%
                                    total_consum += ene.quantity
                                %>
                        <tr>
                            <td class="align_r">
                                ${ene.name}
                            </td>
                            <td class="align_r">
                                ${int(ene.quantity)} kWh
                            </td>
                            <td class="align_r">
                                ${formatLang(ene.price_unit_multi, digits=6)} €
                                %if ene.discount != 0:
                                    ${formatLang(ene.discount, digits=0)}% Dto:
                                %elif ene.isdiscount:
                                    ${formatLang(get_line_discount(ene)[0], digits=1)}% Dto:
                                %endif
                            </td>
                            <td class="align_r">
                                ${formatLang(sentido * ene.price_subtotal, digits=2)} €
                            </td>
                        </tr>
                            %endfor
                        <tr>
                            <td colspan="2" class="align_r bold">${_(u"TOTAL CONSUM:")} ${int(total_consum)} kWh</td>
                            <td>&nbsp;</td>
                            <td colspan="2" class="align_r bold">${_(u"TOTAL:")} ${formatLang(sentido * total_energia_consumida, digits=2)} €</td>
                        </tr>
                    </table>
                </div>
            </div>
            %endif

            %if potencies:
            <div class="section">
                    <h2> ${_(u"POTÈNCIA")} </h2>
                <div class="box_decoration small_text">
                    <table class="small_text data_table" style="width: 100%">
                        <colgroup>
                            <col style="width: 20%"/>
                            <col style="width: auto"/>
                        </colgroup>
                        <tr>
                            <td class="bold align_r">${_(u"PERÍODE")}</td>
                            <td class="bold align_r">${_(u"POTÈNCIA")}</td>
                            <td class="bold align_r">${_(u"PREU UNITARI (€/kW i dia)")}</td>
                            <td class="bold align_r">${_(u"DIES")}</td>
                            <td class="bold align_r">${_(u"TOTAL")}</td>
                        </tr>
                            %for pot in potencies:
                        <tr>
                                    <td class="align_r">${pot.name}</td>
                                    <td class="align_r">${pot.quantity} kW</td>
                                    <td class="align_r">${formatLang(pot.price_unit_multi, digits=6)}
                                        %if pot.discount != 0:
                                            ${formatLang(pot.discount, digits=0)}% ${_(u"Dte.")}
                                        %elif pot.isdiscount:
                                            ${formatLang(get_line_discount(pot)[0], digits=1)}% ${_(u"Dte.")}"
                                        %endif
                                    </td>
                                    <td class="align_r">${int(pot.multi)}</td>
                                    <td class="align_r">${formatLang(sentido * pot.price_subtotal, digits=2)} €</td>
                        </tr>
                            %endfor
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td class="align_r bold">${_(u"TOTAL:")} ${formatLang(sentido * total_potencia_contractada, digits=2)} €</td>
                        </tr>
                    </table>
                </div>
            </div>
            %endif

            <%
                te_maximetre = polissa.facturacio_potencia=='max' or len(periodes_a)>3
            %>

            %if (total_periodes == 6) or canvi_comptador or te_maximetre:
                <p style="page-break-after: always">&nbsp;</p>
                <span class="p_15">&nbsp;</span>
            %endif

            %if te_maximetre:

                <%
                    lectures_m = []
                    for lectura in inv.lectures_potencia_ids:
                        lectures_m.append((lectura.name, lectura.pot_contract,
                        lectura.pot_maximetre ))
                    fact_potencia = [0, 0, 0, 0, 0, 0]

                    maximetre = []

                    i = 0
                    for periode in periodes_m:
                        maximetre.append([periode])
                        i += 1

                    i = 0
                    for lectura in lectures_m:
                        maximetre[i].append(lectura[1])
                        i += 1

                    i = 0
                    for lectura in lectures_m:
                        maximetre[i].append(lectura[2])
                        i += 1

                    i = 0
                    for pot in potencies:
                        fact_potencia[i] = potencies[i].quantity
                        i += 1
                %>
                <%
                    for i in range(len(lectures_m)):
                        maximetre[i].append(fact_potencia[i])

                %>

                <div class="section">
                    <h2> ${_(u"AJUSTAMENT SEGONS LA POTÈNCIA DEMANDADA")} </h2>
                    <div class="box_decoration small_text">
                        <table class="small_text data_table" style="width: 100%">
                            <colgroup>
                                <col style="width: 20%"/>
                                <col style="width: auto"/>
                            </colgroup>
                            <tr>
                                <td class="bold align_r">${_(u"PERÍODE")}</td>
                                <td class="bold align_r">${_(u"POTÈNCIA")}<br/>${_(u"CONTRACTADA")}</td>
                                <td class="bold align_r">${_(u"DEMANDA")}<br/>${_(u"MÀXIMA")}</td>
                                <td class="bold align_r">${_(u"POTÈNCIA A")}<br/>${_(u"FACTURAR (*)")}</td>
                            </tr>
                            %for valor in maximetre:
                                <tr>
                                    <td class="align_r">${valor[0].name}</td>
                                    <td class="align_r">${valor[1]}</td>
                                    <td class="align_r">${valor[2]}</td>
                                    <td class="align_r">${valor[3]}</td>
                                </tr>
                            %endfor
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                        <b><u>${_(u"Regla del maxímetre")}</u></b><br/>
                        ${_(u"(*) Potència a facturar - Depèn del maxímetre i de la potència contractada (PC):")}<br/>
                        <ul>
                            <li> ${_(u"Si Maxímetre < 85% de PC → factura del 85% de la PC")} </li>
                            <li> ${_(u"Si Maxímetre > 105% de PC → factura de la potència que marca el maxímetre + 2 × [maxímetre - 105% PC]")} </li>
                            <li> ${_(u"Si Maxímetre entre 85% i 105% de PC → factura de la potència que marca el maxímetre")} </li>
                        </ul>
                    </div>
                </div>

            %endif
            %if reactiva:

            <div class="section">
                <h2> ${_(u"ENERGIA REACTIVA")} </h2>
                <div class="box_decoration small_text">
                        ${_(u"Ajustament segons dades subministrades per la distribuïdora des del ")} ${localize_period(inv.data_inici, 'ca_ES')} ${_(u" fins a ")} ${localize_period(inv.data_final, 'ca_ES')}
                        <table class="data_table" style="width: 100%">
                            <colgroup>
                                <col style="width: 20%"/>
                                <col style="width: auto"/>
                            </colgroup>
                            <tr>
                                <td class="small_text bold align_r"> ${_(u"PERÍODE")} </td>
                                <td class="small_text bold align_r"> ${_(u"ENERGIA REACTIVA")} </td>
                                <td class="small_text bold align_r"> ${_(u"PREU UNITARI")} </td>
                                <td class="small_text bold align_r"> ${_(u"TOTAL")} </td>
                            </tr>
                            %for react in reactiva:
                                <tr>
                                    <td class="small_text align_r">
                                        ${react.name}
                                    </td>
                                    <td class="small_text align_r"> ${int(react.quantity)} kVArh </td>
                                    <td class="small_text align_r"> ${formatLang(react.price_unit_multi, digits=6)} €/kWArh
                                        %if react.discount != 0:
                                            ${formatLang(react.discount, digits=0)}% ${_(u"dte.")}
                                        %elif react.isdiscount:
                                            ${formatLang(get_line_discount(react)[0], digits=1)}% ${_(u"dte.")}
                                        %endif
                                    </td>
                                    <td class="small_text align_r"> ${formatLang(sentido * react.price_subtotal, digits=2)} € </td>
                                </tr>
                            %endfor
                        <tr>
                                <td> &nbsp; </td>
                                <td> &nbsp; </td>
                                <td colspan="2" class="small_text bold align_r">Total: ${formatLang(sentido * total_reactiva_consumida, digits=2)} €</td>
                        </tr>
                    </table>
                </div>
            </div>

            %endif

            %if altres_lines:
            <div class="section">
                <h2> ${_(u"ALTRES DESPESES")} </h2>
                <div class="box_decoration small_text">
                    <table class="data_table" style="width: 100%">
                        <colgroup>
                            <col style="width: 55%"/>
                            <col style="width: auto"/>
                            <col style="width: 45%"/>
                        </colgroup>
                        <tr>
                            <td class="small_text bold align_l" style="padding-left: 30px;"> ${_(u"CONCEPTE")} </td>
                            <td> &nbsp; </td>
                            <td class="small_text bold align_r"> ${_(u"TOTAL")} </td>
                        </tr>
                        <%
                            i = 0
                            total_otros = 0
                        %>
                        %for i, linia in enumerate(altres_lines):
                            <tr>
                                <td class="small_text align_r">${linia.name}</td>
                               <td> &nbsp; </td>
                                <td class="small_text align_r">${formatLang(linia.price_subtotal, 2)} €</td>
                                <% total_otros = total_otros + linia.price_subtotal %>
                            </tr>
                        %endfor
                        <tr>
                            <td> &nbsp; </td>
                            <td> &nbsp; </td>
                            <td colspan="2" class="small_text bold align_r">TOTAL: ${formatLang(sentido * total_otros, digits=2)} €</td>
                        </tr>
                    </table>
                </div>
            </div>

            %endif

##            %if (total_periodes != 6) and (not canvi_comptador) and (not te_maximetre):
##                <p style="page-break-after: always">&nbsp;</p>
##                <span class="p_15">&nbsp;</span>
##            %endif

            <%
                # DATOS PIE CHART
                # Repartimento según BOE
                rep_BOE = {'i': 36.28, 'c': 32.12 ,'o': 31.60}
                pie_total = inv.amount_total - inv.total_lloguers
                pie_regulats = (inv.total_atr + total_altres)
                pie_impostos = float(inv.amount_tax)
                pie_costos = (pie_total - pie_regulats - pie_impostos )

                reparto = { 'i': ((pie_regulats * rep_BOE['i'])/100),
                            'c': ((pie_regulats * rep_BOE['c'])/100),
                            'o': ((pie_regulats * rep_BOE['o'])/100)
                           }

                dades_reparto = [
                    [[0, rep_BOE['i']], 'i', _(u"Incentius a les enegies renovables, cogeneració i residus"), formatLang(reparto['i'])],
                     [[rep_BOE['i'] , rep_BOE['i'] + rep_BOE['c']], 'c', _(u"Cost de les xarxes de transport i distribució"), formatLang(reparto['c'])] ,
                     [[rep_BOE['i'] + rep_BOE['c'], 100.00], 'o', _(u"Altres costos regulats (inclou anualitat del dèficit)"), formatLang(reparto['o'])]
                    ]
                historic = get_historics(cursor, info['giscedata_polissa_id'],inv.data_inici, inv.data_final)
                historic_graf = {}
                periodes_graf = []
                consums = {}
                historic_dies = 0
                total_historic_eur = 0

                for row in historic:
                        historic_graf.setdefault(row['mes'],{})
                        historic_graf[row['mes']].update({row['periode']: row['consum']})
                        periodes_graf.append(row['periode'])

                periodes_graf = list(set(periodes_graf))
                consums_mig = dict.fromkeys(list(set(periodes_graf)), 0)
                consums_mig.update({'total':0})
                historic_js = []
                for mes, consums in historic_graf.items():
                        p_js = {'mes': mes}
                        for p in periodes_graf:
                            p_js.update({p: consums.get(p,0.0)})
                            consums_mig[p] += consums.get(p,0.0)
                        historic_js.append(p_js)
                        consums_mig['total'] += 1
                if consums:
                        consums_mig = dict((k, v/consums_mig['total']) for k, v in consums.items() if k.startswith('P'))
                        consums_mig = sorted(consums_mig.items(),key=itemgetter(1),reverse=True)
                if historic:
                        total_historic_kw = sum([h['consum'] for h in historic])
                        total_historic_eur = sum([h['facturat'] for h in historic])
                        data_ini = min([h['data_ini'] for h in historic])
                        data_fin = max([h['data_fin'] for h in historic])
                        historic_dies = 1 + (datetime.strptime(data_fin, '%Y-%m-%d') - datetime.strptime(data_ini, '%Y-%m-%d')).days

                        mes_any_inicial = (datetime.strptime(inv.data_inici,'%Y-%m-%d') - timedelta(days=365)).strftime("%Y/%m")
                        h['mes'] = h['mes'][2:]
                        total_any = sum([h['consum'] for h in historic if h['mes'] > mes_any_inicial])
                #DATOS HISTORICO BARRAS
                dies_factura = 1 + (datetime.strptime(inv.data_final, '%Y-%m-%d') - datetime.strptime(inv.data_inici, '%Y-%m-%d')).days
                diari_factura_actual_eur = inv.total_energia / (dies_factura or 1.0)
                diari_factura_actual_kwh = (inv.energia_kwh * 1.0) / (dies_factura or 1.0)
            %>
            <script>
                var pie_total = ${pie_total};
                var pie_data = [{val: ${pie_regulats}, perc: 30, code: "REG"},
                                {val: ${pie_costos}, perc: 55, code: "PROD"},
                                {val: ${pie_impostos},  perc: 15 ,code: "IMP"}
                               ];

                var pie_etiquetes = {'REG': {t: ['${formatLang(float(pie_regulats))}€','${_(u"Costos regulats")}'], w:100},
                                     'IMP': {t: ['${formatLang(float(pie_impostos))}€','${_(u"Impostos aplicats")}'], w:100},
                                     'PROD': {t: ['${formatLang(float(pie_costos))}€','${_(u"Costos de producció")}'], w:100}
                                    };

                var reparto = ${json.dumps(reparto)}
                var dades_reparto = ${json.dumps(dades_reparto)}

                var factura_id = ${inv.id}
                var data_consum = ${json.dumps(sorted(historic_js, key=lambda h: h['mes']))}
            </script>
            <script src="${addons_path}/giscedata_facturacio_comer/report/assets/d3.min.js"></script>
            <script src="${addons_path}/giscedata_facturacio_comer_lumina/report/pie.js"></script>
            <script>
                var w = 630;
                var h = 200;
            </script>
            <script src="${addons_path}/giscedata_facturacio_comer_lumina/report/consum.js"></script>
            % if inv.has_tec271_line():
                <p style="page-break-after: always">&nbsp;</p>
                <%
                    cups = polissa_obj.read(cursor, uid, inv.polissa_id.id, ['cups'])['cups'][1]
                %>
                <div class="section box_decoration supl">
                    <h1 style="font-weight: bold; font-size:1.2em; padding: 0em 0.5em 0.5em 0.5em;">${_(u"TAULA DETALLADA DELS SUPLEMENTS AUTONÒMICS 2013 (*)")}</h1>
                    <%
                        html_table = sup_territorials_tec_271_comer_obj.get_info_html(cursor, uid, cups)
                    %>
                    ${html_table}
                    En caso de que el importe total de la regularización supere los dos euros, sin incluir impuestos, el mismo será fraccionado en partes iguales superiores a 1€ por las empresas comercializadoras en las facturas que se emitan en el plazo de 12 meses a partir de la primera regularización
                    <br>
                    * Tabla informativa conforme a lo establecido en la TEC/271/2019 de 6 de marzo, por la cual le informamos de los parámetros para el cálculo de los suplementos territoriales facilitados por su empresa distribuidora ${polissa.distribuidora.name}
                    <br>
                    <%
                        text = sup_territorials_tec_271_comer_obj.get_info_text(cursor, uid, cups)
                        if text:
                            text = text.format(distribuidora=polissa.distribuidora.name)
                    %>
                    ${text}
                </div>
                % endif
            <%
                comptador_factures += 1
            %>
            % if comptador_factures<len(objects):
                <p style="page-break-after:always"></p>
            % endif
        %endfor
    </body>
</html>
