# -*- coding: utf-8 -*-
{
    "name": "Reports Facturació Lumina (Comercialitzadora)",
    "description": """Reports Facturació Lumina (Comercialitzadora)""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Extrareports",
    "depends":[
        "giscedata_facturacio_comer",
        "report_banner",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_facturacio_comer_report.xml",
    ],
    "active": False,
    "installable": True
}
