# -*- coding: utf-8 -*-

from osv import osv
from tools import config


class AccountMoveLine(osv.osv):
    _name = 'account.move.line'
    _inherit = 'account.move.line'

    def extract_data(self, cursor, uid, acc_move_line_ids, context=None):
        query_file = (u"%s/account_link_lumina/data/consulta_moviments.sql" % config['addons_path'])
        query = open(query_file).read()

        cursor.execute(query, (tuple(acc_move_line_ids),))
        res = cursor.dictfetchall()
        return res

AccountMoveLine()
