select

inv.id as te_factura,
inv.type as invoice_type,
move.date as fecha_emision_registro,
move.name as n_assentament,
mline.ref as n_documento,
part.vat as prov_nif,
part.name as prov_nombre,
prov.name as prov_provinicia,
pais.name as prov_pais,
mline.ref as inv_descripcion,
mline.name as move_description,
acc.code as n_cuenta,
mline.debit as debit,
mline.credit as credit

FROM account_move_line mline
JOIN account_move move ON move.id=mline.move_id
JOIN res_partner part ON part.id=mline.partner_id
LEFT JOIN account_invoice inv ON mline.move_id = inv.move_id
LEFT JOIN res_partner_address inv_addr ON inv.address_invoice_id=inv_addr.id
LEFT JOIN res_municipi mun ON id_municipi=mun.id
LEFT JOIN res_country_state prov ON inv_addr.state_id=prov.id
LEFT JOIN res_country pais ON inv_addr.country_id=pais.id
JOIN account_account acc ON acc.id=mline.account_id

WHERE mline.id IN %s
