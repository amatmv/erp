# -*- coding: utf-8 -*-
import base64

from osv import osv, fields
from datetime import datetime
import pandas as pd
import StringIO
from collections import OrderedDict


class WizardExportAccountMoves(osv.osv_memory):
    _name = 'wizard.export.account.moves'
    _inherit = 'wizard.export.account.moves'

    def build_report_factures(self, invoices_data):
        output = StringIO.StringIO()
        writer = pd.ExcelWriter(output, engine='xlsxwriter')

        if len(invoices_data.get("out")):
            df_out = pd.DataFrame(invoices_data.get("out"), columns=invoices_data.get("out")[0].keys())
            df_out.to_excel(writer, sheet_name='VENTAS', index=False)
            # Posar els noms de les columnes "macos"
            worksheet = writer.sheets['VENTAS']
            for col_num, value in enumerate(df_out.columns.values):
                worksheet.write(0, col_num, value.upper().replace("-", " "))

        if len(invoices_data.get("in")):
            df_in = pd.DataFrame(invoices_data.get("in"), columns=invoices_data.get("in")[0].keys())
            df_in.to_excel(writer, sheet_name='COMPRAS', index=False)
            # Posar els noms de les columnes "macos"
            worksheet = writer.sheets['COMPRAS']
            for col_num, value in enumerate(df_in.columns.values):
                worksheet.write(0, col_num, value.upper().replace("-", " "))

        writer.save()
        res = output.getvalue()
        output.close()
        return res

    def build_report_moviments(self, moves_data):
        output = StringIO.StringIO()
        writer = pd.ExcelWriter(output, engine='xlsxwriter')

        df_in = pd.DataFrame(moves_data, columns=moves_data[0].keys())
        df_in.to_excel(writer, sheet_name='Movimientos', index=False)
        # Posar els noms de les columnes "macos"
        worksheet = writer.sheets['Movimientos']
        for col_num, value in enumerate(df_in.columns.values):
            worksheet.write(0, col_num, value.upper().replace("-", " "))

        writer.save()
        res = output.getvalue()
        output.close()
        return res

    def export_account_moves_data(self, cursor, uid, wiz_id, acc_move_line_data, context=None):
        if context is None:
            context = {}

        wiz = self.browse(cursor, uid, wiz_id)
        initial_date = wiz.initial_date
        end_date = wiz.end_date

        # Set filenames
        filename_template = ('importacio_{tipo_archivo}_{f_inicio}_{f_fin}.{file_type}')
        date_format = '%Y%m%d'
        vals = dict(
            f_inicio=datetime.strptime(initial_date, '%Y-%m-%d').strftime(date_format),
            f_fin=datetime.strptime(end_date, '%Y-%m-%d').strftime(date_format),
            file_type='xls',
        )
        vals.update({'tipo_archivo': 'factures'})
        filename_F = filename_template.format(**vals)
        vals.update({'tipo_archivo': 'moviments'})
        filename_M = filename_template.format(**vals)

        # Build registers with all the info
        fitxer_factures_results = {
            'in': [],
            'out': []
        }
        fitxer_moviments_results = []

        lumina_vat = self.pool.get("res.partner").read(cursor, uid, 1, ['vat'])['vat']
        mlines_counter = 1
        ctx = context.copy()
        ctx['cod-empresa'] = lumina_vat

        seen_invoices = []
        for acc_move_line in acc_move_line_data:
            if acc_move_line['te_factura']:
                if acc_move_line['te_factura'] in seen_invoices:
                    continue
                elif acc_move_line['invoice_type'] in ('out_invoice', 'out_refund'):
                    aux = self.build_out_invoice_register(cursor, uid, acc_move_line, context=ctx)
                    fitxer_factures_results['out'].append(aux)
                else:
                    aux = self.build_in_invoice_register(cursor, uid, acc_move_line, context=ctx)
                    fitxer_factures_results['in'].append(aux)
                seen_invoices.append(acc_move_line['te_factura'])
            else:
                ctx['no-linea'] = mlines_counter
                aux = self.build_move_line_register(cursor, uid, acc_move_line, context=ctx)
                fitxer_moviments_results.append(aux)
                mlines_counter += 1

        # Build the files
        fitxer_factures = self.build_report_factures(fitxer_factures_results)
        fitxer_moviments = self.build_report_moviments(fitxer_moviments_results)

        wiz.write({
            'fitxer_factures': base64.b64encode(fitxer_factures),
            'fitxer_factures_filename': filename_F,
            'fitxer_moviments': base64.b64encode(fitxer_moviments),
            'fitxer_moviments_filename': filename_M,
            'state': 'end'
        })
        return None

    def build_out_invoice_register(self, cursor, uid, data, context=None):
        if context is None:
            context = {}
        inv = self.pool.get("account.invoice").browse(cursor, uid, data['te_factura'])
        res = OrderedDict([
            ('fecha-emision', data['fecha_emision_registro']),
            ('fecha-operacion', data['fecha_emision_registro']),
            ('fecha-registro', data['fecha_emision_registro']),
            ('tipo-factura', ''),
            ('no-documento', inv.number),
            ('cuenta', "430000000"),
            ('nif', ''),
            ('nombre', ''),
            ('provincia', ''),
            ('pais', "011"),
            ('descripcion', "Fra. " + inv.number),
            ('omitir-fuera-de-plazo', ''),
            ('cod-iva', ''),
            ('bien-inversion', ''),
            ('servicios', ''),
            ('base-iva', ''),
            ('iva', ''),
            ('importe-iva', ''),
            ('req-equiv', ''),
            ('importe-rec-equiv', ''),
            ('cod-retencion', ''),
            ('base-retencion', ''),
            ('ret', ''),
            ('importe-ret', ''),
            ('cta-contrapartida', ''),
            ('clave-operacion', ''),
            ('det-oper-intracomunitaria', ''),
            ('cod-iva-2', ''),
            ('bien-inversion-2', ''),
            ('servicios-2', ''),
            ('base-iva-2', ''),
            ('iva-2', ''),
            ('importe-iva-2', ''),
            ('req-equiv-2', ''),
            ('importe-rec-equiv-2', ''),
            ('cod-retencion-2', ''),
            ('base-retencion-2', ''),
            ('ret-2', ''),
            ('importe-ret-2', ''),
            ('cta-contrapartida-2', ''),
            ('clave-operacion-2', ''),
            ('det-oper-intracomunitaria-2', ''),
            ('cod-iva-3', ''),
            ('bien-inversion-3', ''),
            ('servicios-3', ''),
            ('base-iva-3', ''),
            ('iva-3', ''),
            ('importe-iva-3', ''),
            ('req-equiv-3', ''),
            ('importe-rec-equiv-3', ''),
            ('cod-retencion-3', ''),
            ('base-retencion-3', ''),
            ('ret-3', ''),
            ('importe-ret-3', ''),
            ('cta-contrapartida-3', ''),
            ('clave-operacion-3', ''),
            ('det-oper-intracomunitaria-3', ''),
            ('total-factura', inv.amount_total),
            ('pago-automatico', ''),
            ('operacion-arrendamiento', ''),
            ('concurso-acreedores', ''),
            ('cod-seccion-analitica', ''),
            ('cod-proyecto-analitica', ''),
        ])

        i = 2
        for inv_tax in inv.tax_line:
            if "21%" in inv_tax.name:
                res.update([
                    ('base-iva', inv_tax.base),
                    ('iva', "21"),
                    ('importe-iva', inv_tax.amount),
                ])
            elif "elect" in inv_tax.name:
                res.update([
                    ('base-iva-2', inv_tax.base),
                    ('iva-2', "21"),
                    ('importe-iva-2', inv_tax.amount),
                ])
            else:
                res.update([
                    ('base-iva-{0}'.format(i+1), inv_tax.base),
                    ('iva-{0}'.format(i+1), inv_tax.tax_id.amount*100),
                    ('importe-iva-{0}'.format(i+1), inv_tax.amount),
                ])
                i += 1
        if res.get("base-iva-4"):
            raise osv.except_osv(u"Error", u"La factura {0} te mes de 3 impostos i el format de exportacio nomes esta preparat per tenir 3 impostos diferents.".format(inv.number))
        return res

    def build_in_invoice_register(self, cursor, uid, data, context=None):
        if context is None:
            context = {}
        inv = self.pool.get("account.invoice").browse(cursor, uid, data['te_factura'])
        res = OrderedDict([
            ('fecha-emision', data['fecha_emision_registro']),
            ('fecha-operacion', data['fecha_emision_registro']),
            ('fecha-registro', data['fecha_emision_registro']),
            ('tipo-factura', ''),
            ('no-documento', data['n_documento']),
            ('cuenta', ''),
            ('nif', data['prov_nif']),
            ('nombre', data['prov_nombre']),
            ('provincia', data['prov_provinicia']),
            ('pais', data['prov_pais']),
            ('descripcion', "Fra. " + (inv.origin or inv.number)),
            ('omitir-fuera-de-plazo', ''),
            ('cod-iva', ''),
            ('bien-inversion', ''),
            ('servicios', ''),
            ('base-iva', ''),
            ('iva', ''),
            ('importe-iva', ''),
            ('req-equiv', ''),
            ('importe-rec-equiv', ''),
            ('cod-retencion', ''),
            ('base-retencion', ''),
            ('ret', ''),
            ('importe-ret', ''),
            ('cta-contrapartida', ''),
            ('clave-operacion', ''),
            ('det-oper-intracomunitaria', ''),
            ('cod-iva-2', ''),
            ('bien-inversion-2', ''),
            ('servicios-2', ''),
            ('base-iva-2', ''),
            ('iva-2', ''),
            ('importe-iva-2', ''),
            ('req-equiv-2', ''),
            ('importe-rec-equiv-2', ''),
            ('cod-retencion-2', ''),
            ('base-retencion-2', ''),
            ('ret-2', ''),
            ('importe-ret-2', ''),
            ('cta-contrapartida-2', ''),
            ('clave-operacion-2', ''),
            ('det-oper-intracomunitaria-2', ''),
            ('cod-iva-3', ''),
            ('bien-inversion-3', ''),
            ('servicios-3', ''),
            ('base-iva-3', ''),
            ('iva-3', ''),
            ('importe-iva-3', ''),
            ('req-equiv-3', ''),
            ('importe-rec-equiv-3', ''),
            ('cod-retencion-3', ''),
            ('base-retencion-3', ''),
            ('ret-3', ''),
            ('importe-ret-3', ''),
            ('cta-contrapartida-3', ''),
            ('clave-operacion-3', ''),
            ('det-oper-intracomunitaria-3', ''),
            ('total-factura', inv.amount_total),
            ('pago-automatico', ''),
            ('operacion-arrendamiento', ''),
            ('concurso-acreedores', ''),
            ('cod-seccion-analitica', ''),
            ('cod-proyecto-analitica', ''),
        ])

        for i, inv_tax in enumerate(inv.tax_line):
            if i == 0:
                res.update([
                    ('base-iva', inv_tax.base),
                    ('iva', inv_tax.tax_id.amount*100),
                    ('importe-iva', inv_tax.amount),
                ])
            else:
                res.update([
                    ('base-iva-{0}'.format(i+1), inv_tax.base),
                    ('iva-{0}'.format(i+1), inv_tax.tax_id.amount*100),
                    ('importe-iva-{0}'.format(i+1), inv_tax.amount),
                ])
        if res.get("base-iva-4"):
            raise osv.except_osv(u"Error", u"La factura {0} te mes de 3 impostos i el format de exportacio nomes esta preparat per tenir 3 impostos diferents.".format(inv.number))
        return res

    def build_move_line_register(self, cursor, uid, data, context=None):
        if context is None:
            context = {}
        res = OrderedDict([
            ('cod-empresa', context.get("cod-empresa")),
            ('no-linea', context.get("no-linea")),
            ('fecha-registro', data['fecha_emision_registro']),
            ('no-documento', "AST. " + data['n_assentament']),
            ('no-documento-externo', data['n_assentament']),
            ('tipo-documento', ''),
            ('fecha-emision-documento', ''),
            ('tipo-mov', ''),
            ('no-cuenta', data['n_cuenta'].zfill(9)),
            ('descripcion', data['move_description']),
            ('tipo-reg-iva-reten', ''),
            ('tipo-mov-iva-reten', ''),
            ('cod-divisa', ''),
            ('importe-debe', data['debit']),
            ('importe-haber', data['credit']),
            ('cod-iva', ''),
            ('iva', ''),
            ('cod-forma-pago', ''),
            ('cod-terminos-pago', ''),
            ('cod-departamento', ''),
            ('op-triangular', ''),
            ('cod-retencion', ''),
            ('retencion', ''),
            ('importe-base-iva', ''),
            ('tipo-calculo-iva', ''),
            ('tipo-asiento', ''),
            ('suplidos', ''),
            ('cliente-suplidos', ''),
            ('liq-por-tipo-documento', ''),
            ('liq-por-no-documento', ''),
            ('tipo-prorrata-especial', ''),
            ('no-cuenta-prorrata', ''),
            ('bien-de-inversion', ''),
            ('operacion-de-arrendamiento', ''),
            ('cod-inmueble-relacionado', ''),
            ('operacion-intragrupo', ''),
            ('cod-seccion-analitica', ''),
            ('cod-proyecto-analitica', ''),
            ('fecha-vencimiento', ''),
            ('tipo-doc-factura', ''),
            ('omitir-factura-en-347', ''),
            ('no-efecto', ''),
            ('a-f-tipo-registro', ''),
            ('cod-libro-amortizacion', ''),
            ('medio-de-cobro-pago', ''),
            ('iban-o-desc-medio-cobro-pago', ''),
            ('liq-por-no-efecto', ''),
        ])
        return res

    _columns = {
        'fitxer_factures': fields.binary(string='Archivo A (Plan contable)',
                                   readonly=True),
        'fitxer_factures_filename': fields.char('Nombre archivo A', size=128),
        'fitxer_moviments': fields.binary(string='Archivo D (Asientos ordinarios)',
                                   readonly=True),
        'fitxer_moviments_filename': fields.char('Nombre archivo D', size=128),
    }


WizardExportAccountMoves()
