# -*- coding: utf-8 -*-
from report_joiner.report_joiner import ReportJoiner

from c2c_webkit_report import webkit_report
from report import report_sxw
from tools import config


class report_webkit_html(report_sxw.rml_parse):
    def __init__(self, cursor, uid, name, context):
        super(report_webkit_html, self).__init__(cursor, uid, name,
                                                 context=context)
        self.localcontext.update({
            'cursor': cursor,
            'uid': uid,
            'addons_path': config['addons_path']
        })

webkit_report.WebKitParser(
    'report.report_preus_annex',
    'report_preus_annex',
    'giscedata_polissa_comer_lumina/report/preus.mako',
    parser=report_webkit_html
)

ReportJoiner(
    'report.giscedata.polissa.annex', [
        'report.giscedata.polissa',
        'report.giscedata.polissa.comer.lumina.precios'
    ]
)