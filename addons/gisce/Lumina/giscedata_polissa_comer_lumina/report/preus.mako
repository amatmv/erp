<%
    from osv import osv
    mostrar_reactiva = [
        '003', '011', '012', '017', '013', '014', '015', '016'
    ]
    mostrar_exces = [
        '012', '017', '013', '014', '015', '016'
    ]

    ## ---------- Versió modificada de la de utils.py ----------
    ##  --- Agafa product_id diferent i retorna unitat_uom ---

    def get_atr_price(cursor, uid, polissa, pname, tipus, tarifa, context):
        from datetime import date
        from datetime import datetime
        import calendar

        pricelist_obj = polissa.pool.get('product.pricelist')
        pricelistver_obj = polissa.pool.get('product.pricelist.version')
        pricelistitem_obj = polissa.pool.get('product.pricelist.item')

        product_uom_obj = polissa.pool.get('product.uom')
        product_id = polissa_tarifa_obj.get_periodes_producte(cursor, uid, tarifa, tipus)[pname]
        uom_unitat = product_template_obj.read(cursor, uid, product_id, ['uom_id'])['uom_id'][1]
        if not context['date']:
            context = {
                'date': date.today()
            }

        if tipus == 'tp':
            uom_id = product_uom_obj.search(
                cursor, uid, [('factor', '=', 365), ('name', '=', 'kW/dia')])[0]
            if calendar.isleap(datetime.today().year):
                imd_obj = polissa.pool.get('ir.model.data')
                uom_id = imd_obj.get_object_reference(
                    cursor, uid, 'giscedata_facturacio', 'uom_pot_elec_dia_traspas'
                )[1]
                context['uom'] = uom_id
            context.update({
                'uom': uom_id
            })
        pricelist = polissa['llista_preu']
        discount = 0
        if pricelist.visible_discount:
            date_price = date.today().strftime('%Y-%m-%d')
            version = pricelistver_obj.search(cursor, uid,
                [
                    ('pricelist_id', '=', pricelist.id),
                    '|',
                    ('date_start', '=', False),
                    ('date_start', '<=', date_price),
                    '|',
                    ('date_end', '=', False),
                    ('date_end', '>=', date_price)

                ], order='id', limit=1)
            if version:
                items = pricelistitem_obj.search(
                        cursor, uid, [
                            ('price_version_id', '=', version[0]),
                            ('product_id', '=', product_id)
                        ]
                )
                if items:
                    params_to_read = ['price_discount']
                    for item in pricelistitem_obj.read(
                            cursor, uid, items, params_to_read):
                        if item['price_discount']:
                            discount = item['price_discount']
                            break

        price_atr = pricelist_obj.price_get(cursor, uid, [pricelist.id],
                                            product_id, 1,
                                            1,
                                            context)[pricelist.id]
        if discount:
            price_atr = price_atr / (1.0 + discount)
            discount = abs(discount)
        return price_atr, uom_unitat, discount
%>

<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
        <title> Anexo de precios </title>
        <link rel="stylesheet" href="${addons_path}/giscedata_polissa_comer_lumina/report/bootstrap.css"/>
        <link rel="stylesheet" href="${addons_path}/giscedata_polissa_comer_lumina/report/stylesheet_contract.css"/>
    </head>
    <body>
        %for polissa in objects:
            <%
                pool = objects[0].pool
                polissa_tarifa_obj = pool.get('giscedata.polissa.tarifa')
                periodes_obj = pool.get('giscedata.polissa.tarifa.periodes')
                product_template_obj = pool.get('product.template')

                llista_preus = polissa.llista_preu
            %>

            <div id="header_layout" class="">
                <br/>
                <div id="logo_container">
                    <img id="logo" src="data:image/jpeg;base64,${company.logo}"/>
                </div>
            </div>
            <div id="title" class="">
                <h1> ${_("Anexo de Precios")} </h1>
            </div>

            <%
                pricelist = {}
                id_preus = llista_preus.id
                if not id_preus:
                    raise osv.except_osv(_('ERROR: No hay tarifa de comercializadora'), _(''))

                if str(polissa.tarifa) == '':
                    query = "SELECT tarifa_id FROM giscedata_polissa_tarifa_pricelist WHERE pricelist_id={}".format(llista_preus.id)
                    cursor.execute(query)
                    tarifa_list = cursor.fetchall()
                else:
                    tarifa_list = [[polissa.tarifa.id]]

                tipus = [('te', 'e'), ('tp', 'p'), ('tr', 'r'), ('ep', 'm')]
                for tarifa_id  in tarifa_list:
                    for tipu in tipus:
                        tarifa = polissa_tarifa_obj.read(cursor, uid, tarifa_id[0], ['name'])
                        tarifa_name = tarifa['name']
                        pricelist.setdefault(tarifa_name, {})
                        periodes = polissa_tarifa_obj.get_periodes_producte(cursor, uid, tarifa_id[0], tipu[0], context=None)
                        ctx = {}
                        ctx['date'] = polissa.data_alta
                        for p in periodes:
                            price = get_atr_price(cursor, uid, polissa, p, tipu[0], tarifa_id, ctx)
                            pricelist[tarifa_name].setdefault(tipu[1], {})
                            pricelist[tarifa_name][tipu[1]][p] = (price[0], price[1])
            %>

            %for acces in sorted(pricelist):
                <div class="form_section container-fluid">
                    <div id="datos_titular" class="">
                        <div class="annex_section_title">
                            <h3> ${_("PRECIO TARIFA DE ACCESO: ")} ${acces} </h3>
                        </div>
                        <%
                            id_tarifa = polissa_tarifa_obj.search(cursor, uid, [('name', '=', acces)], limit=1)
                            codi_ocsum = polissa_tarifa_obj.read(cursor, uid, id_tarifa, ['codi_ocsum'])[0]['codi_ocsum']

                            if codi_ocsum in mostrar_exces:
                                span = 'span' + str(12 / len(pricelist[acces]))
                            elif codi_ocsum in mostrar_reactiva:
                                span = 'span' + str(max(12/3, 12/len(pricelist[acces])))
                            else:
                                span = 'span' + str(6)
                        %>

                        <div class="small_text row-fluid">
                            %if pricelist[acces].get('e', False):
                                <div class="${span}">
                                    <div class="annex_section_subtitle">
                                        <h4> ${_("Energia")} </h4>
                                    </div>
                                    <div class="annex_section_content">
                                        <table class="data_table">
                                            %for product in sorted(pricelist[acces]['e']):
                                                %if pricelist[acces]['e'][product][0]:
                                                    <tr>
                                                        <td>${product}</td>
                                                        <td>
                                                            ${pricelist[acces]['e'][product][0]}
                                                            € / ${pricelist[acces]['e'][product][1]}
                                                        </td>
                                                    </tr>
                                                %endif
                                            %endfor
                                        </table>
                                    </div>
                                </div>
                            %endif
                            %if pricelist[acces].get('p', False):
                                <div class="${span}">
                                    <div class="annex_section_subtitle">
                                        <h4> ${_("Potencia")} </h4>
                                    </div>
                                    <div class="annex_section_content">
                                        <table class="data_table">
                                            %for product in sorted(pricelist[acces]['p']):
                                                %if pricelist[acces]['p'][product][0]:
                                                    <tr>
                                                        <td>${product}</td>
                                                        <td>
                                                            ${pricelist[acces]['p'][product][0]}
                                                            € / ${pricelist[acces]['p'][product][1]}
                                                        </td>
                                                    </tr>
                                                %endif
                                            %endfor
                                        </table>
                                    </div>
                                </div>
                            %endif
                            %if codi_ocsum in mostrar_reactiva:
                                %if pricelist[acces].get('r', False):
                                    <div class="${span}">
                                        <div class="annex_section_subtitle">
                                            <h4> ${_("Reactiva")} </h4>
                                        </div>
                                        <div class="annex_section_content">
                                            <table class="data_table">
                                                %for product in sorted(pricelist[acces]['r']):
                                                    %if pricelist[acces]['r'][product][0]:
                                                        <tr>
                                                            <td>${product}</td>
                                                            <td>
                                                                ${pricelist[acces]['r'][product][0]}
                                                                € / ${pricelist[acces]['r'][product][1]}
                                                            </td>
                                                        </tr>
                                                    %endif
                                                %endfor
                                            </table>
                                        </div>
                                    </div>
                                %endif
                            %endif
                            %if codi_ocsum in mostrar_exces:
                                %if pricelist[acces].get('m', False):
                                    <div class="${span}">
                                        <div class="annex_section_subtitle">
                                            <h4> ${_("Exceso de potencia")} </h4>
                                        </div>
                                        <div class="annex_section_content">
                                            <table class="data_table">
                                                %for product in sorted(pricelist[acces]['m']):
                                                    %if pricelist[acces]['m'][product][0]:
                                                        <tr>
                                                            <td>${product}</td>
                                                            <td>
                                                                ${pricelist[acces]['m'][product][0]}
                                                                € / ${pricelist[acces]['m'][product][1]}
                                                            </td>
                                                        </tr>
                                                    %endif
                                                %endfor
                                            </table>
                                        </div>
                                    </div>
                                %endif
                            %endif
                        </div>
                        <br/>
                    </div>
                </div>
            %endfor
        %endfor
    </body>
</html>
