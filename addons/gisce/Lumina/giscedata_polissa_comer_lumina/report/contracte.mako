<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
	<head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
        <link rel="stylesheet" href="${addons_path}/giscedata_polissa_comer_lumina/report/stylesheet_contract.css"/>
	</head>
    <%
        from datetime import datetime
        from collections import namedtuple
        from giscedata_polissa.report.utils import localize_period, datetime_to_date
        from giscedata_facturacio.report.utils import get_atr_price

        def clean_text(text):
            return text or ''
        comptador_polisses = 0
    %>
	<%def name="clean(text)">
		${text or ''}
	</%def>

    <%def name="enviament(diferent, text)">
        %if not diferent:
            ${clean(text)}
        %endif
    </%def>
<%
    res = {'currency': company.currency_id.id}
    address = company.partner_id.address[0]
    for field in 'street street2 zip city email phone'.split():
        if address[field]:
            res[field] = address[field]
    %>

	<body>
		%for polissa in objects:
            <%
                setLang(polissa.titular.lang)
                pool = polissa.pool
                direccio_titular = polissa.direccio_pagament
                direccio_envio = polissa.direccio_notificacio
                diferent = (polissa.direccio_notificacio == polissa.direccio_pagament)
                direccio_ps = polissa.cups
                direccio_cups = polissa.cups_direccio
                idx_pob = direccio_cups.rfind('(')
                if idx_pob != -1:
                    direccio_cups = direccio_cups[:idx_pob]

                data_inici = ''
                data_final = ''
                if 'form' in data.keys():
                    form = data['form']
                    data_inici = form['polissa_date']
                    modcon_obj = pool.get('giscedata.polissa.modcontractual')
                    modcon_id = modcon_obj.search(cursor, uid, [
                     ('polissa_id', '=', polissa.id),
                     ('data_inici', '=', data_inici)
                    ], context={'active_test': False})
                    data_final = modcon_obj.read(cursor, uid, modcon_id, ['data_final'])[0]['data_final']
                else:
                    data_inici = polissa.modcontractual_activa.data_inici
                    data_final = polissa.modcontractual_activa.data_final

                d_inici = data_inici and formatLang(data_inici, date=True)
                d_final = data_final and formatLang(data_final, date=True)
            %>

            <div id="header_layout" class="">
                <br/>
                <div id="logo_container">
                    <img id="logo" src="data:image/jpeg;base64,${company.logo}"/>
                </div>
            </div>
            <div id="title" class="">
                <h1> ${_("CONTRATO DE SUMINISTRO ELÉCTRICO")} </h1>
                <h2> ${_("Condiciones Particulares")} </h2>
            </div>

            <div class="form_section">
                <div id="datos_titular" class="">
                    <div class="section_title">
                        <h3> ${_("DATOS TITULAR DEL SUMINISTRO")} </h3>
                    </div>
     
                    <table class="data_table">
                        <colgroup>
                            <col style="width: 20%"/>
                            <col style="width: 50%"/>
                            <col style="width: 10%"/>
                            <col style="width: 20%"/>
                        </colgroup>
                        <tr>
                            <%
                            owner_b = ''
                            if polissa.bank.owner_name:
                                owner_b = polissa.bank.owner_name
                            %>
                            <td>
                                ${_("Titular (cliente):")}
                            </td>
                            <td>
                                <div class="insert_box">
                                    ${polissa.pagador.name}
                                </div>
                            </td>
                            <td>
                                ${_("CIF/NIF:")}
                            </td>
                            <td>
                                <div class="insert_box">
                                    ${polissa.pagador and (polissa.pagador.vat or '').replace('ES', '')}
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                ${_("Dirección fiscal:")}
                            </td>
                            <td>
                                <div class="insert_box">
                                    ${clean(direccio_titular.street)}
                                </div>
                            </td>
                            <td>
                                ${_("C.P.:")}
                            </td>
                            <td>
                                <div class="insert_box">
                                    ${clean(direccio_titular.zip)}
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                ${_("Localidad:")}
                            </td>
                            <td>
                                <div class="insert_box">
                                    ${clean(direccio_titular.city)}
                                </div>
                            </td>
                            <td>
                                ${_("Provincia:")}
                            </td>
                            <td>
                                <div class="insert_box">
                                    ${clean(direccio_titular.state_id.name)}
                                </div>
                            </td>
                        </tr>
                    </table>
     
                </div>
     
                <div id="datos_contacto" class="">
                    <div class="section_title">
                        <div id="same_reason">
                            ## <div class="after_checkmark">
                            ${_("Marcar en caso de ser la misma que la fiscal")}
                            ## </div>
                            <span class="checkmark">
                                <div class="checkmark_box"></div>
                                <%
                                    misma_direccion = direccio_envio.street == direccio_titular.street
                                %>
                                %if misma_direccion:
                                    <div class="checkmark_stem"></div>
                                    <div class="checkmark_kick"></div>
                                %endif
                            </span>
                        </div>
                        <h3> ${_("DATOS DE CONTACTO Y ENVÍO DE FACTURAS")} </h3>
                    </div>

                    <table class="data_table">
                        <colgroup>
                            <col style="width: 20%"/>
                            <col style="width: 25%"/>
                            <col style="width: 10%"/>
                            <col style="width: 15%"/>
                            <col style="width: 10%"/>
                            <col style="width: 20%"/>
                        </colgroup>
                        <tr>
                            <td>
                                ${_("Dirección de facturación:")}
                            </td>
                            <td colspan="3">
                                <div class="insert_box">
                                    ${clean(direccio_envio.street)}
                                </div>
                            </td>
                            <td>
                                ${_("C.P.:")}
                            </td>
                            <td>
                                <div class="insert_box">
                                    ${clean_text(direccio_envio.zip)}
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                ${_("Localidad:")}
                            </td>
                            <td colspan="3">
                                <div class="insert_box">
                                    ${clean_text(direccio_envio.city)}
                                </div>
                            </td>
                            <td>
                                ${_("Provincia:")}
                            </td>
                            <td>
                                <div class="insert_box">
                                    ${clean_text(direccio_envio.state_id.name)}
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                ${_("Forma de envío:")}
                            </td>
                            <td style="text-align: left;">
                                <span class="checkmark">
                                    <div class="checkmark_box"></div>
                                    <div class="checkmark_stem"></div>
                                    <div class="checkmark_kick"></div>
                                </span>
                                <span class="after_checkmark">${_("Correo electrónico")}</span>
                            </td>
                            <td colspan="5">
                                &nbsp;
                            </td>                            
                        </tr>
                        <tr>
                            <td>
                                ${_("Email:")}
                            </td>
                            <td>
                                <div class="insert_box">
                                    ${clean(direccio_titular.email)}
                                </div>
                            </td>
                            <td>
                                ${_("Teléfono:")}
                            </td>
                            <td>
                                <div class="insert_box">
            					    <span class="field">${clean(direccio_titular.phone)}</span>
                                </div>
                            </td>
                            <td>
                                ${_("Móvil:")}
                            </td>
                            <td>
                                <div class="insert_box">
        					        <span class="field">${clean(direccio_titular.mobile)}</span>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
     
                <div id="datos_suministro" class="">
                    <div class="section_title">
                        <h3> ${_("DATOS DEL PUNTO DE SUMINISTRO")} </h3>
                    </div>

                    <table class="data_table">
                        <colgroup>
                            <col style="width: 20%"/>
                            <col style="width: 25%"/>
                            <col style="width: 15%"/>
                            <col style="width: 10%"/>
                            <col style="width: 10%"/>
                            <col style="width: 20%"/>
                        </colgroup>
                        <tr>
                            <td>
                                ${_("Dirección del suministro:")}
                            </td>
                            <td colspan="3">
                                <div class="insert_box">
        					        <span class="field">${direccio_cups}</span>
                                </div>
                            </td>
                            <td>
                                ${_("C.P.:")}
                            </td>
                            <td>
                                <div class="insert_box">
                                    ${clean(direccio_ps.dp)}
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                ${_("Localidad:")}
                            </td>
                            <td colspan="3">
                                <div class="insert_box">
                                    ${clean(direccio_ps.id_poblacio.name)}
                                </div>
                            </td>
                            <td>
                                ${_("Provincia:")}
                            </td>
                            <td>
                                <div class="insert_box">
                                    ${clean(direccio_ps.id_provincia.name)}
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                ${_("CUPS:")}
                            </td>
                            <td>
                                <div class="insert_box">
                                    ${polissa.cups.name}
                                </div>
                            </td>
                            <td>
                                ${_("Tarifa de acceso:")}
                            </td>
                            <td>
                                <div class="insert_box">
                                    ${polissa.tarifa_codi or _("Vigente")}
                                </div>
                            </td>
                            <td>
                                ${_("CNAE:")}
                            </td>
                            <td>
                                <div class="insert_box">
                                    ${clean(polissa.cnae.name)}
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
        					    ${_("Empresa Distribuidora:")}
                            </td>
                            <td colspan="3">
                                <div class="insert_box">
        					        ${clean(polissa.distribuidora.name)}
                                </div>
                            </td>
                            <td>
        					    ${_("Nº contrato:")}
                            </td>
                            <td>
                                <div class="insert_box">
        					        ${clean(polissa.name)}
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                ${_("Ref. Catastral:")}
                            </td>
                            <td colspan="3">
                                <div class="insert_box">
                                    ${polissa.cups.ref_catastral or ''}
                                </div>
                            </td>
                        </tr>
                    </table>

                    <%
                        potencies = polissa.potencies_periode
                        if potencies:
                            periodes = []
                            for i in range(0, 6):
                                if i < len(potencies):
                                    periode = potencies[i]
                                else:
                                    periode = False
                                periodes.append((i+1, periode))
                    %>
                    %if potencies:
                    <table class="data_table collapse">
                        <colgroup>
                            <col style="width: 20%"/>
                            <col style="width: 13%"/>
                            <col style="width: 13%"/>
                            <col style="width: 13%"/>
                            <col style="width: 13%"/>
                            <col style="width: 13%"/>
                            <col style="width: 13%"/>
                        </colgroup>
                        <tr>
                            <th>
                                &nbsp;
                            </th>
                            <th class="center">
                                P1
                            </th>
                            <th class="center">
                                P2
                            </th>
                            <th class="center">
                                P3
                            </th>
                            <th class="center">
                                P4
                            </th>
                            <th class="center">
                                P5
                            </th>
                            <th class="center">
                                P6
                            </th>
                        </tr>
                        <tr>
                            <td>
                                ${_("Potencias contratadas")} <span class="smaller_text">${(" [kW]:")}</span>
                            </td>
                            %for p in periodes:
                                <td style="width: 60px;" class="center border">
                                    <span class="field">${p[1] and p[1].potencia or ' '}</span>
                                </td>
                            %endfor
                        </tr>
                    </table>
                    %else:
                        <table class="data_table">
                            <colgroup>
                                <col style="width: 20%"/>
                                <col style="width: 50%"/>
                                <col style="width: 30%"/>
                            </colgroup>
                            <tr>
                                <td>
                                    ${_("Potencias contratadas:")}
                                </td>
                                <td>
                                    <div class="insert_box">
                                        ${_("Potencias contratadas vigentes")}
                                    </div>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    %endif

                    <br/>

                    <table class="data_table collapse">
                        <colgroup>
                            <col style="width: 20%"/>
                            <col style="width: 10%"/>
                            <col style="width: 70%"/>
                        </colgroup>
                        <tr>
                            <td>
                                ${_("Fecha de inicio estimada:")}
                            </td>
                            <td class="border center">
                                ${d_inici or ''}
                            </td>
                            <td rowspan="2" style="text-align: center" class="border">
                                ${_("La fecha efectiva de entrada en vigor será la que corresponda según se establece en las Condiciones Generales del Contrato")}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                ${_("Fecha de fin de contrato:")}
                            </td>
                            <td class="border center">
                                ${d_final or ''}
                            </td>
                    </table>
                </div>
     
                <div id="precio_contratados" class="">
                    <div class="section_title">
                        <h3> ${_("PRECIOS CONTRATADOS")} </h3>
                    </div>

                    <%
                        import calendar
                        if potencies:
                            ctx = {
                                'sense_agrupar': True,
                            }
                            periodes_energia = sorted(polissa.tarifa.get_periodes(context=ctx).keys())
                            periodes_potencia = sorted(polissa.tarifa.get_periodes('tp', context=ctx).keys())
                            if periodes:
                                ctx = {
                                    'date': datetime.today().strftime('%Y-%m-%d'),
                                }
                                data_i = data_inici and datetime.strptime(polissa.modcontractual_activa.data_inici, '%Y-%m-%d')
                                if data_i and calendar.isleap(data_i.year):
                                    dies = 366
                                else:
                                    dies = 365
                    %>

                    %if potencies:
                        <table class="data_table collapse">
                            <colgroup>
                                <col style="width:30%"/>
                                <col style="width:10%"/>
                                <col style="width:10%"/>
                                <col style="width:10%"/>
                                <col style="width:10%"/>
                                <col style="width:10%"/>
                                <col style="width:10%"/>
                            </colgroup>
                            <thead>
                                <tr>
                                    <th>
                                        &nbsp;
                                    </th>
                                    <th>
                                        P1
                                    </th>
                                    <th>
                                        P2
                                    </th>
                                    <th>
                                        P3
                                    </th>
                                    <th>
                                        P4
                                    </th>
                                    <th>
                                        P5
                                    </th>
                                    <th>
                                        P6
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        ${_("Precio del término de energía €/kWh:")}
                                    </td>
                                    %for p in periodes_energia:
                                        <td style="width: 60px;" class="center border">
                                            <span class="field">${get_atr_price(cursor, uid, polissa, p, 'te', ctx)[0]}</span>
                                        </td>
                                    %endfor
                                    %if len(periodes_energia) < 6:
                                        %for p in range(0, 6-len(periodes_energia)):
                                            <td style="width: 60px;" class="center border">
                                                &nbsp;
                                            </td>
                                        %endfor
                                    %endif
                                </tr>
                                <tr>
                                    <td>
                                        ${_("Precio del término de potencia €/kW/año:")}
                                    </td>
                                    <%
                                    ctx2 = ctx.copy()
                                    ctx2['potencia_anual'] = True
                                    %>
                                    %for p in periodes_potencia:
                                        <td style="width: 60px;" class="center border">
                                            <span class="field">${get_atr_price(cursor, uid, polissa, p, 'tp', ctx2)[0]}</span>
                                        </td>
                                    %endfor
                                    %if len(periodes_potencia) < 6:
                                        %for p in range(0, 6-len(periodes_potencia)):
                                            <td style="width: 60px;" class="center border">
                                                &nbsp;
                                            </td>
                                        %endfor
                                    %endif
                                </tr>
                            </tbody>
                        </table>
                    %else:
                        <div class="small_text" style="padding: 5px; padding-left: 15px;">
                            ${_("Incluido en el anexo de precios adjuntado con este documento.")}
                        </div>
                    %endif
     
                </div>
     
                <div id="datos_bancarios" class="">
                    <div class="section_title">
                        <h3> ${_("DATOS BANCARIOS")} </h3>
                    </div>

                    <table class="data_table">
                        <colgroup>
                            <col style="width: auto"/>
                            <col style="width: auto"/>
                        </colgroup>
                        <tr>
                            <td> ${_("IBAN:")} </td>
                            <td>
                                <div class="insert_box">
                                    <% iban = polissa.bank and polissa.bank.iban[4:] or '' %>
                                    ${iban}
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
     
                <div id="condiciones" class="">
                    <div class="section_title">
                        <h3> ${_("CONDICIONES")} </h3>
                    </div>
                    <div id="lista_condiciones">
                        <ul>
                            <li>
                                ${_("Vencimiento: 7 días naturales desde la fecha de la factura")}
                            </li>
                            <li>
                                ${_("El CLIENTE autoriza a LUMINA al giro, en el número de cuenta especificado, de los recibos correspodientes a facturas que se originen como consecuencia de la relación de suministro de energía eléctrica y servicios contratados, a tenor de lo exigido por la Ley de Servicios de Pago 16/2009")}
                            </li>
                            <li>
                                ${_("Los precios no incluyen el Impuesto Especial sobre la Electricidad ni el I.V.A. / IGIC")}
                            </li>
                            <li>
                                ${_("Las variaciones de tarifas de acceso o conceptos regulados que forman parte del precio serán repercutidas al CLIENTE.")}
                            </li>
                        </ul>
                    </div>
                </div>

                <div id="conformidad">
                    <div class="section_notice">
                        <p> ${_("En prueba de conformidad con lo anteriormente expuesto, ambas Partes firman las presentes Condiciones Particulares del Contrato de Suministro, junto con sus Condiciones Generales, por duplicado y a un solo efecto:")} </p>
                    </div>
                    <table width="100%">
                        <colgroup>
                            <col style="width: 50%"/>
                            <col style="width: 50%"/>
                        </colgroup>
                        <tr>
                            <td class="center small_text">
                                Por LUMINA:
                            </td>
                            <td class="center small_text">
                                Por el CLIENTE:
                            </td>
                        </tr>
                        <tr>
                            <td class="center">
                                <br/>
                                <img src="${addons_path}/giscedata_polissa_comer_lumina/report/assets/signatura_contracte.png" height="75px"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="small_text">
                                <%
                                    from datetime import datetime
                                    import time, babel
                                    if polissa.data_firma_contracte:
                                        data_firma = datetime.strptime(datetime_to_date(polissa.data_firma_contracte), '%Y-%m-%d')
                                    else:
                                        data_firma =  datetime.now()
                                %>
                                A ${babel.dates.format_datetime(data_firma, "d 'de' LLLL 'de' YYYY", locale='es_ES')}
                            </td>
                        </tr>
                    </table>
                </div>

            </div>

                <div style="clear:both"></div>

            </div>
            <%
            comptador_polisses += 1;
            %>
            % if comptador_polisses<len(objects):
                <p style="page-break-after:always"></p>
            % endif
        %endfor
	</body>
</html>
