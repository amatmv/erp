# -*- coding: utf-8 -*-
import time


from osv import osv, fields
from tools.translate import _


class CrmCaseStage(osv.osv):
    """ Stage of case """

    _name = "crm.case.stage"
    _inherit = "crm.case.stage"
    _description = "Stage of case"
    _rec_name = 'name'
    _order = "sequence"

    _columns = {
        'probability': fields.float(
            'Probability (%)', required=True,
            help="This percentage depicts the default/average probability "
                 "of the Case for this stage to be a success"),
        'on_change': fields.boolean(
            'Change Probability Automatically',
            help="Setting this stage will change the probability "
                 "automatically on the opportunity."),
        'requirements': fields.text('Requirements'),
        'section_ids': fields.many2many(
            'crm.case.section', 'section_stage_rel', 'stage_id',
            'section_id', string='Sections')
    }

    _defaults = {
        'sequence': lambda *args: 1,
        'probability': lambda *args: 0.0,
    }

CrmCaseStage()


class CrmCaseSection(osv.osv):
    _name = 'crm.case.section'
    _inherit = 'crm.case.section'

    _columns = {
        'stage_ids': fields.many2many(
            'crm.case.stage', 'section_stage_rel', 'section_id',
            'stage_id', 'Stages'),
    }

CrmCaseSection()


class crm_case(osv.osv):
    _name = 'crm.case'
    _inherit = 'crm.case'

    _columns = {
        'stage_id': fields.many2one(
            'crm.case.stage', 'Stage',
            domain="[('section_ids', '=', section_id)]"),
    }

    def onchange_stage_id(self, cursor, uid, ids, stage_id, context={}):
        if not stage_id:
            return {'value': {}}
        stage = self.pool.get('crm.case.stage').browse(
            cursor, uid, stage_id, context)
        if not stage.on_change:
            return {'value': {}}
        return {'value': {'probability': stage.probability}}

    def stage_find(self, cr, uid, section_id, domain=[], order='sequence'):
        domain = list(domain)
        if section_id:
            domain.append(('section_ids', '=', section_id))
        stage_ids = self.pool.get('crm.case.stage').search(cr, uid, domain, order=order)
        if stage_ids:
            return stage_ids[0]
        return False

    def stage_set(self, cr, uid, ids, stage_id, context=None):
        value = {}
        if hasattr(self, 'onchange_stage_id'):
            value = self.onchange_stage_id(cr, uid, ids, stage_id)['value']
        value['stage_id'] = stage_id
        return self.write(cr, uid, ids, value, context=context)

    def stage_change(self, cr, uid, ids, op, order, context=None):
        if context is None:
            context = {}
        for case in self.browse(cr, uid, ids, context=context):
            seq = 0
            if case.stage_id:
                seq = case.stage_id.sequence
            section_id = None
            if case.section_id:
                section_id = case.section_id.id
            next_stage_id = self.stage_find(
                cr, uid, section_id, [('sequence', op, seq)], order)
            if next_stage_id:
                return self.stage_set(
                    cr, uid, [case.id], next_stage_id, context=context)
        return False

    def stage_next(self, cr, uid, ids, context=None):
        """This function computes next stage for case from its current stage
        using available stage for that case type
        """
        return self.stage_change(cr, uid, ids, '>','sequence', context)

    def stage_previous(self, cr, uid, ids, context=None):
        """This function computes previous stage for case from its current
        stage using available stage for that case type
        """
        return self.stage_change(cr, uid, ids, '<', 'sequence desc', context)

    def message_append(self, cursor, uid, ids, message,
                       keyword=False, context=None):
        if not context:
            context = {}
        if not keyword:
            keyword = _('Historize')
        if not message:
            return False
        user_obj = self.pool.get('res.users')
        history_obj = self.pool.get('crm.case.history')
        for case in self.browse(cursor, uid, ids):
            email = user_obj.browse(cursor, uid, uid).address_id.email
            data = {
                'name': keyword,
                'som': case.som.id,
                'canal_id': case.canal_id.id,
                'user_id': uid,
                'date': time.strftime('%Y-%m-%d %H:%M:%S'),
                'case_id': case.id,
                'section_id': case.section_id.id,
                'description': message,
                'email': email or False
            }
            history_obj.create(cursor, uid, data, context=context)
        return True

    def write(self, cr, uid, ids, vals, context=None):
        if not context:
            context = {}

        if 'date_closed' in vals:
            return super(crm_case, self).write(
                cr, uid, ids, vals, context=context)

        if vals.get('stage_id'):
            stage = self.pool.get('crm.case.stage').browse(
                cr, uid, vals['stage_id'], context=context)
            # change probability of lead(s) if required by stage
            if not vals.get('probability') and stage.on_change:
                vals['probability'] = stage.probability
            text = _("Changed Stage to: %s") % stage.name
            self.message_append(
                cr, uid, ids, text, keyword=text, context=context)

        return super(crm_case, self).write(cr, uid, ids, vals, context)

crm_case()
