# -*- coding: utf-8 -*-
{
    "name": "CRM Case Stage",
    "description": """Create CRM Case Stage and add to CRM Case. Gives to CRM CASE
                    functions about it""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "CRM",
    "depends":[
        "crm_configuration"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "crm_view.xml"
    ],
    "active": False,
    "installable": True
}
