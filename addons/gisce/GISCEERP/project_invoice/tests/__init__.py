from destral import testing
from destral.transaction import Transaction

class TestTask(testing.OOTestCase):

    def setUp(self):
        self.txn = Transaction().start(self.database)

    def tearDown(self):
        self.txn.stop()

    def test_task_close(self):
        '''
            This test checks that, given a task with a bunch of works done, will
            close it correctly generating an invoice with the same partner id
            and all the works done as invoice lines.
        '''

        #Loading of the cursos and the user id
        cr = self.txn.cursor
        uid = self.txn.user

        #Get the necessary models
        imd_obj = self.openerp.pool.get('ir.model.data')
        task_obj = self.openerp.pool.get('project.task')
        inv_line_obj = self.openerp.pool.get('account.invoice.line')
        inv_object = self.openerp.pool.get('account.invoice')

        task_id = imd_obj.get_object_reference(
            cr, uid, 'project_invoice', 'project_task_01'
        )[1]

        current_task = task_obj.read(cr, uid, [task_id], ['partner_id'])

        # Closing the test task
        task_obj.do_close(cr, uid, [task_id], {})

        # Checking that the invoice has been created properly and has the same
        # partner than the task
        self.assertTrue(
            inv_object.search(cr, uid, [
                ('partner_id', '=', current_task[0]['partner_id'][0])
            ])
        )

        # Finally, checking that all invoice lines have been successfully added
        # We could be checking only the first and the n-th work done using
        # inductive reasoning, but as there are only 4 iterations, we'll check
        # them all 4.
        for name in ['Work 1', 'Work 2', 'Work 3', 'Work 4']:
            self.assertTrue(inv_line_obj.search(cr, uid, [
                ('name', '=', name)
            ]))
