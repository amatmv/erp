# -*- coding: utf-8 -*-
{
    "name": "",
    "description": """This module defines a model that contains a workdone type in order to help you to classify your task works.  
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "project"
    ],
    "init_xml": [],
    "demo_xml":[
        "project_invoice_demo.xml"
    ],
    "update_xml":[
        "project_invoice_view.xml",
        "project_view.xml",
        "wizard/wizard_regularize_time_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
