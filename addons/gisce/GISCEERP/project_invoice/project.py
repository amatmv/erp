# -*- encoding: utf-8 -*-

from osv import osv, fields
from tools.translate import _


class ProjectTaskWork(osv.osv):
    _name = 'project.task.work'
    _inherit = 'project.task.work'
    _columns = {
        'type_id': fields.many2one(
            'project.task.work.type', 'Type', ondelete='restrict')
    }

ProjectTaskWork()


class ProjectTask(osv.osv):
    _name = 'project.task'
    _inherit = 'project.task'

    def get_partnerid(self, cursor, uid, ids, context=None):
        def _get_partner_project(cursor, uid, project_id, project_obj):
            project = project_obj.read(
                cursor, uid, project_id, ['parent_id', 'partner_id'])
            if project['partner_id']:
                return project['partner_id'][0]
            elif project['parent_id']:
                return _get_partner_project(
                    cursor, uid, project['parent_id'][0], project_obj)
            else:
                return False

        if isinstance(ids, list):
            ids = ids[0]
        project_obj = self.pool.get('project.project')
        current_task = self.browse(cursor, uid, ids)
        if current_task.partner_id:
            return current_task.partner_id.id
        elif current_task.project_id:
            return _get_partner_project(
                cursor, uid, current_task.project_id.id, project_obj)
        else:
            return False

    def do_close(self, cursor, uid, ids, *args):
        res = super(ProjectTask, self).do_close(cursor, uid, ids, *args)
        inv_object = self.pool.get('account.invoice')
        for task in self.browse(cursor, uid, ids):
            partner_id = task.get_partnerid()
            if partner_id:
                inv_ids = inv_object.search(cursor, uid, [
                    ('partner_id', '=', task.partner_id.id),
                    ('state', '=', 'draft')
                ])
                if not inv_ids:
                    self.create_invoice_from_task(cursor, uid, ids, task)
            else:
                raise osv.except_osv(_('Error'),
                                     _('Can\'t create the invoice because the '
                                       'task has no partner'))

    def create_invoice_from_task(self, cursor, uid, ids, task):
        inv_object = self.pool.get('account.invoice')
        new_inv = {
            'name': task.name,
            'partner_id': task.get_partnerid(),
        }
        new_inv.update(inv_object.onchange_partner_id(
            cursor, uid, ids, 'in_invoice', task.get_partnerid()
        ))

        new_inv_values = new_inv['value']
        for value in new_inv_values:
            new_inv[value] = new_inv_values[value]

        inv_id = inv_object.create(cursor, uid, new_inv)
        type_obj = self.pool.get('project.task.work.type')
        invoice_ln_obj = self.pool.get('account.invoice.line')
        work_obj = self.pool.get('project.task.work')
        work_ids = [work.id for work in task.work_ids]
        for current_workdone in work_obj.read(
                cursor, uid, work_ids, ['name', 'type_id', 'hours']):

            if current_workdone['type_id']:
                is_billable = type_obj.read(
                    cursor, uid, current_workdone['type_id'][0], ['billable']
                )['billable']
            else:
                is_billable = True

            discount = 0
            if not is_billable:
                discount = 100.0
            invoice_ln_obj.create(cursor, uid, {
                'name': current_workdone['name'],
                'discount': discount,
                'invoice_id': inv_id,
                'account_id': new_inv['account_id'],
                'quantity': current_workdone['hours']
            })

ProjectTask()
