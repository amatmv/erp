# -*- encoding: utf-8 -*-

from osv import fields, osv

class ProjectTaskWorkType(osv.osv):
    _name = 'project.task.work.type'

    #Specification of the name of the module. The ERP will
    #take the attribute "code: as the name.
    _rec_name = 'code'

    def name_get(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        billable = {
            True: 'F',
            False: 'NF'
        }

        res = []
        for wtype in self.read(cursor, uid, ids):
            res.append((
                wtype['id'],
                '{} ({})'.format(wtype['code'], billable[wtype['billable']])
            ))
        return res

    _columns = {
        'code': fields.char('Code', size=32, required=True),
        'description': fields.text('Description'),
        'billable': fields.boolean('Billable'),
    }

ProjectTaskWorkType()
