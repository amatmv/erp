# -*- coding: utf-8 -*-
from tools.translate import _
from osv import osv, fields
from sql.aggregate import Sum


class WizardRegularizeTime(osv.osv_memory):
    _name = "project.task.wizard.regularize.time"
    _inherit = "project.task.wizard.regularize.time"

    def update_with_data(self, cursor, uid, ids, context):
        """
        Wizard 'update' button with effective hours
        :param cursor:  OpenERP Cursor
        :param uid:     User ID
        :param ids:     Wizard ids
        :param context: Wizard context
        :return:
        """
        work_obj = self.pool.get('project.task.work')
        updated = 0
        wiz = self.browse(cursor, uid, ids[0])
        q = work_obj.q(cursor, uid)
        task_ids = context.get('active_ids', [])
        domain = [
            ('task_id', 'in', task_ids)
        ]
        if wiz.only_billable:
            domain += [
                ('type_id.billable', '=', True)
            ]
        sql = q.select([
            Sum('hours'), 'task_id'
        ], group_by=('task_id',)).where(domain)
        cursor.execute(*sql)
        values = cursor.dictfetchall()
        self.update_task(cursor, uid, task_ids=task_ids, hours=0)
        for value in values:
            updated += self.update_task(
                cursor, uid,
                task_ids=[value['task_id']],
                hours=value['sum_hours']
            )
        if wiz.only_billable:
            output = _(
                'Updated {} tasks with billable effective hours as planned hours'
            )
        else:
            output = _(
                    'Updated {} tasks with effective hours as planned hours'
            )
        self.write(cursor, uid, ids, {
            'output': output.format(updated)
        })

    _columns = {
        'only_billable': fields.boolean('Only use billable hours')
    }

    _defaults = {
        'only_billable': lambda *a: 1,
    }


WizardRegularizeTime()
