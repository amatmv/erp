# -*- encoding: utf-8 -*-
from osv import fields, osv


class CRMCase(osv.osv):
    _name = 'crm.case'
    _inherit = 'crm.case'

    def get_subject_mail_from_case(self, cursor, uid, case, context=None):
        if case.section_id.code and case.section_id.code == 'IMP_COMER':
            id_factura = case.ref2 and len(case.ref2.split(',')) == 2 and case.ref2.split(',')[0] == 'giscedata.facturacio.factura' and case.ref2.split(',')[1] or ''

            if id_factura:
                id_factura = int(id_factura)
                fact_obj = self.pool.get('giscedata.facturacio.factura')
                num_factura = fact_obj.read(
                    cursor, uid, id_factura, ['number']
                )['number']
                return 'Impago Factura {}'.format(num_factura)
        else:
            return super(CRMCase, self).get_subject_mail_from_case(
                cursor, uid, case, context=context
            )

CRMCase()
