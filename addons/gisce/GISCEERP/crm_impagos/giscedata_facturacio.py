# -*- encoding: utf-8 -*-
from ast import literal_eval
import logging
from datetime import timedelta, datetime

from osv import osv
import pooler
from workalendar.europe import Spain


def find_CRMImpago(cursor, uid, factura_id, section_id, context=None):
    if context is None:
        context = {}
    pool = pooler.get_pool(cursor.dbname)
    case_obj = pool.get('crm.case')
    factura_obj = pool.get('giscedata.facturacio.factura')
    factura = factura_obj.browse(cursor, uid, factura_id, context=context)
    if factura.group_move_id:
        ref_factura = []
        invoice_ids = [
            l.invoice.id for l in factura.group_move_id.line_id if l.invoice
        ]
        factura_ids = factura_obj.search(cursor, uid, [
            ('invoice_id.id', 'in', invoice_ids)
        ])
        for factura_id in factura_ids:
            ref = 'giscedata.facturacio.factura,{0}'.format(factura_id)
            ref_factura.append(ref)
    else:
        ref_factura = ['giscedata.facturacio.factura,{0}'.format(factura_id)]

    if ref_factura:
        search_params = [
            ('state', 'in', ('open', 'draft')),
            ('ref2', 'in', ref_factura),
            ('section_id', '=', section_id)
        ]
        return case_obj.search(cursor, uid, search_params)
    return []


class GiscedataFacturacioFactura(osv.osv):

    _name = 'giscedata.facturacio.factura'
    _inherit = 'giscedata.facturacio.factura'

    def get_plantilla_text(self, cursor, uid, factura_id, context=None):
        return ''

    def check_if_gen_non_payment_case(self, cursor, uid, factura, context=None):
        return factura.pending_state.weight > 0

    def set_pending_post_hook(self, cursor, uid, ids, pending_id, context=None):
        if context is None:
            context = {}
        logger = logging.getLogger(
            'openerp.{0}.set_pending_post_hook'.format(__name__)
        )
        crm_obj = self.pool.get('crm.case')
        section_obj = self.pool.get('crm.case.section')
        dev_linia_obj = self.pool.get('giscedata.facturacio.devolucio.linia')
        cfg_obj = self.pool.get('res.config')
        dies_accio = int(cfg_obj.get(cursor, uid, 'atr_b1_data_accio', '14'))
        dies_no_permesos = literal_eval(
            cfg_obj.get(cursor, uid, 'atr_b1_dies_no_permesos', '[4, 5, 6]')
        )
        dies_permesos = [0, 1, 2, 3]
        # validem que sempre hi hagi com a mínim un dia permes
        assert len(list(set(dies_permesos)-set(dies_no_permesos))) > 0
        dies_permesos = list(set(dies_permesos)-set(dies_no_permesos))
        calendar = Spain()

        section_id = section_obj.search(cursor, uid, [
            ('code', '=', 'IMP_COMER')
        ])[0]

        for factura in self.browse(cursor, uid, ids, context=context):
            crm_cases_ids = find_CRMImpago(
                cursor, uid, factura.id, section_id=section_id, context=context
            )
            polissa = factura.polissa_id
            if self.check_if_gen_non_payment_case(
                    cursor, uid, factura, context=context
            ):
                motiu = ''
                if factura.devolucio_id:
                    search_params = [
                        ('devolucio_id', '=', factura.devolucio_id.id),
                        ('numfactura', '=', factura.number)
                    ]
                    linia_id = dev_linia_obj.search(cursor, uid, search_params)
                    if linia_id:
                        motiu = dev_linia_obj.read(
                            cursor, uid, linia_id[0], ['reject_msg'])
                        motiu = motiu['reject_msg']
                if factura.group_move_id:
                    desc = 'Factura agrupada'
                    signal = {
                        'in_invoice': 1,
                        'out_invoice': 1,
                        'in_refund': -1,
                        'out_refund': -1
                    }
                    amount = sum(
                        l.invoice.amount_total * signal[l.invoice.type]
                        for l in factura.group_move_id.line_id if l.invoice
                    )
                    number = factura.group_move_id.ref
                    invoice_date = ''
                else:
                    desc = 'Contrato: {0} Tarifa: {1} Factura'.format(
                        polissa.name, factura.polissa_id.tarifa.name)
                    amount = factura.amount_total
                    number = factura.number
                    invoice_date = 'Fecha factura: {0} '.format(
                        datetime.strptime(
                            factura.date_invoice,
                            '%Y-%m-%d').strftime('%d/%m/%Y')
                    )
                if crm_cases_ids:
                    logger.info('CRM Impago found for invoices {0}.'.format(
                        number
                    ))
                    continue
                logger.info(
                    u'Invoice {number} from contract {contract} is in the {state} '
                    u'state for process {process}. Generating a CRM Impago.'.format(
                        number=factura.number, contract=polissa.name,
                        state=factura.pending_state.name,
                        process=factura.pending_state.process_id.name
                    )
                )
                user_id = polissa.user_id and polissa.user_id.id or uid

                # Calcular data final
                data_actual = datetime.now()
                data_accio = (data_actual + timedelta(days=dies_accio))

                while data_accio.weekday() not in dies_permesos:
                    data_accio = calendar.add_working_days(
                        data_accio, 1)

                plantilla = self.get_plantilla_text(cursor, uid, factura.id,
                                                    context=context
                                                    )

                id_cas_creat = crm_obj.create(cursor, uid, {
                    'name': 'Impago {0} {1}: {2} {3}€'.format(
                        desc, number, invoice_date, amount
                    ),
                    'user_id': user_id,
                    'email_from': polissa.pagador_email,
                    'planned_cost': amount,
                    'section_id': section_id,
                    'partner_id': factura.partner_id.id,
                    'partner_address_id': factura.address_invoice_id.id,
                    'date_deadline': (
                        data_accio
                    ).strftime('%Y-%m-%d'),
                    'ref': 'giscedata.polissa,{0}'.format(polissa.id),
                    'ref2':
                        'giscedata.facturacio.factura,{0}'.format(factura.id),
                    'description': motiu,
                    'polissa_id': polissa.id
                })
                crm_obj.case_log(cursor, uid, [id_cas_creat], context=context)
                crm_obj.write(cursor, uid, [id_cas_creat],
                               {'description': plantilla},
                               context=context)
            elif factura.pending_state.weight == 0:
                logger.info(
                    'Invoice {0} from contract {1} is in first state. '
                    'Closing opened CRM Impago.'.format(
                        factura.number, polissa.name
                    )
                )
                if crm_cases_ids:
                    crm_obj.case_close(cursor, uid, crm_cases_ids)
                    logger.info('CRM Impago closed for invoices {0}.'.format(
                        factura.number
                    ))

        return super(GiscedataFacturacioFactura, self).set_pending_post_hook(
            cursor, uid, ids, pending_id, context=context
        )

GiscedataFacturacioFactura()
