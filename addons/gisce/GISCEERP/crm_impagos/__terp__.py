# -*- coding: utf-8 -*-
{
    "name": "CRM Impagos",
    "description": """Customitzations for CRM""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "CRM",
    "depends":[
        "base",
        "crm",
        "giscedata_polissa_crm",
        "giscedata_polissa_responsable",
        "giscedata_facturacio_impagat"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
