# -*- coding: utf-8 -*-
{
    "name": "Extension for crm module (Multicompany)",
    "description": """Extension for crm module""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "CRM",
    "depends":[
        "crm_extended",
        "crm_multicompany"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "crm_extended_multicompany_report.xml"
    ],
    "active": False,
    "installable": True
}
