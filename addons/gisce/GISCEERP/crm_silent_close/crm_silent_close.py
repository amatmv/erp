# -*- coding: utf-8 -*-

from osv import osv

class crm_case(osv.osv):

    _name = "crm.case"
    _inherit = "crm.case"

    def case_silent_close(self, cr, uid, ids, *args):
        """Funció case_close per tal de treure tots els correus
        electrònics i així no s'enviarà res."""

        cases = self.browse(cr, uid, ids)
        res = []
        for case in cases:
            old_vals = {
              'email_cc': case.email_cc,
              'email_from': case.email_from
            }
            self.write(cr, uid, [case.id], {'email_cc': False, 'email_from': False})
            res.append(self.case_close(cr, uid, [case.id]))
            self.write(cr, uid, [case.id], old_vals)

        # Retornem True si totes les subcrides han anat bé
        # o False si alguna no ha anat bé
        return reduce(lambda x,y: x and y, res)

crm_case()
