# -*- coding: utf-8 -*-
{
    "name": "CRM Silent Close",
    "description": """Permet tancar casos sense enviar cap email""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "CRM",
    "depends":[
        "crm"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "crm_silent_close_view.xml"
    ],
    "active": False,
    "installable": True
}
