# -*- coding: utf-8 -*-
from datetime import datetime
from slugify import slugify

from osv import osv, fields

DATE_MASK = '%Y-%m-%d %H:%M:%S'


def get_input_time(minutes):
    minimum = 15
    hours = 0
    calc_minutes = 0
    if minutes:
        hours, rest_minutes = divmod(minutes, 60)
        if not hours and rest_minutes < minimum:
            calc_minutes = minimum
        else:
            part, extra = divmod(rest_minutes, minimum)

            if extra >= (minimum / 2):
                calc_minutes = (part + 1) * minimum
            else:
                calc_minutes = part * minimum
    return hours + (calc_minutes/60.0)

STATES_TASK_TRACK = [
    ('doing', 'doing'),
    ('review', 'Review'),
    ('done', 'Done')
 ]

class TaskTrack(osv.osv):

    _name = 'task.track'

    def _fnc_get_spent(self, cursor, uid, ids, field_name, arg, context=None):
        if context is None:
            context = {}
        res = dict.fromkeys(ids, {'spent_time': 0, 'spent_time_visual': 0})

        for track in self.read(cursor, uid, ids, ['start_time', 'end_time']):
            if track['end_time']:
                end_time = datetime.strptime(track['end_time'], DATE_MASK)
                start_time = datetime.strptime(track['start_time'], DATE_MASK)
                minutes, seconds = divmod((end_time-start_time).total_seconds(), 60)
                res[track['id']]['spent_time'] = int(minutes)
                res[track['id']]['spent_time_visual'] = round((end_time-start_time).total_seconds() / (3600.0), 2)

        return res

    def finish_task_track(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        end_time = datetime.now().strftime(DATE_MASK)
        self.write(cursor, uid, ids, {'end_time': end_time, 'state': 'review'})

    def resume_task_track(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        if isinstance(ids, (list, tuple)):
            ids = ids[0]
        track_vals = self.read(cursor, uid, ids, ['name', 'task_id', 'type_id'])
        # clean values
        track_vals.pop('id')
        track_vals['task_id'] = track_vals['task_id'][0]
        track_vals['type_id'] = track_vals['type_id'][0]
        self.create(cursor, uid, track_vals)

    def review_task_track(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        grouped_tasks = {}
        group_by_date = {}
        for track in self.read(cursor, uid, ids, []):
            if track['state'] == 'review':
                task_id = track['task_id'][0]
                user_id = track['user_id'][0]
                type_id = track['type_id'][0]
                task_desc = track['name']
                task_slug = slugify(unicode(task_desc))
                task_time = track['spent_time']
                date_key = track['start_time'][:10]
                task_key = '{}#{}#{}#{}'.format(
                    task_slug, date_key, task_id, type_id
                )
                grouped_tasks.setdefault(task_key, {
                    'tracks': [],
                    'time': 0,
                    'desc': task_desc,
                    'date': date_key,
                    'task_id': task_id,
                    'user_id': user_id,
                    'type_id': type_id
                })
                group_by_date.setdefault(date_key, 0)
                group_by_date[date_key] += task_time

                grouped_tasks[task_key]['tracks'].append(track['id'])
                grouped_tasks[task_key]['time'] += task_time
        return grouped_tasks, group_by_date

    def create_workdones(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        grouped_tasks, group_by_date = self.review_task_track(
            cursor, uid, ids, context=context
        )
        workdone_obj = self.pool.get('project.task.work')
        for track_key, track in grouped_tasks.items():
            workdone_obj.create(
                cursor, uid,
                {
                    'name': track['desc'],
                    'date': track['date'],
                    'hours': get_input_time(track['time']),
                    'user_id': track['user_id'],
                    'task_id': track['task_id'],
                    'type_id': track['type_id'],
                }
            )
            self.write(cursor, uid, track['tracks'], {'state': 'done'})
        return True

    _columns = {
        'name': fields.char('Description task', size=140, required=True, states={'done': [('readonly', True)]}),
        'task_id': fields.many2one('project.task', 'Task', required=True, states={'done': [('readonly', True)]}),
        'user_id': fields.many2one('res.users', 'User', required=True, states={'done': [('readonly', True)]}),
        'state': fields.selection(STATES_TASK_TRACK, 'State'),
        'start_time': fields.datetime('Start time', required=True, states={'done': [('readonly', True)]}),
        'end_time': fields.datetime(
            'End time', required=False, states={'done': [('readonly', True)]}
        ),
        'spent_time': fields.function(
            _fnc_get_spent, multi="time", string='Spent time', type='integer', store={
            'task.track':
                (lambda self, cr, uid, ids, c={}: ids,
                 ['start_time', 'end_time'], 20)
        }, method=True),
        'spent_time_visual': fields.function(
            _fnc_get_spent, multi="time", string='Spent time', type='float',  store={
                'task.track':
                    (lambda self, cr, uid, ids, c={}: ids,
                     ['start_time', 'end_time'], 20)
            }, method=True),
        'type_id': fields.many2one(
            'project.task.work.type', 'Task type', required=True, states={'done': [('readonly', True)]}
        )
    }

    _defaults = {
        'state': lambda *a: 'doing',
        'user_id': lambda self, cr, uid, c: uid,
        'start_time': lambda *a: (datetime.now()).strftime(DATE_MASK)
    }


TaskTrack()



