from osv import osv, fields
from project_timetracking.task_tracking import get_input_time



class WizardValidateTaskTrack(osv.osv_memory):
    _name = 'wizard.validate.task.track'


    _columns = {
        'information': fields.text(
            'Information'
        ),
        'state': fields.selection(
            [('init', 'Init'), ('validated', 'Validated'), ('end', 'End')], string='State')
    }

    def validate_tracks(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        track_ids = context.get('active_ids', [])
        track_obj = self.pool.get('task.track')

        grouped_tasks, group_by_date = track_obj.review_task_track(
            cursor, uid, track_ids, context=context
        )
        message = '{:#^30}\n'.format("Information by day")
        for date_track, value in group_by_date.items():
            timing = round(value / 60.0, 2)
            message += '- Day {} - total time {}\n'.format(date_track, timing)

        message += '\n\n{:#^30}\n'.format("Information by day")
        for track, value in grouped_tasks.items():
            timing = round(value['time']/60.0, 2)
            message += ('\t- {} ({})'
                        '\n\t\t--> total time {}'
                        '\n\t\t-->  input time {}\n').format(
                value['desc'], value['date'], timing, get_input_time(value['time'])
            )

        self.write(cursor, uid, ids, {
            'information': message,
            'state': 'validated'

        })

    def accept_tracks(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        track_ids = context.get('active_ids', [])
        track_obj = self.pool.get('task.track')
        message = ''
        if track_obj.create_workdones(cursor, uid, track_ids, context=context):
            message = 'Create workdones'
        else:
            message = 'Error create workdones'
        self.write(cursor, uid, ids, {
                'information': message,
                'state': 'end'

        })
        return True

WizardValidateTaskTrack()