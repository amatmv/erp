# -*- coding: utf-8 -*-
{
    "name": "CRM Task",
    "description": """Create task tracking for projects""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "CRM",
    "depends":[
        "board",
        "project",
        "project_invoice"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/validate_task_track.xml",
        "task_tracking_view.xml",
        "task_tracking_board.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
