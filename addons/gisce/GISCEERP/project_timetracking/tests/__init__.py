from destral import testing
from destral.transaction import Transaction

from project_timetracking.task_tracking import get_input_time

class TestTaskTracking(testing.OOTestCase):

    def setUp(self):
        self.txn = Transaction().start(self.database)

    def tearDown(self):
        self.txn.stop()

    def test_get_time_input(self):
        '''
            This test checks that, given an input time in minutes
            returns input time fractional 15 minutes
        '''

        spent_time = 0
        self.assertEquals(spent_time, get_input_time(spent_time))
        # 15m
        for x in range(1, 22):
            self.assertEquals(0.25, get_input_time(x))
        # 30m
        for x in range(22, 37):
            self.assertEquals(0.5, get_input_time(x))
        # 45m
        for x in range(37, 52):
            self.assertEquals(0.75, get_input_time(x))
        # 1h
        for x in range(52, 67):
            self.assertEquals(1.0, get_input_time(x))
        # 1h 15m
        for x in range(67, 82):
            self.assertEquals(1.25, get_input_time(x))
        # 1h 30m
        for x in range(82, 97):
            self.assertEquals(1.5, get_input_time(x))
        # 1h 45m
        for x in range(97, 112):
            self.assertEquals(1.75, get_input_time(x))
        # 2h
        for x in range(112, 127):
            self.assertEquals(2, get_input_time(x))
        # 2h 15m
        for x in range(127, 142):
            self.assertEquals(2.25, get_input_time(x))
        # 2h 30m
        for x in range(142, 157):
            self.assertEquals(2.5, get_input_time(x))
        # 2h 45m
        for x in range(157, 172):
            self.assertEquals(2.75, get_input_time(x))
        # 3h
        for x in range(172, 187):
            self.assertEquals(3, get_input_time(x))
        # Testing longest times
        x = 360
        self.assertEquals(6, get_input_time(x))
        x = 483
        self.assertEquals(8.0, get_input_time(x))
        x = 3383
        self.assertEquals(56.5, get_input_time(x))
        x = 1024
        self.assertEquals(17.0, get_input_time(x))
        x = 2048
        self.assertEquals(34.25, get_input_time(x))