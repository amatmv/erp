# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from datetime import datetime

from destral import testing
from destral.transaction import Transaction


class TestCrmTask(testing.OOTestCase):
    def setUp(self):
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user
        self.pool = self.openerp.pool

    def tearDown(self):
        self.txn.stop()
        self.cursor = False
        self.uid = False

    def test_open_case_task(self):
        case_obj = self.pool.get('crm.case')
        section_obj = self.pool.get('crm.case.section')
        task_obj = self.pool.get('project.task')
        project_obj = self.pool.get('project.project')

        case_id = case_obj.search(self.cursor, self.uid, [
            # Search for any case that we can use to test
            ('partner_id', '!=', 1),    # Partner != Our company
            ('state', '=', 'draft'),    # On state DRAFT
        ])

        if not case_id:
            # Get any section for the new case
            section_id = section_obj.search(self.cursor, self.uid, [])
            if section_id:
                section_id = section_id[0]
            else:
                # Or create one
                section_id = section_obj.create(
                    self.cursor, self.uid, {'name': 'CrmTask Test Section'}
                )
            # If there isn't any case we can use from demo data, we create one
            case_id = case_obj.create(self.cursor, self.uid, {
                'partner_id': 2,
                'user_id': 1,
                'section_id': section_id,
                'name': 'Test Open Case',
                'description': 'Test Open case for CRM Task',
            })
        else:
            case_id = case_id[0]

        case = case_obj.browse(self.cursor, self.uid, case_id)

        # Check not existing project/task before opening
        parent_project = project_obj.search(
            self.cursor, self.uid, [
                ('parent_id', '=', False),
                ('partner_id', '=', case.partner_id.id),
            ]
        )
        self.assertFalse(parent_project,
                         msg='There should not exist a parent project')
        project_name = 'CASOS {} - {}'.format(
            datetime.now().strftime('%m/%Y'), case.partner_id.name
        )
        project_id = project_obj.search(
            self.cursor, self.uid, [('name', '=', project_name)]
        )
        self.assertFalse(project_id,
                         msg='Case project should not exists')
        search_params = [
            ('sequence', '=', case.id),
            ('user_id', '=', case.user_id.id),
            ('partner_id', '=', case.partner_id.id),
        ]
        task_id = task_obj.search(self.cursor, self.uid, search_params)
        self.assertFalse(task_id,
                         msg='No tasks should be assigned to this case')

        case.case_open()

        # Check existing project/task after opening
        parent_project = project_obj.search(
            self.cursor, self.uid, [
                ('parent_id', '=', False),
                ('partner_id', '=', case.partner_id.id),
            ]
        )
        self.assertTrue(parent_project, msg='Parent project should exist')
        project_id = project_obj.search(
            self.cursor, self.uid, [('name', '=', project_name)]
        )
        self.assertTrue(project_id,
                        msg='Case project should exist')
        search_params = [
            ('sequence', '=', case.id),
            ('user_id', '=', case.user_id.id),
            ('partner_id', '=', case.partner_id.id),
        ]
        task_id = task_obj.search(self.cursor, self.uid, search_params)
        self.assertTrue(task_id,
                        msg='Task should have been created')
        self.assertTrue(case.read(['task_id'])[0]['task_id'],
                        msg='Case should have a task assigned')
        self.assertEqual(case.read(['task_id'])[0]['task_id'][0], task_id[0],
                         msg='Case should have the task created assigned')
