# -*- coding: utf-8 -*-
{
    "name": "CRM Task",
    "description": """Creació d'una tasca automàticament al obrir un cas""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "CRM",
    "depends":[
        "crm",
        "project_invoice",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "crm_task_view.xml"
    ],
    "active": False,
    "installable": True
}
