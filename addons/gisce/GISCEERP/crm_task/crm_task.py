# -*- coding: utf-8 -*-
from datetime import datetime
from calendar import monthrange
from tools.translate import _
from osv import fields, osv
from ooquery.operators import LeftJoin


class crm_case(osv.osv):

    _name = "crm.case"
    _inherit = "crm.case"

    def _ff_workdones_ids(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        res = dict((x, []) for x in ids)
        q = self.q(cursor, uid)
        sql = q.select(['id', LeftJoin('task_id.work_ids.id')]).where([
            ('id', 'in', ids)
        ])
        cursor.execute(*sql)
        cases = cursor.dictfetchall()
        for case in cases:
            if case['task_id.work_ids.id']:
                res[case['id']].append(case['task_id.work_ids.id'])
        return res

    def _ff_workdones_ids_inv(self, cursor, uid, ids, name, value, args, context=None):
        if not context:
            context = {}

        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        for case in self.browse(cursor, uid, ids, context):
            if case.task_id:
                case.task_id.write({'work_ids': value})

        return True

    _columns = {
        'create_task': fields.boolean('Create a task'),
        'task_id': fields.many2one('project.task', string='Case Task'),
        'workdones_ids': fields.function(
            _ff_workdones_ids, fnct_inv=_ff_workdones_ids_inv,
            type='one2many', relation='project.task.work',
            method=True
        )
    }

    _defaults = {
      'create_task': lambda *a: 1,
    }

    def get_case_project(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        if isinstance(ids, (list, tuple)):
            ids = ids[0]
        project_obj = self.pool.get('project.project')
        case = self.browse(cursor, uid, ids, context=context)

        parent_project = project_obj.search(
            cursor, uid, [
                ('parent_id', '=', False),
                ('partner_id', '=', case.partner_id.id)
            ]
        )
        if parent_project:
            parent_project = parent_project[0]
        else:
            parent_project = project_obj.create(
                cursor, uid, {
                    'partner_id': case.partner_id.id,
                    'name': case.partner_id.name,
                    'contact_id': case.partner_id.address[0].id,
                    'manager': uid,
                }
            )
        project_name = 'CASOS {} - {}'.format(
            datetime.now().strftime('%m/%Y'), case.partner_id.name
        )
        project_id = project_obj.search(
            cursor, uid, [
                ('name', 'ilike', project_name)
            ]
        )
        if project_id:
            project_id = project_id[0]
        else:
            date = datetime.now().strftime('%Y-%m').split('-')
            project_id = project_obj.create(cursor, uid, {
                'name': project_name,
                'parent_id': parent_project,
                'partner_id': case.partner_id.id,
                'contact_id': case.partner_id.address[0].id,
                'manager': uid,
                'date_start': datetime.now().strftime('%Y-%m-%d'),
                'date_end': '{}-{}-{}'.format(
                    date[0], date[1], monthrange(int(date[0]), int(date[1]))[1]
                )
            })
        return project_id or False

    def get_case_task(self, cursor, uid, ids, project_id, context=None):
        if context is None:
            context = {}
        if isinstance(ids, (list, tuple)):
            ids = ids[0]
        task_obj = self.pool.get('project.task')
        case = self.browse(cursor, uid, ids, context=context)
        if case.task_id:
            task_id = case.task_id.id
        else:
            task_id = task_obj.create(cursor, uid, {
                'sequence': case.id,
                'user_id': case.user_id.id,
                'project_id': project_id,
                'partner_id': case.partner_id.id,
                'name': "[{}] {}".format(case.id, case.name),
                'date_start': case.create_date,
                'date_deadline': case.date_deadline,
                'planned_hours': case.planned_cost,
                'priority': str(int(case.priority) - 1),
            }, context=context)
        return task_id

    def case_open(self, cr, uid, ids, *args):
        task_obj = self.pool.get('project.task')
        res = super(crm_case, self).case_open(cr, uid, ids, *args)
        for case_id in ids:
            case_data = self.read(
                cr, uid, case_id, [
                    'create_task', 'task_id'
                ], context=(args[-1] if args else {})
            )
            # Si s'ha activat crear tasca per aquell cas:
            if case_data['create_task']:
                if not case_data['task_id']:
                    # Obtenim el projecte de casos actual
                    project_id = self.get_case_project(
                        cursor=cr, uid=uid, ids=[case_id],
                        context=(args[-1] if args else {})
                    )
                    # Obtenim la tasca per l'usuari actual en el projecte anterior
                    task_id = self.get_case_task(
                        cursor=cr, uid=uid, ids=[case_id], project_id=project_id,
                        context=(args[-1] if args else {})
                    )
                    # Assignem la tasca al cas
                    self.write(cr, uid, [case_id], {'task_id': task_id})
                else:
                    task_id = case_data['task_id'][0]
            task_obj.do_open(cr, uid, [task_id])
        return res

    def case_close(self, cr, uid, ids, *args):
        res = super(crm_case, self).case_close(cr, uid, ids, *args)
        for case in self.browse(cr, uid, ids):
            if case.task_id:
                if not case.task_id.total_hours:
                    raise osv.except_osv(
                        _('Error'),
                        _("You can't close without entering task hours")
                    )
                case.task_id.do_close()
        return res

    def case_cancel(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        task_obj = self.pool.get('project.task')
        res = super(crm_case, self).case_cancel(cursor, uid, ids, context)
        task_ids = [
            x.task_id.id for x in self.browse(cursor, uid, ids) if x.task_id
        ]
        task_obj.do_cancel(cursor, uid, task_ids, context)
        return res

    def case_pending(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        task_obj = self.pool.get('project.task')
        res = super(crm_case, self).case_pending(cursor, uid, ids, context)
        task_ids = [
            x.task_id.id for x in self.browse(cursor, uid, ids) if x.task_id
        ]
        task_obj.do_pending(cursor, uid, task_ids, context)
        return res

    def case_reset(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        task_obj = self.pool.get('project.task')
        res = super(crm_case, self).case_reset(cursor, uid, ids, context)
        task_ids = [
            x.task_id.id for x in self.browse(cursor, uid, ids) if x.task_id
        ]
        task_obj.do_draft(cursor, uid, task_ids, context)
        return res

crm_case()
