# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* crm_multi_close
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.14\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2019-08-12 13:53\n"
"PO-Revision-Date: 2019-08-12 13:53\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: crm_multi_close
#: field:wizard.crm.multi.change,add_comment:0
#: field:wizard.crm.multi.close,add_comment:0
msgid "Comment type"
msgstr ""

#. module: crm_multi_close
#: model:ir.model,name:crm_multi_close.model_wizard_crm_multi_change
msgid "wizard.crm.multi.change"
msgstr ""

#. module: crm_multi_close
#: selection:wizard.crm.multi.change,state:0
#: selection:wizard.crm.multi.close,state:0
msgid "End"
msgstr ""

#. module: crm_multi_close
#: selection:wizard.crm.multi.change,add_comment:0
#: selection:wizard.crm.multi.close,add_comment:0
msgid "Historize"
msgstr ""

#. module: crm_multi_close
#: field:wizard.crm.multi.change,num_cases:0
#: field:wizard.crm.multi.close,num_cases:0
msgid "Number of cases"
msgstr ""

#. module: crm_multi_close
#: selection:wizard.crm.multi.change,state:0
#: selection:wizard.crm.multi.close,state:0
msgid "Init"
msgstr ""

#. module: crm_multi_close
#: selection:wizard.crm.multi.change,new_state:0
#: selection:wizard.crm.multi.close,new_state:0
msgid "Draft"
msgstr ""

#. module: crm_multi_close
#: constraint:ir.actions.act_window:0
msgid "Invalid model name in the action definition."
msgstr ""

#. module: crm_multi_close
#: model:ir.module.module,description:crm_multi_close.module_meta_information
msgid "Permet tancar casos en massa"
msgstr ""

#. module: crm_multi_close
#: selection:wizard.crm.multi.change,new_state:0
#: selection:wizard.crm.multi.close,new_state:0
msgid "Pending"
msgstr ""

#. module: crm_multi_close
#: model:ir.actions.act_window,name:crm_multi_close.action_wizard_crm_multi_close
#: model:ir.module.module,shortdesc:crm_multi_close.module_meta_information
#: view:wizard.crm.multi.close:0
msgid "CRM Multi Close"
msgstr ""

#. module: crm_multi_close
#: constraint:ir.model:0
msgid "The Object name must start with x_ and not contain any special character !"
msgstr ""

#. module: crm_multi_close
#: view:wizard.crm.multi.change:0
msgid "Finalitzar"
msgstr ""

#. module: crm_multi_close
#: code:addons/crm_multi_close/wizard/wizard_crm_multi_change.py:31
#, python-format
msgid "{} cases in \"{}\" state\n"
""
msgstr ""

#. module: crm_multi_close
#: view:wizard.crm.multi.change:0
msgid "Updates"
msgstr ""

#. module: crm_multi_close
#: field:wizard.crm.multi.change,no_action:0
#: field:wizard.crm.multi.close,no_action:0
msgid "No trigger an action"
msgstr ""

#. module: crm_multi_close
#: code:addons/crm_multi_close/wizard/wizard_crm_multi_change.py:85
#, python-format
msgid "Case {} could not be updated to state {}: {}"
msgstr ""

#. module: crm_multi_close
#: constraint:ir.ui.view:0
msgid "Invalid XML for View Architecture!"
msgstr ""

#. module: crm_multi_close
#: field:wizard.crm.multi.change,state:0
#: field:wizard.crm.multi.close,state:0
msgid "State of the wizard process"
msgstr ""

#. module: crm_multi_close
#: field:wizard.crm.multi.change,new_state:0
#: field:wizard.crm.multi.close,new_state:0
msgid "New State"
msgstr ""

#. module: crm_multi_close
#: selection:wizard.crm.multi.change,add_comment:0
#: selection:wizard.crm.multi.close,add_comment:0
msgid "Send partner & historize"
msgstr ""

#. module: crm_multi_close
#: view:wizard.crm.multi.change:0
#: selection:wizard.crm.multi.change,new_state:0
#: view:wizard.crm.multi.close:0
#: selection:wizard.crm.multi.close,new_state:0
msgid "Cancel"
msgstr ""

#. module: crm_multi_close
#: code:addons/crm_multi_close/wizard/wizard_crm_multi_change.py:94
#, python-format
msgid "Updated {} cases to state \"{}\""
msgstr ""

#. module: crm_multi_close
#: view:wizard.crm.multi.close:0
msgid "Apply"
msgstr ""

#. module: crm_multi_close
#: model:ir.model,name:crm_multi_close.model_wizard_crm_multi_close
msgid "wizard.crm.multi.close"
msgstr ""

#. module: crm_multi_close
#: view:wizard.crm.multi.change:0
#: field:wizard.crm.multi.change,comment:0
#: field:wizard.crm.multi.close,comment:0
msgid "Comment"
msgstr ""

#. module: crm_multi_close
#: help:wizard.crm.multi.change,no_action:0
#: help:wizard.crm.multi.close,no_action:0
msgid "Change the state without trigger an action"
msgstr ""

#. module: crm_multi_close
#: field:wizard.crm.multi.change,result:0
#: field:wizard.crm.multi.close,result:0
msgid "Output Text"
msgstr ""

#. module: crm_multi_close
#: model:ir.actions.act_window,name:crm_multi_close.action_wizard_crm_multi_change
#: view:wizard.crm.multi.change:0
msgid "CRM Multi Change"
msgstr ""

#. module: crm_multi_close
#: selection:wizard.crm.multi.change,add_comment:0
#: selection:wizard.crm.multi.close,add_comment:0
msgid "No comment"
msgstr ""

#. module: crm_multi_close
#: selection:wizard.crm.multi.change,new_state:0
#: selection:wizard.crm.multi.close,new_state:0
msgid "Close"
msgstr ""

#. module: crm_multi_close
#: code:addons/crm_multi_close/wizard/wizard_crm_multi_change.py:27
#, python-format
msgid "No cases selected ..."
msgstr ""

#. module: crm_multi_close
#: view:wizard.crm.multi.change:0
msgid "Realitzar"
msgstr ""

#. module: crm_multi_close
#: selection:wizard.crm.multi.change,new_state:0
#: selection:wizard.crm.multi.close,new_state:0
msgid "Open"
msgstr ""

