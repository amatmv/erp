# -*- coding: utf-8 -*-
{
    "name": "CRM Multi Close",
    "description": """Permet tancar casos en massa""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "CRM",
    "depends":[
        "crm"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/wizard_crm_multi_change_view.xml",
        "wizard/wizard_crm_multi_close_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
