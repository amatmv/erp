# -*- coding: utf-8 -*-

from osv import osv


class WizardCrmMultiClose(osv.osv_memory):
    _name = 'wizard.crm.multi.close'
    _inherit = 'wizard.crm.multi.change'

    def perform_close(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({
            'new_state': 'done'
        })

        self.perform_change(cursor, uid, ids, ctx)

        return {}

WizardCrmMultiClose()
