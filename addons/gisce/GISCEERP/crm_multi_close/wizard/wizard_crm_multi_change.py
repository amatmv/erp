# -*- coding: utf-8 -*-
from functools import partial
from tools.translate import _
from osv import osv, fields
from crm.crm import AVAILABLE_STATES


class WizardCrmMultiChange(osv.osv_memory):
    _name = 'wizard.crm.multi.change'

    def _default_num_cases(self, cursor, uid, context=None):
        if not context:
            context = {}
        return len(context.get('active_ids', []))

    def _default_output_text(self, cursor, uid, context=None):
        if not context:
            context = {}
        case_model = self.pool.get('crm.case')
        states = {}
        for case_id in context.get('active_ids', []):
            case = case_model.browse(cursor, uid, case_id)
            if case.state in states:
                states[case.state] += 1
            else:
                states[case.state] = 1
        output = _('No cases selected ...')
        if states:
            output = ''
            for state in states:
                output += _('{} cases in "{}" state\n').format(
                    states[state], _(state)
                )
        return output

    @staticmethod
    def _get_case_state_method(state):
        if not state:
            return False
        elif state in [st for st, out in AVAILABLE_STATES]:
            if state == 'draft':
                return 'case_reset'
            elif state == 'done':
                return 'case_close'
            else:
                return 'case_{}'.format(
                    state
                )

    def perform_change(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        num_cases = len(context.get('active_ids', []))
        wiz = self.browse(cursor, uid, ids[0], context=None)
        new_state = context.get('new_state', wiz.new_state)

        method = self._get_case_state_method(new_state)

        updated = num_cases

        output = ''

        # Update all cases

        for case_id in context.get('active_ids', []):
            api = osv.osv_pool()
            api_execute = partial(api.execute, cursor.dbname, uid, 'crm.case')
            try:
                if wiz.add_comment and wiz.comment:
                    case = api_execute('read', case_id, ['description'])
                    if case['description']:
                        api_execute('case_log', [case_id])
                    api_execute('write', [case_id], {
                        'description': wiz.comment
                    })
                    api_execute(wiz.add_comment, [case_id], context=context)
                if wiz.no_action:
                    api_execute('write', [case_id], {
                        'state': new_state
                    }, context=context)
                else:
                    api_execute(method, [case_id])
            except AttributeError as e:
                updated -= 1
                output += _(
                    'Case {} could not be updated to state {}: {}'
                ).format(
                    case_id, new_state, e.message
                )

        # Replace output string with info

        if updated == num_cases:
            output = _('Updated {} cases to state "{}"').format(
                num_cases or '0',
                _(new_state)
            )

        self.write(cursor, uid, ids, {
            'result': output,
            'state': 'end'
        })

    _columns = {
        'num_cases': fields.integer(
            string='Number of cases', readonly=True
        ),
        'state': fields.selection(
            selection=[('init', 'Init'), ('end', 'End')],
            string='State of the wizard process'
        ),
        'new_state': fields.selection(
            selection=AVAILABLE_STATES,
            string='New State', required=True
        ),
        'add_comment': fields.selection([
            ('', 'No comment'),
            ('case_log_reply', 'Send partner & historize'),
            ('case_log', 'Historize')
        ], 'Comment type'),
        'no_action': fields.boolean(
            'No trigger an action',
            help="Change the state without trigger an action"
        ),
        'comment': fields.text('Comment'),
        'result': fields.text(string='Output Text', readonly=True)
    }

    _defaults = {
        'no_action': lambda *a: 0,
        'num_cases': _default_num_cases,
        'state': lambda *a: 'init',
        'result': _default_output_text
    }

WizardCrmMultiChange()
