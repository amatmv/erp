# -*- coding: utf-8 -*-
{
    "name": "Refacturación",
    "description": """CRM refacturación.""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Facturació",
    "depends": [
        "crm",
        "giscedata_facturacio",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "crm_refacturacion_data.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
