# -*- coding: utf-8 -*-
import unittest
from destral import testing
from destral.transaction import Transaction


class BaseTest(testing.OOTestCase):

    require_demo_data = True

    def test_calc_time_same_tracking(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            context = None

            case_obj = self.openerp.pool.get('crm.case')
            case_ids = case_obj.search(cursor, uid, [])
            if len(case_ids):
                case_obj.unlink(cursor, uid, case_ids)
            crm_section_obj = self.openerp.pool.get('crm.case.section')
            section_ids = crm_section_obj.search(cursor, uid, [])
            section_id = section_ids[0]

            tracking_obj = self.openerp.pool.get('crm.time.tracking')
            comer_tracking_id = tracking_obj.search(cursor, uid,
                                                    [('code', '=', '0')])[0]
            canal_obj = self.openerp.pool.get('res.partner.canal')
            canal_id = canal_obj.search(cursor, uid, [])[0]
            vals = {'name': "Cas de prova",
                    'section_id': section_id,
                    'time_tracking_id': comer_tracking_id,
                    'canal_id': canal_id
                    }
            # Create a new case
            case_obj.create(cursor, uid, vals, context)
            case_ids = case_obj.search(cursor, uid, [])
            self.assertEqual(len(case_ids), 1)

            # Open case with time_tracking_id = 0 (comer)
            case = case_obj.browse(cursor, uid, case_ids[0])
            case.case_open(cursor, uid)

            # Change time_tracking_id of case to 1 (distri) and close case
            distri_tracking_id = tracking_obj.search(cursor, uid,
                                                     [('code', '=', '1')])[0]
            case_obj.write(cursor, uid, case.id, {'time_tracking_id': distri_tracking_id})
            case = case_obj.browse(cursor, uid, case_ids[0])
            case.case_close(cursor, uid)

            # Change time_tracking_id of case to 0 (comer) and open case
            case_obj.write(cursor, uid, case.id,
                           {'time_tracking_id': comer_tracking_id})
            case = case_obj.browse(cursor, uid, case_ids[0])
            case.case_open(cursor, uid)

            # Change time_tracking_id of case to 3 (client) and close case
            client_tracking_id = tracking_obj.search(cursor, uid,
                                                     [('code', '=', '2')])[0]
            case_obj.write(cursor, uid, case.id,
                           {'time_tracking_id': client_tracking_id})
            case = case_obj.browse(cursor, uid, case_ids[0])
            case.case_close(cursor, uid)

            # Calc the days imputed to comer (time_tracking_id = 0) in logs
            log_obj = self.openerp.pool.get('crm.case.log')
            days_comer = log_obj.calc_time_same_tracking(cursor, uid, case.id,
                                                         comer_tracking_id)
            self.assertEqual(0, days_comer)
