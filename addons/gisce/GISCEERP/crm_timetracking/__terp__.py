# -*- coding: utf-8 -*-
{
    "name": "GISCE Time Tracking",
    "description": """Time Tracking to impute time in crm cases.""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "crm"
    ],
    "init_xml": [],
    "demo_xml":[
        "crm_demo.xml"
    ],
    "update_xml":[
        "security/ir.model.access.csv",
        "crm_view.xml"
    ],
    "active": False,
    "installable": True
}
