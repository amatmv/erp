# -*- coding: utf-8 -*-
from osv import osv, fields
from datetime import datetime
from workdays import *
from tools.translate import _


class CRMTimeTracking(osv.osv):
    _name = "crm.time.tracking"

    _rec_name = 'description'

    _columns = {
        'code': fields.char('Codi', size=1),
        'description': fields.char(_(u"Imputació de temps"), size=50,
                                   translate=True),
    }
    _defaults = {
    }

CRMTimeTracking()


class CRMCaseLog(osv.osv):
    _name = "crm.case.log"
    _inherit = "crm.case.log"

    def calc_time_same_tracking(self, cr, uid, case_id, tracking_id, context=None):
        """Calculate the total time imputed to an agent in a case by getting
        all the 'time_spent' of logs with the same tracking_id"""
        log_ids = self.search(cr, uid, [('case_id', '=', case_id),
                                        ('time_tracking_id', '=', tracking_id)])
        if not log_ids:
            return 0
        logs = self.read(cr, uid, log_ids, ['time_spent'])
        accumuled_days = 0
        for log in logs:
            if log['time_spent']:  # n'hauria de tenir sempre
                accumuled_days += log['time_spent']
        return accumuled_days

    def _ff_set_time_log(self, cursor, uid, ids, field_name, arg, context=None):
        """Calculate the days between today and the date of the last log"""
        res = {}
        for lg in self.read(cursor, uid, ids, ['case_id', 'date']):
            log_id = self.search(cursor, uid,
                                 [('case_id', '=', lg['case_id'][0]),
                                  ('date', '<', lg['date'])],
                                 order='date desc', limit=1)
            if log_id:  # Si es el primer log no n'hi hauran
                last_log = self.read(cursor, uid, log_id[0], ['date'])
                ini = datetime.strptime(last_log['date'], '%Y-%m-%d %H:%M:%S')
                now = datetime.strptime(lg['date'], '%Y-%m-%d %H:%M:%S')
                # networkdays include both 'ini' and 'now' days,
                # so we substract 1 of them and we have the days between them
                time_spent = networkdays(ini, now) - 1
                # we also want the hours left after calculating the difference
                # in days, so get the day before the last day, set the time
                # (hours, mins and secs) of the first day and calculate the
                # difference in hours
                day_ant = now - timedelta(days=1)
                day_ant = day_ant.replace(hour=ini.hour, minute=ini.minute,
                                          second=ini.second)
                decim = round((now - day_ant).seconds / 86400.0, 4)
                time_spent += decim
            else:
                time_spent = 0
            res[lg['id']] = time_spent
        return res

    def _get_last_case_logs(self, cursor, uid, ids, context=None):
        log_ids = []
        log_obj = self.pool.get('crm.case.log')
        for case_id in ids:
            log_id = log_obj.search(cursor, uid, [('case_id', '=', case_id)],
                                    order='date desc', limit=1)
            if log_id:
                log_ids.append(log_id[0])
        return log_ids

    _STORE_GET_TIME = {
        'crm.case': (_get_last_case_logs, ['state'], 10),
        'crm.case.log': (lambda self, cr, uid, ids, c=None: ids, ['date'], 10)
    }

    _columns = {
        'time_tracking_id': fields.many2one('crm.time.tracking',
                                            _(u"Imputacio de temps"),
                                              select=True),
        'time_spent': fields.function(_ff_set_time_log, method=True,
                                      string=_(u"Duració dies"), type='float',
                                      readonly=True, store=_STORE_GET_TIME,
                                      help=_(u"Dies hàbils gastats en l'estat actual que s'imputaran")),
    }

CRMCaseLog()


class CRMCaseHistory(osv.osv):
    _name = "crm.case.history"
    _inherit = "crm.case.history"

    _columns = {
        'time_tracking_id': fields.many2one('crm.time.tracking',
                                            _(u"Imputació de temps")),
    }

CRMCaseLog()


class CRMCase(osv.osv):

    _name = 'crm.case'
    _inherit = 'crm.case'

    def _history_vals(self, cr, uid, case, keyword):
        res = super(CRMCase, self)._history_vals(cr, uid, case, keyword)
        res.update({
            'time_tracking_id': case.time_tracking_id.id
        })
        return res

    _columns = {
        'time_tracking_id': fields.many2one('crm.time.tracking',
                                            _(u"Imputació de temps")),
    }

    _defaults = {
    }

CRMCase()
