<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="incidencies"/>
  </xsl:template>

  <xsl:template match="incidencies">
    <xsl:apply-templates select="incidencia" />
  </xsl:template>

  <xsl:template match="incidencia">
    <document>
      <template>
        <pageTemplate id="main">
          <frame id="first" x1="1cm" y1="1cm" width="19cm" height="277mm"/>
        </pageTemplate>
      </template>
      
      <stylesheet>

        <paraStyle 
        	name="titol"
			    fontName="Helvetica-Bold"
    			fontSize="16"
          leading="22"
        />
        
        <paraStyle
        	name="subtitol"
        	fontName="Helvetica"
        	fontSize="8"
        />

        <paraStyle
          name="description"
          fontName="Helvetica"
          fontSize="10"
        />
        
        <blockTableStyle id="taula_contingut">
          <blockValign value="TOP" start="1,0" stop="1,0" />
        </blockTableStyle>

        <blockTableStyle id="taula1">
          <blockFont name="Helvetica-Bold" size="10" />
          <lineStyle kind="LINEBEFORE" colorName="black" />
          <lineStyle kind="LINEABOVE" colorName="black" />
          <lineStyle kind="LINEAFTER" colorName="black" />
          <blockAlignment value="CENTER" start="1,0" stop="1,0" />
          <lineStyle kind="LINEBELOW" colorName="black" start="1,0" stop="1,0" />
        </blockTableStyle>

         <blockTableStyle id="taula2">
          <blockFont name="Helvetica" size="10" />
          <lineStyle kind="LINEBEFORE" start="0,0" stop="0,0" colorName="black" />
          <lineStyle kind="LINEBEFORE" start="0,1" stop="0,1" colorName="black" />
          <lineStyle kind="LINEBEFORE" start="0,2" stop="0,2" colorName="black" />
          <lineStyle kind="LINEBEFORE" start="0,3" stop="0,3" colorName="black" />
          <lineStyle kind="LINEAFTER" start="1,0" stop="1,0" colorName="black" />
          <lineStyle kind="LINEAFTER" start="1,1" stop="1,1" colorName="black" />
          <lineStyle kind="LINEAFTER" start="1,2" stop="1,2" colorName="black" />
          <lineStyle kind="LINEAFTER" start="1,3" stop="1,3" colorName="black" />
        </blockTableStyle>

        <blockTableStyle id="taula3">
          <blockFont name="Helvetica-Bold" size="10" />
          <lineStyle kind="LINEBEFORE" colorName="black" />
          <lineStyle kind="LINEAFTER" colorName="black" />
        </blockTableStyle>

        <blockTableStyle id="taula4">
          <blockFont name="Helvetica" size="10" />
          <lineStyle kind="LINEBEFORE" colorName="black" />
          <lineStyle kind="LINEAFTER" colorName="black" />
        </blockTableStyle>

        <blockTableStyle id="taula5">
          <blockFont name="Helvetica" size="10" />
          <lineStyle kind="LINEBEFORE" start="0,0" stop="0,0" colorName="black" />
          <lineStyle kind="LINEBEFORE" start="0,1" stop="0,1" colorName="black" />
          <lineStyle kind="LINEBEFORE" start="0,2" stop="0,2" colorName="black" />
          <lineStyle kind="LINEAFTER" start="3,0" stop="3,0" colorName="black" />
          <lineStyle kind="LINEAFTER" start="3,1" stop="3,1" colorName="black" />
          <lineStyle kind="LINEAFTER" start="3,2" stop="3,2" colorName="black" />
        </blockTableStyle>

        <blockTableStyle id="taula6">
          <blockFont name="Helvetica" size="10" />
          <lineStyle kind="LINEBEFORE" start="0,0" stop="0,0" colorName="black" />
          <lineStyle kind="LINEBELOW" colorName="black" />
          <lineStyle kind="LINEAFTER" start="1,0" stop="1,0" colorName="black" />
        </blockTableStyle>

        <blockTableStyle id="taula7">
          <blockFont name="Helvetica" size="10" />
          <lineStyle kind="LINEBEFORE" start="0,0" stop="0,0" colorName="black" />
          <lineStyle kind="LINEAFTER" start="2,0" stop="2,0" colorName="black" />
        </blockTableStyle>

        <blockTableStyle id="taula8">
          <blockFont name="Helvetica" size="10" />
          <lineStyle kind="LINEBEFORE" start="0,0" stop="0,0" colorName="black" />
          <lineStyle kind="LINEBEFORE" start="0,1" stop="0,1" colorName="black" />
          <lineStyle kind="LINEAFTER" start="1,0" stop="1,0" colorName="black" />
          <lineStyle kind="LINEAFTER" start="1,1" stop="1,1" colorName="black" />
        </blockTableStyle>

        <blockTableStyle id="taula9">
          <blockFont name="Helvetica" size="10" />
          <lineStyle kind="BOX" start="1,0" stop="1,0" colorName="black" />
          <lineStyle kind="BOX" start="1,1" stop="1,1" colorName="black" />
          <lineStyle kind="BOX" start="1,2" stop="1,2" colorName="black" />
          <lineStyle kind="LINEBEFORE" start="0,0" stop="0,0" colorName="black" />
          <lineStyle kind="LINEBEFORE" start="0,1" stop="0,1" colorName="black" />
          <lineStyle kind="LINEBEFORE" start="0,2" stop="0,2" colorName="black" />
          <lineStyle kind="LINEBEFORE" start="0,3" stop="0,3" colorName="black" />
          <lineStyle kind="LINEAFTER" start="5,0" stop="5,0" colorName="black" />
          <lineStyle kind="LINEAFTER" start="5,1" stop="5,1" colorName="black" />
          <lineStyle kind="LINEAFTER" start="5,2" stop="5,2" colorName="black" />
          <lineStyle kind="LINEAFTER" start="5,3" stop="5,3" colorName="black" />
        </blockTableStyle>

        <blockTableStyle id="taula10">
          <blockFont name="Helvetica" size="10" />
          <lineStyle kind="LINEBEFORE" start="0,0" stop="0,0" colorName="black" />
          <lineStyle kind="LINEAFTER" start="1,0" stop="1,0" colorName="black" />
        </blockTableStyle>

        <blockTableStyle id="taula11">
          <blockFont name="Helvetica" size="10" />
          <lineStyle kind="LINEABOVE" start="0,0" stop="5,0" colorName="black" />
          <lineStyle kind="LINEBEFORE" start="0,0" stop="0,0" colorName="black" />
          <lineStyle kind="LINEBEFORE" start="0,1" stop="0,1" colorName="black" />
          <lineStyle kind="LINEBEFORE" start="0,2" stop="0,2" colorName="black" />
          <lineStyle kind="LINEBEFORE" start="0,3" stop="0,3" colorName="black" />
          <lineStyle kind="LINEBEFORE" start="0,4" stop="0,4" colorName="black" />
          <lineStyle kind="LINEBEFORE" start="0,5" stop="0,5" colorName="black" />
          <lineStyle kind="LINEBEFORE" start="0,6" stop="0,6" colorName="black" />
          <lineStyle kind="LINEAFTER" start="5,0" stop="5,0" colorName="black" />
          <lineStyle kind="LINEAFTER" start="5,1" stop="5,1" colorName="black" />
          <lineStyle kind="LINEAFTER" start="5,2" stop="5,2" colorName="black" />
          <lineStyle kind="LINEAFTER" start="5,3" stop="5,3" colorName="black" />
          <lineStyle kind="LINEAFTER" start="5,4" stop="5,4" colorName="black" />
          <lineStyle kind="LINEAFTER" start="5,5" stop="5,5" colorName="black" />
          <lineStyle kind="LINEAFTER" start="5,6" stop="5,6" colorName="black" />
          <lineStyle kind="LINEBELOW" start="0,6" stop="5,6" colorName="black" />
          <lineStyle kind="BOX" start="1,1" stop="1,1" colorName="black" />
          <lineStyle kind="BOX" start="1,2" stop="1,2" colorName="black" />
          <lineStyle kind="BOX" start="1,3" stop="1,3" colorName="black" />
        </blockTableStyle>
        
        <blockTableStyle id="taula12">
          <blockFont name="Helvetica" size="10" />
          <lineStyle kind="LINEBEFORE" start="0,0" stop="0,0" colorName="black" />
          <lineStyle kind="LINEAFTER" start="3,0" stop="3,0" colorName="black" />
        </blockTableStyle>

        <blockTableStyle id="taula14">
          <blockFont name="Helvetica" size="10" />
          <lineStyle kind="LINEBEFORE" start="0,0" stop="0,0" colorName="black" />
          <lineStyle kind="LINEAFTER" start="0,0" stop="0,0" colorName="black" />
        </blockTableStyle>

        <blockTableStyle id="taula15">
          <blockFont name="Helvetica" size="10" />
          <lineStyle kind="LINEBEFORE" start="0,0" stop="0,0" colorName="black" />
          <lineStyle kind="LINEAFTER" start="0,0" stop="0,0" colorName="black" />
          <lineStyle kind="LINEBELOW" start="0,0" stop="0,0" colorName="black" />
        </blockTableStyle>

        <blockTableStyle id="taula16">
          <blockFont name="Helvetica" size="10" />
          <lineStyle kind="LINEBEFORE" start="0,0" stop="0,0" colorName="black" />
          <lineStyle kind="LINEAFTER" start="1,0" stop="1,0" colorName="black" />
          <lineStyle kind="LINEABOVE" start="0,0" stop="1,0" colorName="black" />
        </blockTableStyle>


      </stylesheet>
    
      <story>
        <blockTable style="taula_contingut" colWidths="2cm,15cm">
        	<tr>
        		<td><image file="addons/crm_extended/report/logo.png" width="15mm" height="15mm" /></td>
            <td><pre style="titol"><xsl:value-of select="//corporate-header/corporation/name" /></pre><pre style="subtitol"><xsl:value-of select="//corporate-header/corporation/title" /></pre></td>
          </tr>
        </blockTable>
        <blockTable style="taula1" colWidths="13cm,4cm">
          <tr>
            <td>Formulari de recollida de dades per INCIDENTS</td>
            <td>Incid�ncia: #<xsl:value-of select="id"/></td>
          </tr>
        </blockTable>
        <blockTable style="taula2" colWidths="3cm,14cm">
          <tr>
            <td>Empresa:</td><td><xsl:value-of select="empresa" /></td>
          </tr>
          <tr>
            <td>Av�s rebut per:</td><td><xsl:value-of select="creador" /></td>
          </tr>
           <tr>
            <td>Data i Hora:</td><td><xsl:value-of select="data" /></td>
          </tr>
        </blockTable>
        <blockTable style="taula3" colWidths="17cm">
          <tr>
            <td>Dades Client</td>
          </tr>
        </blockTable>
        <blockTable style="taula2" colWidths="3cm,14cm">
          <tr>
            <td>Nom:</td><td><xsl:value-of select="partner_address/name" /></td>
          </tr>
          <tr>
            <td>Adre�a:</td><td><xsl:value-of select="partner_address/street"/></td>
          </tr>
        </blockTable>
        <blockTable style="taula12" colWidths="2.5cm,6cm,2.5cm,6cm">
          <tr>
            <td>Tel�fon:</td><td><xsl:value-of select="partner_address/phone"/></td>
            <td>M�bil:</td><td><xsl:value-of select="partner_address/mobile" /></td>
          </tr>
        </blockTable>
        <blockTable style="taula2" colWidths="3cm,14cm">
          <tr>
            <td>Poblaci�:</td><td><xsl:value-of select="partner_address/poblacio"/></td>
          </tr>
        </blockTable>
        <blockTable style="taula3" colWidths="17cm">
          <tr>
            <td>Descripci� de l'anomalia</td>
          </tr>
        </blockTable>
        <blockTable style="taula4" colWidths="17cm">
          <tr>
            <td><xsl:value-of select="name" /></td>
          </tr>
          <tr>
            <td><para style="description"><xsl:value-of select="description" /></para></td>
          </tr>
        </blockTable>
        <blockTable style="taula3" colWidths="17cm">
          <tr>
            <td>Dades de la p�lissa</td>
          </tr>
        </blockTable>
        <blockTable style="taula5" colWidths="2.5cm,6cm,2.5cm,6cm">
          <tr>
            <td>P�lissa:</td><td><xsl:value-of select="polissa/name" /></td>
            <td>Codi:</td><td><xsl:value-of select="polissa/codi" /></td>
          </tr>
          <tr>
            <td>CUPS:</td><td><xsl:value-of select="polissa/cups" /></td>
            <td>N� Comptador:</td><td><xsl:value-of select="polissa/n_comptador" /></td>
          </tr>
          <tr>
            <td>E.T.:</td><td><xsl:value-of select="polissa/et" /></td>
            <td>L�nia:</td><td><xsl:value-of select="polissa/linia" /></td>
          </tr>
        </blockTable>
        <blockTable style="taula7" colWidths="2.5cm,8cm,6.5cm">
          <tr>
            <td>Carrer:</td><td><xsl:value-of select="polissa/carrer" /></td>
            <td>N�m: <xsl:value-of select="polissa/numero" /> Escala: <xsl:value-of select="polissa/escala" /></td>
          </tr>
        </blockTable>
        <blockTable style="taula5" colWidths="2.5cm,6cm,2.5cm,6cm">
          <tr>
            <td>Poblaci�:</td><td><xsl:value-of select="polissa/poblacio" /></td>
            <td>Municipi:</td><td><xsl:value-of select="polissa/municipi" /></td>
          </tr>
        </blockTable>
        <blockTable style="taula6" colWidths="3cm,14cm">
          <tr>
            <td>Av�s passat a:</td><td><xsl:value-of select="responsable" /></td>
          </tr>
        </blockTable>

        <spacer length="10" /> 

        <blockTable style="taula16" colWidths="13cm,4cm">
          <tr>
            <td>Sortida operaris de RET�N</td><td></td>
          </tr>
        </blockTable>
        <blockTable style="taula8" colWidths="5cm,12cm">
          <tr>
            <td>Nom OPERARIS:</td><td><xsl:value-of select="operaris" /></td>
          </tr>
          <tr>
            <td>N�m. COMPTADOR (Activa):</td><td><xsl:value-of select="n_comptador" /></td>
          </tr>
        </blockTable>

        <blockTable style="taula9" colWidths="4cm,0.65cm,2.7cm,4cm,0.65cm,5cm">
          <tr>
            <td>Hores Laborables</td>
            <td><xsl:if test="h_laborables!=''">X</xsl:if></td>
            <td></td>
            <td><xsl:value-of select="categoria" /></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>Hores no Laborables</td>
            <td><xsl:if test="h_n_laborables!=''">X</xsl:if></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>Dies Festius</td>
            <td><xsl:if test="dies_festius!=''">X</xsl:if></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
        </blockTable>

        <blockTable style="taula8" colWidths="4cm,13cm">
          <tr>
            <td>Hora Av�s:</td><td><xsl:value-of select="hora_avis" /></td>
          </tr>
          <tr>
            <td>Hora Normalitzaci�:</td><td><xsl:value-of select="hora_normalitzacio" /></td>
          </tr>
        </blockTable>

        <blockTable style="taula10" colWidths="4cm,13cm">
          <tr>
            <td>Descripci� Avaria:</td><td></td>
          </tr>
        </blockTable>
        <blockTable style="taula14" colWidths="17cm">
          <tr>
            <td><xsl:value-of select="descripcio_cas" /></td>
          </tr>
        </blockTable>
        <blockTable style="taula10" colWidths="4cm,13cm">
          <tr>
            <td>Observacions:</td><td></td>
          </tr>
        </blockTable>
        <blockTable style="taula15" colWidths="17cm">
          <tr>
            <td><xsl:value-of select="observacions" /></td>
          </tr>
        </blockTable>
        <spacer length="10" />
        <condPageBreak height="4.5cm" />
        <blockTable style="taula11" colWidths="4cm,0.65cm,2.7cm,4cm,0.65cm,5cm">
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td>N�m. Incident CALSER:</td>
            <td><xsl:value-of select="num_calser" /></td>
            <td></td>
          </tr>
          <tr>
            <td>Avaria del Client</td>
            <td><xsl:if test="avaria_client!=''">X</xsl:if></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>Avaria Pr�pia</td>
            <td><xsl:if test="avaria_propia!=''">X</xsl:if></td>
            <td></td>
            <td>Agents atmosf�rics:</td>
            <td><xsl:value-of select="agents_atmosferics" /></td>
            <td></td>
          </tr>
          <tr>
            <td>Avaria Tercers</td>
            <td><xsl:if test="avaria_tercers!=''">X</xsl:if></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>S'ha fet parte de l'accident:</td>
            <td></td>
            <td></td>
            <td><xsl:value-of select="parte" /></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>N�m. parte d'Accident (O.T):</td>
            <td><xsl:value-of select="n_parte" /></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>S'han demanat Afectacions pr�viament:</td>
            <td></td>
            <td></td>
            <td><xsl:value-of select="demanat_afectacions" /></td>
            <td></td>
            <td></td>
          </tr>
        </blockTable>

      </story>
    </document>
  </xsl:template>
</xsl:stylesheet>
