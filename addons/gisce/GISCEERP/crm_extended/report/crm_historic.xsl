<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="cases"/>
  </xsl:template>

  <xsl:template match="cases">
    <xsl:apply-templates select="case" />
  </xsl:template>

  <xsl:template match="case">
    <document>
      <template>
        <pageTemplate id="main">
          <frame id="first" x1="1cm" y1="1cm" width="19cm" height="277mm"/>
        </pageTemplate>
      </template>
      
      <stylesheet>

        <paraStyle 
        	name="titol"
			    fontName="Helvetica-Bold"
    			fontSize="16"
          leading="22"
        />
        
        <paraStyle
        	name="subtitol"
        	fontName="Helvetica"
        	fontSize="8"
        />

        <paraStyle name="case_name"
          fontName="Helvetica-Bold"
          fontSize="10"
        />
        

        <paraStyle
          name="description"
          fontName="Helvetica"
          fontSize="10"
        />
        
        <blockTableStyle id="taula_contingut">
          <blockValign value="TOP" start="1,0" stop="1,0" />
        </blockTableStyle>

        <blockTableStyle id="historic">
          <blockFont name="Helvetica" size="10" />
          <lineStyle kind="LINEBELOW" colorName="silver" start="0,0" stop="0,0" />
        </blockTableStyle>

      </stylesheet>
    
      <story>
        <blockTable style="taula_contingut" colWidths="2cm,15cm">
        	<tr>
        		<td><image file="addons/crm_extended/report/logo.png" width="15mm" height="15mm" /></td>
            <td><pre style="titol"><xsl:value-of select="//corporate-header/corporation/name" /></pre><pre style="subtitol"><xsl:value-of select="//corporate-header/corporation/title" /></pre></td>
          </tr>
        </blockTable>
        <para style="case_name">#<xsl:value-of select="id" />. Hist�ric <xsl:value-of select="name" /></para>
        <xsl:apply-templates match="historic" mode="story" />
      </story>
    </document>
  </xsl:template>

  <xsl:template match="historic" mode="story">
      <xsl:apply-templates match="nota" mode="story" />
  </xsl:template>

  <xsl:template match="nota" mode="story">
    <blockTable style="historic" colWidths="15cm">
    <tr>
      <td><xsl:value-of select="data" /></td>
    </tr>
    <tr>
      <td><para style="description"><xsl:value-of select="text" /></para></td>
    </tr>
    </blockTable>
  </xsl:template>
</xsl:stylesheet>
