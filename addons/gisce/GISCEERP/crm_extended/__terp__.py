# -*- coding: utf-8 -*-
{
    "name": "Extension for crm module",
    "description": """Extension for crm module""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "CRM",
    "depends":[
        "crm"
    ],
    "init_xml":[
        "data.xml"
    ],
    "demo_xml": [],
    "update_xml":[
        "crm_extended_view.xml",
        "crm_incidencia_view.xml",
        "crm_extended_report.xml",
        "security/ir.model.access.csv",
    ],
    "active": False,
    "installable": True
}
