# -*- coding: utf-8 -*-

from osv import fields,osv

class crm_incidencia(osv.osv):
    _name = "crm.incidencia"
    _inherits = {'crm.case': 'case_id'}

    _columns = {
      'descripcio': fields.char('Descripció curta', size=256),
      'operaris': fields.char('Operaris', size=256), # A substituïr per un many2many
      'n_comptador': fields.char('Núm. comptador', size=60),
      'hores_laborables': fields.boolean('Hores Laborables'),
      'hores_no_laborables': fields.boolean('Hores no Laborables'),
      'dies_festius': fields.boolean('Dies festius'),
      'hora_avis': fields.datetime('Hora d\'avís'),
      'hora_normalitzacio': fields.datetime('Hora normalització'),
      'num_calser': fields.char('Núm. incident. CALSER', size=60),
      'avaria_client': fields.boolean('Avaria del Client'),
      'avaria_propia': fields.boolean('Avaria Pròpia'),
      'avaria_tercers': fields.boolean('Avaria Tercers'),
      'agents_atmosferics':fields.selection([('si', 'Sí'), ('no', 'No')], 'Agents atmosfèrics'),
      'parte':fields.selection([('si', 'Sí'), ('no', 'No')], 'S\'ha fet parte de l\'accident'),
      'n_parte': fields.char('Núm. parte d\'Accident (O.T)', size=60),
      'demanat_afectacions':fields.selection([('si', 'Sí'), ('no', 'No')], 'S\'han demanat afectacions prèviament'),
      'observacions': fields.text('Observacions'),
                  'case_id': fields.many2one('crm.case', 'Cas', required=True),

    }
    def onchange_partner(self, cr, uid, ids, part):
        if not part:
            return {'value':{'partner_address_id': False}}
        addr = self.pool.get('res.partner').address_get(cr, uid, [part], ['contact'])
        data = {'partner_address_id':addr['contact']}
        return {'value': data}

    def onchange_ref1(self, cr, uid, ids,ref, partner_id):
        if ref and partner_id:
            if ref.split(',')[0] == "giscedata.polissa":
                return {'domain': {'ref': [('partner_id', '=', partner_id)]}}
            else:
                return {}
        else:
            return {}

    def name_get(self, cr, uid, ids, context={}):
        result = []
        if not len(ids):
            return []
        for i in self.read(cr, uid, ids):
            result.append((i['id'], i['name']))
        return result

    def polissa(self, cr, uid, ids, *args):
        refs = self.read(cr, uid, ids, ['ref', 'ref2'])
        refs = refs[0]
        for name in ['ref', 'ref2']:
            if refs[name]:
                model,id = refs[name].split(',')
                id = int(id)
                if model == 'giscedata.polissa' and id > 0:
                    polissa = self.pool.get('giscedata.polissa').browse(cr, uid, id)
                    return {
                      'name': polissa.name,
                      'codi': polissa.codi,
                      'municipi': polissa.cups_municipi,
                      'poblacio': polissa.cups_poblacio,
                      'numero': polissa.cups_numero,
                      'escala': polissa.cups_escala,
                      'carrer': polissa.cups_carrer,
                      'cups': polissa.cups.name,
                      'n_comptador': polissa.n_comptador,
                      'et': polissa.et,
                      'linia': polissa.linea,
                    }
        return {
                'name': '',
                'codi': '',
                'municipi': '',
                'poblacio': '',
                'numero': '',
                'escala': '',
                'carrer': '',
                'cups': '',
                'n_comptador': '',
                'et': '',
                'linia': '',
                }
    def case_reset(self, cr, uid, ids, *args):
        for i in self.browse(cr, uid, ids):
            i.case_id.case_reset(cr, uid, [i.case_id.id], args)
        return True

    def case_open(self, cr, uid, ids, *args):
        for i in self.browse(cr, uid, ids):
            i.case_id.case_open(cr, uid, [i.case_id.id], args)
        return True

    def case_close(self, cr, uid, ids, *args):
        for i in self.browse(cr, uid, ids):
            i.case_id.case_close(cr, uid, [i.case_id.id], args)
        return True

    def case_cancel(self, cr, uid, ids, *args):
        for i in self.browse(cr, uid, ids):
            i.case_id.case_cancel(cr, uid, [i.case_id.id], args)
        return True

    def case_pending(self, cr, uid, ids, *args):
        for i in self.browse(cr, uid, ids):
            i.case_id.case_pending(cr, uid, [i.case_id.id], args)
        return True

crm_incidencia()
