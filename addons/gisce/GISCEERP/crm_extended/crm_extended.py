# -*- coding: utf-8 -*-

from osv import fields,osv

class crm_case(osv.osv):

    _name = "crm.case"
    _inherit = "crm.case"

    def get_users_that_can_write(self, cr, uid, case, vals, context=None):
        user_obj = self.pool.get('res.users')
        res = [
            1,
            case.user_id.id,
            case.perm_read()[0]['create_uid'][0],
            case.section_id.user_id.id,
        ] + user_obj.search(cr, uid, [
            ('address_id.id', '=', case.partner_address_id.id)
        ])
        return res

    def write(self, cr, uid, ids, vals, context=None):
        """Comprovem que l'usuari que pot escriure pot.

        Usuaris que poden escriure:
            - Superadmin
            - El responsable del cas
            - L'ususari que l'ha creat
            - El responsable de la secció
            - Si existeix un usuari que té assignada l'adreça del partner.
        """
        conf_obj = self.pool.get('res.config')

        access_all = int(conf_obj.get(cr, uid, 'crm_access_universal_casos', '0'))

        for case in self.browse(cr, uid, ids, context):
            users_ok = self.get_users_that_can_write(
                cr, uid, case, vals, context=context
            )

            # mirem si tothom pot accedir a tots els casos
            if access_all:
                users_ok += [uid]

            if uid in users_ok:
                return super(crm_case, self).write(cr, uid, [case.id], vals, context)
            else:
                raise osv.except_osv('Error !', 'No puedes modificar éste registro.\n No lo has creado y tampoco eres el usuario responsable.')
                return -1


    def onchange_ref1(self, cr, uid, ids,ref, partner_id):
        if ref and partner_id:
            if ref.split(',')[0] == "giscedata.polissa":
                return {'domain': {'ref': [('titular', '=', partner_id)]}}
            else:
                return {}
        else:
            return {}

crm_case()
