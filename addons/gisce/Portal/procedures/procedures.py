# -*- coding: utf-8 -*-
from osv import osv, fields


PUBLISH_STATES = [('draft', 'Draft'), ('published', 'Published')]

class ProcedureCategory(osv.osv):

    _name = 'procedure.category'
    _rec_name = 'title'
    _order = 'sequence'

    _columns = {
        'title': fields.char('Title', size=256, required=True),
        'sequence': fields.integer('Sequence'),
        'parent_id': fields.many2one('procedure.category',
                                           'Parent Category'),
        'child_ids': fields.one2many('procedure.category',
                                          'parent_id', 'Subcategories'),
        'items': fields.one2many('procedure.item', 'category_id', 'Items'),
        'active': fields.boolean('Active')
    }

ProcedureCategory()

class ProcedureItem(osv.osv):

    _name = 'procedure.item'
    _rec_name = 'title'

    def _ff_has_icon(self, cursor, uid, ids, field_name, arg, context=None):
        if context is None:
            context = {}
        item_obj = self.pool.get(self._name)
        res = dict.fromkeys(ids, False)
        search_param = [('id', 'in', ids),
                        ('icon', '!=', False)]
        items_id = item_obj.search(cursor, uid, search_param)
        for item_id in items_id:
            res[item_id] = True

        return res

    _columns = {
        'published_date': fields.datetime('Create date'),
        'category_id': fields.many2one('procedure.category', 'Category'),
        'title': fields.char('Title', size=256, required=True),
        'content': fields.text('Content'),
        'icon': fields.binary('Icon'),
        'has_icon': fields.function(_ff_has_icon, type='boolean',
                                    method=True, string='Has icon'),
        'sequence': fields.integer('Sequence'),
        'state': fields.selection(PUBLISH_STATES, 'State', select=1),
        'active': fields.boolean('Active')

    }

ProcedureItem()