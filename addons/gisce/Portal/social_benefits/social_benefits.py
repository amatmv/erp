# -*- coding: utf-8 -*-
from osv import osv, fields

class SocialBenefitsCategory(osv.osv):

    _name = 'social.benefits.category'
    _inherit = 'procedure.category'

    _columns = {
        'parent_id': fields.many2one(
            'social.benefits.category', 'Parent Category'),
        'child_ids': fields.one2many(
            'social.benefits.category', 'parent_id', 'Subcategories'),
        'items': fields.one2many(
            'social.benefits.item', 'category_id', 'Items'),
    }

SocialBenefitsCategory()

class SocialBenefitsItem(osv.osv):

    _name = 'social.benefits.item'
    _inherit = 'procedure.item'

    _columns = {
        'category_id': fields.many2one('social.benefits.category', 'Category'),
    }

SocialBenefitsItem()