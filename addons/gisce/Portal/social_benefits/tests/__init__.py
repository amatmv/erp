# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from expects import *
from osv.orm import except_orm


class SocilaBenefitsTests(testing.OOTestCase):

    def test_no_access_not_employee(self):
        soc_obj = self.openerp.pool.get('social.benefits.item')
        user_obj = self.openerp.pool.get('res.users')
        group_obj = self.openerp.pool.get('res.groups')
        with Transaction().start(self.database) as txn:
            group_id = group_obj.search(txn.cursor, txn.user, [
                ('name', '=', 'Employee')
            ])
            expect(group_id).to(have_len(1))
            group_id = group_id[0]
            user_id = user_obj.create(txn.cursor, txn.user, {
                'name': 'user1',
                'login': 'user1'
            })
            user_obj.write(txn.cursor, txn.user, [user_id], {
                'groups_id': [(3, group_id)]
            })

            soc_id = soc_obj.create(txn.cursor, txn.user, {
                'title': 'New proc'
            })

            def callback():
                new = soc_obj.read(txn.cursor, user_id, soc_id)
            expect(callback).to(raise_error(except_orm, 'AccessError'))

    def test_access_employee(self):
        soc_obj = self.openerp.pool.get('social.benefits.item')
        user_obj = self.openerp.pool.get('res.users')
        group_obj = self.openerp.pool.get('res.groups')
        with Transaction().start(self.database) as txn:
            group_id = group_obj.search(txn.cursor, txn.user, [
                ('name', '=', 'Employee')
            ])
            expect(group_id).to(have_len(1))
            group_id = group_id[0]
            user_id = user_obj.create(txn.cursor, txn.user, {
                'name': 'user1',
                'login': 'user1'
            })

            soc_id = soc_obj.create(txn.cursor, txn.user, {
                'title': 'New social'
            })

            soc = soc_obj.read(txn.cursor, user_id, soc_id)
            expect(soc).to(have_keys(
                title=u"New social"
            ))