# -*- coding: utf-8 -*-
{
    "name": "Social Benefits",
    "description": """
        OpenERP Social Benefits
        """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Portal",
    "depends":[
        "base",
        "procedures"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
