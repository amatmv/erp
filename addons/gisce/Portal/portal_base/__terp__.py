# -*- coding: utf-8 -*-
{
    "name": "Portal Base",
    "description": """
        Portal base
        """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Portal",
    "depends": [],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/portal_base_security.xml"
    ],
    "active": False,
    "installable": True
}
