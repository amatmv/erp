# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from expects import *
from osv.orm import except_orm


class NewsTests(testing.OOTestCase):

    def test_create_new(self):
        news_obj = self.openerp.pool.get('news')
        with Transaction().start(self.database) as txn:
            new_id = news_obj.create(txn.cursor, txn.user, {
                'title': 'New test',
                'content': '<h1>TITLE</h1>'
            })

            new = news_obj.read(txn.cursor, txn.user, new_id)
            expect(new).to(have_keys(
                title=u"New test",
                content=u"<h1>TITLE</h1>"
            ))

    def test_no_access_employee(self):
        news_obj = self.openerp.pool.get('news')
        user_obj = self.openerp.pool.get('res.users')
        group_obj = self.openerp.pool.get('res.groups')
        with Transaction().start(self.database) as txn:
            group_id = group_obj.search(txn.cursor, txn.user, [
                ('name', '=', 'Employee')
            ])
            expect(group_id).to(have_len(1))
            group_id = group_id[0]
            user_id = user_obj.create(txn.cursor, txn.user, {
                'name': 'user1',
                'login': 'user1'
            })
            user_obj.write(txn.cursor, txn.user, [user_id], {
                'groups_id': [(3, group_id)]
            })

            enc_id = news_obj.create(txn.cursor, txn.user, {
                'title': 'New test'
            })

            def callback():
                new = news_obj.read(txn.cursor, user_id, enc_id)
            expect(callback).to(raise_error(except_orm, 'AccessError'))

    def test_access_employee(self):
        news_obj = self.openerp.pool.get('news')
        user_obj = self.openerp.pool.get('res.users')
        group_obj = self.openerp.pool.get('res.groups')
        with Transaction().start(self.database) as txn:
            group_id = group_obj.search(txn.cursor, txn.user, [
                ('name', '=', 'Employee')
            ])
            expect(group_id).to(have_len(1))
            group_id = group_id[0]
            user_id = user_obj.create(txn.cursor, txn.user, {
                'name': 'user1',
                'login': 'user1'
            })

            enc_id = news_obj.create(txn.cursor, txn.user, {
                'title': 'New test'
            })

            new = news_obj.read(txn.cursor, user_id, enc_id)
            expect(new).to(have_keys(
                title=u"New test"
            ))