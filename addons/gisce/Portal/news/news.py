from osv import osv, fields


class News(osv.osv):
    _name = 'news'
    _rec_name = 'title'

    def _ff_attachment_ids(self, cursor, uid, ids, field_name, arg,
                                context=None):
        ir_attach_obj = self.pool.get('ir.attachment')
        res = {}
        for item_id in ids:
            search_params = [
                ('res_model', '=', self._name),
                ('res_id', '=', item_id)
            ]
            attach_ids = ir_attach_obj.search(cursor, uid, search_params)
            res[item_id] = attach_ids
        return res

    _columns = {
        'published_date': fields.datetime('Create date'),
        'title': fields.char('Title', size=256),
        'content': fields.text('Content'),
        'attachments': fields.function(_ff_attachment_ids, type='one2many',
                                       obj='ir.attachment',
                                       string='Attachments',
                                       method=True),
    }

News()