# -*- coding: utf-8 -*-
{
    "name": "Encuestas",
    "description": """
        OpenERP encuestas
        """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Portal",
    "depends":[
        "base",
        "portal_base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
