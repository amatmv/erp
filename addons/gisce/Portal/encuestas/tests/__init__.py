# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from expects import *
from osv.orm import except_orm


class EncuestasTests(testing.OOTestCase):

    def setUp(self):
        super(EncuestasTests, self).setUp()
        enc_obj = self.openerp.pool.get('encuesta.encuesta')
        opt_obj = self.openerp.pool.get('encuesta.opciones')
        with Transaction().start(self.database) as txn:
            self.enc_id = enc_obj.create(txn.cursor, txn.user, {
                'name': 'Pregunta 1'
            })
            self.opt_id = opt_obj.create(txn.cursor, txn.user, {
                'name': 'Option 1',
                'encuesta': self.enc_id,

            })
            txn.cursor.commit()

    def test_access_employee(self):
        enc_obj = self.openerp.pool.get('encuesta.encuesta')
        opt_obj = self.openerp.pool.get('encuesta.opciones')
        user_obj = self.openerp.pool.get('res.users')
        group_obj = self.openerp.pool.get('res.groups')
        with Transaction().start(self.database) as txn:
            group_id = group_obj.search(txn.cursor, txn.user, [
                ('name', '=', 'Employee')
            ])
            expect(group_id).to(have_len(1))
            user_id = user_obj.create(txn.cursor, txn.user, {
                'name': 'user1',
                'login': 'user1'
            })
            enc = enc_obj.read(txn.cursor, user_id, self.enc_id)
            expect(enc).to(have_keys(
                name=u"Pregunta 1"
            ))
            opt = opt_obj.read(txn.cursor, user_id, self.opt_id)
            expect(opt).to(have_keys(
                name=u"Option 1"
            ))
            opt_obj.vote(txn.cursor, user_id, [self.opt_id])
            opt = opt_obj.read(txn.cursor, user_id, self.opt_id)
            expect(opt).to(have_keys(
                name=u"Option 1",
                votos=1
            ))



    def test_no_access_employee(self):
        enc_obj = self.openerp.pool.get('encuesta.encuesta')
        user_obj = self.openerp.pool.get('res.users')
        group_obj = self.openerp.pool.get('res.groups')
        with Transaction().start(self.database) as txn:
            group_id = group_obj.search(txn.cursor, txn.user, [
                ('name', '=', 'Employee')
            ])
            expect(group_id).to(have_len(1))
            group_id = group_id[0]
            user_id = user_obj.create(txn.cursor, txn.user, {
                'name': 'user1',
                'login': 'user1'
            })
            user_obj.write(txn.cursor, txn.user, [user_id], {
                'groups_id': [(3, group_id)]
            })

            enc_id = enc_obj.create(txn.cursor, txn.user, {
                'name': 'Pregunta 1'
            })

            def callback():
                new = enc_obj.read(txn.cursor, user_id, enc_id)
            expect(callback).to(raise_error(except_orm, 'AccessError'))
