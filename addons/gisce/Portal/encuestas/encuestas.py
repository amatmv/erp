# -*- coding: utf-8 -*-
from hashlib import sha1

from osv import osv, fields


class EncuestaEncuesta(osv.osv):
    _name = 'encuesta.encuesta'

    _columns = {
        'name': fields.char('Pregunta', size=256),
        'description': fields.text('Descripción'),
        'opciones': fields.one2many(
            'encuesta.opciones', 'encuesta', 'Opciones'),
        'state': fields.selection([
            ('draft', 'Borrador'), ('open', 'Abierta'), ('close', 'Cerrada')
        ], 'Estado'),
        'date_open': fields.date('Fecha abertura'),
        'type': fields.selection([
            ('radio', 'Radio'), ('checkbox', 'Checkbox')
        ], 'Tipo')
    }


    _defaults = {
        'state': lambda *a: 'draft',
        'type': lambda *a: 'radio'
    }

EncuestaEncuesta()


class EncuestaOpciones(osv.osv):
    _name = 'encuesta.opciones'

    def vote(self, cursor, uid, ids, context=None):
        control_obj = self.pool.get('encuesta.usuario')
        for opcion in self.browse(cursor, 1, ids):
            votos = opcion.votos + 1
            opcion.write({'votos': votos})
            control_obj.create(cursor, 1, {
                'user': uid,
                'encuesta': opcion.encuesta.id
            })
        return True

    def _color(self, cursor, uid, ids, field_name, args, context=None):
        if context is None:
            context = {}
        res = {}
        for opcion in self.browse(cursor, uid, ids):
            name = opcion.name.encode('utf-8')
            res[opcion.id] = '#%s' % sha1(name).hexdigest()[:6].upper()
        return res

    _columns = {
        'name': fields.char('Opción', size=256),
        'encuesta': fields.many2one('encuesta.encuesta', 'Encuesta'),
        'votos': fields.integer('Votos'),
        'color': fields.function(_color, size=7, type='char', method=True)
    }

    _defaults = {
        'votos': lambda *a: 0,
    }

EncuestaOpciones()


class EncuestaUsuario(osv.osv):
    _name = 'encuesta.usuario'

    _columns = {
        'user': fields.many2one('res.users', 'Usuario'),
        'encuesta': fields.many2one('encuesta.encuesta', 'Encusta')
    }

EncuestaUsuario()
