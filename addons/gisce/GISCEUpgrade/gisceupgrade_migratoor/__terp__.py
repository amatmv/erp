# -*- coding: utf-8 -*-
{
    "name": "GisceUpgradeMigratoor",
    "description": """Mòdul per migrar mòduls GISCE ERP""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Upgrade",
    "depends":[
        "base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
