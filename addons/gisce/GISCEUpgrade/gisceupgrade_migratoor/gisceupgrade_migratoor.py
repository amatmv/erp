# -*- coding: utf-8 -*-

"""Migració de versió de mòduls de GISCE ERP
"""
import os
import time
from os.path import join as opj

from osv import fields, osv
import pooler
import netsvc
from tools import file_open, xml_import
from tools.convert import _ref, unsafe_eval, _eval_xml
from addons import register_class, get_module_resource
from lxml import etree

MIG_CLASSES = {}

logger = netsvc.Logger()

def log(msg, level=netsvc.LOG_INFO):
    logger.notifyChannel('migration', level, msg)


class xml_pre_import(xml_import):
    """Sobreescrivim la classe xml_import per adaptar-la a les nostres
    funcionalitats.
    """
    def __init__(self, cursor, module, idref, mode, report=None,
                 noupdate=False, search_keys=None):
        self.search_keys = search_keys
        if not self.search_keys:
            self.search_keys = {}
        self.logger = netsvc.Logger()
        super(xml_pre_import, self).__init__(cursor, module, idref, mode,
                                              report, noupdate)
        self._tags = {
            #'menuitem': self._tag_menuitem,
            'record': self._tag_record,
        }

    def _tag_record(self, cr, rec, data_node=None):
        imd_obj = self.pool.get('ir.model.data')
        rec_model = rec.get("model").encode('ascii')
        if rec_model.startswith('ir.'):
            return
        model = self.pool.get(rec_model)
        assert model, "The model %s does not exist !" % (rec_model,)
        rec_id = rec.get("id",'').encode('ascii')
        self._test_xml_id(rec_id)
        #if self.isnoupdate(data_node) and self.mode != 'init':
            # check if the xml record has an id string
        if rec_id:
            if '.' in rec_id:
                module, rec_id2 = rec_id.split('.')
            else:
                module = self.module
                rec_id2 = rec_id
            ir_model_data = self.pool.get('ir.model.data')
            res_id = ir_model_data._update_dummy(cr, self.uid, rec_model,
                                                 module, rec_id2)
            # check if the resource already existed at the last update
            if res_id:
                # if it existed, we don't update the data, but we need to
                # know the id of the existing record anyway
                self.idref[rec_id2] = int(res_id)
                return None
            else:
                # Relinking time!
                # Pot ser que el que tinguem definit ja existeixi a la base de
                # dades però no estigui a l'ir_model_data, en aquest sentit ho 
                # haurem de crear
                res = {}
                for field in rec.findall('./field'):
                    #TODO: most of this code is duplicated above (in _eval_xml)
                    f_name = field.get("name",'').encode('utf-8')
                    f_ref = field.get("ref",'').encode('ascii')
                    f_search = field.get("search",'').encode('utf-8')
                    f_model = field.get("model",'').encode('ascii')
                    if not f_model and model._columns.get(f_name,False):
                        f_model = model._columns[f_name]._obj
                    f_use = field.get("use",'').encode('ascii') or 'id'
                    f_val = False
        
                    if f_search:
                        self.idref['ref'] = _ref(self, cr)
                        q = unsafe_eval(f_search, self.idref)
                        field = []
                        assert f_model, 'Define an attribute model="..." in your .XML file !'
                        f_obj = self.pool.get(f_model)
                        # browse the objects searched
                        s = f_obj.browse(cr, self.uid,
                                         f_obj.search(cr,self.uid, q))
                        # column definitions of the "local" object
                        _cols = self.pool.get(rec_model)._columns
                        # if the current field is many2many
                        if (f_name in _cols
                                and _cols[f_name]._type=='many2many'):
                            f_val = [(6, 0, map(lambda x: x[f_use], s))]
                        elif len(s):
                            # otherwise (we are probably in a many2one field),
                            # take the first element of the search
                            f_val = s[0][f_use]
                    elif f_ref:
                        if f_ref=="null":
                            f_val = False
                        else:
                            try:
                                f_val = self.id_get(cr, f_model, f_ref)
                            except:
                                pass
                    else:
                        f_val = _eval_xml(self,field, self.pool, cr, self.uid,
                                          self.idref)
                        if model._columns.has_key(f_name):
                            if isinstance(model._columns[f_name],
                                          fields.integer):
                                f_val = int(f_val)
                    res[f_name] = f_val
                search_params = []
                constrained = self.search_keys.get(rec_model, res.keys())
                for key, value in res.items():
                    if key not in constrained:
                        continue
                    search_params += [(key, '=', value)]
                obj = self.pool.get(rec_model)
                # Do our best I
                savepoint_rec_id = 'rec_id_%s' % ''.join(
                    [x.isalnum() and x or '_' for x in rec_id]
                )
                cr.savepoint(savepoint_rec_id)
                try:
                    res_id = obj.search(cr, self.uid, search_params,
                                        context={'active_test': False})
                except:
                    cr.rollback(savepoint_rec_id)
                    return None
                finally:
                    cr.release(savepoint_rec_id)
                # Si no trobem res_id provem de crear-ho, si el model no
                # existeix o no hi ha tots els camps, no passa res, que falli
                # i quan fem el següent pas (update oficial) ja ho posarà tot.
                if not res_id:
                    return None
                    """
                    # Do our best II
                    cr.savepoint(rec_id)
                    try:
                        res_id = imd_obj._update(cr, self.uid, rec_model,
                            self.module, res, rec_id or False,
                            not self.isnoupdate(data_node),
                            noupdate=self.isnoupdate(data_node), mode=self.mode)
                        if res_id:
                            self.idref[rec_id] = int(res_id)
                            return rec_model, res_id
                    except Exception:
                        traceback.print_exc()
                        cr.rollback(rec_id)
                        return None
                    finally:
                        cr.release(rec_id)
                    """
                # Si trobem ID vol dir que hi ha les dades que abans potser
                # s'entraven de forma manual i ara van a través d'un XML
                # o que ha canviat la font de l'XML
                if len(res_id) > 1:
                    self.logger.notifyChannel('migration', netsvc.LOG_WARNING,
                                              u"Més d'un resultat per la cerca "
                                              u"%s -> %s" % (search_params,
                                                             res_id))
                res_id =  res_id[0]
                vals = {'res_id': res_id,
                        'module': self.module,
                        'model': rec_model,
                        'date_update': time.strftime('%Y-%m-%d %H:%M:%S'),
                        'name': rec_id,
                        'noupdate': self.isnoupdate(data_node)}
                imd_id = imd_obj.search(cr, self.uid,
                                        [('res_id', '=', res_id),
                                         ('model', '=', rec_model)])
                # Hem trobat que hi ha un model_data que apunta a aquí!
                if imd_id:
                    imd_obj.write(cr, self.uid, imd_id, vals)
                    self.logger.notifyChannel('migration', netsvc.LOG_DEBUG,
                                              u"Enllaçant %s(id:%s) -> "
                                              u"%s(id:%s)" % (self.module,
                                                              rec_id,
                                                              rec_model,
                                                              res_id))
                else:
                    imd_obj.create(cr, self.uid, vals)
                    self.logger.notifyChannel('migration', netsvc.LOG_DEBUG,
                                              u"Creant %s(id:%s) -> "
                                              u"%s(id:%s)" % (self.module,
                                                              rec_id,
                                                              rec_model,
                                                              res_id))
                if rec_id:
                    self.idref[rec_id] = int(res_id)

    def parse(self, de):
        if not de.tag in ['terp', 'openerp']:
            self.logger.notifyChannel("init", netsvc.LOG_ERROR,
                                      "Mismatch xml format" )
            raise Exception("Mismatch xml format: only terp or openerp as root "
                            "tag")

        for n in de.findall('./data'):
            for rec in n:
                if rec.tag in self._tags:
                    try:
                        self._tags[rec.tag](self.cr, rec, n)
                    except:
                        self.logger.notifyChannel("init", netsvc.LOG_ERROR, '\n'+etree.tostring(rec))
                        self.cr.rollback()
                        raise

class myosv(osv.osv):
    _name = False
    def __new__(cls):
        if cls._name:
            MIG_CLASSES[cls._name] = True
            log("catching %s" % cls._name)
        else:
            MIG_CLASSES[cls._inherit] = True
            cls._name = cls._inherit
            log("catching %s" % cls._inherit)
        super(myosv, cls).__new__(cls)

class GisceUpgradeMigratoor(object):
    """Mòdul amb les funcions comunes de migració.
    """
    def __init__(self, module, cursor):
        """Constructor
        """
        self.module = module
        self.cursor = cursor
        self.log = netsvc.Logger().notifyChannel
        self.log('migration', 'info', 'Inicialitzant pool')
        self.pool = pooler.get_pool(cursor.dbname)
        self.suffix = None

    def _list_models(self):
        """Mètode auxiliar per llistar els models que proveeix un mòdul

        Hack per obtenir les classes que es defineixen dins el mòdul
        """
        MIG_CLASSES.clear()
        osv_copy = osv.osv
        osv.osv = myosv
        self.log('migration', 'info', 'Registering module %s' % self.module)
        register_class(self.module)
        osv.osv = osv_copy
        return MIG_CLASSES

    def model_exists(self, model):
        self.cursor.execute("SELECT relname FROM pg_class WHERE relkind in "
                            "('r','v') AND relname = %s", (model,))
        return self.cursor.rowcount

    def _type_args(self, column):
        """Construeix el tipus de dada SQL
        """
        # si és un tipus amb nom amb espai, ens quedem amb la primera part
        type_ = column['data_type'].split()[0]
        o_type = column['data_type']
        if type_ in ('character'):
            o_type = '%s(%s)' % (o_type, column['character_maximum_length'])
        elif type_ in ('numeric'):
            o_type = '%s(%s,%s)' % (o_type, column['numeric_precision'], 
                                  column['numeric_precision_radix'])
        if column['column_default']:
            o_type = '%s DEFAULT %s' % (o_type, column['column_default'])
        if not column['is_nullable']:
            o_type = '%s NOT NULL' % (o_type,)
        return o_type

    def get_local_fields(self, model):
        """Retorna els camps actuals i la comanda SQL per la seva creació
        """
        self.cursor.execute("SELECT column_name, data_type, "
                            "character_maximum_length, numeric_precision, "
                            "numeric_precision_radix, is_nullable, "
                            "column_default "
                            "FROM information_schema.columns WHERE "
                            "table_name = %s", (model,))
        res = {}
        for col in self.cursor.dictfetchall():
            res[col['column_name']] = self._type_args(col)
        return res

    def backup(self, suffix='bak', only_load=False):
        """Mètode per fer backup de les columnes existents
        """
        self.suffix = suffix
        if not only_load:
            self.log('migration', 'info', 'Backing up %s...' % self.module)
            self.log('migration', 'info', 'Suffixing columns with %s' % self.suffix)
        models = self._list_models()
        self.pool.instanciate(self.module, self.cursor)
        if only_load:
            return
        for model in models:
            self.log('migration', 'info', 'Processing model %s' % model)
            obj = self.pool.get(model)
            if not self.model_exists(obj._table):
                continue
            if obj._sequence != obj._table + '_id_seq':
                # Això és de la F**** herència d'ir_actions postgres inherits
                # només hem de modificar els camps que no es troben a la classe
                # pare
                for fobj_name in MIG_CLASSES:
                    fobj = self.pool.get(fobj_name)
                    if fobj._sequence == obj._sequence:
                        for col in fobj._columns:
                            if col in obj._columns:
                                del obj._columns[col]
                        break
            try:
                if obj._table != 'ir_attachment':
                    self.cursor.execute(
                        "select exists("
                        "select * from information_"
                        "schema.tables where table_name='%s_%s')"
                        % (obj._table, self.suffix))
                    exist = self.cursor.fetchone()[0]

                    if not exist:
                        sql = "CREATE UNLOGGED TABLE %s_%s AS SELECT * FROM %s" \
                              % (obj._table, self.suffix, obj._table)
                        self.cursor.execute(sql)
                #local_fields = self.get_local_fields(obj._table)
                #for col in local_fields:
                #    if not col.endswith(suffix):
                #        col_ = '%s_%s' % (col, self.suffix)
                #        if col_ in local_fields.keys():
                #            self.log('migration', 'warning', 'Col %s already '
                #                     'exists in %s!' % (col_, obj._table))
                #            continue
                #        sql = "ALTER TABLE %s ADD COLUMN %s %s" % (obj._table,
                #                                            col_,
                #                                            local_fields[col])
                #        self.cursor.execute(sql)
                #        sql = 'UPDATE %s SET %s = "%s"' % (obj._table, col_,
                #                                           col)
                #        self.cursor.execute(sql)
            except Exception, exc:
                print(exc)
                raise

    def pre_xml(self, search_keys=None, xmlfiles=None, force_xmlfiles=None):
        """Mètode per preparar la càrrega d'XMLs

        Només cal mirar els tags <record />
        """
        if not xmlfiles:
            xmlfiles = []
        if not force_xmlfiles:
            force_xmlfiles = []
        idref = {}
        m = self.module
        terp_file = get_module_resource(self.module, '__terp__.py')
        info = eval(file_open(terp_file).read())
        for kind in ('init', 'update'):
            for filename in info.get('%s_xml' % kind, []) + force_xmlfiles:
                if xmlfiles and filename not in xmlfiles + force_xmlfiles:
                    continue
                ext = os.path.splitext(filename)[1]
                if ext != '.xml':
                    continue
                self.log('init', netsvc.LOG_INFO, 'module %s: (pre)loading %s'
                         % (m, filename))
                fp = file_open(opj(m, filename))
                doc = etree.parse(fp)
                xml_data = xml_pre_import(self.cursor, m, idref, 'update',
                                          search_keys=search_keys)
                xml_data.parse(doc.getroot())
