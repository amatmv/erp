# -*- coding: utf-8 -*-

import pooler

def migrate(cursor, installed_version):
    uid = 1
    pool = pooler.get_pool(cursor.dbname)
    conf_obj = pool.get('giscesoa.sync.config')
    params_search = [('name', '=', 'validar_nif')]
    res_id = conf_obj.search(cursor, uid, params_search,
                            context={'active_test': False})
    if res_id:
        vals = {'value': '1'}
        conf_obj.write(cursor, uid, res_id, vals)
