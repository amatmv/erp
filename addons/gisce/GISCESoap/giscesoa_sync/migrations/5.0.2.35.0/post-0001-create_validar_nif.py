# -*- coding: utf-8 -*-

import pooler
from datetime import datetime

def migrate(cursor, installed_version):
    uid = 1
    pool = pooler.get_pool(cursor.dbname)
    conf_obj = pool.get('giscesoa.sync.config')
    imd_obj = pool.get('ir.model.data')

    params_search = [('name', '=', 'validar_nif')]

    exist = conf_obj.search(cursor, uid, params_search,
                            context={'active_test': False})
    if not exist:
        vals = {'name':'validar_nif',
                'value': '1',
                'description': '"1" indica que es validaran els NIFs de '
                               'manera habitual. "0" no validarà.',
                'ordre': 6
                }
        res_id = conf_obj.create(cursor, uid, vals)
        now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        vals = {
            'module': 'giscesoa_sync',
            'model': 'giscesoa.sync.config',
            'res_id': res_id,
            'name': 'giscesoa_sync_60',
            'noupdate': 1,
            'date_init': now,
            'date_update': now
        }
        imd_obj.create(cursor, uid, vals)
