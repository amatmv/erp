# -*- coding: utf-8 -*-

import pooler
import logging
from oopgrade import oopgrade


def migrate(cursor, installed_version):
    """
    Checks if is a distri or a comer and installs giscesoa_sync_*

    :param cursor: Database cursor
    :param installed_version: Installed version
    :return: None
    """
    admin_uid = 1
    pool = pooler.get_pool(cursor.dbname)
    module_obj = pool.get('ir.module.module')
    logger = logging.getLogger('openerp.migration')

    search_distri = [("name", "=", "giscedata_polissa_distri")]

    id_polissa_distri = module_obj.search(
        cursor, admin_uid, search_distri)
    data_polissa_distri = module_obj.read(
        cursor, admin_uid, id_polissa_distri, ["state"])[0]

    search_comer = [("name", "=", "giscedata_polissa_comer")]
    id_polissa_comer = module_obj.search(
        cursor, admin_uid, search_comer)
    data_polissa_comer = module_obj.read(
        cursor, admin_uid, id_polissa_comer, ["state"])[0]

    is_distri = False
    if data_polissa_distri["state"] == "installed":
        is_distri = True

    is_comer = False
    if data_polissa_comer["state"] == "installed":
        is_comer = True

    if is_distri and not is_comer:
        logger.info("Instalant giscesoa_sync_distri")
        oopgrade.install_modules(cursor, "giscesoa_sync_distri")

    if is_comer and not is_distri:
        logger.info("Instalant giscesoa_sync_comer")
        oopgrade.install_modules(cursor, "giscesoa_sync_comer")

    if is_distri == is_comer:
        if is_distri:
            logger.info("giscedata_polissa_distri i giscedata_polissa_comer instalats alhora, no es fa res ")

        else:
            logger.info("ni giscedata_polissa_distri i giscedata_polissa_comer instalats(cap dels dos), no es fa res ")

up = migrate