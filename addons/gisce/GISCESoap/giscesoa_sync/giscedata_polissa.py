# -*- coding: utf-8 -*-
from osv import osv, fields

class GiscedataPolissaModcontractual(osv.osv):
    """Modificació Contractual d'una Pòlissa."""
    _name = 'giscedata.polissa.modcontractual'
    _inherit = 'giscedata.polissa.modcontractual'

    _columns = {
        'tensio_normalitzada': fields.many2one('giscedata.tensions.tensio',
                                    'Tensió normalitzada', required=False),
        'tensio': fields.integer('Tensió (V)', required=False),
        'comercialitzadora': fields.many2one('res.partner', 'Comercialitzadora',
                                             required=False),
    }

GiscedataPolissaModcontractual()