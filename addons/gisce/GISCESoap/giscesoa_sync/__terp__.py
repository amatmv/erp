# -*- coding: utf-8 -*-
{
    "name": "Actualització de pòlisses de sistema de gestió extern",
    "description": """Integració amb sistema de gestió extern que consisteix en actualitzar informació de pòlisses (i models relacionats) amb la informació que ens ha de proveïr aquest sistema extern. La sincronització es únicament en un sentit, com a mínim pel que respecta a aquest mòdul: des del sistema extern cap a GISCE ERP.""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE_SOA",
    "depends":[
        "base",
        "base_extended",
        "giscedata_polissa",
        "giscedata_cups",
        "giscedata_lectures",
        "oorq"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/giscesoa_sync_security.xml",
        "security/ir.model.access.csv",
        "giscesoa_sync_data.xml",
        "giscesoa_sync_view.xml",
        "giscesoa_sync_workflow_linia.xml",
        "giscesoa_sync_workflow_fitxer.xml",
        "giscesoa_sync_scheduled_action.xml",
        "wizard/giscesoa_sync_upload_file.xml"
    ],
    "active": False,
    "installable": True
}
