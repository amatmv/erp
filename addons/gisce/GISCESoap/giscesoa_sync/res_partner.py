# -*- coding: utf-8 -*-

from osv import osv

class res_partner(osv.osv):
    _name = 'res.partner'
    _inherit = 'res.partner'

    def check_vat(self, cursor, uid, ids):
        conf_obj = self.pool.get('giscesoa.sync.config')
        validar_nif = int(conf_obj.get_property(cursor, uid, self._module,
                                        "validar_nif", "1"))
        if validar_nif:
            return super(res_partner, self).check_vat(cursor, uid, ids)
        else:
            return True

res_partner()