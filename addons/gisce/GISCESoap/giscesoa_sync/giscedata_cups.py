# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscedataCupsPs(osv.osv):
    """Classe d'un CUPS (Punt de servei)."""
    _name = 'giscedata.cups.ps'
    _inherit = 'giscedata.cups.ps'

    _columns = {
        'es': fields.char('Escala', size=50),
        'pt': fields.char('Planta', size=50),
        'pu': fields.char('Porta', size=50),
    }

GiscedataCupsPs()
