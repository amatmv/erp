# -*- coding: utf-8 -*-
"""Models per la presa de lectures
"""

from osv import osv
import netsvc


class GiscedataLecturesComptador(osv.osv):
    """Comptadors
    """
    _name = 'giscedata.lectures.comptador'
    _inherit = 'giscedata.lectures.comptador'

    def __init__(self, pool, cursor):
        super(GiscedataLecturesComptador, self).__init__(pool, cursor)
        logger = netsvc.Logger()
        res_config = self.pool.get('res.config')
        disable_constraint = int(res_config.get(
            cursor, 1, 'disable_check_comptador_actiu_unic', '0'
        ))
        for idx, cnt in enumerate(self._constraints[:]):
            if (cnt[0].func_name == '_check_comptador_actiu_unic' and
                    disable_constraint):
                logger.notifyChannel('sync', netsvc.LOG_INFO,
                                     'Removing constraint %s pointer %s' % (
                                       cnt[0].func_name, id(cnt)
                                     ))
                del self._constraints[idx]


GiscedataLecturesComptador()
