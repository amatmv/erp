# -*- coding: utf-8 -*-
from osv import osv, fields
import base64
import datetime
import os


class GiscesoaSyncUploadFile(osv.osv_memory):

    _name = 'giscesoa.sync.upload.file'

    _KEY_DIR_ENTRADA= "directori_entrada"
    _KEY_SUFIX_IMM = "sufix_immediata"
    _DEFAULT_FILENAME = "wizard"
    _DATE_FORMAT = "%Y%m%dT%H%M%S"

    _columns = {
        'file': fields.binary('Fitxer'),
        'missatge': fields.text('Registre de càrrega'),
        'filename': fields.char('Filename', size=256),
        'state': fields.char('State', size=4)
    }

    _defaults = {
        'missatge': lambda *a: "Tria el fitxer per carregar.",
        'state': lambda *a: 'init',
    }

    def read_sync_config(self, cursor, uid, key, context):
        """
        Read config parameter with the given key
        :param cursor: database cursor
        :param uid: user id
        :param key: name of config field
        :type: string
        :param context:
        :return: value of the given key
        :rtype: string
        """
        sync_model = self.pool.get('giscesoa.sync.config')
        id_config = (sync_model.search(cursor, uid, [('name', '=', key)], context=context))
        config_value = sync_model.read(cursor, uid, id_config, ['value'])
        value = config_value[0].get('value')
        return value

    def write_file_to_dir(self, target_dir, file_decoded, filename):
        """
        Writes file in the given directory
        :param target_dir: directory where the file will be created
        :param file_decoded: file data
        :param filename: name with the file will be created
        :return: nothing
        """
        with open(os.path.join(str(target_dir), str(filename)), "w") as f:
            f.write(file_decoded)

    def wakeup(self, cursor, uid):
        """
        Call remote 'wakeup' function
        :param cursor: database cursor
        :param uid: user id
        :return: result of the wakeup function
        :rtype: Boolean
        """
        try:
            return self.pool.get('giscesoa.sync.fitxer').wakeup(cursor, uid)
        except Exception as e:
            print e
            return False

    def generate_file_name(self, cursor, uid, context):
        """

        :param cursor:
        :param uid:
        :param context:
        :return:
        """
        filename = ""
        current_date = datetime.datetime.now().strftime(self._DATE_FORMAT)
        file_extension = self.read_sync_config(cursor, uid, self._KEY_SUFIX_IMM, context)

        if file_extension:
            filename += str(self._DEFAULT_FILENAME) + "_" + str(current_date) + str(file_extension)
            return filename
        else:
            return False

    def load_file_master(self, cursor, uid, wiz_id, context):
        """

        :param cursor:
        :param uid:
        :param wiz_id:
        :param context:
        :return:
        """
        wizard_message = ""
        wiz_obj = self.pool.get('giscesoa.sync.upload.file')
        br_wiz = wiz_obj.browse(cursor, uid, wiz_id[0])
        file_data = br_wiz.file
        file_decoded = base64.decodestring(file_data)

        filename = self.generate_file_name(cursor, uid, context)
        target_dir = self.read_sync_config(cursor, uid, self._KEY_DIR_ENTRADA, context)

        if target_dir and filename:
            self.write_file_to_dir(target_dir, file_decoded, filename)
            wizard_message += "S'ha carregat el fitxer.\n"

            a = self.wakeup(cursor, uid)
            b = self.wakeup(cursor, uid)

            if a and b:
                wizard_message += "S'ha sincronitzat correctament.\n"
            else:
                wizard_message += "No s'ha sincronitzat correctament.\n"
        else:
            wizard_message += "No s'ha carregat el fitxer.\n"
            if not target_dir:
                wizard_message += str("No s'ha trobat el paràmetre de configuració: {}\n").format(self._KEY_DIR_ENTRADA)
            if not filename:
                wizard_message += str("No s'ha trobat el paràmetre de configuració: {}\n").format(self._KEY_SUFIX_IMM)

        br_wiz.write({'missatge': wizard_message})


GiscesoaSyncUploadFile()
