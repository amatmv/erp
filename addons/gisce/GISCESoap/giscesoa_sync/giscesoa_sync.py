# -*- coding: utf-8 -*-
from osv import osv, fields
from glob import glob
from xml.sax.saxutils import escape
from fuzzywuzzy import process
from fuzzywuzzy.utils import full_process
from oorq.decorators import job
from rq import get_current_job
from datetime import datetime, date, timedelta
from comers import COMERS
from tools.translate import _

import base64
import time
import os
import shutil
import re
import netsvc
import pooler
import traceback



def _log_error(message):
    logger = netsvc.Logger()
    logger.notifyChannel('objects', netsvc.LOG_ERROR,
                         'giscesoa_sync:%s.' % message)


def _progress_report(cr, uid, csv_file, num_linia):
    logger = netsvc.Logger()
    logger.notifyChannel('objects', netsvc.LOG_INFO,
                         'giscesoa_sync:Progress:%s:%s.' % (csv_file,
                                                            num_linia))
default_timeout = 360
custom_timeout = 360

# el camp 'state' ha de contenir sempre un d'aquests valors
# sembla que OpenObject no ho comprova, i el que es clar a
# hores d'ara es que aquesta variable no te cap influencia
# a la navegacio pel workflow, excepte quan es consulta a
# les condicions explicitament.
_wkf_linia_states = [
    ('st_esperar_dades', 'st_esperar_dades'),
    ('st_dividir_linia:OK', 'st_dividir_linia:OK'),
    ('st_dividir_linia:ERROR', 'st_dividir_linia:ERROR'),
    ('st_validacio_general:OK', 'st_validacio_general:OK'),
    ('st_validacio_general:ERROR', 'st_validacio_general:ERROR'),
    ('st_cursar_baixa_polissa:OK', 'st_cursar_baixa_polissa:OK'),
    ('st_cursar_baixa_polissa:ERROR', 'st_cursar_baixa_polissa:ERROR'),
    ('st_crear_cups:OK', 'st_crear_cups:OK'),
    ('st_crear_cups:ERROR', 'st_crear_cups:ERROR'),
    ('st_actualitzar_cups:OK', 'st_actualitzar_cups:OK'),
    ('st_actualitzar_cups:ERROR', 'st_actualitzat_cups:ERROR'),
    ('st_crear_client:OK', 'st_crear_client:OK'),
    ('st_crear_client:ERROR', 'st_crear_client:ERROR'),
    ('st_actualitzar_client:OK', 'st_actualitzar_client:OK'),
    ('st_actualitzar_client:ERROR', 'st_actualitzar_client:ERROR'),
    ('st_crear_polissa:OK', 'st_crear_polissa:OK'),
    ('st_crear_polissa:ERROR', 'st_crear_polissa:ERROR'),
    ('st_actualitzar_polissa:OK', 'st_actualitzar_polissa:OK'),
    ('st_actualitzar_polissa:ERROR', 'st_actualitzar_polissa:ERROR'),
    ('st_comprovar_comptador:OK', 'st_comprovar_comptador:OK'),
    ('st_comprovar_comptador:ERROR', 'st_comprovar_comptador:ERROR'),
    ('st_baixa_comptador:OK', 'st_baixa_comptador:OK'),
    ('st_baixa_comptador:ERROR', 'st_baixa_comptador:ERROR'),
    ('st_crear_comptador:OK', 'st_crear_comptador:OK'),
    ('st_crear_comptador:ERROR', 'st_crear_comptador:ERROR'),

    ('st_crear_butlleti:OK', 'st_crear_butlleti:OK'),
    ('st_crear_butlleti:ERROR', 'st_crear_butlleti:ERROR'),
    ('st_actualitzar_butlleti:OK', 'st_actualitzar_butlleti:OK'),
    ('st_actualitzar_butlleti:ERROR', 'st_actualitzar_butlleti:ERROR'),

    ('st_crear_icp:OK', 'st_crear_icp:OK'),
    ('st_crear_icp:ERROR', 'st_crear_icp:ERROR'),
    ('st_actualitzar_icp:OK', 'st_actualitzar_icp:OK'),
    ('st_actualitzar_icp:ERROR', 'st_actualitzar_icp:ERROR'),

    ('st_exception:ERROR', 'st_exception:ERROR'),

    ('st_tot_fet', 'Línea procesada'), # estat final que fa servir el workflow de fitxer
    ('st_error', 'Errores procesando línea') # estat final que fa servir el workflow de fitxer
]

_wkf_fitxer_states = [
    ('st_esperar_dades', 'st_esperar_dades'),
    ('st_processar_linies:OK', 'st_processar_linies:OK'),
    ('st_processar_linies:ERROR', u'Error procesando las líneas'),
    ('st_linies_processades:OK', 'st_linies_processades:OK'),
    ('st_netejar_polisses:OK', 'st_netejar_polisses:OK'),
    ('st_netejar_polisses:ERROR', u'Error limpiando las pólizas'),
    ('st_escriure_resultats:OK', 'st_escriure_resultats:OK'),
    ('st_escriure_resultats:ERROR', u'Error escribiendo los resultados'),
    ('st_tot_fet', u'Fichero procesado'),
]

_extensions = {'LOCK': '.lock', 'END': '.end'}

TARIFES_INDEX = {}



class GiscesoaSyncConfig(osv.osv):
    """
    Aquest model es podria treure fora de giscesoa_sync per fer-lo servir a
    altres llocs a on sigui convenient un sistema semblant als 'properties'
    de java
    """

    _name = 'giscesoa.sync.config'
    _modelname = 'giscesoa.sync.config'
    _description = u'Configuració'
    _debug = None
    _order = 'ordre'

    _columns = {
        'module': fields.char('Module', size=128, required=True),
        'name': fields.char('Key', size=128, required=True),
        'value': fields.text('Value'),
        'description': fields.text('Usage'),
        'ordre': fields.integer('Order'),
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
         'Aquesta configuracio ja existeix.'),
    ]

    def get_properties(self, cr, uid, module, key, operator='='):
        if operator not in ['=', 'like', 'ilike']:
            raise Exception(
                "Parameter 'operator' must be one of '=', 'like', 'ilike'; "
                "The given value '%s' is not."
                            % (operator))
            # Deixem canviar les variables en temps d'execució
        try:
            cr2 = None
            cr2 = pooler.get_db_only(cr.dbname).cursor()
            c_config = pooler.get_pool(cr.dbname).get('giscesoa.sync.config')
            row_ids = c_config.search(cr2, uid, [('module', '=', module), ('name', operator, key)])
            rows = c_config.read(cr2, uid, row_ids, ['name', 'value'])
            vals = {}
            for row in rows:
                vals[row['name']] = row['value']
            return vals
        finally:
            if cr2:
                cr2.close()

    def get_property(self, cr, uid, module, key, default=False):
        vals = self.get_properties(cr, uid, module, key)
        return vals.get(key, default)


GiscesoaSyncConfig()


class GiscesoaSyncFitxer(osv.osv):
    _name = 'giscesoa.sync.fitxer'
    _modelname = 'giscesoa.sync.fitxer'
    _description = u'Fichero para importar sistema de gestión externo'
    _debug = None

    _order = "name desc"

    _columns = {
        'name': fields.datetime(u"Fecha de importación"),
        'directori': fields.char('Directorio', size=1024),
        'nom_fitxer': fields.char('Nombre del fichero', size=128, select=True),
        #'data_fitxer': fields.date("Data del fitxer"),
        'tipus_proces': fields.selection([('cap', "No especificada"), ('immediata', u"Sincronitzación immediata"),
                                          ("massiva", u"Sincronitzación masiva")], string=u"Tipo de sincronitzación"),
        'linies': fields.one2many('giscesoa.sync.linia', 'name', u'Líneas'),

        'linies_totals': fields.integer("TOTAL"),
        'linies_ok': fields.integer("OK"),
        'linies_error': fields.integer("ERROR"),
        'delimitador': fields.char('Delimintador', size=1),

        # ESTAT INTERN
        'state': fields.selection(_wkf_fitxer_states, 'Estado', readonly=True, select=True),
        'status_message': fields.text('Mensaje de estado'),
    }

    _defaults = {
        'tipus_proces': lambda *a: 'cap',
        'nom_fitxer': lambda *a: False,
        'state': lambda *a: 'st_esperar_dades',
        'linies_totals': lambda *a: False,
        'linies_ok': lambda *a: False,
        'linies_error': lambda *a: False,
    }

    _TIPUS_PROCES = {'IMMEDIATA': 'immediata',
                     'CAP': 'cap',
                     'MASSIVA': 'massiva'}
    _posicio_linia = {
        'NIF': 9,
        'COMER': 24,
        'NOM': 10,
        'CUPS': 0,
        'DATA_ALTA': 17,
        'DATA_BAIXA': 19,
    }

    # TODO : apply when finished testing
    # _sql_constraints = [('nom_fitxer_unic', 'unique(nom_fitxer)',
    #  'Ja s\'havia processat un fitxer amb aquest mateix nom')]

    # ------------------------------------
    # wh_* -- WORKFLOW HELPERS :)
    # ------------------------------------

    def wh_get_property(self, cr, uid, key, default=False):
        c_config = self.pool.get('giscesoa.sync.config')
        return c_config.get_property(cr, uid, 'giscesoa_sync', key, default)

    def wh_assert(self, state, possible_states):
        logger = netsvc.Logger()
        if not state in possible_states:
            import inspect

            file = inspect.stack()[1][1]
            line = inspect.stack()[1][2]
            function = inspect.stack()[1][3]
            message = u"%s:%d: ASSERTION FAILED: workflow %s -- la función %s se ha llamado con state='%s' y se esperava únicamente uno de estos : ['" % (
                file, line, self._modelname, function, state)
            message += "', '".join(possible_states)
            message += "]."
            logger.notifyChannel('objects', netsvc.LOG_INFO,
                                 'giscesoa_sync:%s.' % message)

    def wh_add_status_message(self, state, message, vals):
        assert 'status_message' in vals.keys()
        if vals['status_message']:
            message = "\n".join([vals['status_message'], message])
        vals.update({'state': state, 'status_message': message})

    def wh_eliminar_wkf_instances(self, cursor, uid, id_linia):
        wkf_instance_obj = self.pool.get('workflow.instance')
        wkf_instance_id = wkf_instance_obj.search(cursor, uid,
                                                  [('res_id', '=', id_linia),
                                                  ('res_type','=','giscesoa.sync.linia')])
        wkf_instance_obj.unlink(cursor, uid, wkf_instance_id)

    def wh_linies_processades(self, cr, uid):
        fitxer_obj = self.pool.get('giscesoa.sync.fitxer')

        param_search = [('state', 'in', ('st_linies_processades:OK',
                                         'st_processar_linies:OK'))]

        fitxer_list = fitxer_obj.search(cr, uid, param_search)

        for fit_id in fitxer_list:
            cursor2 = pooler.get_db_only(cr.dbname).cursor()
            try:
                wkf_service = netsvc.LocalService('workflow')
                wkf_service.trg_validate(uid, 'giscesoa.sync.fitxer', fit_id,
                                         'check_processades', cursor2)
                cursor2.commit()
            except:
                cursor2.rollback()
            finally:
                cursor2.close()

    @job(queue='soa', timeout=custom_timeout)
    def wh_inici_proces(self, cr, uid, lines_id, vals):
        if get_current_job():
            fitxer_obj = self.pool.get('giscesoa.sync.fitxer')
            while not fitxer_obj.search(cr, uid, [('id', '=', vals['name'])]):
                time.sleep(1)
        wkf_service = netsvc.LocalService('workflow')
        for linia_id in lines_id:
            wkf_service.trg_validate(uid, 'giscesoa.sync.linia',
                                     linia_id, 'inici_proces', cr)
            self.wh_eliminar_wkf_instances(cr, uid, linia_id)

    def wh_crear_linia(self, cr, uid, list_vals):
        c_linia = self.pool.get('giscesoa.sync.linia')
        lines_id = []
        for vals in list_vals:
            lines_id.append(c_linia.create(cr, uid, vals))
        self.wh_inici_proces(cr, uid, lines_id, list_vals[-1])

    def wh_crear_comercialitzadores(self, cr, uid, comers):
        partner_obj = self.pool.get('res.partner')
        comers_id = partner_obj.search(cr, uid, [('ref', 'in', comers)])
        comers_ref = [x['ref'] for x in
                      partner_obj.read(cr, uid, comers_id,['ref'])]
        for comer_codi in [x for x in comers if x not in comers_ref]:
            if comer_codi in COMERS:
                partner_obj.create(cr, uid,
                                   {'name': COMERS.get(comer_codi),
                                    'ref': comer_codi,
                                    'supplier': 1})

    def wh_detectectar_delimitador(self, path_file_csv):
        with open(path_file_csv, 'r') as file_csv:
            header = file_csv.readline()
            if header.find("\t") != -1:
                return '\t'
            if header.find(';') != -1:
                return ';'
        return '\t'

    def wh_check_fitxer_creat(self, path_file_csv):
        time1 = os.path.getmtime(path_file_csv)
        time.sleep(1)
        time2 = os.path.getmtime(path_file_csv)
        return time1 == time2

    def wh_enviar_notificacions(self, cr, uid, filepath_informe, errors,
                                text_resultat, nom_fitxer, vals):
        """
        Sends a report with the results of the SOA proces

        :param cr: Database cursor
        :param uid: User id
        :param filepath_informe: Path of the file that been loaded
        :param errors: Flag of errors
        :param text_resultat: Results of the proces
        :param nom_fitxer: Name of the file loaded
        :param vals:
        :return: None
        :rtype: None
        """
        # mirem qui vol ser notificat i en quin cas (errors, ok)
        informe_ok_gisce = self.wh_get_property(cr, uid, "informe_ok_gisce")
        informe_ok_client = self.wh_get_property(cr, uid, "informe_ok_client")
        informe_error_gisce = self.wh_get_property(cr, uid, "informe_error_gisce")
        informe_error_client = self.wh_get_property(cr, uid, "informe_error_client")

        # si el cas que ens afecta no necessita notificacio, ho deixem còrrer
        if not (informe_ok_gisce or informe_ok_client or (errors and (informe_error_gisce or informe_error_client))):
            self.wh_add_status_message('st_escriure_resultats:OK',
                                       u"La configuración indica que no se ha de enviar ninguna notificación.", vals)
            return

        # quin servidor de correu farem servir per enviar les notificacions?
        smtphost = self.wh_get_property(cr, uid, "email_notificacio_smtp_host")
        if not smtphost:
            raise Exception(
            u"La configuración de los destinatarios de GISCE es inconsistente (clave 'email_notificacio_smtp_host').")

        # preparem la llista de BCC
        bcc = []
        if informe_ok_gisce or (errors and informe_error_gisce):
            destinataris_gisce = self.wh_get_property(cr, uid, "email_notificacio_gisce_adreca")
            if not destinataris_gisce: raise Exception(
                u"La configuración de los destinatarios de GISCE es inconsistente (clave 'email_notificacio_gisce_adreca').")
            destinataris_gisce = destinataris_gisce.split('\n')
            for adreca in destinataris_gisce:
                adreca = adreca.strip()
                if len(adreca): bcc.append(adreca)
            if not bcc: raise Exception(
                u"La configuración de los destinatarios de GISCE es inconsistente (clave 'email_notificacio_gisce_adreca').")

        # preparem la llista de destinataris
        to = []
        if informe_ok_client or (errors and informe_error_client):
            destinataris_client = self.wh_get_property(cr, uid, "email_notificacio_client_adreca")
            if not destinataris_client: raise Exception(
                u"La configuración de los destinatarios de GISCE es inconsistente (clave 'email_notificacio_client_adreca').")
            destinataris_client = destinataris_client.split('\n')
            for adreca in destinataris_client:
                adreca = adreca.strip()
                if len(adreca): to.append(adreca)
            if not to: raise Exception(
                u"La configuración de los destinatarios de GISCE es inconsistente (clave 'email_notificacio_client_adreca').")

        # preparem l'assumpte
        subject = self.wh_get_property(cr, uid, "email_notificacio_assumpte")
        if not subject: raise Exception(
            u"La configuración de las notificaciones es inconsistente (clave 'email_notificacio_assumpte').")
        subject = subject.replace('%r', text_resultat).replace('%f', nom_fitxer).replace("%d", cr.dbname)

        # preparem el cos del missatge
        body = self.wh_get_property(cr, uid, "email_notificacio_client_explicacio")
        if not body: raise Exception(
            u"La configuración de las notificaciones es inconsistente (clave 'email_notificacio_client_explicacio').")
        body = body.replace('%r', text_resultat).replace('%f', nom_fitxer).replace("%d", cr.dbname)

        # preparem el remitent
        sender = self.wh_get_property(cr, uid, "email_notificacio_remitent")
        if not sender: raise Exception(
            u"La configuración de las notificaciones es inconsistente (clave 'email_notificacio_remitent').")

        # ensamblarem el correu en un objecte de tipus MIMEMultipart
        import smtplib
        from email.MIMEMultipart import MIMEMultipart
        from email.MIMEBase import MIMEBase
        from email.MIMEText import MIMEText
        from email.Utils import COMMASPACE, formatdate
        from email import Encoders
        import os

        msg = MIMEMultipart()
        msg['Subject'] = subject
        msg['From'] = sender
        msg['To'] = COMMASPACE.join(to)
        msg['Bcc'] = COMMASPACE.join(bcc)
        msg.attach(MIMEText(body, 'plain', 'UTF-8'))

        # attachment
        part = MIMEBase('text', "html")
        part.set_payload(open(filepath_informe, "rb").read(), 'UTF-8')
        # Encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename="%s"' % os.path.basename(filepath_informe))
        msg.attach(part)

        # enviem el missatge
        smtp_tsl = self.wh_get_property(cr, uid, "smtp_ttsl_enable", False)
        smtp_tsl_cert = self.wh_get_property(cr, uid, "smtp_ttsl_cert", False)
        smtp_tsl_key = self.wh_get_property(cr, uid, "smtp_ttsl_key", False)
        smtp_user = self.wh_get_property(cr, uid, "smtp_login_user", False)
        smtp_passwd = self.wh_get_property(
            cr, uid, "smtp_login_password", False)
        smtp_login = smtp_user and smtp_passwd
        try:
            s = smtplib.SMTP(smtphost)
            if smtp_tsl:
                if not os.path.isfile(smtp_tsl_key):
                    raise Exception(
                        _('TLS Key file not found: {}').format(smtp_tsl_key))
                if not os.path.isfile(smtp_tsl_cert):
                    raise Exception(
                        _('TLS Cert file not found: {}').format(smtp_tsl_key))
                s.starttls(keyfile=smtp_tsl_key, certfile=smtp_tsl_cert)
            if smtp_login:
                s.login(user=smtp_user, password=smtp_passwd)
            s.sendmail(sender, to + bcc, msg.as_string())
            s.quit()
            self.wh_add_status_message('st_escriure_resultats:OK', "Se ha enviado el mensaje al servidor de correo.",
                                       vals)
        except Exception as send_error:
            self.wh_add_status_message(
                'st_escriure_resultats:OK',
                "Se ha producido un error durante el envio del informe al servidor de correu.",
                vals)
            sentry = self.pool.get('sentry.setup')
            if sentry is not None:
                sentry.client.captureException()
            _log_error('SMTP send error: {}'.format(send_error))

    # ------------------------------------
    # wc_* -- WORKFLOW CONDITION functions
    # ------------------------------------

    def wc_error(self, cr, uid, ids):
        """
        Workflow condition to check if there is any error

        :param cr: Database cursor
        :param uid: User id
        :param ids: Affected ids of the workflow
        :return: If there is any error
        :rtype: bool
        """
        obj = self.browse(cr, uid, ids)[0]
        #print "**** %s:%d: call to     wc_error" % (obj._modelname, obj.id)
        return obj.state and obj.state.endswith(":ERROR")

    def wc_linies_processades(self, cr, uid, ids):
        fitxer = self.browse(cr, uid, ids)[0]
        linia_obj = self.pool.get('giscesoa.sync.linia')
        param_search = [('name', '=', fitxer.id),
                        ('state', 'in', ('st_tot_fet', 'st_error'))]
        linia_list = linia_obj.search(cr, uid, param_search)

        if len(linia_list) == len(fitxer.linies):
            return True
        else:
             return False

    def wc_proces_massiu(self, cursor, uid, ids):
        fitxer = self.browse(cursor, uid, ids)[0]
        return fitxer.tipus_proces == self._TIPUS_PROCES['MASSIVA']



    # ---------------------------------
    # wa_* -- WORKFLOW ACTION functions
    # ---------------------------------
    @job(queue='soa')
    def wh_baixa_polissa(self, cursor, uid, fitxer_id, pol_id, data_baixa):
        linia_obj = self.pool.get('giscesoa.sync.linia')
        fields_to_read = ['status_message', 'id']
        fitxer = self.read(cursor, uid, fitxer_id, fields_to_read)
        vals = {'status_message': fitxer['status_message']}
        cursor2 = pooler.get_db_only(cursor.dbname).cursor()
        try:
            vals_pol = {'status_message': ''}
            linia_obj.wh_cursar_baixa_polissa(
                cursor2, uid, pol_id, data_baixa, vals_pol)
            cursor2.commit()
        except:
            cursor2.rollback()
            self.wh_add_status_message('',
                                       "ERROR: polissa_id %s exception %s " %
                                       (pol_id, traceback.format_exc())
                                       , vals)
            del vals['state']
            self.write(cursor, uid, fitxer_id, vals)
        finally:
            cursor2.close()


    def wa_netejar_polisses(self, cursor, uid, ids):
        fields_to_read = ['status_message', 'id']
        fitxer = self.read(cursor, uid, ids, fields_to_read)[0]
        vals = {'status_message': fitxer['status_message']}

        polissa_obj = self.pool.get('giscedata.polissa')
        dates = self.perm_read(cursor, uid, [fitxer['id']])
        # Utilitzem la data de creació per saber les pòlisses que no s'han
        # actualitzat durant el procés
        data_inici = dates[0]['create_date']
        data_baixa = data_inici.split()[0]

        del_pol_ids = polissa_obj.search(cursor, uid, [('write_date', '!=', False),
                                          ('write_date', '<', data_inici)])
        if len(del_pol_ids):
            for pol_id in del_pol_ids:
                self.wh_baixa_polissa(cursor, uid, fitxer['id'], pol_id, data_baixa)

        self.wh_add_status_message('st_netejar_polisses:OK',
                                   u'pólizas de baja %s' % len(del_pol_ids),
                                   vals)
        self.write(cursor, uid, fitxer['id'], vals)
        return True

    def pre_process_file(self, cursor, uid, file_id, file_path):
        return True

    def post_process_file(self, cursor, uid, file_id, file_path):
        return True

    def order_lines(self, cursor, uid, sorted_dic, vals_line):
        '''
        :param cursor:
        :param uid:
        :param sorted_dic:
        :param line:
        :return:
        '''
        line = vals_line['linia_original']
        delimitador = vals_line['delimitador']
        nom = (line.split(delimitador)[self._posicio_linia['NOM']]).replace(
            ' ', '')
        nif = line.split(delimitador)[self._posicio_linia['NIF']]
        nif_nom = nif + '_' + nom

        sorted_dic.setdefault(nif_nom, [])
        sorted_dic[nif_nom].append(vals_line)
        return True

    def wa_processar_linies(self, cr, uid, ids):
        logger = netsvc.Logger()
        obj = self.browse(cr, uid, ids)[0]
        #print "**** %s:%d: call to wa_processar_linies()" % (obj._modelname, obj.id)

        enc = self.wh_get_property(cr, uid, "encoding_fitxers_intercanvi")
        if not enc:
            raise Exception(
                u"giscesoa_sync: la configuración del módulo es incorrecta (clave 'encoding_fitxers_intercanvi').")


        csv_file = os.path.join(obj.directori, obj.nom_fitxer)
        if not os.path.exists(csv_file):
            raise Exception("Tiene que existir el fichero ANTES de cral el objecto giscesoa_sync_fitxer! ('%s')" % csv_file)

        self.pre_process_file(cr, uid, obj.id, csv_file)

        f = open(csv_file, 'rb')
        num_linia = 0
        linies = f.readlines()

        grouped_nifs = {}
        grouped_comers = []
        for line in linies:
            num_linia += 1
            if not line.startswith("#") and len(line.strip()) > 0:

                try:
                    line = unicode(line, enc)
                except:
                    raise Exception(
                        u"giscesoa_sync: problema con la codificación en la línea %d del fichero \"%s\" -- no es \"%s\"."
                        % (num_linia, csv_file, enc))

                vals = {
                    'ordre': num_linia,
                    'name': obj.id,
                    'linia_original': line,
                    'delimitador': obj.delimitador
                }
                self.order_lines(cr, uid, grouped_nifs, vals)

                comer = line.split(obj.delimitador)[
                    self._posicio_linia['COMER']].zfill(4)

                if comer not in grouped_comers:
                    grouped_comers.append(comer)


        f.close()

        self.wh_crear_comercialitzadores(cr, uid, grouped_comers)
        lengths = {key: len(value) for key,value in grouped_nifs.iteritems()}
        custom_timeout = default_timeout + (lengths[max(lengths)]//50)*default_timeout

        self.post_process_file(cr, uid, obj.id, csv_file)

        for nif, lines in grouped_nifs.items():
            self.wh_crear_linia(cr, uid, lines)

        os.rename(obj.directori+'/'+obj.nom_fitxer,
                  obj.directori+'/'+obj.nom_fitxer.replace(_extensions['LOCK'],
                                                           _extensions['END']))
        obj.write({'state': 'st_processar_linies:OK',
                   'nom_fitxer': obj.nom_fitxer.replace(_extensions['LOCK'],
                                                        _extensions['END'])})
        logger.notifyChannel('giscesoa_sync', netsvc.LOG_INFO,
                             'wa_processar_linies: custom timeout %s.' % custom_timeout)
        return True

    def wa_acabar_error(self, cr, uid, ids):
        obj = self.browse(cr, uid, ids)[0]
        #print "**** %s:%d: call to wa_acabar_error()" % (obj._modelname, obj.id)
        return True

    def wa_acabar_be(self, cr, uid, ids):
        obj = self.browse(cr, uid, ids)[0]
        #print "**** %s:%d: call to wa_acabar_be()" % (obj._modelname, obj.id)
        self.wh_assert(obj.state, ["st_escriure_resultats:OK"])
        return True





    def wa_escriure_resultats(self, cursor, uid, ids):

        attach_obj = self.pool.get('ir.attachment')

        fields_to_read = ['status_message', 'state', 'nom_fitxer',
                          'linies', 'name', 'id']
        obj = self.read(cursor, uid, ids, fields_to_read)[0]
        vals = {'status_message': obj['status_message']}
        self.wh_assert(obj['state'], ["st_netejar_polisses:OK",
                                      'st_processar_linies:OK' ])

        # busquem dades per construir path i nom dels fitxers de sortida
        out_path = self.wh_get_property(cursor, uid, "directori_sortida")
        if not out_path: raise Exception(u"Configuración inválida (clave 'directorio_salida')")
        if not os.path.isdir(out_path): raise Exception(
            u"Configuración inválida (clave 'directori_salida' no apunta a un directorio válido: '%s')" % (out_path))
        sufix_correctes = self.wh_get_property(cursor, uid, 'sufix_correctes')
        if not sufix_correctes: raise Exception(u"Configuración inválida (clave '%s')" % ('sufix_correctes'))
        sufix_errors = self.wh_get_property(cursor, uid, 'sufix_errors')
        if not sufix_errors: raise Exception(u"Configuración inválida (clave '%s')" % ('sufix_errors'))
        sufix_informes = self.wh_get_property(cursor, uid, 'sufix_informes')
        if not sufix_informes: raise Exception(u"Configuración inválida (clave '%s')" % ('sufix_informes'))

        # construim els noms absoluts dels fitxers de sortida
        filepath = os.path.join(out_path, obj['nom_fitxer'])
        filepath_ok = filepath + sufix_correctes
        filepath_error = filepath + sufix_errors
        filepath_informe = filepath + sufix_informes

        # preparem els blocs que serveixen per construir l'informe d'errors per línies
        c_linia = self.pool.get('giscesoa.sync.linia')
        informe_linies_inici = c_linia.informe_errors_inici()
        informe_linies_capcalera = c_linia.informe_errors_capcalera()
        informe_linies_peu = c_linia.informe_errors_peu()
        frequencia_capcalera = int(self.wh_get_property(cursor, uid, 'informe_linies_frequencia_capcalera')) or 1

        # iteració per cada línia, l'escriurem a fitxer que calgui en funció de si s'ha processat amb o sense errors
        informe_linies = ""
        count = 0
        count_ok = 0
        count_nok = 0
        text_resultat = "OK"
        fields_to_read = ['id',
                          'informe_errors_linia', 'linia_original', 'state']
        id_linies = []
        for linia in c_linia.read(cursor, uid, obj['linies'], fields_to_read):
            id_linies.append(linia['id'])
            count += 1
            if linia['state'] == "st_tot_fet":
                if not count_ok:
                    f_ok = open(filepath_ok, "w")

                #print "ok %d" % (linia.id)
                count_ok += 1
                f_ok.write(linia['linia_original'])

            else:
                if not count_nok:
                    informe_linies += informe_linies_inici + "\n"
                    f_error = open(filepath_error, "w")

                if not count_nok % frequencia_capcalera:
                    informe_linies += informe_linies_capcalera + "\n"

                #print "NOK %d" % (linia.id)
                count_nok += 1
                informe_linies += linia['informe_errors_linia'].replace('<@>', str(count_nok)) + "\n"
                f_error.write(linia['linia_original'])


        vals['linies_totals'] = count
        vals['linies_ok'] = count_ok
        vals['linies_error'] = count_nok

        if count_nok > 0:
            informe_linies += informe_linies_peu
            f_error.close()
            self.wh_add_status_message('st_escriure_resultats:OK',
                                       "Se ha generado un fichero de errores: %s" % filepath_error, vals)
            text_resultat = "%d ERRORES" % count_nok

        if count_ok > 0:
            f_ok.close()
            self.wh_add_status_message('st_escriure_resultats:OK',
                                       u"Se ha generado un fichero con las líneas correctas: %s" % filepath_ok, vals)

        titol = 'Resultado del fichero "%s" procesado %s' % (obj['nom_fitxer'], obj['name'])

        informe_fitxer = '<table border="1"><tr><th colspan=2 class="rf">%s</th></tr>\n' % titol
        informe_fitxer += '<tr><td class="rf">Líneas totales</td><td class="rf ar">%i</td></tr>\n' % count
        informe_fitxer += '<tr><td class="rf">Líneas procesadas sin errores</td><td class="rf ar">%i</td></tr>\n' % count_ok
        informe_fitxer += '<tr><td class="rf">Líneas con errores</td><td class="rf ar">%i</td></tr>\n' % count_nok
        informe_fitxer += '</table>'

        css = self.wh_get_property(cursor, uid, 'email_notificacio_css') or ""
        capcalera_html = "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>"
        capcalera_html += "<title>%s</title>" % titol
        capcalera_html += "<style>\n%s\n</style></head><body>" % css
        peu_html = "</body></html>"

        # escrivim l'informe al fitxer corresponent
        f_informe = open(filepath_informe, "w")
        f_informe.write(capcalera_html)
        f_informe.write(informe_fitxer)
        if count_nok:
            f_informe.write("<br>\n")
            f_informe.write(informe_linies)
        f_informe.write(peu_html)
        f_informe.close()

        data = base64.b64encode(open(filepath_informe,'r').read())
        attach_obj.create(cursor, uid, {'name': filepath_informe.split('/')[-1],
                                    'datas': data,
                                    'res_id': obj['id'],
                                    'res_model': self._name})

        self.wh_add_status_message('st_escriure_resultats:OK',
                                   u"Se ha generado un fichero con el informe del proceso: %s" % filepath_informe, vals)

        # enviar notificacions segons es demana a la configuració
        self.wh_enviar_notificacions(cursor, uid, filepath_informe, count_nok > 0, text_resultat, obj['nom_fitxer'], vals)

        # acabar el proces
        self.wh_add_status_message('st_escriure_resultats:OK', "Fichero procesado", vals)
        self.write(cursor, uid, obj['id'], vals)

        return True

    def fitxer_duplicat(self, cr, uid, filename):
        c_fitxer = self.pool.get('giscesoa.sync.fitxer')
        return c_fitxer.search(cr, uid, [('nom_fitxer', '=', filename)])

    # ---------------------------------
    # funcions per la tasca periodica que recull els fitxers d'intercanvi
    # (aquestes funcions no es fan servir des del workflow)
    # ---------------------------------

    def moure_fitxers_llegits(self, cr, uid, in_path, in_suffix, logger):

        filenames = glob(os.path.join(in_path, "*" + in_suffix + '.end'))
        for in_filepath in filenames:
            filename = os.path.split(in_filepath)[1]
            if not self.fitxer_duplicat(cr, uid, filename):
                pass
            else:
                dup_suffix = self.wh_get_property(cr, uid,
                                                  'sufix_fitxer_duplicat')
                if not dup_suffix: raise Exception(
                    u"Configuración inválida (clave '%s')" % (
                    'sufix_fitxer_duplicat'))

                out_path = self.wh_get_property(cr, uid, 'directori_sortida')
                if not out_path: raise Exception(
                    u"Configuración inválida (clave 'directori_sortida')")
                if not os.path.isdir(out_path): raise Exception(
                    u"Configuración inválida (clave 'directori_sortida' no apunta a un directorio válido: '%s')" % (
                        out_path))

                out_filepath = os.path.join(out_path, filename)
                n = len(glob(out_filepath + "*" + dup_suffix))
                if n:
                    dest_suffix = "." + str(n) + dup_suffix
                else:
                    dest_suffix = dup_suffix

                out_filepath += dest_suffix
                try:
                    shutil.move(in_filepath, out_filepath)
                    message = "Ya se había procesado un fichero con este nombre: '%s'; Se deja en el directrio de salida con el nombre'%s'" % (
                        filename, filename + dest_suffix)
                    logger.notifyChannel('objects', netsvc.LOG_INFO,
                                         'giscesoa_sync:%s.' % message)
                except Exception, e:
                    message = "Error while moving '%s' to '%s'" % (
                    in_filepath, out_filepath)
                    logger.notifyChannel('objects', netsvc.LOG_INFO,
                                         'giscesoa_sync:%s.' % message)
                    raise e

    def find_files_and_timestamps(self, cr, uid, tipus_proces):
        logger = netsvc.Logger()
        assert (tipus_proces == 'immediata' or tipus_proces == 'massiva')

        in_path = self.wh_get_property(cr, uid, "directori_entrada")
        if not in_path:
            raise Exception(u"Configuración inválida (clave 'directori_entrada')")
        if not os.path.isdir(in_path):
            raise Exception(u"Configuración inválida (clave 'directori_entrada' no apunta a un directorio valido: '%s')"
                            % (in_path))

        # trobem l'extensió dels fitxers que hem de buscar al directori d'entrada
        key = "sufix_" + tipus_proces
        in_suffix = self.wh_get_property(cr, uid, key)
        if not in_suffix: raise Exception(u"Configuración inválida (clave '%s')" % (key))

        # trobem el marge de temps que donem al generador del fitxer per haver acabat d'escriure'l
        key = "marge_minuts_sincronitzacio_%s" % (tipus_proces)
        marge = self.wh_get_property(cr, uid, key)
        if not marge or int(marge) < 1: raise Exception(u"Configuración inválida (clave '%s')" % (key))
        #XXX TODO: marge = int(marge) * 60 # volem el marge en segons
        marge = 0;

        #Movem els fitxers processats.
        self.moure_fitxers_llegits(cr, uid, in_path, in_suffix, logger)

        # volem processar els fitxers en ordre, però ni os.listdir ni glob.glob no
        # garanteixen cap ordre al resutat.
        # farem un diccionari 'candidats' amb un enter per clau i el nom com a valor
        # l'enter sera (100000 * mtime + index) a on index es l'index del fitxer a la
        # llista que retorna os.listdir, de manera que, si no trobem mes de 100K fitxers
        # no pot haver-hi col.lisions, i llavors podrem demanar a python que ordeni el
        # diccionari.
        candidats = {}

        #recuperem els fitxers que hem de processar.
        filenames = glob(os.path.join(in_path, "*" + in_suffix))
        now = int(time.mktime(datetime.now().timetuple())) # right now, as epoch
        i = 0
        for in_filepath in filenames:

            filename = os.path.split(in_filepath)[1] # split path and filename, and pick just the filename
            mtime = os.stat(in_filepath)[8] # last modification of file, as epoch

            if now >= mtime + marge:
                if not self.fitxer_duplicat(cr, uid, filename):
                    candidats[10000 * mtime + i] = in_filepath
                    i += 1
            else:
                message = "El fichero '" + in_filepath + "' aun no está 'bastante cocido': faltan %d segundos para que se pueda procesar." % (
                    mtime + marge - now)
                logger.notifyChannel('objects', netsvc.LOG_INFO,
                                     'giscesoa_sync:%s.' % message)

        result = []
        for key in sorted(candidats.iterkeys()):
            result.append(os.path.join(in_path, candidats[key]))

        return result

    # Aquesta funció s'ha de cridar periòdicament del del sistema d'scheduled tasks
    # d'OpenERP; En funció del que s'especifiqui al a giscesoa.sync.config i de la
    # data/hora actual es processaran els fitxers d'intercanvi dels directoris
    # compartits.

    def carregar_fitxers(self, cursor, uid, fitxers, tipus_proces, verbose):
        """

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param fitxers: Files to load
        :type fitxers: list of str
        :param tipus_proces: Type of  load (inmediat or )
        :type tipus_proces: str
        :param verbose: Verbosity of the load
        :type verbose: bool
        :return:
        """
        logger = netsvc.Logger()
        for filepath in fitxers:
            cursor2 = pooler.get_db_only(cursor.dbname).cursor()
            try:
                os.rename(filepath, filepath + _extensions['LOCK'])
                path, filename = os.path.split(filepath + _extensions['LOCK'])
                delimitador = self.wh_detectectar_delimitador(os.path.join(
                        path, filename
                    ))

                vals = {
                    'name': time.strftime("%Y-%m-%d %H:%M:%S"),
                    'nom_fitxer': filename,
                    # 'data_fitxer': data_fitxer,
                    'directori': path,
                    'tipus_proces': tipus_proces,
                    'delimitador': delimitador,
                }
                if verbose:
                    message = "giscesoa_sync: Fitxer trobat " + filepath
                    logger.notifyChannel('objects', netsvc.LOG_INFO,
                                         'giscesoa_sync:%s.' % message)
                self.create(cursor2, uid, vals)
                cursor2.commit()
            except Exception as e:
                if verbose:
                    trace = traceback.format_exc()
                    logger.notifyChannel('objects', netsvc.LOG_INFO,
                                     'giscesoa_sync:%s.' % trace)
                sentry = self.pool.get('sentry.setup')
                if sentry is not None:
                    sentry.client.captureException()
                cursor2.rollback()
                return False
            finally:
                cursor2.close()
        return True

    def wakeup(self, cr, uid):
        logger = netsvc.Logger()
        verbose = int(self.wh_get_property(cr, uid, 'verbose', False))
        self.wh_linies_processades(cr, uid)
        if verbose:
            message = u'giscesoa_sync: Mirem si hi ha cap fitxer per processar. Som l\'UID %d a la base ' \
                      u'de dades "%s".'% (uid, cr.dbname)
            logger.notifyChannel('objects', netsvc.LOG_INFO,
                                 'giscesoa_sync:%s.' % message)
        configurat = self.wh_get_property(cr, uid, 'configurat')
        if not configurat or configurat == "0":
            _log_error(u"AVIS: El mòdul giscesoa_sync encara no s'ha configurat. No es processarà cap fitxer.")
        else:
            # busquem fitxers per sincronitzacio immediata
            fitxers = self.find_files_and_timestamps(
                cr, uid, self._TIPUS_PROCES['IMMEDIATA'])
            ret = self.carregar_fitxers(
                cr, uid, fitxers, self._TIPUS_PROCES['IMMEDIATA'], verbose)
            # busquem fitxers per sincronitzacio massiva
            fitxers = self.find_files_and_timestamps(
                cr, uid, self._TIPUS_PROCES['MASSIVA']
            )
            self.carregar_fitxers(
                cr, uid, fitxers, self._TIPUS_PROCES['MASSIVA'], verbose)
        return ret

GiscesoaSyncFitxer()


class GiscesoaSyncLinia(osv.osv):
    _name = 'giscesoa.sync.linia'
    _modelname = 'giscesoa.sync.linia'
    _description = u'Líneas por importar del fichero del SOA'
    _debug = None

    # ----------------------------------------------------------------------------------------------
    # INDEX per relacionar els noms dels camps al model amb el numero de columna al CSV i viceversa
    # ### NO S'HAURIEN DE MODIFICAR MANUALMENT, AQUESTS VALORS VENEN DEL FULL DE CALCUL
    # ----------------------------------------------------------------------------------------------

    _csv_fields = {
        1: {'name': 'csv_cups_codi', 'size': 30, 'caption': u'CUPS', 'tipus': 'char'},
        2: {'name': 'csv_cups_municipi', 'size': 10, 'caption': u'Municipio', 'tipus': 'integer'},
        3: {'name': 'csv_cups_poblacio', 'size': 60, 'caption': u'Población', 'tipus': 'char'},
        4: {'name': 'csv_cups_carrer', 'size': 60, 'caption': u'Calle', 'tipus': 'char'},
        5: {'name': 'csv_cups_numero', 'size': 10, 'caption': u'Número', 'tipus': 'char'},
        6: {'name': 'csv_cups_escala', 'size': 50, 'caption': u'Escalera', 'tipus': 'char'},
        7: {'name': 'csv_cups_planta', 'size': 50, 'caption': u'Planta', 'tipus': 'char'},
        8: {'name': 'csv_cups_pis', 'size': 10, 'caption': u'Piso', 'tipus': 'char'},
        9: {'name': 'csv_abonat_codi', 'size': 60, 'caption': u'Código abonado', 'tipus': 'char'},
        10: {'name': 'csv_abonat_nif', 'size': 32, 'caption': u'NIF abonado', 'tipus': 'char'},
        11: {'name': 'csv_abonat_nom', 'size': 128, 'caption': u'Nombre cliente', 'tipus': 'char'},
        12: {'name': 'csv_abonat_telefon', 'size': 64, 'caption': u'Teléfono', 'tipus': 'char'},
        13: {'name': 'csv_abonat_fax', 'size': 64, 'caption': u'FAX', 'tipus': 'char'},
        14: {'name': 'csv_abonat_mail', 'size': 64, 'caption': u'Email', 'tipus': 'char'},
        15: {'name': 'csv_polissa_codi_comptador', 'size': 20, 'caption': u'Núm. contador', 'tipus': 'char'},
        16: {'name': 'csv_polissa_codi_contracte', 'size': 60, 'caption': u'Núm. póliza (contrato)', 'tipus': 'char'},
        17: {'name': 'csv_polissa_tarifa', 'size': 10, 'caption': u'Tarifa', 'tipus': 'char'},
        18: {'name': 'csv_polissa_data_alta', 'size': 10, 'caption': u'Fecha alta', 'tipus': 'date'},
        19: {'name': 'csv_polissa_donar_de_baixa', 'size': 1, 'caption': u'Baja', 'tipus': 'boolean'},
        20: {'name': 'csv_polissa_data_baixa', 'size': 10, 'caption': u'Fecha baja', 'tipus': 'date'},
        21: {'name': 'csv_polissa_potencia', 'size': 11, 'caption': u'Potencia', 'tipus': 'float'},
        22: {'name': 'csv_polissa_tensio', 'size': 10, 'caption': u'Tensión', 'tipus': 'char'},
        23: {'name': 'csv_polissa_activitat_cnae', 'size': 10, 'caption': u'Actividad CNAE', 'tipus': 'char'},
        24: {'name': 'csv_polissa_comercialitzadora', 'size': 4, 'caption': u'Comercializadora', 'tipus': 'char'},
        25: {'name': 'csv_polissa_observacions', 'size': 1024, 'caption': u'Observaciones', 'tipus': 'text'},
        26: {'name': 'csv_polissa_titular', 'size': 128, 'caption': u'Titular', 'tipus': 'char'},
        27: {'name': 'csv_polissa_estat', 'size': 60, 'caption': u'Estado de la póliza', 'tipus': 'char'},
        28: {'name': 'csv_polissa_distribuidora', 'size': 4, 'caption': u'Distribuidora', 'tipus': 'char'},
        29: {'name': 'csv_polissa_tipus_punt_mesura', 'size': 2, 'caption': u'Tipo punto medida', 'tipus': 'char'},
        30: {'name': 'csv_polissa_potencies_per_periode', 'size': 128, 'caption': u'Potencia por periodo', 'tipus': 'char'},

    }

    _reverse_csv_fields = {}
    for k in _csv_fields.keys():
        _reverse_csv_fields[_csv_fields[k]['name']] = k

    def _auto_init(self, cursor, context):
        res = super(GiscesoaSyncLinia, self)._auto_init(cursor, context)
        cursor.execute('''
            SELECT indexname
            FROM pg_indexes
            WHERE indexname = 'giscesoa_sync_linia_name_state_idx'
            ''')
        if not cursor.fetchone():
            cursor.execute('''
              CREATE INDEX giscesoa_sync_linia_name_state_idx
              ON giscesoa_sync_linia (name, state)
              ''')
            cursor.commit()
        return res

    # ------------------------------------
    # espècie d'"static functions"
    # ------------------------------------

    def informe_errors_inici(self):
        return '<table border="1">'

    def informe_errors_capcalera(self):
        result = u'<tr><th>Número de línea original</th><th>Número de línea el fichero de errores</th>'
        for index, details in self._csv_fields.iteritems():
            result += "<th>%s<br/><br/>%d</th>" % (escape(details['caption']), index)
        result += "</tr>"
        return result

    def informe_errors_peu(self):
        return '</table>'

    # ------------------------------------
    # camps funcio
    # ------------------------------------

    def _split_linia_original(self, linia_original, delimitador):
        #return [x if x else False for x in linia_original.split('\t')]
        return linia_original.split(delimitador)

    def _informe_errors_linia(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for obj in self.browse(cr, uid, ids):
            values = self._split_linia_original(obj.linia_original, obj.delimitador)
            result = '<tr><th rowspan="2">%d</th>' % obj.ordre
            result += '<th rowspan="2"><@></th>'
            for index, details in self._csv_fields.iteritems():
                #value = escape(getattr(obj, details['name'], "") or "")
                value = None
                if not index > len(values):
                    value = values[index - 1]
                if not value:
                    value = "&nbsp;"
                result += "<td>%s</td>" % (value)
            result += "</tr>"
            result += '<tr><td colspan="%d" class="msg"><ul><li>%s</li></ul></td></tr>' % (
                len(self._csv_fields), escape(obj.status_message).replace("\n", "</li><li>") )
            res[obj.id] = result
        return res

    # ----------------------
    # camps "normals"
    # ----------------------

    _columns = {
        # FITXER AL QUE PERTANY LA LINIA
        'name': fields.many2one('giscesoa.sync.fitxer', 'Nombre del fichero',
                                required=True, ondelete='cascade'),
        'ordre': fields.integer(u'Núm. línia'),
        # LINIA TAL COM ES RECUPERA DEL FITXER CSV
        'linia_original': fields.text(u'Linea original'),
         # ESTAT INTERN
         'state': fields.selection(_wkf_linia_states, 'Estado', readonly=True,
                                   select=True),
         'status_message': fields.text('Mensaje de estado'),

        # AJUTS PER INVESTIGAR POSSIBLES PROBLEMES
        # aquest camp no serveix de gaire res si fem servir l'aproximacio
        # de tenir-ho tot en mode 'text' unicament es una manera comoda
        # de poder mirar tots els camps amb un unic field al UI.
        'linia_dividida': fields.text('linia__dividida'),

        # Identificadors d'entitats que fem servir per estalviar cerques i
        # accesos a la base de dades durant l'execució del workflow
        'tmp_cups_id': fields.integer('tmp__cups__id'),
        'tmp_polissa_id': fields.integer('tmp__polissa__id'),
        'tmp_comercialitzadora_id': fields.integer('tmp_comercialitzadora_id'),
        'tmp_municipi_id': fields.integer('tmp__municipi__id'),
        'tmp_tarifa_id': fields.integer('tmp__tarifa__id'),
        'tmp_abonat_id': fields.integer('tmp__abonat__id'),
        'tmp_comptador_id': fields.integer('tmp_comptador_id'),
        'tmp_num_comptador': fields.text('tmp_num_comptador', size=20),

        'delimitador': fields.char('Delimintador', size=1),

        # Camps funcio
        'informe_errors_linia': fields.function(_informe_errors_linia,
                                                method=True, type='string',
                                                string="Informe de errors"),

    }

    # afegim els camps 'temporals' que corresponen a les columnes del fitxer csv
    for index, details in _csv_fields.iteritems():
        fnom = details['name']
        fcaption = details['caption']
        fsize = details['size']
        _columns[fnom] = fields.char(fcaption, size=fsize)

    _defaults = {
        'state': lambda *a: 'st_esperar_dades',
        'linia_original': lambda *a: False,
        'tmp_cups_id': lambda *a: -1,
        'tmp_polissa_id': lambda *a: -1,
        'tmp_municipi_id': lambda *a: -1,
        'tmp_tarifa_id': lambda *a: -1,
        'tmp_comptador_id': lambda *a: -1,
        'tmp_num_comptador': lambda *a: '',
    }

    # ------------------------------------
    # wh_* -- WORKFLOW HELPERS :)
    # ------------------------------------

    def wh_marcar_polissa(self, cursor, uid, codi_polissa):
        polissa_obj = self.pool.get('giscedata.polissa')
        id_pols = polissa_obj.search(cursor, uid, [('name', '=', codi_polissa)])
        if len(id_pols) > 0:
            polissa = polissa_obj.read(cursor, uid, id_pols[0], ['data_alta'])
            vals = {'data_alta': polissa['data_alta']}
            polissa_obj.write(cursor, uid, id_pols, vals)


    def wh_linia_error(self, linia):
        return linia.state and linia.state.endswith(":ERROR")
    def wh_vals_sense_error(self, vals):
        return not vals.get('status_message', '') or \
               not 'ERROR' in vals.get('status_message', '')

    def wh_parse_float(self,value):
        try:
            result = float(value.replace(',', '.').replace(' ',''))
        except:
            result = False
        return result

    def wh_fix_vat(self, vat):
        vat = ''.join([x for x in vat.upper() if x.isalnum()])
        if vat and vat[0].isdigit():
            vat = vat.zfill(9)
        if not vat.startswith('ES'):
            vat = 'ES%s' % vat
        return vat

    def wh_omplir_dades_generic(self, linia, fields, tipus):
        vals = {}
        for k, f in fields:
            value = getattr(linia, f % tipus, None)
            if value is not None:
                vals[k] = value
        return vals

    def wh_omplir_dades_partner_titular(self, cursor, uid, linia):
        tipus = 'abonat'
        fields = [('name', 'csv_%s_nom')]
        vals = self.wh_omplir_dades_generic(linia, fields, tipus)
        vals['vat'] = self.wh_fix_vat(linia.csv_abonat_nif)
        return vals

    def wh_omplir_dades_partner_address_titular(self, cursor, uid, partner_id, linia):
        tipus = 'abonat'
        fields = [('name', 'csv_%s_nom'), ('phone', 'csv_%s_telefon'),
                  ('fax', 'csv_%s_fax'), ('email', 'csv_%s_mail')]
        vals = self.wh_omplir_dades_generic(linia, fields, tipus)
        vals['partner_id'] = partner_id
        vals['type'] = 'default'
        return vals

    def wh_actualitzar_client(self, cr, uid, linia):
        self.wh_actualitzar_client_contacte(cr, uid, linia)
        c_client = self.pool.get('res.partner')
        #print " -- wh_actualitzar_client -- nom:%s -- nif:%s" % (linia.csv_abonat_nom, linia.csv_abonat_nif)
        vals = self.wh_omplir_dades_partner_titular(cr, uid, linia)
        c_client.write(cr, uid, [linia.tmp_abonat_id], vals)

    def wh_actualitzar_client_contacte(self, cr, uid, linia, client_id=None):
        """
        Worker helper to update the client contact

        :param cr: Database cursor
        :param uid: User id
        :type uid: int
        :param linia: Line of the file to process
        :type linia: brwoserecord
        :param client_id: Id of the client to update
        :return:Id of the client
        :rtype: int
        """
        # aquest mecanisme del client_id serveix per fer mes simple crear vs actualitzar
        if not client_id: client_id = linia.tmp_abonat_id

        c_partner_address = self.pool.get('res.partner.address')
        # inhabilitar adreces antigues
        old_addresses_ids = c_partner_address.search(
            cr, uid, [('partner_id', '=', client_id), ('type', '=', 'default'),
                      ('name', "=", linia.csv_abonat_nom)
                      ])
        # crear una nova adreça amb les dades que ens indiquen
        vals = self.wh_omplir_dades_partner_address_titular(
            cr, uid, client_id, linia)
        if old_addresses_ids:
            if len(old_addresses_ids) == 1:
                c_partner_address.write(cr, uid, old_addresses_ids, vals)
            else:
                c_partner_address.write(cr, uid, old_addresses_ids[0], vals)
                c_partner_address.write(cr, uid, old_addresses_ids[1:],
                                        {"active": 0})
            return old_addresses_ids[0]
        else:
            return c_partner_address.create(cr, uid, vals)

    def wh_actualitzar_cups(self, cr, uid, linia):
        c_cups = self.pool.get('giscedata.cups.ps')
        vals = self.wh_omplir_camps_cups(cr, uid, linia)
        c_cups.write(cr, uid, [linia.tmp_cups_id], vals)

    def wh_actualitzar_polissa(self, cr, uid, linia):
        polissa_obj = self.pool.get('giscedata.polissa')
        #Activem la modificació contractual
        wkf_service = netsvc.LocalService('workflow')
        polissa_actual = polissa_obj.browse(cr, uid, linia.tmp_polissa_id)

        if polissa_actual.state == 'baixa':
            wkf_service.trg_validate(uid, 'giscedata.polissa',
                                     linia.tmp_polissa_id,
                                     'reactivar', cr)
            polissa_actual = polissa_obj.browse(cr, uid, linia.tmp_polissa_id)


        wkf_service.trg_validate(uid, 'giscedata.polissa', linia.tmp_polissa_id,
                                 'modcontractual', cr)

        vals = self.wh_omplir_camps_polissa(cr, uid, linia)
        polissa_actual.write(vals)
        self.wh_actualitzar_potencia_polissa(cr, uid, linia)
        if polissa_actual.state == 'esborrany':
            self.wh_activar_polissa(cr, uid, linia)
            polissa_actual = polissa_obj.browse(cr, uid,
                                                linia.tmp_polissa_id)

        te_modcon = False
        canvis = None
        if polissa_actual.modcontractual_activa:
            canvis = polissa_actual.get_changes()
            te_modcon = True

        if te_modcon and not canvis:
            wkf_service.trg_validate(uid, 'giscedata.polissa', linia.tmp_polissa_id, 'undo_modcontractual', cr)
        else:
            try:
                wz_crear_mc_obj = self.pool.get('giscedata.polissa.crear.contracte')
                ctx = {'active_id': polissa_actual.id}
                params = {'duracio': 'nou'}

                wz_id = wz_crear_mc_obj.create(cr, uid, params, ctx)
                wiz = wz_crear_mc_obj.browse(cr, uid, wz_id, ctx)
                res = wz_crear_mc_obj.onchange_duracio(cr, uid,
                                                       [wz_id], wiz.data_inici,
                                                       wiz.duracio, ctx)
                if res.get('warning', False):
                    params = {'accio': 'modificar'}
                    wz_id = wz_crear_mc_obj.create(cr, uid, params, ctx)
                    wiz = wz_crear_mc_obj.browse(cr, uid, wz_id, ctx)
                    res = wz_crear_mc_obj.onchange_duracio(cr, uid,
                                                           [wz_id],
                                                           wiz.data_inici,
                                                           wiz.duracio, ctx)
                    wiz.action_crear_contracte(ctx)

                else:
                    wiz.write({'data_final': res['value']['data_final']})
                    wiz.action_crear_contracte(ctx)


            except Exception as e:
                sentry = self.pool.get('sentry.setup')
                if sentry is not None:
                    sentry.client.captureException()
                raise e


    def wh_add_status_message(self, state, message, vals):
        assert 'status_message' in vals.keys()
        if vals['status_message']:
            message = "\n".join([vals['status_message'], message])
        vals.update({'state': state, 'status_message': message})

    def wh_assert(self, state, possible_states):
        logger = netsvc.Logger()
        if not state in possible_states:
            try:
                import inspect
                file = inspect.stack()[1][1]
                line = inspect.stack()[1][2]
                function = inspect.stack()[1][3]
            except:
                file = 'unknown'
                line = 0
                function = 'unknown'
            message = u"%s:%d: ASSERTION FAILED: la función %s se ha llamado con state='%s' y se esperaba únicamente uno de estos: ['" % (
                file, line, function, state)
            message += "', '".join(possible_states)
            message += "]."
            logger.notifyChannel('objects', netsvc.LOG_INFO,
                                 'giscesoa_sync:%s.' % message)

    def wh_check_and_update_date_or_null(self, vals, str_date, camp, status_if_error):
        if str_date and len(str_date):
            if not self.wh_is_valid_date(str_date):
                self.wh_add_status_message(status_if_error,
                                           u"ERROR:El campo %d (%s) contiene \"%s\" pero ha de tener una fecha con el formato ISO 8601 (YYYY-MM-DD) o estar vacio" % (
                                               self._reverse_csv_fields[camp], camp, str_date), vals)
        else:
            vals[camp] = None

    def wh_comprovar_format_codi_cups(self, cursor, uid, cups):
        """Comprova que el codi CUPS és vàlid --- és una còpia
           de check_cups() al model giscedata_cups_ps al modul
           de la distribuidora"""
        if not cups: return False

        config = self.pool.get('res.config')
        try:
            check_cups = int(config.get(cursor, uid, 'check_cups', 1))
        except ValueError:
            check_cups = True
        if not check_cups:
            return True

        _cups_checksum_table = 'TRWAGMYFPDXBNJZSQVHLCKE'
        tmp = cups[2:18]
        try:
            r0 = int(tmp) % 529
        except:
            return False
        c = int(r0 / 23)
        r = r0 % 23
        check = 'ES%s%s%s' % (tmp, _cups_checksum_table[c], _cups_checksum_table[r])
        return check == cups[:20]

    def wh_crear_client(self, cr, uid, linia):
        c_client = self.pool.get('res.partner')
        #print " -- wh_crear_client -- nom:%s -- nif:%s" % (linia.csv_abonat_nom, linia.csv_abonat_nif)
        vals = self.wh_omplir_dades_partner_titular(cr, uid, linia)
        return c_client.create(cr, uid, vals)

    def wh_crear_cups(self, cr, uid, linia):
        c_cups = self.pool.get('giscedata.cups.ps')
        vals = self.wh_omplir_camps_cups(cr, uid, linia)
        vals['name'] = linia.csv_cups_codi
        return c_cups.create(cr, uid, vals)

    def wh_crear_polissa(self, cr, uid, linia):
        c_polissa = self.pool.get('giscedata.polissa')
        vals = self.wh_omplir_camps_polissa(cr, uid, linia)
        return c_polissa.create(cr, uid, vals)

    def wh_activar_polissa(self, cr, uid, linia):
        if linia.csv_polissa_estat != 'BORRADOR':
            wkf_service = netsvc.LocalService('workflow')
            wkf_service.trg_validate(uid, 'giscedata.polissa',
                                     linia.tmp_polissa_id, 'validar', cr)
            wkf_service.trg_validate(uid, 'giscedata.polissa',
                                     linia.tmp_polissa_id, 'contracte', cr)

    def wh_cups_amb_polissa_activa(self, cr, uid, id_cups):
        assert id_cups != -1
        c_cups = self.pool.get('giscedata.cups.ps')
        return c_cups._get_polissa(cr, uid, id_cups)

    def wh_cursar_baixa_polissa(self, cr, uid, id_polissa, str_data_baixa,
                                vals):
        polissa_obj = self.pool.get('giscedata.polissa')
        polissa = polissa_obj.browse(cr, uid, id_polissa)
        params = {}

        if polissa.active:
            for comptador in polissa.comptadors:
                if comptador.active:
                    if comptador.data_alta:
                        if datetime.strptime(comptador.data_alta, '%Y-%m-%d') > \
                                datetime.strptime(str_data_baixa, '%Y-%m-%d'):
                            self.wh_add_status_message("st_cursar_baixa_polissa:OK",
                                                   u"NOTIFICACIÓN:La fecha de baja es"
                                                   u" menor a la fecha de alta del "
                                                   u" contador, por tanto damos de baja "
                                                   u"el contador con la fecha de hoy.",
                                                   vals)
                            params = {'data_baixa': datetime.now().strftime('%Y-%m-%d'),
                                      'active': 0}
                            comptador.write(params)
                        else:
                            params = {'data_baixa': str_data_baixa,'active': 0}
                            comptador.write(params)
                    else:
                        params = {'data_alta': polissa.data_alta,
                                  'data_baixa': str_data_baixa, 'active': 0}
                        self.wh_add_status_message("st_cursar_baixa_polissa:OK",
                                                    u"NOTIFICACIÓN: La fecha de alta no existe"
                                                    u"por eso se pone como fecha de alta"
                                                    u" al contador la de la póliza",
                                                    vals)
                        comptador.write(params)


            data_baixa = str_data_baixa
            if polissa.modcontractual_activa:
                if datetime.strptime(polissa.modcontractual_activa.data_inici,
                                     '%Y-%m-%d') > datetime.strptime(str_data_baixa,
                                                                     '%Y-%m-%d'):
                    data_baixa = polissa.modcontractual_activa.data_inici

            polissa_obj.write(cr, uid, [id_polissa], {'data_baixa': data_baixa,
                                                      'renovacio_auto': 0})
            wkf_service = netsvc.LocalService('workflow')
            wkf_service.trg_validate(uid, 'giscedata.polissa',
                                     id_polissa, 'baixa', cr)

            polissa_obj.write(cr, uid, [id_polissa], {'data_baixa': str_data_baixa})
        else:
            self.wh_add_status_message("st_cursar_baixa_polissa:OK",
                                       u"NOTIFICACIÓN: La póliza ya estaba de baja",
                                       vals)


    def wh_get_abonat_id(self, cr, uid, str_nif, str_nom):
        c_partner = self.pool.get('res.partner')
        return c_partner.search(cr, uid, [('name', '=', str_nom),
                                          ('vat', '=', self.wh_fix_vat(
                                              str_nif))],
                                order='write_date desc, id desc')

    def wh_get_comercialitzadora_id(self, cr, uid, str_codi):
        c_partner = self.pool.get('res.partner')
        str_codi = str_codi.zfill(4)
        comer = c_partner.search(cr, uid, [('ref', '=', str_codi)])
        if not comer:
            if str_codi in COMERS:
                comer = c_partner.create(cr, uid,
                                         {'name': COMERS.get(str_codi),
                                          'ref': str_codi,
                                          'supplier': 1})
                comer = [comer]
        return comer

    def wh_get_cups(self, cr, uid, id):
        if id == -1: raise Exception(u"No se puede llamar a giscesoa_sync_linia.wh_get_cups() con un id -1!")
        c_cups = self.pool.get('giscedata.cups.ps')
        return c_cups.browse(cr, uid, id)[0]

    def wh_get_cups_id(self, cr, uid, cups_name):
        c_cups = self.pool.get('giscedata.cups.ps')
        ids = c_cups.search(cr, uid, [('name', '=', cups_name)], context={'active_test': False})
        if ids: return ids[0] # com a màxim hi pot haver un resultat, ja que el camp 'name' is unique
        return -1 # si no hi ha resultats, retornem aquest valor "especial" que vol dir que no hi es a la base de dades

    def wh_get_municipi_id(self, cr, uid, codi_ine_municipi):
        c_municipi = self.pool.get('res.municipi')
        ids = c_municipi.search(cr, uid, [('ine', '=', codi_ine_municipi)], context={'active_test': False})
        if ids:
            return ids[0] # com a màxim hi pot haver un resultat, ja que el camp 'name' is unique
        else:
            # si el codi te 6 digits, es probable que l'ultim digit sigui el checksum
            # llavors mentre no prenem la decissio d'actualitzar les nostres bases de
            # dades, farem la cerca fent servir el codi antic, sense el digit de ctrl
            # TODO: aquest nyap s'ha de treure d'aqui!!
            if len(str(codi_ine_municipi)) == 6:
                antic_codi_ine_municipi = codi_ine_municipi[0:5]
                ids = c_municipi.search(cr, uid, [('ine', '=', antic_codi_ine_municipi)],
                                        context={'active_test': False})
                if ids: return ids[0] # com a màxim hi pot haver un resultat, ja que el camp 'name' is unique
            if len(str(codi_ine_municipi)) == 4:
                ine_municipi =codi_ine_municipi.zfill(5)
                ids = c_municipi.search(cr, uid, [('ine', '=', ine_municipi)],
                                        context={'active_test': False})
                if ids: return ids[0] # com a màxim hi pot haver un resultat, ja que el camp 'name' is unique
        return -1 # si no hi ha resultats, retornem aquest valor "especial" que vol dir que no hi es a la base de dades

    def wh_get_polissa(self, cr, uid, id):
        if id == -1: raise Exception(u"No se puede llamar a giscesoa_sync_linia.wh_get_polissa() con un id -1!")
        c_polissa = self.pool.get('giscedata.polissa')
        return c_polissa.browse(cr, uid, id)[0]

    def wh_get_polissa_id(self, cr, uid, linia):
        """
        Returns the polissa id of agiven line

        :param cr: Database cursor
        :param uid: User id
        :type uid: int
        :rerturn: Polissa id of the line , otherways -1
        :rtype: int
        """

        polissa_name = linia.csv_polissa_codi_contracte
        c_polissa = self.pool.get('giscedata.polissa')
        ids = c_polissa.search(cr, uid, [('name', '=', polissa_name)], context={'active_test': False})
        if ids: return ids[0] # com a màxim hi pot haver un resultat, ja que el camp 'name' is unique
        return -1 # si no hi ha resultats, retornem aquest valor "especial" que vol dir que no hi es a la base de dades

    def wh_get_polissa_id_by_cups(self, cr, uid, id_cups):
        c_polissa = self.pool.get('giscedata.polissa')
        ids = c_polissa.search(cr, uid, [('cups', '=', id_cups)])
        if ids: return ids[0] # com a màxim hi pot haver un resultat, ja que el camp 'name' is unique
        return -1 # si no hi ha resultats, retornem aquest valor "especial" que vol dir que no hi es a la base de dades

    def wh_get_property(self, cr, uid, key, default=False):
        c_config = self.pool.get('giscesoa.sync.config')
        return c_config.get_property(cr, uid, 'giscesoa_sync', key, default)

    def wh_get_tarifa_id(self, cr, uid, nom_tarifa):

        def clean(code):
            return ''.join([c.upper() for c in code if c.isalnum()])

        if not TARIFES_INDEX:
            tarifa_obj = self.pool.get('giscedata.polissa.tarifa')
            tarifes_ids = tarifa_obj.search(cr, uid, [])
            for tarifa in tarifa_obj.read(cr, uid, tarifes_ids):
                key = clean(tarifa['name'])
                TARIFES_INDEX[key] = tarifa['id']
        tarifa = clean(nom_tarifa)
        if tarifa in TARIFES_INDEX:
            return [TARIFES_INDEX.get(tarifa)]
        else:
            return -1

    dateformat_re = re.compile(r"^\d{4}-\d{2}-\d{2}$")

    def wh_is_valid_date(self, str_date):
        str_format = "%Y-%m-%d"
        if str_date and len(str_date) == 10:
            try:
                if self.dateformat_re.match(str_date):
                    c = time.strptime(str_date, str_format)
                    str = time.strftime(str_format, c)
                    return str_date == str
            except:
                pass

        return False


    def wh_get_poblacio_id(self, cr, uid, id_municipi, nom_poblacio):

        def poblacio_processor(choice):
            """
            Fuzzy Wuzzy processor

            :param choice: choice to treat
            :return:
            """
            if isinstance(choice, dict):
                return full_process(choice['name'])
            else:
                return full_process(choice)

        def get_best_match(pob_name, pob_list):
            result = process.extractOne(pob_name, pob_list,
                                    poblacio_processor)
            id_poblacio = False
            if result and result[1] >= 90:
                id_poblacio = result[0]['id']
            return id_poblacio


        poblacio_obj = self.pool.get('res.poblacio')
        #Search replace accents
        pob_name = re.sub(u'[àáéèíìóòúù]', '%', nom_poblacio)
        search_params = [('name', 'ilike', pob_name)]
        ids = poblacio_obj.search(cr, uid, search_params)
        if len(ids) == 1:
            id_poblacio = ids[0]
        else:
            poblacio_list = poblacio_obj.search_reader(
                cr, uid, search_params, ['id', 'name'])
            id_poblacio = get_best_match(nom_poblacio, poblacio_list)
        if id_poblacio:
            return id_poblacio

        #Search replace accents and spaces
        pob_name = pob_name.replace(' ', '%')
        search_params = [('name', 'ilike', pob_name)]
        ids = poblacio_obj.search(cr, uid, search_params)
        if len(ids) == 1:
            id_poblacio = ids[0]
        else:
            poblacio_list = poblacio_obj.search_reader(
                cr, uid, search_params, ['id', 'name'])
            id_poblacio = get_best_match(nom_poblacio, poblacio_list)
        if id_poblacio:
            return id_poblacio

        # Search by municipi_id
        search_params = [('municipi_id', '=', id_municipi)]
        poblacio_list = poblacio_obj.search_reader(
            cr, uid, search_params, ['id', 'name'])

        id_poblacio = get_best_match(nom_poblacio, poblacio_list)
        if id_poblacio:
            return id_poblacio
        else:
            crear_pobs = self.wh_get_property(cr, uid, 'crear_poblacions', '1')
            if crear_pobs != '1':
                raise Exception(u"Población {0} no encontrada "
                                u"en las poblaciones del "
                                u"municipio id : {1}.".format(
                    nom_poblacio, id_municipi)
                )
            else:
                id_poblacio = poblacio_obj.create(cr, uid, {
                    'name': nom_poblacio,
                    'municipi_id': id_municipi
                })
        return id_poblacio

    def wh_omplir_camps_cups(self, cr, uid, linia):

        id_poblacio = self.wh_get_poblacio_id(cr, uid, linia.tmp_municipi_id,
                                              linia.csv_cups_poblacio)

        vals = {
            'active': 1,
            'id_municipi': linia.tmp_municipi_id,
            'id_poblacio': id_poblacio,
            'nv': linia.csv_cups_carrer,
            'pnp': linia.csv_cups_numero,
            'es': linia.csv_cups_escala,
            'pt': linia.csv_cups_planta,
            'pu': linia.csv_cups_pis,
        }

        return vals

    def wh_get_tensio_normalitzada_id(self, cr, uid, tensio):
        """
        Worker helper to get the id of the tensio and if not exists create it

        :param cr: Database cursor
        :param uid: User id
        :type uid: int
        :param tensio: Tensio to check
        :type tensio: str
        :return: Id of the tensio
        :rtype: int
        """
        tensio_obj = self.pool.get('giscedata.tensions.tensio')
        search_params = [('name', '=', tensio.lower())]
        tensions_list = tensio_obj.search(cr, uid, search_params)

        if not tensions_list:
            try:
                tensio_v = int(tensio)
            except ValueError:
                tensio_v = 0
            if tensio_v > 1000:
                tipus = 'AT'
            else:
                tipus = 'BT'
            id_tensio = tensio_obj.create(cr, uid, {
                'name': tensio.lower(),
                'l_inferior': 0,
                'l_superior': 0,
                'tensio': tensio_v,
                'tipus': tipus,
            })
        else:
            id_tensio = tensions_list[0]

        return id_tensio

    def wh_omplir_camps_polissa(self, cr, uid, linia):

        if linia.csv_polissa_tensio:
            tensio_normalitzada = self.wh_get_tensio_normalitzada_id(
                cr, uid, linia.csv_polissa_tensio)
        else:
            tensio_normalitzada = None

        vals = {
            'active': 1,
            'name': linia.csv_polissa_codi_contracte,
            'renovacio_auto': 1,
            'data_alta': linia.csv_polissa_data_alta,
            'data_firma_contracte': linia.csv_polissa_data_alta,
            'data_baixa': False,
            'potencia': linia.csv_polissa_potencia,
            'tensio_normalitzada': tensio_normalitzada,
            'observacions': linia.csv_polissa_observacions,
            'tarifa': linia.tmp_tarifa_id,
            'titular': linia.tmp_abonat_id,
            'cups': linia.tmp_cups_id,
            'comercialitzadora': linia.tmp_comercialitzadora_id,
        }

        misc_cnae = self.pool.get('giscemisc.cnae')
        cnae = linia.csv_polissa_activitat_cnae
        id_cnae = misc_cnae.search(cr, uid, [('name', '=', cnae)])

        if id_cnae:
            vals.update({'cnae': id_cnae[0]})

        return vals

    def wh_trobar_estat_polissa(self, cr, uid, id_polissa, id_cups):
        if id_polissa == -1:
            return "no existeix"
        c_polissa = self.pool.get('giscedata.polissa')
        res = c_polissa.read(cr, uid, [id_polissa], fields=['cups', 'active'])
        if not res[0]['active']:
            return "donada de baixa"
        if not res[0]['cups']:
            return "no assignada"
        if id_cups == res[0]['cups'][0]:
            return "activa al mateix cups"
        return "activa a un altre cups"

    def wh_actualitzar_potencia_polissa(self, cr, uid, linia):
        polissa_obj = self.pool.get('giscedata.polissa')
        polissa_obj.generar_periodes_potencia(cr, uid, [linia.tmp_polissa_id])
        polissa = polissa_obj.browse(cr, uid, linia.tmp_polissa_id)

        if linia.csv_polissa_potencies_per_periode:
            potencies = linia.csv_polissa_potencies_per_periode.split(' ')

            for idx, p in enumerate(polissa.potencies_periode):
                try:
                    if float(potencies[idx]) != 0.0:
                        p.write({'potencia': potencies[idx]})
                except IndexError:
                    pass







    # ------------------------------------
    # wc_* -- WORKFLOW CONDITION functions
    # ------------------------------------
    def wc_inici_proces(self, cr, uid, ids):
        # funció dummy per arrencar el workflow
        return True

    def wc_error(self, cr, uid, ids):
        obj = self.browse(cr, uid, ids)[0]
        #print "**** %s:%d: call to     wc_error" % (obj._modelname, obj.id)
        return obj.state and obj.state.endswith(":ERROR")

    def wc_es_baixa(self, cr, uid, ids):
        """
        Workflow check to check if is baixa

        :param cr: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: Ids of the workflow
        :type ids: list of ints
        :return: If is baixa
        :rtype: bool
        """
        obj = self.browse(cr, uid, ids)[0]
        #print "**** %s:%d: call to     wc_es_baixa" % (obj._modelname, obj.id)
        self.wh_assert(obj.state, ['st_validacio_general:OK', 'st_validacio_general:ERROR'])
        return obj.csv_polissa_donar_de_baixa == "1"

    def wc_existeix_client(self, cr, uid, ids):
        obj = self.browse(cr, uid, ids)[0]
        #print "**** %s:%d: call to     wc_existeix_client" % (obj._modelname, obj.id)
        self.wh_assert(obj.state, ['st_crear_cups:OK', 'st_actualitzar_cups:OK'])
        return obj.tmp_abonat_id != -1

    def wc_existeix_cups(self, cr, uid, ids):
        """
        Workflow check that checks fif the cups extists
        :param cr: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: Affected ids
        :type ids: list of int
        :return: If the CUPS exists
        :rtype: bool
        """
        obj = self.browse(cr, uid, ids)[0]
        #print "**** %s:%d: call to     wc_existeix_cups" % (obj._modelname, obj.id)
        self.wh_assert(obj.state, ['st_validacio_general:OK'])
        return obj.tmp_cups_id != -1

    def wc_existeix_polissa(self, cr, uid, ids):
        obj = self.browse(cr, uid, ids)[0]
        #print "**** %s:%d: call to     wc_existeix_polissa" % (obj._modelname, obj.id)
        self.wh_assert(obj.state, ['st_crear_client:OK', 'st_actualitzar_client:OK'])
        return obj.tmp_polissa_id != -1

    def wc_sense_comptador(self, cr, uid, ids):
        obj = self.browse(cr, uid, ids)[0]
        self.wh_assert(obj.state, ['st_comprovar_comptador:OK',
                                   'st_comprovar_comptador:ERROR'])
        return obj.tmp_comptador_id == -1

    def wc_mateix_comptador(self, cr, uid, ids):
        obj = self.browse(cr, uid, ids)[0]
        self.wh_assert(obj.state, ['st_comprovar_comptador:OK',
                                   'st_comprovar_comptador:ERROR'])
        if (obj.state == 'st_comprovar_comptador:ERROR'):
            return False;
        return obj.tmp_num_comptador == obj.csv_polissa_codi_comptador

    # ---------------------------------
    # wa_* -- WORKFLOW ACTION functions
    # ---------------------------------

    def wa_acabar_be(self, cr, uid, ids):
        obj = self.browse(cr, uid, ids)[0]
        self.wh_assert(obj.state, ["st_cursar_baixa_polissa:OK",
                                   "st_actualitzar_icp:OK",
                                   "st_crear_icp:OK"])
        obj.write({'state': 'st_tot_fet'})
        return True

    def wa_acabar_error(self, cr, uid, ids):
        obj = self.browse(cr, uid, ids)[0]
        obj.write({'state': 'st_error'})

        return True

    def wa_actualitzar_client(self, cr, uid, ids):
        obj = self.browse(cr, uid, ids)[0]
        self.wh_assert(obj.state, ["st_actualitzar_cups:OK",
                                   'st_actualitzar_cups:OK'])

        vals = {'status_message': obj.status_message}
        try:
            self.wh_actualitzar_client(cr, uid, obj)
            self.wh_add_status_message("st_actualitzar_client:OK",
                                       u"OK:Se ha actualizado los campos del abonado (id: %d)" % (obj.tmp_abonat_id), vals)

            contacte_id = self.wh_actualitzar_client_contacte(cr, uid, obj)
            self.wh_add_status_message("st_actualitzar_client:OK",
                                      u"OK:Se han actualitzado los campos del contacto (id: %d) del abonado (id: %d)" % (
                                           contacte_id, obj.tmp_abonat_id), vals)
        except:
            self.wh_add_status_message('st_exception:ERROR',
                                       u"ERROR: exception %s " %
                                       traceback.format_exc(), vals)
        finally:
            obj.write(vals)
        return True

    def wa_actualitzar_cups(self, cr, uid, ids):
        """
        Workflow assistent to update CUPS

        :param cr:Database cursor
        :param uid: User id
        :param ids: Workflow id
        :return:
        :rtype: bool
        """
        obj = self.browse(cr, uid, ids)[0]
        self.wh_assert(obj.state, ["st_validacio_general:OK"])

        vals = {'status_message': obj.status_message}
        try:
            self.wh_actualitzar_cups(cr, uid, obj)
            self.wh_add_status_message("st_actualitzar_cups:OK",
                    u"OK:Se han actualitzado los campos del CUPS '%s' (id: %d)" % (
                obj.csv_cups_codi, obj.tmp_cups_id), vals)

        except Exception:
            self.wh_add_status_message('st_exception:ERROR',
                                       "ERROR: exception %s " %
                                       traceback.format_exc(), vals)
        finally:
            obj.write(vals)

        return True

    def wa_actualitzar_polissa(self, cr, uid, ids):
        obj = self.browse(cr, uid, ids)[0]
        #print "**** %s:%d: call to wa_actualitzar_polissa()" % (obj._modelname, obj.id)
        self.wh_assert(obj.state, ["st_crear_client:OK", "st_actualitzar_client:OK"])

        vals = {'status_message': obj.status_message}
        sp_name = 'wa_actualitzar_polissa'
        cr.savepoint(sp_name)

        try:
            self.wh_actualitzar_polissa(cr, uid, obj)
            self.wh_add_status_message("st_actualitzar_polissa:OK",
                                       u"OK:Se han actualitzado los campos de la póliza '%s' (id: %d)" % (
                                           obj.csv_polissa_codi_contracte, obj.tmp_polissa_id ), vals)
        except:
            cr.rollback(sp_name)
            self.wh_add_status_message('st_exception:ERROR',
                                       "ERROR: exception %s " %
                                       traceback.format_exc(), vals)
        finally:
            obj.write(vals)
        return True

    def wa_baixa_comptador(self, cr, uid, ids):
        obj = self.browse(cr, uid, ids)[0]
        #print "**** %s:%d: call to wa_baixa_comptador()" % (obj._modelname, obj.id)
        self.wh_assert(obj.state, ["st_comprovar_comptador:OK", 'st_actualitzar_cups:OK'])

        vals = {'status_message': obj.status_message}
        try:
            c_comptador = self.pool.get('giscedata.lectures.comptador')
            id_comptador = c_comptador.search(cr, uid, [( 'polissa', '=', obj.tmp_polissa_id )])
            assert len(id_comptador) == 1
            #id_comptador = id_comptador[0]

            #comptador = c_comptador.browse(cr, uid, id_comptador)
            c_comptador.write(cr, uid, id_comptador, {'data_baixa': time.strftime('%Y-%m-%d'), 'active': 0, })
        except:
            self.wh_add_status_message('st_exception:ERROR',
                                       "ERROR: exception %s " %
                                       traceback.format_exc(), vals)
        finally:
            obj.write(vals)

    def wa_crear_client(self, cr, uid, ids):
        obj = self.browse(cr, uid, ids)[0]
        #print "**** %s:%d: call to wa_crear_client()" % (obj._modelname, obj.id)
        self.wh_assert(obj.state, ["st_actualitzar_cups:OK", 'st_actualitzar_cups:OK'])

        vals = {'status_message': obj.status_message}
        try:
            client_id = self.wh_crear_client(cr, uid, obj)

            self.wh_add_status_message("st_crear_client:OK", u"OK:Se ha creado un nuevo abonado (id: %d) con NIF '%s'" % (
                client_id, obj.csv_abonat_nif), vals)
            vals['tmp_abonat_id'] = client_id

            contacte_id = self.wh_actualitzar_client_contacte(cr, uid, obj, client_id=client_id)
            self.wh_add_status_message("st_crear_client:OK",
                                       u"OK:Se han actualitzado los campos del contacto (id: %d) del abonado (id: %d)" % (
                                           contacte_id, client_id), vals)
        except:
            self.wh_add_status_message('st_exception:ERROR',
                                       "ERROR: exception %s " %
                                       traceback.format_exc(), vals)
        finally:
            obj.write(vals)

        return True

    def wa_crear_comptador(self, cr, uid, ids):
        """
        Workflow assistent to create comptador

        :param cr: Database cursor
        :param uid: User id
        :param ids:
        :return: True if is writen
        :rtype: bool
        """

        obj = self.browse(cr, uid, ids)[0]
        self.wh_assert(obj.state, ["st_comprovar_comptador:OK",
                                   "st_baixa_comptador:OK"])

        vals = {'status_message': obj.status_message}
        try:
            c_comptador = self.pool.get('giscedata.lectures.comptador')
            vals['state'] = "st_crear_comptador:OK"
            if obj.csv_polissa_codi_comptador:
                comptador_vals = {
                    "name": obj.csv_polissa_codi_comptador,
                    "polissa": obj.tmp_polissa_id
                }
                c_comptador.create(cr, uid, comptador_vals)
        except Exception:
            self.wh_add_status_message('st_exception:ERROR',
                                       "ERROR: exception %s " %
                                       traceback.format_exc(), vals)
            return False
        finally:
            obj.write(vals)
        return True

    def wa_crear_cups(self, cr, uid, ids):
        obj = self.browse(cr, uid, ids)[0]
        #print "**** %s:%d: call to wa_crear_cups()" % (obj._modelname, obj.id)
        self.wh_assert(obj.state, ["st_validacio_general:OK"])

        vals = {'status_message': obj.status_message}
        try:
            if self.wh_comprovar_format_codi_cups(cr, uid, obj.csv_cups_codi):
                nou_cups_id = self.wh_crear_cups(cr, uid, obj)
                vals['tmp_cups_id'] = nou_cups_id
                self.wh_add_status_message("st_actualitzar_cups:OK",
                                           u"OK:Se ha creado un nuevo CUPS '%s' (id: %d)" % (obj.csv_cups_codi, nou_cups_id),
                                           vals)
            else:
                self.wh_add_status_message("st_actualitzar_cups:ERROR",
                                           u"ERROR:El codig CUPS no es válido: \"%s\"." % obj.csv_cups_codi, vals)
        except:
            self.wh_add_status_message('st_exception:ERROR',
                                       "ERROR: exception %s " %
                                       traceback.format_exc(), vals)
        finally:
            obj.write(vals)
        return True

    def wa_crear_polissa(self, cr, uid, ids):
        """
        Workflow assistant to create a polissa

        :param cr: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: Workflow id
        :type ids: list of int
        :return:
        :rtype: bool
        """
        obj = self.browse(cr, uid, ids)[0]
        #print "**** %s:%d: call to wa_crear_polissa()" % (obj._modelname, obj.id)
        self.wh_assert(obj.state, ["st_crear_client:OK", "st_actualitzar_client:OK"])

        vals = {'status_message': obj.status_message}
        try:
            polissa_id = self.wh_crear_polissa(cr, uid, obj)
            self.wh_add_status_message("st_crear_polissa:OK", u"OK:Se ha creado una nueva póliza '%s' (id: %d)" % (
                obj.csv_polissa_codi_contracte, polissa_id ), vals)
            vals['tmp_polissa_id'] = polissa_id
            obj.write(vals)
            obj = self.browse(cr, uid, obj.id)

            self.wh_actualitzar_potencia_polissa(cr, uid, obj)

            self.wh_activar_polissa(cr, uid, obj)


        except Exception:
            self.wh_add_status_message('st_exception:ERROR',
                                       "ERROR: exception %s " %
                                       traceback.format_exc(), vals)
            sentry = self.pool.get('sentry.setup')
            if sentry is not None:
                sentry.client.captureException()
        finally:
            obj.write(vals)

        return True

    def wa_comprovar_comptador(self, cr, uid, ids):
        """
        Workflow assistent to check comptador
        :param cr: Database cursor
        :param uid: User id
        :param ids: ids
        :return:
        """
        obj = self.browse(cr, uid, ids)[0]
        #print "**** %s:%d: call to wa_comprovar_comptador()" % (obj._modelname, obj.id)
        self.wh_assert(obj.state, ["st_crear_polissa:OK", "st_actualitzar_polissa:OK"])

        vals = {'status_message': obj.status_message}

        c_comptador = self.pool.get('giscedata.lectures.comptador')
        ids_comptador = c_comptador.search(cr, uid, [( 'polissa', '=', obj.tmp_polissa_id )])
        if len(ids_comptador) > 1:
            self.wh_add_status_message("st_comprovar_comptador:ERROR",
                                       u"ERROR:La póliza con ID %d tiene más de un contador activo (IDs: %s)" % (
                                           obj.tmp_polissa_id, ", ".join(ids_comptador)))
        else:
            vals['state'] = "st_comprovar_comptador:OK"
            if ids_comptador:
                vals['tmp_comptador_id'] = ids_comptador[0]

                # busquem el 'stock.production.lot' que correspon al comptador, si es que n'hi ha
                comptador = c_comptador.browse(cr, uid, ids_comptador)
                if comptador:
                    if comptador[0].name: # comptador.name es un 'stock.production.lot'
                        vals['tmp_num_comptador'] = comptador[0].name

            else:
                vals['tmp_comptador_id'] = -1 # no hi havia comptador -- wc_sense_comptador() retornara True

        obj.write(vals)
        return True

    def wa_cursar_baixa_polissa(self, cr, uid, ids):
        obj = self.browse(cr, uid, ids)[0]
        #print "**** %s:%d: call to wa_cursar_baixa_polissa()" % (obj._modelname, obj.id)
        self.wh_assert(obj.state, ["st_validacio_general:OK"])
        silenciar_baixa_polissa = self.wh_get_property(cr, uid, "silenciar_baixes_sense_efecte", "0") == "1"

        vals = {'status_message': obj.status_message}
        error = False
        try:
            data_baixa = obj.csv_polissa_data_baixa
            if obj.tmp_cups_id == -1:
                self.wh_add_status_message("st_cursar_baixa_polissa:ERROR",
                                           u"ERROR:Se pide la baja de la póliza '%s' asociada al CUPS '%s', pero el CUPS no existe." % (
                                               obj.csv_polissa_codi_contracte, obj.csv_cups_codi), vals)
                error = True
            if obj.tmp_polissa_id == -1:
                if not silenciar_baixa_polissa:
                    self.wh_add_status_message("st_cursar_baixa_polissa:ERROR",
                                               u"ERROR:Se pide la baja de la póliza '%s', que no existe." % (
                                                   obj.csv_polissa_codi_contracte), vals)
                else:
                    self.wh_add_status_message("st_cursar_baixa_polissa:OK",
                                               u"OK:La póliza '%s' (id: %d) es inexistente, se ha ignorado la solicitud de baja." % (
                                                   obj.csv_polissa_codi_contracte, obj.tmp_polissa_id), vals)
                error = True
            if not obj.csv_polissa_data_baixa:
                self.wh_add_status_message("st_cursar_baixa_polissa:OK",
                                           u"NOTIFICACIÓN:La fecha de baja viene"
                                           u"vacia y se coje la fecha de hoy "
                                           u" como fecha de baja.",
                                           vals)
                data_baixa = datetime.now().strftime('%Y-%m-%d')

            if not error:
                self.wh_cursar_baixa_polissa(cr, uid, obj.tmp_polissa_id,
                                             data_baixa, vals)
                self.wh_add_status_message("st_cursar_baixa_polissa:OK",
                                           u"OK:Se ha dado de baja la póliza"
                                           " '%s' (id: %d)" % (
                                               obj.csv_polissa_codi_contracte,
                                               obj.tmp_polissa_id), vals)
        except:
            self.wh_add_status_message('st_exception:ERROR',
                                       "ERROR: exception %s " %
                                       traceback.format_exc(), vals)
        finally:
            obj.write(vals)

        return True

    def wa_dividir_linia(self, cr, uid, ids):
        obj = self.browse(cr, uid, ids)[0]

        vals = {"status_message": obj.status_message, "state": "st_dividir_linia:OK"}
        try:
            elements = self._split_linia_original(obj.linia_original.strip('\n\r'),
                                                  obj.delimitador)
            idx_codi_polissa = self._reverse_csv_fields['csv_polissa_codi_contracte']-1
            self.wh_marcar_polissa(cr, uid, elements[idx_codi_polissa])

            if not obj.linia_original or not len(obj.linia_original):
                self.wh_add_status_message('st_dividir_linia:ERROR', u"ERROR:Se esperaban %d campos pero hay %d." % (
                    len(self._csv_fields), len(elements) ), vals)

            if len(elements) != len(self._csv_fields):
                self.wh_add_status_message('st_dividir_linia:ERROR', u"ERROR:Se esperaban %d campos pero hay %d." % (
                    len(self._csv_fields), len(elements) ), vals)
            else:
                elements.insert(0,
                                ".") # vull que els camps comencin per 1 -- aquest element zero no es te en compte (ni s'hauria de tenir en compte) enlloc
                vals['linia_dividida'] = []
                for index, details in self._csv_fields.iteritems():
                    fnom = details['name']
                    fcaption = details['caption']
                    fsize = details['size']
                    ftype = details['tipus']
                    elements[index] = elements[index].strip(' ')
                    fvalue = elements[index]
                    if not isinstance(fvalue, unicode):
                        fvalue_check = unicode(fvalue, "utf-8")
                    else:
                        fvalue_check = fvalue
                    if len(fvalue) > fsize:
                        self.wh_add_status_message('st_dividir_linia:ERROR',
                                                   u"ERROR:El campo %d (%s) tiene %d caracteres cuando el máximo es %d." % (
                                                       index, fnom, len(fvalue), fsize), vals)
                    elif len(fvalue) > 0:
                        if ftype == "integer":
                            try:
                                tmp = int(fvalue)
                            except:
                                self.wh_add_status_message('st_dividir_linia:ERROR',
                                                           u"ERROR:El campo %d (%s) ha de ser de tipo 'integer'." % (
                                                               index, fnom), vals)
                        elif ftype == "float":
                            tmp = self.wh_parse_float(fvalue)
                            if tmp is False:
                                self.wh_add_status_message('st_dividir_linia:ERROR',
                                                           u"ERROR:El camp %d (%s) ha de ser de tipo 'float'." % (
                                                               index, fnom), vals)
                            else:
                                elements[index] = tmp

                    if fnom == 'csv_polissa_tarifa':
                        elements[index] = elements[index].replace(' ', '')

                    if fnom == 'csv_polissa_data_alta':
                        if not elements[index]:
                            elements[index] = '1901-01-01'

                    vals[fnom] = elements[index]  # if elements[index] else False
                    vals['linia_dividida'].append('%d:%s: "%s"' % (index, fnom, elements[index]))
                vals['linia_dividida'] = "\n".join(vals['linia_dividida'])
        except Exception as e:
            self.wh_add_status_message(
                'st_exception:ERROR',
                "ERROR: exception {0} traceback {1} ".format(
                    e, traceback.format_exc()),
                vals
            )
        finally:
            obj.write(vals)

        return True

    def wa_validacio_general(self, cr, uid, ids):
        """
        General validation

        :param cr: Database cursor
        :param uid: Uid
        :type uid: int
        :param ids:
        :return: True
        :rtype: bool
        """

        obj = self.browse(cr, uid, ids)[0]
        #print "**** %s:%d: call to wa_validacio_general()" % (obj._modelname, obj.id)
        self.wh_assert(obj.state, ['st_dividir_linia:OK',
                                   'st_validacio_general:OK',
                                   'st_validacio_general:ERROR'])
        vals = {"status_message": obj.status_message, "state": "st_validacio_general:OK"}

        silenciar_baixa_polissa = self.wh_get_property(cr, uid, "silenciar_baixes_sense_efecte", "0") == "1"
        canvi_automatic_cups = self.wh_get_property(cr, uid, "canvi_automatic_de_cups", "0") == "1"
        baixa_auto_polissa = self.wh_get_property(cr, uid,
                                                  "baixa_automatica_de_polissa",
                                                  "0") == "1"
        validar_nif = self.wh_get_property(cr, uid, "validar_nif", "0") == "1"

        try:
            # buscarem els IDs de les entitats relacionades a la base de dades
            # per tenir una mena de mini-cache i no haver de buscar-los a
            # multiples funcions cada cop que els necessitem; aquests IDs es guarden
            # als camps que tenen noms tmp_xxx_id

            vals['tmp_municipi_id'] = self.wh_get_municipi_id(cr, uid, obj.csv_cups_municipi)
            if vals['tmp_municipi_id'] == -1:
                self.wh_add_status_message("st_validacio_general:ERROR",
                                           u"ERROR:El campo %d (csv_cups_municipi) contiene \"%s\", que no es el codigo INE de ningun municipio en la base de datos." % (
                                               self._reverse_csv_fields['csv_cups_municipi'], obj.csv_cups_municipi), vals)

            tmp_cups_id = vals['tmp_cups_id'] = self.wh_get_cups_id(cr, uid, obj.csv_cups_codi)

            if len(obj.csv_polissa_codi_contracte) == 0:
                self.wh_add_status_message('st_validacio_general:ERROR',
                                           u"ERROR:El campo %d (csv_polissa_codi_contracte) 'es obligatorio, pero viene vacio" % (
                                               self._reverse_csv_fields['csv_polissa_codi_contracte']), vals)


            tmp_polissa_id = vals['tmp_polissa_id'] = self.wh_get_polissa_id(cr, uid, obj)


            if tmp_polissa_id != -1: # la polissa ja existia
                tmp_estat_polissa = self.wh_trobar_estat_polissa(cr, uid, tmp_polissa_id, tmp_cups_id)
                if tmp_estat_polissa == "activa a un altre cups" and not baixa_auto_polissa:
                    self.wh_add_status_message("st_validacio_general:ERROR",
                                               u"ERROR:La póliza '%s' esta activa y asociada a otro CUPS." % (
                                                   obj.csv_polissa_codi_contracte), vals)
                elif tmp_estat_polissa == "donada de baixa" and not silenciar_baixa_polissa:
                    self.wh_add_status_message("st_validacio_general:ERROR",
                                               u"ERROR:La póliza '%s' ya existe y esta dada de baja." % (
                                                   obj.csv_polissa_codi_contracte), vals)

            # busquem l'id de la tarifa a giscedata_polissa_tarifa
            tmp = self.wh_get_tarifa_id(cr, uid, obj.csv_polissa_tarifa)
            if tmp == -1:
                self.wh_add_status_message('st_validacio_general:ERROR',
                                           "ERROR:El camp %d "
                                           "(csv_polissa_tarifa) indica \"%s\","
                                           " que no coincideix amb cap tarifa." % (
                                               self._reverse_csv_fields['csv_polissa_tarifa'], obj.csv_polissa_tarifa),
                                           vals)
            elif len(tmp) > 1:
                self.wh_add_status_message('st_validacio_general:ERROR',
                                           u"ERROR:El campo %d (csv_polissa_tarifa) indica \"%s\", que coincide con más de una tarifa (IDs: %s)." % (
                                               self._reverse_csv_fields['csv_polissa_tarifa'], obj.csv_polissa_tarifa,
                                               ", ".join(map(str, tmp))), vals)
            else:
                vals['tmp_tarifa_id'] = tmp[0]

            # busquem l'id de l'abonat a res.partner
            tmp = self.wh_get_abonat_id(cr, uid, obj.csv_abonat_nif, obj.csv_abonat_nom)
            if len(tmp) > 1:
                vals['tmp_abonat_id'] = tmp[0]
                self.wh_add_status_message('st_validacio_general:OK',
                                           u"NOTIFICACIÓN:Hay más de un abonado con el nombre y nif que se especifica (los IDs son %s). Se ha escogido el primero %s" % (
                                               ", ".join(map(str, tmp)),  vals['tmp_abonat_id']), vals)
            elif tmp:
                vals['tmp_abonat_id'] = tmp[0]
            else:
                vals['tmp_abonat_id'] = -1

            # busquem l'id de la comercialitzadora
            tmp = self.wh_get_comercialitzadora_id(cr, uid, obj.csv_polissa_comercialitzadora)
            if len(tmp) > 1:
                self.wh_add_status_message('st_validacio_general:ERROR',
                                           u"ERROR:Hay más de un partner con el mismo codigo de comercializadora: %s (los IDs son %s)." % (
                                               obj.csv_polissa_comercialitzadora, ", ".join(map(str, tmp))), vals)
            elif tmp:
                vals['tmp_comercialitzadora_id'] = tmp[0]
            else:
                vals['tmp_comercialitzadora_id'] = False

            self.wh_check_and_update_date_or_null(vals, obj.csv_polissa_data_baixa, 'csv_polissa_data_baixa',
                                                  'st_validacio_general:ERROR')
            self.wh_check_and_update_date_or_null(vals, obj.csv_polissa_data_alta, 'csv_polissa_data_alta',
                                                  'st_validacio_general:ERROR')
            if not obj.csv_polissa_data_alta or len(obj.csv_polissa_data_alta) == 0:
                self.wh_add_status_message('st_validacio_general:ERROR',
                                           u"El campo %d (csv_polissa_data_alta) es obligatorio, pero viene vacio" % (
                                               self._reverse_csv_fields['csv_polissa_data_alta']), vals)

            if validar_nif:
                if not obj.csv_abonat_nif:
                    self.wh_add_status_message('st_validacio_general:ERROR',
                                               u"ERROR:El campo %d (csv_abonat_nif) es obligatorio, pero es vacio" % (
                                                   self._reverse_csv_fields['csv_abonat_nif']),
                                               vals)

            if self.wh_vals_sense_error(vals) or \
                    not 'ERROR' in vals.get('status_message', ''):
                if baixa_auto_polissa:
                    if obj.csv_polissa_donar_de_baixa != "1":
                        if obj.csv_polissa_data_alta and len(obj.csv_polissa_data_alta) != 0:
                            if tmp_cups_id != -1:
                                tmp_polissa_actual_cups = self.wh_get_polissa_id_by_cups(
                                    cr, uid, tmp_cups_id)
                                if tmp_polissa_actual_cups != -1:
                                    if tmp_polissa_id != tmp_polissa_actual_cups:
                                        year, month, day = map(
                                            int, obj.csv_polissa_data_alta.split('-'))
                                        data_baixa = (date(year, month, day)
                                                      - timedelta(days=1)
                                                      ).strftime('%Y-%m-%d')
                                        self.wh_cursar_baixa_polissa(
                                            cr, uid, tmp_polissa_actual_cups,
                                            data_baixa, vals)
                                        self.wh_add_status_message(
                                            "st_validacio_general:OK",
                                            u"Dada de baja la póliza %s con el CUPS %s por actualitzación/creación de la póliza nueva" %(tmp_polissa_actual_cups, tmp_cups_id),
                                        vals)


        except:
            self.wh_add_status_message('st_exception:ERROR',
                                       "ERROR: exception %s " %
                                       traceback.format_exc(), vals)
        finally:
            obj.write(vals)

        return True


GiscesoaSyncLinia()

