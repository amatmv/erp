# coding=utf-8
from __future__ import unicode_literals
from dateutil.relativedelta import relativedelta
from datetime import datetime
import unittest
from destral import testing
from destral.patch import PatchNewCursors
from destral.transaction import Transaction
from addons import get_module_resource


class TestLoadSOA(testing.OOTestCase):

    def test_diferent_tensions(self):
        """
        Tests that the creation tension is the same as is created

        :return:
        """
        obj_sync = self.openerp.pool.get("giscesoa.sync.fitxer")
        obj_tensio = self.openerp.pool.get("giscedata.tensions.tensio")
        obj_linia = self.openerp.pool.get("giscesoa.sync.linia")
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            soa_url = get_module_resource(
                'giscesoa_sync', 'tests', 'fixtures',
                'basic.txt')

            with PatchNewCursors():
                obj_sync.carregar_fitxers(cursor, uid, [soa_url], 'immediata', True)

                linies_ids = obj_linia.search(cursor, uid, [])
                data_linia = obj_linia.read(cursor, uid, linies_ids, ["state", "status_message"])

                for linia in data_linia:
                    if linia["state"] == "st_error":
                        print(linia["status_message"])
                    self.assertNotEqual(linia["state"], "st_error")
                ids = obj_tensio.search(cursor, uid, [("name", "=", "1X220")])
                self.assertEquals(len(ids), 0)
