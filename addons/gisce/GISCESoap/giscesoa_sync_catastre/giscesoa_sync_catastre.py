# -*- coding: utf-8 -*-

from osv import osv, fields


class GiscesoaSyncLinia(osv.osv):
    """
    Adds the catastre reference to the polissa
    """

    _name = 'giscesoa.sync.linia'
    _inherit = 'giscesoa.sync.linia'

    _csv_central_field = {
        60: {'name': 'csv_cups_ref_catastral', 'size': 20, 'caption': u'Ref. Catastral', 'tipus': 'char'}
    }

    def __init__(self, pool, cr):
        """
        Class constructor
        :param pool: Connection pool
        """

        super(GiscesoaSyncLinia, self).__init__(pool, cr)
        self._csv_fields.update(self._csv_central_field)
        # Actualitzar el _columns
        for index, details in self._csv_central_field.iteritems():
            fnom = details['name']
            fcaption = details['caption']
            fsize = details['size']
            self._columns[fnom] = fields.char(fcaption, size=fsize)

    def wh_omplir_camps_cups(self, cr, uid, linia):
        """
        Actualiza els valors a actualitzar amb la centralització

        :param cr: Database cursor
        :param uid: User id
        :type uid: int
        :param linia: Imported file line
        :return: Values with ref_catastral
        :rtype: dict
        """

        vals = super(GiscesoaSyncLinia, self).wh_omplir_camps_cups(cr, uid, linia)
        vals.update(
            {"ref_catastral": linia.csv_cups_ref_catastral}
        )
        return vals

    def wh_actualitzar_cups(self, cr, uid, linia):
        """
        Updates the data of the cups

        :param cr: Database cursor
        :param uid: User id
        :type uid: int
        :param linia: line to update
        :return: None
        :rtype: None
        """
        c_cups = self.pool.get('giscedata.cups.ps')
        vals = self.wh_omplir_camps_cups(cr, uid, linia)
        c_cups.write(cr, uid, [linia.tmp_cups_id], vals)
        if vals.get("ref_catastral", False):
            try:
                c_cups.update_dir_from_ref(cr, uid, [linia.tmp_cups_id])
            except Exception:
                pass


GiscesoaSyncLinia()
