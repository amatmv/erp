# -*- coding: utf-8 -*-
{
    "name": "Actualització de pòlisses de sistema de gestió extern que genera fitxers CSV per a referencies catastrals",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE_SOA",
    "depends":[
        "giscesoa_sync_distri",
        "giscedata_polissa_distri"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
