# -*- coding: utf-8 -*-
{
    "name": "Actualització de pòlisses de sistema de gestió extern  que genera fitxers CSV per a Comers",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE_SOA",
    "depends":[
        "giscesoa_sync",
        "giscedata_polissa_comer",
        "base_bank_extended"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscesoa_sync_data.xml",
        "giscesoa_sync_workflow_linia.xml"
    ],
    "active": False,
    "installable": True
}
