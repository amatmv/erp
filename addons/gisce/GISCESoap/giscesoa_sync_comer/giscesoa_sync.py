# -*- coding: utf-8 -*-
from __future__ import absolute_import
import os
from datetime import datetime
from osv import osv, fields
from .distris import DISTRIS

from .streets import *

from giscesoa_sync.giscesoa_sync import _wkf_linia_states

import traceback

# el camp 'state' ha de contenir sempre un d'aquests valors
# sembla que OpenObject no ho comprova, i el que es clar a
# hores d'ara es que aquesta variable no te cap influencia
# a la navegacio pel workflow, excepte quan es consulta a
# les condicions explicitament.
from psycopg2._psycopg import cursor

from fuzzywuzzy import process

_wkf_linia_states += [
    ('st_crear_pagador:OK', 'st_crear_pagador:OK'),
    ('st_crear_pagador:ERROR', 'st_crear_pagador:ERROR'),
    ('st_actualitzar_pagador:OK', 'st_actualitzar_pagador:OK'),
    ('st_actualitzar_pagador:ERROR', 'st_actualitzar_pagador:ERROR'),
    ('st_actualitzar_mandato:OK', 'st_actualitzar_mandato:OK'),
    ('st_actualitzar_mandato:ERROR', 'st_actualitzar_mandato:ERROR'),
]

class GiscesoaSyncFitxer(osv.osv):
    _name = 'giscesoa.sync.fitxer'
    _inherit = 'giscesoa.sync.fitxer'

    _posicio_linia = {
        'NIF': 18,
        'COMER': 43,
        'DISTRI': 1,
        'NOM': 19,
        'CUPS': 0,
        'DATA_ALTA': 38,
        'DATA_BAIXA': 40,
    }

    def order_lines(self, cursor, uid, sorted_dic, vals_line):
        '''
        :param cursor:
        :param uid:
        :param sorted_dic:
        :param line:
        :return:
        '''

        line = vals_line['linia_original']
        delimitador = vals_line['delimitador']
        cups = (line.split(delimitador)[self._posicio_linia['CUPS']]).replace(
            ' ', '').strip()
        key = cups

        sorted_dic.setdefault(key, [])
        sorted_dic[key].append(vals_line)

        sorted_dic[key] = sorted(
            sorted_dic[key],
            key=lambda x: (
                x['linia_original'].split(
                    x['delimitador'])[self._posicio_linia['DATA_ALTA']]
            )
        )

        return True

    def wh_crear_distribuidores(self, cr, uid, distris):
        partner_obj = self.pool.get('res.partner')
        distris_id = partner_obj.search(cr, uid, [('ref', 'in', distris)])
        distris_ref = [x['ref'] for x in
                      partner_obj.read(cr, uid, distris_id, ['ref'])]
        for distri_codi in [x for x in distris if x not in distris_ref]:
            if distri_codi in DISTRIS:
                partner_obj.create(cr, uid,
                                   {'name': DISTRIS.get(distri_codi),
                                    'ref': distri_codi,
                                    'supplier': 1})

    def wh_crear_linia(self, cr, uid, list_vals):
        grouped_distris = []
        for vals in list_vals:
            distri = vals['linia_original'].split(vals['delimitador'])[
                    self._posicio_linia['DISTRI']].zfill(4)
            if distri not in grouped_distris:
                grouped_distris.append(distri)
        self.wh_crear_distribuidores(cr, uid, grouped_distris)
        super(GiscesoaSyncFitxer, self).wh_crear_linia(cr, uid, list_vals)

GiscesoaSyncFitxer()

class GiscesoaSyncLinia(osv.osv):
    _name = 'giscesoa.sync.linia'
    _inherit = 'giscesoa.sync.linia'

    _columns = {
        # Identificadors d'entitats que fem servir per estalviar cerques i
        # accesos a la base de dades durant l'execució del workflow
        'tmp_pagador_id': fields.integer('tmp__pagador__id'),
        'tmp_tarifa_comer_id': fields.integer('tmp__tarifa_comer__id'),
        'tmp_bank_id': fields.integer('tmp__bank__id'),
        'tmp_distribuidora_id': fields.integer('tmp_distribuidora_id'),
    }

    _defaults = {
        'tmp_tarifa_comer_id': lambda *a: -1,
        'tmp_bank_id': lambda *a: -1,
    }
    _csv_fields = {
        1: {'name': 'csv_cups_codi', 'size': 30, 'caption': u'CUPS', 'tipus': 'char'},
        2: {'name': 'csv_cups_distribuidora', 'size': 10, 'caption': u'Distribuidora', 'tipus': 'char'},
        3: {'name': 'csv_cups_municipi', 'size': 10, 'caption': u'Municipio', 'tipus': 'integer'},
        4: {'name': 'csv_cups_poblacio', 'size': 60, 'caption': u'Población', 'tipus': 'char'},
        5: {'name': 'csv_cups_carrer', 'size': 256, 'caption': u'Calle', 'tipus': 'char'},
        6: {'name': 'csv_cups_numero', 'size': 10, 'caption': u'Número', 'tipus': 'char'},
        7: {'name': 'csv_cups_escala', 'size': 50, 'caption': u'Escalera', 'tipus': 'char'},
        8: {'name': 'csv_cups_planta', 'size': 50, 'caption': u'Planta', 'tipus': 'char'},
        9: {'name': 'csv_cups_pis', 'size': 10, 'caption': u'Piso', 'tipus': 'char'},
        10: {'name': 'csv_cups_codi_tipus_via', 'size': 10, 'caption': u'Codigo tipo via', 'tipus': 'char'},
        11: {'name': 'csv_cups_abbr_tipus_via', 'size': 20, 'caption': u'Abreviatura tipo via', 'tipus': 'char'},
        12: {'name': 'csv_cups_tipus_via', 'size': 20, 'caption': u'Tipo via', 'tipus': 'char'},
        13: {'name': 'csv_cups_ref_catastral', 'size': 20, 'caption': u'Ref. Catastral', 'tipus': 'char'},
        14: {'name': 'csv_cups_aclarador', 'size': 100, 'caption': u'Aclarador', 'tipus': 'char'},
        15: {'name': 'csv_cups_poligon', 'size': 50, 'caption': u'Polígono', 'tipus': 'char'},
        16: {'name': 'csv_cups_parcela', 'size': 50, 'caption': u'Polígono', 'tipus': 'char'},
        17: {'name': 'csv_cups_cp_cups', 'size': 10, 'caption': u'CP CUPS', 'tipus': 'char'},
        18: {'name': 'csv_abonat_codi', 'size': 60, 'caption': u'Código abonado', 'tipus': 'char'},
        19: {'name': 'csv_abonat_nif', 'size': 32, 'caption': u'NIF abonado', 'tipus': 'char'},
        20: {'name': 'csv_abonat_nom', 'size': 128, 'caption': u'Nombre abonado', 'tipus': 'char'},
        21: {'name': 'csv_abonat_idioma', 'size': 10, 'caption': u'Idioma abonado', 'tipus': 'char'},
        22: {'name': 'csv_abonat_direccio_nom', 'size': 128, 'caption': u'Dirección nombre', 'tipus': 'char'},
        23: {'name': 'csv_abonat_direccio', 'size': 128, 'caption': u'Dirección', 'tipus': 'char'},
        24: {'name': 'csv_abonat_numero', 'size': 128, 'caption': u'Numero', 'tipus': 'char'},
        25: {'name': 'csv_abonat_planta', 'size': 128, 'caption': u'Planta', 'tipus': 'char'},
        26: {'name': 'csv_abonat_aclarador', 'size': 128, 'caption': u'Planta', 'tipus': 'char'},
        27: {'name': 'csv_abonat_cp', 'size': 10, 'caption': u'CP abonado', 'tipus': 'char'},
        28: {'name': 'csv_abonat_ine_municipi', 'size': 10, 'caption': u'INE abonado', 'tipus': 'char'},
        29: {'name': 'csv_abonat_poblacio', 'size': 128, 'caption': u'Población abonado', 'tipus': 'char'},
        30: {'name': 'csv_abonat_provincia', 'size': 128, 'caption': u'Provincia abonado', 'tipus': 'char'},
        31: {'name': 'csv_abonat_pais', 'size': 128, 'caption': u'País abonado', 'tipus': 'char'},
        32: {'name': 'csv_abonat_telefon', 'size': 64, 'caption': u'Teléfono abonado', 'tipus': 'char'},
        33: {'name': 'csv_abonat_fax', 'size': 64, 'caption': u'FAX abonado', 'tipus': 'char'},
        34: {'name': 'csv_abonat_mail', 'size': 240, 'caption': u'Email abonado', 'tipus': 'char'},
        35: {'name': 'csv_abonat_mobil', 'size': 64, 'caption': u'Movil abonado', 'tipus': 'char'},
        36: {'name': 'csv_polissa_codi_comptador', 'size': 20, 'caption': u'Núm. contador', 'tipus': 'char'},
        37: {'name': 'csv_polissa_codi_contracte', 'size': 60, 'caption': u'Núm. póliza (contrato)', 'tipus': 'char'},
        38: {'name': 'csv_polissa_tarifa', 'size': 10, 'caption': u'Tarifa', 'tipus': 'char'},
        39: {'name': 'csv_polissa_data_alta', 'size': 10, 'caption': u'Fecha alta', 'tipus': 'date'},
        40: {'name': 'csv_polissa_donar_de_baixa', 'size': 1, 'caption': u'Baja', 'tipus': 'boolean'},
        41: {'name': 'csv_polissa_data_baixa', 'size': 10, 'caption': u'Fecha baja', 'tipus': 'date'},
        42: {'name': 'csv_polissa_potencia', 'size': 11, 'caption': u'Potencia', 'tipus': 'float'},
        43: {'name': 'csv_polissa_tensio', 'size': 30, 'caption': u'Tensión', 'tipus': 'char'},
        44: {'name': 'csv_polissa_activitat_cnae', 'size': 10, 'caption': u'Actividad CNAE', 'tipus': 'char'},
        45: {'name': 'csv_polissa_comercialitzadora', 'size': 4, 'caption': u'Comercializadora', 'tipus': 'char'},
        46: {'name': 'csv_polissa_observacions', 'size': 1024, 'caption': u'Observaciones', 'tipus': 'text'},
        47: {'name': 'csv_polissa_titular', 'size': 128, 'caption': u'Titular', 'tipus': 'char'},
        48: {'name': 'csv_polissa_estat', 'size': 60, 'caption': u'Estado de la póliza', 'tipus': 'char'},
        49: {'name': 'csv_polissa_ref_distriuidora', 'size': 60, 'caption': u'Referencia distribuidora', 'tipus': 'char'},
        50: {'name': 'csv_polissa_tipus_punt_mesura', 'size': 2, 'caption': u'Tipo punto medida', 'tipus': 'char'},
        51: {'name': 'csv_polissa_potencies_per_periode', 'size': 128, 'caption': u'Potencia por periodo', 'tipus': 'char'},

        52: {'name': 'csv_polissa_data_firma_contracte', 'size': 10, 'caption': u'Fecha firma contrato', 'tipus': 'date'},
        53: {'name': 'csv_fact_facturacio_tipus', 'size': 50, 'caption': u'Tipo facturación', 'tipus': 'char'},
        54: {'name': 'csv_fact_facturacio_potencia', 'size': 64, 'caption': u'Facturación potencia', 'tipus': 'char'},
        55: {'name': 'csv_fact_unitat_facturacio', 'size': 10, 'caption': u'Unidad de facturación', 'tipus': 'char'},
        56: {'name': 'csv_fact_tarifa_comercialitzadora', 'size': 50, 'caption': u'Tarifa comercializadora', 'tipus': 'char'},
        57: {'name': 'csv_fact_tipus_pagament', 'size': 50, 'caption': u'Tipo de pago', 'tipus': 'char'},
        58: {'name': 'csv_fact_grup_pagament', 'size': 50, 'caption': u'Grupo de pago', 'tipus': 'char'},
        59: {'name': 'csv_fact_desc_bancari', 'size': 60, 'caption': u'Descripción bancaria', 'tipus': 'char'},
        60: {'name': 'csv_fact_compte_bancari', 'size': 30, 'caption': u'Cuenta bancaria', 'tipus': 'char'},
        61: {'name': 'csv_fact_persona_pagador', 'size': 7, 'caption': u'Persona pagadora', 'tipus': 'char'},
        62: {'name': 'csv_pagador_codi', 'size': 32, 'caption': u'Codigo pagador', 'tipus': 'char'},
        63: {'name': 'csv_pagador_nif', 'size': 32, 'caption': u'NIF pagador', 'tipus': 'char'},
        64: {'name': 'csv_pagador_nom', 'size': 128, 'caption': u'Nombre pagador', 'tipus': 'char'},
        65: {'name': 'csv_pagador_idioma', 'size': 10, 'caption': u'Idioma pagador', 'tipus': 'char'},
        66: {'name': 'csv_pagador_direccio_nom', 'size': 128, 'caption': u'Dirección nombre pagador', 'tipus': 'char'},
        67: {'name': 'csv_pagador_direccio', 'size': 128, 'caption': u'Dirección pagador', 'tipus': 'char'},
        68: {'name': 'csv_pagador_numero', 'size': 128, 'caption': u'Numero', 'tipus': 'char'},
        69: {'name': 'csv_pagador_planta', 'size': 128, 'caption': u'Planta', 'tipus': 'char'},
        70: {'name': 'csv_pagador_cp', 'size': 10, 'caption': u'CP pagador', 'tipus': 'char'},
        71: {'name': 'csv_pagador_ine_municipi', 'size': 10, 'caption': u'INE pagador', 'tipus': 'char'},
        72: {'name': 'csv_pagador_poblacio', 'size': 128, 'caption': u'Población pagador', 'tipus': 'char'},
        73: {'name': 'csv_pagador_provincia', 'size': 128, 'caption': u'Provincia pagador', 'tipus': 'char'},
        74: {'name': 'csv_pagador_pais', 'size': 128, 'caption': u'País pagador', 'tipus': 'char'},
        75: {'name': 'csv_pagador_telefon', 'size': 64, 'caption': u'Teléfono pagador', 'tipus': 'char'},
        76: {'name': 'csv_pagador_fax', 'size': 64, 'caption': u'FAX pagador', 'tipus': 'char'},
        77: {'name': 'csv_pagador_mail', 'size': 240, 'caption': u'Email pagador', 'tipus': 'char'},
        78: {'name': 'csv_pagador_mobil', 'size': 64, 'caption': u'Movil pagador', 'tipus': 'char'},
        79: {'name': 'csv_fact_enviament_factura', 'size': 12, 'caption': u'Envio de factura', 'tipus': 'char'},
        80: {'name': 'csv_fact_persona_notificacio', 'size': 7, 'caption': u'Persona notificación', 'tipus': 'char'},
        # Dades enviament
        81: {'name': 'csv_notificacio_nom', 'size': 128, 'caption': u'Nombre notificación', 'tipus': 'char'},
        82: {'name': 'csv_notificacio_nif', 'size': 128, 'caption': u'NIF notificación', 'tipus': 'char'},
        83: {'name': 'csv_notificacio_direccio_nom', 'size': 128, 'caption': u'Dirección nombre notificación', 'tipus': 'char'},
        84: {'name': 'csv_notificacio_direccio', 'size': 128, 'caption': u'Dirección notificación', 'tipus': 'char'},
        85: {'name': 'csv_notificacio_numero', 'size': 128, 'caption': u'Numero', 'tipus': 'char'},
        86: {'name': 'csv_notificacio_planta', 'size': 128, 'caption': u'Planta', 'tipus': 'char'},
        87: {'name': 'csv_notificacio_cp', 'size': 10, 'caption': u'CP notificación', 'tipus': 'char'},
        88: {'name': 'csv_notificacio_ine_municipi', 'size': 10, 'caption': u'INE notificación', 'tipus': 'char'},
        89: {'name': 'csv_notificacio_poblacio', 'size': 128, 'caption': u'Población notificación', 'tipus': 'char'},
        90: {'name': 'csv_notificacio_provincia', 'size': 128, 'caption': u'Provincia notificación', 'tipus': 'char'},
        91: {'name': 'csv_notificacio_pais', 'size': 128, 'caption': u'País notificación', 'tipus': 'char'},
        92: {'name': 'csv_notificacio_telefon', 'size': 64, 'caption': u'Teléfono notificación', 'tipus': 'char'},
        93: {'name': 'csv_notificacio_fax', 'size': 64, 'caption': u'FAX notificación', 'tipus': 'char'},
        94: {'name': 'csv_notificacio_mail', 'size': 240, 'caption': u'Email notificación','tipus': 'char'},
        95: {'name': 'csv_notificacio_mobil', 'size': 64, 'caption': u'Movil notificación', 'tipus': 'char'},
        # Dades propietari CC
        96: {'name': 'csv_propietari_cc_nom', 'size': 128, 'caption': u'Propietario CC', 'tipus': 'char'},
        97: {'name': 'csv_propietari_cc_nif', 'size': 128, 'caption': u'NIF Propietario CC', 'tipus': 'char'},
        98: {'name': 'csv_propietari_cc_direccio', 'size': 128, 'caption': u'Dirección propietario cc', 'tipus': 'char'},
        99: {'name': 'csv_propietari_cc_numero', 'size': 128, 'caption': u'Numero propietario cc', 'tipus': 'char'},
        100: {'name': 'csv_propietari_cc_planta', 'size': 128, 'caption': u'Planta propietario cc', 'tipus': 'char'},
        101: {'name': 'csv_propietari_cc_cp', 'size': 10, 'caption': u'CP propietario cc', 'tipus': 'char'},
        102: {'name': 'csv_propietari_cc_ine_municipi', 'size': 10, 'caption': u'INE propietario cc', 'tipus': 'char'},
        103: {'name': 'csv_propietari_cc_poblacio', 'size': 128, 'caption': u'Población propietario cc', 'tipus': 'char'},
        104: {'name': 'csv_propietari_cc_provincia', 'size': 128, 'caption': u'Provincia propietario cc', 'tipus': 'char'},
        105: {'name': 'csv_propietari_cc_pais', 'size': 128, 'caption': u'País propietario cc', 'tipus': 'char'},
        106: {'name': 'csv_propietari_cc_telefon', 'size': 64, 'caption': u'Teléfono propietario cc', 'tipus': 'char'},
        107: {'name': 'csv_propietari_cc_fax', 'size': 64, 'caption': u'FAX propietario cc', 'tipus': 'char'},
        108: {'name': 'csv_propietari_cc_mail', 'size': 240, 'caption': u'Email propietario cc','tipus': 'char'},
        109: {'name': 'csv_propietari_cc_mobil', 'size': 64, 'caption': u'Movil propietario cc', 'tipus': 'char'},
        # Extra address fields
        110: {'name': 'csv_abonat_escala', 'size': 10, 'caption': u'Escalera dirección abonado', 'tipus': 'char'},
        111: {'name': 'csv_abonat_puerta', 'size': 10, 'caption': u'Puerta dirección abonado', 'tipus': 'char'},
        112: {'name': 'csv_pagador_escala', 'size': 10, 'caption': u'Escalera dirección pagador', 'tipus': 'char'},
        113: {'name': 'csv_pagador_puerta', 'size': 10, 'caption': u'Puerta dirección pagador', 'tipus': 'char'},
        114: {'name': 'csv_pagador_aclarador', 'size': 256, 'caption': u'Aclarador dirección pagador', 'tipus': 'char'},
        115: {'name': 'csv_notificacio_escala', 'size': 10, 'caption': u'Escalera dirección pagador', 'tipus': 'char'},
        116: {'name': 'csv_notificacio_puerta', 'size': 10, 'caption': u'Puerta dirección pagador', 'tipus': 'char'},
        117: {'name': 'csv_notificacio_aclarador', 'size': 256, 'caption': u'Aclarador dirección pagador', 'tipus': 'char'},
    }

    _reverse_csv_fields = {}

    _valid_tipus_facturacio = {'MENSUAL': 1, 'BIMESTRAL': 2}
    _valid_facturacio_potencia = {'MAXIMETRO': 'max', 'ICP': 'icp',
                                  'RECARGO_ICP': 'recarrec'}
    _valid_fact_tipus_pagament = ['RECIBO_CSB', 'TRANSFERENCIA_CSB']

    _valid_tipus_enviament = {'POSTAL': 'postal', 'EMAIL': 'email',
                              'POST+EMAIL': 'postal+email'}
    _valid_persona_pagador = {'TITULAR': 'titular', 'OTRO': 'altre_p'}

    _valid_persona_notificacio = {'TITULAR': 'titular', 'PAGADOR': 'pagador',
                                  'ALTRE': 'altre_p'}



    for k in _csv_fields.keys():
        _reverse_csv_fields[_csv_fields[k]['name']] = k

    # afegim els camps 'temporals' que corresponen a les columnes del fitxer csv
    for index, details in _csv_fields.iteritems():
        fnom = details['name']
        fcaption = details['caption']
        fsize = details['size']
        _columns[fnom] = fields.char(fcaption, size=fsize)

    # ------------------------------------
    # wh_* -- WORKFLOW HELPERS :)
    # ------------------------------------

    def wh_get_polissa_id(self, cr, uid, linia):
        polissa_obj = self.pool.get('giscedata.polissa')
        cups_id = self.wh_get_cups_id(cr, uid, linia.csv_cups_codi)
        pols_ids = polissa_obj.search(
            cr, uid, [('cups', '=', cups_id), ('state', '=', 'esborrany')])
        if pols_ids:
            return pols_ids[0]
        polissa_name = linia.csv_polissa_codi_contracte
        c_polissa = self.pool.get('giscedata.polissa')
        ids = c_polissa.search(cr, uid, [('name', '=', polissa_name)], context={'active_test': False})
        if ids:
            return ids[0] # com a màxim hi pot haver un resultat, ja que el camp 'name' is unique
        return -1 # si no hi ha resultats, retornem aquest valor "especial" que vol dir que no hi es a la base de dades

    def wh_get_distribuidora_id(self, cr, uid, str_codi):
        c_partner = self.pool.get('res.partner')
        str_codi = str_codi.zfill(4)
        distri = c_partner.search(cr, uid, [('ref', '=', str_codi),
                                            ('supplier', '=', 1)])
        if not distri:
            if str_codi in DISTRIS:
                distri = c_partner.create(cr, uid,
                                         {'name': DISTRIS.get(str_codi),
                                          'ref': str_codi,
                                          'supplier': 1})
                distri = [distri]
        return distri

    def wh_extended_address(
            self, cursor, uid, street, number, floor, stairs, door,
            clarifying=False):
        '''

        :param cursor:
        :param uid:
        :param street:
        :param number:
        :param floor:
        :param stairs:
        :param door:
        :param clarifying: Optional parameter. By default is False
        :return: dict with extra address values
        '''
        street_cleaned = clean_street(street)
        tv_id = self.wh_get_tipus_via(cursor, uid, street_cleaned['tv'])
        vals = {
            'street': street_cleaned['street'],
            'tv': tv_id,
            'nv': street_cleaned['street'],
            'pnp': number,
            'pt': floor,
            'es': stairs,
            'pu': door,
        }
        if clarifying:
            vals.update({'aclarador': clarifying})
        return vals

    def wh_get_partner_id(self, cursor, uid, nif, nom):
        tmp = super(GiscesoaSyncLinia,
                          self).wh_get_abonat_id(cursor, uid, nif, nom)
        if len(tmp) > 1:
             return tmp[0]
        elif tmp:
            return  tmp[0]
        else:
            return -1

    def wh_neteja_bank(self, bank):
        return "".join([x for x in bank if x.isdigit()])

    def wh_partner_equals(self, partner_1, partner_2):
        result = process.extractBests(unicode(partner_2), [unicode(partner_1)])
        if result and result[0][1] >= 95:
            return True
        return False

    def wh_validacio_bank(self, cursor, uid, iban):
        bank_obj = self.pool.get('res.partner.bank')
        vals = bank_obj.onchange_iban(cursor, uid, 0, iban)
        if 'warning' in vals:
            return False
        elif 'value' in vals:
            return True
        return False

    def wh_get_tipus_via(self, cursor, uid, codi_tipus_via):
        tpv_obj = self.pool.get('res.tipovia')
        search = [('codi', '=', codi_tipus_via)]
        tpv_id = tpv_obj.search(cursor, uid, search)
        if tpv_id:
            return tpv_id[0]
        else:
            return False

    def wh_obtenir_tipus_via(self, cursor, uid, linia):
        tpv_obj = self.pool.get('res.tipovia')
        search = [('codi', '=', linia.csv_cups_codi_tipus_via)]
        tpv_id = tpv_obj.search(cursor, uid, search)
        if tpv_id:
            return tpv_id[0]
        else:
            abr = linia.csv_cups_abbr_tipus_via or linia.csv_cups_tipus_via
            vals = {
                'codi': linia.csv_cups_codi_tipus_via,
                'abr': abr,
                'name': linia.csv_cups_tipus_via
            }
            return tpv_obj.create(cursor, uid, vals)

    def wh_obtenir_pais_id(self, cursor, uid, codi):
        country_id = self.pool.get('res.country').search(
                cursor, uid, [('code', '=', codi)])
        if country_id:
            return country_id[0]
        else:
            return False
    def wh_obtenir_country_state_id(self, cursor, uid, id_municipi):
        municipi_obj = self.pool.get('res.municipi')
        fields_read = ['state']
        state_id = municipi_obj.read(cursor, uid, id_municipi, fields_read)
        if state_id:
            return state_id['state'][0]
        else:
            return False

    def wh_obtenir_ubicacio_partner(self, cursor, uid, ine_municipi, poblacio,
                                    codi_pais):
        id_municipi = super(GiscesoaSyncLinia, self).wh_get_municipi_id(
            cursor, uid, ine_municipi)
        if id_municipi != -1:
            id_poblacio = super(GiscesoaSyncLinia, self).wh_get_poblacio_id(
                cursor, uid, id_municipi, poblacio)
            id_provincia = self.wh_obtenir_country_state_id(cursor, uid,
                                                        id_municipi)
        else:
            id_poblacio = False
            id_municipi = False
            id_provincia = False

        id_pais = self.wh_obtenir_pais_id(cursor, uid, codi_pais)

        vals = {'id_municipi': id_municipi,
                'id_poblacio': id_poblacio,
                'country_id': id_pais,
                'state_id': id_provincia,
                }

        return vals

    def wh_omplir_dades_partner_titular(self, cursor, uid, linia):
        vals = super(GiscesoaSyncLinia,
                          self).wh_omplir_dades_partner_titular(cursor, uid, linia)
        tipus = 'abonat'
        fields = [('lang', 'csv_%s_idioma'), ('ref', 'csv_%s_codi')]
        extra_vals = super(GiscesoaSyncLinia,
                          self).wh_omplir_dades_generic(linia, fields, tipus)
        vals.update(extra_vals)
        return vals

    def wh_omplir_dades_partner_address_titular(self, cursor, uid, partner_id, linia):
        vals = super(GiscesoaSyncLinia,
                          self).wh_omplir_dades_partner_address_titular(cursor, uid,
                                                                partner_id,
                                                                linia)
        c_partner_address = self.pool.get('res.partner.address')
        fields_address = c_partner_address.fields_get(cursor, uid).keys()
        ubicacio_vals = self.wh_obtenir_ubicacio_partner(
            cursor, uid, linia.csv_abonat_ine_municipi,
            linia.csv_abonat_poblacio, linia.csv_abonat_pais)
        tipus = 'abonat'
        fields = [('mobile', 'csv_%s_mobil'), ('zip', 'csv_%s_cp'),
                  ('name', 'csv_%s_direccio_nom'),
                  ('street', 'csv_%s_direccio'),
                  ('aclarador', 'csv_%s_aclarador')]
        extra_vals = super(GiscesoaSyncLinia,
                          self).wh_omplir_dades_generic(linia, fields, tipus)
        if 'tv' in fields_address:
            address_vals = self.wh_extended_address(
                cursor, uid, extra_vals['street'], linia.csv_abonat_numero,
                linia.csv_abonat_planta, linia.csv_abonat_escala,
                linia.csv_abonat_puerta
            )
            extra_vals.update(address_vals)
        else:
            extra_vals['street'] = '{} {} {} {}'.format(
                extra_vals['street'],
                linia.csv_abonat_numero,
                linia.csv_abonat_planta,
                linia.csv_abonat_aclarador
            )
        vals.update(extra_vals)
        vals.update(ubicacio_vals)
        return vals

    def wh_omplir_camps_cups(self, cursor, uid, linia):
        vals = super(GiscesoaSyncLinia,
                          self).wh_omplir_camps_cups(cursor, uid, linia)
        tipus_via_id = self.wh_obtenir_tipus_via(cursor, uid, linia)

        update_vals = {'tv': tipus_via_id,
                       'ref_catastral': linia.csv_cups_ref_catastral,
                       'aclarador': linia.csv_cups_aclarador,
                       'cpo': linia.csv_cups_poligon,
                       'cpa': linia.csv_cups_parcela,
                       'dp': linia.csv_cups_cp_cups,
                       'distribuidora_id': linia.tmp_distribuidora_id,
                       }
        vals.update(update_vals)
        return vals

    def wh_actualitzar_mandato(self, cursor, uid, polissa_id):
        mandate_obj = self.pool.get('payment.mandate')
        search = [('reference', '=', 'giscedata.polissa,%s' % polissa_id)]
        mandate_id = mandate_obj.search(cursor, uid, search)
        if mandate_id:
            vals = {'date_end': datetime.now().strftime('%Y-%m-%d')}
            mandate_obj.write(cursor, uid, mandate_id, vals)

        vals = {'reference': 'giscedata.polissa,%s' % polissa_id}
        mandate_obj.create(cursor, uid, vals)


    def wh_obtenir_unitat_facturacio(self, cursor, uid, nom_unitat):
        product_uom_obj = self.pool.get('product.uom')
        search = [('name', '=', nom_unitat)]
        unitat_id = product_uom_obj.search(cursor, uid, search)
        if unitat_id:
            return unitat_id[0]
        else:
            return False

    def wh_obtenir_tarifa_comer(self, cursor, uid, nom_tarifa):
        price_list_obj = self.pool.get('product.pricelist')
        price_list_v_obj = self.pool.get('product.pricelist.version')
        tarifa_obj = self.pool.get('giscedata.polissa.tarifa')

        search = [('type', '=', 'sale'),
                  ('name', '=', nom_tarifa)]

        tarifa_comer = price_list_obj.search(cursor, uid, search)

        if tarifa_comer:
            return tarifa_comer[0]
        else:
            pid = price_list_obj.create(cursor, uid, {
                'name': nom_tarifa,
                'type': 'sale'
            })
            price_list_v_obj.create(cursor, uid, {
                'name': '1',
                'pricelist_id': pid,
            })
            for tarifa_id in tarifa_obj.search(cursor, uid, []):
                cursor.execute("INSERT INTO giscedata_polissa_tarifa_pricelist  (tarifa_id, pricelist_id) VALUES (%s, %s)", (tarifa_id, pid))
            return pid



    def wh_obtenir_tipus_pagament(self, cursor, uid, linia):
        payment_type_obj = self.pool.get('payment.type')
        search = [('code', '=', linia.csv_fact_tipus_pagament)]
        tipus_pagament = payment_type_obj.search(cursor, uid, search)
        if tipus_pagament:
            return tipus_pagament[0]
        else:
            vals = {
            'name': linia.csv_fact_tipus_pagament,
            'code': linia.csv_fact_tipus_pagament.upper()
            }
            return payment_type_obj.create(cursor, uid, vals)

    def wh_obtenir_grup_pagament(self, cursor, uid, linia):
        vals_message = {'status_message': linia.status_message}
        #Recuperem status message
        payment_mode_obj = self.pool.get('payment.mode')

        search_vals= [('name', '=', linia.csv_fact_grup_pagament)]

        grup_pagament_ids = payment_mode_obj.search(
            cursor, uid, search_vals
        )

        if grup_pagament_ids:
            return grup_pagament_ids[0]
        else:
            payment_type_obj = self.pool.get('payment.type')
            journal_obj = self.pool.get('account.journal')
            bank_obj = self.pool.get('res.partner.bank')
            bank_id = bank_obj.search(cursor, uid, [('partner_id', '=', 1)])
            if bank_id:
                bank_id = bank_id[0]
            else:
                raise Exception(
                    "ERROR: No esta definido el banco del Partner 1")
            journal_id = journal_obj.search(
                cursor, uid, [('code', '=', 'CAJA')])[0]
            type_id = payment_type_obj.search(
                cursor, uid, [('code', '=', 'RECIBO_CSB')])[0]

            vals = {
                'name': linia.csv_fact_grup_pagament,
                'bank_id': bank_id,
                'journal': journal_id,
                'type': type_id,
                'tipo': 'csb_19',
                'nombre': linia.csv_fact_grup_pagament,
                'sufijo': 000,
                'require_bank_account': False,
                'partner_id': 1
            }
            grup_pagament_id = payment_mode_obj.create(cursor, uid, vals)
            return grup_pagament_id



    def wh_omplir_camps_polissa(self, cursor, uid, linia):

        vals = super(GiscesoaSyncLinia, self) \
            .wh_omplir_camps_polissa(cursor, uid, linia)

        config = self.pool.get('res.config')
        forcar_notificacio = self.wh_get_property(
            cursor, uid, 'forcar_notificacio')

        if not linia.csv_polissa_data_firma_contracte:
            data_firma_contracte = linia.csv_polissa_data_alta
        else:
            data_firma_contracte = linia.csv_polissa_data_firma_contracte

        tipus_facturacio = self._valid_tipus_facturacio[
                                    linia.csv_fact_facturacio_tipus]

        tarifa_comercialitzadora = linia.tmp_tarifa_comer_id

        if not linia.csv_fact_enviament_factura in self._valid_tipus_enviament:
            enviament = self._valid_tipus_enviament['POSTAL']
        else:
            enviament = self._valid_tipus_enviament[linia.csv_fact_enviament_factura]

        pagador_sel = self._valid_persona_pagador[
            linia.csv_fact_persona_pagador]
        notificacio = self._valid_persona_notificacio[
            linia.csv_fact_persona_notificacio]

        if (not linia.csv_pagador_direccio_nom
                or not linia.csv_pagador_direccio):
            direccio_pag = self.wh_get_direccio(
                cursor, uid, linia.tmp_pagador_id, 'contact')
        else:
            direccio_pag = self.wh_get_direccio(
                cursor, uid, linia.tmp_pagador_id, 'invoice')

        street_notificacio = linia.csv_notificacio_direccio
        if linia.csv_fact_persona_notificacio == 'TITULAR':
            if street_notificacio:
                direccio_notificacio = self.wh_actualitza_direccio_notificacio(
                cursor, uid, linia.tmp_abonat_id, linia)
            else:
                direccio_notificacio = self.wh_get_direccio(
                    cursor, uid, linia.tmp_abonat_id, 'contact')
        elif linia.csv_fact_persona_notificacio == 'PAGADOR':
            if street_notificacio:
                direccio_notificacio = self.wh_actualitza_direccio_notificacio(
                cursor, uid, linia.tmp_pagador_id, linia)
            else:
                direccio_notificacio = self.wh_get_direccio(
                    cursor, uid, linia.tmp_pagador_id, 'invoice')
        elif linia.csv_fact_persona_notificacio == 'ALTRE':
            if not forcar_notificacio:
                altre_p = self.wh_actualitza_persona_notificacio(
                    cursor, uid, linia)
            elif forcar_notificacio == 'fiscal':
                altre_p = linia.tmp_pagador_id
                notificacio = pagador_sel
            elif forcar_notificacio == 'titular':
                altre_p = linia.tmp_abonat_id
                notificacio = self._valid_persona_notificacio['TITULAR']
            direccio_notificacio = self.wh_actualitza_direccio_notificacio(
                cursor, uid, altre_p, linia)



        #unitat_fact = self.wh_obtenir_unitat_facturacio(
        #    cursor, uid,linia.csv_fact_unitat_facturacio)

        grup_pagament = self.wh_obtenir_grup_pagament(cursor, uid, linia)

        tipo_pago = self.wh_obtenir_tipus_pagament(cursor, uid, linia)

        facturacio_potencia = self._valid_facturacio_potencia[
            linia.csv_fact_facturacio_potencia]

        nous_valors = {
            'active': 1,
            'name': linia.csv_polissa_codi_contracte,
            'renovacio_auto': 1,
            'data_alta': linia.csv_polissa_data_alta,
            'data_firma_contracte': data_firma_contracte,
            'data_baixa': False,
            'potencia': linia.csv_polissa_potencia,
            'observacions': linia.csv_polissa_observacions,
            'tarifa': linia.tmp_tarifa_id,
            'titular': linia.tmp_abonat_id,
            'cups': linia.tmp_cups_id,
            'comercialitzadora': linia.tmp_comercialitzadora_id,
            'ref_dist': linia.csv_polissa_ref_distriuidora,
            'distribuidora': linia.tmp_distribuidora_id,
            'facturacio': tipus_facturacio,
            'facturacio_potencia': facturacio_potencia,
            'payment_mode_id': grup_pagament,
            'tipo_pago': tipo_pago,
            'llista_preu': tarifa_comercialitzadora,
            'pagador': linia.tmp_pagador_id,
            'direccio_pagament': direccio_pag,
            'bank': linia.tmp_bank_id,
            'pagador_sel': pagador_sel,
            'enviament': enviament,
            'notificacio': notificacio,
            'direccio_notificacio': direccio_notificacio,
        }
        if notificacio == 'altre_p':
            vals['altre_p'] = altre_p
        #if unitat_fact:
        #    nous_valors['property_unitat_potencia'] = unitat_fact

        vals.update(nous_valors)
        return vals

    def wh_validar_direccio_pagament(self, cursor, uid, linia):
        if linia.csv_abonat_direccio == linia.csv_pagador_direccio or \
                not linia.csv_pagador_direccio:
            return True
        else:
            return False

    def wh_get_direccio(self, cursor, uid, partner_id, tipus='contact'):
        c_partner_address = self.pool.get('res.partner.address')
        # inhabilitar adreces antigues
        addresses_ids = c_partner_address.search(
            cursor, uid,
            [('partner_id', '=', partner_id), ('type', '=', tipus)])
        if addresses_ids:
            return addresses_ids[0]
        else:
            addresses_ids = c_partner_address.search(
                cursor, uid,
                [('partner_id', '=', partner_id), ('type', '=', 'default')])
            if addresses_ids:
                return addresses_ids[0]
        return False

    def wh_actualitza_persona_notificacio(self, cursor, uid, linia):
        partner_obj = self.pool.get('res.partner')
        tmp = self.wh_get_abonat_id(
            cursor, uid, linia.csv_notificacio_nif, linia.csv_notificacio_nom)
        status_message = {'status_message': linia.status_message}
        state_anterior = linia.state
        if len(tmp) > 1:
                partner_id = tmp[0]
        elif tmp:
            partner_id = tmp[0]
        else:
            fix_vat = self.wh_fix_vat(linia.csv_notificacio_nif)
            vals = {'name': linia.csv_notificacio_nom,
                    'vat': fix_vat,
                    'lang': 'es_ES'}
            partner_id = partner_obj.create(cursor, uid, vals)
            self.wh_add_status_message(
                state_anterior,
                u"OK:Se ha creado un nuevo abonado para notif (id: %d) "
                u"con Nombre %s NIF '%s'" % (
                    partner_id, linia.csv_notificacio_nom, fix_vat),
                status_message)
            linia.write({'status_message': status_message})
        return partner_id

    def wh_actualitza_direccio_notificacio_by_id(self, cursor, uid, partner_id, linia_id):
        linia = self.browse(cursor, uid, linia_id)
        return self.wh_actualitza_direccio_notificacio(cursor, uid, partner_id, linia)

    def wh_actualitza_direccio_notificacio(self, cursor, uid, partner_id, linia):
        c_partner_address = self.pool.get('res.partner.address')
        fields_address = c_partner_address.fields_get(cursor, uid).keys()
        # inhabilitar adreces antigues
        search_addrees = [('partner_id', '=', partner_id)]
        if 'tv' in fields_address:
            street_cleaned = clean_street(linia.csv_notificacio_direccio)
            search_addrees += [
                ('nv', '=', street_cleaned['street']),
                ('pnp', '=', linia.csv_notificacio_numero),
                ('pt', '=', linia.csv_notificacio_planta),
                ('pu', '=', linia.csv_notificacio_puerta),
                ('es', '=', linia.csv_notificacio_escala),
                ('aclarador', '=', linia.csv_notificacio_aclarador),
                ('name', '=', linia.csv_notificacio_direccio_nom)
            ]
        else:
            street = '{} {} {}'.format(
                linia.csv_notificacio_direccio,
                linia.csv_notificacio_numero,
                linia.csv_notificacio_planta
            )
            search_addrees += [('street', '=', street)]
        old_addresses_ids = c_partner_address.search(
            cursor, uid, search_addrees)
        if old_addresses_ids:
            return old_addresses_ids[0]
        else:
            vals = {'partner_id': partner_id,
                    'type': 'delivery'}

            fields = [('name', 'csv_%s_nom'), ('phone', 'csv_%s_telefon'),
                      ('fax', 'csv_%s_fax'), ('email', 'csv_%s_mail'),
                      ('mobile', 'csv_%s_mobil'), ('zip', 'csv_%s_cp'),
                      ('name', 'csv_%s_direccio_nom'),
                      ('street', 'csv_%s_direccio'),
                      ('aclarador', 'csv_%s_aclarador')
                      ]
            tipus = 'notificacio'

            extra_vals = self.wh_omplir_dades_generic(linia, fields, tipus)
            if 'tv' in fields_address:
                address_vals = self.wh_extended_address(
                    cursor, uid, extra_vals['street'],
                    linia.csv_notificacio_numero, linia.csv_notificacio_planta,
                    linia.csv_notificacio_escala, linia.csv_notificacio_puerta
                )
                extra_vals.update(address_vals)
            else:
                extra_vals['street'] = '{} {} {}'.format(
                    extra_vals['street'],
                    linia.csv_notificacio_numero,
                    linia.csv_notificacio_planta)
            vals.update(extra_vals)
            ubicacio_vals = self.wh_obtenir_ubicacio_partner(
                cursor, uid, linia.csv_notificacio_ine_municipi,
                linia.csv_notificacio_poblacio, linia.csv_notificacio_pais)
            vals.update(ubicacio_vals)
            return c_partner_address.create(cursor, uid, vals)

    def wh_actualitzar_pagador_contacte(self, cursor, uid, linia, pagador_id):
        c_partner_address = self.pool.get('res.partner.address')
        fields_address = c_partner_address.fields_get(cursor, uid).keys()
        # inhabilitar adreces antigues
        search_addrees = [('partner_id', '=', pagador_id)]
        if 'tv' in fields_address:
            street_cleaned = clean_street(linia.csv_pagador_direccio)
            search_addrees += [
                ('nv', '=', street_cleaned['street']),
                ('pnp', '=', linia.csv_pagador_numero),
                ('pt', '=', linia.csv_pagador_planta),
                ('pu', '=', linia.csv_pagador_puerta),
                ('es', '=', linia.csv_pagador_escala),
                ('aclarador', '=', linia.csv_pagador_aclarador),
                ('name', '=', linia.csv_pagador_direccio_nom)
            ]
        else:
            street = '{} {} {}'.format(
                linia.csv_pagador_direccio,
                linia.csv_pagador_numero,
                linia.csv_pagador_planta
            )
            search_addrees += [('street', '=', street)]
        old_addresses_ids = c_partner_address.search(
            cursor, uid, search_addrees)
        if old_addresses_ids:
            return old_addresses_ids[0]
        else:
            vals = {'partner_id': pagador_id,
                    'type': 'invoice'}

            fields = [('name', 'csv_%s_nom'), ('phone', 'csv_%s_telefon'),
                      ('fax', 'csv_%s_fax'), ('email', 'csv_%s_mail'),
                      ('mobile', 'csv_%s_mobil'), ('zip', 'csv_%s_cp'),
                      ('name', 'csv_%s_direccio_nom'),
                      ('street', 'csv_%s_direccio'),
                      ('aclarador', 'csv_%s_aclarador')
                      ]
            tipus = 'pagador'
            if (not linia.csv_pagador_direccio_nom
                    and not linia.csv_pagador_direccio):
                return self.wh_get_direccio(
                    cursor, uid, pagador_id, 'contact')

            extra_vals = self.wh_omplir_dades_generic(linia, fields, tipus)
            if 'tv' in fields_address:
                address_vals = self.wh_extended_address(
                    cursor, uid, extra_vals['street'], linia.csv_pagador_numero,
                    linia.csv_pagador_planta,
                    linia.csv_pagador_escala, linia.csv_pagador_puerta
                )
                extra_vals.update(address_vals)
            else:
                extra_vals['street'] = '{} {} {}'.format(
                    extra_vals['street'],
                    linia.csv_pagador_numero,
                    linia.csv_pagador_planta)
            vals.update(extra_vals)
            ubicacio_vals = self.wh_obtenir_ubicacio_partner(
                cursor, uid, linia.csv_pagador_ine_municipi,
                linia.csv_pagador_poblacio, linia.csv_pagador_pais)
            vals.update(ubicacio_vals)
            return c_partner_address.create(cursor, uid, vals)

    def wh_actualitzar_bank(self, cursor, uid, linia, pagador_id):
        partner_bank_obj = self.pool.get('res.partner.bank')
        iban = linia.csv_fact_compte_bancari
        if not iban:
            return False
        country_code = iban[:2]

        if not country_code or country_code.isdigit():
            country_code = 'ES'

        search = [('partner_id', '=', pagador_id),
                  ('state', '=', 'iban'),
                  ('iban', '=', iban)]
        old_bank = partner_bank_obj.search(cursor, uid, search)
        if old_bank:
            return old_bank[0]
        else:
            country = self.wh_obtenir_pais_id(cursor, uid, country_code)
            vals = {
                'state': 'iban', 'iban': iban,
                'partner_id': pagador_id, 'acc_country_id': country,
                'name': linia.csv_fact_desc_bancari
            }
            res = partner_bank_obj.onchange_iban(
                cursor, uid, 0, iban)
            vals.update({'bank': res['value'].get('bank', False)})

            if (not linia.csv_propietari_cc_nom or
                    not linia.csv_propietari_cc_nif):
                vals.update({'owner_id': pagador_id})
            else:
                partner_obj = self.pool.get('res.partner')
                partner_name = partner_obj.read(
                    cursor, uid, pagador_id, ['name'])['name']
                partner_equals = self.wh_partner_equals(
                    linia.csv_propietari_cc_nom, partner_name)
                if partner_equals:
                    vals.update({'owner_id': pagador_id})
                else:
                    fix_vat = self.wh_fix_vat(linia.csv_propietari_cc_nif)
                    partner_id = self.wh_get_abonat_id(
                        cursor, uid,
                        linia.csv_propietari_cc_nif, linia.csv_propietari_cc_nom
                    )
                    status_message = {
                        'status_message': linia.status_message
                    }
                    state_anterior = linia.state
                    if partner_id:
                        if len(partner_id) > 1:
                            self.wh_add_status_message(
                                state_anterior,
                                u"OK:Hay más de 1 abonado para el CC "
                                u"con Nombre {} NIF '{}' "
                                u"se escoge (id: {})".format(
                                    linia.csv_propietari_cc_nom,
                                    fix_vat, partner_id[0]),
                                status_message
                            )
                            linia.write({'status_message': status_message})
                        partner_id = partner_id[0]
                    else:
                        partner_id = partner_obj.create(cursor, uid, {
                            'name': linia.csv_propietari_cc_nom,
                            'vat': fix_vat,
                        })
                        self.wh_add_status_message(
                            state_anterior,
                            u"OK:Se ha creado un nuevo abonado para CC (id: %d) "
                            u"con Nombre %s NIF '%s'" % (
                                partner_id, linia.csv_propietari_cc_nom,
                                fix_vat),
                            status_message
                        )
                        linia.write({'status_message': status_message})
                    vals.update({'owner_id': partner_id})

            return partner_bank_obj.create(cursor, uid, vals)

    def wh_omplir_dades_pagador(self, cursor, uid, linia):
        valid_nif = super(GiscesoaSyncLinia,
                          self).wh_fix_vat(linia.csv_pagador_nif)
        tipus = 'pagador'
        fields = [('lang', 'csv_%s_idioma'), ('ref', 'csv_%s_codi'),
                  ('name', 'csv_%s_nom')]
        vals = self.wh_omplir_dades_generic(linia, fields, tipus)
        vals['vat'] = valid_nif
        return vals

    def wh_crear_pagador(self, cursor, uid, linia):
        partner_obj = self.pool.get('res.partner')
        #print " -- wh_crear_pagador -- nom:%s -- nif:%s" % (linia.csv_abonat_nom, linia.csv_abonat_nif)
        vals = self.wh_omplir_dades_pagador(cursor, uid, linia)
        return partner_obj.create(cursor, uid, vals)

    def wh_actualitzar_pagador(self, cr, uid, linia, pagador_id):
        c_client = self.pool.get('res.partner')
        vals = self.wh_omplir_dades_pagador(cursor, uid, linia)
        #print " -- wh_actualitzar_pagador -- nom:%s -- nif:%s" % (linia.csv_abonat_nom, linia.csv_abonat_nif)
        c_client.write(cr, uid, [pagador_id], vals)
    # ------------------------------------
    # wc_* -- WORKFLOW CONDITION functions
    # ------------------------------------

    def wc_existeix_pagador(self, cursor, uid, ids):
        obj = self.browse(cursor, uid, ids)[0]
        #print "**** %s:%d: call to     wc_existeix_pagador" % (obj._modelname, obj.id)
        self.wh_assert(obj.state, ['st_crear_client:OK',
                                   'st_actualitzar_client:OK'])
        return obj.tmp_pagador_id != -1


    # ---------------------------------
    # wa_* -- WORKFLOW ACTION functions
    # ---------------------------------

    def wa_actualitzar_mandato(self, cursor, uid, ids):
        linia = self.browse(cursor, uid, ids)[0]
        self.wh_assert(linia.state, ["st_crear_comptador:OK"])
        crear_mandato = self.wh_get_property(cursor, uid, 'crear_mandatos')
        vals = {'status_message': linia.status_message}
        if crear_mandato == '1':
            try:
                self.wh_actualitzar_mandato(cursor, uid, linia.tmp_polissa_id)
                self.wh_add_status_message(
                    "st_actualitzar_mandato:OK",
                    u"OK:se creado el mandato para pòliza %s" % (
                        linia.tmp_polissa_id), vals)
            except:
                vals = {'status_message': linia.status_message}
                self.wh_add_status_message(
                    "st_actualitzar_mandato:OK",
                    u"WARINING:No se ha podido crear el mandato para pòliza %s" % (
                        linia.tmp_polissa_id), vals)
                linia.write(vals)
        return True

    def wa_actualitzar_pagador(self, cursor, uid, ids):
        obj = self.browse(cursor, uid, ids)[0]
        self.wh_assert(obj.state, ["st_actualitzar_client:OK",
                                   'st_crear_client:OK'])
        state_anterior = obj.state
        vals = {'status_message': obj.status_message}
        pagador = self._valid_persona_pagador[obj.csv_fact_persona_pagador]
        try:
            pagador_id = self.wh_get_partner_id(cursor, uid, obj.csv_pagador_nif,
                                             obj.csv_pagador_nom)
            if pagador == 'titular':
                contacte_id = self.wh_actualitzar_pagador_contacte(
                    cursor, uid, obj, pagador_id)
            else:
                self.wh_actualitzar_pagador(cursor, uid, obj, pagador_id)
                self.wh_add_status_message("st_actualitzar_pagador:OK",
                                           u"OK:Se ha actualizado los campos del pagador (id: %d)" % (obj.tmp_abonat_id), vals)
                contacte_id = self.wh_actualitzar_pagador_contacte(cursor, uid, obj, pagador_id)
                self.wh_add_status_message("st_actualitzar_pagador:OK",
                                          u"OK:Se han actualitzado los campos del contacto (id: %d) del pagador (id: %d)" % (
                                               contacte_id, obj.tmp_abonat_id), vals)

            tmp_bank_id = self.wh_actualitzar_bank(cursor, uid, obj, pagador_id)
            vals['tmp_bank_id'] = tmp_bank_id
            obj.write(vals)
            # Tornem a l'estat de la línia anterior perquè el
            # wa_actualitzar_polissa i wa_crear_polissa necessiten aquest estat.
            self.wh_add_status_message(state_anterior,
                                       u"Continuación workflow - sync_comer",
                                       vals)
        except:
            self.wh_add_status_message('st_exception:ERROR',
                                       u"ERROR: exception %s " %
                                       traceback.format_exc(), vals)
        finally:

            obj.write(vals)
        return True

    def wa_crear_pagador(self, cursor, uid, ids):
        obj = self.browse(cursor, uid, ids)[0]
        #print "**** %s:%d: call to wa_crear_client()" % (obj._modelname, obj.id)
        self.wh_assert(obj.state, ["st_crear_client:OK", 'st_actualitzar_client:OK'])
        vals = {'status_message': obj.status_message}

        state_anterior = obj.state
        pagador = self._valid_persona_pagador[obj.csv_fact_persona_pagador]
        try:
            pagador_id = self.wh_get_partner_id(cursor, uid, obj.csv_pagador_nif,
                                             obj.csv_pagador_nom)
            if pagador == 'titular':
                pagador_id = obj.tmp_abonat_id
                contacte_id = self.wh_actualitzar_pagador_contacte(
                    cursor, uid, obj, obj.tmp_abonat_id)

            if pagador != 'titular':
                if pagador_id == -1:
                    pagador_id = self.wh_crear_pagador(cursor, uid, obj)
                    self.wh_add_status_message(
                        "st_crear_client:OK",
                        u"OK:Se ha creado un nuevo pagador (id: %d) con NIF '%s'" % (pagador_id, obj.csv_pagador_nif),
                        vals)
                contacte_id = self.wh_actualitzar_pagador_contacte(
                    cursor, uid, obj, pagador_id)
                self.wh_add_status_message(
                    "st_crear_client:OK",
                    u"OK:Se han actualitzado los campos del contacto (id: %d) del pagador (id: %d)" % (contacte_id, pagador_id),
                    vals)

            vals['tmp_pagador_id'] = pagador_id
            tmp_bank_id = self.wh_actualitzar_bank(cursor, uid, obj, pagador_id)
            vals['tmp_bank_id'] = tmp_bank_id
            obj.write(vals)
            # Tornem a l'estat de la línia anterior perquè el
            # wa_actualitzar_polissa i wa_crear_polissa necessiten aquest estat.
            self.wh_add_status_message(state_anterior,
                                       u"Continuación workflow - sync_comer",
                                       vals)
        except:
            self.wh_add_status_message('st_exception:ERROR',
                                       "ERROR: exception %s " %
                                       traceback.format_exc(), vals)
        finally:
            obj.write(vals)
        return True

    def wa_validacio_general(self, cr, uid, ids):
        obj = self.browse(cr, uid, ids)[0]
        #print "**** %s:%d: call to wa_validacio_general()" % (obj._modelname, obj.id)
        self.wh_assert(obj.state, ['st_dividir_linia:OK'])
        vals = {"status_message": obj.status_message, "state": "st_validacio_general:OK"}
        try:
            # comprovem alguns camps que no poden tenir qualsevol valor
            # busquem l'id de la distribuidora
            tmp = self.wh_get_distribuidora_id(cr, uid, obj.csv_cups_distribuidora)

            if len(tmp) > 1:
                self.wh_add_status_message('st_validacio_general:ERROR',
                                           u"ERROR:Hay más de un partner con el mismo codigo de distribuidora: %s (los IDs son %s)." % (
                                               obj.csv_cups_distribuidora, ", ".join(map(str, tmp))), vals)
            elif tmp:
                vals['tmp_distribuidora_id'] = tmp[0]
            else:
                self.wh_add_status_message('st_validacio_general:ERROR',
                                           u"ERROR: El codigo de la distribuidora es obligatorio.", vals)

            if not obj.csv_fact_facturacio_tipus in self._valid_tipus_facturacio:
                self.wh_add_status_message('st_validacio_general:ERROR',
                                           u"ERROR:El campo %d (csv_fact_facturacio_tipus) contiene \"%s\" pero ha de tener uno de estos valores: [\"%s\"]" % (
                                               self._reverse_csv_fields['csv_fact_facturacio_tipus'],
                                               obj.csv_fact_facturacio_tipus,
                                               "\",\"".join([x for x in self._valid_tipus_facturacio])),
                                           vals)
            if not obj.csv_fact_facturacio_potencia in self._valid_facturacio_potencia:
                self.wh_add_status_message('st_validacio_general:ERROR',
                                           u"ERROR:El campo %d (csv_fact_facturacio_potencia) contiene \"%s\" pero ha de tener uno de estos valores: [\"%s\"]" % (
                                               self._reverse_csv_fields['csv_fact_facturacio_potencia'],
                                               obj.csv_fact_facturacio_potencia,
                                               "\",\"".join([x for x in self._valid_facturacio_potencia])),
                                           vals)
            tarifa_comer = self.wh_obtenir_tarifa_comer(cr, uid, obj.csv_fact_tarifa_comercialitzadora)
            if not tarifa_comer:
                self.wh_add_status_message('st_validacio_general:ERROR',
                                           u"ERROR:El campo %d (csv_fact_tarifa_comercialitzadora) contiene \"%s\" pero no es una tarifa de comercializadora valido" % (
                                               self._reverse_csv_fields['csv_fact_tarifa_comercialitzadora'],
                                               obj.csv_fact_tarifa_comercialitzadora)
                                           , vals)
            else:
                vals['tmp_tarifa_comer_id'] = tarifa_comer

            # busquem l'id del pagador a res.partner
            tmp = super(GiscesoaSyncLinia, self).wh_get_abonat_id(cr, uid, obj.csv_pagador_nif, obj.csv_pagador_nom)
            if len(tmp) > 1:
                vals['tmp_pagador_id'] = tmp[0]
                self.wh_add_status_message('st_validacio_general:OK',
                                           u"NOTIFICACIÓN:Hay más de un pagador con el nombre y nif que se especifica (los IDs son %s). Se ha escogido el primero %s" % (
                                               ", ".join(map(str, tmp)),  vals['tmp_pagador_id']), vals)
            elif tmp:
                vals['tmp_pagador_id'] = tmp[0]
            else:
                vals['tmp_pagador_id'] = -1

            if not self.wh_validacio_bank(cr, uid, obj.csv_fact_compte_bancari):
                self.wh_add_status_message('st_validacio_general:ERROR',
                                           u"ERROR:El campo %d (csv_fact_compte_bancari) contiene \"%s\" pero no es una cuenta válida" % (
                                               self._reverse_csv_fields['csv_fact_compte_bancari'],
                                               obj.csv_fact_compte_bancari),
                                           vals)

            if not obj.csv_fact_persona_pagador in self._valid_persona_pagador:
                self.wh_add_status_message('st_validacio_general:ERROR',
                                           u"ERROR:El campo %d (csv_fact_persona_pagador) contiene \"%s\" pero ha de tener uno de estos valores: [\"%s\"]" % (
                                               self._reverse_csv_fields['csv_fact_persona_pagador'],
                                               obj.csv_fact_persona_pagador,
                                               "\",\"".join([x for x in self._valid_persona_pagador])),
                                           vals)

            if not obj.csv_fact_persona_notificacio in self._valid_persona_notificacio:
                self.wh_add_status_message('st_validacio_general:ERROR',
                                           u"ERROR:El campo %d (csv_fact_persona_notificacio) contiene \"%s\" pero ha de tener uno de estos valores: [\"%s\"]" % (
                                               self._reverse_csv_fields['csv_fact_persona_notificacio'],
                                               obj.csv_fact_persona_pagador,
                                               "\",\"".join([x for x in self._valid_persona_notificacio])),
                                           vals)
            obj.write(vals)
            if self.wh_vals_sense_error(vals):
                super(GiscesoaSyncLinia, self).wa_validacio_general(cr, uid, ids)
            return True

        except:
            self.wh_add_status_message('st_exception:ERROR',
                                       "ERROR: exception %s " %
                                       traceback.format_exc(), vals)
            obj.write(vals)

        return True

GiscesoaSyncLinia()
