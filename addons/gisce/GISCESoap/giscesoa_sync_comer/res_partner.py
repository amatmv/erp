# -*- coding: utf-8 -*-

from osv import osv

class res_partner(osv.osv):
    _name = 'res.partner'
    _inherit = 'res.partner'
    def simple_vat_check(self, cursor, uid, country_code, vat_number,
                         context=None):
        conf_obj = self.pool.get('giscesoa.sync.config')
        validar_nif = int(conf_obj.get_property(cursor, uid, 'giscesoa_sync',
                                        "validar_nif", "1"))
        if validar_nif:
            return super(res_partner, self).simple_vat_check(
                cursor, uid,country_code, vat_number, context=None)
        else:
            return True

res_partner()