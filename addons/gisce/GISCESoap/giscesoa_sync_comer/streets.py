# -*- coding: utf-8 -*-
import re


TIPOS_VIA = {
    'AV': {'codi': 'AV', 'name': 'Avenida', 'abr': 'AV'},
    'BO': {'codi': 'BO', 'name': 'Barrio', 'abr': 'BO'},
    'CJ': {'codi': 'CJ', 'name': u'Callejón', 'abr': 'CJ'},
    'CL': {'codi': 'CL', 'name': 'Calle', 'abr': 'CL'},
    'CM': {'codi': 'CM', 'name': 'Camino', 'abr': 'CM'},
    'CN': {'codi': 'CN', 'name': 'Colonia', 'abr': 'CN'},
    'CR': {'codi': 'CR', 'name': 'Carretera', 'abr': 'CR'},
    'PB': {'codi': 'PB', 'name': 'Poblado', 'abr': 'PB'},
    'PD': {'codi': 'PD', 'name': 'Partida', 'abr': 'PD'},
    'PJ': {'codi': 'PJ', 'name': 'Pasaje', 'abr': 'PJ'},
    'PL': {'codi': 'PL', 'name': 'Poligono', 'abr': 'PL'},
    'PQ': {'codi': 'PQ', 'name': 'Parque', 'abr': 'PQ'},
    'PS': {'codi': 'PS', 'name': 'Paseo', 'abr': 'PS'},
    'PZ': {'codi': 'PZ', 'name': 'Plaza', 'abr': 'PZ'},
    'RB': {'codi': 'RB', 'name': 'Rambla', 'abr': 'RB'},
    'RD': {'codi': 'RD', 'name': 'Ronda', 'abr': 'RD'},
    'TR': {'codi': 'TR', 'name': 'Travesia', 'abr': 'TR'},
    'UR': {'codi': 'UR', 'name': u'Urbanización', 'abr': 'UR'},
}

MAPEIG_TIPO_VIA = {
    'AV': ['AVEWNIDA', 'AVINGUDA', 'AVENIDA', 'AVDA .', 'AVDAL.', 'AVDA.',
           'AVDA', 'AVD.', 'AV/', 'AV.', 'AVD', 'AV'],
    'BO': ['BARRIO', 'BARRI'],
    'CL': ['CARRER', 'CALLE', 'C:', 'C/'],
    'CJ': ['CALLEJON-PZA', u'CALLEJÓN', 'CALLEJON'],
    'CM': ['CAMINOS', 'CAMINI', 'CAMINO', u'CAMÍ', 'CMNO.', 'CAMI', 'CMNO',
           'CNO.', 'CNO', 'CM'],
    'CR': ['CARRETERAN', 'CARRETERAS', 'CARRETEWRA', 'CARRETERA', 'CARRETWRA',
           'CARRETRA', 'CRTRA.', 'CTRA .', 'CRTA.', 'CRTRA', 'CTRA.', 'CRA.',
           'CRTA', 'CTA.', 'CTRA', 'CR.', 'CR'],
    'PQ': ['PARQUE'],
    'PJ': ['PASAJE', 'PJE.', 'PTGE', 'PJE', 'PJ.'],
    'PS': ['PASSATGES', 'PASSATGE', 'PASSEIG', 'PASEO', 'PSEO.', 'PSEO', 'PSJE',
           u'Pº', 'PSO', 'PS.', 'PS'],
    'PD': ['PDA.', 'PDA', 'PD.', 'PD'],
    'PL': [u'POLÍGONO', 'Poligono', 'POLIGONO', 'POLGONO', 'POLIGON', 'PLGNO.',
           'POLIG.', 'PLGO.', 'POLG.', 'POLIG', 'POLI.', 'PLGO', 'PLG.', 'PLO.',
           'POLG', 'Pol.', 'POL.', 'PG.', 'PLG', 'POL', 'PG'],
    'PZ': ['PLAÇA', 'PLACA', 'PLAZA', 'PZA.', 'PL.', 'PLZ', 'PZA', 'PZ.', 'PL',
           'PZ'],
    'PB': ['POBLADO', 'POB.'],
    'RB': ['RAMBLA'],
    'RD': ['RONDA'],
    'TR': ['TRAVESIA-', u'TRAVESÍA', 'TRAVESIA'],
    'UR': [u'URBANIZACIÓN', 'URBANIZACION', 'URBANITZAC.', 'URBANIZAC.',
           'URBANIZ.', 'URBANI.', 'URBANI', 'URB.', 'Urb', 'URB']
}

def clean_street(street):
    found = False
    tpv = None
    key = None
    if len(street.split('|')) > 1:
        tpv = street.split('|')[0].strip()
        street = (' '.join(street.split('|')[1:])).strip()
    else:
        for tipovia in MAPEIG_TIPO_VIA:
            tpv = tipovia
            for pre in MAPEIG_TIPO_VIA[tpv] + [tpv]:
                found = street.upper().startswith('{} '.format(pre))
                if found:
                    key = pre
                    break
            if found:
                break
        if found:
            insensitive_replace = re.compile(key, re.IGNORECASE)
            # replace only the first occurence
            street = insensitive_replace.sub('', street, 1).strip()
        else:
            tpv = 'CL'
    return {'street': street, 'tv': tpv}
