#!/bin/bash

OLDIFS=$IFS
IFS="
"
ENTRADA='/home/server/SOA/entrada'
SCRIPTS='/home/server/scripts'
LOG=$ENTRADA'/data.log'

for file in `find $ENTRADA -name "*.txt"`
do
    echo $file
    python $SCRIPTS'/'parsecsv.py --file=$file
    rm $file
    echo [`date`] "Processant fitxer: $file" >> $LOG
    for fileout in `find $ENTRADA -name "*.soa"`
    do
        chown erp $fileout
    done;
done;

IFS=$OLDIFS
