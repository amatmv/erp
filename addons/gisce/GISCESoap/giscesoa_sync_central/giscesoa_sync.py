# -*- coding: utf-8 -*-

from osv import osv, fields


class GiscesoaSyncLinia(osv.osv):
    """Afegeix la centralitzacíó al SOA
    """

    _name = 'giscesoa.sync.linia'
    _inherit = 'giscesoa.sync.linia'

    _csv_central_field = {
        60: {'name': 'csv_cups_central_ordre', 'size': 10, 'caption': 'cups__ordre___centralitzacio', 'tipus': 'char'}}

    def __init__(self, pool):
        super(GiscesoaSyncLinia, self).__init__(pool)
        self._csv_fields.update(self._csv_central_field)
        # Actualitzar el _columns
        for index, details in self._csv_central_field.iteritems():
            fnom = details['name']
            fcaption = details['caption']
            fsize = details['size']
            self._columns[fnom] = fields.char(fcaption, size=fsize)

    def wh_omplir_camps_cups(self, linia):
        """Actualiza els valors a actualitzar amb la centralització
        """
        vals = super(GiscesoaSyncLinia, self).wh_omplir_camps_cups(linia)
        vals.update({'central_ordre': linia.csv_cups_central_ordre})
        return vals


GiscesoaSyncLinia()
