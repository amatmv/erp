# -*- coding: utf-8 -*-
{
    "name": "Sync centralització a través de SOA",
    "description": """Habilitar la sincronització de la centralització a través de SOA""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE_SOA",
    "depends":[
        "base",
        "giscesoa_sync",
        "giscedata_cups_central"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
