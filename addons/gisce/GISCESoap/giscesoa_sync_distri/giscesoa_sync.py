# -*- coding: utf-8 -*-
from __future__ import absolute_import
import datetime
from osv import osv, fields

from giscesoa_sync.giscesoa_sync import _wkf_linia_states

import base64
import time
import os
import shutil
import re
import netsvc
import pooler
import traceback

# el camp 'state' ha de contenir sempre un d'aquests valors
# sembla que OpenObject no ho comprova, i el que es clar a
# hores d'ara es que aquesta variable no te cap influencia
# a la navegacio pel workflow, excepte quan es consulta a
# les condicions explicitament.
_wkf_linia_states += [
    ('st_crear_butlleti:OK', 'st_crear_butlleti:OK'),
    ('st_crear_butlleti:ERROR', 'st_crear_butlleti:ERROR'),
    ('st_actualitzar_butlleti:OK', 'st_actualitzar_butlleti:OK'),
    ('st_actualitzar_butlleti:ERROR', 'st_actualitzar_butlleti:ERROR'),
    ('st_crear_icp:OK', 'st_crear_icp:OK'),
    ('st_crear_icp:ERROR', 'st_crear_icp:ERROR'),
    ('st_actualitzar_icp:OK', 'st_actualitzar_icp:OK'),
    ('st_actualitzar_icp:ERROR', 'st_actualitzar_icp:ERROR'),
]


class GiscesoaSyncFitxer(osv.osv):
    """
    Class to represent a file of SOA
    """
    _name = 'giscesoa.sync.fitxer'
    _inherit = 'giscesoa.sync.fitxer'

    _posicio_linia = {
        'NIF': 58,
        'COMER': 32,
        'NOM': 9,
        'CUPS': 0,
        'DATA_ALTA': 19,
        'DATA_BAIXA': 21,
    }

GiscesoaSyncFitxer()


class GiscesoaSyncLinia(osv.osv):
    """
    Class to represent a line of a SOA file
    """
    _name = 'giscesoa.sync.linia'
    _inherit = 'giscesoa.sync.linia'

    _columns = {}
    _csv_fields = {
        1: {'name': 'csv_cups_codi', 'size': 30, 'caption': u'CUPS', 'tipus': 'char'},
        2: {'name': 'csv_cups_municipi', 'size': 10, 'caption': u'Municipio', 'tipus': 'integer'},
        3: {'name': 'csv_cups_poblacio', 'size': 60, 'caption': u'Población', 'tipus': 'char'},
        4: {'name': 'csv_cups_carrer', 'size': 256, 'caption': u'Calle', 'tipus': 'char'},
        5: {'name': 'csv_cups_numero', 'size': 10, 'caption': u'Número', 'tipus': 'char'},
        6: {'name': 'csv_cups_escala', 'size': 50, 'caption': u'Escalera', 'tipus': 'char'},
        7: {'name': 'csv_cups_planta', 'size': 50, 'caption': u'Planta', 'tipus': 'char'},
        8: {'name': 'csv_cups_pis', 'size': 10, 'caption': u'Piso', 'tipus': 'char'},
        9: {'name': 'csv_abonat_codi', 'size': 60, 'caption': u'Código abonado', 'tipus': 'char'},
        10: {'name': 'csv_abonat_nom', 'size': 128, 'caption': u'Nombre cliente', 'tipus': 'char'},
        11: {'name': 'csv_abonat_telefon', 'size': 64, 'caption': u'Teléfono', 'tipus': 'char'},
        12: {'name': 'csv_abonat_fax', 'size': 64, 'caption': u'FAX', 'tipus': 'char'},
        13: {'name': 'csv_abonat_mail', 'size': 240, 'caption': u'Email', 'tipus': 'char'},
        14: {'name': 'csv_polissa_codi_comptador', 'size': 20, 'caption': u'Núm. contador', 'tipus': 'char'},
        15: {'name': 'csv_polissa_codi_comptador_2', 'size': 20, 'caption': u'Núm. contador 2',
             'tipus': 'char'},
        16: {'name': 'csv_polissa_codi_comptador_3', 'size': 20, 'caption': u'Núm. contador 3',
             'tipus': 'char'},
        17: {'name': 'csv_polissa_codi_comptador_reactiva', 'size': 20, 'caption': u'Núm. contador reactiva',
             'tipus': 'char'},
        18: {'name': 'csv_polissa_codi_contracte', 'size': 60, 'caption': u'Núm. póliza (contrato)', 'tipus': 'char'},
        19: {'name': 'csv_polissa_tarifa', 'size': 10, 'caption': u'Tarifa', 'tipus': 'char'},
        20: {'name': 'csv_polissa_data_alta', 'size': 10, 'caption': u'Fecha alta', 'tipus': 'date'},
        21: {'name': 'csv_polissa_donar_de_baixa', 'size': 1, 'caption': u'Baja',
             'tipus': 'boolean'},
        22: {'name': 'csv_polissa_data_baixa', 'size': 10, 'caption': u'Fecha baja', 'tipus': 'date'},
        23: {'name': 'csv_cups_zona', 'size': 10, 'caption': u'Zona', 'tipus': 'char'},
        24: {'name': 'csv_cups_ordre', 'size': 10, 'caption': u'Orden', 'tipus': 'char'},
        25: {'name': 'csv_polissa_potencia', 'size': 11, 'caption': u'Potencia', 'tipus': 'float'},
        26: {'name': 'csv_polissa_tensio', 'size': 10, 'caption': u'Tensión', 'tipus': 'char'},
        27: {'name': 'csv_polissa_consum_anual', 'size': 10, 'caption': u'Consumo anual', 'tipus': 'integer'},
        28: {'name': 'csv_cups_et', 'size': 60, 'caption': u'ET', 'tipus': 'char'},
        29: {'name': 'csv_cups_linia', 'size': 60, 'caption': u'Línea', 'tipus': 'char'},
        30: {'name': 'csv_abonat_vol_avis_descarreg', 'size': 1, 'caption': u'Aviso descargo',
             'tipus': 'boolean'},
        31: {'name': 'csv_polissa_tipus_avis', 'size': 10, 'caption': u'Tipo aviso', 'tipus': 'char'},
        32: {'name': 'csv_polissa_activitat_cnae', 'size': 10, 'caption': u'Actividad CNAE', 'tipus': 'char'},
        33: {'name': 'csv_polissa_comercialitzadora', 'size': 4, 'caption': u'Comercializadora',
             'tipus': 'char'},
        34: {'name': 'csv_polissa_observacions', 'size': 1024, 'caption': u'Observaciones', 'tipus': 'text'},
        35: {'name': 'csv_polissa_titular', 'size': 128, 'caption': u'Titular', 'tipus': 'char'},
        36: {'name': 'csv_ignorar_polissa_codi_dh', 'size': 10, 'caption': u'Discriminación',
             'tipus': 'char'},
        37: {'name': 'csv_polissa_estat', 'size': 60, 'caption': u'Estado de la póliza', 'tipus': 'char'},
        38: {'name': 'csv_polissa_servei', 'size': 60, 'caption': u'Servício', 'tipus': 'char'},
        39: {'name': 'csv_polissa_icp', 'size': 10, 'caption': u'ICP', 'tipus': 'float'},
        40: {'name': 'csv_polissa_fase', 'size': 60, 'caption': u'Fase', 'tipus': 'char'},
        41: {'name': 'csv_polissa_distribuidora', 'size': 4, 'caption': u'Distribuidora', 'tipus': 'char'},
        42: {'name': 'csv_polissa_coordenades', 'size': 10, 'caption': u'Coordenadas', 'tipus': 'char'},
        43: {'name': 'csv_ignorar_provincia', 'size': 60, 'caption': u'Provincia', 'tipus': 'char'},
        44: {'name': 'csv_polissa_potencia_max_installador', 'size': 11,
             'caption': u'Potencia Máx. instalador', 'tipus': 'float'},
        45: {'name': 'csv_polissa_tipus_punt_mesura', 'size': 2, 'caption': u'Tipo punto medida',
             'tipus': 'char'},
        46: {'name': 'csv_polissa_installacio_icp', 'size': 1, 'caption': u'Disponibilidad ICP', 'tipus': 'char'},
        47: {'name': 'csv_ignorar_tipus_perfil_consum', 'size': 99, 'caption': u'Tipo perfil consumo',
             'tipus': 'char'},
        48: {'name': 'csv_polissa_drets_extensio', 'size': 10, 'caption': u'Derechos de extensión',
             'tipus': 'float'},
        49: {'name': 'csv_polissa_drets_acces', 'size': 10, 'caption': u'Derechos de acceso', 'tipus': 'float'},
        50: {'name': 'csv_polissa_propietat_equip_mesura', 'size': 60, 'caption': u'Propiedad equipo medida',
             'tipus': 'char'},
        51: {'name': 'csv_polissa_propietat_icp', 'size': 60, 'caption': u'Propiedad ICP', 'tipus': 'char'},
        52: {'name': 'csv_polissa_potencies_per_periode', 'size': 128, 'caption': u'Potencia por periodo',
             'tipus': 'char'},
        53: {'name': 'csv_polissa_data_ultim_canvi_tarifa', 'size': 10,
             'caption': u'Fecha movimiento tarifa', 'tipus': 'date'},
        54: {'name': 'csv_polissa_data_ultim_canvi_comercialitzadora', 'size': 10,
             'caption': u'Fecha movimiento comer.', 'tipus': 'date'},
        55: {'name': 'csv_polissa_data_limit_drets_extensio', 'size': 10,
             'caption': u'Fecha límite derechos extensión', 'tipus': 'date'},
        56: {'name': 'csv_polissa_consum_ultims_dos_anys', 'size': 10, 'caption': u'Consumo últimos 2 años',
             'tipus': 'integer'},
        57: {'name': 'csv_polissa_data_ultima_lectura', 'size': 10, 'caption': u'Fecha última lectura',
             'tipus': 'date'},
        58: {'name': 'csv_polissa_numero_fases', 'size': 10, 'caption': u'Número de fases', 'tipus': 'integer'},
        59: {'name': 'csv_abonat_nif', 'size': 32, 'caption': u'NIF abonado', 'tipus': 'char'},
    }

    _reverse_csv_fields = {}
    for k in _csv_fields.keys():
        _reverse_csv_fields[_csv_fields[k]['name']] = k

    # afegim els camps 'temporals' que corresponen a les columnes del fitxer csv
    for index, details in _csv_fields.iteritems():
        fnom = details['name']
        fcaption = details['caption']
        fsize = details['size']
        _columns[fnom] = fields.char(fcaption, size=fsize)

    # ------------------------------------
    # wh_* -- WORKFLOW HELPERS :)
    # ------------------------------------

    def wh_omplir_camps_icp(self, linia):
        propietat = None

        if linia.csv_polissa_installacio_icp == 'C':
            propietat = 'client'
        elif linia.csv_polissa_installacio_icp == 'D':
            propietat = 'empresa'

        return {
            'name': linia.tmp_polissa_id,
            'polissa_id': linia.tmp_polissa_id,
            'intensitat': linia.csv_polissa_icp,
            'propietat': propietat,
        }

    def wh_omplir_camps_butlleti(self, linia):
        return {'name': linia.tmp_polissa_id,
                'cups_id': linia.tmp_cups_id,
                'pot_max_admisible':
                    linia.csv_polissa_drets_extensio,
                'pot_installada': linia.csv_polissa_drets_acces,
                }

    def wh_actualitzar_butlleti(self, cr, uid, linia):
        butlleti_obj = self.pool.get('giscedata.butlleti')

        butlleti_list = butlleti_obj.search(cr, uid,
                                [('cups_id', '=', linia.tmp_cups_id),
                                 ('name', '=', linia.tmp_polissa_id)])
        tmp_butlleti_id = butlleti_list[0]
        vals = self.wh_omplir_camps_butlleti(linia)
        butlleti_obj.write(cr, uid, [tmp_butlleti_id], vals)

        return tmp_butlleti_id

    def wh_actualitzar_icp(self, cr, uid, linia):
        icp_obj = self.pool.get('giscedata.polissa.icp')

        icp_list = icp_obj.search(cr, uid,
                                  [('polissa_id', '=', linia.tmp_polissa_id),
                                   ('name', '=', linia.tmp_polissa_id)])
        tmp_icp_id = icp_list[0]
        vals = self.wh_omplir_camps_icp(linia)
        icp_obj.write(cr, uid, [tmp_icp_id], vals)

        return tmp_icp_id

    def wh_crear_butlleti(self, cr, uid, linia):
        butlleti_obj = self.pool.get('giscedata.butlleti')
        vals = self.wh_omplir_camps_butlleti(linia)
        return butlleti_obj.create(cr, uid, vals)

    def wh_crear_icp(self, cr, uid, linia):
        icp_obj = self.pool.get('giscedata.polissa.icp')

        vals = self.wh_omplir_camps_icp(linia)

        return icp_obj.create(cr, uid, vals)

    def wh_omplir_camps_cups(self, cr, uid, linia):
        """
        Workflow helper to get the data to update on CUPS

        :param cr: Database cursor
        :param uid: User id
        :type uid: int
        :param linia: Linia of the file
        :type linia: list of str
        :return: Fields to update on CUPS
        :rtype: dict
        """

        id_poblacio = self.wh_get_poblacio_id(cr, uid, linia.tmp_municipi_id,
                                              linia.csv_cups_poblacio)
        separar_carrer = int(self.wh_get_property(
            cr, uid, "separar_carrer", False))

        separador_aclarador = self.wh_get_property(
            cr, uid, "separador_aclarador", ",")

        cups_data = {
            'active': 1,
            'id_municipi': linia.tmp_municipi_id,
            'id_poblacio': id_poblacio,
            'pnp': linia.csv_cups_numero,
            'es': linia.csv_cups_escala,
            'pt': linia.csv_cups_planta,
            'pu': linia.csv_cups_pis,
            'zona': linia.csv_cups_zona,
            'ordre': linia.csv_cups_ordre,
            'potencia_conveni': linia.csv_polissa_potencia_max_installador,
            'cne_anual_activa': linia.csv_polissa_consum_anual,
        }

        if separar_carrer:
            carrer = linia.csv_cups_carrer
            indx_sep = carrer.find(separador_aclarador)
            if indx_sep != -1:
                cups_data["nv"] = carrer[:indx_sep].strip()
            else:
                cups_data["nv"] = carrer.strip()
            if carrer.find(separador_aclarador) != -1:
                cups_data["aclarador"] = carrer[indx_sep + 1:].strip()
                cups_data["pnp"] = False
        else:
            cups_data["nv"] = linia.csv_cups_carrer
        return cups_data

    def wh_omplir_camps_polissa(self, cr, uid, linia):
        """
        Fills the fields of the polissa

        :param cursor: Database cursor
        :param uid: User id
        :param linia: Readed line
        :return: Values to write into the polissa
        :rtype: dict
        """

        vals = super(GiscesoaSyncLinia, self).wh_omplir_camps_polissa(
            cr, uid, linia
        )

        if linia.csv_polissa_tensio:
            tensio_normalitzada = self.wh_get_tensio_normalitzada_id(
                cr, uid, linia.csv_polissa_tensio)
        else:
            tensio_normalitzada = None

        vals.update({
            'active': 1,
            'name': linia.csv_polissa_codi_contracte,
            'renovacio_auto': 1,
            'codi': linia.csv_abonat_codi,
            'data_alta': linia.csv_polissa_data_alta,
            'data_firma_contracte': linia.csv_polissa_data_alta,
            'data_baixa': False,
            'potencia': linia.csv_polissa_potencia,
            'tensio_normalitzada': tensio_normalitzada,
            'observacions': linia.csv_polissa_observacions,
            'tarifa': linia.tmp_tarifa_id,
            'titular': linia.tmp_abonat_id,
            'cups': linia.tmp_cups_id,
            'comercialitzadora': linia.tmp_comercialitzadora_id,
        })

        misc_cnae = self.pool.get('giscemisc.cnae')
        cnae = linia.csv_polissa_activitat_cnae
        id_cnae = misc_cnae.search(cr, uid, [('name', '=', cnae)])

        if id_cnae:
            vals.update({'cnae': id_cnae[0]})

        return vals

    def wc_existeix_butlleti(self, cr, uid, ids):
        obj = self.browse(cr, uid, ids)[0]
        # print "**** %s:%d: call to     wc_existeix_polissa" % (obj._modelname, obj.id)
        self.wh_assert(obj.state, ['st_comprovar_comptador:OK', 'st_crear_comptador:OK'])

        butlleti_obj = self.pool.get('giscedata.butlleti')
        butlleti_list = butlleti_obj.search(cr, uid,
                                [('cups_id', '=', obj.tmp_cups_id),
                                 ('name', '=', obj.tmp_polissa_id)])
        if butlleti_list:
            tmp_butlleti_id = butlleti_list[0]
        else:
            tmp_butlleti_id = -1

        return tmp_butlleti_id != -1

    def wc_existeix_icp(self, cr, uid, ids):
        obj = self.browse(cr, uid, ids)[0]

        self.wh_assert(obj.state, ['st_crear_butlleti:OK',
                                   'st_actualitzar_butlleti:OK'])

        icp_obj = self.pool.get('giscedata.polissa.icp')
        search_params = [('polissa_id', '=', obj.tmp_polissa_id),
                         ('name', '=', obj.tmp_polissa_id)]
        icp_list = icp_obj.search(cr, uid, search_params)
        if icp_list:
            tmp_icp_id = icp_list[0]
        else:
            tmp_icp_id = -1

        return tmp_icp_id != -1

    # ---------------------------------
    # wa_* -- WORKFLOW ACTION functions
    # ---------------------------------

    def wa_actualitzar_butlleti(self, cr, uid, ids):
        obj = self.browse(cr, uid, ids)[0]

        self.wh_assert(obj.state, ['st_comprovar_comptador:OK',
                                   'st_crear_comptador:OK'])

        vals = {'status_message': obj.status_message}
        try:
            tmp_butlleti_id = self.wh_actualitzar_butlleti(cr, uid, obj)
            self.wh_add_status_message("st_actualitzar_butlleti:OK",
                                       "OK:S'han actualitzat els camps del butlleti"
                                       " (id: %d)" % tmp_butlleti_id, vals)
        except:
            self.wh_add_status_message('st_exception:ERROR',
                                       "ERROR: exception %s " %
                                       traceback.format_exc(), vals)
        finally:
            obj.write(vals)

        return True

    def wa_actualitzar_icp(self, cr, uid, ids):
        obj = self.browse(cr, uid, ids)[0]
        self.wh_assert(obj.state, ['st_actualitzar_butlleti:OK',
                                   'st_crear_butlleti:OK'])

        vals = {'status_message': obj.status_message}
        try:
            tmp_icp_id = self.wh_actualitzar_icp(cr, uid, obj)
            self.wh_add_status_message("st_actualitzar_icp:OK",
                                       "OK:S'han actualitzat els camps del ICP"
                                       " (id: %d)" % tmp_icp_id, vals)

        except:
            self.wh_add_status_message('st_exception:ERROR',
                                       "ERROR: exception %s " %
                                       traceback.format_exc(), vals)
        finally:
            obj.write(vals)
        return True

    def wa_crear_butlleti(self, cr, uid, ids):
        obj = self.browse(cr, uid, ids)[0]
        #print "**** %s:%d: call to wa_crear_client()" % (obj._modelname, obj.id)
        self.wh_assert(obj.state, ['st_comprovar_comptador:OK',
                                   'st_crear_comptador:OK'])

        vals = {'status_message': obj.status_message}
        try:
            butlleti_id = self.wh_crear_butlleti(cr, uid, obj)

            self.wh_add_status_message("st_crear_butlleti:OK",
                                   "OK:S'ha creat un nou butlleti (id: %d)" %
                                   butlleti_id, vals)
        except:
            self.wh_add_status_message('st_exception:ERROR',
                                       "ERROR: exception %s " %
                                       traceback.format_exc(), vals)
        finally:
            obj.write(vals)
        return True

    def wa_crear_icp(self, cr, uid, ids):
        obj = self.browse(cr, uid, ids)[0]

        self.wh_assert(obj.state, ['st_actualitzar_butlleti:OK',
                                   'st_crear_butlleti:OK'])

        vals = {'status_message': obj.status_message}
        try:
            tmp_icp_id = self.wh_crear_icp(cr, uid, obj)

            self.wh_add_status_message("st_crear_icp:OK",
                                       "OK:S'ha creat un nou icp (id: %d)" %
                                       tmp_icp_id, vals)
        except:
            self.wh_add_status_message('st_exception:ERROR',
                                       "ERROR: exception %s " %
                                       traceback.format_exc(), vals)
        finally:
            obj.write(vals)

        return True

    def wa_validacio_general(self, cr, uid, ids):
        """
        Workflow assistent de validacio general

        :param cr: Database cursor
        :param uid: User id
        :param uid: int
        :param ids: Workflow ids
        :type ids: list of int
        :return: If the data is valid
        :rtype: bool
        """
        obj = self.browse(cr, uid, ids)[0]
        #print "**** %s:%d: call to wa_validacio_general()" % (obj._modelname, obj.id)
        self.wh_assert(obj.state, ['st_dividir_linia:OK'])
        vals = {"status_message": obj.status_message, "state": "st_validacio_general:OK"}
        try:
            # comprovem alguns camps que no poden tenir qualsevol valor
            #_valid_tipus_punt_mesura = ['', '01', '02', '03', '04', '05']
            _valid_polissa_installacio_icp = ['', 'Y', 'C', 'D']
            if not obj.csv_polissa_installacio_icp in _valid_polissa_installacio_icp:
                self.wh_add_status_message('st_validacio_general:ERROR',
                                           u"ERROR:El camp %d (csv_polissa_installacio_icp) contè \"%s\" però ha de tenir un d'aquests valors: [\"%s\"]" % (
                                               self._reverse_csv_fields['csv_polissa_installacio_icp'],
                                               obj.csv_polissa_installacio_icp,
                                               "\",\"".join(_valid_polissa_installacio_icp)),
                                           vals)
            super(GiscesoaSyncLinia, self).wh_check_and_update_date_or_null(vals, obj.csv_polissa_data_ultim_canvi_tarifa,
                                                  'csv_polissa_data_ultim_canvi_tarifa', 'st_validacio_general:ERROR')
            super(GiscesoaSyncLinia, self).wh_check_and_update_date_or_null(vals, obj.csv_polissa_data_ultim_canvi_comercialitzadora,
                                                  'csv_polissa_data_ultim_canvi_comercialitzadora',
                                                  'st_validacio_general:ERROR')
            super(GiscesoaSyncLinia, self).wh_check_and_update_date_or_null(vals, obj.csv_polissa_data_limit_drets_extensio,
                                                  'csv_polissa_data_limit_drets_extensio', 'st_validacio_general:ERROR')
            super(GiscesoaSyncLinia, self).wh_check_and_update_date_or_null(vals, obj.csv_polissa_data_ultima_lectura,
                                                  'csv_polissa_data_ultima_lectura', 'st_validacio_general:ERROR')
            obj.write(vals)
            if self.wh_vals_sense_error(vals):
                return super(GiscesoaSyncLinia, self).wa_validacio_general(cr, uid, ids)

        except Exception:
            self.wh_add_status_message('st_exception:ERROR',
                                       "ERROR: exception %s " %
                                       traceback.format_exc(), vals)
            obj.write(vals)

        return True

GiscesoaSyncLinia()