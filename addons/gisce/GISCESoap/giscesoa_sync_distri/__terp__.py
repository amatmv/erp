# -*- coding: utf-8 -*-
{
    "name": "Actualització de pòlisses de sistema de gestió extern que genera fitxers CSV per a descarrecs",
    "description": """Afegeix camps de distribuidora al sistema de sincronitzacio""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE_SOA",
    "depends":[
        "giscesoa_sync",
        "giscedata_georef_cne",
        "giscedata_polissa_distri",
        "giscedata_butlletins"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscesoa_sync_workflow_linia.xml",
        "giscesoa_sync_data.xml"
    ],
    "active": False,
    "installable": True
}
