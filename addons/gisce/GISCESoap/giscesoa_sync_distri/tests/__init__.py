# coding=utf-8
from __future__ import unicode_literals
from destral import testing
from destral.transaction import Transaction
from addons import get_module_resource
from destral.patch import PatchNewCursors


class TestLoadSOA(testing.OOTestCase):

    def test_split_without_numer(self):
        """
        :return:
        """
        obj_sync = self.openerp.pool.get("giscesoa.sync.fitxer")
        obj_cups = self.openerp.pool.get("giscedata.cups.ps")
        obj_linia = self.openerp.pool.get("giscesoa.sync.linia")
        obj_soa_config = self.openerp.pool.get("giscesoa.sync.config")

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            with PatchNewCursors():
                id_separar = obj_soa_config.search(
                    cursor, uid,
                    [("name", "=", "separar_carrer")]
                )
                if id_separar:
                    obj_soa_config.write(
                        cursor, uid, id_separar,
                        {
                            "module": "giscesoa_sync",
                            "value": "1"
                        })
                else:
                    obj_soa_config.create(
                        cursor, uid,
                        {
                            "name": "separar_carrer",
                            "module": "giscesoa_sync",
                            "value": "1"
                        })
                id_aclarador = obj_soa_config.search(
                    cursor, uid,
                    [("name", "=", "separador_aclarador")]
                )
                if id_aclarador:
                    obj_soa_config.write(
                        cursor, uid,id_aclarador,
                        {
                            "module": "giscesoa_sync",
                            "value": ","
                        }
                    )
                else:
                    obj_soa_config.create(
                        cursor, uid,
                        {
                            "name": "separador_aclarador",
                            "module": "giscesoa_sync",
                            "value": ","
                        }
                    )

                soa_url = get_module_resource(
                    'giscesoa_sync_distri', 'tests', 'fixtures',
                    'example.soa')

                obj_sync.carregar_fitxers(cursor, uid, [soa_url], 'immediata', True)

                linies_ids = obj_linia.search(cursor, uid, [])
                data_linia = obj_linia.read(cursor, uid, linies_ids, ["state"])

                for linia in data_linia:
                    self.assertNotEqual(linia["state"], "st_error")
                search_params = [("name", "=", "ES0999000000031416WJ")]
                cups_ids = obj_cups.search(cursor, uid, search_params)
                for cups in obj_cups.read(cursor, uid, cups_ids, ["nv"]):
                    self.assertEquals(cups["nv"], "Carrer fals")

    def test_split_with_numer(self):
        """
        :return:
        """
        obj_sync = self.openerp.pool.get("giscesoa.sync.fitxer")
        obj_cups = self.openerp.pool.get("giscedata.cups.ps")
        obj_linia = self.openerp.pool.get("giscesoa.sync.linia")
        obj_soa_config = self.openerp.pool.get("giscesoa.sync.config")

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            with PatchNewCursors():
                id_separar = obj_soa_config.search(
                    cursor, uid,
                    [("name", "=", "separar_carrer")]
                )
                if id_separar:
                    obj_soa_config.write(
                        cursor, uid, id_separar,
                        {
                            "name": "separar_carrer",
                            "value": "1"
                        })
                else:
                    obj_soa_config.create(
                        cursor, uid,
                        {
                            "name": "separar_carrer",
                            "module": "giscesoa_sync",
                            "value": "1"
                        })
                id_aclarador = obj_soa_config.search(
                    cursor, uid,
                    [("name", "=", "separador_aclarador")]
                )
                if id_aclarador:
                    obj_soa_config.write(
                        cursor, uid, id_aclarador,
                        {
                            "name": "separador_aclarador",
                            "value": ","
                        }
                    )
                else:
                    obj_soa_config.create(
                        cursor, uid,
                        {
                            "name": "separador_aclarador",
                            "module": "giscesoa_sync",
                            "value": ","
                        }
                    )


                soa_url = get_module_resource(
                    'giscesoa_sync_distri', 'tests', 'fixtures',
                    'example_number.soa')

                obj_sync.carregar_fitxers(cursor, uid, [soa_url], 'immediata', True)

                linies_ids = obj_linia.search(cursor, uid, [])
                data_linia = obj_linia.read(cursor, uid, linies_ids, ["state"])

                for linia in data_linia:
                    self.assertNotEqual(linia["state"], "st_error")
                search_params = [("name", "=", "ES0999000000031416WJ")]
                cups_ids = obj_cups.search(cursor, uid, search_params)
                for cups in obj_cups.read(cursor, uid, cups_ids, ["nv", "aclarador"]):
                    self.assertEquals(cups["nv"], "Carrer fals")
                    self.assertEquals(cups["aclarador"], "123")

    def load_file(self, cursor, uid, filename):
        """
        Test suport function to load a SOA file
        :return:
        """

        obj_sync = self.openerp.pool.get("giscesoa.sync.fitxer")
        soa_url = get_module_resource(
            'giscesoa_sync_distri', 'tests', 'fixtures', filename)
        obj_sync.carregar_fitxers(cursor, uid, [soa_url], 'immediata', True)

    def test_max_len_carrer(self):
        """
        Tests that if you import data with a carrer longer than 60 chars fails

        :return: None
        """

        obj_linia = self.openerp.pool.get("giscesoa.sync.linia")

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            with PatchNewCursors():
                self.load_file(cursor, uid, 'example_carrer_len.soa')
                linies_ids = obj_linia.search(cursor, uid, [])
                data_linia = obj_linia.read(cursor, uid, linies_ids, ["state", "status_message"])
                self.assertEquals(data_linia[0]["state"], "st_tot_fet")

    def test_max_len_carrer_error(self):
        """
        Tests that if you import data with a carrer longer than 256 chars fails

        :return: None
        """

        obj_linia = self.openerp.pool.get("giscesoa.sync.linia")

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            with PatchNewCursors():
                self.load_file(cursor, uid, 'example_carrer_len_256.soa')
                linies_ids = obj_linia.search(cursor, uid, [])
                data_linia = obj_linia.read(cursor, uid, linies_ids, ["state", "status_message"])
                self.assertEquals(data_linia[0]["state"], "st_error")
                self.assertEquals(data_linia[0]["status_message"], "ERROR:El campo 4 (csv_cups_carrer) tiene 257 caracteres cuando el máximo es 256.")
