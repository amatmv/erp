# coding=utf-8
from __future__ import unicode_literals
from dateutil.relativedelta import relativedelta
from datetime import datetime
import unittest
from destral import testing
from destral.transaction import Transaction
from addons import get_module_resource


class TestLoadSOA(testing.OOTestCase):

    def test_diferent_descarrec_adress(self):
        """

        :return:
        """
        obj_sync = self.openerp.pool.get("giscesoa.sync.fitxer")
        obj_pol = self.openerp.pool.get("giscedata.polissa")
        obj_adress = self.openerp.pool.get("res.partner.address")
        obj_linia = self.openerp.pool.get("giscesoa.sync.linia")
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            soa_url = get_module_resource(
                'giscesoa_sync_descarrec', 'tests', 'fixtures',
                'doble_soa.txt')

            obj_sync.carregar_fitxers(cursor, uid, [soa_url], 'immediata', True)

            linies_ids = obj_linia.search(cursor, uid, [])
            data_linia = obj_linia.read(cursor, uid, linies_ids, ["state"])

            for linia in data_linia:
                self.assertNotEqual(linia["state"], "st_error")

            id_1 = obj_pol.search(cursor, uid,
                [("cups", "=", "ES0999000000003111JK")])
            address_1_id = obj_adress.read(
                cursor, uid,
                id_1, ["descarrec_address"])

            id_2 = obj_pol.search(cursor, uid,
                [("cups", "=", "ES0999000000031416WJ")])
            address_2_id = obj_adress.read(
                cursor, uid,
                id_2, ["descarrec_address"])

            self.assertNotEqual(address_1_id, address_2_id)

            #address_2_data =

