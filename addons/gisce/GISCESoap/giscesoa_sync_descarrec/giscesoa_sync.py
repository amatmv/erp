# -*- coding: utf-8 -*-
from __future__ import absolute_import
from osv import osv


class GiscesoaSyncLinia(osv.osv):
    """
    Class that extends GiscesoaSyncLinia for descarrecs
    """
    _name = 'giscesoa.sync.linia'
    _inherit = 'giscesoa.sync.linia'

    def wh_omplir_camps_polissa(self, cursor, uid, linia):
        """
        Workflow helper to fill the fields of a polisssa

        :param cursor: Database cursor
        :param uid:User id
        :type uid: int
        :param linia: File line to process
        :type linia: list of str
        :return: Data to write into the polissa
        :rtype: dict
        """

        pol_data = super(GiscesoaSyncLinia, self).wh_omplir_camps_polissa(
            cursor, uid, linia
        )

        if linia.csv_abonat_mail:
            pol_data["avis_email"] = True

        search_address = [
            ("name", "=", linia.csv_abonat_nom),
            ("street", "=", linia.csv_cups_carrer),
            ("email", "=", linia.csv_abonat_mail),
            ("type", "=", "contact")
        ]

        c_partner_address = self.pool.get("res.partner.address")
        addresses_ids = c_partner_address.search(
            cursor, uid, search_address)

        if addresses_ids:
            pol_data["descarrec_address"] = addresses_ids[0]
        else:
            data_partner = {
                "name": linia.csv_abonat_nom,
                "street": linia.csv_cups_carrer,
                "type": "contact",
                "phone": linia.csv_abonat_telefon
            }
            if linia.csv_abonat_mail:
                data_partner["email"] = linia.csv_abonat_mail
            addresses_id = c_partner_address.create(
                cursor, uid,
                data_partner
            )
            pol_data["descarrec_address"] = addresses_id

        return pol_data


GiscesoaSyncLinia()
