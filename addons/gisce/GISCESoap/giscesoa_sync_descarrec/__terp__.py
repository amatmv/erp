# -*- coding: utf-8 -*-
{
    "name": "Actualització de pòlisses de sistema de gestió extern que genera fitxers CSV per a descarrecs",
    "description": """Modulo para actualitzacion de datos (SOA) para descargos""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE_SOA",
    "depends":[
        "giscesoa_sync",
        "giscesoa_sync_distri"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
