# -*- coding: utf-8 -*-
{
    "name": "Sync CNAE a través de SOA",
    "description": """Habilitar la sincronització del CNAE a través de SOA""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE_SOA",
    "depends":[
        "base",
        "giscesoa_sync",
        "giscemisc_cnae"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
