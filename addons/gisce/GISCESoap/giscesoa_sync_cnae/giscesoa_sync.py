# -*- coding: utf-8 -*-

from osv import osv


class GiscesoaSyncLinia(osv.osv):
    _name = 'giscesoa.sync.linia'
    _inherit = 'giscesoa.sync.linia'

    def wh_omplir_camps_polissa(self, cr, uid, linia):
        """Actualiza els valors a actualitzar amb el CNAE
        """
        misc_cnae = self.pool.get('giscemisc.cnae')
        cnae = linia.csv_polissa_activitat_cnae
        id_cnae = misc_cnae.search(cr, uid, [('name', '=', cnae)])
        vals = super(GiscesoaSyncLinia, self) \
            .wh_omplir_camps_polissa(cr, uid, linia)
        if id_cnae:
            vals.update({'cnae': id_cnae[0]})
        return vals


GiscesoaSyncLinia()
