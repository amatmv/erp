#!/bin/bash
SOAP_UID=
BBDD=""
FILE="/home/gis/tomcat/webapps/ROOT/soap/index.html"
FILE2="/home/gis/tomcat/webapps/ROOT/soap/soap_`date +%m-%Y`.html"

MODIFICADES=`psql $BBDD -c "SELECT count(id),to_char(write_date, 'MM/YYYY') as mes from giscedata_polissa where to_char(write_date, 'YYYY-MM') = to_char(now(), 'YYYY-MM') and write_date is not null and baixa = False and write_uid = $SOAP_UID and to_char(write_date, 'D') in (2,3,4,5,6) group by mes;" -tA | cut -d '|' -f 1`
if [ -z $MODIFICADES ]; then
	MODIFICADES=0
fi
CREADES=`psql $BBDD -c "select count(id),to_char(create_date, 'MM/YYYY') as mes from giscedata_polissa where to_char(create_date, 'YYYY-MM') = to_char(now(), 'YYYY-MM') and create_date is not null and create_uid = $SOAP_UID group by mes;" -tA | cut -d '|' -f 1`
if [ -z $CREADES ]; then
	CREADES=0
fi
BAIXA=`psql $BBDD -c "select count(id),to_char(write_date, 'MM/YYYY') as mes from giscedata_polissa where to_char(write_date, 'YYYY-MM') = to_char(now(), 'YYYY-MM') and write_date is not null and baixa = True and write_uid = $SOAP_UID group by mes order by mes asc;" -tA | cut -d '|' -f 1`
if [ -z $BAIXA ]; then
	BAIXA=0
fi
MES=`date +%m/%Y`
IMG="http://chart.apis.google.com/chart?cht=bvg&chd=t:$MODIFICADES|$CREADES|$BAIXA&chs=400x100&chl=$MES&chco=4d89f9,c6d9fd,fc0c0c&chbh=50&chdl=Modificades ($MODIFICADES)|Creades ($CREADES)|Baixa ($BAIXA)&chtt=SOAP $BBDD&chxt=x"
echo "<html><head><title>SOAP $BBDD</title><meta http-equiv=\"refresh\" content=\"600\" /></head><body>" > $FILE
echo "<img align=\"center\" src=\"$IMG\" />" >> $FILE

psql $BBDD -c "SELECT 'modificades' as accio,count(id),to_char(write_date, 'DD/MM/YYYY HH24:00') as mes from giscedata_polissa where to_char(write_date, 'YYYY-MM') = to_char(now(), 'YYYY-MM') and write_date is not null and baixa = False and write_uid = $SOAP_UID group by mes union select 'creades' as accio,count(id),to_char(create_date, 'DD/MM/YYYY HH24:00') as mes from giscedata_polissa where to_char(create_date, 'YYYY-MM') = to_char(now(), 'YYYY-MM') and create_date is not null and create_uid = $SOAP_UID group by mes union select 'baixa' as accio,count(id),to_char(write_date, 'DD/MM/YYYY HH24:00') as mes from giscedata_polissa where to_char(write_date, 'YYYY-MM') = to_char(now(), 'YYYY-MM') and write_date is not null and baixa = True and write_uid = $SOAP_UID group by mes order by mes asc;" -H >> $FILE

echo "</body></html>" >> $FILE
cp $FILE $FILE2
