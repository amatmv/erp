#!/usr/bin/python
# -*- coding: utf-8 -*-

import csv
import optparse
import ConfigParser
import xmlrpclib
import sys
import time

parser = optparse.OptionParser(version="[GISCE ERP] Migrations")

parser.add_option("-c", "--config", dest="config", help="specify path to Tiny ERP config file")

group = optparse.OptionGroup(parser, "SERVER Related")
group.add_option("--host", dest="host", help="specify the database host")
group.add_option("--port", dest="port", help="specify the database port")
group.add_option("-u", "--user", dest="user", help="specify the username")
group.add_option("-d", "--database", dest="dbname", help="specify the database name")
group.add_option("-p", "--password", dest="password", help="specify the password")
group.add_option("-f", "--csv", dest="csv_file", help="specify the cvs file")
group.add_option("--clean", dest="clean", help="Clean after import")
group.add_option("--escape", dest="escape", help="Escape first line")
parser.add_option_group(group)

options = optparse.Values()
options.db_name = 'terp' # default value
parser.parse_args(values=options)

if hasattr(options, 'config'):
    configparser = ConfigParser.ConfigParser()
    configparser.read([options.config])
    for name, value in configparser.items('options'):
        if not (hasattr(options, name) and getattr(options, name)):
            if value in ('true', 'True'):
                value = True
            if value in ('false', 'False'):
                value = False
            setattr(options, name, value)

# -----

csv_file = options.csv_file

host = options.host
port = options.port
dbname = options.dbname
user = options.user
pwd = options.password

sock = xmlrpclib.ServerProxy('http://%s:%d/xmlrpc/common' % (host, int(port)))
uid = sock.login(dbname, user, pwd)
sock = xmlrpclib.ServerProxy('http://%s:%d/xmlrpc/object' % (host, int(port)))

f = open(csv_file, 'rb')
reader = csv.reader(f, delimiter=';')

escape = eval(options.escape)
clean = eval(options.clean)

try:
    data_inici = sock.execute(dbname, uid, pwd, 'giscegis.nodes', 'get_time')
    for row in reader:
        if not escape:
            try:
                dia, mes, any = row[19].split('/')
                if int(any) <= int(time.strftime('%y')):
                    any = '20%s' % any
                else:
                    any = '19%s' % any
                data_alta = '%s-%s-%s' % (any, mes, dia)
            except:
                data_alta = False
            vals = {
                'cups_ps': row[0],
                'cups_ps_poblacio': row[1],
                'cups_ps_carrer': row[2],
                'cups_ps_numero': row[3],
                'cups_ps_pis': row[4],
                'cups_ps_porta': row[5],
                'cups_ps_zona': row[6],
                'cups_ps_ordre': row[8],
                'res_partner_name': row[10],
                'res_partner_vat': row[11],
                'res_partner_phone': row[12],
                'polissa_name': row[13],
                'polissa_n_comptador': row[14],
                'polissa_n_comptador_2': row[15],
                'polissa_n_comptador_3': row[16],
                'polissa_n_comptador_reactiva': row[17],
                'polissa_potencia': row[18],
                'polissa_data_alta': data_alta,
                'polissa_tensio': row[20],
                'polissa_icp': row[21],
                'polissa_num_fases': row[22],
                'polissa_tarifa': row[23]
            }
            try:
                sock.execute(dbname, uid, pwd, 'giscesoap.polissa', 'create', vals)
                sys.stdout.write('.')
            except xmlrpclib.Fault:
                sys.stdout.write('@')
                pass
            sys.stdout.flush()
        escape = False

    if clean:
        sys.stdout.write('\n')
        import calendar
        from datetime import datetime

        d = datetime.now()
        data_baixa = '%s-%s-%s' % (d.year, d.month, calendar.monthrange(d.year, d.month)[1])

        # Desactivem els que no s'han modificat amb aquesta actualització
        del_pol_ids = sock.execute(dbname, uid, pwd, 'giscedata.polissa', 'search',
                                   [('write_date', '!=', False), ('write_date', '<', data_inici), ('baixa', '=', 0)])
        if len(del_pol_ids):
            try:
                sys.stdout.write('[BAIXA] No modificades en aquesta actualitzacio: %i\n' % (len(del_pol_ids)))
                sock.execute(dbname, uid, pwd, 'giscedata.polissa', 'write', del_pol_ids,
                             {'active': 0, 'baixa': 1, 'data_baixa': data_baixa})
            except xmlrpclib.Fault:
                sys.stdout.write('@')

        # Desactivem els que només s'han creat abans d'aquesta actualització i no s'havien modificat mai
        del_pol_ids2 = sock.execute(dbname, uid, pwd, 'giscedata.polissa', 'search',
                                    [('write_date', '=', False), ('create_date', '<', data_inici), ('baixa', '=', 0)])
        if len(del_pol_ids2):
            try:
                sys.stdout.write(
                    '[BAIXA] Creades abans d\'aquesta actualizacio i no modificades mai: %i\n' % (len(del_pol_ids2)))
                sock.execute(dbname, uid, pwd, 'giscedata.polissa', 'write', del_pol_ids2,
                             {'active': 0, 'baixa': 1, 'data_baixa': data_baixa})
            except xmlrpclib.Fault:
                sys.stdout.write('@')

        # Polisses que ni s'han creat ni modificat amb aquesta actualització
        del_pol_ids3 = sock.execute(dbname, uid, pwd, 'giscedata.polissa', 'search',
                                    [('write_date', '=', False), ('create_date', '=', False), ('baixa', '=', 0)])
        if len(del_pol_ids3):
            try:
                sys.stdout.write('[BAIXA] Ni creades ni modificades mai: %i\n' % (len(del_pol_ids2)))
                sock.execute(dbname, uid, pwd, 'giscedata.polissa', 'write', del_pol_ids3,
                             {'active': 0, 'baixa': 1, 'data_baixa': data_baixa})
            except xmlrpclib.Fault:
                sys.stdout.write('@')
        sys.stdout.flush()


except KeyboardInterrupt:
    print('\nShutting Down...')
    sys.stdout.flush()
    f.close()
    

    


