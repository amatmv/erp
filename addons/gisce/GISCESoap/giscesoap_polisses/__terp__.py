# -*- coding: utf-8 -*-
{
    "name": "Workflow per actualitzar polisses",
    "description": """Mòdul per configurar i engegar el servidor per atendre les peticions SOAP de Pòlisses""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCESoap",
    "depends":[
        "base",
        "giscedata_polissa",
        "giscedata_cups"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscesoap_polisses_workflow.xml"
    ],
    "active": False,
    "installable": False
}
