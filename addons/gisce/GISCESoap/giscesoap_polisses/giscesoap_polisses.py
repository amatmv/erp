# -*- coding: utf-8 -*-
from osv import osv, fields

import pooler

_giscesoapolisses_states = [
    ('esborrany', 'Esborrany'),
    ('valid', 'Camps Valids'),
    ('createcups', 'Create CUPS'),
    ('updatecups', 'Update CUPS'),
    ('donecups', 'CUPS acabat'),
    ('createpartner', 'Create Partner'),
    ('createpartneraddress', 'Create Partner Address'),
    ('updatepartner', 'Update Partner'),
    ('donepartner', 'Partner acabat'),
    ('createtarifa', 'Create Tarifa'),
    ('donetarifa', 'Tarifa acabat'),
    ('createpolissa', 'Create Polissa'),
    ('updatepolissa', 'Update Polissa'),
    ('donepolissa', 'Polissa acabat'),
    ('ko', 'Error'),
    ('done', 'Finalitzat'),
]


class GiscesoapPolisses(osv.osv):
    _name = 'giscesoap.polissa'
    _modelname = 'giscesoap.polissa'
    _description = 'Interfície SOAP per giscesoap_polisses'
    _debug = None

    _columns = {
        'cups_ps': fields.char('cups_ps', size=30),
        'cups_ps_poblacio': fields.char('cups_ps_poblacio', size=60),
        'cups_ps_carrer': fields.char('cups_ps_carrer', size=60),
        'cups_ps_numero': fields.char('cups_ps_numero', size=10),
        'cups_ps_pis': fields.char('cups_ps_pis', size=10),
        'cups_ps_porta': fields.char('cups_ps_porta', size=10),
        'cups_ps_escala': fields.char('cups_ps_escala', size=10),
        'cups_ps_zona': fields.char('cups_ps_zona', size=10),
        'cups_ps_ordre': fields.char('cups_ps_ordre', size=10),
        'res_partner_name': fields.char('res_partner_name', size=128),
        'res_partner_vat': fields.char('res_partner_vat', size=32),
        'res_partner_phone': fields.char('res_partner_phone', size=64),
        'polissa_name': fields.char('polissa_name', size=60),
        'polissa_n_comptador': fields.char('polissa_n_comptador', size=20),
        'polissa_n_comptador_2': fields.char('polissa_n_comptador_2', size=20),
        'polissa_n_comptador_3': fields.char('polissa_n_comptador_3', size=20),
        'polissa_n_comptador_reactiva': fields.char('polissa_n_comptador_reactiva', size=20),
        'polissa_potencia': fields.float('polissa_potencia'),
        'polissa_data_alta': fields.date('polissa_data_alta'),
        'polissa_tensio': fields.char('polissa_tensio', size=10),
        'polissa_icp': fields.char('polissa_icp', size=60),
        'polissa_num_fases': fields.integer('polissa_num_fases'),
        'polissa_tarifa': fields.char('polissa_tarifa', size=10),
        'state': fields.selection(_giscesoapolisses_states, 'Estat', readonly=True),
        'status_message': fields.text('Message Status'),
    }


    def doDone(self, cr, uid, ids):
        self.write(cr, uid, ids, {'state': 'done'})
        return True


    def doKO(self, cr, uid, ids):
        self.write(cr, uid, ids, {'state': 'ko'})
        return True

    def isValid(self, cr, uid, ids):
        # Primer comprovem que cups_ps, res_partner_name, polissa_name
        for obj in self.browse(cr, uid, ids):
            if (obj.cups_ps and obj.res_partner_name and obj.polissa_name):
                return True
            else:
                return False

    def existeixCUPS(self, cr, uid, ids):
        pool = pooler.get_pool(cr.dbname)
        cups_obj = pool.get('giscedata.cups.ps')
        for obj in self.browse(cr, uid, ids):
            cups_ids = cups_obj.search(cr, uid, [('name', '=', obj.cups_ps)], context={'active_test': False})
            if len(cups_ids):
                return True
            else:
                return False

        def updateCUPS(self, cr, uid, ids):

            cups_obj = pooler.get_pool(cr.dbname).get('giscedata.cups.ps')

        for obj in self.browse(cr, uid, ids):
            cups_ids = cups_obj.search(cr, uid, [('name', '=', obj.cups_ps)], context={'active_test': False})
            vals = {
                'name': obj.cups_ps,
                'poblacio': obj.cups_ps_poblacio,
                'carrer': obj.cups_ps_carrer,
                'numero': obj.cups_ps_numero,
                'pis': obj.cups_ps_pis,
                'porta': obj.cups_ps_porta,
                'zona': obj.cups_ps_zona,
                'ordre': obj.cups_ps_ordre,
                'active': 1,
            }
            cups_obj.write(cr, uid, cups_ids, vals)
        return True

    def createCUPS(self, cr, uid, ids):
        cups_obj = pooler.get_pool(cr.dbname).get('giscedata.cups.ps')
        for obj in self.browse(cr, uid, ids):
            vals = {
                'name': obj.cups_ps,
                'poblacio': obj.cups_ps_poblacio,
                'carrer': obj.cups_ps_carrer,
                'numero': obj.cups_ps_numero,
                'pis': obj.cups_ps_pis,
                'porta': obj.cups_ps_porta,
                'zona': obj.cups_ps_zona,
                'ordre': obj.cups_ps_ordre
            }
            cups_obj.create(cr, uid, vals)
        return True

    def existeixPartner(self, cr, uid, ids):
        for obj in self.browse(cr, uid, ids):
            pool = pooler.get_pool(cr.dbname)
            partner_ids = pool.get('res.partner').search(cr, uid, [('name', '=', obj.res_partner_name),
                                                                   ('vat', '=', obj.res_partner_vat)],
                                                         context={'active_test': True})
            if len(partner_ids):
                return True
            else:
                return False

    def createPartner(self, cr, uid, ids):
        partner_obj = pooler.get_pool(cr.dbname).get('res.partner')
        for obj in self.browse(cr, uid, ids):
            vals = {
                'name': obj.res_partner_name,
                'vat': obj.res_partner_vat,
            }
            partner_obj.create(cr, uid, vals)
        return True

    def existeixTarifa(self, cr, uid, ids):
        pool = pooler.get_pool(cr.dbname)
        for obj in self.browse(cr, uid, ids):
            tarifa_ids = pool.get('giscedata.polissa.tarifa').search(cr, uid, [('name', '=', obj.polissa_tarifa)])
            if len(tarifa_ids):
                return True
            else:
                return False

    def createTarifa(self, cr, uid, ids):
        tarifa_obj = pooler.get_pool(cr.dbname).get('giscedata.polissa.tarifa')
        for obj in self.browse(cr, uid, ids):
            vals = {
                'name': obj.polissa_tarifa
            }
            tarifa_obj.create(cr, uid, vals)

        return True


    def existeixPolissa(self, cr, uid, ids):
        for obj in self.browse(cr, uid, ids):
            pool = pooler.get_pool(cr.dbname)
            polissa_ids = pool.get('giscedata.polissa').search(cr, uid, [('name', '=', obj.polissa_name)],
                                                               context={'active_test': False})
            if len(polissa_ids):
                return True
            else:
                return False

    def createPolissa(self, cr, uid, ids):
        partner_obj = pooler.get_pool(cr.dbname).get('res.partner')
        cups_obj = pooler.get_pool(cr.dbname).get('giscedata.cups.ps')
        polissa_obj = pooler.get_pool(cr.dbname).get('giscedata.polissa')
        tarifa_obj = pooler.get_pool(cr.dbname).get('giscedata.polissa.tarifa')
        for obj in self.browse(cr, uid, ids):
            cups_ids = cups_obj.search(cr, uid, [('name', '=', obj.cups_ps)])
            partner_ids = partner_obj.search(cr, uid,
                                             [('name', '=', obj.res_partner_name), ('vat', '=', obj.res_partner_vat)])
            tarifa_ids = tarifa_obj.search(cr, uid, [('name', '=', obj.polissa_tarifa)])
            vals = {
                'cups': cups_ids[0],
                'partner_id': partner_ids[0],
                'tarifa': tarifa_ids[0],
                'name': obj.polissa_name,
                'n_comptador': obj.polissa_n_comptador,
                'n_comptador_2': obj.polissa_n_comptador_2,
                'n_comptador_3': obj.polissa_n_comptador_3,
                'n_comptador_reactiva': obj.polissa_n_comptador_reactiva,
                'potencia': obj.polissa_potencia,
                'data_alta': obj.polissa_data_alta,
                'tensio': obj.polissa_tensio,
                'icp': obj.polissa_icp,
                'num_fases': obj.polissa_num_fases,
            }
            polissa_obj.create(cr, uid, vals)
        return True

    def updatePolissa(self, cr, uid, ids):
        # TODO si el camp data_baixa o baixa ve amb True
        # S'haurà de donar de baixa la pòlissa
        cups_obj = pooler.get_pool(cr.dbname).get('giscedata.cups.ps')
        polissa_obj = pooler.get_pool(cr.dbname).get('giscedata.polissa')
        partner_obj = pooler.get_pool(cr.dbname).get('res.partner')
        tarifa_obj = pooler.get_pool(cr.dbname).get('giscedata.polissa.tarifa')
        for obj in self.browse(cr, uid, ids):
            cups_ids = cups_obj.search(cr, uid, [('name', '=', obj.cups_ps)])
            polissa_ids = polissa_obj.search(cr, uid, [('name', '=', obj.polissa_name)], context={'active_test': False})
            tarifa_ids = tarifa_obj.search(cr, uid, [('name', '=', obj.polissa_tarifa)])
            vals = {
                'cups': cups_ids[0],
                'tarifa': tarifa_ids[0],
                'name': obj.polissa_name,
                'n_comptador': obj.polissa_n_comptador,
                'n_comptador_2': obj.polissa_n_comptador_2,
                'n_comptador_3': obj.polissa_n_comptador_3,
                'n_comptador_reactiva': obj.polissa_n_comptador_reactiva,
                'potencia': obj.polissa_potencia,
                'data_alta': obj.polissa_data_alta,
                'tensio': obj.polissa_tensio,
                'icp': obj.polissa_icp,
                'num_fases': obj.polissa_num_fases,
                'data_baixa': False,
                'baixa': 0,
                'active': 1,
            }
            # Actualitzem la polissa
            polissa_obj.write(cr, uid, polissa_ids, vals)

            # Actualitzem el partner associat a aquesta polissa
            for polissa in polissa_obj.browse(cr, uid, polissa_ids):
                polissa.partner_id.write(cr, uid, [polissa.partner_id.id],
                                         {'name': obj.res_partner_name, 'vat': obj.res_partner_vat, 'active': 1})

        return True


GiscesoapPolisses()
