import base64
from destral import testing
from destral.transaction import Transaction
from addons import get_module_resource


class TestWizardImport(testing.OOTestCase):
    """
    Tests for the Wizard of import Obres data
    """

    require_demo_data = True

    def test_import_capcalera(self):
        """
        Tests that the voltatge is properly calculated

        :return: None
        """
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            wiz_obj = self.openerp.pool.get('wizard.giscedata.obres.import')
            obres_obj = self.openerp.pool.get('giscedata.obres.obra')

            url_obres_data = get_module_resource(
                'giscedata_obres', 'tests', 'fixtures',
                'test_capcalera.txt')

            with open(url_obres_data, "r") as f_obres:
                data_obres = f_obres.read()

            write_data = {"file_cap": base64.b64encode(data_obres)}
            wiz_id = wiz_obj.create(cursor, uid, {})
            wizard = wiz_obj.browse(cursor, uid, wiz_id)
            wizard.write(write_data)
            wizard.importar_data()

            # Check wizard message
            expected_import = {
                "created": 3,
                "modified": 3
            }
            self.assertEquals(wizard.result, """S'ha carregat el fitxer correctament
Obres creades:{created}
Obres modificades:{modified}""".format(**expected_import))

            search_params = [("name", "=", "8000247")]
            fields_to_read = [
                "name", "data", "descripcio", "data_tancament",
                "data_finaliztacio", "estat", "referencia_interna", "tipus"
            ]
            id_obra = obres_obj.search(cursor, uid, search_params)
            obra_data = obres_obj.read(cursor, uid, id_obra[0], fields_to_read)

            obra = obra_data

            # Check created data
            self.assertEquals(obra["name"], "8000247")
            self.assertEquals(obra["data"], "2018-04-24")
            self.assertEquals(
                obra["descripcio"],
                "REPARACIO PLATS 40 KV E.R. QUEROL"
            )
            self.assertFalse(obra["data_tancament"])
            self.assertEquals(obra["data_finaliztacio"], "2018-05-10")
            self.assertEquals(obra["estat"], "EC")
            self.assertFalse(obra["referencia_interna"])
            self.assertEquals(obra["tipus"], "311")

            search_params = [("name", "=", "8000252")]
            id_obra = obres_obj.search(cursor, uid, search_params)
            obra = obres_obj.read(cursor, uid, id_obra[0], fields_to_read)

            # Check modified data
            self.assertEquals(obra["name"], "8000252")
            self.assertEquals(obra["data"], "2018-04-25")
            self.assertEquals(
                obra["descripcio"],
                "COL-LOCAR PIVOTS MOBILS E.T. VESPRADA"
            )
            self.assertFalse(obra["data_tancament"])
            self.assertEquals(obra["data_finaliztacio"], False)
            self.assertEquals(obra["estat"], "EC")
            self.assertFalse(obra["referencia_interna"])
            self.assertEquals(obra["tipus"], "311")