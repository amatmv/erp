# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* giscedata_obres
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.14\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2018-11-02 14:09\n"
"PO-Revision-Date: 2018-11-02 14:09\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: giscedata_obres
#: constraint:ir.model:0
msgid "The Object name must start with x_ and not contain any special character !"
msgstr ""

#. module: giscedata_obres
#: field:giscedata.obres.obra,referencia_interna:0
msgid "Referencia interna"
msgstr ""

#. module: giscedata_obres
#: field:giscedata.obres.activitat,active:0
#: field:giscedata.obres.obra,active:0
msgid "Actiu"
msgstr ""

#. module: giscedata_obres
#: view:giscedata.obres.activitat:0
#: field:giscedata.obres.activitat,gfa:0
#: view:giscedata.obres.obra:0
msgid "GFA (€)"
msgstr ""

#. module: giscedata_obres
#: model:ir.ui.menu,name:giscedata_obres.menu_obres_manteniment
msgid "Manteniment"
msgstr ""

#. module: giscedata_obres
#: field:giscedata.obres.obra,estat:0
msgid "Estat"
msgstr ""

#. module: giscedata_obres
#: view:giscedata.obres.activitat:0
#: view:giscedata.obres.obra:0
msgid "Cost directe(€)"
msgstr ""

#. module: giscedata_obres
#: constraint:ir.actions.act_window:0
msgid "Invalid model name in the action definition."
msgstr ""

#. module: giscedata_obres
#: field:giscedata.obres.activitat,subactivitat:0
msgid "Subactivitat"
msgstr ""

#. module: giscedata_obres
#: view:giscedata.obres.obra:0
msgid "Import pagat per tercers (€)"
msgstr ""

#. module: giscedata_obres
#: field:giscedata.obres.obra,total_gfa:0
msgid "Total GFA"
msgstr ""

#. module: giscedata_obres
#: field:giscedata.obres.obra,total_import_cedit:0
msgid "Total Import Cedit"
msgstr ""

#. module: giscedata_obres
#: field:giscedata.obres.activitat,import_pagat_tercers:0
msgid "Import pagat tercers (€)"
msgstr ""

#. module: giscedata_obres
#: view:giscedata.obres.activitat:0
#: view:giscedata.obres.obra:0
msgid "Detall Econòmic"
msgstr ""

#. module: giscedata_obres
#: view:giscedata.obres.activitat:0
#: field:giscedata.obres.activitat,trei:0
#: view:giscedata.obres.obra:0
msgid "TREI (€)"
msgstr ""

#. module: giscedata_obres
#: field:giscedata.obres.activitat,obra_id:0
#: view:giscedata.obres.obra:0
#: model:ir.model,name:giscedata_obres.model_giscedata_obres_obra
msgid "Obra"
msgstr ""

#. module: giscedata_obres
#: field:giscedata.obres.obra,tipus:0
msgid "Tipus"
msgstr ""

#. module: giscedata_obres
#: constraint:ir.ui.view:0
msgid "Invalid XML for View Architecture!"
msgstr ""

#. module: giscedata_obres
#: field:giscedata.obres.obra,total_import_pagat_tercers:0
msgid "Total Import Pagat Tercers"
msgstr ""

#. module: giscedata_obres
#: model:ir.module.module,description:giscedata_obres.module_meta_information
msgid "Aquest mòdul afegeix les següents funcionalitats:\n"
"  * Llistat d'obres genèric\n"
""
msgstr ""

#. module: giscedata_obres
#: field:giscedata.obres.obra,data:0
msgid "Data creació"
msgstr ""

#. module: giscedata_obres
#: field:giscedata.obres.obra,data_tancament:0
msgid "Data tancament"
msgstr ""

#. module: giscedata_obres
#: field:giscedata.obres.activitat,id:0
msgid "ID"
msgstr ""

#. module: giscedata_obres
#: view:giscedata.obres.obra:0
#: model:ir.actions.act_window,name:giscedata_obres.action_giscedata_obres_obra_tree
#: model:ir.ui.menu,name:giscedata_obres.menu_giscedata_obres_obra_tree
#: model:ir.ui.menu,name:giscedata_obres.menu_obres
msgid "Obres"
msgstr ""

#. module: giscedata_obres
#: field:giscedata.obres.obra,descripcio:0
msgid "Descripció"
msgstr ""

#. module: giscedata_obres
#: field:giscedata.obres.activitat,cost_directe:0
msgid "Cost directe (€)"
msgstr ""

#. module: giscedata_obres
#: model:ir.module.module,shortdesc:giscedata_obres.module_meta_information
msgid "Gestió d'obres"
msgstr ""

#. module: giscedata_obres
#: field:giscedata.obres.activitat,activitat:0
#: field:giscedata.obres.obra,activitat_ids:0
#: model:ir.model,name:giscedata_obres.model_giscedata_obres_activitat
msgid "Activitat"
msgstr ""

#. module: giscedata_obres
#: view:giscedata.obres.activitat:0
#: field:giscedata.obres.activitat,import_cedit:0
#: view:giscedata.obres.obra:0
msgid "Import cedit (€)"
msgstr ""

#. module: giscedata_obres
#: field:giscedata.obres.obra,name:0
msgid "NºObra"
msgstr ""

#. module: giscedata_obres
#: view:giscedata.obres.obra:0
msgid "General"
msgstr ""

#. module: giscedata_obres
#: field:giscedata.obres.obra,total_trei:0
msgid "Total TREI"
msgstr ""

#. module: giscedata_obres
#: field:giscedata.obres.obra,total_cost_directe:0
msgid "Total Cost directe"
msgstr ""

#. module: giscedata_obres
#: field:giscedata.obres.obra,data_finaliztacio:0
msgid "Data real finalizacio"
msgstr ""

#. module: giscedata_obres
#: view:giscedata.obres.activitat:0
msgid "Import pagat percers (€)"
msgstr ""

