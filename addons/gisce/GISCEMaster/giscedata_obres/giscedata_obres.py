# -*- coding: utf-8 -*-
import csv
from datetime import datetime
from StringIO import StringIO
from osv import osv, fields


class GiscedataObresObra(osv.osv):
    """
    Obres Model
    """
    _name = 'giscedata.obres.obra'
    _description = 'Obra'

    def _get_obra_from_activitat(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        model_activitat = self.pool.get('giscedata.obres.activitat')
        return [x['obra_id'][0] for x in model_activitat.read(cursor, uid, ids, ['obra_id'],)]

    def _compute_sumatories(self, cursor, uid, ids, field_name, arg,
                            context=None):
        if context is None:
            context = {}
        res = {}
        computable_fields = ['gfa', 'trei', 'cost_directe',
                             'import_pagat_tercers', 'import_cedit', 'obra_id']
        model_activitat = self.pool.get('giscedata.obres.activitat')
        model_obres = self.pool.get('giscedata.obres.obra')

        for obra in model_obres.read(cursor, uid, ids, ['activitat_ids'], context=context):
            activitat_ids = obra['activitat_ids']
            total_gfa = 0.0
            total_trei = 0.0
            total_cost_directe = 0.0
            total_import_pagat_tercers = 0.0
            total_import_cedit = 0.0
            activitats = model_activitat.read(
                cursor, uid, activitat_ids, computable_fields, context=context)
            for activitat in activitats:
                total_gfa += activitat['gfa']
                total_trei += activitat['trei']
                total_cost_directe += activitat['cost_directe']
                total_import_pagat_tercers += activitat['import_pagat_tercers']
                total_import_cedit += activitat['import_cedit']
            if activitats:
                res[activitat['obra_id'][0]] = {
                    'total_gfa': total_gfa,
                    'total_trei': total_trei,
                    'total_cost_directe': total_cost_directe,
                    'total_import_pagat_tercers': total_import_pagat_tercers,
                    'total_import_cedit': total_import_cedit
                }
        return res

    def _fix_date(self, date_str):
        """
        Fixes the format of the date_str

        :param date_str: Date in format dd/mm/yyyy
        :type date_str: strs
        :return:Date in format yyyy-mm-dd
        :rtype: str
        """
        if date_str:
            return datetime.strptime(date_str, "%d/%m/%Y").strftime('%Y-%m-%d')
        else:
            return False

    def importar_data(self, cursor, uid, ids, data, context=None):
        """
        Imports the data of a csv str to the obres

        :param cursor: Database cursor
        :param uid: User id
        :param ids: Unused
        :param data: Data to import in csv format
        :type data: str
        :param context: Open ERP context
        :type context: dict(str,object)
        :return: (type of operation, number )
        :rtype: dict(str,int)
        """
        obres_obj = self.pool.get("giscedata.obres.obra")

        str_f = StringIO(data)
        obres_reader = csv.reader(str_f, delimiter=';', quotechar='"')
        obres_reader.next()
        stats = dict.fromkeys(["modified", "created"], 0)

        for row in obres_reader:
            id_obra = obres_obj.search(
                cursor, uid, [("name", "=", row[0])]
            )
            data = {
                "name": row[0],
                "data": self._fix_date(row[1]),
                "descripcio": row[2],
                "data_tancament": self._fix_date(row[3]),
                "data_finaliztacio": self._fix_date(row[4]),
                "estat": row[5],
                "referencia_interna": row[6],
                "tipus": row[7]
            }
            if id_obra:
                obres_obj.write(cursor, uid, id_obra, data, context=context)
                stats["modified"] += 1
            else:
                obres_obj.create(cursor, uid, data, context=context)
                stats["created"] += 1

        return stats

    _columns = {
        'name': fields.char('NºObra', size=64, required=True),
        'descripcio': fields.text('Descripció'),
        'data': fields.date('Data creació'),
        'data_finaliztacio': fields.date(
            'Data real finalizacio'),
        'data_tancament': fields.date('Data tancament'),
        'tipus': fields.char('Tipus', size=64,),
        'estat': fields.char('Estat', size=64),
        'referencia_interna': fields.char(
            'Referencia interna', size=64),
        'activitat_ids': fields.one2many(
            'giscedata.obres.activitat', 'obra_id', 'Activitat'),
        'active': fields.boolean('Actiu'),
        'total_gfa': fields.function(
            _compute_sumatories, method=True,
            store={
                'giscedata.obres.activitat':
                    (_get_obra_from_activitat, ['gfa'], 10)},
            string='Total GFA',
            type='float',
            multi='sum_obres'),
        'total_trei': fields.function(
            _compute_sumatories,
            store={
                'giscedata.obres.activitat':
                    (_get_obra_from_activitat, ['trei'], 10)},
            method=True,
            string='Total TREI',
            type='float',
            multi='sum_obres'),
        'total_cost_directe': fields.function(
            _compute_sumatories,
            store={
                'giscedata.obres.activitat':
                    (_get_obra_from_activitat, ['cost_directe'], 10)},
            method=True,
            string='Total Cost directe',
            type='float',
            multi='sum_obres'),
        'total_import_pagat_tercers': fields.function(
            _compute_sumatories,
            store={
                'giscedata.obres.activitat':
                    (_get_obra_from_activitat, ['import_pagat_tercers'], 10)},
            method=True,
            string='Total Import Pagat Tercers',
            type='float',
            multi='sum_obres'),
        'total_import_cedit': fields.function(
            _compute_sumatories,
            store={
                'giscedata.obres.activitat':
                    (_get_obra_from_activitat, ['import_cedit'], 10)},
            method=True,
            string='Total Import Cedit',
            type='float',
            multi='sum_obres')
    }

    _defaults = {
        'active': lambda *a: 1
    }


GiscedataObresObra()


class GiscedataObresActivitat(osv.osv):
    _name = 'giscedata.obres.activitat'
    _description = 'Activitat'
    _rec_name = 'activitat'

    def name_get(self, cr, user, ids, context=None):
        if context is None:
            context = {}
        if not ids:
            return []
        res = []
        data_act = self.read(cr, user, ids, ['activitat', 'subactivitat'])
        for activitat in data_act:
            text_activitat = activitat['activitat'] or ''
            subactivitat = activitat['subactivitat'] or ''
            if text_activitat:
                format_data = (text_activitat.strip(), subactivitat.strip())
                name = '{0} {1}'.format(*format_data)
                res.append((activitat['id'], name))
            else:
                res.append((activitat['id'], ''))
        return res

    _columns = {
        'id': fields.integer('ID'),
        'activitat': fields.char('Activitat', size=256),
        'subactivitat': fields.char('Subactivitat', size=256),
        'gfa': fields.float('GFA (€)'),
        'trei': fields.float('TREI (€)'),
        'cost_directe': fields.float('Cost directe (€)'),
        'import_pagat_tercers': fields.float(
            'Import pagat tercers (€)'),
        'import_cedit': fields.float(
            'Import cedit (€)'),
        'obra_id': fields.many2one(
            'giscedata.obres.obra', 'Obra', ondelete='cascade'),
        'active': fields.boolean('Actiu')
    }


GiscedataObresActivitat()
