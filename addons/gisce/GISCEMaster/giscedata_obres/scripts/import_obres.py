# -*- encoding: utf-8 -*-
from datetime import datetime
import os


import click
from erppeek import Client


def get_res_config_value(connection, name):
    """
    Returns the value of the res_config

    :param connection: ERP peek connection
    :return: Value of
    :rtype: str
    """

    res_config_obj = connection.model("res.config")
    search_params = [
        ("name", "=", name)
    ]
    ids = res_config_obj.search(search_params)
    val = res_config_obj.read(ids, ["value"])
    return val[0]["value"]


@click.command()
@click.option(
    "-s", "--server",
    help="URL del servidor a actualizar",
    type=str, default="http://localhost"
)
@click.option(
    "-d", "--database",
    help="Nombre de la base de datos",
    type=str, default="test"
)
@click.option(
    "-u", "--user",
    help="Usuario para la conexion",
    type=str, default="admin"
)
@click.option(
    "-w", "--password",
    help="Contraseña para la conexion",
    type=str, default="admin"
)
def import_obres(server, database, user, password):
    """
    Imports the obres files

    :param server: ERP host
    :type server: str
    :param database: ERP database
    :type database: str
    :param user: ERP user
    :type user: str
    :param password:ERP user password
    :type password: str

    :return: None
    """

    c = Client(server, database, user, password)
    obj_obres = c.model("giscedata.obres.obra")

    base_input_directory = get_res_config_value(c, "obres_input_directory")
    output_directory = get_res_config_value(c, "obres_output_directory")

    fecha = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

    cap_input_directory = os.path.join(base_input_directory, "capcalera")

    in_files = os.listdir(cap_input_directory)
    for filename in in_files:
        in_url = os.path.join(cap_input_directory, filename)
        out_url = os.path.join(output_directory, filename + fecha)

        with open(in_url, "r") as f_in:
            data = f_in.read()
        result = obj_obres.importar_data([], data)
        print(result)

        os.rename(in_url, out_url)


if __name__ == '__main__':
    import_obres()
