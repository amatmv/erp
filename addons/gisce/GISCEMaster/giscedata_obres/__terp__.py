# -*- coding: utf-8 -*-
{
    "name": "Gestió d'obres",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Llistat d'obres genèric
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base"
    ],
    "init_xml": [],
    "demo_xml": [
        "giscedata_obres_data_demo.xml"
    ],
    "update_xml":[
        "security/giscedata_obres_security.xml",
        "security/ir.model.access.csv",
        "giscedata_obres_view.xml",
        "wizard/giscedata_obres_import.xml"
    ],
    "active": False,
    "installable": True
}
