# -*- coding: utf-8 -*-
from osv import osv, fields
import base64


class WizardGiscedataObresImport(osv.osv_memory):
    """
    Wizard to import obres
    """

    _name = 'wizard.giscedata.obres.import'

    def importar_data(self, cursor, uid, ids, context=None):
        """
        Loads the data of the wizard

        :param cursor: Database cursor
        :param uid: User id
        :param ids: Id of the wizard
        :param context: OpenERP context
        :type context: dict(str,object)
        :return: None
        :rtype: None
        """
        if context is None:
            context = {}

        obres_obj = self.pool.get("giscedata.obres.obra")

        data_obres = self.read(
            cursor, uid, ids, ["file_cap"], context=context
        )
        if data_obres[0]["file_cap"]:
            plain = base64.b64decode(data_obres[0]["file_cap"])
            msg_template = """S'ha carregat el fitxer correctament
Obres creades:{created}
Obres modificades:{modified}"""
            stats = obres_obj.importar_data(
                cursor, uid, [], plain, context=context
            )
            self.write(cursor, uid, ids, {
                "result": msg_template.format(**stats)})
        else:
            result_data = {"result": "No s'ha trobat el fitxer de capçaleres"}
            self.write(cursor, uid, ids, result_data)

    _columns = {
        'file_cap': fields.binary('Fitxer de capçaleres'),
        'state': fields.char('State', size=16),
        'result': fields.text('Resultat'),
        'shp': fields.boolean('Generar SHPs', help=u"Generar fitxers SHP")
    }


WizardGiscedataObresImport()
