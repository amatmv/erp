# -*- coding: utf-8 -*-
{
    "name": "Facturacio bono social (comer)",
    "description": """Mòdul per a la gestió dels impagaments de contractes 
    susceptibles al Bo Social""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "giscedata_facturacio_impagat",
        "giscedata_facturacio_comer"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_facturacio_comer_bono_social_data.xml",
    ],
    "active": False,
    "installable": True
}