# -*- encoding: utf-8 -*-

from osv import osv
from giscedata_correos_base_flux.account_invoice import MAPPING_NOTI_STATES


_MAPPING_NOTI_STATES = {
    'entregado': {
        'carta_1_pendent_pending_state': 'avis_tall_pending_state',
        'carta_1_pending_state': 'avis_tall_pending_state',
        'carta_2_pendent_pending_state': 'avis_tall_pending_state',
        'carta_2_pending_state': 'avis_tall_pending_state',
        'pendent_carta_avis_tall_pending_state': 'carta_avis_tall_pending_state',
        'carta_avis_tall_pending_state': False

    },
    'enviat': {
        'carta_1_pendent_pending_state': 'carta_1_pending_state',
        'carta_1_pending_state': False,
        'carta_2_pendent_pending_state': 'carta_2_pending_state',
        'carta_2_pending_state': False,
        'pendent_carta_avis_tall_pending_state': 'carta_avis_tall_pending_state',
        'carta_avis_tall_pending_state': False,
    },
    'perdida': {
        'carta_1_pendent_pending_state': False,
        'carta_1_pending_state': 'carta_1_pendent_pending_state',
        'carta_2_pendent_pending_state': False,
        'carta_2_pending_state': 'carta_2_pendent_pending_state',
        'pendent_carta_avis_tall_pending_state': False,
        'carta_avis_tall_pending_state': 'pendent_carta_avis_tall_pending_state',
    },
    'no_entrega': {
        'carta_1_pendent_pending_state': 'carta_2_pendent_pending_state',
        'carta_1_pending_state': 'carta_2_pendent_pending_state',
        'carta_2_pendent_pending_state': 'avis_tall_pending_state',
        'carta_2_pending_state': 'avis_tall_pending_state',
        'pendent_carta_avis_tall_pending_state': 'carta_avis_tall_pending_state',
        'carta_avis_tall_pending_state': False,
    }
}


def main():
    for key in MAPPING_NOTI_STATES.keys():
        MAPPING_NOTI_STATES[key].update(_MAPPING_NOTI_STATES[key])


class AccountInvoicePendingState(osv.osv):

    _name = 'account.invoice.pending.state'
    _inherit = 'account.invoice.pending.state'

    main()


AccountInvoicePendingState()
