# -*- coding: utf-8 -*-
from __future__ import absolute_import
from osv import osv


class GiscedataPolissa(osv.osv):
    _name = "giscedata.polissa"
    _inherit = 'giscedata.polissa'

    def start_signature_atr(self, cursor, uid, polissa_id, cas_id, addr_id, context=None):
        if not context:
            context = {}
        atr_obj = self.pool.get('giscedata.switching')
        if cas_id:
            cas_browse = atr_obj.browse(cursor, uid, cas_id, context=context)
            pas_browse = atr_obj.get_pas(cursor, uid, cas_browse, context=context)
            pas_browse.write({
                'enviament_pendent': context.get("enviament_pendent", False)
            })
            cas_browse.write({'state': context.get('state', 'pending')})
            return cas_id
        else:
            return False

    def process_attachment_callback(self, cursor, uid, cas_id, doc_filename,
                                    signature_id, signature_doc_id, model, context=None):
        if not context:
            context = {}
        process_data = context.get('process_data', False)
        if process_data:
            method = process_data.get('callback_method', False)
            if method == '':
                pass
            else:
                pro_obj = self.pool.get('giscedata.signatura.process')
                pro_obj.default_attach_document(
                    cursor, uid, cas_id, signature_id,
                    signature_doc_id, doc_filename, model, context=context
                )


GiscedataPolissa()
