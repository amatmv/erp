# -*- coding: utf-8 -*-
{
    "name": "Signatura digital processos ATR",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_switching",
        "giscedata_signatura_documents",
    ],
    "init_xml": [],
    "demo_xml":[
    ],
    "update_xml":[
        "giscedata_switching_data.xml"
    ],
    "active": False,
    "installable": True
}
