# -*- coding: utf-8 -*-
from __future__ import absolute_import
from osv import osv


class GiscedataSwitching(osv.osv):
    _name = "giscedata.switching"
    _inherit = 'giscedata.switching'

    def enviar_atr(self, cursor, uid, cas_id, context=None):
        if not context:
            context = {}
        atr_obj = self.pool.get('giscedata.switching')
        cas_browse = atr_obj.browse(cursor, uid, cas_id, context=context)
        pas_browse = self.get_pas(cursor, uid, [cas_id])
        pas_browse.write(
            {'enviament_pendent': context.get("enviament_pendent", True)})
        cas_browse.write({'state': context.get('state', 'open')})
        return True

    def process_signature_callback(self, cursor, uid, cas_id, context=None):
        if not context:
            context = {}
        process_data = context.get('process_data', False)
        if process_data:
            method = process_data.get('callback_method', False)
            if method == 'enviar_atr':
                return self.enviar_atr(cursor, uid, cas_id, context=context)

    def process_attachment_callback(self, cursor, uid, cas_id, doc_filename,
                                    signature_id, signature_doc_id, model, context=None):
        if not context:
            context = {}
        process_data = context.get('process_data', False)
        if process_data:
            method = process_data.get('callback_method', False)
            if method == '':
                pass
            else:
                pro_obj = self.pool.get('giscedata.signatura.process')
                pro_obj.default_attach_document(
                    cursor, uid, cas_id, signature_id,
                    signature_doc_id, doc_filename, model, context=context
                )


GiscedataSwitching()
