# -*- coding: utf-8 -*-
{
    "name": "Perfilació de lectures en 3 periodes",
    "description": """
Perfila les tarifes 3.0 i 3.1 en 3 periodes i no en 6
""",
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_perfils_2012"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_perfils_3per_2012_data.xml"
    ],
    "active": False,
    "installable": True
}
