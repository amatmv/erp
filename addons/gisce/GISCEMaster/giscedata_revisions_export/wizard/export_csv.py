# -*- coding: utf-8 -*-
from datetime import datetime

from dateutil.relativedelta import relativedelta

import wizard
import pooler
import base64
import csv
import StringIO


def _init(self, cr, uid, data, context={}):
    return {}


_csv_revisions_form = """<?xml version="1.0"?>
<form string="Fitxer Revisions CSV">
  <field name="name" colspan="4" width="500"/>
  <field name="file" colspan="4"/>
</form>
"""

_csv_revisions_fields = {
  'name': {'string': 'Nom', 'type': 'char', 'size': 60},
  'file': {'string': 'Fitxer', 'type': 'binary'},
}

# temporal - variable q determina les revisions de quins trienis
trienis = [6, 7, 8]


def _csv_revisions(self, cr, uid, data, context={}):
    pool = pooler.get_pool(cr.dbname)
    revisio_ct_obj = pool.get('giscedata.revisions.ct.revisio')
    revisio_lat_obj = pool.get('giscedata.revisions.at.revisio')

    lines = []

    revisio_ct_ids = revisio_ct_obj.search(
        cr, uid,
        [('trimestre.trieni', 'in', trienis),
         ('name.propietari', '=', True)]
    )

    # Per a cada revisió de CTS
    for revisio in revisio_ct_obj.browse(cr, uid, revisio_ct_ids, context):
        line = []
        #Afegim un C pq és revisio de CTS
        line.append('C')
        #Revisio ID
        line.append(revisio.id)
        #Trimestre
        line.append(revisio.trimestre.name)
        #Data
        line.append(revisio.data)
        #CT
        line.append(revisio.name.name)
        #Descripció
        line.append(revisio.name.descripcio)
        #Tipus d'instal·lació
        line.append(revisio.name.id_installacio.name)
        #Resultat (on fem els càlculs)
        if revisio.state == 'tancada' and not revisio.certificable:
            line.append('ACTA')
        elif revisio.state == 'tancada' and revisio.certificable:
            line.append('CERTIFICAT')
        elif revisio.name.ct_baixa:
            line.append('BAIXA')
        #Si està en obres
        elif revisio.name.ct_obres:
            line.append('OBRES')
        elif revisio.name.id_installacio.name == 'CM' and not revisio.data:
            line.append('CENTRE MESURA')
        elif revisio.name.data_industria and relativedelta(datetime.strptime(revisio.trimestre.data_inici, '%Y-%m-%d'), datetime.strptime(revisio.name.data_industria, '%Y-%m-%d')).years < 6:
            # No ens hem passat dels 6 anys
            line.append('CFO')
        else: # la revisio sempre sera revisio.state != 'tancada':
            line.append('PENDENT')
        lines.append(line)

    revisio_at_ids = revisio_lat_obj.search(
        cr, uid,
        [('trimestre.trieni', 'in', trienis),
         ('name.propietari', '=', True)]
    )

    # Per a cada revisió de LAT
    for revisio in revisio_lat_obj.browse(cr, uid, revisio_at_ids, context):
        line = []
        #Afegim una L pq és del tipus LAT
        line.append('L')
        #ID
        line.append(revisio.id)
        #Trimestre
        line.append(revisio.trimestre.name)
        #Data
        line.append(revisio.data)
        #LAT
        line.append(revisio.name.name)
        #Descripció
        line.append(revisio.name.descripcio)
        #Tipus d'instal·lació
        line.append(revisio.name.tipus_installacio.name)
        #Resultat (on fem els càlculs
        if revisio.state == 'tancada' and not revisio.certificable:
            line.append('ACTA')
        elif revisio.state == 'tancada' and revisio.certificable:
            line.append('CERTIFICAT')
        elif revisio.name.longitud_aeria_cad == 0:
            line.append('SUBTERRANIA')
        elif revisio.name.data_industria and relativedelta(datetime.strptime(revisio.trimestre.data_inici, '%Y-%m-%d'), datetime.strptime(revisio.name.data_industria, '%Y-%m-%d')).years < 6:
            # No ens hem passat dels 6 anys
            line.append('CFO')
        elif revisio.name.longitud_aeria_cad > 0 and revisio.state != 'tancada':
            line.append('PENDENT')
        lines.append(line)
    output = StringIO.StringIO()
    writer = csv.writer(output, delimiter=';', quotechar='', quoting=csv.QUOTE_NONE)
    for line in lines:
        line = [isinstance(t, basestring) and t.encode('utf-8') or t for t in line]
        writer.writerow(line)
    file = base64.b64encode(output.getvalue())
    output.close()
    return {'name': 'revisions_%s.csv' % datetime.now().strftime('%Y%m%d') , 'file': file}

    return {}

_csv_defectes_form = """<?xml version="1.0"?>
<form string="Fitxer Defectes CSV">
  <field name="name" colspan="4" width="500"/>
  <field name="file" colspan="4"/>
</form>
"""

_csv_defectes_fields = {
  'name': {'string': 'Nom', 'type': 'char', 'size': 60},
  'file': {'string': 'Fitxer', 'type': 'binary'},
}

def _csv_defectes(self, cr, uid, data, context={}):
    pool = pooler.get_pool(cr.dbname)
    lines = []
    # Per a cada revisió de CTS
    revisio_ct_obj = pool.get('giscedata.revisions.ct.revisio')
    revisio_ct_ids = revisio_ct_obj.search(
        cr, uid,
        [('trimestre.trieni', 'in', trienis),
         ('name.propietari', '=', True)]
    )
    for revisio in revisio_ct_obj.browse(cr, uid, revisio_ct_ids, context):
        if revisio.state == 'tancada' and not revisio.certificable:
            for defecte in revisio.defectes_ids:
                if defecte.estat == 'B' and not defecte.intern and not defecte.reparat:
                    line = []
                    line.append('C')
                    line.append(revisio.id)
                    line.append(defecte.codi)
                    line.append(defecte.descripcio)
                    lines.append(line)

    revisio_lat_obj = pool.get('giscedata.revisions.at.revisio')
    revisio_at_ids = revisio_lat_obj.search(
        cr, uid,
        [('trimestre.trieni', 'in', trienis),
         ('name.propietari', '=', True)]
    )
    for revisio in revisio_lat_obj.browse(cr, uid, revisio_at_ids, context):
        if revisio.state == 'tancada' and not revisio.certificable:
            for defecte in revisio.defectes_ids:
                if not defecte.intern and not defecte.reparat:
                    line = []
                    line.append('L')
                    line.append(revisio.id)
                    line.append(defecte.defecte_id.name)
                    line.append(defecte.defecte_id.descripcio)
                    lines.append(line)

    output = StringIO.StringIO()
    writer = csv.writer(output, delimiter=';', quoting=csv.QUOTE_NONE)
    for line in lines:
        writer.writerow(line)
    file = base64.b64encode(output.getvalue())
    output.close()
    return {'name': 'defectes_%s.csv' % datetime.now().strftime('%Y%m%d') , 'file': file}

    return {}


class wizard_revisions_export(wizard.interface):

    states = {
      'init': {
          'actions': [_init],
          'result': { 'type' : 'state', 'state' : 'csv_revisions' },
      },
      'csv_revisions': {
          'actions': [_csv_revisions],
          'result': {'type': 'form', 'arch': _csv_revisions_form, 'fields': _csv_revisions_fields, 'state': [('csv_defectes', 'Defectes', 'gtk-forward')]}
      },
      'csv_defectes': {
          'actions': [_csv_defectes],
          'result': {'type': 'form', 'arch': _csv_defectes_form, 'fields': _csv_defectes_fields, 'state': [('end', 'Tancar', 'gtk-close')]}
      },
      'end': {
        'actions': [],
        'result': {'type': 'state', 'state': 'end'}
      },
    }

wizard_revisions_export('giscedata.revisions.export')
