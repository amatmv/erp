# -*- coding: utf-8 -*-
{
    "name": "GISCE Data Revisions Export",
    "description": """Revisions""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Revisions",
    "depends":[
        "giscedata_revisions"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_revisions_export_wizard.xml"
    ],
    "active": False,
    "installable": True
}
