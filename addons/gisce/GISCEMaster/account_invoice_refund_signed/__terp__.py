# -*- coding: utf-8 -*-
{
    "name": "Factures Abonadores amb signe",
    "description": """Afegeix el signe negatiu a les factures abonadores""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends": [
        "account_invoice_base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "account_invoice_view.xml"
    ],
    "active": False,
    "installable": True
}
