# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from expects import *


class AccountInvoiceRefundTests(testing.OOTestCase):

    def test_fields_replaced(self):
        fact_obj = self.openerp.pool.get('account.invoice')
        with Transaction().start(self.database) as txn:
            view = fact_obj.fields_view_get(
                txn.cursor, txn.user, view_type='tree'
            )

            assert ('signed_amount_total' in view['fields'].keys())
            string_field = view['fields']['signed_amount_total']['string']
            assert (u'Total' == string_field)

            assert ('signed_amount_untaxed' in view['fields'].keys())
            string_field = view['fields']['signed_amount_untaxed']['string']
            assert (u'Base' == string_field)

            assert ('signed_residual' in view['fields'].keys())
            string_field = view['fields']['signed_residual']['string']
            assert (u'Residual' == string_field)
