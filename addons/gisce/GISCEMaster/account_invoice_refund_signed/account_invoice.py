# -*- coding: utf-8 -*-
from osv import osv, fields
from tools import config


class AccountInvoice(osv.osv):

    _name = 'account.invoice'
    _inherit = 'account.invoice'

    def _ff_signed_amount(self, cursor, uid, ids, field_name, arg, context=None):
        if context is None:
            context = {}
        if not isinstance(ids, (tuple, list)):
            ids = [ids]
        model_fields = [
            'amount_total', 'amount_tax', 'amount_untaxed', 'residual'
        ]
        res = {k: {} for k in ids}
        for fact in self.read(cursor, uid, ids, ['type'] + model_fields):
            for column in model_fields:
                if fact['type'] in ('out_refund', 'in_refund'):
                    res[fact['id']]['signed_' + column] = -1.0 * fact[column]
                else:
                    res[fact['id']]['signed_' + column] = fact[column]
        return res

    _columns = {
        'signed_amount_total': fields.function(
            _ff_signed_amount, type='float',
            digits=(16, int(config['price_accuracy'])),
            string='Total', method=True, multi='signed',
        ),
        'signed_amount_tax': fields.function(
            _ff_signed_amount, type='float',
            digits=(16, int(config['price_accuracy'])),
            string='Tax', method=True, multi='signed',
        ),
        'signed_amount_untaxed': fields.function(
            _ff_signed_amount, type='float',
            digits=(16, int(config['price_accuracy'])),
            string='Base', method=True, multi='signed',
        ),
        'signed_residual': fields.function(
            _ff_signed_amount, type='float',
            digits=(16, int(config['price_accuracy'])),
            string='Residual', method=True, multi='signed',
        )
    }


AccountInvoice()
