# -*- coding: utf-8 -*-

from osv import osv, fields
from giscedata_administracio_publica_cnmc_distri.giscedata_tipus_installacio import TIPUS_REGULATORI


class GiscedataDespatx(osv.osv):
    """Node Despatx del CNMC"""
    _name = 'giscedata.despatx'
    _description = "Despatx del CNMC"

    _columns = {
        'active': fields.boolean('Actiu', select=True),
        'data_baixa': fields.date('Data baixa'),
        'baixa': fields.boolean('Baixa'),
        'name': fields.char('Identificador', size=22, required=True),
        'cini': fields.char('Cini', size=8),
        'denominacio': fields.char('Denominacio', size=100),
        'data_apm': fields.date('Data APM', required=True),
        'any_ps': fields.integer('Any_PS'),
        'vai': fields.float('VAI'),
        '4771_entregada': fields.json('Dades 4771 entregades'),
        '4131_entregada_2016': fields.json('Dades 4131 entregades 2016'),
        '4666_entregada_2017': fields.json('Dades entregades 4666 2017'),
        '4666_entregada_2018': fields.json('Dades entregades 4666 2018'),
        "criteri_regulatori": fields.selection(
            TIPUS_REGULATORI,
            required=True,
            string="Criteri regulatori",
            help="Indica si el criteri a seguir al genrar els fitxers d'"
                 "inventari.\nSegons criteri:S'usa el criteri normal\n Froçar "
                 "incloure:S'afegeix l'element al fitxer\n Forçar excloure: "
                 "L'element no apareixera mai",
            select=1
        ),
        '4666_entregada_2019': fields.json('Dades entregades 4666 2019')
    }
    _defaults = {
        'active': lambda *a: 1,
        "criteri_regulatori": lambda *a: "criteri"
    }
    _sql_constraints = [
        ('name', 'unique (name)',
         'Ja existeix un despatx amb aquest identificador')
    ]

    _order = "id asc"

GiscedataDespatx()
