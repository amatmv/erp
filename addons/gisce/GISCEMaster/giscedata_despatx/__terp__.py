# -*- coding: utf-8 -*-
{
    "name": "GISCE DESPATX",
    "description": """Modulo de despachos para Gisce ERP""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "base_extended",
        "infraestructura",
        "giscedata_cne"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_despatx_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
