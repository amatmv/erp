# -*- coding: utf-8 -*-

from osv import osv, fields


class GiscedataTransformadorTrafo(osv.osv):
    _name = 'giscedata.transformador.trafo'
    _inherit = 'giscedata.transformador.trafo'

    _columns = {
        'obres': fields.many2many(
            'giscedata.obres.obra',
            'giscedata_obres_obra_trafos_rel',
            'trafo_id',
            'obra_id',
            'Obres'
        )
    }

GiscedataTransformadorTrafo()