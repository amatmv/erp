# -*- coding: utf-8 -*-
{
    "name": "Obres per transformadors",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Obres a Transformadors
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "giscedata_transformadors",
        "giscedata_obres"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_transformadors_view.xml"
    ],
    "active": False,
    "installable": True
}
