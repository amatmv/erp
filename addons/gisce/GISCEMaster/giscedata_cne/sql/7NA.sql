SELECT to_char(DATE(%(dfinal_ener)s),'YYYY') AS ANYO,
(
	SELECT CASE
    WHEN to_char(DATE(%(dfinal_ener)s),'MM')::int in (1,2,3) THEN 'T1'
    WHEN to_char(DATE(%(dfinal_ener)s),'MM')::int in (4,5,6) THEN 'T2'
    WHEN to_char(DATE(%(dfinal_ener)s),'MM')::int in (7,8,9) THEN 'T3'
    WHEN to_char(DATE(%(dfinal_ener)s),'MM')::int in (10,11,12) THEN 'T4'
	END
) AS TRIMESTRE,
(
	SELECT ref2 FROM res_partner WHERE id = 1
) AS COD_DIS1,
r.ref2 AS COD_COM3,
pol.agree_tipus AS COD_TPM,
(
	SELECT CASE
    WHEN t.name = '2.0A' THEN '01'
    WHEN t.name = '2.0DHA' THEN '02'
    WHEN t.name = '3.0A' THEN '03'
    WHEN t.name = '3.0A LB' THEN '03'
    WHEN t.name = '3.1A' THEN '04'
    WHEN t.name = '3.1A LB' THEN '04'
    WHEN t.name = '6.1' THEN '05'
    WHEN t.name = '6.1A' THEN '05'
    WHEN t.name = '6.1B' THEN '17'
    WHEN t.name = '6.2' THEN '06'
    WHEN t.name = '6.3' THEN '07'
    WHEN t.name = '6.4' THEN '08'
    WHEN t.name = '6.5' THEN '09'
    WHEN t.name = '2.1A' THEN '10'
    WHEN t.name = '2.1DHA' THEN '11'
    WHEN t.name = '2.0DHS' THEN '12'
    WHEN t.name = '2.1DHS' THEN '13'
	END
) AS TIPO_TAR_ACCESO,
cs.code||'000' AS COD_PRV,
count(distinct(pol.id))::int AS NUM_SUM,
sum(COALESCE(consum.energia, 0)::int) AS ENERGIA
FROM giscedata_polissa_modcontractual modcon
LEFT JOIN giscedata_polissa pol ON modcon.polissa_id = pol.id
LEFT JOIN giscedata_polissa_tarifa t ON t.id = pol.tarifa
LEFT JOIN res_partner r ON modcon.comercialitzadora = r.id
LEFT JOIN giscedata_cups_ps cups ON pol.cups = cups.id
LEFT JOIN res_municipi mun ON cups.id_municipi = mun.id
LEFT JOIN res_country_state cs ON mun.state = cs.id
LEFT JOIN
(
	SELECT fact.polissa_id polissa_id, (sum(line.quantity) * CASE
	  WHEN inv.type IN ('out_refund', 'in_refund') THEN -1 ELSE 1
	END) energia
	FROM giscedata_facturacio_factura fact, giscedata_facturacio_factura_linia flin, account_invoice inv, account_invoice_line line
	WHERE fact.id = flin.factura_id
	AND inv.id = fact.invoice_id
	AND flin.invoice_line_id = line.id
	AND fact.data_inici BETWEEN  %(ini_per_energia)s AND %(fin_per_energia)s
	AND fact.data_final BETWEEN  %(ini_per_energia)s AND %(fin_per_energia)s
	AND flin.tipus = 'energia'
	GROUP BY fact.polissa_id, inv.type
) AS consum ON consum.polissa_id=pol.id
WHERE modcon.data_inici <= %(dfinal_ener)s
AND modcon.data_final >= %(dfinal_ener)s
AND t.name NOT LIKE 'RE%%'
GROUP BY ANYO, TRIMESTRE, COD_DIS1, COD_COM3, COD_TPM, TIPO_TAR_ACCESO, COD_PRV
