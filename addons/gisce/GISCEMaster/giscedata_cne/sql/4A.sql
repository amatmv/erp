SELECT
to_char(DATE(%(dfinal_noener)s),'YYYY') as AÑO,
(
  SELECT CASE WHEN to_char(DATE(%(dfinal_noener)s),'MM')::int in (1,2,3) THEN 'T1'
  WHEN to_char(DATE(%(dfinal_noener)s),'MM')::int in (4,5,6) THEN 'T2'
  WHEN to_char(DATE(%(dfinal_noener)s),'MM')::int in (7,8,9) THEN 'T3'
  WHEN to_char(DATE(%(dfinal_noener)s),'MM')::int in (10,11,12) THEN 'T4'
  END
) as TRIMESTRE,
distri.ref2 AS COD_DIS1,
comer.ref2 AS COD_COM3,
p.agree_tipus as COD_TPM,
11 AS MOTIVO_REP,
'001' AS MOTIVO_REP_OTROS,
count(*) AS NUM_SUM
FROM giscedata_switching AS sw
JOIN crm_case cm ON sw.case_id = cm.id
JOIN res_partner distri ON sw.company_id = distri.id
JOIN res_partner comer ON cm.partner_id = comer.id
JOIN res_partner comer_sortint ON sw.comer_sortint_id = comer_sortint.id
JOIN giscedata_polissa p ON sw.cups_polissa_id = p.id
WHERE
    (
        sw.id IN (
          SELECT hd.sw_id
          FROM giscedata_switching_c1_09 c1
          JOIN giscedata_switching_step_header hd ON c1.header_id=hd.id
          WHERE
              c1.rebuig != True
            AND
              (
                  c1.data_acceptacio >= %(dinici_noener)s
                AND
                  c1.data_acceptacio <= %(dfinal_noener)s
              )
        )
      OR
        sw.id IN (
          SELECT hd.sw_id
          FROM giscedata_switching_c2_09 c2
          JOIN giscedata_switching_step_header hd ON c2.header_id=hd.id
          WHERE
              c2.rebuig != True
            AND
              (
                  c2.data_acceptacio >= %(dinici_noener)s
                AND
                  c2.data_acceptacio <= %(dfinal_noener)s
              )
        )
    )
  AND
    (comer.ref not in ('0636','0638','0642','0644','0688'))
  AND
    (comer_sortint.ref in ('0636','0638','0642','0644','0688'))
GROUP BY AÑO, TRIMESTRE, COD_DIS1, COD_COM3, COD_TPM, MOTIVO_REP, MOTIVO_REP_OTROS;