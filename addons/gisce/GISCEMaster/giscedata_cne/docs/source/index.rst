.. Lectures i comptadors switching documentation master file, created by
   sphinx-quickstart on Wed Apr  3 14:21:33 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentació del mòdul de informes per la CNE
*********************************************

El mòdul permet al GISCE-ERP la generació dels informes referents a la 
circular 1/2005 de la CNE. Aquests informes venen definits per la mateixa **CNE**.

* `Anexo I: Instrucciones para completar la información solicitada en los formularios del Anexo II <https://sede.cne.gob.es/c/document_library/get_file?uuid=59598178-3f1c-47e5-98ee-b705b6c06397&groupId=10136>`_
* `Anexo II: Formularios de envío de información de la CNE <https://sede.cne.gob.es/c/document_library/get_file?uuid=ce663590-4c7d-411a-b902-0f543c815b10&groupId=10136>`_
* `Anexo III: Tablas Circular 1/2005 <https://sede.cne.gob.es/c/document_library/get_file?uuid=97fb83c3-dc74-40be-a5e2-fa8a813e29eb&groupId=10136>`_

Actualment, aquest mòdul és capaç de generar els següents informes:
===================================================================


**1:** Informe de sol·licituds de canvi de MR a ML i de comercialitzadora.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

* **Informe trimestral.**

1. Omplim el camp de la secció 'tipus de fitxer' amb el valor '1' del desplegable.
2. Omplim els camps de la secció 'periode del trimestre' amb la data inicial i data final del trimestre.
3. Premem el botó 'exportar' per generar l'informe. 
4. Quan s'ha generat l'informe apareix la secció 'nom del fitxer' on podrem escollir obrir o guardar l'informe.

En l'exemple hi ha les dates per generar l'informe del primer trimestre de l'any:

.. figure:: /_static/wizard_informe_1.png

* Les dades s'extreuen dels canvis de comercialitzadora en les modificacions contractuals entre les data inicial i data final especificades en la secció 'periode del trimestre', quan dues modificacions contractuals consecutives tenen diferent comercialitzadora. 
* Només es tenen en compte els canvis de comercialitzadora de 'CUR' a 'NO CUR' i de 'NO CUR' a 'NO CUR'. 
* Les dades s'agrupen per diferent tipus de punt de mesura.


 
**2NA1:** Informe de sol·licituds acceptades de canvi de comercialitzadora en funció del retràs fins a la data de canvi efectiu.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

* **Informe trimestral.**

1. Omplim el camp de la secció 'tipus de fitxer' amb el valor '2NA1' del desplegable.
2. Omplim els camps de la secció 'periode del trimestre' amb la data inicial i data final del trimestre.
3. Premem el botó 'exportar' per generar l'informe. 
4. Quan s'ha generat l'informe apareix la secció 'nom del fitxer' on podrem escollir obrir o guardar l'informe.

En l'exemple hi ha les dates per generar l'informe del primer trimestre de l'any:

.. figure:: /_static/wizard_informe_2.png

* Les dades s'extreuen dels canvis de comercialitzadora en les modificacions contractuals entre les data inicial i data final especificades en la secció 'periode del trimestre', quan dues modificacions contractuals consecutives tenen diferent comercialitzadora. 
* Només es tenen en compte els canvis de comercialitzadora de 'CUR' a 'NO CUR', de 'NO CUR' a 'NO CUR' i de 'NO CUR' a 'CUR'. 
* Les dades s'agrupen per diferents comercialitzadores d'entrada i sortida, tipus de punt de mesura i tarifa d'accés.



**5A:** Informe de classifació dels consumidors en funció del número de canvis efectuats de MR a ML.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

* **Informe semestral.**

1. Omplim el camp de la secció 'tipus de fitxer' amb el valor '5A' del desplegable.
2. Omplim els camps de la secció 'periode del trimestre' amb la data inicial i data final del trimestre.
3. Premem el botó 'exportar' per generar l'informe. 
4. Quan s'ha generat l'informe apareix la secció 'nom del fitxer' on podrem escollir obrir o guardar l'informe.

En l'exemple hi ha les dates per generar l'informe del primer semestre de l'any:

.. figure:: /_static/wizard_informe_5a.png

* Les dades s'extreuen dels canvis de comercialitzadora en les modificacions contractuals entre les data inicial i data final especificades en la secció 'periode del trimestre', quan dues modificacions contractuals consecutives tenen diferent comercialitzadora. 
* Només es tenen en comtpe els canvis de comercialitzadora de 'CUR' a 'NO CUR'. 
* Les dades s'agrupen per diferent tipus de punt de mesura. 



**5B:** Informe de classifació dels consumidors en funció del número de canvis efectuats de comercialitzadora.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

* **Informe semestral.**

1. Omplim el camp de la secció 'tipus de fitxer' amb el valor '5B' del desplegable.
2. Omplim els camps de la secció 'periode del trimestre' amb la data inicial i data final del trimestre.
3. Premem el botó 'exportar' per generar l'informe. 
4. Quan s'ha generat l'informe apareix la secció 'nom del fitxer' on podrem escollir obrir o guardar l'informe.

En l'exemple hi ha les dates per generar l'informe del primer semestre de l'any:

.. figure:: /_static/wizard_informe_5b.png

* Les dades s'extreuen dels canvis de comercialitzadora en les modificacions contractuals entre les data inicial i data final especificades en la secció 'periode del trimestre', quan dues modificacions contractuals consecutives tenen diferent comercialitzadora. 
* Només es tenen en compte els canvis de comercialitzadora de 'NO CUR' a 'NO CUR'. 
* Les dades s'agrupen per diferent tipus de punt de mesura.



**7NA:** Informe de classificació dels consumidors en funció del comercialitzador, tipus de punt de subministre, tipus de tarifa d'accés i la provincia on s'ubica el punt de subministrament.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

* **Informe trimestral.**

1. Omplim el camp de la secció 'tipus de fitxer' amb el valor '7NA' del desplegable.
2. Omplim el camp de la secció 'trimestre' amb la data final del trimestre.
3. Omplim els camps de la secció 'periode d'energia' amb la data inicial i la data final dels quatre últims trimestres anteriors al trimestre escollit en el camp de la secció 'trimestre'.
4. Premem el botó 'exportar' per generar l'informe. 
5. Quan s'ha generat l'informe apareix la secció 'nom del fitxer' on podrem escollir obrir o guardar l'informe.

En l'exemple hi ha les dates per generar l'informe del primer trimestre de l'any, amb els quatre últims trimestres anteriors al primer trimestre de l'any com a periode d'energia:

.. figure:: /_static/wizard_informe_7.png

* Les dades dels consumidors s'extreuen de les modificacions contractuals, on la data especificada a la secció 'trimestre' es troba entre les data inicial i data final de la modificació contractual. 
* L'energia s'extreu de les factures, amb data de factura entre les data inicial i data final de la secció 'periode d'energia'. 
* Ambdues es relacionen i s'agrupen per diferents comercialitzadores, tipus de punt de mesura, tarifa d'accés i provincia.
