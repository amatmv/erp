# -*- coding: utf-8 -*-
import wizard
import pooler

_escollir_any_form = """<?xml version="1.0"?>
<form string="Generar Estadística CNE" col="2">
  <field name="year" />
  <field name="print_pdf" />
</form>"""

_escollir_any_fields = {
  'year': {'string': 'Any', 'type': 'char', 'size': 4, 'required': True},
  'print_pdf': {'string': 'Mostrar PDF al finalitzar', 'type': 'boolean'},
}

def _calc(self, cr, uid, data, context={}):
  data['print_ids'] = []
  estadistica = pooler.get_pool(cr.dbname).get('giscedata.cne.estadistica')
  estadistica_lat = pooler.get_pool(cr.dbname).get('giscedata.cne.estadistica.lat')
  estadistica_lat_cedits = pooler.get_pool(cr.dbname).get('giscedata.cne.estadistica.lat.cedits')
  estadistica_cts = pooler.get_pool(cr.dbname).get('giscedata.cne.estadistica.cts')
  estadistica_cts_cedits = pooler.get_pool(cr.dbname).get('giscedata.cne.estadistica.cts.cedits')
  estadistica_crs = pooler.get_pool(cr.dbname).get('giscedata.cne.estadistica.crs')
  estadistica_crs_trafo = pooler.get_pool(cr.dbname).get('giscedata.cne.estadistica.crs.trafo')
  cts_obj = pooler.get_pool(cr.dbname).get('giscedata.cts')
  
  # Busquem l'any per trobar la revisio
  estadistica_ids = estadistica.search(cr, uid,
    [('name', '=', data['form']['year'])],0,1,'revisio DESC')
  if estadistica_ids:
    revisio = estadistica.read(cr, uid,
            estadistica_ids[0], ['revisio'])['revisio'] + 1
  else:
    revisio = 0

  # Per a cada provincia en (Trafos, LAT, LBT, CR i SE)
  cr.execute("""select
  distinct p.id as id 
from 
  giscedata_transformador_trafo t, 
  giscedata_cts ct, 
  res_municipi m, 
  res_country_state p, 
  giscedata_cts_installacio inst, 
  giscedata_transformador_estat e, 
  giscedata_transformador_connexio c 
where 
  c.trafo_id = t.id 
  and t.id_estat = e.id 
  and ct.id_installacio = inst.id 
  and t.ct = ct.id 
  and ct.id_municipi = m.id 
  and m.state = p.id 
  and (t.tiepi = True or t.estadistica = True)
  and (t.reductor = False or t.reductor is null) 
  and e.codi = 1 
  and c.conectada = True 
union 
select 
  distinct p.id as id 
from 
  res_municipi m, 
  res_country_state p, 
  giscedata_at_linia lat, 
  giscedata_at_tram tram 
where 
  lat.municipi = m.id 
  and m.state = p.id 
  and tram.linia = lat.id 
  and (lat.baixa = False or lat.baixa is null)
  and lat.propietari = True 
  and (tram.baixa = False  or tram.baixa is null)
  and lat.tensio is not null 
  and tram.tipus in (1,2) 
union 
select 
  distinct p.id as id 
from 
  giscedata_bt_element e, 
  res_municipi m, 
  res_country_state p 
where 
  e.municipi = m.id 
  and m.state = p.id 
  and (e.baixa = False or e.baixa is null) 
  and e.tipus_linia in (1,2) 
union 
select 
  distinct p.id as id 
from 
  giscedata_cts ct, 
  giscedata_cts_installacio inst, 
  res_municipi m, 
  res_country_state p 
where 
  ct.id_municipi = m.id 
  and m.state = p.id 
  and ct.id_installacio = inst.id 
  and inst.name in ('CR', 'SE')""")
  for provincia in cr.dictfetchall():
  
    # Creem el nou any
    year_id = estadistica.create(cr, uid, {'name': data['form']['year'],
                                           'provincia': provincia['id'],
                                           'revisio': revisio})
    
    # Calculem les dades dels trafos
    cr.execute("""select 
  tcne.id as tipus, 
  count(t.id) as num, 
  ct.cedida as cedida, 
  case 
    when t.potencia_nominal < 20 
      then '0' 
    when t.potencia_nominal >= 20 and t.potencia_nominal < 25 
      then '20' 
    when t.potencia_nominal >= 25 and t.potencia_nominal < 30 
      then '25' 
    when t.potencia_nominal >= 30 and t.potencia_nominal < 50 
      then '30' 
    when t.potencia_nominal >= 50 and t.potencia_nominal < 75 
      then '50' 
    when t.potencia_nominal >= 75 and t.potencia_nominal < 100 
      then '75' 
    when t.potencia_nominal >= 100 and t.potencia_nominal < 125 
      then '100' 
    when t.potencia_nominal >= 125 and t.potencia_nominal < 160 
      then '125' 
    when t.potencia_nominal >= 160 and t.potencia_nominal < 200 
      then '160' 
    when t.potencia_nominal >= 200 and t.potencia_nominal < 250 
      then '200' 
    when t.potencia_nominal >= 250 and t.potencia_nominal < 315 
      then '250' 
    when t.potencia_nominal >= 315 and t.potencia_nominal < 400 
      then '315' 
    when t.potencia_nominal >= 400 and t.potencia_nominal < 500 
      then '400' 
    when t.potencia_nominal >= 500 and t.potencia_nominal < 630 
      then '500' 
    when t.potencia_nominal >= 630 and t.potencia_nominal < 800 
      then '630' 
    when t.potencia_nominal >= 800 and t.potencia_nominal < 1000 
      then '800' 
    when t.potencia_nominal >= 1000 and t.potencia_nominal < 1250 
      then '1000' 
    when t.potencia_nominal >= 1250 
      then '1250' 
  end as t_name 
from 
  giscedata_transformador_trafo t, 
  giscedata_cts ct, 
  res_municipi m, 
  res_country_state p, 
  giscedata_cts_installacio inst, 
  giscedata_transformador_estat e, 
  giscedata_cts_subtipus st, 
  giscedata_cne_ct_tipus tcne 
where 
  t.id_estat = e.id 
  and ct.id_subtipus = st.id 
  and st.categoria_cne = tcne.id 
  and ct.id_installacio = inst.id 
  and t.ct = ct.id 
  and ct.id_municipi = m.id 
  and m.state = p.id 
  and p.id = %s 
  and (t.tiepi = True or t.estadistica = True)
  and (t.reductor = False or t.reductor is null)
  and e.codi = 1 
group by 
  t_name,
  cedida, 
  tcne.id""", (provincia['id'],))
    for trafo in cr.dictfetchall():
      trafo['name'] = trafo['t_name']
      del trafo['t_name']
      trafo['year'] = year_id
      if trafo['cedida']:
        del trafo['cedida']
        estadistica_cts_cedits.create(cr, uid, trafo)
      else:
        del trafo['cedida']
        estadistica_cts.create(cr, uid, trafo)

    # Calculem les dades de linies AT
    cr.execute("""select 
  sum(tram.longitud_cad::float) as long, 
  tram.cedida, 
  tram.tipus, 
  case 
    when lat.tensio < 1000 
      then '1000' 
    when lat.tensio >= 1000 and lat.tensio < 36000 
      then '36000' 
    when lat.tensio >= 36000 and lat.tensio < 72500 
      then '72500' 
    when lat.tensio >= 72500 and lat.tensio < 145000 
      then '145000' 
    when lat.tensio >= 145000 
      then '145001' 
  end as t_name 
from 
  res_municipi m, 
  res_country_state p, 
  giscedata_at_linia lat, 
  giscedata_at_tram tram 
where 
  lat.municipi = m.id 
  and m.state = p.id 
  and p.id = %s 
  and tram.linia = lat.id 
  and (lat.baixa = False or lat.baixa is null) 
  and lat.propietari = True 
  and (tram.baixa = False or tram.baixa is null) 
  and lat.tensio is not null 
  and tram.tipus in (1,2) 
group by 
  t_name, 
  tram.tipus, 
  tram.cedida 
union 
select 
  sum(e.longitud_cad) as long, 
  e.cedida, 
  e.tipus_linia as tipus, 
  '1000' as t_name 
from 
  giscedata_bt_element e, 
  res_municipi m, 
  res_country_state p 
where 
  e.municipi = m.id 
  and m.state = p.id 
  and p.id = %s 
  and (e.baixa = False or e.baixa is null) 
  and e.tipus_linia in (1,2) 
group by 
  e.tipus_linia, 
  e.cedida""", (provincia['id'], provincia['id']))
    for lat in cr.dictfetchall():
      vals = {}
      vals['name'] = lat['t_name']
      vals['year'] = year_id
      if lat['tipus'] == 2:
        vals['sub'] = lat['long']
      else:
        vals['aer'] = lat['long']
      if lat['cedida']:
        estadistica_lat_cedits.create(cr, uid, vals)
      else:
        estadistica_lat.create(cr, uid, vals)

    # Creem línies buides per les linies
    names = ['1000', '36000', '72500', '145000', '145001']
    for name in names:
      estadistica_lat.create(cr, uid, {'name': name, 'year': year_id})
      estadistica_lat_cedits.create(cr, uid, {'name': name, 'year': year_id})

    # Aquí posarem els CR i SE
    search_params = [
      ('id_installacio.name', 'in', ('CR', 'SE')),
      ('id_municipi.state', '=', provincia['id']),
    ]

    cts_ids = cts_obj.search(cr, uid, search_params)
    for ct in cts_obj.browse(cr, uid, cts_ids):
      # Creem el CR o SE
      vals = {
        'name': '%s "%s"' % (ct.name.strip(), ct.descripcio.strip()),
        'tensio': ct.tensio_p,
        'year': year_id,
      }
      id_cr = estadistica_crs.create(cr, uid, vals)
      n_trafos = 0
      for trafo in ct.transformadors:
        tensio = 0
        if trafo.reductor and trafo.id_estat.codi == 1:
          # Es bo busquem quina és la seva tensio
          for connexio in trafo.conexions:
            if connexio.conectada:
              if connexio.connectada_p2:
                # Hem de retornar els valors de P2
                tensio = connexio.tensio_p2
              else:
                # Hem de retornar els valors de P1
                tensio = connexio.tensio_primari
          vals = {
            'name': str(trafo.name).strip(),
            'cr': id_cr,
            'tensio': tensio/1000,
            'potencia': trafo.potencia_nominal/1000,
          }
          estadistica_crs_trafo.create(cr, uid, vals)
          n_trafos += 1

      estadistica_crs.write(cr, uid, [id_cr], {'n_trafos': n_trafos})


    data['print_ids'].append(year_id)

  return {}

def _check_print_report(self, cr, uid, data, context={}):
  if len(data['print_ids']) and data['form']['print_pdf']:
    return 'print_report'
  elif not len(data['print_ids']):
    return 'end'
  elif len(data['print_ids']) and not data['form']['print_pdf']:
    return 'mostrar_tab'

def _print_report(self, cr, uid, data, context={}):
  return {'ids': data['print_ids']}

def _mostrar_tab(self, cr, uid, data, context={}):
  action = {
    'domain': "[('id','in', ["+','.join(map(str,map(int, data['print_ids'])))+"])]",
		'view_type': 'form',
		'view_mode': 'tree,form',
		'res_model': 'giscedata.cne.estadistica',
		'view_id': False,
		'type': 'ir.actions.act_window',
                'name': 'Estadística CNE %s' % (data['form']['year']),
                'res_id': False,
                'limit': len(data['print_ids']),
                'auto_refresh': False,
  }
  return action

def _pre_end(self, cr, uid, data, context={}):
  return {}


class wizard_estadistica_cne(wizard.interface):

  states = {
    'init': {
    	'actions': [],
      'result': {'type': 'state', 'state': 'escollir_any'}
    },
    'escollir_any': {
      'actions': [],
      'result': {'type': 'form', 'arch': _escollir_any_form, 'fields': _escollir_any_fields, 'state': [('calc', 'Generar')]}
    },
    'calc': {
    	'actions': [_calc],
      'result': {'type': 'choice', 'next_state':_check_print_report}
    },
    'print_report': {
      'actions': [_print_report],
      'result': {'type': 'print', 'report': 'giscedata.cne.estadistica', \
        'get_id_from_action':True, 'state':'pre_end'}
    },
    'mostrar_tab': {
      'actions': [],
      'result': {'type': 'action', 'action': _mostrar_tab, 'state': 'pre_end'}
    },
    'pre_end': {
    	'actions': [_pre_end],
    	'result': { 'type' : 'state', 'state' : 'end' },
    },
  }

wizard_estadistica_cne('giscedata.cne.estadistica')


