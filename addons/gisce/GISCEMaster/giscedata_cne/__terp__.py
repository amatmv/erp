# -*- coding: utf-8 -*-
{
    "name": "GISCE CNE",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "base_extended",
        "giscedata_administracio_publica_cne",
        "giscedata_cts",
        "giscedata_transformadors",
        "giscedata_at",
        "giscedata_bt"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_cne_view.xml",
        "giscedata_cne_wizard.xml",
        "giscedata_cne_report.xml",
        "giscedata_cne_data.xml",
        "wizard/wizard_1_2005_view.xml",
        "ir.model.access.csv",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
