# -*- coding: iso-8859-1 -*-
from osv import osv, fields
from tools.sql import install_array_agg
from tools import config
from datetime import datetime
import base64
import time
import netsvc
from tools import config
#
# Reports
#

class giscedata_cts(osv.osv):

    _name = 'giscedata.cts'
    _inherit = 'giscedata.cts'

    _columns = {
      'cedida': fields.boolean('Cedida'),
    }

    _defaults = {
      'cedida': lambda *a: 0,
    }

giscedata_cts()


class giscedata_at_tram(osv.osv):

    _name = 'giscedata.at.tram'
    _inherit = 'giscedata.at.tram'

    _columns = {
      'cedida': fields.boolean('Cedida'),
    }

    _defaults = {
      'cedida': lambda *a: 0,
    }

giscedata_at_tram()

class giscedata_bt_element(osv.osv):

    _name = 'giscedata.bt.element'
    _inherit = 'giscedata.bt.element'

    _columns = {
      'cedida': fields.boolean('Cedida'),
    }

    _defaults = {
      'cedida': lambda *a: 0,
    }

giscedata_bt_element()

class giscedata_cne_ct_tipus(osv.osv):

    _name = 'giscedata.cne.ct.tipus'

    _columns = {
      'name': fields.char('Nom', size=64),
      'codi': fields.char('Codi', size=1),
    }

giscedata_cne_ct_tipus()

class giscedata_cts_subtipus(osv.osv):

    _name = 'giscedata.cts.subtipus'
    _inherit = 'giscedata.cts.subtipus'

    _columns = {
      'categoria_cne': fields.many2one('giscedata.cne.ct.tipus', 'Categoria CNE'),
    }

giscedata_cts_subtipus()

class giscedata_cne_estadistica(osv.osv):

    _name = 'giscedata.cne.estadistica'

    _columns = {
      'name': fields.char('Any', size=4),
      'provincia': fields.many2one('res.country.state', 'Provincia'),
      'create_date': fields.datetime('Data Generació'),
      'revisio': fields.integer('Revisió'),
      'lat': fields.one2many('giscedata.cne.estadistica.lat', 'year', 'LAT'),
      'lat_cedits': fields.one2many('giscedata.cne.estadistica.lat.cedits', 'year', 'LAT Cedits'),
      'cts': fields.one2many('giscedata.cne.estadistica.cts', 'year', 'CTS'),
      'cts_cedits': fields.one2many('giscedata.cne.estadistica.cts.cedits', 'year', 'CTS Cedits'),
      'crs': fields.one2many('giscedata.cne.estadistica.crs', 'year', 'CRs / SEs')
    }

    _defaults = {
      'revisio': lambda *a: '0',
    }

    _order = 'create_date desc,provincia'

giscedata_cne_estadistica()


class giscedata_cne_estadistica_lat(osv.osv):
    _name = 'giscedata.cne.estadistica.lat'

    _columns = {
      'name': fields.selection([('1000', 'Hasta 1kV.'), ('36000', 'De 1 a 36 kV'), ('72500', 'De 36 a 72,5 kV'), ('145000', 'De 72,5 a 145 kV'), ('145001', 'Más de 145 kV')], 'Tensions', size=256),
      'aer': fields.float('Km Aéreas'),
      'sub': fields.float('Km Subterráneas'),
      'year': fields.many2one('giscedata.cne.estadistica', 'Any', ondelete='cascade'),
    }

    _defaults = {
      'aer': lambda *a: 0.0,
      'sub': lambda *a: 0.0,
    }

    _order = 'name asc'

    def create(self, cr, uid, vals, context={}):
        ids = self.search(cr, uid, [('name', '=', vals['name']), ('year', '=', vals['year'])])
        if len(ids):
            self.write(cr, uid, ids[0], vals)
            return ids[0]
        else:
            return super(osv.osv, self).create(cr, uid, vals, context)

giscedata_cne_estadistica_lat()

class giscedata_cne_estadistica_lat_cedits(osv.osv):
    _name = 'giscedata.cne.estadistica.lat.cedits'

    _columns = {
      'name': fields.selection([('1000', 'Hasta 1kV.'), ('36000', 'De 1 a 36 kV'), ('72500', 'De 36 a 72,5 kV'), ('145000', 'De 72,5 a 145 kV'), ('145001', 'Más de 145 kV')], 'Tensions', size=256),
      'aer': fields.float('Km Aéreas'),
      'sub': fields.float('Km Subterráneas'),
      'year': fields.many2one('giscedata.cne.estadistica', 'Any', ondelete='cascade'),
    }

    _defaults = {
      'aer': lambda *a: 0.0,
      'sub': lambda *a: 0.0,
    }

    _order = 'name asc'

    def create(self, cr, uid, vals, context={}):
        ids = self.search(cr, uid, [('name', '=', vals['name']), ('year', '=', vals['year'])])
        if len(ids):
            self.write(cr, uid, ids[0], vals)
            return ids[0]
        else:
            return super(osv.osv, self).create(cr, uid, vals, context)

giscedata_cne_estadistica_lat_cedits()


class giscedata_cne_estadistica_cts(osv.osv):

    _name = 'giscedata.cne.estadistica.cts'

    def _pot_selection(self, cr, uid, context={}):
        p = ['0', '20', '25', '30', '50', '75', '100', '125', '160', '200', '250', '315', '400', '500', '630', '800', '1000', '1250']

        return [(a, a) for a in p]

    _columns = {
      'name': fields.selection(_pot_selection, 'Potencia Normalizada kVA'),
      'tipus': fields.many2one('giscedata.cne.ct.tipus', 'Tipo', ondelete='restrict'),
      'num': fields.integer('Número'),
      'year': fields.many2one('giscedata.cne.estadistica', 'Any', ondelete='cascade'),
    }

    _defaults = {
      'num': lambda *a: 0,
    }

    _order = 'name asc'

    def create(self, cr, uid, vals, context={}):
        ids = self.search(cr, uid, [('name', '=', vals['name']), ('year', '=', vals['year']), ('tipus', '=', vals['tipus'])])
        if len(ids):
            self.write(cr, uid, ids[0], vals)
            return ids[0]
        else:
            return super(osv.osv, self).create(cr, uid, vals, context)

giscedata_cne_estadistica_cts()

class giscedata_cne_estadistica_cts_cedits(osv.osv):

    _name = 'giscedata.cne.estadistica.cts.cedits'

    def _pot_selection(self, cr, uid, context={}):
        p = ['0', '20', '25', '30', '50', '75', '100', '125', '160', '200', '250', '315', '400', '500', '630', '800', '1000', '1250']

        return [(a, a) for a in p]

    _columns = {
      'name': fields.selection(_pot_selection, 'Potencia Normalizada kVA'),
      'tipus': fields.many2one('giscedata.cne.ct.tipus', 'Tipo', ondelete='restrict'),
      'num': fields.integer('Número'),
      'year': fields.many2one('giscedata.cne.estadistica', 'Any', ondelete='cascade'),
    }

    _defaults = {
      'num': lambda *a: 0,
    }

    _order = 'name asc'

    def create(self, cr, uid, vals, context={}):
        ids = self.search(cr, uid, [('name', '=', vals['name']), ('year', '=', vals['year']), ('tipus', '=', vals['tipus'])])
        if len(ids):
            self.write(cr, uid, ids[0], vals)
            return ids[0]
        else:
            return super(osv.osv, self).create(cr, uid, vals, context)

giscedata_cne_estadistica_cts_cedits()


class giscedata_cne_estadistica_crs(osv.osv):

    _name = 'giscedata.cne.estadistica.crs'

    _columns = {
      'name': fields.char('Nombre subestación', size=255),
      'tipo': fields.selection([('C', 'Convencional'), ('B', 'Blindado')], 'Tipo'),
      'pos_trafo': fields.selection([('P', 'Posición'), ('T', 'Trafo')], 'Posición/Trafo'),
      'tensio': fields.char('Tensió', size=50),
      'n_trafos': fields.integer('Nº de Trafos'),
      'lineas': fields.integer('Lineas'),
      'year': fields.many2one('giscedata.cne.estadistica', 'Any', ondelete='cascade', required=True),
      'trafos': fields.one2many('giscedata.cne.estadistica.crs.trafo', 'cr', 'Trafos'),
    }

    _defaults = {
      'tipo': lambda *a: 'C',
      'pos_trafo': lambda *a: 'P',
      'n_trafos': lambda *a: 0,
      'lineas': lambda *a: 0,
    }

    _order = 'name asc'

giscedata_cne_estadistica_crs()

class giscedata_cne_estadistica_crs_trafo(osv.osv):

    _name = 'giscedata.cne.estadistica.crs.trafo'

    _columns = {
      'name': fields.char('Numero', size=10),
      'tensio': fields.integer('Tensio'),
      'potencia': fields.float('Potencia'),
      'cr': fields.many2one('giscedata.cne.estadistica.crs', 'CR', ondelete='cascade'),
    }

    _defaults = {
    }

giscedata_cne_estadistica_crs_trafo()
