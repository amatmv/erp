# -*- coding: utf-8 -*-
{
    "name": "GISCE CUPS multicompany",
    "description": """Multi-company support for CUPS""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Multi Company",
    "depends":[
        "giscedata_cups"
    ],
    "init_xml":[
        "record_rules.xml"
    ],
    "demo_xml": [],
    "update_xml":[
        "giscedata_cups_multicompany_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": False
}
