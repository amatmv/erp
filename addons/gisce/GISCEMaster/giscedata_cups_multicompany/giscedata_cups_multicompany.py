# -*- coding: iso-8859-1 -*-
from osv import osv, fields

class giscedata_cups_escomesa(osv.osv):
	_name = "giscedata.cups.escomesa"
	_inherit = "giscedata.cups.escomesa"
	_columns = {
		'company_id': fields.many2one('res.company', 'Company', required=True),
	}
	_defaults = {
	}
giscedata_cups_escomesa()
