# -*- coding: utf-8 -*-

from osv import osv,fields
from datetime import datetime

class giscedata_liquidacio_tarifes(osv.osv):

    _name = 'giscedata.liquidacio.tarifes'

    _columns = {
      'name': fields.char('Descripción', size=50),
      'codi': fields.char('Código', size=10),
      'tipo': fields.selection([('T', 'Tarifa'), ('P', 'Peaje')], 'Tipo'),
      'observacions': fields.char('Observaciones', size=256),
      'inici': fields.date('Válido desde'),
      'final': fields.date('Válido hasta'),
      'active': fields.boolean('Activa', required=True)
    }

    _order = 'name asc'

    _sql_constraints = [
        ('codi_uniq', 'unique (codi)', 'Ya existe una tarifa con este código.')
    ]

    _defaults = {
      'active': lambda *a: 1,
    }

giscedata_liquidacio_tarifes()


class giscedata_liquidacio_fpd(osv.osv):

    _name = 'giscedata.liquidacio.fpd'

    def check(self, cr, uid, ids, context={}):
        res = {}
        for fpd in self.browse(cr, uid, ids, context={}):
            message = ""
            warnings = 0
            errors = 0
            for t in fpd.tarifa:
                # NUMERO DE CLIENTS
                if not t.n_clients > 0:
                    message += "[A] El número de clientes debe ser mayor que 0\n"
                    warnings += 1
                # POTENCIA CONTRATADA
                if t.name.codi in ('401','415','416','417','418','419') and (t.pc_ph2 + t.pc_ph3 + t.pc_ph4 + t.pc_ph5 + t.pc_ph6 > 0):
                    message += "[A] Las potencias contratadas 2, 3, 4, 5 y 6 deben tener valor cero, para la tarifa%s (%s)\n" % (t.name.codi, t.name.name)
                    warnings += 1
                if t.name.codi in ('401','415','416','417','418','419') and t.pc_ph1 == 0:
                    message += "[A] La potencia contratada 1 debe ser distinta de cero para las Tarifas 2.0.A, 2.0.n.A\n"
                    warnings += 1
                if t.name.codi in ('403','404') and t.pc_ph4 + t.pc_ph5 + t.pc_ph6 > 0:
                    message += "[A] Las potencias contratadas 4, 5 y 6 deben tener valor cero para la tarifa %s (%s)\n" % (t.name.codi, t.name.name)
                    warnings += 1
                if t.name.codi in ('403','404') and (t.pc_ph1 == 0 or t.pc_ph2 == 0 or t.pc_ph3 == 0):
                    message += "[A] Las potencias contratadas 1, 2 y 3 deben tener valor distinto de cero para las Tarifas 3.0.A y 3.1.A\n"
                    warnings += 1
                if t.name.codi not in ('401','415','403','404','416','417','418','419','602','603','604') and not t.pc_ph1 + t.pc_ph2 + t.pc_ph3 + t.pc_ph4 + t.pc_ph5 + t.pc_ph6 > 0:
                    message += "[A] Tiene que haber al menos una potencia con valor distinto de cero para las Tarifas deistintas de la 2.0.A, 2.0.n.A, 3.0.A, 3.1.A\n"
                    warnings += 1
                # ENERGIA FACTURADA
                if t.name.codi in ('401','416','418','602','603','604') and (not t.energia_facturada > 0 or t.e_ph1 + t.e_ph2 + t.e_ph3 + t.e_ph4 + t.e_ph5 + t.e_ph6 > 0):
                    message += "[A] El campo Energía Facturada debe de estar rellenado y a cero los campos Energía PHi, para la Tarifa %s (%s)\n" % (t.name.codi, t.name.name)
                    warnings += 1
                if t.name.codi in ('415','417','419') and (not t.energia_facturada == t.e_ph1 + t.e_ph2 or t.e_ph3 + t.e_ph4 + t.e_ph5 + t.e_ph6 > 0):
                    message += "[A] Los campos energía facturada debe ser igual a la suma de energía de PH1 y PH2, energía PH1 y energía PH2 deben estar rellenados, y vacíos el resto de las energías PHi. Para la tarifa %s\n" % (t.name.codi)
                    warnings += 1
                if t.name.codi in ('403','404') and (not t.energia_facturada == t.e_ph1 + t.e_ph2 + t.e_ph3 or t.e_ph4 + t.e_ph5 + t.e_ph6 > 0):
                    message += "[A] Los campos energía facturada debe ser igual a la suma de energía PH1, PH2 y PH3, energía PH1, PH2 y PH3 deben estar rellenados, y vacíos el resto de las energías PHi. Para la tarifa %s\n" % (t.name.codi)
                    warnings += 1
                if t.name.codi not in ('401','416','418','602','603','604') and not t.energia_facturada == t.e_ph1 + t.e_ph2 + t.e_ph3 + t.e_ph4 + t.e_ph5 + t.e_ph6:
                    message += "[A] La Energía Facturada no es igual a la suma de las Energías PHi para la tarifa %s (%s)\n" % (t.name.codi, t.name.name)
                    warnings += 1
                # TODO: Comprovar identació
                t.write({'status': message, 'errors': errors, 'warnings': warnings})
        return True

    def _count(self, cr, uid, ids, field_name, *args):
        res = {}
        for t in self.browse(cr, uid, ids):
            res[t.id] = 1
        return res

    def xml_anual(self, cr, uid, company_id, year, context={}):
        from xml.dom import minidom
        res = {}
        # Generarem l'XML anual per empresa
        # Busquem totes les liquidacions d'aquesta empresa per l'any
        ids = self.search(cr, uid, [('year', '=', year), ('company_id.id', '=', company_id)])
        # Creem l'objecte per crear l'XML
        doc = minidom.Document()
        if not len(ids):
            return res
        else:
            filename = 'FPDA_%s12.%s.xml' % (year, self.browse(cr, uid, ids[0]).company_id.codi_r1.zfill(4))
            di_fact = doc.createElement("DI_FACTURACION_PEAJES")
            di_fact.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance")
            di_fact.setAttribute("xsi:noNamespaceSchemaLocation", "FPDp_aaaann.eeee.xsd")
            doc.appendChild(di_fact)
            for fpd in self.browse(cr, uid, ids, context):
                if len(fpd.tarifa):
                    for t in fpd.tarifa:
                        facturacion = doc.createElement("FACTURACION")
                        di_fact.appendChild(facturacion)

                        n = doc.createElement("DIPE_CODIGO_EMPRESA")
                        facturacion.appendChild(n)
                        txt = doc.createTextNode(fpd.company_id.codi_r1 or '')
                        n.appendChild(txt)

                        n = doc.createElement(u"DIPE_AÑO_FACTURACION")
                        facturacion.appendChild(n)
                        txt = doc.createTextNode(fpd.year)
                        n.appendChild(txt)

                        n = doc.createElement("DIPE_MES_FACTURACION")
                        facturacion.appendChild(n)
                        txt = doc.createTextNode(fpd.month)
                        n.appendChild(txt)

                        n = doc.createElement("DIPE_CODIGO_TARIFA")
                        facturacion.appendChild(n)
                        txt = doc.createTextNode(t.name.codi)
                        n.appendChild(txt)

                        n = doc.createElement("DIPE_NUMERO_CLIENTES")
                        facturacion.appendChild(n)
                        txt = doc.createTextNode(str(t.n_clients))
                        n.appendChild(txt)

                        n = doc.createElement("DIPE_POTENCIA_CONTRATADA_PH1")
                        facturacion.appendChild(n)
                        txt = doc.createTextNode(str(t.pc_ph1))
                        n.appendChild(txt)

                        n = doc.createElement("DIPE_POTENCIA_CONTRATADA_PH2")
                        facturacion.appendChild(n)
                        txt = doc.createTextNode(str(t.pc_ph2))
                        n.appendChild(txt)

                        n = doc.createElement("DIPE_POTENCIA_CONTRATADA_PH3")
                        facturacion.appendChild(n)
                        txt = doc.createTextNode(str(t.pc_ph3))
                        n.appendChild(txt)

                        n = doc.createElement("DIPE_POTENCIA_CONTRATADA_PH4")
                        facturacion.appendChild(n)
                        txt = doc.createTextNode(str(t.pc_ph4))
                        n.appendChild(txt)

                        n = doc.createElement("DIPE_POTENCIA_CONTRATADA_PH5")
                        facturacion.appendChild(n)
                        txt = doc.createTextNode(str(t.pc_ph5))
                        n.appendChild(txt)

                        n = doc.createElement("DIPE_POTENCIA_CONTRATADA_PH6")
                        facturacion.appendChild(n)
                        txt = doc.createTextNode(str(t.pc_ph6))
                        n.appendChild(txt)

                        n = doc.createElement("DIPE_POTENCIA_A_FACTURAR_PH1")
                        facturacion.appendChild(n)
                        txt = doc.createTextNode(str(t.pf_ph1))
                        n.appendChild(txt)

                        n = doc.createElement("DIPE_POTENCIA_A_FACTURAR_PH2")
                        facturacion.appendChild(n)
                        txt = doc.createTextNode(str(t.pf_ph2))
                        n.appendChild(txt)

                        n = doc.createElement("DIPE_POTENCIA_A_FACTURAR_PH3")
                        facturacion.appendChild(n)
                        txt = doc.createTextNode(str(t.pf_ph3))
                        n.appendChild(txt)

                        n = doc.createElement("DIPE_ENERGIA_PH1")
                        facturacion.appendChild(n)
                        txt = doc.createTextNode(str(t.e_ph1))
                        n.appendChild(txt)

                        n = doc.createElement("DIPE_ENERGIA_PH2")
                        facturacion.appendChild(n)
                        txt = doc.createTextNode(str(t.e_ph2))
                        n.appendChild(txt)

                        n = doc.createElement("DIPE_ENERGIA_PH3")
                        facturacion.appendChild(n)
                        txt = doc.createTextNode(str(t.e_ph3))
                        n.appendChild(txt)

                        n = doc.createElement("DIPE_ENERGIA_PH4")
                        facturacion.appendChild(n)
                        txt = doc.createTextNode(str(t.e_ph4))
                        n.appendChild(txt)

                        n = doc.createElement("DIPE_ENERGIA_PH5")
                        facturacion.appendChild(n)
                        txt = doc.createTextNode(str(t.e_ph5))
                        n.appendChild(txt)

                        n = doc.createElement("DIPE_ENERGIA_PH6")
                        facturacion.appendChild(n)
                        txt = doc.createTextNode(str(t.e_ph6))
                        n.appendChild(txt)

                        n = doc.createElement("DIPE_ENERGIA_FACTURADA")
                        facturacion.appendChild(n)
                        txt = doc.createTextNode(str(t.energia_facturada))
                        n.appendChild(txt)

                        n = doc.createElement("DIPE_FACT_POTENCIA")
                        facturacion.appendChild(n)
                        txt = doc.createTextNode(str(t.facturacio_potencia))
                        n.appendChild(txt)

                        n = doc.createElement("DIPE_FACT_ENERGIA")
                        facturacion.appendChild(n)
                        txt = doc.createTextNode(str(t.facturacio_energia))
                        n.appendChild(txt)

                        n = doc.createElement("DIPE_FACT_ENERGIA_REACTIVA")
                        facturacion.appendChild(n)
                        txt = doc.createTextNode(str(t.facturacio_energia_r))
                        n.appendChild(txt)

                        n = doc.createElement("DIPE_FACT_EXCESOS_POTENCIA")
                        facturacion.appendChild(n)
                        txt = doc.createTextNode(str(t.facturacio_excessos_pot))
                        n.appendChild(txt)

                        n = doc.createElement("DIPE_TOTAL_FACTURACION")
                        facturacion.appendChild(n)
                        txt = doc.createTextNode(str(t.total_facturacio))
                        n.appendChild(txt)

            res[filename] = doc.toxml("ISO-8859-1")
            return res

    def xml(self, cr, uid, ids, context={}):
        from xml.dom import minidom
        res = {}
        for fpd in self.browse(cr, uid, ids, context):
            filename = 'FPDM_%s%s.%s.xml' % (fpd.year, fpd.month.zfill(2), fpd.company_id.codi_r1.zfill(4))
            doc = minidom.Document()
            if len(fpd.tarifa):
                di_fact = doc.createElement("DI_FACTURACION_PEAJES")
                di_fact.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance")
                di_fact.setAttribute("xsi:noNamespaceSchemaLocation", "FPDp_aaaann.eeee.xsd")
                doc.appendChild(di_fact)
                for t in fpd.tarifa:
                    facturacion = doc.createElement("FACTURACION")
                    di_fact.appendChild(facturacion)

                    n = doc.createElement("DIPE_CODIGO_EMPRESA")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(fpd.company_id.codi_r1 or '')
                    n.appendChild(txt)

                    n = doc.createElement(u"DIPE_AÑO_FACTURACION")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(fpd.year)
                    n.appendChild(txt)

                    n = doc.createElement("DIPE_MES_FACTURACION")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(fpd.month)
                    n.appendChild(txt)

                    n = doc.createElement("DIPE_CODIGO_TARIFA")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(t.name.codi)
                    n.appendChild(txt)

                    n = doc.createElement("DIPE_NUMERO_CLIENTES")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.n_clients))
                    n.appendChild(txt)

                    n = doc.createElement("DIPE_POTENCIA_CONTRATADA_PH1")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.pc_ph1))
                    n.appendChild(txt)

                    n = doc.createElement("DIPE_POTENCIA_CONTRATADA_PH2")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.pc_ph2))
                    n.appendChild(txt)

                    n = doc.createElement("DIPE_POTENCIA_CONTRATADA_PH3")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.pc_ph3))
                    n.appendChild(txt)

                    n = doc.createElement("DIPE_POTENCIA_CONTRATADA_PH4")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.pc_ph4))
                    n.appendChild(txt)

                    n = doc.createElement("DIPE_POTENCIA_CONTRATADA_PH5")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.pc_ph5))
                    n.appendChild(txt)

                    n = doc.createElement("DIPE_POTENCIA_CONTRATADA_PH6")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.pc_ph6))
                    n.appendChild(txt)

                    n = doc.createElement("DIPE_POTENCIA_A_FACTURAR_PH1")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.pf_ph1))
                    n.appendChild(txt)

                    n = doc.createElement("DIPE_POTENCIA_A_FACTURAR_PH2")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.pf_ph2))
                    n.appendChild(txt)

                    n = doc.createElement("DIPE_POTENCIA_A_FACTURAR_PH3")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.pf_ph3))
                    n.appendChild(txt)

                    n = doc.createElement("DIPE_ENERGIA_PH1")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.e_ph1))
                    n.appendChild(txt)

                    n = doc.createElement("DIPE_ENERGIA_PH2")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.e_ph2))
                    n.appendChild(txt)

                    n = doc.createElement("DIPE_ENERGIA_PH3")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.e_ph3))
                    n.appendChild(txt)

                    n = doc.createElement("DIPE_ENERGIA_PH4")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.e_ph4))
                    n.appendChild(txt)

                    n = doc.createElement("DIPE_ENERGIA_PH5")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.e_ph5))
                    n.appendChild(txt)

                    n = doc.createElement("DIPE_ENERGIA_PH6")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.e_ph6))
                    n.appendChild(txt)

                    n = doc.createElement("DIPE_ENERGIA_FACTURADA")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.energia_facturada))
                    n.appendChild(txt)

                    n = doc.createElement("DIPE_FACT_POTENCIA")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.facturacio_potencia))
                    n.appendChild(txt)

                    n = doc.createElement("DIPE_FACT_ENERGIA")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.facturacio_energia))
                    n.appendChild(txt)

                    n = doc.createElement("DIPE_FACT_ENERGIA_REACTIVA")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.facturacio_energia_r))
                    n.appendChild(txt)

                    n = doc.createElement("DIPE_FACT_EXCESOS_POTENCIA")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.facturacio_excessos_pot))
                    n.appendChild(txt)

                    n = doc.createElement("DIPE_TOTAL_FACTURACION")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.total_facturacio))
                    n.appendChild(txt)

                res[filename] = doc.toxml("ISO-8859-1")
            else:
                res[filename] = ""

        return res

    def name_get(self, cr, uid, ids, context={}):
        if not len(ids):
            return []
        res = []
        for liq in self.browse(cr, uid, ids):
            name = '%s/%s' % (liq.month,liq.year)
            res.append((liq.id, name))
        return res

    def name_search(self, cursor, uid, name, args=[], operator='ilike',
                    context=None, limit=80):
        """
        Redefine the name_search method to allow searching by code.
        """
        ids = []
        if name:
            cursor.execute("select id from giscedata_liquidacio_fpd "
                           "where month::varchar || '/' || year::varchar "
                           "ilike %s", ('%' + name + '%',))
            ids = [a[0] for a in cursor.fetchall()]
        if ids:
            args += [('id', 'in', ids)]
        ids = self.search(cursor, uid, args, limit=limit)
        return self.name_get(cursor, uid, ids, context=context)

    _columns = {
      'company_id': fields.many2one('res.company', 'Empresa', readonly=True, ondelete='restrict'),
      'year': fields.char('Año', size=4, readonly=True),
      'month': fields.char('Mes', size=2, readonly=True),
      'inici': fields.date('Inicio'),
      'final': fields.date('Final'),
      'tarifa': fields.one2many('giscedata.liquidacio.fpd.tarifa', 'liquidacio_id', 'Tarifas', readonly=True, states={'borrador': [('readonly', False)], 'rectificacion':[('readonly', False)]}),
      'state': fields.selection([('borrador', 'Borrador'), ('para_liquidar', 'Para liquidar'), ('enviado', 'Enviado'), ('rectificacion', 'Rectificacion'), ('corregido', 'Corregido'), ('cerrada', 'Cerrada')], 'Estado', readonly=True),
      'count': fields.function(_count, type='integer', method=True),
      'active': fields.boolean('Activa', readonly=True, states={'enviado': [('readonly', False)], 'cerrada': [('readonly', False)]}),
    }

    _sql_constraints = [
        ('year_month_uniq', 'unique (year, month, company_id)', 'No se puede repetir la llave (mes,año)')
    ]

    _defaults = {
      'company_id': lambda self,cr,uid,c: self.pool.get('res.users').browse(cr, uid, uid, c).company_id.id,
      'active': lambda *a: 1,
      'month': lambda *a: datetime.now().strftime('%m'),
      'year': lambda *a: datetime.now().strftime('%Y'),
      'state': lambda *a: 'borrador',
    }

    _order = 'year desc, month desc'

    def para_liquidar(self, cr, uid, ids, args={}):
        from osv import osv
        for id in ids:
            # Validació Errors
            self.check(cr, uid, [id])
            liq = self.browse(cr, uid, id)
            for tarifa in liq.tarifa:
                if tarifa.errors > 0:
                    raise osv.except_osv('Error', 'La liquidación no es correcta. Compruebe los errores que se describen por cada tarifa.\n\n ** Pulse el botón Validación para ver de nuevo los errores **\n\nERRORES: %i AVISOS:%i\n\n%s' % (tarifa.errors, tarifa.warnings, tarifa.status))
            if len(liq.tarifa):
                # Validació XML
                from lxml import etree
                import StringIO
                xsd_file = 'addons/giscedata_liquidacio/xsd/FPDp_aaaamm.eeee.xsd'
                xml = self.xml(cr, uid, [id])
                xml = xml[xml.keys()[0]]
                f = open(xsd_file, 'r')
                xmlschema = etree.XMLSchema(etree.parse(StringIO.StringIO(f.read())))
                doc = etree.parse(StringIO.StringIO(xml))
                if not xmlschema(doc):
                    raise osv.except_osv('Error', 'El fichero XML no es válido.')
            self.write(cr, uid, [id], {'state': 'para_liquidar'})

giscedata_liquidacio_fpd()


class giscedata_liquidacio_fpd_tarifa(osv.osv):

    _name = 'giscedata.liquidacio.fpd.tarifa'

    def _total_facturacio(self, cr, uid, ids, field_name, *args):
        res = {}
        for t in self.browse(cr, uid, ids):
            res[t.id] = t.facturacio_excessos_pot + t.facturacio_potencia + t.facturacio_energia + t.facturacio_energia_r
        return res

    def _energia_facturada(self, cr, uid, ids, field_name, *args):
        res = {}
        for t in self.browse(cr, uid, ids):
            res[t.id] = t.e_ph1 + t.e_ph2 + t.e_ph3 + t.e_ph4 + t.e_ph5 + t.e_ph6
        return res

    def sumatori_energia_facturada(self, cr, uid, ids, *args):
        res = {}
        for t in self.browse(cr, uid, ids):
            ener = t.e_ph1 + t.e_ph2 + t.e_ph3 + t.e_ph4 + t.e_ph5 + t.e_ph6
            self.write(cr, uid, [t.id], {'energia_facturada': ener})
        return True

    def change_ph(self, cr, uid, ids, ph1,ph2,ph3,ph4,ph5,ph6):
        return {'value': {'energia_facturada': ph1+ph2+ph3+ph4+ph5+ph6}}

    _columns = {
      'name': fields.many2one('giscedata.liquidacio.tarifes', 'Tarifa', ondelete='restrict', domain=[('tipo', '=', 'P')], help="Código Asignado a cada tarifa de acceso"),
      'liquidacio_id': fields.many2one('giscedata.liquidacio.fpd', 'Liquidación', ondelete='cascade'),
      'n_clients': fields.integer('Número de clientes', help="Número de clientes facturados en el mes correspondientes a cada tarifa de acceso"),
      'pc_ph1': fields.integer('Potencia Contractada PH1', help="Potencia contratada, en el PH1"),
      'pc_ph2': fields.integer('Potencia Contractada PH2', help="Potencia contratada, en el PH2"),
      'pc_ph3': fields.integer('Potencia Contractada PH3', help="Potencia contratada, en el PH3"),
      'pc_ph4': fields.integer('Potencia Contractada PH4', help="Potencia contratada, en el PH4"),
      'pc_ph5': fields.integer('Potencia Contractada PH5', help="Potencia contratada, en el PH5"),
      'pc_ph6': fields.integer('Potencia Contractada PH6', help="Potencia contratada, en el PH6"),
      'pf_ph1': fields.integer('Potencia Facturada PH1', help="Potencia facturada en el periodo PH1. Sólo se rellenará para las tarifas con tres periodos horarios. En kW."),
      'pf_ph2': fields.integer('Potencia Facturada PH2', help="Potencia facturada en el periodo PH2. Sólo se rellenará para las tarifas con tres periodos horarios. En kW."),
      'pf_ph3': fields.integer('Potencia Facturada PH3', help="Potencia facturada en el periodo PH3. Sólo se rellenará para las tarifas con tres periodos horarios. En kW."),
      'e_ph1': fields.integer('Energía PH1', help="Energía facturada en el PH1. en kWh."),
      'e_ph2': fields.integer('Energía PH2', help="Energía facturada en el PH2. en kWh."),
      'e_ph3': fields.integer('Energía PH3', help="Energía facturada en el PH3. en kWh."),
      'e_ph4': fields.integer('Energía PH4', help="Energía facturada en el PH4. en kWh."),
      'e_ph5': fields.integer('Energía PH5', help="Energía facturada en el PH5. en kWh."),
      'e_ph6': fields.integer('Energía PH6', help="Energía facturada en el PH6. en kWh."),
      #'energia_facturada': fields.function(_energia_facturada, type="integer", string='Energía Facturada', method=True, help="Es la energía total facturada. Será el sumatorio de las energías de cada PH. En kWh."),
      'energia_facturada': fields.integer('Energía Facturada', help="Es la energía total facturada. Será el sumatorio de las energías de cada PH. En kWh."),
      'facturacio_potencia': fields.float('Facturación Potencia (€)', digits=(16,2), help="Importe facturado por Término de Potencia. En Euros con dos decimales."),
      'facturacio_energia': fields.float('Facturación Energía (€)', digits=(16,2), help="Importe facturado por Término de Energía. En Euros con dos decimales."),
      'facturacio_energia_r': fields.float('Facturación Energía Reactiva (€)', digits=(16,2), help="Importe facturado, en su caso, por este complemento tarifario. En Euros con dos decimales."),
      'facturacio_excessos_pot': fields.float('Facturación Excesos Potencia', digits=(16,2), help="Importe facturado, en su caso, por este complemento tarifario. En Euros con dos decimales."),
      'total_facturacio': fields.function(_total_facturacio, type='float', digits=(16,2), string="Total Facturación", method=True, help="Facturación bruta total. Sumatorio de los importes facturados por los términos anteriores. En Euros con dos decimales."),
      'company_id': fields.many2one('res.company', 'Empresa'),
      'status': fields.text('Status'),
      'errors': fields.integer('Errores'),
      'warnings': fields.integer('Avisos')

    }

    # Posem per defecte el valor de l'empresa de l'usuari
    # Posem per defecte tots els valors a 0
    _defaults = {
                  'company_id': lambda self,cr,uid,c: self.pool.get('res.users').browse(cr, uid, uid, c).company_id.id,
      'n_clients': lambda *a: 0,
      'pc_ph1': lambda *a: 0,
      'pc_ph2': lambda *a: 0,
      'pc_ph3': lambda *a: 0,
      'pc_ph4': lambda *a: 0,
      'pc_ph5': lambda *a: 0,
      'pc_ph6': lambda *a: 0,
      'pf_ph1': lambda *a: 0,
      'pf_ph2': lambda *a: 0,
      'pf_ph3': lambda *a: 0,
      'e_ph1': lambda *a: 0,
      'e_ph2': lambda *a: 0,
      'e_ph3': lambda *a: 0,
      'e_ph4': lambda *a: 0,
      'e_ph5': lambda *a: 0,
      'e_ph6': lambda *a: 0,
      'energia_facturada': lambda *a: 0,
      'facturacio_potencia': lambda *a: 0,
      'facturacio_energia': lambda *a: 0,
      'facturacio_energia_r': lambda *a: 0,
      'facturacio_excessos_pot': lambda *a: 0,
      'errors': lambda *a: 0,
      'warnings': lambda *a: 0,
    }

    _sql_constraints = [
        ('tarifa_uniq', 'unique (name, liquidacio_id)', 'No puede haber la misma tarifa más de una vez en una misma liquidación')
    ]

giscedata_liquidacio_fpd_tarifa()

# Models per liquidacions desgregades (Tarifes)

class giscedata_liquidacio_ftd(osv.osv):

    _name = 'giscedata.liquidacio.ftd'

    def check(self, cr, uid, ids, context={}):
        res = {}
        for ftd in self.browse(cr, uid, ids, context={}):
            message = ""
            warnings = 0
            errors = 0
            for t in ftd.tarifa:
                # ENERGIA FACTURADA
                if t.dh.codi in ('0','1'):
                    if t.energia_facturada == 0 or t.e_ph1 + t.e_ph2 + t.e_ph3 + t.e_ph4 + t.e_ph5 != 0:
                        message += "[A] La Energía Facturada debe estar rellenada y los campos Energía PH vacíos para el Codigo DH %s con tarifa %s\n" % (t.dh.name, t.name.name)
                        warnings += 1
                else:
                    if t.e_ph1 + t.e_ph2 + t.e_ph3 + t.e_ph4 + t.e_ph5 != t.energia_facturada:
                        message += "[A] La Energía Facturada: %i no coincide con la suma de Energías: %i. PH1: %i | PH2: %i | PH3: %i | PH4: %i | PH5: %i\n" % (t.energia_facturada, t.e_ph1 + t.e_ph2 + t.e_ph3 + t.e_ph4 + t.e_ph5, t.e_ph1, t.e_ph2, t.e_ph3, t.e_ph4, t.e_ph5)
                        warnings += 1
                # FACTURACION TERMINO ENERGIA
                # FACTURACION DH
                # ENERGIA POR EXCESOS DE CONSUMO
                if t.energia_exceso > abs(t.energia_facturada):
                    message += "[A] La energía por exceso de cosumo %i no puede ser mayor que el valor absoluto de la energía facturada." % (t.energia_exceso)
                    warnings += 1
                # NUMERO DE CLIENTES
                if not t.n_clients > 0:
                    message += "[A] El número de clientes debe ser mayor que 0."
                    warnings += 1
                t.write({'status': message, 'errors': errors, 'warnings': warnings})
        return True

    def _count(self, cr, uid, ids, field_name, *args):
        res = {}
        for t in self.browse(cr, uid, ids):
            res[t.id] = 1
        return res

    def xml(self, cr, uid, ids, context={}):
        from xml.dom import minidom
        res = {}
        for ftd in self.browse(cr, uid, ids, context):
            filename = 'FTDM_%s%s.%s.xml' % (ftd.year, ftd.month.zfill(2), ftd.company_id.codi_r1.zfill(4))
            doc = minidom.Document()
            if len(ftd.tarifa):
                di_fact = doc.createElement("DI_FACTURACION_TARIFA")
                di_fact.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance")
                di_fact.setAttribute("xsi:noNamespaceSchemaLocation", "FTDp_aaaamm.eeee.xsd")
                doc.appendChild(di_fact)
                for t in ftd.tarifa:
                    facturacion = doc.createElement("FACTURACION")
                    di_fact.appendChild(facturacion)

                    n = doc.createElement("DITA_CODIGO_EMPRESA")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(ftd.company_id.codi_r1 or '')
                    n.appendChild(txt)

                    n = doc.createElement(u"DITA_AÑO_FACTURACION")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(ftd.year)
                    n.appendChild(txt)

                    n = doc.createElement("DITA_MES_FACTURACION")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(ftd.month)
                    n.appendChild(txt)

                    n = doc.createElement("DITA_CODIGO_TARIFA")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(t.name.codi)
                    n.appendChild(txt)

                    n = doc.createElement("DITA_CODIGO_DH")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(t.dh.codi)
                    n.appendChild(txt)

                    n = doc.createElement("DITA_NUMERO_CLIENTES")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.n_clients))
                    n.appendChild(txt)

                    n = doc.createElement("DITA_POTENCIA_FACTURADA")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.p_fact))
                    n.appendChild(txt)

                    n = doc.createElement("DITA_ENERGIA_PH1")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.e_ph1))
                    n.appendChild(txt)

                    n = doc.createElement("DITA_ENERGIA_PH2")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.e_ph2))
                    n.appendChild(txt)

                    n = doc.createElement("DITA_ENERGIA_PH3")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.e_ph3))
                    n.appendChild(txt)

                    n = doc.createElement("DITA_ENERGIA_PH4")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.e_ph4))
                    n.appendChild(txt)

                    n = doc.createElement("DITA_ENERGIA_PH5")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.e_ph5))
                    n.appendChild(txt)

                    n = doc.createElement("DITA_ENERGIA_FACTURADA")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.energia_facturada))
                    n.appendChild(txt)

                    n = doc.createElement("DITA_ENERGIA_EXCESO_CONSUMO")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.energia_exceso))
                    n.appendChild(txt)

                    n = doc.createElement("DITA_FACTURACION_POTENCIA")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.facturacio_potencia))
                    n.appendChild(txt)

                    n = doc.createElement("DITA_FACTURACION_ENERGIA")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.facturacio_energia))
                    n.appendChild(txt)

                    n = doc.createElement("DITA_FACTURACION_REACTIVA")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.facturacio_energia_r))
                    n.appendChild(txt)

                    n = doc.createElement("DITA_FACTURACION_DH")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.facturacio_dh))
                    n.appendChild(txt)

                    n = doc.createElement("DITA_TOTAL_FACTURACION")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.total_facturacio))
                    n.appendChild(txt)

                res[filename] = doc.toxml("ISO-8859-1")
            else:
                res[filename] = ""

        return res

    def name_get(self, cr, uid, ids, context={}):
        if not len(ids):
            return []
        res = []
        for liq in self.browse(cr, uid, ids):
            name = '%s/%s' % (liq.month,liq.year)
            res.append((liq.id, name))
        return res

    _columns = {
      'company_id': fields.many2one('res.company', 'Empresa', readonly=True, ondelete='restrict'),
      'year': fields.char('Año', size=4, readonly=True),
      'month': fields.char('Mes', size=2, readonly=True),
      'inici': fields.date('Inicio'),
      'final': fields.date('Final'),
      'tarifa': fields.one2many('giscedata.liquidacio.ftd.tarifa', 'liquidacio_id', 'Tarifas', readonly=True, states={'borrador': [('readonly', False)], 'rectificacion':[('readonly', False)]}),
      'state': fields.selection([('borrador', 'Borrador'), ('para_liquidar', 'Para liquidar'), ('enviado', 'Enviada'), ('rectificacion', 'Rectificación'), ('corregido', 'Corregido'), ('cerrada', 'Cerrada')], 'Estado', readonly=True),
      'count': fields.function(_count, type='integer', method=True),
      'active': fields.boolean('Activa', readonly=True, states={'enviado': [('readonly', False)], 'cerrada': [('readonly', False)]}),
    }

    _sql_constraints = [
        ('year_month_uniq', 'unique (year, month, company_id)', 'No se puede repetir la llave (mes,año)')
    ]

    _defaults = {
                  'company_id': lambda self,cr,uid,c: self.pool.get('res.users').browse(cr, uid, uid, c).company_id.id,
      'active': lambda *a: 1,
      'month': lambda *a: datetime.now().strftime('%m'),
      'year': lambda *a: datetime.now().strftime('%Y'),
      'state': lambda *a: 'borrador',
    }

    def para_liquidar(self, cr, uid, ids, args={}):
        from osv import osv
        for id in ids:
            # Validació Errors
            self.check(cr, uid, [id])
            liq = self.browse(cr, uid, id)
            for tarifa in liq.tarifa:
                if tarifa.errors > 0:
                    raise osv.except_osv('Error', 'La liquidación no es correcta. Compruebe los errores que se describen por cada tarifa.\n ** Pulse el botón Validación para ver de nuevo los errores **\nERRORES: %i AVISOS:%i\n%s' % (tarifa.errors, tarifa.warnings, tarifa.status))
            if len(liq.tarifa):
                # Validació XML
                from lxml import etree
                import StringIO
                xsd_file = 'addons/giscedata_liquidacio/xsd/FTDp_aaaamm.eeee.xsd'
                xml = self.xml(cr, uid, [id])
                xml = xml[xml.keys()[0]]
                f = open(xsd_file, 'r')
                xmlschema = etree.XMLSchema(etree.parse(StringIO.StringIO(f.read())))
                doc = etree.parse(StringIO.StringIO(xml))
                if not xmlschema(doc):
                    raise osv.except_osv('Error', 'El fichero XML no es válido.')
            self.write(cr, uid, [id], {'state': 'para_liquidar'})

giscedata_liquidacio_ftd()

class giscedata_liquidacio_dh(osv.osv):

    _name = 'giscedata.liquidacio.dh'

    _columns = {
      'codi': fields.char('Código', size=2),
      'name': fields.char('Descripción', size=255),
    }

    _sql_constraints = [
        ('codi_uniq', 'unique (codi)', 'No se puede repetir el codigo de la DH')
    ]


giscedata_liquidacio_dh()


class giscedata_liquidacio_ftd_tarifa(osv.osv):

    _name = 'giscedata.liquidacio.ftd.tarifa'

    def _total_facturacio(self, cr, uid, ids, field_name, *args):
        res = {}
        for t in self.browse(cr, uid, ids):
            res[t.id] = t.facturacio_potencia + t.facturacio_energia + t.facturacio_energia_r + t.facturacio_dh
        return res

    def _energia_facturada(self, cr, uid, ids, *args):
        res = {}
        for t in self.browse(cr, uid, ids):
            res[t.id] = t.e_ph1 + t.e_ph2 + t.e_ph3 + t.e_ph4 + t.e_ph5
        return res

    def sumatori_energia_facturada(self, cr, uid, ids, *args):
        res = {}
        for t in self.browse(cr, uid, ids):
            ener = t.e_ph1 + t.e_ph2 + t.e_ph3 + t.e_ph4 + t.e_ph5
            self.write(cr, uid, [t.id], {'energia_facturada': ener})
        return True

    def change_ph(self, cr, uid, ids, ph1,ph2,ph3,ph4,ph5):
        return {'value': {'energia_facturada': ph1+ph2+ph3+ph4+ph5}}

    _columns = {
      'name': fields.many2one('giscedata.liquidacio.tarifes', 'Tarifa', ondelete='restrict', help='Código asignado a cada tarifa de suministro', domain=[('tipo', '=', 'T')]),
      'dh': fields.many2one('giscedata.liquidacio.dh', 'DH', ondelete='restrict', help='Código asignado a cada Tipo de Discriminación Horaria en las tarifas integrales'),
      'liquidacio_id': fields.many2one('giscedata.liquidacio.ftd', 'Liquidació', ondelete='cascade'),
      'n_clients': fields.integer('Número de clientes', help='Número de clientes facturados en el mes correspondientes a cada tarifa de suministro'),
      'p_fact': fields.integer('Poténcia Facturada', help='Concepto definido por la normativa que afecta a las tarifas integrales. En kW'),
      'e_ph1': fields.integer('Energía PH1', help='Energía facturada en el periodo 1. En kWh'),
      'e_ph2': fields.integer('Energía PH2', help='Energía facturada en el periodo 2. En kWh'),
      'e_ph3': fields.integer('Energía PH3', help='Energía facturada en el periodo 3. En kWh'),
      'e_ph4': fields.integer('Energía PH4', help='Energía facturada en el periodo 4. En kWh'),
      'e_ph5': fields.integer('Energía PH5', help='Energía facturada en el periodo 5. En kWh'),
      #'energia_facturada': fields.function(_energia_facturada, type="integer", string='Energía Facturada', method=True, help='Energía total facturada. Sumatorio de todos los periodos. En kWh'),
      'energia_facturada': fields.integer('Energía Facturada', help='Energía total facturada. Sumatorio de todos los periodos. En kWh'),
      'energia_exceso': fields.integer('Energía Exceso', help='Energía facturada por exceso de consumo'),
      'facturacio_potencia': fields.float('Facturación Potencia', digits=(16,2), help='Importe facturado por Términode Potencia. En Euros con dos decimales'),
      'facturacio_energia': fields.float('Facturación Energía', digits=(16,2), help='Importe facurado por Término de Energía. En Euros con dos decimales'),
      'facturacio_energia_r': fields.float('Facturación Energía Reactiva (€)', digits=(16,2), help='Importe facturado, en su caso, por este complemento tarifario. En Euros con dos decimales'),
      'facturacio_dh': fields.float('Facturación DH', digits=(16,2), help='Importe facturado, en su caso, por este complemento tarifario. En Euros con dos decimales'),
      'total_facturacio': fields.function(_total_facturacio, type='float', digits=(16,2), string="Total Facturación", method=True, help='Facturación bruta total. Suministro de los importes facturados por los términos anteriores. En Euros con dos deimales'),
      'company_id': fields.many2one('res.company', 'Empresa'),
      'status': fields.text('Status'),
      'errors': fields.integer('Errores'),
      'warnings': fields.integer('Avisos')
    }

    # Posem per defecte el valor de l'empresa de l'usuari
    # Posem per defecte tots els valors a 0
    _defaults = {
                  'company_id': lambda self,cr,uid,c: self.pool.get('res.users').browse(cr, uid, uid, c).company_id.id,
      'n_clients': lambda *a: 0,
      'p_fact': lambda *a: 0,
      'e_ph1': lambda *a: 0,
      'e_ph2': lambda *a: 0,
      'e_ph3': lambda *a: 0,
      'e_ph4': lambda *a: 0,
      'e_ph5': lambda *a: 0,
      'energia_exceso': lambda *a: 0.0,
      'energia_facturada': lambda *a: 0.0,
      'facturacio_potencia': lambda *a: 0.0,
      'facturacio_energia': lambda *a: 0.0,
      'facturacio_energia_r': lambda *a: 0.0,
      'facturacio_dh': lambda *a: 0.0,
      'errors': lambda *a: 0,
      'warnings': lambda *a: 0,
    }

    _sql_constraints = [
        ('tarifa_uniq', 'unique (name, dh, liquidacio_id)', 'No puede haber la misma tarifa y dh más de una vez en una misma liquidación')
    ]

giscedata_liquidacio_ftd_tarifa()


class giscedata_liquidacio_curs(osv.osv):

    _name = 'giscedata.liquidacio.curs'

    _columns = {
      'codi': fields.char('Código', size=4, required=True),
      'name': fields.char('Comercializadora', size=255, required=True),
    }

giscedata_liquidacio_curs()


class giscedata_liquidacio_rfur(osv.osv):

    _name = 'giscedata.liquidacio.rfur'

    def check(self, cr, uid, ids, context={}):
        res = {}
        for rfur in self.browse(cr, uid, ids, context={}):
            message = ""
            warnings = 0
            errors = 0
        return True

    def _count(self, cr, uid, ids, field_name, *args):
        res = {}
        for t in self.browse(cr, uid, ids):
            res[t.id] = 1
        return res

    def xml(self, cr, uid, ids, context={}):
        from xml.dom import minidom
        res = {}
        for rfur in self.browse(cr, uid, ids, context):
            filename = 'RFUR_%s%s.%s.xml' % (rfur.year, rfur.month.zfill(2), rfur.company_id.codi_r1.zfill(4))
            doc = minidom.Document()
            if len(rfur.cur):
                di_fact = doc.createElement("DI_FACTURACION_ITC1659")
                di_fact.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance")
                di_fact.setAttribute("xsi:noNamespaceSchemaLocation", "RFUR_aaaann.eeee.xsd")
                doc.appendChild(di_fact)
                for cur in rfur.cur:
                    facturacion = doc.createElement("FACTURACION")
                    di_fact.appendChild(facturacion)

                    n = doc.createElement("DIUR_CODIGO_EMPRESA")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(rfur.company_id.codi_r1)
                    n.appendChild(txt)

                    n = doc.createElement("DIUR_CODIGO_CUR")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(cur.name.codi)
                    n.appendChild(txt)

                    n = doc.createElement(u"DIUR_AÑO_ABONO")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(rfur.year)
                    n.appendChild(txt)

                    n = doc.createElement("DIUR_MES_ABONO")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(rfur.month)
                    n.appendChild(txt)

                    n = doc.createElement("DIUR_TOTAL_ABONO")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode('%.2f' % cur.total_abono)
                    n.appendChild(txt)

                res[filename] = doc.toxml("ISO-8859-1")
            else:
                res[filename] = ""
        return res

    def name_get(self, cr, uid, ids, context={}):
        if not len(ids):
            return []
        res = []
        for liq in self.browse(cr, uid, ids):
            name = '%s/%s' % (liq.month,liq.year)
            res.append((liq.id, name))
        return res

    _columns = {
      'company_id': fields.many2one('res.company', 'Empresa', readonly=True, ondelete='restrict'),
      'year': fields.char('Año', size=4, readonly=True),
      'month': fields.char('Mes', size=2, readonly=True),
      'inici': fields.date('Inicio'),
      'final': fields.date('Final'),
      'cur': fields.one2many('giscedata.liquidacio.rfur.cur', 'rfur_id', 'Resumen', readonly=True, states={'borrador': [('readonly', False)], 'rectificacion':[('readonly', False)]}),
      'state': fields.selection([('borrador', 'Borrador'), ('para_liquidar', 'Para liquidar'), ('enviado', 'Enviada'), ('rectificacion', 'Rectificación'), ('corregido', 'Corregido'), ('cerrada', 'Cerrada')], 'Estado', readonly=True),
      'count': fields.function(_count, type='integer', method=True),
      'active': fields.boolean('Activa', readonly=True, states={'enviado': [('readonly', False)], 'cerrada': [('readonly', False)]}),
    }

    _sql_constraints = [
        ('year_month_uniq', 'unique (year, month, company_id)', 'No se puede repetir la llave (mes,año)')
    ]

    _defaults = {
                  'company_id': lambda self,cr,uid,c: self.pool.get('res.users').browse(cr, uid, uid, c).company_id.id,
      'active': lambda *a: 1,
      'month': lambda *a: datetime.now().strftime('%m'),
      'year': lambda *a: datetime.now().strftime('%Y'),
      'state': lambda *a: 'borrador',
    }

    def para_liquidar(self, cr, uid, ids, args={}):
        from osv import osv
        for id in ids:
            self.check(cr, uid, [id])
            liq = self.browse(cr, uid, id)
            for cur in liq.cur:
                if cur.errors > 0:
                    raise osv.except_osv('Error', 'La liquidación no es correcta. Compruebe los errores que se describen por cada tarifa.\n ** Pulse el botón Validación para ver de nuevo los errores **\nERRORES: %i AVISOS:%i\n%s' % (cur.errors, cur.warnings, cur.status))
            if len(liq.cur):
                # Validació XML
                from lxml import etree
                import StringIO
                xsd_file = 'addons/giscedata_liquidacio/xsd/RFUR_aaaamm.eeee.xsd'
                xml = self.xml(cr, uid, [id])
                xml = xml[xml.keys()[0]]
                f = open(xsd_file, 'r')
                xmlschema = etree.XMLSchema(etree.parse(StringIO.StringIO(f.read())))
                doc = etree.parse(StringIO.StringIO(xml))
                if not xmlschema(doc):
                    raise osv.except_osv('Error', 'El fichero XML no es válido.')
            self.write(cr, uid, [id], {'state': 'para_liquidar'})

giscedata_liquidacio_rfur()

class giscedata_liquidacio_rfur_cur(osv.osv):

    _name = 'giscedata.liquidacio.rfur.cur'

    _columns = {
      'name': fields.many2one('giscedata.liquidacio.curs', 'Comercializadora', required=True),
      'total_abono': fields.float('Total Abono', digits=(16,2), help="Diferencia de ingresos obtenidos por los comercializadores en base al artículo 21 ó a la DT 4a de la Orden ITC 1659/2009 y los correspondientes a la tarifa de último recurso sin aplicación de la modalidad de discriminación horaria. En Euros con ds decimales.", required=True),
      'rfur_id': fields.many2one('giscedata.liquidacio.rfur', 'Liquidación', required=True, ondelete='cascade'),
      'company_id': fields.many2one('res.company', 'Empresa'),
      'status': fields.text('Status'),
      'errors': fields.integer('Errores'),
      'warnings': fields.integer('Avisos')
    }

    _defaults = {
                  'company_id': lambda self,cr,uid,c: self.pool.get('res.users').browse(cr, uid, uid, c).company_id.id,
      'errors': lambda *a: 0,
      'warnings': lambda *a: 0,
      'total_abono': lambda *a: 0.00,
    }

    _sql_constraints = [
        ('tarifa_uniq', 'unique (name, rfur_id)', 'No puede haber la misma comercializadora más de una vez en una misma liquidación')
    ]


giscedata_liquidacio_rfur_cur()

# Models per liquidacions agregades

class giscedata_liquidacio_fpa(osv.osv):

    _name = 'giscedata.liquidacio.fpa'

    def _count(self, cr, uid, ids, field_name, *args):
        res = {}
        for f in self.browse(cr, uid, ids):
            res[f.id] = 1
        return res

    def _mesos_string(self, cr, uid, ids, field_name, *args):
        res = {}
        for f in self.browse(cr, uid, ids):
            for m in f.mesos:
                if m.month.zfill(2) == datetime.now().strftime('%m'):
                    res[f.id] = m.month.zfill(2)
                    break
                else:
                    res[f.id] = '-1'
        return res

    _columns = {
      'company_id': fields.many2one('res.company', 'Empresa', readonly=True, ondelete='restrict'),
      'year': fields.char('Any', size=4, readonly=True),
      'month': fields.char('Mes', size=2, readonly=True),
      'mesos': fields.one2many('giscedata.liquidacio.fpa.mesos', 'liquidacio_id', 'Mesos', readonly=True, states={'borrador': [('readonly', False)]}),
      'state': fields.selection([('borrador', 'Esborrany'), ('para_liquidar', 'Para liquidar'), ('liquidada', 'Liquidada')], 'Estado', readonly=True),
      'count': fields.function(_count, type='integer', method=True),
      'mesos_string': fields.function(_mesos_string, type='char', size=2, method=True, string="Mes aplicable"),
      'active': fields.boolean('Activa', readonly=True, states={'liquidada': [('readonly', False)]}),
    }

    _defaults = {
                  'company_id': lambda self,cr,uid,c: self.pool.get('res.users').browse(cr, uid, uid, c).company_id.id,
      'active': lambda *a: 1,
      'month': lambda *a: datetime.now().strftime('%m'),
      'year': lambda *a: datetime.now().strftime('%Y'),
      'state': lambda *a: 'borrador',
    }


    _sql_constraints = [
        ('year_month_uniq', 'unique (year, month, company_id)', 'No se puede repetir la llave (mes,año)')
    ]


giscedata_liquidacio_fpa()

class giscedata_liquidacio_fpa_mesos(osv.osv):

    _name = 'giscedata.liquidacio.fpa.mesos'

    _columns = {
      'liquidacio_id': fields.many2one('giscedata.liquidacio.fpa', 'Liquidación', ondelete='cascade'),
      'month': fields.char('Mes', size=2, readonly=True),
      'ingressos': fields.float('Ingresos (€)', digits=(16,2)),
      'energia': fields.integer('Energía'),
    }

    # Per defecte posem valors a 0
    _defaults = {
      'ingressos': lambda *a: 0,
      'energia': lambda *a: 0,
    }

    _sql_constraints = [
        ('month_liquidacio_uniq', 'unique (month, liquidacio_id)', 'No puede haber el mismo mes más de una vez en la misma liquidación')
    ]

giscedata_liquidacio_fpa_mesos()


class giscedata_liquidacio_periodes_tarifaris(osv.osv):
    """ períodes tarifaris per model FPCC (3/2010 CNE"""
    _name = 'giscedata.liquidacio.periodes_tarifaris'

    _columns = {
        'codi': fields.char('Codi', size=3, readonly=True),
        'inici': fields.date('Data Inici'),
        'final': fields.date('Data Fi'),
        'active': fields.boolean('Actiu', required=True),
    }

    def name_get(self, cr, uid, ids, context={}):
        if not len(ids):
            return []
        res = []
        for pt_id in ids:
            vals = self.read(cr, uid, pt_id, ['inici', 'final'])
            res.append((pt_id, "De %s a %s" % (vals['inici'], vals['final'])))
        return res

    _defaults = {
        'active': lambda *a: 1,
    }

giscedata_liquidacio_periodes_tarifaris()


class giscedata_liquidacio_fpcc(osv.osv):
    """ Model per la generació del model de la CNE 3/2010 """

    _name = 'giscedata.liquidacio.fpcc'
    _rec_name = 'year'

    def _count(self, cr, uid, ids, field_name, *args):
        res = {}
        for t in self.browse(cr, uid, ids):
            res[t.id] = 1
        return res

    def xml(self, cr, uid, ids, context={}):
        from xml.dom import minidom
        res = {}
        for fpcc in self.browse(cr, uid, ids, context):
            filename = 'FPCC_%s12.%s.xml' % (
                fpcc.year,
                fpcc.company_id.codi_r1.zfill(4)
            )
            doc = minidom.Document()
            if len(fpcc.tarifa):
                di_fact = doc.createElement("DI_CONSUMOS_PEAJES")
                di_fact.setAttribute(
                    "xmlns:xsi",
                    "http://www.w3.org/2001/XMLSchema-instance")
                di_fact.setAttribute("xsi:noNamespaceSchemaLocation",
                                     "FPCC_aaaamm.eeee.xsd")
                doc.appendChild(di_fact)
                for t in fpcc.tarifa:
                    facturacion = doc.createElement("FACTURACION")
                    di_fact.appendChild(facturacion)

                    n = doc.createElement("DICC_CODIGO_EMPRESA")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(fpcc.company_id.codi_r1 or '')
                    n.appendChild(txt)

                    n = doc.createElement(u"DICC_AÑO_FACTURACION")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.any_fact))
                    n.appendChild(txt)

                    n = doc.createElement("DICC_MES_FACTURACION")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(t.mes_fact)
                    n.appendChild(txt)

                    n = doc.createElement(u"DICC_AÑO_CONSUMO")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.any_cons))
                    n.appendChild(txt)

                    n = doc.createElement("DICC_MES_CONSUMO")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(t.mes_cons)
                    n.appendChild(txt)

                    n = doc.createElement("DICC_PERIODO_TARIFARIO")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(t.periode_tarifari.codi)
                    n.appendChild(txt)

                    n = doc.createElement("DICC_CODIGO_TARIFA")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(t.name.codi)
                    n.appendChild(txt)

                    n = doc.createElement("DICC_NUMERO_CLIENTES")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.n_clients))
                    n.appendChild(txt)

                    n = doc.createElement("DICC_POTENCIA_CONTRATADA_PH1")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.pc_ph1))
                    n.appendChild(txt)

                    n = doc.createElement("DICC_POTENCIA_CONTRATADA_PH2")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.pc_ph2))
                    n.appendChild(txt)

                    n = doc.createElement("DICC_POTENCIA_CONTRATADA_PH3")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.pc_ph3))
                    n.appendChild(txt)

                    n = doc.createElement("DICC_POTENCIA_CONTRATADA_PH4")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.pc_ph4))
                    n.appendChild(txt)

                    n = doc.createElement("DICC_POTENCIA_CONTRATADA_PH5")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.pc_ph5))
                    n.appendChild(txt)

                    n = doc.createElement("DICC_POTENCIA_CONTRATADA_PH6")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.pc_ph6))
                    n.appendChild(txt)

                    n = doc.createElement("DICC_POTENCIA_A_FACTURAR_PH1")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.pf_ph1))
                    n.appendChild(txt)

                    n = doc.createElement("DICC_POTENCIA_A_FACTURAR_PH2")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.pf_ph2))
                    n.appendChild(txt)

                    n = doc.createElement("DICC_POTENCIA_A_FACTURAR_PH3")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.pf_ph3))
                    n.appendChild(txt)

                    n = doc.createElement("DICC_ENERGIA_PH1")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.e_ph1))
                    n.appendChild(txt)

                    n = doc.createElement("DICC_ENERGIA_PH2")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.e_ph2))
                    n.appendChild(txt)

                    n = doc.createElement("DICC_ENERGIA_PH3")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.e_ph3))
                    n.appendChild(txt)

                    n = doc.createElement("DICC_ENERGIA_PH4")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.e_ph4))
                    n.appendChild(txt)

                    n = doc.createElement("DICC_ENERGIA_PH5")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.e_ph5))
                    n.appendChild(txt)

                    n = doc.createElement("DICC_ENERGIA_PH6")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.e_ph6))
                    n.appendChild(txt)

                    n = doc.createElement("DICC_ENERGIA_FACTURADA")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.energia_facturada))
                    n.appendChild(txt)

                    n = doc.createElement("DICC_FACT_POTENCIA")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.facturacio_potencia))
                    n.appendChild(txt)

                    n = doc.createElement("DICC_FACT_ENERGIA")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.facturacio_energia))
                    n.appendChild(txt)

                    n = doc.createElement("DICC_FACT_ENERGIA_REACTIVA")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.facturacio_energia_r))
                    n.appendChild(txt)

                    n = doc.createElement("DICC_FACT_EXCESOS_POTENCIA")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.facturacio_excessos_pot))
                    n.appendChild(txt)

                    n = doc.createElement("DICC_TOTAL_FACTURACION")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.total_facturacio))
                    n.appendChild(txt)

                res[filename] = doc.toxml("ISO-8859-1")
            else:
                res[filename] = ""
        return res

    def para_liquidar(self, cr, uid, ids, args={}):
        from osv import osv
        for id in ids:
            # Validació Errors
            #self.check(cr, uid, [id])
            liq = self.browse(cr, uid, id)
            #for tarifa in liq.tarifa:
            #    if tarifa.errors > 0:
            #        raise osv.except_osv('Error',
            #    '''La liquidación no es correcta. Compruebe los errores que se
            #    describen por cada tarifa.\n\n ** Pulse el botón Validación
            #    para ver de nuevo los errores **\n\nERRORES: %i
            #    AVISOS:%i\n\n%s''' % (
            #    tarifa.errors, tarifa.warnings, tarifa.status))

            if len(liq.tarifa):
                # Validació XML
                from lxml import etree
                import StringIO
                xsd_file = \
                    'addons/giscedata_liquidacio/xsd/FPCC_aaaamm.eeee.xsd'
                xml = self.xml(cr, uid, [id])
                xml = xml[xml.keys()[0]]
                f = open(xsd_file, 'r')
                xmlschema = etree.XMLSchema(
                    etree.parse(StringIO.StringIO(f.read())))
                doc = etree.parse(StringIO.StringIO(xml))
                if not xmlschema(doc):
                    raise osv.except_osv('Error',
                                         'El fichero XML no es válido.')
            self.write(cr, uid, [id], {'state': 'para_liquidar'})

    _columns = {
        'company_id': fields.many2one('res.company', 'Empresa', readonly=True,
                                      ondelete='restrict'),
        'year': fields.char('Any', size=4, readonly=True),
        'inici': fields.date('Inicio'),
        'final': fields.date('Final'),
        'tarifa': fields.one2many('giscedata.liquidacio.fpcc.tarifa',
                                  'liquidacio_id', 'Tarifas', readonly=True,
                                  states={'borrador': [('readonly', False)],
                                  'rectificacion': [('readonly', False)]}),
        'state': fields.selection([('borrador', 'Esborrany'),
                                   ('para_liquidar', 'Para liquidar'),
                                   ('enviado', 'Enviado'),
                                   ('rectificacion', 'Rectificacion'),
                                   ('corregido', 'Corregido'),
                                   ('cerrada', 'Cerrada')],
                                  'Estado', readonly=True),
        'count': fields.function(_count, type='integer', method=True),
        'active': fields.boolean('Activa', readonly=True,
                                 states={'liquidada': [('readonly', False)]}),
    }

    _defaults = {
        'company_id': lambda self, cr, uid, c:
        self.pool.get('res.users').browse(cr, uid, uid, c).company_id.id,
        'active': lambda *a: 1,
        'state': lambda *a: 'borrador',
    }

    _sql_constraints = [('year_liquidacio_fpcc_uniq',
                         'unique(company_id,year)',
                         "No hi pot haver-hi el més d'una liquidació per any")
                        ]

giscedata_liquidacio_fpcc()


class giscedata_liquidacio_fpcc_tarifa(osv.osv):

    _name = 'giscedata.liquidacio.fpcc.tarifa'

    mesos = [('%02d' % x,) * 2 for x in xrange(1, 13)]

    def _total_facturacio(self, cr, uid, ids, field_name, *args):
        res = {}
        for t in self.browse(cr, uid, ids):
            res[t.id] = t.facturacio_potencia + t.facturacio_energia + \
                t.facturacio_energia_r + t.facturacio_excessos_pot
        return res

    def change_ph(self, cr, uid, ids, ph1, ph2, ph3, ph4, ph5, ph6):
        return {'value': {'energia_facturada':
                          ph1 + ph2 + ph3 + ph4 + ph5 + ph6}}

    def ff_mes_string(self, cr, uid, ids, field_name, *args):
        camps = {
            'm_facturacio': ('any_fact', 'mes_fact'),
            'm_consum': ('any_cons', 'mes_cons'),
        }
        res = {}

        camp = camps[field_name] or camps['m_consum']
        for mes in self.read(cr, uid, ids, list(camp)):
            res[mes['id']] = "%s/%02s" % (mes[camp[1]], mes[camp[0]])
        return res

    _columns = {
        'name': fields.many2one('giscedata.liquidacio.tarifes', 'Tarifa',
                                ondelete='restrict',
                                domain=[('tipo', '=', 'P')],
                                help="Código Asignado a cada tarifa de "
                                     "acceso"),
        'liquidacio_id': fields.many2one('giscedata.liquidacio.fpcc',
                                         'Liquidación', ondelete='cascade'),
        'any_fact': fields.integer('Any Facturació', size=4),
        'mes_fact': fields.selection(mesos, 'Mes Facturació'),
        'any_cons': fields.integer('Any Consum', size=4),
        'mes_cons': fields.selection(mesos, 'Mes Consum'),
        'm_facturacio': fields.function(ff_mes_string, string='Mes Facturacio',
                                        type='char', method=True),
        'm_consum': fields.function(ff_mes_string, string='Mes Consum',
                                    type='char', method=True),
        'periode_tarifari':
        fields.many2one('giscedata.liquidacio.periodes_tarifaris',
                        'Periode Tarifari', ondelete='restrict',
                        help="Periode tarifari on aplica"),
        'n_clients':
        fields.integer('Número de clientes',
                       help="Número de clientes facturados en el mes "
                            "correspondientes a cada tarifa de acceso"),
        'pc_ph1': fields.integer('Potencia Contractada PH1',
                                 help="Potencia contratada, en el PH1"),
        'pc_ph2': fields.integer('Potencia Contractada PH2',
                                 help="Potencia contratada, en el PH2"),
        'pc_ph3': fields.integer('Potencia Contractada PH3',
                                 help="Potencia contratada, en el PH3"),
        'pc_ph4': fields.integer('Potencia Contractada PH4',
                                 help="Potencia contratada, en el PH4"),
        'pc_ph5': fields.integer('Potencia Contractada PH5',
                                 help="Potencia contratada, en el PH5"),
        'pc_ph6': fields.integer('Potencia Contractada PH6',
                                 help="Potencia contratada, en el PH6"),
        'pf_ph1': fields.integer('Potencia Facturada PH1',
                                 help="Potencia facturada en el periodo PH1. "
                                      "Sólo se rellenará para las tarifas "
                                      "con tres periodos horarios. En kW."),
        'pf_ph2': fields.integer('Potencia Facturada PH2',
                                 help="Potencia facturada en el periodo PH2. "
                                      "Sólo se rellenará para las tarifas "
                                      "con tres periodos horarios. En kW."),
        'pf_ph3': fields.integer('Potencia Facturada PH3',
                                 help="Potencia facturada en el periodo PH3. "
                                      "Sólo se rellenará para las tarifas "
                                      "con tres periodos horarios. En kW."),
        'e_ph1': fields.integer('Energía PH1',
                                help="Energía facturada en el PH1. en kWh."),
        'e_ph2': fields.integer('Energía PH2',
                                help="Energía facturada en el PH2. en kWh."),
        'e_ph3': fields.integer('Energía PH3',
                                help="Energía facturada en el PH3. en kWh."),
        'e_ph4': fields.integer('Energía PH4',
                                help="Energía facturada en el PH4. en kWh."),
        'e_ph5': fields.integer('Energía PH5',
                                help="Energía facturada en el PH5. en kWh."),
        'e_ph6': fields.integer('Energía PH6',
                                help="Energía facturada en el PH6. en kWh."),
        'energia_facturada':
        fields.integer('Energía Facturada',
                       help="Es la energía total facturada. "
                            "Será el sumatorio de las energías de cada PH. "
                            "En kWh."),
        'facturacio_potencia':
        fields.float('Facturación Potencia (€)', digits=(16, 2),
                     help="Importe facturado por Término de Potencia. "
                          "En Euros con dos decimales."),
        'facturacio_energia':
        fields.float('Facturación Energía (€)', digits=(16, 2),
                     help="Importe facturado por Término de Energía. "
                          "En Euros con dos decimales."),
        'facturacio_energia_r':
        fields.float('Facturación Energía Reactiva (€)', digits=(16, 2),
                     help="Importe facturado, en su caso, por este "
                          "complemento tarifario. En Euros con dos "
                          "decimales."),
        'facturacio_excessos_pot':
        fields.float('Facturación Excesos Potencia', digits=(16, 2),
                     help="Importe facturado, en su caso, por este "
                          "complemento tarifario. En Euros con dos "
                          "decimales."),
        'total_facturacio':
        fields.function(_total_facturacio, type='float', digits=(16, 2),
                        string="Total Facturación", method=True,
                        help="Facturación bruta total. Sumatorio de los "
                             "importes facturados por los términos "
                             "anteriores. En Euros con dos decimales."),
        'company_id': fields.many2one('res.company', 'Empresa'),
        'status': fields.text('Status'),
        'errors': fields.integer('Errores'),
        'warnings': fields.integer('Avisos'),
    }

    # Posem per defecte el valor de l'empresa de l'usuari
    # Posem per defecte tots els valors a 0
    _defaults = {
        'company_id': lambda self, cr, uid, c:
        self.pool.get('res.users').browse(cr, uid, uid, c).company_id.id,
        'any_fact': lambda *a: int(datetime.now().strftime('%Y')),
        'any_cons': lambda *a: int(datetime.now().strftime('%Y')),
        'n_clients': lambda *a: 0,
        'pc_ph1': lambda *a: 0,
        'pc_ph2': lambda *a: 0,
        'pc_ph3': lambda *a: 0,
        'pc_ph4': lambda *a: 0,
        'pc_ph5': lambda *a: 0,
        'pc_ph6': lambda *a: 0,
        'pf_ph1': lambda *a: 0,
        'pf_ph2': lambda *a: 0,
        'pf_ph3': lambda *a: 0,
        'e_ph1': lambda *a: 0,
        'e_ph2': lambda *a: 0,
        'e_ph3': lambda *a: 0,
        'e_ph4': lambda *a: 0,
        'e_ph5': lambda *a: 0,
        'e_ph6': lambda *a: 0,
        'energia_facturada': lambda *a: 0,
        'facturacio_potencia': lambda *a: 0,
        'facturacio_energia': lambda *a: 0,
        'facturacio_energia_r': lambda *a: 0,
        'facturacio_excessos_pot': lambda *a: 0,
        'errors': lambda *a: 0,
        'warnings': lambda *a: 0,
    }

    _sql_constraints = [('tarifa_uniq',
                         'unique (name, liquidacio_id,'
                         'any_fact,mes_fact,any_cons,mes_cons,'
                         'periode_tarifari)',
                         'No se puede repetir la combinación '
                         'tarifa/ fecha consumo/fecha facturación/'
                         'per. tarifario más de una vez en una misma'
                         'liquidación')
                        ]

giscedata_liquidacio_fpcc_tarifa()


class GiscedataLiquidacioFPFR(osv.osv):
    _name = 'giscedata.liquidacio.fpfr'

    def name_get(self, cr, uid, ids, context={}):
        if not len(ids):
            return []
        res = []
        for liq in self.browse(cr, uid, ids):
            name = '%s/%s' % (liq.month, liq.year)
            res.append((liq.id, name))
        return res

    def name_search(self, cursor, uid, name, args=[], operator='ilike',
                    context=None, limit=80):
        """
        Redefine the name_search method to allow searching by code.
        """
        ids = []
        if name:
            cursor.execute("select id from giscedata_liquidacio_fpd "
                           "where month::varchar || '/' || year::varchar "
                           "ilike %s", ('%' + name + '%',))
            ids = [a[0] for a in cursor.fetchall()]
        if ids:
            args += [('id', 'in', ids)]
        ids = self.search(cursor, uid, args, limit=limit)
        return self.name_get(cursor, uid, ids, context=context)

    def _count(self, cr, uid, ids, field_name, *args):
        res = {}
        for t in self.browse(cr, uid, ids):
            res[t.id] = 1
        return res

    def para_liquidar(self, cr, uid, ids, context=None):
        for lid in ids:
            # Validació Errors
            liq = self.browse(cr, uid, lid)
            if len(liq.tarifa):
                # Validació XML
                from lxml import etree
                import StringIO
                xsd_file = 'addons/giscedata_liquidacio/xsd/FPFR_aaaamm.eeee.xsd'
                xml = self.xml(cr, uid, [lid])
                xml = xml[xml.keys()[0]]
                f = open(xsd_file, 'r')
                xmlschema = etree.XMLSchema(etree.parse(StringIO.StringIO(f.read())))
                doc = etree.parse(StringIO.StringIO(xml))
                if not xmlschema(doc):
                    raise osv.except_osv('Error',
                                         'El fichero XML no es válido.')
            self.write(cr, uid, [lid], {'state': 'para_liquidar'})

    def xml(self, cr, uid, ids, context={}):
        from xml.dom import minidom
        res = {}
        for liq in self.browse(cr, uid, ids, context):
            filename = 'FPFR_%s%s.%s.xml' % (
                liq.year, liq.month.zfill(2), liq.company_id.codi_r1.zfill(4)
            )
            doc = minidom.Document()
            if len(liq.tarifa):
                di_fact = doc.createElement("DI_FACTURACION_PEAJES_FRAUDE")
                di_fact.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance")
                di_fact.setAttribute("xsi:noNamespaceSchemaLocation", "FPFR_aaaann.eeee.xsd")
                doc.appendChild(di_fact)
                for t in liq.tarifa:
                    facturacion = doc.createElement("FACTURACION")
                    di_fact.appendChild(facturacion)

                    n = doc.createElement("DIFR_CODIGO_EMPRESA")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(liq.company_id.codi_r1 or '')
                    n.appendChild(txt)

                    n = doc.createElement(u"DIFR_AÑO_FACTURACION")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(liq.year)
                    n.appendChild(txt)

                    n = doc.createElement("DIFR_MES_FACTURACION")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(liq.month)
                    n.appendChild(txt)

                    n = doc.createElement("DIFR_CODIGO_TARIFA")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(t.name.codi)
                    n.appendChild(txt)

                    n = doc.createElement("DIFR_CODIGO_IDENTIFICADOR")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.identificador))
                    n.appendChild(txt)

                    n = doc.createElement("DIFR_TIPO_IDENTIFICADOR")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.tipo_identificador))
                    n.appendChild(txt)

                    n = doc.createElement("DIFR_POTENCIA_CONTRATADA_PH1")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.pc_ph1))
                    n.appendChild(txt)

                    n = doc.createElement("DIFR_POTENCIA_CONTRATADA_PH2")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.pc_ph2))
                    n.appendChild(txt)

                    n = doc.createElement("DIFR_POTENCIA_CONTRATADA_PH3")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.pc_ph3))
                    n.appendChild(txt)

                    n = doc.createElement("DIFR_POTENCIA_CONTRATADA_PH4")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.pc_ph4))
                    n.appendChild(txt)

                    n = doc.createElement("DIFR_POTENCIA_CONTRATADA_PH5")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.pc_ph5))
                    n.appendChild(txt)

                    n = doc.createElement("DIFR_POTENCIA_CONTRATADA_PH6")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.pc_ph6))
                    n.appendChild(txt)

                    n = doc.createElement("DIFR_POTENCIA_A_FACTURAR_PH1")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.pf_ph1))
                    n.appendChild(txt)

                    n = doc.createElement("DIFR_POTENCIA_A_FACTURAR_PH2")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.pf_ph2))
                    n.appendChild(txt)

                    n = doc.createElement("DIFR_POTENCIA_A_FACTURAR_PH3")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.pf_ph3))
                    n.appendChild(txt)

                    n = doc.createElement("DIFR_ENERGIA_PH1")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.e_ph1))
                    n.appendChild(txt)

                    n = doc.createElement("DIFR_ENERGIA_PH2")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.e_ph2))
                    n.appendChild(txt)

                    n = doc.createElement("DIFR_ENERGIA_PH3")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.e_ph3))
                    n.appendChild(txt)

                    n = doc.createElement("DIFR_ENERGIA_PH4")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.e_ph4))
                    n.appendChild(txt)

                    n = doc.createElement("DIFR_ENERGIA_PH5")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.e_ph5))
                    n.appendChild(txt)

                    n = doc.createElement("DIFR_ENERGIA_PH6")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.e_ph6))
                    n.appendChild(txt)

                    n = doc.createElement("DIFR_ENERGIA_FACTURADA")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.energia_facturada))
                    n.appendChild(txt)

                    n = doc.createElement("DIFR_FACT_POTENCIA")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.facturacio_potencia))
                    n.appendChild(txt)

                    n = doc.createElement("DIFR_FACT_ENERGIA")
                    facturacion.appendChild(n)
                    if t.name.codi.startswith('6'):
                        t.facturacio_energia = 0.0
                    txt = doc.createTextNode(str(t.facturacio_energia))
                    n.appendChild(txt)

                    n = doc.createElement("DIFR_FACT_ENERGIA_REACTIVA")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.facturacio_energia_r))
                    n.appendChild(txt)

                    n = doc.createElement("DIFR_FACT_EXCESOS_POTENCIA")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.facturacio_excessos_pot))
                    n.appendChild(txt)

                    n = doc.createElement("DIFR_TOTAL_FACTURACION")
                    facturacion.appendChild(n)
                    txt = doc.createTextNode(str(t.total_facturacio))
                    n.appendChild(txt)

                res[filename] = doc.toxml("ISO-8859-1")
            else:
                res[filename] = ""

        return res

    _columns = {
        'company_id': fields.many2one('res.company', 'Empresa', readonly=True,
                                      ondelete='restrict'),
        'year': fields.char('Año', size=4, readonly=True),
        'month': fields.char('Mes', size=2, readonly=True),
        'inici': fields.date('Inicio'),
        'final': fields.date('Final'),
        'tarifa': fields.one2many('giscedata.liquidacio.fpfr.tarifa',
                                  'liquidacio_id', 'Tarifas', readonly=True,
                                  states={'borrador': [('readonly', False)],
                                  'rectificacion': [('readonly', False)]}),
        'state': fields.selection([('borrador', 'Esborrany'),
                                   ('para_liquidar', 'Para liquidar'),
                                   ('enviado', 'Enviado'),
                                   ('rectificacion', 'Rectificacion'),
                                   ('corregido', 'Corregido'),
                                   ('cerrada', 'Cerrada')],
                                  'Estado', readonly=True),
        'count': fields.function(_count, type='integer', method=True),
        'active': fields.boolean('Activa', readonly=True,
                                 states={'liquidada': [('readonly', False)]}),
    }

    def default_get_company(self, cursor, uid, context=None):
        users_obj = self.pool.get('res.users')
        return users_obj.browse(cursor, uid, uid, context).company_id.id

    _defaults = {
        'company_id': default_get_company,
        'active': lambda *a: 1,
        'month': lambda *a: datetime.now().strftime('%m'),
        'year': lambda *a: datetime.now().strftime('%Y'),
        'state': lambda *a: 'borrador',
    }

    _order = 'year desc, month desc'

    _sql_constraints = [
        ('year_month_uniq',
         'unique (year, month, company_id)',
         'No se puede repetir la llave (mes, año)')
    ]

GiscedataLiquidacioFPFR()


class GiscedataLiquidacioFPFRTarifa(osv.osv):

    _name = 'giscedata.liquidacio.fpfr.tarifa'

    def _total_facturacio(self, cr, uid, ids, field_name, *args):
        res = {}
        for t in self.browse(cr, uid, ids):
            res[t.id] = (
                t.facturacio_excessos_pot
                + t.facturacio_potencia
                + t.facturacio_energia
                + t.facturacio_energia_r
            )
        return res

    def _energia_facturada(self, cr, uid, ids, field_name, *args):
        res = {}
        for t in self.browse(cr, uid, ids):
            res[t.id] = (
                t.e_ph1
                + t.e_ph2
                + t.e_ph3
                + t.e_ph4
                + t.e_ph5
                + t.e_ph6
            )
        return res

    def sumatori_energia_facturada(self, cr, uid, ids, *args):
        res = {}
        for t in self.browse(cr, uid, ids):
            ener = t.e_ph1 + t.e_ph2 + t.e_ph3 + t.e_ph4 + t.e_ph5 + t.e_ph6
            self.write(cr, uid, [t.id], {'energia_facturada': ener})
        return True

    def change_ph(self, cr, uid, ids, ph1, ph2, ph3, ph4, ph5, ph6):
        return {'value': {
            'energia_facturada': ph1 + ph2 + ph3 + ph4 + ph5 + ph6
        }}

    _columns = {
        'name': fields.many2one(
            'giscedata.liquidacio.tarifes',
            'Tarifa',
            ondelete='restrict',
            domain=[('tipo', '=', 'P')],
            help="Código Asignado a cada tarifa de acceso",
            required=True,
        ),
        'liquidacio_id': fields.many2one(
            'giscedata.liquidacio.fpfr',
            'Liquidación',
            ondelete='cascade',
            required=True,
        ),
        'tipo_identificador': fields.selection(
            [('1', 'Punto de suministro'),
             ('2', 'Persona física'),
             ('3', 'Persona jurídica')],
            'Código Identificador',
            help="Tipo de identificador: (1) Punto de suministro, "
                 "(2) persona física o (3) persona jurídica. En "
                 "función del código de identificador consignado",
            required=True,
        ),
        'identificador': fields.char(
            'Identificador', size=22,
            help="Identificación del defraudador. Se admitirá como "
                 "identificador único un CUPS, NIF o CIF.",
            required=True
        ),
        'pc_ph1': fields.integer(
            'Potencia Contractada PH1',
            help="Potencia contratada, en el PH1"
        ),
        'pc_ph2': fields.integer(
            'Potencia Contractada PH2',
            help="Potencia contratada, en el PH2"
        ),
        'pc_ph3': fields.integer(
            'Potencia Contractada PH3',
            help="Potencia contratada, en el PH3"
        ),
        'pc_ph4': fields.integer(
            'Potencia Contractada PH4',
            help="Potencia contratada, en el PH4"
        ),
        'pc_ph5': fields.integer(
            'Potencia Contractada PH5',
            help="Potencia contratada, en el PH5"
        ),
        'pc_ph6': fields.integer(
            'Potencia Contractada PH6',
            help="Potencia contratada, en el PH6"
        ),
        'pf_ph1': fields.integer(
            'Potencia Facturada PH1',
            help="Potencia facturada en el periodo PH1. Sólo se rellenará "
                 "para las tarifas con tres periodos horarios. En kW."
        ),
        'pf_ph2': fields.integer(
            'Potencia Facturada PH2',
            help="Potencia facturada en el periodo PH2. Sólo se rellenará "
                 "para las tarifas con tres periodos horarios. En kW."
        ),
        'pf_ph3': fields.integer(
            'Potencia Facturada PH3',
            help="Potencia facturada en el periodo PH3. Sólo se rellenará "
                 "para las tarifas con tres periodos horarios. En kW."
        ),
        'e_ph1': fields.integer(
            'Energía PH1',
            help="Energía facturada en el PH1. en kWh."
        ),
        'e_ph2': fields.integer(
            'Energía PH2',
            help="Energía facturada en el PH2. en kWh."
        ),
        'e_ph3': fields.integer(
            'Energía PH3',
            help="Energía facturada en el PH3. en kWh."
        ),
        'e_ph4': fields.integer(
            'Energía PH4',
            help="Energía facturada en el PH4. en kWh."
        ),
        'e_ph5': fields.integer(
            'Energía PH5',
            help="Energía facturada en el PH5. en kWh."
        ),
        'e_ph6': fields.integer(
            'Energía PH6',
            help="Energía facturada en el PH6. en kWh."
        ),
        'energia_facturada': fields.integer(
            'Energía Facturada',
            help="Es la energía total facturada. Será el sumatorio de las "
                 "energías de cada PH. En kWh."
        ),
        'facturacio_potencia': fields.float(
            'Facturación Potencia (€)',
            digits=(16, 2),
            help="Importe facturado por Término de Potencia. "
                 "En Euros con dos decimales."
        ),
        'facturacio_energia': fields.float(
            'Facturación Energía (€)',
            digits=(16, 2),
            help="Importe facturado por Término de Energía. "
                 "En Euros con dos decimales."
        ),
        'facturacio_energia_r': fields.float(
            'Facturación Energía Reactiva (€)',
            digits=(16, 2),
            help="Importe facturado, en su caso, por este complemento "
                 "tarifario. En Euros con dos decimales."
        ),
        'facturacio_excessos_pot': fields.float(
            'Facturación Excesos Potencia',
            digits=(16, 2),
            help="Importe facturado, en su caso, por este complemento "
                 "tarifario. En Euros con dos decimales."
        ),
        'total_facturacio': fields.function(
            _total_facturacio,
            type='float',
            digits=(16, 2),
            string="Total Facturación",
            method=True,
            help="Facturación bruta total. Sumatorio de los importes "
                 "facturados por los términos anteriores. En Euros "
                 "con dos decimales."
        ),
        'company_id': fields.many2one(
            'res.company',
            'Empresa'
        ),
    }

    def default_get_company(self, cursor, uid, context=None):
        users_obj = self.pool.get('res.users')
        return users_obj.browse(cursor, uid, uid, context).company_id.id

    _defaults = {
        'company_id': default_get_company,
        'pc_ph1': lambda *a: 0,
        'pc_ph2': lambda *a: 0,
        'pc_ph3': lambda *a: 0,
        'pc_ph4': lambda *a: 0,
        'pc_ph5': lambda *a: 0,
        'pc_ph6': lambda *a: 0,
        'pf_ph1': lambda *a: 0,
        'pf_ph2': lambda *a: 0,
        'pf_ph3': lambda *a: 0,
        'e_ph1': lambda *a: 0,
        'e_ph2': lambda *a: 0,
        'e_ph3': lambda *a: 0,
        'e_ph4': lambda *a: 0,
        'e_ph5': lambda *a: 0,
        'e_ph6': lambda *a: 0,
        'energia_facturada': lambda *a: 0,
        'facturacio_potencia': lambda *a: 0,
        'facturacio_energia': lambda *a: 0,
        'facturacio_energia_r': lambda *a: 0,
        'facturacio_excessos_pot': lambda *a: 0,
    }

    _sql_constraints = [
        ('tarifa_identificador_uniq', 'unique (name, liquidacio_id, identificador)', 'No puede haber la misma tarifa con el mismo identificador más de una vez en una misma liquidación')
    ]

GiscedataLiquidacioFPFRTarifa()
