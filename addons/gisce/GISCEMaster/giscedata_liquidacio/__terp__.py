# -*- coding: utf-8 -*-
{
    "name": "Liquidació d'Activitats Regulades",
    "description": """Genera els XML per enviar a la CNE sobre la facturació de tarifes""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Facturació",
    "depends":[
        "base",
        "base_extended"
    ],
    "init_xml":[
        "giscedata_liquidacio_data.xml"
    ],
    "demo_xml":[
        "giscedata_liquidacio_demo.xml"
    ],
    "update_xml":[
        "giscedata_liquidacio_security.xml",
        "giscedata_liquidacio_menu.xml",
        "giscedata_liquidacio_wizard.xml",
        "giscedata_liquidacio_view.xml",
        "company_view.xml",
        "giscedata_liquidacio_workflow.xml",
        "ir.model.access.csv",
        "giscedata_liquidacio_report.xml",
        "security/giscedata_liquidacio_security.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
