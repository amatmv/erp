# -*- coding: utf-8 -*-

import wizard
import pooler
from xml.dom import minidom
import base64
from addons import get_module_resource


_init_form = """<?xml version="1.0"?>
<form string="XML" width="600" height="600">
  <field name="name" colspan="4" nolabel="1" readonly="1"/>
  <group>
    <field name="xml" readonly="0" colspan="4" nolabel="1" width="600" height="550"/>
  </group>
  <field name="file" colspan="4" nolabel="1" readonly="1"/>
</form>"""

_init_fields = {
  'name': {'type': 'char', 'size': 50},
  'xml': {'type':'text'},
  'file': {'type': 'binary'}
}

def _xml(self, cr, uid, data, context={}):
    fpd_obj = pooler.get_pool(cr.dbname).get('giscedata.liquidacio.fpd')
    fpd = fpd_obj.browse(cr, uid, data['id'])
    filename = 'FPDM_%s%s.%s.xml' % (fpd.year, fpd.month.zfill(2), fpd.company_id.codi_r1.zfill(4))
    xml = fpd.xml()
    if len(fpd.tarifa):
        dom_str = minidom.parseString(xml[filename])
        xml_str = dom_str.toprettyxml()
    else:
        xml_str = xml[filename]

    # Aquestes dues variables les farem servir per guardar l'xml
    data['filename'] = filename
    data['xml'] = xml[filename]

    if len(fpd.tarifa):
        # Validació
        from lxml import etree
        import StringIO
        from osv import osv
        xsd_file = get_module_resource(
            'giscedata_liquidacio', 'xsd', 'FPDp_aaaamm.eeee.xsd'
        )
        f = open(xsd_file, 'r')
        xmlschema = etree.XMLSchema(etree.parse(StringIO.StringIO(f.read())))
        doc = etree.parse(StringIO.StringIO(xml[filename]))
        if not xmlschema(doc) and len(fpd.tarifa):
            raise osv.except_osv('Error', 'El fichero XML no es válido.')

    return {'name': filename, 'xml': xml_str, 'file': base64.b64encode(xml[filename])}


class wizard_giscedata_liquidacio_fpd_crear_xml(wizard.interface):

    states = {
      'init': {
        'actions': [_xml],
        'result': {'type': 'form', 'arch': _init_form, 'fields': _init_fields, 'state': [('end', 'Cerrar', 'gtk-close')]}

      },
    }

wizard_giscedata_liquidacio_fpd_crear_xml('giscedata.liquidacio.fpd.crear_xml')
