# -*- coding: utf-8 -*-

import wizard
import pooler
from datetime import datetime


def _init(self, cr, uid, data, context={}):
    cr.execute("select id,name from res_company")
    data['empreses'] = cr.dictfetchall()
    data['n_empreses'] = len(data['empreses'])
    data['contador'] = 0
    return {}


def _mes_empreses(self, cr, uid, data, context={}):
    if data['contador'] >= data['n_empreses']:
        return 'end'
    else:
        return 'operacio'


def _operacio(self, cr, uid, data, context={}):
    if 'year' in data['form']:
        year = data['form']['year']
    else:
        year = datetime.now().strftime('%Y')
    return {
        'company_id': data['empreses'][data['contador']]['id'],
        'year': year,
    }

_operacio_form = """<?xml version="1.0"?>
<form string="Creacion FPCCs">
  <field name="company_id" readonly="1" />
  <newline />
  <field name="year" />
</form>"""


_operacio_fields = {
    'company_id': {'string': 'Empresa', 'type': 'many2one',
                   'relation': 'res.company', 'required': True},
    'year': {'string': 'Año', 'type': 'char', 'size': 4, 'required': True},
}


def _seguent(self, cr, uid, data, context={}):
    data['contador'] += 1
    return {}


def _generar(self, cr, uid, data, context={}):
    empresa_obj = pooler.get_pool(cr.dbname).get('res.company')
    fpcc_obj = pooler.get_pool(cr.dbname).get('giscedata.liquidacio.fpcc')

    empresa = empresa_obj.browse(cr, uid, data['form']['company_id'])
    inici = '%s-01-16' % (data['form']['year'])
    final = '%d-01-15' % (int(data['form']['year'])+1)
    vals = {
        'inici': inici,
        'final': final,
        'company_id': empresa.id,
        'year': data['form']['year']
    }
    fpcc_obj.create(cr, uid, vals)
    data['contador'] += 1
    return {}


class wizard_giscedata_liquidacio_fpcc_crear(wizard.interface):

    states = {
        'init': {
            'actions': [_init],
            'result': {'type': 'state', 'state': 'mes_empreses'}
        },
        'mes_empreses': {
            'actions': [],
            'result': {'type': 'choice', 'next_state': _mes_empreses}
        },
        'operacio': {
            'actions': [_operacio],
            'result': {'type': 'form', 'arch': _operacio_form,
                       'fields': _operacio_fields,
                       'state': [('seguent', 'Siguiente',
                                  'gtk-go-forward'),
                                 ('generar', 'Generar i Siguiente',
                                  'gtk-save'),
                                 ('end', 'Cerrar',
                                  'gtk-close')
                                 ]
                       }
        },
        'generar': {
            'actions': [_generar],
            'result': {'type': 'state', 'state': 'mes_empreses'}
        },
        'seguent': {
            'actions': [_seguent],
            'result': {'type': 'state', 'state': 'mes_empreses'}
        },
        'end': {
            'actions': [],
            'result': {'type': 'state', 'state': 'end'}
        },

    }

wizard_giscedata_liquidacio_fpcc_crear('giscedata.liquidacio.fpcc.crear')
