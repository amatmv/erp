# -*- coding: utf-8 -*-

import wizard
import pooler
from xml.dom import minidom
import base64


def _init(self, cr, uid, data, context={}):
    company = pooler.get_pool(cr.dbname).get('res.users').browse(cr, uid, uid).company_id.name

    return {
      'company': company,
    }

_init_form = """<?xml version="1.0"?>
<form string="XML" col="4">
  <field name="company" colspan="4" width="300"/>
  <field name="year" />
</form>"""

_init_fields = {
  'company': {'string': 'Empresa', 'type': 'char', 'size': 256, 'readonly': True},
  'year': {'string': 'Año (AAAA)', 'type': 'char', 'size': 4, 'required': True},
}


_xml_form = """<?xml version="1.0"?>
<form string="XML" width="600" height="600">
  <field name="name" colspan="4" nolabel="1" readonly="1"/>
  <group>
    <field name="xml" readonly="0" colspan="4" nolabel="1" width="600" height="550"/>
  </group>
  <field name="file" colspan="4" nolabel="1" readonly="1"/>
</form>"""

_xml_fields = {
  'name': {'type': 'char', 'size': 50},
  'xml': {'type':'text'},
  'file': {'type': 'binary'}
}

def _xml(self, cr, uid, data, context={}):
    cid = pooler.get_pool(cr.dbname).get('res.users').browse(cr, uid, uid).company_id.id
    fpd_obj = pooler.get_pool(cr.dbname).get('giscedata.liquidacio.fpd')
    xml = fpd_obj.xml_anual(cr, uid, cid, data['form']['year'])
    if xml:
        filename = xml.keys()[0]
        dom_str = minidom.parseString(xml[filename])
        xml_str = dom_str.toprettyxml()
        # Validació
        from lxml import etree
        import StringIO
        from osv import osv
        xsd_file = 'addons/giscedata_liquidacio/xsd/FPDp_aaaamm.eeee.xsd'
        f = open(xsd_file, 'r')
        xmlschema = etree.XMLSchema(etree.parse(StringIO.StringIO(f.read())))
        doc = etree.parse(StringIO.StringIO(xml[filename]))
        if not xmlschema(doc) and len(fpd.tarifa):
            raise osv.except_osv('Error', 'El fichero XML no es válido.')
    else:
        xml_str = ""

    # Aquestes dues variables les farem servir per guardar l'xml
    data['filename'] = filename
    data['xml'] = xml[filename]

    return {'name': filename, 'xml': xml_str, 'file': base64.b64encode(xml[filename])}


class wizard_giscedata_liquidacio_fpda_crear_xml(wizard.interface):

    states = {
      'init': {
        'actions': [_init],
        'result': {'type': 'form', 'arch': _init_form, 'fields': _init_fields, 'state': [('xml', 'Generar')]}
      },
      'xml': {
        'actions': [_xml],
        'result': {'type': 'form', 'arch': _xml_form, 'fields': _xml_fields, 'state': [('end', 'Cerrar', 'gtk-close')]}

      },
    }

wizard_giscedata_liquidacio_fpda_crear_xml('giscedata.liquidacio.fpda.crear_xml')
