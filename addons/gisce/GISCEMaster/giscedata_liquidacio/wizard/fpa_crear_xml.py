# -*- coding: utf-8 -*-

import wizard
import pooler
from xml.dom import minidom

_init_form = """<?xml version="1.0"?>
<form string="XML" width="600" height="600">
  <group>
    <field name="xml" readonly="0" colspan="4" nolabel="1" width="600" height="550"/>
  </group>
</form>"""

_init_fields = {
  'xml': {'type':'text'},
}

def _xml(self, cr, uid, data, context={}):
    fpa_obj = pooler.get_pool(cr.dbname).get('giscedata.liquidacio.fpa')
    fpa = fpa_obj.browse(cr, uid, data['id'])
    doc = minidom.Document()
    di_fact = doc.createElement("DI_FACTURACION_PEAJES_AG")
    di_fact.setAttribute("xmlns:xsi", "http://www.w3c.org/2001/XMLSchema-instance")
    di_fact.setAttribute("xsi:noNamespaceSchemaLocation", "FPAp_aaaann.eeee.xsd")
    doc.appendChild(di_fact)
    for mes in fpa.mesos:
        facturacion = doc.createElement("FACTURACION")
        di_fact.appendChild(facturacion)

        n = doc.createElement("DIPE_CODIGO_EMPRESA")
        facturacion.appendChild(n)
        txt = doc.createTextNode(fpa.company_id.codi_r1 or '')
        n.appendChild(txt)

        n = doc.createElement(u"DIPE_AÑO_FACTURACION")
        facturacion.appendChild(n)
        txt = doc.createTextNode(str(fpa.year))
        n.appendChild(txt)

        n = doc.createElement("DIPE_MES_FACTURACION")
        facturacion.appendChild(n)
        txt = doc.createTextNode(str(mes.month))
        n.appendChild(txt)

        n = doc.createElement("DIPE_ENERGIA")
        facturacion.appendChild(n)
        txt = doc.createTextNode(str(mes.energia))
        n.appendChild(txt)

        n = doc.createElement("DIPE_FACTURACION")
        facturacion.appendChild(n)
        txt = doc.createTextNode(str(mes.ingressos))
        n.appendChild(txt)


    #print doc.toxml("ISO-8859-1")

    return {'xml': doc.toprettyxml()}


class wizard_giscedata_liquidacio_fpa_crear_xml(wizard.interface):

    states = {
      'init': {
        'actions': [_xml],
        'result': {'type': 'form', 'arch': _init_form, 'fields': _init_fields, 'state': [('end', 'Cerrar', 'gtk-close')]}

      },
    }

wizard_giscedata_liquidacio_fpa_crear_xml('giscedata.liquidacio.fpa.crear_xml')
