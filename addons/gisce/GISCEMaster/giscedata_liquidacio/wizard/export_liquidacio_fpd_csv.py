# -*- coding: utf-8 -*-
import base64
import csv
import StringIO
from tools.translate import _

import pooler
from osv import osv, fields

class GiscedataLiquidacioFpdExportarCsvWizard(osv.osv_memory):
    """Wizard per exportar liquidació en CSV"""
    _name = 'giscedata.liquidacio.fpd.exportar.csv.wizard'

    def _genera_csv(self, cursor, uid, context=None):
        """ Genera el CSV de la liquidació seleccionada"""
        if not context:
            context = {}

        liq_obj = self.pool.get('giscedata.liquidacio.fpd')
        tarifa_obj = self.pool.get('giscedata.liquidacio.fpd.tarifa')
        
        liquidacio = liq_obj.browse(cursor, uid, context['active_id'], context)
          
        lines = []
        tarifes_ids = [ x.id for x in liquidacio.tarifa ]
        title = [
            _(u'tarifa'),
            _(u'num. clients'),
            _(u'potcon_ph1'),
            _(u'potcon_ph2'),
            _(u'potcon_ph3'),
            _(u'potcon_ph4'),
            _(u'potcon_ph5'),
            _(u'potcon_ph6'),
            _(u'ene_ph1'),
            _(u'ene_ph2'),
            _(u'ene_ph3'),
            _(u'ene_ph4'),
            _(u'ene_ph5'),
            _(u'ene_ph6'),
            _(u'energia facturada'),
            _(u'potfact_ph1'),
            _(u'potfact_ph2'),
            _(u'potfact_ph3'),
            _(u'facturació potència'),
            _(u'facturació energia'),
            _(u'facturació energia reactiva'),
            _(u'facturació excessos potència'),
            _(u'total facturació'),
           ]


        for tid in tarifes_ids:
            tarifa =  tarifa_obj.browse(cursor, uid, tid, context)
            line = []
            line.append(tarifa.name.name)
            line.append(tarifa.n_clients)
            # Pot. contractada
            line.append(tarifa.pc_ph1)
            line.append(tarifa.pc_ph2)
            line.append(tarifa.pc_ph3)
            line.append(tarifa.pc_ph4)
            line.append(tarifa.pc_ph5)
            line.append(tarifa.pc_ph6)
            # Energia 
            line.append(tarifa.e_ph1)
            line.append(tarifa.e_ph2)
            line.append(tarifa.e_ph3)
            line.append(tarifa.e_ph4)
            line.append(tarifa.e_ph5)
            line.append(tarifa.e_ph6)
            line.append(tarifa.energia_facturada)
            # Pot. Facturada
            line.append(tarifa.pf_ph1)
            line.append(tarifa.pf_ph2)
            line.append(tarifa.pf_ph3)
            # Totals
            line.append(tarifa.facturacio_potencia)
            line.append(tarifa.facturacio_energia)
            line.append(tarifa.facturacio_energia_r)
            line.append(tarifa.facturacio_excessos_pot)
            line.append(round(tarifa.total_facturacio,2))

            lines.append(line)

        #lines = [['1','Tarifa 1'],['2','Tarifa 2']]

        output = StringIO.StringIO()
        writer = csv.writer(output, delimiter=';', quoting=csv.QUOTE_NONE)
        writer.writerow([ c.encode('utf-8') for c in title ])
        for line in lines:
            writer.writerow(line)
        file = base64.b64encode(output.getvalue())
        output.close()

        return file

    def _default_info(self, cursor, uid, context=None):
        if not context:
            context = {}
    
        liq_obj = self.pool.get('giscedata.liquidacio.fpd')

        liquidacio = liq_obj.read(cursor, uid, context['active_id'],
            ['year','month','company_id'],context)

        return _(u'CSV de la liquidació %s/%s de %s') % ( 
                    liquidacio['month'],
                    liquidacio['year'],
                    liquidacio['company_id'][1])

    def _default_filename(self, cursor, uid, context=None):
        if not context:
            context = {}
    
        liq_obj = self.pool.get('giscedata.liquidacio.fpd')
        company_obj = self.pool.get('res.company')
        partner_obj = self.pool.get('res.partner')

        liquidacio = liq_obj.read(cursor, uid, context['active_id'],
            ['year','month','company_id'],context)

        any = liquidacio['year']
        mes = liquidacio['month']
        
        company = company_obj.read(cursor, uid, liquidacio['company_id'][0],
            ['partner_id'],context)

        partner = partner_obj.read(cursor, uid, company['partner_id'][0], ['ref'])
        ref = partner['ref'] or '0000'

        return _('liquidacio_%s_%s%s.csv') % (ref,any,mes)
    
    _columns = {
        'file': fields.binary('Liquidacio CSV'),
        'name': fields.char('Nom',size=128),
        'state': fields.char('State',size=16),
        'info': fields.text('Info'),
    }

    _defaults = {
        'state': lambda *x: 'init',
        'info': _default_info,
        'file': _genera_csv,
        'name': _default_filename,
    }

GiscedataLiquidacioFpdExportarCsvWizard()
