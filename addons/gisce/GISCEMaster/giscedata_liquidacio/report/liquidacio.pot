# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2010-12-21 03:35+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"

msgid "Potència Contractada PH3: "
msgstr ""

msgid "Tarifa: {0}"
msgstr ""

msgid "Potència Facturada PH3: "
msgstr ""

msgid "Facturació Total: "
msgstr ""

msgid "Energies"
msgstr ""

msgid "Energia PH5: "
msgstr ""

msgid "Energia PH1: "
msgstr ""

msgid "Potències Contractades"
msgstr ""

msgid "Facturació Energía Reactiva: "
msgstr ""

msgid "Número de clients: {0}"
msgstr ""

msgid "Facturació Excessos Potència: "
msgstr ""

msgid "Potència Contractada PH2: "
msgstr ""

msgid "Potències Facturades"
msgstr ""

msgid "Potència Contractada PH4: "
msgstr ""

msgid "Liquidacions {0} del {1}"
msgstr ""

msgid "Potència Contractada PH6: "
msgstr ""

msgid "Facturació"
msgstr ""

msgid "Energia PH3: "
msgstr ""

msgid "Facturació Potència: "
msgstr ""

msgid "Energia PH4: "
msgstr ""

msgid "Energia Facturada: "
msgstr ""

msgid "Potència Facturada PH1: "
msgstr ""

msgid "Potència Contractada PH1: "
msgstr ""

msgid "Energia PH6: "
msgstr ""

msgid "Facturació Energía: "
msgstr ""

msgid "Potència Facturada PH2: "
msgstr ""

msgid "Energia PH2: "
msgstr ""

msgid "Potència Contractada PH5: "
msgstr ""

