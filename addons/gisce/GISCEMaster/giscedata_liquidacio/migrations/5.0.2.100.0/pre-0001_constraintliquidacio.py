# -*- coding: utf-8 -*-

import netsvc


def up(cursor, installed_version):
    logger = netsvc.Logger()
    logger.notifyChannel(
        'migrations', netsvc.LOG_INFO,
        "Eliminant la constraint de 'tarifa_uniq'."
    )

    query = """ALTER TABLE giscedata_liquidacio_fpfr_tarifa 
               DROP CONSTRAINT giscedata_liquidacio_fpfr_tarifa_tarifa_uniq;"""

    cursor.execute(query)
    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         "Realitzat correctament")


def down(cursor, installed_version):
    logger = netsvc.Logger()
    logger.notifyChannel(
        'migrations', netsvc.LOG_INFO,
        "Eliminant la constraint 'tarifa_identificador_uniq', i creant de nou 'tarifa_uniq'."
    )

    query_drop = """ALTER TABLE giscedata_liquidacio_fpfr_tarifa 
                    DROP CONSTRAINT giscedata_liquidacio_fpfr_tarifa_tarifa_identificador_uniq;"""

    query_add = """ALTER TABLE giscedata_liquidacio_fpfr_tarifa 
                   ADD CONSTRAINT giscedata_liquidacio_fpfr_tarifa_tarifa_uniq 
                   UNIQUE (name, liquidacio_id);"""

    cursor.execute(query_drop)
    cursor.execute(query_add)
    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         "Realitzat correctament")

migrate = up