# -*- coding: utf-8 -*-

import netsvc
import pooler

def migrate(cursor, installed_version):
    logger = netsvc.Logger()
    logger.notifyChannel('migrations', netsvc.LOG_INFO,
                         "Borrant la constraint de liquidacio_id, ja que no "
                         "apuntava a la taula correcte")

    cursor.execute("""alter table giscedata_liquidacio_fpfr_tarifa
                      drop constraint
                      giscedata_liquidacio_fpfr_tarifa_liquidacio_id_fkey;""")

    logger.notifyChannel('migration', netsvc.LOG_INFO,
                            u"Realitzat correctament")