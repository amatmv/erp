# -*- coding: utf-8 -*-

import netsvc
import pooler

def migrate(cursor, installed_version):
    logger = netsvc.Logger()
    logger.notifyChannel('migrations', netsvc.LOG_INFO,
                         "Afegint la constraint de liquidacio_id correcte")

    cursor.execute("""alter table giscedata_liquidacio_fpfr_tarifa
                      add constraint
                      giscedata_liquidacio_fpfr_tarifa_liquidacio_id_fkey
                      foreign key (liquidacio_id)
                      references giscedata_liquidacio_fpfr
                      on delete cascade
                      ;""")

    logger.notifyChannel('migration', netsvc.LOG_INFO,
                            u"Realitzat correctament")
