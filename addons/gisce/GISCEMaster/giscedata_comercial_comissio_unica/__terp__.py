# -*- coding: utf-8 -*-
{
    "name": "Modul Comercial: Comissions Úniques'One Shoot'",
    "description": """
    Afageix la comissió de modalitat 'one shoot': es paguen X € al comercial per cada contracte nou.
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_comercial_comissio",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_comercial_view.xml",
        "giscedata_comercial_comissio_unica_data.xml",
        "security/ir.model.access.csv",
    ],
    "active": False,
    "installable": True
}
