# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from osv import osv
import json


class TestComercials(testing.OOTestCase):

    def setUp(self):
        self.openerp.install_module("giscedata_tarifas_peajes_20160101")
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user
        self.pool = self.openerp.pool
        self.comercial_obj = self.pool.get("hr.employee")
        self.polissa_obj = self.pool.get("giscedata.polissa")
        self.comissio_unica_obj = self.pool.get("giscedata.comercial.comissio.unica")
        self.pol_comissio_unica_obj = self.pool.get("giscedata.polissa.comissio.unica")
        self.imd_obj = self.pool.get('ir.model.data')
        self.comercial_id = self.imd_obj.get_object_reference(self.cursor, self.uid, 'hr', 'employee1')[1]
        self.polissa_id = self.imd_obj.get_object_reference(self.cursor, self.uid, 'giscedata_polissa', 'polissa_0001')[1]
        self.polissa2_id = self.imd_obj.get_object_reference(self.cursor, self.uid, 'giscedata_polissa', 'polissa_0002')[1]
        self.polissa3_id = self.imd_obj.get_object_reference(self.cursor, self.uid, 'giscedata_polissa', 'polissa_0003')[1]
        # Configurar llista preus
        self.list_id = self.pool.get("ir.model.data").get_object_reference(
            self.cursor, self.uid, "giscedata_facturacio", "pricelist_tarifas_electricidad"
        )[1]
        versio_id = self.pool.get("ir.model.data").get_object_reference(
            self.cursor, self.uid, "giscedata_tarifas_peajes_20160101", "boe_302_2015"
        )[1]
        self.pool.get("product.pricelist.version").write(self.cursor, self.uid, versio_id, {'date_end': False})
        product_o = self.pool.get("product.product")
        product_id = self.imd_obj.get_object_reference(self.cursor, self.uid, "giscedata_comercial_comissio_unica", "product_comissio_unica")[1]
        product_o.write(self.cursor, self.uid, product_id, {'standard_price': 1.5})
        for pol_id in [self.polissa_id, self.polissa2_id, self.polissa3_id]:
            pol = self.polissa_obj.browse(self.cursor, self.uid, pol_id)
            pol.write({"llista_preu": self.list_id})
            pol.send_signal(['validar', 'contracte'])
            pol.modcontractual_activa.write({'data_final': '3000-01-01'})

    def tearDown(self):
        self.txn.stop()

    def test_facturar_comissio_unica(self):
        # Creem una comissio de 10 i l'asignem, la cambiem a 15 i l'asisgnem a un altre contracte
        exp_obj = self.pool.get("hr.expense.expense")
        comissio_id = self.comissio_unica_obj.create(self.cursor, self.uid, {})
        self.comercial_obj.write(self.cursor, self.uid, self.comercial_id, {'comissio': "giscedata.comercial.comissio.unica, {0}".format(comissio_id)})
        self.polissa_obj.action_asignar_comissio_from_comercial(self.cursor, self.uid, self.polissa_id)
        self.polissa_obj.action_asignar_comissio_from_comercial(self.cursor, self.uid, self.polissa2_id)
        self.polissa_obj.action_asignar_comissio_from_comercial(self.cursor, self.uid, self.polissa3_id)
        cpid = int(self.polissa_obj.read(self.cursor, self.uid, self.polissa3_id, ['comissio'])['comissio'].split(",")[1])
        self.pol_comissio_unica_obj.write(self.cursor, self.uid, cpid, {'pagada': True})
        # Facturem els contractes 1,2 i 3. S'ha de generar una ordre amb 2 linies i donar un missatge de avis per el tercer contracte
        wiz_obj = self.pool.get("wizard.facturar.comissio.from.polissa")
        context = {'active_ids': [self.polissa_id, self.polissa2_id, self.polissa3_id]}
        wiz_id = wiz_obj.create(self.cursor, self.uid, {}, context=context)
        wiz_obj.action_facturar_comissio_from_polissa(self.cursor, self.uid, wiz_id, context=context)
        exp_ids = wiz_obj.read(self.cursor, self.uid, wiz_id, ['ordres_ids'])[0]['ordres_ids']
        exp_ids = json.loads(exp_ids or "{}")
        exp_ids = list(set(exp_ids))
        self.assertEqual(len(exp_ids), 1)
        exp = exp_obj.browse(self.cursor, self.uid, exp_ids[0])
        self.assertEqual(exp.amount, 3.0)
        self.assertEqual(len(exp.line_ids), 2)
