# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _
from datetime import datetime


class GiscedataComercialComissioUnica(osv.osv):
    _name = 'giscedata.comercial.comissio.unica'
    _inherit = 'giscedata.comercial.comissio'
    _description = u"Comissió Única"

    def generar_descripcio(self, cursor, uid, cid, context=None):
        if not cid:
            return ""
        return _(u"Comissio per cada contracte nou")

    def get_comissio_polissa_vals(self, cursor, uid, comissio_id, context=None):
        vals = self.read(cursor, uid, comissio_id, ['comercial_id'], context=context)
        if vals.get('comercial_id'):
            vals['comercial_id'] = vals.get('comercial_id')[0]
        del vals['id']
        return vals

GiscedataComercialComissioUnica()


class GiscedataPolissaComissioUnica(osv.osv):
    _name = 'giscedata.polissa.comissio.unica'
    _inherit = 'giscedata.polissa.comissio'
    _description = u"Comissió Única"

    def generar_descripcio(self, cursor, uid, cid, context=None):
        if not cid:
            return ""
        vals = self.read(cursor, uid, cid, ['pagada', 'polissa_id'], context=context)
        estat = "Pagada" if vals['pagada'] else "No pagada"
        return _(u"Comissio per contracte nou ({0}) - {1}").format(vals['polissa_id'][1], estat)

    def check_cobrar_comissio(self, cursor, uid, comissio_polissa_id, context=None):
        polinfo = self.read(cursor, uid, comissio_polissa_id, ['polissa_id', 'pagada'])
        if polinfo['pagada']:
            return False, _(u"* Contracte {0}: la comissio per aquest contracte ja estava pagada.\n").format(polinfo['polissa_id'][1])
        else:
            return True, ""

    def calc_linia_expense_vals(self, cursor, uid, comissio_polissa_id, context=None):
        if context is None:
            context = {}
        pricelist_obj = self.pool.get('product.pricelist')
        polissa_o = self.pool.get('giscedata.polissa')
        ctx = context.copy()
        ctx['date'] = datetime.today().strftime("%Y-%m-%d")
        product_id = self.pool.get("ir.model.data").get_object_reference(
            cursor, uid, 'giscedata_comercial_comissio_unica', 'product_comissio_unica'
        )[1]
        pol_id = self.read(cursor, uid, comissio_polissa_id, ['polissa_id'])['polissa_id'][0]
        llista_id = polissa_o.read(cursor, uid, pol_id, ['llista_preu'], context=ctx)['llista_preu'][0]
        price = pricelist_obj.price_get(
            cursor, uid, [llista_id], product_id, 1.0, context=ctx
        )[llista_id]
        com_data = self.read(cursor, uid, comissio_polissa_id, ['name'])
        vals = [{
            'product_id': product_id,
            'unit_amount': price,
            'unit_quantity': 1,
            'name': com_data['name'].split("-")[0]
        }]
        self.write(cursor, uid, comissio_polissa_id, {'pagada': True})
        return vals

    _columns = {
        'pagada': fields.boolean(u"Pagada al comercial"),
    }


GiscedataPolissaComissioUnica()
