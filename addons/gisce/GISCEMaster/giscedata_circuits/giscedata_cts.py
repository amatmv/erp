# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscedataCts(osv.osv):

    _name = 'giscedata.cts'
    _inherit = 'giscedata.cts'

    _columns = {
      'circuit': fields.many2one('giscedata.cts.subestacions.posicio', 'Circuit'),
    }

GiscedataCts()
