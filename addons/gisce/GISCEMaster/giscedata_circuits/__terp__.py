# -*- coding: utf-8 -*-
{
    "name": "Giscedata Circuits",
    "description": """
    This module provide :
      * Mòdul de circuits per cts i ats.
      * Exten els models afegint un camp circuit.
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_cts",
        "giscedata_at",
        "giscedata_cts_subestacions"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_at_view.xml",
        "giscedata_cts_view.xml"
    ],
    "active": False,
    "installable": True
}
