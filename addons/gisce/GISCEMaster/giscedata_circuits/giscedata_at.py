# -*- coding: utf-8 -*-
from osv import osv, fields

class GiscedataAtTram(osv.osv):

    _name = 'giscedata.at.tram'
    _inherit = 'giscedata.at.tram'

    _columns = {
      'circuit': fields.many2one('giscedata.cts.subestacions.posicio', 'Circuit'),
    }

GiscedataAtTram()
