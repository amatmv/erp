# -*- coding: utf-8 -*-
from osv import osv, fields

# Aixó ha de desapareixer i s'ha de migrar en el moment d'afegir els passamurs
RELATED_MODELS = [
    ('giscedata.cts', 'CTS'),
    ('giscedata.at.suport', 'Suports AT')
]


class GiscedataAtVano(osv.osv):
    def _get_origen_vanos(self, cursor, uid, ids, field_name, args, context):
        """
        Get the code of the Origen elements
        :param cursor: Database Cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :param ids: IDs of the elements to compute the field value
        :type ids: list of int
        :param field_name: Not Used
        :type field_name: Not Used
        :param args: Not Used
        :type args: Not Used
        :param context: Not Used
        :type context: Not Used
        :return: Relation between the IDs of the elements and the field value
        :rtype: dict[int,str or bool]
        """

        res = {}
        cts_obj = self.pool.get("giscedata.cts")
        sup_obj = self.pool.get("giscedata.at.suport")

        for vano_id in ids:
            tmp = self.read(
                cursor, uid, vano_id, ['suport_origen']
            )['suport_origen']
            if tmp:
                model, model_id = tmp.split(',')
                if model == "giscedata.cts":
                    origen = cts_obj.read(
                        cursor, uid, int(model_id), ['name']
                    )['name']
                elif tmp.split(',')[0] == "giscedata.at.suport":
                    origen = sup_obj.read(
                        cursor, uid, int(model_id), ['name']
                    )['name']
                else:
                    origen = False
            else:
                origen = False
            res[vano_id] = origen

        return res

    def _get_final_vanos(self, cursor, uid, ids, field_name, args, context):
        """
        Get the code of the Final elements
        :param cursor: Database Cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :param ids: IDs of the elements to compute the field value
        :type ids: list of int
        :param field_name: Not Used
        :type field_name: Not Used
        :param args: Not Used
        :type args: Not Used
        :param context: Not Used
        :type context: Not Used
        :return: Relation between the IDs of the elements and the field value
        :rtype: dict[int,str or bool]
        """

        res = {}
        cts_obj = self.pool.get("giscedata.cts")
        sup_obj = self.pool.get("giscedata.at.suport")

        for vano_id in ids:
            tmp = self.read(
                cursor, uid, vano_id, ['suport_final']
            )['suport_final']
            if tmp:
                model, model_id = tmp.split(',')
                if model == "giscedata.cts":
                    final = cts_obj.read(
                        cursor, uid, int(model_id), ['name']
                    )['name']
                elif tmp.split(',')[0] == "giscedata.at.suport":
                    final = sup_obj.read(
                        cursor, uid, int(model_id), ['name']
                    )['name']
                else:
                    final = False
            else:
                final = False
            res[vano_id] = final

        return res

    def _get_vanos_ids(self, cursor, uid, ids, context=None):
        """
        Get the IDs of the elements wich the field must be updated
        :param cursor: Not Used
        :type cursor: Not Used
        :param uid: Not Used
        :type uid: Not Used
        :param ids: Not Used
        :type ids: Not Used
        :param context: Not Used
        :type context: Not Used
        :return: The IDS of the elements wich the fields must be updated
        :rtype: list of int
        """
        return ids

    _name = 'giscedata.at.vano'
    _description = 'Vanos AT'

    _columns = {
        'name': fields.char('Codi', size=256, required=True, select=1),
        'origen': fields.function(
            _get_origen_vanos, method=True, store={
                'giscedata.at.vano': (_get_vanos_ids, ['suport_origen'], 10)
            },
            type='char',
            string='Codi origen',
            size=256,
            select=1
        ),
        'final': fields.function(
            _get_final_vanos, method=True, store={
                'giscedata.at.vano': (_get_vanos_ids, ['suport_final'], 10)
            },
            type='char',
            string='Codi origen',
            size=256,
            select=1
        ),
        'suport_origen': fields.reference(
            'Element origen', RELATED_MODELS, size=256,
        ),
        'suport_final': fields.reference(
            'Element final', RELATED_MODELS, size=256,
        ),
        'tram_id': fields.many2one('giscedata.at.tram', 'Tram', select=1),
        'longitud': fields.float('Longitud'),
    }


GiscedataAtVano()
