# -*- coding: utf-8 -*-
{
    "name": "GISCEData AT Vano",
    "description": """Modul que implementa els Vanos d'AT""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Alta Tensió",
    "depends":[
        "base_extended",
        "base_extended_distri",
        "giscedata_at",
        "giscedata_cts"
    ],
    "init_xml":[],
    "demo_xml":[],
    "update_xml":[
        "security/ir.model.access.csv",
        "giscedata_at_vano_view.xml"
    ],
    "active": False,
    "installable": True
}
