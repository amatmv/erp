# -*- coding: utf-8 -*-

from osv import osv, fields


class GiscedataPolissaCondicions(osv.osv):

    _name = 'giscedata.polissa.condicions'

    _columns = {
        'name': fields.char('Nom', size=64, required=True),
        'text': fields.text('Descripció', required=True)
    }

GiscedataPolissaCondicions()


class GiscedataPolissaAddCondicions(osv.osv):

    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def onchange_condicions(self, cursor, uid, ids,
                            condicio_ids, text, context=None):

        res = {'value': {}, 'domain': {}, 'warning': {}}
        condicio_obj = self.pool.get('giscedata.polissa.condicions')
        condicio_ids = condicio_ids and condicio_ids[0][2] or []

        text = text or ''
        for condicio in condicio_obj.browse(cursor, uid, condicio_ids):
            if text:
                text += '\n\n%s' % condicio.text
            else:
                text = condicio.text
        res['value'].update({'text_condicions': text})

        return res

    _columns = {
        'condicions_especials': fields.boolean('Condicions especials',
                    help=(u"Marcar aquesta opció per habilitar la "
                          u"possibilitat d'incloure condicions "
                          u"especials al contracte"),
                    readonly=True,
                    states={
                        'esborrany':[('readonly', False)],
                        'validar': [('readonly', False)],
                        'modcontractual': [('readonly', False)]
                    }),
        'condicio_ids': fields.many2many('giscedata.polissa.condicions',
                            'condicions_polissa_rel',
                            'polissa_id', 'condicio_id',
                            string="Condicions",
                            readonly=True,
                            states={
                                'esborrany':[('readonly', False)],
                                'validar': [('readonly', False)],
                                'modcontractual': [('readonly', False)]
                            }),
        'text_condicions': fields.text('Text condicions',
                                readonly=True,
                                states={
                                    'esborrany':[('readonly', False)],
                                    'validar': [('readonly', False)],
                                    'modcontractual': [('readonly', False)]
                                }) 
    }

    _defaults = {
        'condicions_especials': lambda *a: False,
    }

GiscedataPolissaAddCondicions()


class GiscedataPolissaModContractualAddCondicions(osv.osv):

    _name = 'giscedata.polissa.modcontractual'
    _inherit = 'giscedata.polissa.modcontractual'

    _columns = {
        'condicions_especials': fields.boolean('Condicions especials',
                    help=(u"Marcar aquesta opció per habilitar la "
                          u"possibilitat d'incloure condicions "
                          u"especials al contracte")),
        'condicio_ids': fields.many2many('giscedata.polissa.condicions',
                                         'condicions_modcontractual_rel',
                                         'modcontractual_id', 'condicio_id',
                                         string="Condicions",),
        'text_condicions': fields.text('Text condicions') 
    }

    _defaults = {
        'condicions_especials': lambda *a: False,
    }

GiscedataPolissaModContractualAddCondicions()
