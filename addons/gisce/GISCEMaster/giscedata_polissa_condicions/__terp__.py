# -*- coding: utf-8 -*-
{
    "name": "Condicions Pòlissa",
    "description": """Afegeix la possibilitat d'incloure
condicions especials a les pòlisses""",
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_polissa"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_polissa_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
