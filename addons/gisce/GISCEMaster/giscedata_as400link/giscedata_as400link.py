# -*- coding: iso-8859-1 -*-
from osv import osv, fields
import as400odbc
import pyodbc
import pooler

class giscedata_cts(osv.osv):
  _name = 'giscedata.cts'
  _inherit = 'giscedata.cts'

  def create(self, cur, uid, vals, context={}):

    ct_id =  super(osv.osv, self).create(cur, uid, vals, context)

    # Aqui hem de crear la connexio odbc i donar d'alta el ct a l'AS-400
    # si company_id = 2, Bassols la taula es CTSBAEFF
    # si company_id = 3, Curos la taula és CTSECUFF
    # Camps:
    # ECODET = vals['name']
    # EDESET = vals['descripcio']
    # EDIP   = Codi INE (NO ID)
    # ECTZTC = -1

    if ct_id:
      cnxn = pyodbc.connect(as400odbc.get_connection_string())
      cr = cnxn.cursor()

      # Per defecte posem la tauala de Bassols
      taula = 'CTSBAEFF'
      if vals['company_id'] == 2:
        taula = 'CTSBAEFF'
      elif vals['company_id'] == 3:
        taula = 'CTSECUFF'

      # Buscem el codi INE
      municipi = pooler.get_pool(cur.dbname).get('res.municipi').read(cur, uid, vals['id_municipi'], ['ine'])
      cr.execute("INSERT INTO %s (ECODET, EDESET, EDIP, ECTZTC) VALUES ('%s', '%s', %i, -1)" % \
        (taula, vals['name'].replace("'", " "), vals['descripcio'].replace("'", " "), int(municipi['ine'])))
      cnxn.commit()
      cnxn.close()

    return ct_id

giscedata_cts()


class giscedata_transformador_trafo(osv.osv):
  _name = "giscedata.transformador.trafo"
  _inherit = "giscedata.transformador.trafo"

  _columns = {
    'n_compte': fields.char('Número de compte', size=256, readonly=True),
  }

  def create(self, cur, uid, vals, context={}):
    trafo_id =  super(osv.osv, self).create(cur, uid, vals, context)
 
    # Aqui hem de crear la connexio odbc i donar d'alta el trafo a l'AS-400
    # si company_id = Bassols la taula es TRFBAEFF
    # si company_id = Curos la taula és TRFECUFF
    # Camps:
    # TCDCCOM = max(TCDCCOM) + 1
    # TCDACC  = 'NT %s' % (vals['name'])
    # TNMCOMP = 'TRAFO %s KVA' % (vals['potencia_nominal'])
    # TCDUNIO = 'NF %s' % (vals['numero_fabricacio'])

    if trafo_id:
      cnxn = pyodbc.connect(as400odbc.get_connection_string())
      cr = cnxn.cursor()

      # Per defecte posem la tauala de Bassols
      taula = 'TRFBAEFF'
      if vals['company_id'] == 2:
        taula = 'TRFBAEFF'
      elif vals['company_id'] == 3:
        taula = 'TRFECUFF'

      # Busquem el TCDCCOM
      tcdccom = 1
      cr.execute("SELECT max(TCDCCOM)+1 AS TCDCCOM FROM %s" % taula)
      row = cr.fetchone()
      try:
        if row.TCDCCOM:
          tcdccom = row.TCDCCOM
      except AttributeError:
        if row.tcdccom:
          tcdccom = row.tcdccom

      cr.execute("INSERT INTO %s (TCDCCOM, TCDACC, TNMCOMP, TCDUNIO) VALUES (%i, 'NT %s',\
        'TRAFO %s KVA', 'NF %s')" % (taula, int(tcdccom), vals['name'], vals['potencia_nominal'],\
        vals['numero_fabricacio']))
      cnxn.commit()
      cnxn.close()
      
      # Posem el n_compte de l'AS-400 al trafo
      pooler.get_pool(cur.dbname).get('giscedata.transformador.trafo').write(cur, uid, [trafo_id],\
      {'n_compte': tcdccom})

    return trafo_id

giscedata_transformador_trafo()
