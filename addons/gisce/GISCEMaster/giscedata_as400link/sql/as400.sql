SET client_min_messages = warning;

CREATE TABLE CTSBAEFF (
  id serial NOT NULL,
  ECODET character varying(100) NOT NULL,
  EDESET character varying(255) NOT NULL,
  EDIP integer,
  ECTZTC integer
);

CREATE TABLE CTSECUFF (
  id serial NOT NULL,
  ECODET character varying(100) NOT NULL,
  EDESET character varying(255) NOT NULL,
  EDIP integer NOT NULL,
  ECTZTC integer NOT NULL
);

CREATE TABLE TRFBAEFF (
  id serial NOT NULL,
  TCDCCOM integer NOT NULL,
  TCDACC character varying(255) NOT NULL,
  TNMCOMP character varying(255) NOT NULL,
  TCDUNIO character varying(255) NOT NULL
);

CREATE TABLE TRFECUFF (
  id serial NOT NULL,
  TCDCCOM integer NOT NULL,
  TCDACC character varying(255) NOT NULL,
  TNMCOMP character varying(255) NOT NULL,
  TCDUNIO character varying(255) NOT NULL
);
