# -*- coding: utf-8 -*-
{
    "name": "GISCE Data enllaç a AS-400",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Integration",
    "depends":[
        "giscedata_cts",
        "giscedata_transformadors"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_as400link_view.xml"
    ],
    "active": False,
    "installable": True
}
