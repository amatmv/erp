# -*- coding: utf-8 -*-
{
    "name": "GISCE Data Transformadors",
    "description": """
    Models per gestioar els transformadors
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Centres Transformadors/Transformadors",
    "depends":[
        "giscedata_cts",
        "giscedata_administracio_publica_cnmc_distri",
        "stock"
    ],
    "init_xml": [],
    "demo_xml":[
        "giscedata_transformadors_demo.xml",
        "giscedata_transformadors_sequence_demo.xml"
    ],
    "update_xml":[
        "giscedata_transformadors_data.xml",
        "giscedata_transformadors_view.xml",
        "giscedata_transformadors_report.xml",
        "giscedata_transformadors_wizard.xml",
        "giscedata_cts_view.xml",
        "ir.model.access.csv",
        "security/ir.model.access.csv",
        "giscedata_tipus_installacio_transformardors_data.xml",
        "giscedata_transformadors_sequence.xml"
    ],
    "active": False,
    "installable": True
}
