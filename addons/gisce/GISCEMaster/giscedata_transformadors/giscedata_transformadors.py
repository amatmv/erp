# -*- coding: utf-8 -*-
from osv import osv, fields
import pooler
from datetime import *
from giscedata_administracio_publica_cnmc_distri.wizard.wizard_recalcul_cinis_tis import MODELS_SELECTION
from giscedata_administracio_publica_cnmc_distri.giscedata_tipus_installacio import TIPUS_REGULATORI

MODELS_SELECTION.append(
    ('giscedata.transformador.trafo', 'Transformadors'),
)

TIPUS_CT = [
    ('interior', 'Interior'),
    ('exterior', 'Exterior')
]


class GiscedataTransformadorLocalitzacio(osv.osv):

    def create(self, cr, uid, vals, context={}):
        if 'code' not in vals:
            cr.execute("SELECT code FROM giscedata_transformador_localitzacio "
                       "ORDER BY code DESC LIMIT 1")
            codi = cr.fetchone()
            if codi and len(codi):
                vals['code'] = int(codi[0]) + 1
            else:
                vals['code'] = 1
        return super(osv.osv, self).create(cr, uid, vals, context)

    _name = 'giscedata.transformador.localitzacio'
    _description = 'Localitzacions dels transformadors'
    _columns = {
       'name': fields.char('Nom de la localització', size=60),
       'code': fields.integer('Codi'),
       'relacio': fields.many2one('ir.model', 'Relació'),
       'estats': fields.one2many('giscedata.transformador.estat', 'localitzacio', 'Estats'),
    }

    _defaults = {

    }

    _order = "name, id"


GiscedataTransformadorLocalitzacio()


class GiscedataEstatTransformador(osv.osv):

    def create(self, cr, uid, vals, context={}):
        if 'codi' not in vals:
            cr.execute("SELECT codi FROM giscedata_transformador_estat "
                       "ORDER BY codi DESC LIMIT 1")
            codi = cr.fetchone()
            if codi and len(codi):
                codi = int(codi[0]) + 1
                vals['codi'] = codi
            else:
                vals['codi'] = 1
        return super(osv.osv, self).create(cr, uid, vals, context)

    _name = 'giscedata.transformador.estat'
    _description = 'Estat del transformador'
    _columns = {
      'name': fields.char('Nom', size=60, required=True),
      'descripcio': fields.text('Descripcio'),
      'codi': fields.integer('Codi'),
      'localitzacio': fields.many2one('giscedata.transformador.localitzacio', 'Localització aplicable'),
      'cnmc_inventari': fields.boolean("Apareix a l'inventari")
    }

    _defaults = {
      'cnmc_inventari': lambda *a: True,
    }

    _order = "name, id"


GiscedataEstatTransformador()


class GiscedataConnexioUne(osv.osv):
    _name = 'giscedata.transformador.connexioune'
    _description = 'Connexio UNE del transformador'
    _columns = {
      'name': fields.char('Nom', size=60, required=True),
      'descripcio': fields.text('Descripcio'),
    }

    _defaults = {
    }

    _order = "name, id"


GiscedataConnexioUne()


class giscedata_refrigerant(osv.osv):
    _name = 'giscedata.transformador.refrigerant'
    _descripcio = 'Refrigerant del transformador'
    _columns = {
      'name': fields.char('Nom', size=60, required=True),
      'descripcio': fields.text('Descripcio'),
    }

    _defaults = {
    }

    _order = "name, id"


giscedata_refrigerant()


class giscedata_marca(osv.osv):
    _name = 'giscedata.transformador.marca'
    _descripcio = 'Marca del transformador'
    _columns = {
      'name': fields.char('Nom', size=60, required=True),
      'descripcio': fields.text('Descripcio'),
    }

    _defaults = {
    }

    _order = "name, id"


giscedata_marca()


class giscedata_transformador_magatzem(osv.osv):

    _name = "giscedata.transformador.magatzem"
    _description = "Magatzems a que es poden enviar els Transformadors"

    _columns = {
                  'name': fields.char('Nom', size=60),
                  'contacte': fields.many2one('res.partner', 'Contacte'),
          }


giscedata_transformador_magatzem()


class GiscedataTrafo(osv.osv):

    def create(self, cr, uid, vals, context=None):
        """
        Overwrites the Trafo create by setting up the historic, localitzacio and
        estat of the Trafo. It sets the localitzacio to Magatzem by default if
        it's not provided on the vals.
        :param cr: Database Cursors
        :type cr: Cursor
        :param uid: User ID
        :type uid: int
        :param vals: Attribute values of the Trafo it's about to be created
        :type vals: dict[str, Any]
        :param context: OpenERP context
        :type context: dict[str, Any]
        :return: The ID of the created Trafo
        :rtype: int
        """
        if context is None:
            context = {}
        vals['state'] = 'creat'
        loc_magatzem_id = self.pool.get(
            'giscedata.transformador.localitzacio'
        ).search(cr, uid, [('code', '=', 4)])[0]
        if 'localitzacio' not in vals:
            vals['localitzacio'] = loc_magatzem_id
        trafo_id = super(osv.osv, self).create(cr, uid, vals, context)

        if trafo_id:
            historic = self.pool.get('giscedata.transformador.historic')
            historic_vals = {
                'data_sortida': (datetime.now()).strftime('%Y-%m-%d %H:%M:%S'),
                'name': vals['magatzem_entrada'],
                'tipus_desti': loc_magatzem_id,
                'data_entrada': (
                        datetime.now() + timedelta(hours=1)
                ).strftime('%Y-%m-%d %H:%M:%S'),
                'transformador_id': trafo_id,
            }
            historic.create(cr, uid, historic_vals, context)
            if loc_magatzem_id != vals['localitzacio']:
                historic_vals = {
                    'origen': vals['magatzem_entrada'],
                    'tipus_origen': loc_magatzem_id,
                    'data_sortida': (
                        datetime.now() + timedelta(hours=2)
                    ).strftime('%Y-%m-%d %H:%M:%S'),
                    'name': vals.get('ct', None),
                    'tipus_desti': vals['localitzacio'],
                    'data_entrada': (
                        datetime.now() + timedelta(hours=3)
                    ).strftime('%Y-%m-%d %H:%M:%S'),
                    'transformador_id': trafo_id,
                }
                historic.create(cr, uid, historic_vals, context)

        return trafo_id

    def _get_localitzacions(self, cr, uid, context={}):
        obj = self.pool.get('giscedata.transformador.localitzacio')
        ids = obj.search(cr, uid, [])
        res = obj.read(cr, uid, ids, ['name', 'id'], context)
        res = [(r['id'], r['name']) for r in res]
        return res

    def _get_ct_desc(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for trafo in self.browse(cr, uid, ids):
            if trafo.ct.id != False:
                res[trafo.id] = trafo.ct.descripcio
            else:
                res[trafo.id] = ''
        return res

    def _get_localitzacio_name(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for trafo in self.browse(cr, uid, ids):
            sql = """
            SELECT name
            FROM giscedata_transformador_historic
            WHERE
             transformador_id = %s
            ORDER BY data_entrada desc
            LIMIT 1"""
            cr.execute(sql, (int(trafo.id), ))
            id_localitzacio = cr.fetchone()
            if id_localitzacio:
                try:
                    res[trafo.id] = self.pool.get(trafo.localitzacio.relacio.model).browse(cr, uid, id_localitzacio[0]).name
                except:
                    res[trafo.id] = ''
            else:
                res[trafo.id] = ''
        return res

    def _localitzacio_name_search(self, cr, uid, obj, name, args, context=None):
        if not len(args):
            return []
        else:
            res = []
            for trafo in self.browse(cr, uid, self.search(cr, uid, [])):
                if (trafo.localitzacio_name
                        and trafo.localitzacio_name.capitalize().find(
                            args[0][2].capitalize()) != -1):
                    res.append(trafo.id)
            if not res:
                return [('id', '=', '0')]
            return [('id', 'in', res)]

    def _suggerir_numero(self, cr, uid, context):

        return self.pool.get('ir.sequence').get(
            cr, uid, 'giscedata.transformador.trafo'
        )

    def _tensio_primari_actual(self, cr, uid, ids, field_name, arg, context):
        return self._tensio(cr, uid, ids, field_name, arg, 'pri', context)

    def _tensio_secundari_actual(self, cr, uid, ids, field_name, arg, context):
        return self._tensio(cr, uid, ids, field_name, arg, 'sec', context)

    def _tensio(self, cr, uid, ids, field_name, arg, mode, context):
        res = {}
        conn_obj = self.pool.get('giscedata.transformador.connexio')
        for idcon in ids:
            actual = conn_obj.search(
                cr, uid, [('trafo_id', '=', idcon), ('conectada', '=', True)]
            )
            tensio = False
            if actual:
                connexio = conn_obj.read(cr, uid, actual)[0]
                if mode == 'pri':
                    tensio = (
                        (connexio['connectada_p2'] and
                         connexio['tensio_p2']) or connexio['tensio_primari']
                    )
                elif mode == 'sec':
                    tensio = max(
                        connexio['tensio_b1'],
                        connexio['tensio_b2'],
                        connexio['tensio_b3']
                    )
            res[idcon] = tensio

        return res

    def _tensio_primari_actual_search(self, cr, uid, obj, name, args, context=None):
        if not len(args):
            return []
        else:
            cerca = {'tensio_primari': '', 'tensio_p2': ''}
            for s in args:
                for tensio_field in cerca.keys():
                    cerca_tmp = cerca[tensio_field]
                    cerca[tensio_field] = '%s %s %s %s %d ' % (
                        cerca_tmp ,
                        len(cerca_tmp) and ' and ' or '',
                        tensio_field ,
                        s[1],
                        s[2])
            sql = """
              SELECT
                id,
                trafo_id,
                conectada,
                connectada_p2,
                tensio_primari,
                tensio_p2
            FROM giscedata_transformador_connexio
            WHERE
                conectada AND (
                (( NOT connectada_p2 OR connectada_p2 IS NULL) and %s ) OR
                (connectada_p2 and %s ))
            """ % (cerca['tensio_primari'], cerca['tensio_p2'])
            trafos_ids = []
            cr.execute(sql)
            for connexio in cr.fetchall():
                trafos_ids.append(connexio[1])
            return [('id', 'in', trafos_ids)]

    def _estat_sel(self, cr, uid, ids, field_name, arg, context=None):
        return dict([(x, False) for x in ids])

    def _get_estat_sel(self, cr, uid, context):
        res = []
        est_obj = self.pool.get('giscedata.transformador.estat')
        estats = est_obj.read(cr, uid,
            est_obj.search(cr, uid, []), ['id', 'name'])
        for estat in estats:
            res.append((estat['id'], estat['name']))
        return res

    def _estat_sel_search(self, cr, uid, obj, name, args, context=None):
        if not len(args):
            return []
        else:
            trafo_obj = self.pool.get('giscedata.transformador.trafo')
            trafo_ids = trafo_obj.search(cr, uid, [('id_estat', '=', args[0][2])])
            return [('id', 'in', trafo_ids)]

    def _data_pm(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        res = dict.fromkeys(ids, False)
        for item in self.browse(cursor, uid, ids, context):
            if item.historic and not item.bloquejar_pm:
                # Busquem el destí un CT més antic
                h_cts = [h for h in item.historic
                         if (h.tipus_desti and
                             h.tipus_desti.relacio.model == 'giscedata.cts')]
                if h_cts:
                    res[item.id] = min(h.data_entrada for h in h_cts)
            else:
                res[item.id] = item.data_pm
        return res

    def _get_trafos(self, cursor, uid, ids, context=None):
        cursor.execute("select h.transformador_id from "
        "giscedata_transformador_historic AS h "
        "LEFT JOIN giscedata_transformador_trafo "
        "  AS t ON (h.transformador_id=t.id) "
        "WHERE h.id IN %s and not t.bloquejar_pm",
        (tuple(ids), ))
        return [x[0] for x in cursor.fetchall()]

    def _get_trafos_nobloc(self, cursor, uid, ids, context=None):
        cursor.execute("select id from giscedata_transformador_trafo "
        "where not bloquejar_pm and id in %s", (tuple(ids), ))
        return [x[0] for x in cursor.fetchall()]

    def _get_trafos_nobloc_cini(self, cursor, uid, ids, context=None):
        if self._name == 'giscedata.transformador.trafo':
            cursor.execute("select id from giscedata_transformador_trafo "
                           "where not bloquejar_cini and id in %s",
                           (tuple(ids),))
        elif self._name == 'giscedata.transformador.connexio':
            cursor.execute("select con.trafo_id from "
                           "giscedata_transformador_connexio as con "
                           "left join giscedata_transformador_trafo as t on "
                           "(con.trafo_id = t.id)"
                           "where con.id in %s and "
                           "(con.conectada or con.connectada_p2)"
                           "and not t.bloquejar_cini", (tuple(ids),))
        return [x[0] for x in cursor.fetchall()]

    def _overwrite_apm(self, cursor, uid, ids, field_name, field_value, args,
                       context=None):
        if not isinstance(ids, (tuple, list)):
            ids = [ids]

        cursor.execute("update giscedata_transformador_trafo set data_pm=%s "
                       "where id in %s and bloquejar_pm",
                       (field_value or None, tuple(ids),))

    def _cini(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        res = dict.fromkeys(ids, False)
        from cini.models import Transformador
        for tf in self.browse(cursor, uid, ids):
            if tf.localitzacio.code == 4:
                # Transformador en magatzem
                res[tf.id] = 'I2900600'
            else:
                trafo = Transformador()
                try:
                    tensio_pri = tf.tensio_primari_actual
                    trafo.tension_p = tensio_pri / 1000.0
                except TypeError:
                    trafo.tension_p = 0
                try:
                    tensio_sec = tf.tensio_secundari_actual
                    trafo.tension_s = tensio_sec / 1000.0
                except TypeError:
                    trafo.tension_s = 0
                if tf.id_estat.codi == 1:
                    # Transformador en servei
                    trafo.estado = 'S'
                elif tf.id_estat.codi == 7:
                    # Transformador en reserva
                    trafo.estado = 'R'
                # trafo.situacion = tf.ct.id_installacio.name
                subestacions = self.pool.get('giscedata.cts.subestacions')
                if subestacions and subestacions.search(
                        cursor, uid, [('ct_id', '=', tf.ct.id)],
                        context={'active_test': False}
                ):
                    # Es una subestacio
                    trafo.situacion = 'SE'
                else:
                    # Es un centre de transformacio
                    trafo.situacion = 'CT'
                trafo.potencia = tf.potencia_nominal / 1000.0
                res[tf.id] = str(trafo.cini)
        return res

    def _cini_inv(self, cursor, uid, ids, name, value, args, context=None):
        if not context:
            context = {}

        if not isinstance(ids, (tuple, list)):
            ids = [ids]

        cursor.execute("update giscedata_transformador_trafo set cini=%s"
                       "where id in %s and bloquejar_cini", (value or None, tuple(ids),))

        return True

    def _get_trafos_nobloc_ti(self, cursor, uid, ids, context=None):
        """
        Retorna els ids de tranformadors a recalcular basat en uns triggers de
        diferents objectes.
        """

        if context is None:
            context = {}
        trafo_ids = []
        if self._name == 'giscedata.transformador.connexio':
            ids = self.search(cursor, uid, [
                ('id', 'in', ids),
                '|',
                    ('conectada', '=', 1),
                    ('connectada_p2', '=', 1)
            ])
            for trafo in self.read(cursor, uid, ids, ['trafo_id']):
                trafo_ids.append(trafo['trafo_id'][0])

        elif self._name == 'giscedata.transformador.trafo':
            trafo_ids = self.search(cursor, uid, [
                ('bloquejar_cnmc_tipus_install', '!=', 1),
                ('id', 'in', ids)
            ])
        return trafo_ids

    def _tipus_instalacio_cnmc_id(self, cursor, uid, ids, field_name, arg,
                                  context=None):
        if not context:
            context = {}
        res = dict.fromkeys(ids, False)
        from tipoinstalacion.models import Transformador
        for trafo in self.browse(cursor, uid, ids, context=context):
            # TODO: Fix https://github.com/gisce/erp/issues/3252
            if trafo.bloquejar_cnmc_tipus_install:
                res[trafo.id] = trafo.tipus_instalacio_cnmc_id.id
                continue
            ti = Transformador()
            try:
                ti.tension_p = trafo.tensio_primari_actual / 1000.0
            except (TypeError, ValueError):
                ti.tension_p = None
            try:
                ti.tension_s = trafo.tensio_secundari_actual / 1000.0
            except (TypeError, ValueError):
                ti.tension_s = None
            if ti.tipoinstalacion:
                ti_obj = self.pool.get('giscedata.tipus.installacio')
                ti_ids = ti_obj.search(cursor, uid, [
                    ('name', '=', ti.tipoinstalacion)
                ])
                res[trafo.id] = ti_ids[0]
        return res

    def _tipus_instalacio_cnmc_id_inv(self, cursor, uid, ids, name, value, args,
                                      context=None):
        if not context:
            context = {}

        if not isinstance(ids, (tuple, list)):
            ids = [ids]
        cursor.execute(
            "update giscedata_transformador_trafo set "
            "tipus_instalacio_cnmc_id=%s "
            "where id in %s and bloquejar_cnmc_tipus_install",
            (value or None, tuple(ids),)
        )
        return True

    def moure(
            self, cursor, uid, ids, id_localitzacio, id_desti,
            data_entrada=None, data_sortida=None, context=None
    ):
        """
        Move transformador of localitzation

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: Trafos ids
        :type ids: list(int), int
        :param id_localitzacio: Localitzacio id
        :type id_localitzacio: list of int
        :param id_desti: Desti id
        :type id_desti: int
        :type context: OpenERP context
        :param context: dict
        :rtype: bool
        """

        if data_entrada is None:
            data_entrada = str(datetime.now() + timedelta(hours=1))

        if data_sortida is None:
            data_sortida = str(datetime.now())

        if isinstance(ids, (int, long)):
            ids = [ids]

        trf_obj = self.pool.get("giscedata.transformador.trafo")
        hist_obj = self.pool.get("giscedata.transformador.historic")
        loc_obj = self.pool.get("giscedata.transformador.localitzacio")
        trafo = trf_obj.browse(cursor, uid, ids[0], context=context)

        hist_data = {
            "transformador_id": ids[0],
            "name": id_desti,
            "data_entrada": data_entrada,
            "observacions": "",
            "tipus_desti": id_localitzacio,
            "tipus_origen": trafo.localitzacio.id,
            "data_sortida": data_sortida,
        }

        if trafo.localitzacio.code == 4:
            mag_id = trf_obj.read(
                cursor, uid, ids[0], ["magatzem_entrada"], context=context
            )["magatzem_entrada"]
            if mag_id:
                hist_data["origen"] = mag_id[0]
        elif trafo.localitzacio.code == 1:
            ct_id = trf_obj.read(
                cursor, uid, ids[0], ["ct"], context=context
            )["ct"]
            if ct_id:
                hist_data["origen"] = ct_id[0]

        # Aqui ens hauriem de plantejar si els historics haurien de ser també
        # session managed
        hist_obj.create(
            cursor, uid,
            hist_data,
            context=context
        )

        loc = loc_obj.browse(cursor, uid, id_localitzacio)
        if loc.estats:
            id_estat = loc.estats[0].id
        else:
            id_estat = False
        if loc.code == 4:
            trf_obj.write(
                cursor, uid, ids[0],
                {
                    "id_estat": id_estat,
                    "ct": None,
                    "localitzacio": id_localitzacio
                },
                context=context
            )
        else:
            trf_obj.write(
                cursor, uid, ids[0],
                {
                    "id_estat": id_estat,
                    "ct": id_desti,
                    "localitzacio": id_localitzacio
                },
                context=context
            )

        return True

    _name = 'giscedata.transformador.trafo'
    _description = 'Transformadors'
    _columns = {
        'name': fields.char('Numero de transformador', size=50, required=True),
        'ct': fields.many2one('giscedata.cts', 'Centre Transformador'),
        'ct_desc': fields.function(_get_ct_desc, type='char', method=True, string='Descripció CT'),
        'ordre_dins_ct': fields.integer('Ordre dins el ct'),
        'reductor': fields.boolean('Reductor'),
        'tiepi': fields.boolean('TIEPI'),
        'id_marca':  fields.many2one('giscedata.transformador.marca', 'Marca', ondelete='restrict'),
        'tipo': fields.char('Tipus de transformador', size=60),
        'normaune': fields.char('Norma UNE', size=50),
        'id_connexioune': fields.many2one('giscedata.transformador.connexioune', 'Connexio UNE'),
        'numero_fabricacio': fields.char('Numero de fabricacio', size=60),
        'ordre_protocol': fields.char('Ordre de protocol', size=60),
        'comanda': fields.char('Numero de comanda', size=10),
        'data_compra': fields.datetime('Data de compra'),
        'any_fabricacio': fields.integer('Any de fabricacio'),
        'pes_total': fields.integer('Pes total Kg'),
        'pes_oli': fields.integer('Pes total aillant Kg'),
        'pes_desencubar': fields.integer('Pes a desencubar Kg'),
        'alcada': fields.integer('Alcada'),
        'amplada': fields.integer('Amplada'),
        'fondaria': fields.integer('Fondaria'),
        'potencia_nominal': fields.integer('Potencia nominal (KVA)'),
        'escalfament': fields.integer('Escalfament (Co)'),
        'tensiocc': fields.char('Tensio de cc a 75Co (220)', size=10),
        'tensiocc380': fields.char('Tensio de cc a 75Co (380)', size=10),
        'nivell_aillament_at': fields.char('Nivell aillament AT ( a 50Hz i a xoc)', size=25),
        'nivell_aillament_bt': fields.char('Nivell aillament BT ( a 50Hz i a xoc)', size=25),
        'soroll': fields.char('Nivell de soroll (dB)', size=10),
        'material_at': fields.char('Material AT', size=25),
        'material_bt': fields.char('Material BT', size=25),
        'id_refrigerant':  fields.many2one('giscedata.transformador.refrigerant', 'Refrigerant'),
        'id_estat': fields.many2one('giscedata.transformador.estat', 'Estat'),
        'id_estat_sel': fields.function( _estat_sel, selection=_get_estat_sel,
          fnct_search=_estat_sel_search, type="selection", method=True,
          string='Estat',size="10"),
        'k': fields.float('Constant K'),
        'ompliment_integral': fields.boolean('Ompliment Integral'),
        'calibre_ti_b1': fields.char('Calibre TI B1', size=25),
        'calibre_ti_b2': fields.char('Calibre TI B2', size=25),
        'id_propietari': fields.many2one('res.partner','Propietari'),
        'darrer_filtrat': fields.datetime('Darrer Filtrat'),
        'id_proveidor': fields.many2one('res.partner', 'Proveidor'),
        'connector_at_endollable': fields.boolean('Connector AT Endollable'),
        'connector_bt_endollable': fields.boolean('Connector BT Endollable'),
        'estadistica': fields.boolean('Estadistica'),
        'tipusct': fields.selection(TIPUS_CT, "Transformador per Exterior/Interior"),
        'observacions': fields.text('Observacions'),
        'elements_rodes': fields.boolean('Rodes'),
        'elements_nivell': fields.boolean('Nivell'),
        'elements_dipexpansio': fields.boolean('DipExpansió'),
        'elements_aixeta': fields.boolean('Aixeta'),
        'elements_termometre': fields.boolean('Termómetre'),
        'elements_placa': fields.boolean('Placa'),
        'tensio_b1': fields.char('Tensio Baixa B1', size=10),
        'tensio_b2': fields.char('Tensio Baixa B2', size=10),
        'tensio_b3': fields.char('Tensio Baixa B3', size=10),
        'ordre_transformador_ct': fields.integer('Ordre Transformador CT'),
        'id_incidencies': fields.one2many('giscedata.transformador.incidencia', 'name', 'Incidencies'),
        'conexions': fields.one2many('giscedata.transformador.connexio','trafo_id', 'Conexions'),
        'mesures': fields.one2many('giscedata.transformador.mesures', 'trafo', 'Mesures'),
        'protocol_img': fields.binary('Imatge Protocol'),
        'trafo_img': fields.binary('Imatge Trafo'),
        'placa_img': fields.binary('Imatge Placa'),
        'localitzacio': fields.many2one(
            'giscedata.transformador.localitzacio', 'Localització',
            selection=_get_localitzacions
        ),
        'localitzacio_name': fields.function(
            _get_localitzacio_name, fnct_search=_localitzacio_name_search,
            type='char', method=True, string='Nom de la localització', size=200
        ),
        'historic': fields.one2many('giscedata.transformador.historic', 'transformador_id', 'Històric'),
        'state': fields.char('Estat', size=60),
        'magatzem_entrada': fields.many2one(
            'giscedata.transformador.magatzem', 'Magatzem d\'entrada',
            readonly=True,
            states={
                'nou':[
                    ('readonly', False),
                    ('required', True)
                ]
            }
        ),
        'cini': fields.function(_cini,
            type='char', size=8, method=True, store={
                'giscedata.transformador.trafo': (
                    _get_trafos_nobloc_cini, [
                        'bloquejar_cini', 'magatzem_entrada', 'localitzacio',
                        'id_estat', 'ct', 'potencia_nominal'
                    ], 10
                ),
                'giscedata.transformador.connexio': (
                    _get_trafos_nobloc_cini, [
                        'tensio_primari', 'tensio_p2', 'connectada_p2',
                        'tensio_b1', 'tensio_b2', 'tensio_b3', 'conectada'
                    ], 10
                )
            }, fnct_inv=_cini_inv, string='CINI'),
        'bloquejar_cini': fields.boolean('Bloquejar CINI'),
        'tensio_primari_actual': fields.function(_tensio_primari_actual,
          fnct_search=_tensio_primari_actual_search,
                  type='integer',method=True, string="Tensió primari presa activa", size=6),
        'tensio_secundari_actual': fields.function(
            _tensio_secundari_actual, type='integer', method=True,
            string='Tensió secundari presa activa'
        ),
        # dades administratives
        'data_pm': fields.function(
        _data_pm, type='date', method=True, store={
            'giscedata.transformador.historic': (
                _get_trafos, ['data_entrada'], 10
            ),
            'giscedata.transformador.trafo': (
                _get_trafos_nobloc, ['historic', 'bloquejar_pm'], 10
            )
        },
        fnct_inv=_overwrite_apm,
        string='Data APM', help=u"Data de posada en marxa del transformacdor "
                                u"segons l'històric. Es considera promera "
                                u"instal·lació del transformador en un CT"
        ),
        'perc_financament': fields.float('% pagat per la companyia'),
        'propietari': fields.boolean('Propietari'),
        'bloquejar_pm': fields.boolean('Bloquejar APM',
                                      help=u"Si està activat, agafa la data "
                                           u"entrada, si no, la de "
                                           u"l'històric"),
        'baixa': fields.boolean('Baixa'),
        'data_baixa': fields.datetime('Data de Baixa'),
        'active': fields.boolean('Actiu'),
        'cnmc_tipo_instalacion': fields.char(
            u'CNMC Tipus Instal·lació', size=10,readonly=False),
        'bloquejar_cnmc_tipus_install': fields.boolean(
            u'Bloquejar TI', readonly=False,
            help=u"Si està activat, permet modificar manualment el "
                 u"tipus d'instal·lació de la CNMC"
        ),
        'tipus_instalacio_cnmc_id': fields.function(
            _tipus_instalacio_cnmc_id,
            method=True,
            string='Tipologia CNMC',
            relation='giscedata.tipus.installacio',
            type='many2one',
            fnct_inv=_tipus_instalacio_cnmc_id_inv,
            store={
                'giscedata.transformador.connexio': (
                    _get_trafos_nobloc_ti, [
                        'tensio_primari', 'tensio_b2', 'conectada', 'tensio_b3',
                        'tensio_b1', 'tensio_p2', 'connectada_p2'
                    ], 10
                ),
                'giscedata.transformador.trafo': (
                    _get_trafos_nobloc_ti, [
                        'bloquejar_cnmc_tipus_install'
                    ], 10
                )
            }
        ),
        'perdues_buit': fields.float(u'Pèrdues de buit (kW)', digits=(9, 3)),
        'perdues_curtcircuit_nominal': fields.float(
            u'Pèrdues curtcircuit (kW)', digits=(9, 3),
            help=u"Pèrdues de curtcircuit a potencia nominal."
        ),
        'energia_anual': fields.float(
            'Energia anual circulada (kWh)', digits=(15, 3)
        ),
        'potencia_activa': fields.float(
            'Potencia pic activa (MW)', digits=(15, 3),
            help=u"Potencia pic d'activa costat de baixa"
        ),
        'potencia_reactiva': fields.float(
            'Potencia pic reactiva (MVAr)', digits=(15, 3),
            help=u"Potencia pic de reactiva costat de baixa"
        ),
        '4771_entregada': fields.json('Dades 4771 entegades'),
        '4131_entregada_2016': fields.json('Dades 4131 entregades 2016'),
        '4666_entregada_2017': fields.json('Dades entregades 4666 2017'),
        '4666_entregada_2018': fields.json('Dades entregades 4666 2018'),
        "stock_id": fields.many2one(
            "stock.production.lot", string="Numero de serie"
        ),
        "criteri_regulatori": fields.selection(
            TIPUS_REGULATORI,
            required=True,
            string="Criteri regulatori",
            help="Indica si el criteri a seguir al genrar els fitxers d'"
                 "inventari.\nSegons criteri:S'usa el criteri normal\n Froçar "
                 "incloure:S'afegeix l'element al fitxer\n Forçar excloure: "
                 "L'element no apareixera mai",
            select=1
        ),
        '4666_entregada_2019': fields.json('Dades entregades 4666 2019'),
    }
    _defaults = {
        'state': lambda * a: 'nou',
        'name': _suggerir_numero,
        'potencia_nominal': lambda *a: 0,
        'active': lambda *a: 1,
        'baixa': lambda *a: False,
        'perc_financament': lambda *a: 100.00,
        'propietari': lambda *a: 1,
        'bloquejar_pm': lambda *a: False,
        'bloquejar_cini': lambda *a: 0,
        "criteri_regulatori": lambda * a: 'criteri',
    }

    _sql_constraints = [
          ('numero_fabricacio_uniq', 'unique (numero_fabricacio)', 'Ja existeix un transformador amb aquest número de sèrie'),
          ('name_uniq', 'unique (name)', 'Ja existeix un transformador amb aquest número'),
          ('name_numeric',
           "check(name ~ '^[0-9]+$')",
           'El nom ha de ser un número')
    ]

    _order = "name::int, id"
GiscedataTrafo()

class giscedata_trafo_mesures(osv.osv):

    def create(self, cr, uid, vals, context={}):
        cr.execute("SELECT ct FROM giscedata_transformador_trafo WHERE id =\
        "+str(vals['trafo']))
        ct = cr.fetchone()
        if ct:
            vals['ct'] = ct[0]
            return super(osv.osv, self).create(cr, uid, vals, context)
        else:
            return False

    _name = 'giscedata.transformador.mesures'
    _descripcio = 'Mesures del Transformador'

    _columns = {
      'name' : fields.datetime('Data'),
      'trafo' : fields.many2one('giscedata.transformador.trafo', 'Transformador'),
      'ct': fields.many2one('giscedata.cts', 'Centre Transformador'),
      'temp' : fields.float('Temperatura'),
      'intensitat_max_bt2' : fields.float('Intensitat Màxima BT'),
      'intensitat_inici_bt2' : fields.float('Intenstat de disparo'),
      'rigidesa_dialectrica' : fields.float('Rigidesa Dialèctrica'),
      'temp_max' : fields.float('Temperatura Màxima'),
      'temp_inici' : fields.float('Temperatura de disparo'),
      'bt2_buit_rs' : fields.float('Buit Fase R(w), S(v)'),
      'bt2_buit_rt' : fields.float('Buit Fase R(w), T(u)'),
      'bt2_buit_st' : fields.float('Buit Fase S(v), T(u)'),
      'bt2_buit_rn' : fields.float('Buit Fase R(w), N'),
      'bt2_buit_sn' : fields.float('Buit Fase S(v), N'),
      'bt2_buit_tn' : fields.float('Buit Fase T(u), N'),
      'bt2_carrega_rs' : fields.float('Càrrega Fase R(w), S(v)'),
      'bt2_carrega_rt' : fields.float('Càrrega Fase R(w), T(u)'),
      'bt2_carrega_st' : fields.float('Càrrega Fase S(v), T(u)'),
      'bt2_carrega_rn' : fields.float('Càrrega Fase R(w), N'),
      'bt2_carrega_sn' : fields.float('Càrrega Fase S(v), N'),
      'bt2_carrega_tn' : fields.float('Càrrega Fase T(u), N'),
      'bt2_intensitat_r' : fields.float('Intensitat Fase R(w)'),
      'bt2_intensitat_s' : fields.float('Intensitat Fase S(v)'),
      'bt2_intensitat_t' : fields.float('Intensitat Fase T(u)'),
      'bt2_intensitat_n' : fields.float('Intensitat Neutre'),
      'bt1_buit_rs' : fields.float('Buit Fase R(w), S(v)'),
      'bt1_buit_rt' : fields.float('Buit Fase R(w), T(u)'),
      'bt1_buit_st' : fields.float('Buit Fase S(v), T(u)'),
      'bt1_buit_rn' : fields.float('Buit Fase R(w), N'),
      'bt1_buit_sn' : fields.float('Buit Fase S(v), N'),
      'bt1_buit_tn' : fields.float('Buit Fase T(u), N'),
      'bt1_carrega_rs' : fields.float('Càrrega Fase R(w), S(v)'),
      'bt1_carrega_rt' : fields.float('Càrrega Fase R(w), T(u)'),
      'bt1_carrega_st' : fields.float('Càrrega Fase S(v), T(u)'),
      'bt1_carrega_rn' : fields.float('Càrrega Fase R(w), N'),
      'bt1_carrega_sn' : fields.float('Càrrega Fase S(v), N'),
      'bt1_carrega_tn' : fields.float('Càrrega Fase T(u), N'),
      'bt1_intensitat_r' : fields.float('Intensitat Fase R(w)'),
      'bt1_intensitat_s' : fields.float('Intensitat Fase S(v)'),
      'bt1_intensitat_t' : fields.float('Intensitat Fase T(u)'),
      'bt1_intensitat_n' : fields.float('Intensitat Neutre'),
      'bt1_resum_fases' : fields.float('Resum Tensió entre Fases'),
      'bt1_resum_neutre' : fields.float('Resum Tensió entre Fase i Neutre'),
      'bt2_resum_fases' : fields.float('Resum Tensió entre Fases'),
      'bt2_resum_neutre' : fields.float('Resum Tensió entre Fase i Neutre'),
      'intensitat_max_bt1' :fields.float('Intesitat Màxima BT'),
      'intensitat_inici_bt1' : fields.float('Intensitat de disparo'),
      'observacions' : fields.text('Observacions'),
      'buit' : fields.float('Buit'),
      'carrega' : fields.float('Càrrega'),
      'intensitat' : fields.float('Intensitat'),
      'intensitat_max_b1' : fields.float('Intensitat Mâxima BT'),
      'potencia_max_bt1': fields.float('Potencia Màxima BT'),
      'potencia_max_bt2': fields.float('Potencia Màxima BT'),
    }

    _defaults = {
    }

    _order = "name, id"

giscedata_trafo_mesures()

class giscedata_connexio_situacio(osv.osv):
    _name = 'giscedata.transformador.connexio.situacio'
    _descripcio = 'Situacio del la connexio al transformador'
    _columns = {
      'name': fields.char('Nom', size=60, required=True),
      'descripcio': fields.text('Descripcio'),
    }

    _defaults = {
    }

    _order = "name, id"

giscedata_connexio_situacio()


class giscedata_connexio(osv.osv):
    _name = 'giscedata.transformador.connexio'
    _description = 'Conexio del transformador'
    _columns = {
      'trafo_id': fields.many2one('giscedata.transformador.trafo', 'Transformador'),
      'name': fields.char('Núm.', required=True, size=3),
      'tensio_primari': fields.integer('(V) P1'),
      'tensio_b2': fields.integer('(V) B2'),
      'amp_nom_primari': fields.float('(A) P1'),
      'amp_nom_b2': fields.float('(A) B2'),
      'conectada': fields.boolean('Connectada', readonly=True),
      'dataconexio': fields.date('Data Connexió'),
      'tensio_b3': fields.integer('(V) B3'),
      'amp_nom_b3': fields.float('(A) B3'),
      'tensio_b1':  fields.integer('(V) B1'),
      'amp_nom_b1': fields.float('(A) B1'),
      'tensio_p2':  fields.integer('(V) P2'),
      'connectada_p2': fields.boolean('P2 Con'),
      'amp_nom_p2': fields.float('(A) P2'),
      'id_situacio':  fields.many2one('giscedata.transformador.connexio.situacio', 'Situacio'),
    }

    _defaults = {
    }

    _order = "name, id"

giscedata_connexio()

"""
class giscedata_transformador_localitzacio(osv.osv):
        _name = "giscedata.transformador.localitzacio"
        _description = "Localització d'un Transformador"

        _columns = {
                'name': fields.char('Tipus de localització', size=60),
        }

giscedata_transformador_localitzacio()
"""

class GiscedataTransformadorHistoric(osv.osv):
    _name = "giscedata.transformador.historic"
    _description = "Històric dels transformadors"

    def _get_localitzacions(self, cr, uid, context={}):
        obj = self.pool.get('giscedata.transformador.localitzacio')
        ids = obj.search(cr, uid, [])
        res = obj.read(cr, uid, ids, ['name', 'id'], context)
        res = [(r['id'], r['name']) for r in res]
        return res

    def _get_origen_name(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for historic in self.browse(cr, uid, ids):
            if historic.origen is not False and historic.origen != 0:
                try:
                    vals = self.pool.get(historic.tipus_origen.relacio.model).read(\
                    cr, uid, historic.origen, ['name'])
                    res[historic.id] = vals['name']
                except:
                    res[historic.id] = ''
            else:
                res[historic.id] = ''
        return res

    def _get_desti_name(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for historic in self.browse(cr, uid, ids):
            if historic.name is not False and historic.name != 0:
                try:
                    vals = self.pool.get(historic.tipus_desti.relacio.model).read(cr,\
                    uid, historic.name, ['name'])
                    res[historic.id] = vals['name']
                except:
                    res[historic.id] = ''
            else:
                res[historic.id] = ''
        return res

    def _get_next_date(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for historic in self.browse(cr, uid, ids):
            data = None
            if historic.transformador_id:
                cr.execute("select h2.data_sortida from giscedata_transformador_historic h2 where h2.transformador_id = %s and h2.data_sortida > '%s' order by h2.data_sortida asc limit 1" % (int(historic.transformador_id), historic.data_entrada))
                data = cr.fetchone()
            if data != None and len(data) > 0:
                res[historic.id] = data[0]
            else:
                res[historic.id] = ''
        return res

    _columns = {
        'name': fields.integer('Destí'),
        'origen': fields.integer('Origen'),
        'tipus_origen': fields.many2one('giscedata.transformador.localitzacio', 'Tipus origen', selection=_get_localitzacions),
        'tipus_desti': fields.many2one('giscedata.transformador.localitzacio', "Tipus destí", selection=_get_localitzacions),
        'data_entrada': fields.datetime('Data entrada', required=True),
        'data_sortida': fields.datetime('Data sortida', required=True),
        'observacions': fields.text('Observacions'),
        'origen_name': fields.function(_get_origen_name, type='char', method=True, string="Origen"),
        'desti_name': fields.function(_get_desti_name, type='char', method=True, string="Destí"),
        'transformador_id': fields.many2one('giscedata.transformador.trafo', 'Transformador'),
        'next_date': fields.function(_get_next_date, type='datetime', method=True, string="Data sortida"),
    }

    _order = "data_sortida asc"

GiscedataTransformadorHistoric()

class giscedata_transformador_incidencia(osv.osv):
    _name = 'giscedata.transformador.incidencia'
    _descripcio = 'Incidencies del Transformador'


    def _ct_emp(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for incidencia in self.browse(cr, uid, ids):
            if incidencia.ct:
                ct_obj = pooler.get_pool(cr.dbname).get('giscedata.cts')
                cts = ct_obj.read(cr, uid, ct_obj.search(cr, uid, [('name', '=', incidencia.ct)]), ['descripcio'])
                if len(cts):
                    res[incidencia.id] = cts[0]['descripcio']
                else:
                    res[incidencia.id] = ''
            else:
                res[incidencia.id] = ''
        return res

    _columns = {
      'name': fields.many2one('giscedata.transformador.trafo', 'Transformador'),
      'ct': fields.char('CT', size=10),
      'ct_emp': fields.function(_ct_emp, type='char', method=True, string='Emplaçament CT', readonly=True),
      'data': fields.datetime('Data Alta'),
      'estat': fields.char('Estat', size=10),
      'observacions': fields.text('Observacions'),
    }

    _defaults = {
    }

    _order = "data, id"

giscedata_transformador_incidencia()
