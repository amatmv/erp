# -*- coding: utf-8 -*-
from osv import osv, fields


class giscedata_cts(osv.osv):
    _name = 'giscedata.cts'
    _inherit = 'giscedata.cts'

    def _potencia_total_r(self, cr, uid, ids, field_name, ar, context):
        res = {}
        """
        for ct in self.browse(cr, uid, ids):
          potencia = 0.0
          for trafo in ct.transformadors:
            if trafo.id_estat:
              if trafo.id_estat.codi == 1 and trafo.reductor == True:
                potencia += trafo.potencia_nominal
          res[ct.id] = potencia
        """

        sql = """
        SELECT ct.id, coalesce(sum(t.potencia_nominal),0)
        FROM giscedata_cts ct
            LEFT JOIN giscedata_transformador_trafo t ON (t.ct = ct.id and t.reductor = True)
            LEFT JOIN giscedata_transformador_estat e ON (t.id_estat = e.id and e.codi = 1)
        WHERE ct.id IN (%s) GROUP BY ct.id;""" % (','.join(map(str, map(int, ids))))
        cr.execute(sql)
        for ct in cr.fetchall():
            res[ct[0]] = ct[1]
        return res

    def _potencia_total(self, cr, uid, ids, field_name, ar, context):
        """
        Calculates the power of the CT counting the trafos that apear
        on the inventary

        :param cr: Database cursor
        :param uid: User id
        :param ids: Afected ids
        :param field_name:
        :param ar: Arguments
        :param context: OpenERP context
        :return: Dict with id:power
        """
        res = dict.fromkeys(ids, 0)

        sql = """
        SELECT ct.id,coalesce(sum(t.potencia_nominal),0)
        FROM giscedata_cts ct
            INNER JOIN giscedata_transformador_trafo t
                ON (t.ct = ct.id and t.reductor = False)
            INNER JOIN giscedata_transformador_estat e
                ON (t.id_estat = e.id)
        WHERE
            ct.id IN (%s)
            AND e.cnmc_inventari
        GROUP BY ct.id;""" % (','.join(map(str, map(int, ids))))
        cr.execute(sql)
        for ct in cr.fetchall():
            res[ct[0]] = ct[1]
        return res

    def _historic_trafos(self, cr, uid, ids, field_name, ar, context):
        res = {}
        # FIXME: No calcula bé els històrics dels transformadors CAS #76711
        for ct in self.browse(cr, uid, ids):
            cr.execute(
                "select h.id from giscedata_transformador_historic h, ir_model m, giscedata_transformador_localitzacio l where h.name = " + str(
                    ct.id) + " and h.tipus_desti = l.id and l.relacio = m.id and m.model = 'giscedata.cts'")
            ids = [x[0] for x in cr.fetchall()]
            res[ct.id] = ids
        return res

    _columns = {
        'mesures_trafos': fields.one2many('giscedata.transformador.mesures',
                                          'ct', 'Mesures'),
        'transformadors': fields.one2many('giscedata.transformador.trafo', 'ct',
                                          'Transformadors'),
        'historic_trafos': fields.function(_historic_trafos, type='one2many',
                                           relation='giscedata.transformador.historic',
                                           method=True),
        'potencia_r': fields.function(
            _potencia_total_r, type='float', method=True,
            string='Potència Total reductors: (kVA)'
        ),
        'potencia': fields.function(_potencia_total, type='float', method=True,
                                    string='Potència Total del CT: (kVA)'),
    }


giscedata_cts()
