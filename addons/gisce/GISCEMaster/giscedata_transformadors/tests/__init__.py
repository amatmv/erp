# coding=utf-8
from destral import testing
from destral.transaction import Transaction

from oopgrade import DataMigration
from addons import get_module_resource


class TestTrafoElement(testing.OOTestCase):

    def test_has_cini_fields(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            trafo_obj = self.openerp.pool.get('giscedata.transformador.trafo')
            fields = trafo_obj.fields_get(cursor, uid)
            self.assertIn('cini', fields)
            self.assertIn('bloquejar_cini', fields)


class TestMigrations(testing.OOTestCase):

    def test_migrate_2_68(self):
        """In this version we have added _data.xml file and most of the
        instances already have this variables set up"""
        with Transaction().start(self.database) as txn:

            cursor = txn.cursor
            uid = txn.user

            # get the original res_id to check after
            imd_obj = self.openerp.pool.get('ir.model.data')
            imd_ids = imd_obj.search(cursor, uid, [
                ('module', '=', 'giscedata_transformadors')
            ])
            xml_ids = {}
            f_read = ['name', 'res_id', 'model']
            for rec in imd_obj.read(cursor, uid, imd_ids, f_read):
                if rec['model'] in (
                     'giscedata.transformador.localitzacio',
                     'giscedata.transformador.estat',
                     'res.partner.category'
                ):
                    xml_ids[rec['name']] = rec['res_id']

            # Remove data created by _data.xml to test that we linked that
            cursor.execute(
                "DELETE FROM ir_model_data WHERE module = %s and model in %s",
                ('giscedata_transformadors',
                 (
                     'giscedata.transformador.localitzacio',
                     'giscedata.transformador.estat',
                     'res.partner.category'
                )))
            data_path = get_module_resource(
                'giscedata_transformadors', 'giscedata_transformadors_data.xml'
            )
            with open(data_path, 'r') as f:
                data_xml = f.read()
            dm = DataMigration(data_xml, cursor, 'giscedata_transformadors', {
                'giscedata.transformador.localitzacio': ['code'],
                'giscedata.transformador.estat': ['codi']
            })
            dm.migrate()

            imd_ids = imd_obj.search(cursor, uid, [
                ('module', '=', 'giscedata_transformadors')
            ])
            xml_migrated_ids = {}
            f_read = ['name', 'res_id', 'model']
            for rec in imd_obj.read(cursor, uid, imd_ids, f_read):
                if rec['model'] in (
                        'giscedata.transformador.localitzacio',
                        'giscedata.transformador.estat',
                        'res.partner.category'
                ):
                    xml_migrated_ids[rec['name']] = rec['res_id']

            self.assertEqual(xml_ids, xml_migrated_ids)
