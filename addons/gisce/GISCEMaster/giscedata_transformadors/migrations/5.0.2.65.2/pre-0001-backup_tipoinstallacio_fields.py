# coding=utf-8
from oopgrade import oopgrade


def up(cursor, installed_version):
    oopgrade.rename_columns(
        cursor, {
            'giscedata_transformador_trafo': [
                ('tipus_instalacio_cnmc_id', 'tipus_instalacio_cnmc_id_pre_v2_65_1')
            ]
        }
    )


def down(cursor, installed_version):
    oopgrade.rename_columns(
        cursor, {
            'giscedata_transformador_trafo': [
                ('tipus_instalacio_cnmc_id_pre_v2_65_1', 'tipus_instalacio_cnmc_id')
            ]
        }
    )

migrate = up
