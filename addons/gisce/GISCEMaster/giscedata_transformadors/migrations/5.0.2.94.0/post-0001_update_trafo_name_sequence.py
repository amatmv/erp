# coding=utf-8
import logging


def up(cursor, installed_version):

    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')

    logger.info('Setting UP the Trafo sequence for the field NAME.')

    sql = """
        UPDATE ir_sequence AS seq 
        SET number_next = data.num
        FROM (
          SELECT coalesce(max(trf.name::int),0)+1 AS num,
                 seq.id AS id
          FROM giscedata_transformador_trafo trf, ir_sequence seq
          WHERE seq.name='Numeració Trafo'
          AND seq.code='giscedata.transformador.trafo'
          GROUP BY seq.id
        ) AS data
        WHERE seq.id=data.id
    """

    cursor.execute(sql)

    logger.info('Trafo NAME sequence set up with the current value')


def down(cursor, installed_version):
    pass


migrate = up