# coding=utf-8
from oopgrade import oopgrade


def up(cursor, installed_version):
    if not installed_version:
        return
    oopgrade.rename_columns(cursor, {
        'giscedata_transformador_trafo': [('cini', 'cini_pre_auto_trafo')]
    })


def down(cursor):
    oopgrade.rename_columns(cursor, {
        'giscedata_transformador_trafo': [('cini_pre_auto_trafo', 'cini')]
    })


migrate = up
