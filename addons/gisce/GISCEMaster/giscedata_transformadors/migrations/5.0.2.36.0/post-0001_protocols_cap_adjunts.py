# -*- coding: utf-8 -*-
from addons.gisceupgrade_migratoor.gisceupgrade_migratoor import log
import pooler
import magic
from base64 import b64decode


def pass_to_attachment(cursor, uid, column, prefix_filename, trafo_obj, attach_obj):
    param_search = [(column, '!=', False)]
    t_ids = trafo_obj.search(cursor, uid, param_search,
                             context={'active_test': False})
    fields_to_read = ['id', 'name', column]
    for trafo_id in t_ids:
        trafo = trafo_obj.read(cursor, uid, trafo_id, fields_to_read)
        type = magic.from_buffer(b64decode(trafo[column])).lower()
        filename = '%s-%s' % (prefix_filename, trafo['name'])
        if type:
            filename += '.%s' % type.split()[0]

        attach_obj.create(cursor, uid,
                          {'name': filename,
                           'datas': trafo[column],
                           'res_id': trafo['id'],
                           'res_model': trafo_obj._name})
        trafo_obj.write(cursor, uid, trafo_id, {column: False})


def migrate(cursor, installed_version):
    log(u'migració de protocols')
    uid = 1
    pool = pooler.get_pool(cursor.dbname)
    trafo_obj = pool.get('giscedata.transformador.trafo')
    attach_obj = pool.get('ir.attachment')
    pass_to_attachment(cursor, uid, 'protocol_img', 'PROTOCOL', trafo_obj, attach_obj)
    pass_to_attachment(cursor, uid, 'placa_img', 'PLACA', trafo_obj, attach_obj)
    pass_to_attachment(cursor, uid, 'trafo_img', 'TRAFO', trafo_obj, attach_obj)


