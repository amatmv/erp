Documentació del mòdul Transformadors
=====================================

===========
Introducció
===========

El mòdul de transformadors ens permet inventariar els Transformadors i els seus
elements més característics

===========================
Crear una nou Transformador
===========================

Per crear un nou Transformador ho podem fer a través del llistat de tots els
Tranformadors i després apretar el botó de **Nou**, igual que es fa amb la
resta de registres de l'ERP.

Ens apareixerà un formulari on podem visualitzar una capçalera i diferents
parts:

  * General
  * Dades Administratives
  * Elements
  * Observacions
  * Conexions
  * Històric

En l'apartat de **Dades Administratives** tenim els camps:

  .. image:: _static/formulari_administratiu.png

  * **Propietari (check)**: Per indicar si som propietaris o no.
  * **Propietari**: Qui és el propietari del Transformador, habitualment
    nosaltres mateixos.
  * **% pagat per la compañia**: Per indicar quin percentatge ha pagat l'empresa
    per aquest element.
  * **Data de Compra**: Data en la qual es va adquirir el transformador
  * **Número de comanda**: Referència al número de comanda de compra del
    transformador
  * **Bloquejar APM**: Permet fixar a una data concreta la *Data APM*. D'aquesta
    forma es permet posar-hi una data fixa i que no es sobreescrigui
    automàticament tal com s'explica a *Data APM*
  * **Data APM**: En quina data es va posar en marxa. Aquest camp
    s'actualitza sol segons l'històric de moviments. S'agafa la data del
    moviment més antic de l'històric on el destí és un Centre Transformador (CT)
    si no es marca el camp *Bloquejar APM*. En aquest cas es pot posar una data
    arbritària.
  * **Actiu**: Per marcar si aquest transformador està actiu o no. En el cas que
    no estigui actiu, no ens apareixerà al llistat a no ser que li diguem
    explícitament que volem veure els transformadors no actius.
  * **Baixa**: Indica si el transformador està de baixa.
  * **Data de baixa**: Per indicar en quina data es va donar de baixa el
    transformador. Aquest camp només és visible quan el camp *Baixa* està activat.
  * **Obres**: Amb quines obres relacionem aquest CT.