<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="transformador-list"/>
  </xsl:template>

  <xsl:template match="transformador-list">
    <document>
      <template>
        <pageTemplate id="main">
          <frame id="first" x1="1cm" y1="1cm" width="19cm" height="277mm"/>
        </pageTemplate>
      </template>
      
      <stylesheet>

        <paraStyle name="titol"
					fontName="Helvetica"
					fontSize="12"
          leading="32"
				/>

        <blockTableStyle id="taula_contingut">
          <lineStyle kind="GRID" colorName="silver"/>
          <blockFont name="Helvetica" size="7" />
		  <blockBackground colorName="grey" start="0,0" stop="9,0" />
		  <blockFont name="Helvetica-Bold" size="7" start="0,0" stop="9,0" />
          <blockAlignment value="RIGHT" start="3,1" />
        </blockTableStyle>

        <blockTableStyle id="taula2">
          <blockFont name="Helvetica-Bold" size="7" />
          <blockAlignment value="RIGHT" start="3,0" />
          <blockAlignment value="LEFT" start="4,0" />
        </blockTableStyle>

      </stylesheet>
    
      <story>
      <para style="titol" t="1">Llistat de transformadors</para>

      <blockTable style="taula_contingut" colWidths="1cm,2.5cm,5.8cm,1cm,1.6cm,1.6cm,1.6cm,1.5cm,1.5cm,2.2cm">
       <tr>
          <td t="1">N�</td>
          <td t="1">Marca</td>
          <td t="1">Centre Transformador</td>
          <td t="1">Pot</td>
          <td t="1">B1</td>
          <td t="1">B2</td>
          <td t="1">B3</td>
          <td t="1">UNE</td>
          <td t="1">Tipus</td>
          <td t="1">Estat</td>
        </tr>
        <xsl:apply-templates select="transformador" mode="story"/>
      </blockTable>
      <blockTable style="taula2" colWidths="1cm,2.5cm,5.8cm,1cm,1.6cm,1.6cm,1.6cm,1.5cm,1.5cm,2.2cm">
        <tr>
          <td><xsl:value-of select="count(transformador)" /></td>
          <td></td>
          <td t="1">Pot�ncia total:</td>
          <td><xsl:value-of select="sum(transformador/potencia)" /></td>
          <td>kVA</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
      </blockTable>
      </story>
    </document>
  </xsl:template>
  <xsl:template match="transformador" mode="story">
    <tr>
      <td><xsl:value-of select="numero"/></td>
      <td><xsl:value-of select="marca"/></td>
      <td><xsl:value-of select="concat(ct, ' ', descripcio)"/></td>
      <td><xsl:value-of select="potencia"/></td>
      <td><xsl:value-of select="b1"/></td>
      <td><xsl:value-of select="b2"/></td>
      <td><xsl:value-of select="b3"/></td>
      <td><xsl:value-of select="une" /></td>
      <td><xsl:value-of select="tipus"/></td>
      <td><xsl:value-of select="estat"/></td>
    </tr>
  </xsl:template>
</xsl:stylesheet>
