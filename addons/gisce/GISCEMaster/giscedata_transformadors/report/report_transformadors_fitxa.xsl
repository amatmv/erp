<?xml version="1.0" encoding="iso-8859-15"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="transformador-list"/>
  </xsl:template>

  <xsl:template match="transformador-list">
    <document>
      <template pageSize="(297mm,19cm)" topMargin="0cm" bottomMargin="0cm" rightMargin="0cm">
        <pageTemplate id="main">
          <frame id="first" x1="1cm" y1="0cm" width="277mm" height="19cm"/>
        </pageTemplate>
      </template>

      <stylesheet>

        <paraStyle name="titol"
					fontName="Helvetica"
					fontSize="12"
          leading="16"
				/>

        <paraStyle name="titol2"
	  fontName="Helvetica-Bold"
	  fontSize="10"
          spaceBefore="0.1cm"
          leading="14"
				/>
				
		<paraStyle name="contingut"
			fontname="Helvetica"
			fontSize="8"
		 />

        <blockTableStyle id="taula1">
          <blockFont name="Helvetica" size="8" />
          <blockFont name="Helvetica-Bold" size="8" start="0,0" stop="0,14" />
          <blockFont name="Helvetica-Bold" size="8" start="2,0" stop="2,14" />
          <lineStyle kind="LINEBELOW" colorName="silver"/>
        </blockTableStyle>

        <blockTableStyle id="taula2">
          <blockFont name="Helvetica" size="8" />
          <blockFont name="Helvetica-Bold" size="8" start="0,0" stop="6,0" />
        </blockTableStyle>

        <blockTableStyle id="taula_contingut">
          <lineStyle kind="GRID" colorName="silver"/>
          <blockFont name="Helvetica" size="8" />
					<blockBackground colorName="grey" start="0,0" stop="13,0" />
					<blockFont name="Helvetica-Bold" size="8" start="0,0" stop="13,0" />

        </blockTableStyle>

      </stylesheet>
    
      <story>
        <xsl:apply-templates select="transformador" mode="story"/>

      </story>

    </document>

  </xsl:template>

  <xsl:template match="transformador" mode="story">
      <para style="titol"><u>Transformador <xsl:value-of select="numero" /></u></para>

      <blockTable style="taula1" colWidths="4.5cm,6cm,4.5cm,6cm">

        <tr>
          <td t="1">N�mero:</td><td><xsl:value-of select="numero"/></td>
          <td t="1">CT:</td><td><xsl:value-of select="ct" /></td>
        </tr>
        <tr>
          <td t="1">Marca: </td><td><xsl:value-of select="marca" /></td>
          <td t="1">Pot�ncia Nominal (kVA):</td><td><xsl:value-of select="potencia" /></td>
        </tr>
        <tr>
          <td t="1">Tipus: </td><td><xsl:value-of select="tipo" /></td>
          <td t="1">Escalfament (�C): </td><td><xsl:value-of select="escalfament" /></td>
        </tr>
        <tr>
          <td t="1">Norma UNE: </td><td><xsl:value-of select="normaune" /></td>
          <td t="1">Tensi� cc a 75�C (%) (220): </td><td><xsl:value-of select="tensiocc" /></td>
        </tr>
        <tr>
          <td t="1">Grup connexi� UNE: </td><td><xsl:value-of select="id_connexioune" /></td>
          <td t="1">Tensi� cc a 75�C (%) (380): </td><td><xsl:value-of select="tensiocc380" /></td>
        </tr>  
        <tr>
          <td t="1">Ordre protocol: </td><td><xsl:value-of select="ordre_protocol" /></td>
          <td t="1">Nivell d'a�llament AT: </td><td><xsl:value-of select="nivell_aillament_at" /></td>
        </tr>
        <tr>
          <td t="1">N�mero fabricaci�: </td><td><xsl:value-of select="numero_fabricacio" /></td>
          <td t="1">Nivell de soroll dB (A): </td><td><xsl:value-of select="soroll" /></td>
        </tr> 
        <tr>
          <td t="1">Any fabricaci�: </td><td><xsl:value-of select="any_fabricacio" /></td>
          <td t="1">Nivell d'a�llament BT: </td><td><xsl:value-of select="nivell_aillament_bt" /></td>
        </tr> 
        <tr>
          <td t="1">Pes total (Kg): </td><td><xsl:value-of select="pes_total" /></td>
          <td t="1">Material AT: </td><td><xsl:value-of select="material_at" /></td>
        </tr>
        <tr>
          <td t="1">Pes l�quid a�llant (Kg): </td><td><xsl:value-of select="pes_oli" /></td>
          <td t="1">Material BT: </td><td><xsl:value-of select="material_bt" /></td>
        </tr> 
        <tr>
          <td t="1">Pes a desencubar (Kg): </td><td><xsl:value-of select="pes_desencubar" /></td>
          <td t="1">L�quid a�llant: </td><td><xsl:value-of select="id_refrigerant" /></td>
        </tr> 
        <tr>
          <td t="1">Amplada: </td><td><xsl:value-of select="amplada" /></td>
          <td t="1">Al�ada: </td><td><xsl:value-of select="alcada" /></td>
        </tr>
        <tr>
          <td t="1">Estat actual: </td><td><xsl:value-of select="estat" /></td>
          <td t="1">Fondaria: </td><td><xsl:value-of select="fondaria" /></td>
        </tr>
        <tr>
          <td t="1">Constant K:</td><td><xsl:value-of select="k" /></td>
          <td t="1">Calibre TI B1:</td><td><xsl:value-of select="calibre_ti_b1" /></td>
        </tr>
        <tr>
          <td t="1">Ompliment integral:</td><td><xsl:value-of select="ompliment_integral" /></td>
          <td t="1">Calibre TI B2:</td><td><xsl:value-of select="calibre_ti_b2" /></td>
        </tr>
      </blockTable>
      <blockTable style="taula2" colWidths="4cm,3cm,3cm,4cm,3cm,4cm,3cm">
        <tr>
          <td t="1">Exterior/Interior</td>
          <td t="1">Rodes</td>
          <td t="1">Nivell</td>
          <td t="1">Dip. Expansi�</td>
          <td t="1">Aixeta</td>
          <td t="1">Term�metre</td>
          <td t="1">Placa</td>
        </tr>
        <tr>
          <td><xsl:value-of select="tipus" /></td>
          <td><xsl:choose><xsl:when test="rodes=1">S�</xsl:when><xsl:otherwise>No</xsl:otherwise></xsl:choose></td>
          <td><xsl:choose><xsl:when test="nivell=1">S�</xsl:when><xsl:otherwise>No</xsl:otherwise></xsl:choose></td>
          <td><xsl:choose><xsl:when test="dip_expensio=1">S�</xsl:when><xsl:otherwise>No</xsl:otherwise></xsl:choose></td>
          <td><xsl:choose><xsl:when test="aixeta=1">S�</xsl:when><xsl:otherwise>No</xsl:otherwise></xsl:choose></td>
          <td><xsl:choose><xsl:when test="termometre=1">S�</xsl:when><xsl:otherwise>No</xsl:otherwise></xsl:choose></td>
          <td><xsl:choose><xsl:when test="placa=1">S�</xsl:when><xsl:otherwise>No</xsl:otherwise></xsl:choose></td>
        </tr>
      </blockTable>
      <para style="titol2" t="1">Connexions:</para>
      <xsl:apply-templates select="connexions-list" mode="story"/>
      <para style="titol2" t="1">Observacions:</para>
      <para style="contingut"><xsl:value-of select="observacions" /></para>
      <nextFrame />
  </xsl:template>

  <xsl:template match="connexions-list" mode="story">
    <blockTable style="taula_contingut" colWidths="0.8cm,4cm,2.5cm,2cm,1.4cm,2cm,1.4cm,2cm,1.4cm,2cm,1.4cm,2cm,1.4cm,3cm">
       <tr>
          <td t="1">N�</td>
          <td t="1">Situacio</td>
          <td t="1">Connectada</td>
          <td t="1">Tensi� P.</td>
          <td t="1">Int.</td>
          <td t="1">Tensi� P2</td>
          <td t="1">Int P2</td>
          <td t="1">Tensi� B1</td>
          <td t="1">Int B1</td>
          <td t="1">Tensi� B2</td>
          <td t="1">Int B2</td>
          <td t="1">Tensi� B3</td>
          <td t="1">Int B3</td>
          <td t="1">Data Con.</td>
        </tr>
        <xsl:apply-templates select="connexio" mode="story"/>
    </blockTable>
  </xsl:template>

  <xsl:template match="connexio" mode="story">
    <tr>
        <td><xsl:value-of select="connexio-name"/></td>
        <td><xsl:value-of select="connexio-situacio"/></td>
        <xsl:if test="connexio-connectada=1">
        <td>S� <xsl:if test="connectada_p2!=1">(1)</xsl:if><xsl:if test="connectada_p2=1">(2)</xsl:if></td>
        </xsl:if>
        <xsl:if test="connexio-connectada!=1">
        <td>No</td>
        </xsl:if>
        <td><xsl:value-of select="connexio-tp"/></td>
        <td><xsl:value-of select="connexio-int"/></td>
        <td><xsl:value-of select="connexio-p2"/></td>
        <td><xsl:value-of select="connexio-int2"/></td>
        <td><xsl:value-of select="connexio-tensiob1"/></td>
        <td><xsl:value-of select="connexio-ib1"/></td>
        <td><xsl:value-of select="connexio-tensiob2"/></td>
        <td><xsl:value-of select="connexio-ib2"/></td>
        <td><xsl:value-of select="connexio-tensiob3"/></td>
        <td><xsl:value-of select="connexio-ib3"/></td>
        <td><xsl:value-of select="connexio-data"/></td>
    </tr>
  </xsl:template>

</xsl:stylesheet>

