# -*- coding: utf-8 -*-

from osv import osv, fields
from datetime import datetime, timedelta


class SwitchTrafo(osv.osv_memory):
    _name = 'giscedata.switch.trafo'

    def switch_trafo(self, cursor, uid, ids, context=None):
        """
        Method to switch trafo

        :param cursor: Datbase cursor
        :param uid: User id
        :type uid: int
        :param ids: Wizard id
        :type ids: list(int), int
        :param context: OpenERP context
        :type context: dict
        :rtype: bool
        """

        trf_obj = self.pool.get("giscedata.transformador.trafo")
        loc_obj = self.pool.get("giscedata.transformador.localitzacio")
        mag_obj = self.pool.get("giscedata.transformador.magatzem")
        cts_obj = self.pool.get("giscedata.cts")

        if context is None:
            context = {}

        if ids:
            context['no_bbox_search'] = True
            id_mag = mag_obj.search(
                cursor,
                uid,
                [],
                context=context
            )[0]

            id_loc_mag = loc_obj.search(cursor, uid, [("code", "=", "4")])[0]
            id_loc_ct = loc_obj.search(cursor, uid, [("code", "=", "1")])[0]

            wizard = self.browse(cursor, uid, ids[0], context=context)
            trafo_entrada = wizard.trafo_entrada
            trafo_sortida = wizard.trafo_sortida
            data = wizard.data_funcionament
            data_entrada = datetime.strptime(
                data, '%Y-%m-%d %H:%M:%S'
            ) + timedelta(hours=1)
            data_sortida = datetime.strptime(data, '%Y-%m-%d %H:%M:%S')
            ct = wizard.trafo_sortida.ct

            copy_fields = ["id_estat"]
            if 'trafo_copy_fields' in context:
                copy_fields += context.pop('trafo_copy_fields')

            copy_data = trf_obj.read(
                cursor, uid, trafo_sortida.id, copy_fields,
                context=context, load="_classic_write"
            )
            copy_data.pop("id")

            estat_mag = trf_obj.read(
                cursor, uid, trafo_entrada.id, copy_fields,
                context=context, load="_classic_write"
            )['id_estat']

            trf_obj.moure(
                cursor, uid, trafo_sortida.id, id_loc_mag, id_mag, data_entrada,
                data_sortida, context=context
            )
            trf_obj.moure(
                cursor, uid, trafo_entrada.id, id_loc_ct, ct.id, data_entrada,
                data_sortida, context=context
            )

            trf_obj.write(
                cursor, uid, trafo_entrada.id, copy_data, context=context
            )
            trf_obj.write(
                cursor, uid, trafo_sortida.id,
                {'geom': None, 'node_id': None, 'id_estat': estat_mag},
                context=context
            )

            info = (
                "El Trafo {}, ha estat intercanviat per el Trafo {}.".format(
                    wizard.trafo_sortida.name,
                    wizard.trafo_entrada.name

                )
            )
            wizard.write({"state": "end", "log": info})
            if not ct.bloquejar_cini and not ct.bloquejar_cnmc_tipus_install:
                cts_obj.write(
                    cursor, uid, [ct.id],
                    {
                        'bloquejar_cini': True,
                        'bloquejar_cnmc_tipus_install': True
                    }
                )
                cts_obj.write(
                    cursor, uid, [ct.id],
                    {
                        'bloquejar_cini': False,
                        'bloquejar_cnmc_tipus_install': False
                    }
                )
            elif not ct.bloquejar_cini:
                cts_obj.write(cursor, uid, [ct.id], {'bloquejar_cini': True})
                cts_obj.write(cursor, uid, [ct.id], {'bloquejar_cini': False})
            elif not ct.bloquejar_cnmc_tipus_install:
                cts_obj.write(
                    cursor, uid, [ct.id], {'bloquejar_cnmc_tipus_install': True}
                )
                cts_obj.write(
                    cursor, uid, [ct.id],
                    {'bloquejar_cnmc_tipus_install': False}
                )
            context['no_bbox_search'] = False

        return True

    def _default_trafo_sortida(self, cursor, uid, context=None):
        """
        Calculates the default value of trafo_entrada

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param context: OpenERP context
        :type context: dict
        :rtype: None
        :return: None
        """
        if context is None:
            context = {}
        return context.get("active_id")

    def _default_localitzacio_code(self, cursor, uid, context=None):
        """
        Calculates the defautl value of field funtcion localitzacio_code

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param context: OpenERP context
        :type context: dict

        :rtype: str
        """

        if context is None:
            context = {}
        trf_ojb = self.pool.get("giscedata.transformador.trafo")
        ident = context.get("active_id")
        trf = trf_ojb.browse(cursor, uid, ident)
        return str(trf.localitzacio.code)

    def _ff_localitzacio_code(self, cursor, uid, ids, name, args, context=None):
        """
        Fields function calculator for localitzacio_codi

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: Transformadors ids
        :type ids: list(int), int
        :param name:
        :type name:
        :param args:
        :type args:
        :param context: OpenERP context
        :type context: ditc
        :rtype: dict
        """
        ret = dict.fromkeys(ids, '')

        return ret

    _columns = {
        'state': fields.selection(
            [('init', 'Init'), ('end', 'End'), ], 'State'
        ),
        "trafo_sortida": fields.many2one(
            "giscedata.transformador.trafo",
            "Transformador a retirar",
            required=True
        ),
        "trafo_entrada": fields.many2one(
            "giscedata.transformador.trafo",
            "Transformador a instal·lar",
            required=True
        ),
        "data_funcionament": fields.datetime(
            "Data posada en funcionament",
            required=True
        ),
        'localitzacio_code': fields.function(
            _ff_localitzacio_code,
            string="Codi localitzacio",
            type='char',
            method=True
        ),
        "log": fields.text(
            "Missatge",
            readonly=True,
        )
    }

    _defaults = {
        'state': lambda *a: 'init',
        'trafo_sortida': _default_trafo_sortida,
        "data_funcionament": lambda *a: str(datetime.now()),
        'localitzacio_code': _default_localitzacio_code
    }


SwitchTrafo()
