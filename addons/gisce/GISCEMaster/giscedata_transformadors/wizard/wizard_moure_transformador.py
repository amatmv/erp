# -*- coding: iso-8859-1 -*-
import wizard
import pooler
from datetime import *

_moure_transformador_form = """<?xml version="1.0"?>
<form string="Informacio de sortida">
  <field name="data_sortida" required="1"/>
  <newline />
  <field name="tipus_origen" widget="selection" readonly="1"/>
</form>"""

def _get_localitzacions(self, cr, uid, context={}):
    obj = pooler.get_pool(cr.dbname).get('giscedata.transformador.localitzacio')
    ids = obj.search(cr, uid, [])
    res = obj.read(cr, uid, ids, ['name', 'id'], context)
    res = [(r['id'], r['name']) for r in res]
    return res


_moure_transformador_fields = {
  'data_sortida' : {'string': "Data i Hora de sortida", 'type': 'datetime' },
  'tipus_origen': {'string': "Tipus d'origen", 'type': 'many2one', 'relation':'giscedata.transformador.localitzacio', 'selection': _get_localitzacions},
  'origen': {'string': 'Origen', 'type': 'integer'},
}

_pas2_form = """<?xml version="1.0"?>
<form string="Informacio d'entrada">
  <field name="data_entrada" required="1"/>
  <newline />
  <field name="tipus_desti" widget="selection"/>
</form>"""

_pas2_fields = {
  'data_entrada' : {'string': "Data i Hora d'entrada", 'type': 'datetime' },
  'tipus_desti': {'string': "Destí", 'type': 'many2one', 'relation':'giscedata.transformador.localitzacio', 'selection': _get_localitzacions},
}

_finalitzar_form = """<?xml version="1.0"?>
<form string="Observacions del moviment" col="2">
        <separator string="Observacions" colspan="2" />
  <field name="observacions" nolabel="1" colspan="2" />
</form>"""

_finalitzar_fields = {
  'observacions' : {'string': "Observacions", 'type': 'text' },
}

_detalls_desti_form = """<?xml version="1.0"?>
<form string="Detalls del destí">
  <field name="name" />
</form>"""

_detalls_desti_fields = {}

_demanar_estat_form = """<?xml version="1.0"?>
<form string="Informacio d'entrada">
  <field name="tipus_desti" readonly="1" colspan="4"/>
  <field name="id_estat" domain="[('localitzacio.id', '=', tipus_desti)]"
  colspan="4"/>
</form>"""

_demanar_estat_fields = {
  'id_estat': {'string': "Estat", 'type': 'many2one', 'relation':
  'giscedata.transformador.estat'},
  'tipus_desti': {'string': "Destí", 'type': 'many2one', 'relation':
  'giscedata.transformador.localitzacio'},
}

_demanar_posicio_form = """<?xml version="1.0"?>
<form string="Informacio d'entrada">
  <field name="ordre_dins_ct" required="1"/>
</form>"""

_demanar_posicio_fields = {
  'ordre_dins_ct': {'string': "Ordre dins el ct", 'type': 'integer'},
}

def _init_moure(self, cr, uid, data, context={}):
    d = None
    trafo_obj = pooler.get_pool(cr.dbname).get('giscedata.transformador.trafo').browse(cr, uid, data['id'])
    if trafo_obj.localitzacio.id != False:
        cr.execute("SELECT data_entrada,name FROM giscedata_transformador_historic WHERE tipus_desti = "+str(trafo_obj.localitzacio.id)+" AND transformador_id = "+str(data['id'])+" ORDER BY data_entrada DESC LIMIT 1")
        d = cr.fetchone() or None
    if d != None:
        dsortida = d[0]
        origen = d[1]
    else:
        dsortida = (datetime.now()).strftime('%Y-%m-%d %H:%M:%S')
        origen = 0
    if not origen:
        origen = False
    return {'data_sortida': dsortida,
                                  'origen': origen,
                                  'tipus_origen': trafo_obj.localitzacio.id}

def _pas2(self, cr, uid, data, context={}):
    return {'data_entrada': data['form']['data_sortida']}


def _detalls_desti(self, cr, uid, data, context={}):
    domain = []
    td_obj = \
    pooler.get_pool(cr.dbname).get('giscedata.transformador.localitzacio').browse(cr,\
    uid, data['form']['tipus_desti'])
    if td_obj.relacio.model == 'res.partner':
        p_ids = pooler.get_pool(cr.dbname).get('res.partner').search(cr, uid, [('category_id', '=', 'MOV_TRAFOS')])
        if len(p_ids):
            domain = [('category_id', '=', 'MOV_TRAFOS')]
    # Hack per modificar el formulari dins de l'accio del wizard
    self.states['detalls_desti']['result']['fields'] = \
    {'name' : {'string': td_obj.relacio.name, 'required': True,\
    'type': 'many2one', 'relation': td_obj.relacio.model, 'domain': domain}}

    return {}

def _estats(self, cr, uid, data, context={}):
    td_obj = \
    pooler.get_pool(cr.dbname).get('giscedata.transformador.localitzacio').browse(cr,\
    uid, data['form']['tipus_desti'])
    if len(td_obj.estats) > 0:
        data['localitzacio'] = data['form']['tipus_desti']
        return 'demanar_estat'
    else:
        return 'posicio'

def _posicio(self, cr, uid, data, context={}):
    td_obj = \
    pooler.get_pool(cr.dbname).get('giscedata.transformador.localitzacio').browse(cr,\
    uid, data['form']['tipus_desti'])
    if td_obj.relacio.model == 'giscedata.cts':
        return 'demana_posició'
    else:
        return 'finalitzar'


def _guardar(self, cr, uid, data, context=None):
    if context is None:
        context = {}
    historic_obj = pooler.get_pool(cr.dbname).get(
        'giscedata.transformador.historic'
    )
    trafos_obj = pooler.get_pool(cr.dbname).get('giscedata.transformador.trafo')
    cts_obj = pooler.get_pool(cr.dbname).get('giscedata.cts')
    vals = dict(data['form'])
    vals['transformador_id'] = data['id']
    if 'id_estat' in data['form']:
        del vals['id_estat']
        id_estat = data['form']['id_estat']
    else:
        id_estat = None
    if 'ordre_dins_ct' in data['form']:
        del vals['ordre_dins_ct']
    ct_vals = {
        'localitzacio': data['form']['tipus_desti'], 'id_estat': id_estat,
    }
    historic_obj.create(cr, uid, vals, context)
    td_obj = pooler.get_pool(cr.dbname).get(
        'giscedata.transformador.localitzacio'
    ).browse(cr, uid, data['form']['tipus_desti'])
    bloq_cini = True
    bloq_ti = True
    ct_id = None
    if td_obj.relacio.model != 'giscedata.cts':
        trafo_data = trafos_obj.read(cr, uid, data['id'], ['ct'])
        if 'ct' in trafo_data and trafo_data['ct']:
            ct_id = trafo_data['ct'][0]
            ct_data = cts_obj.read(
                cr, uid, ct_id,
                ['bloquejar_cini', 'bloquejar_cnmc_tipus_install']
            )
            bloq_cini = ct_data['bloquejar_cini']
            bloq_ti = ct_data['bloquejar_cnmc_tipus_install']
        else:
            bloq_cini = True
        ct_vals['ct'] = None
        trafo_fields = trafos_obj.fields_get(cr, uid)
        if 'geom' in trafo_fields and 'node_id' in trafo_fields:
            ct_vals['geom'] = False
            ct_vals['node_id'] = False
    elif td_obj.relacio.model == 'giscedata.cts':
        ct_vals['ct'] = data['form']['name']
        ct_id = ct_vals['ct']
        ct_vals['ordre_dins_ct'] = data['form']['ordre_dins_ct']
        ct_data = cts_obj.read(
            cr, uid, ct_id, ['bloquejar_cini', 'bloquejar_cnmc_tipus_install']
        )
        bloq_cini = ct_data['bloquejar_cini']
        bloq_ti = ct_data['bloquejar_cnmc_tipus_install']
    trafos_obj.write(cr, uid, [data['id']], ct_vals)
    if not bloq_cini and not bloq_ti:
        cts_obj.write(
            cr, uid, [ct_id],
            {'bloquejar_cini': True, 'bloquejar_cnmc_tipus_install': True}
        )
        cts_obj.write(
            cr, uid, [ct_id],
            {'bloquejar_cini': False, 'bloquejar_cnmc_tipus_install': False}
        )
    elif not bloq_cini:
        cts_obj.write(
            cr, uid, [ct_id],
            {'bloquejar_cini': True}
        )
        cts_obj.write(
            cr, uid, [ct_id],
            {'bloquejar_cini': False}
        )
    elif not bloq_ti:
        cts_obj.write(
            cr, uid, [ct_id],
            {'bloquejar_cnmc_tipus_install': True}
        )
        cts_obj.write(
            cr, uid, [ct_id],
            {'bloquejar_cnmc_tipus_install': False}
        )
    return {}


class wizard_mouretrafo(wizard.interface):

    states = {
      'init': {
        'actions': [_init_moure],
        'result': {'type': 'form', 'arch': _moure_transformador_form,'fields': _moure_transformador_fields, 'state':[('end', 'Cancelar', 'gtk-cancel'), ('pas2', 'Continuar', 'gtk-go-forward')]}
      },
      'pas2': {
          'actions': [_pas2],
        'result': {'type': 'form', 'arch': _pas2_form,'fields': _pas2_fields, 'state':[('end', 'Cancelar', 'gtk-cancel'), ('init', 'Enrere', 'gtk-go-back'), ('detalls_desti', 'Continuar', 'gtk-go-forward')]}
      },
      'detalls_desti': {
          'actions': [_detalls_desti],
        'result': {'type': 'form', 'arch': _detalls_desti_form,'fields':
        _detalls_desti_fields, 'state':[('end', 'Cancelar', 'gtk-cancel'),
        ('pas2', 'Enrere', 'gtk-go-back'), ('estats', 'Continuar', 'gtk-go-forward')]}
      },
      'estats': {
        'actions': [],
        'result': {'type': 'choice', 'next_state': _estats}
      },
      'demanar_estat': {
          'actions': [],
        'result': {'type': 'form', 'arch': _demanar_estat_form,'fields':
        _demanar_estat_fields, 'state':[('end', 'Cancelar', 'gtk-cancel'),
        ('detalls_desti', 'Enrere', 'gtk-go-back'), ('posicio', 'Continuar', 'gtk-go-forward')]}
      },
      'posicio': {
        'actions': [],
        'result': {'type': 'choice', 'next_state': _posicio}
      },
      'demanar_estat': {
          'actions': [],
        'result': {'type': 'form', 'arch': _demanar_posicio_form,'fields':
        _demanar_posicio_fields, 'state':[('end', 'Cancelar', 'gtk-cancel'),
        ('demanar_estat', 'Enrere', 'gtk-go-back'), ('finalitzar', 'Continuar', 'gtk-go-forward')]}
      },
      'finalitzar': {
          'actions': [],
        'result': {'type': 'form', 'arch': _finalitzar_form,'fields': _finalitzar_fields, 'state':[('end', 'Cancelar', 'gtk-cancel'), ('guardar', 'Finalitzar', 'gtk-ok')]}
      },
      'guardar': {
          'actions': [_guardar],
          'result': { 'type' : 'state', 'state' : 'end' },
      },
    }

wizard_mouretrafo('giscedata.transformador.trafo.moure')
