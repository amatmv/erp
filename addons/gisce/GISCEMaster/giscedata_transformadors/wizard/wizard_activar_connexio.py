# -*- coding: iso-8859-1 -*-
import wizard
import pooler
from datetime import *


_avis_form = """<?xml version="1.0"?>
<form string="Avís">
  <label string="Amb aquest assistent canviaràs la connexió activa del transformador." />
</form>"""

_avis_fields = {}

def _escollir_connexio(self, cr, uid, data, context={}):
    return {'transformador': data['id'], 'data': (datetime.now()).strftime('%Y-%m-%d %H:%M:%S') }

_escollir_connexio_form = """<?xml version="1.0"?>
<form string="Informacio d'entrada">
  <field name="transformador" colspan="4"/>
  <field name="connexio" domain="[('trafo_id', '=', transformador)]" />
  <field name="data" />
</form>"""

_escollir_connexio_fields = {
  'transformador': {'string': 'Transformador', 'type': 'many2one', 'readonly': True, 'relation': 'giscedata.transformador.trafo'},
        'connexio': {'string': 'Connexió', 'required': True, 'type': 'many2one', 'relation': 'giscedata.transformador.connexio'},
  'data': {'string': 'Data', 'type': 'date'},
}


def _guardar(self, cr, uid, data, context={}):
    for connexio in pooler.get_pool(cr.dbname).get('giscedata.transformador.connexio').search(cr, uid, [('trafo_id', '=', data['id']), ('conectada', '=', True)]):
        pooler.get_pool(cr.dbname).get('giscedata.transformador.connexio').write(cr, uid, [connexio], {'conectada': False})
    pooler.get_pool(cr.dbname).get('giscedata.transformador.connexio').write(cr,\
    uid, [data['form']['connexio']], {'conectada': True, 'dataconexio': data['form']['data']})

    return {}

class wizard_activar_connexio(wizard.interface):

    states = {
      'init': {
          'actions': [],
        'result': {'type': 'form', 'arch': _avis_form,'fields': _avis_fields, 'state':[('end', 'Cancelar', 'gtk-cancel'), ('escollir_connexio', 'Continuar', 'gtk-go-forward')]}
      },
      'escollir_connexio': {
          'actions': [_escollir_connexio],
          'result': {'type': 'form', 'arch': _escollir_connexio_form,'fields': _escollir_connexio_fields, 'state':[('end', 'Cancelar', 'gtk-cancel'), ('guardar', 'Activar', 'gtk-ok')]}
      },
      'guardar': {
          'actions': [_guardar],
          'result': { 'type' : 'state', 'state' : 'end' },
      },
    }

wizard_activar_connexio('giscedata.transformador.trafo.connexioactiva')
