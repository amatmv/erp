# -*- coding: utf-8 -*-

from osv import osv, fields


class giscedata_cts(osv.osv):
    _name = 'giscedata.cts'
    _inherit = 'giscedata.cts'

    def _codi_instalacio(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for ct in self.browse(cr, uid, ids):
            codi = 22
            # Si no es del tipus CT sera 0
            if ct.id_installacio.name != 'CT':
                res[ct.id] = 000
                continue
            # Si tinc dos transformadors dividirem la potencia per 2
            if len(ct.transformadors) == 2:
                codi += 10
                potencia = int(ct.potencia)/2
            elif len(ct.transformadors) == 0:
                potencia = 0
            else:
                if ct.potencia:
                    potencia = int(ct.potencia)
                else:
                    potencia = 0

            if 15 < potencia <= 25:
                codi += 1
            elif 25 < potencia <= 50:
                codi += 2
            elif 50 < potencia <= 100:
                codi += 3
            elif 100 < potencia <= 160:
                codi += 4
            elif 160 < potencia <= 250:
                codi += 5
            elif 250 < potencia <= 400:
                codi += 6
            elif 400 < potencia <= 630:
                codi += 7
            elif 630 < potencia <= 1000:
                codi += 8
            elif 1000 < potencia <= 1250 or potencia > 1250:
                codi += 9

            if ct.id_subtipus.categoria_cne.codi == 'L':
                codi += 20
            elif ct.id_subtipus.categoria_cne.codi == 'I':
                codi += 40
            elif ct.id_subtipus.categoria_cne.codi == 'S':
                codi += 46

            res[ct.id] = codi
        return res

    _columns = {
        'codi_instalacio': fields.function(
            _codi_instalacio, type='integer',
            method=True, string="Codi de tipus d'instal·lació"),
    }


giscedata_cts()