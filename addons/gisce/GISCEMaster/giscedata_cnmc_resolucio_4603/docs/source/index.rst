Documentació del mòdul Resolució 4603
======================================

=================
Revisió de canvis
=================

====== ========================================================================
Versió Descripció del canvi
====== ========================================================================
v0.1.0 Versió inicial
v0.2.0 Versió 2015, Informació Data APM, Propietari,
====== ========================================================================

.. note::
    Us recomanem que aneu comprovant perodicament si hi ha alguna nova versió d'aquest manual des de
    l'enllaç que us hem facilitat per descarregar-lo.

===========
Introducció
===========

Aquest mòdul permet la generació dels formularis de la Ressolució 4603 relatius a l’inventari
de les instal·lacions en format XML.

El mòdul permet generar 8 fitxers .csv corresponents als NODES descrits a la ressolució 4603.

Una vegada validats els fitxers csv es genera el fitxer XML amb tots els nodes, en el format indicat a la Resolució.


.. note::
    Relacionats amb aquest módul hi ha el módul de subestacions, cel·les i elements de tall, despatxos, posicions,
    Catàleg de cables AT, Catàleg de cables BT, Expedients.

.. note::
    Degut a que hi han diferencies entre els CINI publicats a la Resolució 4603 i els de la circular 4/2014 creiem
    convenient unificar els criteris i fer servir tothom els CINI de la Circular 4/2014 que es una publicació més
    recent.

===============================
Descripció dels diferents NODES
===============================

-----------
NODE: LINIA
-----------

**LINIES AT**


.. note::
    Tots els camps referents a les línies provenen dels models de  **trams,** **linies,** **conductors,** i **expedient**
    per tant hauran d'estar ben cumplimentats tots els camps que fa servir. Aquest NODE es genera en 2 fitxers csv,
    un per línies AT i un altre per les línies BT

ELEMENTS SELECCIONATS
^^^^^^^^^^^^^^^^^^^^^

Només s'inclouran els Trams que compleixin les següents condicions de la fitxa
de **Trams**. També es trindran en compte les dades de la fitxa **Linia AT** de
la línia a la qual pertany

**De Línia AT**

* **Actiu**: La línia ha d'estar activa
* **Propietari**: Ha d'estar marcat com a propietari perque apareixi als llistats.
* **Nom**: La línia **NO** ha de ser la `-1` que es la que conté tots els embarrats_AT.

**De Trams**

* **Actiu**:
   * Ha d'estar actiu o,
   * Si no està actiu, hi ha d'haver el camp **data baixa** amb la data posterior
     a 31 de Desembre de l'any sol·licitat.
* **Data APM**: Data de posta en marxa anterior al final de l'any sol·licitat,
  a 31 de Desembre. ( p.e. per l'any 2014 data_APM < 01/01/2015)
* **Conductor**: El tipus del cable associat al tram i **NO** ha de ser
  **EMBARRAT** (E)

.. note::
    S'han de comprovar tots els registres que tenen data_APM de l'any en que es genera el fitxer (en aquest cas 2015)
    perque no surtiran als fitxers .csv.
    S'han de comprovar les instal·lacions **reformades** durant l'any en que es genera el fitxer i comprovar les 
    dates_APM dels registres en questió per veure si han de sortir o no als .csv y si cal modificar la data_APM per
    que apareixin en la generació dels fitxers.

ORÍGEN
^^^^^^

S'han afegit nous camps a la fitxa dels trams:

* **Tensió màxima de disseny**: És la tensió per la que està dissenyat el tram de línia (V). Aquest camp permet 
  classificar els trams de línia AT en funció de la tensió màxima per la que ha estat dissenyat i no per latensió
  a la que està treballant actualment.


A continuació es detalla d'on s'obtenenen els valors dels camps d'aquest fitxer:

Camps obtinguts directament dels camps de la fitxa de **TRAMS**

+----------------------------+-------------------------------------------------------------------------------------+
| CAMP                       |CAMP DE LA BASE DE DADES                                                             |
+============================+=====================================================================================+
|IDENTIFICADOR               |IDENTIFICADOR DEL TRAM                                                               |
+----------------------------+-------------------------------------------------------------------------------------+
|CINI                        |CINI DEL TRAM                                                                        |
+----------------------------+-------------------------------------------------------------------------------------+
|ORIGEN                      |ORIGEN TRAM                                                                          |
+----------------------------+-------------------------------------------------------------------------------------+
|DESTINO                     |FINAL TRAM                                                                           |
+----------------------------+-------------------------------------------------------------------------------------+
|PARTICIPACION               |100-%FINANÇAMENT                                                                     |
+----------------------------+-------------------------------------------------------------------------------------+
|FECHA APS                   |DATA APM DEL TRAM                                                                    |
+----------------------------+-------------------------------------------------------------------------------------+
|FECHA_BAJA                  |CAMP FIX=NULL                                                                        |
+----------------------------+-------------------------------------------------------------------------------------+
|NUMERO_CIRCUITOS            |CIRCUITS                                                                             |
+----------------------------+-------------------------------------------------------------------------------------+
|NUMERO_CONDUCTORES          |CAMP FIX=1                                                                           |
+----------------------------+-------------------------------------------------------------------------------------+
|LONGITUD                    |LONGITUD DEL TRAM/1000                                                               |
+----------------------------+-------------------------------------------------------------------------------------+
|SECCION                     |VALOR NUMÉRIC DEL CAMP **SECCIÓ** DE LA FITXA DEL CONDUCTOR ASSOCIAT AL TRAM         |
+----------------------------+-------------------------------------------------------------------------------------+


Camps **CALCULATS**: En aquest cas el valor no existeix en un camp de la BD i es calculen els camps de la 4603
a partir de valors de camps de la BD i càlculs addicionals.

+----------------------------+-------------------------------------------------------------------------------------+
| CAMP                       |CAMP DE LA BASE DE DADES                                                             |
+============================+=====================================================================================+
|CODIGO_CCAA_1               |EN FUNCIÓ DEL MUNICIPI DE LA LAT I LA CCAA RELACIONADA EN LA TAULA 4 DE LA RESSOLUCIÓ|
|                            |4603                                                                                 |
+----------------------------+-------------------------------------------------------------------------------------+
|CODIGO_CCAA_2               |Aquest camp sempre serà igual a CODIGO_CCAA_1                                        |
+----------------------------+-------------------------------------------------------------------------------------+
|CODIGO_TIPO_LINEA           |VALOR DE LA TAULA 3 DE LA RESSOLUCIÓ 4603, EN FUNCIÓ DEL CINI DEL TRAM               |
+----------------------------+-------------------------------------------------------------------------------------+
|CAPACIDAD                   |VALOR DEL CAMP INTESITAT MÀXIMA DE LA FITXA DEL CONDUCTOR ASSOCIAT AL TRAM           |
|                            |EN FUNCIÓ DE LA TENSIÓ MÀXIMA DE DISSENY DEL TRAM I SI NO DE LA TENSIÓ DE LA LINIA AT|
|                            |A LA QUE ESTA ASSOCIAT EL TRAM (en MVA)                                              |
+----------------------------+-------------------------------------------------------------------------------------+

CODIGO_CCAA_2 aquest camp es només pels trams de línia que comencen en una comunitat autónoma i acaben
en una altra.
En aquest cas s’ha determinat que es creará una línia nova quan es faci un canvi de comunitat autónoma.
Així no podran existir línies i trams que comparteixin comunitats autònomes diferents.


**Catàleg de Cables AT**

Des del menú Linies AT / Manteniment / Cables / Cable.

S'ha de cumplimentar correctament el catàleg de cables AT per poder calcular correctament els camps **SECCIO**
i **CAPACITAT** d'aquest fitxer.

La intensitat del cable **INTENSITAT ADMISIBLE** estarà indicada en Ampers, aquest valor serà el de catàleg, o en
tot cas la capacitat s'ha de correspondre amb la capacitat de l'hivern, per a una temperatura ambient de 10ºC segons
indica l'annex 1 de la ressolució. El camp **SECCIO** es cumplimenta amb un valor numèric.

.. image:: _static/cataleg_cables_at.png


**LINIES BT**

.. note::
    Tots els camps referents a les línies BT provenen dels models de **Elements BT** i **CT** 
    per tant hauran d'estar ben cumplimentats tots els camps que fa servir.

ELEMENTS SELECCIONATS
^^^^^^^^^^^^^^^^^^^^^

Només s'inclouran els Trams BT que compleixin les següents 
condicions de la fitxa de **Elements bt**:

* **Actiu**:
   * Ha d'estar actiu o,
   * Si no està actiu, hi ha d'haver el camp **data baixa** amb la data posterior
     a 31 de Desembre de l'any sol·licitat.
* **Cable**: El `Tipus` del Cable seleccionat **NO** és **Embarrat** (E)
* **Propietari**: Ha d'estar marcat com a propietari. Si es vol que surti
  encara que no s'en sigui el propietari, es pot utilitzar el camp `% pagat per
  la companyia`
* **Data APM**: Data de posta en marxa anterior al final de l'any sol·licitat,
  a 31 de Desembre. ( p.e. per l'any 2014 data_APM < 01/01/2015)

ORÍGEN
^^^^^^
Camps obtinguts directament dels camps de la fitxa de **ELEMENTS_BT**

+------------------------------+-------------------------------------------------------------------------------------+
| CAMP                         |CAMP DE LA BASE DE DADES                                                             |
+==============================+=====================================================================================+
|IDENTIFICADOR                 |IDENTIFICADOR DEL TRAM BT (INTERN DE LA TOPOLOGIA)                                   |
+------------------------------+-------------------------------------------------------------------------------------+
|CINI                          |CINI DEL TRAM BT                                                                     |
+------------------------------+-------------------------------------------------------------------------------------+
|ORIGEN                        |NUS INICI, (INTERN DE LA TOPOLOGIA)                                                  |
+------------------------------+-------------------------------------------------------------------------------------+
|DESTINO                       |NUS FINAL, (INTERN DE LA TOPOLOGIA)                                                  |
+------------------------------+-------------------------------------------------------------------------------------+
|CODIGO_CCAA_1                 |EN FUNCIÓ DEL **MUNICIPI** DEL TRAM_BT I LA CCAA RELACIONADA EN LA TAULA 4 DE LA     |
|                              |RESSOLUCIÓ 4603                                                                      |
+------------------------------+-------------------------------------------------------------------------------------+
|CODIGO_CCAA_2                 |Aquest camp sempre serà igual a CODIGO_CCAA_1                                        |
+------------------------------+-------------------------------------------------------------------------------------+
|PARTICIPACION                 |100-%FINANÇAMENT                                                                     |
+------------------------------+-------------------------------------------------------------------------------------+
|FECHA APS                     |DATA APM DEL TRAM BT                                                                 |
+------------------------------+-------------------------------------------------------------------------------------+
|FECHA_BAJA                    |CAMP FIX=NULL                                                                        |
+------------------------------+-------------------------------------------------------------------------------------+
|NUMERO_CIRCUITOS              |CAMP FIX=1                                                                           |
+------------------------------+-------------------------------------------------------------------------------------+
|NUMERO_CONDUCTORES            |CAMP FIX=1                                                                           |
+------------------------------+-------------------------------------------------------------------------------------+
|LONGITUD                      |LONGITUD DEL TRAM_BT/1000                                                            |
+------------------------------+-------------------------------------------------------------------------------------+
|SECCION                       |VALOR NUMÉRIC DEL CAMP **SECCIÓ** DE LA FITXA DEL CONDUCTOR ASSOCIAT AL TRAM_BT      |
+------------------------------+-------------------------------------------------------------------------------------+


Camps **CALCULATS**: En aquest cas el valor no existeix en un camp de la BD i es calculen els camps de la 4603
a partir de valors de camps de la BD i càlculs addicionals.

+-----------------------------+-------------------------------------------------------------------------------------+
| CAMP                        |CAMP CALCULAT                                                                        |
+=============================+=====================================================================================+
|CODIGO_TIPO_LINEA            |VALOR DE LA TAULA 3 DE LA RESSOLUCIÓ 4603, EN FUNCIÓ DEL CINI DEL TRAM_BT            |
+-----------------------------+-------------------------------------------------------------------------------------+
|CAPACIDAD                    |VALOR FUNCIÓ DEL CAMP **INTESITAT MÀXIMA** DE LA FITXA DEL CONDUCTOR ASSOCIAT AL     |
|                             |TRAMB_BT I LA TENSIÓ DEL TRAM AL CAMP **VOLTATGE**                                   |
+-----------------------------+-------------------------------------------------------------------------------------+

**Catàleg de Cables BT**

Des del menú Linies BT / Manteniment / Cables / Cable.

S'ha de cumplimentar correctament el catàleg de cables AT per poder calcular correctament els camps **SECCIO**
i **CAPACITAT** d'aquest fitxer.

La intensitat del cable **INTENSITAT ADMISIBLE** estarà indicada en Ampers, aquest valor serà el de catàleg, o en
tot cas la capacitat s'ha de correspondre amb la capacitat de l'hivern, per a una temperatura ambient de 10ºC segons
indica l'annex 1 de la ressolució. El camp **SECCIO** es cumplimenta amb un valor numèric.


.. image:: _static/cataleg_cables_BT.png


A la fitxa del catàleg de cables s'ha afegit el camp:

* **Data acta posada en marxa**: Aquesta data correspondria amb la data en que es presenta a industria la legalització de la línia BT.

Quedant el camp existent

* **Data alta**: Aquesta data correspon a la data en que es va instal·lar.( data de realització de la obra)

----------------
NODE: SUBESTACIÓ
----------------

.. note::
   Per obtenir aquest fitxer cal tenir configurat el mòdul de subestacions.

ELEMENTS SELECCIONATS
^^^^^^^^^^^^^^^^^^^^^

Només s'inclouran les Subestacions que compleixin les següents condicions
de la fitxa de **Subestacions**:

* **Actiu**:
   * Ha d'estar actiu o,
   * Si no està actiu, hi ha d'haver el camp **data baixa** amb la data posterior
     a 31 de Desembre de l'any sol·licitat.
* **Propietari**: Ha d'estar marcat com a propietari. Si es vol que surti
  encara que no s'en sigui el propietari, es pot utilitzar el camp `% pagat per
  la companyia`
* **Data APM**: Data de posta en marxa anterior al final de l'any sol·licitat,
  a 31 de Desembre. ( p.e. per l'any 2014 data_APM < 01/01/2015)

ORÍGEN
^^^^^^

Camps obtinguts directament dels camps de la fitxa de **SUBESTACIONS**

+-----------------------+-------------------------------------------------------------------------------------+
| CAMP                  |CAMP DE LA BASE DE DADES                                                             |
+=======================+=====================================================================================+
|IDENTIFICADOR          |IDENTIFICADOR DE LA SUBESTACIÓ                                                       |
+-----------------------+-------------------------------------------------------------------------------------+
|CINI                   |CINI DE LA SUBESTACIÓ                                                                |
+-----------------------+-------------------------------------------------------------------------------------+
|DENOMINACIÓN           |NOM DE LA SUBESTACIÓ                                                                 |
+-----------------------+-------------------------------------------------------------------------------------+
|CODIGO_TIPO_POSICION   |CAMP FIX=NULL                                                                        |
+-----------------------+-------------------------------------------------------------------------------------+
|CODIGO_CCAA            |EN FUNCIÓ DEL MUNICIPI DE LA SE I LA CCAA RELACIONADA EN LA TAULA 4 DE LA RESSOLUCIÓ |
+-----------------------+-------------------------------------------------------------------------------------+
|PARTICIPACION          |100-%FINANÇAMENT                                                                     |
+-----------------------+-------------------------------------------------------------------------------------+
|FECHA APS              |DATA APM                                                                             |
+-----------------------+-------------------------------------------------------------------------------------+
|FECHA_BAJA             |CAMP FIX=NULL                                                                        |
+-----------------------+-------------------------------------------------------------------------------------+
|POSICIONES             |SUMA EL NOMBRE DE POSICIONS QUE TE LA SE A LA PESTANYA POSICIONS TANT SI ESTAN       |
|                       |EQUIPADES AMB INTERUPTOR O NO                                                        |
+-----------------------+-------------------------------------------------------------------------------------+


-----------------------------------------------------------
NODE: POSICIONES EQUIPADAS CON INTERRUPTOR EN SUBESTACIONES
-----------------------------------------------------------

.. note::
    Per obtenir aquest fitxer cal tenir configurat el mòdul de subestaciions.
    
ELEMENTS SELECCIONATS
^^^^^^^^^^^^^^^^^^^^^

Només s'inclouran les posicions  que compleixin les següents 
condicions de la fitxa de **Posicions**:

* **Tipus Interruptor**: Ha de ser del tipus **Interruptor automàtic** (2)
* **Actiu**:
   * Ha d'estar actiu o,
   * Si no està actiu, hi ha d'haver el camp **data baixa** amb la data posterior
     a 31 de Desembre de l'any sol·licitat.
* **Propietari**: Ha d'estar marcat com a propietari. Si es vol que surti
  encara que no s'en sigui el propietari, es pot utilitzar el camp `% pagat per
  la companyia`
* **Data APM**: Data de posta en marxa anterior al final de l'any sol·licitat,
  a 31 de Desembre ( p.e. per l'any 2014 data_APM < 01/01/2015)

ORÍGEN
^^^^^^

Camps obtinguts directament dels camps de la fitxa de **POSICIONS**

+-----------------------+-------------------------------------------------------------------------------------+
| CAMP                  |CAMP DE LA BASE DE DADES                                                             |
+=======================+=====================================================================================+
|IDENTIFICADOR          |IDENTIFICADOR DE LA POSICIÓN                                                         |
+-----------------------+-------------------------------------------------------------------------------------+
|CINI                   |CINI DE LA POSICIÓN                                                                  |
+-----------------------+-------------------------------------------------------------------------------------+
|DENOMINACIÓN           |NOM DE LA SUBESTACIÓ                                                                 |
+-----------------------+-------------------------------------------------------------------------------------+
|CODIGO_TIPO_POSICION   |CAMP FIX=NULL                                                                        |
+-----------------------+-------------------------------------------------------------------------------------+
|CODIGO_CCAA            |EN FUNCIÓ DEL MUNICIPI DE LA SE I LA CCAA RELACIONADA EN LA TAULA 4 DE LA RESSOLUCIÓ |
+-----------------------+-------------------------------------------------------------------------------------+
|PARTICIPACION          |100-%FINANÇAMENT                                                                     |
+-----------------------+-------------------------------------------------------------------------------------+
|FECHA APS              |DATA  DE POSADA EN MARXA DE LA POSICIÓ                                               |
+-----------------------+-------------------------------------------------------------------------------------+
|FECHA_BAJA             |CAMP FIX=NULL                                                                        |
+-----------------------+-------------------------------------------------------------------------------------+
|POSICIONES             |SUMA EL NOMBRE DE POSICIONS QUE TE LA SE A LA PESTANYA POSICIONS TANT SI ESTAN       |
|                       |EQUIPADES AMB INTERUPTOR O NO                                                        |
+-----------------------+-------------------------------------------------------------------------------------+


Camps **CALCULATS**: En aquest cas el valor no existeix en un camp de la BD i es calculen els camps de la 4603
a partir de valors de camps de la BD i càlculs addicionals.

+-----------------------+-------------------------------------------------------------------------------------+
| CAMP                  |CAMP CALCULAT                                                                        |
+=======================+=====================================================================================+
|CODIGO_TIPO_POSICION   |VALOR DE LA TAULA 3 DE LA RESSOLUCIÓ 4603, EN FUNCIÓ DEL CINI DE LA POSICIÓ          |
+-----------------------+-------------------------------------------------------------------------------------+

El tipus de posició d'intemperie serà assimilable a la convencional i el tipus interior serà assimilable a la 
posició blindada.


-------------
NODE: MÁQUINA
-------------

ELEMENTS SELECCIONATS
^^^^^^^^^^^^^^^^^^^^^

Només s'inclouran els Transformadors que compleixin les següents condicions de
la fitxa de **Transformadors**:

* **Estat**: Els transformadors associats a un estat marcat com **apareix a
  l'inventari** (veure la nota `notaestat`_)
* **Actiu**:
   * Ha d'estar actiu o,
   * Si no està actiu, hi ha d'haver el camp **data baixa** amb la data posterior
     a 31 de Desembre de l'any sol·licitat.
* **Propietari**: Ha d'estar marcat com a propietari. Si es vol que surti
  encara que no s'en sigui el propietari, es pot utilitzar el camp `% pagat per
  la companyia`
* **Data APM**: Data de posta en marxa anterior al final de l'any sol·licitat,
  a 31 de Desembre. ( p.e. per l'any 2014 data_APM < 01/01/2015)

.. note::
   
   Els Transformadors Reductors també estan inclosos en el
   llistat si compleixen les condicions especificades.

.. note::
   :name: notaestat

   Apareixeran en aquest inventari tots els transformadors que l'estat en que
   es troben te marcada la casella **apareix a l'inventari**, tal com s'indica
   a la imatge següent `notaestatimg`_, i tots el que el seu estat es NULL.
   Cada empresa pot indicar que apareixin en aquest llistat els transformadors
   que es troben en diferents estats modificant el checkbox corresponent.

.. image:: _static/estats_inventari.png
   :name: notaestatimg

ORÍGEN
^^^^^^

Camps obtinguts directament dels camps de la fitxa de **TRANSFORMADORS**

+-----------------------+-------------------------------------------------------------------------------------+
| CAMP                  |CAMP DE LA BASE DE DADES                                                             |
+=======================+=====================================================================================+
|IDENTIFICADOR          |CÓDIGO INTERNO DEL TRANSFORMADOR                                                     |
+-----------------------+-------------------------------------------------------------------------------------+
|CINI                   |CINI DEL TRANSFORMADOR                                                               |
+-----------------------+-------------------------------------------------------------------------------------+
|DENOMINACIÓN           |NÚMERO DE SERIE DEL TRANSFORMADOR                                                    |
+-----------------------+-------------------------------------------------------------------------------------+
|CODIGO_ZONA            |CAMP FIX=NULL                                                                        |
+-----------------------+-------------------------------------------------------------------------------------+
|CODIGO_CCAA            |EN FUNCIÓ DEL MUNICIPI DEL CT/SE I LA CCAA RELACIONADA EN LA TAULA 4 DE LA RESSOLUCIÓ|
+-----------------------+-------------------------------------------------------------------------------------+
|PARTICIPACION          |100-%FINANÇAMENT                                                                     |
+-----------------------+-------------------------------------------------------------------------------------+
|FECHA APS              |DATA APM                                                                             |
+-----------------------+-------------------------------------------------------------------------------------+
|FECHA_BAJA             |CAMP FIX=NULL                                                                        |
+-----------------------+-------------------------------------------------------------------------------------+
|POSICIONES             |SUMA EL NOMBRE DE POSICIONS QUE TE LA SE A LA PESTANYA POSICIONS TANT SI ESTAN       |
|                       |EQUIPADES AMB INTERUPTOR O NO                                                        |
+-----------------------+-------------------------------------------------------------------------------------+


Camps **CALCULATS**: En aquest cas el valor no existeix en un camp de la BD i es calculen els camps de la 4603
a partir de valors de camps de la BD i càlculs addicionals.


+-----------------------+-------------------------------------------------------------------------------------+
| CAMP                  |CAMP CALCULAT                                                                        |
+=======================+=====================================================================================+
|CODIGO_TIPO_MAQUINA    |VALOR DE LA TAULA 3 DE LA RESSOLUCIÓ 4603, EN FUNCIÓ DEL CINI DEL TRANSFORMADOR      |
+-----------------------+-------------------------------------------------------------------------------------+

--------------
NODE: DESPACHO
--------------

ELEMENTS SELECCIONATS
^^^^^^^^^^^^^^^^^^^^^

Només s'inclouran els Despatxos que compleixin les següents 
condicions de la fitxa de la fitxa accessible a `Infraestructura / Despatxos`:

* **Any_PS**: L'any de posada en servei ha de ser inferior o igual al
  sol·licitat

ORÍGEN
^^^^^^

.. image:: _static/DESPATXOS_1.png

.. image:: _static/DESPATXOS_2.png

Camps obtinguts directament dels camps de la fitxa de **DESPATXOS**

+-----------------------+-------------------------------------------------------------------------------------+
| CAMP                  |CAMP DE LA BASE DE DADES                                                             |
+=======================+=====================================================================================+
|IDENTIFICADOR          |IDENTIFICADOR                                                                        |
+-----------------------+-------------------------------------------------------------------------------------+
|CINI                   |CINI DEL DESPATX                                                                     |
+-----------------------+-------------------------------------------------------------------------------------+
|DENOMINACIÓN           |DESCRIPCIÓ DE LA INSTAL·LACIÓ                                                        |
+-----------------------+-------------------------------------------------------------------------------------+
|AÑO_PS                 |DATA DE POSADA EN MARXA DE LA INSTAL·LACIÓ                                           |
+-----------------------+-------------------------------------------------------------------------------------+
|VALOR DE LA INVERSIÓN  |VALOR DE LA INVERSIÓ EN €                                                            |
+-----------------------+-------------------------------------------------------------------------------------+

----------------------------------------
NODE: EQUIPOS DE MEJORA DE LA FIABILIDAD
----------------------------------------

ELEMENTS SELECCIONATS
^^^^^^^^^^^^^^^^^^^^^

Només s'inclouran les Cel·les i elements de Tall que compleixin les següents 
condicions de la fitxa de **Cel·les i elements de Tall** associades a CT's i
suports:

* **Categoria inventari**: Ha de ser **Fiabilitat** tant si està associat a CT
  com si està associat a suports de línies AT/MT (seccionadors, interruptors,
  fusibles, seccionadors, ...).
* **Actiu**:
   * Ha d'estar actiu o,
   * Si no està actiu, hi ha d'haver el camp **data baixa** amb la data posterior
     a 31 de Desembre de l'any sol·licitat.
* **Propietari**: Ha d'estar marcat com a propietari. Si es vol que surti
  encara que no s'en sigui el propietari, es pot utilitzar el camp `% pagat per
  la companyia`
* **Data APM**: Data de posta en marxa anterior al final de l'any sol·licitat,
  a 31 de Desembre

ORÍGEN
^^^^^^

Camps obtinguts directament dels camps de la fitxa de **CEL·LES I ELEMENTS DE TALL**


+-----------------------+-------------------------------------------------------------------------------------+
| CAMP                  |CAMP DE LA BASE DE DADES                                                             |
+=======================+=====================================================================================+
|IDENTIFICADOR          |CODI DE LA CEL·LA O ELEMENT DE TALL                                                  |
+-----------------------+-------------------------------------------------------------------------------------+
|CINI                   |CINI DEL CEL·LA O ELEMENT DE TALL                                                    |
+-----------------------+-------------------------------------------------------------------------------------+
|DENOMINACIÓN           |CODI DE LA CEL·LA O ELEMENT DE TALL                                                  |
+-----------------------+-------------------------------------------------------------------------------------+
|CODIGO_CCAA            |EN FUNCIÓ DEL MUNICIPI DEL CT/LAT I LA CCAA RELACIONADA EN LA TAULA 4 DE LA          |
|                       |RESSOLUCIÓ                                                                           |
+-----------------------+-------------------------------------------------------------------------------------+
|FECHA APS              |DATA APM                                                                             |
+-----------------------+-------------------------------------------------------------------------------------+
|FECHA_BAJA             |CAMP FIX=NULL                                                                        |
+-----------------------+-------------------------------------------------------------------------------------+


Camps **CALCULATS**: En aquest cas el valor no existeix en un camp de la BD i es calculen els camps de la 4603
a partir de valors de camps de la BD i càlculs addicionals.


+-----------------------+-------------------------------------------------------------------------------------+
| CAMP                  |CAMP CALCULAT                                                                        |
+=======================+=====================================================================================+
|CODIGO_TIPO_INST       |VALOR DE LA TAULA 3 DE LA RESSOLUCIÓ 4603, EN FUNCIÓ DEL CINI DE L'ELEMENT DE TALL   |
+-----------------------+-------------------------------------------------------------------------------------+


-------------------------------
NODE: CENTROS DE TRANSFORMACIÓN
-------------------------------

ELEMENTS SELECCIONATS
^^^^^^^^^^^^^^^^^^^^^

Només s'inclouran els Centres Transformadors que compleixin les següents 
condicions de la fitxa de **CT**:

* **Tipus Instal·lació**: Qualsevol excepte Subestacions (**SE**). Inclou CH,
  CP, CM, CR ...
* **Actiu**:
   * Ha d'estar actiu o,
   * Si no està actiu, hi ha d'haver el camp **data baixa** amb la data posterior
     a 31 de Desembre de l'any sol·licitat.
* **Propietari**: Ha d'estar marcat com a propietari. Si es vol que surti
  encara que no s'en sigui el propietari, es pot utilitzar el camp `% pagat per
  la companyia`
* **Data APM**: Data de posta en marxa anterior al final de l'any sol·licitat,
  a 31 de Desembre

ORÍGEN
^^^^^^
Camps obtinguts directament dels camps de la fitxa de **CTS**

+-----------------------+-------------------------------------------------------------------------------------+
| CAMP                  |CAMP DE LA BASE DE DADES                                                             |
+=======================+=====================================================================================+
|IDENTIFICADOR          |CODI DEL CT                                                                          |
+-----------------------+-------------------------------------------------------------------------------------+
|CINI                   |CINI DEL CT                                                                          |
+-----------------------+-------------------------------------------------------------------------------------+
|DENOMINACIÓN           |DESCRIPCIÓ DEL CT                                                                    |
+-----------------------+-------------------------------------------------------------------------------------+
|CODIGO_CCAA            |EN FUNCIÓ DEL MUNICIPI DEL CT I LA CCAA RELACIONADA EN LA TAULA 4 DE LA RESSOLUCIÓ   |
+-----------------------+-------------------------------------------------------------------------------------+
|PARTICIPACION          |100-%FINANÇAMENT                                                                     |
+-----------------------+-------------------------------------------------------------------------------------+
|FECHA APS              |DATA APM                                                                             |
+-----------------------+-------------------------------------------------------------------------------------+
|FECHA_BAJA             |CAMP FIX=NULL                                                                        |
+-----------------------+-------------------------------------------------------------------------------------+

Camps **CALCULATS**: En aquest cas el valor no existeix en un camp de la BD i es calculen els camps de la 4603
a partir de valors de camps de la BD i càlculs addicionals.


+-----------------------+-------------------------------------------------------------------------------------+
| CAMP                  |CAMP CALCULAT                                                                        |
+=======================+=====================================================================================+
|CODIGO_TIPO_CT         |VALOR DE LA TAULA 3 DE LA RESSOLUCIÓ 4603, EN FUNCIÓ DELS TRANSFORMADORS INSTAL·LATS |
|                       |I DEL TIPUS DE CT.( CASETA, LOCAL, SUBTERRANI, INTEMPERIE), NUMERO DE MAQUINES I     |
|                       |POTENCIA                                                                             |
+-----------------------+-------------------------------------------------------------------------------------+


=======================
Generació dels informes
=======================

----
Menú
----

Per accedir a l'assistent per generar els fitxers CSV i el XML cal anar al menú
**Administració pública / CNMC / Resolucions / Resolucio 4603 / Generar XML Inventari**

.. figure:: _static/MENU_4603_1.png
   :align: center

A través d'aquest assistent podem realitzar dues operacions:
  1. Generar els 8 informes en format csv ( indicats en els apartats anteriors)
  2. Generar l'XML a partir dels fitxers csv.

.. image:: _static/MENU_4603_2.png
   :align: center

En el desplegable tipus es pot seleccionar el fitxer .csv que es vol generar


================ ======================================================
Fitxer           Descripció
================ ======================================================
LAT CSV          Fitxer de les linies AT (NODE: LINIA)
BT CSV           Fitxer de les línies BT (NODE: LINIA)
Subestacions CSV NODE SUBESTACIONS
Posicions CSV    NODE POSICIONS EQUIPADES AMB INTERRUPTOR DE SUBESTACIÓ
Maquines CSV     NODE MAQUINA
Despatx CSV      NODE DESPATX
Fiabilitat CSV   NODE EQUIPS DE MILLORA DE LA FIABILITAT
CTS CSV          NODE CENTRES DE TRANSFORMACIÓ
================ ======================================================

.. image:: _static/MENU_4603_3.png
   :align: center

Una vegada generat el fitxer csv es pot analitzar per validar-lo i veure quines dades falten a la base de dades
o fer modificacions directament al fitxer.csv

Una vegada validats/modificats tots els 8 fitxers csv, ja es pot procedir a la càrrega dels 8 csv i generació del
fitxer XML en el format indicat en la Resolució 4603.

Aquest fitxer xml es valida en base al fitxer XSD.


=======================
Generació dels informes
=======================

Els informes es poden generar a través del menú: **Administració pública >
CNMC > Circulares > Ressolució 4603**

.. note::
    Depenent de la quantitat de dades els informes poden tardar força temps
    en generar-se.

Una vegada hagi finalitzat el procés podrem descarregar l'informe generat en fotmat XML

