# -*- coding: utf-8 -*-

from osv import osv, fields


class GiscedataCtsSubestacionsPosicio(osv.osv):
    _name = 'giscedata.cts.subestacions.posicio'
    _inherit = 'giscedata.cts.subestacions.posicio'

    def _codi_instalacio(self, cr, uid, ids, field_name, arg, context=None):
        res = {}

        for pos in self.browse(cr, uid, ids):
            codi = 0
            if 1000 <= int(pos.tensio.tensio) < 36000:
                if pos.tipus_posicio == 'B':
                    codi = 102
                elif pos.tipus_posicio == 'C':
                    codi = 106
            if 36000 <= int(pos.tensio.tensio) < 110000:
                if pos.tipus_posicio == 'B':
                    codi = 95
                elif pos.tipus_posicio == 'C':
                    codi = 99
            if 110000 <= int(pos.tensio.tensio) < 132000:
                if pos.tipus_posicio == 'B':
                    codi = 88
                elif pos.tipus_posicio == 'C':
                    codi = 92

            res[pos.id] = codi
        return res

    _columns = {
        'codi_instalacio': fields.function(
            _codi_instalacio, type='integer',
            method=True, string="Codi de tipus d'instal·lació"),
    }

GiscedataCtsSubestacionsPosicio()
