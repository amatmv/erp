# -*- coding: utf-8 -*-
import subprocess
import tempfile
import base64
import os
import netsvc
from tools import config

from osv import osv, fields
import tools
from tools.translate import _
from libcnmc.res_4603.INV import INV

OPCIONS = [('inv', 'XML Inventari'), ('lat', 'LAT CSV'),
           ('lbt', 'BT CSV'), ('sub', 'Subestacions CSV'),
           ('pos', 'Posicions CSV'), ('maq', 'Maquines CSV'),
           ('desp', 'Despatx CSV'), ('fia', 'Fiabilitat CSV'),
           ('cts', 'CTS CSV')]

class WizardGenerarCnmcResolucio4603(osv.osv_memory):
    """Wizard per generar els XML de CNMC de inventari"""
    _name = 'wizard.generar.cnmc.resolucio.4603'
    _max_hours = 10

    def action_exec_script(self, cursor, uid, ids, context=None):
        """Llença l'script per l'informe de l'inventari.
        """
        script_name = {'inv': ['res_4603_inv', 'XML'],
                       'lat': ['res_4603_lat', 'LAT CSV'],
                       'lbt': ['res_4603_lbt', 'BT CSV'],
                       'sub': ['res_4603_sub', 'Subestacions CSV'],
                       'pos': ['res_4603_pos', 'Posicions CSV'],
                       'maq': ['res_4603_maq', 'Maquina CSV'],
                       'desp': ['res_4603_des', 'Despatx CSV'],
                       'fia': ['res_4603_fia', 'Fiabilitat CSV'],
                       'cts': ['res_4603_cts', 'CTS CSV']}

        wizard = self.browse(cursor, uid, ids[0], context)

        #Agafar l'scrip que seleccionem al tipus
        exe = script_name.get(wizard.tipus)[0]

        # Crido la llibreria de CNMC
        user = self.pool.get('res.users').browse(cursor, uid, uid)
        port = tools.config['port'] or 8069
        filename = tempfile.mkstemp()[1]
        args = ['cnmc', exe, '-d', cursor.dbname, '-p', port, '-u', user.login,
                '-w', user.password, '--no-interactive', '-o', filename,
                '-c', wizard.r1, '-y', wizard.anyo]
        if wizard.tipus == 'inv':
            # Que entrin codi R1, l'any i seleccionin els CSVs
            if not wizard.r1:
                raise osv.except_osv('Error_R1',
                                     _('No hi ha el codi R1'))
            if not wizard.anyo:
                raise osv.except_osv('Error_Any',
                                     _("No hi ha l'any"))
            #Guardo els arxius en temporals
            #AT
            filename_at = tempfile.mkstemp()[1]
            data_at = base64.decodestring(wizard.csv_lat)
            open(filename_at, 'w').write(data_at)
            #BT
            filename_bt = tempfile.mkstemp()[1]
            data_bt = base64.decodestring(wizard.csv_lbt)
            open(filename_bt, 'w').write(data_bt)
            #Subestacions
            filename_sub = tempfile.mkstemp()[1]
            data_sub = base64.decodestring(wizard.csv_sub)
            open(filename_sub, 'w').write(data_sub)
            #Posicions
            filename_pos = tempfile.mkstemp()[1]
            data_pos = base64.decodestring(wizard.csv_pos)
            open(filename_pos, 'w').write(data_pos)
            #Maquines
            filename_maq = tempfile.mkstemp()[1]
            data_maq = base64.decodestring(wizard.csv_maq)
            open(filename_maq, 'w').write(data_maq)
            #Despatx
            filename_desp = tempfile.mkstemp()[1]
            data_desp = base64.decodestring(wizard.csv_desp)
            open(filename_desp, 'w').write(data_desp)
            #Fiabilitat
            filename_fia = tempfile.mkstemp()[1]
            data_fia = base64.decodestring(wizard.csv_fia)
            open(filename_fia, 'w').write(data_fia)
            #Transformadors
            filename_trans = tempfile.mkstemp()[1]
            data_trans = base64.decodestring(wizard.csv_trans)
            open(filename_trans, 'w').write(data_trans)

            inv = INV(
                codi_r1=wizard.r1,
                liniesat=filename_at,
                liniesbt=filename_bt,
                subestacions=filename_sub,
                posicions=filename_pos,
                maquinas=filename_maq,
                despatxos=filename_desp,
                fiabilidad=filename_fia,
                transformacion=filename_trans,
                output=None
            )
            try:
                inv.check_encoding()
            except Exception, e:
                raise osv.except_osv('Error', e.message)

            args = [
                'cnmc', exe, '-o', filename, '-c', wizard.r1,
                '-l', filename_at, '-b', filename_bt, '-e', filename_sub,
                '-z', filename_pos, '-m', filename_maq, '-x', filename_desp,
                '-f', filename_fia, '-t', filename_trans
            ]

        # Afegir la crida al log
        logger = netsvc.Logger()
        logger.notifyChannel(
            'server', netsvc.LOG_INFO,
            'libcnmc executed: {}'.format(' '.join(map(str, args)))
        )

        # Carrega el fitxer CSV
        env = os.environ.copy()
        sentry_dsn = config.get('sentry_dsn')
        if sentry_dsn:
            env['SENTRY_DSN'] = sentry_dsn
        retcode = subprocess.call(map(str, args), env=env)
        if retcode:
            raise osv.except_osv(
                'Error',
                _('El procés de generar el fitxer ha fallat. Codi: %s')
                % retcode
            )

        tmpxmlfile = open(filename, 'r+')
        report = base64.b64encode(tmpxmlfile.read())
        filenom = ''
        if wizard.tipus == 'inv':
            filenom = 'Inv%s_R1-%s.xml' % (wizard.anyo, wizard.r1)
        elif wizard.tipus == 'lat':
            filenom = 'inv_%s_lat.csv' % wizard.anyo
        elif wizard.tipus == 'lbt':
            filenom = 'inv_%s_lbt.csv' % wizard.anyo
        elif wizard.tipus == 'sub':
            filenom = 'inv_%s_sub.csv' % wizard.anyo
        elif wizard.tipus == 'pos':
            filenom = 'inv_%s_pos.csv' % wizard.anyo
        elif wizard.tipus == 'maq':
            filenom = 'inv_%s_maq.csv' % wizard.anyo
        elif wizard.tipus == 'desp':
            filenom = 'inv_%s_desp.csv' % wizard.anyo
        elif wizard.tipus == 'fia':
            filenom = 'inv_%s_fia.csv' % wizard.anyo
        elif wizard.tipus == 'cts':
            filenom = 'inv_%s_cts.csv' % wizard.anyo

        wizard.write({'name': filenom, 'file': report, 'state': 'done'})
        os.unlink(filename)
        if wizard.tipus == 'inv':
            os.unlink(filename_at)
            os.unlink(filename_bt)
            os.unlink(filename_sub)
            os.unlink(filename_pos)
            os.unlink(filename_maq)
            os.unlink(filename_desp)
            os.unlink(filename_fia)
            os.unlink(filename_trans)

    _columns = {
        'state': fields.selection([('init', 'Init'),
                                   ('done', 'Done'), ],
                                  'State'),
        'name': fields.char('File name', size=64),
        'file': fields.binary('Fitxer generat'),
        'r1': fields.char('Codi R1', size=12),
        'tipus': fields.selection(OPCIONS, 'Tipus', required=True),
        'anyo': fields.integer('Any', size=4),
        'csv_lat': fields.binary('Fitxer LAT'),
        'csv_lbt': fields.binary('Fitxer BT'),
        'csv_sub': fields.binary('Fitxer Subestacions'),
        'csv_pos': fields.binary('Fitxer Posicions'),
        'csv_maq': fields.binary('Fitxer Màquines'),
        'csv_desp': fields.binary('Fitxer Despatx'),
        'csv_fia': fields.binary('Fitxer Fiabilitat'),
        'csv_trans': fields.binary('Fitxer CTS')
    }

    _defaults = {
        'state': lambda *a: 'init',
        'tipus': lambda *a: 'inv',
    }

WizardGenerarCnmcResolucio4603()
