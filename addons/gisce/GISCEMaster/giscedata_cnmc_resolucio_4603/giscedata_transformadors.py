# -*- coding: utf-8 -*-

from osv import osv, fields


class giscedata_trafo(osv.osv):
    _name = 'giscedata.transformador.trafo'
    _inherit = 'giscedata.transformador.trafo'

    def _codi_instalacio(self, cr, uid, ids, field_name, arg, context=None):
        res = {}

        for ct in self.browse(cr, uid, ids):
            codi = 0
            #Aquest cini es el de les maquines amb estat reserva, posarem codi 0
            #igual que en l'estat. El codi 4 de localitzacio es magatzem.
            if (ct.state == 'reserva' or ct.cini == 'I290060' or
                        ct.localitzacio.code == 4):
                codi = 0
            elif not ct.reductor:
                codi = 165
            else:
                #tensio primaria
                if int(ct.tensio_primari_actual) == 220000:
                    #tensio secundaria
                    if 110000 <= int(ct.tensio_b1) < 220000:
                        codi = 159
                    if 36000 <= int(ct.tensio_b1) < 110000:
                        codi = 160
                    if 1000 <= int(ct.tensio_b1) < 36000:
                        codi = 161
                #tensio primaria
                if 110000 <= int(ct.tensio_primari_actual) <= 132000:
                    #tensio secundaria
                    if 36000 <= int(ct.tensio_b1) < 110000:
                        codi = 162
                    if 1000 <= int(ct.tensio_b1) < 36000:
                        codi = 163
                #tensio primaria
                if 36000 <= int(ct.tensio_primari_actual) <= 66000:
                    codi = 164
                #tensio primaria
                if 1000 <= int(ct.tensio_primari_actual) < 36000:
                    codi = 165

            res[ct.id] = codi
        return res

    _columns = {
        'codi_instalacio': fields.function(
            _codi_instalacio, type='integer',
            method=True, string="Codi de tipus d'instal·lació"),
    }

giscedata_trafo()