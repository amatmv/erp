# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields


class ChangeLog(osv.osv):

    _name = 'change.log'
    _order = 'create_date desc'

    def _store_changes(self, cursor, uid, ids, model,
                       vals, fields, context=None):
        model_obj = self.pool.get(model)
        user_obj = self.pool.get('res.users')
        #Do not log anything if sync user
        #if user_obj.browse(cursor, uid, uid).login == 'sync':
        #    return True
        #if fields to store not in vals, do nothing
        fields_to_store = list(set(vals.keys()) & set(fields))
        if not len(fields_to_store):
            return True
        fields_data= model_obj.fields_get(cursor, uid, fields_to_store)
        pre_vals = model_obj.read(cursor, uid, ids,
                             fields_to_store, context=context)
        for pre_val in pre_vals:
            for field in fields_to_store:
                if (fields_data[field].get('type', False)
                    in ('one2many', 'many2many')):
                    continue
                if fields_data[field].get('relation', False):
                    rel_obj = self.pool.get(fields_data[field]['relation'])
                    new_value = vals[field]
                    if vals[field]:
                        try:
                            new_value = (vals[field],
                                         rel_obj.name_get(cursor, uid,
                                                          [vals[field]])[0][1])
                        except Exception, e:
                            new_value = (vals[field],
                                         rel_obj.read(cursor, uid, vals[field],
                                            [rel_obj._rec_name])[rel_obj._rec_name])
                    old_value = pre_val[field]
                elif fields_data[field].get('selection', False):
                    for key, value in fields_data[field]['selection']:
                        if key == pre_val[field]:
                            old_value = value
                        if key == vals[field]:
                            new_value = value
                elif fields_data[field].get('type', False) == 'boolean':
                    new_value = vals[field] and 'True' or 'False'
                    old_value = pre_val[field] and 'True' or 'False'
                else:
                    new_value = vals[field]
                    old_value = pre_val[field]
                if old_value == new_value:
                    continue
                change_vals = {'model': model,
                               'obj_id': pre_val['id'],
                               'reference': '%s,%s' % (model, pre_val['id']),
                               'desc': model_obj.name_get(cursor, uid,
                                                          pre_val['id'])[0][1],
                               'field': fields_data[field]['string'],
                               'old_value': old_value,
                               'new_value': new_value}
                self.create(cursor, uid, change_vals)
        return True

    def _get_models(self, cursor, uid, context=None):
        cursor.execute('select m.model, m.name from ir_model m order by m.model')
        return cursor.fetchall()

    _columns = {
        'model': fields.char('Model', size=50),
        'obj_id': fields.integer('ID'),
        'reference': fields.reference('Source Object', selection=_get_models,
                                      size=128),
        'desc': fields.char('Description', size=128),
        'field': fields.char('Field', size=50),
        'old_value': fields.text('Old value'),
        'new_value': fields.text('New value'),
        'create_uid': fields.many2one('res.users', 'User'),
        'create_date': fields.datetime('Create date'),
    }

ChangeLog()