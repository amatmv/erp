# -*- coding: utf-8 -*-
{
    "name": "Change log",
    "description": """Simple audit for fields and model in parameters""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Generic Modules",
    "depends":[
        "base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "change_log_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
