# -*- coding: utf-8 -*-
from __future__ import absolute_import
from base.res.partner import ADDRESS_TYPE_SEL


if 'descarrec' not in dict(ADDRESS_TYPE_SEL):
    ADDRESS_TYPE_SEL.append(('descarrec', 'Descàrrec'))
