# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction


class TestWizardGiscedataDescarrecsComunicatCarrer(testing.OOTestCase):

    def test_poblacions_afectades(self):
        """
        Tests that the poblacions can be changed and is stored on the DB
        :return:
        """

        wiz_model = self.openerp.pool.get('giscedata.descarrecs.comunicat.carrer')
        desc_model = self.openerp.pool.get('giscedata.descarrecs.descarrec')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            desc_data = {
                "data_inici": "2018-01-01",
                "data_final": "2018-01-02",
                "name": "test"
            }
            desc_id = desc_model.create(cursor, uid, desc_data)

            id_wiz = wiz_model.create(cursor, uid, {
            }, context={"active_id": desc_id})
            br_wiz = wiz_model.browse(cursor, uid, id_wiz)

            self.assertEqual(len(br_wiz.poblacions_afectades), 0)
            br_wiz.poblacions_afectades.append(
                {
                    "descarrec_id": desc_id
                }
            )
            self.assertEqual(len(br_wiz.poblacions_afectades), 1)

            ret = br_wiz.poblacions_afectades.pop()
            self.assertEqual(ret, {"descarrec_id": desc_id})
            self.assertEqual(len(br_wiz.poblacions_afectades), 0)


class TestDescarrecToIncidencia(testing.OOTestCase):

    def setUp(self):
        self.txn = Transaction().start(self.database)

    def test_creation_incidencia_from_descarrec(self):
        """
            Test the creation of an incidencia when a descarrec is created
        """
        # loading the database cursor and the user id
        cr = self.txn.cursor
        uid = self.txn.user
        self.maxDiff = None

        # loading the models
        models_obj = self.openerp.pool.get('ir.model.data')
        descarrec_obj = self.openerp.pool.get('giscedata.descarrecs.descarrec')
        incidencia_obj = self.openerp.pool.get('giscedata.qualitat.incidence')
        interval_obj = self.openerp.pool.get('giscedata.qualitat.span')
        affected_customers_obj = self.openerp.pool.get(
            'giscedata.qualitat.affected_customer')
        affected_intallations_obj = self.openerp.pool.get(
            'giscedata.qualitat.affected_installation')

        # getting the id of the element descarrec_01
        descarrec_id = models_obj.get_object_reference(cr, uid,
                                                    'giscedata_descarrecs',
                                                    'descarrec_01')[1]

        # creating the object incidencia en getting back the id of the object
        incidencia_id = descarrec_obj.create_incidencia(cr, uid,
                                                        descarrec_id)

        # getting the attributes of the incidencia with id=incidencia_id
        incidencia_data = incidencia_obj.read(cr, uid, incidencia_id)

        # getting the interval id and the atributes
        interval_id = incidencia_data['spans_ids'][0]
        interval_data = interval_obj.read(cr, uid, interval_id)

        # getting the affected customers and installations ids
        affected_customers_ids = interval_data['affected_customer_ids']
        affected_installations_ids = interval_data['affected_installation_ids']

        # getting the
        affected_customers_data = affected_customers_obj.read(
            cr, uid, affected_customers_ids
        )
        affected_installations_data = affected_intallations_obj.read(
            cr, uid, affected_installations_ids
        )

        incidencia_test_values = {'code': 'Descarrec_01',
                                  'name': 'Descarrec de test 01',
                                  'type_id': (1, 'Programada'),
                                  'origin_id': (2, u'Actuació manual'),
                                  'cause_id': (2, u'Distribución'),
                                  'affected_means': 1,
                                  'descarrec_id': (1, u'Descarrec_01'),
                                  'data': '2017-04-20 04:20:00',
                                  'id': 1,
                                  'spans_ids': [1]}

        intervals_test_values = {'potencia_total_installacions': 0.0,
                                 'begin_date': '2017-04-20 04:20:00',
                                 'name': u'1',
                                 'end_date': '2017-04-20 06:20:00',
                                 'cause': False,
                                 'affected_customer_ids': [1, 2, 3, 4],
                                 'span_incidence': 'Descarrec de test 01',
                                 'incidence_id': (1, 'Descarrec de test 01'),
                                 'potencia_total_clients': 0.0,
                                 'date_create': False,
                                 'affected_installation_ids': [1, 2],
                                 'total_switches': 0,
                                 'total_clients': 4,
                                 'total_installacions': 2,
                                 'duration': 7200,
                                 'user_create': False,
                                 'type': False,
                                 'id': 1,
                                 'switches_ids': [],
                                 'incidence_type': 'Programada'}

        affected_customers_test_values = [
            {'zone_ct_id': False, 'number': False,
             'street': u'Carrer Client 01',
             'id': 1, 'cups': False, 'line': False, 'contracted_power': 0.0,
             'escomesa_type': False, 'codecustomer': False,
             'ct_id': (11, u'CT-0001'), 'data_alta': False,
             'appartment_number': False, 'policy': u'0001',
             'escomesa': False, 'tension': False, 'code_ct': u'CT-0001',
             'contador': False, 'pricelist': False, 'span_id': (1, u'1'),
             'name': u'Client 01', 'level': False, 'codeine': False,
             'y': False, 'x': False},
            {'zone_ct_id': False, 'number': False,
             'street': u'Carrer Client 02',
             'id': 2, 'cups': False, 'line': False, 'contracted_power': 0.0,
             'escomesa_type': False, 'codecustomer': False,
             'ct_id': (11, u'CT-0001'), 'data_alta': False,
             'appartment_number': False, 'policy': u'0002',
             'escomesa': False, 'tension': False, 'code_ct': u'CT-0001',
             'contador': False, 'pricelist': False, 'span_id': (1, u'1'),
             'name': u'Client 02', 'level': False, 'codeine': False,
             'y': False, 'x': False},
            {'zone_ct_id': False, 'number': False,
             'street': u'Carrer Client 03',
             'id': 3, 'cups': False, 'line': False, 'contracted_power': 0.0,
             'escomesa_type': False, 'codecustomer': False,
             'ct_id': (12, u'CT-0002'), 'data_alta': False,
             'appartment_number': False, 'policy': u'0003',
             'escomesa': False, 'tension': False, 'code_ct': u'CT-0002',
             'contador': False, 'pricelist': False, 'span_id': (1, u'1'),
             'name': u'Client 03', 'level': False, 'codeine': False,
             'y': False, 'x': False},
            {'zone_ct_id': False, 'number': False,
             'street': u'Carrer Client 04',
             'id': 4L, 'cups': False, 'line': False, 'contracted_power': 0.0,
             'escomesa_type': False, 'codecustomer': False,
             'ct_id': (12, u'CT-0002'), 'data_alta': False,
             'appartment_number': False, 'policy': u'0001',
             'escomesa': False, 'tension': False, 'code_ct': u'CT-0002',
             'contador': False, 'pricelist': False, 'span_id': (1, u'1'),
             'name': u'Client 04', 'level': False, 'codeine': False,
             'y': False, 'x': False}]

        affected_installations_test_values = [
            {'code_ct': u'CT-0001', 'name': u'Instal·lació 01',
             'zone_ct_id': False, 'type': u'T', 'code_inst': False,
             'codeine': False, 'power': 0.0, 'data': False, 'id': 1,
             'span_id': (1, u'1')},
            {'code_ct': u'CT-0002', 'name': u'Instal·lació 02',
             'zone_ct_id': False, 'type': u'C', 'code_inst': False,
             'codeine': False, 'power': 0.0, 'data': False, 'id': 2,
             'span_id': (1, u'1')}]

        # executing the test comparations
        self.assertEqual(incidencia_data, incidencia_test_values)
        self.assertEqual(interval_data, intervals_test_values)
        self.assertEqual(
            affected_customers_data,
            affected_customers_test_values
        )
        self.assertEqual(affected_installations_data,
                         affected_installations_test_values)

    def tearDown(self):
        self.txn.stop()


class TestPartner(testing.OOTestCaseWithCursor):

    def test_descarrec_in_partner_address_type(self):
        cursor = self.cursor
        uid = self.uid
        pool = self.openerp.pool

        addr_obj = pool.get('res.partner.address')

        type_field = addr_obj.fields_get(cursor, uid, ['type'])['type']
        self.assertIn('descarrec', dict(type_field['selection']))
