# -*- coding: utf-8 -*-

import netsvc


def migrate(cursor, installed_version):
    """
    Script de migració que adapta la taula de la DB a la nova manera
    d'especificar les accions que es realitzen en les maniobres de
    donar-treure en els descarrecs. Abans les accions s'indicaven amb un
    camp selection i ara s'ha creat un model de dades per a guardar aquestes
    accions, tot creat una relacio amb les maniobres.
    """

    logger = netsvc.Logger()
    logger.notifyChannel('migrations', netsvc.LOG_INFO,
                         "Migrant dades de giscedata_descarrecs_operacions_tipus_operacio")

    # Update dels obrirs per el seu actual id de la relacio
    sql_obrir = """
    SELECT id 
    FROM giscedata_descarrecs_operacions_tipus_accio
    WHERE NAME = 'Obrir'
        """

    sql_update_obrir_obertura = """
    UPDATE giscedata_descarrecs_operacions_obertura
    SET id_accio = %s
    WHERE accio='obrir'
    AND id_accio IS NULL
    """

    sql_update_obrir_tancament = """
    UPDATE giscedata_descarrecs_operacions_tancament
    SET id_accio = %s
    WHERE accio='obrir'
    AND id_accio IS NULL
    """

    # Update dels tancars per el seu actual id de la relacio
    cursor.execute(sql_obrir)
    id_obrir = cursor.fetchone()[0]
    cursor.execute(sql_update_obrir_obertura, (id_obrir,))
    cursor.execute(sql_update_obrir_tancament, (id_obrir,))

    sql_tancar = """
    SELECT id 
    FROM giscedata_descarrecs_operacions_tipus_accio
    WHERE NAME = 'Tancar'
    """
    sql_update_tancar_obertura = """
        UPDATE giscedata_descarrecs_operacions_obertura
        SET id_accio = %s
        WHERE accio='tancar'
        AND id_accio IS NULL
        """

    sql_update_tancar_tancament = """
        UPDATE giscedata_descarrecs_operacions_tancament
        SET id_accio = %s
        WHERE accio='tancar'
        AND id_accio IS NULL
        """

    cursor.execute(sql_tancar)
    id_tancar = cursor.fetchone()[0]
    cursor.execute(sql_update_tancar_obertura, (id_tancar,))
    cursor.execute(sql_update_tancar_tancament, (id_tancar,))


up = migrate