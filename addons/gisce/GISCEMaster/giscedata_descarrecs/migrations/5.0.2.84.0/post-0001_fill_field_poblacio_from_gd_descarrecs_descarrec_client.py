# coding=utf-8

import logging


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')

    logger.info('Updating giscedata.descarrecs.descarrec.clients model.')
    logger.info('This update fill the new field \'poblacio\'')

    query_select = '''
        SELECT clients.id, poblacio.id
        FROM giscedata_descarrecs_descarrec_clients AS clients
        LEFT JOIN giscedata_cups_ps AS cups ON (clients.cups = cups.name)
        LEFT JOIN res_poblacio AS poblacio ON (cups.id_poblacio = poblacio.id);
    '''

    cursor.execute(query_select)
    elements_list = cursor.fetchall()

    query_update = '''
        UPDATE giscedata_descarrecs_descarrec_clients
        SET poblacio = %s
        WHERE id = %s
    '''

    for element in elements_list:
        client_id = element[0]
        poblacio = element[1]
        if poblacio:
            cursor.execute(query_update, (poblacio, client_id))


def down(cursor, installed_version):
    pass


migrate = up
