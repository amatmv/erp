# -*- coding: utf-8 -*-
from osv import osv, fields

class giscedata_qualitat_incidence(osv.osv):

    _name = "giscedata.qualitat.incidence"
    _inherit = "giscedata.qualitat.incidence"

    _columns = {
        'descarrec_id': fields.many2one(
            'giscedata.descarrecs.descarrec', 'Descarrec origen')
    }

giscedata_qualitat_incidence()
