# -*- coding: utf-8 -*-
{
    "name": "Descarrecs",
    "description": """Descarrecs

    Variables disponibles para el aviso:
    - descarrec_address_name:
    - descarrec_inici:
    - descarrec_final:
    - descarrec_inici_text:
    - descarrec_final_text:
    - carrer: Calle del cliente
    - num:
    - num_text:
    - hora_inic: Hora de inicio del descargo
    - hora_inici: Hora de inicio del descargo
    - hora_fi: Hora de fin del descargo
    - poblacio: Poblacion del descargo
    - client: Nombre del cliente
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Descarrecs",
    "depends": [
        "c2c_webkit_report",
        "base",
        "giscedata_polissa",
        "giscedata_cts",
        "giscedata_transformadors",
        "crm_generic",
        "giscegis_configsimulacio_at",
        "giscegis_configsimulacio_bt",
        "giscedata_qualitat",
        "poweremail",
        "crm_multi_close",
        "giscegis_simulacio_at",
        "giscegis_simulacio_bt"
    ],
    "init_xml": [],
    "demo_xml":[
        "giscedata_descarrecs_demo.xml"
    ],
    "update_xml":[
        "giscedata_descarrecs_2_incidencia_wizard.xml",
        "giscedata_descarrecs_wizard.xml",
        "giscedata_descarrecs_view.xml",
        "giscedata_descarrecs_sequence.xml",
        "giscedata_descarrecs_data.xml",
        "giscedata_polissa_view.xml",
        "crm_view.xml",
        "giscedata_descarrecs_workflow.xml",
        "giscedata_descarrecs_report.xml",
        "ir.model.access.csv",
        "security/giscedata_descarrecs_security.xml",
        "security/ir.model.access.csv",
        "giscedata_descarrecs_operacions_tipus_accio_data.xml",
        "wizard/enviar_mails_avisos_descarrec_wizard.xml",
        "wizard/enviar_mails_avisos_descarrec_wizard_mailtemplate_data.xml",
        "res_municipi_view.xml",
        "wizard/comunicat_carrer_view.xml",
        "wizard/export_descarrecs_view.xml",
        "giscedata_descarres_exportar_descarrecs_wizard.xml"
    ],
    "active": False,
    "installable": True
}
