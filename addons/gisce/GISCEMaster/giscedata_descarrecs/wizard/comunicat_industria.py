# -*- coding: utf-8 -*-
import wizard
import pooler


def get_adreces(cursor, uid, ids, context=None):
    """
    Gets the list of adreces

    :param cursor: Database cursror
    :param uid: User id
    :type uid: int
    :param ids: Ids of the descarrecs
    :type ids: list of int
    :param context: OpenERP context
    :type context: dict
    :return: List of affected municipis
    :rtype: list of str
    """

    pool = pooler.get_pool(cursor.dbname)
    descarrec_obj = pool.get("giscedata.descarrecs.descarrec")
    inst_obj = pool.get("giscedata.descarrecs.descarrec.installacions")
    cli_obj = pool.get("giscedata.descarrecs.descarrec.clients")
    cts_obj = pool.get("giscedata.cts")

    lst_inst = descarrec_obj.read(
        cursor, uid, ids[0], ["installacions"])["installacions"]
    lst_clients = descarrec_obj.read(
        cursor, uid, ids[0], ["clients"])["clients"]
    ret = []
    if lst_inst:
        for inst_id in lst_inst:
            nom_ct = inst_obj.read(cursor, uid, inst_id, ["code_ct"])
            search_param = [("name", "=", nom_ct["code_ct"])]
            id_ct = cts_obj.search(cursor, uid, search_param)
            ct = cts_obj.read(cursor, uid, id_ct, ["adreca"])
            if ct[0]["adreca"]:
                municipi = ct[0]["adreca"]
                ret.append(municipi)
    else:
        for client in lst_clients:
            nom_ct = cli_obj.read(cursor,uid, client, ["code_ct"])
            search_param = [("name", "=", nom_ct["code_ct"])]
            id_ct = cts_obj.search(cursor, uid, search_param)
            ct = cts_obj.read(cursor, uid, id_ct, ["adreca"])
            if ct[0]["adreca"]:
                municipi = ct[0]["adreca"]
                ret.append(municipi)
    return list(set(ret))


def get_municipis_afectats(cursor, uid, ids, context=None):
    """
    Gets the list of municipi

    :param cursor: Database cursror
    :param uid: User id
    :param ids: Ids of the descarrecs
    :param context: OpenERP context
    :return: List of affected municipis
    :rtype: list of str
    """

    pool = pooler.get_pool(cursor.dbname)
    descarrec_obj = pool.get("giscedata.descarrecs.descarrec")
    inst_obj = pool.get("giscedata.descarrecs.descarrec.installacions")
    cli_obj = pool.get("giscedata.descarrecs.descarrec.clients")
    cts_obj = pool.get("giscedata.cts")

    lst_inst = descarrec_obj.read(
        cursor, uid, ids[0], ["installacions"])["installacions"]
    lst_clients = descarrec_obj.read(
        cursor, uid, ids[0], ["clients"])["clients"]
    ret = []
    if lst_inst:
        for inst_id in lst_inst:
            nom_ct = inst_obj.read(cursor, uid, inst_id, ["code_ct"])
            search_param = [("name", "=", nom_ct["code_ct"])]
            id_ct = cts_obj.search(cursor, uid, search_param)
            ct = cts_obj.read(cursor, uid, id_ct, ["id_municipi"])
            municipi = " ".join(ct[0]["id_municipi"][1].split(",")[::-1])
            ret.append(municipi)
    else:
        for client in lst_clients:
            nom_ct = cli_obj.read(cursor,uid, client, ["code_ct"])
            search_param = [("name", "=", nom_ct["code_ct"])]
            id_ct = cts_obj.search(cursor, uid, search_param)
            ct = cts_obj.read(cursor, uid, id_ct, ["id_municipi"])
            municipi = " ".join(ct[0]["id_municipi"][1].split(",")[::-1])
            ret.append(municipi)
    return list(set(ret))


def _init(self, cr, uid, data, context={}):
    """
    Carreguem el text que ja està guardat a la base de dades

    :param self:
    :param cr: Database cursor
    :param uid: User id
    :type uid: int
    :param data: Descarrec data
    :type data: dict
    :param context: OpenERP context
    :type context: dict
    :return: Data to use in report
    :rtype: dict
    """

    descarrec_obj = pooler.get_pool(cr.dbname).get(
        'giscedata.descarrecs.descarrec')
    descarrec = descarrec_obj.browse(cr, uid, data['id'], context)
    causes = descarrec.causes
    municipis_afectats = get_municipis_afectats(cr, uid, [data["id"]], context)
    adreces = get_adreces(cr, uid, [data["id"]], context)
    xarxa = descarrec.xarxa
    linies_bt = {}
    for client in descarrec.clients:
        if client['code_ct'] in linies_bt:
            if client['line'] not in linies_bt[client['code_ct']]:
                temp = linies_bt[client['code_ct']]
                temp.append(client['line'])
                linies_bt[client['code_ct']] = temp
        else:
            linies_bt[client['code_ct']] = [client['line']]

    if not causes:
        causes = descarrec.descripcio
    ret = {
        'nuclis_afectats': descarrec.nuclis_afectats,
        'causes': causes,
        'cts_afectats': descarrec.cts_afectats,
        "clients": len(descarrec.clients),
        "num_municipis": len(municipis_afectats),
        "municipis_afectats": municipis_afectats,
        "num_descarrec": descarrec.name,
        'adreces': ",".join(adreces),
        "tramitacio": descarrec.sollicitud.tramitacio,
        'poblacions': ",".join(municipis_afectats),
        "xarxa": xarxa,
        "linies_bt": linies_bt,
    }

    if descarrec.data_inici:
        ret["inici"] = descarrec.data_inici.split(" ")

    else:
        ret["inici"] = descarrec.data_inici

    if descarrec.data_final:
        ret["fi"] = descarrec.data_final.split(" ")
    else:
        ret["fi"] = descarrec.data_final
    return ret

_init_form = """<?xml version="1.0"?>
<form string="Comunicat Indústria">
  <separator string="CTS afectats" colspan="4" />
  <field name="cts_afectats" nolabel="1" colspan="4" />
  <separator string="Descripció dels treballs" colspan="4" />
  <field name="causes" nolabel="1" colspan="4" width="500" height="100"/>
</form>"""

_init_fields = {
    'nuclis_afectats': {'string': 'Nuclis afectats', 'type': 'text'},
    'causes': {'string': 'Causes', 'type': 'text'},
    'cts_afectats': {'string': 'CTS afectats', 'type': 'text',
                     'readonly': True},
}


def _save_text(self, cr, uid, data, context=None):
    """
    Guardem el text que ens han introduït amb el formulari

    :param self:
    :param cr:
    :param uid:
    :param data:
    :param context:
    :return:
    """
    if context is None:
        context = {}
    descarrec_obj = pooler.get_pool(cr.dbname).get('giscedata.descarrecs.descarrec')
    descarrec_obj.write(cr, uid, [data['id']], {'nuclis_afectats': data['form']['nuclis_afectats'],'causes': data['form']['causes']})
    return {}


def _print(self, cr, uid, data, context={}):
    return {'ids': [data['id']]}


class giscedata_descarrecs_comunicat_industria(wizard.interface):
    states = {
        'init': {
            'actions': [_init],
            'result': {'type': 'form', 'arch': _init_form,
                       'fields': _init_fields,
                       'state': [('end', 'Cancelar', 'gtk-cancel'),
                                 ('save_text', 'Imprimir', 'gtk-print')]}
        },
        'save_text': {
            'actions': [_save_text],
            'result': {'type': 'state', 'state': 'print'}
        },
        'print': {
            'actions': [_print],
            'result': {'type': 'print', 'report':
                'giscedata.descarrecs.comunicat_industria',
                       'get_id_from_action': True,
                       'state': 'end'}
        },
    }


giscedata_descarrecs_comunicat_industria('giscedata.descarrecs.comunicat.industria')
