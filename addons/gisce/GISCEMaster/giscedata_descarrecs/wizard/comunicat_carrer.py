# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _


class GiscedataDescarrecsComunicatCarrer(osv.osv_memory):

    _name = 'giscedata.descarrecs.comunicat.carrer'

    _rec_name = 'descarrec_name'

    def _get_carrers_afectats(self, cursor, uid, ids, context=None):
        """
        Get data of the afected streets

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: Involved elements ids
        :type ids: list of int
        :param context: OpenERP context
        :type context: dict[str, Any]
        :return: Afected streets
        :rtype: dict[str, str]
        """
        if context is None:
            context = {}

        ret = {}
        descarrec_obj = self.pool.get("giscedata.descarrecs.descarrec")
        desc_client_obj = self.pool.get(
            "giscedata.descarrecs.descarrec.clients"
        )
        desc_id = context.get('active_id', False)
        clients = descarrec_obj.read(
            cursor, uid, desc_id, ["clients"], context
        )["clients"]
        clients_data = desc_client_obj.read(
            cursor, uid, clients, ["street", "codeine", "poblacio", "number"]
        )

        for client in clients_data:
            carrer = client["street"].split(",")[0]
            num = client["number"]
            if client["codeine"] and client["poblacio"]:
                mun = " ".join(
                    map(str.strip, str(client["codeine"][1]).split(","))[::-1]
                )
                pob = " ".join(
                    map(str.strip, str(client["poblacio"][1]).split(","))[::-1]
                )
                if "{};{}".format(mun, pob) in ret:
                    if carrer in ret["{};{}".format(mun, pob)]:
                        num = str.strip(str(num))
                        if num and (num not in ret["{};{}".format(
                                mun, pob)][carrer]):
                            if num not in ret["{};{}".format(mun, pob)][carrer]:
                                ret["{};{}".format(mun, pob)][carrer].append(
                                    num)
                    else:
                        ret["{};{}".format(mun, pob)][carrer] = [num]
                else:
                    ret["{};{}".format(mun, pob)] = {carrer: [num]}
        return ret

    def _load_poblacions_afectades(self, cursor, uid, ids, context=None):
        """
        Empty function used to refresh the view.
        :param cursor: Database cursor
        :param uid: User ID
        :type uid: int
        :param ids: Involved elements IDS
        :type ids: list of int
        :param context: OpenERP Context
        :type context: dict[str, Any]
        """
        pass

    def _generate_poblacions_afectades(self, cursor, uid, ids, context=None):
        """
        Create the poblacions afectades and link to their descarrec, used to set
        the poblacions afectades values to its defaults values.
        :param cursor: Database cursor
        :param uid: User ID
        :type uid: int
        :param ids: Involved elements IDS
        :type ids: list of int
        :param context: OpenERP Context
        :type context: dict[str, Any]
        """
        carrers_afect_obj = self.pool.get(
            "giscedata.descarrecs.descarrec.carrers.afectats"
        )
        poblacio_obj = self.pool.get("res.poblacio")
        desc_id = context.get('active_id', False)

        # Busquem si ja hi ha carrers afectats creats, i en el cas d'existir,
        # els eliminem.
        carrers_afect_ids = carrers_afect_obj.search(
            cursor, uid, [("descarrec_id", "=", desc_id)]
        )
        if len(carrers_afect_ids) > 0:
            carrers_afect_obj.unlink(cursor, uid, carrers_afect_ids)

        data_carrers = self._get_carrers_afectats(
            cursor, uid, desc_id, context
        )
        for mun_pob in data_carrers:
            mun_name, pob_name = mun_pob.split(';')
            id_pob = poblacio_obj.search(
                cursor, uid, [('name', '=', pob_name)]
            )[0]
            carr_afect = ""
            for carr_name in data_carrers[mun_pob]:
                carr_afect += carr_name + ": "
                temp = []
                for x in data_carrers[mun_pob][carr_name]:
                    try:
                        temp.append(int(x))
                    except:
                        temp.append(x)
                data_carrers[mun_pob][carr_name] = temp
                for nums in sorted(data_carrers[mun_pob][carr_name]):
                    carr_afect += str(nums) + ","
                carr_afect = carr_afect[:-1]
                carr_afect += "\n"

            # Creem els carrers afectats novament.
            carrers_afect_obj.create(
                cursor,
                uid,
                {'pob_name': mun_name + ": " + pob_name,
                 'mun_name': mun_name,
                 'descarrec_id': desc_id,
                 'poblacio_id': id_pob,
                 'carrers_afectats': carr_afect,
                 }
            )

    def _fnct_set_poblacions(self, cursor, uid, ids, field_name, values, args,
                             context=None):
        """
        Save the changed values of the field into the DB.

        :param cursor: Database cursor
        :param uid: User ID
        :type uid: int
        :param ids: Involved elements IDS
        :type ids: list of int
        :param field_name: Name of the objective field.
        :type field_name: str
        :param values: Value of the objective field.
        :type values: list of list of int or dict[str, str or int]
        :param args: Function arguments
        :type args: dict[str,Any]
        :param context: OpenERP Context
        :type context: dict[str, Any]
        """

        if context is None:
            context = {}
        carrers_afect_obj = self.pool.get(
            "giscedata.descarrecs.descarrec.carrers.afectats"
        )
        for value in values:
            poblacio_id = value[1]
            if value[0] == 0:
                carrers_afect_obj.write(cursor, uid, poblacio_id, value[2])
            elif value[0] == 1:
                carrers_afect_obj.write(cursor, uid, poblacio_id, value[2])
            elif value[0] == 2:
                carrers_afect_obj.unlink(cursor, uid, value[1])


    def _fnct_get_poblacions(self, cursor, uid, ids, field_name, args,
                             context=None):
        """
        Load the values of the field.
        :param cursor: Database cursor
        :param uid: User ID
        :type uid: int
        :param ids: Involved elements IDS
        :type ids: list of int
        :param field_name: Name of the objective field.
        :type field_name: str
        :param args: Function arguments
        :type args: dict[str,Any]
        :param context: OpenERP Context
        :type context: dict[str, Any]
        """

        if context is None:
            context = {}

        carrers_afect_obj = self.pool.get(
            "giscedata.descarrecs.descarrec.carrers.afectats"
        )

        desc_id = context.get('active_id', False)

        # comprovem si hi el descarrec ja té els carrers afectats creats
        carrers_afect_ids = carrers_afect_obj.search(
            cursor, uid, [("descarrec_id", "=", desc_id)]
        )

        if len(carrers_afect_ids) > 0:
            # El descarrec ja te els carrers afectats creats.
            return {ids[0]: carrers_afect_ids}
        else:
            return {ids[0]: []}

    def _descarrec_name(self, cr, uid, context=None):
        """
        Set the default value of the descarrec_name field of the element
        :param cr: Database cursor
        :param uid: User ID
        :type uid: int
        :param context: OpenERP Context
        :type context: dict[str, Any]
        """
        if context is None:
            context = {}

        descarrec_obj = self.pool.get("giscedata.descarrecs.descarrec")
        descarrec_name = descarrec_obj.read(
            cr, uid, context.get('active_id'), ["name"])["name"]
        return descarrec_name

    def _primer_paragraf(self, cr, uid, context=None):
        """
        Set the default value of the p_paragraf field of the element
        :param cr: Database cursor
        :param uid: User ID
        :type uid: int
        :param context: OpenERP Context
        :type context: dict[str, Any]
        """
        if context is None:
            context = {}
        descarrec_obj = self.pool.get("giscedata.descarrecs.descarrec")
        p_paragraf = descarrec_obj.read(
            cr,
            uid,
            context.get('active_id'),
            ["p_paragraf"]
        )["p_paragraf"]
        return p_paragraf

    def _print_report(self, cursor, uid, ids, context=None):
        """
        Empty function used to refresh the view.
        :param cursor: Database cursor
        :param uid: User ID
        :type uid: int
        :param ids: Involved elements IDS
        :type ids: list of int
        :param context: OpenERP Context
        :type context: dict[str, Any]
        """
        if context is None:
            context = {}
        poblacions_afect_obj = self.pool.get(
            "giscedata.descarrecs.descarrec.carrers.afectats")
        wizard_obj = self.pool.get("giscedata.descarrecs.comunicat.carrer")
        descarrec_obj = self.pool.get("giscedata.descarrecs.descarrec")
        wiz = wizard_obj.read(
            cursor,
            uid,
            ids[0],
            ['poblacions_afectades',
             'p_paragraf',
             ],
            context=context
        )[0]
        data = {
            'poblacions': {},
            'model': 'giscedata.descarrecs.descarrec'
        }
        for pob_afect_id in wiz['poblacions_afectades']:
            pob_afect = poblacions_afect_obj.read(
                cursor,
                uid,
                pob_afect_id,
                ['pob_name', 'mun_name', 'carrers_afectats']
            )
            pob_name = pob_afect['pob_name']
            afect_carrers = pob_afect['carrers_afectats']
            data['poblacions'][pob_name] = afect_carrers
        descarrec_obj.write(
            cursor, uid, context.get('active_ids'), {
                'p_paragraf': wiz['p_paragraf']
            }
        )

        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'giscedata.descarrecs.fulls.penjar',
            'datas': data,
            'ids': context.get('active_ids', False)
        }

    _columns = {
        'poblacions_afectades': fields.function(
            _fnct_get_poblacions,
            method=True,
            string=_('Poblacions i carrers afectats'),
            relation='giscedata.descarrecs.descarrec.carrers.afectats',
            type='one2many',
            fnct_inv=_fnct_set_poblacions
        ),
        'p_paragraf': fields.text(_('Primer paràgraf')),
        'descarrec_name': fields.char(
            _('Codi del descàrrec'),
            size=50
        ),
        'state': fields.selection(
            [
                ('init', 'Init'),
                ('save_poblacions', 'Save poblacions'),
                ('print', 'Print')
            ],
            _('Estat del wizard')
        ),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'p_paragraf': _primer_paragraf,
        'descarrec_name': _descarrec_name,
    }


GiscedataDescarrecsComunicatCarrer()
