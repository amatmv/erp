# -*- coding: utf-8 -*-
from osv import osv, fields
import time
import base64


class GiscedataDescarrecsDescarrecExport(osv.osv_memory):

    _name = 'giscedata.descarrecs.descarrec.export'
    _rec_name = 'descarrec_name'

    def _generar_fitxer(self, cursor, uid, ids, context=None):
        """
        Attends the generar fitxer from the wizard

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: Wizard id
        :type ids: list of int
        :param context: OpenERP context
        :return: None
        :rtype: None
        """

        obj_desc = self.pool.get("giscedata.descarrecs.descarrec")
        descarrecs_date = self.read(cursor, uid, ids, ["date"], context=context)
        file_data = obj_desc.export_descarrecs(
            cursor, uid, descarrecs_date[0]["date"],
            format="sqlite", context=context)
        write_data = {
            'filename': "descarrecs.db",
            "file": file_data,
            "state": "done"
        }
        self.write(cursor, uid, ids, write_data)

    _columns = {
        'date': fields.date('Data'),
        'file': fields.binary('Descarrecs exportats'),
        'filename': fields.char('File name', size=64),
        'state': fields.char('State', size=16),
    }

    _defaults = {
        'date': lambda *a: time.strftime('%Y-%m-%d')
    }


GiscedataDescarrecsDescarrecExport()
