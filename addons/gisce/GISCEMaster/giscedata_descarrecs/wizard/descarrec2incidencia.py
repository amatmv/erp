# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _

class descarrec2incidencia(osv.osv_memory):
    _name = 'giscedata.descarrec.2.incidencia'

    def default_get(self, cr, uid, fields_list, context=None):

        descarrec = context.get('active_ids')

        if len(descarrec) == 1:
            descarrec = descarrec[0]
            incidencia_obj = self.pool.get('giscedata.qualitat.incidence')
            search_params = [('descarrec_id', '=', descarrec)]
            found = incidencia_obj.search(cr, uid, search_params, 0)
            if found:
                raise osv.except_osv(
                    'Error',
                    _(u"Ja s'ha creat una incidència a partir "
                      u"d'aquest descàrrec.")
                )
                # return {
                #     'domain': [('id', '=', found[0])],
                #     'name': _('Incidència generada'),
                #     'view_type': 'form',
                #     'view_mode': 'tree,form',
                #     'res_model': 'giscedata.qualitat.incidence',
                #     'type': 'ir.actions.act_window'
                # }
                # self.write({
                #     'state': 'end',
                # })
            else:
                return super(descarrec2incidencia,
                         self).default_get(cr, uid, fields_list,
                                           context=context)
        else:
            raise osv.except_osv(
                'Error',
                _(u"S'està intentant crear una incidència a partir de més "
                  u"d'un descàrrec a l'hora. Selecciona  només un element.")
            )

    # def create(self, cr, uid, vals, context=None):
    #
    #     descarrec = context.get('active_ids')
    #     if len(descarrec) == 1:
    #         descarrec = descarrec[0]
    #         incidencia_obj = self.pool.get('giscedata.qualitat.incidence')
    #         search_params = [('descarrec_id', '=', descarrec)]
    #         found = incidencia_obj.search(cr, uid, search_params, 0)
    #
    #         if found:
    #             raise osv.except_osv(
    #                 'Error',
    #                 _(u"Ja s'ha creat una incidència a partir "
    #                   u"d'aquest descàrrec.")
    #             )
    #
    #         super(descarrec2incidencia, self).create(cr, uid, vals,
    #                                                  context=context)
    #     else:
    #         raise osv.except_osv(
    #             'Error',
    #             _(u"S'està intentant crear una incidència de més"
    #               u"d'un descàrrec a l'hora")
    #         )

    def generar_incidencia(self, cursor, uid, ids, context=None):

        wizard = self.browse(cursor, uid, ids[0], context)
        descarrec = wizard.descarrec_sel['id']
        incidencia_obj = self.pool.get('giscedata.qualitat.incidence')
        search_params = [('descarrec_id', '=', descarrec)]
        found = incidencia_obj.search(cursor, uid, search_params, 0)
        if found:
            raise osv.except_osv(
                'Error',
                _(u"Ja s'ha creat una incidència a partir d'aquest descàrrec.")
            )
        else:
            descarrec_obj = self.pool.get('giscedata.descarrecs.descarrec')
            incidencia_id = descarrec_obj.create_incidencia(cursor, uid,
                                                            descarrec)
            wizard.write({
                'state': 'end',
                'incidencia_id': incidencia_id
            })

    def obrir_incidencia_creada(self, cursor, uid, ids, context=None):

        wizard = self.browse(cursor, uid, ids[0], context)
        incidencia_id = wizard.incidencia_id

        return {
            'domain': [('id', '=', incidencia_id)],
            'name': _('Incidència generada'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.qualitat.incidence',
            'type': 'ir.actions.act_window'
        }

    def _descarrec(self, cursor, uid, context=None):
        if len(context.get('active_ids')) == 1:
            return context.get('active_id')
        else:
            return False

    _columns = {
        'state': fields.selection(
            [('init', 'Init'), ('end', 'End'), ], 'State'
        ),
        'descarrec_sel': fields.many2one(
            'giscedata.descarrecs.descarrec',
            'Descarrecs per generar incidencia',
            required=True),
        'incidencia_id': fields.char('Codi',size=50)
    }

    _defaults = {
        'state': lambda *a: 'init',
        'descarrec_sel': _descarrec,
    }

descarrec2incidencia()
