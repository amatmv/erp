# -*- coding: utf-8 -*-
import wizard


def _mostrar(self, cr, uid, data, context={}):
  cr.execute("select c.id from crm_case c, crm_case_categ cc, crm_case_section cs where ref = %s and c.section_id = cs.id and cs.code = 'des' and c.categ_id = cc.id and cc.categ_code = 'des-cm' and c.state = 'open'", ('giscedata.descarrecs.descarrec,%i' % data['id'],))
  ids = map(lambda x: x[0], cr.fetchall())

  cr.execute("select id from ir_ui_view where name = 'crm.case.descarrecs.tree'")
  view_id = cr.fetchone()

  action = {
    'domain': "[('id','in', ["+','.join(map(str,map(int, ids)))+"])]",
		'view_type': 'form',
		'view_mode': 'tree,form',
		'res_model': 'crm.case',
		'view_id': False,
    'limit': len(ids),
		'type': 'ir.actions.act_window'
  }
  return action

class wizard_mostrar_avisos_pendents_mail(wizard.interface):

  states = {
    'init': {
    	'actions': [],
      'result': {'type': 'state', 'state': 'mostrar'}
    },
    'mostrar': {
    	'actions': [],
    	'result': {'type': 'action', 'action': _mostrar, 'state': 'end'}
    },
  }

wizard_mostrar_avisos_pendents_mail('mostar.avisos.pendents.mail')
