# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _
from datetime import datetime
import babel.dates
import netsvc


class GiscedataEnviarMailsAvisosDescarrecs(osv.osv_memory):

    _name = 'giscedata.enviar.mails.avisos.descarrecs'

    def default_get(self, cr, uid, fields_list, context=None):
        """
        Return the default values of the fields in fields_list.
        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param fields_list: Fields to return the default value.
        :type fields_list: list of str
        :param context: OpenERP context
        :type context: dict[str, Any]
        :return: The default value of the model fields in the fields_list
        :rtype: dict[str, str]
        """
        if context is None:
            context = {}
        descarrec_obj = self.pool.get("giscedata.descarrecs.descarrec")
        clients_af_obj = self.pool.get("giscedata.descarrecs.descarrec.clients")
        municipi_obj = self.pool.get("res.municipi")
        partner_obj = self.pool.get("res.partner")
        partner_address_obj = self.pool.get("res.partner.address")

        if len(context.get('active_ids')) == 1:
            descarrec_id = context.get('active_id')
        else:
            return False

        fields_to_default = self._columns.keys()
        if 'mails' not in fields_list:
            return {
                "status1": self._get_status1(cr, uid, context),
                "status2": self._get_status2(cr, uid, context)
            }
        superdefault = super(GiscedataEnviarMailsAvisosDescarrecs, self
                             ).default_get(cr, uid, fields_to_default, context)

        clients = descarrec_obj.read(cr, uid, descarrec_id, ['clients'])
        municipis_afectats_ids = []
        mails_municipis_afect = ""

        for client_id in clients['clients']:
            client = clients_af_obj.read(cr, uid, client_id, ['codeine'])
            if client['codeine']:
                if client['codeine'][0] not in municipis_afectats_ids:
                    municipis_afectats_ids.append(client['codeine'][0])
                    ajuntament_id = municipi_obj.read(
                        cr, uid, client['codeine'][0], ['ajuntament_id']
                    )['ajuntament_id'][0]
                    ajuntament_address_ids = partner_obj.read(
                        cr, uid, ajuntament_id, ['address']
                    )['address']
                    mails_municipis_afect += "\n### {} ###\n".format(
                        client['codeine'][1]
                    )
                    for address_id in ajuntament_address_ids:
                        address_data = partner_address_obj.read(
                            cr, uid, address_id, ['email', 'name', 'type']
                        )
                        if (address_data['email'] and
                                address_data['type'] == "descarrec"):
                            mails_municipis_afect += "{}: {}\n".format(
                                address_data.get('name', 'desconegut'),
                                address_data['email']
                            )
        mails = (superdefault.get("mails", "") or "") + mails_municipis_afect
        superdefault.update({"mails": mails})
        return superdefault

    def enviar_mails(self, cr, uid, ids, context=None):
        """
        Configure and set the mail sender wizard, format the mails content for
        each entry in the field "mails" of the model and send it.
        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: Wizard id
        :type ids: list of int
        :param context: OpenERP context
        :type context: dict[str, Any]
        """
        if context is None:
            context = {}

        wizard = self.browse(cr, uid, ids[0], context)
        pwe_tmpl_obj = self.pool.get('poweremail.templates')
        pwe_send_wiz_obj = self.pool.get('poweremail.send.wizard')
        imd_obj = self.pool.get('ir.model.data')
        descarrec_obj = self.pool.get('giscedata.descarrecs.descarrec')
        company_obj = self.pool.get("res.company")

        descarrec_id = context.get('active_id')
        template_id = imd_obj.get_object_reference(
            cr,
            uid,
            'giscedata_descarrecs',
            'template_avis_descarrec_entitats_publiques'
        )[1]

        template = pwe_tmpl_obj.browse(cr, uid, template_id)
        descarrec_data = descarrec_obj.read(
            cr, uid, descarrec_id, ['name', 'data_inici', 'data_final']
        )
        data_inici, hora_inici = descarrec_data["data_inici"].split(' ')
        dtf = datetime.strptime(data_inici, '%Y-%m-%d')
        dtf = dtf.strftime("%y%m%d")
        dt = datetime.strptime(dtf, '%y%m%d')
        data_inici = babel.dates.format_datetime(
            dt, 'd LLLL Y', locale=context.get("lang", "es_ES")
        )
        hora_inici = datetime.strptime(
            hora_inici, "%H:%M:%S"
        ).strftime('%H:%M')
        hora_final = descarrec_data['data_final'].split(' ')[1]
        hora_final = datetime.strptime(
            hora_final, "%H:%M:%S"
        ).strftime('%H:%M')
        company_name = company_obj.read(cr, uid, 1, ["name"])["name"]

        mail_from = False
        if template.enforce_from_account:
            mail_from = template.enforce_from_account.id
        if not mail_from or not isinstance(mail_from, int):
            mail_from = context.get('use_mail_account', False)
            if not mail_from or not isinstance(mail_from, int):
                raise osv.except_osv(
                    _('Error al enviar els correus!'),
                    _("Compte d'enviament del correu no definit.")
                )

        ctx = {'active_ids': [descarrec_id], 'active_id': descarrec_id,
               'template_id': template_id, 'src_rec_ids': [descarrec_id],
               'src_model': 'giscedata.descarrecs.descarrec',
               'from': mail_from,
               'state': 'single', 'priority': '0',
               'email_to': 'default_email_to',
               'body_name_to': 'default_body_name_to',
               'desc_name': descarrec_data['name'],
               'data_desc': data_inici,
               'h_ini_desc': hora_inici,
               'h_fi_desc': hora_final,
               'company_name': company_name
               }

        try:
            for line in wizard.mails.splitlines():
                line_strip = line.strip()
                if not line_strip:
                    continue
                if line_strip.startswith('#'):
                    continue
                client_name, client_email = line_strip.split(':')
                client_email = client_email.strip()
                ctx['email_to'] = client_email
                ctx['body_name_to'] = client_name
                params = {'state': 'single', 'priority': '0',
                          'from': ctx['from']}
                pwe_send_wiz_id = pwe_send_wiz_obj.create(cr, uid, params, ctx)
                pwe_send_wiz_obj.write(
                    cr, uid, [pwe_send_wiz_id], {'to': client_email}
                )
                pwe_send_wiz_obj.send_mail(cr, uid, [pwe_send_wiz_id], ctx)
        except Exception as e:
            logger = netsvc.Logger()
            logger.notifyChannel(
                'objects', netsvc.LOG_ERROR,
                "giscedata_descarrecs_avisos_generics:{}.".format(e.message)
            )
            raise osv.except_osv(_('Error al enviar els correus!'), e.message)

        wizard.write({
            'state': 'send'
        })

    def _get_status1(self, cursor, uid, context=None):
        """
        Return the default value for the field status1
        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param context: OpenERP context
        :type context: dict[str, Any]
        :return: The default value of the field status1
        :rtype: str
        """

        txt = _("Recopilats els correus electrònics de les" +
                " entitats públiques a avisar.")
        return txt

    def _get_status2(self, cursor, uid, context=None):
        """
        Return the default value for the field status2
        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param context: OpenERP context
        :type context: dict[str, Any]
        :return: The default value of the field status2
        :rtype: str
        """
        txt = _("Avisos enviats correctament a les entitats " +
                "públiques especificades.")
        return txt

    _columns = {
        'status1': fields.text('Estat del proces 1'),
        'status2': fields.text('Estat del proces 2'),
        'state': fields.selection(
            [
                ('init', 'Init'),
                ('send', 'Send')
            ], 'Estat del wizard'
        ),
        'mails': fields.text('Mails'),
    }
    _defaults = {
        'state': lambda *a: 'init',
        'status1': _get_status1,
        'status2': _get_status2
    }


GiscedataEnviarMailsAvisosDescarrecs()
