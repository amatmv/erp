# -*- coding: utf-8 -*-
from osv import osv, fields
import netsvc
from datetime import datetime
import babel.dates
import sqlite3
import tempfile
import os
import base64
from tools.translate import _


_sollicitud_states = [
  ('draft', 'Esborrany'),
  ('per_simular', 'Per simular'),
  ('simulant', 'Simulant'),
  ('done', 'Done')
]

_xarxa_selection = [
  ('1', 'AT'),
  ('2', 'BT')
]


class GiscedataDescarrecsSollicitud(osv.osv):
    """
    Sol·licitud de descarrec
    """

    _name = "giscedata.descarrecs.sollicitud"
    _description = "Descarrecs Solicitut"

    _tramitacio = (
      ('N', 'Normal'),
      ('U', 'Urgent'),
      ('I', 'Immediata'),
    )

    def create(self, cr, uid, vals, context={}):
        vals['name'] = self.pool.get('ir.sequence').get(cr, uid, 'giscedata.descarrecs.sollicitud')
        return super(osv.osv, self).create(cr, uid, vals, context)

    def _timespan_diff(self, cr, uid, ids, name, arg, context={}):
        res = {}
        for id in ids:
            id = int(id)
            cr.execute("select extract('epoch' from data_final - data_inici) as time from giscedata_descarrecs_sollicitud where id = %s", (id,))
            diff = cr.fetchone()
            if len(diff) and diff[0]:
                res[id] = int(diff[0])
            else:
                res[id] = 0
        return res

    _columns = {
        'name': fields.char('Codi', size=10, readonly=True),
        'descripcio': fields.char(
            'Descripció', size=255, readonly=True,
            required=True, states={'draft': [('readonly', False)]}
        ),
        'xarxa': fields.selection(
            _xarxa_selection, 'Xarxa afectada', readonly=True,
            states={'draft': [('readonly', False)]}, required=True
        ),
        'cap_instalacio': fields.many2one(
            'res.partner.address', 'Cap Instalació',
            readonly=True, states={'draft': [('readonly', False)]},
            required=True
        ),
        'centre_control': fields.many2one('res.partner.address', 'Centre Control', readonly=True, states={'draft': [('readonly', False)]}, required=True),
        'data_inici': fields.datetime('Inici', required=True, readonly=True, states={'draft': [('readonly', False)]}),
        'data_final': fields.datetime(
            'Final', required=True, readonly=True,
            states={'draft': [('readonly', False)]}
        ),
        'solicitant': fields.many2one(
            'res.partner.address', 'Solicitant',
            required=True, readonly=True, states={'draft': [('readonly', False)]}
        ),
        'treball': fields.text(
            'Treballs Previstos', readonly=True,
            states={'draft': [('readonly', False)]}
        ),
        'instalacion_afectada': fields.text(
              'Instal·lació a deixar sense tensió', readonly=True,
              states={'draft': [('readonly', False)]}
        ),
        'observacions': fields.text('Observacions', readonly=True, states={'draft': [('readonly', False)]}),
        'duration': fields.function(_timespan_diff, method=True, type='integer', string='Periode (Segons)', readonly=True, states={'draft': [('readonly', False)]}),
        'operacions_obertura': fields.one2many('giscedata.descarrecs.operacions.obertura', 'sollicitud_id', 'Operacions Obertura', readonly=True, states={'draft': [('readonly', False)]}),
        'operacions_tancament': fields.one2many('giscedata.descarrecs.operacions.tancament', 'sollicitud_id', 'Operacions Tancament', readonly=True, states={'draft': [('readonly', False)]}),
        'observacions_obertura': fields.text(
            'Observacions d\'obertura', readonly=True,
            states={'draft': [('readonly', False)]}
        ),
        'observacions_tancament': fields.text(
            'Observacions de tancament', readonly=True,
            states={'draft': [('readonly', False)]}
        ),
        'state': fields.selection(_sollicitud_states, 'Estat', readonly=True),
        'tramitacio': fields.selection(_tramitacio, 'Tramitació'),
        'modificacio_esquema': fields.boolean('Modificació de l\'esquema i/o caracterítiques d\'equips i/o instal.lacions'),
        'agent_descarrec': fields.many2one('res.partner.address', 'Agent del descàrrec', readonly=True, states={'draft': [('readonly', False)]}),
        'cap_de_treball': fields.many2one('res.partner.address', 'Cap de treball', readonly=True, states={'draft': [('readonly', False)]}),
    }

    _order = "name"

    _defaults = {
      'state': lambda *a: 'draft',
    }

    def simulant(self, cr, uid, ids, context={}):
        for id in ids:
            wf_service = netsvc.LocalService("workflow")
            wf_service.trg_validate(uid, 'giscedata.descarrecs.sollicitud', id, 'simulant', cr)
            return self.browse(cr, uid, id).state

    def done(self, cr, uid, ids, context={}):
        for id in ids:
            wf_service = netsvc.LocalService("workflow")
            wf_service.trg_validate(uid, 'giscedata.descarrecs.sollicitud', id, 'done', cr)
            return self.browse(cr, uid, id).state


GiscedataDescarrecsSollicitud()


class GiscedataDescarrecsOperacionsTipusAccio(osv.osv):
    """
    This class implements the different kinds of actions aviable to do in the
    descarrec maniobres. This give to the user the possibility to configure
    their own kind of actions to do in the descarrec maniobres.
    """
    _name = 'giscedata.descarrecs.operacions.tipus.accio'

    _columns = {
        'name': fields.char('Acció', size=256, required=True, translate=True)
    }


GiscedataDescarrecsOperacionsTipusAccio()


class GiscedataDescarrecsOperacionsObertura(osv.osv):

    _name = 'giscedata.descarrecs.operacions.obertura'

    _columns = {
        'name': fields.char('Element', size=60),
        'ct': fields.char('CT', size=60),
        'descripcio': fields.text('Descripció'),
        'sequence': fields.integer('Ordre'),
        'sollicitud_id': fields.many2one('giscedata.descarrecs.sollicitud', 'Sol·licitud'),
        'id_accio': fields.many2one('giscedata.descarrecs.operacions.tipus.accio', 'Acció'),
        'personal': fields.char('Personal', size=60),
        'hora': fields.datetime('Hora')
    }

    _defaults = {
    }

    _order = "sequence"


GiscedataDescarrecsOperacionsObertura()


class GiscedataDescarrecsOperacionsTancament(osv.osv):

    _name = 'giscedata.descarrecs.operacions.tancament'

    _columns = {
        'name': fields.char('Element', size=60),
        'ct': fields.char('CT', size=60),
        'descripcio': fields.text('Descripció'),
        'sequence': fields.integer('Ordre'),
        'sollicitud_id': fields.many2one('giscedata.descarrecs.sollicitud',
                                         'Sol·licitud'),
        'id_accio': fields.many2one('giscedata.descarrecs.operacions.tipus.accio', 'Acció'),
        'personal': fields.char('Personal', size=60),
        'hora': fields.datetime('Hora')
    }

    _defaults = {
    }

    _order = "sequence"


GiscedataDescarrecsOperacionsTancament()

_descarrec_states = [
    ('draft', 'Esborrany'),
    ('simulat', 'Simulat'),
    ('cancell', 'Cancel·lat'),
    ('confirm', 'Confirmat'),
    ('done', 'Finalitzat'),
]


class GiscedataDescarrecsDescarrec(osv.osv):

    _name = "giscedata.descarrecs.descarrec"

    def _timespan_diff(self, cr, uid, ids, name, arg, context={}):
        res = {}
        for id in ids:
            id = int(id)
            cr.execute("select extract('epoch' from data_final - data_inici) as time from giscedata_descarrecs_descarrec where id = %s", (id,))
            diff = cr.fetchone()
            if len(diff) and diff[0]:
                res[id] = int(diff[0])
            else:
                res[id] = 0
        return res

    def _avisos_pendents_escrit(self, cr, uid, ids, name, arg, context={}):
        res = {}
        for id in ids:
            id = int(id)
            if self.browse(cr, uid, id).state == 'confirm':
                cr.execute("select count(c.id) from crm_case c, crm_case_categ cc, crm_case_section cs where ref = %s and c.section_id = cs.id and cs.code = 'des' and c.categ_id = cc.id and cc.categ_code = 'des-ce' and c.state = 'open'", ('giscedata.descarrecs.descarrec,%s' % id,))
                res[id] = int(cr.fetchone()[0])
            else:
                res[id] = 0
        return res

    def _avisos_pendents_telf(self, cr, uid, ids, name, arg, context={}):
        res = {}
        for id in ids:
            id = int(id)
            if self.browse(cr, uid, id).state == 'confirm':
                cr.execute("select count(c.id) from crm_case c, crm_case_categ cc, crm_case_section cs where ref = %s and c.section_id = cs.id and cs.code = 'des' and c.categ_id = cc.id and cc.categ_code = 'des-tt' and c.state = 'open'", ('giscedata.descarrecs.descarrec,%s' % id,))
                res[id] = int(cr.fetchone()[0])
            else:
                res[id] = 0
        return res

    def _avisos_pendents_mail(self, cr, uid, ids, name, arg, context={}):
        res = {}
        for id in ids:
            id = int(id)
            if self.browse(cr, uid, id).state == 'confirm':
                cr.execute("select count(c.id) from crm_case c, crm_case_categ cc, crm_case_section cs where ref = %s and c.section_id = cs.id and cs.code = 'des' and c.categ_id = cc.id and cc.categ_code = 'des-cm' and c.state = 'open'", ('giscedata.descarrecs.descarrec,%s' % id,))
                res[id] = int(cr.fetchone()[0])
            else:
                res[id] = 0
        return res

    def export_descarrecs(self, cursor, uid, date, format="sqlite", context=None):
        """
        Exports descarrecs with state confirmat and with descarrec
        date >= date on a file

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param date:
        :param format: Export format, by default sqlite
        :param context: OpenERP context
        :type context: dict
        :return: Exported file in base64
        :rtype: str
        """

        search_params = [
            ('data_inici', '>=', date),
            ('state', '=', 'confirm')
        ]
        descarecs_ids = self.search(cursor, uid, search_params)
        file_url = tempfile.mktemp()
        if format == "sqlite":
            sqlite_con = sqlite3.connect(file_url)
            sqlite_cur = sqlite_con.cursor()
            sql_create = """
            CREATE TABLE descarrecs(
            inici datetime,
            fi datetime,
            municipi text,
            poblacio text,
            codi_postal text,
            carrers_afectats text
            );
            """
            sql_insert = """
            INSERT INTO descarrecs(
            inici,fi, municipi, poblacio, codi_postal,carrers_afectats
            )
            VALUES
            (?,?,?,?,?,?)
            """
            sqlite_cur.execute(sql_create)
            for descarrec_id in descarecs_ids:
                descarrec = self.browse(cursor, uid, descarrec_id)
                poblacions = []
                municipis = []
                carrers_afectats = []
                num_carrers = {}
                for client in descarrec.clients:
                    if client.codeine:
                        poblacions.append(client.poblacio.name)
                    if client.codeine:
                        municipis.append(client.codeine.name)
                    if client.street:
                        if client.number:
                            num = client.number.split(',')[0].strip()
                            if client.street in num_carrers:
                                if num not in num_carrers[client.street]:
                                    num_carrers[client.street].append(num)
                            else:
                                num_carrers[client.street] = []
                                num_carrers[client.street].append(num)

                for key in num_carrers:
                    temp = ""
                    temp += key + ": "
                    temp += ", ".join(sorted(num_carrers[key]))
                    carrers_afectats.append(temp)
                data = [
                    descarrec.data_inici,
                    descarrec.data_final,
                    ",".join(sorted(list(set(municipis)))),
                    ",".join(sorted(list(set(poblacions)))),
                    "",
                    ", ".join(sorted(list(set(carrers_afectats))))
                ]
                sqlite_cur.execute(sql_insert, data)
            sqlite_con.commit()
            sqlite_con.close()
            with open(file_url, 'r') as f:
                db_data = f.read()
            os.remove(file_url)
            return base64.encodestring(db_data)
        return ""

    def _cts_afectats(self, cr, uid, ids, name, arg, context=None):
        """
        Generates the afected cts list

        :param cr: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: Descarrecs ids
        :type ids: list of int
        :param name:
        :param arg:
        :param context: OpenERP Context
        :type context: dict
        :return: Cts afectats by the descarrec {descarrec_id:cts_afectats}
        :rtype: dict
        """
        res = {}
        for identifier in ids:
            identifier = int(identifier)
            sql = """
              SELECT i.code_ct, ct.descripcio
              FROM giscedata_descarrecs_descarrec_installacions i,
                giscedata_cts ct,
                giscedata_descarrecs_descarrec d
              WHERE i.code_ct = ct.name AND i.descarrec_id = %s
              GROUP BY i.code_ct, ct.descripcio
              ORDER BY  i.code_ct ASC
              """
            cr.execute(sql, (identifier,))
            cts = cr.dictfetchall()
            noms_cts = []
            for ct in cts:
                noms_cts.append('{} "{}"'.format(ct['code_ct'], ct['descripcio']))
            res[identifier] = ",".join(noms_cts)
        return res

    def _cts_afectats_obj(self, cr, uid, ids, name, arg, context={}):
        res = {}
        sql = """SELECT ct.id, i.code_ct, ct.descripcio
        FROM giscedata_descarrecs_descarrec_installacions i,
        giscedata_cts ct, giscedata_descarrecs_descarrec d
        WHERE i.code_ct = ct.name AND i.descarrec_id = %s
        GROUP BY ct.id, i.code_ct, ct.descripcio
        ORDER BY i.code_ct ASC"""
        for identifier in ids:
            cr.execute(sql, (identifier,))
            res[identifier] = cr.dictfetchall()
        return res

    def _poblacions_afectades(self, cr, uid, ids, name, arg, context={}):
        res = {}
        # agafem els cts que hi ha afectats
        cts = self._cts_afectats_obj(cr, uid, ids, name, arg, context)
        for i in cts.keys():
            string = ''
            for ct in cts[i]:
                sql = """
        SELECT poblacio.name AS poblacio
        FROM res_poblacio poblacio  LEFT JOIN  giscedata_cts AS ct
        ON ct.id_poblacio=poblacio.id  WHERE ct.id=%s
        """
                cr.execute(sql, (ct['id'],))
                p = cr.dictfetchone()
                if p:
                    string += "%s\n" % (p['poblacio'],)
            res[i] = string
        return res

    def observacions_descarrec(self, cr, uid, ids):
        """
        Function that retrieve all the "observacions_descarrec" aviable for each
        polissa affected by the descarrecs and fill the observacions field of
        each descarrec with her own observacions data.
        :param      cr:         Database cursor
        :type       cr:         cursor
        :param      uid:        User ID
        :type       uid:        int
        :param      ids:        Affected IDS
        :type       ids:        list of int
        :return:    True
        :rtype:     bool
        """

        polissa_obj = self.pool.get("giscedata.polissa")
        clients_obj = self.pool.get("giscedata.descarrecs.descarrec.clients")
        descarrec_obj = self.pool.get("giscedata.descarrecs.descarrec")
        for desc_id in ids:
            res = False
            desc_id = int(desc_id)
            clients_afect = descarrec_obj.read(cr, uid, desc_id, ['clients'])
            for client_afect_id in clients_afect['clients']:
                polissa_id = clients_obj.read(
                    cr, uid, client_afect_id, ['policy'])['policy'][0]
                polissa = polissa_obj.read(
                    cr, uid, polissa_id, ['name',
                                          'cups',
                                          'observacions_descarrec']
                )
                if polissa["observacions_descarrec"]:
                    if not res:
                        res = ""
                    if len(polissa["observacions_descarrec"]) > 0:
                        res += ("-{} - {} : {}\n\n").format(
                            polissa["cups"][1],
                            polissa["name"],
                            polissa["observacions_descarrec"]
                        )
            if res:
                descarrec_obj.write(cr, uid, desc_id, {'observacions': res})

        return True

    def on_change_data_ini(self, cr, uid, ids, data_ini, data_final, context=None):
        """
        Make data_final as today when current data_fin is null

        :param cr: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: Ids of the descarrecs
        :type ids: list[int]
        :param data_ini: Data inici of descarrec
        :param data_final: Data final of descarec
        :param context: OpenERP context
        :type context: dict
        :return: Values to update
        :rtype: dict
        """

        result = {}
        if not data_final:
            result = {'value': {
                'data_final': data_ini, }
            }
        return result

    _columns = {
        'name': fields.char('Codi', size=60),
        'descripcio': fields.char('Descripció', size=255),
        'sollicitud': fields.many2one('giscedata.descarrecs.sollicitud', 'Sol·licitud'),
        'xarxa': fields.selection(_xarxa_selection, 'Xarxa afectada'),
        'data_inici': fields.datetime('Inici', required=True),
        'data_final': fields.datetime('Final', required=True),
        'simulacio': fields.many2one(
            'giscegis.configsimulacio.at', 'Simulacio'
        ),
        'duration': fields.function(
            _timespan_diff, method=True,
            type='integer', string='Periode (Segons)'
        ),
        'installacions': fields.one2many(
            'giscedata.descarrecs.descarrec.installacions',
            'descarrec_id', 'Instal·lacions'
        ),
        'clients': fields.one2many(
            'giscedata.descarrecs.descarrec.clients',
            'descarrec_id',
            'Clients'
        ),
        'observacions': fields.text('Observacions', readonly=False),
        'state': fields.selection(_descarrec_states, 'Estat', readonly=True),
        'avisos_pendents_telf': fields.function(
            _avisos_pendents_telf, method=True,
            type='integer', string="Telefon"
        ),
        'avisos_pendents_escrit': fields.function(
            _avisos_pendents_escrit, method=True,
            type='integer', string="Escrit"
        ),
        'avisos_pendents_mail': fields.function(_avisos_pendents_mail, method=True, type='integer', string="Mail"),
        'nuclis_afectats': fields.text('Nuclis afectats'),
        'causes': fields.text('Causes'),
        'carrers_afectats': fields.one2many(
            'giscedata.descarrecs.descarrec.carrers.afectats',
            'descarrec_id',
            'Carrers Afectats'
        ),
        'cts_afectats': fields.function(_cts_afectats, type='text', string='CTS afectats', method=True),
        'poblacions_afectades': fields.function(
            _poblacions_afectades,
            type='text',
            string='Poblacions afectades',
            method=True),
        'p_paragraf': fields.text('Primer paràgraf'),
    }

    _order = "data_inici desc"

    # Creació de casos per avis al CRM i carrers afectats per al descarrec
    # al confirmar
    def get_carrers_afectats(self, cursor, uid, desc_id, context=None):
        """
        Get data of the afected streets by the descarrec
        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param desc_id: Descarrec id
        :type desc_id: list of int
        :param context: OpenERP context
        :type context: dict[str, Any]
        :return: Afected streets
        :rtype: dict[str, str]
        """
        if context is None:
            context = {}

        ret = {}
        descarrec_obj = self.pool.get("giscedata.descarrecs.descarrec")
        desc_client_obj = self.pool.get(
            "giscedata.descarrecs.descarrec.clients"
        )
        clients = descarrec_obj.read(
            cursor, uid, desc_id, ["clients"], context
        )["clients"]
        clients_data = desc_client_obj.read(
            cursor, uid, clients, ["street", "codeine", "poblacio", "number"]
        )

        for client in clients_data:
            carrer = client["street"].split(",")[0]
            num = client["number"]
            if client["codeine"] and client["poblacio"]:
                mun = " ".join(
                    map(str.strip, str(client["codeine"][1]).split(","))[::-1]
                )
                pob = " ".join(
                    map(str.strip, str(client["poblacio"][1]).split(","))[::-1]
                )
                if "{};{}".format(mun, pob) in ret:
                    if carrer in ret["{};{}".format(mun, pob)]:
                        num = str.strip(str(num))
                        if num and (num not in ret["{};{}".format(
                                mun, pob)][carrer]):
                            if num not in ret["{};{}".format(mun, pob)][carrer]:
                                ret["{};{}".format(mun, pob)][carrer].append(
                                    num)
                    else:
                        ret["{};{}".format(mun, pob)][carrer] = [num]
                else:
                    ret["{};{}".format(mun, pob)] = {carrer: [num]}
        return ret

    def generate_poblacions_afectades(self, cursor, uid, desc_id, context=None):
        """
        Generate the instance of the afected poblacions-carrers by
        the descarrec.
        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param desc_id: Descarrec id
        :type desc_id: list of int
        :param context: OpenERP context
        :type context: dict[str, Any]
        """
        carrers_afect_obj = self.pool.get(
            "giscedata.descarrecs.descarrec.carrers.afectats"
        )
        poblacio_obj = self.pool.get("res.poblacio")

        # Busquem si ja hi ha carrers afectats creats, i en el cas d'existir,
        # els eliminem.
        carrers_afect_ids = carrers_afect_obj.search(
            cursor, uid, [("descarrec_id", "=", desc_id)]
        )
        if len(carrers_afect_ids) > 0:
            carrers_afect_obj.unlink(cursor, uid, carrers_afect_ids)

        data_carrers = self.get_carrers_afectats(
            cursor, uid, desc_id, context
        )
        for mun_pob in data_carrers:
            mun_name, pob_name = mun_pob.split(';')
            id_pob = poblacio_obj.search(
                cursor, uid, [('name', '=', pob_name)]
            )[0]
            carr_afect = ""
            for carr_name in data_carrers[mun_pob]:
                carr_afect += carr_name + ": "
                temp = []
                for x in data_carrers[mun_pob][carr_name]:
                    try:
                        temp.append(int(x))
                    except:
                        temp.append(x)
                data_carrers[mun_pob][carr_name] = temp
                for nums in sorted(data_carrers[mun_pob][carr_name]):
                    carr_afect += str(nums) + ", "
                carr_afect = carr_afect[:-2]
                carr_afect += "\n"

            # Creem els carrers afectats
            carrers_afect_obj.create(
                cursor,
                uid,
                {'pob_name': mun_name + ": " + pob_name,
                 'mun_name': mun_name,
                 'descarrec_id': desc_id,
                 'poblacio_id': id_pob,
                 'carrers_afectats': carr_afect,
                 }
            )

    def confirm(self, cr, uid, ids, context=None):
        """
        Confirm the descarrecs after the simulation changing theirs states,
        fill the field poblacions_afectades, the field p_paragraf and creating
        the crm cases to advice the afected clients.
        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: Descarrecs ids
        :type ids: list of int
        :param context: OpenERP context
        :type context: dict[str, Any]
        """
        if context is None:
            context = {}

        crm_obj = self.pool.get('crm.case')
        to_open = []
        new_context = context.copy()
        new_context['model'] = 'giscedata.descarrecs.descarrec'
        for descarrec in self.browse(cr, uid, ids):
            # Generem els carrers afectats del descàrrec.
            new_context['active_id'] = descarrec.id
            self.generate_poblacions_afectades(cr, uid, descarrec.id, new_context)
            # Omplim el primer paragraf per al report dels carrers afectats.
            self.fill_primer_paragraf(cr, uid, [descarrec.id], new_context)
            # Generacio dels casos d'avis dels clients afectats.
            for client in descarrec.clients:
                if client.policy and client.policy.descarrec_address:
                    # Si el client te una adreca de descarrec assignada
                    vals = {
                        'name': descarrec.descripcio,
                        'partner_id': (
                            client.policy.descarrec_address.partner_id.id
                        ),
                        'partner_address_id': (
                            client.policy.descarrec_address.id),
                        'ref': "giscedata.descarrecs.descarrec,{}".format(
                            descarrec.id
                        ),
                        'email_from': (
                            client.policy.descarrec_address.email or ''
                        ),
                        'ref2': 'giscedata.polissa,{}'.format(client.policy.id)
                    }
                    if client.policy.avis_telefon and (
                            client.policy.descarrec_address.phone or (
                            client.policy.descarrec_address.mobile)
                    ):
                        crm_ids = crm_obj.create_case_generic(
                            cr, uid, [descarrec.id], context=new_context,
                            description=descarrec.descripcio, section='des',
                            category='des-tt', extra_vals=vals
                        )
                        to_open.extend(crm_ids)
                    if client.policy.avis_escrit:
                        # Creem el cas per avis escrit
                        crm_ids = crm_obj.create_case_generic(
                            cr, uid, [descarrec.id], context=new_context,
                            description=descarrec.descripcio, section='des',
                            category='des-ce', extra_vals=vals
                        )
                        to_open.extend(crm_ids)
                    if client.policy.avis_email and (
                            client.policy.descarrec_address.email
                    ):
                        # Busquem el text que toca
                        avis_obj = self.pool.get(
                            'giscedata.descarrecs.avis.mail'
                        )
                        avis_id = avis_obj.search(
                            cr, uid, [('current', '=', 1)]
                        )
                        if avis_id and len(avis_id):
                            dd_inici = descarrec.data_inici
                            dd_final = descarrec.data_final
                            avis = avis_obj.browse(cr, uid, avis_id[0])
                            try:
                                number = int(client.number)
                            except Exception:
                                number = False
                            try:
                                number_text = client.number
                            except Exception:
                                number_text = False
                            if isinstance(dd_inici, str):
                                data_inici = dd_inici
                            else:
                                data_inici = dd_inici.strftime(
                                    '%d/%m/%Y %H:%M:%S')

                            if isinstance(dd_inici, str):
                                data_final = dd_final
                            else:
                                data_final = dd_final.strftime(
                                    '%d/%m/%Y %H:%M:%S')

                            if isinstance(dd_inici, str):
                                only_data_inici = dd_inici.split(" ")[0]
                            else:
                                only_data_inici = dd_inici.strftime('%d/%m/%Y')

                            if isinstance(descarrec.data_inici, str):
                                only_data_final = dd_final.split(" ")[0]
                            else:
                                only_data_final = dd_final.strftime(
                                    '%d/%m/%Y')

                            des_adrs_name = client.policy.descarrec_address.name
                            client_poblacio = client.poblacio.name

                            di = datetime.strptime(
                                data_inici, '%Y-%m-%d %H:%M:%S'
                            )
                            data_inici_text = babel.dates.format_datetime(
                                di, 'd LLLL Y',
                                locale=new_context.get("lang", "es_ES")
                            )

                            df = datetime.strptime(
                                data_final, '%Y-%m-%d %H:%M:%S'
                            )
                            data_final_text = babel.dates.format_datetime(
                                df, 'd LLLL Y',
                                locale=new_context.get("lang", "es_ES")
                            )

                            hora_ini = babel.dates.format_datetime(di, "HH:mm")
                            hora_fi = babel.dates.format_datetime(df, "HH:mm")

                            vars = {
                                'descarrec_address_name': des_adrs_name,
                                "data_inici": only_data_inici,
                                "data_final": only_data_final,
                                'descarrec_inici': data_inici,
                                'descarrec_final': data_final,
                                'descarrec_inici_text': data_inici_text,
                                'descarrec_final_text': data_final_text,
                                'carrer': client.street,
                                'num': number or 'S/N',
                                'num_text': number_text or 'S/N',
                                "hora_inic": hora_ini,
                                "hora_inici": hora_ini,
                                "hora_fi": hora_fi,
                                "poblacio": client_poblacio,
                                "client": client.name
                            }
                            vals['description'] = avis.text % vars
                            mail_subject = avis.name
                            vals['name'] = mail_subject
                            crm_ids = crm_obj.create_case_generic(
                                cr, uid, [descarrec.id], context=new_context,
                                description=mail_subject, section='des',
                                category='des-cm', extra_vals=vals
                            )
                            to_open.extend(crm_ids)
                    # if client.policy.avis_sms:
                        # TODO
                        # Es podria utilitzar una passarelÂ·la ?
            if to_open:
                crm_obj.case_open(cr, uid, to_open)
            descarrec.write({'state': 'confirm'})
        return True

    def done_check(self, cr, uid, ids, *args):
        for d in self.browse(cr, uid, ids):
            if d.avisos_pendents_telf + d.avisos_pendents_escrit:
                return False
            else:
                return True

    def fill_primer_paragraf(self, cr, uid, ids, context=None):
        """
        Function that calculate and fill the field p_paragraf.
        :param cr: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: Descarrecs ids
        :type ids: list of int
        :param context: OpenERP context
        :type context: dict[str, Any]
        """
        if context is None:
            context = {}

        descarrec_obj = self.pool.get("giscedata.descarrecs.descarrec")
        company_obj = self.pool.get("res.company")
        company_name = company_obj.read(cr, uid, 1, ["name"])["name"]

        for id in ids:
            descarrec = descarrec_obj.read(
                cr,
                uid,
                id,
                ["data_inici", "data_final"]
            )

            if descarrec["data_inici"]:
                data_inici = descarrec["data_inici"]
                dtf = datetime.strptime(data_inici.split(' ')[0], '%Y-%m-%d')
                dtf = dtf.strftime("%y%m%d")
                dt = datetime.strptime(dtf, '%y%m%d')
                data_inici = babel.dates.format_datetime(
                    dt, 'd LLLL Y', locale=context.get("lang", "es_ES")
                )

                hora_inici = descarrec["data_inici"].split(" ")[1]
                hora_inici = datetime.strptime(
                    hora_inici, "%H:%M:%S"
                ).strftime('%H:%M')
            else:
                data_inici = "(data inici N\A)"
                hora_inici = "(hora inici N\A)"

            if descarrec["data_final"]:
                hora_fi = descarrec["data_final"].split(" ")[1]
                hora_fi = datetime.strptime(
                    hora_fi, "%H:%M:%S"
                ).strftime('%H:%M')
            else:
                hora_fi = "(hora fi N\A)"

            tmpl_p1 = _(" comunica als seus clients, que el proper ")
            tmpl_p2 = _(", a  partir de les ")
            tmpl_p3 = _(" hores fins a les ")
            tmpl_p4 = _(" hores, no hi  haurà subministrament elèctric a les "
                        "zones següents:")

            text_paragraf = (
                company_name + tmpl_p1 + data_inici + tmpl_p2 + hora_inici
                + tmpl_p3 + hora_fi + tmpl_p4
            )

            descarrec_obj.write(cr, uid, id, {'p_paragraf': text_paragraf})

    _defaults = {
        'state': lambda *a: 'draft',
    }

    def create_incidencia(self, cr, uid, descarrec_id):

        """
               Method to create an incidencia from a descarrec

               :param cr: Database cursor
               :param uid: User id
               :param descarrec_id: Id of the descarrec from which will get the
                                    data.
               :return incidencia_id: Id of the new incidencia created.
        """

        # retrieve the data from descarrec by the descarred_id
        descarrec_data = self.read(cr, uid, descarrec_id)

        # getting the needed data models
        origen_obj = self.pool.get('giscedata.qualitat.origin')
        causa_obj = self.pool.get('giscedata.qualitat.cause')
        tipus_obj = self.pool.get('giscedata.qualitat.type')
        incidencia_obj = self.pool.get('giscedata.qualitat.incidence')
        interval_obj = self.pool.get('giscedata.qualitat.span')
        cts_obj = self.pool.get('giscedata.cts')
        policy_obj = self.pool.get('giscedata.polissa')
        clients_affected_obj = self.pool.get(
            'giscedata.descarrecs.descarrec.clients')
        clients_affected_incidencia_obj = self.pool.get(
            'giscedata.qualitat.affected_customer')
        installacions_affected_obj = self.pool.get(
            'giscedata.descarrecs.descarrec.installacions')
        installacions_affected_incidencia_obj = self.pool.get(
            'giscedata.qualitat.affected_installation')

        # filling an incidencia dictionari with the descarrec data.
        incidencia_data = {
            'name': descarrec_data['descripcio'],
            'code': descarrec_data['name'],
            'origin_id': origen_obj.search(cr, uid, [('code', '=', 2)])[0],
            'cause_id': causa_obj.search(cr, uid, [('code', '=', 2)])[0],
            'type_id': tipus_obj.search(cr, uid, [('code', '=', 1)])[0],
            'affected_means': int(descarrec_data['xarxa']),
            'data': descarrec_data['data_inici'],
            'descarrec_id': descarrec_id
        }

        # creating an incidencia elemente into the DB
        incidencia_id = incidencia_obj.create(cr, uid, incidencia_data)

        # filling an interval dictionari with the descarrec data and linking it
        # with the incidence_id
        interval_data = {
            'name': "1",
            'incidence_id': incidencia_id,
            'begin_date': descarrec_data['data_inici'],
            'end_date': descarrec_data['data_final']
        }

        # creating an interval elemente into the DB
        interval_id = interval_obj.create(cr, uid, interval_data)

        # retrieving all the client ids affected by the descarrec
        clients_affected_ids = clients_affected_obj.search(
            cr, uid, [('descarrec_id', '=', descarrec_id)])

        # retrieving all the clients matching with the ids in the
        # clients_affected_ids
        clients_affected = clients_affected_obj.read(cr,
                                                     uid,
                                                     clients_affected_ids)

        # copying the descarrec affected client data for each client
        # in the clients_affected list to create an incidencia affected client
        for client in clients_affected:

            client['ct_id'] = cts_obj.search(cr, uid,
                                             [('name', '=', client['code_ct'])])
            client.update({
                'zone_ct_id': (client['zone_ct_id'][0]
                               if client['zone_ct_id'] else False),
                'codeine': (client['codeine'][0]
                            if client['codeine'] else False),
                'policy': (policy_obj.read(cr, uid, client['policy'][0])['name']
                           if client['policy'] else False),
                'ct_id': (client['ct_id'][0]
                          if client['ct_id'] else False)
            })

            client_incidencies_data = {
                'name': client['name'],
                'code_ct': client['code_ct'],
                'zone_ct_id': client['zone_ct_id'],
                'ct_id': client['ct_id'],
                'codeine': client['codeine'],
                'codecustomer': client['codecustomer'],
                'line': client['line'],
                'cups': client['cups'],
                'policy': client['policy'],
                'contracted_power': client['contracted_power'],
                'tension': client['tension'],
                'pricelist': client['pricelist'],
                'escomesa': client['escomesa'],
                'escomesa_type': client['escomesa_type'],
                'street': client['street'],
                'number': client['number'],
                'level': client['level'],
                'appartment_number': client['appartment_number'],
                'contador': client['contador'],
                'data_alta': client['data_alta'],
                # linking the affected client to the interval element
                # by the interval_id
                'span_id': interval_id
            }

            # creating an affected client element into the DB
            clients_affected_incidencia_obj.create(cr, uid,
                                                   client_incidencies_data)

        # retrieving all the installacions ids affected by the descarrec
        installacions_affected_ids = installacions_affected_obj.search(
            cr, uid, [('descarrec_id', '=', descarrec_id)])

        # retrieving all the clients matching with the ids in the
        # clients_affected_ids
        installacions_affected = installacions_affected_obj.read(
            cr, uid, installacions_affected_ids)

        # copying the descarrec affected installacio data for each installacio
        # in the installacions_affected list to create an incidencia affected
        # installacio.
        for installacio in installacions_affected:
            installacio.update({
                'zone_ct_id': (installacio['zone_ct_id'][0]
                               if installacio['zone_ct_id'] else False),
                'codeine': (installacio['codeine'][0]
                            if installacio['codeine'] else False),
            })
            installacio_incidencies_data = {
                'name': installacio['name'],
                'type': installacio['type'],
                'code_ct': installacio['code_ct'],
                'zone_ct_id': installacio['zone_ct_id'],
                'code_inst': installacio['code_inst'],
                'power': installacio['power'],
                'codeine': installacio['codeine'],
                'data': installacio['data'],
                # linking the affected installacio to the interval element
                # by the interval_id
                'span_id': interval_id
            }

            # creating an affected installacio element into the DB
            installacions_affected_incidencia_obj.create(
                cr, uid, installacio_incidencies_data)

        return incidencia_id


GiscedataDescarrecsDescarrec()


class GiscedataDescarrecsDescarrecInstallacions(osv.osv):

    _name = 'giscedata.descarrecs.descarrec.installacions'

    _columns = {
      'name': fields.char('Nom', size=16),
      'descarrec_id': fields.many2one(
          'giscedata.descarrecs.descarrec', 'Descàrrec'
      ),
      'type': fields.selection(
          [('T', 'Trafo'), ('C', 'Conta-AT')], 'Tipus', required=True
      ),
      'code_ct': fields.char('Codi CT', size=50),
      'zone_ct_id': fields.many2one('giscedata.cts.zona', 'Zona'),
      'code_inst': fields.char('Codi Instal·lació', size=16),
      'power': fields.integer('Potència'),
      'codeine': fields.many2one('res.municipi', 'Municipi'),
      'data': fields.datetime('Data'),
    }
    _defaults = {}


GiscedataDescarrecsDescarrecInstallacions()


class GiscedataDescarrecsDescarrecClients(osv.osv):

    _name = 'giscedata.descarrecs.descarrec.clients'

    def _avis(self, cr, uid, ids, name, arg, context={}):
        res = {}
        for client in self.browse(cr, uid, ids, context):
            if client.policy and client.policy.descarrec_address:
                if client.policy.avis_email:
                    res[client.id] = 1

        return res

    _columns = {
        'descarrec_id': fields.many2one(
            'giscedata.descarrecs.descarrec', 'Descàrrec'
        ),
        'name': fields.char('Nom', size=255, required=True),
        'code_ct': fields.char('Codi CT', size=50),
        'zone_ct_id': fields.many2one('giscedata.cts.zona', 'Zona'),
        'codeine': fields.many2one('res.municipi', 'Municipi'),
        'poblacio': fields.many2one('res.poblacio', 'Poblacio'),
        'codecustomer': fields.char('Codi client', size=50),
        'line': fields.char('Lí­nia', size=50),
        'cups': fields.char('CUPS', size=50),
        'policy': fields.many2one('giscedata.polissa', 'Pòlissa'),
        'contracted_power': fields.float('Potència Contractada (kW)'),
        'tension': fields.char('Tensió', size=15),
        'pricelist': fields.char('Tarifa', size=10),
        'escomesa': fields.char('Escomesa', size=50),
        'escomesa_type': fields.char('Tipo Escomesa', size=50),
        'street': fields.char('Carrer', size=255, required=True),
        'number': fields.char('Número', size=10),
        'level': fields.char('Planta', size=10),
        'appartment_number': fields.char('Pis', size=10),
        'contador': fields.char('Contador', size=50),
        'data_alta': fields.datetime('Data alta'),
        'avis': fields.function(
            _avis, type='boolean', method=True, string="Avis"
        ),

    }


GiscedataDescarrecsDescarrecClients()


class GiscedataDescarrecsAvisMail(osv.osv):

    _name = 'giscedata.descarrecs.avis.mail'

    _columns = {
      'name': fields.char('Descripció', size=64, required=True),
      'text': fields.text('Text del correu', required=True),
      'current': fields.boolean('Actual', required=True),
    }

    _defaults = {
      'current': lambda *a: 0,
    }

    _sql_constraints = [(
        'curren_uniq', 'unique (current)',
        'Només es pot tenir marcat com a current un avís'
    )]


GiscedataDescarrecsAvisMail()


class GiscedataDescarrecsDescarrecCarrersAfectats(osv.osv):

    _name = 'giscedata.descarrecs.descarrec.carrers.afectats'

    _rec_name = 'pob_name'

    def onchange_carrer_pob(
            self, cr, uid, ids, pob_id, carrers_afectats, descarrec_id
    ):

        res = {'value': {}}
        if pob_id:
            poblacio_obj = self.pool.get('res.poblacio')
            municipi_obj = self.pool.get('res.municipi')

            poblacio = poblacio_obj.read(
                cr, uid, pob_id, ['name', 'municipi_id']
            )
            municipi = municipi_obj.read(
                cr, uid, poblacio['municipi_id'][0], ['name']
            )

            res['value']['pob_name'] = (
                municipi['name'] + ": " + poblacio['name'])
            res['value']['mun_name'] = municipi['name']

        if ids:
            if carrers_afectats and ids:
                self.write(
                    cr, uid, ids,
                    {'carrers_afectats': carrers_afectats}
                )
        else:
            if carrers_afectats and pob_id:
                self.create(cr, uid, {
                    "descarrec_id": descarrec_id,
                    "carrers_afectats": carrers_afectats,
                    "poblacio_id": pob_id,
                    "pob_name": res["value"]["pob_name"],
                    "mun_name": res["value"]["pob_name"]
                })

        return res

    def _descarrec_id(self, cr, uid, context=None):
        """
        Search and return the id of the related descarrec of the model
        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param context: OpenERP context
        :type context: dict[str, Any]
        :return: The id of the related descarrec of the model.
        :rtype: int
        """
        desc_name = context.get("descarrec_name")
        desc_id = self.pool.get("giscedata.descarrecs.descarrec").search(
            cr,
            uid,
            [("name", "=", desc_name)]
        )[0]
        return desc_id

    _columns = {
        'pob_name': fields.char('Poblacions afectades', size=264),
        'mun_name': fields.char('Municipi afectat', size=264, readonly=True),
        'descarrec_id': fields.many2one('giscedata.descarrecs.descarrec',
                                        'Descàrrec'),
        'poblacio_id': fields.many2one('res.poblacio',
                                       'Població afectada',
                                       required=True),
        'carrers_afectats': fields.text('Carrers afectats', required=True),
    }

    _defaults = {
        'descarrec_id': _descarrec_id
    }


GiscedataDescarrecsDescarrecCarrersAfectats()
