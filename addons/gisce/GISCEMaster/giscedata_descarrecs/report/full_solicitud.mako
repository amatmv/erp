<%
  from datetime import datetime

  def get_date_hour(date):
      data = datetime.strptime(date, '%Y-%m-%d %H:%M:%S')
      dia = data.strftime('%d/%m/%Y')
      hora = data.strftime('%H:%M')
      return dia, hora
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<head>
  <style type="text/css">
    ${css}
  </style>
  <link rel="stylesheet" href="${addons_path}/giscedata_descarrecs/report/solicituds.css"/>
</head>
<body>
    <div class="report_header">
        <span class="header_logo">
            <img id="company_logo" src="data:image/jpeg;base64,${company.logo}"/>
        </span>
        <span class="header_text"> ${company.rml_header1 or ''}</span>
    </div>
    %for descarrec in objects:
        <div class="dades_full_sol_des">
                <%block name="custom_data1">
                </%block>
                <div class="full_sol_desc_title1">
                    <strong>${_("PETICIÓ DE DESCÀRREC ELÈCTRIC EN BT I MT")}</strong>
                </div>
                <span class="full_desc_inbox_left_bot">
                    ${_("Descàrrec elèctric amb referència:")} ${descarrec.name}
                </span>
                <%block name="custom_data_box2">
                </%block>
        </div>
        <br/>
        <div class="full_desc_title">${_("Full de Sol·licitud de Descàrrec")}</div>
      <table>
        <tr>
          <th class="w25">${_("SOL·LICITANT")}</th>
          <th class="w25">${_("EMPRESA")}</th>
          <th class="w25">${_("CÀRREC")}</th>
          <th class="w25">${_("TELÈFON")}</th>
        </tr>
        <tr>
          <td class="nocount">${descarrec.solicitant.name or ''}</td>
          <td class="nocount" style="font-size: 10px;">${descarrec.solicitant.partner_id.name or ''}</td>
          <td class="nocount">${descarrec.solicitant.function.name or ''}</td>
          <td class="nocount">${descarrec.solicitant.phone or ''}</td>
        </tr>
      </table>
      <div class="full_desc_instalacio_afectada_container">
          <div class="full_desc_instalacio_afectada_title">${_("Instal·lació a deixar sense tensió: ")}</div>
          <div class="full_desc_instalacio_afectada_data">${descarrec.instalacion_afectada or ''}</div>
      </div>
      <div class="full_desc_treballs_previstos_container">
          <div class="full_desc_treballs_previstos_title">${_("Treballs previstos:")}</div>
          <div class="full_desc_treballs_previstos_data">${descarrec.treball or ''}</div>
      </div>
      <div class="full_desc_mod_esquema_container">
          <span><strong>
              ${_("Els treballs previstos modifiquen l'esquema general: ")}
          </strong></span>
          <span>
              %if descarrec.modificacio_esquema:
                ${_("sí")}
              %else:
                ${_("no")}
              %endif
          </span>
      </div>

      <table>
        <tr>
          <th rowspan="2">${_("HORARIS")}</th>
          <th colspan="2">${_("SOL·LICITAT")}</th>
          <th colspan="2">${_("EFECTUAT")}</th>
        </tr>
        <tr>
          <th>${_("DIA/MES/ANY")}</th>
          <th>${_("HORA:MINUT")}</th>
          <th>${_("DIA/MES/ANY")}</th>
          <th>${_("HORA:MINUT")}</th>
        </tr>
        <%
          data0, hora0 = get_date_hour(descarrec.data_inici)
          data1, hora1 = get_date_hour(descarrec.data_final)
        %>
        <tr>
          <td class="nocount p5">${_("TALL SUBMINISTRAMENT")}</td>
          <td class="nocount p5">${data0}</td>
          <td class="nocount p5">${hora0}</td>
          <td class="nocount p5"></td>
          <td class="nocount p5"></td>
        </tr>
        <tr>
          <td class="nocount p5">${_("REPRESA SUBMINISTRAMENT")}</td>
          <td class="nocount p5">${data1}</td>
          <td class="nocount p5">${hora1}</td>
          <td class="nocount p5"></td>
          <td class="nocount p5"></td>
        </tr>
      </table>
      <br/>
      <table>
        <tr>
          <th class='w30'>${_("SOL·LICITANT")}</th>
          <th class='w30'>${_("CAP DE LA INSTAL·LACIÓ")}</th>
          <th class='w30'>${_("CENTRE DE CONTROL")}</th>
        </tr>
        <tr>
          <td class="nocount" style="text-align: left; padding-left: 5px; padding-top: 5px;">
            ${_("Firma")}
            <br/><br/><br/><br/>
          </td>
          <td class="nocount" style="text-align: left; padding-left: 5px; padding-top: 5px;">
            ${_("Firma")}
            <br/><br/><br/><br/>
          </td>
          <td class="nocount" style="text-align: left; padding-left: 5px; padding-top: 5px;">
            ${_("Firma")}
            <br/><br/><br/><br/>
          </td>
        </tr>
        <tr>
          <td class="nocount left_text">${_("Nom: {}").format(descarrec.solicitant.name or "")}</td>
          <td class="nocount left_text">${_("Nom: {}").format(descarrec.cap_instalacio.name or "")}</td>
          <td class="nocount left_text">${_("Nom: {}").format(descarrec.centre_control.name or "")}</td>
        </tr>
        <tr>
          <td class="nocount left_text">${_("Data: ")}</td>
          <td class="nocount left_text">${_("Data: ")}</td>
          <td class="nocount left_text">${_("Data: ")}</td>
        </tr>
      </table>
      <br/>
      <table>
        <tr>
          <th>${_("Observacions")}</th>
        </tr>
        <tr>
          <td class="nocount left_text"><div class="sol_desc_observacions">${descarrec.observacions or ''}</div></td>
        </tr>
      </table>
    %endfor
    <%block name="custom_data3">
    </%block>
</body>
</html>
