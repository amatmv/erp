<%
def localize_date(period):
    if period != 'False':
        from datetime import datetime
        dtf = datetime.strptime(period, '%Y-%m-%d')
        return dtf.strftime("%d/%m/%Y")
    else:
        return ''
%>
<html>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
        <style type="text/css">
            ${css}
        </style>
        <link rel="stylesheet" href="${addons_path}/giscedata_descarrecs/report/industria.css"/>
    </head>
    <body>
    <div class="report_header">
        <span class="header_logo">
            <img id="company_logo" src="data:image/jpeg;base64,${company.logo}"/>
        </span>
        <br/>
        <span class="header_text">${company.rml_header1 or ''}</span>
    </div>
    <br/>
        <div id="avis">
             <%block name="custom_missatge" >
                 %if not data["form"]:
                     ${_("Senyors:")}<br/><br/>
                     ${_("D’acord amb el que disposa l’Article 101.3 del RD 1955/2000, us comuniquem les interrupcions programades següents:")}
                 %else:
                     %if data["form"]["tramitacio"] == "U":
                         ${_("Senyors:")}<br/><br/>
                         ${_("D’acord amb el que disposa l’Article 101.3 del RD 1955/2000, us comuniquem les interrupcions programades molt urgents, següents:")})
                     %else:
                         ${_("Senyors:")}<br/><br/>
                         ${_("D’acord amb el que disposa l’Article 101.3 del RD 1955/2000, us comuniquem les interrupcions programades següents:")}
                     %endif
                 %endif
            </%block>
        </div>
        <table>
            <tr>
                <td class="min">${_("Àrea de distribució")}</td>
                <td>
                    <%block name="custom_area">
                        ${",".join(data["form"]["municipis_afectats"])}
                    </%block>
                </td>
            </tr>
            <tr>
                <td class="min">${_("Descàrrec amb referència")}</td>
                <td>${data["form"]["num_descarrec"]}</td>
            </tr>
            <tr>
                <td class="min">${_("Data i hora d'inici")}</td>
                <td>
                    %if data["form"]["inici"]:
                        ${localize_date(data["form"]["inici"][0])}
                        %if data["form"]["inici"][1].startswith("01:"):
                            ${_("a la")} ${data["form"]["inici"][1]}
                        %else:
                            ${_("a les")} ${data["form"]["inici"][1]}
                        %endif
                    %endif
                </td>
            </tr>
            <tr>
                <td class="min">${_("Data i hora de fi")}</td>
                <td>
                    %if data["form"]["fi"]:
                        ${localize_date(data["form"]["fi"][0])}
                        %if data["form"]["fi"][1].startswith("01:"):
                            ${_("a la")} ${data["form"]["fi"][1]}
                        %else:
                            ${_("a les")} ${data["form"]["fi"][1]}
                        %endif
                    %endif
                </td>
            </tr>
            <tr>
                <td class="min">${_("Població")}</td>
                <td>${data["form"]["poblacions"] or ''}</td>
            </tr>
            <tr>
                <td class="min">${_("Adreça")}</td>
                <td>${data["form"]["adreces"] or ''}</td>
            </tr>
            <tr>
                <td class="min">${_("Centre de transformació")}</td>
                %if data["form"]["xarxa"] == '1':
                    <td>${data["form"]["cts_afectats"] or ''}</td>
                % else:
                    <td>
                        %for ct,linies in data["form"]["linies_bt"].iteritems():
                            <ul class="lines_list">
                                %for linia in linies:
                                    <li class="lines_list_item">
                                        ${_("Línia de Baixa Tensió núm. "+linia+" del "+ct)}
                                    </li>
                                %endfor
                            </ul>
                        %endfor
                    </td>
                %endif
            </tr>
            <tr>
                <td class="min">${_("Núm. de clients afectats")}</td>
                <td>${data["form"]["clients"]}</td>
            </tr>
            <tr>
                <td class="min">${_("Descripció dels treballs")}</td>
                <td>
                    <%block name="custom_descripcio">
                    ${data["form"]["causes"]}
                    </%block>
                </td>
            </tr>
        </table>
        <br/>
        <p>${_("Rebeu una salutació molt atenta.")}</p>
    </body>
</html>