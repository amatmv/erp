 # -*- coding: utf-8 -*-

from c2c_webkit_report import webkit_report
from report import report_sxw
from report import common
from tools import config
from tools.translate import _


class ReportWebkitHtml(report_sxw.rml_parse):

    def __init__(self, cursor, uid, name, context=None):
        """
        Class constructor

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param name: module name
        :type name: str
        :param context: OpenERP context
        :type context: dict
        """
        super(ReportWebkitHtml, self).__init__(
            cursor, uid, name, context=context)
        if len(name.split('.')) >= 2:
            action = name.split('.')[-2]
        else:
            action = ''
        if len(name.split('.')) > 0:
            target = name.split('.')[-1]
        else:
            target = ''
        if action == "solicitud":
            document = _("Document FSD-01 del procediment PGO-01")
        elif action == "treure":
            document = _("Document FSD-03 del procediment PGO-01")
        elif action == "donar":
            document = _("Document FSD-04 del procediment PGO-01")
        else:
            document = ""
        if target == "agent":
            exemplar = _("EXEMPLAR PER L'AGENT DE DESCÀRREC ")
        elif target == "cdc":
            exemplar = _("EXEMPLAR PER AL CENTRE DE CONTROL")
        else:
            exemplar = ""
        peu_pagina = _("Les cinc regles d'or són d'obligat compliment")
        self.localcontext.update({
            'cursor': cursor,
            'uid': uid,
            'addons_path': config['addons_path'],
            'action': action,
            'document': document,
            'exemplar': exemplar,
            'peu_pagina': peu_pagina,
        })


class ReportWebkitHtmlFulls(report_sxw.rml_parse):

    def __init__(self, cursor, uid, name, context=None):
        """
        Class constructor

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param name: module name
        :type name: str
        :param context: OpenERP context
        :type context: dict
        """
        super(ReportWebkitHtmlFulls, self).__init__(
            cursor, uid, name, context=context)

        obj_des = self.pool.get("giscedata.descarrecs.descarrec")
        bro = obj_des.browse(cursor, uid, context.get("active_ids"))

        self.localcontext.update({
            'cursor': cursor,
            'uid': uid,
            'addons_path': config['addons_path'],
            'objects': bro,
        })

    def set_context(self, objects, data, ids, report_type=None):
        """
        Context setter

        :param objects: Objects of the report.
        :type objects: Any
        :param data: Data to pass to the report.
        :type data: dict[str, Any]
        :param ids: Ids of the elements that asked for the report.
        :type ids: list of int
        :param report_type: Type of report.
        :type report_type: unicode
        """
        self.localcontext['data'] = data
        self.datas = data
        self.ids = ids
        self.objects = objects
        if report_type:
            if report_type == 'odt':
                self.localcontext.update({'name_space': common.odt_namespace})
            else:
                self.localcontext.update({'name_space': common.sxw_namespace})


webkit_report.WebKitParser(
    'report.giscedata.descarrecs.solicitud.agent',
    'giscedata.descarrecs.sollicitud',
    'giscedata_descarrecs/report/full_solicitud.mako',
    parser=ReportWebkitHtml
)

webkit_report.WebKitParser(
    'report.giscedata.descarrecs.solicitud.cdc',
    'giscedata.descarrecs.sollicitud',
    'giscedata_descarrecs/report/full_solicitud.mako',
    parser=ReportWebkitHtml
)

webkit_report.WebKitParser(
    'report.giscedata.descarrecs.full.maniobra.treure.agent',
    'giscedata.descarrecs.sollicitud',
    'giscedata_descarrecs/report/full_maniobra.mako',
    parser=ReportWebkitHtml
)

webkit_report.WebKitParser(
    'report.giscedata.descarrecs.full.maniobra.treure.cdc',
    'giscedata.descarrecs.sollicitud',
    'giscedata_descarrecs/report/full_maniobra.mako',
    parser=ReportWebkitHtml
)

webkit_report.WebKitParser(
    'report.giscedata.descarrecs.full.maniobra.donar.agent',
    'giscedata.descarrecs.sollicitud',
    'giscedata_descarrecs/report/full_maniobra.mako',
    parser=ReportWebkitHtml
)

webkit_report.WebKitParser(
    'report.giscedata.descarrecs.full.maniobra.donar.cdc',
    'giscedata.descarrecs.sollicitud',
    'giscedata_descarrecs/report/full_maniobra.mako',
    parser=ReportWebkitHtml
)

webkit_report.WebKitParser(
    'report.giscedata.descarrecs.comunicat_industria',
    'giscedata.descarrecs.sollicitud',
    'giscedata_descarrecs/report/comunicat_industria.mako',
    parser=ReportWebkitHtml
)

webkit_report.WebKitParser(
    'report.giscedata.descarrecs.fulls.penjar',
    'giscedata.descarrecs.descarrec',
    'giscedata_descarrecs/report/comunicat_carrer.mako',
    parser=ReportWebkitHtmlFulls
)
