<%
  from datetime import datetime

  def get_date_hour(date):
      data = datetime.strptime(date, '%Y-%m-%d %H:%M:%S')
      dia = data.strftime('%d/%m/%Y')
      hora = data.strftime('%H:%M')
      return dia, hora
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<head>
    <style type="text/css">
        ${css}
    </style>
    <link rel="stylesheet" href="${addons_path}/giscedata_descarrecs/report/solicituds.css"/>
    <%block name="custom_css" />
</head>
<body>
    <div class="report_header">
        <span class="header_logo">
            <img id="company_logo" src="data:image/jpeg;base64,${company.logo}"/>
        </span>
        <span class="header_text">${company.rml_header1 or ''}</span>
    </div>
    %for descarrec in objects:
        <%
        data0, hora0 = get_date_hour(descarrec.data_inici)
        data1, hora1 = get_date_hour(descarrec.data_final)
        %>
            <div class="dades_sol_des">
                <div class="tipus_maniobra">
                    %if action == "treure":
                        ${_("treure")}
                    %elif action == "donar":
                        ${_("donar")}
                    %endif
                </div>
                <div class="tipus_maniobra_box">
                    <%block name="custom_data1">
                    </%block>
                    <div class="inbox_msg_container">
                        ${_("full de maniobres per ")}
                        <em class="inbox_msg_u">
                             %if action == "treure":
                                <strong>${_("treure")}</strong>
                             %elif action == "donar":
                                <strong>${_("donar")}</strong>
                             %endif
                        </em>
                        ${_("&nbsp;la tensió")}
                    </div>
                    <span class="inbox_left">
                        ${_("Maniobres del descàrrec elèctric  amb referència:")} ${descarrec.name or ''}
                    </span>
                    <%block name="custom_data_box2">
                    </%block>
                </div>
            </div>
        <table class="accions_descarrec">
            <thead class="accions_descarrec_head">
                <tr>
                    <th>${_("fet")}</th>
                    <th>${_("núm. i nom del centre")}</th>
                    <th>${_("element")}</th>
                    <th>${_("maniobra")}</th>
                    <th>${_("personal")}</th>
                    <th>${_("hora")}</th>
                </tr>
            </thead>
            <%
            if action == 'treure':
                accions = sorted(descarrec.operacions_obertura, key=lambda elem : elem.sequence)
            elif action == 'donar':
                accions = sorted(descarrec.operacions_tancament, key=lambda elem : elem.sequence)
            %>
            <tbody class="accions_descarrec_body">
            %for accio in accions:
                <tr>
                    <td></td>
                    <td>${accio.ct or ''}</td>
                    <td>${accio.name or ''}</td>
                    <td>${accio.id_accio.name or ''}</td>
                    <td>${accio.personal or ''}</td>
                    <td>${accio.hora or ''}</td>
                </tr>
            %endfor
            </tbody>
        </table>
        <br><br>
        <%block name="custom_data_box3">
            %for descarrec in objects:
                %if (descarrec.observacions_obertura and action == "treure") or (descarrec.observacions_tancament and action == "donar"):
                    <div class="maniobres_observacions_container">
                        <table>
                            <tr>
                                <th>${_("Observacions")}</th>
                            </tr>
                            %if action == "treure":
                                <tr>
                                    <td class="nocount left_text"><div class="maniobres_observacions">${descarrec.observacions_obertura or ''}</div></td>
                                </tr>
                            %elif action == "donar":
                                <tr>
                                    <td class="nocount left_text"><div class="maniobres_observacions">${descarrec.observacions_tancament or ''}</div></td>
                                </tr>
                            %endif
                        </table>
                    </div>.
                %endif
            %endfor
        </%block>
        %if descarrec != objects[-1]:
          <p style="page-break-after:always"></p>
        %endif
    %endfor
</body>
</html>
