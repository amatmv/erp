<html>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
        <style type="text/css">
            ${css}
        </style>
        <link rel="stylesheet" href="${addons_path}/giscedata_descarrecs/report/comunicat_carrer.css"/>
    </head>
    % for object in objects:
        %for poblacio in object.carrers_afectats:
            <div class="report_header">
                <span class="header_logo">
                    <img id="company_logo" src="data:image/jpeg;base64,${company.logo}"/>
                </span>
                <span class="header_text">${company.rml_header1 or ''}</span>
            </div>
            <body>
                <div class="box_header_container">
                    <div id="doc_title_container">
                        <div id="doc_title_content">
                            ${_("COMUNICACIÓ D'INTERRUPCIÓ PROGRAMADA DEL SUBMINISTRAMENT ELÈCTRIC")}
                        </div>
                    </div>
                    <div class="containter_left_text">
                        <div id="left_text">
                            ${_("Ref: {}".format(object.name))}
                        </div>
                    </div>
                </div>
                <div id="paragraf">
                    ${object.p_paragraf}
                </div>
                <div id="carrers">
                    <li><b>${_("Municipi")}: </b> ${poblacio.pob_name.split(":")[0]}</li>
                    <li><b>${_("Població")}:</b> ${poblacio.pob_name.split(":")[1]}</li>
                    <ul>
                    %for carrer in poblacio.carrers_afectats.splitlines():
                        <li class="hidden-bullet">${carrer}</li>
                    %endfor
                    </ul>
                </div>
            </body>
            %if poblacio != object.carrers_afectats[-1]:
                <p style="page-break-after:always"></p>
            %endif
        %endfor
    % endfor
</html>
