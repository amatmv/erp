# -*- coding: utf-8 -*-

from osv import osv, fields


class ResMunicipi(osv.osv):
    _name = 'res.municipi'
    _inherit = 'res.municipi'

    _columns = {
        'ajuntament_id': fields.many2one(
            'res.partner', 'Ajuntament', required=False
        )
    }


ResMunicipi()
