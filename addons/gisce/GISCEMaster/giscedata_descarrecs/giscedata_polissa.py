# -*- coding: utf-8 -*-
from osv import osv, fields

class giscedata_polissa(osv.osv):

    _name = "giscedata.polissa"
    _inherit = "giscedata.polissa"

    _columns = {
      'avis_telefon': fields.boolean('Telèfon'),
      'avis_escrit': fields.boolean('Escrit'),
      'avis_sms': fields.boolean('SMS'),
      'avis_email': fields.boolean('E-mail'),
      'descarrec_address': fields.many2one('res.partner.address', 'Adreça de notificació'),
      'observacions_descarrec': fields.text("Observacions")
    }

    _defaults = {
      'avis_telefon': lambda *a: 0,
      'avis_escrit': lambda *a: 0,
      'avis_sms': lambda *a: 0,
      'avis_email': lambda *a: 0,
    }

giscedata_polissa()
