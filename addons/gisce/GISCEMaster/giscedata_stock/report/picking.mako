## coding=utf-8
<%
    from dateutil import parser

    stock_move_o = pool.get('stock.move')
    address_obj = pool.get('res.partner.address')

    stock_move_state_values = stock_move_o.fields_get(cursor, uid, context={'lang': context['lang']})['state']['selection']
    stock_move_state_values = dict(stock_move_state_values)
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
        <style type="text/css">
            ${css}
        </style>
        <link rel="stylesheet" href="${addons_path}/giscedata_stock/report/picking.css"/>
    </head>
    <body>
        %for picking in objects:
            <%
                logo = company.logo
                company_name = company.name
                enterprise = company.partner_id
                cif = enterprise.vat
                address_id = enterprise.address_get(['invoice'])['invoice']
                address = address_obj.browse(cursor, uid, address_id)


                supplier_name = _("No hay datos del proveedor")
                supplier_vat = ""
                supplier_street = ""
                supplier_location = ""
                supplier_phone = ""
                if picking.address_id:
                    supplier = picking.address_id.partner_id
                    supplier_address_id = supplier.address_get()['default']
                    supplier_address = address_obj.browse(cursor, uid, supplier_address_id)
                    supplier_vat = supplier.vat
                    supplier_name = supplier.name
                    supplier_phone = supplier_address.phone
                    supplier_zip = supplier_address.zip
                    supplier_poblacion = supplier_address.id_poblacio.name
                    supplier_provincia = supplier_address.state_id.name
                    supplier_street = supplier_address.street

                    supplier_location = "{}, ".format(supplier_zip) if supplier_zip else ""
                    supplier_location += supplier_poblacion if supplier_poblacion else ""
                    supplier_location += " ({})".format(supplier_provincia) if supplier_provincia else ""

                picking_date = parser.parse(picking.date).date().strftime("%d/%m/%Y") if picking.date else ''
                picking_shipping_date = parser.parse(picking.min_date).date().strftime("%d/%m/%Y") if picking.min_date else ''

            %>
## BEGIN    HEADER
            <table id="header">
                <tr>
                    <td id="company-logo" rowspan="4"><img class="logo" src="data:image/jpeg;base64,${company.logo}"/></td>
                    <td>${company.name}</td>
                </tr>
                <tr>
                    <td>${company.partner_id.vat}</td>
                </tr>
                <tr>
                    <td>${address.street}</td>
                </tr>
                <tr>
                    <td>${address.zip}, ${address.id_municipi.name} (${address.state_id.name})</td>
                </tr>
            </table>
## END      HEADER
            <table id="picking-info">
                 <colgroup>
                   <col span="1" style="width: 50%;">
                   <col span="1" style="width: 50%;">
                </colgroup>
                <thead>
                    <tr>
                        <th id="picking-info-header" colspan="2">${_("Albarán")} - ${picking.name}</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td id="separator">
                            ${self.datos_del_albaran(picking, picking_date, picking_shipping_date)}
                        </td>
                        <td>
                            <table id="datos-del-proveedor">
                                <tr>
                                    <th>${_("Datos del proveedor")}</th>
                                </tr>
                                <tr>
                                    <td>${supplier_name}</td>
                                </tr>
                                %if supplier_vat:
                                    <tr>
                                        <td>${supplier_vat}</td>
                                    </tr>
                                %endif
                                <tr>
                                    <td>${supplier_street}</td>
                                </tr>
                                <tr>
                                    <td>${supplier_location}</td>
                                </tr>
                                <tr>
                                    <td>${supplier_phone}</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table id="picking-lines">
                                <colgroup>
                                   <col span="1" style="width: 12%;">
                                   <col span="1" style="width: 38%;">
                                   <col span="1" style="width: 12%;">
                                   <col span="1" style="width: 12%;">
                                   <col span="1" style="width: 12%;">
                                   <col span="1" style="width: 14%;">
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th class="subtitles">${_("Código")}</th>
                                        <th class="subtitles">${_("Descripción")}</th>
                                        <th class="subtitles">${_("Lote")}</th>
                                        <th class="subtitles">${_("Estado")}</th>
                                        <th class="align-right subtitles">${_("Cantidad")}</th>
                                        <th class="align-right subtitles">${_("Destino")}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    %for line in picking.move_lines:
                                        <%
                                            prodlot = line.prodlot_id.name if line.prodlot_id else ""
                                            destination = line.location_dest_id.name if line.location_dest_id else _("Desconocido")
                                        %>
                                        <tr class="row-values">
                                            <td>${line.product_id.default_code}</td>
                                            <td id="descr">${line.name}</td>
                                            <td class="qty">${prodlot}</td>
                                            <td class="qty">${stock_move_state_values[line.state]}</td>
                                            <td class="qty">${line.product_qty}</td>
                                            <td class="qty">${destination}</td>
                                        </tr>
                                    %endfor
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    %if picking.note:
                        <tr><td id="notes-header" class="bold" colspan="2">${_("Notas")}</td></tr>
                        <tr><td id="notes-body" colspan="2">${picking.note.replace('\n','<br/>')}</td></tr>
                    %endif
                </tbody>
            </table>
            %if picking != objects[-1]:
                <p style="page-break-after:always"></p>
            %endif
        %endfor
    </body>
</html>

<%def name="datos_del_albaran(picking, picking_date, picking_shipping_date)">
    <table id="datos-del-albaran">
        <colgroup>
           <col span="1" style="width: 50%;">
           <col span="1" style="width: 50%;">
        </colgroup>
        <thead>
            <tr>
                <th colspan="2">${_(u"Datos del albarán")}</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>${_("Albarán")}:</td>
                <td>${picking.name}</td>
            </tr>
            <tr>
                <td>${_("Albarán proveedor")}:</td>
                <td>${picking.supplier_packing_name or ''}</td>
            </tr>
            <tr>
                <td>${_("Fecha del albarán")}: </td>
                <td>${picking_date}</td>
            </tr>
            <tr>
                <td>${_("Fecha de envio prevista")}: </td>
                <td>${picking_shipping_date}</td>
            </tr>
        </tbody>
    </table>
</%def>