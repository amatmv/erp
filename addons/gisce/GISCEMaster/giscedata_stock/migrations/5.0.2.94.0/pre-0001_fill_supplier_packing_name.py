# -*- coding: utf-8 -*-
import logging


def migrate(cursor, installed_version):
    logger = logging.getLogger('openerp.migration')

    if not installed_version:
        return

    q = """
        UPDATE stock_picking 
        SET supplier_packing_name = 'Desconocido' 
        WHERE supplier_packing_name IS NULL        
    """

    logger.info("Emplenant l'albarà de proveïdor a l'albarà d'entrada...")
    cursor.execute(q)
    logger.info("Albarans de proveïdor emplenats.")
