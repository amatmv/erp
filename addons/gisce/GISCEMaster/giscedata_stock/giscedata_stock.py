# -*- encoding: utf-8 -*-
from osv import fields, osv
from stock import PICKING_STATES


class stock_picking(osv.osv):
    _name = "stock.picking"
    _inherit = "stock.picking"

    def __init__(self, pool, cursor):
        replace_val = ('assigned', 'Available')
        if replace_val in PICKING_STATES:
            PICKING_STATES[PICKING_STATES.index(replace_val)] = (
                'assigned', 'Pendiente'
            )
        super(stock_picking, self).__init__(pool, cursor)

    def create_empty_return_picking(self, cursor, uid, picking, pdate, invoice_state, context=None):
        """
        The created empty return picking is marked as a return picking and its
        backorder will be the original picking.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param picking: <stock.picking> browse record.
        :type picking: osv.orm.browse_record
        :param pdate: picking's date.
        :type pdate: str
        :param invoice_state: picking's invoice state (invoiced, to be invoiced,
        etc).
        :type invoice_state: str
        :param context: OpenERP context.
        :type context: dict
        :return: <stock.picking> id of the new return picking.
        :rtype: long
        """
        res = super(stock_picking, self).create_empty_return_picking(
            cursor, uid, picking, pdate, invoice_state, context=context
        )
        picking_wv = {'backorder_id': picking.id, 'return': True}
        self.write(cursor, uid, [res], picking_wv, context=context)
        return res

    def action_invoice_create(self, cursor, user, ids, journal_id=False, group=False, type='out_invoice', context=None):
        """
        When creating invoice from picking, then link the new invoice to the
        picking.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param user: <res.users> ids.
        :type user: long
        :param ids: <stock.picking> ids.
        :type ids: list[long]
        :param journal_id: <account.journal> id.
        :type journal_id: long
        :param group: ??
        :param type: invoice type (out invoice, etc)
        :type type: str
        :param context: OpenERP context
        :type context: dict
        :return:
        """
        res = super(stock_picking, self).action_invoice_create(
            cursor, user, ids, journal_id=journal_id, group=group, type=type,
            context=context
        )
        for picking_id, invoice_id in res.items():
            picking_wv = {'invoice_id': invoice_id}
            self.write(cursor, user, picking_id, picking_wv, context=context)

        return res

    _columns = {
        'name': fields.char('Albarán', size=64, select=True),
        'supplier_packing_name': fields.char(
            'Albarán proveedor', size=64, select=True
        ),
        'origin': fields.char('Pedido orígen', size=64),
        'supplier_picking_date': fields.date(
            'Fecha albarán',
            help='Indica la fecha en la que se recibió el albarán'
        ),
        'invoice_id': fields.many2one('account.invoice', 'Factura', ondelete='set null'),
        'return': fields.boolean('Devolución', help='Indica si es de devolución')
    }


stock_picking()


class stock_location(osv.osv):
    _name = "stock.location"
    _inherit = "stock.location"

    def _product_qty_available(
            self, cr, uid, ids, field_names, arg, context=None):
        if context is None:
            context = {}
        return super(stock_location, self).__product_qty_available(
            cr, uid, ids, field_names, arg, context
        )

    def _product_qty_search(self, cursor, uid, obj, names, args, context):
        if context is None:
            context = {}
        ids = []
        datas = self._product_qty_available(
            cursor, uid, self.search(cursor, uid, []), [names], False, context)
        amount = float(args[0][2])
        for sid in datas:
            if args[0][1] == '<' and datas[sid][names] < amount:
                ids.append(sid)
            elif args[0][1] == '=' and datas[sid][names] == amount:
                ids.append(sid)
            elif args[0][1] == '>' and datas[sid][names] > amount:
                ids.append(sid)
            elif args[0][1] == '!=' and datas[sid][names] != amount:
                ids.append(sid)
        return [('id', 'in', ids)]

    _columns = {
        'stock_real': fields.function(
            _product_qty_available, method=True, type='float',
            string='Stock real', multi="stock", fnct_search=_product_qty_search
        ),
        'stock_virtual': fields.function(
            _product_qty_available, method=True, type='float',
            string='Stock virtual', multi="stock",
            fnct_search=_product_qty_search
        ),
    }


stock_location()


class ProductStockLocationListView(osv.osv):
    """Product Stock Location List """
    _name = "product.stock.location.list"
    _description = "Llistat de Productes en ubicacions amb dades de stock"
    _auto = False

    _nostock_filter = """
      (
        stock_virtual_values.product_qty IS NOT NULL
        OR stock_real_values.product_qty IS NOT NULL
      )"""


    _select_zeros_from_rules = """
    SELECT
      product.id AS "product_id",
      xlocation.location_id AS "location_id",
      0 AS "product_qty"
    FROM product_product AS product
    LEFT JOIN view_product_x_location_ids AS xlocation
      ON xlocation.product_id = product.id
    LEFT JOIN stock_warehouse_orderpoint AS rules ON
      rules.location_id = xlocation.location_id
      AND product.id = rules.product_id
    WHERE rules.id IS NOT NULL
    GROUP BY xlocation.location_id, product.id"""

    _select_stock_moves = """
        SELECT
            q.product_id AS "product_id",
            q.location_id AS "location_id",
            SUM(q.stock) AS "product_qty"
        FROM (
            SELECT q.location_id, q.product_id, -SUM(q.sum) AS stock FROM (
                SELECT
                    sm.product_id AS product_id,
                    SUM(sm.product_qty),
                    sm.location_id AS location_id
                FROM stock_move AS sm
                WHERE sm.state {states}
                GROUP BY sm.location_id, sm.product_id
            ) AS q
            GROUP BY q.location_id, q.product_id
            UNION ALL
            SELECT q.location_dest_id, q.product_id, SUM(q.sum) AS stock FROM (
                SELECT
                    sm.product_id AS product_id,
                    SUM(sm.product_qty),
                    sm.location_dest_id AS location_dest_id
                FROM stock_move AS sm
                WHERE sm.state {states}
                GROUP BY sm.location_dest_id, sm.product_id
            ) AS q
            GROUP BY q.location_dest_id, q.product_id
        ) AS q
        GROUP BY q.location_id, q.product_id
        HAVING SUM(q.stock) <> 0
    """

    _select_virtual_stock_moves = _select_stock_moves.format(
        states="NOT IN ('draft', 'cancel')"
    )

    _select_real_stock_moves = _select_stock_moves.format(
        states="= 'done'"
    )

    _select_stock ="""
    SELECT
      product.id AS product_id,
      product_stock_virtual.location_id AS location_id,
      SUM(COALESCE(product_stock_virtual.product_qty, 0)) AS "product_qty",
      SUM(COALESCE(product_stock_virtual.product_qty, 0) * template.standard_price) AS "product_price"
    FROM (
      {select_zeros_from_rules} UNION ( {select_stock_moves} )
    ) AS product_stock_virtual
    LEFT JOIN product_product AS product ON
      product.id = product_stock_virtual.product_id
    LEFT JOIN product_template AS template ON
      template.id = product.product_tmpl_id
    GROUP BY
      product.id, template.id, product_stock_virtual.location_id
    """

    _select_virtual_stock = _select_stock.format(
        select_stock_moves=_select_virtual_stock_moves,
        select_zeros_from_rules=_select_zeros_from_rules
    )

    _select_real_stock = _select_stock.format(
        select_stock_moves=_select_real_stock_moves,
        select_zeros_from_rules=_select_zeros_from_rules
    )

    _select_prodlot_stock_price = """
SELECT
  lot.product_id AS product_id,
  lot_in_location.location_id AS location_id,
  COUNT(lot.id) AS product_qty,
  SUM(lot.standard_price) AS product_price
FROM (
  SELECT prodlot_id, product_id, location_dest_id AS "location_id"
  FROM stock_move
  WHERE state = 'done'
  GROUP BY prodlot_id, product_id, location_dest_id, write_date
  HAVING write_date = MAX(write_date)
) AS lot_in_location
LEFT JOIN stock_production_lot lot ON lot_in_location.prodlot_id = lot.id
GROUP BY lot.product_id, lot_in_location.location_id"""

    _query = '''
    SELECT
      -- Table "custom" fields
      ROW_NUMBER () OVER (ORDER BY product.default_code) AS "id",
      templ.name || ' - ' || location.name AS "name",
      -- Product fields
      product.id AS "product_id",
      product.default_code AS "product_code",
      templ.name AS "product_name",
      templ.categ_id AS "product_category",
      product.variants AS "product_variant",
      product.fabricant_id AS "fabricant_id",
      product.ref_fabricant AS "ref_fabricant",
      templ.standard_price AS "product_price",
      -- Stock Rules
      COALESCE(rules.product_min_qty, 0) AS "product_min",
      COALESCE(rules.product_max_qty, 0) AS "product_max",
      -- Stock
      SUM(COALESCE(stock_virtual_values.product_qty, 0)) AS "stock_virtual",
      SUM(COALESCE(stock_real_values.product_qty, 0)) AS "stock_real",
      -- Stock Price
      CASE
        WHEN product.serial_number_price = TRUE THEN
          SUM(COALESCE(stock_price.product_price, 0))
        WHEN product.serial_number_price = FALSE OR product.serial_number_price IS NULL THEN
          SUM(COALESCE(stock_real_values.product_price, 0))
      END AS "stock_price",
      -- Locations
      warehouse.id AS "warehouse_id",
      location.id AS "location_id"
      {additional_fields}
    FROM product_product AS product
    LEFT JOIN product_template AS templ ON product.product_tmpl_id = templ.id
    LEFT JOIN view_product_x_location_ids AS xlocation ON xlocation.product_id = product.id
    LEFT JOIN stock_location AS location ON location.id = xlocation.location_id
    LEFT JOIN stock_warehouse AS warehouse ON
      warehouse.lot_stock_id = location.id
      OR warehouse.lot_input_id = location.id
      OR warehouse.lot_output_id = location.id
      OR warehouse.lot_stock_id = location.location_id
      OR warehouse.lot_input_id = location.location_id
      OR warehouse.lot_output_id = location.location_id
    LEFT JOIN ({select_virtual_stock}) AS stock_virtual_values ON
      location.id = stock_virtual_values.location_id
      AND product.id = stock_virtual_values.product_id
    LEFT JOIN ({select_real_stock}) AS stock_real_values ON
      location.id = stock_real_values.location_id
      AND product.id = stock_real_values.product_id
    LEFT JOIN ({select_prodlot_stock_price}) AS stock_price ON
      location.id = stock_price.location_id
      AND product.id = stock_price.product_id
    LEFT OUTER JOIN stock_warehouse_orderpoint AS rules ON 
      rules.warehouse_id = warehouse.id AND
      rules.location_id = location.id AND
      rules.product_id = product.id
    {additional_joins}
    WHERE
      templ.categ_id IN %(categ_ids)s
      AND location.id IN %(location_ids)s
      {additional_filters}
    GROUP BY
      product.id, templ.id, location.id, warehouse.id, rules.id
      {additional_groupby}
    ORDER BY product.default_code
    '''

    # Other Views
    _view_product_x_location_ids_name = "view_product_x_location_ids"
    _view_product_x_location_ids_query = """
    SELECT
      product.id AS "product_id",
      location.id AS "location_id"
    FROM product_product AS product
    CROSS JOIN stock_location AS location
    ORDER BY product.id, location.id"""

    _views = [
        _name.replace('.', '_'),
        _view_product_x_location_ids_name,
    ]

    _columns = {
        # Self fields
        'id': fields.integer('ID', readonly=True),
        'name': fields.char('Texto', size=120, readonly=True),
        # Product fields
        'product_id': fields.many2one(
            'product.product', 'Producto', readonly=True),
        'product_code': fields.char('Código', size=16, readonly=True),
        'product_variant': fields.char(
            size=150, string='Variante', readonly=True),
        'fabricant_id': fields.many2one(
            'res.partner', 'Fabricante', readonly=True),
        'ref_fabricant': fields.char(
            size=150, string='Referencia del fabricante', readonly=True),
        'product_category': fields.many2one(
            'product.category', 'Categoría', readonly=True
        ),
        'product_price': fields.float('Precio', readonly=True),
        # Stock rules min/max stock values
        'product_min': fields.integer('Stock mínimo', readonly=True),
        'product_max': fields.integer('Stock máximo', readonly=True),
        # Stock Available
        'stock_virtual': fields.integer('Stock virtual', readonly=True),
        'stock_real': fields.integer('Stock real', readonly=True),
        'stock_price': fields.float('Valor del Stock', readonly=True),
        # Locations
        'warehouse_id': fields.many2one(
            'stock.warehouse', 'Almacén', readonly=True),
        'location_id': fields.many2one(
            'stock.location', 'Ubicación', readonly=True),
        # Location in location
        'location_fila': fields.char(
            'Fila', size=100, readonly=True),
        'location_nivel': fields.char(
            'Nivel', size=100, readonly=True),
        'location_columna': fields.char(
            'Columna', size=100, readonly=True),
    }

    def get_location_ids(self, cursor, uid, context=None):
        location_obj = self.pool.get('stock.location')
        parent_id = location_obj.search(cursor, uid, [
            ('name', '=ilike', 'stock')
        ])
        if parent_id:
            location_ids = location_obj.search(cursor, uid, [
                ('id', 'child_of', parent_id)
            ])
        else:
            location_ids = location_obj.search(cursor, uid, [])
        return location_ids

    def get_categ_ids(self, cursor, uid, context=None):
        categ_obj = self.pool.get('product.category')
        varconf_o = self.pool.get('res.config')

        materials_category_name = varconf_o.get(
            cursor, uid, 'stock_materials_category', 'Materials'
        )

        dmn = [('name', '=', materials_category_name)]
        categ_mat_id = categ_obj.search(cursor, uid, dmn, context=context)

        if categ_mat_id:
            dmn = [('id', 'child_of', categ_mat_id)]
            categ_ids = categ_obj.search(cursor, uid, dmn, context=context)
        else:
            categ_ids = categ_obj.search(cursor, uid, [], context=context)

        return categ_ids

    def get_additional_fields(self, cursor, uid, kwargs):
        fields = ""
        if self.is_module_installed(cursor, uid, 'product_locations'):
            fields += """,
        plocation.fila AS "location_fila",
        plocation.nivel AS "location_nivel",
        plocation.columna AS "location_columna"
        """
        else:
            fields += """,
        templ.loc_row AS "location_fila",
        templ.loc_rack AS "location_nivel",
        templ.loc_case AS "location_columna"
        """
        return fields

    def get_additional_joins(self, cursor, uid, kwargs):
        joins = ""
        if self.is_module_installed(cursor, uid, 'product_locations'):
            joins += """
        LEFT OUTER JOIN product_location AS plocation ON
            plocation.location_id = location.id AND
            plocation.product_id = product.id
        """
        return joins

    def get_additional_filters(self, cursor, uid, kwargs):
        filters = ""
        if not kwargs.get('nostock', False):
            filters += """
        AND (
            (
                stock_virtual_values.product_qty IS NOT NULL
            ) OR (
                stock_real_values.product_qty IS NOT NULL
            )
        )"""
        return filters

    def get_additional_groupby(self, cursor, uid, kwargs):
        groupby = ""
        if self.is_module_installed(cursor, uid, 'product_locations'):
            groupby += ",plocation.id"
        return groupby

    def get_updated_query(self, cursor, uid, kwargs):
        format_values = {
            'additional_fields': self.get_additional_fields(
                cursor, uid, kwargs),
            'additional_joins': self.get_additional_joins(
                cursor, uid, kwargs),
            'additional_filters': self.get_additional_filters(
                cursor, uid, kwargs),
            'additional_groupby': self.get_additional_groupby(
                cursor, uid, kwargs),
            'select_zeros_from_rules': self._select_zeros_from_rules,
            'select_virtual_stock': self._select_virtual_stock,
            'select_real_stock': self._select_real_stock,
            'select_prodlot_stock_price': self._select_prodlot_stock_price
        }
        return self._query .format(**format_values)

    def drop_views(self, cursor, uid, context=None):
        """
        This method drops all materialized views created by this module
        :param cursor:
        :param uid:
        :param context:
        :return:
        """
        for view_name in self._views:
            query = (
                "DROP MATERIALIZED VIEW IF EXISTS {view_name}"
            ).format(
                view_name=view_name
            )
            cursor.execute(query)

    def init_views(self, cursor, uid, context=None):
        """
        This method creates the required views for the main view

        - view_product_x_location_ids:
            Cross join between products and locations
        :param cursor:
        :param uid:
        :param context:
        :return:
        """
        if context is None:
            context = {}

        create_view_query = (
            "CREATE MATERIALIZED VIEW public.{view_name}\nAS {query}"
        ).format(
            view_name=self._view_product_x_location_ids_name,
            query=self._view_product_x_location_ids_query
        )
        cursor.execute(create_view_query)

    def redo_view(self, cursor, uid, **kwargs):
        self.drop_views(cursor, uid)
        self.init_views(cursor, uid)

        categ_ids = self.get_categ_ids(cursor, uid)
        location_ids = self.get_location_ids(cursor, uid)

        query = (
            "CREATE MATERIALIZED VIEW public.{view_name}\nAS {mat_query}"
        ).format(**{
            'view_name': self._name.replace('.', '_'),
            'mat_query': self.get_updated_query(cursor, uid, kwargs),
        })
        cursor.execute(query, {
            'categ_ids': tuple(categ_ids),
            'location_ids': tuple(location_ids),
        })

    def init(self, cursor):
        self.redo_view(cursor, 1)

    def refresh(self, cursor, uid, **kwargs):
        # TODO: Check if new categories to not redo the view
        self.redo_view(cursor, uid, **kwargs)

    def is_module_installed(self, cursor, uid, modname):
        modules_obj = self.pool.get('ir.module.module')
        mod_inst = modules_obj.search(
            cursor, uid, [
                ('name', '=', modname),
                ('state', '=', 'installed')
            ]
        )
        return True if mod_inst else False


ProductStockLocationListView()
