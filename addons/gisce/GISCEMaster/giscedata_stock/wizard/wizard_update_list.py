# -*- encoding: utf-8 -*-
from osv import fields, osv
from tools.translate import _


class UpdateProductStockLocationListView(osv.osv_memory):
    """Product Stock Location List """
    _name = "wizard.update.product.stock.location.list"

    _columns = {
        'nostock': fields.boolean('Mostrar productos sin stock', default=False),
    }

    def action_refresh_list_view(self, cursor, uid, ids, context=None):
        list_obj = self.pool.get('product.stock.location.list')
        wiz = self.browse(cursor, uid, ids)[0]
        list_obj.refresh(cursor, uid, nostock=wiz.nostock)
        return {
            'name': _('Updated Product Stock List'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'product.stock.location.list',
            'type': 'ir.actions.act_window'
        }


UpdateProductStockLocationListView()
