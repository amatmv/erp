# -*- coding: utf-8 -*-
{
    "name": "Giscedata Stock",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
- Nous camps als albarans
- Renombra etiquetes d'albarans
- Afegeix llistat complet de productes en stock
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends": [
        'stock',
        'mrp',
        'product_fabricant',
        'giscedata_product',
        "c2c_webkit_report",
        'partner_address_tipovia',
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        'wizard/wizard_dividir_moviment_view.xml',
        'giscedata_stock_view.xml',
        'invoice_view.xml',
        'wizard/wizard_update_list.xml',
        'security/ir.model.access.csv',
        'giscedata_stock_report.xml',
        'giscedata_stock_data.xml',
    ],
    "active": False,
"installable": True
}
