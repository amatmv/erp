# -*- coding: utf-8 -*-
# TODO
# make multiple pickings and check: real stock and virtual stock
from destral import testing
from destral.transaction import Transaction


class TestProductStockLocation(testing.OOTestCase):

    def setUp(self):
        self.pool = self.openerp.pool

    def test_stock_rules(self):
        """
        Tests if the minimum and maximum stock values are consistent.
        """
        orderpoint_o = self.pool.get('stock.warehouse.orderpoint')
        product_stock_location_list_o = self.pool.get('product.stock.location.list')
        irmd_o = self.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            kwargs = {'nostock': 0}
            q = product_stock_location_list_o.get_updated_query(cursor, uid, kwargs)

            categ_ids = product_stock_location_list_o.get_categ_ids(cursor, uid)
            location_ids = product_stock_location_list_o.get_location_ids(cursor, uid)

            # We'll only test the stock by location for the product MB1
            product_id = irmd_o.get_object_reference(
                cursor, uid, 'product', 'product_product_mb1'
            )[1]

            # These parameters are the default parameters when creating the
            # materialized view from the wizard.
            q_params = {
                'categ_ids': tuple(categ_ids),
                'location_ids': tuple(location_ids)
            }

            cursor.execute(q, q_params)
            product_stock_location_list = cursor.dictfetchall()

            dmn = [('product_id', '=', product_id)]
            # BE AWARE: this test only works if there is only one orderpoint
            # for the product MB1. If for some reason we add a new orderpoint
            # to this product, the test should be modified.
            mb1_orderpoint_id = orderpoint_o.search(cursor, uid, dmn)[0]
            orderpoint_f = ['location_id', 'product_min_qty', 'product_max_qty']
            mb1_orderpoint_v = orderpoint_o.read(
                cursor, uid, mb1_orderpoint_id, orderpoint_f
            )

            for product_in_location in product_stock_location_list:
                # We'll only test the stock by location for the product MB1
                if product_in_location['product_id'] == product_id:
                    # If the orderpoint is configured for the same location
                    # as the stock by the product-location row, then the query
                    # should return the same maximum/minimum quantities as is
                    # configured in the orderpoint
                    if product_in_location['location_id'] == mb1_orderpoint_v['location_id'][0]:
                        product_min_qty = mb1_orderpoint_v['product_min_qty']
                        product_max_qty = mb1_orderpoint_v['product_max_qty']
                    # else, the query values must be 0 (as there are not
                    # orderpoints configured for this location).
                    else:
                        product_min_qty = 0
                        product_max_qty = 0

                    self.assertEqual(product_in_location['product_min'], product_min_qty)
                    self.assertEqual(product_in_location['product_max'], product_max_qty)
