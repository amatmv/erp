from osv import osv, fields


class account_invoice(osv.osv):

    _name = "account.invoice"
    _inherit = "account.invoice"

    def unlink(self, cr, uid, ids, context=None):
        """
        When unlink invoice, change invoice state of the related pickings to
        'to be invoiced'.
        :param cr:
        :param uid: <res.users> id
        :type uid: long
        :param ids: <account.invoice> ids
        :type ids: [long]
        :param context: OpenERP context
        :type context: {}
        :return: True if unlinked
        :rtype: bool
        """
        picking_o = self.pool.get('stock.picking')
        if isinstance(ids, list):
            dmn = [('invoice_id', 'in', ids)]
        else:
            dmn = [('invoice_id', '=', ids)]

        dmn.append(('invoice_state', '=', 'invoiced'))
        picking_ids = picking_o.search(cr, uid, dmn, context=context)

        res = super(account_invoice, self).unlink(cr, uid, ids, context=context)
        if res:
            if picking_ids:
                picking_wv = {'invoice_state': '2binvoiced'}
                picking_o.write(cr, uid, picking_ids, picking_wv, context=context)

        return res

    _columns = {
        'picking_ids': fields.one2many(
            'stock.picking', 'invoice_id', 'Albaranes'
        )
    }


account_invoice()
