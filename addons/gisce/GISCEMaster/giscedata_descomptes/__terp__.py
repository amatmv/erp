# -*- coding: utf-8 -*-
{
    "name": "Módulo de descuentos",
    "description": """
        Módulo de descuentos que añade la posibilidad de configurar los 
        descuentos desde el contrato, con una fecha de inicio y fin.
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends": [
        "giscedata_serveis_contractats",
        "giscedata_facturacio_comer"
    ],
    "init_xml": [],
    "demo_xml": [
        "giscedata_facturacio_demo.xml"
    ],
    "update_xml": [
        "security/ir.model.access.csv",
        "giscedata_facturacio_data.xml",
        "giscedata_polissa_view.xml",
    ],
    "active": False,
    "installable": True
}
