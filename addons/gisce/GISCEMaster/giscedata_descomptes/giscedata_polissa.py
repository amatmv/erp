# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscedataPolissa(osv.osv):

    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def copy_data(self, cursor, uid, ids, default=None, context=None):
        """
        Avoid to duplicate the discounts when the contract is duplicated.
        """
        if default is None:
            default = {}
        if context is None:
            context = {}
        default_values = {
            'descomptes': False,
        }
        default.update(default_values)
        res_id = super(GiscedataPolissa, self).copy_data(
            cursor, uid, ids, default, context)
        return res_id

    def _search_discounts(self, cursor, uid, obj, name, args, context=None):
        """
        Returns the domain that will be used to find the contracts that have
        the discounts.
        :param cursor:
        :param uid:
        :param obj: the object over which we are searching
        :param name: name of the field on which we are searching
        :param args: domain that has been introduced to the filter. In this
        case, will have the format: [('buscar_descompte', 'ilike', 'X')]
        :type args: list
        :param context:
        :return:
        """
        if context is None:
            context = {}

        if not args:
            return [('id', '=', 0)]
        disc_obj = self.pool.get('giscedata.facturacio.descompte')
        term_introduced = args[0][2]
        disc_ids = disc_obj.search(cursor, uid, [
            ('product.name', 'ilike', term_introduced)
        ])

        if disc_ids:
            discounts_v = disc_obj.read(cursor, uid, disc_ids, ['polissa_id'])
            contract_ids = [
                disc['polissa_id'][0] for disc in discounts_v
            ]
            return [('id', 'in', contract_ids)]
        else:
            return [('id', '=', 0)]

    def _discounts(self, cursor, uid, ids, field_name, arg, context=None):
        return dict.fromkeys(ids, False)

    _columns = {
        'descomptes': fields.one2many(
            'giscedata.facturacio.descompte', 'polissa_id', 'Descomptes'
        ),
        'buscar_descompte': fields.function(
            _discounts, type='char', string=u'Descomptes',
            fnct_search=_search_discounts
        )
    }


GiscedataPolissa()
