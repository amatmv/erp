# -*- encoding: utf-8 -*-

from osv import osv, fields


class GiscedataFacturacioDescompte(osv.osv):

    _name = 'giscedata.facturacio.descompte'

    @staticmethod
    def _get_discount_desc(discount):
        """
        Returns the description of the discount that will be used as a
        description of the invoice line.
        :param discount: the discount
        :type discount: browse_record
        :return: The description
        :rtype: str
        """
        return discount.product.name or 'Campaña de descuento'

    @staticmethod
    def applicable_discount(fact, discount):
        """
        Check if the discount is applicable on the invoice.
        :param fact: the invoice
        :type fact: browse_record
        :param discount: the discount
        :type discount: browse_record
        :return: True if the dates of the discount are overlapped with the dates
        of the invoice.
        """

        def between(date_1, date_2, date_3):
            """
            :return: date_1 is between date_2 and date_3
            """
            return date_2 <= date_1 <= date_3

        end_date = discount.end_date or '3000-01-01'
        return (
                between(fact.data_inici, discount.start_date, end_date)
                or between(fact.data_final, discount.start_date, end_date)
        )

    def get_discount_line_vals(self, cursor, uid, ids, fact, context=None):
        """
        Get the vals of the discount line that will be created in the invoice.
        :param cursor:
        :param uid:
        :param ids: the service applicable
        :param ids: list | long
        :param fact: the invoice
        :type fact: browse_record
        :param context:
        :return: dict containing the values
        """
        if isinstance(ids, (tuple, list)):
            ids = ids[0]
        if context is None:
            context = {}

        discount = self.browse(cursor, uid, ids)
        pricelist_obj = self.pool.get('product.pricelist')
        product_o = self.pool.get('product.product')

        discount_pricelist_id = discount.pricelist.id
        ctx = context.copy()
        price = 0.0
        for fact_line in fact.linia_ids:
            ctx['pricelist_base_price'] = fact_line.price_subtotal
            # Price of the invoice line calculated over the pricelist of the
            # discount
            product_id = False

            if fact_line.product_id:
                product_id = fact_line.product_id.id

            elif fact_line.tipus == 'lloguer':
                # The meter rent lines at the comer don't have a product
                dmn = [('default_code', '=', 'ALQ01')]
                product_ids = product_o.search(cursor, uid, dmn, context=context)
                product_id = product_ids[0]

            if product_id:
                price_by_pricelist = pricelist_obj.price_get(
                    cursor, uid, [discount_pricelist_id], product_id, 1.0,
                    context=ctx
                )
                price += price_by_pricelist[discount_pricelist_id]

        vals = {
            'uos_id': discount.product.uom_id.id,
            'multi': 1,
            'product_id': discount.product.id,
            'tipus': 'altres',
            'name': self._get_discount_desc(discount),
            'data_desde': fact.data_inici,
            'data_fins': fact.data_final,
            'quantity': 1,
            'force_price': -price,
            'isdiscount': True,
        }
        return vals

    def create_discount_lines(self, cursor, uid, fact, context):
        """
        Creates the discount lines that are to be created in the invoice based
        on the discounts applicable on it.
        :param cursor:
        :param uid:
        :param fact: the invoice
        :type fact: browse_record
        :param context: context
        :type context: dict
        :return: None
        """
        if context is None:
            context = {}

        disc_obj = self.pool.get('giscedata.facturacio.descompte')
        facturador_obj = self.pool.get('giscedata.facturacio.facturador')

        for discount in fact.polissa_id.descomptes:
            if self.applicable_discount(fact, discount):
                discount_line_vals = disc_obj.get_discount_line_vals(
                    cursor, uid, discount.id, fact, context=context)
                facturador_obj.crear_linia(
                    cursor, uid, fact.id, discount_line_vals,
                    # We want the new invoice line to take our discount
                    # pricelist instead of the invoice one.
                    context={'force_llista_preu_id': discount.pricelist.id}
                )

    _columns = {
        'polissa_id': fields.many2one(
            'giscedata.polissa', 'Contrato', required=True),
        'product': fields.many2one(
            'product.product', 'Producto', required=True),
        'start_date': fields.date('Fecha Inicio', required=True),
        'end_date': fields.date('Fecha Fin'),
        'pricelist': fields.many2one(
            'product.pricelist', 'Lista de precios', required=True)
    }

    _order = 'start_date'


GiscedataFacturacioDescompte()


class GiscedataFacturacioFacturador(osv.osv):
    """ discount line to invoice discounts"""

    _name = 'giscedata.facturacio.facturador'
    _inherit = 'giscedata.facturacio.facturador'

    def fact_via_lectures(self, cursor, uid, polissa_id, lot_id, context=None):
        """
        Overwrite the method to include the discount line if needed.
        """
        if context is None:
            context = {}
        fact_obj = self.pool.get('giscedata.facturacio.factura')
        discount_obj = self.pool.get('giscedata.facturacio.descompte')

        fact_ids = super(
            GiscedataFacturacioFacturador, self
        ).fact_via_lectures(cursor, uid, polissa_id, lot_id, context)

        for fact in fact_obj.browse(cursor, uid, fact_ids, context):
            if fact.journal_id.code in ('ENERGIA', 'ENERGIA.R') and fact.polissa_id.descomptes:
                discount_obj.create_discount_lines(cursor, uid, fact, context)

        fact_obj.button_reset_taxes(cursor, uid, fact_ids, context)
        return fact_ids


GiscedataFacturacioFacturador()
