# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction

from giscedata_polissa.tests import utils as utils_polissa

from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta


class TestsFacturacionDescuento(testing.OOTestCase):

    def setUp(self):
        self.pool = self.openerp.pool
        self.imd_obj = self.pool.get('ir.model.data')
        self.fact_obj = self.pool.get('giscedata.facturacio.factura')
        self.polissa_obj = self.pool.get('giscedata.polissa')
        self.wiz_obj = self.pool.get('wizard.manual.invoice')
        self.journal_obj = self.pool.get('account.journal')
        self.product_obj = self.pool.get('product.product')
        self.lectura_obj = self.pool.get('giscedata.lectures.lectura')
        self.lectura_pot_obj = self.pool.get('giscedata.lectures.potencia')
        self.discount_obj = self.pool.get('giscedata.facturacio.descompte')
        self.tax_obj = self.pool.get('account.tax')
        self.openerp.install_module(
            'giscedata_tarifas_peajes_20190101'
        )
        self.openerp.install_module(
            'giscedata_tarifas_pagos_capacidad_20190101'
        )
        self.FECHA_INICIO_FACTURA = '2019-01-01'
        self.FECHA_FIN_FACTURA = '2019-01-31'
        self.txn = Transaction().start(self.database)

    def tearDown(self):
        self.txn.stop()

    def setup_contract(self, cursor, uid, context=None):
        if context is None:
            context = {}
        self.import_data(cursor, uid)
        polissa = self.polissa_obj.browse(
            cursor, uid, self.polissa_id
        )

        tarifa = self.imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'tarifa_20DHS'
        )[1]

        kw_day_unity = self.pool.get('product.uom').search(
            cursor, uid, [('name', '=', 'kW/dia')])[0]
        polissa.write({
            'potencia': context.get('potencia', 2),
            'llista_preu': self.pricelist_id,
            'tarifa': tarifa,
            'property_unitat_potencia': kw_day_unity
        })
        polissa.send_signal([
            'validar', 'contracte'
        ])

        # Borramos todas las lecturas activos para evitar interferencias
        for comptador in polissa.comptadors:
            for l in comptador.lectures:
                l.unlink(context={})
            for lp in comptador.lectures_pot:
                lp.unlink(context={})
            comptador.write({'lloguer': True, 'preu_lloguer': 0.5})

        comptador = polissa.comptadors[0]
        self.comptador_id = comptador.id

        vals = {
            'name': datetime.strftime(
                (
                    datetime.strptime(self.FECHA_INICIO_FACTURA, '%Y-%m-%d') +
                    relativedelta(months=-1)
                ), '%Y-%m-%d'
            ),
            'periode': self.periode_id,
            'lectura': 10,
            'tipus': 'A',
            'comptador': comptador.id,
            'observacions': '',
            'origen_id': self.origen_id,
        }
        self.lectura_obj.create(cursor, uid, vals)

        vals = {
            'name': datetime.strftime(
                (
                    datetime.strptime(self.FECHA_FIN_FACTURA, '%Y-%m-%d') -
                    timedelta(days=1)
                ), '%Y-%m-%d'
            ),
            'periode': self.periode_id,
            'lectura': 200,
            'tipus': 'A',
            'comptador': comptador.id,
            'observacions': '',
            'origen_id': self.origen_id,
        }
        self.lectura_obj.create(cursor, uid, vals)

        self.p2_period = self.imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'p2_e_tarifa_20DHS'
        )[1]

        self.lectura_obj.create(cursor, uid, {
            'name': datetime.strftime(
                (
                        datetime.strptime(self.FECHA_INICIO_FACTURA,
                                          '%Y-%m-%d') +
                        relativedelta(months=-1)
                ), '%Y-%m-%d'
            ),
            'periode': self.p2_period,
            'lectura': 10,
            'tipus': 'A',
            'comptador': self.comptador_id,
            'observacions': '',
            'origen_id': self.origen_id,
        })
        self.lectura_obj.create(cursor, uid, {
            'name': datetime.strftime(
                (
                        datetime.strptime(self.FECHA_FIN_FACTURA, '%Y-%m-%d') -
                        timedelta(days=1)
                ), '%Y-%m-%d'
            ),
            'periode': self.p2_period,
            'lectura': 200,
            'tipus': 'A',
            'comptador': self.comptador_id,
            'observacions': '',
            'origen_id': self.origen_id,
        })

        utils_polissa.crear_modcon(
            self.pool, cursor, uid, self.polissa_id, {
                'potencies_periode': [
                    (1, polissa.potencies_periode[0].id, {'potencia': 7.000})
                ]
             }, '2018-01-01', '2019-12-31'
        )

        config_taxes_wiz_obj = self.pool.get('config.taxes.facturacio.comer')

        iva_venda = self.tax_obj.search(cursor, uid, [
            ('name', '=', 'IVA 21%'),
            ('type_tax_use', 'ilike', 'sale')
        ])[0]
        iva_compra = self.tax_obj.search(cursor, uid, [
            ('name', 'ilike', '%21% IVA Soportado (operaciones corrientes)%'),
            ('type_tax_use', 'ilike', 'purchase')
        ])[0]
        iese_venda = self.tax_obj.search(cursor, uid, [
            ('name', 'ilike', '%Impuesto especial sobre la electricidad%'),
        ])[0]
        wiz_id = config_taxes_wiz_obj.create(cursor, uid, {
            'iva_venda': iva_venda,
            'iese_venda': iese_venda,
            'iva_compra': iva_compra,
        })
        config_taxes_wiz_obj.action_set(cursor, uid, [wiz_id])

        self.product_obj.write(cursor, uid, self.discount_product, {
            'taxes_id': [(6, 0, [iva_venda, iese_venda])]
        })

        res_obj = self.pool.get('res.config')
        res_id = res_obj.search(
            cursor, uid, [('name', '=', 'fact_uom_cof_to_multi')])
        res_obj.write(cursor, uid, res_id, {'value': 0})

        values = {
            'polissa_id': context.get('polissa_id', self.polissa_id),
            'start_date': '2019-01-01',
            'end_date': '2019-12-31',
            'pricelist': self.discount_pricelist_id,
            'product': self.discount_product,
        }

        self.discount_obj.create(cursor, uid, values)
        return polissa

    def manual_invoice(self, cursor, uid, fecha_inicio='', fecha_fin=''):

        wiz_id = self.wiz_obj.create(cursor, uid, {
            'polissa_id': self.polissa_id,
            'date_start': fecha_inicio or self.FECHA_INICIO_FACTURA,
            'date_end': fecha_fin or self.FECHA_FIN_FACTURA,
            'journal_id': self.journal_id
        })
        wiz_fact = self.wiz_obj.browse(cursor, uid, wiz_id)

        wiz_fact.action_manual_invoice()
        invoice_id = eval(wiz_fact.invoice_ids)[0]
        wiz_fact.unlink()

        fact = self.fact_obj.browse(
            cursor, uid, invoice_id
        )
        period_id = self.pool.get('account.period').find(
            cursor, uid, datetime.today()
        )[0]

        fact.write({'period_id': period_id})

        return fact

    @staticmethod
    def add_days_to_date(date, days=0):
        return datetime.strftime((
                datetime.strptime(date, '%Y-%m-%d') +
                timedelta(days=days)
        ), '%Y-%m-%d')

    def import_data(self, cursor, uid):
        self.polissa_id = self.imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]
        self.pricelist_id = self.imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio',
            'pricelist_tarifas_electricidad'
        )[1]
        self.discount_pricelist_id = self.imd_obj.get_object_reference(
            cursor, uid, 'giscedata_descomptes',
            'pricelist_discounts'
        )[1]

        self.discount_product = self.imd_obj.get_object_reference(
            cursor, uid, 'giscedata_descomptes', 'product_discount'
        )[1]

        self.periode_id = self.imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'p1_e_tarifa_20DHS'
        )[1]

        self.origen_id = self.imd_obj.get_object_reference(
            cursor, uid, 'giscedata_lectures', 'origen10'
        )[1]

        self.journal_id = self.journal_obj.search(
            cursor, uid, [('code', '=', 'ENERGIA')]
        )[0]

    def get_discount_lines(self, invoice):
        return filter(
            lambda invoice_line: invoice_line.price_subtotal < 0,
            invoice.linia_ids
        )

    def test_create_discount_line_when_creating_invoice(self):
        """
        Comprueba que la línea de descuento se crea cuando se facturan
        manualmente las lecturas, y que ésta se crea bien:
        - El precio subtotal es la energía de la factura con el descuento
        aplicado.
        - Los impuestos se calculan bien.
        - El nombre de la línea es el correcto.
        - El tipo de la línea es el correcto.
        """

        cursor = self.txn.cursor
        uid = self.txn.user

        self.setup_contract(cursor, uid)
        fact = self.manual_invoice(cursor, uid)

        discount_lines = self.get_discount_lines(fact)
        self.assertTrue(discount_lines)
        discount_line = discount_lines[0]
        self.assertEquals(discount_line.tipus, 'altres')
        self.assertEquals(len(discount_line.invoice_line_tax_id), 2)
        self.assertEquals(round(fact.total_energia * 0.05, 2),
                          round(-discount_line.price_subtotal, 2))

    def test_coherent_amount_when_opening_invoice(self):
        """
        Comprueba que al abrir la factura, el residual e importes totales son
        coherentes con el descuento aplicado.
        """

        cursor = self.txn.cursor
        uid = self.txn.user

        self.setup_contract(cursor, uid)
        fact = self.manual_invoice(cursor, uid)
        amount_draft = fact.amount_total
        fact.invoice_open()

        discount_lines = self.get_discount_lines(fact)
        self.assertTrue(discount_lines)
        self.assertEquals(amount_draft, fact.amount_total)

    def test_coherent_amount_when_rectifying_invoice(self):
        """
        Comprueba que al anular y rectificar una facturam los importes y el
        balance total es correcto.
        """

        cursor = self.txn.cursor
        uid = self.txn.user

        self.setup_contract(cursor, uid)
        fact = self.manual_invoice(cursor, uid)
        amount_draft = fact.amount_total
        fact.invoice_open()

        discount_lines = self.get_discount_lines(fact)
        self.assertTrue(discount_lines)
        self.assertEquals(amount_draft, fact.amount_total)
