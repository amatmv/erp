# -*- coding: utf-8 -*-

from addons.gisceupgrade_migratoor.gisceupgrade_migratoor import GisceUpgradeMigratoor
import netsvc


def migrate(cursor, installed_version):
    logger = netsvc.Logger()
    logger.notifyChannel('migration', netsvc.LOG_INFO, 'Migration from %s'
                         % installed_version)

    # La normativa antiga es diu "2" la passem a Normativa 2 i així es podrà
    # enllaçar durant la migració.
    cursor.execute("UPDATE giscedata_revisions_at_normativa "
                   "SET name ='Normativa 2' WHERE name ='2'")

    # backup
    mig = GisceUpgradeMigratoor('giscedata_revisions', cursor)
    mig.backup(only_load=True)

    # Fixem quins camps volem fer servir per tal de vincular registres que ja
    # existeixen amb els que ens venen al data.xml
    mig.pre_xml({
        'giscedata.revisions.at.tipusdefectes': ['name'],
        'giscedata.revisions.ct.tipusdefectes': ['name'],
        'giscedata.revisions.at.tipusvaloracio': ['t_qty', 't_type'],
        'giscedata.revisions.ct.tipusvaloracio': ['t_qty', 't_type'],
        'giscedata.revisions.at.normativa': ['name'],
        'giscedata.revisions.ct.normativa': ['name']
    })