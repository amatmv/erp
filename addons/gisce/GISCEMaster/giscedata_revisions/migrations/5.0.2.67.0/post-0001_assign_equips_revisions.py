# -*- coding: utf-8 -*-
import netsvc
from oopgrade import oopgrade


def up(cursor, installed_version):
    if not installed_version:
        return
    logger = netsvc.Logger()
    # Cal assignar a totes les revisions AT l'equip utilitzat que
    #   Fins ara era hardcoded
    # AT: {
    #   E-01 MESURADOR DE POSADA A TERRA, E-02 MESURADOR DE DISTÀNCIES LASER,
    #   E-08 GALGA MEDIDOR DE CABLES + PÈRTIGA, E-15 PLOMADA
    # }
    equips_at = ['E-01', 'E-02', 'E-08', 'E-15']

    query = "SELECT id FROM giscedata_revisions_equips WHERE codi IN %s"
    cursor.execute(query, (tuple(equips_at),))
    equips_list = [x[0] for x in cursor.fetchall()]

    logger.notifyChannel(
        'migration', netsvc.LOG_INFO, 'Updating equips for all revisions AT'
    )
    cursor.execute(
        "SELECT id FROM giscedata_revisions_at_revisio WHERE state='tancada'"
    )
    revisions = cursor.fetchall()
    insert_list = []
    for revisio in revisions:   # For each revisio BT
        id_revisio = revisio[0]
        for id_equip in equips_list:      # For each equip used in the reports
            insert = "INSERT INTO giscedata_revisions_at_equips " \
                "(equip_id, revisio_id) VALUES (%s, %s)"
            params = (id_equip, id_revisio)
            insert_list.append((insert, params))
    for insert, params in insert_list:
        cursor.execute(insert, params)


def down(cursor):
    return

migrate = up
