# -*- coding: utf-8 -*-

import netsvc
from addons.gisceupgrade_migratoor.gisceupgrade_migratoor\
    import GisceUpgradeMigratoor


def migrate(cursor, installed_version):
    logger = netsvc.Logger()
    logger.notifyChannel(
        'migration', netsvc.LOG_INFO, 'Updating equips for 2016'
    )

    # Backup
    mig = GisceUpgradeMigratoor('giscedata_revisions', cursor)
    mig.backup(only_load=True)

    # Fixem quins camps volem fer servir per tal de vincular registres que ja
    # existeixen amb els que ens venen al data.xml
    mig.pre_xml({
        'giscedata.revisions.equips': ['codi'],
    })
