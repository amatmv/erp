# coding=utf-8

import logging


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')

    logger.info('Deleting menu_revisions_ct_configuracio action')
    cursor.execute(
        """
        DELETE FROM ir_values
        WHERE
            key = 'action' AND
            key2 = 'tree_but_open' AND
            res_id IN (
                SELECT res_id FROM ir_model_data
                WHERE name = 'menu_revisions_ct_configuracio'
        )"""
    )

    logger.info('Deleting menu_revisions_at_configuracio action')
    cursor.execute(
        """
        DELETE FROM ir_values
        WHERE
            key = 'action' AND
            key2 = 'tree_but_open' AND
            res_id IN (
                SELECT res_id FROM ir_model_data
                WHERE name = 'menu_revisions_at_configuracio'
        )"""
    )

    logger.info('Deleting menu_revisions_at_revisions action')
    cursor.execute(
        """
        DELETE FROM ir_values
        WHERE
            key = 'action' AND
            key2 = 'tree_but_open' AND
            res_id IN (
                SELECT res_id FROM ir_model_data
                WHERE name = 'menu_revisions_at_revisions'
        )"""
    )

    logger.info('Deleting menu_revisions_ct_automatitzacio action')
    cursor.execute(
        """
        DELETE FROM ir_values
        WHERE
            key = 'action' AND
            key2 = 'tree_but_open' AND
            res_id IN (
                SELECT res_id FROM ir_model_data
                WHERE name = 'menu_revisions_ct_automatitzacio'
        )"""
    )


def down(cursor, installed_version):
    pass

migrate = up
