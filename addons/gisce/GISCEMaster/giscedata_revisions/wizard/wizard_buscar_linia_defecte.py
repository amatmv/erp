# -*- coding: iso-8859-1 -*-
import wizard
import pooler


_init_form = """<?xml version="1.0"?>
<form string="Buscar Línies per defecte">
  <group>
    <image name="gtk-find" />
  </group>
  <group col="2">
    <field name="codi" />
    <field name="estat" />
  </group>
</form>"""

_init_fields = {
  'codi': {'string': 'Codi', 'type': 'char', 'size': 15, 'required': True},
  'estat': {'string': 'Estat', 'type': 'selection', 'selection': [(1, 'Sense reparar'), (2, 'Reparat'), (3, 'Tots')]}
}
def _init(self, cr, uid, data, context={}):
    return {'estat': 1}

def _buscar_linies(self, cr, uid, data, context={}):
    form = data['form']
    q = ''
    if form['estat'] == 1:
        q = ' and d.reparat = False'
    elif form['estat'] == 2:
        q = ' and d.reparat = True'

    cr.execute("select r.name as linia from giscedata_revisions_at_defectes d, giscedata_revisions_at_tipusdefectes td, giscedata_revisions_at_revisio r, giscedata_at_linia l where r.name = l.id and d.revisio_id = r.id and td.id = d.defecte_id and td.name like %s"+q, ('%'+form['codi']+'%',))
    ids = map(lambda x: x[0], cr.fetchall())

    action = {
      'domain': "[('id','in', ["+','.join(map(str,map(int, ids)))+"])]",
                  'name': 'Línies amb el defecte %s' % (form['codi'],),
                  'view_type': 'form',
                  'view_mode': 'tree,form',
                  'res_model': 'giscedata.at.linia',
                  'view_id': False,
      'limit': len(ids),
                  'type': 'ir.actions.act_window'
    }
    return action

def _buscar_revisions(self, cr, uid, data, context={}):
    form = data['form']
    q = ''
    if form['estat'] == 1:
        q = ' and d.reparat = False'
    elif form['estat'] == 2:
        q = ' and d.reparat = True'

    cr.execute("select r.id as revisio from giscedata_revisions_at_defectes d, giscedata_revisions_at_tipusdefectes td, giscedata_revisions_at_revisio r, giscedata_at_linia l where r.name = l.id and d.revisio_id = r.id and td.id = d.defecte_id and td.name like %s"+q, ('%'+form['codi']+'%',))
    ids = map(lambda x: x[0], cr.fetchall())

    action = {
      'domain': "[('id','in', ["+','.join(map(str,map(int, ids)))+"])]",
                  'name': 'Revisions AT amb el defecte %s' % (form['codi'],),
                  'view_type': 'form',
                  'view_mode': 'tree,form',
                  'res_model': 'giscedata.revisions.at.revisio',
                  'view_id': False,
      'limit': len(ids),
                  'type': 'ir.actions.act_window'
    }
    return action

def _buscar_defectes(self, cr, uid, data, context={}):
    form = data['form']
    q = ''
    if form['estat'] == 1:
        q = ' and d.reparat = False'
    elif form['estat'] == 2:
        q = ' and d.reparat = True'

    cr.execute("select distinct d.id,l.id from giscedata_revisions_at_defectes d, giscedata_revisions_at_tipusdefectes td, giscedata_revisions_at_revisio r, giscedata_at_linia l where r.name = l.id and d.revisio_id = r.id and td.id = d.defecte_id and td.name like %s"+q, ('%'+form['codi']+'%',))
    ids = map(lambda x: x[0], cr.fetchall())

    action = {
      'domain': "[('id','in', ["+','.join(map(str,map(int, ids)))+"])]",
                  'name': 'Defectes %s' % (form['codi'],),
                  'view_type': 'form',
                  'view_mode': 'tree,form',
                  'res_model': 'giscedata.revisions.at.defectes',
                  'view_id': False,
      'limit': len(ids),
                  'type': 'ir.actions.act_window'
    }
    return action


class wizard_buscar_linia_defecte(wizard.interface):

    states = {
      'init': {
          'actions': [_init],
        'result': {'type': 'form', 'arch': _init_form,'fields': _init_fields, 'state':[('end', 'Cancelar', 'gtk-cancel'), ('buscar_linies', 'Buscar Línies', 'gtk-ok'), ('buscar_revisions', 'Buscar Revisions', 'gtk-ok'), ('buscar_defectes', 'Buscar Defectes', 'gtk-ok')]}
      },
      'buscar_linies': {
          'actions': [],
          'result': {'type': 'action', 'action': _buscar_linies, 'state': 'end'}
      },
      'buscar_revisions': {
        'actions': [],
        'result': {'type': 'action', 'action': _buscar_revisions, 'state': 'end'}
      },
      'buscar_defectes': {
        'actions': [],
        'result': {'type': 'action', 'action': _buscar_defectes, 'state': 'end'}
      }
    }

wizard_buscar_linia_defecte('buscar.linia.defecte')
