# *-* coding: utf-8 *-*

import wizard

_init_form = """<?xml version="1.0"?>
<form string="Opcions d'entrada" col="2">
  <field name="year" />
</form>
"""

_init_fields = {
  'year': {'type': 'integer', 'string': 'Any del resum'}
}

def _print(self, cr, uid, data, context={}):
    cr.execute("select distinct r.id from giscedata_trieni t, giscedata_revisions_ct_revisio r,  giscedata_revisions_ct_defectes d where r.trimestre = t.id and d.revisio_id = r.id and d.estat = 'B' and t.anyy = %s", (data['form']['year'],))
    return {'ids': [a[0] for a in cr.fetchall()]}

class wizard_resum_defectes_anuals(wizard.interface):

    states = {
      'init': {
        'actions': [],
        'result': {'type': 'form', 'arch': _init_form, 'fields': _init_fields, 'state': [('end', 'Cancelar', 'gtk-cancel'), ('print', 'Imprimir', 'gtk-print')]}
      },
      'print': {
        'actions': [_print],
        'result': {'type': 'print', 'report':
        'giscedata.revisions.ct.revisio.report.resum.anual', 'get_id_from_action':True,
          'state':'end'}
      },
      'end': {
        'actions': [],
        'result': {'type': 'state', 'state': 'end'}
      }
    }


wizard_resum_defectes_anuals('giscedata.revisions.ct.defectes.anuals')
