# *-* coding: utf-8 *-*

import wizard

_init_form = """<?xml version="1.0"?>
<form string="Opcions d'entrada" col="2">
  <label string="Imprimir el resum de defectes de tots els anys?" colspan="2" />
</form>
"""

_init_fields = {
}

def _print(self, cr, uid, data, context={}):
    cr.execute("select distinct r.id from giscedata_trieni t, giscedata_revisions_at_revisio r,  giscedata_revisions_at_defectes d where r.trimestre = t.id and d.revisio_id = r.id")
    return {'ids': [a[0] for a in cr.fetchall()]}

class wizard_resum_defectes_anuals_totals_at(wizard.interface):

    states = {
      'init': {
        'actions': [],
        'result': {'type': 'form', 'arch': _init_form, 'fields': _init_fields, 'state': [('end', 'Cancelar', 'gtk-cancel'), ('print', 'Imprimir', 'gtk-print')]}
      },
      'print': {
        'actions': [_print],
        'result': {'type': 'print', 'report':
        'giscedata.revisions.at.revisio.report.resum.anual.totals', 'get_id_from_action':True,
          'state':'end'}
      },
      'end': {
        'actions': [],
        'result': {'type': 'state', 'state': 'end'}
      }
    }


wizard_resum_defectes_anuals_totals_at('giscedata.revisions.at.defectes.anuals.totals')
