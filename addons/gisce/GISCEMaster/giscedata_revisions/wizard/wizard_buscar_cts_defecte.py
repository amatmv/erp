# -*- coding: iso-8859-1 -*-
import wizard
import pooler


def _get_cts_tipus(self, cr, uid, context={}):
    obj = pooler.get_pool(cr.dbname).get('giscedata.cts.tipus')
    ids = obj.search(cr, uid, [])
    res = obj.read(cr, uid, ids, ['name', 'id'], context)
    res = [(r['id'], r['name']) for r in res]
    return res


_init_form = """<?xml version="1.0"?>
<form string="Buscar CTS per defecte">
  <group>
    <image name="gtk-find" />
  </group>
  <group col="2">
    <field name="tipus_ct" widget="selection"/>
    <field name="codi" domain="[('tipus_ct', '=', tipus_ct), ('actiu', '=', True)]"/>
    <field name="estat" />
  </group>
</form>"""

_init_fields = {
  'tipus_ct': {'string': 'Tipus de CT', 'type': 'many2one', 'relation':'giscedata.cts.tipus', 'selection': _get_cts_tipus, 'required': True},
  'codi': {'string': 'Codi', 'type': 'many2one', 'relation': 'giscedata.revisions.ct.tipusdefectes', 'required': True},
  'estat': {'string': 'Estat', 'type': 'selection', 'selection': [(1, 'Sense reparar'), (2, 'Reparat'), (3, 'Tots')]}
}
def _init(self, cr, uid, data, context={}):
    return {'estat': 1}

def _buscar(self, cr, uid, data, context={}):
    form = data['form']
    q = ''
    if form['estat'] == 1:
        q = ' and reparat = False'
    elif form['estat'] == 2:
        q = ' and reparat = True'

    cr.execute("select r.name as ct from giscedata_revisions_ct_defectes d, giscedata_revisions_ct_revisio r, giscedata_cts ct where r.name = ct.id and d.revisio_id = r.id and d.descripcio_id = %s and estat = 'B'"+q, (form['codi'],))
    ids = map(lambda x: x[0], cr.fetchall())
    td = pooler.get_pool(cr.dbname).get('giscedata.revisions.ct.tipusdefectes').browse(cr, uid, form['codi'])

    action = {
      'domain': "[('id','in', ["+','.join(map(str,map(int, ids)))+"])]",
                  'name': 'CTS amb el defecte %s' % (td.name,),
                  'view_type': 'form',
                  'view_mode': 'tree,form',
                  'res_model': 'giscedata.cts',
                  'view_id': False,
      'limit': len(ids),
                  'type': 'ir.actions.act_window'
    }
    return action


class wizard_buscar_cts_defecte(wizard.interface):

    states = {
      'init': {
          'actions': [_init],
        'result': {'type': 'form', 'arch': _init_form,'fields': _init_fields, 'state':[('end', 'Cancelar', 'gtk-cancel'), ('buscar', 'Buscar', 'gtk-go-forward')]}
      },
      'buscar': {
          'actions': [],
          'result': {'type': 'action', 'action': _buscar, 'state': 'end'}
      },
    }

wizard_buscar_cts_defecte('buscar.cts.defecte')
