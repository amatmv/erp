# -*- coding: utf-8 -*-
import wizard
import pooler
import calendar

_init_form = """<?xml version="1.0"?>
<form string="Crear trieni">
    <field name="trieni" />
</form>"""

_init_fields = {
  'trieni': {'string': 'Trieni a copiar', 'type': 'integer'},
}

def _init(self, cr, uid, data, context={}):
    return {'estat': 1}


def _crear(self, cr, uid, data, context={}):
    trieni_obj = pooler.get_pool(cr.dbname).get('giscedata.trieni')
    revisio_ct_obj = pooler.get_pool(cr.dbname).get('giscedata.revisions.ct.revisio')
    revisio_at_obj = pooler.get_pool(cr.dbname).get('giscedata.revisions.at.revisio')
    trieni_ids = trieni_obj.search(cr, uid, [('trieni', '=', data['form']['trieni'])])
    trimestres = {
      '1': 1,
      '2': 4,
      '3': 7,
      '4': 10,
    }
    for trieni in trieni_obj.browse(cr, uid, trieni_ids):
        vals = {
          'name': '%s/%s' % (trieni.name.split('/')[0], int(trieni.name.split('/')[1])+3),
          'anyy': trieni.anyy + 3,
          'trieni': trieni.trieni + 1,
          'data_inici': '%s-%s-01' % (trieni.anyy + 3, trimestres[trieni.name.split('/')[0]]),
          'data_final': '%s-%s-%s' % (trieni.anyy + 3, trimestres[trieni.name.split('/')[0]]+2, calendar.monthrange(trieni.anyy + 3, trimestres[trieni.name.split('/')[0]]+2)[1])
        }
        trieni_id = trieni_obj.create(cr, uid, vals)
        for ct in trieni.cts:
            revisio_ct_vals = {
              'name': ct.name.id,
              'trimestre': trieni_id,
            }
            revisio_ct_obj.create(cr, uid, revisio_ct_vals)
        for lat in trieni.lat:
            revisio_at_vals = {
              'name': lat.name.id,
              'trimestre': trieni_id,
            }
            revisio_at_obj.create(cr, uid, revisio_at_vals)

    return {}




class wizard_crear_trieni(wizard.interface):

    states = {
      'init': {
          'actions': [_init],
        'result': {'type': 'form', 'arch': _init_form,'fields': _init_fields, 'state':[('end', 'Cancelar', 'gtk-cancel'), ('crear', 'Crear', 'gtk-save')]}
      },
      'crear': {
          'actions': [_crear],
          'result': {'type': 'state', 'state': 'end'}
      },
    }

wizard_crear_trieni('giscedata.revisions.crear.trieni')
