# -*- coding: iso-8859-1 -*-
import wizard
import pooler

_avis_form = """<?xml version="1.0"?>
<form string="Avís" col="2">
  <label string="Amb aquest assistent generàs els Certificats/Actes de
  les revisions seleccionades." colspan="2" />
  <field name="versions" />
</form>"""

_avis_fields = {
    'versions': {'string': 'Versió desitjada', 'type': 'selection',\
        'selection': [('eic', 'EIC'), ('empresa', 'Empresa'), \
        ('totes', 'Totes')], 'required': True}
}


def _escollir_data(self, cr, uid, data, context={}):
    demanar_data = False
    data['revisions'] = []
    data['revisions_sd'] = []
    for revisio in pooler.get_pool(cr.dbname).get('giscedata.revisions.at.revisio').browse(cr, uid, data['ids']):
        if revisio.state == 'tancada':
            if revisio.data_ca == False:
                data['revisions_sd'].append(revisio.id)
                demanar_data = True
            else:
                data['revisions'].append(revisio.id)
    if demanar_data:
        return 'demanar_data'
    else:
        return 'certificats'

def _demanar_data(self, cr, uid, data, context={}):
    return {'revisions': data['revisions_sd']}

_demanar_data_form = """<?xml version="1.0" encoding="utf-8"?>
<form string="Data Certificat/Acta" col="2">
  <field name="data" />
  <label string="" colspan="2" />
  <label string="Les següents revisions no tenen data de certificació" colspan="2" />
  <field name="revisions" widget="one2many" nolabel="1" colspan="2"/>
</form>"""

_demanar_data_fields = {
 'data': {'string': 'Data Certificat/Acta', 'type': 'date', 'required': True},
 'revisions': {'string': 'Revisions', 'type':'one2many', 'relation': 'giscedata.revisions.at.revisio', 'readonly': True},
}

_show_popup_form = """<?xml version="1.0"?>
<form string="Avís">
  <image name="gtk-dialog-info" colspan="1" />
  <label string="No hi ha cap acta ni cap certificat disponible." colspan="3" />
</form>"""

_show_popup_fields = {}

def _guardar_data(self, cr, uid, data, context={}):
    pooler.get_pool(cr.dbname).get('giscedata.revisions.at.revisio').write(cr, uid, data['revisions_sd'], {'data_ca': data['form']['data']})
    for r in data['revisions_sd']:
        data['revisions'].append(r)
    return {}

def _certificats(self, cr, uid, data, context={}):
    ids = []
    flag = True
    for revisio in pooler.get_pool(cr.dbname).get('giscedata.revisions.at.revisio').browse(cr, uid, data['revisions']):
        # Si no té defectes passa directament als certificables
        if len(revisio.defectes_ids) == 0:
            ids.append(revisio.id)
        # Si te defectes però tots son interns també ha de passar
        # a ser certificable
        else:
            for d in revisio.defectes_ids:
                flag &= d.intern
            if flag:
                ids.append(revisio.id)

    if len(ids):
        data['certificats'] = ids
    else:
        data['certificats'] = []
    return {}

def _certificats_empresa(self, cr, uid, data, context={}):
    if (data['form']['versions'] == 'totes' or data['form']['versions'] ==\
    'empresa') and len(data['certificats']):
        return 'print_certificats_empresa'
    else:
        return 'certificats_eic'

def _certificats_eic(self, cr, uid, data, context={}):
    if (data['form']['versions'] == 'totes' or data['form']['versions'] ==\
    'eic') and len(data['certificats']):
        return 'print_certificats_eic'
    else:
        return 'actes'


def _print_certificats(self, cr, uid, data, context={}):
    return {'ids': data['certificats']}

def _actes(self, cr, uid, data, context={}):
    ids = []
    for revisio in pooler.get_pool(cr.dbname).get('giscedata.revisions.at.revisio').browse(cr, uid, data['revisions']):
        # En que trobem un defecte que no sigui intern i no estigui
        # reparat ja tindrem una acta
        for defecte in revisio.defectes_ids:
            # Si el defecte no es intern i no esta reparat
            if not defecte.intern and defecte.reparat == False:
                ids.append(revisio.id)
                pass
    if len(ids):
        data['actes'] = ids
    else:
        data['actes'] = []
    return  {}

def _actes_empresa(self, cr, uid, data, context={}):
    if (data['form']['versions'] == 'totes' or data['form']['versions'] ==\
    'empresa') and len(data['actes']):
        return 'print_actes_empresa'
    else:
        return 'actes_eic'

def _actes_eic(self, cr, uid, data, context={}):
    if (data['form']['versions'] == 'totes' or data['form']['versions'] ==\
    'eic') and len(data['actes']):
        return 'print_actes_eic'
    else:
        return 'popup'

def _print_actes(self, cr, uid, data, context={}):
    return {'ids': data['actes']}


def _popup(self, cr, uid, data, context={}):
    if not len(data['actes']) and not len(data['certificats']):
        return 'show_popup'
    else:
        return 'end'


class wizard_certificats_actes(wizard.interface):

    states = {
      'init': {
          'actions': [],
        'result': {'type': 'form', 'arch': _avis_form,'fields': _avis_fields, 'state':[('end', 'Cancelar', 'gtk-cancel'), ('escollir_data', 'Continuar', 'gtk-go-forward')]}
      },
      'escollir_data': {
          'actions': [],
          'result': {'type':'choice', 'next_state':_escollir_data}
      },
      'demanar_data': {
        'actions':[_demanar_data],
        'result': {'type': 'form', 'arch': _demanar_data_form, 'fields': _demanar_data_fields, 'state':[('end', 'Cancelar', 'gtk-cancel'), ('guardar_data', 'Continuar', 'gtk-go-forward')]}
      },
      'guardar_data': {
        'actions':[_guardar_data],
        'result': {'type': 'state', 'state': 'certificats'}
      },
      'certificats': {
        'actions': [_certificats],
        'result': {'type': 'state', 'state': 'certificats_empresa'}
      },
      'certificats_empresa': {
          'actions': [],
          'result': {'type':'choice', 'next_state':_certificats_empresa}
      },
      'print_certificats_empresa': {
        'actions': [_print_certificats],
        'result': {'type': 'print', 'report':
        'giscedata.revisions.at.revisio.report3', 'get_id_from_action':True, \
        'state':'certificats_eic'}
      },
      'certificats_eic': {
        'actions': [],
        'result': {'type': 'choice', 'next_state': _certificats_eic}
      },
      'print_certificats_eic': {
        'actions': [_print_certificats],
        'result': {'type': 'print', 'report':\
        'giscedata.revisions.at.revisio.report5', 'get_id_from_action':True, \
        'state':'actes'}
      },
      'actes': {
        'actions': [_actes],
        'result': {'type': 'state', 'state': 'actes_empresa'}
      },
      'actes_empresa': {
        'actions': [],
        'result': {'type': 'choice', 'next_state': _actes_empresa}
      },
      'print_actes_empresa': {
        'actions': [_print_actes],
        'result': {'type': 'print', 'report':
        'giscedata.revisions.at.revisio.report2', 'get_id_from_action':True,
        'state':'actes_eic'}
      },
      'actes_eic': {
        'actions': [],
        'result': {'type': 'choice', 'next_state': _actes_eic}
      },
      'print_actes_eic': {
        'actions': [_print_actes],
        'result': {'type': 'print', 'report': \
        'giscedata.revisions.at.revisio.report4', 'get_id_from_action':True, 'state':'popup'}
      },
      'popup': {
        'actions': [],
        'result': {'type': 'choice', 'next_state': _popup}
      ,},
      'show_popup': {
        'actions': [],
        'result': {'type': 'form', 'arch': _show_popup_form,'fields': _show_popup_fields, 'state':[('end', 'D\'acord', 'gtk-ok')]}
      },
      'end': {
        'actions': [],
        'result': {'type':'state', 'state': 'end'}
      },
    }

wizard_certificats_actes('giscedata.revisions.at.certificats_actes')
