# -*- coding: iso-8859-1 -*-
import wizard
import pooler
from datetime import *


_avis_form = """<?xml version="1.0"?>
<form string="Avís">
  <label string="Amb aquest assistent afegiràs un ct a una revisió." />
</form>"""

_avis_fields = {}

def _get_trienis(self, cr, uid, context={}):
    """
    obj = pooler.get_pool(cr.dbname).get('giscedata.trieni')
    ids = obj.search(cr, uid, [('anyy', '=', datetime.now().year)])
    res = obj.read(cr, uid, ids, ['name', 'id'], context)
    """
    cr.execute("select id,name from giscedata_trieni where trieni = (select trieni from giscedata_trieni where anyy = %s limit 1)" % datetime.now().year)
    res = cr.dictfetchall()
    res = [(r['id'], r['name']) for r in res]
    return res

_assignar_form = """<?xml version="1.0"?>
<form string="Assignar a una revisió">
  <field name="revisio" />
</form>"""

_assignar_fields = {
  'revisio': {'string': "Revisió", 'type': 'selection', 'selection': _get_trienis},
}

def _init(self, cr, uid, data, context={}):
    return {}

def _guardar(self, cr, uid, data, context={}):
    vals = {}
    vals['name'] = data['id']
    vals['trimestre'] = data['form']['revisio']
    rev = pooler.get_pool(cr.dbname).get('giscedata.revisions.ct.revisio')
    newid = rev.create(cr, uid, vals)
    return {}

class wizard_ct_revisio(wizard.interface):

    states = {
      'init': {
          'actions': [_init],
        'result': {'type': 'form', 'arch': _avis_form,'fields': _avis_fields, 'state':[('end', 'Cancelar', 'gtk-cancel'), ('assignar', 'Continuar', 'gtk-go-forward')]}
      },
      'assignar': {
        'actions': [],
        'result': {'type': 'form', 'arch': _assignar_form,'fields': _assignar_fields, 'state':[('end', 'Cancelar', 'gtk-cancel'), ('guardar', 'Continuar', 'gtk-go-forward')]}
      },
      'guardar': {
        'actions': [_guardar],
        'result': {'type': 'state', 'state': 'end'}
      },
    }

wizard_ct_revisio('giscedata.revisions.ct.arevisar')
