# -*- coding: utf-8 -*-

from osv import osv, fields
from types import *
from datetime import *

import pooler
import math
from psycopg2.extensions import AsIs


AUTOCOMPLETE_NORMATIVE_2 = {
    'default': 'NA',
    '1.1': {'default': 'C'},
    '1.2': {'default': 'C'},
    '1.3': {'default': 'C'},
    '2.1': {
        'no_seccionador': 'NA',
        'autovalvules': 'C',
        'seccionador': 'C',
        'ct': 'NC'
    },
    '2.2': {
        'seccionador': 'C',
        'autovalvules': 'C',
        'ct': 'NC',
        'poste': {'PFU': 'NA', 'PFO': 'NA', 'PM': 'C'}
    },
    '2.3': {
        'seccionador': 'C',
        'autovalvules': 'C',
        'ct': 'NC',
        'poste': {'PFU': 'NA', 'PFO': 'NA', 'PM': 'C'}
    },
    '3.1': {'default': 'C'},
    '4.1': {
        'ct': 'NC',
        'poste': {'PFU': 'C', 'PFO': 'NA', 'PM': 'NA'}
    },
    '4.2': {
        'ct': 'NC',
        'poste': {'PFU': 'NA', 'PFO': 'C', 'PM': 'NA'}
    },
    '4.3': {
        'ct': 'NC',
        'poste': {'PFU': 'NA', 'PFO': 'NA', 'PM': 'C'}
    },
    '4.4': {
        'ct': 'NC',
        'poste': {'PFU': 'NA', 'PFO': 'NA', 'PM': 'C'}
    },
    '4.5': {
        'default': 'C',
        'ct': 'NC',
    },
    '4.6': {
        'default': 'C',
        'ct': 'NC',
    },
    '5.1': {
        'default': 'NA',
        'ct': 'NC'
    },
    '5.2': {
        'default': 'NA',
        'ct': 'NC'
    },
    '5.3': {
        'default': 'NA',
        'ct': 'NC'
    },
    '6.1': {
        'default': 'C',
        'ct': 'NC'
    },
    '6.2': {
        'default': 'C',
        'ct': 'NC'
    },
    '7.1': {
        'default': 'C',
    },
    '7.2': {
        'default': 'NA',
    },
    '7.3': {
        'creua_carretera': 'C',
        'creua_linia': 'NA',
        'no_creuament': 'NA'
    },
    '7.4': {
        'default': 'NA',
    },
    '7.5': {
        'creua_linia': 'C',
        'creua_carretera': 'NA',
        'no_creuament': 'NA'
    },
    '7.6': {
        'creua_linia': 'C',
        'creua_carretera': 'NA',
        'no_creuament': 'NA'
    },
    '7.7': {
        'creua_linia': 'C',
        'creua_carretera': 'C',
        'no_creuament': 'NA'
    },
    '8.1': {
        'default': 'C',
    },
    '8.2': {
        'default': 'C',
    },
    '8.3': {
        'default': 'NA',
        'pas_per_zones': 'C'
    },
    '9.1': {
        'no_seccionador': 'NA',
        'ct': 'NC',
        'seccionador': 'C'
    }
}

AUTOCOMPLETE_NORMATIVE_2_PEUSA = {
    '2.3a': {
        'seccionador': 'C',
        'autovalvules': 'C',
        'ct': 'NC',
        'poste': {'PFU': 'NA', 'PFO': 'NA', 'PM': 'C'}
    },
    '2.3b': {
        'seccionador': 'C',
        'autovalvules': 'C',
        'ct': 'NC',
        'poste': {'PFU': 'NA', 'PFO': 'NA', 'PM': 'C'}
    },
    '4.7a': {
        'seccionador': 'C',
        'default': 'NA',
        'ct': 'NC'
    },
    '4.7b': {
        'default': 'C',
        'ct': 'NC'
    },
    '4.7c': {
        'ct': 'NC',
        'autovalvules': 'C',
        'default': 'NA'
    },
    '4.7d': {
        'default': 'NC',
        'poste': {'PFU': 'NA', 'PFO': 'NA', 'PM': 'C'},
        'ct': 'NC'
    },
    '4.7e': {
        'default': 'NC',
        'poste': {'PFU': 'C', 'PFO': 'NA', 'PM': 'NA'},
        'ct': 'NC'
    },
    '4.7f': {
        'default': 'C',
        'ct': 'NC'
    }
}

INTERVAL_MAP = {
    'L': 'A corregir a termini',
    'G': 'A corregir a termini',
    'MG': 'De correcció immediata',
}

class giscedata_revisions_tecnic(osv.osv):

    _name = "giscedata.revisions.tecnic"
    _description = "Tecnics de revisions"

    _columns = {
      'name': fields.char('Nom', size=100),
      'titolacio': fields.char('Titolació', size=100),
      'n_collegiat': fields.char('Nº de col·legiat', size=60),
      'dni': fields.char('DNI', size=60),
      'domicili': fields.char('Domicili', size=255),
      'municipi': fields.many2one('res.municipi',
                                  'Municipi'),
      'telefon': fields.char('Telèfon', size=60),
      'collegi': fields.char('Col·legi', size=60),
      'email': fields.char('Correu electrònic', size=100),
      'organisme': fields.char('Organisme', size=60),
      'active': fields.boolean('Actiu', required=False, readonly=False),
    }

    _defaults = {
      'active': lambda *a: 1,
    }

    _order = "name, id"

giscedata_revisions_tecnic()


class giscedata_revisions_equips(osv.osv):

    _name = 'giscedata.revisions.equips'
    _description = 'Equips utilitzats en les revisions'

    _columns = {
      'codi': fields.char('Codi', size=10),
      'descripcio': fields.char('Nom', size=255),
      'active': fields.boolean('Active'),
      'data_alta': fields.date("Data d'alta"),
      'data_baixa': fields.date("Data de baixa"),
    }

    _order = 'codi asc'

    # Funció name_get() per tal que retorni 'Codi - Descripcio'

    def name_get(self, cr, uid, ids, context={}):
        if not len(ids):
            return []
        res = []
        for equip in self.browse(cr, uid, ids):
            name = '%s - %s' % (equip.codi,  equip.descripcio)
            res.append((equip.id, name))
        return res

giscedata_revisions_equips()


class GiscedataRevisionsCtReglaments(osv.osv):
    _name = 'giscedata.revisions.ct.reglaments'

    _columns = {
        'name': fields.char('Nom del reglament', size=50),
        'data_ini': fields.date('Data inici', required=True),
        'data_fi': fields.date('Data final'),
        'active': fields.boolean('Actiu')
    }

GiscedataRevisionsCtReglaments()


class giscedata_revisions_ct_revisio(osv.osv):
    def assignar(self, cr, uid, ids, args):
        for revisio in self.browse(cr, uid, ids):
            if not revisio.data or not revisio.tecnic.id:
                raise osv.except_osv('Error !', 'Has d\'assignar un tècnic i una data.')
                return False
            else:
                normativa = self.pool.get('giscedata.revisions.ct.normativa').search(cr, uid, [('vigent', '=', True)])[0]
                ct = self.pool.get('giscedata.cts').browse(cr, uid, revisio.name.id)

                existents = []
                for defecte in revisio.defectes_ids:
                    existents.append(defecte.descripcio_id.id)

                for tipusdefecte in \
                self.pool.get('giscedata.revisions.ct.tipusdefectes').search(cr, \
                uid,[('tipus_ct', '=', ct.id_subtipus.tipus_id.id), ('normativa_id', \
                '=', normativa)]):
                    if tipusdefecte not in existents:
                        vals = {'name': str(tipusdefecte), 'revisio_id': revisio.id, 'descripcio_id': tipusdefecte}
                        self.pool.get('giscedata.revisions.ct.defectes').create(cr, uid, vals)
                self.write(cr, uid, [revisio.id], {'state': 'assignat', 'intensitat_defecte': revisio.name.intensitat_defecte, 'temps_disparo': revisio.name.temps_disparo, 'tensio_maxima': revisio.name.tensio_maxima})
                return True

    def desassignar(self, cr, uid, ids, args):
        for revisio in self.browse(cr, uid, ids):
            self.write(cr, uid, [revisio.id], {'state': 'sense assignar'})
        return True

    def tancar(self, cr, uid, ids, args):
        for revisio in self.browse(cr, uid, ids):
            self.write(cr, uid, [revisio.id], {'state': 'tancada'})
        return True

    def reobrir(self, cr, uid, ids, args):
        for revisio in self.browse(cr, uid, ids):
            self.write(cr, uid, [revisio.id], {'state': 'reoberta'})
        return True

    def _nom_ct(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for defecte in self.browse(cr, uid, ids):
            res[defecte.id] = defecte.name.descripcio
        return res

    def _nom_ct_search(self, cr, uid, obj, name, args, context=None):
        if not len(args):
            return []
        else:
            s_ids = self.pool.get('giscedata.cts').search(cr, uid,\
              [('descripcio', args[0][1], args[0][2])])
            if not len(s_ids):
                return [('id','=','0')]
            else:
                ids = []
                for ct in self.pool.get('giscedata.cts').browse(cr, uid, s_ids):
                    for r in ct.revisions:
                        ids.append(r.id)
                return [('id', 'in', ids)]

    def _tots_reparats(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for revisio in self.browse(cr, uid, ids):
            if revisio.state == 'tancada':
                if len(revisio.defectes_ids) == 0:
                    res[revisio.id] = {'tots_reparats': True,
                                       'num_tots_reparats': '0/0',
                                       'ind_reparats': True,
                                       'num_ind_reparats': '0/0',
                                      }
                else:
                    reparats = []
                    incorrectes = []
                    reparats_ind = []
                    incorrectes_ind = []
                    for defecte in revisio.defectes_ids:
                        reparats.append(defecte.reparat and \
                                        defecte.estat == 'B')
                        incorrectes.append(defecte.estat == 'B')
                        if not defecte.intern:
                            reparats_ind.append(defecte.reparat and \
                                                defecte.estat == 'B')
                            incorrectes_ind.append(defecte.estat == 'B')
                    res[revisio.id] = {'tots_reparats':
                                            (reparats.count(True) ==
                                             incorrectes.count(True)),
                                       'num_tots_reparats': '%s/%s' % \
                                        (reparats.count(True),
                                         incorrectes.count(True)),
                                       'ind_reparats':
                                            (reparats_ind.count(True) ==
                                             incorrectes_ind.count(True)),
                                       'num_ind_reparats': '%s/%s' % \
                                        (reparats_ind.count(True),
                                         incorrectes_ind.count(True))}
            else:
                res[revisio.id] = {'tots_reparats': True,
                                   'num_tots_reparats': '0/0',
                                   'ind_reparats': True,
                                   'num_ind_reparats': '0/0',
                                  }
        return res

    def _certificable(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for revisio in self.browse(cr, uid, ids):
            tots_reparats = []
            if len(revisio.defectes_ids):
                for defecte in revisio.defectes_ids:
                    if defecte.estat == 'B' and not defecte.intern and defecte.reparat:
                        tots_reparats.append(True)
                    elif defecte.estat == 'B' and not defecte.intern and not defecte.reparat:
                        tots_reparats.append(False)
                        pass
                    else:
                        tots_reparats.append(True)
                if False in tots_reparats:
                    res[revisio.id] = False
                else:
                    res[revisio.id] = True
            else:
                res[revisio.id] = False
        return res

    def _tots_reparats_search(self, cr, uid, obj, name, args, context=None):
        #TODO: Substituir per una query SQL potent
        if not len(args):
            return []
        else:
            res = []
            for revisio in self.browse(cr, uid, self.search(cr, uid, [('state','=','tancada')])):
                if revisio.tots_reparats == bool(args[0][2]):
                    res.append(revisio.id)
            if not len(res):
                return [('id','=','0')]
            return [('id','in',res)]

    def _ind_reparats_search(self, cr, uid, obj, name, args, context=None):
        #TODO: Substituir per una query SQL potent
        if not len(args):
            return []
        else:
            res = []
            for revisio in self.browse(cr, uid, self.search(cr, uid, [('state','=','tancada')])):
                if revisio.ind_reparats == bool(args[0][2]):
                    res.append(revisio.id)
            if not len(res):
                return [('id','=','0')]
            return [('id','in',res)]

    def _data_comprovacio(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for id in ids:
            cr.execute("select data_comprovacio from giscedata_revisions_ct_defectes \
            where revisio_id = %s and data_comprovacio is not null order by data_comprovacio desc limit 1", (id,))
            data = cr.fetchone()
            if data and len(data):
                res[id] = data[0]
            else:
                res[id] = ''
        return res

    def _municipi(self, cr, uid, ids, field_name, arg, context):
        # Fake function, nomes ens interessa buscar
        res = {}
        for i in ids:
            res[i] = ''
        return res

    def _codi(self, cursor, uid, ids, field_name, arg, context):
        res = {}
        cols = ['id', 'name.name', 'trimestre.name']
        dmn = [('id', 'in', ids)]
        query = self.q(cursor, uid).select(cols).where(dmn)
        cursor.execute(*query)
        for v in cursor.dictfetchall():
            res[v['id']] = "{}/{}".format(v['name.name'], v['trimestre.name'])
        return res

    def _municipi_search(self, cr, uid, obj, name, args, context=None):
        if not len(args):
            return []
        else:
            cr.execute("""select r.id from giscedata_revisions_ct_revisio r, giscedata_cts ct, res_municipi m where r.name = ct.id and ct.id_municipi = m.id and m.name ilike %s""", ('%%%s%%' % args[0][2],))
            r_ids = [a[0] for a in cr.fetchall()]
            if not len(r_ids):
                return [('id','=','0')]
            else:
                return [('id', 'in', r_ids)]

    def _mesura_ct(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        res = dict.fromkeys(ids, False)
        m_obj = self.pool.get('giscedata.cts.mesures')
        for rev_id in ids:
            mesures = m_obj.search(cursor, uid, [
                ('id_revisio', '=', rev_id)
            ], order="name desc", limit=1)
            if mesures:
                last_mesura = mesures[0]
                res[rev_id] = (
                    last_mesura,
                    m_obj.read(cursor, uid, last_mesura, ['name'])['name']
                )
        return res

    def name_get(self, cr, uid, ids, context={}):
        if not len(ids):
            return []
        res = []
        for revisio in self.browse(cr, uid, ids, context):
            name = '%s - %s' % (revisio.name.name, revisio.trimestre.name)
            res.append((revisio.id, name))
        return res

    def name_search(self, cursor, user, name='', args=None, operator='ilike', context=None, limit=80):
        """
        Search function for the model

        :param cursor: Database cursor
        :param user: User id
        :type user: int
        :param name: Text to search
        :type name: str
        :param args:
        :param operator: Operator to apply on the search
        :type operator: str
        :param context: OpenERP context
        :type context: dict
        :param limit: Max number of results
        :type limit: int
        :return: List of the found ids
        :rtype: list[(int,str)]
        """

        sql = """
            SELECT ct_rev.id
                FROM giscedata_revisions_ct_revisio AS ct_rev
                LEFT JOIN giscedata_cts AS ct ON (ct_rev.name=ct.id)
                LEFT JOIN giscedata_trieni AS tri ON (ct_rev.trimestre = tri.id)
              WHERE ct.name::text||' - '||tri.name ILIKE %(name)s
              LIMIT %(limit)s;  
              
        """
        sql_params = {
            "name": "%"+name+"%",
            "limit": limit
        }
        cursor.execute(sql, sql_params)
        data = cursor.fetchall()
        return self.name_get(cursor, user, [d[0] for d in data])

    def create(self, cursor, uid, vals, context=None):
        if not context:
            context = {}
        id_ct = vals.get('name', False)
        if id_ct:
            cts_obj = self.pool.get('giscedata.cts')
            data_pm_ct = cts_obj.read(
                cursor, uid, id_ct, ['data_pm'])['data_pm']
            if data_pm_ct:
                regl_obj = self.pool.get('giscedata.revisions.ct.reglaments')
                regl_ids = regl_obj.search(
                    cursor, uid, [
                        ('data_ini', '<=', data_pm_ct),
                        '|',
                        ('data_fi', '=', False),
                        ('data_fi', '>=', data_pm_ct)
                    ], context={'active_test': False}
                )
                vals['reglaments'] = [(6, False, regl_ids)]

        res_id = super(giscedata_revisions_ct_revisio,
                       self).create(cursor, uid, vals, context)
        return res_id

    _name = "giscedata.revisions.ct.revisio"
    _description = "CTs a revisar"

    _columns = {
        'name': fields.many2one(
            'giscedata.cts', 'Centre Transformador',
            required=True, readonly=False,
            states={
                'assignat': [('readonly', True)],
                'tancada': [('readonly', True)],
                'reoberta': [('readonly', True)]
            }
        ),
        'nom_ct': fields.function(
            _nom_ct, fnct_search=_nom_ct_search, method=True,
            type='char', string="Nom del CT"
        ),
        'mesura_ct_id': fields.function(
            _mesura_ct, 'Mesura CT', type='many2one',
            relation='giscedata.cts.mesures', method=True),
        'trimestre': fields.many2one(
            'giscedata.trieni', 'Trimestre',
            required=True, readonly=False, states={
                'assignat': [('readonly', True)],
                'tancada': [('readonly', True)],
                'reoberta': [('readonly', True)]
            }
        ),
        'tecnic': fields.many2one(
            'giscedata.revisions.tecnic', 'Tècnic',
            readonly=False, states={
                'assignat': [('readonly', True)],
                'tancada': [('readonly', True)],
                'reoberta': [('readonly', True)]
            }
        ),
        'operari': fields.many2one(
            'res.partner.address', 'Operari',
            readonly=False, states={
                'assignat': [('readonly', True)],
                'tancada': [('readonly', True)],
                'reoberta': [('readonly', True)]
            }
        ),
        'data': fields.datetime(
            'Data Inici', readonly=False, states={
                'tancada': [('readonly', True)]
            }
        ),
        'data_fi': fields.datetime(
            'Data Final', readonly=False, states={
                'tancada': [('readonly', True)]
            }
        ),
        'reglaments': fields.many2many(
            'giscedata.revisions.ct.reglaments',
            'revisio_reglaments_rel', 'revisio_id', 'reglament_id',
            'Reglaments'),
        'observacions': fields.text('Observacions'),
        'data_ca': fields.datetime('Data Certificat Acta'),
        'data_ca2': fields.datetime('Data Certificat Reparació'),
        'mesures': fields.boolean('Mesures'),
        'talar': fields.char('Trimestre de Tala', size=60),
        'trieni': fields.integer('Trieni'),
        'state': fields.char('Estat', size=60, readonly=True),
        'defectes_ids': fields.one2many(
            'giscedata.revisions.ct.defectes', 'revisio_id', 'Defectes',
            states={
                'sense assignar': [('readonly', True)],
                'tancada': [('readonly', True)]
            }
        ),
        'tots_reparats': fields.function(
            _tots_reparats, method=True, type='boolean',
            string='Defectes reparats',
            fnct_search=_tots_reparats_search, multi='defecte'
        ),
        'num_tots_reparats': fields.function(
            _tots_reparats, method=True, type='char',
            string='Reparats/Totals', multi='defecte'
        ),
        'ind_reparats': fields.function(
            _tots_reparats, method=True, type='boolean',
            string='Defectes Indústria reparats',
            fnct_search=_ind_reparats_search, multi='defecte'
        ),
        'num_ind_reparats': fields.function(
            _tots_reparats, method=True, type='char',
            string='Reparats/Totals Indústria', multi='defecte'
        ),
        'certificable': fields.function(
            _certificable, method=True, type='boolean',
            string='Certificable', fnct_search=_tots_reparats_search
        ),
        'intensitat_prova': fields.float('Intensitat de prova (A)'),
        'intensitat_defecte': fields.float(
            'Intensitat defecte real (A)', readonly=True
        ),
        'temps_disparo': fields.float(
            'Temps de Disparo Protecció', readonly=True
        ),
        'tensio_maxima': fields.float(
            'Tensió Màxima Admissible Id (V)', readonly=True
        ),
        'observacions_mesura': fields.text('Observacions'),
        'proves_mesura': fields.one2many(
            'giscedata.revisions.ct.revisio.proves', 'revisio', 'Proves'
        ),
        'equips_utilitzats': fields.many2many(
            'giscedata.revisions.equips', 'giscedata_revisions_cts_equips',
            'revisio_id', 'equip_id', 'Equips'
        ),
        'croquis_mesura': fields.binary('Croquis'),
        'equips_pic': fields.many2many(
            'giscedata.revisions.equips', 'giscedata_revisions_cts_equips_pic',
            'revisio_id', 'equip_id', 'Equips P.I.C'
        ),
        'data_comprovacio': fields.function(
            _data_comprovacio, method=True,
            string="Data comprovació reparació de defectes",
            type='char'
        ),
        'municipi': fields.function(
            _municipi, method=True, fnct_search=_municipi_search,
            string="Municipi", type='char', size=256, select=True
        ),
        'codi': fields.function(
            _codi, method=True, string="Codi inspecció", type='char', size=64
        )
    }

    _defaults = {
      'state': lambda *a: 'sense assignar',
    }

    _sql_constraints = [(
        'ct_trim', 'unique (name, trimestre)',
        'No es pot revisar una ct més d\'una vegada al mateix trimestre.'
    )]

    _order = "name, id"

giscedata_revisions_ct_revisio()


class giscedata_revisions_ct_revisio_proves_situacio(osv.osv):

    _name = 'giscedata.revisions.ct.revisio.proves.situacio'
    _description = 'Diferents situacions d\'una prova de mesura'

    _columns = {
      'name': fields.char('Situació', size=50, required=True),
    }

giscedata_revisions_ct_revisio_proves_situacio()

class giscedata_revisions_ct_revisio_proves(osv.osv):

    _name = 'giscedata.revisions.ct.revisio.proves'
    _description = 'Prova de mesura'

    def _situacions_selection(self, cr, uid, context={}):
        obj = self.pool.get('giscedata.revisions.ct.revisio.proves.situacio')
        ids = obj.search(cr, uid, [])
        res = obj.read(cr, uid, ids, ['name', 'id'], context)
        res = [(r['id'], r['name']) for r in res]
        return res

    def _v(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for p in self.browse(cr, uid, ids):
            try:
                res[p.id] = math.sqrt(((math.pow(p.vm1, 2) + math.pow(p.vm2, 2)) / 2) - math.pow(p.v0, 2))
            except ValueError:
                res[p.id] = 0
        return res

    def _v_def(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for p in self.browse(cr, uid, ids):
            if p.revisio.intensitat_prova > 0:
                v_def = (p.v * p.revisio.intensitat_defecte) / p.revisio.intensitat_prova
                res[p.id] = v_def / 1000 # Ho passem a V
            else:
                res[p.id] = 0.0
        return res

    _columns = {
      'name': fields.many2one('giscedata.revisions.ct.revisio.proves.situacio', 'Situació'),
      'mesura': fields.selection([('C', 'Contacte'), ('PI', 'Pas Interior'), ('PE', 'Pas Exteriror')], 'Mesura'),
      'terra': fields.selection([('G', 'Grava'), ('M', 'Metall'), ('C', 'Ciment'), ('A', 'Asfalt'), ('H', 'Herba'), ('R', 'Rajola'), ('T', 'Terra'), ('GO', 'Goma')], 'Terra'),
      'v0': fields.float('Vo (mV)', digits=(15,3)),
      'vm1': fields.float('V+ (mV)', digits=(15,3)),
      'vm2': fields.float('V- (mV)', digits=(15,3)),
      'v': fields.function(_v, type='float', method=True, string='V (mV)', digits=(15,3)),
      'v_def': fields.function(_v_def, type='float', method=True, string='Tensió per I defecte (V)', digits=(15,3)),
      'revisio': fields.many2one('giscedata.revisions.ct.revisio', 'Revisió'),
    }

    _order = 'id asc'


giscedata_revisions_ct_revisio_proves()

class giscedata_revisions_ct_tipusvaloracio(osv.osv):

    _name = "giscedata.revisions.ct.tipusvaloracio"
    _description = "Tipus de valoració"

    _columns = {
      'name': fields.char('Valoració', size=60),
      'active': fields.boolean('Active'),
      't_qty': fields.integer('Temps'),
      't_type': fields.selection([('day', 'Dia'),('month', 'Mes'), ('year', 'Any')], 'Unitat')
    }

    _defaults = {
      'active': lambda *a: True,
      't_type': lambda *a: 'day',
    }


giscedata_revisions_ct_tipusvaloracio()


class giscedata_revisions_ct_normativa(osv.osv):

    _name = "giscedata.revisions.ct.normativa"
    _description = "Normativa"

    _columns = {
      'name': fields.char('Nom de la normativa', size=255),
      'inici': fields.date('Inici'),
      'final': fields.date('Final'),
      'vigent': fields.boolean('Vigent', readonly=True),
    }

    _defaults = {

    }

    _order = "name, id"

giscedata_revisions_ct_normativa()


class giscedata_revisions_ct_tipusdefectes(osv.osv):

    _name = "giscedata.revisions.ct.tipusdefectes"
    _description = "Tipus de defecte i a quin tipus de ct s'aplica"

    _columns = {
      'name': fields.char('Tipus', size=25),
      'tipus_ct': fields.many2one('giscedata.cts.tipus', 'Tipus de ct'),
      'codi': fields.char('Codi', size=25),
      'normativa_id': fields.many2one('giscedata.revisions.ct.normativa', 'Normativa'),
      'ordre': fields.integer('Ordre'),
      'actiu': fields.boolean('Actiu'),
      'descripcio': fields.text('Descripció'),
      'intern': fields.boolean('Intern'),
    }

    _defaults = {

    }

    _order = "ordre, codi, name"

giscedata_revisions_ct_tipusdefectes()


class giscedata_revisions_ct_defectes(osv.osv):

    def _ordre(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for defecte in self.browse(cr, uid, ids):
            if defecte.descripcio_id:
                res[defecte.id] = defecte.descripcio_id.ordre
            else:
                res[defecte.id] = 0
        return res

    def _descripcio(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for defecte in self.browse(cr, uid, ids):
            if defecte.descripcio_id:
                res[defecte.id] = defecte.descripcio_id.descripcio
            else:
                res[defecte.id] = ''
        return res

    def _codi(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for defecte in self.browse(cr, uid, ids):
            if defecte.descripcio_id:
                res[defecte.id] = defecte.descripcio_id.codi
            else:
                res[defecte.id] = ''
        return res

    def _data_limit_reparacio(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for defecte in self.browse(cr, uid, ids):
            if defecte.valoracio:
                cr.execute("SELECT r.data + INTERVAL '%s %s' AS data_limit_reparacio FROM giscedata_revisions_ct_revisio r, giscedata_revisions_ct_defectes d WHERE r.id = d.revisio_id AND d.id = %s" % (defecte.valoracio.t_qty, defecte.valoracio.t_type, defecte.id))
                res[defecte.id] = cr.fetchone()[0]
            else:
                res[defecte.id] = ''
        return res

    def _intern(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for defecte in self.browse(cr, uid, ids):
            if defecte.descripcio_id:
                res[defecte.id] = defecte.descripcio_id.intern
            else:
                res[defecte.id] = 0
        return res

    def _search_by_data_lim(self, cr, uid, obj, name, args, context=None):
        """
        Function used to search by the function field "data limit" of the model.
        :param cr: Database cursor
        :type cr: DBCursor
        :param uid: Not Used
        :param obj: Not Used
        :param name: Not Used
        :param args: Search filter parameters
        :type args: List of (any,str,any)
        :param context: Not Used
        :return: Search filter of the ids selected by the search filter params
        :rtype List of (str,str, List of int)
        """

        search_date = args[0][2]
        search_operator = args[0][1]
        query = """
        SELECT d.id AS id_defecte
        FROM  giscedata_revisions_ct_revisio r, 
              giscedata_revisions_ct_defectes d,
              giscedata_revisions_ct_tipusvaloracio v
        WHERE r.id = d.revisio_id
        AND d.valoracio = v.id
        AND (r.data + (v.t_qty::text || ' ' || v.t_type)::interval) %s %s
        """

        cr.execute(query, (AsIs(search_operator), search_date))
        ids_res = cr.fetchall()
        return [('id', 'in', ids_res)]

    def _search_by_codi(self, cr, uid, obj, name, args, context=None):
        '''
        Search function that returns the defectes ids according to the codi
        field.
        :param cr: Database Cursor
        :type cr: DBCursor
        :param uid: User identifier
        :param obj: Not Used
        :param name: Not Used
        :param args: Search filter parameters
        :type args: list[(any,str,any)]
        :param context: Not Used
        :return: filtered ids
        :rtype: list[(str,str,list[int])]
        '''

        search_codi = args[0][2]
        query = """
        SELECT d.id AS id_defectes
        FROM giscedata_revisions_ct_defectes d
        LEFT JOIN giscedata_revisions_ct_tipusdefectes td
        ON td.id = d.descripcio_id
        WHERE td.codi ILIKE %s
        """

        cr.execute(query, ('%'+search_codi+'%',))
        ids_res = cr.fetchall()

        return [('id', 'in', ids_res)]


    _name = "giscedata.revisions.ct.defectes"
    _description = "Defectes d'una CT"

    _columns = {
        'name': fields.char('Codi', size=60),
        'estat': fields.selection([('A', 'Correcte'), ('B', 'Incorrecte'), ('C', 'No comprovat'), ('D', 'No aplicable')], 'Estat'),
        'observacions': fields.text('Observacions'),
        'observacions_rep': fields.text('Observacions de reparació'),
        'data_reparacio': fields.datetime('Data de reparació'),
        'data_comprovacio': fields.datetime('Data de comprovació'),
        'valoracio': fields.many2one('giscedata.revisions.ct.tipusvaloracio', 'Valoració'),
        'reparat': fields.boolean('Reparat'),
        'revisio_id': fields.many2one('giscedata.revisions.ct.revisio', 'Revisió'),
        'intern': fields.function(_intern, method=True, string="Intern", type="boolean"),
        'descripcio_id': fields.many2one('giscedata.revisions.ct.tipusdefectes', 'Descripció'),
        'ordre': fields.function(_ordre, method=True, string="Ordre", type="integer"),
        'codi': fields.function(
            _codi,
            method=True,
            string="Codi",
            type="char",
            fnct_search=_search_by_codi
        ),
        'descripcio': fields.function(_descripcio, method=True, string="Descripció", type="char"),
        'data_limit_reparacio': fields.function(_data_limit_reparacio, method=True, string="Data límit de reparació", type="char", size=60),
    }

    _defaults = {
      'estat': lambda * a: 'C',
    }

    _order = "descripcio_id"

giscedata_revisions_ct_defectes()



class giscedata_at_linia(osv.osv):
    _name = "giscedata.at.linia"
    _inherit = "giscedata.at.linia"
    _columns = {
                  'revisions': fields.one2many('giscedata.revisions.at.revisio', 'name', 'Revisions'),
          }
giscedata_at_linia()


class GiscedataRevisionsAtReglaments(osv.osv):
    _name = 'giscedata.revisions.at.reglaments'

    _columns = {
        'name': fields.char('Nom del reglament', size=50),
        'data_ini': fields.date('Data inici', required=True),
        'data_fi': fields.date('Data final'),
        'active': fields.boolean('Actiu')
    }

GiscedataRevisionsAtReglaments()


class giscedata_revisions_at_revisio(osv.osv):

    def assignar(self, cr, uid, rev_id, args):
        if isinstance(rev_id, list):
            rev_id = rev_id[0]
        revisio = self.browse(cr, uid, rev_id)
        if not revisio.data or not revisio.tecnic.id:
            raise osv.except_osv(
                'Error !', 'Has d\'assignar un tècnic i una data.')

        linia_id = revisio.name.id
        linia_obj = self.pool.get('giscedata.at.linia')
        linia_data = linia_obj.read(cr, uid, linia_id, ['suports'])

        if not linia_data.get('suports', False):
            raise osv.except_osv(
                'Error !', 'Aquesta linia no te suports')

        suports_obj = self.pool.get('giscedata.revisions.at.suports')

        for suport in linia_data.get('suports'):
            if not suports_obj.search(cr, uid, [
                ('revisio_at_id', '=', rev_id),
                ('suport_id', '=', suport),
            ]):
                suports_obj.create(cr, uid, {
                    'revisio_at_id': rev_id,
                    'suport_id': suport
                })
        self.write(cr, uid, [revisio.id], {'state': 'assignat'})
        return True

    def desassignar(self, cr, uid, ids, args):
        for revisio in self.browse(cr, uid, ids):
            self.write(cr, uid, [revisio.id], {'state': 'sense assignar'})
        return True

    def tancar(self, cr, uid, ids, args):
        for revisio in self.browse(cr, uid, ids):
            self.write(cr, uid, [revisio.id], {'state': 'tancada'})
        return True

    def reobrir(self, cr, uid, ids, args):
        for revisio in self.browse(cr, uid, ids):
            self.write(cr, uid, [revisio.id], {'state': 'reoberta'})
        return True

    def sincronitzar_defectes(self, cr, uid, ids, args, context=None):
        if isinstance(ids, list):
            ids = ids[0]

        query = """
            SELECT defecteSuport.id, suport.suport_id, defecteSuport.classificacio,
            defecteSuport.tipus_defecte_id, defecteSuport.observacions,
            defecteSuport.tipus_valoracio, suport.revisio_at_id
            FROM giscedata_revisions_at_suports_defectes defecteSuport,
            giscedata_revisions_at_suports suport,
            giscedata_revisions_at_revisio revisio
            WHERE revisio.id = suport.revisio_at_id AND
            suport.id = defecteSuport.rev_suport_id AND
            defecteSuport.valoracio = 'I' AND
            revisio.id = %s
        """
        cr.execute(
            query, (ids,)
        )
        defecte_suports = cr.dictfetchall()

        revisio_inst = self.browse(cr, uid, ids)
        updateds = []
        defecte_obj = self.pool.get('giscedata.revisions.at.defectes')
        for defecte in revisio_inst.defectes_ids:
            for defecte_suport in defecte_suports:
                defecte_suport_id = defecte_suport['suport_id']
                defecte_suport_tipus_defecte_id = defecte_suport[
                    'tipus_defecte_id'
                ]
                if defecte.suport.id == defecte_suport_id and defecte.defecte_id.id == defecte_suport_tipus_defecte_id:
                    updateds.append(
                        (defecte_suport_id, defecte_suport_tipus_defecte_id)
                    )
                    params = {
                        'valoracio': defecte_suport['tipus_valoracio'],
                        'observacions': defecte_suport['observacions'],
                        'revisio_id': defecte_suport['revisio_at_id'],
                    }
                    defecte_obj.write(cr, uid, [defecte.id], params)

        for defecte_suport in defecte_suports:
            updated = False
            defecte_suport_id = defecte_suport['suport_id']
            defecte_suport_tipus_defecte_id = defecte_suport['tipus_defecte_id']
            for suport_id, defecte_id in updateds:
                updated = defecte_suport_id == suport_id and defecte_id == defecte_suport_tipus_defecte_id
                if updated:
                    break
            if not updated:
                params = {
                    'suport': defecte_suport_id,
                    'defecte_id': defecte_suport_tipus_defecte_id,
                    'valoracio': defecte_suport['tipus_valoracio'],
                    'observacions': defecte_suport['observacions'],
                    'revisio_id': defecte_suport['revisio_at_id'],
                }
                defecte_obj.create(cr, uid, params, context)

    def _data_comprovacio(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for id in ids:
            cr.execute("select data_comprovacio from giscedata_revisions_at_defectes \
            where revisio_id = %s and data_comprovacio is not null order by data_comprovacio desc limit 1", (id,))
            data = cr.fetchone()
            if data and len(data):
                res[id] = data[0]
            else:
                res[id] = ''
        return res

    def _nom_linia(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for defecte in self.browse(cr, uid, ids):
            if defecte.name:
                res[defecte.id] = defecte.name.descripcio
            else:
                res[defecte.id] = ''
        return res

    def _nom_linia_search(self, cr, uid, obj, name, args, context=None):
        if not len(args):
            return []
        else:
            s_ids = self.pool.get('giscedata.at.linia').search(cr, uid,\
              [('descripcio', args[0][1], args[0][2])])
            if not len(s_ids):
                return [('id','=','0')]
            else:
                ids = []
                for ct in self.pool.get('giscedata.at.linia').browse(cr, uid, s_ids):
                    for r in ct.revisions:
                        ids.append(r.id)
                return [('id', 'in', ids)]


    def _tots_reparats(self, cr, uid, ids, field_name, arg, context={}):

        res = {}
        for revisio in self.browse(cr, uid, ids):
            tots_reparats = []
            ind_reparats = []
            if len(revisio.defectes_ids):
                for defecte in revisio.defectes_ids:
                    if defecte.reparat:
                        tots_reparats.append(True)
                        if not defecte.intern:
                            ind_reparats.append(True)
                    else:
                        tots_reparats.append(False)
                        if not defecte.intern:
                            ind_reparats.append(False)

                res[revisio.id] = {'tots_reparats': not False in tots_reparats,
                                   'num_tots_reparats': '%s/%s' % \
                                         (tots_reparats.count(True),
                                          len(tots_reparats)),
                                   'ind_reparats': not False in ind_reparats,
                                   'num_ind_reparats':  '%s/%s' %\
                                         (ind_reparats.count(True),
                                          len(ind_reparats)),
                                  }
            else:
                res[revisio.id] = {'tots_reparats': True,
                                   'num_tots_reparats': '0/0',
                                   'ind_reparats': True,
                                   'num_ind_reparats': '0/0',
                                  }
        return res


    def _certificable(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for revisio in self.browse(cr, uid, ids):
            if revisio.state == 'tancada':
                if len(revisio.defectes_ids) == 0:
                    res[revisio.id] = True
                else:
                    reparats = []
                    for defecte in revisio.defectes_ids:
                        if not defecte.intern:
                            reparats.append(defecte.reparat)
                    res[revisio.id] = not False in reparats
            else:
                res[revisio.id] = False
        return res


    def _tots_reparats_search(self, cr, uid, obj, name, args, context=None):
        #TODO: Substituir per una query SQL potent
        if not len(args):
            return []
        else:
            res = []
            for revisio in self.browse(cr, uid, self.search(cr, uid, [('state','=','tancada')])):
                if revisio.tots_reparats == bool(args[0][2]):
                    res.append(revisio.id)
            if not len(res):
                return [('id','=','0')]
            return [('id','in',res)]

    def _ind_reparats_search(self, cr, uid, obj, name, args, context=None):
        #TODO: Substituir per una query SQL potent
        if not len(args):
            return []
        else:
            res = []
            for revisio in self.browse(cr, uid, self.search(cr, uid, [('state','=','tancada')])):
                if revisio.ind_reparats == bool(args[0][2]):
                    res.append(revisio.id)
            if not len(res):
                return [('id','=','0')]
            return [('id','in',res)]

    def _municipi(self, cr, uid, ids, field_name, arg, context):
        # Fake function, nomes ens interessa buscar
        res = {}
        for i in ids:
            res[i] = ''
        return res

    def _municipi_search(self, cr, uid, obj, name, args, context=None):
        if not len(args):
            return []
        else:
            cr.execute("""select r.id from giscedata_revisions_at_revisio r, giscedata_at_linia l, res_municipi m where r.name = l.id and l.municipi = m.id and m.name ilike %s""", ('%%%s%%' % args[0][2],))
            r_ids = [a[0] for a in cr.fetchall()]
            if not len(r_ids):
                return [('id','=','0')]
            else:
                return [('id', 'in', r_ids)]

    def name_get(self, cr, uid, ids, context={}):
        if not len(ids):
            return []
        res = []
        for revisio in self.browse(cr, uid, ids, context):
            name = '%s - %s' % (revisio.name.name, revisio.trimestre.name)
            res.append((revisio.id, name))
        return res

    def create(self, cursor, uid, vals, context=None):
        if context is None:
            context = {}
        linia_at_id = vals.get('name', False)
        if linia_at_id:
            linia_at_obj = self.pool.get('giscedata.at.linia')
            linia_at = linia_at_obj.browse(cursor, uid, linia_at_id)
            at_trams = linia_at.trams
            if at_trams:
                data_pm_at = min([tram.data_pm for tram in at_trams])
                regl_obj = self.pool.get('giscedata.revisions.at.reglaments')
                regl_ids = regl_obj.search(
                    cursor, uid, [
                        ('data_ini', '<=', data_pm_at),
                        '|',
                        ('data_fi', '=', False),
                        ('data_fi', '>=', data_pm_at)
                    ], context={'active_test': False}
                )
                vals['reglaments'] = [(6, False, regl_ids)]

        res_id = super(giscedata_revisions_at_revisio,
                       self).create(cursor, uid, vals, context)
        return res_id

    def _autocomplete(self, cursor, uid, ids, context=None):
        for revisio_id in ids:
            revisio_suport_obj = self.pool.get('giscedata.revisions.at.suports')
            revisio_suport_ids = revisio_suport_obj.search(
                cursor, uid, [('revisio_at_id', '=', revisio_id)],
                context=context
            )
            revisio_suport_obj._autocomplete_normative_2(
                cursor, uid, revisio_suport_ids, context=context
            )

    _name = "giscedata.revisions.at.revisio"
    _description = "Línies AT a revisar"

    _columns = {
        'name': fields.many2one(
            'giscedata.at.linia', 'Línia', required=True, readonly=False,
            states={
                'assignat': [('readonly', True)],
                'tancada': [('readonly', True)],
                'reoberta': [('readonly', True)]
            }
        ),
        'nom_linia': fields.function(
            _nom_linia, fnct_search=_nom_linia_search, method=True,
            type='char', string="Nom de la Línia"
        ),
        'trimestre': fields.many2one(
            'giscedata.trieni', 'Trimestre', required=True, readonly=False,
            states={
                'assignat': [('readonly', True)],
                'tancada': [('readonly', True)],
                'reoberta': [('readonly', True)]
            }
        ),
        'tecnic': fields.many2one(
            'giscedata.revisions.tecnic', 'Tècnic', readonly=False,
            states={
                'assignat': [('readonly', True)],
                'tancada': [('readonly', True)],
                'reoberta': [('readonly', True)]
            }
        ),
        'operari': fields.many2one(
            'res.partner.address', 'Operari', readonly=False,
            states={
                'assignat': [('readonly', True)],
                'tancada': [('readonly', True)],
                'reoberta': [('readonly', True)]
            }
        ),
        'data': fields.datetime(
            'Data Inici', readonly=False, states={
                'tancada': [('readonly', True)]
            }
        ),
        'data_fi': fields.datetime(
            'Data Final', readonly=False, states={
                'tancada': [('readonly', True)]
            }
        ),
        'reglaments': fields.many2many(
            'giscedata.revisions.at.reglaments',
            'revisio_reglaments_at_rel', 'revisio_id', 'reglament_id',
            'Reglaments'),
        'observacions': fields.text('Observacions'),
        'data_ca': fields.datetime('Data Certificat Acta'),
        'data_ca2': fields.datetime('Data Certificat Reparació'),
        'mesures': fields.boolean('Mesures'),
        'talar': fields.char('Trimestre de Tala', size=60),
        'trieni': fields.integer('Trieni'),
        'state': fields.char('Estat', size=60, readonly=True),
        'defectes_ids': fields.one2many(
            'giscedata.revisions.at.defectes', 'revisio_id', 'Defectes',
            states={
                'sense assignar': [('readonly', True)],
                'tancada': [('readonly', True)]
            }
        ),
        'data_comprovacio': fields.function(
            _data_comprovacio, method=True,
            string="Data comprovació reparació de defectes", type='char'
        ),
        'tots_reparats': fields.function(
            _tots_reparats, method=True, type='boolean',
            string='Defectes reparats', fnct_search=_tots_reparats_search,
            multi='defecte'
        ),
        'num_tots_reparats': fields.function(
            _tots_reparats, method=True, type='char',
            string='Reparats/Totals', multi='defecte'
        ),
        'ind_reparats': fields.function(
            _tots_reparats, method=True, type='boolean',
            string='Defectes Indústria reparats',
            fnct_search=_ind_reparats_search, multi='defecte'
        ),
        'num_ind_reparats': fields.function(
            _tots_reparats, method=True, type='char',
            string='Reparats/Totals Indústria', multi='defecte'
        ),
        'equips_utilitzats': fields.many2many(
            'giscedata.revisions.equips', 'giscedata_revisions_at_equips',
            'revisio_id', 'equip_id', 'Equips'
        ),
        'certificable': fields.function(
            _certificable, method=True, type='boolean',
            string='Certificable', fnct_search=_tots_reparats_search
        ),
        'municipi': fields.function(
            _municipi, method=True, fnct_search=_municipi_search,
            string="Municipi", type='char', size=256, select=True
        ),
        'suports': fields.one2many(
            'giscedata.revisions.at.suports', 'revisio_at_id', 'Suports'
        )
    }

    _defaults = {
      'state': lambda *a: 'sense assignar',
    }

    _order = "name, id"

    _sql_constraints = [('linia_trim', 'unique (name, trimestre)', 'No es pot revisar una línia més d\'una vegada al mateix trimestre.')]

giscedata_revisions_at_revisio()


class giscedata_revisions_at_tipusvaloracio(osv.osv):

    _name = "giscedata.revisions.at.tipusvaloracio"
    _description = "Tipus de valoració"

    _columns = {
      'name': fields.char('Valoració', size=60),
      'active': fields.boolean('Active'),
      't_qty': fields.integer('Temps'),
      't_type': fields.selection([('day', 'Dia'),('month', 'Mes'), ('year', 'Any')], 'Unitat')
    }

    _defaults = {
      'active': lambda *a: True,
      't_type': lambda *a: 'day',
    }

giscedata_revisions_at_tipusvaloracio()

class giscedata_revisions_at_normativa(osv.osv):

    _name = "giscedata.revisions.at.normativa"
    _description = "Normativa"

    _columns = {
      'name': fields.char('Nom de la normativa', size=255),
      'inici': fields.date('Inici'),
      'final': fields.date('Final'),
      'vigent': fields.boolean('Vigent'),
    }

    _defaults = {

    }

    _order = "name, id"

giscedata_revisions_at_normativa()

class giscedata_revisions_at_tipusdefectes(osv.osv):

    _name = "giscedata.revisions.at.tipusdefectes"
    _description = "Tipus de defecte i a quin tipus de lat s'aplica"

    _columns = {
      'name': fields.char('Codi', size=25),
      'descripcio': fields.text('Descripció'),
      'normativa': fields.many2one('giscedata.revisions.at.normativa', 'Normativa'),
      'intern': fields.boolean('Intern'),
    }

    _defaults = {

    }

    _order = "name, name"

giscedata_revisions_at_tipusdefectes()


class giscedata_revisions_at_defectes(osv.osv):

    def onchange_defecte_desc(self, cr, uid, ids, defecte_id):
        res = {}
        if defecte_id:
            td = self.pool.get('giscedata.revisions.at.tipusdefectes').browse(cr, uid, defecte_id)
            return {'value':{'defecte_desc': td.descripcio, 'intern': td.intern}}
        else:
            return {'value':{'defecte_desc': ''}}


    def _defecte_desc(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for defecte in self.browse(cr, uid, ids):
            if defecte.defecte_id:
                res[defecte.id] = defecte.defecte_id.descripcio
            else:
                res[defecte.id] = ''
        return res

    def _data_limit_reparacio(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for defecte in self.browse(cr, uid, ids):
            if defecte.valoracio:
                cr.execute("SELECT r.data + INTERVAL '%s %s' AS data_limit_reparacio FROM giscedata_revisions_at_revisio r, giscedata_revisions_at_defectes d WHERE r.id = d.revisio_id AND d.id = %s" % (defecte.valoracio.t_qty, defecte.valoracio.t_type, defecte.id))
                res[defecte.id] = cr.fetchone()[0]
            else:
                res[defecte.id] = ''
        return res

    def _intern(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for defecte in self.browse(cr, uid, ids):
            if defecte.defecte_id:
                res[defecte.id] = defecte.defecte_id.intern
            else:
                res[defecte.id] = 0
        return res

    def _linia(self, cr, uid, ids, field_name, arg, context={}):
        res = {}
        for defecte in self.browse(cr, uid, ids, context):
            res[defecte.id] = defecte.revisio_id.name.name
        return res

    def _search_by_data_lim(self, cr, uid, obj, name, args, context=None):
        """
        Function used to search by the function field "data limit reparacio"
        of the model.
        :param cr: Database cursor
        :type cr: DBCursor
        :param uid: Not Used
        :param obj: Not Used
        :param name: Not Used
        :param args: Search filter parameters
        :type args: List of (any,str,any)
        :param context: Not Used
        :return: Search filter of the ids selected by the search filter params
        :rtype List of (str,str, List of int)
        """
        search_date = args[0][2]
        search_operator = args[0][1]
        query = """
                SELECT d.id AS id_defecte
                FROM  giscedata_revisions_at_revisio r,
                      giscedata_revisions_at_defectes d,
                      giscedata_revisions_at_tipusvaloracio v
                WHERE r.id = d.revisio_id
                AND d.valoracio = v.id
                AND (r.data + (v.t_qty::text ||' '|| v.t_type)::INTERVAL) %s %s
                """

        cr.execute(query, (AsIs(search_operator), search_date))
        ids_res = cr.fetchall()
        return [('id', 'in', ids_res)]

    _name = "giscedata.revisions.at.defectes"
    _description = "Defectes d'una LAT"

    _columns = {
      'id': fields.integer('Id'),
      'name': fields.char('Codi', size=60),
      'suport': fields.many2one('giscedata.at.suport', 'Suport', ondelete='restrict'),
      'defecte_id': fields.many2one('giscedata.revisions.at.tipusdefectes', 'Codi tipus defecte'),
      'defecte_desc': fields.function(_defecte_desc, method=True, string="Descripció tipus defecte", type="text"),
      'data_limit_reparacio': fields.function(
          _data_limit_reparacio,
          method=True,
          string="Data límit de reparació",
          type="datetime",
          size=60,
          fnct_search=_search_by_data_lim
      ),
      'data_reparacio': fields.datetime('Data de reparació'),
      'valoracio': fields.many2one('giscedata.revisions.at.tipusvaloracio', 'Valoració'),
      'observacions': fields.text('Observacions'),
      'reparat': fields.boolean('Reparat'),
      'observacions_reparacio': fields.text('Observacions de Reparació'),
      'data_comprovacio': fields.datetime('Data de comprovació'),
      'revisio_id': fields.many2one('giscedata.revisions.at.revisio', 'Revisió'),
      'intern': fields.function(_intern, method=True, string="Intern", type="boolean"),
      'linia': fields.function(_linia, method=True, string="Linia", type="char", size=60),
    }

    _defaults = {

    }

    _order = "name, id"

giscedata_revisions_at_defectes()

# REPORTS AMB POSTGRESQL VIEWS

class giscedata_revisions_ct_automatitzacio1(osv.osv):
    _name = "giscedata.revisions.ct.automatitzacio1"
    _description = "Primer llistat d'automatització de revisions de CTs"
    _auto = False
    _columns = {
      'name': fields.char('CT', size=30, readonly=True),
      'descripcio': fields.char('Descripció', size=255, readonly=True),
      'data_cfo': fields.date('Data CFO', readonly=True),
    }

    _order = "name, id"

    def init(self, cr):
        cr.execute("""
          create or replace view giscedata_revisions_ct_automatitzacio1 as (
            select id, name, descripcio, data_cfo from (select ct.id, ct.name, ct.descripcio, (select e.industria_data as data from giscedata_expedients_expedient e, giscedata_cts_expedients_rel r, giscedata_cts c where c.id = r.ct_id and r.expedient_id = e.id and e.industria_data is not null and c.id = ct.id order by data desc limit 1) as data_cfo from giscedata_cts ct where ct.id not in (select rev.name from giscedata_revisions_ct_revisio rev) and ct.ct_baixa = False and ct.active = True) as foo where data_cfo is null)""")

giscedata_revisions_ct_automatitzacio1()

class giscedata_revisions_ct_automatitzacio2(osv.osv):
    _name = "giscedata.revisions.ct.automatitzacio2"
    _description = "Segon llistat d'automatització de revisions de CTs"
    _auto = False
    _columns = {
      'name': fields.char('CT', size=30, readonly=True),
      'descripcio': fields.char('Descripció', size=255, readonly=True),
      'data_cfo': fields.date('Data CFO', readonly=True),
    }

    _order = "name, id"

    def init(self, cr):
        cr.execute("""
          create or replace view giscedata_revisions_ct_automatitzacio2 as (
            select id, name, descripcio, data_cfo from (select ct.id, ct.name, ct.descripcio, (select e.industria_data as data from giscedata_expedients_expedient e, giscedata_cts_expedients_rel r, giscedata_cts c where c.id = r.ct_id and r.expedient_id = e.id and e.industria_data is not null and c.id = ct.id order by data desc limit 1) as data_cfo from giscedata_cts ct where ct.id not in (select rev.name from giscedata_revisions_ct_revisio rev) and ct.ct_baixa = False and ct.active = True) as foo where data_cfo is not null)""")

giscedata_revisions_ct_automatitzacio2()

class giscedata_revisions_ct_automatitzacio3(osv.osv):
    _name = "giscedata.revisions.ct.automatitzacio3"
    _description = "Tercer llistat d'automatització de revisions de CTs"
    _auto = False
    _columns = {
      'name': fields.char('CT', size=30, readonly=True),
      'descripcio': fields.char('Descripció', size=255, readonly=True),
      'data_cfo': fields.date('Data CFO', readonly=True),
    }

    _order = "name, id"

    def init(self, cr):
        cr.execute("""
          create or replace view giscedata_revisions_ct_automatitzacio3 as (
            select id, name, descripcio, data_cfo from (select ct.id, ct.name, ct.descripcio, (select e.industria_data as data from giscedata_expedients_expedient e, giscedata_cts_expedients_rel r, giscedata_cts c where c.id = r.ct_id and r.expedient_id = e.id and e.industria_data is not null and c.id = ct.id order by data desc limit 1) as data_cfo from giscedata_cts ct where ct.id in (select rev.name from giscedata_revisions_ct_revisio rev) and ct.ct_baixa = False and ct.active = True) as foo where data_cfo is not null and to_char(now(), 'YYYY')::int - to_char(data_cfo, 'YYYY')::int < 6)""")


giscedata_revisions_ct_automatitzacio3()


class GiscedataRevisionsAtSuports(osv.osv):

    def create(self, cr, uid, vals, context=None):
        if not context:
            context = {}
        if not vals:
            vals = {}

        suport_id = super(GiscedataRevisionsAtSuports, self).create(
            cr, uid, vals, context)

        normativa_obj = self.pool.get('giscedata.revisions.at.normativa')
        normativa_id = normativa_obj.search(cr, uid, [
            ('vigent', '=', True)
        ])

        defectes_obj = self.pool.get('giscedata.revisions.at.tipusdefectes')
        defecte_ids = defectes_obj.search(cr, uid, [
            ('normativa', '=', normativa_id)
        ])

        defectes = []
        suports_defectes_obj = self.pool.get(
            'giscedata.revisions.at.suports.defectes')
        for defecte_id in defecte_ids:
            suports_defectes_id = suports_defectes_obj.create(cr, uid, {
                'rev_suport_id': suport_id,
                'tipus_defecte_id': defecte_id,
            })
            defectes.append(suports_defectes_id)

        self.write(cr, uid, suport_id, {
            'defecte_ids': [(6, 0, defectes)]
        })

        return suport_id

    def _autovalvules(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for suport in self.browse(cr, uid, ids):
            if suport.suport_id:
                res[suport.id] = suport.suport_id.autovalvules
            else:
                res[suport.id] = ''
        return res

    def _seccionador(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for suport in self.browse(cr, uid, ids):
            if suport.suport_id:
                res[suport.id] = suport.suport_id.seccionador
            else:
                res[suport.id] = ''
        return res

    def _creua_linia(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for suport in self.browse(cr, uid, ids):
            if suport.suport_id:
                res[suport.id] = suport.suport_id.creua_linia
            else:
                res[suport.id] = ''
        return res

    def _creua_carretera(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for suport in self.browse(cr, uid, ids):
            if suport.suport_id:
                res[suport.id] = suport.suport_id.creua_carretera
            else:
                res[suport.id] = ''
        return res

    def _poste(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for suport in self.browse(cr, uid, ids):
            if suport.suport_id:
                res[suport.id] = suport.suport_id.poste.name
            else:
                res[suport.id] = ''
        return res

    def _antiescala(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for suport in self.browse(cr, uid, ids):
            if suport.suport_id:
                res[suport.id] = suport.suport_id.antiescala
            else:
                res[suport.id] = ''
        return res

    def _centre_transformador(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for suport in self.browse(cr, uid, ids):
            if suport.suport_id:
                res[suport.id] = suport.suport_id.centre_transformador
            else:
                res[suport.id] = ''
        return res

    def _frequentat(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for suport in self.browse(cr, uid, ids):
            if suport.suport_id:
                frequentat = suport.suport_id.frequentat
                formatted_frequentat = ''
                if frequentat == 'no_frequentat':
                    formatted_frequentat = 'No freqüentat'
                elif frequentat == 'no_calcat':
                    formatted_frequentat = 'Freqüentat sense calçat'
                elif frequentat == 'calcat':
                    formatted_frequentat = 'Freqüentat amb calçat'
                res[suport.id] = formatted_frequentat
            else:
                res[suport.id] = ''
        return res

    def _codi_inspeccio(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for suport in self.browse(cr, uid, ids):
            if suport.suport_id:
                revisio = suport.revisio_at_id
                res[suport.id] = '{}/{}'.format(revisio.name.name, revisio.trimestre.name)
            else:
                res[suport.id] = ''
        return res

    def _data(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for suport in self.browse(cr, uid, ids):
            if suport.suport_id:
                res[suport.id] = suport.revisio_at_id.data
            else:
                res[suport.id] = ''
        return res

    def _tecnic(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for suport in self.browse(cr, uid, ids):
            if suport.suport_id:
                res[suport.id] = suport.revisio_at_id.tecnic.name
            else:
                res[suport.id] = ''
        return res

    def _codi_linia_descripcio(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for suport in self.browse(cr, uid, ids):
            if suport.suport_id:
                res[suport.id] = '{} - {}'.format(
                    suport.revisio_at_id.name.name,
                    suport.suport_id.linia_descripcio
                )
            else:
                res[suport.id] = ''
        return res

    def _ferramenta(self, cr, uid, ids, field_name, arg, context=None):
        '''
        :return: Última mesura de ferramenta d'un suport de la llista de mesures
        de ferramenta del suport. Si no en té llavors retorna el símbol 'guió'.
        '''

        res = {}
        for suport in self.browse(cr, uid, ids):
            if suport.suport_id and suport.suport_id.mesures:
                res[suport.id] = suport.suport_id.mesures[-1].ferramenta
            else:
                res[suport.id] = '-'
        return res

    def _seguretat_reforcada(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for suport in self.browse(cr, uid, ids):
            if suport.suport_id:
                res[suport.id] = suport.suport_id.seguretat_reforcada
            else:
                res[suport.id] = ''
        return res

    def _total_defectes(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for suport in self.browse(cr, uid, ids):
            total_defectes = 0
            for defecte_suport in suport.defecte_ids:
                if defecte_suport.valoracio == 'I':
                    total_defectes += 1
            res[suport.id] = total_defectes
        return res

    def _pas_per_zones(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for suport in self.browse(cr, uid, ids):
            if suport.suport_id:
                res[suport.id] = suport.suport_id.pas_per_zones
            else:
                res[suport.id] = ''
        return res

    def _n_defectes_anteriors(self, cr, uid, ids, field_name, arg, context=None):
        defectes_revisions__obj = self.pool.get(
            'giscedata.revisions.at.defectes'
        )
        self_obj = self.pool.get('giscedata.revisions.at.suports')
        res = {}
        for revisio_suport_id in ids:
            revisio_suport_vals = self_obj.read(
                cr, uid, [revisio_suport_id], ['suport_id', 'revisio_at_id']
            )
            suport_id = revisio_suport_vals[0]['suport_id'][0]
            revisio_id = revisio_suport_vals[0]['revisio_at_id'][0]

            res[revisio_suport_id] = len(defectes_revisions__obj.search(
                cr, uid, [
                    ('suport', '=', suport_id),
                    ('reparat', '=', False),
                    ('revisio_id', '!=', revisio_id)
                ]
            ))
        return res

    def _frequentat_bool(self, cr, uid, ids, field_name, arg, context=None):
        self_obj = self.pool.get('giscedata.revisions.at.suports')
        res = {}
        for revisio_suport_id in ids:
            frequentat = self_obj.read(
                cr, uid, [revisio_suport_id], ['frequentat']
            )[0]['frequentat']
            res[revisio_suport_id] = 'Freqüentat' in frequentat
        return res

    def _autocomplete_normative_2(self, cr, uid, ids, context=None):
        defs_suports_obj = self.pool.get(
            'giscedata.revisions.at.suports.defectes'
        )
        selfs_vals = self.read(cr, uid, ids, ['defecte_ids'], context=context)
        for self_vals in selfs_vals:
            defs_suports_obj.autocomplete(
                cr, uid, self_vals['defecte_ids'], context=context
            )
        self.write(cr, uid, ids, {'autocompleted': True}, context=context)

    _name = 'giscedata.revisions.at.suports'

    _columns = {
        'revisio_at_id': fields.many2one(
            'giscedata.revisions.at.revisio', string='Revisió'),
        'suport_id': fields.many2one(
            'giscedata.at.suport', string='Suport'),
        'defecte_ids': fields.one2many(
            'giscedata.revisions.at.suports.defectes', 'rev_suport_id'),
        'autovalvules': fields.function(
            _autovalvules,
            method=True,
            string="Autovalvules",
            type="boolean"
        ),
        'seccionador': fields.function(
            _seccionador,
            method=True,
            string="Seccionador",
            type="boolean"
        ),
        'creua_linia': fields.function(
            _creua_linia,
            method=True,
            string="Creua linia",
            type="boolean"
        ),
        'creua_carretera': fields.function(
            _creua_carretera,
            method=True,
            string="Creua carretera",
            type="boolean"
        ),
        'poste': fields.function(
            _poste,
            method=True,
            string="Poste",
            type="char",
            size=60
        ),
        'antiescala': fields.function(
            _antiescala,
            method=True,
            string="Antiescala",
            type="boolean"
        ),
        'centre_transformador': fields.function(
            _centre_transformador,
            method=True,
            string="Centre transformador",
            type="boolean"
        ),
        'frequentat': fields.function(
            _frequentat,
            method=True,
            string="Frequentat",
            type="char",
            size=30
        ),
        'codi_inspeccio': fields.function(
            _codi_inspeccio,
            method=True,
            string="Codi inspecció",
            type="char",
            size=10
        ),
        'data': fields.function(
            _data,
            method=True,
            string="Data",
            type="date"
        ),
        'tecnic': fields.function(
            _tecnic,
            method=True,
            string="Tècnic",
            type="char",
            size=100
        ),
        'codi_linia_descripcio': fields.function(
            _codi_linia_descripcio,
            method=True,
            string="Codi linia / descripció",
            type="char",
            size=100
        ),
        'ferramenta': fields.function(
            _ferramenta,
            method=True,
            string="Ferramenta",
            type="char"
        ),
        'observacions': fields.text('Observacions'),
        'seguretat_reforcada': fields.function(
            _seguretat_reforcada,
            method=True,
            string="Seguretat reforçada",
            type="boolean"
        ),
        'total_defectes': fields.function(
            _total_defectes,
            method=True,
            string="Defectes actuals",
            type="integer"
        ),
        'pas_per_zones': fields.function(
            _pas_per_zones,
            method=True,
            string="Pas per zones",
            type="boolean"
        ),
        'autocompleted': fields.boolean('Autocompletat'),
        'revisat': fields.boolean('Revisat'),
        'n_defectes_anteriors': fields.function(
            _n_defectes_anteriors,
            method=True,
            string="Defectes anteriors",
            type="integer"
        ),
        'frequentat_bool': fields.function(
            _frequentat_bool,
            method=True,
            string="Freqüentat",
            type="boolean"
        )
    }


GiscedataRevisionsAtSuports()


class GiscedataRevisionsAtSuportsDefectes(osv.osv):

    def on_change_valoracio(self, cr, uid, ids, valoracio, classificacio, context=None):
        fields = {
            'valoracio': valoracio,
            'classificacio': 'default',
            'data_limit_reparacio': False,
            'tipus_valoracio': False
        }

        if valoracio == 'I':
            tipus_valoracio_obj = self.pool.get(
                'giscedata.revisions.at.tipusvaloracio'
            )
            if not classificacio or classificacio == 'default':
                classificacio = 'L'

            tipus_valoracio_id = tipus_valoracio_obj.search(
                cr, uid, [('name', '=', INTERVAL_MAP.get(classificacio))]
            )[0]

            fields['valoracio'] = valoracio
            fields['classificacio'] = classificacio
            fields['tipus_valoracio'] = tipus_valoracio_id

        self.write(cr, uid, ids[0], fields)

        # A petició les dades referents a la reparació no seran utilitzades.
        if valoracio == 'I' and False:
            fields['data_limit_reparacio'] = self.get_data_limit_reparacio(
                cr, uid, ids, context
            )

        return {'value': fields}

    def on_change_tipus_valoracio(self, cr, uid, ids, classificacio, context=None):
        tipus_valoracio_obj = self.pool.get(
            'giscedata.revisions.at.tipusvaloracio'
        )
        fields = {
            'tipus_valoracio': tipus_valoracio_obj.search(
                cr, uid, [('name', '=', INTERVAL_MAP.get(classificacio))]
            )[0]
        }
        self.write(cr, uid, ids[0], fields)
        return {'value': fields}

    def get_data_limit_reparacio(self, cr, uid, ids, context=None):
        res = ''
        for defecte_suport in self.browse(cr, uid, ids):
            if defecte_suport.valoracio == 'I':
                query = """
                            SELECT r.data + INTERVAL %s AS data_limit_reparacio
                            FROM giscedata_revisions_at_revisio r,
                            giscedata_revisions_at_suports s
                            WHERE s.revisio_at_id = r.id AND
                            s.id = %s
                        """
                tipus_valoracio = defecte_suport.tipus_valoracio

                cr.execute(
                    query,
                    (
                        str(
                            tipus_valoracio.t_qty) + ' ' + tipus_valoracio.t_type,
                        defecte_suport.rev_suport_id.id
                    )
                )
                res = cr.fetchone()[0]
        return res

    def _data_limit_reparacio(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for defecte_suport in self.browse(cr, uid, ids):
            if defecte_suport.valoracio == 'I':
                query = """
                    SELECT r.data + INTERVAL %s AS data_limit_reparacio
                    FROM giscedata_revisions_at_revisio r,
                    giscedata_revisions_at_suports s
                    WHERE s.revisio_at_id = r.id AND
                    s.id = %s
                """
                tipus_valoracio = defecte_suport.tipus_valoracio

                cr.execute(
                    query,
                    (
                        str(tipus_valoracio.t_qty) + ' ' + tipus_valoracio.t_type,
                        defecte_suport.rev_suport_id.id
                    )
                )
                res[defecte_suport.id] = cr.fetchone()[0]
            else:
                res[defecte_suport.id] = ''
        return res

    def _descripcio(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for defecte in self.browse(cr, uid, ids):
            if defecte.tipus_defecte_id:
                res[defecte.id] = defecte.tipus_defecte_id.descripcio
            else:
                res[defecte.id] = ''
        return res

    def _intern(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for defecte in self.browse(cr, uid, ids):
            tipus_defecte = defecte.tipus_defecte_id
            if tipus_defecte:
                res[defecte.id] = tipus_defecte.intern
            else:
                res[defecte.id] = 0
        return res

    def autocomplete(self, cursor, uid, ids, context=None):
        suport_rev_at_obj = self.pool.get('giscedata.revisions.at.suports')
        self_querier = self.q(cursor, uid)
        self_query = self_querier.select(
            ['tipus_defecte_id.intern', 'tipus_defecte_id.name',
             'rev_suport_id', 'id']
        ).where([('id', 'in', ids)])
        cursor.execute(*self_query)
        selfs_vals = cursor.dictfetchall()
        for defecte_vals in selfs_vals:

            suport = suport_rev_at_obj.read(
                cursor, uid, defecte_vals['rev_suport_id'],
                ['pas_per_zones', 'centre_transformador', 'autovalvules',
                 'seccionador', 'creua_carretera', 'creua_linia', 'poste'],
                context=context
            )

            tipus_defecte_name = defecte_vals['tipus_defecte_id.name']

            if defecte_vals['tipus_defecte_id.intern']:
                valoracio = AUTOCOMPLETE_NORMATIVE_2_PEUSA.get(
                    tipus_defecte_name, False
                )
            else:
                # en cas que treballem amb un altre empresa que tingui
                # diferents defectes interns, llavors nomes hauriem de crear
                # la nova taula i posar-la aqui.
                valoracio = AUTOCOMPLETE_NORMATIVE_2.get(
                    tipus_defecte_name, False
                )
            value = False
            if not valoracio:
                value = AUTOCOMPLETE_NORMATIVE_2.get('default', False)
            else:
                if len(valoracio) == 1:
                    value = valoracio.get('default')
                else:
                    seccionador = False
                    creuament = False
                    if suport['pas_per_zones']:
                        value = valoracio.get('pas_per_zones', False)
                    if not value and suport['centre_transformador']:
                        value = valoracio.get('ct', False)
                    if not value and suport['autovalvules']:
                        value = valoracio.get('autovalvules', False)
                    if not value and suport['seccionador']:
                        value = valoracio.get('seccionador', False)
                        seccionador = True
                    if not value and not seccionador:
                        value = valoracio.get('no_seccionador', False)
                    if not value and suport['creua_carretera']:
                        value = valoracio.get('creua_carretera', False)
                        creuament = True
                    if (not value
                        or tipus_defecte_name == '7.5'
                        or tipus_defecte_name == '7.6') and suport['creua_linia']:
                        value = valoracio.get('creua_linia', False)
                        creuament = True
                    if not value and not creuament:
                        value = valoracio.get('no_creuament', False)
                    if not value:
                        value = valoracio.get('poste', False)
                        poste = suport['poste']
                        if not value or not poste:
                            value = valoracio.get('default', False)
                        else:
                            if 'PFU' in poste:
                                value = value.get('PFU')
                            elif 'PM' in poste:
                                value = value.get('PM')
                            elif 'PFO' in poste:
                                value = value.get('PFO')
                            else:
                                value = False
            params = {
                'valoracio': value,
                'classificacio': 'default'
            }
            self.write(cursor, uid, defecte_vals['id'], params, context=context)

    _name = 'giscedata.revisions.at.suports.defectes'
    _rec_name = 'rev_suport_id'

    _columns = {
        'descripcio_defecte': fields.function(
            _descripcio, method=True, string="Codi", type="char", size='255'
        ),
        'rev_suport_id': fields.many2one(
            'giscedata.revisions.at.suports', 'Suports'
        ),
        'tipus_defecte_id': fields.many2one(
            'giscedata.revisions.at.tipusdefectes', 'Tipus defecte ID'
        ),
        'valoracio': fields.selection(
            [
                ('C', 'Correcte'), ('I', 'Incorrecte'),
                ('NA', 'No aplica'), ('NC', 'No comprovat')
            ],
            string='Valoració'
        ),
        'classificacio': fields.selection(
            [
                ('default', ''),
                ('L', 'LLeu'), ('G', 'Greu'), ('MG', 'Molt Greu')
            ],
            string='Classificació'
        ),
        'tipus_valoracio': fields.many2one(
            'giscedata.revisions.at.tipusvaloracio',
            'Tipus de valoració'
        ),
        'observacions': fields.text('Observacions'),
        'reparat': fields.boolean('Reparat'),
        'data_reparacio': fields.datetime('Data de reparació'),
        'data_comprovacio': fields.datetime('Data de comprovació'),
        'observacions_reparacio': fields.text('Observacions de Reparació'),
        'data_limit_reparacio': fields.function(
            _data_limit_reparacio,
            method=True,
            string="Data límit de reparació",
            type="char",
            size=60
        ),
        'intern': fields.function(
            _intern, method=True, string="Intern", type="boolean"
        )
    }

    _defaults = {
        'classificacio': 'default'
    }


GiscedataRevisionsAtSuportsDefectes()
