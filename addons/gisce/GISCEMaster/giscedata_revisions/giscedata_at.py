# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _


class GiscedataAtSuport(osv.osv):
    _name = 'giscedata.at.suport'
    _inherit = 'giscedata.at.suport'

    _columns = {
        "defectes_ids": fields.one2many(
            "giscedata.revisions.at.defectes",
            "suport",
            "Revisions")
    }

    def search_defectes(self, cursor, uid, ids, context=None):
        if not isinstance(ids, list):
            ids = [ids]
        def_obj = self.pool.get('giscedata.revisions.at.defectes')
        sups = def_obj.search(cursor, uid, [('suport.id', 'in', ids)])
        return sups

    def write(self, cursor, uid, ids, vals, context=None):
        def_obj = self.pool.get('giscedata.revisions.at.defectes')
        sups = self.search_defectes(cursor, uid, ids, context)
        if 'name' in vals:
            # one2many widget always save all the fields, name included...
            for sup_id in ids:
                sup = self.read(cursor, uid, [sup_id], ['name'], context)[0]
                if sups and sup['name'] != vals['name']:
                    sups = set([x.suport.name
                                for x in def_obj.browse(cursor, uid, sups)])
                    raise osv.except_osv(
                        _('Error'),
                        _('No es poden renombrar suports que tinguin defectes '
                          'associats: %s') % ', '.join(sups)
                    )
        res = super(GiscedataAtSuport,
                    self).write(cursor, uid, ids, vals, context)
        return res

    def unlink(self, cursor, uid, ids, context=None):
        def_obj = self.pool.get('giscedata.revisions.at.defectes')
        sups = self.search_defectes(cursor, uid, ids, context)
        if sups:
            sups = set([x.suport.name
                        for x in def_obj.browse(cursor, uid, sups)])
            raise osv.except_osv(
                _('Error'),
                _('No es poden eliminar suports que tinguin defectes '
                  'associats: %s') % ', '.join(sups)
            )
        res = super(GiscedataAtSuport, self).unlink(cursor, uid, ids, context)
        return res

GiscedataAtSuport()
