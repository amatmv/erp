## coding=utf-8
<%!
    from datetime import datetime
    import collections

    titles = {
        'Intemperi': {
            '1': 'SUPORTS',
            '2': 'VENTS',
            '3': 'FONAMENTS',
            '4': 'DISTÀNCIES DE SEGURETAT',
            '5': 'POSADA A TERRA',
            '6': 'TRANSFORMADOR',
            '7': 'ELEMENTS DE SECCIONAMENT I PROTECCIÓ EN A.T.',
            '8': 'EMBARRATS I CONNEXIONS EN ALTA TENSIÓ',
            '9': 'PROTECCIONS EN EL SECUNDARI DE BT DEL TRANSFORMADOR'
        },
        'Interior': {
            '1': 'POSADA A TERRA (P.A.T.)',
            '2': 'TRANSFORMADORS',
            '3': 'ELEMENTS DE SECCIONAMENT I PROTECCIÓ EN ALTA TENSIÓ',
            '4': 'EMBARRATS I CONNEXIONS EN ALTA TENSIÓ',
            '5': 'LOCALS',
            '6': 'PROTECCIONS EN EL SECUNDARI DE BT DEL TRANSFORMADOR'
        }
    }
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
        <style type="text/css">
            ${css}
        </style>
        <link rel="stylesheet" href="${addons_path}/giscedata_revisions/report/guia_reconeixement_general.css"/>
    </head>
    <body>
        <%
            full = 1
        %>
        %for revisio_ct in objects:
            <%
                tipus = revisio_ct.name.id_subtipus.tipus_id.name
                subversion = '0'
                if tipus == 'Intemperi':
                    subversion = '1'

            %>

            <%
                defectes = collections.OrderedDict()
            %>
            %for defecte in revisio_ct.defectes_ids:
                <%
                    key = defecte.codi.split(".")[0]
                    if not defectes.get(key):
                        defectes.update({key: [defecte]})
                    else:
                        defectes[key].append(defecte)
                %>
            %endfor
            <table id="big-table">
                <thead>
                    <tr>
                        <td id="version">
                            Codi doc. 13.3-2${subversion}_03
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <div id="bar_big" class="bar">&nbsp;</div>
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <table id="header">
                                <thead>
                                    <tr>
                                        <th id="report-title" colspan="3">
                                            Centres Transformadors d'${tipus} (CT) GUIA DE RECONEIXEMENT
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="td-div" colspan="3">
                                            <div id="bar_small" class="bar">&nbsp;</div>
                                        </td>
                                    </tr>
                                    <tr id="header-subtitles">
                                        <td>CODI INSPECCIÓ:</td>
                                        <td>DATA INSPECCIÓ:</td>
                                        <td>SIGNATURA INSPECTOR:</td>
                                    </tr>
                                    <tr id="header-values">
                                        <%
                                            if revisio_ct.data:
                                                 data_temp = datetime.strptime(revisio_ct.data[:10], '%Y-%m-%d')
                                                 data = data_temp.strftime('%d/%m/%Y')
                                            else:
                                                data = ""
                                        %>
                                        <td>${revisio_ct.name.name}/${revisio_ct.trimestre.name}</td>
                                        <td>${data}</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="td-div" colspan="3">
                                            <div id="bar_middle" class="bar">&nbsp;</div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                %for defecte_general, defectes in defectes.items():
                    <tr>
                        <td>
                            <table class="super-defecte">
                                <tr>
                                    <td class="code-col"><h2>${defecte_general}</h2></td>
                                    <td colspan="3"><h2>${titles[tipus][defecte_general]}</h2></td>
                                </tr>
                                <tr class="header-super-defecte">
                                    <td>CODI</td>
                                    <td class="descr-col">DESCRIPCIÓ DEL DEFECTE</td>
                                    <td class="center-col estat-col">Valoració</td>
                                    <td class="center-col valoracio-col">Correcció</td>
                                </tr>
                                %for defecte in defectes:
                                    <tr class="data">
                                        <%
                                            codi = (defecte.codi).lower()
                                            estat_codi = defecte.estat
                                            estat = ''
                                            valoracio = '-'
                                            if estat_codi == 'A':
                                                estat = 'Correcte'
                                            elif estat_codi == 'B':
                                                estat = 'Incorrecte'
                                                valoracio = 'Correcció en 1 any'
                                            elif estat_codi == 'C':
                                                estat = 'No comprovat'
                                            elif estat_codi == 'D':
                                                estat = 'No aplicable'
                                            # la valoració, al report, mai serà diferent
                                            # de Correcció en 1 any.
                                        %>
                                        <td class="codi">${codi}</td>
                                        <td>${defecte.descripcio}</td>
                                        <td class="center-col estat-col">${estat}</td>
                                        <td class="center-col valoracio-col">${valoracio}</td>
                                    </tr>
                                    <tr>
                                        <%
                                            observacio = defecte.observacions
                                            if not observacio:
                                                observacio = ''
                                        %>
                                        <td colspan="4">OBSERVACIONS: ${observacio}</td>
                                    </tr>
                                %endfor
                            </table>
                        </td>
                    </tr>
                %endfor
                </tbody>
            </table>
        %endfor
    </body>
</html>