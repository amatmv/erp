<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="revisio-list"/>
  </xsl:template>

  <xsl:template match="revisio-list">
    <document>
      <template pageSize="(19cm,297mm)" leftMargin="3cm" rightMargin="3cm" showBoundary="0">
        <pageTemplate id="main">
          <frame id="body" x1="1cm" y1="5mm" width="175mm" height="287mm" />

          <pageGraphics>
            <translate dx="-168mm" dy="170mm" />
            <rotate degrees="-90" />
            <setFont name="Helvetica" size="7" />
            <drawString x="0mm"
            y="170mm">Format doc. 13.3-09_08 / Exemplar per l'entitat d'inspecci�</drawString>
          </pageGraphics>

        </pageTemplate>
      </template>
      
      <stylesheet>

        <paraStyle name="text"
					fontName="Helvetica"
					fontSize="9"
          alignment="justify"
				/>

        <blockTableStyle id="taula_logos">
          <blockAlignment value="LEFT" start="0,0" stop="0,0" />
          <blockAlignment value="RIGHT" start="1,0" stop="1,0" />
        </blockTableStyle>

        <blockTableStyle id="taula_titol">
          <blockAlignment value="CENTER" />
          <blockTopPadding length="0" />
          <blockLeftPadding length="0" />
          <blockRightPadding length="0" />
          <blockBottomPadding length="0" />
          <blockBackground colorName="silver" />
          <lineStyle kind="BOX" colorName="silver" />
          <blockFont name="Helvetica-Bold" size="10" />
          <blockLeading length="4" start="0,0" stop="0,0" />
          <blockLeading length="4" start="0,2" stop="0,2" />
        </blockTableStyle>

        <blockTableStyle id="taula_contingut">
          <lineStyle kind="LINEBELOW" colorName="silver" start="0,0" stop="1,0" />
          <blockFont name="Helvetica" size="9" />
          <blockLeading length="7" />
          
          <blockLeading length="0.000001" start="0,0" stop="1,0" />

          <blockLeading length="20" start="0,18" stop="1,18" />
          <lineStyle kind="LINEBELOW" colorName="silver" start="0,19" stop="1,19" />
          <blockFont name="Helvetica-Bold" start="0,19" stop="1,19" />
          <blockLeading length="10" start="0,19" stop="1,19" />
       
          <blockLeading length="20" start="0,28" stop="1,28" />
          <lineStyle kind="LINEBELOW" colorName="silver" start="0,29" stop="1,29" />
          <blockFont name="Helvetica-Bold" start="0,29" stop="1,29" />
          <blockLeading length="10" start="0,29" stop="1,29" />
        </blockTableStyle>
        
        <blockTableStyle id="taula_trafos">
          <blockFont name="Helvetica" size="8" />
        </blockTableStyle>
        
        <blockTableStyle id="taula_trafos_pot">
          <blockFont name="Helvetica" size="8" />
          <blockFont name="Helvetica-Bold" start="1,0" stop="1,0" />
        </blockTableStyle>

        <blockTableStyle id="taula_firma">
          <blockFont name="Helvetica" size="9" />
          <blockAlignment value="RIGHT" start="1,0" stop="1,0" />
        </blockTableStyle>

      </stylesheet>
    
      <story>
        <xsl:apply-templates select="revisio" mode="story" />
      </story>
    </document>
  </xsl:template>

  <xsl:template match="revisio" mode="story">
    <blockTable colWidths="100mm, 75mm" style="taula_logos">
      <tr>
        <td><image file="addons/giscedata_revisions/report/gisce.tif"
        width="56mm" height="17mm" /></td>
        <td><image file="addons/giscedata_revisions/report/enac.tif"
        width="22mm" height="17mm" /></td>
      </tr>
    </blockTable>
    <spacer length="10" />
    <blockTable colWidths="175mm" style="taula_titol">
      <tr><td></td></tr>
      <tr>
        <td>CERTIFICAT DE RECONEIXEMENT PERI�DIC</td>
      </tr>
      <tr>
        <td>INSTAL�LACI� EL�CTRICA DE SERVEI P�BLIC</td>
      </tr>
      <tr><td></td></tr>
    </blockTable>
    <spacer length="6" />
    <para alignment="center" fontName="Helvetica-Bold">Reconeixement de Centres Transformadors <xsl:value-of select="tipus/general" /></para>
    <blockTable colWidths="70mm,105mm" style="taula_contingut">
      <tr><td></td><td></td></tr>
      <tr>
        <td>Codi de document</td>
        <td>C-CT-<xsl:value-of select="concat(ct/codi, ' ')" /><xsl:value-of select="trimestre" /></td>
      </tr>
      <tr>
        <td>Codi de revisi�</td>
        <td><xsl:value-of select="concat(ct/codi, ' ')" /><xsl:value-of select="trimestre" /></td>
      </tr>
      <tr>
        <td>Expedient autoritizaci� administrativa</td>
        <td><xsl:value-of select="ct/expedient" /></td>
      </tr>
      <tr>
        <td>Data de revisi�</td>
        <td><xsl:value-of select="concat(substring(data, 9, 2), '/', substring(data, 6, 2), '/', substring(data, 1, 4))"/></td>
      </tr>
      <tr>
        <td>Data comprovaci� reparaci� de defectes</td>
        <td><xsl:if test="data_comprovacio!=''"><xsl:value-of select="concat(substring(data_comprovacio, 9, 2), '/', substring(data_comprovacio, 6, 2), '/', substring(data_comprovacio, 1, 4))" /></xsl:if></td>
      </tr>
      <tr>
      	<td>Tipus de comprovaci�</td>
      	<td><xsl:if test="data_ca2 != '' and data_ca2 != '0'">2a visita</xsl:if></td>
      </tr>
      <tr>
        <td>Unitat de reconeixement</td>
        <td><xsl:value-of select="ct/codi" /></td>
      </tr>
      <tr>
        <td>Empresa titular</td>
        <td><xsl:value-of select="ct/propietari/nom" /></td>
      </tr>
      <tr>
        <td>Adre�a per a notificacions</td>
        <td><xsl:value-of select="concat(ct/propietari/partner/adreca/carrer, ' - ', ct/propietari/partner/adreca/cp, ' - ', ct/propietari/partner/adreca/ciutat)" /></td>
      </tr>
      <tr>
        <td>Organisme d'inspecci�</td>
        <td><xsl:value-of select="tecnic/organisme" /></td>
      </tr>
      <tr>
        <td>Adre�a Organisme d'inspecci�</td>
        <td><xsl:value-of select="tecnic/adreca" /></td>
      </tr>
      <tr>
        <td>Nom  cognoms del titulat</td>
        <td><xsl:value-of select="tecnic/nom" /></td>
      </tr>
      <tr>
        <td>DNI</td>
        <td><xsl:value-of select="tecnic/dni" /></td>
      </tr>
      <tr>
        <td>Titulaci�</td>
        <td><xsl:value-of select="tecnic/titolacio" /></td>
      </tr>
      <tr>
        <td>N�mero de col�legiat</td>
        <td><xsl:value-of select="tecnic/ncol" /></td>
      </tr>
      <tr>
        <td>Adre�a per a notificacions</td>
        <td><xsl:value-of select="tecnic/adreca" /></td>
      </tr>
      <tr>
        <td>Tel�fon</td>
        <td><xsl:value-of select="tecnic/telefon" /></td>
      </tr>
      <tr>
        <td>Correu electr�nic</td>
        <td><xsl:value-of select="tecnic/email" /></td>
      </tr>
      <tr>
        <td>Dades de la instal�laci�</td>
        <td></td>
      </tr>
      <tr>
        <td>Instal�laci�</td>
        <td>Centre Transformador <xsl:value-of select="ct/codi" /></td>
      </tr>
      <tr>
        <td>Empla�ament</td>
        <td><xsl:value-of select="ct/descripcio" /></td>
      </tr>
      <tr>
      <tr>
        <td>Poblaci�</td>
        <td><xsl:value-of select="ct/poblacio" /></td>
      </tr>
        <td>Municipi</td>
        <td><xsl:value-of select="ct/municipi" /></td>
      </tr>
      <tr>
        <td>Tipus de Centre Transformador</td>
        <td><xsl:value-of select="ct/tipus/general" /></td>
      </tr>
      <tr>
        <td>Tensi� de primari</td>
        <!-- buscar de totes les connexions quina �s la que est� connectada
             i mostrar la tensi� -->
        <td><xsl:value-of select="ct/tensio_p" /></td>
      </tr>
      <tr>
        <td>Tensi� de secundari</td>
        <td><xsl:value-of select="ct/tensio_s" /></td>
      </tr>
      <tr>
        <td>Sortides de baixa tensi�</td>
        <td><xsl:value-of select="ct/sortides_baixa" /></td>
      </tr>
      <tr>
        <td>Data de posada en marxa</td>
        <!-- Depen de l'expedient -->
        <td><xsl:if test="ct/data_marxa!=''"><xsl:value-of select="concat(substring(ct/data_marxa, 9, 2), '/', substring(ct/data_marxa, 6, 2), '/', substring(ct/data_marxa, 1, 4))"/></xsl:if></td>
      </tr>
      <tr>
        <td>Transformadors</td>
        <td>
          <xsl:apply-templates select="ct/trafos" mode="story" />
        </td>
      </tr>
      <tr>
        <td>Certificaci�</td>
        <td></td>
      </tr>
    </blockTable>
    <para style="text"><xsl:value-of select="tecnic/nom" />, enginyer competent que ha realitzat la inspecci� de la instal�laci� el�ctrica de refer�ncia, d'acord amb l'article 7 del Decret 328/2001, de 4 de desembre, pel qual s'etableix el procediment aplicable per efectuar els reconeixements peri�dics de les instal�lacions de producci�, transformaci�, transport i distribuci� d'energia el�ctrica, publicat en el DOGC n�3.536,
    </para>
    <spacer length="15" />
   <para style="text"><b>CERTIFICA:</b></para>
    <para style="text">
      Que aquesta �s correcta i no t� defectes que precisin correcci� immediata ni defectes a esmenar dins un termini.
    </para>
    <spacer length="10" />
    <para style="text">
      Observacions i explicaci� d'accions no realitzades: <xsl:if test="observacions != '' and observacions != '0'"><xsl:value-of select="observacions" /></xsl:if>
    </para>
    <spacer length="40" />
    <blockTable style="taula_firma" colWidths="70mm,105mm">
      <tr>
        <td>Signatura</td><td>Girona, a <xsl:choose><xsl:when test="data_ca2 != '' and data_ca2 != '0'"><xsl:value-of select="concat(substring(data_comprovacio, 9, 2), '/', substring(data_comprovacio, 6, 2), '/', substring(data_comprovacio, 1, 4))" /></xsl:when><xsl:otherwise><xsl:value-of select="concat(substring(data, 9, 2), '/', substring(data, 6, 2), '/', substring(data, 1, 4))" /></xsl:otherwise></xsl:choose></td>
      </tr>
    </blockTable>
    <para style="text">
      Aquest document queda a disposici� de l'organisme competent del Departament d'Ind�stria, Comer� i Turisme
    </para>
    <para style="text">
      El present certificat t� una validesa de 3 anys a partir de la data de revisi�
    </para>
  </xsl:template>


  <xsl:template match="trafos" mode="story">
    <xsl:if test="count(trafo[estat/codi=1][reductor=0])&gt;0">
    <blockTable colWidths="2cm,2cm,3cm,4.5cm" style="taula_trafos"> 
      <xsl:for-each select="trafo[estat/codi=1][reductor=0]">
      <tr>
        <td>N�m.</td>
        <td><xsl:value-of select="numero" /></td>
        <td>Pot�ncia</td>
        <td><xsl:value-of select="concat(potencia, ' kVA')" /></td>
      </tr>
      </xsl:for-each>
    </blockTable>
    <blockTable colWidths="7cm, 4.5cm" style="taula_trafos_pot">
      <tr>
        <td>POT�NCIA TOTAL</td>
        <td><xsl:value-of select="concat(sum(trafo[estat/codi=1][reductor=0]/potencia), ' kVA')" /></td>
      </tr>
    </blockTable>
    </xsl:if>
    <xsl:if test="count(trafo[estat/codi=1][reductor=1])&gt;0">
    <blockTable colWidths="7cm, 4.5cm" style="taula_trafos_pot">
      <tr>
        <td>Reductors</td>
        <td></td>
      </tr>
    </blockTable>
    <blockTable colWidths="2cm,2cm,3cm,4.5cm" style="taula_trafos"> 
      <xsl:for-each select="trafo[estat/codi=1][reductor=1]">
      <tr>
        <td>N�m.</td>
        <td><xsl:value-of select="numero" /></td>
        <td>Pot�ncia</td>
        <td><xsl:value-of select="concat(potencia, ' kVA')" /></td>
      </tr>
      </xsl:for-each>
    </blockTable>
    <blockTable colWidths="7cm, 4.5cm" style="taula_trafos_pot">
      <tr>
        <td>POT�NCIA TOTAL</td>
        <td><xsl:value-of select="concat(sum(trafo[estat/codi=1][reductor=1]/potencia), ' kVA')" /></td>
      </tr>
    </blockTable>
    </xsl:if>
  </xsl:template>


  <xsl:template match="trafo" mode="story">
    <xsl:choose>
      <xsl:when test="count(estat[codi=1])&gt;0">
        <xsl:for-each select="trafo/estat[codi=1]">
          <tr>
            <td>N�m.</td>
            <td><xsl:value-of select="numero" /></td>
            <td>Pot�ncia</td>
            <td><xsl:value-of select="concat(potencia, ' kVA')" /></td>
          </tr>
        </xsl:for-each>
      </xsl:when>
      <xsl:otherwise>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
      </xsl:otherwise>
    </xsl:choose>
    
  </xsl:template>

</xsl:stylesheet>
