<?xml version="1.0" encoding="iso-8859-15"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

  <xsl:key name="revisions-per-any" match="revisio" use="any" />

  <xsl:template match="/">
    <xsl:apply-templates select="llistat" />
  </xsl:template>

  <xsl:template match="llistat">
    <document compression="1">
      <template pageSize="(21cm, 29.7cm)">
        <pageTemplate id="main">
          <pageGraphics>
          	<!-- Cap�alera -->
          </pageGraphics>
          <frame id="first" x1="1cm" y1="1cm" width="19cm" height="277mm"/>
        </pageTemplate>
      </template>
      
      <stylesheet> 
      	<paraStyle name="text"
      		fontName="Helvetica"
      		fontSize="10" />
      		
      	<blockTableStyle id="any">
          <blockFont name="Helvetica" size="10" />
          <lineStyle kind="BOX" colorName="black" />
          <blockBackground colorName="grey" start="0,0" stop="-1,0" />
        </blockTableStyle>
        
      		
      </stylesheet>
      
      <story>
      	<para style="text" fontSize="14" t="1"><tr t="1">RESUM ANYS DE DEFECTES A LINIES AT</tr></para>
      	<spacer length="20" />
      	<!-- Agrupem els revisions per anys -->


	<blockTable style="any" colWidths="4cm,5cm,5cm,5cm">
		<tr t="1">
			<td><para style="text" t="1">ANY</para></td>
			<td><para style="text" t="1">TOTAL DE DEFECTES</para></td>
			<td><para style="text" t="1">TOTAL DE REPARATS</para></td>
			<td><para style="text" t="1">PENDENTS DE REPARACI�</para></td>
		</tr>
	<xsl:for-each select="revisio[count(. | key('revisions-per-any', any)[1]) = 1]">
		<xsl:sort select="any" />
		<xsl:variable name="any" select="any" />
			<tr>
				<td><para style="text"><xsl:value-of select="any" /></para></td>
				<td><para style="text"><xsl:value-of select="count(//llistat/revisio[any=$any]/defectes/defecte[intern!=1])" /></para></td>
				<td><para style="text"><xsl:value-of select="count(//llistat/revisio[any=$any]/defectes/defecte[reparat=1 and intern!=1])" /> (<xsl:value-of select="format-number((count(//llistat/revisio[any=$any]/defectes/defecte[reparat=1 and intern!=1]) div count(//llistat/revisio[any=$any]/defectes/defecte[intern!=1]))*100, '###.##')" /> %)</para></td>
				<td><para style="text"><xsl:value-of select="count(//llistat/revisio[any=$any]/defectes/defecte[reparat=0 and intern!=1])" /> (<xsl:value-of select="format-number((count(//llistat/revisio[any=$any]/defectes/defecte[reparat=0 and intern!=1]) div count(//llistat/revisio[any=$any]/defectes/defecte[intern!=1]))*100, '###.##')" /> %)</para></td>
			</tr>
		
	</xsl:for-each>
	</blockTable>

      </story>
    </document>
  </xsl:template>

  
</xsl:stylesheet>
