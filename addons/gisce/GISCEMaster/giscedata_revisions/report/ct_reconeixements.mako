## coding=utf-8
<%
    from datetime import datetime,date
    from dateutil.relativedelta import relativedelta

    def obtenir_trimestres(objectes):
        if "trieni" not in origin_access:
            return filtrar_per_trimestres(objectes)
        return objectes

    def filtrar_per_trimestres(objectes):
        dic_trimestres = {}
        for rev in objectes:
            if not rev.trimestre.name in dic_trimestres.keys():
                dic_trimestres.update({rev.trimestre.name : [rev]})
            else:
                dic_trimestres[rev.trimestre.name].append(rev)
        return dic_trimestres

    def definir_tipus_ct(rev_ct):
        if rev_ct.name.id_installacio_name == "CT":
            if rev_ct.name.id_subtipus.tipus_id.code == 1:
                return "PT"
            elif rev_ct.name.id_subtipus.tipus_id.code == 2:
                return "ET"
            else:
                return "Desconegut"
        else:
            return rev_ct.name.id_installacio_name

    def comprovar_cfo(data_ind, data_inici):
        data_aux = data_inici - relativedelta(years=6)
        return data_aux <= data_ind

    def existeix_ct(revisions_list, tipus_CT):
        if revisions_list:
            for item in revisions_list:
                if item[0][0] == tipus_CT:
                    return True
        return False
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<head>
    <style type="text/css">
    ${css}
    </style>
    <link rel="stylesheet" href="${addons_path}/giscedata_revisions/report/stylesheet_reconeixements.css"/>
</head>
<body>
    <%
        llista_trimestres = obtenir_trimestres(objects)
        if "trieni" not in origin_access:
            diccionari_trimestres = llista_trimestres
            llista_trimestres = sorted(llista_trimestres.keys())
    %>
    %for trieni in llista_trimestres:
        <div id="logo">
              <img id="logo_img" src="${addons_path}/giscedata_revisions/report/gisce_logo.png">
        </div>
        <div id="separador"></div>
        <div id="capcalera1">
            <h1>${_("RECONEIXEMENTS CT")}</h1>
            <h2>${company.name}</h2>
        </div>
        <br><br><br><br>
        %if "trieni" not in origin_access:
            <h2 style="margin-top: -15px">${_("Trimestre")} ${trieni}</h2>
        %else:
            <h2 style="margin-top: -15px">${_("Trimestre")} ${trieni.name}</h2>
        %endif

        <div id="separador2"></div>
        <div>
            <p class="bold fs12px">${_("Centres Transformadors:")}</p>
            <%
                cts = []
                if "trieni" not in origin_access:
                    cts = diccionari_trimestres.get(trieni)
                else:
                    cts = trieni.cts
                cts = sorted(cts, key=lambda elem: elem.name.name)
                revisions_list = []
            %>
            <table class="table data-table table-intercoloured-rows">
                <thead>
                    <tr>
                        <th>${_("Codi")}</th>
                        <th>${_("Descripció")}</th>
                        <th>${_("Categoria")}</th>
                        <th>${_("Tipus")}</th>
                        <th>${_("Data rev.")}</th>
                        <th>${_("Data-APM")}</th>
                        <th>${_("Observacions")}</th>
                    </tr>
                </thead>
                <tbody>
                <%
                    categories = ['Local', 'Caseta', 'Soterrat', 'Intemperie']
                    resum_per_categoria = dict.fromkeys(
                        categories, None
                    )
                    for resum in resum_per_categoria:
                        resum_per_categoria[resum] = dict.fromkeys(['previstes', 'revisades'], 0)
                %>
                %for rev_ct in cts:
                    <tr>
                        <td class="col_codi">${rev_ct.name.name}</td>
                        <td>${rev_ct.name.descripcio}</td>
                        <%
                            tipus_CT = definir_tipus_ct(rev_ct)
                            revisada = bool(rev_ct.data)
                            categoria = rev_ct.name.id_subtipus.categoria_cne.name
                            resum_per_categoria[categoria]['previstes'] += 1
                            resum_per_categoria[categoria]['revisades'] += revisada
                            cfo = None
                            if not existeix_ct(revisions_list, tipus_CT):
                                if rev_ct.data:
                                    nou_ct = [tipus_CT, 1, 1]
                                else:
                                    nou_ct = [tipus_CT, 1, 0]
                                revisions_list.append([nou_ct])
                            else:
                                for item in revisions_list:
                                    if item[0][0] == tipus_CT:
                                        item[0][1] += 1
                                        if revisada:
                                            item[0][2] += 1


                        %>
                        <td class="col_categoria">${categoria}</td>
                        <td class="col_tipus">${tipus_CT}</td>

                        <td class="col_data">
                        %if rev_ct.data:
                            <%
                                data_revisio = datetime.strptime(rev_ct.data, '%Y-%m-%d %H:%M:%S')
                                data_rev = data_revisio.strftime('%d/%m/%Y')
                            %>
                            ${data_rev}
                        %endif
                        </td>
                        <td class="col_data">
                        %if rev_ct.name.data_pm:
                            <%
                                date_format = "%Y-%m-%d"
                                data_APM = datetime.strptime(rev_ct.name.data_pm, date_format)
                                data_APM_imprimir = data_APM.strftime('%d/%m/%Y')
                                if "trieni" not in origin_access:
                                    data_trimestre = datetime.strptime(diccionari_trimestres.get(trieni)[0].trimestre.data_inici, date_format)
                                else:
                                    data_trimestre = datetime.strptime(trieni.data_inici, date_format)
                                cfo = comprovar_cfo(data_APM, data_trimestre)
                            %>
                                ${data_APM_imprimir}
                        %endif
                        </td>
                        <td class="col_observacions">
                        %if rev_ct.name.ct_obres:
                            ${_("Obres")}
                        %endif
                        %if cfo:
                            CFO
                        %endif
                        </td>
                    </tr>
                %endfor
                </tbody>
            </table>
        </div>
        <p class="bold fs12px pt30px">${_("Resum instal·lacions revisades:")}</p>
        <div id="div-summary-by-type">
            <table class="table table-summary table-intercoloured-rows">
                <thead>
                    <tr>
                        <th>${_("Instal·lació")}</th>
                        <th>${_("Previstes")}</th>
                        <th>${_("Revisades")}</th>
                    </tr>
                </thead>
                <tbody>
                    <% total_previstes = 0 %>
                    %for revisions_ct in revisions_list:
                        <%
                            previstes = revisions_ct[0][1]
                            total_previstes += previstes
                        %>
                        <tr>
                            <td class="center">${revisions_ct[0][0]}</td>
                            <td class="center">${previstes}</td>
                            <td class="center">${revisions_ct[0][2]}</td>
                        </tr>
                    %endfor
                    <tr class="h40px">
                        <td class="bold center">${_("Total")}</td>
                        <td class="bold center">${total_previstes}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div id="div-summary-by-category">
            <table id="table-summary-by-category" class="table table-summary table-intercoloured-rows">
                <colgroup>
                    <col class="col-inst">
                    <col class="col-prev">
                    <col class="col-revi">
                </colgroup>
                <thead>
                    <tr>
                        <th>${_("Instal·lació")}</th>
                        <th>${_("Previstes")}</th>
                        <th>${_("Revisades")}</th>
                    </tr>
                </thead>
                <tbody>
                    %for categoria in categories:
                        <tr>
                            <td>${_(categoria)}</td>
                            <td class="center">${resum_per_categoria[categoria]['previstes']}</td>
                            <td class="center">${resum_per_categoria[categoria]['revisades']}</td>
                        </tr>
                    %endfor
                    <tr></tr>
                </tbody>
            </table>
        </div>
            %if trieni != llista_trimestres[-1]:
                <p style="page-break-after:always;"></p>
            %endif
    %endfor
</body>
</html>
