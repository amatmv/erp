<%
    from datetime import datetime
    pool = objects[0].pool

    def get_revisions_ct_ordenades(trieni):
        revisions_ct_obj = objects[0].pool.get("giscedata.revisions.ct.revisio")
        llista_revisions_ct=[]
        for i in revisions_ct_obj.search(cursor,uid,[("trimestre",'=',trieni),("state","=","tancada")]):
            llista_revisions_ct.append(revisions_ct_obj.read(cursor,uid,i,["name"]))
        return sorted(llista_revisions_ct, key=lambda elem : elem['name'][1])

    def get_revisions_la_ordenades(trieni):
        revisions_la_obj = objects[0].pool.get("giscedata.revisions.at.revisio")
        llista_revisions_la=[]
        for i in revisions_la_obj.search(cursor,uid,[("trimestre",'=',trieni),("state","=","tancada")]):
            llista_revisions_la.append(revisions_la_obj.read(cursor,uid,i,["name"]))
        return sorted(llista_revisions_la, key=lambda elem : elem['name'][1])

%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<head>
  <style type="text/css">
    ${css}
  </style>
  <link rel="stylesheet" href="${addons_path}/giscedata_revisions/report/estils_portada.css"/>
</head>
<body>
  <div id=content>
    %for trieni in objects:
      <div id=capcalera>
        ${_("RECONEIXEMENTS PERIÒDICS DE LES INSTAL·LACIONS DE PRODUCCIÓ, TRANSFORMACIÓ, TRANSPORT I DISTRIBUCIÓ D'ENERGIA ELÈCTRICA")}
        <h1>Trimestre ${trieni.name}</h1>
      </div>
      <div class=llista_info>
        <h2>${_("RECONEIXEMENT DELS CENTRES DE TRANSFORMACIÓ")}</h2>
        <table>
            <thead>
                <tr>
                    <td>
                        <br><br>
                    </td>
                </tr>
            </thead>
          <%
            cts = get_revisions_ct_ordenades(trieni.id)
          %>
            %for rev_ct in cts:
                <%
                    index = trieni.cts.name.name.index(rev_ct['name'][1])
                    elem = trieni.cts[index]
                %>
                <tr>
                    <td>${elem.name.name}</td>
                    <td>${elem.name.descripcio}</td>
                </tr>
            %endfor
        </table>
      </div>
      <div class=llista_info>
        <h2>${_("RECONEIXEMENT DE LES LÍNIES D'ALTA TENSIÓ")}</h2>
        <table>
            <thead>
                <tr>
                    <td>
                        <br><br>
                    </td>
                </tr>
            </thead>
          <%
            la = get_revisions_la_ordenades(trieni.id)
          %>
            %for rev_la in la:
                <%
                    index = trieni.lat.name.name.index(rev_la['name'][1])
                    elem = trieni.lat[index]
                %>
                <tr>
                    <td>${elem.name.name}</td>
                    <td>${elem.name.descripcio}</td>
                </tr>
            %endfor
        </table>
      </div>

      %if trieni.lbt:
        <div class=llista_info>
          <h2>${_("RECONEIXEMENT DE LES LÍNIES DE BAIXA TENSIÓ")}</h2>
          <table>
            <thead>
                <tr>
                    <td>
                        <br><br>
                    </td>
                </tr>
            </thead>
              <%
                  lbt = sorted(trieni.lbt, key=lambda elem : elem.name.name)
              %>
              %for rev_lb in lbt:
                  <tr>
                      <td>${rev_lb.name.name}</td>
                      <td>${rev_lb.name.descripcio}</td>
                  </tr>
              %endfor
          </table>
        </div>
      %endif

      %if trieni != objects[-1]:
        <p style="page-break-after:always"></p>
      %endif

    %endfor
  </div>
  <div id="logos">
    <div id="logo_gisce">
      <img class="logo_img" src="${addons_path}/giscedata_revisions/report/gisce_logo.png">
    </div>
    <div id="logo_company">
      %if company.logo:
        <img class="logo_img" src="data:image/jpeg;base64,${company.logo}"/>
      %endif
    </div>
    <div style="clear: both;"></div>
  </div>
</body>
</html>
