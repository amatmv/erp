<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="revisio-list" />
  </xsl:template>

  <xsl:template match="revisio-list">
    <document>
      <template pageSize="(297mm,19cm)" topMargin="1cm" bottomMargin="1cm" rightMargin="1cm">
        <pageTemplate id="main">
          <frame id="first" x1="1cm" y1="1cm" width="277mm" height="17cm"/>
        </pageTemplate>
      </template>
      
      <stylesheet>

        <paraStyle name="titol"
					fontName="Helvetica"
					fontSize="10"
          leading="20"
				/>

        <paraStyle name="empresa"
					fontName="Helvetica"
					fontSize="8"
          leading="16"
          alignment="right"
				/>
	<paraStyle name="text"
		fontName="Helvetica"
		fontSize="8" />

        <blockTableStyle id="taula_contingut">
          <blockFont name="Helvetica" size="8" />
          <lineStyle kind="LINEBELOW" colorName="silver" start="0,0" stop="8,0"/>
        </blockTableStyle>

      </stylesheet>
    
      <story>
      <para style="titol" t="1"><tr t="1">RECONEIXEMENTS PERI�DICS DE LES INSTAL�LACIONS DE PRODUCCI�, TRANSFORMACI�, TRANSPORT I DISTRIBUCI� D'ENERGIA EL�CTRICA</tr></para>
      <para style="empresa">Empresa: <b><xsl:value-of select="corporate-header/corporation/name" /></b></para>

      <!--<blockTable style="taula_contingut" colWidths="1.37cm,1.37cm,2.37cm,1.37cm,5.37cm,1.37cm,14.74cm">-->
      <!--<blockTable style="taula_contingut" colWidths="1.37cm,1.37cm,2.37cm,1.37cm,3.37cm,1.37cm,11.46cm,2cm,2.37cm">-->
      <blockTable style="taula_contingut" colWidths="1.37cm,1.37cm,2cm,1.37cm,3.37cm,1.37cm,12.37cm,2cm,1.37cm" repeatRows="1">
       <tr t="1">
          <td t="1">Codi</td>
          <td t="1">Revisi�</td>
          <td t="1">Data revisi�</td>
          <td t="1">CT</td>
          <td></td>
          <td t="1">Descripci�</td>
          <td></td>
          <td t="1">Data L�mit</td>
          <td t="1">Reparat</td>
        </tr>
        <xsl:apply-templates select="revisio/defectes/defecte" mode="story">
          <xsl:sort select="../../data" order="ascending" />
          <xsl:sort select="name" data-type="number" order="ascending"/>
        </xsl:apply-templates>
      </blockTable>
      
      </story>
    </document>
  </xsl:template>
  <xsl:template match="revisio/defectes/defecte" mode="story">
    <xsl:if test="estat='B' and reparat!=1">
    <tr>
      <td><xsl:value-of select="name"/></td>
      <td><xsl:value-of select="../../trimestre"/></td>
      <td><xsl:value-of select="concat(substring(../../data, 9, 2), '/', substring(../../data, 6, 2), '/', substring(../../data, 1, 4))"/></td>
      <td><xsl:value-of select="../../ct/codi"/></td>
      <td><xsl:value-of select="../../ct/descripcio"/></td>
      <td><xsl:value-of select="codi"/></td>
      <td><para style="text"><xsl:value-of select="descripcio" /></para><para style="text"><xsl:value-of select="observacions" /></para></td>
      <td><xsl:value-of select="concat(substring(data_limit, 9, 2), '/', substring(data_limit, 6, 2), '/', substring(data_limit, 1, 4))" /></td>
      <td><xsl:if test="reparat=1">S�</xsl:if><xsl:if test="reparat=0">No</xsl:if></td>
    </tr>
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>
