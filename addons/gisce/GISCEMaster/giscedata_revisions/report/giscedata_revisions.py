# -*- coding: utf-8 -*-

from c2c_webkit_report import webkit_report
from report import report_sxw
from tools import config


class report_webkit_html(report_sxw.rml_parse):
    def __init__(self, cursor, uid, name, context):
        super(report_webkit_html, self).__init__(cursor, uid, name,
                                                 context=context)
        self.localcontext.update({
            'cursor': cursor,
            'uid': uid,
            'addons_path': config['addons_path'],
            'origin_access': name,
        })


webkit_report.WebKitParser(
    'report.giscedata.revisions.inspeccio',
    'giscedata.revisions.ct.revisio',
    'giscedata_revisions/report/inspeccio.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.revisions.inspeccio_at',
    'giscedata.revisions.at.revisio',
    'giscedata_revisions/report/inspeccio_at.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.revisions.reconeixement',
    'giscedata.revisions.at.revisio',
    'giscedata_revisions/report/guia_reconeixement_at.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.revisions.portada',
    'giscedata.trieni',
    'giscedata_revisions/report/portada.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParserNumberPages(
    'report.giscedata.revisions.lliurament.certificats',
    'giscedata.trieni',
    'giscedata_revisions/report/lliurament_certificats.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.revisions.reconeixement.certificats',
    'giscedata.revisions.ct.revisio',
    'giscedata_revisions/report/reconeixement_certificats.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.revisions.reconeixement.certificats_at',
    'giscedata.revisions.at.revisio',
    'giscedata_revisions/report/reconeixement_certificats_at.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.revisions.full.reparacio',
    'giscedata.revisions.ct.revisio',
    'giscedata_revisions/report/full_reparacio.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.revisions.full.reparacio_industria',
    'giscedata.revisions.ct.revisio',
    'giscedata_revisions/report/full_reparacio.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.revisions.full.reparacio_intern',
    'giscedata.revisions.ct.revisio',
    'giscedata_revisions/report/full_reparacio.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.revisions.full.reparacio_at',
    'giscedata.revisions.at.revisio',
    'giscedata_revisions/report/full_reparacio_at.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.revisions.full.reparacio_at_industria',
    'giscedata.revisions.at.revisio',
    'giscedata_revisions/report/full_reparacio_at.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.revisions.full.reparacio_at_intern',
    'giscedata.revisions.at.revisio',
    'giscedata_revisions/report/full_reparacio_at.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.revisions.ct.reconeixements.trieni',
    'giscedata.trieni',
    'giscedata_revisions/report/ct_reconeixements.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.revisions.ct.reconeixements.revisions',
    'giscedata.revisions.ct.revisio',
    'giscedata_revisions/report/ct_reconeixements.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.revisions.at.reconeixements.trieni',
    'giscedata.trieni',
    'giscedata_revisions/report/at_reconeixements.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.revisions.at.reconeixements.revisions',
    'giscedata.revisions.at.revisio',
    'giscedata_revisions/report/at_reconeixements.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.revisions.at.guia.verificacio.inspeccio.lat',
    'giscedata.revisions.at.revisio',
    'giscedata_revisions/report/guia_verificacio_inspeccio_lat.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.revisions.ct.guia.reconeixement.general',
    'giscedata.revisions.ct.revisio',
    'giscedata_revisions/report/guia_reconeixement_general.mako',
    parser=report_webkit_html
)

webkit_report.WebKitParser(
    'report.giscedata.revisions.ct.mesures.pic',
    'giscedata.revisions.ct.revisio',
    'giscedata_revisions/report/revisions_cts_mesura_pic.mako',
    parser=report_webkit_html
)
