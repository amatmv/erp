<%
    from datetime import datetime
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
    	<style type="text/css">
        ${css}
            *{
                font-family: "Liberation Sans"
            }
            body{
                font-size: 11px;
                margin-right: 50px;
                margin-left: 50px;
            }
            table{
                font-size: 10px;
                font-weight: bold;
                width: 100%;
                border-collapse: collapse;
                border: 1px solid black;
            }
            table.borderless{
                border: none;
            }
            table.centered{
                align-content: center;
                text-align: center;
            }
            table.half_righted{
                float: right;
                width: 50%;
            }
            th{
                background-color: #d9d9d9;
                border: 1px solid black;
                text-align: center;
                vertical-align: bottom;
                height: 100px;
                padding-bottom: 10px;
            }
            thead{
              display:table-header-group;
            }
            tbody{
              display:table-row-group;
            }
            tfoot{
              display:table-row-group;
            }
      			tr{
      				page-break-inside: avoid;
      			}
            .page-break { page-break-after:always;}
            #table_separator{
                border: 0px;
                font-weight: normal;
            }
            .capcalera{
                position: relative;
                top: 30px;
            }
            #contingut{
                position: relative;
                top: 50px;
            }
            .logo{
                position: relative;
                float: left;
                width: 50%;
            }
            .dades_full{
                position: relative;
                float: left;
                width: 50%;
                font-size: 8px;
                text-align: right;
            }
            .bar{
                position: relative;
                top: 40px;
                background-color: #808080;
                padding-left: 120px;
                color: white;
                font-weight: bold;
            }
            .titol{
                text-align: center;
                position: relative;
                top: 40px;
                font-weight: bold;
                font-size: 18px;
            }
            .principal{
                margin-top: 10px;
                font-size: 8px;
            }
            #peu{
                position: relative;
                margin-top: 15px;
            }
            #observacions{
                position: relative;
                float: left;
                width: 49%;
                height: 100px;
                margin-right: 5px;
            }
            #inspector{
                position: relative;
                float: left;
                width: 49%;
                height: 100px;
                margin-left: 5px;
                left: 4px;
            }
            #escrit{
                position: relative;
                top: 5px;
                font-size: 12px;
            }
            .punts{
                border-bottom: 1px dotted black;
            }
            .normalFont{
                font-weight: normal;
            }
            .inspeccio{
                background-color: #d9d9d9;
                border: 1px solid black;
                width: 110px;
                text-align: center;
            }
            .field_inspector{
                width: 50%;
                padding-left: 5px;
                border-left: 1px solid black;
                height: 43px;
            }
            .spaced{
                padding-top: 5px;
                padding-bottom: 5px;
            }
            .pagina_codi{
                position: relative;
                float: right;
                width: 51%;
                margin-top: 50px;
                margin-bottom: 10px;
            }
            .titol_taula{
                background-color: #d9d9d9;
                border: 1px solid black;
                text-align: center;
                height: 32px;
            }
            .titol_observacions{
                text-align: left;
                padding-left: 5px;
                height: 15px;
            }
            .rotated{
                text-align: center;
                vertical-align: middle;
                width: 15px;
                margin: 0px;
                padding: 0px;
                padding-left: 3px;
                padding-top: 3px;
                white-space: nowrap;
                -webkit-transform: rotate(-90deg);
                -moz-transform: rotate(-90deg);
                transform: rotate(-90deg);
            }
            .field{
                border: 1px solid black;
                padding-top: 5px;
                padding-bottom: 5px;
                padding-left: 7px;
                font-weight: normal;
            }
            .simple_border{
                border: 1px solid black;
            }
            .double_border{
                border: 1px solid black;
                border-right: 2px solid black;
            }
            .w_5{
                width: 5px;
            }
            .w_20{
                width: 20px;
            }
            .w_120{
                width: 120px;
            }
        </style>
    </head>
    <body>
      %for rev in objects:
        <table class="principal borderless normalFont">
            <thead>
                <tr>
                    <th colspan="16" style="background: none; border: none">
                    <table class="borderless centered half_righted">
                        <tr>
                            <td class="w_20 normalFont spaced">${_("FULL")}</td>
                            <td class="w_20 normalFont spaced"><div class="w_20 punts">&nbsp;</div></td>
                            <td class="w_20 normalFont spaced">${_("DE")}</td>
                            <td class="w_20 normalFont spaced"><div class="w_20 punts">&nbsp;</div></td>
                            <td class="w_20 spaced">&nbsp;</td>
                            <td class="w_120 normalFont spaced">${_("CODI DE LA INSPECCIÓ")}</td>
                            <td class="inspeccio normalFont spaced">${"{0}/{1}".format(rev.name.name, rev.trimestre.name)}</td>
                        </tr>
                    </table>
                    </th>
                </tr>
                <tr>
                    <th class="titol_taula double_border" colspan="4">&nbsp;</th>
                    <th class="titol_taula" colspan="11">${_("ELEMENTS COMPROVATS")}</th>
                    <th class="titol_taula"></th>
                </tr>
                <tr>
                    <th class="titol_taula">${_("NÚM. SUPORT")}</th>
                    <th> <div class="rotated">${_("CODI DEFECTE")}</div></th>
                    <th> <div class="rotated">${_("* ELEMENTS")}</div></th>
                    <th class="double_border"> <div class="rotated">${_("MESURA DE TERRA")}</div></th>
                    <th> <div class="rotated">${_("Conductors")}</div></th>
                    <th> <div class="rotated">${_("p.a.t.")}</div></th>
                    <th> <div class="rotated">${_("aïlladors")}</div></th>
                    <th> <div class="rotated">${_("suport")}</div></th>
                    <th> <div class="rotated">${_("Senyal de perill")}</div></th>
                    <th> <div class="rotated">${_("Vents")}</div></th>
                    <th> <div class="rotated">${_("Fonaments")}</div></th>
                    <th> <div class="rotated">${_("Encreuaments")}</div></th>
                    <th> <div class="rotated">${_("Distàncies")}</div></th>
                    <th> <div class="rotated">${_("Pas per zones")}</div></th>
                    <th> <div class="rotated">${_("Maniobra i protecció")}</div></th>
                    <th class="titol_taula">${_("OBSERVACIONS DEL DEFECTE")}</th>
                </tr>
            </thead>
            <tbody>
              <%
                suports = rev.name.suports
                defecte_list = rev.defectes_ids
              %>
              %for suport in suports:
                <%
                  mesura_list = suport.mesures
                  mesura = False
                  for m in mesura_list:
                    if m.trimestre == rev.trimestre.name:
                      mesura = m.ferramenta
                      break
                  elements = ""
                  if suport.autovalvules:
                    elements += "A"
                  if suport.seccionador:
                    if elements:
                      elements += ', '
                    elements += 'SC'
                  defectes = []
                  codis = ""
                  observ = ""
                  for d in defecte_list:
                    if d.suport.id == suport.id:
                      defectes.append(d)
                      if codis:
                        codis += '<br/>'
                        observ += '<br/>'
                      codis += d.defecte_id.name
                      if d.observacions:
                        observ += d.observacions
                      else:
                        observ += '-'
                %>
                <tr>
                %for elem in range(0,16):
                  %if elem == 0: # Num Suport
                    <td class="simple_border field">${suport.name}</td>
                  %elif elem == 1: # Codi defecte
                    <td class="simple_border field">${codis or '&nbsp;'}</td>
                  %elif elem == 2: # Elements
                    <td class="simple_border field">${elements or '&nbsp;'}</td>
                  %elif elem == 3: # Mesura Terra
                    <td class="double_border field">${mesura or '&nbsp;'}</td>
                  %elif elem == 15: # Observacions
                    <td class="simple_border field">${observ or '&nbsp;'}</td>
                  %else:
                    <td class="simple_border field">&#x2714;</td>
                  %endif
                %endfor
              </tr>
            %endfor
            </tbody>
            <tfoot>
                <tr>
                  <td colspan="16">
                    <div id="peu">
                      <table id="observacions">
                        <tr>
                          <td class="titol_taula titol_observacions" colspan="3">${_("OBSERVACIONS DE LA INSPECCIÓ")}</td>
                        </tr>
                        %for elem in range(0, 4):
                        <tr>
                          <td class="w_5"></td>
                          <td class="punts">&nbsp;</td>
                          <td class="w_5"></td>
                        </tr>
                        %endfor:
                        <tr>
                          <td style="height: 10px;" colspan="3"></td>
                        </tr>
                      </table>
                      <table id="inspector">
                        <tr>
                          <td class="titol_taula titol_observacions" colspan="2">${_("INSPECTOR")}</td>
                        </tr>
                        <tr>
                          <td class="field_inspector">${_("Nom: {0}".format(rev.tecnic.name or ''))}</td>
                          <td class="field_inspector">${_("Signatura:")}</td>
                        </tr>
                        <tr>
                          <%
                          if rev.data:
                              data_obj = datetime.strptime(rev.data, '%Y-%m-%d %H:%M:%S')
                              data = data_obj.strftime('%d/%m/%Y')
                              hora = data_obj.strftime('%H:%M:%S')
                          else:
                              data = ''
                              hora = ''
                          %>
                          <td class="field_inspector" style="vertical-align: top;">${_("Data: {0}".format(data))}</td>
                          <td class="field_inspector"></td>
                        </tr>
                      </table>
                    </div>
                    <div id="escrit">
                      ELEMENTS: A= Autovàlvules, SB= Seccionador buit, SC= Seccionador càrrega, I= Interruptor, C= Conversió Subt.<br>
                      Les caselles dels elements comprovats es marcaràn amb un símbol de ✓ una vegada feta la seva comprovació.
                    </div>
                  </td>
                </tr>
            </tfoot>
        </table>
        %if rev != objects[-1]:
          <p style="page-break-after:always;"></p>
        %endif
      %endfor
    </body>
</html>
