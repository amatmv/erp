<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="revisio-list"/>
  </xsl:template>

  <xsl:template match="revisio-list">
    <document>
      <template pageSize="(19cm,297mm)" leftMargin="3cm" rightMargin="3cm" showBoundary="0">
        <pageTemplate id="main">
          <frame id="body" x1="1cm" y1="5mm" width="175mm" height="287mm" />

          <pageGraphics>
            <translate dx="-168mm" dy="170mm" />
            <rotate degrees="-90" />
            <setFont name="Helvetica" size="7" />
            <drawString x="0mm"
            y="170mm">Format doc. 13.3-11_07 / Exemplar per l'empresa</drawString>
          </pageGraphics>

        </pageTemplate>
      </template>
      
      <stylesheet>

        <paraStyle name="text"
					fontName="Helvetica"
					fontSize="9"
          alignment="justify"
				/>

        <blockTableStyle id="taula_logos">
          <blockAlignment value="LEFT" start="0,0" stop="0,0" />
          <blockAlignment value="RIGHT" start="1,0" stop="1,0" />
        </blockTableStyle>

        <blockTableStyle id="taula_titol">
          <blockAlignment value="CENTER" />
          <blockTopPadding length="0" />
          <blockLeftPadding length="0" />
          <blockRightPadding length="0" />
          <blockBottomPadding length="0" />
          <blockBackground colorName="silver" />
          <lineStyle kind="BOX" colorName="silver" />
          <blockFont name="Helvetica-Bold" size="10" />
          <blockLeading length="4" start="0,0" stop="0,0" />
          <blockLeading length="4" start="0,2" stop="0,2" />
        </blockTableStyle>

        <blockTableStyle id="taula_contingut">
          <lineStyle kind="LINEBELOW" colorName="silver" start="0,0" stop="1,0" />
          <blockFont name="Helvetica" size="9" />
          <blockLeading length="7" />
          
          <blockLeading length="0.000001" start="0,0" stop="1,0" />
          <blockLeading length="20" start="0,8" stop="1,8" />

          <blockLeading length="20" start="0,17" stop="1,17" />
          <lineStyle kind="LINEBELOW" colorName="silver" start="0,18" stop="1,18" />
          <blockFont name="Helvetica-Bold" start="0,18" stop="1,18" />
          <blockLeading length="10" start="0,18" stop="1,18" />

          <blockLeading length="20" start="0,21" stop="1,21" />
          <lineStyle kind="LINEBELOW" colorName="silver" start="0,22" stop="1,22" />
          <blockFont name="Helvetica-Bold" start="0,22" stop="1,22" />
          <blockLeading length="10" start="0,22" stop="1,22" />
          
          <blockLeading length="20" start="0,29" stop="1,29" />
          <lineStyle kind="LINEBELOW" colorName="silver" start="0,30" stop="1,30" />
          <blockFont name="Helvetica-Bold" start="0,30" stop="1,30" />
          <blockLeading length="10" start="0,30" stop="1,30" />
        </blockTableStyle>

        <blockTableStyle id="taula_firma">
          <blockFont name="Helvetica" size="9" />
          <blockAlignment value="RIGHT" start="1,0" stop="1,0" />
        </blockTableStyle>

        <blockTableStyle id="cap_taula_defectes">
          <blockFont name="Helvetica-Bold" size="10" />
          <lineStyle kind="BOX" colorName="silver" />
          <blockAlignment value="CENTER" />
        </blockTableStyle>
        
        <blockTableStyle id="taula_defectes">
          <blockFont name="Helvetica" size="8" />
          <blockFont name="Helvetica-Bold" start="0,0" stop="6,0" />
          <lineStyle kind="BOX" colorName="silver" />
          <blockAlignment value="CENTER" />
        </blockTableStyle>

        <blockTableStyle id="taula_tecnic">
          <blockFont name="Helvetica" size="10" />
          <lineStyle kind="BOX" colorName="silver" />
          <blockFont name="Helvetica-Bold" start="0,1" stop="3,1" />
        </blockTableStyle>

      </stylesheet>
    
      <story>
        <xsl:apply-templates select="revisio" mode="story" />
      </story>
    </document>
  </xsl:template>

  <xsl:template match="revisio" mode="story">
    <blockTable colWidths="100mm, 75mm" style="taula_logos">
      <tr>
        <td><image file="addons/giscedata_revisions/report/gisce.tif"
        width="56mm" height="17mm" /></td>
        <td><image file="addons/giscedata_revisions/report/enac.tif"
        width="22mm" height="17mm" /></td>
      </tr>
    </blockTable>
    <spacer length="10" />
    <blockTable colWidths="175mm" style="taula_titol">
      <tr><td></td></tr>
      <tr t="1">
        <td t="1">ACTA DE RECONEIXEMENT PERI�DIC</td>
      </tr>
      <tr t="1">
        <td t="1">INSTAL�LACI� EL�CTRICA DE SERVEI P�BLIC</td>
      </tr>
      <tr><td></td></tr>
    </blockTable>
    <spacer length="6" />
    <para alignment="center" fontName="Helvetica-Bold">L�nies d'Alta Tensi�</para>
    <blockTable colWidths="70mm,105mm" style="taula_contingut">
      <tr><td></td><td></td></tr>
      <tr t="1">
        <td t="1">Codi de document</td>
        <td>A-AT-<xsl:value-of select="concat(linia/codi, ' ')" /><xsl:value-of select="trimestre" /></td>
      </tr>
      <tr t="1">
        <td t="1">Codi de revisi�</td>
        <td><xsl:value-of select="concat(linia/codi, ' ')" /><xsl:value-of select="trimestre" /></td>
      </tr>
      <tr t="1">
        <td t="1">Expedient autoritizaci� administrativa</td>
        <td><xsl:value-of select="linia/expedients" /></td>
      </tr>
      <tr t="1">
        <td t="1">Data de revisi�</td>
        <td><xsl:value-of select="concat(substring(data, 9, 2), '/', substring(data, 6, 2), '/', substring(data, 1, 4))" /></td>
      </tr>
      <tr t="1">
        <td t="1">Unitat de reconeixement</td>
        <td>LAT-<xsl:value-of select="linia/codi" /></td>
      </tr>
      <tr t="1">
        <td t="1">Tipus d'instal�laci�</td>
        <td t="1">L�nia a�ria d'alta tensi�</td>
      </tr>
      <tr t="1">
        <td t="1">Empresa titular</td>
        <td><xsl:value-of select="linia/propietari/nom" /></td>
      </tr>
      <tr t="1">
        <td t="1">Adre�a per a notificacions</td>
        <td><xsl:value-of select="concat(linia/propietari/adreca/carrer, ' ', linia/propietari/adreca/cp, ' - ', linia/propietari/adreca/ciutat)" /></td>
      </tr>
      <tr t="1">
        <td t="1">Organisme d'inspecci�</td>
        <td><xsl:value-of select="tecnic/organisme" /></td>
      </tr>
      <tr t="1">
        <td t="1">Adre�a Organisme d'inspecci�</td>
        <td><xsl:value-of select="tecnic/adreca" /></td>
      </tr>
      <tr t="1">
        <td t="1">Nom  cognoms del titulat</td>
        <td><xsl:value-of select="tecnic/nom" /></td>
      </tr>
      <tr t="1">
        <td t="1">DNI</td>
        <td><xsl:value-of select="tecnic/dni" /></td>
      </tr>
      <tr t="1">
        <td t="1">Titulaci�</td>
        <td><xsl:value-of select="tecnic/titolacio" /></td>
      </tr>
      <tr t="1">
        <td t="1">N�mero de col�legiat</td>
        <td><xsl:value-of select="tecnic/ncol" /></td>
      </tr>
      <tr t="1">
        <td t="1">Adre�a per a notificacions</td>
        <td><xsl:value-of select="tecnic/adreca" /></td>
      </tr>
      <tr t="1">
        <td t="1">Tel�fon</td>
        <td><xsl:value-of select="tecnic/telefon" /></td>
      </tr>
      <tr t="1">
        <td t="1">Correu electr�nic</td>
        <td><xsl:value-of select="tecnic/email" /></td>
      </tr>
      <tr t="1">
        <td t="1">Dades de la instal�laci�</td>
        <td></td>
      </tr>
      <tr t="1">
        <td t="1">Instal�laci�</td>
        <td t="1">L�nia AT <xsl:value-of select="linia/codi" /></td>
      </tr>
      <tr t="1">
        <td t="1">Empla�ament</td>
        <td><xsl:value-of select="linia/descripcio" /></td>
      </tr>
      <tr t="1">
        <td t="1">Municipi</td>
        <td><xsl:value-of select="linia/municipi" /></td>
      </tr>
      <tr t="1">
        <td t="1">Caracter�stiques t�cniques principals</td>
        <td></td>
      </tr>
      <tr t="1">
        <td t="1">Origen de la l�nia</td>
        <td><xsl:value-of select="linia/origen" /></td>
      </tr>
      <tr t="1">
        <td t="1">Final de la l�nia</td>
        <td><xsl:value-of select="linia/final" /></td>
      </tr>
      <tr t="1">
        <td t="1">Tensi� de servei</td>
        <td><xsl:value-of select="concat(linia/tensio, ' V')" /></td>
      </tr>
      <tr t="1">
        <td t="1">Secci� conductors</td>
        <td><xsl:value-of select="linia/seccio_conductors" /></td>
      </tr>
      <tr t="1">
        <td t="1">Longitud A�ria</td>
        <td>
          <xsl:choose>
            <xsl:when test="linia/aeri_op&gt;0">
              <xsl:value-of select="concat(format-number(linia/aeri_op, '###,###.##'), ' m')" />
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="concat(format-number(linia/aeri_cad, '###,###.##'), ' m')" />
            </xsl:otherwise>
          </xsl:choose>
        </td>
      </tr>
      <tr t="1">
        <td t="1">Longitud Subterr�nia</td>
        <td>
          <xsl:choose>
            <xsl:when test="linia/subt_op&gt;0.0">
              <xsl:value-of select="concat(format-number(linia/subt_op, '###,###.##'), ' m')" />
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="concat(format-number(linia/subt_cad, '###,###.##'), ' m')" />
            </xsl:otherwise>
          </xsl:choose>
        </td>
      </tr>
      <tr t="1">
        <td t="1">Suports</td>
        <td t="1">Met�l�lics: <xsl:value-of select="linia/n_sup_metal"/>; Formig�: <xsl:value-of select="linia/n_sup_for" />; Fusta: <xsl:value-of select="linia/n_sup_fus" /></td>
      </tr>
      <tr t="1">
        <td t="1">Acta</td>
        <td></td>
      </tr>
    </blockTable>
    <para style="text"><xsl:value-of select="tecnic/nom" />, enginyer competent que ha realitzat la inspecci� de la instal�laci� el�ctrica de refer�ncia, d'acord amb l'article 7 del Decret 328/2001, de 4 de desembre, pel qual s'etableix el procediment aplicable per efectuar els reconeixements peri�dics de les instal�lacions de produccio�, transformaci�, transport i distribuci� d'energia el�ctrica, publicat en el DOGC n�3.536,
    </para>
    <spacer length="15" />
    <para style="text" t="1"><b>FA CONSTAR:</b></para>
    <para style="text" t="1">
      <tr t="1">Que la instal�laci� t� defectes i s'adjunta a aquesta acta una relaci� de defectes d'acord amb la legislaci� vigent segons el Decret 328/2001</tr>
    </para>
    <spacer length="10" />
    <para style="text" t="1">
      <tr t="1">Observacions i explicaci� d'accions no realitzades: </tr><xsl:if test="observacions != '' and observacions != '0'"><xsl:value-of select="observacions" /></xsl:if>
    </para>
    <spacer length="40" />
    <blockTable style="taula_firma" colWidths="70mm,105mm">
      <tr t="1">
        <td t="1">Signatura</td><td t="1">Girona, a <xsl:value-of select="concat(substring(data, 9, 2), '/', substring(data, 6, 2), '/', substring(data, 1, 4))" /></td>
      </tr>
    </blockTable>
    <para style="text" t="1">
      <tr t="1">Aquest document queda a disposici� de l'organisme competent del Departament d'Ind�stria, Comer� i Turisme</tr>
    </para>
    <nextPage />
    <xsl:apply-templates select="defectes" mode="story" />
  </xsl:template>

  <xsl:template match="defectes" mode="story">
    <blockTable colWidths="100mm, 75mm" style="taula_logos">
      <tr>
        <td><image file="addons/giscedata_revisions/report/gisce.tif"
        width="56mm" height="17mm" /></td>
        <td><image file="addons/giscedata_revisions/report/enac.tif"
        width="22mm" height="17mm" /></td>
      </tr>
    </blockTable>
    <spacer length="10" />
    <blockTable colWidths="175mm" style="taula_titol">
      <tr><td></td></tr>
      <tr t="1">
        <td t="1">ACTA DE RECONEIXEMENT PERI�DIC</td>
      </tr>
      <tr t="1">
        <td t="1">INSTAL�LACI� EL�CTRICA DE SERVEI P�BLIC</td>
      </tr>
      <tr><td></td></tr>
    </blockTable>
    <spacer length="6" />
    <para alignment="center" fontName="Helvetica-Bold">L�nies d'Alta Tensi�</para>
    <spacer length="10" />
    <blockTable style="cap_taula_defectes" colWidths="175mm">
      <tr t="1"><td t="1">DEFECTES A LES L�NIES D'ALTA TENSI� - L�NIA AT <xsl:value-of select="linia/codi" /></td></tr>
    </blockTable>
    <blockTable style="taula_defectes" colWidths="0.8cm,1.5cm,3cm,1.2cm,6cm,2.5cm,2.5cm" repeatRows="0">
      <tr t="1">
        <td t="1">Codi</td>
        <td t="1">Suport</td>
        <td t="1">Situaci� l�nia</td>
        <td t="1">Tipus</td>
        <td t="1">Descripci�</td>
        <td t="1">Valoraci�</td>
        <td t="1">L�mit reparaci�</td>
      </tr>
      <xsl:apply-templates select="defecte" mode="story" />
    </blockTable>
    <blockTable style="taula_tecnic" colWidths="5cm, 5cm, 3cm, 4.5cm">
      <tr t="1">
        <td t="1">T�cnic:</td>
        <td t="1">Titulaci�:</td>
        <td t="1">Col�legiat:</td>
        <td t="1">Data de reconeixement:</td>
      </tr>
      <tr>
        <td><xsl:value-of select="../tecnic/nom" /></td>
        <td><xsl:value-of select="../tecnic/titolacio" /></td>
        <td><xsl:value-of select="../tecnic/ncol" /></td>
        <td><xsl:value-of select="concat(substring(../data, 9, 2), '/', substring(../data, 6, 2), '/', substring(../data, 1, 4))" /></td>
      </tr>
    </blockTable>
    <setNextTemplate name="main" />
  </xsl:template>

  <xsl:template match="defecte" mode="story">
    <xsl:if test="intern!=1">
    <tr>
      <td><xsl:value-of select="codi" /></td>
      <td><xsl:value-of select="suport" /></td>
      <td><para style="text" fontSize="8"><xsl:value-of select="/revisio-list/revisio/linia/descripcio" /></para></td>
      <td><xsl:value-of select="tipus" /></td>
      <xsl:choose>
        <xsl:when test="string-length(descripcio) * 0.14 &gt; 11">
          <td><para style="text" alignment="left" fontSize="8"><xsl:value-of select="concat(substring(descripcio, 1, (7 div 0.14)-3), '...')" /></para></td>
        </xsl:when>
        <xsl:otherwise>
          <td><para style="text" fontSize="8" alignment="left"><xsl:value-of select="descripcio" /></para></td>
        </xsl:otherwise>
      </xsl:choose>
      <td><para style="text" alignment="left" fontSize="8"><xsl:value-of select="valoracio" /></para></td>
      <td><xsl:value-of select="concat(substring(data_rep, 9, 2), '/', substring(data_rep, 6, 2), '/', substring(data_rep, 1, 4))" /></td>
    </tr>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td><para fontName="Helvetica" fontSize="8"><xsl:value-of select="observacions" /></para></td>
      <td></td>
      <td></td>
    </tr>
    </xsl:if>
  </xsl:template>

</xsl:stylesheet>
