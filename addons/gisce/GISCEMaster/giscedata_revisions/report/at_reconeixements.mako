## coding=utf-8
<%
    from datetime import datetime,date
    from dateutil.relativedelta import relativedelta

    def obtenir_trimestres(objectes):
        if "trieni" not in origin_access:
            return filtrar_per_trimestres(objectes)
        return objectes

    def filtrar_per_trimestres(objectes):
        dic_trimestres = {}
        for rev in objectes:
            if not rev.trimestre.name in dic_trimestres.keys():
                dic_trimestres.update({rev.trimestre.name : [rev]})
            else:
                dic_trimestres[rev.trimestre.name].append(rev)
        return dic_trimestres

    def calcul_circuits(trams):
        result = 0
        for tram in trams:
            if tram.tipus == 1 and not tram.baixa:
                result += (tram.longitud_cad/tram.circuits)
        return result

    def formatFloat(value, decimals = 2, sep = ","):
        return "%s%s%0*u" % (int(value), sep, decimals, (10 ** decimals) * (value - int(value)))
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<head>
    <style type="text/css">
    ${css}
    </style>
    <link rel="stylesheet" href="${addons_path}/giscedata_revisions/report/stylesheet_reconeixements.css"/>
    <link rel="stylesheet" href="${addons_path}/giscedata_revisions/report/at_reconeixements.css"/>
</head>
<body>
    <%
        llista_trimestres = obtenir_trimestres(objects)
        if "trieni" not in origin_access:
            diccionari_trimestres = llista_trimestres
            llista_trimestres = sorted(llista_trimestres.keys())
    %>
    %for trieni in llista_trimestres:
        <div id="logo">
              <img id="logo_img" src="${addons_path}/giscedata_revisions/report/gisce_logo.png">
        </div>
        <div id="separador"></div>
        <div id="capcalera1">
            <h1>${_("RECONEIXEMENTS LAT")}</h1>
            <h2>${company.name}</h2>
        </div>
        <br><br><br><br>
        %if "trieni" not in origin_access:
            <h2 style="margin-top: -15px">${_("Trimestre")}  ${trieni}</h2>
        %else:
            <h2 style="margin-top: -15px">${_("Trimestre")}  ${trieni.name}</h2>
        %endif

        <div id="separador2"></div>
        <div>
            <p class="bold fs12px">${_("Línies alta tensió:")}</p>
            <%
                ats = []
                if "trieni" not in origin_access:
                    ats = diccionari_trimestres.get(trieni)
                else:
                    ats = trieni.lat
                ats = sorted(ats, key=lambda elem: elem.name.name)
                longituds_revisades = 0
                longituds_aeries = 0
                longituds_subt = 0
                longituds_aeries_circuits = 0
                longituds_aeries_2_circuits = 0

                date_format = "%Y-%m-%d"
                if "trieni" not in origin_access:
                    data_trimestre = datetime.strptime(diccionari_trimestres.get(trieni)[0].trimestre.data_inici, date_format)
                else:
                    data_trimestre = datetime.strptime(trieni.data_inici, date_format)

                length_summary = {
                    '1_circ': {'planned': 0, 'reviewed' : 0, 'str': 'LAT 1 circuits'},
                    '2_circ': {'planned': 0, 'reviewed' : 0, 'str': 'LAT 2 circuits'},
                    'subt': {'planned': 0, 'reviewed' : 0, 'str': 'LAT subterrània'},
                }
            %>
            <table class="table data-table table-intercoloured-rows">
                <thead>
                    <tr>
                        <th>${_("Codi")}</th>
                        <th>${_("Origen")}</th>
                        <th>${_("Final")}</th>
                        <th>${_("Aeri")}</th>
                        <th>${_("Aeri circ.")}</th>
                        <th>${_("Aeri 2 circ.")}</th>
                        <th>${_("Subt.")}</th>
                        <th>${_("Tensió")}</th>
                        <th>${_("Data rev.")}</th>
                        <th>${_("Observacions")}</th>
                    </tr>
                </thead>
                <tbody>
                %for rev_at in ats:
                    <%
                        longituds_aeries += rev_at.name.longitud_aeria_cad
                        longituds_subt += rev_at.name.longitud_sub_cad

                        aeri = 0 if rev_at.name.longitud_aeria_cad == 0 else rev_at.name.longitud_aeria_cad
                        aeri_calc = calcul_circuits(rev_at.name.trams)
                        aeri_circ = 0 if aeri_calc == 0 else aeri_calc
                        aeri_2_circ = aeri - aeri_circ
                        aeri_1_circ = aeri_circ - aeri_2_circ
                        subt = 0 if rev_at.name.longitud_sub_cad == 0 else rev_at.name.longitud_sub_cad

                        longituds_aeries_circuits += aeri_calc
                        longituds_aeries_2_circuits += aeri_2_circ

                        length_summary['1_circ']['planned'] += aeri_1_circ
                        length_summary['2_circ']['planned'] += aeri_2_circ
                        length_summary['subt']['planned'] += subt

                        data_rev = ""
                        if rev_at.data:
                            data_revisio = datetime.strptime(rev_at.data, '%Y-%m-%d %H:%M:%S')
                            data_rev = data_revisio.strftime('%d/%m/%Y')
                            aeri_circuit_revisat = calcul_circuits(rev_at.name.trams)
                            longituds_revisades += aeri_circuit_revisat
                            length_summary['1_circ']['reviewed'] += aeri_1_circ
                            length_summary['2_circ']['reviewed'] += aeri_2_circ
                    %>
                    <tr>
                        <td class="col_codi_at">${rev_at.name.name}</td>
                        <td>${rev_at.name.origen}</td>
                        <td>${rev_at.name.final}</td>
                        <td class="right">${formatFloat(aeri)}</td>
                        <td class="right">${formatFloat(aeri_circ)}</td>
                        <td class="right">${formatFloat(aeri_2_circ)}</td>
                        <td class="right">${formatFloat(subt)}</td>
                        <td class="right">${rev_at.name.tensio}</td>
                        <td class="col_data">${data_rev}</td>
                        <td class="col_observacions">&nbsp</td>
                    </tr>
                %endfor
                <tr class="fila_totals">
                    <td colspan="2">&nbsp</td>
                    <td class="bold">${_("Totals:")}</td>
                    <td class="bold right">${formatFloat(longituds_aeries)}</td>
                    <td class="bold right">${formatFloat(longituds_aeries_circuits)}</td>
                    <td class="bold right">${formatFloat(longituds_aeries_2_circuits)}</td>
                    <td class="bold right">${formatFloat(longituds_subt)}</td>
                    <td class="bold right" colspan="2">&nbsp</td>
                </tr>
                </tbody>
            </table>
        </div>
        <p class="bold fs12px pt30px">${_("Resum linies revisades:")}</p>
        <div id="div-1st-summary">
            <table id="table-resum-at" class="table table-intercoloured-rows">
                <thead>
                    <tr>
                        <th class="llista_head">${_("Descripció")}</th>
                        <th class="llista_head">${_("Totals")}</th>
                    </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="col_instalacio">${_("Total aeri:")}</td>
                    <td class="col_instalacio right">${formatFloat(longituds_aeries)}</td>
                </tr>
                <tr>
                    <td class="col_instalacio">${_("Total aeri circuits:")}</td>
                    <td class="col_instalacio right">${formatFloat(longituds_aeries_circuits)}</td>
                </tr>
                <tr>
                    <td class="col_instalacio">${_("Total aeri revisat:")}</td>
                    <td class="col_instalacio right">${formatFloat(longituds_revisades)}</td>
                </tr>
                <tr>
                    <td class="col_instalacio">${_("Total aeri pendent:")}</td>
                    <%
                        # Calcul amb CFO # pendent = longituds_aeries_circuits - longituds_revisades - longitud_aeries_cfo
                        pendent = longituds_aeries_circuits - longituds_revisades
                    %>
                    <td class="col_instalacio right">${0 if pendent == 0 else formatFloat(pendent)}</td>
                </tr>
                </tbody>
            </table>
        </div>
        <div id="div-2nd-summary">
            <table class="table-intercoloured-rows table">
                <colgroup>
                    <col id="col-inst">
                    <col id="col-prev">
                    <col id="col-revi">
                </colgroup>
                <thead>
                    <tr>
                        <th>${_("Instal·lació")}</th>
                        <th>${_("Previstes")} (km)</th>
                        <th>${_("Revisades")} (km)</th>
                    </tr>
                </thead>
                <tbody>
                    %for row in ['1_circ', '2_circ', 'subt']:
                        <tr>
                            <td>${_(length_summary[row]['str'])}</td>
                            %for col in ['planned', 'reviewed']:
                                <td class="right">${formatFloat(length_summary[row][col])}</td>
                            %endfor
                        </tr>
                    %endfor
                </tbody>
            </table>
        </div>
        %if trieni != llista_trimestres[-1]:
            <p style="page-break-after:always;"></p>
        %endif
    %endfor
</body>
</html>