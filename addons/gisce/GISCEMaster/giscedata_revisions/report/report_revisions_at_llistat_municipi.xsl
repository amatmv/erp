<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

  <xsl:key name="revisions-per-municipi" match="revisio" use="municipi" />

  <xsl:template match="/">
    <xsl:apply-templates select="revisio-list" />
  </xsl:template>

  <xsl:template match="revisio-list">
    <document>
      <template>
        <pageTemplate id="main">
          <frame id="first" x1="1cm" y1="1cm" width="19cm" height="277mm"/>
        </pageTemplate>
      </template>
      
      <stylesheet>

        <paraStyle name="titol"
	  fontName="Helvetica"
	  fontSize="16"
          leading="32"
				/>

        <paraStyle name="titol2"
	  fontName="Helvetica"
	  fontSize="10"
          leading="20"
				/>
				
	<paraStyle name="text"
	  fontName="Helvetica"
	  fontSize="8"
	/>

        <blockTableStyle id="taula_contingut">
          <lineStyle kind="GRID" colorName="silver"/>
          <blockFont name="Helvetica" size="8" />
	  <blockBackground colorName="grey" start="0,0" stop="7,0" />
	  <blockFont name="Helvetica-Bold" size="8" start="0,0" stop="7,0" />
          <blockAlignment value="RIGHT" start="3,1" />
          <blockAlignment value="RIGHT" start="4,1" />
        </blockTableStyle>
        
        <blockTableStyle id="taula2">
          <blockFont name="Helvetica-Bold" size="8" />
          <blockAlignment value="RIGHT" start="3,0" />
          <blockAlignment value="RIGHT" start="4,0" />
        </blockTableStyle>

      </stylesheet>
    
      <story>
      <xsl:for-each select="revisio[count(. | key('revisions-per-municipi', municipi)[1]) = 1]">
		<xsl:sort select="municipi" />
		<xsl:variable name="municipi" select="municipi" />
		<para style="titol" t="1"><tr t="1">L�nies AT per reconeixements</tr></para>
      		<para style="titol2" t="1"><tr t="1">Trimestre: </tr> <xsl:value-of select="trimestre" /></para>
                <para style="titol2" t="1"><tr t="1">Municipi:</tr> <xsl:value-of select="municipi" /></para>

                <blockTable style="taula_contingut" colWidths="1.3cm,5cm,5cm,1.5cm,1.5cm,1.5cm,2cm,3cm">
		       <tr t="1">
		          <td t="1">Codi</td>
		          <td t="1">Origen</td>
		          <td t="1">Final</td>
		          <td t="1">Aeri</td>
		          <td t="1">Subt.</td>
		          <td t="1">Tensi�</td>
		          <td t="1">Data rev.</td>
		          <td t="1">Observacions</td>
		        </tr>
		<xsl:for-each select="key('revisions-per-municipi', municipi)">
		  <xsl:apply-templates select="linia" mode="story">
		    <xsl:sort select="codi" data-type="number" />
                  </xsl:apply-templates>
		</xsl:for-each>
		</blockTable>
		<blockTable style="taula2" colWidths="1.3cm,5cm,5cm,1.5cm,1.5cm,1.5cm,2cm,3cm">
		        <tr t="1">
		          <td></td>
		          <td></td>
		          <td t="1">Totals:</td>
		          <td><xsl:value-of select="format-number(sum(//revisio[municipi=$municipi]/linia/aeri), '#.##')" /></td>
		          <td><xsl:value-of select="format-number(sum(//revisio[municipi=$municipi]/linia/subt), '#.##')" /></td>
		          <td></td>
		          <td></td>
		          <td></td>
		        </tr>
      		</blockTable>
		<nextPage />
	</xsl:for-each>


        
      
      </story>
    </document>
  </xsl:template>
  
  <xsl:template match="linia" mode="story">
    <tr>
      <td><xsl:value-of select="codi"/></td>
      <td><para style="text"><xsl:value-of select="origen"/></para></td>
      <td><para style="text"><xsl:value-of select="final"/></para></td>
      <td align="right"><xsl:value-of select="format-number(aeri, '#.##')"/></td>
      <td align="rigth"><xsl:value-of select="format-number(subt, '#.##')"/></td>
      <td><xsl:value-of select="tensio"/></td>
      <td><xsl:if test="data!=''"><xsl:value-of select="concat(substring(data, 9, 2), '/', substring(data, 6, 2), '/', substring(data, 1, 4))"/></xsl:if></td>
      <td><para style="text"><xsl:value-of select="observacions"/></para></td>
    </tr>
  </xsl:template>
</xsl:stylesheet>
