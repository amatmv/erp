<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="revisio-list" />
  </xsl:template>

  <xsl:template match="revisio-list">
    <document>
      <template>
        <pageTemplate id="main">
          <frame id="first" x1="1cm" y1="1cm" width="19cm" height="277mm"/>
        </pageTemplate>
      </template>
      
      <stylesheet>

        <paraStyle name="titol"
					fontName="Helvetica"
					fontSize="16"
          leading="32"
				/>

        <paraStyle name="titol2"
					fontName="Helvetica"
					fontSize="10"
          leading="20"
				/>
				
		<paraStyle name="text"
			fontName="Helvetica"
			fontSize="8"
		/>

        <blockTableStyle id="taula_contingut">
          <lineStyle kind="GRID" colorName="silver"/>
          <blockFont name="Helvetica" size="8" />
		  <blockBackground colorName="grey" start="0,0" stop="-1,0" />
		  <blockFont name="Helvetica-Bold" size="8" start="0,0" stop="-1,0" />
		  <blockAlignment value="LEFT" />
        </blockTableStyle>

      </stylesheet>
    
      <story>
      <para style="titol" t="1"><tr t="1">CT's per reconeixements</tr></para>
      <para style="titol2">Trimestre: <xsl:value-of select="revisio/trimestre" /></para>

      <blockTable style="taula_contingut" colWidths="3cm,7cm,1.5cm,1.8cm,1.8cm,4cm" repeatRows="1">
       <tr t="1">
          <td t="1">Codi</td>
          <td t="1">Descripci�</td>
          <td t="1">Tipus</td>
          <td t="1">Data rev.</td>
          <td t="1">IND�STRIA</td>
          <td t="1">Observacions</td>
        </tr>
        <xsl:apply-templates select="revisio" mode="story">
          <xsl:sort select="../../data" order="ascending" />
          <xsl:sort select="ct/codi" data-type="number" />
        </xsl:apply-templates>
      </blockTable>
      
      <spacer length="25" />
      <para style="titol2" t="1"><tr t="1">Resum Instal�lacions revisades:</tr></para>
      
      <blockTable style="taula_contingut">
      	<tr t="1">
      		<td t="1">Instal�laci�</td>
      		<td t="1">Quantitat</td>
      	</tr>
      	<tr>
      		<td>CH</td><td><xsl:value-of select="count(revisio/ct[tipus='CH' and ../data!=''])" /></td>
      	</tr>
      	<tr>
      		<td>ET</td><td><xsl:value-of select="count(revisio/ct[tipus='CT' and tipus2=2 and ../data!=''])" /></td>
      	</tr>
      	<tr>
      		<td>PT</td><td><xsl:value-of select="count(revisio/ct[tipus='CT' and tipus2=1 and ../data!=''])" /></td>
      	</tr>
      	<tr>
      		<td>CR</td><td><xsl:value-of select="count(revisio/ct[tipus='CR' and ../data!=''])" /></td>
      	</tr>
      	<tr>
      		<td>SE</td><td><xsl:value-of select="count(revisio/ct[tipus='SE' and ../data!=''])" /></td>
      	</tr>
      </blockTable>
      
      </story>
    </document>
  </xsl:template>
  <xsl:template match="revisio" mode="story">
    <tr>
      <td><xsl:value-of select="ct/codi"/></td>
      <td><para style="text"><xsl:value-of select="ct/descripcio"/></para></td>
      <xsl:choose>
        <xsl:when test="ct/tipus = 'CT' and ct/tipus2 = 1">
      <td>PT</td>
        </xsl:when>
        <xsl:when test="ct/tipus = 'CT' and ct/tipus2 = 2">
      <td>ET</td>
        </xsl:when>
        <xsl:otherwise>
      <td><xsl:value-of select="ct/tipus" /></td>
      	</xsl:otherwise>
      </xsl:choose>
      <td><xsl:if test="data!=''"><xsl:value-of select="concat(substring(data, 9, 2), '/', substring(data, 6, 2), '/', substring(data, 1, 4))"/></xsl:if></td>
      <td><xsl:if test="ct/data_cfo!=''"><xsl:value-of select="concat(substring(ct/data_cfo, 9, 2), '/', substring(ct/data_cfo, 6, 2), '/', substring(ct/data_cfo, 1, 4))"/></xsl:if></td>
      <td>
      <xsl:if test="substring-after(trimestre, '/') - 7 &lt; substring(ct/data_cfo, 1, 4)"><xsl:text>CFO </xsl:text></xsl:if>
      <xsl:if test="ct/ct_obres = 1"><xsl:text>OBRES </xsl:text></xsl:if>
      <xsl:if test="ct/ct_baixa = 1"><xsl:text>BAIXA </xsl:text></xsl:if>
      </td>
    </tr>
  </xsl:template>
</xsl:stylesheet>
