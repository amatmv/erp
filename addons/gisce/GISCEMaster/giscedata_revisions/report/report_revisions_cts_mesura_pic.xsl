<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="revisio-list"/>
  </xsl:template>

  <xsl:template match="revisio-list">
    <document>
      <template pageSize="(21cm, 29.7cm)">
        <pageTemplate id="main">
          <pageGraphics>
          	<image file="addons/giscedata_revisions/report/gisce.tif" x="1cm" y="28cm" width="2.3cm" height="0.8cm" />
          	<place x="3.3cm" y="27cm" width="16.7cm" height="2cm">
          		<para style="textmini" alignment="right">13.3-38</para>
          		<para style="textmini" alignment="right">Full Presa de dades de Mesura de P.I.C</para>
          		<para style="textmini" alignment="right">Revisi� 2</para>
          		<para style="textmini" alignment="right">Full 1 de 1</para>
          	</place>
          	<place x="1cm" y="25cm" width="19cm" height="2cm">
          		<para style="text" alignment="center" backColor="grey" textColor="white">13.3-38</para>
          		<para style="text" fontSize="10" alignment="center" t="1">FULL PRESA DE DADES DE MESURA DE P.I.C</para>
          	</place>
          </pageGraphics>
          <frame id="first" x1="1cm" y1="1cm" width="19cm" height="247mm"/>
        </pageTemplate>
      </template>
      
      <stylesheet>

        <paraStyle name="text"
        	fontName="Helvetica"
        	fontSize="10"
		/>
		
		<paraStyle name="textmini"
        	fontName="Helvetica"
        	fontSize="7"
		/>
		
		<paraStyle name="titol"
			fontName="Helvetica-Bold"
			fontSize="10"
		/>
		
		<blockTableStyle id="mesures">
		  <lineStyle kind="LINEABOVE" colorName="black" start="0,0" stop="-1,0" />
		  <lineStyle kind="LINEBEFORE" colorName="black" start="0,0" stop="5,1" />
		  <lineStyle kind="LINEABOVE" colorName="black" start="0,2" stop="-1,1" />
		  <lineStyle kind="GRID" colorName="black" start="5,1" stop="8,1" />
		  <lineStyle kind="LINEAFTER" colorName="black" start="8,0" stop="-1,1" />
		  <lineStyle kind="GRID" colorName="black" start="0,2" stop="-1,-1" />
		  <blockBackground colorName="silver" start="0,0" stop="-1,1" />
		</blockTableStyle>

      </stylesheet>
    
      <story>
      
        <xsl:apply-templates select="revisio" mode="story" />
        <setNextTemplate name="main" />
      </story>
      
    </document>
    
  </xsl:template>

  <xsl:template match="revisio" mode="story">
    <para style="titol" alignment="center" t="1">Caracter�stiques t�cniques principals de la instal�laci�</para>
    <para style="titol" alignment="center"><xsl:value-of select="ct/codi" /> - <xsl:value-of select="ct/descripcio" /></para>
    <spacer length="0.5cm" />
    <blockTable colWidths="8cm,1.5cm,8cm,1.5cm">
    	<tr>
    		<td><para style="text" fontName="Helvetica-Bold" t="1">Intensitat de prova (A):</para></td>
    		<td><para style="text"><xsl:value-of select="i_prova" /></para></td>
    		<td><para style="text" fontName="Helvetica-Bold" t="1">Temps de Disparo Protecci�:</para></td>
    		<td><para style="text"><xsl:value-of select="t_disparo" /></para></td>
    	</tr>
    	<tr>
    		<td><para style="text" fontName="Helvetica-Bold" t="1">Intensitat defecte real (A):</para></td>
    		<td><para style="text"><xsl:value-of select="i_defecte" /></para></td>
    		<td><para style="text" fontName="Helvetica-Bold" t="1">Tensi� M�xima Admissible Id (v):</para></td>
    		<td><para style="text"><xsl:value-of select="t_max" /></para></td>
    	</tr>
    </blockTable>
    
    <spacer length="0.5cm" />
    <xsl:if test="count(equips/equip)&gt;1">
   	<para style="text" fontSize="8" fontName="Helvetica-Bold" t="1" alignment="right">EQUIPS DE MESURA</para>
    <xsl:for-each select="equips/equip">
       <para style="text" fontSize="8" alignment="right"><xsl:value-of select="codi" /></para>
    </xsl:for-each>
    </xsl:if>
    <xsl:if test="croquis!=0">
    	<para style="text" fontSize="8" fontName="Helvetica-Bold" t="1">SITUACI� MESURES</para>
    	<image width="14cm" height="6cm">
    		<xsl:value-of select="croquis" />
    	</image>
   	    <spacer length="0.5cm" />
    </xsl:if>
    <blockTable style="mesures" colWidths="1cm,4.3cm,1.7cm,1.7cm,1.7cm,1.45cm,1.45cm,1.45cm,1.45cm,2.7cm">
    	<tr t="1">
	  <td><para style="textmini" alignment="center" t="1">ORDRE</para></td>
	  <td><para style="textmini" alignment="center" t="1">SITUACI�</para></td>
	  <td><para style="textmini" alignment="center" t="1">MESURA</para></td>
	  <td><para style="textmini" alignment="center" t="1">TERRA</para></td>
	  <td><para style="textmini" alignment="center" t="1">I PROVA</para></td>
	  <td><para></para></td>
	  <td><xpre style="textmini" alignment="center" t="1">TENSIONS (mV)</xpre></td>
	  <td><para></para></td>
	  <td><para></para></td>
	  <td><para style="textmini" alignment="center" t="1">TENSI� per I defecte (V)</para></td>
    	</tr>
	<tr>
	  <td></td>
	  <td></td>
	  <td></td>
	  <td></td>
	  <td></td>
	  <td><para style="textmini" alignment="center">Vo</para></td>
	  <td><para style="textmini" alignment="center">V+</para></td>
	  <td><para style="textmini" alignment="center">V-</para></td>
	  <td><para style="textmini" alignment="center">V</para></td>
	  <td></td>
	</tr>
    	<xsl:apply-templates select="proves/prova" mode="story" />
    </blockTable>
    <spacer length="0.5cm" />
    <para style="text" fontName="Helvetica-Bold" t="1"><tr t="1">Inspector</tr></para>
    <blockTable colWidths="2.5cm,15cm">
      <tr>
	<td><para style="text" t="1"><tr t="1">Nom:</tr></para></td>
	<td><para style="text" t="1"><xsl:value-of select="tecnic/nom" /></para></td>
      </tr>
      <tr>
	<td><para style="text" t="1"><tr t="1">Signatura:</tr></para></td>
	<td></td>
      </tr>
      <tr>
	<td><para style="text" t="1"><tr t="1">Data:</tr></para></td>
	<td><para style="text" t="1"><xsl:value-of select="concat(substring(data, 9, 2), '/', substring(data, 6, 2), '/', substring(data, 1, 4))"/></para></td>
      </tr>
    </blockTable>
    <xsl:if test="observacions_mesura!=''">
    <spacer length="0.5cm" />
    <para style="text" fontName="Helvetica-Bold" t="1"><tr t="1">Observacions:</tr></para>
    <para style="text" leftIndent="0.7cm"><xsl:value-of select="observacions_mesura" /></para>
    </xsl:if>
    
  </xsl:template>
  
  <xsl:template match="proves/prova" mode="story">
  	<tr>
	  <td><para style="textmini" alignment="right"><xsl:value-of select="position()" /></para></td>
	  <td><para style="textmini" alignment="center"><xsl:value-of select="situacio" /></para></td>
	  <td><para style="textmini" alignment="center"><xsl:value-of select="mesura" /></para></td>
	  <td><para style="textmini" alignment="center"><xsl:value-of select="terra" /></para></td>
	  <td><para style="textmini" alignment="right"><xsl:value-of select="format-number(../../i_prova, '###,###.00')" /></para></td>
	  <td><para style="textmini" alignment="right"><xsl:value-of select="v0" /></para></td>
	  <td><para style="textmini" alignment="right"><xsl:value-of select="vm1" /></para></td>
	  <td><para style="textmini" alignment="right"><xsl:value-of select="vm2" /></para></td>
	  <td><para style="textmini" alignment="right"><xsl:value-of select="format-number(v, '###,###.00')" /></para></td>
	  <td><para style="textmini" alignment="right"><xsl:value-of select="format-number(v_def, '###,###.000')" /></para></td>
  	</tr>
  </xsl:template>

</xsl:stylesheet>
