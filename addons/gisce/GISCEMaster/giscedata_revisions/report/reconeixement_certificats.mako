## -*- coding: utf-8 -*-
<%
    from datetime import datetime,date
    from babel.dates import format_date, format_datetime, format_time
    from osv import osv

    def get_tipus_ct(codiTipus):
        cts_subtipus_obj = objects[0].pool.get("giscedata.cts.subtipus")
        ids = cts_subtipus_obj.search(cursor,uid,[("codi",'=',codiTipus)])
        return cts_subtipus_obj.browse(cursor,uid,ids[0]).tipus_id.name

    def te_defectes_interns(revisio):
        return revisio.num_ind_reparats[2] == "0"
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<head>
  <style type="text/css">
    ${css}
  </style>
  <link rel="stylesheet" href="${addons_path}/giscedata_revisions/report/reconeixement_certificats.css"/>
</head>
<body>
    <%
        revisions = sorted(objects, key=lambda elem: elem.name.name)
    %>
    %for rev in revisions:
        <div class="top_margin"></div>
        <%
            te_defectes = not te_defectes_interns(rev)
            if te_defectes:
                tots_reparats = rev.ind_reparats
            else:
                tots_reparats = True

            mostrar_acta = te_defectes and not tots_reparats
        %>
        <div class="lateral_container">
            <div class="lateral">${_("Format doc.")}
                %if mostrar_acta:
                    13.3-12_08
                %else:
                    13.3-09_08
                %endif
            </div>
        </div>
        <div class="images_header padded_text">
            <div class="leftplacement">
                <img src="${addons_path}/giscedata_revisions/report/gisce-eti_logo.png">
            </div>
            <div class="rightplacement">
                <img src="${addons_path}/giscedata_revisions/report/enac.png">
            </div>
            <div style="clear:both"></div>
        </div>

        <br>
        <div class="grey_header">
            <div class="bold_header1">
                %if mostrar_acta:
                    ${_("ACTA")}
                %else:
                    ${_("CERTIFICAT")}
                %endif
                ${_("DE RECONEIXEMENT PERIÒDIC")}<br>
                ${_("INSTAL·LACIÓ ELÈCTRICA DE SERVEI PÚBLIC")}
            </div>
        </div>

        <div class="bold_header1">${_("Reconeixement de Centres Transformadors")}</div>
        <hr>

        <div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Codi de document")}</div>
                <div class="right_col">
                    %if mostrar_acta:
                        A-CT-${rev.name.name} ${rev.trimestre.name}
                    %else:
                        C-CT-${rev.name.name} ${rev.trimestre.name}
                    %endif
                </div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Codi de revisió")}</div>
                <div class="right_col">${rev.name.name} ${rev.trimestre.name}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Expedient autorització administrativa")}</div>
                <div class="right_col">${rev.name.expedient_actual if rev.name.expedient_actual else ""}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Data de revisió")}</div>
                <%
                    data_obj_rev = datetime.strptime(rev.data, '%Y-%m-%d %H:%M:%S')
                    data_rev = data_obj_rev.strftime('%d/%m/%Y')
                %>
                <div class="right_col">${data_rev}</div>
            </div><br>
            %if te_defectes and tots_reparats:
                <div class="new_row padded_text">
                    <div class="left_col">${_("Data comprovació reparació de defectes")}</div>
                    <%
                        data_obj_compr = datetime.strptime(rev.data_comprovacio, '%Y-%m-%d %H:%M:%S')
                        data_compr = data_obj_compr.strftime('%d/%m/%Y')
                    %>
                    <div class="right_col">${data_compr}</div>
                </div><br>
                <div class="new_row padded_text">
                    <div class="left_col">${_("Tipus de comprovació")}</div>
                    <div class="right_col">${_("2a visita")}</div>
                </div><br>
            %endif
            <div class="new_row padded_text">
                <div class="left_col">${_("Unitat de reconeixement")}</div>
                <div class="right_col">${rev.name.name}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Empresa titular")}</div>
                <div class="right_col">${company.name}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Adreça per a notificacions")}</div>
                <% company_address = company.partner_id %>
                <div class="right_col">
                    ${company_address.address[0].street} - ${company_address.address[0].zip} - ${company_address.city}
                </div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Organisme d'inspecció")}</div>
                <div class="right_col">${rev.tecnic.organisme}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Adreça Organisme d'inspecció")}</div>
                <div class="right_col">${rev.tecnic.domicili}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Nom cognoms del titulat")}</div>
                <div class="right_col">${rev.tecnic.name}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("DNI")}</div>
                <div class="right_col">${rev.tecnic.dni}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Titulació")}</div>
                <div class="right_col">${rev.tecnic.titolacio}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Número de col·legiat")}</div>
                <div class="right_col">${rev.tecnic.n_collegiat}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Adreça per a notifiacions")}</div>
                <div class="right_col">${rev.tecnic.domicili}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Telèfon")}</div>
                <div class="right_col">${rev.tecnic.telefon}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Correu electrònic")}</div>
                <div class="right_col">${rev.tecnic.email}</div>
            </div><br>
        </div>

        <br class="new_line">
        <br class="new_line">

        <div class="bold_header2 padded_text" >${_("Dades de la instal·lació")}</div><hr>
        <div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Instal·lació")}<br></div>
                <div class="right_col">${_("Centre Transformador")} ${rev.name.name}</div>
            </div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Emplaçament")}<br></div>
                <div class="right_col">${rev.name.descripcio}</div>
            </div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Municipi")}<br></div>
                <div class="right_col">${rev.name.id_municipi.name}</div>
            </div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Tipus de Centre Transformador")}<br></div>
                <div class="right_col">${get_tipus_ct(rev.name.id_subtipus.codi)}</div>
            </div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Tensió de primari")}<br></div>
                <div class="right_col">${rev.name.tensio_p} V</div>
            </div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Tensió de secundari")}<br></div>
                <div class="right_col">${rev.name.tensio_s} V</div>
            </div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Sortides de baixa tensió")}<br></div>
                <%
                    if len(rev.name.sortidesbt) == 0:
                        num_sortides_bt = 0
                    else:
                        num_sortides_bt = len([x for x in rev.name.sortidesbt.seccio if x != False])
                %>
                <div class="right_col">${num_sortides_bt}</div>
            </div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Data de posada en marxa")}<br></div>
                <%
                    if rev.name.data_pm:
                        data_obj = datetime.strptime(rev.name.data_pm, '%Y-%m-%d')
                        data_pm = data_obj.strftime('%d/%m/%Y')
                    else:
                        data_pm = ''
                %>
                <div class="right_col">${data_pm}</div>
            </div>
            <br>
            <table class="padded_text" style="font-size: inherit; width:100%;">
                <tr>
                    <td rowspan="2" style="vertical-align:center; width:40%;">${_("Transformadors")}</td>
                    <td>
                        <table style="font-size: inherit; width:100%;">
                            %for transformador in rev.name.transformadors:
                                %if transformador.id_estat.codi == 1:
                                    <tr>
                                        <td style="width: 20%;">${_("Núm.")}</td>
                                        <td style="width: 20%; text-align:right">${transformador.name}</td>
                                        <td style="text-align:right">${_("Potència")}</td>
                                        <td style="text-align:right">${int(transformador.potencia_nominal)} ${_("kVA")}</td>
                                    </tr>
                                %endif
                            %endfor
                            <tr>
                                <td colspan="2">${_("POTÈNCIA TOTAL")}</td>
                                <td colspan="2" style="text-align:right">${int(rev.name.potencia)} ${_("kVA")}</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>

        <br class="new_line">
        <br class="new_line">

        <div class="wrap">
            %if tots_reparats:
                <div class="bold_header2 padded_text">${_("Certificació")}</div><hr>
            %else:
                <div class="bold_header2 padded_text">${_("Acta")}</div><hr>
            %endif
            <p class="padded_text justified">
                ${rev.tecnic.name}, ${_("enginyer competent que ha realitzat la inspecció de la instal·lacció elèctrica de referència, d'acord amb l'article 7 del Decret 328/2001, de 4 de desembre, pel qual s'estableix el procediment aplicable per efectuar els reconeixements periòdics de les instal·lacions de producció, transformació, transport i distribució d'energia elèctrica, publicat en el DOGC nº 3.536,")}
            </p>
            <br><br>
            %if tots_reparats:
                <div class="bold_header2 padded_text">${_("CERTIFICA:")}</div>
                <p class="padded_text">
                    ${_("Que aquesta és correcta i no té defectes que precisin correció immediata ni defectes a esmenar dins un termini.")}

                </p>
            %else:
                <div class="bold_header2 padded_text">${_("FA CONSTAR:")}</div>
                <p class="padded_text justified">
                    ${_("Que la instal·lació té defectes i s'adjunta a aquesta acta una relació de defectes d'acord amb la legislació vigent segons el Decret 328/2001")}
                </p>
            %endif
            <br><br>
            <div class="padded_text">
                ${_("Observacions i explicació d'accions no realitzades:")}
            </div>
            %if rev.observacions:
                <br>
                <div class="padded_text">
                    ${rev.observacions}
                </div>
            %endif

            <br><br><br><br><br><br>
            <div>
                <span class="leftplacement padded_text">${_("Signatura")}</span>
                <%
                    if te_defectes and tots_reparats:
                        data_obj = data_obj_compr
                    else:
                        data_obj = data_obj_rev

                    d = date(data_obj.year, data_obj.month, data_obj.day)
                    data_document = format_date(d, "d MMMM 'de' yyyy", locale='ca_ES')
                %>
                <span class="rightplacement padded_text document_date">${_("Girona")}, ${data_document}</span>
            </div>
            <br class="new_line">
            <div class="padded_text"><br>
                ${_("Aquest document queda a disposició de l'organisme competent del Departament d'Indústria, Comerç i Turisme")}
            </div>
            %if tots_reparats:
                <div class="padded_text">
                    ${_("El present certificat té una validesa de 3 anys a partir de la data de revisió")}
                </div>
            %endif
        </div>

        <p style="page-break-after:always;"><br></p>
        %if mostrar_acta:
            <div class="top_margin"></div>
            <div class="lateral_container">
                <div class="lateral">${_("Format doc.")} 13.3-12_08</div>
            </div>
            <div class="images_header padded_text">
                <div class="leftplacement">
                    <img src="${addons_path}/giscedata_revisions/report/gisce-eti_logo.png"  >
                </div>
                <div class="rightplacement">
                    <img src="${addons_path}/giscedata_revisions/report/enac.png"  >
                </div>
                <div style="clear:both"></div>
            </div>

            <br>
            <div class="grey_header">
                <div class="bold_header1">
                    ${_("ACTA")} ${_("DE RECONEIXEMENT PERIÒDIC")}<br>
                    ${_("INSTAL·LACIÓ ELÈCTRICA DE SERVEI PÚBLIC")}
                </div>
            </div>

            <div class="bold_header1">${_("Reconeixement de Centres Transformadors")}</div>

            <div class="border_gray">
                <div class="titol_defectes">
                    ${_("DEFECTES AL CENTRE TRANSFORMADOR")} ${rev.name.name}
                </div>
            </div>
            <div class="border_gray without_border_top">
                <table class="llista_defectes">
                    <tr class="bold_text">
                        <td>${_("Codi")}</td>
                        <td>${_("Tipus")}</td>
                        <td>${_("Descripció")}</td>
                        <td>${_("Valoració")}</td>
                        <td>${_("Limit reparació")}</td>
                    </tr>
                    %for defecte in rev.defectes_ids:
                        %if defecte.estat == 'B' and not defecte.intern:
                            <tr>
                                <td>${defecte.id}</td>
                                <td>${defecte.codi}</td>
                                <%
                                    if len(defecte.descripcio) > 100:
                                        defecte_descripcio = defecte.descripcio[:100] + "..."
                                    else:
                                        defecte_descripcio = defecte.descripcio
                                %>
                                <td style="text-align:left">${defecte_descripcio}</td>
                                <td>${defecte.valoracio.name}</td>
                                <%
                                    if defecte.data_limit_reparacio:
                                        data_obj = datetime.strptime(defecte.data_limit_reparacio, '%Y-%m-%d %H:%M:%S')
                                        data_limit_reparacio = data_obj.strftime('%d/%m/%Y')
                                    else:
                                        raise osv.except_osv(
                                            _(u"Falten dades"),
                                            _(u"El defecte {} de la revisió {}-{} no té data limit de reparació").format(
                                                defecte.codi, rev.name.name, rev.trimestre.name
                                            )
                                        )
                                %>
                                <td>${data_limit_reparacio}</td>
                            </tr>
                            <tr>
                                <td>&nbsp</td>
                                <td>&nbsp</td>
                                <td style="text-align:left">${defecte.observacions}</td>
                                <td>&nbsp</td>
                                <td>&nbsp</td>
                            </tr>
                        %endif
                    %endfor
                </table>
            </div>
            <div class="border_gray without_border_top">
                <table class="taula_tecnic">
                    <tr>
                        <td>${_("Tècnic:")}</td>
                        <td>${_("Titulació:")}</td>
                        <td>${_("Col·legiat:")}</td>
                        <td>${_("Data de reconeixment:")}</td>
                    </tr>
                    <tr class="bold_text">
                        <td>${rev.tecnic.name}</td>
                        <td>${rev.tecnic.titolacio}</td>
                        <td>${rev.tecnic.n_collegiat}</td>
                        <td>${data_rev}</td>
                    </tr>
                </table>
            </div>
        %endif

        %if rev != revisions[-1]:
            <p style="page-break-after:always;"><br></p>
        %endif
    %endfor
</body>
</html>
