<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

  <xsl:key name="revisions-per-municipi" match="revisio" use="municipi" />

  <xsl:template match="/">
    <xsl:apply-templates select="revisio-list" />
  </xsl:template>

  <xsl:template match="revisio-list">
    <document>
      <template>
        <pageTemplate id="main">
          <frame id="first" x1="1cm" y1="1cm" width="19cm" height="277mm"/>
        </pageTemplate>
      </template>
      
      <stylesheet>

        <paraStyle name="titol"
	  fontName="Helvetica"
	  fontSize="16"
          leading="32"
				/>

        <paraStyle name="titol2"
	  fontName="Helvetica"
	  fontSize="10"
          leading="20"
				/>
				
	<paraStyle name="text"
	  fontName="Helvetica"
	  fontSize="8"
	/>

        <blockTableStyle id="taula_contingut">
          <lineStyle kind="GRID" colorName="silver"/>
          <blockFont name="Helvetica" size="8" />
		  <blockBackground colorName="grey" start="0,0" stop="-1,0" />
		  <blockFont name="Helvetica-Bold" size="8" start="0,0" stop="-1,0" />
		  <blockAlignment value="LEFT" />
        </blockTableStyle>

      </stylesheet>
    
      <story>
      <xsl:for-each select="revisio[count(. | key('revisions-per-municipi', municipi)[1]) = 1]">
		<xsl:sort select="municipi" />
		
		<para style="titol">CT's per reconeixements</para>
                <para style="titol2">Trimestre: <xsl:value-of select="trimestre" /></para>
                <para style="titol2">Municipi: <xsl:value-of select="municipi" /></para>

                <blockTable style="taula_contingut" colWidths="3cm,7cm,1.5cm,1.8cm,1.8cm,4cm" repeatRows="1">
                  <tr t="1">
                    <td t="1">Codi</td>
                    <td t="1">Descripci�</td>
                    <td t="1">Tipus</td>
                    <td t="1">Data rev.</td>
                    <td t="1">IND�STRIA</td>
                    <td t="1">Observacions</td>
                 </tr>

		<xsl:for-each select="key('revisions-per-municipi', municipi)">
		  <xsl:apply-templates select="ct" mode="story">
            <xsl:sort select="../../data" order="ascending" />
		    <xsl:sort select="codi" data-type="number" />
                  </xsl:apply-templates>
		</xsl:for-each>
		</blockTable>
		<nextPage />
	</xsl:for-each>


        
      
      </story>
    </document>
  </xsl:template>
  
  <xsl:template match="ct" mode="story">
    <tr>
      <td><xsl:value-of select="codi"/></td>
      <td><para style="text"><xsl:value-of select="descripcio"/></para></td>
      <xsl:choose>
        <xsl:when test="tipus = 'CT' and tipus2 = 1">
      <td>PT</td>
        </xsl:when>
        <xsl:when test="tipus = 'CT' and tipus2 = 2">
      <td>ET</td>
        </xsl:when>
        <xsl:otherwise>
      <td><xsl:value-of select="tipus" /></td>
      	</xsl:otherwise>
      </xsl:choose>
      <td><xsl:if test="data!=''"><xsl:value-of select="concat(substring(data, 9, 2), '/', substring(data, 6, 2), '/', substring(data, 1, 4))"/></xsl:if></td>
      <td><xsl:if test="data_cfo!=''"><xsl:value-of select="concat(substring(data_cfo, 9, 2), '/', substring(data_cfo, 6, 2), '/', substring(data_cfo, 1, 4))"/></xsl:if></td>
      <td>
      <xsl:if test="substring-after(trimestre, '/') - 7 &lt; substring(data_cfo, 1, 4)"><xsl:text>CFO </xsl:text></xsl:if>
      <xsl:if test="ct_obres = 1"><xsl:text>OBRES </xsl:text></xsl:if>
      <xsl:if test="ct_baixa = 1"><xsl:text>BAIXA </xsl:text></xsl:if>
      </td>
    </tr>
  </xsl:template>
</xsl:stylesheet>
