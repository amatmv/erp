<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="revisio-list" />
  </xsl:template>

  <xsl:template match="revisio-list">
    <document>
      <template>
        <pageTemplate id="main">
          <frame id="first" x1="1cm" y1="1cm" width="19cm" height="277mm"/>
        </pageTemplate>
      </template>
      
      <stylesheet>

        <paraStyle name="titol"
					fontName="Helvetica"
					fontSize="16"
          leading="32"
				/>

        <paraStyle name="titol2"
					fontName="Helvetica"
					fontSize="10"
          leading="20"
				/>
				
		<paraStyle name="text"
			fontName="Helvetica"
			fontSize="8" />

        <blockTableStyle id="taula_contingut">
          <lineStyle kind="GRID" colorName="silver"/>
          <blockFont name="Helvetica" size="8" />
					<blockBackground colorName="grey" start="0,0" stop="-1,0" />
					<blockFont name="Helvetica-Bold" size="8" start="0,0" stop="-1,0" />
          <blockAlignment value="RIGHT" start="2,1" />
          <blockAlignment value="RIGHT" start="3,1" />
        </blockTableStyle>

        <blockTableStyle id="taula2">
          <blockFont name="Helvetica-Bold" size="8" />
          <blockAlignment value="RIGHT" start="2,0" />
          <blockAlignment value="RIGHT" start="3,0" />
        </blockTableStyle>
        

      </stylesheet>
    
      <story>
      <para style="titol" t="1"><tr t="1">L�nies AT per reconeixements</tr></para>
      <para style="titol2" t="1"><tr t="1">Trimestre: </tr><xsl:value-of select="revisio/trimestre" /></para>

      <blockTable style="taula_contingut" colWidths="2.3cm,8cm,1.5cm,1.5cm,1.5cm,2cm,4cm">
       <tr t="1">
          <td t="1">Codi</td>
          <td t="1">Descripci�</td>
          <td t="1">Aeri</td>
          <td t="1">Subt.</td>
          <td t="1">Tensi�</td>
          <td t="1">Data rev.</td>
          <td t="1">Observacions</td>
        </tr>
        <xsl:apply-templates select="revisio" mode="story">
          <xsl:sort select="../../data" order="ascending" />
          <xsl:sort select="linia/codi" data-type="number" />
        </xsl:apply-templates>
      </blockTable>
      
      <blockTable style="taula2" colWidths="2.3cm,8cm,1.5cm,1.5cm,1.5cm,2cm,4cm">
        <tr t="1">
          <td></td>
          <td t="1">Totals: </td>
          <td><xsl:value-of select="format-number(sum(revisio/linia/aeri), '#.##')" /></td>
          <td><xsl:value-of select="format-number(sum(revisio/linia/subt), '#.##')" /></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
      </blockTable>
      </story>
    </document>
  </xsl:template>
  <xsl:template match="revisio" mode="story">
    <tr>
      <td><xsl:value-of select="linia/codi"/></td>
      <td><para style="text"><xsl:value-of select="linia/descripcio" /></para></td>
      <td><xsl:value-of select="format-number(linia/aeri, '#.##')"/></td>
      <td><xsl:value-of select="format-number(linia/subt, '#.##')"/></td>
      <td><xsl:value-of select="linia/tensio"/></td>
      <td><xsl:value-of select="concat(substring(data, 9, 2), '/', substring(data, 6, 2), '/', substring(data, 1, 4))"/></td>
      <td><para style="text"><xsl:value-of select="observacions"/></para></td>
    </tr>
  </xsl:template>
</xsl:stylesheet>
