<%
    from datetime import datetime

    pool = objects[0].pool
    user_obj = pool.get('res.users')

    def obtenir_empresa():
        return user_obj.browse(cursor, uid, uid).company_id.name

    equip_obj = pool.get('giscedata.revisions.equips')

    def obtenir_equips():
        return equip_obj.search(cursor,uid,[])
    def obtenir_equip(id):
        return equip_obj.browse(cursor, uid, id)
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
      <style type="text/css">
        ${css}
      </style>
      <link rel="stylesheet" href="${addons_path}/giscedata_revisions/report/inspeccio.css"/>
    </head>
    <body>
        %for rev in objects:
            <div id="capcalera">
                <div id="logo">
                    <img src="${addons_path}/giscedata_revisions/report/gisce_logo.png"/>
                </div>
                <div id="dades_full">
                    <%
                        revisio = 8
                    %>
                    ${"13.3-13_0{}".format(revisio)}
                    <br>
                    ${_("Full d'inspecció")}<br>
                    ${_("Revisió {}".format(revisio))}<br>
                    ${_("Full 1 de 1")}
                </div>
            </div>
            <div style="clear: both;"></div>
            <div id="bar">
                13.3-13
            </div>
            <div id="titol">
                ${_("FULL D'INSPECCIÓ")}
            </div>
            <div id="bar_small">&nbsp;</div>
            <div id="contingut">
                <table class="tipus_inspeccio">
                    <tr>
                        <td class="separador">${_("TIPUS D'INSPECCIÓ:")}</td>
                        <td>
                            <div class="checkbox">&nbsp;</div>
                            ${_("INSPECCIÓ INICIAL")}
                        </td>
                        <td>
                            <div class="checkbox">&nbsp;</div>
                            ${_("INSPECCIÓ PERIÒDICA")}
                        </td>
                        <td>
                            <div class="checkbox">X</div>
                            ${_("VERIFICACIÓ PERIÒDICA")}
                        </td>
                    </tr>
                </table>
                <table class="cos">
                    <tr>
                        <td class="w_120 separador dark spaced">
                            ${_("RECONEIXEMENT DE:")}
                        </td>
                        <td class="dark spaced">
                            <div class="checkbox">&nbsp;</div>
                            CT
                        </td>
                        <td class="dark spaced">
                            <div class="checkbox">&nbsp;</div>
                            SE
                        </td>
                        <td class="dark spaced">
                            <div class="checkbox">&nbsp;</div>
                            CE
                        </td>
                        <td class="dark spaced">
                            <div class="checkbox">X</div>
                            LAT AEREA
                        </td>
                        <td class="dark spaced">
                            <div class="checkbox">&nbsp;</div>
                            LAT SUBT
                        </td>
                        <td class="dark spaced">
                            <div class="checkbox">&nbsp;</div>
                            LBT
                        </td>
                    </tr>
                </table>
                <table class="cos">
                    <tr>
                        <td class="w_60 separador spaced">${_("EMPRESA:")}</td>
                        <td class="company">${obtenir_empresa()}</td>
                    </tr>
                </table>
                <table class="cos">
                    <tr>
                        <td class="w_50 dark">
                            <table class="borderless">
                                <tr>
                                    <td class="w_60 spaced dark">${_("MUNICIPI:")}</td>
                                    <td class="field spaced dark">${rev.name.municipi.name}</td>
                                </tr>
                            </table>
                        </td>
                        <td class="w_50 separador dark">
                            <table class="borderless">
                                <tr>
                                    <td class="w_70 spaced dark">${_("POBLACIÓ:")}</td>
                                    <td class="field spaced dark">${rev.name.poblacio.name}</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table class="cos">
                    <tr>
                        <td class="w_50">
                            <table class="borderless">
                                <tr>
                                    <td class="w_150 spaced">${_("CODI CT/LAT/SE/CE/LBT:")}</td>
                                    <td class="field spaced">${rev.name.name}</td>
                                </tr>
                            </table>
                        </td>
                        <td class="w_50 separador">
                            <table class="borderless">
                                <tr>
                                    <td class="w_150 spaced">${_("NOM CT/LAT/SE/CE/LBT:")}</td>
                                    <td class="field spaced">${rev.name.descripcio}</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table class="cos">
                    <tr>
                        <td class="w_50">
                            <table class="borderless">
                                <tr>
                                    <%
                                        if rev.data:
                                            data_obj = datetime.strptime(rev.data, '%Y-%m-%d %H:%M:%S')
                                            data = data_obj.strftime('%d/%m/%Y')
                                            hora = data_obj.strftime('%H:%M:%S')
                                        else:
                                            data = ''
                                            hora = ''

                                        if rev.data_fi:
                                            data_obj = datetime.strptime(rev.data_fi, '%Y-%m-%d %H:%M:%S')
                                            hora_fi = data_obj.strftime('%H:%M:%S')
                                        else:
                                            hora_fi = ''
                                    %>
                                    <td class="w_175 spaced">${_("DATA INSPECCIÓ:")}</td>
                                    <td class="field spaced" style="padding-left:15px">${data}</td>
                                </tr>
                                <tr>
                                    <td class="w_175 spaced">${_("HORA INICI INSPECCIÓ:")}</td>
                                    <td class="field spaced" style="padding-left:15px">${hora}</td>
                                </tr>
                                <tr>
                                    <td class="w_175 spaced">${_("HORA FINAL INSPECCIÓ:")}</td>
                                    <td class="field spaced" style="padding-left:15px">${hora_fi}</td>
                                </tr>
                            </table>
                        </td>
                        <td class="w_50 separador">
                            <table class="borderless">
                                <tr>
                                    <td class="w_130 spaced">${_("CODI INSPECCIÓ:")}</td>
                                    <td class="field spaced">${"{0}/{1}".format(rev.name.name, rev.trimestre.name)}</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table class="cos">
                    <tr>
                        <td class="w_50">
                            <table class="borderless">
                                <tr>
                                    <td class="w_175 spaced">${_("TIPUS DE CT (NOMÉS EN CTs):")}</td>
                                    <td class="field spaced"></td>
                                </tr>
                            </table>
                        </td>
                        <td class="w_50">
                            <table class="borderless">
                                <tr>
                                    <td class="w_250 spaced">${_("TIPUS DE POSTA A TERRA (NOMÉS EN CTs):")}</td>
                                    <td class="field spaced"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table class="cos">
                    <tr>
                        <td class="w_130 spaced separador dark">
                            <div class="checkbox">

                            </div>
                            ${_("INTERIOR")}
                        </td>
                        <td class="w_130 spaced separador dark">
                            <div class="checkbox">

                            </div>
                            ${_("INTEMPÈRIE")}
                        </td>
                        <td class="w_100 spaced dark">
                            <div class="checkbox">

                            </div>
                            ${_("CTR")}
                        </td>
                        <td class="w_100 spaced separador dark">
                            <div class="checkbox">

                            </div>
                            ${_("UNICA")}
                        </td>
                        <td class="w_130 spaced dark">
                            <div class="checkbox">

                            </div>
                            ${_("PROTECCIÓ + SERVEI")}
                        </td>
                    </tr>
                </table>
                <table class="cos">
                    <tr>
                        <td class="separador" colspan="5">
                            <u>${_("NOMÉS PER INSTAL·LACIONS PROPIETAT DEL CLIENT (línies aèries i subterrànies d'AT):")}</u><br>
                            ${_("El titular de la instal·lació aporta la documentació relativa als treballs de manteniment realitzats")}
                            ${_("per l'empresa mantenidora autoritzada")}
                        </td>
                    </tr>
                    <tr>
                        <td class="separador w_10">
                            <div class="checkbox">&nbsp;</div>
                        </td>
                        <td class="w_20">
                            ${_("SÍ")}
                        </td>
                        <td class="w_10">
                            <div class="checkbox">&nbsp;</div>
                        </td>
                        <td class="w_20">
                            ${_("NO")}
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <table class="borderless">
                                <tr>
                                    <td class="w_130">
                                        ${_("Instal·lador autoritzat/DQE:")}
                                    </td>
                                    <td class="w_300">
                                        <div class="punts">&nbsp;</div>
                                    </td>
                                    <td class="w_100">
                                        ${_("Períodes aportats:")}
                                    </td>
                                    <td>
                                        <div class="punts">&nbsp;</div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table class="cos">
                    <tr>
                        <td class="observacions">${_("OBSERVACIONS:")}</td>
                    </tr>
                </table>
                <div id="equips">
                    <b>${_("EQUIPS UTILITZATS")}</b>
                    <br>
                    %for equip in rev.equips_utilitzats:
                        <span id="m_10" class="t_10">
                            ${"{0} {1}".format(equip.codi, equip.descripcio)}<br>
                        </span>
                    %endfor
                </div>
                <div id="inspector">
                    <table id="dades_inspector">
                        <tr>
                            <td colspan="2" id="titol_inspector">${_("INSPECTOR")}</td>
                        </tr>
                        <tr>
                            <td class="p_5 w_150">${_("Nom: {0}".format(rev.tecnic.name or ''))}</td>
                            <td class="centered">${_("Firma:")}</td>
                        </tr>
                        <tr>
                            <td class="p_5 padding_bottom" colspan="2">${_("Data: {0}".format(data))}</td>
                        </tr>
                    </table>
                </div>
            </div>
            %if rev != objects[-1]:
              <p style="page-break-after:always;"></p>
            %endif
        %endfor
    </body>
</html>
