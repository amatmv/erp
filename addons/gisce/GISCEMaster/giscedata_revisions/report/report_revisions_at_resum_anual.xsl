<?xml version="1.0" encoding="iso-8859-15"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

  <xsl:key name="revisions-per-trimestre" match="revisio" use="trimestre" />
  <xsl:key name="revisions-per-municipi" match="revisio" use="municipi" />

  <xsl:template match="/">
    <xsl:apply-templates select="llistat" />
  </xsl:template>

  <xsl:template match="llistat">
    <document compression="1">
      <template pageSize="(21cm, 29.7cm)">
        <pageTemplate id="main">
          <pageGraphics>
          	<!-- Cap�alera -->
          </pageGraphics>
          <frame id="first" x1="1cm" y1="1cm" width="19cm" height="277mm"/>
        </pageTemplate>
      </template>
      
      <stylesheet> 
      	<paraStyle name="text"
      		fontName="Helvetica"
      		fontSize="10" />
      		
      	<blockTableStyle id="trimestre">
          <blockFont name="Helvetica" size="10" />
          <lineStyle kind="BOX" colorName="black" />
          <blockBackground colorName="grey" start="0,0" stop="-1,-1" />
        </blockTableStyle>
        
        <blockTableStyle id="resum">
        	<blockFont name="Helvetica" size="10" />
        	<lineStyle kind="BOX" colorName="black" />
        	<blockBackground colorName="silver" start="0,0" stop="-1,-1" />
        </blockTableStyle>
      		
      </stylesheet>
      
      <story>
      	<para style="text" fontSize="14" t="1"><tr t="1">RESUM DE DEFECTES AT REPARATS PER ANY I MUNICIPI</tr></para>
      	<para style="text" fontSize="14" t="1"><tr t="1">ANY: </tr><xsl:value-of select="revisio/trimestre" /></para>
      	<spacer length="20" />
      	<!-- Agrupem els revisios per trimeste -->


	<xsl:for-each select="revisio[count(. | key('revisions-per-trimestre', trimestre)[1]) = 1]">
		<xsl:sort select="trimestre" />
		<xsl:variable name="trimestre" select="trimestre" />
		<blockTable style="trimestre" colWidths="3cm,16cm">
			<tr t="1">
				<td><para style="text" t="1">TRIMESTRE</para></td>
				<td><para style="text"><xsl:value-of select="trimestre" /></para></td>
			</tr>
		</blockTable>
		<xsl:for-each select="//llistat/revisio[count(. | key('revisions-per-municipi', municipi)[1]) = 1]">
			<xsl:sort select="municipi" />
			<xsl:variable name="municipi" select="municipi" />
			<xsl:if test="count(//llistat/revisio[municipi=$municipi and trimestre=$trimestre]) &gt; 0">
				
				<blockTable colWidths="3cm,16cm">
					<tr t="1">
						<td><para style="text" fontName="Helvetica-Bold" t="1">Municipi:</para></td>
						<td><para style="text" fontName="Helvetica-Bold" alignment="left"><xsl:value-of select="municipi" /></para></td>
					</tr>
				</blockTable>
				<blockTable colWidths="3cm,7cm,3cm,3cm,3cm">
					<tr t="1">
						<td><para style="text" t="1">LAT</para></td>
						<td><para style="text" t="1">Descripci�</para></td>
						<td><para style="text" t="1">TOTALS</para></td>
						<td><para style="text" t="1">Reparats</para></td>
						<td><para style="text" t="1">Pendents</para></td>
					</tr>
				<xsl:for-each select="//llistat/revisio[municipi=$municipi and trimestre=$trimestre]">
					<xsl:sort select="nom" />
					<tr>
						<td><para style="text"><xsl:value-of select="nom" /></para></td>
						<td><para style="text"><xsl:value-of select="descripcio" /></para></td>
						<td><para style="text"><xsl:value-of select="count(defectes/defecte[intern!=1])" /></para></td>
						<td><para style="text"><xsl:value-of select="count(defectes/defecte[reparat=1 and intern!=1])" /></para></td>
						<td><para style="text"><xsl:value-of select="count(defectes/defecte[intern!=1]) - count(defectes/defecte[reparat=1 and intern!=1])" /></para></td>
					</tr>
				</xsl:for-each>
				</blockTable>
				<blockTable style="resum" colWidths="10cm,3cm,3cm,3cm">
					<tr t="1">
						<td><para style="text" t="1">Resum municipi:</para></td>
						<td><para style="text"><xsl:value-of select="count(//llistat/revisio[municipi=$municipi and trimestre=$trimestre]/defectes/defecte[intern!=1])" /></para></td>
						<td><para style="text"><xsl:value-of select="count(//llistat/revisio[municipi=$municipi and trimestre=$trimestre]/defectes/defecte[reparat=1 and intern!=1])" /> (<xsl:value-of select="format-number((count(//llistat/revisio[municipi=$municipi and trimestre=$trimestre]/defectes/defecte[reparat=1 and intern!=1]) div count(//llistat/revisio[municipi=$municipi and trimestre=$trimestre]/defectes/defecte[intern!=1]))*100, '###.##')" />%)</para></td>
						<td><para style="text"><xsl:value-of select="count(//llistat/revisio[municipi=$municipi and trimestre=$trimestre]/defectes/defecte[intern!=1]) - count(//llistat/revisio[municipi=$municipi and trimestre=$trimestre]/defectes/defecte[reparat=1 and intern!=1])" /></para></td>
					</tr>
				</blockTable>
			</xsl:if>
		</xsl:for-each>
	</xsl:for-each>

      </story>
    </document>
  </xsl:template>

  
</xsl:stylesheet>
