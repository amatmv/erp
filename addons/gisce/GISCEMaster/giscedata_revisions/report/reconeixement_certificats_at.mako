<%
    from datetime import datetime,date
    from babel.dates import format_date, format_datetime, format_time

    def te_defectes_interns(rev):
        return rev.num_ind_reparats[2] == "0"
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<head>
  <style type="text/css">
    ${css}
  </style>
  <link rel="stylesheet" href="${addons_path}/giscedata_revisions/report/reconeixement_certificats.css"/>
</head>
<body>
    <%
        revisions = sorted(objects, key=lambda elem: elem.name.name)
    %>
    %for rev in revisions:
        <div class="top_margin"></div>
        <%
            te_defectes = not te_defectes_interns(rev)
            if te_defectes:
                tots_reparats = rev.ind_reparats
            else:
                tots_reparats = True

            mostrar_acta = te_defectes and not tots_reparats
        %>
        <div class="lateral_container">
            <div class="lateral">${_("Format doc.")}
                %if mostrar_acta:
                    13.3-11_07
                %else:
                    13.3-08_07
                %endif
            </div>
        </div>
        <div class="images_header padded_text">
            <div class="leftplacement">
                <img src="${addons_path}/giscedata_revisions/report/gisce-eti_logo.png">
            </div>
            <div class="rightplacement">
                <img src="${addons_path}/giscedata_revisions/report/enac.png">
            </div>
            <div style="clear:both"></div>
        </div>

        <br>
        <div class="grey_header">
            <div class="bold_header1">
                %if mostrar_acta:
                    ${_("ACTA")}
                %else:
                    ${_("CERTIFICAT")}
                %endif
                ${_("DE RECONEIXEMENT PERIÒDIC")}<br>
                ${_("INSTAL·LACIÓ ELÈCTRICA DE SERVEI PÚBLIC")}
            </div>
        </div>

        <div class="bold_header1">${_("Reconeixement de Línies d'Alta Tensió")}</div>
        <hr>

        <div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Codi de document")}</div>
                <div class="right_col">
                    %if mostrar_acta:
                        A-AT-${rev.name.name} ${rev.trimestre.name}
                    %else:
                        C-AT-${rev.name.name} ${rev.trimestre.name}
                    %endif
                </div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Codi de revisió")}</div>
                <div class="right_col">${rev.name.name} ${rev.trimestre.name}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Expedient autorització administrativa")}</div>
                <div class="right_col">${rev.name.expedients_string if rev.name.expedients_string else ""}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Data de revisió")}</div>
                <%
                    data_obj_rev = datetime.strptime(rev.data, '%Y-%m-%d %H:%M:%S')
                    data_rev = data_obj_rev.strftime('%d/%m/%Y')
                %>
                <div class="right_col">${data_rev}</div>
            </div><br>
            %if te_defectes and tots_reparats:
                <div class="new_row padded_text">
                    <div class="left_col">${_("Data comprovació reparació de defectes")}</div>
                    <%
                        data_obj_compr = datetime.strptime(rev.data_comprovacio, '%Y-%m-%d %H:%M:%S')
                        data_compr = data_obj_compr.strftime('%d/%m/%Y')
                    %>
                    <div class="right_col">${data_compr}</div>
                </div><br>
                <div class="new_row padded_text">
                    <div class="left_col">${_("Tipus de comprovació")}</div>
                    <div class="right_col">${_("2a visita")}</div>
                </div><br>
            %endif
            <div class="new_row padded_text">
                <div class="left_col">${_("Unitat de reconeixement")}</div>
                <div class="right_col">LAT-${rev.name.name}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Tipus d'instal·lació")}</div>
                <div class="right_col">${_("Línia aèria d'alta tensió")}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Empresa titular")}</div>
                <div class="right_col">${company.name}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Adreça per a notificacions")}</div>
                <% company_address = company.partner_id %>
                <div class="right_col">
                    ${company_address.address[0].street} ${company_address.address[0].zip} - ${company_address.city}
                </div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Organisme d'inspecció")}</div>
                <div class="right_col">${rev.tecnic.organisme}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Adreça Organisme d'inspecció")}</div>
                <div class="right_col">${rev.tecnic.domicili}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Nom cognoms del titulat")}</div>
                <div class="right_col">${rev.tecnic.name}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("DNI")}</div>
                <div class="right_col">${rev.tecnic.dni}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Titulació")}</div>
                <div class="right_col">${rev.tecnic.titolacio}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Número de col·legiat")}</div>
                <div class="right_col">${rev.tecnic.n_collegiat}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Adreça per a notifiacions")}</div>
                <div class="right_col">${rev.tecnic.domicili}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Telèfon")}</div>
                <div class="right_col">${rev.tecnic.telefon}</div>
            </div><br>
            <div class="new_row padded_text">
                <div class="left_col">${_("Correu electrònic")}</div>
                <div class="right_col">${rev.tecnic.email}</div>
            </div><br>
        </div>

        <br class="new_line">
        <br class="new_line">

        <div class="bold_header2 padded_text" >${_("Dades de la instal·lació")}</div><hr>
        <div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Instal·lació")}<br></div>
                <div class="right_col">${_("Línia AT")} ${rev.name.name}</div>
            </div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Emplaçament")}<br></div>
                <div class="right_col">${rev.name.descripcio}</div>
            </div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Municipi")}<br></div>
                <div class="right_col">${rev.name.poblacio.municipi_id.name}</div>
            </div>
        </div>

        <br class="new_line">
        <br class="new_line">
        <div class="bold_header2 padded_text" >${_("Característiques tècniques principals")}</div><hr>
        <div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Origen de la línia")}<br></div>
                <div class="right_col">${rev.name.origen}</div>
            </div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Final de la línia")}<br></div>
                <div class="right_col">${rev.name.final}</div>
            </div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Tensió de servei")}<br></div>
                <div class="right_col">${rev.name.tensio} V</div>
            </div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Secció conductors")}<br></div>
                <div class="right_col">${rev.name.seccio_string}</div>
            </div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Longitud Aèria")}<br></div>
                <div class="right_col">
                    ${0 if rev.name.longitud_aeria_cad == 0 else rev.name.longitud_aeria_cad} m
                </div>
            </div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Longitud Subterrània")}<br></div>
                <div class="right_col">
                    ${0 if rev.name.longitud_sub_cad == 0 else rev.name.longitud_sub_cad} m
                </div>
            </div>
            <div class="new_row padded_text">
                <div class="left_col">${_("Suports")}<br></div>
                <div class="right_col">
                    ${_("Metàl·lics:")}
                    ${rev.name.n_suports_metallics if rev.name.n_suports_metallics else 0};
                    ${_("Formigó:")}
                    ${rev.name.n_suports_formigo if rev.name.n_suports_formigo else 0};
                    ${_("Fusta:")}
                    ${rev.name.n_suports_fusta if rev.name.n_suports_fusta else 0}
                </div>
            </div>
        </div>

        <br class="new_line">
        <br class="new_line">

        <div class="wrap">
            %if tots_reparats:
                <div class="bold_header2 padded_text">${_("Certificació")}</div><hr>
            %else:
                <div class="bold_header2 padded_text">${_("Acta")}</div><hr>
            %endif
            <p class="padded_text justified">
                ${rev.tecnic.name}, ${_("enginyer competent que ha realitzat la inspecció de la instal·lacció elèctrica de referència, d'acord amb l'article 7 del Decret 328/2001, de 4 de desembre, pel qual s'estableix el procediment aplicable per efectuar els reconeixements periòdics de les instal·lacions de producció, transformació, transport i distribució d'energia elèctrica, publicat en el DOGC nº 3.536,")}
            </p>
            <br><br>
            %if tots_reparats:
                <div class="bold_header2 padded_text">${_("CERTIFICA:")}</div>
                <p class="padded_text">
                    ${_("Que aquesta és correcta i no té defectes que precisin correció immediata ni defectes a esmenar dins un termini.")}

                </p>
            %else:
                <div class="bold_header2 padded_text">${_("FA CONSTAR:")}</div>
                <p class="padded_text">
                    ${_("Que la instal·lació té defectes i s'adjunta a aquesta acta una relació de defectes d'acord amb la legislació vigent segons el Decret 328/2001")}
                </p>
            %endif
            <br><br>
            <div class="padded_text">
                ${_("Observacions i explicació d'accions no realitzades:")}
            </div>
            %if rev.observacions:
                <br>
                <div class="padded_text">
                    ${rev.observacions}
                </div>
            %endif

            <br><br><br><br><br><br>
            <div class="leftplacement padded_text">${_("Signatura")}</div>
            <%
                if te_defectes and tots_reparats:
                    data_obj = data_obj_compr
                else:
                    data_obj = data_obj_rev

                d = date(data_obj.year, data_obj.month, data_obj.day)
                data_document = format_date(d, "d MMMM 'de' yyyy", locale='ca_ES')
            %>
            <div class="rightplacement padded_text">${_("Girona")}, ${data_document}</div>
            <br class="new_line">
            <div class="padded_text"><br>
                ${_("Aquest document queda a disposició de l'organisme competent del Departament d'Indústria, Comerç i Turisme")}
            </div>
            %if tots_reparats:
                <div class="padded_text">
                    ${_("El present certificat té una validesa de 3 anys a partir de la data de revisió")}
                </div>
            %endif
        </div>

        <p style="page-break-after:always;"><br></p>
        %if mostrar_acta:
            <div class="top_margin"></div>
            <div class="lateral_container">
                <div class="lateral">${_("Format doc.")} 13.3-11_07</div>
            </div>
            <div class="images_header padded_text">
                <div class="leftplacement">
                    <img src="${addons_path}/giscedata_revisions/report/gisce-eti_logo.png"  >
                </div>
                <div class="rightplacement">
                    <img src="${addons_path}/giscedata_revisions/report/enac.png"  >
                </div>
                <div style="clear:both"></div>
            </div>

            <br>
            <div class="grey_header">
                <div class="bold_header1">
                    ${_("ACTA")} ${_("DE RECONEIXEMENT PERIÒDIC")}<br>
                    ${_("INSTAL·LACIÓ ELÈCTRICA DE SERVEI PÚBLIC")}
                </div>
            </div>

            <div class="bold_header1">${_("Reconeixement de Línies d'Alta Tensió")}</div>

            <div class="border_gray">
                <div class="titol_defectes">
                    ${_("DEFECTES A LES LÍNIES D'ALTA TENSIÓ")} ${rev.name.name}
                </div>
            </div>
            <div class="border_gray without_border_top">
                <table class="llista_defectes">
                    <tr class="bold_text">
                        <td style="width:2%">${_("Codi")}</td>
                        <td style="width:2%">${_("Suport")}</td>
                        <td style="width:5%">${_("Situació línia")}</td>
                        <td style="width:2%">${_("Tipus")}</td>
                        <td style="width:20%">${_("Descripció")}</td>
                        <td style="width:3%">${_("Valoració")}</td>
                        <td style="width:3%">${_("Limit reparació")}</td>
                    </tr>
                    %for defecte in rev.defectes_ids:
                        %if not defecte.intern:
                            <tr>
                                <td>${defecte.id}</td>
                                <td>${defecte.suport.name}</td>
                                <td>${rev.name.descripcio}</td>
                                <td>${defecte.defecte_id.name}</td>
                                <%
                                    if len(defecte.defecte_id.descripcio) > 55:
                                        defecte_descripcio = defecte.defecte_id.descripcio[:55] + "..."
                                    else:
                                        defecte_descripcio = defecte.defecte_id.descripcio
                                %>
                                <td style="text-align:left">${defecte_descripcio}</td>
                                <td style="text-align:left">${defecte.valoracio.name}</td>
                                <%
                                    data_obj = datetime.strptime(defecte.data_limit_reparacio, '%Y-%m-%d %H:%M:%S')
                                    data_limit_reparacio = data_obj.strftime('%d/%m/%Y')
                                %>
                                <td>${data_limit_reparacio}</td>
                            </tr>
                            <tr>
                                <td>&nbsp</td>
                                <td>&nbsp</td>
                                <td>&nbsp</td>
                                <td>&nbsp</td>
                                <td style="text-align:left">${defecte.observacions}</td>
                                <td>&nbsp</td>
                                <td>&nbsp</td>
                            </tr>
                        %endif
                    %endfor
                </table>
            </div>
            <div class="border_gray without_border_top">
                <table class="taula_tecnic">
                    <tr>
                        <td>${_("Tècnic:")}</td>
                        <td>${_("Titulació:")}</td>
                        <td>${_("Col·legiat:")}</td>
                        <td>${_("Data de reconeixment:")}</td>
                    </tr>
                    <tr class="bold_text">
                        <td>${rev.tecnic.name}</td>
                        <td>${rev.tecnic.titolacio}</td>
                        <td>${rev.tecnic.n_collegiat}</td>
                        <td>${data_rev}</td>
                    </tr>
                </table>
            </div>
        %endif

        %if rev != revisions[-1]:
            <p style="page-break-after:always;"><br></p>
        %endif
    %endfor
</body>
</html>
