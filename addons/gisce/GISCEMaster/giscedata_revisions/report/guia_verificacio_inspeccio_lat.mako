## coding=utf-8
<%
    from datetime import datetime
    from osv import osv
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
        <style type="text/css">
            ${css}
        </style>
        <link rel="stylesheet" href="${addons_path}/giscedata_revisions/report/guia_verificacio_inspeccio_lat.css"/>
    </head>
    <body>
        <%
            revisions = 0
            check_symbol = "x"
        %>
        %for rev_lat in objects:
            <%
                revisions += 1
                suports = 0
            %>
            %for suport in rev_lat.suports:
                <%
                    suports += 1
                %>
                <div class="codi-document">Codi doc: 13.3-48_05</div>
                <div class="first4lines">
                    <img src="${addons_path}/giscedata_revisions/report/gisce_logo.png"/>
                    <div>Pàgina 1 de 2</div>
                </div>

                <table class="title">
                    <tr>
                        <th>13.3-48</th>
                        <th>GUIA GENERAL DE VERIFICACIONS I INSPECCIONS INICIALS I PERIÒDIQUES - LAT AEREA</th>
                    </tr>
                </table>

                <table id="header_table">
                    <tr>
                        <th width="20%">CODI D'INSPECCIÓ:</th>
                        <th width="30%">DATA:</th>
                        <th width="20%">TÈCNIC:</th>
                        <th width="25%" colspan="2">Núm. Suport:</th>
                    </tr>
                    <tr>
                        <td>${suport.codi_inspeccio}</td>
                        <%
                            data_temp = datetime.strptime(suport.data, '%Y-%m-%d %H:%M:%S')
                            data = data_temp.strftime('%d/%m/%Y')
                        %>
                        <td>${data}</td>
                        <td>${suport.tecnic}</td>
                        <td colspan="2">${suport.suport_id.name}</td>
                    </tr>
                    <tr>
                        <th colspan="3">CODI LÍNIA / DESCRIPCIÓ:</th>
                        <th>NO FREQÜENTAT:</th>
                        %if suport.frequentat == 'No freqüentat':
                            <td>X</td>
                        %else:
                            <td></td>
                        %endif
                    </tr>
                    <tr>
                        <td colspan="3" rowspan="2">${suport.codi_linia_descripcio}</td>
                        <th>FREQÜENTAT AMB CALÇAT:</th>
                        %if suport.frequentat == 'Freqüentat amb calçat':
                            <td>X</td>
                        %else:
                            <td></td>
                        %endif
                    </tr>
                    <tr>
                        <th>FREQÜENTAT SENSE CALÇAT:</th>
                        %if suport.frequentat == 'Freqüentat sense calçat':
                            <td>X</td>
                        %else:
                            <td></td>
                        %endif
                    </tr>
                </table>
                <table id="central_table">
                    <tr>
                        <th width="5%" rowspan="2">Codi</th>
                        <th width="76%" rowspan="2">Defecte</th>
                        <th width="10%" colspan="4">VALORACIÓ</th>
                        <th width="9%" colspan="3">Classificació defecte</th>
                    </tr>
                    <tr>
                        <th class="possible_valoracio">C</th>
                        <th class="possible_valoracio">I</th>
                        <th class="possible_valoracio">N/A</th>
                        <th class="possible_valoracio">N/C</th>
                        <th class="possible_classificacio">L</th>
                        <th class="possible_classificacio">G</th>
                        <th class="possible_classificacio">MG</th>
                    </tr>

                    <%
                        middle_titles = {
                            '1.':'CONDUCTORS',
                            '2.':'MESURES DE TERRA',
                            '3.':'AÏLLADORS',
                            '4.':'SUPORTS',
                            '5.':'VENTS',
                            '6.':'FONAMENTS',
                            '7.':'ENCREUAMENTS',
                            '8.':'PAS PER ZONES',
                            '9.':'MANIOBRA I PROTECCIÓ',
                        }
                        new_bloc = True
                        prefix = ''
                    %>
                    %for defecte in suport.defecte_ids:
                        <%
                            tipus_defecte = defecte.tipus_defecte_id
                            id_defecte = tipus_defecte.name
                            valoracio = defecte.valoracio
                            classificacio = defecte.classificacio
                            try:
                                prefix_sufix = id_defecte.split(".")
                            except:
                                raise osv.except_osv(
                                    u"Error",
                                    u"És possible que hi hagi un defecte buit."
                                )
                            new_prefix = prefix_sufix[0]

                            new_bloc = new_prefix != prefix
                        %>
                        %if new_bloc:
                            <%
                                prefix = new_prefix
                                middle_title_name = middle_titles.get(prefix + '.')
                            %>
                            <tr class="middle_title">
                                <th>${prefix}</th>
                                <td colspan="8" class="middle_title_inner">${middle_title_name}</td>
                            </tr>
                            <% new_bloc = False %>
                        %endif
                        <tr>
                            <td><div class="celda">${id_defecte}</div></td>
                            <td>${tipus_defecte.descripcio}</td>
                            %if valoracio == 'C':
                                <td class="celda">${check_symbol}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            %elif valoracio == 'I':
                                <td></td>
                                <td class="celda highlight">${check_symbol}</td>
                                <td></td>
                                <td></td>
                            %elif valoracio == 'NA':
                                <td></td>
                                <td></td>
                                <td class="celda">${check_symbol}</td>
                                <td></td>
                            %elif valoracio == 'NC':
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class="celda">${check_symbol}</td>
                            %else:
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            %endif
                            %if not classificacio or classificacio == 'default':
                                <td></td>
                                <td></td>
                                <td></td>
                            %elif classificacio == 'L':
                                <td class="celda highlight">${check_symbol}</td>
                                <td></td>
                                <td></td>
                            %elif classificacio == 'G':
                                <td></td>
                                <td class="celda highlight">${check_symbol}</td>
                                <td></td>
                            %elif classificacio == 'MG':
                                <td></td>
                                <td></td>
                                <td class="celda highlight">${check_symbol}</td>
                            %endif
                        </tr>
                    %endfor
                </table>
                <table id="descr_table">
                    <tr>
                       <th colspan="2" height="30px">Descripció defectes / Observacions</th>
                    </tr>
                    <%
                        n_linies = 0
                    %>
                    %for defecte in suport.defecte_ids:
                        %if defecte.valoracio == 'I':
                            <%
                                tipus_defecte = defecte.tipus_defecte_id
                                id_defecte = tipus_defecte.name
                                descr_defecte = defecte.observacions
                                if not descr_defecte:
                                    descr_defecte = 'Sense observació'
                            %>
                            <tr>
                                <td width="5%" class="celda highlight">${id_defecte}</td>
                                <td>${descr_defecte}</td>
                            </tr>
                        %endif
                    %endfor
                    <% observacions = suport.observacions%>
                    %if observacions:
                        <% observacions = observacions.replace('\n', '</br>')%>
                        <tr>
                            <td width="5%" class="celda" height="60px">obs</td>
                            <td height="60px" valign="top">${observacions}</td>
                        </tr>
                    %else:
                        <tr>
                            <td width="5%" class="celda" height="60px"></td>
                            <td height="60px"></td>
                        </tr>
                    %endif
                </table>
## #############################################################################
## ############################FI PRIMERA PAGINA################################
## #############################################################################
                <p style="page-break-after:always;"></p>
## #############################################################################
## ############################INICI SEGONA PAGINA##############################
## #############################################################################
                <div class="codi-document">Codi doc: 13.3-48_05</div>
                <div class="first4lines">
                    <img src="${addons_path}/giscedata_revisions/report/gisce_logo.png"/>
                    <div>Pàgina 2 de 2</div>
                </div>
                <table class="title">
                    <tr>
                        <th>13.3-48</th>
                        <th>GUIA GENERAL DE VERIFICACIONS I INSPECCIONS INICIALS I PERIÒDIQUES - LAT AEREA</th>
                    </tr>
                </table>
                <table width="100%" id="header_table">
                    <tr>
                        <th width="20%">CODI D'INSPECCIÓ:</th>
                        <th width="30%">DATA:</th>
                        <th width="20%">TÈCNIC:</th>
                        <th width="25%" colspan="2">Núm. Suport:</th>
                    </tr>
                    <tr>
                        <td>${suport.codi_inspeccio}</td>
                        <%
                            data_temp = datetime.strptime(suport.data, '%Y-%m-%d %H:%M:%S')
                            data = data_temp.strftime('%d/%m/%Y')
                        %>
                        <td>${data}</td>
                        <td>${suport.tecnic}</td>
                        <td colspan="2">${suport.suport_id.name}</td>
                    </tr>
                    <tr>
                        <th colspan="3">CODI LÍNIA / DESCRIPCIÓ:</th>
                        <th>NO FREQÜENTAT:</th>
                        %if suport.frequentat == 'No freqüentat':
                            <td>X</td>
                        %else:
                            <td></td>
                        %endif
                    </tr>
                    <tr>
                        <td colspan="3" rowspan="2">${suport.codi_linia_descripcio}</td>
                        <th>FREQÜENTAT AMB CALÇAT:</th>
                        %if suport.frequentat == 'Freqüentat amb calçat':
                            <td>X</td>
                        %else:
                            <td></td>
                        %endif
                    </tr>
                    <tr>
                        <th>FREQÜENTAT SENSE CALÇAT:</th>
                        %if suport.frequentat == 'Freqüentat sense calçat':
                            <td>X</td>
                        %else:
                            <td></td>
                        %endif
                    </tr>
                </table>
                <table class="subtitle">
                    <tr>
                        <td>PRESA DE DADES PER A LA VERIFICACIÓ DELS SISTEMES DE POSADA A TERRA A <em> SUPORTS NO FREQÜENTATS</em></td>
                    </tr>
                </table>
                <table id="datatable_1" class="datatable">
                    <tr>
                        <th>Elements</th>
                        <th>Resistència màxima de posada a terra (&#937)</th>
                        <th>Valor mesurat de p.a.t. Rf de protecció (&#937)</th>
                        <th>Valor mesurat de p.a.t Rf de autovàlvules (&#937)</th>
                    </tr>
                    <tr>
                        %if suport.centre_transformador:
                            <td>-</td>
                        %elif suport.autovalvules and suport.seccionador:
                            <td style="font-size: 10px;">SECCIONADOR, AUTOVÀLVULES</td>
                        %elif suport.autovalvules:
                            <td style="font-size: 10px;">AUTOVÀLVULES</td>
                        %elif suport.seccionador:
                            <td style="font-size: 10px;">SECCIONADOR</td>
                        %else:
                            <td>-</td>
                        %endif
                        %if suport.centre_transformador:
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                        %else:
                            %if suport.seccionador or suport.autovalvules:
                                <td>20</td>
                            %else:
                                <td>-</td>
                            %endif
                            %if suport.seccionador:
                                <%
                                    ferramenta = suport.ferramenta
                                    pinta = ferramenta != '-' and float(ferramenta) > 20
                                %>
                                %if pinta:
                                    <td class="highlight">${ferramenta}</td>
                                %else:
                                    <td>${ferramenta}</td>
                                %endif
                            %endif
                            %if not suport.seccionador and not suport.autovalvules and (suport.frequentat == 'Freqüentat sense calçat' or suport.frequentat == 'Freqüentat amb calçat'):
                                <%
                                    ferramenta = suport.ferramenta
                                    pinta = ferramenta != '-' and float(ferramenta) > 20
                                %>
                                %if pinta:
                                    <td class="highlight">${ferramenta}</td>
                                %else:
                                    <td>${ferramenta}</td>
                                %endif
                            %elif not suport.seccionador:
                                <td>-</td>
                            %endif
                            %if suport.autovalvules:
                                <%
                                    ferramenta = suport.ferramenta
                                    pinta = ferramenta != '-' and float(ferramenta) > 20
                                %>
                                %if pinta:
                                    <td class="highlight">${ferramenta}</td>
                                %else:
                                    <td>${ferramenta}</td>
                                %endif
                            %else:
                                <td>-</td>
                            %endif
                        %endif
                    </tr>
                </table>
                <hr id="separator">
                <table class="subtitle">
                    <tr>
                        <td>PRESA DE DADES PER A LA VERIFICACIÓ DELS SISTEMES DE POSADA A TERRA A <em> SUPORTS FREQÜENTATS</em></td>
                    </tr>
                </table>
                <table id="datatable_2" class="datatable">
                    <tr>
                        <th>Elements</th>
                        <th>Valor de Resistència de PAT  <b><i>(Rf)(&#937)</i></b></th>
                        <th>Intensitat màxima de defecte a terra <i><b>I mdt (A)</b>(Dada a facilitar per la companyia elèctrica)</i></th>
                        <th>Temps calculat d'actuació de la protecció <i><b>t (s)</b>(Dada a facilitar per la companyia elèctrica)</i></th>
                        <th>Valor màxim adoptat de la tensió de pas aplicada <i><b>Vpa (V)</b> (Obtenir aquesta dada apartir de la taula adjunta)</i></th>
                        <th>Valor màxim adoptat de la tensió de contacte aplicada <i><b>Vca (V)</b> (Obtenir aquesta dada apartir de la taula adjunta)</i></th>
                        <th>Intensitat injectada <b><i>Im (A)</i></b></th>
                    </tr>
                    <tr id="empty_row">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
                <p class="text">Taula orientativa dels valors de pas y contacte màxim admisibles en funció del tiemps màxim de duració de la falta.</p>
                <table id="orientative_datatable" class="datatable">
                    <tr>
                        <th>Temps t (s)</th>
                        <td>0,05</td>
                        <td>0,1</td>
                        <td>0,2</td>
                        <td>0,3</td>
                        <td>0,4</td>
                        <td>0,50</td>
                        <td>1</td>
                        <td>2</td>
                        <td>5</td>
                        <td>10</td>
                        <td>> 10</td>
                    </tr>
                    <tr id="orientative_table_row_2">
                        <th>Vpa (V)</th>
                        <td>7350</td>
                        <td>6330</td>
                        <td>5280</td>
                        <td>4200</td>
                        <td>3100</td>
                        <td>2040</td>
                        <td>1070</td>
                        <td>900</td>
                        <td>810</td>
                        <td>800</td>
                        <td>500</td>
                    </tr>
                    <tr id="orientative_table_row_3">
                        <th>Vca (V)</th>
                        <td>735</td>
                        <td>633</td>
                        <td>528</td>
                        <td>420</td>
                        <td>310</td>
                        <td>204</td>
                        <td>107</td>
                        <td>90</td>
                        <td>81</td>
                        <td>80</td>
                        <td>50</td>
                    </tr>
                </table>

                <section id="formules">
                    <div class="formula_a">
                        Calcul de la tensió resultant:
                        <img src="${addons_path}/giscedata_revisions/report/FormulaTensioResultant.png"/>
                    </div>
                    <div class="formula_b">
                        Càlcul del valor real de la tensió de pas o contacte:
                        <img src="${addons_path}/giscedata_revisions/report/FormulaTensioDePas.png"/>
                    </div>
                </section>

                <table id="last_table">
                    <tr>
                        <th width="40%" rowspan="2" colspan="2">Posició</th>
                        <th width="8%" rowspan="2">Tipus de sòl</th>
                        <th width="20%" colspan="3">Valors de tensions mesurades</th>
                        <th width="8%">Tensió resultant</th>
                        <th width="8%">Pas o Contacte</th>
                        <th width="8%">Valor real de la tensió de pas/contacte</th>
                        <th width="8%" rowspan="2">Valoració</th>
                    </tr>
                    <tr id="magnitudes">
                        <th>Vo (mV)</th>
                        <th>Vo+ (mV)</th>
                        <th>Vo- (mV)</th>
                        <th>V (mV)</th>
                        <th>P/C</th>
                        <th>Vp-Vc (V)</th>
                    </tr>
                    %for i in range(1,7):
                        <tr>
                            <td id="numbered_column">${i}</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    %endfor
                </table>
                <p class="text">CROQUIS y/o OBSERVACIONS</p>
                <div id="croquis"></div>
                %if not (revisions == len(objects) and suports == len(rev_lat.suports)):
                    <p style="page-break-after:always;"></p>
                %else:
                %endif
            %endfor
        %endfor
    </body>
</html>
