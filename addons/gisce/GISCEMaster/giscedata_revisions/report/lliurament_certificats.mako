<%
    from datetime import datetime,date

    def getUltimaData(trieni):
        if not trieni:
            return ''
        data = str(trieni.data_final)
        dates = data.split('/')
        if len(dates) != 3:
            return ''
        else:
            m = dates[0]
            d = dates[1]
            y = dates[2]
        d = '{0}-{1}-{2}'.format(d, m, y)
        return d

    def get_revisions_ct_ordenades(trieni):
        revisions_ct_obj = objects[0].pool.get("giscedata.revisions.ct.revisio")
        llista_revisions_ct=[]
        for i in revisions_ct_obj.search(cursor,uid,[("trimestre",'=',trieni),("state","=","tancada")]):
            llista_revisions_ct.append(revisions_ct_obj.read(cursor,uid,i,["name"]))
        return sorted(llista_revisions_ct, key=lambda elem : elem['name'][1])

    def get_revisions_la_ordenades(trieni):
        revisions_la_obj = objects[0].pool.get("giscedata.revisions.at.revisio")
        llista_revisions_la=[]
        for i in revisions_la_obj.search(cursor,uid,[("trimestre",'=',trieni),("state","=","tancada")]):
            llista_revisions_la.append(revisions_la_obj.read(cursor,uid,i,["name"]))
        return sorted(llista_revisions_la, key=lambda elem : elem['name'][1])

%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<head>
  <style type="text/css">
    ${css}
  </style>
  <link rel="stylesheet" href="${addons_path}/giscedata_revisions/report/lliurament_certificats.css"/>
</head>
<body>
  %for trieni in objects:
    <div id="separador">13.3-06</div>
    <div id=capcalera1>
        <h1>${_("FULL DE LLIURAMENT DE CERTIFICATS")}</h1>
        <div id="text_petit_esquerra">${_("Lliurament de certificats a:")}</div>
        <div id="text_petit_dreta">${_("Segell de registre:")}</div>
        <h2>${company.name}</h2>
        <div id="quadre_registre">
            <table>
                <tr>
                    <th colspan="2">${_("REGISTRE DE SORTIDA")}</th>
                </tr>
                <tr>
                    <td width="60%">DATA<p style="font-size: 15px;margin:0;padding:0">&emsp;&emsp;/&emsp;&emsp;/</p></td>
                    <td  width="40%">NÚM.</td>
                </tr>
            </table>
        </div>
    </div>
    <div id="list-title">
        <h2 style="margin-top: -15px">Trimestre ${trieni.name}</h2>
        <div id="separador2"></div>
    </div>
    <div class=llista_info>
      <p>${_("Centres Transformadors:")}</p>
      <table>
          <%
            cts = get_revisions_ct_ordenades(trieni.id)
          %>
            %for rev_ct in cts:
                <%
                    index = trieni.cts.name.name.index(rev_ct['name'][1])
                    elem = trieni.cts[index]
                %>
                <tr>
                    <td>${elem.name.name}</td>
                    <td>${elem.name.descripcio}</td>
                </tr>
            %endfor
      </table>
    </div>
    <div class=llista_info>
      <p>${_("Línies d'Alta Tensió:")}</p>
      <table>
          <%
            la = get_revisions_la_ordenades(trieni.id)
          %>
            %for rev_la in la:
                <%
                    index = trieni.lat.name.name.index(rev_la['name'][1])
                    elem = trieni.lat[index]
                %>
                <tr>
                    <td>${elem.name.name}</td>
                    <td>${elem.name.descripcio}</td>
                </tr>
            %endfor
      </table>
    </div>
    %if trieni.lbt:
      <div class=llista_info>
        <p>${_("Línies de Baixa Tensió:")}</p>
        <table>
            <%
                lbt = sorted(trieni.lbt, key=lambda elem : elem.name.name)
            %>
            %for rev_lb in lbt:
                <tr>
                    <td>${rev_lb.name.name}</td>
                    <td>${rev_lb.name.descripcio}</td>
                </tr>
            %endfor
        </table>
      </div>
    %endif
    <div id="data_final">
        <div id="text_data_final">${_("Realitzat per (inspector):")}</div>
            <br><br><br><br>Data: ${date.today().strftime('%d/%m/%Y')}
    </div>
    %if trieni != objects[-1]:
      <p style="page-break-after:always;clear: both"></p>
    %endif
  %endfor
</body>
</html>
