## coding=utf-8
<%
    from datetime import datetime
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
        <style type="text/css">
            ${css}
        </style>
        <link rel="stylesheet" href="${addons_path}/giscedata_revisions/report/revisions_cts_mesura_pic.css"/>
    </head>
    <body>
        <% page = 0 %>
        %for rev in objects:
            <%
                date = datetime.strptime(rev.data, '%Y-%m-%d %H:%M:%S')
                date = date.strftime('%d/%m/%Y')
                ct_code = "{} - {}".format(rev.name.name, rev.nom_ct)
                observations = rev.observacions_mesura if rev.observacions_mesura else "-"
                intensity_test = '{:.2f}'.format(round(rev.intensitat_prova, 2))
                intensity_def = '{:.2f}'.format(round(rev.intensitat_defecte, 2))
                trigger_time = '{:.2f}'.format(round(rev.temps_disparo, 2))
                max_tension = '{:.2f}'.format(round(rev.tensio_maxima, 2))
                page += 1
            %>
            <table id="title">
                <tr><td class="center bold title-col-h">${_("CODI C.T.")}:</td><td class="center title-col-d">${ct_code}</td><td class="center bold title-col-h">${_("CODI INSPECCIÓ")}:</td><td class="center title-col-d">${rev.codi}</td></tr>
            </table>
            <table id="installation-characteristics">
                <tr><th colspan="4" class="center">${_("Característiques tècniques principals de la instal·lació")}</th></tr>
                <tr><td class="bold col-ic-h">${_("Intensitat de prova")} (A):</td><td class="right col-ic-d">${intensity_test}</td><td class="bold col-ic-h">${_("Temps de Disparo Protecció")}:</td><td class="right col-ic-d">${trigger_time}</td></tr>
                <tr><td class="bold col-ic-h">${_("Intensitat defecte real")} (A):</td><td class="right col-ic-d">${intensity_def}</td><td class="bold col-ic-h">${_("Tensió Màxima Admissible Id")} (v):</td><td class="right col-ic-d">${max_tension}</td></tr>
            </table>
            <table id="equipment">
                <tr>
                    <th>${_("EQUIPS DE MESURA")}</th>
                    <td>
                    <% n_equips = 0 %>
                    %if rev.equips_pic:
                        %for equipment in rev.equips_pic:
                            <%
                                n_equips += 1
                                eq_code = equipment.codi

                                if n_equips < len(rev.equips_pic):
                                    eq_code += ","
                            %>
                            ${eq_code}
                        %endfor
                    %else:
                        -
                    %endif
                    </td>
                </tr>
            </table>
            <table id="locations">
                <tr><th>${_("SITUACIÓ MESURES")}</th></tr>
                %if rev.croquis_mesura:
                    <tr><td class="center"><img src="data:image/jpeg;base64,${rev.croquis_mesura}"></td></tr>
                %endif
            </table>
            <table id="data">
                <colgroup>
                    <col id="sequence"/>
                    <col id="location"/>
                    <col id="measure"/>
                    <col id="ground"/>
                    <col id="i-test"/>
                    <col id="vo"/>
                    <col id="vp"/>
                    <col id="vn"/>
                    <col id="v"/>
                    <col id="def-tensions"/>
                </colgroup>
                <thead>
                    <tr>
                        <th rowspan="2">${_("ORDRE")}</th>
                        <th rowspan="2">${_("SITUACIÓ")}</th>
                        <th rowspan="2">${_("MESURA")}</th>
                        <th rowspan="2">${_("TERRA")}</th>
                        <th rowspan="2">${_("I PROVA")}</th>
                        <th colspan="4">${_("TENSIONS")} (mV)</th>
                        <th rowspan="2">${_("TENSIÓ per I defecte")} (V)</th>
                    </tr>
                    <tr>
                        <th>Vo</th>
                        <th>V+</th>
                        <th>V-</th>
                        <th>V</th>
                    </tr>
                </thead>
                <tbody>
                    <% sequence = 1 %>
                    %for measurement in rev.proves_mesura:
                        <%
                            v0 = '{:.2f}'.format(round(measurement.v0, 2))
                            vp = '{:.2f}'.format(round(measurement.vm1, 2))
                            vn = '{:.2f}'.format(round(measurement.vm2, 2))
                            v = '{:.2f}'.format(round(measurement.v, 2))
                            v_def = '{:.2f}'.format(round(measurement.v_def, 2))
                        %>
                        <tr>
                            <td>${sequence}</td>
                            <td>${measurement.name.name}</td>
                            <td>${measurement.mesura}</td>
                            <td>${measurement.terra}</td>
                            <td>${intensity_test}</td>
                            <td class="right pr5px">${v0}</td>
                            <td class="right pr5px">${vp}</td>
                            <td class="right pr5px">${vn}</td>
                            <td class="right pr5px">${v}</td>
                            <td class="right pr5px">${v_def}</td>
                        </tr>
                        <% sequence += 1 %>
                    %endfor
                </tbody>
            </table>
            <table id="by">
                <tr><th class="col-by-1">${_("Inspector")}</th><td class="col-by-2 ">${rev.tecnic.name}</td><th class="col-by-3">${_("Data")}:</th><td class="col-by-4 left">${date}</td><th class="col-by-5">${_("Signatura")}:</th><td></td></tr>
            </table>
            <table id="obs">
                <tr><th class="col-by-1">${_("Observacions")}:</th><td>${observations}</td></tr>
            </table>
            %if page < len(objects):
                <p style="page-break-after:always"></p>
            %endif
        %endfor
    </body>
</html>