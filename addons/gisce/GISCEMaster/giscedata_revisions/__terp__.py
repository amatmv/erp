# -*- coding: utf-8 -*-
{
    "name": "GISCE Data Revisions",
    "description": """Revisions""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Revisions",
    "depends":[
        "c2c_webkit_report",
        "base_extended",
        "giscedata_expedients",
        "giscedata_cts",
        "giscedata_trieni",
        "giscedata_at"
    ],
    "init_xml": [],
    "demo_xml": [
        "giscedata_revisions_demo.xml",
    ],
    "update_xml":[
        "giscedata_revisions_view.xml",
        "giscedata_revisions_wizard.xml",
        "giscedata_revisions_report.xml",
        "giscedata_revisions_data.xml",
        "giscedata_cts_view.xml",
        "ir.model.access.csv",
        "security/giscedata_revisions_security.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
