# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscedataCts(osv.osv):
    _name = "giscedata.cts"
    _inherit = "giscedata.cts"
    _columns = {
        'revisions': fields.one2many(
            'giscedata.revisions.ct.revisio', 'name', 'Revisions'),
        'intensitat_defecte': fields.float('Intensitat defecte real (A)'),
        'temps_disparo': fields.float('Temps de Disparo Protecció'),
        'tensio_maxima': fields.float('Tensió Màxima Admissible Id (V)'),
    }

GiscedataCts()


class GiscedataCtsMesures(osv.osv):
    _name = "giscedata.cts.mesures"
    _inherit = "giscedata.cts.mesures"
    _columns = {
        'id_revisio': fields.many2one(
            'giscedata.revisions.ct.revisio', 'Revisió CT')
    }

    _sql_constraints = [(
        'id_revisio', 'unique (id_revisio)',
        'Només hi pot haver una revisió assignada a la mesura'
    )]

GiscedataCtsMesures()
