from osv import fields, osv

class GiscedataTrieni(osv.osv):

    _name = "giscedata.trieni"
    _inherit = "giscedata.trieni"

    _columns = {
        'cts': fields.one2many('giscedata.revisions.ct.revisio', 'trimestre',
                               'Revisions CTS'),
        'lat': fields.one2many('giscedata.revisions.at.revisio', 'trimestre',
                               'Revisions LAT'),
    }

GiscedataTrieni()