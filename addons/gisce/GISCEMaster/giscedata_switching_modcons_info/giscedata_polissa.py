# -*- coding: utf-8 -*-
from osv import osv, fields, orm


class GiscedataPolissa(osv.osv):
    """Pòlissa."""
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    _columns = {
        'atr_activation_id': fields.many2one('giscedata.switching', 'ATR activacio'),
        'atr_cancel_id': fields.many2one('giscedata.switching', 'ATR baixa')
    }

GiscedataPolissa()


class GiscedataPolissaModcontractual(osv.osv):
    """Modificació Contractual d'una Pòlissa."""
    _name = 'giscedata.polissa.modcontractual'
    _inherit = 'giscedata.polissa.modcontractual'

    def store_activation_case(self, cursor, uid, ids, sw_id, context=None):
        if context is None:
            context = {}

        for modcon in self.browse(cursor, uid, ids, context=context):
            if not modcon.modcontractual_ant:
                modcon.polissa_id.write({'atr_activation_id': sw_id})
            modcon.write({'atr_activation_id': sw_id})

    def store_cancelation_case(self, cursor, uid, ids, sw_id, context=None):
        if context is None:
            context = {}

        for modcon in self.browse(cursor, uid, ids, context=context):
            if not modcon.modcontractual_seg:
                modcon.polissa_id.write({'atr_cancel_id': sw_id})
            modcon.write({'atr_cancel_id': sw_id})

    _columns = {
        'atr_activation_id': fields.many2one('giscedata.switching', 'ATR activacio'),
        'atr_cancel_id': fields.many2one('giscedata.switching', 'ATR baixa')
    }

GiscedataPolissaModcontractual()
