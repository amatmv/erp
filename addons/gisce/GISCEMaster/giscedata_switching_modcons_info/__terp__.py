# -*- coding: utf-8 -*-
{
    "name": "Switching Modcons Info",
    "description": """Store ATR cases in modcons and contracts that are afected by them""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends": [
        "giscedata_switching",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "giscedata_polissa_view.xml"
    ],
    "active": False,
    "installable": True
}
