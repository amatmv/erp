# -*- coding: utf-8 -*-
from osv import osv, fields, orm


class GiscedataSwitching(osv.osv):
    """Classe per gestionar el canvi de comercialitzador
    """

    _name = "giscedata.switching"
    _inherit = "giscedata.switching"

    def activa_cas_atr(self, cursor, uid, sw, context=None):
        res = super(GiscedataSwitching, self).activa_cas_atr(
            cursor, uid, sw, context=context
        )

        if len(res) and res[0] == "OK" and len(sw.cups_polissa_id.modcontractuals_ids):
            polissa = sw.cups_polissa_id
            pol_obj = self.pool.get("giscedata.polissa")

            if len(polissa.modcontractuals_ids) == 1 and polissa.state == "activa":
                # Contract has been activated for first time (A3/C1/C2)
                polissa.modcontractual_activa.store_activation_case(
                    sw.id, context=context
                )
            elif len(polissa.modcontractuals_ids) > 1 and polissa.state == "activa":
                # We have done a modcon (M1)
                polissa.modcontractual_activa.modcontractual_ant.store_activation_case(
                    sw.id, context=context
                )
                polissa.modcontractual_activa.store_cancelation_case(
                    sw.id, context=context
                )
            elif polissa.state == "baixa":
                # We have passed the contract to baixa (B1/C1-06/C2-06)
                polissa.modcontractual_activa.store_cancelation_case(
                    sw.id, context=context
                )

                # In M1 with owner change we may have the new contract
                ref_new = False
                if sw.ref and sw.ref.split(",")[0] == "giscedata.polissa":
                    ref_new = sw.ref
                elif sw.ref2 and sw.ref2.split(",")[0] == "giscedata.polissa":
                    ref_new = sw.ref2
                if ref_new:
                    new_polissa = pol_obj.browse(cursor, uid, int(ref_new.split(",")[1]))
                    if len(new_polissa.modcontractuals_ids) == 1 and new_polissa.state == "activa":
                        new_polissa.modcontractual_activa.store_activation_case(
                            sw.id, context=context
                        )
        return res

GiscedataSwitching()
