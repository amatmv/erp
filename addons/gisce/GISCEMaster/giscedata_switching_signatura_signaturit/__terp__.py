# -*- coding: utf-8 -*-
{
    "name": "Signatura digital processos ATR Signaturit",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_signatura_documents_signaturit",
        "giscedata_switching_signatura",
    ],
    "init_xml": [],
    "demo_xml":[
    ],
    "update_xml":[
        'giscedata_switching_report.xml'
    ],
    "active": False,
    "installable": True
}
