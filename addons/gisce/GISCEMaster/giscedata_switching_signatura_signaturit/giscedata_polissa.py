# -*- coding: utf-8 -*-
from __future__ import absolute_import
import json
from osv import osv


class GiscedataPolissa(osv.osv):
    _name = "giscedata.polissa"
    _inherit = 'giscedata.polissa'

    def start_signature_atr(self, cursor, uid, polissa_id, cas_id, addr_id, context=None):
        if not context:
            context = {}
        cas_id = super(GiscedataPolissa, self).start_signature_atr(
            cursor, uid, polissa_id, cas_id, addr_id, context=context
        )

        imd_obj = self.pool.get('ir.model.data')
        attach_obj = self.pool.get('ir.attachment')
        add_obj = self.pool.get('res.partner.address')
        pro_obj = self.pool.get('giscedata.signatura.process')

        atr_report_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_switching', 'report_info_cas_ATR'
        )[1]
        pol_num = self.read(cursor, uid, polissa_id, ['name'])['name']

        partner = self.read(
            cursor, uid, polissa_id, ['titular']
        )['titular']

        partner_name = partner[1]
        email = add_obj.read(cursor, uid, addr_id, ['email'])['email']
        data = json.dumps({
            'callback_method': 'enviar_atr'
        })
        atr_categ = attach_obj.get_category_for(
            cursor, uid, 'atr', context=context)
        values = {
            'subject': 'Firma de modificación del contrato ' + pol_num,
            'delivery_type': 'email',
            'recipients': [
                (0, 0, {
                    'partner_address_id': addr_id,
                    'name': partner_name,
                    'email': email
                })
            ],
            'reminders': 0,
            'type': 'advanced',
            'data': data,
            'all_signed': False,
            'files': [
                (0, 0, {
                    'model': 'giscedata.switching,{}'.format(cas_id),
                    'report_id': atr_report_id,
                    'category_id': atr_categ
                })
            ]
        }

        process_id = pro_obj.create(cursor, uid, values, context=context)

        # Executar l'inici del proces
        return pro_obj.start(cursor, uid, [process_id], context=context)


GiscedataPolissa()
