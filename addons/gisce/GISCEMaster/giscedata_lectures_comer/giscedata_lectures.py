# -*- coding: utf-8 -*-
from osv import osv, fields

class GiscedataLecturesComptador(osv.osv):

    _name = 'giscedata.lectures.comptador'
    _inherit = 'giscedata.lectures.comptador'
    
    _columns = {
        'name': fields.char('Nº de sèrie', size=32, required=True),
    }
    
GiscedataLecturesComptador()


class GiscedataLecturesOrigenComer(osv.osv):
    """Orígens de lectures per comercialitzadora
    """

    _name = 'giscedata.lectures.origen_comer'

    def get_origin_by_code(self, cursor, uid, code, context=None):
        """
        :param code: Origin code
        :return: Returns origin id
        """
        origin_ids = self.search(
            cursor, uid, [('codi', '=', code)], context=context
        )

        if not origin_ids:
            return False

        return origin_ids[0]

    _columns = {
        'name': fields.char('Nom', size=64, required=True, translate=True),
        'codi': fields.char('Codi', size=2, required=True),
    }

    _order = "codi asc"

GiscedataLecturesOrigenComer()


class GiscedataLecturesLectura(osv.osv):
    """Lectures d'energia
    """

    _name = 'giscedata.lectures.lectura'
    _inherit = 'giscedata.lectures.lectura' 
    
    def _get_default_origen(self, cursor, uid, context=None):
        """Retorna l'ID de l'orígen manual (codi MA)
        """
        origens = self.pool.get('giscedata.lectures.origen_comer')
        origen_id = origens.search(cursor, uid, [('codi', '=', 'MA')])
        if origen_id:
            return origen_id[0]
        return False

    _columns = {
        'origen_comer_id': fields.many2one('giscedata.lectures.origen_comer', 
                                           'Orígen Comer.', required=True),
    }
    
    _defaults = {
        'origen_comer_id': _get_default_origen,
    }
GiscedataLecturesLectura()

class GiscedataLecturesPotencia(osv.osv):
    """Lectures de potència.
    """

    _name = 'giscedata.lectures.potencia'
    _inherit = 'giscedata.lectures.potencia'
    
    def _get_default_origen(self, cursor, uid, context=None):
        """Retorna l'ID de l'orígen manual (codi MA)
        """
        origens = self.pool.get('giscedata.lectures.origen_comer')
        origen_id = origens.search(cursor, uid, [('codi', '=', 'MA')])
        if origen_id:
            return origen_id[0]
        return False
    
    _columns = {
        'origen_comer_id': fields.many2one('giscedata.lectures.origen_comer', 
                                           'Orígen Comer.', required=True),
    }
    _defaults = {
        'origen_comer_id': _get_default_origen,
    }
GiscedataLecturesPotencia()
