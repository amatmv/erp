# -*- coding: utf-8 -*-
{
    "name": "Lectura de comptadors (Comercialitzadora)",
    "description": """Lectures dels comptadors""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_lectures"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_lectures_view.xml",
        "giscedata_lectures_data.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
