# -*- coding: utf-8 -*-
{
    "name": "GISCE Indústria (Per Línia) multicompany",
    "description": """Multi-company support for Indústria""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_industria_perlinia"
    ],
    "init_xml":[
        "record_rules.xml"
    ],
    "demo_xml": [],
    "update_xml":[
        "giscedata_industria_perlinia_multicompany_view.xml",
        "giscedata_industria_perlinia_multicompany_wizard.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
