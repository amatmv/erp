# -*- coding: utf-8 -*-

from osv import osv, fields
from giscedata_administracio_publica_cnmc_distri.wizard.wizard_recalcul_cinis_tis import MODELS_SELECTION
from tools.translate import _
from giscedata_administracio_publica_cnmc_distri.giscedata_tipus_installacio import TIPUS_REGULATORI

MODELS_SELECTION.append(
    ('giscedata.celles.cella', 'Cel·les'),
)


TIPUS_INTERRUPTOR = [
    ('1', 'Automàtic'),
    ('2', 'Seccionador')
]


TIPUS_POSICIO = [
    ('L', 'Línia'),
    ('R', 'Rupto')
]

TIPUS_CNMC = [
    ('S', 'Seccionador'),
    ('R', 'Reconnectador'),
    ('T', 'Telesenyalitzador'),
    ('F', 'Fusible'),
    ('SA', 'Seccionalitzador'),
    ('I', 'Interruptor'),
    ('IS', 'Interruptor-seccionador')
]

TIPUS_TI = [
    ('S', 'TI-174 Seccionador (de cuchillas)'),
    ('R', 'TI-177 Reconectador'),
    ('RS', 'TI-179 Reconectador-Seccionador'),
    ('SF', 'TI-181 Seccionador Fusible (XS-SXS)'),
    ('AS', 'TI-182 Autoseccionador/Seccionalizador'),
    ('I', 'TI-183 Interruptor'),
    ('IS', 'TI-187 Interruptor-seccionador'),
    ('IST', 'TI-187A Interruptor-seccionador telecontrolado')
]

INSTALLACIONS = [
    ('giscedata.cts', 'CT'),
    ('giscedata.at.suport', 'Suport AT')
]


class GiscedataCellesAillament(osv.osv):
    _name = 'giscedata.celles.aillament'

    _columns = {
        'name': fields.char('Codi', size=16, required=True),
        'descripcio': fields.text('Descripció')
    }


GiscedataCellesAillament()


class GiscedataCellesTipusElement(osv.osv):
    _name = 'giscedata.celles.tipus.element'

    _columns = {
        'name': fields.char('Tipus element', size=256),
        'codi': fields.char('Codi tipus element', size=8),
        'codi_cnmc': fields.selection(TIPUS_CNMC, 'Tipus CNMC CINI'),
        'codi_ti': fields.selection(TIPUS_TI, 'Tipus CNMC TI')
    }


GiscedataCellesTipusElement()


class GiscedataCellesTipusPosicio(osv.osv):
    _name = 'giscedata.celles.tipus.posicio'

    _columns = {
        'name': fields.char('Tipus posició', size=256),
        'codi': fields.char('Codi tipus posició', size=8)
    }


GiscedataCellesTipusPosicio()


class GiscedataCellesCella(osv.osv):
    _name = 'giscedata.celles.cella'
    _description = 'Elements de Tall Alta Tensió'

    def name_get(self, cr, uid, ids, context=None):
        res = []
        if isinstance(ids, (int, long)):
            ids = [ids]

        celles = self.read(cr, uid, ids, ['name', 'descripcio'], context=context)
        for cella in celles:
            name = cella['name']
            if not name:
                name = ''

            descripcio = cella['descripcio']
            if not descripcio:
                descripcio = ''

            composed_name = "{} - {}".format(name.strip(), descripcio.strip())
            res.append((cella['id'], composed_name))

        return res

    def _tipus_instalacio_cnmc_id_inv(self, cursor, uid, ids, name, value,
                                      args, context=None):
        """
        Function that handles the assignation of the tipus_instalacio_cnmc_id

        :param cursor: Database cursor
        :param uid: User id
        :param ids:  Afected ids
        :param name: Name of the modified element
        :param value: Value of the modified field
        :param args:
        :param context: Context OpenERP context
        :return: True
        """
        if context is None:
            context = {}
        if not isinstance(ids, (tuple, list)):
            ids = [ids]
        sql = """
        UPDATE giscedata_celles_cella SET tipus_instalacio_cnmc_id = %s
        WHERE id IN %s AND bloquejar_cnmc_tipus_install
        """
        cursor.execute(sql, (value or None, tuple(ids)))
        return True

    def _tipus_instalacio_cnmc_id(self, cursor, uid, ids, field_name,
                                  arg, context=None):

        """
        Function that handles the assignation of the tipus instalacio

        :param cursor: Ddatabase cursor
        :param uid: User id
        :param ids: Afected ids
        :param field_name: Name of the modified element
        :param arg:
        :param context: OpenERP Context
        :return: dict of condensador id , tipus installacio id
        """
        if not context:
            context = {}
        res = dict.fromkeys(ids, False)
        from tipoinstalacion.models import Fiabilidad
        for fia in self.browse(cursor, uid, ids, context=context):
            if fia.bloquejar_cnmc_tipus_install:
                res[fia.id] = fia.tipus_instalacio_cnmc_id.id
                continue
            ti = Fiabilidad()
            ti.tipoelemento = fia.tipus_element.codi_ti
            if ti.tipoinstalacion:
                ti_obj = self.pool.get('giscedata.tipus.installacio')
                if fia.telemando and ti.tipoinstalacion == "TI-187":
                    ti_ids = ti_obj.search(cursor, uid, [
                        ('name', '=', 'TI-187A')
                    ])
                else:
                    ti_ids = ti_obj.search(cursor, uid, [
                        ('name', '=', ti.tipoinstalacion)
                    ])
                if ti_ids:
                    res[fia.id] = ti_ids[0]
                else:
                    mess = 'TI {0} not found in giscedata.tipus.installacio'
                    raise mess.format(ti.tipoinstalacion)
        return res

    def _get_element_nobloc_ti(self, cursor, uid, ids, context=None):
        """
        Function that indicates when the tipus installacio is locked

        :param cursor: Database cursor
        :param uid: User id
        :param ids: Afected ids
        :param context:
        :return: List of non locked elements
        """
        search_params = [
            ('bloquejar_cnmc_tipus_install', '!=', 1),
            ('id', 'in', ids)
        ]
        return self.search(cursor, uid, search_params, context=context)

    def _default_installacio(self, cursor, uid, context=None):
        if not context:
            context = {}
        if 'installacio_id' in context and 'installacio_type' in context:
            return '%s,%s' % (context['installacio_type'],
                              context['installacio_id'])
        else:
            return False

    def _data_pm(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        res = dict.fromkeys(ids, False)
        for cella in self.browse(cursor, uid, ids, context):
            if cella.expedients and not cella.bloquejar_pm:
                res[cella.id] = max(e.industria_data for e in cella.expedients)
            else:
                res[cella.id] = cella.data_pm
        return res

    def _get_celles(self, cursor, uid, ids, context=None):
        cursor.execute("select e__c.cella_id from "
        "giscedata_expedients_expedient_cella_rel AS e__c "
        "LEFT JOIN giscedata_celles_cella AS c on (e__c.cella_id=c.id) "
        "where e__c.expedient_id in %s and not c.bloquejar_pm",
            (tuple(ids), ))
        return [x[0] for x in cursor.fetchall()]

    def _get_celles_nobloc(self, cursor, uid, ids, context=None):
        cursor.execute("select id from giscedata_celles_cella "
        "where not bloquejar_pm and id in %s", (tuple(ids), ))
        return [x[0] for x in cursor.fetchall()]

    def _get_celles_nobloc_cini(self, cursor, uid, ids, context=None):
        cursor.execute("select id from giscedata_celles_cella "
                       "where not bloquejar_cini and id in %s", (tuple(ids),))
        return [x[0] for x in cursor.fetchall()]

    def _cini(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        res = dict.fromkeys(ids, False)
        from cini.models import Fiabilidad
        has_celles_subestacions = 'posicio_id' in self.fields_get(cursor, uid)
        sub_obj = self.pool.get('giscedata.cts.subestacions')
        for cella in self.browse(cursor, uid, ids):
            fiabilidad = Fiabilidad()
            if cella.tensio:
                try:
                    fiabilidad.tension = cella.tensio.tensio / 1000.0
                except TypeError:
                    fiabilidad.tension = 0
            if cella.tipus_element.codi_cnmc:
                fiabilidad.tipo = cella.tipus_element.codi_cnmc
            fiabilidad.telemando = cella.telemando
            if not cella.installacio:
                continue
            values = cella.installacio.split(',')
            if len(values) != 2:
                continue
            tipus_instal, inst_id = values
            inst_id = int(inst_id)
            if has_celles_subestacions and cella.posicio_id:
                fiabilidad.situacion = 'SE'
            if tipus_instal == 'giscedata.cts':
                fiabilidad.situacion = 'CT'
                # No hi ha el modul giscedata_celles_subestacions i per tant
                # no ens podem refiar només del camp 'posicio_id' i mirem si
                # el CT que té assignat és una subestació.
                if sub_obj and sub_obj.search_count(cursor, uid, [
                        ('ct_id', '=', inst_id)
                ]):
                    fiabilidad.situacion = 'SE'
            elif tipus_instal == 'giscedata.at.suport':
                fiabilidad.situacion = 'LAT'
            res[cella.id] = str(fiabilidad.cini)
        return res

    def _cini_inv(self, cursor, uid, ids, name, value, args, context=None):
        if not context:
            context = {}

        if not isinstance(ids, (tuple, list)):
            ids = [ids]

        cursor.execute("update giscedata_celles_cella set cini=%s "
                       "where id in %s and bloquejar_cini",
                       (value or None, tuple(ids),))
        return True

    def _overwrite_apm(self, cursor, uid, ids, field_name, field_value, args,
                       context=None):
        if not isinstance(ids, (tuple, list)):
            ids = [ids]

        cursor.execute("update giscedata_celles_cella set data_pm=%s "
                       "where id in %s and bloquejar_pm",
                       (field_value or None, tuple(ids),))

    def _aillament(self, cursor, uid, context=None):
        if not context:
            context = {}
        aill_obj = self.pool.get('giscedata.celles.aillament')
        ids = aill_obj.search(cursor, uid, [], context=context)
        return [
            (a['id'], a['name']) for a in aill_obj.read(cursor, uid, ids)
        ]

    def _tipus_element(self, cursor, uid, context=None):
        if not context:
            context = {}
        tipus_obj = self.pool.get('giscedata.celles.tipus.element')
        ids = tipus_obj.search(cursor, uid, [], context=context)
        return [
            (a['id'], a['name']) for a in tipus_obj.read(cursor, uid, ids)
        ]

    def _tipus_posicio(self, cursor, uid, context=None):
        if not context:
            context = {}
        tipus_obj = self.pool.get('giscedata.celles.tipus.posicio')
        ids = tipus_obj.search(cursor, uid, [], context=context)
        return [
            (a['id'], a['name']) for a in tipus_obj.read(cursor, uid, ids)
        ]

    def _installacio_name(self, cursor, uid, ids, field_name, arg,
                          context=None):
        """
        Function that build the composed name of the installacio where the cella
        is linked.

        :param      cursor: Database cursor
        :type       cursor: cursor
        :param      uid:    User id
        :type       uid:    integer
        :param      ids:    Afected ids
        :type       ids:    list
        :param      field_name: Unused
        :param      arg:        Unused
        :param      context:    OpenERP context
        :type       context:    dict
        :return:    Dictionari with celles ids as a key and the values of the
                    installation composed name related to each id.
        """
        res = dict.fromkeys(ids, False)
        # Obtenim el llistat així, perquè les obtindrem traduides segons el
        # context
        selection = dict(
            self.fields_get(cursor, uid, ['installacio'],
                            context=context)['installacio']['selection']
        )
        for cella in self.read(cursor, uid, ids, ['installacio']):
            if cella['installacio']:
                obj, obj_id = cella['installacio'].split(',')
                selection_name = selection[obj]
                obj = self.pool.get(obj)
                obj_id = int(obj_id)
                if obj_id == 0:
                    deleted_element_msg = _(
                        "Instal·lació eliminada, s'ha de reasignar l'element"
                    )
                    res[cella['id']] = "{}: {}".format(
                        selection_name, deleted_element_msg
                    )
                else:
                    obj_id = int(obj_id)
                    res[cella['id']] = "{}: {}".format(
                        selection_name, obj.name_get(cursor, uid, obj_id)[0][1]
                    )
        return res

    def _installacio_name_search(self, cursor, uid, obj, name, args,
                                 context=None):
        """
        Function that build the domain of installation search

        :param      cursor:     Database cursor
        :type       cursor:     cursor
        :param      uid:        User id
        :type       uid:        integer
        :param      obj:        Unused
        :param      name:       Unused
        :param      context:    OpenERP context
        :type       context:    dict
        :return:    list with the domain filter inside.
        """
        domain = []
        for tipus_inst in INSTALLACIONS:
            obj = self.pool.get(tipus_inst[0])
            res = obj.name_search(cursor, uid, name=args[0][2], context=context)
            domain += ['%s,%s' % (tipus_inst[0], res_id[0]) for res_id in res]

        search_params = [('installacio', 'in', domain)]

        if len(args[0][2]) > 1:
            r = [
                 x[0] for x in
                 self.fields_get(
                     cursor, uid, ['installacio'], context=context
                 )['installacio']['selection']
                 if args[0][2].lower() in x[1].lower()
                 ]
            if r:
                search_params.insert(0, '|')
                search_params.append(('installacio', 'like', r[0]))

        return search_params

    _columns = {
        'name': fields.char('Codi', 25, required=True),
        'descripcio': fields.char('Descripcio', 128),
        'installacio': fields.reference('Instal·lació', INSTALLACIONS, 150,
                                        required=True),
        'installacio_name': fields.function(
            _installacio_name,
            fnct_search=_installacio_name_search,
            type='char',
            method=True,
            string='Nom instal·lació',
            size=256,
        ),
        'cini': fields.function(
            _cini,
            type='char', size=8, method=True, store={
                'giscedata.celles.cella': (
                    _get_celles_nobloc_cini, [
                        'tensio', 'telemando', 'tipus_element',
                        'bloquejar_cini', 'installacio'
                    ], 10
                )
            }, fnct_inv=_cini_inv, string='CINI'
        ),
        'bloquejar_cini': fields.boolean('Bloquejar CINI'),
        'tensio': fields.many2one('giscedata.tensions.tensio', 'Tensió',
                                  required=True),
        'inventari': fields.selection([
            ('l2+p', 'L2 + P'),
            ('fiabilitat', 'Fiabilitat')
        ], 'Categorització inventari', required=True),
        'tipus_element': fields.many2one(
            'giscedata.celles.tipus.element',
            'Tipus Element',
            selection=_tipus_element,
            required=True,
            ondelete='restrict'
        ),
        'tipus_posicio': fields.many2one(
            'giscedata.celles.tipus.posicio',
            'Tipus posició',
            selection=_tipus_posicio,
            required=True,
            ondelete='restrict'
        ),
        'aillament': fields.many2one(
            'giscedata.celles.aillament',
            'Fluid aïllant',
            type='integer',
            selection=_aillament,
            required=True,
            ondelete='restrict'
        ),
        'serial': fields.many2one("stock.production.lot",
                                  "Número de sèrie", select="1"),
        'product_id': fields.related('serial', 'product_id', type='many2one',
                                     relation='product.product', store=True,
                                     readonly=True, string="Marca i model"),
        'propietari': fields.boolean('Propietari'),
        'perc_financament': fields.float('% pagat per la companyia',
                                         required=True),
        'expedients': fields.many2many('giscedata.expedients.expedient',
                                       'giscedata_expedients_expedient_cella_rel',
                                       'cella_id', 'expedient_id',
                                       'Expedients'),
        'data_pm': fields.function(
            _data_pm, type='date', method=True, store={
                'giscedata.expedients.expedient': (
                    _get_celles, ['industria_data'], 10
                ),
                'giscedata.celles.cella': (
                    _get_celles_nobloc, ['expedients', 'bloquejar_pm'], 10
                )
            },
            fnct_inv=_overwrite_apm,
            string='Data APM', help=u"Data de l'acta de posada en marxa de la"
                                    u" instal·lació"
        ),
        'bloquejar_pm': fields.boolean('Bloquejar APM',
                                       help=u"Si està activat, agafa la data "
                                            u"entrada, si no, la de ,"
                                            u"l'expedient"),
        'active': fields.boolean('Actiu'),
        'data_baixa': fields.date('Data baixa'),
        'motoritzacio': fields.boolean('Motorització'),
        'telemando': fields.boolean('Telemando'),
        'cnmc_tipo_instalacion': fields.char(u'CNMC Tipus Instal·lació', size=10,
                                             readonly=False),
        'bloquejar_cnmc_tipus_install': fields.boolean(
            u'Bloquejar Tipus Instal·lació CNMC', readonly=False,
            help=u"Si està activat, permet modificar manualment el "
                 u"tipus d'instal·lació de la CNMC"
        ),
        'cedida': fields.boolean('Cedida'),
        'tipus_instalacio_cnmc_id': fields.function(
            _tipus_instalacio_cnmc_id,
            method=True,
            string='Tipologia CNMC',
            relation='giscedata.tipus.installacio',
            type='many2one',
            fnct_inv=_tipus_instalacio_cnmc_id_inv,
            store={
                'giscedata.celles.cella': (
                    _get_element_nobloc_ti, [
                        'tipus_element', 'bloquejar_cnmc_tipus_install'
                    ], 10
                )
            }
        ),
        'tram_id': fields.many2one(
            'giscedata.at.tram', u'Tram de línia AT',
            help="Tram al que s'aplica"),
        '4771_entregada': fields.json('Dades 4771 entregada'),
        '4131_entregada_2016': fields.json('Dades 4131 entregada 2016'),
        '4666_entregada_2017': fields.json('Dades entregades 4666 2017'),
        '4666_entregada_2018': fields.json('Dades entregades 4666 2018'),
        'observacions': fields.text('Observacions'),
        "criteri_regulatori": fields.selection(
            TIPUS_REGULATORI,
            required=True,
            string="Criteri regulatori",
            help="Indica si el criteri a seguir al genrar els fitxers d'"
                 "inventari.\nSegons criteri:S'usa el criteri normal\n Froçar "
                 "incloure:S'afegeix l'element al fitxer\n Forçar excloure: "
                 "L'element no apareixera mai",
            select=1
        ),
        '4666_entregada_2019': fields.json('Dades entregades 4666 2019')
    }

    _defaults = {
        'bloquejar_cini': lambda *a: 0,
        'propietari': lambda *a: 1,
        'perc_financament': lambda *a: 100,
        'installacio': _default_installacio,
        'active': lambda *a: 1,
        'motoritzacio': lambda *a: 0,
        'telemando': lambda *a: 0,
        'bloquejar_pm': lambda *a: 0,
        'cedida': lambda *a: 0,
        'bloquejar_cnmc_tipus_install': lambda *a: 0,
        "criteri_regulatori": lambda *a: "criteri"
    }

GiscedataCellesCella()


class GiscedataCellesRele(osv.osv):

    _name = 'giscedata.celles.rele'

    _rec_name = 'serial'

    def fill_reles_params(self, cursor, uid, ids, context=None):
        """
        Function that fill params of the rele by the category

        :param      cursor:     Database cursor
        :type       cursor:     cursor
        :param      uid:        User id
        :type       uid:        integer
        :param      ids:        Afected ids
        :type       ids:        integer list
        :param      context:    OpenERP context
        :type       context:    dict
        :return:    True
        """
        param_t_obj = self.pool.get('giscedata.celles.rele.param.template')
        param_obj = self.pool.get('giscedata.celles.rele.param')

        rele_read_fields = ['name', 'limits', 'value', 'step', 'group_id']

        for rele in self.browse(cursor, uid, ids):

            # Construim una llista dels parametres del rele
            rele_params = {}
            for p in rele.params:
                rele_params[p.id] = p.read()[0]
                rele_params[p.id].pop('id')

            group_id = rele.serial.product_id.categ_id.id
            param_t_ids = param_t_obj.search(cursor, uid, [
                ('group_id.category_ids.id', '=', group_id)
            ])

            param_templates = param_t_obj.read(cursor, uid, param_t_ids,
                                               rele_read_fields)
            reles_to_delete = []
            for param_t in param_templates:
                # Per cada parametre existent, busquem si el rele te algun
                # parametre que coincidexi
                for rele_param_id, rele_param_v in rele_params.items():
                    # S'ha de complir que, per tots els camps llegits, siguin
                    # iguals en tots els reles comparats
                    if all([
                        rele_param_v[rele_field] == param_t[rele_field]
                        for rele_field in rele_read_fields
                    ]):
                        reles_to_delete.append(rele_param_id)
                        continue

                param_t.pop('id')
                new_param_values = param_t.copy()
                new_param_values['group_id'] = new_param_values['group_id'][0]
                new_param_values['rele_id'] = rele.id
                param_obj.create(cursor, uid, new_param_values)

            if rele_params:
                param_obj.unlink(cursor, uid, reles_to_delete)
        return True

    _columns = {
        'serial': fields.many2one(
            'stock.production.lot',
            'Número de sèrie',
            select=True
        ),
        'product_id': fields.related(
            'serial',
            'product_id',
            type='many2one',
            relation='product.product',
            store=True,
            readonly=True,
            string='Marca i model'
        ),
        'category_id': fields.related(
            'product_id',
            'categ_id',
            type='many2one',
            relation='product.category',
            readonly=True,
            string='Categoria'
        ),
        'params': fields.one2many(
            'giscedata.celles.rele.param', 'rele_id',
            'Paràmetres'
        ),
        'descripcio': fields.char('Descripció', size=128),
        "criteri_regulatori": fields.selection(
            TIPUS_REGULATORI,
            required=True,
            string="Criteri regulatori",
            help="Indica si el criteri a seguir al genrar els fitxers d'"
                 "inventari.\nSegons criteri:S'usa el criteri normal\n Froçar "
                 "incloure:S'afegeix l'element al fitxer\n Forçar excloure: "
                 "L'element no apareixera mai",
            select=1
        )

    }

    _defaults = {
        "criteri_regulatori": lambda *a: "criteri"
    }


GiscedataCellesRele()


# GRUPS
class GiscedataCellesReleGroups(osv.osv):

    _name = 'giscedata.celles.rele.groups'
    _description = 'Categories de Relés'
    _columns = {
        'name': fields.char('Nom', size=50, required=True),
        'category_ids': fields.many2many(
            'product.category',
            'giscedata_group_category_rel',
            'group_id',
            'category_id',
            'Categories assignades'
        ),
        'template_ids': fields.one2many(
            'giscedata.celles.rele.param.template',
            'group_id',
            'Paràmetres'
        )
    }
    _order = 'name'

GiscedataCellesReleGroups()


# PARAM TEMPLATE
class GiscedataCellesReleParametresTemplate(osv.osv):
    _name = 'giscedata.celles.rele.param.template'
    _description = 'Template dels parametres d\'un grup'
    _columns = {
        'group_id': fields.many2one(
            'giscedata.celles.rele.groups',
            'Grup'
        ),
        'name': fields.char(
            'Nom',
            size=50,
            required=True
        ),
        'value': fields.char(
            'Valor per defecte',
            size=50,
            required=True
        ),
        'limits': fields.char(
            'Límits',
            size=50,
            required=True
        ),
        'step': fields.char(
            'Pas',
            size=50,
            required=True
        )
    }
    _order = 'group_id'

GiscedataCellesReleParametresTemplate()


# PARAM
class GiscedataCellesReleParametres(osv.osv):
    _name = 'giscedata.celles.rele.param'
    _description = 'Conté i relaciona el valor d\'un parametre amb el seu relé'
    _columns = {
        'rele_id': fields.many2one(
            'giscedata.celles.rele',
            'Relé',
            required=True,
            ondelete='cascade'
        ),
        'group_id': fields.many2one(
            'giscedata.celles.rele.groups',
            'Grup',
            readonly=True
        ),
        'name': fields.char(
            'Nom',
            size=50,
            required=True,
            readonly=True
        ),
        'value': fields.char(
            'Valor',
            size=50,
            required=True
        ),
        'limits': fields.char(
            'Límits',
            size=50,
            required=True,
            readonly=True
        ),
        'step': fields.char(
            'Pas',
            size=50,
            required=True,
            readonly=True
        )
    }
    _order = "group_id"

GiscedataCellesReleParametres()
