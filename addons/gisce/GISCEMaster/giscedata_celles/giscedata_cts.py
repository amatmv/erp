# -*- coding: utf-8 -*-

from osv import osv, fields


class GiscedataCts(osv.osv):
    _name = 'giscedata.cts'
    _inherit = 'giscedata.cts'

    def _celles_ids(self, cursor, uid, ids, field_name, args, context=None):
        if not context:
            context = {}
        celles_obj = self.pool.get('giscedata.celles.cella')
        # Returns all cells, even active = False
        res = {i: [] for i in ids}
        ctx = context.copy()
        ctx.update({'active_test': False})
        for ct_id in ids:
            celles_ids = celles_obj.search(cursor, uid, [
                ('installacio', '=', 'giscedata.cts,%s' % ct_id)
            ], context=ctx)
            res[ct_id] = celles_ids
        return res

    def _celles_ids_inv(self, cursor, uid, obj_id, name, value, arg,
                        context=None):
        celles_obj = self.pool.get('giscedata.celles.cella')
        o2m = fields.one2many('giscedata.celles.cella', 'installacio')
        obj_id = 'giscedata.cts,%s' % obj_id
        o2m.set(cursor, celles_obj, obj_id, name, value, user=uid,
                context=context)
        return True

    _columns = {
        'celles_ids': fields.function(
            _celles_ids,
            type='one2many',
            obj='giscedata.celles.cella',
            string='Cel·les i elements de tall',
            fnct_inv=_celles_ids_inv,
            method=True
        )
    }

GiscedataCts()