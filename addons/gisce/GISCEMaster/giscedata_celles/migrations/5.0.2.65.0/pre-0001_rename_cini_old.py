# coding=utf-8
from oopgrade import oopgrade


def up(cursor, installed_version):
    if not installed_version:
        return
    oopgrade.rename_columns(cursor, {
        'giscedata_celles_cella': [('cini', 'cini_pre_auto_cella')]
    })


def down(cursor):
    oopgrade.rename_columns(cursor, {
        'giscedata_celles_cella': [('cini_pre_auto_cella', 'cini')]
    })


migrate = up
