import netsvc


def migrate(cursor, installed_version):
    if installed_version:
        logger = netsvc.Logger()
        logger.notifyChannel('migrations', netsvc.LOG_INFO,
                             "Migrem el camp ct_id al nou reference installacio")

        cursor.execute("update giscedata_celles_cella set installacio = 'giscedata.cts,' || ct_id where installacio is null")
        cursor.execute("alter table giscedata_celles_cella drop column ct_id")
        logger.notifyChannel('migration', netsvc.LOG_INFO, "Fet")