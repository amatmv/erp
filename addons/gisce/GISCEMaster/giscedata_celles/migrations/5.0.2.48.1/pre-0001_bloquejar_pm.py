# -*- coding: utf-8 -*-

import netsvc
import pooler


def migrate(cursor, installed_version):
    """Canvis a executar relatius a giscedata_at
    """
    if installed_version:
        logger = netsvc.Logger()
        logger.notifyChannel('migration', netsvc.LOG_INFO, 'Migration from %s'
                             % installed_version)

        taula = 'giscedata_celles_cella'

        cursor.execute("select count(column_name) as existeix "
                       "FROM information_schema.columns "
                       "WHERE table_name='%s' "
                       "and column_name='bloquejar_pm'" % taula)

        existeix_columna = cursor.dictfetchall()[0]['existeix']

        if existeix_columna:
            logger.notifyChannel('migration', netsvc.LOG_INFO,
                                 u"Ja existeix la columna bloquejar_pm...")
        else:
            cursor.execute("alter table %s ADD column bloquejar_pm boolean" % taula)
