# -*- coding: utf-8 -*-
import netsvc
import pooler


def migrate(cursor, installed_version):
    if installed_version:
        logger = netsvc.Logger()

        uid = 1
        pool = pooler.get_pool(cursor.dbname)

        inst_name = u"Cel·les"
        model = 'giscedata.celles.cella'
        taula = 'giscedata_celles_cella'

        logger.notifyChannel('migrations', netsvc.LOG_INFO,
                             "Modificar %s.bloquejar_pm " % inst_name)

        #Omplim bloquejar_pm en funció de data_pm
        cursor.execute("update %s set bloquejar_pm=(data_pm is not null) "
                       "WHERE bloquejar_pm IS NULL" % taula)

        logger.notifyChannel('migration', netsvc.LOG_INFO,
                             u"Modificar %s.bloquejar_pm realitzat "
                             u"correctament" % inst_name)

        logger.notifyChannel('migration', netsvc.LOG_INFO,
                             u"Migrant data_pm de %s" % inst_name)
        inst_obj = pool.get(model)

        cursor.execute('SELECT id FROM %s '
                       'WHERE data_pm IS NULL and NOT bloquejar_pm' % taula)

        insts = cursor.fetchall()
        total = len(insts)
        counter = 0
        for res in insts:
            inst_obj.write(cursor, uid, res[0], {})
            if (counter % 100) == 0:
                logger.notifyChannel('migration', netsvc.LOG_INFO,
                                     u"%d/%d" % (counter, total))
            counter += 1

        logger.notifyChannel('migration', netsvc.LOG_INFO,
                             u"Migrat data_pm de %d %s "
                             u"correctament" % (total, inst_name))