# -*- coding: utf-8 -*-
{
    "name": "Cel·les i elements de tall",
    "description": """
    * Permet gestionar les cel·les i els elemenets de tall d'un CT
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_cts",
        "giscedata_tensions",
        "stock",
        "giscedata_at",
        "giscedata_administracio_publica_cnmc_distri"
    ],
    "init_xml": [],
    "demo_xml":[
        "giscedata_celles_demo.xml"
    ],
    "update_xml":[
        "giscedata_celles_view.xml",
        "giscedata_cts_view.xml",
        "giscedata_at_view.xml",
        "security/ir.model.access.csv",
        "giscedata_tipus_installacio_celles_data.xml"
    ],
    "active": False,
    "installable": True
}
