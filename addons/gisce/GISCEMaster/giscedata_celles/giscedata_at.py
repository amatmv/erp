# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscedataAtSuport(osv.osv):
    _name = 'giscedata.at.suport'
    _inherit = 'giscedata.at.suport'

    def _celles_ids(self, cursor, uid, ids, field_name, args, context=None):
        celles_obj = self.pool.get('giscedata.celles.cella')
        res = {i: [] for i in ids}
        for inst_id in ids:
            celles_ids = celles_obj.search(cursor, uid, [
                ('installacio', '=', '%s,%s' % (self._name, inst_id))
            ])
            res[inst_id] = celles_ids
        return res

    def _celles_ids_inv(self, cursor, uid, obj_id, name, value, arg,
                        context=None):
        celles_obj = self.pool.get('giscedata.celles.cella')
        o2m = fields.one2many('giscedata.celles.cella', 'installacio')
        obj_id = '%s,%s' % (self._name, obj_id)
        o2m.set(cursor, celles_obj, obj_id, name, value, user=uid,
                context=context)
        return True

    def _tenen_cella(self, cursor, uid, ids, field_name, args, context=None):
        """
        Compute the value of the field 'seccionador' of the model
        giscedata.at.suport. It computes the value for the affected records
        specified by the 'ids' param and for those elements set on True since
        there is no way to trigger when the record is no longer related to any
        giscedata.celles.cella element.
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :param ids: Affected IDS
        :type ids: list of int
        :param field_name: Not Used
        :type field_name: Not Used
        :param args: Not Used
        :type args: Not Used
        :param context: Not Used
        :type context: Not Used
        :return: The updated values for the field 'seccionador' of the affected
        elements.
        :rtype: dict[int,boolean]
        """

        # We recheck here all the giscedata.at.suport elements with the field
        # 'seccionador' set on True to see if already have a
        # giscedata.celles.cella element related
        sql = """
            SELECT id
            FROM giscedata_at_suport
            WHERE seccionador
        """
        cursor.execute(sql)
        rows = cursor.fetchall()
        for row in rows:
            ids.append(row[0])

        # Compute the value for all affected IDS
        res = dict.fromkeys(ids, False)
        composed_ref = []
        for sup_id in ids:
            composed_ref.append("giscedata.at.suport,"+str(sup_id))
        sql = """
            SELECT installacio
            FROM giscedata_celles_cella
            WHERE installacio IN %(refs)s
        """
        cursor.execute(sql, {'refs': tuple(composed_ref)})
        rows = cursor.fetchall()
        for row in rows:
            sup_id = int(row[0].split(',')[1])
            res[sup_id] = True

        return res

    def _sups_to_update(self, cr, uid, ids, context=None):
        """
        Compute the IDS of all the giscedata.at.suport records to update the
        fields 'seccionador'
        :param cr: Database cursor
        :type cr: Cursor
        :param uid: User ID
        :type uid: int
        :param ids: Changed records IDS of the model giscedata.celles.cella
        :type ids: list of int
        :param context: Not Used
        :type context: Not Used
        :return: The IDS of all the giscedata.at.suport records to be updated
        :rtype list of int
        """
        celles_obj = self.pool.get('giscedata.celles.cella')
        celles_data = celles_obj.read(cr, uid, ids, ['installacio'])
        ids_to_update = []
        for cella_data in celles_data:
            if 'giscedata.at.suport' in cella_data['installacio']:
                sup_id = cella_data['installacio'].split(',')[1]
                ids_to_update.append(int(sup_id))
        return ids_to_update

    _columns = {
        'celles_ids': fields.function(
            _celles_ids,
            type='one2many',
            obj='giscedata.celles.cella',
            string='Cel·les i elements de tall',
            fnct_inv=_celles_ids_inv,
            method=True
        ),
        'seccionador': fields.function(
            _tenen_cella,
            type='boolean',
            string='Seccionador',
            readonly=True,
            method=True,
            store={
                'giscedata.celles.cella': (
                    _sups_to_update, ['installacio'], 10
                )
            }
        )
    }


GiscedataAtSuport()


class GiscedataATLinia(osv.osv):
    _name = 'giscedata.at.linia'
    _inherit = 'giscedata.at.linia'

    def _celles_ids(self, cursor, uid, ids, field_name, args, context=None):
        suport_obj = self.pool.get('giscedata.at.suport')
        res = {i: [] for i in ids}
        for lat in self.read(cursor, uid, ids, ['suports'], context=context):
            for suport in suport_obj.read(cursor, uid, lat['suports'],
                                          ['celles_ids'], context=context):
                res[lat['id']] += suport['celles_ids']
        return res

    _columns = {
        'celles_ids': fields.function(
            _celles_ids,
            type='one2many',
            obj='giscedata.celles.cella',
            string='Cel·les i elements de tall',
            method=True
        )
    }


GiscedataATLinia()
