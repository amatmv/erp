# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from destral import testing
from destral.transaction import Transaction


class TestCelles(testing.OOTestCase):
    """
    Tests for the giscedata_celles module
    """
    def test_write(self):
        """
        To test the  write of a giscedata_cella
        with bloquejar_cnmc_tipus_install
        :return: None
        """
        with Transaction().start(self.database) as txn:
            cella_obj = self.openerp.pool.get('giscedata.celles.cella')
            ti_obj = self.openerp.pool.get('giscedata.tipus.installacio')
            ti_new_id = ti_obj.search(txn.cursor,
                                   txn.user,
                                   [('name', '=', 'TI-0CU')])[0]
            ids = cella_obj.search(txn.cursor, txn.user, [])
            cella = cella_obj.read(txn.cursor, txn.user, ids[0])
            ti_original_id = cella['tipus_instalacio_cnmc_id'][0]

            # Test blocked
            values = {'bloquejar_cnmc_tipus_install': False}
            cella_obj.write(txn.cursor, txn.user, ids, values)
            cella = cella_obj.read(txn.cursor, txn.user, ids[0])
            self.assertEqual(int(cella['tipus_instalacio_cnmc_id'][0]),
                             ti_original_id)
            self.assertFalse(cella['bloquejar_cnmc_tipus_install'])
            values = {'tipus_instalacio_cnmc_id': ti_new_id}
            cella_obj.write(txn.cursor, txn.user, ids, values)
            cella = cella_obj.read(txn.cursor, txn.user, ids[0])
            self.assertEqual(int(cella['tipus_instalacio_cnmc_id'][0]),
                             ti_original_id)

            # Test with unblocked
            values = {'bloquejar_cnmc_tipus_install': True}
            cella_obj.write(txn.cursor, txn.user, ids, values)
            values = {'tipus_instalacio_cnmc_id': ti_new_id}
            cella_obj.write(txn.cursor, txn.user, ids, values)
            cella = cella_obj.read(txn.cursor, txn.user, ids[0])
            self.assertEqual(int(cella['tipus_instalacio_cnmc_id'][0]),
                             ti_new_id)
            self.assertTrue(cella['bloquejar_cnmc_tipus_install'])
            values = {'tipus_instalacio_cnmc_id': ti_original_id}
            cella_obj.write(txn.cursor, txn.user, ids, values)
            cella = cella_obj.read(txn.cursor, txn.user, ids[0])
            self.assertEqual(cella['tipus_instalacio_cnmc_id'][0],
                             ti_original_id)


class TestReles(testing.OOTestCase):
    def setUp(self):
        self.txn = Transaction().start(self.database)

    def test_assign_params_simple_category(self):
        """Test a rele with only one Param group and deleting the ones
        that don't match the assigned by the group"""
        cursor = self.txn.cursor
        uid = self.txn.user

        img_obj = self.openerp.pool.get('ir.model.data')
        rele_obj = self.openerp.pool.get('giscedata.celles.rele')

        rele_id = img_obj.get_object_reference(
            cursor, uid, 'giscedata_celles', 'rele_00'
        )[1]

        rele_obj.fill_reles_params(cursor, uid, [rele_id])

        rele = rele_obj.browse(cursor, uid, rele_id)
        params_list = [unicode(p.name) for p in rele.params]

        params_test = ['Arrancada 51/67 P', 'Corba', 'Dial/Temps',
                       'Temporitzat temps definit', 'Temporització']

        self.assertListEqual(params_list, params_test)

    def test_assign_params_multiple_category(self):
        """Test a rele with a multiple Param group and with a double execution
        of the refill function to test if they refill without duplicate"""
        cursor = self.txn.cursor
        uid = self.txn.user

        img_obj = self.openerp.pool.get('ir.model.data')
        rele_obj = self.openerp.pool.get('giscedata.celles.rele')

        rele_id = img_obj.get_object_reference(
            cursor, uid, 'giscedata_celles', 'rele_01'
        )[1]

        # First time to fill when empty
        rele_obj.fill_reles_params(cursor, uid, [rele_id])

        # Second time to make sure that the params don't get duplicated
        rele_obj.fill_reles_params(cursor, uid, [rele_id])

        rele = rele_obj.browse(cursor, uid, rele_id)
        params_list = [str(p.name) for p in rele.params]

        params_test = ['Arrancada 51/67 N', 'Corba', 'Dial',
                       'Temporitzat temps definit', 'Arrancada 50/67 P',
                       'Temporització']

        self.assertListEqual(params_list, params_test)

    def tearDown(self):
        self.txn.stop()
