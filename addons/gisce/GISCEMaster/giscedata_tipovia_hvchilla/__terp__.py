# -*- coding: utf-8 -*-
{
    "name": "Tipus de vies segons HVChilla",
    "description": """
    Aquest mòdul carrega els tipus de vies i codis de carrer segons hvchilla
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "base_extended"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_tipovia_hvchilla_data.xml"
    ],
    "active": False,
    "installable": True
}
