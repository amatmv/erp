# -*- coding: utf-8 -*-
from osv import osv
from giscedata_comerdist import OOOPPool


class GiscedataFacturacioFactura(osv.osv):
    """Sobreescrivim per aplicar bono social a l'exportació
    """
    _name = 'giscedata.facturacio.factura'
    _inherit = 'giscedata.facturacio.factura'

    def export_to_comer(self, cursor, uid, ids, force=False, context=None):
        fids = super(GiscedataFacturacioFactura,
                     self).export_to_comer(cursor, uid, ids, force, context)
        # Recalculem el bono social
        configs = {}
        for factura in self.browse(cursor, uid, ids, context):
            # Comprovem si ja està exportada?
            if factura.remote_id:
                # comprovem que existeixi factura.remote_id en remot
                config_id = factura.get_config().id
                configs.setdefault(config_id, [])
                configs[config_id].append(factura.remote_id)
        for config_id, factures in configs.items():
            proxy = OOOPPool.get_ooop(cursor, uid, config_id)
            factura_proxy = proxy.GiscedataFacturacioFactura
            factura_proxy.apply_bono_social(factures)
        return fids

GiscedataFacturacioFactura()
