# -*- coding: utf-8 -*-
{
    "name": "Bono Social (Comerdist)",
    "description": """
    This module provide :
        * Aplicar bono social en l'exportació de distribuidora a comercialitzadora
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_comerdist_facturacio_dist"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
