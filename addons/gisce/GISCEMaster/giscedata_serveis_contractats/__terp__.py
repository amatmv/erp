# -*- coding: utf-8 -*-
{
    "name": "Módulo de servicios contratados (Base)",
    "description": """
        Añade la pestaña de Servicios Contratados en el contrato.
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends": [
        'giscedata_polissa',
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        'giscedata_polissa_view.xml',
    ],
    "active": False,
    "installable": True
}
