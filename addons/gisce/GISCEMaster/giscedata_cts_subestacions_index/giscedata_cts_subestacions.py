from base_index.base_index import BaseIndex


class GiscedataCts(BaseIndex):
    """
    Class to index CTs
    """
    _name = 'giscedata.cts.subestacions'
    _inherit = 'giscedata.cts.subestacions'
    _index_title = '{obj.name}'
    _index_summary = '{obj.descripcio} ({obj.id_municipi.name})'

    def index_name(self, value):
        """
        Formats the CT name in variour formats to be indexed

        :param value: Name of ct
        :return: Formated names
        """
        number = ''.join(x for x in value if x.isdigit())
        content = [value, number, number.lstrip('0')]
        content = ' '.join(content)
        return content

    def date_and_year(self, data):
        """
        Returns a string 'date year' to let search by year only

        :param data: Date
        :return: Formated date
        """
        year = ''
        if data and len(data) > 4:
            year = ' {0}'.format(data[:4])

        return '{0}{1}'.format(data, year)

    _index_fields = {
        "cini": True,
        "name": index_name,
        "descripcio": True,
        "id_municipi.name": True,
        "potencia": lambda self, data: '{0}kVA'.format(data),
        "adreca": True,
        "data_pm": date_and_year,
        "tensio_p": lambda self, data: '{0}V'.format(data)
    }

    def get_lat_lon(self, cursor, uid, item, context=None):
        """
        Method to get latitude and longitude

        :param cursor: Database cursor
        :param uid: User id
        :param item: Item to index
        :param context: OpenERP context
        :return: Tuple with lat and lon
        """
        if context is None:
            context = {}
        srid = self.get_db_srid(cursor, uid)
        cursor.execute("""
        SELECT
            ST_Y(ST_Transform(
                    ST_SetSRID(ST_Point(%(x)s, %(y)s), %(srid)s),
                    4326)
                 ) AS lat,
            ST_X(ST_Transform(
                    ST_SetSRID(ST_Point(%(x)s, %(y)s), %(srid)s),
                    4326)
                 ) AS lon
        """, {'srid': srid, 'x': item.x or 0, 'y': item.y or 0, })
        res = cursor.fetchone()
        if not res:
            res = (0, 0)
        return res


GiscedataCts()
