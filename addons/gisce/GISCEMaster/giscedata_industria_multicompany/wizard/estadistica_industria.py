# -*- coding: utf-8 -*-
import wizard
import pooler

_escollir_any_form = """<?xml version="1.0"?>
<form string="Generar Estadística Indústria" col="2">
  <field name="year" />
  <field name="print_pdf" />
</form>"""

_escollir_any_fields = {
  'year': {'string': 'Any', 'type': 'char', 'size': 4, 'required': True},
  'print_pdf': {'string': 'Mostrar PDF al finalitzar', 'type': 'boolean'},
}

def _calc(self, cr, uid, data, context={}):
  data['print_ids'] = []
  estadistica_year = pooler.get_pool(cr.dbname).get('giscedata.industria.estadistica.year')
  estadistica = pooler.get_pool(cr.dbname).get('giscedata.industria.estadistica')
  company_id = pooler.get_pool(cr.dbname).get('res.users').browse(cr, uid, uid, context).company_id.id
  # Borrem dades antigues
  estadistica_year_ids = estadistica_year.search(cr, uid, [('name', '=', data['form']['year']), ('company_id', '=', company_id)])
  estadistica_year.unlink(cr, uid, estadistica_year_ids)
  #Si ens eliminem l'any, per cascada ens eliminara les linies de l'estadistica d'aquell any
  #cr.execute("delete from giscedata_industria_estadistica")
  #cr.commit()

  # Per a cada provincia en (Trafos, LAT, LBT)
  cr.execute("select distinct p.id as id from giscedata_transformador_trafo t, giscedata_cts ct, res_municipi m, res_country_state p, giscedata_cts_installacio inst, giscedata_transformador_estat e, giscedata_transformador_connexio c where c.trafo_id = t.id and t.id_estat = e.id and ct.id_installacio = inst.id and t.ct = ct.id and ct.id_municipi = m.id and m.state = p.id and (t.tiepi = True or t.estadistica = True) and e.codi = 1 and c.conectada = True and t.company_id = %i union select distinct p.id as id from res_municipi m, res_country_state p, giscedata_at_linia lat, giscedata_at_tram tram where lat.municipi = m.id and m.state = p.id and tram.linia = lat.id and lat.baixa = False and lat.propietari = True and tram.baixa = False and lat.tensio is not null and tram.tipus in (1,2) and lat.company_id = %i union select distinct p.id as id from giscedata_bt_element e, res_municipi m, res_country_state p where e.municipi = m.id and m.state = p.id and (e.baixa = False or e.baixa is null) and e.tipus_linia in (1,2) and e.company_id = %i", (company_id, company_id, company_id))
  for provincia in cr.dictfetchall():
  
    # Creem el nou any
    year_id = estadistica_year.create(cr, uid, {'name': data['form']['year'], 'provincia': provincia['id']})
    
    # Calculem les dades dels trafos
    cr.execute("""
select count(p.name) as trafo_num,
       sum(t.potencia_nominal) as trafo_pot,
       case when c.connectada_p2 = True
         then
           case 
	         when c.tensio_p2 < 1000*1.05
	           then '1000'
	         when c.tensio_p2 >= 1000*1.05 and c.tensio_p2 < 4500*1.05
	           then '4500'
	         when c.tensio_p2 >= 4500*1.05 and c.tensio_p2 < 8000*1.05
	           then '8000'
	         when c.tensio_p2 >= 8000*1.05 and c.tensio_p2 < 12500*1.05
	           then '12500'
	         when c.tensio_p2 >= 12500*1.05 and c.tensio_p2 < 17500*1.05
	           then '17500'
	         when c.tensio_p2 >= 17500*1.05 and c.tensio_p2 < 25000*1.05
	           then '25000'
	         when c.tensio_p2 >= 25000*1.05 and c.tensio_p2 < 37500*1.05
	           then '37500'
	         when c.tensio_p2 >= 37500*1.05 and c.tensio_p2 < 55500*1.05
	           then '55500'
	         when c.tensio_p2 >= 55500*1.05 and c.tensio_p2 < 99000*1.05
	           then '99000'
	         when c.tensio_p2 >= 99000*1.05 and c.tensio_p2 < 176000*1.05
	           then '176000'
	         when c.tensio_p2 >= 176000*1.05 and c.tensio_p2 < 300000*1.05
	           then '300000'
	         when c.tensio_p2 >= 300000*1.05
	           then '300001'
	       end
	   else    
	       case 
	         when c.tensio_primari < 1000*1.05
	           then '1000'
	         when c.tensio_primari >= 1000*1.05 and c.tensio_primari < 4500*1.05
	           then '4500'
	         when c.tensio_primari >= 4500*1.05 and c.tensio_primari < 8000*1.05
	           then '8000'
	         when c.tensio_primari >= 8000*1.05 and c.tensio_primari < 12500*1.05
	           then '12500'
	         when c.tensio_primari >= 12500*1.05 and c.tensio_primari < 17500*1.05
	           then '17500'
	         when c.tensio_primari >= 17500*1.05 and c.tensio_primari < 25000*1.05
	           then '25000'
	         when c.tensio_primari >= 25000*1.05 and c.tensio_primari < 37500*1.05
	           then '37500'
	         when c.tensio_primari >= 37500*1.05 and c.tensio_primari < 55500*1.05
	           then '55500'
	         when c.tensio_primari >= 55500*1.05 and c.tensio_primari < 99000*1.05
	           then '99000'
	         when c.tensio_primari >= 99000*1.05 and c.tensio_primari < 176000*1.05
	           then '176000'
	         when c.tensio_primari >= 176000*1.05 and c.tensio_primari < 300000*1.05
	           then '300000'
	         when c.tensio_primari >= 300000*1.05
	           then '300001'
	       end	       
       end as t_name
     from 
       giscedata_transformador_trafo t,
       giscedata_cts ct, res_municipi m,
       res_country_state p,
       giscedata_transformador_estat e,
       giscedata_transformador_connexio c
     where 
       c.trafo_id = t.id 
       and t.id_estat = e.id 
       and t.ct = ct.id 
       and ct.id_municipi = m.id 
       and m.state = p.id 
       and p.id = %i 
       and (t.tiepi = True or t.estadistica = True) 
       and e.codi = 1 
       and c.conectada = True
       and t.company_id = %i
     group by t_name
    """, (provincia['id'], company_id))
    for trafo in cr.dictfetchall():
      trafo['name'] = trafo['t_name']
      del trafo['t_name']
      trafo['year'] = year_id
      estadistica.create(cr, uid, trafo)

    # Calculem les dades de linies AT
    cr.execute("select sum(greatest(tram.longitud_cad::float, tram.longitud_op::float)) as long, tram.tipus, tram.circuits, case when lat.tensio < 1000*1.05 then '1000' when lat.tensio >= 1000*1.05 and lat.tensio < 4500*1.05 then '4500' when lat.tensio >= 4500*1.05 and lat.tensio < 8000*1.05 then '8000' when lat.tensio >= 8000*1.05 and lat.tensio < 12500*1.05 then '12500' when lat.tensio >= 12500*1.05 and lat.tensio < 17500*1.05 then '17500' when lat.tensio >= 17500*1.05 and lat.tensio < 25000*1.05 then '25000' when lat.tensio >= 25000*1.05 and lat.tensio < 37500*1.05 then '37500' when lat.tensio >= 37500*1.05 and lat.tensio < 55500*1.05 then '55500' when lat.tensio >= 55500*1.05 and lat.tensio < 99000*1.05 then '99000' when lat.tensio >= 99000*1.05 and lat.tensio < 176000*1.05 then '176000' when lat.tensio >= 176000*1.05 and lat.tensio < 300000*1.05 then '300000' when lat.tensio >= 300000*1.05 then '300001' end as t_name from res_municipi m, res_country_state p, giscedata_at_linia lat, giscedata_at_tram tram where lat.municipi = m.id and m.state = p.id and p.id = %i and tram.linia = lat.id and lat.baixa = False and lat.propietari = True and tram.baixa = False and lat.tensio is not null and tram.tipus in (1,2) and lat.company_id = %i group by t_name, tram.tipus, tram.circuits", (provincia['id'], company_id))
    for lat in cr.dictfetchall():
      vals = {}
      vals['name'] = lat['t_name']
      vals['year'] = year_id
      if lat['circuits'] == 1 and lat['tipus'] == 1:
        vals['l_1c'] = lat['long']
      elif lat['circuits'] == 2:
        vals['l_2c'] = lat['long']
      elif lat['circuits'] > 2:
        vals['l_m2c'] = lat['long']
      elif lat['tipus'] == 2:
        vals['l_sub'] = lat['long']
      estadistica.create(cr, uid, vals)

    # Calculem les dades de linies BT
    cr.execute("select '1000' as t_name, sum(greatest(e.longitud_cad, e.longitud_op)) as long, e.tipus_linia from giscedata_bt_element e, res_municipi m, res_country_state p where e.municipi = m.id and m.state = p.id and p.id = %i and (e.baixa = False or e.baixa is null) and e.tipus_linia in (1,2) and e.company_id = %i group by e.tipus_linia", (provincia['id'], company_id))
    for lbt in cr.dictfetchall():
      vals = {}
      vals['name'] = lbt['t_name']
      vals['year'] = year_id
      if lbt['tipus_linia'] == 1:
        vals['l_1c'] = lbt['long']
      else:
        vals['l_sub'] = lbt['long']
      estadistica.create(cr, uid, vals)

    # Creem línies buides
    names = ['1000', '4500', '8000', '12500', '17500', '25000', '37500', '55500', '99000', '176000', '300000', '300001']
    for name in names:
      estadistica.create(cr, uid, {'name': name, 'year': year_id})

    data['print_ids'].append(year_id)

  return {}

def _check_print_report(self, cr, uid, data, context={}):
  if len(data['print_ids']) and data['form']['print_pdf']:
    return 'print_report'
  elif not len(data['print_ids']):
    return 'end'
  elif len(data['print_ids']) and not data['form']['print_pdf']:
    return 'mostrar_tab'

def _print_report(self, cr, uid, data, context={}):
  return {'ids': data['print_ids']}

def _mostrar_tab(self, cr, uid, data, context={}):
  action = {
    'domain': "[('id','in', ["+','.join(map(str,map(int, data['print_ids'])))+"])]",
		'view_type': 'form',
		'view_mode': 'tree,form',
		'res_model': 'giscedata.industria.estadistica.year',
		'view_id': False,
    'limit': len(data['print_ids']),
		'type': 'ir.actions.act_window'
  }
  return action

def _pre_end(self, cr, uid, data, context={}):
  return {}


class wizard_estadistica_industria(wizard.interface):

  states = {
    'init': {
    	'actions': [],
      'result': {'type': 'state', 'state': 'escollir_any'}
    },
    'escollir_any': {
      'actions': [],
      'result': {'type': 'form', 'arch': _escollir_any_form, 'fields': _escollir_any_fields, 'state': [('calc', 'Generar')]}
    },
    'calc': {
    	'actions': [_calc],
      'result': {'type': 'choice', 'next_state':_check_print_report}
    },
    'print_report': {
      'actions': [_print_report],
      'result': {'type': 'print', 'report': 'giscedata.industria.estadistica', \
        'get_id_from_action':True, 'state':'pre_end'}
    },
    'mostrar_tab': {
      'actions': [],
      'result': {'type': 'action', 'action': _mostrar_tab, 'state': 'pre_end'}
    },
    'pre_end': {
    	'actions': [_pre_end],
    	'result': { 'type' : 'state', 'state' : 'end' },
    },
  }

wizard_estadistica_industria('giscedata.industria.estadistica.multicompany')


