# -*- coding: iso-8859-1 -*-
from osv import osv, fields

class giscedata_industria_estadistica_year(osv.osv):
	_name = "giscedata.industria.estadistica.year"
	_inherit = "giscedata.industria.estadistica.year"
	_columns = {
		'company_id': fields.many2one('res.company', 'Company', required=True),
	}
	_defaults = {
		'company_id': lambda self,cr,uid,c: self.pool.get('res.users').browse(cr, uid, uid, c).company_id.id,
	}
giscedata_industria_estadistica_year()

class giscedata_industria_estadistica(osv.osv):
  _name = 'giscedata.industria.estadistica'
  _inherit = 'giscedata.industria.estadistica'

  _columns = {
		'company_id': fields.many2one('res.company', 'Company', required=True),
	}

  _defaults = {
		'company_id': lambda self,cr,uid,c: self.pool.get('res.users').browse(cr, uid, uid, c).company_id.id,
	}

  def create(self, cr, uid, vals, context={}):
    ids = self.search(cr, uid, [('name', '=', vals['name']), ('year', '=', vals['year']), ('company_id', '=', self.pool.get('res.users').browse(cr, uid, uid, context).company_id.id)])
    if len(ids):
      self.write(cr, uid, ids[0], vals)
      return ids[0]
    else:
      return super(osv.osv, self).create(cr, uid, vals, context)

giscedata_industria_estadistica()
