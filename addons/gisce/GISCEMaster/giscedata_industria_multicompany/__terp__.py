# -*- coding: utf-8 -*-
{
    "name": "GISCE Indústria multicompany",
    "description": """Multi-company support for Indústria""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Multi Company",
    "depends":[
        "giscedata_industria"
    ],
    "init_xml":[
        "record_rules.xml"
    ],
    "demo_xml": [],
    "update_xml":[
        "giscedata_industria_multicompany_view.xml",
        "giscedata_industria_wizard.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
