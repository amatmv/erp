# -*- coding: utf-8 -*-
{
    "name": "Infraestructura (Base)",
    "description": """*Menu base per tots els mòduls de 
  infraestructura com CT's, linies, comptadors o telecomunicacions.""",
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "GISCEMaster",
    "depends": [],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "infraestructura_menu.xml"
    ],
    "active": False,
    "installable": True
}
