# -*- coding: utf-8 -*-
{
    "name": "Punt Frontera",
    "description": """
    
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscegis_base"
    ],
    "init_xml": [],
    "demo_xml": [
        "giscedata_punt_frontera_demo.xml",
        "giscedata_punt_frontera_sequence_demo.xml"
    ],
    "update_xml":[
        "security/giscedata_punt_frontera_security.xml",
        "security/ir.model.access.csv",
        "giscedata_punt_frontera_data.xml",
        "giscedata_punt_frontera_sequence.xml",
        "giscedata_punt_frontera_view.xml"
    ],
    "active": False,
    "installable": True
}
