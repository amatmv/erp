# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscedataPuntFronteraTipus(osv.osv):

    _name = 'giscedata.punt.frontera.tipus'
    _description = "Tipus de Punts Frontera"

    def create(self, cr, uid, vals, context=None):
        """
        Patched create to avoid create duplicated types of Punts Frontera
        :param cr: Database cursor
        :type cr: Cursor
        :param uid: User ID
        :type uid: int
        :param vals: Attributes values of the element to be created
        :type vals: dict[str, Any]
        :param context: OpenERP context
        :type context: dict[str, Any]
        :return: Id of the new created element
        :rtype: int
        """

        ids = self.search(cr, uid, [('name', '=', vals['name'])])
        if ids and len(ids):
            return ids[0]
        else:
            return super(osv.osv, self).create(cr, uid, vals)

    _columns = {
      'name': fields.char('Tipus', size=64, required=True, select=1),
      'description': fields.char('Descripció', size=256),
    }

    _order = "name, id"


GiscedataPuntFronteraTipus()


class GiscedataPuntFrontera(osv.osv):

    def _get_next_name(self, cursor, uid, context=None):
        """
        Function to get the next name of the sequence for the PuntFrontera class
        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :param context: OpenERP context
        :type context: dict[str, Any]
        :return: The next name for the PuntFrontera set by the sequence
        :rtype: str
        """

        return self.pool.get('ir.sequence').get(
            cursor, uid, self._name
        )

    _name = 'giscedata.punt.frontera'
    _description = "Punts Frontera"

    _columns = {
        'name': fields.char(
            'Nom', size=256, required=True, readonly=True, select=1
        ),
        'codi': fields.char('Codi', size=256, required=True, select=1),
        'tipus': fields.many2one(
            'giscedata.punt.frontera.tipus', 'Tipus', required=True, select=1
        ),
        "active": fields.boolean("Active")
    }

    _order = "name, id"

    _defaults = {
        'name': _get_next_name,
        'active': lambda *a: 1,
    }


GiscedataPuntFrontera()
