from osv import osv, fields


class GiscedataPolissa(osv.osv):
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    _columns = {
        'group_id': fields.many2one('giscedata.polissa.group', 'Grup', select=1)
    }

GiscedataPolissa()