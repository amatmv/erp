from osv import osv, fields


class GiscedataPolissaGroup(osv.osv):
    _name = 'giscedata.polissa.group'

    _columns = {
        'name': fields.char('Grup', size=64, required=True),
        'code': fields.char('Codi', size=10, required=True)
    }

GiscedataPolissaGroup()