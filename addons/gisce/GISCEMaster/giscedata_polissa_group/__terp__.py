# -*- coding: utf-8 -*-
{
    "name": "Grups per les pòlisses",
    "description": """Afegeix gestió de grups de pòlisses""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_polissa"
    ],
    "init_xml": [],
    "demo_xml":[
        "giscedata_polissa_group_demo.xml"
    ],
    "update_xml":[
        "giscedata_polissa_group_view.xml",
        "giscedata_polissa_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
