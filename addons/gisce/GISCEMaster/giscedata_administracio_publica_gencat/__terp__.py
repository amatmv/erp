# -*- coding: utf-8 -*-
{
    "name": "Administració pública (GENCAT)",
    "description": """Crea el menú d'administració pública com a base GENCAT""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_administracio_publica"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_administracio_publica_gencat_view.xml"
    ],
    "active": False,
    "installable": True
}
