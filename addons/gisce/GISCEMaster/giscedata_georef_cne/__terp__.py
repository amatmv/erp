# -*- coding: utf-8 -*-
{
    "name": "Fitxer georeferenciació CNE",
    "description": """
    This module provide :
      * Wizard per generar els fitxers de georeferenciació de la CNE.
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_cts",
        "giscedata_transformadors",
        "giscedata_at",
        "giscedata_bt",
        "giscedata_cups",
        "giscedata_polissa",
        "giscedata_cne"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/wizard_georef_file_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
