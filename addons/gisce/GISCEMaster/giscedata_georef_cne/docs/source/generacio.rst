Generació de fitxers georeferenciats per CNE segons Circular 1/2012 des de GISCE-ERP
=======================================================================================
Introducció:
-------------

En aquest document es detallen els camps que s'incorporen als informes georeferenciats F4,F5-AT,F5-BT y F7 
segons la circular 1/2012 de la CNE respecte als sol·licitats l'any 2011 i en relació a la informació 
gestionada mitjançant Gisce-ERP


F4 (Centres transformadors)
----------------------------

Els Centres transformadors que apareixen en aquest informe han de cumplir els següents requisits:

* Que el **tipus d'instal·lació** no sigui ni *Sub Estació* (SE) ni *Central de producció Hidràulica* (CH)

A més a més:

* el CT ha de tenir definit el **vèrtex**, el **municipi**, el **node** y la **categoria de la cne**.

.. _`F4 CTS`:
.. figure:: ./img/F4_CT_form_es.png
   :align: center
   
   Formulari de CTS amb camps per F4 georef CNE 2012

Aquest informe incorpora els següents camps:

* **Data de posada en marxa**:

	Indica a quina data es va posar en servei el centre transformador. 
	S'utiliza el camp ja existent  “*Data posada en marxa*” de la fitxa del CT en qüestió. 
	Si aquest camp està buit, s'utiliza la data “*Data Indústria*” de l'últim expedient rel·lacionat amb 
	el ct, es a dir, el de “*Data indústria*” més recent que se pot visualizar a la pestanya d'Expedients. 
	Si no se n'hi troba cap es deixa buit.

* **Propietari**:

	Indica si l'empresa distribuïdora és propietaria del centre transformador. 
	S'utiliza el nou camp “*Propietari*” de la fitxa del CT en qüestió. 
	Si està marcat, es considera que es propietat de l'empresa distribuïdora. 
	Per defecte, s'ha marcat aquest camp, de manera que tots els CT’s són propietat de la distribuïdora.

* **% de Finançament**

	Indica quin és el percentatge de finançament de l'empresa distribuïdora en la construcció del CT. 
	S'utiliza el nou camp “*% Finançament*” de la fitxa de CT’s. 
	Per defecte s'ha omplert aquest camp al 100 %.

F5-AT (Línies d'Alta Tensió)
------------------------------

Els trams de línia que apareixen en aquest informe han de cumplir els següents requisits:

* El **nom** de la línia diferent de *1*

A més a més: 

* El tram ha de tenir el **cable** i el **tipus de línia** correctament informat

.. _`F5 LAT`:
.. figure:: ./img/F5A_LAT_form_es.png
   :align: center
   
   Formulari de Línies d'Alta amb camps per F5-AT georef CNE 2012

.. _`F5 LAT Tram`:
.. figure:: ./img/F5A_Tram_LAT_form_es.png
   :align: center
   
   Formulari de Trams d'Alta amb camps per F5-AT georef CNE 2012

Aquest informe incorpora els següents camps en el formulari de Trams AT:

* **Data de posada en marxa**:

	Indica a quina data es va posar en servei el tram d'alta tensió. 
	S'utiliza el nou camp “*Data posada en marxa*” de la fitxa del Tram d'alta en qüestió. 
	Si aquest camp està buit, s'utiliza la data “*Data d'autorització d'Indústria*” de l'expedient més recent rel·lacionat amb 
	la línia d'Alta, és a dir, el de “*Data indústria*” més recent que se pot visualizar a la pestanya Expedients. 
	Si no se n'hi troba cap es deixa buit.
	
* **Propietari**:

	Indica si l'empresa distribuïdora és propietaria de la línia. 
	S'utiliza el camp ja existent “*Propietari*” de la fitxa de la Línia en qüestió. 
	Si està marcat, es considera que és propietat de l'empresa distribuïdora. 

* **% de Finançament**

	Indica quin és el percentatge de finançament de l'empresa distribuïdora en la construcció del tram de línia. 
	S'utiliza el nou camp “*% Finançament*” de la fitxa del tram d'alta en qüestió. 
	Per defecte s'ha omplert aquest camp al 100 %.
	
F5-BT (Línies de Baixa Tensió)
------------------------------

Els trams de línia que apareixen en aquest informe han de cumplir els següents requisits:

* No estar de **baixa** i que el **tipus de cable** no sigui *Embarrat* (E)

A més a més: 

* El tram ha de tenir el **cable** i el **tipus de línea** correctament informat

Aquest informe incorpora els següents camps:

.. _`F5 LBT`:
.. figure:: ./img/F5B_LBT_form_es.png
   :align: center
   
   Formulari de Trams de Baixa amb camps per F5-BT georef CNE 2012

* **Data de posada en marxa**:

	Indica a quina data es va posar en servei el tram de baixa tensió. 
	S'utiliza el camp ja existent  “*Data posada en marxa*” de la fitxa de l'Element en qüestió. 
	Si no se n'hi troba cap es deixa buit.

* **Propietari**:

	Indica si l'empresa distribuïdora és propietaria de la línia. 
	S'utiliza el nou camp “*Propietari*” de la fitxa de l'Element en qüestió. 
	Si està marcat, es considera que és propietat de l'empresa distribuïdora. 
	Per defecte, s'ha marcat aquest camp, de manera que tots els Trams són propietat de la distribuïdora.

* **% de Finançament**:

	Indica quin és el percentatge de finançament de l'empresa distribuïdora en la construcció del tram de línia. 
	S'utiliza el nou camp “*% Finançament*” de la fitxa del tram d'alta en qüestió. 
	Per defecte s'ha omplert aquest camp al 100 %.
		
* **Altres**:

	S'ha incorporat el nou camp “*Posat a façana*” per facilitar en el futur el càlcul del CINI, 
	ja que a la seva codificació s'ha de tenir en compte si el tram de línia és sobre pals o penjat de façana. 

F7 (CUPS)
---------

Els CUPS que apareixen en aquest informe són **tots** els que estan actius

Aquest informe incorpora els següents camps:

.. _`F7 CUPS`:
.. figure:: ./img/F7_CUPS_form_es.png
   :align: center
   
   Formulari de CUPS amb camps per F7 georef CNE 2012

* **Energia activa anual consumida**:

	Indica el consum d'energia activa del CUPS durant l'any anterior.
	Es mostra aquest camp a la fitxa del CUPS en qüestió.
	Es pot omplir amb un procés automàtic si Gisce-ERP incorpora el mòdul de facturació 
	o mitjançant la càrrega d'un fitxer CSV especialment preparat extret del sistema de facturació. 
	
* **Energia reactiva anual consumida**

	Indica el consum d'energia reactiva del CUPS durant l'any anterior.
	Es mostra aquest camp a la fitxa del CUPS en qüestió.
	Es pot omplir amb un procés automàtic si Gisce-ERP incorpora el mòdul de facturació 
	o mitjançant la càrrega d'un fitxer CSV especialment preparat extret del sistema de facturació.
	
* **Tensió**

	Indica la tensió assignada al CUPS.
	Per fer-ho, s'utiliza el camp "*Tensió*" de la línia de baixa que alimenta la escomesa associada al CUPS.

* **Equip de mesura**

	Indica quin tipus d'equip de mesura té el CUPS.
	En aquest moment se consideren tots "*Electromecánics*" (MEC). Es marquen com “*Contracte no actiu*” (CNA) 
	els que no tenen una pòlissa activa associada

F8 (Règim Especial)
-------------------

Aquest informe incorpora **tots** els CUPS que pertanyen a les instal·lacions de Règim Especial actives.
Destacar els següents camps de l'informe:

* **Energia anual consumida**

    Talment com en l'informe F7, aquest inclou les energies activa i reactiva anual consumides (camps 10 i 11 respectivament).
    Aquestes s'obtenen del formulari del CUPS. Veure imatge `F7 CUPS`_.

* **Potència instal·lada**

    La potència instal·lada (9è camp del document) es pren del valor de potència nominal de la instal·lació de Règim Especial.
    
.. _`F8 Règim Especial`:
.. figure:: ./img/F8_re_es.png
   :align: center
   
   Formulari de Règim Especial amb camps per F8 georef CNE 2012

        


