.. Wizard Generació Informes Georeferenciats de la CNE documentation master file, created by
   sphinx-quickstart on Fri May 25 16:31:54 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Benvinguts a la documentació del Wizard de Generació d'Informes Georeferenciats de la CNE 
=========================================================================================

Contents:

.. toctree::
   :maxdepth: 2

   wizard
   generacio
   
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

