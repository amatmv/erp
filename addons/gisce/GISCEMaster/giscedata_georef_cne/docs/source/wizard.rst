Wizard de generació de fitxers georeferenciats per CNE segons Circular 1/2012 des de GISCE-ERP
===============================================================================================
Introducció:
-------------
   
.. figure :: img/Wizard_F4_ca.png
   :scale: 75 %

Els informes georeferenciats de la CNE es generen a partir de la informació emmagatzemada a 
Gisce-ERP mitjançant una sèrie d'scripts de python. A més a més dels scripts que generen els 
informes hi ha altres scripts que els complementen per omplir dades rellevants de fonts 
externes que no es poden calcular automàticament utilitzant les dades de Gisce-ERP. Tots aquests 
scripts s'engloben dins el projecte `georef`_ allotjat al `github de Gisce`_.

Per permetre la generació dels informes des de Gisce-ERP s'ha desenvolupat un wizard 
específic. Aquest wizard el trobareu a **Administració Pública -> CNE -> Generar informes georeferenciació**

Es poden generar els següents fitxers georefereniats de la CNE:

* **F1**:\ [1]_\ Subestacions. Posicions amb interruptor. *(SE)*
* **F1bis**:\ [1]_\ Subestacions. Posicions sense interruptor. *(SEbis)*
* **F4**: Centres transformadors *(CT)*
* **F5-AT**: Línies d'alta tensió *(LAT)*
* **F5-BT**: Línies de baixa tensió *(LBT)*
* **F7**: CUPS *(CUPS)*
* **F8**: Règim Especial *(RE)*

.. [1]  La generació dels fitxers F1 i F1bis només estarà disponible en aquelles
   instal·lacions que disposin del mòdul de Subestacions

Característiques generals:
--------------------------

.. figure :: img/Wizard_F4_generat_ca.png
   :scale: 75 %

El Wizard està format per un sol formulari amb dos estats, *fitxer no generat* i *fitxer generat*.
Els camps imprescindibles per qualsevol fitxer són:

* **Tipus**: Especifica l'informe que es vol generar
* **Codi R1**: Codi de l'empresa distribuidora segons circular 1/2012 de la CNE
* **Num. Processadors**: Numero de processos que utilitzarem per la generació del fitxer. 

.. note:: 
   La generació de fitxers utilitza processos independents amb capacitat de
   multiprocés per accelerar-ne la generació. 

.. tip::
   El número de processos afecta a la velocitat de la generació del fitxer,
   però també als recursos del servidor utilitzats. Normalment, el valor per
   defecte **Automàtic** és una bona opció no obstant, en servidors compartits
   i en moments de molta càrrega, pot ésser convenient utilitzar menys
   processos.

Les accions que es poden realizar es realitzen prement el botó corresponent:

* **Generar Fitxer**: Genera el fitxer seleccionat en el desplegable *Tipus*
* **Tanca**: Tanca el wizard 

El fitxer resultant es pot descarregar i veure en el camp **Fitxer** (fitxer generat)

* **Input de text**: Mostra la mida del fitxer
* **Botó seleccionar**: No té funcionaliat
* **Obrir**: Obre el fitxer a l'editor de textes
* |arxivador_ico| : Descarrega el fitxer
* |neteja_ico| : Esborra l'accés al fitxer generat

Quan es descarrega el fitxer, el nom que es proposa per guardar ja és el nom recomanat per 
la circular de la CNE:

* **YYYY**: Any de generació de l'informe
* **FN**: Número d'informe
* **RRR**: Codi R1 de la distribuidora 

| ``RR_YYYY_FN_R1-RRR.txt``

P.e:

| ``RR_2011_F4_R1-000.txt``

Càrrega de energies per F7 (CUPS)
---------------------------------

Per la generació de l'informe F7 (CUPS) es necessita el consum d'energia activa i reactiva
de l'any anterior. Aquesta informació es pot calcular mitjançant Gisce-ERP si inclou el mòdul de 
facturació. 
En cas contrari, es pot carregar aquesta informació mitjançant un fitxer CSV 
conveninentment preparat extret del sistema extern de facturació.

Quan es selecciona l'informe F7, apareix automàticament un nou component del formulari en funció de
si Gisce-ERP té el mòdul de facturació o no. 

Gisce-ERP amb facturació. Recàlcul de energia anual
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. figure :: img/Wizard_F7_ca.png
   :scale: 75 %

Apareixen dos camps nous:

* **Any lectures**: 
  Any del qual es calcularan els consums. Per defecte any anterior a l'any en curs.
* **Recalcula camps energia**: 
  Si es marca aquest ''checkbox'' s'omplen els camps d'energia abans de la generació del fitxer utilitzant les factures de l'any seleccionat 

.. note::
   Un cop calculades les energies, no fa falta tornar a seleccionar el camp en successives generacions 
   del fitxer, ja que les dades queden emmagatzemades permanentment.

.. _CSV_Energia:

Gisce-ERP sense facturació. Càrrega energies anuals
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. figure :: img/Wizard_F7_CSV_ca.png
   :scale: 75 %
   
Apareix un nou camp de tipus fitxer **CSV**.

* **Botó seleccionar**: Ens permet escollir el fitxer CSV amb els consums

El fitxer CSV ha de contenir una línia per CUPS amb els camps separats per '**;**'
, en aquest ordre:
   
* Els 20 primers caracters del CUPS 
* Energia Activa consumida durant l'any anterior en kWh 
* Energia Reactiva consumida durant l'any anterior en kWh 

Els decimals es separaran per punts, p.e. 5.4

Un exemple:

| ``ES0999000000123456KH;103.4;12.03``
| ``ES0999000000234567LM;23.4;0``
| ``...``

.. note::
   Un cop carregades les energies mitjançant CSV, no fa falta tornar-les a carregar en 
   successives generacions del fitxer, ja que les dades queden emmagatzemades permanentment.

.. |arxivador_ico| image:: img/icona_fitxer.png
                   :align: middle

.. |neteja_ico| image:: img/icona_neteja.png
                :align: middle

.. _github de Gisce: https://github.com/gisce

.. _georef: https://github.com/gisce/georef

Càrrega d'energies per F8 (CUPS)
---------------------------------

En aquest moment únicament hi ha disponible la opció d'importar les energies dels CUPS de Règim Especial
a través de la càrrega d'energies anuals amb CSV. Tal i com pot fer-se amb els F7. Veure més amunt :ref:`CSV_Energia`.


