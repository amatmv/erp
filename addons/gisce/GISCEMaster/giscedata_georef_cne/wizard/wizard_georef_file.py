# -*- coding: utf-8 -*-
import subprocess
import tempfile
import base64
import pooler
import os
from datetime import datetime
from tools.translate import _
import multiprocessing

from osv import osv, fields
import tools


class WizardGeorefFile(osv.osv_memory):
    """Wizard per generar els informes de georeferenciació de la CNE.
    """
    _name = 'wizard.georef.file'

    _sqlfile = ("%s/giscedata_georef_cne/sql/recalcular_energies_cups.sql"
                % tools.config['addons_path'])

    _query = open(_sqlfile).read()

    def action_exec_script(self, cursor, uid, ids, context=None):
        """Llença l'script per l'informe.
        """

        script_name = {'se': ['georef_subest.py', 'F1'],
                       'sebis': ['georef_subest.py', 'F1bis'],
                       'cts': ['georef_cts.py', 'F4'],
                       'lbt': ['georef_lbt.py', 'F5-BT'],
                       'lat': ['georef_lat.py', 'F5-AT'],
                       'cups': ['georef_cups.py', 'F7'],
                       'kwh': ['georef_energia_anual.py', 'F7'],
                       're': ['georef_re.py', 'F8']}

        wizard = self.browse(cursor, uid, ids[0], context)

        any_energia = (wizard.type in ('cups', 're') and wizard.omple_camps_energia
                       and wizard.any_energia or False)
        fitxer_csv_energia = (wizard.type in ('cups', 're') and wizard.file_energia
                       or False)

        exe = script_name.get(wizard.type)[0]
        tmp = tempfile.mkstemp()[1]
        user = self.pool.get('res.users').browse(cursor, uid, uid)
        port = tools.config['port'] or 8069
        args = [exe, '-d', cursor.dbname, '-p', port]
        args += ['-u', user.login, '-w', user.password]
        args += ['-c', wizard.codi_r1.encode('utf-8'), '-o', tmp]
        args += ['--no-interactive']
        env = os.environ.copy()
        if wizard.num_procs != '*':
            env.update({'N_PROC': wizard.num_procs})

        if int(os.getenv('QUIET', '0')):
            args += ['--quiet']

        if wizard.type == 'se':
            args += ['--interruptor']
        elif wizard.type == 'sebis':
            args += ['--no-interruptor']

        if any_energia:
            # Omple mitjançant factures any seleccionat
            if wizard.type == 're':
                raise osv.except_osv(_(u'Atenció'),
                                 _(u"La opció de calcular el consum de CUPS "
                                   u"de RE no està disponible actualment."))
            self.recalcula_energia_cne_anual(cursor, any_energia)
        elif fitxer_csv_energia:
            # Carrega el fitxer CSV
            filename = tempfile.mkstemp()[1]
            data = base64.decodestring(wizard.file_energia)
            open(filename, 'w').write(data)
            cne_exe = script_name.get('kwh')[0]
            cne_args = [cne_exe, '-d', cursor.dbname, '-p', port,
                    '-u', user.login, '-w', user.password, '-f', filename]

            subprocess.call(map(str, cne_args))
            os.unlink(filename)

        retcode = subprocess.call(map(str, args), env=env)
        if retcode:
            raise osv.except_osv(
                'Error',
                _('El procés de generar el fitxer ha fallat. Codi: %s')
                % retcode
            )
        report = base64.b64encode(open(tmp).read())
        os.unlink(tmp)
        filename = ('RR_%d_%s_R1-%s.txt' %
                        (datetime.now().year - 1,
                        script_name.get(wizard.type)[1],
                        wizard.codi_r1[-3:]))

        wizard.write({'name': filename, 'file': report, 'state': 'done'})

    def recalcula_energia_cne_anual(self, cursor, any_factures):
        """Actualitza els camps cne_anual_(re)activa"""
        db = pooler.get_db_only(cursor.dbname)
        cr_tmp = db.cursor()
        cr_tmp.execute(self._query, (any_factures,))
        cr_tmp.commit()
        cr_tmp.close()

    def _tenim_facturacio(self, cursor, uid, ids, field_name, ar,
                          context=None):
        """Comprova si tenim facturació per escollir mètode
        de càrrega d'energia (wrapper per fields.function)"""
        res = {}
        cou = self.tenim_facturacio(cursor, uid, context)
        for wid in ids:
            res[wid] = cou
        return res

    def tenim_facturacio(self, cursor, uid, context=None):
        """Comprova si tenim facturació per escollir mètode
        de càrrega d'energia"""
        module_obj = self.pool.get('ir.module.module')
        cou = module_obj.search_count(cursor, uid, [
                        ('name', '=', 'giscedata_facturacio'),
                        ('state', '=', 'installed')])
        return cou

    def tenim_subestacions(self, cursor, uid, context=None):
        """Comprova si tenim subestacions per mostrar o no la
        opció de generació de F1'ns"""
        module_obj = self.pool.get('ir.module.module')
        cou = module_obj.search_count(cursor, uid, [
                        ('name', '=', 'giscedata_cts_subestacions'),
                        ('state', '=', 'installed')])
        return cou

    def _obte_tipus_informe(self, cursor, uid, context=None):
        """ Retorna la llista amb els fitxers que es poden generar """

        opcions = [('se', 'SE (F1)'), ('sebis', 'SEbis (F1bis)'),
                ('cts', 'CT (F4)'), ('lbt', 'LBT (F5)'),
                ('lat', 'LAT (F5)'), ('cups', 'CUPS (F7)'),
                ('re', 'RE (F8)')]

        #treure els F1's de les opcions
        cou = self.tenim_subestacions(cursor, uid, context)

        if not cou:
            opcions.pop(0)
            opcions.pop(0)

        return opcions

    _columns = {
        'type': fields.selection(_obte_tipus_informe,
               'Tipus', required=True),
        'file': fields.binary('Fitxer'),
        'codi_r1': fields.char('Codi R1', size=3, required=True),
        'state': fields.char('State', size=16),
        'name': fields.char('File name', size=64),
        'any_energia': fields.integer('Any lectures',
                size=4, help=('Any de les lectures per càlcul energia '
                              'anual per CUPS. Normalment any anterior')),
        'omple_camps_energia': fields.boolean("Recalcula els camps d'energia"),
        'file_energia': fields.binary('CSV',
                help=("Fitxer CSV amb les dades d'eneria consumida per CUPS "
                      "l'any anterior en KWh:\nESXXXXXXXXXX;activa;reactiva")),
        'te_fact': fields.function(_tenim_facturacio, type='boolean',
                                   method=True, string="Tenim facturació"),
        'num_procs': fields.selection(
            [('*', _('Automàtic'))]
            + [(str(i), _("%d Processos") % i) for i in range(1,
                                            multiprocessing.cpu_count() + 1)],
            'Num. Processadors', required=True,
            help=("Número de processos a utilitzar per generar el fitxer. Com "
                  "més processos, més ràpid, però utilitza més recursos. "
                  "Automàticament utilitza un procés per processador")),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'any_energia': lambda *a: datetime.now().year - 1,
        'omple_camps_energia': lambda *a: 0,
        'te_fact': tenim_facturacio,
        'num_procs': lambda *a: '*',
    }

WizardGeorefFile()
