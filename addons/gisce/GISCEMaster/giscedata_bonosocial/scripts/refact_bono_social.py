# -*- coding: utf-8 -*-
from loop import OOOP
from datetime import datetime
o = OOOP(dbname='oerp5_comer', port=18069)
HARDCODED_VALUES = {
    'name': 'P1',
    'tipus': 'energia',
    'product_id': 2,
    'uos_id': 3,
    'account_id': 1264,
    'invoice_line_tax_id': [(6,0, [1, 37])]
}
for fid in o.GiscedataFacturacioFactura.search([('lot_facturacio.id', '=', '16'),
                                                ('llista_preu.id', '=', 15)]):
    factura = o.GiscedataFacturacioFactura.get(fid)
    print "Factura %s" % factura.id
    # Eliminem les línies si són d'energia
    for linia in factura.linia_ids:
        if linia.tipus in ('energia', 'altres'):
            linia.unlink()
    # Recalculem les linies 
    for lectura in factura.lectures_energia_ids:
        if lectura.tipus == 'activa':
            consum = lectura.lect_actual - lectura.lect_anterior
            if consum < 0:
                consum += lectura.comptador_id.giro
            print "  Consum per comptador %s: %s" % (lectura.comptador_id.name,
                                                     consum)
            consum = float(consum)
            data_anterior = datetime.strptime(lectura.data_anterior, '%Y-%m-%d')
            data_actual =  datetime.strptime(lectura.data_actual, '%Y-%m-%d')
            dies_periode_lectura = (data_actual - data_anterior).days
            factor = round(dies_periode_lectura / 30.0, 2)
            vals = HARDCODED_VALUES.copy()
            vals['data_desde'] = lectura.data_anterior
            vals['data_fins'] = lectura.data_actual
            vals['factura_id'] = fid
            if not consum:
                vals['price_unit_multi'] = 0
                vals['quantity'] = 0
                o.GiscedataFacturacioFacturaLinia.create(vals)
                continue
            print "  Periode %s a %s" % (lectura.data_anterior,
                                         lectura.data_actual)
            print "  Dies periode lectura: %s" % dies_periode_lectura
            print "  Factor: %s" % factor
            consum_normal = consum
            # Descompte dels 12.5
            if consum/dies_periode_lectura >= 12.5:
                descompte = 12.5 * factor
            else:
                descompte = (consum/dies_periode_lectura) * factor
            descompte = round(descompte, 2)
            print "  Descompte: %s" % descompte            
            consum_normal -= descompte
            # Exces de 500
            exces = 0
            if consum > 500 * factor:
                exces = consum - (500 * factor)
            print "  Consum normal: %s" % consum_normal
            print "  Exces: %s" % exces
            # Facturem
            vals['price_unit_multi'] = 0
            vals['quantity'] = descompte
            o.GiscedataFacturacioFacturaLinia.create(vals)
            # Consum normal
            vals['price_unit_multi'] = 0.112480
            vals['quantity'] = consum_normal
            o.GiscedataFacturacioFacturaLinia.create(vals)
            if exces:
                vals['tipus'] = 'altres'
                vals['price_unit_multi'] = 0.02839
                vals['quantity'] = exces
                o.GiscedataFacturacioFacturaLinia.create(vals)
#    factura.invoice_cancel()
#    factura.action_cancel_draft()
    factura.button_reset_taxes()
#    factura.invoice_open()
