# -*- coding: utf-8 -*-

from osv import osv, fields


class ProductPricelist(osv.osv):
    """Afegim l'opció de bono social.
    """
    _name = 'product.pricelist'
    _inherit = 'product.pricelist'

    _columns = {
        'bono_social': fields.boolean('Bono social')
    }

    _defaults = {
        'bono_social': lambda *a: 0,
    }

ProductPricelist()
