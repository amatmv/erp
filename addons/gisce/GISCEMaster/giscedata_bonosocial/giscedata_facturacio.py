# -*- coding: utf-8 -*-

from osv import osv
from tools import cache
import netsvc
from datetime import datetime

logger = netsvc.Logger()


def bs_log(msg):
    logger.notifyChannel('bonosocial', netsvc.LOG_INFO, msg)


class GiscedataFacturacioFactura(osv.osv):
    """Afegim la funció de bono social.
    """
    _name = 'giscedata.facturacio.factura'
    _inherit = 'giscedata.facturacio.factura'

    @cache()
    def bono_social_exc_price(self, cursor, uid, pricelist, date):
        """Obtenim el preu de l'excés del bono social.
        """
        pricelist_obj = self.pool.get('product.pricelist')
        imd_obj = self.pool.get('ir.model.data')
        imd_id = imd_obj._get_id(cursor, uid,
                                 'giscedata_bonosocial',
                                 'p1_exc_bonosocial')
        product = imd_obj.read(cursor, uid, imd_id, ['res_id'])['res_id']
        price = pricelist_obj.price_get(
            cursor, uid, [pricelist], product, 1, context={'date': date}
        )[pricelist]
        return price

    def apply_bono_social(self, cursor, uid, ids, context=None):
        """Apliquem l'operació del bono social.
        """
        logger = netsvc.Logger()
        linia_obj = self.pool.get('giscedata.facturacio.factura.linia')
        if not context:
            context = {}
        for factura in self.browse(cursor, uid, ids, context):
            if not factura.llista_preu.bono_social:
                continue
            bs_log("Factura %s" % factura.id)
            ORIG_VALUES = False
            for linia in factura.linia_ids:
                if linia.tipus == 'energia':
                    ORIG_VALUES = linia.read(
                        ['name', 'tipus', 'product_id', 'uos_id', 'account_id',
                         'invoice_line_tax_id', 'price_unit_multi',
                         'uom_multi_id']
                    )[0]
                    linia.unlink()
            for key in ('product_id', 'uos_id', 'account_id'):
                ORIG_VALUES[key] = ORIG_VALUES.get(key, (False, False))[0]
            ORIG_VALUES['invoice_line_tax_id'] = [
                (6, 0, ORIG_VALUES.get('invoice_line_tax_id', []))
            ]
            if 'id' in ORIG_VALUES:
                del ORIG_VALUES['id']
            for lectura in factura.lectures_energia_ids:
                if lectura.tipus == 'activa':
                    consum = lectura.lect_actual - lectura.lect_anterior
                    if consum < 0:
                        consum += lectura.comptador_id.giro
                    bs_log(u"Consum per comptador %s: %s"
                           % (lectura.comptador_id.name, consum))
                    consum = float(consum)
                    data_anterior = datetime.strptime(lectura.data_anterior,
                                                      '%Y-%m-%d')
                    data_actual = datetime.strptime(lectura.data_actual,
                                                     '%Y-%m-%d')
                    dies_periode_lectura = (data_actual - data_anterior).days
                    factor = round(dies_periode_lectura / 30.0, 2)
                    vals = ORIG_VALUES.copy()
                    vals['data_desde'] = lectura.data_anterior
                    vals['data_fins'] = lectura.data_actual
                    vals['factura_id'] = factura.id
                    if not consum:
                        vals['price_unit_multi'] = 0
                        vals['quantity'] = 0
                        linia_obj.create(cursor, uid, vals.copy())
                        continue
                    bs_log("Periode %s a %s" % (lectura.data_anterior,
                                                lectura.data_actual))
                    bs_log("Dies periode lectura: %s" % dies_periode_lectura)
                    bs_log("Factor: %s" % factor)
                    consum_normal = consum
                    # Descompte dels 12.5
                    if consum / dies_periode_lectura >= 12.5:
                        descompte = 12.5 * factor
                    else:
                        descompte = (consum / dies_periode_lectura) * factor
                    descompte = round(descompte, 2)
                    bs_log("Descompte: %s" % descompte)
                    consum_normal -= descompte
                    # Exces de 500
                    exces = 0
                    if consum > 500 * factor:
                        exces = consum - (500 * factor)
                    bs_log("Consum normal: %s" % consum_normal)
                    bs_log("Exces: %s" % exces)
                    # Facturem
                    vals['price_unit_multi'] = 0
                    vals['quantity'] = descompte
                    linia_obj.create(cursor, uid, vals.copy())
                    # Consum normal
                    vals['price_unit_multi'] = ORIG_VALUES['price_unit_multi']
                    vals['quantity'] = consum_normal
                    linia_obj.create(cursor, uid, vals.copy())
                    if exces:
                        vals['tipus'] = 'altres'
                        vals['price_unit_multi'] = self.bono_social_exc_price(
                            cursor, uid,
                            factura.llista_preu.id,
                            factura.data_final
                        )
                        vals['quantity'] = exces
                        linia_obj.create(cursor, uid, vals.copy())
            factura.button_reset_taxes()

GiscedataFacturacioFactura()


class GiscedataFacturacioFacturador(osv.osv):
    """Sobreescrovim el mètode fact_via_lectures per aplicar el bono social.
    """
    _name = 'giscedata.facturacio.facturador'
    _inherit = 'giscedata.facturacio.facturador'

    def fact_via_lectures(self, cursor, uid, polissa_id, lot_id, context=None):
        factures = super(GiscedataFacturacioFacturador,
                         self).fact_via_lectures(cursor, uid, polissa_id,
                                                 lot_id, context)
        factura_obj = self.pool.get('giscedata.facturacio.factura')
        factura_obj.apply_bono_social(cursor, uid, factures)
        return factures

    def fact_via_proveidor(self, cursor, uid, polissa_id, lot_id,
                           context=None):
        factures = super(GiscedataFacturacioFacturador,
                         self).fact_via_proveidor(cursor, uid, polissa_id,
                                                  lot_id, context)
        factura_obj = self.pool.get('giscedata.facturacio.factura')
        factura_obj.apply_bono_social(cursor, uid, factures)
        return factures

GiscedataFacturacioFacturador()
