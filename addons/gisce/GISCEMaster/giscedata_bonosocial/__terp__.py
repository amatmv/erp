# -*- coding: utf-8 -*-
{
    "name": "Bono Social",
    "description": """
    This module provide :
      * Decidir quines llistes de preus són del tipus 'Bono social'
      * Facturació a comercialitzadora amb operacions pel bono social
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_facturacio_comer"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_bonosocial_data.xml",
        "pricelist_view.xml"
    ],
    "active": False,
    "installable": True
}
