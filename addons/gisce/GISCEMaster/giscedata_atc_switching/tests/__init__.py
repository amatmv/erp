# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from addons import get_module_resource
from giscedata_switching.tests.common_tests import TestSwitchingImport
from datetime import datetime, timedelta
import base64
from workdays import *
import json


class TestATCSwitching(TestSwitchingImport):

    def test_create_r1_from_atc(self):

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            self.switch(txn, 'comer')
            pol_obj = self.openerp.pool.get("giscedata.polissa")
            atc_obj = self.openerp.pool.get('giscedata.atc')
            wiz_o = self.openerp.pool.get("wizard.create.atc.from.polissa")
            pol_id = self.get_contract_id(txn, xml_id='polissa_0001')
            pol = pol_obj.browse(cursor, uid, pol_id)
            pol.distribuidora.write({'ref': 4444})
            # Creem un cas ATC
            wiz_id = wiz_o.create(cursor, uid, {'subtipus_id': 1}, {'active_ids': [pol_id]})
            wiz_o.create_atc_case(cursor, uid, [wiz_id], {'active_ids': [pol_id]})
            wiz = wiz_o.browse(cursor, uid, wiz_id)
            cas = atc_obj.browse(cursor, uid, wiz.generated_cases[0])
            pol = pol_obj.browse(cursor, uid, pol_id)
            self.assertEqual(cas.time_tracking_id.code, '0')
            self.assertEqual(cas.process_step, '')

            # Cridem l'assistent de crear R1 desde ATC
            # Ha de tornar un diccionari que cridi el asistents de R1.
            # Ens quedarem amb el context i cridarem l'assistent amb aquest context
            wiz_o = self.openerp.pool.get("wizard.generate.r1.from.atc.case")
            wiz_id = wiz_o.create(cursor, uid, {}, {'active_ids': [cas.id]})
            res = wiz_o.generate_r1(cursor, uid, [wiz_id], {'active_id': cas.id})
            context = eval(dict(res).get("context", "{}"))
            extra_values = context.get("extra_values")
            self.assertEqual(extra_values['auto_r1_atc'], True)
            self.assertEqual(context['polissa_id'], pol.id)
            self.assertEqual(extra_values['ref_id'], cas.id)
            self.assertEqual(extra_values['ref_model'], "giscedata.atc")

            # Cridem l'assistent de crear R1 amb el context obringut.
            wiz_o = self.openerp.pool.get('wizard.subtype.r1')
            wiz_id = wiz_o.create(cursor, uid, {'comentaris': "TEST"}, context)
            res = wiz_o.action_create_r1_case(cursor, uid, [wiz_id], context)
            r1_id = res.get("domain")[0][2]
            r1 = self.openerp.pool.get("giscedata.switching").browse(cursor, uid, r1_id)
            cas = atc_obj.browse(cursor, uid, cas.id)
            self.assertEqual(r1.ref, "giscedata.atc, {}".format(cas.id))
            self.assertEqual(cas.ref, "giscedata.switching, {}".format(r1.id))
            self.assertEqual(cas.state, "pending")
            self.assertEqual(cas.time_tracking_id.code, "1")
            self.assertEqual(cas.process_step, '01')

    def test_create_r1_from_atc_009(self):

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            self.switch(txn, 'comer')
            pol_obj = self.openerp.pool.get("giscedata.polissa")
            atc_obj = self.openerp.pool.get('giscedata.atc')
            wiz_o = self.openerp.pool.get("wizard.create.atc.from.polissa")
            pol_id = self.get_contract_id(txn, xml_id='polissa_0001')
            fact_id = self.get_invoice_id(txn, xml_id='factura_dso_0001')
            subtipus_id = self.openerp.pool.get("giscedata.subtipus.reclamacio").search(cursor, uid, [('name', '=', '009')])[0]
            pol = pol_obj.browse(cursor, uid, pol_id)
            pol.distribuidora.write({'ref': 4444})
            # Creem un cas ATC
            wiz_id = wiz_o.create(cursor, uid, {'subtipus_id': subtipus_id, 'factura_id': fact_id, 'associar_factura': True}, {'active_ids': [pol_id]})
            wiz_o.create_atc_case(cursor, uid, [wiz_id], {'active_ids': [pol_id]})
            wiz = wiz_o.browse(cursor, uid, wiz_id)
            cas = atc_obj.browse(cursor, uid, wiz.generated_cases[0])
            pol = pol_obj.browse(cursor, uid, pol_id)
            self.assertEqual(cas.time_tracking_id.code, '0')
            self.assertEqual(cas.ref2, 'giscedata.facturacio.factura,{}'.format(fact_id))

            # Cridem l'assistent de crear R1 desde ATC
            # Ha de tornar un diccionari que cridi el asistents de R1.
            # Ens quedarem amb el context i cridarem l'assistent amb aquest context
            wiz_o = self.openerp.pool.get("wizard.generate.r1.from.atc.case")
            wiz_id = wiz_o.create(cursor, uid, {}, {'active_ids': [cas.id]})
            res = wiz_o.generate_r1(cursor, uid, [wiz_id], {'active_id': cas.id})
            context = eval(dict(res).get("context", "{}"))
            extra_values = context.get("extra_values")
            self.assertEqual(extra_values['auto_r1_atc'], True)
            self.assertEqual(extra_values['ref_id'], cas.id)
            self.assertEqual(extra_values['ref_model'], "giscedata.atc")
            self.assertEqual(res['res_model'], "giscedata.switching.r101.wizard")
            self.assertEqual(context['proces'], 'R1-02009')

            # Cridem l'assistent de crear R1 amb el context obringut.
            wiz_o = self.openerp.pool.get('giscedata.switching.r101.wizard')
            wiz_id = wiz_o.create(cursor, uid, {}, context)
            wiz_o.action_create_atr_case(cursor, uid, [wiz_id], context)
            wiz = wiz_o.browse(cursor, uid, wiz_id)
            r1_id = json.loads(wiz.generated_cases)[0]
            r1 = self.openerp.pool.get("giscedata.switching").browse(cursor, uid, r1_id)
            cas = atc_obj.browse(cursor, uid, cas.id)
            self.assertEqual(r1.ref, "giscedata.atc, {}".format(cas.id))
            self.assertEqual(cas.ref, "giscedata.switching, {}".format(r1.id))
            self.assertEqual(cas.state, "pending")
            self.assertEqual(cas.time_tracking_id.code, "1")
