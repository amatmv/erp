# -*- coding: utf-8 -*-
from osv import osv, fields
from gestionatr.defs import *
from datetime import datetime
import json
from tools.translate import _


class WizardCreateAtc(osv.osv_memory):

    _inherit = "wizard.create.atc.from.polissa"

    def create_atc_case(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        res = super(WizardCreateAtc, self).create_atc_case(cursor, uid, ids, context=context)
        wizard = self.browse(cursor, uid, ids[0], context)
        atc_obj = self.pool.get('giscedata.atc')
        for atc_id in wizard.generated_cases:
            if wizard.associar_factura and wizard.factura_id:
                atc_obj.write(cursor, uid, atc_id, {'ref2': 'giscedata.facturacio.factura,{}'.format(wizard.factura_id.id)})
        return res

    def onchange_subtipus(self, cursor, uid, ids, subtipus_id, associar_factura, factura_id, context=None):
        if context is None:
            context = {}

        res = super(WizardCreateAtc, self).onchange_subtipus(cursor, uid, ids, subtipus_id)

        res['domain'].update({
            'factura_id': [('polissa_id', 'in', context.get("active_ids", []))]
            })

        if not subtipus_id and not factura_id:
            res['value']['associar_factura'] = False
        elif subtipus_id:
            subtipus_obj = self.pool.get('giscedata.subtipus.reclamacio')
            subtipus = subtipus_obj.read(cursor, uid, subtipus_id, ['name'])['name']
            if subtipus in ['009', '036']:
                res['value']['associar_factura'] = True
            elif not factura_id:  # and subtipus not in ['009', '036']
                res['value']['associar_factura'] = False
        return res

    def open_r1_wizard(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        generated_cases = self.read(cursor, uid, ids[0], ['generated_cases'])[0]['generated_cases']
        ctx = context.copy()
        ctx.update({'active_ids': generated_cases,
                    'active_id': generated_cases[0]})

        res = {
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'wizard.generate.r1.from.atc.case',
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': ctx,
        }

        # context.update({'subtipus_id': self.read(cursor, uid, ids, ['subtipus_id'])[0]['subtipus_id']})
        #
        # res = {
        #     'view_type': 'form',
        #     'view_mode': 'form',
        #     'type': 'ir.actions.act_window',
        #     'target': 'new',
        #     'context': context,
        # }
        #
        # if len(ids) > 1:
        #     res.update({'res_model': 'wizard.r101.from.multiple.contracts'})
        #     res['context'].update({'contract_ids': context.get('active_ids', [])})
        # else:
        #     res.update({'res_model': 'wizard.create.r1'})
        #     res['context'].update({'polissa_id': ids[0]})
        return res

    _columns = {
        'associar_factura': fields.boolean("Associar factura"),
        'factura_id': fields.many2one("giscedata.facturacio.factura", "Factura Reclamada")
    }


WizardCreateAtc()
