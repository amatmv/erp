# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _
from tools import config


class WizardGenerateR1FromCase(osv.osv_memory):
    _name = 'wizard.generate.r1.from.atc.case'
    # active_id giscedata.atc

    def _default_info(self, cursor, uid, context=None):
        if context is None:
            context = {}
        if len(context.get("active_ids", [])) > 1:
            raise osv.except_osv(_(u"Error Usuari"), _(u"No es poden crear R1 desde varis casos de atenció al client alhora. S'ha de fer amb un sol cas d'atenció al client."))
        cas_id = context.get('active_id', False)

        return _('Aquest assistent generarà un fitxer R1 a partir del cas {0} atc.').format(cas_id)

    def get_fields_of_atc_case_for_r1_wiz(self, cursor, uid, ids, atc_id, context=None):
        if isinstance(atc_id, list):
            atc_id = atc_id[0]
        atc_obj = self.pool.get('giscedata.atc')
        atc = atc_obj.browse(cursor, uid, atc_id, context=context)
        error_msg = None
        if not atc.polissa_id:
            error_msg = _('ERROR: el cas {0} no te polissa assignada').format(atc_id)
        if not atc.subtipus_id:
            error_msg = _('ERROR: el cas {0} no te subtipus assignat').format(atc_id)

        if error_msg:
            values_err = {
                'state': 'end',
                'info': error_msg
            }
            self.write(cursor, uid, ids, values_err, context=context)
            return False
        else:
            return {
                'subtype': atc.subtipus_id.id,
                'polissa_id': atc.polissa_id.id
            }

    def call_wizard_create_r1(self, cursor, uid, atc_id, subtipus_id, polissa_id, auto_r1_atc=True, context=None):
        subtipus_name = self.pool.get("giscedata.subtipus.reclamacio").read(cursor, uid, subtipus_id, ['name'])['name']
        ref = self.pool.get("giscedata.atc").read(cursor, uid, atc_id, ['ref2'])['ref2']
        extra_values = {
            'auto_r1_atc': auto_r1_atc,
            'ref_id': atc_id,
            'ref_model': "giscedata.atc"
        }
        if subtipus_name in ['009', '036'] and "giscedata.facturacio.factura" in (ref or ""):
            invoice_id = int(ref.split(",")[-1])
            if subtipus_name == "009":
                proces = 'R1-02009'
            else:
                proces = 'R1-02036'
            return {
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'giscedata.switching.r101.wizard',
                'type': 'ir.actions.act_window',
                'target': 'new',
                'context': "{{"
                           "'invoice_id':{0}, "
                           "'proces': '{1}', "
                           "'extra_values': {2},"
                           "}}".format(invoice_id, proces, extra_values)
            }
        else:
            return {
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'wizard.subtype.r1',
                'type': 'ir.actions.act_window',
                'target': 'new',
                'context': "{{ "
                           "'subtipus_id':{0}, "
                           "'polissa_id': {1}, "
                           "'extra_values': {2}"
                           "}}".format(subtipus_id, polissa_id, extra_values)
            }

    def generate_r1(self, cursor, uid, ids, context=None):
        atc_case_id = context.get('active_id', False)
        if atc_case_id:
            r1_wizard_fields = self.get_fields_of_atc_case_for_r1_wiz(
                cursor, uid, ids, atc_case_id, context=context
            )
            if r1_wizard_fields:
                return self.call_wizard_create_r1(
                    cursor, uid, atc_case_id, r1_wizard_fields['subtype'],
                    r1_wizard_fields['polissa_id'], context=context
                )
        else:
            values_err = {
                'state': 'end',
                'info': "ERROR: No s'ha trobat cap cas de Atenció al Client"
            }
            self.write(cursor, uid, ids, values_err, context=context)
        return False

    _columns = {
        'state': fields.selection([('init', 'Init'), ('end', 'End')], 'State'),
        'info': fields.text('Informació', readonly=True),
        'auto_r1': fields.boolean("Automatisme R1")
    }

    _defaults = {
        'state': lambda *a: 'init',
        'info': _default_info,
        'auto_r1': lambda *a: True,
    }

WizardGenerateR1FromCase()
