# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscedataAtc(osv.osv):

    _name = 'giscedata.atc'
    _inherit = 'giscedata.atc'

    def _ff_has_process(self, cursor, uid, ids, field_name, arg, context):
        res = dict.fromkeys(ids, '')
        for ref_vals in self.read(cursor, uid, ids, ['ref', 'ref2']):
            has_process = False
            if ref_vals['ref']:
                has_process |= 'giscedata.switching' in ref_vals['ref']
            if ref_vals['ref2']:
                has_process |= 'giscedata.switching' in ref_vals['ref2']
            res[ref_vals['id']] = has_process
        return res

    def _ff_get_process_step(self, cursor, uid, ids, field_name, arg, context):
        res = dict.fromkeys(ids, '')
        sw_obj = self.pool.get('giscedata.switching')

        for ref_vals in self.read(cursor, uid, ids, ['ref', 'ref2']):
            if ref_vals['ref']:
                if 'giscedata.switching' in ref_vals['ref']:
                    process_id = ref_vals['ref'].split(',')[1]
                    pas_name = sw_obj.read(cursor, uid, int(process_id), ['step_id'])
                    if pas_name and pas_name.get('step_id'):
                        res[ref_vals['id']] = pas_name['step_id'][1]
            if ref_vals['ref2']:
                if 'giscedata.switching' in ref_vals['ref2']:
                    process_id = ref_vals['ref2'].split(',')[1]
                    pas_name = sw_obj.read(cursor, uid, int(process_id), ['step_id'])
                    if pas_name and pas_name.get('step_id'):
                        res[ref_vals['id']] = pas_name['step_id'][1]
        return res

    _columns = {
        'has_process': fields.function(_ff_has_process, method=True,
                                       string=u"Té Procés", type='boolean'),
        'process_step': fields.function(_ff_get_process_step, method=True,
                                        string=u"Pas del procés", type='char',
                                        size=10)

    }


GiscedataAtc()
