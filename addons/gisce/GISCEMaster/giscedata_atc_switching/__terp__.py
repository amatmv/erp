# -*- coding: utf-8 -*-
{
    "name": "ATC switching",
    "description": """atc switching. 
        Per generar fitxers atr des de casos""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_switching",
        "giscedata_atc",
        "giscedata_facturacio",
    ],
    "init_xml": [],
    "demo_xml":[

    ],
    "update_xml":[
        "giscedata_atc_data.xml",
        "giscedata_atc_view.xml",
        "giscedata_subtipus_reclamacio_view.xml",
        "wizard/wizard_generate_r1_from_atc_case_view.xml",
        "wizard/wizard_create_atc_from_polissa.xml",
        "security/ir.model.access.csv",

    ],
    "active": False,
    "installable": True
}
