# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* giscedata_product
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.14\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2019-07-26 17:22\n"
"PO-Revision-Date: 2019-07-26 17:22\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: giscedata_product
#: field:stock.production.lot,standard_price:0
msgid "Preu estàndard"
msgstr ""

#. module: giscedata_product
#: constraint:ir.ui.view:0
msgid "Invalid XML for View Architecture!"
msgstr ""

#. module: giscedata_product
#: field:product.product,stock_price:0
msgid "Valor de l'Stock"
msgstr ""

#. module: giscedata_product
#: field:product.product,sale_price_same_as_standard_price:0
msgid "Igualar preu de venta amb preu de cost"
msgstr ""

#. module: giscedata_product
#: model:ir.module.module,shortdesc:giscedata_product.module_meta_information
msgid "GISCE Data Product"
msgstr ""

#. module: giscedata_product
#: field:product.product,serial_number_price:0
msgid "Preu de número de série"
msgstr ""

