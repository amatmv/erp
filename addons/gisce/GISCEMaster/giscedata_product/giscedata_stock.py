# -*- encoding: utf-8 -*-

from osv.expression import OOQuery
from osv import fields, osv
from tools.translate import _
from tools import config


class stock_production_lot(osv.osv):

    _name = "stock.production.lot"
    _inherit = "stock.production.lot"

    _columns = {
        'standard_price': fields.float(
            "Preu estàndard", digits=(16, int(config['price_accuracy']))
        )
    }


stock_production_lot()
