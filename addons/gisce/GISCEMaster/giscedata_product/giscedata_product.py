# -*- encoding: utf-8 -*-

from osv import fields, osv
import copy

class product_product(osv.osv):

    _name = "product.product"
    _inherit = "product.product"

    def write(self, cursor, uid, ids, vals, context=None):
        """
        If sale_price_same_as_standard_price, then list_price (sale price) will
        have the same value as standard price.
        :param cursor:
        :param uid:
        :param ids:
        :param vals:
        :param context:
        :return: True.
        """
        if not isinstance(ids, list):
            ids = [ids]

        product_f = ['sale_price_same_as_standard_price', 'standard_price']
        product_vs = self.read(cursor, uid, ids, product_f, context=context)
        original_vals = copy.deepcopy(vals)
        for product_v in product_vs:
            product_id = product_v['id']
            if vals.get('sale_price_same_as_standard_price', product_v['sale_price_same_as_standard_price']):
                if 'standard_price' in vals:
                    sale_price = vals['standard_price']
                else:
                    sale_price = product_v['standard_price']

                vals.update({'list_price': sale_price})
                super(product_product, self).write(
                    cursor, uid, product_id, vals, context=context
                )
            else:
                super(product_product, self).write(
                    cursor, uid, product_id, original_vals, context=context
                )
        return True

    def _get_stock_price(self, cursor, uid, ids, field_name, arg, context=None):
        lot_obj = self.pool.get('stock.production.lot')
        prodlots_obj = self.pool.get('stock.report.prodlots')

        lot_query_domain = [
            ('stock_available', '>', 0),
            ('product_id.serial_number_price', '=', True),
            '|', '|',
            ('product_id.track_production', '=', True),
            ('product_id.track_incoming', '=', True),
            ('product_id.track_outgoing', '=', True)
        ]

        # stock.location
        location_id = context.get('location', False)
        if location_id:
            report_prodlots_ids = prodlots_obj.search(
                cursor, uid,
                [('location_id', '=', location_id), ('product_id', 'in', ids)],
                context=context
            )
            stocks_by_location = prodlots_obj.read(
                cursor, uid, report_prodlots_ids,
                ['name', 'product_id', 'prodlot_id'],
                context=context
            )
            self_ids = set()
            prodlot_ids = []
            for sbl in stocks_by_location:
                self_ids.add(sbl['product_id'][0])
                prodlot_id = sbl['prodlot_id']
                if prodlot_id:
                    prodlot_ids.append(prodlot_id[0])

            self_ids = list(self_ids)
            selfs_vals = self.read(
                cursor, uid, self_ids, ['standard_price'], context=context
            )
            standard_price = {self_vals['id']: self_vals['standard_price'] for self_vals in selfs_vals}

            product_stock_qty_by_location = {}
            for stock_by_location in stocks_by_location:
                self_id = stock_by_location['product_id'][0]
                product_stock_qty_by_location[self_id] = stock_by_location['name'] * standard_price[self_id]

            lot_query_domain.append(('id', 'in', prodlot_ids))
        else:
            lot_query_domain.append(('product_id', 'in', ids))
            selfs_vals = self.read(
                cursor, uid, ids, ['qty_available', 'standard_price'],
                context=context
            )
            classic_product_price = {}
            for self_vals in selfs_vals:
                classic_product_price[self_vals['id']] = self_vals['qty_available'] * self_vals['standard_price']

        lots_ids = lot_obj.search(cursor, uid, lot_query_domain, context=context)
        prices = lot_obj.read(
            cursor, uid, lots_ids, ['standard_price', 'product_id'],
            context=context
        )

        production_lot_prices = {}
        for price in prices:
            self_id = price['product_id'][0]
            price = price['standard_price']
            if not price:
                price = self.read(
                    cursor, uid, self_id, ['standard_price'], context=context
                )['standard_price']
            production_lot_prices[self_id] = price + production_lot_prices.get(
                self_id, 0
            )

        res = {}
        for product_id in ids:
            value = production_lot_prices.get(product_id, 0)
            if not value:
                if location_id:
                    value = product_stock_qty_by_location.get(product_id, 0)
                else:
                    value = classic_product_price.get(product_id, 0)

            res[product_id] = value
        return res

    def onchange_track_fields(self, cursor, uid, ids, prdctn, ncmng, tgng, context=None):
        vals = {}
        if not (prdctn or ncmng or tgng):
            vals = {'serial_number_price': False}
        return {'value': vals}

    def onchange_serial_number_price(self, cursor, uid, ids, snp, context=None):
        vals = {}
        if snp:
            vals = {
                'track_production': True,
                'track_incoming': True,
                'track_outgoing': True
            }
        return {'value': vals}

    _columns = {
        'stock_price': fields.function(
            _get_stock_price, string='Valor de l\'Stock', type='float', method=True
        ),
        'serial_number_price': fields.boolean('Preu de número de série'),
        'sale_price_same_as_standard_price': fields.boolean(
            'Igualar preu de venta amb preu de cost'
        ),
    }


product_product()
