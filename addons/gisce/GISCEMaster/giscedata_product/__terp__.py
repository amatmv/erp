# -*- coding: utf-8 -*-
{
    "name": "GISCE Data Product",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Product",
    "depends":[
        "stock",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_product_view.xml",
        "giscedata_stock_view.xml",
    ],
    "active": False,
    "installable": True
}
