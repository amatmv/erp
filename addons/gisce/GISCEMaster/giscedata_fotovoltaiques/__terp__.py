# -*- coding: utf-8 -*-
{
    "name": "Instalacions Fotovoltàiques",
    "description": """Gestió de conexions d'instal·lacions fotovoltàiques a la xarxa de baixa tensió""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "CRM",
    "depends":[
        "base",
        "crm",
        "giscedata_cups",
        "giscedata_re"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_fotovoltaiques_view.xml",
        "giscedata_fotovoltaiques_data.xml",
        "ir.model.access.csv",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
