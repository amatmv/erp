
# -*- coding: utf-8 -*-
import time
import datetime

from osv import fields,osv

class giscedata_fotovoltaiques_modes_connexio(osv.osv):
    """Modes de connexio

    ???????????

    @type osv.osv: ??
    @param osv.osv: ??
    """
    _name = "giscedata_fotovoltaiques_modes_connexio"
    _columns = {
      'name': fields.char('Nom Mode Connexió', size=64, required=True),
      'description': fields.text('Descripció'),
    }
giscedata_fotovoltaiques_modes_connexio()

class giscedata_fotovoltaiques_inversor(osv.osv):
    """Inversor

    ???????????

    @type osv.osv: ??
    @param osv.osv: ??
    """
    _name = "giscedata_fotovoltaiques_inversor"
    _columns = {
      'name': fields.char('Nom Inversor', size=64, required=True),
      'description': fields.text('Descripció'),
    }
giscedata_fotovoltaiques_inversor()

class giscedata_fotovoltaiques_dispositiu_protector(osv.osv):
    """Dispositius Protectors

    ???????????

    @type osv.osv: ??
    @param osv.osv: ??
    """
    _name = "giscedata_fotovoltaiques_dispositiu_protector"
    _columns = {
      'name': fields.char('Nom Dispositiu Protector', size=64, required=True),
      'description': fields.text('Descripció'),
    }
giscedata_fotovoltaiques_dispositiu_protector()

class giscedata_fotovoltaiques_elements_connexio(osv.osv):
    """Elements de connexio

    ???????????

    @type osv.osv: ??
    @param osv.osv: ??
    """
    _name = "giscedata_fotovoltaiques_elements_connexio"
    _columns = {
      'name': fields.char('Nom Element Connexió', size=64, required=True),
      'description': fields.text('Descripció'),
    }
giscedata_fotovoltaiques_elements_connexio()

class giscedata_fotovoltaiques_sollicitud(osv.osv):
    """Sol·licitud

    Sol·licitud de connexió a la xarxa d'una instal·lació fotovoltàica

    Estats en els que pot estar :
      - Draft ( esborrany )
      - Open ( oberta )
      - Cancel ( cancelada )
      - Accepted ( acceptada )
      - Sent ( enviada )
      - ToSign ( per signar )
      - Signed ( signat )
      - ToConnect ( per connectar )
      - Done ( connectada )

    @type osv.osv: ??
    @param osv.osv: ??
    """

    def deadline_date(self, *args):
        _create_date = datetime.datetime.now()
        _diff = datetime.timedelta(weeks=4)
        _deadline_date = _create_date + _diff
        return _deadline_date.strftime('%Y-%m-%d %H:%M:%S')

    def add_reply(self, cr, uid, ids, context=None):
        """Afegeix una adreça d'email per respondre als events i feedback

        """
        res = True
        for ref in self.browse(cr, uid, ids):
            res = res and ref.case_id.add_reply(context)
        return res

    def case_log(self, cr, uid, ids,context=None, email=False, *args):
        """Loguejem un event

        """
        res = True
        for ref in self.browse(cr, uid, ids):
            res = res and ref.case_id.case_log(context, email, *args)
        return res

    def case_log_reply(self, cr, uid, ids, context=None, email=False, *args):
        """Fem historize i enviem email

        """
        res = True
        for ref in self.browse(cr, uid, ids):
            res = res and ref.case_id.case_log_reply(context, email, *args)
        return res

    def remind_partner(self, cr, uid, ids, context=None, attach=False):
        """Recordem al client que tenim pendent la tasca

        """
        for ref in self.browse(cr, uid, ids):
            return ref.case_id.remind_partner(context, attach)

    def remind_user(self, cr, uid, ids, context=None, attach=False, destination=True):
        """Recordem al usuari de l'ERP que tenim pendent la tasca

        """
        for ref in self.browse(cr, uid, ids):
            subject = 'Sol·licitud Connexió Fotovoltàica [%s] - %s' % (ref.id,
                                                                       ref.name)
            return ref.case_id.remind_user(attach, destination, subject)

    def case_cancel(self,cr,uid,ids,*args):
        """Cancel·la una sol·licitud

        """
        res = True
        for sol in self.browse(cr, uid, ids):
            res = res and sol.case_id.case_cancel(*args)
        return res

    def case_open(self,cr,uid,ids,*args):
        """Reobre una sol·licitud

        """
        res = True
        for sol in self.browse(cr, uid, ids):
            res = res and sol.case_id.case_open(*args)
        return res

    def case_accept(self,cr,uid,ids,*args):
        """Accepta una sol·licitud

        """
        _create_date = datetime.datetime.now()
        _diff = datetime.timedelta(days=365)
        _deadline_date = _create_date + _diff
        res = True
        for sol in self.browse(cr,uid,ids):
            data = {'state': 'accept',
                    'active':True,
                    'date_deadline':_deadline_date.strftime('%Y-%m-%d %H:%M:%S'),
                    'date_reply':_create_date.strftime('%Y-%m-%d %H:%M:%S')}
            sol.write(data)
        return res

    def case_sent(self,cr,uid,ids,*args):
        """Envia una sol·licitud


        """
        res = True
        for sol in self.browse(cr,uid,ids):
            data = {'state': 'sent', 'active':True}
            sol.write(data)
        return res

    def case_tosign(self,cr,uid,ids,*args):
        """Per signar una sol·licitud

        """

        _create_date = datetime.datetime.now()
        _diff = datetime.timedelta(weeks=4)
        _deadline_date = _create_date + _diff

        res = True
        for sol in self.browse(cr,uid,ids):
            data = {'state': 'tosign', 'active':True, 'date_deadline':_deadline_date.strftime('%Y-%m-%d %H:%M:%S')}
            sol.write(data)
        return res

    def case_signed(self,cr,uid,ids,*args):
        """Signada una sol·licitud

        """
        _create_date = datetime.datetime.now()

        res = True
        for sol in self.browse(cr,uid,ids):
            data = {'state': 'signed',
                    'active':True,
                    'date_contract':_create_date.strftime('%Y-%m-%d %H:%M:%S')}
            sol.write(data)
        return res

    def case_toconnect(self,cr,uid,ids,*args):
        """Per connectar una sol·licitud

        """

        _create_date = datetime.datetime.now()
        _diff = datetime.timedelta(weeks=4)
        _deadline_date = _create_date + _diff

        res = True
        for sol in self.browse(cr,uid,ids):
            data = {'state': 'toconnect', 'active':True, 'date_deadline':_deadline_date.strftime('%Y-%m-%d %H:%M:%S')}
            sol.write(data)
        return res

    def case_close(self,cr,uid,ids,*args):
        """Tanca una sol·licitud

        """

        _create_date = datetime.datetime.now()
        res = True
        for sol in self.browse(cr,uid,ids):
            data = {'date_start':_create_date.strftime('%Y-%m-%d %H:%M:%S')}
            self.write(cr,uid,ids,data)
            res = res and sol.case_id.case_close()

        # Create Instal·lació
        self.pool.get('giscedata_fotovoltaiques_installacio').create(cr,uid, {
          'name': sol.name,
          'sollicitud_id': sol.id
        })

        return res


    _name = "giscedata_fotovoltaiques_sollicitud"
    _inherits = {'crm.case': 'case_id'}

    _columns = {
      'case_id' : fields.many2one('crm.case', 'Cas', required=True),
      'situacio': fields.char('Situació', size=255, required=True),
      'cups': fields.many2one('giscedata.cups.ps', 'CUPS'),
      'municipi': fields.many2one('res.municipi','Terme Municipal'),
      'punt_connexio_client': fields.char('Punt de Connexió Client', size=255, required=True),
      'potencia_pic': fields.float('Potència Pic (kW)'),
      'potencia_nominal': fields.float('Potència Nominal (kW)'),
      'potencia_nominal_maxima': fields.float('Potència Nominal Màxima (kW)'),
      'generacio_anual_prevista': fields.float('Generació Anual Prevista (kWh any)'),
      'model_comptador': fields.many2one('product.product','Model de Comptador'),
      'n_serie_comptador': fields.char('Nº de série de comptador :', size=100),
      'n_plaques': fields.integer('Nº de plaques'),
      'tipus_ondulador': fields.many2one('product.product','Tipus Ondulador'),
      'n_onduladors': fields.integer('Nº Onduladors'),
      'modem': fields.many2one('product.product','Marca i model del Modem'),
      'serie_modem': fields.char('Nº série del módem', size=100),
      'tensio_nominal_minima': fields.float('Tensió Nominal Mínima'),
      'potencia_de_curtcircuit': fields.float('Potència de Curtcircuit'),
      'modes_de_connexio_id': fields.many2many('giscedata_fotovoltaiques_modes_connexio','modes_connexio_rel','giscedata_fotovoltaiques_sollicitud_id','uid','Modes de connexió'),
      'inversor_id': fields.many2one('giscedata_fotovoltaiques_inversor','Inversor'),
      'dispositius_de_proteccio_id': fields.many2many('giscedata_fotovoltaiques_dispositiu_protector','dispositiu_proteccio_rel','giscedata_fotovoltaiques_dispositiu_protector','uid','Dispositius de Protecció'),
      'date_reply': fields.datetime('Data Resposta'),
      'date_contract': fields.datetime('Data Contracte'),
      'date_start': fields.datetime('Data Posta en Marxa'),
    }

    _defaults = {
      'date': lambda *a: time.strftime('%Y-%m-%d %H:%M:%S'),
      'date_deadline': deadline_date
    }

giscedata_fotovoltaiques_sollicitud()


class giscedata_fotovoltaiques_installacio(osv.osv):
    _name = "giscedata_fotovoltaiques_installacio"
    _columns = {
      'name': fields.char('Instal·lació', size=64, required=True),
      'description': fields.text('Descripció'),
      'sollicitud_id': fields.many2one('giscedata_fotovoltaiques_sollicitud', 'Sol·licitud', required=True),
    }
giscedata_fotovoltaiques_installacio()
