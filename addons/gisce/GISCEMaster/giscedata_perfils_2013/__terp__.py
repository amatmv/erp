# -*- coding: utf-8 -*-
{
    "name": "Perfilació de lectures (valors 2013)",
    "description": """
Actualitza els valors de perfilació per l'any 2013
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_perfils"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_perfils_2013_data.xml"
    ],
    "active": False,
    "installable": True
}
