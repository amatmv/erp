# -*- coding: utf-8 -*-
{
    "name": "Personalització pòlissa Sóller",
    "description": """
    This module provide :
      * Estats per avís de tall (avís 1 i avís 2)
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_polissa"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_polissa_view.xml",
        "giscedata_polissa_workflow.xml"
    ],
    "active": False,
    "installable": True
}
