# -*- coding: utf-8 -*-
import pooler
import netsvc

def migrate(cursor, installed_version):
    '''workflow to active from avis1 and avis2 polisses'''
    pool = pooler.get_pool(cursor.dbname)
    wf_service = netsvc.LocalService('workflow')
    logger = netsvc.Logger()
    polissa_obj = pool.get('giscedata.polissa')
    user_obj = pool.get('res.users')
    sync_id = user_obj.search(cursor, 1, [('login', '=', 'sync')])[0]
    search_params = [('state', 'in', ('avis1', 'avis2'))]
    polissa_ids = polissa_obj.search(cursor, 1, search_params)
    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         'Trobades %s pòlisses en avis1 o avis2.' % len(polissa_ids))
    for polissa_id in polissa_ids:
        wf_service.trg_validate(sync_id, 'giscedata.polissa',
                                polissa_id, 'activar', cursor)
         