# -*- coding: utf-8 -*-
from osv import osv, fields
import netsvc

class GiscedataPolissa(osv.osv):
    """Customització pòlissa per Sóller.
    """
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    _polissa_states_selection = [
        ('avis1', 'Avís 1'),
        ('avis2', 'Avís 2')
    ]

    def __init__(self, pool, cursor):
        """Init to add new states."""
        super(GiscedataPolissa, self).__init__(pool, cursor)
        for new_selection in self._polissa_states_selection:
            if new_selection not in self._columns['state'].selection:
                self._columns['state'].selection.append(new_selection)

    def wkf_avis1(self, cursor, uid, ids):
        """Estat avís 1 del workflow.
        """
        self.write(cursor, uid, ids, {'state': 'avis1'})
        return True

    def avis1_xmlrpc(self, cursor, uid, ids):
        """Passa a esborrany una pòlissa a través d'XML-RPC."""
        wf_service = netsvc.LocalService('workflow')
        for p_id in ids:
            wf_service.trg_validate(uid, 'giscedata.polissa', p_id, 'avis1',
                                    cursor)
        return True

    def wkf_avis2(self, cursor, uid, ids):
        """Estat avís 2 del workflow.
        """
        self.write(cursor, uid, ids, {'state': 'avis2'})
        return True

    def avis2_xmlrpc(self, cursor, uid, ids):
        """Passa a esborrany una pòlissa a través d'XML-RPC."""
        wf_service = netsvc.LocalService('workflow')
        for p_id in ids:
            wf_service.trg_validate(uid, 'giscedata.polissa', p_id, 'avis2',
                                    cursor)
        return True

GiscedataPolissa()
