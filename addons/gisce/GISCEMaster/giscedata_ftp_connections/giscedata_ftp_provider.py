# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _


class GiscedataFTPProvider(osv.osv):

    _name = 'giscedata.ftp.provider'

    def get_provider(self, cursor, uid, service, partner_id=None, context=None):
        if not partner_id and not service:
            raise Exception("Error", _(u"S'ha de especificar un partner id o un servei per poder decidir el ftp provider"))
        sp = [
            ('enabled', '=', True),
            ('{0}_enabled'.format(service), '=', True)
        ]
        if partner_id:
            sp.append(
                ('partner_id', '=', partner_id)
            )
        pids = self.search(cursor, uid, sp)
        if not len(pids):
            sp2 = [
                ('enabled', '=', True),
                ('{0}_enabled'.format(service), '=', True),
                ('partner_id', '=', False)
            ]
            pids = self.search(cursor, uid, sp2)
        if len(pids) > 1:
            raise osv.except_osv(_("Error de configuracio"), _(u"Hi ha mes de un provider configurat per el partner {0} i el servei {1}").format(partner_id, service))
        elif not len(pids):
            raise osv.except_osv(_("Error de configuracio"), _(u"No hi ha cap provider configurat per el partner {0} i el servei {1}").format(partner_id, service))
        return pids[0]

    def get_provider_browse(self, cursor, uid, service, partner_id=None, context=None):
        return self.browse(cursor, uid, self.get_provider(cursor, uid, service, partner_id=partner_id, context=context))

    def upload(self, cursor, uid, tpid, file_obj, filename, path="/", file_path=None, context=None):
        if isinstance(tpid, (list, tuple)):
            tpid = tpid[0]
        inf = self.browse(cursor, uid, tpid)
        if inf.sftp:
            server = inf.sftp
        elif inf.ftp:
            server = inf.ftp
        else:
            raise osv.except_osv(
                _(u"Error de configuració"),
                _(u"S'ha de definir un servidor ftp o sftp per a {0}").format(inf.name)
            )
        conn = server.login()

        return server.upload_file(conn, filename, path, file_obj=file_obj, file_path=file_path)

    def read_file(self, cursor, uid, tpid, file_path, context=None):
        inf = self.browse(cursor, uid, tpid)
        if inf.sftp:
            server = inf.sftp
        elif inf.ftp:
            server = inf.ftp
        else:
            raise osv.except_osv(
                _(u"Error de configuració"),
                _(u"S'ha de definir un servidor ftp o sftp per a {0}").format(inf.name)
            )
        conn = server.login()
        return server.read_file(conn, file_path)

    _columns = {
        'name': fields.char('Description', size=50),
        'enabled': fields.boolean('Enabled'),
        'partner_id': fields.many2one('res.partner', 'Utility'),
        'sftp': fields.many2one('giscedata.sftp.connections', 'SFTP service'),
        'ftp': fields.many2one('giscedata.ftp.connections', 'FTP service'),
    }

    _defaults = {
        'enabled': lambda *a: True,
    }

GiscedataFTPProvider()
