# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from stubserver import FTPStubServer
from io import BytesIO
from ftplib import FTP
from datetime import datetime
from base64 import b64encode, b64decode
from StringIO import StringIO
from addons import get_module_resource


class TestsFtpConnections(testing.OOTestCase):
    def setUp(self):
        self.server = FTPStubServer(0)
        self.server.run()
        self.port = self.server.server.server_address[1]

        self.txn = Transaction().start(self.database)

    def tearDown(self):
        self.txn.stop()
        self.server.stop()

    def test_create_and_connect(self):
        cursor = self.txn.cursor
        uid = self.txn.user

        ftp_obj = self.openerp.pool.get('giscedata.ftp.connections')

        params_create = {
            'name': 'FTP Mock Example',
            'ip_address': 'localhost',
            'user': 'user1',
            'password': 'passwd',
            'root_dir': '/',
            'read_dir': '/',
            'port': self.port
        }

        ftp_id = ftp_obj.create(cursor, uid, params_create)

        conn = ftp_obj.login(cursor, uid, ftp_id)

        # Up example file into newdir folder to check model functions
        conn.storlines('STOR /out_file/example_file.pdf', BytesIO(b'more data'))

        folder_files = ftp_obj.get_files(cursor, uid, [], conn, path='/out_file')

        self.assertEqual(len(folder_files), 1)

        self.assertEqual(folder_files[0], 'example_file.pdf')

        read_file = ftp_obj.read_file(
            cursor, uid, [], conn, '/out_file/example_file.pdf'
        )

        self.assertEqual(read_file, 'more data')

        ftp_obj.close(cursor, uid, [], conn)
