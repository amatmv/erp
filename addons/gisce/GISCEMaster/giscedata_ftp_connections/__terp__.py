# -*- coding: utf-8 -*-
{
    "name": "Gestion de conexiones FTP",
    "description": """Gestion de conexiones FTP""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base_extended",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_ftp_connections_view.xml",
        "giscedata_ftp_provider_view.xml",
        "giscedata_ftp_connections_data.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
