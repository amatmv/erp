# -*- coding: utf-8 -*-

from ftplib import all_errors, FTP, FTP_PORT
from paramiko import SFTP, Transport
from osv import osv, fields
from tools.translate import _
from StringIO import StringIO
import re
from os import sep

_CATEGORIES = [
    ('general', 'General')
]


class GiscedataFtpConnectionsCategory(osv.osv):

    _name = 'giscedata.ftp.connections.category'

    def name_get(self, cursor, uid, ids, context=None):
        result = []
        for record in self.browse(cursor, uid, ids, context):
            result.append((record.id, '{} / {}'.format(record.name, record.description)))
        return result

    _columns = {
        'name': fields.char('Nom', size=50, required=True),
        'code': fields.char('Codi', size=4, required=True),
        'description': fields.char('Descripció', size=150),
    }


GiscedataFtpConnectionsCategory()


# Based on addons/gisce/GISCEMaster/giscedata_telegestio/ftp.py
class GiscedataFtpConnections(osv.osv):
    """
    Modelo de datos para gestionar connexiones ftp de manera genrica
    """

    _name = 'giscedata.ftp.connections'
    _description = u'Conexiones FTP'

    def login(self, cursor, uid, server_id, context=None):
        '''login to a ftp server
        returns a connection to the server'''

        if isinstance(server_id, (list, tuple)):
            server_id = server_id[0]

        server = self.browse(cursor, uid, server_id, context=context)
        try:
            if server.port:
                ftpcon = FTP()
                ftpcon.connect(server.ip_address, server.port)
            else:
                ftpcon = FTP(server.ip_address)

            if server.anonymous:
                ftpcon.login()
            else:
                ftpcon.login(server.user, server.password)
        except all_errors, e:
            raise osv.except_osv(_('Error!'),
                                 _('Connection error: %s') % e)
        return ftpcon

    def close(self, cursor, uid,  ids, ftpcon, context=None):
        '''close connection to a ftp server'''
        return ftpcon.quit()

    def get_files(self, cursor, uid,  ids, ftpcon, path='/', context=None):
        '''return list of files to read'''
        try:
            res = ftpcon.nlst(path)
        except all_errors, e:
            raise osv.except_osv(_('Error!'),
                                 _('Directory listing error: %s') % (e))
        # some FTP servers returns full path, others only filename
        if res:
            res = [p.split('/')[-1] for p in res]
        return res

    def get_files_full_info(self, cursor, uid,  ids, ftpcon, path='/', context=None):
        '''return list of files to read'''
        res = []
        try:
            ftpcon.retrlines('LIST ' + path, res.append)
        except all_errors, e:
            raise osv.except_osv(_('Error!'),
                                 _('Directory listing error: %s') % (e))

        return res

    def read_file(self, cursor, uid,  ids, ftpcon, file, context=None):
        '''returns the content of a file as string'''
        output = StringIO()
        ftpcon.retrbinary('RETR %s' % file, output.write)
        return output.getvalue()

    def get_read_dirs(self, cursor, uid,  ids, server_id, context=None):
        if isinstance(server_id, (list, tuple)):
            server_id = server_id[0]

        server = self.read(cursor, uid, server_id, ['read_dir'], context=context)

        dir_list = server['read_dir'].split(';') or ['/']

        return dir_list

    def upload_file(self, cursor, uid, ids, ftpcon, filename, path="/", file_obj=None, file_path=None, context=None):
        """
        :param ftpcon: instance of FTP
        :param filename: name of file to upload
        :param path: path to dest on ftp
        :param file_obj: file object opened instance
        :param file_path: filename with absolute path to file in fs
        :param context:
        :return:
        """
        if not ftpcon.nlst(path):
            self.create_dirs(ftpcon, path)
        ftpcon.cwd(path)
        if not file_obj:
            file_obj = open(file_path, 'rb')
        link = 'STOR {}'.format(filename)
        try:
            res = ftpcon.storbinary(link, file_obj)  # send the file
            ftpcon.quit()
        except all_errors:
            return False
        finally:
            file_obj.close()

        return res

    @staticmethod
    def create_dirs(ftpcon, dir_path):
        tmp = dir_path.split(sep)
        dirs = []
        for p in tmp:
            if not dirs:
                dirs.append(p)
                continue
            dirs.append(dirs[-1] + '/' + p)
        for p in dirs:
            try:
                ftpcon.mkd(p)
            except all_errors:
                continue

    @staticmethod
    def parse_list_fromat(string_record):
        # From this machine guy https://gist.github.com/BenExile/2923448
        regex = (
            '^([\\-dbclps])'                  # Directory flag 
            '([\\-rwxs]{9})\\s+'              # Permissions
            '(\\d+)\\s+'                      # Number of items
            '(\\w+)\\s+'                      # File owner
            '(\\w+)\\s+'                      # File group
            '(\\d+)\\s+'                      # File size in bytes
            '(\\w{3}\\s+\\d{1,2}\\s+'         # 3-char month and 1/2-char day of the month [7]
            '(?:\\d{1,2}:\\d{1,2}|\\d{4}))'   # Time or year (need to check conditions) [+= 7]
            '\\s+(.+)$'                       # File/directory name [8]
        )
        # Split fuction sometimes return a "" on last pos
        # Size of array should be 9
        res = re.split(regex, string_record)
        if res:
            len_res = len(res)
            if len_res != 9:
                if len_res == 10 and not res[9]:
                    res = res[:9]
                else:
                    res = False

        return res

    _columns = {
        'name': fields.char('Descripció', size=150, required=True),
        'category_type': fields.many2one('giscedata.ftp.connections.category', 'Categoria'),
        'ip_address': fields.char('IP address', size=30, required=True),
        'user': fields.char('Usuari', size=100),
        'password': fields.char('Contrasenya', size=50),
        'anonymous': fields.boolean('Connexio anonima'),
        'root_dir': fields.char('Directori principal', size=20),
        'read_dir': fields.char('Directori de lectura(s)', size=255,
                                help='semi-colon(;) separated dir list'),
        'port': fields.integer('Port')

    }

    _defaults = {
        'anonymous': lambda *a: False,
        'root_dir': lambda *a: '/',
        'read_dir': lambda *a: '/',
        'port': lambda *a: FTP_PORT
    }

GiscedataFtpConnections()


# Based on addons/gisce/GISCEMaster/giscedata_telegestio/cch_server.py
class GiscedataSftpConnections(osv.osv):
    """
    Modelo de datos para gestionar connexiones sftp de manera genrica
    """

    _name = 'giscedata.sftp.connections'
    _description = u'Conexiones SFTP'

    def login(self, cursor, uid, server_id, context=None):
        '''login to a sftp server
        returns a connection to the server'''

        if isinstance(server_id, (list, tuple)):
            server_id = server_id[0]

        server = self.browse(cursor, uid, server_id, context=context)
        try:
            if server.port:
                transport = Transport((server.ip_address, server.port))
            else:
                transport = Transport(server.ip_address)

            if server.anonymous:
                transport.connect()
                sftpcon = SFTP.from_transport(transport)
            else:
                transport.connect(
                    username=server.user,
                    password=server.password
                )
                sftpcon = SFTP.from_transport(transport)
        except Exception, e:
            raise osv.except_osv(_('Error!'),
                                 _('Connection error: %s') % e)
        return sftpcon

    def close(self, cursor, uid,  ids, sftpcon, context=None):
        '''close connection to a ftp server'''
        return sftpcon.close()

    def get_files(self, cursor, uid,  ids, sftpcon, path='/', context=None):
        '''return list of files to read'''
        try:
            res = sftpcon.listdir(path)
        except Exception, e:
            raise osv.except_osv(_('Error!'),
                                 _('Directory listing error: %s') % (e))
        # some SFTP servers returns full path, others only filename
        if res:
            res = [p.split('/')[-1] for p in res]
        return res

    def get_files_full_info(self, cursor, uid,  ids, sftpcon, path='/', context=None):
        '''return list of files to read'''
        try:
            res = sftpcon.listdir_attr(path)
        except Exception, e:
            raise osv.except_osv(_('Error!'),
                                 _('Directory listing error: %s') % (e))

        return res

    def read_file(self, cursor, uid,  ids, sftpcon, file, context=None):
        '''returns the content of a file as string'''
        output = StringIO()
        remote_file = sftpcon.open(file, 'r')
        output.writelines([l for l in remote_file])
        return output.getvalue()

    def upload_file(self, cursor, uid,  ids, sftpcon, filename, path=".", file_obj=None, file_path=None, context=None):
        """uploads file to path.
        :param sftpcon: instance of SFTP
        :param filename: name of file to upload
        :param path: path to dest on ftp
        :param file_obj: file like object with read() method
        :param file_path: filename with absolute path to file in fs
        :param context:
        :return: True
        """
        if not file_obj and not file_path:
            raise osv.except_osv("Error", _(u"You must provide a file_path or a file_like object to upload"))
        if not file_obj and file_path:
            file_obj = open(file_path, 'r')

        sftpcon.putfo(file_obj, '{}/{}'.format(path, filename))
        return True

    def get_read_dirs(self, cursor, uid,  ids, server_id, context=None):
        if isinstance(server_id, (list, tuple)):
            server_id = server_id[0]

        server = self.read(cursor, uid, server_id, ['read_dir'], context=context)

        dir_list = server['read_dir'].split(';') or ['/']

        return dir_list

    _columns = {
        'name': fields.char('Descripció', size=150, required=True),
        'category_type': fields.many2one('giscedata.ftp.connections.category', 'Categoria'),
        'ip_address': fields.char('IP address', size=30, required=True),
        'user': fields.char('Usuari', size=100),
        'password': fields.char('Contrasenya', size=50),
        'anonymous': fields.boolean('Connexio anonima'),
        'root_dir': fields.char('Directori principal', size=20),
        'read_dir': fields.char('Directori de lectura(s)', size=255,
                                help='semi-colon(;) separated dir list'),
        'port': fields.integer('Port')

    }

    _defaults = {
        'anonymous': lambda *a: False,
        'root_dir': lambda *a: '/',
        'read_dir': lambda *a: '/',
        'port': lambda *a: 22
    }

GiscedataSftpConnections()
