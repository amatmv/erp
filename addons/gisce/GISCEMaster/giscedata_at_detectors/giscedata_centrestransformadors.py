# -*- coding: utf-8 -*-

from osv import osv, fields


class giscedata_cts(osv.osv):

    _name = "giscedata.cts"
    _inherit = "giscedata.cts"

    def _detectors(self, cr, uid, ids, field_name, ar, context=None):
        res = dict([(i, False) for i in ids])
        det = self.pool.get('giscedata.at.detectors')
        det_ids = det.search(cr, uid, [('situacio', 'like', 'giscedata.cts')],
                   context={'active_test': False})
        val = det.read(cr, uid, det_ids, ['situacio'])
        for i in val:
            ct_id = int(i['situacio'].split(',')[1])
            if not ct_id in ids:
                continue
            if not res[ct_id]:
                res[ct_id] = [i['id']]
            else:
                res[ct_id].append(i['id'])
        return res

    _columns = {
        'detectors': fields.function(_detectors, method=True,
                         type='one2many', relation='giscedata.at.detectors',
                         string='Detectors'),
    }

giscedata_cts()

# -*- coding: utf-8 -*-

from osv import osv, fields


class giscedata_cts(osv.osv):

    _name = "giscedata.cts"
    _inherit = "giscedata.cts"

    def _detectors(self, cr, uid, ids, field_name, ar, context=None):
        res = dict([(i, False) for i in ids])
        det = self.pool.get('giscedata.at.detectors')
        det_ids = det.search(cr, uid, [('situacio', 'like', 'giscedata.cts')],
                   context={'active_test': False})
        val = det.read(cr, uid, det_ids, ['situacio'])
        for i in val:
            ct_id = int(i['situacio'].split(',')[1])
            if not ct_id in ids:
                continue
            if not res[ct_id]:
                res[ct_id] = [i['id']]
            else:
                res[ct_id].append(i['id'])
        return res

    _columns = {
        'detectors': fields.function(_detectors, method=True,
                         type='one2many', relation='giscedata.at.detectors',
                         string='Detectors'),
    }

giscedata_cts()

