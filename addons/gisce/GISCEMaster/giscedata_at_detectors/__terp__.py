# -*- coding: utf-8 -*-
{
    "name": "GISCE Detectors",
    "description": """Detectors""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Alta Tensió",
    "depends":[
        "base_extended",
        "giscedata_at",
        "giscedata_cts"
    ],
    "init_xml": [],
    "demo_xml": [
        "giscedata_at_detectors_demo.xml",
        "giscedata_at_detectors_sequence_demo.xml",
    ],
    "update_xml":[
        "giscedata_at_detectors_view.xml",
        "giscedata_at_detectors_sequence.xml",
        "report/giscedata_at_detectors_report.xml",
        "report/giscedata_at_detectors_report_ultima.xml",
        "wizard/giscedata_at_detectors_wizard.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
