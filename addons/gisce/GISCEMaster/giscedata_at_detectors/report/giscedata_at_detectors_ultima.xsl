<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <document>
      <template bottomMargin="10mm" pageSize="(297mm,190mm)" rightMargin="10mm" topMargin="10mm">
        <pageTemplate id="main">
          <frame height="175mm" id="first" width="277mm" x1="10mm" y1="10mm"/>
        </pageTemplate>
      </template>
      <stylesheet> 
        <paraStyle fontName="Helvetica-Bold" fontSize="8" alignment="center" name="titol"/>
        <paraStyle fontName="Helvetica-Bold" fontSize="6" alignment="center" name="header"/>
        <paraStyle fontName="Helvetica" fontSize="6" alignment="center" name="data"/>
        <blockTableStyle id="data">
          <lineStyle kind="LINEABOVE" start="0,0" stop="-1,0" colorName="black"/>
          <lineStyle kind="LINEBEFORE" start="-1,0" stop="-1,1" colorName="black"/>
          <lineStyle kind="LINEAFTER" start="-1,0" stop="-1,1" colorName="black"/>
          <lineStyle kind="LINEBEFORE" start="0,0" stop="0,1" colorName="black"/>
          <lineStyle kind="LINEAFTER" start="0,0" stop="0,1" colorName="black"/>
          <lineStyle kind="LINEAFTER" start="1,0" stop="1,1" colorName="black"/>
          <lineStyle kind="LINEAFTER" start="2,0" stop="2,1" colorName="black"/>
          <lineStyle kind="LINEAFTER" start="3,0" stop="3,1" colorName="black"/>
          <lineStyle kind="LINEAFTER" start="4,0" stop="4,1" colorName="black"/>
          <lineStyle kind="LINEAFTER" start="5,0" stop="5,1" colorName="black"/>
          <lineStyle kind="LINEAFTER" start="6,0" stop="6,1" colorName="black"/>
          <lineStyle kind="LINEAFTER" start="7,0" stop="7,1" colorName="black"/>
          <lineStyle kind="LINEAFTER" start="8,0" stop="8,1" colorName="black"/>
          <lineStyle kind="LINEAFTER" start="9,0" stop="9,1" colorName="black"/>
          <lineStyle kind="LINEBELOW" start="0,0" stop="-1,0" colorName="black"/>
          <blockValign value="MIDDLE" start="0,0" stop="-1,-1"/>
        </blockTableStyle>
        <blockTableStyle id="header">
          <blockBackground colorName="lightgrey" start="0,0" stop="-1,0"/>
          <lineStyle kind="LINEABOVE" start="0,0" stop="-1,0" colorName="black"/>
          <lineStyle kind="LINEBEFORE" start="-1,0" stop="-1,1" colorName="black"/>
          <lineStyle kind="LINEAFTER" start="-1,0" stop="-1,1" colorName="black"/>
          <lineStyle kind="LINEBEFORE" start="0,0" stop="0,1" colorName="black"/>
          <lineStyle kind="LINEAFTER" start="0,0" stop="0,1" colorName="black"/>
          <lineStyle kind="LINEAFTER" start="1,0" stop="1,1" colorName="black"/>
          <lineStyle kind="LINEAFTER" start="2,0" stop="2,1" colorName="black"/>
          <lineStyle kind="LINEAFTER" start="3,0" stop="3,1" colorName="black"/>
          <lineStyle kind="LINEAFTER" start="4,0" stop="4,1" colorName="black"/>
          <lineStyle kind="LINEAFTER" start="5,0" stop="5,1" colorName="black"/>
          <lineStyle kind="LINEAFTER" start="6,0" stop="6,1" colorName="black"/>
          <lineStyle kind="LINEAFTER" start="7,0" stop="7,1" colorName="black"/>
          <lineStyle kind="LINEAFTER" start="8,0" stop="8,1" colorName="black"/>
          <lineStyle kind="LINEAFTER" start="9,0" stop="9,1" colorName="black"/>
          <lineStyle kind="LINEBELOW" start="0,0" stop="-1,0" colorName="black"/>
          <lineStyle kind="GRID" start="0,2" stop="-1,-1" colorName="black"/>
          <blockValign value="MIDDLE" start="0,0" stop="-1,-1"/>
        </blockTableStyle>
        <blockTableStyle id="header_rev">
          <blockValign value="MIDDLE" start="0,0" stop="-1,-1"/>
          <lineStyle kind="LINEBELOW" start="0,0" stop="-1,0" colorName="black"/>
          <lineStyle kind="LINEABOVE" start="0,-1" stop="-1,-1" colorName="black"/>
        </blockTableStyle>
      </stylesheet>
      <story>
      <spacer length="20"/>
      <para style="titol" t="1"> Llistat dels detectors de curtcircuit instal•lats a la xarxa de distribució elèctrica - última revisió</para>
      <spacer length="20"/>
      <!--<blockTable colWidths="17mm,50mm,10mm,35mm,25mm,15mm,15mm,15mm,35mm,65mm" style="header">-->
      <blockTable colWidths="17mm,50mm,10mm,35mm,25mm,10mm,10mm,10mm,10mm,15mm,25mm,65mm" style="header">
        <tr>
          <td t="1"><para style="header">Nom</para></td>
          <td t="1"><para style="header">Producte</para></td>
          <td t="1"><para style="header">Situació</para></td>
          <td t="1"><para style="header">LAT descripció</para></td>
          <td t="1"><para style="header">Municipi</para></td>
          <td t="1"><para style="header">Tipus Prog.</para></td>
          <td t="1"><para style="header">Temps led</para></td>
          <td t="1"><para style="header">Ampers fase</para></td>
          <td t="1"><para style="header">Ampers terra</para></td>
          <td t="1"><para style="header">Data alta</para></td>
          <!--<td t="1"><para style="header">Data pila</para></td>-->
          <td t="1"><para style="header">Observacions</para></td>
          <td t="1"><para style="header">Revisions</para></td>
        </tr>
      </blockTable>
      <xsl:for-each select="//arrel/detector">
        <xsl:variable name="codi" select="codi" />
        <!--<blockTable colWidths="17mm,50mm,10mm,35mm,25mm,15mm,15mm,15mm,35mm,65mm" style="data">-->
      <blockTable colWidths="17mm,50mm,10mm,35mm,25mm,10mm,10mm,10mm,10mm,15mm,25mm,65mm" style="data">
          <tr>
            <td><para style="data"><xsl:value-of select="nom"/></para></td>
            <td><para style="data"><xsl:value-of select="producte"/></para></td>
            <td><para style="data"><xsl:value-of select="situacio"/></para></td>
            <td><para style="data"><xsl:value-of select="lat_desc"/></para></td>
            <td><para style="data"><xsl:value-of select="municipi"/></para></td>
            <td><para style="data"><xsl:value-of select="tipus_prog"/></para></td>
            <td><para style="data"><xsl:value-of select="temps_pila"/></para></td>
            <td><para style="data"><xsl:value-of select="ampers_fase"/></para></td>
            <td><para style="data"><xsl:value-of select="ampers_neutre"/></para></td>
            <td><para style="data"><xsl:value-of select="data_alta"/></para></td>
            <!-- <td><para style="data"><xsl:value-of select="data_pila"/></para></td> -->
            <td><para style="data"><xsl:value-of select="observacions"/></para></td>
            <td>
              <xsl:if test="count(revisio) &gt; 0">
                <blockTable colWidths="20mm,40mm" style="header_rev">
                  <tr>
                      <td t="1"><para style="header">Data</para></td>
                      <td t="1"><para style="header">Actuació</para></td>
                  </tr>
                  <xsl:for-each select="revisio">
                    <xsl:sort select="date_rev" order="descending" />
                    <xsl:if test="not(position() > 1)">
                      <tr>
                        <td><para style="data"><xsl:value-of select="date_rev"/></para></td>
                        <td><para style="data"><xsl:value-of select="desc_rev"/></para></td>
                      </tr>
                    </xsl:if>
                  </xsl:for-each>
                </blockTable>
              </xsl:if>
            </td>
          </tr>
        </blockTable>
      </xsl:for-each>
      <xsl:if test="not(position() = last())"><nextPage/></xsl:if>
      </story>
    </document>
  </xsl:template>
</xsl:stylesheet>
