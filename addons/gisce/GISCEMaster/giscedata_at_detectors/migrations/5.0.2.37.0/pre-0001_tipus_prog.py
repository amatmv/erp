# -*- coding: utf-8 -*-

import netsvc

def migrate(cursor, installed_version):
    """Canvis a executar relatius a giscedata_at_detectors
    """
    logger = netsvc.Logger()
    logger.notifyChannel('migrations', netsvc.LOG_INFO, 'Migration from %s'
                         % installed_version)

    # backup
    cursor.execute("""alter table giscedata_at_detectors RENAME column
                      tipus_prog TO tipus_prog_old""")
