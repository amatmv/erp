# -*- coding: utf-8 -*-
"""Migrar les dades dels tipus_prog_old
"""
import pooler
import netsvc

def migrate(cursor, installed_version):
    logger = netsvc.Logger()
    logger.notifyChannel('migrations', netsvc.LOG_INFO,
                            "Migrant els tipus_prog_old")

    uid = 1
    pool = pooler.get_pool(cursor.dbname)
    detec_obj = pool.get('giscedata.at.detectors')

    cursor.execute("""select id,tipus_prog from giscedata_at_detectors_programacio""")
    progs = dict([(res[1], res[0]) for res in cursor.fetchall()])

    if progs:
        cursor.execute("""select id,tipus_prog_old from giscedata_at_detectors""")
        for detec in cursor.dictfetchall():
            if str(detec['tipus_prog_old']) in progs:
                tipus_prog_id = progs[str(detec['tipus_prog_old'])]
                detec_obj.write(cursor, uid, detec['id'], {'tipus_prog': tipus_prog_id})