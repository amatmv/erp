# -*- coding: utf-8 -*-

import logging

def up(cursor, installed_version):
    """Canvis a executar relatius a giscedata_at_detectors_programacio
    """
    if not installed_version:
        return
    logger = logging.getLogger('openerp.migration')
    logger.info('Rename ampers_neutre to old_ampers_neutre')
    cursor.execute("""ALTER TABLE giscedata_at_detectors_programacio RENAME COLUMN ampers_neutre TO old_ampers_neutre""")
    logger.info('Add column ampers_neutre')
    cursor.execute("""ALTER TABLE giscedata_at_detectors_programacio ADD COLUMN ampers_neutre integer""")
    logger.info('Copy values')
    cursor.execute("""UPDATE giscedata_at_detectors_programacio SET ampers_neutre = (old_ampers_neutre::integer)""")


def down(cursor, installed_version):
    pass

migrate = up