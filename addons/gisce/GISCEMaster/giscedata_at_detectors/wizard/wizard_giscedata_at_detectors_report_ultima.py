# *-* coding: utf-8 *-*

import wizard

_init_form = """<?xml version="1.0"?>
<form>
    <label string="Generar el llistat de detectors actius amb només l'última revisió"/>
</form>
"""

_init_fields = {
}

def _print_detectors(self, cr, uid, data, context={}):
    cr.execute("select id from giscedata_at_detectors where active is True")
    return { 'ids': [ a[0] for a in cr.fetchall() ] }

class wizard_giscedata_at_detectors_report_ultima(wizard.interface):
    states = {
        'init': {
          'actions': [],
          'result': {
              'type': 'form',
              'arch': _init_form,
              'fields': _init_fields,
              'state': [
                  ('end', 'Cancelar', 'gtk-cancel'),
                  ('print', 'Imprimir', 'gtk-print')
                  ]
              }
        },
        'print': {
          'actions': [_print_detectors],
          'result': {
              'type': 'print',
              'report': 'giscedata.at.detectors.report.ultima',
              'get_id_from_action': True,
              'state': 'end'
              }
        },
        'end': {
          'actions': [],
          'result': {
              'type': 'state',
              'state': 'end'
              }
        }
    }

wizard_giscedata_at_detectors_report_ultima('giscedata.at.detectors.report.ultima')