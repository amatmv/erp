# -*- coding: utf-8 -*-

from osv import osv, fields

class GiscedataAtDetectorsProgramacio(osv.osv):
    _name = 'giscedata.at.detectors.programacio'

    _rec_name = 'tipus_prog'

    _columns = {
        'tipus_prog': fields.char('Tipus programació', size=5),
        'temps_pila': fields.integer('Temps led [h]', help="Hores que aguanta "
                                           "el led del detector"),
        'ampers_fase': fields.char('Ampers fase [A]', size=8),
        'ampers_neutre': fields.integer('Ampers terra [A]'),
    }

GiscedataAtDetectorsProgramacio()


class GiscedataAtDetectors(osv.osv):

    _name = 'giscedata.at.detectors'
    _description = 'Detectors Alta Tensió'

    def _codi(self, cr, uid, ids, field_name, ar, context={}):
        res = {}
        detectors = self.browse(cr, uid, ids)
        for det in detectors:
            if det.tram_id and det.tram_id.linia:
                codi = '%s-%s-%s' % (det.tram_id.linia.name or '',
                                     det.tram_id.name or '',
                                     det.name or '')
            else:
                codi = ''
            res[det.id] = codi
        return res

    def _municipi_id(self, cr, uid, ids, field_name, ar, context={}):
        res = {}
        detectors = self.browse(cr, uid, ids)
        for det in detectors:
            if (det.tram_id
                    and det.tram_id.linia
                        and det.tram_id.linia.municipi):
                municipi_id = det.tram_id.linia.municipi.id
                municipi_name = det.tram_id.linia.municipi.name
                res[det.id] = (municipi_id, municipi_name)
            else:
                res[det.id] = False
        return res

    def _final(self, cr, uid, ids, field_name, ar, context={}):
        res = {}
        detectors = self.browse(cr, uid, ids)
        for det in detectors:
            if det.tram_id:
                final = det.tram_id.final
            else:
                final = False
            res[det.id] = final
        return res

    def _origen(self, cr, uid, ids, field_name, ar, context={}):
        res = {}
        detectors = self.browse(cr, uid, ids)
        for det in detectors:
            if det.tram_id:
                origen = det.tram_id.origen
            else:
                origen = False
            res[det.id] = origen
        return res

    def _lat_id(self, cr, uid, ids, field_name, ar, context={}):
        res = {}
        detectors = self.browse(cr, uid, ids)
        for det in detectors:
            if det.tram_id and det.tram_id.linia:
                lat_id = det.tram_id.linia.id
                lat_name = det.tram_id.linia.name
                res[det.id] = (lat_id, lat_name)
            else:
                res[det.id] = False
        return res

    def _lat_desc(self, cr, uid, ids, field_name, ar, context={}):
        res = {}
        detectors = self.browse(cr, uid, ids)
        for det in detectors:
            if det.tram_id and det.tram_id.linia:
                lat_desc = det.tram_id.linia.descripcio
            else:
                lat_desc = False
            res[det.id] = lat_desc
        return res

    def _product(self, cr, uid, ids, field_name, ar, context={}):
        res = {}
        detectors = self.browse(cr, uid, ids)
        for det in detectors:
            if det.stock_id and det.stock_id.product_id:
                product_id = det.stock_id.product_id.id
                product_name = det.stock_id.product_id.name
                res[det.id] = (product_id, product_name)
            else:
                res[det.id] = False
        return res

    def _data_pila(self, cr, uid, ids, field_name, ar, context={}):
        res = {}
        detectors = self.browse(cr, uid, ids)
        for det in detectors:
            data_canvi = False
            if det.stock_id:
                for rev in det.stock_id.revisions:
                    if not (rev.description and
                                ('#canvipila' in rev.description.lower() or
                                 '#cambiopila' in rev.description.lower())):
                        continue
                    data_canvi = ((not data_canvi or data_canvi < rev.date)
                                         and rev.date)
            res[det.id] = data_canvi
        return res

    def _situacio(self, cr, uid, ids, field_name, ar, context={}):
        res = {}
        detectors = self.browse(cr, uid, ids)
        for det in detectors:
            situacio = False
            if det.situacio:
                vals = det.situacio.split(',')
                m_name = vals[0]
                m_id = int(vals[1])
                if m_id:
                    mod = self.pool.get(m_name).browse(cr, uid, m_id)
                    loc = 'cts' in mod._table_name and 'CT' or 'Suport'
                    situacio = '%s: %s' % (loc, mod.name)
            res[det.id] = situacio
        return res

    def _lat_name_search(self, cr, uid, obj, name, args, context=None):
        if not args:
            return []
        res = self.search(cr, uid,
                    [('tram_id.linia.name', 'ilike', args[0][2])])
        if not res:
            return [('id', '=', '0')]
        return [('id', 'in', res)]

    def _lat_desc_search(self, cr, uid, obj, name, args, context=None):
        if not args:
            return []
        res = self.search(cr, uid,
                    [('tram_id.linia.descripcio', 'ilike', args[0][2])])
        if not res:
            return [('id', '=', '0')]
        return [('id', 'in', res)]

    def _municipi_search(self, cr, uid, obj, name, args, context=None):
        if not args:
            return []
        res = self.search(cr, uid,
                    [('tram_id.linia.municipi.name', 'ilike', args[0][2])])
        if not res:
            return [('id', '=', '0')]
        return [('id', 'in', res)]

    def _product_search(self, cr, uid, obj, name, args, context=None):
        if not args:
            return []
        res = self.search(cr, uid,
                    [('stock_id.product_id.name', 'ilike', args[0][2])])
        if not res:
            return [('id', '=', '0')]
        return [('id', 'in', res)]

    def _suggerir_name(self, cr, uid, context):

        return self.pool.get('ir.sequence').get(
            cr, uid, 'giscedata.at.detectors'
        )

    _columns = {
        'name': fields.char('Nom', size=16, readonly=True),
        'codi': fields.function(_codi, method=True, type='char',
                                string='Codi', size=60),
        'active': fields.boolean('Actiu'),
        'data_alta': fields.datetime('Data d\'alta'),
        'data_baixa': fields.datetime('Data de baixa'),
        'tram_id': fields.many2one('giscedata.at.tram', 'Tram'),
        'origen': fields.function(_origen, method=True, type='char',
                                  size=60, string='Origen'),
        'final': fields.function(_final, method=True, type='char',
                                  size=60, string='Final'),
        'lat_id': fields.function(_lat_id, method=True, type='many2one',
                          relation='giscedata.at.linia', string='Línia',
                          fnct_search=_lat_name_search),
        'lat_desc': fields.function(_lat_desc, method=True, type='char',
                                    size=60, string='Descripció línia',
                                    fnct_search=_lat_desc_search),
        'municipi_id': fields.function(_municipi_id, method=True,
                                 type='many2one', relation='res.municipi',
                                 string='Municipi',
                                 fnct_search=_municipi_search),
        'situacio': fields.reference(
            'Situació',
            selection=[
                ('giscedata.at.suport', 'Suport'),
                ('giscedata.cts', 'CT')
            ], size=128),
        'situacio_str': fields.function(_situacio, method=True, type='char',
                                        size=30, string="Situació"),
        'stock_id': fields.many2one('stock.production.lot', "Número de sèrie"),
        'product_id': fields.function(_product, method=True, type='many2one',
                                 relation="product.product", string='Producte',
                                 fnct_search=_product_search),
        'temps_pila': fields.related('tipus_prog', 'temps_pila', type='integer',
                                     string='Temps led [h]',
                                     help="Hores que aguanta el "
                                          "led del detector"),
        'data_pila': fields.function(_data_pila, method=True, type='date',
                                     string='Data canvi pila',
                                     help="La data s'obté de les revisions "
                                          "del producte identificant "
                                          "l'etiqueta #canvipila"),
        'ampers_fase': fields.related('tipus_prog', 'ampers_fase', type='char',
                                      size=8, string='Ampers fase [A]',
                                      readonly=True),
        'ampers_neutre': fields.related('tipus_prog', 'ampers_neutre',
                                        type='integer',
                                        string='Ampers terra [A]',
                                        readonly=True),
        'tipus_prog': fields.many2one('giscedata.at.detectors.programacio',
                                      "Tipus programació"),
        'observacions': fields.text('Observacions'),
    }

    _defaults = {
        'active': lambda *a: 1,
        'name': _suggerir_name
    }


GiscedataAtDetectors()