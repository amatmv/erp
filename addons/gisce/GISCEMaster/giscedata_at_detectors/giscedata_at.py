# -*- coding: utf-8 -*-

from osv import osv, fields


class giscedata_at_tram(osv.osv):

    _name = "giscedata.at.tram"
    _inherit = "giscedata.at.tram"

    _columns = {
        'detectors': fields.one2many('giscedata.at.detectors',
                                     'tram_id', "Detectors",
                                      context={'active_test': False}),
    }

giscedata_at_tram()


class giscedata_at_linia(osv.osv):

    _name = "giscedata.at.linia"
    _inherit = "giscedata.at.linia"
    
    def _detectors(self, cr, uid, ids, field_name, ar, context=None):
        res = dict([(i, False) for i in ids])
        det = self.pool.get('giscedata.at.detectors')
        det_ids = det.search(cr, uid, [('tram_id.linia.id', 'in', ids)],
                             context={'active_test': False})
        val = det.read(cr, uid, det_ids, ['lat_id'])
        for i in val:
            lat_id = i['lat_id'][0]
            if not res[lat_id]:
                res[lat_id] = [i['id']]
            else:
                res[lat_id].append(i['id'])
        return res

    _columns = {
        'detectors': fields.function(_detectors, method=True,
                         type='one2many', relation='giscedata.at.detectors',
                         string='Detectors'),
    }

giscedata_at_linia()


class giscedata_at_suport(osv.osv):

    _name = "giscedata.at.suport"
    _inherit = "giscedata.at.suport"

    def _detectors(self, cr, uid, ids, field_name, ar, context=None):
        res = dict([(i, False) for i in ids])
        det = self.pool.get('giscedata.at.detectors')
        det_ids = det.search(cr, uid, [('situacio', 'like', 'giscedata.at')],
                   context={'active_test': False})
        val = det.read(cr, uid, det_ids, ['situacio'])
        for i in val:
            sup_id = int(i['situacio'].split(',')[1])
            if not sup_id in ids:
                continue
            if not res[sup_id]:
                res[sup_id] = [i['id']]
            else:
                res[sup_id].append(i['id'])
        return res

    _columns = {
        'detectors': fields.function(
            _detectors, method=True, type='one2many',
            relation='giscedata.at.detectors',
            string='Detectors', select=True
        ),
    }

giscedata_at_suport()


class giscedata_at_tram(osv.osv):

    _name = "giscedata.at.tram"
    _inherit = "giscedata.at.tram"

    _columns = {
        'detectors': fields.one2many('giscedata.at.detectors',
                                     'tram_id', "Detectors",
                                      context={'active_test': False}),
    }

giscedata_at_tram()


class giscedata_at_linia(osv.osv):

    _name = "giscedata.at.linia"
    _inherit = "giscedata.at.linia"
    
    def _detectors(self, cr, uid, ids, field_name, ar, context=None):
        res = dict([(i, False) for i in ids])
        det = self.pool.get('giscedata.at.detectors')
        det_ids = det.search(cr, uid, [('tram_id.linia.id', 'in', ids)],
                             context={'active_test': False})
        val = det.read(cr, uid, det_ids, ['lat_id'])
        for i in val:
            lat_id = i['lat_id'][0]
            if not res[lat_id]:
                res[lat_id] = [i['id']]
            else:
                res[lat_id].append(i['id'])
        return res

    _columns = {
        'detectors': fields.function(_detectors, method=True,
                         type='one2many', relation='giscedata.at.detectors',
                         string='Detectors'),
    }

giscedata_at_linia()


class giscedata_at_suport(osv.osv):

    _name = "giscedata.at.suport"
    _inherit = "giscedata.at.suport"

    def _detectors(self, cr, uid, ids, field_name, ar, context=None):
        res = dict([(i, False) for i in ids])
        det = self.pool.get('giscedata.at.detectors')
        det_ids = det.search(cr, uid, [('situacio', 'like', 'giscedata.at')],
                   context={'active_test': False})
        val = det.read(cr, uid, det_ids, ['situacio'])
        for i in val:
            sup_id = int(i['situacio'].split(',')[1])
            if not sup_id in ids:
                continue
            if not res[sup_id]:
                res[sup_id] = [i['id']]
            else:
                res[sup_id].append(i['id'])
        return res

    _columns = {
        'detectors': fields.function(_detectors, method=True,
                         type='one2many', relation='giscedata.at.detectors',
                         string='Detectors'),
    }

giscedata_at_suport()
