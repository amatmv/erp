# coding=utf-8
import logging
from tqdm import tqdm
from oopgrade.oopgrade import column_exists, drop_columns


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')

    if column_exists(cursor, 'res_municipi', 'presentar_informe'):
        logger.info('Column presentar_informe already exists. Passing')
        return

    # Create new column and set default value

    alter_table_res_muicipi_create_column_presentar_informe_as_false = (
        'ALTER TABLE res_municipi '
        'ADD presentar_informe boolean NOT NULL '
        'DEFAULT false '
    )

    logger.info('Creating column presentar_informe and setting to false')
    cursor.execute(
        alter_table_res_muicipi_create_column_presentar_informe_as_false
    )

    logger.info(
        'Created column presentar_informe for '
        'all municipis and set default value false'
    )


def down(cursor, installed_version):
    drop_columns(cursor, ('res_municipi', 'presentar_informe'))


migrate = up
