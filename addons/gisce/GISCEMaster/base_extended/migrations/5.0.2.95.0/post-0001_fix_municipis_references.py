# coding=utf-8

import logging
from psycopg2.extensions import AsIs


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')

    logger.info(
        "Update the reference to the new municipis: "
        "Oza-Cesuras and Cerdedo-Cotobade."
    )

    ines_list = [
        {
            'new_ine': '15902',  # Oza-Cesuras
            'old_ines': ('15026', '15063')  # Oza dos Ríos, Cesuras
        },
        {
            'new_ine': '36902',  # Cerdedo-Cotobade
            'old_ines': ('36011', '36012')  # Cerdedo, Cotobade
        }
    ]

    query = """
        SELECT kcu.column_name AS field, kcu.table_name AS table
        FROM information_schema.constraint_column_usage ccu
        LEFT JOIN information_schema.key_column_usage kcu 
           ON kcu.constraint_name=ccu.constraint_name
        LEFT JOIN information_schema.table_constraints tc 
           ON tc.constraint_name=ccu.constraint_name
        WHERE ccu.table_name='res_municipi' and constraint_type='FOREIGN KEY';
    """

    cursor.execute(query)

    for rel in cursor.dictfetchall():
        table = rel['table']
        field = rel['field']
        update_query = """
            UPDATE %(table)s AS destiny 
            SET %(field)s = source.id
            FROM (
                SELECT id 
                FROM res_municipi
                WHERE ine = %(new_ine)s
            ) AS source
            WHERE destiny.%(field)s IN (
                SELECT id 
                FROM res_municipi
                WHERE ine IN %(old_ines)s
            )
        """
        for params in ines_list:
            cursor.execute(
                update_query, {
                    'table': AsIs(table),
                    'field': AsIs(field),
                    'new_ine': params['new_ine'],
                    'old_ines': params['old_ines']
                }
            )

    logger.info("References updated correctly.")


def down(cursor, installed_version):
    pass


migrate = up
