# -*- coding: utf-8 -*-

import netsvc


def migrate(cursor, installed_version):
    logger = netsvc.Logger()
    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         'Deleting old reports')
    """ Al passar de versions antigues de ERP a version més noves es va afegir
    l'id als reports xmls. Això va provocar que alguns reports es dupliquéssin
    Aquesta consulta busca els reports repetits que no tenen cap mòdul associat
    i els esborra, ja que en alguns casos provoqen errors
    """
    cursor.execute("""DELETE FROM ir_act_report_xml WHERE id IN (
    SELECT r.id
    FROM ir_act_report_xml r
    LEFT JOIN ir_model_data i
        ON i.res_id = r.id and i.model = 'ir.actions.report.xml'
    WHERE report_name IN  (
        SELECT report_name
        FROM ir_act_report_xml
        GROUP BY report_name
        HAVING COUNT(*) > 1)
    AND i.module is NULL)
    """)
    logger.notifyChannel('migration', netsvc.LOG_INFO,
        '  %d Old reports deleted' % cursor.rowcount or 0)
    logger.notifyChannel('migration', netsvc.LOG_INFO, 'Done.')
