#!/usr/bin/env python
# -*- coding: utf-8 -*-

import optparse
import csv
import sys

parser = optparse.OptionParser(version="[GISCE ERP] Migrations")

group = optparse.OptionGroup(parser, "File Related")
group.add_option("-f", "--csv", dest="csv_file", help="specify the cvs file")
group.add_option("--delimiter", dest="delimiter", help="specify the delimiter for cvs file")
parser.add_option_group(group)

options = optparse.Values()
options.delimiter = ','
parser.parse_args(values=options)

csv_file = options.csv_file
delimiter = options.delimiter


xml = """      <record model="res.municipi" id="ine_%s">
        <field name="ine">%s</field>
        <field name="dc">%s</field>
        <field name="name">%s</field>
        <field search="[('code','=','%s')]" model='res.country.state' name='state'/>
      </record>
"""

sys.stdout.write("""<?xml version="1.0" encoding="UTF-8"?>
<openerp>
    <data>
""")
reader = csv.reader(open(csv_file, "rUb"), delimiter=delimiter)
lines = []
for row in reader:
    if not row[0][0].isdigit():
        continue
    lines.append(row)

for row in sorted(lines, key=lambda row: row[0]+row[1]):
    state = row[0]
    muni = row[1]
    ine = state + muni
    dc = row[2]
    name = row[3]
    sys.stdout.write(xml % (ine, ine, dc, name, state))
sys.stdout.write("""    </data>
</openerp>
""")
sys.stdout.flush()
