#!/usr/bin/env python
# -*- coding: utf-8 -*-

import optparse
import csv
import sys

parser = optparse.OptionParser(version="[GISCE ERP] Migrations")

group = optparse.OptionGroup(parser, "File Related")
group.add_option("-f", "--csv", dest="csv_file", help="specify the csv file")
group.add_option("--delimiter", dest="delimiter",
                             help="specify the delimiter for cvs file")
group.add_option("-d", "--data", dest="data_file",
                             help="specify the villages data file to upgrade")
parser.add_option_group(group)

options = optparse.Values()
options.delimiter = ','
parser.parse_args(values=options)

csv_file = options.csv_file
delimiter = options.delimiter

xml = """      <record model="res.comarca" id="%s">
        <field name="name">%s</field>
        <field name="codi">%s</field>
      </record>
"""

sys.stdout.write("""<?xml version="1.0" encoding="UTF-8"?>
<openerp>
    <data>
""")
reader = csv.reader(open(csv_file, "rUb"), delimiter=delimiter)
lines = []
for row in reader:
    if not row[0][0].isdigit():
        continue
    lines.append(row)

municipis = {}
comarques = {}
seq_com = 0
codi_com = ''
for row in sorted(lines, key=lambda row: row[2]):
    name = row[2]
    if not name in comarques:
        cm_id = "cm_%s" % seq_com
        seq_com += 1
        codi_com = row[3][0:6]
        sys.stdout.write(xml % (cm_id, name, codi_com))
        comarques.update({name: cm_id})
    ine = "ine_%s" % row[0][0:5]
    municipis.update({ine: comarques[name]})
sys.stdout.write("""    </data>
</openerp>
""")
sys.stdout.flush()

if not options.data_file:
    sys.exit()
# Actualitzar el .data amb la referència de la comarca
id_len = 9
id_start = len("id=\"")
data_file = options.data_file
in_file = open(data_file, 'r').readlines()
out_file = open('%s_new' % data_file, 'w')
for line in in_file:
    out_file.write(line)
    if line.find('res.municipi') > -1:
        pos_tag = line.find('id=')
        ref_ine = line[pos_tag+id_start:pos_tag+id_start+id_len]
        if municipis.get(ref_ine, False):
            str_comarca = "\t\t<field name=\"comarca\" ref=\"%s\"></field>\n" % \
                                                           municipis[ref_ine]
            out_file.write(str_comarca)

