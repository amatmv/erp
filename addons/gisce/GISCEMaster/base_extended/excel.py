from xlwt import Workbook, easyxf


class Sheet(object):

    def __init__(self, name, encoding='utf-8'):
        self.__xls = Workbook(encoding=encoding)
        self.__sheet = self.__xls.add_sheet(name)

        # xlwt can only store 4094 diffrent easyfx styles
        # so you can only call easyxf 4094 times, be careful
        self.__float_style = easyxf("", "#,###.00")

    def add_row(self, row):
        c = 0
        r = self.__sheet.last_used_row + 1
        for cell in row:
            if isinstance(cell, float):
                self.__sheet.write(r, c, cell, self.__float_style)
            else:
                self.__sheet.write(r, c, cell)
            c += 1

    def save(self, output):
        self.__xls.save(output)
