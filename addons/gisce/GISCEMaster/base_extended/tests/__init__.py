# -*- coding: utf-8 -*-
import unittest
from destral import testing
from destral.transaction import Transaction


class BaseTest(testing.OOTestCase):

    def test_vat_validation(self):
        with Transaction().start(self.database) as txn:
            partner_obj = self.openerp.pool.get('res.partner')
            cursor = txn.cursor
            uid = txn.user

            partner_ids = [1]

            # Valid DNI
            valid_dni = 'ES11111111H'
            partner_obj.write(cursor, uid, partner_ids, {'vat': valid_dni})
            self.assertEquals(
                partner_obj.get_vat_type(cursor, uid, partner_ids), 'NI'
            )
            self.assertFalse(partner_obj.is_nie_vat(valid_dni))
            self.assertFalse(partner_obj.is_passport_vat(valid_dni))
            self.assertFalse(partner_obj.is_enterprise_vat(valid_dni))

            # Invalid DNI
            invalid_dnis = ['ES11111111T', 'NL11111111H']
            for invalid_dni in invalid_dnis:
                partner_obj.write(
                    cursor, uid, partner_ids, {'vat': invalid_dni}
                )
                self.assertEquals(
                    partner_obj.get_vat_type(cursor, uid, partner_ids), 'OT'
                )
                self.assertFalse(partner_obj.is_enterprise_vat(invalid_dni))
                self.assertFalse(partner_obj.is_nie_vat(invalid_dni))
                self.assertFalse(partner_obj.is_passport_vat(invalid_dni))

            # Valid CIF
            valid_cif = 'ESA11111119'
            partner_obj.write(cursor, uid, partner_ids, {'vat': valid_cif})
            self.assertEquals(
                partner_obj.get_vat_type(cursor, uid, partner_ids), 'NI'
            )
            self.assertFalse(partner_obj.is_dni_vat(valid_cif))
            self.assertFalse(partner_obj.is_nie_vat(valid_cif))
            self.assertFalse(partner_obj.is_passport_vat(valid_cif))

            # Invalid CIF
            invalid_cif = 'ESA11111118'
            partner_obj.write(cursor, uid, partner_ids, {'vat': invalid_cif})
            self.assertEquals(
                partner_obj.get_vat_type(cursor, uid, partner_ids), 'OT'
            )
            self.assertFalse(partner_obj.is_dni_vat(invalid_cif))
            self.assertFalse(partner_obj.is_nie_vat(invalid_cif))
            self.assertFalse(partner_obj.is_passport_vat(invalid_cif))

            # Valid NIE
            valid_nie = 'ESX1111111G'
            partner_obj.write(cursor, uid, partner_ids, {'vat': valid_nie})
            self.assertEquals(
                partner_obj.get_vat_type(cursor, uid, partner_ids), 'NE'
            )
            self.assertFalse(partner_obj.is_dni_vat(valid_nie))
            self.assertFalse(partner_obj.is_enterprise_vat(valid_nie))
            self.assertFalse(partner_obj.is_passport_vat(valid_nie))

            # Invalid NIE
            invalid_nie = 'ESX1111111A'
            partner_obj.write(cursor, uid, partner_ids, {'vat': invalid_nie})
            self.assertEquals(
                partner_obj.get_vat_type(cursor, uid, partner_ids), 'OT'
            )
            self.assertFalse(partner_obj.is_dni_vat(invalid_nie))
            self.assertFalse(partner_obj.is_enterprise_vat(invalid_nie))
            self.assertFalse(partner_obj.is_passport_vat(invalid_nie))

            # Valid Passport
            valid_passport = 'PSABC000000A'
            partner_obj.write(cursor, uid, partner_ids, {'vat': valid_passport})
            self.assertEquals(
                partner_obj.get_vat_type(cursor, uid, partner_ids), 'PS'
            )
            self.assertFalse(partner_obj.is_dni_vat(valid_passport))
            self.assertFalse(partner_obj.is_enterprise_vat(valid_passport))
            self.assertFalse(partner_obj.is_nie_vat(valid_passport))

            # Invalid Passport
            invalid_passport = 'SPABC00ZZ00Z'
            partner_obj.write(cursor, uid, partner_ids, {'vat': invalid_passport})
            self.assertEquals(
                partner_obj.get_vat_type(cursor, uid, partner_ids), 'OT'
            )
            self.assertFalse(partner_obj.is_dni_vat(invalid_passport))
            self.assertFalse(partner_obj.is_enterprise_vat(invalid_passport))
            self.assertFalse(partner_obj.is_nie_vat(invalid_passport))

            # Invalid VAT
            invalid_vats = ['', 'ES']
            for invalid_vat in invalid_vats:
                partner_obj.write(
                    cursor, uid, partner_ids, {'vat': invalid_vat}
                )
                self.assertEquals(
                    partner_obj.get_vat_type(cursor, uid, partner_ids), 'OT'
                )
                self.assertFalse(partner_obj.is_dni_vat(invalid_vat))
                self.assertFalse(partner_obj.is_enterprise_vat(invalid_vat))
                self.assertFalse(partner_obj.is_nie_vat(invalid_vat))
                self.assertFalse(partner_obj.is_passport_vat(invalid_vat))

    def test_vate_es_empresa_no_vat(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            partner_obj = self.openerp.pool.get('res.partner')
            es_empresa = partner_obj.vat_es_empresa(cursor, uid, None)
            self.assertFalse(es_empresa)

    def test_separar_cognoms(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            partner_obj = self.openerp.pool.get('res.partner')

            # Test a name with the standard format: 'C1 C2, N'
            test_name = "Cg1 Cg2,  Nm"
            res = partner_obj.separa_cognoms(cursor, uid, test_name)
            self.assertFalse(res['fuzzy'])
            self.assertEqual(res['nom'], 'Nm')
            self.assertEqual(res['cognoms'][0], 'Cg1')
            self.assertEqual(res['cognoms'][1], 'Cg2')

            # Change config var for partner name format
            conf_obj = self.openerp.pool.get('res.config')
            var_id = conf_obj.search(
                cursor, uid, [('name', '=', 'partner_name_format')]
            )[0]
            conf_obj.write(cursor, uid, var_id, {'value': 'N C1 C2'})

            # Test a name with the alternative format: 'N C1 C2'
            test_name = "Nm  Cg1 Cg 2"
            res = partner_obj.separa_cognoms(cursor, uid, test_name)
            self.assertTrue(res['fuzzy'])
            self.assertEqual(res['nom'], 'Nm')
            self.assertEqual(res['cognoms'][0], 'Cg1')
            self.assertEqual(res['cognoms'][1], 'Cg 2')

            # Test a name with only name: 'N'
            test_name = "Nm"
            res = partner_obj.separa_cognoms(cursor, uid, test_name)
            self.assertFalse(res['fuzzy'])
            self.assertEqual(res['nom'], 'Nm')
            self.assertEqual(res['cognoms'][0], '')
            self.assertEqual(res['cognoms'][1], '')

            # Test special characters
            test_name = "Mª Angels Cg1 Cg 2"
            res = partner_obj.separa_cognoms(cursor, uid, test_name)
            self.assertTrue(res['fuzzy'])
            self.assertEqual(res['nom'], 'Mª')
            self.assertEqual(res['cognoms'][0], 'Angels')
            self.assertEqual(res['cognoms'][1], 'Cg1 Cg 2')

    def test_wizard_unify_partners(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            context = {}

            imd_obj = self.openerp.pool.get('ir.model.data')
            wiz_obj = self.openerp.pool.get('wizard.unify.partners')

            asus = imd_obj.get_object_reference(
                cursor, uid, 'base', 'res_partner_asus'
            )[1]

            tinyatwork = imd_obj.get_object_reference(
                cursor, uid, 'base', 'res_partner_tinyatwork'
            )[1]

            wiz_parameters = {
                'partner_to_unify': asus,
                'partner_to_delete': tinyatwork,
            }
            wiz_id = wiz_obj.create(cursor, uid, wiz_parameters, context=context)

            attach_obj = self.openerp.pool.get('ir.attachment')
            attach_obj.create(cursor, uid, {'res_model': 'res.partner', 'res_id': asus, 'name': 'a'}, context=context)
            attach_obj.create(cursor, uid, {'res_model': 'res.partner', 'res_id': tinyatwork, 'name': 'b'}, context=context)

            attach_2_ids = attach_obj.search(cursor, uid, [
                ('res_model', '=', 'res.partner'), ('res_id', '=', asus)]
            )
            self.assertEqual(len(attach_2_ids), 1)
            attach_8_ids = attach_obj.search(cursor, uid, [
                ('res_model', '=', 'res.partner'), ('res_id', '=', tinyatwork)
            ])
            self.assertEqual(len(attach_8_ids), 1)

            wiz = wiz_obj.browse(cursor, uid, wiz_id, context=context)
            wiz.action_unify_partners(context=context)

            attach_2_ids = attach_obj.search(cursor, uid, [('res_id', '=', asus)])
            self.assertEqual(len(attach_2_ids), 2)
            attach_8_ids = attach_obj.search(cursor, uid, [('res_id', '=', tinyatwork)])
            self.assertEqual(len(attach_8_ids), 0)

            queries = wiz.get_queries(context=context)
            self.assertEqual(len(queries), 9)
            for query in queries:
                field = query[0].lower().split(' set ')[1].split(' where')[0].split('=')[0]
                table = query[0].lower().split(' set ')[0].replace('update ', '')
                if field and table:
                    model = self.openerp.pool.get(table.replace('_', '.'))
                    if model is not None:
                        res = model.search(cursor, uid, [(field, '=', 8)])
                    self.assertEqual(res, [])
            self.assertIn('S\'han unificat', wiz.info)




