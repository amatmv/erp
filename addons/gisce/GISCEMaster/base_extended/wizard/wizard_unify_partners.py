# -*- coding: utf-8 -*-
import json

from osv import osv, fields
from tools.translate import _


class WizardUnifyPartners(osv.osv_memory):
    """Wizard pel switching
    """
    _name = 'wizard.unify.partners'

    def get_queries(self, cursor, uid, ids, context=None):
        query = """SELECT 'update ' || kcu.table_name || ' set ' || kcu.column_name || '={partner_to_unify_id} where ' || 
                kcu.column_name || '={partner_to_delete_id};' 
                FROM information_schema.constraint_column_usage ccu
                LEFT JOIN information_schema.key_column_usage kcu ON kcu.constraint_name = ccu.constraint_name
                LEFT JOIN information_schema.table_constraints tc ON tc.constraint_name = ccu.constraint_name
                WHERE ccu.table_name = 'res_partner' and constraint_type = 'FOREIGN KEY';"""

        cursor.execute(query)
        res = cursor.fetchall()
        if not res:
            raise osv.except_osv('Error',
                                 _(u"S'ha produït un error en el procés de generació de les queries"))

        query_create = """   CREATE OR REPLACE FUNCTION search_reference(
                    needle text
                )
                RETURNS table(update_q text)
                AS $$
                declare
                  count_res integer := 0;
                  table_name varchar;
                  column_name varchar;
                begin
                  FOR table_name,column_name IN
                    SELECT c.table_name, c.column_name from information_schema.columns c where data_type = 'character varying' 
                    and character_maximum_length = 128
                  LOOP
                    EXECUTE format('SELECT count(*) FROM %s WHERE %s like ''%s,%%''',
                       table_name,
                       column_name,
                       needle
                    ) INTO count_res;
                    IF count_res > 0 THEN
                      update_q := format('update %I SET %I = ''%s,{partner_to_unify_id}'' WHERE %I like ''%s,{partner_to_delete_id}'';', table_name,column_name,needle,column_name,needle);
                      RETURN NEXT;
                    END IF;
                 END LOOP;
                END;
                $$ language plpgsql;

                select * from search_reference('res.partner'); """
        cursor.execute(query_create)
        res_2 = cursor.fetchall()
        res.extend(res_2)
        res.append((u"update ir_attachment set res_id ={partner_to_unify_id} where res_model='res.partner' and res_id={partner_to_delete_id};",))
        return res

    def action_unify_partners(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        wiz = self.browse(cursor, uid, ids[0], context)
        partner_to_unify = wiz.partner_to_unify
        partner_to_delete = wiz.partner_to_delete

        if partner_to_unify.id == partner_to_delete.id:
            raise osv.except_osv('Error',
                                 _(u"El partner que vol unificar i el que vol eliminar son el mateix. Ha de "
                                   u"seleccionar dos partners diferents"))

        res = self.get_queries(cursor, uid, ids, context=context)
        for sql in res:
            cursor.execute(sql[0].format(
                partner_to_unify_id=partner_to_unify.id,
                partner_to_delete_id=partner_to_delete.id
            ))

        self.write(cursor, uid, ids, {
            'state': 'done',
            'info': "S'han unificat els partners {} i {}.\nEl partner resultant és {} amb id ({})".format(
                partner_to_unify.name, partner_to_delete.name, partner_to_unify.name, partner_to_unify.id
            )
        })

        partner_obj = self.pool.get('res.partner')
        partner_obj.unlink(cursor, uid, partner_to_delete.id, context=context)
        return True

    _columns = {
        'partner_to_unify': fields.many2one('res.partner', 'Client a mantenir', required=True, ondelete='cascade'),
        'partner_to_delete': fields.many2one('res.partner', 'Client a eliminar', required=True, ondelete='cascade'),
        'info': fields.text('Info', size=4000),
        'state': fields.char('State', size=16),
    }

    _defaults = {
        'state': lambda *a: 'init',
    }


WizardUnifyPartners()