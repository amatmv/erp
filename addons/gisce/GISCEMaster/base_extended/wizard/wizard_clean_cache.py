# -*- coding: utf-8 -*-
from osv import fields, osv
from tools.translate import _
from tools.misc import cache


class WizardCleanCache(osv.osv_memory):
    """Wizard"""
    _name = 'wizard.clean.cache'

    def action_clean_cache(self, cursor, uid, ids, context=None):
        cache.clean_caches_for_db(cursor.dbname)
        self.write(cursor, uid, ids, {'info': _(u"S'ha netejat la cache per la base de dades {0}.").format(cursor.dbname)})
        return

    _columns = {
        'info': fields.text('Informació', readonly=True),
    }


WizardCleanCache()
