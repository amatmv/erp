# -*- coding: utf-8 -*-
"""Conjunt de models per modificar el comportament base."""

from osv import osv, fields
import netsvc
import time
import threading
import multiprocessing
import traceback
import times
from hashlib import sha1
from tools.config import config
from tools import ustr
import pooler
from tools.translate import _
from oorq.decorators import job
from oorq.tasks import execute
from oorq.oorq import setup_redis_connection, oorq_log as log
from rq import Queue, get_current_job, job as rq_job
from rq.job import JobStatus, _job_stack
import re
from stdnum import es
import warnings


class NoDependency(object):
    def __init__(self):
        self.stack = _job_stack

    def __enter__(self):
        self.stack.push(None)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.stack.pop()


def compute_hash_instance(dbname, osv_object, method, *args):
    if args:
        args = '-'.join([str(x) for x in args])
    return sha1('%s-%s-%s-%s' % (dbname, osv_object, method, args)).hexdigest()


class res_comunitat_autonoma(osv.osv):
    """Comunitat Autònomes"""
    _name = 'res.comunitat_autonoma'
    _description = 'Comunitats Autonomes'

    def get_ccaa_from_municipi(self, cursor, uid, id_municipi):
        # retorna la comunitat autonoma per un id de municipi
        cursor.execute("""
        select com.id
        from res_municipi as m
        left join res_country_state as provincia on (m.state = provincia.id)
        left join res_comunitat_autonoma as com on (provincia.comunitat_autonoma = com.id)
        where m.id = %s
         """, [id_municipi])
        comunitat_id = cursor.fetchall()[0]
        return comunitat_id

    _columns = {
        'name': fields.char('Nom', size=60, required=True),
        'codi': fields.char('Codi', size=10, required=True),
    }

    _defaults = {
    }

    _order = "name asc"

res_comunitat_autonoma()


class res_country_state(osv.osv):

    _name = 'res.country.state'
    _inherit = 'res.country.state'

    _columns = {
        'ree_code': fields.char('Código REE', size=2),
        'comunitat_autonoma': fields.many2one('res.comunitat_autonoma', 'codi',
                                              ondelete='cascade'),
    }
res_country_state()


class ResComarca(osv.osv):
    """Comarca + Codi INE."""
    _name = 'res.comarca'
    _description = 'Comarques'
    _columns = {
        'name': fields.char('Nom', size=60, required=True),
        'codi': fields.char('Codi', size=10, required=True),
    }

    _defaults = {

    }

    _order = "name asc"

ResComarca()


class ResMunicipi(osv.osv):
    """Municipis + Codi INE."""
    _name = 'res.municipi'
    _description = 'Municipi + Codi INE'
    _columns = {
        'state': fields.many2one('res.country.state', 'Provincia',
                                 ondelete='cascade'),
        'name': fields.char('Nom Municipi', size=60, required=True),
        'ine': fields.char('INE Municipi', size=10, required=True),
        'dc': fields.char('DC', size=1, readonly=True),
        'comarca': fields.many2one('res.comarca', 'Comarca'),
        'subsistema_id': fields.many2one(
            'res.subsistemas.electricos', 'Subsistema Elèctric', required=True
        ),
        'presentar_informe': fields.boolean(
            'Presentar informe',
            help=_(
                'Especifica si el municipi requereix '
                'd\'informe de tasas municipals'
            )
        )
    }

    _defaults = {
        'presentar_informe': lambda *a: False,
    }

    _order = "name asc"

ResMunicipi()


class ResPoblacio(osv.osv):
    """Localitats personalitzades."""
    _name = 'res.poblacio'

    _columns = {
        'name': fields.char('Població', size=60, required=True),
        'municipi_id': fields.many2one('res.municipi', 'Municipi',
                                       required=True)
    }

    _order = "name asc"

    _sql_constraints = [('name_unique',
                         'unique (name, municipi_id)',
                         _(u'Ja existeix la població per aquest municipi'))
                        ]

ResPoblacio()


class ResPartner(osv.osv):
    """Model res_partner. Hi afegim funcions bàsiques."""
    _name = 'res.partner'
    _inherit = 'res.partner'

    def _cifnif(self, cr, uid, ids, field_name, args, context=None):
        """Retorna CI o NI segons l'identificació sigui CIF/NIF."""

        ids = map(int, ids)
        res = {}
        cifnif = {True: 'CI', False: 'NI'}
        cr.execute("""\
select 
    id,coalesce(vat, 'ES0') as vat
from 
    res_partner 
where id in (%s)""" % ', '.join(map(str, ids)))
        for partner in cr.dictfetchall():
            res[partner['id']] = cifnif[self.vat_es_empresa(cr, uid,
                                                            partner['vat'])]
        return res

    def separa_cognoms(self, cursor, uid, name, context=None):
        """ Retorna un diccionar amb el nom i els cognoms deduïts
            No cal fiar-se'n 100%
            Si el 2on cognom té un espai fuzzy=1"""
        if not context:
            context = {}
        conf_obj = self.pool.get('res.config')
        name_struct = conf_obj.get(
            cursor, uid, 'partner_name_format', 'C1 C2, N'
        )
        # separem el nom i els cognoms
        if name_struct == 'N C1 C2':
            m = re.match(
                r"((?P<name>[^ ]+)"
                r"([ ]+(?P<first_name>[^ ]+)([ ]+(?P<last_name>.*))?)?)",
                name
            )
        else:  # 'C1 C2, N'
            m = re.match(
                r"(((?P<first_name>[^ ]+)([ ]+(?P<last_name>[^,]+))?,[ ]*)?"
                r"(?P<name>.*))",
                name
            )
        trossos = m.groupdict()
        nom = trossos["name"].strip()
        cognom1 = trossos["first_name"].strip() if trossos["first_name"] else ''
        cognom2 = trossos["last_name"].strip() if trossos["last_name"] else ''

        fuzzy = ' ' in cognom2
        res = {'nom': nom, 'cognoms': [cognom1, cognom2], 'fuzzy': fuzzy}
        return res

    def ajunta_cognoms(self, cursor, uid, name_as_dict, context=None):
        conf_obj = self.pool.get('res.config')
        name_struct = conf_obj.get(
            cursor, uid, 'partner_name_format', 'C1 C2, N'
        )
        if name_struct == 'C1 C2, N':
            name_formatter = "{C1} {C2}, {N}"
        elif name_struct == 'N C1 C2':
            name_formatter = "{N} {C1} {C2}"
        else:
            raise osv.except_osv(
                _(u"Format de nom incorrecte"),
                _(u"No s'ha reconegut el format de noms. Verifica "
                  u"que la variable de configuració "
                  u"partner_name_format tingui algun dels següents "
                  u"valors:\n"
                  u"- 'C1 C2, N'\n"
                  u"- 'N C1 C2")
            )
        partner_name = name_formatter.format(**name_as_dict)
        return partner_name

    def vat_es_empresa(self, cursor, uid, vat, context=None):
        """ Retorna True si el NIF és d'empresa
        DEPRECATED, use is_enterprise_vat or has_enterprise_vat instead
        """
        warnings.warn("Method 'vat_es_empresa' is deprecated, use "
                      "'is_enterprise_vat' instead.", DeprecationWarning)
        return self.is_enterprise_vat(vat)
     
    @staticmethod
    def is_vat(vat):
        """ Returns True if vat is vat"""
        if not vat:
            return False
        vat_value = len(vat) == 9 and vat or vat[2:]
        return es.vat.is_valid(vat_value)

    @staticmethod
    def is_enterprise_vat(vat):
        """ Returns True if vat is enterprise"""
        if not vat:
            return False

        country_code = len(vat) >= 2 and vat[:2]
        if country_code == 'ES':
            return es.cif.is_valid(vat[2:])
        return False

    def has_enterprise_vat(self, cursor, uid, ids, context=None):
        """ Returns True if the partner has an enterprise vat"""
        if context is None:
            context = {}

        if isinstance(ids, (list, tuple)):
            ids = ids[0]

        vat = self.read(cursor, uid, ids, ['vat'])['vat']
        return self.is_enterprise_vat(vat)

    def vat_es_nie(self, cursor, uid, vat, context=None):
        """ Returns True if NIE (foreigner's document)
        DEPRECATED, use is_nie_vat or has_nie_vat instead
        """
        if not context:
            context = {}

        return self.is_nie_vat(vat)

    @staticmethod
    def is_nie_vat(vat):
        """ Returns True if vat is NIE (foreigner's document)"""
        if not vat:
            return False

        country_code = len(vat) >= 2 and vat[:2]
        if country_code == 'ES':
            return es.nie.is_valid(vat[2:])
        return False

    def has_nie_vat(self, cursor, uid, ids, context=None):
        """ Returns True if the partner has a NIE vat (foreigner's document)"""
        if context is None:
            context = {}

        if isinstance(ids, (list, tuple)):
            ids = ids[0]

        vat = self.read(cursor, uid, ids, ['vat'])['vat']
        return self.is_nie_vat(vat)

    @staticmethod
    def is_passport_vat(vat):
        """ Returns True if vat is passport"""
        if not vat:
            return False
        return vat[:2] == 'PS'

    def has_passport_vat(self, cursor, uid, ids, context=None):
        """ Returns True if the partner has a passport vat"""
        if context is None:
            context = {}

        if isinstance(ids, (list, tuple)):
            ids = ids[0]

        vat = self.read(cursor, uid, ids, ['vat'])['vat']
        return self.is_passport_vat(vat)

    @staticmethod
    def is_dni_vat(vat):
        """ Returns True if vat is DNI (Spanish VAT)"""
        if not vat:
            return False

        country_code = len(vat) >= 2 and vat[:2]
        if country_code == 'ES':
            return es.dni.is_valid(vat[2:])

        return False

    def has_dni_vat(self, cursor, uid, ids, context=None):
        """ Returns True if the partner has a NIE vat (foreigner's document)"""
        if context is None:
            context = {}

        if isinstance(ids, (list, tuple)):
            ids = ids[0]

        vat = self.read(cursor, uid, ids, ['vat'])['vat']
        return self.is_nie_vat(vat)

    def get_vat_type(self, cursor, uid, ids, context=None):
        """Returns correct vat type according to CNMC"""
        if context is None:
            context = {}

        if isinstance(ids, (list, tuple)):
            ids = ids[0]

        vat = self.read(cursor, uid, ids, ['vat'])['vat']

        if self.is_enterprise_vat(vat):
            return 'NI'  # NIF
        elif self.is_dni_vat(vat):
            return 'NI'  # Also NIF
        elif self.is_passport_vat(vat):
            return 'PS'  # Passport
        elif self.is_nie_vat(vat):
            return 'NE'  # NIE
        else:
            return 'OT'  # Otro

    def _ff_emails(self, cursor, uid, ids, field_name, args, context=None):
        """string with emails partner addresses
        """
        if not context:
            context = {}

        if not isinstance(ids, (tuple, list)):
            ids = [ids]

        res = {}.fromkeys(ids, '')

        paddress_obj = self.pool.get('res.partner.address')

        pa_ids = paddress_obj.search(cursor, uid, [('partner_id', 'in', ids)],
                                     context=context)
        pa_fields = ['partner_id', 'email']

        for pa_vals in paddress_obj.read(cursor, uid, pa_ids, pa_fields,
                                         context=context):
            if pa_vals['partner_id']:
                partner_id = pa_vals['partner_id'][0]
                if pa_vals['email']:
                    res[partner_id] = '{0},{1}'.format(res[partner_id],
                                                     pa_vals['email'])

        return res

    def _ff_emails_search(self, cursor, uid, obj, name, args, context=None):
        """
        Search partners by email
        """
        if not context:
            context = {}

        paddress_obj = self.pool.get('res.partner.address')

        search_dict = []
        for arg in args:
            search_dict.append(('email', arg[1], arg[2]))

        pa_ids = paddress_obj.search(cursor, uid, search_dict, context=context)

        p_ids = []

        for p_vals in paddress_obj.read(cursor, uid, pa_ids, ['partner_id'],
                                        context=context):
            if p_vals['partner_id']:
                p_ids.append(p_vals['partner_id'][0])

        return [('id', 'in', p_ids)]

    def get_partner_address(self, cursor, uid, partner_id, context=None):
        """ Returns the first active partner address. If context contains
        "active_test": False then just returns first partner address."""
        if not context:
            context = {}

        if isinstance(partner_id, list):
            partner_id = partner_id[0]

        res = self.read(cursor, uid, partner_id, ['address'], context=context)
        return len(res['address']) and res['address'][0] or None

    _columns = {
      'cifnif': fields.function(_cifnif, type='char', string="CIF/NIF", size=2,
                                method=True),
      # complementa ref: útil p.e. per R2 de comer's i distri's
      'ref2': fields.char("Ref2", size=6),
      # Emails de les partners address
      'emails': fields.function(_ff_emails, type='text', string="Emails",
                                fnct_search=_ff_emails_search, method=True),
    }

ResPartner()

class ResTipovia(osv.osv):
    """Model per guardar els diferents tipos de via."""
    _name = 'res.tipovia'

    def name_search(self, cursor, user, name, args=None, operator='ilike',
                    context=None, limit=80):
        """Sobreescrivim el mètode per buscar a través del nom."""
        if not args:
            args = []
        if not context:
            context = {}
        ids = []
        if name:
            ids = self.search(cursor, user, [('codi', 'ilike', name)] + args,
                              limit=limit)
        if not ids:
            ids = self.search(cursor, user, [('name', operator, name)] + args,
                              limit=limit)

        return self.name_get(cursor, user, ids, context=context)

    _columns = {
        'codi': fields.char('Codi', size=10, required=True, select=True,
                            readonly=True),
        'name': fields.char('Descripció', size=255, required=True,
                            readonly=True),
        'abr': fields.char('Abreviatura', size=10)
    }

    _sql_constraints = [
                    ('codi', 'unique (codi)',
                     'Ja existeix un tipus de via amb aquest codi.')
                    ]

ResTipovia()

class ResConfig(osv.osv):
    """Sistema per guardar configuracions tipus (clau,valor)."""

    _name = 'res.config'

    _bloc_code_get = multiprocessing.Lock()
    _bloc_code_set = multiprocessing.Lock()

    def get(self, cursor, uid, key, default=False):
        """Retorna el valor d'una certa clau."""
        ids = self.search(cursor, uid, [('name', '=', key)])
        if not ids:
            res = default
        else:
            res = self.read(cursor, uid, ids, ['value'])[0].get('value',
                                                                 default)
        return res

    def set(self, cursor, uid, key, value):
        """Asigna un valor per una clau."""
        self._bloc_code_set.acquire()
        vals = {
          'name': key,
          'value': value
        }
        ids = self.search(cursor, uid, [('name', '=', key)])
        if not ids:
            res = self.create(cursor, uid, vals)
        else:
            res = self.write(cursor, uid, ids, vals)
        self._bloc_code_set.release()
        return res

    _columns = {
        'name': fields.char('Key', size=256, required=True),
        'value': fields.text('Value'),
        'description': fields.text('Description')
    }

    _sql_constraints = [
                    ('name_uniq', 'unique (name)', 'The key must be unique !')]

ResConfig()



class MultiprocessBackground(osv.osv):
    """Shared Queue for OpenERP.
    """
    _name = 'multiprocess.background'

    class background(job):

        _queue_lock = multiprocessing.Lock()

        def __init__(self, *args, **kwargs):
            super(MultiprocessBackground.background, self).__init__(*args,
                                                                    **kwargs)
            # Override the default queue set by oorq.decorator.job
            if 'queue' not in kwargs or kwargs['queue'] is None:
                self.queue = None
            self.skip_check = kwargs.get('skip_check', False)

        def __call__(self, f):
            def f_job(*args, **kwargs):
                redis_conn = setup_redis_connection()
                if not get_current_job():
                    osv_object = args[0]._name
                    dbname = args[1].dbname
                    uid = args[2]
                    fname = f.__name__
                    if self.queue is None:
                        self.queue = 'background_%s' % dbname
                    # Les cues de background tenen un timeout d'un dia
                    q = Queue(self.queue, default_timeout=86400,
                              connection=redis_conn, async=self.async)
                    # Pass OpenERP server config to the worker
                    conf_attrs = dict(
                        [(attr, value)
                         for attr, value in config.options.items()]
                    )
                    try:
                        self._queue_lock.acquire()
                        pool = pooler.get_pool(dbname)
                        user = pool.get('res.users').browse(args[1], uid, uid)
                        hash = compute_hash_instance(dbname, osv_object, fname,
                                                     *args[3:])
                        found = False
                        if not self.skip_check:
                            # Primer comprovem que no el tinguem encuat però
                            # sense processar
                            hashes = dict([(j.meta['hash'], j) for j in q.jobs])
                            if hash in hashes:
                                rjob = hashes[hash]
                                found = True
                            else:
                                # Comprovem els que estiguin en marxa, és una
                                # mica hack, ja que no hi ha cap manera directa
                                # de fer-ho
                                # https://github.com/nvie/rq/issues/253
                                for j_key in redis_conn.keys('rq:job:*'):
                                    try:
                                        rjob = rq_job.Job(
                                            id=j_key.split(':')[-1]
                                        )
                                        rjob.refresh()
                                        if (rjob.origin == self.queue
                                            and rjob.get_status()
                                            in (JobStatus.QUEUED, JobStatus.STARTED)):
                                            if rjob.meta.get('hash') == hash:
                                                found = True
                                                break
                                    except:
                                        pass
                        if found:
                            enqueued = times.to_local(rjob.enqueued_at,
                                                      user.context_tz)
                            raise osv.except_osv("Error",
                                _(u"Aquesta tasca (%s) ja està en marxa per "
                                  u"l'usuari: %s a: "
                                  u"%s. Status: %s")
                                    % (rjob.id, rjob.meta['user'], enqueued,
                                       rjob.get_status())
                            )
                            return

                        job = q.enqueue(
                            execute, conf_attrs, dbname, uid, osv_object, fname,
                            *args[3:], **kwargs
                        )
                        job.meta['hash'] = hash
                        job.meta['user'] = user.name
                        job.save()
                        log('Enqueued job (id:%s): [%s] pool(%s).%s%s'
                            % (job.id, dbname, osv_object, fname, args[2:]))
                        if not f.__doc__:
                            to_translate = f.__name__
                        else:
                            to_translate = f.__doc__ \
                                .decode('utf-8') \
                                .strip(u' \n.') \
                                .split('.')[0] \
                                .split('\n')[0] \
                                .encode('utf-8')
                        proces = _(to_translate)
                        msg = _(u"Ha començat el procés \"%s\" en segon pla. En"
                                u" finalitzar rebrà una request."
                                ) % ustr(proces)
                        raise osv.except_osv(_(u"Procés en background"), msg)
                    finally:
                        self._queue_lock.release()
                else:
                    res = f(*args, **kwargs)
                    pool = pooler.get_pool(args[1].dbname)
                    func = f.__name__
                    pool.get('res.request').create(args[1], args[2], {
                        'name': _('Tasca %s finalitzada correctament') % func,
                        'body': 'Resultat:\n%s' % res,
                        'act_to': args[2]
                    })
                    return res
            return f_job

MultiprocessBackground()

class MultiprocessCall(osv.osv):
    """Multiprocess call for OpenERP.
    """
    _name = 'multiprocess.call'

    def _default_producer(self, sequence, queue):
        """Put items in the queue.
        """
        for item in sequence:
            queue.put(item)

    def _default_consumer(self, queue, dbname, uid, obj, method, context=None):
        db_obj = pooler.get_db_only(dbname)
        pool = pooler.get_pool(dbname)
        obj = pool.get(obj)
        while True:
            item = queue.get()
            if item is None:
                break
            mp_cr = db_obj.cursor()
            service_name = 'sync.%s' % id(mp_cr)
            try:
                getattr(obj, method)(mp_cr, uid, *item, context=context)
                mp_cr.commit()
                if netsvc.service_exist(service_name):
                    netsvc.SERVICES[service_name].commit()
            except Exception:
                traceback.print_exc()
                mp_cr.rollback()
                if netsvc.service_exist(service_name):
                    netsvc.SERVICES[service_name].rollback()
            finally:
                mp_cr.close()
                if netsvc.service_exist(service_name):
                    netsvc.SERVICES[service_name].close()
                    del netsvc.SERVICES[service_name]

    def process(self, cursor, uid, obj, method, items, context=None):
        """Create a MultiprocessCall.
        """
        if not context:
          context = {}
        logger = netsvc.Logger()
        queue = multiprocessing.Queue()
        args = (queue, cursor.dbname, uid, obj, method)
        kwargs = {'context': context}
        n_consumers = 0
        workers = context.get('workers', multiprocessing.cpu_count())
        consumers = {}
        # Multiprocessing
        while n_consumers < workers:
            cons_p = multiprocessing.Process(target=self._default_consumer,
                                             args=args, kwargs=kwargs)
            cons_p.daemon = context.get('daemonic', True)
            cons_p.start()
            logger.notifyChannel("objects", netsvc.LOG_INFO,
                                 u"Starting consumer for method %s PID: %s" %
                                 (method, cons_p.pid))
            consumers[cons_p.pid] = cons_p
            n_consumers += 1
        # Produce
        self._default_producer(items, queue)
        # Sentinels!
        n_consumers = 0
        while n_consumers < workers:
            queue.put(None)
            n_consumers += 1
        # Tanquem el procés
        for cons_pid, cons_p in consumers.items():
            cons_p.join()
            if not cons_p.is_alive():
                logger.notifyChannel("objects", netsvc.LOG_INFO,
                                     u"Stopping consumer for method %s PID: %s"
                                     % (method, cons_pid))
MultiprocessCall()

class ResTask(osv.osv, netsvc.Agent):
    """OpenERP background tasks.
    """
    _name = 'res.task'
    _pid = 0
    _pid_protect = threading.Semaphore()

    def add_background_call(self, cursor, uid, callback, tid=None,
                            timestamp=None, *args):
        """Afegim la crida background.
        """
        if not timestamp:
            timestamp = time.time()
        dbname = cursor.dbname
        if not tid:
            self._pid_protect.acquire()
            self._pid += 1
            pid = self._pid
            self._pid_protect.release()
            f_name = callback.__doc__.strip() or callback.__func__
            tid = self.create(cursor, uid, {'name': '%i:%s' % (pid, f_name)})
        tasks = [x[2] for x in self._Agent__tasks_by_db.get(dbname, [])]
        if callback not in tasks:
            self.setAlarm(callback, timestamp, dbname, uid, tid, *args)
        return tid

    def update_progress(self, cursor, uid, ids, step=1, progress=0):
        """Update progress bar.
        """
        for pgb in self.read(cursor, uid, ids, ['progress', 'state']):
            vals = {'state': 'running'}
            if not pgb['progress']:
                vals['start'] = time.strftime('%Y-%m-%d %H:%M:%S')
            if not progress:
                progress = pgb['progress'] + step
            vals['progress'] = progress
            if progress >= 100:
                vals['state'] = 'done'
                vals['progress'] = 100
                vals['end'] = time.strftime('%Y-%m-%d %H:%M:%S')
            self.write(cursor, uid, ids, vals)
        return True

    def button_run(self, cursor, uid, ids, context=None):
        """Run.
        """
        self.write(cursor, uid, ids, {'state': 'waiting'})
        return True

    def button_pause(self, cursor, uid, ids, context=None):
        """Pause.
        """
        self.write(cursor, uid, ids, {'state': 'paused'})
        return True

    _columns = {
        'name': fields.char('Calback name', size=256, requried=True),
        'start': fields.datetime('Start time'),
        'end': fields.datetime('End time'),
        'progress': fields.float('Progress'),
        'state': fields.selection([('draft', 'Draft'), ('waiting', 'Waiting'),
                                   ('running', 'Running'), ('paused', 'Paused'),
                                   ('done', 'Done')], 'State'),
        'active': fields.boolean('Active'),
    }

    _defaults = {
        'active': lambda *a: 1,
        'progress': lambda *a: 0.0,
        'state': lambda *a: 'draft',
    }

ResTask()
