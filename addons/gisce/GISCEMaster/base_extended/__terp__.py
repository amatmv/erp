# -*- coding: utf-8 -*-
{
    "name": "Base extension",
    "description": """Base models extensions""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Generic Modules",
    "depends":[
        "base",
        "l10n_ES_toponyms",
        "oorq"
    ],
    "init_xml": [],
    "demo_xml":[
        "res_poblacio_demo.xml"
    ],
    "update_xml":[
        "base_extended_view.xml",
        "res_ccaa_data.xml",
        "res_comarca_data.xml",
        "res_subsistemas_electricos_data.xml",
        "res_municipi_data.xml",
        "res_country_state_data.xml",
        "res_partner_data.xml",
        "res_provincia_ree_code_data.xml",
        "wizard/wizard_clean_cache_view.xml",
        "wizard/wizard_unify_partners_view.xml",
        "ir.model.access.csv",
        "security/base_extended_security.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
