# -*- coding: utf-8 -*-
"""Subsistemas Eléctricos"""

from osv import osv, fields


class ResSubsistemasElectricos(osv.osv):

    _name = 'res.subsistemas.electricos'
    _rec_name = 'description'
    _description = 'Subsistemas Eléctricos del Estado Español'

    _columns = {
        'code': fields.char('Codi', size=2, required=True),
        'num_code': fields.integer('Codi Numèric', required=True),
        'description': fields.char('Descripció', size=64, required=True),
    }

ResSubsistemasElectricos()
