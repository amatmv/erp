# coding=utf-8
import logging


def up(cursor, installed_version):
    logger = logging.getLogger('openerp.migration')
    logger.info('''
        Duplicating res.configs from giscedata_facturacio to preserve it's values.
    ''')
    cursor.execute("""
        INSERT INTO res_config (name, value) (
            SELECT 'migration_' || name, value FROM res_config WHERE name IN (
                'fact_change_date_in_acc_inv',
                'fact_change_date_out_acc_inv',
                'allow_open_future_provider_invoices',
                'always_compensate_RA_out_invoice',
                'always_compensate_RA_in_invoice'  
            )
        )
    """)
    logger.info('Migration successful.')


def down(cursor, installed_version):
    pass

migrate = up
