# coding=utf-8
import logging


def up(cursor, installed_version):
    logger = logging.getLogger('openerp.migration')
    logger.info('''
        Updating module\'s res.config and res.groups models that are defined 
        in giscedata_facturacio and are related with account_invoice.
    ''')
    cursor.execute("""
        UPDATE ir_model_data
        SET module = 'account_invoice_base'
        WHERE module = 'giscedata_facturacio'
        AND name IN (
            'conf_fact_change_date_in_acc_inv',
            'conf_fact_change_date_out_acc_inv',
            'allow_open_future_provider_invoices',
            'always_compensate_RA_out_invoice',
            'always_compensate_RA_in_invoice',
            'group_payments_manager',
            'group_invoice_manager'
        )
    """)
    logger.info('Migration successful.')


def down(cursor, installed_version):
    pass

migrate = up
