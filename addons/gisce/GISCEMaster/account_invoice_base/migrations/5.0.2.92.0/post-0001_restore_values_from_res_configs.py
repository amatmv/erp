# coding=utf-8
import logging


def up(cursor, installed_version):
    logger = logging.getLogger('openerp.migration')
    logger.info('''
        Restoring old res.configs values that have been duplicated and removing 
        the duplicates.
    ''')
    for config_var in (
            'fact_change_date_in_acc_inv',
            'fact_change_date_out_acc_inv',
            'allow_open_future_provider_invoices',
            'always_compensate_RA_out_invoice',
            'always_compensate_RA_in_invoice'
    ):
        cursor.execute("""
            UPDATE res_config SET value = (
                SELECT value FROM res_config WHERE name = %s
            )
            WHERE name LIKE %s
        """, ('migration_{}'.format(config_var), config_var))

    cursor.execute("""
        DELETE FROM res_config WHERE name IN (
            'migration_fact_change_date_in_acc_inv',
            'migration_fact_change_date_out_acc_inv',
            'migration_allow_open_future_provider_invoices',
            'migration_always_compensate_RA_out_invoice',
            'migration_always_compensate_RA_in_invoice'
        )
    """)
    logger.info('Migration successful.')


def down(cursor, installed_version):
    pass


migrate = up
