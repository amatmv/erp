# -*- coding: utf-8 -*-
from osv import osv
from tools.translate import _


class AccountInvoiceReport(osv.osv):

    _auto = False
    _name = 'account.invoice.report'

    def get_taxes(self, cursor, uid, ids, context=None):
        """
        Obtains the invoice taxes to be shown at an invoice.
        :param cursor: Openerp Cursor
        :type cursor: sql_db.Cursor
        :param uid: User ID
        :type uid: long
        :param ids: <account.invoice> ids
        :type ids: list
        :param context:
        :return: Dictionary where the keys are the invoice ids and the value is
                 a list of tax lines, set as dictionaries with the following
                 structure:
                    - name: tax name
                    - base: tax base amount
                    - amount: tax amount
        """
        if not isinstance(ids, list):
            ids = [ids]

        tax_o = self.pool.get('account.invoice.tax')
        tax_f = ['name', 'amount', 'base', 'amount', 'base_amount',
                 'tax_amount', 'invoice_id']
        dmn = [('invoice_id', 'in', ids)]
        tax_vs = tax_o.q(cursor, uid).read(tax_f).where(dmn)

        res = {}
        for tax_v in tax_vs:
            invoice_id = tax_v['invoice_id']
            tax_name = tax_v['name']
            res.setdefault(invoice_id, [])

            res[invoice_id].append({
                'name': tax_name,
                'base': tax_v['base'],
                'amount': tax_v['amount'],
            })

        return res

    def get_company_info(self, cursor, uid, ids, context=None):
        """
        Obtains the necessary data to show of company ("comercialitzadora")on a
        invoice.
        :param cursor:
        :param uid:
        :param ids: <res.company> ids
        :param context:
        :return: Dictionary where the key is the company id and the value is a
        dictionary with the following structure:
            - name: "nom de la comercialitzadora"
            - email: "correu electronic de la comercialitzadora"
            - lateral_info: "informació legal de la comercialitzadora.
                Tipicament es posa al lateral de la factura."
            - phone: "numero de telefon de la comercialitzadora"
            - logo: "imatge amb el logo de la comercializadora"
            - vat: "CIF de la comercialitzadora"
            - street: "direccio de la comercialitzadora"
            - zip: ...
            - poblacio: ...
            - state: ...
            - entity: entity to show if bar code.
        """
        if not isinstance(ids, list):
            ids = [ids]

        partner_o = self.pool.get('res.partner')
        res_partner_address_o = self.pool.get('res.partner.address')
        company_o = self.pool.get('res.company')

        address_f = ['street', 'zip', 'id_poblacio.name', 'email', 'phone',
                     'state_id.name']
        address_select = res_partner_address_o.q(cursor, uid).select(address_f)

        company_f = ['partner_id.id', 'partner_id.name', 'partner_id.vat',
                     'id', 'rml_footer2', 'logo']
        dmn = [('id', 'in', ids)]

        company_vs = company_o.q(cursor, uid).read(company_f).where(dmn)

        res = {}

        for company_v in company_vs:
            company_id = company_v['id']

            company_address_id = partner_o.address_get(
                cursor, uid, [company_v['partner_id.id']]
            )['default']

            q = address_select.where([('id', '=', company_address_id)])
            cursor.execute(*q)
            address_v = cursor.dictfetchall()[0]

            vat = company_v['partner_id.vat']
            entity = ''.join([c for c in vat if c.isdigit()])

            res[company_id] = {
                'name': company_v['partner_id.name'],
                'vat': vat,
                'logo': company_v['logo'],
                'lateral_info': company_v['rml_footer2'],
                'street': address_v['street'],
                'zip': address_v['zip'],
                'poblacio': address_v['id_poblacio.name'],
                'state': address_v['state_id.name'],
                'phone': address_v['phone'],
                'email': address_v['email'],
                'entity': entity,
            }

        return res

    def get_contact_info(self, cursor, uid, ids, context=None):
        """
        Obtains the contact information related to the invoice.
        :param cursor:
        :param uid:
        :param ids: <account.invoice> ids
        :param context:
        :return: Dictionary where the key is the invoice id and the value is a
            dictionary with the following structure:
                - name: contact name
                - street: contact street address
                - zip: contact zip address
                - city: contact city (id_municipi)
                - aclarador: ...
        """
        if not isinstance(ids, list):
            ids = [ids]

        factura_o = self.pool.get('account.invoice')

        name_f = 'address_contact_id.name'
        street_f = 'address_contact_id.street'
        zip_f = 'address_contact_id.zip'
        city_f = 'address_contact_id.id_municipi'
        aclarador_f = 'address_contact_id.aclarador'

        factura_f = [name_f, street_f, 'id', zip_f, city_f, aclarador_f]
        dmn = [('id', 'in', ids)]

        res = {}
        factura_vs = factura_o.q(cursor, uid).read(factura_f).where(dmn)

        for factura_v in factura_vs:
            factura_id = factura_v['id']

            aclarador = factura_v[aclarador_f]
            municipi = ''
            if not aclarador:
                aclarador = ""
            if factura_v[city_f]:
                mun_obj = self.pool.get('res.municipi')
                municipi = mun_obj.read(
                    cursor, uid, factura_v[city_f], ['name'])['name']

            res[factura_id] = {
                'name': factura_v[name_f],
                'street': factura_v[street_f],
                'zip': factura_v[zip_f],
                'city': municipi,
                'aclarador':  aclarador,
            }

        return res


AccountInvoiceReport()
