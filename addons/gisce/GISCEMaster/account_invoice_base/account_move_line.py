from osv import osv, fields
from datetime import datetime


class AccountMoveLine(osv.osv):
    _name = 'account.move.line'
    _inherit = 'account.move.line'

    def reconcile_lines_of_group_move(
            self, cursor, uid, group_move_id, line_ids, context=None
    ):
        if context is None:
            context = {}
        mline_obj = self.pool.get('account.move.line')
        move_obj = self.pool.get('account.move')
        ctx = context.copy()
        ctx['force_different_account'] = True
        mline_obj.reconcile_lines(cursor, uid, line_ids, context=ctx)
        # When all the group is paid we have to reconcile all the invoices
        if mline_obj.browse(cursor, uid, line_ids[-1]).reconcile_id:
            reconcile_lines_invoices = move_obj.get_lines_to_reconcile_for_group_id(
                cursor, uid, group_move_id, context=ctx
            )
            for ml_invoice, ml_related_ids in reconcile_lines_invoices.items():
                mline_obj.reconcile_lines(
                    cursor, uid, [ml_invoice] + ml_related_ids, context=ctx
                )
        return True

    def _invoice(self, cursor, user, ids, name, arg, context=None):
        invoice_obj = self.pool.get('account.invoice')
        res = {}
        for line_id in ids:
            res[line_id] = False
        if ids:
            cursor.execute("""SELECT l.id, i.id
                           FROM account_move_line l, account_invoice i
                           WHERE (l.move_id = i.move_id OR
                                  (l.move_id = i.group_move_id AND
                                   case when i.type like 'out_%%' then l.ref = replace(i.number, '/', '') else l.ref = i.origin end))
                           AND l.id in %s""",
                           (tuple(ids),))
            invoice_ids = []
            for line_id, invoice_id in cursor.fetchall():
                res[line_id] = invoice_id
                invoice_ids.append(invoice_id)
        return res

    def _invoice_search(self, cursor, user, obj, name, args, context):

        return super(AccountMoveLine,
                     self)._invoice_search(cursor, user, obj,
                                           name, args, context)

    _columns = {
        'invoice': fields.function(
            _invoice,
            method=True,
            string='Invoice',
            type='many2one',
            relation='account.invoice',
            fnct_search=_invoice_search
        ),
    }

AccountMoveLine()


class AccountMove(osv.osv):
    _name = 'account.move'
    _inherit = 'account.move'

    def is_group_move(self, cursor, uid, move_id, context=None):
        if context is None:
            context = {}
        if isinstance(move_id, (list, tuple)):
            move_id = move_id[0]

        invoice_o = self.pool.get("account.invoice")
        inv_id = invoice_o.search(cursor, uid, [
            ('group_move_id', '=', move_id),
        ])
        return inv_id

    def get_lines_to_reconcile_for_group_id(self, cursor, uid, group_move_id, context=None):
        if context is None:
            context = {}
        move_obj = self.pool.get('account.move')
        mline_obj = self.pool.get('account.move.line')
        reconcile_obj = self.pool.get('account.move.reconcile')
        move = move_obj.browse(cursor, uid, group_move_id, context=context)
        group_ref = move.ref
        reconcile_lines_invoices = {}
        for move_line in move.line_id:
            if move_line.name == group_ref:
                continue
            sp = [
                ('partner_id', '=', move_line.partner_id.id),
                ('account_id', '=', move_line.account_id.id),
                ('move_id', '!=', group_move_id),
                ('id', 'not in', reconcile_lines_invoices.keys()),
                ('ref', '=', move_line.ref)
            ]

            move_ammount = (move_line.debit or 0.0) - (move_line.credit or 0.0)
            partial_payments = 0.0
            if move_line.invoice:
                for l in move_line.invoice.payment_ids:
                    partial_payments += (l.debit or 0.0) - (l.credit or 0.0)

            # total = float_round(
            #     abs(move_ammount + partial_payments),
            #     precision_rounding=precision_rounding
            # )
            # We have found some issues with de float_round:
            #    float_round(118.71, 2) -> 118.71000000000001
            # so we use the basic round().
            total = round(abs(move_ammount + partial_payments), 2)

            if move_line.credit:
                if abs(partial_payments) < abs(move_ammount):
                    direction = -1
                else:
                    direction = 1
            else:
                if abs(partial_payments) < abs(move_ammount):
                    direction = 1
                else:
                    direction = -1

            base_sp = sp[:]
            if direction < 0:
                sp.append(('debit', '=', total))
            else:
                sp.append(('credit', '=', total))

            invoice_move_lines = mline_obj.search(cursor, uid, sp)
            # Try to find
            if not invoice_move_lines:
                sp = base_sp[:]
                direction = -1 * direction
                if direction < 0:
                    sp.append(('debit', '=', total))
                else:
                    sp.append(('credit', '=', total))
                invoice_move_lines = mline_obj.search(cursor, uid, sp)

            if invoice_move_lines:
                invoice_move_lines = mline_obj.read(
                    cursor, uid, invoice_move_lines, ['reconcile_id'])
                invoice_move_line = invoice_move_lines[0]
                for mvline in invoice_move_lines:
                    if not mvline['reconcile_id']:
                        invoice_move_line = mvline
                if invoice_move_line['reconcile_id']:
                    raise Exception(_('Already reconciled.'))
                invoice_move_line = invoice_move_line['id']
            else:
                raise Exception(_('Invoice not found!'))

            lines_to_reconcile = [move_line.id]
            partial_reconciles = mline_obj.read(cursor, uid, invoice_move_line, ['reconcile_partial_id'])
            if partial_reconciles['reconcile_partial_id']:
                rec_info = reconcile_obj.read(
                    cursor, uid, partial_reconciles['reconcile_partial_id'][0], ['line_partial_ids']
                )
                for line_partial in rec_info.get('line_partial_ids', []):
                    if line_partial not in lines_to_reconcile + [invoice_move_line]:
                        lines_to_reconcile.append(line_partial)
            reconcile_lines_invoices[invoice_move_line] = lines_to_reconcile
        return reconcile_lines_invoices

AccountMove()
