# -*- coding: utf-8 -*-
{
    "name": "Account Invoice Facturació",
    "description": """Mòdul de Account Invoice que agrupa la funcionalitat necessària per a la facturació de facturació i gas""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "base_extended",
        "account",
        "account_payment",
        "account_payment_extension",
        "c2c_webkit_report",
        "partner_address_tipovia",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "account_invoice_data.xml",
        "account_invoice_view.xml",
        "security/giscedata_facturacio_security.xml",
        "security/ir.model.access.csv",
        "account_invoice_report.xml",
    ],
    "active": False,
    "installable": True
}
