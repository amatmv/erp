# Translation of OpenERP Server.
# This file contains the translation of the following modules:
# * account_invoice_base
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.14\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2019-08-12 13:41+0000\n"
"PO-Revision-Date: 2019-08-12 13:41+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: \n"
"Generated-By: Babel 2.7.0\n"

#. module: account_invoice_base
#: constraint:ir.model:0
msgid "The Object name must start with x_ and not contain any special character !"
msgstr ""

#. module: account_invoice_base
#: code:addons/account_invoice_base/account_invoice.py:363
msgid "Error ja existeix una factura agrupada: {0}"
msgstr ""

#. module: account_invoice_base
#: code:addons/account_invoice_base/account_invoice.py:370
msgid "Error la factura no està en estat obert: {0}"
msgstr ""

#. module: account_invoice_base
#: code:addons/account_invoice_base/account_invoice.py:272
msgid "Wrong original invoice date!"
msgstr ""

#. module: account_invoice_base
#: code:addons/account_invoice_base/account_move_line.py:158
msgid "Invoice not found!"
msgstr ""

#. module: account_invoice_base
#: code:addons/account_invoice_base/account_invoice.py:954
msgid "No s'ha trobat un període comptable per la data actual."
msgstr ""

#. module: account_invoice_base
#: code:addons/account_invoice_base/account_invoice.py:1182
msgid "N57 amount {0} not match invoice amount: {1}"
msgstr ""

#. module: account_invoice_base
#: model:ir.module.module,shortdesc:account_invoice_base.module_meta_information
msgid "Account Invoice Facturació"
msgstr ""

#. module: account_invoice_base
#: model:ir.module.module,description:account_invoice_base.module_meta_information
msgid ""
"Mòdul de Account Invoice que agrupa la funcionalitat necessària per a la "
"facturació de facturació i gas"
msgstr ""

#. module: account_invoice_base
#: code:addons/account_invoice_base/account_invoice.py:1248
msgid "Sense Idioma"
msgstr ""

#. module: account_invoice_base
#: code:addons/account_invoice_base/account_invoice.py:953
#: code:addons/account_invoice_base/account_invoice.py:962
msgid "Error de Configuració"
msgstr ""

#. module: account_invoice_base
#: field:account.invoice,lang_partner:0
#: field:account.invoice,lang_partner_sel:0
msgid "Idioma Client"
msgstr ""

#. module: account_invoice_base
#: code:addons/account_invoice_base/account_invoice.py:98
#, python-format
msgid "Dipòsit de garantia %s"
msgstr ""

#. module: account_invoice_base
#: constraint:ir.ui.view:0
msgid "Invalid XML for View Architecture!"
msgstr ""

#. module: account_invoice_base
#: code:addons/account_invoice_base/account_invoice.py:963
msgid "El diari {0} no te el compte de credit configurat."
msgstr ""

#. module: account_invoice_base
#: code:addons/account_invoice_base/account_move_line.py:155
msgid "Already reconciled."
msgstr ""

#. module: account_invoice_base
#: code:addons/account_invoice_base/account_invoice.py:273
msgid ""
"The original invoice date {} is a future date. You cannot open invoices "
"with future original invoice date."
msgstr ""

#. module: account_invoice_base
#: code:addons/account_invoice_base/account_invoice.py:476
msgid "Desfer pagament: {}"
msgstr ""

#. module: account_invoice_base
#: code:addons/account_invoice_base/account_invoice.py:456
#: code:addons/account_invoice_base/account_invoice.py:539
msgid "Aquesta factura esta agrupada i no es pot desfer el pagament."
msgstr ""

#. module: account_invoice_base
#: code:addons/account_invoice_base/account_invoice.py:400
msgid "Error s'han trobat factures amb diferents IBANs:"
msgstr ""

#. module: account_invoice_base
#: view:account.invoice:0
msgid "Supplier invoice"
msgstr ""

#. module: account_invoice_base
#: field:account.invoice,group_move_id:0
msgid "Group move"
msgstr ""

#. module: account_invoice_base
#: model:account.journal,name:account_invoice_base.facturacio_journal_caja
msgid "CAJA"
msgstr ""

#. module: account_invoice_base
#: view:account.invoice:0
msgid "Invoice"
msgstr ""

#. module: account_invoice_base
#: model:ir.model,name:account_invoice_base.model_account_invoice_report
msgid "account.invoice.report"
msgstr ""

#: rml:account_invoice_base/report/account_invoice.mako:75
msgid "CIF:"
msgstr ""

#: rml:account_invoice_base/report/account_invoice.mako:76
msgid "Domicili:"
msgstr ""

#: rml:account_invoice_base/report/account_invoice.mako:92
msgid "Dades de la factura"
msgstr ""

#: rml:account_invoice_base/report/account_invoice.mako:97
msgid "Nº de factura:"
msgstr ""

#: rml:account_invoice_base/report/account_invoice.mako:105
msgid "Data:"
msgstr ""

#: rml:account_invoice_base/report/account_invoice.mako:121
msgid "Nom:"
msgstr ""

#: rml:account_invoice_base/report/account_invoice.mako:129
msgid "DNI/CIF:"
msgstr ""

#: rml:account_invoice_base/report/account_invoice.mako:143
msgid "Adreça:"
msgstr ""

#: rml:account_invoice_base/report/account_invoice.mako:189
msgid "Unitats"
msgstr ""

#: rml:account_invoice_base/report/account_invoice.mako:192
msgid "Concepte"
msgstr ""

#: rml:account_invoice_base/report/account_invoice.mako:195
msgid "Preu unitari"
msgstr ""

#: rml:account_invoice_base/report/account_invoice.mako:198
msgid "Import"
msgstr ""

#: rml:account_invoice_base/report/account_invoice.mako:238
msgid "Base"
msgstr ""

#: rml:account_invoice_base/report/account_invoice.mako:241
msgid "Impost"
msgstr ""

#: rml:account_invoice_base/report/account_invoice.mako:244
msgid "Quota"
msgstr ""

#: rml:account_invoice_base/report/account_invoice.mako:247
msgid "Total"
msgstr ""

#: rml:account_invoice_base/report/account_invoice.mako:253
msgid "IESE"
msgstr ""

#: rml:account_invoice_base/report/account_invoice.mako:272
msgid "TOTAL"
msgstr ""

