# -*- coding: utf-8 -*-

import netsvc
from destral import testing
from destral.transaction import Transaction
import datetime
from expects import expect, equal
from account_invoice_base.account_invoice import amount_grouped


class TestsInvoiceChangingPeriodsWhen(testing.OOTestCase):

    def test_open_invoice_with_forced_period_change_when_open(self):
        invoice_obj = self.openerp.pool.get('account.invoice')
        imd_obj = self.openerp.pool.get('ir.model.data')
        wf_service = netsvc.LocalService('workflow')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            invoice_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'invoice_0001'
            )[1]

            invoice_fields = invoice_obj.read(
                cursor, uid, invoice_id,
                ['date_start', 'date_stop', 'date_invoice', 'period_id']
            )

            #period_id = period_obj.search(cursor, uid, [('name', '=', '02/2016')])[0]
            period_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'period_1')[1]

            invoice_obj.write(cursor, uid, invoice_id, {'period_id': period_id})

            current_year = datetime.datetime.today().year
            date_inv = datetime.date(current_year, 8, 01)
            invoice_obj.write(cursor, uid, invoice_id, {'date_invoice': date_inv})

            wf_service.trg_validate(
                uid, 'account.invoice', invoice_id, 'invoice_open', cursor
            )

            expected_period_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'period_8')[1]

            invoice_period = invoice_obj.read(
                cursor, uid, invoice_id,
                ['period_id']
            )['period_id'][0]

            self.assertEqual(invoice_period, expected_period_id)

    def test_open_invoice_with_no_period_id_set_when_open(self):
        invoice_obj = self.openerp.pool.get('account.invoice')
        imd_obj = self.openerp.pool.get('ir.model.data')
        wf_service = netsvc.LocalService('workflow')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            invoice_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'invoice_0001'
            )[1]

            invoice_fields = invoice_obj.read(
                cursor, uid, invoice_id,
                ['date_start', 'date_stop', 'date_invoice', 'period_id']
            )

            invoice_obj.write(cursor, uid, invoice_id, {'period_id': False})

            current_year = datetime.datetime.today().year
            date_inv = datetime.date(current_year, 8, 01)
            invoice_obj.write(cursor, uid, invoice_id,
                              {'date_invoice': date_inv})

            wf_service.trg_validate(
                uid, 'account.invoice', invoice_id, 'invoice_open', cursor
            )

            expected_period_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'period_8')[1]

            invoice_period = invoice_obj.read(
                cursor, uid, invoice_id,
                ['period_id']
            )['period_id'][0]

            self.assertEqual(invoice_period, expected_period_id)

    def test_open_ra_invoice_bra_compenses_original_invoice_if_original_is_not_paid_and_not_config_var(self):
        """Opening a RA invoice, if original invoice is not paid and we don't
        have always_compensate_RA_out_invoice configured we have to compense
        refund with the rectifying.
        """
        invoice_obj = self.openerp.pool.get('account.invoice')
        inv_line_obj = self.openerp.pool.get('account.invoice.line')
        imd_obj = self.openerp.pool.get('ir.model.data')
        conf_obj = self.openerp.pool.get('res.config')

        wf_service = netsvc.LocalService('workflow')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            # Configure config variable to use the default opening RA process
            always_compensate_RA_id = imd_obj.get_object_reference(
                cursor, uid, 'account_invoice_base',
                'always_compensate_RA_out_invoice'
            )[1]
            conf_obj.write(
                cursor, uid, [always_compensate_RA_id], {'value': '0'}
            )

            invoice_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'test_invoice_original_unpaid'
            )[1]

            wf_service.trg_validate(
                uid, 'account.invoice', invoice_id, 'invoice_open', cursor
            )

            orig_invoice = invoice_obj.browse(cursor, uid, invoice_id)
            expect(orig_invoice.state).to(equal('open'))

            rectify_ids = invoice_obj.refund(
                cursor, uid, [invoice_id], rectificative_type='BRA'
            )

            rectify_ids += invoice_obj.rectify_only(
                cursor, uid, [invoice_id], rectificative_type='RA'
            )

            bra_invoice_id = rectify_ids[0]
            ra_invoice_id = rectify_ids[1]

            expect(len(rectify_ids)).to(equal(2))

            account_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'a_sale'
            )[1]

            inv_line_obj.create(
                cursor, uid, {
                    'name': 'New line',
                    'price_unit': 10.0,
                    'quantity': 1.0,
                    'invoice_id': ra_invoice_id,
                    'account_id': account_id,
                }
            )

            wf_service.trg_validate(
                uid, 'account.invoice', ra_invoice_id, 'invoice_open', cursor
            )

            rectify = invoice_obj.browse(cursor, uid, ra_invoice_id)

            expect(rectify.rectificative_type).to(equal('RA'))
            expect(rectify.state).to(equal('open'))
            expect(rectify.residual).to(equal(rectify.rectifying_id.amount_total + 10))
            expect(rectify.saldo).to(equal(10))

            bra_invoice = invoice_obj.browse(cursor, uid, bra_invoice_id)

            orig_invoice = invoice_obj.browse(cursor, uid, invoice_id)
            expect(orig_invoice.state).to(equal('paid'))

            expect(bra_invoice.rectificative_type).to(equal('BRA'))
            expect(bra_invoice.state).to(equal('paid'))

            journal_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'sales_journal'
            )[1]

            rectify.pay_and_reconcile(
                1210, account_id, rectify.period_id.id, journal_id,
                None, None, None
            )

            rectify = invoice_obj.browse(cursor, uid, ra_invoice_id)

            expect(rectify.state).to(equal('paid'))
            expect(rectify.residual).to(equal(0))
            expect(rectify.saldo).to(equal(10))

    def test_open_ra_invoice_bra_compenses_rectifying_invoice_if_original_is_not_paid_and_config_var(self):
        """Opening a RA invoice, if original invoice is not paid and we have
        always_compensate_RA_out_invoice configured we have to
        compense refund with the RA invoice.
        """
        invoice_obj = self.openerp.pool.get('account.invoice')
        inv_line_obj = self.openerp.pool.get('account.invoice.line')
        imd_obj = self.openerp.pool.get('ir.model.data')
        conf_obj = self.openerp.pool.get('res.config')

        wf_service = netsvc.LocalService('workflow')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            # Configure config variable to compensate RA with BRA while
            # opening RA independently if the original is unpaid
            always_compensate_RA_id = imd_obj.get_object_reference(
                cursor, uid, 'account_invoice_base',
                'always_compensate_RA_out_invoice'
            )[1]
            conf_obj.write(
                cursor, uid, [always_compensate_RA_id], {'value': '1'}
            )

            invoice_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'test_invoice_1'
            )[1]

            rectify_ids = invoice_obj.refund(
                cursor, uid, [invoice_id], rectificative_type='BRA'
            )

            rectify_ids += invoice_obj.rectify_only(
                cursor, uid, [invoice_id], rectificative_type='RA'
            )

            bra_invoice_id = rectify_ids[0]
            ra_invoice_id = rectify_ids[1]

            expect(len(rectify_ids)).to(equal(2))

            account_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'a_sale'
            )[1]

            inv_line_obj.create(
                cursor, uid, {
                    'name': 'New line',
                    'price_unit': 10.0,
                    'quantity': 1.0,
                    'invoice_id': ra_invoice_id,
                    'account_id': account_id,
                }
            )

            wf_service.trg_validate(
                uid, 'account.invoice', ra_invoice_id, 'invoice_open', cursor
            )

            rectify = invoice_obj.browse(cursor, uid, ra_invoice_id)

            expect(rectify.rectificative_type).to(equal('RA'))
            expect(rectify.state).to(equal('open'))
            expect(rectify.residual).to(equal(10))
            expect(rectify.saldo).to(equal(10))

            bra_invoice = invoice_obj.browse(cursor, uid, bra_invoice_id)

            expect(bra_invoice.rectificative_type).to(equal('BRA'))
            expect(bra_invoice.residual).to(equal(0))
            expect(bra_invoice.state).to(equal('paid'))

            journal_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'sales_journal'
            )[1]

            rectify.pay_and_reconcile(
                10, account_id, rectify.period_id.id, journal_id,
                None, None, None
            )

            rectify = invoice_obj.browse(cursor, uid, ra_invoice_id)

            expect(rectify.state).to(equal('paid'))
            expect(rectify.residual).to(equal(0))
            expect(rectify.saldo).to(equal(10))

    def test_get_model_ids_returns_correct_ids(self):
        invoice_obj = self.openerp.pool.get('account.invoice')
        imd_obj = self.openerp.pool.get('ir.model.data')
        module_obj = self.openerp.pool.get('ir.module.module')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            imd_ids = imd_obj.search(
                cursor, uid, [('name', 'like', 'invoice_0%')]
            )
            inv_ids = [
                res['res_id'] for res in
                imd_obj.read(cursor, uid, imd_ids, ['res_id'])
            ]
            model_dict = invoice_obj.get_factura_ids_from_invoice_id(cursor, uid, inv_ids)

            gas_fact_module_state = module_obj.read(
                cursor, uid, module_obj.search(
                    cursor, uid, [('name', '=', 'giscegas_facturacio')]
                ), ['state']
            )[0]['state']
            if gas_fact_module_state != 'installed':
                self.assertNotIn('giscegas.facturacio.factura', model_dict.keys())
            self.assertIn('account.invoice', model_dict.keys())
            self.assertEquals(
                invoice_obj, model_dict['account.invoice'][1]
            )

            inv_res_ids = model_dict['account.invoice'][0]
            self.assertFalse(list(set(inv_res_ids) - set(inv_ids)))


class TestUnpayInvoice(testing.OOTestCaseWithCursor):

    def test_unpay_invoice_removing_movements(self):
        invoice_obj = self.openerp.pool.get('account.invoice')
        journal_obj = self.openerp.pool.get('account.journal')
        imd_obj = self.openerp.pool.get('ir.model.data')

        cursor = self.cursor
        uid = self.uid

        invoice_id = imd_obj.get_object_reference(
            cursor, uid, 'account', 'test_invoice_1'
        )[1]

        bank_journal_id = imd_obj.get_object_reference(
            cursor, uid, 'account', 'bank_journal'
        )[1]


        invoice = invoice_obj.browse(cursor, uid, invoice_id)

        expect(invoice.state).to(equal('paid'))
        expect(len(invoice.payment_ids)).to(equal(1))
        journal_obj.write(cursor, uid, [bank_journal_id], {
            'update_posted': True
        })

        invoice.undo_payment_remove()
        invoice = invoice_obj.browse(cursor, uid, invoice_id)

        expect(invoice.state).to(equal('open'))
        expect(invoice.residual).to(equal(invoice.amount_total))
        expect(len(invoice.payment_ids)).to(equal(0))

    def test_unpay_invoice_create_inversed_account_moves(self):
        invoice_obj = self.openerp.pool.get('account.invoice')
        journal_obj = self.openerp.pool.get('account.journal')
        imd_obj = self.openerp.pool.get('ir.model.data')

        cursor = self.cursor
        uid = self.uid

        invoice_id = imd_obj.get_object_reference(
            cursor, uid, 'account', 'test_invoice_1'
        )[1]
        bank_journal_id = imd_obj.get_object_reference(
            cursor, uid, 'account', 'bank_journal'
        )[1]

        journal_obj.write(cursor, uid, [bank_journal_id], {
            'update_posted': True
        })

        invoice = invoice_obj.browse(cursor, uid, invoice_id)

        expect(invoice.state).to(equal('paid'))
        expect(len(invoice.payment_ids)).to(equal(1))

        invoice.undo_payment_with_moves()
        invoice = invoice_obj.browse(cursor, uid, invoice_id)

        expect(invoice.state).to(equal('open'))
        expect(invoice.residual).to(equal(invoice.amount_total))
        expect(len(invoice.payment_ids)).to(equal(2))

    def test_undo_payment_mode_1_calls_removes(self):
        from mock import patch
        inv_obj = self.openerp.pool.get('account.invoice')
        cfg_obj = self.openerp.pool.get('res.config')

        cursor = self.cursor
        uid = self.uid

        cfg_obj.set(cursor, uid, 'undo_payment_mode', '1')

        with patch.object(inv_obj, 'undo_payment_remove', return_value=1):
            res = inv_obj.undo_payment(cursor, uid, [])
            expect(res).to(equal(1))

    def test_undo_payment_mode_2_calls_moves(self):
        from mock import patch
        inv_obj = self.openerp.pool.get('account.invoice')
        cfg_obj = self.openerp.pool.get('res.config')

        cursor = self.cursor
        uid = self.uid

        cfg_obj.set(cursor, uid, 'undo_payment_mode', '2')

        with patch.object(inv_obj, 'undo_payment_with_moves', return_value=2):
            res = inv_obj.undo_payment(cursor, uid, [])
            expect(res).to(equal(2))


class TestPayInvoice(testing.OOTestCase):

    def group_invoices(self, cursor, uid, invoice_ids, context=None):
        invoice_o = self.openerp.pool.get("account.invoice")
        invoice_o.make_move_differences(cursor, uid, invoice_ids, context=context)
        invoice = invoice_o.browse(cursor, uid, invoice_ids[0])
        return invoice.group_move_id.id, invoice.group_move_id.ref

    def check_move_line_for_invoice_line(self, cursor, uid, invoice_line, move):
        mline_o = self.openerp.pool.get("account.move.line")
        types = {'out_invoice': -1, 'in_invoice': 1, 'out_refund': 1, 'in_refund': -1}
        direction = types[invoice_line.invoice_id.type]
        # let pay negative invoices
        if invoice_line.price_subtotal < 0:
            direction *= -1
        params = [
            ('account_id', '=', invoice_line.account_id.id),
            ('move_id', '=', move.id),
            ('debit', '=', direction > 0 and abs(invoice_line.price_subtotal) or 0.0),
            ('credit', '=', direction < 0 and abs(invoice_line.price_subtotal) or 0.0),
            ('reconcile_id', '=', False),
            ('reconcile_partial_id', '=', False),
        ]
        mids = mline_o.search(cursor, uid, params)
        self.assertTrue(len(mids) > 0)

    def check_get_invoice_to_be_paid(self, cursor, uid, invoice_id=None, invoice_ref=None, context=None):
        """
        Get an invoice that is opened without any partial/full payment made. Contability is check to be correct.
        """
        if context is None:
            context = {}
        invoice_obj = self.openerp.pool.get('account.invoice')
        imd_obj = self.openerp.pool.get('ir.model.data')
        if not invoice_id:
            if not invoice_ref:
                invoice_ref = 'test_invoice_original_unpaid'
            invoice_id = imd_obj.get_object_reference(
                cursor, uid, 'account', invoice_ref
            )[1]
        invoice_obj.write(cursor, uid, invoice_id, context.get("extra_vals", {}))
        wf_service = netsvc.LocalService("workflow")
        wf_service.trg_validate(uid, 'account.invoice', invoice_id, 'invoice_open', cursor)
        # Check everything is correct
        invoice = invoice_obj.browse(cursor, uid, invoice_id)
        self.assertEqual(invoice.state, 'open')
        self.assertEqual(invoice.residual, invoice.amount_total)
        self.assertTrue(invoice.move_id)
        move = invoice.move_id
        for invoice_line in invoice.invoice_line:
            self.check_move_line_for_invoice_line(cursor, uid, invoice_line, move)
        self.assertEqual(len([l.id for l in move.line_id]), len([l.id for l in invoice.invoice_line]) + 1)
        self.assertEqual(len([l.id for l in move.line_id if l.account_id == invoice.account_id]), 1)
        return invoice

    def make_payment(self, cursor, uid, ammount_to_pay, invoice, journal_id=None, period_id=None, context=None):
        imd_obj = self.openerp.pool.get('ir.model.data')
        if not journal_id:
            journal_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'bank_journal'
            )[1]

        acc_id = self.openerp.pool.get("account.journal").read(
            cursor, uid, journal_id, ['default_credit_account_id']
        )['default_credit_account_id'][0]

        if not period_id:
            period_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'period_1'
            )[1]

        invoice.pay_and_reconcile(
            ammount_to_pay, acc_id, period_id, journal_id,
            False, period_id, False, context=context
        )
        invoice = invoice.browse()[0]
        return invoice

    def check_invoice_correctly_full_paid(self, cursor, uid, invoice, context=None):
        # Check invoice
        self.assertEqual(invoice.state, 'paid')
        self.assertEqual(invoice.residual, 0)
        # Check invoice move_lines
        move = invoice.move_id
        mline_reconciled = [ml for ml in move.line_id if ml.reconcile_partial_id]
        self.assertEqual(len(mline_reconciled), 0)
        mline_reconciled = [ml for ml in move.line_id if ml.reconcile_id]
        self.assertEqual(len(mline_reconciled), 1)
        mline_reconciled = mline_reconciled[0]
        self.assertEqual(mline_reconciled.account_id.id, invoice.account_id.id)
        # Check payments move_lines
        for mline in mline_reconciled.reconcile_id.line_id:
            self.check_move_lines_coherence(cursor, uid, mline, [], context=context)

    def check_move_lines_coherence(self, cursor, uid, move_line, checked_lines, context=None):
        checked_lines.append(move_line)
        acc_id = move_line.account_id.id
        if move_line.reconcile_id:
            for mline in move_line.reconcile_id.line_id:
                if mline in checked_lines:
                    continue
                self.assertEqual(mline.account_id.id, acc_id)
                checked_lines.extend(self.check_move_lines_coherence(cursor, uid, mline, checked_lines, context=context))
        if move_line.reconcile_partial_id:
            for mline in move_line.reconcile_partial_id.line_partial_ids:
                if mline in checked_lines:
                    continue
                self.assertEqual(mline.account_id.id, acc_id)
                checked_lines.extend(self.check_move_lines_coherence(cursor, uid, mline, checked_lines, context=context))
        return checked_lines

    def test_pay_invoice(self):
        """
        Test de pagar una factura de client positiva.
        (out_invoice, residual > 0)
        """
        imd_obj = self.openerp.pool.get('ir.model.data')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            invoice = self.check_get_invoice_to_be_paid(cursor, uid)
            invoice = self.make_payment(cursor, uid, invoice.amount_total, invoice)

            # Check invoice is full paid
            self.check_invoice_correctly_full_paid(cursor, uid, invoice)
            move = invoice.move_id

            # Hi ha un mline amb debit el total de la factura a la conta de
            # la factura
            inv_mline = [l for l in move.line_id if l.debit][0]
            self.assertEqual(inv_mline.account_id.name, u'Main Receivable')
            self.assertEqual(inv_mline.account_id.id, invoice.account_id.id)
            self.assertEqual(inv_mline.debit, invoice.amount_total)

            # Aquest mline esta conciliat totalment amb una altre mline a la
            # mateixa conta
            reconciled_line = [l for l in inv_mline.reconcile_id.line_id if l != inv_mline]
            self.assertEqual(len(reconciled_line), 1)
            reconciled_line = reconciled_line[0]
            self.assertEqual(reconciled_line.credit, inv_mline.debit)
            self.assertEqual(reconciled_line.account_id.id, inv_mline.account_id.id)

            # L'ultim mline es el de pagament cap a la conta del diari
            # utilitzat per pagar
            payment_line = [l for l in reconciled_line.move_id.line_id if l != reconciled_line]
            self.assertEqual(len(payment_line), 1)
            payment_line = payment_line[0]
            account_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'cash'
            )[1]
            self.assertEqual(payment_line.debit, reconciled_line.credit)
            self.assertEqual(payment_line.account_id.name, u'Petty Cash')
            self.assertEqual(payment_line.account_id.id, account_id)

    def test_pay_negative_invoice(self):
        """
        Test de pagar una factura de client positiva.
        (out_invoice, residual < 0)
        """
        imd_obj = self.openerp.pool.get('ir.model.data')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            invoice = self.check_get_invoice_to_be_paid(cursor, uid, invoice_ref='test_invoice_original_unpaid_negative')
            invoice = self.make_payment(cursor, uid, invoice.amount_total, invoice)

            # Check invoice is full paid
            self.check_invoice_correctly_full_paid(cursor, uid, invoice)
            move = invoice.move_id

            # Hi ha un mline amb credit el total de la factura a la conta de
            # la factura
            inv_mline = [l for l in move.line_id if l.credit][0]
            self.assertEqual(inv_mline.account_id.name, u'Main Receivable')
            self.assertEqual(inv_mline.account_id.id, invoice.account_id.id)
            self.assertEqual(inv_mline.credit, abs(invoice.amount_total))

            # Aquest mline esta conciliat totalment amb una altre mline a la
            # mateixa conta
            reconciled_line = [l for l in inv_mline.reconcile_id.line_id if l != inv_mline]
            self.assertEqual(len(reconciled_line), 1)
            reconciled_line = reconciled_line[0]
            self.assertEqual(reconciled_line.debit, inv_mline.credit)
            self.assertEqual(reconciled_line.account_id.id, inv_mline.account_id.id)

            # L'ultim mline es el de pagament cap a la conta del diari
            # utilitzat per pagar
            payment_line = [l for l in reconciled_line.move_id.line_id if l != reconciled_line]
            self.assertEqual(len(payment_line), 1)
            payment_line = payment_line[0]
            account_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'cash'
            )[1]
            self.assertEqual(payment_line.credit, reconciled_line.debit)
            self.assertEqual(payment_line.account_id.name, u'Petty Cash')
            self.assertEqual(payment_line.account_id.id, account_id)

    def test_pay_invoice_partial(self):
        """
        Test de pagar parcialment una factura de client positiva.
        (out_invoice, residual > 0)
        """
        imd_obj = self.openerp.pool.get('ir.model.data')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            invoice = self.check_get_invoice_to_be_paid(cursor, uid)
            invoice = self.make_payment(cursor, uid, invoice.residual/2.0, invoice)

            # Check invoice reconciled mlines coherence
            for mline in invoice.move_id.line_id:
                self.check_move_lines_coherence(cursor, uid, mline, [])

            # Check partial payment is made
            self.assertEqual(invoice.residual, invoice.amount_total/2.0)
            move = invoice.move_id

            # Hi ha un mline amb debit el total de la factura a la conta de
            # la factura
            inv_mline = [l for l in move.line_id if l.debit][0]
            self.assertEqual(inv_mline.account_id.name, u'Main Receivable')
            self.assertEqual(inv_mline.account_id.id, invoice.account_id.id)
            self.assertEqual(inv_mline.debit, invoice.amount_total)

            # Aquest mline esta conciliat parcialment amb una altre mline a la
            # mateixa conta
            reconciled_line = [l for l in inv_mline.reconcile_partial_id.line_partial_ids if l != inv_mline]
            self.assertEqual(len(reconciled_line), 1)
            reconciled_line = reconciled_line[0]
            self.assertEqual(reconciled_line.credit, invoice.residual)
            self.assertEqual(reconciled_line.account_id.id, inv_mline.account_id.id)

            # L'ultim mline es el de pagament cap a la conta del diari
            # utilitzat per pagar
            payment_line = [l for l in reconciled_line.move_id.line_id if l != reconciled_line]
            self.assertEqual(len(payment_line), 1)
            payment_line = payment_line[0]
            account_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'cash'
            )[1]
            self.assertEqual(payment_line.debit, reconciled_line.credit)
            self.assertEqual(payment_line.account_id.name, u'Petty Cash')
            self.assertEqual(payment_line.account_id.id, account_id)

            # Make another payment that completes the total
            invoice = self.make_payment(cursor, uid, invoice.amount_total / 2.0, invoice)
            # Check invoice is full paid
            self.check_invoice_correctly_full_paid(cursor, uid, invoice)
            move = invoice.move_id

            # Hi ha un mline amb debit el total de la factura a la conta de
            # la factura
            inv_mline = [l for l in move.line_id if l.debit][0]
            # Aquest mline esta conciliat totalment amb 2 mlines a la
            # mateixa conta
            reconciled_lines = [l for l in inv_mline.reconcile_id.line_id if l != inv_mline]
            self.assertEqual(len(reconciled_lines), 2)
            for reconciled_line in reconciled_lines:
                self.assertEqual(reconciled_line.credit, inv_mline.debit / 2.0)
                self.assertEqual(reconciled_line.account_id.id, inv_mline.account_id.id)
                # L'ultim mline es el de pagament cap a la conta del diari
                # utilitzat per pagar
                payment_line = [l for l in reconciled_line.move_id.line_id if l != reconciled_line]
                self.assertEqual(len(payment_line), 1)
                payment_line = payment_line[0]
                account_id = imd_obj.get_object_reference(
                    cursor, uid, 'account', 'cash'
                )[1]
                self.assertEqual(payment_line.debit, reconciled_line.credit)
                self.assertEqual(payment_line.account_id.name, u'Petty Cash')
                self.assertEqual(payment_line.account_id.id, account_id)

    def test_pay_negative_invoice_partial(self):
        """
        Test de pagar parcialment una factura de client positiva.
        (out_invoice, residual < 0)
        """
        imd_obj = self.openerp.pool.get('ir.model.data')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            invoice = self.check_get_invoice_to_be_paid(cursor, uid, invoice_ref='test_invoice_original_unpaid_negative')
            invoice = self.make_payment(cursor, uid, invoice.residual/2.0, invoice)

            # Check invoice reconciled mlines coherence
            for mline in invoice.move_id.line_id:
                self.check_move_lines_coherence(cursor, uid, mline, [])

            # Check partial payment is made
            self.assertEqual(invoice.residual, invoice.amount_total/2.0)
            move = invoice.move_id

            # Hi ha un mline amb credit el total de la factura a la conta de
            # la factura
            inv_mline = [l for l in move.line_id if l.credit][0]
            self.assertEqual(inv_mline.account_id.name, u'Main Receivable')
            self.assertEqual(inv_mline.account_id.id, invoice.account_id.id)
            self.assertEqual(inv_mline.credit, abs(invoice.amount_total))

            # Aquest mline esta conciliat parcialment amb una altre mline a la
            # mateixa conta
            reconciled_line = [l for l in inv_mline.reconcile_partial_id.line_partial_ids if l != inv_mline]
            self.assertEqual(len(reconciled_line), 1)
            reconciled_line = reconciled_line[0]
            self.assertEqual(reconciled_line.debit, abs(invoice.residual))
            self.assertEqual(reconciled_line.account_id.id, inv_mline.account_id.id)

            # L'ultim mline es el de pagament cap a la conta del diari
            # utilitzat per pagar
            payment_line = [l for l in reconciled_line.move_id.line_id if l != reconciled_line]
            self.assertEqual(len(payment_line), 1)
            payment_line = payment_line[0]
            account_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'cash'
            )[1]
            self.assertEqual(payment_line.credit, reconciled_line.debit)
            self.assertEqual(payment_line.account_id.name, u'Petty Cash')
            self.assertEqual(payment_line.account_id.id, account_id)

            # Make another payment that completes the total
            invoice = self.make_payment(cursor, uid, invoice.amount_total / 2.0, invoice)
            # Check invoice is full paid
            self.check_invoice_correctly_full_paid(cursor, uid, invoice)
            move = invoice.move_id

            # Hi ha un mline amb credit el total de la factura a la conta de
            # la factura
            inv_mline = [l for l in move.line_id if l.credit][0]
            # Aquest mline esta conciliat totalment amb 2 mlines a la
            # mateixa conta
            reconciled_lines = [l for l in inv_mline.reconcile_id.line_id if l != inv_mline]
            self.assertEqual(len(reconciled_lines), 2)
            for reconciled_line in reconciled_lines:
                self.assertEqual(reconciled_line.debit, inv_mline.credit / 2.0)
                self.assertEqual(reconciled_line.account_id.id, inv_mline.account_id.id)
                # L'ultim mline es el de pagament cap a la conta del diari
                # utilitzat per pagar
                payment_line = [l for l in reconciled_line.move_id.line_id if l != reconciled_line]
                self.assertEqual(len(payment_line), 1)
                payment_line = payment_line[0]
                account_id = imd_obj.get_object_reference(
                    cursor, uid, 'account', 'cash'
                )[1]
                self.assertEqual(payment_line.credit, reconciled_line.debit)
                self.assertEqual(payment_line.account_id.name, u'Petty Cash')
                self.assertEqual(payment_line.account_id.id, account_id)

    def test_pay_refund_invoice(self):
        """
        Test de pagar una factura de client positiva.
        (out_refund, residual > 0)
        """
        imd_obj = self.openerp.pool.get('ir.model.data')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            invoice = self.check_get_invoice_to_be_paid(cursor, uid, context={"extra_vals": {'type': 'out_refund'}})
            invoice = self.make_payment(cursor, uid, invoice.amount_total, invoice)

            # Check invoice is full paid
            self.check_invoice_correctly_full_paid(cursor, uid, invoice)
            move = invoice.move_id

            # Hi ha un mline amb credit el total de la factura a la conta de
            # la factura
            inv_mline = [l for l in move.line_id if l.credit][0]
            self.assertEqual(inv_mline.account_id.name, u'Main Receivable')
            self.assertEqual(inv_mline.account_id.id, invoice.account_id.id)
            self.assertEqual(inv_mline.credit, invoice.amount_total)

            # Aquest mline esta conciliat totalment amb una altre mline a la
            # mateixa conta
            reconciled_line = [l for l in inv_mline.reconcile_id.line_id if l != inv_mline]
            self.assertEqual(len(reconciled_line), 1)
            reconciled_line = reconciled_line[0]
            self.assertEqual(reconciled_line.debit, inv_mline.credit)
            self.assertEqual(reconciled_line.account_id.id, inv_mline.account_id.id)

            # L'ultim mline es el de pagament cap a la conta del diari
            # utilitzat per pagar
            payment_line = [l for l in reconciled_line.move_id.line_id if l != reconciled_line]
            self.assertEqual(len(payment_line), 1)
            payment_line = payment_line[0]
            account_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'cash'
            )[1]
            self.assertEqual(payment_line.credit, reconciled_line.debit)
            self.assertEqual(payment_line.account_id.name, u'Petty Cash')
            self.assertEqual(payment_line.account_id.id, account_id)

    def test_pay_refund_negative_invoice(self):
        """
        Test de pagar una factura de client positiva.
        (out_refund, residual < 0)
        """
        imd_obj = self.openerp.pool.get('ir.model.data')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            invoice = self.check_get_invoice_to_be_paid(
                cursor, uid, invoice_ref='test_invoice_original_unpaid_negative',
                context={"extra_vals": {'type': 'out_refund'}}
            )
            invoice = self.make_payment(cursor, uid, invoice.amount_total, invoice)

            # Check invoice is full paid
            self.check_invoice_correctly_full_paid(cursor, uid, invoice)
            move = invoice.move_id

            # Hi ha un mline amb debit el total de la factura a la conta de
            # la factura
            inv_mline = [l for l in move.line_id if l.debit][0]
            self.assertEqual(inv_mline.account_id.name, u'Main Receivable')
            self.assertEqual(inv_mline.account_id.id, invoice.account_id.id)
            self.assertEqual(inv_mline.debit, abs(invoice.amount_total))

            # Aquest mline esta conciliat totalment amb una altre mline a la
            # mateixa conta
            reconciled_line = [l for l in inv_mline.reconcile_id.line_id if l != inv_mline]
            self.assertEqual(len(reconciled_line), 1)
            reconciled_line = reconciled_line[0]
            self.assertEqual(reconciled_line.credit, inv_mline.debit)
            self.assertEqual(reconciled_line.account_id.id, inv_mline.account_id.id)

            # L'ultim mline es el de pagament cap a la conta del diari
            # utilitzat per pagar
            payment_line = [l for l in reconciled_line.move_id.line_id if l != reconciled_line]
            self.assertEqual(len(payment_line), 1)
            payment_line = payment_line[0]
            account_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'cash'
            )[1]
            self.assertEqual(payment_line.debit, reconciled_line.credit)
            self.assertEqual(payment_line.account_id.name, u'Petty Cash')
            self.assertEqual(payment_line.account_id.id, account_id)

    def test_pay_refund_invoice_partial(self):
        """
        Test de pagar parcialment una factura de client positiva.
        (out_refund, residual > 0)
        """
        imd_obj = self.openerp.pool.get('ir.model.data')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            invoice = self.check_get_invoice_to_be_paid(cursor, uid, context={"extra_vals": {'type': 'out_refund'}})
            invoice = self.make_payment(cursor, uid, invoice.residual/2.0, invoice)

            # Check invoice reconciled mlines coherence
            for mline in invoice.move_id.line_id:
                self.check_move_lines_coherence(cursor, uid, mline, [])

            # Check partial payment is made
            self.assertEqual(invoice.residual, invoice.amount_total/2.0)
            move = invoice.move_id

            # Hi ha un mline amb debit el total de la factura a la conta de
            # la factura
            inv_mline = [l for l in move.line_id if l.credit][0]
            self.assertEqual(inv_mline.account_id.name, u'Main Receivable')
            self.assertEqual(inv_mline.account_id.id, invoice.account_id.id)
            self.assertEqual(inv_mline.credit, invoice.amount_total)

            # Aquest mline esta conciliat parcialment amb una altre mline a la
            # mateixa conta
            reconciled_line = [l for l in inv_mline.reconcile_partial_id.line_partial_ids if l != inv_mline]
            self.assertEqual(len(reconciled_line), 1)
            reconciled_line = reconciled_line[0]
            self.assertEqual(reconciled_line.debit, invoice.residual)
            self.assertEqual(reconciled_line.account_id.id, inv_mline.account_id.id)

            # L'ultim mline es el de pagament cap a la conta del diari
            # utilitzat per pagar
            payment_line = [l for l in reconciled_line.move_id.line_id if l != reconciled_line]
            self.assertEqual(len(payment_line), 1)
            payment_line = payment_line[0]
            account_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'cash'
            )[1]
            self.assertEqual(payment_line.credit, reconciled_line.debit)
            self.assertEqual(payment_line.account_id.name, u'Petty Cash')
            self.assertEqual(payment_line.account_id.id, account_id)

            # Make another payment that completes the total
            invoice = self.make_payment(cursor, uid, invoice.amount_total / 2.0, invoice)
            # Check invoice is full paid
            self.check_invoice_correctly_full_paid(cursor, uid, invoice)
            move = invoice.move_id

            # Hi ha un mline amb debit el total de la factura a la conta de
            # la factura
            inv_mline = [l for l in move.line_id if l.credit][0]
            # Aquest mline esta conciliat totalment amb 2 mlines a la
            # mateixa conta
            reconciled_lines = [l for l in inv_mline.reconcile_id.line_id if l != inv_mline]
            self.assertEqual(len(reconciled_lines), 2)
            for reconciled_line in reconciled_lines:
                self.assertEqual(reconciled_line.debit, inv_mline.credit / 2.0)
                self.assertEqual(reconciled_line.account_id.id, inv_mline.account_id.id)
                # L'ultim mline es el de pagament cap a la conta del diari
                # utilitzat per pagar
                payment_line = [l for l in reconciled_line.move_id.line_id if l != reconciled_line]
                self.assertEqual(len(payment_line), 1)
                payment_line = payment_line[0]
                account_id = imd_obj.get_object_reference(
                    cursor, uid, 'account', 'cash'
                )[1]
                self.assertEqual(payment_line.credit, reconciled_line.debit)
                self.assertEqual(payment_line.account_id.name, u'Petty Cash')
                self.assertEqual(payment_line.account_id.id, account_id)

    def test_pay_refund_negative_invoice_partial(self):
        """
        Test de pagar parcialment una factura de client positiva.
        (out_refund, residual > 0)
        """
        imd_obj = self.openerp.pool.get('ir.model.data')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            invoice = self.check_get_invoice_to_be_paid(
                cursor, uid,
                invoice_ref='test_invoice_original_unpaid_negative',
                context={"extra_vals": {'type': 'out_refund'}}
            )
            invoice = self.make_payment(cursor, uid, invoice.residual/2.0, invoice)

            # Check invoice reconciled mlines coherence
            for mline in invoice.move_id.line_id:
                self.check_move_lines_coherence(cursor, uid, mline, [])

            # Check partial payment is made
            self.assertEqual(invoice.residual, invoice.amount_total/2.0)
            move = invoice.move_id

            # Hi ha un mline amb credit el total de la factura a la conta de
            # la factura
            inv_mline = [l for l in move.line_id if l.debit][0]
            self.assertEqual(inv_mline.account_id.name, u'Main Receivable')
            self.assertEqual(inv_mline.account_id.id, invoice.account_id.id)
            self.assertEqual(inv_mline.debit, abs(invoice.amount_total))

            # Aquest mline esta conciliat parcialment amb una altre mline a la
            # mateixa conta
            reconciled_line = [l for l in inv_mline.reconcile_partial_id.line_partial_ids if l != inv_mline]
            self.assertEqual(len(reconciled_line), 1)
            reconciled_line = reconciled_line[0]
            self.assertEqual(reconciled_line.credit, abs(invoice.residual))
            self.assertEqual(reconciled_line.account_id.id, inv_mline.account_id.id)

            # L'ultim mline es el de pagament cap a la conta del diari
            # utilitzat per pagar
            payment_line = [l for l in reconciled_line.move_id.line_id if l != reconciled_line]
            self.assertEqual(len(payment_line), 1)
            payment_line = payment_line[0]
            account_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'cash'
            )[1]
            self.assertEqual(payment_line.debit, reconciled_line.credit)
            self.assertEqual(payment_line.account_id.name, u'Petty Cash')
            self.assertEqual(payment_line.account_id.id, account_id)

            # Make another payment that completes the total
            invoice = self.make_payment(cursor, uid, invoice.amount_total / 2.0, invoice)
            # Check invoice is full paid
            self.check_invoice_correctly_full_paid(cursor, uid, invoice)
            move = invoice.move_id

            # Hi ha un mline amb credit el total de la factura a la conta de
            # la factura
            inv_mline = [l for l in move.line_id if l.debit][0]
            # Aquest mline esta conciliat totalment amb 2 mlines a la
            # mateixa conta
            reconciled_lines = [l for l in inv_mline.reconcile_id.line_id if l != inv_mline]
            self.assertEqual(len(reconciled_lines), 2)
            for reconciled_line in reconciled_lines:
                self.assertEqual(reconciled_line.credit, inv_mline.debit / 2.0)
                self.assertEqual(reconciled_line.account_id.id, inv_mline.account_id.id)
                # L'ultim mline es el de pagament cap a la conta del diari
                # utilitzat per pagar
                payment_line = [l for l in reconciled_line.move_id.line_id if l != reconciled_line]
                self.assertEqual(len(payment_line), 1)
                payment_line = payment_line[0]
                account_id = imd_obj.get_object_reference(
                    cursor, uid, 'account', 'cash'
                )[1]
                self.assertEqual(payment_line.debit, reconciled_line.credit)
                self.assertEqual(payment_line.account_id.name, u'Petty Cash')
                self.assertEqual(payment_line.account_id.id, account_id)

    def test_pay_positive_grouped_invoice_positive_positive(self):
        """
        Test de pagar una factura agrupacio positiva de 2 factures:
        (out_invoice, residual > 0)
        (out_invoice, residual > 0)
        """
        imd_obj = self.openerp.pool.get('ir.model.data')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            invoice = self.check_get_invoice_to_be_paid(cursor, uid)
            invoice2 = self.check_get_invoice_to_be_paid(cursor, uid, invoice_ref='test_invoice_original_unpaid_2')
            move_id, move_ref = self.group_invoices(cursor, uid, [invoice.id, invoice2.id])
            # Refresh invoice browse
            invoice = invoice.browse()[0]
            amount_to_pay = amount_grouped(invoice)
            amount_expected = invoice.amount_total + invoice2.amount_total
            self.assertEqual(amount_to_pay, amount_expected)
            invoice1 = self.make_payment(
                cursor, uid, amount_to_pay, invoice
            )
            invoice2 = invoice2.browse()[0]

            reconciled_lines_total = 0.0
            for invoice in [invoice1, invoice2]:
                # Check invoice is full paid
                self.check_invoice_correctly_full_paid(cursor, uid, invoice)
                move = invoice.move_id

                # Hi ha un mline amb debit el total de la factura a la conta de
                # la factura
                inv_mline = [l for l in move.line_id if l.debit][0]
                self.assertEqual(inv_mline.account_id.name, u'Main Receivable')
                self.assertEqual(inv_mline.account_id.id, invoice.account_id.id)
                self.assertEqual(inv_mline.debit, invoice.amount_total)

                # Aquest mline esta conciliat totalment amb una altre mline a la
                # mateixa conta
                reconciled_line = [l for l in inv_mline.reconcile_id.line_id if l != inv_mline]
                self.assertEqual(len(reconciled_line), 1)
                reconciled_line = reconciled_line[0]
                self.assertEqual(reconciled_line.credit, inv_mline.debit)
                self.assertEqual(reconciled_line.account_id.id, inv_mline.account_id.id)
                self.assertEqual(reconciled_line.name, u'{0} - {1}'.format(move_ref, invoice.number.replace("/", "")))
                reconciled_lines_total += reconciled_line.credit

                # L'ultim mline es el moviment agrupador. Te el mateix account_id que les factures
                # agrupades
                group_line = [l for l in reconciled_line.move_id.line_id if l != reconciled_line and l.ref == move_ref]
                self.assertEqual(len(group_line), 1)
                group_line = group_line[0]
                self.assertEqual(group_line.account_id.name, u'Main Receivable')
                self.assertEqual(group_line.account_id.id, reconciled_line.account_id.id)

            self.assertEqual(group_line.debit, reconciled_lines_total)
            # Finalment aquest mline agrupador esta conciliat amb el mline del
            # moviment que fa el pagament cap al diari utilitzat per pagar.
            account_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'cash'
            )[1]
            reconcile_group_line = [l for l in group_line.reconcile_id.line_id if l != group_line]
            self.assertEqual(len(reconcile_group_line), 1)
            reconcile_group_line = reconcile_group_line[0]
            self.assertEqual(reconcile_group_line.account_id.name, u'Main Receivable')
            self.assertEqual(reconcile_group_line.account_id.id, reconciled_line.account_id.id)
            self.assertEqual(reconcile_group_line.credit, group_line.debit)
            payment_line = [l for l in reconcile_group_line.move_id.line_id if l != reconcile_group_line]
            self.assertEqual(len(payment_line), 1)
            payment_line = payment_line[0]
            self.assertEqual(payment_line.account_id.name, u'Petty Cash')
            self.assertEqual(payment_line.account_id.id, account_id)
            self.assertEqual(payment_line.debit, reconcile_group_line.credit)

    def test_pay_positive_grouped_invoice_positive_negative(self):
        """
        Test de pagar una factura agrupacio positiva de 2 factures:
        (out_invoice, residual > 0)
        (out_invoice, residual < 0)
        """
        imd_obj = self.openerp.pool.get('ir.model.data')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            invoice = self.check_get_invoice_to_be_paid(cursor, uid)
            invoice2 = self.check_get_invoice_to_be_paid(cursor, uid, invoice_ref='test_invoice_original_unpaid_2_negative')
            move_id, move_ref = self.group_invoices(cursor, uid, [invoice.id, invoice2.id])
            # Refresh invoice browse
            invoice = invoice.browse()[0]
            amount_to_pay = amount_grouped(invoice)
            amount_expected = invoice.amount_total + invoice2.amount_total
            self.assertEqual(amount_to_pay, amount_expected)
            invoice1 = self.make_payment(
                cursor, uid, amount_to_pay, invoice
            )
            invoice2 = invoice2.browse()[0]

            reconciled_lines_total = 0.0
            
            # CHECK INVOICE 1 (positive)
            invoice = invoice1
            # Check invoice is full paid
            self.check_invoice_correctly_full_paid(cursor, uid, invoice)
            move = invoice.move_id

            # Hi ha un mline amb debit el total de la factura a la conta de
            # la factura
            inv_mline = [l for l in move.line_id if l.debit][0]
            self.assertEqual(inv_mline.account_id.name, u'Main Receivable')
            self.assertEqual(inv_mline.account_id.id, invoice.account_id.id)
            self.assertEqual(inv_mline.debit, invoice.amount_total)

            # Aquest mline esta conciliat totalment amb una altre mline a la
            # mateixa conta
            reconciled_line = [l for l in inv_mline.reconcile_id.line_id if l != inv_mline]
            self.assertEqual(len(reconciled_line), 1)
            reconciled_line = reconciled_line[0]
            self.assertEqual(reconciled_line.credit, inv_mline.debit)
            self.assertEqual(reconciled_line.account_id.id, inv_mline.account_id.id)
            self.assertEqual(reconciled_line.name, u'{0} - {1}'.format(move_ref, invoice.number.replace("/", "")))
            reconciled_lines_total += reconciled_line.credit

            # L'ultim mline es el moviment agrupador. Te el mateix account_id que les factures
            # agrupades
            group_line = [l for l in reconciled_line.move_id.line_id if l != reconciled_line and l.ref == move_ref]
            self.assertEqual(len(group_line), 1)
            group_line = group_line[0]
            self.assertEqual(group_line.account_id.name, u'Main Receivable')
            self.assertEqual(group_line.account_id.id, reconciled_line.account_id.id)
            
            # CHECK INVOICE 2 (negative)
            invoice = invoice2
            # Check invoice is full paid
            self.check_invoice_correctly_full_paid(cursor, uid, invoice)
            move = invoice.move_id

            # Hi ha un mline amb credit el total de la factura a la conta de
            # la factura
            inv_mline = [l for l in move.line_id if l.credit][0]
            self.assertEqual(inv_mline.account_id.name, u'Main Receivable')
            self.assertEqual(inv_mline.account_id.id, invoice.account_id.id)
            self.assertEqual(inv_mline.credit, abs(invoice.amount_total))

            # Aquest mline esta conciliat totalment amb una altre mline a la
            # mateixa conta
            reconciled_line = [l for l in inv_mline.reconcile_id.line_id if l != inv_mline]
            self.assertEqual(len(reconciled_line), 1)
            reconciled_line = reconciled_line[0]
            self.assertEqual(reconciled_line.debit, inv_mline.credit)
            self.assertEqual(reconciled_line.account_id.id, inv_mline.account_id.id)
            self.assertEqual(reconciled_line.name, u'{0} - {1}'.format(move_ref, invoice.number.replace("/", "")))
            reconciled_lines_total -= reconciled_line.debit

            # L'ultim mline es el moviment agrupador. Te el mateix account_id que les factures
            # agrupades
            group_line = [l for l in reconciled_line.move_id.line_id if l != reconciled_line and l.ref == move_ref]
            self.assertEqual(len(group_line), 1)
            group_line = group_line[0]
            self.assertEqual(group_line.account_id.name, u'Main Receivable')
            self.assertEqual(group_line.account_id.id, reconciled_line.account_id.id)

            # Finalment aquest mline agrupador esta conciliat amb el mline del
            # moviment que fa el pagament cap al diari utilitzat per pagar.
            self.assertEqual(group_line.debit, reconciled_lines_total)
            self.assertEqual(1000, reconciled_lines_total)
            account_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'cash'
            )[1]
            reconcile_group_line = [l for l in group_line.reconcile_id.line_id if l != group_line]
            self.assertEqual(len(reconcile_group_line), 1)
            reconcile_group_line = reconcile_group_line[0]
            self.assertEqual(reconcile_group_line.account_id.name, u'Main Receivable')
            self.assertEqual(reconcile_group_line.account_id.id, reconciled_line.account_id.id)
            self.assertEqual(reconcile_group_line.credit, group_line.debit)
            payment_line = [l for l in reconcile_group_line.move_id.line_id if l != reconcile_group_line]
            self.assertEqual(len(payment_line), 1)
            payment_line = payment_line[0]
            self.assertEqual(payment_line.account_id.name, u'Petty Cash')
            self.assertEqual(payment_line.account_id.id, account_id)
            self.assertEqual(payment_line.debit, reconcile_group_line.credit)

    def test_pay_negative_grouped_invoice_negative_negative(self):
        """
        Test de pagar una factura agrupacio negativa de 2 factures:
        (out_invoice, residual < 0)
        (out_invoice, residual < 0)
        """
        imd_obj = self.openerp.pool.get('ir.model.data')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            invoice = self.check_get_invoice_to_be_paid(cursor, uid, invoice_ref='test_invoice_original_unpaid_negative')
            invoice2 = self.check_get_invoice_to_be_paid(cursor, uid, invoice_ref='test_invoice_original_unpaid_2_negative')
            move_id, move_ref = self.group_invoices(cursor, uid, [invoice.id, invoice2.id])
            # Refresh invoice browse
            invoice = invoice.browse()[0]
            amount_to_pay = amount_grouped(invoice)
            amount_expected = invoice.amount_total + invoice2.amount_total
            self.assertEqual(amount_to_pay, amount_expected)
            invoice1 = self.make_payment(
                cursor, uid, amount_to_pay, invoice
            )
            invoice2 = invoice2.browse()[0]

            reconciled_lines_total = 0.0
            for invoice in [invoice1, invoice2]:
                # Check invoice is full paid
                self.check_invoice_correctly_full_paid(cursor, uid, invoice)
                move = invoice.move_id

                # Hi ha un mline amb credit el total de la factura a la conta de
                # la factura
                inv_mline = [l for l in move.line_id if l.credit][0]
                self.assertEqual(inv_mline.account_id.name, u'Main Receivable')
                self.assertEqual(inv_mline.account_id.id, invoice.account_id.id)
                self.assertEqual(inv_mline.credit, abs(invoice.amount_total))

                # Aquest mline esta conciliat totalment amb una altre mline a la
                # mateixa conta
                reconciled_line = [l for l in inv_mline.reconcile_id.line_id if l != inv_mline]
                self.assertEqual(len(reconciled_line), 1)
                reconciled_line = reconciled_line[0]
                self.assertEqual(reconciled_line.debit, inv_mline.credit)
                self.assertEqual(reconciled_line.account_id.id, inv_mline.account_id.id)
                self.assertEqual(reconciled_line.name, u'{0} - {1}'.format(move_ref, invoice.number.replace("/", "")))
                reconciled_lines_total -= reconciled_line.debit

                # L'ultim mline es el moviment agrupador. Te el mateix account_id que les factures
                # agrupades
                group_line = [l for l in reconciled_line.move_id.line_id if l != reconciled_line and l.ref == move_ref]
                self.assertEqual(len(group_line), 1)
                group_line = group_line[0]
                self.assertEqual(group_line.account_id.name, u'Main Receivable')
                self.assertEqual(group_line.account_id.id, reconciled_line.account_id.id)

            self.assertEqual(group_line.credit, abs(reconciled_lines_total))
            self.assertEqual(-1400, reconciled_lines_total)
            # Finalment aquest mline agrupador esta conciliat amb el mline del
            # moviment que fa el pagament cap al diari utilitzat per pagar.
            account_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'cash'
            )[1]
            reconcile_group_line = [l for l in group_line.reconcile_id.line_id if l != group_line]
            self.assertEqual(len(reconcile_group_line), 1)
            reconcile_group_line = reconcile_group_line[0]
            self.assertEqual(reconcile_group_line.account_id.name, u'Main Receivable')
            self.assertEqual(reconcile_group_line.account_id.id, reconciled_line.account_id.id)
            self.assertEqual(reconcile_group_line.debit, group_line.credit)
            payment_line = [l for l in reconcile_group_line.move_id.line_id if l != reconcile_group_line]
            self.assertEqual(len(payment_line), 1)
            payment_line = payment_line[0]
            self.assertEqual(payment_line.account_id.name, u'Petty Cash')
            self.assertEqual(payment_line.account_id.id, account_id)
            self.assertEqual(payment_line.credit, reconcile_group_line.debit)

    def test_pay_negative_grouped_invoice_positive_negative(self):
        """
        Test de pagar una factura agrupacio negativa de 2 factures:
        (out_invoice, residual < 0)
        (out_invoice, residual < 0)
        """
        imd_obj = self.openerp.pool.get('ir.model.data')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            invoice = self.check_get_invoice_to_be_paid(cursor, uid, invoice_ref='test_invoice_original_unpaid_negative')
            invoice2 = self.check_get_invoice_to_be_paid(cursor, uid, invoice_ref='test_invoice_original_unpaid_2')
            move_id, move_ref = self.group_invoices(cursor, uid, [invoice.id, invoice2.id])
            # Refresh invoice browse
            invoice = invoice.browse()[0]
            amount_to_pay = amount_grouped(invoice)
            amount_expected = invoice.amount_total + invoice2.amount_total
            self.assertEqual(amount_to_pay, amount_expected)
            invoice1 = self.make_payment(
                cursor, uid, amount_to_pay, invoice
            )
            invoice2 = invoice2.browse()[0]

            reconciled_lines_total = 0.0
            # CHECK INVOICE 1 (negative)
            invoice = invoice1
            # Check invoice is full paid
            self.check_invoice_correctly_full_paid(cursor, uid, invoice)
            move = invoice.move_id

            # Hi ha un mline amb credit el total de la factura a la conta de
            # la factura
            inv_mline = [l for l in move.line_id if l.credit][0]
            self.assertEqual(inv_mline.account_id.name, u'Main Receivable')
            self.assertEqual(inv_mline.account_id.id, invoice.account_id.id)
            self.assertEqual(inv_mline.credit, abs(invoice.amount_total))

            # Aquest mline esta conciliat totalment amb una altre mline a la
            # mateixa conta
            reconciled_line = [l for l in inv_mline.reconcile_id.line_id if l != inv_mline]
            self.assertEqual(len(reconciled_line), 1)
            reconciled_line = reconciled_line[0]
            self.assertEqual(reconciled_line.debit, inv_mline.credit)
            self.assertEqual(reconciled_line.account_id.id, inv_mline.account_id.id)
            self.assertEqual(reconciled_line.name, u'{0} - {1}'.format(move_ref, invoice.number.replace("/", "")))
            reconciled_lines_total -= reconciled_line.debit

            # L'ultim mline es el moviment agrupador. Te el mateix account_id que les factures
            # agrupades
            group_line = [l for l in reconciled_line.move_id.line_id if l != reconciled_line and l.ref == move_ref]
            self.assertEqual(len(group_line), 1)
            group_line = group_line[0]
            self.assertEqual(group_line.account_id.name, u'Main Receivable')
            self.assertEqual(group_line.account_id.id, reconciled_line.account_id.id)
            # CHECK INVOICE 2 (positive)
            invoice = invoice2
            # Check invoice is full paid
            self.check_invoice_correctly_full_paid(cursor, uid, invoice)
            move = invoice.move_id

            # Hi ha un mline amb credit el total de la factura a la conta de
            # la factura
            inv_mline = [l for l in move.line_id if l.debit][0]
            self.assertEqual(inv_mline.account_id.name, u'Main Receivable')
            self.assertEqual(inv_mline.account_id.id, invoice.account_id.id)
            self.assertEqual(inv_mline.debit, abs(invoice.amount_total))

            # Aquest mline esta conciliat totalment amb una altre mline a la
            # mateixa conta
            reconciled_line = [l for l in inv_mline.reconcile_id.line_id if l != inv_mline]
            self.assertEqual(len(reconciled_line), 1)
            reconciled_line = reconciled_line[0]
            self.assertEqual(reconciled_line.credit, inv_mline.debit)
            self.assertEqual(reconciled_line.account_id.id, inv_mline.account_id.id)
            self.assertEqual(reconciled_line.name, u'{0} - {1}'.format(move_ref, invoice.number.replace("/", "")))
            reconciled_lines_total += reconciled_line.credit

            # L'ultim mline es el moviment agrupador. Te el mateix account_id que les factures
            # agrupades
            group_line = [l for l in reconciled_line.move_id.line_id if l != reconciled_line and l.ref == move_ref]
            self.assertEqual(len(group_line), 1)
            group_line = group_line[0]
            self.assertEqual(group_line.account_id.name, u'Main Receivable')
            self.assertEqual(group_line.account_id.id, reconciled_line.account_id.id)

            # Finalment aquest mline agrupador esta conciliat amb el mline del
            # moviment que fa el pagament cap al diari utilitzat per pagar.
            self.assertEqual(group_line.credit, abs(reconciled_lines_total))
            self.assertEqual(-1000, reconciled_lines_total)
            account_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'cash'
            )[1]
            reconcile_group_line = [l for l in group_line.reconcile_id.line_id if l != group_line]
            self.assertEqual(len(reconcile_group_line), 1)
            reconcile_group_line = reconcile_group_line[0]
            self.assertEqual(reconcile_group_line.account_id.name, u'Main Receivable')
            self.assertEqual(reconcile_group_line.account_id.id, reconciled_line.account_id.id)
            self.assertEqual(reconcile_group_line.debit, group_line.credit)
            payment_line = [l for l in reconcile_group_line.move_id.line_id if l != reconcile_group_line]
            self.assertEqual(len(payment_line), 1)
            payment_line = payment_line[0]
            self.assertEqual(payment_line.account_id.name, u'Petty Cash')
            self.assertEqual(payment_line.account_id.id, account_id)
            self.assertEqual(payment_line.credit, reconcile_group_line.debit)

    def test_pay_positive_grouped_invoice_positive_refund(self):
        """
        Test de pagar una factura agrupacio positiva de 2 factures:
        (out_invoice, residual > 0)
        (out_refund, residual > 0)
        """
        imd_obj = self.openerp.pool.get('ir.model.data')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            invoice = self.check_get_invoice_to_be_paid(cursor, uid)
            invoice2 = self.check_get_invoice_to_be_paid(cursor, uid, invoice_ref='test_invoice_original_unpaid_2', context={'extra_vals': {'type': 'out_refund'}})
            move_id, move_ref = self.group_invoices(cursor, uid, [invoice.id, invoice2.id])
            # Refresh invoice browse
            invoice = invoice.browse()[0]
            amount_to_pay = amount_grouped(invoice)
            amount_expected = invoice.amount_total - invoice2.amount_total
            self.assertEqual(amount_to_pay, amount_expected)
            invoice1 = self.make_payment(
                cursor, uid, amount_to_pay, invoice
            )
            invoice2 = invoice2.browse()[0]

            reconciled_lines_total = 0.0

            # CHECK INVOICE 1 (positive)
            invoice = invoice1
            # Check invoice is full paid
            self.check_invoice_correctly_full_paid(cursor, uid, invoice)
            move = invoice.move_id

            # Hi ha un mline amb debit el total de la factura a la conta de
            # la factura
            inv_mline = [l for l in move.line_id if l.debit][0]
            self.assertEqual(inv_mline.account_id.name, u'Main Receivable')
            self.assertEqual(inv_mline.account_id.id, invoice.account_id.id)
            self.assertEqual(inv_mline.debit, invoice.amount_total)

            # Aquest mline esta conciliat totalment amb una altre mline a la
            # mateixa conta
            reconciled_line = [l for l in inv_mline.reconcile_id.line_id if l != inv_mline]
            self.assertEqual(len(reconciled_line), 1)
            reconciled_line = reconciled_line[0]
            self.assertEqual(reconciled_line.credit, inv_mline.debit)
            self.assertEqual(reconciled_line.account_id.id, inv_mline.account_id.id)
            self.assertEqual(reconciled_line.name, u'{0} - {1}'.format(move_ref, invoice.number.replace("/", "")))
            reconciled_lines_total += reconciled_line.credit

            # L'ultim mline es el moviment agrupador. Te el mateix account_id que les factures
            # agrupades
            group_line = [l for l in reconciled_line.move_id.line_id if l != reconciled_line and l.ref == move_ref]
            self.assertEqual(len(group_line), 1)
            group_line = group_line[0]
            self.assertEqual(group_line.account_id.name, u'Main Receivable')
            self.assertEqual(group_line.account_id.id, reconciled_line.account_id.id)

            # CHECK INVOICE 2 (negative)
            invoice = invoice2
            # Check invoice is full paid
            self.check_invoice_correctly_full_paid(cursor, uid, invoice)
            move = invoice.move_id

            # Hi ha un mline amb credit el total de la factura a la conta de
            # la factura
            inv_mline = [l for l in move.line_id if l.credit][0]
            self.assertEqual(inv_mline.account_id.name, u'Main Receivable')
            self.assertEqual(inv_mline.account_id.id, invoice.account_id.id)
            self.assertEqual(inv_mline.credit, abs(invoice.amount_total))

            # Aquest mline esta conciliat totalment amb una altre mline a la
            # mateixa conta
            reconciled_line = [l for l in inv_mline.reconcile_id.line_id if l != inv_mline]
            self.assertEqual(len(reconciled_line), 1)
            reconciled_line = reconciled_line[0]
            self.assertEqual(reconciled_line.debit, inv_mline.credit)
            self.assertEqual(reconciled_line.account_id.id, inv_mline.account_id.id)
            self.assertEqual(reconciled_line.name, u'{0} - {1}'.format(move_ref, invoice.number.replace("/", "")))
            reconciled_lines_total -= reconciled_line.debit

            # L'ultim mline es el moviment agrupador. Te el mateix account_id que les factures
            # agrupades
            group_line = [l for l in reconciled_line.move_id.line_id if l != reconciled_line and l.ref == move_ref]
            self.assertEqual(len(group_line), 1)
            group_line = group_line[0]
            self.assertEqual(group_line.account_id.name, u'Main Receivable')
            self.assertEqual(group_line.account_id.id, reconciled_line.account_id.id)

            # Finalment aquest mline agrupador esta conciliat amb el mline del
            # moviment que fa el pagament cap al diari utilitzat per pagar.
            self.assertEqual(group_line.debit, reconciled_lines_total)
            self.assertEqual(1000, reconciled_lines_total)
            account_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'cash'
            )[1]
            reconcile_group_line = [l for l in group_line.reconcile_id.line_id if l != group_line]
            self.assertEqual(len(reconcile_group_line), 1)
            reconcile_group_line = reconcile_group_line[0]
            self.assertEqual(reconcile_group_line.account_id.name, u'Main Receivable')
            self.assertEqual(reconcile_group_line.account_id.id, reconciled_line.account_id.id)
            self.assertEqual(reconcile_group_line.credit, group_line.debit)
            payment_line = [l for l in reconcile_group_line.move_id.line_id if l != reconcile_group_line]
            self.assertEqual(len(payment_line), 1)
            payment_line = payment_line[0]
            self.assertEqual(payment_line.account_id.name, u'Petty Cash')
            self.assertEqual(payment_line.account_id.id, account_id)
            self.assertEqual(payment_line.debit, reconcile_group_line.credit)

    def test_pay_negative_grouped_invoice_refund_refund(self):
        """
        Test de pagar una factura agrupacio negativa de 2 factures:
        (out_refund, residual > 0)
        (out_refund, residual > 0)
        """
        imd_obj = self.openerp.pool.get('ir.model.data')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            invoice = self.check_get_invoice_to_be_paid(cursor, uid, invoice_ref='test_invoice_original_unpaid', context={'extra_vals': {'type': 'out_refund'}})
            invoice2 = self.check_get_invoice_to_be_paid(cursor, uid, invoice_ref='test_invoice_original_unpaid_2', context={'extra_vals': {'type': 'out_refund'}})
            move_id, move_ref = self.group_invoices(cursor, uid, [invoice.id, invoice2.id])
            # Refresh invoice browse
            invoice = invoice.browse()[0]
            amount_to_pay = amount_grouped(invoice)
            amount_expected = -(invoice.amount_total + invoice2.amount_total)
            self.assertEqual(amount_to_pay, amount_expected)
            invoice1 = self.make_payment(
                cursor, uid, amount_to_pay, invoice
            )
            invoice2 = invoice2.browse()[0]

            reconciled_lines_total = 0.0
            for invoice in [invoice1, invoice2]:
                # Check invoice is full paid
                self.check_invoice_correctly_full_paid(cursor, uid, invoice)
                move = invoice.move_id

                # Hi ha un mline amb credit el total de la factura a la conta de
                # la factura
                inv_mline = [l for l in move.line_id if l.credit][0]
                self.assertEqual(inv_mline.account_id.name, u'Main Receivable')
                self.assertEqual(inv_mline.account_id.id, invoice.account_id.id)
                self.assertEqual(inv_mline.credit, abs(invoice.amount_total))

                # Aquest mline esta conciliat totalment amb una altre mline a la
                # mateixa conta
                reconciled_line = [l for l in inv_mline.reconcile_id.line_id if l != inv_mline]
                self.assertEqual(len(reconciled_line), 1)
                reconciled_line = reconciled_line[0]
                self.assertEqual(reconciled_line.debit, inv_mline.credit)
                self.assertEqual(reconciled_line.account_id.id, inv_mline.account_id.id)
                self.assertEqual(reconciled_line.name, u'{0} - {1}'.format(move_ref, invoice.number.replace("/", "")))
                reconciled_lines_total -= reconciled_line.debit

                # L'ultim mline es el moviment agrupador. Te el mateix account_id que les factures
                # agrupades
                group_line = [l for l in reconciled_line.move_id.line_id if l != reconciled_line and l.ref == move_ref]
                self.assertEqual(len(group_line), 1)
                group_line = group_line[0]
                self.assertEqual(group_line.account_id.name, u'Main Receivable')
                self.assertEqual(group_line.account_id.id, reconciled_line.account_id.id)

            self.assertEqual(group_line.credit, abs(reconciled_lines_total))
            self.assertEqual(-1400, reconciled_lines_total)
            # Finalment aquest mline agrupador esta conciliat amb el mline del
            # moviment que fa el pagament cap al diari utilitzat per pagar.
            account_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'cash'
            )[1]
            reconcile_group_line = [l for l in group_line.reconcile_id.line_id if l != group_line]
            self.assertEqual(len(reconcile_group_line), 1)
            reconcile_group_line = reconcile_group_line[0]
            self.assertEqual(reconcile_group_line.account_id.name, u'Main Receivable')
            self.assertEqual(reconcile_group_line.account_id.id, reconciled_line.account_id.id)
            self.assertEqual(reconcile_group_line.debit, group_line.credit)
            payment_line = [l for l in reconcile_group_line.move_id.line_id if l != reconcile_group_line]
            self.assertEqual(len(payment_line), 1)
            payment_line = payment_line[0]
            self.assertEqual(payment_line.account_id.name, u'Petty Cash')
            self.assertEqual(payment_line.account_id.id, account_id)
            self.assertEqual(payment_line.credit, reconcile_group_line.debit)

    def test_pay_negative_grouped_invoice_positive_refund(self):
        """
        Test de pagar una factura agrupacio negativa de 2 factures:
        (out_invoice, residual > 0)
        (out_refund, residual > 0)
        """
        imd_obj = self.openerp.pool.get('ir.model.data')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            invoice = self.check_get_invoice_to_be_paid(cursor, uid, invoice_ref='test_invoice_original_unpaid', context={'extra_vals': {'type': 'out_refund'}})
            invoice2 = self.check_get_invoice_to_be_paid(cursor, uid, invoice_ref='test_invoice_original_unpaid_2')
            move_id, move_ref = self.group_invoices(cursor, uid, [invoice.id, invoice2.id])
            # Refresh invoice browse
            invoice = invoice.browse()[0]
            amount_to_pay = amount_grouped(invoice)
            amount_expected = -(invoice.amount_total - invoice2.amount_total)
            self.assertEqual(amount_to_pay, amount_expected)
            invoice1 = self.make_payment(
                cursor, uid, amount_to_pay, invoice
            )
            invoice2 = invoice2.browse()[0]

            reconciled_lines_total = 0.0
            # CHECK INVOICE 1 (negative)
            invoice = invoice1
            # Check invoice is full paid
            self.check_invoice_correctly_full_paid(cursor, uid, invoice)
            move = invoice.move_id

            # Hi ha un mline amb credit el total de la factura a la conta de
            # la factura
            inv_mline = [l for l in move.line_id if l.credit][0]
            self.assertEqual(inv_mline.account_id.name, u'Main Receivable')
            self.assertEqual(inv_mline.account_id.id, invoice.account_id.id)
            self.assertEqual(inv_mline.credit, abs(invoice.amount_total))

            # Aquest mline esta conciliat totalment amb una altre mline a la
            # mateixa conta
            reconciled_line = [l for l in inv_mline.reconcile_id.line_id if l != inv_mline]
            self.assertEqual(len(reconciled_line), 1)
            reconciled_line = reconciled_line[0]
            self.assertEqual(reconciled_line.debit, inv_mline.credit)
            self.assertEqual(reconciled_line.account_id.id, inv_mline.account_id.id)
            self.assertEqual(reconciled_line.name, u'{0} - {1}'.format(move_ref, invoice.number.replace("/", "")))
            reconciled_lines_total -= reconciled_line.debit

            # L'ultim mline es el moviment agrupador. Te el mateix account_id que les factures
            # agrupades
            group_line = [l for l in reconciled_line.move_id.line_id if l != reconciled_line and l.ref == move_ref]
            self.assertEqual(len(group_line), 1)
            group_line = group_line[0]
            self.assertEqual(group_line.account_id.name, u'Main Receivable')
            self.assertEqual(group_line.account_id.id, reconciled_line.account_id.id)
            # CHECK INVOICE 2 (positive)
            invoice = invoice2
            # Check invoice is full paid
            self.check_invoice_correctly_full_paid(cursor, uid, invoice)
            move = invoice.move_id

            # Hi ha un mline amb credit el total de la factura a la conta de
            # la factura
            inv_mline = [l for l in move.line_id if l.debit][0]
            self.assertEqual(inv_mline.account_id.name, u'Main Receivable')
            self.assertEqual(inv_mline.account_id.id, invoice.account_id.id)
            self.assertEqual(inv_mline.debit, abs(invoice.amount_total))

            # Aquest mline esta conciliat totalment amb una altre mline a la
            # mateixa conta
            reconciled_line = [l for l in inv_mline.reconcile_id.line_id if l != inv_mline]
            self.assertEqual(len(reconciled_line), 1)
            reconciled_line = reconciled_line[0]
            self.assertEqual(reconciled_line.credit, inv_mline.debit)
            self.assertEqual(reconciled_line.account_id.id, inv_mline.account_id.id)
            self.assertEqual(reconciled_line.name, u'{0} - {1}'.format(move_ref, invoice.number.replace("/", "")))
            reconciled_lines_total += reconciled_line.credit

            # L'ultim mline es el moviment agrupador. Te el mateix account_id que les factures
            # agrupades
            group_line = [l for l in reconciled_line.move_id.line_id if l != reconciled_line and l.ref == move_ref]
            self.assertEqual(len(group_line), 1)
            group_line = group_line[0]
            self.assertEqual(group_line.account_id.name, u'Main Receivable')
            self.assertEqual(group_line.account_id.id, reconciled_line.account_id.id)

            # Finalment aquest mline agrupador esta conciliat amb el mline del
            # moviment que fa el pagament cap al diari utilitzat per pagar.
            self.assertEqual(group_line.credit, abs(reconciled_lines_total))
            self.assertEqual(-1000, reconciled_lines_total)
            account_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'cash'
            )[1]
            reconcile_group_line = [l for l in group_line.reconcile_id.line_id if l != group_line]
            self.assertEqual(len(reconcile_group_line), 1)
            reconcile_group_line = reconcile_group_line[0]
            self.assertEqual(reconcile_group_line.account_id.name, u'Main Receivable')
            self.assertEqual(reconcile_group_line.account_id.id, reconciled_line.account_id.id)
            self.assertEqual(reconcile_group_line.debit, group_line.credit)
            payment_line = [l for l in reconcile_group_line.move_id.line_id if l != reconcile_group_line]
            self.assertEqual(len(payment_line), 1)
            payment_line = payment_line[0]
            self.assertEqual(payment_line.account_id.name, u'Petty Cash')
            self.assertEqual(payment_line.account_id.id, account_id)
            self.assertEqual(payment_line.credit, reconcile_group_line.debit)