## -*- coding: utf-8 -*-
<%
    from operator import attrgetter, itemgetter
    from datetime import datetime, timedelta
    from babel.numbers import format_currency
    from account_invoice_base.report.utils import localize_period
    import json, re

    report_o = pool.get('account.invoice.report')
    empresa = report_o.get_company_info(cursor, uid, company.id)[company.id]
%>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
        <style>
            @font-face {
                font-family: "Roboto-Regular";
                src: url("${assets_path}/fonts/Roboto/Roboto-Regular.ttf") format('truetype');
                font-weight: normal;
            }
            @font-face {
                font-family: "Lato-Regular";
                src: url("${assets_path}/fonts/Lato/Lato-Regular.ttf") format('truetype');
                font-weight: normal;
            }
        </style>
        <link rel="stylesheet" href="${addons_path}/c2c_webkit_report/assets/css/gisce.css"/>
        <link rel="stylesheet" href="${assets_path}/css/gisce.css"/>
        <link rel="stylesheet" href="${addons_path}/account_invoice_base/report/account_invoice.css"/>
        <%block name="custom_css"></%block>
    </head>
    <body>
        %for invoice in objects:
            <%
                lang = invoice.partner_id.lang or 'es_ES'
                setLang(lang)
                contact = report_o.get_contact_info(cursor, uid, invoice.id)[invoice.id]
                invoice_taxes = report_o.get_taxes(cursor, uid, invoice.id)[invoice.id]
                dades_client = invoice.address_invoice_id
                nom_client = dades_client.name or ''

                poblacio_client = ''
                provincia_client = ''

                if dades_client.id_poblacio:
                    poblacio_client = dades_client.id_poblacio.name or ''
                if dades_client.state_id:
                    provincia_client = dades_client.state_id.name or ''
            %>

            <table class="styled-table">
                <colgroup>
                    <col style="width: 30%"/>
                    <col style="width: 40%"/>
                    <col style="width: 30%"/>
                </colgroup>
                <tr>
                    <td>
                        ${self.logo_factura(invoice)}
                    </td>
                    <td class="center">
                        <h1>
                            ${company.name}
                        </h1>
                    </td>
                    <td id="partner-address">
                        <%
                            phone = empresa['phone']
                            email = empresa['email'] or ''
                            company_address = empresa['street']
                            company_address2 = empresa['poblacio'] + ' ' + empresa['zip']
                        %>
                        ${_(u"CIF:")} ${company.partner_id.vat or ''} <br/>
                        ${_(u"Address:")} ${company_address} <br/>
                        ${company_address2} <br/>
                        ${email} <br/>
                        ${phone}
                    </td>
                </tr>
            </table>
            <div class="default-margin">
                <div class="left-40">
                    <table class="styled-table no-padding-table">
                        <colgroup>
                            <col style="width: 30%"/>
                            <col style="width: auto"/>
                        </colgroup>
                        <tr>
                            <th colspan="2">
                                ${_(u"Invoice information")}
                            </th>
                        </tr>
                        <tr>
                            <td>
                                ${_(u"Invoice number:")}
                            </td>
                            <td>
                                ${invoice.number or ''}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                ${_(u"Date:")}
                            </td>
                            <td>
                                ${localize_period(invoice.date_invoice, lang)}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%
                                    if invoice.rectifying_id:
                                        if 'refund' in invoice.type:
                                            rect_type = _(u'Abonament de:')
                                            sign = -1
                                        else:
                                            rect_type = _(u'Rectifica de:')
                                            sign = 1
                                    else:
                                        rect_type = ''
                                        sign = 1
                                %>
                                ${rect_type}
                            </td>
                            <td>
                                <%
                                    if invoice.rectifying_id:
                                        rect_inv = invoice.rectifying_id.number
                                    else:
                                        rect_inv = ''
                                %>
                                ${rect_inv}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                ${_(u"Name:")}
                            </td>
                            <td>
                                ${invoice.partner_id.name}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                ${_(u"DNI/CIF:")}
                            </td>
                            <td>
                                <%
                                    if invoice.partner_id.vat:
                                        nif = (invoice.partner_id.vat).replace('ES', '')
                                    else:
                                        nif = ''
                                %>
                                ${nif}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                ${_(u"Address:")}
                            </td>
                            <td>
                                ${dades_client.street or ''} ${poblacio_client} ${provincia_client} ${dades_client.zip or ''}
                            </td>
                        </tr>
                        ${self.additional_details(invoice)}
                    </table>
                </div>
                <div class="right-45">
                    <div class="">
                        <table class="contact-info">
                            <tr>
                                <td>
                                    ${contact['name']}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    ${contact['street']}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    ${contact['aclarador']}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    ${contact['zip']} ${contact['city']}
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div id="preus" class="default-margin">
                <table class="styled-table">
                    <colgroup>
                        <col style="width: 10%"/>
                        <col style="width: 50%"/>
                        <col style="width: 20%"/>
                        <col style="width: 20%"/>
                    </colgroup>
                    <tr>
                        <th>
                            ${_(u"Quantity")}
                        </th>
                        <th>
                            ${_(u"Concept")}
                        </th>
                        <th>
                            ${_(u"Unit price")}
                        </th>
                        <th>
                            ${_(u"Amount")}
                        </th>
                    </tr>
                    <%
                        altres_lines = [l for l in invoice.invoice_line]
                    %>
                    %for i in sorted(altres_lines):
                        <tr>
                            <%
                                quantitat = sign * i.quantity
                                if float(quantitat).is_integer():
                                    quantitat = formatLang(quantitat, 0)
                                else:
                                    quantitat = formatLang(quantitat, 2)
                            %>
                            <td class="center">${quantitat}</td>
                            <td class="">${i.name}</td>
                            <td class="center">
                                ${format_currency(float(i.price_unit), 'EUR', locale='es_ES')}
                            </td>
                            <td class="center">
                                ${format_currency(sign * float(i.price_subtotal), 'EUR', locale='es_ES')}
                            </td>
                        </tr>
                    %endfor
                </table>
            </div>

            ${self.details(invoice)}

            <div id="impostos" class="default-margin secondary_color_background">
                <table class="styled-table">
                    <colgroup>
                        <col style="width: 10%"/>
                        <col style="width: 50%"/>
                        <col style="width: 20%"/>
                        <col style="width: 20%"/>
                    </colgroup>
                    <tr>
                        <th>
                            ${_(u"Base amount")}
                        </th>
                        <th>
                            ${_(u"Tax")}
                        </th>
                        <th>
                            ${_('Quota')}
                        </th>
                    </tr>
                    % for line in invoice_taxes:
                        <tr>
                            <td class="center">${formatLang(sign * float(line['base']))} €</td>
                            <td class="">${line['name']}</td>
                            <td class="center">${formatLang(sign * float(line['amount']))} €</td>
                        </tr>
                    % endfor
                </table>
            </div>
            <div class="right-40">
                <table class="styled-table">
                    <tr>
                        <th>
                            ${_(u"TOTAL")}
                        </th>
                    </tr>
                    <tr>
                        <td class="center">
                            ${format_currency(sign * float(invoice.amount_total), 'EUR', locale='es_ES')}
                        </td>
                    </tr>
                </table>
            </div>
        %endfor
    </body>
</html>

<%def name="logo_factura(invoice)">
    <img alt="logo" class="logo" src="data:image/jpeg;base64,${company.logo}"/>
</%def>
<%def name="details(invoice)">
    <div id="details">${invoice.comment or ''}</div>
</%def>
<%def name="additional_details(invoice)"></%def>
