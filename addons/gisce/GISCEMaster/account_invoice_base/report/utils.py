# -*- coding: utf-8 -*-


def localize_period(period, locale):
    """
    This function converts a numeric date to a traditional writing style.
    Example (in Spanish):
    2016/01/01 -> Viernes, 1 de Enero de 2016
    :param period: Date to be formatted to writing style
    :param locale: Language to be written
    :return: String with date in writing style
    """
    try:
        import babel
        from datetime import datetime
        dtf = datetime.strptime(period, '%Y-%m-%d')
        dtf = dtf.strftime("%y%m%d")
        dt = datetime.strptime(dtf, '%y%m%d')
        return babel.dates.format_datetime(dt, "d 'de' LLLL 'de' yyyy",
                                           locale=locale)
    except Exception:
        return ''
