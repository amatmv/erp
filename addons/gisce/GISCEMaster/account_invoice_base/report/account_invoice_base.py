# -*- coding: utf-8 -*-

from c2c_webkit_report import webkit_report
from report import report_sxw


webkit_report.WebKitParser(
    'report.account.invoice',
    'account.invoice',
    'account_invoice_base/report/account_invoice.mako',
    parser=report_sxw.rml_parse
)
