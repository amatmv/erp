# -*- coding: utf-8 -*-
from datetime import datetime


from osv import osv, fields
from tools.translate import _
import netsvc
import time
from oorq.oorq import ProgressJobsPool
from autoworker import AutoWorker
from oorq.decorators import job
from oorq.oorq import setup_redis_connection
from osv.osv import TransactionExecute
import pooler


def amount_grouped(invoice, mode='residual'):
    assert mode in ('residual', 'amount_total')
    if invoice.group_move_id:
        signal = {
            'in_invoice': 1,
            'out_invoice': 1,
            'in_refund': -1,
            'out_refund': -1
        }
        amount = sum(
            getattr(l.invoice, mode) * signal[l.invoice.type]
                for l in invoice.group_move_id.line_id
                    if l.invoice
        )
        if mode == 'residual':
            for l in invoice.group_move_id.line_id:
                if l.ref == invoice.group_move_id.ref:
                    break
            if l.reconcile_partial_id:
                amount = 0
                for l2 in l.reconcile_partial_id.line_partial_ids:
                    amount += l2.debit - l2.credit
    else:
        amount = getattr(invoice, mode)
    return amount


class AccountInvoice(osv.osv):

    _name = 'account.invoice'
    _inherit = 'account.invoice'

    def check_account_sequence_standard_configured(self, cursor, uid, context=None):
        seq_o = self.pool.get("ir.sequence")
        seq_to_check = ['Account Journal', 'Account reconcile sequence']
        seq_ids = seq_o.search(cursor, uid, [
            ('name', 'in', seq_to_check),
            ('implementation', '!=', 'standard')
        ])
        if len(seq_ids):
            raise osv.except_osv(
                _(u"Error de configuracio"),
                _(u"Per poder fer pagaments en paral.lel abans s'ha de configurar les seqüències {0} a mode 'standard'").format(seq_to_check)
            )
        return True

    def get_model_list(self, cursor, uid, context=None):
        return []

    def get_factura_ids_from_invoice_id(self, cursor, uid, ids, context=None):
        """
        :param ids: ids of the model account.invoice
        :return: a dict indexed by the model name and as a value, a tuple with
        the model reference and its ids:
        {
            'account.invoice': ([1, 2, 3, 4, 5, 6, ...], <Model 'account.invoice'>),
            'giscedata.facturacio.factura': ([10, 30, 50, ...], <Model 'giscedata.facturacio.factura'>),
            'giscegas.facturacio.factura': ([20, 40, 60, ...], <Model 'giscegas.facturacio.factura'>),
        }
        """
        if context is None:
            context = {}
        if not isinstance(ids, (tuple, list)):
            ids = [ids]
        res = {}
        acc_inv_ids = set(ids)
        for model_name in self.get_model_list(cursor, uid):
            model_obj = self.pool.get(model_name)
            cursor.execute(*(
                model_obj.q(cursor, uid).select(['id']).where([
                    ('invoice_id', 'in', ids)
                ])
            ))
            model_ids = list(map(lambda res: res[0], cursor.fetchall()))
            if model_ids:
                res.update({
                    model_name: (model_ids, model_obj)
                })
            acc_inv_ids = acc_inv_ids - set(model_ids)
        if not context.get('no_account_invoice'):
            res.update({'account.invoice': (list(acc_inv_ids), self)})
        return res

    def line_get_convert(self, cursor, uid, x, part, date, context=None):
        if context is None:
            context = {}
        res = super(AccountInvoice, self).line_get_convert(
            cursor, uid, x, part, date, context=context
        )
        cfg = self.pool.get('res.config')
        product_id = int(cfg.get(cursor, uid, 'deposit_product_id', -1))
        if product_id and x.get('product_id') == product_id:
            if context.get('invoice_id'):
                for model_name, (fact_ids, model_obj) in self.get_factura_ids_from_invoice_id(
                    cursor, uid, context.get('invoice_id', []), context={'no_account_invoice': True}
                ).items():
                    if fact_ids:
                        fact_id = fact_ids[0]
                        polissa_name = model_obj.q(cursor, uid).read(['polissa_id.name'], only_active=False).where([
                            ('id', '=', fact_id)
                        ])[0]['polissa_id.name']
                        res['ref'] = polissa_name
                        res['name'] = _(u'Dipòsit de garantia %s') % res['ref']
        return res

    def pay_and_reconcile_group(self, cursor, uid, ids, pay_amount,
                                pay_account_id, period_id, pay_journal_id,
                                writeoff_acc_id, writeoff_period_id,
                                writeoff_journal_id, context=None, name=''):
        move_obj = self.pool.get('account.move')
        mline_obj = self.pool.get('account.move.line')
        if context is None:
            context = {}
        if isinstance(ids, (list, tuple)):
            invoice_id = ids[0]
        else:
            invoice_id = ids

        invoice = self.browse(cursor, uid, invoice_id, context=context)
        if not invoice.group_move_id:
            raise osv.except_osv('Error', 'This invoice is not grouped')
        # Si la cobrem ha de ser -1
        # Si la paguem ha de ser 1
        direction = None
        group_move_ref = invoice.group_move_id.ref
        for line in invoice.group_move_id.line_id:
            if line.name == group_move_ref:
                if line.debit:
                    direction = -1
                else:
                    direction = 1
                break
        if direction is None:
            raise osv.except_osv(
                'Error', '{0} line not found!'.format(group_move_ref)
            )
        # We have to pay a group of invoices check if the amount
        # to pay is the desired.
        amount = pay_amount
        to_pay = amount < 0
        if 'date_p' in context and context['date_p']:
            date = context['date_p']
        else:
            date = time.strftime('%Y-%m-%d')
        move_lines = [
            # Move 1
            (0, 0, {
                'debit': to_pay and abs(amount),
                'credit': not to_pay and abs(amount),
                'account_id': invoice.account_id.id,
                'partner_id': invoice.partner_id.id,
                'ref': group_move_ref,
                'date': date,
                'name': group_move_ref
            }),
            # Move 2
            (0, 0, {
                'debit': not to_pay and abs(amount),
                'credit': to_pay and abs(amount),
                'account_id': pay_account_id,
                'partner_id': invoice.partner_id.id,
                'ref': group_move_ref,
                'date': date,
                'name': group_move_ref
            })
        ]
        move = {
            'ref': group_move_ref,
            'line_id': move_lines,
            'journal_id': pay_journal_id,
            'period_id': period_id,
            'date': date
        }
        move_id = move_obj.create(cursor, uid, move, context=context)

        currency = False
        if pay_journal_id:
            journal_obj = self.pool.get('account.journal')
            journal = journal_obj.browse(cursor, uid, pay_journal_id)
            currency = journal.currency
        if not currency:
            user = self.pool.get('res.users').browse(cursor, uid, uid)
            currency = user.company_id.currency_id
        precision_rounding = currency.rounding

        # Let's go to conciliate everything
        # Every invoice with its mirror and the grouped move with the
        # payment
        reconcile_lines_total = []
        for move_line in move_obj.browse(cursor, uid, move_id).line_id:
            if move_line.account_id.id == invoice.account_id.id:
                reconcile_lines_total.append(move_line.id)
        for move_line in invoice.group_move_id.line_id:
            if move_line.name == group_move_ref:
                reconcile_lines_total.append(move_line.id)
        mline_obj.reconcile_lines_of_group_move(cursor, uid, invoice.group_move_id.id, reconcile_lines_total, context=context)
        return move_id

    def pay_and_reconcile_with_progress(
            self, cursor, uid, jpool_data, invoices_datas, pay_account_id, period_id,
            pay_journal_id, writeoff_acc_id, writeoff_period_id,
            writeoff_journal_id, context=None, name=''
    ):
        """
         jpool_data és un diccionari amb:
         {
            'progress_obj': nom del model que te les barres de progress a emplenar
            'progress_id': ID de l'objecte, per poder fer un write a través de progress_obj
            'progres_field':camp al que fer el write amb el progres
         }


        invoices_datas es una llista de tuplas amb:
        [
            (ID_factura1, pay_amount1),
            (ID_factura_2, pay_amount2),
            ...
        ]
        """
        if context is None:
            context = {}

        from oorq.oorq import setup_redis_connection
        setup_redis_connection()

        progress_obj = jpool_data.get("progress_obj")
        progress_id = jpool_data.get("progress_obj_id")
        progress_obj = self.pool.get(progress_obj).browse(cursor, uid, progress_id)
        progress_field = jpool_data.get("progres_field")
        if not progress_obj or not progress_field:
            raise Exception(
                "Error",
                _(
                    u"Si es vol pagar en paral.lel i/o contabilitzar el "
                    u"progres s'ha de proporcionar un 'progress_obj' i un "
                    u"'progress_field'."
                )
            )

        j_pool = ProgressJobsPool(progress_obj, progress_field, "openerp.pay_and_reconcile.progress")
        amax_proc = int(self.pool.get("res.config").get(cursor, uid, "pay_and_reconcile_tasks_max_procs", "0"))
        if not amax_proc:
            amax_proc = None
        aw = AutoWorker(queue="pay_and_reconcile_tasks", default_result_ttl=24 * 3600, max_procs=amax_proc)
        for inv_id, pay_amount in invoices_datas:
            j_pool.add_job(super(AccountInvoice, self).pay_and_reconcile_async(
                cursor, uid, inv_id, pay_amount, pay_account_id, period_id,
                pay_journal_id, writeoff_acc_id, writeoff_period_id,
                writeoff_journal_id, context, name
            ))

        aw.work()
        j_pool.join()
        res = []
        for inv_res in j_pool.results.values():
            res.append(inv_res)
        progress_obj.write({
            progress_field: 100.0
        })
        return res

    @job(queue="pay_and_reconcile_tasks")
    def pay_and_reconcile_async(self, cursor, uid, ids, pay_amount, pay_account_id,
                          period_id, pay_journal_id, writeoff_acc_id,
                          writeoff_period_id, writeoff_journal_id, context=None,
                          name=''):
        return self.pay_and_reconcile(
            cursor, uid, ids, pay_amount, pay_account_id, period_id,
            pay_journal_id, writeoff_acc_id, writeoff_period_id,
            writeoff_journal_id, context, name
        )

    def pay_and_reconcile(self, cursor, uid, ids, pay_amount, pay_account_id,
                          period_id, pay_journal_id, writeoff_acc_id,
                          writeoff_period_id, writeoff_journal_id, context=None,
                          name=''):
        if context is None:
            context = {}
        if isinstance(ids, (list, tuple)):
            invoice_id = ids[0]
        else:
            invoice_id = ids

        invoice = self.read(cursor, uid, invoice_id, ['group_move_id'],
                            context=context)
        if invoice['group_move_id']:
            res = self.pay_and_reconcile_group(
                cursor, uid, ids, pay_amount, pay_account_id, period_id,
                pay_journal_id, writeoff_acc_id, writeoff_period_id,
                writeoff_journal_id, context, name
            )
        else:
            res = super(AccountInvoice, self).pay_and_reconcile(
                cursor, uid, ids, pay_amount, pay_account_id, period_id,
                pay_journal_id, writeoff_acc_id, writeoff_period_id,
                writeoff_journal_id, context, name
            )
        return res

    def action_cancel(self, cursor, uid, ids, *args):
        res = super(AccountInvoice, self).action_cancel(cursor, uid, ids, *args)
        account_move_obj = self.pool.get('account.move')
        invoices = self.read(cursor, uid, ids, ['group_move_id'])
        for i in invoices:
            if i['group_move_id']:
                account_move_obj.button_cancel(cursor, uid, [i['group_move_id'][0]])
                # delete the move this invoice was pointing to
                # Note that the corresponding move_lines and move_reconciles
                # will be automatically deleted too
                account_move_obj.unlink(cursor, uid, [i['group_move_id'][0]])
        return res

    def always_compensate_RA(self, cursor, uid, invoice_type):
        """
        Returns the value of the config variable that determines if opening RA
        invoice the BRA always has to compensate the RA. If the config variable
        is not found then it returns the value from the superclass.

        :param cursor:
        :param uid:
        :param invoice_type: Type of the invoice
        :type invoice_type: str
        :return:
        """
        conf_obj = self.pool.get('res.config')
        config_var = ''
        if invoice_type.startswith('out'):
            config_var = 'always_compensate_RA_out_invoice'
        if invoice_type.startswith('in'):
            config_var = 'always_compensate_RA_in_invoice'

        config_val = conf_obj.get(cursor, uid, config_var)
        if config_val is not None:
            return bool(int(config_val))
        return super(AccountInvoice, self).always_compensate_RA(
            cursor, uid, invoice_type
        )

    def action_move_create(self, cursor, uid, ids, *args):
        conf_obj = self.pool.get('res.config')

        allow_open_future_provider_invoices = bool(int(conf_obj.get(
            cursor, uid, 'allow_open_future_provider_invoices', False
        )))
        today = datetime.today().strftime('%Y-%m-%d')

        for inv in self.read(cursor, uid, ids, ['origin_date_invoice']):
            if (not allow_open_future_provider_invoices and
                    inv['origin_date_invoice'] > today):
                raise osv.except_osv(
                    _('Wrong original invoice date!'),
                    _('The original invoice date {} is a future date. You '
                      'cannot open invoices with future original invoice date.'
                      ).format(inv['origin_date_invoice'])
                )

        res = super(AccountInvoice, self).action_move_create(
            cursor, uid, ids, *args
        )

        return res

    def action_date_assign(self, cursor, uid, ids, *args):
        # Change only when invoice doesn't have number
        cfg_obj = self.pool.get('res.config')
        period_obj = self.pool.get('account.period')

        fields_to_read = ['type', 'number', 'date_invoice']
        change_date_in_acc_inv = bool(int(
            cfg_obj.get(cursor, uid, 'fact_change_date_in_acc_inv', '0')))
        change_date_out_acc_inv = bool(int(
            cfg_obj.get(cursor, uid, 'fact_change_date_out_acc_inv', '0')))

        current_date = datetime.now().strftime('%Y-%m-%d')
        for inv in self.read(cursor, uid, ids, fields_to_read):
            # Change only when invoice doesn't have number
            write_values = {}
            if not inv['number']:
                inv_type = inv['type']
                if change_date_out_acc_inv and inv_type.startswith('out'):
                    write_values['date_invoice'] = current_date
                elif change_date_in_acc_inv and inv_type.startswith('in'):
                    write_values['date_invoice'] = current_date
            date_i = False
            if write_values:
                date_i = write_values['date_invoice']
            else:
                date_i = inv['date_invoice']

            if date_i:
                period_ids = period_obj.search(
                    cursor, uid, [
                        ('date_start', '<=', date_i),
                        ('date_stop', '>=', date_i)
                    ]
                )
                if period_ids:
                    period_id = period_ids[0]
                    write_values['period_id'] = period_id

            if write_values:
                self.write(cursor, uid, [inv['id']], write_values)
        return super(AccountInvoice, self).action_date_assign(
            cursor, uid, ids, *args
        )

    def make_move_differences(self, cursor, uid, ids, context=None):
        """Group invoice in accounting mode with differences.
        """
        if context is None:
            context = {}
        types = {
            'out_invoice': -1,
            'in_invoice': 1,
            'out_refund': 1,
            'in_refund': -1
        }
        wf_service = netsvc.LocalService('workflow')
        mline_obj = self.pool.get('account.move.line')
        move_obj = self.pool.get('account.move')
        account_obj = self.pool.get('account.account')
        ir_seq = self.pool.get('ir.sequence')
        move_lines = []
        diff = 0
        # Contra assentments to cancel the amount
        # TODO: Work with a bridge journal
        group_move_ref = context.get('group_move_ref')
        if group_move_ref is None:
            group_move_ref = ir_seq.get(
                cursor, uid, 'account.invoice.payment.group'
            )
        ibans_dict = {}
        #take the choosen date
        if 'date_p' in context and context['date_p']:
            date=context['date_p']
        else:
            date=time.strftime('%Y-%m-%d')
        for invoice in self.browse(cursor, uid, ids, context=context):
            if invoice.group_move_id:
                raise osv.except_osv(
                    u'Error',
                    _(u'Error ja existeix una factura agrupada: {0}').format(
                        invoice.number
                    )
                )
            if invoice.state != 'open':
                raise osv.except_osv(
                    u'Error',
                    _(u'Error la factura no està en estat obert: {0}').format(
                        invoice.number)
                )
            direction = types[invoice.type]
            if invoice.type in ('in_invoice', 'in_refund'):
                ref = invoice.reference
            else:
                ref = self._convert_ref(cursor, uid, invoice.number)
            amount = invoice.residual
            diff += amount * direction
            move_lines += [(0, 0, {
                'debit': direction * amount > 0 and abs(direction * amount),
                'credit': direction * amount < 0 and abs(direction * amount),
                'account_id': invoice.account_id.id,
                'partner_id': invoice.partner_id.id,
                'ref': ref,
                'date': date,
                'currency_id': False,
                'amount_currency': 0,
                'period_id': invoice.period_id.id,
                'journal_id': invoice.journal_id.id,
                'name': '{0} - {1}'.format(group_move_ref, ref),
                'invoice': invoice.id
            })]
            iban = (invoice.partner_bank and invoice.partner_bank.iban or
                    "no iban")
            ibans_dict.setdefault(iban, [])
            ibans_dict[iban].append(invoice.number)

        if len(ibans_dict.keys()) > 1:
            msg = _(u"Error s'han trobat factures amb diferents IBANs:")
            for iban, invoices in ibans_dict.iteritems():
                msg = u"{0}\n    * {1}: {2}".format(msg, iban, invoices)
            raise osv.except_osv(u'Error', msg)

        # Make the last move with the difference
        move_lines += [(0, 0, {
            'debit': diff < 0 and abs(diff),
            'credit': diff > 0 and abs(diff),
            'account_id': invoice.account_id.id,
            'partner_id': invoice.partner_id.id,
            'ref': group_move_ref,
            'date': date,
            'currency_id': False,
            'amount_currency': 0,
            'period_id': invoice.period_id.id,
            'journal_id': invoice.journal_id.id,
            'name': group_move_ref
        })]

        move = {
            'ref': group_move_ref,
            'line_id': move_lines,
            'journal_id': invoice.journal_id.id,
            'period_id': invoice.period_id.id,
            'date': date
        }
        move_id = move_obj.create(cursor, uid, move, context=context)
        move = move_obj.browse(cursor, uid, move_id)
        move.post()
        # Update the stored value (fields.function), so we write
        # to trigger recompute
        for invoice in self.browse(cursor, uid, ids, context=context):
            invoice.write({
                'group_move_id': move_id,
            }, context=context)
            wf_service.trg_validate(uid, 'account.invoice', invoice.id,
                                    'open_test', cursor)
            mline_obj.write(cursor, uid, invoice.move_line_id_payment_get(), {})
            invoice.write({})
        return diff

    def undo_payment_with_moves(self, cursor, uid, ids, context=None):
        """
        Undo payment creating a inverse movement of last payment
        """
        move_obj = self.pool.get('account.move')
        ml_obj = self.pool.get('account.move.line')
        wf_service = netsvc.LocalService('workflow')
        rec_obj = self.pool.get('account.move.reconcile')

        if context is None:
            context = {}
        period_id = self.pool.get('account.period').find(cursor, uid)[0]
        user = self.pool.get("res.users").browse(cursor, uid, uid)
        partner_of_company = user.company_id and user.company_id.partner_id.id or 1
        for invoice in self.browse(cursor, uid, ids, context=context):
            if invoice.group_move_id:
                raise osv.except_osv('Error', _(
                    'Aquesta factura esta agrupada '
                    'i no es pot desfer el pagament.')
                                     )

            if not invoice.payment_ids:
                continue
            lines_to_reconcile = [x.id for x in invoice.payment_ids]
            last_payment = invoice.payment_ids[0]
            move_data = move_obj.copy_data(
                cursor, uid, last_payment.move_id.id,
                {'date': time.strftime('%Y-%m-%d'), 'period_id': period_id}
            )[0]
            total_line = None
            final_lines = []
            for line in move_data['line_id']:
                # we go to second position because `copy_data` returns
                # info to directly create one2many fields as:
                # [(0, 0, {values}]
                line_data = line[2]
                debit = line_data['debit']
                credit = line_data['credit']
                line_data['name'] = _('Desfer pagament: {}').format(
                    line_data['name']
                )
                line_data['debit'] = credit
                line_data['credit'] = debit
                line_data['reconcile_id'] = False
                line_data['reconcile_partial_id'] = False
                line_data['date'] = time.strftime('%Y-%m-%d')
                line_data['date_created'] = time.strftime('%Y-%m-%d')
                line_data['period_id'] = period_id

                if line_data['partner_id'] == partner_of_company:  # Pagaments moviment unic es fan al partner principal
                    total_line = line
                elif line_data['ref'] in (invoice.number, invoice.origin):
                    final_lines.append(line)

            if total_line and len(final_lines) != len(move_data['line_id']):
                total_line[2]['debit'] = sum([x[2]['credit'] for x in final_lines])
                total_line[2]['credit'] = sum([x[2]['debit'] for x in final_lines])
                final_lines.append(total_line)
                move_data['line_id'] = final_lines

            move_id = move_obj.create(cursor, uid, move_data)
            move = move_obj.browse(cursor, uid, move_id)
            for line in move.line_id:
                if line.account_id.id == last_payment.account_id.id:
                    lines_to_reconcile.append(line.id)
            lines_to_reconcile += self.move_line_id_payment_get(
                cursor, uid, [invoice.id]
            )

            # We have to unreconcile current payments
            rec_ids = []
            for payment_line in invoice.payment_ids:
                if payment_line.reconcile_id:
                    rec_ids.append(payment_line.reconcile_id.id)
                if payment_line.reconcile_partial_id:
                    rec_ids.append(payment_line.reconcile_partial_id.id)
            rec_obj.unlink(cursor, uid, rec_ids)

            ctx = context.copy()
            ctx['force_different_account'] = True
            ml_obj.reconcile_partial(
                cursor, uid, lines_to_reconcile, 'manual', ctx
            )

            # Update the stored value (fields.function), so we write to trigger recompute
            wf_service.trg_validate(uid, 'account.invoice', invoice.id,
                                    'open_test', cursor)
            ml_obj.write(cursor, uid, invoice.move_line_id_payment_get(), {})
            invoice.write({})
            return True

    def undo_first_undoing_payment_with_moves(self, cursor, uid, ids, context=None):
        """
        Undo payment creating a inverse movement of last payment without undoing
        """
        move_obj = self.pool.get('account.move')
        ml_obj = self.pool.get('account.move.line')
        wf_service = netsvc.LocalService('workflow')
        rec_obj = self.pool.get('account.move.reconcile')

        if context is None:
            context = {}
        period_id = self.pool.get('account.period').find(cursor, uid)[0]
        for invoice in self.browse(cursor, uid, ids, context=context):
            if invoice.group_move_id:
                raise osv.except_osv('Error', _(
                    'Aquesta factura esta agrupada '
                    'i no es pot desfer el pagament.')
                                     )

            lines_to_reconcile = [x.id for x in invoice.payment_ids]
            move_to_reconcile = [x.move_id.id for x in invoice.payment_ids]
            payments = []
            for move_id in move_to_reconcile:
                payments = ml_obj.search(cursor, uid, [('id', 'in', lines_to_reconcile), ('move_id', '=', move_id), ('ref','in',[invoice.number, invoice.origin]),('blocked', 'not in', [True])])
                if payments:
                    break
            #moves_ids = move_obj.search(cursor, uid, [('id', 'in', move_to_reconcile), ('state', 'not in', ['cancelled', 'Cancelled'])])
            #payments = ml_obj.search(cursor, uid, [('id', 'in', lines_to_reconcile), ('move_id', 'in', moves_ids)])
            last_payment = ml_obj.browse(cursor, uid, payments)
            if not last_payment:
                continue

            last_payment = last_payment[0]
            #move_line_ids = ml_obj.search(cursor, uid, [('move_id', '=', last_payment.move_id.id)])
            move_data = move_obj.copy_data(
                cursor, uid, last_payment.move_id.id,
                {'date': time.strftime('%Y-%m-%d'), 'period_id': period_id}
            )[0]
            total_line = None
            final_lines = []
            for line in move_data['line_id']:
                # we go to second position because `copy_data` returns
                # info to directly create one2many fields as:
                # [(0, 0, {values}]
                line_data = line[2]
                debit = line_data['debit']
                credit = line_data['credit']
                line_data['name'] = _('Desfer pagament: {}').format(
                    line_data['name']
                )
                line_data['debit'] = credit
                line_data['credit'] = debit
                line_data['reconcile_id'] = False
                line_data['reconcile_partial_id'] = False
                line_data['date'] = time.strftime('%Y-%m-%d')
                line_data['date_created'] = time.strftime('%Y-%m-%d')
                line_data['period_id'] = period_id
                line_data['blocked'] = True

                if line_data['partner_id'] == 1:  # Els pagaments amb moviment unic es fan al partner 1
                    total_line = line
                elif line_data['ref'] in (invoice.number, invoice.origin):
                    final_lines.append(line)

            if total_line and len(final_lines) != len(move_data['line_id']):
                total_line[2]['debit'] = sum([x[2]['credit'] for x in final_lines])
                total_line[2]['credit'] = sum([x[2]['debit'] for x in final_lines])
                final_lines.append(total_line)
                move_data['line_id'] = final_lines
            move_id = move_obj.create(cursor, uid, move_data)
            move = move_obj.browse(cursor, uid, move_id)
            for line in move.line_id:
                if line.account_id.id == last_payment.account_id.id:
                    lines_to_reconcile.append(line.id)
            lines_to_reconcile += self.move_line_id_payment_get(
                cursor, uid, [invoice.id]
            )
            #Marco como anulado el pago
            #ml_obj.write(cursor, uid, move_line_ids, {'move_canceled': True})
            ml_obj.write(cursor, uid, [last_payment.id], {'blocked': True})
            #move_obj.write(cursor, uid, [move_id], {'state': 'cancelled'})

            # We have to unreconcile current payments
            rec_ids = []
            for payment_line in invoice.payment_ids:
                if payment_line.reconcile_id:
                    rec_ids.append(payment_line.reconcile_id.id)
                if payment_line.reconcile_partial_id:
                    rec_ids.append(payment_line.reconcile_partial_id.id)
            rec_obj.unlink(cursor, uid, rec_ids)

            ctx = context.copy()
            ctx['force_different_account'] = 'True'
            ml_obj.reconcile_partial(
                cursor, uid, lines_to_reconcile, 'manual', ctx
            )

            # Update the stored value (fields.function), so we write to trigger recompute
            wf_service.trg_validate(uid, 'account.invoice', invoice.id,
                                    'open_test', cursor)
            ml_obj.write(cursor, uid, invoice.move_line_id_payment_get(), {})
            invoice.write({})
            return True

    def undo_payment(self, cursor, uid, ids, context=None):
        """Dispatch different modes of undo payments"""
        cfg_obj = self.pool.get('res.config')
        mode = int(cfg_obj.get(cursor, uid, 'undo_payment_mode', '1'))
        if mode == 1:
            return self.undo_payment_remove(cursor, uid, ids, context)
        elif mode == 2:
            return self.undo_payment_with_moves(cursor, uid, ids, context)
        elif mode == 3:
            return self.undo_first_undoing_payment_with_moves(cursor, uid, ids, context)

    def undo_payment_remove(self, cursor, uid, ids, context=None):
        """funcio per desfer tots els pagos d'una factura"""

        if context is None:
            context = {}

        wf_service = netsvc.LocalService('workflow')
        move_obj = self.pool.get('account.move')
        ml_obj = self.pool.get('account.move.line')
        reconcile_obj = self.pool.get('account.move.reconcile')

        for invoice in self.browse(cursor, uid, ids):
            if invoice.group_move_id:
                raise osv.except_osv('Error', _(
                    'Aquesta factura esta agrupada '
                    'i no es pot desfer el pagament.')
                )
            # Get payment moves
            move_ids = map(lambda x: x.move_id.id, invoice.payment_ids)
            reconcile_ids = []
            refs = []
            for payment in invoice.payment_ids:
                refs.append(payment.ref)
                if payment.reconcile_id:
                    reconcile_ids.append(payment.reconcile_id.id)
                if payment.reconcile_partial_id:
                    reconcile_ids.append(payment.reconcile_partial_id.id)
            # Undo reconcile
            reconcile_obj.unlink(cursor, uid, reconcile_ids, context)
            move_obj.button_cancel(cursor, uid, move_ids, context)
            ml_ids = ml_obj.search(cursor, uid, [
                ('move_id', 'in', move_ids),
                ('ref', 'in', refs)
            ])
            ml_obj.unlink(cursor, uid, ml_ids, check=False)
            # Unlink payment moves
            for move in move_obj.read(cursor, uid, move_ids, ['line_id']):
                if not move['line_id']:
                    move_obj.unlink(cursor, uid, [move['id']], context)
            # Re-Open the invoice
            wf_service.trg_validate(uid, 'account.invoice', invoice.id,
                                    'open_test', cursor)
        return True

    def unpay_group(self, cursor, uid, ids, amount, pay_account_id, period_id,
                    pay_journal_id, context=None, name=''):
        """Unpay a grouped invoices.
        """
        if context is None:
            context = {}
        if isinstance(ids, (list, tuple)):
            invoice_id = ids[0]
        else:
            invoice_id = ids
        invoice = self.browse(cursor, uid, invoice_id, context=context)
        if not invoice.group_move_id:
            raise osv.except_osv('Error', 'This invoice is not grouped')
        # Unpay all invoices
        invoices_to_regroup = []
        for line in invoice.group_move_id.line_id:
            if line.invoice:
                ctx = context.copy()
                ctx['unpay_individual'] = True
                invoices_to_regroup.append(line.invoice.id)
                unp_invoice = line.invoice
                unp_invoice.unpay(
                    unp_invoice.amount_total,
                    pay_account_id,
                    period_id,
                    pay_journal_id,
                    ctx,
                    'Dev. grp {0}'.format(invoice.group_move_id.name)
                )
        ctx = context.copy()
        ctx['group_move_ref'] = invoice.group_move_id.ref
        for line in invoice.group_move_id.line_id:
            if line.invoice:
                line.invoice.write({'group_move_id': False})
        self.make_move_differences(
            cursor, uid, invoices_to_regroup, context=ctx
        )
        return True

    def unpay(self, cursor, uid, ids, amount, pay_account_id, period_id,
              pay_journal_id, context=None, name=''):
        """This method makes an invoice unpaid, updating an other movement.

        The code is inspired by invoice.py:pay_and_reconcile L:992
        """
        wf_service = netsvc.LocalService('workflow')
        acc_move_obj = self.pool.get('account.move')
        rec_obj = self.pool.get('account.move.reconcile')
        move_obj = self.pool.get('account.move.line')
        journal_obj = self.pool.get('account.journal')
        if context is None:
            context = {}
        if len(ids) != 1:
            raise osv.except_osv(
                "Error",
                "Can only pay one invoice at a time"
            )
        invoice = self.browse(cursor, uid, ids[0])
        if invoice.group_move_id and not context.get('unpay_individual'):
            return invoice.unpay_group(
                amount, pay_account_id, period_id, pay_journal_id,
                context, name
            )
        # Get the account used to pay the invoice
        journal = journal_obj.browse(cursor, uid, pay_journal_id)
        src_account_id = journal.default_credit_account_id.id
        # Take the seq as name for move
        types = {'out_invoice': -1, 'in_invoice': 1, 'out_refund': 1, 'in_refund': -1}
        direction = types[invoice.type]
        #take the choosen date
        if 'date_p' in context and context['date_p']:
            date=context['date_p']
        else:
            date=time.strftime('%Y-%m-%d')

        # Take the amount in currency and the currency of the payment
        if 'amount_currency' in context and context['amount_currency'] and 'currency_id' in context and context['currency_id']:
            amount_currency = context['amount_currency']
            currency_id = context['currency_id']
        else:
            amount_currency = False
            currency_id = False

        if invoice.type in ('in_invoice', 'in_refund'):
            ref = invoice.reference
        else:
            ref = self._convert_ref(cursor, uid, invoice.number)
        # Pay attention to the sign for both debit/credit AND amount_currency
        l1 = {
            'debit': direction * amount>0 and direction * amount,
            'credit': direction * amount<0 and - direction * amount,
            'account_id': src_account_id,
            'partner_id': invoice.partner_id.id,
            'ref':ref,
            'date': date,
            'currency_id':currency_id,
            'amount_currency':amount_currency and direction * amount_currency or 0.0,
            'period_id': period_id,
            'journal_id': pay_journal_id
        }
        l2 = {
            'debit': direction * amount<0 and - direction * amount,
            'credit': direction * amount>0 and direction * amount,
            'account_id': pay_account_id,
            'partner_id': invoice.partner_id.id,
            'ref':ref,
            'date': date,
            'currency_id':currency_id,
            'amount_currency':amount_currency and - direction * amount_currency or 0.0,
            'period_id': period_id,
            'journal_id': pay_journal_id
        }

        if not name:
            name = invoice.invoice_line and invoice.invoice_line[0].name or invoice.number
        l1['name'] = name
        l2['name'] = name
        lines = [(0, 0, l1), (0, 0, l2)]
        move = {
            'ref': ref,
            'line_id': lines,
            'journal_id': pay_journal_id,
            'period_id': period_id,
            'date': date
        }
        move_id = acc_move_obj.create(cursor, uid, move, context=context)
        move = acc_move_obj.browse(cursor, uid, move_id)
        if invoice.reconciled:
            lines_to_reconcile = [
                l.id for l in move.line_id if l.account_id.id == pay_account_id
            ]
        else:
            lines_to_reconcile = [l.id for l in move.line_id]
        lines_to_reconcile += self.move_line_id_payment_get(
            cursor, uid, [invoice.id]
        )
        # We have to unreconcile current payments
        rec_ids = []
        for payment_line in invoice.payment_ids:
            lines_to_reconcile.append(payment_line.id)
            if payment_line.reconcile_id:
                rec_ids.append(payment_line.reconcile_id.id)
            if payment_line.reconcile_partial_id:
                rec_ids.append(payment_line.reconcile_partial_id.id)
        rec_obj.unlink(cursor, uid, rec_ids)

        ctx = context.copy()
        ctx['force_different_account'] = True
        move_obj.reconcile_partial(
            cursor, uid, lines_to_reconcile, 'manual', ctx
        )

        # Update the stored value (fields.function), so we write to trigger recompute
        wf_service.trg_validate(uid, 'account.invoice', invoice.id,
                                'open_test', cursor)
        move_obj.write(cursor, uid, invoice.move_line_id_payment_get(), {})
        invoice.write({})
        return True

    def unpay_multiple(self, cursor, uid, ids, amount, pay_account_id,
                       period_id, pay_journal_id, context=None, name=''):
        """

        :param cursor:
        :param uid:
        :param ids: <account.invoice> ids.
        :param amount: Dictionary where the key is the <account.invoice> id and
            the value is the amount to unpay.
        :param pay_account_id:
        :param period_id:
        :param pay_journal_id:
        :param context:
        :param name:
        :return:
        """
        if context is None:
            context = {}

        journal_o = self.pool.get('account.journal')
        account_move_o = self.pool.get('account.move')
        account_move_line_o = self.pool.get('account.move.line')
        account_move_reconcile_o = self.pool.get('account.move.reconcile')
        wf_service = netsvc.LocalService('workflow')

        # Processar estat pendent
        pstate_o = self.pool.get('account.invoice.pending.state')
        conf_o = self.pool.get('res.config')
        default_move_pstate = int(conf_o.get(cursor, uid, 'unpay_move_pending_state', 0))
        move_pstate = context.get('unpay_move_pending_state', default_move_pstate)

        if 'amount_currency' in context:
            raise NotImplementedError

        types = {
            'out_invoice': -1,      'in_invoice': 1,
            'out_refund': 1,        'in_refund': -1
        }

        date = context.get('date_p', False)
        if not date:
            date = time.strftime('%Y-%m-%d')

        journal = journal_o.browse(cursor, uid, pay_journal_id)
        base_src_account_id = journal.default_credit_account_id.id

        total_debit = 0
        total_credit = 0

        line_id = []
        for invoice in self.browse(cursor, uid, ids, context=context):
            if invoice.group_move_id:
                raise NotImplementedError('Les devolucions amb moviment únic per factures agrupades no està suportat')
            direction = types[invoice.type]
            invoice_amount = direction * amount[invoice.id]
            debit_amount = abs(invoice_amount) if invoice_amount < 0 else False
            credit_amount = abs(invoice_amount) if invoice_amount > 0 else False

            reference = invoice.reference
            if invoice.type not in ('in_invoice', 'in_refund'):
                reference = self._convert_ref(cursor, uid, invoice.number)

            invoice_move_line_vals = {
                'ref': reference,               'name': name,
                'debit': debit_amount,          'credit': credit_amount,
                'account_id': pay_account_id,   'partner_id': invoice.partner_id.id,
                'period_id': period_id,         'journal_id': pay_journal_id,
                'date': date
            }

            line_id.append((0, 0, invoice_move_line_vals))

            total_debit += debit_amount
            total_credit += credit_amount

        move_line_total = {
            'ref': name,                        'name': name,
            'debit': total_credit,              'credit': total_debit,
            'account_id': base_src_account_id,  'partner_id': 1,
            'period_id': period_id,             'journal_id': pay_journal_id,
            'date': date
        }
        line_id.append((0, 0, move_line_total))
        line_id = list(reversed(line_id))

        move = {
            'ref': name,                'date': date,
            'line_id': line_id,         'journal_id': pay_journal_id,
            'period_id': period_id,
        }
        move_id = account_move_o.create(cursor, uid, move, context=context)
        move = account_move_o.browse(cursor, uid, move_id)
        dev_lines = {x.ref: x.id for x in move.line_id}

        ctx = context.copy()
        ctx['force_different_account'] = True

        for invoice in self.browse(cursor, uid, ids, context=context):
            lines_to_reconcile = [
                l.id for l in move.line_id if l.account_id.id == pay_account_id and l.invoice.id == invoice.id
            ]

            lines_to_reconcile += self.move_line_id_payment_get(
                cursor, uid, [invoice.id]
            )

            # We have to unreconcile current payments
            rec_ids = []
            for payment_line in invoice.payment_ids:
                lines_to_reconcile.append(payment_line.id)
                if payment_line.reconcile_id:
                    rec_ids.append(payment_line.reconcile_id.id)
                if payment_line.reconcile_partial_id:
                    rec_ids.append(payment_line.reconcile_partial_id.id)
            account_move_reconcile_o.unlink(cursor, uid, rec_ids)

            reference = invoice.reference
            if invoice.type not in ('in_invoice', 'in_refund'):
                reference = self._convert_ref(cursor, uid, invoice.number)
            account_move_line_o.reconcile_lines(
                cursor, uid, [dev_lines[reference]]+lines_to_reconcile, 'manual', context=ctx
            )

            # Update the stored value (fields.function), so we write to trigger
            # recompute
            wf_service.trg_validate(
                uid, 'account.invoice', invoice.id, 'open_test', cursor
            )

            account_move_line_o.write(cursor, uid, invoice.move_line_id_payment_get(), {})
            invoice.write({})

            # Processar estat pendent
            if pstate_o is not None and move_pstate:
                self.go_on_pending(cursor, uid, ids, context)

        return True

    def pay_with_unique_account_move(
            self, cursor, uid, ids, reference, pay_journal_id, pay_partner_id=1,
            pay_account_id=None, date=None, period_id=None,
            writeoff_acc_id=None, writeoff_period_id=None,
            writeoff_journal_id=None, cobraments_journal_id=None, context=None):
        """
        Calcula el ammount_total i després crida el pay_and_reconcile_multiple
        """
        if context is None:
            context = {}
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        if context.get('from_payment_line'):
            raise NotImplementedError(
                u"Error",
                u"Si es paga amb payment.lines s'ha de cridar directament al "
                u"pay_and_reconcile_multiple amb el amount_total calculat."
            )
        amount_total = 0.0
        types = {'out_invoice': -1, 'in_invoice': 1, 'out_refund': 1, 'in_refund': -1}
        for inv_data in self.read(cursor, uid, ids, ['type', 'residual']):
            to_pay = inv_data['residual']
            direction = types[inv_data['type']]
            # No fa falta mirar lo de posarho en negatiu perque el residual ja porta el simbol
            # if to_pay < 0:
            #     direction *= -1
            to_pay = to_pay * direction
            amount_total += to_pay

        return self.pay_and_reconcile_multiple(
            cursor, uid, amount_total, ids, reference, pay_journal_id,
            pay_partner_id=pay_partner_id,
            pay_account_id=pay_account_id,
            date=date,
            period_id=period_id,
            writeoff_acc_id=writeoff_acc_id,
            writeoff_period_id=writeoff_period_id,
            writeoff_journal_id=writeoff_journal_id,
            cobraments_journal_id=cobraments_journal_id,
            context=context
        )

    def pay_and_reconcile_multiple(
            self, cursor, uid, amount_total, ids, reference, pay_journal_id,
            pay_partner_id=1, pay_account_id=None, date=None, period_id=None,
            writeoff_acc_id=None, writeoff_period_id=None,
            writeoff_journal_id=None, cobraments_journal_id=None, context=None
    ):
        """
        Paga varies factures fent un únic moviment en que hi ha una linia
        amb el total i una linia per cada factura.
        Si es proporciona un cobraments_journal_id adicionalment hi ha 2
        linies més per cada factura com a moviment intermig.

        Quan no pot pagar algun ID elimina tots els moviments generats per deixar-ho tot net.
        Retorna una llista amb els ids que no ha pogut pagar. Per tant, si va tot be retorna llista buida.

        Hi ha la particularitat de que 'ids' poden ser tant ids de
        account.invoice com de payment.line. Si son de payment.line s'ha de
        especificar en el context amb un 'from_payment_line' a True. Esta fet
        així perqué es pugui cridar per pagar remeses.

        ATENCIO: el amount_total s'espera negatiu quan el total és a cobrar i
        positiu quan és a pagar. S'ha de pennsar que és el "total a pagar",
        per tant si és negatiu el cobres i si és positiu el pagues.
        """
        account_move_line_o = self.pool.get('account.move.line')
        account_move_o = self.pool.get('account.move')
        payment_line_o = self.pool.get('payment.line')
        invoice_o = self.pool.get("account.invoice")
        journal_o = self.pool.get("account.journal")
        if not isinstance(ids, list):
            ids = [ids]

        if not len(ids):
            return True

        if context is None:
            context = {}

        # Preparacio de les dades per pagar
        from_payment_line = context.get('from_payment_line')
        if not date:
            date = datetime.today().strftime("%Y-%m-%d")
        if not period_id:
            search_params = [
                ('date_start', '<=', date),
                ('date_stop', '>=', date),
            ]
            acc_per_obj = self.pool.get('account.period')
            period_id = acc_per_obj.search(cursor, uid, search_params)
            if not period_id:
                raise osv.except_osv(
                    _(u'Error de Configuració'),
                    _(u"No s'ha trobat un període comptable per la data actual.")
                )
            else:
                period_id = period_id[0]
        if not pay_account_id:
            pay_account_id = journal_o.read(cursor, uid, pay_journal_id, ['default_credit_account_id', 'name'])
            if not pay_account_id['default_credit_account_id']:
                raise osv.except_osv(
                    _(u'Error de Configuració'),
                    _(u"El diari {0} no te el compte de credit configurat.").format(pay_account_id['name'])
                )
            else:
                pay_account_id = pay_account_id['default_credit_account_id'][0]

        # Revisem si es vol escriure el progres i/o fer en paral.lel i ho preparem tot
        jpl_data = context.get("jpool_data")
        work_async = context.get("work_async")
        j_pool = None
        progress_obj = progress_field_mlines = progress_field_reconcile = None
        if jpl_data or work_async:
            progress_obj = jpl_data.get("progress_obj")
            progress_id = jpl_data.get("progress_obj_id")
            progress_obj = self.pool.get(progress_obj).browse(cursor, uid, progress_id)
            progress_field_mlines = jpl_data.get("progress_field_mlines")
            progress_field_reconcile = jpl_data.get("progress_field_reconcile")
            if not progress_obj or not progress_field_reconcile:
                raise Exception(
                    "Error",
                    _(u"Si es vol pagar en paral.lel i/o contabilitzar el progres s'ha de proporcionar un "
                      u"'progress_obj' i un 'progress_field_reconcile'.")
                )
        # De moment el pagament per workers només està habilitat per remeses
        if work_async and from_payment_line:
            from oorq.oorq import setup_redis_connection
            setup_redis_connection()
            self.check_account_sequence_standard_configured(cursor, uid, context=context)
            j_pool = ProgressJobsPool(progress_obj, progress_field_reconcile, "openerp.pay_and_reconcile_multiple.progress")

        # Ara comencem a pagar.
        ctx = context.copy()
        ctx['force_different_account'] = True
        ctx['date_p'] = date
        # El counter es fa servir després per saber quines linies s'han de
        # conciliar
        counter = 0
        counter_name = str(counter) + " - "

        # Linia del total del pagament
        debit = 0
        credit = 0
        if amount_total < 0:
            debit = abs(amount_total)
        else:
            credit = amount_total
        move_line_total = {
            'name': counter_name+reference,
            'debit': debit,
            'credit': credit,
            'account_id': pay_account_id,
            'partner_id': pay_partner_id,
            'ref': reference,
            'date': date
        }
        move_lines_vs = [(0, 0, move_line_total)]
        move_of_grouped_invoices = {}
        move_lines_ids_to_reconcile = {}

        # Una linia per cada factura
        total = len(ids)
        created_moves = []
        try:
            for current_id in ids:
                counter += 1
                counter_name = str(counter) + " - "
                if from_payment_line:
                    current_info = payment_line_o.read(cursor, uid, current_id, ['move_line_id', 'amount_currency', 'account_id', 'ml_inv_ref', 'partner_id'])
                else:
                    current_info = invoice_o.read(cursor, uid, current_id, ['type', 'residual', 'partner_id'])

                if from_payment_line:
                    move_lines_to_reconcile = [current_info['move_line_id'][0]]
                else:
                    move_lines_to_reconcile = invoice_o.move_line_id_payment_get(cursor, uid, [current_id])

                mline_info = account_move_line_o.read(cursor, uid, move_lines_to_reconcile[0], ['ref', 'move_id'])
                move_line_to_reconcile_ref = mline_info['ref']
                move_id = mline_info['move_id'][0]
                move_lines_ids_to_reconcile[counter] = {
                    'current_id': current_id,
                    'mlines': move_lines_to_reconcile
                }
                if account_move_o.is_group_move(cursor, uid, move_id):
                    move_of_grouped_invoices[counter] = move_id

                if from_payment_line:
                    amount = current_info['amount_currency']
                else:
                    types = {'out_invoice': -1, 'in_invoice': 1, 'out_refund': 1, 'in_refund': -1}
                    direction = types[current_info['type']]
                    # No fa falta mirar lo de posarho en negatiu perque el residual ja porta el simbol
                    # if current_info['residual'] < 0:
                    #     direction *= -1
                    amount = direction * current_info['residual']

                debit_amount = amount if amount > 0 else 0
                credit_amount = abs(amount) if amount < 0 else 0

                if from_payment_line:
                    acc_id = current_info['account_id'][0]
                    ml_ref = current_info['ml_inv_ref']
                    if ml_ref:
                        acc_id = invoice_o.get_src_account_id_from_invoice(cursor, uid, ml_ref[0], context=ctx)
                else:
                    acc_id = invoice_o.get_src_account_id_from_invoice(cursor, uid, current_id, context=ctx)

                partner_id = current_info['partner_id'][0]
                invoice_move_line_vals = {
                    'name': counter_name+reference,
                    'debit': debit_amount,
                    'credit': credit_amount,
                    'account_id': acc_id,  # normalment 430000
                    'partner_id': partner_id,
                    'ref': move_line_to_reconcile_ref,
                    'date': date,
                }
                if cobraments_journal_id:
                    # La linia final de la factura no vindrà desde la acc_id de
                    # la factura, sino desde la conta intermitja
                    intermig_account_id = journal_o.read(cursor, uid, cobraments_journal_id, ['default_credit_account_id'])['default_credit_account_id'][0]
                    invoice_move_line_vals.update({
                        'account_id': intermig_account_id
                    })
                    # Creem les dos linies intermitjes per aquesta factura
                    avui = datetime.today().strftime("%Y-%m-%d")
                    l1 = {
                        'name': counter_name+reference,
                        'debit': debit_amount,
                        'credit': credit_amount,
                        'account_id': acc_id,  # normalment 430000
                        'partner_id': partner_id,
                        'ref': move_line_to_reconcile_ref,
                        'date': avui,
                    }
                    l2 = {
                        'name': counter_name+reference,
                        'debit': credit_amount,
                        'credit': debit_amount,
                        'account_id': intermig_account_id,  # normalment 430000
                        'partner_id': partner_id,
                        'ref': move_line_to_reconcile_ref,
                        'date': avui,
                    }
                    account_move_vals = {
                        'ref': move_line_to_reconcile_ref,
                        'journal_id': cobraments_journal_id,
                        'period_id': period_id,
                        'date': avui,
                        'line_id': [(0, 0, l1), (0, 0, l2)],
                        'partner_id': pay_partner_id
                    }
                    if j_pool:
                        account_move_o_aux = TransactionExecute(cursor.dbname, uid, 'account.move')
                        account_move_id = account_move_o_aux.create(
                            account_move_vals, context=ctx
                        )
                    else:
                        account_move_id = account_move_o.create(
                            cursor, uid, account_move_vals, context=ctx
                        )
                    created_moves.append(account_move_id)
                    account_move = account_move_o.browse(cursor, uid, account_move_id)
                    for line in account_move.line_id:
                        move_lines_ids_to_reconcile[counter]['mlines'].append(line.id)
                move_lines_vs.append((0, 0, invoice_move_line_vals))
                # Si podem, contabilitzem el progres
                if progress_field_mlines:
                    progress_obj.write({
                        progress_field_mlines: (counter * 1.0 / (total + 0.1*total)) * 100
                    })

            # Moviment amb totes les linies
            account_move_vals = {
                'ref': reference,
                'journal_id': pay_journal_id,
                'period_id': period_id,
                'date': date,
                'line_id': move_lines_vs,
                'partner_id': pay_partner_id
            }
            # Quan fem servir autoworkers per pagar factures ens asegurem de que
            # el moviment queda creat amb una transacció independent i commitada
            # perque sino els workers no veuran el move_id
            if j_pool:
                account_move_o_aux = TransactionExecute(cursor.dbname, uid, 'account.move')
                account_move_id = account_move_o_aux.create(
                    account_move_vals, context=ctx
                )
            else:
                account_move_id = account_move_o.create(
                    cursor, uid, account_move_vals, context=ctx
                )

            created_moves.append(account_move_id)
            if from_payment_line:
                # Gravem a la remesa el moviment de pagament
                order_id = payment_line_o.read(cursor, uid, ids[0], ['order_id'])['order_id'][0]
                if not j_pool:
                    self.pool.get("payment.order").write(cursor, uid, order_id, {'payment_move_id': account_move_id})
                else:
                    payment_order_o = TransactionExecute(cursor.dbname, uid, 'payment.order')
                    payment_order_o.write(order_id, {'payment_move_id': account_move_id})

            # Si podem, contabilitzem el progres
            if progress_field_mlines:
                progress_obj.write({
                    progress_field_mlines: 100.0
                })

            # Obtenim els move_lines generats al crear el move
            account_move = account_move_o.browse(cursor, uid, account_move_id)
            account_move_lines = {int(x.name.split("-")[0]): x.id for x in account_move.line_id}

            # Comencem a conciliar-ho tot
            counter = 0.0
            total = len(move_lines_ids_to_reconcile.items())
            if not j_pool:
                ctx['raise_exception'] = True
                for factura_count, data in move_lines_ids_to_reconcile.items():
                    move_group = None
                    if factura_count in move_of_grouped_invoices.keys():
                        move_group = move_of_grouped_invoices[factura_count]

                    self.process_reconcile_line_to_pay(
                        cursor, uid, data['current_id'],
                        data['mlines'] + [account_move_lines[factura_count]],
                        account_move_id, move_of_group=move_group,
                        from_payment_line=from_payment_line,
                        writeoff_acc_id=writeoff_acc_id,
                        writeoff_journal_id=writeoff_journal_id,
                        writeoff_period_id=writeoff_period_id,
                        context=ctx
                    )
                    counter += 1.0
                    # Si podem, contabilitzem el progres
                    if progress_field_reconcile:
                        progress_obj.write({
                            progress_field_reconcile: (counter / total) * 100
                        })
            else:  # if j_pool
                amax_proc = int(self.pool.get("res.config").get(cursor, uid, "pay_and_reconcile_tasks_max_procs", "0"))
                if not amax_proc:
                    amax_proc = None
                aw = AutoWorker(queue="pay_and_reconcile_multiple_tasks", default_result_ttl=24 * 3600, max_procs=amax_proc)
                for factura_count, data in move_lines_ids_to_reconcile.items():
                    move_group = None
                    if factura_count in move_of_grouped_invoices.keys():
                        move_group = move_of_grouped_invoices[factura_count]
                    j_pool.add_job(
                        self.process_reconcile_line_to_pay_async(
                            cursor, uid, data['current_id'],
                            data['mlines'] + [account_move_lines[factura_count]],
                            account_move_id, move_of_group=move_group,
                            from_payment_line=from_payment_line,
                            writeoff_acc_id=writeoff_acc_id,
                            writeoff_journal_id=writeoff_journal_id,
                            writeoff_period_id=writeoff_period_id,
                            context=ctx
                        )
                    )
                if total:
                    aw.work()
                    j_pool.join()
                # Quan fem per worker i algun falla no ens enterem i no podem
                # deixar un moviment creat amb linies sense conciliar, per tant ho
                # hem de tirar enrere tot
                failed = []
                for is_succes, affected_id in j_pool.results.values():
                    if not is_succes:
                        failed.append(affected_id)
                if len(failed) or len(j_pool.failed_jobs) > 0:
                    self.clean_moves(cursor, uid, created_moves, context=context)
                # Ens asegurem que quedi registrat com que ha acavat
                progress_obj.write({
                    progress_field_reconcile: 100.0
                })
                return failed
        except Exception, e:
            if j_pool:
                # Quan es paga per workers hem creat moviments amb transaction execute que ara s'haurien de netejar.
                self.clean_moves(cursor, uid, created_moves, context=context)
            raise e
        return []

    @job(queue="pay_and_reconcile_multiple_tasks")
    def process_reconcile_line_to_pay_async(
            self, cursor, uid, current_id, move_lines, payment_move,
            move_of_group=None, from_payment_line=False, writeoff_acc_id=None,
            writeoff_period_id=None, writeoff_journal_id=None, context=None
    ):
        return self.process_reconcile_line_to_pay(
            cursor, uid, current_id, move_lines, payment_move,
            move_of_group=move_of_group,
            from_payment_line=from_payment_line,
            writeoff_acc_id=writeoff_acc_id,
            writeoff_journal_id=writeoff_journal_id,
            writeoff_period_id=writeoff_period_id,
            context=context
        )

    def process_reconcile_line_to_pay(
            self, cursor, uid, current_id, move_lines, payment_move,
            move_of_group=None, from_payment_line=False, writeoff_acc_id=None,
            writeoff_period_id=None, writeoff_journal_id=None, context=None
    ):
        try:
            if context is None:
                context = {}

            account_move_line_o = self.pool.get('account.move.line')
            payment_line_o = self.pool.get('payment.line')

            if move_of_group:
                res = account_move_line_o.reconcile_lines_of_group_move(
                    cursor, uid, move_of_group, move_lines, context=context
                )
            else:
                res = account_move_line_o.reconcile_lines(
                    cursor, uid, move_lines, 'manual', writeoff_acc_id,
                    writeoff_period_id, writeoff_journal_id, context=context
                )
            # Cridem el pay_and_reconcile per cada factura perque els altres
            # métodes que s'esperen que es cridi quan es paga una factura s'executin
            if from_payment_line:
                payment_line_wv = {'payment_move_id': payment_move}
                payment_line_o.write(
                    cursor, uid, current_id, payment_line_wv, context=context
                )
                inv_id = payment_line_o.read(cursor, uid, current_id, ['ml_inv_ref'])['ml_inv_ref']
                if inv_id:
                    inv_id = inv_id[0]
            else:
                inv_id = current_id
            if inv_id:
                self.pay_and_reconcile(
                    cursor, uid, [inv_id], 0.0, None, None, None, None, None, None,
                    context=context
                )
            return True, current_id
        except Exception as e:
            if context.get("raise_exception"):
                raise e
            else:
                return False, current_id

    def clean_moves(self, cursor, uid, move_ids, context=None):
        if context is None:
            context = {}
        if not isinstance(move_ids, (list, tuple)):
            move_ids = [move_ids]
        tmp_cr = pooler.get_db_only(cursor.dbname).cursor()
        move_o = self.pool.get("account.move")
        paymentl_o = self.pool.get("payment.line")
        for move in move_o.browse(tmp_cr, uid, move_ids):
            move.write({'state': 'draft'})
            for line in move.line_id:
                if line.reconcile_id:
                    line.reconcile_id.unlink()
                if line.reconcile_partial_id:
                    line.reconcile_partial_id.unlink()
            pid = paymentl_o.search(tmp_cr, uid, [('payment_move_id', '=', move.id)])
            if pid:
                paymentl_o.write(tmp_cr, uid, pid, {'payment_move_id': False})
        move_o.unlink(tmp_cr, uid, move_ids)
        tmp_cr.commit()
        tmp_cr.close()
        return True

    def pay_from_n57(self, cursor, uid, ids, amount, pay_journal_id=None,
                     context=None):
        if context is None:
            context = {}
        journal_obj = self.pool.get('account.journal')
        period_obj = self.pool.get('account.period')
        for invoice in self.browse(cursor, uid, ids, context=context):
            residual = amount_grouped(invoice)
            if amount != residual:
                return {
                    'state': 'error',
                    'notes': _(u'N57 amount {0} not match invoice amount: {1}').format(
                        amount, residual
                    )
                }
            if pay_journal_id is None:
                pay_journal_id = journal_obj.search(cursor, uid, [
                    ('code', '=', 'CAJA')
                ])[0]
            journal = journal_obj.browse(cursor, uid, pay_journal_id)
            pay_account_id = journal.default_credit_account_id.id
            period_id = period_obj.find(cursor, uid, context['date_p'])[0]
            writeoff_acc_id = False
            writeoff_period_id = False
            writeoff_journal_id = False
            name = context.get('norma_57_name', False) or 'Norma57 payment'
            invoice.pay_and_reconcile(
                amount,
                pay_account_id,
                period_id,
                pay_journal_id,
                writeoff_acc_id,
                writeoff_period_id,
                writeoff_journal_id,
                context,
                name
            )
        return {'state': 'confirmed'}

    def _payment_type_selection(self, cursor, uid, context=None):
        """Construïm un seleccionable segons tots els tipus de pagament
        que hi ha.
        """
        selection = []
        payment_type_obj = self.pool.get('payment.type')
        payment_type_ids = payment_type_obj.search(cursor, uid, [])
        for payment_type in payment_type_obj.read(cursor, uid, payment_type_ids,
                                                  ['id', 'name']):
            selection.append((payment_type['id'], payment_type['name']))
        return selection

    def _ff_lang_partner(self, cr, uid, ids, field, arg, context=None):
        ''' Retorna l'idioma del partner (client) '''
        if not context:
            context = {}
        factures = self.browse(cr, uid, ids)
        return dict([(x.id, x.partner_id.lang) for x in factures])

    def _lang_partner_search(self, cr, uid, obj, name, arg, context=None):
        partner_obj = self.pool.get('res.partner')
        # Partners amb l'idioma escollit
        if arg[0][2] == '*':
            # Sense idioma definit
            search_param = [('lang', 'in', [False, ''])]
        else:
            search_param = [('lang', arg[0][1], arg[0][2])]
        partners = partner_obj.search(cr, uid,
            search_param)
        return [('partner_id', 'in', partners)]

    def _get_langs_sel(self, cr, uid, context):
        res = []
        lang_obj = self.pool.get('res.lang')
        langs = lang_obj.read(cr, uid, lang_obj.search(cr, uid, []),
            ['code', 'name'])
        for l in langs:
            res.append((l['code'], l['name']))
        res.append(('*', _('Sense Idioma')))
        return res

    _columns = {
        'partner_id': fields.many2one('res.partner', 'Partner',
                                      select=True,
                                      change_default=True,
                                      readonly=True,
                                      required=True,
                                      states={'draft': [('readonly', False)]}),
        'payment_type': fields.many2one('payment.type', 'Payment type',
                                        selection=_payment_type_selection),
        'lang_partner': fields.function(_ff_lang_partner,
                            fnct_search=_lang_partner_search, type='char',
                            size='5', string="Idioma Client", method=True),
        'lang_partner_sel': fields.function(_ff_lang_partner,
                                            selection=_get_langs_sel,
                        fnct_search=_lang_partner_search, type='selection',
                        string="Idioma Client", method=True, size='5'),
        'group_move_id': fields.many2one('account.move', 'Group move', select=True, ondelete="set null")
    }

    _order = "id desc"

AccountInvoice()
