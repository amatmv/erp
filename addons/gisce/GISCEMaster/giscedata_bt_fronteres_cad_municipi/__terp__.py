# -*- coding: utf-8 -*-
{
    "name": "GISCE Data Fronteres CAD",
    "description": """Funcions per les eines CAD als BT Fronteres""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "CAD",
    "depends":[
        "giscedata_bt_fronteres"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
