# -*- coding: utf-8 -*-
{
    "name": "Subtipus Reclamacions",
    "description": """Aquest mòdul crea el model per els subtipus de reclamacions
  utilitzats en ATR i ATC i afageix les dades dels subtipus a la base de dades.""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends": [
        "base",
        "crm",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_subtipus_reclamacio_data.xml",
        "giscedata_subtipus_reclamacio_view.xml",
        "security/giscedata_subtipus_reclamacio_security.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
