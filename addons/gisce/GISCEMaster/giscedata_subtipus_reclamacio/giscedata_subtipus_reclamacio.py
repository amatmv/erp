# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscedataSwitchingSubtipus(osv.osv):
    ''' Tabla 82 From CNMC Gestión ATR documentation'''

    _name = 'giscedata.subtipus.reclamacio'

    def get_subtipus(self, cursor, uid, code, context=None):
        search_params = [('name', '=', code)]
        subtipus_ids = self.search(cursor, uid, search_params)
        if subtipus_ids:
            return subtipus_ids[0]
        return False

    _columns = {
        'name': fields.char(u'Codi', size=3),
        'desc': fields.char(u'Descripció', size=240),
        'type': fields.char(u'Tipus', size=2),
        'sector': fields.selection([('e', 'Electricitat'), ('g', 'Gas'), ('t', 'Tots')], "Sector"),
        'circular': fields.boolean("S'informa a la circular d'arencio al client"),
        'atr': fields.boolean("S'utilitza en processos ATR")
    }

    _defaults = {
        'sector': lambda *a: 't',
        'circular': lambda *a: True,
        'atr': lambda *a: True,
    }


GiscedataSwitchingSubtipus()
