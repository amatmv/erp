# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _
from datetime import datetime


class CrmCase(osv.osv):

    _name = "crm.case"
    _inherit = "crm.case"

    def case_pending(self, cr, uid, ids, *args):
        res = super(CrmCase, self).case_pending(cr, uid, ids, args)
        self.write(cr, uid, ids, {'pending_date': datetime.now().strftime("%Y-%m-%d %H:%M:%S")})
        return res

    _columns = {
        'pending_date': fields.datetime('Pendent', readonly=True),
    }

CrmCase()


class GiscedataOt(osv.OsvInherits):

    _name = "giscedata.ot"
    _inherits = {"crm.case": "crm_id"}
    _order = "id desc"

    def onchange_operator_partner_id(self, cursor, uid, ids, partner_id):
        if not partner_id:
            return {'value': {'operator_id': False}}
        address_obj = self.pool.get('res.partner.address')
        addres_ids = address_obj.search(
            cursor, uid, [('partner_id', '=', partner_id)]
        )
        if len(addres_ids) == 1:
            return {'value': {'operator_id': addres_ids[0]}}
        else:
            return {'value': {'operator_id': False}}

    def onchange_polissa_id(self, cursor, uid, ids, polissa_id):
        if not polissa_id:
            return {'value': {'cups_id': False, 'partner_id': False,
                              'partner_address_id': False}}
        polissa_obj = self.pool.get('giscedata.polissa')
        polissa = polissa_obj.browse(cursor, uid, polissa_id)
        partner_address_id = False
        address_obj = self.pool.get('res.partner.address')
        addres_ids = address_obj.search(
            cursor, uid, [('partner_id', '=', polissa.titular.id)]
        )
        if len(addres_ids) == 1:
            partner_address_id = addres_ids[0]

        return {'value': {'cups_id': polissa.cups.id,
                          'partner_id': polissa.titular.id,
                          'partner_address_id': partner_address_id}}

    def _get_default_section(self, cursor, uid, context=None):
        imd_obj = self.pool.get('ir.model.data')
        section_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_ot', 'crm_case_section_ot'
        )
        return section_id[1]

    def create_ot_case_from(self, cursor, uid, model_ids, context=None,
                            description=None,  section='CASE',
                            category=None,  assign=None, extra_vals=None,
                            empresa=None):
        if not context:
            context = {}
        if not extra_vals:
            extra_vals = {}

        section_obj = self.pool.get('crm.case.section')
        categ_obj = self.pool.get('crm.case.categ')
        user_obj = self.pool.get('res.users')
        ot_obj = self.pool.get("giscedata.ot")
        # Search the section
        section_id = extra_vals.get('section_id')
        if section_id is None:
            search_params = [('code', '=', section)]
            section_ids = section_obj.search(cursor, uid, search_params)
            if len(section_ids) == 1:
                section_id = section_ids[0]
            else:
                raise osv.except_osv(_(u"Error"),
                                     _(u"Section %s not found" % section))

        ot_type = ot_obj.calc_ot_type(cursor, uid, section_id, context=context)
        # Search the category
        search_params = [('categ_code', '=', category),
                         ('section_id', '=', section_id)]
        categ_ids = categ_obj.search(cursor, uid, search_params)
        if len(categ_ids) == 1:
            categ_id = categ_ids[0]
        else:
            categ_id = None

        # Search the user
        search_params = [('login', '=', assign)]
        user_ids = user_obj.search(cursor, uid, search_params)
        if len(user_ids) == 1:
            user_id = user_ids[0]
        else:
            user_id = None

        case_ids = []

        section_desc = section_obj.read(cursor, uid, section_id, ["name"])
        vals = {'name': description,
                'description': description,
                'section_id': section_id,
                'categ_id': categ_id,
                'user_id': user_id,
                'operator_partner_id': empresa.id,
                'ot_type': ot_type
                }
        # Update vals with extra_vals values
        vals.update(extra_vals)
        model = context.get('model', False)
        if model:
            # Search if we can associate the model with a case
            link_obj = self.pool.get('res.request.link')
            search_params = [('object', '=', '%s' % model)]
            link_ids = link_obj.search(cursor, uid, search_params)
            if not link_ids:
                raise osv.except_osv(_(u"Error"),
                                     _(u"Model %s cannot be associated"
                                       u" to a CRM case" % (model)))

            model_obj = self.pool.get(model)
            for objecte in model_obj.browse(cursor, uid, model_ids, context=context):
                vals.update({'ref': '%s,%i' % (model, objecte.id)})
                if model == 'giscedata.polissa':
                    partner_address_id = None
                    address_obj = self.pool.get('res.partner.address')
                    addres_ids = address_obj.search(
                        cursor, uid, [('partner_id', '=', objecte.titular.id)]
                    )
                    if len(addres_ids) == 1:
                        partner_address_id = addres_ids[0]

                    ot_name = "{0} - {1} - {2}".format(
                        section_desc["name"],
                        objecte.cups.name,
                        objecte.codi
                    )
                    vals.update({
                        'name': ot_name,
                        'polissa_id': objecte.id,
                        'cups_id': objecte.cups.id,
                        'partner_id': objecte.titular.id,
                        'partner_address_id': partner_address_id,
                    })
                elif model == 'giscedata.switching':
                    partner_address_id = None
                    address_obj = self.pool.get('res.partner.address')
                    addres_ids = address_obj.search(
                        cursor, uid, [('partner_id', '=', objecte.titular_polissa.id)]
                    )
                    if len(addres_ids):
                        partner_address_id = addres_ids[0]

                    # Generate custom name
                    ot_name = "{0} - {1} - {2}".format(
                        section_desc["name"],
                        objecte.cups_id.name,
                        objecte.codi_sollicitud
                    )

                    vals.update({
                        'name': ot_name,
                        'polissa_id': objecte.cups_polissa_id.id,
                        'cups_id': objecte.cups_id.id,
                        'partner_id': objecte.titular_polissa.id,
                        'partner_address_id': partner_address_id,
                    })

                case_ids.append(self.create(cursor, uid, vals, context))
        else:
            case_ids.append(self.create(cursor, uid, vals, context))
        return case_ids

    def _get_user_from_op(self, cr, uid, ids, field_name, ar, context=None):
        if context is None:
            context = {}
        res = {}
        user_obj = self.pool.get('res.users')
        usr_addr = user_obj.read(cr, uid, uid, ['address_id'])['address_id']
        if not usr_addr:
            return res
        usr_addr = usr_addr[0]
        operator_partners_ids = self.read(cr, uid, ids, ['operator_partner_id'])
        address_obj = self.pool.get('res.partner.address')
        for op_id in operator_partners_ids:
            op_address_ids = address_obj.search(
                cr, uid, [('partner_id', '=', op_id['operator_partner_id'][0])]
            )
            res[op_id['id']] = usr_addr in op_address_ids
        return res

    def _ff_user_from_op(self, cr, uid, obj, name, args, context):
        user_obj = self.pool.get('res.users')
        groups_obj = self.pool.get("res.groups")
        address_obj = self.pool.get('res.partner.address')

        otmanager_group = groups_obj.search(
            cr, uid, [('name', '=', 'GISCEDATA OT / Manager')]
        )[0]
        user_groups = user_obj.read(cr, uid, uid, ['groups_id'])['groups_id']
        # If user is OT Manager, show all OTs
        if otmanager_group in user_groups:
            return [(True, '=', True)]

        # Get the partner from address of uid
        usr_info = user_obj.read(cr, uid, uid, ['address_id'])
        usr_addr = usr_info['address_id']
        if not usr_addr:  # If no address, don't show any OT
            return [('operator_partner_id', '=', -1)]
        usr_addr = usr_addr[0]
        partner_info = address_obj.read(cr, uid, usr_addr, ['partner_id'])
        return [('operator_partner_id', '=', partner_info['partner_id'][0])]

    def onchange_section_id(self, cursor, uid, ids, section_id, polissa_id,
                            cups_id, context=None):
        if not section_id:
            return {'value': {}}
        sect_obj = self.pool.get("crm.case.section")
        pol_obj = self.pool.get("giscedata.polissa")
        cups_obj = self.pool.get("giscedata.cups.ps")
        ot_obj = self.pool.get("giscedata.ot")

        new_section_info = sect_obj.read(cursor, uid, section_id, ['name', 'code'])
        sect_name = new_section_info['name']

        polissa = ""
        cups = ""
        if polissa_id:
            pol_info = pol_obj.read(cursor, uid, polissa_id, ['name', 'codi'])
            polissa = " - {0}".format(pol_info['name'] or pol_info['codi'])
        if cups_id:
            cups_info = cups_obj.read(cursor, uid, cups_id, ['name'])
            cups = " - {0}".format(cups_info['name'])

        name = "{0}{1}{2}".format(sect_name, cups, polissa)
        ot_type = ot_obj.calc_ot_type(cursor, uid, section_id, context=context)
        return {'value': {'name': name, 'ot_type': ot_type}}

    _STORE_OP_USER_ID = {
        'giscedata.ot': (lambda self, cr, uid, ids, c=None: ids,
                         ['operator_partner_id'], 10)
    }

    def check_work_date(self, cursor, uid, ids):
        for ot_info in self.read(cursor, uid, ids, ['work_date']):
            if ot_info['work_date']:
                wdate = datetime.strptime(ot_info['work_date'], "%Y-%m-%d %H:%M:%S")
                if wdate > datetime.now().replace(hour=23, minute=59, second=59):
                    return False
        return True

    def _ot_types(self, cursor, uid, context=None):
        return [('', '')]

    _constraints = [
        (check_work_date, _(u"La data de realització no pot ser posterior a avui."),
         ["work_date"]),
    ]

    # CRM.CASE WORKFLOW
    # -----------------
    #
    # Must be re-called with the CRM_ID field in order to extend them

    def case_open(self, cr, uid, ids, *args):
        crm_obj = self.pool.get('crm.case')
        crm_ids = [
            ot_data['crm_id'][0]
            for ot_data in self.read(cr, uid, ids, ['crm_id'])
        ]
        crm_obj.case_open(cr, uid, crm_ids, args)

    def case_close(self, cr, uid, ids, *args):
        crm_obj = self.pool.get('crm.case')
        crm_ids = [
            ot_data['crm_id'][0]
            for ot_data in self.read(cr, uid, ids, ['crm_id'])
        ]
        crm_obj.case_close(cr, uid, crm_ids, args)

    def case_cancel(self, cr, uid, ids, *args):
        crm_obj = self.pool.get('crm.case')
        crm_ids = [
            ot_data['crm_id'][0]
            for ot_data in self.read(cr, uid, ids, ['crm_id'])
        ]
        crm_obj.case_cancel(cr, uid, crm_ids, args)

    def case_pending(self, cr, uid, ids, *args):
        crm_obj = self.pool.get('crm.case')
        crm_ids = [
            ot_data['crm_id'][0]
            for ot_data in self.read(cr, uid, ids, ['crm_id'])
        ]
        crm_obj.case_pending(cr, uid, crm_ids, args)

    def case_reset(self, cr, uid, ids, *args):
        crm_obj = self.pool.get('crm.case')
        crm_ids = [
            ot_data['crm_id'][0]
            for ot_data in self.read(cr, uid, ids, ['crm_id'])
        ]
        crm_obj.case_reset(cr, uid, crm_ids, args)

    def case_escalate(self, cr, uid, ids, *args):
        crm_obj = self.pool.get('crm.case')
        crm_ids = [
            ot_data['crm_id'][0]
            for ot_data in self.read(cr, uid, ids, ['crm_id'])
        ]
        crm_obj.case_escalate(cr, uid, crm_ids, args)

    def calc_ot_type(self, cursor, uid, section_id, context=None):
        return ''

    _columns = {
        'crm_id': fields.many2one('crm.case', required=True),
        'operator_partner_id': fields.many2one('res.partner',
                                               'Empresa Encarregada', select=1,
                                               required=True),
        'is_user_from_op': fields.function(_get_user_from_op,
                                           method=True, type='boolean',
                                           fnct_search=_ff_user_from_op),
        'operator_id': fields.many2one('res.partner.address', 'Operari'),
        'work_date': fields.datetime('Data Realització'),
        'work_period_init': fields.datetime('Inici periode realització'),
        'work_period_end': fields.datetime('Final periode realització'),
        'work_done': fields.boolean('Treball Realitzat',
                                    help="Indica si s'ha realitzat la tasca o "
                                         "per el contrari no ha estat n"
                                         "ecessari"),
        'work_in_progres': fields.boolean(string='Operari en camp'),
        'cups_id': fields.many2one('giscedata.cups.ps', 'CUPS', size=22),
        'cups_address': fields.related('cups_id', 'direccio', type='char',
                                       relation='giscedata.cups.ps',
                                       string="Direcció CUPS", store=False,
                                       readonly=True, size=300),
        'cups_et': fields.related('cups_id', 'et', type='char',
                                  relation='giscedata.cups.ps',
                                  string="CT CUPS", store=False,
                                  readonly=True, size=300),
        'cups_name': fields.related('cups_id', 'name', type='char',
                                    relation='giscedata.cups.ps',
                                    string="CUPS", store=False,
                                    readonly=True, size=100),
        'polissa_name': fields.related('polissa_id', 'name', type='char',
                                       relation='giscedata.polissa',
                                       string="Abonat", store=False,
                                       readonly=True, size=100),
        'operator_partner_name': fields.related('operator_partner_id', 'name',
                                                type='char',
                                                relation='res.partner',
                                                string='Empresa Encarregada',
                                                store=False, readonly=True,
                                                size=100),

        'operator_name': fields.related('operator_id', 'name', type='char',
                                        relation='res.partner.address',
                                        string='Operari',
                                        store=False, readonly=True, size=100),
        'ot_partner_name': fields.related('partner_id', 'name', type='char',
                                       relation='res.partner',
                                       string="Titular Suministre", store=False,
                                       readonly=True, size=100),
        'partner_addr_name': fields.related('partner_address_id', 'name',
                                            type='char', store=False,
                                            relation='res.partner.address',
                                            string="Contacte Titular",
                                            readonly=True, size=100),
        'partner_telefon': fields.related('partner_address_id', 'phone',
                                          type='char', store=False,
                                          relation='res.partner.address',
                                          string="Telèfon Titular",
                                          readonly=True, size=100),
        'ot_partner_mobile': fields.related('partner_address_id', 'mobile',
                                          type='char', store=False,
                                          relation='res.partner.address',
                                          string="Mobil Titular",
                                          readonly=True, size=100),
        'user_name': fields.related('user_id', 'name', type='char', store=False,
                                    relation='res.users', string="Responsable",
                                    readonly=True, size=100),
        'ot_type': fields.selection(_ot_types, string='Tipus de OT'),
    }

    _defaults = {
        'section_id': _get_default_section,
    }

GiscedataOt()
