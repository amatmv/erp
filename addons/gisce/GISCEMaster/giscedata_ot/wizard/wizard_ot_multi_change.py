# -*- coding: utf-8 -*-

from osv import osv


class WizardSwitchingMultiChange(osv.osv_memory):
    _name = 'wizard.ot.multi.change'
    _inherit = 'wizard.crm.multi.change'

    def _default_atr_output_text(self, cursor, uid, context=None):
        if not context:
            context = {}

        ot_obj = self.pool.get('giscedata.ot')

        ot_ids = context.get('active_ids', [])
        crm_ids = [x['crm_id'][0] for x in ot_obj.read(cursor, uid, ot_ids)]
        ctx = context.copy()
        ctx.update({
            'active_ids': crm_ids
        })

        return self._default_output_text(cursor, uid, context=ctx)

    def perform_ot_change(self, cursor, uid, ids, context=None):
        if not context:
            context = {}

        ot_obj = self.pool.get('giscedata.ot')

        ot_ids = context.get('active_ids', [])
        crm_ids = [x['crm_id'][0] for x in ot_obj.read(cursor, uid, ot_ids)]

        ctx = context.copy()
        ctx.update({
            'active_ids': crm_ids
        })

        self.perform_change(
            cursor, uid, ids, context=ctx
        )

    _defaults = {
        'result': _default_atr_output_text
    }

WizardSwitchingMultiChange()
