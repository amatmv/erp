# -*- coding: utf-8 -*-

from osv import fields, osv
from tools.translate import _

from datetime import datetime


class WizardOperariCamp(osv.osv_memory):
    """
    Wizard per a l'activació de Ordres de Treball.
    """
    _name = 'wizard.operari.camp'

    def action_operari_camp(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        ot_obj = self.pool.get('giscedata.ot')
        wiz = self.browse(cursor, uid, ids[0], context)
        ot_obj.write(
            cursor, uid, context.get('active_ids', []),
            {'work_in_progres': wiz.operari_camp}
        )
        wiz.write({'state': 'done'})
        return True

    _columns = {
        'operari_camp': fields.boolean(string='Operari en camp'),
        'state': fields.selection([('init', 'Init'),
                                   ('fail', 'Fail'),
                                   ('done', 'Done')], 'State')
    }

    _defaults = {
        'state': lambda *a: 'init',
    }


WizardOperariCamp()
