# -*- coding: utf-8 -*-

from osv import osv, fields


class WizardListATRCases(osv.osv_memory):
    """Wizard per a veure els casos ATR associats a aquesta ordre de treball"""
    _name = 'wizard.list.atr.cases'

    def action_get_atr_cases(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        wiz = self.browse(cursor, uid, ids[0], context=context)
        ot = wiz.ot_id
        casos = []
        refer_dict = ot.ref and ot.ref.split(',') or ','
        if refer_dict[0] == 'giscedata.switching':
            casos.append(refer_dict[1])
        refer_dict = ot.ref2 and ot.ref2.split(',') or ','
        if refer_dict[0] == 'giscedata.switching':
            casos.append(refer_dict[1])
        return {
            'name': 'Casos associats a la ordre de treball',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.switching',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': "[('id', 'in', %s)]" % str(tuple(casos)),
        }

    _columns = {
        'ot_id': fields.many2one('giscedata.ot', 'OT', required=True),
    }

WizardListATRCases()
