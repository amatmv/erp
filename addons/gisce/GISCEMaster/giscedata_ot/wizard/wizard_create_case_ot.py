# -*- encoding: utf-8 -*-
from osv import osv
from osv import fields
from datetime import datetime
from tools.translate import _
import json


class WizardCreateCaseOt(osv.osv_memory):

    _name = 'wizard.create.ot'
    _inherit = 'wizard.create.case'

    def get_sw_comments(self, cursor, uid, sw_id, context=None):
        sw_obj = self.pool.get("giscedata.switching")
        objecte = sw_obj.browse(cursor, uid, sw_id, context=context)
        # Generate custom description (comments)
        generated_comments = objecte.additional_info

        model_01 = "giscedata.switching.{0}.01".format(
            objecte.proces_id.name.lower()
        )
        pas01_obj = self.pool.get(model_01)
        pas01_id = pas01_obj.search(
            cursor, uid, [("sw_id", "=", objecte.id)]
        )
        pas01 = pas01_obj.browse(cursor, uid, pas01_id[0])
        if pas01.comentaris:
            generated_comments = "{0}\n\n{1}".format(pas01.comentaris, generated_comments)
        return generated_comments

    def action_case_generic(self, cursor, uid, ids, context=None):

        wizard = self.browse(cursor, uid, ids[0])
        case_obj = self.pool.get('giscedata.ot')
        sw_obj = self.pool.get("giscedata.switching")
        wiz_create_step_obj = self.pool.get("wizard.create.step")

        if not context:
            context = {}

        extra_vals = {
            'date': datetime.today().strftime('%Y-%m-%d'),
            'section_id': wizard.section_id.id,
            'date_deadline': False
        }

        case_ids = []
        for model_id in context.get('active_ids', []):
            ldate = False
            generated_comments = wizard.comments
            if context.get('model', False) == "giscedata.switching":
                if wizard.create_atr_accept_step:
                    wiz_id = wiz_create_step_obj.create(
                        cursor, uid, {}, context={'active_ids': [model_id]}
                    )
                    wiz_create_step_obj.write(
                        cursor, uid, [wiz_id], {'step': '02'}
                    )
                    wiz_create_step_obj.action_create_steps(
                        cursor, uid, [wiz_id], context={'active_ids': [model_id]}
                    )

                ldate = sw_obj.get_deadline_for_step(
                    cursor, uid, model_id, context=context
                )
                if len(context.get('active_ids', [])) > 1:
                    generated_comments = "{0}\n{1}".format(
                        generated_comments, self.get_sw_comments(cursor, uid, model_id, context=context)
                    )

            extra_vals.update({'date_deadline': wizard.date or ldate})

            case_id = case_obj.create_ot_case_from(
                cursor, uid, [model_id], context=context,
                section=wizard.section_id.code, description=generated_comments,
                category=wizard.categ_id and wizard.categ_id.categ_code or None,
                assign=wizard.user_id.login, extra_vals=extra_vals,
                empresa=wizard.operator_partner_id
            )[0]

            if context.get('model', False) == "giscedata.switching":
                sw_obj.write(cursor, uid, model_id, {
                    'state': 'pending',
                    'ref2': 'giscedata.ot,{}'.format(case_id)
                })
            case_ids.append(case_id)

        self.write(cursor, uid, ids, {'state': 'end', 'case_ids': json.dumps(case_ids)})

    def action_open_cases(self, cursor, uid, ids, context=None):
            case_ids = self.read(cursor, uid, ids[0], ['case_ids'])[0]['case_ids']
            case_ids = json.loads(case_ids)
            return {
                'name': _(u'OTs'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'giscedata.ot',
                'view_id': False,
                'type': 'ir.actions.act_window',
                'domain': [('id', 'in', case_ids)]
            }

    def _default_comments(self, cursor, uid, context=None):
        if context.get('model', False) == "giscedata.switching" and len(context.get('active_ids', [])) == 1:
            return self.get_sw_comments(cursor, uid, context.get('active_ids', [])[0], context=context)
        else:
            return ""

    _columns = {
        'operator_partner_id': fields.many2one('res.partner',
                                               'Empresa Encarregada',
                                               required=True),
        'state': fields.selection([('init', 'Init'), ('end', 'End')], 'State'),
        'case_ids': fields.text(),
        'comments': fields.text('Comments'),
        'date': fields.datetime(
            string="Fecha Limite",
            help=u"Quan es crea desde un cas ATR, deixar buit per agafar la data limit del cas."
        ),
        'create_atr_accept_step': fields.boolean(
            string=u"Generar pasos d'acceptació ATR",
            help=u"Al generar les OTs generar els apssos d'acceptació "
                 u"(02 i 11 per Cn) dels casos ATR"
        )
    }

    _defaults = {
        'state': lambda *a: 'init',
        'create_atr_accept_step': lambda *a: True,
        'comments': _default_comments
    }

WizardCreateCaseOt()
