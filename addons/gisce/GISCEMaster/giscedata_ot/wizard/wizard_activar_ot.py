# -*- coding: utf-8 -*-

from osv import fields, osv
from tools.translate import _

from datetime import datetime


class WizardActivarOT(osv.osv_memory):
    """
    Wizard per a l'activació de Ordres de Treball.
    """
    _name = 'wizard.activar.ot'

    def _default_info(self, cursor, uid, context=None):
        return _(u"Es tancaran les Ordres de Treball.")

    def action_active_ot(self, cursor, uid, wizard_id, context=None):
        """
        Activa la OT copiant les lectures i els comptadors a la
        pòlissa associada.
        """
        if context is None:
            context = {}
        if isinstance(wizard_id, list):
            wizard_id = wizard_id[0]
        ot_obj = self.pool.get('giscedata.ot')
        wiz = self.browse(cursor, uid, wizard_id)
        info = ""
        for active_id in context.get('active_ids', []):
            current_ot = ot_obj.browse(cursor, uid, active_id, context)
            try:
                current_ot.case_close()
            except Exception as err:
                info += _(u"No s'ha pogut tancar la OT {}({}): {}").format(
                    current_ot.id, current_ot.name, err.message
                )
        wiz.write({
            'info': info,
            'state': 'done'
        })
        return

    _columns = {
        'info': fields.text('Info'),
        'state': fields.selection([('init', 'Init'),
                                   ('fail', 'Fail'),
                                   ('done', 'Done')], 'State')
    }

    _defaults = {
        'state': lambda *a: 'init',
        'info': _default_info,
    }


WizardActivarOT()
