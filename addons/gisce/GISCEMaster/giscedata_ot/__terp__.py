# -*- coding: utf-8 -*-
{
    "name": "GISCE Ordres de Treball",
    "description": """Ordres de Treball.""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "c2c_webkit_report",
        "crm",
        "crm_multi_close",
        "giscedata_cups_distri",
        "giscedata_tensions",
        "giscedata_polissa_crm",
        "giscedata_switching_distri"
    ],
    "init_xml": [],
    "demo_xml": ['giscedata_ot_demo.xml'],
    "update_xml":[
        "security/giscedata_ot_security.xml",
        "security/ir.model.access.csv",
        "giscedata_ot_data.xml",
        "giscedata_ot_view.xml",
        "wizard/wizard_activar_ot_view.xml",
        "wizard/wizard_create_case_ot_view.xml",
        "wizard/wizard_list_atr_cases_view.xml",
        "wizard/wizard_ot_multi_change_view.xml",
        "wizard/wizard_operari_camp_view.xml"
    ],
    "active": False,
    "installable": True
}
