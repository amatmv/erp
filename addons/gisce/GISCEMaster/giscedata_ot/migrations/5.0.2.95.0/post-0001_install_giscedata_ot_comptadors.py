# coding=utf-8
import logging
from oopgrade import oopgrade


def up(cursor, installed_version):
    if installed_version:
        logger = logging.getLogger('openerp.migration')

        logger.info("Update ir_model_data: model_giscedata_ot_tancament, model_giscedata_ot_lectura, model_giscedata_ot_comptador, model_giscedata_ot_potencia', 'model_wizard_new_comptador_ot have been moved to 'giscedata_ot_comptadors'")
        cursor.execute("""
            UPDATE ir_model_data
            SET module = 'giscedata_ot_comptadors'
            WHERE name in (
            'model_giscedata_ot_tancament', 
            'model_giscedata_ot_lectura', 
            'model_giscedata_ot_comptador', 
            'model_giscedata_ot_potencia',
            'model_wizard_new_comptador_ot')
        """)

        logger.info('Install the module giscedata_ot_comptadors where all the logic of comptadors has been moved.')
        oopgrade.install_modules(cursor, 'giscedata_ot_comptadors')
        logger.info('Migration successful!')


def down(cursor, installed_version):
    pass


migrate = up
