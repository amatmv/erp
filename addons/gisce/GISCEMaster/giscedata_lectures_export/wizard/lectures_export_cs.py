# -*- coding: utf-8 -*-

import wizard
import pooler
import calendar
import csv
import StringIO
import base64
import bz2

def _triar_mes(self, cr, uid, data, context={}):
  return {
    'compressed': 0,
    'potencia_min': 0.0,
    'potencia_max': 10000.0,
  }


def _selection_mes(self, cr, uid, context={}):
  cr.execute("select distinct to_char(name, 'MM/YYYY') as mes, to_char(name, 'YYYYMM') as ts from giscedata_lectures_lectura order by ts desc")
  return [(a[0], a[0]) for a in cr.fetchall()]

_triar_mes_form = """<?xml version="1.0"?>
<form string="Exportación Lecturas CS" col="4">
  <field name="mes" required="1"/>
  <newline />
  <field name="potencia_min" />
  <field name="potencia_max" />
  <field name="compressed" />
</form>"""

_triar_mes_fields = {
  'mes': {'string': 'Mes a exportar', 'type': 'selection', 'selection': _selection_mes},
  'compressed': {'string': 'Comprimido (BZip2)', 'type': 'boolean'},
  'potencia_min': {'string': 'Potencia mínima (>=)', 'type': 'float', 'digits': (16,3), 'required': True},
  'potencia_max': {'string': 'Potencia máxima (<=)', 'type': 'float', 'digits': (16,3), 'required': True},
}

def _export(self, cr, uid, data, context={}):
  
  mes,any = data['form']['mes'].split('/')

  polissa_obj = pooler.get_pool(cr.dbname).get('giscedadta.polissa')
  tarifa_obj = pooler.get_pool(cr.dbname).get('giscedata.polissa.tarifa')
  lectura_obj = pooler.get_pool(cr.dbname).get('giscedata.lectures.lectura')
  comptador_obj = pooler.get_pool(cr.dbname).get('giscedata.lectures.comptador')

  # Per cada periode que no sigui del tipus 6? posar aquí el filtre
  tarifes_to_export = {
    '2.0A': '0',
    '2.0DHA': '1',
    '2.1A': '0',
    '2.1DHA': '1',
    '3.0A': '2',
    '3.1A': '3',
  }

  
  periodes = {}
  tarifa_search = [
    ('name', 'in', tarifes_to_export.keys()),
  ]
  for tarifa_id in tarifa_obj.search(cr, uid, tarifa_search):
    periodes[tarifa_id] = []
    for periode in tarifa_obj.browse(cr, uid, tarifa_id).periodes:
      if periode.tipus == 'te':
        periodes[tarifa_id].append(periode.id)


  # Busquem totes les lectures que tenen aquest periode i que la data esta en el mes aquest
  data_inici = '%s-%s-01' % (any, mes)
  data_final = '%s-%s-%s' % (any, mes, calendar.monthrange(int(any), int(mes))[1])

  lectures = {}

  sql = """select 
  sum(lectura) as consum,
  giro,
  comptador,
  periode,
  cups,
  tarifa,
  to_char(min(data_inici), 'DDMMYYYY') as data_inici,
  to_char(max(data_final), 'DDMMYYYY') as data_final
from 
  (
    (select 
       l.lectura,
       l.comptador,
       c.giro,
       pt.name as periode,
       cu.name as cups,
       t.name as tarifa,
       l.name as data_final, 
       l.name as data_inici
     from 
       giscedata_lectures_lectura l
     left join giscedata_lectures_comptador c
       on (l.comptador = c.id)
     right join giscedata_polissa p
       on (c.polissa = p.id and p.potencia > %%.3f and p.potencia <= %%.3f)
     left join giscedata_cups_ps cu
       on (p.cups = cu.id)
     left join giscedata_polissa_tarifa_periodes per
       on (l.periode = per.id)
     right join giscedata_polissa_tarifa t
       on (per.tarifa = t.id and t.name in (%s))
     left join product_product pr
       on (per.product_id = pr.id)
     left join product_template pt
       on (pr.product_tmpl_id = pt.id)
     where 
       to_char(l.name, 'MM-YYYY') = %%s
       and l.tipus = 'A'
     ) 
  union 
    (select 
      -l.lectura,
      l.comptador,
      c.giro,
      pt.name as periode,
      cu.name as cups,
      t.name as tarifa,
      l.name as data_inici,
      l.name as data_final
     from 
       giscedata_lectures_lectura l
     left join giscedata_lectures_comptador c
       on (l.comptador = c.id)
     right join giscedata_polissa p
       on (c.polissa = p.id and p.potencia > %%.3f and p.potencia <= %%.3f)
     left join giscedata_cups_ps cu
       on (p.cups = cu.id)
     left join giscedata_polissa_tarifa_periodes per
       on (l.periode = per.id)
     right join giscedata_polissa_tarifa t
       on (per.tarifa = t.id and t.name in (%s))
     left join product_product pr
       on (per.product_id = pr.id)
     left join product_template pt
       on (pr.product_tmpl_id = pt.id)
     where
       to_char(l.name, 'MM-YYYY') = to_char(%%s::date - 31*p.facturacio, 'MM-YYYY')
       and l.tipus = 'A'
     )
  ) as foo
group by
  giro,
  comptador,
  periode,
  tarifa,
  cups
order by
  cups asc,
  periode asc""" % (','.join(map(lambda x: "'%s'" % x, tarifes_to_export.keys())), ','.join(map(lambda x: "'%s'" % x, tarifes_to_export.keys())))

  form = data['form']
  vals =  (form['potencia_min'], form['potencia_max'], '%s-%s' % (mes,any), form['potencia_min'], form['potencia_max'], data_final)
  print sql % vals
  cr.execute(sql, vals)

  for lectura in cr.dictfetchall():
    cups = lectura['cups']
    periode = lectura['periode']
    consum = lectura['consum']
    if consum < 0:
      consum += lectura['giro']
    if not lectures.has_key(cups):
      lectures[cups] = {
        'tarifa': lectura['tarifa'],
        'data_inici': lectura['data_inici'],
        'data_final': lectura['data_final'],
        'P1': 0, 
        'P2': 0, 
        'P3': 0, 
        'P4': 0, 
        'P5': 0, 
        'P6': 0
      }
    lectures[cups][periode] += consum

  
  # CUPS;TIPO;INICIO;FIN;CON1;CON2;CON3;CON4;CON5;CON6;ZONA
  output = StringIO.StringIO()
  writer = csv.writer(output, delimiter=';', quoting=csv.QUOTE_NONE)
  for cups in sorted(lectures):
    if lectures[cups]['tarifa'] in ('2.0A', '2.1A'):
      p1 = lectures[cups]['P1']
      p2 = lectures[cups]['P2']
      # swap
      lectures[cups]['P1'] = p2
      lectures[cups]['P2'] = p1
    row = [
      cups,
      tarifes_to_export[lectures[cups]['tarifa']],
      lectures[cups]['data_inici'],
      lectures[cups]['data_final'],
    ]
    # Els 6 consums
    for i in range(1,7):
      row.append(lectures[cups]['P%i' % i])

    row.append(0) # Zona 0
    writer.writerow(row)
 
  if data['form']['compressed']:
    comp = bz2.BZ2Compressor()
    comp.compress(output.getvalue())
    output.close()
    file = base64.b64encode(comp.flush())
    filename = 'CONSUMOS_%s%s.txt.bz2' % (any, mes)
  else:
    file = base64.b64encode(output.getvalue())
    filename = 'CONSUMOS_%s%s.txt' % (any,mes)
  return {
    'name': filename,
    'file': file,
  }

_export_form = """<?xml version="1.0"?>
<form string="Exportación Lecturas CS">
  <field name="name" colspan="4" width="500" invisible="1"/>
  <field name="file" colspan="4"/>
</form>
"""

_export_fields = {
  'name': {'string': 'Nom', 'type': 'char', 'size': 60},
  'file': {'string': 'Fichero', 'type': 'binary'},
}


class wizard_lectures_export_cs(wizard.interface):

  states = {
    'init': {
      'actions': [],
      'result': {'type': 'state', 'state': 'triar_mes'},
    },
    'triar_mes': {
      'actions': [_triar_mes],
      'result': {'type': 'form', 'arch': _triar_mes_form, 'fields': _triar_mes_fields, 'state': [('end', 'Cancelar', 'gtk-cancel'), ('export', 'Generar fichero', 'gtk-execute')]}
    },
    'export': {
      'actions': [_export],
      'result': {'type': 'form', 'arch': _export_form, 'fields': _export_fields, 'state': [('end', 'Cerrar', 'gtk-close')]},
    },
    'end': {
      'actions': [],
      'result': {'type': 'state', 'state': 'end'}
    }
  }

wizard_lectures_export_cs('giscedata.lectures.export.cs')
