# -*- coding: utf-8 -*-
{
    "name": "Exportació de Lectures pel Concentrador secundari",
    "description": """
Mòdul per exportar les lectures en el concentrador secundari.
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_lectures"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_lectures_export_wizard.xml"
    ],
    "active": False,
    "installable": True
}
