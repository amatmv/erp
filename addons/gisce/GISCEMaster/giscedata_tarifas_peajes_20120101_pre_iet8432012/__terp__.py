# -*- coding: utf-8 -*-
{
    "name": "Tarifas Peajes Gener 2012 (pre IET 843/2012)",
    "description": """
Actualització de les tarifes de peatges segons el BOE nº 315 - 31/12/2011.
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_polissa",
        "giscedata_lectures",
        "product",
        "giscedata_facturacio"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_tarifas_peajes_20120101_data.xml"
    ],
    "active": False,
    "installable": True
}
