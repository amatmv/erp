# -*- coding: utf-8 -*-
{
    "name": "Perfilació de lectures (valors 2017) Balears",
    "description": """
Actualitza els valors de perfilació per l'any 2017 a Balears
""",
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_perfils_2017"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_perfils_mm_2017_data.xml"
    ],
    "active": False,
    "installable": True
}
