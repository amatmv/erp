# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction


class TestsCallejerosCups(testing.OOTestCase):

    def test_onchange_callejero_actualiza_cups(self):
        self.openerp.install_module('giscedata_tipovia_catastro')

        imd_obj = self.openerp.pool.get('ir.model.data')
        callejero_obj = self.openerp.pool.get('giscemisc.callejero')
        cups_obj = self.openerp.pool.get('giscedata.cups.ps')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            tv_calle_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_tipovia_catastro', 'tipovia_CL'
            )[1]

            municipi_id = imd_obj.get_object_reference(
                cursor, uid, 'base_extended', 'ine_17079'
            )[1]

            new_callejero_id = callejero_obj.create(
                cursor, uid,
                {'tipo_via': tv_calle_id,
                 'municipi': municipi_id,
                 'calle': u'del Carme'
                 }
            )

            self.assertTrue(new_callejero_id)

            cups_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_cups', 'cups_01'
            )[1]

            cups_obj.write(
                cursor, uid, [cups_id],
                {'callejero_id': new_callejero_id}
            )

            res = cups_obj.onchange_callejero(
                cursor, uid, [cups_id], new_callejero_id
            )

            cups_obj.write(cursor, uid, [cups_id], res['value'])

            add_reads = cups_obj.read(cursor, uid, cups_id, [])

            self.assertEqual(add_reads['tv'][0], tv_calle_id)

            self.assertEqual(add_reads['id_municipi'][0], municipi_id)

            self.assertEqual(add_reads['nv'], u'del Carme')

            self.assertEqual(add_reads['id_provincia'][1], u'Girona')
