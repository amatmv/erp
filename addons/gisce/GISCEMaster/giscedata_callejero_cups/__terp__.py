# -*- coding: utf-8 -*-
{
    "name": "Callejero cups",
    "description": """Afegeix callejero al cups""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "giscedata_cups",
        "giscemisc_callejero"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_cups_view.xml",
    ],
    "active": False,
    "installable": True
}