# -*- coding: utf-8 -*-
{
    "name": "Perfil GISCE Master per Electriques",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Profile",
    "depends":[
        "base_extended",
        "giscedata_at",
        "giscedata_at_informes",
        "giscedata_cts",
        "giscedata_transformadors",
        "giscedata_transformadors_informes",
        "giscedata_descarrecs",
        "giscedata_expedients",
        "giscedata_expedients_informes",
        "giscedata_revisions",
        "giscedata_expedients",
        "giscedata_expedients_informes",
        "giscedata_base",
        "giscedata_cups",
        "partner_customer",
        "partner_supplier",
        "partner_tecnic"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
