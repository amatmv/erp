# -*- coding: utf-8 -*-

import wizard
import pooler
import base64
import csv
import StringIO
import time
from zipfile import ZipFile

def _selection_mes(self, cr, uid, context={}):
    cr.execute("""select distinct to_char(inici, 'MM/YYYY') as month, to_char(inici, 'YYYYMM') as ts from giscedata_re_curva where compact = True order by ts desc""")
    return [(a[0], a[0]) for a in cr.fetchall()]

_triar_mes_form = """<?xml version="1.0"?>
<form string="MEDIDAS" col="4">
  <field name="mes" />
</form>"""

_triar_mes_fields = {
  'mes': {'string': 'Mes', 'type': 'selection', 'selection': _selection_mes},
}

def _check_mes(self, cr, uid, data, context={}):
    # Comprovem que totes les instal·lacions tenen la curva de càrrega entrada y compactada
    # pel mes que anem a fer
    ok = []
    txt = []
    cr.execute("""select
    i.name,
    c.compact
  from
    giscedata_re i
  left join giscedata_re_curva c
    on (c.inst_re = i.id and to_char(c.inici, 'MM/YYYY') = %s)
  where i.active = True""", (data['form']['mes'],))

    insts = cr.dictfetchall()

    if not len(insts):
        ok.append(False)

    for inst in insts:
        if inst['compact'] == True:
            txt.append(u"* La instalación "
                       u" %s está lista" % inst['name'])
            ok.append(True)
        if inst['compact'] == False:
            txt.append(u"* La instalación %s no tiene "
                       u" la curva compactada" % inst['name'])
            ok.append(False)
        if inst['compact'] == None:
            txt.append(u"* La instalación %s no tiene "
                       u" curva de carga" % inst['name'])
            ok.append(False)

    if reduce(lambda a,b: a and b, ok):
        # Tot correcte, podem generar el MEDIDAS
        self.states['check_mes']['result']['state'] = [('gen_fitxer', 'Generar MEDIDAS')]
    else:
        # No es pot generar el magre
        self.states['check_mes']['result']['state'] = [('end', 'Finalizar')]

    return {
      'text': '\n'.join(txt),
    }

_check_mes_form = """<?xml version="1.0"?>
<form string="MEDIDAS" col="4">
  <field name="text" readonly="1" colspan="4" nolabel="1" width="500" />
</form>"""

_check_mes_fields = {
  'text': {'string': 'Texto', 'type': 'text', 'readonly': True},
}


def _gen_fitxer(self, cr, uid, data, context={}):
    perf_obj = pooler.get_pool(cr.dbname).get('giscedata.re.curva.lectura')
    user_obj = pooler.get_pool(cr.dbname).get('res.users')
    distri = user_obj.browse(cr, uid, uid).company_id.cups_code
    month,year = data['form']['mes'].split('/')
    # Busquem les diferents agregacions que hi ha per aquest mes en els perfils
    cr.execute("select distinct cups from giscedata_re_curva_lectura where year = %s and month = %s order by cups asc", (year, month))
    cils = [a[0] for a in cr.fetchall()]

    # Preparem el fitxer
    yearmonth = '%s%s' % (year, month)
    filename = 'medidas_{distri}_{date}_2_{date_now}.zip'.format(
        distri=distri, date=yearmonth, date_now=time.strftime('%Y%m%d')
    )
    medidas_file = StringIO.StringIO()
    zip_file = ZipFile(medidas_file, 'w')

    # Per cada CIL creem la seva curva horaria
    for cil in cils:
        cr.execute("""select
      cups,
      (substring(name, 1, 4) || '-' || substring(name, 5, 2) || '-' || substring(name, 7, 2) || ' ' || substring(name, 9, 2) || ':00:00')::timestamp as timestamp,
      estacio,
      sum(l_liq) as l_liq,
      sum(l_r2) as l_r2,
      sum(l_r3) as l_r3
    from (
      (select
        cups,
        name,
        estacio,
        lectura as l_liq,
        0 as l_r2,
        0 as l_r3
      from giscedata_re_curva_lectura
      where
        cups = %s
        and month = %s
        and year = %s
        and magnitud = 'AS'
      )
      union
      (select
        cups,
        name,
        estacio,
        -lectura as l_liq,
        0 as l_r2,
        0 as l_r3
      from giscedata_re_curva_lectura
      where
        cups = %s
        and month = %s
        and year = %s
        and magnitud = 'AE'
      )
      union
      (select
        cups,
        name,
        estacio,
        0 as l_liq,
        lectura as l_r2,
        0 as l_r3
       from giscedata_re_curva_lectura
       where
        cups = %s
        and month = %s
        and year = %s
        and magnitud = 'R2'
      )
      union
      (select
        cups,
        name,
        estacio,
        0 as l_liq,
        0 as l_r2,
        lectura as l_r3
       from giscedata_re_curva_lectura
       where
        cups = %s
        and month = %s
        and year = %s
        and magnitud = 'R3'
      )
    ) as foo
    group by
      cups,
      name,
      estacio
    order by
      name asc""", (cil, month, year)*4)
        output = StringIO.StringIO()
        writer = csv.writer(output, delimiter=';', quoting=csv.QUOTE_NONE)
        for r in cr.dictfetchall():
            line = []
            line.append('%s001' % r['cups'][:20])
            line.append(r['timestamp'].replace('-', '/'))
            line.append(r['estacio'])
            line.append(r['l_liq'])
            line.append(r['l_r2'])
            line.append(r['l_r3'])
            line.append(None)
            line.append(None)
            line.append('R')
            line.append(None)  # Això és el punt i coma de final de línia

            writer.writerow(line)
        temp_filename = ('medidas_{distri}_{CIL}001_{date}_2_{date_now}.txt'.
                         format(distri=distri, CIL=cil[:20], date=yearmonth,
                                date_now=time.strftime('%Y%m%d'))
                         )
        file_data = output.getvalue()
        zip_file.writestr(temp_filename, file_data)
    zip_file.close()
    output.close()
    medidas_file_value = medidas_file.getvalue()
    medidas_file.close()
    medidas_file = base64.b64encode(medidas_file_value)

    return {'name': filename, 'file': medidas_file}

_gen_fitxer_form = """<?xml version="1.0"?>
<form string="MEDIDAS" col="4">
  <field name="name" readonly="1" colspan="4"/>
  <field name="file" readonly="1" nolabel="1" colspan="4"/>
</form>"""

_gen_fitxer_fields = {
  'name': {'string': 'Fichero', 'type': 'char', 'size': 255, 'readonly': True},
  'file': {'string': 'Contenido Fichero', 'type': 'binary', 'readonly': True},
}

class wizard_medidas(wizard.interface):

    states = {
      'init': {
        'actions': [],
        'result': {'type': 'state', 'state': 'triar_mes'},
      },
      'triar_mes': {
        'actions': [],
        'result': {'type': 'form', 'arch': _triar_mes_form, 'fields': _triar_mes_fields, 'state': [('check_mes', 'Siguiente')]}
      },
      'check_mes': {
        'actions': [_check_mes],
        'result': {'type': 'form', 'arch': _check_mes_form, 'fields': _check_mes_fields, 'state': [('gen_fitxer', 'Generar MEDIDAS')]}
      },
      'gen_fitxer': {
        'actions': [_gen_fitxer],
        'result': {'type': 'form', 'arch': _gen_fitxer_form, 'fields': _gen_fitxer_fields, 'state': [('end', 'Finalizar')]}
      },
      'end': {
        'actions': [],
        'result': {'type': 'state', 'state': 'end'},
      }
    }

wizard_medidas('giscedata.sicilia.medidas')
