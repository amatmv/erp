# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* giscedata_sicilia
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.14\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2012-09-19 15:33:47+0000\n"
"PO-Revision-Date: 2012-09-19 15:33:47+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: giscedata_sicilia
#: model:ir.module.module,description:giscedata_sicilia.module_meta_information
msgid "\n"
"Ens permet generar els diferents fitxers per comunicar-nos amb la CNE de Règim Especial.\n"
"  * MEDIDAS\n"
"  "
msgstr ""

#. module: giscedata_sicilia
#: model:ir.module.module,shortdesc:giscedata_sicilia.module_meta_information
msgid "Mòdul Sicilia"
msgstr ""

#. module: giscedata_sicilia
#: wizard_field:giscedata.sicilia.medidas,gen_fitxer,name:0
msgid "Fichero"
msgstr ""

#. module: giscedata_sicilia
#: wizard_view:giscedata.sicilia.medidas,check_mes:0
msgid "MAGRE"
msgstr ""

#. module: giscedata_sicilia
#: wizard_field:giscedata.sicilia.medidas,check_mes,text:0
msgid "Texto"
msgstr ""

#. module: giscedata_sicilia
#: model:ir.actions.wizard,name:giscedata_sicilia.wizard_medidas
#: model:ir.ui.menu,name:giscedata_sicilia.menu_generar_medidas
msgid "Generar MEDIDAS"
msgstr ""

#. module: giscedata_sicilia
#: wizard_field:giscedata.sicilia.medidas,triar_mes,mes:0
msgid "Mes"
msgstr ""

#. module: giscedata_sicilia
#: wizard_button:giscedata.sicilia.medidas,triar_mes,check_mes:0
msgid "Siguiente"
msgstr ""

#. module: giscedata_sicilia
#: wizard_field:giscedata.sicilia.medidas,gen_fitxer,file:0
msgid "Contenido Fichero"
msgstr ""

#. module: giscedata_sicilia
#: wizard_view:giscedata.sicilia.medidas,gen_fitxer:0
#: wizard_view:giscedata.sicilia.medidas,triar_mes:0
msgid "MEDIDAS"
msgstr ""

#. module: giscedata_sicilia
#: wizard_button:giscedata.sicilia.medidas,check_mes,gen_fitxer:0
msgid "Generar MAGRE"
msgstr ""

#. module: giscedata_sicilia
#: wizard_button:giscedata.sicilia.medidas,gen_fitxer,end:0
msgid "Finalizar"
msgstr ""

