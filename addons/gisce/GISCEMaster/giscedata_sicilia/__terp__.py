# -*- coding: utf-8 -*-
{
    "name": "Mòdul Sicilia",
    "description": """
Ens permet generar els diferents fitxers per comunicar-nos amb la CNE de Règim Especial.
  * MEDIDAS
  """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_administracio_publica_cne",
        "giscedata_re"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_sicilia_wizard.xml"
    ],
    "active": False,
    "installable": True
}
