# -*- coding: utf-8 -*-
{
    "name": "GISCE Data Baixa Tensió",
    "description": """Modul de baixa tensió""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Baixa Tensió",
    "depends":[
        "base_extended",
        "giscedata_cts",
        "giscedata_administracio_publica_cnmc_distri",
        "giscedata_tensions"
    ],
    "init_xml": [],
    "demo_xml":[
        "giscedata_bt_demo.xml",
        "giscedata_bt_sequence_demo.xml"
    ],
    "update_xml":[
        "giscedata_bt_view.xml",
        "ir.model.access.csv",
        "security/giscedata_bt_security.xml",
        "security/ir.model.access.csv",
        "giscedata_tipus_installacio_bt_data.xml",
        "wizard/set_no_apm_active_bts_wizard_view.xml",
        "giscedata_bt_sequence.xml",
        "giscedta_bt_suport_tipus_data.xml"
    ],
    "active": False,
    "installable": True
}
