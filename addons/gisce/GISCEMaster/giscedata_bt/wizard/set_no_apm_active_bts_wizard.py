# -*- coding: utf-8 -*-
from uuid import uuid4
from tools.translate import _
from osv import osv, fields
from addons import get_module_resource
import csv
import base64
import tempfile


class WizardSetApm(osv.osv_memory):
    """Wizard per modificar la data d'una modificació contractual
    """
    _name = 'wizard.set.apm'

    def change_state(self, cursor, uid, ids, context=None):

        has_not_init_apms = self.set_query_execute(cursor, uid, ids,
                                                   context=context)
        if has_not_init_apms:
            self.write(cursor, uid, ids, {'state': 'confirmed'},
                       context=context)
        else:
            self.write(cursor, uid, ids,
                       {'state': 'done',
                        'info': "No hi ha elements bt sense dates APM"},
                       context=context)

    def generate_csv_with_selected(self, cursor, uid, ids, not_apm_ids=None,
                                   context=None):
        if not_apm_ids:
            keys = not_apm_ids[0].keys()
            with tempfile.TemporaryFile() as output_file:
                dict_writer = csv.DictWriter(output_file, keys)
                dict_writer.writeheader()
                dict_writer.writerows(not_apm_ids)
                output_file.seek(0)
                return output_file.read()
        return False

    def set_query_execute(self, cursor, uid, ids, context=None):
        if not context:
            context = {}

        sql_select = 'seleccionar_bt_no_apm.sql'

        query = get_module_resource('giscedata_bt', 'sql', sql_select)
        sql = open(query).read()
        cursor.execute(sql)
        not_apm_ids = cursor.dictfetchall()
        if not_apm_ids:
            file_f = self.generate_csv_with_selected(
                cursor, uid, ids, not_apm_ids, context=context)
            file_f = base64.b64encode(file_f)
            unique_key = str(uuid4())[-13:]
            file_name = 'update_apm_bt' + unique_key + '.csv'
            self.write(cursor, uid, ids, {
                'file': file_f,
                'filename': file_name}, context=context)
            return True
        return False

    def update_APM(self, cursor, uid, ids, context=None):
        if not context:
            context = {}

        # TODO: something to avoid double query execute
        sql_select = 'seleccionar_bt_no_apm.sql'

        query = get_module_resource('giscedata_bt', 'sql', sql_select)
        sql = open(query).read()
        cursor.execute(sql)
        list_dicts = cursor.dictfetchall()
        # End TODO
        if list_dicts:
            bt_ids = tuple([d['id'] for d in list_dicts])
            if bt_ids:
                sql_select = 'update_bt_no_apm.sql'
                query = get_module_resource('giscedata_bt', 'sql', sql_select)
                sql = open(query).read()
                params = {
                    'elements_ids': bt_ids
                }
                cursor.execute(sql, params)
                self.write(cursor, uid, ids,
                           {'info': _("Dates APM actualitzades"),
                            'state': 'done'},
                           context=context
                           )

        else:
            self.write(cursor, uid, ids,
                       {'info': _("No hi ha elements bt sense dates APM"),
                        'state': 'done'},
                       context=context
                       )

    _columns = {
        'state': fields.selection(
            [('init', 'Init'),
             ('confirmed', 'Confirmed'),
             ('done', 'Done'), ],
            'State'),
        'file': fields.binary('Fitxer'),
        'filename': fields.char("Nom Fitxer", size=256),
        'info': fields.text('Info'),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'info': lambda *a: _('Es procedirà a actualitzar les dates APM dels '
                             'elements BT')
    }




WizardSetApm()