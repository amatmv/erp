# coding=utf-8
from destral import testing
from destral.transaction import Transaction


class TestBtElement(testing.OOTestCase):

    require_demo_data = True

    def test_has_cini_fields(self):

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            bt_obj = self.openerp.pool.get('giscedata.bt.element')
            fields = bt_obj.fields_get(cursor, uid)
            self.assertIn('cini', fields)
            self.assertIn('bloquejar_cini', fields)

    def test_auto_cini(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            imd_obj = self.openerp.pool.get('ir.model.data')
            bt_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_bt', 'element_1'
            )[1]

            bt_obj = self.openerp.pool.get('giscedata.bt.element')

            # Auto CINI creating a BT element
            bt = bt_obj.browse(cursor, uid, bt_id)
            self.assertEqual(bt.cini, 'I20511DB')

            # Auto CINI changing values
            bt.write({'voltatge': '230', 'tipus_linia': 3})
            bt = bt_obj.browse(cursor, uid, bt_id)
            self.assertEqual(bt.cini, 'I20541DA')

            # No changing CINI if blocked
            bt.write({'bloquejar_cini': 1})
            bt.write({'voltatge': '400', 'tipus_linia': 1})
            bt = bt_obj.browse(cursor, uid, bt_id)
            self.assertEqual(bt.cini, 'I20541DA')

            # Recalculating on unblocking
            bt.write({'bloquejar_cini': 0})
            bt = bt_obj.browse(cursor, uid, bt_id)
            self.assertEqual(bt.cini, 'I20511DB')

    def test_update_data_pm(self):
        """
        Tests that the data_pm updates when a expedient added

        :return:None
        """

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            expedient_obj = self.openerp.pool.get('giscedata.expedients.expedient')
            test_date = '2016-10-17'
            expedient_data = {
                'name': 1,
                'industria_data': test_date
            }
            exp_id = expedient_obj.create(cursor, uid, expedient_data)
            bt_obj = self.openerp.pool.get('giscedata.bt.element')
            bt_data = {'name': 1}
            bt_id = bt_obj.create(cursor, uid, bt_data)
            bt = bt_obj.browse(cursor, uid, bt_id)
            self.assertFalse(bt.data_pm)

            expedient_data_link = {'expedients_ids': [(4, exp_id)]}
            bt_obj.write(cursor, uid, bt_id, expedient_data_link)
            bt = bt_obj.browse(cursor, uid, bt_id)

            self.assertEqual(bt.data_pm, test_date)
