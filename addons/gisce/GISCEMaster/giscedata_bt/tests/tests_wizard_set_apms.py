# coding=utf-8
from destral import testing
from destral.transaction import Transaction
from base64 import b64decode


class TestWizardAPM(testing.OOTestCase):
    def test_no_apms_to_update(self):
        bt_obj = self.openerp.pool.get('giscedata.bt.element')
        wiz_apm_obj = self.openerp.pool.get("wizard.set.apm")
        imd_obj = self.openerp.pool.get('ir.model.data')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            wiz_id = wiz_apm_obj.create(cursor, uid, {})
            # Check if wizard is created correctly
            self.assertTrue(wiz_id)

            # Obteins wizard from browse
            wiz = wiz_apm_obj.browse(cursor, uid, wiz_id)

            # In bt_elements demo files elements 1 and 2 are seted with no APM
            # Let's check

            element1_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_bt', 'ele_1'
            )[1]
            element2_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_bt', 'ele_2'
            )[1]

            self.assertTrue(element1_id, element2_id)

            # Let's initialize apm for element2 manually to check don't show
            # this
            # bt_obj.write(cursor, uid, element2_id, {'data_pm': '2018-01-19'})
            # data_pm is a field function and can't update without  query
            cursor.execute("update giscedata_bt_element set data_pm = '2099-01-24' where id = {0}".format(element2_id))

            self.assertEquals(
                bt_obj.read(
                    cursor, uid, element2_id, ['data_pm']
                )['data_pm'], '2099-01-24')


            # Check if found something
            found_som_no_apm = wiz.set_query_execute()
            self.assertTrue(found_som_no_apm)

            gen_csv = b64decode(wiz.file)
            d_alta = bt_obj.read(cursor, uid, element1_id, ['data_alta']
                                 )['data_alta']
            self.assertEquals(gen_csv.count(d_alta), 37)

            # Let's override not set apm dates (in this case element 1)
            wiz.update_APM()
            self.assertEquals(wiz.info, "Dates APM actualitzades")

            # Now let's recall (All apm should be set)
            found_som_no_apm = wiz.set_query_execute()
            self.assertFalse(found_som_no_apm)




