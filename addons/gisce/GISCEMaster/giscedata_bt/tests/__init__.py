# coding=utf-8
from destral import testing
from destral.transaction import Transaction
from tests_elements_bt import *
from tests_wizard_set_apms import *


class TestBtElement(testing.OOTestCase):

    require_demo_data = True

    def test_voltatge(self):
        """
        Tests that the voltatge is properly calculated

        :return: None
        """
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            bt_obj = self.openerp.pool.get('giscedata.bt.element')
            cable_obj = self.openerp.pool.get("giscedata.bt.cables")
            tensio_obj = self.openerp.pool.get('giscedata.tensions.tensio')

            # Check without assigned cable
            id_bt1 = bt_obj.create(
                cursor, uid, {"voltatge": "230", "tipus_linia": 1}
            )
            data_bt = bt_obj.read(cursor, uid, id_bt1, ["voltatge"])
            self.assertEqual(data_bt["voltatge"], "230")

            bt_obj.write(cursor, uid, id_bt1, {"cable": False, "voltatge": "44"})
            data_bt = bt_obj.read(cursor, uid, id_bt1, ["voltatge"])
            self.assertEqual(data_bt["voltatge"], "44")

            # Check with a cable with tensio
            id_tensio = tensio_obj.search(cursor, uid, [("name", "=", "230")])[0]
            cable_obj.write(cursor, uid, 1, {"nivell_tensio": id_tensio})
            id_cable = 1
            id_bt2 = bt_obj.create(
                cursor, uid, {"cable": id_cable, "tipus_linia": 1}
            )
            data_bt = bt_obj.read(cursor, uid, id_bt2, ["voltatge"])
            self.assertEqual(data_bt["voltatge"], "230")
            bt_obj.write(cursor, uid, id_bt2, {"voltatge": "42"})
            data_bt = bt_obj.read(cursor, uid, id_bt2, ["voltatge"])
            self.assertEqual(data_bt["voltatge"], "230")

            # Check with a cable without tensio
            id_bt3 = bt_obj.create(cursor, uid, {"tipus_linia": 1})
            data_bt3 = bt_obj.read(cursor, uid, id_bt3, ["voltatge"])
            self.assertEqual(data_bt3["voltatge"], False)
            bt_obj.write(cursor, uid, id_bt3, {"voltatge": "42"})
            data_bt3 = bt_obj.read(cursor, uid, id_bt3, ["voltatge"])
            self.assertEqual(data_bt3["voltatge"], "42")

            # Check that if sets a cable with tension the voltatge is recalculed
            bt_obj.write(cursor, uid, id_bt3, {"cable": id_cable})
            data_bt3 = bt_obj.read(cursor, uid, id_bt3, ["voltatge"])
            self.assertEqual(data_bt3["voltatge"], "230")

    def test_lbt_volt(self):
        """
        Test to check that tension if fill if the cable is write

        :return: None
        :rtype: None
        """
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            bt_obj = self.openerp.pool.get('giscedata.bt.element')
            cable_obj = self.openerp.pool.get("giscedata.bt.cables")
            tensio_obj = self.openerp.pool.get('giscedata.tensions.tensio')
            material_obj = self.openerp.pool.get("giscedata.bt.material")
            familia_obj = self.openerp.pool.get("giscedata.bt.familiacables")
            tipus_obj = self.openerp.pool.get("giscedata.bt.tipuscable")

            search_params_mat = [("name", "=", "Material 1")]
            id_material = material_obj.search(cursor, uid, search_params_mat)[0]

            search_params_tensio = [("name", "=", "3x230/400")]
            id_tensio = tensio_obj.search(cursor, uid, search_params_tensio)[0]

            search_params_fam = [("name", "=", "Familia 1")]
            id_fam = familia_obj.search(cursor, uid, search_params_fam)[0]

            search_params_tipus = [("name", "=", "Trenat")]
            id_tipus = tipus_obj.search(cursor, uid, search_params_tipus)[0]

            data_cable = {
                "nivell_tensio": id_tensio,
                "material": id_material,
                "familia": id_fam,
                "tipus": id_tipus,
                "name": "Cable 1",
                "intensitat_admisible": 1,
                "seccio": 1,
                "tipus_linia": 1
            }
            id_cable = cable_obj.create(cursor, uid, data_cable)

            id_bt = bt_obj.create(cursor, uid, {"tipus_linia": 1})
            data_bt = bt_obj.read(cursor, uid, id_bt, ["voltatge"])
            self.assertEqual(data_bt["voltatge"], False)

            bt_obj.write(cursor, uid, id_bt, {"cable": False, "voltatge": "42"})
            data_bt3 = bt_obj.read(cursor, uid, id_bt, ["voltatge"])
            self.assertEqual(data_bt3["voltatge"], "42")

            bt_obj.write(cursor, uid, id_bt, {"cable": id_cable})
            data_bt = bt_obj.read(cursor, uid, id_bt, ["voltatge"])
            self.assertEqual(data_bt["voltatge"], "400")


    def test_has_cini_fields(self):

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            bt_obj = self.openerp.pool.get('giscedata.bt.element')
            fields = bt_obj.fields_get(cursor, uid)
            self.assertIn('cini', fields)
            self.assertIn('bloquejar_cini', fields)

    def test_auto_cini(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            imd_obj = self.openerp.pool.get('ir.model.data')
            bt_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_bt', 'ele_1'
            )[1]

            bt_obj = self.openerp.pool.get('giscedata.bt.element')

            # Auto CINI creating a BT element
            bt = bt_obj.browse(cursor, uid, bt_id)
            self.assertEqual(bt.cini, 'I20511DB')

            # Auto CINI changing values
            bt.cable.write({'nivell_tensio': False})
            bt.write({'voltatge': '230', 'tipus_linia': 3})
            bt = bt_obj.browse(cursor, uid, bt_id)
            self.assertEqual(bt.cini, 'I20541DA')

            # No changing CINI if blocked
            bt.write({'bloquejar_cini': 1})
            bt.write({'voltatge': '400', 'tipus_linia': 1})
            bt = bt_obj.browse(cursor, uid, bt_id)
            self.assertEqual(bt.cini, 'I20541DA')

            # Recalculating on unblocking
            bt.write({'bloquejar_cini': 0})
            bt = bt_obj.browse(cursor, uid, bt_id)
            self.assertEqual(bt.cini, 'I20511DB')

    def test_write_cini_unblocked(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            imd_obj = self.openerp.pool.get('ir.model.data')
            bt_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_bt', 'ele_2'
            )[1]
            bt_obj = self.openerp.pool.get('giscedata.bt.element')
            bt = bt_obj.browse(cursor, uid, bt_id)
            self.assertFalse(bt.cini)

            bt.write({'cini': 'I20511AA'})
            bt = bt_obj.browse(cursor, uid, bt_id)
            self.assertEqual(bt.cini, 'I20511AA')

            bt.write({'bloquejar_cini': 0, 'cini': 'I20511BA'})
            bt = bt_obj.browse(cursor, uid, bt_id)
            self.assertEqual(bt.cini, 'I20511DB')

    def test_update_data_pm(self):
        """
        Tests that the data_pm updates when a expedient added

        :return:None
        """

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            expedient_obj = self.openerp.pool.get('giscedata.expedients.expedient')
            test_date = '2016-10-17'
            expedient_data = {
                'name': 1,
                'industria_data': test_date
            }
            exp_id = expedient_obj.create(cursor, uid, expedient_data)
            bt_obj = self.openerp.pool.get('giscedata.bt.element')
            bt_data = {'name': 1, 'tipus_linia': 1}
            bt_id = bt_obj.create(cursor, uid, bt_data)
            bt = bt_obj.browse(cursor, uid, bt_id)
            self.assertFalse(bt.data_pm, False)

            expedient_data_link = {'expedients_ids': [(4, exp_id)]}
            bt_obj.write(cursor, uid, bt_id, expedient_data_link)
            bt = bt_obj.browse(cursor, uid, bt_id)

            self.assertEqual(bt.data_pm, test_date)
