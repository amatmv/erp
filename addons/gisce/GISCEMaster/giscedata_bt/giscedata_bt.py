# -*- coding: utf-8 -*-
from osv import osv, fields
import time
from giscedata_administracio_publica_cnmc_distri.wizard.wizard_recalcul_cinis_tis import MODELS_SELECTION
from giscedata_administracio_publica_cnmc_distri.giscedata_tipus_installacio import TIPUS_REGULATORI

MODELS_SELECTION.append(
    ('giscedata.bt.element', 'Línies BT'),
)


class GiscedataBtTipuslinia(osv.osv):

    _name = "giscedata.bt.tipuslinia"
    _description = "Tipus de la línia"

    _columns = {
      'name': fields.char('Tipus', size=60, required=True),
    }

    _defaults = {

    }

    _order = "name, id"


GiscedataBtTipuslinia()


class GiscedataBtMaterial(osv.osv):

    _name = "giscedata.bt.material"
    _description = "Material"

    _columns = {
      'name': fields.char('Material', size=60),
      'codi': fields.char('Codi', size=1),
    }

    _defaults = {
    }

    _order = "name, id"


GiscedataBtMaterial()


class GiscedataBtTipuscable(osv.osv):

    _name = "giscedata.bt.tipuscable"
    _description = "Tipus de Cable"

    _columns = {
      'codi': fields.char('Codi', size=1),
      'name': fields.char('Descripció', size=60),
    }

    _defaults = {

    }

    _order = "name, id"


GiscedataBtTipuscable()


class GiscedataBtConductors(osv.osv):

    _name = "giscedata.bt.conductors"
    _description = "Conductors del cable"

    _columns = {
      'name': fields.char('Nom', size=60),
      'n_conductors': fields.integer('Número de conductors'),
    }

    _defaults = {

    }

    _order = "name, id"


GiscedataBtConductors()


class GiscedataBtFamiliacables(osv.osv):

    _name = "giscedata.bt.familiacables"
    _description = "Família dels cables"

    _columns = {
      'name': fields.char('Nom', size=60),
    }

GiscedataBtFamiliacables()


class GiscedataBtCables(osv.osv):

    _name = "giscedata.bt.cables"
    _description = "Cables"

    def name_get(self, cr, uid, ids, context={}):
        if not len(ids):
            return []
        res = []
        for cable in self.browse(cr, uid, ids):
            if cable.material:
                name = '%s "%s"' % (
                    cable.name.strip(),  cable.material.name.strip()
                )
            else:
                name = '%s' % cable.name
            res.append((cable.id, name))
        return res


    def _n_trams(self, cr, uid, ids, field_name, ar, context={}):
        res = {}
        cr.execute(
            "SELECT c.id,count(t.id) from giscedata_bt_cables c "
            "left join giscedata_bt_element t on (t.cable = c.id) "
            "where c.id in (%s) group by c.id" % (
                ','.join(map(str, map(int, ids)))
            )
        )
        for cable in cr.fetchall():
            res[cable[0]] = cable[1]
        return res

    _columns = {
        'name': fields.char('Codi', size=60, required=True),
        'material': fields.many2one(
            'giscedata.bt.material', 'Material', required=True,
            ondelete='restrict'
        ),
        'seccio': fields.float('Secció', required=True),
        'resistencia': fields.float('Resistencia XL (Ohms/Km)', digits=(16, 8)),
        'reactancia': fields.float('Reactancia XL (Ohms/Km)', digits=(16, 8)),
        'observacions': fields.text('Observacions'),
        'd_interior': fields.float('Diàmetre interior'),
        'd_aillament': fields.float('Diàmetre sobre aïllament'),
        'd_exterior': fields.float('Diàmetre exteriror'),
        'pes': fields.float('Pes (Kg/Km)'),
        'capacitat': fields.float('Capacitat (microF/Km)'),
        'r_minim': fields.float('Radi mínim de curvatura (mm)'),
        'carrega_trencament': fields.float('Càrrega de trancament (Nw)'),
        'aillament': fields.char('Aïllament', size=10),
        'tipus': fields.many2one(
            'giscedata.bt.tipuscable', 'Tipus', required=True,
            ondelete='restrict'
        ),
        'conductors': fields.many2one('giscedata.bt.conductors', 'Conductors'),
        'intensitat_admisible': fields.float(
            'Intensitat Admisible', required=True
        ),
        'familia': fields.many2one(
            'giscedata.bt.familiacables', 'Família', required=True,
            ondelete='restrict'
        ),
        'n_trams': fields.function(
            _n_trams, method=True, type='integer', string='Nº de Trams'
        ),
        'nivell_tensio': fields.many2one(
            'giscedata.tensions.tensio',
            string="Nivell tensio",
            widget="selection",
            select=1,
            domain=[("tipus", "=", "BT")],
            required=False
        )
    }

    _defaults = {

    }

    _order = "name, id"


GiscedataBtCables()


class GiscedataBtElement(osv.osv):
    """
    Class to represent BT element
    """
    _name = 'giscedata.bt.element'
    _description = "Trams Baixa Tensió"

    def create(self, cr, uid, vals, context={}):
        if 'name' not in vals:
            vals['name'] = self._get_nextnumber(cr, uid, context)
        vals['mslink'] = vals['name']

        return super(GiscedataBtElement, self).create(cr, uid, vals, context)

    def name_get(self, cursor, uid, ids, context=None):
        """
        Sobreescrivim el metode name_get per a fer un cast de integer a str
        del camp "name".
        """
        if context is None:
            context = {}
        res = []
        if ids:
            q = self.q(cursor, uid)
            for element in q.read(['name']).where([('id', 'in', ids)]):
                name = element['name']
                res.append((element['id'], str(name)))
        return res

    def _get_nextnumber(self, cursor, uid, context=None):
        """Retorna el següent número d'element BT."""

        return int(
            self.pool.get('ir.sequence').get(
                cursor, uid, 'giscedata.bt.element'
            )
        )

    def unlink(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        if not isinstance(ids, list):
            ids = [ids]
        existing_elements = self.search(cursor, uid, [('id', 'in', ids)])
        if not existing_elements:
            return True
        else:
            ids = existing_elements
        if context.get('permanent'):
            return super(GiscedataBtElement, self).unlink(
                cursor, uid, ids, context=context
            )
        else:
            no_data_baixa = []
            for element in self.read(
                    cursor, uid, ids, ['data_baixa'], context=context
            ):
                if not element['data_baixa']:
                    no_data_baixa.append(element['id'])

            self.write(
                cursor, uid, ids, {'baixa': 1, 'active': 0}, context=context
            )
            if no_data_baixa:
                self.write(
                    cursor, uid, no_data_baixa,
                    {'data_baixa': time.strftime('%Y-%m-%d')}, context=context
                )
            return True

    def read_distinct(self, cr, uid, values):
        cr.execute("""
          select name from giscedata_bt_element WHERE active = True
        """)
        bt_in_bbdd = cr.dictfetchall()
        for v in values:
            try:
                bt_in_bbdd.remove({'name': v})
            except:
                pass
        return bt_in_bbdd

    def read_distinct2(self, cr, uid, values):
        in_array = []
        for value in values:
            cr.execute("""
              select name from giscedata_bt_element where name = %s
              and active = True
            """, (value, ))
            if not cr.fetchone():
                in_array.append(value)
        return in_array

    def _int_to_char_name(self, cr, uid, ids, field_name, ar, context=None):
        res = {}
        for id in ids:
            cr.execute("select name from giscedata_bt_element where id = %s",
                       (id,))
            res[id] = str(cr.fetchone()[0])
        return res

    def _data_pm(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        res = dict.fromkeys(ids, False)
        for item in self.browse(cursor, uid, ids, context):
            if item.expedients_ids and not item.bloquejar_pm:
                res[item.id] = max(e.industria_data
                                   for e in item.expedients_ids)
            else:
                res[item.id] = item.data_pm
        return res

    def _get_elements(self, cursor, uid, ids, context=None):
        cursor.execute(
            "select e__t.element_id from "
            "giscedata_bt_element_expedient AS e__t "
            "LEFT JOIN giscedata_bt_element AS t on (e__t.element_id=t.id) "
            "where e__t.expedient_id in %s and not t.bloquejar_pm",
            (tuple(ids), )
        )
        return [x[0] for x in cursor.fetchall()]

    def _get_elements_nobloc(self, cursor, uid, ids, context=None):
        cursor.execute(
            "select id from giscedata_bt_element "
            "where not bloquejar_pm and id in %s", (tuple(ids), )
        )
        return [x[0] for x in cursor.fetchall()]

    def _get_elements_nobloc_cini(self, cursor, uid, ids, context=None):
        if self._name == 'giscedata.bt.element':
            cursor.execute("select id from giscedata_bt_element "
                           "where not bloquejar_cini and id in %s",
                           (tuple(ids),))
        elif self._name == 'giscedata.bt.cables':
            cursor.execute("""
            SELECT elem.id
            FROM giscedata_bt_element AS elem
            LEFT JOIN giscedata_bt_cables AS cab ON
            (cab.id = elem.cable)
            WHERE cab.id IN %s
            AND NOT elem.bloquejar_cini
            """, (tuple(ids),))
        return [x[0] for x in cursor.fetchall()]

    def _overwrite_apm(self, cursor, uid, ids, field_name, field_value, args,
                       context=None):
        if not isinstance(ids, (tuple, list)):
            ids = [ids]

        cursor.execute("update giscedata_bt_element set data_pm=%s "
                       "where id in %s and bloquejar_pm",
                       (field_value or None, tuple(ids),))

    def _cini(self, cursor, uid, ids, field_name, arg, context=None):
        """
        Function that calculates the CINI

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: Ids of the bt elements to calculate
        :type ids: list of int
        :param field_name: unused
        :param arg: unused
        :param context: OpenERP context
        :type context: dict
        :return:
        """
        if not context:
            context = {}
        res = dict.fromkeys(ids, False)
        from cini.models import Linea
        for bt in self.browse(cursor, uid, ids):
            linea = Linea()
            try:
                linea.tension = int(bt.voltatge)
            except (TypeError, ValueError):
                linea.tension = None
            linea.num_conductores = 1
            linea.num_circuitos = 1
            if bt.cable:
                linea.seccion = bt.cable.seccio
            if bt.tipus_linia:
                if bt.tipus_linia.name.startswith('A'):
                    if 'f' in bt.tipus_linia.name.lower():
                        linea.despliegue = 'AF'
                    else:
                        linea.despliegue = 'AP'
                else:
                    linea.despliegue = 'S'

            res[bt.id] = str(linea.cini)

        return res

    def _cini_inv(self, cursor, uid, ids, name, value, args, context=None):
        if not context:
            context = {}

        if not isinstance(ids, (tuple, list)):
            ids = [ids]

        cursor.execute("update giscedata_bt_element set cini=%s "
                       "where id in %s and bloquejar_cini",
                       (value or None, tuple(ids),))
        return True

    def _get_element_nobloc_ti(self, cursor, uid, ids, context=None):
        search_params = [
            ('bloquejar_cnmc_tipus_install', '!=', 1),
            ('id', 'in', ids)
        ]
        return self.search(cursor, uid, search_params, context=context)

    def _tipus_instalacio_cnmc_id(self, cursor, uid, ids, field_name, arg,
                                   context=None):
        if not context:
            context = {}
        res = dict.fromkeys(ids, False)
        from tipoinstalacion.models import Linea
        for bt in self.browse(cursor, uid, ids, context=context):
            # TODO: Fix https://github.com/gisce/erp/issues/3252
            if bt.bloquejar_cnmc_tipus_install:
                res[bt.id] = bt.tipus_instalacio_cnmc_id.id
                continue
            ti = Linea()
            ti.num_conductores = 1
            ti.num_circuitos = 1
            try:
                ti.tension = float(bt.voltatge) / 1000.0
            except (TypeError, ValueError):
                ti.tension = None
            if bt.cable:
                ti.seccion = bt.cable.seccio
            if bt.tipus_linia:
                if bt.tipus_linia.name.startswith('A'):
                    if 'f' in bt.tipus_linia.name.lower():
                        ti.despliegue = 'AF'
                    else:
                        ti.despliegue = 'AP'
                else:
                    ti.despliegue = 'S'
            if ti.tipoinstalacion:
                ti_obj = self.pool.get('giscedata.tipus.installacio')
                ti_ids = ti_obj.search(cursor, uid, [
                    ('name', '=', ti.tipoinstalacion)
                ])
                res[bt.id] = ti_ids[0]

        return res

    def _tipus_instalacio_cnmc_id_inv(self, cursor, uid, ids, name, value, args,
                                      context=None):
        if not context:
            context = {}
        if not isinstance(ids, (tuple, list)):
            ids = [ids]
        cursor.execute(
            "update giscedata_bt_element set tipus_instalacio_cnmc_id=%s "
            "where id in %s and bloquejar_cnmc_tipus_install",
            (value or None, tuple(ids),)
        )
        return True

    def _set_voltage(self, cursor, uid, ids, field_name, field_value, args,
                     context=None):
        """
        Function to manage the write on the voltage field

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: Ids of the BT elements to write
        :type ids: list of int
        :param field_name: Field to write
        :type field_name: str
        :param field_value: Value to write
        :param args:
        :param context: OpenERP context
        :type context: dict
        :return: None
        :rtype: None
        """

        sql_update_tensio = """
            UPDATE giscedata_bt_element
            SET voltatge = %s
            WHERE id = %s
        """

        if not isinstance(ids, list):
            ids = [ids]
        calc_vals = self._calculate_voltage(
            cursor, uid, ids, field_value, args, context)

        for identifier in ids:
            volt_val = field_value or calc_vals.get(identifier, False)
            cursor.execute(sql_update_tensio, (volt_val, identifier))

    def _calculate_voltage(self, cursor, uid, ids, field_name, arg,
                           context=None):
        """
        Calculates the voltage of the BT element

        :param cursor: Database cursor
        :param uid: User id
        :param ids: BT elements id
        :param field_name: name of the filed to calculate
        :param arg:
        :param context: OpenERP context
        :return: Id and value
        :rtype: dict
        """

        obj_bt = self.pool.get("giscedata.bt.element")
        obj_cable = self.pool.get("giscedata.bt.cables")

        ret = dict.fromkeys(ids, False)

        actual_data = super(GiscedataBtElement, self).read(
            cursor, uid, ids, ["voltatge"]
        )
        for element in actual_data:
            ret[element["id"]] = element["voltatge"]
        data_cable = obj_bt.read(cursor, uid, ids, ["cable"])

        for bt_element in data_cable:
            if bt_element["cable"]:
                br_bt = obj_cable.browse(cursor, uid, bt_element["cable"][0])
                if br_bt.nivell_tensio:
                    ret[bt_element["id"]] = str(br_bt.nivell_tensio.tensio)
        return ret

    def _get_cable_volt(self, cursor, uid, ids, context=None):
        """
        Returns the list of elements to recalculate the voltage when the
        nivell_tension is changed

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: Affected ids
        :type ids: list
        :param context:OpenERP context
        :type context: dict
        :return:Ids to recalculate
        :rtype: list
        """

        obj_bt = self.pool.get("giscedata.bt.element")
        return obj_bt.search(cursor, uid, [("cable", "in", ids)])

    def _get_element_volt(self, cursor, uid, ids, context=None):
        """
        Returns the list of elements to recalculate the voltage when the
        cable is changed

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: Affected ids
        :type ids: list
        :param context:OpenERP context
        :type context: dict
        :return:Ids to recalculate
        :rtype: list
        """

        return ids

    _columns = {
        'name': fields.integer('Codi'),
        'active': fields.boolean('Actiu', select=True),
        'ct': fields.many2one('giscedata.cts', 'Codi CT'),
        'situacio': fields.char('Situació', size=60),
        'num_linia': fields.char('Número de línia', size=3),
        'tipus_linia': fields.many2one(
            'giscedata.bt.tipuslinia', 'Tipus de línia',
            ondelete='restrict', required=True
        ),
        'longitud_op': fields.float('Longitud real'),
        'longitud_cad': fields.float('Longitud CAD', readonly=True),
        'voltatge': fields.function(
            _calculate_voltage, string='Voltatge', method=True, size=10,
            type='char',
            store={
                "giscedata.bt.cables": (_get_cable_volt, ["nivell_tensio"], 10),
                "giscedata.bt.element": (
                    _get_element_volt, ["cable", "voltatge"], 10
                )
            },
            fnct_inv=_set_voltage

        ),
        'cable': fields.many2one(
            'giscedata.bt.cables', 'Cable', ondelete='restrict'),
        'municipi': fields.many2one('res.municipi', 'Municipi'),
        'cini': fields.function(
            _cini, type='char', size=8, method=True, store={
                'giscedata.bt.element': (
                    _get_elements_nobloc_cini, [
                        'voltatge', 'cable', 'bloquejar_cini', 'tipus_linia',
                        'posada_facana'
                    ], 10
                ),
                'giscedata.bt.cables': (
                    _get_elements_nobloc_cini, [
                        'seccio'
                    ], 10
                )
            }, fnct_inv=_cini_inv, string='CINI'
        ),
        'bloquejar_cini': fields.boolean('Bloquejar CINI'),
        'data_alta': fields.date('Data alta'),
        'data_baixa': fields.date('Data baixa', select=1),
        'baixa': fields.boolean('Baixa'),
        'mslink': fields.integer('MSLINK'),
        'mapid': fields.integer('MAPID'),
        'perc_financament': fields.float('% pagat per la companyia'),
        'coeficient': fields.float('Coeficient', digits=(5, 4)),
        'propietari': fields.boolean('Propietari'),
        'posada_facana': fields.boolean('Posada en façana'),
        'expedients_ids': fields.many2many(
            'giscedata.expedients.expedient', 'giscedata_bt_element_expedient',
            'element_id', 'expedient_id', 'Expedients'),
        'data_pm': fields.function(
            _data_pm, type='date', method=True, store={
                'giscedata.expedients.expedient': (
                    _get_elements, ['industria_data'], 10
                ),
                'giscedata.bt.element': (
                    _get_elements_nobloc, ['expedients_ids', 'bloquejar_pm'], 10
                )
            },
            fnct_inv=_overwrite_apm,
            string='Data APM', help=u"Data de l'acta de posada en marxa de la"
                                    u" instal·lació"
        ),
        'bloquejar_pm': fields.boolean('Bloquejar APM',
            help=u"Si està activat, agafa la data "
            u"entrada, si no, la de ,"
            u"l'expedient"),
        'cnmc_tipo_instalacion': fields.char(
            u'CNMC Tipus Instal·lació', size=10, readonly=False),
        'bloquejar_cnmc_tipus_install': fields.boolean(
            u'Bloquejar TI', readonly=False,
            help=u"Si està activat, permet modificar manualment el "
                 u"tipus d'instal·lació de la CNMC"
        ),
        'tipus_instalacio_cnmc_id': fields.function(
            _tipus_instalacio_cnmc_id,
            method=True,
            string='Tipologia CNMC',
            relation='giscedata.tipus.installacio',
            type='many2one',
            fnct_inv=_tipus_instalacio_cnmc_id_inv,
            store={
                'giscedata.bt.element': (
                    _get_element_nobloc_ti, [
                        'voltatge', 'cable', 'tipus_linia',
                        'posada_facana', 'bloquejar_cnmc_tipus_install'
                    ], 10
                )
            }
        ),
        '4771_entregada': fields.json('Dades entregades 4771'),
        '4131_entregada_2016': fields.json('Dades entregades 4131 2016'),
        '4666_entregada_2017': fields.json('Dades entregades 4666 2017'),
        '4666_entregada_2018': fields.json('Dades entregades 4666 2018'),
        'observacions': fields.text('Observacions'),
        "criteri_regulatori": fields.selection(
            TIPUS_REGULATORI,
            required=True,
            string="Criteri regulatori",
            help="Indica si el criteri a seguir al genrar els fitxers d'"
                 "inventari.\nSegons criteri:S'usa el criteri normal\n Froçar "
                 "incloure:S'afegeix l'element al fitxer\n Forçar excloure: "
                 "L'element no apareixera mai",
            select=1
        ),
        '4666_entregada_2019': fields.json('Dades entregades 4666 2019'),
    }

    _defaults = {
        'active': lambda *a: 1,
        'data_alta': lambda *a: time.strftime('%Y-%m-%d'),
        'longitud_cad': lambda *a: 0,
        'longitud_op': lambda *a: 0,
        'baixa': lambda *a: 0,
        'perc_financament': lambda *a: 100.00,
        'coeficient': lambda *a: 1.0,
        'propietari': lambda *a: 1,
        'bloquejar_pm': lambda *a: False,
        'bloquejar_cini': lambda *a: 0,
        'name': _get_nextnumber,
        "criteri_regulatori": lambda *a: "criteri"
    }

    _sql_constraints = [
     ('perc_financament_0_100', 'CHECK(perc_financament between 0 and 100 )',
         "El % de finançament a d'estar entre 0 i 100"),
     ('coeficient_ge_1', 'CHECK(coeficient >= 1 )',
         "El coeficient de longitud ha de de ser més gran que 1")]


GiscedataBtElement()


class GiscedataCts(osv.osv):

    _name = 'giscedata.cts'
    _inherit = 'giscedata.cts'

    def _longitud_bt(self, cr, uid, ids, field_name, ar, context={}):
        res = {}
        for id in ids:
            id = int(id)
            cr.execute(
                "select sum(longitud_cad) from giscedata_bt_element "
                "where ct = %s and (baixa = False or baixa is null)", (id,)
            )
            res[id] = cr.fetchone()[0]
        return res

    def _longitud_bt_aeri(self, cr, uid, ids, field_name, ar, context={}):
        res = {}
        for id in ids:
            id = int(id)
            cr.execute(
                "select sum(longitud_cad) from giscedata_bt_element elem "
                "inner join giscedata_bt_tipuslinia tip "
                "on tip.id = elem.tipus_linia "
                "  and (tip.name like '%%Aèria%%' or tip.name like '%%Aerea%%')"
                "where ct = %s and (baixa = False or baixa is null)", (id,)
            )
            res[id] = cr.fetchone()[0] or 0
        return res

    def get_longitud_bt_aeria_tensat_sobre_postes(self, cursor, uid, ids, context=None):
        return self._get_longitud_bt_aeria_core(
            cursor, uid, ids, False, context=context
        )

    def get_longitud_bt_aeria_sobre_facana(self, cursor, uid, ids, context=None):
        return self._get_longitud_bt_aeria_core(
            cursor, uid, ids, True, context=context
        )

    def _get_longitud_bt_aeria_core(self, cursor, uid, ids, facana, context=None):
        self_obj = self.pool.get('giscedata.cts')
        element_bt_obj = self.pool.get('giscedata.bt.element')

        elements_bt_fields = ['posada_facana', 'longitud_cad',
                              'tipus_linia']
        res = {}
        for self_vals in self_obj.read(cursor, uid, ids, ['elements_bt']):
            self_id = self_vals['id']
            elements_bt = self_vals['elements_bt']
            value = 0
            for element_bt in element_bt_obj.read(cursor, uid, elements_bt, elements_bt_fields):
                posada_facana = element_bt['posada_facana']
                subterranean = 'subt' in element_bt['tipus_linia'][1].lower()
                if not subterranean and (facana and posada_facana or not facana and not posada_facana):
                    value += element_bt['longitud_cad']
            res[self_id] = value
        return res

    def _longitud_bt_subt(self, cr, uid, ids, field_name, ar, context={}):
        res = {}
        for id in ids:
            id = int(id)
            cr.execute(
                "select sum(longitud_cad) "
                "from giscedata_bt_element elem "
                "inner join giscedata_bt_tipuslinia tip "
                "on tip.id = elem.tipus_linia "
                "  and (tip.name like 'Subterrània' "
                "or tip.name like 'Subterranea') "
                "where ct = %s and (baixa = False or baixa is null)", (id,))
            res[id] = cr.fetchone()[0] or 0
        return res

    def _longitud_bt_km(self, cr, uid, ids, field_name, ar, context={}):
        res = {}
        cts_ids = ','.join(map(str, map(int, ids)))
        cr.execute(
            "SELECT ct.id, "
            "coalesce(round((sum(e.longitud_cad)/1000)::numeric,2),0) "
            "from giscedata_cts ct "
            "left join giscedata_bt_element e on (e.ct = ct.id "
            "and (e.baixa = False or baixa is null)) "
            "where ct.id in (%s) group by ct.id" % cts_ids
        )
        for a in cr.fetchall():
            res[a[0]] = a[1]

        return res

    _columns = {
      'longitud_bt': fields.function(
          _longitud_bt, method=True, type='float', string='Longitud BT'
      ),
      'longitud_bt_aeri': fields.function(
          _longitud_bt_aeri, method=True, type='float',
          string='Longitud BT Aeri'
      ),
      'longitud_bt_subt': fields.function(
          _longitud_bt_subt, method=True, type='float',
          string='Longitud BT Subterrani'
      ),
      'longitud_bt_km': fields.function(
          _longitud_bt_km, method=True, type='float', string='Longitud BT'
      ),
      'elements_bt': fields.one2many(
          'giscedata.bt.element', 'ct', 'Elements BT'
      ),
    }


GiscedataCts()


class GiscedataBtSuportTipus(osv.osv):

    _name = 'giscedata.bt.suport.tipus'
    _description = 'Tipus de Suports BT'

    def create(self, cr, uid, vals, context=None):
        """
        This method overloads the original create by adding the logic to avoid
        the creation of duplicated GiscedataBtSuportTipus
        :param cr: Database Cursor
        :type cr: Cursor
        :param uid: User ID
        :type uid: int
        :param vals: Values of the object that is going to be created.
        :type vals: dict[str, Any]
        :param context: OpenERP context
        :type context: dict[str, Any]
        :return: Id of the new created element
        :rtype: int
        """

        if context is None:
            context = {}

        ids = self.search(
            cr, uid,
            ['&', ('code', '=', vals['code']), ('name', '=', vals['name'])]
        )

        if ids and len(ids):
            return ids[0]
        else:
            return super(GiscedataBtSuportTipus, self).create(cr, uid, vals)

    _columns = {
        'name': fields.char('Nom', size=64, required=True),
        'code': fields.char('Codi', size=64, required=True),
        'description': fields.char('Descripció', size=256),
    }


GiscedataBtSuportTipus()


class GiscedataBtSuport(osv.osv):

    _name = 'giscedata.bt.suport'
    _description = 'Suports de Baixa Tensió'

    def _get_nextnumber(self, cursor, uid, context=None):
        """
        Returns the next node numbers from the sequence giscedata.bt.suport
        :param cursor: Database Cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :param context: OpenERP context
        :type context: dic[str,Any]
        :return: Next GiscedataBtSuport name value
        :rtype: str
        """

        return self.pool.get('ir.sequence').get(
            cursor, uid, 'giscedata.bt.suport'
        )

    _columns = {
        'name': fields.char('Codi', size=64, required=True),
        'observacions': fields.text('Observacions'),
        'data_pm': fields.date('Data PM'),
        'baixa': fields.boolean('Baixa'),
        'active': fields.boolean('Actiu'),
        'data_baixa': fields.date('Data Baixa'),
        'tipus': fields.many2one(
            'giscedata.bt.suport.tipus', 'Tipus', required=True
        ),
        'tirant': fields.boolean('Tirant'),
        'tornapuntes': fields.boolean('Tornapuntes'),
        'reforc_neutre': fields.boolean('Reforç Neutre'),
        'parallamps': fields.boolean('Parallamps')
    }

    _defaults = {
        'active': lambda *a: 1,
        'name': _get_nextnumber,
    }


GiscedataBtSuport()
