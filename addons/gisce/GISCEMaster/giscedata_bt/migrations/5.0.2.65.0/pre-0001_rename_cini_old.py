# coding=utf-8
from oopgrade import oopgrade


def up(cursor, installed_version):
    if not installed_version:
        return
    oopgrade.rename_columns(cursor, {
        'giscedata_bt_element': [('cini', 'cini_pre_auto_bt')]
    })


def down(cursor):
    oopgrade.rename_columns(cursor, {
        'giscedata_bt_element': [('cini_pre_auto_bt', 'cini')]
    })


migrate = up
