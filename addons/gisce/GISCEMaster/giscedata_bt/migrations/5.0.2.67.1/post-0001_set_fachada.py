# coding=utf-8
import netsvc
from oopgrade import oopgrade
import pooler


def migrate(cursor, installed_version):
    """
    Fills fachada field using cini_pre_auto_bt data if 5th character is 4

    :param cursor: Database cursor
    :param installed_version:  Installed version
    :return: None
    """
    logger = netsvc.Logger()
    uid = 1
    sql = """
        SELECT id
        FROM giscedata_bt_element
        WHERE cini_pre_auto_bt IS NOT NULL AND
            substring(cini_pre_auto_bt,5,1)='4'
    """
    if oopgrade.column_exists(cursor, 'giscedata_bt_element', 'cini_pre_auto_bt'):
        cursor.execute(sql)
        ids = cursor.fetchall()
        ids = [ident[0] for ident in ids]
        logger.notifyChannel(
            'migration',
            netsvc.LOG_INFO,
            'Found {} bt elements, updating posada_facana field'.format(len(ids))
        )
        pool = pooler.get_pool(cursor.dbname)
        model = pool.get('giscedata.bt.element')
        model.write(cursor, uid, ids, {'posada_facana': 1})
    else:
        logger.notifyChannel(
            'migration',
            netsvc.LOG_INFO,
            'The giscedata_bt_element don\'t have cini_pre_auto_bt column'
        )
