# coding=utf-8

import logging


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')
    logger.info('Setting UP the LBT sequence for the field NAME.')

    sql = """
        UPDATE ir_sequence AS seq 
        SET number_next = data.num
        FROM (
          SELECT coalesce(max(lbt.name::int),0)+1 AS num,
                 seq.id AS id
          FROM giscedata_bt_element lbt, ir_sequence seq
          WHERE seq.name='Numeració LBT'
          AND seq.code='giscedata.bt.element'
          GROUP BY seq.id
        ) AS data
        WHERE seq.id=data.id
    """
    cursor.execute(sql)

    logger.info('LBT NAME sequence set up with the current value')


def down(cursor, installed_version):
    pass


migrate = up