# coding=utf-8

import logging
import pooler


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')
    logger.info(
        "Create new tipus linia BT, 'Aèria Poste' and 'Aèria Façana', and "
        "assign each LBT to the correct type"
    )
    tipus_linia_obj = pooler.get_pool(cursor.dbname).get(
        'giscedata.bt.tipuslinia'
    )
    lbt_obj = pooler.get_pool(cursor.dbname).get('giscedata.bt.element')

    sql = """
        SELECT id, name FROM giscedata_bt_tipuslinia;
    """

    cursor.execute(sql)
    query_result = cursor.dictfetchall()
    tipus_linia_p = False
    tipus_linia_p_id = False
    tipus_linia_f = False
    tipus_linia_f_id = False

    for row in query_result:
        if row['name'] in ('AERIA','AÈRIA', 'Aèria', 'Aeria', 'aèria', 'aeria'):
            tipus_linia_p = row['name']+" Poste"
            tipus_linia_p_id = row['id']
            tipus_linia_f = row['name']+" Façana"
        elif row['name'] in ('AÉREA', 'AEREA', 'Aérea', 'Aerea', 'aérea', 'aerea'):
            tipus_linia_p = row['name'] + " Poste"
            tipus_linia_p_id = row['id']
            tipus_linia_f = row['name'] + " Fachada"

        if tipus_linia_f and tipus_linia_p_id:
            tipus_linia_f_id = tipus_linia_obj.create(
                cursor, 1, {'name': tipus_linia_f}
            )
            if not tipus_linia_f_id:
                logger.warning(
                    "Impossible to create new tipus linia BT correctly"
                )
                return
            tipus_linia_obj.write(
                cursor, 1, [tipus_linia_p_id], {'name': tipus_linia_p}
            )
            logger.info(
                "New tipus linia BT, '{}' and '{}' created correctly.".format(
                    tipus_linia_f, tipus_linia_p
                )
            )
            break

    if tipus_linia_f_id and tipus_linia_p_id:
        lbt_ids = lbt_obj.search(
            cursor, 1, [
                ('tipus_linia', '=', tipus_linia_p_id),
                ('posada_facana', '=', True)
            ]
        )
        if lbt_ids:
            lbt_obj.write(
                cursor, 1, lbt_ids, {'tipus_linia': tipus_linia_f_id}
            )
        logger.info("All LBT assigned to the right tipus linia BT")
    else:
        logger.warning(
            "It have not been possible to assign the lines to the right tipus "
            "linia BT"
        )


def down(cursor, installed_version):
    pass


migrate = up
