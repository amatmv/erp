# -*- coding: utf-8 -*-
from osv import fields, osv


class TgConcentratorInfrestructura(osv.osv):
    '''Implements the basic TG concentrator model'''

    _name = 'tg.concentrator'
    _inherit = 'tg.concentrator'

    _columns = {
        'ct_id': fields.many2one('giscedata.cts', 'CT'),
        'trafo_ids': fields.one2many(
            'giscedata.transformador.trafo',
            'cnc_id',
            'Transformadors',
        )
    }


TgConcentratorInfrestructura()
