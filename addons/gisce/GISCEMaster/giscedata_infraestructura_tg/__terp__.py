# -*- coding: utf-8 -*-
{
    "name": "Infraestructure Telegestió",
    "description": """Module for telegestió"""
                   """infraestructure relation""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends": [
        "giscedata_telegestio",
        "giscedata_telemesures_infraestructura",
        "giscedata_transformadors",
        "giscedata_cts",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "giscedata_transformadors_view.xml",
        "giscedata_telegestio_view.xml",
        "giscedata_cts_view.xml",
    ],
    "active": False,
    "installable": True
}
