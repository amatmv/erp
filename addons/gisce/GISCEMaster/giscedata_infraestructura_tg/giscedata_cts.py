# -*- coding: utf-8 -*-
from osv import fields, osv


class GiscedataCtsTg(osv.osv):
    '''Implements the basic TG concentrator model'''

    _name = 'giscedata.cts'
    _inherit = 'giscedata.cts'

    _columns = {
        'cnc_ids': fields.one2many(
            'tg.concentrator',
            'ct_id',
            'Concentradors Telegestio',
        )
    }


GiscedataCtsTg()