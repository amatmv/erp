# -*- coding: utf-8 -*-
from osv import fields, osv


class GiscedataTransformadorTrafoTG(osv.osv):
    '''Vinculació Trafo amb concentradors de TG
    '''
    _name = 'giscedata.transformador.trafo'
    _inherit = 'giscedata.transformador.trafo'

    _columns = {
        'cnc_id': fields.many2one('tg.concentrator', 'Concentrador')
    }


GiscedataTransformadorTrafoTG()