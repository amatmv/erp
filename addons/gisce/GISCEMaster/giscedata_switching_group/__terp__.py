# -*- coding: utf-8 -*-
{
    "name": "Switching group",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Poder assignar i buscar per grup als casos ATR
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "giscedata_switching",
        "giscedata_polissa_group"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_switching_view.xml"
    ],
    "active": False,
    "installable": True
}
