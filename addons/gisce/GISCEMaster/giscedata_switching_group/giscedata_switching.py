# -*- coding: utf-8 -*-

from osv import osv, fields


class GiscedataSwitching(osv.osv):
    _name = 'giscedata.switching'
    _inherit = 'giscedata.switching'

    def _ff_group_polissa_search(self, cursor, uid, obj, name, args, context=None):
        if not context:
            context = {}
        if not args:
            return [('id', '=', 0)]
        else:
            ids = self.search(cursor, uid, [
                ('cups_polissa_id.group.name', 'ilike', args[0][2])
            ], context=context)
            ids.append(-1)
            return [('id', 'in', ids)]

    def _ff_group_polissa(self, cursor, uid, ids, field_name, arg,
                                 context=None):
        res = dict.fromkeys(ids, False)
        for sw in self.browse(cursor, uid, ids, context=context):
            polissa = sw.cups_polissa_id
            if polissa.group:
                res[sw.id] = polissa.group.id
        return res

    _columns = {
        'group_polissa': fields.function(
            _ff_group_polissa,
            method=True, string='Grup', type='many2one',
            relation='giscedata.polissa.group', readonly=True,
            fnct_search=_ff_group_polissa_search
        ),
    }

GiscedataSwitching()