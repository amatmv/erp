# -*- coding: utf-8 -*-
import base64
import time
import csv
import StringIO
import zipfile
from datetime import datetime

from osv import osv, fields
from tools import config
from tools.sql import install_array_agg
import netsvc

class GiscedataOcsumLectures(osv.osv):

    _name = 'giscedata.ocsum.lectures'
    _auto = False

    _module = 'giscedata_ocsum'

    def get_sql(self, cursor):
        sql = open('%s/%s/sql/lectures.sql'
                   % (config['addons_path'],
                      self._module)).read()
        return sql

    def init(self, cursor):
        # Install array_agg for postgresql < 8.4
        install_array_agg(cursor)
        sql = self.get_sql(cursor)
        cursor.execute("""DROP VIEW IF EXISTS giscedata_ocsum_lectures""")
        cursor.execute('''
            CREATE OR REPLACE VIEW giscedata_ocsum_lectures AS
              (%s)''' % sql)

    _columns = {
        'id': fields.integer('ID', readonly=True),
        'polissa_id': fields.integer('Polissa ID', readonly=True),
        'cups': fields.char('CUPS', size="22", readonly=True),
        'periode': fields.char('Periode', size="6", readonly=True),
        'ano': fields.char('Any', size="4", readonly=True),
        'fecha_lectura_anterior': fields.date('Lectura anterior',
                                              readonly=True),
        'fecha_lectura_actual': fields.date('Lectura actual', readonly=True),
        'tarifa': fields.char('Tarifa', size="6", readonly=True),
        'discriminacion_horaria': fields.char('DH', size="6", readonly=True),
        'energia_activa_p1': fields.float('Activa P1', digits=(16,3),
                                          readonly=True),
        'energia_activa_p2': fields.float('Activa P2', digits=(16,3),
                                          readonly=True),
        'energia_activa_p3': fields.float('Activa P3', digits=(16,3),
                                          readonly=True),
        'energia_activa_p4': fields.float('Activa P4', digits=(16,3),
                                          readonly=True),
        'energia_activa_p5': fields.float('Activa P5', digits=(16,3),
                                          readonly=True),
        'energia_activa_p6': fields.float('Activa P6', digits=(16,3),
                                          readonly=True),
        'energia_activa_p7': fields.float('Activa P7', digits=(16,3),
                                          readonly=True),
        'energia_reactiva_p1': fields.float('Reactiva P1', digits=(16,3),
                                            readonly=True),
        'energia_reactiva_p2': fields.float('Reactiva P2', digits=(16,3),
                                            readonly=True),
        'energia_reactiva_p3': fields.float('Reactiva P3', digits=(16,3),
                                            readonly=True),
        'energia_reactiva_p4': fields.float('Reactiva P4', digits=(16,3),
                                            readonly=True),
        'energia_reactiva_p5': fields.float('Reactiva P5', digits=(16,3),
                                            readonly=True),
        'energia_reactiva_p6': fields.float('Reactiva P6', digits=(16,3),
                                            readonly=True),
        'energia_reactiva_p7': fields.float('Reactiva P7', digits=(16,3),
                                            readonly=True),
        'potencia_maxima_p1': fields.float('Max. P1', digits=(16,3),
                                           readonly=True),
        'potencia_maxima_p2': fields.float('Max. P2', digits=(16,3),
                                           readonly=True),
        'potencia_maxima_p3': fields.float('Max. P3', digits=(16,3),
                                           readonly=True),
        'potencia_maxima_p4': fields.float('Max. P4', digits=(16,3),
                                           readonly=True),
        'potencia_maxima_p5': fields.float('Max. P5', digits=(16,3),
                                           readonly=True),
        'potencia_maxima_p6': fields.float('Max. P6', digits=(16,3),
                                           readonly=True),
        'potencia_maxima_p7': fields.float('Max. P7', digits=(16,3),
                                           readonly=True),

    }

GiscedataOcsumLectures()

class GiscedataOcsumCUPS(osv.osv):

    _name = 'giscedata.ocsum.cups'
    _auto = False

    _module = 'giscedata_ocsum'

    def get_sql(self, cursor):
        sql = open('%s/%s/sql/ocsum.sql'
                   % (config['addons_path'],
                      self._module)).read()
        return sql

    def init(self, cursor):
        # Install array_agg for postgresql < 8.4
        install_array_agg(cursor)
        sql = self.get_sql(cursor)
        cursor.execute("""DROP VIEW IF EXISTS giscedata_ocsum_cups""")
        cursor.execute('''
            CREATE OR REPLACE VIEW giscedata_ocsum_cups as
            (%s)''' % sql)

    _columns = {
        'id': fields.integer('ID', readonly=True),
        'polissa_id': fields.integer('Polissa ID', readonly=True),
        'cups': fields.char('CUPS', size="22", readonly=True),
        'direccion_del_suministro': fields.char('Adreça CUPS', size="250", readonly=True),
        'poblacion_del_suministro': fields.char('Població', size="50", readonly=True),
        'codigos_postal_del_suministro': fields.char('CP', size="10", readonly=True),
        'provincia_del_suministro': fields.char('Provincia', size="100", readonly=True),
        'empresa_distribuidora': fields.char('Distribuïdora', size="100", readonly=True),
        'codigo_empresa_distribuidora': fields.char('Codi', size="4", readonly=True),
        'documento_identidad': fields.char("Documento identidad", size="15", readonly=True),
        'tipo_documento_identidad': fields.char('Tipo de documento', size="20", readonly=True),
        'identificador': fields.char('Identificador', size="25", readonly=True),
        'nombre_y_apellidos_denominacion_social': fields.char('Titular', size="250", readonly=True),
        'domicilio_del_titular': fields.char('Dom. Titular', size="250", readonly=True),
        'poblacion_del_titular': fields.char('Pob. Titular', size="250", readonly=True),
        'codigo_postal_del_titular': fields.char('CP Titular', size="10", readonly=True),
        'provincia_del_titular': fields.char('Prov. Titular', size="50", readonly=True),
        'tipo_vivienda': fields.char('Tipus de vivenda', size="40", readonly=True),
        'impagos': fields.char('Impagament', size="40", readonly=True),
        'deposito_de_garantia': fields.char('Garantia', size="30", readonly=True),
        'autorizacion_cesion_datos': fields.char('Autorització', size="2", readonly=True),
        'fecha_alta': fields.date('Data alta', readonly=True),
        'tarifa': fields.char('Tarifa', size="10", readonly=True),
        'tipo_coeficiente': fields.char('Coeficiente', size="16", readonly=True),
        'tension': fields.integer('Tensió', readonly=True),
        'potencia_maxima_autorizada': fields.float('Pot. Max.', digits=(16,3), readonly=True),
        'potencia_maxima_autorizada_acta_puesta_en_marcha': fields.float('Pot. Act.', digits=(16,3), readonly=True),
        'tipo_de_punto_de_medida': fields.integer('Tipus de punt', readonly=True),
        'icp_instalado': fields.char('ICP Instal·lat', size="4", readonly=True),
        'tipo_perfil_de_consumo': fields.char('Tipo perfil', size="2", readonly=True),
        'discriminacion_horaria': fields.char('Discriminació', size="6", readonly=True),
        'derechos_de_acceso_reconocidos': fields.float('Drets d\'accés reconeguts', digits=(16,3), readonly=True),
        'derechos_de_extension_reconocidos': fields.float('Drets d\'extensió reconeguts', digits=(16,3), readonly=True),
        'propiedad_equipo_medida': fields.char('Propietat Equip de mesura', size="20", readonly=True),
        'propiedad_del_icp': fields.char('Propietat ICP', size="20", readonly=True),
        'potencia_periodo_1': fields.float('Pot. P1', digits=(16,3), readonly=True),
        'potencia_periodo_2': fields.float('Pot. P2', digits=(16,3), readonly=True),
        'potencia_periodo_3': fields.float('Pot. P3', digits=(16,3), readonly=True),
        'potencia_periodo_4': fields.float('Pot. P4', digits=(16,3), readonly=True),
        'potencia_periodo_5': fields.float('Pot. P5', digits=(16,3), readonly=True),
        'potencia_periodo_6': fields.float('Pot. P6', digits=(16,3), readonly=True),
        'potencia_periodo_7': fields.float('Pot. P7', digits=(16,3), readonly=True),
        'potencia_periodo_8': fields.float('Pot. P8', digits=(16,3), readonly=True),
        'potencia_periodo_9': fields.float('Pot. P9', digits=(16,3), readonly=True),
        'potencia_periodo_10': fields.float('Pot. P10', digits=(16,3), readonly=True),
        'fecha_ultimo_movimiento_de_contratacion': fields.date('Ult. Movimiento', readonly=True),
        'fecha_ultimo_cambio_comercializador': fields.date('Ult. Cambio comer.', readonly=True),
        'fecha_limite_derechos_de_extension': fields.date('Data limit drets extensió', readonly=True),
        'fecha_ultima_lectura': fields.date('Data darrera lectura', readonly=True)
    }

GiscedataOcsumCUPS()


class GiscedataOcsumLOPD(osv.osv):

    _name = 'giscedata.ocsum.lopd'
    _auto = False

    _module = 'giscedata_ocsum'

    def get_sql(self, cursor):
        sql = open('%s/%s/sql/lopd.sql'
                   % (config['addons_path'],
                      self._module)).read()
        return sql

    def init(self, cursor):
        # Install array_agg for postgresql < 8.4
        install_array_agg(cursor)
        sql = self.get_sql(cursor)
        cursor.execute("""DROP VIEW IF EXISTS giscedata_ocsum_lopd""")
        cursor.execute('''
            CREATE OR REPLACE VIEW giscedata_ocsum_lopd AS
              (%s)''' % sql)

    _columns = {
        'id': fields.integer('ID', readonly=True),
        'name': fields.integer('Titular ID', size="15", readonly=True),
        'tipo_documento_identidad': fields.char('Tipo de documento', size="20",
                                                readonly=True),
        'documento_identidad': fields.char("Documento identidad", size="15",
                                           readonly=True),
        'nombre_titular': fields.char("Nombre Titular", size="250",
                                      readonly=True),
        'fecha_alta': fields.date('Data alta', readonly=True),
        'observaciones': fields.text('Observaciones', readonly=True),
    }

GiscedataOcsumLOPD()


class GiscedataOcsumExport(osv.osv_memory):
    """Assistent per exportar dades de l'OCSUM.
    """
    _name = 'giscedata.ocsum.export'

    _columns = {
        'name': fields.char('Nom', size=256),
        'file': fields.binary('Fitxer'),
        'state': fields.selection([('init', 'Init'), ('end', 'End')],
                                  'Estat')
    }
    _defaults = {
        'state': lambda *a: 'init',
        'name': lambda *a: 'OCSUM_%s.txt' % time.strftime('%Y%m%d%H%M%S')
    }

    def export_file(self, cursor, uid, ids, context=None):
        """Funció per exportar el fitxer.
        """
        ocsum_cups_obj = self.pool.get('giscedata.ocsum.cups')
        ocsum_lect_obj = self.pool.get('giscedata.ocsum.lectures')

        HEADERS_LECT = ([
            u"ANO",
            u"FECHA_LECTURA_ANTERIOR",
            u"FECHA_LECTURA_ACTUAL",
            u"TARIFA",
            u"DISCRIMINACION_HORARIA",
            u"ENERGIA_ACTIVA_P1",
            u"ENERGIA_ACTIVA_P2",
            u"ENERGIA_ACTIVA_P3",
            u"ENERGIA_ACTIVA_P4",
            u"ENERGIA_ACTIVA_P5",
            u"ENERGIA_ACTIVA_P6",
            u"ENERGIA_ACTIVA_P7",
            u"ENERGIA_REACTIVA_P1",
            u"ENERGIA_REACTIVA_P2",
            u"ENERGIA_REACTIVA_P3",
            u"ENERGIA_REACTIVA_P4",
            u"ENERGIA_REACTIVA_P5",
            u"ENERGIA_REACTIVA_P6",
            u"ENERGIA_REACTIVA_P7",
            u"POTENCIA_MAXIMA_P1",
            u"POTENCIA_MAXIMA_P2",
            u"POTENCIA_MAXIMA_P3",
            u"POTENCIA_MAXIMA_P4",
            u"POTENCIA_MAXIMA_P5",
            u"POTENCIA_MAXIMA_P6",
            u"POTENCIA_MAXIMA_P7"
        ])

        HEADERS_CUPS = [
            u"CUPS",
            u"DIRECCION_DEL_SUMINISTRO",
            u"POBLACION_DEL_SUMINISTRO",
            u"CODIGO_POSTAL_DEL_SUMINISTRO",
            u"PROVINCIA_DEL_SUMINISTRO",
            u"EMPRESA_DISTRIBUIDORA",
            u"CODIGO_EMPRESA_DISTRIBUIDORA",
            u"IDENTIFICADOR",
            u"NOMBRE_Y_APELLIDOS_DENOMINACION_SOCIAL",
            u"DOMICILIO_DEL_TITULAR",
            u"POBLACION_DEL_TITULAR",
            u"CODIGO_POSTAL_DEL_TITULAR",
            u"PROVINCIA_DEL_TITULAR",
            u"TIPO_VIVIENDA",
            u"IMPAGOS",
            u"DEPOSITO_DE_GARANTIA",
            u"AUTORIZACION_CESION_DATOS",
            u"FECHA_ALTA",
            u"TARIFA",
            u"TENSION",
            u"POTENCIA_MAXIMA_AUTORIZADA",
            u"POTENCIA_MAXIMA_AUTORIZADA_ACTA_PUESTA_EN_MARCHA",
            u"TIPO_DE_PUNTO_DE_MEDIDA",
            u"ICP_INSTALADO",
            u"TIPO_PERFIL_DE_CONSUMO",
            u"DISCRIMINACION HORARIA",
            u"DERECHOS_DE_ACCESO_RECONOCIDOS",
            u"DERECHOS_DE_EXTENSION_RECONOCIDOS",
            u"PROPIEDAD_EQUIPO_MEDIDA",
            u"PROPIEDAD_DEL_ICP",
            u"POTENCIA_PERIODO_1",
            u"POTENCIA_PERIODO_2",
            u"POTENCIA_PERIODO_3",
            u"POTENCIA_PERIODO_4",
            u"POTENCIA_PERIODO_5",
            u"POTENCIA_PERIODO_6",
            u"POTENCIA_PERIODO_7",
            u"POTENCIA_PERIODO_8",
            u"POTENCIA_PERIODO_9",
            u"POTENCIA_PERIODO_10",
            u"FECHA_ULTIMO_MOVIMIENTO_DE_CONTRATACION",
            u"FECHA_ULTIMO_CAMBIO_COMERCIALIZADOR",
            u"FECHA_LIMITE_DERECHOS_DE_EXTENSION",
            u"FECHA_ULTIMA_LECTURA"
        ]
        logger = netsvc.Logger()
        logger.notifyChannel("addons", netsvc.LOG_INFO,
                             "Starting OCSUM export")
        #Get the sql from ocsum lectures model
        sql = ocsum_lect_obj.get_sql(cursor)
        cursor.execute(sql)
        lectures = {}
        max_lect = {}
        for line in cursor.fetchall():
            line = list(line)
            polissa = line[1]
            #Delete first four fields from lectures sql
            #Not used when exporting
            del line[0:4]
            lectures.setdefault(polissa, [])
            lectures[polissa].extend(line)
            max_lect.setdefault(polissa, 0)
            max_lect[polissa] += 1
        max_lect = max(max_lect.values())

        HEADERS = (HEADERS_CUPS + (HEADERS_LECT * max_lect))

        sql = ocsum_cups_obj.get_sql(cursor)
        cursor.execute(sql)
        # Indexem per pòlissa
        informe = [','.join(HEADERS)]
        for line in cursor.fetchall():
            line = list(line)
            polissa = line[1]
            #Delete first two fields from query not used when exporting
            del line[0:2]
            line.extend(lectures.get(polissa, []))
            informe.append(','.join([unicode(a or '') for a in line]))
        informe = '\n'.join(informe)
        informe += '\n'
        mfile = base64.b64encode(informe.encode('utf-8'))
        self.write(cursor, uid, ids, {'state': 'end', 'file': mfile}, context)
        logger.notifyChannel("addons", netsvc.LOG_INFO,
                             "Ending OCSUM export")

GiscedataOcsumExport()


class GiscedataCNMCExport(osv.osv_memory):
    """Wizard for exporting CNMC SIPS format.
    """
    _name = 'giscedata.cnmc.sips.export'

    _columns = {
        'name': fields.char('Nom', size=256),
        'file': fields.binary('Fitxer'),
        'state': fields.selection([('init', 'Init'), ('end', 'End')],
                                  'Estat')
    }
    _defaults = {
        'state': lambda *a: 'init',
        'name': lambda *a: '%s_electricidad.zip' % time.strftime('%Y-%m-%d')
    }

    def export_file(self, cursor, uid, ids, context=None):
        """Funció per exportar el fitxer.
        """
        ocsum_cups_obj = self.pool.get('giscedata.ocsum.cups')
        ocsum_lect_obj = self.pool.get('giscedata.ocsum.lectures')
        ocsum_lopd_obj = self.pool.get('giscedata.ocsum.lopd')

        HEADERS_CUPS = [
            u"codigoEmpresaEmisora",
            u"cups",
            u"codigoPostalPS",
            u"direccionCompletaPS",
            u"poblacionPS",
            u"fechaAltaSuministro",
            u"tarifaEnVigor",
            u"tension",
            u"potenciaMaximaBIEEnW",
            u"potenciaMaximaAPMEnW",
            u"clasificacionPS",
            u"disponibilidadICP",
            u"tipoPerfilConsumo",
            u"valorDerechosExtensionEnW",
            u"valorDerechosAccesoEnW",
            u"propiedadEquipoMedida",
            u"propiedadICP",
            u"potenciasContratadasEnW",
            u"fechaUltimoMovimientoContrato",
            u"fechaUltimoCambioComercializador",
            u"fechaLimiteDerechosReconocidos",
            u"fechaUltimaLectura",
            u"informacionImpagos",
            u"importeDepositoGarantia",
            u"tipoIdTitular",
            u"idTitular",
            u"nombreCompletoTitular",
            u"direccionCompletaTitular",
            u"esViviendaHabitual",
        ]
        
        HEADERS_LECT = ([
            u"cups",
            u"fechaInicioConsumo",
            u"fechaFinConsumo",
            u"consumoEnergiaActivaEnKWh",
            u"consumoEnergiaReactivaEnKVAR",
            u"potenciaDemandadaEnW",
        ])
        
        HEADERS_LOPD = [
            u"tipoIdTitular",
            u"idTitular",
            u"nombreCompletoTitular",
            u"fechaEjercicioDerecho",
            u"observaciones",
        ]

        zip_io = StringIO.StringIO()
        zip = zipfile.ZipFile(zip_io, 'w',
                              compression=zipfile.ZIP_DEFLATED)

        logger = netsvc.Logger()
        logger.notifyChannel("addons", netsvc.LOG_INFO,
                             "Starting CNMC SIPS export")

        sql = ocsum_cups_obj.get_sql(cursor)
        cursor.execute(sql)
        # Indexem per pòlissa
        output = StringIO.StringIO()
        writer = csv.writer(output, delimiter=',')
        writer.writerow(HEADERS_CUPS)
    
        for data in cursor.dictfetchall():
            domicilio = '%s %s %s %s' %(data['domicilio_del_titular'],
                                        data['codigo_postal_del_titular'],
                                        data['poblacion_del_titular'],
                                        data['provincia_del_titular'])
            line = [data['codigo_empresa_distribuidora'],
                    data['cups'],
                    data['codigo_postal_del_suministro'],
                    '',  # data['direccion_del_suministro'],
                    data['poblacion_del_suministro'],
                    data['fecha_alta'],
                    data['tarifa'],
                    data['tension'],
                    int(data['potencia_maxima_autorizada'] * 1000),
                    int(data['potencia_maxima_autorizada_acta_puesta_en_marcha']
                     * 1000),
                    data['tipo_de_punto_de_medida'],
                    data['icp_instalado'] == 'ICP instalado' and 1 or 0,
                    (data['tipo_coeficiente']
                     and 'P%s' % data['tipo_coeficiente'][-1] or ''),
                    int(data['derechos_de_extension_reconocidos'] * 1000),
                    int(data['derechos_de_acceso_reconocidos']
                     and data['derechos_de_acceso_reconocidos'] * 1000 or 0),
                    (data['propiedad_equipo_medida'][0]),
                    (data['propiedad_del_icp']
                     and data['propiedad_del_icp'][0] or ''),
                    ';'.join(p for p in [str(int(data['potencia_periodo_1']
                               and data['potencia_periodo_1'] * 1000 or 0)),
                              str(int(data['potencia_periodo_2']
                               and data['potencia_periodo_2'] * 1000 or 0)),
                              str(int(data['potencia_periodo_3']
                               and data['potencia_periodo_3'] * 1000 or 0)),
                              str(int(data['potencia_periodo_4']
                               and data['potencia_periodo_4'] * 1000 or 0)),
                              str(int(data['potencia_periodo_5']
                               and data['potencia_periodo_5'] * 1000 or 0)),
                              str(int(data['potencia_periodo_6']
                               and data['potencia_periodo_6'] * 1000 or 0))]
                             if p != '0'),
                    data['fecha_ultimo_movimiento_de_contratacion'],
                    data['fecha_ultimo_cambio_comercializador'],
                    data['fecha_limite_derechos_de_extension'],
                    data['fecha_ultima_lectura'],
                    data['impagos'],
                    0,
                    data['tipo_documento_identidad'],
                    '',  # data['documento_identidad'] 
                    '',  # data['nombre_y_apellidos_denominacion_social'],
                    '',  # domicilio,
                    (data['tipo_vivienda'] == 'Vivienda habitual'
                     and 1 or 0),
                   ]
            tmp = [isinstance(t, basestring) and t.encode('utf-8')
                   or t for t in line]
            writer.writerow(line)

        date_now = datetime.strftime(datetime.now(), '%Y-%m-%d')
        zip.writestr('%s_electricidad_sips.csv' % date_now, 
                     output.getvalue())

        # _electricidad_consumos.csv
        output = StringIO.StringIO()
        writer = csv.writer(output, delimiter=',')
        writer.writerow(HEADERS_LECT)
        
        #Get the sql from ocsum lectures model
        sql = ocsum_lect_obj.get_sql(cursor)
        cursor.execute(sql)
        for row in cursor.fetchall():
            row = list(row)
            cups = row[2]
            fecha_inicio = row[5]
            fecha_fin = row[6]
            
            activa = row[9:15]
            reactiva = row[15:21]
            potencies = []
            for p in row[21:27]:
                if p:
                    str(potencies.append(str(int(p * 1000))))
                else:
                    potencies.append('0')
            activa_txt = ';'.join([a and str(a) or '0' for a in activa])
            reactiva_txt = ';'.join([r and str(r) or '0' for r in reactiva])
            potencies_txt = ';'.join(potencies)
            line = [cups, fecha_inicio, fecha_fin, activa_txt, reactiva_txt,
                    potencies_txt]

            tmp = [isinstance(t, basestring) and t.encode('utf-8')
                   or t for t in line]
            writer.writerow(line)
 
        zip.writestr('%s_electricidad_consumos.csv' % date_now,
                     output.getvalue())
        
        # _electricidad_lopd.csv
        # output = StringIO.StringIO()
        # writer = csv.writer(output, delimiter=',')
        # writer.writerow(HEADERS_LOPD)
        #
        # # Get the sql from lopd lectures model
        # sql = ocsum_lopd_obj.get_sql(cursor)
        # cursor.execute(sql)
        # for row in cursor.fetchall():
        #     line = list(row)
        #     tmp = [isinstance(t, basestring) and t.encode('utf-8')
        #            or t for t in line]
        #     writer.writerow(line)
        #
        # zip.writestr('%s_electricidad_lopd.csv' % date_now,
        #              output.getvalue())

        zip.close()
        mfile = base64.b64encode(zip_io.getvalue())
        self.write(cursor, uid, ids, {'state': 'end', 'file': mfile}, context)
        logger.notifyChannel("addons", netsvc.LOG_INFO,
                             "Ending CNMC SIPS export")

GiscedataCNMCExport()
