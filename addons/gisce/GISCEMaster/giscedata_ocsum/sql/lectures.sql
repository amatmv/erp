SELECT
  lectures.lectura_id AS id,
  f.polissa_id as polissa_id,
  cups.name AS cups,
  to_char(lot.data_final, 'YYYYMM') as periode,
  to_char(f.data_final, 'YYYY') as ano,
  lectures.data_anterior AS fecha_lectura_anterior,
  lectures.data_actual AS fecha_lectura_actual,
  tarifa.name as tarifa,
  CASE
    WHEN f.potencia < 15 THEN
      (CASE
        WHEN tarifa_periodes.num_periodes = 1 THEN 'NO'
        ELSE 'SI'
      END)
    ELSE 'DH'||tarifa_periodes.num_periodes::varchar
  END AS discriminacion_horaria,
  energies.consums[1] as energia_activa_p1,
  energies.consums[2] as energia_activa_p2,
  energies.consums[3] as energia_activa_p3,
  energies.consums[4] as energia_activa_p4,
  energies.consums[5] as energia_activa_p5,
  energies.consums[6] as energia_activa_p6,
  reactiva.consums[1] as energia_reactiva_p1,
  reactiva.consums[2] as energia_reactiva_p2,
  reactiva.consums[3] as energia_reactiva_p3,
  reactiva.consums[4] as energia_reactiva_p4,
  reactiva.consums[5] as energia_reactiva_p5,
  reactiva.consums[6] as energia_reactiva_p6,
  potencia.maximetre[1] as potencia_maxima_p1,
  potencia.maximetre[2] as potencia_maxima_p2,
  potencia.maximetre[3] as potencia_maxima_p3,
  potencia.maximetre[4] as potencia_maxima_p4,
  potencia.maximetre[5] as potencia_maxima_p5,
  potencia.maximetre[6] as potencia_maxima_p6
FROM giscedata_facturacio_factura f
INNER JOIN giscedata_polissa p
  ON p.id = f.polissa_id
INNER JOIN giscedata_cups_ps cups
  ON cups.id = f.cups_id
INNER JOIN giscedata_facturacio_lot lot ON
(f.lot_facturacio = lot.id
 and to_char(lot.data_final, 'YYYYMM') < to_char(now(), 'YYYYMM')
 and to_char(lot.data_final, 'YYYYMM') > to_char(now() -
 interval '2 year', 'YYYYMM'))
INNER JOIN account_invoice i
ON (f.invoice_id = i.id and i.type = 'out_invoice')
LEFT JOIN account_journal j
ON (i.journal_id = j.id and j.code ilike 'ENERGIA%')
LEFT JOIN (SELECT min(l.data_anterior) as data_anterior,
                  max(l.data_actual) as data_actual,
                  min(l.id) as lectura_id,
                  l.factura_id as factura_id
           FROM giscedata_facturacio_lectures_energia l
           GROUP BY l.factura_id) lectures
ON (lectures.factura_id = f.id)
LEFT JOIN giscedata_polissa_tarifa tarifa
ON (f.tarifa_acces_id = tarifa.id)
LEFT JOIN (SELECT array_agg(name) as periodes,
          array_agg(qty) as consums,
          factura_id
       FROM (SELECT il.name,
            sum(il.quantity) as qty,
            l.factura_id
         FROM giscedata_facturacio_factura_linia l
         INNER JOIN account_invoice_line il
         ON (l.invoice_line_id = il.id)
         WHERE l.tipus = 'energia'
         GROUP BY l.factura_id, il.name
             ORDER BY l.factura_id, il.name) as foo
       GROUP BY factura_id) energies
ON (energies.factura_id = f.id)
LEFT JOIN (SELECT array_agg(name) as periodes,
          array_agg(qty) as consums,
          factura_id
       FROM (SELECT il.name,
            sum(il.quantity) as qty,
            l.factura_id
         FROM giscedata_facturacio_factura_linia l
         INNER JOIN account_invoice_line il
         ON (l.invoice_line_id = il.id)
         WHERE l.tipus = 'reactiva'
         GROUP BY l.factura_id, il.name
             ORDER BY l.factura_id, il.name) as foo
       GROUP BY factura_id) reactiva
ON (reactiva.factura_id = f.id)
LEFT JOIN (SELECT array_agg(name) as periodes,
          array_agg(pot_maximetre) as maximetre,
          factura_id
       FROM (SELECT l.name,
            l.pot_maximetre,
            l.factura_id
         FROM giscedata_facturacio_lectures_potencia l
         GROUP BY l.factura_id, l.name, l.pot_maximetre
             ORDER BY l.factura_id, l.name) as foo
       GROUP BY foo.factura_id) potencia
       ON (potencia.factura_id = f.id)
LEFT JOIN (SELECT count(id) as num_periodes, tarifa
           FROM giscedata_polissa_tarifa_periodes
           WHERE tipus = 'te' group by tarifa) tarifa_periodes
           ON tarifa_periodes.tarifa = f.tarifa_acces_id
WHERE f.tipo_rectificadora in ('N', 'R') and f.tipo_factura = '01'
AND p.state not in ('esborrany', 'validar')
AND f.id NOT IN (SELECT DISTINCT ref
                 FROM giscedata_facturacio_factura
                 WHERE coalesce(ref, 0) <> 0)
AND coalesce(lectures.lectura_id, 0) <> 0
ORDER BY polissa_id asc, periode desc
