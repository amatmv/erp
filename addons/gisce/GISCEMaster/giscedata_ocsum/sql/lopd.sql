SELECT
    CASE
        WHEN substring(titular.vat,1,2) = 'ES' and substring(titular.vat,3,1) in ('0','1','2','3','4','5','6','7','8','9')
          THEN 'DNI'
        WHEN substring(titular.vat,1,2) = 'ES' and substring(titular.vat,3,1) in ('X','Y')
          THEN 'NIE'
        WHEN substring(titular.vat,1,2) = 'PS'
          THEN 'PASAPORTE'
        ELSE 'CIF'
    END AS tipo_documento_identidad,
  titular.vat AS documento_identidad,
  titular.name AS nombre_titular,
  titular.lopd_data_alta AS fecha_alta,
  titular.lopd_comment AS observaciones
FROM res_partner AS titular
WHERE titular.lopd_active
