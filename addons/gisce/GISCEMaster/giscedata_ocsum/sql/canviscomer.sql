SELECT
s.code||'000' as provincia_0,
(select ref2 from res_partner where id = 1) as distribuidor1,
r1.ref2 as Comer_entrante_2,
r2.ref2 as Comer_saliente_3,
'C3' as TipoCambio_4,
p.agree_tipus::int as TipoPunto_5,
(SELECT CASE WHEN t.name = '2.0A' THEN '1'
  WHEN t.name = '2.0DHA' THEN '2'
  WHEN t.name = '3.0A' THEN '3'
  WHEN t.name = '3.0A LB' THEN '3'
  WHEN t.name = '3.1A' THEN '4'
  WHEN t.name = '3.1A LB' THEN '4'
  WHEN t.name = '6.1' THEN '5'
  WHEN t.name = '6.2' THEN '6'
  WHEN t.name = '6.3' THEN '7'
  WHEN t.name = '6.4' THEN '8'
  WHEN t.name = '6.5' THEN '9'
  WHEN t.name = '2.1A' THEN '1T'
  WHEN t.name = '2.1DHA' THEN '2T'
  WHEN t.name = '2.0DHS' THEN '2S'
  WHEN t.name = '2.1DHS' THEN '2V'
  END) as TarifaATR_6,
count(m1.polissa_id) as TotalSolicitudesEnviadas_7,
'0' as  SolicitudesAnuladas_8,
'0' as Reposiciones_9,
null as ClientesSalientes_10,
'0' as NumImpagados_11,
'00' as TipoRetraso_12,
'0.0' as TMSolicitudesAceptadas_13,
count(m1.polissa_id) as NumSolicitudesAceptadas_14,
'00' as TipoRetraso_15,
'0.0' as TMActivacion_16,
'0' as NumIncidencies_17,
count(m1.polissa_id) as NumSolicitudesActivadas_18
FROM giscedata_polissa_modcontractual m1
INNER JOIN giscedata_polissa_modcontractual m2
ON (m1.modcontractual_ant = m2.id
and m1.comercialitzadora != m2.comercialitzadora)
INNER JOIN giscedata_polissa p ON m1.polissa_id = p.id
INNER JOIN res_partner r1 ON m1.comercialitzadora = r1.id
INNER JOIN res_partner r2 ON m2.comercialitzadora = r2.id
INNER JOIN giscedata_polissa_tarifa t ON m1.tarifa = t.id
INNER JOIN giscedata_cups_ps c ON m1.cups = c.id
INNER JOIN res_municipi v ON c.id_municipi = v.id
INNER JOIN res_country_state s ON v.state = s.id
WHERE m1.data_inici >= %(data)s
and m1.data_inici <= %(data)s
GROUP BY s.code, r1.ref2, r2.ref2, p.agree_tipus, t.name
ORDER BY s.code, r1.ref2, r2.ref2, p.agree_tipus, t.name;
