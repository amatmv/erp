#so-8859-1 -*-
from osv import osv, fields
from tools.sql import install_array_agg
from tools import config
from datetime import datetime
import base64
import time
import netsvc
from tools import config
from libcomxml.core import XmlField, XmlModel
#
# Reports
#

class Cabecera(XmlModel):

    _sort_order = ('agente', 'tipo_mercado', 'tipo_agente', 'periodo')
    
    def __init__(self):
	self.datos = XmlField('Cabecera')
	self.agente = XmlField('CodigoAgente')
        self.tipo_mercado = XmlField('TipoMercado')
        self.tipo_agente = XmlField('TipoAgente')
        self.periodo = XmlField('Periodo')
        super(Cabecera, self).__init__('Cabecera', 'datos')
 

class SolicitudesRealizadas(XmlModel):
 
    _sort_order = ('datos_solicitudes', )#, 'detalle_aceptadas', 'detalle_activadas')
    
    def __init__(self):


        self.datos = XmlField('SolicitudesRealizadas')
        self.datos_solicitudes = []
#        self.detalle_aceptadas = []
#        self.detalle_activadas = []
        super(SolicitudesRealizadas, self).__init__('SolicitudesRealizadas', 'datos')
 
class DatosSolicitudes(XmlModel):
 
    _sort_order = ('provincia', 'distribuidor', 'comer_entrante', 'comer_saliente', 'tipo_cambio', 'tipo_punto', 'tarifa_atr', 'total_solicitudes_enviadas', 'solicitudes_anuladas', 'reposiciones', 'clientes_salientes', 'num_impagados', 'detalle_aceptadas', 'detalle_activadas')
    
    def __init__(self):
        self.datos = XmlField('DatosSolicitudes')
        self.provincia = XmlField('Provincia')
        self.distribuidor = XmlField('Distribuidor')
        self.comer_entrante = XmlField('Comer_entrante')
        self.comer_saliente = XmlField('Comer_saliente')
        self.tipo_cambio = XmlField('TipoCambio')
        self.tipo_punto = XmlField('TipoPunto')
        self.tarifa_atr = XmlField('TarifaATR')
        self.total_solicitudes_enviadas = XmlField('TotalSolicitudesEnviadas')
        self.solicitudes_anuladas = XmlField('SolicitudesAnuladas')
        self.reposiciones = XmlField('Reposiciones')
        self.clientes_salientes = XmlField('ClientesSalientes')
        self.numero_impagados = XmlField('NumImpagados')
        self.detalle_aceptadas = []
        self.detalle_activadas = []
        super(DatosSolicitudes, self).__init__('DatosSolicitudes', 'datos')

class DetalleAceptadas(XmlModel):

#    _sort_order = ('tipo_retraso', 'tm_solicitudes_aceptadas', 'num_solicitudes_aceptadas')

    def __init__(self):
        self.datos = XmlField('DetalleAceptadas')
        self.tipo_retraso = XmlField('TipoRetraso')
        self.tm_solicitudes_aceptadas = XmlField('TMSolicitudesAceptadas')
        self.num_solicitudes_aceptadas = XmlField('NumSolicitudesAceptadas')
        super(DetalleAceptadas, self).__init__('DetalleAceptadas', 'datos')
 

class DetalleActivadas(XmlModel):

#    _sort_order = ('tipo_retraso', 'tm_activacion', 'num_incidencias','num_solicitudes_activadas')

    def __init__(self):
        self.datos = XmlField('DetalleActivadas')
        self.tipo_retraso = XmlField('TipoRetraso')
        self.tm_activacion = XmlField('TMActivacion')
        self.num_incidencias = XmlField('NumIncidencias')
        self.num_solicitudes_activadas = XmlField('NumSolicitudesActivadas')
        super(DetalleActivadas, self).__init__('DetalleActivadas', 'datos')


class CambiosSuministrador(XmlModel):
 
    def __init__(self):

	xmlns="http://www.w3.org/2001/XMLSchema-instance" 
	xsi="SolicitudesRealizadas_v1.0.xsd"

        self.datos = XmlField('MensajeSolicitudesRealizadas', attributes={'xmlns': xmlns,'xsi': xsi})
        self.cabecera = Cabecera()
        self.solicitudes_realizadas = SolicitudesRealizadas()
#        self.detalle_aceptadas = DetalleAceptadas()
#        self.detalle_activadas = DetalleActivadas()
        super(CambiosSuministrador, self).__init__('MensajeSolicitudesRealizadas', 'datos')


class giscedata_ocsum_canvis_comer(osv.osv_memory):
    """Assistent per exportar dades de la OCSUM.
    """
    _name = 'giscedata.ocsum.canvis.comer'

    _columns = {
        'data': fields.date('Data'),
        'name': fields.char('Nom', size=256),
        'file': fields.binary('Fitxer'),
        'state': fields.selection([('init', 'Init'), ('end', 'End')],
                                  'Estat')
    }

    _defaults = {
        'state': lambda *a: 'init',
        'name': lambda *a: 'CNE_%s.txt' % time.strftime('%Y%m%d%H%M%S')
    }

    def export_file(self, cursor, uid, ids, context=None):

        logger = netsvc.Logger()
        logger.notifyChannel("addons", netsvc.LOG_INFO,
                             "Starting OCSUM export")

#        Executem informes

        wizard = self.browse(cursor, uid, ids[0], context=context)

        sql = open('%s/%s/sql/canviscomer.sql'
                   % (config['addons_path'],
                      self._module)).read()


        params = {'data': wizard.data}

	r1 = self.pool.get('res.users').browse(cursor, uid, uid).company_id.codi_r1
	data = wizard.data
        year, month, day = data.split('-')

        cursor.execute(sql,(params))

	cambios = CambiosSuministrador()
	cambios.cabecera.feed({
#	    'agente': 'R1-323'
	    'agente': 'R1-' + str(r1),
	    'tipo_mercado': 'E',
	    'tipo_agente': 'D',
#	    'periodo': '201312'
	    'periodo': str(year) + str(month)
	})
       

	for line in cursor.fetchall():
	    datos_solicitudes = DatosSolicitudes()
	    datos_solicitudes.feed({
		'provincia': line[0],
		'distribuidor': line[1],
		'comer_entrante': line[2],
		'comer_saliente': line[3],
		'tipo_cambio': line[4],
		'tipo_punto': line[5],
		'tipo_atr': line[6],
		'total_solicitudes_enviadas': line[7],
		'solicitudes_enviadas': line[8],
		'reposiciones': line[9],
		'clientes_salientes': line[10],
		'numero_impagados': line[11]
	    })

	    detalle_aceptadas = DetalleAceptadas()
	    detalle_aceptadas.feed({
		'tipo_retraso': line[12],
		'tm_solicitudes_aceptadas': line[13],
		'num_solicitudes_aceptadas': line[14]
	    })
            datos_solicitudes.detalle_aceptadas.append(detalle_aceptadas)

            detalle_activadas = DetalleActivadas()
            detalle_activadas.feed({
                'tipo_retraso': line[15],
                'tm_activacion': line[16],
                'num_incidencias': line[17],
                'num_solicitudes_activadass': line[18]
            })
            datos_solicitudes.detalle_activadas.append(detalle_activadas)
	    cambios.solicitudes_realizadas.datos_solicitudes.append(datos_solicitudes)

	cambios.build_tree()

	informe = []
        informe.append(str(cambios))
        informe = '\n'.join(informe)
        informe += '\n'
        mfile = base64.b64encode(informe.encode('utf-8'))
        self.write(cursor, uid, ids, {'state': 'end', 'file': mfile}, context)
        logger.notifyChannel("addons", netsvc.LOG_INFO,
                             "Ending OCSUM export")

giscedata_ocsum_canvis_comer()

