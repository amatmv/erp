# -*- coding: utf-8 -*-
{
    "name": "OCSUM",
    "description": """
    This module provide :
        * Exportació CSV per la OCSUM
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_administracio_publica_cne",
        "giscedata_cups_distri",
        "giscedata_facturacio_distri",
        "giscedata_polissa_distri"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_ocsum_view.xml",
        "res_partner_view.xml",
        "wizard/wizard_ocsum_canvis_comer_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
