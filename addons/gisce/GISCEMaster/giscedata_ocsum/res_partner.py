# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _
from datetime import datetime, timedelta


class ResPartner(osv.osv):
    """Afegim dades per marcar Partners dels que no es pot publicar la seva
       informació als SIPS perquè han exercit el seu dret no segons la LOPD
       (Robinson)
    """
    _name = 'res.partner'
    _inherit = 'res.partner'

    def onchange_lopd_active(self, cursor, uid, ids, lopd_active,
                             lopd_data_alta):

        res = {'value': {}}

        if lopd_active:
            if not lopd_data_alta:
                avui = datetime.today().strftime("%Y-%m-%d")
                res['value'].update({'lopd_data_alta': avui})
        return res

    _columns = {
        'lopd_active': fields.boolean(u'No publicar',
                                      help=u'Aquest client ha sol·licitat que '
                                           u'no es difonguin les seves dades '
                                           u'acollint-se a la lopd'),
        'lopd_data_alta': fields.date(u'Data sol·licitud',
                                      help=u'Data en la qual es va sol·licitar '
                                           u'excempció de LOPD'),
        'lopd_comment': fields.text(u'Comentaris'),
    }

    _defaults = {
        'lopd_active': lambda *a: False,
    }

ResPartner()