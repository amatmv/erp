from base_index.base_index import BaseIndex


class GiscedataCupsPs(BaseIndex):
    """
    Class to index CUPS
    """
    _name = 'giscedata.cups.ps'
    _inherit = 'giscedata.cups.ps'

    _index_title = '{obj.name} {obj.titular:.20}'
    _index_summary = '{obj.direccio}'

    _index_fields = {
        'zona': True,
        'ordre': True,
        'linia': True,
        'id_escomesa.name': True
    }

    def get_schema(self, cursor, uid, item, context=None):
        """
        Gets the index schema

        :param cursor: Database cursor
        :param uid: User id
        :param item: item to index
        :param context: OpenERP context
        :return: Dict with the schema
        """

        if item:
            lat, lon = self.get_lat_lon(cursor, uid, item, context=context)
            content = self.get_content(cursor, uid, item, context=context)

            if item.titular:
                title = self._index_title.format(obj=item)
                if len(item.titular) > 20:
                    title = '{} ...'.format(title)
            else:
                title = '{obj.name}'.format(obj=item)

            return {
                'id': item.id,
                'path': self.get_path(cursor, uid, item.id, context=context),
                'title': title,
                'content': ' '.join(set(content)),
                'gistype': self._name,
                'summary': self._index_summary.format(obj=item),
                'lat': lat,
                'lon': lon
            }
        else:
            return {}


GiscedataCupsPs()


class GiscedataCupsEscomesa(BaseIndex):
    """
    Class to index Escomesa
    """
    _name = 'giscedata.cups.escomesa'
    _inherit = 'giscedata.cups.escomesa'

    _index_title = '{obj.name}'
    _index_summary = '{obj.blockname.name} {obj.name} {obj.ncups} CUPS'

    def index_blockname_name(self, value):
        """
        Filters the blocknames

        :param value: Value to filter
        :return: Returns the value filtret
        """
        if value in ('CONTA-S', 'CONTA-SS', 'CONTA-Q'):
            value = ''
        return value

    _index_fields = {
        'name': True,
        'id_municipi.name': True,
        'blockname.name': index_blockname_name
    }


GiscedataCupsEscomesa()
