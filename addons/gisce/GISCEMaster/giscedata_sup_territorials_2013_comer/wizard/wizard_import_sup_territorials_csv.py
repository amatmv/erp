# -*- coding: utf-8 -*-

from osv import osv, fields
import base64
import csv
from enerdata.cups import cups
import datetime
from tools.translate import _
import logging

try:
    import cStringIO as StringIO
except ImportError:
    import StringIO


class WizardImportSupTerritorialsCsv(osv.osv_memory):
    """ Wizard per importar CSV dels Suplements Territorials"""
    _name = 'wizard.import.sup.territorials.csv'

    _columns = {
        'state': fields.selection([
            ('init', 'Init'),
            ('end', 'Final')
        ], 'Estat'),
        'info': fields.text('Informació', readonly=True),
        'csv_file': fields.binary(
            'Fitxer', states={'load': [('requiered', True)]}
        ),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'csv_file': False
    }

    def importar_csv(self, cursor, uid, ids, context=None):
        def conv(num):
            return float(num.replace(',', '.')) if num else 0

        def conv_date(date):
            try:
                assert(datetime.datetime.strptime(date, '%Y/%m/%d'))
            except:
                for format_date in ('%d/%m/%Y', '%Y-%m-%d', '%d-%m-%Y'):
                    try:
                        return datetime.datetime.strptime(
                            date, format_date
                        ).strftime('%Y/%m/%d')
                    except:
                        pass
                info = _("La data: {} té un format desconegut.".format(date))
                raise osv.except_osv(_(u'Error!'), info)
            return date

        def is_header(reader):
            try:
                assert cups.check_cups_number(reader) is True
            except:
                return True
            return False

        wiz = self.browse(cursor, uid, ids[0], context)
        wiz_file = wiz.csv_file
        if csv:
            sup_territorial = self.pool.get(
                'giscedata.suplements.territorials.2013.comer'
            )
            txt = base64.decodestring(wiz_file)
            csv_file = StringIO.StringIO(txt)
            reader = csv.reader(csv_file, delimiter=';')
            linies_correctes = linies_totals = 0
            for row in reader:
                if not is_header(row[0]):
                    values = {
                        'cups': row[0],
                        'fecha_desde': conv_date(row[1]),
                        'fecha_hasta': conv_date(row[2]),
                        'media_pond_potencia_a_facturar_p1': conv(row[3]),
                        'media_pond_potencia_a_facturar_p2': conv(row[4]),
                        'media_pond_potencia_a_facturar_p3': conv(row[5]),
                        'media_pond_potencia_a_facturar_p4': conv(row[6]),
                        'media_pond_potencia_a_facturar_p5': conv(row[7]),
                        'media_pond_potencia_a_facturar_p6': conv(row[8]),
                        'importe_total_suplemento_termino_potencia': conv(row[9]),
                        'valor_energia_activa_p1': conv(row[10]),
                        'valor_energia_activa_p2': conv(row[11]),
                        'valor_energia_activa_p3': conv(row[12]),
                        'valor_energia_activa_p4': conv(row[13]),
                        'valor_energia_activa_p5': conv(row[14]),
                        'valor_energia_activa_p6': conv(row[15]),
                        'importe_total_suplemento_termino_energia': conv(row[16]),
                        'importe_total_a_regularizar': conv(row[17]),
                    }
                    linies_totals += 1
                    try:
                        sup_territorial.create(cursor, uid, values)
                        linies_correctes += 1
                    except Exception as e:
                        logging.error(e)
            info = u'S\'han processat %s línies correctament de %s' % \
                   (linies_correctes, linies_totals)
            wiz.write({'state': 'end', 'info': info})
        else:
            raise osv.except_osv(_(u'Has d\'indicar un fitxer CSV a processar'))
        return None

WizardImportSupTerritorialsCsv()
