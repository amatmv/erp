# -*- coding: utf-8 -*-
import json
from collections import OrderedDict
from datetime import datetime

from osv import osv, fields
import pooler


class GiscedataSuplementsTerritorials2013Comer(osv.osv):
    _name = 'giscedata.suplements.territorials.2013.comer'

    _columns = {
        'cups': fields.char(u'CUPS', size=25),
        'fecha_desde': fields.date(u'Fecha Desde'),
        'fecha_hasta': fields.date(u'Fecha Hasta'),
        'media_pond_potencia_a_facturar_p1': fields.integer(
            'MediaPondPotenciaAFacturar_P1 (W)'
        ),
        'media_pond_potencia_a_facturar_p2': fields.integer(
            'MediaPondPotenciaAFacturar_P2 (W)'
        ),
        'media_pond_potencia_a_facturar_p3': fields.integer(
            'MediaPondPotenciaAFacturar_P3 (W)'
        ),
        'media_pond_potencia_a_facturar_p4': fields.integer(
            'MediaPondPotenciaAFacturar_P4 (W)'
        ),
        'media_pond_potencia_a_facturar_p5': fields.integer(
            'MediaPondPotenciaAFacturar_P5 (W)'
        ),
        'media_pond_potencia_a_facturar_p6': fields.integer(
            'MediaPondPotenciaAFacturar_P6 (W)'
        ),
        'importe_total_suplemento_termino_potencia': fields.float(
            'ImporteTotalSuplementoTerminoPotencia(€)', digits=(12, 2)
        ),
        'valor_energia_activa_p1': fields.float(
            'ValorEnergiaActiva_P1(kWh)', digits=(12, 2)
        ),
        'valor_energia_activa_p2': fields.float(
            'ValorEnergiaActiva_P2(kWh)', digits=(12, 2)
        ),
        'valor_energia_activa_p3': fields.float(
            'ValorEnergiaActiva_P3(kWh)', digits=(12, 2)
        ),
        'valor_energia_activa_p4': fields.float(
            'ValorEnergiaActiva_P4(kWh)', digits=(12, 2)
        ),
        'valor_energia_activa_p5': fields.float(
            'ValorEnergiaActiva_P5(kWh)', digits=(12, 2)
        ),
        'valor_energia_activa_p6': fields.float(
            'ValorEnergiaActiva_P6(kWh)', digits=(12, 2)
        ),
        'importe_total_suplemento_termino_energia': fields.float(
            'ImporteTotalSuplementoTerminoEnergia(€)', digits=(12, 2)
        ),
        'importe_total_a_regularizar': fields.float(
            'ImporteTotalARegularizar (€)', digits=(12, 2)
        )
    }

    @staticmethod
    def get_info_html(cursor, uid, cups):
        pool = pooler.get_pool(cursor.dbname)
        sups_obj = pool.get('giscedata.suplements.territorials.2013.comer')

        schema = sups_obj.get_info_data(cursor, uid, cups)
        if schema:
            values = schema['columns']
            periodes = schema['periods']
            fields = values.keys()
            html = '''
            <style type="text/css">
                table.supl, table.supl th, table.supl td {
                    border-collapse: collapse;
                    border-spacing: unset;
                    font-family:sans-serif;
                    font-size: 7px;
                }
                table.supl td, table.supl th {
                    border: 1px solid black;
                    padding: 2px;
                }
                .c {
                    text-align:center;
                }
            </style>
            <table class="supl">
            <tr>
                <th></th>
                <th>Importe (€)</th>
                <th></th>
                <th>V1</th>
                <th>V2</th>
                <th>V3</th>
                <th>V4</th>
                <th>V5</th>
                <th>V6</th>
            </tr>
            '''

            for i in range(periodes):
                html += sups_obj.get_report_html_for_period(
                    {j: values[j][i] for j in fields}
                )

            html += '''
                <tr>
                    <th>Importe Total</th>
                    <td style="text-align:right;">{}</td>
                    <td colspan="7"</td>
                </tr>
                </table>
            '''.format(schema['total'])

            return html
        return ''

    @staticmethod
    def get_info_json(cursor, uid, cups):
        pool = pooler.get_pool(cursor.dbname)
        sups_obj = pool.get('giscedata.suplements.territorials.2013.comer')
        sups_ids = sups_obj.search(
            cursor, uid, {'cups': cups}, order='fecha_desde asc'
        )

        schema = sups_obj.get_info_data(cursor, uid, cups)

        return json.dumps(schema)

    @staticmethod
    def get_info_data(cursor, uid, cups):
        pool = pooler.get_pool(cursor.dbname)
        sups_obj = pool.get('giscedata.suplements.territorials.2013.comer')
        search_params = [
            ('cups', '=', cups)
        ]
        sups_ids = sups_obj.search(
            cursor, uid, search_params, order='fecha_desde asc'
        )

        if sups_ids:
            fields = OrderedDict([
                ('fecha_desde', []),
                ('fecha_hasta', []),
                ('media_pond_potencia_a_facturar_p1', []),
                ('media_pond_potencia_a_facturar_p2', []),
                ('media_pond_potencia_a_facturar_p3', []),
                ('media_pond_potencia_a_facturar_p4', []),
                ('media_pond_potencia_a_facturar_p5', []),
                ('media_pond_potencia_a_facturar_p6', []),
                ('importe_total_suplemento_termino_potencia', []),
                ('valor_energia_activa_p1', []),
                ('valor_energia_activa_p2', []),
                ('valor_energia_activa_p3', []),
                ('valor_energia_activa_p4', []),
                ('valor_energia_activa_p5', []),
                ('valor_energia_activa_p6', []),
                ('importe_total_suplemento_termino_energia', []),
                ('importe_total_a_regularizar', [])
            ])

            schema = {
                'cups': cups,
                'columns': fields,
                'total': 0.0,
                'periods': len(sups_ids)
            }

            for sups_id in sups_ids:
                sups_values = sups_obj.read(cursor, uid, sups_id, fields.keys())
                for key in fields.keys():
                    value = sups_values[key]
                    schema['columns'][key].append(value)
                    if key == 'importe_total_a_regularizar':
                        schema['total'] += value
            return schema
        return None

    @staticmethod
    def get_report_html_for_period(period, show_zeroes=False):

        values_1_6_potencia = ''
        values_1_6_energia = ''
        for fieldname in sorted(period.keys()):
            value = str(period[fieldname])
            if not show_zeroes and period[fieldname] == 0:
                value = '-'
            if fieldname.startswith('media_pond'):
                values_1_6_potencia += '<td class="c">{}</td>'.format(value)
            elif fieldname.startswith('valor_energia_activa'):
                values_1_6_energia += '<td class="c">{}</td>'.format(value)

        html = '''
                <tr>
                    <th rowspan="2">Periodo del {}<br> al {}</th>
                    <td style="text-align:right;" rowspan="2">{}</td>
                    <th>Energía (kWh)</th>
                    {}
                </tr>
                <tr>
                    <th>Potencia (W)</th>
                    {}
                </tr>
            '''.format(
            datetime.strptime(
                period['fecha_desde'], '%Y-%m-%d'
            ).strftime('%d/%m/%Y'),
            datetime.strptime(
                period['fecha_hasta'], '%Y-%m-%d'
            ).strftime('%d/%m/%Y'),
            period['importe_total_a_regularizar'],
            values_1_6_energia,
            values_1_6_potencia
        )

        return html


GiscedataSuplementsTerritorials2013Comer()
