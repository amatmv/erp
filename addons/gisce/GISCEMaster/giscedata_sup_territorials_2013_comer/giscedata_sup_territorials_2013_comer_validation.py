# -*- coding: utf-8 -*-

from osv import osv, fields


class GiscedataSupTeritorials2013ComerValidation(osv.osv):

    _name = 'giscedata.facturacio.validation.validator'
    _inherit = 'giscedata.facturacio.validation.validator'

    def check_duplicates_sups(self, cursor, uid, clot, date_from, date_to,
                              parameters):
        pool = self.pool
        extra_obj = pool.get('giscedata.facturacio.extra')

        # Getting the extra lines from in that contract within the dates
        extra_line_ids = extra_obj.get_extra_lines_from_contract_and_date(
            cursor, uid, clot.polissa_id.id, date_to
        )
        search_params = [
            ('id', 'in', extra_line_ids),
            ('product_id.default_code', '=', 'SETU35')
        ]
        sup_terr_extra_line_ids = extra_obj.search(cursor, uid, search_params)

        if (len(sup_terr_extra_line_ids) > 1):
            return {
                'date_from': date_from,
                'date_to': date_to
            }

        return None

    def check_cups_exist(self, cursor, uid, clot, date_from,
                         date_to, parameters):
        pool = self.pool
        extra_obj = pool.get('giscedata.facturacio.extra')
        sup_territorials_2013_comer_obj = pool.get(
            'giscedata.suplements.territorials.2013.comer'
        )
        # First, let's search for some extra line from our contract within the
        # dates
        extra_line_ids = extra_obj.get_extra_lines_from_contract_and_date(
            cursor, uid, clot.polissa_id.id, date_to
        )

        # Then, we must see if that extra lines are SET35 products
        search_params = [
            ('id', 'in', extra_line_ids),
            ('product_id.default_code', '=', 'SETU35')
        ]
        sup_terr_extra_line_id = extra_obj.search(cursor, uid, search_params)

        if sup_terr_extra_line_id:
            # If there's an SET35 product, we have to finally check if it
            # exists in the 2013_comer table
            contract_cups = clot.polissa_id.cups.name[:20]
            sups_id = sup_territorials_2013_comer_obj.search(
                cursor, uid, [('cups', 'like', contract_cups)]
            )
            if not sups_id:
                return {'cups': contract_cups}

        return None


GiscedataSupTeritorials2013ComerValidation()
