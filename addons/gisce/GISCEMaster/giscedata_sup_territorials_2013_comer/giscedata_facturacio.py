# -*- coding: utf-8 -*-
from osv import osv
from datetime import datetime
from dateutil.relativedelta import relativedelta

VALID_CCAAS = ['08', '09', '10', '17']


class GiscedataFacturacioSwitchingHelper(osv.osv):
    _name = 'giscedata.facturacio.switching.helper'
    _inherit = 'giscedata.facturacio.switching.helper'

    def calc_extra_name(self, cursor, uid, polissa_id):

        polissa_obj = self.pool.get('giscedata.polissa')
        cups_obj = self.pool.get('giscedata.cups.ps')
        ccaa_obj = self.pool.get('res.comunitat_autonoma')

        cups_id = polissa_obj.read(
            cursor, uid, polissa_id, ['cups']
        )['cups'][0]

        municipi_id = cups_obj.read(
            cursor, uid, cups_id, ['id_municipi']
        )['id_municipi'][0]

        ccaa_id = ccaa_obj.get_ccaa_from_municipi(cursor, uid, municipi_id)[0]
        ccaa_data = ccaa_obj.read(
            cursor, uid, ccaa_id, ['name', 'codi']
        )

        comunidad_name = ''
        if ccaa_data['codi'] in VALID_CCAAS:
            comunidad_name = ccaa_data['name'] + ' '
        name = 'Suplemento territorial por tributos autonómicos ' \
               'de la Comunidad Autónoma {}del año 2013'.format(comunidad_name)

        return name

    def get_values_in_ext(self, cursor, uid, fact_id, fact_name, ori_lines,
                          context=None):
        values = super(
            GiscedataFacturacioSwitchingHelper, self
        ).get_values_in_ext(cursor, uid, fact_id, fact_name, ori_lines, context)

        imd_obj = self.pool.get('ir.model.data')
        cfg_obj = self.pool.get('res.config')
        obj = self.pool.get('giscedata.facturacio.switching.helper')

        con48_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio_comer', 'concepte_48'
        )[1]

        delay_months = int(cfg_obj.get(cursor, uid, 'delay_concepte_48', 0))

        for vals in values:
            if delay_months and vals['product_id'] == con48_id:
                for x in ('date_from', 'date_to'):
                    vals[x] = (
                        datetime.strptime(vals[x], '%Y-%m-%d')
                        + relativedelta(months=delay_months)
                    ).strftime('%Y-%m-%d')
            if vals['product_id'] == con48_id:
                new_name = self.calc_extra_name(cursor, uid, vals['polissa_id'])
                vals.update({
                    'name': new_name
                })
        return values

    def get_line_others_vals(
            self, cursor, uid, concepte, factura_vals, tipus, context=None):
        vals = super(
            GiscedataFacturacioSwitchingHelper, self).get_line_others_vals(
            cursor, uid, concepte, factura_vals, tipus, context
        )

        imd_obj = self.pool.get('ir.model.data')
        con48_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio_comer', 'concepte_48'
        )[1]

        # Especification from CNMC
        if vals['product_id'] == con48_id:
            vals['force_price'] = concepte.importe
            vals['quantity'] = 1

        return vals

GiscedataFacturacioSwitchingHelper()
