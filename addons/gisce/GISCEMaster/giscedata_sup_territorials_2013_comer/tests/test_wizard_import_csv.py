# -*- coding: utf-8 -*-
from addons import get_module_resource
from destral import testing
from destral.transaction import Transaction
from osv.orm import except_orm
import pandas as pd
from datetime import datetime
import time
import base64
try:
    import cStringIO as StringIO
except ImportError:
    import StringIO


class TestWizardImportCsvSups2013(testing.OOTestCase):
    def setUp(self):
        self.txn = Transaction().start(self.database)

    def tearDown(self):
        self.txn.stop()

    def test_wizard_import_csv_correctly(self):
        cursor = self.txn.cursor
        uid = self.txn.user
        sup_territorial = self.openerp.pool.get(
            'giscedata.suplements.territorials.2013.comer'
        )
        wiz_obj = self.openerp.pool.get('wizard.import.sup.territorials.csv')
        csv_filename = 'giscedata_sup_territorials_2013_comer_demo.csv'
        resource = get_module_resource(
            'giscedata_sup_territorials_2013_comer', 'tests', 'fixtures',
            csv_filename
        )
        with open(resource, 'r') as demo_file:
            content = demo_file.read()

            ctx = {}
            values = {
                'csv_file': base64.b64encode(content)
            }
            wiz_id = wiz_obj.create(cursor, uid, values, context=ctx)
            sup_id = sup_territorial.create(cursor, uid, {})
            wiz = wiz_obj.browse(cursor, uid, wiz_id, context=ctx)
            wiz.importar_csv()

            # Checking that these 4 CUPS exist in the Sups table
            demo_cups = [
                'ES6645158467763303WG0F',
                'ES1304206770587920WH0F',
                'ES8085217581887646XP0F',
                'ES5882568587372936ZY0F'
            ]
            for cups in demo_cups:
                search_params = [('cups', '=', cups)]
                id = sup_territorial.search(cursor, uid, search_params)
                msg = (("El cups %s no existeix a la taula.") % cups)
                self.assertTrue(id, msg)

        return True
