# -*- coding: utf-8 -*-
import unittest
import base64

from destral import testing
from destral.transaction import Transaction
from expects import *
from osv.osv import except_osv
from addons import get_module_resource
from gestionatr.input.messages import F1


class TestImportF1(testing.OOTestCase):
    def setUp(self):
        module_obj = self.openerp.pool.get('ir.module.module')

        newest_name = None

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            newest_id = module_obj.search(
                cursor, uid, [
                    ('name', 'like', 'giscedata_tarifas_peajes_20160101'),
                    ('state', '!=', 'installed')
                ], limit=1, order='name DESC'
            )
            if newest_id:
                newest_name = module_obj.read(
                    cursor, uid, newest_id[0], ['name']
                )['name']

        if newest_name:
            self.openerp.install_module(newest_name)
        return super(TestImportF1, self).setUp()

    def activar_polissa_CUPS(self, txn):
        cursor = txn.cursor
        uid = txn.user
        imd_obj = self.openerp.pool.get('ir.model.data')

        polissa_obj = self.openerp.pool.get('giscedata.polissa')

        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]

        polissa_obj.send_signal(cursor, uid, [polissa_id], [
            'validar', 'contracte'
        ])

    def crear_modcon(self, txn, potencia, ini, fi, tariff_id=None):
        cursor = txn.cursor
        uid = txn.user
        pool = self.openerp.pool
        polissa_obj = pool.get('giscedata.polissa')
        imd_obj = pool.get('ir.model.data')
        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]
        pol = polissa_obj.browse(cursor, uid, polissa_id)
        pol.send_signal(['modcontractual'])
        polissa_obj.write(cursor, uid, polissa_id, {'potencia': potencia})

        if tariff_id:
            polissa_obj.write(cursor, uid, polissa_id, {'tarifa': tariff_id})

        wz_crear_mc_obj = pool.get('giscedata.polissa.crear.contracte')

        ctx = {'active_id': polissa_id}
        params = {'duracio': 'nou'}

        wz_id_mod = wz_crear_mc_obj.create(cursor, uid, params, ctx)
        wiz_mod = wz_crear_mc_obj.browse(cursor, uid, wz_id_mod, ctx)
        res = wz_crear_mc_obj.onchange_duracio(
            cursor, uid, [wz_id_mod], wiz_mod.data_inici, wiz_mod.duracio,
            ctx
        )
        wiz_mod.write({
            'data_inici': ini,
            'data_final': fi
        })
        wiz_mod.action_crear_contracte(ctx)

    def add_supported_iva_to_periods(self, txn):
        tarifa_obj = self.openerp.pool.get('giscedata.polissa.tarifa')
        periode_obj = self.openerp.pool.get('giscedata.polissa.tarifa.periodes')
        tax_obj = self.openerp.pool.get('account.tax')
        product_obj = self.openerp.pool.get('product.product')

        cursor = txn.cursor
        uid = txn.user

        iva_id = tax_obj.search(
            cursor, uid, [
                ('name', '=', '21% IVA Soportado (operaciones corrientes)')
            ]
        )

        product_ids = []
        for tarifa_id in tarifa_obj.search(cursor, uid, []):
            periode_ids = tarifa_obj.read(
                cursor, uid, tarifa_id, ['periodes']
            )['periodes']
            for periode_vals in periode_obj.read(cursor, uid, periode_ids):
                product_ids.append(periode_vals['product_id'][0])
                if periode_vals['product_reactiva_id']:
                    product_ids.append(periode_vals['product_reactiva_id'][0])
                if periode_vals['product_exces_pot_id']:
                    product_ids.append(periode_vals['product_exces_pot_id'][0])

        product_obj.write(
            cursor, uid, product_ids, {
                'supplier_taxes_id': [(6, 0, iva_id)]
            }
        )

    def add_supported_iva_to_concepts(self, txn, iva='21'):
        tax_obj = self.openerp.pool.get('account.tax')
        product_obj = self.openerp.pool.get('product.product')

        cursor = txn.cursor
        uid = txn.user

        name = '{}% IVA Soportado (operaciones corrientes)'.format(iva)
        iva_id = tax_obj.search(
            cursor, uid, [
                ('name', '=', name)
            ]
        )

        no_iva_concepts = ['CON06', 'CON40', 'CON41', 'CON42']
        product_ids = product_obj.search(
            cursor, uid, [
                '|',
                ('default_code', 'like', 'CON'),
                ('default_code', 'like', 'ALQ'),
                ('default_code', 'not in', no_iva_concepts)
            ]
        )
        product_obj.write(
            cursor, uid, product_ids, {
                'supplier_taxes_id': [(6, 0, iva_id)]
            }
        )

    def add_sell_taxes_to_suplemento_territorial(self, txn):
        tax_obj = self.openerp.pool.get('account.tax')
        product_obj = self.openerp.pool.get('product.product')

        cursor = txn.cursor
        uid = txn.user

        tax_ids = tax_obj.search(
            cursor, uid, [
                '|',
                ('name', '=', 'IVA 21%'),
                ('name', '=', 'Impuesto especial sobre la electricidad')
            ]
        )

        product_ids = product_obj.search(
            cursor, uid, [
                ('default_code', '=', 'SETU35')
            ]
        )
        product_obj.write(
            cursor, uid, product_ids, {
                'taxes_id': [(6, 0, tax_ids)]
            }
        )

    def prepare_for_phase_3(self, txn):
        polissa_obj = self.openerp.pool.get(
            'giscedata.polissa'
        )
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        pricelist_obj = self.openerp.pool.get('product.pricelist')
        cups_obj = self.openerp.pool.get('giscedata.cups.ps')
        partner_obj = self.openerp.pool.get('res.partner')
        address_obj = self.openerp.pool.get('res.partner.address')
        imd_obj = self.openerp.pool.get('ir.model.data')

        xml_path = get_module_resource(
            'giscedata_facturacio_switching', 'tests', 'fixtures',
            'test_correct.xml'
        )

        with open(xml_path, 'r') as f:
            self.xml_file = f.read()
        self.xml_file = self.xml_file.replace(
            '<CodigoREEEmpresaEmisora>1234</CodigoREEEmpresaEmisora>',
            '<CodigoREEEmpresaEmisora>0031</CodigoREEEmpresaEmisora>'
        )

        conceptes_xml_path = get_module_resource(
            'giscedata_facturacio_switching', 'tests', 'fixtures',
            'F1_conceptes.xml'
        )

        with open(conceptes_xml_path, 'r') as f:
            self.conceptes_xml_file = f.read()

        cursor = txn.cursor
        uid = txn.user

        cups_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_cups', 'cups_01'
        )[1]
        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]
        import_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
        )[1]
        distri_id = partner_obj.search(
            cursor, uid, [('ref', '=', '0031')]
        )[0]
        pricelist_id = pricelist_obj.search(
            cursor, uid, [('name', '=', 'TARIFAS ELECTRICIDAD')]
        )[0]
        self.line_id = line_obj.create_from_xml(
            cursor, uid, import_id, 'Import Name', self.xml_file
        )
        line_obj.write(cursor, uid, self.line_id, {'cups_id': cups_id})
        polissa_obj.write(
            cursor, uid, polissa_id, {
                'cups': cups_id,
                'state': 'activa',
                'distribuidora': distri_id,
                'facturacio_potencia': 'icp'
            }
        )
        cups_obj.write(cursor, uid, cups_id, {'distribuidora_id': distri_id})
        address_obj.create(cursor, uid, {'partner_id': distri_id})
        partner_obj.write(
            cursor, uid, distri_id, {
                'property_product_pricelist_purchase': pricelist_id
            }
        )

        self.activar_polissa_CUPS(txn)

        self.add_supported_iva_to_periods(txn)
        self.add_supported_iva_to_concepts(txn)
        self.add_sell_taxes_to_suplemento_territorial(txn)

    def test_we_import_extra_lines_correctly(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        error_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.error'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            xml_path = get_module_resource(
                'giscedata_sup_territorials_2013_comer', 'tests', 'fixtures',
                'test_suplemento_territorial.xml'
            )

            with open(xml_path, 'r') as f:
                xml_file = f.read()
            xml_file = xml_file.replace(
                '<CodigoREEEmpresaEmisora>1234</CodigoREEEmpresaEmisora>',
                '<CodigoREEEmpresaEmisora>0031</CodigoREEEmpresaEmisora>'
            )

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', xml_file
            )

            line_obj.process_line_sync(cursor, uid, line_id)
            line_vals = line_obj.browse(cursor, uid, line_id)
            expect(line_vals.state).to(equal('valid'))
            expect(line_vals.import_phase).to(equal(40))

            expect(len(line_vals.liniaextra_id)).to(equal(1))

            extra_line = line_vals.liniaextra_id[0]

            expect(len(extra_line.tax_ids)).to(equal(2))
            expect(
                {tax.name for tax in extra_line.tax_ids}
            ).to(
                equal(
                    {'IVA 21%', 'Impuesto especial sobre la electricidad'}
                )
            )
            read_params = [
                'product_id',
                'name',
                'price_unit_multi',
                'data_desde',
                'data_fins',
                'quantity',
                'uos_id',
                'invoice_line_tax_id',
            ]
            for p in read_params:
                print getattr(extra_line, p)
            expect(extra_line.quantity).to(equal(1.0))
            expect(extra_line.price_unit).to(equal(1.66))
            expect(extra_line.price_subtotal).to(equal(1.66))
            self.assertTrue(
                extra_line.name.startswith(
                    u'Suplemento territorial por tributos autonómicos de'
                    u' la Comunidad Autónoma')
            )
