# -*- coding: utf-8 -*-

from addons import get_module_resource
from destral import testing
from destral.transaction import Transaction


class TestValidationLot(testing.OOTestCase):
    def setUp(self):
        self.txn = Transaction().start(self.database)

    def tearDown(self):
        self.txn.stop()

    def create_invoice_extra_line_48(self, txn, dades_linia):
        cursor = txn.cursor
        uid = txn.user
        pool = self.openerp.pool
        extraline_obj = pool.get('giscedata.facturacio.extra')

        tax_obj = self.openerp.pool.get('account.tax')

        tax_ids = tax_obj.search(
            cursor, uid, [
                ('name', '=', 'IVA 21%'),
            ]
        )
        polissa_id = dades_linia['polissa_id']

        product_obj = pool.get('product.product')
        product_etu35 = product_obj.search(
            cursor, uid, [('default_code', '=', 'SETU35')]
        )[0]
        product = product_obj.browse(cursor, uid, product_etu35)
        if product.product_tmpl_id.property_account_income:
            account = product.product_tmpl_id.property_account_income.id
        else:
            account = product.categ_id.property_account_income_categ.id
        diari_obj = pool.get('account.journal')
        diari_id = diari_obj.search(cursor, uid, [('code', '=', 'ENERGIA')])[0]
        termes = dades_linia['terms']
        extraline_vals = {
            'account_id': account,
            'journal_ids': [[6, 0, [diari_id]]],
            'product_id': product_etu35,
            'price_unit': dades_linia['total'],
            'name': product.description,
            'date_line_from': '2013-01-01',
            'date_line_to': '2013-12-31',
            'date_from': dades_linia['date_from'],
            'date_to': dades_linia['date_to'],
            'polissa_id': polissa_id,
            'term': termes,
            'taxs_id': [(6, 0, tax_ids)],
            'notes': 'EMPTY',
        }

        return extraline_obj.create(cursor, uid, extraline_vals)

    def test_sups_2013_ETU1_validation(self):
        cursor = self.txn.cursor
        uid = self.txn.user

        imd_obj = self.openerp.pool.get('ir.model.data')
        clot_obj = self.openerp.pool.get('giscedata.facturacio.contracte_lot')
        extra_line_obj = self.openerp.pool.get('giscedata.facturacio.extra')
        sup_territorials_2013_comer_obj = self.openerp.pool.get(
            'giscedata.suplements.territorials.2013.comer'
        )

        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.validation.validator'
        )
        clot_id = imd_obj.get_object_reference(
            cursor, uid,
            'giscedata_facturacio',
            'cont_lot_0001'
        )[1]

        product_id = imd_obj.get_object_reference(
            cursor, uid,
            'giscedata_facturacio_comer',
            'concepte_48'
        )[1]

        clot = clot_obj.browse(cursor, uid, clot_id)

        extra_line_vals = {
            'polissa_id': clot.polissa_id.id,
            'date_from': '2017-09-06',
            'date_to': '2017-09-06',
            'price_unit': 1,
            'term': 1,
            'product_id': product_id
        }

        extra_line_obj.create(cursor, uid, extra_line_vals)

        contract_cups = clot.polissa_id.cups.name

        self.assertTrue(
            validator_obj.check_cups_exist(
                cursor, uid, clot, '2017-09-06', '2017-09-06', {}
            )
        )

        sup_id = sup_territorials_2013_comer_obj.create(
            cursor, uid, {'cups': contract_cups}
        )

        self.assertEquals(
            validator_obj.check_cups_exist(
                cursor, uid, clot, '2017-09-06', '2017-09-06', {}
            ), None
        )

        return True

    def test_sups_2013_ETU2_validation(self):
        cursor = self.txn.cursor
        uid = self.txn.user
        imd_obj = self.openerp.pool.get('ir.model.data')
        clot_obj = self.openerp.pool.get('giscedata.facturacio.contracte_lot')
        extra_line_obj = self.openerp.pool.get('giscedata.facturacio.extra')
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.validation.validator'
        )
        clot_id = imd_obj.get_object_reference(
            cursor, uid,
            'giscedata_facturacio',
            'cont_lot_0001'
        )[1]
        clot = clot_obj.browse(cursor, uid, clot_id)
        dades_linia = {
            'polissa_id': clot.polissa_id.id,
            'terms': 1,
            'total': 1,
            'date_from': '2017-09-06',
            'date_to': '2017-09-06',
        }

        self.assertEquals(validator_obj.check_duplicates_sups(
            cursor, uid, clot, dades_linia['date_from'], dades_linia['date_to'], {}
        ), None)

        line_id_1 = self.create_invoice_extra_line_48(self.txn, dades_linia)

        self.assertEquals(validator_obj.check_duplicates_sups(
            cursor, uid, clot, dades_linia['date_from'], dades_linia['date_to'],
            {}
        ), None)

        line_id_2 = self.create_invoice_extra_line_48(self.txn, dades_linia)

        self.assertTrue(validator_obj.check_duplicates_sups(
            cursor, uid, clot, dades_linia['date_from'], dades_linia['date_to'],
            {}
        ))

        vals = {
            'date_from': '2017-09-05',
            'date_to': '2017-09-05',
            'price_unit': 1,
            'term': 1
        }

        extra_line_obj.write(cursor, uid, [line_id_1], vals)
        line_1 = extra_line_obj.browse(cursor, uid, line_id_1)

        self.assertEquals(validator_obj.check_duplicates_sups(
            cursor, uid, clot, line_1.date_from, line_1.date_to, {}
        ), None)

        vals = {
            'date_from': '2017-09-07',
            'date_to': '2017-09-07',
        }

        extra_line_obj.write(cursor, uid, [line_id_1], vals)

        self.assertEquals(validator_obj.check_duplicates_sups(
            cursor, uid, clot, line_1.date_from, line_1.date_to, {}
        ), None)

        return True
