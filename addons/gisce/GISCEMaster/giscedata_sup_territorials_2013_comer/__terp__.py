# -*- coding: utf-8 -*-
{
    "name": "",
    "description": """Adaptacions per a la refacturació de suplements 
  territorials per a les comer, 2013.
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "giscedata_facturacio_comer",
        "giscedata_facturacio_switching"
    ],
    "init_xml": [],
    "demo_xml":[
        "giscedata_sup_territorials_2013_comer_demo.xml"
    ],
    "update_xml":[
        "giscedata_sup_territorials_2013_comer_view.xml",
        "wizard/wizard_import_sup_territorials_csv.xml",
        "giscedata_sup_territorials_2013_comer_validation_data.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
