# -*- coding: utf-8 -*-
{
    "name": "GISCE Expedients multicompany",
    "description": """Multi-company support for Expedients""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Multi Company",
    "depends":[
        "giscedata_expedients"
    ],
    "init_xml":[
        "record_rules.xml"
    ],
    "demo_xml": [],
    "update_xml":[
        "giscedata_expedients_multicompany_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
