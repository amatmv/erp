# -*- coding: iso-8859-1 -*-
from osv import osv, fields

class giscedata_expedients_expedient(osv.osv):
	_name = "giscedata.expedients.expedient"
	_inherit = "giscedata.expedients.expedient"
	_columns = {
		'company_id': fields.many2one('res.company', 'Company', required=True),
	}
	_defaults = {
		#'company_id': lambda self,cr,uid,c: self.pool.get('res.users').browse(cr, uid, uid, c).company_id.id,
	}
giscedata_expedients_expedient()


class giscedata_expedients_projecte(osv.osv):
	_name = "giscedata.expedients.projecte"
	_inherit = "giscedata.expedients.projecte"
	_columns = {
		'company_id': fields.many2one('res.company', 'Company', required=True),
	}
	_defaults = {
		#'company_id': lambda self,cr,uid,c: self.pool.get('res.users').browse(cr, uid, uid, c).company_id.id,
	}
giscedata_expedients_projecte()
