# -*- coding: utf-8 -*-
{
    "name": "GISCE Indústria",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base_extended",
        "giscedata_administracio_publica",
        "giscedata_administracio_publica_industria",
        "giscedata_cts",
        "giscedata_at",
        "giscedata_bt"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_industria_view.xml",
        "giscedata_industria_wizard.xml",
        "giscedata_industria_report.xml",
        "ir.model.access.csv",
        "security/giscedata_industria_security.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
