# -*- coding: utf-8 -*-

from osv import osv, fields

#
# Reports
#


class giscedata_industria_estadistica_year(osv.osv):

    _name = 'giscedata.industria.estadistica.year'

    _columns = {
      'name': fields.char('Any', size=7),
      'linies': fields.one2many('giscedata.industria.estadistica',
                                'year', 'Línies'),
      'provincia': fields.many2one('res.country.state', 'Provincia'),
      'create_date': fields.datetime('Data Generació'),
      'revisio': fields.integer('Revisió'),
    }

    _defaults = {
        'revisio': lambda *a: 0,
    }

    _order = 'create_date desc,provincia'

giscedata_industria_estadistica_year()


class giscedata_industria_estadistica(osv.osv):
    _name = 'giscedata.industria.estadistica'
    _description = 'Report Estadística per a Indústria'

    _columns = {
      'name': fields.selection(
        [('1000', 'Menys de 1.000 V.'),
         ('4500', 'de 1.000 a 4.500 V'),
         ('8000', 'Més de 4.500 a 8.000 V'),
         ('12500', 'Més de 8.000 a 12.500 V'),
         ('17500', 'Més de 12.500 a 17.500 V'),
         ('25000', 'Més de 17.500 a 25.000 V'),
         ('37500', 'Més de 25.000 a 37.500 V'),
         ('55500', 'Més de 37.500 a 55.500 V'),
         ('99000', 'Més de 55.500 a 99.000 V'),
         ('176000', 'Més de 99.00 a 176.000 V'),
         ('300000', 'Més de 176.000 a 300.000 V'),
         ('300001', 'Més de 300.000 V')],
        'Tensions', size=256),
      'trafo_num': fields.integer('Número de trafos'),
      'trafo_pot': fields.float('Capacitat total kVA'),
      'l_1c': fields.float('Línies 1 circuit'),
      'l_2c': fields.float('Línies 2 circuits'),
      'l_m2c': fields.float('Línies 2+ circuits'),
      'l_sub': fields.float('Línies subtarràneas'),
      'year': fields.many2one('giscedata.industria.estadistica.year', 'Any',
                              ondelete='cascade'),
    }

    _defaults = {
      'trafo_num': lambda *a: 0,
      'trafo_pot': lambda *a: 0.0,
      'l_1c': lambda *a: 0.0,
      'l_2c': lambda *a: 0.0,
      'l_m2c': lambda *a: 0.0,
      'l_sub': lambda *a: 0.0,
    }

    _order = 'name asc'

    def create(self, cr, uid, vals, context={}):
        keys = ['l_1c', 'l_2c', 'l_m2c', 'l_sub']
        ids = self.search(cr, uid, [('name', '=', vals['name']),
                                    ('year', '=', vals['year'])])
        if len(ids):
            for existent in self.read(cr, uid, ids, keys):
                for key in keys:
                    vals.update({key: vals.get(key, 0) + existent[key]})
            self.write(cr, uid, ids[0], vals)
            return ids[0]
        else:
            return super(osv.osv, self).create(cr, uid, vals, context)

giscedata_industria_estadistica()
