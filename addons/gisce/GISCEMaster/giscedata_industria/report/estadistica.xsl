<?xml version="1.0" encoding="iso-8859-15"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

  <xsl:decimal-format name="ca_ES" decimal-separator="," grouping-separator="."/>

  <xsl:key name="tensions-per-provincia" match="tensio" use="provincia" />
  

  <xsl:template match="/">
    <xsl:apply-templates select="estadistiques"/>
  </xsl:template>

  <xsl:template match="estadistiques">
    <document compression="1">
      <template pageSize="(21cm, 29.7cm)">
        <pageTemplate id="main">
          <frame id="first" x1="1cm" y1="1cm" width="19cm" height="247mm"/>
          <pageGraphics>
            <setFont name="Helvetica-Bold" size="7"/>
            <drawString x="10mm" y="10mm" t="1"><xsl:value-of select="estadistica/provincia" />: <xsl:value-of select="estadistica/any" /> versi� <xsl:value-of select="estadistica/revisio" /> de <xsl:value-of select="estadistica/create_date" /></drawString>
            <lines>10mm 9mm 287mm 10mm</lines>
          </pageGraphics>
        </pageTemplate>
      </template>
      
      <stylesheet> 
      	<paraStyle name="text"
      		fontName="Helvetica"
      		fontSize="10" />
      		
      	<paraStyle name="titol"
      		fontName="Helvetica"
      		fontSize="14"
      		leading="16"
      		alignment="center" />
      		
      	<paraStyle name="vermell"
      		textColor="darkred"
      		fontName="Helvetica-Bold"
      		fontSize="10" />
      		
      	<paraStyle name="blau"
      		textColor="darkblue"
      		fontName="Helvetica-Bold"
      		fontSize="10"
      		leftIndent="0.5cm" />
      		
      	<blockTableStyle id="taula2">
      		<lineStyle kind="LINEAFTER" colorName="black" start="0,0" stop="-1,0" />
      		<lineStyle kind="LINEABOVE" colorName="black" start="0,0" stop="-1,0" />
      		<lineStyle kind="LINEBEFORE" colorName="black" start="0,0" stop="0,0" />
      		<blockBackground colorName="silver" start="0,0" stop="-1,0" />
      	</blockTableStyle>
      		
      	<blockTableStyle id="taula1">
      		<lineStyle kind="LINEAFTER" colorName="black" start="0,0" stop="2,0" />
      		<lineStyle kind="LINEAFTER" colorName="black" start="3,0" stop="5,0" />
      		<lineStyle kind="LINEABOVE" colorName="black" start="1,0" stop="4,0" />
      		<lineStyle kind="LINEBEFORE" colorName="black" start="0,0" stop="0,0" />
      		<blockBackground colorName="silver" start="0,0" stop="-1,0" />
      	</blockTableStyle>
      		
      		
      	<blockTableStyle id="taula">
          <blockFont name="Helvetica" size="10" />
          <lineStyle kind="LINEBEFORE" colorName="black" start="0,0" stop="0,0" />
          <lineStyle kind="GRID" colorName="black" start="1,1" stop="-1,-1"/>
          <lineStyle kind="GRID" colorName="black" start="0,1" stop="-1,-1"/>
          <lineStyle kind="GRID" colorName="black" start="3,0" stop="5,0" />
          <lineStyle kind="LINEAFTER" colorName="black" start="0,0" stop="1,0" />
          <lineStyle kind="LINEAFTER" colorName="black" start="6,0" stop="6,0" />
          <blockBackground colorName="silver" start="0,1" stop="0,-1" />
          <blockBackground colorName="silver" start="0,0" stop="-1,0" />
        </blockTableStyle>

      		
      </stylesheet>
    
      <story>
      	<xsl:apply-templates select="estadistica" mode="story"/>
      </story>
    </document>
  </xsl:template>
  
  <xsl:template match="estadistica" mode="story">
  
      	<para style="titol" t="1">ESTAD�STICA DE LA IND�STRIA D'ENERGIA EL�CTRICA (<xsl:value-of select="any" />)</para>
      	<spacer length="16" />
      	<para style="titol" t="1" leading="16">INSTAL�LACIONS PER AL TRANSPORT I DISTRIBUCI� D'ENERGIA EL�CTRICA PROPIETAT DE L'EMPRESA</para>
      	<spacer length="16" />
      	<xsl:variable name="year" select="any" />
      	<xsl:variable name="prov" select="provincia" />
      	<para style="text"><xsl:value-of select="provincia" /></para>
      	<!--<xsl:for-each select="tensio[count(. | key('tensions-per-provincia', provincia)[1]) = 1]">-->
      		
      		<blockTable colWidths="5cm,4cm,10cm" style="taula2">
      			<tr>
      				<td><para style="text" t="1" alignment="center">TENSIONS NOMINALS</para><para style="text" t="1" alignment="center">L�mits dels int�rvals en V</para></td>
      				<td><para style="text" t="1" alignment="center">Transformadors reductors</para></td>
      				<td><para style="text" t="1" alignment="center">Longitud de l�nies el�ctriques</para></td>
      			</tr>
      		</blockTable>
      		<blockTable colWidths="5cm,1.5cm,2.5cm,7.5cm,2.5cm" style="taula1">
      			<tr>
      				<td></td>
      				<td><para style="text" t="1" alignment="center">N�m.</para></td>
      				<td><para style="text" t="1" alignment="center">Capacitat total kVA</para></td>
      				<td><para style="text" t="1" alignment="center">A�ries</para></td>
      				<td><para style="text" t="1" alignment="center">Subterr�nies</para></td>
      			</tr>
      		</blockTable>
      		<blockTable style="taula" colWidths="5cm,1.5cm,2.5cm,2.5cm,2.5cm,2.5cm,2.5cm">
      			<tr>
      				<td></td>
      				<td></td>
      				<td></td>
      				<td><para style="text" t="1" alignment="center">1 cir.</para></td>
      				<td><para style="text" t="1" alignment="center">2 cir.</para></td>
      				<td><para style="text" t="1" alignment="center">M�s de 2 cir.</para></td>
      				<td></td>
      			</tr>
      			<tr>
      				<td><para style="text" t="1">Menys de 1.000 V</para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="//estadistica[any=$year and provincia=$prov]/tensio[nom=1000]/trafo_num" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=1000]/trafo_pot, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=1000]/l_1c, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=1000]/l_2c, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=1000]/l_m2c, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=1000]/l_sub, '###.###', 'ca_ES')" /></para></td>
      			</tr>
      			<tr>
      				<td><para style="text" t="1">de 1.000 a 4.500 V</para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="//estadistica[any=$year and provincia=$prov]/tensio[nom=4500]/trafo_num" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=4500]/trafo_pot, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=4500]/l_1c, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=4500]/l_2c, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=4500]/l_m2c, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=4500]/l_sub, '###.###', 'ca_ES')" /></para></td>
      			</tr>
      			<tr>
      				<td><para style="text" t="1">M�s de 4.500 a 8.000 V</para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="//estadistica[any=$year and provincia=$prov]/tensio[nom=8000]/trafo_num" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=8000]/trafo_pot, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=8000]/l_1c, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=8000]/l_2c, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=8000]/l_m2c, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=8000]/l_sub, '###.###', 'ca_ES')" /></para></td>
      			</tr>
      			<tr>
      				<td><para style="text" t="1">M�s de 8.000 a 12.500 V</para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="//estadistica[any=$year and provincia=$prov]/tensio[nom=12500]/trafo_num" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=12500]/trafo_pot, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=12500]/l_1c, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=12500]/l_2c, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=12500]/l_m2c, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=12500]/l_sub, '###.###', 'ca_ES')" /></para></td>
      			</tr>
      			<tr>
      				<td><para style="text" t="1">M�s de 12.500 a 17.500 V</para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="//estadistica[any=$year and provincia=$prov]/tensio[nom=17500]/trafo_num" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=17500]/trafo_pot, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=17500]/l_1c, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=17500]/l_2c, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=17500]/l_m2c, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=17500]/l_sub, '###.###', 'ca_ES')" /></para></td>
      			</tr>
      			<tr>
      				<td><para style="text" t="1">M�s de 17.500 a 25.000 V</para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="//estadistica[any=$year and provincia=$prov]/tensio[nom=25000]/trafo_num" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=25000]/trafo_pot, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=25000]/l_1c, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=25000]/l_2c, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=25000]/l_m2c, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=25000]/l_sub, '###.###', 'ca_ES')" /></para></td>
      			</tr>
      			<tr>
      				<td><para style="text" t="1">M�s de 25.000 a 37.500 V</para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="//estadistica[any=$year and provincia=$prov]/tensio[nom=37500]/trafo_num" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=37500]/trafo_pot, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=37500]/l_1c, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=37500]/l_2c, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=37500]/l_m2c, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=37500]/l_sub, '###.###', 'ca_ES')" /></para></td>
      			</tr>
      			<tr>
      				<td><para style="text" t="1">M�s de 37.500 a 55.500 V</para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="//estadistica[any=$year and provincia=$prov]/tensio[nom=55500]/trafo_num" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=55500]/trafo_pot, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=55500]/l_1c, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=55500]/l_2c, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=55500]/l_m2c, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=55500]/l_sub, '###.###', 'ca_ES')" /></para></td>
      			</tr>
      			<tr>
      				<td><para style="text" t="1">M�s de 55.500 a 99.000 V</para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="//estadistica[any=$year and provincia=$prov]/tensio[nom=99000]/trafo_num" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=99000]/trafo_pot, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=99000]/l_1c, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=99000]/l_2c, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=99000]/l_m2c, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=99000]/l_sub, '###.###', 'ca_ES')" /></para></td>
      			</tr>
      			<tr>
      				<td><para style="text" t="1">M�s de 99.00 a 176.000 V</para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="//estadistica[any=$year and provincia=$prov]/tensio[nom=176000]/trafo_num" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=176000]/trafo_pot, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=176000]/l_1c, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=176000]/l_2c, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=176000]/l_m2c, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=176000]/l_sub, '###.###', 'ca_ES')" /></para></td>
      			</tr>
      			<tr>
      				<td><para style="text" t="1">M�s de 176.000 a 300.000 V</para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="//estadistica[any=$year and provincia=$prov]/tensio[nom=300000]/trafo_num" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=300000]/trafo_pot, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=300000]/l_1c, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=300000]/l_2c, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=300000]/l_m2c, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=300000]/l_sub, '###.###', 'ca_ES')" /></para></td>
      			</tr>
      			<tr>
      				<td><para style="text" t="1">M�s de 300.000 V</para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="//estadistica[any=$year and provincia=$prov]/tensio[nom=300001]/trafo_num" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=300001]/trafo_pot, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=300001]/l_1c, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=300001]/l_2c, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=300001]/l_m2c, '###.###', 'ca_ES')" /></para></td>
      				<td><para style="text" alignment="right"><xsl:value-of select="format-number(//estadistica[any=$year and provincia=$prov]/tensio[nom=300001]/l_sub, '###.###', 'ca_ES')" /></para></td>
      			</tr>
      		</blockTable>
      		<blockTable colWidths="5cm,1.5cm,2.5cm,2.5cm,2.5cm,2.5cm,2.5cm">
      			<tr>
      				<td><para style="text" t="1">TOTALS</para></td>
      				<td><para style="text" alignment="right"><b><xsl:value-of select="sum(//estadistica[any=$year and provincia=$prov]/tensio/trafo_num)" /></b></para></td>
      				<td><para style="text" alignment="right"><b><xsl:value-of select="format-number(sum(//estadistica[any=$year and provincia=$prov]/tensio/trafo_pot), '###.###', 'ca_ES')" /></b></para></td>
      				<td><para style="text" alignment="right"><b><xsl:value-of select="format-number(sum(//estadistica[any=$year and provincia=$prov]/tensio/l_1c), '###.###', 'ca_ES')" /></b></para></td>
      				<td><para style="text" alignment="right"><b><xsl:value-of select="format-number(sum(//estadistica[any=$year and provincia=$prov]/tensio/l_2c), '###.###', 'ca_ES')" /></b></para></td>
      				<td><para style="text" alignment="right"><b><xsl:value-of select="format-number(sum(//estadistica[any=$year and provincia=$prov]/tensio/l_m2c), '###.###', 'ca_ES')" /></b></para></td>
      				<td><para style="text" alignment="right"><b><xsl:value-of select="format-number(sum(//estadistica[any=$year and provincia=$prov]/tensio/l_sub), '###.###', 'ca_ES')" /></b></para></td>
      			</tr>
      		</blockTable>
      	<nextFrame />
</xsl:template>
  
</xsl:stylesheet>
