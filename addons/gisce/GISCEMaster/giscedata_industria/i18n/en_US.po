# Translation of OpenERP Server.
# This file contains the translation of the following modules:
# 
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: GISCE-ERP\n"
"Report-Msgid-Bugs-To: https://github.com/gisce/erp/issues\n"
"POT-Creation-Date: 2012-11-05 17:39:20+0000\n"
"PO-Revision-Date: 2012-11-05 17:39+0000\n"
"Last-Translator: Agustí Fita <afita@gisce.net>\n"
"Language-Team: English (United States) (http://trad.gisce.net/projects/p/erp/language/en_US/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: en_US\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. module: giscedata_industria
#: model:ir.ui.menu,name:giscedata_industria.menu_giscedata_industria_estadistica
msgid "Estadístiques anuals"
msgstr ""

#. module: giscedata_industria
#: model:ir.model,name:giscedata_industria.model_giscedata_industria_estadistica_year
msgid "giscedata.industria.estadistica.year"
msgstr ""

#. module: giscedata_industria
#: model:ir.model,name:giscedata_industria.model_giscedata_industria_estadistica
msgid "Report Estadística per a Indústria"
msgstr ""

#. module: giscedata_industria
#: field:giscedata.industria.estadistica.year,revisio:0
msgid "Revisió"
msgstr ""

#. module: giscedata_industria
#: model:ir.actions.act_window,name:giscedata_industria.action_giscedata_industria_estadistica_form1
msgid "Informes Estadística Indústria"
msgstr ""

#. module: giscedata_industria
#: wizard_button:giscedata.industria.estadistica,escollir_any,calc:0
msgid "Generar"
msgstr ""

#. module: giscedata_industria
#: view:giscedata.industria.estadistica.year:0
msgid "Tensions"
msgstr ""

#. module: giscedata_industria
#: constraint:ir.model:0
msgid ""
"The Object name must start with x_ and not contain any special character !"
msgstr ""

#. module: giscedata_industria
#: view:giscedata.industria.estadistica.year:0
msgid "Subterrànies"
msgstr ""

#. module: giscedata_industria
#: constraint:ir.ui.view:0
msgid "Invalid XML for View Architecture!"
msgstr ""

#. module: giscedata_industria
#: view:giscedata.industria.estadistica.year:0
msgid "Estadistica"
msgstr ""

#. module: giscedata_industria
#: field:giscedata.industria.estadistica.year,create_date:0
msgid "Data Generació"
msgstr ""

#. module: giscedata_industria
#: model:ir.actions.report.xml,name:giscedata_industria.giscedata_industria_estadistica
msgid "Estadística"
msgstr ""

#. module: giscedata_industria
#: view:giscedata.industria.estadistica.year:0
msgid "Aèries"
msgstr ""

#. module: giscedata_industria
#: constraint:ir.actions.act_window:0
msgid "Invalid model name in the action definition."
msgstr ""

#. module: giscedata_industria
#: wizard_field:giscedata.industria.estadistica,escollir_any,print_pdf:0
msgid "Mostrar PDF al finalitzar"
msgstr ""

#. module: giscedata_industria
#: model:ir.actions.wizard,name:giscedata_industria.wizard_giscedata_industria_estadistica
msgid "Estadística Indústria"
msgstr ""

#. module: giscedata_industria
#: wizard_view:giscedata.industria.estadistica,escollir_any:0
#: model:ir.ui.menu,name:giscedata_industria.menu_wizard_giscedata_industria_estadistica
msgid "Generar Estadística Indústria"
msgstr ""

#. module: giscedata_industria
#: view:giscedata.industria.estadistica.year:0
msgid "Transformadors Reductors"
msgstr ""

#. module: giscedata_industria
#: wizard_field:giscedata.industria.estadistica,escollir_any,year:0
msgid "Any"
msgstr ""

#. module: giscedata_industria
#: view:giscedata.industria.estadistica.year:0
msgid "Longitud de Línies Elèctriques"
msgstr ""
