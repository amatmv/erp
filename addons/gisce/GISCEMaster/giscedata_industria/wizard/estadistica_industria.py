# -*- coding: utf-8 -*-
import wizard
import pooler

_escollir_any_form = """<?xml version="1.0"?>
<form string="Generar Estadística Indústria" col="2">
  <field name="year" />
  <field name="print_pdf" />
</form>"""

_escollir_any_fields = {
  'year': {'string': 'Any', 'type': 'char', 'size': 4, 'required': True},
  'print_pdf': {'string': 'Mostrar PDF al finalitzar', 'type': 'boolean'},
}


def _calc(self, cr, uid, data, context={}):
    """
    Function that calculates every entry of the estadistica industria report

    :param self: Self class
    :param cr: Database cursor
    :param uid: User id
    :param data: Data to calculate
    :param context: OpenERP context
    :return: {}
    """
    data['print_ids'] = []
    estadistica_year = pooler.get_pool(cr.dbname).get(
        'giscedata.industria.estadistica.year')
    estadistica = pooler.get_pool(cr.dbname).get(
        'giscedata.industria.estadistica')

    # Busquem l´última estadística de l'any seleccionat
    searc_params = [('name', '=', data['form']['year'])]
    estadistica_year_ids = estadistica_year.search(cr, uid, searc_params, 0, 1, 'revisio desc')

    if estadistica_year_ids:
        year = estadistica_year.read(cr, uid, estadistica_year_ids[0], ['revisio'])
        revisio = year['revisio'] + 1
    else:
        revisio = 0

    # Per a cada provincia en (Trafos, LAT, LBT)
    sql_provincies = """
        SELECT DISTINCT p.id AS id
        FROM giscedata_transformador_trafo t, giscedata_cts ct, res_municipi m,
          res_country_state p, giscedata_cts_installacio inst,
          giscedata_transformador_estat e, giscedata_transformador_connexio c
        WHERE c.trafo_id = t.id
          AND t.id_estat = e.id
          AND ct.id_installacio = inst.id
          AND t.ct = ct.id AND ct.id_municipi = m.id
          AND m.state = p.id
          AND (t.tiepi = TRUE OR t.estadistica = TRUE)
          AND e.codi = 1 AND c.conectada = TRUE
    UNION
          SELECT DISTINCT p.id AS id
          FROM res_municipi m, res_country_state p, giscedata_at_linia lat,
            giscedata_at_tram tram
          WHERE lat.municipi = m.id AND m.state = p.id
            AND tram.linia = lat.id AND lat.baixa = FALSE
            AND lat.propietari = TRUE AND tram.baixa = FALSE
            AND lat.tensio IS NOT NULL AND tram.tipus IN (1,2)
    UNION
          SELECT DISTINCT p.id AS id
          FROM giscedata_bt_element e, res_municipi m,
            res_country_state p
          WHERE e.municipi = m.id
            AND m.state = p.id
            AND (e.baixa = FALSE OR e.baixa IS NULL)
            AND e.tipus_linia IN (1,2)
    """
    cr.execute(sql_provincies)
    for provincia in cr.dictfetchall():

        # Creem el nou any
        year_values = {
            'name': data['form']['year'],
            'provincia': provincia['id'],
            'revisio': revisio
        }
        year_id = estadistica_year.create(cr, uid, year_values)

        # Calculem les dades dels trafos
        sql_trafos = """
        SELECT count(p.name) AS trafo_num,
          sum(t.potencia_nominal) AS trafo_pot,
          CASE WHEN c.connectada_p2 = TRUE
          THEN
            CASE
              WHEN c.tensio_p2 < 1000*1.05
                THEN '1000'
              WHEN c.tensio_p2 >= 1000*1.05 AND c.tensio_p2 < 4500*1.05
                THEN '4500'
              WHEN c.tensio_p2 >= 4500*1.05 AND c.tensio_p2 < 8000*1.05
                THEN '8000'
              WHEN c.tensio_p2 >= 8000*1.05 AND c.tensio_p2 < 12500*1.05
                THEN '12500'
              WHEN c.tensio_p2 >= 12500*1.05 AND c.tensio_p2 < 17500*1.05
                THEN '17500'
              WHEN c.tensio_p2 >= 17500*1.05 AND c.tensio_p2 < 25000*1.05
                THEN '25000'
              WHEN c.tensio_p2 >= 25000*1.05 AND c.tensio_p2 < 37500*1.05
               THEN '37500'
              WHEN c.tensio_p2 >= 37500*1.05 AND c.tensio_p2 < 55500*1.05
                THEN '55500'
              WHEN c.tensio_p2 >= 55500*1.05 AND c.tensio_p2 < 99000*1.05
                THEN '99000'
              WHEN c.tensio_p2 >= 99000*1.05 AND c.tensio_p2 < 176000*1.05
                THEN '176000'
              WHEN c.tensio_p2 >= 176000*1.05 AND c.tensio_p2 < 300000*1.05
                THEN '300000'
              WHEN c.tensio_p2 >= 300000*1.05
                THEN '300001'
            END
          ELSE
            CASE
              WHEN c.tensio_primari < 1000*1.05
              THEN '1000'
                WHEN c.tensio_primari >= 1000*1.05
                  AND c.tensio_primari < 4500*1.05
                    THEN '4500'
                WHEN c.tensio_primari >= 4500*1.05
                  AND c.tensio_primari < 8000*1.05
                    THEN '8000'
                WHEN c.tensio_primari >= 8000*1.05
                  AND c.tensio_primari < 12500*1.05
                    THEN '12500'
                WHEN c.tensio_primari >= 12500*1.05
                  AND c.tensio_primari < 17500*1.05
                    THEN '17500'
                WHEN c.tensio_primari >= 17500*1.05
                  AND c.tensio_primari < 25000*1.05
                    THEN '25000'
                WHEN c.tensio_primari >= 25000*1.05
                  AND c.tensio_primari < 37500*1.05
                    THEN '37500'
                WHEN c.tensio_primari >= 37500*1.05
                  AND c.tensio_primari < 55500*1.05
                    THEN '55500'
                WHEN c.tensio_primari >= 55500*1.05
                  AND c.tensio_primari < 99000*1.05
                    THEN '99000'
                WHEN c.tensio_primari >= 99000*1.05
                  AND c.tensio_primari < 176000*1.05
                    THEN '176000'
                WHEN c.tensio_primari >= 176000*1.05
                  AND c.tensio_primari < 300000*1.05
                    THEN '300000'
                WHEN c.tensio_primari >= 300000*1.05
                  THEN '300001'
                END
          END AS t_name
        FROM
          giscedata_transformador_trafo t,
          giscedata_cts ct, res_municipi m,
          res_country_state p,
          giscedata_cts_installacio inst,
          giscedata_transformador_estat e,
          giscedata_transformador_connexio c
        WHERE
          c.trafo_id = t.id
            AND t.id_estat = e.id
            AND ct.id_installacio = inst.id
            AND t.ct = ct.id
            AND ct.id_municipi = m.id
            AND m.state = p.id
            AND p.id = %s
            AND (t.tiepi = TRUE OR t.estadistica = TRUE)
            AND e.codi = 1
            AND c.conectada = TRUE
        GROUP BY t_name
        """

        cr.execute(sql_trafos, (int(provincia['id']),))
        for trafo in cr.dictfetchall():
            trafo['name'] = trafo['t_name']
            del trafo['t_name']
            trafo['year'] = year_id
            estadistica.create(cr, uid, trafo)

        # Calculem les dades de linies AT
        sql_lat = """
          SELECT sum(tram.longitud_cad::FLOAT) AS long,
            tram.tipus,
            tram.circuits,
            CASE
              WHEN lat.tensio < 1000*1.05 THEN '1000'
              WHEN lat.tensio >= 1000*1.05 AND lat.tensio < 4500*1.05
                THEN '4500'
              WHEN lat.tensio >= 4500*1.05 AND lat.tensio < 8000*1.05
                THEN '8000'
              WHEN lat.tensio >= 8000*1.05 AND lat.tensio < 12500*1.05
                THEN '12500'
              WHEN lat.tensio >= 12500*1.05 AND lat.tensio < 17500*1.05
                THEN '17500'
              WHEN lat.tensio >= 17500*1.05 AND lat.tensio < 25000*1.05
                THEN '25000'
              WHEN lat.tensio >= 25000*1.05 AND lat.tensio < 37500*1.05
                THEN '37500'
              WHEN lat.tensio >= 37500*1.05 AND lat.tensio < 55500*1.05
                THEN '55500'
              WHEN lat.tensio >= 55500*1.05 AND lat.tensio < 99000*1.05
                THEN '99000'
              WHEN lat.tensio >= 99000*1.05 AND lat.tensio < 176000*1.05
                THEN '176000'
              WHEN lat.tensio >= 176000*1.05 AND lat.tensio < 300000*1.05
                THEN '300000'
              WHEN lat.tensio >= 300000*1.05
                THEN '300001'
            END AS t_name
              FROM
                res_municipi m,
                res_country_state p,
                giscedata_at_linia lat,
                giscedata_at_tram tram
            WHERE lat.municipi = m.id
              AND m.state = p.id
              AND p.id = %s
              AND tram.linia = lat.id
              AND lat.baixa = FALSE
              AND lat.propietari = TRUE
              AND tram.baixa = FALSE
              AND lat.tensio IS NOT NULL
              AND tram.tipus IN (1,2)
            GROUP BY t_name, tram.tipus, tram.circuits
        """
        cr.execute(sql_lat, (int(provincia['id']),))
        for lat in cr.dictfetchall():
            vals = {
                'name': lat['t_name'],
                'year': year_id
            }
            if lat['circuits'] == 1 and lat['tipus'] == 1:
                vals['l_1c'] = lat['long']
            elif lat['circuits'] == 2:
                vals['l_2c'] = lat['long']
            elif lat['circuits'] > 2:
                vals['l_m2c'] = lat['long']
            elif lat['tipus'] == 2:
                vals['l_sub'] = lat['long']
            estadistica.create(cr, uid, vals)

        # Calculem les dades de linies BT
        sql_lbt = """
          SELECT '1000' AS t_name, sum(e.longitud_cad) AS long, e.tipus_linia
          FROM giscedata_bt_element e, res_municipi m, res_country_state p
          WHERE e.municipi = m.id
            AND m.state = p.id
            AND p.id = %s
            AND (e.baixa = FALSE OR e.baixa IS NULL)
            AND e.tipus_linia IN (1,2)
          GROUP BY e.tipus_linia
        """
        cr.execute(sql_lbt, (int(provincia['id']),))
        for lbt in cr.dictfetchall():
            vals = {
                'name': lbt['t_name'],
                'year': year_id
            }
            if lbt['tipus_linia'] == 1:
                vals['l_1c'] = lbt['long']
            else:
                vals['l_sub'] = lbt['long']
            estadistica.create(cr, uid, vals)

        # Creem línies buides
        names = ['1000', '4500', '8000', '12500', '17500', '25000', '37500',
                 '55500', '99000', '176000', '300000', '300001']
        for name in names:
            estadistica.create(cr, uid, {'name': name, 'year': year_id})

        data['print_ids'].append(year_id)

    return {}


def _check_print_report(self, cr, uid, data, context={}):
    if len(data['print_ids']) and data['form']['print_pdf']:
        return 'print_report'
    elif not len(data['print_ids']):
        return 'end'
    elif len(data['print_ids']) and not data['form']['print_pdf']:
        return 'mostrar_tab'


def _print_report(self, cr, uid, data, context={}):
    return {'ids': data['print_ids']}


def _mostrar_tab(self, cr, uid, data, context={}):
    action = {
        'domain': "[('id','in', ["+','.join(map(str, map(int, data['print_ids'])))+"])]",
        'view_type': 'form',
        'view_mode': 'tree,form',
        'res_model': 'giscedata.industria.estadistica.year',
        'view_id': False,
        'name': 'Estadística Indústria %s' % (data['form']['year']),
        'res_id': False,
        'auto_refresh': False,
        'limit': len(data['print_ids']),
        'type': 'ir.actions.act_window'
    }
    return action


def _pre_end(self, cr, uid, data, context={}):
    return {}


class wizard_estadistica_industria(wizard.interface):
    """
    Wizard class for estadistica industria
    """
    states = {
        'init': {
            'actions': [],
            'result': {'type': 'state', 'state': 'escollir_any'}
        },
        'escollir_any': {
            'actions': [],
            'result': {
                'type': 'form',
                'arch': _escollir_any_form,
                'fields': _escollir_any_fields,
                'state': [('calc', 'Generar')]
            }
        },
        'calc': {
            'actions': [_calc],
            'result': {'type': 'choice', 'next_state': _check_print_report}
        },
        'print_report': {
            'actions': [_print_report],
            'result': {
                'type': 'print',
                'report': 'giscedata.industria.estadistica',
                'get_id_from_action': True,
                'state': 'pre_end'
            }
        },
        'mostrar_tab': {
            'actions': [],
            'result': {
                'type': 'action',
                'action': _mostrar_tab,
                'state': 'pre_end'
            }
        },
        'pre_end': {
            'actions': [_pre_end],
            'result': {'type': 'state', 'state': 'end'},
        },
        'end': {
            'actions': [],
            'result': {'type': 'state', 'state': 'end'}
        }
    }


wizard_estadistica_industria('giscedata.industria.estadistica')
