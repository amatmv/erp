# -*- coding: utf-8 -*-
import subprocess
import tempfile
import base64
import os
import zipfile
import netsvc
import datetime

from osv import osv, fields
import tools
from tools.translate import _
import click


class WizardGenerarCnmcPla(osv.osv_memory):
    """
    Wizard per generar els XML de CNMC
    """

    _name = 'wizard.generar.cnmc.pla'
    id_pla = 0

    def get_codi_r1(self, cursor, uid, ids, context=None):
        """
        Returns the R1 code of the company

        :param cursor: Database cursor
        :param uid: User id
        :param ids: Unused
        :param context: OpenERP context
        :return: Code R1
        :rtype: str
        """

        user_obj = self.pool.get('res.users')
        user = user_obj.browse(cursor, uid, uid, context=context)

        r1 = user.company_id.partner_id.ref2

        if r1 and r1.startswith('R1-'):
            r1 = r1[3:]
        return r1

    def action_exec_script(self, cursor, uid, ids, context=None):
        """
        Llença l'script per l'informe XML.

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param ids:
        :type ids: list
        :param context: OpenERP context
        :type context: dict
        :return: None
        """

        r1 = self.get_codi_r1(cursor, uid, [], context)
        wizard = self.browse(cursor, uid, ids[0], context)
        self.id_pla = context['active_id']
        model_pla = self.pool.get('giscedata.cnmc.pla_inversio')
        pla = model_pla.browse(cursor, uid, self.id_pla)

        search_params = [('pla_inversio', '=', self.id_pla)]
        resums = self.pool.get('giscedata.cnmc.resum_any')
        resums_ids = resums.search(cursor, uid, search_params)
        anys = resums.read(cursor, uid, resums_ids, ['anyo'])

        user = self.pool.get('res.users').browse(cursor, uid, uid)
        port = tools.config['port'] or 8069

        cnmc_files = {
            "res-4667-lat": "PI_R1-{}_1.txt",
            "res-4667-lbt": "PI_R1-{}_2.txt",
            "res-4667-otros": "PI_R1-{}_3.txt",
            "res-4667-pos": "PI_R1-{}_4.txt",
            "res-4667-maq": "PI_R1-{}_5.txt",
            "res-4667-des": "PI_R1-{}_6.txt",
            "res-4667-fia": "PI_R1-{}_7.txt",
            "res-4667-ct": "PI_R1-{}_8.txt",
            "res-4667-res": "PI_RESUMEN_R1-{}.txt",
            "res-4667-resccaa": "PI_RESUMEN_CCAA_R1-{}.txt",
            "res-4667-pro": "PI_PROYECTOS_R1-{}.txt",
            "res-4667-macro": "PI_MACRO_R1-{}.txt"
        }
        zip_url = tempfile.mkstemp()[1]
        z = zipfile.ZipFile(zip_url, "w")

        for command, tmpl_fname in cnmc_files.items():
            filename = tempfile.mkstemp()[1]
            cnmc_files[command] = filename

            if int(click.__version__.split(".")[0]) < 7:
                command = command.replace("-", "_")

            csv_args = [
                "cnmc", command, "-d", cursor.dbname, "-p", port,
                "-u", user.login, "-w", user.password, "-o", filename,
                "-y", pla.anyo, "--no-interactive"
            ]

            logger = netsvc.Logger()
            logger.notifyChannel(
                'server', netsvc.LOG_INFO,
                'script executed: {}'.format(' '.join(map(str, csv_args)))
            )

            env = os.environ.copy()
            retcode = subprocess.call(map(str, csv_args), env=env)

            if retcode:
                msg = _('El procés de generar el fitxer ha fallat. Codi: {}')
                raise osv.except_osv(
                    'Error',
                    msg.format(retcode)
                )

            tmpcsv = open(filename, 'r+')
            report = tmpcsv.read()
            os.unlink(filename)
            z.writestr(tmpl_fname.format(r1), report)
        z.close()
        with open(zip_url, "r") as f:
            zip_data = f.read()
        zip_data = base64.b64encode(zip_data)
        fecha = datetime.datetime.now().strftime("%Y%m%d")
        filenom = (
        'PI_{}{}_{}_R1-{}_{}.zip'.format(
            str(min(anys)['anyo'])[2:],
            str(max(anys)['anyo'])[2:],
            fecha,
            pla.codi,
            pla.anyo)
        )
        wizard.write({'name': filenom, 'file': zip_data, 'state': 'done'})

        os.unlink(zip_url)

    _columns = {
        'state': fields.selection(
            [('init', 'Init'),
             ('done', 'Done'), ],
            'State'),
        'name': fields.char('File name', size=64),
        'file': fields.binary('Fitxer'),
    }

    _defaults = {
        'state': lambda *a: 'init'
    }

WizardGenerarCnmcPla()
