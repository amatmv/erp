# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _

class WizardGenerarResum(osv.osv_memory):
    """
    Wizard per generar els XML de CNMC
    """

    _name = 'wizard.generar.resum'
    id_pla = 0

    def add_status(self, cursor, uid, ids, status):
        wizard = self.browse(cursor, uid, ids[0], None)
        if wizard.status:
            status = wizard.status + '\n' + status
        wizard.write({'status': status})

    def get_resumen_any(self, cursor, uid, ids, context=None):
        """
        Gets the ids of the resumen anys on the pla

        :param cursor: Database cursor
        :param uid: User id
        :param ids: List of the plans to get the resums
        :param context: OpenERP context
        :return: Ids of the resums_any
        :rtype: list
        """

        model_res = self.pool.get('giscedata.cnmc.resum_any')
        search_params = [("pla_inversio", "in", ids)]

        return model_res.search(cursor, uid, search_params)

    def fill_resum_pla(self, cursor, uid, ids, context=None):
        """
        Fill resum

        :param cursor: Database cursor
        :param uid: User id
        :param ids: Afected ids
        :param context: OpenERP context
        :return:
        """

        vol_total_inv_prv = dict.fromkeys(ids, 0)
        ayudas_prv = dict.fromkeys(ids, 0)
        financiacion_prv = dict.fromkeys(ids, 0)
        vpi_retribuible_prv = dict.fromkeys(ids, 0)
        vol_total_inv_bt_prv = dict.fromkeys(ids, 0)

        model_lat = self.pool.get("giscedata.cnmc.linies")
        model_lbt = self.pool.get('giscedata.cnmc.liniesbt')
        model_altres = self.pool.get('giscedata.cnmc.altres')
        model_pos = self.pool.get('giscedata.cnmc.posicions')
        model_maq = self.pool.get('giscedata.cnmc.maquines')
        model_des = self.pool.get('giscedata.cnmc.despatx')
        model_fia = self.pool.get('giscedata.cnmc.fiabilitat')
        model_ct = self.pool.get('giscedata.cnmc.ct')
        model_res = self.pool.get('giscedata.cnmc.resum_any')
        model_proj = self.pool.get('giscedata.cnmc.projectes')
        frri_data = model_res.read(cursor, uid, ids, ["frri"], context)
        frri = {}
        for element in frri_data:
            frri[element["id"]] = element["frri"]

        for resum in ids:
            search_params_elem = [("resums_inversio", "=", resum)]

            ids_lat = model_lat.search(cursor, uid, search_params_elem)
            ids_lbt = model_lbt.search(cursor, uid, search_params_elem)
            ids_altres = model_altres.search(cursor, uid, search_params_elem)
            ids_pos = model_pos.search(cursor, uid, search_params_elem)
            ids_des = model_des.search(cursor, uid, search_params_elem)
            ids_maq = model_maq.search(cursor, uid, search_params_elem)
            ids_fia = model_fia.search(cursor, uid, search_params_elem)
            ids_ct = model_ct.search(cursor, uid, search_params_elem)

            fields_default = ["codi", "inv_financiada", "ajudes",
                              "vol_total_inv", "vpi_retri"]

            fields_lbt = [
                "vol_inv_prev", "ayudas_prv", "financiacion_prv",
                "vpi_retribuible_prv", "cod_proyecto"
            ]

            data_lat = model_lat.read(cursor, uid, ids_lat, fields_default)
            data_lbt = model_lbt.read(cursor, uid, ids_lbt, fields_lbt)
            data_altres = model_altres.read(cursor, uid, ids_altres, fields_default)
            data_pos = model_pos.read(cursor, uid, ids_pos, fields_default)
            data_des = model_des.read(cursor, uid, ids_des, fields_default)
            data_maq = model_maq.read(cursor, uid, ids_maq, fields_default)
            data_fia = model_fia.read(cursor, uid, ids_fia, fields_default)
            data_ct = model_ct.read(cursor, uid, ids_ct, fields_default)

            datas = [data_lat, data_altres, data_pos,data_des, data_maq,
                     data_fia, data_ct]

            for readed_data in datas:
                for data in readed_data:
                    vol_total_inv_prv[resum] += data["vol_total_inv"]
                    ayudas_prv[resum] += data["ajudes"]
                    financiacion_prv[resum] += data["inv_financiada"]
                    vpi_retribuible_prv[resum] += data["vpi_retri"]

            for data in data_lbt:
                vol_total_inv_prv[resum] += data["vol_inv_prev"]
                ayudas_prv[resum] += data["ayudas_prv"]
                financiacion_prv[resum] += data["financiacion_prv"]
                vpi_retribuible_prv[resum] += data["vpi_retribuible_prv"]
                vol_total_inv_bt_prv[resum] += data["vol_inv_prev"]

            n_projectes = len(model_proj.search(
                cursor, uid,
                [("resums_inversio", "=", resum)]))

            model_res.write(
                cursor, uid, resum, {
                    "volum_total_inv": vol_total_inv_prv[resum],
                    "ajudes_prev": ayudas_prv[resum],
                    "financiacio": financiacion_prv[resum],
                    "vpi_retribuible_prv": vpi_retribuible_prv[resum] * frri[resum],
                    "voltotal_inv_bt_prv": vol_total_inv_bt_prv[resum],
                    "n_projectes": n_projectes,


                }
            )

    def get_all_ccaas(self, cursor, uid, ids, context=None):
        """
        Gets all the CCAAs from from all the models

        :param cursor: Database cursor
        :param uid: User id
        :param ids: List of the ids to resum_any
        :param context: OpenERP context
        :return: List of CCAAS ids
        """

        search_params = [("resums_inversio", "in", ids)]
        model_lat = self.pool.get("giscedata.cnmc.linies")
        model_lbt = self.pool.get('giscedata.cnmc.liniesbt')
        model_altres = self.pool.get('giscedata.cnmc.altres')
        model_pos = self.pool.get('giscedata.cnmc.posicions')
        model_maq = self.pool.get('giscedata.cnmc.maquines')
        model_des = self.pool.get('giscedata.cnmc.despatx')
        model_fia = self.pool.get('giscedata.cnmc.fiabilitat')
        model_ct = self.pool.get('giscedata.cnmc.ct')

        ids_lat = model_lat.search(cursor, uid, search_params)
        ids_lbt = model_lbt.search(cursor, uid, search_params)
        ids_altres = model_altres.search(cursor, uid, search_params)
        ids_pos = model_pos.search(cursor, uid, search_params)
        ids_des = model_des.search(cursor, uid, search_params)
        ids_maq = model_maq.search(cursor, uid, search_params)
        ids_fia = model_fia.search(cursor, uid, search_params)
        ids_ct = model_ct.search(cursor, uid, search_params)

        ccaas_lat = model_lat.read(cursor, uid, ids_lat, ["ccaa", "ccaa_2"])
        ccaas_lbt = model_lbt.read(
            cursor, uid, ids_lbt, ["codigo_ccaa_1", "codigo_ccaa_2"])
        ccaas_altres = model_altres.read(
            cursor, uid, ids_altres, ["codigo_ccaa"])
        ccaas_pos = model_pos.read(cursor, uid, ids_pos, ["ccaa"])
        ccaas_maq = model_maq.read(cursor, uid, ids_maq, ["ccaa"])
        ccaas_des = model_des.read(cursor, uid, ids_des, ["codigo_ccaa"])
        ccaas_fia = model_fia.read(cursor, uid, ids_fia, ["ccaa"])
        ccaas_ct = model_ct.read(cursor, uid, ids_ct, ["ccaa"])

        ccaas_lat1 = [k["ccaa"][0] for k in ccaas_lat if k["ccaa"]]
        ccaas_lat2 = [k["ccaa_2"][0] for k in ccaas_lat if k["ccaa_2"]]

        ccaas_lbt1 = [k["codigo_ccaa_1"][0] for k in ccaas_lbt if k["codigo_ccaa_1"]]
        ccaas_lbt2 = [k["codigo_ccaa_2"][0] for k in ccaas_lbt if k["codigo_ccaa_2"]]

        ccaas_altres = [k["codigo_ccaa"][0] for k in ccaas_altres if k["codigo_ccaa"]]
        ccaas_pos = [k["ccaa"][0] for k in ccaas_pos if k["ccaa"]]
        ccaas_maq = [k["ccaa"][0] for k in ccaas_maq if k["ccaa"]]
        ccaas_des = [k["codigo_ccaa"][0] for k in ccaas_des if k["codigo_ccaa"]]
        ccaas_fia = [k["ccaa"][0] for k in ccaas_fia if k["ccaa"]]
        ccaas_ct = [k["ccaa"][0] for k in ccaas_ct if k["ccaa"]]
        ccaas = set(
            ccaas_lat1 + ccaas_lat2 + ccaas_lbt1 + ccaas_lbt2 + ccaas_altres +
            ccaas_pos + ccaas_maq+ccaas_des + ccaas_fia + ccaas_ct)
        return list(ccaas)

    def get_all_years(self, cursor, uid, ids, context=None):
        """
        Get all the years of the resum_any

        :param cursor: Database cursor
        :param uid: User id
        :param ids: ids of tre resum_any
        :type ids: list
        :param context: OpenERP list
        :return: List of years
        :rtype:list
        """

        model_any = self.pool.get("giscedata.cnmc.resum_any")
        data = model_any.read(cursor, uid, ids, ["anyo"])
        return [y["anyo"] for y in data]

    def get_all_year_resum(self, cursor, uid, ident, context=None):
        """
        Get the id of the resums of all the years

        :param cursor: Database cursor
        :param uid: User id
        :param ident: id of the resum_any
        :type ident: int
        :param context: OpenERP list
        :return: Year id
        :rtype: dict
        """

        model_any = self.pool.get("giscedata.cnmc.resum_any")
        ids = model_any.search(
            cursor, uid, [("pla_inversio", "=", ident)])
        data = model_any.read(cursor, uid, ids, ["anyo"])
        ret = {}
        for d in data:
            ret[d["anyo"]] = d["id"]
        return ret

    def get_year_resum(self, cursor, uid, ids, context=None):
        """
        Get the year of a resumen inversio

        :param cursor: Database cursor
        :param uid: User id
        :param ids: ids of the resumen
        :param context: OpenERP context
        :return: Year
        """
        model_resum = self.pool.get("giscedata.cnmc.resum_any")
        data = model_resum.read(cursor, uid, ids, ["anyo"])[0]
        return data["anyo"]

    def fill_resum_ccaa(self, cursor, uid, ids, context=None):
        """
        Fill resum CCAA

        :param cursor: Database cursor
        :param uid: Uer id
        :param ids: Ids of the giscedata.cnmc.resum_any to fill
        :param context: OpenERP context
        :return:
        """

        model_resum = self.pool.get("giscedata.cnmc.resum_any")
        resums = model_resum.read(cursor, uid, ids, ["frri"])
        frri_id = {}
        for resum in resums:
            frri_id[resum["id"]] = resum["frri"]

        model_ccaa = self.pool.get("giscedata.cnmc.resum_ccaa")
        search_params = [("resums_inversio", "in", ids)]
        del_ids = model_ccaa.search(cursor, uid, search_params)
        model_ccaa.unlink(cursor, uid, del_ids)
        model_proj = self.pool.get('giscedata.cnmc.projectes')

        ccaas = self.get_all_ccaas(cursor, uid, ids, context)
        years = self.get_all_years(cursor, uid, ids, context)

        vol_total_inv_prv_ccaa = dict.fromkeys(years, {})
        ayudas_prv_ccaa = dict.fromkeys(years, {})
        financiacion_prv_ccaa = dict.fromkeys(years, {})
        vpi_retribuible_prv_ccaa = dict.fromkeys(years, {})
        num_proyectos_ccaa = dict.fromkeys(years, {})
        vol_total_inv_bt_prv_ccaa = dict.fromkeys(years, {})
        for year in years:
            vol_total_inv_prv_ccaa[year] = dict.fromkeys(ccaas, 0)
            ayudas_prv_ccaa[year] = dict.fromkeys(ccaas, 0)
            financiacion_prv_ccaa[year] = dict.fromkeys(ccaas, 0)
            vpi_retribuible_prv_ccaa[year] = dict.fromkeys(ccaas, 0)
            vol_total_inv_bt_prv_ccaa[year] = dict.fromkeys(ccaas, 0)
        for year in years:
            num_proyectos_ccaa[year] = {}
            for ccaa in ccaas:
                num_proyectos_ccaa[year][ccaa] = set()

        model_lat = self.pool.get("giscedata.cnmc.linies")
        model_lbt = self.pool.get('giscedata.cnmc.liniesbt')
        model_altres = self.pool.get('giscedata.cnmc.altres')
        model_pos = self.pool.get('giscedata.cnmc.posicions')
        model_maq = self.pool.get('giscedata.cnmc.maquines')
        model_des = self.pool.get('giscedata.cnmc.despatx')
        model_fia = self.pool.get('giscedata.cnmc.fiabilitat')
        model_ct = self.pool.get('giscedata.cnmc.ct')

        ids_lat = model_lat.search(cursor, uid, search_params)
        ids_lbt = model_lbt.search(cursor, uid, search_params)
        ids_altres = model_altres.search(cursor, uid, search_params)
        ids_pos = model_pos.search(cursor, uid, search_params)
        ids_des = model_des.search(cursor, uid, search_params)
        ids_maq = model_maq.search(cursor, uid, search_params)
        ids_fia = model_fia.search(cursor, uid, search_params)
        ids_ct = model_ct.search(cursor, uid, search_params)
        ids_proj = model_proj.search(cursor, uid, search_params)

        fields_lat = ["ajudes", "vol_total_inv", "inv_financiada",
                      "vpi_retri", "ccaa", "ccaa_2", "resums_inversio",
                      "codi"]

        fields_lbt = ["cod_proyecto", "vol_inv_prev", "ayudas_prvs",
                      "financiacion_prv", "vpi_retribuible_prv",
                      "resums_inversio", "codigo_ccaa_1", "codigo_ccaa_2",
                      "ayudas_prv", "vol_inv_prev"]

        fields_altres = ["codi", "codigo_ccaa", "vol_total_inv",
                         "inv_financiada", "ajudes", "vpi_retri",
                         "resums_inversio"]

        fields_pos = ["codi", "inv_financiada", "ajudes", "vol_total_inv",
                      "ccaa", "resums_inversio", "vpi_retri"]

        fields_des = ["codi", "codigo_ccaa", "inv_financiada", "ajudes",
                      "vol_total_inv", "vpi_retri", "vol_total_inv",
                      "resums_inversio"]

        fields_maq = ["codi", "inv_financiada", "ajudes", "vol_total_inv",
                      "vpi_retri", "ccaa", "resums_inversio"]

        fields_fia = ["codi", "inv_financiada", "ajudes", "vol_total_inv",
                      "vpi_retri", "ccaa", "resums_inversio"]

        fields_ct = ["codi", "inv_financiada", "ajudes", "vol_total_inv",
                     "vpi_retri", "ccaa", "resums_inversio"]

        fields_proj = ["codi", "ccaa", "resums_inversio"]

        data_lat = model_lat.read(cursor, uid, ids_lat, fields_lat)
        data_lbt = model_lbt.read(cursor, uid, ids_lbt, fields_lbt)
        data_altres = model_altres.read(cursor, uid, ids_altres, fields_altres)
        data_pos = model_pos.read(cursor, uid, ids_pos, fields_pos)
        data_des = model_des.read(cursor, uid, ids_des, fields_des)
        data_maq = model_maq.read(cursor, uid, ids_maq, fields_maq)
        data_fia = model_fia.read(cursor, uid, ids_fia, fields_fia)
        data_ct = model_ct.read(cursor, uid, ids_ct, fields_ct)
        data_proj = model_proj.read(cursor, uid, ids_proj, fields_proj)

        for data in data_proj:
            if data["ccaa"]:
                ccaa = data["ccaa"][0]
                year = self.get_year_resum(
                    cursor, uid, data["resums_inversio"], context)
                num_proyectos_ccaa[year][ccaa].add(data["codi"])

        for data in data_lat:
            if data["ccaa"]:
                ccaa = data["ccaa"][0]
                year = self.get_year_resum(
                cursor, uid, data["resums_inversio"], context)
                vol_total_inv_prv_ccaa[year][ccaa] += data["vol_total_inv"]
                ayudas_prv_ccaa[year][ccaa] += data["ajudes"]
                financiacion_prv_ccaa[year][ccaa] += data["inv_financiada"]
                vpi_retribuible_prv_ccaa[year][ccaa] += data["vpi_retri"]

        for data in data_lbt:
            if data["codigo_ccaa_1"]:
                ccaa = data["codigo_ccaa_1"][0]
                year = self.get_year_resum(
                    cursor, uid, data["resums_inversio"], context)
                vol_total_inv_prv_ccaa[year][ccaa] += data["vol_inv_prev"]
                ayudas_prv_ccaa[year][ccaa] += data["ayudas_prv"]
                financiacion_prv_ccaa[year][ccaa] += data["financiacion_prv"]
                vpi_retribuible_prv_ccaa[year][ccaa] += data["vpi_retribuible_prv"]
                vol_total_inv_bt_prv_ccaa[year][ccaa] += data["vol_inv_prev"]

        for data in data_altres:
            if data["codigo_ccaa"]:
                ccaa = data["codigo_ccaa"][0]
                year = self.get_year_resum(
                    cursor, uid, data["resums_inversio"], context)
                vol_total_inv_prv_ccaa[year][ccaa] += data["vol_total_inv"]
                ayudas_prv_ccaa[year][ccaa] += data["ajudes"]
                financiacion_prv_ccaa[year][ccaa] += data["inv_financiada"]
                vpi_retribuible_prv_ccaa[year][ccaa] += data["vpi_retri"]

        for data in data_pos:
            if data["ccaa"]:
                ccaa = data["ccaa"][0]
                year = self.get_year_resum(
                    cursor, uid, data["resums_inversio"], context)
                vol_total_inv_prv_ccaa[year][ccaa] += data["vol_total_inv"]
                ayudas_prv_ccaa[year][ccaa] += data["ajudes"]
                financiacion_prv_ccaa[year][ccaa] += data["inv_financiada"]
                vpi_retribuible_prv_ccaa[year][ccaa] += data["vpi_retri"]

        for data in data_des:
            if data["codigo_ccaa"]:
                ccaa = data["codigo_ccaa"][0]
                year = self.get_year_resum(
                    cursor, uid, data["resums_inversio"], context)
                vol_total_inv_prv_ccaa[year][ccaa] += data["vol_total_inv"]
                ayudas_prv_ccaa[year][ccaa] += data["ajudes"]
                financiacion_prv_ccaa[year][ccaa] += data["inv_financiada"]
                vpi_retribuible_prv_ccaa[year][ccaa] += data["vpi_retri"]

        for data in data_maq:
            if data["ccaa"]:
                ccaa = data["ccaa"][0]
                year = self.get_year_resum(
                    cursor, uid, data["resums_inversio"], context)
                vol_total_inv_prv_ccaa[year][ccaa] += data["vol_total_inv"]
                ayudas_prv_ccaa[year][ccaa] += data["ajudes"]
                financiacion_prv_ccaa[year][ccaa] += data["inv_financiada"]
                vpi_retribuible_prv_ccaa[year][ccaa] += data["vpi_retri"]

        for data in data_fia:
            if data["ccaa"]:
                ccaa = data["ccaa"][0]
                year = self.get_year_resum(
                    cursor, uid, data["resums_inversio"], context)
                vol_total_inv_prv_ccaa[year][ccaa] += data["vol_total_inv"]
                ayudas_prv_ccaa[year][ccaa] += data["ajudes"]
                financiacion_prv_ccaa[year][ccaa] += data["inv_financiada"]
                vpi_retribuible_prv_ccaa[year][ccaa] += data["vpi_retri"]

        for data in data_ct:
            if data["ccaa"]:
                ccaa = data["ccaa"][0]
                year = self.get_year_resum(
                    cursor, uid, data["resums_inversio"], context)
                vol_total_inv_prv_ccaa[year][ccaa] += data["vol_total_inv"]
                ayudas_prv_ccaa[year][ccaa] += data["ajudes"]
                financiacion_prv_ccaa[year][ccaa] += data["inv_financiada"]
                vpi_retribuible_prv_ccaa[year][ccaa] += data["vpi_retri"]

        year_resum = self.get_all_year_resum(
            cursor, uid, context["active_id"], context)

        for year in years:
            id_resum = year_resum[year]
            for ccaa in ccaas:
                vpi = vol_total_inv_prv_ccaa[year][ccaa] * frri_id[id_resum]
                model_ccaa.create(cursor, uid,
                    {
                        "codigo_ccaa": ccaa,
                        "anio_periodo": year,
                        "vol_total_inv_prv_ccaa": vol_total_inv_prv_ccaa[year][ccaa],
                        "ayudas_prv_ccaa": ayudas_prv_ccaa[year][ccaa],
                        "financiacion_prv_ccaa": financiacion_prv_ccaa[year][ccaa],
                        "num_proyectos_ccaa": len(set(num_proyectos_ccaa[year][ccaa])),
                        "vol_total_inv_bt_prv_ccaa": vol_total_inv_bt_prv_ccaa[year][ccaa],
                        "vpi_retribuible_prv_ccaa": vpi,
                        "resums_inversio": id_resum
                    }, context
                )

    def get_proyectos_pla(self, cursor, uid, ident, context=None):
        """
        Returns the list of project ids of the pla

        :param cursor: Database cursor
        :param uid: User id
        :param ident: identifier of the pla
        :type ident:int
        :param context: OpenERP context
        :return: Projects ids
        :rtype: list
        """

        search_params_res = [("pla_inversio", "=", ident)]
        model_resum = self.pool.get("giscedata.cnmc.resum_any")
        ids_res = model_resum.search(cursor, uid, search_params_res)

        search_params_proj = [("resums_inversio", "in", ids_res)]
        model_proj = self.pool.get("giscedata.cnmc.projectes")
        return model_proj.search(cursor, uid, search_params_proj)

    def fill_projectes(self, cursor, uid, ids, context=None):
        """
        Fills projectes

        :param cursor: Database cursor
        :param uid: User id
        :param ids: Affected ids
        :param ids: int
        :param context: OpenERP context
        :type context: dict
        :return:
        """

        proj_ids = self.get_proyectos_pla(cursor, uid, context["active_id"], context)

        vol_total_inv_prev_proy = dict.fromkeys(proj_ids, 0)
        ayudas_prv_proy = dict.fromkeys(proj_ids, 0)
        financiacion_prv_proy = dict.fromkeys(proj_ids, 0)
        vpi_retribuible_prv_proy = dict.fromkeys(proj_ids, 0)

        search_params = [("resums_inversio", "in", ids)]

        model_lat = self.pool.get("giscedata.cnmc.linies")
        model_lbt = self.pool.get('giscedata.cnmc.liniesbt')
        model_altres = self.pool.get('giscedata.cnmc.altres')
        model_pos = self.pool.get('giscedata.cnmc.posicions')
        model_maq = self.pool.get('giscedata.cnmc.maquines')
        model_des = self.pool.get('giscedata.cnmc.despatx')
        model_fia = self.pool.get('giscedata.cnmc.fiabilitat')
        model_ct = self.pool.get('giscedata.cnmc.ct')
        model_proj = self.pool.get("giscedata.cnmc.projectes")

        ids_lat = model_lat.search(cursor, uid, search_params)
        ids_lbt = model_lbt.search(cursor, uid, search_params)
        ids_altres = model_altres.search(cursor, uid, search_params)
        ids_pos = model_pos.search(cursor, uid, search_params)
        ids_des = model_des.search(cursor, uid, search_params)
        ids_maq = model_maq.search(cursor, uid, search_params)
        ids_fia = model_fia.search(cursor, uid, search_params)
        ids_ct = model_ct.search(cursor, uid, search_params)

        fields_lbt = ["cod_proyecto", "vol_inv_prev", "ayudas_prv",
                      "financiacion_prv", "vpi_retribuible_prv"]

        fields_default = ["codi", "vol_total_inv", "vpi_retri",
                          "inv_financiada", "ajudes"]

        data_lat = model_lat.read(cursor, uid, ids_lat, fields_default)
        data_lbt = model_lbt.read(cursor, uid, ids_lbt, fields_lbt)
        data_altres = model_altres.read(cursor, uid, ids_altres, fields_default)
        data_pos = model_pos.read(cursor, uid, ids_pos, fields_default)
        data_des = model_des.read(cursor, uid, ids_des, fields_default)
        data_maq = model_maq.read(cursor, uid, ids_maq, fields_default)
        data_fia = model_fia.read(cursor, uid, ids_fia, fields_default)
        data_ct = model_ct.read(cursor, uid, ids_ct, fields_default)
        datas = [data_lat, data_altres, data_pos, data_des, data_maq, data_fia,
                 data_ct]

        for readed_data in datas:
            for data in readed_data:
                if data["codi"]:
                    proj_id = data["codi"][0]
                    if proj_id in proj_ids:
                        vol_total_inv_prev_proy[proj_id] += data["vol_total_inv"]
                        ayudas_prv_proy[proj_id] += data["ajudes"]
                        financiacion_prv_proy[proj_id] += data["inv_financiada"]
                        vpi_retribuible_prv_proy[proj_id] += data["vpi_retri"]

        for data in data_lbt:
            if data["cod_proyecto"]:
                proj_id = data["cod_proyecto"][0]
                if proj_id in proj_ids:
                    vol_total_inv_prev_proy[proj_id] += data["vol_inv_prev"]
                    ayudas_prv_proy[proj_id] += data["ayudas_prv"]
                    financiacion_prv_proy[proj_id] += data["financiacion_prv"]
                    vpi_retribuible_prv_proy[proj_id] += data["vpi_retribuible_prv"]

        for proj in proj_ids:
            model_proj.write(
                cursor, uid, proj,
                {
                    "vol_total_inv_prev_proy": vol_total_inv_prev_proy[proj],
                    "ayudas_prv_proy": ayudas_prv_proy[proj],
                    "financiacion_prv_proy": financiacion_prv_proy[proj],
                    "vpi_retribuible_prv_proy": vpi_retribuible_prv_proy[proj]
                },
                context
            )

    def action_exec_script(self, cursor, uid, ids, context=None):
        """
        Omple els resums i projectes

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: Unused
        :type ids: list
        :param context: OpenERP context
        :type context: dict
        :return: None
        """
        if not context:
            context = {}

        resumen_ids = self.get_resumen_any(
            cursor, uid, context["active_ids"], context)

        self.fill_resum_pla(cursor, uid, resumen_ids, context)
        self.fill_resum_ccaa(cursor, uid, resumen_ids, context)
        self.fill_projectes(cursor, uid, resumen_ids, context)
        self.add_status(cursor, uid, ids, _('Projectes i resums omplers'))

    _columns = {
        'state': fields.selection(
            [('init', 'Init'),
             ('done', 'Done'), ],
            'State'),
        "status": fields.text(_('Resultat')),
        'name': fields.char('File name', size=64),
    }

    _defaults = {
        'state': lambda *a: 'init'
    }

WizardGenerarResum()
