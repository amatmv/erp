# -*- coding: utf-8 -*-

import re
from osv import osv, fields
from tools.translate import _

FINALITAT = [('AB', 'Escomeses de barem'),
             ('ND', 'Nova demanda'),
             ('AN', 'Adaptació a normativa no estatal'),
             ('RE', 'Renovació de Equips'),
             ('RR', 'Resta')]

ESTADO = [
    ("0", "Element sense modificacions"),
    ("1", "Element amb modificacions"),
    ("2", "Alta element nou")
]

"""
Models for the generation of the 4666
"""


class GiscedataCnmcPlaInversio(osv.osv):
    """
    Plans de inversions del CNMC
    """

    _name = 'giscedata.cnmc.pla_inversio'
    _description = "Pla de inversions per any"
    _rec_name = 'codi'

    _columns = {
        'codi': fields.char('Codi R1', size=3, required=True),
        'anyo': fields.integer('Any', required=True),
        'resums_inversio': fields.one2many('giscedata.cnmc.resum_any',
                                           'pla_inversio', "Resums de l'any"),
    }

    _sql_constraints = [
        ('anyo', 'unique (anyo)',
         _('Ja existeix un pla de inversió per aquest any.'))
    ]

    _order = "anyo asc"

GiscedataCnmcPlaInversio()


class GiscedataCnmcResumAny(osv.osv):
    """
    Resums del pla de inversions
    """

    _name = 'giscedata.cnmc.resum_any'
    _description = "Resum de l'any"
    _rec_name = 'anyo'

    def write(self, cr, uid, ids, vals, context=None):
        """
        Method that handles the write event of the module
        
        :param cr: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: Affected ids
        :type ids: list, int
        :param vals: values to write 
        :type vals: dict
        :param context: OpenERP context
        :type context: dict
        :return: If the write is done
        :rtype: bool
        """

        if "macro_crec_pib" in vals.keys() and vals["macro_crec_pib"]:
            macro_crec_pib = re.sub("[^0-9|.]", "", vals["macro_crec_pib"].replace(",", "."))
            vals["macro_crec_pib"] = macro_crec_pib

        if "macro_inc_demanda_sector" in vals.keys() and vals["macro_inc_demanda_sector"]:
            macro_inc_demanda_sector = re.sub("[^0-9|.]", "", vals["macro_inc_demanda_sector"].replace(",", "."))
            vals["macro_inc_demanda_sector"] = macro_inc_demanda_sector

        if "macro_limite_sector" in vals.keys() and vals["macro_limite_sector"]:
            macro_limite_sector = re.sub("[^0-9]|.]", "", vals["macro_limite_sector"].replace(",", "."))
            vals["macro_limite_sector"] = macro_limite_sector

        if "macro_pib_prev" in vals.keys() and vals["macro_pib_prev"]:
            macro_pib_prev = re.sub("[^0-9|.]", "", vals["macro_pib_prev"].replace(",", "."))
            vals["macro_pib_prev"] = macro_pib_prev

        return super(GiscedataCnmcResumAny, self).write(cr, uid, ids, vals, context)

    _columns = {
        'anyo': fields.integer(
            'Any',
            help="Todos y cada uno de los años que componen el Periodo BBBB "
                 "anteriormente especificado. Se establecerá una declaración "
                 "para cada uno de los tres años que se recogen en el periodo "
                 "de evaluación de los Planes de Inversión."
        ),
        'pla_inversio': fields.many2one(
            'giscedata.cnmc.pla_inversio',
            'Pla de inversio',
            required=True),
        'limit_empresa': fields.float(
            'Limit de inversió per empresa (€)',
            help="Límite de inversión para la empresa distribuidora (€), para "
                 "el año N del periodo BBBB, según lo dispuesto en el Art. "
                 "16.2 del RD 1048/2013. ",
            digits=(12, 3)
        ),
        'demanda_empresa_p0': fields.float(
            "Demanda prevista el any previ (MWh)",
            help="Demanda previsto para la Empresa Distribuidora (MWh), para "
                 "el año previo al inicio del periodo BBBB.",
            digits=(8, 3),
        ),
        'inc_demanda': fields.float(
            "Increment de la demanda prevista any n (MWh)",
            help="Incremento de demanda previsto para la Empresa Distribuidora "
                 "del sistema (MWh), para el año N del periodoBBBB.",
            digits=(8, 3)
        ),
        'frri': fields.float(
            'Factor de Retard retrubit de la inversió',
            help="Factor de Retardo retributivo de la inversión, conforme a lo "
                 "establecido en el Art. 16.10.e) del RD 1048/2013 y según lo "
                 "dispuesto en la Disposición adicional segunda del "
                 "RD 1048/2013 sobre “Particularidades del primer periodo "
                 "regulatorio",
            digits=(8, 6),
        ),
        'vpi_sup': fields.selection(
            [('si', 'SI'), ('no', 'NO')],
            'VPI superat',
            help="La valoración del volumen de inversión con derecho a "
                 "retribución a cargo del sistema en el año N+2, que la "
                 "empresa prevé poner en servicio el año N del periodo BBBB, "
                 "supera el límite establecido para la empresa en dicho año "
                 "N del periodo BBBB. "
                 "• SI: supera el limite "
                 "• NO: es inferior o igual al limite ",
        ),
        'volum_total_inv': fields.float(
            'Volum total de inversió (€)',
            help="Volumen total de inversión previsto, incluyendo ayudas y "
                 "cesiones, en (€), que la empresa distribuidora prevé poner "
                 "el año N del periodo BBBB. Esta cantidad no incluirá el "
                 "Factor de Retardo Retributivo de la Inversión (FRRI) ",
            digits=(12, 3)
        ),
        'ajudes_prev': fields.float(
            'Ajudes previstess (€)',
            help="Ayudas Previstas, en €, que recibirá la empresa "
                 "distribuidora para las instalaciones recogidas dentro del "
                 "Plan de Inversión para el año N del periodo BBBB. "
                 "Esta cantidad no incluirá el Factor de Retardo Retributivo "
                 "de la Inversión (FRRI)",
            digits=(12, 3)
        ),
        'financiacio': fields.float(
            'Financiació (€)',
            help="Volumen de cesiones y financiación de terceros previstas, "
                 "en €, que recibirá la empresa distribuidora para las "
                 "instalaciones recogidas dentro del Plan de Inversión para "
                 "el año N del periodo BBBB. Esta cantidad no incluirá el "
                 "Factor de Retardo Retributivo de la Inversión (FRRI)",
            digits=(12, 3)
        ),
        "vpi_retribuible_prv": fields.float(
            "Valor total de inversio previst (€)",
            help="Valor total de inversión previsto, en €, con derecho a "
                 "retribución a cargo del sistema en el año n+2 por las "
                 "instalaciones puestas en servicio el año n. Esta cantidad "
                 "debe incluir el Factor de Retardo Retributivo de la "
                 "Inversión (FRRI) ",
            digits=(12, 3),
        ),
        'n_projectes': fields.integer(
            'Número de projectes',
            help="Número total de proyectos incluidos dentro del Plan de "
                 "Inversión de la distribuidora en el año N del periodo BBBB"
        ),
        "voltotal_inv_bt_prv": fields.float(
            "Volum total de inversio previst en BT (€)",
            help="Volumen total de inversión previsto en redes de BT, "
                 "incluyendo ayudas y cesiones, expresado en (€), que la "
                 "empresa distribuidora prevé poner el año N del "
                 "periodo BBBB.",
            digits=(12, 3)
        ),

        # Indicadors macroeconomics
        "macro_crec_pib": fields.char(
            "Variacio anual del PIB",
            help="Variación Anual del PIB en el año N del periodo BBBB. "
                 "Variación anual en porcentaje previsto por el Ministerio de "
                 "Economía y Competitividad para los ejercicios del Periodo "
                 "BBBB",
            size=23
        ),
        "macro_pib_prev": fields.char(
            "PIB Nominal (M€)",
            help="PIB Nominal (M€) para el año N del periodo BBBB. Variación "
                 "anual en porcentaje previsto por el Ministerio de Economía "
                 "y Competitividad par",
            size=23
        ),
        "macro_limite_sector": fields.char(
            "Limit total sectorial (€)",
            help="Límite de inversión total sectorial (€), para el año N del "
                 "periodo BBBB, según lo dispuesto en el Art. 16.1 del RD "
                 "1048/2013. ",
            size=23
        ),
        "macro_inc_demanda_sector": fields.char(
            "Incremento del demanda del sistema (TWh)",
            help="Incremento de demanda del sistema (TWh) conforme a lo "
                 "establecido en la Planificación del Sistema para el año N "
                 "del periodo BBBB",
            size=23
        ),
        'vpi_bt': fields.float('% VPI en BT', digits=(16, 3)),
        'inv_ccaa': fields.char('Inversió amb dret retribució per CCAA',
                                size=10),
        'inf_ccaa': fields.selection([('si', 'SI'), ('no', 'NO')],
                                     'Informe favorable de la CCAA'),
        'projectes': fields.one2many('giscedata.cnmc.projectes', 'resums_inversio',
                                     "Projectes"),
        'linies': fields.one2many('giscedata.cnmc.linies', 'resums_inversio',
                                  "Linies"),
        'liniesbt': fields.one2many('giscedata.cnmc.liniesbt', 'resums_inversio',
                                  "Linies BT"),
        'posicions': fields.one2many('giscedata.cnmc.posicions', 'resums_inversio',
                                     "Posicions"),
        'maquines': fields.one2many('giscedata.cnmc.maquines', 'resums_inversio',
                                    "Maquines"),
        'despatxos': fields.one2many('giscedata.cnmc.despatx', 'resums_inversio',
                                   "Despatxos"),
        'fiabilitats': fields.one2many('giscedata.cnmc.fiabilitat', 'resums_inversio',
                                   "Equips de millora de fiabilitat"),
        'cts': fields.one2many('giscedata.cnmc.ct', 'resums_inversio',
                                   "Cts"),
        'altres': fields.one2many('giscedata.cnmc.altres', 'resums_inversio',
                                   "Altres"),
        'resum_ccaa': fields.one2many(
            'giscedata.cnmc.resum_ccaa',
            'resums_inversio',
            "Resum per comunitat autonoma"),
    }

    _order = "anyo asc"


GiscedataCnmcResumAny()


class GiscedataCnmcResumCCAA(osv.osv):
    """
    Resums del pla de inversions per CCAA
    """

    _name = 'giscedata.cnmc.resum_ccaa'
    _description = "Resum de l'any per CCAA"
    _rec_name = 'anyo'

    _columns = {
        "codigo_ccaa": fields.many2one(
            "res.comunitat_autonoma",
            "CCAA",
            required=True,
            help="Comunidad Autónoma donde ejerce su labor la Empresa "
                 "Distribuidora. La codificación de Comunidades Autónomas es "
                 "la recogida en la Tabla 2 del apartado 5 del Anexo II de la "
                 "presente resolución. Se establecerá una declaración para "
                 "cada uno de los territorios (CCAA) en los que se realiza "
                 "la actividad de distribución y para cada uno de los tres "
                 "años que se recogen en el periodo de evaluación de los "
                 "Planes de Inversión.",
        ),
        "anio_periodo": fields.integer(
            "Any del periode",
            help="Todos y cada uno de los años que componen el Periodo BBBB "
                 "anteriormente especificado. Se establecerá una declaración "
                 "para cada uno de los tres años que se recogen en el periodo "
                 "de evaluación de los Planes de Inversión.",
            required=True),
        "vol_total_inv_prv_ccaa": fields.float(
            "Volum total de la inversio previst per CCAA (€)",
            help="Volumen total de inversión previsto, incluyendo ayudas y "
                 "cesiones, en (€), que la empresa distribuidora prevé poner "
                 "el año N del periodo BBBB en la CCAA. Esta cantidad no "
                 "incluirá el Factor de Retardo Retributivo de la "
                 "Inversión (FRRI)",
            digits=(12, 3),
        ),
        "ayudas_prv_ccaa": fields.float(
            "Ajudes previstes per CCAA (€)",
            help="Ayudas Previstas en los proyectos a desarrollar dentro de "
                 "la CCAA, en €, que recibirá la empresa distribuidora para "
                 "las instalaciones recogidas dentro del Plan de Inversión "
                 "para el año N del periodo BBBB en la CCAA Esta cantidad "
                 "no incluirá el Factor de Retardo Retributivo de la "
                 "Inversión (FRRI) ",
            digits=(12, 3),
        ),
        "financiacion_prv_ccaa": fields.float(
            "Volum de finaciacio per tercers (€)",
            help="Volumen de cesiones y financiación de terceros previstas, "
                 "en €, que recibirá la empresa distribuidora para las "
                 "instalaciones recogidas dentro del Plan de Inversión para "
                 "el año N del periodo BBBB en la CCAA Esta cantidad no "
                 "incluirá el Factor de Retardo Retributivo de "
                 "la Inversión (FRRI)",
            digits=(12, 3),
        ),
        "vpi_retribuible_prv_ccaa": fields.float(
            "Valor total d'inversio previst (€)",
            help="Valor total de inversión previsto, en €, con derecho a "
                 "retribución a cargo del sistema en el año n+2 por las "
                 "instalaciones puestas en servicio el año n en la CCAA. "
                 "Esta cantidad debe incluir el Factor de Retardo "
                 "Retributivo de la Inversión (FRRI) ",
            digits=(12, 3),
        ),
        "num_proyectos_ccaa": fields.integer(
            "Numero total de projectes inclosos en el pla",
            help="Número total de proyectos incluidos dentro del Plan de "
                 "Inversión de la distribuidora en el año N del periodo "
                 "BBBB en la CCAA"
        ),
        "vol_total_inv_bt_prv_ccaa": fields.float(
            "Volum total de inversio previst en xarxes BT (€)",
            help="Volumen total de inversión previsto en redes de BT,"
                 "incluyendo ayudas y cesiones, expresado en (€), que la "
                 "empresa distribuidora prevé poner el año N del periodo "
                 "BBBB en la CCAA",
            digits=(12, 3),
        ),
        'resums_inversio': fields.many2one(
            'giscedata.cnmc.resum_any',
            'Resums'
        ),
    }

    _order = "anio_periodo asc"


GiscedataCnmcResumCCAA()


class GiscedataCnmcProjectes(osv.osv):
    """
    Projectes del Resum de l'any
    """

    _name = 'giscedata.cnmc.projectes'
    _description = "Projectes instal·lació"
    _rec_name = 'codi'

    def duplicate(self, cr, uid, ids, context=None):
        proj = self.browse(cr, uid, ids[0], context)
        return self.copy(cr, uid, ids[0], {'codi': '%s_copia' % proj.codi})

    _columns = {
        'codi': fields.char(
            'Codi de projecte',
            help="Código Único del proyecto, que deberá mantenerse "
                 "inalterado hasta que sea finalizado en su totalidad y "
                 "las instalaciones en él contenidas sean puestas "
                 "efectivamente en servicio. Deberá ser coincidente con el "
                 "que se establezca en la relación de seguimiento para el "
                 "control de la ejecución de los planes de inversión según "
                 "lo establecido en el Art. 17 del RD 1048/2013.",
            size=22,
            required=True),
        'name': fields.char(
            'Nom del projecte',
            help="Nombre del Proyecto",
            size=60),
        'ccaa': fields.many2one(
            'res.comunitat_autonoma',
            'CCAA',
            help="Comunidad Autónoma donde se prevé ejecutar el proyecto "
                 "PY-XXXX y que debe ser coincidente con uno de los "
                 "territorios en los que se desarrolla la actividad "
                 "de distribución por parte de la Empresa Distribuidora. "
                 "En caso de que el proyecto discurra por varias "
                 "COMUNIDADES AUTÓNOMAS, caso de las líneas de AT, "
                 "este campo irá destinado a recoger el origen de la "
                 "instalación primordial del Proyecto. La codificación de "
                 "COMUNIDADES AUTÓNOMAS es la recogida en la Tabla 2 "
                 "del apartado 5 del anexo II de la presente resolución. "
        ),
        'ccaa_2': fields.many2one(
            'res.comunitat_autonoma',
            'CCAA 2',
            help="Comunidad Autónoma donde se prevé ejecutar el proyecto "
                 "PY-XXXX y que debe ser coincidente con uno de los "
                 "territorios en los que se desarrolla la actividad de "
                 "distribución por parte de la Empresa Distribuidora.En caso "
                 "de que el proyecto discurra por varias COMUNIDADES "
                 "AUTÓNOMAS, caso de las líneas de AT, este campo irá "
                 "destinado a recoger el final de la instalación primordial "
                 "del Proyecto. La codificación de COMUNIDADES AUTÓNOMAS es "
                 "la recogida en la Tabla 2 del apartado 5 del anexo "
                 "IIde la presente resolución."
        ),
        'memoria': fields.text(
            'Memoria descriptiva',
            help="Breve descripción explicativa del Proyecto",
            size=300
        ),
        "vol_total_inv_prev_proy": fields.float(
            "Volumen total de la inversio prevista (€)",
            help="Volumen total de inversión del proyecto PY-XXXX previsto, "
                 "incluyendo ayudas y cesiones, en €, que la empresa "
                 "distribuidora prevé ejecutar el año N del periodo BBBB "
                 "Esta cantidad no incluirá el Factor de Retardo Retributivo "
                 "de la Inversión (FRRI)",
            digits=(10, 3)
        ),
        "ayudas_prv_proy": fields.float(
            "Ajudes previstes del projecte (€)",
            help="Ayudas Previstas del proyecto PYXXXX del Plan de Inversión, "
                 "expresado en €, que recibirá la empresa distribuidora para "
                 "la ejecución del mismo el año N del periodo BBBB Esta "
                 "cantidad no incluirá el Factor de Retardo "
                 "Retributivo de laInversión (FRRI)",
            digits=(10, 3)
        ),
        "financiacion_prv_proy": fields.float(
            "Volum de financiacio de tercers (€)",
            help="Volumen de cesiones y financiación de terceros previstas "
                 "del proyecto PY-XXXX, en €, que recibirá la empresa "
                 "distribuidora para la ejecución del mismo el año N del "
                 "periodo BBBB Esta cantidad no incluirá el Factor de "
                 "Retardo Retributivo de la Inversión (FRRI)",
            digits=(8, 3)
        ),
        "vpi_retribuible_prv_proy": fields.float(
            "Volum total de la inversio previst del projecte (€)",
            help="Valor total de inversión previsto del proyecto PY-XXXX, en "
                 "€, con derecho a retribución a cargo del sistema en el "
                 "año n+2 por las instalaciones puestas en servicio el año "
                 "n. Esta cantidad debe incluir el Factor de Retardo "
                 "Retributivo de la Inversión (FRRI) ",
            digits=(12, 3)
        ),
        'resums_inversio': fields.many2one(
            'giscedata.cnmc.resum_any',
            'Resums',
            ondelete='cascade'
        ),
        "estado": fields.selection(
            ESTADO,
            "Estat",
            help="0: Elemento sin modificaciones. "
                 "1: Elemento con modificaciones. "
                 "2: Alta elemento nuevo. ",
            size=1
        ),

        # 'comentaris': fields.text('Comentaris', size=300),
    }

    _sql_constraints = [
        ('codi', 'unique (codi)',
         _('Ja existeix un projecte amb aquest codi.'))
    ]

    _order = "codi asc"

GiscedataCnmcProjectes()


class GiscedataCnmcLiniesBT(osv.osv):
    """
    LBT de l'any
    """

    _name = 'giscedata.cnmc.liniesbt'
    _description = "LBT planificades"
    _columns = {
        "cod_proyecto": fields.many2one(
            'giscedata.cnmc.projectes',
            'Codi de projecte',
            help="Código Único del proyecto al que pertenece la instalación, "
                 "que deberá mantenerse inalterado hasta que sea finalizado "
                 "en su totalidad y las instalaciones en él contenidas sean "
                 "puestas efectivamente en servicio. Deberá ser coincidente "
                 "con el que se establezca en la relación de seguimiento "
                 "para el control de la ejecución de los planes de inversión "
                 "según lo establecido en el Art. 17 del RD 1048/2013. ",
            required=True,
        ),
        'finalidad': fields.selection(
            FINALITAT,
            'Finalitat',
            help="Código de tipo de Finalidad. Tabla 4."
        ),
        "identificador_py": fields.char(
            "Identificador unic de la instalacio projectada",
            help="Identificador Único de la Instalación proyectada,que "
                 "deberá mantenerse inalterado hasta que sea puesta "
                 "efectivamente en servicio. Deberá ser coincidente con el "
                 "que se establezca en la relación de seguimiento para el "
                 "control de la ejecución de los planes de inversión según "
                 "lo establecido en el Art. 17 del RD 1048/2013. ",
            size=22
        ),
        "cini_prv": fields.char(
            "CINI",
            help="Códigos de Identificación Normalizada de instalaciones. "
                 "Tabla 5 del apartado 5 del anexo II de la "
                 "presente resolución.",
            size=8),
        "cod_tipo_inst": fields.many2one(
            "giscedata.tipus.installacio",
            "TI",
            help="Código denominativo de la tipología de la instalación según "
                 "Tabla 3 del apartado 5 del anexo II de la presente "
                 "resolución."
        ),
        "codigo_ccaa_1": fields.many2one(
            "res.comunitat_autonoma",
            "CCAA 1",
            help="Comunidad Autónoma por la que discurrirá la línea. En caso "
                 "de que discurra por varias Comunidades Autónomas, este "
                 "campo irá destinado a recoger la de origen. La "
                 "codificación de Comunidades Autónomas es la recogida en "
                 "la Tabla 2 del apartado 5 del anexo II de lapresente "
                 "resolución."
        ),
        "codigo_ccaa_2": fields.many2one(
            "res.comunitat_autonoma",
            "CCAA 2",
            help="Comunidad Autónoma por la que discurrirá la línea. En caso "
                 "de que discurra por varias Comunidades Autónomas, este "
                 "campo irá destinado a recoger la de origen. La codificación "
                 "de Comunidades Autónomas es la recogida en la Tabla 2 del "
                 "apartado 5 del anexo II de la presente resolución."
        ),
        "anio_prev_aps": fields.integer(
            "Any previst APS",
            help="Año previsto para la Puesta en servicio de instalación.",
            size=4
        ),
        "longitud_prv": fields.float(
            "Longitud total de la linea (Km)",
            help="Longitud Total de la línea en Km.",
            digits=(6, 3)),
        "capacidad_prv": fields.integer(
            "Capacitat prevista (kVA)",
            help="kVA totales de la línea.",
            size=6
        ),
        "vol_inv_prev": fields.float(
            "Volumen de la inversion prevista (€)",
            help="Volumen total de inversión prevista para instalación "
                 "proyectada, incluyendo ayudas y cesiones, en €, que la "
                 "empresa distribuidora prevé ejecutar el año N del "
                 "periodo BBBB",
            digits=(10, 3)
        ),
        "ayudas_prv": fields.float(
            "Ajudes previstes (€)",
            help="Ayudas Previstas imputables a la instalación proyectada, "
                 "expresadas en €, que la empresa distribuidora prevé "
                 "ejecutar el año N del periodo BBBB",
            digits=(10, 3)
        ),
        "financiacion_prv": fields.float(
            "Financiacio prevista (€)",
            help="Volumen de cesiones y financiación de terceros previstas "
                 "para instalación proyectada, en €, que la empresa "
                 "distribuidora prevé ejecutar el año N del periodo BBBB",
            digits=(8, 3)
        ),
        "vpi_retribuible_prv": fields.float(
            "Valor previst de inversio retribuible (€)",
            help="Valor total de inversión previsto para la instalación "
                 "proyectada, en €, con derecho a retribución a cargo del "
                 "sistema en el año n+2 por las instalaciones puestas en "
                 "servicio el año n. Esta cantidad debe incluir el Factor "
                 "de Retardo Retributivo de la Inversión (FRRI)",
            digits=(10, 3)
        ),
        'resums_inversio': fields.many2one(
            'giscedata.cnmc.resum_any',
            'Resums'
        ),
        "estado": fields.selection(
            ESTADO,
            "Estat",
            help="0: Elemento sin modificaciones. "
                 "1: Elemento con modificaciones. "
                 "2: Alta elemento nuevo. ",
            size=1,
        )
    }


GiscedataCnmcLiniesBT()


class GiscedataCnmcLinies(osv.osv):
    """
    LAT de l'any
    """

    _name = 'giscedata.cnmc.linies'
    _description = "LAT planificades"
    _rec_name = 'codi'

    def duplicate(self, cr, uid, ids, context=None):
        linia = self.browse(cr, uid, ids[0], context)
        return self.copy(cr, uid, ids[0], {'codi': '%s_copia' % linia.codi})

    _columns = {

        'codi': fields.many2one(
            'giscedata.cnmc.projectes',
            'Codi de projecte',
            help="Código Único del proyecto al que pertenece la instalación, "
                 "que deberá mantenerse inalterado hasta que sea finalizado "
                 "en su totalidad y las instalaciones en él contenidas sean "
                 "puestas efectivamente en servicio. Deberá ser coincidente "
                 "con el que se establezca en la relación de seguimiento "
                 "para el control de la ejecución de los planes de "
                 "inversión según lo establecido en el Art. "
                 "17 del RD 1048/2013.",
            required=True,
        ),
        'finalitat': fields.selection(
            FINALITAT,
            'Finalitat',
            help="Código de tipo de Finalidad. Tabla 4."
        ),
        'id_instalacio': fields.char(
            "Identificació únic d'instal·lació",
            help="Identificador Único de la Instalación proyectada, que "
                 "deberá mantenerse inalterado hasta que sea puesta "
                 "efectivamente en servicio. Deberá ser coincidente con "
                 "el que se establezca en la relación de seguimiento para "
                 "el control de la ejecución de los planes de inversión "
                 "según lo establecido en el Art. 17 del RD 1048/2013.",
            size=22
        ),
        'cini': fields.char(
            'CINI',
            help="Códigos de Identificación Normalizada de instalaciones. "
                 "Tabla 5 del apartado 5 del anexo II de "
                 "la presente resolución.",
            size=8
        ),
        'codi_tipus_inst': fields.many2one(
            "giscedata.tipus.installacio",
            "TI",
            help="Código denominativo de la tipología de la instalación según "
                 "Tabla 3 del apartado 5 del anexo II de "
                 "la presente resolución."
        ),
        'origen': fields.char('Origen', size=50),
        'desti': fields.char('Destí', size=50),
        'inv_financiada': fields.float(
            'Finançat per tercers (€)',
            help="Volumen de cesiones y financiación de terceros previstas "
                 "para instalación proyectada, en €, que la empresa "
                 "distribuidora prevé ejecutar el año N del periodo BBBB",
            digits=(8, 3)
        ),
        'ajudes': fields.float(
            'Ajudes previstes (€)',
            help="Volumen total de inversión prevista para instalación "
                 "proyectada, incluyendo ayudas y cesiones, en €, que la "
                 "empresa distribuidora prevé ejecutar el año N "
                 "del periodo BBBB",
            digits=(10, 3)
        ),
        'vol_total_inv': fields.float(
            'Volum total inversio prevista (€)',
            help="Volumen total de inversión prevista para instalación "
                 "proyectada, incluyendo ayudas y cesiones, en €, que la "
                 "empresa distribuidora prevé ejecutar el año N "
                 "del periodo BBBB",
            digits=(10, 3)
        ),
        'vpi_retri': fields.float(
            'Valor previst de inversió retribuible (€)',
            help="Valor total de inversión previsto para la instalación "
                 "proyectada, en €, con derecho a retribución a cargo del "
                 "sistema en el año n+2 por las instalaciones puestas en "
                 "servicio el año n. Esta cantidad debe incluir el "
                 "Factor de Retardo Retributivo de la Inversión (FRRI)",
            digits=(10, 3)
        ),
        'any_apm': fields.integer(
            'Any previst acta posada en marxa',
            help="Año previsto para la Puesta en servicio de instalación "
                 "proyectada.",
            size=4
        ),
        'long_total': fields.float(
            'Longitud total prevista (Km)',
            help="Longitud Total de la línea en Km.",
            digits=(8, 3)
        ),
        "capacidad_prv": fields.integer(
            "Capacitat prevista (MVA)",
            help="MVA totales de la línea.",
            size=6,
        ),
        'ccaa': fields.many2one(
            "res.comunitat_autonoma",
            "CCAA 1",
            help="Comunidad Autónoma por la que discurrirá la línea. En caso "
                 "de que discurra por varias COMUNIDADES AUTÓNOMAS, este "
                 "campo irá destinado a recoger la de origen. La codificación "
                 "de COMUNIDADES AUTÓNOMAS es la recogida en la Tabla 2 del "
                 "apartado 5 del anexo II de la presente resolución."
        ),
        "ccaa_2": fields.many2one(
            "res.comunitat_autonoma",
            "CCAA 2",
            help="Comunidad Autónoma por la que discurrirá la línea. En caso "
                 "de que discurra por varias COMUNIDADES AUTÓNOMAS, este "
                 "campo irá destinado a recoger la de origen. La "
                 "codificación de COMUNIDADES AUTÓNOMAS es la recogida en la "
                 "Tabla 2 del apartado 5 del anexo II de la "
                 "presente resolución. "
        ),
        'resums_inversio': fields.many2one('giscedata.cnmc.resum_any',
                                           'Resums'),
        'comentaris': fields.text('Comentaris', size=300),
        "estado": fields.selection(
            ESTADO,
            "Estat",
            help="0: Elemento sin modificaciones. "
                 "1: Elemento con modificaciones. "
                 "2: Alta elemento nuevo. ",
            size=1)
    }

    _sql_constraints = [
        ('id_instalacio', 'unique (id_instalacio)',
         _('Ja existeix una linia amb aquest identificador.'))
    ]

    _order = "codi asc"


GiscedataCnmcLinies()


class GiscedataCnmcPosicions(osv.osv):
    """
    Posicions de l'any
    """

    _name = 'giscedata.cnmc.posicions'
    _description = "Posicions planificades"
    _rec_name = 'codi'

    def duplicate(self, cr, uid, ids, context=None):
        posicio = self.browse(cr, uid, ids[0], context)
        return self.copy(cr, uid, ids[0], {'codi': '%s_copia' % posicio.codi})

    _columns = {
        'codi': fields.many2one(
            'giscedata.cnmc.projectes',
            'Codi de projecte',
            help="Código Único del proyecto al que pertenece la instalación, "
                 "que deberá mantenerse inalterado hasta que sea finalizado "
                 "en su totalidad y las instalaciones en él contenidas "
                 "sean puestas efectivamente en servicio.Deberá "
                 "ser coincidente con el que se establezca en la relación "
                 "de seguimiento para el control de la ejecución de los "
                 "planes de inversión según lo establecido en el "
                 "Art. 17 del RD 1048/2013.",
            required=True,
        ),
        'finalitat': fields.selection(
            FINALITAT,
            'Finalitat',
            help="Código de tipo de Finalidad. Tabla 4"
        ),
        'id_instalacio': fields.char(
            "Identificació únic d'instal·lació",
            help="Identificador Único de la Instalación proyectada,que "
                 "deberá mantenerse inalterado hasta que sea puesta "
                 "efectivamente en servicio. Deberá ser coincidente con el "
                 "que se establezca en la relación de seguimiento para el "
                 "control de la ejecución de los planes de inversión según "
                 "lo establecido en el Art. 17 del RD 1048/2013.",
            size=22
        ),
        'cini': fields.char(
            'CINI',
            help="Códigos de Identificación Normalizada de instalaciones. "
                 "Tabla 5 del apartado 5 del anexo II de la presente "
                 "resolución.",
            size=8),
        'codi_tipus_inst': fields.many2one(
            "giscedata.tipus.installacio",
            "TI",
            help="Código denominativo de la tipología de la instalación "
                 "según Tabla 3 del apartado 5 del Anexo II de la "
                 "presente resolución."
        ),
        'inv_financiada': fields.float(
            'Finançat per tercers (€)',
            help="Volumen de cesiones y financiación de terceros previstas "
                 "para instalación proyectada, en €, que la empresa "
                 "distribuidora prevé ejecutar el año N del periodo BBBB",
            digits=(8, 3)
        ),
        'ajudes': fields.float(
            'Ajudes previstes (€)',
            help="Ayudas Previstas imputables a la instalación proyectada, "
                 "expresadas en €, que la empresa distribuidora prevé "
                 "ejecutar el año N del periodo BBBB",
            digits=(8, 3)
        ),
        'vol_total_inv': fields.float(
            'Import total previst (€)',
            help="Volumen total de inversión prevista para instalación "
                 "proyectada,incluyendo ayudas y cesiones, en €, que "
                 "la empresa distribuidora prevé ejecutar el año "
                 "N del periodo BBBB ",
            digits=(10, 3)
        ),
        'vpi_retri': fields.float(
            'Valor previst de inversió retribuible (€)',
            help="Valor total de inversión previsto para la instalación "
                 "proyectada, en €, con derecho a retribución a cargo "
                 "del sistema en el año n+2 por las instalaciones "
                 "puestas en servicio el año n. Esta cantidad debe "
                 "incluir el Factor de Retardo Retributivo de "
                 "la Inversión (FRRI)",
            digits=(10, 3)
        ),
        'any_apm': fields.integer(
            'Any acta posada en marxa',
            help="Año previsto para la Puesta en servicio de "
                 "instalación proyectada.",
            size=4
        ),
        'ccaa': fields.many2one(
            'res.comunitat_autonoma',
            'CCAA',
            help="Comunidad Autónoma donde se prevé que se ubique la "
                 "subestación La codificación de COMUNIDADES AUTÓNOMAS es la "
                 "recogida en la Tabla 2 del apartado 5 del Anexo II de la "
                 "presente resolución."
        ),
        'municipi': fields.many2one('res.municipi', 'Municipi'),
        'resums_inversio': fields.many2one('giscedata.cnmc.resum_any',
                                           'Resums'),
        "estado": fields.selection(
            ESTADO,
            'Estat',
            help="0: Elemento sin modificaciones. "
                 "1: Elemento con modificaciones. "
                 "2: Alta elemento nuevo",
            size=1
        ),
    }

    _sql_constraints = [
        ('id_instalacio', 'unique (id_instalacio)',
         _('Ja existeix una posició amb aquest identificador.'))
    ]

    _order = "codi asc"


GiscedataCnmcPosicions()


class GiscedataCnmcMaquines(osv.osv):
    """
    Maquines de l'any
    """

    _name = 'giscedata.cnmc.maquines'
    _description = "Màquines planificades"
    _rec_name = 'codi'

    def duplicate(self, cr, uid, ids, context=None):
        maq = self.browse(cr, uid, ids[0], context)
        return self.copy(cr, uid, ids[0], {'codi': '%s_copia' % maq.codi})

    _columns = {
        'codi': fields.many2one(
            'giscedata.cnmc.projectes',
            'Codi de projecte',
            help="Código Único del proyecto al que pertenece la "
                 "instalación, que deberá mantenerse inalterado hasta que "
                 "sea finalizado en su totalidad y las instalaciones "
                 "en él contenidas sean puestas efectivamente en servicio. "
                 "Deberá ser coincidente con el que se establezca en la "
                 "relación de seguimiento para el control de laejecución de "
                 "los planes de inversión según lo establecido en "
                 "el Art. 17 del RD 1048/2013.",
            required=True,
        ),
        'finalitat': fields.selection(
            FINALITAT,
            'Finalitat',
            help="Código de tipo de Finalidad. Tabla 4."
        ),
        'id_instalacio': fields.char(
            "Identificació únic d'instal·lació",
            help="Identificador Único de la Instalación proyectada, que "
                 "deberá mantenerse inalterado hasta que sea puesta "
                 "efectivamente en servicio. Deberá ser coincidente "
                 "con el que se establezca en la relación de seguimiento "
                 "para el control de la ejecución de los planes de inversión "
                 "según lo establecido en el Art. 17 del RD 1048/2013.",
            size=22
        ),
        'denominacio': fields.char('Denominacio', size=100),
        'cini': fields.char(
            'CINI',
            help="Códigos de Identificación Normalizada de instalaciones. "
                 "Tabla 5 del apartado 5 del anexo II de la "
                 "presente resolución.",
            size=8),
        'codi_tipus_inst': fields.many2one(
            "giscedata.tipus.installacio",
            "TI",
            help="Código denominativo de la tipología de la instalación "
                 "según Tabla 3 del apartado 5 del anexo II de la "
                 "presente resolución.",
        ),
        'inv_financiada': fields.float(
            "Finançat per tercers (€)",
            help="Volumen de cesiones y financiación de terceros "
                 "previstas para instalación proyectada, en €, que "
                 "la empresa distribuidora prevé ejecutar el año "
                 "N del periodo BBBB",
            digits=(10, 3)
        ),
        'ajudes': fields.float(
            "Ajudes previstes (€)",
            help="Ayudas Previstas imputables a la instalación proyectada, "
                 "expresadas en €, que la empresa distribuidora prevé "
                 "ejecutar el año N del periodo BBBB",
            digits=(10, 3)
        ),
        'vol_total_inv': fields.float(
            'Volum total de inversio prevista (€)',
            help="Volumen total de inversión prevista para instalación "
                 "proyectada, incluyendo ayudas y cesiones, en €, que la "
                 "empresa distribuidora prevé ejecutar el año "
                 "N del periodo BBBB",
            digits=(10, 3)
        ),
        'vpi_retri': fields.float(
            'Valor previst de inversió retribuible (€)',
            help="Valor total de inversión previsto para la "
                 "instalación proyectada, en €, con derecho a retribución a "
                 "cargo del sistema en el año n+2 por las "
                 "instalaciones puestas en servicio el año n. Esta "
                 "cantidad debe incluir el Factor de Retardo Retributivo "
                 "de la Inversión (FRRI) ",
            digits=(10, 3)
        ),
        'any_apm': fields.integer(
            'Any previst acta posada en marxa',
            help="Año previsto para la Puesta en servicio de "
                 "instalación proyectada",
            size=4
        ),
        'capacidad_prv': fields.integer(
            "Capacitat prevista (MVA)",
            size=3
        ),
        'pot_inst_prev': fields.float(
            'Capacitat prevista (MVA)',
            digits=(10, 3)
        ),
        'ccaa': fields.many2one(
            'res.comunitat_autonoma',
            'CCAA',
            help="Comunidad Autónoma donde se prevé que se ubique la "
                 "maquina proyectada La codificación de COMUNIDADES "
                 "AUTÓNOMAS es la recogida en la Tabla 2 del apartado 5 "
                 "del anexo II de la presente resolución."
        ),
        'municipi': fields.many2one('res.municipi', 'Municipi'),
        'resums_inversio': fields.many2one('giscedata.cnmc.resum_any',
                                           'Resums'),
        'comentaris': fields.text('Comentaris', size=300),
        "estado": fields.selection(
            ESTADO,
            "Estat",
            help="0: Elemento sin modificaciones."
                 "1: Elemento con modificaciones."
                 "2: Alta elemento nuevo",
            size=1)
    }

    _sql_constraints = [
        ('id_instalacio', 'unique (id_instalacio)',
         _('Ja existeix una màquina amb aquest identificador.'))
    ]

    _order = "codi asc"


GiscedataCnmcMaquines()


class GiscedataCnmcDespatx(osv.osv):
    """
    Despatx de l'any
    """

    _name = 'giscedata.cnmc.despatx'
    _description = "Despatxos"
    _rec_name = 'codi'

    def duplicate(self, cr, uid, ids, context=None):
        despatx = self.browse(cr, uid, ids[0], context)
        return self.copy(cr, uid, ids[0], {'codi': '%s_copia' % despatx.codi})

    _columns = {
        'codi': fields.many2one(
            'giscedata.cnmc.projectes',
            'Codi de projecte',
            help="Código Único del proyecto al que pertenece la instalación, "
                 "que deberá mantenerse inalterado hasta que sea finalizado "
                 "en su totalidad y las instalaciones en él contenidas "
                 "sean puestas efectivamente en servicio. Deberá ser "
                 "coincidente con el que se establezca en la relación "
                 "deseguimiento para el control de la ejecución de los "
                 "planes de inversión según lo establecido en el "
                 "Art. 17 del RD 1048/2013.",
            required=True,
        ),
        'finalitat': fields.selection(
            FINALITAT,
            'Finalitat',
            help="Código de tipo de Finalidad. Tabla 4"
        ),
        'id_instalacio': fields.char(
            "Identificació únic d'instal·lació",
            help="Identificador Único de la Instalación proyectada, "
                 "que deberá mantenerse inalterado hasta que sea "
                 "puesta efectivamente en servicio. Deberá ser "
                 "coincidente con el que se establezca en la relación "
                 "de seguimiento para el control de la ejecución de los "
                 "planes de inversión según lo establecido en "
                 "el Art. 17 del RD 1048/2013.",
            size=22
        ),
        'cini': fields.char(
            'CINI',
            help="Códigos de Identificación Normalizada de instalaciones."
                 "Tabla 5 del apartado 5 del anexo II de la presente "
                 "resolución.",
            size=8),
        "codigo_ccaa": fields.many2one(
            "res.comunitat_autonoma",
            "CCAA",
            help="Comunidad Autónoma donde se prevé que se ubique el "
                 "despacho. La codificación de COMUNIDADES AUTÓNOMAS es "
                 "la recogida en la Tabla 2 del apartado 5 del "
                 "anexo II de la presente resolución.",
        ),
        'inv_financiada': fields.float(
            "Finançat per tercers (€)",
            digits=(16, 3)),
        'ajudes': fields.float(
            "Ajudes previstes (€)",
            help="Ayudas Previstas imputables a la instalación proyectada, "
                 "expresadas en €, que la empresa distribuidora prevé "
                 "ejecutar el año N del periodo BBBB",
            digits=(16, 3)
        ),
        'vol_total_inv': fields.float(
            'Volum total de inversio previst (€)',
            help="Volumen total de inversión prevista para instalación "
                 "proyectada,incluyendo ayudas y cesiones, en €, que la "
                 "empresa distribuidora prevé ejecutar el año N del "
                 "periodo BBBB",
            digits=(10, 3)
        ),
        'vpi_retri': fields.float(
            'Valor previst de inversió retribuible (€)',
            help="Valor total de inversión previsto para la instalación "
                 "proyectada, en €, con derecho a retribución a cargo "
                 "del sistema en el año n+2 por las instalaciones puestas "
                 "en servicio el año n. Esta cantidad debe incluir el "
                 "Factor de Retardo Retributivo de la Inversión (FRRI)",
            digits=(10, 3),
        ),
        'any_apm': fields.integer(
            'Any previst acta posada en marxa',
            help="Año previsto para la Puesta en servicio de instalación "
                 "proyectada.",
            size=4
        ),
        'resums_inversio': fields.many2one('giscedata.cnmc.resum_any',
                                           'Resums'),
        'comentaris': fields.text('Comentaris', size=300),
        "estado": fields.selection(
            ESTADO,
            "Estat",
            help="0: Elemento sin modificaciones."
                 "1: Elemento con modificaciones."
                 "2: Alta elemento nuevo.",
            size=1)
    }

    _sql_constraints = [
        ('id_instalacio', 'unique (id_instalacio)',
         _('Ja existeix un despatx amb aquest identificador.'))
    ]

    _order = "codi asc"


GiscedataCnmcDespatx()


class GiscedataCnmcFiabilitat(osv.osv):
    """
    Fiabilitat de l'any
    """

    _name = 'giscedata.cnmc.fiabilitat'
    _description = "Equips de millora de la fiabilitat"
    _rec_name = 'codi'

    def duplicate(self, cr, uid, ids, context=None):
        fiab = self.browse(cr, uid, ids[0], context)
        return self.copy(cr, uid, ids[0], {'codi': '%s_copia' % fiab.codi})

    _columns = {
        'codi': fields.many2one(
            'giscedata.cnmc.projectes',
            'Codi de projecte',
            help="Código Único del proyecto al que pertenece la instalación, "
                 "que deberá mantenerse inalterado hasta que sea finalizado "
                 "en su totalidad y las instalaciones en él contenidas sean "
                 "puestas efectivamente en servicio. Deberá ser coincidente "
                 "con el que se establezca en la relación de seguimiento "
                 "para el control de la ejecución de los planes de "
                 "inversión según lo establecido en el Art. 17 del "
                 "RD 1048/2013.",
            required=True,
        ),
        'finalitat': fields.selection(
            FINALITAT,
            'Finalitat',
            help="Código de tipo de Finalidad. Tabla 4"
        ),
        'id_instalacio': fields.char(
            "Identificació únic d'instal·lació",
            help="Identificador Único de la Instalación proyectada, que "
                 "deberá mantenerse inalterado hasta que sea puesta "
                 "efectivamente en servicio. Deberá ser coincidente "
                 "con el que se establezca en la relación de seguimiento "
                 "para el control de la ejecución de los planes de "
                 "inversión según lo establecido en el "
                 "Art. 17 del RD 1048/2013. ",
            size=22
        ),
        'cini': fields.char(
            'CINI',
            help="Códigos de Identificación Normalizada de instalaciones."
                 "Tabla 5 del apartado 5 del anexo II de la presente"
                 "resolución.",
            size=8
        ),
        'codi_tipus_inst': fields.many2one(
            "giscedata.tipus.installacio",
            "TI",
            help="Código denominativo de la tipología de la instalación "
                 "según Tabla 3 del apartado 5 del anexo II de "
                 "la presente resolución. "
        ),
        'inv_financiada': fields.float(
            "Finançat per tercers (€)",
            help="Volumen de cesiones y financiación de terceros "
                 "previstas para instalación proyectada, en €, que la "
                 "empresa distribuidora prevé ejecutar el año "
                 "N del periodo BBBB ",
            digits=(8, 3)
        ),
        'ajudes': fields.float(
            "Ajudes previstes (€)",
            help="Ayudas Previstas imputables a la instalación proyectada, "
                 "expresadas en €, que la empresa distribuidora prevé ejecutar "
                 "el año N del periodo BBBB",
            digits=(10, 3)
        ),
        'vol_total_inv': fields.float(
            'Import total previst (€)',
            help="Volumen total de inversión prevista para instalación "
                 "proyectada, incluyendo ayudas y cesiones, en €, que "
                 "la empresa distribuidora prevé ejecutar el año N "
                 "del periodo BBBB",
            digits=(10, 3)
        ),
        'vpi_retri': fields.float(
            'Valor previst de inversió retribuible (€)',
            help="Valor total de inversión previsto para la instalación "
                 "proyectada, en €, con derecho a retribución a cargo "
                 "del sistema en el año n+2 por las instalaciones "
                 "puestas en servicio el año n. Esta cantidad "
                 "debe incluir el Factor de Retardo Retributivo",
            digits=(10, 3)
        ),
        'any_apm': fields.integer(
            'Any acta posada en marxa',
            help="Año previsto para la Puesta en servicio de "
                 "instalación proyectada.",
            size=4
        ),
        'pot_inst_prev': fields.float('Potencia instalada prevista',
                                      digits=(10, 3)),
        'ccaa': fields.many2one(
            'res.comunitat_autonoma',
            'CCAA',
            help="Comunidad Autónoma donde se prevé que se ubique la "
                 "maquina proyectada La codificación de COMUNIDADES "
                 "AUTÓNOMAS es la recogida en la Tabla 2 del apartado "
                 "5 del anexo II de la presente resolución."
        ),
        'municipi': fields.many2one('res.municipi', 'Municipi'),
        'resums_inversio': fields.many2one('giscedata.cnmc.resum_any',
                                           'Resums'),
        'comentaris': fields.text('Comentaris', size=300),
        "estado": fields.selection(
            ESTADO,
            "Estat",
            help="0: Elemento sin modificaciones."
                 "1: Elemento con modificaciones."
                 "2: Alta elemento nuevo.",
            size=1
        )
    }

    _sql_constraints = [
        ('id_instalacio', 'unique (id_instalacio)',
         _('Ja existeix un equip de fiabilitat amb aquest identificador.'))
    ]

    _order = "codi asc"


GiscedataCnmcFiabilitat()


class GiscedataCnmcCt(osv.osv):
    """
    CTs de l'any
    """

    _name = 'giscedata.cnmc.ct'
    _description = "Centres de transformacio"
    _rec_name = 'codi'

    def duplicate(self, cr, uid, ids, context=None):
        ct = self.browse(cr, uid, ids[0], context)
        return self.copy(cr, uid, ids[0], {'codi': '%s_copia' % ct.codi})

    _columns = {
        'codi': fields.many2one(
            'giscedata.cnmc.projectes',
            'Codi de projecte',
            help="Código Único del proyecto al que pertenece la "
                 "instalación, que deberá mantenerse inalterado hasta "
                 "que sea finalizado en su totalidad y las instalaciones "
                 "en él contenidas sean puestas efectivamente en servicio."
                 "Deberá ser coincidente con el que se establezca en la "
                 "relación de seguimiento para el control de la ejecución "
                 "de los planes de inversión según lo establecido en el "
                 "Art. 17 del RD 1048/2013.",
            required=True,
        ),
        'finalitat': fields.selection(
            FINALITAT,
            'Finalitat',
            help="Código de tipo de Finalidad. Tabla 4."

        ),
        'id_instalacio': fields.char(
            "Identificació únic d'instal·lació",
            help="Identificador Único de la Instalación proyectada,que "
                 "deberá mantenerse inalterado hasta que sea puesta "
                 "efectivamente en servicio. Deberá ser coincidente "
                 "con el que se establezca en la relación de seguimiento "
                 "para el control de la ejecución de los planes de "
                 "inversión según lo establecido en el "
                 "Art. 17 del RD 1048/2013.",
            size=22
        ),
        'cini': fields.char(
            'CINI',
            help="Códigos de Identificación Normalizada de instalaciones."
                 "Tabla 5 del apartado 5 del anexo II de la presente "
                 "resolución.",
            size=8),
        'codi_tipus_inst': fields.many2one(
            "giscedata.tipus.installacio",
            "TI",
            help="Código denominativo de la tipología de la instalación "
                 "según Tabla 3 del apartado 5 del anexo II de la presente "
                 "resolución."
        ),
        'inv_financiada': fields.float(
            "Finançat per tercers (€)",
            help="Volumen de cesiones y financiación de terceros previstas "
                 "para instalación proyectada, en €, que "
                 "la empresa distribuidora prevé ejecutar "
                 "el año N del periodo BBBB",
            digits=(8, 3)
        ),
        'ajudes': fields.float(
            "Ajudes previstes (€)",
            help="Ayudas Previstas imputables a la instalación proyectada, "
                 "expresadas en €, que la empresa distribuidora prevé "
                 "ejecutar el año N del periodo BBBB",
            digits=(10, 3)
        ),
        'vol_total_inv': fields.float(
            "Import total previst (€)",
            help="Volumen total de inversión prevista para instalación "
                 "proyectada, incluyendo ayudas y cesiones, en "
                 "€, que la empresa distribuidora prevé "
                 "ejecutar el año N del periodo BBBB",
            digits=(10, 3)
        ),
        'vpi_retri': fields.float(
            'Valor previst de inversió retribuible (€)',
            help="Valor total de inversión previsto para la instalación "
                 "proyectada, en €, con derecho a "
                 "retribución a cargo del sistema en el año n+2 por "
                 "las instalaciones puestas en servicio el año n."
                 "Esta cantidad debe incluir el "
                 "Factor de Retardo Retributivo de la Inversión (FRRI) ",
            digits=(10, 3)
        ),
        'any_apm': fields.integer(
            'Any acta posada en marxa',
            help="Año previsto para la Puesta en servicio de instalación "
                 "proyectada.",
            size=4
        ),
        'pot_inst_prev': fields.float('Potencia instalada prevista',
                                      digits=(10, 3)),
        'ccaa': fields.many2one(
            'res.comunitat_autonoma',
            'CCAA',
            help="Comunidad Autónoma donde se prevé que se ubique el CT "
                 "La codificación de COMUNIDADES AUTÓNOMAS es la recogida "
                 "en la Tabla 2 del apartado 5 del anexo II de la "
                 "presente resolución."
        ),
        'municipi': fields.many2one('res.municipi', 'Municipi'),
        'resums_inversio': fields.many2one('giscedata.cnmc.resum_any',
                                           'Resums'),
        "estado": fields.selection(
            ESTADO,
            'Estado',
            help="0: Elemento sin modificaciones."
                 "1: Elemento con modificaciones."
                 "2: Alta elemento nuevo.",
            size=1),
    }

    _sql_constraints = [
        ('id_instalacio', 'unique (id_instalacio)',
         _('Ja existeix un ct amb aquest identificador.'))
    ]

    _order = "codi asc"


GiscedataCnmcCt()


class GiscedataCnmcAltres(osv.osv):
    """
    Altres de l'any
    """

    _name = 'giscedata.cnmc.altres'
    _description = "Altres instal·lacions"
    _rec_name = 'codi'

    def duplicate(self, cr, uid, ids, context=None):
        altre = self.browse(cr, uid, ids[0], context)
        return self.copy(cr, uid, ids[0], {'codi': '%s_copia' % altre.codi})

    _columns = {
        'codi': fields.many2one(
            'giscedata.cnmc.projectes',
            'Codi de projecte',
            help="Código Único del proyecto al que pertenece el activo "
                 "necesario para el ejercicio de la actividad de "
                 "distribución distinto de los activos eléctricos "
                 "recogidos en las unidades físicas. Este código único "
                 "deberá mantenerse inalterado hasta que sea finalizado "
                 "en su totalidad y las instalaciones en él contenidas "
                 "sean puestas efectivamente en servicio. Deberá ser "
                 "coincidente con el que se establezca en la "
                 "relación de seguimiento para el control de la ejecución "
                 "de los planes de inversión según lo establecido en el "
                 "Art. 17 del RD 1048/2013.",
            required=True,
        ),
        'finalitat': fields.selection(
            FINALITAT,
            'Finalitat',
            help="Código de tipo de Finalidad. Tabla 4."
        ),
        "identificador_py": fields.char(
            "Identificador unic de la instal·lacio projectada",
            help="Identificador Único del activo necesario para el ejercicio "
                 "de la actividad de distribución distintos de los activos "
                 "eléctricos recogidos en las unidades físicas.Dicho "
                 "Identificador Único deberá mantenerse inalterado hasta que "
                 "sea puesta efectivamente en servicio. Deberá ser "
                 "coincidente con el que se establezca en la relación de "
                 "seguimiento para el control de la ejecución de los "
                 "planes de inversión según lo establecido en el Art. "
                 "17 del RD 1048/2013.",
            size=22
        ),
        'cini': fields.char(
            'CINI',
            help="Códigos de Identificación Normalizada de instalaciones."
                 "Tabla 5 del apartado 5 del anexo II de la "
                 "presente resolución. ",
            size=8),
        'codigo_ccaa': fields.many2one(
            "res.comunitat_autonoma",
            "CCAA",
            help="Comunidad Autónoma donde se prevé que se ubique el activo."
                 "La codificación de Comunidades Autónomas es la recogida en "
                 "la Tabla 2 del apartado 5 del anexo II de la "
                 "presente resolución. ",
        ),
        'any_apm': fields.integer(
            "Any acta posada en marxa",
            help="Año previsto para de la entrada en servicio del "
                 "activo proyectado",
            size=4
        ),
        'inv_financiada': fields.float(
            "Finançat per tercers (€)",
            help="Volumen de cesiones y financiación de terceros previstas "
                 "para el activo proyectado, en €, que la empresa "
                 "distribuidora prevé ejecutar el año N del periodo BBBB",
            digits=(8, 3)
        ),
        'ajudes': fields.float(
            "Ajudes previstes (€)",
            help="Ayudas Previstas imputables al activo proyectado,"
                 "expresadas en €, que la empresa distribuidora "
                 "prevé ejecutar el año N del periodo BBBB",
            digits=(10, 3)
        ),
        'vol_total_inv': fields.float(
            "Import total previst (€)",
            help="Volumen total de inversión prevista para el activo "
                 "proyectado, incluyendo ayudas y cesiones, en €, que "
                 "la empresa distribuidora prevé ejecutar el año N "
                 "del periodo BBBB ",
            digits=(10, 3)
        ),
        'vpi_retri': fields.float(
            "Valor previst de inversió retribuible (€)",
            help="Valor total de inversión previsto para la instalación "
                 "proyectada, en €, con derecho a retribución a cargo "
                 "del sistema en el año n+2 por las instalaciones puestas "
                 "en servicio el año n. Esta cantidad debe incluir el "
                 "Factor de Retardo Retributivo de la Inversión (FRRI)",
            digits=(10, 3)
        ),
        "estado": fields.selection(
            ESTADO,
            "Estat",
            help="0: Elemento sin modificaciones. "
                 "1: Elemento con modificaciones. "
                 "2: Alta elemento nuevo.",
            size=1),
        "resums_inversio": fields.many2one(
            "giscedata.cnmc.resum_any",
            "Resums"),
    }

    _order = "codi asc"


GiscedataCnmcAltres()
