.. Lectures i comptadors switching documentation master file, created by
   sphinx-quickstart on Wed Apr  3 14:21:33 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentació del mòdul de informes per CNMC 1048
************************************************

El mòdul permet al GISCE-ERP la generació dels informes referents al RD
1048/2013. Aquests informes venen definits per la mateixa **CNMC**.

Actualment, aquest mòdul és capaç de generar els següents informes:
===================================================================

**1:** XML de plans d'inversió
++++++++++++++++++++++++++++++

Aquest informe es genera a partir de la informació introduïda a tal efecte en
el ERP. S'han creat els models amb els camps necessaris per la seva generació a
partir de la documentació de la CNMC. Podem accedir-hi a través del menú
`Administració Pública -> CNE -> Plans d'inversió`

Tindrem el llistat dels plans d'inversió que tenim introduïts.

.. note:: 
   
   S'ha d'introduïr un pla d'inversió per any en curs. Per exemple,
   l'any 2014 s'ha de generar el pla d'inversió que contingui els resums del
   2015 i el 2016, ja que s'entreguen en el mateix fitxer XML a la CNMC.

En el llistat ens apareixaràn amb l'any i el codi R1 els plans de inversió, per exemple en la següent imatge podem observar els plans de inversió del 2013 i 2014, i com entre parèntesis, ens indica que tenen els respectius dos resums.

.. figure:: /_static/pla1.png
   :scale: 80%
   
   *Llistat de plans de inversió.*


Per tal de crear un pla de inversió hem d'apretar el botó de "Nou" i ens apareixarà una altre finestra buida on hi aniràn els resums dels dos anys.

Primer haurem d'omplir els dos camps del codi R1 propi per el pla d'inversió i l'any en que volem que sigui el pla, i després ja podrem clicar el botó de crear els resums, situat just sota del camp de l'any.

.. figure:: /_static/boto.png
   :scale: 80%

   *Dintre del pla de inversió, crearem els resums fent click al botó senyalat.*


Cal crear dos resums, un que ha d'ésser l'any que vindrà i l'altre per l'any següent del que vindrà. Per exemple, si estem a l'any 2014, crearem dos resums, el de 2015 i el de 2016.
Per cada resum anirem emplenant els camps amb el que ens demanin, en algunes pestanyes ens trobarem que cal crear una nova línia, per això anirem al botó de nou situat a on es pot observar a la imatge, emplenarem els camps i apretarem el botó d'aceptar.

.. figure:: /_static/boto_pesta.png
   :scale: 80%

   *En totes les pestanyes menys en la del Resum haurem de crear les noves entrades amb el botó.*


Quan haguem emplenat tots els camps de totes les pestanyes apretarem el botó d'aceptar i guardarem els canvis amb el botó de guardar.

.. figure:: /_static/plaguar.png
   :scale: 80%

   *Guardem els canvis del nou resum.*


Quan tinguem tots els resums per els anys corresponents ja podrem guardar i només faltarà cridar l'assitent de l'XML. L'assistent el podrem cridar dintre del mateix pla, apretant al botó situat a la dreta del menú que posa "Generar XML CNMC Pla" tal i com apareix a l'imatge següent, o bé en el llistat de plans de inversió seleccionant un pla amb un sol click i apretant sobre el botó anomenat "Acció".

.. figure:: /_static/opcio1.png
   :align: center

   *Botó per accedir a l'assistent.*


Ens apareixarà una finestra on simplement haurem d'apretar el botó de "Generar l'XML" on, al cap de un moment, veurem el camp fitxer on podrem apretar "Obrir" per veure l'XML generat o la icona del disquet on podrem guardar el fitxer on volguem. 

.. figure:: /_static/opcio2.png
   :scale: 80%

   *Menú de l'assistent inicial.*

.. figure:: /_static/result.png
   :scale: 80%

   *Un cop generat l'assistent podrem escollir que fer amb l'arxiu.*

