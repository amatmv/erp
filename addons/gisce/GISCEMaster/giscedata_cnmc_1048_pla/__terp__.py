# -*- coding: utf-8 -*-
{
    "name": "GISCE CNMC 1048 PLA INVERSIO",
    "description": """Modulo para la generacio de los planes de inversion""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "base_extended",
        "giscedata_administracio_publica_cne",
        "giscedata_cne"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_cnmc_1048_pla_view.xml",
        "wizard/wizard_generar_cnmc_pla.xml",
        "security/ir.model.access.csv",
        "wizard/wizard_generar_resum.xml"
    ],
    "active": False,
    "installable": True
}
