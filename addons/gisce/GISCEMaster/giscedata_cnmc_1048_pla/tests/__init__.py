# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction


class TestPla(testing.OOTestCase):
    """
    Tests for the generation of plans inversio
    """
    def skip_test_generate(self):
        """
        To test the  write of a giscedata_cella
        with bloquejar_cnmc_tipus_install
        :return: None
        """
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            model_pla = self.openerp.pool.get('giscedata.cnmc.pla_inversio')
            mod_pos = self.openerp.pool.get('giscedata.cnmc.posicions')
            mod_proj = self.openerp.pool.get('giscedata.cnmc.projectes')

            wiz_obj = self.openerp.pool.get('wizard.generar.cnmc.pla')

            context = {"active_id": 1}

            id_pla = model_pla.create(cursor, uid, {"anyo": 2019, "codi": 1})
            id_proj = mod_proj.create(cursor, uid, {"codi": "2019"})
            mod_pos.create(cursor, uid, {"codi": id_proj})

            wiz_obj.action_exec_script(cursor, uid, [id_pla], context)

