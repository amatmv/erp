# -*- coding: utf-8 -*-

import netsvc


def up(cursor, installed_version):
    """
    Alters field any_apm from char to int
    
    :param cursor: Database cursor 
    :param installed_version: Installed version
    :return: None
    """
    logger = netsvc.Logger()
    logger.notifyChannel(
        "migrations",
        netsvc.LOG_INFO,
        "Canviant el tipus del camp any_apm")

    cursor.execute("""
    ALTER TABLE giscedata_cnmc_linies 
      ALTER COLUMN any_apm TYPE int USING any_apm::INTEGER;
    """)
    cursor.execute("""
    ALTER TABLE giscedata_cnmc_posicions
      ALTER COLUMN any_apm TYPE int USING any_apm::INTEGER;
    """)
    cursor.execute("""
    ALTER TABLE giscedata_cnmc_maquines
      ALTER COLUMN any_apm TYPE int USING any_apm::INTEGER;
    """)

    cursor.execute("""
    ALTER TABLE giscedata_cnmc_fiabilitat
      ALTER COLUMN any_apm TYPE int USING any_apm::INTEGER;
    """)
    cursor.execute("""
    ALTER TABLE giscedata_cnmc_ct
      ALTER COLUMN any_apm TYPE int USING any_apm::INTEGER;
    """)
    cursor.execute("""
    ALTER TABLE giscedata_cnmc_altres
      ALTER COLUMN any_apm TYPE int USING any_apm::INTEGER;
    """)
    cursor.execute("""
    ALTER TABLE giscedata_cnmc_despatx
      ALTER COLUMN any_apm TYPE int USING any_apm::INTEGER;
    """)

    logger.notifyChannel(
        "migration",
        netsvc.LOG_INFO,
        "Realitzat correctament")
