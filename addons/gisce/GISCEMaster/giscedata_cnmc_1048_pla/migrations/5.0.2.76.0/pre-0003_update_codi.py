# -*- coding: utf-8 -*-

import netsvc
from psycopg2.extensions import AsIs


def up(cursor, installed_version):
    """
    Alters field codi_tipus_inst from char to many2one

    :param cursor: Database cursor
    :param installed_version: Installed version
    :return: None
    """

    logger = netsvc.Logger()
    logger.notifyChannel(
        "migrations",
        netsvc.LOG_INFO,
        "Canviant el tipus del camp codi de projecte")
    tables_ti = [
        "giscedata_cnmc_linies",
        "giscedata_cnmc_posicions",
        "giscedata_cnmc_maquines",
        "giscedata_cnmc_despatx",
        "giscedata_cnmc_ct",
        "giscedata_cnmc_altres",
        "giscedata_cnmc_fiabilitat"
    ]
    for table in tables_ti:
        logger.notifyChannel(
            "migrations",
            netsvc.LOG_INFO,
            "Migrant taula {}".format(table))
        cursor.execute("""
          ALTER TABLE %(table)s RENAME COLUMN codi to codi_old;
        """, {"table": AsIs(table)})
        cursor.execute("""
            ALTER TABLE %(table)s ADD COLUMN codi int;
        """, {"table": AsIs(table)})
        cursor.execute("""
          UPDATE %(table)s set codi=(SELECT id from giscedata_cnmc_projectes where name=codi_old);
        """, {"table": AsIs(table)})

    logger.notifyChannel(
        "migration",
        netsvc.LOG_INFO,
        "Realitzat correctament")
