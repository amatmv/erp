# -*- coding: utf-8 -*-

import netsvc


def up(cursor, installed_version):
    """
    Alters field any_apm from char to int
    
    :param cursor: Database cursor 
    :param installed_version: Installed version
    :return: None
    """

    logger = netsvc.Logger()
    logger.notifyChannel(
        "migrations",
        netsvc.LOG_INFO,
        "Canviant el tipus dels camps macroeconomics a char")

    cursor.execute("""
    ALTER TABLE giscedata_cnmc_resum_any 
      ALTER COLUMN macro_crec_pib TYPE char(23);
    """)
    cursor.execute("""
    ALTER TABLE giscedata_cnmc_resum_any
      ALTER COLUMN macro_pib_prev TYPE char(23);
    """)
    cursor.execute("""
    ALTER TABLE giscedata_cnmc_resum_any
      ALTER COLUMN macro_limite_sector TYPE char(23);
    """)
    cursor.execute("""
    ALTER TABLE giscedata_cnmc_resum_any
      ALTER COLUMN macro_inc_demanda_sector TYPE char(23);
    """)

    logger.notifyChannel(
        "migration",
        netsvc.LOG_INFO,
        "Realitzat correctament")
