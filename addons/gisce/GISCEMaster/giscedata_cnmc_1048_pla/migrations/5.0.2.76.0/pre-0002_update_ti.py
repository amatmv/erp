# -*- coding: utf-8 -*-

import netsvc
from psycopg2.extensions import AsIs


def up(cursor, installed_version):
    """
    Alters field codi_tipus_inst from char to many2one

    :param cursor: Database cursor
    :param installed_version: Installed version
    :return: None
    """

    logger = netsvc.Logger()
    logger.notifyChannel(
        "migrations",
        netsvc.LOG_INFO,
        "Canviant el tipus del camp codi_tipus_inst")
    tables_ti = [
        "giscedata_cnmc_linies",
        "giscedata_cnmc_posicions",
        "giscedata_cnmc_maquines",
        "giscedata_cnmc_despatx",
        "giscedata_cnmc_fiabilitat",
        "giscedata_cnmc_ct",
        "giscedata_cnmc_altres"
    ]
    for table in tables_ti:
        logger.notifyChannel(
            "migrations",
            netsvc.LOG_INFO,
            "Migrant taula {}".format(table))
        cursor.execute("""
          ALTER TABLE %(table)s RENAME COLUMN codi_tipus_inst to codi_tipus_inst_old;
        """, {"table": AsIs(table)})
        cursor.execute("""
            ALTER TABLE %(table)s ADD COLUMN codi_tipus_inst int;
        """, {"table": AsIs(table)})
        cursor.execute("""
          UPDATE %(table)s set codi_tipus_inst=(SELECT id from giscedata_tipus_installacio where name=codi_tipus_inst_old);
        """, {"table": AsIs(table)})

    logger.notifyChannel(
        "migration",
        netsvc.LOG_INFO,
        "Realitzat correctament")
