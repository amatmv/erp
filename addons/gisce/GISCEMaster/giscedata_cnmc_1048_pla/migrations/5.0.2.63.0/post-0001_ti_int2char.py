# -*- coding: utf-8 -*-

import netsvc


def migrate(cursor, installed_version):
    logger = netsvc.Logger()
    logger.notifyChannel('migrations', netsvc.LOG_INFO,
                         'Convertint TIs de int a char')

    tables = ['giscedata_cnmc_linies', 'giscedata_cnmc_posicions',
              'giscedata_cnmc_maquines', 'giscedata_cnmc_despatx',
              'giscedata_cnmc_fiabilitat', 'giscedata_cnmc_ct',
              'giscedata_cnmc_altres']
    field = 'codi_tipus_inst'

    for table in tables:
        sql = '''
                ALTER TABLE {0}
                  ALTER COLUMN {1} TYPE VARCHAR(10)'''
        cursor.execute(sql.format(table, field))

    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         u"Realitzat correctament")
