# -*- coding: utf-8 -*-

import netsvc
import pooler

def migrate(cursor, installed_version):
    logger = netsvc.Logger()
    logger.notifyChannel('migrations', netsvc.LOG_INFO,
                         "Borrant la constraint de unique "
                         "codi de giscedata_cnmc")

    cursor.execute("""alter table giscedata_cnmc_linies
                      drop constraint giscedata_cnmc_linies_codi;""")
    cursor.execute("""alter table giscedata_cnmc_posicions
                      drop constraint giscedata_cnmc_posicions_codi;""")
    cursor.execute("""alter table giscedata_cnmc_maquines
                      drop constraint giscedata_cnmc_maquines_codi;""")
    cursor.execute("""alter table giscedata_cnmc_fiabilitat
                      drop constraint giscedata_cnmc_fiabilitat_codi;""")
    cursor.execute("""alter table giscedata_cnmc_ct
                      drop constraint giscedata_cnmc_ct_codi;""")

    logger.notifyChannel('migration', netsvc.LOG_INFO,
                            u"Realitzat correctament")