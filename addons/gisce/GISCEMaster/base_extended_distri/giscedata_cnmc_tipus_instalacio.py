# -*- coding: utf-8 -*-

from osv import osv, fields


class giscedata_cnmc_tipo_instalacion(osv.osv):
    """Tipus Instalacions del CNMC"""
    _name = 'giscedata_cnmc.tipo_instalacion'
    _description = "Tipus d'Instalació del CNMC"

    def asignar_cini(self, cursor, uid, codi):
        """Asignar valor de cini donat un codi"""
        descripcio = None
        cini = None

        ids = self.search(cursor, uid, [('codi', '=', codi)])
        if not ids:
            vals = {
                'codi': codi,
                'descripcio': descripcio,
                'cini': cini
            }
            res = self.create(cursor, uid, vals)
        else:
            desc = self.read(cursor, uid, ids, ['descripcio'])
            vals = {
                'codi': codi,
                'descripcio': desc,
                'cini': cini
            }
            res = self.write(cursor, uid, ids, vals)

        return res

    _columns = {
        'codi': fields.integer('Codi', required=True),
        'descripcio': fields.char('Descripcio', size=255),
        'cini': fields.char('Cini', size=12, required=True),
    }

    _sql_constraints = [
        ('cini', 'unique (cini)',
         'Ja existeix un tipus de instalació amb aquest cini.')
    ]

    _order = "codi asc"

giscedata_cnmc_tipo_instalacion()

