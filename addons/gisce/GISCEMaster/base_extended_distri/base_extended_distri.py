# -*- coding: utf-8 -*-

from osv import osv,fields


class res_partner_address(osv.osv):
    _name = 'res.partner.address'
    _inherit = 'res.partner.address'

    _columns = {
      'municipi': fields.many2one('res.municipi', 'Municipi'),
    }

res_partner_address()


class res_partner_category(osv.osv):
    _name = 'res.partner.category'
    _inherit = 'res.partner.category'

    _columns = {
      'code': fields.char('Codi categoria', size=10),
    }

res_partner_category()
