# -*- coding: utf-8 -*-
import netsvc
import pooler


def migrate(cursor, installed_version):
    logger = netsvc.Logger()

    logger.notifyChannel('migrations', netsvc.LOG_INFO,
                         u"Esborrem el model giscedata_cnmc.tipo_instalacion"
                         u" si existeix.")

    sql = ("DELETE FROM ir_model_data WHERE model='ir.model' AND "
           "name='model_giscedata_cnmc_tipo_instalacion'")

    cursor.execute(sql)

    logger.notifyChannel('migrations', netsvc.LOG_INFO,
                         u"Esborrats %d registres si existeix.")