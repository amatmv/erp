# -*- coding: utf-8 -*-
{
    "name": "Base extension (Distribuidora)",
    "description": """Base models extensions""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Generic Modules",
    "depends":[
        "base",
        "base_extended"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "base_extended_distri_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
