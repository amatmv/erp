# -*- coding: utf-8 -*-
from osv import osv,fields
from tools.translate import _
from datetime import datetime
import operator
from giscedata_transformadors_saturacio_tg.giscedata_telemesures_base import TECNOLOGY_MEASURES_PREFIX


class GiscedataTransforamadorTrafoSaturacioTg(osv.osv):

    _name = 'giscedata.transformador.trafo'
    _inherit = 'giscedata.transformador.trafo'

    def calc_saturacio_from_tg(self, cursor, uid, trafo_id, max_date,
                               context=None):
        """
        Calcules trafo saturation using TG curves and billings

        Bn: Find first monthly absolute billing before max_date
            compares billing maximeter (Bn MAX) with trafo power

                Bn saturation = Bn MAX / kVA

        When 1 output: B1+B2 =  Bn

        When 2 outputs:
            Calcs Bn saturation

            * Gets billing period curve an finds max value (Bn Curve MAX).
            * Gets a factor for every curve:
                kn = Bn MAX / curve MAX
            * Adds 2 curves applying factor:
                Ct = C1 * k1 + C2 + k2
            * For B1+B2 Saturation gets Ct maximum (Ct MAX) value and
            compares with trafo power:
                B1+B2 saturation = Ct MAX / kVA

        :param trafo_id: Trafo id
        :param max_date: date to find billing from
        :param context:
        :return: tuple
           OK: True/False
           text: result description
        """

        reg_obj = self.pool.get('giscedata.registrador')
        trafo_obj = self.pool.get('giscedata.transformador.trafo')
        sat_obj = self.pool.get('giscedata.transformadors.saturacio')

        trafo_data = trafo_obj.read(
            cursor, uid, trafo_id, ['name', 'ct', 'potencia_nominal']
        )

        trafo_name = trafo_data['name']
        ct_id = trafo_data['ct'] and trafo_data['ct'][0] or False
        ct_name = trafo_data['ct'] and trafo_data['ct'][1] or 'not found'

        if not ct_id:
            #TODO: Buscar a històric
            text = _(u'* ERROR: Trafo {} no vinculat a cap CT. No es pot '
                     u'calcular la saturacio\n').format(trafo_name)
            return False, text

        text = _(u'## Calculant saturacio del transformador {} actualment en el '
                 u'CT {}:\n').format(
            trafo_name,
            ct_name
        )

        # gets trafo registrador
        reg_ids = reg_obj.search(cursor, uid, [('trafo_id', '=', trafo_id)])

        if len(reg_ids) == 0:
            text += _(u" * ERROR: No s'ha trobat cap registrador TG\n")
            return False, text

        maximeters = {}
        reg_saturations = {}
        reg_fields = ['name', 'trafo_output', 'technology']
        for reg_data in reg_obj.read(cursor, uid, reg_ids, reg_fields):
            reg_id = reg_data['id']
            reg_name = reg_data['name']
            tech = reg_data['technology']
            output = reg_data['trafo_output']
            text += _(u"   # Registrador {} per sortida {}\n").format(
                    reg_name, output
            )
            reg_max = reg_obj.get_maximeter_date(
                cursor, uid, reg_id, bill_date=max_date, context=context
            )
            if not reg_max:
                text += _(u"   * ERROR: No s'ha trobat cap maxímetre al "
                          u"registrador {}\n").format(reg_name)
                return False, text

            c_start, c_end, maximeter, maximeter_date = reg_max
            text += _(u"   * Tancament de {} a {}:\n"
                      u"     - Maxímetre de {} W el dia {}\n").format(*reg_max)
            curve_model = '{}.profile'.format(
                TECNOLOGY_MEASURES_PREFIX.get(tech, 'tg')
            )
            curve_obj = self.pool.get(curve_model)
            # look for curve
            curve_search = [
                ('name', '=', reg_name),
                ('timestamp', '>', c_start),
                ('timestamp', '<=', c_end),
            ]
            if 'type' in curve_obj.fields_get(cursor, uid).keys():
                curve_search.append(('type', '=', 'p'))
            curve_ids = curve_obj.search(
                cursor, uid, curve_search, order='timestamp ASC'
            )
            curve_data = curve_obj.read(
                cursor, uid, curve_ids, ['ai', 'magn', 'timestamp', 'season']
            )
            # Cn
            curve_sort = sorted(curve_data, key=lambda k: k['timestamp'])
            curve = [(e['timestamp'], e['ai'] * e['magn'])
                     for e in curve_sort
                    ]
            if not curve:
                text += _(u"   * ERROR: No s'ha trobat la corba del registrador "
                          u"{} entre {} i {}\n").format(
                    reg_name, c_start, c_end
                )
                return False, text

            curve_values = dict(curve).values()
            # curve MAX
            curve_max = max(curve_values)
            curve_max_day = curve[curve_values.index(curve_max)][0]
            # kn
            curve_k = curve_max and maximeter / curve_max * 1.0 or 1.0
            text += _(u"   * Corba pel registrador {} entre {} i {}:\n"
                      u"     - {} hores \n"
                      u"     - Màxim de {} W a les {} \n"
                      u"   * FACTOR (kn): {}\n"
                      ).format(
                reg_name, c_start, c_end, len(curve), curve_max, curve_max_day,
                curve_k
            )
            maximeters[reg_id] = dict(zip(
                ['start', 'end', 'max', 'max_date', 'curve', 'curve_k',
                 'output'],
                [c_start, c_end, maximeter, maximeter_date, curve, curve_k,
                 output]
            ))
            if output in reg_saturations.keys():
                text += _(u'   * [W] Sortida del transformador incorrecta. '
                          u'Configurada sortida {}\n').format(output)
                output = 'b' + str((int(output[-1]) % 2) + 1)
            reg_saturations[output] = maximeter
        text += "--\n"
        trafo_power = trafo_data['potencia_nominal']
        saturation_date = datetime.strptime(
            c_end, '%Y-%m-%d %H:%M:%S'
        ).date()

        if len(maximeters.keys()) > 1:
            # 2 outputs
            maxim = sum([m['max'] for m in maximeters.values()])
            saturation = ((maxim / 1000.0) / trafo_power)
            # Normalized curve saturation
            # Maximum of Normalized curves addition
            curve_len_set = set([len(m['curve']) for m in maximeters.values()])
            if len(curve_len_set) > 1:
                text += _(u" * ERROR: Les corves de les dues sortides són "
                          u" diferents: {}\n").format(curve_len_set)
                return False, text
            add_curve = zip(
                *[map(lambda p: operator.mul(p, m['curve_k']), dict(m['curve']).values())
                  for m in maximeters.values()
                 ]
            )
            final_curve = [a[0] + a[1] for a in add_curve]
            normalized_max = max(final_curve)
            normalized_max_date = maximeters.values()[0]['curve'][final_curve.index(normalized_max)][0]
            text += _(u'* Corba agregada ponderada:\n'
                      u'  - Màxim de la corba: {} el dia {}\n').format(
                              normalized_max, normalized_max_date
                    )
            saturation = ((normalized_max / 1000.0) / trafo_power)
        else:
            # 1 output
            saturation = (
                (maximeters.values()[0]['max'] / 1000.0) / trafo_power
            )
            text += _(u" * Saturació pel transformador {} ({} kVA) in CT {}:\n"
                      u"     {} el dia {}\n").format(
                trafo_name, trafo_power, ct_name, saturation, saturation_date
            )

        # saturation data
        saturation_data = {
            'ct': ct_id,
            'trafo': trafo_id,
            'data': str(saturation_date),
            'max_b1': trafo_power,
            'max_b2': reg_saturations.get('b2',False) and trafo_power or 0,
            'mesurada_b1': reg_saturations.get('b1', 0) / 1000.0,
            'mesurada_b2': reg_saturations.get('b2', 0) / 1000.0,
            'saturacio_b1': 100.0 * (reg_saturations.get('b1', 0) / 1000.0) / trafo_power,
            'saturacio_b2': 100.0 * (reg_saturations.get('b2', 0) / 1000.0) / trafo_power,
            'b1_b2': saturation * 100.0,
            'observacions': text,
        }
        # validating existence
        sat_ids = sat_obj.search(cursor, uid,
            [('ct', '=', ct_id), ('trafo','=', trafo_id),
             ('data', '=', str(saturation_date))
             ]
        )
        if sat_ids:
            text += _(u'## Sobreescrivim la saturacio del CT {} el dia {} '
                      u'pel trafo {}\n\n').format(
                ct_name, saturation_date, trafo_name
            )
            sat_obj.write(cursor, uid, sat_ids[0], saturation_data, context=context)
        else:
            text += _(u'## Creem la saturacio del CT {} el dia {} '
                      u'pel trafo {}\n\n').format(
                ct_name, saturation_date, trafo_name
            )
            sat_obj.create(cursor, uid, saturation_data, context=context)

        return True, text


GiscedataTransforamadorTrafoSaturacioTg()
