SELECT
    ct.name as ct_name,
    ct.descripcio as description,
    COALESCE(trafo.name, '') as trafo_name,
    sat.data as data,
    COALESCE(sat.mesurada_b1, 0.00) as mesurada_b1,
    COALESCE(sat.max_b1, 0.00) as max_b1,
    COALESCE(saturacio_b1, 0.00) as saturacio_b1,
    COALESCE(sat.mesurada_b2, 0.00) as mesurada_b2,
    COALESCE(sat.max_b2, 0.00) as max_b2,
    COALESCE(sat.saturacio_b2, 0.00) as saturacio_b2,
    COALESCE(sat.b1_b2, 0.00) as saturacio_total
FROM giscedata_cts AS ct
LEFT JOIN giscedata_transformadors_saturacio sat ON sat.ct=ct.id AND TO_CHAR(sat.data, 'YYYY/MM') = %(mes_any)s
LEFT JOIN giscedata_transformador_trafo trafo on trafo.id=sat.trafo
WHERE ct.active
ORDER BY ct.name