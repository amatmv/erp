# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _
from datetime import datetime


class WizardSaturacioTrafoFromTG(osv.osv_memory):
    """
    Asistent per crear saturacions de transformadors a partir de la
    """

    _name = "wizard.saturacio.trafo.from.tg"

    def get_trafo_ids(self, cursor, uid, context=None):
        if context is None:
            context = {}

        trafo_obj = self.pool.get('giscedata.transformador.trafo')

        if context.get('all', False):
            # All the trafos
            trafo_ids = trafo_obj.search(cursor, uid, [], context=context)
        else:
            trafo_ids = context.get('active_ids', False)

        return trafo_ids

    def _default_info(self, cursor, uid, context=None):
        trafo_obj = self.pool.get('giscedata.transformador.trafo')

        trafo_ids = self.get_trafo_ids(cursor, uid, context)

        if len(trafo_ids) == 1:
            trafo_data = trafo_obj.read(
                cursor, uid, trafo_ids[0], ['name', 'ct']
            )
            txt = _(u"""Es calcularà la saturació del transformador {} """
                    u"""actualment en el CT {}""").format(
                trafo_data['name'],
                trafo_data['ct'] and trafo_data['ct'][1] or ''
            )
        else:
            txt = _(u"""Es calcularà la saturació de {} """
                    u"""transformadors""").format(len(trafo_ids))

        return txt

    def calc_saturacio_trafos(self, cursor, uid, ids, context=None):
        """
        Calcula la saturació dels trafos utilitzant la funció del trans
        :param cursor:
        :param uid:
        :param ids:
        :param context:
        :return:
        """
        wiz = self.browse(cursor, uid, ids[0], context)

        trafo_obj = self.pool.get('giscedata.transformador.trafo')

        trafo_ids = self.get_trafo_ids(cursor, uid, context)
        data = datetime.strptime(wiz.data, '%Y-%m-%d %H:%M:%S')
        info_text = ''
        ok_counter = 0
        total_counter = 0
        for trafo_id in trafo_ids:
            res, text = trafo_obj.calc_saturacio_from_tg(
                cursor, uid, trafo_id, data
            )
            info_text += '\n'
            info_text += text
            ok_counter += res and 1 or 0
            total_counter += 1

        text = _(u'Calculada la saturació de {}/{} transformadors\n').format(
            ok_counter, total_counter
        )
        text += info_text

        wiz.write({
            'info': text,
            'state': 'done'
        })

    _columns = {
        'data': fields.datetime('Data',
            help=u'Es buscarà el tancament anterior a aquesta data més '
                 u'recent entre la data seleccionada i el mateix dia '
                 u'del mes anterior'
        ),
        'state': fields.char('State', size=16),
        'info': fields.text('Info'),
    }

    _defaults = {
        'data': lambda *a: datetime.today().strftime('%Y-%m-%d %H:%M:%S'),
        'state': lambda *a: 'init',
        'info': _default_info
    }

WizardSaturacioTrafoFromTG()
