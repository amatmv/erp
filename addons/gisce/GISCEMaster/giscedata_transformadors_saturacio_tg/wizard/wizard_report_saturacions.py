# -*- coding: utf-8 -*-
from osv import osv, fields
from datetime import datetime


class WizardSaturacioReport(osv.osv_memory):
    """
    Asistent per Informe de saturació de trafos en una data concreta
    """

    _name = "wizard.saturacio.report"

    def _default_mes_any(self, cursor, uid, context=None):

        sql = "SELECT distinct TO_CHAR(data, 'YYYY/MM') mes_any " \
              "FROM giscedata_transformadors_saturacio " \
              "ORDER BY mes_any DESC"

        cursor.execute(sql)
        return [(row[0], row[0]) for row in cursor.fetchall()]

    def action_print_saturacio_trafos(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        wiz = self.browse(cursor, uid, ids[0])
        context.update({
            'date': wiz.mes_any
        })
        return {
            'type': 'ir.actions.report.xml',
            'model': 'giscedata.transformadors.saturacio',
            'report_name': 'giscedata.transformadors.saturacio.tg',
            'report_webkit': 'giscedata_transformadors_saturacio_tg/report/saturacio_report.mako',
            'webkit_header': 'giscedata_transformadors_saturacio.webkit_saturacio_header',
            'groups_id': ids,
            'multi': '0',
            'auto': '0',
            'header': '0',
            'report_rml': 'False',
            'datas': {
                'ids': ids,
                'context': context
            }
        }

    _columns = {
        'mes_any': fields.selection(_default_mes_any, 'Data informe'),
    }


WizardSaturacioReport()