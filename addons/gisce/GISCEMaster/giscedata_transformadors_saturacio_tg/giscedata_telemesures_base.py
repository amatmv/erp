# -*- coding: utf-8 -*-
from osv import osv, fields
from datetime import datetime, date, timedelta

TECNOLOGY_MEASURES_PREFIX = {
    'telegestion': 'tg',
    'telemeasure': 'tm'
}


class GiscedataRegistradorSaturacioTg(osv.osv):

    _name = 'giscedata.registrador'
    _inherit = 'giscedata.registrador'

    def get_maximeter_date(self, cursor, uid, reg_id, bill_date=None, context=None):
        '''
        returns most recent maxímeter of registrator (period 0)
        until 30 days before bill_date if found. None otherwise
        c: database connection
        reg_id: registrator id
        bill_date: datetime object. today if None as default value
        returns: billing date start and end, maxímeter (W) , date of maximeter
                 None if not found
        '''
        reg_data = self.read(cursor, uid, reg_id, ['name', 'technology'])
        if bill_date is None:
            bill_date = datetime.today()
        from_date = bill_date - timedelta(days=60)
        tech = reg_data['technology']

        billing_model = '{}.billing'.format(
            TECNOLOGY_MEASURES_PREFIX.get(tech, 'tg')
        )

        bill_obj = self.pool.get(billing_model)

        search_data = [
            ('name', '=', reg_data['name']),
            ('value', '=', 'a'),  # absolute
            ('type', '=', 'month'),
            ('period', '=', 0),
            ('date_end', '<=', bill_date.strftime('%Y-%m-%d')),
            ('date_end', '>=', from_date.strftime('%Y-%m-%d'))
        ]
        bill_ids = bill_obj.search(cursor, uid, search_data, 0, 0, 'date_end DESC')
        if not bill_ids:
            return None
        bill_data = bill_obj.read(cursor, uid, bill_ids[0], [])

        return bill_data['date_begin'], bill_data['date_end'], bill_data['max'], bill_data['date_max']

GiscedataRegistradorSaturacioTg()
