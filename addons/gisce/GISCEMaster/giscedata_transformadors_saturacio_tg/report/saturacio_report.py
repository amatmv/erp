# -*- coding: utf-8 -*-
from c2c_webkit_report import webkit_report
from report import report_sxw

webkit_report.WebKitParser(
    'report.giscedata.transformadors.saturacio.tg',
    'giscedata.transformadors.saturacio',
    'giscedata_transformadors_saturacio_tg/report/saturacio_report.mako',
    parser=report_sxw.rml_parse
)