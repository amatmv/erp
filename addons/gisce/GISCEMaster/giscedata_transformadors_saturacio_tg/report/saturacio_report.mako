<%
from datetime import datetime

mes_any = data['context'].get('date', False)
if not mes_any:
    mes_any = datetime.today().strftime('%Y/%m')

sql_path = '{}/giscedata_transformadors_saturacio_tg/sql/saturacio_report.sql'.format(addons_path)
with open(sql_path, 'r') as sqlfile:
    sql = sqlfile.read()
    cursor.execute(sql, {'mes_any': mes_any})
res = cursor.fetchall()
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<meta charset="utf-8">
<html>
<head>
    <title>${_(u"Saturació trasnformadors BT")}</title>
    <style type="text/css">
        body {
            font-size: 12px;
        }

        h2 {
            font-size: .8em;
        }

        table {
            width: 100%;
            padding: 0px;
            font-size: .8em;
            margin: 0px;
            border-collapse: collapse;
        }

        .no_border{
            border: 0px;
        }

        thead { display: table-header-group }
        tfoot { display: table-row-group }
        tr {
            page-break-inside: avoid;
        }

        th, td {
            border: 1px #000000 solid;
            padding: 2px 6px;
            margin: 0px;
        }

        td {
            text-align: left;
        }

        .saturated {
            color: red;
            font-weight: bold;
        }
    </style>
</head>
<body>
<h2>${_(u"SATURACIÓ DE TRANSFORMADORS EL MES {}").format(mes_any)}</h2>
<table>
    <thead>
    <tr height="2px"><th class="no_border" colspan="12">&nbsp;</th></tr>
    <tr>
        <th colspan="4" class="no_border">&nbsp;</th>
        <th colspan="3">B1</th>
        <th colspan="3">B2</th>
        <th class="no_border">&nbsp;</th>
    </tr>
    <tr>
        <th>CT</th>
        <th>${_('DESCRIPCIÓ')}</th>
        <th>${_('Nº. TRF.')}</th>
        <th>${_('DATA')}</th>
        <th>${_('I. MESURADA')}</th>
        <th>${_('I. MAX. ADMISIBLE')}</th>
        <th>${_('%. SATURACIÓ')}</th>
        <th>${_('I. MESURADA')}</th>
        <th>${_('I. MAX. ADMISIBLE')}</th>
        <th>${_('%. SATURACIÓ')}</th>
        <th>${_('B1+B2')}</th>
    </tr>
    </thead>
    <tbody>
        %for row in res:
            <tr${row[10]>80.0 and " class='saturated'" or ''}>
                <td>${row[0]}</td>
                <td>${row[1]}</td>
                <td>${row[2]}</td>
                <td>${row[3] and datetime.strptime(row[3], '%Y-%m-%d %H:%M:%S').strftime('%d/%m/%Y') or ''}</td>
                <td>${row[4]}</td>
                <td>${row[5]}</td>
                <td>${row[6]} %</td>
                <td>${row[7]}</td>
                <td>${row[8]}</td>
                <td>${row[9]} %</td>
                <td>${row[10]} %</td>
            </tr>
        %endfor
    </tbody>
</table>
</body>
</html>
