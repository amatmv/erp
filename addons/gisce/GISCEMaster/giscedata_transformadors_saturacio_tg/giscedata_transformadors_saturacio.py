# -*- coding: utf-8 -*-
from osv import osv,fields


class SaturacionsTg(osv.osv):

    _name = 'giscedata.transformadors.saturacio'
    _inherit = 'giscedata.transformadors.saturacio'

    _columns = {
        'observacions': fields.text('Observacions')
    }

SaturacionsTg()