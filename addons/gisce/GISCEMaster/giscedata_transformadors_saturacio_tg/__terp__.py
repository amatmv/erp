# -*- coding: utf-8 -*-
{
    "name": "Saturació dels Transformadors des de TG",
    "description": """Saturació dels transformadors des de STG:
    Utilitza les corbes y els tancamente dels supervisors de TG per calcular
    la saturació dels comptadors""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_transformadors_saturacio",
        "giscedata_lectures_telegestio",
        "giscedata_telemesures_infraestructura"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "wizard/wizard_saturacio_trafo_from_tg_view.xml",
        "wizard/wizard_report_saturacions_view.xml",
        "giscedata_transformadors_saturacio_view.xml",
        "giscedata_transformadors_saturacio_report.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
