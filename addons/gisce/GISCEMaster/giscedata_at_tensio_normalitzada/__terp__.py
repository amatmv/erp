# -*- coding: utf-8 -*-
{
    "name": "GISCE Data Alta Tensió (Tensions normalitzades)",
    "description": """
Fa que el camp tensió de les línies AT sigui un camp 'selection' i utilitzi les tensions normalitzades definides amb el mòdul 'giscedata_tensions'.
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_at",
        "giscedata_tensions"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
