# -*- coding: utf-8 -*-

from osv import osv, fields

class giscedata_at_linia(osv.osv):

    _name = 'giscedata.at.linia'
    _inherit = 'giscedata.at.linia'

    def _tensio_normalitzada(self, cr, uid, context={}):
        obj = self.pool.get('giscedata.tensions.tensio')
        ids = obj.search(cr, uid, [('tipus', '=', 'AT')], order='tipus')
        res = obj.read(cr, uid, ids, ['name', 'id'], context)
        res = sorted([(int(r['name']), r['name']) for r in res],
                     key=lambda a: a[0])
        return res

    _columns = {
        'tensio': fields.selection(_tensio_normalitzada, 'Tensió')
    }

giscedata_at_linia()
