# -*- encoding: utf-8 -*-
"""Module for comerdist base.
"""
from osv import osv, fields
from osv.orm import except_orm
import time
import pooler
from erppeek_wst import ClientWST
from service import security
from datetime import datetime, timedelta
import netsvc
from tools.translate import _
from functools import wraps


def timer(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        start = datetime.now()
        res = f(*args, **kwargs)
        end = datetime.now()
        log = netsvc.Logger()
        duration = "Time: %.3fs." % ((end - start).seconds +
                             float((end - start).microseconds) / 10 ** 6)
        model = hasattr(args[0], 'model') and args[0].model or ''
        log.notifyChannel('timer', netsvc.LOG_INFO,
                          '%s %s %s' % (model, f.__name__, duration))
        return res
    return wrapper


def model2cc(model):
    """Converteix el model en CamelCase.
    """
    return ''.join(map(lambda x: x.capitalize(), model.split('.')))


class SyncCursor(object):
    """Sync Cursor.
    """
    def __init__(self, cursor, ttl=3600):
        self._cursor = cursor
        self.create_date = datetime.now()
        self.last_access = datetime.now()
        self.ttl = ttl

    @property
    def cursor(self):
        """Gets the cursor and updates last_accessed.
        """
        self.last_access = datetime.now()
        return self._cursor

    def close(self):
        """Closes the cursor.
        """
        self.cursor.close()

    def rollback(self):
        """Rollbacks the cursor.
        """
        return self.cursor.rollback()

    def commit(self):
        """Commit the cursor.
        """
        return self.cursor.commit()

    def is_abandoned(self):
        """Checks if the cursor is long away from ttl.
        """
        return datetime.now() > self.last_access + timedelta(seconds=self.ttl)


class SyncService(netsvc.Service):
    """Sync service to allow XML-RPC transactions.
    """
    def __init__(self, name='sync'):
        """Init method.
        """
        netsvc.Service.__init__(self, name)
        self.joinGroup('web-services')
        self.cursors = {}
        self.exportMethod(self.get_transaction)
        self.exportMethod(self.execute)
        self.exportMethod(self.rollback)
        self.exportMethod(self.commit)
        self.exportMethod(self.close)

    def log(self, log_level, message):
        """Logs througth netsvc.Logger().
        """
        logger = netsvc.Logger()
        logger.notifyChannel('transaction-ws', log_level, message)

    def clean(self):
        """Clean abandoned cursors.
        """
        self.log(netsvc.LOG_INFO, 'Searching for abandoned transactions...')
        for trans, cursor in self.cursors.items():
            if cursor.is_abandoned():
                last_access = cursor.last_accessed.strftime('%Y-%m-%d %H:%M:%S')
                self.log(netsvc.LOG_INFO, 'Deleting transaction ID: %i. '
                                          'Last accessed on %s'
                                          % (trans, last_access))
                cursor.rollback()
                cursor.close()
                del self.cursors[trans]

    def get_transaction(self, dbname, uid, passwd, transaction_id):
        """Get transaction for all XML-RPC.
        """
        security.check(dbname, uid, passwd)
        if transaction_id not in self.cursors:
            database, pool = pooler.get_db_and_pool(dbname)
            cursor = database.cursor()
            sync_cursor = SyncCursor(cursor)
            self.log(netsvc.LOG_INFO, 'Creating a new transaction ID: %s'
                     % transaction_id)
            self.cursors[transaction_id] = sync_cursor
        return transaction_id

    def get_cursor(self, transaction_id):
        """Gets cursor and pool.
        """
        if transaction_id not in self.cursors:
            raise Exception("There are no Cursor for this transacion %s"
                            % transaction_id)
        cursor = self.cursors[transaction_id]
        return cursor

    def execute(self, dbname, uid, passwd, transaction_id, obj, method, *args,
                **kw):
        """Executes code with transaction_id.
        """
        security.check(dbname, uid, passwd)
        sync_cursor = self.get_cursor(transaction_id)
        cursor = sync_cursor.cursor
        pool = pooler.get_db_and_pool(dbname)[1]
        try:
            #self.log(netsvc.LOG_INFO, 'Executing from transaction ID: %s'
            #         % transaction_id)
            res = pool.execute_cr(cursor, uid, obj, method, *args, **kw)
        except Exception, exc:
            self.rollback(dbname, uid, passwd, transaction_id)
            import traceback
            traceback.print_exc()
            raise exc
        return res

    def rollback(self, dbname, uid, passwd, transaction_id):
        """Rollbacks XML-RPC transaction.
        """
        security.check(dbname, uid, passwd)
        sync_cursor = self.get_cursor(transaction_id)
        self.log(netsvc.LOG_INFO, 'Rolling back transaction ID: %s'
                 % transaction_id)
        return sync_cursor.rollback()

    def commit(self, dbname, uid, passwd, transaction_id):
        """Commit XML-RPC transaction.
        """
        security.check(dbname, uid, passwd)
        sync_cursor = self.get_cursor(transaction_id)
        self.log(netsvc.LOG_INFO, 'Commiting transaction ID: %s'
                 % transaction_id)
        return sync_cursor.commit()

    def close(self, dbname, uid, passwd, transaction_id):
        """Closes XML-RPC transaction.
        """
        security.check(dbname, uid, passwd)
        sync_cursor = self.get_cursor(transaction_id)
        self.log(netsvc.LOG_INFO,
                 'Closing transaction ID: %s' % transaction_id)
        res = sync_cursor.close()
        del self.cursors[transaction_id]
        return res

SyncService()


class OOOPPool(object):
    '''Pool of erppeek sync connections'''

    @staticmethod
    def get_ooop(cursor, uid, config_id):
        """Get sync connection from config"""
        dbname = cursor.dbname
        tid = str(id(cursor))
        service_name = 'sync.%s' % tid
        if not service_name in netsvc.SERVICES:
            cfg_obj = pooler.get_pool(dbname).get('giscedata.comerdist.config')
            config = cfg_obj.browse(cursor, uid, config_id)
            syncproxy = ClientWST("http://%s:%s" % (config.host, config.port),
                               db=config.dbname, user=config.user,
                               password=config.password)
            syncproxy.get_transaction(tid)
            setattr(syncproxy, 'search_cache', {})
            netsvc.SERVICES[service_name] = syncproxy
        return netsvc.SERVICES[service_name]


class Sync(object):
    """Sync object.
    """
    def __init__(self, cursor, uid, conn, model, **kwargs):
        """Init function.
        """
        self.cursor = cursor
        self.uid = uid
        self.conn = conn
        self.manager = getattr(conn, model2cc(model))
        self.model = model
        self.local_fields_list = []
        self.remote_fields_list = []
        self.config = kwargs.get('config', False)

    def get_match_fields(self):
        """Return match fields for remote object.
        """
        match_fields = [('name', 'name')]
        if self.config:
            dbname = self.cursor.dbname
            model = 'giscedata.comerdist.model.matchfields'
            mf_obj = pooler.get_pool(dbname).get(model)
            search_params = [
                ('model_id.name', '=', self.model),
                ('model_id.config_id.id', '=', self.config)
            ]
            mf_ids = mf_obj.search(self.cursor, self.uid, search_params)
            if mf_ids:
                match_fields = [(mf.src_name, mf.dst_name)
                                for mf in mf_obj.browse(self.cursor, self.uid,
                                                        mf_ids)]
        return match_fields

    def get_custom_fields(self, mode):
        """Get fields where type update is defined.
        """
        custom_fields = []
        if self.config:
            dbname = self.cursor.dbname
            model = 'giscedata.comerdist.model.syncfields'
            bf_obj = pooler.get_pool(dbname).get(model)
            search_params = [
                ('model_id.name', '=', self.model),
                ('model_id.config_id.id', '=', self.config)
            ]
            bf_ids = bf_obj.search(self.cursor, self.uid, search_params)
            if bf_ids:
                for field in bf_obj.browse(self.cursor, self.uid, bf_ids):
                    if mode in field.update.split('/'):
                        custom_fields.append(field.name)
            else:
                # Si no trobem config per aquest model entenem que volem
                # sincronitzar tots els camps que es diuen igual en local
                # i en remot. Sense demanar els blacklisted
                if mode != 'never':
                    custom_fields = self.get_sync_fields()
        return custom_fields

    def get_blacklisted_fields(self):
        """Get blacklisted fields to not synchronize.
        """
        black_fields = self.get_custom_fields('never')
        black_fields.append('id')
        return black_fields

    def get_mapping_fields(self):
        """Get mapping fields to synchronize.
        """
        if not self.config:
            map_fields = {}
        else:
            dbname = self.cursor.dbname
            model = 'giscedata.comerdist.model.mapfields'
            bf_obj = pooler.get_pool(dbname).get(model)
            search_params = [
                ('model_id.name', '=', self.model),
                ('model_id.config_id.id', '=', self.config)
            ]
            bf_ids = bf_obj.search(self.cursor, self.uid, search_params)
            map_fields = {}
            for mf in bf_obj.browse(self.cursor, self.uid, bf_ids):
                map_fields[mf.src_name] = mf.dst_name
        return map_fields

    def get_local_fields(self, fields=None):
        """Get local fields definition.
        """
        local_obj = pooler.get_pool(self.cursor.dbname).get(self.model)
        return local_obj.fields_get(self.cursor, self.uid, fields)

    @property
    def local_fields(self):
        """List of local fields.
        """
        if not self.local_fields_list:
            self.local_fields_list = self.get_local_fields().keys()
        return self.local_fields_list

    def get_remote_fields(self):
        """Get remote fields definition.
        """
        return self.manager.fields_get()

    def get_remote_defaults(self, fields=None):
        """Get remote defaults.
        """
        if not fields:
            fields = self.get_remote_fields().keys()
        return self.manager.default_get(fields)

    def get_required_fields(self, object_id=False):
        remote_defs = self.get_remote_fields()
        defaults_defs = self.get_remote_defaults()
        required_fields = {}
        if object_id:
            obj = self.get_object(object_id)
        for field in remote_defs:
            field_name = remote_defs[field].get('string', field)
            if remote_defs[field].get('required', False):
                required_fields[field] = field_name
            if obj and obj.state in remote_defs[field].get('states', {}):
                for state in remote_defs[field]['states'][obj.state]:
                    if state[0] == 'required':
                        if state[1] and field not in required_fields:
                            required_fields[field] = field_name
                        else:
                            required_fields.pop(field, None)
            # Si té un valor per defecte és com si no fos obligatori
            # perquè ja l'agafarà
            if field in required_fields and field in defaults_defs:
                required_fields.pop(field, None)
        return required_fields

    @property
    def remote_fields(self):
        """List of remote fields.
        """
        if not self.remote_fields_list:
            self.remote_fields_list = self.get_remote_fields().keys()
        return self.remote_fields_list

    def get_sync_fields(self, mode=None):
        """Get the list of sync fields.
        """
        sync_fields = []
        sync_fields = list(set(self.local_fields) & set(self.remote_fields))
        if mode:
            sync_fields = list(set(self.get_custom_fields(mode)) &
                               set(sync_fields))
        return sync_fields

    def get_local_values(self, object_id, fields=None):
        """Get local values.
        """
        if not fields:
            fields = []
        local_obj = pooler.get_pool(self.cursor.dbname).get(self.model)
        vals = local_obj.read(self.cursor, self.uid, object_id, fields)
        # Convertim els many2one, s'ha de comprovar, perquè potser no fa falta
        # ja que després si és un many2one fem un altre objecte sync.
        for field, value in vals.items():
            if isinstance(value, type(())):
                vals[field] = value[0]
        return vals

    def get_search_params(self, object_id, match=None):
        """Return search_params for this Sync object.
        """
        cache_reference = '%s,%s' % (self.model, object_id)
        if cache_reference in self.conn.search_cache:
            return self.conn.search_cache[cache_reference]
        if not match:
            match = self.get_match_fields()
        local_match_fields = [x[0] for x in match]
        search_params = []
        l_vals = self.get_local_values(object_id)
        def_local_fields = self.get_local_fields(local_match_fields)
        for flm, frm in match:
            if flm in l_vals:
                if 'relation' in def_local_fields.get(flm, {}):
                    relation = Sync(self.cursor, self.uid, self.conn,
                                    def_local_fields[flm]['relation'],
                                    config=self.config)
                    for s_param in relation.get_search_params(l_vals[flm]):
                        s_param = (frm + '.' + s_param[0], '=', s_param[2])
                        search_params.append(s_param)
                else:
                    s_param = (frm, '=', l_vals[flm])
                    search_params.append(s_param)
        self.conn.search_cache[cache_reference] = search_params
        return search_params

    def get_nested_value(self, object_id, nested_fields):
        """Get value from a nested field.
        """
        dbname = self.cursor.dbname
        model_obj = pooler.get_pool(dbname).get(self.model)
        obj = model_obj.browse(self.cursor, self.uid, object_id)
        for field in nested_fields:
            obj = getattr(obj, field)
        return obj

    def get_object(self, object_id, match=None, active_test=True,
                   extra_search=[], double_test=False):
        """Retorna l'objecte remot per interactuar amb ell.
        @match: search params for searching remote object
        @extra_search: extra search params to add to match
        @active_test: If false, will search active and no active records
        @double_test: If true and active test set to False,
        search for active records first, and if no results, search no
        active records
        """
        remote_id = self.get_local_values(object_id,
                                        ['remote_id']).get('remote_id', False)
        if remote_id:
            try:
                res = self.conn.search(self.model, [('id', '=', remote_id)],
                                       0, False, False, {'active_test': False})
                if res:
                    return self.manager.get(remote_id)
                else:
                    raise
            except Exception, e:
                res = False
        search_params = self.get_search_params(object_id, match)
        search_params.extend(extra_search)
        context = {'active_test': active_test}
        #if double test and active_test set to False
        #firts search active records and if no results
        #search no active records later
        if double_test and not active_test:
            context = {'active_test': True}
            results = self.manager.search(search_params, 0,
                                          False, False, context)
            if not results:
                context = {'active_test': active_test}
                results = self.manager.search(search_params, 0,
                                          False, False, context)
        else:
            results = self.manager.search(search_params, 0,
                                          False, False, context)
        if len(results) > 1:
            raise osv.except_osv('Error',
                                 _(u"S'ha trobat més d'una coincidencia "
                                   u"amb %s i tipus %s.\n"
                                   u"Total: %i registres trobats.")
                                 % (search_params, self.model,
                                    len(results)))
        if results:
            obj = self.manager.get(results[0])
        else:
            obj = False
        return obj

    def sync(self, object_id, values=None, match=None, blacklisted=None,
             mapping=None, extra_search=[],
             active_test=True, double_test=False,
             already_sync=None):
        """Sync this model with remote source.
        """
        pool = pooler.get_pool(self.cursor.dbname)
        def_remote_fields = self.get_remote_fields()

        if not already_sync:
            already_sync = {}

        # Match fields
        if not match:
            # Default match is with name for local and name for remote
            # ('local_name', 'remote_name')
            match = self.get_match_fields()

        # Map fields
        if not mapping:
            mapping = {}
        mapping.update(self.get_mapping_fields())

        nested_fields = []
        read_nested_fields = []
        for map_key in mapping.keys():
            nested_f = map_key.split('.')
            if len(nested_f) > 1:
                nested_fields.append(map_key)
                read_nested_fields.append(nested_f[0])
        read_nested_fields = list(set(read_nested_fields))

        # Blacklisted fields
        if not blacklisted:
            blacklisted = []
        blacklisted += self.get_blacklisted_fields()

        # Setting up filters
        obj = self.get_object(object_id, match=match,
                              extra_search=extra_search,
                              active_test=active_test,
                              double_test=double_test)
        fields_to_remove = (set(blacklisted)
                            - set(mapping.keys())
                            - set(mapping.values()))
        if obj:
            mode = 'update'
            sync_fields = self.get_sync_fields(mode) + mapping.values()
            # Netejem els blacklisted fields
            # Afegim manualment el camp remote_id. Si no existeix
            # el read simplement no el retorna
            obj_fields = (list(set(sync_fields) - fields_to_remove)
                          + read_nested_fields + ['remote_id'])
            remote_values = obj.read(obj_fields)
        else:
            mode = 'create'
            sync_fields = self.get_sync_fields(mode) + mapping.values()
            # Netejem els blacklisted fields
            obj_fields = (list(set(sync_fields) - fields_to_remove)
                          + read_nested_fields + ['remote_id'])
            remote_values = {}
            # Arrepleguem tots els valors locals si no existeix
            # l'objecte remot per evitar perdues d'informacio
            values = None

        if not values:
            # Loading all values
            local_values = self.get_local_values(object_id, obj_fields)
            values = local_values.copy()

        # Mapping en diferents objectes
        for nested_string in nested_fields:
            nsearch = set(values.keys()) & set([nested_string.split('.')[0]])
            if nsearch:
                r_field = nested_string
                values[r_field] = self.get_nested_value(object_id,
                                                nested_string.split('.'))

        # Synching values
        synched_fields = {}
        for field, value in values.items():
            # Si aquest camp existeix renombrat no l'hem d'enviar, ja que
            # xafaria el camp que hem enviat abans
            if field in mapping.values():
                continue
            # Simplement canvien de nom al remot
            if field in mapping:
                field = mapping[field]
                # Si el camp renobrat està a la blacklist el treiem i així
                # es podrà sincronitzar el que hem renombrat.
                if field in blacklisted:
                    blacklisted.remove(field)
            if field in sync_fields and field not in blacklisted:
                # No sincronitzem one2manys
                if def_remote_fields[field]['type'] == 'one2many':
                    continue
                # Si no tenim valor en local i en remot, no cal fer res
                if not value and not remote_values.get(field, False):
                    continue
                if 'relation' in def_remote_fields.get(field, {}) and value:
                    related_model = def_remote_fields[field]['relation']
                    if not related_model in already_sync:
                        already_sync[related_model] = []
                    relation = Sync(self.cursor, self.uid, self.conn,
                                    related_model,
                                    config=self.config)
                    value_id = value
                    if value in already_sync[related_model]:
                        value = relation.get_object(value)
                    else:
                        value = relation.sync(value, already_sync=already_sync)
                        already_sync[related_model].append(value_id)
                    if field in remote_values and remote_values[field]:
                        if remote_values[field].id == value.id:
                            continue
                if 'relation' not in def_remote_fields.get(field, {}):
                    if field in remote_values:
                        if remote_values[field] == value:
                            continue
                synched_fields[field] = value
        #Escribim en remot el remote_id si cal
        if ('remote_id' in self.remote_fields
            and object_id != remote_values.get('remote_id', 0)):
            synched_fields['remote_id'] = object_id
        try:
            if synched_fields:
                if mode == 'update':
                    obj.write(synched_fields)
                elif mode == 'create':
                    obj = self.manager.create(synched_fields)
            #Escribim en local el remote_id si cal
            if 'remote_id' in self.local_fields:
                local_obj = pool.get(self.model).browse(self.cursor,
                                                    self.uid,
                                                    object_id)
                if local_obj.remote_id != obj.id:
                    local_obj.write({'remote_id': obj.id},
                                    {'sync': False})
        except Exception, exc:
            exc_message = _(u"Envieu aquesta informació "
                            u"al vostre proveïdor.\n")
            exc_message += u"-" * 80 + "\n"
            exc_message += _(u"Objecte: %s\n") % self.model
            if obj:
                exc_message += u"Id remot: %s\n" % obj.id
            exc_message += u"Transaction id: %s\n" % self.conn.transaction_id
            exc_message += u"-" * 80 + "\n"
            exc_message += u"Valors\n"
            for field_name, field_value in synched_fields.items():
                field_tag = (def_remote_fields and
                             def_remote_fields[field_name].get('string',
                                                               field_name))
                exc_message += "    - %s: %s\n" % (field_tag, field_value)
            exc_message += u"-" * 80 + "\n"
            exc_message += u"ERROR\n"
            exc_message += exc.faultCode + "\n"
            exc_message += u"-" * 80 + "\n"
            exc_message += u"STACKTRACE\n"
            exc_message += u"-" * 80 + "\n"
            exc_message += exc.faultString
            raise osv.except_osv(_(u'Error remot en sincronitzar'),
                                 exc_message)
        return obj


class GiscedataComerdistConfig(osv.osv):
    """Configuració del sistema de sincronització.
    """
    _name = 'giscedata.comerdist.config'

    def name_get(self, cr, user, ids, context=None):
        res = []
        for config in self.read(cr, user, ids, [], context):
            name = '%s:%s/%s' % (config['host'], config['port'],
                                 config['dbname'])
            res.append((config['id'], name))
        return res

    def name_search(self, cr, user, name, args=None, operator='ilike',
                    context=None, limit=80):
        if not args:
            args = []
        if not context:
            context = {}
        ids = []
        if not ids:
            ids = self.search(cr, user, [('dbname', operator, name)] + args,
                              limit=limit)
        return self.name_get(cr, user, ids, context=context)

    def default_active(self, cursor, uid, context):
        """Retorna el valor per defecte d'activa.
        """
        return 1

    _columns = {
        'host': fields.char('Host', size=256, required=True),
        'port': fields.integer('Port', required=True),
        'dbname': fields.char('Base de dades', size=256, required=True),
        'user': fields.char('Usuari', size=64, required=True),
        'password': fields.char('Contrassenya', size=64, required=True,
                                invisible=True),
        'user_local': fields.many2one('res.users', 'Usuari local',
                                      required=True, ondelete='restrict'),
        'partner_id': fields.many2one('res.partner', 'Pernter vinculat',
                                      requrired=True),
        'model_ids': fields.one2many('giscedata.comerdist.model', 'config_id',
                                     'Models'),
        'active': fields.boolean('Activa', select=True),
    }

    _sql_constraints = [('partner_id_uniq', 'unique (partner_id)',
                         'Ja existeix una configuració per aquest partner.')]

    _defaults = {
        'active': default_active,
    }

GiscedataComerdistConfig()


class GiscedataComerdistLog(osv.osv):
    """Log pel sistema de sincronització.
    """

    _name = 'giscedata.comerdist.log'

    def create(self, cursor, user, vals, context=None):
        """Sobreescrivim el create per si volem fer debug amb el netsvc.logger.
        """
        res_id = super(GiscedataComerdistLog, self).create(cursor, user, vals,
                                                           context)
        return res_id

    _columns = {
        'name': fields.datetime('Log date'),
        'log': fields.text('Log value')
    }

    _defaults = {
        'name': lambda *a: time.strftime('%Y-%m-%d %H:%M:%S'),
    }

GiscedataComerdistLog()


class GiscedataComerdistModel(osv.osv):
    """Model de sincronització.
    """
    _name = 'giscedata.comerdist.model'

    def get_all_sync_fields(self, cursor, uid, ids, context=None):
        """Get all fields to sync.
        """
        for model in self.browse(cursor, uid, ids, context):
            sync_fields = {}
            config = model.config_id
            ooop = OOOPPool.get_ooop(cursor, uid, config.id)
            sync = Sync(cursor, uid, ooop, model.name, config=config.id)
            def_remote_fields = sync.get_remote_fields()
            for field in sync.get_sync_fields():
                sync_fields[field] = def_remote_fields[field]
            return sync_fields

    def button_write_sync_to_custom(self, cursor, uid, ids, context=None):
        """Writes all fields to sync into custom sync list.
        """
        synclisted_obj = self.pool.get('giscedata.comerdist.model.syncfields')
        for model in self.browse(cursor, uid, ids, context):
            existing_fields = {}
            for field in model.sync_fields:
                existing_fields[field.name] = field.id
            fields = model.get_all_sync_fields()
            sync_fields = self.get_all_sync_fields(cursor, uid, ids, context)
            for field in sync_fields:
                field_name = fields[field].get('string', field)
                field_type = fields[field].get('type', '')
                if field not in existing_fields:
                    synclisted_obj.create(cursor, uid, {'name': field,
                                                        'string': field_name,
                                                        'type': field_type,
                                                        'model_id': model.id})
                else:
                    synclisted_obj.write(cursor, uid, existing_fields[field],
                                         {'string': field_name,
                                          'type': field_type})
        return True

    def _sync_match_fields(self, cursor, uid, ids, context=None):
        """Comprovem que els match_fields que són als
        sync fields no estiguin configurats com a never
        """

        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        msg = ''
        for config in self.browse(cursor, uid, ids):
            sync_fields = dict([(x.name, x.update)
                                for x in config.sync_fields])
            for field in config.match_fields:
                if (field.src_name in sync_fields and
                    sync_fields[field.src_name] == 'never'):
                    msg += (_(u"Camp '%s' del model '%s'\n") %
                            (field.src_name, config.name))
        if msg:
            msg = _(u"Els següents camps de recerca "
                    u"no estan marcats per sincronitzar:\n") + msg
            raise osv.except_osv('Error', msg)

    def create(self, cursor, uid, values, context=None):

        id = super(GiscedataComerdistModel,
                    self).create(cursor, uid, values, context=context)

        self._sync_match_fields(cursor, uid, [id], context)
        return id

    def write(self, cursor, uid, ids, values, context=None):
        """Comprovem que tots els match_fields son als
        sync fields i no estan configurats com a never
        """

        res = super(GiscedataComerdistModel,
                    self).write(cursor, uid, ids, values, context=context)

        self._sync_match_fields(cursor, uid, ids, context)
        return res

    _columns = {
        'config_id': fields.many2one('giscedata.comerdist.config', 'Config',
                                     required=True, ondelete='cascade'),
        'name': fields.char('Local model', required=True, size=64,
                            search=True),
        'match_fields': fields.one2many('giscedata.comerdist.model.matchfields',
                                        'model_id', 'Camps per fer match'),
        'sync_fields': fields.one2many('giscedata.comerdist.model.syncfields',
                                    'model_id', 'Camps per no sincronitzar'),
        'map_fields': fields.one2many('giscedata.comerdist.model.mapfields',
                                      'model_id', 'Camps amb canvi de nom')
    }

    _order = 'name asc'

    _sql_constraints = [('name_uniq', 'unique (name)',
                         'Aquest model ja existex')]

GiscedataComerdistModel()


class GiscedataComerdistModelMatchfields(osv.osv):
    """Fields per fer match en la sincronització.
    """
    _name = 'giscedata.comerdist.model.matchfields'

    _columns = {
        'src_name': fields.char('Nom local', size=64, required=True),
        'dst_name': fields.char('Nom remot', size=64, required=True),
        'model_id': fields.many2one('giscedata.comerdist.model', 'Model',
                                    required=True, ondelete='cascade')
    }

    _order = "src_name asc"

GiscedataComerdistModelMatchfields()


class GiscedataComerdistModelSyncfields(osv.osv):
    """Fields que no s'han de sincronitzar.
    """
    _name = 'giscedata.comerdist.model.syncfields'

    def default_update(self, cursor, uid, context=None):
        """Obté el valor per defecte de l'update.
        """
        return 'never'

    _update_selection = [('never', 'Never'),
                         ('create', 'Create'),
                         ('update', 'Update'),
                         ('create/update', 'Always')]

    _columns = {
        'name': fields.char('Nom del camp', size=64, required=True),
        'update': fields.selection(_update_selection, 'Sync', required=True),
        'string': fields.char('String', size=64, readonly=True),
        'type': fields.char('Tipus', size=64, readonly=True),
        'model_id': fields.many2one('giscedata.comerdist.model', 'Model',
                                    required=True, ondelete='cascade')
    }

    _defaults = {
        'update': default_update,
    }

    _order = "name asc"

GiscedataComerdistModelSyncfields()


class GiscedataComerdistModelMapfields(osv.osv):
    """Fields per sincronitzar que es diuen diferent localment i remotament.
    """
    _name = 'giscedata.comerdist.model.mapfields'

    _columns = {
        'src_name': fields.char('Nom local', size=64, required=True),
        'dst_name': fields.char('Nom remot', size=64, required=True),
        'model_id': fields.many2one('giscedata.comerdist.model', 'Model',
                                    required=True, ondelete='cascade')
    }

    _order = "src_name asc"

GiscedataComerdistModelMapfields()
