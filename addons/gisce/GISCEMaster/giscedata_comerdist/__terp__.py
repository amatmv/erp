# -*- coding: utf-8 -*-
{
    "name": "Comerdist (base)",
    "description": """
    This module provide :
      * Sincronització entre Comercialitzadora i Distribuidora
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "ir.model.access.csv",
        "giscedata_comerdist_view.xml",
        "giscedata_comerdist_data.xml",
        "security/giscedata_comerdist_security.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
