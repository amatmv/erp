# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* giscedata_comerdist
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.14\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2018-06-22 06:23\n"
"PO-Revision-Date: 2018-06-22 06:23\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: giscedata_comerdist
#: model:ir.model,name:giscedata_comerdist.model_giscedata_comerdist_model_matchfields
msgid "giscedata.comerdist.model.matchfields"
msgstr ""

#. module: giscedata_comerdist
#: view:giscedata.comerdist.model:0
#: view:giscedata.comerdist.model.syncfields:0
msgid "Sync fields"
msgstr ""

#. module: giscedata_comerdist
#: field:giscedata.comerdist.log,log:0
msgid "Log value"
msgstr ""

#. module: giscedata_comerdist
#: model:ir.model,name:giscedata_comerdist.model_giscedata_comerdist_model_syncfields
msgid "giscedata.comerdist.model.syncfields"
msgstr ""

#. module: giscedata_comerdist
#: model:ir.model,name:giscedata_comerdist.model_giscedata_comerdist_model
msgid "giscedata.comerdist.model"
msgstr ""

#. module: giscedata_comerdist
#: code:addons/giscedata_comerdist/giscedata_comerdist.py:598
#, python-format
msgid "Envieu aquesta informació al vostre proveïdor.\n"
""
msgstr ""

#. module: giscedata_comerdist
#: field:giscedata.comerdist.model,sync_fields:0
msgid "Camps per no sincronitzar"
msgstr ""

#. module: giscedata_comerdist
#: selection:giscedata.comerdist.model.syncfields,update:0
msgid "Create"
msgstr ""

#. module: giscedata_comerdist
#: view:giscedata.comerdist.model:0
msgid "Comerdist model"
msgstr ""

#. module: giscedata_comerdist
#: view:giscedata.comerdist.config:0
msgid "Configuració Comerdist"
msgstr ""

#. module: giscedata_comerdist
#: constraint:ir.actions.act_window:0
msgid "Invalid model name in the action definition."
msgstr ""

#. module: giscedata_comerdist
#: constraint:ir.model:0
msgid "The Object name must start with x_ and not contain any special character !"
msgstr ""

#. module: giscedata_comerdist
#: view:giscedata.comerdist.model.syncfields:0
msgid "Blacklisted fields"
msgstr ""

#. module: giscedata_comerdist
#: field:giscedata.comerdist.model,config_id:0
msgid "Config"
msgstr ""

#. module: giscedata_comerdist
#: field:giscedata.comerdist.model.mapfields,model_id:0
#: field:giscedata.comerdist.model.matchfields,model_id:0
#: field:giscedata.comerdist.model.syncfields,model_id:0
msgid "Model"
msgstr ""

#. module: giscedata_comerdist
#: field:giscedata.comerdist.log,name:0
msgid "Log date"
msgstr ""

#. module: giscedata_comerdist
#: field:giscedata.comerdist.config,user:0
msgid "Usuari"
msgstr ""

#. module: giscedata_comerdist
#: field:giscedata.comerdist.model.syncfields,string:0
msgid "String"
msgstr ""

#. module: giscedata_comerdist
#: code:addons/giscedata_comerdist/giscedata_comerdist.py:765
#, python-format
msgid "Els següents camps de recerca no estan marcats per sincronitzar:\n"
""
msgstr ""

#. module: giscedata_comerdist
#: view:giscedata.comerdist.config:0
#: field:giscedata.comerdist.config,model_ids:0
msgid "Models"
msgstr ""

#. module: giscedata_comerdist
#: selection:giscedata.comerdist.model.syncfields,update:0
msgid "Never"
msgstr ""

#. module: giscedata_comerdist
#: model:ir.model,name:giscedata_comerdist.model_giscedata_comerdist_log
msgid "giscedata.comerdist.log"
msgstr ""

#. module: giscedata_comerdist
#: selection:giscedata.comerdist.model.syncfields,update:0
msgid "Update"
msgstr ""

#. module: giscedata_comerdist
#: field:giscedata.comerdist.config,dbname:0
msgid "Base de dades"
msgstr ""

#. module: giscedata_comerdist
#: view:giscedata.comerdist.model:0
#: view:giscedata.comerdist.model.mapfields:0
msgid "Renamed fields"
msgstr ""

#. module: giscedata_comerdist
#: field:giscedata.comerdist.config,host:0
msgid "Host"
msgstr ""

#. module: giscedata_comerdist
#: field:giscedata.comerdist.model.syncfields,name:0
msgid "Nom del camp"
msgstr ""

#. module: giscedata_comerdist
#: view:giscedata.comerdist.config:0
msgid "Parametres de sincronització"
msgstr ""

#. module: giscedata_comerdist
#: code:addons/giscedata_comerdist/giscedata_comerdist.py:619
#, python-format
msgid "Error remot en sincronitzar"
msgstr ""

#. module: giscedata_comerdist
#: field:giscedata.comerdist.config,active:0
msgid "Activa"
msgstr ""

#. module: giscedata_comerdist
#: model:ir.module.module,shortdesc:giscedata_comerdist.module_meta_information
msgid "Comerdist (base)"
msgstr ""

#. module: giscedata_comerdist
#: view:giscedata.comerdist.config:0
msgid "Paràmetres connexió"
msgstr ""

#. module: giscedata_comerdist
#: constraint:ir.ui.view:0
msgid "Invalid XML for View Architecture!"
msgstr ""

#. module: giscedata_comerdist
#: field:giscedata.comerdist.model,match_fields:0
msgid "Camps per fer match"
msgstr ""

#. module: giscedata_comerdist
#: selection:giscedata.comerdist.model.syncfields,update:0
msgid "Always"
msgstr ""

#. module: giscedata_comerdist
#: model:ir.model,name:giscedata_comerdist.model_giscedata_comerdist_config
msgid "giscedata.comerdist.config"
msgstr ""

#. module: giscedata_comerdist
#: field:giscedata.comerdist.model,map_fields:0
msgid "Camps amb canvi de nom"
msgstr ""

#. module: giscedata_comerdist
#: code:addons/giscedata_comerdist/giscedata_comerdist.py:601
#, python-format
msgid "Objecte: %s\n"
""
msgstr ""

#. module: giscedata_comerdist
#: view:giscedata.comerdist.model:0
#: view:giscedata.comerdist.model.matchfields:0
msgid "Match fields"
msgstr ""

#. module: giscedata_comerdist
#: field:giscedata.comerdist.config,partner_id:0
msgid "Pernter vinculat"
msgstr ""

#. module: giscedata_comerdist
#: model:ir.model,name:giscedata_comerdist.model_giscedata_comerdist_model_mapfields
msgid "giscedata.comerdist.model.mapfields"
msgstr ""

#. module: giscedata_comerdist
#: field:giscedata.comerdist.config,password:0
msgid "Contrassenya"
msgstr ""

#. module: giscedata_comerdist
#: field:giscedata.comerdist.config,user_local:0
msgid "Usuari local"
msgstr ""

#. module: giscedata_comerdist
#: code:addons/giscedata_comerdist/giscedata_comerdist.py:762
#, python-format
msgid "Camp '%s' del model '%s'\n"
""
msgstr ""

#. module: giscedata_comerdist
#: model:ir.ui.menu,name:giscedata_comerdist.menu_comerdist_main
msgid "Comerdist"
msgstr ""

#. module: giscedata_comerdist
#: field:giscedata.comerdist.model.mapfields,dst_name:0
#: field:giscedata.comerdist.model.matchfields,dst_name:0
msgid "Nom remot"
msgstr ""

#. module: giscedata_comerdist
#: field:giscedata.comerdist.model.syncfields,update:0
msgid "Sync"
msgstr ""

#. module: giscedata_comerdist
#: model:ir.module.module,description:giscedata_comerdist.module_meta_information
msgid "\n"
"    This module provide :\n"
"      * Sincronització entre Comercialitzadora i Distribuidora\n"
"    "
msgstr ""

#. module: giscedata_comerdist
#: view:giscedata.comerdist.model:0
msgid "Get all sync fields"
msgstr ""

#. module: giscedata_comerdist
#: code:addons/giscedata_comerdist/giscedata_comerdist.py:448
#, python-format
msgid "S'ha trobat més d'una coincidencia amb %s i tipus %s.\n"
"Total: %i registres trobats."
msgstr ""

#. module: giscedata_comerdist
#: field:giscedata.comerdist.model.syncfields,type:0
msgid "Tipus"
msgstr ""

#. module: giscedata_comerdist
#: field:giscedata.comerdist.model,name:0
msgid "Local model"
msgstr ""

#. module: giscedata_comerdist
#: field:giscedata.comerdist.model.mapfields,src_name:0
#: field:giscedata.comerdist.model.matchfields,src_name:0
msgid "Nom local"
msgstr ""

#. module: giscedata_comerdist
#: field:giscedata.comerdist.config,port:0
msgid "Port"
msgstr ""

#. module: giscedata_comerdist
#: model:ir.actions.act_window,name:giscedata_comerdist.action_comerdist_config_tree
#: model:ir.ui.menu,name:giscedata_comerdist.menu_comerdist_config
#: model:ir.ui.menu,name:giscedata_comerdist.menu_comerdist_config_tree
msgid "Configuració"
msgstr ""

