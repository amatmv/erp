# -*- coding: utf-8 -*-
"""Com que l'usuari sync no es creava automàticament, a partir de la v2.6.0 sí.
Hi ha un forcecreate="1" en l'xml que el crea, això vol dir que si no el tenim
a la taula ir.model.data ens el crearía duplicat.

Aquest script el crearà per tal que no el torni a crear
"""

import netsvc
import pooler

def migrate(cursor, installed_version):
    uid = 1
    logger = netsvc.Logger()
    cursor.execute("SELECT id from res_users where id = %s" % (uid,))
    if not cursor.fetchall():
        logger.notifyChannel('migration', netsvc.LOG_ERROR,
                             'No existeix uid:%s (superusuari).' % uid)
        raise Exception("No existeix uid:%s" % uid)
    pool = pooler.get_pool(cursor.dbname)
    user_obj = pool.get('res.users')
    ids = user_obj.search(cursor, uid, [('login', '=', 'sync')],
                          context={'active_test': False})
    model_data_obj = pool.get('ir.model.data')
    data_ids = model_data_obj.search(cursor, uid,
                                     [('name', '=', 'sync_user'),
                                      ('module', '=', 'giscedata_comerdist')])
    if ids and not data_ids:
        vals = {
            'name': 'sync_user',
            'model': 'res.users',
            'module': 'giscedata_comerdist',
            'res_id': ids[0],
            'noupdate': 1,
        }
        model_data_obj.create(cursor, uid, vals)
