# -*- coding: utf-8 -*-

from osv import osv, fields

class ResMunicipi(osv.osv):
    """Municipis + Codi INE."""
    _name = 'res.municipi'
    _inherit = 'res.municipi'

    _columns = {
        'active': fields.boolean('Actiu')
    }

    _defaults = {
        'active': lambda *a: 1,
    }

ResMunicipi()
