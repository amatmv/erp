# -*- coding: utf-8 -*-

from osv import osv,fields

class giscedata_cups_escomesa(osv.osv):

    _name = 'giscedata.cups.escomesa'
    _inherit = 'giscedata.cups.escomesa'

    def read_distinct(self, cr, uid, values, id_municipi):
        cr.execute("select e.name,e.id, e.id_municipi as id_municipi from giscedata_cups_escomesa e where e.id_municipi = %i and e.name::int not in ("+','.join(map(str, map(int, values)))+")", (id_municipi,))
        return cr.dictfetchall()

    def read_distinct2(self, cr, uid, values, id_municipi):
        in_array = []
        for value in values:
            cr.execute("select name from giscedata_cups_escomesa where name = %s and id_municipi = %i", (str(value), id_municipi))
            if not cr.fetchone():
                in_array.append(value)
        return in_array

    _columns = {
      'id_municipi': fields.many2one('res.municipi', 'Municipi', required=True),
    }

giscedata_cups_escomesa()
