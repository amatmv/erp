# -*- coding: utf-8 -*-
{
    "name": "GISCE Data CUPS CAD",
    "description": """Funcions per les eines CAD als CUPS""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "CAD",
    "depends":[
        "base",
        "base_extended",
        "giscedata_cups",
        "giscedata_cups_distri"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_cups_distri_view.xml"
    ],
    "active": False,
    "installable": True
}
