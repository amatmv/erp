# -*- coding: utf-8 -*-
{
    "name": "Giscedata Lectures PDA per rutes virtuals",
    "description": """Giscedata Lectures PDA per rutes virtuals""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_lectures_pda",
        "giscedata_facturacio",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
      "wizard/wizard_export_view.xml",
    ],
    "active": False,
    "installable": True
}
