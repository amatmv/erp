# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _
from base64 import b64decode, b64encode
import csv
from StringIO import StringIO


class GiscedataLecturesPdaExport(osv.osv_memory):
    _name = 'giscedata.lectures.pda.export'
    _inherit = 'giscedata.lectures.pda.export'

    def read_cups_file(self, cursor, uid, ids, context=None):
        """
        Overwrite action_export_date method on parent wizard to allows user
        to load cups id's from a file.
        :param cursor: Database cursor
        :param uid: User id
        :param ids: wizard ids
        :param context:
        :return: int list with cups ids
        """
        cups_obj = self.pool.get('giscedata.cups.ps')

        wizard = self.browse(cursor, uid, ids[0])
        cupses_file = wizard.cups_file
        res = []
        if cupses_file:
            cups_file = StringIO(b64decode(cupses_file))
            reads = csv.reader(cups_file, delimiter=';')
            for cups in reads:
                try:
                    res.append(int(cups[0]))
                except TypeError as e:
                    pass
                except ValueError as e:
                    try:
                        cups_id = cups_obj.search(
                            cursor, uid, [('name', 'like', cups[0])]
                        )[0]
                        res.append(cups_id)
                    except Exception as e:
                        pass
            cups_file.close()
        else:
            raise osv.except_osv(
                _('ERROR'),
                _('El fichero cups no esta cargado')
            )
        return res

    def action_export_data(self, cursor, uid, ids, context=None):
        """
        Overwrite action_export_date method on parent wizard to allows user
        to load cups id's from a file.
        :param cursor: Database cursor
        :param uid: User id
        :param ids: wizard ids
        :param context:
        :return:
        """
        res = super(GiscedataLecturesPdaExport, self).action_export_data(
            cursor, uid, ids, context=context
        )

        file_cups = self.read(
            cursor, uid, ids, ['import_model', 'status'], context=context
        )[0]

        if file_cups['import_model'] == 'c':
            status = file_cups['status'].replace(
                'False.ruta', 'ruta_virtual.ruta'
            )
            self.write(cursor, uid, ids, {
                'name': 'ruta_virtual.ruta',
                'status': status
            }, context=context)
        return res

    def get_cups_ids(self, cursor, uid, ids, zona, context=None):
        imp_file = self.read(
            cursor, uid, ids, ['import_model'], context=context
        )[0]['import_model']

        # If imp from file
        if imp_file == 'c':
            return self.read_cups_file(cursor, uid, ids, context=context)
        else:
            return super(GiscedataLecturesPdaExport, self).get_cups_ids(
                cursor, uid, ids, zona, context=context)

    _columns = {
        'import_model': fields.selection(
            [('z', 'Per zona'), ('c', 'Fitxer de CUPS')],
            'Mode de generació'
        ),
        'cups_file': fields.binary(
            'Fitxer cups',
            help=u'Fitxer amb els CUPS ordenats. \n'
                 u' * Si el primer registre comença per ES , es consideren '
                 u'CUPS\n'
                 u' * Si el primer registre NO comença per ES, ids de CUPS de '
                 u'   la base de dades\n'
        ),
    }

    _defaults = {
        'import_model': lambda *a: 'z',
    }

GiscedataLecturesPdaExport()
