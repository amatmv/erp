# -*- coding: utf-8 -*-
{
    "name": "Pagos por capacidad Gener 2016",
    "description": """
  Actualització de les tarifes de peatges segons el BOE nº 302 - 18/12/2015.
  IET/2735/2015
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "giscedata_polissa",
        "giscedata_pagos_capacidad"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_tarifas_pagos_capacidad_20160101_data.xml"
    ],
    "active": False,
    "installable": True
}
