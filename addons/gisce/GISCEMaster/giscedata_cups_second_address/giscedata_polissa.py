# -*- coding: utf-8 -*-

from osv import osv, fields


class GiscedataPolissaCups(osv.osv):

    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    _columns = {
        'cups_act_direccio': fields.related('cups', 'act_direccio',
                                   type='char',
                                   relation='giscedata.cups.ps',
                                   string="Direcció Act. CUPS", store=False,
                                   readonly=True, size=256),
    }

GiscedataPolissaCups()
