# -*- coding: utf-8 -*-
{
    "name": "GISCE Data CUPS. Secondary address",
    "description": """Creates new secondary address for maintaining the one appearing in the butlleti, 
  and another one for reflecting changes since the butlleti was done.""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "CUPS",
    "depends":[
        "giscedata_cups",
        "giscedata_polissa"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_cups_view.xml",
        "giscedata_polissa_view.xml",
        "wizard/wizard_config_second_address_view.xml"
    ],
    "active": False,
    "installable": True
}
