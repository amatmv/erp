# -*- coding: utf-8 -*-
from osv import osv, fields
import netsvc


class ConfigCupsSecondAddress(osv.osv_memory):

    _name = 'wizard.config.cups.second.address'

    def set_second_address(self, cursor, uid, ids, context=None):
        """Copy address from principal to secondary
        """
        wizard = self.browse(cursor, uid, ids[0])
        cups_obj = self.pool.get('giscedata.cups.ps')
        uid = 1
        logger = netsvc.Logger()
        cups_ids = cups_obj.search(cursor, uid, [])
        logger.notifyChannel('cups_second_address', netsvc.LOG_INFO, 
                             'Updating act_direccio in %s CUPS' % len(cups_ids))
        cups_obj.copy_from_principal(cursor, uid, cups_ids,
                                     {'sync': False})
        # Set notes to null because migration produces logs
        cups_obj.write(cursor, uid, cups_ids, {'notes': False},
                       {'sync': False})
        logger.notifyChannel('cups_second_address', netsvc.LOG_INFO, 'Done.')

        return {'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'ir.actions.configuration.wizard',
                'type': 'ir.actions.act_window',
                'target':'new',
                }

ConfigCupsSecondAddress()
