# -*- coding: utf-8 -*-

from osv import osv, fields


class GiscedataCupsSecondAddress(osv.osv):
    """Classe d'un CUPS (Punt de servei)."""

    _name = 'giscedata.cups.ps'
    _inherit = 'giscedata.cups.ps'

    def create(self, cursor, uid, values, context=None):
        '''copy principal address to secondary if
        no value for secondary address'''
        
        cups_id = super(GiscedataCupsSecondAddress,
                    self).create(cursor, uid, values, context=context)
        if not context:
            context = {}
        context.update({'log': False})
        act_nv = self.read(cursor, uid, cups_id)['act_nv']
        #if no street name in secondary, copy from principal
        if not act_nv:
            self.copy_from_principal(cursor, uid, [cups_id],
                                     context=context)
        return cups_id

    def copy_from_principal(self, cursor, uid, ids, context=None):
        """Copia la direcció del principal a l'actual."""
        fields_to_read = ['tv', 'nv', 'pnp', 'es', 'pt',
                          'pu', 'cpo', 'cpa', 'aclarador']
        for cups in self.read(cursor, uid, ids, fields_to_read, context):
            cups_id = cups['id']
            del cups['id']
            vals = {}
            for key, value in cups.items():
                if isinstance(value, (list, tuple)):
                    value = value[0]
                vals['act_%s' % key] = value
            self.write(cursor, uid, cups_id, vals, context)

    def get_direccio_fields(self, cursor, uid, field_name, context=None):
        '''return fields for constructing direccio depending on field_name'''
        fields = {}
        if field_name == 'act_direccio':
            fields['fields_normal'] = ['act_nv', 'act_pnp', 'act_es', 'act_pt',
                                       'act_pu', 'act_aclarador', 'dp']
            fields['fields_poligon'] = ['act_cpo', 'act_cpa']
            fields['fields_all'] = ['act_tv', 'id_municipi', 'id_poblacio']
            fields['field_tv'] = 'act_tv'
        else:
            fields = super(GiscedataCupsSecondAddress,
                           self).get_direccio_fields(cursor, uid,
                                                     field_name,
                                                     context=context)
        return fields

    def _direccio(self, cursor, uid, ids, field_name, arg, context=None):

        return super(GiscedataCupsSecondAddress, self)._direccio(cursor, uid,
                                                            ids, field_name,
                                                            arg, context=context)

    _columns = {
        'act_tv': fields.many2one('res.tipovia', 'Tipus Via'),
        'act_nv': fields.char('Carrer', size=256),
        'act_pnp': fields.char('Número', size=9),
        'act_es': fields.char('Escala', size=4),
        'act_pt': fields.char('Planta', size=4),
        'act_pu': fields.char('Porta', size=4),
        'act_cpo': fields.char('Poligon', size=4),
        'act_cpa': fields.char('Parcel·la', size=4),
        'act_aclarador': fields.char('Aclarador', size=256),
        'act_direccio': fields.function(_direccio, method=True,
                                        string="Direcció Actual",
                                    store={'giscedata.cups.ps':
                                       (lambda self, cr, uid, ids, c=None: ids,
                                       ['act_tv', 'act_nv', 'act_pnp',
                                        'act_es', 'act_pt', 'act_pu',
                                        'act_cpo', 'act_cpa', 'dp',
                                        'id_municipi', 'id_poblacio',
                                        'act_aclarador'], 10)},
                                            type='char', size=256),
    }

GiscedataCupsSecondAddress()
