# -*- coding: utf-8 -*-
{
    "name": "giscedata_bt_fronteres",
    "description": """Model pels fusibles frontera de BT""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Baixa tensió",
    "depends":[
        "base",
        "base_extended",
        "giscedata_bt"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "ir.model.access.csv",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
