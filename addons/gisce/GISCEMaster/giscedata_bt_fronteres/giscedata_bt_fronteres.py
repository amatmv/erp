# -*- coding: utf-8 -*-

from osv import osv, fields

class giscedata_bt_fronteres_blockname(osv.osv):
  _name = 'giscedata.bt.fronteres.blockname'
  _description = 'Blocknames per les fronteres BT'

  def create(self, cr, uid, vals, context={}):
    if not vals.has_key('code'):
      cr.execute("SELECT code from giscedata_bt_fronteres_blockname ORDER BY code DESC LIMIT 1")
      codi = cr.fetchone()
      if codi and len(codi):
        vals['code'] = int(codi[0]) + 1
      else:
        vals['code'] = 1
    return super(osv.osv, self).create(cr, uid, vals, context)

  _columns = {
    'name': fields.char('Blockname', size=32, required=True),
    'description': fields.char('Descripció', size=255),
    'code': fields.integer('Codi'),
  }

  _defaults = {

  }

  _order = "name, id"

giscedata_bt_fronteres_blockname()


class giscedata_bt_fronteres(osv.osv):
  _name = 'giscedata.bt.fronteres'
  _description = 'Model pels fusibles frontera de BT'

  def _get_nextnumber(self, cr, uid, context={}):
    cr.execute("select coalesce(max(name::int),0)+1 from giscedata_bt_fronteres")
    return str(cr.fetchone()[0])

  _columns = {
    'name': fields.char('Codi', size=8, required=True),
    'id_municipi': fields.many2one('res.municipi', 'Municipi'),
    'blockname': fields.many2one('giscedata.bt.fronteres.blockname', 'Blockname'),
  }

  _defaults = {
    'name': _get_nextnumber, 
  }

giscedata_bt_fronteres()

