# -*- coding: utf-8 -*-
{
    "name": "Butlletins",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Informació a butlletins i les vincula a un CUPS/pòlissa
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "giscedata_cups",
        "giscedata_polissa"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_butlletins_view.xml",
        "giscedata_cups_view.xml",
        "giscedata_polissa_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
