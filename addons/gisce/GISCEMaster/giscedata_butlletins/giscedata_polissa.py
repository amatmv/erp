# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscedataPolissa(osv.osv):
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def copy(self, cursor, uid, id, default=None, context=None):

        if not default:
            default = {}

        default.update({'butlletins': []})
        res_id = super(GiscedataPolissa, self).copy(
            cursor, uid, id, default, context
        )
        return res_id

    def _ff_butlletins(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        res = {}
        # TODO: Improve performance
        for polissa in self.browse(cursor, uid, ids, context=context):
            res.setdefault(polissa.id, [])
            cups = polissa.cups
            if cups and cups.butlletins:
                res[polissa.id].extend([b.id for b in cups.butlletins])
        return res

    def _ff_inv_butlletins(
            self, cursor, uid, ids, name, value, arg, context=None):
        if not context:
            context = {}
        if not isinstance(ids, list):
            ids = [ids]
        one2many = fields.one2many('giscedata.butlleti', 'cups_id')
        cups_obj = self.pool.get('giscedata.cups.ps')
        for polissa in self.read(cursor, uid, ids, ['cups']):
            if polissa['cups']:
                one2many.set(
                    cursor, cups_obj, polissa['cups'][0], False, value,
                    user=uid, context=context
                )

    _columns = {
        'butlletins': fields.function(
            _ff_butlletins, type='one2many', relation='giscedata.butlleti',
            string='Butlletins', method=True, fnct_inv=_ff_inv_butlletins
        )
    }

GiscedataPolissa()
