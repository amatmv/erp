import netsvc


def migrate(cursor, installed_version):
    """
    Migration function that alters the precision of the potencia adscrita
    field

    :param cursor: Database cursor
    :param installed_version: Actual version of the PowerERP
    :return: None
    """

    logger = netsvc.Logger()
    logger.notifyChannel(
        'migrations',
        netsvc.LOG_INFO,
        "Canviem la precisio del camp potencia adscrita"
    )

    sql_update = """
    ALTER TABLE giscedata_cups_ps 
    ALTER COLUMN potencia_adscrita TYPE DECIMAL(15,3);
    """
    cursor.execute(sql_update)

    logger.notifyChannel(
        'migration',
        netsvc.LOG_INFO,
        "Fet"
    )


up = migrate
