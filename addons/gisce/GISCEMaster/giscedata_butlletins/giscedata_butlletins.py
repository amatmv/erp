# -*- encoding: utf-8 -*-

from osv import osv, fields

# TODO: crear mòdul giscedata_butlleti_cat per afegir els camps de Catalunya.


class GiscedataButlletiActivitat(osv.osv):
    """Activitat d'ús que marca el butlletí.
    """
    _name = 'giscedata.butlleti.activitat'
    _description = 'Activitat del butlletí'
    _order = 'name asc'
    _columns = {
        'name': fields.char('Activitat', size=256, required=True)
    }
    _sql_constraints = [('name_uniq', 'unique (name)',
                         'Aquesta activitat ja existeix')]

GiscedataButlletiActivitat()

DADES_INSTALLACIO = [('nova', 'Nova'),
                     ('modificacio', 'Modificació'),
                     ('ampliacio', 'Ampliació')]


class GiscedataButlleti(osv.osv):
    """Classe per modelar un butlletí d'instal.lador
    """
    _name = 'giscedata.butlleti'
    _description = __doc__
    _order = 'data_vigencia desc'  # Butlletins wiht newest date first

    def _get_cups(self, cursor, uid, context=None):

        if not context:
            context = {}
        return context.get('cups_id', False)

    def _has_contract(self, cursor, uid, ids, field_name, arg, context= None):
        res = {}

        if not context:
            context = {}

        for butlleti in self.browse(cursor, uid, ids, context=context):
            res.setdefault(butlleti.id, False)
            cups = butlleti.cups_id
            if cups:
                active_contract = cups.polissa_polissa
                if active_contract:
                    res[butlleti.id] = True

        return res

    def _has_contract_search(self, cr, uid, obj, name, args, context=None):

        if not context:
            context = {}
        return [('cups_id.polissa_polissa', '!=', False)]

    _columns = {
        'name': fields.char('Nº Expedient', size=32, required=True),
        'partner_id': fields.many2one('res.partner', 'Empresa instal·ladora'),
        'partner_address_id': fields.many2one('res.partner.address', 'Adreça '
                                              'empresa instal·ladora'),
        'installador_id': fields.many2one('res.partner', 'Instal·lador'),
        'dades_installacio': fields.selection(DADES_INSTALLACIO,
                                              'Tipus instal·lació'),
        'activitat_us': fields.many2one('giscedata.butlleti.activitat',
                                        'Ús a què es destina', size=256),
        'pot_max_admisible': fields.float('Pot. Màx. Admisible',
                                          digits=(16, 3)),
        'pot_installada': fields.float('Potència instal·lada', digits=(16, 3)),
        'tensio': fields.integer('Tensió (V)'),
        'resistencia_terra': fields.float(u'Resistència del terra '
                                          u'de protecció'),
        'resistencia_aillament': fields.float('Resistència d\'aïllament'),
        'interruptor_diferencial': fields.float('Intensitat Int. Diferencial'),
        'derivacio_individual': fields.char('Secció derivació individual',
                                            size=256),
        'cups_id': fields.many2one('giscedata.cups.ps', 'CUPS', size=40,
                                      required=True, ondelete='cascade'),
        'data_provisional_obra': fields.date('Data provisional obra'),
        'data': fields.date('Data del butlletí'),
        'data_vigencia': fields.date('Data de vigència'),
        'numero_installacio': fields.char('Número d\'instal·lació', size=32),
        'has_contract': fields.function(
            _has_contract, type='boolean', method=True,
            fnct_search=_has_contract_search, string=u'Té pòlissa activa'
        )
    }

    _defaults = {
        'pot_max_admisible': lambda *a: 0.0,
        'pot_installada': lambda *a: 0.0,
        'tensio': lambda *a: 0,
        'resistencia_terra': lambda *a: 0.0,
        'resistencia_aillament': lambda *a: 0.0,
        'interruptor_diferencial': lambda *a: 0.0,
        'cups_id': _get_cups,
    }

GiscedataButlleti()
