# -*- coding: utf-8 -*-
from osv import osv, fields
from tools import multikeysort


class GiscedataCupsPs(osv.osv):
    _name = 'giscedata.cups.ps'
    _inherit = 'giscedata.cups.ps'

    def _get_pot_adscrita(
            self, cr, uid, ids, field_name, arg, context=None):
        """
        Fields function to calculate the potencia_adscrita field

        :param cr: Database curso
        :param uid: User id
        :type uid: int
        :param ids: CUPS ids to calculate
        :type ids: list[int]
        :param field_name: name of the field
        :type field_name: str
        :param arg:
        :param context: OpenERP context
        :type context: dict
        :return: Id, value dict
        :rtype: dict[int, str]
        """

        if context is None:
            context = {}

        butlletins_obj = self.pool.get('giscedata.butlleti')
        cups_elems = self.read(cr, uid, ids, ['id', 'force_potencia_adscrita'])
        filter = [('cups_id', 'in', ids), '|',
                  ('data_vigencia', '!=', False),
                  ('data', '!=', False)]
        butlletins_ids = butlletins_obj.search(
            cr, uid, filter
        )
        butlletins_elems = butlletins_obj.read(
            cr, uid, butlletins_ids,
            ['id', 'cups_id', 'pot_max_admisible', 'data_vigencia', 'data']
        )

        ret = dict.fromkeys(ids, 0.0)
        pot_bloq = dict.fromkeys(ids)
        for cups in cups_elems:
            pot_bloq[cups['id']] = cups['force_potencia_adscrita']

        # order from oldest to newest
        ordered_butlletins = multikeysort(
            butlletins_elems, ['data_vigencia', 'data', 'id']
        )

        # s'escriuran tots els valors de potencia, pero com que haurem ordenat
        # d'antic a nou, podem asegurar que l'ultim valor escrit sera el de la
        # potencia mes actual
        for butlleti in ordered_butlletins:
            cups_id = butlleti['cups_id'][0]
            potencia = butlleti['pot_max_admisible']
            if not pot_bloq[cups_id]:
                ret[cups_id] = potencia

        return ret

    def _get_cups_to_update(self, cr, uid, ids, context=None):
        """
        ToDo
        :param cursor:
        :param uid:
        :param ids:
        :param context:
        :return:
        """
        if context is None:
            context = {}

        ret = []
        butlleti_obj = self.pool.get('giscedata.butlleti')

        for id in ids:
            cups_id = butlleti_obj.read(
                cr, uid, id, ['cups_id']
            )['cups_id'][0]

            butlleti_id = butlleti_obj.search(
                cr, uid, [('cups_id', '=', cups_id)],
                order="data_vigencia desc, id desc",
                limit=1
            )
            if id in butlleti_id:
                ret.append(cups_id)

        return ret

    def _get_cups_ids(self, cursor, uid, ids, context=None):
        """
        ToDo
        :param cursor:
        :param uid:
        :param ids:
        :param context:
        :return:
        """
        return ids

    def _write_pot_adscrita(self, cursor, uid, ids, name, value, args, context=None):
        """
        ToDo
        :param cursor:
        :param uid:
        :param ids:
        :param name:
        :param value:
        :param args:
        :param context:
        :return:
        """
        if not context:
            context = {}

        if not isinstance(ids, (tuple, list)):
            ids = [ids]

        cursor.execute("update giscedata_cups_ps set potencia_adscrita=%s "
                       "where id in %s and force_potencia_adscrita",
                       (value or None, tuple(ids),))
        return True

    _columns = {
        'butlletins': fields.one2many(
            'giscedata.butlleti',
            'cups_id',
            'Butlletins'
        ),
        'force_potencia_adscrita': fields.boolean("Bloquejar potència adscrita"),
        'potencia_adscrita': fields.function(
            _get_pot_adscrita,
            method=True,
            string="Potència adscrita (kW)",
            store={
                'giscedata.butlleti': (
                    _get_cups_to_update,
                    ['cups_id', 'pot_max_admisible'],
                    10),
                'giscedata.cups.ps': (
                    _get_cups_ids,
                    ['force_potencia_adscrita'],
                    10)
            },
            digits=(15, 3),
            type='float',
            fnct_inv=_write_pot_adscrita)
    }


GiscedataCupsPs()
