# -*- coding: utf-8 -*-
from osv import osv, fields
from dateutil.relativedelta import relativedelta
from tools.translate import _
import pymssql
from tools import config
from addons import get_module_resource


class TelemeasureLoader(osv.osv_memory):

    _name = 'wizard.load.telemeasures'

    def action_load_telemeasures(self, cursor, uid, ids, context=None):

        meter_obj = self.pool.get('giscedata.lectures.comptador')
        origen_obj = self.pool.get('giscedata.lectures.origen')
        polissa_obj = self.pool.get('giscedata.polissa')
        tarifa_obj = self.pool.get('giscedata.polissa.tarifa')
        wizard = self.browse(cursor, uid, ids[0], context=context)

        meter = meter_obj.read(cursor, uid, wizard.meter_id['id'], ['name'])
        polissa_id = meter_obj.read(cursor, uid, wizard.meter_id['id'],
                                    ['polissa'])
        tarifa_id = polissa_obj.read(cursor, uid, polissa_id['polissa'][0],
                                     ['tarifa'])
        tarifa_name = tarifa_obj.read(cursor, uid, tarifa_id['tarifa'][0],
                                      ['name'])['name']
        origin_id = origen_obj.search(cursor, uid, [('codi', '=', 10)])[0]
        periods = tarifa_obj.get_periodes(cursor, uid, tarifa_id['tarifa'][0],
                                          tipus='te')

        conn = pymssql.connect(config['sqlserver_host'],
                               config['sqlserver_user'],
                               config['sqlserver_pass'],
                               config['sqlserver_dbname'])

        if 'RE' in tarifa_name:  # Get 3rd contract if generation
            contract = 3
        else:
            contract = 1

        sql_file = 'get_meter_telemeasures.sql'
        sql = get_module_resource('giscedata_telemedida_sql', 'sql', sql_file)
        result = 0
        with open(sql, 'r') as sqlfile:
            query = sqlfile.read()
        with conn.cursor(as_dict=True) as pyms_cr:
            pyms_cr.execute(query, (meter['name'], wizard.date))
            for row in pyms_cr:
                if self.manage_row(cursor, uid, row, wizard, periods,
                                   contract, origin_id):
                    result += 1
        wizard.write(
            {'info': _('As a result {} active, reactive and maximeter readings '
                       'have been generated.'.format(result))}, context=context)

    def manage_row(self, cursor, uid, row_data, wizard, periods, contract,
                   origin_id):
        if (contract == 1 == row_data['Tipocontrato']
                and row_data['PeriodoTarifario'] != 0):
            vals = {'name': (row_data['FinDelPeriodo'].date() -
                             relativedelta(days=1)).strftime('%Y-%m-%d'),
                    'periode': get_proper_period(
                        row_data['PeriodoTarifario'], periods),
                    'comptador': wizard.meter_id['id'],
                    'origen_id': origin_id,
                    'observacions': wizard.observations}
            self.create_active_read(cursor, uid, row_data, vals)
            self.create_reactive_read(cursor, uid, row_data, vals)
            self.create_max_read(cursor, uid, row_data, vals)
            return True
        else:
            return False

    def create_active_read(self, cursor, uid, row_data, vals):
        lect_lec_obj = self.pool.get('giscedata.lectures.lectura')
        active_vals = vals.copy()
        active_vals['lectura'] = int(row_data['EnergiaAbsolutaActiva'])
        active_vals['tipus'] = 'A'
        lect_lec_obj.create(cursor, uid, active_vals)

    def create_reactive_read(self, cursor, uid, row_data, vals):
        lect_lec_obj = self.pool.get('giscedata.lectures.lectura')
        reactive_vals = vals.copy()
        reactive_vals['lectura'] = int(row_data[
                                           'EnergiaAbsolutaReactivaInductiva'])
        reactive_vals['tipus'] = 'R'
        lect_lec_obj.create(cursor, uid, reactive_vals)

    def create_max_read(self, cursor, uid, row_data, vals):
        lect_pot_obj = self.pool.get('giscedata.lectures.potencia')
        max_vals = vals.copy()
        max_vals['lectura'] = int(row_data['MaximoDeLasPotencias'])
        max_vals['exces'] = row_data['ExcesosDeLasPotencias']
        lect_pot_obj.create(cursor, uid, max_vals)

    _columns = {
        'meter_id': fields.many2one('giscedata.lectures.comptador',
                                    'Meter', required=True),
        'date': fields.date('Date', required=True),
        'info': fields.text('Information', readonly=True),
        'observations': fields.char('Message', size=50)
    }

    _defaults = {
        'meter_id': lambda cursor, uid, ids, context: context.get(
            'active_ids')[0],
        'info': lambda *a: _('Insert the end of the period date'),
        'observations': lambda *a: _('Imported data'),
    }

TelemeasureLoader()


def get_proper_period(period, periods):
    code = 'P{}'.format(str(period))
    return periods[code]
