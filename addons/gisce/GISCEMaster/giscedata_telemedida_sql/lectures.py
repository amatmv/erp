from giscedata_lectures_tecnologia.giscedata_lectures import TECHNOLOGY_TYPE_SEL

if 'telemeasure_sql' not in dict(TECHNOLOGY_TYPE_SEL).keys():
    TECHNOLOGY_TYPE_SEL += [('telemeasure_sql', 'Telemeasure SQL')]
