SELECT
  InformacionTarificacion.Id_PM,
  EnlacePM.NombrePM,
  EnlacePM.Contador,
  InformacionTarificacion.Tipocontrato,
  InformacionTarificacion.PeriodoTarifario,
  InformacionTarificacion.EnergiaAbsolutaActiva,
  InformacionTarificacion.EnergiaIncrementalActiva,
  InformacionTarificacion.EnergiaAbsolutaReactivaInductiva,
  InformacionTarificacion.EnergiaIncrementalReactivaInductiva,
  InformacionTarificacion.EnergiaAbsolutaReactivaCapacitiva,
  InformacionTarificacion.EnergiaIncrementalReactivaCapacitiva,
  InformacionTarificacion.MaximoDeLasPotencias,
  InformacionTarificacion.FechaDelMaximo,
  InformacionTarificacion.ExcesosDeLasPotencias,
  InformacionTarificacion.InicioDelPeriodo,
  InformacionTarificacion.FinDelPeriodo,
  InformacionTarificacion.CualificadorDeEnergiaActiva
FROM InformacionTarificacion
INNER JOIN EnlacePM ON InformacionTarificacion.Id_PM = EnlacePM.IdPM
WHERE
  EnlacePM.Contador=%s
  AND InformacionTarificacion.FinDelPeriodo=%s
ORDER BY InformacionTarificacion.Tipocontrato, InformacionTarificacion.PeriodoTarifario;