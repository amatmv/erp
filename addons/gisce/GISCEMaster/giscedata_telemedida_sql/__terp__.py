# -*- coding: utf-8 -*-
{
    "name": "Obtention of telemeasures from SQL",
    "description": """
    Obtention of readings from an SQL database
    """,
    "version": "0-dev",
    "author": "GISCE-TI",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "mongodb_backend",
        "infraestructura",
        "giscedata_lectures_distri"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/wizard_load_telemeasures_view.xml",
        "security/ir.model.access.csv",
        "lectures_view.xml"
    ],
    "active": False,
    "installable": True
}
