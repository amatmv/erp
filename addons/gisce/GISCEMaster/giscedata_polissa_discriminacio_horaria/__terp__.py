# -*- coding: utf-8 -*-
{
    "name": "Discriminació Horària per les pòlisses",
    "description": """Afegeix les polisses pels clients""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Customers",
    "depends":[
        "giscedata_polissa"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_polissa_discriminacio_horaria_view.xml"
    ],
    "active": False,
    "installable": True
}
