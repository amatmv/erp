# -*- coding: utf-8 -*-
{
    "name": "CNMC Circular 4/2014",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Generació dels formularis de la circular 4/2014
    - F1
    - F1 bis
    - F11
  """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "giscedata_administracio_publica_cne",
        "giscedata_cne",
        "giscedata_cts",
        "giscedata_transformadors",
        "giscedata_lectures_distri",
        "giscedata_cups_distri"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/wizard_circular_4_2014_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
