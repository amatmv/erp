UPDATE giscedata_cups_ps c
SET cne_anual_activa = energia.activa , cnmc_potencia_facturada = energia.potencia_facturada
FROM (
select c.id as id , c.name as cups ,
COALESCE(SUM(il.quantity*(fl.tipus='energia')::int*(CASE WHEN i.type='out_refund' THEN -1 ELSE 1 END)),0.0) as activa,
COALESCE((select max(il1.quantity)
from account_invoice_line il1
where il1.id in (
        select invoice_line_id
        from giscedata_facturacio_factura_linia fl1
        where fl1.factura_id in (
            select id
            from giscedata_facturacio_factura f1
            where
            f1.cups_id = c.id and to_char(f1.data_final, 'YYYY') = %s
            AND f1.tipo_rectificadora in ('N', 'R', 'RA')
            and f1.id not in (
                SELECT ref
                FROM giscedata_facturacio_factura WHERE
                tipo_rectificadora in ('A', 'B', 'R', 'RA', 'BRA'))
            order by data_final desc limit 1
            )
        and fl1.tipus = 'potencia')),0.0) as potencia_facturada
from giscedata_cups_ps c
LEFT JOIN giscedata_facturacio_factura f ON (f.cups_id=c.id and to_char(f.data_final, 'YYYY') = %s)
LEFT JOIN account_invoice i ON (i.id=f.invoice_id)

LEFT JOIN giscedata_facturacio_factura_linia fl ON (fl.factura_id=f.id and fl.tipus in ('energia','reactiva'))
LEFT JOIN account_invoice_line il on (fl.invoice_line_id=il.id)

GROUP BY c.id,c.name
) as energia
WHERE energia.id=c.id