update giscedata_cups_ps
set cne_anual_reactiva = energia.reactiva
FROM(
select c.id as cups_id, COALESCE(SUM(l.consum),0.0) as reactiva
from giscedata_cups_ps c
left join giscedata_polissa p on(p.cups = c.id)
left join giscedata_lectures_comptador co on (co.polissa = p.id)
left join giscedata_lectures_lectura l on (l.comptador = co.id and l.tipus = 'R' and to_char(l.name,'YYYY') = %s)
where
 (
        to_char(p.data_alta, 'YYYY') = %s
        or 
        to_char(p.data_baixa,'YYYY') = %s
        or
        p.data_baixa is null
 )
 and p.state not in ('esborrany','validar','cancelada')
group by c.id
) energia
where energia.cups_id = id
