update giscedata_cups_ps c
set cnmc_numero_lectures = lectures.total_lectures
from (
select cups.id as id, cups.name as name ,count(lec.name) as total_lectures
from
  (select l1.comptador as comptador, l1.name as name, l1.tipus as tipus from giscedata_lectures_lectura l1
  group by l1.comptador, l1.name, l1.tipus) lec,
  giscedata_lectures_comptador comp,
  giscedata_polissa pol,
  giscedata_cups_ps cups
where
 lec.comptador = comp.id
 and comp.polissa = pol.id
 and pol.cups = cups.id
 and to_char(lec.name,'YYYY') = %s
 and lec.tipus='A'
 group by cups.id, cups.name
) as lectures
where lectures.id = c.id