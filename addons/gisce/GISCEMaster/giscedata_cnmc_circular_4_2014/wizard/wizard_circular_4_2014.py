# -*- coding: utf-8 -*-
import base64
from datetime import datetime
import multiprocessing
import os
import subprocess
import tempfile
import netsvc

from osv import osv, fields
from tools import config
from tools.translate import _


class WizardCircular42014(osv.osv_memory):
    _name = 'wizard.circular.4_2014'

    def add_status(self, cursor, uid, ids, status, context=None):
        if not context:
            context = None
        wiz = self.browse(cursor, uid, ids[0], context=context)
        if wiz.status:
            status = wiz.status + '\n' + status
        wiz.write({'status': status})

    def get_script_params(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        user_obj = self.pool.get('res.users')
        user = user_obj.read(cursor, uid, uid, ['login', 'password'])
        wiz = self.browse(cursor, uid, ids[0], context=context)
        uri = 'http'
        if config.get('secure'):
            uri += 's'
        uri += '://localhost'
        args = {
            'server': uri,
            'port': config.get('port'),
            'user': user['login'],
            'password': user['password'],
            'database': cursor.dbname,
        }
        if wiz.num_proc != '*':
            args['num-proc'] = int(wiz.num_proc)
        return args

    def run_script(self, cursor, uid, ids, script_args, context=None):
        if not context:
            context = {}
        env = os.environ.copy()
        sentry_dsn = config.get('sentry_dsn')
        if sentry_dsn:
            env['SENTRY_DSN'] = sentry_dsn
        # Add logger call
        logger = netsvc.Logger()
        logger.notifyChannel(
            'server', netsvc.LOG_INFO,
            'script executed: {}'.format(' '.join(map(str, script_args)))
        )
        retcode = subprocess.call(script_args, env=env)
        if retcode:
            raise osv.except_osv(
                'Error',
                _("No s'ha pogut generar l'informe correctament. "
                  "Codi: %s") % retcode
            )
        return retcode

    def actualitzar_energia(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        sql = '%s/giscedata_cnmc_circular_4_2014/sql/recalcular_%s_cnmc_cups.sql'
        wiz = self.browse(cursor, uid, ids[0], context=context)
        status = False
        if wiz.te_facturacio:
            year = str(wiz.year)
            with open(sql % (config['addons_path'], 'lectures'), 'r') as f:
                query = f.read()
                cursor.execute(query, (year, ))
            with open(sql % (config['addons_path'], 'valors'), 'r') as f:
                query = f.read()
                cursor.execute(query, (year, year))
            with open(sql % (config['addons_path'], 'lect_reactiva'), 'r') as f:
                query = f.read()
                cursor.execute(query, (year, year, year))
                status = True

        elif wiz.file_in_cnmc:
            filename = tempfile.mkstemp()[1]
            data = base64.decodestring(wiz.file_in_cnmc)
            with open(filename, 'w') as f:
                f.write(data)
            args = wiz.get_script_params()
            args.update({'file-input': filename})
            args = (
                ['update_cnmc_stats'] +
                ['--%s=%s' % (k, v) for k, v in args.items() if v] +
                ['--no-interactive']
            )
            wiz.run_script(args, context=context)
            wiz.write({'file_in_cnmc': False})
            os.unlink(filename)
            status = True
        if status:
            wiz.add_status(
                _(u"Valors actualitzats correctament per l'any %s") % wiz.year
            )
        return True

    def actualitzar_cinis(self, cursor, uid, ids, context=None):
        if not context:
            context = None
        wiz = self.browse(cursor, uid, ids[0], context=context)
        if wiz.file_in_cinis:
            filename = tempfile.mkstemp()[1]
            data = base64.decodestring(wiz.file_in_cinis)
            with open(filename, 'w') as f:
                f.write(data)
            args = wiz.get_script_params()
            args.update({'file-input': filename})
            args = (
                ['update_cinis_comptador'] +
                ['--%s=%s' % (k, v) for k, v in args.items() if v] +
                ['--no-interactive']
            )
            wiz.run_script(args, context=context)
            wiz.write({'file_in_cinis': False})
            os.unlink(filename)
            wiz.add_status(_(u"CINIS de comptadors actualitzats correctament"))
        return True

    def action_gen_formulari(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        wiz = self.browse(cursor, uid, ids[0], context=context)
        args = wiz.get_script_params()
        output = tempfile.mkstemp()[1]
        args.update({
            'codi-r1': wiz.codi_r1, 'year': wiz.year, 'output': output
        })
        args = (
            ['f%s' % wiz.type] +
            ['--%s=%s' % (k, v) for k, v in args.items() if v] +
            ['--no-interactive']
        )
        wiz.run_script(args, context=context)
        result = base64.b64encode(open(output).read())
        filename = 'CIR4_2014_%s_%s_%s.txt' % (wiz.type, wiz.codi_r1,
                                               datetime.now().year)
        status = _(u"Informe %s generat correctaemnt") % filename
        wiz.write({
            'name': filename, 'file_out_cnmc': result, 'state': 'done',
            'status': status
        })


    def _te_facturacio(self, cursor, uid, ids, field_name, args, context=None):
        module_obj = self.pool.get('ir.module.module')
        cou = module_obj.search_count(cursor, uid, [
            ('name', '=', 'giscedata_facturacio'),
            ('state', '=', 'installed')
        ])
        return dict.fromkeys(ids, bool(cou))

    def _default_te_facturacio(self, cursor, uid, context=None):
        fact = self._te_facturacio(cursor, uid, [1], '', None, context=context)
        return fact[1]

    def _num_proc(self, cursor, uid, context=None):
        return (
            [('*', 'Automàtic')] +
            [(str(x), ('%s proc.') % x)
                for x in xrange(1, multiprocessing.cpu_count() + 2)]
        )

    _columns = {
        'type': fields.selection(
            [('1', 'F1'), ('1bis', 'F1Bis'), ('11', 'F11')],
            'Formulari',
            required=True
        ),
        'codi_r1': fields.char('Codi R1', size=3, required=True),
        'state': fields.char('Estat', size=16),
        'name': fields.char('Nom del fitxer', size=64),
        'year': fields.integer(
            'Any del càlcul',
            size=4,
            required=True,
            help=(u"Any pel qual es vol generar l'informe, normalment és "
                  u"l'any anterior al vigent.")
        ),
        'recalcular_camps_cnmc': fields.boolean(
            'Actulitzar valors CNMC CUPS',
            help=u"Torna a actualitzar els camps d'estadística CNMC del CUPS"
        ),
        'te_facturacio': fields.function(
            _te_facturacio,
            type='boolean',
            method=True,
            string='Te facturació'
        ),
        'num_proc': fields.selection(
            _num_proc,
            'Num. Processadors',
            required=True,
            help=(u"Número de processos a utilitzar per generar el fitxer. Com "
                  u"més processos, més ràpid, però utilitza més recursos. "
                  u"Automàticament utilitza un procés per processador")
        ),
        'file_in_cnmc': fields.binary('CUPS CSV',
            help=(u"Fitxer CSV amb les dades CNMC per CUPS "
                  u"l'any anterior en KWh:\nESXXXXXXXXXX;activa;reactiva"
                  u"potencia_facturada;numero_lecturas")
        ),
        'file_in_cinis': fields.binary('Comptadors CSV',
            help=(u"Fitxer CSV amb els CINIS dels comptadors "
                 u"El format és:\nNúmero de comptador;CINI")
        ),
        'file_out_cnmc': fields.binary('Resultat'),
        'status': fields.text('Estatus')
    }

    _defaults = {
        'state': lambda *a: 'init',
        'year': lambda *a: datetime.now().year - 1,
        'recalcular_camps_cnmc': lambda *a: 0,
        'te_facturacio': _default_te_facturacio,
        'num_procs': lambda *a: '*',
    }

WizardCircular42014()