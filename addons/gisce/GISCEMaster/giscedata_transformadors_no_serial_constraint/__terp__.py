# -*- coding: utf-8 -*-
{
    "name": "No Serial als Transformadors",
    "description": """Elimina la constraint del número de fabricació dels Transformadors""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "giscedata_transformadors"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_transformador_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
