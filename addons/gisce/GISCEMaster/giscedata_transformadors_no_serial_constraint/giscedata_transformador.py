from osv import fields, osv


class giscedata_transformador_no_serial_contraint(osv.osv):

    _name = 'giscedata.transformador.noserialconstraint'
    _auto = False


    def init(self, cr):
        cr.execute("SELECT * from information_schema.constraint_table_usage where constraint_name = 'giscedata_transformador_trafo_numero_fabricacio_uniq'")
        constraint = cr.fetchall()
        if constraint and len(constraint):
            cr.execute("ALTER TABLE giscedata_transformador_trafo DROP CONSTRAINT giscedata_transformador_trafo_numero_fabricacio_uniq")
            cr.commit()

giscedata_transformador_no_serial_contraint()
