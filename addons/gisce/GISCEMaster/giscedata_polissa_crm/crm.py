# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright Joan M. Grande <jgrande@el-gas.es>
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields

class GiscedataPolissaCRM(osv.osv):
    
    _name = 'crm.case'
    _inherit = 'crm.case'

    def create_case_generic(self, cursor, uid, model_ids, context=None,
                                  description='Generic Case',
                                  section='CASE', 
                                  category=None,
                                  assign=None,
                                  extra_vals=None):
        
        case_ids = super(GiscedataPolissaCRM, self).create_case_generic(
                                  cursor, uid, model_ids, context,
                                  description, section,
                                  category, assign, extra_vals)
        
        if not context:
            context = {}
        model = context.get('model',False)
        
        if model == 'giscedata.polissa':
            #Si el model es una polissa, afegim la referencia explicita
            for case in self.browse(cursor, uid, case_ids, context):
                polissa_id = case.ref.partition(',')[2]
                case.write({'polissa_id': polissa_id})

        return case_ids    
   
    _columns = {
        'polissa_id': fields.many2one('giscedata.polissa',
                                     'Abonat', required=False),        
    }
    
GiscedataPolissaCRM()