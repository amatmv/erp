# -*- coding: utf-8 -*-
{
    "name": "Extensió del CRM per una pòlissa",
    "description": """
  * Afegir el camp polissa_id al cas del crm
  * Accés directe als casos creats
  * Wizard per crear ordres des de multiples pòlisses
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "CRM",
    "depends":[
        "crm_generic",
        "giscedata_polissa"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "crm_data.xml",
        "crm_view.xml",
        "crm_report.xml"
    ],
    "active": False,
    "installable": True
}
