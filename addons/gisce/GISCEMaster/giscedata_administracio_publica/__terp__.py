# -*- coding: utf-8 -*-
{
    "name": "Administració pública",
    "description": """Crea el menú d'administració pública com a base""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_administracio_publica_view.xml"
    ],
    "active": False,
    "installable": True
}
