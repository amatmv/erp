# -*- coding: utf-8 -*-
{
    "name": "Tarifas Generación 1544/2011",
    "description": """
Actualització de les tarifes de peatges segons el BOE nº 276 - 16/11/2011.
1544/2011
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_polissa_distri",
        "product",
        "giscedata_facturacio_distri"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_tarifas_generacion_2011_data.xml"
    ],
    "active": False,
    "installable": True
}
