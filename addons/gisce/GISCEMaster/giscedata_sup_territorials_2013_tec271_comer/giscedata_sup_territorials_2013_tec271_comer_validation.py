# -*- coding: utf-8 -*-

from osv import osv, fields


class GiscedataSupTeritorials2013TEC271ComerValidation(osv.osv):

    _name = 'giscedata.facturacio.validation.validator'
    _inherit = 'giscedata.facturacio.validation.validator'

    def check_duplicates_sups_etu271(self, cursor, uid, fact, parameters):
        pool = self.pool
        linia_obj = pool.get('giscedata.facturacio.factura.linia')

        date_from = fact.data_inici
        date_to = fact.data_final
        # Getting the extra lines from in invoice
        search_params = [
            ('factura_id', '=', fact.id),
            ('invoice_line_id.product_id.default_code', '=', 'TEC271')
        ]

        sql = linia_obj.q(cursor, uid).select(
            ['id']).where(search_params)
        cursor.execute(*sql)
        if (cursor.rowcount > 1):
            return {
                'date_from': date_from,
                'date_to': date_to
            }

        return None

    def check_cups_exist_etu271(self, cursor, uid, fact, parameters):
        pool = self.pool
        linia_obj = pool.get('giscedata.facturacio.factura.linia')
        sup_territorials_2013_tec271_comer_obj = pool.get(
            'giscedata.suplements.territorials.2013.tec271.comer'
        )
        # Getting the extra lines from in invoice
        search_params = [
            ('factura_id', '=', fact.id),
            ('invoice_line_id.product_id.default_code', '=', 'TEC271')
        ]

        sql = linia_obj.q(cursor, uid).select(
            ['id']).where(search_params)
        cursor.execute(*sql)

        if cursor.rowcount:
            # If there's an TEC271 product, we have to finally check if it
            # exists in the 2013_comer table
            contract_cups = fact.cups_id.name[:20]
            sups_id = sup_territorials_2013_tec271_comer_obj.search(
                cursor, uid, [('cups', 'like', contract_cups)]
            )
            if not sups_id:
                return {'cups': contract_cups}

        return None

    def check_cups_not_notificated_etu271(self, cursor, uid, fact, parameters):
        pool = self.pool
        linia_obj = pool.get('giscedata.facturacio.factura.linia')
        sup_territorials_2013_tec271_comer_obj = pool.get(
            'giscedata.suplements.territorials.2013.tec271.comer'
        )
        # Getting the extra lines from in invoice
        search_params = [
            ('factura_id', '=', fact.id),
            ('invoice_line_id.product_id.default_code', '=', 'TEC271')
        ]

        sql = linia_obj.q(cursor, uid).select(
            ['id']).where(search_params)
        cursor.execute(*sql)

        if cursor.rowcount:
            # If there's an TEC271 product, we have to finally check if it
            # exists in the 2013_comer table
            contract_cups = fact.cups_id.name[:20]
            sups_id = sup_territorials_2013_tec271_comer_obj.search(
                cursor, uid, [
                    ('cups', 'like', contract_cups)
                ]
            )
            if sups_id:
                sups_id = sup_territorials_2013_tec271_comer_obj.search(
                    cursor, uid, [
                        ('id', 'in', sups_id), ('notificado', '=', False)
                    ]
                )
                if sups_id:
                    return {'cups': contract_cups}
        return None


GiscedataSupTeritorials2013TEC271ComerValidation()
