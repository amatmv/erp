# -*- coding: utf-8 -*-
import json

from osv import osv
from datetime import datetime
from dateutil.relativedelta import relativedelta
from tools.translate import _


# Codigos de comunidad autonoma a aplicar

"""
            name             | codi 
-----------------------------+------
 Andalucia                   | 01
 Aragón                      | 02
 Asturias, Principado de     | 03
 Baleares, Islas             | 04
 Canarias                    | 05
 Cantabria                   | 06
 Castilla y León             | 07
 Castilla - La Mancha        | 08
 Cataluña                    | 09
 Comunidad Valenciana        | 10
 Extremadura                 | 11
 Galicia                     | 12
 Madrid, Comunidad de        | 13
 Murcia, Región de           | 14
 Navarra, Comunidad Foral de | 15
 País Vasco                  | 16
 Rioja, La                   | 17
 Ceuta                       | 18
 Melilla                     | 19

"""

# Andalucía | Aragón | Principado de Asturias | Cantabria | Castilla y León |
# Cataluña | Extremadura | Galicia | Madrid | Murcia | Navarra
# https://www.boe.es/boe/dias/2019/03/12/pdfs/BOE-A-2019-3486.pdf


class GiscedataFacturacioSwitchingHelper(osv.osv):
    _name = 'giscedata.facturacio.switching.helper'
    _inherit = 'giscedata.facturacio.switching.helper'

    def calc_extra_name(self, cursor, uid, polissa_id):

        polissa_obj = self.pool.get('giscedata.polissa')
        cups_obj = self.pool.get('giscedata.cups.ps')
        ccaa_obj = self.pool.get('res.comunitat_autonoma')

        cups_id = polissa_obj.read(
            cursor, uid, polissa_id, ['cups']
        )['cups'][0]

        municipi_id = cups_obj.read(
            cursor, uid, cups_id, ['id_municipi']
        )['id_municipi'][0]

        ccaa_id = ccaa_obj.get_ccaa_from_municipi(cursor, uid, municipi_id)[0]
        ccaa_data = ccaa_obj.read(
            cursor, uid, ccaa_id, ['name', 'codi']
        )

        comunidad_name = ccaa_data['name']

        name = _(
            "Suplement territorial per tributs autonòmics de "
            "la Comunitat Autònoma %s de l'any 2013"
        ) % comunidad_name

        return name

    def get_values_in_ext(self, cursor, uid, fact_id, fact_name, ori_lines,
                          context=None):
        values = super(
            GiscedataFacturacioSwitchingHelper, self
        ).get_values_in_ext(cursor, uid, fact_id, fact_name, ori_lines, context)

        imd_obj = self.pool.get('ir.model.data')
        cfg_obj = self.pool.get('res.config')
        fact_obj = self.pool.get('giscedata.facturacio.factura')
        obj = self.pool.get('giscedata.facturacio.switching.helper')
        sup_obj = self.pool.get(
            'giscedata.suplements.territorials.2013.tec271.comer'
        )

        con49_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio_comer', 'concepte_49'
        )[1]

        delay_months = int(
            cfg_obj.get(cursor, uid, 'delay_concepte_49', 0)
        )  # No existeix la variable pero per si s'implementa

        set_data_final = int(
            cfg_obj.get(cursor, uid, 'end_date_set_as_invoice_for_suplements_concepte_49', 0)
        )

        cargar_comentaris = int(
            cfg_obj.get(cursor, uid, 'cargar_suplementos_tec_f1', 1)
        )

        for vals in values:
            if delay_months and vals['product_id'] == con49_id:
                for x in ('date_from', 'date_to'):
                    vals[x] = (
                        datetime.strptime(vals[x], '%Y-%m-%d')
                        + relativedelta(months=delay_months)
                    ).strftime('%Y-%m-%d')

            if vals['product_id'] == con49_id:
                new_name = self.calc_extra_name(cursor, uid, vals['polissa_id'])
                orig_comments = {
                    'comentarios_originales': context.get('concepte_49_notes', '')
                }
                comments = '<JSON>{}</JSON>'.format(
                    json.dumps(orig_comments)
                )
                notas = '{}\n{}'.format(vals['notes'], comments)

                if cargar_comentaris:
                    try:
                        sup_id = sup_obj.create_fake_suplemento_from_f1_info(
                            cursor, uid,
                            fact_id, vals,
                            orig_comments['comentarios_originales'],
                            context=context
                        )
                    except Exception as e:
                        if context.get('line_id', False):
                            error_obj = self.pool.get('giscedata.facturacio.switching.error')
                            error_obj.create_error_from_code(
                                cursor, uid, context['line_id'], '2', '099',
                                {'error': e.message}
                            )
                        pass

                vals.update({
                    'name': new_name,
                    'notes': notas
                })

                if set_data_final:
                    data_final = fact_obj.read(cursor, uid, fact_id, ['data_final'])['data_final']
                    vals.update({
                        'date_from': data_final,
                        'date_to': data_final,
                        'date_line_from': data_final,
                        'date_line_to': data_final
                    })

        return values

    def get_line_others_vals(
            self, cursor, uid, concepte, factura_vals, tipus, context=None):
        if context is None:
            context = {}
        vals = super(
            GiscedataFacturacioSwitchingHelper, self).get_line_others_vals(
            cursor, uid, concepte, factura_vals, tipus, context
        )

        imd_obj = self.pool.get('ir.model.data')
        con49_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio_comer', 'concepte_49'
        )[1]

        # Especification from CNMC
        if vals['product_id'] == con49_id:
            vals['force_price'] = concepte.importe
            vals['quantity'] = 1
            context['concepte_49_notes'] = concepte.comentarios
            context['f_inicio_periodo'] = concepte.fecha_desde
            context['f_fin_periodo'] = concepte.fecha_hasta
            context['con_price'] = concepte.precio_unidad

        return vals

GiscedataFacturacioSwitchingHelper()


class GiscedataFacturacioFactura(osv.osv):
    _name = 'giscedata.facturacio.factura'
    _inherit = 'giscedata.facturacio.factura'

    def has_tec271_line(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        if isinstance(ids, (list, tuple)):
            ids = ids[0]

        factura = self.browse(cursor, uid, ids)
        imd_obj = self.pool.get('ir.model.data')
        con49_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio_comer', 'concepte_49'
        )[1]
        altres_lines = []
        sups_ids = []
        cups_name = factura.cups_id.name[:20]
        if cups_name:
            for l in factura.linia_ids:
                if l.tipus == 'altres' and l.product_id.id == con49_id:
                    altres_lines.append(l)
            sups_obj = self.pool.get('giscedata.suplements.territorials.2013.tec271.comer')
            search_params = [
                ('cups', '=like', '{}%'.format(cups_name))
            ]
            sups_ids = sups_obj.search(
                cursor, uid, search_params
            )
        return altres_lines and sups_ids

GiscedataFacturacioFactura()
