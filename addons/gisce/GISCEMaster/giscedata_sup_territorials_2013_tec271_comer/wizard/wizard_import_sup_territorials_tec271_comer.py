# -*- coding: utf-8 -*-

from osv import osv, fields
from base64 import b64decode
import pandas as pd
import datetime
from tools.translate import _
from wizard import except_wizard
from StringIO import StringIO
from numpy import isnan
from pandas.api.types import is_string_dtype
from tqdm import tqdm


class WizardImportSupTerritorialsTec271Comer(osv.osv_memory):
    """ Wizard per importar CSV dels Suplements Territorials"""
    _name = 'wizard.import.sup.territorials.tec271.comer'

    valid_header = {
        u'CUPS': 'cups',
        u'TarifaATRFact': 'tarifa_atr_fact',
        u'FechaDesde': 'fecha_desde',
        u'FechaHasta': 'fecha_hasta',
        u'MediaPondPotenciaAFacturar_P1(W)': 'media_pond_potencia_a_facturar_p1',
        u'MediaPondPotenciaAFacturar_P2(W)': 'media_pond_potencia_a_facturar_p2',
        u'MediaPondPotenciaAFacturar_P3(W)': 'media_pond_potencia_a_facturar_p3',
        u'MediaPondPotenciaAFacturar_P4(W)': 'media_pond_potencia_a_facturar_p4',
        u'MediaPondPotenciaAFacturar_P5(W)': 'media_pond_potencia_a_facturar_p5',
        u'MediaPondPotenciaAFacturar_P6(W)': 'media_pond_potencia_a_facturar_p6',
        # suplementos potencia?
        u'ImporteTotalSuplementoTerminoPotencia(€)': 'importe_total_suplemento_termino_potencia',
        u'ValorEnergiaActiva_P1(kWh)': 'valor_energia_activa_p1',
        u'ValorEnergiaActiva_P2(kWh)': 'valor_energia_activa_p2',
        u'ValorEnergiaActiva_P3(kWh)': 'valor_energia_activa_p3',
        u'ValorEnergiaActiva_P4(kWh)': 'valor_energia_activa_p4',
        u'ValorEnergiaActiva_P5(kWh)': 'valor_energia_activa_p5',
        u'ValorEnergiaActiva_P6(kWh)': 'valor_energia_activa_p6',
        # Suplementos energia?
        u'ImporteTotalSuplementoTerminoEnergia(€)': 'importe_total_suplemento_termino_energia',
        u'ImporteTotalARegularizar(€)': 'importe_total_a_regularizar'
    }

    _columns = {
        'state': fields.selection(
            [('init', 'Init'), ('end', 'Final')],
            'Estat'
        ),
        'info': fields.text('Informació', readonly=True),
        'import_file': fields.binary(
            'Fitxer', states={'load': [('requiered', True)]}
        ),
        'file_name': fields.char('Nom del fitxer', size=350),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'import_file': False
    }

    def importar_file(self, cursor, uid, ids, context=None):
        file_info = self.read(
            cursor, uid, ids, ['import_file', 'file_name'], context=context
        )[0]

        file_name = file_info['file_name']

        file_ext = file_name.split('.')[-1]

        if not file_ext.startswith('xls'):
            raise except_wizard(
                'Error',
                'El fitxer ha de tenir com a extensio (.xls o .xlsx)'
            )

        raw_file = StringIO(b64decode(file_info['import_file']))

        df_file = pd.read_excel(
            raw_file,
            dtype={
                u'TarifaATRFact': str,
                u'FechaDesde':
                    datetime.datetime,
                u'FechaHasta': datetime.datetime
            }
        )

        raw_file.close()

        # Lambda expression to remove \n and ' ' from column names
        clean_header_of_unworthy_chars = (
            lambda s: s.replace('\n', '').replace(' ', '').replace("\"", "")
        )
        df_file = df_file.rename(
            columns=lambda x: clean_header_of_unworthy_chars(x)
        )
        df_file['TarifaATRFact'] = df_file['TarifaATRFact'].astype(
            'str', errors='ignore')

        self.calculate_calculated_values(df_file)

        # Validate file header column names
        self.validate_header(cursor, uid, df_file, context=context)

        # Remove fake lines prom dataframe (last line with totals)
        df_file = df_file[df_file.CUPS.notnull()]

        # # Remove hours, minutes and seconds
        # df_file[u'FechaDesde'] = df_file[u'FechaDesde'].dt.date
        # df_file[u'FechaHasta'] = df_file[u'FechaHasta'].dt.date

        df_file.fillna(value=0, inplace=True)

        tqdm.pandas()
        df_file.progress_apply(
            lambda x: self.create_suplemento(
                cursor, uid, ids, x, filename=file_name, context=context
            ),
            axis=1
        )

        has_errors = bool(
            self.read(cursor, uid, ids, ['info'], context=context)[0]['info']
        )

        write_wiz_dic = {
            'state': 'end'
        }

        if not has_errors:
            write_wiz_dic.update({'info': 'Suplements creats correctament'})

        self.write(cursor, uid, ids, write_wiz_dic, context=context)

        return

    def validate_header(self, cursor, uid, file_df, context=None):
        '''
        Function to validate if file contains column names included
        in self.valid_header

        :param file_df: Pandas data frame
        :return:
        '''
        file_header = list(file_df.columns.values)

        for column_name in self.valid_header.keys():
            if column_name not in file_header:
                raise except_wizard(
                    'Error',
                    'El fitxer ha de tenir la columna %s' % column_name
                )

    def create_suplemento(self, cursor, uid, ids, row, filename=False, context=None):
        sup_tec271_obj = self.pool.get(
            'giscedata.suplements.territorials.2013.tec271.comer'
        )

        def conv_date(date):
            if not isinstance(date, (basestring, str, unicode)) and isinstance(date, datetime.date):
                date = str(date)
            try:
                assert(datetime.datetime.strptime(date, '%Y-%m-%d'))
            except:
                for format_date in ('%d/%m/%Y', '%Y/%m/%d', '%d-%m-%Y'):
                    try:
                        return datetime.datetime.strptime(
                            date, format_date
                        ).strftime('%Y-%m-%d')
                    except:
                        pass
                info = _("La data: {} té un format desconegut.".format(date))
                raise osv.except_osv(_(u'Error!'), info)
            return date

        sup_obj_create_dict = {}
        # Names of fields in erp model op sup tec 271
        for k, v in self.valid_header.iteritems():
            value = row[k]

            if v == 'tarifa_atr_fact':
                value = str(int(value)).zfill(3)

            if v in ('fecha_desde', 'fecha_hasta'):
                value = conv_date(value)

            sup_obj_create_dict.update(
                {
                    v: value
                }
            )
        try:
            sup_obj_create_dict['nombre_fichero'] = filename
            search_params = [
                ('cups', '=', sup_obj_create_dict['cups']),
                ('tarifa_atr_fact', '=', sup_obj_create_dict['tarifa_atr_fact']),
                ('fecha_desde', '=', sup_obj_create_dict['fecha_desde']),
                ('fecha_hasta', '=', sup_obj_create_dict['fecha_hasta']),
            ]
            sup_ids = sup_tec271_obj.search(cursor, uid, search_params)
            if sup_ids:
                sup_tec271_obj.write(
                    cursor, uid, sup_ids, sup_obj_create_dict, context=context
                )
            else:
                sup_id = sup_tec271_obj.create(
                    cursor, uid, sup_obj_create_dict, context=context
                )

        except Exception as e:
            msg = (
                'Error en la linia amb cups %s: %s' % (
                    sup_obj_create_dict['cups'], str(e)
                )
            )

            self.update_info(cursor, uid, ids, msg, context=context)

    def update_info(self, cursor, uid, ids, msg, context=None):
        info = self.read(cursor, uid, ids, ['info'], context=context)[0]['info']

        self.write(cursor, uid, ids, {'info': '%s%s\n' % (info, msg)})

    def calculate_calculated_values(self, consums_df):
        '''
        Calcula los campos calculables para el ficheros si no lo estan
        :param consums_df: DataFrame consumemas
        :return:
        '''

        def mul_nan(b, a, c=1):
            if isnan(b):
                b = 0
            if isnan(a):
                a = 0
            if isnan(c):
                c = 0
            return b * a * c

        def sum_array_nan(l):
            return sum([0.0 if (isnan(elem) or False) else elem for elem in l])

        period_names = [u'P1', u'P2', u'P3', u'P4', u'P5', u'P6']

        # Remove hours, minutes and seconds
        if is_string_dtype(consums_df[u'FechaDesde']):
            consums_df[u'FechaDesde'] = consums_df[u'FechaDesde'].apply(lambda x: datetime.datetime.strptime(x, '%Y-%m-%d'))
        if is_string_dtype(consums_df[u'FechaHasta']):
            consums_df[u'FechaHasta'] = consums_df[u'FechaHasta'].apply(lambda x: datetime.datetime.strptime(x, '%Y-%m-%d'))

        consums_df[u'FechaDesde'] = consums_df[u'FechaDesde'].dt.date
        consums_df[u'FechaHasta'] = consums_df[u'FechaHasta'].dt.date

        # Calculate number of days and create column if don't exist
        consums_df[u"NumeroDias"] = consums_df.apply(
            lambda row: (row.FechaHasta - row.FechaDesde).days, axis=1
        )

        # Calculo del total potencia

        have_importes_periodes_potencia_calulated = {
            u'ImporteSuplementoTerminoPotencia_P1(€)',
        }.issubset(consums_df.columns)

        empty_importes_periodes_potencia_calulated = consums_df[
            u'ImporteSuplementoTerminoPotencia_P1(€)'
        ].isnull().values.all()

        if not have_importes_periodes_potencia_calulated or empty_importes_periodes_potencia_calulated:
            for p in period_names:
                media_p_pot = (
                    u'MediaPondPotenciaAFacturar_{}(W)'.format(p)
                )
                suplemento_p_pot = (
                    u'SuplementoPotencia_{}(€/W)'.format(p)
                )
                total_p_pot = (
                    u'ImporteSuplementoTerminoPotencia_{}(€)'.format(p)
                )

                consums_df[total_p_pot] = consums_df.apply(
                    lambda x: mul_nan(x[media_p_pot],
                                      x[suplemento_p_pot]),
                    axis=1
                )

        column_importe_total_potencia_exist = {
            u'ImporteTotalSuplementoTerminoPotencia(€)',
        }.issubset(consums_df.columns)

        column_importe_total_potencia_empty = consums_df[
            u'ImporteTotalSuplementoTerminoPotencia(€)'
        ].isnull().values.all()

        if not column_importe_total_potencia_exist or column_importe_total_potencia_empty:
            # sumar parcials
            partial_import = u'ImporteSuplementoTerminoPotencia_{}(€)'
            consums_df[
                u'ImporteTotalSuplementoTerminoPotencia(€)'] = consums_df.apply(
                lambda x: sum_array_nan(
                    [x[partial_import.format(v)] for v in period_names]
                ), axis=1
            )

        # Calculo del total energia

        have_importes_periodes_energia_calulated = {
            u'ImporteSuplementoTerminoEnergia_P1(€)',
        }.issubset(consums_df.columns)

        empty_importes_periodes_energia_calulated = consums_df[
            u'ImporteSuplementoTerminoEnergia_P1(€)'
        ].isnull().values.all()

        if not have_importes_periodes_energia_calulated or empty_importes_periodes_energia_calulated:
            for p in period_names:
                valor_p_energy = (
                    u'ValorEnergiaActiva_{}(kWh)'.format(p)
                )
                suplemento_p_energy = (
                    u'SuplementoEnergia_P1(€/kWh)'.format(p)
                )

                total_p_energy = (
                    u'ImporteSuplementoTerminoEnergia_{}(€)'.format(p)
                )

                consums_df[total_p_energy] = consums_df.apply(
                    lambda x: mul_nan(x[valor_p_energy],
                                      x[suplemento_p_energy]),
                    axis=1
                )

        column_importe_total_energia_exist = {
            u'ImporteTotalSuplementoTerminoEnergia(€)',
        }.issubset(consums_df.columns)

        column_importe_total_energia_empty = consums_df[
            u'ImporteTotalSuplementoTerminoEnergia(€)'
        ].isnull().values.all()

        if not column_importe_total_energia_exist or column_importe_total_energia_empty:
            # sumar parcials
            partial_import = u'ImporteSuplementoTerminoEnergia_{}(€)'
            consums_df[
                u'ImporteTotalSuplementoTerminoEnergia(€)'] = consums_df.apply(
                lambda x: sum_array_nan(
                    [x[partial_import.format(v)] for v in period_names]
                ), axis=1
            )
            # Mirar si hi ha els imports sino calcular

        # Calulo del total regularizado

        column_importe_total_regularizado_exist = {
            u'ImporteTotalARegularizar(€)',
        }.issubset(consums_df.columns)

        column_importe_total_regularizado_empty = consums_df[
            u'ImporteTotalARegularizar(€)'
        ].isnull().values.all()

        if not column_importe_total_regularizado_exist or column_importe_total_regularizado_empty:
            consums_df[
                u'ImporteTotalARegularizar(€)'] = consums_df.apply(
                lambda x: sum(
                    [
                        x[u'ImporteTotalSuplementoTerminoPotencia(€)'],
                        x[u'ImporteTotalSuplementoTerminoEnergia(€)']
                    ]
                ), axis=1
            )

        consums_df.fillna(value=0, inplace=True)

WizardImportSupTerritorialsTec271Comer()
