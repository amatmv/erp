# -*- coding: utf-8 -*-
import json
from collections import OrderedDict
from datetime import datetime

from osv import osv, fields
import pooler
from gestionatr.defs import TABLA_107
from comentarios_suplementos_retrofix \
    import SuplementosTerritorialesCommentsComer as stcc
import numpy as np


class GiscedataSuplementsTerritorials2013Tec271Comer(osv.osv):
    _name = 'giscedata.suplements.territorials.2013.tec271.comer'

    _description = 'Suplementos Territoriales TEC271'

    _columns = {
        'cups': fields.char(u'CUPS', size=25, select=1),
        'tarifa_atr_fact': fields.selection(TABLA_107, u"Tarifa"),
        'fecha_desde': fields.date(u'Fecha Desde'),
        'fecha_hasta': fields.date(u'Fecha Hasta'),
        'media_pond_potencia_a_facturar_p1': fields.integer(
            'MediaPondPotenciaAFacturar_P1 (W)'
        ),
        'media_pond_potencia_a_facturar_p2': fields.integer(
            'MediaPondPotenciaAFacturar_P2 (W)'
        ),
        'media_pond_potencia_a_facturar_p3': fields.integer(
            'MediaPondPotenciaAFacturar_P3 (W)'
        ),
        'media_pond_potencia_a_facturar_p4': fields.integer(
            'MediaPondPotenciaAFacturar_P4 (W)'
        ),
        'media_pond_potencia_a_facturar_p5': fields.integer(
            'MediaPondPotenciaAFacturar_P5 (W)'
        ),
        'media_pond_potencia_a_facturar_p6': fields.integer(
            'MediaPondPotenciaAFacturar_P6 (W)'
        ),
        'importe_total_suplemento_termino_potencia': fields.float(
            'ImporteTotalSuplementoTerminoPotencia(€)', digits=(12, 2)
        ),
        'valor_energia_activa_p1': fields.float(
            'ValorEnergiaActiva_P1(kWh)', digits=(12, 2)
        ),
        'valor_energia_activa_p2': fields.float(
            'ValorEnergiaActiva_P2(kWh)', digits=(12, 2)
        ),
        'valor_energia_activa_p3': fields.float(
            'ValorEnergiaActiva_P3(kWh)', digits=(12, 2)
        ),
        'valor_energia_activa_p4': fields.float(
            'ValorEnergiaActiva_P4(kWh)', digits=(12, 2)
        ),
        'valor_energia_activa_p5': fields.float(
            'ValorEnergiaActiva_P5(kWh)', digits=(12, 2)
        ),
        'valor_energia_activa_p6': fields.float(
            'ValorEnergiaActiva_P6(kWh)', digits=(12, 2)
        ),
        'importe_total_suplemento_termino_energia': fields.float(
            'ImporteTotalSuplementoTerminoEnergia(€)', digits=(12, 2)
        ),
        'importe_total_a_regularizar': fields.float(
            'ImporteTotalARegularizar (€)', digits=(12, 2)
        ),
        'notificado': fields.boolean(string='Notificado'),
        'nombre_fichero': fields.char(
            string='Cargado en el fichero', size=500, readonly=True),
        'create_date': fields.datetime('Fecha de creación', readonly=True),
    }


    @staticmethod
    def get_info_html(cursor, uid, cups, with_total=True):
        pool = pooler.get_pool(cursor.dbname)
        sups_obj = pool.get('giscedata.suplements.territorials.2013.tec271.comer')

        def calcular_terminos_segun_importe(importe_total):
            n_termes = 1
            if 2.0 < importe_total <= 12.12:
                n_termes = int(importe_total)
            elif importe_total > 12.12:
                n_termes = 12
            return n_termes

        schema = sups_obj.get_info_data(cursor, uid, cups)
        if schema:
            values = schema['columns']
            periodes = schema['periods']
            fields = values.keys()
            html = '''
            <style type="text/css">
                table.supl, table.supl th, table.supl td {
                    border-collapse: collapse;
                    border-spacing: unset;
                    font-family:sans-serif;
                    font-size: 7px;
                }
                table.supl td, table.supl th {
                    border: 1px solid black;
                    padding: 2px;
                }
                .c {
                    text-align:center;
                }
            </style>
            <table class="supl">
            <tr>
                <th></th>
                <th>Importe (€)</th>
                <th></th>
                <th>P1</th>
                <th>P2</th>
                <th>P3</th>
                <th>P4</th>
                <th>P5</th>
                <th>P6</th>
            </tr>
            '''

            for i in range(periodes):
                html += sups_obj.get_report_html_for_period(
                    {j: values[j][i] for j in fields}
                )
            facturas = ''
            if with_total:
                terminos = calcular_terminos_segun_importe(
                    schema['total']
                )
                facturas = 'en un total de {} factura/s'.format(terminos)

            html += '''
                <tr>
                    <th>Importe Total</th>
                    <td style="text-align:right; border-right: hidden;">{total}€</td>
                    <td colspan="7" style="text-align:left;">{facturas}</td>
                </tr>
                </table>                
            '''.format(total=schema['total'], facturas=facturas)

            return html
        return ''

    @staticmethod
    def get_info_text(cursor, uid, cups):
        pool = pooler.get_pool(cursor.dbname)
        sups_obj = pool.get(
            'giscedata.suplements.territorials.2013.tec271.comer')
        cups_obj = pool.get('giscedata.cups.ps')

        schema = sups_obj.get_info_data(cursor, uid, cups)
        if schema:
            cups_name = cups[:20]
            search_params = [
                ('name', '=like', '{}%'.format(cups_name)),
                ('id_municipi.state.comunitat_autonoma.codi', '=', '09')
            ]
            sql_q = cups_obj.q(cursor, uid).select(['id']).where(search_params)
            cursor.execute(*sql_q)
            cups_ccaa = cursor.dictfetchall()
            if cups_ccaa:
                return '''<div class="supl">
                    Por ello {distribuidora} va a proceder
                    a regularizar las cantidades que resultan 
                    de la diferencia entre  la aplicación del valor fijado 
                    en concepto de suplemento territorial de dicha Comunidad 
                    Autónoma, respecto a las cantidades que provisionalmente 
                    fueron refacturadas en virtud de lo establecido 
                    en la Orden ETU 35/2017, de 23 de enero.
                    </div>'''
        return ''


    @staticmethod
    def get_info_json(cursor, uid, cups):
        pool = pooler.get_pool(cursor.dbname)
        sups_obj = pool.get('giscedata.suplements.territorials.2013.tec271.comer')
        # sups_ids = sups_obj.search(
        #     cursor, uid, {'cups': cups}, order='fecha_desde asc'
        # )

        schema = sups_obj.get_info_data(cursor, uid, cups)

        return json.dumps(schema)

    @staticmethod
    def get_info_data(cursor, uid, cups):
        pool = pooler.get_pool(cursor.dbname)
        sups_obj = pool.get('giscedata.suplements.territorials.2013.tec271.comer')
        cups_name = cups[:20]
        if not cups_name:
            return None
        search_params = [
            ('cups', '=like', '{}%'.format(cups_name))
        ]
        sups_ids = sups_obj.search(
            cursor, uid, search_params, order='fecha_desde asc'
        )

        if sups_ids:
            fields = OrderedDict([
                ('fecha_desde', []),
                ('fecha_hasta', []),
                # ('tarifa_atr_fact', []), # TODO remove?
                ('media_pond_potencia_a_facturar_p1', []),
                ('media_pond_potencia_a_facturar_p2', []),
                ('media_pond_potencia_a_facturar_p3', []),
                ('media_pond_potencia_a_facturar_p4', []),
                ('media_pond_potencia_a_facturar_p5', []),
                ('media_pond_potencia_a_facturar_p6', []),
                ('importe_total_suplemento_termino_potencia', []),
                ('valor_energia_activa_p1', []),
                ('valor_energia_activa_p2', []),
                ('valor_energia_activa_p3', []),
                ('valor_energia_activa_p4', []),
                ('valor_energia_activa_p5', []),
                ('valor_energia_activa_p6', []),
                ('importe_total_suplemento_termino_energia', []),
                ('importe_total_a_regularizar', [])
            ])

            schema = {
                'cups': cups,
                'columns': fields,
                'total': 0.0,
                'periods': len(sups_ids)
            }

            for sups_id in sups_ids:
                sups_values = sups_obj.read(cursor, uid, sups_id, fields.keys())
                for key in fields.keys():
                    value = sups_values[key]
                    schema['columns'][key].append(value)
                    if key == 'importe_total_a_regularizar':
                        schema['total'] += value
            return schema
        return None

    @staticmethod
    def get_report_html_for_period(period, show_zeroes=False):

        values_1_6_potencia = ''
        values_1_6_energia = ''
        for fieldname in sorted(period.keys()):
            value = str(period[fieldname])
            if not show_zeroes and period[fieldname] == 0:
                value = '-'
            if fieldname.startswith('media_pond'):
                values_1_6_potencia += '<td class="c">{}</td>'.format(value)
            elif fieldname.startswith('valor_energia_activa'):
                values_1_6_energia += '<td class="c">{}</td>'.format(value)

        html = '''
                <tr>
                    <th rowspan="2">Periodo del {}<br> al {}</th>
                    <td style="text-align:right;" rowspan="2">{}</td>
                    <th>Energía (kWh)</th>
                    {}
                </tr>
                <tr>
                    <th>Potencia (W)</th>
                    {}
                </tr>
            '''.format(
            datetime.strptime(
                period['fecha_desde'], '%Y-%m-%d'
            ).strftime('%d/%m/%Y'),
            datetime.strptime(
                period['fecha_hasta'], '%Y-%m-%d'
            ).strftime('%d/%m/%Y'),
            period['importe_total_a_regularizar'],
            values_1_6_energia,
            values_1_6_potencia
        )

        return html

    def check_if_suplemento_already_exist(self, cursor, uid, cups_name, context=None):
        '''
        Search if already exist an entry for current processing cups
        :param info:
        :return: True if exist else False
        '''
        return bool(
            self.search(
                cursor, uid,
                [('cups', 'ilike', '{}%'.format(cups_name[:20]))],
                context=context
            )
        )

    def auto_notification_suplementos_active(self, cursor, uid, context=None):
        '''
        Check if config far to auto noty is active
        '''
        conf_obj = self.pool.get('res.config')

        auto_mark_sup_as_notified = int(
            conf_obj.get(cursor, uid, 'noty_suplementos_f1', '1')
        )

        return bool(auto_mark_sup_as_notified)

    @staticmethod
    def extract_suplement_data_from_f1_info(nota, fecha_inicio_2013, fecha_final_2013, importe_suplemento):
        '''
        Extract required info from F1 to create suplement entry, returned value
        will be used to update info before create entry
        :param info:
        :return: dict with model keys filled with f1 info
        '''

        def get_periods_dict_pots(ori_data_dict, key_pattern):
            # Obtiene las potencias de un periodo
            res_dict = {}
            for period in range(1, 7):
                current_pot = ori_data_dict[key_pattern.format(period)]
                current_pot = current_pot.replace(u' ', u'')

                if current_pot:
                    current_pot = int(current_pot)
                else:
                    current_pot = 0

                res_dict[
                    u'P{}'.format(period)
                ] = current_pot
            return res_dict

        def pondered_pots(consumes_list, weights):
            # Obtain pondered dict from all periods for all P
            res_dict = {}
            for period in range(1, 7):
                res_key = u'media_pond_potencia_a_facturar_p{}'.format(period)
                # pots_current_period_list will contain pots of period
                # for all consumes
                pots_current_period_list = [
                    pot[2][u'P{}'.format(period)]
                    for pot in consumes_list
                ]

                pondered_average_p = np.average(
                    pots_current_period_list, weights=weights
                )

                res_dict[res_key] = int(round(pondered_average_p, 0))
            return res_dict

        def get_energy_from_parsed_note_dict(nota_dict):
            # parse '+0000001252773' to 12527.73 por all P
            res_dict = {}
            for period in range(1, 7):
                get_key = u'energia_activa_{}'.format(period)
                set_key = u'valor_energia_activa_p{}'.format(period)
                energy_period_value = nota_dict[get_key].replace(u' ', u'')
                if energy_period_value:
                    decimal_part = float(energy_period_value[-2:])/100.0
                    entry_part = float(energy_period_value[:-2])
                    energy_period_value = round(
                        float(entry_part + decimal_part), 2
                    )
                else:
                    energy_period_value = 0.0

                res_dict[set_key] = energy_period_value
            return res_dict

        nota_dict = stcc.extract_data_from_notas(nota)
        aprox_periods = []

        fecha_primer_cambio = nota_dict[u'fecha_primer_cambio']
        fecha_segundo_cambio = nota_dict[u'fecha_segundo_cambio']

        if not fecha_primer_cambio:
            pots_period_0 = get_periods_dict_pots(
                nota_dict, u'potencia_contratada_{}'
            )
            aprox_periods.append(
                (fecha_inicio_2013, fecha_final_2013, pots_period_0)
            )
        else:
            fecha_primer_cambio = fecha_primer_cambio.strftime(u'%Y-%m-%d')

            pots_period_0 = get_periods_dict_pots(
                nota_dict, u'potencia_contratada_{}'
            )

            aprox_periods.append(
                (fecha_inicio_2013, fecha_primer_cambio, pots_period_0)
            )

            if fecha_segundo_cambio:
                pots_period_1 = get_periods_dict_pots(
                    nota_dict, u'potencia_contratada_primer_cambio_{}'
                )

                pots_period_2 = get_periods_dict_pots(
                    nota_dict, u'potencia_contratada_segundo_cambio_{}'
                )

                fecha_segundo_cambio = fecha_segundo_cambio.strftime(
                    u'%Y-%m-%d'
                )

                aprox_periods.append(
                    (fecha_primer_cambio, fecha_segundo_cambio, pots_period_1)
                )
                aprox_periods.append(
                    (fecha_segundo_cambio, fecha_final_2013, pots_period_2)
                )
            else:
                pots_period_1 = get_periods_dict_pots(
                    nota_dict, u'potencia_contratada_primer_cambio_{}'
                )
                aprox_periods.append(
                    (fecha_primer_cambio, fecha_final_2013, pots_period_1)
                )

        weights = [
            (
                datetime.strptime(pp[1], u'%Y-%m-%d')
                -
                datetime.strptime(pp[0], u'%Y-%m-%d')
            ).days
            for pp in aprox_periods
        ]

        # potencias_ponderadas_dict contains:
        # {media_pond_potencia_a_facturar_p1..p6}
        potencias_ponderadas_dict = pondered_pots(
            aprox_periods, weights=weights
        )

        energy_parsed_dict = get_energy_from_parsed_note_dict(nota_dict)

        res = {
            u'fecha_desde': fecha_inicio_2013,
            u'fecha_hasta': fecha_final_2013,
            u'importe_total_suplemento_termino_potencia': False,
            u'importe_total_suplemento_termino_energia': False,
            u'importe_total_a_regularizar': importe_suplemento,
        }

        res.update(potencias_ponderadas_dict)
        res.update(energy_parsed_dict)

        return res

    def create_fake_suplemento_from_f1_info(self, cursor, uid, factura_id, vals, ori_comment, context=None):
        '''
        Si durante el proceso de regularización, se realiza un cambio de titular
        desde otra comer a la comercializadora que esta usando el ERP, es
        necesario guardar la información que se pueda de los suplementos para
        informar a los clientes en la factura,
        la información no sera del todo real, solamente el total a regularizar.

        :return: id of new entry or False if not created
        '''
        if context is None:
            context = {}

        res_ids = []

        fact_obj = self.pool.get('giscedata.facturacio.factura')
        cups_name = fact_obj.get_cups_name(
            cursor, uid, factura_id, context=context
        )

        # Miramos si existe un suplemento para el cups
        exist_suplement = self.check_if_suplemento_already_exist(
            cursor, uid, cups_name, context=context
        )

        if not exist_suplement:
            # Mira si la variable de configuracion esta activa
            auto_noty = self.auto_notification_suplementos_active(
                cursor, uid, context=context
            )

            create_suplemento_vals = {
                u'notificado': auto_noty,
                u'cups': cups_name,
                u'nombre_fichero': vals[u'notes'],
                u'tarifa_atr_fact': False
            }

            # Should be 362 size, if not is for a bug of gestionATR witch
            # strip comment, fuc da polis

            fecha_inicio_2013 = context.get(u'f_inicio_periodo', u'2012-12-31')
            fecha_final_2013 = context.get(u'f_fin_periodo', u'2013-12-31')
            importe_suplemento = context.get(u'con_price', 0.0)

            if u'f_inicio_periodo' in context:
                context.pop(u'f_inicio_periodo')

            if u'f_fin_periodo' in context:
                context.pop(u'f_fin_periodo')

            if u'con_price' in context:
                context.pop(u'con_price')

            # Devuelve la informacion del suplemento dividida para los cambios
            # de potencia
            suplements_update = self.extract_suplement_data_from_f1_info(
                ori_comment.ljust(362),
                fecha_inicio_2013, fecha_final_2013,
                importe_suplemento
            )

            suplements_update.update(create_suplemento_vals)
            res_id = self.create(
                cursor, uid, suplements_update, context=context
            )

            return res_id

        return False

GiscedataSuplementsTerritorials2013Tec271Comer()