# -*- encoding: utf-8 -*-
from retrofix import record, exception
from retrofix.fields import Date, Char, __all__

if 'DateNone' not in __all__:
    __all__.append('DateNone')


class DateNone(Date):
    def __init__(self, pattern):
        Date.__init__(self, pattern)

    def set_from_file(self, value):
        if value == '0' * len(value) or value == ' ' * len(value):
            return
        try:
            from datetime import datetime
            return datetime.strptime(value, self._pattern)
        except ValueError:
            raise exception.RetrofixException(
                'Invalid date value "%s" does not '
                'match pattern "%s" in field "%s"' % (
                    value, self._pattern, self._name)
            )


class SuplementosTerritorialesCommentsComer(object):
    COMMENTS = {
        (1, 4, 'extension_refacturacion', Char),  # Siempre 0000
        (5, 2, 'numero_faccion', Char),  # Fraccion terminos
        # Potencias contratadas P1, P2, P3, P4, P5, P6 en W
        # comienzo del periodo del año 2013
        (7, 14, 'potencia_contratada_1', Char),
        (21, 14, 'potencia_contratada_2', Char),
        (35, 14, 'potencia_contratada_3', Char),
        (49, 14, 'potencia_contratada_4', Char),
        (63, 14, 'potencia_contratada_5', Char),
        (77, 14, 'potencia_contratada_6', Char),
        # Potencias contratadas P1, P2, P3, P4, P5, P6 en W
        # primer cambio de potencia si se ha realizado
        (91, 14, 'potencia_contratada_primer_cambio_1', Char),
        (105, 14, 'potencia_contratada_primer_cambio_2', Char),
        (119, 14, 'potencia_contratada_primer_cambio_3', Char),
        (133, 14, 'potencia_contratada_primer_cambio_4', Char),
        (147, 14, 'potencia_contratada_primer_cambio_5', Char),
        (161, 14, 'potencia_contratada_primer_cambio_6', Char),
        # Potencias contratadas P1, P2, P3, P4, P5, P6 en W
        # segundo cambio de potencia si se ha realizado
        (175, 14, 'potencia_contratada_segundo_cambio_1', Char),
        (189, 14, 'potencia_contratada_segundo_cambio_2', Char),
        (203, 14, 'potencia_contratada_segundo_cambio_3', Char),
        (217, 14, 'potencia_contratada_segundo_cambio_4', Char),
        (231, 14, 'potencia_contratada_segundo_cambio_5', Char),
        (245, 14, 'potencia_contratada_segundo_cambio_6', Char),

        (259, 10, 'fecha_primer_cambio', DateNone('%Y-%m-%d')),
        (269, 10, 'fecha_segundo_cambio', DateNone('%Y-%m-%d')),

        # Energia activa 6 periodos 11 enteros 2 decimales en kWh
        (279, 14, 'energia_activa_1', Char),
        (293, 14, 'energia_activa_2', Char),
        (307, 14, 'energia_activa_3', Char),
        (321, 14, 'energia_activa_4', Char),
        (335, 14, 'energia_activa_5', Char),
        (349, 14, 'energia_activa_6', Char),
    }

    def __init__(self, pool, cursor, uid, context=None):
        self.pool = pool
        self.cursor = cursor
        self.uid = uid
        self.context = {} if context is None else context

    @staticmethod
    def extract_data_from_notas(notas):
        dict_nota = record.Record.extract(
            notas,
            SuplementosTerritorialesCommentsComer.COMMENTS
        ).__dict__['_values']

        return dict_nota
