# -*- coding: utf-8 -*-
{
    "name": "",
    "description": """Adaptacions per a la refacturació de suplements 
  territorials per a les comer, 2019 amb revisio TEC/271/2019.
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "giscedata_facturacio_comer",
        "giscedata_facturacio_switching",
        "giscedata_sup_territorials_2013_comer"
    ],
    "init_xml": [],
    "demo_xml":[
        "giscedata_sup_territorials_2013_comer_demo.xml"
    ],
    "update_xml":[
        "giscedata_sup_territorials_2013_tec271_comer_view.xml",
        "giscedata_sup_territorials_2013_tec271_comer_data.xml",
        "giscedata_sup_territorials_2013_tec271_comer_validation_data.xml",
        "giscedata_sup_territorials_2013_tec271_comer_data.xml",
        "wizard/wizard_import_sup_territorials_tec271_comer_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
