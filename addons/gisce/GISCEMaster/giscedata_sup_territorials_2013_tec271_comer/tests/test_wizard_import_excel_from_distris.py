# -*- coding: utf-8 -*-
from base64 import b64encode

from destral import testing
from destral.transaction import Transaction
from utils import *


class TestImportWizardF1TEC271(testing.OOTestCase):
    def setUp(self):
        self.openerp.install_module('giscedata_tarifas_peajes_20160101')

        self.txn = Transaction().start(self.database)

    def tearDown(self):
        self.txn.stop()

    def test_import_excel_file_create_suplementos(self):
        cursor = self.txn.cursor
        uid = self.txn.user
        txn = self.txn
        pool = self.openerp.pool

        wiz_import_excel_obj = pool.get(
            'wizard.import.sup.territorials.tec271.comer'
        )

        sup_tec271_obj = pool.get(
            'giscedata.suplements.territorials.2013.tec271.comer'
        )

        sup_ids = sup_tec271_obj.search(cursor, uid, [])

        sup_tec271_obj.unlink(cursor, uid, sup_ids)

        sup_ids = sup_tec271_obj.search(cursor, uid, [])

        self.assertFalse(sup_ids)

        xml_path = get_module_resource(
            'giscedata_sup_territorials_2013_tec271_comer',
            'tests', 'fixtures',
            'file_suplementos_from_distri.xlsx'
        )

        with open(xml_path, 'r') as f:
            excel_import_file = f.read()

        create_params = {
            'import_file': b64encode(excel_import_file),
            'file_name': 'file_suplementos_from_distri.xlsx',
        }

        wiz_id = wiz_import_excel_obj.create(cursor, uid, create_params)

        wiz_import_excel_obj.importar_file(cursor, uid, [wiz_id])

        sup_ids = sup_tec271_obj.search(cursor, uid, [])

        self.assertTrue(len(sup_ids), 1)

        sup_created_info = sup_tec271_obj.read(
            cursor, uid, sup_ids[0], []
        )

        self.assertEqual(sup_created_info['cups'], u'ES1234000000000001JN0F')
        self.assertEqual(sup_created_info['fecha_desde'], '2012-12-31')
        self.assertEqual(sup_created_info['fecha_hasta'], '2013-07-31')
