from addons import get_module_resource


def activar_polissa_cups(pool, txn):
    cursor = txn.cursor
    uid = txn.user

    imd_obj = pool.get('ir.model.data')

    polissa_obj = pool.get('giscedata.polissa')

    polissa_id = imd_obj.get_object_reference(
        cursor, uid, 'giscedata_polissa', 'polissa_0001'
    )[1]

    polissa_obj.send_signal(
        cursor, uid, [polissa_id], ['validar', 'contracte']
    )


def crear_modcon(pool, txn, potencia, ini, fi, tariff_id=None):
    polissa_obj = pool.get('giscedata.polissa')
    imd_obj = pool.get('ir.model.data')
    wz_crear_mc_obj = pool.get('giscedata.polissa.crear.contracte')

    cursor = txn.cursor
    uid = txn.user

    polissa_id = imd_obj.get_object_reference(
        cursor, uid, 'giscedata_polissa', 'polissa_0001'
    )[1]

    pol = polissa_obj.browse(cursor, uid, polissa_id)
    pol.send_signal(['modcontractual'])

    polissa_obj.write(cursor, uid, polissa_id, {'potencia': potencia})

    if tariff_id:
        polissa_obj.write(cursor, uid, polissa_id, {'tarifa': tariff_id})

    ctx = {'active_id': polissa_id}
    params = {'duracio': 'nou'}

    wz_id_mod = wz_crear_mc_obj.create(cursor, uid, params, ctx)
    wiz_mod = wz_crear_mc_obj.browse(cursor, uid, wz_id_mod, ctx)

    res = wz_crear_mc_obj.onchange_duracio(
        cursor, uid, [wz_id_mod], wiz_mod.data_inici, wiz_mod.duracio,
        ctx
    )

    print res  # todo revisar si update

    wiz_mod.write({
        'data_inici': ini,
        'data_final': fi
    })
    wiz_mod.action_crear_contracte(ctx)


def add_supported_iva_to_periods(pool, txn):
    tarifa_obj = pool.get('giscedata.polissa.tarifa')
    periode_obj = pool.get('giscedata.polissa.tarifa.periodes')
    tax_obj = pool.get('account.tax')
    product_obj = pool.get('product.product')

    cursor = txn.cursor
    uid = txn.user

    iva_id = tax_obj.search(
        cursor, uid, [
            ('name', '=', '21% IVA Soportado (operaciones corrientes)')
        ]
    )

    product_ids = []
    for tarifa_id in tarifa_obj.search(cursor, uid, []):
        periode_ids = tarifa_obj.read(
            cursor, uid, tarifa_id, ['periodes']
        )['periodes']
        for periode_vals in periode_obj.read(cursor, uid, periode_ids):
            product_ids.append(periode_vals['product_id'][0])
            if periode_vals['product_reactiva_id']:
                product_ids.append(periode_vals['product_reactiva_id'][0])
            if periode_vals['product_exces_pot_id']:
                product_ids.append(periode_vals['product_exces_pot_id'][0])

    product_obj.write(
        cursor, uid, product_ids, {
            'supplier_taxes_id': [(6, 0, iva_id)]
        }
    )


def add_supported_iva_to_concepts(pool, txn, iva='21'):
    tax_obj = pool.get('account.tax')
    product_obj = pool.get('product.product')

    cursor = txn.cursor
    uid = txn.user

    name = '{}% IVA Soportado (operaciones corrientes)'.format(iva)
    iva_id = tax_obj.search(
        cursor, uid, [
            ('name', '=', name)
        ]
    )

    no_iva_concepts = ['CON06', 'CON40', 'CON41', 'CON42']
    product_ids = product_obj.search(
        cursor, uid, [
            '|',
            ('default_code', 'like', 'CON'),
            ('default_code', 'like', 'ALQ'),
            ('default_code', 'not in', no_iva_concepts)
        ]
    )
    product_obj.write(
        cursor, uid, product_ids, {
            'supplier_taxes_id': [(6, 0, iva_id)]
        }
    )


def add_sell_taxes_to_suplemento_territorial(pool, txn):
    tax_obj = pool.get('account.tax')
    product_obj = pool.get('product.product')

    cursor = txn.cursor
    uid = txn.user

    tax_ids = tax_obj.search(
        cursor, uid, [
            '|',
            ('name', '=', 'IVA 21%'),
            ('name', '=', 'Impuesto especial sobre la electricidad')
        ]
    )

    product_ids = product_obj.search(
        cursor, uid, [
            ('default_code', 'in', ('TEC271', 'SETU35'))
        ]
    )

    product_obj.write(
        cursor, uid, product_ids, {
            'taxes_id': [(6, 0, tax_ids)]
        }
    )


def prepare_for_phase_3(pool, txn):
    polissa_obj = pool.get('giscedata.polissa')
    line_obj = pool.get('giscedata.facturacio.importacio.linia')
    pricelist_obj = pool.get('product.pricelist')

    cups_obj = pool.get('giscedata.cups.ps')
    partner_obj = pool.get('res.partner')
    address_obj = pool.get('res.partner.address')
    imd_obj = pool.get('ir.model.data')

    xml_path = get_module_resource(
        'giscedata_facturacio_switching', 'tests', 'fixtures',
        'test_correct.xml'
    )

    with open(xml_path, 'r') as f:
        xml_file = f.read()

    xml_file = xml_file.replace(
        '<CodigoREEEmpresaEmisora>1234</CodigoREEEmpresaEmisora>',
        '<CodigoREEEmpresaEmisora>0031</CodigoREEEmpresaEmisora>'
    )

    conceptes_xml_path = get_module_resource(
        'giscedata_facturacio_switching', 'tests', 'fixtures',
        'F1_conceptes.xml'
    )

    with open(conceptes_xml_path, 'r') as f:
        conceptes_xml_file = f.read()

    cursor = txn.cursor
    uid = txn.user

    cups_id = imd_obj.get_object_reference(
        cursor, uid, 'giscedata_cups', 'cups_01'
    )[1]
    polissa_id = imd_obj.get_object_reference(
        cursor, uid, 'giscedata_polissa', 'polissa_0001'
    )[1]
    import_id = imd_obj.get_object_reference(
        cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
    )[1]
    distri_id = partner_obj.search(
        cursor, uid, [('ref', '=', '0031')]
    )[0]
    pricelist_id = pricelist_obj.search(
        cursor, uid, [('name', '=', 'TARIFAS ELECTRICIDAD')]
    )[0]

    line_id = line_obj.create_from_xml(
        cursor, uid, import_id, 'Import Name', xml_file
    )

    line_obj.write(cursor, uid, line_id, {'cups_id': cups_id})

    polissa_obj.write(
        cursor, uid, polissa_id, {
            'cups': cups_id,
            'state': 'activa',
            'distribuidora': distri_id,
            'facturacio_potencia': 'icp'
        }
    )

    cups_obj.write(cursor, uid, cups_id, {'distribuidora_id': distri_id})

    address_obj.create(cursor, uid, {'partner_id': distri_id})
    partner_obj.write(
        cursor, uid, distri_id, {
            'property_product_pricelist_purchase': pricelist_id
        }
    )

    activar_polissa_cups(pool, txn)

    add_supported_iva_to_periods(pool, txn)
    add_supported_iva_to_concepts(pool, txn)
    add_sell_taxes_to_suplemento_territorial(pool, txn)

    return {
        'polissa_id': polissa_id,
        'f1_xml_file': xml_file,
        'concepts_xml_file': conceptes_xml_file,
        'import_id': import_id,
        'line_id': line_id
    }


def create_invoice_extra_line_49(pool, txn, dades_linia):
    cursor = txn.cursor
    uid = txn.user
    txn = txn

    extraline_obj = pool.get('giscedata.facturacio.extra')

    tax_obj = pool.get('account.tax')

    tax_ids = tax_obj.search(
        cursor, uid, [
            ('name', '=', 'IVA 21%'),
        ]
    )
    polissa_id = dades_linia['polissa_id']

    product_obj = pool.get('product.product')
    product_tec271 = product_obj.search(
        cursor, uid, [('default_code', '=', 'TEC271')]
    )[0]

    product = product_obj.browse(cursor, uid, product_tec271)
    if product.product_tmpl_id.property_account_income:
        account = product.product_tmpl_id.property_account_income.id
    else:
        account = product.categ_id.property_account_income_categ.id
    diari_obj = pool.get('account.journal')
    diari_id = diari_obj.search(cursor, uid, [('code', '=', 'ENERGIA')])[0]
    termes = dades_linia['terms']
    extraline_vals = {
        'account_id': account,
        'journal_ids': [[6, 0, [diari_id]]],
        'product_id': product_tec271,
        'price_unit': dades_linia['total'],
        'name': product.description,
        'date_line_from': '2013-01-01',
        'date_line_to': '2013-12-31',
        'date_from': dades_linia['date_from'],
        'date_to': dades_linia['date_to'],
        'polissa_id': polissa_id,
        'term': termes,
        'taxs_id': [(6, 0, tax_ids)],
        'notes': 'EMPTY',
    }

    return extraline_obj.create(cursor, uid, extraline_vals)
