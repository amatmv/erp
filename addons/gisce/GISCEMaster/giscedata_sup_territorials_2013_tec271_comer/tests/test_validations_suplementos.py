# -*- coding: utf-8 -*-

from addons import get_module_resource
from destral import testing
from destral.transaction import Transaction
from utils import *


class TestValidationLotTEC271(testing.OOTestCase):
    def setUp(self):
        self.txn = Transaction().start(self.database)

        cursor = self.txn.cursor
        uid = self.txn.user

        sup_tec271_obj = self.openerp.pool.get(
            'giscedata.suplements.territorials.2013.tec271.comer'
        )

        sup_ids = sup_tec271_obj.search(cursor, uid, [])

        sup_tec271_obj.unlink(cursor, uid, sup_ids)

        sup_ids = sup_tec271_obj.search(cursor, uid, [])

        self.assertFalse(sup_ids)

    def tearDown(self):
        self.txn.stop()

    def test_sups_2013_TEC271_1_validation(self):
        cursor = self.txn.cursor
        uid = self.txn.user

        imd_obj = self.openerp.pool.get('ir.model.data')
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        extra_line_obj = self.openerp.pool.get('giscedata.facturacio.extra')
        sup_territorials_2013_comer_obj = self.openerp.pool.get(
            'giscedata.suplements.territorials.2013.tec271.comer'
        )

        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.validation.validator'
        )

        factura_id = imd_obj.get_object_reference(
            cursor, uid,
            'giscedata_facturacio',
            'factura_0007'
        )[1]

        factura = fact_obj.browse(cursor, uid, factura_id)

        factura.write({'data_final': '2017-09-20'})

        dades_linia = {
            'polissa_id': factura.polissa_id.id,
            'date_from': '2017-09-06',
            'date_to': '2017-09-06',
            'price_unit': 1,
            'terms': 1,
            'total': 1,
        }
        line_id_1 = create_invoice_extra_line_49(
            self.openerp.pool, self.txn, dades_linia
        )

        extra_line_obj.compute_extra_lines(cursor, uid, [factura_id])

        contract_cups = factura.cups_id.name

        self.assertTrue(
            validator_obj.check_cups_exist_etu271(
                cursor, uid, factura, {}
            )
        )

        sup_id = sup_territorials_2013_comer_obj.create(
            cursor, uid, {'cups': contract_cups}
        )

        self.assertEquals(
            validator_obj.check_cups_exist_etu271(
                cursor, uid, factura, {}
            ), None
        )

        self.assertTrue(
            validator_obj.check_cups_not_notificated_etu271(
                cursor, uid, factura, {}
            )
        )

        sup_territorials_2013_comer_obj.write(
            cursor, uid, [sup_id], {'notificado': True}
        )

        self.assertEquals(
            validator_obj.check_cups_not_notificated_etu271(
                cursor, uid, factura, {}
            ), None
        )

    def test_sups_2013_TEC271_2_validation(self):
        cursor = self.txn.cursor
        uid = self.txn.user
        txn = self.txn
        pool = self.openerp.pool

        imd_obj = pool.get('ir.model.data')
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        extra_line_obj = pool.get('giscedata.facturacio.extra')
        validator_obj = pool.get(
            'giscedata.facturacio.validation.validator'
        )
        factura_id = imd_obj.get_object_reference(
            cursor, uid,
            'giscedata_facturacio',
            'factura_0007'
        )[1]
        factura = fact_obj.browse(cursor, uid, factura_id)

        factura.write({'data_final': '2017-09-20'})

        dades_linia = {
            'polissa_id': factura.polissa_id.id,
            'date_from': '2017-09-06',
            'date_to': '2017-09-06',
            'price_unit': 1,
            'terms': 1,
            'total': 1,
        }

        line_id_1 = create_invoice_extra_line_49(pool, txn, dades_linia)

        # Test without extra line
        self.assertEquals(validator_obj.check_duplicates_sups_etu271(
            cursor, uid, factura, {}
        ), None)

        extra_line_obj.compute_extra_lines(cursor, uid, [factura_id])
        # Test with one extra line
        self.assertEquals(validator_obj.check_duplicates_sups_etu271(
            cursor, uid, factura, {}
        ), None)

        line_id_2 = create_invoice_extra_line_49(pool, txn, dades_linia)

        extra_line_obj.compute_extra_lines(cursor, uid, [factura_id])
        # Test with more than one extra line
        self.assertTrue(validator_obj.check_duplicates_sups_etu271(
            cursor, uid, factura, {}
        ))
