# -*- coding: utf-8 -*-
import unittest
import base64

from destral import testing
from destral.transaction import Transaction
from expects import *
from osv.osv import except_osv
from addons import get_module_resource
from gestionatr.input.messages import F1
from datetime import datetime
from utils import *
from ..giscedata_sup_territorials_2013_tec271_comer import GiscedataSuplementsTerritorials2013Tec271Comer as GSTC
from json import loads


class TestImportF1TEC271(testing.OOTestCase):
    def setUp(self):
        # En el año 2100 dejara de funcionar este test
        current_year = datetime.today().year % 1000 + 1
        for year in range(16, current_year):
            self.openerp.install_module(
                'giscedata_tarifas_peajes_20{}0101'.format(str(year).zfill(2))
            )

        self.txn = Transaction().start(self.database)
        self.set_ref_company()
        self.delete_suplements_tec271()

    def tearDown(self):
        self.txn.stop()

    def set_ref_company(self):
        cursor = self.txn.cursor
        uid = self.txn.user
        user_obj = self.openerp.pool.get('res.users')
        partner_obj = self.openerp.pool.get('res.partner')
        partner_obj.write(
            cursor, uid,
            user_obj.browse(cursor, uid, uid).company_id.partner_id.id,
            {
                'ref': '4321'
            }
        )

    def delete_suplements_tec271(self):
        cursor = self.txn.cursor
        uid = self.txn.user

        q = 'DELETE FROM giscedata_suplements_territorials_2013_tec271_comer'

        cursor.execute(q)

    def create_modcon(self, cursor, uid, polissa_id, date_from, date_to=False, context=None):

        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        imd_obj = self.openerp.pool.get('ir.model.data')
        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]
        pol = polissa_obj.browse(cursor, uid, polissa_id)
        pol.send_signal(['modcontractual'])
        # polissa_obj.write(cursor, uid, polissa_id, {'potencia': potencia})
        wz_crear_mc_obj = self.openerp.pool.get('giscedata.polissa.crear.contracte')

        ctx = {'active_id': polissa_id}
        params = {'duracio': 'nou'}

        wz_id_mod = wz_crear_mc_obj.create(cursor, uid, params, ctx)
        wiz_mod = wz_crear_mc_obj.browse(cursor, uid, wz_id_mod, ctx)
        res = wz_crear_mc_obj.onchange_duracio(
            cursor, uid, [wz_id_mod], wiz_mod.data_inici, wiz_mod.duracio,
            ctx
        )

        mod_create = {
            'data_inici': date_from,
            'data_final': date_to
        }

        mod_create.update(res.get('value', {}))

        wiz_mod.write(mod_create)

        wiz_mod.action_crear_contracte(ctx)

    def test_import_extra_lines_correctly_from_f1(self):
        '''
        Estado inicial:
            - Entorno sin suplementos creados
            - F1 con concepto 49 sin cambios de potencia
              (f1_suplemento_territorial_tec271.xml)
        Proceso:
            - Comprueba que no haya suplementos
            - Importa un F1 con concepto 49 (suplementos)
            - Comprobamos que haya creado el suplemento en base la
              información del comentario del concepto
        '''
        cursor = self.txn.cursor
        uid = self.txn.user
        txn = self.txn
        pool = self.openerp.pool

        line_obj = pool.get(
            'giscedata.facturacio.importacio.linia'
        )

        error_obj = pool.get(
            'giscedata.facturacio.switching.error'
        )

        imd_obj = pool.get('ir.model.data')

        tec271_obj = pool.get(
            'giscedata.suplements.territorials.2013.tec271.comer'
        )

        suplement_creat_id = tec271_obj.search(cursor, uid, [])
        self.assertFalse(suplement_creat_id)

        res = prepare_for_phase_3(pool, txn)

        xml_path = get_module_resource(
            'giscedata_sup_territorials_2013_tec271_comer',
            'tests', 'fixtures',
            'f1_suplemento_territorial_tec271.xml'
        )

        with open(xml_path, 'r') as f:
            xml_file = f.read()
        xml_file = xml_file.replace(
            '<CodigoREEEmpresaEmisora>1234</CodigoREEEmpresaEmisora>',
            '<CodigoREEEmpresaEmisora>0031</CodigoREEEmpresaEmisora>'
        )

        import_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
        )[1]
        line_id = line_obj.create_from_xml(
            cursor, uid, import_id, 'Import Name', xml_file
        )

        line_obj.process_line_sync(cursor, uid, line_id)
        line_vals = line_obj.browse(cursor, uid, line_id)
        expect(line_vals.state).to(equal('valid'))
        expect(line_vals.import_phase).to(equal(40))

        expect(len(line_vals.liniaextra_id)).to(equal(1))

        extra_line = line_vals.liniaextra_id[0]

        expect(len(extra_line.tax_ids)).to(equal(2))
        expect(
            {tax.name for tax in extra_line.tax_ids}
        ).to(
            equal(
                {'IVA 21%', 'Impuesto especial sobre la electricidad'}
            )
        )
        expect(extra_line.quantity).to(equal(1.0))
        expect(extra_line.price_unit).to(equal(1.66))
        expect(extra_line.price_subtotal).to(equal(1.66))
        self.assertEqual(
            extra_line.name,
            (
                u'Suplement territorial per tributs autonòmics de '
                u'la Comunitat Autònoma País Vasco de l\'any 2013'
            )
        )

        expected_product_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio_comer', 'concepte_49'
        )[1]

        self.assertEqual(extra_line.product_id.id, expected_product_id)

        suplement_creat_id = tec271_obj.search(cursor, uid, [])
        self.assertEqual(len(suplement_creat_id), 1)

        suplement_creat_id = suplement_creat_id[0]

        sup_vals = tec271_obj.read(cursor, uid, suplement_creat_id, [])

        # PrecioUnidadConceptoRepercutible = Total a regularizar
        self.assertTrue(sup_vals['importe_total_a_regularizar'] == 20.75)

    def test_import_extra_lines_correctly_from_f1_one_pow_change(self):
        '''
        Estado inicial:
            - Entorno sin suplementos creados
            - F1 con concepto 49 con un cambio de potencia
              (f1_suplemento_territorial_tec271_one_pot_change.xml)
        Proceso:
            - Comprueba que no haya suplementos
            - Importa un F1 con concepto 49 (suplementos)
            - Comprobamos que haya creado el suplemento en base la
              información del comentario del concepto
        '''
        cursor = self.txn.cursor
        uid = self.txn.user
        txn = self.txn
        pool = self.openerp.pool

        line_obj = pool.get(
            'giscedata.facturacio.importacio.linia'
        )

        error_obj = pool.get(
            'giscedata.facturacio.switching.error'
        )

        imd_obj = pool.get('ir.model.data')

        tec271_obj = pool.get(
            'giscedata.suplements.territorials.2013.tec271.comer'
        )

        suplement_creat_id = tec271_obj.search(cursor, uid, [])
        self.assertFalse(suplement_creat_id)

        res = prepare_for_phase_3(pool, txn)

        xml_path = get_module_resource(
            'giscedata_sup_territorials_2013_tec271_comer',
            'tests', 'fixtures',
            'f1_suplemento_territorial_tec271_one_pot_change.xml'
        )

        with open(xml_path, 'r') as f:
            xml_file = f.read()
        xml_file = xml_file.replace(
            '<CodigoREEEmpresaEmisora>1234</CodigoREEEmpresaEmisora>',
            '<CodigoREEEmpresaEmisora>0031</CodigoREEEmpresaEmisora>'
        )

        import_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
        )[1]
        line_id = line_obj.create_from_xml(
            cursor, uid, import_id, 'Import Name', xml_file
        )

        line_obj.process_line_sync(cursor, uid, line_id)
        line_vals = line_obj.browse(cursor, uid, line_id)
        expect(line_vals.state).to(equal('valid'))
        expect(line_vals.import_phase).to(equal(40))

        expect(len(line_vals.liniaextra_id)).to(equal(1))

        extra_line = line_vals.liniaextra_id[0]

        expect(len(extra_line.tax_ids)).to(equal(2))
        expect(
            {tax.name for tax in extra_line.tax_ids}
        ).to(
            equal(
                {'IVA 21%', 'Impuesto especial sobre la electricidad'}
            )
        )
        expect(extra_line.quantity).to(equal(1.0))
        expect(extra_line.price_unit).to(equal(1.66))
        expect(extra_line.price_subtotal).to(equal(1.66))
        self.assertEqual(
            extra_line.name,
            (
                u'Suplement territorial per tributs autonòmics de '
                u'la Comunitat Autònoma País Vasco de l\'any 2013'
            )
        )

        expected_product_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio_comer', 'concepte_49'
        )[1]

        self.assertEqual(extra_line.product_id.id, expected_product_id)

        suplement_creat_id = tec271_obj.search(cursor, uid, [])
        self.assertEqual(len(suplement_creat_id), 1)

        suplement_creat_id = suplement_creat_id[0]

        sup_vals = tec271_obj.read(cursor, uid, suplement_creat_id, [])

        # PrecioUnidadConceptoRepercutible = Total a regularizar
        self.assertTrue(sup_vals['importe_total_a_regularizar'] == 20.75)

    def test_import_extra_lines_correctly_from_f1_two_pow_change(self):
        '''
        Estado inicial:
            - Entorno sin suplementos creados
            - F1 con concepto 49 con dos cambios de potencia
              (f1_suplemento_territorial_tec271_two_pow_changes.xml)
        Proceso:
            - Comprueba que no haya suplementos
            - Importa un F1 con concepto 49 (suplementos)
            - Comprobamos que haya creado el suplemento en base la
              información del comentario del concepto
        '''
        cursor = self.txn.cursor
        uid = self.txn.user
        txn = self.txn
        pool = self.openerp.pool

        line_obj = pool.get(
            'giscedata.facturacio.importacio.linia'
        )

        error_obj = pool.get(
            'giscedata.facturacio.switching.error'
        )

        imd_obj = pool.get('ir.model.data')

        tec271_obj = pool.get(
            'giscedata.suplements.territorials.2013.tec271.comer'
        )

        suplement_creat_id = tec271_obj.search(cursor, uid, [])
        self.assertFalse(suplement_creat_id)

        res = prepare_for_phase_3(pool, txn)

        xml_path = get_module_resource(
            'giscedata_sup_territorials_2013_tec271_comer',
            'tests', 'fixtures',
            'f1_suplemento_territorial_tec271_two_pow_changes.xml'
        )

        with open(xml_path, 'r') as f:
            xml_file = f.read()
        xml_file = xml_file.replace(
            '<CodigoREEEmpresaEmisora>1234</CodigoREEEmpresaEmisora>',
            '<CodigoREEEmpresaEmisora>0031</CodigoREEEmpresaEmisora>'
        )

        import_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
        )[1]
        line_id = line_obj.create_from_xml(
            cursor, uid, import_id, 'Import Name', xml_file
        )

        line_obj.process_line_sync(cursor, uid, line_id)
        line_vals = line_obj.browse(cursor, uid, line_id)
        expect(line_vals.state).to(equal('valid'))
        expect(line_vals.import_phase).to(equal(40))

        expect(len(line_vals.liniaextra_id)).to(equal(1))

        extra_line = line_vals.liniaextra_id[0]

        expect(len(extra_line.tax_ids)).to(equal(2))
        expect(
            {tax.name for tax in extra_line.tax_ids}
        ).to(
            equal(
                {'IVA 21%', 'Impuesto especial sobre la electricidad'}
            )
        )
        expect(extra_line.quantity).to(equal(1.0))
        expect(extra_line.price_unit).to(equal(1.66))
        expect(extra_line.price_subtotal).to(equal(1.66))
        self.assertEqual(
            extra_line.name,
            (
                u'Suplement territorial per tributs autonòmics de '
                u'la Comunitat Autònoma País Vasco de l\'any 2013'
            )
        )

        expected_product_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio_comer', 'concepte_49'
        )[1]

        self.assertEqual(extra_line.product_id.id, expected_product_id)

        suplement_creat_id = tec271_obj.search(cursor, uid, [])
        self.assertEqual(len(suplement_creat_id), 1)

        suplement_creat_id = suplement_creat_id[0]

        sup_vals = tec271_obj.read(cursor, uid, suplement_creat_id, [])

        # PrecioUnidadConceptoRepercutible = Total a regularizar
        self.assertTrue(sup_vals['importe_total_a_regularizar'] == 20.75)

    def test_parse_nota(self):
        '''
        Proceso:
            - Comprueba que la función extract_suplement_data_from_f1_info
            devuelve correctamente parseada la información para los distintos
            casos de nota del F1 en los suplementos:

                - Sin cambios de potencia 2.0A +1 periodo
                - 1 cambio de potencia 2.0A +1 periodo
                - 2 cambios de potencia 3.0A - +3 periodos
        '''
        fake_import = 420.0

        start_gate = u'2012-12-31'
        end_game = u'2013-12-31'

        comment_0_changes = (
            u'00000100000000003300                                          '
            u'                                                              '
            u'                                                              '
            u'                                                              '
            u'                              +0000001252773                  '
            u'                                                    '
        )

        comment_1_changes = (
            u'00000100000000013943                                          '
            u'                            00000000007967                    '
            u'                                                              '
            u'                                                              '
            u'          2013-02-01          +0000000061100+0000000643800    '
            u'                                                    '
        )

        comment_2_changes = (
            u'000001000000000235560000000002355600000000023556'
            u'                                          '
            u'000000000280000000000002355600000000023556      '
            u'                                    '
            u'000000000235560000000002400000000000023556      '
            u'                                    '
            u'2013-02-282013-11-30'
            u'+0000001252773+0000001252773+0000001252773'
            u'                                          '
        )

        no_pot_change_res = GSTC.extract_suplement_data_from_f1_info(
            comment_0_changes, start_gate, end_game, fake_import
        )

        self.assertEqual(no_pot_change_res[u'fecha_desde'], u'2012-12-31')
        self.assertEqual(no_pot_change_res[u'fecha_hasta'], u'2013-12-31')
        self.assertEqual(
            no_pot_change_res[u'valor_energia_activa_p1'], 12527.73
        )
        self.assertEqual(
            no_pot_change_res[u'media_pond_potencia_a_facturar_p1'], 3300
        )

        one_pot_change_res = GSTC.extract_suplement_data_from_f1_info(
            comment_1_changes, start_gate, end_game, fake_import
        )
        self.assertEqual(
            one_pot_change_res[u'valor_energia_activa_p1'], 611.0
        )
        # (13943. * 32. + 7967. * 333.) / (32. + 333.) -> Round 0 = 8491
        self.assertEqual(
            one_pot_change_res[u'media_pond_potencia_a_facturar_p1'], 8491
        )

        two_pot_change_res = GSTC.extract_suplement_data_from_f1_info(
            comment_2_changes, start_gate, end_game, fake_import
        )

        self.assertEqual(
            two_pot_change_res[u'valor_energia_activa_p1'], 12527.73
        )

        # (23556. * 59. + 28000. * 275. + 23556 * 31) / (59. + 275. + 31.)
        # -> Round 0 = 26904
        self.assertEqual(
            two_pot_change_res[u'media_pond_potencia_a_facturar_p1'], 26904
        )

    def test_import_f1_no_pot_change_with_suplement_already_created(self):
        '''
        Estado inicial:
            - Entorno con un suplemento creado
            - F1 con concepto 49 sin cambios de potencia con el mismo cups que
              el suplemento creado
              (f1_suplemento_territorial_tec271.xml)
        Proceso:
            - Importa un F1 con concepto 49 (suplementos)
            - Crea suplemento con el cups de la importación
            - process_line_sync
            - Comprobamos que no haya creado un nuevo suplemento
        '''
        cursor = self.txn.cursor
        uid = self.txn.user
        txn = self.txn
        pool = self.openerp.pool

        line_obj = pool.get(
            'giscedata.facturacio.importacio.linia'
        )

        fact_obj = pool.get('giscedata.facturacio.factura')
        fact_line_obj = pool.get('giscedata.facturacio.factura.linia')

        error_obj = pool.get(
            'giscedata.facturacio.switching.error'
        )

        imd_obj = pool.get('ir.model.data')

        tec271_obj = pool.get(
            'giscedata.suplements.territorials.2013.tec271.comer'
        )

        res = prepare_for_phase_3(pool, txn)

        xml_path = get_module_resource(
            'giscedata_sup_territorials_2013_tec271_comer',
            'tests', 'fixtures',
            'f1_suplemento_territorial_tec271.xml'
        )

        with open(xml_path, 'r') as f:
            xml_file = f.read()
        xml_file = xml_file.replace(
            '<CodigoREEEmpresaEmisora>1234</CodigoREEEmpresaEmisora>',
            '<CodigoREEEmpresaEmisora>0031</CodigoREEEmpresaEmisora>'
        )

        import_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
        )[1]
        line_id = line_obj.create_from_xml(
            cursor, uid, import_id, 'Import Name', xml_file
        )

        cups_line = line_obj.read(
            cursor, uid, line_id, ['cups_text']
        )['cups_text']

        suplement_creat_id_ori = tec271_obj.create(
            cursor, uid,
            {
                u'cups': cups_line,
                u'fecha_desde': u'2012-12-31',
                u'fecha_hasta': u'2013-12-31',
                u'tarifa_atr_fact': u'001',
                u'importe_total_suplemento_termino_potencia': 10.,
                u'importe_total_suplemento_termino_energia': 10.75,
                u'importe_total_a_regularizar': 20.75,
                u'media_pond_potencia_a_facturar_p1': 2000,
                u'media_pond_potencia_a_facturar_p2': 0,
                u'media_pond_potencia_a_facturar_p3': 0,
                u'media_pond_potencia_a_facturar_p4': 0,
                u'media_pond_potencia_a_facturar_p5': 0,
                u'media_pond_potencia_a_facturar_p6': 0,
                u'valor_energia_activa_p1': 3000.,
                u'valor_energia_activa_p2': 0.,
                u'valor_energia_activa_p3': 0.,
                u'valor_energia_activa_p4': 0.,
                u'valor_energia_activa_p5': 0.,
                u'valor_energia_activa_p6': 0.,
            }
        )

        suplement_creat_id = tec271_obj.search(cursor, uid, [])
        self.assertTrue(len(suplement_creat_id) == 1)

        suplement_creat_id = suplement_creat_id[0]

        self.assertEqual(suplement_creat_id_ori, suplement_creat_id)

        line_obj.process_line_sync(cursor, uid, line_id)
        line_vals = line_obj.browse(cursor, uid, line_id)

        cups_sup = tec271_obj.read(
            cursor, uid, suplement_creat_id, ['cups']
        )['cups']

        self.assertEqual(cups_line, cups_sup)

        expect(line_vals.state).to(equal('valid'))
        expect(line_vals.import_phase).to(equal(40))

        expect(len(line_vals.liniaextra_id)).to(equal(1))

        extra_data = line_vals.liniaextra_id[0].read(['polissa_id', 'journal_ids'])[0]

        polissa_id = extra_data['polissa_id'][0]

        journal_id = extra_data['journal_ids'][0]

        extra_line = line_vals.liniaextra_id[0]

        expect(len(extra_line.tax_ids)).to(equal(2))
        expect(
            {tax.name for tax in extra_line.tax_ids}
        ).to(
            equal(
                {'IVA 21%', 'Impuesto especial sobre la electricidad'}
            )
        )
        expect(extra_line.quantity).to(equal(1.0))
        expect(extra_line.price_unit).to(equal(1.66))
        expect(extra_line.price_subtotal).to(equal(1.66))
        self.assertEqual(
            extra_line.name,
            (
                u'Suplement territorial per tributs autonòmics de '
                u'la Comunitat Autònoma País Vasco de l\'any 2013'
            )
        )

        expected_product_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio_comer', 'concepte_49'
        )[1]

        self.assertEqual(extra_line.product_id.id, expected_product_id)

        # Nos aseguramos que no cree otro suplemento
        suplement_creat_id = tec271_obj.search(cursor, uid, [])
        self.assertEqual(len(suplement_creat_id), 1)

        suplement_creat_id = suplement_creat_id[0]

        sup_vals = tec271_obj.read(cursor, uid, suplement_creat_id, [])

        # PrecioUnidadConceptoRepercutible = Total a regularizar
        self.assertTrue(sup_vals['importe_total_a_regularizar'] == 20.75)

        # Test invoice open with suplementos line
        date_start = datetime(year=2017, month=2, day=1).strftime('%Y-%m-%d')
        date_end = datetime.today().strftime('%Y-%m-%d')

        self.create_modcon(cursor, uid, polissa_id, date_start, date_to=date_end,
                           context=None)

        wiz_fact_manual_obj = pool.get('wizard.manual.invoice')
        wiz_fact_manual_id = wiz_fact_manual_obj.create(cursor, uid, vals={
            'polissa_id': polissa_id,
            'date_start': date_start,
            'date_end': date_end,
            'date_invoice': datetime.today().strftime('%Y-%m-%d'),
            'journal_id': journal_id
        })

        pricelist_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio',
            'pricelist_tarifas_electricidad'
        )[1]

        context = {'llista_preu': pricelist_id}

        #   -   Create regular invoice within ExtraLine's dates

        wiz_fact_manual_obj.action_manual_invoice(
            cursor, uid, [wiz_fact_manual_id], context)

        fact_id = wiz_fact_manual_obj.read(
            cursor, uid, wiz_fact_manual_id, ['invoice_ids']
        )[0]['invoice_ids']
        fact_id = loads(fact_id)[0]

        line_ids = fact_obj.read(
            cursor, uid, fact_id, ['linia_ids']
        )['linia_ids']

        line_ids = fact_line_obj.search(
            cursor, uid,
            [
                ('id', 'in', line_ids),
                ('product_id', '=', expected_product_id)
            ]
        )

        self.assertTrue(len(line_ids), 1)

        line_info = fact_line_obj.read(cursor, uid, line_ids, [])[0]

        self.assertEqual(extra_line.price_unit, line_info['price_subtotal'])

        fact_obj.button_reset_taxes(cursor, uid, [fact_id])

        fact_obj.invoice_open(cursor, uid, [fact_id])

        fact_info = fact_obj.read(cursor, uid, fact_id, [])

        self.assertTrue(fact_info['state'], u'open')
