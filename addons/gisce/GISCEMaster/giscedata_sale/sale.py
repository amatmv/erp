# -*- encoding: utf-8 -*-
from osv import fields, osv


class sale_order(osv.osv):
    _name = "sale.order"
    _inherit = "sale.order"

    def onchange_warehouse_id(self, cr, uid, ids, warehouse_id, context=None):
        if not warehouse_id:
            return {}
        warehouse_obj = self.pool.get('stock.warehouse')
        warehouse_f = ['lot_stock_id', 'lot_output_id']
        res = warehouse_obj.read(
            cr, uid, warehouse_id, warehouse_f, context=context
        )
        location_id = res['lot_stock_id'][0]
        location_dest_id = res['lot_output_id'][0]
        template = {
            'value': {
                'location_id': location_id,
                'location_dest_id': location_dest_id
            },
            'domain': {
                'location_id': [('id', 'child_of', [location_id])]
            }
        }
        return template

    def onchange_shop_id(self, cr, uid, ids, shop_id):
        template = super(sale_order, self).onchange_shop_id(
            cr, uid, ids, shop_id
        )
        if shop_id:
            shop_f = ['warehouse_id.id', 'warehouse_id.lot_stock_id',
                      'warehouse_id.lot_output_id']
            dmn = [('id', '=', shop_id)]
            q = self.pool.get('sale.shop').q(cr, uid).select(shop_f).where(dmn)
            cr.execute(*q)
            shop_v = cr.dictfetchall()[0]

            template['value'].update({
                'warehouse_id': shop_v['warehouse_id.id'],
                'location_id': shop_v['warehouse_id.lot_stock_id'],
                'location_dest_id': shop_v['warehouse_id.lot_output_id'],
            })

        return template

    def action_ship_create_stock_move_if_type_product_or_consu(
            self, cursor, uid, ids, line, picking_id, date_planned, product_uos,
            order, context=None
    ):
        stock_move_o = self.pool.get('stock.move')

        stock_move_id = stock_move_o.create(cursor, uid, {
            'name': line.name[:64],
            'picking_id': picking_id,
            'product_id': line.product_id.id,
            'date_planned': date_planned,
            'product_qty': line.product_uom_qty,
            'product_uom': line.product_uom.id,
            'product_uos_qty': line.product_uos_qty,
            'product_uos': product_uos,
            'product_packaging': line.product_packaging.id,
            'address_id': line.address_allotment_id.id or order.partner_shipping_id.id,
            'location_id': order.location_id.id,
            'location_dest_id': order.location_dest_id.id,
            'sale_line_id': line.id,
            'tracking_id': False,
            'state': 'draft',
            'note': line.notes,
        })
        return stock_move_id

    _columns = {
        'warehouse_id': fields.many2one(
            'stock.warehouse', 'Almacén', required=True, readonly=True,
            states={'draft': [('readonly', False)]}
        ),
        'location_id': fields.many2one(
            'stock.location', 'Ubicación', required=True, readonly=True,
            states={'draft': [('readonly', False)]}
        ),
        'location_dest_id': fields.many2one(
            'stock.location', 'Ubicación destino', required=True, readonly=True,
            states={'draft': [('readonly', False)]}
        ),
        'shop_id': fields.many2one(
            'sale.shop', 'Shop', required=False, readonly=True,
            states={'draft': [('readonly', False)]}
        ),
    }


sale_order()
