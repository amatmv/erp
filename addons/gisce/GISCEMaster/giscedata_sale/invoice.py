# -*- encoding: utf-8 -*-
from osv import osv, fields


class account_invoice(osv.osv):

    _name = "account.invoice"
    _inherit = "account.invoice"

    _columns = {
        'sale_ids': fields.many2many(
            'sale.order', 'sale_order_invoice_rel', 'invoice_id', 'order_id',
            'Pedidos de compra'
        )
    }


account_invoice()
