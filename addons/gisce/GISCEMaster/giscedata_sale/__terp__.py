# -*- coding: utf-8 -*-
{
    "name": "GISCE Sale",
    "description": """Extensió del mòdul sale base.""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE",
    "depends":[
        "sale",
        "giscedata_stock",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "sale_view.xml",
        "invoice_view.xml",
    ],
    "active": False,
    "installable": True
}
