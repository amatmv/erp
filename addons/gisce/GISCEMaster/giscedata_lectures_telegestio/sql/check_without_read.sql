SELECT
  meter.id as meter_id,
  CASE
    WHEN coalesce(meter.tg_last_read, '1900-01-01') = '1900-01-01'
      THEN 99999
    ELSE date_part('day', now() - coalesce(meter.tg_last_read, '1900-01-01'))::int
  END as days_last_read
FROM giscedata_lectures_comptador meter
INNER JOIN giscedata_polissa polissa
  ON polissa.id = meter.polissa
LEFT JOIN tg_validate_exception_meter exception_meter
  ON exception_meter.meter_id = meter.id
LEFT JOIN tg_validate_exception exception
  ON exception.id = exception_meter.exception_id
WHERE
  polissa.state not in ('esborrany', 'validar', 'cancelada')
  AND meter.tg_cnc_conn = True
  AND coalesce(exception.code, '') <> 'NC'
  AND meter.active = True
  AND (date_part('day', now() - coalesce(meter.tg_last_read, '1900-01-01')) > %s
       OR coalesce(meter.tg_last_read, '1900-01-01') = '1900-01-01')