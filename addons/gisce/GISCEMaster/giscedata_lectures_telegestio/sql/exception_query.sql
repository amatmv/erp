SELECT exception.code
FROM tg_validate_exception exception
INNER JOIN tg_validate_exception_meter exception_meter
ON exception.id = exception_meter.exception_id
WHERE exception_meter.meter_id = %s