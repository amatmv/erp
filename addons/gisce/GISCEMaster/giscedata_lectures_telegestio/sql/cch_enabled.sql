SELECT comptador_id FROM (
  SELECT
    DISTINCT c.id as comptador_id,
    cu.name as cups,
    rank() over (PARTITION BY cu.name ORDER BY c.data_alta desc) as rank
  FROM giscedata_lectures_comptador c
  INNER JOIN giscedata_polissa p ON (c.polissa = p.id)
  INNER JOIN giscedata_cups_ps cu ON (p.cups = cu.id)
  INNER JOIN giscedata_polissa_modcontractual m ON (
      m.polissa_id = p.id
      AND m.tg = %(tg)s
      AND m.agree_tipus = '05'
      AND m.data_inici <= %(end)s
      AND m.data_final >= %(start)s
      )
  INNER JOIN giscedata_polissa_tarifa t ON (
      m.tarifa = t.id
      AND t.name != 'RE'
      AND t.name != 'RE12'
      )
  WHERE
    c.data_alta <= %(end)s
    AND (c.data_baixa is null or c.data_baixa >= %(start)s)
    AND c.tg
) as rank_q WHERE rank = 1 GROUP BY comptador_id
