# -*- coding: utf-8 -*-
{
    "name": "Lectures Telegestio",
    "description": """
    Create billing reads and validate meter billing information from smart meters
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_lectures_distri",
        "giscedata_telegestio_stg",
        "crm_generic",
        "giscedata_ftp_connections"
    ],
    "init_xml": [],
    "demo_xml":[
        "giscedata_lectures_telegestio_demo.xml",
        "giscedata_lectures_telegestio_demo_billing.xml",
        "giscedata_lectures_demo.xml"
    ],
    "update_xml":[
        "crm_data.xml",
        "giscedata_polissa_view.xml",
        "wizard/wizard_create_read_tg_view.xml",
        "wizard/wizard_show_read_tg_view.xml",
        "lectures_view.xml",
        "wizard/wizard_validate_lectures_tg_view.xml",
        "wizard/wizard_export_curve_file_view.xml",
        "wizard/wizard_export_comer_P5D_view.xml",
        "wizard/wizard_update_connectivity_tg_view.xml",
        "giscedata_ftp_provider_view.xml",
        "giscedata_ftp_connections_data.xml",
        "security/giscedata_lectures_telegestio_security.xml",
        "security/ir.model.access.csv",
        "lectures_scheduler.xml",
        "lectures_data.xml"
    ],
    "active": False,
    "installable": True
}
