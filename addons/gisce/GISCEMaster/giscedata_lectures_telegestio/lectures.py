# -*- coding: utf-8 -*-
from __future__ import absolute_import

from osv import osv, fields
from osv.expression import OOQuery
from tools.translate import _
from tools.misc import cache
from tools import config
import math
from datetime import datetime, timedelta
import pooler
import netsvc
import re
import types
from oorq.decorators import job, create_jobs_group
import xlwt
from ast import literal_eval
from addons import get_module_resource

from mongodb_backend.mongodb2 import mdbpool
from giscedata_polissa.giscedata_polissa import TG_OPERATIVA
from giscedata_polissa.giscedata_polissa import CONTRACT_IGNORED_STATES
from giscedata_lectures_tecnologia.giscedata_lectures import TECHNOLOGY_TYPE_SEL
from giscedata_telegestio.utils import *
from dateutil.relativedelta import relativedelta
import calendar

''' [0]: profile selector
    [1]: line format '''

if 'prime' not in dict(TECHNOLOGY_TYPE_SEL).keys():
    TECHNOLOGY_TYPE_SEL += [('prime', 'PRIME')]

CURVE_TYPE_DICT = {'cch_val': ('valid', 'P5D', _(u'CCH_VAL (P5D)')),
                   'cch_cons': ('fact', 'CONS', _(u'CCH_CON (CONS)')),
                   'cch_fact': ('fact', 'F5D', _(u'CCH_FACT (F5D)')),
                   }


def fill_measure(measure, line):
    # search better measure in tg.profile line
    # *_fact first, then not fact an zero otherwise
    return line.get('{0}_fact'.format(measure), line.get(measure, 0))


def get_last_date_cch_val(curve):
    last_date = curve[-1][1]
    return datetime.strptime(last_date, '%Y/%m/%d %H:%M')

def get_all_meter_names(model):
    tm_model_mdb = mdbpool.get_collection(model)
    return tm_model_mdb.distinct('name')

class TgValidateException(osv.osv):

    _name = 'tg.validate.exception'

    _columns = {
        'meter_ids': fields.many2many('giscedata.lectures.comptador',
                                     'tg_validate_exception_meter',
                                     'exception_id', 'meter_id',
                                     'Meters', readonly=True),
        'name': fields.char('Exception', size=60, required=True),
        'code': fields.char('Code', size=10, required=True),
    }

TgValidateException()


class TgValidateMeterException(osv.osv):

    _name = 'tg.validate.meter.exception'
    _rec_name = 'serial'

    def get_serials(self, cursor, uid, context=None):
        meter_ids = self.search(cursor, uid, [], context=context)
        meter_vals = self.read(cursor, uid, meter_ids, context=context)
        return [x['serial'] for x in meter_vals]

    _columns = {
        'serial': fields.char('Serial', size=60, required=True),
        'notes': fields.text('Notes'),
    }

TgValidateMeterException()


class TgLecturesValidate(osv.osv):

    _name = 'tg.lectures.validate'
    _auto = False

    def get_all_meter_names(self, cursor, uid):

        tg_billing_mdb = mdbpool.get_collection('tg_billing')
        return tg_billing_mdb.distinct('name')

    def get_format_date(self, cursor, uid, date, hour=False,
                        default_format=True, context=None):

        if isinstance(date, types.StringTypes):
            if len(date) > 10:
                date = datetime.strptime(date, '%Y-%m-%d %H:%M:%S')
            else:
                date = datetime.strptime(date, '%Y-%m-%d')

        format = '%Y-%m-%d'
        if not default_format:
            format = '%d/%m/%Y'
        if hour:
            format += ' %H'

        return date.strftime(format)

    def update_invalid(self, cursor, uid, dst, src):
        """ Append all the values of dst with the values of src
        " using a \n as separator. Create dst key if.
        """
        for key, value in src.iteritems():
            if key in dst:
                dst[key] += "\n %s" % value
            else:
                dst[key] = value
        return dst

    def total_equal_sum(self, cursor, uid, meter_name, context=None):
        '''checks if the period 0 is equal to the sum
        of all other periods with a margin of 1'''

        tg_billing_obj = self.pool.get('tg.billing')
        meter_obj = self.pool.get('giscedata.lectures.comptador')

        if not context:
            context = {}

        result = {'badids': {}, 'notfound': {}}

        last_valid_read = self.get_last_valid_read(cursor, uid, meter_name,
                                                   context=context)  

        meter_id = meter_obj.get_meter(cursor, uid, meter_name,
                                       last_valid_read, context=context)
        
        if not meter_id:
            return result
        
        # Exits if meter marked as not to check totalizer (TT)
        if 'BT' in meter_obj.get_exceptions(cursor, uid, meter_id):
            return result

        meter = meter_obj.browse(cursor, uid, meter_id)
        search_params = [('name', '=', meter_name),
                         ('date_end', '<=', last_valid_read),
                         ('value', '=', 'a'),
                         ('valid', '=', True)]

        if meter.tg_last_read:
            search_params.extend([('date_end', '>', meter.tg_last_read)])

        read_ids = tg_billing_obj.search(cursor, uid, search_params)
        reads = tg_billing_obj.read(cursor, uid, read_ids)
        read_vals = {}
        #Compare sum and total by date and type
        for read in reads:
            date = read['date_end'][:10]
            type = read['type']
            read_vals.setdefault(date, {})
            read_vals[date].setdefault(type, {})
            read_vals[date][type].setdefault('total', 0)
            read_vals[date][type].setdefault('suma', 0)
            read_vals[date][type].setdefault('periodos', 0)
            read_vals[date][type].setdefault('id', 0)
            if read['period'] == 0:
                read_vals[date][type]['total'] += read['ai']
            else:
                read_vals[date][type]['suma'] += read['ai']
                read_vals[date][type]['periodos'] += 1
            if not read_vals[date][type]['id']:
                read_vals[date][type]['id'] = read['id']
        message = ''
        for date in read_vals:
            for type, value in read_vals[date].iteritems():
                #The difference can be as much as the number of periods
                #That is because each period is an isolated register
                #so rounding problems when suming all periods multiplies
                #by the number of periods
                if abs(value['total'] - value['suma']) > value['periodos']:
                    date_format = self.get_format_date(cursor, uid, date,
                                                       default_format=False,
                                                       context=context)
                    message += (_(u"Total not equal "
                                 u"Date to: %s, Total %s, Sum %s\n"))
                    message %= (date_format, value['total'], value['suma'])
                    read_id = value['id']
                    result['badids'][value['id']] = message
        return result

    def month_vs_day(self, cursor, uid, meter_name, context=None):
        '''checks if the monthly billing (if present) equals to
        daily billing (if present) from the same date'''

        tg_billing_obj = self.pool.get('tg.billing')
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        cfg_obj = self.pool.get('res.config')

        if not context:
            context = {}

        result = {'badids': {}, 'notfound': {}}

        # Exits if config do not want to check
        tg_check_month_day = int(cfg_obj.get(cursor, uid,
                                             'tg_check_month_vs_day', '0'))
        if not tg_check_month_day:
            return result

        last_valid_read = self.get_last_valid_read(cursor, uid, meter_name,
                                                   context=context)  
        meter_id = meter_obj.get_meter(cursor, uid, meter_name,
                                       last_valid_read, context=context)

        # Sometimes we do not find the meter.
        # Perhaps it is a supervisor meter. Take it into account.

        # Exits if meter marked as not to check comparison (CC)
        if (meter_id and
            'CC' in meter_obj.get_exceptions(cursor, uid, meter_id)):
            return result

        search_params = [('name', '=', meter_name),
                         ('date_end', '<=', last_valid_read),
                         ('value', '=', 'a'),
                         ('type', '=', 'month'),
                         ('valid', '=', True)]
        if meter_id:
            meter = meter_obj.browse(cursor, uid, meter_id)
            if meter.tg_last_read:
                search_params.extend([('date_end', '>', meter.tg_last_read)])

        read_ids = tg_billing_obj.search(cursor, uid, search_params)
        reads = tg_billing_obj.read(cursor, uid, read_ids)
        read_vals = {}
        #Compare sum and total by date and type
        message = ''
        for read in reads:
            #Search for corresponding daily billing
            search_params = [
                         ('name', '=', read['name']),
                         ('type', '=', 'day'),
                         ('value', '=', read['value']),
                         ('period', '=', read['period']),
                         ('date_end', '=', read['date_end']),
                         ('valid', '=', True)]
            read_day_id = tg_billing_obj.search(cursor, uid, search_params,
                                                limit=1)
            if read_day_id:
                read_day = tg_billing_obj.read(cursor, uid,
                                               read_day_id, ['ai'])[0]
            else:
                continue

            if read['ai'] != read_day['ai']:
                date_format = self.get_format_date(cursor, uid,
                                                   read['date_end'],
                                                   default_format=False,
                                                   context=context)
                message += (_(u"Montly billing do not equal daily billing\n"
                              u"Date to: %s, Period: %s, Month %s, Day %s"))
                message %= (date_format, read['period'],
                            read['ai'], read_day['ai'])
                result['badids'][read['id']] = message

        return result

    def get_previous_read(self, cursor, uid, vals, context=None):
        '''return values of the previous valid read in vals'''
        tg_billing_obj = self.pool.get('tg.billing')
        search_params = [('name', '=', vals['name']),
                         ('type', '=', vals['type']),
                         ('value', '=', vals['value']),
                         ('period', '=', vals['period']),
                         ('date_begin', '<', vals['date_begin']),
                         ('valid', '=', True)]
        read_id = tg_billing_obj.search(cursor, uid, search_params,
                                        limit=1, order='date_begin desc')
        if read_id:
            return tg_billing_obj.read(cursor, uid, read_id)[0]
        return {}

    def check_negative_read(self, cursor, uid, meter_name, context=None):
        """ Check if the consumptions has negative values """

        meter_obj = self.pool.get('giscedata.lectures.comptador')
        tg_billing = mdbpool.get_collection('tg_billing')

        result = {'badids': {}, 'notfound': {}}
        negatives = tg_billing.find(
            {'name': meter_name, 'valid': False,
             '$or': [{'ai': {'$lt': 0}},
                     {'ae': {'$lt': 0}},
                     {'r1': {'$lt': 0}},
                     {'r2': {'$lt': 0}},
                     {'r3': {'$lt': 0}},
                     {'r4': {'$lt': 0}},
                      ],
            },
            {'_id': 0, 'ai': 1, 'ae': 1, 'period': 1,
             'r1': 1, 'r2': 1, 'r3': 1, 'r4': 1,
             'id': 1, 'date_begin': 1}
            )

        for negative in negatives:
            date_meter = self.get_format_date(cursor, uid,
                                              negative['date_begin'])
            meter_id = meter_obj.get_meter(cursor, uid, meter_name, date_meter,
                                           context=context)
            if not meter_id:
                result['notfound'][negative['id']] = negative['date_begin']
                continue
            meter_exceptions = meter_obj.get_exceptions(cursor, uid, meter_id,
                                                   context=context)
            if 'NR' in meter_exceptions:
                continue
            # Format date for printing
            date = self.get_format_date(cursor, uid, negative['date_begin'],
                                        default_format=False)
            # Extract negative values for message
            msg_values = ['%s: %s, ' % (i.upper(), negative[i])
                          for i in ('ai', 'ae', 'r1', 'r2', 'r3', 'r4')
                          if negative[i] < 0]
            message = (_(u"Negative read "
                         u"Date %s, period %s, %s\n"))
            message %= (date, negative['period'],
                        ', '.join(msg_values))
            result['badids'][negative['id']] = message
        return result

    def negative_read(self, cursor, uid, vals,
                      context=None, exceptions=[]):
        '''Negative read in vals'''
        message = ''
        #If we do not have to check negative read
        #for this meter skip
        if 'NR' in exceptions:
            return message
        to_check = ['ai', 'ae', 'r1', 'r2', 'r3', 'r4']
        values = [mag.upper() for mag in to_check if vals[mag] < 0]
        if values:
            date = self.get_format_date(cursor, uid,
                                    vals['date_begin'],
                                    context=context)
            message = (_(u"Negative read "
                        u"Date %s, period %s, values %s\n")
                                 % (date,
                                    vals['period'],
                                    (', ').join(values)))
        return message

    def impossible_read(self, cursor, uid, vals, polissa, exceptions=[],
                        context=None):
        '''Checks if a consumer is generating energy
        or if it has a reactive read in the incorrect quadrant.
        Also check too high consumption
        Also check turn back read'''
        message = ''
        res = {}
        date = self.get_format_date(cursor, uid,
                                    vals['date_begin'],
                                    context=context)
        date_format = self.get_format_date(cursor, uid,
                                    vals['date_begin'],
                                    default_format=False,
                                    context=context)

        if vals['value'] == 'a':
            #absolute read
            previous_vals = self.get_previous_read(cursor, uid,
                                                   vals, context)
            if not previous_vals:
                return res
            #if EE not in exceptions, we have to check export energy
            if (not 'EE' in exceptions and
                vals['ae'] - previous_vals['ae'] != 0):
                message += _(u"Impossible read: "
                             u"This meter is exporting energy\n")
                message += _(u"Date: %s, Period: %s, AE: %s")
                message %= (date_format, vals['period'], vals['ae'])
            if (not 'EE' in exceptions and
                not 'ER' in exceptions and
                (vals['r2'] - previous_vals['r2'] != 0
                or vals['r3'] - previous_vals['r3'] != 0)):
                message += _(u"Impossible read: "
                             u"This meter is generating "
                             u"quadrant 2 or 3 reactive\n")
                message += _(u"Date: %s, R2: %s, R3: %s")
                message %=  (date_format, vals['r2'], vals['r3'])
            #Max watts consumption = potencia * 24 hours a day
            #checking only in daily billing
            time_between_reads = (datetime.strptime(vals['date_end'],
                                                   '%Y-%m-%d %H:%M:%S') -
                                 datetime.strptime(previous_vals['date_end'],
                                                  '%Y-%m-%d %H:%M:%S'))
            #In many cases initial read it is not from 00 hour
            #so check max_kwatts in real hours
            hours_between_reads = ((time_between_reads.days * 24) +
                                   (time_between_reads.seconds / 3600))
            max_kwatts = polissa.potencia * hours_between_reads
            #If OL not in exceptions, we have to check consumption
            if (not 'OL' in exceptions and
                vals['ai'] - previous_vals['ai'] >= max_kwatts):
                message += _(u"Impossible read: "
                             u"Consumption over limit\n")
                message += _(u"Date: %s, Period: %s, AI: %s")
                message %= (date_format, vals['period'], vals['ai'])
            if vals['ai'] - previous_vals['ai'] < 0:
                message += _(u"Turn back read:\n")
                message += _(u"Date: %s, Period: %s, AI: %s")
                message %= (date_format, vals['period'], vals['ai'])
        else:
            #incremental read
            #If EE not in exceptions, we have to check consumption
            if not 'EE' in exceptions and vals['ae'] != 0:
                message += _(u"Impossible read: "
                             u"This meter is exporting energy\n")
                message += _(u"Date: %s, Period: %s, AE: %s")
                message %= (date, vals['period'], vals['ae'])
            if (not 'EE' in exceptions and
                not 'ER' in exceptions and
                (vals['r2'] != 0 or vals['r3'] != 0)):
                message += _(u"Impossible read: "
                             u"This meter is generating "
                             u"quadrant 2 or 3 reactive\n")
                message += _(u"Date: %s, R2: %s, R3: %s")
                message %= (date, vals['r2'], vals['r3'])
            #Max watts consumption = potencia * 24 hours a day
            time_between_reads = ((datetime.strptime(vals['date_end'],
                                                   '%Y-%m-%d %H:%M:%S') -
                                 datetime.strptime(vals['date_begin'],
                                                  '%Y-%m-%d %H:%M:%S')))
            #In many cases initial read it is not from 00 hour
            #so check max_kwatts in real hours
            hours_between_reads = ((time_between_reads.days * 24) +
                                   (time_between_reads.seconds / 3600))
            if hours_between_reads == 0:
                return message
            max_kwatts = polissa.potencia * hours_between_reads
            #If OL not in exceptions, we have to check consumption
            if (not 'OL' in exceptions and
                vals['ai'] >= max_kwatts):
                message += _(u"Impossible read: "
                             u"Consumption over limit\n")
                message += _(u"Date: %s, Period: %s, AI: %s")
                message %= (date, vals['period'], vals['ai'])
        if message:
            res[vals['id']] = message
        return res

    def reactive_check(self, cursor, uid, vals, polissa, exceptions=[],
                       context=None):
        '''Check reactive (R1) not superior to 75%
        of active (cosfi < 0.80)'''

        message = ''
        res = {}
        config = self.pool.get('res.config')
        if not int(config.get(cursor, uid,
                           'tg_reactive_check', '1')):
            return res
        if vals['value'] == 'a':
            previous_vals = self.get_previous_read(cursor, uid, vals, context)
            #With no previous vals we cannot calculate consumption
            if not previous_vals:
                return message
            time_between_reads = ((datetime.strptime(vals['date_end'],
                                                       '%Y-%m-%d %H:%M:%S') -
                                  datetime.strptime(previous_vals['date_end'],
                                                    '%Y-%m-%d %H:%M:%S')))
            hours_between_reads = ((time_between_reads.days * 24) +
                                   (time_between_reads.seconds / 3600))
            r1_con = vals['r1'] - previous_vals['r1']
            ai_con = vals['ai'] - previous_vals['ai']
        else:
            r1_con = vals['r1']
            ai_con = vals['ai']
            time_between_reads = ((datetime.strptime(vals['date_end'],
                                                       '%Y-%m-%d %H:%M:%S') -
                                  datetime.strptime(vals['date_begin'],
                                                    '%Y-%m-%d %H:%M:%S')))
            hours_between_reads = ((time_between_reads.days * 24) +
                                   (time_between_reads.seconds / 3600))
        #Do not check reactive if consumption is too low
        #5 kVarh limit per day (0.21 per hour)
        if (r1_con > 0.21 * hours_between_reads and
            r1_con > (ai_con * 75 / 100)):
            date = self.get_format_date(cursor, uid,
                                    vals['date_begin'],
                                    context=context)
            message += _(u"Reactive too high: ")
            message += (_(u"Date %s, Period: %s, AI: %s, R1: %s")
                                    % (date, vals['period'],
                                       ai_con,
                                       r1_con))
        if message:
            res[vals['id']] = message
        return res

    def min_interval(self, cursor, uid, vals, polissa, exceptions=[],
                     context=None):
        '''Checks min interval for billing in minutes
        If the interval is lower than configured value
        tg read will be deleted
        Returns True if min_interval exceeds'''

        #Only check if monthly billing
        if vals['type'] == 'month':
            tg_billing_obj = self.pool.get('tg.billing')
            cfg_obj = self.pool.get('res.config')
            min_minutes = int(cfg_obj.get(cursor, uid,
                           'tg_monthly_min_interval', '10'))
            time_between_reads = (datetime.strptime(vals['date_end'],
                                                   '%Y-%m-%d %H:%M:%S') -
                                  datetime.strptime(vals['date_begin'],
                                                  '%Y-%m-%d %H:%M:%S'))
            minutes_between_reads = (time_between_reads.days * 24 * 60 +
                                     time_between_reads.seconds / 60)
            if minutes_between_reads <= min_minutes:
                tg_billing_obj.unlink(cursor, uid, vals['id'])
        elif vals['type'] == 'day':
            #We have to get previous read before checking
            previous_vals = self.get_previous_read(cursor, uid,
                                               vals, context)
            if not previous_vals:
                return {}
            #Get min_minutes interval from config
            tg_billing_obj = self.pool.get('tg.billing')
            cfg_obj = self.pool.get('res.config')
            min_minutes = int(cfg_obj.get(cursor, uid,
                           'tg_monthly_min_interval', '10'))
            #Compute time between read and previous read
            time_between_reads = (datetime.strptime(vals['date_end'],
                                                   '%Y-%m-%d %H:%M:%S') -
                                 datetime.strptime(previous_vals['date_end'],
                                                  '%Y-%m-%d %H:%M:%S'))
            minutes_between_reads = (time_between_reads.days * 24 * 60 +
                                     time_between_reads.seconds / 60)
            if minutes_between_reads <= min_minutes:
                tg_billing_obj.unlink(cursor, uid, vals['id'])
        return {}

    def clean_duplicates(self, cursor, uid, meter_name):
        """ From one meter_name check all the measures deleting duplicates
        " if there is some duplicate that has been validated is the one that
        " remains
        """
        tg_billing = mdbpool.get_collection('tg_billing')

        meter_dups = tg_billing.aggregate([
            {'$match': {'name': meter_name}},
            {'$group': {'_id': {'name': "$name",
                                'type': "$type",
                                'value': "$value",
                                'period': "$period", 
                                'date_begin': "$date_begin",
                                'ai': "$ai"
                        },
                        'count': {'$sum': 1}}},
            {'$match': {'count': {'$gt': 1}}},
        ])['result']

        for dup in meter_dups:
            # Look for all the duplied measures matching criteria
            dup_ids = tg_billing.find(dup['_id'], {'_id': 1, 'valid': 1})

            exist_one_valid = False
            delete_ids = []
            for dup_id in dup_ids:
                if 'valid' in dup_id and dup_id['valid'] and not exist_one_valid:
                    # If we already have a valid measure we should delete all
                    exist_one_valid = True
                else:
                    delete_ids.append(dup_id['_id'])

            if not exist_one_valid:
                # Al least one of the duplicate must remain if noone is valid
                delete_ids.pop()

            tg_billing.remove({'_id': {'$in': delete_ids}})

    def check_same_time_many_measures(self, cursor, uid, meter_name,
                                      context=None):
        """ Look for measures that has same meter, type, value,
        " period and date_begin but different ai reads
        """
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        tg_billing = mdbpool.get_collection('tg_billing')

        result = {'badids': {}, 'notfound': {}}

        dups = tg_billing.aggregate([
            {'$match': {'name': meter_name}},
            {'$group': {'_id': {'name': '$name',
                                'type': '$type',
                                'value': '$value',
                                'period': '$period',
                                'date_begin': '$date_begin',
                        },
                        'reads': {'$push': {'ai': '$ai',
                                            'id': '$id',
                                            'valid': '$valid'}},
                        'count': {'$sum': 1}
                        }
            },
            {'$match': {'count': {'$gt': 1}}},
        ])['result']

        for dup in dups:
            date_begin = dup['_id']['date_begin']
            date_meter = self.get_format_date(cursor, uid, date_begin)
            meter_id = meter_obj.get_meter(cursor, uid, meter_name, date_meter,
                                           context=context)
            if not meter_id:
                for read in dup['reads']:
                    id = read['id']
                    result['notfound'][id] = date_begin
                continue
            date_msg = self.get_format_date(cursor, uid, date_begin,
                                            default_format=False)
            # Look for a valid read among reads found
            valid_read = None
            to_delete = []
            for read in dup['reads']:
                if read['valid']:
                    valid_read = read
                    break
            # If we have a valid read, we can delete all
            # the other ones with a difference less than 1 in ai
            if valid_read is not None:
                for read in dup['reads']:
                    if (read['id'] != valid_read['id']
                        and abs(read['ai'] - valid_read['ai']) <= 1):
                        to_delete.append(read['id'])
            check_reads = [read for read in dup['reads']
                           if read['id'] not in to_delete]
            check_read_ids = [read['id'] for read in check_reads]
            # Set the error message only in the first duplied read, but
            # all of them should be in the invalid list
            measure_list = ', '.join([str(read['ai'])
                                     for read in check_reads])
            for read_id in check_read_ids:
                msg = ''
                if check_read_ids.index(read_id) == 0:
                    msg = _(u"%s different reads found "
                            u"on %s for period %s. AI: %s")
                    msg %= (len(check_read_ids), date_msg,
                            dup['_id']['period'], measure_list)
                result['badids'][read_id] = msg
            if to_delete:
                tg_billing.remove({'id': {'$in': to_delete}})
        return result

    def get_last_valid_read(self, cursor, uid, meter_name,
                            full_read=False, context=None):

        tg_billing_obj = self.pool.get('tg.billing')

        search_params = [('name', '=', meter_name),
                         ('valid', '=', True)]
        tg_billing_ids = tg_billing_obj.search(cursor, uid, search_params,
                                               limit=1,
                                               order='date_end desc')
        res = None
        if tg_billing_ids:
            tg_billing = tg_billing_obj.read(cursor, uid, tg_billing_ids)[0]
            if full_read:
                res = tg_billing
            else:
                res = self.get_format_date(cursor, uid,
                                           tg_billing['date_end'])

        return res

    def get_meter_updated_vals(self, cursor, uid, last_valid, cnc_id, registrator_id):

        return {
            'tg_last_read': last_valid['date_end'][:10],
            'tg_cnc_id': cnc_id,
            'tg': True,
            'technology_type': 'prime',
            'registrador_id': registrator_id,
        }

    def update_last_valid_read(self, cursor, uid, meter_names,
                               context=None):

        meter_obj = self.pool.get('giscedata.lectures.comptador')
        cnc_obj = self.pool.get('tg.concentrator')
        registrator_obj = self.pool.get('giscedata.registrador')

        if not context:
            context = {}
        context.update({'sync': False})

        if not isinstance(meter_names, (list, tuple)):
            meter_names = [meter_names]

        for meter_name in meter_names:
            last_valid = self.get_last_valid_read(cursor, uid,
                                                  meter_name,
                                                  full_read=True,
                                                  context=context)
            if last_valid is None:
                continue

            last_valid_datetime = datetime.strptime(
                last_valid['date_end'],
                '%Y-%m-%d %H:%M:%S'
            )
            if last_valid_datetime.hour == 0:
                last_valid_datetime -= timedelta(days=1)

            date_last_valid_read = last_valid_datetime.strftime('%Y-%m-%d')

            meter_id = meter_obj.get_meter(
                cursor,
                uid,
                meter_name,
                date_last_valid_read,
                context=context
            )
            if not meter_id:
                continue
            cnc_id = cnc_obj.get_cnc(cursor, uid, last_valid['cnc_name'],
                                     context=context)
            registrator_id = registrator_obj.register_tg(
                cursor, uid, cnc_id, meter_name
            )

            # if thres is a meter_id, its an billing registrator
            registrator_obj.write(
                cursor, uid, registrator_id, {'type': 'billing'}
            )

            vals = self.get_meter_updated_vals(
                cursor, uid, last_valid, cnc_id, registrator_id
            )

            meter_obj.write(cursor, uid, [meter_id],
                            vals, context=context)
        return {'badids': {}, 'notfound': {}}

    @job(queue='tg_validate', timeout=600, result_ttl=24*3600)
    def meters_without_read(self, cursor, uid, context=None):
        '''Checks if a meter do not have received
        a read for more than X days or never'''

        meter_obj = self.pool.get('giscedata.lectures.comptador')
        tg_validate_meter_exception = self.pool.get(
            'tg.validate.meter.exception'
        )
        cfg_obj = self.pool.get('res.config')
        case_obj = self.pool.get('crm.case')
        if not context:
            context = {}
        context.update({'model': 'giscedata.polissa',
                        'active_test': True})
        # Search for meters to check
        days = int(cfg_obj.get(cursor, uid, 'tg_last_read_advice', '2'))
        query = open('%s/giscedata_lectures_telegestio/sql/check_without_read.sql'
                     % config['addons_path']).read()
        cursor.execute(query, (days,))

        for meter_id, days_last_read in cursor.fetchall():
            message = ''
            last_read = meter_obj.read(cursor, uid, meter_id,
                                       ['tg_last_read', 'name', 'polissa'],
                                       context=context)
            meter_serial = meter_obj.build_name_tg(
                cursor,
                uid,
                meter_id,
                context=context
            )
            if tg_validate_meter_exception.search(
                    cursor,
                    uid,
                    [
                        ('serial', '=', meter_serial)
                    ],
                    context=context
            ):
                continue
            if days_last_read == 99999:
                message += (_(u"TG meter %s without any read")
                             % last_read['name'])
            else:
                read_date = datetime.strptime(last_read['tg_last_read'],
                                              '%Y-%m-%d').strftime('%d/%m/%Y')
                message += _(u"TG meter %s last read was "
                                 u"more than %s days ago. %s")
                message %= (last_read['name'], days, read_date)

            if not message:
                continue

            #Do not open another case if already one in open or pending state
            #This way we let automatic checks to be closed manually when done
            #and do not create many cases with the same incidences
            if meter_obj.check_case_open(cursor, uid, meter_id, section='TG',
                                         category='TGRE', context=context):
                continue

            # Create case for this meter
            extra_vals = {'description': message,
                          'ref2': 'giscedata.lectures.comptador,%i'
                                    % last_read['id']}
            description = _(u"Unreaded meter %s") % last_read['name']
            case_id = case_obj.create_case_generic(cursor, uid,
                                        [last_read['polissa'][0]],
                                        context=context,
                                        description=description,
                                        section='TG',
                                        category='TGRE',
                                        extra_vals=extra_vals)
            # Open the case
            case_obj.case_open(cursor, uid, case_id)

        return True

    def create_case_invalid(self, cursor, uid, invalids, context=None):
        '''Create case with invalid measures'''
        case_obj = self.pool.get('crm.case')
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        billing_obj = self.pool.get('tg.billing')
        config_obj = self.pool.get('res.config')
        billing_mdb = mdbpool.get_collection('tg_billing')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'model': 'giscedata.polissa'})

        # Order invalids by timestamp first
        search_params = [('id', 'in', invalids.keys())]
        ordered_ids = billing_obj.search(cursor, uid, search_params,
                                         order='date_end')

        today = datetime.now().strftime('%Y-%m-%d')
        date_check_from = config_obj.get(cursor, uid, 'tg_billing_check_from',
                                         today)

        meters = {}
        # Search meter_id for each invalid and group then
        for billing_id in ordered_ids:
            tg_billing = billing_mdb.find_one({'id': billing_id},
                                              {'date_end': 1, 'name': 1})
            date_meter = self.get_format_date(cursor, uid,
                                              tg_billing['date_end'])
            # Do not notify these in cases. They are too old.
            if date_meter < date_check_from:
                continue
            meter_id = meter_obj.get_meter(cursor, uid, tg_billing['name'],
                                           date_meter, context=context)
            meters.setdefault(meter_id, [])
            if invalids[billing_id] != '':
                meters[meter_id].append(invalids[billing_id])

        for meter_id in meters:
            if not meter_id:
                continue
            # Do not open another case if already one in open or pending state
            # This way we let automatic checks to be closed manually when done
            # and do not create many cases with the same incidences
            meter = meter_obj.browse(cursor, uid, meter_id)
            case_message = '\n'.join(meters[meter_id])
            case_vals = {
                'description': case_message,
                'ref2': 'giscedata.lectures.comptador,%i' % meter.id,
                'name': (_('TG Billing checkings for %s (%s)')
                         % (meter.name, meter.build_name_tg())),
                'polissa_id': meter.polissa.id,
            }
            ctx['model_id'] = meter.polissa.id

            case_obj.append_description_to_polissa_by_name(
                cursor, uid, None, case_vals['name'], case_vals['description'],
                section='TG', category='TGBI',  context=ctx
            )
        return True

    def create_case_notfound(self, cursor, uid, notfound, meter_name,
                             context=None):
        '''Create case with notfound measures'''
        case_obj = self.pool.get('crm.case')
        config_obj = self.pool.get('res.config')

        meter_obj = self.pool.get('giscedata.lectures.comptador')
        meter = meter_obj.search(cursor, uid, [('name', '=', meter_name)])
        cnc_name = ''
        if meter:
            cnc_name = str(meter_obj.read(cursor, uid, meter[0]).get('tg_cnc_id')[1])[:-3].upper()

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'model': False})

        today = datetime.now().strftime('%Y-%m-%d')
        date_check_from = config_obj.get(cursor, uid, 'tg_billing_check_from',
                                         today)
        date_check_from = datetime.strptime(date_check_from, '%Y-%m-%d').date()
        msg = ''

        only_days = [
            self.get_format_date(cursor, uid, date, context=context)
            for date in notfound.itervalues()
            if date.date() >= date_check_from
        ]
        # If we have no days to show, do not create any case
        if not only_days:
            return True
        sorted_days = sorted(list(set(only_days)))
        _not_found_msg = _('Meter %s not found on %s in concentrator %s\n')
        for timestamp in sorted_days:
            date = self.get_format_date(cursor, uid, timestamp,
                                        default_format=False)
            msg += _not_found_msg % (meter_name, date, cnc_name)

        case_name = _('TG Billing out of range for {}').format(meter_name)

        return case_obj.append_description_to_case_by_name(
            cursor,
            uid,
            None,
            case_name,
            msg,
            'TG',
            'TGBI',
            context=context
        )

    _block_invalid_check_funcs = ['check_negative_read',
                                  'check_same_time_many_measures',
                                 ]

    _single_check_funcs = [('min_interval', True),
                           ('impossible_read', True),
                           ('reactive_check', False)]

    _post_funcs = ['total_equal_sum',
                   'month_vs_day',
                   'update_last_valid_read',
                  ]

    @job(queue='tg_validate', timeout=600, result_ttl=24*3600)
    def validate_individual(self, cursor, uid, ids, meter_name, context=None):
        '''This function has to validate the billing
        information that comes from smart meters
        '''
        logger = netsvc.Logger()
        tg_billing_obj = self.pool.get('tg.billing')
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        polissa_obj = self.pool.get('giscedata.polissa')
        meter_exc_obj = self.pool.get('tg.validate.meter.exception')
        case_obj = self.pool.get('crm.case')
        tg_billing_mdb = mdbpool.get_collection('tg_billing')
        if not context:
            context = {}

        # Check for meter exceptions
        meter_exceptions = meter_exc_obj.get_serials(cursor, uid,
                                                     context=context)
        self.clean_duplicates(cursor, uid, meter_name)

        search_params = [('name', '=', meter_name),
                         ('valid', '=', False)]

        tg_billing_ids = tg_billing_obj.search(cursor, uid,
                                               search_params,
                                               order='date_end')
        if not tg_billing_ids:
            return True

        if meter_name in meter_exceptions:
            tg_billing_obj.validate(cursor, uid, tg_billing_ids,
                                    action='validate', context=context)
            func = getattr(self, 'update_last_valid_read')
            func(cursor, uid, meter_name, context=context)
            return True

        invalids = {}
        notfound = {}
        # Block checks
        for func_name in self._block_invalid_check_funcs:
            func = getattr(self, func_name)
            res = func(cursor, uid, meter_name, context=context)
            invalids = self.update_invalid(cursor, uid, invalids,
                                           res['badids'])
            notfound.update(res['notfound'])

        invalid_ids = list(set(invalids.keys() + notfound.keys()))
        to_be_validated = list(set(tg_billing_ids) - set(invalid_ids))

        meters = []
        for tg_billing_id in tg_billing_ids:
            tg_billing = tg_billing_obj.read(cursor, uid,
                                             [tg_billing_id], [])[0]

            #get meter active in date_end
            date_meter = tg_billing['date_end']
            meter_id = meter_obj.get_meter(cursor, uid,
                                           tg_billing['name'],
                                           date_meter, context=context)
            if not meter_id:
                if not isinstance(date_meter, datetime):
                    date_meter = datetime.strptime(date_meter, '%Y-%m-%d %H:%M:%S')
                notfound[tg_billing_id] = date_meter
                continue
            meters.append(meter_id)
            meter_vals = meter_obj.read(cursor, uid, meter_id,
                                        ['polissa', 'meter_type'],
                                        context)
            polissa_id = meter_vals['polissa'][0]
            try:
                polissa = polissa_obj.browse(cursor, uid, polissa_id,
                                             context={'date': date_meter})
            except Exception as e:
                if context.get('validate', False):
                    day_before_meter = (
                        datetime.strptime(date_meter, '%Y-%m-%d %H:%M:%S') +
                        relativedelta(days=1)).strftime('%Y-%m-%d %H:%M:%S')
                    polissa = polissa_obj.browse(cursor, uid, polissa_id,
                                                 context={'date':
                                                          day_before_meter})
                else:
                    raise
            num_periodes = (polissa.tarifa.
                            get_num_periodes(context={'agrupar_periodes':
                                                      False}))
            exceptions = meter_obj.get_exceptions(cursor, uid, meter_id,
                                              context=context)

            # Generation meters can export Energy
            if meter_vals['meter_type'] in ['G']:
                exceptions.extend(['EE', 'ER'])

            #For one period fares, we will store
            #period 0, 1 and 2 for checking
            if num_periodes == 1:
                num_periodes = 2
            #Remove not necessary periods
            if tg_billing['period'] > num_periodes:
                tg_billing_obj.unlink(cursor, uid, tg_billing['id'])
                continue
            #Store meter_id in tg_billing for later processing
            tg_billing_obj.write(cursor, uid, [tg_billing_id],
                                 {'meter_id': meter_id})
            invalid = False
            for func_name, mark_invalid in self._single_check_funcs:
                func = getattr(self, func_name)
                res = func(cursor, uid, tg_billing, polissa,
                           exceptions, context=context)
                if res:
                    invalids = self.update_invalid(cursor, uid, invalids,
                                                   res)
                    #Mark as invalid only if not invalid before
                    if mark_invalid and not invalid:
                        invalid = mark_invalid

            # Exclude invalid by block test validation (to_be_validated)
            if not invalid and tg_billing['id'] in to_be_validated:
                # If we arrive here the measure is valid,
                # no inserts during the process
                tg_billing_obj.validate(cursor, uid, [tg_billing['id']],
                                        action='validate', context=context)

        for func_name in self._post_funcs:
            func = getattr(self, func_name)
            res = func(cursor, uid, meter_name, context=context)
            invalids = self.update_invalid(cursor, uid, invalids,
                                           res['badids'])
            notfound.update(res['notfound'])

        if meters:
            meter_obj.update_meters_connectivity_fields(
                    cursor, uid, meters
            )

        # Create cases
        if invalids:
            self.create_case_invalid(cursor, uid, invalids,
                                     context=context)
        if notfound:
            self.create_case_notfound(cursor, uid, notfound, meter_name,
                                      context=context)
        return True

    def validate_billing(self, cursor, uid, ids, meter_names=[],
                         context=None):
        '''This function has to validate the billing
        information that comes from smart meters
        '''
        if not context:
            context = {}
        context.update({'validate': True})
        if 'lang' not in context:
            user_obj = self.pool.get('res.users')
            lang = user_obj.read(cursor, uid, uid, ['context_lang'])['context_lang']
            if lang:
                context.update({'lang': lang})
        if not meter_names:
            meter_names = self.get_all_meter_names(cursor, uid)

        jobs_ids = []

        for meter_name in meter_names:
            j = self.validate_individual(cursor, uid, ids, meter_name,
                                         context=context)
            jobs_ids.append(j.id)

        if len(jobs_ids) > 1:
            create_jobs_group(
                cursor.dbname, uid, _('Validate billing'),
                'tg.validate.billing', jobs_ids
            )

        if context.get('tg_check_unreaded', True):
            self.meters_without_read(cursor, uid, context)
        return True

TgLecturesValidate()


class TgProfileValidate(osv.osv):

    _name = 'tg.profile.validate'
    _auto = False

    def get_all_meter_names(self, cursor, uid):

        tg_profile_mdb = mdbpool.get_collection('tg_profile')
        return tg_profile_mdb.distinct('name')

    def get_format_date(self, cursor, uid, date, hour=False,
                        default_format=True):

        format = '%Y-%m-%d'
        if not default_format:
            format = '%d/%m/%Y'
        if hour:
            format += ' %H'

        return date.strftime(format)

    def clean_duplicates(self, cursor, uid, meter_name):
        """ From one meter_name check all the measures deleting duplicates
        " if there is some duplicate that has been validated is the one that
        " remains
        """
        tg_profile = mdbpool.get_collection('tg_profile')
        # We also group by season, the day with hour change we must
        # two equal measures at the same hour but with different seaseon value
        meter_dups = tg_profile.aggregate([
            {'$match': {'name': meter_name}},
            {'$group': {'_id': {'name': "$name",
                                'timestamp': "$timestamp",
                                'ai': "$ai",
                                'season': "$season"
                        },
                        'count': {'$sum': 1}}},
            {'$match': {'count': {'$gt': 1}}},
        ])['result']

        for dup in meter_dups:
            # Look for all the duplied measures matching ai, name & timestamp
            dup_ids = tg_profile.find(dup['_id'], {'_id': 1, 'valid': 1})

            exist_one_valid = False
            delete_ids = []
            for dup_id in dup_ids:
                if ('valid' in dup_id and dup_id['valid']
                        and not exist_one_valid):
                        # If we already have a valid measure
                        # we should delete all
                        exist_one_valid = True
                else:
                    delete_ids.append(dup_id['_id'])

            if not exist_one_valid:
                # Al least one of the duplicate must remain if noone is valid
                delete_ids.pop()

            tg_profile.remove({'_id': {'$in': delete_ids}})

    def update_invalid(self, cursor, uid, dst, src):
        """ Append all the values of dst with the values of src
        " using a \n as separator. Create dst key if.
        """
        for key, value in src.iteritems():
            if key in dst:
                dst[key] += "\n %s" % value
            else:
                dst[key] = value
        return dst

    def check_exported_energy(self, cursor, uid, meter_name, context=None):
        """ Check that all 'ae' are 0 so ensure that
            meter is not exporting energy
        """
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        tg_profile = mdbpool.get_collection('tg_profile')
        conf_obj = self.pool.get('res.config')

        check_enabled = int(conf_obj.get(cursor, uid,
                                         'tg_profile_check_exported', '1')
                            )

        result = {'badids': {}, 'notfound': {}}
        if not check_enabled:
            return result
        exporters = tg_profile.find(
            {'name': meter_name, 'valid': False,
             'ae': {'$ne': 0}
            },
            {'_id': 0, 'ae': 1, 'id': 1, 'magn': 1, 'timestamp': 1}
            )
        for export in exporters:
            #First look for the meter on timestamps date to get exceptions
            date_meter = self.get_format_date(cursor, uid, export['timestamp'])
            meter_id = meter_obj.get_meter(cursor, uid, meter_name, date_meter)
            if not meter_id:
                result['notfound'][export['id']] = export['timestamp']
                continue
            meter_exceptions = meter_obj.get_exceptions(cursor, uid, meter_id,
                                                   context=context)
            meter_data = meter_obj.read(cursor, uid, meter_id, ['meter_type'])
            meter_type = meter_data['meter_type']
            #Check if the meter has exceptions or is a generation one
            if 'EE' in meter_exceptions or meter_type in ['G']:
                continue
            date_msg = self.get_format_date(cursor, uid, export['timestamp'],
                                            hour=True, default_format=False)
            msg = _("This meter at %s was exporting energy AE: %s")
            msg = msg % (date_msg, export['ae'] * export['magn'])
            result['badids'][export['id']] = msg
        return result

    def check_exported_reactive(self, cursor, uid, meter_name, context=None):
        """ Check that all 'r2' and 'r3' are 0 so ensure that
        " the menter is not exporting energy
        """
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        tg_profile = mdbpool.get_collection('tg_profile')
        conf_obj = self.pool.get('res.config')

        check_enabled = int(conf_obj.get(cursor, uid,
                                         'tg_profile_check_exported', '1')
                            )

        result = {'badids': {}, 'notfound': {}}
        if not check_enabled:
            return result
        exporters = tg_profile.find(
            {'name': meter_name, 'valid': False,
             '$or': [{'r2': {'$ne': 0}},
                     {'r3': {'$ne': 0}},
                     ],
            },
            {'_id': 0, 'r2': 1, 'r3': 1, 'id': 1, 'magn': 1, 'timestamp': 1}
            )
        for export in exporters:
            date_meter = self.get_format_date(cursor, uid, export['timestamp'])
            meter_id = meter_obj.get_meter(cursor, uid, meter_name, date_meter)
            if not meter_id:
                result['notfound'][export['id']] = export['timestamp']
                continue
            #Check if the meter has exceptions or is a generation one
            meter_exceptions = meter_obj.get_exceptions(cursor, uid, meter_id,
                                                        context=context)
            meter_data = meter_obj.read(cursor, uid, meter_id, ['meter_type'])
            meter_type = meter_data['meter_type']
            if ('EE' in meter_exceptions or
                'ER' in meter_exceptions or
                meter_type in ['G']):
                continue
            date_msg = self.get_format_date(cursor, uid, export['timestamp'],
                                            hour=True, default_format=False)
            msg = _("This meter at %s was exporting reactive r2: %s, r3: %s")
            msg = msg % (date_msg, export['r2'] * export['magn'],
                         export['r3'] * export['magn'])
            result['badids'][export['id']] = msg
        return result

    def check_impossible_measure(self, cursor, uid, meter_name, context=None):
        """ Check if the consumptions has negative values (ai or r1) """

        meter_obj = self.pool.get('giscedata.lectures.comptador')
        tg_profile = mdbpool.get_collection('tg_profile')
        conf_obj = self.pool.get('res.config')

        impossible = int(conf_obj.get(cursor, uid,
                                      'tg_profile_impossible',
                                      '999999999'
                                      ))

        result = {'badids': {}, 'notfound': {}}
        impossibles = tg_profile.find(
            {'name': meter_name, 'valid': False,
             '$or': [{'ai': {'$gt': impossible}},
                     {'ae': {'$gt': impossible}},
                     {'r1': {'$gt': impossible}},
                     {'r2': {'$gt': impossible}},
                     {'r3': {'$gt': impossible}},
                     {'r4': {'$gt': impossible}},
                     ],
            },
            {'_id': 0}
            )
        for impossible in impossibles:
            date_meter = self.get_format_date(cursor, uid,
                                              impossible['timestamp'])
            meter_id = meter_obj.get_meter(cursor, uid,
                                           meter_name, date_meter)
            if not meter_id:
                result['notfound'][impossible['id']] = impossible['timestamp']
                continue
            date = self.get_format_date(cursor, uid, impossible['timestamp'],
                                        hour=True, default_format=False)
            msg = _(u"Impossible measure at %s ai: %s, ae: %s,"
                    u"r1: %s, r2: %s, r3: %s, r4: %s,")
            msg = msg % (date,
                         impossible['ai'],
                         impossible['ae'],
                         impossible['r1'],
                         impossible['r2'],
                         impossible['r3'],
                         impossible['r4'])
            result['badids'][impossible['id']] = msg
        return result

    def check_negative_read(self, cursor, uid, meter_name, context=None):
        """ Check if the consumptions has negative values (ai or r1) """

        meter_obj = self.pool.get('giscedata.lectures.comptador')
        tg_profile = mdbpool.get_collection('tg_profile')

        result = {'badids': {}, 'notfound': {}}
        negatives = tg_profile.find(
            {'name': meter_name, 'valid': False,
             '$or': [{'ai': {'$lt': 0}},
                     {'r1': {'$lt': 0}},
                      ],
            },
            {'_id': 0, 'ai': 1, 'r1': 1, 'id': 1, 'magn': 1, 'timestamp': 1}
            )
        for negative in negatives:
            date_meter = self.get_format_date(cursor, uid,
                                              negative['timestamp'])
            meter_id = meter_obj.get_meter(cursor, uid,
                                           meter_name, date_meter)
            if not meter_id:
                result['notfound'][negative['id']] = negative['timestamp']
                continue
            meter_exceptions = meter_obj.get_exceptions(cursor, uid, meter_id,
                                                   context=context)
            if 'NR' in meter_exceptions:
                continue
            date = self.get_format_date(cursor, uid, negative['timestamp'],
                                        hour=True, default_format=False)
            msg = _("Meter has negative measure at %s ai: %s, r1: %s")
            msg = msg % (date, negative['ai'] * negative['magn'],
                         negative['r1'] * negative['magn'])
            result['badids'][negative['id']] = msg
        return result

    def check_control_bits(self, cursor, uid, meter_name, context=None):
        """ Check control bit """

        control_messages = {
            7: _(u"Bit 7. Invalid measure"),
            6: _(u"Bit 6. Synchronized meter during period"),
            5: _(u"Bit 5. Overflow"),
            4: _(u"Bit 4. Time verification during period"),
            3: _(u"Bit 3. Modified parameters during period"),
            2: _(u"Bit 2. Intrusion detected"),
            1: _(u"Bit 1. Incomplete period due to power failure"),
            0: ''
        }

        meter_obj = self.pool.get('giscedata.lectures.comptador')
        tg_profile = mdbpool.get_collection('tg_profile')
        conf_obj = self.pool.get('res.config')

        bc_exclude = eval(conf_obj.get(cursor, uid,
                                       'tg_profile_bc_exclude', '[]')
                          )

        result = {'badids': {}, 'notfound': {}}
        bad_bcs = tg_profile.find(
            {'name': meter_name, 'valid': False,
             'bc': {'$ne': '00'}
            },
            {'_id': 0, 'id': 1, 'bc': 1, 'timestamp': 1}
            )
        for measure in bad_bcs:
            # Check if bc has been excluded from checking
            if not 'bc' in measure or measure['bc'] in bc_exclude:
                continue
            date_meter = self.get_format_date(cursor, uid,
                                              measure['timestamp'])
            meter_id = meter_obj.get_meter(cursor, uid,
                                           meter_name, date_meter)
            if not meter_id:
                result['notfound'][measure['id']] = measure['timestamp']
                continue
            meter_exceptions = meter_obj.get_exceptions(cursor, uid, meter_id,
                                                        context=context)
            if 'CB' in meter_exceptions:
                continue
            # bit 7 = 1 is an invalid measure
            # bit 1 or 2 or 3 or 5 or 6 = 1 can be an invalid measure
            # Otherwise we can consider it valid
            # We reverse the result.
            # Otherwise bit 7 corresponds to 0 position in list
            control_bits = bin(int(measure['bc'], 16))[2:].zfill(8)[::-1]
            messages = []
            invalid_measure = False
            for bit, control_message in control_messages.items():
                if control_bits[bit] == '1' and bit == 7:
                    messages.append(control_message)
                    invalid_msg = _(u"Invalid")
                elif control_bits[bit] == '1' and bit in (1, 2, 3, 5, 6):
                    messages.append(control_message)
                    invalid_msg = _(u"Possible invalid")
            if messages:
                msg = _("%s measure %s CB: %s (%s)\n%s")
                date = self.get_format_date(cursor, uid, measure['timestamp'],
                                        hour=True, default_format=False)
                msg = msg % (invalid_msg, date, measure['bc'],
                             control_bits[::-1], '\n'.join(messages))
                result['badids'][measure['id']] = msg
        return result

    def check_same_time_many_measures(self, cursor, uid, meter_name,
                                      context=None):
        """ Look for measures that has same timestamp and meter but different
        " mesures
        """
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        tg_profile = mdbpool.get_collection('tg_profile')

        result = {'badids': {}, 'notfound': {}}
        # We also group by season, the day with hour change we must
        # two equal measures at the same hour but with different seaseon value
        measure_dups = tg_profile.aggregate([
            {'$match': {'name': meter_name}},
            {'$group': {'_id': {'name': '$name',
                        'timestamp': '$timestamp',
                        'season': '$season'
                        },
                          'diff': {'$addToSet': '$ai'},
                          'ids': {'$addToSet': '$id'},
                          'count': {'$sum': 1}
                        }
            },
            {'$match': {'count': {'$gt': 1}}},
        ])['result']

        for dup in measure_dups:
            timestamp = dup['_id']['timestamp']
            date_meter = self.get_format_date(cursor, uid, timestamp)
            meter_id = meter_obj.get_meter(cursor, uid, meter_name, date_meter)
            if not meter_id:
                for id in dup['ids']:
                    result['notfound'][id] = timestamp
                continue
            date_msg = self.get_format_date(cursor, uid, timestamp,
                                            hour=True, default_format=False)
            # Set the error message only in the first duplied measure, but
            # all of them should be in the invalid list
            measure_list = ','.join([str(x) for x in dup['diff']])
            for measure_id in dup['ids']:
                msg = ''
                if dup['ids'].index(measure_id) == 0:
                    msg = _("%s different reads found on %s. AI: %s")
                    msg = msg % (len(dup['ids']), date_msg, measure_list)
                result['badids'][measure_id] = msg
        return result

    def single_check_high_reactive(self, cursor, uid, measure, polissa,
                                   exceptions, context=None):
        """ Individual check if more reactive than 75% of active """
        config = self.pool.get('res.config')
        result = {}
        if not int(config.get(cursor, uid,
                              'tg_reactive_check', '1')):
             return result
        if measure['r1'] > measure['ai'] * 0.75:
            date = self.get_format_date(cursor, uid, measure['timestamp'],
                                        hour=True, default_format=False)
            msg = _("High reactive at %s ai: %s, r1: %s")
            msg = msg % (date,
                         measure['ai'] * measure['magn'],
                         measure['r1'] * measure['magn'])
            result[measure['id']] = msg
        return result

    def single_check_more_than_pot(self, cursor, uid, measure, polissa,
                                   exceptions, context=None):
        """
        Check if measure is higher than the consumption allowed by the policy \
            with tolerance. The tolerance is an absolute percentage and if is \
            0 this check is disabled. The tolerance is stored in \
            tg_profile_power_tolerance configuration variable. E.g. if power \
            is 10kWh and tolerance 120% then 12kWh are allowed. The default \
            tolerance value is 200.

        :param cursor: database cursor
        :param uid: user identifier
        :param measure: the measure as tg.profile
        :param polissa: the policy as osv.orm.browse_record object
        :param exceptions: meter exceptions (not used)
        :param context: optional context
        :return: dict with the measure id in the key and the error \
            description in the value
        """
        result = {}

        res_config = self.pool.get('res.config')
        tg_profile_power_tolerance = float(res_config.get(
            cursor,
            uid,
            'tg_profile_power_tolerance',
            '200'
        ))

        if tg_profile_power_tolerance == 0:
            return result

        #Ensure we are getting the right day. When 0 hour
        #it is 24 hour from previous day
        timestamp = measure['timestamp']
        hour = timestamp.hour
        if timestamp.hour == 0:
            timestamp -= timedelta(days=1)
            hour = 24
        day = self.get_format_date(cursor, uid, timestamp)
        #We use the same function as profiles module for getting
        #the period, so hour - 1 as done when profiling
        hour = '%02i:00:00' % (hour - 1)
        season = '0' if measure['season'] == 'W' else '1'
        if polissa.tarifa.get_num_periodes('tp') < 3:
            pot = polissa.potencia
        else:
            periode = polissa.tarifa.get_periode_ts(day, hour, season)
            # Converts grouped periods
            grouped_periods = polissa.tarifa.get_grouped_periods()
            periode = grouped_periods.get(periode, periode)
            for periode_pot in polissa.potencies_periode:
                if periode_pot.periode_id.name == periode:
                    pot = periode_pot.potencia
                    break
        power_consumed = measure['ai'] * measure['magn']
        # power_hourly_limit = pot * tg_profile_power_tolerance * 10
        power_hourly_limit = pot * 1000 * (tg_profile_power_tolerance / 100)
        if power_consumed > power_hourly_limit:
            date = self.get_format_date(cursor, uid, measure['timestamp'],
                                        hour=True, default_format=False)
            msg = _("Impossible consumption at %s ai: %s, Power: %s")
            msg = msg % (date, power_consumed, pot)
            result[measure['id']] = msg

        return result

    def create_case_invalid(self, cursor, uid, invalids, context=None):
        '''Create case with invalid measures'''
        case_obj = self.pool.get('crm.case')
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        profile_obj = self.pool.get('tg.profile')
        config_obj = self.pool.get('res.config')
        profile_mdb = mdbpool.get_collection('tg_profile')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'model': 'giscedata.polissa'})

        #Order invalids by timestamp first
        search_params = [('id', 'in', invalids.keys())]
        ordered_profile_ids = profile_obj.search(cursor, uid, search_params,
                                                 order='timestamp')

        today = datetime.now().strftime('%Y-%m-%d')
        date_check_from = config_obj.get(cursor, uid,
                                         'tg_profile_check_from', today)

        meters = {}
        # Search meter_id for each invalid and group then
        for profile_id in ordered_profile_ids:
            profile = profile_mdb.find_one({'id': profile_id},
                                           {'timestamp': 1, 'name': 1})
            date_meter = self.get_format_date(cursor, uid,
                                              profile['timestamp'])
            #Do not notify this ones in cases. They are too old.
            if date_meter < date_check_from:
                continue
            meter_id = meter_obj.get_meter(cursor, uid, profile['name'],
                                           date_meter)
            meters.setdefault(meter_id, [])
            meters[meter_id].append(invalids[profile_id])

        for meter_id in meters:
            #Do not open another case if already one in open or pending state
            #This way we let automatic checks to be closed manually when done
            #and do not create many cases with the same incidences
            if meter_obj.check_case_open(cursor, uid, meter_id, section='TG',
                                         category='TGPR', context=context):
                continue
            meter = meter_obj.browse(cursor, uid, meter_id)
            case_message = '\n'.join(meters[meter_id])
            case_vals = {
                'description': case_message,
                'ref2': 'giscedata.lectures.comptador,%i' % meter.id,
                'name': (_('TG Profile checkings for %s (%s)')
                         % (meter.name, meter.build_name_tg())),
                'polissa_id': meter.polissa.id,
            }
            case_id = case_obj.create_case_generic(cursor, uid,
                                                   [meter.polissa.id],
                                                   context=ctx,
                                                   section='TG',
                                                   category='TGPR',
                                                   extra_vals=case_vals)
            case_obj.case_open(cursor, uid, case_id)
        return True

    def create_case_notfound(self, cursor, uid, notfound, meter_name,
                             context=None):
        '''Create case with notfound measures'''
        case_obj = self.pool.get('crm.case')
        config_obj = self.pool.get('res.config')

        meter_obj = self.pool.get('giscedata.lectures.comptador')
        meter = meter_obj.search(cursor, uid, [('name', '=', meter_name)])
        cnc_name = ''
        if meter:
            cnc_name = str(meter_obj.read(cursor, uid, meter[0]).get('tg_cnc_id')[1])[:-3].upper()

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'model': False})

        today = datetime.now().strftime('%Y-%m-%d')
        date_check_from = config_obj.get(cursor, uid,
                                         'tg_profile_check_from', today)
        date_check_from = datetime.strptime(date_check_from,
                                            '%Y-%m-%d').date()
        msg = ''
        only_days = [date.date() for date in notfound.itervalues()
                     if date.date() >= date_check_from]
        #If we have no days to show, do not create any case
        if not only_days:
            return True
        sorted_days = sorted(list(set(only_days)))
        _not_found_msg = _('Meter %s not found on %s in concentrator %s\n')
        for timestamp in sorted_days:
            date = self.get_format_date(cursor, uid, timestamp,
                                        hour=False, default_format=False)
            msg += _not_found_msg % (meter_name, date, cnc_name)

        case_name = _('TG Profile out of range for {}').format(meter_name)

        return case_obj.append_description_to_case_by_name(
            cursor,
            uid,
            None,
            case_name,
            msg,
            'TG',
            'TGPR',
            context=context
        )

    def has_pending_validation(self, cursor, uid, meter_name, context=None):
        '''check if we have at least one invalid
        read associated to meter_name'''

        tg_profile_mdb = mdbpool.get_collection('tg_profile')
        profile = tg_profile_mdb.find_one({'name': meter_name,
                                           'valid': False})
        return profile and True or False

    def update_last_valid_profile(self, cursor, uid, meter_names,
                                  context=None):

        meter_obj = self.pool.get('giscedata.lectures.comptador')
        profile_obj = self.pool.get('tg.profile')

        if not context:
            context = {}
        context.update({'sync': False})

        if not isinstance(meter_names, (list, tuple)):
            meter_names = [meter_names]

        for meter_name in meter_names:
            search_params = [('name', '=', meter_name),
                             ('valid', '=', True)]
            profile_id = profile_obj.search(cursor, uid, search_params,
                                            limit=1, order='timestamp desc')
            if not profile_id:
                continue
            date = profile_obj.read(cursor, uid, profile_id,
                                    ['timestamp'])[0]['timestamp']
            meter_id = meter_obj.get_meter(cursor, uid, meter_name,
                                           date[:10], context=context)
            if not meter_id:
                continue
            meter_obj.write(cursor, uid, meter_id,
                            {'tg_last_profile': date},
                            context=context)
        return True

    _block_invalid_check_funcs = ['check_same_time_many_measures',
                                  'check_impossible_measure',
                                  'check_negative_read',
                                  'check_exported_energy',
                                  'check_exported_reactive',
                                  'check_control_bits',
                                 ]
    _single_check_funcs = [('single_check_high_reactive', False),
                           ('single_check_more_than_pot', True)]

    @job(queue='tg_validate_profile', timeout=1200, result_ttl=24*3600)
    def validate_individual(self, cursor, uid, ids, meter_name, context=None):
        '''This function has to validate the profile
        information that comes from smart meters
        '''
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        meter_exc_obj = self.pool.get('tg.validate.meter.exception')
        polissa_obj = self.pool.get('giscedata.polissa')
        tg_profile_obj = self.pool.get('tg.profile')
        conf_obj = self.pool.get('res.config')

        tg_profile_mdb = mdbpool.get_collection('tg_profile')

        #check if the meter has invalid profiles. If not, do not continue
        if not self.has_pending_validation(cursor, uid, meter_name):
            return True

        if not context:
            context = {}

        invalids = {}
        notfound = {}
        self.clean_duplicates(cursor, uid, meter_name)

        #Get full meter exceptions
        meter_exceptions = meter_exc_obj.get_serials(cursor, uid,
                                                     context=context)

        # If meter exception all measures are valid
        if meter_name in meter_exceptions:
            search_params = [('name', '=', meter_name),
                             ('valid', '=', False)]
            profile_ids = tg_profile_obj.search(cursor, uid, search_params,
                                                context=context)
            tg_profile_obj.validate(cursor, uid, profile_ids)
            return True

        # Block checks
        for func_name in self._block_invalid_check_funcs:
            func = getattr(self, func_name)
            res = func(cursor, uid, meter_name, context=context)
            invalids = self.update_invalid(cursor, uid, invalids,
                                           res['badids'])
            notfound.update(res['notfound'])

        # After block checks search for reads not included in invalids
        invalid_ids = list(set(invalids.keys() + notfound.keys()))
        to_be_validated = tg_profile_mdb.find({
                            'name': meter_name,
                            'valid': False,
                            'id': {'$nin': invalid_ids}
        })
        # Individual measure checks
        check_individual = int(conf_obj.get(cursor, uid,
                                        'tg_profile_check_individual',
                                        '1'))
        if check_individual:
            for measure in to_be_validated:
                timestamp = measure['timestamp']
                date_meter = self.get_format_date(cursor, uid, timestamp)
                meter_id = meter_obj.get_meter(cursor, uid, meter_name,
                                               date_meter)
                if not meter_id:
                    notfound[measure['id']] = timestamp
                    continue
                meter_exceptions = meter_obj.get_exceptions(cursor, uid,
                                                            meter_id,
                                                            context=context)
                meter = meter_obj.browse(cursor, uid, meter_id)
                polissa = polissa_obj.browse(cursor, uid, meter.polissa.id,
                                             context={'date': date_meter})
                invalid = False
                for func_name, mark_invalid in self._single_check_funcs:
                    func = getattr(self, func_name)
                    res = func(cursor, uid, measure, polissa,
                               meter_exceptions, context=context)
                    if res:
                        invalids = self.update_invalid(cursor, uid, invalids,
                                                       res)
                        #Mark as invalid only if not invalid before
                        if mark_invalid and not invalid:
                            invalid = mark_invalid

                if not invalid:
                    # If we arrive here the measure is valid,
                    # no inserts during the process
                    tg_profile_obj.validate(cursor, uid, [measure['id']])
        else:
            measure_ids = [x['id'] for x in to_be_validated]
            tg_profile_obj.validate(cursor, uid, measure_ids)

        self.update_last_valid_profile(cursor, uid, meter_name,
                                       context=context)

        create_cases = int(conf_obj.get(cursor, uid,
                                        'tg_create_profile_cases', '1'))
        if create_cases:
            if invalids:
                self.create_case_invalid(cursor, uid, invalids,
                                         context=context)
            if notfound:
                self.create_case_notfound(cursor, uid, notfound, meter_name,
                                          context=context)
        return True

    def validate_profile(self, cursor, uid, ids, meter_names=[],
                         context=None):
        '''This function has to validate the profile
        information that comes from smart meters
        '''

        if not meter_names:
            meter_names = self.get_all_meter_names(cursor, uid)

        jobs_ids = []
        for meter_name in meter_names:
            j = self.validate_individual(cursor, uid, ids, meter_name,
                                         context=context)
            jobs_ids.append(j.id)

        if len(jobs_ids) > 1:
            create_jobs_group(
                cursor.dbname, uid, _('Validate profile'),
                'tg.validate.profile', jobs_ids
            )
        return True

TgProfileValidate()


class TgLecturesComptador(osv.osv):

    _name = 'giscedata.lectures.comptador'
    _inherit = 'giscedata.lectures.comptador'

    def get_cch_enabled(self, cursor, uid, start, end, tg='1', context=None):
        """Get the meters enabled with CCH in a range of dates
        """
        sql_file = get_module_resource(
            'giscedata_lectures_telegestio', 'sql', 'cch_enabled.sql'
        )
        with open(sql_file, 'r') as f:
            sql = f.read()

        cursor.execute(sql, {'start': start, 'end': end, 'tg': tg})
        return [x[0] for x in cursor.fetchall()]

    @cache()
    def get_periods(self, cursor, uid, tarifa_id, context=None):
        '''return al periods of tarifa_id'''
        periode_obj = self.pool.get('giscedata.polissa.tarifa.periodes')
        search_params = [('tarifa', '=', tarifa_id)]
        periodes_ids = periode_obj.search(cursor, uid, search_params)
        res = {'te': {}, 'tp': {}}
        for periode in periode_obj.browse(cursor, uid,
                                          periodes_ids,
                                          context=context):
            if periode.tipus == 'te':
                res['te'].update({periode.name: periode.id})
            elif periode.tipus == 'tp':
                res['tp'].update({periode.name: periode.id})
        return res

    def existing_read(self, cursor, uid, meter_id, date,
                      tarifa_id, context=None):
        '''returns true if it exists another read with same date'''
        lectura_obj = self.pool.get('giscedata.lectures.lectura')
        search_params = [('comptador', '=', meter_id),
                         ('tipus', '=', 'A'),
                         ('name', '=', date),
                         ('periode.tarifa', '=', tarifa_id)]
        lectura_ids = lectura_obj.search(cursor, uid, search_params,
                                         context=context)
        if lectura_ids:
            return True
        return False

    def get_tg_prefix(self, cursor, uid, meter_id, context=None):
        if isinstance(meter_id, (list, tuple)):
            meter_id = meter_id[0]
        q = OOQuery(self, cursor, uid)
        sql = q.select(['serial.product_id.default_code'], only_active=False).where([
            ('id', '=', meter_id),
        ])
        cursor.execute(*sql)
        code = cursor.fetchone()
        return code[0] if code else ''

    def get_search_meter_tg_params(self, cursor, uid, tg_name, context):
        config = self.pool.get('res.config')
        len_codi = int(config.get(cursor, uid, 'tg_clean_serial_chars', '5'))
        meter_product_code = tg_name[0:len_codi]
        return [
            ('serial.product_id.default_code', '=', meter_product_code)
        ]

    def build_names_tg(self, cursor, uid, meters_id, context=None):
        config = self.pool.get('res.config')
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        len_codi = int(config.get(cursor, uid, 'tg_clean_serial_chars', '5'))
        len_serial = int(config.get(cursor, uid, 'tg_serial_length', '13'))
        len_serie = len_serial - len_codi
        # Build full name from serial if present
        res = {}
        for meter in self.browse(cursor, uid, meters_id, context=context):
            if meter.serial:
                default_code = meter_obj.get_tg_prefix(cursor, uid, meter.id,
                                                       context=context)
                meter_name = '%s%s' % (default_code,
                                       meter.serial.name.zfill(len_serie) or '')
            else:
                meter_name = meter.name
            res[str(meter.id)] = meter_name
        return res

    def build_name_tg(self, cursor, uid, meter_id, context=None):
        if not isinstance(meter_id, (list, tuple)):
            meter_id = [meter_id]
        names_tg = self.build_names_tg(cursor, uid, meter_id, context=context)
        return names_tg[str(meter_id[0])]

    @cache(timeout=60)
    def check_case_open(self, cursor, uid, meter_id, section=False,
                        category=False, context=None):
        '''checks if there is another case in open or pending state'''

        case_obj = self.pool.get('crm.case')
        section_id = False
        categ_id = False
        #search for section
        if section:
            section_obj = self.pool.get('crm.case.section')
            search_params = [('code', '=', section)]
            section_ids = section_obj.search(cursor, uid, search_params)
            section_id = section_ids and section_ids[0] or False
        #Search for category only if section was found
        if section_id and category:
            categ_obj = self.pool.get('crm.case.categ')
            search_params = [('categ_code', '=', category),
                             ('section_id', '=', section_id)]
            categ_ids = categ_obj.search(cursor, uid, search_params)
            categ_id = categ_ids and categ_ids[0] or False

        #Search for cases
        ref2 = 'giscedata.lectures.comptador,%s' % meter_id
        search_params = [('ref2', '=', ref2),
                         ('state', 'in', ('open', 'pending'))]
        if section_id:
            search_params.append(('section_id', '=', section_id))
        if categ_id:
            search_params.append(('categ_id', '=', categ_id))
        case_ids = case_obj.search(cursor, uid, search_params)
        return case_ids and True or False

    @cache()
    def get_exceptions(self, cursor, uid, meter_id, context=None):
        '''return list with exception codes for validating this meter'''
        if isinstance(meter_id, (list, tuple)):
            meter_id = meter_id[0]

        query = open('%s/giscedata_lectures_telegestio/sql/exception_query.sql'
                   % config['addons_path']).read()
        cursor.execute(query, (meter_id,))
        return [x[0] for x in cursor.fetchall()]

    @cache(timeout=600)
    def get_meter(self, cursor, uid, serial, date_read, context=None):
        '''returns id of the meter with serial'''

        if not context:
            context = {}

        cfg_obj = self.pool.get('res.config')

        # How many characters do we have to clean from serial?
        if context.get('clean_serial', True):
            tg_clean_serial = int(cfg_obj.get(cursor, uid,
                                          'tg_clean_serial_chars', '0'))
            serial = serial[tg_clean_serial:]
            # stripping left zeroes
            if int(cfg_obj.get(cursor, uid,
                               'tg_clean_left_zeroes_serial', '0')):
                rs = re.findall('^0+(.*)', serial)
                if rs:
                    serial = rs[0]
        # first of all, search only active meters in date_end
        # We can only find as much as one, because there is a constraint
        # that does not allow two meters with same serial being active
        search_params = [
                ('serial.name', '=', serial),
                ('polissa.state', 'not in', CONTRACT_IGNORED_STATES),
                ('data_alta', '<=', date_read),
                # Avoids meters registered prior to policy register date.
                ('polissa.data_alta', '<=', date_read)
            ]
        from osv.expression import OOQuery
        q = OOQuery(self, cursor, uid)
        sql = q.select(['id']).where(search_params)
        cursor.execute(*sql)
        for res in cursor.fetchall():
            return res[0]

        # Search no active meters if no results.
        context.update({'active_test': False})
        # Update search params
        search_params.extend([('data_baixa', '>=', date_read)])
        q = OOQuery(self, cursor, uid)
        sql = q.select(['id'], limit=1, only_active=False).where(search_params)
        cursor.execute(*sql)
        for res in cursor.fetchall():
            return res[0]

        if context.get('validate', False):
            # Allow one day before data_alta if no results yet.
            context.update({'active_test': True})
            # Update search params
            try:
                day_before_read = datetime.strptime(
                    date_read, '%Y-%m-%d %H:%M:%S') + relativedelta(days=1)
            except ValueError as e:
                day_before_read = datetime.strptime(
                    date_read, '%Y-%m-%d') + relativedelta(days=1)
            except TypeError as e:
                day_before_read = date_read
            search_params = [
                ('serial.name', '=', serial),
                ('polissa.state', 'not in', CONTRACT_IGNORED_STATES),
                ('data_alta', '<=', day_before_read),
                # Avoids meters registered prior to policy register date.
                ('polissa.data_alta', '<=', day_before_read)
            ]
            q = OOQuery(self, cursor, uid)
            sql = q.select(['id'], limit=1, only_active=False).where(
                search_params)
            cursor.execute(*sql)
            for res in cursor.fetchall():
                return res[0]

        return False

    @cache()
    def has_in_tpl(self, cr, uid, context=None):
        '''Has in_tpl (cached)'''
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        return meter_obj.fields_get(cr, uid).get('in_tpl', False)

    def create_read_from_tg(self, cursor, uid, ids, context=None):
        '''Create reads from validated tg billing info'''
        if not context:
            context = {}
        db = pooler.get_db_only(cursor.dbname)
        global_date_limit = context.get('date_limit', '3000-01-01')
        force_tg_read = context.get('force_tg_read', False)
        billing_obj = self.pool.get('tg.billing')
        origin_obj = self.pool.get('giscedata.lectures.origen')
        lect_obj = self.pool.get('giscedata.lectures.lectura')
        lect_pot_obj = self.pool.get('giscedata.lectures.potencia')
        polissa_obj = self.pool.get('giscedata.polissa')
        config = self.pool.get('res.config')
        global_reactive_insert = config.get(
            cursor,
            uid,
            'tg_reactive_insert',
            'always'
        )

        res = {}
        for meter in self.browse(cursor, uid, ids, context=context):
            try:
                date_limit = global_date_limit
                tmp_cr = db.cursor()
                res[meter.id] = {'date_read': False,
                                 'existing': False}
                #If meter is not active, billing info cannot be
                #greater than meter data_baixa
                if meter.data_baixa:
                    date_limit = min(meter.data_baixa,
                                     date_limit)
                #Billing date is one day ahead so
                #date_limit must be + 1 day
                date_limit_plus1 = datetime.\
                                    strftime(datetime.strptime(date_limit,
                                                               '%Y-%m-%d')
                                             + timedelta(days=1), '%Y-%m-%d')
                #Build full name from serial if present
                meter_name = self.build_name_tg(cursor, uid, meter.id, context)
                billing_dates = billing_obj.get_last_billing_date(tmp_cr, uid,
                                                              meter_name,
                                                              date_limit_plus1,
                                                              context=context)
                if not billing_dates['month'] and not billing_dates['day']:
                    #No valid billing info before date_limit
                    #Cannot insert any read
                    continue
                elif billing_dates['month'] and not billing_dates['day']:
                    type = 'month'
                    date = billing_dates['month']
                elif not billing_dates['month'] and billing_dates['day']:
                    type = 'day'
                    date = billing_dates['day']
                elif billing_dates['month'] >= billing_dates['day']:
                    type = 'month'
                    date = billing_dates['month']
                else:
                    type = 'day'
                    date = billing_dates['day']
                #Check for polissa values in date
                polissa_id = meter.polissa.id
                polissa = polissa_obj.browse(tmp_cr, uid, polissa_id,
                                             context={'date': date})

                # If polissa is integrated with CCH we can only go 3 days
                # ago
                if polissa.tg == '1':
                    days_back = 3
                else:
                    days_back = int(
                        config.get(cursor, uid, 'tg_max_days_back', 3)
                    )

                max_date_ago_tg = (
                    datetime.strptime(date_limit, '%Y-%m-%d') -
                    timedelta(days=days_back)
                ).strftime('%Y-%m-%d')
                if days_back and date < max_date_ago_tg:
                    continue

                num_periods = (polissa.tarifa.
                               get_num_periodes(context={'agrupar_periodes':
                                                          False}))
                billing_data = [
                    {'tariff_id': polissa.tarifa.id,
                     'data': billing_obj.get_billing(tmp_cr, uid,
                                                       meter_name, date,
                                                       type, num_periods,
                                                       context=context)
                     }
                ]
                if not billing_data:
                    continue
                #TODO: Insert billing data as reads
                #Telelectura origin is the one with codi 10
                origin_id = origin_obj.search(tmp_cr, uid,
                                              [('codi', '=', '60')])[0]
                meter_id = meter.id
                #only get date part from datetime response
                #minus 1 day for invoice date
                date_read_plus1 = billing_data[0]['data'][0]['date_end'][:10]
                date_read = datetime.\
                                strftime(datetime.strptime(date_read_plus1,
                                                           '%Y-%m-%d')
                                         - timedelta(days=1), '%Y-%m-%d')
                #Assign values from polissa that will be used later
                tarifa_id = polissa.tarifa.id
                potencia = polissa.potencia

                #Check if polissa has a tarifa or pot change between
                #demanded read date and real read date to be inserted
                tariff_change = False
                if date[:10] != date_read:
                    try:
                        polissa_date_read = polissa_obj.browse(tmp_cr, uid,
                                                               polissa_id,
                                                               context={'date':
                                                               date_read})
                    except Exception as e:
                        date_after = datetime.strptime(
                            date_read, '%Y-%m-%d') + relativedelta(
                            days=1)
                        polissa_date_read = polissa_obj.browse(tmp_cr, uid,
                                                               polissa_id,
                                                               context={'date':
                                                               date_after})
                    if (polissa.tarifa.id != polissa_date_read.tarifa.id):
                        #If tarifa has changed, recompute num_periods and
                        #get again billing_data because original is not correct
                        tariff_change = True
                        tarifa_id = polissa_date_read.tarifa.id
                        num_periods = (polissa_date_read.tarifa.
                           get_num_periodes(context={'agrupar_periodes':
                                                     False}))

                        new_billing_data = [
                            {'tariff_id': polissa_date_read.tarifa.id,
                             'data': billing_obj.get_billing(tmp_cr, uid,
                                                             meter_name,
                                                             date_read_plus1,
                                                             type, num_periods,
                                                             context=context)
                             }
                        ]

                        billing_data.extend(new_billing_data)

                    if potencia != polissa_date_read.potencia:
                        potencia = polissa_date_read.potencia

                res[meter_id].update({'date_read': date_read})
                #If date_read before last meter read do not insert
                #Do not check if we want to force creation
                if not force_tg_read:
                    last_read = meter.data_ultima_lectura(tarifa_id)
                    if last_read >= date_read:
                        res[meter_id].update({'existing': True})
                        continue
                #If yet another read present, skip meter
                if self.existing_read(tmp_cr, uid, meter_id,
                                      date_read, tarifa_id,
                                      context=context):
                    res[meter_id].update({'existing': True})
                    continue
                # Unload meter from TPL when TG read is inserted
                if self.has_in_tpl(tmp_cr, uid):
                    meter_obj = self.pool.get('giscedata.lectures.comptador')
                    in_tpl = meter_obj.read(tmp_cr, uid, meter_id, ['in_tpl'])
                    if in_tpl['in_tpl']:
                        vals = {'in_tpl': False}
                        meter_obj.write(tmp_cr, uid, [meter_id], vals,
                                        context={'sync': False})
                # common values for all reads
                common_vals = {'comptador': meter_id,
                               'name': date_read,
                               'origen_id': origin_id}

                new_billing_data = list(billing_data)
                pot_periods = 0
                energy_periods = 0
                for tariff in new_billing_data:
                    tariff['periods'] = self.get_periods(tmp_cr, uid,
                                                         tariff['tariff_id'],
                                                         context=context)
                    pot_periods += len(tariff['periods']['tp'].keys())
                    energy_periods += len(tariff['periods']['te'].keys())
                # If only one pot period and more than one energy period
                # max is the max for all periods
                all_periods_max = 0
                if pot_periods == 1 and energy_periods > 1:
                    all_periods_max = -1
                    for tariff in new_billing_data:
                        all_periods_max = \
                            max([all_periods_max] + [x['max'] for x in
                                                     tariff['data']])
                if polissa.reactive_conf != 'global':
                    reactive_insert = polissa.reactive_conf
                else:
                    if global_reactive_insert in ('always', 'never', 'zero'):
                        reactive_insert = global_reactive_insert
                    else:
                        reactive_insert = 'always'

                for tariff in new_billing_data:
                    for billing in tariff['data']:
                        self.create_energy_readings_tg(
                            tmp_cr, uid, billing, common_vals, tariff['periods']
                            , potencia, reactive_insert)
                        self.create_power_readings_tg(
                            tmp_cr, uid, billing, common_vals, tariff['periods']
                            , all_periods_max)
                tmp_cr.commit()
            except Exception, exc:
                #TODO. Create case with error entering reads
                tmp_cr.rollback()
            finally:
                tmp_cr.close()

        return res

    def create_energy_readings_tg(self, cursor, uid, billing, common_vals,
                               tarifa_periods, potencia, reactive_insert):
        lect_obj = self.pool.get('giscedata.lectures.lectura')
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        config = self.pool.get('res.config')
        meter_id = common_vals.get('comptador', [])
        tariff = meter_obj.q(cursor, uid).read([
            'polissa.tarifa.name'
        ]).where([('id', '=', meter_id)])

        if tariff and tariff[0]['polissa.tarifa.name'].startswith('RE'):
            period_name = 'P0'
        elif billing['period'] == 0:
            period_name = 'P1'
        else:
            period_name = 'P%s' % billing['period']
        vals = common_vals.copy()
        # Insert active values
        vals.update({'tipus': 'A',
                     'periode': tarifa_periods['te'][period_name],
                     'lectura': billing['ai'],
                     })
        lect_obj.create(cursor, uid, vals)
        # Insert reactive values. Mandatory if potencia > 15
        vals = common_vals.copy()
        if potencia > 15:
            reactive_insert = 'always'
        if reactive_insert != 'never':
            tg_react_quads = config.get(
                cursor,
                uid,
                'tg_react_quads',
                '[1]'
            )
            tg_react_quads = literal_eval(tg_react_quads)
            tg_react = 0
            for tg_quad in tg_react_quads:
                tg_react += billing['r%s' % tg_quad]
            insert_option = {
                'always': tg_react,
                'zero': 0
            }
            reactive_read = insert_option[reactive_insert]
            vals.update({'tipus': 'R',
                         'periode': tarifa_periods['te'][period_name],
                         'lectura': reactive_read,
                         })
            lect_obj.create(cursor, uid, vals)

    def create_power_readings_tg(self, cursor, uid, billing, common_vals,
                              tarifa_periods, all_periods_max):
        lect_pot_obj = self.pool.get('giscedata.lectures.potencia')
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        meter_id = common_vals.get('comptador', [])
        tariff = meter_obj.q(cursor, uid).read([
            'polissa.tarifa.name'
        ]).where([('id', '=', meter_id)])

        if tariff and tariff[0]['polissa.tarifa.name'].startswith('RE'):
            period_name = 'P0'
        elif billing['period'] == 0:
            period_name = 'P1'
        else:
            period_name = 'P%s' % billing['period']
        # Insert max values. Max in billing comes in watts
        # lectures potencia does not have tipus field
        if tarifa_periods['tp'].get(period_name, False):
            vals = common_vals.copy()
            max_value = all_periods_max or billing['max']
            vals.update({'periode': tarifa_periods['tp'][period_name],
                         'lectura': round(int(max_value) / 1000.0, 3),
                         'exces': 0,
                         })
            lect_pot_obj.create(cursor, uid, vals)

    def _get_intensitat_tel(self, cursor, uid, ids, name, arg, context=None):
        '''retorna la intensitat teorica segons la potencia
        programada al comptador'''

        res = {}
        for comptador in self.browse(cursor, uid, ids, context):
            try:
                if not comptador.tg_icp_active:
                    continue
            except Exception, e:
                continue
            try:
                polissa = comptador.polissa
                if not polissa.tensio_normalitzada:
                    res[comptador.id] = 0
                    continue
            except Exception, e:
                res[comptador.id] = 0
                continue
            tensio = polissa.tensio
            nom_tensio = polissa.tensio_normalitzada.name
            pot = comptador.tg_icp_potencia * 1000  # W
            #si es trifasic fem un càlcul diferent dels monofasics
            if nom_tensio.startswith('3x'):
                intensitat = round(pot / (tensio * math.sqrt(3)), 2)
            else:
                intensitat = round(pot / tensio, 2)
            res[comptador.id] = intensitat

        return res

    def _get_comptador_from_polissa(self, cursor, uid, ids, context=None):
        '''get comptador from polissa'''
        if not context:
            context = {}
        search_params = [('polissa', 'in', ids)]
        comptador_obj = self.pool.get('giscedata.lectures.comptador')
        return comptador_obj.search(cursor, uid,
                                    search_params,
                                    context=context)

    def _get_cnc_conn(self, cursor, uid, ids, name, arg, context=None):
        '''True if we have an associated cnc for the meter'''

        res = {}
        for meter in self.browse(cursor, uid, ids, context=context):
            try:
                res[meter.id] = meter.tg_cnc_id and True or False
            except Exception, e:
                res[meter.id] = False
        return res

    def _get_pot_from_polissa(self, cursor, uid, context=None):
        if not context:
            context = {}
        polissa_id = context.get('polissa_id', False)
        res = 0
        if polissa_id:
            polissa_obj = self.pool.get('giscedata.polissa')
            vals = polissa_obj.read(cursor, uid, polissa_id, ['potencia'])
            res = vals and vals['potencia'] or 0
        return res

    def get_curve_type_dict(self, cursor, uid, context):
        return CURVE_TYPE_DICT

    def get_curve_tg(self, cursor, uid, ids, time_from, time_until,
                     cch_type='all', context=None):
        """Gets curve from time_from to time_until
            cch_type:
               * 'all': every CCH line
               * 'valid': only marked as valid and from meter (cch_bruta)
               * 'fact': "fact" values only when 'cch_fact' is True
        """
        if not context:
            context = {}

        if isinstance(ids, (list, tuple)):
            ids = ids[0]

        tp_obj = self.pool.get('tg.profile')

        conta_fields = ['name', 'data_alta', 'data_baixa', 'active']

        conta_vals = self.read(cursor, uid, ids, conta_fields)
        meter_id = conta_vals['id']
        tg_name = self.build_name_tg(cursor, uid, meter_id)

        first_meter_slot = conta_vals['data_alta'] + ' 01:00:00'
        start = max(time_from, first_meter_slot)
        search_vals = [('name', '=', tg_name),
                       ('timestamp', '>=', start),
                       ('timestamp', '<', time_until),
                       ]

        if conta_vals['data_baixa'] and not conta_vals['active']:
            # you must take until next day 00:00:00
            end_date = datetime.strptime(conta_vals['data_baixa'], '%Y-%m-%d')
            end_time = (end_date + timedelta(days=1)).strftime(
                '%Y-%m-%d 00:00:00'
            )
            search_vals.append(('timestamp', '<=', end_time))

        # Only Valid
        if cch_type in ['valid']:
            search_vals.extend(
                [
                    ('valid', '=', True),
                    ('cch_bruta', '=', True)
                ]
            )
        # Only Fact
        if cch_type in ['fact']:
            search_vals.append(('cch_fact', '=', True))


        tp_ids = tp_obj.search(cursor, uid, search_vals, 0, 0,
                               'timestamp asc')

        return tp_ids

    def cch_format_P5D(self, cursor, uid, tp_val, cups_name, context=None):
        """"Returs a TgProfile record list as a P5D format list"""

        season = tp_val['season'] == 'W' and '0' or '1'
        timestamp = datetime.strptime(tp_val['timestamp'],
                                      '%Y-%m-%d %H:%M:%S')
        profile_datetime = timestamp.strftime('%Y/%m/%d %H:%M')
        magnitude = tp_val['magn']

        line = [cups_name[:22],
                profile_datetime,
                season,
                int(tp_val['ai'] * magnitude),
                int(tp_val['ae'] * magnitude),
                '',
                ]

        return line

    def cch_format_F5D(self, cursor, uid, tp_val, cups_name, context=None):
        """"Returs a TgProfile record list as a F5D format list"""

        if not context:
            context = {}

        season = tp_val['season'] == 'W' and '0' or '1'
        timestamp = datetime.strptime(tp_val['timestamp'],
                                      '%Y-%m-%d %H:%M:%S')
        magnitude = tp_val['magn']

        profile_datetime = timestamp.strftime('%Y/%m/%d %H:%M')
        fact_number = context.get('fact_number', 'F0000000000')
        kind = int(tp_val.get('kind_fact', 6))
        # distinct kind_facts in whole curve
        kind_facts = context.get('kind_facts', [])

        sureness = (kind == 1 and 1 or 0)
        if kind == 3 and len(kind_facts) == 1 and kind_facts[0] == 3:
            sureness = 1

        line = [cups_name[:22],
                profile_datetime,
                season,
                int(tp_val['ai_fact'] * magnitude),
                int(fill_measure('ae', tp_val) * magnitude),
                int(fill_measure('r1', tp_val) * magnitude),
                int(fill_measure('r2', tp_val) * magnitude),
                int(fill_measure('r3', tp_val) * magnitude),
                int(fill_measure('r4', tp_val) * magnitude),
                kind,
                sureness,
                fact_number,
                '',
                ]

        return line

    def cch_format_CONS(self, cursor, uid, tp_val, cups_name, context=None):
        """"Returs a TgProfile record list as a CONS format list"""

        if not context:
            context = {}

        timestamp = datetime.strptime(tp_val['timestamp'],
                                      '%Y-%m-%d %H:%M:%S')
        magnitude = tp_val['magn']

        # hour calculation
        profile_time = timestamp.strftime('%H')
        if context.get('is_dst_day', False):
            new_time = int(profile_time) + context.get('hour_add', 0)
            # only after 2 AM protection
            if new_time >= 2:
                profile_time = '{:02}'.format(new_time)
        # date calculation (00 -> 24)
        if profile_time == '00':
            profile_time = '{0:02}'.format(24 + context.get('hour_add', 0))
            curve_date = (timestamp - timedelta(days=1))
        else:
            curve_date = timestamp

        profile_date = curve_date.strftime('%d/%m/%Y')

        measure = tp_val.get('ai_fact', tp_val.get('ai', 0))
        val = '{0:011.3f}'.format(
            (int(measure * magnitude) / 1000.0)
        ).replace('.', ',')

        # Real if 'kind_fact' not provided
        method = tp_val.get('kind_fact', 1) > 0 and 'E' or 'R'

        line = [cups_name[:22],
                profile_date,
                profile_time,
                val,
                method
                ]

        return line

    def get_cch(self, cursor, uid, meter_ids, cch_type, time_from, time_until,
                context=None):

        if not context:
            context = {}

        logger = netsvc.Logger()

        if not isinstance(meter_ids, (list, tuple)):
            meter_ids = [meter_ids]

        res = dict([(m, []) for m in meter_ids])

        curve_types = self.get_curve_type_dict(cursor, uid, context=context)

        (curve_type, curve_format, curve_desc) = curve_types.get(
            cch_type, curve_types['cch_val'])
        # date / time validation
        # if not time in var, we take:
        #  hour 1 in time_from
        #  hour 0 in time_until + 1 day
        if ':' not in time_from:
            start_time = '{0} 01:00:00'.format(time_from)
        else:
            start_time = time_from

        if ':' not in time_until:
            end_date = datetime.strptime(time_until, '%Y-%m-%d')
            new_end_date = end_date + timedelta(days=1, hours=1)
            end_time = '{0}'.format(new_end_date)
        else:
            end_time = time_until

        tp_obj = self.pool.get('tg.profile')
        polissa_obj = self.pool.get('giscedata.polissa')
        counter = 0

        for meter_id in meter_ids:
            counter += 1
            # searching for cups
            if meter_id:
                polissa_id = self.read(cursor, uid, meter_id,
                                       ['polissa'])['polissa'][0]
                cups_vals = polissa_obj.read(cursor, uid, polissa_id,
                                             ['cups'])
                cups_name = '{0:<22}'.format(cups_vals['cups'][1])
            else:
                cups_name = '                   n/a'

            tg_name = self.build_name_tg(cursor, uid, meter_id)
            logger.notifyChannel(
                'exporta_curves_tg', netsvc.LOG_INFO,
                _(u'%s/%s: %04d/%04d [%s]') % (tg_name, cups_name, counter,
                                               len(meter_ids), curve_desc))

            tp_ids = self.get_curve_tg(cursor, uid, [meter_id], start_time,
                                       end_time, curve_type, context)

            func_name = 'cch_format_%s' % curve_format
            format_func = getattr(self, func_name)
            season = ''
            context.update({'is_dst_day': False, 'hour_add': 0})
            change_day = ''

            tp_values = sorted(
                tp_obj.read(cursor, uid, tp_ids, []),
                key=lambda x: (x['timestamp'], x['season'])
            )

            # to calculate sureness (firmeza) in F5D format file
            kind_facts = tuple(set([t.get('kind_fact', 6) for t in tp_values]))
            context.update({'kind_facts': kind_facts})

            for tp_val in tp_values:
                timestamp = datetime.strptime(tp_val['timestamp'],
                                              '%Y-%m-%d %H:%M:%S')
                # to detect season change on complete curves only (FACT, CONS)
                # used in CCH_CONS
                if season and (tp_val['season'] != season):
                    context.update({'is_dst_day': True})
                    change_day = timestamp.strftime('%Y-%m-%d')
                    if tp_val['season'] == 'S':
                        # From Winter to Summer
                        context.update({'hour_add': -1})
                    else:
                        # From Summer to Winter
                        context.update({'hour_add': 1})
                if context.get('is_dst_day'):
                    new_day = timestamp.strftime('%Y-%m-%d')
                    # we change on hour 1 to generate hour 25/23 on dst day
                    if new_day != change_day and timestamp.hour != 0:
                        context.update({'is_dst_day': False, 'hour_add': 0})
                season = tp_val['season']

                line = format_func(cursor, uid, tp_val, cups_name, context)

                res[meter_id].append(line)

        return res

    def get_cch_filename(
            self,
            cursor,
            uid,
            meter_ids,
            cch_type,
            version,
            cch_date=None,
            context=None,
            default_marketer=None
    ):
        """
        The filename for a CCH file.

        :param cursor: database cursor
        :param uid: user identifier
        :param meter_ids: meter identifiers
        :param cch_type: type of CCH, valid types in CURVE_TYPE_DICT
        :param version: version
        :param cch_date: date to get the proper values of policy meter
        :param context: dict with the context
        :param default_marketer: the marketer code to be used if meter isn't \
            provided
        :return: string with the filename
        """

        if not context:
            context = {}

        if not isinstance(meter_ids, (tuple, list)):
            meter_ids = [meter_ids]

        user_obj = self.pool.get('res.users')
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        polissa_obj = self.pool.get('giscedata.polissa')
        partner_obj = self.pool.get('res.partner')

        curve_types = self.get_curve_type_dict(cursor, uid, context)

        (curve_type, curve_format) = curve_types.get(cch_type,
                                                         ('valid', 'P5D'))[:2]

        version = str(int(version))

        if meter_ids and meter_ids[0] is not None:
            meter = meter_obj.browse(cursor, uid, meter_ids[0], context=context)
            # We need a curve date to select the correct modcon
            if not cch_date:
                modcon_date = datetime.today().strftime("%Y-%m-%d")
            else:
                modcon_date = cch_date
            ctx = context.update({'date': modcon_date})
            pol_vals = polissa_obj.read(
                cursor,
                uid,
                meter.polissa.id,
                ['comercialitzadora', 'cups'],
                context=ctx
            )
            cups_name = pol_vals['cups'][1]
            com_vals = partner_obj.read(
                cursor,
                uid,
                pol_vals['comercialitzadora'][0],
                ['ref'],
                context=context
            )
            com = com_vals['ref']
        else:
            cups_name = 'all'
            if default_marketer is not None:
                com = default_marketer
            else:
                com = 'all'

        today = datetime.today().strftime("%Y%m%d")
        extension = context.get('extension', 'csv')

        distri = user_obj.browse(cursor, uid, uid, context=context)
        dis = distri.company_id.partner_id.ref

        if cch_type in ('cch_cons',) and extension == 'xls':
            filename = "CONS_{0}_{1}.{2}".format(
                cups_name,
                today,
                extension
            )
        elif cch_type in ('cch_val', 'cch_fact', 'cch_cons'):
            filename = "{0}_{1}_{2}_{3}.{4}".format(
                curve_format.upper(), dis, com, today, version
            )
        elif cch_type == 'cch_cide_chf':
            now = datetime.now().strftime("%Y%m%d%H%M%S")
            filename = "{0}_{1}_CHF_{2}.csv".format(dis, com, now)
        else:
            filename = "{0}_{1}.csv".format(cch_type.upper(), today)

        return filename

    def get_as_xls(self, cursor, uid, output, curves, alt_sheetname='CCH_CONS',
                   context=None):
        wb = xlwt.Workbook()
        for curva in curves.values():
            # sheet name : CUPS
            sheet_name = curva and curva[0] and curva[0][0] or alt_sheetname
            ws = wb.add_sheet(sheet_name)
            count_line = 0
            for line in curva:
                count_field = 0
                for val in line:
                    ws.write(count_line, count_field, val)
                    count_field += 1
                count_line += 1
        wb.save(output)

    def get_comer_cch_val(self, cursor, uid, comer_id, context=None):

        if isinstance(comer_id, (tuple, list)):
            comer_id = comer_id[0]

        modcon_obj = self.pool.get('giscedata.polissa.modcontractual')

        # We search all cups of the comer throught modcons
        modcon_obj.search(cursor, uid, [('comercialitzadora', '=', comer_id)])

    def _get_multi_meter_info(self, cursor, uid, ids, name, arg, context=None):
        meter_exc_obj = self.pool.get('tg.validate.meter.exception')
        # Get the meters exceptions
        meter_exceptions = meter_exc_obj.get_serials(cursor, uid,
                                                     context=context)
        res = dict.fromkeys(ids, False)

        for meter_id, meter_name in self.build_names_tg(cursor, uid,
                                                        ids).items():
            res[int(meter_id)] = {
                'tg_meter_exception': meter_name in meter_exceptions,
                'meter_tg_name': meter_name,
            }
        return res

    def _search_meter_tg(self, cursor, uid, obj, name, args, context=None):
        if not context:
            context = {}
        if not args:
            return [('id', '=', 0)]
        config = self.pool.get('res.config')
        compt_obj = self.pool.get('giscedata.lectures.comptador')
        len_codi = int(config.get(cursor, uid, 'tg_clean_serial_chars', '5'))

        tg_name = args[0][-1]
        meter_name = tg_name[len_codi:]
        if int(config.get(cursor, uid, 'tg_clean_left_zeroes_serial', '0')):
            meter_name = meter_name.lstrip('0')

        extra_params = compt_obj.get_search_meter_tg_params(
            cursor, uid, tg_name, context)

        return [('serial.name', '=', meter_name)] + extra_params

    def button_update_meter_connectivity_fields(self, cursor, uid, ids,
                                                context=None):
        self.update_meters_connectivity_fields(cursor, uid, ids, context=context)

    def update_meters_connectivity_fields(self, cursor, uid, meter_ids, context=None):
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        meters_names = []
        if not meter_ids:
            profile_meters = get_all_meter_names('tg_profile')
            billing_meters = get_all_meter_names('tg_billing')
            if len(profile_meters) > len(billing_meters):
                _names = profile_meters
            else:
                _names = billing_meters
            for meter_name in _names:
                m_id = meter_obj.search(cursor, uid, [('meter_tg_name', '=', meter_name)])
                if m_id:
                    a_date = meter_obj.read(cursor, uid, m_id[0],
                                            ['data_alta'])['data_alta']
                    meters_names.append((meter_name, m_id, a_date))
        else:
            meters_names = meter_obj.read(cursor, uid, meter_ids,
                                          ['meter_tg_name', 'data_alta'])
            meters_names = [(x['meter_tg_name'], x['id'], x['data_alta'])
                            for x in meters_names]
        date_to = datetime.today()
        date_from = date_to - timedelta(days=30)
        date_from_months = (date_to - relativedelta(months=6)).replace(day=1)
        self._update_profiles_connectivity_field(cursor, uid, meters_names,
                                                 date_from, date_to,
                                                 context=context)
        self._update_daily_billings_connectivity_field(cursor, uid, meters_names,
                                                       date_from, date_to,
                                                       context=context)
        self._update_monthly_billings_connectivity_field(cursor, uid, meters_names,
                                                         date_from_months, date_to,
                                                         context=context)

    def _update_profiles_connectivity_field(self, cursor, uid, meters_names,
                                            date_from, date_to, context=None):
        if not isinstance(meters_names, list):
            meters_names = [meters_names]

        meter_obj = self.pool.get('giscedata.lectures.comptador')
        tg_profile_obj = self.pool.get('tg.profile')
        for meter_name in meters_names:
            if meter_name[2] > date_from.strftime('%Y-%m-%d'):
                date_from = datetime.strptime(
                    '{} 00:00:00'.format(meter_name[2]), '%Y-%m-%d %H:%M:%S')
            max_hours = get_hours_amount(date_from, date_to)
            actual_hours = len(tg_profile_obj.search(cursor, uid, [
                ('name', '=', meter_name[0]),
                ('timestamp', '>=', date_from.strftime('%Y-%m-%d %H:%M:%S')),
                ('timestamp', '<=', date_to.strftime('%Y-%m-%d %H:%M:%S')),
                ('valid', '=', True)]))
            percentage = 100 * actual_hours / max_hours
            meter_obj.write(cursor, uid, meter_name[1], {
                'profiles_meter_connectivity': percentage if percentage <= 100
                else 100})

    def _update_daily_billings_connectivity_field(self, cursor, uid, meters_names,
                                                  date_from, date_to,
                                                  context=None):
        if not isinstance(meters_names, list):
            meters_names = [meters_names]

        meter_obj = self.pool.get('giscedata.lectures.comptador')
        tg_billing_obj = self.pool.get('tg.billing')
        for meter_name in meters_names:
            if meter_name[2] > date_from.strftime('%Y-%m-%d'):
                str_date_from = '{} 01:00:00'.format(meter_name[2])
            else:
                str_date_from = date_from.strftime('%Y-%m-%d %H:%M:%S')
            str_date_to = date_to.strftime('%Y-%m-%d %H:%M:%S')
            max_days = (date_to - datetime.strptime(str_date_from, '%Y-%m-%d %H:%M:%S')).days
            search_params = [('name', '=', meter_name[0]),
                             ('period', '=', 0),
                             ('contract', '=', 1),
                             ('type', '=', 'day'),
                             ('date_begin', '>=', str_date_from),
                             ('date_end', '<=', str_date_to),
                             ('valid', '=', True)]
            actual_days = len(tg_billing_obj.search(cursor, uid,
                                                    search_params))
            daily_percentage = 100 * actual_days / max_days
            meter_obj.write(cursor, uid, meter_name[1], {
                'daily_billings_meter_connectivity': daily_percentage if
                daily_percentage <= 100 else 100,
            })

    def _update_monthly_billings_connectivity_field(self, cursor, uid, meters_names,
                                                    date_from, date_to, context=None):
        if not isinstance(meters_names, list):
            meters_names = [meters_names]

        meter_obj = self.pool.get('giscedata.lectures.comptador')
        tg_billing_obj = self.pool.get('tg.billing')
        for meter_name in meters_names:
            if meter_name[2] > date_from.strftime('%Y-%m-%d'):
                str_date_from = '{} 01:00:00'.format(meter_name[2])
            else:
                str_date_from = date_from.strftime('%Y-%m-%d %H:%M:%S')
            str_date_to = date_to.date().strftime('%Y-%m-%d %H:%M:%S')
            search_params = [('name', '=', meter_name[0]),
                             ('period', '=', 0),
                             ('contract', '=', 1),
                             ('type', '=', 'month'),
                             ('date_begin', '>=', str_date_from),
                             ('date_begin', '<=', str_date_to),
                             ('value', '=', 'a'),
                             ('valid', '=', True)]
            max_months = 6
            actual_months = len(tg_billing_obj.search(cursor, uid,
                                                      search_params))
            monthly_percentage = 100 * actual_months / max_months
            meter_obj.write(cursor, uid, meter_name[1], {
                'monthly_billings_meter_connectivity': monthly_percentage if
                monthly_percentage <= 100 else 100,
            })

    def check_meter_for_tg_data_shortage(self, cursor, uid, meter_id,
                                         report=None, context=None):
        """
        Check if the meter is lacking TG registers from any of the required data
        :param cursor: db cursor
        :param uid: user ID
        :param meter_id: Meter ID
        :param report: list of reports its data is required
        Possible values: S02, S04, S05
        :param context: context param
        :return: Dictionary containing the required reports with a boolean value
        indicating if the meter requires request for it or not.
        Example: {
                      'S02': True,
                      'S04': False,
                      'S05': True
                  }
            So the meter with ID == meter_id requires requests for S02 and S05
            data.
        """
        if not report:
            report = ['S02', 'S04', 'S05']
        result = {}
        for r_name in report:
            result.setdefault(r_name, False)
            func_name = 'check_meter_for_{}_shortage'.format(r_name.lower())
            func = getattr(self, func_name)
            result[r_name] = func(cursor, uid, meter_id, context=context)

        return result

    def check_meter_for_s04_shortage(self, cursor, uid, meter_id, context=None):
        conf_obj = self.pool.get('res.config')
        billing_obj = self.pool.get('tg.billing')

        m_back = int(conf_obj.get(cursor, uid,
                                  'tg_monthly_billings_retry_months', '1'))
        meter = self.read(cursor, uid, meter_id, ['meter_tg_name',
                                                  'tg_last_read'])
        meter_name = meter['meter_tg_name']
        now_date = datetime.now()
        for back in range(m_back+1)[1:]:
            f_date = datetime(now_date.year, now_date.month, 1
                              ) - relativedelta(months=back-1)
            t_date = f_date + relativedelta(days=1)
            billings = billing_obj.search(cursor, uid, [
                ('date_end', '>=', f_date.strftime('%Y-%m-%d')),
                ('date_end', '<', t_date.strftime('%Y-%m-%d')),
                ('name', '=', meter_name),
                ('type', '=', 'month'),
                ('value', '=', 'a')])
            if not billings:
                return False
            else:
                _date = datetime(now_date.year, now_date.month, 1
                                 ).strftime('%Y-%m-%d %H:%M:%S')
                if meter['tg_last_read'] < _date:
                    return False
        return True

    def check_meter_for_s05_shortage(self, cursor, uid, meter_id, context=None):
        conf_obj = self.pool.get('res.config')
        billing_obj = self.pool.get('tg.billing')

        d_back = int(conf_obj.get(cursor, uid,
                                  'tg_daily_billings_retry_days', '1'))
        meter = self.read(cursor, uid, meter_id, ['meter_tg_name',
                                                  'tg_last_read'])
        meter_name = meter['meter_tg_name']
        now_date = datetime.now()
        for back in range(d_back+1)[1:]:
            _date = datetime(now_date.year, now_date.month, now_date.day
                             ) - relativedelta(days=back)
            billings = billing_obj.search(cursor, uid, [
                ('date_begin', '=', _date.strftime('%Y-%m-%d %H:%M:%S')),
                ('date_end', '=', _date.strftime('%Y-%m-%d %H:%M:%S')),
                ('name', '=', meter_name),
                ('type', '=', 'day'),
                ('value', '=', 'a')])
            if not billings:
                return False
            else:
                last_date = datetime.now() - relativedelta(days=1)
                _date = datetime(last_date.year, last_date.month, last_date.day
                                 ).strftime('%Y-%m-%d %H:%M:%S')
                if meter['tg_last_read'] < _date:
                    return False
        return True

    def check_meter_for_s02_shortage(self, cursor, uid, meter_id, context=None):
        conf_obj = self.pool.get('res.config')
        profile_obj = self.pool.get('tg.profile')

        m_back = int(conf_obj.get(cursor, uid,
                                  'tg_hourly_profiles_retry_months', '1'))
        meter = self.read(cursor, uid, meter_id, ['meter_tg_name',
                                                  'tg_last_profile'])
        meter_name = meter['meter_tg_name']
        now_date = datetime.now()
        # Check profiles for current month
        for day in range(now_date.day)[1:]:
            f_date = datetime(now_date.year, now_date.month, day, 1)
            t_date = datetime(now_date.year, now_date.month, day
                              ) + relativedelta(days=1)
            profiles = profile_obj.search(cursor, uid, [
                ('timestamp', '>=', f_date.strftime('%Y-%m-%d %H:%M:%S')),
                ('timestamp', '<=', t_date.strftime('%Y-%m-%d %H:%M:%S')),
                ('name', '=', meter_name)
            ])
            if not profiles:
                return False
        for back in range(m_back)[1:]:
            _date = datetime.now - relativedelta(months=back)
            days = calendar.monthrange(_date.year, _date.month)[1]
            for day in range(days+1)[1:]:
                f_date = datetime(_date.year, _date.month, day, 1)
                t_date = datetime(_date.year, _date.month, day
                                  ) + relativedelta(days=1)
                profiles = profile_obj.search(cursor, uid, [
                    ('timestamp', '>=', f_date.strftime('%Y-%m-%d %H:%M:%S')),
                    ('timestamp', '<=', t_date.strftime('%Y-%m-%d %H:%M:%S')),
                    ('name', '=', meter_name)
                ])
                if not profiles:
                    return False
        last_date = datetime.now() - relativedelta(days=1)
        _date = datetime(last_date.year, last_date.month, last_date.day
                         ).strftime('%Y-%m-%d %H:%M:%S')
        if meter['tg_last_profile'] < _date:
            return False
        return True

    def group_meters_by_best_availability_hours(self, cursor, uid, m_data,
                                                r_date=None, context=None):
        """
        Group meters by their best connectivity hour that is shared with most
        meters. The goal is to get high percentages of success on the requests
        but also do as little requests as possible.
        :param m_data: {
                        'S02': [(meter_id1, tg_name1), (meter_id2, tg_name2)],
                        'S04': []
                       }
        :param r_date: Datetime to use to group meters
        :return: {
                  'S02': {'hour 1': [(meter_id1, tg_name1)],
                          'hour 2': [(meter_id2, tg_name2)],
                          ...,
                  'S04': {}
                 }
        """
        conf_obj = self.pool.get('res.config')
        avail_obj = self.pool.get('tg.meter.availability')
        min_value = int(conf_obj.get(
            cursor, uid, 'tg_min_percentage_quality_availability', 80))
        # Get aggregations of every meter
        faulty_meters = []
        aggregations = {}
        if r_date is None:
            # Defaults calcultate for tomorrow
            r_date = (datetime.now() + relativedelta(days=1)).date()
        if isinstance(r_date, datetime):
            r_date = r_date.date()
        for report, meters in m_data.items():
            for meter in meters:
                if meter not in aggregations.keys():
                    aggr = avail_obj.obtain_meter_availability_for_weekday(
                        cursor, uid, meter[1], r_date, min_value=min_value,
                        last_hour=23, context=context)
                    if not aggr:
                        faulty_meters.append(meter)
                    else:
                        aggregations[meter] = aggr
        # Ensure we have aggr for every meter searching best hour without
        # minimum value. If there is no availability data set to False
        if faulty_meters:
            for meter in faulty_meters:
                aggr = avail_obj.obtain_meter_availability_for_weekday(
                    cursor, uid, meter[1], r_date, last_hour=23, context=context)
                if aggr:
                    best_hour = avail_obj.get_best_conn_hour_from_aggr(aggr)
                    for hour in aggr:
                        if hour['hour'] == best_hour:
                            aggregations[meter] = [hour]
                else:
                    aggregations[meter] = False
        # Get best hours and list of meters for each
        result = {}
        for report, meters in m_data.items():
            result[report] = {i: [] for i in range(0, 24)}
            for meter in meters:
                if not aggregations.get(meter, False):
                    continue
                if len(aggregations[meter]) == 1:  # Faulty meter?
                    result[report][aggregations[meter][0]['hour']].append(meter)
                else:
                    h_ = avail_obj.get_best_conn_hour_with_continuity_from_aggr(
                        aggregations[meter])
                    result[report][h_].append(meter)
                    # for hour in aggregations[meter]:
                    #     result[report][hour['hour']].append(meter)
            # Sort hours by most meters
            by_amount = sorted([(hour, amount) for hour, amount in
                                result[report].items()], key=lambda k: k[1],
                               reverse=True)
            # Eliminate repeated meters from all hours
            pos = 0
            for hour in by_amount:
                pos += 1
                for seconds in by_amount[pos:]:
                    result[report][seconds[0]] = list(set(result[report][seconds[0]])
                                                - set(result[report][hour[0]]))
        return result

    _columns = {
        'tg': fields.boolean('TG meter'),
        'tg_icp_active': fields.boolean('ICP active', required=False),
        'tg_icp_potencia': fields.float('Programmed power',
                                         digits=(16, 3)),
        'tg_icp_intensitat': fields.function(_get_intensitat_tel,
                                        method=True, type='float',
                                        digits=(16, 2), string='Intensity',
                                        readonly=True,
                        store={'giscedata.lectures.comptador':
                               (lambda self, cursor, uid, ids, c=None: ids,
                                ['tg_icp_potencia', 'tg_icp_active'], 20),
                               'giscedata.polissa':
                               (_get_comptador_from_polissa,
                                ['tensio_normalitzada'], 20),
                              },),
        'tg_last_read': fields.date('Last read'),
        'tg_last_profile': fields.datetime('Last profile'),
        'tg_cnc_id': fields.many2one('tg.concentrator', 'Concentrator'),
        'tg_cnc_conn': fields.function(_get_cnc_conn,
                                        method=True, type='boolean',
                                        string='Cnc Connected',
                                        readonly=True,
                        store={'giscedata.lectures.comptador':
                               (lambda self, cursor, uid, ids, c=None: ids,
                                ['tg_cnc_id'], 20),
                              },),
        'tg_meter_exception': fields.function(_get_multi_meter_info,
                                              method=True, type='boolean',
                                              string='Meter exception',
                                              readonly=True, multi='tg_name'),
        'tg_exception_ids': fields.many2many('tg.validate.exception',
                                             'tg_validate_exception_meter',
                                             'meter_id', 'exception_id',
                                             'Exceptions'),
        'policy_remote_managed': fields.related(
            'polissa',
            'tg',
            readonly=True,
            type='selection',
            selection=TG_OPERATIVA,
            string='Pòlissa Telegestionada'
        ),
        'policy_marketer': fields.related(
            'polissa',
            'comercialitzadora',
            readonly=True,
            type='many2one',
            relation='res.partner',
            string='Marketer'
        ),
        'meter_tg_name': fields.function(_get_multi_meter_info, method=True,
                                         type='char', size=25, multi='tg_name',
                                         string='Nº de sèrie TG',
                                         fnct_search=_search_meter_tg
                                         ),
        'request_id': fields.char('IdReq', size=10, readonly=True),
        'version': fields.char('Version', size=10, readonly=True),
        'concentrator': fields.char('Cnc', size=20, readonly=True),
        'meter': fields.char('Meter', size=20, readonly=True),
        'timestamp': fields.datetime('Date of update', readonly=True),
        'season': fields.char('Season', size=1, readonly=True),
        'serial_number': fields.char('Serial', size=20, readonly=True),
        'manufacturer': fields.char('Manufacturer', size=100, readonly=True),
        'model_type': fields.char('Model', size=20, readonly=True),
        'manufacturing_year': fields.integer('Manufacture year', readonly=True),
        'equipment_type': fields.selection([('contador', 'Meter'),
                                            ('supervisor', 'Supervisor'),
                                            ('Tipo4MDPLC', 'contador Tipo 4'),
                                           ], 'Equipment type',
                                           readonly=True),
        'firmware_version': fields.char('Firmware', size=20, readonly=True),
        'prime_firmware_version': fields.char('Firmware PRIME', size=20, readonly=True),
        'protocol': fields.char('Protocol', size=20, readonly=True),
        'id_multicast': fields.char('Id multicast', size=20, readonly=True),
        'mac': fields.char('MAC address', size=20, readonly=True),
        'primary_voltage': fields.integer('Primary voltage', readonly=True),
        'secondary_voltage': fields.integer('Secondary voltage', readonly=True),
        'primary_current': fields.integer('Primary current', readonly=True),
        'secondary_current': fields.integer('Secondary current', readonly=True),
        'time_threshold_voltage_sags': fields.integer('Threshold voltage sags', readonly=True),
        'time_threshold_voltage_swells': fields.integer('Threshold voltage '
                                                        'swells', readonly=True),
        'load_profile_period': fields.integer('Load profile period', readonly=True),
        'demand_close_contracted_power': fields.float('Demand close to '
                                                        'contracted power', readonly=True),
        'reference_voltage': fields.integer('Reference voltage', readonly=True),
        'long_power_failure_threshold': fields.integer('Long power failure '
                                                       'threshold', readonly=True),
        'voltage_sag_threshold': fields.float('Voltage sag threshold', readonly=True),
        'voltage_swell_threshold': fields.float('Voltage swell threshold', readonly=True),
        'voltage_cut-off_threshold': fields.float('Voltage cut-off '
                                                    'threshold', readonly=True),
        'automatic_monthly_billing': fields.boolean('Automatic monthly '
                                                    'billing', readonly=True),
        'scroll_display_mode': fields.char('Scroll display mode', size=10, readonly=True),
        'time_scroll_display': fields.integer('Time scroll display', readonly=True),
        'profiles_meter_connectivity': fields.integer('Profiles received %', readonly=True, help='Percentage of profiles received on the las 30 days over the maximum amount possible.'),
        'monthly_billings_meter_connectivity': fields.integer('Monthly billings received %', readonly=True, help='Percentage of billings received on the last 6 months over the maximum amount possible'),
        'daily_billings_meter_connectivity': fields.integer('Daily billings received %', readonly=True, help='Percentage of billings received on the last 30 days over the maximum amount possible'),
        'last_known_connection_status': fields.selection([
            (2, 'Active'), (1, 'Temporary failure'),
            (0, 'Permanent failure')], 'Connection status', readonly=True),
        'last_connection_status_update': fields.datetime('Last connection '
                                                         'status update',
                                                         readonly=True),
        'request_ids': fields.many2many('tg.stg.request.register',
                                        'stg_requests_meters_rel', 'meter_id',
                                        'request_id', 'STG requests'),
        'summary_ids': fields.many2many('tg.stg.request.summary',
                                        'stg_summary_meters_rel', 'meter_id',
                                        'summary_id', 'STG request summaries'),
    }

    def onchange_serial(self, cursor, uid, ids, name):
        value = {}
        if not name:
            return value
        # stock_production_lot
        lot_obj = self.pool.get('stock.production.lot')
        id_serial = lot_obj.search(cursor, uid, [('name', '=', name)])
        if id_serial:
            value['serial'] = id_serial[0]
        else:
            value['serial'] = False
        return {'value': value}

    _defaults = {
        'tg': lambda *a: False,
        'tg_icp_active': lambda *a: False,
        'tg_icp_potencia': _get_pot_from_polissa,
    }

TgLecturesComptador()


class TgLecturesConcentrator(osv.osv):

    _name = 'tg.concentrator'
    _inherit = 'tg.concentrator'

    def check_meters_for_tg_data_shortage(self, cursor, uid, cnc_id, report=None
                                          , context=None):
        """
        Check which meters are lacking required TG registers.
        :param cursor: db cursor
        :param uid: user ID
        :param cnc_id: TG concentrator ID
        :param report: list of reports its data is required
        Possible values: S02, S04, S05
        :param context: context param
        :return: Dictionary containing the required reports with a list of
        meters for every report.
        Example:
            {
                'S02': [(meter_id_1, tg_name_1), (meter_id_2, tg_name_2)],
                'S04': [(meter_id_2, tg_name_2), (meter_id_3, tg_name_3)]
            }
          So meter_2 requires data for S02 and S04 reports and the meters 1 only
          needs S02 data.
        """
        if not report:
            report = ['S02', 'S04', 'S05']
        if isinstance(cnc_id, (list, tuple)):
            cnc_id = cnc_id[0]
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        to_request = {}
        logger = netsvc.Logger()

        for r_name in report:
            to_request.setdefault(r_name, [])
        cnc_read = self.read(cursor, uid, cnc_id, ['name', 'meter_ids'])

        logger.notifyChannel("TG ReadsRetry", netsvc.LOG_INFO,
                             "Check meters data shortage for CNC: {}".format(
                                 cnc_read['name']))
        self.read_tg_pending_files_for_cnc(cursor, uid, cnc_id, report=report,
                                           context=context)
        m_names = meter_obj.read(cursor, uid, cnc_read['meter_ids'],
                                 ['meter_tg_name'])
        meter_list = [(m['id'], m['meter_tg_name']) for m in m_names]
        for meter in meter_list:
            res = meter_obj.check_meter_for_tg_data_shortage(
                cursor, uid, meter[0], report=report, context=context)
            for r_name in to_request.keys():
                if not res.get(r_name, False):
                    to_request[r_name].append(meter)

        return to_request

    _columns = {
        'meter_ids': fields.one2many('giscedata.lectures.comptador',
                                     'tg_cnc_id', 'Meters'),
        'request_ids': fields.one2many('tg.stg.request.register',
                                       'request_id', 'STG requests'),
    }

    def get_meters_connection_status(self, cursor, uid, name, context=None):
        meter_obj = self.pool.get('giscedata.lectures.comptador')

        cnc_id = self.search(cursor, uid, [('name', '=', name)])
        if cnc_id:
            meter_ids = self.read(cursor, uid, cnc_id, ['meter_ids']
                                  )[0]['meter_ids']
            actives = 0
            tfs = 0
            pfs = 0
            for meter in meter_obj.read(cursor, uid, meter_ids, ['last_known_connection_status']):
                status = meter['last_known_connection_status']
                if status == 2:
                    actives += 1
                elif status == 1:
                    tfs += 1
                else:
                    pfs += 1
            self.write(cursor, uid, cnc_id, {
                'total_meters_active': actives,
                'total_meters_temporary_failure': tfs,
                'total_meters_permanent_failure': pfs,
            })

TgLecturesConcentrator()
