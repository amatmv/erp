# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _


class TgReader(osv.osv):
    _name = 'tg.reader'
    _inherit = 'tg.reader'

    def insert_values_S06(self, cursor, uid, meter, version, context=None):

        meter_obj = self.pool.get('giscedata.lectures.comptador')
        meter_reg = meter_obj.search(cursor, uid,
                                     [('meter_tg_name', '=', meter.name)])
        if meter_reg:
            for value in meter.values:
                if 'equipment_type' in value:
                    value['equipment_type'] = value['equipment_type'].rstrip()
                if value.get('equipment_type') != 'supervisor':
                    try:
                        meter_obj.write(cursor, uid, meter_reg, value,
                                        context=context)
                    except Exception as e:
                        msg = 'ERROR writing values for meter {}: {}'.format(
                            value.get('meter'), e)
                        meter.warnings = msg

TgReader()
