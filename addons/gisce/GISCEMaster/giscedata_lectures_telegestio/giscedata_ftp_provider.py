# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscedataFTPProvider(osv.osv):

    _inherit = 'giscedata.ftp.provider'

    _columns = {
        'p5d_enabled': fields.boolean('Enabled for P5D'),
        'p5d_upload': fields.char(
            "P5D Upload folder",
            size=256,
        )
    }

    _defaults = {
        'p5d_enabled': lambda *a: False,
        'p5d_upload': lambda *a: '/upload/{comer}/Salida',
    }


GiscedataFTPProvider()
