# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from expects import *
from datetime import datetime
from addons import get_module_resource
from primestg.report import Report as primeReport


class TestCalculateConnectivityValues(testing.OOTestCase):

    def setUp(self):
        self.pool = self.openerp.pool

        s02_24_path = get_module_resource(
            'giscedata_lectures_telegestio', 'tests', 'data', 'S02.xml'
        )
        self.s02_24_xml = open(s02_24_path, 'r')

        s05_path = get_module_resource(
            'giscedata_lectures_telegestio', 'tests', 'data', 'S05.xml'
        )
        self.s05_xml = open(s05_path, 'r')

        s04_path = get_module_resource(
            'giscedata_lectures_telegestio', 'tests', 'data', 'S04.xml'
        )
        self.s04_xml = open(s04_path, 'r')

    def tearDown(self):
        self.s02_24_xml.close()
        self.s05_xml.close()
        self.s04_xml.close()
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            self.unlink_tg_models(cursor, uid)

    def unlink_tg_models(self, cursor, uid):
        for model_name in ['tg.billing', 'tg.profile']:
            cch_obj = self.pool.get(model_name)
            del_ids = cch_obj.search(cursor, uid, [('name', 'in', ['ZIV42553686', 'ZIV36301516'])])
            cch_obj.unlink(cursor, uid, del_ids)

    def test_profiles_connectivity_calculation(self):
            tg_reader_obj = self.pool.get('tg.reader')
            meter_obj = self.pool.get('giscedata.lectures.comptador')
            profile_obj = self.pool.get('tg.profile')
            tg_xml_24 = primeReport(self.s02_24_xml)
            with Transaction().start(self.database) as txn:
                cursor = txn.cursor
                uid = txn.user
                tg_reader_obj.insert_primestg(cursor, uid, tg_xml_24)

                meter_id = meter_obj.search(cursor, uid, [('name', '=', '42553686')])
                profiles = profile_obj.search(cursor, uid, [])
                profile_obj.write(cursor, uid, profiles, {'valid': True})
                field = meter_obj.read(cursor, uid, meter_id[0], ['profiles_meter_connectivity', 'data_alta'])
                expect(field['profiles_meter_connectivity']).to(equal(False))
                meter_obj._update_profiles_connectivity_field(cursor, uid, ('ZIV42553686', meter_id, field['data_alta']),
                                                              datetime(2017, 2, 1),
                                                              datetime(2017, 2, 3))
                field = meter_obj.read(cursor, uid, meter_id[0], ['profiles_meter_connectivity', 'data_alta'])
                expect(field['profiles_meter_connectivity']).to(equal(50))

                meter_obj._update_profiles_connectivity_field(cursor, uid, ('ZIV42553686', meter_id, field['data_alta']),
                                                              datetime(2017, 2, 1),
                                                              datetime(2017, 2, 5))
                field = meter_obj.read(cursor, uid, meter_id[0], ['profiles_meter_connectivity'])
                expect(field['profiles_meter_connectivity']).to(equal(25))

    def test_billings_connectivity_calculation(self):
            tg_reader_obj = self.pool.get('tg.reader')
            meter_obj = self.pool.get('giscedata.lectures.comptador')
            billing_obj = self.pool.get('tg.billing')
            tg_xml_05 = primeReport(self.s05_xml)
            tg_xml_04 = primeReport(self.s04_xml)
            with Transaction().start(self.database) as txn:
                cursor = txn.cursor
                uid = txn.user
                tg_reader_obj.insert_primestg(cursor, uid, tg_xml_05)
                tg_reader_obj.insert_primestg(cursor, uid, tg_xml_04)

                meter_id = meter_obj.search(cursor, uid, [('name', '=', '36301516')])
                billings = billing_obj.search(cursor, uid, [])
                billing_obj.write(cursor, uid, billings, {'valid': True})
                field = meter_obj.read(cursor, uid, meter_id[0], [
                    'daily_billings_meter_connectivity',
                    'monthly_billings_meter_connectivity', 'data_alta'])
                expect(field['daily_billings_meter_connectivity']).to(equal(False))
                expect(field['monthly_billings_meter_connectivity']).to(equal(False))
                meter_obj._update_daily_billings_connectivity_field(cursor, uid, ('ZIV36301516', meter_id, field['data_alta']), datetime(2015, 11, 1), datetime(2015, 12, 1))
                meter_obj._update_monthly_billings_connectivity_field(cursor,
                          uid, ('ZIV36301516', meter_id, field['data_alta']), datetime(2016, 10, 1), datetime(2017, 4, 1))
                field = meter_obj.read(cursor, uid, meter_id[0], [
                    'daily_billings_meter_connectivity',
                    'monthly_billings_meter_connectivity'])
                expect(field['daily_billings_meter_connectivity']).to(equal(6))
                expect(field['monthly_billings_meter_connectivity']).to(equal(16))
