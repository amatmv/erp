# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from destral.patch import PatchNewCursors
from expects import *
from giscedata_polissa.tests.utils import crear_modcon, activar_polissa
from datetime import datetime, timedelta
from addons import get_module_resource
from primestg.report import Report as primeReport
import os


class TestLoadOfReadsFromTG(testing.OOTestCase):

    def setUp(self):
        self.pool = self.openerp.pool
        self.imd_obj = self.pool.get('ir.model.data')
        self.lect_obj = self.pool.get('giscedata.lectures.lectura')
        self.wz_create_read = self.pool.get('wizard.create.read.tg')
        self.tg_billing_obj = self.pool.get('tg.billing')
        self.polissa_obj = self.pool.get('giscedata.polissa')
        self.modcon_obj = self.pool.get('giscedata.polissa.modcontractual')
        self.config = self.pool.get('res.config')
        self.origin_obj = self.pool.get('giscedata.lectures.origen')
        self.potencia_obj = self.pool.get('giscedata.lectures.potencia')
        self.res_config = self.pool.get('res.config')
        self.oorq = os.environ.get('OORQ_ASYNC', 'True')
        os.environ['OORQ_ASYNC'] = 'False'
        s04_path = get_module_resource(
            'giscedata_lectures_telegestio', 'tests', 'data', 'S04.xml'
        )
        self.s04_xml = open(s04_path, 'r')
        s05_dataalta_path = get_module_resource(
            'giscedata_lectures_telegestio', 'tests', 'data',
            'S05_data_alta.xml'
        )
        self.s05_dataalta_xml = open(s05_dataalta_path, 'r')

    def tearDown(self):
        os.environ['OORQ_ASYNC'] = self.oorq

    def test_normal_day_load(self):

        with Transaction().start(self.database) as txn:

            uid = txn.user
            cursor = txn.cursor

            with PatchNewCursors():
                polissa_id = self.imd_obj.get_object_reference(
                    cursor, uid, 'giscedata_polissa', 'polissa_0004'
                )[1]
                origin_id = self.origin_obj.search(cursor, uid,
                                                   [('codi', '=', '60')])[0]
                activar_polissa(self.pool, cursor, uid, polissa_id)
                self.create_config_vars(cursor, uid)
                ctx = {'lang': 'en_US'}
                wz_id = self.wz_create_read.create(cursor, uid, {}, ctx)
                wiz = self.wz_create_read.browse(cursor, uid, wz_id, ctx)
                # Dia normal
                wiz.write({
                    'meter_id': 3,
                    'date_limit': '2015-11-12',
                    'force': False
                })
                self.wz_create_read.action_create_read(cursor, uid, [wz_id],
                                                       ctx)
                reads = self.get_created_reads(cursor, uid, wiz, '2015-11-01')
                expect(reads[0]['lectura']).to(equal(105012))
                expect(reads[0]['tipus']).to(equal('A'))
                expect(reads[0]['name']).to(equal('2015-11-12'))
                expect(reads[0]['origen_id'][0]).to(equal(origin_id))
                expect(reads[1]['lectura']).to(equal(130))
                expect(reads[1]['tipus']).to(equal('R'))
                expect(reads[1]['name']).to(equal('2015-11-12'))
                expect(reads[1]['origen_id'][0]).to(equal(origin_id))
                self.delete_created_reads(cursor, uid, wiz, '2015-11-01')
                # Using wizard force option
                wiz.write({
                    'force': True
                })
                self.wz_create_read.action_create_read(cursor, uid, [wz_id],
                                                       ctx)
                reads = self.get_created_reads(cursor, uid, wiz, '2015-11-01')
                expect(reads[0]['lectura']).to(equal(105012))
                expect(reads[0]['tipus']).to(equal('A'))
                expect(reads[0]['name']).to(equal('2015-11-12'))
                expect(reads[1]['lectura']).to(equal(130))
                expect(reads[1]['tipus']).to(equal('R'))
                expect(reads[1]['name']).to(equal('2015-11-12'))

    def test_load_reactive_check_reactive_config(self):

        with Transaction().start(self.database) as txn:

            uid = txn.user
            cursor = txn.cursor
            self.config.set(cursor, uid, 'tg_reactive_insert', 'zero')
            with PatchNewCursors():
                polissa_id = self.imd_obj.get_object_reference(
                    cursor, uid, 'giscedata_polissa', 'polissa_0004'
                )[1]
                self.polissa_obj.write(
                    cursor, uid, polissa_id, {'reactive_conf': 'always'}
                )
                origin_id = self.origin_obj.search(cursor, uid,
                                                   [('codi', '=', '60')])[0]
                activar_polissa(self.pool, cursor, uid, polissa_id)
                self.create_config_vars(cursor, uid)
                ctx = {'lang': 'en_US'}
                wz_id = self.wz_create_read.create(cursor, uid, {}, ctx)
                wiz = self.wz_create_read.browse(cursor, uid, wz_id, ctx)
                # Dia normal
                wiz.write({
                    'meter_id': 3,
                    'date_limit': '2015-11-12',
                    'force': False
                })
                self.wz_create_read.action_create_read(cursor, uid, [wz_id],
                                                       ctx)
                reads = self.get_created_reads(cursor, uid, wiz, '2015-11-01')
                expect(reads[0]['lectura']).to(equal(105012))
                expect(reads[0]['tipus']).to(equal('A'))
                expect(reads[0]['name']).to(equal('2015-11-12'))
                expect(reads[0]['origen_id'][0]).to(equal(origin_id))
                expect(reads[1]['lectura']).to(equal(130))
                expect(reads[1]['tipus']).to(equal('R'))
                expect(reads[1]['name']).to(equal('2015-11-12'))
                expect(reads[1]['origen_id'][0]).to(equal(origin_id))
                self.delete_created_reads(cursor, uid, wiz, '2015-11-01')
                # Using wizard force option
                self.polissa_obj.write(
                    cursor, uid, polissa_id, {'reactive_conf': 'global'}
                )
                wiz.write({
                    'force': True
                })
                self.wz_create_read.action_create_read(cursor, uid, [wz_id],
                                                       ctx)
                reads = self.get_created_reads(cursor, uid, wiz, '2015-11-01')
                expect(reads[0]['lectura']).to(equal(105012))
                expect(reads[0]['tipus']).to(equal('A'))
                expect(reads[0]['name']).to(equal('2015-11-12'))
                expect(reads[1]['lectura']).to(equal(0))
                expect(reads[1]['tipus']).to(equal('R'))
                expect(reads[1]['name']).to(equal('2015-11-12'))

    def test_tariff_change_day_load(self):

        with Transaction().start(self.database) as txn:

            uid = txn.user
            cursor = txn.cursor

            with PatchNewCursors():
                polissa_id = self.imd_obj.get_object_reference(
                    cursor, uid, 'giscedata_polissa', 'polissa_0004'
                )[1]
                activar_polissa(self.pool, cursor, uid, polissa_id)
                self.create_config_vars(cursor, uid)
                ctx = {'lang': 'en_US'}
                wz_id = self.wz_create_read.create(cursor, uid, {}, ctx)
                wiz = self.wz_create_read.browse(cursor, uid, wz_id, ctx)

                # Dia canvi tarifa
                amendment = {
                    'tarifa': 3,
                }
                crear_modcon(self.pool, cursor, uid, polissa_id, amendment,
                             '2015-11-15', '2021-01-01')
                wiz.write({
                    'meter_id': 3,
                    'date_limit': '2015-11-14',
                    'force': False
                })
                self.wz_create_read.action_create_read(cursor, uid, [wz_id],
                                                       ctx)
                reads = self.get_created_reads(cursor, uid, wiz, '2015-11-01')
                expect(reads[0]['lectura']).to(equal(105014))
                expect(reads[0]['tipus']).to(equal('A'))
                expect(reads[0]['name']).to(equal('2015-11-14'))
                expect(reads[0]['periode'][1]).to(equal('2.0A (P1)'))
                expect(reads[1]['lectura']).to(equal(150))
                expect(reads[1]['tipus']).to(equal('R'))
                expect(reads[1]['name']).to(equal('2015-11-14'))
                expect(reads[1]['periode'][1]).to(equal('2.0A (P1)'))
                expect(reads[2]['lectura']).to(equal(9014))
                expect(reads[2]['tipus']).to(equal('A'))
                expect(reads[2]['name']).to(equal('2015-11-14'))
                expect(reads[2]['periode'][1]).to(equal('2.0DHA (P1)'))
                expect(reads[3]['lectura']).to(equal(150))
                expect(reads[3]['tipus']).to(equal('R'))
                expect(reads[3]['name']).to(equal('2015-11-14'))
                expect(reads[3]['periode'][1]).to(equal('2.0DHA (P1)'))
                expect(reads[4]['lectura']).to(equal(25014))
                expect(reads[4]['tipus']).to(equal('A'))
                expect(reads[4]['name']).to(equal('2015-11-14'))
                expect(reads[4]['periode'][1]).to(equal('2.0DHA (P2)'))
                expect(reads[5]['lectura']).to(equal(150))
                expect(reads[5]['tipus']).to(equal('R'))
                expect(reads[5]['name']).to(equal('2015-11-14'))
                expect(reads[5]['periode'][1]).to(equal('2.0DHA (P2)'))
                self.delete_created_reads(cursor, uid, wiz, '2015-11-01')
                # Using wizard force option
                wiz.write({
                    'force': True
                })
                self.wz_create_read.action_create_read(cursor, uid, [wz_id],
                                                       ctx)
                reads = self.get_created_reads(cursor, uid, wiz, '2015-11-01')
                expect(reads[0]['lectura']).to(equal(105014))
                expect(reads[0]['tipus']).to(equal('A'))
                expect(reads[0]['name']).to(equal('2015-11-14'))
                expect(reads[0]['periode'][1]).to(equal('2.0A (P1)'))
                expect(reads[1]['lectura']).to(equal(150))
                expect(reads[1]['tipus']).to(equal('R'))
                expect(reads[1]['name']).to(equal('2015-11-14'))
                expect(reads[1]['periode'][1]).to(equal('2.0A (P1)'))
                expect(reads[2]['lectura']).to(equal(9014))
                expect(reads[2]['tipus']).to(equal('A'))
                expect(reads[2]['name']).to(equal('2015-11-14'))
                expect(reads[2]['periode'][1]).to(equal('2.0DHA (P1)'))
                expect(reads[3]['lectura']).to(equal(150))
                expect(reads[3]['tipus']).to(equal('R'))
                expect(reads[3]['name']).to(equal('2015-11-14'))
                expect(reads[3]['periode'][1]).to(equal('2.0DHA (P1)'))
                expect(reads[4]['lectura']).to(equal(25014))
                expect(reads[4]['tipus']).to(equal('A'))
                expect(reads[4]['name']).to(equal('2015-11-14'))
                expect(reads[4]['periode'][1]).to(equal('2.0DHA (P2)'))
                expect(reads[5]['lectura']).to(equal(150))
                expect(reads[5]['tipus']).to(equal('R'))
                expect(reads[5]['name']).to(equal('2015-11-14'))
                expect(reads[5]['periode'][1]).to(equal('2.0DHA (P2)'))

    def test_tariff_change_load_after_day(self):

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor

            with PatchNewCursors():
                polissa_id = self.imd_obj.get_object_reference(
                    cursor, uid, 'giscedata_polissa', 'polissa_0004'
                )[1]
                activar_polissa(self.pool, cursor, uid, polissa_id)
                self.create_config_vars(cursor, uid)
                self.validate_reads_from_date(cursor, uid, '2015-11-01')
                ctx = {'lang': 'en_US'}
                wz_id = self.wz_create_read.create(cursor, uid, {},
                                                   ctx)
                wiz = self.wz_create_read.browse(cursor, uid, wz_id,
                                                 ctx)
                # Dia cFanvi tarifa
                amendment = {
                    'tarifa': 3,
                }
                crear_modcon(self.pool, cursor, uid, polissa_id, amendment,
                             '2015-11-15', '2021-01-01')
                wiz.write({
                    'meter_id': 3,
                    'date_limit': '2015-11-15',
                    'force': False
                })
                self.wz_create_read.action_create_read(cursor, uid, [wz_id],
                                                       ctx)
                reads = self.get_created_reads(cursor, uid, wiz, '2015-11-01')
                expect(reads[0]['lectura']).to(equal(9015))
                expect(reads[0]['tipus']).to(equal('A'))
                expect(reads[0]['name']).to(equal('2015-11-15'))
                expect(reads[0]['periode'][1]).to(equal('2.0DHA (P1)'))
                expect(reads[1]['lectura']).to(equal(160))
                expect(reads[1]['tipus']).to(equal('R'))
                expect(reads[1]['name']).to(equal('2015-11-15'))
                expect(reads[1]['periode'][1]).to(equal('2.0DHA (P1)'))
                expect(reads[2]['lectura']).to(equal(25015))
                expect(reads[2]['tipus']).to(equal('A'))
                expect(reads[2]['name']).to(equal('2015-11-15'))
                expect(reads[2]['periode'][1]).to(equal('2.0DHA (P2)'))
                expect(reads[3]['lectura']).to(equal(160))
                expect(reads[3]['tipus']).to(equal('R'))
                expect(reads[3]['name']).to(equal('2015-11-15'))
                expect(reads[3]['periode'][1]).to(equal('2.0DHA (P2)'))
                self.delete_created_reads(cursor, uid, wiz, '2015-11-01')
                # Using wizard force option
                wiz.write({
                    'force': True
                })
                self.wz_create_read.action_create_read(cursor, uid, [wz_id],
                                                       ctx)
                reads = self.get_created_reads(cursor, uid, wiz, '2015-11-01')
                expect(reads[0]['lectura']).to(equal(9015))
                expect(reads[0]['tipus']).to(equal('A'))
                expect(reads[0]['name']).to(equal('2015-11-15'))
                expect(reads[0]['periode'][1]).to(equal('2.0DHA (P1)'))
                expect(reads[1]['lectura']).to(equal(160))
                expect(reads[1]['tipus']).to(equal('R'))
                expect(reads[1]['name']).to(equal('2015-11-15'))
                expect(reads[1]['periode'][1]).to(equal('2.0DHA (P1)'))
                expect(reads[2]['lectura']).to(equal(25015))
                expect(reads[2]['tipus']).to(equal('A'))
                expect(reads[2]['name']).to(equal('2015-11-15'))
                expect(reads[2]['periode'][1]).to(equal('2.0DHA (P2)'))
                expect(reads[3]['lectura']).to(equal(160))
                expect(reads[3]['tipus']).to(equal('R'))
                expect(reads[3]['name']).to(equal('2015-11-15'))
                expect(reads[3]['periode'][1]).to(equal('2.0DHA (P2)'))

    def test_load_last_day_not_enough_tgreads(self):

        with Transaction().start(self.database) as txn:

            uid = txn.user
            cursor = txn.cursor

            with PatchNewCursors():
                polissa_id = self.imd_obj.get_object_reference(
                    cursor, uid, 'giscedata_polissa', 'polissa_0004'
                )[1]
                activar_polissa(self.pool, cursor, uid, polissa_id)
                self.create_config_vars(cursor, uid)
                self.unvalidate_monthly_reads(cursor, uid)
                self.unvalidate_reads_from_date(cursor, uid, '2015-11-27 '
                                                             '00:00:00')
                ctx = {'lang': 'en_US'}
                wz_id = self.wz_create_read.create(cursor, uid, {}, ctx)
                wiz = self.wz_create_read.browse(cursor, uid, wz_id, ctx)
                # Ultim dia mes
                wiz.write({
                    'meter_id': 3,
                    'date_limit': '2015-11-30',
                    'force': False
                })
                self.wz_create_read.action_create_read(cursor, uid, [wz_id],
                                                       ctx)
                reads = self.get_created_reads(cursor, uid, wiz, '2015-11-01')
                # Expect alreay existing read from last month
                expect(reads).to(equal([]))
                self.delete_created_reads(cursor, uid, wiz, '2015-11-01')
                # Using wizard force option
                wiz.write({
                    'force': True
                })
                self.wz_create_read.action_create_read(cursor, uid, [wz_id],
                                                       ctx)
                expect(reads).to(equal([]))
                self.delete_created_reads(cursor, uid, wiz, '2015-11-01')
                self.validate_monthly_reads(cursor, uid)
                self.validate_reads_from_date(cursor, uid, '2015-11-27 '
                                                           '00:00:00')

    def test_load_last_day_contract_not_tg(self):
        with Transaction().start(self.database) as txn:

            uid = txn.user
            cursor = txn.cursor

            with PatchNewCursors():
                polissa_id = self.imd_obj.get_object_reference(
                    cursor, uid, 'giscedata_polissa', 'polissa_0004'
                )[1]
                activar_polissa(self.pool, cursor, uid, polissa_id)
                self.create_config_vars(cursor, uid)
                days_back = int(
                    self.config.get(cursor, uid, 'tg_max_days_back', 3) - 1
                )
                self.validate_reads_from_date(cursor, uid, '2015-11-01')
                self.unvalidate_monthly_reads(cursor, uid)
                date_back = '2015-11-{} 00:00:00'.format(str(30 - days_back))
                self.unvalidate_reads_from_date(cursor, uid, date_back)
                amendment = {
                    'tg': '2',
                }
                modcon_act_id = self.polissa_obj.read(
                    cursor, uid, [polissa_id], ['modcontractual_activa'], {}
                )
                modcon_act_id = modcon_act_id[0]['modcontractual_activa'][0]
                modcon_inici = self.modcon_obj.read(cursor, uid,
                                                    modcon_act_id,
                                                    ['data_inici'],
                                                    {})
                new_start = datetime.strftime(datetime.strptime(
                    modcon_inici['data_inici'], '%Y-%m-%d') + timedelta(
                    days=1), '%Y-%m-%d')
                crear_modcon(self.pool, cursor, uid, polissa_id, amendment,
                             new_start, '2021-01-01')

                ctx = {'lang': 'en_US'}
                wz_id = self.wz_create_read.create(cursor, uid, {}, ctx)
                wiz = self.wz_create_read.browse(cursor, uid, wz_id, ctx)
                # Ultim dia mes
                wiz.write({
                    'meter_id': 3,
                    'date_limit': '2015-11-30',
                    'force': False
                })
                self.wz_create_read.action_create_read(cursor, uid, [wz_id],
                                                       ctx)
                reads = self.get_created_reads(cursor, uid, wiz, '2015-11-01')
                expect(reads[0]['lectura']).to(equal(105027))
                expect(reads[0]['tipus']).to(equal('A'))
                expect(reads[0]['name']).to(equal('2015-11-26'))
                expect(reads[0]['periode'][1]).to(equal('2.0A (P1)'))
                expect(reads[1]['lectura']).to(equal(270))
                expect(reads[1]['tipus']).to(equal('R'))
                expect(reads[1]['name']).to(equal('2015-11-26'))
                expect(reads[1]['periode'][1]).to(equal('2.0A (P1)'))

                # Not enough reads available
                self.delete_created_reads(cursor, uid, wiz, '2015-11-01')
                days_back += 1
                date_back = '2015-11-{} 00:00:00'.format(str(30 - days_back))
                self.unvalidate_reads_from_date(cursor, uid, date_back)
                self.wz_create_read.action_create_read(cursor, uid, [wz_id],
                                                       ctx)
                reads = self.get_created_reads(cursor, uid, wiz, '2015-11-01')
                expect(reads).to(equal([]))

                self.validate_monthly_reads(cursor, uid)
                self.validate_reads_from_date(cursor, uid, date_back)

    def test_load_last_day_without_monthly_reads(self):

        with Transaction().start(self.database) as txn:

            uid = txn.user
            cursor = txn.cursor

            with PatchNewCursors():
                polissa_id = self.imd_obj.get_object_reference(
                    cursor, uid, 'giscedata_polissa', 'polissa_0004'
                )[1]
                activar_polissa(self.pool, cursor, uid, polissa_id)
                self.create_config_vars(cursor, uid)
                self.validate_reads_from_date(cursor, uid, '2015-11-01')
                self.unvalidate_monthly_reads(cursor, uid)
                ctx = {'lang': 'en_US'}
                wz_id = self.wz_create_read.create(cursor, uid, {}, ctx)
                wiz = self.wz_create_read.browse(cursor, uid, wz_id, ctx)
                # Ultim dia mes
                wiz.write({
                    'meter_id': 3,
                    'date_limit': '2015-11-30',
                    'force': False
                })
                self.wz_create_read.action_create_read(cursor, uid, [wz_id],
                                                       ctx)
                reads = self.get_created_reads(cursor, uid, wiz, '2015-11-01')
                expect(reads[0]['lectura']).to(equal(105031))
                expect(reads[0]['tipus']).to(equal('A'))
                expect(reads[0]['name']).to(equal('2015-11-30'))
                expect(reads[0]['periode'][1]).to(equal('2.0A (P1)'))
                expect(reads[1]['lectura']).to(equal(310))
                expect(reads[1]['tipus']).to(equal('R'))
                expect(reads[1]['name']).to(equal('2015-11-30'))
                expect(reads[1]['periode'][1]).to(equal('2.0A (P1)'))

                self.delete_created_reads(cursor, uid, wiz, '2015-11-01')
                # Using wizard force option
                wiz.write({
                    'force': True
                })
                self.wz_create_read.action_create_read(cursor, uid, [wz_id],
                                                       ctx)
                self.validate_monthly_reads(cursor, uid)
                reads = self.get_created_reads(cursor, uid, wiz, '2015-11-01')
                expect(reads[0]['lectura']).to(equal(105031))
                expect(reads[0]['tipus']).to(equal('A'))
                expect(reads[0]['name']).to(equal('2015-11-30'))
                expect(reads[0]['periode'][1]).to(equal('2.0A (P1)'))
                expect(reads[1]['lectura']).to(equal(310))
                expect(reads[1]['tipus']).to(equal('R'))
                expect(reads[1]['name']).to(equal('2015-11-30'))
                expect(reads[1]['periode'][1]).to(equal('2.0A (P1)'))

    def test_load_last_day_with_monthly_reads(self):

        with Transaction().start(self.database) as txn:

            uid = txn.user
            cursor = txn.cursor

            with PatchNewCursors():
                polissa_id = self.imd_obj.get_object_reference(
                    cursor, uid, 'giscedata_polissa', 'polissa_0004'
                )[1]
                activar_polissa(self.pool, cursor, uid, polissa_id)
                self.create_config_vars(cursor, uid)
                ctx = {'lang': 'en_US'}
                self.validate_monthly_reads(cursor, uid)
                wz_id = self.wz_create_read.create(cursor, uid, {}, ctx)
                wiz = self.wz_create_read.browse(cursor, uid, wz_id, ctx)
                # Ultim dia mes
                wiz.write({
                    'meter_id': 3,
                    'date_limit': '2015-11-30',
                    'force': False
                })
                self.wz_create_read.action_create_read(cursor, uid, [wz_id],
                                                       ctx)
                reads = self.get_created_reads(cursor, uid, wiz, '2015-11-01')
                expect(reads[0]['lectura']).to(equal(105031))
                expect(reads[0]['tipus']).to(equal('A'))
                expect(reads[0]['name']).to(equal('2015-11-30'))
                expect(reads[0]['periode'][1]).to(equal('2.0A (P1)'))
                expect(reads[1]['lectura']).to(equal(310))
                expect(reads[1]['tipus']).to(equal('R'))
                expect(reads[1]['name']).to(equal('2015-11-30'))
                expect(reads[1]['periode'][1]).to(equal('2.0A (P1)'))

                self.delete_created_reads(cursor, uid, wiz, '2015-11-01')
                # Using wizard force option
                wiz.write({
                    'force': True
                })
                self.wz_create_read.action_create_read(cursor, uid, [wz_id],
                                                       ctx)
                reads = self.get_created_reads(cursor, uid, wiz, '2015-11-01')
                expect(reads[0]['lectura']).to(equal(105031))
                expect(reads[0]['tipus']).to(equal('A'))
                expect(reads[0]['name']).to(equal('2015-11-30'))
                expect(reads[0]['periode'][1]).to(equal('2.0A (P1)'))
                expect(reads[1]['lectura']).to(equal(310))
                expect(reads[1]['tipus']).to(equal('R'))
                expect(reads[1]['name']).to(equal('2015-11-30'))
                expect(reads[1]['periode'][1]).to(equal('2.0A (P1)'))

    def test_load_previous_day_to_existing_last_read_without_forcing(self):

        with Transaction().start(self.database) as txn:

            uid = txn.user
            cursor = txn.cursor

            with PatchNewCursors():
                polissa_id = self.imd_obj.get_object_reference(
                    cursor, uid, 'giscedata_polissa', 'polissa_0004'
                )[1]
                activar_polissa(self.pool, cursor, uid, polissa_id)
                self.create_config_vars(cursor, uid)
                ctx = {'lang': 'en_US'}
                self.validate_monthly_reads(cursor, uid)
                self.validate_reads_from_date(cursor, uid, '2015-11-01')
                wz_id = self.wz_create_read.create(cursor, uid, {}, ctx)
                wiz = self.wz_create_read.browse(cursor, uid, wz_id, ctx)
                # Ultim dia mes
                wiz.write({
                    'meter_id': 3,
                    'date_limit': '2015-11-30',
                    'force': False
                })
                self.wz_create_read.action_create_read(cursor, uid, [wz_id],
                                                       ctx)
                # Using wizard force option
                wiz.write({
                    'date_limit': '2015-11-20'
                })
                self.wz_create_read.action_create_read(cursor, uid, [wz_id],
                                                       ctx)
                reads = self.get_created_reads(cursor, uid, wiz, '2015-11-01')
                # Expect to find the readings of the end of month
                expect(reads[0]['lectura']).to(equal(105031))
                expect(reads[0]['tipus']).to(equal('A'))
                expect(reads[0]['name']).to(equal('2015-11-30'))
                expect(reads[0]['periode'][1]).to(equal('2.0A (P1)'))
                expect(reads[1]['lectura']).to(equal(310))
                expect(reads[1]['tipus']).to(equal('R'))
                expect(reads[1]['name']).to(equal('2015-11-30'))
                expect(reads[1]['periode'][1]).to(equal('2.0A (P1)'))

    def test_load_previous_day_to_existing_last_read_with_forcing(self):

        with Transaction().start(self.database) as txn:

            uid = txn.user
            cursor = txn.cursor

            with PatchNewCursors():
                polissa_id = self.imd_obj.get_object_reference(
                    cursor, uid, 'giscedata_polissa', 'polissa_0004'
                )[1]
                activar_polissa(self.pool, cursor, uid, polissa_id)
                self.create_config_vars(cursor, uid)
                ctx = {'lang': 'en_US'}
                self.validate_monthly_reads(cursor, uid)
                self.validate_reads_from_date(cursor, uid, '2015-11-01')
                wz_id = self.wz_create_read.create(cursor, uid, {}, ctx)
                wiz = self.wz_create_read.browse(cursor, uid, wz_id, ctx)
                # Ultim dia mes
                wiz.write({
                    'meter_id': 3,
                    'date_limit': '2015-11-30',
                    'force': False
                })
                self.wz_create_read.action_create_read(cursor, uid, [wz_id],
                                                       ctx)
                # Using wizard force option
                wiz.write({
                    'date_limit': '2015-11-20',
                    'force': True
                })
                self.wz_create_read.action_create_read(cursor, uid, [wz_id],
                                                       ctx)
                reads = self.get_created_reads(cursor, uid, wiz, '2015-11-01')
                # Expect to find the readings of the end of month
                expect(reads[0]['lectura']).to(equal(105031))
                expect(reads[0]['tipus']).to(equal('A'))
                expect(reads[0]['name']).to(equal('2015-11-30'))
                expect(reads[0]['periode'][1]).to(equal('2.0A (P1)'))
                expect(reads[1]['lectura']).to(equal(310))
                expect(reads[1]['tipus']).to(equal('R'))
                expect(reads[1]['name']).to(equal('2015-11-30'))
                expect(reads[1]['periode'][1]).to(equal('2.0A (P1)'))

                # And the readings from the 20th
                expect(reads[2]['lectura']).to(equal(105019))
                expect(reads[2]['tipus']).to(equal('A'))
                expect(reads[2]['name']).to(equal('2015-11-20'))
                expect(reads[2]['periode'][1]).to(equal('2.0A (P1)'))
                expect(reads[3]['lectura']).to(equal(210))
                expect(reads[3]['tipus']).to(equal('R'))
                expect(reads[3]['name']).to(equal('2015-11-20'))
                expect(reads[3]['periode'][1]).to(equal('2.0A (P1)'))

    def test_maximeter_reads_created(self):
        with Transaction().start(self.database) as txn:

            uid = txn.user
            cursor = txn.cursor

            with PatchNewCursors():
                polissa_id = self.imd_obj.get_object_reference(
                    cursor, uid, 'giscedata_polissa', 'polissa_0004'
                )[1]
                activar_polissa(self.pool, cursor, uid, polissa_id)
                self.create_config_vars(cursor, uid)
                ctx = {'lang': 'en_US'}
                self.validate_monthly_reads(cursor, uid)
                self.validate_reads_from_date(cursor, uid, '2015-11-01')
                wz_id = self.wz_create_read.create(cursor, uid, {}, ctx)
                wiz = self.wz_create_read.browse(cursor, uid, wz_id, ctx)
                # Ultim dia mes
                wiz.write({
                    'meter_id': 3,
                    'date_limit': '2015-11-30',
                    'force': False
                })
                self.wz_create_read.action_create_read(cursor, uid, [wz_id],
                                                       ctx)
                reads = self.get_max_created_reads(cursor, uid, wiz, '2015-11-01')
                expect(reads[0]['lectura']).to(equal(7.576))

    def test_maximeter_reads_created_21DHA(self):
        with Transaction().start(self.database) as txn:

            uid = txn.user
            cursor = txn.cursor

            # Especificar valor '' a res_config

            with PatchNewCursors():
                self.clean_created_billings(cursor, uid)
                tg_reader_obj = self.openerp.pool.get('tg.reader')
                from primestg.report import Report as primeReport
                tg_xml = primeReport(self.s04_xml)
                tg_reader_obj.insert_primestg(cursor, uid, tg_xml)

                polissa_id = self.imd_obj.get_object_reference(
                    cursor, uid, 'giscedata_polissa', 'polissa_0009'
                )[1]
                # self.res_config.set(cursor, uid, 'tg_serial_length', '11')
                activar_polissa(self.pool, cursor, uid, polissa_id)
                self.create_config_vars(cursor, uid)
                ctx = {'lang': 'en_US'}
                self.validate_monthly_reads(cursor, uid)
                self.validate_reads_from_date(cursor, uid, '2017-05-01')
                wz_id = self.wz_create_read.create(cursor, uid, {}, ctx)
                wiz = self.wz_create_read.browse(cursor, uid, wz_id, ctx)
                # Ultim dia mes
                wiz.write({
                    'meter_id': 4,
                    'date_limit': '2017-07-01',
                    'force': False
                })
                self.wz_create_read.action_create_read(cursor, uid, [wz_id],
                                                       ctx)
                reads = self.get_max_created_reads(cursor, uid, wiz, '2017-06-01')
                self.clean_created_billings(cursor, uid)
                expect(reads[0]['lectura']).to(equal(7.576))

    def test_maximeter_reads_created_20DHS(self):
        with Transaction().start(self.database) as txn:

            uid = txn.user
            cursor = txn.cursor

            # Especificar valor '' a res_config

            with PatchNewCursors():
                self.clean_created_billings(cursor, uid)
                tg_reader_obj = self.openerp.pool.get('tg.reader')
                from primestg.report import Report as primeReport
                tg_xml = primeReport(self.s04_xml)
                tg_reader_obj.insert_primestg(cursor, uid, tg_xml)

                polissa_id = self.imd_obj.get_object_reference(
                    cursor, uid, 'giscedata_polissa', 'polissa_0008'
                )[1]
                # self.res_config.set(cursor, uid, 'tg_serial_length', '11')
                activar_polissa(self.pool, cursor, uid, polissa_id)
                self.create_config_vars(cursor, uid)
                ctx = {'lang': 'en_US'}
                self.validate_monthly_reads(cursor, uid)
                self.validate_reads_from_date(cursor, uid, '2017-05-01')
                wz_id = self.wz_create_read.create(cursor, uid, {}, ctx)
                wiz = self.wz_create_read.browse(cursor, uid, wz_id, ctx)
                # Ultim dia mes
                wiz.write({
                    'meter_id': 5,
                    'date_limit': '2017-07-01',
                    'force': False
                })
                self.wz_create_read.action_create_read(cursor, uid, [wz_id],
                                                       ctx)
                reads = self.get_max_created_reads(cursor, uid, wiz, '2017-06-01')
                self.clean_created_billings(cursor, uid)
                expect(reads[0]['lectura']).to(equal(8.428))

    def test_validate_billings_before_data_alta(self):
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor

            with PatchNewCursors():
                self.clean_created_billings(cursor, uid)
                tg_reader_obj = self.openerp.pool.get('tg.reader')
                tg_validate_obj = self.pool.get('tg.lectures.validate')
                meter_obj = self.openerp.pool.get('giscedata.lectures.comptador')
                pol_obj = self.openerp.pool.get('giscedata.polissa')
                tg_billing_obj = self.pool.get('tg.billing')
                tg_xml = primeReport(self.s05_dataalta_xml)
                tg_reader_obj.insert_primestg(cursor, uid, tg_xml)

                polissa_id = self.imd_obj.get_object_reference(
                    cursor, uid, 'giscedata_polissa', 'polissa_0008'
                )[1]
                data_alta = pol_obj.read(cursor, uid, polissa_id, ['data_alta'])
                pol_obj.write(cursor, uid, polissa_id,
                              {'data_alta': '2016-01-01'})
                activar_polissa(self.pool, cursor, uid, polissa_id)
                self.create_config_vars(cursor, uid)
                ctx = {'lang': 'en_US',
                       'tg_check_unreaded': False,
                       'validate': True}
                meter_name = meter_obj.build_name_tg(cursor, uid, 5,
                                                     context=ctx)
                tg_validate_obj.validate_individual(cursor, uid, [], meter_name,
                                                    context=ctx)
                pol_obj.write(cursor, uid, polissa_id, {
                    'data_alta': data_alta['data_alta']})
                valid_billings = tg_billing_obj.search(cursor, uid, [
                    ('name', '=', 'ZIV0081178117'),
                    ('valid', '=', 'true'),
                ])
                expect(len(valid_billings)).to(equal(16))

    def delete_created_reads(self, cursor, uid, wiz, date_from):
        l_ids = self.lect_obj.search(
            cursor, uid, [
                ('comptador', '=', wiz.meter_id.id),
                ('name', '>=', date_from)
            ]
        )
        self.lect_obj.unlink(
            cursor, uid, l_ids, {}
        )

    def get_created_reads(self, cursor, uid, wiz, date_from):
        l_ids = self.lect_obj.search(
            cursor, uid, [
                ('comptador', '=', wiz.meter_id.id),
                ('name', '>=', date_from)
            ]
        )
        return self.lect_obj.read(
            cursor, uid, l_ids, []
        )

    def get_max_created_reads(self, cursor, uid, wiz, date_from):
        l_ids = self.potencia_obj.search(
            cursor, uid, [
                ('comptador', '=', wiz.meter_id.id),
                ('name', '>=', date_from)
            ]
        )
        return self.potencia_obj.read(
            cursor, uid, l_ids, []
        )

    def unvalidate_reads_from_date(self, cursor, uid, date_from):
        l_ids = self.tg_billing_obj.search(
            cursor, uid, [
                ('name', '=', 'ZIV0042553686'),
                ('date_end', '>=', date_from)
            ]
        )
        return self.tg_billing_obj.write(
            cursor, uid, l_ids, {'valid': False}
        )

    def validate_reads_from_date(self, cursor, uid, date_from):
        l_ids = self.tg_billing_obj.search(
            cursor, uid, [
                ('name', '=', 'ZIV0042553686'),
                ('date_end', '>=', date_from)
            ]
        )
        return self.tg_billing_obj.write(
            cursor, uid, l_ids, {'valid': True}
        )

    def unvalidate_monthly_reads(self, cursor, uid):
        l_ids = self.tg_billing_obj.search(
            cursor, uid, [
                ('type', '=', 'month')
            ]
        )
        return self.tg_billing_obj.write(
            cursor, uid, l_ids, {'valid': False}
        )

    def validate_monthly_reads(self, cursor, uid):
        l_ids = self.tg_billing_obj.search(
            cursor, uid, [
                ('type', '=', 'month')
            ]
        )
        return self.tg_billing_obj.write(
            cursor, uid, l_ids, {'valid': True}
        )

    def create_config_vars(self, cursor, uid):
        search_params = [('name', '=', 'tg_clean_left_zeroes_serial')]
        identifier = self.config.search(cursor, uid, search_params)
        if identifier:
            self.config.write(cursor, uid, identifier, {'value': '1'})
        else:
            self.config.create(cursor, uid, {
                'name': "tg_clean_left_zeroes_serial",
                'value': "1"})
        search_params = [('name', '=', 'tg_clean_serial_chars')]
        identifier = self.config.search(cursor, uid, search_params)
        if identifier:
            self.config.write(cursor, uid, identifier, {'value': '3'})
        else:
            self.config.create(cursor, uid, {
                'name': "tg_clean_serial_chars",
                'value': "3"})

    def clean_created_billings(self, cursor, uid):
        bill_ids = self.tg_billing_obj.search(
            cursor, uid, [('name', 'in', ["ZIV36301516", "ZIV0073277327", "ZIV0081178117"])])
        self.tg_billing_obj.unlink(cursor, uid, bill_ids)
