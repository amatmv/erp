from destral import testing
from destral.transaction import Transaction
from mongodb_backend.mongodb2 import mdbpool
from datetime import datetime
from destral.patch import PatchNewCursors
from expects import *
import time
from addons import get_module_resource
from primestg.report import Report as primeReport
from test_create_reads_from_tg import *
from test_connectivity_fields import *


class TestGiscedataLecturesTelegestioReactiva(testing.OOTestCase):
    """
    Tests of inserting reactive.
    """
    def setUp(self):
        self.billing_id = 0

    def tearDown(self):
        tg_billing = self.openerp.pool.get('tg.billing')
        with Transaction().start(self.database) as txn:

            uid = txn.user
            cursor = txn.cursor
            if self.billing_id != 0:
                tg_billing.unlink(cursor, uid, self.billing_id)

    def test_insert_reactive(self):
        """
        Test of inserting reactive.
        """
        giscedata_cups_ps = self.openerp.pool.get('giscedata.cups.ps')
        giscedata_polissa = self.openerp.pool.get('giscedata.polissa')
        giscedata_polissa_modcontractual = \
            self.openerp.pool.get('giscedata.polissa.modcontractual')
        giscedata_lectures_comptador = \
            self.openerp.pool.get('giscedata.lectures.comptador')
        tg_billing = self.openerp.pool.get('tg.billing')
        giscedata_lectures_lectura = \
            self.openerp.pool.get('giscedata.lectures.lectura')
        res_config = self.openerp.pool.get('res.config')
        origin_obj = self.openerp.pool.get('giscedata.lectures.origen')

        with Transaction().start(self.database) as txn:

            uid = txn.user
            cursor = txn.cursor

            with PatchNewCursors():

                cups = {
                    'id_municipi': 875,
                    'name': u'ES0113000051404984MG0F'
                }
                cups_id = giscedata_cups_ps.create(cursor, uid, cups)
                policy_id = giscedata_polissa.create(cursor, uid, {'potencia': 1})
                contractual_amendment = {
                    'name': u'1',
                    'polissa_id': policy_id,
                    'active': True,
                    'state': u'actiu',
                    'tipus': u'alta',
                    'cups': cups_id,
                    'tarifa': 1,
                    'potencia': 1.0,
                    'tensio_normalitzada': 3,
                    'tensio': 220,
                    'potencies_periode': u'P1: 1.0',
                    'data_inici': '2015-08-01',
                    'data_final': '2016-07-31',
                    'comercialitzadora': 1,
                }
                giscedata_polissa_modcontractual.create(
                    cursor,
                    uid,
                    contractual_amendment
                )
                meter = {
                    'name': u'ITE0131750580',
                    'polissa': policy_id,
                    'data_alta': '2015-08-01'
                }
                meter_id = giscedata_lectures_comptador.create(cursor, uid, meter)

                billing = {
                    u'r4': 569,
                    u'valid_date': '2015-09-01 00:00:00',
                    u'ae': 0,
                    u'r1': 1964,
                    u'r2': 0,
                    u'r3': 0,
                    u'ai': 7984,
                    u'max': 2452,
                    u'date_end': '2015-09-01 00:00:00',
                    u'period': 0,
                    u'value': u'a',
                    u'valid': True,
                    u'date_max': '2015-08-26 13:15:00',
                    u'date_begin': '2015-08-01 00:00:00',
                    u'type': u'month',
                    u'name': u'ITE0131750580'
                }
                origin_id = origin_obj.search(cursor, uid,
                                              [('codi', '=', '60')])[0]
                billing_id = tg_billing.create(cursor, uid, billing)
                self.billing_id = billing_id
                context = {'date_limit': '2015-09-01'}
                active = {
                    'ajust': 0,
                    'ajust_exporta': 0,
                    'lectura_exporta': 0,
                    'generacio': None,
                    'motiu_ajust': None,
                    'periode': 1,
                    'name': '2015-08-31',
                    'observacions': None,
                    'consum': None,
                    'incidencia_id': None,
                    'comptador': meter_id,
                    'origen_id': origin_id,
                    'lectura': 7984,
                    'tipus': u'A',
                    'lectura_exporta': 0,
                    'ajust_exporta': 0,
                    'generacio': None,
                }
                reactive = {
                    'ajust': 0,
                    'ajust_exporta': 0,
                    'lectura_exporta': 0,
                    'generacio': None,
                    'motiu_ajust': None,
                    'periode': 1,
                    'name': '2015-08-31',
                    'observacions': None,
                    'consum': None,
                    'incidencia_id': None,
                    'comptador': meter_id,
                    'origen_id': origin_id,
                    'lectura': 1964,
                    'tipus': u'R',
                    'lectura_exporta': 0,
                    'ajust_exporta': 0,
                    'generacio': None,
                }
                query = [
                    ('name', '=', '2015-08-31'),
                    ('comptador', '=', meter_id),
                ]

                res_config.set(cursor, uid, 'tg_reactive_insert', 'always')
                expected_always = [active, reactive]
                giscedata_lectures_comptador.create_read_from_tg(
                    cursor,
                    uid,
                    [meter_id],
                    context
                )

                always = giscedata_lectures_lectura.search_reader(
                    cursor,
                    uid,
                    query
                )
                ids_always = []
                for read in always:
                    ids_always.append(read['id'])
                    del read['id']
                expect(always).to(equal(expected_always))
                giscedata_lectures_lectura.unlink(
                    cursor,
                    uid,
                    ids_always,
                    context={}
                )

                res_config.set(cursor, uid, 'tg_reactive_insert', 'zero')
                reactive['lectura'] = 0
                expected_zero = [active, reactive]
                giscedata_lectures_comptador.create_read_from_tg(
                    cursor,
                    uid,
                    [meter_id],
                    context
                )
                zero = giscedata_lectures_lectura.search_reader(cursor, uid, query)
                ids_zero = []
                for read in zero:
                    ids_zero.append(read['id'])
                    del read['id']
                expect(zero).to(equal(expected_zero))
                giscedata_lectures_lectura.unlink(
                    cursor,
                    uid,
                    ids_zero,
                    context={}
                )

                res_config.set(cursor, uid, 'tg_reactive_insert', 'never')
                expected_never = [active]
                giscedata_lectures_comptador.create_read_from_tg(
                    cursor,
                    uid,
                    [meter_id],
                    context
                )
                never = giscedata_lectures_lectura.search_reader(
                    cursor,
                    uid,
                    query
                )
                ids_never = []
                for read in never:
                    ids_never.append(read['id'])
                    del read['id']
                expect(never).to(equal(expected_never))
                giscedata_lectures_lectura.unlink(
                    cursor,
                    uid,
                    ids_never,
                    context={}
                )
                tg_billing.unlink(cursor, uid, [billing_id])


class TestFunctionFieldMeterException(testing.OOTestCase):

    def test_field_exception_returns_false_when_no_exception(self):

        giscedata_lectures_comptador = \
            self.openerp.pool.get('giscedata.lectures.comptador')

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor

            imd_obj = self.openerp.pool.get('ir.model.data')
            cmpt_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_lectures', 'comptador_0001')[1]
            cmpt = giscedata_lectures_comptador.browse(cursor, uid, cmpt_id)
            expect(cmpt.tg_meter_exception).to(be_false)

    def test_field_exception_returns_true_when_exception(self):

        giscedata_lectures_comptador = \
            self.openerp.pool.get('giscedata.lectures.comptador')
        giscedata_meter_exception = \
            self.openerp.pool.get('tg.validate.meter.exception')

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor

            imd_obj = self.openerp.pool.get('ir.model.data')
            cmpt_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_lectures', 'comptador_0001')[1]
            cmpt = giscedata_lectures_comptador.browse(cursor, uid,
                                                       cmpt_id)
            exception = {
                'serial': cmpt.build_name_tg(),
                'notes': 'testing exceptions'
                }
            giscedata_meter_exception.create(cursor, uid, exception)

            expect(cmpt.tg_meter_exception).to(be_true)

    def test_field_exception_returns_false_when_name_diff_build_name(self):

        giscedata_lectures_comptador = \
            self.openerp.pool.get('giscedata.lectures.comptador')

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor

            imd_obj = self.openerp.pool.get('ir.model.data')
            cmpt_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_lectures', 'comptador_0001')[1]
            cmpt = giscedata_lectures_comptador.browse(cursor, uid, cmpt_id)

            giscedata_lectures_comptador.write(cursor, uid, cmpt_id,
                                           {'name': 'B63011088'})

            expect(cmpt.tg_meter_exception).to(be_false)


class TG_Tests(testing.OOTestCase):

    def setUp(self):
        self.mongodb_name = 'test_{0}'.format(int(time.time()))

        s06_path = get_module_resource(
            'giscedata_lectures_telegestio', 'tests', 'data', 'S06.xml'
        )
        self.s06_xml = open(s06_path, 'r')

        # Mongo cleaning

    def tearDown(self):
        self.s06_xml.close()

    def test_load_S06(self):
        imd_obj = self.openerp.pool.get('ir.model.data')
        tg_reader_obj = self.openerp.pool.get('tg.reader')
        meter_obj = self.openerp.pool.get('giscedata.lectures.comptador')
        res_config = self.openerp.pool.get('res.config')
        report_read = self.s06_xml.read()
        report = primeReport(report_read)

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            old_serial_len = res_config.get(cursor, uid,
                                            'tg_clean_serial_chars', '3')
            res_config.set(cursor, uid, 'tg_clean_serial_chars', '3')
            res_config.set(cursor, uid, 'tg_clean_left_zeroes_serial', '1')
            tg_reader_obj.insert_primestg(cursor, uid, report)
            meter_reg = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_lectures', 'comptador_0001'
            )[1]
            res_config.set(cursor, uid, 'tg_clean_serial_chars', old_serial_len)
            meter_data = meter_obj.read(cursor, uid, meter_reg)
            expect(meter_data['request_id']).to(equal('2075918626'))
            expect(meter_data['version']).to(equal('3.1.c'))
            expect(meter_data['concentrator']).to(equal('CT11'))
            expect(meter_data['meter']).to(equal('ZIV0036301516'))
            expect(meter_data['timestamp']).to(equal('2015-09-29 10:03:45'))
            expect(meter_data['season']).to(equal('S'))
            expect(meter_data['serial_number']).to(equal('0040318130'))
            expect(meter_data['manufacturer']).to(equal('FABZ'))
            expect(meter_data['model_type']).to(equal('NT'))
            expect(meter_data['manufacturing_year']).to(equal(15))
            expect(meter_data['equipment_type']).to(equal('contador'))
            expect(meter_data['firmware_version']).to(equal('VK013'))
            expect(meter_data['prime_firmware_version']).to(equal('V2203'))
            expect(meter_data['protocol']).to(equal('DLMS0106'))
            expect(meter_data['id_multicast']).to(equal(''))
            expect(meter_data['mac']).to(equal('40:40:22:67:34:B2'))
            expect(meter_data['primary_voltage']).to(equal(2300))
            expect(meter_data['secondary_voltage']).to(equal(2300))
            expect(meter_data['primary_current']).to(equal(50))
            expect(meter_data['secondary_current']).to(equal(50))
            expect(meter_data['time_threshold_voltage_sags']).to(equal(180))
            expect(meter_data['time_threshold_voltage_swells']).to(equal(180))
            expect(meter_data['load_profile_period']).to(equal(3600))
            expect(meter_data['demand_close_contracted_power']).to(equal(95.00))
            expect(meter_data['reference_voltage']).to(equal(230))
            expect(meter_data['long_power_failure_threshold']).to(equal(180))
            expect(meter_data['voltage_sag_threshold']).to(equal(7.00))
            expect(meter_data['voltage_swell_threshold']).to(equal(7.00))
            expect(meter_data['voltage_cut-off_threshold']).to(equal(50.00))
            expect(meter_data['automatic_monthly_billing']).to(equal(True))
            expect(meter_data['scroll_display_mode']).to(equal('A'))
            expect(meter_data['time_scroll_display']).to(equal(3))


class TestSingleCheck(testing.OOTestCase):
    """
    Tests for single measure checks.
    """

    def test_single_check_more_than_pot(self):
        """
        Test of check hourly limit.
        """
        giscedata_cups_ps = self.openerp.pool.get('giscedata.cups.ps')
        giscedata_polissa = self.openerp.pool.get('giscedata.polissa')
        giscedata_polissa_modcontractual = \
            self.openerp.pool.get('giscedata.polissa.modcontractual')
        tg_profile = self.openerp.pool.get('tg.profile')
        tg_profile_validate = self.openerp.pool.get('tg.profile.validate')

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor

            cups = {
                'id_municipi': 875,
                'name': u'ES0113000051404984MG0F'
            }
            cups_id = giscedata_cups_ps.create(cursor, uid, cups)
            policy_id = giscedata_polissa.create(cursor, uid, {'potencia': 1})
            contractual_amendment = {
                'name': u'1',
                'polissa_id': policy_id,
                'active': True,
                'state': u'actiu',
                'tipus': u'alta',
                'cups': cups_id,
                'tarifa': 1,
                'potencia': 1.0,
                'tensio_normalitzada': 3,
                'tensio': 220,
                'potencies_periode': u'P1: 1.0',
                'data_inici': '2015-08-01',
                'data_final': '2016-07-31',
                'comercialitzadora': 1
            }
            giscedata_polissa_modcontractual.create(
                cursor,
                uid,
                contractual_amendment
            )

            insert_measure_correct = {
                u'r4': 0.0,
                u'cch_fact': False,
                u'ae': 0.0,
                u'name': u'ITE0131750580',
                u'r2': 0.0,
                u'bc': u'00',
                u'ai': 19.0,
                u'season': u'S',
                u'r3': 0.0,
                u'cch_bruta': True,
                u'valid': False,
                u'valid_date': False,
                u'magn': 1,
                u'timestamp': '2015-08-31 02:00:00',
                u'r1': 11.0
            }
            measure_id_correct = tg_profile.create(
                cursor,
                uid,
                insert_measure_correct
            )
            tg_profile_mdbpool = mdbpool.get_collection('tg_profile')
            measures = tg_profile_mdbpool.find({'id': measure_id_correct})
            measure_correct = measures[0]

            policy = giscedata_polissa.browse(
                cursor,
                uid,
                policy_id,
                context={'date': '2015-08-31'}
            )

            meter_exceptions = None
            result = tg_profile_validate.single_check_more_than_pot(
                cursor,
                uid,
                measure_correct,
                policy,
                meter_exceptions,
            )
            expect(result).not_to(have_key(measure_id_correct))

            insert_measure_tolerated = {
                u'r4': 0.0,
                u'cch_fact': False,
                u'ae': 0.0,
                u'name': u'ITE0131750580',
                u'r2': 0.0,
                u'bc': u'00',
                u'ai': 1200.0,
                u'season': u'S',
                u'r3': 0.0,
                u'cch_bruta': True,
                u'valid': False,
                u'valid_date': False,
                u'magn': 1,
                u'timestamp': '2015-08-31 02:00:00',
                u'r1': 11.0
            }
            measure_id_tolerated = tg_profile.create(
                cursor,
                uid,
                insert_measure_tolerated
            )
            measures = tg_profile_mdbpool.find({'id': measure_id_tolerated})
            measure_tolerated = measures[0]

            result_tolerated = tg_profile_validate.single_check_more_than_pot(
                cursor,
                uid,
                measure_tolerated,
                policy,
                meter_exceptions,
            )
            expect(result_tolerated).not_to(have_key(measure_id_tolerated))

            insert_measure_error = {
                u'r4': 0.0,
                u'cch_fact': False,
                u'ae': 0.0,
                u'name': u'ITE0131750580',
                u'r2': 0.0,
                u'bc': u'00',
                u'ai': 2001.0,
                u'season': u'S',
                u'r3': 0.0,
                u'cch_bruta': True,
                u'valid': False,
                u'valid_date': False,
                u'magn': 1,
                u'timestamp': '2015-08-31 02:00:00',
                u'r1': 11.0
            }
            measure_id_error = tg_profile.create(
                cursor,
                uid,
                insert_measure_error
            )
            measures = tg_profile_mdbpool.find({'id': measure_id_error})
            measure_error = measures[0]

            result_error = tg_profile_validate.single_check_more_than_pot(
                cursor,
                uid,
                measure_error,
                policy,
                meter_exceptions,
            )
            expected_result_error = 'Impossible consumption at 31/08/2015 ' \
                                    '02 ai: 2001.0, Power: 1.0'
            expect(result_error[measure_id_error]).\
                to(equal(expected_result_error))


class TestCreateCase(testing.OOTestCase):
    """
    Tests for create cases for invalid measures.
    """

    def test_create_case_notfound(self):
        """
        Test create case for measures without a valid meter.
        """
        res_config = self.openerp.pool.get('res.config')
        tg_profile_validate = self.openerp.pool.get('tg.profile.validate')
        crm_case = self.openerp.pool.get('crm.case')

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor

            context = {'lang': 'en_US'}
            res_config.set(cursor, uid, 'tg_profile_check_case_notfound', '1')
            crm_case.search(cursor, uid, [])
            time = datetime.strptime(
                '2015-08-31 02:00:00',
                '%Y-%m-%d %H:%M:%S'
            )
            notfound = {1: time}
            meter_name = 'ITE0131750580'
            result = tg_profile_validate.create_case_notfound(
                cursor,
                uid,
                notfound,
                meter_name,
                context=context
            )
            expect(result).to(be_true)
            case_ids = crm_case.search(
                cursor, uid, [('section_id', '=', 'Telegestio')]
            )
            case = crm_case.read(cursor, uid, case_ids)
            expect(case[0]['id']).to_not(be_false)
            expect(case[0]['create_date']).to_not(be_false)
            expect(case[0]['date']).to_not(be_false)
            expect(case[0]['log_ids']).to_not(be_false)
            expect(case[0]['history_line']).to_not(be_false)
            del case[0]['id']
            del case[0]['create_date']
            del case[0]['date']
            del case[0]['description']
            del case[0]['email_last']
            del case[0]['log_ids']
            del case[0]['history_line']
            del case[0]['section_id']
            del case[0]['categ_ids']
            del case[0]['categ_id']
            expected_case = [
                {
                    'date_closed': False,
                    'probability': False,
                    'canal_id': False,
                    'tg_ct': u'',
                    'partner_address_id': False,
                    'date_action_last': False,
                    'polissa_id': False,
                    'active': True,
                    'tg_cnc': '',
                    'planned_revenue': False,
                    'date_action_next': False,
                    'create_uid': (1, u'Administrator'),
                    'user_id': (1, u'Administrator'),
                    'partner_id': False,
                    'priority': u'3',
                    'state': u'open',
                    'email_cc': False,
                    'ref': False,
                    'som': False,
                    'planned_cost': False,
                    'ref2': False,
                    'name': u'TG Profile out of range for ITE0131750580',
                    'date_deadline': False,
                    'tg_hour_fi': False,
                    'tg_hour_ini': False,
                    'email_from': False
                }
            ]
            expect(case).to(equal(expected_case))

    def test_not_create_case_notfound(self):
        """
        Test disable creation case for measures without a valid meter  by \
            configuration.
        """
        res_config = self.openerp.pool.get('res.config')
        tg_profile_validate = self.openerp.pool.get('tg.profile.validate')
        crm_case = self.openerp.pool.get('crm.case')

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor

            case_vals = {
                    'date_closed': False,
                    'history_line': [],
                    'probability': False,
                    'canal_id': False,
                    'tg_ct': u'',
                    'partner_address_id': False,
                    'date_action_last': False,
                    'polissa_id': False,
                    'active': True,
                    'tg_cnc': '',
                    'planned_revenue': False,
                    'date_action_next': False,
                    'user_id': 1,
                    'partner_id': False,
                    'priority': u'3',
                    'state': u'open',
                    'email_cc': False,
                    'ref': False,
                    'description': ':\nMeter ITE0131750580 not found on 31/08/2015 in concentrator \n',
                    'som': False,
                    'planned_cost': False,
                    'ref2': False,
                    'section_id': 2,
                    'categ_id': 2,
                    'name': u'TG Profile out of range for ITE0131750580',
                    'date_deadline': False,
                    'email_last': False,
                    'tg_hour_fi': False,
                    'tg_hour_ini': False,
                    'email_from': False
                }
            crm_case.create(cursor, uid, case_vals)
            case_ids_old = crm_case.search(cursor, uid, [])
            res_config.set(cursor, uid, 'tg_profile_check_case_notfound', '1')

            time = datetime.strptime(
                '2015-08-31 02:00:00',
                '%Y-%m-%d %H:%M:%S'
            )
            not_found = {1: time}
            meter_name = 'ITE0131750580'
            context = {'lang': 'en_US'}
            result = tg_profile_validate.create_case_notfound(
                cursor,
                uid,
                not_found,
                meter_name,
                context=context
            )
            expect(result).to(equal(False))
            case_ids_new = crm_case.search(cursor, uid, [])
            expect(len(case_ids_old)).to(equal(len(case_ids_new)))
