# -*- coding: utf-8 -*-
from osv import osv, fields
from datetime import datetime, timedelta
import csv
import netsvc
import StringIO
from addons.giscedata_lectures_telegestio.lectures import get_last_date_cch_val
from tools.translate import _


class GiscedataCupsPs(osv.osv):

    _name = 'giscedata.cups.ps'
    _inherit = 'giscedata.cups.ps'

    def get_CCH(self, cursor, uid, ids, cch_type='cch_val', start_date=None,
                end_date=None, fformat='csv', context=None):

        if not context:
            context = {}

        if not isinstance(ids, (tuple, list)):
            ids = [ids]

        if fformat == 'xls':
            ids = ids[:1]

        cups_obj = self.pool.get('giscedata.cups.ps')
        pol_obj = self.pool.get('giscedata.polissa')
        modcon_obj = self.pool.get('giscedata.polissa.modcontractual')
        meter_obj = self.pool.get('giscedata.lectures.comptador')

        set_last_cch = context.get('set_last_cch', False)
        logger = netsvc.Logger()

        curve = []
        res_to_write = []

        if not end_date:
            end_date = datetime.today().strftime('%Y-%m-%d')

        for cups_id in ids:
            max_profile_date = None
            vals = cups_obj.read(
                cursor, uid, cups_id, ['last_cch_val_curve', 'name'],
                context=context
            )
            cups_name = vals['name']
            last_cch_val_curve = vals['last_cch_val_curve']
            if last_cch_val_curve:
                last_cch_val_curve_datetime = datetime.strptime(
                    last_cch_val_curve,
                    "%Y-%m-%d %H:%M:%S"
                )

            # If not start_date, takes last generated
            if start_date:
                from_timestamp = start_date
            elif last_cch_val_curve:
                one_hour = timedelta(hours=1)
                next_slot_cch_val_curve_datetime = \
                    last_cch_val_curve_datetime + one_hour
                from_timestamp = \
                    next_slot_cch_val_curve_datetime.strftime(
                        "%Y-%m-%d %H:%M:%S"
                    )
            else:
                from_timestamp = '2015-09-01'

            # search contracts in period
            modcon_intervals = cups_obj.get_modcontractual_intervals(
                cursor,
                uid,
                [cups_id],
                from_timestamp,
                end_date,
                context=context
            )
            for cut_date in sorted(modcon_intervals):
                policy = modcon_obj.read(
                    cursor,
                    uid,
                    modcon_intervals[cut_date]['id'],
                    ['polissa_id','tg']
                )
                if policy['tg'] != '1':
                    continue
                pol_id = policy['polissa_id']
                m_start = modcon_intervals[cut_date]['dates'][0]
                m_end = modcon_intervals[cut_date]['dates'][1]
                meters = pol_obj.comptadors_actius(cursor, uid, pol_id,
                                                   m_start, m_end)
                # returns newer first, we reverse it to start on older
                for meter_id in reversed(meters):
                    # Get curve from MAX(from_timestamp, m_start)
                    #             to MIN(end_date, m_end)
                    from_timestamp = max(from_timestamp, m_start)
                    to_timestamp = min(end_date, m_end)
                    meter_curve = meter_obj.get_cch(
                        cursor,
                        uid,
                        meter_id,
                        cch_type,
                        from_timestamp,
                        to_timestamp,
                        context=context
                    )
                    if cch_type in ('cch_val', 'cch_fact') and set_last_cch:
                        if meter_curve[meter_id]:
                            max_profile_date = get_last_date_cch_val(
                                meter_curve[meter_id]
                            )
                    curve.extend(meter_curve[meter_id])

            if max_profile_date:
                current_cups_last_cch_val_curve = datetime(1,1,1)
                if last_cch_val_curve:
                    current_cups_last_cch_val_curve = \
                        last_cch_val_curve_datetime

                if max_profile_date > current_cups_last_cch_val_curve:
                    cups_info = {
                        'cups_id': cups_id,
                        'cups_name': cups_name,
                        'from_timestamp': from_timestamp,
                        'max_profile_date': max_profile_date
                    }
                    res_to_write.append(cups_info)

        for cups_info in res_to_write:
            cups_obj.write(cursor, uid, cups_info['cups_id'],
                           {'last_cch_val_curve':
                            cups_info['max_profile_date']})
            logger.notifyChannel(
                'exporta_curves_tg',
                netsvc.LOG_INFO,
                _("CUPS {0} last_cch_val_curve from '{1}' to '{2}'").format(
                    cups_info['cups_name'],
                    cups_info['from_timestamp'],
                    cups_info['max_profile_date']
                )
            )

        output = StringIO.StringIO()
        if fformat == 'xls':
            alt_sheet_name = context.get('fact_name', 'CCH_CONS')
            meter_obj.get_as_xls(cursor, uid, output, {1: curve},
                                 alt_sheet_name, context=None)
        else:
            writer = csv.writer(output, delimiter=';')

            for line in curve:
                writer.writerow(line)

        text = output.getvalue()
        output.close()

        return text

    def get_CCH_VAL(self, cursor, uid, ids, start_date=None, end_date=None,
                    fformat='csv', context=None):
            return self.get_CCH(cursor, uid, ids, 'cch_val', start_date,
                                end_date, fformat, context)

    def get_CCH_FACT(self, cursor, uid, ids, start_date=None, end_date=None,
                     fformat='csv', context=None):
            return self.get_CCH(cursor, uid, ids, 'cch_fact', start_date,
                                end_date, fformat, context)

    def get_CCH_CONS(self, cursor, uid, ids, start_date=None, end_date=None,
                     fformat='csv', context=None):
            return self.get_CCH(cursor, uid, ids, 'cch_cons', start_date,
                                end_date, fformat, context)

    _columns = {
        'last_cch_val_curve': fields.datetime(u'Última curva CCH_VAL generada'),
    }

GiscedataCupsPs()
