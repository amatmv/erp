from osv import osv
from tools import cache
from datetime import datetime


class CrmCase(osv.osv):

    _inherit = 'crm.case'
    _name = 'crm.case'

    @cache()
    def date_format(self, cursor, uid, ids, context=None):
        """
        Format for date from context or user preferences.

        :param cursor: database cursor
        :param uid: user identifier
        :param ids: not used
        :param context: dictionary with context
        :return: string with the date format
        """
        if not context:
            context = {}

        context_lang = self.pool.get('res.users').read(
            cursor,
            uid,
            uid,
            ['context_lang']
        )['context_lang']

        lang_code = context.get('lang', context_lang)
        return self.pool.get('res.lang').search_reader(
            cursor,
            uid,
            [('code', '=', lang_code)],
            ['date_format']
        )[0]['date_format']

    def append_description_to_case_by_name(
            self,
            cursor,
            uid,
            ids,
            case_name,
            append_description,
            section,
            category,
            context=None
    ):
        """
        Append description to a case and open it. Create new one if there is \
        case with this name. Do nothing if case already contains the \
        description.

        :param cursor: database cursor
        :param uid: user identifier
        :param ids: not used
        :param case_name: string with the name of the case
        :param append_description: string with description to be appended
        :param section: string with the section of the case
        :param category: string with the category of the case
        :param context: dictionary with the context
        :return: True if case opened, otherwise False
        """

        if not context:
            context = {}

        config_obj = self.pool.get('res.config')

        context_model = context.copy()
        model_id = False
        if context.get('model_id', False) and context.get('model', False):
            model_id = context['model_id']
        else:
            context_model.update({'model': False})

        today = datetime.today().strftime(
            self.date_format(cursor, uid, [], context=context)
        )

        search_description = ':\n{}'.format(append_description)
        append_description = '{}{}'.format(today, search_description)

        # Check if we have to append or create looking at config
        append = int(config_obj.get(cursor, uid, 'tg_crm_append'))
        if append:
            case_ids = self.search(
                cursor,
                uid,
                [('name', '=', case_name)],
                limit=1,
                order='create_date desc',
                context=context
            )
        else:
            case_ids = []

        if case_ids:
            description = self.read(
                cursor,
                uid,
                case_ids,
                ['description'],
                context=context
            )[0]['description'] or ''

            if search_description in description:
                return False
            else:
                description = '{}\n{}'.format(
                    description,
                    append_description
                )
                self.write(
                    cursor,
                    uid,
                    case_ids,
                    {'description': description},
                    context=context
                )
        else:
            case_vals = {
                'description': append_description,
                'name': case_name,
            }
            if model_id:
                case_ids = self.create_case_generic(
                    cursor, uid, [model_id], context=context_model,
                    extra_vals=case_vals, section=section, category=category
                )
            else:
                case_ids = self.create_case_generic(
                    cursor, uid, [], context=context_model,
                    extra_vals=case_vals, section=section, category=category
                )
        return self.case_open(cursor, uid, case_ids)


CrmCase()
