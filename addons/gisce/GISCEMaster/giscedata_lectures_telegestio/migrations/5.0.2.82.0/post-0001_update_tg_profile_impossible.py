# coding=utf-8

import logging


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')

    logger.info(
        'Set value of config_tg_profile_impossible of res.config to 55000')

    query_rename = '''
        UPDATE res_config SET value = 55000 where name ='tg_profile_impossible';
    '''
    cursor.execute(query_rename)


def down(cursor, installed_version):
    pass