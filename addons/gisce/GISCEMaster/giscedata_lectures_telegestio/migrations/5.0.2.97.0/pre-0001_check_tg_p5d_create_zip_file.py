# coding=utf-8

import logging


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')

    cursor.execute("SELECT id from res_config WHERE name = 'tg_p5d_create_zip_file'")
    res = cursor.fetchall()
    if res:
        cursor.execute("UPDATE res_config set name = 'OLD_tg_p5d_create_zip_file' WHERE name = 'tg_p5d_create_zip_file'")
        logger.info('Change conf var name of tg_p5d_create_zip_file to OLD_tg_p5d_create_zip_file')


def down(cursor, installed_version):
    pass

migrate = up
