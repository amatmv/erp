import netsvc
from oopgrade import DataMigration


def migrate(cr, installed_version):
    """
    Migrates manually sets tg_clean_serial_chars, tg_clean_left_zeroes_serial
    and tg_reactive_insert from xml data

    :param cr: Database cursor
    :param installed_version: Installed version of the module
    :return: None
    """

    xml_content = '''<?xml version="1.0" ?>
    <openerp>
        <data noupdate="1">
            <record model="res.config" id="config_tg_clean_serial_chars">
                <field name="name">tg_clean_serial_chars</field>
                <field name="value">5</field>
            </record>
            <record model="res.config" id="config_tg_clean_left_zeroes_serial">
                <field name="name">tg_clean_left_zeroes_serial</field>
                <field name="value">0</field>
            </record>
            <record model="res.config" id="config_tg_reactive_insert">
                <field name="name">tg_reactive_insert</field>
                <field name="value">always</field>
            </record>
        </data>
    </openerp>
    '''
    dm = DataMigration(
        xml_content, cr, 'giscedata_lectures_telegestio',
        search_params={'res.config': ['name']}
    )
    dm.migrate()

up = migrate
