# coding=utf-8

import logging


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')

    logger.info(
        'Set value of technology_type of giscedata.lectures.comptador to '
        'prime for all TG meters')

    cursor.execute('''update giscedata_lectures_comptador set technology_type='prime' where tg=True''')


def down(cursor, installed_version):
    pass

migrate = up
