"""
Update tg_reactive_insert configuration from '1' to 'always' and from '0' to \
    'zero'. A new option never doesn't insert reactive measures.
"""

import netsvc
import pooler



def migrate(cursor, installed_version):
    uid = 1

    pool = pooler.get_pool(cursor.dbname)
    res_config = pool.get('res.config')
    old_tg_reactive_insert = res_config.get(
        cursor,
        uid,
        'tg_reactive_insert'
    )
    migration_tg_reactive_insert = {
        '1': 'always',
        '0': 'zero'
    }
    if old_tg_reactive_insert in migration_tg_reactive_insert:
        new_tg_reactive_insert = \
            migration_tg_reactive_insert[old_tg_reactive_insert]
        res_config.set(
            cursor,
            uid,
            'tg_reactive_insert',
            new_tg_reactive_insert
        )
        logger = netsvc.Logger()
        message = 'Migrated tg_reactive_insert from {0} to {1}'.format(
            old_tg_reactive_insert,
            new_tg_reactive_insert
        )
        logger.notifyChannel('migration', netsvc.LOG_INFO, message)
