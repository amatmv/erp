from osv import osv, fields

class UpdateConnectivityTG(osv.osv_memory):
    """
        Wizard to update meter connectivity.
    """
    _name = 'wizard.update.connectivity.tg'



    def action_update_connectivity_fields(self, cursor, uid, ids, context=None):
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        meter_obj.update_meters_connectivity_fields(cursor, uid, context.get('active_ids'), context=context)


    _columns = {
        'meter_id': fields.many2many('giscedata.lectures.comptador', 'comptador_wizard', 'id', 'id', 'Meter',
                                     required=True)
    }

    _defaults = {
        'meter_id': lambda cursor, uid, ids, context: context.get(
            'active_ids')
    }


UpdateConnectivityTG()
