# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _
import time
from datetime import datetime


class WizardShowReadTG(osv.osv_memory):

    _name = 'wizard.show.read.tg'

    def action_show_read(self, cursor, uid, ids, context=None):

        if not context:
            context = {}
        tgmodel = context.get('tgmodel', 'tg.billing')

        wizard = self.browse(cursor, uid, ids[0], context=context)
        meter = wizard.meter_id
        #Build full name from serial if present
        meter_name = meter.build_name_tg()
        domain = [('name', '=', meter_name)]

        if tgmodel == 'tg.billing':
            view_name = 'TG Billings'
        elif tgmodel == 'tg.profile':
            view_name = 'TG Profiles'
        else:
            view_name = 'TM Reads'

        vals_view = {
                'name': view_name,
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': tgmodel,
                'limit': 80,
                'type': 'ir.actions.act_window',
                'domain': domain,
                }

        return vals_view

    _columns = {
        'meter_id': fields.many2one('giscedata.lectures.comptador',
                                    'Meter', required=True),
    }


WizardShowReadTG()


class WizardShowEventTG(osv.osv_memory):

    _name = 'wizard.show.event.tg'

    def action_show_event(self, cursor, uid, ids, context=None):

        if not context:
            context = {}
        tgmodel = context.get('tgmodel', 'tg.event')

        wizard = self.browse(cursor, uid, ids[0], context=context)
        meter = wizard.meter_id
        events = []
        ev_groups = ['ev_group_{}'.format(ev_g) for ev_g in range(1, 7)]
        for ev in ev_groups:
            if self.read(cursor, uid, ids[0], [ev])[0][ev] == 1:
                events.append(int(ev[-1]))
        #Build full name from serial if present
        meter_name = meter.build_name_tg()
        domain = [('name', '=', meter_name), ('event_group', 'in', events)]
        view_name = 'TG Events'

        vals_view = {
                'name': view_name,
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': tgmodel,
                'limit': 80,
                'type': 'ir.actions.act_window',
                'domain': domain,
                }

        return vals_view

    _columns = {
        'meter_id': fields.many2one('giscedata.lectures.comptador',
                                    'Meter', required=True),
        'ev_group_1': fields.boolean('Group 1'),
        'ev_group_2': fields.boolean('Group 2'),
        'ev_group_3': fields.boolean('Group 3'),
        'ev_group_4': fields.boolean('Group 4'),
        'ev_group_5': fields.boolean('Group 5'),
        'ev_group_6': fields.boolean('Group 6'),
    }

    _defaults = {
        'ev_group_1': lambda *a: True,
        'ev_group_2': lambda *a: True,
        'ev_group_3': lambda *a: True,
        'ev_group_4': lambda *a: True,
        'ev_group_5': lambda *a: True,
        'ev_group_6': lambda *a: False,
    }

WizardShowEventTG()
