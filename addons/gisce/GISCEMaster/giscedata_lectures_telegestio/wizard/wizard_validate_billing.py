# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _
from itertools import groupby


class WizardValidateBillingMeterTG(osv.osv_memory):

    _name = 'wizard.validate.billing'
    _inherit = 'wizard.validate.billing'

    def action_validate(self, cursor, uid, ids, context=None):
        '''extend validate billing manually to update
        last tg read in meter. tg last read always goes forward'''
        if not context:
            context = {}
        tgmodel = context.get('tgmodel', 'tg.billing')

        res = super(WizardValidateBillingMeterTG,
                     self).action_validate(cursor, uid, ids, context=context)

        model_obj = self.pool.get(tgmodel)
        if tgmodel == 'tg.billing':
            validate_obj = self.pool.get('tg.lectures.validate')
        else:
            validate_obj = self.pool.get('tg.profile.validate')
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        model_ids = context.get('active_ids', [])

        if tgmodel == 'tg.billing':
            meter_names = []
            for model_id in model_ids:
                model = model_obj.read(cursor, uid,
                                       [model_id], [])[0]
                #get meter active in date_end
                meter_id = meter_obj.get_meter(cursor, uid,
                                               model['name'],
                                               model['date_end'])
                if not meter_id:
                    continue
                #Update last valid reads dict
                meter_names.append(model['name'])
            validate_obj.update_last_valid_read(cursor, uid, meter_names,
                                                context=context)
        elif tgmodel == 'tg.profile':
            models = model_obj.read(cursor, uid,
                                    model_ids, ['name'])
            meter_names = list(set([x['name'] for x in models]))
            validate_obj.update_last_valid_profile(cursor, uid, meter_names,
                                                   context=context)
        return res

WizardValidateBillingMeterTG()
