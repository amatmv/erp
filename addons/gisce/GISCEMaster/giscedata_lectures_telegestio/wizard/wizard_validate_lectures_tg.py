# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _


class WizardValidateLecturesTG(osv.osv_memory):

    _name = 'wizard.validate.lectures.tg'

    def action_validate(self, cursor, uid, ids, context=None):

        meter_obj = self.pool.get('giscedata.lectures.comptador')

        if not context:
            context = {}

        wizard = self.browse(cursor, uid, ids[0], context=context)

        if wizard.type == 'profile':
            tg_validate_obj = self.pool.get('tg.profile.validate')
            meter_name = meter_obj.build_name_tg(cursor, uid,
                                                 wizard.meter_id.id,
                                                 context=context)
            tg_validate_obj.validate_profile(cursor, uid, [], [meter_name],
                                             context=context)
        elif wizard.type == 'billing':
            tg_validate_obj = self.pool.get('tg.lectures.validate')
            meter_names = []
            if wizard.meter_id:
                meter_name = meter_obj.build_name_tg(cursor, uid,
                                                     wizard.meter_id.id,
                                                     context=context)
                meter_names = [meter_name]
                context.update({'tg_check_unreaded': False})
            tg_validate_obj.validate_billing(cursor, uid, [], meter_names,
                                             context=context)

        return {}

    def _default_meter_id(self, cursor, uid, context=None):
        if context is None:
            context = {}

        if context.get('from_meter', False):
            return context.get('active_id', False)
        else:
            return False

    _columns = {
        'type': fields.selection([('profile', 'TG Profiles'),
                                  ('billing', 'TG Billing')], 'Type',
                                 required=True),
        'meter_id': fields.many2one('giscedata.lectures.comptador', 'Meter',
                                    help="Blank for all meters"),
    }

    _defaults = {
        'meter_id': _default_meter_id,
    }

WizardValidateLecturesTG()
