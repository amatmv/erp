# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _
import time
from datetime import datetime


class WizardExportComerP5D(osv.osv_memory):

    _name = 'wizard.export.comer.p5d'

    def _default_info(self, cursor, uid, context=None):

        res_obj = self.pool.get('res.config')
        filedir = res_obj.get(
            cursor, uid, 'tg_profile_temp_filedir', '/tmp/curves'
        )

        text = _(u"Genera el CCH_VAL en format P5D de la comercialitzadora "
                 u"seleccionada. Si es seleccionen Totes, es generen els "
                 u"fitxers a la carpeta per defecte '{0}'.").format(filedir)

        return text

    def _get_comers(self, cursor, uid, context=None):
        modcon_obj = self.pool.get('giscedata.polissa.modcontractual')
        comer_obj = self.pool.get('res.partner')

        comer_ids = modcon_obj.get_tg_comers(cursor, uid)
        comer_vals = comer_obj.read(
            cursor, uid, comer_ids, ['name', 'ref']
        )

        res = [(0, '[all] Totes')]
        for comer in comer_vals:
            sel_name = '[{0}] {1}'.format(
                comer['ref'], comer['name']
            )
            item = (comer['id'], sel_name)
            res.append(item)

        return res

    def action_export_p5d_file(self, cursor, uid, ids, context=None):

        if not context:
            context = {}

        wizard = self.browse(cursor, uid, ids[0])
        comer_id = wizard.comer_id
        set_last_cch = wizard.set_last_cch or False

        modcon_obj = self.pool.get('giscedata.polissa.modcontractual')

        res_obj = self.pool.get('res.config')
        filedir = res_obj.get(
            cursor, uid, 'tg_profile_temp_filedir', '/tmp/curves'
        )

        ctx = context.copy()
        ctx.update({'set_last_cch': set_last_cch})

        if not comer_id:
            text = _("S'ha encuat la generació del fitxer P5D per totes les "
                     "comercialitzadores que tenen pòlisses telegestionades")
            modcon_obj.cronjob_generate_cch_val_curves(
                cursor,
                uid,
                [],
                filedir=filedir,
                context=ctx,
                upload=wizard.upload
            )
        else:
            comer_obj = self.pool.get('res.partner')
            comer_vals = comer_obj.read(cursor, uid, comer_id, ['name', 'ref'])
            text = _("S'ha encuat la generació del fitxer P5D per la "
                     "comercialitzadora "
                     "[{0}] {1}").format(comer_vals['ref'], comer_vals['name'])
            modcon_obj.get_comer_cch_val_async(
                cursor,
                uid,
                comer_id,
                filedir=filedir,
                context=ctx,
                upload=wizard.upload
            )

        wizard.write({'state': 'end', 'info': text})

    def onchange_upload(self, cursor, uid, ids, upload):

        if upload:
            set_last_cch = True
        else:
            set_last_cch = False

        return {'value': {'set_last_cch': set_last_cch}}

    _columns = {
        'state': fields.char('State', size=16),
        'info': fields.text('Info'),
        'comer_id': fields.selection(_get_comers, 'Comercialitzadora'),
        'upload': fields.boolean(
            'Upload files to SFTP CCH server',
            help='Upload the generated files to SFTP CCH server'
        ),
        'set_last_cch': fields.boolean('Marca Última curva',
                                       help=u"Marca al CUPS la última "
                                            u"curva generada per començar "
                                            u"a partir d'aquesta en la "
                                            u"següent generació del P5D"),
        'name': fields.char('Nom Fitxer', size=256),
        'fitxer': fields.binary('Nom'),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'info': _default_info,
        'comer_id': lambda *a: 0,
        'upload': lambda *a: False,
        'set_last_cch': lambda *a: False,
    }

WizardExportComerP5D()
