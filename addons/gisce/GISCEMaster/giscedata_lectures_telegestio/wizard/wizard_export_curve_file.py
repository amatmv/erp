# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
import csv
import StringIO
import base64
import zipfile
import bz2
from giscedata_lectures_telegestio.lectures import CURVE_TYPE_DICT

COMPRESS_SEL = [('no', _('No')),
                ('zip', _('en ZIP')),
                ('bz2', _('en BZIP2'))]


class GiscedataExportCurvaFileWizard(osv.osv_memory):
    """Wizard pel switching
    """
    _name = 'giscedata.export.curve.file.wizard'

    def get_all_tg_meters(self, cursor, uid):
        '''Returns all active TG meters '''
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        meter_ids = meter_obj.search(cursor, uid, [('tg', '!=', False)])

        return meter_ids

    def _default_data_inici(self, cursor, uid, context=None):
        if not context:
            context = {}

        mes_anterior = datetime.today() + relativedelta(months=-1)
        return mes_anterior.strftime("%Y-%m-01")

    def _default_info(self, cursor, uid, context=None):
        if not context:
            context = {}

        meter_obj = self.pool.get('giscedata.lectures.comptador')

        txt = _(u'Exportació de curves des de perfils de Telegestió.')
        meter_ids = context.get('active_ids', [])
        if context.get('get_all_meters', False):
            meter_ids = self.get_all_tg_meters(cursor, uid)
            txt += _(u"\n S'exportaran totes les curves dels comptadors de "
                     u"Telegestió: %s comptadors") % len(meter_ids)
        elif len(meter_ids) > 1:
            # Tenim més d'un comptador
            tgname1 = meter_obj.build_name_tg(cursor, uid, meter_ids[0])
            tgname2 = meter_obj.build_name_tg(cursor, uid, meter_ids[-1])
            txt += (_(u"\n S'exportaran %d comptadors: "
                      u"'%s' a '%s'") % (len(meter_ids), tgname1, tgname2))

        return txt

    def action_export_curve_file(self, cursor, uid, ids, context=None):
        if not context:
            context = {}

        meter_obj = self.pool.get('giscedata.lectures.comptador')
        meter_ids = context.get('active_ids', False)
        all_meters = context.get('get_all_meters', False)
        multi = all_meters or (len(meter_ids) > 1)

        wiz = self.browse(cursor, uid, ids[0])

        if not (wiz.data_inici and wiz.data_fi):
            raise osv.except_osv(_(u'Dates invàlides'),
                                 _(u'Has de seleccionar la data de inici i la '
                                   u'data fi'))

        curve_types = meter_obj.get_curve_type_dict(cursor, uid, context)

        data_inici = datetime.strptime(wiz.data_inici, '%Y-%m-%d')
        data_fi = datetime.strptime(wiz.data_fi, '%Y-%m-%d')
        cch_type = wiz.curve_type
        extension = wiz.curve_extension or 'csv'
        compress = wiz.compress
        zip_file = compress == 'zip'
        bzip_file = compress == 'bz2'

        if all_meters:
            meter_ids = self.get_all_tg_meters(cursor, uid)
            tgname = 'all'
        else:
            if not meter_ids[0]:
                raise osv.except_osv(_(u'Comptador no trobat'),
                                     _(u"No s'ha trobat el comptador"))
            tgname = meter_obj.build_name_tg(cursor, uid, meter_ids[0])
            if multi:
                tgname += '-%s' % meter_obj.build_name_tg(cursor, uid,
                                                          meter_ids[-1])

        ctx = context.copy()
        ctx.update({'extension': extension})
        filename = meter_obj.get_cch_filename(
            cursor,
            uid,
            meter_ids,
            cch_type,
            0,
            cch_date=data_fi,
            context=ctx
        )

        curves = meter_obj.get_cch(cursor, uid, meter_ids, cch_type,
                                   data_inici.strftime("%Y-%m-%d"),
                                   data_fi.strftime("%Y-%m-%d"),
                                   context=context)

        output = StringIO.StringIO()
        zip_io = StringIO.StringIO()
        if cch_type in ('cch_cons',) and extension == 'xls':
            meter_obj.get_as_xls(cursor, uid, output, curves, tgname, context)
        else:
            writer = csv.writer(output, delimiter=';')

            for curva in curves.values():
                for line in curva:
                    writer.writerow(line)

        if compress != 'no':
            zip_filename = filename
            filename = '{0}.{1}'.format(filename, compress)
            if zip_file:
                zip_fp = zipfile.ZipFile(zip_io, 'w',
                                         compression=zipfile.ZIP_DEFLATED)
                zip_fp.writestr(zip_filename, output.getvalue())

                output.close()
                zip_fp.close()
                mfile = base64.b64encode(zip_io.getvalue())
            elif bzip_file:
                bzdata = bz2.compress(output.getvalue())
                mfile = base64.b64encode(bzdata)

        else:
            mfile = base64.b64encode(output.getvalue())
            output.close()

        data_inici_str = '{0} 01:00:00'.format(data_inici.strftime("%Y-%m-%d"))
        # Always day after hour 00
        data_fi_curva = data_fi + timedelta(days=1)
        data_fi_str = '{0} 00:00:00'.format(data_fi_curva.strftime("%Y-%m-%d"))

        if multi:
            info_tmpl = _(u"Exportades %d línies de %d comptadors desde "
                          u"%s (inclosa) fins a %s (inclosa) en format "
                          u"%s")

            num_linies = sum([len(c) for c in curves.values()])
            info = info_tmpl % (num_linies, len(meter_ids), data_inici_str,
                                data_fi_str, curve_types[wiz.curve_type][2])
        else:
            info_tmpl = _(u"Exportades %d línies del comptador %s desde "
                          u"%s (inclosa) fins a %s (inclosa) en format "
                          u"%s")
            info = info_tmpl % (len(curves.values()[0]), tgname, data_inici_str,
                                data_fi_str, curve_types[wiz.curve_type][2])

        wiz.write({'file': mfile, 'name': filename, 'state': 'done',
                   'info': info}, context=context)

    def _get_curve_formats(self, cursor, uid, context=None):
        ''' Returns Available Curve types'''
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        curve_types = meter_obj.get_curve_type_dict(cursor, uid, context)

        return [(k, v[2]) for k, v in curve_types.items()]

    _columns = {
        'state': fields.char('State', size=16),
        'data_inici': fields.date('Data inici'),
        'data_fi': fields.date('Data Fi'),
        'curve_type': fields.selection(_get_curve_formats, 'Format Fitxer',),
        'curve_extension': fields.selection([('csv', 'CSV'),
                                             ('xls', 'Excel (xls)')],
                                             'Extensió'),
        'compress': fields.selection(COMPRESS_SEL, 'Comprimeix',
                                     help=u"Comprimeix el fitxer resultant en "
                                          u"el format especificat"),
        'name': fields.char('Nom', size=256),
        'file': fields.binary('Fitxer'),
        'info': fields.text('Info')
    }

    _defaults = {
        'state': lambda *a: 'init',
        'info': _default_info,
        'curve_type': lambda *a: 'cch_val',
        'curve_extension': lambda *a: 'csv',
        'compress': lambda *a: 'no',
        #'data_inici': _default_data_inici,
    }

GiscedataExportCurvaFileWizard()
