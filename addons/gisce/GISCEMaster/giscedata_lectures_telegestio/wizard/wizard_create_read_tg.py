# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _
import time
from datetime import datetime


class WizardCreateReadTG(osv.osv_memory):

    _name = 'wizard.create.read.tg'

    def action_create_read(self, cursor, uid, ids, context=None):

        if not context:
            context = {}
        #Search for language to get date format
        str_lang = context.get('lang', 'es_ES')
        lang_obj = self.pool.get('res.lang')
        date_format = lang_obj.search_reader(cursor, uid,
                                         [('code', '=', str_lang)],
                                         ['date_format'])[0]['date_format']

        meter_obj = self.pool.get('giscedata.lectures.comptador')
        wizard = self.browse(cursor, uid, ids[0], context=context)
        context.update({'date_limit': wizard.date_limit})
        if wizard.force:
            context.update({'force_tg_read': True})
        res = meter_obj.create_read_from_tg(cursor, uid, [wizard.meter_id.id],
                                      context=context)
        notes = ''
        if not res[wizard.meter_id.id]['date_read']:
            notes = _(u"There is not a valid measure for this meter")

        else:
            date_read = res[wizard.meter_id.id]['date_read']
            formatted_date = datetime.strftime(datetime.strptime(date_read,
                                                                 '%Y-%m-%d'),
                                               date_format)
            if res[wizard.meter_id.id]['existing']:
                notes = (_(u"No read created!!\n"
                           u"Another existing read on "
                           u"date %s or after") % formatted_date)
            else:
                notes = (_(u"Read created succesfully!!\n"
                           u"Read date is %s") % formatted_date)

        wizard.write({'state': 'end',
                      'notes': notes})

    _columns = {
        'meter_id': fields.many2one('giscedata.lectures.comptador',
                                    'Meter', required=True),
        'date_limit': fields.date('Date limit', required=True,
                                  help=u"Date limit for searching a "
                                       u"read for this meter"),
        'force': fields.boolean('Force',
                                help=u"Force creation of read even when "
                                     u"a later read exists"),
        'notes': fields.text('Notes', readonly=True),
        'state': fields.selection([('init', 'Init'),
                                   ('fail', 'Fail'),
                                   ('end', 'End')],
                                  'State'),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'force': lambda *a: False,
        'date_limit': lambda *a: datetime.strftime(datetime.now(), '%Y-%m-%d'),
    }

WizardCreateReadTG()
