# -*- coding: utf-8 -*-
from __future__ import absolute_import
from osv import osv, fields
import netsvc
import os
from giscedata_polissa.giscedata_polissa import CONTRACT_IGNORED_STATES
from datetime import datetime, timedelta
from tools.translate import _
from oorq.decorators import job
import bz2
from base64 import b64encode
from base_extended.base_extended import MultiprocessBackground
from zipfile import ZipFile
import tempfile


class GiscedataPolissa(osv.osv):

    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    _columns = {
            'reactive_conf': fields.selection(
                (
                    ('always', 'Always generate reactive charge'),
                    ('zero', 'Reactive charge always 0'),
                    ('never', 'Without reactive charge'),
                    ('global', 'Use config variable'),
                ), 'Generate reactive charge', required=False,
                readonly=True, states={'esborrany': [('readonly', False)],
                                       'modcontractual': [('readonly', False)],
                                       'validar': [('readonly', False)]},
                help="Allow selection to generate reactive charge"),
        }

    _defaults = {
        'reactive_conf': lambda *a: 'global'
    }

GiscedataPolissa()


class GiscedataPolissaModcontractual(osv.osv):
    """Contract extension to generate P5D (CCH_VAL) curves.
    """
    _name = 'giscedata.polissa.modcontractual'
    _inherit = 'giscedata.polissa.modcontractual'

    @job(queue='tg_validate', timeout=6000)
    def get_comer_cch_val_async(
            self,
            cursor,
            uid,
            comer_id,
            filedir=None,
            context=None,
            upload=False
    ):
        self.get_comer_cch_val(cursor, uid, comer_id, filedir=filedir,
                               context=context, upload=upload)

    def get_comer_cch_val(
            self,
            cursor,
            uid,
            comer_id,
            filedir=None,
            context=None,
            upload=False
    ):
        """
        Writes a CCH_VAL curve "TG with CCH" contracts for comer selected by \
            comer_id only

        :param cursor: database cursor
        :param uid: user identifier
        :param comer_id: marketer identifier
        :param filedir: path to save files
        :param context: dictionary of context
        :param upload: True to upload the file to server, False otherwise.
        :return: None
        """
        logger = netsvc.Logger()
        if not context:
            context = {}

        if isinstance(comer_id, (list, tuple)):
            comer_id = comer_id[0]

        if not filedir:
            res_obj = self.pool.get('res.config')
            filedir = res_obj.get(
                cursor,
                uid,
                'tg_profile_temp_filedir',
                '/tmp/curves'
            )

        meter_obj = self.pool.get('giscedata.lectures.comptador')
        cups_obj = self.pool.get('giscedata.cups.ps')
        res_partner = self.pool.get('res.partner')
        ir_attachment = self.pool.get('ir.attachment')
        tg_cch_server = self.pool.get('tg.cch.server')

        ctx = context.copy()
        ctx.update({'active_test': False})
        contract_ids = self.search(
            cursor, uid,
            [('comercialitzadora', '=', comer_id), ('tg', '=', '1')],
            context=ctx
        )

        contract_vals = self.read(
            cursor, uid, contract_ids,
            ['cups', 'comercialitzadora']
        )
        cups_ids = list(set([c['cups'][0]for c in contract_vals]))

        marketer = res_partner.read(
            cursor,
            uid,
            comer_id,
            ['name','ref'],
            context=context
        )

        logger.notifyChannel(
            'exporta_curves_tg', netsvc.LOG_INFO,
            _('Exporting CCH VAL for comer "{0}"').format(marketer['name']))

        version = 0
        unique_filename = False
        while not unique_filename:
            filename = meter_obj.get_cch_filename(
                cursor,
                uid,
                None,
                'cch_val',
                version,
                cch_date=None,
                context=context,
                default_marketer=marketer['ref']
            )
            filename += '.bz2'
            if ir_attachment.search(
                    cursor,
                    uid,
                    [
                        ('name', '=', filename),
                        ('res_model', '=', 'wizard.export.comer.p5d')
                    ]
            ):
                version += 1
            else:
                unique_filename = True

        file_path = '{0}/{1}'.format(filedir, filename)
        has_data = False
        with bz2.BZ2File(file_path, mode='w') as bz2_file:
            for cups_id in cups_ids:
                p5d = cups_obj.get_CCH(cursor, uid, cups_id,
                                       cch_type='cch_val', context=ctx)
                if p5d:
                    has_data = True
                    bz2_file.write(p5d)

        if has_data:
            output = open(file_path, 'r')
            myfile = output.read()
            attachment_values = {
                'name': filename,
                'res_model': 'wizard.export.comer.p5d',
                'datas': b64encode(myfile),
                'marketer': comer_id,
                'type': 'p5d'
            }
            output.close()
            ir_attachment_id = ir_attachment.create(
                cursor,
                uid,
                attachment_values,
                context=context
            )

            if upload:
                sftp_obj = self.pool.get('tg.cch.server')
                ftp_obj = self.pool.get('giscedata.ftp.connections')
                sftp_server = sftp_obj.search(cursor, uid, [])
                ftp_server = ftp_obj.search(cursor, uid, [('category_type.code', '=', 'TGM')])
                if sftp_server:
                    # SFTP Mode
                    ir_attachment.upload(
                        cursor,
                        uid,
                        ir_attachment_id,
                        context=context
                    )
                elif ftp_server:
                    # FTP Mode
                    server = self.pool.get('tg.cch.server')
                    server.ftp_upload(
                        cursor, uid, 'p5d', marketer['ref'], filename, file_path, ir_attachment_id, context=context
                    )

            return filename, myfile
        return filename, False

    def get_tg_comers(self, cursor, uid, context=None):
        contract_obj = self.pool.get('giscedata.polissa')

        contract_ids = contract_obj.search(
            cursor, uid, [
                ('tg', '=', '1'), ('state', 'not in', CONTRACT_IGNORED_STATES)
            ], context={'active_test': False}
        )

        contract_vals = contract_obj.read(
            cursor, uid, contract_ids, ['comercialitzadora']
        )

        comer_ids = list(set(c['comercialitzadora'][0] for c in contract_vals))

        return comer_ids

    @MultiprocessBackground.background(queue='low')
    def cronjob_generate_cch_val_curves(
            self,
            cursor,
            uid,
            ids,
            filedir=None,
            context=None,
            upload=False
    ):
        """
        Writes all P5D files

        :param cursor: database cursor
        :param uid: user identifier
        :param ids: not used
        :param filedir: string with the path to save the P5D files. If it is \
            None the configuration tg_profile_temp_filedir is used
        :param context: dictionary with the context
        :param upload: True uploads P5D files to SFTP CCH server, otherwise \
            False.
        :return: None
        """

        if not context:
            context = {}
        ir_attachment = self.pool.get('ir.attachment')
        res_config = self.pool.get('res.config')
        comer_obj = self.pool.get('res.partner')

        if not filedir:
            filedir = res_config.get(
                cursor,
                uid,
                'tg_profile_temp_filedir',
                '/tmp/curves'
            )

        context.setdefault('lang', 'es_ES')
        today = datetime.today().strftime("%Y%m%d")
        if res_config.get(cursor, uid, 'tg_p5d_create_zip_file', '0') == '1':
            create_p5d_zip = True
        else:
            create_p5d_zip = False
        if create_p5d_zip:
            p5ds_file = '{}/P5D_{}.zip'.format(filedir, today)
            zip_p5ds_file = ZipFile(p5ds_file, 'w')

        comer_ids = self.get_tg_comers(cursor, uid, context=context)

        for comer_id in comer_ids:
            ref = comer_obj.read(cursor, uid, comer_id, ['ref'])['ref']
            dest_dir = '{0}/{1}'.format(filedir, ref)
            if not os.path.exists(dest_dir):
                os.makedirs(dest_dir)

            filename, content = self.get_comer_cch_val(
                cursor,
                uid,
                comer_id,
                filedir=dest_dir,
                context=context,
                upload=upload
            )

            if create_p5d_zip and content:
                dcontent = bz2.decompress(content)
                arc_filename = filename.replace('.bz2', '')
                filetext = tempfile.NamedTemporaryFile(delete=False)
                filetext.write(dcontent)
                filetext.close()
                zip_p5ds_file.write(filetext.name, arcname=arc_filename)

        if create_p5d_zip:
            zip_p5ds_file.close()
            with open(p5ds_file, 'rb') as myzip:
                content = myzip.read()
            attachment_values = {
                'name': 'P5D_{0}.zip'.format(today),
                'res_model': 'wizard.export.comer.p5d',
                'datas': b64encode(content),
                'type': 'p5d'
            }
            ir_attachment.create(
                cursor,
                uid,
                attachment_values,
                context=context
            )

        return True


GiscedataPolissaModcontractual()
