# -*- coding: utf-8 -*-
from osv import osv, fields
import pooler

class giscedata_curva_horaria_adapter(osv.osv):
  """
  Aquesta classe modela els diferents tipus de fitxers que generen els 
  comptadors.
  :column name: nom identificatiu del comptador o del fitxer
  :column separator: caràcter separador de columnes al CSV
  :column skip_count: nombre de files a saltar-se del CSV
  :column tipus_lectura: a triar entre 'a' i 'i' (acumulada i incremental)
  :column camp_activa: nom del camp que conté la lectura d'E.Activa
  :column camp_reactiva: nom del camp que conté la lectura d'E.Reactiva
  :column camps: FK cap al mapeig de camps entre CSV i BD
  """

  def do_row(self, cr, uid, id, row):
    """
    Processa la línia `row` amb els mapejos corresponents als camps `fields`.
    :param id: el id de l'adapter que es fa servir
    :param row: la fila del CSV
    :return: un diccionari amb els elements de `fields` com a claus.
    """
    vals = {}
    adapter = self.browse(cr, uid, id)
    for c in adapter.camps:
      d = {'input': row[c.column]}
      exec c.f in d
      vals[c.name] = d['output']
    return vals

  _name = 'giscedata.curva.horaria.adapter'
  _columns = {
    'name': fields.char('Model de fitxer', size=40),
    'separator': fields.char('Separador', size=1),
    'skip_count': fields.integer('skip_count'),
    'tipus_lectura': fields.char('Tipus de lectura', size=1),
    'factor_unitats': fields.integer('Factor de multiplicació d\'unitats'),
    'camp_activa_in': fields.char('Camp de la lectura d Activa d entrada', size=32),
    'camp_reactiva_in': fields.char('Camp de la lectura de Reactiva d entrada', size=32),
    'camp_activa_out': fields.char('Camp de la lectura d Activa de sortida', size=32),
    'camp_reactiva_out': fields.char('Camp de la lectura de Reactiva de sortida', size=32),
    'camps': fields.one2many('giscedata.curva.horaria.adapter.field', 'adapter', 'Camps')
  }

giscedata_curva_horaria_adapter()

class giscedata_curva_horaria_adapter_field(osv.osv):
  """
  Aquesta classe modela els mapeig entre camps de CSV i de objecte a la BD.
  La funció f ha de ser una funció amb un sol argument d'entrada, que serà
  el valor del camp al CSV. Aquesta, retornarà el valor que es prendrà com 
  a vàlid per guardar a la BD.
  :column name: nom del camp a la base de dades
  :column column: índex de la columna del CSV 
  :column f: codi python que s'avaluarà amb exec i amb un diccionari de context amb el valor de la columa del csv com a valor de la clau 'input'. El resultat haurà de ser gravat a la variable 'output' dins el codi python, i això farà que estigui disponible al diccionari de contexte a la clau 'output'.
  """
  _name = 'giscedata.curva.horaria.adapter.field'
  _columns = {
    'name': fields.char('Nom del camp a la BD', size=40),
    'column': fields.integer('Índex del camp al CSV'),
    'f': fields.text('Funció transformadora'),
    'adapter': fields.many2one('giscedata.curva.horaria.adapter', 'Adapter')
  }

giscedata_curva_horaria_adapter_field()

class giscedata_curva_horaria(osv.osv):
  _name = 'giscedata.curva.horaria'
  _description = 'Curves horaries'

  _columns = {
    'comptador': fields.many2one('giscedata.lectures.comptador', 'Comptador', readonly=True),
    'direccion_enlace': fields.char('Dirección enlace', size=5, readonly=True),
    'punto_medida': fields.char('Punto de medida', size=5, readonly=True),
    'modelo': fields.char('Modelo', size=32, readonly=True),
    'submodelo': fields.char('SubModelo', size=32, readonly=True),
    'num_serie': fields.char('Número de serie', size=64, readonly=True),
    'data_inici': fields.datetime('Data Inici', readonly=True),
    'data_final': fields.datetime('Data Final', readonly=True),

    'linies': fields.one2many('giscedata.curva.horaria.linia', 'curva','Línies'),
  }

  def name_get(self, cr, uid, ids, context={}):
    if not len(ids):
      return []
    res = []
    for f in self.read(cr, uid, ids, ['data_inici', 'data_final']):
      res.append((f['id'], "Des de %s a %s" % (f['data_inici'], f['data_final'])))
    return res

giscedata_curva_horaria()

class giscedata_lectures_comptador(osv.osv):
  _name = 'giscedata.lectures.comptador'
  _inherit = 'giscedata.lectures.comptador'
  _columns = {
    'curvas': fields.one2many('giscedata.curva.horaria', 'comptador', 'Curvas horarias'),
  }

giscedata_lectures_comptador()

class giscedata_curva_horaria_linia(osv.osv):
  def _get_data_lectura(self, data):
    """
    Mètode per retornar la data de lectura corresponent a una data donada. El que es
    retorna és la data de l'últim dia de mes.
    :param data: data inicial
    """
    import calendar
    from datetime import date
    anny, mes, dia = map(int, str.split(data, '-'))
    dsetmana, ndies = calendar.monthrange(anny, mes)
    return date(anny, mes, ndies).isoformat()

  def _get_dies_canvi_estacio(self, anny):
    """
    Mètode per retornar els dies de canvi d'hora/estació de l'any `anny`.
    :param anny: any del què volem saber els dies de canvi d'estació
    """
    import calendar
    from datetime import date
    _mesos_canvi = [3, 10] # això és sempre així, ho podem escriure al codi tranquilament
    res = []
    aux = 0
    anny = int(anny)
    for i in range(0,len(_mesos_canvi)):
      t = calendar.monthrange(anny, _mesos_canvi[i])
      for d in range(1, t[1]+1):
        if calendar.weekday(anny, _mesos_canvi[i], d) == 6:
          aux = d
      res.append(date(anny, _mesos_canvi[i], aux).isoformat())
    return res

  def _get_reader(self, file, delimitador):
    """
    Mètode per obtenir un reader de CSV donats el fitxer i el text delimitador.
    Ho posem en un mètode apart per si es canvia la manera d'obtenir el fitxer.
    :param file: fitxer CSV en format Base64
    :param delimitador: text que delimita els camps al CSV
    """
    import base64
    import csv
    import tempfile
    f = tempfile.NamedTemporaryFile()
    f.write(base64.b64decode(file))
    f.file.flush()
    if delimitador == 'T':
      reader = csv.reader(open(f.name, "rUb"), delimiter='\t')
    else:
      reader = csv.reader(open(f.name, "rUb"), delimiter=delimitador)
    f.close()
    return reader

  def _format_data(self, data, iformat, oformat):
    import re
    # obtenir el separador que es fa servir a iformat
    m = re.search('([/.-])', iformat)
    if not m:
      print("No s'ha trobat un caràcter separador, retornant la data tal qual.")
      return data
    isep = iformat[m.start()]
    # convertim el patró de la data a dict
    iformat_dict = str.split(iformat, isep)
    # convertim la data a dict
    data_dict = str.split(data, isep)
    odata_dict = {}
    # guardem la data en un dict amb les claus segons el patró
    # p.e.: odata_dict['DD'] = 10, odata_dict['MM'] = 02, odata_dict['YYYY'] = 2009
    for k in range(0, len(iformat_dict)):
      odata_dict[iformat_dict[k]] = data_dict[k]
    m = re.search(('([/.-])'), oformat)
    osep = oformat[m.start()]
    oformat_dict = str.split(oformat, osep)
    odata = []
    for k in oformat_dict:
      odata.append(odata_dict[k].zfill(len(k)))
    return osep.join(map(str, odata))

  def importar(self, cr, uid, file, model_comptador, id_comptador, perdues):
    """
    Importa els continguts del fitxer `file` a la taula `giscedata_curva_horaria_linia`.
    :param file: el fitxer CSV en format Base64
    :param model_comptador: nom del model de mappings
    :param id_comptador: id del comptador al que s'associen les lectures
    """
    from datetime import date
    # creem l'adapter
    adapter_obj = self.pool.get('giscedata.curva.horaria.adapter')
    adapter_ids = adapter_obj.search(cr, uid, [('id','=',model_comptador)])
    adapter = adapter_obj.browse(cr, uid, adapter_ids[0])

    reader = self._get_reader(file, adapter.separator)
    _x = 0
    hora = 0
    hora_anterior = -1
    data_anterior = ''
    estacio = -1
    tarifa = self.pool.get('giscedata.lectures.comptador').browse(cr, uid, id_comptador).tarifa_activa
    # obtenim els periodes de la tarifa
    cr.execute("select id,tipus from giscedata_polissa_tarifa_periodes where tarifa = %s and tipus = 'te'", (tarifa,))
    suma_energia = {}
    for r in cr.dictfetchall():
      suma_energia[r['id']] = {'a': 0, 'r': 0}

    _re = False
    polissa = None
    polissa_obj = self.pool.get('giscedata.polissa')
    polissa_ids = polissa_obj.search(cr, uid, [('n_comptador','=',id_comptador)])
    if len(polissa_ids):
      polissa = polissa_obj.browse(cr, uid, polissa_ids[0])

    if polissa and polissa.cups.blockname.code == 2:
      _re = True

    if _re:
      camp_activa = adapter.camp_activa_out
      camp_reactiva = adapter.camp_reactiva_out
    else:
      camp_activa = adapter.camp_activa_in
      camp_reactiva = adapter.camp_reactiva_in

    # creem un giscedata_curva_horaria
    numserie = self.pool.get('giscedata.lectures.comptador').browse(cr, uid, id_comptador).name.name
    ch_id = self.pool.get('giscedata.curva.horaria').create(cr, uid, {'comptador': id_comptador, 'num_serie': numserie, 'modelo': '__modelo__', 'submodelo': '__submodelo__', 'punto_medida': '__punto_medida__', 'direccion_enlace': '__direccion_enlace__'})

    periodes = []
    data_inici = ''
    for row in reader:
      if _x >= adapter.skip_count: 
        # -- ja tenim el CSV gravat, inserim a la BD
        # guardem primer a la taula que és una còpia del CSV
        
        vals = adapter.do_row(cr, uid, adapter.id, row)
        vals['curva'] = ch_id
        self.create(cr, uid, vals)
        # ara guardem a la taula de lectures
        # segons: `http://wiki.gisce.local/wiki/show/Horaris+periodes+Perfilació`_
        data = vals['name']
        hora = vals['hora']
        if _x == adapter.skip_count:
          data_inici = "%s %s" % (data, hora)
        # obtenir estacio inicial i primera data
        mes = data[-2:]
        if estacio == -1:
          if int(mes) in [11,12,1,2,3]: 
            estacio = 0
          else:
            estacio = 1 
          data_inicial = data

        # mirem si és un festiu per triar el model
        festius = self.pool.get('giscedata.lectures.perfils.dfestius')
        if festius.is_festiu(cr, uid, data):
          model = 'giscedata_lectures_perfils_hfestius'
        else:
          model = 'giscedata_lectures_perfils_hlaborables'

        # mirem si a l'hora que estem mirant, hi ha canvi d'estació
        if data in self._get_dies_canvi_estacio(data.split('-')[0]):
          if data_anterior == data and hora == hora_anterior:
            # data i hora duplicades! canvi d'estació!
            if estacio == 0:
              estacio = 1
            else:
              estacio = 0

        # busquem el periode 
        cr.execute("select estacio,periode from " + model + " where dinici <= %s and dfinal >= %s and hinici <= %s and hfinal > %s and periode in ("+",".join(map(str, suma_energia.keys()))+") and estacio = %s", (data, data, hora, hora, estacio))
        # i ara el periode concret per la fila del CSV  que estem tractant
        for p in cr.dictfetchall():
          periode = p['periode']
        
        # calculem la lectura contemplant les pèrdues
        vals[camp_activa] = (1+float(perdues/100)) * int(vals[camp_activa])
        vals[camp_reactiva] = (1+float(perdues/100)) * int(vals[camp_reactiva])
        # afegim a l'acumulat l'energia de la fila actual
        if adapter.tipus_lectura == 'i':
          suma_energia[periode]['a'] += adapter.factor_unitats * int(vals[camp_activa])
          suma_energia[periode]['r'] += adapter.factor_unitats * int(vals[camp_reactiva])
        elif adapter.tipus_lectura == 'a':
          suma_energia[periode]['a'] = adapter.factor_unitats * int(vals[camp_activa])
          suma_energia[periode]['r'] = adapter.factor_unitats * int(vals[camp_reactiva])
        # ens guardem la data i hora que acabem de llegir
        data_anterior = data
        hora_anterior = hora
        
      _x = _x + 1
    data_final = data
    ll = self.pool.get('giscedata.lectures.lectura')
    for periode in suma_energia.keys():
      vals_la = {}
      vals_la['periode'] = periode
      vals_la['comptador'] = id_comptador
      vals_la['name'] = "%s 23:59:59" % self._get_data_lectura(data)
      vals_la['tipus'] = 'A'
      # TODO obtenir lectura anterior i sumarla aquí sii adapter.tipus_lectura = 'i'
      vals_la['lectura'] = suma_energia[periode]['a']
      vals_la['observacions'] = "Lecturas importadas de contador"
      ll.create(cr, uid, vals_la)
      vals_lr = {}
      vals_lr['periode'] = periode
      vals_lr['comptador'] = id_comptador
      vals_lr['name'] = "%s 23:59:59" % self._get_data_lectura(data)
      vals_lr['tipus'] = 'R'
      # TODO obtenir lectura anterior i sumarla aquí sii adapter.tipus_lectura = 'i'
      vals_lr['lectura'] = suma_energia[periode]['r']
      vals_lr['observacions'] = "Lecturas importadas de contador"
      ll.create(cr, uid, vals_lr)

    # actualitzem la curva horaria amb la última data
    self.pool.get('giscedata.curva.horaria').write(cr, uid, ch_id, {'data_inici': data_inici, 'data_final': data_final})
    return { 'text': 'Se ha finalizado la importación.' }

  _name = 'giscedata.curva.horaria.linia'
  _description = 'Importar lectures de comptadors'
  _columns = {
    'curva': fields.many2one('giscedata.curva.horaria', 'Curva horaria', readonly=True),
    'name': fields.date('Data', readonly=True),
    'hora': fields.time('Hora', readonly=True),
    'activa_consumida': fields.integer('Activa I', readonly=True),
    'cualificador_ac': fields.char('Cualificador AC', size=4, readonly=True),
    'activa_generada': fields.integer('Activa E', readonly=True),
    'cualificador_ag': fields.char('Cualificador AG', size=4, readonly=True),
    'reactiva_consumida_inductiva': fields.integer('Reactiva Consumida Inductiva', readonly=True),
    'cualificador_rci': fields.char('Cualificador RCI', size=4, readonly=True),
    'reactiva_consumida_capacitiva': fields.integer('Reactiva Consumida Capacitiva', readonly=True),
    'cualificador_rcc': fields.char('Cualificador RCC', size=4, readonly=True),
    'reactiva_generada_inductiva': fields.integer('Reactiva Generada Inductiva', readonly=True),
    'cualificador_rgi': fields.char('Cualificador RGI', size=4, readonly=True),
    'reactiva_generada_capacitiva': fields.integer('Reactiva Generada Capacitiva', readonly=True),
    'cualificador_rgc': fields.char('Cualificador RGC', size=4, readonly=True),
    'reserva_1': fields.integer('Reserva 1', readonly=True),
    'cualificador_r1': fields.char('Cualificador R1', size=4, readonly=True),
    'reserva_2': fields.integer('Reserva 2', readonly=True),
    'cualificador_r2': fields.char('Cualificador R2', size=4, readonly=True),
  }
  _defaults = {}
  _order = "name, hora"


giscedata_curva_horaria_linia()


