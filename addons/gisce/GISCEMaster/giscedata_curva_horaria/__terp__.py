# -*- coding: utf-8 -*-
{
    "name": "Importar lectura de comptadors",
    "description": """Importar lectures dels comptadors""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "giscedata_lectures",
        "giscedata_polissa"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_curva_horaria_wizard.xml",
        "giscedata_curva_horaria_data.xml",
        "giscedata_curva_horaria_view.xml"
    ],
    "active": False,
    "installable": True
}
