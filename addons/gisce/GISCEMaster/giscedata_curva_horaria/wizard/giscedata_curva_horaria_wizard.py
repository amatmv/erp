# -*- coding: utf-8 -*-

import wizard
import pooler

"""
mod:`giscedata_curva_horaria_wizard` - Wizard per importar lectures de comptadors
=====================================================================================

Aquest mòdul carrega els fitxers amb les lectures de comptadors i les passa al mòdul
mod:`giscedata_curva_horaria` per tal que es guardin a la base de dades.

"""

def _init(self, cr, uid, data, context={}):
  return {}

_load_csv_form = """<?xml version="1.0"?>
<form string="Importación de lecturas de contador">
  <field name="csv" colspan="4" required="1"/>
  <field name="model" required="1"/>
  <field name="perdues" required="1" />
  <field name="comptador" required="1"/>
</form>
"""
_load_csv_fields = {
  'csv': {'string': 'Fichero de lecturas', 'type': 'binary', 'required': True},
  'model': {'string': 'Modelo de fichero', 'type': 'many2one', 'relation':'giscedata.curva.horaria.adapter', 'required': True },
  'comptador': {'string': 'Contador', 'type': 'many2one', 'relation':'giscedata.lectures.comptador', 'required': True},
  'perdues': {'string': '% Pérdidas', 'type': 'float', 'required': True},
}

_call_import_form = """<?xml version="1.0"?>
<form string="Importación de lecturas de contador">
  <field  name="text" nolabel="1" readonly="1" />
</form>
"""

_call_import_fields = {
  'text': {'string': 'Text', 'type': 'text'}
}

def _import(self, cr, uid, data, context={}):
  """
  Mètode que cridarà a giscedata_lectures_import.import(file)
  """
  imp = pooler.get_pool(cr.dbname).get('giscedata.curva.horaria.linia')
  return imp.importar(cr, uid, data['form']['csv'],data['form']['model'],data['form']['comptador'], data['form']['perdues'])

class giscedata_curva_horaria_wizard(wizard.interface):
  states = {
    'init': {
      'actions': [ _init ],
      'result': {'type': 'state', 'state': 'load_csv'}
    },
    'load_csv': {
      'actions' : [],
      'result': {'type': 'form', 'arch': _load_csv_form, 'fields': _load_csv_fields, 'state': [('call_import','Importar','gtk-go-forward')]}
    },
    'call_import': {
      'actions': [ _import ],
      'result': {'type': 'form', 'arch': _call_import_form, 'fields': _call_import_fields, 'state': [('end','Acabar','gtk-go-forward')]}
    }
  }

giscedata_curva_horaria_wizard('giscedata.curva.horaria')

