# -*- coding: utf-8 -*-
{
    "name": "Pagos por capacidad Gener 2019",
    "description": """
  Actualització de les tarifes pagos por capacidad segons el BOE nº 308 - 22/12/2018.
  TEC/1366/2018
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "giscedata_polissa",
        "giscedata_pagos_capacidad"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_tarifas_pagos_capacidad_20190101_data.xml"
    ],
    "active": False,
    "installable": True
}
