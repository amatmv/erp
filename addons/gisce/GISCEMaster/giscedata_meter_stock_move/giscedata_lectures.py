# -*- coding: utf-8 -*-
from osv import osv


class GiscedataLecturesComptador(osv.osv):

    _name = "giscedata.lectures.comptador"
    _inherit = "giscedata.lectures.comptador"

    def create(self, cursor, uid, vals, context=None):
        """
        Makes a movement to "Red de Distribución".
        :param cursor:
        :param uid:
        :param vals:
        :param context:
        :return:
        """
        stock_location_o = self.pool.get('stock.location')
        picking_o = self.pool.get('stock.picking')
        stock_move_o = self.pool.get('stock.move')
        prodlot_o = self.pool.get('stock.production.lot')
        varconf_o = self.pool.get('res.config')

        meter_id = super(GiscedataLecturesComptador, self).create(
            cursor, uid, vals, context=None
        )

        related_varconfs = [
            'dest_location_meter_assigned',
            'avoid_move_same_destination_meter_assigned'
        ]

        dmn = [('name', 'in', related_varconfs)]
        varconfs_vs = varconf_o.q(cursor, uid).read(['name', 'value']).where(dmn)

        varconfs_vs = {x['name']: x['value'] for x in varconfs_vs}

        dest_stock_location = varconfs_vs['dest_location_meter_assigned']
        allow_redundant_move = varconfs_vs['avoid_move_same_destination_meter_assigned'] == '0'

        prodlot_id = vals.get('serial', False)
        prodlot = prodlot_o.browse(cursor, uid, prodlot_id, context=context)

        source_location_id = prodlot.location_id

        if source_location_id:
            source_location_id = source_location_id.id
        else:
            source_location_id = stock_location_o.search(
                cursor, uid, [('name', '=', 'Inventory loss')],
                context=context
            )[0]

        dest_stock_location_id = stock_location_o.search(
            cursor, uid, [('name', '=', dest_stock_location)],
            context=context
        )[0]

        same_location = source_location_id == dest_stock_location_id

        do_stock_move = bool(dest_stock_location) and (
                (not allow_redundant_move and not same_location) or allow_redundant_move
        )

        if prodlot_id and do_stock_move:
            default_name = 'Instalació comptador {}'.format(prodlot.name)

            picking_cv = {
                'type': 'out',
                'name': default_name,
            }
            picking_id = picking_o.create(
                cursor, uid, picking_cv, context=context
            )

            product = prodlot.product_id

            stock_move_cv = {
                'name': default_name,
                'picking_id': picking_id,
                'product_id': product.id,
                'product_uom': product.product_tmpl_id.uom_id.id,
                'prodlot_id': prodlot_id,
                'location_dest_id': dest_stock_location_id,
                'location_id': source_location_id,
            }
            stock_move_id = stock_move_o.create(
                cursor, uid, stock_move_cv, context=context
            )

            template = stock_move_o.onchange_product_id(
                cursor, uid, [stock_move_id], product.id, picking_type='out'
            )

            stock_move_name = template.get('value', {}).get('name', False)
            if stock_move_name:
                stock_move_wv = {
                    'name': stock_move_name
                }
                stock_move_o.write(
                    cursor, uid, stock_move_id, stock_move_wv, context=context
                )

            picking_o.draft_validate(cursor, uid, [picking_id])

        return meter_id


GiscedataLecturesComptador()
