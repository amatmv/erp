# -*- coding: utf-8 -*-
{
    "name": "Moviment d'stock pels comptadors",
    "description": """Implementa:
    - Automatització del moviment de comptadors quan un lot de producció s'assigna a una pòlissa.""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends": [
        "giscedata_lectures_distri",
        "giscedata_stock_location"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "giscedata_meter_stock_move_data.xml",
    ],
    "active": False,
    "installable": True
}
