# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction


class TestsMeterStockMove(testing.OOTestCase):

    def setUp(self):
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user

    def tearDown(self):
        self.txn.stop()

    def set_meter_stock_move_varconfs(self, destination, avoid_redundant):
        varconf_o = self.openerp.pool.get('res.config')

        dmn = [('name', '=', 'dest_location_meter_assigned')]
        varconf_id = varconf_o.search(self.cursor, self.uid, dmn)
        varconf_wv = {'value': destination}
        varconf_o.write(self.cursor, self.uid, varconf_id, varconf_wv)

        dmn = [('name', '=', 'avoid_move_same_destination_meter_assigned')]
        varconf_id = varconf_o.search(self.cursor, self.uid, dmn)

        avoid_redundant_v = '1' if avoid_redundant else '0'

        varconf_wv = {'value': avoid_redundant_v}
        varconf_o.write(self.cursor, self.uid, varconf_id, varconf_wv)

    def get_policy_id(self, ref):
        imd_o = self.openerp.pool.get('ir.model.data')
        policy_id = imd_o.get_object_reference(
            self.cursor, self.uid, 'giscedata_polissa', ref
        )[1]

        return policy_id

    def get_prodlot_id(self, ref):
        imd_o = self.openerp.pool.get('ir.model.data')
        prodlot_id = imd_o.get_object_reference(
            self.cursor, self.uid, 'stock', ref
        )[1]

        return prodlot_id

    def test_stock_movement_done(self):

        stock_move_o = self.openerp.pool.get('stock.move')

        location_dest_name = 'Output'

        self.set_meter_stock_move_varconfs(location_dest_name, False)
        policy_id = self.get_policy_id('polissa_0001')
        prodlot_id = self.get_prodlot_id('stock_production_lot_001')

        meter_o = self.openerp.pool.get('giscedata.lectures.comptador')
        meter_cv = {
            'polissa': policy_id,
            'meter_type': 'PF',
            'name': 'Acer KLJO9AJRE91SX',
            'serial': prodlot_id
        }
        meter_o.create(self.cursor, self.uid, meter_cv)

        stock_move_id = stock_move_o.search(
            self.cursor, self.uid, [('prodlot_id', '=', prodlot_id)]
        )[0]

        stock_move_v = stock_move_o.read(
            self.cursor, self.uid, stock_move_id, ['location_dest_id', 'state']
        )

        self.assertEqual(stock_move_v['location_dest_id'][1], location_dest_name)
        self.assertEqual(stock_move_v['state'], 'done')

        self.set_meter_stock_move_varconfs(location_dest_name, True)

        meter_cv = {
            'polissa': policy_id,
            'meter_type': 'PF',
            'name': 'Acer KLJO9AJRE91SX',
            'serial': prodlot_id
        }
        meter_o.create(self.cursor, self.uid, meter_cv)

        stock_move_ids = stock_move_o.search(
            self.cursor, self.uid, [('prodlot_id', '=', prodlot_id)]
        )

        self.assertEqual(len(stock_move_ids), 1)
