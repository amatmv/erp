# -*- coding: utf-8 -*-

from destral import testing
from destral.transaction import Transaction
from osv.orm import ValidateException
from expects import expect, raise_error
from giscedata_lectures_distri.giscedata_polissa import _SELF_CONSUME_METERS


class TestPolissaLecturesDistri(testing.OOTestCase):

    def activar_polissa(self, pool, cursor, uid, polissa_id):
        polissa_obj = pool.get('giscedata.polissa')

        polissa_obj.send_signal(cursor, uid, [polissa_id], [
            'validar', 'contracte'
        ])

    def test_check_meters_in_self_consumption_contracts(self):

        for self_code in _SELF_CONSUME_METERS.keys():

            with Transaction().start(self.database) as txn:
                pool = self.openerp.pool
                cursor = txn.cursor
                uid = txn.user
                pol_obj = pool.get('giscedata.polissa')
                meter_obj = pool.get('giscedata.lectures.comptador')
                imd_obj = pool.get('ir.model.data')

                pol_id = imd_obj.get_object_reference(
                    cursor, uid, 'giscedata_polissa', 'polissa_0005'
                )[1]

                pol_obj.write(
                    cursor, uid, pol_id, {'autoconsumo': self_code}
                )

                meter_create = {
                    'polissa': pol_id,
                    'name': ' ',
                    'meter_type': ' '
                }
                meter_name = 9999999900
                meter_ids = []
                for mt in _SELF_CONSUME_METERS[self_code]:
                    meter_name += 1
                    meter_create.update({
                        'name': str(meter_name),
                        'meter_type': mt
                    })
                    meter_ids.append(meter_obj.create(cursor, uid, meter_create))
                self.assertTrue(
                    len(meter_ids) == len(_SELF_CONSUME_METERS[self_code])
                )
                true_response = pol_obj.check_meters_on_dso_contracts(
                    cursor, uid, pol_id
                )
                self.assertTrue(true_response)

    def test_check_meters_in_self_consumption_contracts_fails(self):

        with Transaction().start(self.database) as txn:
            pool = self.openerp.pool
            cursor = txn.cursor
            uid = txn.user
            pol_obj = pool.get('giscedata.polissa')
            meter_obj = pool.get('giscedata.lectures.comptador')
            imd_obj = pool.get('ir.model.data')

            pol_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0004'
            )[1]

            pol_obj.write(
                cursor, uid, pol_id, {'autoconsumo': u'2A'}
            )

            meter_in_pol = meter_obj.search(
                cursor, uid, [('polissa', '=', pol_id)]
            )

            res_obj = pool.get('res.config')

            res_obj.set(cursor, uid, 'unlink_meter_with_reads', 1)

            if meter_in_pol:
                meter_obj.unlink(cursor, uid, meter_in_pol)

            expect(
                lambda: self.assertRaises(
                    ValidateException, self.activar_polissa(
                        pool, cursor, uid, pol_id
                    )
                )
            ).to(
                raise_error(
                    ValidateException,
                    u"warning -- ValidateError\n\nError occurred while "
                    u"validating the field(s) Autoconsum: Error "
                    u"Autoconsum: Faltan contadors obligatoris a la polissa "
                    u"per aquest tipus d'autoconsum. Segons el tipus "
                    u"d'autoconsum , es necessiten com a m\xednim alguns "
                    u"comptadors de diferent tipus (Punt Frontera, "
                    u"Generaci\xf3, Consum)"
                )
            )