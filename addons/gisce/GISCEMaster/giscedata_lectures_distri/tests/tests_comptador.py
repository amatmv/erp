# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from expects import expect, equal

class TestsGiscedataLecturesComptador(testing.OOTestCase):

    def setUp(self):
        self.openerp.install_module('product_caracteristiques')
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user

    def tearDown(self):
        self.txn.stop()

    def get_comptador(self):
        imd_obj = self.openerp.pool.get('ir.model.data')
        return imd_obj.get_object_reference(
            self.cursor, self.uid, 'giscedata_lectures', 'comptador_0001'
        )[1]

    def get_product_id(self):
        imd_obj = self.openerp.pool.get('ir.model.data')
        return imd_obj.get_object_reference(
            self.cursor, self.uid, 'giscedata_lectures_distri', 'product_meter_0001'
        )[1]

    def get_stock_production_lot(self):
        imd_obj = self.openerp.pool.get('ir.model.data')
        return imd_obj.get_object_reference(
            self.cursor, self.uid, 'giscedata_lectures_distri', 'serial_stock_B63011077'
        )[1]

    def get_lloguer_id(self):
        imd_obj = self.openerp.pool.get('ir.model.data')
        return imd_obj.get_object_reference(
            self.cursor, self.uid, 'giscedata_lectures_distri', 'product_alq_0055'
        )[1]

    def test_new_meter(self):
        """
        Test assign product to metter and validate if automatism of assignee caracteristiques is correct
        :return:
        """
        # Init meter
        meter_obj = self.openerp.pool.get('giscedata.lectures.comptador')
        meter_id = self.get_comptador()
        meter_obj.write(self.cursor, self.uid, meter_id, {
            'giro': 1000000,
            'technology_type': None,
        })

        # Create caracteristiques
        product_id = self.get_product_id()
        caract_obj = self.openerp.pool.get('product.caracteristica')
        caract_obj.create(self.cursor, self.uid, {
            'name': 'giro',
            'value': '100000000',
            'product_id': product_id,
        })
        caract_obj.create(self.cursor, self.uid, {
            'name': 'technology',
            'value': 'mechanic',
            'product_id': product_id,
        })
        caract_obj.create(self.cursor, self.uid, {
            'name': 'alquiler',
            'value': 'ALQ55',
            'product_id': product_id,
        })

        # Assign new product to meter
        stock_prod_lot_id = self.get_stock_production_lot()
        res = meter_obj.on_change_serial(self.cursor, self.uid, None, stock_prod_lot_id)['value']
        meter_obj.write(self.cursor, self.uid, meter_id, {
            'giro': res['giro'],
            'technology_type': res['technology_type'],
        })

        # Meter caracteristiques (lloger, giro...) changed?
        meter_end_data = meter_obj.read(
            self.cursor, self.uid, meter_id, ['giro', 'technology_type', 'tg', 'lloguer', 'product_lloguer_id'])
        expect(meter_end_data['giro']).to(equal(100000000))
        expect(meter_end_data['technology_type']).to(equal('mechanic'))
