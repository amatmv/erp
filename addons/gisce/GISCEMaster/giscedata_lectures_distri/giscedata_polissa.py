# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _

_SELF_CONSUME_METERS = {
        u'00': (u'PF',),
        u'01': (u'PF', u'G'),
        u'2A': (u'G', u'C'),
        u'2B': (u'PF', u'G'),
        u'2G': (u'PF', u'G'),
        u'31': (u'PF', ),
        u'32': (u'PF', ),
        u'33': (u'PF', ),
        u'41': (u'PF', ),
        u'42': (u'PF', ),
        u'51': (u'PF', ),
        u'52': (u'PF', ),
        u'53': (u'PF', ),
        u'54': (u'PF', ),
        u'55': (u'PF', ),
        u'56': (u'PF', ),
        u'61': (u'PF', ),
        u'62': (u'PF', ),
        u'63': (u'PF', ),
        u'64': (u'PF', ),
        u'71': (u'PF', ),
        u'72': (u'PF', ),
        u'73': (u'PF', ),
        u'74': (u'PF', ),
    }


class GiscedataPolissa(osv.osv):
    """Extensió per la pòlissa pel mòdul de lectures.
    """
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def __init__(self, pool, cursor):
        super(GiscedataPolissa, self).__init__(pool, cursor)

    def check_meters_on_dso_contracts(self, cursor, uid, pol_id, context=None):

        pol = self.read(cursor, uid, pol_id, ['autoconsumo', 'contract_type'])

        if pol:
            if pol['contract_type'] == '05': # Generación Régimen Especial
                return True
            meter_obj = self.pool.get('giscedata.lectures.comptador')
            meters_search_params = [
                ('polissa', '=', pol['id']),
                ('active', '=', True)
            ]

            meters_in_contract_ids = meter_obj.search(
                cursor, uid, meters_search_params
            )
            if meters_in_contract_ids:
                meters_types = meter_obj.read(
                    cursor, uid, meters_in_contract_ids, ['meter_type']
                )
                if meters_types:

                    m_types = set(
                        token['meter_type'] for token in meters_types
                    )
                    expected_meters = _SELF_CONSUME_METERS[pol['autoconsumo']]

                    return all(
                        k in m_types for k in expected_meters
                    )
            return pol['autoconsumo'] == u'00'

        return True

    def _cnt_contract_has_correct_meters(self, cursor, uid, ids):
        '''Check if dso contracts have correct meters on validate'''
        states = ('activa', 'modcontractual')
        for polissa in self.read(cursor, uid, ids, ['state', 'autoconsumo']):
            if polissa['state'] in states:
                return self.check_meters_on_dso_contracts(
                    cursor, uid, polissa['id']
                )

        return True

    _constraints = [
        (
            _cnt_contract_has_correct_meters,
            _("Error Autoconsum: Faltan contadors obligatoris a la polissa per "
              "aquest tipus d'autoconsum. Segons el tipus d'autoconsum , es "
              "necessiten com a mínim alguns comptadors de diferent tipus "
              "(Punt Frontera, Generació, Consum)"),
            ['autoconsumo']
        ),
    ]

GiscedataPolissa()
