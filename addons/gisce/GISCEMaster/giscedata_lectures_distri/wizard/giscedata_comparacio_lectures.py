# -*- coding: utf-8 -*-

from osv import osv, fields
import csv
import re
import time
import base64
from tools.translate import _
from tools import config
import StringIO


class GiscedataComparacioLectures(osv.osv_memory):
    """Assistent per exportar les dades de comparació
    "" de lectures
    """
    _name = 'giscedata.comparacio.lectures'

    _columns = {
        'from_date': fields.char('Mes', size=7),
        'name': fields.char('Nom', size=256),
        'file': fields.binary('Fitxer'),
        'state': fields.selection([('init', 'Init'),
                                   ('invalid', 'No vàlid'),
                                   ('end', 'End')],
                                  'Estat')
    }
    _defaults = {
        'state': lambda *a: 'init',
        'from_date': lambda *a: '{mes}/{any}'.format(mes=time.strftime('%m'),
                                                     any=time.strftime('%Y')),
        'name': lambda *a: 'comparacio_lecturas_{nom_fitxer}_%s.csv'
                           % time.strftime('%Y%m%d%H%M%S')

    }

    INFORME_CSV = {
        'comparativa_lectures_csv': {
            'header': [_(u'Pòlissa'),
                       _(u'Data_última_lectura'),
                       _(u'Dies_última_lectura'),
                       _(u'Consum_actual'),
                       _(u'Consum_diari_actual'),
                       _(u'%_n-1'),
                       _(u'%_n-12'),
                       _(u'Data_inici_factura_anterior'),
                       _(u'Data_final_factura_anterior'),
                       _(u'Dies_factura_anterior'),
                       _(u'Consum_factura_anterior'),
                       _(u'Consum_diari_factura_anterior'),
                       _(u'Data_inici_factura_any_anterior'),
                       _(u'Data_final_factura_any_anterior'),
                       _(u'Dies_factura_any_anterior'),
                       _(u'Consum_factura_any_anterior'),
                       _(u'Consum_diari_factura_any_anterior')],
            'query': (
                '%s/giscedata_lectures_distri/sql/comparativa_lectures.sql'
                % config['addons_path'])

        }
    }

    def validate_from_date(self, from_date):
        date_split = from_date.split('/')
        if len(date_split) != 2:
            return 1
        else:
            if re.match("^(0?[1-9]|1[012])$", date_split[0]) is not None:
                if re.match("^(19\d{2}|2\d{3})$", date_split[1]) is not None:
                    return 0
                else:
                    return 2
            else:
                return 3

    def validate_success(self):
        v = {}
        v['state'] = 'init'
        return {'value': v}

    def error_date(self):
        warn = {}
        v = {}
        v['state'] = 'invalid'
        warn['title'] = _(u'Data no vàlida')
        warn['message'] = _(u'S\'ha d\'introduir un mes amb el següent'
                            + u' format: mm/yyyy')
        return {'warning': warn, 'value': v}

    def error_month(self):
        warn = {}
        v = {}
        v['state'] = 'invalid'
        warn['title'] = _(u'Mes no vàlid')
        warn['message'] = _(u'Introduïu el mes amb 2 xifres i que '
                            + u' que sigui vàlid')
        return {'warning': warn, 'value': v}

    def error_year(self):
        warn = {}
        v = {}
        v['state'] = 'invàlid'
        warn['title'] = _(u'Any no vàlid')
        warn['message'] = _(u'Introduïu l\'any amb 4 xifres i'
                            + u' que sigui vàlid')
        return {'warning': warn, 'value': v}

    def onchange_from_date(self, cr, uid, ids, from_date):
        results = {0: self.validate_success,
                   1: self.error_date,
                   2: self.error_year,
                   3: self.error_month}
        res = self.validate_from_date(from_date)

        return results[res]()

    def generar_csv(self, cursor, params, query, header, context=None):
        """Genera la consulta sql amb les paràmetres d'entrada params"""
        cursor.execute(query, params)
        resum_table = cursor.fetchall()

        output = StringIO.StringIO()
        writer = csv.writer(output, delimiter=';', quoting=csv.QUOTE_NONE)
        writer.writerow([_(h).encode('utf8') for h in header])

        for row in resum_table:
            tmp = [isinstance(t, basestring) and t.encode('utf-8')
                   or t for t in row]
            writer.writerow(tmp)

        return {'csv': base64.b64encode(output.getvalue())}

    def export_file(self, cursor, uid, ids, context=None):
        """Funció per exportar el fitxer.
        """

        if not context:
            context = {}

        wizard = self.browse(cursor, uid, ids[0])
        from_date = wizard.from_date

        results = {0: self.validate_success,
                   1: self.error_date,
                   2: self.error_year,
                   3: self.error_month}
        res = self.validate_from_date(from_date)

        validation_result = results[res]()
        if validation_result['value']['state'] != 'init':
            raise osv.except_osv(validation_result['warning']['title'],
                                 validation_result['warning']['message'])
        else:
            params = (from_date,)
            sqlfile = self.INFORME_CSV['comparativa_lectures_csv']['query']
            header = self.INFORME_CSV['comparativa_lectures_csv']['header']
            query = open(sqlfile).read()
            filename = (wizard.name).format(
                nom_fitxer=from_date.replace('/', '_'))

            res = self.generar_csv(cursor, params, query, header, context)
            wizard.write({'file': res['csv'], 'name': filename,
                          'state': 'end'})


GiscedataComparacioLectures()
