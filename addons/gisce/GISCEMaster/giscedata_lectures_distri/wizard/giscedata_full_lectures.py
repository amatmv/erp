# -*- coding: utf-8 -*-

# Adaptació de l'script ./import_lectures_nopolissa_xmlrpc.py

import csv
import time
import base64
import StringIO
import traceback
import sys

from osv import osv, fields


class GiscedataLecturesImportarFullLectures(osv.osv_memory):
    """Classe per fer la importació del full de lectures
    """

    _name = 'giscedata.lectures.importar.full.lectures'

    def action_exec_script(self, cursor, uid, ids, context=None):
        """importar el full de lectures de fitxer csv"""

        wizard = self.browse(cursor, uid, ids[0], context)
        silent = wizard.silent
        txt = base64.decodestring(wizard.file)
        csv_file = StringIO.StringIO(txt)        
        reader = csv.reader(csv_file, delimiter=';')
        
        result = []
        conta_lect_no_valida = []
        conta_no_trobats = []
        conta_sense_lectura = []
        conta_amb_lectura = []
        
        HEADERS = {
          0: 'data',
          1: 'comptador',
          2: 'P1A',
          3: 'P2A',
          4: 'P3A',
          5: 'P4A',
          6: 'P5A',
          7: 'P6A',
          8: 'P1R',
          9: 'P2R',
          10: 'P3R',
          11: 'P4R',
          12: 'P5R',
          13: 'P6R',
          14: 'MAX1',
          15: 'MAX2',
          16: 'MAX3',
          17: 'MAX4',
          18: 'MAX5',
          19: 'MAX6',
          20: 'EXC1',
          21: 'EXC2',
          22: 'EXC3',
          23: 'EXC4',
          24: 'EXC5',
          25: 'EXC6',
          26: 'MULT',
        }
        
        REV_HEADERS = {}
        
        for index, name in HEADERS.items():
            REV_HEADERS[name] = index
        
        if silent:
            result.append('***********************************************\n'
                          ' Running in silent mode (readonly)\n'
                          '***********************************************\n')

        comp = self.pool.get('giscedata.lectures.comptador')
        lect = self.pool.get('giscedata.lectures.lectura')
        poli = self.pool.get('giscedata.polissa')
        peri = self.pool.get('giscedata.polissa.tarifa.periodes')
        pote = self.pool.get('giscedata.lectures.potencia')
        
        for n_row, row in enumerate(reader):
            if row[0].startswith('#'):
                continue
            # Busquem el contador
            data_lectura = row[0]
            comptador = row[1]
            if wizard.use == 'comptador':
                idc = comp.search(cursor, uid, [('name', '=', comptador)])
            elif wizard.use == 'cups':
                idc = comp.search(cursor, uid, [
                    ('polissa.cups.name', '=', comptador)
                ])
            elif wizard.use == 'contracte':
                idc = comp.search(cursor, uid, [
                    ('polissa.name', '=', comptador)
                ])
            else:
                raise Exception('Use not supported')
            result.append(">> Important lectures %s %s" % (
                wizard.use, comptador
            ))
            contador = comp.read(cursor, uid, idc, ['id', 'polissa'])
            if not len(contador):
                conta_no_trobats.append(comptador)
                continue
        
            data = time.strftime('%Y-%m-%d', time.strptime(data_lectura, '%d/%m/%Y'))
            # mirem si el comptador ja té lectura per la data que importem
            lectures = lect.search(cursor, uid, [('comptador', '=', idc[0])])
            amb_lectura = False
            for l in lectures:
                ll = lect.read(cursor, uid, l,
                                    ['id', 'tipus', 'name', 'periode'])
                if ll['name'] == data:
                    result.append(">> tenim lectura ja entrada pel %s"
                                  " %s, tipus %s, periode %s i la data %s" %
                                 (wizard.use, comptador, ll['tipus'].encode('utf8'),
                                  ll['periode'][1].encode('utf8'), data))
                    amb_lectura = True
            if amb_lectura:
                conta_amb_lectura.append(comptador)
                continue
            amb_lectura = False
            polissa = poli.read(cursor, uid, [contador[0]['polissa'][0]],
                                                         ['name', 'tarifa'])[0]
            periode = peri.search(cursor, uid, [('tipus', '=', 'te'),
                                    ('tarifa.id', '=', polissa['tarifa'][0])])
            periodes = peri.read(cursor, uid, periode, ['name', 'tipus'])
            i = REV_HEADERS['P1A']
            # FORMAT CSV
            # data - comptador - activa [ p1, p2, p3, p4, p5, p6 ]
            #  0         1                2   3   4   5   6   7
            # reactiva [ p1, p2, p3, p4, p5, p6 ] - max [ p1, p2, p3, p4, p5, p6 ]
            #            8   9   10  11  12  13           14  15  16  17  18  19
            # excesos [ p1, p2, p3, p4, p5, p6 ]
            #           20  21  22  23  24  25
        
            savepoint = 'lect_row_%i' % n_row
            cursor.savepoint(savepoint)
            err_sp = False
            for p in periodes:
                vals = {}
                if row[i]:
                    try:
                        # fem dos castings, algú hi carda decimals...
                        lectura = int(float(row[i]))  
                        vals['name'] = data
                        vals['periode'] = p['id']
                        vals['lectura'] = lectura
                        vals['tipus'] = 'A'
                        vals['comptador'] = idc[0]
                        vals['observacions'] = 'Datos importados'
                        vals['origen_id'] = wizard.origen_id.id
                        try:
                            if not silent:
                                lect.create(cursor, uid, vals)
                        except Exception as e:
                            result.append(e[0])
                            err_sp = True
                            conta_lect_no_valida.append(comptador)
                            cursor.rollback(savepoint)
                            traceback.print_exc()
                            sys.stderr.flush()
                    except ValueError as e:
                        result.append(e[0])
                        sys.stderr.write('Error', e.message)
                        sys.stderr.flush()
                        if conta_lect_no_valida.count(comptador) == 0:
                            conta_lect_no_valida.append(comptador)
                else:
                    if conta_sense_lectura.count(comptador) == 0:
                        conta_sense_lectura.append("%s a %s [%s] (%s)"
                                                   % (comptador, HEADERS[i],i, row[i]))
                if err_sp:
                    break
                if row[i+6]:
                    try:
                        lectura = int(float(row[i+6]))
                        vals['name'] = data
                        vals['periode'] = p['id']
                        vals['lectura'] = lectura
                        vals['tipus'] = 'R'
                        vals['comptador'] = idc[0]
                        vals['observacions'] = 'Datos importados'
                        vals['origen_id'] = wizard.origen_id.id
                        savepoint = 'lect_row_%i' % n_row
                        try:
                            if not silent:
                                lect.create(cursor, uid, vals)
                        except Exception as e:
                            result.append(e[0])
                            err_sp = True
                            conta_lect_no_valida.append(comptador)
                            cursor.rollback(savepoint)
                            traceback.print_exc()
                            sys.stderr.flush()
                    except ValueError as e:
                        result.append(e[0])
                        if conta_lect_no_valida.count(row[1]) == 0:
                            conta_lect_no_valida.append(row[1])
                else:
                    pass
                if err_sp:
                    break
                i += 1

            if err_sp:
                cursor.release(savepoint)
                continue;

            # importació de lectures de potència (_p)
            periode_p = peri.search(cursor, uid, [('tipus', '=', 'tp'),
                                      ('tarifa.id', '=', polissa['tarifa'][0])])
            periodes_p = peri.read(cursor, uid, periode_p, ['name', 'tipus'])

            i = REV_HEADERS['MAX1']
            for pp in periodes_p:
                vals = {}
                if row[i]:
                    try:
                        lectura = float(row[i])
                        if row[i+6]:
                            exces = float(row[i+6])
                        else:
                            exces = 0
                        vals['name'] = data
                        vals['periode'] = pp['id']
                        vals['lectura'] = lectura
                        vals['comptador'] = idc[0]
                        vals['exces'] = exces
                        vals['observacions'] = 'Datos importados'
                        try:
                            if not silent:
                                pote.create(cursor, uid, vals) 
                        except Exception as e:
                            result.append(e[0])
                            err_sp = True
                            traceback.print_exc()
                            sys.stderr.flush()
                    except ValueError as e:
                        result.append(e[0])
                        sys.stderr.write(e.message)
                        sys.stderr.flush()
                        if conta_lect_no_valida.count(row[1]) == 0:
                            conta_lect_no_valida.append(row[1])
                else:
                    pass
                if err_sp:
                    break
                i += 1
            cursor.release(savepoint)
        
        result.append(">> %s amb lectures no vàlides al CSV: %s" % (
            wizard.use, conta_lect_no_valida
        ))
        result.append(">> %s no trobats a la BD: %s" % (
            wizard.use, conta_no_trobats
        ))
        result.append(">> %s sense lectura al CSV: %s" % (
            wizard.use, conta_sense_lectura
        ))
        result.append(">> %s que ja tenien lectura a la BD: %s" % (
            wizard.use, conta_amb_lectura
        ))
        wizard.write({'result': '\n'.join(result), 'state': 'done'})

    def _default_origen_id(self, cursor, uid, context=None):
        if context is None:
            context = {}
        orig = self.pool.get('giscedata.lectures.origen')
        return orig.search(cursor, uid, [('codi', '=', '10')])[0]

    _columns = {
        'origen_id': fields.many2one(
            'giscedata.lectures.origen', 'Origen', required=True
        ),
        'use': fields.selection([
            ('contracte', 'Contracte'),
            ('cups', 'CUPS'),
            ('comptador', 'Comptador')
        ], 'Utilitzar', required=True),
        'result': fields.text('Resultat'),
        'state': fields.char('State', size=4),
        'file': fields.binary('Fitxer'),
        'filename': fields.char('Nom', size=32),
        'silent': fields.boolean('Prova'),
    }
    
    _defaults = {
        'use': lambda *a: 'comptador',
        'state': lambda *a: 'init',
        'origen_id': _default_origen_id,
    }

GiscedataLecturesImportarFullLectures()
