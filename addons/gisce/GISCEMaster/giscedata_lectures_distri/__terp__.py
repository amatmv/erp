# -*- coding: utf-8 -*-
{
    "name": "Lectura de comptadors (Distribuidora)",
    "description": """Lectures dels comptadors""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends": [
        "base",
        "giscedata_lectures_tecnologia",
        "giscedata_polissa_distri",
        "mongodb_backend",
        "stock"
    ],
    "init_xml": [],
    "demo_xml": ["product_demo.xml"],
    "update_xml": [
        "giscedata_lectures_view.xml",
        "autoconsum_view.xml",
        "wizard/giscedata_full_lectures_wizard.xml",
        "wizard/giscedata_comparacio_lectures_wizard.xml",
        "ir.model.access.csv",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
