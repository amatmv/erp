# -*- coding: utf-8 -*-
from osv import osv, fields
from giscedata_lectures_tecnologia.giscedata_lectures import TECHNOLOGY_TYPE_SEL
import pooler


CARACTERISTIQUES_VALIDES = ['giro', 'technology', 'phases']


class GiscedataLecturesComptador(osv.osv):
    """Comptador de distribuidrora.
    """

    _name = 'giscedata.lectures.comptador'
    _inherit = 'giscedata.lectures.comptador'

    def write(self, cursor, uid, ids, vals, context=None):
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        if 'cini' in vals:
            for comptador in self.read(cursor, uid, ids, ['bloquejar_cini']):
                # En el cas que desbloquejem i escrivim el CINI alhora.
                comptador.update(vals)
                if comptador['bloquejar_cini']:
                    del vals['cini']
                    break
        return super(GiscedataLecturesComptador,
                     self).write(cursor, uid, ids, vals, context)

    def on_change_serial(self, cursor, uid, ids, serial_id, context=None):
        """
        Onchange pel camp "Nº de sèrie (magatzem)
        :param cursor:
        :param uid:
        :param ids:
        :param serial_id: el camp modificat
        :param context:
        :return:
        """
        res = {'value': {}, 'domain': {}, 'warning': {}}
        caracteristiques = self.get_caracteristiques_comptador(cursor, uid, serial_id, context=context)
        for name, value in caracteristiques.iteritems():
            self.update_values(cursor, uid, res, name, value)
        return res

    def get_caracteristiques_comptador(self, cursor, uid, serial_id, context=None):
        """
        Obtenim certes característiques d'un producte donat un nº de sèrie de comptador
        Les característiques que ens interessen es defineixen en CARACTERISTIQUES_VALIDES
        :param context:
        :param cursor:
        :param uid:
        :param ids:
        :param serial_id: nº de sèrie (magatzem)
        :return: caracteristiques
        """
        if context is None:
            context = {}
        caracteristiques = {}
        # Validem que el mòdul de caracterísiques está instal·lat
        caracteristica_obj = self.pool.get('product.caracteristica')
        if caracteristica_obj:
            lot_obj = self.pool.get('stock.production.lot')
            product_obj = self.pool.get('product.product')
            # Busquem les característiques des del nº de sèrie
            if serial_id:
                product_id = lot_obj.read(cursor, uid, serial_id, ['product_id'])['product_id']
                extra_vals_ids = product_obj.read(cursor, uid, product_id[0], ['extra_vals_ids'])['extra_vals_ids']
                for extra_vals_id in extra_vals_ids:
                    caracteristica_data = caracteristica_obj.read(cursor, uid, extra_vals_id)
                    caracteristica_nom = caracteristica_data['name']
                    caracteristica_value = caracteristica_data['value']
                    caracteristica_value = caracteristica_value.strip()
                    # Ens quedem amb les característiques que ens interessen
                    if caracteristica_nom in CARACTERISTIQUES_VALIDES and caracteristica_value:
                        caracteristiques[caracteristica_nom] = caracteristica_value
        return caracteristiques

    def update_values(self, cursor, uid, res, name, value):
        """
        Depenent del name rebut per paràmetre, actualizta el valor corresponent al formulari
        :param cursor:
        :param uid:
        :param res:
        :param name: nom del camp a modificar
        :param value: valor a assignar
        :return:
        """
        if name == 'giro':
            # actualizta el valor del giro
            if value.isdigit():
                res['value'].update({'giro': int(value)})
        if name == 'technology' and value in [x[0] for x in TECHNOLOGY_TYPE_SEL]:
            # actualitza la tecnologia, i activa/desactiva la check de telegestió
            res['value'].update({'technology_type': value})
            if value in ['prime', 'smmweb']:
                res['value'].update({'tg': True})
            else:
                res['value'].update({'tg': False})

    _columns = {
        'serial': fields.many2one("stock.production.lot",
                                  "Nº de sèrie (magatzem)", select="1"),
        # 'name': fields.related('serial', 'name', type='char',
        #                        string="Nº de sèrie", store=True,
        #                       readonly=True, size=32),
        'product_id': fields.related('serial', 'product_id', type='many2one',
                                     relation='product.product', store=True,
                                     readonly=True, string="Producte"),
        'tensio': fields.integer('Tensió'),
        'cini': fields.char('CINI', size=256),
        'bloquejar_cini': fields.boolean('Bloquejar CINI'),
        'num_precinte': fields.char("Num. Precinte", 50),
        'data_precinte': fields.date("Data Precinte"),
    }

    _defaults = {
        'bloquejar_cini': lambda *a: 0,
    }


GiscedataLecturesComptador()
