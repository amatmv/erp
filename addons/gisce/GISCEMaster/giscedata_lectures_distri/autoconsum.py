# coding=utf-8
from osv import fields  # ,osv
from mongodb_backend import osv_mongodb


class CchAutocons(osv_mongodb.osv_mongodb):

    _name = 'cch_autocons'
    _time_column = 'datetime'

    _columns = {
        'name': fields.char('CUPS', size=22, required=True),
        'datetime': fields.datetime('Fecha y hora medida', required=True),
        'create_at': fields.datetime('Fecha y hora creada'),
        'update_at': fields.datetime('Fecha y hora actualizada'),
        'season': fields.selection([(0, 'Invierno'),
                                    (1, 'Verano')], 'Verano/Invierno'),
        'ae': fields.float('Activa entrante', required=True),
        'as': fields.float('Activa saliente'),
        'r1': fields.float('Reactiva cuadrante 1'),
        'r2': fields.float('Reactiva cuadrante 2'),
        'r3': fields.float('Reactiva cuadrante 3'),
        'r4': fields.float('Reactiva cuadrante 4'),
        'bill': fields.char('Código de factura', size=26)

    }


CchAutocons()


class CchGenNetaBeta(osv_mongodb.osv_mongodb):

    _name = 'cch_gennetabeta'
    _time_column = 'datetime'

    _columns = {
        'name': fields.char('CUPS', size=22, required=True),
        'datetime': fields.datetime('Fecha y hora medida', required=True),
        'create_at': fields.datetime('Fecha y hora creada'),
        'update_at': fields.datetime('Fecha y hora actualizada'),
        'season': fields.selection([(0, 'Invierno'),
                                    (1, 'Verano')], 'Verano/Invierno'),
        'ae': fields.float('Activa entrante'),
        'as': fields.float('Activa saliente', required=True),
        'r1': fields.float('Reactiva cuadrante 1'),
        'r2': fields.float('Reactiva cuadrante 2'),
        'r3': fields.float('Reactiva cuadrante 3'),
        'r4': fields.float('Reactiva cuadrante 4'),
        'bill': fields.char('Código de factura', size=26)

    }


CchGenNetaBeta()
