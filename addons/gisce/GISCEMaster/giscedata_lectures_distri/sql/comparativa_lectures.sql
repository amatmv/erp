SELECT polissa,
       data_actual,
       dies_actuals,
       consum_actual,
       consum_dia_actual,
       CASE WHEN to_number(consum_dia,'99999999999999990D99') = 0.00 THEN '0' ELSE trim(to_char((((to_number(consum_dia_actual,'99999999999999990D99')*100.0/to_number(consum_dia,'99999999999999990D99'))-100.0)),'99999999999999990')) END || '%%' as n1,
       CASE WHEN to_number(consum_dia_anual,'99999999999999990D99') = 0.00 THEN '0' ELSE trim(to_char((((to_number(consum_dia_actual,'99999999999999990D99')*100.0/to_number(consum_dia_anual,'99999999999999990D99'))-100.0)),'99999999999999990')) END || '%%' as n12, 
       data_inici,
       data_final,
       dies_ultim,
       quantitat,
       consum_dia,
       data_inici_anual,
       data_final_anual,
       dies_ultim_anual,
       quantitat_anual,
       consum_dia_anual
        FROM 
        (
            SELECT p.name as polissa, data_actual, data_actual-data_final as dies_actuals, consum_actual, trim(to_char(consum_actual / cast((data_actual-data_final) as float),'99999999999999990D99'))  as consum_dia_actual, 
            data_inici, data_final,data_final - data_inici as dies_ultim, 
            -- subconsultes a la query
            (select sum(quantity)
                from giscedata_facturacio_factura f 
                left join giscedata_facturacio_factura_linia l on (l.factura_id = f.id and l.tipus = 'energia')
                left join account_invoice_line il on (l.invoice_line_id = il.id)
                where f.polissa_id = total.polissa_id and f.data_final < total.data_actual and f.tipo_rectificadora in ('N', 'R') and f.id not in (
                    select ref from giscedata_facturacio_factura where tipo_rectificadora in ('A', 'B', 'R', 'BRA','RA') and polissa_id = total.polissa_id
                )
                group by f.id, f.data_inici,f.data_final
                order by f.data_final desc limit 1) as quantitat
            ,
            trim(to_char(((select sum(quantity)
                            from giscedata_facturacio_factura f 
                            left join giscedata_facturacio_factura_linia l on (l.factura_id = f.id and l.tipus = 'energia')
                            left join account_invoice_line il on (l.invoice_line_id = il.id)
                            where f.polissa_id = total.polissa_id and f.data_final < total.data_actual and f.tipo_rectificadora in ('N', 'R') and f.id not in (
                                select ref from giscedata_facturacio_factura where tipo_rectificadora in ('A', 'B', 'R', 'BRA','RA') and polissa_id = total.polissa_id
                            )
                            group by f.id, f.data_inici,f.data_final
                            order by f.data_final desc limit 1)/cast((total.data_final-total.data_inici) as float)),'99999999999999990D99')) as consum_dia,
            -- Referencia anual
            data_inici_anual,data_final_anual, data_final_anual - data_inici_anual as dies_ultim_anual, 
            -- subconsultes a la query
            (select sum(quantity)
                from giscedata_facturacio_factura f 
                left join giscedata_facturacio_factura_linia l on (l.factura_id = f.id and l.tipus = 'energia')
                left join account_invoice_line il on (l.invoice_line_id = il.id)
                where f.polissa_id = total.polissa_id and total.data_actual_anual BETWEEN f.data_inici AND f.data_final and f.tipo_rectificadora in ('N', 'R') and f.id not in (
                    select ref from giscedata_facturacio_factura where tipo_rectificadora in ('A', 'B', 'R', 'BRA','RA') and polissa_id = total.polissa_id
                )
                group by f.id, f.data_inici,f.data_final
                order by f.data_final desc limit 1) as quantitat_anual
            ,
            trim(to_char(((select sum(quantity)
                            from giscedata_facturacio_factura f 
                            left join giscedata_facturacio_factura_linia l on (l.factura_id = f.id and l.tipus = 'energia')
                            left join account_invoice_line il on (l.invoice_line_id = il.id)
                            where f.polissa_id = total.polissa_id and total.data_actual_anual BETWEEN f.data_inici AND f.data_final and f.tipo_rectificadora in ('N', 'R') and f.id not in (
                                select ref from giscedata_facturacio_factura where tipo_rectificadora in ('A', 'B', 'R', 'BRA','RA') and polissa_id = total.polissa_id
                            )
                            group by f.id, f.data_inici,f.data_final
                            order by f.data_final desc limit 1)/cast((total.data_final_anual-total.data_inici_anual) as float)),'99999999999999990D99')) as consum_dia_anual
            FROM (
                SELECT actual.polissa_id, MAX(f.data_final) as data_final,max(f.data_inici) as data_inici, MAX(f2.data_final) as  data_final_anual,max(f2.data_inici) as data_inici_anual
                , MAX(actual.data_actual) as data_actual, MAX(actual.data_actual_anual) as data_actual_anual, MAX(actual.consum_actual) as consum_actual
                FROM (
                    SELECT max(l.name) as data_actual, max(l.name)- interval '1 year' as data_actual_anual, coalesce(sum(l.consum), 0) as consum_actual, p.id as polissa_id
                    FROM giscedata_lectures_lectura l
                    LEFT JOIN  giscedata_lectures_comptador c on (l.comptador = c.id) 
                    LEFT JOIN  giscedata_polissa p on (c.polissa = p.id)
                    LEFT JOIN  giscedata_polissa_tarifa_periodes pe on (l.periode = pe.id)
                    LEFT JOIN  giscedata_polissa_tarifa t on (pe.tarifa = t.id)
                    where to_char(l.name, 'MM/YYYY') in ( %s ) and l.tipus = 'A' and t.id = p.tarifa
                    group by p.name, p.id
                ) actual
                LEFT JOIN giscedata_facturacio_factura f on (actual.polissa_id=f.polissa_id AND (NOT actual.data_actual BETWEEN f.data_inici AND f.data_final AND f.data_final < actual.data_actual
                        AND f.tipo_rectificadora in ('N', 'R') and f.id not in (
                            SELECT ref 
                            FROM giscedata_facturacio_factura 
                            WHERE tipo_rectificadora in ('A', 'B', 'R', 'BRA', 'RA') AND polissa_id = f.polissa_id
                )))
        LEFT JOIN giscedata_facturacio_factura f2 on (actual.polissa_id=f2.polissa_id AND 
            (
                actual.data_actual_anual BETWEEN f2.data_inici AND f2.data_final
                AND f2.tipo_rectificadora in ('N', 'R') and f2.id not in (
                    SELECT ref 
                    FROM giscedata_facturacio_factura 
                    WHERE tipo_rectificadora in ('A', 'B', 'R', 'BRA', 'RA') AND polissa_id = f2.polissa_id
                )
        ))  
    GROUP BY actual.polissa_id
    ORDER BY actual.polissa_id

) total
LEFT JOIN giscedata_polissa p on (p.id = total.polissa_id)
) final ;   
