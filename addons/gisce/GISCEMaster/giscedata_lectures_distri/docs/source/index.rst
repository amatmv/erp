.. Documentació de lectures per distribuïdores master file, created by
   sphinx-quickstart on Tue Jun 25 16:18:37 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentació de lectures per distribuïdores 
============================================

Continguts:

.. toctree::
   :maxdepth: 2

   almacenes_comptadors
   comparativa_lectures
   
.. Indices and tables
.. ==================
    * :ref:`genindex`
    * :ref:`search`

