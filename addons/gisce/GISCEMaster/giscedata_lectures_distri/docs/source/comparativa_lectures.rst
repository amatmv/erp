.. Generació de fitxer d'exportació amb les comparatives de lectures documentation master file, created by
   sphinx-quickstart on Tue Jun 25 16:18:37 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Comparativa de consums
**********************

Introducció
===========
Es tracta de comparar el consum actual, en base a les lectures introduïdes al
ERP abans de facturar, amb els consums de les factures del mes anterior (n-1) i
del mateix mes de l'any anterior (n-12).

En aquest manual s'explica com generar una exportació de les comparatives 
de lectures per un mes concret.

* Com exportar un fitxer
* Camps del fitxer

El fitxer exportat contindrà la informació de consum actual del mes indicat 
amb la comparativa de l'anterior factura i la de fa un any.

Per poder realitzar les comparatives entre diferents consums i donat que aquests
poden tenir diferent número de dies, s'ha procedit a calcular el consum diari. 
Aquest és la divisió entre el consum i el número de dies d'aquest i així obtenir
una homogenització del consum entre períodes diferents i poder fer-ne una comparació
molt més acurada.

Com exportar un fitxer
======================

Per accedir a generar l'exportació cal anar a 
``Facturació > Lectures > Exportació comparativa de lectures``.

En el formulari que apareixerà a continuació s'ha d'indicar el mes que es vol
fer la comparativa amb el següent format: ``mm/yyyy``.

Un cop introduit el mes desitjat s'ha de clicar el botó d'exportar i aquest
generarà el fitxer amb les comparatives.



.. figure:: /_static/comparativa_lectures.png
    :width: 500px

    Finestra de l'exportació de comparativa de lectures


Camps del fitxer
================

A continuació es descriu cada un dels camps del fitxer.

**Pòlissa**
    Número de pòlissa.

**Data_última_lectura**
    Dia de l'última lectura del mes indicat.

**Dies_última_lectura**
    Dies que hi ha entre l'última lectura i la data final de la factura anterior.

**Consum_actual**
    Consum que hi ha des de l'anterior factura a l'última lectura.

**Consum_diari_actual**
    Proporció entre el *Consum_actual* i els *Dies_última_lectura*.

**%_n-1**
    Comportament en percentatge entre el *Consum_diari_actual* i *Consum_diari_factura_anterior*. 
    (Per exemple: un -25%, significa que el consum actual és un 25% menor al 
    consum de la factura anterior.)

**%_n-12**
    Comportament en percentatge entre el *Consum_diari_actual* i *Consum_diari_factura_any_anterior*.
    (Per exemple: un 14%, significa que el consum actual és un 13% superior al
    consum de la factura de l'any anterior)
    

**Data_inici_factura_anterior**
    Data d'inici de la factura anterior.

**Data_final_factura_anterior**
    Data final de la factura anterior.

**Dies_factura_anterior**
    Dies entre la *Data_inici_factura_anterior* i *Data_final_factura_anterior*.

**Consum_factura_anterior**
    Consum que es va facturar en la factura anterior.

**Consum_diari_factura_anterior**
    Proporció entre el *Consum_diari_factura_anterior* i els
    *Dies_factura_anterior*.

**Data_inici_factura_any_anterior**
    Data d'inici de la factura de l'any anterior al mes indica al mes indicat.
    
**Data_final_factura_any_anterior**
    Data final de la factura de l'any anterior al mes indica al mes indicat.
    
**Dies_factura_any_anterior**
    Dies entre la *Data_inici_factura_any_anterior* i *Data_final_factura_any_anterior*.

**Consum_factura_any_anterior**
    Consum que es va facturar en la factura de l'any anterior.

**Consum_diari_factura_any_anterior**
    Proporció entre el *Consum_diari_factura_any_anterior* i els
    *Dies_factura_any_anterior*.


