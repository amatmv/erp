# -*- coding: utf-8 -*-
def migrate(cursor, installed_version):
    cursor.execute("""ALTER TABLE giscedata_lectures_comptador
    DROP CONSTRAINT giscedata_lectures_comptador_name_fkey""")
    cursor.execute("""ALTER TABLE giscedata_lectures_comptador
    RENAME name TO serial""")
    cursor.execute("""ALTER TABLE giscedata_lectures_comptador
    ADD COLUMN name character varying(32)""")
    cursor.execute("""ALTER TABLE giscedata_lectures_comptador
    ADD CONSTRAINT giscedata_lectures_comptador_serial_fkey FOREIGN KEY (serial) REFERENCES stock_production_lot(id) ON DELETE SET NULL""")
    cursor.execute("SELECT c.id,s.name from giscedata_lectures_comptador c, stock_production_lot s where c.serial = s.id")
    for c_id, name in cursor.fetchall():
        cursor.execute("UPDATE giscedata_lectures_comptador set name=%s where id=%s", (name, c_id))
    cursor.execute("""ALTER TABLE giscedata_lectures_comptador
    ALTER column name set NOT NULL""")