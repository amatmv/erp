# -*- coding: utf-8 -*-
def migrate(cursor, installed_version):
    cursor.execute("""ALTER TABLE giscedata_lectures_comptador
    DROP CONSTRAINT giscedata_lectures_comptador_product_id_fkey""")
    cursor.execute("""ALTER TABLE giscedata_lectures_comptador
    RENAME product_id TO product_lloguer_id""")
    cursor.execute("""ALTER TABLE giscedata_lectures_comptador
    ADD CONSTRAINT giscedata_lectures_comptador_product_lloguer_id_fkey FOREIGN KEY (product_lloguer_id) REFERENCES product_product(id) ON DELETE SET NULL""")
    cursor.execute("""UPDATE giscedata_lectures_comptador
    SET lloguer = False, propietat = 'client' where product_lloguer_id is null""")