# -*- coding: utf-8 -*-
{
    "name": "Obres per Subestacions",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Obres a Posicions de subestació
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "giscedata_cts_subestacions",
        "giscedata_obres",
        "giscedata_administracio_publica_cnmc_distri"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_cts_subestacions_view.xml"
    ],
    "active": False,
    "installable": True
}
