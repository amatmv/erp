# -*- coding: utf-8 -*-

from osv import osv, fields


class GiscedataCtsSubestacionsPosicio(osv.osv):
    _name = 'giscedata.cts.subestacions.posicio'
    _inherit = 'giscedata.cts.subestacions.posicio'

    _columns = {
        'obres': fields.many2many(
            'giscedata.obres.obra',
            'giscedata_obres_obra_cts_subestacions_posicio_rel',
            'posicio_id',
            'obra_id',
            'Obres'
        )
    }

GiscedataCtsSubestacionsPosicio()