# -*- coding: utf-8 -*-
{
    "name": "Giscedata Stock Location",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
- Ubicació en localització per lots de producció
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends": [
        'stock',
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        'giscedata_stock_view.xml',
    ],
    "active": False,
    "installable": True
}
