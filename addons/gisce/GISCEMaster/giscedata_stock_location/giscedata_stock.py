# -*- encoding: utf-8 -*-
from osv import fields, osv


class stock_production_lot(osv.osv):
    _name = "stock.production.lot"
    _inherit = "stock.production.lot"

    def get_location(self, cursor, uid, ids, field_name, arg=None, context=None):
        """
        Obtains the last stock movement's destination of a production lot.
        :param cursor:
        :param uid:
        :param ids:
        :param field_name:
        :param arg:
        :param context:
        :return: Dictionary where the key is <stock.production.lot> id and the
        value is the <stock.location> id.
        """
        q = """
        SELECT sm.location_dest_id AS location
        FROM stock_move AS sm
        WHERE sm.prodlot_id = %s
        ORDER BY sm.date_planned DESC LIMIT 1
        """
        vals = {}
        for stock_production_lot_id in ids:
            cursor.execute(q, (stock_production_lot_id,))
            stock_production_lot_v = cursor.dictfetchall()
            if stock_production_lot_v:
                vals[stock_production_lot_id] = stock_production_lot_v[0]['location']

        return vals

    def _store_location_id_stock_movement(self, cursor, uid, ids, context=None):
        """
        Obtains the the production lots that were moved using a stock movement.
        :param cursor:
        :param uid:
        :param ids:
        :param context: <stock.move> ids.
        :return: list of <stock.production.lot> ids.
        """
        dmn = [('id', 'in', ids), ('state', 'in', ['done'])]
        stock_move_f = ['prodlot_id.id']
        q = self.q(cursor, uid).select(stock_move_f).where(dmn)
        cursor.execute(*q)
        stock_production_lot_ids = [stock_move_v['prodlot_id.id'] for stock_move_v in cursor.dictfetchall()]
        return stock_production_lot_ids

    _STORE_LOCATION_ID = {
        'stock.move': (
            _store_location_id_stock_movement, ['state'], 10
        )
    }

    _columns = {
        'location_id': fields.function(
            get_location, store=_STORE_LOCATION_ID, method=True,
            type='many2one', string='Ubicación', relation='stock.location'
        ),
        'fila': fields.char('Fila', 50),
        'columna': fields.char('Columna', 50),
    }


stock_production_lot()
