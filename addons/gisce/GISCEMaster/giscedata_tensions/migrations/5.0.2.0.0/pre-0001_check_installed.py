from addons.gisceupgrade_migratoor.gisceupgrade_migratoor import (
    log,
    GisceUpgradeMigratoor)

def migrate(cursor, installed_version):
    mig = GisceUpgradeMigratoor('giscedata_tensions', cursor)
    if (not mig.model_exists('giscedata_tensions_tensio')
        or (
            cursor.execute("select id from giscedata_tensions_tensio")
            and not cr.rowcount)):
        log("giscedata_tensions unused, no migrate!")
    else:
        mig.backup(suffix='v4')
        mig.pre_xml()
