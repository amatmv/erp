# -*- coding: utf-8 -*-
{
    "name": "GISCE Data Tensions",
    "description": """
Models per normalitzar totes les tensions. Permet:
  * Configurar diferents tensions normalitzades, establint uns llindars.
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base"
    ],
    "init_xml": [],
    "demo_xml": [
        "giscedata_tensions_demo.xml",
    ],
    "update_xml": [
        "giscedata_tensions_view.xml",
        "giscedata_tensions_data.xml",
        "ir.model.access.csv",
        "security/giscedata_tensions_security.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
