# -*- coding: utf-8 -*-
"""Mòdul giscedata_tensions."""
from __future__ import absolute_import

from osv import osv, fields
from gestionatr.defs import TABLA_64


class GiscedataTensionsTensio(osv.osv):
    """Tensió d'una tarifa."""
    _name = 'giscedata.tensions.tensio'

    def _cnmc_selection(self, cursor, uid, context):
        return [(t[0], "({0}) - {1}".format(t[0], t[1])) for t in TABLA_64]

    def get_cnmc_name(self, cursor, uid, cnmc_code):
        """
        Funció que donat un codi cnmc ens retorna el nom ex.6 -> 3x230/400

        :param cursor: Database cursor
        :param uid: User id
        :param cnmc_code:
        :return:
        :rtype: dict
        """
        return dict(TABLA_64)[cnmc_code]

    def get_cnmc_code(self, cursor, uid, cnmc_name):
        """
        Funció que donat un nom de tensio ens retorna el codi cnmc
        corresponent ex.3x230/400 -> 6

        :param cursor: Database cursor
        :param uid: User id
        :param cnmc_name:
        :return:
        """
        found = [k for k, v in TABLA_64 if v == cnmc_name.upper()]
        return found and found[0] or False

    def _check_tipus(self, cursor, uid, ids):
        """Comprova si és AT o BT."""
        for tensio in self.browse(cursor, uid, ids):
            if tensio.tensio >= 1000 and tensio.tipus == 'BT':
                return False
            if tensio.tensio < 1000 and tensio.tipus == 'AT':
                return False
        return True
    
    _columns = {
        'name': fields.char('Codi tensió normalitzada', size=25, required=True,
                            select=True),
        'l_inferior': fields.integer('Llindar inferior', required=True),
        'l_superior': fields.integer('Llindar superior', required=True),
        'active': fields.boolean('Active'),
        'tensio': fields.integer('Tensió a aplicar', required=True),
        'tipus': fields.selection(
            [('AT', 'AT'), ('BT', 'BT')], 'Tipus', required=True
        ),
        'cnmc_code': fields.selection(
            _cnmc_selection, 'Codi CNMC',
            help='Codi de la CNMC que identifica la tensió. imprescindible per '
                 'a la generació de fitxers com els SIPS i la gestió ATR'
        ),
    }
    
    
    _constraints = [(_check_tipus, 'Error: Tipus mal escollit', ['tipus']), ] 
    _sql_constraints = [('name_uniq', 'unique (name)',
                         'El codi de la tensió normalitzada ha de ser unic.'),]
    
    _defaults = {
        'active': lambda *a: True,
        'tensio': lambda *a: 0,
        'tipus': lambda *a: 'BT',
    }

GiscedataTensionsTensio()
