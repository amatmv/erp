# -*- coding: utf-8 -*-

import netsvc


def migrate(cursor, installed_version):
    """
    Updates old permissions using groups from module giscedata_cne
     to groups from giscedata_admin_pub_cne
    :param cursor: Cursor from OpenERP
    :param installed_version: Last installed version of the module from OpenERP
    :return: None
    """
    logger = netsvc.Logger()
    if not installed_version:
        return
    logger.notifyChannel(
        'migration', netsvc.LOG_INFO,
        'Updating permissions for CNE from other CNE modules'
    )
    local = 'giscedata_admin_pub_cne'
    modules = [
        'giscedata_cne',
    ]
    grups = {
        'giscedata_admin_pub_cne': [
            'group_giscedata_admin_pub_cne_r',
            'group_giscedata_admin_pub_cne_w',
            'group_giscedata_admin_pub_cne_u',
        ],
        'giscedata_cne': [
            'group_giscedata_cne_r',
            'group_giscedata_cne_w',
            'group_giscedata_cne_u',
        ]
    }
    query = "UPDATE ir_model_access" \
            " SET group_id= (SELECT res_id FROM ir_model_data " \
            " WHERE model='res.groups' AND name='{0}' AND module='{1}')" \
            " WHERE group_id = (SELECT res_id FROM ir_model_data " \
            " WHERE model='res.groups' AND name='{2}' AND module='{3}')"
    for mod_name in modules:
        logger.notifyChannel(
            'migration', netsvc.LOG_INFO,
            'Updating group permissions from ' + mod_name +
            ' to ' + local
        )
        for admin, other in zip(grups[local], grups[mod_name]):
            logger.notifyChannel(
                'migration', netsvc.LOG_INFO,
                'Updating ' + other + ' to ' + admin
            )
            q = query.format(admin, local, other, mod_name)
            cursor.execute(q)
