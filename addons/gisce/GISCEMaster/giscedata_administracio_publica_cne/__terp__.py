# -*- coding: utf-8 -*-
{
    "name": "Administració pública",
    "description": """Crea el menú d'administració pública com a base CNE""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_administracio_publica"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_administracio_publica_cne_view.xml",
        "security/giscedata_admin_pub_cne_security.xml"
    ],
    "active": False,
    "installable": True
}
