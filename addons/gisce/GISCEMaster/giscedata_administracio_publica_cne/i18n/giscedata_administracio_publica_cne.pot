# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* giscedata_administracio_publica_cne
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.14\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2019-07-29 11:49\n"
"PO-Revision-Date: 2019-07-29 11:49\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: giscedata_administracio_publica_cne
#: model:ir.module.module,description:giscedata_administracio_publica_cne.module_meta_information
msgid "Crea el menú d'administració pública com a base CNE"
msgstr ""

#. module: giscedata_administracio_publica_cne
#: model:ir.module.module,shortdesc:giscedata_administracio_publica_cne.module_meta_information
msgid "Administració pública"
msgstr ""

#. module: giscedata_administracio_publica_cne
#: model:ir.ui.menu,name:giscedata_administracio_publica_cne.menu_giscedata_cnmc_resolucions
msgid "Resolucions"
msgstr ""

#. module: giscedata_administracio_publica_cne
#: model:ir.ui.menu,name:giscedata_administracio_publica_cne.menu_cne_circulars
msgid "Circulars"
msgstr ""

#. module: giscedata_administracio_publica_cne
#: model:ir.ui.menu,name:giscedata_administracio_publica_cne.menu_admin_pub_cne
msgid "CNMC"
msgstr ""

