# -*- encoding: utf-8 -*-
import logging
from osv import osv, fields
from tools.translate import _


class WizardFacturacioGroup(osv.osv_memory):
    _name = 'wizard.obrir.factures.group'
    _rec_name = 'lot_id'

    _columns = {
        'lot_id': fields.many2one(
            'giscedata.facturacio.lot', 'Lot', required=True
        ),
        'group_id': fields.many2one(
            'giscedata.polissa.group', 'Grup',
            help="Deixar en blanc per obrir totes les factures"
                 " en esborrany que pugi"
        ),
        'info': fields.text('Info')
    }

    _defaults = {
        'lot_id': lambda *a: a[3].get('active_id', False)
    }

    def obrir_factures(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        logger = logging.getLogger('openerp.{0}.obrir_factures'.format(__name__))
        lot_obj = self.pool.get('giscedata.facturacio.lot')
        wiz = self.browse(cursor, uid, ids[0], context=context)

        ctx = context.copy()
        ctx['active_test'] = False
        ctx['extra_filter'] = []
        message = ('Open invoices from lot {wiz.lot_id.name}'
                   ' (id: {wiz.lot_id.id}) '.format(wiz=wiz))

        if wiz.group_id:
            ctx['extra_filter'].append(
                ('polissa_id.group_id.id', '=', wiz.group_id.id)
            )
            message += ' with group {wiz.group_id.name}'.format(wiz=wiz)
        info = _(u"S'ha llançat un procés en segon pla per "
                 u"obrir les factures en esborrany."
                 u"Es pot tancar la finestra."
                 )
        try:
            logger.info(message)
            lot_obj.obrir_factures_button(
                cursor, uid, [wiz.lot_id.id], context=ctx
            )
        except Exception as e:
            sentry = self.pool.get('sentry.setup')
            if sentry is not None:
                sentry.client.captureException()
            info = e.message

        wiz.write({
            'info': info
        })

WizardFacturacioGroup()
