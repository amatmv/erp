# -*- encoding: utf-8 -*-

from osv import osv, fields
from tools.translate import _


class WizardValidarGroup(osv.osv_memory):
    _name = 'wizard.validar.group'
    _rec_name = 'lot_id'

    _columns = {
        'lot_id': fields.many2one(
            'giscedata.facturacio.lot', 'Lot', required=True
        ),
        'group_id': fields.many2one(
            'giscedata.polissa.group', 'Grup',
            help="Deixar en blanc per facturar tot el que sigui possible"
        ),
        'info': fields.text('Info')
    }

    _defaults = {
        'lot_id': lambda *a: a[3].get('active_id', False)
    }

    def validar(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        lot_obj = self.pool.get('giscedata.facturacio.lot')
        wiz = self.browse(cursor, uid, ids[0], context=context)

        ctx = context.copy()
        ctx['active_test'] = False
        ctx['extra_filter'] = []

        if wiz.group_id:
            ctx['extra_filter'].append(
                    ('polissa_id.group_id.id', '=', wiz.group_id.id)
            )
        info = _(u"S'ha llançat un procés en segon pla per fer "
                  u"la validació."
                  u"Es pot tancar la finestra."
                  )
        try:
            lot_obj.validar_button(
                    cursor, uid, [wiz.lot_id.id], context=ctx
            )
        except Exception as e:
            sentry = self.pool.get('sentry.setup')
            if sentry is not None:
                sentry.client.captureException()
            info = e.message
        wiz.write({
            'info': info
        })

WizardValidarGroup()
