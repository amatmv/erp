# -*- coding: utf-8 -*-
{
    "name": "Facturació grup",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Seleccionar contractes a facturar segons el grup de la pòlissa
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "giscedata_facturacio",
        "giscedata_polissa_group"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/wizard_facturacio_group_view.xml",
        "wizard/wizard_obrir_factures_group_view.xml",
        "wizard/wizard_validar_group_view.xml",
        "giscedata_facturacio_view.xml",
        "security/ir.model.access.csv",
        "giscedata_polissa_view.xml"
    ],
    "active": False,
    "installable": True
}
