from osv import osv, fields


class GiscedataFacturacioContracteLot(osv.osv):
    _name = 'giscedata.facturacio.contracte_lot'
    _inherit = 'giscedata.facturacio.contracte_lot'

    _columns = {
        'group_id': fields.related(
            'polissa_id', 'group_id',
            type='many2one',
            relation='giscedata.polissa.group',
            string='Grup',
            store=True
        )
    }

GiscedataFacturacioContracteLot()
