# -*- coding: utf-8 -*-
from osv import osv
import netsvc
from giscedata_comerdist import OOOPPool, Sync

class GiscedataPolissa(osv.osv):
    """Pòlissa sincronització sóller.
    """
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def ws_signal_avis1(self, cursor, uid, ids, context=None):
        """Trigger per avis1.
        """
        wf_service = netsvc.LocalService('workflow')
        for polissa_id in ids:
            wf_service.trg_validate(uid, 'giscedata.polissa', polissa_id,
                                    'avis1', cursor)
        return True

    def wkf_avis1(self, cursor, uid, ids):
        """Sobreescrivim l'estat avis1 per sincronitzar.
        """
        for polissa in self.browse(cursor, uid, ids):
            if polissa.must_be_synched():
                config_id = polissa.get_config().id
                ooop = OOOPPool.get_ooop(cursor, uid, config_id)
                sync = Sync(cursor, uid, ooop, self._name, config=config_id)
                obj = sync.get_object(polissa.id)
                if obj:
                    obj.ws_signal_avis1()
        super(GiscedataPolissa, self).wkf_avis1(cursor, uid, ids)
        return True

    def ws_signal_avis2(self, cursor, uid, ids, context=None):
        """Trigger per avis1.
        """
        wf_service = netsvc.LocalService('workflow')
        for polissa_id in ids:
            wf_service.trg_validate(uid, 'giscedata.polissa', polissa_id,
                                    'avis2', cursor)
        return True

    def wkf_avis2(self, cursor, uid, ids):
        """Sobreescrivim l'estat avis2 per sincronitzar.
        """
        for polissa in self.browse(cursor, uid, ids):
            if polissa.must_be_synched():
                config_id = polissa.get_config().id
                ooop = OOOPPool.get_ooop(cursor, uid, config_id)
                sync = Sync(cursor, uid, ooop, self._name, config=config_id)
                obj = sync.get_object(polissa.id)
                if obj:
                    obj.ws_signal_avis2()
        super(GiscedataPolissa, self).wkf_avis2(cursor, uid, ids)
        return True

GiscedataPolissa()
