# -*- coding: utf-8 -*-
{
    "name": "Comerdist (Sóller)",
    "description": """
    This module provide :
    Sincronització especial per Sóller:
      * Sincronitza els estats avis1 i avis2
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_comerdist_polissa"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
