# -*- coding: utf-8 -*-
{
    "name": "Factura-e",
    "description": """Export facturae in facturae v3.2.1 format""",
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Account",
    "depends":[
        "account",
        "account_payment",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "partner_view.xml",
        "payment_type_view.xml",
        "account_invoice_view.xml",
        "attachment_categories_data.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
