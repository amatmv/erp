# -*- encoding: utf-8 -*-

from osv import osv
from tools import config
from facturae import facturae
import netsvc
import base64
import tempfile
import subprocess
from tools.translate import _
import magic
import os


class AccountInvoiceFacturae(osv.osv):

    _name = 'account.invoice.facturae'
    _auto = False

    @staticmethod
    def get_invoice_sign(invoice):
        if invoice.type.endswith('refund'):
            return -1
        return 1

    def facturae_header(self, cursor, uid, invoice, context=None):
        '''Generate facturae header'''

        header = facturae.FileHeader()
        batch = facturae.Batch()

        company = invoice.company_id.partner_id
        sign = self.get_invoice_sign(invoice)

        identifier = '%s%s' % (invoice.number,
                               company.vat[2:]) 

        total1 = facturae.Totals('TotalInvoicesAmount')
        total1.feed({
            'totalamount': sign * invoice.amount_total,
        })

        total2 = facturae.Totals('TotalOutstandingAmount')
        total2.feed({
            'totalamount': sign * invoice.amount_total,
        })

        total3 = facturae.Totals('TotalExecutableAmount')
        total3.feed({
            'totalamount': sign * invoice.amount_total,
        })

        batch.feed({
            'batchidentifier': identifier,
            'invoicescount': 1,
            'totalinvoicesamount': total1,
            'totaloutstandingamount': total2,
            'totalexecutableamount': total3,
            'invoicecurrencycode': 'EUR'
        })

        header.feed({
            'schemaversion': '3.2.1',
            'modality': 'I',
            'invoiceissuertype': 'EM',
            'batch': batch,
        })

        return header

    def facturae_administrative_centre(self, administrative, center_address,
                                       info=None, context=None):
        centre = facturae.AdministrativeCentre()
        centre.feed({
            'centrecode': administrative.code,
            'roletypecode': administrative.role,
            'addressinspain': center_address,
        })
        return centre

    def facturae_party(self, cursor, uid, partner, address=None,
                       type='seller', info=None, context=None):

        partner_obj = self.pool.get('res.partner')
        address_obj = self.pool.get('res.partner.address')

        if type == 'seller':
            party = facturae.Party('SellerParty')
        else:
            party = facturae.Party('BuyerParty')
        taxidentification = facturae.TaxIdentification()

        partner_type = (partner_obj.vat_es_empresa(cursor, uid, partner.vat)
                        and 'J' or 'F')
        residence_type_code = (
            partner.country_id and partner.country_id.code == 'ES' and 'R'
            or 'U'
        )
 
        taxidentification.feed({
            'persontypecode': partner_type,
            'residencetypecode': residence_type_code,
            'taxidentificationnumber': partner.vat[2:] 
        })

        if address is None:
            address = partner_obj.address_get(cursor, uid, [partner.id],
                                              ['invoice'])
            address = address_obj.browse(cursor, uid, address['invoice'],
                                         context=context)

        legalentity = facturae.LegalEntity()

        xmladdress = facturae.AddressInSpain()
        xmladdress.feed({
            'address': address.street,
            'postcode': address.zip,
            'town': address.city,
            'province': address.state_id.name,
            'countrycode': 'ESP'
        })

        contact = facturae.ContactDetails()
        contact.feed({
            'telephone': '34%s' % address.phone,
            'electronicmail': address.email,
        })

        legalentity.feed({
            'corporatename': partner.name,
            'tradename': partner.name[:40],
            'addressinspain': xmladdress,
            'contactdetails': contact,
        })

        if type == 'seller':
            party.feed({
                'taxidentification': taxidentification,
                'legalentity': legalentity,
            })
        else:
            admin_centres = facturae.AdministrativeCentres()
            centres = []
            for administrative in address.administrative_ids:
                center_address = facturae.AddressInSpain()
                center_address.feed({
                    'address': address.street,
                    'postcode': address.zip,
                    'town': address.city,
                    'province': address.state_id.name,
                    'countrycode': 'ESP'
                })
                centre = self.facturae_administrative_centre(
                    administrative, center_address, info=info, context=context)
                centres.append(centre)
            admin_centres.feed({
                'administrativecentre': centres,
            })

            party.feed({
                'taxidentification': taxidentification,
                'administrativecentres': admin_centres,
                'legalentity': legalentity,
            })

        return party            

    def facturae_parties(self, cursor, uid, invoice, info=None, context=None):
        '''Generate facturae parties'''
        
        company = invoice.company_id.partner_id
        seller = self.facturae_party(cursor, uid, company,
                                     type='seller',
                                     context=context)

        partner = invoice.partner_id
        address = invoice.address_invoice_id
        buyer = self.facturae_party(cursor, uid, partner,
                                    address=address,
                                    type='buyer',
                                    info=info,
                                    context=context)

        parties = facturae.Parties()
        parties.feed({
            'sellerparty': seller,
            'buyerparty': buyer,
        })

        return parties

    def facturae_issue_data(self, cursor, uid, invoice, context=None):
        issuedata = facturae.InvoiceIssueData()
        issuedata.feed({
            'issuedate': invoice.date_invoice,
            'invoicecurrencycode': invoice.currency_id.code,
            'taxcurrencycode': invoice.currency_id.code,
            'languagename': 'es',
        })

        return issuedata

    def facturae_tax_code(self, cursor, uid, tax_name, context=None):

        if 'iva' in tax_name:
            code = '01'
        elif 'especial' in tax_name:
            code = '07'
        else:
            code = '05' 

        return code

    def facturae_taxes(self, cursor, uid, invoice, context=None):
        '''Generate facturae taxes'''

        tax_obj = self.pool.get('account.tax')

        taxes = facturae.Taxes('TaxesOutputs')

        tax_list = []
        for tax_line in invoice.tax_line:
            tax_name = tax_line.name.lower()
            code = self.facturae_tax_code(cursor, uid, tax_name)

            # Search for rate
            if 'especial' in tax_name:
                tax_rate = 5.11269632
            else:
                tax = tax_line.tax_id
                tax_rate = abs(tax.amount) * 100
            
            base = facturae.TaxData('TaxableBase')
            base.feed({
                'totalamount': tax_line.base_amount,
            })

            amount = facturae.TaxData('TaxAmount')
            amount.feed({
                'totalamount': tax_line.tax_amount,
            })

            tax = facturae.Tax()
            tax.feed({
                'taxtypecode': code,
                'taxrate': tax_rate,
                'taxablebase': base,
                'taxamount': amount,
            })
            tax_list.append(tax)

        taxes.feed({
            'tax': tax_list,
        })

        return taxes

    def facturae_totals(self, cursor, uid, invoice, context=None):
        '''Generate facturae totals'''
        sign = self.get_invoice_sign(invoice)
        totals = facturae.InvoiceTotals()
        totals.feed({
            'totalgrossamount': sign * invoice.amount_untaxed,
            'totalgrossamountbeforetaxes': sign * invoice.amount_untaxed,
            'totaltaxoutputs': sign * invoice.amount_tax,
            'totaltaxeswithheld': 0,
            'invoicetotal': sign * invoice.amount_total,
            'totaloutstandingamount': sign * invoice.amount_total,
            'totalexecutableamount': sign * invoice.amount_total,
            
        })

        return totals

    def facturae_items_tax(self, cursor, uid, line, context=None):
        '''Generate facturae taxes for each line'''

        if not context:
            context = {}
        sign = self.get_invoice_sign(line.invoice_id)
        tax_obj = self.pool.get('account.invoice.tax')

        taxes_outputs = {}

        tax_list = []
        # Compute taxes for this line
        ctx = context.copy()
        ctx.update({'string_key': True})
        taxes = tax_obj.compute(cursor, uid, line.invoice_id.id, line.id,
                                context=ctx)
        for tax in line.invoice_line_tax_id:
            tax_values = taxes[str(tax.id)]
            tax_name = tax.name.lower()
            tax_code = self.facturae_tax_code(cursor, uid, tax_name)

            base = facturae.TaxData('TaxableBase')
            base.feed({
                'totalamount': sign * tax_values['base'],
            })

            if 'especial' in tax_name:
                tax_rate = 5.11269632
            else:
                tax_rate = abs(tax.amount) * 100

            tax_xml = facturae.Tax()
            tax_xml.feed({
                'taxtypecode': tax_code,
                'taxrate': tax_rate,
                'taxablebase': base,
            })
            tax_list.append(tax_xml)

        if not tax_list:
            # If the invoice line has no taxes associated we have to inform it
            # is a Non-taxable operation
            base = facturae.TaxData('TaxableBase')
            base.feed({
                'totalamount': 0,
            })

            tax_xml = facturae.Tax()
            tax_xml.feed({
                'taxtypecode': '01',  # IVA
                'taxrate': 0,  # 0% tax rate
                'taxablebase': base,
            })
            tax_list.append(tax_xml)

            special_tax = facturae.SpecialTaxableEvent()
            special_tax.feed({
                'specialtaxableeventcode': '02',  # Non-taxable transaction
                'specialtaxableeventreason': _('Non-taxable transaction')
            })

            taxes_outputs.update({
                'specialtaxableevent': special_tax
            })

        taxes_xml = facturae.Taxes('TaxesOutputs')
        taxes_xml.feed({'tax': tax_list})

        taxes_outputs.update({
            'taxesoutputs': taxes_xml
        })

        return taxes_outputs

    def facturae_items(self, cursor, uid, invoice, context=None):
        '''Generate facturae items using invoice lines'''
        sign = self.get_invoice_sign(invoice)
        items = facturae.Items()

        items_list = []
        for line in invoice.invoice_line:
            taxes_outputs = self.facturae_items_tax(
                cursor, uid, line, context=context)

            item = facturae.InvoiceLine()
            item.feed({
                'itemdescription': line.name,
                'quantity': line.quantity,
                'unitpricewithouttax': sign * line.price_unit,
                'totalcost': sign * line.price_subtotal,
                'grossamount': sign * line.price_subtotal
            })
            if invoice.facturae_filereference:
                item.feed(
                    {'filereference': invoice.facturae_filereference}
                )
            item.feed(taxes_outputs)
            items_list.append(item)

        items.feed({
            'invoiceline': items_list,
        })

        return items

    def facturae_account(self, cursor, uid, account, type='debit', context=None):

        if type == 'credit':
            accountxml = facturae.Account('AccountToBeCredited')
        else:
            accountxml = facturae.Account('AccountToBeDebited')
        accountxml.feed({
            'iban': account.iban,
            'bic': account.bank.bic,
        })
        return accountxml

    def facturae_payment(self, cursor, uid, invoice, context=None):
        '''Generate facturae payment'''

        acc_obj = self.pool.get('res.partner.bank')
        pm_obj = self.pool.get('payment.mode')

        payment = facturae.PaymentDetails()
        sign = self.get_invoice_sign(invoice)

        # Transfer payment must indicate credit account
        if invoice.payment_type.facturae_type == '04':

            payment_mode_id = context.get('payment_mode_id')
            if payment_mode_id:
                acc_id = pm_obj.read(cursor, uid,
                        payment_mode_id,['bank_id'])['bank_id'][0]
                account = acc_obj.browse(cursor, uid, acc_id)
            else:
                # Search for default account in company partner
                company = invoice.company_id.partner_id
                search_params = [('partner_id', '=', company.id),
                                 ('default_bank', '=', True)]
                acc_id = acc_obj.search(cursor, uid, search_params)
                if not acc_id:
                    account = company.bank_ids[0]
                else:
                    account = acc_obj.browse(cursor, uid, acc_id[0])
            credit_account = self.facturae_account(cursor, uid, account,
                                                   type='credit')
            debit_account = None
        # Direct debit must indicate debit account
        elif invoice.payment_type.facturae_type == '02':
            account = invoice.partner_bank
            credit_account = None
            debit_account = self.facturae_account(cursor, uid, account)            
        else:
            credit_account = None
            debit_account = None
        installment = facturae.Installment()
        installment.feed({
            'installmentduedate': invoice.date_due,
            'installmentamount': sign * invoice.amount_total,
            'paymentmeans': invoice.payment_type.facturae_type,
            'accounttobecredited': credit_account,
            'accounttobedebited': debit_account 
        })

        payment.feed({
            'installment': [installment],
        })

        return payment

    FACTURAE_EXTENSION = {
        'compressed': ['zip', 'gzip'],  # 3.1.9.2.1.1.
        'extensions': [  # 3.1.9.2.1.2.
            'xml', 'doc', 'gif', 'rtf', 'pdf', 'xls', 'jpg',
            'bmp', 'tiff'
        ]
    }

    MIME_TO_EXTENSION = {
        'jpeg': 'jpg',
        'msword': 'doc',
        'vnd.ms-excel': 'xls',
        # The following belongs to docx and xlsx format but we add it as doc and xls
        'vnd.openxmlformats-officedocument.wordprocessingml.document': 'doc',
        'vnd.openxmlformats-officedocument.spreadsheetml.sheet': 'xls'
    }

    def facturae_attachments(self, cursor, uid, attachments_data, context=None):
        '''
        :param attachments_data: reads of attachments ['datas', 'datas_fname']
        '''
        attachments = []
        for attachment_data in attachments_data:

            file_plain = attachment_data['datas']
            file_name = attachment_data['datas_fname']
            # Decode file
            decoded_file = base64.b64decode(file_plain)

            # Get true format of file
            file_format = magic.from_buffer(
                decoded_file, mime=True).split('/')[1]

            if file_format in self.FACTURAE_EXTENSION['compressed']:
                f = magic.Magic(mime=True, uncompress=True)

                file_descriptor, file_path = tempfile.mkstemp(
                    suffix='.{}'.format(file_format))

                open_file = os.fdopen(file_descriptor, 'w')
                open_file.write(decoded_file)
                open_file.close()
                real_format = f.from_file(file_path).split('/')[1]
                os.unlink(file_path)

                attachment = facturae.Attachment()
                attachment.feed({
                    'attachmentcompressionalgorithm': file_format.upper(),
                    'attachmentformat': real_format,
                    'attachmentencoding': 'BASE64',
                    'attachmentdata': file_plain,
                })

            else:
                real_format = file_format
                attachment = facturae.Attachment()
                attachment.feed({
                    'attachmentcompressionalgorithm': 'NONE',
                    'attachmentformat': real_format,
                    'attachmentencoding': 'BASE64',
                    'attachmentdata': file_plain,
                })

            if real_format not in self.FACTURAE_EXTENSION['extensions']:
                available_extensions = ', '.join(
                    self.FACTURAE_EXTENSION['extensions'])
                raise osv.except_osv(
                    _('Warning!'),
                    _(
                        'File {} doesn\'t have a valid extension should be '
                        'in ({})!'
                    ).format(file_name, available_extensions)
                )
            attachments.append(attachment)
        return attachments

    def get_facturae_attachments_data(self, cursor, uid, res_id, res_model,
                                      context=None):
        '''
        :param res_id: id of object in res model
        :param res_model: model of object witch we will obtain attachments
        :return: list of dicts with datas and datas_fname of attachments of
        object with facturae category
        '''
        imd_obj = self.pool.get('ir.model.data')
        atc_obj = self.pool.get('ir.attachment')

        atc_category_id = imd_obj.get_object_reference(
            cursor, uid, 'facturae_module', 'facturae_attachments'
        )[1]

        from osv.expression import OOQuery
        q = OOQuery(atc_obj, cursor, uid)
        sql = q.select(['id']).where([
            ('category_id', '=', atc_category_id),
            ('res_model', '=', res_model),
            ('res_id', '=', res_id),
        ])
        cursor.execute(*sql)
        atc_ids = [a[0] for a in cursor.fetchall()]

        if atc_ids:
            return atc_obj.read(
                cursor, uid, atc_ids, ['datas', 'datas_fname'], context=context
            )

        return []

    def facturae_documents(self, cursor, uid, invoice,
                           attachment=None, context=None):

        documents = None
        attachments = []
        if attachment is not None:
            for report in attachment:
                data = {'model': report['model']}
                ctx = context.copy()
                ctx.update({'lang': invoice.partner_id.lang})
                service = netsvc.LocalService("report.%s" % report['report'])
                result, format = service.create(cursor, uid,
                                                [report['model_id']],
                                                data, ctx)
                attachxml = facturae.Attachment()
                attachxml.feed({
                    'attachmentcompressionalgorithm': 'NONE',
                    'attachmentformat': format,
                    'attachmentencoding': 'BASE64',
                    'attachmentdata': base64.b64encode(result),
                })
                attachments.append(attachxml)

        attachments_data = self.get_facturae_attachments_data(
            cursor, uid, invoice.id, 'account.invoice', context=context
        )

        facturae_attachments = self.facturae_attachments(
            cursor, uid, attachments_data, context=context
        )
        attachments.extend(facturae_attachments)

        if attachments:
            documents = facturae.RelatedDocuments()
            documents.feed({'attachment': attachments})

        return documents

    def facturae_invoices(self, cursor, uid, invoice,
                          info=None, attachment=None, context=None):
        '''Generate facturae invoices'''

        invoices = facturae.Invoices()
        invoicexml = facturae.Invoice()

        header = facturae.InvoiceHeader()
        header.feed({
            'invoicenumber': invoice.number,
            'invoicedocumenttype': 'FC',
            'invoiceclass': 'OO',
        })

        # TODO Take into account corrective invoices

        documents = self.facturae_documents(
            cursor, uid, invoice, attachment, context=context
        )

        extra_info_data = {}
        extrainfo = None

        if info is not None:
            extra_info_data.update({'invoiceadditionalinformation': info})
        if documents is not None:
            extra_info_data.update({'relateddocuments': documents})
        if extra_info_data:
            extrainfo = facturae.AdditionalData()
            extrainfo.feed(extra_info_data)

        issuedata = self.facturae_issue_data(cursor, uid, invoice, context=context)
        taxes = self.facturae_taxes(cursor, uid, invoice, context=context)
        totals = self.facturae_totals(cursor, uid, invoice, context=context)
        items = self.facturae_items(cursor, uid, invoice, context=context)
        payment = self.facturae_payment(cursor, uid, invoice, context=context)

        invoicexml.feed({
            'invoiceheader': header,
            'invoiceissuedata': issuedata,
            'taxesoutputs': taxes,
            'invoicetotals': totals,
            'items': items,
            'paymentdetails': payment,
            'additionaldata': extrainfo,
        })

        invoices.feed({
            'invoice': [invoicexml],
        })

        return invoices

    def facturae_signature(self, cursor, uid, xml):

        jar = ("%s/facturae_module/java/FacturaeJ.jar") % config['addons_path']
        # Temporary store
        xml_unsigned = tempfile.NamedTemporaryFile()
        xml_unsigned.write(xml)
        xml_unsigned.seek(0)

        xml_signed = tempfile.NamedTemporaryFile()

        # Certificate data
        certificate = config['facturae_cert']
        cert_passwd = config['facturae_cert_pass']

        # Run java signature
        call = ['java','-jar', jar ,'0']
        call += [xml_unsigned.name, xml_signed.name]
        call += ['facturae31']
        call += [certificate, cert_passwd]
        subprocess.call(call, stdout=None, stderr=None)

        # Read result
        xml_signed.seek(0)
        res = xml_signed.read()

        # Close temporary store
        xml_signed.close()
        xml_unsigned.close()

        return res

    def facturae_generate_xml(self, cursor, uid, invoice_id,
                              info=None, attachment=None, context=None):
        """Generate facturae xml format
        :param invoice_id: id of the invoice to be transformed
        :type invoice_id: integer
        :param info: Extra information to place in additional info tag
        :type info: str or unicode
        :param attachment: list of dicts with the format
        ({'report': report_name, 'model': model, 'model_id': model_id})
        :type attachment: list
        :param context: dict of values to be passed as context
        :type context: dict
        """

        if not context:
            context = {}

        invoice_obj = self.pool.get('account.invoice')
        cfg_obj = self.pool.get('res.config')

        invoice = invoice_obj.browse(cursor, uid, invoice_id)

        xml = facturae.FacturaeRoot()
        header = self.facturae_header(cursor, uid, invoice,
                                      context=context)

        parties = self.facturae_parties(cursor, uid, invoice, info=info,
                                        context=context)

        invoices = self.facturae_invoices(cursor, uid, invoice,
                                          info=info,
                                          attachment=attachment,
                                          context=context)

        xml.feed({
            'fileheader': header,
            'parties': parties,
            'invoices': invoices,
        })

        filename = 'facturae_%s.xsig' % invoice.number.replace('/', '')

        xml.pretty_print = True
        xml.build_tree()

        signed_enabled = int(cfg_obj.get(cursor, uid, 'facturae_signed_enabled', '1'))
        str_xml = str(xml)
        if signed_enabled:
            str_xml = self.facturae_signature(cursor, uid, str_xml)
        return str_xml, filename, xml


AccountInvoiceFacturae()
