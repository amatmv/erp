# -*- encoding: utf-8 -*-

from osv import osv, fields


class PartnerAddressAdministrativeFacturae(osv.osv):

    _name = 'res.partner.address.administrative'
    _rec_name = 'code'
    _order = 'role'

    _get_roles = [('01', 'Accounting office'),
                  ('02', 'Management body'),
                  ('03', 'Proceeding unit'),
                  ('04', 'Proposing body')
                  ]

    _columns = {
        'address_id': fields.many2one('res.partner.address', 'Address',
                                      required=True),
        'code': fields.char('Code', size=13, required=True),
        'role': fields.selection(_get_roles, 'Role', required=True)
    }

PartnerAddressAdministrativeFacturae()


class PartnerAddressFacturae(osv.osv):

    _name = 'res.partner.address'
    _inherit = 'res.partner.address'

    _columns = {
        'administrative_ids': fields.one2many(
                            'res.partner.address.administrative',
                            'address_id', 'Administrative centers')
    }

PartnerAddressFacturae()
