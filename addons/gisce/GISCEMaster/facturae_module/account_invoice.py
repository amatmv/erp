# -*- encoding: utf-8 -*-

from osv import osv, fields

from osv.expression import OOQuery


class AccountInvoice(osv.osv):
    _name = 'account.invoice'
    _inherit = 'account.invoice'

    def create(self, cursor, uid, vals, context=None):
        if context is None:
            context = {}
        address_invoice_id = vals['address_invoice_id']
        rpa_obj = self.pool.get('res.partner.address')
        # Quan es crea una factura es sol passar el tipus per context
        # perque despres el default de la account_invoice el busca alla
        inv_type = vals.get('type', False)
        if not inv_type:
            inv_type = context.get('type', 'out_invoice')
        if inv_type in ['out_invoice', 'out_refund']:
            rpa_ids = rpa_obj.q(cursor, uid).read([
                'id'
            ]).where([
                ('id', '=', address_invoice_id),
                ('administrative_ids.code', '!=', None)
            ])
            if rpa_ids:
                vals['facturae'] = True
        return super(AccountInvoice, self).create(
            cursor, uid, vals, context=context
        )


    _columns = {
        'facturae': fields.boolean('Facturae', select=1, readonly=True,
            help='It will be active when it is a facturae'
        ),
        'facturae_exported': fields.boolean(
            'FacturaE generada',
            help='It will be active when the facturae is generated'
        ),
        'facturae_exported_date': fields.date(
            'Facturae exported date'
        ),
        'facturae_filereference': fields.char(
            'Facturae file reference',
            help='File Reference for indicate into XML',
            size=20,
            readonly=True,
            states={'draft': [('readonly', False)]}
        )
    }

    _defaults = {
        'facturae_exported': lambda *a: False,
    }


AccountInvoice()
