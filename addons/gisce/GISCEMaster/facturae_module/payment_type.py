# -*- encoding: utf-8 -*-

from osv import osv, fields


class PaymentTypeFacturae(osv.osv):

    _name = 'payment.type'
    _inherit = 'payment.type'

    _get_facturae_payment = [
        ('01', 'In Cash'),
        ('02', 'Direct debit'),
        ('03' ,'Receipt'),
        ('04', 'Credit Transfer'),
        ('05', 'Accepted bill of exchang'),
        ('06', 'Documentary credit'),
        ('07', 'Contract award'),
        ('08', 'Bill of exchange'),
        ('09', 'Transferable promissory note'),
        ('10', 'Non transferable promissory note'),
        ('11', 'Cheque'),
        ('12', 'Open account reimbursement'),
        ('13', 'Special payment'),
        ('14', 'Set-off by reciprocal credits'),
        ('15', 'Payment by postgiro'),
        ('16', 'Certified cheque'),
        ('17', 'Banker’s draft'),
        ('18', 'Cash on delivery'),
        ('19', 'Payment by card'),
    ]

    _columns = {
        'facturae_type': fields.selection(_get_facturae_payment,
                                          'Payment type Facturae')
    }

PaymentTypeFacturae()
