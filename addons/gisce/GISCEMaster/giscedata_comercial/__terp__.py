# -*- coding: utf-8 -*-
{
    "name": "Modul Comercial",
    "description": """
    Modul base per a la gestió de comercials. Afegeix empleats(comercials) 
    i delegacions, el camp comercial al contracte i crea un menu perqué els comercials l'utilitzin.
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "crm",
        "hr_office",
        "giscedata_polissa_responsable",
        "giscedata_facturacio",
        "giscedata_cnmc_sips_comer",
        "poweremail",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_comercial_data.xml",
        "giscedata_polissa_view.xml",
        "hr_employee_view.xml",
        "menu_comercial_view.xml",
        "menu_admin_comercial_view.xml",
        "wizard/wizard_alta_comercial_view.xml",
        "security/ir.model.access.csv",
    ],
    "active": False,
    "installable": True
}
