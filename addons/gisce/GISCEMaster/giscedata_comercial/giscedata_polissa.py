# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscedataPolissa(osv.osv):

    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def _default_comercial(self, cursor, uid, id, context=None):
        '''Usuari connectat'''
        emp_obj = self.pool.get("hr.employee")
        eid = emp_obj.search(cursor, uid, [('user_id', '=', uid)])
        if len(eid):
            return eid[0]
        return False

    def write(self, cursor, uid, ids, vals, context=None):
        if 'comercial_id' in vals and vals.get("comercial_id") and 'user_id' not in vals:
            emp_obj = self.pool.get("hr.employee")
            inf = emp_obj.read(cursor, uid, vals.get("comercial_id"), ['user_id'])
            if inf['user_id']:
                vals['user_id'] = inf['user_id'][0]
        return super(GiscedataPolissa, self).write(cursor, uid, ids, vals, context=context)

    _columns = {
        'comercial_id': fields.many2one('hr.employee', 'Comercial')
    }

    _defaults = {
        'comercial_id': _default_comercial
    }

GiscedataPolissa()
