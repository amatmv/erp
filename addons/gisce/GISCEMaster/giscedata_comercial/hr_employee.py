# -*- coding: utf-8 -*-
from osv import osv, fields


class HrEmployee(osv.osv):

    _inherit = 'hr.employee'

    def get_asigned_contracts_to_comercial(self, cursor, uid, emp_id, context=None):
        if context is None:
            context = {}
        if isinstance(emp_id, (list, tuple)):
            emp_id = emp_id[0]

        pol_o = self.pool.get("giscedata.polissa")
        pol_ids = pol_o.search(cursor, uid, [('comercial_id', '=', emp_id)])
        return pol_ids


HrEmployee()
