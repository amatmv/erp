# -*- coding: utf-8 -*-
from __future__ import (absolute_import)
from tools.translate import _
from osv import osv, fields
import base64


class WizardAltaComercial(osv.osv_memory):

    _name = 'wizard.alta.comercial'

    def default_get(self, cr, uid, fields, context=None):
        data = super(WizardAltaComercial, self).default_get(cr, uid, fields, context)
        pw_obj = self.pool.get("poweremail.core_accounts")
        pwid = pw_obj.search(cr, uid, [('state', '=', 'approved')])
        if pwid:
            info = pw_obj.read(cr, uid, pwid[0], ['smtptls', 'smtpssl', 'smtpport', 'smtpserver'])
            data.update({
                'smtptls': info['smtptls'],
                'smtpssl': info['smtpssl'],
                'smtpport': info['smtpport'],
                'smtpserver': info['smtpserver'],
            })
        return data

    def onchange_contact_email(self, cr, uid, ids, email):
        if not email:
            return {'value': {}}
        else:
            return {'value': {'pwemail': email}}

    def action_init(self, cursor, uid, ids, context=None):
        wizard = self.browse(cursor, uid, ids[0], context=context)
        wizard.write({
            'state': 'init'
        })

    def action_pantalla_partner(self, cursor, uid, ids, context=None):
        wizard = self.browse(cursor, uid, ids[0], context=context)
        wizard.write({
            'state': 'crear_adreca'
        })

    def action_pantalla_adreca(self, cursor, uid, ids, context=None):
        wizard = self.browse(cursor, uid, ids[0], context=context)
        wizard.write({
            'state': 'crear_usuari'
        })

    def action_processar_creacio(self, cursor, uid, ids, context=None):
        wizard = self.browse(cursor, uid, ids[0], context=context)
        # Netegem les dades entrades per l'usuari'
        camps_dict = self.fields_get(cursor, uid)
        for fname in camps_dict:
            if camps_dict[fname]['type'] == 'char':
                wizard.write({fname: (getattr(wizard, fname) or " ").strip()})
        wizard = self.browse(cursor, uid, ids[0], context=context)

        msg = ""
        # Crear, si fa falta, el partner
        try:
            part_id = self.get_partner(cursor, uid, wizard.vat)
            if not part_id:
                part_id = self.create_partner(cursor, uid, wizard)
                msg += _(" * S'ha creat una empresa nova amb CIF/NIF {0}.\n").format(wizard.vat)
            else:
                msg += _(" * Ja hi existia una empresa amb el CIF/NIF {0}. No se m'ha creat cap de nova.\n").format(wizard.vat)
                part_id = part_id[0]

            # Crear l'addreça i asignarla al partner
            address_id = self.create_address(cursor, uid, part_id, wizard)
            msg += _(" * S'ha creat una nova adreça a l'empresa amb CIF/NIF {0}.\n").format(wizard.vat)
            # Crear, si fa falta, l'usuari
            user_id = self.get_user(cursor, uid, wizard)
            if wizard.create_user_id:
                if user_id:
                    msg += _(" * Ja existia l'usuari {0}.\n").format(wizard.username)
                else:
                    user_id = self.create_user(cursor, uid, address_id, wizard)
                    msg += _(" * S'ha creat l'usuari {0} amb contrasenya {0}.\n").format(wizard.username)
            # Crear, si fa falta, conta de poweremail
            pw_id = None
            if wizard.crear_poweremail and user_id:
                pw_id = self.get_poweremail(cursor, uid, wizard)
                if pw_id:
                    msg += _(" * Ja existia la conta de poweremail de {0}.\n").format(wizard.email)
                else:
                    pw_id = self.create_poweremail(cursor, uid, user_id, wizard)
                    msg += _(" * S'ha creat la conta de poweremail de {0}.\n").format(wizard.email)
            emp_id = self.create_employee(cursor, uid, wizard, part_id, address_id, user_id)
            msg += _(" * S'ha creat el comercial {0}.\n").format(wizard.nom + " " + (wizard.cognom1 or "") + " "+ (wizard.cognom2 or ""))
            msg += self.accions_adicionals(cursor, uid, wizard, part_id, address_id, user_id, pw_id, emp_id, context=context)
            # Finalitzar
            wizard.write({
                'info': msg,
                'state': 'end'
            })
        except osv.except_osv, e:
            raise e
        except Exception, e:
            raise osv.except_osv(_(u"Atenció!"), _(u"Hi ha hagut algun problema durant el procés. Revisa que les dades introduïdes siguin correctes i estiguin totes completades. Missatge d'error:\n\n{0}").format(e.message))

    def accions_adicionals(self, cursor, uid, wizard, part_id, address_id, user_id, pw_id, emp_id, context=None):
        return ""

    def get_partner(self, cursor, uid, vat, context=None):
        part_obj = self.pool.get("res.partner")
        newvat = vat.upper()
        part_id = part_obj.search(cursor, uid, [('vat', '=', newvat)])
        if not part_id:
            if newvat.startswith("ES"):
                newvat = newvat[2:]
            else:
                newvat = "ES" + newvat
            part_id = part_obj.search(cursor, uid, [('vat', '=', newvat)])
        return part_id

    def create_partner(self, cursor, uid, wizard, context=None):
        part_obj = self.pool.get("res.partner")
        conf_obj = self.pool.get('res.config')

        name_as_dict = {
            "N": wizard.nom, "C1": wizard.cognom1, "C2": wizard.cognom2
        }
        name_struct = conf_obj.get(cursor, uid, 'partner_name_format', 'C1 C2, N')
        if name_struct == 'C1 C2, N':
            name_formatter = "{C1} {C2}, {N}"
        else:
            name_formatter = "{N} {C1} {C2}"
        nom = name_formatter.format(**name_as_dict)

        newvat = wizard.vat.upper()
        if not newvat.startswith("ES"):
            newvat = "ES"+newvat

        vals = {
            'name': nom,
            'vat': newvat,
        }
        part_id = part_obj.create(cursor, uid, vals)
        return part_id

    def create_employee(self, cursor, uid, wizard, partner_id, address_id, user_id, context=None):
        part_obj = self.pool.get("hr.employee")
        municipi_id, country_id, state_id = False, False, False
        if wizard.id_municipi:
            municipi_id = wizard.id_municipi.id
            if wizard.id_municipi.state:
                state_id = wizard.id_municipi.state.id
                if wizard.id_municipi.state.country_id:
                    country_id = wizard.id_municipi.state.country_id.id

        imd_obj = self.pool.get("ir.model.data")
        categ = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_comercial', 'hr_category_comercial'
        )[1]
        vals = {
            'name': wizard.nom + " " + (wizard.cognom1 or "") + " "+ (wizard.cognom2 or ""),
            'address_id': address_id,
            'address_home_id': address_id,
            'user_id': user_id,
            'work_phone': wizard.telefon,
            'country_id': country_id,
            'category_id': categ,
            'work_office': wizard.work_office and wizard.work_office.id
        }
        part_id = part_obj.create(cursor, uid, vals)
        return part_id

    def create_address(self, cursor, uid, partner_id, wizard, context=None):
        partner_address_obj = self.pool.get('res.partner.address')
        municipi_id, country_id, state_id = False, False, False
        if wizard.id_municipi:
            municipi_id = wizard.id_municipi.id
            if wizard.id_municipi.state:
                state_id = wizard.id_municipi.state.id
                if wizard.id_municipi.state.country_id:
                    country_id = wizard.id_municipi.state.country_id.id
        addr_vals = {
            'nv': wizard.nv,
            'pnp': wizard.pnp,
            'aclarador': wizard.aclarador,
            'zip': wizard.dp,
            'id_municipi': municipi_id,
            'country_id': country_id,
            'state_id': state_id,
            'tv': wizard.tv and wizard.tv.id or False,
            'es': wizard.es,
            'pt': wizard.pt,
            'pu': wizard.pu,
            'phone': wizard.telefon,
            'email': wizard.email,
            'partner_id': partner_id,
            'name': wizard.nom + " " + (wizard.cognom1 or "") + " "+ (wizard.cognom2 or "")
        }
        address_id = partner_address_obj.create(
            cursor, uid, addr_vals, context=context
        )
        return address_id

    def get_user(self, cursor, uid, wizard, context=None):
        user_obj = self.pool.get("res.users")
        uid = user_obj.search(cursor, uid, [('login', '=', wizard.username)])
        if uid:
            return uid[0]
        return False

    def create_user(self, cursor, uid, addres_id, wizard, context=None):
        user_obj = self.pool.get("res.users")
        imd_obj = self.pool.get("ir.model.data")
        grp_obj = self.pool.get("res.groups")
        groups_names = [
            'Account Invoice / Manager',
            'Base extended  / Manager',
            'CRM / Manager',
            'Employee',
            'Finance / Accountant',
            'Finance / Invoice',
            'Finance / Invoices Manager',
            'GISCEDATA CUPS / Manager',
            'GISCEDATA facturació / Manager',
            'GISCEDATA OV / Manager',
            'GISCEDATA Pòlissa / Manager',
            'Human Resources / User',
            'Partner Manager',
            'Poweremail / Settings_Manager',
            'Switching / User',
            'Reclamacio / User',

        ]
        gids = grp_obj.search(cursor, uid, [('name', 'in', groups_names)])
        action = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_comercial', 'action_menu_giscedata_comercial'
        )[1]
        vals = {
            'login': wizard.username,
            'password': wizard.username,
            'signature': wizard.nom + " " + (wizard.cognom1 or "") + " "+ (wizard.cognom2 or ""),
            'address_id': addres_id,
            'menu_id': action,
            'action_id': action,
            'company_id': 1,
            'name': wizard.nom + " " + (wizard.cognom1 or "") + " "+ (wizard.cognom2 or "")
        }
        usr_id = user_obj.create(cursor, uid, vals)
        user_obj.write(cursor, uid, usr_id, {
            'groups_id': [(6, 0, gids)]
        })
        return usr_id

    def get_poweremail(self, cursor, uid, wizard, context=None):
        pw_obj = self.pool.get("poweremail.core_accounts")
        pid = pw_obj.search(cursor, uid, [('email_id', '=', wizard.username)])
        if pid:
            return pid[0]
        return False

    def create_poweremail(self, cursor, uid, user_id, wizard, context=None):
        pw_obj = self.pool.get("poweremail.core_accounts")
        vals = {
            'smtpuname': wizard.username,
            'email_id': wizard.email,
            'user': user_id,
            'name': wizard.nom + " " + (wizard.cognom1 or "") + " "+ (wizard.cognom2 or ""),
            'company': 'no',
            'smtptls': wizard.smtptls,
            'smtpssl': wizard.smtpssl,
            'smtppass': wizard.smtppass,
            'smtpport': wizard.smtpport,
            'smtpserver': wizard.smtpserver,
        }
        pid = pw_obj.create(cursor, uid, vals)
        pw_obj.do_approval(cursor, uid, [pid])
        return pid

    def _estats_disponibles(self, cursor, uid, context=None):
        return [
            ("init", "Pantalla Inicial"),
            ("crear_adreca", "Creació d'Empresa"),
            ("crear_usuari", "Creació Usuari ERP"),
            ("end", "Finalitzat")
        ]

    _columns = {
        # Dades Partner/Comercial
        'work_office': fields.many2one('hr.office', 'Delegació'),
        'nom': fields.char("Nom/Raó Social", size=64),
        'cognom1': fields.char("1r Cognom", size=64),
        'cognom2': fields.char("2n Cognom", size=64),
        'vat': fields.char("CIF/NIF", size=16),
        # Dades Adreça
        'id_municipi': fields.many2one('res.municipi', 'Municipi'),
        'aclarador': fields.char('Aclarador', size=256),
        'tv': fields.many2one('res.tipovia', 'Tipus Via'),
        'nv': fields.char('Carrer', size=256),
        'pnp': fields.char('Número', size=9),
        'bq': fields.char('Bloc', size=4),
        'es': fields.char('Escala', size=4),
        'pt': fields.char('Planta', size=12),
        'pu': fields.char('Porta', size=4),
        'dp': fields.char('Codi Postal', size=5),
        # Dades Contacte
        'telefon': fields.char('Telèfon', size=16),
        'email': fields.char('Correu', size=64),
        # Dades Usuari
        'username': fields.char('Login Usuari', size=64),
        # Dades poweremail
        'pwemail': fields.char('Correu', size=64),
        'smtptls': fields.boolean("Utilitzar TLS"),
        'smtpssl': fields.boolean("Utilitzar SSL"),
        'smtppass': fields.char('Password', size=32),
        'smtpport': fields.integer("Port"),
        'smtpserver': fields.char('Servidor SMTP', size=64),
        # Dades Wizard
        'create_user_id': fields.boolean("Pot entrar al ERP (crear usuari?)"),
        'crear_poweremail': fields.boolean("Pot enviar mails"),
        'state': fields.selection(_estats_disponibles, "Estat"),
        'info': fields.text("Resultat")
    }

    _defaults = {
        'state': lambda *a: 'init',
    }


WizardAltaComercial()
