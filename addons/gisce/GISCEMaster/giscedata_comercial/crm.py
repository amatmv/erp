# -*- coding: utf-8 -*-
from osv import osv, fields


class crm_case(osv.osv):
    _name = "crm.case"
    _inherit = "crm.case"

    def _get_default_section(self, cursor, uid, context=None):
        if context is None:
            context = {}
        if context.get("from_gestio_comercial"):
            imd_obj = self.pool.get('ir.model.data')
            section_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_comercial', 'crm_case_section_comercial'
            )
            return section_id[1]
        else:
            return False

    _defaults = {
        'section_id':_get_default_section,
    }


crm_case()
