# -*- coding: utf-8 -*-
from __future__ import absolute_import
from osv import osv
from giscedata_polissa_condicions_generals.report import ReportContracteCondicionsGenerals
from c2c_webkit_report import webkit_report
import logging
from report import report_sxw
import base64
from tools.translate import _


class GiscedataPolissa(osv.osv):
    _name = "giscedata.polissa"
    _inherit = 'giscedata.polissa'

    def start_signature_process(self, cursor, uid, contract_id, context=None):
        """
        Starts a contract signature process from OV call.
        I get all the data needed for making that call.
        :param cursor:
        :param uid:
        :param ids:
        :param contract_id: Contract ID number
        :param context:
        :return:
        """

        # I get the PDF document information
        report_obj = self.pool.get('ir.actions.report.xml')
        report_id = report_obj.search(cursor, uid, [
            ("report_name", '=', "giscedata.polissa")])[0]
        report_xml = report_obj.browse(cursor, uid, report_id)

        # I look if giscedata_polissa_condicions_generals is installed
        ir_module_obj = self.pool.get('ir.module.module')
        module_id = ir_module_obj.search(cursor, uid,
            [('name', '=', 'giscedata_polissa_condicions_generals')])
        state = ir_module_obj.read(
            cursor, uid, module_id, ['state'])[0]['state']
        if state == 'installed':
            report_printer = ReportContracteCondicionsGenerals(
                'report.{0}'.format(report_xml.report_name),
                'giscedata.polissa',
                report_xml.report_webkit,
                parser=report_sxw.rml_parse
            )
        else:
            report_printer = webkit_report.WebKitParser(
                'report.{0}'.format(report_xml.report_name),
                'giscedata.polissa',
                report_xml.report_webkit,
                parser=report_sxw.rml_parse
            )

        # Printing time!
        data = {
            'model': 'giscedata.polissa',
            'id': contract_id,
            'report_type': 'webkit'
        }
        document_binary = report_printer.create(
            cursor, uid, [contract_id], data, context=context)
        document = base64.b64encode(document_binary[0])

        # I get the partner info, id, name, surname, email, mobile
        contract_obj = self.pool.get('giscedata.polissa')
        partner_obj = self.pool.get('res.partner')
        partner_address_obj = self.pool.get('res.partner.address')
        contracte_data = contract_obj.read(
            cursor, uid, contract_id, ['titular', 'name'])
        titular_id = contracte_data['titular'][0]
        partner_data = partner_obj.read(
            cursor, uid, titular_id, ['id', 'name', 'address', 'vat']
        )
        partner_address_data = partner_address_obj.read(
            cursor, uid, partner_data['address'][0], ['name', 'email', 'mobile']
        )
        vat = partner_data['vat']
        mobile = partner_address_data['mobile']
        email = partner_address_data['email']
        user_splitted = partner_address_data['name'].split(' ')
        surname = ''
        name = user_splitted[0]
        if len(user_splitted) > 3:
            name = str(user_splitted[0]) + ' ' + str(user_splitted[1])
            surname = ' '.join(user_splitted[2:])
        elif len(user_splitted) > 2:
            name = str(user_splitted[0])
            surname = ' '.join(user_splitted[1:])
        elif len(user_splitted) > 1:
            surname = user_splitted[1]

        # I create a Small description of the document
        description = "Contracte -" + (contracte_data['name'] or vat)

        # My model type
        model = 'giscedata.polissa,' + str(contract_id)

        # Send signature to signature module
        signature_obj = self.pool.get('giscedata.signatura.documents')
        res = signature_obj.new_signature(
            cursor, uid, [], model, email, description, name, surname,
            document, mobile, titular_id, context
        )
        return res

    def process_signature_callback(self, cursor, uid, contract_id, context=None):
        if not context:
            context = {}

        logger = logging.getLogger(__name__)

        config_vals = {
            'activacio_cicle': 'A'
        }

        # Activo usuari a l'oficina virtual si no existeix
        partner_obj = self.pool.get('res.partner')
        partner_address_obj = self.pool.get('res.partner.address')
        sw_obj = self.pool.get('giscedata.switching')
        contracte_data = self.read(
            cursor, uid, contract_id, ['titular', 'state'])
        partner_id = contracte_data['titular'][0]

        partner_data = partner_obj.read(
            cursor, uid, partner_id, ['vat', 'ov_users_ids', 'address'])
        address = partner_data['address'][0]
        vat = partner_data['vat'].replace('ES', '')
        addr = partner_address_obj.read(
            cursor, uid, address, ['email', 'mobile']
        )
        email = addr['email']
        mobile = addr['mobile']

        if not partner_data.get('ov_users_ids', False):
            partner_obj.enable_ov_user(cursor, uid, partner_id, vat, email, mobile)

        # Si no hi ha sw_obj -> No hi ha switching instalat
        if not sw_obj or contracte_data['state'] != 'esborrany':
            return
        # Creo cas ATR C1
        search_params = [('cups_polissa_id', '=', contract_id)]
        polissa = sw_obj.search(cursor, uid, search_params)

        if not polissa:
            cas_atr = self.crear_cas_atr(
                cursor, uid, contract_id, 'C1',
                config_vals=config_vals, context=context
            )
            sw_id = cas_atr[2]

            if sw_id:
                # Busco ID del pas
                step = sw_obj.read(cursor, uid, sw_id, ['step_id'])['step_id'][0]
                pas = sw_obj.get_pas_id(cursor, uid, sw_id=sw_id, step_id=step)

                # Envio correu al OutBox
                mail = sw_obj.notifica_pas_a_client(
                    cursor, uid, sw_id, pas,
                    'giscedata.switching.c1.01', context=context
                )
                if not mail[0]:
                    logger.error(mail)
                    raise osv.except_osv(_('Error!'), (mail[1]))
            else:
                raise osv.except_osv(_('Error!'), cas_atr[1])


GiscedataPolissa()
