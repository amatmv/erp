# -*- coding: utf-8 -*-
{
    "name": "Signatura digital de polisses dels clients Lleidanet (Comercialitzadora)",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_polissa_comer_signatura",
    ],
    "init_xml": [],
    "demo_xml":[
    ],
    "update_xml":[],
    "active": False,
    "installable": True
}
