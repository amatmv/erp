SELECT cups.name, to_char(m1.data_inici, 'YYYYMMDD')
FROM giscedata_polissa_modcontractual AS m1
LEFT JOIN giscedata_polissa AS p ON p.id=m1.polissa_id
LEFT JOIN giscedata_cups_ps AS cups ON cups.id=m1.cups
WHERE m1.tipus='alta'
  AND m1.data_inici>=%(inici)s and m1.data_inici<=%(final)s
ORDER BY cups.name