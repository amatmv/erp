SELECT
    c.name AS cups,
    to_char(casos.fecha_activacion, 'YYYYMMDD') AS fecha_activacion
FROM (
    SELECT
        sw_c1_05.data_activacio AS fecha_activacion,
        sw.cups_polissa_id AS polissa_id,
        sw.cups_id AS cups_id
    FROM
        giscedata_switching AS sw,
        giscedata_switching_step_header AS sw_sh,
        giscedata_switching_c1_05 AS sw_c1_05
    WHERE
        sw_c1_05.header_id = sw_sh.id
        AND sw_sh.sw_id = sw.id
        AND sw_c1_05.data_activacio >= %(inici)s
        AND sw_c1_05.data_activacio <= %(final)s
UNION
    SELECT
        sw_c2_05.data_activacio AS fecha_activacion,
        sw.cups_polissa_id AS polissa_id,
        sw.cups_id AS cups_id
    FROM
        giscedata_switching AS sw,
        giscedata_switching_step_header AS sw_sh,
        giscedata_switching_c2_05 AS sw_c2_05
    WHERE
        sw_c2_05.header_id = sw_sh.id
        AND sw_sh.sw_id = sw.id
        AND sw_c2_05.data_activacio >= %(inici)s
        AND sw_c2_05.data_activacio <= %(final)s
) AS casos
INNER JOIN giscedata_polissa AS p ON (casos.polissa_id = p.id)
INNER JOIN giscedata_cups_ps AS c ON (casos.cups_id = c.id)
ORDER BY cups, fecha_activacion;
