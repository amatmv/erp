SELECT
    c.name AS cups,
    to_char(casos.fecha_activacion, 'YYYYMMDD') AS fecha_activacion,
    comer_entrant.ref2 AS comer_entrante,
    comer_sortint.ref2 AS comer_saliente,
    casos.process AS tipo_cambio
FROM (
    SELECT
        'C1' AS process,
        sw_c1_05.data_activacio AS fecha_activacion,
        sw.cups_polissa_id AS polissa_id,
        sw.cups_id AS cups_id,
        sw.comer_sortint_id AS comer_sortint_id,
        sw.case_id AS case_id
    FROM
        giscedata_switching AS sw,
        giscedata_switching_step_header AS sw_sh,
        giscedata_switching_c1_05 AS sw_c1_05
    where
        sw_c1_05.header_id = sw_sh.id
        AND sw_sh.sw_id = sw.id
        AND sw_c1_05.data_activacio >= %(inici)s
        AND sw_c1_05.data_activacio <= %(final)s
UNION
    SELECT
        'C2' AS process,
        sw_c2_05.data_activacio AS fecha_activacion,
        sw.cups_polissa_id AS polissa_id,
        sw.cups_id AS cups_id,
        sw.comer_sortint_id AS comer_sortint_id,
        sw.case_id
    FROM
        giscedata_switching AS sw,
        giscedata_switching_step_header AS sw_sh,
        giscedata_switching_c2_05 AS sw_c2_05
    where
        sw_c2_05.header_id = sw_sh.id
        AND sw_sh.sw_id = sw.id
        AND sw_c2_05.data_activacio >= %(inici)s
        AND sw_c2_05.data_activacio <= %(final)s
) AS casos
INNER JOIN giscedata_polissa p ON (casos.polissa_id = p.id)
INNER JOIN giscedata_cups_ps c ON (casos.cups_id = c.id)
INNER JOIN res_partner AS comer_sortint ON (casos.comer_sortint_id = comer_sortint.id)
INNER JOIN crm_case AS crm ON (casos.case_id = crm.id)
INNER JOIN res_partner AS comer_entrant ON (crm.partner_id = comer_entrant.id)
ORDER BY cups, fecha_activacion;
