# -*- coding: utf-8 -*-
from tools import config
from osv import osv, fields
import csv
import StringIO
import base64


class WizardGenerarCnmcIvc(osv.osv_memory):
    _name = "wizard.generar.cnmc.ivc"

    def whereiam(self, cursor, uid, context=None):
        '''retorna si estem a distri o a comer depenent de si el
        cups pertany a la nostra companyia o no'''

        cups_obj = self.pool.get('giscedata.cups.ps')
        user_obj = self.pool.get('res.users')

        cups_id = cups_obj.search(
            cursor, uid, [('distribuidora_id', '!=', '')], limit=1,
            context=context
        )[0]

        cups = cups_obj.browse(cursor, uid, cups_id)
        user = user_obj.browse(cursor, uid, uid)

        distri_id = cups.distribuidora_id.id
        comp_id = user.company_id.partner_id.id

        if distri_id == comp_id:
            return 'distri'
        return 'comer'

    def crear(self, cursor, uid, ids, context=None):
        if not context:
            context = {}

        wizard = self.browse(cursor, uid, ids[0], context)

        user_obj = self.pool.get('res.users')
        company_obj = self.pool.get('res.company')

        user = user_obj.browse(cursor, uid, uid)

        if self.whereiam(cursor, uid, context) == 'distri':
            sql_file = 'cerca_distri.sql'
            codi_inf = 'R1-{0}'.format(user.company_id.codi_r1)
        else:
            codi_inf = 'R2-{0}'.format(user.company_id.codi_r2)
            if self.pool.get('giscedata.switching'):
                sql_file = 'cerca_comer_swit.sql'
            else:
                sql_file = 'cerca_comer.sql'

        sql = open("{0}/{1}/sql/{2}".format(config['addons_path'],
                                            'giscedata_cnmc_ivc',
                                            sql_file)).read()
        cursor.execute(sql, {'inici': wizard.data_inici,
                             'final': wizard.data_final})

        mes_ini = '{0[0]}{0[1]}'.format(wizard.data_inici.split('-'))
        mes_fi = '{0[0]}{0[1]}'.format(wizard.data_final.split('-'))
        filename = 'IVC_{0}_{1}_{2}.csv'.format(codi_inf, mes_ini, mes_fi)

        output = StringIO.StringIO()
        writer = csv.writer(output)

        for row in cursor.fetchall():
            writer.writerow((codi_inf,)+row)

        fitxer = base64.b64encode(output.getvalue())

        wizard.write({'state': 'end', 'file': fitxer, 'name': filename})

        output.close()
        return True

    _columns = {
        'state': fields.selection([('init', 'Init'), ('end', 'End')], 'State'),
        'data_inici': fields.date('Data d\'inici', required=True),
        'data_final': fields.date('Data de fi', required=True),
        'name': fields.char('Nom', size=256),
        'file': fields.binary('Fitxer')
    }

    _defaults = {
        'state': lambda *a: 'init',
    }

WizardGenerarCnmcIvc()
