Documentació del mòdul per generar el fixer CSV segons CNMC INF/DE/0066/44 per distribuïdora
============================================================================================

===========
Introducció
===========

Per poder fer una auditoria de canvis de comercialitzadora, la CNMC demana un
CSV amb el format definit a IS/DE/013116, anomenat Informe de Verificacio de
Consentiment (IVC).

=========
Generació
=========

Per generar el fitxer, hem d'executar l'assistent que trobarem a
`Administració pública > CNMC > INFORME VERIFICACIO CONSENTIMENT (IVC) > Pas 1: Generar Llistat`

.. image:: _static/menu.png


Només caldrà introduïr la data d'inici i de fi que desitgem (ambdues incloses)
i prèmer el botó **Crear**.

.. image:: _static/assistent_1.png

Un cop creat, des de **Obrir** es podra obrir el fitxer. També es pot guardar
fent clic a la icona de la dreta d'**Obrir**.

.. image:: _static/assistent_2.png


-----------------
Comercialitzadora
-----------------
^^^^^^^^^^^^^^^^^
Dades utilitzades
^^^^^^^^^^^^^^^^^

Per omplir el fitxer es tenen en compte totes les pólisses que compleixin les
següents condicions:

* Que estiguin creades entre les dates escollides i que tinguin la corresponent modificació contractual de tipus `alta`.

* Si s'utilitza switching també es tindrà en compte que existeixi un cas C1 o C2 associat a la polissa.

* En cas que s'utlitzi switching però no existeixi cap cas relacionat amb la polissa també es tindrà en compte.

Així doncs, el fitxer generat serà correcte si les altes a la comercialitzadora
es registren com a modificació contractual de la pòlissa de tipus `alta`
correctament i, en cas d'utilitzar switching per aquella pòlissa en concret, que
existeixi un cas relacionat amb la polissa del tipus C1 o C2.

+----------------------------+------------------------------------------------+
| **CODIGO_INFORMANTE**      | Camp `Codi R2` de la companyia                 |
+----------------------------+------------------------------------------------+
| **CODIGO_CUPS**            | CUPS                                           |
+----------------------------+------------------------------------------------+
| **FECHA_ACTIVACION**       | Data inici de la modificació contractual nova  |
+----------------------------+------------------------------------------------+


-------------
Distribuïdora
-------------
^^^^^^^^^^^^^^^^^
Dades utilitzades
^^^^^^^^^^^^^^^^^

Per omplir el fitxer es tenen en compte tots els canvis de comercialitzadora
registrats com a modificació contractual amb data d'inici entre les dates
escollides. Per fer-ho es comprova que la modificació anterior té una
comercialitzadora diferent a la que s'està analitzant.

Així doncs, el fitxer generat serà correcte si els canvis de comercialitzadora
es registren com a modificació contractual de la pòlissa correctament.

+-----------------------------+-----------------------------------------------+
| **CODIGO_INFORMANTE**       | Camp `Codi R1` de la companyia                |
+-----------------------------+-----------------------------------------------+
| **CODIGO_CUPS**             | CUPS                                          |
+-----------------------------+-----------------------------------------------+
| **FECHA_ACTIVACION**        | Data inici de la modificació contractual nova |
+-----------------------------+-----------------------------------------------+
| **CODIGO_COMERCIALIZADORA** | Camp `Ref2` amb el codi R2                    |
+-----------------------------+-----------------------------------------------+


.. note::
   :name: notar2

   Codi R2 (R2-xxx) de la CNMC gestionable en el camp `Ref2` de la fitxa de
   l'empresa comercialitzadora accessible des del menú `Empreses`
