# -*- coding: utf-8 -*-
{
    "name": "GISCE CNMC IVC",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Generació de fitxer CSV la llista de canvis de modificacions contractuals
    entre dues dates segons CNMC  IS/DE/013116""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "giscedata_polissa",
        "giscedata_administracio_publica_cne"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_cnmc_ivc_view.xml",
        "wizard/wizard_generar_cnmc_ivc_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
