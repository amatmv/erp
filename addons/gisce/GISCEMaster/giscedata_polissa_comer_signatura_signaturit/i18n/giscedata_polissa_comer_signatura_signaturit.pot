# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* giscedata_polissa_comer_signatura_signaturit
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.14\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2019-07-29 13:41\n"
"PO-Revision-Date: 2019-07-29 13:41\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: giscedata_polissa_comer_signatura_signaturit
#: model:ir.actions.act_window,name:giscedata_polissa_comer_signatura_signaturit.action_wizard_signature_model
msgid "Buscar Procesos de Firma"
msgstr ""

#. module: giscedata_polissa_comer_signatura_signaturit
#: view:wizard.signature.model:0
msgid "Listar processos de firma"
msgstr ""

#. module: giscedata_polissa_comer_signatura_signaturit
#: view:wizard.signature.model:0
msgid "Buscar procesos de firma relacionados"
msgstr ""

#. module: giscedata_polissa_comer_signatura_signaturit
#: constraint:ir.ui.view:0
msgid "Invalid XML for View Architecture!"
msgstr ""

#. module: giscedata_polissa_comer_signatura_signaturit
#: model:ir.module.module,shortdesc:giscedata_polissa_comer_signatura_signaturit.module_meta_information
msgid "Signatura digital de polisses dels clients Signaturit (Comercialitzadora)"
msgstr ""

#. module: giscedata_polissa_comer_signatura_signaturit
#: constraint:ir.actions.act_window:0
msgid "Invalid model name in the action definition."
msgstr ""

#. module: giscedata_polissa_comer_signatura_signaturit
#: view:wizard.signature.model:0
msgid "Buscar"
msgstr ""

