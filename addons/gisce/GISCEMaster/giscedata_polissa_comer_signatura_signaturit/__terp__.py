# -*- coding: utf-8 -*-
{
    "name": "Signatura digital de polisses dels clients Signaturit (Comercialitzadora)",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_signatura_documents_signaturit",
        "giscedata_polissa_comer_signatura",
    ],
    "init_xml": [],
    "demo_xml":[
    ],
    "update_xml":[
        "wizard_signature_model_polissa.xml",
        "giscedata_polissa_report.xml",
    ],
    "active": False,
    "installable": True
}
