# -*- coding: utf-8 -*-
from __future__ import absolute_import
from osv import osv, fields
from tools.translate import _
from giscedata_polissa.giscedata_polissa import CONTRACT_STATES


class WizardSignarContracteEsborrany(osv.osv_memory):

    _name = 'wizard.signar.contracte.esborrany'
    _inherit = 'wizard.signar.contracte.esborrany'

    def request(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        pol_obj = self.pool.get('giscedata.polissa')
        wizard = self.browse(cursor, uid, ids[0], context)
        email = wizard.email
        delivery_type = wizard.delivery_type
        context['delivery_type'] = delivery_type
        pol_id = context['active_id']
        if not email:
            raise osv.except_osv(
                _('Error!'),
                _(u"Es necessita una direcció amb correu electrònic on "
                  u"enviar el document a firmar")
            )
        # Signar
        process_id = pol_obj.start_signature_process(
            cursor, uid, pol_id, context=context)
        if delivery_type == 'url':
            return_val = {
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'giscedata.signatura.process',
                'type': 'ir.actions.act_window',
                'domain': [('id', 'in', [process_id])]
            }
        else:
            return_val = {'type': 'ir.actions.act_window_close'}
        return return_val

    _columns = {
        'delivery_type': fields.selection([
            ('url', 'Codi QR'),
            ('email', 'Correu electrònic')
        ], "Forma d'enviament", required=True),
    }

    _defaults = {
        'delivery_type': lambda *a: 'email',
    }


WizardSignarContracteEsborrany()
