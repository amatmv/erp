# -*- coding: utf-8 -*-
from __future__ import absolute_import
import json
from osv import osv
from datetime import datetime


class GiscedataPolissa(osv.osv):
    _name = "giscedata.polissa"
    _inherit = 'giscedata.polissa'
    _description = "Polissa d'un Client"

    def start_signature_process(self, cursor, uid, contract_id, context=None):
        """
        Starts a contract signature process from OV call.
        I get all the data needed for making that call.
        :param cursor:
        :param uid:
        :param ids:
        :param contract_id: Contract ID number
        :param context:
        :return:
        """

        imd_obj = self.pool.get('ir.model.data')
        man_obj = self.pool.get('payment.mandate')
        attach_obj = self.pool.get('ir.attachment')
        add_obj = self.pool.get('res.partner.address')
        pro_obj = self.pool.get('giscedata.signatura.process')

        # Crear el process
        pol_num = self.read(cursor, uid, contract_id, ['name'])['name'] or ' '
        account_iban = self.read(cursor, uid, contract_id, ['bank'])['bank'][1]

        today = datetime.strftime(datetime.now(), '%Y-%m-%d')

        mandate_reference = "giscedata.polissa,{}".format(
            contract_id
        )
        mandate_report_id = imd_obj.get_object_reference(
            cursor, uid, 'l10n_ES_remesas', 'report_mandato_generico'
        )[1]

        mandate_new_id = man_obj._create_mandate(
            cursor, uid, today, mandate_reference, 'core',
            context=context
        )

        man_obj.write(cursor, uid, [mandate_new_id], {
            'date': False,
            'signed': 0,
            'debtor_iban': account_iban.replace(" ", "")
        })

        contract_report_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'report_contracte'
        )[1]

        payment_id = self.read(
            cursor, uid, contract_id, ['direccio_pagament']
        )['direccio_pagament'][0]
        payment_values = add_obj.read(
            cursor, uid, payment_id, ['name', 'email', 'phone'])
        data = json.dumps({
            'callback_method': 'alta_contracte'
        })
        contract_categ = attach_obj.get_category_for(
            cursor, uid, 'contrato', context=context)
        mandate_categ = attach_obj.get_category_for(
            cursor, uid, 'mandato', context=context)
        values = {
            'subject': 'Firma de contrato ' + pol_num,
            'delivery_type': context.get('delivery_type', 'email'),
            'recipients': [
                (0, 0, {
                    'partner_address_id': payment_id,
                    'name': payment_values['name'],
                    'email': payment_values['email'],
                    # 'phone': payment_values['phone']
                })
            ],
            'reminders': 0,
            'type': 'advanced',
            'data': data,
            'all_signed': True,
            'body': 'Para finalizar el proceso de contratación deberá firmar '
                    'los documentos adjuntados con este correo electrónico.',
            'files': [
                (0, 0, {
                    'model': 'giscedata.polissa,{}'.format(contract_id),
                    'report_id': contract_report_id,
                    'category_id': contract_categ
                }),
                (0, 0, {
                    'model': 'payment.mandate,{}'.format(mandate_new_id),
                    'report_id': mandate_report_id,
                    'category_id': mandate_categ
                })
            ]
        }

        process_id = pro_obj.create(cursor, uid, values, context)

        # Executar l'inici del proces
        pro_obj.start(cursor, uid, [process_id], context=None)
        return process_id


GiscedataPolissa()
