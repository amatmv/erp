# -*- coding: utf-8 -*-
{
    "name": "Perfilació de lectures (valors 2014) Balears",
    "description": """
Actualitza els valors de perfilació per l'any 2014 a Balears
""",
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_perfils_2014"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_perfils_mm_2014_data.xml"
    ],
    "active": False,
    "installable": True
}
