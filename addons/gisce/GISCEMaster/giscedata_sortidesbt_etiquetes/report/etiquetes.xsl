<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="cts"/>
  </xsl:template>
  
  

  <xsl:template match="cts">
    <document>
      <template>
        <pageTemplate id="main">
          <frame id="s1" x1="0.9cm" y1="23.9cm" width="9.5cm" height="5.5cm" showBoundary="1" />
          <frame id="s1ct" x1="2cm" y1="23.9cm" width="7.5cm" height="1cm" />
          <frame id="s2" x1="10.6cm" y1="23.9cm" width="9.5cm" height="5.5cm" showBoundary="1" />
          <frame id="s2ct" x1="11.7cm" y1="23.9cm" width="7.5cm" height="1cm" />
          <frame id="s3" x1="0.9cm" y1="18.1cm" width="9.5cm" height="5.5cm" showBoundary="1" />
          <frame id="s3ct" x1="2cm" y1="18.1cm" width="7.5cm" height="1cm" />
          <frame id="s4" x1="10.6cm" y1="18.1cm" width="9.5cm" height="5.5cm" showBoundary="1" />
          <frame id="s4ct" x1="11.7cm" y1="18.1cm" width="7.5cm" height="1cm" />
          <frame id="s5" x1="0.9cm" y1="12.3cm" width="9.5cm" height="5.5cm" showBoundary="1" />
          <frame id="s5ct" x1="2cm" y1="12.3cm" width="7.5cm" height="1cm" />
          <frame id="s6" x1="10.6cm" y1="12.3cm" width="9.5cm" height="5.5cm" showBoundary="1" />
          <frame id="s6ct" x1="11.7cm" y1="12.3cm" width="7.5cm" height="1cm" />
          <frame id="s7" x1="0.9cm" y1="6.5cm" width="9.5cm" height="5.5cm" showBoundary="1" />
          <frame id="s7ct" x1="2cm" y1="6.5cm" width="7.5cm" height="1cm" />
          <frame id="s8" x1="10.6cm" y1="6.5cm" width="9.5cm" height="5.5cm" showBoundary="1" />
          <frame id="s8ct" x1="11.7cm" y1="6.5cm" width="7.5cm" height="1cm" />
          <frame id="s9" x1="0.9cm" y1="0.7cm" width="9.5cm" height="5.5cm" showBoundary="1" />
          <frame id="s9ct" x1="2cm" y1="0.7cm" width="7.5cm" height="1cm" />
          <frame id="s10" x1="10.6cm" y1="0.7cm" width="9.5cm" height="5.5cm" showBoundary="1" />
          <frame id="s10ct" x1="11.7cm" y1="0.7cm" width="7.5cm" height="1cm" />
        </pageTemplate>
      </template>
      
      <stylesheet>

        <paraStyle name="titol"
          fontName="Helvetica-Bold"
          fontSize="12"
          leading="30"
		/>
		<paraStyle name="text"
		  fontName="Helvetica"
		  fontSize="12"
		/>
		<paraStyle name="minitext"
		  fontName="Helvetica"
		  fontSize="8"
		/>



      </stylesheet>
    
      <story>
      	<xsl:apply-templates match="ct" mode="story" />
      </story>
    </document>
  </xsl:template>
  
  <xsl:template match="ct" mode="story">
    <xsl:for-each select="sortides">
      	  <para style="titol" alignment="center">SORTIDA N. <xsl:value-of select="position()" /></para>

      	  <para style="text"><b>Sortida: </b> <xsl:value-of select="descripcio" /></para>
      	  <para style="text"><b>Secci� l�nia: </b> <xsl:value-of select="concat(seccio, ' ', material)" /> </para>
      	  <para style="text"><b>Tensi�: </b> <xsl:if test="tensio!=''"><xsl:value-of select="format-number(tensio, '#')" /></xsl:if></para>
      	  <para style="text"><b>Observacions: </b></para>
      	  <nextFrame />
      	  <para style="minitext"><xsl:value-of select="ct" /></para>
      	  <nextFrame />
      	</xsl:for-each>
  </xsl:template>


</xsl:stylesheet>
