# -*- coding: utf-8 -*-
{
    "name": "GISCE Data Etiquetes per Sortides BT",
    "description": """Report per imprimir etiquetes per les Sortides BT""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE Master",
    "depends":[
        "giscedata_cts"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_sortidesbt_etiquetes_report.xml"
    ],
    "active": False,
    "installable": True
}
