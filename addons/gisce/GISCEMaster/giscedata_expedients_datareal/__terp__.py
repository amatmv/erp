# -*- coding: utf-8 -*-
{
    "name": "GISCE Data Expedients Data Real posada en marxa",
    "description": """Afegeix el camp 'data_real' a un expedient per tal de poder dir quina va ser la data real de posada en marxa.""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "giscedata_expedients"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_expedients_datareal_view.xml"
    ],
    "active": False,
    "installable": True
}
