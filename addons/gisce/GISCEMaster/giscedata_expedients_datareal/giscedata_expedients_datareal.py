# *-*  codig: utf-8 *-*

from osv import osv, fields

class giscedata_expedients_expedient(osv.osv):

    _name = 'giscedata.expedients.expedient'
    _inherit = 'giscedata.expedients.expedient'

    _columns = {
      'data_real': fields.date('Data Real'),
    }

giscedata_expedients_expedient()
