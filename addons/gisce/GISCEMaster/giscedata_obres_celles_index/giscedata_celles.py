from base_index.base_index import BaseIndex

MODEL_ID_TEMPLATE = {
    "giscegis.blocs.seccionadorunifilar": "1{0}",
    "giscegis.blocs.seccionadorat": "2{0}",
    "giscegis.blocs.fusiblesat": "3{0}",
    "giscegis.blocs.interruptorat": "4{0}"
}


class GiscedataCelles(BaseIndex):
    _name = "giscedata.celles.cella"
    _inherit = "giscedata.celles.cella"
    _index_fields = {
        "obres": lambda self, data: ' '.join(["obra{0}".format(d.name) or "" for d in data])
    }

    def get_module_id(self, cursor, uid, cella_name, context=None):
        """
        Returns the gis model and the id of the given cella name

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param cella_name: Name of the cella
        :type cella_name: str
        :param context:
        :return: Model name, id
        :rtype: tuple or None
        """

        mod_fus = self.pool.get("giscegis.blocs.fusiblesat")
        mod_sec_uni = self.pool.get("giscegis.blocs.seccionadorunifilar")
        mod_int = self.pool.get("giscegis.blocs.interruptorat")
        mod_sec = self.pool.get("giscegis.blocs.seccionadorat")

        search_params = [("codi", "=", cella_name)]
        res_fus = mod_fus.search(cursor, uid, search_params)
        if len(res_fus) == 1:
            return "giscegis.blocs.seccionadorat", res_fus[0]

        res_sec_uni = mod_sec_uni.search(cursor, uid, search_params)
        if len(res_sec_uni) == 1:
            return "giscegis.blocs.seccionadorunifilar", res_sec_uni[0]

        res_int = mod_int.search(cursor, uid, search_params)
        if len(res_int) == 1:
            return "giscegis.blocs.interruptorat", res_int[0]

        res_sec = mod_sec.search(cursor, uid, search_params)
        if len(res_sec) == 1:
            return "giscegis.blocs.seccionadorat", res_sec[0]
        return False

    def get_path(self, cursor, uid, item_id, context=None):
        """
        Returns the path to index

        :param cursor: Database cursor
        :param uid: User id
        :param item_id: Item to get the path
        :param context: OpenERP context
        :return: path to index
        :rtype: str or bool
        """

        name = self.read(cursor, uid, item_id, ["name"])["name"]
        model_name, identifier = self.get_module_id(cursor, uid, name, context)
        if model_name:
            identifier = MODEL_ID_TEMPLATE[model_name].format(identifier)
            return "giscedata.interruptors.at/{}".format(identifier)
        else:
            return False

    def rec_getattr(self, obj, attr):
        """
        Returns the readed values

        :param obj: Object to read the values
        :type obj: object
        :param attr: value to read using dot syntax
        :type attr: str
        :return: Readed values
        :rtype: dict
        """
        return reduce(getattr, attr.split('.'), obj)

    def get_schema(self, cursor, uid, item, context=None):
        """
        Gets the schema of index

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param item: Item to index
        :param context: OpenERP context
        :type context: dict
        :return: Schema to index
        :rtype: dict
        """

        lat, lon = self.get_lat_lon(cursor, uid, item, context=context)
        content = self.get_content(cursor, uid, item, context=context)

        if content:
            return {
                'id': item.id,
                'path': self.get_path(cursor, uid, item.id, context=context),
                'title': self._index_title.format(obj=item),
                'content': ' '.join(set(content)),
                'gistype': "giscedata.interruptors.at",
                'summary': self._index_summary.format(obj=item),
                'lat': lat,
                'lon': lon
            }
        return False

    def get_content(self, cursor, uid, item, context=None):
        """
        Overrides get_content

        :param cursor: Database cursor
        :param uid: User id
        :param item: Item to index
        :param context: OpenERP context
        :return: Content to index
        :rtype: str of bool
        """
        content = self.get_obres(cursor, uid, item)
        ret = self.get_module_id(cursor, uid, item.name)

        if ret:
            model_name, identifier = ret
            mod_gis = self.pool.get(model_name)
            item = mod_gis.browse(cursor, uid, identifier)
            for field, value in mod_gis._index_fields.items():
                try:
                    data = self.rec_getattr(item, field)
                except AttributeError:
                    continue
                if callable(value):
                    data = value(self, data)
                if data:
                    content.append(str(data))
        return content

    def get_obres(self, cursor, uid, item, context=None):
        mod_cel = self.pool.get("giscedata.celles.cella")
        mod_obr = self.pool.get("giscedata.obres.obra")
        obres = mod_cel.read(cursor, uid, item.id, ["obres"])
        res_obres = mod_obr.read(cursor, uid, obres["obres"], ["name"])
        cont = []
        for obra in res_obres:
            cont.append("obra"+obra["name"])
        return cont

    def get_lat_lon(self, cursor, uid, item, context=None):
        """
        Returns the lat and lon of the element

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param item: Item to index
        :param context: OpenERP context
        :return: lat,lon
        :rtype: tuple
        """
        name = self.read(cursor, uid, item.id, ["name"])["name"]
        ret = self.get_module_id(cursor, uid, name, context)
        if ret:
            model_name, identifier = ret
            sql_template = """
            SELECT
                ST_Y(ST_GeometryN(ST_Transform(gv.geom,4326),1)
                     ) AS lat,
                ST_X(ST_GeometryN(ST_Transform(gv.geom,4326),1)
                     ) AS lon
            FROM giscegis_interruptorsat_geom gv
            WHERE
              gv.id = %s
            """
            identifier = MODEL_ID_TEMPLATE[model_name].format(identifier)
            cursor.execute(sql_template, (identifier, ))
            return cursor.fetchone()
        return 0, 0

GiscedataCelles()
