# -*- coding: utf-8 -*-
from osv import osv, fields

TECHNOLOGY_TYPE_SEL = [
    ('mechanic', 'Electromecànic')
 ]


class GiscedataLecturesComptador(osv.osv):
    """Comptador de distribuidrora.
    """

    _name = 'giscedata.lectures.comptador'
    _inherit = 'giscedata.lectures.comptador'

    _columns = {
        'technology_type': fields.selection(TECHNOLOGY_TYPE_SEL, 'Tecnologia')
    }

    _defaults = {
        'technology_type': lambda *a: 'mechanic',
    }


GiscedataLecturesComptador()
