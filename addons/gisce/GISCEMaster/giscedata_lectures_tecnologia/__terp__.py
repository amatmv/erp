# -*- coding: utf-8 -*-
{
    "name": "Selector de tecnologia de comptadors",
    "description": """Tecnologia dels comptadors""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_lectures"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_lectures_view.xml"
    ],
    "active": False,
    "installable": True
}
