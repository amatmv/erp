# -*- coding: utf-8 -*-

import netsvc

def migrate(cursor, installed_version):
  """Migration to 2.38.0
  """
  logger = netsvc.Logger()
  logger.notifyChannel(
      'migration',
      netsvc.LOG_INFO,
      'Setting the default do_unpay to 0 to all devolutions'
  )
  cursor.execute("ALTER TABLE giscedata_facturacio_devolucio ADD column do_unpay boolean")
  cursor.execute("UPDATE giscedata_facturacio_devolucio SET do_unpay = False")

  logger.notifyChannel('migration', netsvc.LOG_INFO, 'Done.')
