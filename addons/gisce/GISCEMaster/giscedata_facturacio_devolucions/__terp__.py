# -*- coding: utf-8 -*-
{
    "name": "Devolucions remeses",
    "description": """
Gestió de devolucions del banc
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Facturació",
    "depends": [
        "devolucions_base",
        "giscedata_facturacio",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "giscedata_facturacio_devolucions_data.xml",
        "giscedata_facturacio_view.xml",
        "security/ir.model.access.csv",
    ],
    "active": False,
    "installable": True
}
