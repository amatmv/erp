import sys
import requests
import json


api_url = "https://api.cnmc.gob.es/maestras/v1"
sips_proc_id = 2

outfile = "/tmp/tablas_maestras.py"

# get SIPS tables
uri = "{0}/tablas/procedimiento/{1}".format(api_url, sips_proc_id)
sys.stdout.write("Getting tables list ({0})...".format(uri))
sys.stdout.flush()
r = requests.get(uri)

tables = r.json()
sys.stdout.write("ok\n".format(uri))
sys.stdout.write("    Found {0} tables\n".format(len(tables)))

tablas_maestras = [] 

for table in tables:
    table_name = table['nombreCorto']
    uri = "{0}/tabla/{1}".format(api_url, table_name)
    sys.stdout.write("Getting table {0} ({1})...".format(table_name, uri))
    sys.stdout.flush()
    r = requests.get(uri)
    selection = r.json()
    sys.stdout.write("ok\n".format(uri))
    tablas_maestras.append({'meta': table, 'selection': selection})
    sys.stdout.flush()

with open(outfile,'w') as pyfile:
    for table in tablas_maestras:
        pyfile.write('# {0} \n'.format(
            table['meta']['nombre'].encode('utf8'))
        )
        pyfile.write('# {0} \n'.format(
            table['meta']['descripcion'].encode('utf8'))
        )
        pyfile.write('# {0} \n'.format(
            table['meta']['nombreCorto'].encode('utf8'))
        )

        table_data = dict([(i['clave'], i['valor']) for i in table['selection']])
        pyfile.write(
            json.dumps(
                table_data,
                ensure_ascii=False,
                sort_keys=True,
                indent=4,
            ).encode('utf8')
        )
        pyfile.write('\n')
        pyfile.write('\n')
