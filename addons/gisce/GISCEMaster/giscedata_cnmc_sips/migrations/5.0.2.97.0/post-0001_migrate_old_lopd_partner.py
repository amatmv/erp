# coding=utf-8
import pooler
import logging
from tqdm import tqdm

logger = logging.getLogger('openerp.' + __name__)


def up(cursor, installed_version):
    if not installed_version:
        return
    pool = pooler.get_pool(cursor.dbname)
    uid = 1

    partner_obj = pool.get('res.partner')
    polissa_obj = pool.get('giscedata.polissa')

    logger.info('Migrating old values from giscedata_cnmc_sips lopd_exclude')
    partner_ids = partner_obj.search(cursor, uid, [
        ('lopd_exclude', '=', 1)
    ], context={'active_test': False})
    props = ['lopd_exclude', 'lopd_start_date']
    for partner in tqdm(partner_obj.read(cursor, uid, partner_ids, props)):
        polissa_ids = polissa_obj.search(cursor, uid, [
            ('titular.id', '=', partner['id'])
        ], context={'active_test': False})
        polissa_obj.write(cursor, uid, polissa_ids, {
            'no_cessio_sips': 'active',
            'no_cessio_sips_data': partner['lopd_start_date']
        })
    logger.info('Done')

migrate = up
