# -*- coding: utf-8 -*-
from __future__ import absolute_import

from osv import osv, fields

from tools import config
from tools.sql import install_array_agg


class GiscedataCNMCSipsMedidas(osv.osv):

    _name = 'giscedata.cnmc.sips.medidas'
    _auto = False

    def get_sql(self, cursor):
        sql = open('%s/%s/sql/sips2_medidas.sql'
                   % (config['addons_path'],
                      self._module)).read()
        return sql

    def init(self, cursor):
        # Install array_agg for postgresql < 8.4
        install_array_agg(cursor)
        sql = self.get_sql(cursor)
        cursor.execute("""DROP VIEW IF EXISTS giscedata_cnmc_sips_medidas""")
        cursor.execute('''
            CREATE OR REPLACE VIEW giscedata_cnmc_sips_medidas AS
              (%s)''' % sql)

    _columns = {
        'id': fields.integer('ID', readonly=True),
        'polissa_id': fields.integer('Polissa ID', readonly=True),
        'cups': fields.char('CUPS', size="22", readonly=True),
        'periode': fields.char('Periode', size="6", readonly=True),
        'ano': fields.char('Any', size="4", readonly=True),
        'fecha_lectura_anterior': fields.date('Lectura anterior',
                                              readonly=True),
        'fecha_lectura_actual': fields.date('Lectura actual', readonly=True),
        'tarifa': fields.char('Tarifa', size="6", readonly=True),
        'energia_activa_p1': fields.integer('Activa P1', readonly=True),
        'energia_activa_p2': fields.integer('Activa P2', readonly=True),
        'energia_activa_p3': fields.integer('Activa P3', readonly=True),
        'energia_activa_p4': fields.integer('Activa P4', readonly=True),
        'energia_activa_p5': fields.integer('Activa P5', readonly=True),
        'energia_activa_p6': fields.integer('Activa P6', readonly=True),
        'energia_reactiva_p1': fields.integer('Reactiva P1', readonly=True),
        'energia_reactiva_p2': fields.integer('Reactiva P2', readonly=True),
        'energia_reactiva_p3': fields.integer('Reactiva P3', readonly=True),
        'energia_reactiva_p4': fields.integer('Reactiva P4', readonly=True),
        'energia_reactiva_p5': fields.integer('Reactiva P5', readonly=True),
        'energia_reactiva_p6': fields.integer('Reactiva P6', readonly=True),
        'potencia_maxima_p1': fields.integer('Max. P1', readonly=True),
        'potencia_maxima_p2': fields.integer('Max. P2', readonly=True),
        'potencia_maxima_p3': fields.integer('Max. P3', readonly=True),
        'potencia_maxima_p4': fields.integer('Max. P4', readonly=True),
        'potencia_maxima_p5': fields.integer('Max. P5', readonly=True),
        'potencia_maxima_p6': fields.integer('Max. P6', readonly=True),
    }

GiscedataCNMCSipsMedidas()


class GiscedataCNMCSipsPS(osv.osv):

    _name = 'giscedata.cnmc.sips.ps'
    _auto = False

    def get_sql(self, cursor):
        sql = open('%s/%s/sql/sips2_ps.sql'
                   % (config['addons_path'],
                      self._module)).read()
        return sql

    def init(self, cursor):
        # Install array_agg for postgresql < 8.4
        install_array_agg(cursor)
        sql = self.get_sql(cursor)
        cursor.execute("""DROP VIEW IF EXISTS giscedata_cnmc_sips_ps""")
        cursor.execute('''
            CREATE OR REPLACE VIEW giscedata_cnmc_sips_ps as
            (%s)''' % sql)

    _columns = {
        'id': fields.integer('ID', readonly=True),
        'polissa_id': fields.integer('Polissa ID', readonly=True),
        'cups': fields.char('CUPS', size="22", readonly=True),
        'direccion_del_suministro': fields.char('Adreça CUPS', size="250", readonly=True),
        'poblacion_del_suministro': fields.char('Població', size="50", readonly=True),
        'codigos_postal_del_suministro': fields.char('CP', size="10", readonly=True),
        'ine_municipio': fields.char('INE municipi', size="6", readonly=True),
        'provincia_del_suministro': fields.char('Provincia', size="100", readonly=True),
        'ine_provincia': fields.char('INE Provincia', size="6", readonly=True),
        'empresa_distribuidora': fields.char('Distribuïdora', size="100", readonly=True),
        'codigo_tarifa': fields.char('Tarifa CNMC', size=3, readonly=True),
        'codigo_tension': fields.char('Tensió CNMC', size=3, readonly=True),
        'tension': fields.char('Tensió', size=64, readonly=True),
        'tension_v': fields.integer('Tensió V', readonly=True),
        'codigo_empresa_distribuidora': fields.char('Codi REE', size="4", readonly=True),
        'documento_identidad': fields.char("Document identitat", size="15", readonly=True),
        'tipo_documento_identidad': fields.char('Tipus de document', size="20", readonly=True),
        'identificador': fields.char('Identificador', size="25", readonly=True),
        'nombre_y_apellidos_denominacion_social': fields.char('Titular', size="250", readonly=True),
        'domicilio_del_titular': fields.char('Dom. Titular', size="250", readonly=True),
        'poblacion_del_titular': fields.char('Pob. Titular', size="250", readonly=True),
        'codigo_postal_del_titular': fields.char('CP Titular', size="10", readonly=True),
        'provincia_del_titular': fields.char('Prov. Titular', size="50", readonly=True),
        'tipo_vivienda': fields.char('Tipus de vivenda', size="40", readonly=True),
        'impagos': fields.char('Impagament', size="40", readonly=True),
        'deposito_de_garantia': fields.char('Garantia', size="30", readonly=True),
        'autorizacion_cesion_datos': fields.char('Autorització', size="2", readonly=True),
        'fecha_alta': fields.date('Data alta', readonly=True),
        'tipo_coeficiente': fields.char('Coeficient', size="16", readonly=True),
        'potencia_maxima_autorizada': fields.float('Pot. Max.', digits=(16,3), readonly=True),
        'potencia_maxima_autorizada_acta_puesta_en_marcha': fields.float('Pot. Act.', digits=(16,3), readonly=True),
        'tipo_de_punto_de_medida': fields.integer('Tipus de punt', readonly=True),
        'icp_instalado': fields.char('ICP Instal·lat', size="4", readonly=True),
        'tipo_perfil_de_consumo': fields.char('Tipus perfil', size="2", readonly=True),
        'discriminacion_horaria': fields.char('Discriminació', size="6", readonly=True),
        'derechos_de_acceso_reconocidos': fields.float('Drets d\'accés reconeguts', digits=(16,3), readonly=True),
        'derechos_de_extension_reconocidos': fields.float('Drets d\'extensió reconeguts', digits=(16,3), readonly=True),
        'propiedad_equipo_medida': fields.char('Propietat Equip de mesura', size="20", readonly=True),
        'propiedad_del_icp': fields.char('Propietat ICP', size="20", readonly=True),
        'potencia_periodo_1': fields.float('Pot. P1', digits=(16,3), readonly=True),
        'potencia_periodo_2': fields.float('Pot. P2', digits=(16,3), readonly=True),
        'potencia_periodo_3': fields.float('Pot. P3', digits=(16,3), readonly=True),
        'potencia_periodo_4': fields.float('Pot. P4', digits=(16,3), readonly=True),
        'potencia_periodo_5': fields.float('Pot. P5', digits=(16,3), readonly=True),
        'potencia_periodo_6': fields.float('Pot. P6', digits=(16,3), readonly=True),
        'potencia_periodo_7': fields.float('Pot. P7', digits=(16,3), readonly=True),
        'potencia_periodo_8': fields.float('Pot. P8', digits=(16,3), readonly=True),
        'potencia_periodo_9': fields.float('Pot. P9', digits=(16,3), readonly=True),
        'potencia_periodo_10': fields.float('Pot. P10', digits=(16,3), readonly=True),
        'fecha_ultimo_movimiento_de_contratacion': fields.date('Ult. Movimiento', readonly=True),
        'fecha_ultimo_cambio_comercializador': fields.date('Ult. Cambio comer.', readonly=True),
        'fecha_limite_derechos_de_extension': fields.date('Data limit drets extensió', readonly=True),
        'fecha_ultima_lectura': fields.date('Data darrera lectura', readonly=True),
        'tipo_telegestion': fields.char('Tipus Telegestió', size=3, readonly=True),
        'aplicacion_bono_social': fields.char('Aplicació de bo social', size=1,
                                              readonly=True),
        'codigo_comercializadora': fields.char('Codi comercialitzadora', size=4,
                                               readonly=True)


    }

GiscedataCNMCSipsPS()
