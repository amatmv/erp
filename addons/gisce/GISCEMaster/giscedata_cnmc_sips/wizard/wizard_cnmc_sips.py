# -*- coding: utf-8 -*-
from __future__ import absolute_import

import base64
import time
import csv
import StringIO
import zipfile
from datetime import datetime

from osv import osv, fields

import netsvc

from gestionatr.defs import CONV_T109_T111


class GiscedataCNMCSipsExportWizard(osv.osv_memory):
    """Wizard for exporting CNMC SIPS format.
    """
    _name = 'giscedata.cnmc.sips.export.wizard'

    _columns = {
        'name': fields.char('Nom', size=256),
        'file': fields.binary('Fitxer'),
        'state': fields.selection([('init', 'Init'), ('end', 'End')],
                                  'Estat')
    }
    _defaults = {
        'state': lambda *a: 'init',
        'name': lambda *a: '%s_electricidad.zip' % time.strftime('%Y-%m-%d')
    }

    def export_file(self, cursor, uid, ids, context=None):
        """Export cnmc file function.
        """
        ocsum_cups_obj = self.pool.get('giscedata.cnmc.sips.ps')
        ocsum_lect_obj = self.pool.get('giscedata.cnmc.sips.medidas')

        HEADERS_CUPS = [
            u"codigoEmpresaDistribuidora",
            u"cups",
            u"nombreEmpresaDistribuidora",
            u"codigoPostalPS",
            u"municipioPS",
            u"codigoProvinciaPS",
            u"fechaAltaSuministro",
            u"codigoTarifaATREnVigor",
            u"codigoTensionV",
            u"potenciaMaximaBIEW",
            u"potenciaMaximaAPMW",
            u"codigoClasificacionPS",
            u"codigoDisponibilidadICP",
            u"tipoPerfilConsumo",
            u"valorDerechosExtensionEnW",
            u"valorDerechosAccesoEnW",
            u"codigoPropiedadEquipoMedida",
            u"codigoPropiedadICP",
            u"potenciasContratadasEnWP1",
            u"potenciasContratadasEnWP2",
            u"potenciasContratadasEnWP3",
            u"potenciasContratadasEnWP4",
            u"potenciasContratadasEnWP5",
            u"potenciasContratadasEnWP6",
            u"fechaUltimoMovimientoContrato",
            u"fechaUltimoCambioComercializador",
            u"fechaLimiteDerechosReconocidos",
            u"fechaUltimaLectura",
            u"informacionImpagos",
            u"importeDepositoGarantia",
            u"tipoIdTitular",
            u"esViviendaHabitual",
            u"codigoComercializadora",
            u"codigoTelegestion",
            u"codigoFaseEquipoMedida",
            u"codigoAutoconsumo",
            # optional fields
            u"codigoTipoContrato",
            u"codigoPeriodicidadFacturacion",
            u"codigoBIE",
            u"fechaEmisionBIE",
            u"fechaCaducidadBIE",
            u"codigoAPM",
            u"fechaEmisionAPM",
            u"fechaCaducidadAPM",
            u"relacionTransformacionIntensidad",
            u"cnae",
            u"codigoModoControlPotencia",
            u"potenciaCGPW",
            u"codigoDHEquipoDeMedida",
            u"codigoAccesibilidadContador",
            u"codigoPSContratable",
            u"motivoEstadoNoContratable",
            u"codigoTensionMedida",
            u"codigoClaseExpediente",
            u"codigoMotivoExpediente",
            u"codigoTipoSuministro",
            u"AplicacionBonoSocial",
        ]
        
        HEADERS_LECT = ([
            u"cups",
            u"fechaInicioConsumo",
            u"fechaFinConsumo",
            u"codigoTarifaATR",
            u"consumoEnergiaActivaEnWhP1",
            u"consumoEnergiaActivaEnWhP2",
            u"consumoEnergiaActivaEnWhP3",
            u"consumoEnergiaActivaEnWhP4",
            u"consumoEnergiaActivaEnWhP5",
            u"consumoEnergiaActivaEnWhP6",
            u"consumoEnergiaReactivaEnVArhP1",
            u"consumoEnergiaReactivaEnVArhP2",
            u"consumoEnergiaReactivaEnVArhP3",
            u"consumoEnergiaReactivaEnVArhP4",
            u"consumoEnergiaReactivaEnVArhP5",
            u"consumoEnergiaReactivaEnVArhP6",
            u"potenciaDemandadaEnWP1",
            u"potenciaDemandadaEnWP2",
            u"potenciaDemandadaEnWP3",
            u"potenciaDemandadaEnWP4",
            u"potenciaDemandadaEnWP5",
            u"potenciaDemandadaEnWP6",
            # optional fields
            u"codigoDHEquipoDeMedida",
            u"codigoTipoLectura"
        ])
        
        zip_io = StringIO.StringIO()
        zip = zipfile.ZipFile(zip_io, 'w',
                              compression=zipfile.ZIP_DEFLATED)

        logger = netsvc.Logger()
        logger.notifyChannel("addons", netsvc.LOG_INFO,
                             "Starting CNMC SIPS export")

        sql = ocsum_cups_obj.get_sql(cursor)
        cursor.execute(sql)
        # Indexem per pòlissa
        output = StringIO.StringIO()
        writer = csv.writer(output, delimiter=',')
        writer.writerow(HEADERS_CUPS)
    
        for data in cursor.dictfetchall():
            r1_code = data['codigo_empresa_distribuidora']
            # If contains 3x or tension > 400 is a Triphasic meter
            tension_name = data['tension_name']
            is_triphasic = (
                tension_name and '3x' in tension_name
                or data['tension_v'] > 400
            )
            line = [
                r1_code,
                data['cups'],
                data['nombre_distri'],
                data['codigo_postal_del_suministro'],
                data['municipio_ine'],
                data['provincia_code'],
                data['fecha_alta'],
                data['tarifa_code'],
                data['tension'],
                int(data['potencia_maxima_autorizada'] * 1000),
                int(data['potencia_maxima_autorizada_acta_puesta_en_marcha']
                    * 1000),
                '0{0}'.format(data['tipo_de_punto_de_medida']),
                data['icp_instalado'] == 'ICP instalado' and 1 or 0,
                (data['tipo_coeficiente']
                 and 'P%s' % data['tipo_coeficiente'][-1] or ''),
                int(data['derechos_de_extension_reconocidos'] * 1000),
                int(data['derechos_de_acceso_reconocidos']
                    and data['derechos_de_acceso_reconocidos'] * 1000 or 0),
                (data['propiedad_equipo_medida'][0]),
                (data['propiedad_del_icp']
                 and data['propiedad_del_icp'][0]
                 or data['propiedad_equipo_medida'][0]),
                str(int(data['potencia_periodo_1']
                        and data['potencia_periodo_1'] * 1000 or False) or ''),
                str(int(data['potencia_periodo_2']
                        and data['potencia_periodo_2'] * 1000 or False) or ''),
                str(int(data['potencia_periodo_3']
                        and data['potencia_periodo_3'] * 1000 or False) or ''),
                str(int(data['potencia_periodo_4']
                        and data['potencia_periodo_4'] * 1000 or False) or ''),
                str(int(data['potencia_periodo_5']
                        and data['potencia_periodo_5'] * 1000 or False) or ''),
                str(int(data['potencia_periodo_6']
                        and data['potencia_periodo_6'] * 1000 or False) or ''),
                data['fecha_ultimo_movimiento_de_contratacion'],
                data['fecha_ultimo_cambio_comercializador'],
                data['fecha_limite_derechos_de_extension'],
                data['fecha_ultima_lectura'],
                data['impagos'],
                0,  # DepositoGarantia
                data['tipo_documento_identidad'],
                (data['tipo_vivienda'] == 'Vivienda habitual'
                 and 1 or 0),
                data['codigo_comercializadora'],  # Código comercializadora
                CONV_T109_T111[str(data['telegestion']).zfill(2)],
                is_triphasic and 'T' or 'M',
                '00',  # Autoconsumo
            ]
            # 20 Empty Optional fields
            line.extend([''] * 20)
            line.append(data['aplicacion_bono_social'])
            tmp = [isinstance(t, basestring) and t.encode('utf-8')
                   or t for t in line]
            writer.writerow(line)

        date_now = datetime.strftime(datetime.now(), '%Y-%m-%d')
        zip.writestr('%s_electricidad_ps.csv' % date_now,
                     output.getvalue())

        # _electricidad_consumos.csv
        output = StringIO.StringIO()
        writer = csv.writer(output, delimiter=',')
        writer.writerow(HEADERS_LECT)
        
        # Get the sql from ocsum medidas model
        sql = ocsum_lect_obj.get_sql(cursor)
        cursor.execute(sql)
        for row in cursor.fetchall():
            row = list(row)
            line = [row[2]]  # CUPS
            line.extend(row[5:])
            # optional fields
            line.extend([''] * 2)

            writer.writerow(line)
 
        zip.writestr('%s_electricidad_consumos.csv' % date_now,
                     output.getvalue())

        zip.close()
        mfile = base64.b64encode(zip_io.getvalue())
        self.write(cursor, uid, ids, {'state': 'end', 'file': mfile}, context)
        logger.notifyChannel("addons", netsvc.LOG_INFO,
                             "Ending CNMC SIPS export")

GiscedataCNMCSipsExportWizard()
