SELECT
  lectures.lectura_id AS id,
  f.polissa_id as polissa_id,
  cups.name AS cups,
  to_char(lot.data_final, 'YYYYMM') as periode,
  to_char(f.data_final, 'YYYY') as ano,
  lectures.data_anterior +
  CASE
    WHEN lectures.data_anterior = c.data_alta
    THEN -1
    ELSE 0
  END AS fecha_lectura_anterior,
  lectures.data_actual AS fecha_lectura_actual,
  tarifa.codi_ocsum as tarifa,
  COALESCE(energies.consums[1] * 1000, 0)::bigint as energia_activa_p1,
  COALESCE(energies.consums[2] * 1000, 0)::bigint as energia_activa_p2,
  COALESCE(energies.consums[3] * 1000, 0)::bigint as energia_activa_p3,
  COALESCE(energies.consums[4] * 1000, 0)::bigint as energia_activa_p4,
  COALESCE(energies.consums[5] * 1000, 0)::bigint as energia_activa_p5,
  COALESCE(energies.consums[6] * 1000, 0)::bigint as energia_activa_p6,
  COALESCE(reactiva.consums[1] * 1000, 0)::bigint as energia_reactiva_p1,
  COALESCE(reactiva.consums[2] * 1000, 0)::bigint as energia_reactiva_p2,
  COALESCE(reactiva.consums[3] * 1000, 0)::bigint as energia_reactiva_p3,
  COALESCE(reactiva.consums[4] * 1000, 0)::bigint as energia_reactiva_p4,
  COALESCE(reactiva.consums[5] * 1000, 0)::bigint as energia_reactiva_p5,
  COALESCE(reactiva.consums[6] * 1000, 0)::bigint as energia_reactiva_p6,
  COALESCE(potencia.maximetre[1] * 1000, 0)::bigint as potencia_maxima_p1,
  COALESCE(potencia.maximetre[2] * 1000, 0)::bigint as potencia_maxima_p2,
  COALESCE(potencia.maximetre[3] * 1000, 0)::bigint as potencia_maxima_p3,
  COALESCE(potencia.maximetre[4] * 1000, 0)::bigint as potencia_maxima_p4,
  COALESCE(potencia.maximetre[5] * 1000, 0)::bigint as potencia_maxima_p5,
  COALESCE(potencia.maximetre[6] * 1000, 0)::bigint as potencia_maxima_p6
FROM giscedata_facturacio_factura f
INNER JOIN giscedata_polissa p
  ON p.id = f.polissa_id
INNER JOIN giscedata_cups_ps cups
  ON cups.id = f.cups_id
INNER JOIN giscedata_facturacio_lot lot ON
(f.lot_facturacio = lot.id
 and to_char(lot.data_final, 'YYYYMM') < to_char(now(), 'YYYYMM')
 and to_char(lot.data_final, 'YYYY01') > to_char(now() -
 interval '3 year', 'YYYY01'))
INNER JOIN account_invoice i
ON (f.invoice_id = i.id and i.type = 'out_invoice')
LEFT JOIN account_journal j
ON (i.journal_id = j.id and j.code ilike 'ENERGIA%')
LEFT JOIN (SELECT min(l.data_anterior) as data_anterior,
                  max(l.data_actual) as data_actual,
                  min(l.id) as lectura_id,
                  min(l.comptador_id) as comptador_id,
                  l.factura_id as factura_id
           FROM giscedata_facturacio_lectures_energia l
           GROUP BY l.factura_id) lectures
ON (lectures.factura_id = f.id)
LEFT JOIN giscedata_polissa_tarifa tarifa
ON (f.tarifa_acces_id = tarifa.id)
LEFT JOIN (SELECT array_agg(name) as periodes,
          array_agg(qty) as consums,
          factura_id
       FROM (SELECT il.name,
            sum(il.quantity) as qty,
            l.factura_id
         FROM giscedata_facturacio_factura_linia l
         INNER JOIN account_invoice_line il
         ON (l.invoice_line_id = il.id)
         WHERE l.tipus = 'energia'
         GROUP BY l.factura_id, il.name
             ORDER BY l.factura_id, il.name) as foo
       GROUP BY factura_id) energies
ON (energies.factura_id = f.id)
LEFT JOIN (SELECT array_agg(name) as periodes,
          array_agg(qty) as consums,
          factura_id
       FROM (SELECT il.name,
            sum(il.quantity) as qty,
            l.factura_id
         FROM giscedata_facturacio_factura_linia l
         INNER JOIN account_invoice_line il
         ON (l.invoice_line_id = il.id)
         WHERE l.tipus = 'reactiva'
         GROUP BY l.factura_id, il.name
             ORDER BY l.factura_id, il.name) as foo
       GROUP BY factura_id) reactiva
ON (reactiva.factura_id = f.id)
LEFT JOIN (SELECT array_agg(name) as periodes,
          array_agg(pot_maximetre) as maximetre,
          factura_id
       FROM (SELECT l.name,
            l.pot_maximetre,
            l.factura_id
         FROM giscedata_facturacio_lectures_potencia l
         GROUP BY l.factura_id, l.name, l.pot_maximetre
             ORDER BY l.factura_id, l.name) as foo
       GROUP BY foo.factura_id) potencia
       ON (potencia.factura_id = f.id)
LEFT JOIN (SELECT count(id) as num_periodes, tarifa
           FROM giscedata_polissa_tarifa_periodes
           WHERE tipus = 'te' group by tarifa) tarifa_periodes
           ON tarifa_periodes.tarifa = f.tarifa_acces_id
LEFT JOIN giscedata_lectures_comptador c ON lectures.comptador_id=c.id
WHERE f.tipo_rectificadora in ('N', 'R', 'RA') and f.tipo_factura = '01'
AND p.state not in ('esborrany', 'validar', 'cancelada')
AND (not p.no_cessio_sips = 'active' and p.state != 'impagament')
-- Exclude refunded invoices
AND i.refund_by_id IS NULL
AND coalesce(lectures.lectura_id, 0) <> 0
-- Exclude Generation CUPS (RE)
and tarifa.name not in ('RE', 'RE12')
ORDER BY polissa_id asc, periode desc
