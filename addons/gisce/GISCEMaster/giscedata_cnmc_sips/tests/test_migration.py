# coding=utf-8
import imp
from destral import testing
from addons import get_module_resource


class TestMigrations(testing.OOTestCaseWithCursor):

    def test_0001_migrate_old_lopd_partner(self):
        cursor = self.txn.cursor
        uid = self.txn.user
        pool = self.openerp.pool

        imd_obj = pool.get('ir.model.data')
        pol_obj = pool.get('giscedata.polissa')
        partner_obj = pool.get('res.partner')

        # Agafem dos contractes i els treiem del tiutlar camptocamp
        pol_ids = []
        for ref in ('polissa_0009', 'polissa_0007'):
            pol_ids.append(
                imd_obj.get_object_reference(
                    cursor, uid, 'giscedata_polissa', ref
                )[1]
            )
        titular_id = imd_obj.get_object_reference(
            cursor, uid, 'base', 'res_partner_sednacom'
        )[1]
        pol_obj.write(cursor, uid, pol_ids, {'titular': titular_id})
        partner_obj.write(cursor, uid, [titular_id], {
            'lopd_exclude': 1,
            'lopd_start_date': '2019-04-30'
        })

        script = get_module_resource(
            'giscedata_cnmc_sips', 'migrations', '5.0.2.97.0',
            'post-0001_migrate_old_lopd_partner.py'
        )
        mod = imp.load_source('migration', script)
        migrate = getattr(mod, 'migrate')
        migrate(cursor, '5.0.2.96.0')

        result = pol_obj.search(cursor, uid, [
            ('no_cessio_sips', '=', 'active'),
            ('no_cessio_sips_data', '=', '2019-04-30')
        ])
        self.assertItemsEqual(result, pol_ids)
