# -*- coding: utf-8 -*-
{
    "name": "GISCE CNMC SIPS v2",
    "description": """ CNMC SIPS v2 generation

  """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_administracio_publica_cnmc_distri",
        "giscedata_facturacio_distri",
        "giscedata_butlletins",
        "giscedata_tensions"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "res_partner_view.xml",
        "wizard/wizard_cnmc_sips_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
