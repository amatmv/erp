# -*- coding: utf-8 -*-
{
    "name": "GISCE Productes amb llistes d'ubicacions",
    "description": """GISCE Productes amb llistes d'ubicacions:
    - Reemplaça els tres camps indicant una posició amb un llistat
    - Afegeix el model d'ubicacions per cada producte i localització.""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "stock",
    ],
    "init_xml": [],
    "demo_xml":[],
    "update_xml":[
        "product_view.xml",
        "security/ir.model.access.csv",
    ],
    "active": False,
    "installable": True
}