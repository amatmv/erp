# -*- encoding: utf-8 -*-
from tools.translate import _
from osv import osv, fields
from osv.orm import OnlyFieldsConstraint


class ProductLocation(osv.osv):
    """Localización en Ubicaciones"""

    _name = "product.location"

    def _default_product_id(self, cursor, uid, context=None):
        return context.get('producte_id', False)

    _columns = {
        'fila': fields.char('Fila', size=100),
        'nivel': fields.char('Nivel', size=100),
        'columna': fields.char('Columna', size=100),
        'product_id': fields.many2one(
            'product.product', 'Producto',  required=True, ondelete='cascade'
        ),
        'location_id': fields.many2one(
            'stock.location', 'Ubicación', required=True),
    }

    _defaults = {
        'product_id': _default_product_id
    }


ProductLocation()


class product_product(osv.osv):
    _name = 'product.product'
    _inherit = 'product.product'

    _columns = {
        'locations_ids': fields.one2many('product.location', 'product_id',
                                         'Localización en ubicaciones'),
    }

    def _cnt_locations_ids_not_null(self, cursor, uid, ids):
        """
        Comprueba que haya alguna localización en ubicaciones en el producto.
        """
        for product in self.read(cursor, uid, ids, ['locations_ids', 'type']):
            if not product['locations_ids'] and product['type'] == 'product':
                return False
        return True

    _constraints = [
        OnlyFieldsConstraint(
            _cnt_locations_ids_not_null,
            _('El producto debe tener alguna ubicación establecida si no es un '
              'servicio.'),
            ['locations_ids', 'type']
        ),
    ]


product_product()
