# -*- coding: utf-8 -*-
{
    "name": "Signatura digital Cessión SIPS Signaturit (Comercializadora)",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_signatura_documents_signaturit",
        "giscedata_sips_signatura",
    ],
    "init_xml": [],
    "demo_xml":[
    ],
    "update_xml":[
        'wizard/wizard_partner_no_cession_sips_view.xml',
        'wizard/wizard_polissa_no_cession_sips_view.xml',
        'giscedata_polissa_view.xml',
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
