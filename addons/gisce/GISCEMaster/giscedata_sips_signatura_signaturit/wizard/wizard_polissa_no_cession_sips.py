# coding=utf-8
from osv import osv, fields
from tools.translate import _


class WizardPolissaNoCessionSIPS(osv.osv_memory):
    _name = 'wizard.polissa.no.cession.sips'

    def request(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        wizard = self.browse(cursor, uid, ids[0], context)
        pol_obj = self.pool.get('giscedata.polissa')
        sign = wizard.signature
        if sign:
            context['sign'] = True
            addr_obj = self.pool.get('res.partner.address')
            addr_id = wizard.partner_address_id.id
            email = addr_obj.read(cursor, uid, addr_id, ['email'])['email']
            context['address'] = addr_id
            if not email:
                raise osv.except_osv(
                    _('Error!'),
                    _(u"Se necesita una dirección con correo electrónico "
                      u"donde enviar el documento a firmar.")
                )
        pol_obj.request_no_cession_sips(
            cursor, uid, [wizard.polissa_id.id], context=context)
        return {'type': 'ir.actions.act_window_close'}

    def onchange_sign(self, cursor, uid, ids, sign, context=None):
        if context is None:
            context = {}
        template = {'domain': {}, 'value': {}, 'warning': {}}
        if sign:
            address_obj = self.pool.get('res.partner.address')
            pol_obj = self.pool.get('giscedata.polissa')
            pol_id = context.get('active_id', False)
            if pol_id:
                titular = pol_obj.read(cursor, uid, pol_id, ['titular'])['titular'][0]
            # Busco la direcció de notificació.
                search_params = [
                    ('type', '=', 'contact'),
                    ('partner_id', '=', titular)
                ]
                address_id = address_obj.search(
                    cursor, uid, search_params, limit=1, context=context)
                if address_id:
                    template['value'].update({'partner_address_id': address_id[0]})
                    tmp = self.onchange_partner_address(
                        cursor, uid, ids, address_id[0], context=None)['value']
                    template['value'].update(tmp)
                return template

    def onchange_partner_address(self, cursor, uid, ids, addr_id, context=None):
        if context is None:
            context = {}

        template = {'domain': {}, 'value': {}, 'warning': {}}

        addr_obj = self.pool.get('res.partner.address')
        if addr_id:
            email = addr_obj.read(cursor, uid, addr_id, ['email'])
            if email:
                template['value'].update({'email': email['email']})
        else:
            template['value'].update({'email': ''})

        return template

    def _default_partner_id(self, cursor, uid, context=None):
        if context is None:
            context = {}
        if context.get('active_id', False):
            pol_id = context['active_id']
            pol_obj = self.pool.get('giscedata.polissa')
            titular = pol_obj.read(cursor, uid, pol_id, ['titular'])['titular']
            if titular:
                return titular[0]
        return False

    def _default_polissa_id(self, cursor, uid, context=None):
        if context is None:
            context = {}
        return context.get('active_id', False)

    _columns = {
        'polissa_id': fields.many2one('giscedata.polissa', 'Póliza'),
        'signature': fields.boolean('Firma Digital'),
        'email': fields.char('E-mail', size=256, readonly=True),
        'partner_address_id': fields.many2one(
            'res.partner.address', 'Dirección a enviar'),
        'partner_id': fields.many2one('res.partner', 'Titular', required=True)
    }

    _defaults = {
        'signature': lambda *a: False,
        'partner_id': _default_partner_id,
        'polissa_id': _default_polissa_id
    }


WizardPolissaNoCessionSIPS()
