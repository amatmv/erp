# coding=utf-8
from osv import osv, fields
from tools.translate import _


class WizardPartnerNoCessionSIPS(osv.osv_memory):
    _name = 'wizard.partner.no.cession.sips'
    _inherit = 'wizard.partner.no.cession.sips'

    def request(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        wizard = self.browse(cursor, uid, ids[0], context)
        sign = wizard.signature
        partner_obj = self.pool.get('res.partner')
        if sign:
            context['sign'] = True
            addr_obj = self.pool.get('res.partner.address')
            addr_id = wizard.partner_address_id.id
            context['address'] = addr_id
            email = addr_obj.read(cursor, uid, addr_id, ['email'])['email']
            if not email:
                raise osv.except_osv(
                    _('Error!'),
                    _(u"Se necesita una dirección con correo electrónico "
                      u"donde enviar el documento a firmar.")
                )
        partner_obj.request_no_cession_sips(
            cursor, uid, [wizard.partner_id.id], context=context)
        return {'type': 'ir.actions.act_window_close'}

    def onchange_sign(self, cursor, uid, ids, sign, context=None):
        if context is None:
            context = {}
        template = {'domain': {}, 'value': {}, 'warning': {}}
        if sign:
            address_obj = self.pool.get('res.partner.address')
            partner_id = context.get('active_id', False)
            # Busco la direcció de notificació.
            search_params = [
                ('type', '=', 'contact'),
                ('partner_id', '=', partner_id)
            ]
            address_id = address_obj.search(
                cursor, uid, search_params, limit=1, context=context)
            if address_id:
                template['value'].update({'partner_address_id': address_id[0]})
                tmp = self.onchange_partner_address(
                    cursor, uid, ids, address_id[0], context=None)['value']
                template['value'].update(tmp)
            return template

    def onchange_partner_address(self, cursor, uid, ids, addr_id, context=None):
        if context is None:
            context = {}

        template = {'domain': {}, 'value': {}, 'warning': {}}

        addr_obj = self.pool.get('res.partner.address')
        if addr_id:
            email = addr_obj.read(cursor, uid, addr_id, ['email'])
            if email:
                template['value'].update({'email': email['email']})
        else:
            template['value'].update({'email': ''})

        return template

    _columns = {
        'signature': fields.boolean('Firma Digital'),
        'email': fields.char('E-mail', size=256),
        'partner_address_id': fields.many2one(
            'res.partner.address', 'Dirección a enviar'),
    }

    _defaults = {
        'signature': lambda *a: False
    }


WizardPartnerNoCessionSIPS()
