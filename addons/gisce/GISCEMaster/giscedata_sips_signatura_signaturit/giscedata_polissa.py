# -*- coding: utf-8 -*-
from __future__ import absolute_import
import json
from osv import osv
from datetime import datetime


class GiscedataPolissa(osv.osv):
    _name = "giscedata.polissa"
    _inherit = 'giscedata.polissa'

    def request_no_cession_sips(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        cas_ids = super(GiscedataPolissa, self).request_no_cession_sips(
            cursor, uid, ids, context=context
        )
        if context.get('sign', False):
            addr_id = context['address']
            self.start_sips_cession(
                cursor, uid, ids, cas_ids, addr_id, context=context)
        return cas_ids

    def start_sips_cession(self, cursor, uid, pol_ids, cas_ids, addr_id, context=None):
        if not context:
            context = {}
        super(GiscedataPolissa, self).start_sips_cession(
            cursor, uid, pol_ids, cas_ids, addr_id, context=context
        )
        if not isinstance(cas_ids, (list, tuple)):
            cas_ids = [cas_ids]
        if not isinstance(pol_ids, (list, tuple)):
            pol_ids = [pol_ids]

        conf_obj = self.pool.get('res.config')
        imd_obj = self.pool.get('ir.model.data')
        attach_obj = self.pool.get('ir.attachment')
        add_obj = self.pool.get('res.partner.address')
        pro_obj = self.pool.get('giscedata.signatura.process')

        max_signable_documents = int(conf_obj.get(
            cursor, uid, 'signature_signaturit_max_signable_documents', '2'))
        sips_report_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'report_peticion_privacidad'
        )[1]

        partner = self.read(cursor, uid, pol_ids, ['titular'])[0]['titular']
        partner_name = partner[1]
        data = json.dumps({
            'callback_method': 'enviar_sips',
            'cas_ids': cas_ids
        })
        subject = 'Firma de petición de privacidad de ' + partner_name
        email = add_obj.read(cursor, uid, addr_id, ['email'])['email']
        if len(cas_ids) > 1:
            subject = subject + ' para multiples contratos'
        sips_categ = attach_obj.get_category_for(
            cursor, uid, 'sips', context=context)

        # Here we divide the pol_ids in chunks because each process has a max
        # of elements
        while pol_ids:
            pol_process = pol_ids[:max_signable_documents]
            pol_ids = pol_ids[max_signable_documents:]
            files = []
            for pol_id in pol_process:
                doc_file = (0, 0, {
                    'model': 'giscedata.polissa,{}'.format(pol_id),
                    'report_id': sips_report_id,
                    'category_id': sips_categ,
                })
                files.append(doc_file)

            recipients = [
                (0, 0, {
                    'partner_address_id': addr_id,
                    'name': partner_name,
                    'email': email
                })
            ]
            values = {
                'subject': subject,
                'delivery_type': 'email',
                'recipients': recipients,
                'reminders': 0,
                'type': 'advanced',
                'data': data,
                'all_signed': False,
                'files': files
            }

            process_id = pro_obj.create(cursor, uid, values, context=context)

            # Executar l'inici del proces
            pro_obj.start(cursor, uid, [process_id], context=context)

        return True


GiscedataPolissa()
