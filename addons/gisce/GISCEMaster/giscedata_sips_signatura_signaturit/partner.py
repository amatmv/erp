# coding=utf-8
from osv import osv


class ResPartner(osv.osv):
    _name = 'res.partner'
    _inherit = 'res.partner'

    def request_no_cession_sips(self, cursor, uid, ids, who='titular', context=None):
        if context is None:
            context = {}
        pol_obj = self.pool.get('giscedata.polissa')
        owned_contracts = self.search_unactive_sips(
            cursor, uid, ids, who, context=context
        )
        cas_ids = pol_obj.request_no_cession_sips(
            cursor, uid, owned_contracts, context=context
        )
        return cas_ids


ResPartner()
