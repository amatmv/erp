# -*- coding: utf-8 -*-
{
    "name": "SIPS",
    "description": """
        Mòdul per el Sistema de Informació de Punts de Suministres
        """,
    "version": "0-dev",
    "author": "Gisce",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "mongodb_backend",
        "oorq",
        "giscedata_cnmc_sips_comer"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_sips_view.xml",
        "security/giscedata_sips_security.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
