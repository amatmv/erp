# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* giscedata_sips
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.14\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2019-02-26 13:08\n"
"PO-Revision-Date: 2019-02-26 13:08\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: giscedata_sips
#: model:ir.actions.act_window,name:giscedata_sips.act_errors_per_fitxer
#: view:log.fitxer.error:0
#: field:log.fitxer.error,errors:0
msgid "Errors"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,pot_cont_p4:0
msgid "Potencia Contratada Periode 4 (kW)"
msgstr ""

#. module: giscedata_sips
#: view:giscedata.sips.consums:0
#: view:giscedata.sips.ps:0
msgid "Potencia"
msgstr ""

#. module: giscedata_sips
#: constraint:ir.actions.act_window:0
msgid "Invalid model name in the action definition."
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,codi_postal:0
msgid "Codi Postal Suministre"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,exportat:0
msgid "Exportat"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.consums,potencia_3:0
msgid "Potencia Activa 3"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.consums,potencia_4:0
msgid "Potencia Activa 4"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.consums,potencia_5:0
msgid "Potencia Activa 5"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.consums,potencia_6:0
msgid "Potencia Activa 6"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.consums,potencia_7:0
msgid "Potencia Activa 7"
msgstr ""

#. module: giscedata_sips
#: field:log.fitxer,t_final:0
msgid "Temps final de la importació"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,propietat_equip_mesura:0
msgid "Propietat Equip de Mesura"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,tensio:0
msgid "Tensió"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,max_maximetre_p1:0
msgid "Màxim del maximetre P1 (kW)"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,direccio:0
msgid "Direcció Suministre"
msgstr ""

#. module: giscedata_sips
#: model:ir.ui.menu,name:giscedata_sips.menu_importacions
msgid "Importacions"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,consum_any_p5:0
msgid "Consum últim any Periode 5 (W)"
msgstr ""

#. module: giscedata_sips
#: view:log.fitxer:0
msgid "Estat de la importacio"
msgstr ""

#. module: giscedata_sips
#: model:ir.model,name:giscedata_sips.model_giscedata_sips_importer
msgid "giscedata.sips.importer"
msgstr ""

#. module: giscedata_sips
#: view:giscedata.sips.consums:0
#: view:giscedata.sips.ps:0
#: model:ir.actions.act_window,name:giscedata_sips.action_giscedata_sips_ps
#: model:ir.module.module,shortdesc:giscedata_sips.module_meta_information
#: model:ir.ui.menu,name:giscedata_sips.menu_giscedata_sips_sips
msgid "SIPS"
msgstr ""

#. module: giscedata_sips
#: selection:giscedata.sips.ps,persona_fj:0
msgid "Física"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,poblacio:0
msgid "Població Suministre"
msgstr ""

#. module: giscedata_sips
#: model:ir.ui.menu,name:giscedata_sips.menu_log_fitxer_error_tree
msgid "Errors en la importació"
msgstr ""

#. module: giscedata_sips
#: field:log.fitxer,t_inici:0
msgid "Temps inici de la importació"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,max_maximetre_p4:0
msgid "Màxim del maximetre P4 (kW)"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,data_ulti_mov:0
msgid "Data Últim Moviment"
msgstr ""

#. module: giscedata_sips
#: view:giscedata.sips.ps:0
msgid "Màxims Maximetres"
msgstr ""

#. module: giscedata_sips
#: model:ir.ui.menu,name:giscedata_sips.menu_giscedata_sips_consum
msgid "Consum"
msgstr ""

#. module: giscedata_sips
#: field:log.fitxer,message:0
msgid "Missatges d'event"
msgstr ""

#. module: giscedata_sips
#: model:ir.ui.menu,name:giscedata_sips.menu_log_fitxers
msgid "Totes les importacions"
msgstr ""

#. module: giscedata_sips
#: model:ir.model,name:giscedata_sips.model_giscedata_sips_ps
msgid "giscedata.sips.ps"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,consum_any_p4:0
msgid "Consum últim any Periode 4 (W)"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,pot_cont_p6:0
msgid "Potencia Contratada Periode 6 (kW)"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.consums,activa_6:0
msgid "Consum Activa 6"
msgstr ""

#. module: giscedata_sips
#: view:log.fitxer.error:0
msgid "Errors de la importacio"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.consums,activa_4:0
msgid "Consum Activa 4"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.consums,activa_5:0
msgid "Consum Activa 5"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.consums,activa_2:0
msgid "Consum Activa 2"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.consums,activa_3:0
msgid "Consum Activa 3"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.consums,activa_1:0
msgid "Consum Activa 1"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.consums,tipo_a5:0
msgid "Tipo de DH A5"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.consums,tipo_a4:0
msgid "Tipo de DH A4"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.consums,tipo_a7:0
msgid "Tipo de DH A7"
msgstr ""

#. module: giscedata_sips
#: view:giscedata.sips.consums:0
#: field:giscedata.sips.consums,name:0
#: view:giscedata.sips.ps:0
#: field:giscedata.sips.ps,name:0
msgid "CUPS"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.consums,tipo_a1:0
msgid "Tipo de DH A1"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.consums,tipo_a3:0
msgid "Tipo de DH A3"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.consums,tipo_a2:0
msgid "Tipo de DH A2"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,fianza:0
msgid "Fiança"
msgstr ""

#. module: giscedata_sips
#: field:log.fitxer.error,log_fitxer:0
msgid "Id log fitxer"
msgstr ""

#. module: giscedata_sips
#: model:ir.model,name:giscedata_sips.model_log_fitxer_error
msgid "log.fitxer.error"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,distri:0
msgid "Distribuidora"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,data_ult_canv:0
msgid "Data Últim Canvi Comercialitzador"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,max_maximetre_p2:0
msgid "Màxim del maximetre P2 (kW)"
msgstr ""

#. module: giscedata_sips
#: selection:giscedata.sips.ps,propietat_equip_mesura:0
#: selection:giscedata.sips.ps,propietat_icp:0
msgid "Empresa Distribuidora"
msgstr ""

#. module: giscedata_sips
#: selection:giscedata.sips.ps,indicatiu_icp:0
msgid "ICP instalado"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,des_tarifa:0
#: field:giscedata.sips.ps,tarifa:0
msgid "Tarifa"
msgstr ""

#. module: giscedata_sips
#: constraint:ir.ui.view:0
msgid "Invalid XML for View Architecture!"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.consums,data_final:0
msgid "Data final"
msgstr ""

#. module: giscedata_sips
#: field:log.fitxer,rate_error:0
msgid "Rate of errors"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,consum_any_p1:0
msgid "Consum últim any Periode 1 (W)"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,pot_cont_p3:0
msgid "Potencia Contratada Periode 3 (kW)"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,pot_cont_p1:0
msgid "Potencia Contratada Periode 1 (kW)"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,der_extensio:0
msgid "Dret Extensió (kW)"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,consum_any_p2:0
msgid "Consum últim any Periode 2 (W)"
msgstr ""

#. module: giscedata_sips
#: field:log.fitxer,progres:0
msgid "Progres"
msgstr ""

#. module: giscedata_sips
#: model:ir.actions.act_window,name:giscedata_sips.act_consums_per_sips
#: model:ir.actions.act_window,name:giscedata_sips.action_giscedata_sips_consums
msgid "Consums"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,cognom:0
msgid "Cognoms Titular"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.consums,potencia_1:0
msgid "Potencia Activa 1"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.consums,reactiva_4:0
msgid "Consum Reactiva 4"
msgstr ""

#. module: giscedata_sips
#: field:log.fitxer,number_of_lines:0
msgid "Number of lines"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,pot_max_puesta:0
msgid "Potència Màxima Acta"
msgstr ""

#. module: giscedata_sips
#: model:ir.actions.act_window,name:giscedata_sips.action_active_log_fitxers
#: model:ir.actions.act_window,name:giscedata_sips.action_log_fitxers
#: view:log.fitxer:0
#: view:log.fitxer.error:0
msgid "Log"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,active:0
msgid "Actiu"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,perfil_consum:0
msgid "Perfil Consum"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,indicatiu_icp:0
msgid "Control de Potència"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,pot_max_bie:0
msgid "Potència Màxima BIE"
msgstr ""

#. module: giscedata_sips
#: selection:giscedata.sips.ps,propietat_equip_mesura:0
#: selection:giscedata.sips.ps,propietat_icp:0
msgid "Titular del punt de suministre"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,direccio_titular:0
msgid "Direcció Titular"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,persona_fj:0
msgid "Tipus Persona"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,primera_vivienda:0
msgid "Informació Punt Suministre"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.consums,tipo_p6:0
msgid "Tipo de DH P6"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.consums,tipo_p7:0
msgid "Tipo de DH P7"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.consums,tipo_p4:0
msgid "Tipo de DH P4"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.consums,tipo_p5:0
msgid "Tipo de DH P5"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.consums,tipo_p2:0
msgid "Tipo de DH P2"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.consums,tipo_p3:0
msgid "Tipo de DH P3"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.consums,tipo_p1:0
msgid "Tipo de DH P1"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.consums,real_estimada:0
msgid "Real/Estimada"
msgstr ""

#. module: giscedata_sips
#: view:giscedata.sips.consums:0
msgid "Activa"
msgstr ""

#. module: giscedata_sips
#: selection:giscedata.sips.ps,indicatiu_icp:0
msgid "ICP no instalado"
msgstr ""

#. module: giscedata_sips
#: field:log.fitxer,number_of_errors:0
msgid "Number of errors"
msgstr ""

#. module: giscedata_sips
#: field:log.fitxer,name:0
#: field:log.fitxer.error,name:0
msgid "Arxiu"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,data_alta:0
msgid "Data Alta Suministre"
msgstr ""

#. module: giscedata_sips
#: selection:giscedata.sips.ps,primera_vivienda:0
msgid "Vivienda habitual"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,consum_any_p3:0
msgid "Consum últim any Periode 3 (W)"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,pot_cont_p5:0
msgid "Potencia Contratada Periode 5 (kW)"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.consums,activa_7:0
msgid "Consum Activa 7"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,codi_sifco:0
msgid "Codi Sifco"
msgstr ""

#. module: giscedata_sips
#: field:log.fitxer,estat:0
msgid "Estat"
msgstr ""

#. module: giscedata_sips
#: view:giscedata.sips.ps:0
msgid "General"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,cod_distri:0
msgid "Codi Distribuidora"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.consums,reactiva_3:0
msgid "Consum Reactiva 3"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.consums,reactiva_2:0
msgid "Consum Reactiva 2"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.consums,reactiva_1:0
msgid "Consum Reactiva 1"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.consums,reactiva_7:0
msgid "Consum Reactiva 7"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.consums,reactiva_6:0
msgid "Consum Reactiva 6"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.consums,reactiva_5:0
msgid "Consum Reactiva 5"
msgstr ""

#. module: giscedata_sips
#: model:ir.ui.menu,name:giscedata_sips.menu_active_log_fitxers
msgid "Importacions actives"
msgstr ""

#. module: giscedata_sips
#: constraint:ir.model:0
msgid "The Object name must start with x_ and not contain any special character !"
msgstr ""

#. module: giscedata_sips
#: selection:giscedata.sips.ps,primera_vivienda:0
msgid "No vivienda habitual"
msgstr ""

#. module: giscedata_sips
#: view:giscedata.sips.ps:0
msgid "Titular"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,data_lim_exten:0
msgid "Data Límit Drets Extensió"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,max_maximetre_p6:0
msgid "Màxim del maximetre P6 (kW)"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,nom:0
msgid "Nom Titular"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,der_acces_llano:0
msgid "Dret Accés Llano (kW)"
msgstr ""

#. module: giscedata_sips
#: view:giscedata.sips.consums:0
msgid "Activa 2"
msgstr ""

#. module: giscedata_sips
#: view:giscedata.sips.consums:0
msgid "Activa 3"
msgstr ""

#. module: giscedata_sips
#: view:giscedata.sips.consums:0
msgid "Activa 1"
msgstr ""

#. module: giscedata_sips
#: view:giscedata.sips.consums:0
msgid "Activa 6"
msgstr ""

#. module: giscedata_sips
#: view:giscedata.sips.consums:0
msgid "Reactiva"
msgstr ""

#. module: giscedata_sips
#: view:giscedata.sips.consums:0
msgid "Activa 4"
msgstr ""

#. module: giscedata_sips
#: view:giscedata.sips.consums:0
msgid "Activa 5"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,provincia:0
msgid "Provincia Suministre"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.consums,tipo_r4:0
msgid "Tipo de DH R4"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.consums,tipo_r5:0
msgid "Tipo de DH R5"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.consums,tipo_r6:0
msgid "Tipo de DH R6"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.consums,tipo_a6:0
msgid "Tipo de DH A6"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.consums,tipo_r1:0
msgid "Tipo de DH R1"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.consums,tipo_r2:0
msgid "Tipo de DH R2"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.consums,tipo_r3:0
msgid "Tipo de DH R3"
msgstr ""

#. module: giscedata_sips
#: model:ir.actions.act_window,name:giscedata_sips.action_log_fitxer_error_tree
msgid "Log Errors"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,pot_cont_p2:0
msgid "Potencia Contratada Periode 2 (kW)"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,tipo_pm:0
msgid "Tipus PM"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,consum_any_p6:0
msgid "Consum últim any Periode 6 (W)"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,max_maximetre_p7:0
msgid "Màxim del maximetre P7 (kW)"
msgstr ""

#. module: giscedata_sips
#: selection:giscedata.sips.ps,persona_fj:0
msgid "Jurídica"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.consums,potencia_2:0
msgid "PotenciaActiva 2"
msgstr ""

#. module: giscedata_sips
#: field:log.fitxer,elapsed:0
msgid "Temps transcorregut"
msgstr ""

#. module: giscedata_sips
#: model:ir.model,name:giscedata_sips.model_giscedata_sips_consums
msgid "giscedata.sips.consums"
msgstr ""

#. module: giscedata_sips
#: model:ir.model,name:giscedata_sips.model_log_fitxer
msgid "log.fitxer"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,max_maximetre_p3:0
msgid "Màxim del maximetre P3 (kW)"
msgstr ""

#. module: giscedata_sips
#: field:log.fitxer,speed:0
msgid "Velocitat d'inserció"
msgstr ""

#. module: giscedata_sips
#: view:giscedata.sips.ps:0
msgid "Consum últim any"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,der_acces_valle:0
msgid "Dret Accés Valle (kW)"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.consums,tipo_r7:0
msgid "Tipo de DH R7"
msgstr ""

#. module: giscedata_sips
#: model:ir.ui.menu,name:giscedata_sips.menu_giscedata_sips
msgid "SIPS (2015)"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,consum_any_p7:0
msgid "Consum últim any Periode 7 (W)"
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,max_maximetre_p5:0
msgid "Màxim del maximetre P5 (kW)"
msgstr ""

#. module: giscedata_sips
#: field:log.fitxer.error,line_number:0
msgid "Line number"
msgstr ""

#. module: giscedata_sips
#: view:giscedata.sips.consums:0
msgid "CONSUMS"
msgstr ""

#. module: giscedata_sips
#: model:ir.module.module,description:giscedata_sips.module_meta_information
msgid "\n"
"        Mòdul per el Sistema de Informació de Punts de Suministres\n"
"        "
msgstr ""

#. module: giscedata_sips
#: field:giscedata.sips.ps,propietat_icp:0
msgid "Propietat ICP"
msgstr ""

