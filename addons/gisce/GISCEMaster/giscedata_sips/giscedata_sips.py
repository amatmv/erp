# -*- coding: utf-8 -*-

from datetime import datetime
import operator
import os
import shutil
import zipfile
import json

from osv import osv, fields
from tools import config, cache
from mongodb_backend import osv_mongodb
from oorq.decorators import job
from fuzzywuzzy import process
from slugify import slugify
from sippers import logger
from sippers.file import SipsFile, PackedSipsFile
from sippers.backends import get_backend
from sippers.exceptions import *
from lockfile import LockFile


TARIFES_INDEX = {}
TENSIONS_INDEX = {}


@cache()
def get_distri(obj, cursor, uid, distri, code, context=None):
    """Obté una distribuidora segons el codi.
    """
    partner_obj = obj.pool.get('res.partner')
    partner = partner_obj.search(cursor, uid, [
        ('ref', '=', code)
    ], limit=1, context={'active_test': True})
    if partner:
        return partner[0]
    else:
        return partner_obj.create(cursor, uid, {
            'name': distri,
            'ref': code,
            'supplier': 1
        })

@cache()
def get_provincia(obj, cursor, uid, provincia, context=None):
    prov_obj = obj.pool.get('res.country.state')
    provincies_id = prov_obj.search(cursor, uid, [])
    provincies = prov_obj.read(cursor, uid, provincies_id, ['name'])
    provincies_slug = {slugify(x['name']): x['id'] for x in provincies}
    choices = provincies_slug.keys()
    res = process.extractOne(slugify(provincia), choices)
    if res and res[1] >= 90:
        return provincies_slug[res[0]]
    else:
        return False


@cache()
def get_poblacio(obj, cursor, uid, id_provincia, poblacio, context=None):
    poblacio_obj = obj.pool.get('res.poblacio')
    poblacions_id = poblacio_obj.search(cursor, uid, [
        ('municipi_id.state', '=', id_provincia)
    ])
    poblacions = poblacio_obj.read(cursor, uid, poblacions_id, ['name'])
    poblacions_slug = {slugify(x['name']): x['id'] for x in poblacions}
    choices = poblacions_slug.keys()
    res = process.extractOne(slugify(poblacio), choices)
    if res and res[1] >= 90:
        return poblacions_slug[res[0]]
    else:
        return False

@cache()
def get_pobl_from_name(obj, cursor, uid, poblacio, context=None):
    pobl_obj = obj.pool.get('res.poblacio')
    pobls_ids = pobl_obj.search(cursor, uid, [])
    poblacions = pobl_obj.read(cursor, uid, pobls_ids , ['name'])
    poblacions_slug = {slugify(x['name']): x['id'] for x in poblacions}
    choices = poblacions_slug.keys()
    res = process.extractOne(slugify(poblacio), choices)
    if res:
        return poblacions_slug[res[0]]
    else:
        return False

@cache()
def get_muni_from_pobl(obj, cursor, uid, poblacio_id, context=None):
    pobl_obj = obj.pool.get('res.poblacio')
    pobl_municipi = pobl_obj.read(cursor, uid, poblacio_id, ['municipi_id'])
    if 'municipi_id' in pobl_municipi:
        municipi_id = pobl_municipi['municipi_id'][0]
        return municipi_id
    else:
        return False

@cache()
def get_prov_from_muni(obj, cursor, uid, muni_id, context=None):
    muni_obj = obj.pool.get('res.municipi')
    municipi_provincia = muni_obj.read(cursor, uid, muni_id, ['state'])
    if 'state' in municipi_provincia:
        prov_id = municipi_provincia['state'][0]
        return prov_id
    else:
        return False

@cache()
def get_municipi(obj, cursor, uid, id_provincia, municipi, context=None):
    muni_obj = obj.pool.get('res.municipi')
    munis_id = muni_obj.search(cursor, uid, [
        ('state', '=', id_provincia)
    ])
    municipis = muni_obj.read(cursor, uid, munis_id, ['name'])
    municipis_slug = {slugify(x['name']): x['id'] for x in municipis}
    choices = municipis_slug.keys()
    res = process.extractOne(slugify(municipi), choices)
    if res and res[1] >= 90:
        return municipis_slug[res[0]]
    else:
        return muni_obj.search(cursor, uid, [
            ('ine', '=', '99999')
        ])[0]


@cache()
def get_tarifa(obj, cursor, uid, tarifa_code, context=None):

    def clean(code):
        return ''.join([c.upper() for c in code if c.isalnum()])

    if not TARIFES_INDEX:
        tarifa_obj = obj.pool.get('giscedata.polissa.tarifa')
        tarifes_ids = tarifa_obj.search(cursor, uid, [])
        for tarifa in tarifa_obj.read(cursor, uid, tarifes_ids):
            key = clean(tarifa['name'])
            TARIFES_INDEX[key] = tarifa['id']
    tarifa = clean(tarifa_code)
    return TARIFES_INDEX.get(tarifa, False)


@cache()
def get_tensio_norm(obj, cursor, uid, tensio, context=None):
    tensio_obj = obj.pool.get('giscedata.tensions.tensio')
    if not TENSIONS_INDEX:
        tensions_ids = tensio_obj.search(cursor, uid, [])
        for t in tensio_obj.read(cursor, uid, tensions_ids, ['name']):
            TENSIONS_INDEX[t['name']] = t['id']

    tensio_match = process.extractOne(tensio, TENSIONS_INDEX.keys()) or (0, 0)

    # Gestionem les alta tensió
    try:
        tensio_int = int(tensio)
        if tensio_match[1] < 90 and tensio_int >= 10000:
            tid = tensio_obj.create(cursor, uid, {
                'name': tensio,
                'tensio': tensio_int,
                'l_inferior': tensio_int,
                'l_superior': tensio_int,
                'tipus': 'AT'
            })
            TENSIONS_INDEX[tensio] = tid
            return tid
    except ValueError:
        pass

    return TENSIONS_INDEX.get(tensio_match[0], False)

def get_uri():
    uri = 'erpmongodb://'
    if config.get('mongodb_user', False):
        uri += '{user}:{pass}@'
    uri += '{host}:{port}/{name}'
    uri_params = {}
    for p in ('user', 'pass', 'host', 'port', 'name'):
        k = 'mongodb_{}'.format(p)
        uri_params[p] = config.get(k, '')
    return uri.format(**uri_params)


class GiscedataSipsImporter(osv.osv_memory):
    _name = 'giscedata.sips.importer'

    def process_dir(self, cursor, uid, path):
        logger.info("Searching for SIPS fils in %s", path)
        pj = os.path.join
        is_f = os.path.isfile
        processed_path = pj(path, "done")
        if not os.path.exists(processed_path):
            os.mkdir(processed_path)

        files = [pj(path, f) for f in os.listdir(path) if is_f(pj(path, f))]
        logger.info("found %s files: %s", len(files), files)
        for sips in files:
            logger.info("Importing %s...", sips)
            try:
                shutil.move(sips, processed_path)
                new_sips = pj(processed_path, os.path.basename(sips))
                self.import_file(cursor, uid, new_sips)
            except ParserNotFoundException:
                logger.info("Parser not found for %s", sips)
        return files

    @job(queue='sips', timeout=3600*24)
    def import_file(self, cursor, uid, path):
        uri_backend = get_uri()
        backend = get_backend(uri_backend)
        log_obj = self.pool.get('log.fitxer')
        log_error_obj = self.pool.get('log.fitxer.error')
        lock = LockFile(path)
        if lock.is_locked():
            return False
        with lock:
            with backend(uri_backend) as bnd:
                if zipfile.is_zipfile(path):
                    logger.info('Packed sips file detected')
                    with PackedSipsFile(path) as psf:
                        zstats = psf.stats
                        for sips_file in psf:
                            stats = sips_file.stats
                            n_files = zstats.progress.split()[1]
                            file_name = '{}>{}'.format(psf.path, sips_file.path)
                            log_sips = log_obj.create(cursor, uid, {
                                'name': file_name,
                                'estat': 'INICI {}'.format(n_files),
                                'progres': int(stats.progress.split('%')[0]),
                                't_inici': stats.start.strftime('%Y-%m-%d %H:%M:%S'),
                                'elapsed': stats.elapsed_time,
                                'speed': stats.speed
                            })
                            for line in sips_file:
                                if not line:
                                    continue
                                bnd.insert(line)
                                log_obj.write(cursor, uid, [log_sips], {
                                    'progres': int(stats.progress.split('%')[0]),
                                    'elapsed': stats.elapsed_time,
                                    'speed': stats.speed,
                                    'number_of_errors': stats.number_of_errors,
                                    'rate_error': stats.rate_error
                                })
                            n_files = zstats.progress.split()[1]
                            log_obj.write(cursor, uid, [log_sips], {
                                'progres': int(stats.progress.split('%')[0]),
                                'elapsed': stats.elapsed_time,
                                'speed': stats.speed,
                                'estat': 'FINAL {}'.format(n_files),
                                'number_of_errors': stats.number_of_errors,
                                'rate_error': stats.rate_error,
                                'number_of_lines': stats.line_number
                            })
                            for line_number, errors in stats.errors:
                                log_error_obj.create(cursor, uid, {
                                    'log_fitxer': log_sips,
                                    'name': file_name,
                                    'line_number': line_number,
                                    'errors': json.dumps(errors)
                                })

                else:
                    with SipsFile(path) as sips_file:
                        stats = sips_file.stats
                        file_name = sips_file.path
                        log_sips = log_obj.create(cursor, uid, {
                            'name': file_name,
                            'estat': 'INICI',
                            'progres': int(stats.progress.split('%')[0]),
                            't_inici': stats.start.strftime('%Y-%m-%d %H:%M:%S'),
                            'elapsed': stats.elapsed_time,
                            'speed': stats.speed
                        })
                        for line in sips_file:
                            if not line:
                                continue
                            bnd.insert(line)
                            log_obj.write(cursor, uid, [log_sips], {
                                'progres': int(stats.progress.split('%')[0]),
                                'elapsed': stats.elapsed_time,
                                'speed': stats.speed,
                                'number_of_errors': stats.number_of_errors,
                                'rate_error': stats.rate_error,
                                'estat': 'FINAL',
                            })
                        log_obj.write(cursor, uid, [log_sips], {
                            'state': 'FINAL',
                            'progres': int(stats.progress.split('%')[0]),
                            'elapsed': stats.elapsed_time,
                            'speed': stats.speed,
                            'number_of_errors': stats.number_of_errors,
                            'rate_error': stats.rate_error,
                            'number_of_lines': stats.line_number
                        })
                        for line_number, errors in stats.errors:
                            log_error_obj.create(cursor, uid, {
                                'log_fitxer': log_sips,
                                'name': file_name,
                                'line_number': line_number,
                                'errors': json.dumps(errors)
                            })
        return True

GiscedataSipsImporter()


class GiscedataSipsPs(osv_mongodb.osv_mongodb):
    _name = 'giscedata.sips.ps'

    def calcul_maximetre_max(self, cursor, uid, ids, context=None):
        consum_obj = self.pool.get('giscedata.sips.consums')

        for cups in self.read(cursor, uid, ids, ['name', 'tarifa']):
            # TODO: Ajuntar amb la tarifa i fer continue per els dos casos
            if not cups:
                print "No s'ha trobat aquest sips"
                continue
            # desestimem les tarifes inferiors a 3.0
            if cups['tarifa'] < '3':
                print "tarifa inferior a 3"
                continue

            search_params = [('name', '=', cups['name'].encode('ascii',
                                                               'ignore'))]
            # buscar els consums per el cups i l'any
            consums_ids = consum_obj.search(cursor, uid, search_params)
            if consums_ids:
                vals_p1 = []
                vals_p2 = []
                vals_p3 = []
                vals_p4 = []
                vals_p5 = []
                vals_p6 = []
                vals_p7 = []
                setconsums = False

                for con_id in consums_ids:
                    llista_cons = consum_obj.read(cursor, uid, int(con_id),
                                                  ['potencia_1',
                                                   'potencia_2',
                                                   'potencia_3',
                                                   'potencia_4',
                                                   'potencia_5',
                                                   'potencia_6',
                                                   'potencia_7'])
                    llista_cons.pop("id", None)

                    vals_p1.append(llista_cons['potencia_1'])
                    vals_p2.append(llista_cons['potencia_2'])
                    vals_p3.append(llista_cons['potencia_3'])
                    vals_p4.append(llista_cons['potencia_4'])
                    vals_p5.append(llista_cons['potencia_5'])
                    vals_p6.append(llista_cons['potencia_6'])
                    if 'activa_7' in llista_cons:
                        vals_p7.append(llista_cons['potencia_7'])
                        setconsums = True

                try:
                    val_maxim_p1 = max(vals_p1)
                    val_maxim_p2 = max(vals_p2)
                    val_maxim_p3 = max(vals_p3)
                    val_maxim_p4 = max(vals_p4)
                    val_maxim_p5 = max(vals_p5)
                    val_maxim_p6 = max(vals_p6)

                    if setconsums:
                        val_maxim_p7 = max(vals_p7)
                        self.write(cursor, uid, [cups['id']], {
                            'max_maximetre_p1': val_maxim_p1,
                            'max_maximetre_p2': val_maxim_p2,
                            'max_maximetre_p3': val_maxim_p3,
                            'max_maximetre_p4': val_maxim_p4,
                            'max_maximetre_p5': val_maxim_p5,
                            'max_maximetre_p6': val_maxim_p6,
                            'max_maximetre_p7': val_maxim_p7})
                    else:
                        self.write(cursor, uid, [cups['id']], {
                            'max_maximetre_p1': val_maxim_p1,
                            'max_maximetre_p2': val_maxim_p2,
                            'max_maximetre_p3': val_maxim_p3,
                            'max_maximetre_p4': val_maxim_p4,
                            'max_maximetre_p5': val_maxim_p5,
                            'max_maximetre_p6': val_maxim_p6})

                except Exception as e:
                    print "Error: {}".format(e.message)

        return True


    def calcul_consums_any(self, cursor, uid, ids, any_con=None, context=None):
        consum_obj = self.pool.get('giscedata.sips.consums')

        #Agafar any actual si no cridem cap any
        if not any_con:
            any_con = datetime.today().year - 1

        data_inici = "%s-01-01" % any_con
        data_final = "%s-12-31" % any_con

        for cups in self.read(cursor, uid, ids, ['name']):
            if not cups:
                print "No s'ha trobat aquest sips"
                continue

            search_params = [('data_final', '>=', data_inici),
                             ('data_final', '<=', data_final),
                             ('name', '=', cups['name'].encode(
                                 'ascii', 'ignore'))]
            # buscar els consums per el cups i l'any
            consums_ids = consum_obj.search(cursor, uid, search_params)
            if consums_ids:
                # consums de l'activa inicialitzats a 0
                activa_1 = 0.0
                activa_2 = 0.0
                activa_3 = 0.0
                activa_4 = 0.0
                activa_5 = 0.0
                activa_6 = 0.0
                activa_7 = 0.0
                setconsums = False

                for con_id in consums_ids:
                    llista_cons = consum_obj.read(cursor, uid, int(con_id),
                                                  ['activa_1',
                                                   'activa_2',
                                                   'activa_3',
                                                   'activa_4',
                                                   'activa_5',
                                                   'activa_6',
                                                   'activa_7'])

                    activa_1 += float(llista_cons['activa_1'])
                    activa_2 += float(llista_cons['activa_2'])
                    activa_3 += float(llista_cons['activa_3'])
                    activa_4 += float(llista_cons['activa_4'])
                    activa_5 += float(llista_cons['activa_5'])
                    activa_6 += float(llista_cons['activa_6'])
                    if 'activa_7' in llista_cons:
                        activa_7 += float(llista_cons['activa_7'])
                        setconsums = True
                try:
                    if setconsums:
                        self.write(cursor, uid, [cups['id']], {
                            'consum_any_p1': activa_1,
                            'consum_any_p2': activa_2,
                            'consum_any_p3': activa_3,
                            'consum_any_p4': activa_4,
                            'consum_any_p5': activa_5,
                            'consum_any_p6': activa_6,
                            'consum_any_p7': activa_7})
                    else:
                        self.write(cursor, uid, [cups['id']], {
                            'consum_any_p1': activa_1,
                            'consum_any_p2': activa_2,
                            'consum_any_p3': activa_3,
                            'consum_any_p4': activa_4,
                            'consum_any_p5': activa_5,
                            'consum_any_p6': activa_6})

                except Exception as e:
                    print "Error: {}".format(e.message)

        return True

    _columns = {
        'name': fields.char(
            'CUPS', size=22, required=True, exact_match=True, select=True
        ),  # a
        'distri': fields.char('Distribuidora', size=30),  # b
        'cod_distri': fields.char(
            'Codi Distribuidora', size=4, exact_match=True, select=True
        ),  # b
        'codi_sifco': fields.char('Codi Sifco', size=3),
        'poblacio': fields.char('Població Suministre', size=30),
        'direccio': fields.char('Direcció Suministre', size=80),
        'codi_postal': fields.char(
            'Codi Postal Suministre', size=5, exact_match=True, select=True
        ),
        'provincia': fields.char(
            'Provincia Suministre', size=10, exact_match=True, select=True
        ),
        'data_alta': fields.date('Data Alta Suministre'),
        'nom': fields.char('Nom Titular', size=15),
        'cognom': fields.char('Cognoms Titular', size=40),
        'direccio_titular': fields.char('Direcció Titular', size=80),  # aa
        # potencia maxima autoritzada (...) de puesta en marcha de inst. de alta
        'pot_max_puesta': fields.float('Potència Màxima Acta', size=12),  # kW
        # potencia maxima autorizada por boletin de instalador autorizado
        'pot_max_bie': fields.float('Potència Màxima BIE', size=12),  # kW
        'tarifa': fields.char(
            'Tarifa', size=5, exact_match=True, select=True
        ),
        'des_tarifa': fields.char('Tarifa', size=30),
        'tensio': fields.char('Tensió', size=9),  # En Volts
        'tipo_pm': fields.char('Tipus PM', size=8),
        'indicatiu_icp': fields.selection([('0', 'ICP no instalado'),
                                           ('1', 'ICP instalado')],
                                          'Control de Potència'),
        'perfil_consum': fields.char('Perfil Consum', size=2),  # selection # m
        'der_extensio': fields.float('Dret Extensió (kW)', size=12),
        'der_acces_llano': fields.float('Dret Accés Llano (kW)', size=12),
        'der_acces_valle': fields.float('Dret Accés Valle (kW)', size=12),
        'propietat_equip_mesura': fields.selection([
                                    ('0', 'Empresa Distribuidora'),
                                    ('1', 'Titular del punt de suministre')],
                                                   'Propietat Equip de Mesura'),
        'propietat_icp': fields.selection([('0',
                                            'Empresa Distribuidora'),
                                           ('1',
                                            'Titular del punt de suministre')
                                           ], 'Propietat ICP'),
        'pot_cont_p1': fields.float('Potencia Contratada Periode 1 (kW)',
                                    size=12),  # q
        'pot_cont_p2': fields.float('Potencia Contratada Periode 2 (kW)',
                                    size=12),  # q
        'pot_cont_p3': fields.float('Potencia Contratada Periode 3 (kW)',
                                    size=12),  # q
        'pot_cont_p4': fields.float('Potencia Contratada Periode 4 (kW)',
                                    size=12),  # q
        'pot_cont_p5': fields.float('Potencia Contratada Periode 5 (kW)',
                                    size=12),  # q
        'pot_cont_p6': fields.float('Potencia Contratada Periode 6 (kW)',
                                    size=12),  # q
        'data_ulti_mov': fields.datetime('Data Últim Moviment'),
        'data_ult_canv': fields.datetime('Data Últim Canvi Comercialitzador'),
        'data_lim_exten': fields.datetime('Data Límit Drets Extensió'),
        'persona_fj': fields.selection([('0', 'Física'), ('1', 'Jurídica')],
                                       'Tipus Persona'),
        'primera_vivienda': fields.selection([('0', 'Vivienda habitual'),  # ab
                                             ('1', 'No vivienda habitual')],
                                             'Informació Punt Suministre'),
        'fianza': fields.integer('Fiança'),  # No apareix al BOE
        'consum_any_p1': fields.float('Consum últim any Periode 1 (W)',
                                      size=12),
        'consum_any_p2': fields.float('Consum últim any Periode 2 (W)',
                                      size=12),
        'consum_any_p3': fields.float('Consum últim any Periode 3 (W)',
                                      size=12),
        'consum_any_p4': fields.float('Consum últim any Periode 4 (W)',
                                      size=12),
        'consum_any_p5': fields.float('Consum últim any Periode 5 (W)',
                                      size=12),
        'consum_any_p6': fields.float('Consum últim any Periode 6 (W)',
                                      size=12),
        'consum_any_p7': fields.float('Consum últim any Periode 7 (W)',
                                      size=12),
        'max_maximetre_p1': fields.float('Màxim del maximetre P1 (kW)',
                                         size=12),
        'max_maximetre_p2': fields.float('Màxim del maximetre P2 (kW)',
                                         size=12),
        'max_maximetre_p3': fields.float('Màxim del maximetre P3 (kW)',
                                         size=12),
        'max_maximetre_p4': fields.float('Màxim del maximetre P4 (kW)',
                                         size=12),
        'max_maximetre_p5': fields.float('Màxim del maximetre P5 (kW)',
                                         size=12),
        'max_maximetre_p6': fields.float('Màxim del maximetre P6 (kW)',
                                         size=12),
        'max_maximetre_p7': fields.float('Màxim del maximetre P7 (kW)',
                                         size=12),
        'exportat': fields.boolean('Exportat'),  # No apareix al BOE
        'active': fields.boolean('Actiu'),  # No apareix al BOE
    }

    _defaults = {
        'exportat': lambda *a: 0,
        'active': lambda *a: 1
    }

GiscedataSipsPs()


class GiscedataSipsConsums(osv_mongodb.osv_mongodb):
    _name = 'giscedata.sips.consums'
    _columns = {
        'name': fields.char('CUPS', size=22, required=True),
        'data_final': fields.date('Data final'),
        'real_estimada': fields.char('Real/Estimada', size=2),
        'activa_1': fields.float('Consum Activa 1', size=12),
        'tipo_a1': fields.char('Tipo de DH A1', size=2),
        'activa_2': fields.float('Consum Activa 2', size=12),
        'tipo_a2': fields.char('Tipo de DH A2', size=2),
        'activa_3': fields.float('Consum Activa 3', size=12),
        'tipo_a3': fields.char('Tipo de DH A3', size=2),
        'activa_4': fields.float('Consum Activa 4', size=12),
        'tipo_a4': fields.char('Tipo de DH A4', size=2),
        'activa_5': fields.float('Consum Activa 5', size=12),
        'tipo_a5': fields.char('Tipo de DH A5', size=2),
        'activa_6': fields.float('Consum Activa 6', size=12),
        'tipo_a6': fields.char('Tipo de DH A6', size=2),
        'activa_7': fields.float('Consum Activa 7', size=12),
        'tipo_a7': fields.char('Tipo de DH A7', size=2),
        'reactiva_1': fields.float('Consum Reactiva 1', size=12),
        'tipo_r1': fields.char('Tipo de DH R1', size=2),
        'reactiva_2': fields.float('Consum Reactiva 2', size=12),
        'tipo_r2': fields.char('Tipo de DH R2', size=2),
        'reactiva_3': fields.float('Consum Reactiva 3', size=12),
        'tipo_r3': fields.char('Tipo de DH R3', size=2),
        'reactiva_4': fields.float('Consum Reactiva 4', size=12),
        'tipo_r4': fields.char('Tipo de DH R4', size=2),
        'reactiva_5': fields.float('Consum Reactiva 5', size=12),
        'tipo_r5': fields.char('Tipo de DH R5', size=2),
        'reactiva_6': fields.float('Consum Reactiva 6', size=12),
        'tipo_r6': fields.char('Tipo de DH R6', size=2),
        'reactiva_7': fields.float('Consum Reactiva 7', size=12),
        'tipo_r7': fields.char('Tipo de DH R7', size=2),
        'potencia_1': fields.float('Potencia Activa 1', size=12),
        'tipo_p1': fields.char('Tipo de DH P1', size=2),
        'potencia_2': fields.float('PotenciaActiva 2', size=12),
        'tipo_p2': fields.char('Tipo de DH P2', size=2),
        'potencia_3': fields.float('Potencia Activa 3', size=12),
        'tipo_p3': fields.char('Tipo de DH P3', size=2),
        'potencia_4': fields.float('Potencia Activa 4', size=12),
        'tipo_p4': fields.char('Tipo de DH P4', size=2),
        'potencia_5': fields.float('Potencia Activa 5', size=12),
        'tipo_p5': fields.char('Tipo de DH P5', size=2),
        'potencia_6': fields.float('Potencia Activa 6', size=12),
        'tipo_p6': fields.char('Tipo de DH P6', size=2),
        'potencia_7': fields.float('Potencia Activa 7', size=12),
        'tipo_p7': fields.char('Tipo de DH P7', size=2),
    }

GiscedataSipsConsums()


class LogFitxer(osv_mongodb.osv_mongodb):
    _name = 'log.fitxer'
    _columns = {
        'name': fields.char('Arxiu', size=256, required=True, readonly=True),
        'estat': fields.char('Estat', size=256, readonly=True),
        'progres': fields.float('Progres', size=10, readonly=True),
        't_inici': fields.datetime('Temps inici de la importació',
                                   readonly=True),
        't_final': fields.datetime('Temps final de la importació',
                                   readonly=True),
        'elapsed': fields.char('Temps transcorregut', size=256, readonly=True),
        'speed': fields.char("Velocitat d'inserció", size=256, readonly=True),
        'message': fields.char("Missatges d'event", size=300, readonly=True),
        'number_of_lines': fields.integer('Number of lines', readonly=True),
        'number_of_errors': fields.integer('Number of errors', readonly=True),
        'rate_error': fields.float('Rate of errors', readonly=True)
    }
    _order = "t_inici desc"
LogFitxer()

class LogFitxerError(osv_mongodb.osv_mongodb):
    _name = 'log.fitxer.error'
    _columns = {
        'log_fitxer': fields.integer('Id log fitxer', readonly=True),
        'name': fields.char('Arxiu', size=256, readonly=True),
        'line_number': fields.integer('Line number', readonly=True),
        'errors': fields.text('Errors', readonly=True)
    }
LogFitxerError()
