from sippers.backends import register, MongoDBBackend


class ERPMongoDBBackend(MongoDBBackend):

    def __init__(self, uri):
        self.uri = uri.lstrip('erp')
        super(ERPMongoDBBackend, self).__init__(self.uri)

    def insert_ps(self, ps):
        collection = self.ps_collection
        oid = self.db[collection].find_one({'name': ps['name']})
        if oid:
            ps['id'] = oid['id']
            self.db[collection].update({'name': ps['name']}, ps)
        else:
            counter = self.db['counters'].find_and_modify(
                {'_id': self.ps_collection},
                {'$inc': {'counter': 1}}
            )
            ps.update({'id': counter['counter']})
            oid = self.db[collection].insert(ps)
        return oid
        # self.db[collection].remove({"name": ps['name']})
        # counter = self.db['counters'].find_and_modify(
        #     {'_id': self.ps_collection},
        #     {'$inc': {'counter': 1}}
        # )
        # ps.update({'id': counter['counter']})
        # return self.db[collection].insert(ps)

    def insert_measures(self, values):
        collection = self.measures_collection
        self.db[collection].remove({"name": values[0]["name"]})
        counter = self.db['counters'].find_and_modify(
            {'_id': self.measures_collection},
            {'$inc': {'counter': len(values)}}
        )
        if counter['counter'] < len(values):
            self.db['counters'].update({'_id': self.measures_collection},
                                       {'counter': len(values)})
            start = len(values)
        else:
            start = int(counter['counter']) + len(values)
        end = start - len(values)
        ids = []
        for x, value in zip(xrange(start, end, -1), values):
            value['id'] = x
            ids.append(value)
        return self.db[collection].insert(ids)

    def insert_cnmc_measure(self, value):
        collection = self.measures_collection
        self.db[collection].remove(
            {"name" : value["name"],
             "data_final": value["data_final"]
             }
        )
        counter = self.db['counters'].find_and_modify(
            {'_id': self.measures_collection},
            {'$inc': {'counter': 1}}
        )
        if counter['counter'] < 1:
            self.db['counters'].update({'_id': self.measures_collection},
                                       {'counter': 1})
            start = 1
        else:
            start = int(counter['counter']) + 1
        value['id'] = start
        return self.db[collection].insert(value)


register("erpmongodb", ERPMongoDBBackend)
