# -*- coding: utf-8 -*-
{
    "name": "Account Chart IESE",
    "description": """
    Mòdulo para crear las cuentas e impuestos especiales para el sector eléctrico
    TODO: Wizard para asignar los impuestos automáticamente. 
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Account",
    "depends":[
        "base",
        "l10n_chart_ES",
        "account",
        "account_chart_update"
    ],
    "init_xml": [],
    "demo_xml":[
        "account_chart_demo.xml"
    ],
    "update_xml":[
        "taxes_data.xml",
        "account_chart_iese_wizard.xml"
    ],
    "active": False,
    "installable": True
}
