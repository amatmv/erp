# -*- coding: utf-8 -*-
{
    "name": "CTS index",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Index per CTs
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "giscedata_cts",
        "base_index",
        "giscegis_blocs_ctat"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
