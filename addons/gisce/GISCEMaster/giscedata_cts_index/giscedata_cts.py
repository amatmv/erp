from base_index.base_index import BaseIndex
from osv import osv


class GiscegisBlocsCtat(osv.osv):
    _name = "giscegis.blocs.ctat"
    _inherit = "giscegis.blocs.ctat"
    _fields_to_watch = ["vertex"]


GiscegisBlocsCtat()


class GiscedataCts(BaseIndex):
    """
    Class to index CTs
    """
    _name = 'giscedata.cts'
    _inherit = 'giscedata.cts'

    def index_name(self, value):
        """
        Formats the CT name in variour formats to be indexed

        :param value: Name of ct
        :return: Formated names
        """
        number = ''.join(x for x in value if x.isdigit())
        content = [value, number, number.lstrip('0')]
        content = ' '.join(content)
        return content

    _index_title = '{obj.name}'
    _index_summary = '{obj.descripcio} ({obj.id_municipi.name})'

    def date_and_year(self, data):
        """
        Returns a string 'date year' to let search by year only

        :param data: Date
        :return: Formated date
        """
        year = ''
        if data and len(data) > 4:
            year = ' {0}'.format(data[:4])

        return '{0}{1}'.format(data, year)

    _index_fields = {
        'cini': True,
        'name': index_name,
        'descripcio': True,
        'id_municipi.name': True,
        'potencia': lambda self, data: '{0}kVA'.format(data),
        'adreca': True,
        'data_pm': date_and_year,
        'tensio_p': lambda self, data: '{0}V'.format(data)
    }


GiscedataCts()
