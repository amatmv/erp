# -*- coding: utf-8 -*-
import base64
try:
    from cStringIO import StringIO
except:
    from StringIO import StringIO


class colorPicker(object):
    def __init__(self, colors, values):
        self.min_value = min(values)
        self.max_value = max(values)
        self.colors = colors
        self.slice = (self.max_value - self.min_value) / float(len(colors))
        self.slices = [self.min_value + (self.slice * index) for index in xrange(1, len(colors) + 1)]

    def pick_color(self, value):
        for index in xrange(0, len(self.slices)):
            if value < self.slices[index]:
                return self.colors[index]
        return self.colors[-1]


def getCsvData(object, user, mode):
    '''
    Gets attached audit data from invoice
    :param pool:  pool object
    :param mode: file prefix (curve, phf, ....)
    :return: dict with data as
    '''

    pool = object.pool
    att_obj = pool.get('ir.attachment')
    cursor = object._cr
    uid = isinstance(user, int) and user or user.id

    data_csv = ''
    ids = att_obj.search(cursor, uid, [
        ('res_model', '=', 'giscedata.facturacio.factura'),
        ('res_id', '=', object.id),
        ('datas_fname', '=like', '{}_%'.format(mode.upper()))
    ])
    if ids:
        elem = ids[0]
        line = att_obj.read(cursor, uid, elem, ['datas', 'datas_fname'])
        base64_data = line['datas']
        data_csv = StringIO(base64.b64decode(base64_data))
    return data_csv


def getAxisAndData(collection, mode='hourly'):
    '''
    It returns axis list and data list.
    Data may be aggregated in a per day to create a bar dataset
    :param collection: data dictionary {'2018-01-01 01': 0.003,'2018-01-01 01': 0.02,
    :param mode: 'hourly' or 'dayly'
    :return:
     (axis list, data list)
    '''
    axis = []
    data = []
    if mode == 'hourly':
        for key in sorted(collection.keys()):
            axis.append(key)
            data.append(collection[key])
    else:
        # dayly
        days = {}
        # gets total per day
        for key in sorted(collection.keys()):
            axis.append(key)
            day, hour = key.split(' ')
            days[day] = days.get(day, 0.0) + collection[key]
        # totals bars
        for key in sorted(collection.keys()):
            day, hour = key.split(' ')
            if hour in ['00', '01', '23', '24', '25']:
                value = 0
            else:
                value = days[day]
            data.append(value)
    return axis, data
