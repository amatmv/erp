<%
    import csv
    import json
    from datetime import datetime
    from giscedata_facturacio_indexada.report.report_indexada_helpers import getCsvData, colorPicker, getAxisAndData
    from collections import namedtuple

    pool = objects[0].pool
    att_obj = pool.get('ir.attachment')
    cursor = objects[0]._cr
    uid = user.id

    invoice = objects[0]

    green_deg = ['#ddffdd', '#aaffaa', '#77ff77', '#33ff33', '#00ff00']
    Mode = namedtuple('Mode', ['title', 'colors', 'factor'])
    modes = {
        'graph': Mode(
                title=_(u'Evolució horaria'),
                colors=['#0000aa', '#ff0000',],
                factor=1,
        ),
        'curvegraph': Mode(
                title=_(u'Consum'),
                colors=['#00aa00', '#ff0000',],
                factor=1,
        ),
        'phf': Mode(
                title=_(u'Preu Horari Final (cts. de €)'),
                colors=['#00aa00', '#88aa88', '#ffffff', '#ffff00', '#ff8f00'],
                factor=100,
            ),
        'curve': Mode(
                title=_(u'Consum Horari (kWh)'),
                colors=green_deg,
                factor=1,
            ),
        'pmd': Mode(
                title=_(u'Preu OMIE €/kWh'),
                colors=green_deg,
                factor=1,
            ),
        'pc3_ree': Mode(
                title=_(u'Pagament per capacitat mig (€/kWh'),
                colors=green_deg,
                factor=1,
            ),
        'perdues': Mode(
                title=_(u'Pèrdues (%)'),
                colors=green_deg,
                factor=1,
            ),
    }

%>
<%def name="header(mode)">\
    <div class="header">
        <div class="dades_factura">
            <div class="detalls">
                <h2>${_('DETALL FACTURA {} de {} a {}').format(invoice.number or '-',invoice.data_inici ,invoice.data_final)}</h2>
                <br>
                <table>
                    <tr>
                        <td class="bold">CUPS</td>
                        <td>${invoice.cups_id.name or ''}</td>
                        <td style="width: 25px;">&nbsp;</td>
                        <td class="bold">${_(u'Direcció PS')}</td>
                        <td>${invoice.cups_id.direccio or ''}</td>
                    </tr>
                </table>
                <div class="subtitol">
                    ${modes[mode].title}
                </div>
            </div>
        </div>
        <div class="logo">
            <img class="img_logo" src="data:image/jpeg;base64,${invoice.company_id.logo}"/>
        </div>
    </div>
</%def>
<%def name="continguts(mode)">\
    <div>
        <%
            modeObj = modes[mode]
            colors = modeObj.colors
            factor = modeObj.factor
            data_csv = getCsvData(invoice, user, mode)
            if data_csv:

                helper.getTable(data_csv, colors, factor)
            else:
                notFound()
        %>
    </div>
</%def>
<%def name="graph(nom, mode1, mode2)">
    <%
        GraphModeObj = modes[nom]
        data = []
        colors = []
        titles = []

        index = 0
        for mode_str in [mode1, mode2]:
            mode
            data.append([])
            ModeObj = modes[mode_str]
            data_csv = getCsvData(invoice, user, mode_str)
            csv_lines = csv.reader(data_csv, delimiter=';')
            rows = []
            for row in csv_lines:
                rows.append((row[0], float(row[1])))
            collection = dict(rows)
            if index == 0 and nom=='curvegraph':
                axis, data[index] = getAxisAndData(collection, 'dayly')
            else:
                axis, data[index] = getAxisAndData(collection)

            colors.append(GraphModeObj.colors[index])
            titles.append(ModeObj.title)
            index += 1
        styles = ['area-spline', 'spline']
        if nom == 'curvegraph':
            styles = ['area-step', 'spline']
            titles = ['Consum diari [kWh]', 'Consum horari [kWh]']
    %>
    <div id="${nom}"></div>
    <script>

    // Dict with 1 dict element with axis data/hour elements
    var axis = [${axis}]; // [["2018-01-01 01", "2018-01-01 02",...]]

    // dict with two data dicts
    // [[1,2,3,4],[1,2,3,4]]
    var data = [
        ${data[0]},
        ${data[1]},
    ];

    // dict with 2 titles
    // ['Title 1','Title 2']
    var titles = ${json.dumps(titles)};

    // dict with 2 colors
    // ['#ff0000','#00ff00']
    var colors = ${colors};

    // dict with 2 styles (from c3 documentation)
    // ['area-spline','spline']
    var styles = ${styles};

    var ${nom} = create_graph('${nom}', axis, data, titles, colors, styles);

    </script>
</%def>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<link rel="stylesheet" href="${addons_path}/giscedata_facturacio_indexada/report/css/detalle_factura_indexada.css"/>
<link rel="stylesheet" href="${addons_path}/giscedata_facturacio_indexada/report/js/c3.min.css">
<head>
<title>${_(u'Detall Factura Indexada')}</title>
</head>
<body>
    <script src="${addons_path}/giscedata_facturacio_indexada/report/js/d3.min.js" charset="utf-8"></script>
    <script src="${addons_path}/giscedata_facturacio_indexada/report/js/c3.min.js"></script>

    <script src="${addons_path}/giscedata_facturacio_indexada/report/js/index_graph.js"></script>
    <%namespace name="helper" file="/giscedata_facturacio_indexada/report/detall_indexada_helpers.mako" />
    <%
        mode = "curvegraph"
        header(mode)
        graph(mode, 'curve', 'curve')
    %>
    <div style="clear: both;"></div>
    <p style="page-break-after:always"></p>
    <%
        mode = "graph"
        header(mode)
        graph(mode, 'phf', 'curve')
    %>
    <div style="clear: both;"></div>
    <p style="page-break-after:always"></p>
    <%
        mode = "phf"
        header(mode)
        continguts(mode)
    %>
    <div style="clear: both;"></div>
    <p style="page-break-after:always"></p>
    <%
        mode = "curve"
        header(mode)
        continguts(mode)
    %>
    <div style="clear: both;"></div>
    <p style="page-break-after:always"></p>
    <%
        mode = "perdues"
        header(mode)
        continguts(mode)
    %>
</body>
</html>
