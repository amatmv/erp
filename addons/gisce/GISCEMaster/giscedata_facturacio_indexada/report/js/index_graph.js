function create_graph(div, axis, dades, titles, colors, styles){
    var chart = c3.generate({
        bindto: '#' + div,
        size: {
            height: 480,
            width: 1024,
        },
        data: {
            x: 'x',
            columns: [
                ['x'].concat(axis[0]),
                ['data1'].concat(dades[0]),
                ['data2'].concat(dades[1]),
            ],
            axes: {
                data2: 'y2',
            },
            types: {
                data1: styles[0],
                data2: styles[1],
            },
            xFormat: '%Y-%m-%d %H',
            names: {
                data1: titles[0],
                data2: titles[1],
            },
            colors: {
               data1: colors[0],
               data2: colors[1],

            }
        },
        point: {
            show: false
        },
        axis: {
          x: {
             type: 'timeseries',
             tick: {
                     count: axis[0].length / 24,
                     format: '%Y-%m-%d',
             }
          },
          y2: {
            show: true
          }
        }
    });
    return chart
}
