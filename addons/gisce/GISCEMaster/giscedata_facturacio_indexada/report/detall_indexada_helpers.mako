<%def name="getLegend(cp, factor)">
<table class="taula_llegenda">
    <tr>
    %for index in xrange(len(cp.slices)):
        <th class="llegenda_header" style="background-color: ${cp.colors[index]}">${_('Valor {}').format(index)}</th>
    %endfor
    </tr>
    <tr>
    %for index in xrange(len(cp.slices)):
        <td class="llegenda_fila">${index >= 1 and cp.slices[index - 1]*factor or 0} - ${cp.slices[index]*factor}</td>
    %endfor
    </tr>
</table>
</%def>
<%def name="getTable(data_csv, colors, factor)">
    <%
        header = False
        data_act = None
    %>
    <table class="taula_dades">
        <tr>
            <td>&nbsp;</td>
            %for elem in range(1, 26):
                <td class="field">${elem}</td>
            %endfor
        </tr>
        <tr>
        <%
            contador = 1

            csv_lines = csv.reader(data_csv, delimiter=';')
            lines = [l for l in csv_lines]
            values = [float(line[1]) for line in lines if len(line) > 2]

            cp = colorPicker(colors, values)
        %>
        %for line in lines:
            <%
                if len(line) < 2:
                    continue
                csv_reading = line[1]
                reading = csv_reading
                if factor != 1:
                    reading = factor * float(reading)

            %>
            %if (data_act is not None) and data_act != str.split(line[0], ' ')[0]:
                <%
                    header = False
                %>
                %if contador <= 25:
                    %for elem in range(contador, 26):
                        <td>&nbsp;</td>
                    %endfor
                %endif
                </tr><tr>
                <%
                    contador = 1
                %>
            %endif
            %if not header:
                <%
                    header = True
                    data_act = line[0].split(' ')[0]
                    data_print = datetime.strptime(data_act, "%Y-%m-%d").strftime('%d %a')
                %>
                <td class="title">${data_print}</td>
            %endif
            <td class="field" style="background-color: ${cp.pick_color(float(csv_reading))}">${reading}</td>
            <%
                contador += 1
            %>
        %endfor
        %if contador <= 25:
            %for elem in range(contador, 26):
                <td>&nbsp;</td>
            %endfor
        %endif
        </tr>
    </table>
    <%
        getLegend(cp, factor)
    %>
</%def>
<%def name="notFound()">\
    <h2>No se ha podido leer el archivo adjunto</h2>
</%def>
