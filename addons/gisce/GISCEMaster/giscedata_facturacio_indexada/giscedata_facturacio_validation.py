# -*- coding: utf-8 -*-
from osv import osv
from tools.translate import _
from tools import config
import csv

from giscedata_lectures import INVOICE_WITH_CURVE
from libfacturacioatr.pool.component import Prmdiari
import calendar
from datetime import datetime, date
from esios.archives import LIQUICOMUN_PRIORITY as PRIORITY
from giscedata_facturacio_indexada.report.report_indexada_helpers import getCsvData


class GiscedataFacturacioValidationValidator(osv.osv):
    _name = 'giscedata.facturacio.validation.validator'
    _inherit = 'giscedata.facturacio.validation.validator'
    _desc = 'Validador de factures indexada'

    def check_existance_of_curve(self, cursor, uid, clot, date_from,
                                 date_to, parameters):
        polissa_obj = self.pool.get('giscedata.polissa')

        polissa = clot.polissa_id
        # if not profiled
        # invoicing modes that don't need curve (like pvpc)
        no_check_modes = parameters.get('no_check_modes', [])
        tarifa = polissa.tarifa
        mode = polissa.mode_facturacio
        if mode in INVOICE_WITH_CURVE and mode not in no_check_modes:
            res_curve = polissa_obj.validate_existance_of_curve(
                cursor, uid, polissa.id, date_from, date_to
            )
            if len(res_curve) == 0:
                if tarifa.name.startswith('6.'):
                    return {'message': _(u'Es requereix corba completa per poder '
                                         u'facturar.'),
                            'date_from': date_from,
                            'date_to': date_to}
                else:
                    return {'message': _(u'Si es salta la validació es procedirà a '
                                         u'perfilar.'),
                            'date_from': date_from,
                            'date_to': date_to}

        return None

    def check_fullness_of_curve(self, cursor, uid, clot, date_from,
                                date_to, parameters):
        polissa_obj = self.pool.get('giscedata.polissa')
        polissa = clot.polissa_id
        pol_id = polissa.id

        if polissa.mode_facturacio in INVOICE_WITH_CURVE:

            if polissa_obj.validate_existance_of_curve(
                    cursor, uid, pol_id, date_from, date_to):
                # If the curve exists, check fullness
                gaps, model_name = polissa_obj.validate_fullness_of_curve(
                        cursor, uid, pol_id, date_from, date_to)

                if len(gaps) != 0:
                    return {'message': _(u'Es requereix corba completa per '
                                         u'poder facturar.'),
                            'date_from': date_from,
                            'date_to': date_to,
                            'curve': model_name}

            # If the curve is empty return True
        return None

    def check_indexed_esios_version(self, cursor, uid, clot, date_from,
                                    date_to, parameters):

        contract_obj = self.pool.get('giscedata.polissa')

        last_date = datetime.strptime(date_to, '%Y-%m-%d')
        num_days = calendar.monthrange(last_date.year, last_date.month)[1]
        end_date = datetime(
            last_date.year, last_date.month, num_days
        ).date()

        polissa = contract_obj.browse(
            cursor, uid, clot.polissa_id.id, context={'date': date_to}
        )

        if polissa.mode_facturacio not in INVOICE_WITH_CURVE:
            return None

        tarifa_name = polissa.tarifa.name

        esios_token = config.get('esios_token', '')

        postfix = ('{}_{}'.format(
            end_date.strftime("%Y%m01"),
            end_date.strftime("%Y%m%d"))
        )

        # Current version
        prmdiari = Prmdiari('C2_prmdiari_{}'.format(postfix), esios_token)
        cur_version = prmdiari.file_version

        try:
            cur_version_idx = PRIORITY.index(cur_version)
        except ValueError:
            cur_version_idx = PRIORITY.index('A1')

        # Min desired version
        version = parameters.get(tarifa_name, 'A1')
        try:
            version_idx = PRIORITY.index(version)
        except ValueError:
            version_idx = PRIORITY.index('A1')

        if cur_version_idx > version_idx:
            return {
            'cur_version': cur_version,
            'version': version,
            'tariff': tarifa_name
        }
        return None

    def check_indexed_invoice_coef_version(self, cursor, uid, fact, parameters):

        polissa = fact.polissa_id
        if polissa.mode_facturacio not in INVOICE_WITH_CURVE:
            return None

        self.pool.get('ir.attachment')
        tarifa_name = fact.tarifa_acces_id.name

        # audit file to look for
        audit_file = parameters.get('audit_file', 'pmd')

        # get audit file
        csv_data = getCsvData(fact, uid, audit_file)
        csvreader = csv.reader(csv_data, delimiter=";")

        # Min desired version
        version = parameters.get(tarifa_name, 'A1')

        row = csvreader.next()
        cur_version = row[2]

        try:
            version_idx = PRIORITY.index(version)
        except ValueError:
            version_idx = PRIORITY.index('A1')

        if PRIORITY.index(cur_version) > version_idx:
            return {
                'audit': audit_file,
                'cur_version': cur_version,
                'version': version,
                'tariff': tarifa_name
            }
        return None


GiscedataFacturacioValidationValidator()
