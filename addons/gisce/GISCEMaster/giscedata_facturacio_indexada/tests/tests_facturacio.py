from destral import testing
from destral.transaction import Transaction


class TestsInvoiceValidation(testing.OOTestCase):
    def setUp(self):
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.factura.linia')
        warn_obj = self.openerp.pool.get(
            'giscedata.facturacio.validation.warning.template'
        )
        self.txn = Transaction().start(self.database)

        cursor = self.txn.cursor
        uid = self.txn.user

        for linia_id in line_obj.search(cursor, uid, []):
            quant = line_obj.read(
                cursor, uid, linia_id, ['quantity']
            )['quantity']
            line_obj.write(cursor, uid, linia_id, {'quantity': quant})

        # We make sure that all warnings are active
        warn_ids = warn_obj.search(
            cursor, uid, [], context={'active_test': False}
        )
        warn_obj.write(cursor, uid, warn_ids, {'active': True})

    def tearDown(self):
        self.txn.stop()

    def test_check_invoiced_energy_with_real_curve(self):
        vali_obj = self.openerp.pool.get(
            'giscedata.facturacio.validation.validator'
        )
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        imd_obj = self.openerp.pool.get('ir.model.data')

        cursor = self.txn.cursor
        uid = self.txn.user

        fact_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'factura_0008'
        )[1]

        # Set a real curve to the invoice
        fact_obj.write(cursor, uid, [fact_id], {'cch_fact_available': True})
        fact = fact_obj.browse(cursor, uid, fact_id)

        # Set the policy to invoice method with indexed prices
        fact.polissa_id.write({'mode_facturacio': 'index'})

        # A correct invoice should not differ in invoiced energy
        result = vali_obj.check_invoiced_energy(cursor, uid, fact, {})
        self.assertIsNone(result)

        # Change invoiced energy on one invoice line to differ to a maximum
        # of 1 kWh
        for linia in fact.linia_ids:
            if linia.tipus == 'energia':
                linia.write({'quantity': linia.quantity - 1})
                break

        # The validation should not send any error
        fact = fact_obj.browse(cursor, uid, fact_id)
        result = vali_obj.check_invoiced_energy(
            cursor, uid, fact, {}
        )
        self.assertIsNone(result)

        # Change invoiced energy of all invoice lines to differ for more
        # than 1 kWh
        for linia in fact.linia_ids:
            if linia.tipus == 'energia':
                linia.write({'quantity': linia.quantity - 1})

        # The validation should not send any error
        fact = fact_obj.browse(cursor, uid, fact_id)
        result = vali_obj.check_invoiced_energy(
            cursor, uid, fact, {}
        )
        self.assertIsNotNone(result)

    def test_check_invoiced_energy_with_profiled_curve(self):
        vali_obj = self.openerp.pool.get(
            'giscedata.facturacio.validation.validator'
        )
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        imd_obj = self.openerp.pool.get('ir.model.data')

        cursor = self.txn.cursor
        uid = self.txn.user

        fact_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'factura_0008'
        )[1]

        # Set a profiled curve to the invoice
        fact_obj.write(cursor, uid, [fact_id], {'cch_fact_available': False})
        fact = fact_obj.browse(cursor, uid, fact_id)

        # A correct invoice should not differ in invoiced energy
        result = vali_obj.check_invoiced_energy(cursor, uid, fact, {})
        self.assertIsNone(result)

        # Change invoiced energy on one invoice line to differ to a maximum
        # of 1 kWh
        for linia in fact.linia_ids:
            if linia.tipus == 'energia':
                linia.write({'quantity': linia.quantity - 1})
                break

        fact = fact_obj.browse(cursor, uid, fact_id)
        result = vali_obj.check_invoiced_energy(
            cursor, uid, fact, {}
        )
        self.assertIsNotNone(result)
